---
title: >-
    حکایت دو روباه که شکار خسرو شدند
---
# حکایت دو روباه که شکار خسرو شدند

<div class="b" id="bn1"><div class="m1"><p>آن دو روبه چون به هم هم برشدند</p></div>
<div class="m2"><p>پس به عشرت جفت یک دیگر شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسروی در دشت شد با یوز و باز</p></div>
<div class="m2"><p>آن دو روبه را ز هم افکند باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماده می‌پرسد ز نر، کی رخنه‌جوی</p></div>
<div class="m2"><p>ما کجا با هم رسیم، آخر بگوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت اگر ما را بود از عمر بهر</p></div>
<div class="m2"><p>بر دکان پوستین دوزان شهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگری گفتش که ابلیس از غرور</p></div>
<div class="m2"><p>راه بر من می‌زند وقت حضور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چو با او برنمی‌آیم به زور</p></div>
<div class="m2"><p>در دلم از غبن آن افتاد شور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کنم کز وی نجاتی باشدم</p></div>
<div class="m2"><p>وز می معنی حیاتی باشدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت تا پیش توست این نفس سگ</p></div>
<div class="m2"><p>از برت ابلیس نگریزد به تگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشوهٔ ابلیس از تلبیس تست</p></div>
<div class="m2"><p>در تو یک یک آرزو ابلیس تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر کنی یک آرزوی خود تمام</p></div>
<div class="m2"><p>در تو صد ابلیس زاید والسلام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلخن دنیا که زندان آمدست</p></div>
<div class="m2"><p>سر به سر اقطاع شیطان آمدست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست از اقطاع او کوتاه دار</p></div>
<div class="m2"><p>تا نباشد هیچ کس را با تو کار</p></div></div>