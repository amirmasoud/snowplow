---
title: >-
    گم‌شدن شبلی از بغداد
---
# گم‌شدن شبلی از بغداد

<div class="b" id="bn1"><div class="m1"><p>گم شد از بغداد شبلی چندگاه</p></div>
<div class="m2"><p>کس بسوی او کجا می‌برد راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز جستندش به هر موضع بسی</p></div>
<div class="m2"><p>در مخنث خانه‌ای دیدش کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان آن گروهی بی‌ادب</p></div>
<div class="m2"><p>چشم‌تر بنشسته بود و خشک لب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایلی گفت ای برنگ راز جوی</p></div>
<div class="m2"><p>این چه جای تست آخر بازگوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت این قومند چون تردامنی</p></div>
<div class="m2"><p>در ره دنیا نه مرد و نه زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چو ایشانم، ولی در راه دین</p></div>
<div class="m2"><p>نه زنی در دین نه مردی چند ازین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گم شدم در ناجوانمردی خویش</p></div>
<div class="m2"><p>شرم می‌دارم من از مردی خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرک جان خویش را آگاه کرد</p></div>
<div class="m2"><p>ریش خود دستارخوان راه کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو مردان دل خرد کرد اختیار</p></div>
<div class="m2"><p>کرد بر استادگان عزت نثار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر تو بیش آیی ز مویی در نظر</p></div>
<div class="m2"><p>خویشتن را از بتی باشی بتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدح و ذمت گر تفاوت می‌کند</p></div>
<div class="m2"><p>بتگری باشی که او بت می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو حق رابندهٔ، بت‌گر مباش</p></div>
<div class="m2"><p>ور تو مرد ایزدی، آزر مباش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست ممکن در میان خاص و عام</p></div>
<div class="m2"><p>از مقام بندگی برتر مقام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بندگی کن بیش از این دعوی مجوی</p></div>
<div class="m2"><p>مرد حق شو، عزت از عزی مجوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون ترا صد بت بود در زیر دلق</p></div>
<div class="m2"><p>چون نمایی خویش را صوفی به خلق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای مخنث، جامهٔ مردان مدار</p></div>
<div class="m2"><p>خویش را زین بیش سرگردان مدار</p></div></div>