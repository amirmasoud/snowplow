---
title: >-
    حکایت مرد توبه شکن
---
# حکایت مرد توبه شکن

<div class="b" id="bn1"><div class="m1"><p>کرده بود آن مرد بسیاری گناه</p></div>
<div class="m2"><p>توبه کرد از شرم، بازآمد به راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار دیگر نفس چون قوت گرفت</p></div>
<div class="m2"><p>توبه بشکست و پی شهوت گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدتی دیگر ز راه افتاده بود</p></div>
<div class="m2"><p>در همه نوعی گناه افتاده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از آن دردی درآمد در دلش</p></div>
<div class="m2"><p>وز خجالت کار شد بس مشکلش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به جز بی حاصلی بهره نداشت</p></div>
<div class="m2"><p>خواست تا توبه کند زهره نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب چون قلیه وی بر تابه‌ای</p></div>
<div class="m2"><p>دل پر آتش داشت در خونابه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر غباری در رهش پیوست بود</p></div>
<div class="m2"><p>ز آب چشم او همه بنشست بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سحرگه هاتفیش آواز داد</p></div>
<div class="m2"><p>سازگارش کرد، کارش ساز داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت می‌گوید خداوند جهان</p></div>
<div class="m2"><p>چون در اول توبه کردی ای فلان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عفو کردم، توبه بپذیرفتمت</p></div>
<div class="m2"><p>می‌توانستم ولی نگرفتمت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بار دیگر چون شکستی توبه پاک</p></div>
<div class="m2"><p>دادمت مهل و نگشتم خشم‌ناک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور چنانست این زمان ای بی‌خبر</p></div>
<div class="m2"><p>آرزوی تو که بازآیی دگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بازآی آخر که در بگشاده‌ایم</p></div>
<div class="m2"><p>تو غرامت کرده باز ایستاده‌ایم</p></div></div>