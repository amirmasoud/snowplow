---
title: >-
    نارضا بودن ایاز از اینکه محمود سلطنت را به او داد
---
# نارضا بودن ایاز از اینکه محمود سلطنت را به او داد

<div class="b" id="bn1"><div class="m1"><p>گفت ایاز خاص را محمود خواند</p></div>
<div class="m2"><p>تاج دارش کرد و بر تختش نشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت شاهی دادمت، لشگر تراست</p></div>
<div class="m2"><p>پادشاهی کن که این کشور تراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن همی‌خواهم که تو شاهی کنی</p></div>
<div class="m2"><p>حلقه در گوش مه و ماهی کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه آن بشنود از خیل و سپاه</p></div>
<div class="m2"><p>جمله را شد چشم از آن غیرت سیاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی می‌گفت شاهی با غلام</p></div>
<div class="m2"><p>در جهان هرگز نکرد این احترام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک آن ساعت ایاز هوشیار</p></div>
<div class="m2"><p>می‌گریست از کار سلطان زار زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمله گفتندش که تو دیوانه‌ای</p></div>
<div class="m2"><p>می‌ندانی وز خرد بیگانه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون به سلطانی رسیدی ای غلام</p></div>
<div class="m2"><p>چیست چندین گریه، بنشین شادکام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داد ایاز آن قوم را حالی جواب</p></div>
<div class="m2"><p>گفت بس دورید از راه صواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیستی آگه که شاه انجمن</p></div>
<div class="m2"><p>دور می‌اندازدم از خویشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌دهد مشغولیم تا من ز شاه</p></div>
<div class="m2"><p>بازمانم دور مشغول سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به حکم من کند ملک جهان</p></div>
<div class="m2"><p>من نگردم غایب از وی یک زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچ گوید آن توانم کرد و بس</p></div>
<div class="m2"><p>لیک ازو دوری نجویم یک نفس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من چه خواهم کرد ملک و کار او</p></div>
<div class="m2"><p>ملکت من بس بود دیدار او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر تو مرد طالبی و حق‌شناس</p></div>
<div class="m2"><p>بندگی کردن درآموز از ایاس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای به روز و شب معطل مانده</p></div>
<div class="m2"><p>همچنان بر گام اول مانده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر شبی از بهر تو ای بوالفضول</p></div>
<div class="m2"><p>می‌کنند از اوج جباری نزول</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو ز جای خود چو مردی بی‌ادب</p></div>
<div class="m2"><p>برنگیری گام، نه روز و نه شب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آمدند از اوج عزت پیش باز</p></div>
<div class="m2"><p>تو ز پس رفتی و کردی احتراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای دریغا نیستی تو مرد این</p></div>
<div class="m2"><p>با که بتوان گفت آخر درد این</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا بهشت و دوزخت در ره بود</p></div>
<div class="m2"><p>جان توزین رازکی آگه بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون ازین هر دو برون آیی تمام</p></div>
<div class="m2"><p>صبح این دولت برونت آید ز شام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گلشن جنت نه این اصحاب راست</p></div>
<div class="m2"><p>زانک علیون ذوی الالباب راست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو چو مردان، این بدین ده آن بدان</p></div>
<div class="m2"><p>درگذر، نه دل بدین ده نه بدان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون ز هر دو درگذشتی فرد تو</p></div>
<div class="m2"><p>گر زنی باشی تو باشی مرد تو</p></div></div>