---
title: >-
    حکایت عنکبوت و خانهٔ او
---
# حکایت عنکبوت و خانهٔ او

<div class="b" id="bn1"><div class="m1"><p>دیدهٔ آن عنکبوت بی‌قرار</p></div>
<div class="m2"><p>در خیالی می‌گذارد روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش گیرد وهم دوراندیش را</p></div>
<div class="m2"><p>خانه‌ای سازد به کنجی خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوالعجب دامی بسازد از هوس</p></div>
<div class="m2"><p>تا مگر در دامش افتد یک مگس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مگس افتد به دامش سرنگون</p></div>
<div class="m2"><p>برمکد از عرق آن سرگشته خون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از آن خشکش کند بر جایگاه</p></div>
<div class="m2"><p>قوت خود سازد از و تا دیرگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگهی باشد که آن صاحب سرای</p></div>
<div class="m2"><p>چوب اندر دست، استاده بپای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانهٔ آن عنکبوت و آن مگس</p></div>
<div class="m2"><p>جمله ناپیدا کند در یک نفس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست دنیا، وانک دروی ساخت قوت</p></div>
<div class="m2"><p>چون مگس در خانهٔ آن عنکبوت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر همه دنیا مسلم آیدت</p></div>
<div class="m2"><p>گم شود تا چشم بر هم آیدت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به شاهی سرفرازی می‌کنی</p></div>
<div class="m2"><p>طفل راه پرده بازی می‌کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک مطلب گر نخوردی مغز خر</p></div>
<div class="m2"><p>ملک گاوان را دهند ای بی‌خبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرک از کوس و علم درویش نیست</p></div>
<div class="m2"><p>مرد او ، کان بانگ بادی بیش نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست بادی در علم، در کوس بانگ</p></div>
<div class="m2"><p>باد بانگی کمتر ارزد نیم دانگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابلق بیهودگی چندین متاز</p></div>
<div class="m2"><p>در غرور خواجگی چندین مناز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پوست آخر درکشیدند از پلنگ</p></div>
<div class="m2"><p>درکشند آخر ز تو هم بی‌درنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون محال آمد پدیدار آمدن</p></div>
<div class="m2"><p>گم شدن به یا نگو سار آمدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست ممکن سرفرازی کردنت</p></div>
<div class="m2"><p>سر بنه تا کی ز بازی کردنت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا بنه این سروری دیگر مکن</p></div>
<div class="m2"><p>یا ز سربازی بنه در سرمکن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای سر ای و باغ تو زندان تو</p></div>
<div class="m2"><p>وای جانت، وابلای جان تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در گذر زین خاکدان پر غرور</p></div>
<div class="m2"><p>چند پیمایی جهان ای ناصبور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشم همت برگشای و ره ببین</p></div>
<div class="m2"><p>پس قدم در ره نه و درگه ببین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون رسانیدی بدان درگاه جان</p></div>
<div class="m2"><p>خود نگنجی تو ز عزت در جهان</p></div></div>