---
title: >-
    گفتار شبلی که پس از مردن به خواب جوانمردی آمد
---
# گفتار شبلی که پس از مردن به خواب جوانمردی آمد

<div class="b" id="bn1"><div class="m1"><p>چون بشد شبلی ازین جای خراب</p></div>
<div class="m2"><p>بعد از آن دیدش جوامردی به خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت حق با تو چه کرد ای نیک بخت</p></div>
<div class="m2"><p>گفت ؛ چون شد در حسابم کار سخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مرا بس خویشتن دشمن بدید</p></div>
<div class="m2"><p>ضعف و نومیدی و عجز من بدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمتش آمد بدان بیچارگیم</p></div>
<div class="m2"><p>پس ببخشود از کرم یک بارگیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خالقا بیچارهٔ راهم ترا</p></div>
<div class="m2"><p>همچو موری لنگ در چاهم ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نمی‌دانم که من اهل چه‌ام</p></div>
<div class="m2"><p>یا کجاام یا کدامم یا که‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌تنی بی‌دولتی بی‌حاصلی</p></div>
<div class="m2"><p>بی‌نوایی بی‌قراری بی‌دلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر در خون جگر بگداخته</p></div>
<div class="m2"><p>بهرهٔ از عمر ناپرداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه کرده جمله تاوان آمده</p></div>
<div class="m2"><p>جان به لب عمرم به پایان آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل ز دستم رفته و دین گم شده</p></div>
<div class="m2"><p>صورتم نامانده معنی گم شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نه کافر نه مسلمان مانده</p></div>
<div class="m2"><p>در میان هر دو حیران مانده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه مسلمانم نه کافر، چون کنم</p></div>
<div class="m2"><p>مانده سرگردان و مضطر، چون کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دری تنگم گرفتارآمده</p></div>
<div class="m2"><p>روی در دیوار پندار آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر من بیچاره این در برگشای</p></div>
<div class="m2"><p>وین ز راه افتاده را راهی نمای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنده را گر نیست زاد راه هیچ</p></div>
<div class="m2"><p>می‌نیاساید ز اشک و آه هیچ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم توانی سوخت از آهش گناه</p></div>
<div class="m2"><p>هم ز اشکش شست دیوان سیاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که دریاهای اشکش حاصل است</p></div>
<div class="m2"><p>گو بیا کو درخور این منزل است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وانک او را دیدهٔ خون بار نیست</p></div>
<div class="m2"><p>گو برو کو را بر ما کار نیست</p></div></div>