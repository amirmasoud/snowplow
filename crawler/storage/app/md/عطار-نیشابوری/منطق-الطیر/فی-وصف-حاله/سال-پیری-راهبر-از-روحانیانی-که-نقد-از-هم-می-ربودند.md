---
title: >-
    سال پیری راهبر از روحانیانی که نقد از هم می‌ربودند
---
# سال پیری راهبر از روحانیانی که نقد از هم می‌ربودند

<div class="b" id="bn1"><div class="m1"><p>در رهی می‌رفت پیری راهبر</p></div>
<div class="m2"><p>دید از روحانیان خلقی مگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود نقدی سخت رایج در میان</p></div>
<div class="m2"><p>می‌ربودند آن ز هم روحانیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیر کرد آن قوم را حالی سؤال</p></div>
<div class="m2"><p>گفت چیست این نقد برگویید حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ روحانیش گفت ای پیرراه</p></div>
<div class="m2"><p>دردمندی می‌گذشت این جایگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برکشید آهی ز دل پاک و برفت</p></div>
<div class="m2"><p>ریخت اشک گرم بر خاک و برفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما کنون آن اشک گرم و آه سرد</p></div>
<div class="m2"><p>می‌بریم از یک دگر در راه درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رب اشک و آه بسیاریم هست</p></div>
<div class="m2"><p>گر ندارم هیچ این باریم هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون روایی دارد آنجا اشک راه</p></div>
<div class="m2"><p>بنده دارد این متاع آن جایگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پاک کن از آه صحن جان من</p></div>
<div class="m2"><p>پس بشوی از اشک من دیوان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌روم گم راه، ره نایافته</p></div>
<div class="m2"><p>دل چو دیوان جز سیه نایافته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ره نمایم باش و دیوانم بشوی</p></div>
<div class="m2"><p>از دو عالم تختهٔ جانم بشوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی‌نهایت درد دل دارم ز تو</p></div>
<div class="m2"><p>جان اگر دارم خجل دارم ز تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر در اندوه تو بردم به سر</p></div>
<div class="m2"><p>کاشکی بودیم صد عمر دگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا در اندوهت به سر می‌بردمی</p></div>
<div class="m2"><p>هر زمان دردی دگر می‌بردمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مانده‌ام از دست خود در صد ز حیر</p></div>
<div class="m2"><p>دست من ای دست گیر من تو گیر</p></div></div>