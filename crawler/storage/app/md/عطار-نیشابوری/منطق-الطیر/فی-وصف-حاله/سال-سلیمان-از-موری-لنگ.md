---
title: >-
    سال سلیمان از موری لنگ
---
# سال سلیمان از موری لنگ

<div class="b" id="bn1"><div class="m1"><p>چون سلیمان کرد با چندان کمال</p></div>
<div class="m2"><p>پیش موری لنگ از عجز آن سؤال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت برگوی ای ز من آغشته‌تر</p></div>
<div class="m2"><p>تا کدامین گل به غم به سر شسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد آن ساعت جوابش مور لنگ</p></div>
<div class="m2"><p>گفت خشت واپسین در گور تنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واپسین خشتی که پیوندد به خاک</p></div>
<div class="m2"><p>منقطع گردد همه اومید پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مرا در زیر خاک ای پاک ذات</p></div>
<div class="m2"><p>منقطع گردد امید از کاینات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس بپوشد خشت آخر روی من</p></div>
<div class="m2"><p>تو مگردان روی فضل از سوی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به خاک آرم سرگشته روی</p></div>
<div class="m2"><p>هیچ با رویم میار از هیچ سوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی آن دارد کزان چندان گناه</p></div>
<div class="m2"><p>هیچ با رویم نیاری ای اله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو کریم مطلقی ای کردگار</p></div>
<div class="m2"><p>عفو کن از هرچ رفت و در گذار</p></div></div>