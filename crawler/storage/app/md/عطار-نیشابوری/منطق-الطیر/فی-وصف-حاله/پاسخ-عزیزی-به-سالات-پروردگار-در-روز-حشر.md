---
title: >-
    پاسخ عزیزی به سالات پروردگار در روز حشر
---
# پاسخ عزیزی به سالات پروردگار در روز حشر

<div class="b" id="bn1"><div class="m1"><p>آن عزیزی گفت فردا ذوالجلال</p></div>
<div class="m2"><p>گر کند در دشت حشر از من سؤال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای فرو مانده چه آوردی ز راه</p></div>
<div class="m2"><p>گویم از زندان چه آرند ای اله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرق ادبارم ز زندان آمده</p></div>
<div class="m2"><p>پای و سر گم کرده حیران آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد در کف خاک درگاه توم</p></div>
<div class="m2"><p>بنده و زندانی راه توم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی آن دارد که نفروشی مرا</p></div>
<div class="m2"><p>خلعتی از فضل درپوشی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین همه آلودگی پاکم بری</p></div>
<div class="m2"><p>در مسلمانی فرو خاکم بری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نهان گردد تنم در خاک و خشت</p></div>
<div class="m2"><p>بگذری از هرچ کردم خوب و زشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفریدن رایگانم چون رواست</p></div>
<div class="m2"><p>رایگانم گر بیامرزی سزاست</p></div></div>