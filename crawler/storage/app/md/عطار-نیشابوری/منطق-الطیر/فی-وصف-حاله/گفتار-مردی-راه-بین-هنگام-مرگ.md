---
title: >-
    گفتار مردی راه‌بین هنگام مرگ
---
# گفتار مردی راه‌بین هنگام مرگ

<div class="b" id="bn1"><div class="m1"><p>راه بینی وقت پیچاپیچ مرگ</p></div>
<div class="m2"><p>گفت چون ره را ندارم زاد و برگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خوی خجلت کفی گل کرده‌ام</p></div>
<div class="m2"><p>پس از و خشتی به حاصل کرده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیشهٔ پر اشک دارم نیز من</p></div>
<div class="m2"><p>ژندهٔ برچیده‌ام بهر کفن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اولم زان اشک اگر خونی دهید</p></div>
<div class="m2"><p>آخرم آن خشت زیر سرنهید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان کفن در آب چشم آغشته‌ام</p></div>
<div class="m2"><p>ای دریغا سر به سر به سرشته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کفن چون در تنم پوشید پاک</p></div>
<div class="m2"><p>زود تسلیمم کنید آنگه به خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون چنین کردید، تا محشر ز میغ</p></div>
<div class="m2"><p>بر سر خاکم نبارد جز دریغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانی این چندین دریغا بهر چیست</p></div>
<div class="m2"><p>پشه‌ای با باد نتوانست زیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه از خورشید می‌جوید وصال</p></div>
<div class="m2"><p>می‌نیابد، اینت سودا و محال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه هست این خود محالی آشکار</p></div>
<div class="m2"><p>جز محال اندیشی او را نیست کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرک او ننهد درین اندیشه سر</p></div>
<div class="m2"><p>او ازین بهتر چه اندیشه دگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخت‌تر بینم بهر دم مشکلم</p></div>
<div class="m2"><p>چون بپردازم ازین مشکل دلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کیست چون من فرد و تنها مانده</p></div>
<div class="m2"><p>خشک لب غرقاب دریا مانده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه مرا هم راز و هم دم هیچ کس</p></div>
<div class="m2"><p>نه مرا هم درد و محرم هیچ کس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه ز همت میل ممدوحی مرا</p></div>
<div class="m2"><p>نه ز ظلمت خلوت روحی مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه دل کس نه دل خود نیز هم</p></div>
<div class="m2"><p>نه سر نیک و سر بد نیز هم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه هوای لقمهٔ سلطان مرا</p></div>
<div class="m2"><p>نه قفای سیلی دربان مرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه به تنهایی صبوری یک دمم</p></div>
<div class="m2"><p>نه بدل از خلق دوری یک دمم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست احوال من زیر و زبر</p></div>
<div class="m2"><p>همچنان کان پیر داد از خود خبر</p></div></div>