---
title: >-
    گفتهٔ پاک‌دینی که سی‌سال عمر بی‌خود می‌گذارد
---
# گفتهٔ پاک‌دینی که سی‌سال عمر بی‌خود می‌گذارد

<div class="b" id="bn1"><div class="m1"><p>پاک دینی گفت سی سال تمام</p></div>
<div class="m2"><p>عمر بی‌خود می‌گذارم بر دوام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو اسمعیل در خود ناپدید</p></div>
<div class="m2"><p>آن زمان کو را پدر سر می‌برید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بود آنکس که او عمری گذاشت</p></div>
<div class="m2"><p>همچو آن یک دم که اسمعیل داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس چه داند تا درین حبس تعب</p></div>
<div class="m2"><p>عمر خود چون می‌گذارم روز و شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه می‌سوزم چو شمع از انتظار</p></div>
<div class="m2"><p>گاه می‌گریم چر ابر نوبهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو فروغ شمع می‌بینی خوشی</p></div>
<div class="m2"><p>می‌نبینی در سر او آتشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنک از بیرون کند در تن نگاه</p></div>
<div class="m2"><p>کی بود هرگز درون سینه راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خم چوگان چه گویی، هیچ جای</p></div>
<div class="m2"><p>می‌ندانم پای از سر، سر ز پای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از وجودم خود نکردم هیچ سود</p></div>
<div class="m2"><p>کانچ کردم وانچ گفتم هیچ بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دریغا نیست از کس یاریم</p></div>
<div class="m2"><p>عمر ضایع گشت در بی‌کاریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون توانستم ندانستم ، چه سود</p></div>
<div class="m2"><p>چون بدانستم، توانستم نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این زمان جز عجز و جز بیچارگی</p></div>
<div class="m2"><p>می‌ندارم چارهٔ یک بارگی</p></div></div>