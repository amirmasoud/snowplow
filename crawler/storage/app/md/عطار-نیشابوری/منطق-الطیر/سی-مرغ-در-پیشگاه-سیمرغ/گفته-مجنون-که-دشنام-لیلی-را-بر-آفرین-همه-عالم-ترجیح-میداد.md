---
title: >-
    گفتهٔ مجنون که دشنام لیلی را بر آفرین همهٔ عالم ترجیح میداد
---
# گفتهٔ مجنون که دشنام لیلی را بر آفرین همهٔ عالم ترجیح میداد

<div class="b" id="bn1"><div class="m1"><p>گفت مجنون گر همه روی زمین</p></div>
<div class="m2"><p>هر زمان بر من کنندی آفرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نخواهم آفرین هیچ کس</p></div>
<div class="m2"><p>مدح من دشنام لیلی باد و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشتراز صد مدح یک دشنام او</p></div>
<div class="m2"><p>بهتر از ملک دو عالم نام او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مذهب خود با توگفتم ای عزیز</p></div>
<div class="m2"><p>گر بود خواری چه خواهد بود نیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت برق عزت آید آشکار</p></div>
<div class="m2"><p>پس برآرد از همه جانها دمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بسوزد جان به صد زاری چه سود</p></div>
<div class="m2"><p>آنگهی از عزت و خواری چه سود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازگفتند آن گروه سوخته</p></div>
<div class="m2"><p>جان ما و آتش افروخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی شود پروانه از آتش نفور</p></div>
<div class="m2"><p>زانک او را هست در آتش حضور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه ما را دست ندهد وصل یار</p></div>
<div class="m2"><p>سوختن ما را دهد دست، اینت کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر رسیدن سوی آن دلخواه نیست</p></div>
<div class="m2"><p>پاک پرسیدن جز اینجا راه نیست</p></div></div>