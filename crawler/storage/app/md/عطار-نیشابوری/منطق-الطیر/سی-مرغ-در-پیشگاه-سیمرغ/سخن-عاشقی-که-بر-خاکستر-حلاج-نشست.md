---
title: >-
    سخن عاشقی که بر خاکستر حلاج نشست
---
# سخن عاشقی که بر خاکستر حلاج نشست

<div class="b" id="bn1"><div class="m1"><p>گفت چون در آتش افروخته</p></div>
<div class="m2"><p>گشت آن حلاج کلی سوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقی آمد مگر چوبی بدست</p></div>
<div class="m2"><p>بر سر آن طشت خاکستر نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس زفان بگشاد هم چون آتشی</p></div>
<div class="m2"><p>باز می‌شورید خاکستر خوشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانگهی می‌گفت برگویید راست</p></div>
<div class="m2"><p>کانک خوش می‌زد انا الحق او کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچ گفتی آنچ بشنیدی همه</p></div>
<div class="m2"><p>وانچ دانستی و می‌دیدی همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن همه جز اول افسانه نیست</p></div>
<div class="m2"><p>محو شو چون جایت این ویرانه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اصل باید، اصل مستغنی و پاک</p></div>
<div class="m2"><p>گر بود فرع و اگر نبود چه باک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست خورشید حقیقی بر دوام</p></div>
<div class="m2"><p>گونه ذره‌مان نه سایه والسلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون برآمد صد هزاران قرن بیش</p></div>
<div class="m2"><p>قرنهای بی زمان نه پس نه پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعد از آن مرغان فانی را بناز</p></div>
<div class="m2"><p>بی‌فنای کل به خود دادند باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون همه خویش با خویش آمدند</p></div>
<div class="m2"><p>در بقا بعد از فنا پیش آمدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست هرگز، گر نوست و گر کهن</p></div>
<div class="m2"><p>زان فنا و زان بقا کس را سخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم چنان کو دور دورست از نظر</p></div>
<div class="m2"><p>شرح این دورست از شرح و خبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیکن از راه مثال اصحابنا</p></div>
<div class="m2"><p>شرح جستند از بقا بعد الفنا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن کجا اینجا توان پرداختن</p></div>
<div class="m2"><p>نو کتابی باید آن را ساختن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زانک اسرار البقا بعد الفنا</p></div>
<div class="m2"><p>آن شناسد کو بود آنرا سزا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا تو هستی در وجود و در عدم</p></div>
<div class="m2"><p>کی توانی زد درین منزل قدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون نه این ماند نه آن در ره ترا</p></div>
<div class="m2"><p>خواب چون می‌آید ای ابله ترا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در نگر تا اول و آخر چه بود</p></div>
<div class="m2"><p>گر به آخر دانی این آخر چه سود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نطفهٔ پرورده در صد عز و ناز</p></div>
<div class="m2"><p>تا شده هم عاقل و هم کار ساز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرده او را واقف اسرار خویش</p></div>
<div class="m2"><p>داده او را معرفت در کار خویش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بعد از آنش محو کرده محو کل</p></div>
<div class="m2"><p>زان همه عزت درافکنده بذل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باز گردانیده او را خاک راه</p></div>
<div class="m2"><p>باز کرده فانی او را چندگاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس میان این فنا صد گونه راز</p></div>
<div class="m2"><p>گفته بی او، لیک با او گفته باز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بعد از آن او را بقایی داده کل</p></div>
<div class="m2"><p>عین عزت کرده بر وی عین ذل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو چه دانی تا چه داری پیش تو</p></div>
<div class="m2"><p>با خود آی آخر فرواندیش تو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نگردد جان تو مردود شاه</p></div>
<div class="m2"><p>کی شوی مقبول شاه آن جایگاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا نیابی در فنا کم کاستی</p></div>
<div class="m2"><p>در بقا هرگز نبینی راستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اول اندازد بخواری در رهت</p></div>
<div class="m2"><p>باز برگیرد به عزت ناگهت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیست شو تا هستیت از پی رسد</p></div>
<div class="m2"><p>تا تو هستی، هست در تو کی رسد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا نگردی محو خواری فنا</p></div>
<div class="m2"><p>کی رسد اثبات از عز بقا</p></div></div>