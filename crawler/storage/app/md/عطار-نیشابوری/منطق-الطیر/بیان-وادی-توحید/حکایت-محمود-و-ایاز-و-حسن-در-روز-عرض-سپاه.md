---
title: >-
    حکایت محمود و ایاز و حسن در روز عرض سپاه
---
# حکایت محمود و ایاز و حسن در روز عرض سپاه

<div class="b" id="bn1"><div class="m1"><p>گفت روزی فرخ و مسعود بود</p></div>
<div class="m2"><p>روز عرض لشگر محمود بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد به صحرا بی‌عدد پیل و سپاه</p></div>
<div class="m2"><p>بود بالایی، بر آنجا رفت شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد بر او هم ایاز و هم حسن</p></div>
<div class="m2"><p>هر سه می‌کردند عرض انجمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود روی عالم از پیل و سپاه</p></div>
<div class="m2"><p>همچو از مور و ملخ بگرفته راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم عالم آن چنان لشگر ندید</p></div>
<div class="m2"><p>بیش از آن لشگر کسی دیگر ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس زفان بگشاد شاه نامور</p></div>
<div class="m2"><p>با ایاز خاص خود گفت، ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست چندین پیل و لشگر آن من</p></div>
<div class="m2"><p>من همه آن تو، تو سلطان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه گفت این لفظ شاه نامدار</p></div>
<div class="m2"><p>سخت فارغ بود ایاز و برقرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه را خدمت نکرد این جایگاه</p></div>
<div class="m2"><p>خود نگفت او کین مرا گفتست شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد حسن آشفته وگفت ای غلام</p></div>
<div class="m2"><p>می‌کند شاهیت چندین احترام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو چنین استاده چون بی حرمتی</p></div>
<div class="m2"><p>پشت خم ندهی و نکنی خدمتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو چرا حرمت نمی‌داری نگاه</p></div>
<div class="m2"><p>حق‌شناسی نبود این در پیش شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون ایاز القصه بشنود این خطاب</p></div>
<div class="m2"><p>گفت هست این را موافق دو جواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک جواب آنست کین بی‌روی و راه</p></div>
<div class="m2"><p>گر کند خدمت به پیش پادشاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا به خاک افتد به خواری پیش او</p></div>
<div class="m2"><p>یا سخن گوید بزاری پیش او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیشتر از شاه و کمتر آمدن</p></div>
<div class="m2"><p>جمله باشد در برابر آمدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من کیم تا سر بدین کار آورم</p></div>
<div class="m2"><p>در میان خود را پدیدار آورم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنده آن اوست و تشریف آن اوست</p></div>
<div class="m2"><p>من کیم، فرمان همه فرمان اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنچ هر روزی شه پیروز کرد</p></div>
<div class="m2"><p>وین کرم کو با ایاز امروز کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر دو عالم خطبهٔ ذاتش کنند</p></div>
<div class="m2"><p>می‌ندانم تا مکافاتش کنند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من دریغ معرض کجا آیم پدید</p></div>
<div class="m2"><p>من که باشم، یا چرا آیم پدید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نی کنم خدمت نه در سر آیمش</p></div>
<div class="m2"><p>کیستم تا در برابر آیمش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون حسن بشنود این قول از ایاس</p></div>
<div class="m2"><p>گفت احسنت ای ایاز حق شناس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خط بدادم من که در ایام شاه</p></div>
<div class="m2"><p>لایقی هر دم به صد انعام شاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس حسن دیگر بگفتش کو جواب</p></div>
<div class="m2"><p>گفت نیست آن پیش تو گفتن صواب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر من و شه هر دو با هم بودمی</p></div>
<div class="m2"><p>این سخن را سخت محرم بودمی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لیک تو چون محرم آن نیستی</p></div>
<div class="m2"><p>چون بگویم، چون تو سلطان نیستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس حسن را زود بفرستاد شاه</p></div>
<div class="m2"><p>شد حسن نیز از حساب آن سپاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون در آن خلوت نه ما بود و نه من</p></div>
<div class="m2"><p>گر حسن مویی شود نبود حسن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاه گفتا خلوت آمد، راز گوی</p></div>
<div class="m2"><p>آن جواب خاص با من باز گوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفت هر گه از کمال لطف شاه</p></div>
<div class="m2"><p>می‌کند سوی من مسکین نگاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در فروغ پرتو آن یک نظر</p></div>
<div class="m2"><p>محو می‌گردد وجودم سر به سر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از حیای آفتاب فر شاه</p></div>
<div class="m2"><p>پاک برمی خیزم آن ساعت ز راه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون نمی‌ماند ز من نام وجود</p></div>
<div class="m2"><p>چون به خدمت پیشت افتم در سجود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر تو می‌بینی کسی را آن زمان</p></div>
<div class="m2"><p>من نیم آن هست هم شاه جهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر تو یک لطف و اگر صد می‌کنی</p></div>
<div class="m2"><p>از خداوندی تو با خود می‌کنی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سایه‌ای کو گم شود در آفتاب</p></div>
<div class="m2"><p>زو کی آید خدمتی در هیچ باب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هست ایازت سایه‌ای در کوی تو</p></div>
<div class="m2"><p>گم شده در آفتاب روی تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون شد از خود بنده فانی او نماند</p></div>
<div class="m2"><p>هرچ خواهی کن تو دانی او نماند</p></div></div>