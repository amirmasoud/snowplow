---
title: >-
    عقیدهٔ دیوانه‌ای دربارهٔ عالم
---
# عقیدهٔ دیوانه‌ای دربارهٔ عالم

<div class="b" id="bn1"><div class="m1"><p>گفت آن دیوانه را مردی عزیز</p></div>
<div class="m2"><p>چیست عالم، شرح ده این مایه چیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت هست این عالم پر نام و ننگ</p></div>
<div class="m2"><p>همچو نخلی بسته از صد گونه رنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به دست این نخل می‌مالد یکی</p></div>
<div class="m2"><p>آن همه یک موم گردد بی‌شکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون همه مومست و چیزی نیز نیست</p></div>
<div class="m2"><p>رو که چندان رنگ جز یک چیز نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون یکی باشد همه، نبود دوی</p></div>
<div class="m2"><p>نه منی برخیزد اینجا نه توی</p></div></div>