---
title: >-
    گفتار مردی صوفی با کسی که او را قفا زد
---
# گفتار مردی صوفی با کسی که او را قفا زد

<div class="b" id="bn1"><div class="m1"><p>صوفیی می‌رفت چون بی‌حاصلی</p></div>
<div class="m2"><p>زد قفای محکمش سنگین دلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دلی پر خون سر از پس کرد او</p></div>
<div class="m2"><p>گفت آنک از تو قفایی خورد او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرب سی سالست تا او مرد و رفت</p></div>
<div class="m2"><p>عالم هستی به پایان برد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد گفتش ای همه دعوی نه کار</p></div>
<div class="m2"><p>مرده کی گوید سخن، شرمی بدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که تو دم می‌زنی هم دم نه‌ای</p></div>
<div class="m2"><p>تا که مویی ماندهٔ محرم نه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بود مویی اضافت در میان</p></div>
<div class="m2"><p>هست صد عالم مسافت در میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو خواهی تا بدین منزل رسی</p></div>
<div class="m2"><p>تا که مویی ماندهٔ مشکل رسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچ داری، آتشی را برفروز</p></div>
<div class="m2"><p>تا از ارپای بر آتش بسوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نماندت هیچ، مندیش از کفن</p></div>
<div class="m2"><p>برهنه خود را به آتش در فکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تو و رخت تو خاکستر شود</p></div>
<div class="m2"><p>ذرهٔ پندار تو کمتر شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور چو عیسی از تو یک سوزن بماند</p></div>
<div class="m2"><p>در رهت می‌دان که صد ره زن بماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه عیسی رخت در کوی او فکند</p></div>
<div class="m2"><p>سوزنش هم بخیه بر روی او فکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون حجاب آید وجود این جایگاه</p></div>
<div class="m2"><p>راست ناید ملک و مال و آب و جاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرچ داری یک یک از خود بازکن</p></div>
<div class="m2"><p>پس به خود در خلوتی آغاز کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون درونت جمع شد در بی‌خودی</p></div>
<div class="m2"><p>تو برون آیی ز نیکی و بدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون نماندت نیک و بد، عاشق شوی</p></div>
<div class="m2"><p>پس فنای عشق را لایق شوی</p></div></div>