---
title: >-
    حکایت مفلسی که عاشق پسر پادشاه شد و بدین گناه او را محکوم به مرگ کردند
---
# حکایت مفلسی که عاشق پسر پادشاه شد و بدین گناه او را محکوم به مرگ کردند

<div class="b" id="bn1"><div class="m1"><p>پادشاهی ماه وش، خورشید فر</p></div>
<div class="m2"><p>داشت چون یوسف یکی زیبا پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس به حسن او پسر هرگز نداشت</p></div>
<div class="m2"><p>هیچ خلق آن حشمت و آن عز نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک او بودند دلبندان همه</p></div>
<div class="m2"><p>بندهٔ رویش خداوندان همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به شب از پرده پیدا آمدی</p></div>
<div class="m2"><p>آفتابی نو به صحرا آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی او را وصف کردن روی نیست</p></div>
<div class="m2"><p>زانک مه از روی او یک موی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر رسن کردی از آن زلف دو تاه</p></div>
<div class="m2"><p>صد هزاران دل فرو رفتی به چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف عالم سوز آن شمع طراز</p></div>
<div class="m2"><p>کار کردی برهمه عالم دراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصف شست زلف آن یوسف جمال</p></div>
<div class="m2"><p>هیچ نتوان گفت در پنجاه سال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم چون نرگس اگر بر هم زدی</p></div>
<div class="m2"><p>آتش اندر جملهٔ عالم زدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خندهٔ او چون شکر کردی نثار</p></div>
<div class="m2"><p>صد هزاران گل شکفتی بی‌بهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دهانش خود نشد معلوم هیچ</p></div>
<div class="m2"><p>زانک نتوان گفت از معدوم هیچ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون ز زیر پرده بیرون آمدی</p></div>
<div class="m2"><p>هر سر مویش به صد خون آمدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فتنهٔ جان و جهان بود آن پسر</p></div>
<div class="m2"><p>هرچ گویم بیش از آن بود آن پسر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو برون راندی سوی میدان فرس</p></div>
<div class="m2"><p>برهنه بودیش تیغ از پیش و پس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرک سوی آن پسر کردی نگاه</p></div>
<div class="m2"><p>برگرفتندیش در ساعت ز راه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود درویشی گدایی بی‌خبر</p></div>
<div class="m2"><p>بی‌سر و بن شد ز عشق آن پسر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قسم ازو جز عجز و آشفتن نداشت</p></div>
<div class="m2"><p>جانش می‌شد زهرهٔ گفتن نداشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون بیافت آن درد را هم پشت او</p></div>
<div class="m2"><p>عشق و غم درجان و دل می‌ کشت او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز و شب در کوی او بنشسته بود</p></div>
<div class="m2"><p>چشم از خلق جهان بربسته بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ کس محرم نبودش در جهان</p></div>
<div class="m2"><p>همچنان می‌گشت با غم بی‌جنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روز و شب رویی چو زر، اشکی چو سیم</p></div>
<div class="m2"><p>منتظر بنشسته بودی دل دو نیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زنده زان بودی گدای نا صبور</p></div>
<div class="m2"><p>کان پسر گه گاه بگذشتی ز دور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شاه زاد، از دور چون پیدا شدی</p></div>
<div class="m2"><p>جملهٔ بازار پر غوغا شدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در جهان برخاستی صد رستخیز</p></div>
<div class="m2"><p>خلق یک سر آمدندی درگریز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چاوشان از پیش و از پس می‌شدند</p></div>
<div class="m2"><p>هر زمان در خون صد کس می‌شدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بانگ بردا برد می‌رفتی به ماه</p></div>
<div class="m2"><p>قرب یک فرسنگ بگرفتی سپاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون شنیدی بانگ چاوش آن گدا</p></div>
<div class="m2"><p>سر بگشتیش و در افتادی ز پا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غشیش آوردی و در خون ماندی</p></div>
<div class="m2"><p>وز وجود خویش بیرون ماندی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چشم بایستی در آن دم صد هزار</p></div>
<div class="m2"><p>تا برو بگریستی خون زار زار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گاه چون نیلی شدی آن ناتوان</p></div>
<div class="m2"><p>گاه خون از زیر او گشتی روان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گاه بفسردی ز آهش اشک او</p></div>
<div class="m2"><p>گاه اشکش سوختی از رشک او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیم کشته، نیم مرده، نیم جان</p></div>
<div class="m2"><p>وز تهی دستی نبودش نیم نان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این چنین کس را چنین افتاده پست</p></div>
<div class="m2"><p>آن چنان شه زاده چون آید به دست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نیم ذره سایه بود آن بی‌خبر</p></div>
<div class="m2"><p>خواست تا خورشید درگیرد ببر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>می‌شد آن شه زاده روزی با سپاه</p></div>
<div class="m2"><p>آن گدا یک نعره زد آن جایگاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زو برآمد نعره و بی‌خویش شد</p></div>
<div class="m2"><p>گفت جانم سوخت و عقل از پیش شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چند خواهم سوخت جان خویش ازین</p></div>
<div class="m2"><p>نیست صبر و طاقت من بیش ازین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این سخن می‌گفت آن سرگشته مرد</p></div>
<div class="m2"><p>هر زمان بر سنگ می‌زد سر ز درد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون بگفت این، گشت زایل هوش او</p></div>
<div class="m2"><p>پس روان شد خون ز چشم و گوش او</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چاوش شه زاده زو آگاه شد</p></div>
<div class="m2"><p>عزم غمزش کرد، پیش شاه شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفت بر شه‌زادهٔ تو شهریار</p></div>
<div class="m2"><p>عشق آوردست رندی بی‌قرار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاه از غیرت چنان مدهوش شد</p></div>
<div class="m2"><p>کز تف دل مغز او پر جوش شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفت برخیزید بردارش کشید</p></div>
<div class="m2"><p>پای بسته، سر نگوسارش کشید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در زمان رفتند خیل پادشا</p></div>
<div class="m2"><p>حلقه‌ای کردند گرد آن گدا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پس بسوی دار کردندش کشان</p></div>
<div class="m2"><p>بر سر او گشت خلقی خون فشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نه ز دردش هیچ کس آگاه بود</p></div>
<div class="m2"><p>نه کسش آنجا شفاعت خواه بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون به زیر دار آوردش و زیر</p></div>
<div class="m2"><p>ز آتش حسرت برآمد زو نفیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفت مهلم ده ز بهر کردگار</p></div>
<div class="m2"><p>تا کنم یک سجده باری زیر دار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مهل دادش آن وزیر خشم ناک</p></div>
<div class="m2"><p>تا نهاد او روی خود بر روی خاک</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پس میان سجده گفتا ای اله</p></div>
<div class="m2"><p>چون بخواهد کشت شاهم بی‌گناه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پیش از آن کز جان برآیم بی‌خبر</p></div>
<div class="m2"><p>روزیم گردان جمال آن پسر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا ببینم روی او یک بار نیز</p></div>
<div class="m2"><p>جان کنم بر روی او ایثار نیز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون ببینم روی آن شه زاد خوش</p></div>
<div class="m2"><p>صد هزار جان توانم داد خوش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پادشاها بنده حاجت خواه تست</p></div>
<div class="m2"><p>عاشقتست و کشتهٔ این راه تست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هستم از جان بندهٔ این در هنوز</p></div>
<div class="m2"><p>گر شدم عاشق، نیم کافر هنوز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون تو حاجت می‌بر آری صد هزار</p></div>
<div class="m2"><p>حاجت من کن روا کارم برآر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چون بخواست این حاجت آن مظلوم راه</p></div>
<div class="m2"><p>تیر او آمد مگر بر جایگاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چون شنید آن راز او پنهان و زیر</p></div>
<div class="m2"><p>درد کردش دل ز درد آن فقیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>رفت پیش پادشاه و می‌گریست</p></div>
<div class="m2"><p>حال آن دل داده برگفتش که چیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زاری او در مناجاتش بگفت</p></div>
<div class="m2"><p>در میان سجده حاجاتش بگفت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شاه را دردی ازو در دل فتاد</p></div>
<div class="m2"><p>خوش شد و بر عفو کردن دل نهاد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شاه حالی گفت آن شه‌زاده را</p></div>
<div class="m2"><p>سر مگردان آن ز پا افتاده را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>این زمان برخیز زیر دار شو</p></div>
<div class="m2"><p>پیش آن سرگشتهٔ خون‌خوار شو</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مستمند خویش را آواز ده</p></div>
<div class="m2"><p>بی‌دل تست او، دل او بازده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>لطف کن با او که قهر تو کشید</p></div>
<div class="m2"><p>نوش خور با او که زهر تو چشید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از رهش برگیر سوی گلشن آر</p></div>
<div class="m2"><p>چون بیایی، با خودش پیش من آر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>رفت آن شه زادهٔ یوسف جمال</p></div>
<div class="m2"><p>تا نشیند با گدایی در وصال</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>رفت آن خورشید روی آتشین</p></div>
<div class="m2"><p>تا شود با ذرهٔ خلوت نشین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>رفت آن دریای پر گوهر خوشی</p></div>
<div class="m2"><p>تا کند با قطره دست اندرکشی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از خوشی این جایگه بر سر زنید</p></div>
<div class="m2"><p>پای برکوبید، دستی برزنید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آخر آن شه‌زاده زیر دار شد</p></div>
<div class="m2"><p>چون قیامت فتنهٔ بیدار شد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آن گدا را در هلاک افتاده دید</p></div>
<div class="m2"><p>سرنگون بر روی خاک افتاده دید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خاک از خون دو چشمش گل شده</p></div>
<div class="m2"><p>عالمی پر حسرتش حاصل شده</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>محو گشته، گم شده، ناچیز هم</p></div>
<div class="m2"><p>زین بتر چه بود دگر، آن نیز هم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چون چنان دید آن به خون افتاده را</p></div>
<div class="m2"><p>آب در چشم آمد آن شه‌زاده را</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>خواست تا پنهان کند اشک از سپاه</p></div>
<div class="m2"><p>بر نمی‌آمد مگر با اشک شاه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اشک چون باران روان کرد آن زمان</p></div>
<div class="m2"><p>گشت حاصل صد جهان درد آن زمان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هرک او در عشق صادق آمدست</p></div>
<div class="m2"><p>بر سرش معشوق عاشق آمدست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گر به صدق عشق پیش آید ترا</p></div>
<div class="m2"><p>عاشقت معشوق خویش آید ترا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عاقبت شه‌زاده خورشید فش</p></div>
<div class="m2"><p>از سر لطف آن گدا را خواند خوش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>آن گدا آواز او نشنیده بود</p></div>
<div class="m2"><p>لیک بسیاری ز دورش دیده بود</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چون گدا برداشت روی از خاک راه</p></div>
<div class="m2"><p>در برابر دید روی پادشاه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>آتش سوزنده با دریای آب</p></div>
<div class="m2"><p>گرچه می‌سوزد، نیارد هیچ تاب</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بود آن درویش بی‌دل آتشی</p></div>
<div class="m2"><p>قربتش افتاد با دریا خوشی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>جان به لب آورد، گفت ای شهریار</p></div>
<div class="m2"><p>چون چنینم می‌توانی کشت زار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>حاجت این لشگر گر بز نبود</p></div>
<div class="m2"><p>این بگفت و گوییی هرگز نبود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نعره‌ای زد، جان ببخشید و بمرد</p></div>
<div class="m2"><p>همچو شمعی باز خندید و بمرد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چون وصال دلبرش معلوم گشت</p></div>
<div class="m2"><p>فانی مطلق شد و معدوم گشت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سالکان دانند در میدان درد</p></div>
<div class="m2"><p>تا فنای عشق با مردان چه کرد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ای وجودت با عدم آمیخته</p></div>
<div class="m2"><p>لذت تو با عدم آمیخته</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تا نیاری مدتی زیر و زبر</p></div>
<div class="m2"><p>کی توانی یافت ز آسایش خبر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دست بگشاده چو برقی جسته‌ای</p></div>
<div class="m2"><p>وز خلاشه پیش برقی بسته‌ای</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>این چه کارتست مردانه درآی</p></div>
<div class="m2"><p>عقل برهم سوز دیوانه درآی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گر نخواهی کرد تو این کیمیا</p></div>
<div class="m2"><p>یک نفس باری بنظاره بیا</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چند اندیشی چو من بی‌خویش شو</p></div>
<div class="m2"><p>یک نفس در خویش پیش اندیش شو</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تا دمی آخر به درویشی رسی</p></div>
<div class="m2"><p>در کمال ذوق بی‌خویشی رسی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>من که نه من مانده‌ام نه غیر من</p></div>
<div class="m2"><p>برتر است از عقل شر و خیر من</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گم شدم در خویشتن یک بارگی</p></div>
<div class="m2"><p>چارهٔ من نیست جز بیچارگی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>آفتاب فقر چون بر من بتافت</p></div>
<div class="m2"><p>هر دو عالم هم ز یک روزن بتافت</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>من چو دیدم پرتو آن آفتاب</p></div>
<div class="m2"><p>من بماندم باز شد آبی به آب</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>هرچ گاهی بردم و گه باختم</p></div>
<div class="m2"><p>جمله در آب سیاه انداختم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>محو گشتم، گم شدم، هیچم نماند</p></div>
<div class="m2"><p>سایه ماندم ذرهٔ پیچم نماند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>قطره بودم، گم شدم در بحر راز</p></div>
<div class="m2"><p>می‌نیابم این زمان آن قطره باز</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>گرچه گم گشتن نه کار هر کسیست</p></div>
<div class="m2"><p>در فنا گم گشتم و چون من بسیست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>کیست در عالم ز ماهی تا به ماه</p></div>
<div class="m2"><p>کو نخواهد گشت گم این جایگاه</p></div></div>