---
title: >-
    حکایت طوطی
---
# حکایت طوطی

<div class="b" id="bn1"><div class="m1"><p>طوطی آمد با دهان پر شکر</p></div>
<div class="m2"><p>در لباس فستقی با طوق زر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشه گشته باشه‌ای از فر او</p></div>
<div class="m2"><p>هر کجا سرسبزیی از پر او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سخن گفتن شکرریز آمده</p></div>
<div class="m2"><p>در شکر خوردن پگه خیز آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت هر سنگین دل و هر هیچکس</p></div>
<div class="m2"><p>چون منی را آهنین سازد قفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من در این زندان آهن مانده باز</p></div>
<div class="m2"><p>زآرزوی آب خضرم در گداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خضر مرغانم از آنم سبزپوش</p></div>
<div class="m2"><p>بوک دانم کردن آب خضر نوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نیارم در بر سیمرغ تاب</p></div>
<div class="m2"><p>بس بود از چشمهٔ خضرم یک آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر نهم در راه چون سوداییی</p></div>
<div class="m2"><p>می‌روم هر جای چون هر جاییی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نشان یابم ز آب زندگی</p></div>
<div class="m2"><p>سلطنت دستم دهد در بندگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هدهدش گفت ای ز دولت بی‌نشان</p></div>
<div class="m2"><p>مرد نبود هرک نبود جان فشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان ز بهر این به کار آید تو را</p></div>
<div class="m2"><p>تا دمی در خورد یار آید تو را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آب حیوان خواهی و جان‌دوستی</p></div>
<div class="m2"><p>رو که تو مغزی نداری پوستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان چه خواهی کرد، بر جانان فشان</p></div>
<div class="m2"><p>در ره جانان چو مردان جان فشان</p></div></div>