---
title: >-
    حکایت پادشاهی که سیب بر سر غلام خود می‌گذاشت و آن را نشانه می‌گرفت
---
# حکایت پادشاهی که سیب بر سر غلام خود می‌گذاشت و آن را نشانه می‌گرفت

<div class="b" id="bn1"><div class="m1"><p>پادشاهی بود بس عالی گهر</p></div>
<div class="m2"><p>گشت عاشق بر غلام سیم بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد چنان عاشق که بی‌آن بت دمی</p></div>
<div class="m2"><p>نه نشستی و نه آسودی دمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غلامانش به رتبت بیش داشت</p></div>
<div class="m2"><p>دایما در پیش چشم خویش داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه چون در قصر تیر انداختی</p></div>
<div class="m2"><p>آن غلام از بیم او بگداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانک از سیبی هدف کردی مدام</p></div>
<div class="m2"><p>پس نهادی سیب بر فرق غلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیب را بشکافتی حالی به تیر</p></div>
<div class="m2"><p>و آن غلام از بیم گشتی چون زریر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زو مگر پرسید مردی بی‌خبر</p></div>
<div class="m2"><p>کز چه شد گلگونهٔ رویت چو زر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه حرمت که پیش شه تو راست</p></div>
<div class="m2"><p>شرح ده کاین زرد رویت از چه خاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت بر سر می‌نهد سیبی مرا</p></div>
<div class="m2"><p>گر رسد از تیرش آسیبی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوید انگارم غلامی خود نبود</p></div>
<div class="m2"><p>در سپاهم ناتمامی خود نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور چنان باشد که آید تیر راست</p></div>
<div class="m2"><p>جمله گویندش ز بخت پادشاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من میان این دو غم در پیچ پیچ</p></div>
<div class="m2"><p>بر چه‌ام جان پر خطر، بر هیچ هیچ</p></div></div>