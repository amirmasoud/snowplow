---
title: >-
    حکایت درویشی که عاشق دختر پادشاه شد
---
# حکایت درویشی که عاشق دختر پادشاه شد

<div class="b" id="bn1"><div class="m1"><p>شهریاری دختری چون ماه داشت</p></div>
<div class="m2"><p>عالمی پر عاشق و گمراه داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه را بیداریی پیوست بود</p></div>
<div class="m2"><p>زانک چشم نیم خوابش مست بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارض از کافور و زلف از مشک داشت</p></div>
<div class="m2"><p>لعل سیراب از لبش لب خشک داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جمالش ذره‌ای پیدا شدی</p></div>
<div class="m2"><p>عقل از لایعقلی رسوا شدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شکر طعم لبش بشناختی</p></div>
<div class="m2"><p>از خجل بفسردی و بگداختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قضا می‌رفت درویشی اسیر</p></div>
<div class="m2"><p>چشم افتادش بر آن ماه منیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرده‌ای در دست داشت آن بی‌نوا</p></div>
<div class="m2"><p>نان آوان مانده بد بر نانوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم او چون بر رخ آن مه فتاد</p></div>
<div class="m2"><p>گرده از دستش شد و در ره فتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دختر از پیشش چو آتش برگذشت</p></div>
<div class="m2"><p>خوش درو خندید خوش خوش برگذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن گدا پس خندهٔ او چون بدید</p></div>
<div class="m2"><p>خویش را بر خاک غرق خون بدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیم نان داشت آن گدا و نیم جان</p></div>
<div class="m2"><p>زان دو نیمه پاک شد در یک زمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه قرارش بود شب نه روز هم</p></div>
<div class="m2"><p>دم نزد از گریه و از سوز هم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یاد کردی خندهٔ آن شهریار</p></div>
<div class="m2"><p>گریه افتادی برو چون ابر زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هفت سال القصه بس آشفته بود</p></div>
<div class="m2"><p>با سگان کوی دختر خفته بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خادمان دختر و خدمت گران</p></div>
<div class="m2"><p>جمله گشتند ای عجب واقف بر آن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عزم کردند آن جفا کاران به جمع</p></div>
<div class="m2"><p>تا ببرند آن گدا را سر چو شمع</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در نهان دختر گدا را خواند و گفت</p></div>
<div class="m2"><p>چون تویی را چون منی کی بود جفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قصد تو دارند، بگریز و برو</p></div>
<div class="m2"><p>بر درم منشین، برخیز و برو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن گدا گفتا که من آن روز دست</p></div>
<div class="m2"><p>شسته‌ام از جان که گشتم از تو مست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صد هزاران جان چون من بی‌قرار</p></div>
<div class="m2"><p>باد بر روی تو هر ساعت نثار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون مرا خواهند کشتن ناصواب</p></div>
<div class="m2"><p>یک سؤالم را به لطفی ده جواب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون مرا سر می‌بریدی رایگان</p></div>
<div class="m2"><p>ازچه خندیدی تو در من آن زمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت چون می‌دیدمت ای بی‌هنر</p></div>
<div class="m2"><p>بر تو می‌خندیدم آن ای بی‌خبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر سر و روی تو خندیدن رواست</p></div>
<div class="m2"><p>لیک در روی تو خندیدن خطاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این بگفت و رفت از پیشش چو دود</p></div>
<div class="m2"><p>هرچه بود اصلا همه آن هیچ بود</p></div></div>