---
title: >-
    حکایت مردی که در کوه چین سنگ شد
---
# حکایت مردی که در کوه چین سنگ شد

<div class="b" id="bn1"><div class="m1"><p>بود مردی سنگ شد در کوه چین</p></div>
<div class="m2"><p>اشک می‌بارد ز چشمش بر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر زمین چون اشک ریزد زار زار</p></div>
<div class="m2"><p>سنگ گردد اشک آن مرد آشکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از آن سنگی فتد در دست میغ</p></div>
<div class="m2"><p>تا قیامت زو نبارد جز دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست علم آن مرد پاک راست گوی</p></div>
<div class="m2"><p>گر به چین باید شدن او را بجوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانک علم از غصهٔ بی همتان</p></div>
<div class="m2"><p>سنگ شد، تا کی ز کافر نعمتان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله تاریک است این محنت سرای</p></div>
<div class="m2"><p>علم در وی چون جواهر ره نمای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره بر جانت درین تاریک جای</p></div>
<div class="m2"><p>جوهر علمست و علم جان فزای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو درین تاریکی بی پا و سر</p></div>
<div class="m2"><p>چون سکندر مانده‌ای بی‌راه بر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر تو برگیری ازین جوهر بسی</p></div>
<div class="m2"><p>خویش را یابی پشیمان‌تر کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور نباید جوهرت ای هیچ کس</p></div>
<div class="m2"><p>هم پشیمان‌تر تو خواهی بود بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بود ور نبود این جوهر ترا</p></div>
<div class="m2"><p>هر زمان یابم پشیمان‌تر ترا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این جهان و آن جهان در جان گمست</p></div>
<div class="m2"><p>تن ز جان و جان ز تن پنهان گمست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون برون رفتی ازین گم در گمی</p></div>
<div class="m2"><p>هست آنجا جای خاص آدمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر رسی زینجا بجای خاص باز</p></div>
<div class="m2"><p>پی بری در یک نفس صد گونه راز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور درین ره بازمانی وای تو</p></div>
<div class="m2"><p>گم شود در نوحه سر تا پای تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شب مخسب و روز در هم می‌مخور</p></div>
<div class="m2"><p>این طلب در تو پدید آید مگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می‌طلب تو تا طلب کم گرددت</p></div>
<div class="m2"><p>خورد روز و خواب شب کم گرددت</p></div></div>