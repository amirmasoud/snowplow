---
title: >-
    حکایت محمود و دیوانهٔ ویرانه‌نشین
---
# حکایت محمود و دیوانهٔ ویرانه‌نشین

<div class="b" id="bn1"><div class="m1"><p>شد مگر محمود در ویرانه‌ای</p></div>
<div class="m2"><p>دید آنجا بی‌دلی دیوانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر فرو برده به اندوهی که داشت</p></div>
<div class="m2"><p>پشت زیر بار آن کوهی که داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه را چون دید، گفتش دورباش</p></div>
<div class="m2"><p>ورنه بر جانت زنم صد دور باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو نه‌ای شاهی، که تو دون همتی</p></div>
<div class="m2"><p>در خدای خویش کافر نعمتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت محمودم، مرا کافر مگوی</p></div>
<div class="m2"><p>یک سخن با من بگو، دیگر مگوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت اگر می‌دانیی ای بی‌خبر</p></div>
<div class="m2"><p>کز که دور افتاده‌ای زیر و زبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستی خاکستر و خاکت تمام</p></div>
<div class="m2"><p>جمله آتش ریزیی بر سر مدام</p></div></div>