---
title: >-
    عقیدهٔ دیوانه‌ای درباره دو عالم
---
# عقیدهٔ دیوانه‌ای درباره دو عالم

<div class="b" id="bn1"><div class="m1"><p>کرد از دیوانه‌ای مردی سؤال</p></div>
<div class="m2"><p>کاین دو عالم چیست با چندین خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت کاین هر دو جهان بالا و پست</p></div>
<div class="m2"><p>قطرهٔ آب است نه نیست و نه ‌هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت از اول قطرهٔ آب آشکار</p></div>
<div class="m2"><p>قطرهٔ آب است با چندین نگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نگاری کان بود بر روی آب</p></div>
<div class="m2"><p>گر همه زآهن بود گردد خراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ چیزی نیست زآهن سخت‌تر</p></div>
<div class="m2"><p>هم بنا بر آب دارد در نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچ را بنیاد بر آبی بود</p></div>
<div class="m2"><p>گر همه آتش بود خوابی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس ندیده‌ست آب هرگز پایدار</p></div>
<div class="m2"><p>کی بود بی‌آب بنیاد استوار</p></div></div>