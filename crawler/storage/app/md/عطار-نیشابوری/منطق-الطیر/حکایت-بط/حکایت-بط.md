---
title: >-
    حکایت بط
---
# حکایت بط

<div class="b" id="bn1"><div class="m1"><p>بط به صد پاکی برون آمد ز آب</p></div>
<div class="m2"><p>در میان جمع با خیرالثیاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت در هر دو جهان ندهد خبر</p></div>
<div class="m2"><p>کس ز من یک پاک‌روتر پاک‌تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده‌ام هر لحظه غسلی بر صواب</p></div>
<div class="m2"><p>پس سجاده باز افکنده بر آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو من بر آب چون استد یکی</p></div>
<div class="m2"><p>نیست باقی در کراماتم شکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد مرغان منم با رای پاک</p></div>
<div class="m2"><p>دایمم هم جامه و هم جای پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نیابم در جهان بی‌آب سود</p></div>
<div class="m2"><p>زانک زاد و بود من در آن بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچ در دل عالمی غم داشتم</p></div>
<div class="m2"><p>شستم از دل کاب همدم داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب در جوی منست اینجا مدام</p></div>
<div class="m2"><p>من به خشکی چون توانم یافت کام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مرا با آب افتادست کار</p></div>
<div class="m2"><p>از میان آب چون گیرم کنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنده از آب است دایم هرچ هست</p></div>
<div class="m2"><p>این چنین از آب نتوان شست دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ره وادی کجا دانم برید</p></div>
<div class="m2"><p>زانک با سیمرغ نتوانم پرید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنک باشد قلهٔ آبش تمام</p></div>
<div class="m2"><p>کی تواند یافت از سیمرغ کام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هدهدش گفت ای به آبی خوش شده</p></div>
<div class="m2"><p>گرد جانت آب چون آتش شده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در میان آب خوش خوابت ببرد</p></div>
<div class="m2"><p>قطرهٔ آب آمد و آبت ببرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آب هست از بهر هر ناشسته روی</p></div>
<div class="m2"><p>گر تو بس ناشسته رویی آب جوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چند باشد همچو آب روشنت</p></div>
<div class="m2"><p>روی هر ناشسته رویی دیدنت</p></div></div>