---
title: >-
    درتعصب گوید
---
# درتعصب گوید

<div class="b" id="bn1"><div class="m1"><p>ای گرفتار تعصب مانده</p></div>
<div class="m2"><p>دایما در بغض و در حب مانده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو لاف از عقل و از لب می‌زنی</p></div>
<div class="m2"><p>پس چرا دم در تعصب می‌زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خلافت میل نیست ای بی‌خبر</p></div>
<div class="m2"><p>میل کی آید ز بوبکر و عمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میل اگر بودی در آن دو مقتدا</p></div>
<div class="m2"><p>هر دو کردندی پسر را پیشوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو گر بودند حق از حق وران</p></div>
<div class="m2"><p>منع واجب آمدی بر دیگران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منع را گر ناپدیدار آمدند</p></div>
<div class="m2"><p>ترک واجب را روادار آمدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نمی‌آمد کسی در منع یار</p></div>
<div class="m2"><p>جمله راتکذیب کن یا اختیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کنی تکذیب اصحاب رسول</p></div>
<div class="m2"><p>قول پیغامبر نکردستی قبول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت هر یاریم نجمی روشن است</p></div>
<div class="m2"><p>بهترین قرنها قرن منست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهترین خلق یاران من‌اند</p></div>
<div class="m2"><p>آفرین با دوست داران من‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهترین چون نزد تو باشد بتر</p></div>
<div class="m2"><p>کی توان گفتن ترا صاحب نظر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی روا داری که اصحاب رسول</p></div>
<div class="m2"><p>مرد ناحق را کنند از جان قبول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا نشانندش به جای مصطفا</p></div>
<div class="m2"><p>بر صحابه نیست این باطل روا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اختیار جمله شان گر نیست راست</p></div>
<div class="m2"><p>اختیار جمع قرآن پس خطاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بل که هرچ اصحاب پیغامبر کنند</p></div>
<div class="m2"><p>حق کنند و لایق حق ور کنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا کنی معزول یک تن را ز کار</p></div>
<div class="m2"><p>می‌کنی تکذیب سی و سه هزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنک کار او جز به حق یک دم نکرد</p></div>
<div class="m2"><p>تا به زانو بند اشتر، کم نکرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>او چو چندینی در آویزد به کار</p></div>
<div class="m2"><p>حق ز حق‌ور کی برد این ظن مدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میل در صدیق اگر جایز بدی</p></div>
<div class="m2"><p>در اقیلونی کجا هرگز بدی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در عمر گر میل بودی ذره‌ای</p></div>
<div class="m2"><p>کی پسر، کشتی به زخم دره‌ای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دایما صدیق مرد راه بود</p></div>
<div class="m2"><p>فارغ از کل لازم درگاه بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مال و دختر کرد بر سر جان نثار</p></div>
<div class="m2"><p>ظلم نکند این چنین کس، شرم دار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پاک از قشر روایت بود او</p></div>
<div class="m2"><p>زانک در معجز درایت بود او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنک بر منبر ادب دارد نگاه</p></div>
<div class="m2"><p>خواجه را ننشیند او بر جایگاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون ببیند این همه از پیش و پس</p></div>
<div class="m2"><p>ناحق او را کی تواند گفت کس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باز فاروقی که عدلش بود کار</p></div>
<div class="m2"><p>گاه می‌زد خشت و گه می‌کند خار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با در منه شهر را برخاستی</p></div>
<div class="m2"><p>می‌شدی در شهر وره می‌خواستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود هر روزی درین حبس هوس</p></div>
<div class="m2"><p>هفت لقمه نان طعام او و بس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرکه بودی با نمک بر خوان او</p></div>
<div class="m2"><p>نه ز بیت‌المال بودی نان او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ریگ بودی گر بخفتی بسترش</p></div>
<div class="m2"><p>دره بودی بالشی زیر سرش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برگرفتی همچو سقا مشک آب</p></div>
<div class="m2"><p>بیوه‌زن را آب بردی وقت خواب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شب برفتی دل ز خود برداشتی</p></div>
<div class="m2"><p>جملهٔ شب پاس لشگر داشتی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با حذیفه گفت ای صاحب نظر</p></div>
<div class="m2"><p>هیچ می‌بینی نفاقی در عمر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کو کسی کو عیب من در روی من</p></div>
<div class="m2"><p>میل نکند تحفه آرد سوی من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر خلافت بر خطا می‌داشت او</p></div>
<div class="m2"><p>هفده من دلقی چرا برداشت او</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون نه جامه دست دادش نه گلیم</p></div>
<div class="m2"><p>بر مرقع دوخت ده پاره ادیم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آنک زین سان شاهی خیلی کند</p></div>
<div class="m2"><p>نیست ممکن کو به کس میلی کند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آنک گاهی خشت و گاهی گل کشید</p></div>
<div class="m2"><p>این همه سختی نه بر باطل کشید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر خلافت از هوا می‌راندی</p></div>
<div class="m2"><p>خویش را در سلطنت بنشاندی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شهر هاء منکر از حسام او</p></div>
<div class="m2"><p>شد تهی از کفر در ایام او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر تعصب می‌کنی از بهر این</p></div>
<div class="m2"><p>نیست انصافت بمیر از قهر این</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>او نمرد از زهر و تو از قهر او</p></div>
<div class="m2"><p>چند میری گر نخوردی زهر او</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>می‌نگر ای جاهل ناحق شناس</p></div>
<div class="m2"><p>از خلافت خواجگی خود قیاس</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر تو گر این خواجگی آید به سر</p></div>
<div class="m2"><p>زین غمت صد آتش افتد در جگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر کسی ز ایشان خلافت بستدی</p></div>
<div class="m2"><p>عهدهٔ صد گونه آفت بستدی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نیست آسان تا که جان در تن بود</p></div>
<div class="m2"><p>عهدهٔ خلقی که در گردن بود</p></div></div>