---
title: >-
    حکایت کوف
---
# حکایت کوف

<div class="b" id="bn1"><div class="m1"><p>کوف آمد پیش چون دیوانه‌ای</p></div>
<div class="m2"><p>گفت من بگزیده‌ام ویرانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاجزی‌ام در خرابی زاده من</p></div>
<div class="m2"><p>در خرابی می‌روم بی‌باده من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه معموری بسی خوش یافتم</p></div>
<div class="m2"><p>هم مخالف هم مشوش یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرک در جمعیتی خواهد نشست</p></div>
<div class="m2"><p>در خرابی بایدش رفتن چو مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خرابی جای می‌سازم به رنج</p></div>
<div class="m2"><p>زانک باشد در خرابی جای گنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق گنجم در خرابی ره نمود</p></div>
<div class="m2"><p>سوی گنجم جز خرابی ره نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور بردم از همه کس رنج خویش</p></div>
<div class="m2"><p>بوک یابم بی طلسمی گنج خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر فرو رفتی به گنجی پای من</p></div>
<div class="m2"><p>باز رستی این دل خودرای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق بر سیمرغ جز افسانه نیست</p></div>
<div class="m2"><p>زانک عشقش کار هر مردانه نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من نیم در عشق او مردانه‌ای</p></div>
<div class="m2"><p>عشق گنجم باید و ویرانه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هدهدش گفت ای ز عشق گنج مست</p></div>
<div class="m2"><p>من گرفتم کامدت گنجی به دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر سر آن گنج خود را مرده گیر</p></div>
<div class="m2"><p>عمر رفته ره به سر نابرده گیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق گنج و عشق زر از کافریست</p></div>
<div class="m2"><p>هرک از زر بت کند او آزریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زر پرستیدن بود از کافری</p></div>
<div class="m2"><p>نیستی آخر ز قوم سامری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر دلی کز عشق زر گیرد خلل</p></div>
<div class="m2"><p>در قیامت صورتش گردد بدل</p></div></div>