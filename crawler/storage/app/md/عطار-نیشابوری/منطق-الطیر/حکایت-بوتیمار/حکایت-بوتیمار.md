---
title: >-
    حکایت بوتیمار
---
# حکایت بوتیمار

<div class="b" id="bn1"><div class="m1"><p>پس درآمد زود بوتیمار پیش</p></div>
<div class="m2"><p>گفت ای مرغان من و تیمار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لب دریاست خوش‌تر جای من</p></div>
<div class="m2"><p>نشنود هرگز کسی آوای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کم آزاری من هرگز دمی</p></div>
<div class="m2"><p>کس نیازارد ز من در عالمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر لب دریا نشینم دردمند</p></div>
<div class="m2"><p>دایما اندوهگین و مستمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زآرزوی آب دل پر خون کنم</p></div>
<div class="m2"><p>چون دریغ آید، نجوشم چون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نیم من اهل دریا، ای عجب</p></div>
<div class="m2"><p>بر لب دریا بمیرم خشک لب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه دریا می‌زند صد گونه جوش</p></div>
<div class="m2"><p>من نیارم کرد از او یک قطره نوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ز دریا کم شود یک قطره آب</p></div>
<div class="m2"><p>زآتش غیرت دلم گردد کباب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون منی را عشق دریا بس بود</p></div>
<div class="m2"><p>در سرم این شیوه سودا بس بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز غم دریا نخواهم این زمان</p></div>
<div class="m2"><p>تاب سیمرغم نباشد الامان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنک او را قطرهٔ آبست اصل</p></div>
<div class="m2"><p>کی تواند یافت از سیمرغ وصل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هدهدش گفت ای ز دریا بی‌خبر</p></div>
<div class="m2"><p>هست دریا پر نهنگ و جانور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاه تلخ است آب او را گاه شور</p></div>
<div class="m2"><p>گاه آرام است او را گاه زور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منقلب چیز است و ناپاینده هم</p></div>
<div class="m2"><p>گه شونده گاه بازآینده هم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس بزرگان را که کشتی کرد خرد</p></div>
<div class="m2"><p>بس که در گرداب او افتاد و مرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرک چون غواص ره دارد در او</p></div>
<div class="m2"><p>از غم جان دم نگه دارد در او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور زند در قعر دریا دم کسی</p></div>
<div class="m2"><p>مرده از بن با سر افتد چون خسی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از چنین کس کاو وفاداری نداشت</p></div>
<div class="m2"><p>هیچ‌کس اومید دلداری نداشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر تو از دریا نیایی با کنار</p></div>
<div class="m2"><p>غرقه گرداند تو را پایان کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می‌زند او خود ز شوق دوست جوش</p></div>
<div class="m2"><p>گاه در موج است و گاهی در خروش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>او چو خود را می‌نیابد کام دل</p></div>
<div class="m2"><p>تو نیابی هم از او آرام دل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هست دریا چشمه‌ای ز کوی او</p></div>
<div class="m2"><p>تو چرا قانع شدی بی روی او</p></div></div>