---
title: >-
    گفتگوی مرد دیده‌ور با دریا
---
# گفتگوی مرد دیده‌ور با دریا

<div class="b" id="bn1"><div class="m1"><p>دیده‌ور مردی به دریا شد فرود</p></div>
<div class="m2"><p>گفت ای دریا چرا داری کبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامهٔ ماتم چرا پوشیده‌ای</p></div>
<div class="m2"><p>نیست هیچ آتش، چرا جوشیده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد دریا آن نکو دل را جواب</p></div>
<div class="m2"><p>کز فراق دوست دارم اضطراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ز نامردی نیم من مرد او</p></div>
<div class="m2"><p>جامه نیلی کرده‌ام از درد او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشک لب بنشسته‌ام مدهوش من</p></div>
<div class="m2"><p>زآتش عشق آب من شد جوش زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بیابم قطره‌ای از کوثرش</p></div>
<div class="m2"><p>زندهٔ جاوید گردم بر درش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورنه چون من صد هزاران خشک لب</p></div>
<div class="m2"><p>می‌بمیرد در ره او روز و شب</p></div></div>