---
title: >-
    حکایت خواجه‌ای که عاشق کودکی فقاع فروش شد
---
# حکایت خواجه‌ای که عاشق کودکی فقاع فروش شد

<div class="b" id="bn1"><div class="m1"><p>خواجه‌ای از خان و مان آواره شد</p></div>
<div class="m2"><p>وز فقاعی کودکی بی‌چاره شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد ز فرط عشق سودایی ازو</p></div>
<div class="m2"><p>گشت سر غوغای رسوایی ازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچ او را بود اسباب و ضیاع</p></div>
<div class="m2"><p>می‌فروخت و می‌خرید از وی فقاع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نماندش هیچ، بس درویش شد</p></div>
<div class="m2"><p>عشق آن بی‌دل یکی صد بیش شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه می‌دادند نان او را تمام</p></div>
<div class="m2"><p>گرسنه بودی و سیر از جان مدام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانک چندانی که نانش می‌رسید</p></div>
<div class="m2"><p>جمله می‌برد و فقاعی می‌خرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایما بنشسته بودی گرسنه</p></div>
<div class="m2"><p>تا خرد یک دم فقاعی صد تنه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایلی گفتش که ای آشفته کار</p></div>
<div class="m2"><p>عشق چه بود سر این کن آشکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت آن باشد که صد عالم متاع</p></div>
<div class="m2"><p>جمله بفروشی برای یک فقاع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا چنین کاری نیفتد مرد را</p></div>
<div class="m2"><p>او چه داند عشق را و درد را</p></div></div>