---
title: >-
    حکایت عربی که در عجم افتاد و سر گذشت او با قلندران
---
# حکایت عربی که در عجم افتاد و سر گذشت او با قلندران

<div class="b" id="bn1"><div class="m1"><p>در عجم افتاد خلقی از عرب</p></div>
<div class="m2"><p>ماند از رسم عجم او در عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظاره می‌گذشت آن بی‌خبر</p></div>
<div class="m2"><p>بر قلندر راه افتادش مگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید مشتی شنگ را، نه سر نه تن</p></div>
<div class="m2"><p>هر دو عالم باخته بی یک سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله کم زن مهره دزد پاک بر</p></div>
<div class="m2"><p>در پلیدی هریک از هم پاک تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر یکی را کردهٔ دزدی به دست</p></div>
<div class="m2"><p>هیچ دردی ناچشیده جمله مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بدید آن قوم را میلش فتاد</p></div>
<div class="m2"><p>عقل و جان بر شارع سیلش فتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون قلندریان چنانش یافتند</p></div>
<div class="m2"><p>آب برده عقل و جانش یافتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله گفتندش درآ ای هیچ کس</p></div>
<div class="m2"><p>او درون شد بیش و کم این بود بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد رندی مست از یک دردیش</p></div>
<div class="m2"><p>محو شد از خویش و گم شد مردیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مال و ملک و سیم و زر بودش بسی</p></div>
<div class="m2"><p>برد ازو در یک ندب حالی کسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رندی آمد دردی افزونش داد</p></div>
<div class="m2"><p>وز قلندر عور سر بیرونش داد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرد می‌شد همچنان تا با عرب</p></div>
<div class="m2"><p>عور و مفلس، تشنه جان و خشک لب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اهل او گفتند بس آشفته‌ای</p></div>
<div class="m2"><p>کو زر و سیمت، کجا تو خفته‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیم و زر شد، آمد آشفتن ترا</p></div>
<div class="m2"><p>شوم بود این در عجم رفتن ترا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دزد راهت زد، کجا شد مال تو</p></div>
<div class="m2"><p>شرح ده تا من بدانم حال تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت می‌رفتم خرامان در رهی</p></div>
<div class="m2"><p>اوفتاده بر قلندر ناگهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچ دیگر می‌ندانم نیز من</p></div>
<div class="m2"><p>سیم و زر رفت وشدم ناچیز من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت وصف این قلندر کن مرا</p></div>
<div class="m2"><p>گفت وصف اینست و بس قال اندرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرد اعرابی فنایی مانده بود</p></div>
<div class="m2"><p>زان همه قال اندرایی مانده بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پای درنه یا سر خود گیر تو</p></div>
<div class="m2"><p>جان ببر یا نه به جان بپذیر تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر تو بپذیری به جان اسرار عشق</p></div>
<div class="m2"><p>جان فشانان سرکنی در کار عشق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان فشانی و بمانی برهنه</p></div>
<div class="m2"><p>ماندت قال اندرایی دربنه</p></div></div>