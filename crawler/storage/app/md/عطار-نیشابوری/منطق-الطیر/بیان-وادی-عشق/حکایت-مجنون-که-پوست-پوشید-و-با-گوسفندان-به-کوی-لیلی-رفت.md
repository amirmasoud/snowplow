---
title: >-
    حکایت مجنون که پوست پوشید و با گوسفندان به کوی لیلی رفت
---
# حکایت مجنون که پوست پوشید و با گوسفندان به کوی لیلی رفت

<div class="b" id="bn1"><div class="m1"><p>اهل لیلی نیز مجنون را دمی</p></div>
<div class="m2"><p>در قبیله ره ندادندی همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشت چوپانی در آن صحرا نشست</p></div>
<div class="m2"><p>پوستی بستد ازو مجنون مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرنگون شد، پوست اندر سرفکند</p></div>
<div class="m2"><p>خویشتن را کرد همچون گوسفند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن شبان را گفت بهر کردگار</p></div>
<div class="m2"><p>در میان گوسفندانم گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی لیلی ران رمه، من در میان</p></div>
<div class="m2"><p>تا بیابم بوی لیلی یک زمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نهان از دوست، زیر پوست من</p></div>
<div class="m2"><p>بهره گیرم ساعتی از دوست من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ترا یک دم چنین دردیستی</p></div>
<div class="m2"><p>در بن هر موی تو مردیستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دریغا درد مردانت نبود</p></div>
<div class="m2"><p>روزی مردان میدانت نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت مجنون چو زیر پوست شد</p></div>
<div class="m2"><p>در رمه پنهان به کوی دوست شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش خوشی برخاست اول جوش ازو</p></div>
<div class="m2"><p>پس به آخر گشت زایل هوش ازو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون درآمد عشق و آب از سرگذشت</p></div>
<div class="m2"><p>برگرفتش آن شبان بردش به دشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آب زد بر روی آن مست خراب</p></div>
<div class="m2"><p>تا دمی بنشست آن آتش ز آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعد از آن، روزی مگر مجنون مست</p></div>
<div class="m2"><p>کرد با قومی به صحرا درنشست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک تن از قومش به مجنون گفت باز</p></div>
<div class="m2"><p>سر برهنه مانده‌ای ای سرفراز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جامه‌ای کان دوست‌تر داری و بس</p></div>
<div class="m2"><p>گر بگویی من بیارم این نفس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت هرجامه سزای دوست نیست</p></div>
<div class="m2"><p>هیچ جامه بهترم از پوست نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پوستی خواهم از آن گوسفند</p></div>
<div class="m2"><p>چشم بد را نیز می‌سوزم سپند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اطلس و اکسون مجنون پوستست</p></div>
<div class="m2"><p>پوست خواهد هرک لیلی دوستست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برده‌ام در پوست بوی دوست من</p></div>
<div class="m2"><p>کی ستانم جامه‌ای جز پوست من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل خبر از پوست یافت از دوستی</p></div>
<div class="m2"><p>چون ندارم مغز باری پوستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عشق باید کز خرد بستاندت</p></div>
<div class="m2"><p>پس صفات تو بدل گرداندت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کمترین چیزیت در محو صفات</p></div>
<div class="m2"><p>بخشش جانست و ترک ترهات</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پای درنه گر سرافرازی چنین</p></div>
<div class="m2"><p>زانک بازی نیست جان بازی چنین</p></div></div>