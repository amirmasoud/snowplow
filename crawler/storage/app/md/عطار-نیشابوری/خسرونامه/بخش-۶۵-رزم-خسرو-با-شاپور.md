---
title: >-
    بخش ۶۵ - رزم خسرو با شاپور
---
# بخش ۶۵ - رزم خسرو با شاپور

<div class="b" id="bn1"><div class="m1"><p>بآخر کار حرب آغاز کرد او</p></div>
<div class="m2"><p>علم را دامن از هم باز کرد او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاهش خیمه بر هامون کشیدند</p></div>
<div class="m2"><p>چو لاله تیغها بر خون کشیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدشت و کوه در چندان سپه بود</p></div>
<div class="m2"><p>که زان، روی همه عالم سیه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو صور صبح در دنیا دمیدند</p></div>
<div class="m2"><p>ز بستر خفتگان در میرمیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو صبح آمد، خروس صبحگاهی</p></div>
<div class="m2"><p>بفریاد اندر آمد از پگاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مغرب حلقهٔ مه کرددر گوش</p></div>
<div class="m2"><p>ز مشرق چشمهٔ خورشید زد جوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بر فرق سپهر سر بریده</p></div>
<div class="m2"><p>نهادند آن کلاه زر کشیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پدید آمد خروش از هر دو لشکر</p></div>
<div class="m2"><p>رسید از هردو لشکر تا دو پیکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس لشکر، نیفتادی ز افلاک</p></div>
<div class="m2"><p>فروغ ذرهٔ خورشید بر خاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گفتی از جهان نام زمین شد</p></div>
<div class="m2"><p>زمین را پشت، کوه آتشین شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو گفتی گرد گردونیست دیگر</p></div>
<div class="m2"><p>سر تیغ و سنان دروی چو اختر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه دشت از درفشیدن چنان بود</p></div>
<div class="m2"><p>که گفتی آسمان آتش فشان بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فروغ خود و عکس تیغ و جوشن</p></div>
<div class="m2"><p>ز مشرق تا بمغرب کرده روشن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شدند آن شیرمردان مغز پولاد</p></div>
<div class="m2"><p>چنانک آهن ازیشان تن فرو داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرافرازان چو کوه آهنین تن</p></div>
<div class="m2"><p>بآهن کوه آهن بر زمین زن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بسیاری که تیر از شست برجست</p></div>
<div class="m2"><p>زهر دو سوی ره بر تیر دربست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هوا گفتی ز پیکان ژاله بارست</p></div>
<div class="m2"><p>زمین گفتی ز بس خون لاله زارست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قیامت نقد و صور و کوس غرّان</p></div>
<div class="m2"><p>خدنگ تیر همچون نامه پرّان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه روی فلک از مرغ ناوک</p></div>
<div class="m2"><p>سراسر گشته چون دامی مشبّک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زره چون میغ، وز شست سواران</p></div>
<div class="m2"><p>بسوی میغ میبارید باران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز عکس تیغ چرخ هفت پاره</p></div>
<div class="m2"><p>نهان شد روز روشن چون ستاره</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان بارید بر گردنکشان تیغ</p></div>
<div class="m2"><p>که هنگام بهاران ژاله از میغ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز جوش و نعره و فریاد وآواز</p></div>
<div class="m2"><p>صدا میآمد از هفت آسمان باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز بانگ کوس، وز زخم چکاچاک</p></div>
<div class="m2"><p>طنین افتاد در نه طاس افلاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان شد زخم کوس و نعرهٔ جوش</p></div>
<div class="m2"><p>که گردون پنبه محکم کرد در گوش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو بانگ کوس در دشت اوفتادی</p></div>
<div class="m2"><p>زمین چون چرخ در گشت اوفتادی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمین از خون گرفته سهمناکی</p></div>
<div class="m2"><p>شده برج فلک ازگرد خاکی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غبار خاک زیر پای باره</p></div>
<div class="m2"><p>شده چون سرمه درچشم ستاره</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو هر تیغی میان بحرخون بود</p></div>
<div class="m2"><p>ز بحر خون میان تیغ چون بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه روی زمین دریای خون شد</p></div>
<div class="m2"><p>فلک بروی چو طشتی سرنگون شد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو بحر خون ز سر حدّ جهان شد</p></div>
<div class="m2"><p>فلک چون کشتیی برخون روان شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو موج خون زسردرمیگذشتی</p></div>
<div class="m2"><p>بدان دریا فرو کردند طشتی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بخشکی بر اجل کشتی روان دید</p></div>
<div class="m2"><p>که دریا پرنهنگ جان ستان دید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دران دریا اجل را کی عمل بود</p></div>
<div class="m2"><p>که هریک مرد، میر صداجل بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپه یکباره رویارو فتادند</p></div>
<div class="m2"><p>بخون یکدگر بازو گشادند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شدند از گرد سپه خورشید گمراه</p></div>
<div class="m2"><p>سیه شد همچو خال دلبران، ماه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زمین را یک طبق از گرد برخاست</p></div>
<div class="m2"><p>فلک را یک طبق از گرد شد راست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهان از گردره پر شد سراسر</p></div>
<div class="m2"><p>زمین با آسمان آمد برابر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نمیدیدند لشکر یکدگر را</p></div>
<div class="m2"><p>بیفگندند این تیغ آن سپر را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز بسیاری که گرد وخاک برخاست</p></div>
<div class="m2"><p>بیک ره از جهان فریاد برخاست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو شد روی زمین درزیر خون بر</p></div>
<div class="m2"><p>بسوی پشت ماهی برد خون سر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فرو شد تا بماهی خون لشکر</p></div>
<div class="m2"><p>برآمد تا بماه اللّه اکبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی خونریز را بیرون همی تاخت</p></div>
<div class="m2"><p>یکی را سوی میدان خون همی تاخت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه صحرا چه آزاد و چه بنده</p></div>
<div class="m2"><p>تن بی سر سر بی تن فگنده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شه خسرو بسان کوه پاره</p></div>
<div class="m2"><p>بتیغ خون فشان میکند خاره</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدستش خنجر زهر آب داده</p></div>
<div class="m2"><p>بفتراکش کمند تاب داده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زرمحش خسروان را خون چو جویی</p></div>
<div class="m2"><p>ز تیغش سرکشان را سر چو گویی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بآخر خسرو صد پیل در پیش</p></div>
<div class="m2"><p>بیک ره بانگ زد بر لشکر خویش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو پیل و چون سپه را جمله کرد او</p></div>
<div class="m2"><p>چو کوهی سوی کوهی حمله برد او</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سپاه خصم را برکند ازجای</p></div>
<div class="m2"><p>درامد لشکر سرگشته از پای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هزاهز در میان لشکر افتاد</p></div>
<div class="m2"><p>تو گفتی آتشی در کشور افتاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه گویم کان سپه چون جنگ کردند</p></div>
<div class="m2"><p>که دشت از کشته برخود تنگ کردند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سر مرد مبارز جمله صفدر</p></div>
<div class="m2"><p>جدا هریک سر مردی بکف در</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بآخر از قضای بد شبانگاه</p></div>
<div class="m2"><p>شکست افتاد بر شاپور ناگاه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نماند آرام آن خیل و حشم را</p></div>
<div class="m2"><p>نگونساری پدید آمد علم را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>علم را بود در سر باد پندار</p></div>
<div class="m2"><p>برون شد از سرش چون شد نگونسار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گریزان شد شه شاپور سرمست</p></div>
<div class="m2"><p>بشهر آمد نهان دروازه دربست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه شب بهر رفتن کار میکرد</p></div>
<div class="m2"><p>ز سیم و زر شتر را بارمیکرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گل تر را شبانگه با سپاهی</p></div>
<div class="m2"><p>بترمد برد از دزدیده راهی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو این میدان میناگون نگین یافت</p></div>
<div class="m2"><p>عروس هفت طارم بر زمین تافت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز تاب روی او روی زمانه</p></div>
<div class="m2"><p>چو آتش میزد از هر سو زبانه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو روشن شد جهان تیره بوده</p></div>
<div class="m2"><p>فرو ماندند خلق خیره بوده</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>برون رفتند چون صاحب گناهان</p></div>
<div class="m2"><p>ز شاه پاکدل زنهارّ خواهان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>که ما را بر زمین بودن زمان ده</p></div>
<div class="m2"><p>بجان، خلق جهانی را امان ده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بجان بندد جهان پیشت میان را</p></div>
<div class="m2"><p>اگر جانی دهی خلق جهان را</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز خلق هیچکس کس کینه نگرفت</p></div>
<div class="m2"><p>غضنفر صید لاغر سینه نگرفت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شه ایشان را بنیکویی کسی کرد</p></div>
<div class="m2"><p>بجای هر یکی شفقت بسی کرد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دو هفته بود وزانجام صبحگاهی</p></div>
<div class="m2"><p>روان شد سوی ترمذ با سپاهی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سپاهی کش عدد ازحد برون بود</p></div>
<div class="m2"><p>ز ریگ و برگ و کوکبها فزون بود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بآخر چون سوی ترمد رسیدند</p></div>
<div class="m2"><p>بگرد قلعهٔ اوصف کشیدند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چنان آن خندق او بود پر آب</p></div>
<div class="m2"><p>که ماهی بر زمین میکرد شیناب</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چنان برجش بمه پیوسته بودی</p></div>
<div class="m2"><p>که مه را در شدن ره بسته بودی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مگر ماه فلک از برج او تافت</p></div>
<div class="m2"><p>که اوج خویشتن در برج او یافت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>فراز و شیبش از مه تا بماهی</p></div>
<div class="m2"><p>چه میگویم کجا بودش سباهی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نه پل بود ونه بر آبش گذر بود</p></div>
<div class="m2"><p>ز سر تا پای آن را پا و سر بود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بآخر چون علم زد شمع انجم</p></div>
<div class="m2"><p>بگردون شد خروش از جمع مردم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سپه سوی حصار آهنگ کردند</p></div>
<div class="m2"><p>بتیر و سنگ لختی جنگ کردند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کسی را کز دو لشکر این هوس خاست</p></div>
<div class="m2"><p>نشد ازهیچ سویی کار کس راست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بآخر هم بدین کردار یک ماه</p></div>
<div class="m2"><p>بماند آن لشکر درمانده در راه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>شبی فرّخ بر خسرو درون شد</p></div>
<div class="m2"><p>مگر آن شب بتزویر و فسون شد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بخسرو گفت این را نیست تدبیر</p></div>
<div class="m2"><p>مگر آنرا بدست آرم بتزویر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>که گر صد سال زیر آن نشینم</p></div>
<div class="m2"><p>یقین دانم که روی آن نبینم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>فتاد اندیشهیی در راهم اکنون</p></div>
<div class="m2"><p>بگویم تا چه گوید شاهم اکنون</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بیاید هر شبی مردی توانا</p></div>
<div class="m2"><p>ز خندق آب کش گردد ببالا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بچندان برکشد ازخندق او آب</p></div>
<div class="m2"><p>که خندق زو بخواهد شد فرو آب</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>مرا عزمیست تا یکشب بزورق</p></div>
<div class="m2"><p>شوم آهسته تا آنسوی خندق</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو مرد آن دلو صد من را درآرد</p></div>
<div class="m2"><p>نشینم من درو تا بر سر آرد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو رفتم، گر دهد اقبال یاری</p></div>
<div class="m2"><p>بریزم در زمین خونش بخواری</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>وزان پس زورقی صدراست کن تو</p></div>
<div class="m2"><p>نشان آن ز من درخواست کن تو</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که تا چون بازیابی آن نشانی</p></div>
<div class="m2"><p>تنی صد را بزورق در نشانی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یکایک را ببالا برکشم من</p></div>
<div class="m2"><p>که گر پیلست تنها برکشم من</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو بر بالا رسد مردی صد، آنگاه</p></div>
<div class="m2"><p>در آن قلعه بگشاییم بر شاه</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>پل آن قلعه را بر آب بندیم</p></div>
<div class="m2"><p>بدولت دشمنان را خواب بندیم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>جهان گردد بکام شیر مردان</p></div>
<div class="m2"><p>اگر یاری دهد این چرخ گردان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چنان شه را خوش آمد گفتهٔ او</p></div>
<div class="m2"><p>که شد یکبارگی آشفتهٔ او</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>فراوان آفرینش کرد شهزاد</p></div>
<div class="m2"><p>که پیش بندگانت بنده شه باد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بغایت رای و تدبیری صوابست</p></div>
<div class="m2"><p>دلت صافی و رایت آفتابست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نکو افتاد این اندیشه مندی</p></div>
<div class="m2"><p>کنون برخیز تا زورق ببندی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بآخر چون نکو شد کار زورق</p></div>
<div class="m2"><p>دگر شب رفت فرخ سوی خندق</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>شبی بود از سیاهی همچو روزی</p></div>
<div class="m2"><p>که دور افتد دلی از دلفروزی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ز مشرق تا بمغرب تیره گشته</p></div>
<div class="m2"><p>ز ظلمت چشم انجم خیره گشته</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بزورق برنشست آن مرد مکّار</p></div>
<div class="m2"><p>روان شد همچنان تا زیر دیوار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چو مرد آن دلو از بالا درانداخت</p></div>
<div class="m2"><p>سپه گر خویش را تنها درانداخت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بزودی مرد بر بالا کشیدش</p></div>
<div class="m2"><p>که تا فرّخ جگر گه بر دریدش</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>شبی تاریک بود و مرد غافل</p></div>
<div class="m2"><p>ز دست خصم زخمی خورد بر دل</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نشان آن بود کان دلو سبک رو</p></div>
<div class="m2"><p>زند بر آب ده ره نزد خسرو</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو فرّخ دلو را ده ره چنان کرد</p></div>
<div class="m2"><p>بزودی شاه زورقها روان کرد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>فگند القصّه فرّخ آن رسن را</p></div>
<div class="m2"><p>ببالا برکشید او شصت تن را</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>دگر یاران تنی صد برکشیدند</p></div>
<div class="m2"><p>بیک ره ازمیان خنجر کشیدند</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>از آنجا تا پس دروازه رفتند</p></div>
<div class="m2"><p>نهان بی بانگ و بی آوازه رفتند</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>پس دروازه ده تن خفته بودند</p></div>
<div class="m2"><p>ندانم تا شهادت گفته بودند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بزاری هر ده آنجا کشته گشتند</p></div>
<div class="m2"><p>میان خون دل آغشته گشتند</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>پس آنگه در نهانی در گشادند</p></div>
<div class="m2"><p>بروی آب خندق پل نهادند</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چو بنهادند پل، لشکر درآمد</p></div>
<div class="m2"><p>خورشی از سپه یکسر برآمد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>شه شاپور تا شد آگه از کار</p></div>
<div class="m2"><p>فرو شد لشکر و لشکر گه ازکار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نه چندان شور آن شب در جهان بود</p></div>
<div class="m2"><p>که در روز قیامت بیش ازان بود</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>شبی مانند روز رستخیزی</p></div>
<div class="m2"><p>فتاده هر گروهی در گریزی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>خروش آن سپه بر ماه میشد</p></div>
<div class="m2"><p>کسی کان میشنود از راه میشد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>سپاه هرمز آن شب خون چنان ریخت</p></div>
<div class="m2"><p>که باران بهاری ز اسمان ریخت</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو پل بستند کز پل خون نمیشد</p></div>
<div class="m2"><p>چرا آن خون بپل بیرون نمیشد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو صبح خوش نفس خوش خوش نفس زد</p></div>
<div class="m2"><p>جرس جنبان شب لختی جرس زد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>هوا از صبح رنگ آمیز شد سرد</p></div>
<div class="m2"><p>زمین از زردهٔ خورشید شد زرد</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>شه شاپور با فیروز نسناس</p></div>
<div class="m2"><p>درامد پیش شه با تیغ و کرباس</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>زمین را بوسه زد زاری بسی کرد</p></div>
<div class="m2"><p>که چون شه کُشت زین لشکر بسی مرد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>مرا گر هم کشد فرمانروا اوست</p></div>
<div class="m2"><p>وگر گویم که بخشد پادشااوست</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>مرا کزره ببرد ابلیس مکّار</p></div>
<div class="m2"><p>که من برخویشتن گشتم ستمگار</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>اگر عفوم کند لطفی عظیمست</p></div>
<div class="m2"><p>که دل درمعرض امّید و بیمست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>میان خاک، خون من که ریزد</p></div>
<div class="m2"><p>دو من خاکم، ز خون من چه خیزد</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>خوش آمد شاه راگفتار شاپور</p></div>
<div class="m2"><p>فرستادش بشاهی با نشابور</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>وزان پس پیش فرّخ رفت فیروز</p></div>
<div class="m2"><p>رخی پر اشک خونین سنیه پر سوز</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>میان خاک ره بر سر بگردید</p></div>
<div class="m2"><p>ز چشمش قلزم گوهر بگردید</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بفرخ گفت بد کردم بسی من</p></div>
<div class="m2"><p>ولی با خویشتن، نه با کسی من</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>در آخر گرچه بد کردار بودم</p></div>
<div class="m2"><p>ولی با تو در اوّل یار بودم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>اگر من ترک کردم حقّ یاری</p></div>
<div class="m2"><p>بجای آور تو با من حق گزاری</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بدی را چشم میدارم نکویی</p></div>
<div class="m2"><p>که شه عفوم کند گر تو بگویی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>ز زاری کردنش چون جوی خون رفت</p></div>
<div class="m2"><p>بیاری کردنش فرّخ برون رفت</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>گرفتش دست و پیش شاهش آورد</p></div>
<div class="m2"><p>دو لب خشک و دو رخ چون کاهش آورد</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بخسرو گفت: این درخون بگشته</p></div>
<div class="m2"><p>بجان آمد مکن یاد از گذشته</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>اگرچه جرم صد انبار دارد</p></div>
<div class="m2"><p>ولی بر شاه حق بسیار دارد</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>کرم کن زانکه شاهان زمانه</p></div>
<div class="m2"><p>کرم کردند با من جاودانه</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>شه از بهر دل فرخ چنان کرد</p></div>
<div class="m2"><p>که هرگز بر نکوکاری زیان کرد؟</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چو تو نه خار این راهی نه گلزار</p></div>
<div class="m2"><p>میازار از کس و کس را میازار</p></div></div>