---
title: >-
    بخش ۱۵ -  آغاز داستان
---
# بخش ۱۵ -  آغاز داستان

<div class="b" id="bn1"><div class="m1"><p>کنیزی بود قیصر را در ایوان</p></div>
<div class="m2"><p>که بودش مشتری هندوی دربان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبودی آدمی در روم و بغداد</p></div>
<div class="m2"><p>بزیبایی آن حور پری زاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبش جان داروی دلبستگان بود</p></div>
<div class="m2"><p>مفرّح نامهٔدلخستگان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهانش پر شکر چون نُقل دانی</p></div>
<div class="m2"><p>چگویم پستهٔ چون ناردانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزاران خوشهٔ مشکین بمویش</p></div>
<div class="m2"><p>چو خوشه سرکشیده گِرد رویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مشک تازه یک یک موی شسته</p></div>
<div class="m2"><p>بآب زندگانی روی شسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ابرو طاق بر گردون فکنده</p></div>
<div class="m2"><p>ز گیسو مشک بر هامون فکنده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حریر عارضش نرمی خز داشت</p></div>
<div class="m2"><p>رخش گلنار و گل را رنگرز داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ایوان شد شه قیصر بشبگاه</p></div>
<div class="m2"><p>نشسته بود آن بت روی چون ماه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شاه آن چهرهٔ زیبای او دید</p></div>
<div class="m2"><p>دل خود مست یک یک جای او دید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بچربی گفت جانا در برم کش</p></div>
<div class="m2"><p>بنقدی بوسهیی دو بر سرم کش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنیزک پیش شاه برجست از جای</p></div>
<div class="m2"><p>نهادش همچو گیسو روی بر پای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شه از قندش شکر را بار میکرد</p></div>
<div class="m2"><p>شکر میخورد و دیگر کار میکرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شه بر تل سیمین برد خیمه</p></div>
<div class="m2"><p>شد از یاقوت،‌دُرج دُر دو نیمه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درآمد آب گرم از باد گیری</p></div>
<div class="m2"><p>شکر در لب گداخت و ریخت شیری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شیر و شکّرش هر دو بسر شد</p></div>
<div class="m2"><p>کنیزک یکسر از شه بارور شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس از یک هفته کاری بود رفته</p></div>
<div class="m2"><p>که شه شد دور از آن ماه دو هفته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برون شد از جزیره همچو بادی</p></div>
<div class="m2"><p>که پیکی در رسیدش بامدادی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که کافر عزم شهر روم دارد</p></div>
<div class="m2"><p>بترسا قصد نامعلوم دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شه آن بت را رها کرد و برون شد</p></div>
<div class="m2"><p>بدریا رفت و زو صد جوی خون شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو اسکندر به آب زندگانی</p></div>
<div class="m2"><p>بسنبل آمد آن جمشید ثانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سپه چون مور جمله زیر فرمان</p></div>
<div class="m2"><p>شه قیصر بکردار سلیمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درو دشت از سپاه او سیه شد</p></div>
<div class="m2"><p>ز بیم شاه رنگ از روی مه شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درآهن غرق کرده همچنان سُم</p></div>
<div class="m2"><p>مگر چشم، از دو گوش اسب تادم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپه چون کوه میشد فوج بر فوج</p></div>
<div class="m2"><p>چنانک از روی دریا موج بر موج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز لشکر پشت ماهی شد شکسته</p></div>
<div class="m2"><p>شکم را باز برآورد خسته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نمیافکند جوشن بیم آن بود</p></div>
<div class="m2"><p>ولیکن پای گاوی در میان بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو قیصر رفت، آن زیبا کنیزک</p></div>
<div class="m2"><p>بنازیدی بفرزند مبارک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که گرمن مادر فرزند گردم</p></div>
<div class="m2"><p>چو شاخ سبز نیرومند گردم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو شاخ سبزم آرد میوه دربار</p></div>
<div class="m2"><p>زبی برگی برون آیم بیکبار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وگربی میوه شد شاخ سرافراز</p></div>
<div class="m2"><p>بسوزد تا بماند بارکش باز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کنون بنگر که چرخ حُقّه کردار</p></div>
<div class="m2"><p>چگونه مُهره گردانید در کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شه قیصر یکی خاتون زنی داشت</p></div>
<div class="m2"><p>که دل از رشک او ناروشنی داشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنیزک بود ملک خود هزارش</p></div>
<div class="m2"><p>وزان صد خادم و صد پیشکارش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز قارون کم ندیدی نعمت خویش</p></div>
<div class="m2"><p>ز قیصر بیش دیدی حرمت خویش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رخی چون ماه داشت آن دانهٔ دُرّ</p></div>
<div class="m2"><p>بمه در ننگرستی از تکبّر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز شیرینی چو شکّر تلخ کُش بود</p></div>
<div class="m2"><p>جهان بر وی ز شیرینی تُرشُ بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز کار آن کنیزک آگهی داشت</p></div>
<div class="m2"><p>همی بر کار او اندیشه بگماشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که گر او را ز قیصر بچه آید</p></div>
<div class="m2"><p>همه کار منش بازیچه آید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز گردون برتری جوید دماغش</p></div>
<div class="m2"><p>بپیش آفتاب آید چراغش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شود از تر مزاجی پای کوبی</p></div>
<div class="m2"><p>ببندد دست من بر خشک چوبی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو من این دم ز آتش دود بینم</p></div>
<div class="m2"><p>گر این آتش نشانم سود بینم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو چوبی را توانی ساخت تختی</p></div>
<div class="m2"><p>اگر تو خوار بگذاریش لختی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بغفلت چون برآید روزگاری</p></div>
<div class="m2"><p>شود آن چوب تخت آنگاه داری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خرد را رهنمون باید گرفتن</p></div>
<div class="m2"><p>چنین کاری کنون باید گرفتن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو یاری خواهی از یاری که باید</p></div>
<div class="m2"><p>بوقت خویش کن کاری که باید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کنیزی را برخود خواند بانو</p></div>
<div class="m2"><p>که درمانی بساز و گیر دارو</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بحلوا کن همی داروی این درد</p></div>
<div class="m2"><p>شکر لب را بده حلوا و برگرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مگر زین دارو آن مرغ سبکدل</p></div>
<div class="m2"><p>بیندازد بچه چون مرغ بسمل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کنیزک همچو گردون پشت خم داد</p></div>
<div class="m2"><p>چو صبحی خنده زد و انگاه دم داد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که گر دارد رخم چون غنچه آن ماه</p></div>
<div class="m2"><p>چو گُل خونش بریزم بر سر راه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بگفت این وز پیش آن فسونگر</p></div>
<div class="m2"><p>پری رخ شد برون چون حلقه بر در</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو شد بیرون بکرد اندیشه آن ماه</p></div>
<div class="m2"><p>نداد آن گفت را در گوش دل راه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که گر امروز گیرم سست این کار</p></div>
<div class="m2"><p>بصد سختی شوم فردا گرفتار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نباید کرد بد با بی گناهی</p></div>
<div class="m2"><p>نباید کند خود را نیز چاهی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گُنه نبود بتر زین در طبیعت</p></div>
<div class="m2"><p>مکن با بی گناهی این صنیعت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دل قیصر اگر گردد خبردار</p></div>
<div class="m2"><p>مرا در خون بگرداند چو پرگار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز قفل غم دلش در بند آمد</p></div>
<div class="m2"><p>بپیش مادر فرزند آمد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که از خاتون شنیدم پاسخ امروز</p></div>
<div class="m2"><p>که داری در شکم دُرّی شب افروز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرا از درد تو فرمود بانو</p></div>
<div class="m2"><p>که آن دُر را فرود آرم بدارو</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دل من بسته دارد با خدا کار</p></div>
<div class="m2"><p>نیم این بیوفایی را وفادار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چرا باکودکی گردم فسونساز</p></div>
<div class="m2"><p>که گردد آن فسون آخر بمن باز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دلی کو خویش را نبود نکوخواه</p></div>
<div class="m2"><p>بزودی چشم بد یابد بدو راه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کنون من راز خاتون با تو گفتم</p></div>
<div class="m2"><p>بسی از پرده بیرون با تو گفتم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز کارتو غمی بسیار خوردم</p></div>
<div class="m2"><p>ز تو بر جان خود زنهار خوردم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنان باید که فرمانم بری تو</p></div>
<div class="m2"><p>بکوشی تا ز فرمان نگذری تو</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ترا در خانهٔ خود جای سازم</p></div>
<div class="m2"><p>ز رویت خانه شهر آرای سازم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بیندازم ترادر خانه بستر</p></div>
<div class="m2"><p>بیایم چون قلم پیش تو بر سر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بسازم کار تو پنهان ز خاتون</p></div>
<div class="m2"><p>که تا گل بشکفد از غنچه بیرون</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو گل بشکتفه شد برگیرم او را</p></div>
<div class="m2"><p>کجا من با دو پستان شیرم او را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ازین شهرش بشهر خود برم من</p></div>
<div class="m2"><p>بشیر و شکّرش میپرورم من</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو بالا گیرد آنگه بازش آرم</p></div>
<div class="m2"><p>بر قیصر بصد اعزازش آرم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که گر اینجا بماند این گل نغز</p></div>
<div class="m2"><p>زند خاتون زرشکش خار در مغز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شد آبستن از آن اندیشه بی خویش</p></div>
<div class="m2"><p>چو مستسقی شکم بنهاد در پیش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نمیدانست آن آبستنی شاه</p></div>
<div class="m2"><p>که شب آبستنست و طفل در راه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو بشنود این سخن تن زد زمانی</p></div>
<div class="m2"><p>گشاد از پسته چون شکّر زبانی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بران زیبا کنیزک آفرین کرد</p></div>
<div class="m2"><p>که منشیناد بر تو از زمین گرد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو دور چرخ بادا زندگانیت</p></div>
<div class="m2"><p>مبادا چرخ بی دور جوانیت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ترامن ای کنیزک، گرچه خامم</p></div>
<div class="m2"><p>دلم میسوزد از جانت غلامم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز دولتگاه جان دلداریت باد</p></div>
<div class="m2"><p>ز عمر خویش برخورداریت باد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کسی کز نیکویی دارد نصیبی</p></div>
<div class="m2"><p>نکو خواهی ازو نبود غریبی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ترا گر این سخن ناگفته بودی</p></div>
<div class="m2"><p>خراج گور بر من رفته بودی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کنون کاری که میخواهی بجا آر</p></div>
<div class="m2"><p>مرا زین سرنگونساری بپا آر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>کنیزک برد او را سوی خانه</p></div>
<div class="m2"><p>یکی معجون برآمیخت از بهانه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>در آن خانه پر از خون کرد طاسی</p></div>
<div class="m2"><p>نهاد این کار را بر خون اساسی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ز خون پر کرده طاسی مینهادند</p></div>
<div class="m2"><p>که عشقی را اساسی مینهادند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تو هم در طاس گردون سر نگونی</p></div>
<div class="m2"><p>نمیدانی که سر در طاس خونی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گر آن خون بایدت، دل بر شفق نه</p></div>
<div class="m2"><p>فلک بر خون رود، جان بر طبق نه</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کنیزک شد سوی کدبانوی خویش</p></div>
<div class="m2"><p>بشادی شکر گفت از داروی خویش</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که دارو دادم و خون شد روانه</p></div>
<div class="m2"><p>زهی دارو که در خون کرد خانه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شنود آن قول خاتون، مکر نشناخت</p></div>
<div class="m2"><p>چو چنگش در درون پرده بنواخت</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بدو گفت آنچه باید کرد کردی</p></div>
<div class="m2"><p>کنون درمانش کن گر مرد مردی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو خون خصم در گردن نشاید</p></div>
<div class="m2"><p>بیک دارو دو خون کردن نشاید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کنیزک باز گشت و چون گل از خار</p></div>
<div class="m2"><p>بپیش طاس خون آمد دگر بار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نشست و ماجرا از دل ادا کرد</p></div>
<div class="m2"><p>بسی برجانش آبستن دعا کرد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کنیزک پرده دار کار او شد</p></div>
<div class="m2"><p>چو مه در پرده خدمتگار او شد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بشیر و شکّرش پروانه میداد</p></div>
<div class="m2"><p>چو شهدش تربیب درخانه میداد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو زن را نوبت زادن درآمد</p></div>
<div class="m2"><p>ز غنچه گل بافتادن درآمد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گلی بشکفت همچون نوبهاری</p></div>
<div class="m2"><p>که حسنش ماه را بنهاد خاری</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چو آمد بر زمین آن سرو دلخواه</p></div>
<div class="m2"><p>خجل در پرده شد بر آسمان ماه</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چنان پاکیزه و بازیب و فر بود</p></div>
<div class="m2"><p>که خورشیدی ز جمشیدی دگر بود</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چوجانآمد عزیز از مصر شاهی</p></div>
<div class="m2"><p>چو یوسف نیل چرخ از شرم ماهی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>اگرچه کودک یکروزه بود او</p></div>
<div class="m2"><p>بتن یکسالهیی را مینمود او</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چنین دانم که از دریای عنصر</p></div>
<div class="m2"><p>نظیر او نخیزد دانهٔ دُر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو مادر دید ماه و سرو باغش</p></div>
<div class="m2"><p>جهان روشن شد از چشم چراغش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>برومی کرد نام آن دلستان را</p></div>
<div class="m2"><p>که باشد پارسی خسرو زبان را</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>کنیزک گفت کاکنون وقت آنست</p></div>
<div class="m2"><p>که رفتن به بود، کار این زمانست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بشهر خود برم این دلستان را</p></div>
<div class="m2"><p>چو جانست او بکوشم سخت جان را</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>که میدانست کان گل را بناچار</p></div>
<div class="m2"><p>گلی در آب خواهد بود پرخار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>دُری کان از صدف آمد بصد ناز</p></div>
<div class="m2"><p>بدریا افکند خاتون بسر باز</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بزهر آن نوش لب را چاره جوید</p></div>
<div class="m2"><p>بدارو درد آن مهپاره جوید</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بسی بگریست مادر از پس او</p></div>
<div class="m2"><p>که بود آن مادر بیکس کس او</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ولی چون کار سخت افتاد، ناکام</p></div>
<div class="m2"><p>چو مرغی ماند بی دُردانه در دام</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>اگر ما روز و شب تدبیر سازیم</p></div>
<div class="m2"><p>همان بهتر که با تقدیر سازیم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>سپر چون نیست یک تیر قضا را</p></div>
<div class="m2"><p>رضاده حکم و تقدیر خدا را</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>کنیزک دل از آن بنگاه برداشت</p></div>
<div class="m2"><p>بکشتی در نشست و راه برداشت</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>دو گنجش بود در کشتی نهاده</p></div>
<div class="m2"><p>یکی از زر دگر از شاه زاده</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>دو خادم نیز خدمتگار بودند</p></div>
<div class="m2"><p>که چون کافور و عنبر یار بودند</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>درآمد باد و ابری سخت ناگاه</p></div>
<div class="m2"><p>بگردانید کشتی قرب یک ماه</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>به بیراهی بس کشتی نگون کرد</p></div>
<div class="m2"><p>باخر سر بآبسکون برون کرد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>کنار بحر جمعی کاروان بود</p></div>
<div class="m2"><p>شکر لب همچو شمعی در میان بود</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>مگر آن کاروان میشد باهواز</p></div>
<div class="m2"><p>بهمراهی ایشان گشت دمساز</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>روانه شد چنان کز باد خاکی</p></div>
<div class="m2"><p>بزیر محمل او بیسراکی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>زهر منزل بهر منزل همی شد</p></div>
<div class="m2"><p>سبک میشد از آن کز دل همی شد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>شبی تیره جهانی آرمیده</p></div>
<div class="m2"><p>سیاهی در پلاس شب دمیده</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>زمینی بود بگرفته سیاهی</p></div>
<div class="m2"><p>فکنده قیر برمه سایگاهی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>همه شب شب سیاهی میسرشتی</p></div>
<div class="m2"><p>شتر در شب سیاهی مینوشتی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>شبانروزی بماهی ره بریدند</p></div>
<div class="m2"><p>سرمه رهزنان در راه دیدند</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بگرد کاروان بس حلقه کردند</p></div>
<div class="m2"><p>ز حلق آن حلقه در خون غرقه کردند</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>مگر دزدی که خون بی باک میریخت</p></div>
<div class="m2"><p>ز حلق دایه خون بر خاک میریخت</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بسی از درد دل آن دایه بگریست</p></div>
<div class="m2"><p>که بی من چون بود این طفل را زیست</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ندارم از جهان جز نیم جانی</p></div>
<div class="m2"><p>دهید این نیم جان را نیم نانی</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>که تا هر کار کان آید ز دستم</p></div>
<div class="m2"><p>بدان رغبت نمایم تا که هستم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چو بس بیچاره میدیدند او را</p></div>
<div class="m2"><p>بجان آخر ببخشیدند او را</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بره درباخودش بسیار بردند</p></div>
<div class="m2"><p>ز بیمارش بسی تیمار خوردند</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چو خوزستان پدیدار آمد از دور</p></div>
<div class="m2"><p>شکر را سر بره دادند رنجور</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>کنیزک ماند با آن بچهٔ خرد</p></div>
<div class="m2"><p>برهنه پای و سر بر دست میبرد</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>گرسنه بیسر و سامان بمانده</p></div>
<div class="m2"><p>ز جان سیر آمده حیران بمانده</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>طمع ببرید ازدور جوانی</p></div>
<div class="m2"><p>چو پیری ناامید از زندگانی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ز دست روزگارش پای در گل</p></div>
<div class="m2"><p>ز چرخ بیسر و پا دست بر دل</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو ابری بر رخ صحرا بمانده</p></div>
<div class="m2"><p>چو باران اشک بر صحرا فشانده</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ز نرگس، روی آن صحرا فروشست</p></div>
<div class="m2"><p>ز اشک او گل از صحرا برون رست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ز خون چشم، صحرا کرد پرگل</p></div>
<div class="m2"><p>جهانی درد، صحرا کرد بر دل</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>دلش از صحن آن صحرا برون بود</p></div>
<div class="m2"><p>تنش وابستهٔ صحرای خون بود</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>ز خون هر سنگ صحرا کرد گلگون</p></div>
<div class="m2"><p>دل هر سنگ صحرا گشت ازو خون</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>بزاری چشم بر صحرا نهاده</p></div>
<div class="m2"><p>وزو فریاد در صحرا فتاده</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>در آنصحرا ز ابر افزون گرسته</p></div>
<div class="m2"><p>وزو هر سنگ صحرا خون گرسته</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>در آن صحراش یک گرگ آشنانه</p></div>
<div class="m2"><p>ز صحرا در دلش جز تنگانه</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>چو تنگی دید در صحرای سینه</p></div>
<div class="m2"><p>ز سینه ریخت بر صحرا خزینه</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>بسی سودا بصحرا خواست آورد</p></div>
<div class="m2"><p>ولیکن همچو صحرا کاست آورد</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>بآخر شش شبانروز آن دلفروز</p></div>
<div class="m2"><p>قدم میزد بره تا هفتمین روز</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چو پیدا گشت از ایوان چارم</p></div>
<div class="m2"><p>بروز هفتمین سلطان انجم</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ز چرخ نیلگون آیینه خور</p></div>
<div class="m2"><p>سپیده سرمه ریخت از مهبط زر</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چنان آن گوی زر زیر علم شد</p></div>
<div class="m2"><p>که لوح مه ز تیغ او قلم شد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بخوزستان رسید آن تنگ شکّر</p></div>
<div class="m2"><p>گرفته شیرخواری تنگ در بر</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بره در منظری پر کار میدید</p></div>
<div class="m2"><p>یکی ایوان فلک کردار میدید</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>چنان ازدور آن ایوان نمودی</p></div>
<div class="m2"><p>که جفت طاق نوشروان نمودی</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>دکانی بود پیشش سرکشیده</p></div>
<div class="m2"><p>فلک بابام او سر در کشیده</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>کنیزک سخت سستی داشت در راه</p></div>
<div class="m2"><p>بدکانی برآمد چون بشب ماه</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>ز رنج شیر و تفت آشکاره</p></div>
<div class="m2"><p>بنالید آن شکر لب شیرخواره</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>کجا برگ گلی را تاب باشد</p></div>
<div class="m2"><p>که در شهری شکر بی آب باشد</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>بسستی سیمبر را بر بیفتاد</p></div>
<div class="m2"><p>زبانش پیش دراز در بیفتاد</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>ز نرگس روی زر پر سیم کرد او</p></div>
<div class="m2"><p>دل پرخون بحق تسلیم کرد او</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>چو کاری سخت آمد پیش مخروش</p></div>
<div class="m2"><p>سبک کن حلقهٔ تسلیم در گوش</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>دلی در بند تا وقتش درآید</p></div>
<div class="m2"><p>ترازان حلقه درها برگشاید</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>که حق یک در نبندد مصلحت را</p></div>
<div class="m2"><p>که صد نگشایدت صد منفعت را</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>شه آن ناحیت را بود باغی</p></div>
<div class="m2"><p> زحوضش چشمهٔ گردون چراغی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بخوشی باغ در عالم علم بود</p></div>
<div class="m2"><p>مگر آن باغ خوش، باغ اِرم بود</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>کنیزک بر در آن باغ خفته</p></div>
<div class="m2"><p>دلش بیدار و عقل و هوش رفته</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>برون آمد از آن در باغبانی</p></div>
<div class="m2"><p>گلی تر دید پیش گلستانی</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>کجا مِه مرد بود آن مرد را نام</p></div>
<div class="m2"><p>جوانمردی او را کهتر ایام</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>در آن نزدیک طفلی مرده بودش</p></div>
<div class="m2"><p>جهان پیر جانی برده بودش</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>مصیبت خورده مرد از باغ میرفت</p></div>
<div class="m2"><p> ز درد طفل دل پر داغ میرفت</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>زن مِه مرد با او بود همراه</p></div>
<div class="m2"><p>ز طفل رفته اندر ناله و آه</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>جهان آن طفلشان افکند در سر</p></div>
<div class="m2"><p>که تا این طفل را گیرند در بر</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>چو دیدندنش چنان بر در بمانده</p></div>
<div class="m2"><p>مهی ماه نوش در بر بمانده</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>بدو مِه مرد ظنّی بس نکو برد</p></div>
<div class="m2"><p>بکهتر خانهٔ خویشش فرو برد</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>نشست القصه مرد و زن سخنور</p></div>
<div class="m2"><p>بپرسیدند حال آن سمنبر</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>سمنبر گفت حال من درازست</p></div>
<div class="m2"><p>نمانده آب و یک نانم نیازست</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>که این گلرخ ز بی شیری مادر</p></div>
<div class="m2"><p>گدازان شد ز بهر شیر و شکر</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>توانم دید خود را خاکساری</p></div>
<div class="m2"><p>نیارم دید بر فرقش غباری</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>بشد مِه مرد حلوا برد و نانش</p></div>
<div class="m2"><p>که طفلش مرده بود این بود و آنش</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>توهم ای مرد مرده باش از پیش</p></div>
<div class="m2"><p>که تا حلوا رسد از تو بدرویش</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>چو حلوا خوردن تو بیش گردد</p></div>
<div class="m2"><p>شود خون و سزای نیش گردد</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>چراحلوا بشیرینی کنی نوش </p></div>
<div class="m2"><p>که خون آرد بشیرینیت در جوش</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>ز حلوا کی بود روی سلامت</p></div>
<div class="m2"><p>که حلوا در قفا دارد حجامت</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>درونت دوزخست ای مالک خویش</p></div>
<div class="m2"><p>طبق دارد ز جسمت هفت بندیش</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>گر آرندت طبق با نان ز مطبخ</p></div>
<div class="m2"><p>طبق بانان در اندازی بدوزخ</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>بهر گندم که خوردی بیحسابی</p></div>
<div class="m2"><p>دلت را با بهشت افتد حجابی</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>شکم چون دوزخی با هفت در دان</p></div>
<div class="m2"><p>درو هروادیی وادی دگر دان</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>ازان یک وادیش پیشان ندارد</p></div>
<div class="m2"><p>که حرص آدمی پایان ندارد</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>اگر معده نبودی غم نبودی</p></div>
<div class="m2"><p>خصومت در همه عالم نبودی</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>شنودی قصّهٔ حلوا و نان را</p></div>
<div class="m2"><p>بسست این زلّه کن این را و آن را</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>کنیزک چون بسی حلواو نان خورد</p></div>
<div class="m2"><p>دلش شد گرم و تن زنهار جان خورد</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>عرق همچون گلاب از وی روانشد</p></div>
<div class="m2"><p>دو گلبرگش چو شاخ زعفران شد</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>دو چشمه خشک باز آمد ز پستانش</p></div>
<div class="m2"><p>دو چشمهٔ چشم بگشاد ازنم آنش</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>ز بیماری درآید کوه از پای</p></div>
<div class="m2"><p>چه سنجد کاه برگی باد پیمای</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>برنجوری شکر شیرین نیاید</p></div>
<div class="m2"><p>که لب را از شکر تلخی فزاید</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>بتراز تن شکستن زحمتی نیست</p></div>
<div class="m2"><p>ورای تندرستی نعمتی نیست</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>دو نعمت را مکن در شکر سستی</p></div>
<div class="m2"><p>یکی امن و دگر یک تندرستی</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>چو در باغ آن سمنبر گشت بیمار</p></div>
<div class="m2"><p>بماند آن باغبان در رنج و تیمار</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>بزن گفت ای غلام تو زمانه</p></div>
<div class="m2"><p>نهان دار این کنیزک را بخانه</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>که تا گر این کنیزک زار میرد</p></div>
<div class="m2"><p>دلم این طفل را دلدار گیرد</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>که هرگز در همه روی زمین من</p></div>
<div class="m2"><p>ندیدم ماهرویی مثل این من</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>ببینی گر بود از عمر بهره</p></div>
<div class="m2"><p>که چون زیبا شود این ماه چهره</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>بدین روی و بدین منظر که او راست</p></div>
<div class="m2"><p>بماهی و بسروی ماند او راست</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>بجان خواهم که کارش را کنی ساز</p></div>
<div class="m2"><p>نگیری زین شکر لب شیرخود باز</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>زنش گفتا بجان فرمان برم من</p></div>
<div class="m2"><p>که گر این طفل بردم جان برم من</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>چنان در پرده پنهان دارم این راز</p></div>
<div class="m2"><p>که نتواند شدن از پرده آواز</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>ز زیر پرده این دُرّ شب افروز</p></div>
<div class="m2"><p>نگردد آشکارا گر شود روز</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>چو نور دیده او را راز دارم</p></div>
<div class="m2"><p>بزیر هفت پردهش باز دارم</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>زن بد را مده نزدیک خود جای</p></div>
<div class="m2"><p>که مردان از زن نیکند بر پای</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>بسی بهتر بود در کُنج خانه</p></div>
<div class="m2"><p>عیال نیک از گنج و خزانه</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>چو مرد نیک رازن سازگارست</p></div>
<div class="m2"><p>همه کارش بدان زن چون نگارست</p></div></div>