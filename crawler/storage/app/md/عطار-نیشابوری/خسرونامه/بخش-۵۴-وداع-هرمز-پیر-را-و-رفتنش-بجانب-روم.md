---
title: >-
    بخش ۵۴ - وداع هرمز پیر را و رفتنش بجانب روم
---
# بخش ۵۴ - وداع هرمز پیر را و رفتنش بجانب روم

<div class="b" id="bn1"><div class="m1"><p>بآخر خسرو از وی ره نشان خواست</p></div>
<div class="m2"><p>وداعش کرد و میشد بر نشان راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو القصه از آنجا درکشیدند</p></div>
<div class="m2"><p>بکوه و آب و جسری در رسیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهی خوش بود صحرا و سر کوه</p></div>
<div class="m2"><p>دهی پر نعمت و خلقی بانبوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی ده رفت با یاران بهم شاه</p></div>
<div class="m2"><p>بخواست از اهل ده یک مرد همراه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که تا رهبر بود در راه او را</p></div>
<div class="m2"><p>کند از نیک و بد آگاه او را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خورشید آسیا سنگ زراندود</p></div>
<div class="m2"><p>ز زیر آسیای چرخ بنمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزاران دانه داشت آن توتیا رنگ</p></div>
<div class="m2"><p>بیکبار آس کرد آن آسیا سنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپیدی روز میدانی چراتافت</p></div>
<div class="m2"><p>که روی روز گرد آسیا یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بآخر شهریار وجمع یاران</p></div>
<div class="m2"><p>روان گشتند چون از میغ باران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو روز دیگر آن ایوان نه طاق</p></div>
<div class="m2"><p>منور شد ز نور شمع آفاق</p></div></div>