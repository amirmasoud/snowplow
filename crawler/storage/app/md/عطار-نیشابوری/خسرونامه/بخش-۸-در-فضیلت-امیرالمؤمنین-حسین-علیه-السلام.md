---
title: >-
    بخش ۸ - در فضیلت امیرالمؤمنین حسین علیه السلام
---
# بخش ۸ - در فضیلت امیرالمؤمنین حسین علیه السلام

<div class="b" id="bn1"><div class="m1"><p>امامی کافتاب خافقینست</p></div>
<div class="m2"><p>امام از ماه تا ماهی حسینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خورشیدی جهان را خسرو آمد</p></div>
<div class="m2"><p>که نه معصوم پاکش پس رو آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آن خورشید اصل خاندانست</p></div>
<div class="m2"><p>بمهرش نه فلک از پی روانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ آسمان مکرمت بود</p></div>
<div class="m2"><p>جهان علم و بحر معرفت بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهمّت هر دو عالم کم گرفته</p></div>
<div class="m2"><p>ولی نورش همه عالم گرفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ او بود خورشید الهی</p></div>
<div class="m2"><p>شبی تاریک، مویش از سیاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی کو آفتاب و شب بهم خواست</p></div>
<div class="m2"><p>حسن آن از حسین آمد بهم راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امام ده و دو حق کرد قسمت</p></div>
<div class="m2"><p>که هر یک پردهیی سازد ز عصمت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ده و دو پرده زان آمد پدیدار</p></div>
<div class="m2"><p>حسینی بود امّا پردهیی زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببر داین راه او گر مبتلا بود</p></div>
<div class="m2"><p>ولی خونریز او در کربلا بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر هستی تو اهل پردهٔ راز</p></div>
<div class="m2"><p>ازین پرده بزاری میده آواز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی خون کرده‌اند اهل ملامت</p></div>
<div class="m2"><p>ولی این خون نخسبد تا قیامت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آن خونی که بر روی زمانهست</p></div>
<div class="m2"><p>برفت از چشم و این خون جاودانهست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو ذاتش آفتاب جاودان بود</p></div>
<div class="m2"><p>ز خون او شفق باقی ازان بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو آن خورشید دین شد ناپدیدار</p></div>
<div class="m2"><p>در آن خون چرخ میگردد چو پرگار</p></div></div>