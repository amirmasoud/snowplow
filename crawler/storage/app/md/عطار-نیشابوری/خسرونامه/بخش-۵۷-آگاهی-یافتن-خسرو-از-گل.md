---
title: >-
    بخش ۵۷ - آگاهی یافتن خسرو از گل
---
# بخش ۵۷ - آگاهی یافتن خسرو از گل

<div class="b" id="bn1"><div class="m1"><p>چوصبح پرده در از پرده دم زد</p></div>
<div class="m2"><p>عروس عالم غیبی علم زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم عیسی از آن زد صبح خوش دم</p></div>
<div class="m2"><p>که بویی داشت از عیسی و مریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شد از شمع این پیروزه گلشن</p></div>
<div class="m2"><p>جهان را چون چراغی چشم روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو خادم دشمن شهزاده بودند</p></div>
<div class="m2"><p>وزو در سختیی افتاده بودند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپیش شه شدند و راز گفتند</p></div>
<div class="m2"><p>همه احوال دختر باز گفتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که با شهزاده برنایی چنین کرد</p></div>
<div class="m2"><p>وزو در یک زمان خون بر زمین کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه شهر این زمان گویند امروز</p></div>
<div class="m2"><p>همه زین غصّه میگریند وزین سوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چوشاه ترک شد زان قصّه آگاه</p></div>
<div class="m2"><p>فغان برخاست زو زین غصّه ناگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حمیّت دردل او کارگر شد</p></div>
<div class="m2"><p>قرار و صبر از جانش بدر شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دریا شد دل شوریدهٔ او</p></div>
<div class="m2"><p>برامد موج خون از دیدهٔ او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خون شد هر دو چشم او ازان غم</p></div>
<div class="m2"><p>نداشت او چشم دیدن را ازان هم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بفرمود آن زمان شاه سرافراز</p></div>
<div class="m2"><p>که تا شهزاده را برند سر، باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزرگان چون شنیدند این سخن را</p></div>
<div class="m2"><p>شفاعت خواستند آن سرو بن را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که این کشتن نه کار پادشاهست</p></div>
<div class="m2"><p>که این شهزاده بی شک بی گناهست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گنه زان مرد نامعلوم رفتست</p></div>
<div class="m2"><p>که دختر خفته او در بوم رفتست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شه چین خورد بی اندازه سوگند</p></div>
<div class="m2"><p>کزین پس برندارم هرگزش بند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بجان بخشیدمش تا باشد از دور</p></div>
<div class="m2"><p>ولی میلش کشم در چشمهٔ نور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی کو دختری در خانه دارد</p></div>
<div class="m2"><p>تنی لاغر دلی دیوانه دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غم دختر که میخ دامن تست</p></div>
<div class="m2"><p>چو طوق آتشین درگردن تست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وزیر خاص را فرمود آنگاه</p></div>
<div class="m2"><p>که دو چشمش ز میل اندازدرراه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزیر خاص چون شه را چنان دید</p></div>
<div class="m2"><p>بدان دلداده دل را مهربان دید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ببرد آن سیمبر راو نهان کرد</p></div>
<div class="m2"><p>زبان در پیش دختر دُرفشان کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که بهر چشم بد نیلت کشم من</p></div>
<div class="m2"><p>مبادم چشم اگر میلت کشم من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ترا پنهان بدارم تا شه چین</p></div>
<div class="m2"><p>چو مه با مهر گردد از ره کین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو دل خوش کرد لختی شاه با تو</p></div>
<div class="m2"><p>بگویم گفتنی آنگاه باتو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفت این و بپیش شاه چین شد</p></div>
<div class="m2"><p>زخون چشم خونین آستین شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که میلش در کشیدم وز قیاسی</p></div>
<div class="m2"><p>جهان بر چشم او شد چون پلاسی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه گویم من که باد از چشم شه دور</p></div>
<div class="m2"><p>که چون تاریک شد آن چشمهٔ نور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو شه بشنود گفتا نیست باکی</p></div>
<div class="m2"><p>مخور زوغم که باد آن شوم خاکی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگفت این و بفرمود آن زمان شاه</p></div>
<div class="m2"><p>که آتش را برافروزند در راه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز نفت وهیزم آتش برفروزند</p></div>
<div class="m2"><p>گل سیراب در آتش بسوزند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو بردارش کنند آنگه بزاری</p></div>
<div class="m2"><p>میان آتش آرندش بخواری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گلی را کی بود طاقت، زهی خوش</p></div>
<div class="m2"><p>کش اوّل دار باشد آخر آتش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>براه عشق ازین کمتر نباید</p></div>
<div class="m2"><p>که عاشق تا نسوزد بر نیاید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چوآتش بوتهٔ مردان راهست</p></div>
<div class="m2"><p>بباید سوخت آتش خوابگاهست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کسی داند بلای عشق دلخواه</p></div>
<div class="m2"><p>که خون و آتشش دارد بدل راه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلی عاشق ازین بسیار بیند</p></div>
<div class="m2"><p>که تخت خویشتن از دار بیند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کسی کز عشق خود بشنوده باشد</p></div>
<div class="m2"><p>چنان نبود که عاشق بوده باشد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>الا ای اهل درد آخر کجایید</p></div>
<div class="m2"><p>درین مجلس زمانی حاضر آیید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز میغ دیده بارانها ببارید</p></div>
<div class="m2"><p>برین غم کشته طوفانها ببارید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز خونریزی نیامد کم درین راه</p></div>
<div class="m2"><p>که خون شد زهرهٔ عالم درین راه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خبر در عرصهٔ آن کشور افتاد</p></div>
<div class="m2"><p>که برنایی بکشتن باسر افتاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سراسر شهر چین آوازه بگرفت</p></div>
<div class="m2"><p>ز مردم راه بر دروازه بگرفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دوان گشتند از دروازه در باغ</p></div>
<div class="m2"><p>بیاوردند گل را بر جگر داغ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دلی پر آتش از کین میدمیدند</p></div>
<div class="m2"><p>بزلف آن سیمبر را میکشیدند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو کاهی روی گل دو چشم نمناک</p></div>
<div class="m2"><p>بخونی کاهگل کرده همه خاک</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لبی و صد شکر زلفی و صد تاب</p></div>
<div class="m2"><p>رخی و صد گهر چشمی و صد آب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برسوایی فتاده در کشاکش</p></div>
<div class="m2"><p>ببردندش بسوی دار و آتش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بآخر گل چو حیرانی فروماند</p></div>
<div class="m2"><p>ز یک یک مژّه صد طوفان فرو راند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدل گفتا بباید گفت رازم</p></div>
<div class="m2"><p>که چون من سوختم آنگه چه سازم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو جان پرتاب و دل دربند دارم</p></div>
<div class="m2"><p>بگویم راز، پنهان چند دارم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دگر ره گفت رسواگردی ای زن</p></div>
<div class="m2"><p>صبوری کن دمی گر مردی ای زن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فراوان خلق بود استاده بر راه</p></div>
<div class="m2"><p>عجب مانده ز زیبایی آن ماه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز نیکو رویی آن سرو آزاد</p></div>
<div class="m2"><p>قیامت در میان خلق افتاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بهم گفتند هرگز درجهانی</p></div>
<div class="m2"><p>نبیند کس نکوتر زین جوانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کسی در غم چنین بنموده باشد</p></div>
<div class="m2"><p>بشادی خودچگونه بوده باشد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هنوزش خطّ مشکین نادمیده</p></div>
<div class="m2"><p>جهان درخط کشیدش نارسیده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بدین خوبی که هست این سیمبر ماه</p></div>
<div class="m2"><p>همانا جرم هست از دختر شاه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بردند آن صنم را در بر دار</p></div>
<div class="m2"><p>برامد بانگ زاری بر سر کار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>غریوی از میان خلق برخاست</p></div>
<div class="m2"><p>تو گفتی جان خلق از حلق برخاست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو ظاهر شد خروش و اشک ریزی</p></div>
<div class="m2"><p>برامد های و هوی رستخیزی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چوسوی دار شد آن نازنین ماه</p></div>
<div class="m2"><p>ازو بی او برامد آتشین آه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدل میگفت: نی از دار ترسم</p></div>
<div class="m2"><p>ولیکن از فراق یار ترسم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>اگر خسرو شهم در پیش بودی</p></div>
<div class="m2"><p>مرا زین جان فشاندن بیش بودی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خوشی برخیزمی من از سر جان</p></div>
<div class="m2"><p>ولیکن نیست بی خسرو سر آن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هزاران جان و دل بر روی دلدار</p></div>
<div class="m2"><p>توان دادن چه در آتش چه بردار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وفا نبود که بی او جان دهم من</p></div>
<div class="m2"><p>مگر جان بر رخ جانان دهم من</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دلی دارم که درمانی ندارد</p></div>
<div class="m2"><p>چنین دل را غم جانی ندارد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بجان گر کار جانانم برآید</p></div>
<div class="m2"><p>روا دارم اگر جانم برآید</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بیا ای دوست تا سوزم ببینی</p></div>
<div class="m2"><p>که میخواهم که امروزم ببینی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دلم بر مرگ از افسون صد ورق خواند</p></div>
<div class="m2"><p>یکی نشنود و ازجان یک رمق ماند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دلم خون شد ز گرمی در تن از تو</p></div>
<div class="m2"><p>نکو دل گرمیی دیدم من ازتو</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بدست دشمنانم باز دادی</p></div>
<div class="m2"><p>بنای دوستی محکم نهادی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بزیر دار در ماندم بخواری</p></div>
<div class="m2"><p>بر آتش می بسوزندم بزاری</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نه تو زاتش خبر داری نه ازدار</p></div>
<div class="m2"><p>اگر وقت آمد ازدارم فرود آر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>مرا در عشق کمتر چیز دارست</p></div>
<div class="m2"><p>بتر از دار و آتش صد هزارست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دلاچندم بخون گردانی آخر</p></div>
<div class="m2"><p>بجان آوردیم، میدانی آخر؟</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدست خویش خود را خوار کردی</p></div>
<div class="m2"><p>برسوایی مرا بردار کردی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تو با من آنچه کردی کس نکردست</p></div>
<div class="m2"><p>هنوزت عشقبازی بس نکردست؟</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بسی گویی ولی سودی ندارد</p></div>
<div class="m2"><p>که کارت روی بهبودی ندارد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کسی کز یار خود صد بارش افتاد</p></div>
<div class="m2"><p>چه سازد چون بصد کس کارش افتاد؟</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نکو بودالحقم کاری و باری</p></div>
<div class="m2"><p>بسر بازیم دربایست داری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز قوم عاشقان نه کار بازیست</p></div>
<div class="m2"><p>که اوّل کار او را دار بازیست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اگر لرزندهیی برجان چه چیزی</p></div>
<div class="m2"><p>نه مردی نه زنی یعنی که حیزی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اگر خواهی که اهل نار گردی</p></div>
<div class="m2"><p>ز جان بیزار گرد دار گردی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو گفت این، های و هوی سخت دربست</p></div>
<div class="m2"><p>برفتن جانش از تن رخت بر بست</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو مردان نعرهیی از دل براورد</p></div>
<div class="m2"><p>بنعره پای دل از گل براورد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زبان بگشاد کاین رسوایی امروز</p></div>
<div class="m2"><p>بتر از کشتنست و از بسی سوز</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ولیک افتادهام در برگ ریزان</p></div>
<div class="m2"><p>بگویم، جان عزیزست ای عزیزان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگر زین بیش آگاهیم بودی</p></div>
<div class="m2"><p>کجا این سوز و گمراهیم بودی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>کنون آگاه گشتم من که ناگاه</p></div>
<div class="m2"><p>چه گفتند از من درویش باشاه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>الا ای خلق استاده برین دار</p></div>
<div class="m2"><p>خدا داند که بی جرمم درین کار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شما را دو گواهم عذر خواهست </p></div>
<div class="m2"><p>که این دم در بر من دو گواهست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>مپندارید از من زرق و دستان</p></div>
<div class="m2"><p>که هرگز مرد نبود نار پستان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مپندارید کز من کار خامست</p></div>
<div class="m2"><p>دوپستان دو گواه من تمامست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>منم در درد و دردم را دوا نه</p></div>
<div class="m2"><p>زنی دلداده و مرد شما نه</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>زنی را زار و سرگردان ببینید</p></div>
<div class="m2"><p>نیم من مرد، ای مردان ببینید</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>زنیام من که کرد آواره دهرم</p></div>
<div class="m2"><p>نه آن نامرد چندان باره شهرم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نبود از شیر مردی هیچ تقصیر</p></div>
<div class="m2"><p>چو رسوا کرد تقدیرم چه تدبیر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>که این گردون پیرسال پرورد</p></div>
<div class="m2"><p>زنی پیرست امّا ناجوانمرد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کنون چون من زنم کی مرد گردم</p></div>
<div class="m2"><p>چو مردان با دلم این درد خوردم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سپهر گرم رو سردی بسی کرد</p></div>
<div class="m2"><p>بدین زن ناجوانمردی بسی کرد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>کنون ای شیر مردان گر که مردید</p></div>
<div class="m2"><p>ازین زن، در میان خود مگردید</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو هست اینجا شما را جای مردی</p></div>
<div class="m2"><p>کنید این خسته زن را پایمردی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>زنی را پایمرد درد باشید</p></div>
<div class="m2"><p>که تادر کار این زن مرد باشید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>جهانی مرد و زن چون آن بدیدند</p></div>
<div class="m2"><p>از آن زن، بر زمین طوفان بدیدند</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>زنان گشته چو مردان مست درکوی</p></div>
<div class="m2"><p>همه مردان زنان دو دست بر روی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چو گلرخ از بر پیراهن خویش</p></div>
<div class="m2"><p>دو پستان کرد بیرون از تن خویش</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>خروشی در میان مردم افتاد</p></div>
<div class="m2"><p>تو گفتی آتشی در انجم افتاد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>همه خیره در آن پستان بماندند</p></div>
<div class="m2"><p>همه در کار گل حیران بماندند</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بپوشیدند در معجر سرماه</p></div>
<div class="m2"><p>خبر بردند ازان دلبر بر شاه</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>شه چینی چو آگه گشت ازان کار</p></div>
<div class="m2"><p>گل تر را بر خود خواند ازدار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو سروی سیمبر از در درامد</p></div>
<div class="m2"><p>دل خاقان چین از بر برامد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بیک دیدن دلش زیر و زبر شد</p></div>
<div class="m2"><p>بسی در عشقش از دختر بتر شد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چنان از مهر او دیوانه دل گشت</p></div>
<div class="m2"><p>کزان اندیشه هم در خودخجل گشت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بدل گفتا چنین زیبا که او هست</p></div>
<div class="m2"><p>دل دختر ز زیبایی فرو بست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چو بربود از برم او دل چنین زود</p></div>
<div class="m2"><p>چه گویم، حق بدست دخترم بود</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چنین رویی که این دلدار دارد</p></div>
<div class="m2"><p>بسی دختر درین غم یار دارد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>کسی در سوز این دلبر عجب نیست</p></div>
<div class="m2"><p>پدر چون فتنه شد دختر عجب نیست</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بگرمابه فرستادش بصد ناز</p></div>
<div class="m2"><p>دو تاکرد آن گره مشکین ز سر باز</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بحکم شه ز گرمابه برون شد</p></div>
<div class="m2"><p>بمشک و اطلسش زیور درون شد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چنان شد مهر او در جان آن شاه</p></div>
<div class="m2"><p>که یک ساعت دلش نشکفت ازان ماه</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>ز گلرخ حال او پرسید بسیار</p></div>
<div class="m2"><p>نیاورد آن صنم بر خود پدیدار</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مرا گفتا، پدر بازارگان بود</p></div>
<div class="m2"><p>همه کارش طواف بحر و کان بود</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>مرا هرجا که شد با خویشتن برد</p></div>
<div class="m2"><p>بآخر بار، هم در کار من مرد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>بدریا غرق گشت و من بناگاه</p></div>
<div class="m2"><p>ز کشتی اوفتادم بر سر راه</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>ز بیم ناجوانمردان ضرورت</p></div>
<div class="m2"><p>چو مردان ساختم خود را بصورت</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چو سوی این نگارستان فتادم</p></div>
<div class="m2"><p>بدار و آتش و زندان فتادم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ز جور دخترت در بند ماندم</p></div>
<div class="m2"><p>دران اندوه هم یک چند ماندم</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>نگفتم من زنم با آن دل افروز</p></div>
<div class="m2"><p>که ترسیدم ز رسوایی امروز</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>سخن میگفت ازینسان تا شب آمد</p></div>
<div class="m2"><p>فلک را ماه چون جان بر لب آمد</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو چتر خسرو انجم نگون شد</p></div>
<div class="m2"><p>لب دریای گردون جوی خون شد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>برامد راست چون آیینه از درج</p></div>
<div class="m2"><p>ز قلعه کوتوال و ماه از برج</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>دران شب شاه چین شمعی نهاده</p></div>
<div class="m2"><p>نشسته بود با آن حور زاده</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>همی چندانکه گل را بیش میدید</p></div>
<div class="m2"><p>سراپایش بکام خویش میدید</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>بت لاغر میان فربه سرین بود</p></div>
<div class="m2"><p>برخ چون گل بلب چون انگبین بود</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چو شاه ان انگبین و گل بهم دید</p></div>
<div class="m2"><p>خرد را زیر آن زلف بخم دید</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>دلش را زلف گل در دام آورد</p></div>
<div class="m2"><p>خرد آنجا زبان در کام آورد</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>حساب وصل آن دلبر بسی کرد</p></div>
<div class="m2"><p>خط و خالی بدست دل کسی کرد</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو صبر او چو تیری ازکمان جست</p></div>
<div class="m2"><p>دلش در بر چومرغی زاشیان جست</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>شهنشاه جوان و ماه در پیش</p></div>
<div class="m2"><p>چگونه صبر ماند خود بیندیش</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>بزد دست و کشیدش موی در بر</p></div>
<div class="m2"><p>چنان کافتاد ان مهروی بر سر</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>گل عاشق خروشی در جهان بست</p></div>
<div class="m2"><p>ز دل صد سیل خون بر دیدگان بست</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بفندق مشک را از گل فرو کند</p></div>
<div class="m2"><p>ز شاخ گلستان سنبل فرو کند</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>زمانی شعر ازرق چاک میزد</p></div>
<div class="m2"><p>زمانی اشک خون بر خاک میزد</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>خروش شیر برانجم فرو بست</p></div>
<div class="m2"><p>سرشکش راه بر مردم فرو بست</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>زمانی آه خون آلود میکرد</p></div>
<div class="m2"><p>زمانی زاتش دل دود میکرد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>شهش گفت این چه بیدادست آخر</p></div>
<div class="m2"><p>بده داد این چه فریادست آخر</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>تو میدانی که شاه گیتی افروز</p></div>
<div class="m2"><p>منم در چارحدّ عالم امروز</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>اگر از ماه گردون وصل جویم</p></div>
<div class="m2"><p>بنازد چون سخن بر اصل گویم</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>تو از پیش چو من شه سر بتابی</p></div>
<div class="m2"><p>نترسی زانکه بی تن سر بیابی</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>ترا به گر ز من میگیری امشب</p></div>
<div class="m2"><p>حساب رفته تا کی گیری امشب</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>بمی با من بعشرت پای داری</p></div>
<div class="m2"><p>که عشرت را ومی را جای داری</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>بعیش خوش، غم دل را قضا کن</p></div>
<div class="m2"><p>میسوزی طلب، ماتم رها کن</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>گل از گفتار شاه چین بجوشید</p></div>
<div class="m2"><p>همه خون دلش از کین بجوشید</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بدو گفت ای دغا باز دغا گوی</p></div>
<div class="m2"><p>جفاکار جفاورز جفا جوی</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>دغا بازی، حریف من نیی تو</p></div>
<div class="m2"><p>که چون من آتشین خرمن نیی تو</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>بترک من بگو ورنه ازین غم</p></div>
<div class="m2"><p>بریزم از تن خود خون همین دم</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>بخون خویشتن بندم میان را</p></div>
<div class="m2"><p>ز ننگ خود بپردازم جهان را</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>ز دست دخترت جستم کنون من</p></div>
<div class="m2"><p>چرا در پای تو گردم بخون من</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>منم با مادری مرده بزاری</p></div>
<div class="m2"><p>پدر غرقه شده در سوکواری</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>دلی ماتمزده خود میبپرسی</p></div>
<div class="m2"><p>بروز رستخیز از من عروسی؟</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>بزور تیغ از من وصل، افسوس</p></div>
<div class="m2"><p>گه گر بکشی مرا تیغت دهم بوس</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>شهش در بند کرد و رای آن بود</p></div>
<div class="m2"><p>که گل گردن نهد چه جای آن بود</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>نه بندش سودمند آمد نه پندش</p></div>
<div class="m2"><p>بطرح افگند شاه مستمندش</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>ولیکن پیش او رفتی چو بادی</p></div>
<div class="m2"><p>بدیدی روی او هر بامدادی</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>سخن گفتی ز هر فصلی و بابی</p></div>
<div class="m2"><p>ولی هرگز ندادی گل جوابی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>نکردی هیچ سوی او نگاهی</p></div>
<div class="m2"><p>که می ننگ آمدش زین پادشاهی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>نمیآسود از زاری و ناله</p></div>
<div class="m2"><p>خوشی بر لاله میبارید ژاله</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>فغان میکرد و میگفت ای جهاندار</p></div>
<div class="m2"><p>ز جان سیرم ندارم در جهان کار</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>بفضل خود برون بر از جهانم</p></div>
<div class="m2"><p>مرا تا کی ز جان، برگیر جانم</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>ندانم تا چه فال و بخت دارم</p></div>
<div class="m2"><p>که هر دم تازه بندی سخت دارم</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>نشسته بیدل و دلدار رفته</p></div>
<div class="m2"><p>بسی بارم فتاده یار رفته</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>چو در پرده ندارم هیچ یاری</p></div>
<div class="m2"><p>بجز زاری ندارم هیچ کاری</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>مرا چون نی خوشست این زاری من</p></div>
<div class="m2"><p>خنک شد این تب و بیماری من</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>شده تب از دم سردم خنکتر</p></div>
<div class="m2"><p>دلم گشته ز بیماری سبک تر</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>دلم بر آتشست از عشق هرمز</p></div>
<div class="m2"><p>ولی چشمم نگردد گرم هرگز</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>کجایی ای درون جان نشسته</p></div>
<div class="m2"><p>چنین پیدا چنین پنهان نشسته</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>اگرچه رویت از سویی نبینم</p></div>
<div class="m2"><p>ولی بر روی تو مویی نبینم</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>چنان بگرفتهیی یکسر نهادم</p></div>
<div class="m2"><p>که از خود مینیاید هیچ یادم</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>گلی از عشق تو در سینه دارم</p></div>
<div class="m2"><p>که خاری میشود گر دم برآرم</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>دلم در عشق چندان شور دارد</p></div>
<div class="m2"><p>که گر درعرش پیچد زور دارد</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>ز چشم پیل بالاخون چکیدهست</p></div>
<div class="m2"><p>که بر بالای چشم من بریدهست</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>گهرهای مرا کز دل دراید</p></div>
<div class="m2"><p>ترا بخشم گرم از دل براید</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>بهرمویی ز خون صد برق گردم</p></div>
<div class="m2"><p>که تا بیتو دران خون غرق گردم</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>ز سر تا پای پیوندی ندارم</p></div>
<div class="m2"><p>که چون زلفت بروبندی ندارم</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>چگویم راز دل زین بیش دیگر</p></div>
<div class="m2"><p>تو خود دانی فرواندیش دیگر</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>نیارم راز دل گفتن تمامت</p></div>
<div class="m2"><p>که روزی بایدم همچون قیامت</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>بگفت این و برفت ازهوش آن ماه</p></div>
<div class="m2"><p>چنان کز تفّ اوزد جوش آن ماه</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>چنین بودی دلی پر انتظارش</p></div>
<div class="m2"><p>غم خسرو شدی هم غمگسارش</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>نشسته با دل امّیدوار او</p></div>
<div class="m2"><p>که روزی باز بیند روی یار او</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>بصد زاری چو مرغی پر بریده</p></div>
<div class="m2"><p>میان دام، نیمی سر بریده</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>دمی میزد بامّید و دگر نه</p></div>
<div class="m2"><p>ز سستی زان دمش یک جو خبر نه</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>ازینسان بود روز و روزگارش</p></div>
<div class="m2"><p>نه یک همدم نه یک آموزگارش</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>موکّل بود بر گل خادمی زشت</p></div>
<div class="m2"><p>که نامش بود کافور و چوانگشت</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>ولیکن سخت نیکو خوی بودی</p></div>
<div class="m2"><p>بسی از مشک صدقش بوی بودی</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>نگهبان بود بر درّ شب افروز</p></div>
<div class="m2"><p>بشفقت کار گل کردی شب و روز</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>بدلداری شبش افسانه بودی</p></div>
<div class="m2"><p>بروزش همدم و همخانه بودی</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>بسی پندش بدادی در هر اندوه</p></div>
<div class="m2"><p>که بر دل می مکن چندین غم انبوه</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>بسی مگری که چشمت خیره گردد</p></div>
<div class="m2"><p>جهان برچشم روشن تیره گردد</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>بسی سوگند خوردی گاه و بیگاه</p></div>
<div class="m2"><p>که گر گردم من از حال تو آگاه</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>بسازم چارهٔ کارت بزودی</p></div>
<div class="m2"><p>برارم ماه بختت از کبودی</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>اگر باید گرفتن ترک جانم</p></div>
<div class="m2"><p>برای تو غمی نبود ازانم</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>بجان تو که گردیدم جهانی</p></div>
<div class="m2"><p>بفرّ تو ندیدم دلستانی</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>یقین دانم که ازنسل شهانی</p></div>
<div class="m2"><p>ولی در غم فتاده ناگهانی</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>مکن پنهان ز من رازی که داری</p></div>
<div class="m2"><p>برآراز پرده آوازی که داری</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>چه گر خادم بیاید نامساعد</p></div>
<div class="m2"><p>نمیکرد اعتماد آن سیم ساعد</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>شب و روز آن سمنبر نوحه گر بود</p></div>
<div class="m2"><p>زهر روزیش، هر روزی بتر بود</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>ز زاری کردن آن ماهپاره</p></div>
<div class="m2"><p>بفریاد آمد از گردون ستاره</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>ز درّ اشک او پروین بسر گشت</p></div>
<div class="m2"><p>بنات النعش نیز از رشک برگشت</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>شفق را خون چشمش رنگ میکرد</p></div>
<div class="m2"><p>فلک را تفّ او دلتنگ میکرد</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>ز آهش مرغ شب برتابه میسوخت</p></div>
<div class="m2"><p>جگر زان سوز درخونابه میسوخت</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>اگر دم برکشیدی صبح ازکوه</p></div>
<div class="m2"><p>فرورفتی دمش حالی از اندوه</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>وگرمه خیمه بر افلاک بردی</p></div>
<div class="m2"><p>از آن غم رخت را با خاک بردی</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>وگر خورشید سوز او بدیدی</p></div>
<div class="m2"><p>بشب رفتی چو روز اوبدیدی</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>دل کافور ازو میسوخت امّا</p></div>
<div class="m2"><p>نمیکرد آگهش گل زان معمّا</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>برین منوال چون بگذشت سالی</p></div>
<div class="m2"><p>شد آن مهروی از حال بحالی</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>دران اندوه لب برهم نهاده</p></div>
<div class="m2"><p>دلی چندی که شد بر غم نهاده</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>چو شد یکبارگی صبر و قرارش</p></div>
<div class="m2"><p>در آن سختی ز حد بگذشت کارش</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>بسی بی طاقتی بودش از آن پیش</p></div>
<div class="m2"><p>ولی طاقت نمیآورد از آن بیش</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>برخود خواند خادم را یکی روز</p></div>
<div class="m2"><p>بسوگندش امین کرد آن دل افروز</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>نه چندان خورد سوگند آن وفادار</p></div>
<div class="m2"><p>که هرگز هیچکس باشد روا دار</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>گل آنگه گفت چون سوگند خوردی</p></div>
<div class="m2"><p>دلی با جان من پیوند کردی</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>اگرچه خادمی، مخدوم گشتی</p></div>
<div class="m2"><p>امین چار حدّ روم گشتی</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>کنون چندانکه خواهد بود جانم</p></div>
<div class="m2"><p>تو خواهی بود محرم در جهانم</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>چو القصّه بسی گوی سخن برد</p></div>
<div class="m2"><p>ز اوّل کرد آغاز و ببُن برد</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>دلی پرداشت میگفت آن فسانه</p></div>
<div class="m2"><p>فرو نگذاشت حرفی ازمیانه</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>سخن میگفت و اشک از دیده میریخت</p></div>
<div class="m2"><p>گهی پیدا گهی دزدیده میریخت</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>چو شمعش آتشی بر فرق میشد</p></div>
<div class="m2"><p>ز آب چشم در خون غرق میشد</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>ز چندانی نوازش یاد میکرد</p></div>
<div class="m2"><p>چو چنگی زان نوافریاد میکرد</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>گهی از خون دل افگار میشد</p></div>
<div class="m2"><p>گهی از آه آتشبار میشد</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>چوحال خویش پیش او بیان کرد</p></div>
<div class="m2"><p>ز دل کافور را آتشفشان کرد</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>چنان کافور از آن قصّه عجب ماند</p></div>
<div class="m2"><p>که چون مشک از گل تر خشک لب ماند</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>پر آتش گشت دل زان سرگذشتش</p></div>
<div class="m2"><p>بسی بگریست و آب از سر گذشتش</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>به گلرخ گفت ای چون گل دل افروز</p></div>
<div class="m2"><p>اگر تو نامهیی بنویسی امروز</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>چو بادی نامه را آنجا رسانم</p></div>
<div class="m2"><p>ولیکن چون شدم آنجا بمانم</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>به ترکستان نیارم آمدن باز</p></div>
<div class="m2"><p>که شاه چین بکین من کند ساز</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>چو خسرو گردد از حال تو آگاه</p></div>
<div class="m2"><p>بسازد چارهٔ کارت همانگاه</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>بهرنوعی که داند چاره جوید</p></div>
<div class="m2"><p>خلاص کارت ای مهپاره جوید</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>کنون چون شد دل سرگشته ازدست</p></div>
<div class="m2"><p>مده یکبارگی سر رشته از دست</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>دل خود بازده، دل را بخویش آر</p></div>
<div class="m2"><p>قلم گیر و دوات و نامه پیش آر</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>چو گل دید آن همه آزادی او</p></div>
<div class="m2"><p>بجوش آمد دلش از شادی او</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>بر آن خادم بصد دل مهربان شد</p></div>
<div class="m2"><p>که او را مهربان الحق توان شد</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>از آنسو کرد خادم برگ ره ساز</p></div>
<div class="m2"><p>وزینسو گل بزاری نامه آغاز</p></div></div>
<div class="b" id="bn245"><div class="m1"><p>نوشت آن نامه وزمهرش نشان کرد</p></div>
<div class="m2"><p>به کافور سیه داد و روان کرد</p></div></div>
<div class="b" id="bn246"><div class="m1"><p>کنون بشنو حدیث نامهٔ گل</p></div>
<div class="m2"><p>دمی نظّاره کن هنگامهٔ گل</p></div></div>
<div class="b" id="bn247"><div class="m1"><p>فریدست این زمان بحر معانی</p></div>
<div class="m2"><p>که بروی ختم شد گوهرفشانی</p></div></div>
<div class="b" id="bn248"><div class="m1"><p>ز بس معنی که دارم می ندانم</p></div>
<div class="m2"><p>که هر یک را بهم چون در رسانم</p></div></div>
<div class="b" id="bn249"><div class="m1"><p>چو مویم معنیی گرد ضمیرست</p></div>
<div class="m2"><p>بدستم نرم کردن چون خمیرست</p></div></div>
<div class="b" id="bn250"><div class="m1"><p>چو معنی از ضمیر آرم برون من</p></div>
<div class="m2"><p>چو مویی از خمیر آرم برون من</p></div></div>
<div class="b" id="bn251"><div class="m1"><p>ز بس معنی که پیوندم بهم در</p></div>
<div class="m2"><p>چو زلف دلبران افتد بهم بر</p></div></div>
<div class="b" id="bn252"><div class="m1"><p>چو مویی معنیی در پیش گیرم</p></div>
<div class="m2"><p>بر آن معنی فرا اندیش گیرم</p></div></div>
<div class="b" id="bn253"><div class="m1"><p>چو در معنی سخن پرداز گردم</p></div>
<div class="m2"><p>بسوی نامهٔ گل باز گردم</p></div></div>