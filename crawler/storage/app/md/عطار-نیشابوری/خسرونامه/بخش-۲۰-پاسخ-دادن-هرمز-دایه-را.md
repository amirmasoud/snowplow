---
title: >-
    بخش ۲۰ - پاسخ دادن هرمز دایه را
---
# بخش ۲۰ - پاسخ دادن هرمز دایه را

<div class="b" id="bn1"><div class="m1"><p>چو از دایه سخن بشنود هرمز</p></div>
<div class="m2"><p>چنان شد کان نیارم گفت هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفت ای ز دانش دور مانده</p></div>
<div class="m2"><p>ز غول نفس خود مغرور مانده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نداری شرم با موی چو پنبه</p></div>
<div class="m2"><p>که حلق چون منی برّی بدنبه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز موی همچو پنبه دام کردی</p></div>
<div class="m2"><p>چو مرغی پیش دامم رام کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مساز این پنبه دام مکر و فن را</p></div>
<div class="m2"><p>بنه این پنبه کرباس و کفن را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوانی میکنی در پیش من تو</p></div>
<div class="m2"><p>حساب گور کن ای پیرزن تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بافسونی مرا می بر نشانی</p></div>
<div class="m2"><p>نیم زان دست افسون چند خوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بر من مینهی کاری بصد ناز</p></div>
<div class="m2"><p>نترسی کو فرو افتد ز هم باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو دم میده اگر همدم بماند</p></div>
<div class="m2"><p>تو برهم نه اگر بر هم بماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسالوسی لباسی بر سرم نه</p></div>
<div class="m2"><p>بعشوه پیش پایی دیگرم نه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجازرق تو یابد دست بر من</p></div>
<div class="m2"><p>فسون و زرق نتوان بست بر من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا آهسته میرانی سوی شست</p></div>
<div class="m2"><p>چو صیدی میکشی تا برکشی دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مشو در خون خویش و خون من تو</p></div>
<div class="m2"><p>یکی دیگر گزین بیرون من تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر او نیکوست نیکوکاریش باد</p></div>
<div class="m2"><p>ز نیکوییش برخورداریش باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهرنوعی که هست او آنِ خویشست</p></div>
<div class="m2"><p>خداوندست و در فرمانِ خویشست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا با آن سمنبر نیست کاری</p></div>
<div class="m2"><p>که گل را همنشین باید بهاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کجا درماند از چون من کسی گل</p></div>
<div class="m2"><p>که چون من خار ره دارد بسی گل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه گردم گرد شمع عالم افروز</p></div>
<div class="m2"><p>مرا با گل نه عیدست و نه نوروز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو من پروانهٔ آن دلفروزم</p></div>
<div class="m2"><p>اگر با شمع پرّم پر بسوزم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برو ای پیر جادوی فسون باز</p></div>
<div class="m2"><p>که نتوانی شدن با من فسون ساز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بروای بوالعجب باز سیه پر</p></div>
<div class="m2"><p>که تو گمراه را دیوست همبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برو ای شوم سرداده بتلبیس</p></div>
<div class="m2"><p>که در شومی سبق بردی ز ابلیس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو زین شیوه سخن هرمز فرو خواند</p></div>
<div class="m2"><p>ازودایه چو خر دریخ فرو ماند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بهرمز گفت ای بیشرم آخر</p></div>
<div class="m2"><p>شدی در سرد گویی گرم آخر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مشو گرم ای ز دیده رفته آبت</p></div>
<div class="m2"><p>تو از من به اگر ندهم جوابت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازین صد بازیت بر من اگر من</p></div>
<div class="m2"><p>نیارم بر تو صد بازی دگر من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ببین کار جهان کاین روستایی</p></div>
<div class="m2"><p>دهد درجادویی بر من گوایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چوجادویم نگویم بیش با تو</p></div>
<div class="m2"><p>نمایم جادویی خویش با تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنانت زیر دام آرم بمردی</p></div>
<div class="m2"><p>که بر یک خشت صد گردم بگردی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان گردی اگر بگریزی از دام</p></div>
<div class="m2"><p>که میخوانی خدا را تو بصد نام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مپیما از تهوّر درد بر من</p></div>
<div class="m2"><p>چنین منگر بچشم خُرد بر من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر گردم بلعب و لهو مشغول</p></div>
<div class="m2"><p>سراسیمه شود از مکر من غول</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر بر ره نهم دامی بتلبیس</p></div>
<div class="m2"><p>ز بیم من بتک بگریزد ابلیس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نگویی تو که آخر من کراام</p></div>
<div class="m2"><p>تو گل را باش اگر نه من تراام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدین زودی چنین گشتی تو بامن</p></div>
<div class="m2"><p>نه یکدم همنشین گشتی تو بامن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز گفت دایه هرمز گشت خاموش</p></div>
<div class="m2"><p>نکردش یک سخن را بعد ازان گوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همی چندانکه دایه بیش میگفت</p></div>
<div class="m2"><p>ز گفت دایه هرمز بیش میخفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه خود می دفع کرد از راه خوابش</p></div>
<div class="m2"><p>نداد آن یک سخن آن یک جوابش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو دایه دم نمیزد هرمز از پیش</p></div>
<div class="m2"><p>برون رفت و جدایی داد از خویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چوهرمز رفت دایه بر جگر داغ</p></div>
<div class="m2"><p>برجعت پیش گل آمد ازان باغ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشسته بود گلرخ دیدهها تر</p></div>
<div class="m2"><p>دلی برخاسته دو چشم بر در</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه خون دلش بالا گرفته</p></div>
<div class="m2"><p>کنار او ز خون دریا گرفته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز بی صبری ز دل رفته قرارش</p></div>
<div class="m2"><p>زمین پرخون زچشم سیل بارش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زبان بگشاد کای دایه کجایی</p></div>
<div class="m2"><p>چرا استادگی چندین نمایی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>الا ای دایه آخر دیر کردی</p></div>
<div class="m2"><p>مرا از زندگانی سیر کردی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>الا ای دایه چندینی چه بودت</p></div>
<div class="m2"><p>مگر در راه دیوی در ربودت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>الا ای دایه بس چُستی تو در کار</p></div>
<div class="m2"><p>ترا باید فرستادن بهر کار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>الا ایدایه خوابت در ربودست</p></div>
<div class="m2"><p>و یا در راه آبت در ربودست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>الا ای دایه تا کی اشک رانم</p></div>
<div class="m2"><p>بگو با من که تا جایت بدانم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگو تا این تن آسانیت تاکی</p></div>
<div class="m2"><p>بگو تا این گران جانیت تا کی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چراست ای دایه چندینی قرارت</p></div>
<div class="m2"><p>که خونین شد دلم در انتظارت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرا رمزی ز پیری یادگارست</p></div>
<div class="m2"><p>که سوزی سخت سوز انتظارست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مبادا هیچکس را چشم بر راه</p></div>
<div class="m2"><p>کز و رخ زرد گردد عمر کوتاه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>درآمد دایه گلرخ را چنان دید</p></div>
<div class="m2"><p>رخ گل همچو برگ زعفران دید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگل گفت ای عزیز جان مادر</p></div>
<div class="m2"><p>نبردی پیش ازین فرمان ما در</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چرا آخر چنین شوریده گشتی</p></div>
<div class="m2"><p>ز سر تا پای غرق دیده گشتی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چرا آخر چنین در خون نشستی</p></div>
<div class="m2"><p>ز خون دیده در جیحون نشستی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چرا آخر چنین بیخویش گشتی</p></div>
<div class="m2"><p>ز یکجو صابری درویش گشتی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مرا امروز رسوا کردی ای گل</p></div>
<div class="m2"><p>ز رسواییم پیدا کردی ای گل</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کجادانی تو خود کاین بیوفا مرد</p></div>
<div class="m2"><p>چه ناخوش گفت و با من چه جفا کرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گرفتم طالع آن روستایی</p></div>
<div class="m2"><p>سر بد دارد و برگ جدایی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نه بتوان گفت باتو آنکه گفتم</p></div>
<div class="m2"><p>ندارد برگ گل چندانکه گفتم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از اوّل در وفا میزد دلش جوش</p></div>
<div class="m2"><p>در آخر گشت خشم آلود و خاموش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کنون گر صد سخن برهم بتابم</p></div>
<div class="m2"><p>یکی را باز میندهد جوابم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو دیواری باستادست خاموش</p></div>
<div class="m2"><p>نمیدارد چو دیواری سخن گوش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کجا دیوار را گر گوش بودی</p></div>
<div class="m2"><p>سخن بشنودی و خاموش بودی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>رواست از سنگ گفتار و ازو نه</p></div>
<div class="m2"><p>سخن آید ز دیوار و ازو نه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو سوسن گرچه هرمز ده زبانست</p></div>
<div class="m2"><p>ز گل دارد حیا خاموش از آنست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چنانش یافتم در سرفرازی</p></div>
<div class="m2"><p>که نتوان کرد باوی هیچ بازی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بگفتم صد سخن زرّین و سیمین</p></div>
<div class="m2"><p>نزد یکدم که سگ یامردمست این</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو او بر یاد باغ پادشاهست</p></div>
<div class="m2"><p>سری دارد که بادش در کلاهست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سبک سر بود و چهره زرد کرد او</p></div>
<div class="m2"><p>چو باد از من گذشت و گرد کرد او</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چودایه گفت این و گل شنیدش</p></div>
<div class="m2"><p>چو بادی آتشی در سر دویدش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دو چشم نرگسین او ازین سوز</p></div>
<div class="m2"><p>ز نوک مژه از خون شد جگر دوز</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هزاران اشک خون آلود نوخیز</p></div>
<div class="m2"><p>فرو بارید از مژگان سرتیز</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بدانسان در دلش افتاد جوشی</p></div>
<div class="m2"><p>که پیدا شد زهرمویش خروشی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سر زلف جهان آرای برکند</p></div>
<div class="m2"><p>بدندان پشت دست ازجای برکند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بغایت غصّه میکردش ز هرمز</p></div>
<div class="m2"><p>که باگل این که داند کرد هرگز</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز اشک آتشین مژگانش میسوخت</p></div>
<div class="m2"><p>ز درد ناامیدی جانش میسوخت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>زبان بگشاد و گفت ای دایه زنهار</p></div>
<div class="m2"><p>مشو در خون جان من بیکبار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مگرد از گل جداگر گل جفا کرد</p></div>
<div class="m2"><p>که نتوان پارهیی از خود جدا کرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز دستم رفت دل و ز کار من آب</p></div>
<div class="m2"><p>دلم خون شد مرا ای دایه دریاب</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اگر کار دلم را در نیابی</p></div>
<div class="m2"><p>نشانم از جهان دیگر نیابی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>درین اندوه جان از من برآید</p></div>
<div class="m2"><p>بمیرم تا جهان بر من سر آید</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چون من رفتم گرفتاریت باشد</p></div>
<div class="m2"><p>پشیمانی و خونخواریت باشد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدست خود چوگل را کُشته باشی</p></div>
<div class="m2"><p>چو گل از خون دل آغشته باشی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز گفت گل خروشان گشت دایه</p></div>
<div class="m2"><p>ز تف سینه جوشان گشت دایه</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بگل گفت ای خرد بر باد داده</p></div>
<div class="m2"><p>همانا نیستی تو شاهزاده</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو هرمز شد پی او سخت میدار</p></div>
<div class="m2"><p>ندیدم سست رگ تر از تو در کار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کسی را سر فرود آید بهرمز</p></div>
<div class="m2"><p>نیاید تا سر آن نیز هرگز</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تو دانی آنکه من مردم درین تاب</p></div>
<div class="m2"><p>دگر هرگز نخواهم گفت ازین باب</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بسی گررشتهٔ طبلم بتابی</p></div>
<div class="m2"><p>ز من سررشتهٔ این وانیابی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نخواهم نیز ره پیمود دیگر</p></div>
<div class="m2"><p>بجز کشتن چه خواهد بود دیگر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز گل این خار چون بیرون کنم من</p></div>
<div class="m2"><p>چو گل را می نخواهد چون کنم من</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ترا این برزگر نپسندد آخر</p></div>
<div class="m2"><p>که آبی بر کلوخی بندد آخر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نمیخواهد ترا کار جهان بین</p></div>
<div class="m2"><p>کرا بر گویم آخر درجهان این</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بشد بر تو ز بدنامی جهان تنگ</p></div>
<div class="m2"><p>که من مردن روا دارم ازین ننگ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو تابستان شود زین چشم بی شرم</p></div>
<div class="m2"><p>هوای هرمزت در دل شود گرم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو باغ از برگ ریزان زرد گردد</p></div>
<div class="m2"><p>هوایت بو که آخر سرد گردد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تو ای گلرخ دو لب داری شکر بار</p></div>
<div class="m2"><p>فرو مگذار شیر آخر بیکبار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>تو ای گل مشک داری دام نسرین</p></div>
<div class="m2"><p>مشو درحلقهٔ آن خطّ مشکین</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>برو این بار از گردن بینداز</p></div>
<div class="m2"><p>اگر جانست جان از تن بینداز</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چو میدانی که هرمز هیچکس نیست</p></div>
<div class="m2"><p>چرا از هرمزت پس هیچ بس نیست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>در اوّل دل ربود و برد هوشت</p></div>
<div class="m2"><p>در آخر هم فرو گوید بگوشت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ندارد باتو رونق کار هرمز</p></div>
<div class="m2"><p>نیاید باصلاح این کار هرگز</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چو نیست این کار اسبی تنگ بسته</p></div>
<div class="m2"><p>چه شورآری چو داری تنگ پسته</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو اسبی تنگ بسته مینبینی</p></div>
<div class="m2"><p>دلت گر برنشاند بر نشینی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مرا تو بیخبر گویی دگر بار</p></div>
<div class="m2"><p>بر هرمز شو و از وی خبر آر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو سیمابی بشادی رخ بر افروز</p></div>
<div class="m2"><p>سبویی نیز بر سنگش زن امروز</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چه بر سنگش زنم از عذر تو لنگ</p></div>
<div class="m2"><p>اگر او را همی خواهی سروسنگ</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>مخور زان لب بسی حلوای بی دود</p></div>
<div class="m2"><p>که بر جامه چکانی روغنی زود</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بخوردی لاجرم، شادی برویت</p></div>
<div class="m2"><p>بگیرد استخوانی در گلویت</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>تو تازان لب بماندی خشک دندان</p></div>
<div class="m2"><p>لبت هرگز ندیدم نیز خندان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>گلی نادیده لب از خنده خالی</p></div>
<div class="m2"><p>شده چون بلبلی پر کنده حالی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چگونه کس تواند دید هرگز</p></div>
<div class="m2"><p>که تو هر روز غم بینی ز هرمز</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو در میدان رسوایی فتادی</p></div>
<div class="m2"><p>درین میدان بزن گویی بشادی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>زهی شهزاده کز ننگت چنانم</p></div>
<div class="m2"><p>که میخواهم که در عالم نمانم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>همه شب گل گلاب از چشم میریخت</p></div>
<div class="m2"><p>عرق از روی و اشک از خشم میریخت</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو دایه این سخنها کرد تقریر</p></div>
<div class="m2"><p>گل بی برگ آبی شد ز تشویر</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>زمانی شمع گریان بود بر گل</p></div>
<div class="m2"><p>زمانی صبح خندان بود بر گل</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ز چندان گریهٔ آن ماه دلبند</p></div>
<div class="m2"><p>گهی آن میگرست و گاه این خند</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو بیرون کرد خورشید منوّر</p></div>
<div class="m2"><p>ز زیر قبهٔ نیلوفری سر</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>درآمد آفتاب از برج ماهی</p></div>
<div class="m2"><p>سپیدی ریخت بر روی سیاهی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ز زیر پرده چون چهره نمود او</p></div>
<div class="m2"><p>بنیزه حلهٔ مه در ربود او</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>گل عاشق دل پر تفت و پر سوز</p></div>
<div class="m2"><p>فرو افتاد در تب ده شبانروز</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>دو تاگشت و چنان پر درد شد او</p></div>
<div class="m2"><p>که در ده روز یکتا نان نخورداو</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بشبها درد بیداریش بودی</p></div>
<div class="m2"><p>برو زاندوه بیماریش بودی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>نه یکساعت قرار و نه دمی صبر</p></div>
<div class="m2"><p>دلی چون بحر خون و دیده چون ابر</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ز سوز دل زبانش آتش گرفته</p></div>
<div class="m2"><p>ز تفت عشق جانش آتش گرفته</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>فتاده عکس بر موی از رخ زرد</p></div>
<div class="m2"><p>فسرده اشک بر روی از دم سرد</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>ز چشمش رونق دیدار رفته</p></div>
<div class="m2"><p>زبانش در دهان از کار رفته</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو دایه دید گل را این چنین زار</p></div>
<div class="m2"><p>بگل گفت ای زده در چشم جان خار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چنین تا بر سر آتش نشستی</p></div>
<div class="m2"><p>ز غم بر جان من سیلاب بستی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>زمانی دم زن از گریه مشو گرم</p></div>
<div class="m2"><p>ز یزدان ترس دار آخر ز خود شرم</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بپاسخ گفت گل چون سوکواران</p></div>
<div class="m2"><p>چرا بر خود نگریم همچو باران</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گلم زان زار میگریم چنین من</p></div>
<div class="m2"><p>که دور افتادهام از انگبین من</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>نیی ای دایه ازدرد من آگاه</p></div>
<div class="m2"><p>که چشمم زیر خون دارد وطنگاه</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>نمیدانی که با من چیست هر شب</p></div>
<div class="m2"><p>که چشمم خون دل بگریست هر شب</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>مکن ای دایه زین بیشم مفرسای</p></div>
<div class="m2"><p>جوان و عاشقم بر من ببخشای</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>نمیدانی که در چه درد وداغم</p></div>
<div class="m2"><p>که میجوشد ز خون دل دماغم</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>کنون کاری که بر جان من آمد</p></div>
<div class="m2"><p>بسر در خون مرا در گردن آمد</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چه گر یک درد بی دردی نخوردی</p></div>
<div class="m2"><p>ازین ره کوفتن گردی نخوردی</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ز صد دردم یکی گر بر تو بودی</p></div>
<div class="m2"><p>ز آهت چنبز گردون بسودی</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بسستی چون همی بینی چو مویم</p></div>
<div class="m2"><p>بسختی چند گویی پیش رویم</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>شوی پیشم چو آتش گرم گفتار</p></div>
<div class="m2"><p>چو یخ سردم کنی هر دم درین کار</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>چودل بربود عشق از آستینم</p></div>
<div class="m2"><p>بخواهش کی پذیرد پوستینم</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>اگر خواهم که پنهان دارم این درد</p></div>
<div class="m2"><p>نیارم داشت چون جان دارم این درد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>دل لایعقلم در دست من نیست</p></div>
<div class="m2"><p>که این بی خویشتن با خویشتن نیست</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>زبان را گر کنم از عشق خاموش</p></div>
<div class="m2"><p>چگونه اشک خون بنشانم از جوش</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>چو دوزم جامهیی در عشق دلجوی</p></div>
<div class="m2"><p>سرشک اندازد از دل بخیه برروی</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>مده پندم که پندت بند جانست</p></div>
<div class="m2"><p>نگردد به ز پند این دل نه آنست</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>دل گرمم نگردد سرد ازین درد</p></div>
<div class="m2"><p>مشو گرم و مزن بر آهن سرد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>برو مردی بکن بهرخدا را</p></div>
<div class="m2"><p>ببین بار دگر آن بیوفا را</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>مگر آن سنگ دل دلگرم گردد</p></div>
<div class="m2"><p>ز گرمی همچو مومی نرم گردد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>چو موم از گرمی ار نرمی پذیرد</p></div>
<div class="m2"><p>بگرمی و بنرمی نقش گیرد</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>برو یک ره دگر سنگی درانداز</p></div>
<div class="m2"><p>کلوخ امروز کن دیگر ز سرباز</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>دل گلرخ برون آور ازین کار</p></div>
<div class="m2"><p>مگر چیزی فرو افتد ازین بار</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>بیکباری نیاید کارها راست</p></div>
<div class="m2"><p>بباید کرد ره را بارها راست</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>بیک ضربت نخیزد گوهر از سنگ</p></div>
<div class="m2"><p>بیک دفعت نریزد شکر از تنگ</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>نگردد پخته هر دیگی بیک سوز</p></div>
<div class="m2"><p>نیابد پختگی میوه بیک روز</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>بروزی بیش، مه نتوان قران کرد</p></div>
<div class="m2"><p>حجی نیکو بسالی میتوان کرد</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>برین درباش همچون حلقه پیوست</p></div>
<div class="m2"><p>چو زنجیری مگر در هم زند دست</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چو تخمی را بکشتی بار اوّل</p></div>
<div class="m2"><p>ز بی آبی بمگذارش معطّل</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>مشو زود و رو آبش ده زهرور</p></div>
<div class="m2"><p>که بس نزدیک تخم آید ببردر</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>سخن میگفت تا شب همچنین گرم</p></div>
<div class="m2"><p>که تا شد دایه را دل زان سخن نرم</p></div></div>