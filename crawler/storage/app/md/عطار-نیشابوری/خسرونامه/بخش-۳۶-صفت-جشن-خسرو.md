---
title: >-
    بخش ۳۶ - صفت جشن خسرو
---
# بخش ۳۶ - صفت جشن خسرو

<div class="b" id="bn1"><div class="m1"><p>نشسته شاه رومی همچو جمشید</p></div>
<div class="m2"><p>بسر برافسری روشن چو خورشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزرگان و وزیران معظّم</p></div>
<div class="m2"><p>همه بر پای مانده دست برهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز یک سو نو خطان استاده بر راه</p></div>
<div class="m2"><p>ز یکسو امردان باروی چون ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببردر، امردان دیبای زربفت</p></div>
<div class="m2"><p>سر هریک ز تاب باده پر تفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمر بسته کلاه زر کشیده</p></div>
<div class="m2"><p>بپیش صُفّهها صف برکشیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسر بر، نو خطان تاج مکلّل</p></div>
<div class="m2"><p>کشیده حلقه چون خطّ مسلسل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدست آورده هر یک جام زرّین</p></div>
<div class="m2"><p>چو ماهی کاورد بر دست، پروین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز گلبن تا بگلبن می گرفته</p></div>
<div class="m2"><p>ز رنگ می رخ گل خوی گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سر مجلس بدست پای مردان</p></div>
<div class="m2"><p>پیاله همچو دستنبوی گردان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زده گز بر گلو مرغ مسمّن:</p></div>
<div class="m2"><p>صراحی از میان پر تابگردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خونی، رنگ شیر دختر رز</p></div>
<div class="m2"><p>ولیکن گشته بی شوهر زبان گز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شراب زهرگین شکّر فشانده</p></div>
<div class="m2"><p>زمی مرغ صراحی پر فشانده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صلای باده در یک گوش رفته</p></div>
<div class="m2"><p>ز راه گوش دیگر هوش رفته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخار عود میشد بیست فرسنگ</p></div>
<div class="m2"><p>مشام از مغز کرده دود آهنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخور افگنده در سرها بخاری</p></div>
<div class="m2"><p>ز مشک افتاده در مجلس غباری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هوای شمع روشن، گشته تیره</p></div>
<div class="m2"><p>ز دود عود و از گرد زریره</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بت نوروز رخ چون عید خرّم</p></div>
<div class="m2"><p>مه خورشید فر در زیر شبنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لبالب آب دندان در برابر</p></div>
<div class="m2"><p>پیاپی کرده جام می سراسر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فروغ دامن می آستین سوز</p></div>
<div class="m2"><p>می اندر پوست گشته پوستین دوز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صراحی همچو مرغان سحرخیز</p></div>
<div class="m2"><p>ز مخلب کرده در مجلس شکر ریز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زالحان سرود عاشقانه</p></div>
<div class="m2"><p>شده رّقاص، نقش آستانه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز عکس باده، در جام گهر دار</p></div>
<div class="m2"><p>شده سرمست صورتهای دیوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>می سرکش نشسته در دم چشم</p></div>
<div class="m2"><p>ز مستی پای کوبان مردم چشم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لب شیرین ترکان تروش روی</p></div>
<div class="m2"><p>بنطق تلخ شورانگیز هر سوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فروغ روی چندان حور زاده</p></div>
<div class="m2"><p>جهانی را بهشتی نور داده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز لعل شاهدان آب دندان</p></div>
<div class="m2"><p>شده می همچو گل در جام خندان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شعاع شمع روشن کرده مجلس</p></div>
<div class="m2"><p>سماع جمع جان را گشته مونس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فروغ شمع بر جام اوفتاده</p></div>
<div class="m2"><p>بشب خورشید در دام اوفتاده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز نور شمع شب را روز گشته</p></div>
<div class="m2"><p>جهانی را جهان افروز گشته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو باد صبح در عالم وزیده</p></div>
<div class="m2"><p>حریفان را صبوحی در رسیده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بباد صبح در تختی نهاده</p></div>
<div class="m2"><p>چو آتش جمله در تختی فتاده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپیده دم فسرده زردهٔ شمع</p></div>
<div class="m2"><p>گدازان باد پیه گردهٔ شمع</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مه از خون شفق سر جوش خورده</p></div>
<div class="m2"><p>شب از زرّین طبق سرپوش کرده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خمار اندر خیال می پرستان</p></div>
<div class="m2"><p>ببازی خیال آورده دستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صبوحی را صراحی پر نهاده</p></div>
<div class="m2"><p>ز‌آب تلخ چرب آخر نهاده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حریفان جمله دریاکش نشسته</p></div>
<div class="m2"><p>چو کوهی بر سر آتش نشسته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شده درگوش مرغان صبوحی</p></div>
<div class="m2"><p>چو موسیقار قول بوالفتوحی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قرابه دیده چون خم دستیاری</p></div>
<div class="m2"><p>پیاله کرده از می سنگساری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قدح بر چنگ و برنای عراقی</p></div>
<div class="m2"><p>گرفته راه نی با چنگ ساقی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سبک گشته دل ازتنگی سینه</p></div>
<div class="m2"><p>همه مردان گران از آبگینه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گشاده چار رگ از لب صراحی</p></div>
<div class="m2"><p>شده خون در تن از مستی مباحی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شبی خوش بود و مهتابی دل افروز</p></div>
<div class="m2"><p>قدح مهتاب میپیمود تا روز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بساقی گفت شاه عاشق مست</p></div>
<div class="m2"><p>که می درده که چون گل رفتم ازدست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز می گر شد گران جان سبکبار</p></div>
<div class="m2"><p>گران جانی مکن دستی سبک دار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مرا چندان می خوش ده بزودی</p></div>
<div class="m2"><p>که ماه من شود زیر کبودی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برآرم همچومستان های و هویی</p></div>
<div class="m2"><p>که پیدا نیست هشیاریم مویی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بگفت این و سماع فرد درخواست</p></div>
<div class="m2"><p>زبی خویشی دلش ار درد برخاست</p></div></div>