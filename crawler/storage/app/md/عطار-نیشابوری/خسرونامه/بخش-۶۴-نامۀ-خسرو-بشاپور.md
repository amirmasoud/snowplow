---
title: >-
    بخش ۶۴ - نامۀ خسرو بشاپور
---
# بخش ۶۴ - نامۀ خسرو بشاپور

<div class="b" id="bn1"><div class="m1"><p>بنام آنکه جان را زونشان نیست</p></div>
<div class="m2"><p>خرد را نیز هم یارای آن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگو تا عقل پیش او چه سنجد</p></div>
<div class="m2"><p>چنان ذاتی کجا در عقل گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان معنی که او عقل آفریده</p></div>
<div class="m2"><p>ز مویی گرد ادراکش رسیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه عقل داناست و سخنگوی</p></div>
<div class="m2"><p>نداند در حقیقت کنه یک موی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو عقل جمله در مویست عاجز</p></div>
<div class="m2"><p>بکنه حق که یابد راه هرگز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ذاتش برترست از هرچه دانیم</p></div>
<div class="m2"><p>چگونه شرح او گفتن توانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو جمله عاجزیم از برگ کاهی</p></div>
<div class="m2"><p>ورای عجز، ما را نیست راهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدایی در خداوندی سزاوار</p></div>
<div class="m2"><p>رسولش عیسی خورشید اسرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وزان پس گفت کای شاپورگمراه</p></div>
<div class="m2"><p>که بیرون آمدی در کینهٔ شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سراز فرمان شاه دین کشیدی</p></div>
<div class="m2"><p>خطی در گرد راه دین کشیدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدزدیدی زن شاه زمین را</p></div>
<div class="m2"><p>کنون پای آراگر مردی تو این را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که کرد این فعل هرگز در زمانه</p></div>
<div class="m2"><p>ترا دیدم ببدفعلی یگانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو میدانی که گر من کینه خواهم</p></div>
<div class="m2"><p>نیاری تاب در پیش سپاهم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر لشکر کشم بر کشور تو</p></div>
<div class="m2"><p>نه کشور ماند ونه لشکر تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر یک نیزه آرد بر تو زوری</p></div>
<div class="m2"><p>که گر پیلی بخاک افتی چو موری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر یک مردم آرد روی بر تو</p></div>
<div class="m2"><p>ز نامردی بجنبد موی بر تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو نتوانی تو با ما حرب جویی</p></div>
<div class="m2"><p>نداری حیلتی جز چرب گویی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر با ما درشتی پیش گیری</p></div>
<div class="m2"><p>بکام دشمنان خویش گیری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مکن، گل را کسی کن ورنه ناکام</p></div>
<div class="m2"><p>چو گل غرقه شوی درخون سرانجام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکن، فرمان شاهان خوار مگذار</p></div>
<div class="m2"><p>زگلرخ در ره خود خار مگذار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر فرمان کنی، جان سودبینی</p></div>
<div class="m2"><p>وگرنه جان زیان بس زود بینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غم و شادی و مرگ و زندگانی</p></div>
<div class="m2"><p>بگفتم والسلام اکنون تو دانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو خطّ نامه نوک خامه بنگاشت</p></div>
<div class="m2"><p>درامد پیک و حالی نامه برداشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قدم میزد چو بادی از ره دور</p></div>
<div class="m2"><p>که تافی الجمله شد نزدیک شاپور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدادش نامه و شاپور برخواند</p></div>
<div class="m2"><p>ز خشم آن پیک را حالی بدر راند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نهاد آن پیک مسکین پای در راه</p></div>
<div class="m2"><p>رسید آخر بکم مدّت بدرگاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر خسرو شد وآگاه کردش</p></div>
<div class="m2"><p>حدیث سیرت آن شاه کردش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که آن نامه بدرّید و مرا راند</p></div>
<div class="m2"><p>ترا بدفعل و شوم و باد سر خواند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو شه بشنید ازو برجست ازجای</p></div>
<div class="m2"><p>میان دربست و پس ننشست از پای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپاهی همچو دریا انجمن کرد</p></div>
<div class="m2"><p>جهانی در جهانی موجزن کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپه را جوشن و تیغ و سپر داد</p></div>
<div class="m2"><p>سه ساله جامگی و سیم و زر داد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو مور و چون ملخ چندان سپه بود</p></div>
<div class="m2"><p>که کس رانه گذر بود و نه ره بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نبد چندان زمین از مرد خالی</p></div>
<div class="m2"><p>کزو بالا گرفتی گرد حالی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بسیاری که مرد از جای برخاست</p></div>
<div class="m2"><p>نمیارست گرد ازجای برخاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برامد نالهٔ نای از در شاه</p></div>
<div class="m2"><p>غبار از پای میشد تا سرماه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روان گشتند لشکر تا خراسان</p></div>
<div class="m2"><p>دل شاپور شد زان غم هراسان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کجا دانست کان آفت ز پی داشت</p></div>
<div class="m2"><p>پشیمانی نمود و سود کی داشت</p></div></div>