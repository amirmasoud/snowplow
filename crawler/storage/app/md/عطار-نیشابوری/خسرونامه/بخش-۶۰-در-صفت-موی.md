---
title: >-
    بخش ۶۰ - در صفت موی
---
# بخش ۶۰ - در صفت موی

<div class="b" id="bn1"><div class="m1"><p>الا ای موی مشکین رنگ آخر</p></div>
<div class="m2"><p>شدم مویی نیم از سنگ آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا ای مشکموی افتاده‌ام من</p></div>
<div class="m2"><p>چو موی تو به روی افتاده‌ام من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم چون موی تو در چین نشسته</p></div>
<div class="m2"><p>تو در رومی کمر بر موی بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مویی گر رسم ای دوست با تو</p></div>
<div class="m2"><p>برون آیم چو موی از پوست با تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چومویت مشکبار آمد سفر کن</p></div>
<div class="m2"><p>سحرگه بر صبامویی گذر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا مویی ز حال خود خبر ده</p></div>
<div class="m2"><p>ز مویت مژدهٔ باد سحرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا از خود سر مویی کن آگاه</p></div>
<div class="m2"><p>که چون موی توام افتاده در راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بچشم آمد سر مویی فراقم</p></div>
<div class="m2"><p>چو موی ابرویت، پیوسته طاقم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مویم، در غم آن موی مشکین</p></div>
<div class="m2"><p>بمویی در نمیاید ترا این</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو موی از من بپشت افتادهیی باز</p></div>
<div class="m2"><p>مکن این سرکشی چون مویت آغاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر یک موی تو بینم ز سویی</p></div>
<div class="m2"><p>بسر پیش تو بازآیم چو مویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر یک موی برگویم ز دردم</p></div>
<div class="m2"><p>چو مویی در رباید باد سردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو مویی زان بچشمت در نیایم</p></div>
<div class="m2"><p>که در چشم تو مویی مینمایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو مویی گشتم و چه جای مویست</p></div>
<div class="m2"><p>که از مویی کمم این را چه رویست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تنم گر چه چو مویی مینماید</p></div>
<div class="m2"><p>ولی با تو بمویی درنیاید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو مویی شد تن من از نزاری</p></div>
<div class="m2"><p>بمویی مینیابم از تو یاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بمویی گر ز تو یاریم بودی</p></div>
<div class="m2"><p>چو مویت کی نگونساریم بودی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بمویی دل ده این بیخویشتن را</p></div>
<div class="m2"><p>که قوّت باشد از مویی رسن را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر باشی بمویی دستگیرم</p></div>
<div class="m2"><p>برون آری چو مویی از خمیرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو موی تو به پا افتاده‌ام پست</p></div>
<div class="m2"><p>سر مویی سزد گر بر نهی دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>منم مویی بکوهی غم گرفتار</p></div>
<div class="m2"><p>چنین مویی نگر زیر چنان بار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو مویت کی بتو خواهم رسیدن</p></div>
<div class="m2"><p>که مویی کوه نتواند کشیدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز باریکی، بمویی نیست زورم</p></div>
<div class="m2"><p>که من مویی میان بسته، چو مورم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ره عشق تو باریکست چون موی</p></div>
<div class="m2"><p>چو مویی، من بمویی کردهام روی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز تو مویی نخواهم گشت آگاه</p></div>
<div class="m2"><p>چگونه موی برمویی برد راه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بمویی گر ببندی بندبندم</p></div>
<div class="m2"><p>نیم قادر که مویی برکشندم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تن من گر نه کم از موی بودی</p></div>
<div class="m2"><p>مرا نیروی مویی روی بودی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو مویی شد تنم از ناتوانی</p></div>
<div class="m2"><p>ترا زین موی کی باشد نشانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سر مویی اگر در شانه داری</p></div>
<div class="m2"><p>من آن مویم که داری یا نداری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو مویی کردهام بیتو تن از تو</p></div>
<div class="m2"><p>چو موی تو شکن دارم من از تو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو مویی سرکشی پیوست بر من</p></div>
<div class="m2"><p>فرو بندی بمویی دست بر من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو موی، اینکار را رویی ندارم</p></div>
<div class="m2"><p>که زور بازوی مویی ندارم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بمویی مانم از زاری کنون من</p></div>
<div class="m2"><p>سر مویی ز سر کردم برون من</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر من همچو مویی تن نمایم</p></div>
<div class="m2"><p>چو موی از زیر پیراهن نمایم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو مویی گر بپیراهن برافتم</p></div>
<div class="m2"><p>ز هر مویی بسرگردن برافتم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان زارم که ازمویی بصد روی</p></div>
<div class="m2"><p>بهر مویی که دارم کی برم بوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو مویی بیتو زار و مستمندم</p></div>
<div class="m2"><p>بتو آویخته چون مو ببندم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دلم مویی نپیچد از بَرِتو</p></div>
<div class="m2"><p>بمویش میکشم تا بر دَرِ تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو موی تو دلم را نیست در دست</p></div>
<div class="m2"><p>بمویی مینگردد این دل مست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو از موییست دل شوریدهٔ من</p></div>
<div class="m2"><p>چو مو از سر برون شد دیدهٔ من</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر دربندم آری همچو مویت</p></div>
<div class="m2"><p>چو مویی سر نهم بر خاک کویت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو مویی گر ببرّی سر به هیچم</p></div>
<div class="m2"><p>چو مویت سر ز خطّ تو نپیچم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نپیچم سر ز موی تو به سویی</p></div>
<div class="m2"><p>که دل آویختست از تو به مویی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر چون موی سر پیچد دل از تو</p></div>
<div class="m2"><p>چو مویش بند آید حاصل از تو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ندارم من چو موی تو سَرِ خویش</p></div>
<div class="m2"><p>که چون موی تو میافتم پس و پیش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگر چون موی در تابم کنی هیچ</p></div>
<div class="m2"><p>نپیچم سر چو موی از تاب و از پیچ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو مویت گر دراندازی برویم</p></div>
<div class="m2"><p>نیایم بر تو بیرون همچو مویم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو یک موی توام به از دو عالم</p></div>
<div class="m2"><p>نیارم دید یک مو از سرت کم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو مویی بر نمی‌گیری به هیچم</p></div>
<div class="m2"><p>چگونه همچو مویی بر تو پیچم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گرت از من چو مویی سر نگردد</p></div>
<div class="m2"><p>پس از من با تو مویی در نگردد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو کی باشی چو من چون موی در بند</p></div>
<div class="m2"><p>که با کس نیستت چون موی پیوند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه گر آیی مراچون موی بینی</p></div>
<div class="m2"><p>چو موی مژّه بر چشمم نشینی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نگردد از تو مویی کم اگر تو</p></div>
<div class="m2"><p>سر مویی کنی بر من گذر تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو مویی در سیاهی ماندهام من</p></div>
<div class="m2"><p>ز موی مژّه خون افشاندهام من</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو موی مژّه سرتیزی کن آخر</p></div>
<div class="m2"><p>بمویی قصد خونریزی کن آخر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همی خواهم من سرگشته چون موی</p></div>
<div class="m2"><p>چو موی مژّه با تو روی در روی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گرفته موی تو مست اوفتاده</p></div>
<div class="m2"><p>چو موی مژّه لب بر هم نهاده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز دستم تا برفت آن موی چون شست</p></div>
<div class="m2"><p>چو موی مژّه برهم میزنم دست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر مویی بود باقی ز عالم</p></div>
<div class="m2"><p>رسیم آخر چو موی مژّه باهم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همی بافم هزاران حیله چون موی</p></div>
<div class="m2"><p>مگر مویی ز تو بنمایدم روی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو مویم گر فرود آری بر خویش</p></div>
<div class="m2"><p>چو مویت بر تو اندازم سر خویش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو مویی پیش تو بر فرق آیم</p></div>
<div class="m2"><p>میان خون چو مویی غرق آیم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>منم چو موی بی آن روی مانده</p></div>
<div class="m2"><p>بسی سرگشته تر از موی مانده</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو مویت دور از روی توام من</p></div>
<div class="m2"><p>بسر گردانی موی توام من</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو مویت تا کی اندر بند چینم</p></div>
<div class="m2"><p>چو موی خویش بنشان بر زمینم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر چون موی گرد تو بر آیم</p></div>
<div class="m2"><p>چو می از شادی آن بر سر آیم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>منم مویی شده از عشق رویت</p></div>
<div class="m2"><p>تویی در پایم افگنده چو مویت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو افگندی مرا چون موی در پای</p></div>
<div class="m2"><p>بیار آن موی تا برخیزم از جای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گرم موی تو دست آویز گردد</p></div>
<div class="m2"><p>چو موی این خسته دل سر تیز گردد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>منم یک موی با صد عیش ناخوش</p></div>
<div class="m2"><p>ز بهر تو بهر مویی بلاکش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو مویم بی رخت افتاده در شست</p></div>
<div class="m2"><p>بمانده همچو مویی درهم و پست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تنم بی روی تو مانند مویست</p></div>
<div class="m2"><p>چو مویم بیتو کارم پشت رویست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو مویی هجرت آرد روی بر من</p></div>
<div class="m2"><p>همی با تیغ خیزد موی بر من</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو از موی دو تای تو جدایم</p></div>
<div class="m2"><p>چو مویت مانده با پشت دوتایم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>من چون موی را کس غمخوری نیست</p></div>
<div class="m2"><p>غمم را همچو موی تو سری نیست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سیه بیتو چو مویم عالم از تست</p></div>
<div class="m2"><p>بهر مویی مرا گویی غم از تست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سر موییست جمعیّت ز رویت</p></div>
<div class="m2"><p>ازان بیتو پریشانم چو مویت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو موی افتادهام بر روی پیوست</p></div>
<div class="m2"><p>که می ندهد مرا مویی ز تو دست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>اگر مویی شود پیراهن تو</p></div>
<div class="m2"><p>ندانم بود مویی بر تن تو</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چومویی چند گردانی بخونم</p></div>
<div class="m2"><p>که چون مویی نمیپرسی که چونم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو مویی تا دلم بشکافتی تو</p></div>
<div class="m2"><p>چو موی من ز من سر تافتی تو</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تن من همچو مویی چند داری</p></div>
<div class="m2"><p>چو مویم تابکی در بندداری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو مویم کردی و خونم بخوردی</p></div>
<div class="m2"><p>ولیک از جور مویی کم نکردی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو مویی گشتهام بنمای رویی</p></div>
<div class="m2"><p>ترا کمتر بود این غم بمویی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>منم مویی ره عالم گرفته</p></div>
<div class="m2"><p>تویی مویی ز عالم کم گرفته</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو بی موی تو ای سرکش بماندم</p></div>
<div class="m2"><p>چو مویت پای بر آتش بماندم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز من تا مرگ مویی در میانست</p></div>
<div class="m2"><p>نگه کن درتنم کان موی آنست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>اگرچه همچو مویی ناتوانم</p></div>
<div class="m2"><p>چو موی لقمه بر چشمت گرانم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو موی تو کجا برسر نشینم</p></div>
<div class="m2"><p>که چون مویم نشاندی بر زمینم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>مرا گرچه بمویی راه هم نیست</p></div>
<div class="m2"><p>ز بیداد توام یک موی کم نیست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بر این بیدل بمویی خواب بستی</p></div>
<div class="m2"><p>چو موی خود وفا بر هم شکستی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>وفانیست ازتو مویی روی هرگز</p></div>
<div class="m2"><p>ز ناخن برنیاید موی هرگز</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>وفاناید سر مویی ز سر مست</p></div>
<div class="m2"><p>نیاید موی بیرون از کف دست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>منم مویی که خون گریم زرشکت</p></div>
<div class="m2"><p>ترا خود تر نشد مویی ز اشکت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز سودا پختن تو موی بردم</p></div>
<div class="m2"><p>سر مویی مکن صفرا که مردم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>مکن بر موی صفرا، زین چه خیزد</p></div>
<div class="m2"><p>مکن چون موی ازان صفرا بریزد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>شدم مویی مبادا کینت بامن</p></div>
<div class="m2"><p>که مویی درنگیرد اینت با من</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چراگفتی چو مویی هیچ هیچی</p></div>
<div class="m2"><p>چو مویت تا کی آخر پیچ پیچی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو مویی من نیم باتو بزاری</p></div>
<div class="m2"><p>چو مویی بر زمینم کش بخواری</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چو مار موی پیچانت ای سبک روح</p></div>
<div class="m2"><p>دلم را کرد از یک موی مجروح</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو مویی گشتم از رنج جراحت</p></div>
<div class="m2"><p>چگونه موی داند رنج و راحت</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ز تو قسمم بود مویی وفا بوک</p></div>
<div class="m2"><p>که تا دارم چو موی قندزت سوک</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>اگر یابم بموی قندزت راه</p></div>
<div class="m2"><p>خوشت در برکشم چون موی روباه</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>اگر چون موی شد روز سپیدم</p></div>
<div class="m2"><p>سر مویی بتو ماندست امیدم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>سر من گوی کن ای مشکمویم</p></div>
<div class="m2"><p>که مویت بس بود چوگان گویم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>اگر سر درکشم زآن موی در چشم</p></div>
<div class="m2"><p>سر من باز بر چون موی بر چشم</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو هجرت کرد چون مویی نزارم</p></div>
<div class="m2"><p>ز تو بس باد مویی یادگارم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>اگر دارم بمویی بیتو رویی</p></div>
<div class="m2"><p>هنوزم چون جنب خشکست مویی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>یکی کردم دو مویت ز ارزویت</p></div>
<div class="m2"><p>که تا با تو یکی گردم چو مویت</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو موی آوردهیی با هیچم آخر</p></div>
<div class="m2"><p>چو موی تو بتو برپیچم آخر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>اگر درچاهم و موی تو بر ماه</p></div>
<div class="m2"><p>بسم مویت رسن ای یوسف چاه</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>وگر بر ماهی و موی تو برخاک</p></div>
<div class="m2"><p>بست مویی کند ای عیسی پاک</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ز مویی نیست گفتن پیش ازین روی</p></div>
<div class="m2"><p>که درتو مینگیرد یکسر موی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چو یک مویم من از صد روی بیتو</p></div>
<div class="m2"><p>چو صفرا میشکافم موی بیتو</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>ز من تا موی تو چون موی سرتافت</p></div>
<div class="m2"><p>دلم در شرح مویت موی بشکافت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بهرمویی گرم بودی زبانی</p></div>
<div class="m2"><p>نبودی هیچ موی بی فغانی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>بدین هر بیت مویی میشکافم</p></div>
<div class="m2"><p>که در هر بیت مویی میببافم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو در باریکی یک تار مویم</p></div>
<div class="m2"><p>سخن باریکتر از موی گویم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>سخن میراند ازمویی بصد روی</p></div>
<div class="m2"><p>بیا گر میببینی موی در موی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو من مویی شدم در نوک خامه</p></div>
<div class="m2"><p>پریشان شد چو مویی خط نامه</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ز تو چون نیست مویی حصّهٔ من</p></div>
<div class="m2"><p>چو موی تو دراز این قصّهٔ من</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>سر مویی امیدم گر نبودی</p></div>
<div class="m2"><p>مرازان سر چو موی این سر نبودی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>درامیدت تنم چون موی پیوست</p></div>
<div class="m2"><p>سر مویی فرو نگذارد از دست</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>میان ما اگر یک موی ماندست</p></div>
<div class="m2"><p>چو موی این کار را صد روی ماندست</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>گسسته کی شود این موی هرگز</p></div>
<div class="m2"><p>خود این مویی نداردروی هرگز</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>میان ماست مویی در میانه</p></div>
<div class="m2"><p>میان تست آن موی ای یگانه</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>میان ما فلک مویی بسنجد</p></div>
<div class="m2"><p>که گر خود موی گردد در نگنجد</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چو مویت هرکه او زیر و زبر نیست</p></div>
<div class="m2"><p>ازین سرّش سر مویی خبر نیست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ز مویی چند گویم بیش ازین نیز</p></div>
<div class="m2"><p>که ازمویی نیاید بیش ازین چیز</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>بسی گفتم ز موی ایماه اکنون</p></div>
<div class="m2"><p>بسر بردم چو مویت راه اکنون</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بسی گفتم ز موی مشکبارت</p></div>
<div class="m2"><p>بیک یک موی، صد صد جان نثارت</p></div></div>