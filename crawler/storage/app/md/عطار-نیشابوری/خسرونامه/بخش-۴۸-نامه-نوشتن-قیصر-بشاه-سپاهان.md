---
title: >-
    بخش ۴۸ - نامه نوشتن قیصر بشاه سپاهان
---
# بخش ۴۸ - نامه نوشتن قیصر بشاه سپاهان

<div class="b" id="bn1"><div class="m1"><p>بگفت این و دبیری را بفرمود</p></div>
<div class="m2"><p>که کلکش از عطارد گوی بربود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دبیر شاه چون بگرفت خامه</p></div>
<div class="m2"><p>بنام حق مزین کرد نامه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوندی که دور از چند و چونست</p></div>
<div class="m2"><p>دو عالم را بکلّی رهنمونست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانداری که این چرخ کهن ساخت</p></div>
<div class="m2"><p>خرد را دایهٔ طفل سخن ساخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکوکاری که عالم کرد موجود</p></div>
<div class="m2"><p>که درعالم نبودش هیچ مقصود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز او اندر حقیقت دیگری نیست</p></div>
<div class="m2"><p>رهش را حدّ و ملکش را سری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان از ظلّ فضلش را نجاتست</p></div>
<div class="m2"><p>سر مویی ز فضلش کایناتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زانجم، شمع جان افروز آرد</p></div>
<div class="m2"><p>گهی شب را برد، گه روز آرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنی، شکّر، زتود اطلس نگارد</p></div>
<div class="m2"><p>ز کس، ناکس، زناکس، کس برآرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسی در وصف او تصنیف کردند</p></div>
<div class="m2"><p>بسی با یکدگر تعریف کردند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزاران قرن میکردند فکرت</p></div>
<div class="m2"><p>بآخر با سرامد عجز و حیرت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازان پس گفت عیسی را ثنایی</p></div>
<div class="m2"><p>مسیحی، پاک روحی، پاک رایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان ای شاه سر از خط کشیده</p></div>
<div class="m2"><p>که در روی زمین هیچ آفریده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندارد تاب کین ما زمانی</p></div>
<div class="m2"><p>که مینازند از مهرم جهانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنسل شاه ذوالقرنین ماییم</p></div>
<div class="m2"><p>شه و شهزادهٔ ثقلین ماییم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو دانی پایگاه ما که چندست</p></div>
<div class="m2"><p>فلک نرسد بما گرچه بلندست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دران میدان که آنجا جنبش ماست</p></div>
<div class="m2"><p>فلک چون گوی، سرگردان آنجاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منم شاهی که خورشیدم نگینست</p></div>
<div class="m2"><p>چه جای ملکت روی زمینست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر خشمی برانم، دوزخ آنجاست</p></div>
<div class="m2"><p>شود آبی و گردد، چون یخ آنجاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکن، خود را ز خسرو خشم مرسان</p></div>
<div class="m2"><p>سپاهان را چو سرمه چشم مرسان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روان کن آن سمنبر را بر من</p></div>
<div class="m2"><p>بترس از دارو گیر لشگر من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که گردی بیقرار از تو برآریم</p></div>
<div class="m2"><p>کم از یکدم دمار از تو برآریم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو نامه سر بمهر خسروان شد</p></div>
<div class="m2"><p>بدست پیک دادند و روان شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روان شد پیک خوش رو تا سپاهان</p></div>
<div class="m2"><p>بقصر شاه آمد صبحگاهان</p></div></div>