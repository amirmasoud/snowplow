---
title: >-
    بخش ۹ - در فضیلت امام ابوحنیفه
---
# بخش ۹ - در فضیلت امام ابوحنیفه

<div class="b" id="bn1"><div class="m1"><p>جهان را هم امام و هم خلیفه</p></div>
<div class="m2"><p>کِرا میدانی الّا بوحنیفه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان علم و دریای معانی</p></div>
<div class="m2"><p>امام اوّل و لقمان ثانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر اعدای دین بسیار جمعند</p></div>
<div class="m2"><p>ز کار بوحنیفه سر چو شمعند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ امّت آمد آن سرافراز</p></div>
<div class="m2"><p>چراغی کو عدو را مینهد گاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قضا کردند بر وی عرضه ناگاه</p></div>
<div class="m2"><p>بنپذیرفت یعنی جان آگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا را و قدر را معتبر یافت</p></div>
<div class="m2"><p>ولیکن این قضا اندر قدر یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نعمان سرخ روی حق چو لالهست</p></div>
<div class="m2"><p>قضا چکند بشاگردش حوالهست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قضا در جنگ او آمد فروتر</p></div>
<div class="m2"><p>که از یوسف همه چیزی نکوتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو تو یوسف قضا را این زمان بس</p></div>
<div class="m2"><p>مرا قاضی اکبر جاودان بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چودر دین محمّد داد دین داد</p></div>
<div class="m2"><p>محمّد را چنین بود و چنین داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو او استاد دین آمد در اسرار</p></div>
<div class="m2"><p>چو تو بگذشتی از قرآن و اخبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر در فقه صد جامع کبیرست</p></div>
<div class="m2"><p>ز یک شاگردش آن جامع صغیرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مجرّد شو اگر کوفی شعاری</p></div>
<div class="m2"><p>برافشان چون الف چیزی که داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ره کوفیت میباید روان شو</p></div>
<div class="m2"><p>الف دانی تو باری همچنان شو</p></div></div>