---
title: >-
    بخش ۳۵ - عشرت كردن گل و خسرو باهم
---
# بخش ۳۵ - عشرت كردن گل و خسرو باهم

<div class="b" id="bn1"><div class="m1"><p>شبی خوشتر ز نوروز جوانی</p></div>
<div class="m2"><p>می صافی چو آب زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل و خسرو فتاده هر دو سرمست</p></div>
<div class="m2"><p>بکش آورده پای و زلف در دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیه پوشیده زلف شبروگل</p></div>
<div class="m2"><p>شده شب همچو روز از پرتو گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخ چون روز آن دُرّ شب افروز</p></div>
<div class="m2"><p>شب تاریک روشن کرده چون روز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین از بوی مویش مشک خورده</p></div>
<div class="m2"><p>شکر با قند او لب خشک کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخوزستان شکر از خندهٔ او</p></div>
<div class="m2"><p>تبرزد در سپاهان بندهٔ او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل سیرابش آتش درگرفته</p></div>
<div class="m2"><p>لبش خندان و زلفش برگرفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهانی دل فتاده خرقه او</p></div>
<div class="m2"><p>خرد در گوش کرده حلقه او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو سروی ماه گلرخ سر فگنده</p></div>
<div class="m2"><p>مهی در سر، سری در برفگنده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر زلفش ز مشک تر زده آب</p></div>
<div class="m2"><p>ز تاب روی گل گشته سیه تاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدح در حلق گل گشته شفق ریز</p></div>
<div class="m2"><p>شده گل چون قدح ازمی عرق ریز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو گلرخ برقع از رخ برگرفتی</p></div>
<div class="m2"><p>ز خجلت باغ، زاری در گرفتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگر شعر سیه بر سر فگندی</p></div>
<div class="m2"><p>مه و خورشید را درسر فگندی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگر آن نازنین با جام بودی</p></div>
<div class="m2"><p>بگردون بر،‌نفیر عام بودی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکر از لعل گل در یوزه گر بود</p></div>
<div class="m2"><p>بنفشه خرقه پوش آن شکر بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمی رنگ رخ آن ماه میتافت</p></div>
<div class="m2"><p>بتنهایی همه بر شاه میتافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو برخاست از نشست مسند آن ماه</p></div>
<div class="m2"><p>دل خلوت نشین برخاست از راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گل سرمست چون برخاست از جای</p></div>
<div class="m2"><p>شهش گفت اوفتادم مست درپای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو برخیزی تو، بنشیند سلامت</p></div>
<div class="m2"><p>چو بنشینی تو، برخیزد قیامت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل خسرو بخون پیوستهٔ تست</p></div>
<div class="m2"><p>ازانم همچو خون دل بستهٔ تست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک امشب ده بدست خود شرابم</p></div>
<div class="m2"><p>کز آبادی حسنت بس خرابم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هوای دست یازی دارم امشب</p></div>
<div class="m2"><p>سرگردن فرازی دارم امشب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو زلف چون دو هندوی زره پوش</p></div>
<div class="m2"><p>منه در ترکتازی بر سر دوش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمی با من می گلرنگ در کش</p></div>
<div class="m2"><p>مباش ای سیمبر چون زلف سرکش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفت این وز لعلش گلشکر ساخت</p></div>
<div class="m2"><p>دو دست خویش در گردش کمر ساخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز رشک شه فغان برخاست از ماه</p></div>
<div class="m2"><p>که شاهد بود شاهد بازی شاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گل تر هندوی زلفش ببازی</p></div>
<div class="m2"><p>درآورده بدست ترکتازی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گهر بر نطع و رخ بر شه فگنده</p></div>
<div class="m2"><p>شکر برگل قصب بر مه فگنده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>میی بستد ز دست شاه گلروی</p></div>
<div class="m2"><p>میی چون روی او گلرنگ و گلبوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شکر میریخت، نازی تلخ میکرد</p></div>
<div class="m2"><p>بشیرینی شرابی تلخ میخورد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بشکّر شیر رز را بوسه میداد</p></div>
<div class="m2"><p>بجرعه خاک را سنبوسه میداد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زبان بگشاد خسرو شاه سرمست</p></div>
<div class="m2"><p>بگل گفت ای ز مستی رفته از دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو تو مستی ز لب می ده بمستان</p></div>
<div class="m2"><p>دمی بنشین که در خوابند مستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که خواهد یافتن زین به زمانی</p></div>
<div class="m2"><p>سر سَر دِه بده در ده زمانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مکن دل ناخوش از قلّاشی ما</p></div>
<div class="m2"><p>دمی خوش باش،‌در خوش باشی ما</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بزاری میسراید مرغ شبگیر</p></div>
<div class="m2"><p>بیار ای مرغ زرّین نالهٔ‌زیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو گلرخ پاسخ شه یافت برجست</p></div>
<div class="m2"><p>بخدمت پیش شه شد باده بردست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز قدّش سروبن تشویر میخورد</p></div>
<div class="m2"><p>ز چشمش نرگس تر تیر میخورد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو سرو آزاد کرد قدّ اوبود</p></div>
<div class="m2"><p>مقر آمد بپیش قدّ او زود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فشانان آستین میشد بر شاه</p></div>
<div class="m2"><p>گریبانش گرفته دامنماه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بمشکین زلف روی حضرتش رفت</p></div>
<div class="m2"><p>مبارکباد عید عشرتش گفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شه و گل مانده با هم هر دو تنها</p></div>
<div class="m2"><p>بمستی گام زن گرد چمنها</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مشام از بوی می پر مشک کرده</p></div>
<div class="m2"><p>ببوسه هر دو لب تر خشک کرده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز می بیخود دل خونخوارهٔ‌گل</p></div>
<div class="m2"><p>فروزان آتش از رخسارهٔ گل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو شد از می گل لعلش در آتش</p></div>
<div class="m2"><p>ز می شد گفتهیی نعلش درآتش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز شوق سرخی رخسارهٔ ماه</p></div>
<div class="m2"><p>سیه شد دیدهٔ‌خونخوارهٔ شاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر گل رفت شه دستی بدل بر</p></div>
<div class="m2"><p>که تا با گل کند دستی بگل در</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گلش گفت ای دگر ره گشته بیداد</p></div>
<div class="m2"><p>مرا از دست بیداد تو فریاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ترا بر من چو حاصل باقیی نیست</p></div>
<div class="m2"><p>بگیر این می که چون گل ساقیی نیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دگر ره بازم آوردی عتابی</p></div>
<div class="m2"><p>نماندت گوییا باقی حسابی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز چشمت مستم و از باده هم مست</p></div>
<div class="m2"><p>بدار آخر ازین مست دژم دست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دل گلرخ ز نرگس پاره داری</p></div>
<div class="m2"><p>که تو خود نرگس این کاره داری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خطی چون مشک عنبر ناک داری</p></div>
<div class="m2"><p>سخن چون زهر و لب تریاک داری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مکن چندین بشهوت میل، حالی</p></div>
<div class="m2"><p>که شهوت بگذرد چون سیل، حالی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ازان چیزی که یک دم بیش نبود</p></div>
<div class="m2"><p>اگر نوشی بود جز نیش نبود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رها کن شهوت اندر ذوق مستی</p></div>
<div class="m2"><p>زمانی دور شو از هرچه هستی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو گشتی مست، با گل کن دمی گشت</p></div>
<div class="m2"><p>بگرد مفرش زنگار گون دشت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز عالم دلخوشی داری جهانی</p></div>
<div class="m2"><p>چو گل خوش باش از عالم زمانی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شه از گفتار گل از دست شد مست</p></div>
<div class="m2"><p>ز پا افتاد مست و رفت از دست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دلش بیهوش شد از خواب نوشین</p></div>
<div class="m2"><p>که دل پرجوش داشت ازتاب دوشین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو طفل صبح بر روی زمانه</p></div>
<div class="m2"><p>برانداخت از دهن شیرشبانه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو طفلان دست از بر تنگ بگشاد</p></div>
<div class="m2"><p>جُلیل از چهرهٔ گلرنگ بگشاد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سر از گهوارهٔ گردون بدر کرد</p></div>
<div class="m2"><p>بخندید و جهانی پر شکر کرد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو طاوس عروس خضر خضرا</p></div>
<div class="m2"><p>علم زد بر سر این چتر مینا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>برامد از حمل چون چتر زربفت</p></div>
<div class="m2"><p>جهان را سال سیم افشان بسر رفت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو این طاوس زرّین در حمل شد</p></div>
<div class="m2"><p>زمانه با زر رکنی بدل شد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همه کهسارها از لاله جوشید</p></div>
<div class="m2"><p>همه صحرا ز سبزه حلّه پوشید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو گفتی ذرّه ذرّه خاک صحرا</p></div>
<div class="m2"><p>نهفت از سبزه زیر گرد خضرا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هوا را آب خضر از سر درآمد</p></div>
<div class="m2"><p>زمین را گنج قارون با سرآمد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چمن از دست گل پیمانه میخورد</p></div>
<div class="m2"><p>صبا هر شاخ را سر، شانه میکرد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کنار جوی از سبزه سپر بست</p></div>
<div class="m2"><p>میان کوه از لاله کمربست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چمن گفتی دبیرستان همی داشت</p></div>
<div class="m2"><p>که چندین طفل در بستان همی داشت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هزاران گل چو طفلان نوشکفته</p></div>
<div class="m2"><p>ز برگ سبز، لوحی بر گرفته</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>صبا چون جلوهشان دادی بصدروح</p></div>
<div class="m2"><p>نهادی هر یکی انگشت بر لوح</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جهان پیرانه سر گفتی جوان شد</p></div>
<div class="m2"><p>زمین از سبزه همچون آسمان شد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هزاران لعبت زنگار جامه</p></div>
<div class="m2"><p>شدند از شاخها پرّان چو نامه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هزاران طرفه جادوی کرشمه</p></div>
<div class="m2"><p>شده شینابگر بر روی چشمه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هزاران تیز چشم سرمه کرده</p></div>
<div class="m2"><p>برون میآمدند از زیر پرده</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هزاران طفل خرد شیرخواره</p></div>
<div class="m2"><p>برآورند سر از گاهواره</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بزاری عندلیب از گل سخنجوی</p></div>
<div class="m2"><p>گل از گهواره چون عیسی سخنگوی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ز شاخ سرو طوطی شکرخوار</p></div>
<div class="m2"><p>برآورده فغان از شکریار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چمن از هر طرف چون نخل بندان</p></div>
<div class="m2"><p>نموده لعبتان آب دندان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چمن مرواحه ساز از پرّ طاوس</p></div>
<div class="m2"><p>شده روح صبا چون روح محبوس</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چمن از دست گل سر جوش خورده</p></div>
<div class="m2"><p>مسطّح حلقهها در گوش کرده</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به دم مشّاطهٔ باد سحرگاه</p></div>
<div class="m2"><p>زده بر نوعروسان چمن راه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>صبای تند در عالم دمیده</p></div>
<div class="m2"><p>جُلیل سبز گل، در هم دریده</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سه لب کرده دو لب خندان بیکبار</p></div>
<div class="m2"><p>لب کشت و لب جوی و لب یار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>جهان جانفروز افروخته شمع</p></div>
<div class="m2"><p>بهشتی حلّه پوش از هر سویی جمع</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو سنبل خاک را زنجیر مویی</p></div>
<div class="m2"><p>چو سوسن باغ را آزاده رویی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز روی کوه لاله خنجر افراز</p></div>
<div class="m2"><p>ز جرم ابر ژاله ناوک انداز</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بنفشه خرقهٔ‌فیروزه در بر</p></div>
<div class="m2"><p>گل زرد افسر زر بفت بر سر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بنفشه جلوه کرده پرّ طاوس</p></div>
<div class="m2"><p>شکوفه در نثار و در زمین بوس</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بنفشه سر گران از بس خرابی</p></div>
<div class="m2"><p>کشیده لاله در خارا عتابی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بنفشه طفل بود از ناتوانی</p></div>
<div class="m2"><p>ولی نامد ازو رنگ جوانی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بنفشه بر مثال خرقه پوشان</p></div>
<div class="m2"><p>سرآورده بزانو چون خموشان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بنفشه خرقه میپوشد بطامات</p></div>
<div class="m2"><p>ولی نیلوفرست اهل کرامات</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>که نیلوفر چو نیلی پوش اصحاب</p></div>
<div class="m2"><p>سجاده بازافگندست برآب</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو آب از باد نوروزی گره یافت</p></div>
<div class="m2"><p>ز روی آب، نیلوفر زره یافت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو خورشیدش بتفت و تاب افگند</p></div>
<div class="m2"><p>زره برداشت سپر بر آب افگند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>برامد ارغوان همچون تبرخون</p></div>
<div class="m2"><p>فرو میریخت گفتی از جگر، خون</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>سراسر پیرهن در خون کشیده</p></div>
<div class="m2"><p>زبانها از قفا بیرون کشیده</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>تنش در دام، کافورنهانی</p></div>
<div class="m2"><p>شده چون پنبه، مویش در جوانی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چه، کز روز جوانی پیر میزاد</p></div>
<div class="m2"><p>ولی کش ابرهر دم شیرمیداد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو چشم چشمهها گرینده شد باز</p></div>
<div class="m2"><p>دهان یاسمین از خنده شد باز</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو شد خندان، پدید آمد زبانش</p></div>
<div class="m2"><p>زبان بگشاد و پیدا شد دهانش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>برامد لاله همچون عود و مجمر</p></div>
<div class="m2"><p>برون آتش، درون عود معنبر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بسان شعلهٔ‌آتش بر افروخت</p></div>
<div class="m2"><p>بران آتش دلش چون عود میسوخت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>درآمد پای کوبان بلبل مست</p></div>
<div class="m2"><p>که گل درجلوه میآید بصد دست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو بلبل بر سر گل نوحه گر شد</p></div>
<div class="m2"><p>گل از پیکان برون آمد سپر شد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>همی کز مهد زنگاری جدا شد</p></div>
<div class="m2"><p>بیک شبنم کلاه او قبا شد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>گل نازک چو در دست صبا ماند</p></div>
<div class="m2"><p>برای دفع او بر خود دعا خواند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چوگل خواندی دعابستان شنیدی</p></div>
<div class="m2"><p>صبا ازدم دعا را در دمیدی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چوشد پیکان گل از خون دل، پر</p></div>
<div class="m2"><p>کف ابرش نثاری کرد از دُر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چو دُر بستد، نمود از کف زر زرد</p></div>
<div class="m2"><p>بمرد باغبان گفت از سر درد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>که زر بستان و دُر از ناتوانی</p></div>
<div class="m2"><p>مرا یک هفته ده آخر امانی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بآخر مرد، آن دُرّو زر ساو</p></div>
<div class="m2"><p>نه زر بستد نه دادش هفتهیی داو</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چو گل در بار کم عمری فتادی</p></div>
<div class="m2"><p>بزاری بانگ بر قمری فتادی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>ز گل قمری خوش الحان همی خواند</p></div>
<div class="m2"><p>همه شب ق و القرآن همی خواند</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو موسیقار درس عاشقان گفت</p></div>
<div class="m2"><p>چو موسی بلبل عاشق برآشفت</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ز مستی طوف در گلزار میکرد</p></div>
<div class="m2"><p>همه شب آن سبق تکرار میکرد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>زبان بگشاد چون داود بلبل</p></div>
<div class="m2"><p>زبور عشق خود، میخواند بر گل</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو گل از صد زبان تکبیر گفتی</p></div>
<div class="m2"><p>ز گلبن فاخته تفسیر گفتی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>همه شب فاخته میگفت یاحی</p></div>
<div class="m2"><p>من از سر شاخ طوبی دورتاکی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چوسار از سرو گفتی سرگذشتی</p></div>
<div class="m2"><p>صریر نعره زن از سر گذشتی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چو یک بلبل ز شاخ آواز دادی</p></div>
<div class="m2"><p>دگر بلبل جوابش بازدادی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>بنعره بلبلان دُردانه سفتند</p></div>
<div class="m2"><p>همه شب تا بروز افسانه گفتند</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>ز یک یک شاخ بانگ چنگ برخاست</p></div>
<div class="m2"><p>هزاران مرغ رنگارنگ برخاست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ندانم تا کرا باغی چنان بود</p></div>
<div class="m2"><p>وگر بود آنچنان بر آسمان بود</p></div></div>