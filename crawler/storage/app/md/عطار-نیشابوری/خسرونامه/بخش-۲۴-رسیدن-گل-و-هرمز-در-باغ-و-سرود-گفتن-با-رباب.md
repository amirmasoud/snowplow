---
title: >-
    بخش ۲۴ - رسیدن گل و هرمز در باغ و سرود گفتن با رباب
---
# بخش ۲۴ - رسیدن گل و هرمز در باغ و سرود گفتن با رباب

<div class="b" id="bn1"><div class="m1"><p>چو دایه آن دو دلبر را چنان دید</p></div>
<div class="m2"><p>دو جان هر دو بیرون ازجهان دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگل گفت ای چمن پرنور از تو</p></div>
<div class="m2"><p>دماغ بلبلان مخمور از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قمر را روی تو تشویر داده</p></div>
<div class="m2"><p>شکر را پستهٔ تو شیر داده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بی عقلی ز سر تا پای رفتی</p></div>
<div class="m2"><p>چو اینجا آمدی از جای رفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان باغ آخر خیز ای گل</p></div>
<div class="m2"><p>ز مستی ماندهیی مستیز ای گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا هر جایگه راییست دیگر</p></div>
<div class="m2"><p>ولی هر کار را جاییست دیگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل عاشق ز گفت دایهٔ پیر</p></div>
<div class="m2"><p>عرق میریخت چون باران ز تشویر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بآخر از کنار راه برجست</p></div>
<div class="m2"><p>بعشرت بر میان جان کمر بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفته بود دست دایه در دست</p></div>
<div class="m2"><p>بدیگر دست دست هرمز مست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان باغ میشد در میانه</p></div>
<div class="m2"><p>یکی زانسو یکی زینسو روانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکنج باغ در، خلوتگهی بود</p></div>
<div class="m2"><p>که آن در خورد خورشید و مهی بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قران کردند مهر و ماه با هم</p></div>
<div class="m2"><p>بدان برج آمدند از راه باهم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشستند و میآوردند حالی</p></div>
<div class="m2"><p>دو دل پر آرزو و جای خالی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازان مجلس چو دوری چند برگشت</p></div>
<div class="m2"><p>فلک در دور ازان خوشی بسر گشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو هرمز مست شد برداشت رودی</p></div>
<div class="m2"><p>بگفت از پردهٔ رازی سرودی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بزاری زخمه را میخست بر رود</p></div>
<div class="m2"><p>ز خون دیده پل میبست بر رود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو‌آب زر ز ابریشم فرو ریخت</p></div>
<div class="m2"><p>دل از ابریشم هر مژّه خون ریخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرودی گفت هرمز کای دلارام</p></div>
<div class="m2"><p>جهان چون جانستان آمد بده جام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آتش آب در ده کاسهیی زود</p></div>
<div class="m2"><p>که عمر از کیسهٔ مارفت چون دود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیاپی ده می کهنه بنوروز</p></div>
<div class="m2"><p>که دل پر عشق دارم سینه پر سوز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیار آن آب چون آتش زمانی</p></div>
<div class="m2"><p>که نیست از دی و از فردا نشانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو ریزان شد شکوفه از درختان</p></div>
<div class="m2"><p>میی در ده چو روی نیکبختان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیا تا بانگ جوی آب بینی</p></div>
<div class="m2"><p>شکوفه بینی و مهتاب بینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسی چادر کشد اشکوفهٔ پاک</p></div>
<div class="m2"><p>کشیده ما بچادر روی برخاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>می سر جوش را در ده صلایی</p></div>
<div class="m2"><p>که دردمانه سر دارد نه پایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفت این وز عشق روی دلبر</p></div>
<div class="m2"><p>بسر میگشت و خون میکرد از بر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جوان و مست و عاشق در چنین حال</p></div>
<div class="m2"><p>دلی بس پر سخن لیکن زبان لال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین جایی کسی با دل نماند</p></div>
<div class="m2"><p>که چه دیوانه چه عاقل نماند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیامد دایه و بر گل زد آبی</p></div>
<div class="m2"><p>شد آن آب از رخ گل چون گلابی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گل بی خویشتن از عشق و مستی</p></div>
<div class="m2"><p>درامد از هوای می پرستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بصحن باغ شد با دلبر خویش</p></div>
<div class="m2"><p>ز نرگس کرد پرخون زیور خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صبا از قحف لاله جرعه میخورد</p></div>
<div class="m2"><p>چمن چون نوعروسی جلوه میکرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز یکیک برگ نقاشان فطرت</p></div>
<div class="m2"><p>نموده خرده کاریهای قدرت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عروسان چمن برقع گشاده</p></div>
<div class="m2"><p>هزاران بچهٔ بی شوی زاده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چمن درخاصیت چون مریم آمد</p></div>
<div class="m2"><p>که فرزند چمن عیسی دم آمد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو بسراینده شد آن سرو آزاد</p></div>
<div class="m2"><p>برقص افتاد گل چون شاخ شمشاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گل و بلبل همه شب راز گفتند</p></div>
<div class="m2"><p>حدیث عشقبازی باز گفتند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جوانی بود و مستی و بهاری</p></div>
<div class="m2"><p>جهان ایمن زهی خوش روزگاری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گل و هرمز بهم انباز گشته</p></div>
<div class="m2"><p>ز خون شیشه سنگ انداز گشته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدستی زلف گل آورده در چنگ</p></div>
<div class="m2"><p>بدستی خورده می از جام گلرنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو لختی طوف کردند آن دو دلجوی</p></div>
<div class="m2"><p>بخلوتگاه رفتند از لب جوی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بی صبری دل هرمز همی جست</p></div>
<div class="m2"><p>که تا با گل مگر درکش کند دست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بنقدی وصل شیرودنبه میدید</p></div>
<div class="m2"><p>بران آتش دل چون پنبه میدید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درآمد همچو مرغی سوی دنبه</p></div>
<div class="m2"><p>بچربی دایه را میکرد پنبه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو آگه شد زبان بگشاد دایه</p></div>
<div class="m2"><p>که مارا نیست بر سالوس پایه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو مویم پنبه شد در پنبه کردن</p></div>
<div class="m2"><p>مرا پنبه مکن در دنبه خوردن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو پنبه تا تو در اطلس رسیدی</p></div>
<div class="m2"><p>چو کرم پیله پشمم در کشیدی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز گفت دایه گل تشویر میخورد</p></div>
<div class="m2"><p>از آن تشویر شکّر شیر میخورد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز شرم او عرق میریخت از گل</p></div>
<div class="m2"><p>نهان میکرد گل در زیر سنبل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بر دایه دلی پر غم نشسته</p></div>
<div class="m2"><p>ز خجلت بر گلش شبنم نشسته</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بآخر دایهٔ مسکین برون شد</p></div>
<div class="m2"><p>کنون بشنو که حال هر دوچونشد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو طاقت طاق شد هرمز برآشفت</p></div>
<div class="m2"><p>بزیر لب زیک شکّر سخن گفت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگل گفت ای دو یاقوتت شکرریز</p></div>
<div class="m2"><p>ز مخموری دو بادامت سحرخیز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قمر همسایهٔ سی کوکب تو</p></div>
<div class="m2"><p>شکر همشیرهٔ لعل لب تو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تویی شمع و شکرداری بخروار</p></div>
<div class="m2"><p>منم بر شمع تو پروانه کردار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو بر عشقست پروانه دماغی</p></div>
<div class="m2"><p>گزیرش نبود از روغن چراغی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو شمعی گشتهیی همخانهٔ من</p></div>
<div class="m2"><p>بیک شکّر بده پروانهٔ من</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز صد شکّر مرا آخر یکی ده</p></div>
<div class="m2"><p>اگر بسیار ندهی اندکی ده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بخوشی صدقه ده یک بوسه ما را</p></div>
<div class="m2"><p>که صدقه باز گرداند بلا را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بده یک بوسه چه جای ملالست</p></div>
<div class="m2"><p>که امشب چاشنی باری حلالست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نخستین کوزه در دردی زنی تو</p></div>
<div class="m2"><p>اگر بخیه بدین خردی زنی تو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مباش آخر بدین باریک ریسی</p></div>
<div class="m2"><p>که یک یک نخ چنین بر من نویسی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ترا چون ملک خوزستانست امروز</p></div>
<div class="m2"><p>بیک شکّر مکن بخل ای دلفروز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چوشد جانم ز جام خسروی مست</p></div>
<div class="m2"><p>بیک بوسه دلم را کن قوی دست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بآخر چون بسی باهم بگفتند</p></div>
<div class="m2"><p>چو شیر و چون شکر با هم بخفتند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گل از سر چون صلای ناز درداد</p></div>
<div class="m2"><p>متاع عیش را آواز در داد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز شوخی چون زحد بگذشت نازش</p></div>
<div class="m2"><p>بلب عذری چو شکّر خواست بازش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>خوشا آن کینه و آن عذرجویی</p></div>
<div class="m2"><p>که آن دم خوشترست از هرچه گویی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو دورخ هر دو روبارو نهادند</p></div>
<div class="m2"><p>ز بوسه قفل با یکسو نهادند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دورخ بر هم لب از پاسخ فگندند</p></div>
<div class="m2"><p>ببوسه اسب در شهرخ فگندند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو جوزا آن دو مهوش روی در روی</p></div>
<div class="m2"><p>ببوسه دیده هر یک موی بر موی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دودست اندر کش آوردند هر دو</p></div>
<div class="m2"><p>سخنهای خوش آوردند هر دو</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>حکایت چون ز شکّر برتر آید</p></div>
<div class="m2"><p>بسی از شهد و شکّر خوشتر آید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چوخوشتر باشد از دو عاشق نغز</p></div>
<div class="m2"><p>دو شکرشان بهم بادام در مغز</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو باهم هر دو دلبر دوست بودند</p></div>
<div class="m2"><p>دو مغز و هر دو در یک پوست بودند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زده اسباب شادی دست درهم</p></div>
<div class="m2"><p>بپای افتاده دو سرمست در هم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زبان بگشاد هرمز در شب تار</p></div>
<div class="m2"><p>که صبحا برمدم جزبر لب یار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>مدم زنهار ای صبح از فضولی</p></div>
<div class="m2"><p>دمی دیگر مکن خلوت بشولی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مدم کامشب بهم کاریست ما را</p></div>
<div class="m2"><p>بشب در روز بازاریست ما را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو شمعی تا بروزم زنده امشب</p></div>
<div class="m2"><p>بمیرم گر زنی یک خنده امشب</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تویی ای صبح امشب دستگیرم</p></div>
<div class="m2"><p>نفس گرمی برآری من بمیرم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هر آنکس را که با ماهیست حالی</p></div>
<div class="m2"><p>برو یک دم شبی، ماهی چو سالی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شب وصل یکه دل خرّم نماید</p></div>
<div class="m2"><p>بسی کوته تر از یکدم نماید</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دل هرمز در آن شب جوش میزد</p></div>
<div class="m2"><p>ز بیم روز نوشا نوش میزد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بگل میگفت کای تنگ شکر پاش</p></div>
<div class="m2"><p>که ما گشتیم از لعلت گهرپاش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گلی در تنگ آوردیم و رستیم</p></div>
<div class="m2"><p>بشکّر تا بگردن در نشستیم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ازین داد وستد با حور زادی</p></div>
<div class="m2"><p>بآخر بستدیم از عمر دادی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بکام خویش دیده چشم بد را</p></div>
<div class="m2"><p>بکام دل رسانیدیم خود را</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ندانم تا مرادر دلفروزی</p></div>
<div class="m2"><p>چنین شب نیزخواهد بود روزی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چنین شب نیز با چندین سلامت</p></div>
<div class="m2"><p>نبیند خلق تا روز قیامت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بآخر چون شکر بر شهد خستند</p></div>
<div class="m2"><p>بپسته بر گشادن عهد بستند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>که گر مهلت بود در زندگانی</p></div>
<div class="m2"><p>بهم رانیم عمری کامرانی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>سمنبر با شکر لب قول میکرد</p></div>
<div class="m2"><p>دلش فریاد و جان لاحول میکرد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>میان هر دو شد چون عهد بسته</p></div>
<div class="m2"><p>گلش گفتا که کردی لعل خسته</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>کشیده داردست ای مایهٔ ناز</p></div>
<div class="m2"><p>که بسیاری خوری از ما شکر باز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بیا تا خوش بخسبیم و بخندیم</p></div>
<div class="m2"><p>کلید بوسه در دریا فگندیم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>جوابش داد هرمز کای سمنبوی</p></div>
<div class="m2"><p>چه برخیزد ازین خفتن سخن گوی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>تو آتش در جهان افگندی امشب</p></div>
<div class="m2"><p>گلی زان بر جهان میخندی امشب</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نیم آن مرغ من کز چشمهٔ نوش</p></div>
<div class="m2"><p>شوم از شربتی آب تو خاموش</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>مگس چون نیست شکّر هست قوتم</p></div>
<div class="m2"><p>بسوی پرده بر چون عنکبوتم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کسی را آنچنان گنج نهانی</p></div>
<div class="m2"><p>دهن بندد بآب زندگانی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ز راه کور بر میبایدت خاست</p></div>
<div class="m2"><p>نیاید کارم از آبی تهی راست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>نداده بادهیی آسوده امشب</p></div>
<div class="m2"><p>بآبم میدهی پالوده امشب</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو هستم شکّرت را چاشنی گیر</p></div>
<div class="m2"><p>بچربی نیز خواهم روغن از شیر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو شکّر هست لختی شیرباید</p></div>
<div class="m2"><p>چه میگویم هدف را تیر باید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز پسته چند بیرون افگنم پوست</p></div>
<div class="m2"><p>که پسته کار بیگاریست ای دوست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>لبت را چون زکوة آب حیاتست</p></div>
<div class="m2"><p>چو از هر جاترا بیشک ز کوتست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چو من درویشم از بهر نباتی</p></div>
<div class="m2"><p>بدین درویش بیدل ده ز کوتی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چه میخواهی ز من زین بیش آخر</p></div>
<div class="m2"><p>نبودت هیچکس درویش آخر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو تو با من بیک نعمت کنی ساز</p></div>
<div class="m2"><p>خداوندت یکی را ده دهد باز</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بشکّر در ده آواز سبیلی</p></div>
<div class="m2"><p>که نیکو نبود از نیکو بخیلی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>هوی میخواند هرمز را بتعلیم</p></div>
<div class="m2"><p>که بگذارد الف بر حلقهٔ‌میم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو هرمز آن الف را مختلف کرد</p></div>
<div class="m2"><p>دو ساق خویش گل چون لام الف کرد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بگردانید روی آن سیم تن حور</p></div>
<div class="m2"><p>که بادا آن الف از میم من دور</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نخواهد یافت الف بر میم من راه</p></div>
<div class="m2"><p>الف چیزی ندارد بوسه در خواه</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ترا جز بوسه دادن نیست رویی</p></div>
<div class="m2"><p>نیابد آن الف زین میم مویی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>اگر تو همچو سیمی دیدی این میم</p></div>
<div class="m2"><p>ندارد هیچ کاری سنگ در سیم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>دل سنگینت این میخواهد از کار</p></div>
<div class="m2"><p>که تو سنگی دراندازی بیکبار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>سر دندان به شکّر تیز کردی</p></div>
<div class="m2"><p>که شفتالوی بادانگیز خوردی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ببوسه گر دلت با ما رضا داد</p></div>
<div class="m2"><p>ز تنگ گل بسی شکّر ترا باد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>وگر راضی نیی دم بر زن از پوست</p></div>
<div class="m2"><p>شبت خوش باداینک رفتم ای دوست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو سالم نیست بیست از من میازار</p></div>
<div class="m2"><p>زکوة از بیست باید داد ناچار</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چو من درزاد خویش از بیست طاقم</p></div>
<div class="m2"><p>مکن چون بیست عقد از جفت ساقم</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چو مقصود من از تو هست دیدار</p></div>
<div class="m2"><p>تو چون من باش اگر هستی خریدار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ببستان قدر گل چندانست ای دوست</p></div>
<div class="m2"><p>که زیر پرده با غنچه ست هم پوست</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چو از پرده برآید چست و چالاک</p></div>
<div class="m2"><p>ببویند و بیندازند در خاک</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چو بیضه پاره شد بر مهر عنبر</p></div>
<div class="m2"><p>چو عود خام سوزندش بمجمر</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>نگین کز کان بدست آورده حکاک</p></div>
<div class="m2"><p>کند از چرخ گردنده دلش چاک</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بمهر من مکن زنهار آهنگ</p></div>
<div class="m2"><p>که گل در غنچه بهتر لعل در سنگ</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>مرا خواهی هوای خویش بگذار</p></div>
<div class="m2"><p>مر این درجم بجای خویش بگذار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بمهر من نیابی جز شکر چیز</p></div>
<div class="m2"><p>بمهر درج من منگرد گر نیز</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>کلید درج محکم دار امروز</p></div>
<div class="m2"><p>که تاچون گردد آن کار ای دل افروز</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ز گل هرمز بجوش آمد دگربار</p></div>
<div class="m2"><p>که در شورم مکن ای خوش نمک یار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ز تو، بی غم نیابد کس نصیبی</p></div>
<div class="m2"><p>که رعنایی ز گل نبود غریبی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بکام دل چه میگیری جدایی</p></div>
<div class="m2"><p>فراغت نیستت تا کی نمایی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گواهی میدهی بر خویشتن تو</p></div>
<div class="m2"><p>ولی عاشق تری باللّه ز من تو</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>ز روباهی بپرسیدند احوال</p></div>
<div class="m2"><p>ز معروفان گواهش بود دنبال</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چو دل با تو کند در کاسه دستی</p></div>
<div class="m2"><p>چرا در کاسه گیری دست مستی</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>دلت از نقش عشقم دور چون شد</p></div>
<div class="m2"><p>که نقش از سنگ نتواند برون شد</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>بلی در سنگ بودت نقش آتش</p></div>
<div class="m2"><p>بجست این آتش از سنگ تو خوش خوش</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو میدانی تو کردار زمانه</p></div>
<div class="m2"><p>چرا شوری درین زنبور خانه</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چو در کاری بخواهی کرد آرام</p></div>
<div class="m2"><p>در اوّل کن که پیدا نیست انجام</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>روا باشد که دوران زمانه</p></div>
<div class="m2"><p>بود ما را در انجام از میانه</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>عجب نبود که ندهد عمر من داو</p></div>
<div class="m2"><p>مکن، مستیز، ای گل مست مشتاو</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>وگر حاصل نمیداری تو کامم</p></div>
<div class="m2"><p>شدم، انگار نشنودی تو نامم</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>درین معنی نیفتادت بد از من</p></div>
<div class="m2"><p>لبت گر یک شکر صد بستد از من</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بدندان گر لبت را خسته کردم</p></div>
<div class="m2"><p>ببوسه مرهمش پیوسته کردم</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بدندان زان لب لعلت گزیدم</p></div>
<div class="m2"><p>که تا خون از لب لعلت مزیدم</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>چوخوردی خونم از لب باز کردم</p></div>
<div class="m2"><p>که خوش خوش از لبت خون بازخوردم</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>کنون رفتم چه عذرت خواهم امشب</p></div>
<div class="m2"><p>که در بی مهریت بی ماهم امشب</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>چو گفت این خواست تا برخیزد از جای</p></div>
<div class="m2"><p>گلش افتاد همچون زلف در پای</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>که گل را این چنین مپسندی آخر</p></div>
<div class="m2"><p>بیک حمله سپر بفگندی آخر</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>گلم زان پیش تو افگند بادم</p></div>
<div class="m2"><p>مشو از خط که سر بر خط نهادم</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>دل خود دانهٔ دام تو کردم</p></div>
<div class="m2"><p>خرد را خطبه برنام تو کردم</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>چو سر بر پایت آرم سرفرازم</p></div>
<div class="m2"><p>چو جان در پایت افشانم بنازم</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>درون جانی ای در خون جانم</p></div>
<div class="m2"><p>ندانم جز تو کس بیرون جانم</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>زهی دلسوز یار ناوفادار</p></div>
<div class="m2"><p>زهی غمخواره دلبند جگرخوار</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>چو دامن روی من در پای دیده</p></div>
<div class="m2"><p>وزین سرگشته، دامن درکشیده</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>ز بیمهری مشو ای مه ز من دور</p></div>
<div class="m2"><p>که نه هرگز بود بی مهر مه نور</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>چو دل را در میان خط کشیدی</p></div>
<div class="m2"><p>خطی در دل کشیدی و رمیدی</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>چو حلقه تا بدر بازم نهادی</p></div>
<div class="m2"><p>چو شمعم سوختی گازم نهادی</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>کنون از خشم من دم سرد کردی</p></div>
<div class="m2"><p>دلم را شهربند درد کردی</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چوخاک راه پیشت بردبارم</p></div>
<div class="m2"><p>چو خون دیده سر نه بر کنارم</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>چنین نازک مباش ای جان من تو</p></div>
<div class="m2"><p>که از گل برنتابی یک سخن تو</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>بسی میلم ز عشرت از تو بیشست</p></div>
<div class="m2"><p>ولی بیمم ز رسوایی خویشست</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>گل شیرین بشکّر لب گشاده</p></div>
<div class="m2"><p>فسون میخواند سر بر خط نهاده</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>بآخر آن فسون هم کار گر شد</p></div>
<div class="m2"><p>دل هرمز از آن دلبر دگر شد</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بگلرخ گفت ای چون گل کم آزار</p></div>
<div class="m2"><p>مگیر از من چو گل از یک دم آزار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چو کامم برنمیآری کنون من</p></div>
<div class="m2"><p>بکام تو دهم خطّی بخون من</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>چو با من مینسازی کژ چه بازم</p></div>
<div class="m2"><p>من دلسوخته با تو بسازم</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>بگفت این و شکر در تنگ آورد</p></div>
<div class="m2"><p>ز زلفش ماه در خرچنگ آورد</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>گهی دزدید از آب زندگانی</p></div>
<div class="m2"><p>بلب بردش ز شکّر رایگانی</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>گهی بر انگبین زد قند او را</p></div>
<div class="m2"><p>گهی بگسست گردن بند او را</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>گهی شکّر ز مغز پسته خورد او</p></div>
<div class="m2"><p>گهی لعلش بمرجان خسته کرد او</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>گهی با سیم کار او چو زر کرد</p></div>
<div class="m2"><p>گهی با دوست دست اندر کمر کرد</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>گهی صد حلقه زانزلف زره پوش</p></div>
<div class="m2"><p>بیکدم کرد همچون حلقه در گوش</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>گهی از پسته عنّابش بخست او</p></div>
<div class="m2"><p>ببوسه بر شکر فندق شکست او</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>ز سیبش کرد شفتالو بسی باز</p></div>
<div class="m2"><p>مگر پیوسته بود آن هر دوزاغاز</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>بخفتند آن دو دلبر همچنین مست</p></div>
<div class="m2"><p>که تا باد سحرگه بر زمین جست</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>سپاه روز چون بر شب غلو کرد</p></div>
<div class="m2"><p>نسیم صبح جان را تازه رو کرد</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>بگوش آمد ز دریای سیاهی</p></div>
<div class="m2"><p>خروش مرغ شبگیر از پگاهی</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>ز باد صبح گل سرمست برجست</p></div>
<div class="m2"><p>نگر گل چون بود در صبحدم مست</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>چو گل برخاست هرمز نیز برخاست</p></div>
<div class="m2"><p>صبوحی را ز گلرخ باده درخواست</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>گلش گفتا ز بویی میزنی خوش</p></div>
<div class="m2"><p>خمارت میکند از مستی دوش</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>بدست خود می مخموریم ده</p></div>
<div class="m2"><p>وزان پس درشدن دستوریم ده</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>بباید رفت چون روزست و ما مست</p></div>
<div class="m2"><p>که تا برمانیابد چشم بد دست</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>که چون پیمانه پر گردد بناکام</p></div>
<div class="m2"><p>بگرداند سر خود در سرانجام</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>بگفت این و میی خورد و میی داد</p></div>
<div class="m2"><p>دم از آب قدح میزد پریزاد</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>چو کرد آب قدح را آن پری نوش</p></div>
<div class="m2"><p>شد او همچون پری در آب خاموش</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>بیفتادند هر دو مایهٔ ناز</p></div>
<div class="m2"><p>ز مستی سرگران کرده ز سرباز</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>یکی سر در کنار آن نهاده</p></div>
<div class="m2"><p>غمش سر در میان جان نهاده</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>یکی را پای آن یک گشته بالین</p></div>
<div class="m2"><p>نهاده یار را بالین سیمین</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>دو عاشق را ز خود یک جو خبر نه</p></div>
<div class="m2"><p>وزین عالم وزان عالم اثر نه</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>ز خوب و زشت دنیا باز رسته</p></div>
<div class="m2"><p>بکلّی از نیاز و ناز رسته</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>شنودم از یکی مستی بآواز</p></div>
<div class="m2"><p>که می زان میخورم کز خود رهم باز</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>چو صبح از چرخ گردون پرده بفگند</p></div>
<div class="m2"><p>سپیده صد هزاران زرده بفگند</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>سپیده از پس بالا درآمد</p></div>
<div class="m2"><p>دُر صبح از بن دریا برآمد</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>چو شد روشن درآمد دایهٔ پیر</p></div>
<div class="m2"><p>دو دلبر دید پای هر دو در قیر</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>نه نقلی حاضر ونه شمع بر پای</p></div>
<div class="m2"><p>نه می مانده، نه مجلسخانه برجای</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>جهان روشن شده، شمعی نشسته</p></div>
<div class="m2"><p>شرابی ریخته، جامی شکسته</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>همه خانه قدح پاره گرفته</p></div>
<div class="m2"><p>زمین سیمای میخواره گرفته</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>درآمد دایه و فریاد در بست</p></div>
<div class="m2"><p>زبانگش دلگشای از جای برجست</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>چو هرمز دید گل را جست بر پای</p></div>
<div class="m2"><p>که تا بدرود کردش مست برجای</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>چو می بدرود کرد آن رشک مه را</p></div>
<div class="m2"><p>ز بوسه بدرقه برداشت ره را</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>گل خورشید رخ برخاست و دایه</p></div>
<div class="m2"><p>روان دایه پس گل همچو سایه</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>بسوی قصر شد، وان روز تا شب</p></div>
<div class="m2"><p>ز شوق آن شبش میگفت یا رب</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>گهی میکرد ازان مستی خمارش</p></div>
<div class="m2"><p>گهی زان ناز و آن بوس و کنارش</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>گهی زان عیش و خوشی یاد میکرد</p></div>
<div class="m2"><p>گهی زان آرزو فریاد میکرد</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>گهی زان خوشدلیها باز میگفت</p></div>
<div class="m2"><p>گهی میخاست، گاهی باز میخفت</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>کنون بنگر که گردون چه جفا کرد</p></div>
<div class="m2"><p>که تا آن هر دو را از هم جدا کرد</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>فلک گویی یکی بازیگر آمد</p></div>
<div class="m2"><p>که هر ساعت برنگی دیگر آمد</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>فلک دانی که چیست ای مرد باهش</p></div>
<div class="m2"><p>یکی بیگانه پرور، آشنا کش</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>بدین چون مدّتی بگذشت از ایام</p></div>
<div class="m2"><p>گل و هرمز نیاسودند از کام</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>گهی کام و گهی ناکام بودی</p></div>
<div class="m2"><p>گهی جام و گهی پیغام بودی</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>گهی با هم گهی بی هم نشسته</p></div>
<div class="m2"><p>گهی هم غم گهی همدم نشسته</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>جهان بر کام خود راندند یک چند</p></div>
<div class="m2"><p>ولیک از کار آن هر دو فلک خند</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>نمی کرد آسیاب چرخ در کوب</p></div>
<div class="m2"><p>از آن بود آسیا بر کام جاروب</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>گل از دل دانهیی در خورد میکاشت</p></div>
<div class="m2"><p>بعشرت آسیا بر گرد میداشت</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>چه شادی و چه غم آنجا که او شد</p></div>
<div class="m2"><p>همه در آسیای او فرو شد</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>ندانستند از اوّل این جهان را</p></div>
<div class="m2"><p>که آخر چه درآید از پس آنرا</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>جهان با یک شکر صد نیش نی داشت</p></div>
<div class="m2"><p>دمی شادیش سالی غم ز پی داشت</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>اگر گل بر جهان خندید یک روز</p></div>
<div class="m2"><p>ببین کز شیشه گریان شد بصد سوز</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>ز دنیا آدمی را خرّمی نیست</p></div>
<div class="m2"><p>کسی کوخرّمست او آدمی نیست</p></div></div>