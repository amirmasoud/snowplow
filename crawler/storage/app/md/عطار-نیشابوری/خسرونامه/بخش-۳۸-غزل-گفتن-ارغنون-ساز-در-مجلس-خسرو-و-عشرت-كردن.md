---
title: >-
    بخش ۳۸ - غزل گفتن ارغنون ساز در مجلس خسرو و عشرت كردن
---
# بخش ۳۸ - غزل گفتن ارغنون ساز در مجلس خسرو و عشرت كردن

<div class="b" id="bn1"><div class="m1"><p>می جان پرورم ده در صبوحی</p></div>
<div class="m2"><p>لان الرّاح ریحانی و روحی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک امشب از قدح می نوش تا لب</p></div>
<div class="m2"><p>که فردا را امیدی نیست تا شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بادی دی شد و فردا نیامد</p></div>
<div class="m2"><p>غم ما را سری پیدا نیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهاریخوش بخور با صبح خیزان</p></div>
<div class="m2"><p>که عمرت پیش دارد برگ ریزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مرغ صبحگاهی زد پر وبال</p></div>
<div class="m2"><p>پر و بالی بزن تا خوش شود حال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دور باده گر دلشاد گردی</p></div>
<div class="m2"><p>دمی از جور چرخ آزاد گردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو می بر بایدت دور زمانه</p></div>
<div class="m2"><p>دمی بنشین بعشرت شادمانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که چون کشتی عمر افتد بگرداب</p></div>
<div class="m2"><p>امان نبود که یک شربت خوری آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا عمری که با صد گونه پیچست</p></div>
<div class="m2"><p>یک امروزست و آنهم پی بهیچست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سحرخیزا می بنشسته درده</p></div>
<div class="m2"><p>ز پسته بوسهٔ سر بسته در ده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برآورهای و هویی همچو مستان</p></div>
<div class="m2"><p>ز نقد عمر داد وقت بستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میی در ده که جمله سر براهیم</p></div>
<div class="m2"><p>که مهمان جهان از دیرگاهیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میی در ده تو ای سرو سهی، زود</p></div>
<div class="m2"><p>که زود از ما جهان خواهد تهی بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز صد شادی دلت آرام یابد</p></div>
<div class="m2"><p>اگر یک باده در تو کام یابد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیا تا امشبی دلشاد باشیم</p></div>
<div class="m2"><p>شبی از غم چو سرو آزاد باشیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بشادی آستینی بر فشانیم</p></div>
<div class="m2"><p>چوتنگ آید اجل مرکب برانیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دمی بر بانگ چنگ و ناله نی</p></div>
<div class="m2"><p>سراسر کن قدح، در ده پیاپی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برآمد از جهان آواز مستان</p></div>
<div class="m2"><p>ببد مستی جهان را داد بستان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می و معشوق و عشق و روز نوروز</p></div>
<div class="m2"><p>ز توبه توبه باید کرد امروز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیار آن بادهٔ خوشبوی چون مشک</p></div>
<div class="m2"><p>که تا تر گردد از می مان لب خشک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو مطرب این غزل برگفت شهزاد</p></div>
<div class="m2"><p>میان باغ از مستی بیفتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سوی قصر گلش بردند از باغ</p></div>
<div class="m2"><p>رخ گل شد از آن چون لاله پر داغ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو دیگر روز از این طاق مقرنس</p></div>
<div class="m2"><p>جهان پوشیده شد در زرد اطلس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه روی زمین بگرفته زردی</p></div>
<div class="m2"><p>به یک ره آسمان شد لاجوردی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیامد خسرو و بر تخت بنشست</p></div>
<div class="m2"><p>بمخموری گرفته جام در دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز سر در، مجلسی نو، ساز کردند</p></div>
<div class="m2"><p>همه ساز طرب آغاز کردند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی ساقی خاص شاه، بی ریش</p></div>
<div class="m2"><p>کزو دل ریش میکندی ز تشویش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکر دزدیده لعلش درمزیده</p></div>
<div class="m2"><p>بجان زرداده دشنامش خریده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر بفروختی عالم سزیدی</p></div>
<div class="m2"><p>بر آنکس کو ازو بوسی خریدی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لب او رهزن پیر و جوان بود</p></div>
<div class="m2"><p>بدندان همه پیران ازان بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صلای تلخ می در داد ساقی</p></div>
<div class="m2"><p>ز شیرینی خود نگذاشت باقی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو باده پای کوبان بر سر آمد</p></div>
<div class="m2"><p>شه از یک کاسه چون دیگی برآمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شه را باده در سر کارگر شد</p></div>
<div class="m2"><p>بمطرب گفت خسرو بیخبر شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برای کوری شاه سپاهان</p></div>
<div class="m2"><p>بزن ای نغمه زن راه سپاهان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی یوسف جمالی عود برداشت</p></div>
<div class="m2"><p>زبان در نغمهٔ داود برداشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شکر لب چون بریشم بست بر عود</p></div>
<div class="m2"><p>ز پرده برگشاد آواز داود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو گوش کرّنا مالید هموار</p></div>
<div class="m2"><p>بسر گردید گردون کرّناوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز مجلس الصّلای نوش برخاست</p></div>
<div class="m2"><p>ز دل فریاد و از جان جوش برخاست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درآمد مرغ بریان مرحبا گوی</p></div>
<div class="m2"><p>بصد الحان صراحی الصّلا گوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صراحی خود نفس تا پیش و پس داشت</p></div>
<div class="m2"><p>مگر از باده تنگی نفس داشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>می چون خون بی اندازه میشد</p></div>
<div class="m2"><p>جگر زان خون ببر در تازه میشد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جگر را بود آن می آب کسنی</p></div>
<div class="m2"><p>کسی کان می بخورد او بود کس نی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زمانی بود خوانی بر کشیدند</p></div>
<div class="m2"><p>جهانی تا جهانی برکشیدند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>صراحی از قفا خوردن باستاد</p></div>
<div class="m2"><p>قدح از آب تا گردن باستاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بیاوردند از صد گونه جلّاب</p></div>
<div class="m2"><p>قدح پرماهیان کرده چو سیماب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو دف از سر قدح یکسان ز هر سو</p></div>
<div class="m2"><p>بپای افگنده همچون چنگ گیسو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو شربت رفت خوانسالار بنهاد</p></div>
<div class="m2"><p>زهر نوعی ابا بسیار بنهاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ندیده بود هرگز گرده ماه</p></div>
<div class="m2"><p>ز خوان آسمان چون خوان آن شاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نواله داشت در بر نان ز هر سوی</p></div>
<div class="m2"><p>هریسه داشت در سر خوان زهر سوی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ابا و قلیه و حلوا و بریان</p></div>
<div class="m2"><p>نهاده تا بشیر مرغ بر خوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو نان شد خورده آمد خادمی چست</p></div>
<div class="m2"><p>بطشت و آب هر کس دست میشست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چوخوان از پیش خسرو بر گرفتند</p></div>
<div class="m2"><p>طری مجلس نو بر گرفتند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شه از ساقی گلرخ جام درخواست</p></div>
<div class="m2"><p>زهرمطرب سماع عام درخواست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بیک ره مطربان نام بردار</p></div>
<div class="m2"><p>نهادند آنچه دانستند در کار</p></div></div>