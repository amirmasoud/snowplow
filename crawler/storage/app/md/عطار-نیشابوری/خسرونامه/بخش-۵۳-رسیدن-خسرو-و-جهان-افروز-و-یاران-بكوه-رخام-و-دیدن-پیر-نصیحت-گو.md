---
title: >-
    بخش ۵۳ - رسیدن خسرو و جهان افروز و یاران بكوه رخام و دیدن پیر نصیحت گو
---
# بخش ۵۳ - رسیدن خسرو و جهان افروز و یاران بكوه رخام و دیدن پیر نصیحت گو

<div class="b" id="bn1"><div class="m1"><p>چو بگذشتند ازان دریای خونخوار</p></div>
<div class="m2"><p>یکی کوه بلند آمد پدیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی حصن رخامین بر سر کوه</p></div>
<div class="m2"><p>درختان گشته گرد حصن انبوه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بران حصن قوی بر رفت خسرو</p></div>
<div class="m2"><p>جوانمردانش در پی گشته پس رو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپیش آن صفّهیی میدید از دور</p></div>
<div class="m2"><p>که چون شمعی فروزان بود از نور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بساط صفّه دوخ و بوریا بود</p></div>
<div class="m2"><p>دران محرابگه پیری دو تا بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مرغی برسر کوهی نشسته</p></div>
<div class="m2"><p>ز هر شادی و اندوهی برسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپیش کردگار استاده بر پای</p></div>
<div class="m2"><p>نهاده دست بر هم چشم بر جای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخفته گربهیی بر جام. پیر</p></div>
<div class="m2"><p>ز سر تا پای او مانندهٔ شیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو کس همدم نبودش زادمیزاد</p></div>
<div class="m2"><p>بر خود گربهیی را همدمی داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر هستی تو شیر پردهٔ راز</p></div>
<div class="m2"><p>ببُر از آدمی با گربهیی ساز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که از مردم اگرچه خویش باشد</p></div>
<div class="m2"><p>وفای گربه و سگ بیش باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توّقف کرد شه تا پیر دمساز</p></div>
<div class="m2"><p>بپرداخت و سلامش کرد آغاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبان بگشاد پیر کار دیده</p></div>
<div class="m2"><p>بدو گفت ای بسی تیمار دیده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برو بنشین چه میگردی جهانی</p></div>
<div class="m2"><p>که جمعیت بسی گردد زمانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چوهمدم نیست تو همدم نیابی</p></div>
<div class="m2"><p>که چون محرم نیی محرم نیابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بتنهایی بسر بر روزگاری</p></div>
<div class="m2"><p>که تنهایی ترا بهتر ز یاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسی من گرد عالم بر دویدم</p></div>
<div class="m2"><p>بجستم عاقبت همدم ندیدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز نااهلان فرو خوردم همه عمر</p></div>
<div class="m2"><p>ز حق اهلی طلب کردم همه عمر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگرچه در جهان دیدم بسی را</p></div>
<div class="m2"><p>ندیدم هیچ اهلیت کسی را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چرا در حلقهٔ مردم نشینم</p></div>
<div class="m2"><p>که جز در دیده، مردم مینبینم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگرچه یک جوم بیرون شوی نیست</p></div>
<div class="m2"><p>همه آفاق در چشمم جوی نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مگر دیوار من کوتاه تر بود</p></div>
<div class="m2"><p>که در ملکم نه دیوار ونه در بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون عمریست تادر گوشه تنها</p></div>
<div class="m2"><p>بدل با خویش میگویم سخنها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه گویم، با که گویم، چند گویم</p></div>
<div class="m2"><p>چو چیزی گم نکردم، چند جویم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درین زندان کافر کیش غدار</p></div>
<div class="m2"><p>ز حیرت کافری میآردم بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نمیدانم کیم یا از کجایم</p></div>
<div class="m2"><p>چه میسازم، ز جان و ز تن جدایم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درین گرداب اگر افتی دمی تو</p></div>
<div class="m2"><p>ز من سرگشته تر گردی همی تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چوخسرو پیر را میدید هشیار</p></div>
<div class="m2"><p>ز هر نوعی سؤالش کرد بسیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ولیک آن پیر را در هیچ بابی</p></div>
<div class="m2"><p>نشد پوشیده بر خاطر، جوابی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به خسرو گفت کم دیدم جوانی</p></div>
<div class="m2"><p>ز تو شیرین زبان تر در جهانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه دردانش ترا ماننده دیدم</p></div>
<div class="m2"><p>نه مثلت درجهان داننده دیدم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بحمداللّه نمردم ناگهان من</p></div>
<div class="m2"><p>بدیدم زندهیی را در جهان من</p></div></div>