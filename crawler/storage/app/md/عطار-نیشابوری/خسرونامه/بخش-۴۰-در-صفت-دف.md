---
title: >-
    بخش ۴۰ - در صفت دف
---
# بخش ۴۰ - در صفت دف

<div class="b" id="bn1"><div class="m1"><p>یکی صورت درآمد ماه پیکر</p></div>
<div class="m2"><p>نهاده همچو گردون پای بر سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخی مانند ماه آسمان داشت</p></div>
<div class="m2"><p>ولیک از پنج ماه نوفغان داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تپانچه بر رخ چون ماه میخورد</p></div>
<div class="m2"><p>چنان کز درد آن فریاد میکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان میتافت رویش از برون دست</p></div>
<div class="m2"><p>که از چستی بچنبر می برون جست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آوازش بچنبر جان برآورد</p></div>
<div class="m2"><p>سر بسیار کس در چنبر آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گردون چنبری گشت آشکاره</p></div>
<div class="m2"><p>جلاجل چنبرش همچون ستاره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سه چیز مختلف او را تن آمد</p></div>
<div class="m2"><p>ز حیوان ونبات و معدن آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو روی و چار گوشش بود و سرنه</p></div>
<div class="m2"><p>بسی زد حلقه از هر سوی و درنه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو می بنواخت از مهر دلش دوست</p></div>
<div class="m2"><p>ازان شادی نمیگنجید در پوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرچه دید روی دوست بیرون</p></div>
<div class="m2"><p>نیامد پیش او از پوست بیرون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرچه پوست از آهو رسیدش</p></div>
<div class="m2"><p>ولی شیرافگنی نیکو رسیدش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آن بی پا و سر برداشت آواز</p></div>
<div class="m2"><p>نمیدانست کس از پای، سر، باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو آواز خوشش بیهوش میداشت</p></div>
<div class="m2"><p>خوش آوازی خود را گوش میداشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهر پرده رهش پیوسته بودی</p></div>
<div class="m2"><p>ولکین پردهٔ او بسته بودی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگاری ماهروی از پرده برخاست</p></div>
<div class="m2"><p>بزد آن کوژ را در پردهٔ راست</p></div></div>