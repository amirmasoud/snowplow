---
title: >-
    (۳) حکایت درحال ارواح پیش از آفریدن اجسام
---
# (۳) حکایت درحال ارواح پیش از آفریدن اجسام

<div class="b" id="bn1"><div class="m1"><p>چنین گفتند کان مدت که ارواح</p></div>
<div class="m2"><p>درو بود آفریده پیش از اشباح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمار مدتش سالی سه چارست</p></div>
<div class="m2"><p>که هر یک زان جهان او هزارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین نقلست کان جانهای عالی</p></div>
<div class="m2"><p>دران مدت که بود از جسم خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجمع آن جمله را پیوسته کردند</p></div>
<div class="m2"><p>بیک صفشان بهم در بسته کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس آنگه از پس جانها بیکبار</p></div>
<div class="m2"><p>برأی العین دنیا شد پدیدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو آن جانها همه دنیا بدیدند</p></div>
<div class="m2"><p>بجان و دل سوی دنیا دویدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وزان قسمی که ماند آنجایگه باز</p></div>
<div class="m2"><p>بهشت افتاد شان بر راست آنجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو این قسم ای عجب جنت بدیدند</p></div>
<div class="m2"><p>بده جان از بر دوزخ رمیدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بماندند اندکی ارواح بر جای</p></div>
<div class="m2"><p>که ایشان را نماند از هیچ سو رای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه دنیا را نه جنت را گزیدند</p></div>
<div class="m2"><p>نه از دوزخ سر موئی رمیدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خطاب آمد که ای جانهای مجنون</p></div>
<div class="m2"><p>شما اینجا چه می‌خواهید اکنون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم آزادید از دنیا و جنت</p></div>
<div class="m2"><p>هم از دوزخ شما را نیست محنت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه می‌باید شما را در ره ما</p></div>
<div class="m2"><p>که لازم شد شما را درگه ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خروشی زان همه جانها برآمد</p></div>
<div class="m2"><p>تو گفتی عمر بر جانها سرآمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که ای دارای عرش و فرش و کرسی</p></div>
<div class="m2"><p>چو تو داناتری از ما چه پرسی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترا خواهیم ما دیگر همه هیچ</p></div>
<div class="m2"><p>توئی حق الیقین دیگر همه هیچ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خطاب آمد که گر خواهان مائید</p></div>
<div class="m2"><p>همه خواهان انواع بلائید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی چندان که موی جانور هست</p></div>
<div class="m2"><p>دگر دیگ بیابان سر بسر هست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر چندان که دارد قطره باران</p></div>
<div class="m2"><p>دگر چندان که برگ شاخساران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فزون زان بیش هر رنج و بلا من</p></div>
<div class="m2"><p>فرو ریزم بزاری بر شما من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خسک سازم هزاران آتشین بیش</p></div>
<div class="m2"><p>نهم‌تان هر زمان بر سینهٔ ریش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو آن جانها خطاب حق شنیدند</p></div>
<div class="m2"><p>ازان شادی خروشی برکشیدند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که جان ما فدای آن بلا باد</p></div>
<div class="m2"><p>بما تو هرچه خواهی آن بماباد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بلای تو بجان ما باز گیریم</p></div>
<div class="m2"><p>ز عمر جاودان آغاز گیریم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو با هر جانش سری در میانست</p></div>
<div class="m2"><p>گمان سر هر جانی چنانست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که صاحب سر این درگه جز او نیست</p></div>
<div class="m2"><p>ز سر معرفت آگه جز او نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان کارواح می‌دانند نیکوست</p></div>
<div class="m2"><p>ولی یک روح را دارد ازان دوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دگرها پردهٔ آن روح باشند</p></div>
<div class="m2"><p>برای آن همه مجروح باشند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو موئی راه بر در می‌کشیدند</p></div>
<div class="m2"><p>وگر هجده هزاران می‌بریدند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه ارواح اگر چه یک صفت بود</p></div>
<div class="m2"><p>ولی مقصود اهل معرفت بود</p></div></div>