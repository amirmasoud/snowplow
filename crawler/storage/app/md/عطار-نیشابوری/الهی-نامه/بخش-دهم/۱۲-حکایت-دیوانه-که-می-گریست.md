---
title: >-
    (۱۲) حکایت دیوانه که می‬گریست
---
# (۱۲) حکایت دیوانه که می‬گریست

<div class="b" id="bn1"><div class="m1"><p>یکی دیوانه بودی بر سر راه</p></div>
<div class="m2"><p>نشسته بر سر خاکستر آنگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانی اشک چون گوهر فشاندی</p></div>
<div class="m2"><p>زمای نیز خاکستر فشاندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی گفت ای بخاکستر گرفتار</p></div>
<div class="m2"><p>چرا پیوسته می‌گرئی چنین زار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین گفت او که پر شورست جانم</p></div>
<div class="m2"><p>چو شمعی غرقه اندر اشک ازانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که حق می‌بایدم بی غیر و بی پیچ</p></div>
<div class="m2"><p>ولی حق را نمی‌باید مرا هیچ</p></div></div>