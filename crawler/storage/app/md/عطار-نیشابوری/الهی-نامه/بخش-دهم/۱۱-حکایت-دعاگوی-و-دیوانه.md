---
title: >-
    (۱۱) حکایت دعاگوی و دیوانه
---
# (۱۱) حکایت دعاگوی و دیوانه

<div class="b" id="bn1"><div class="m1"><p>دعا می‌کرد آن داننندهٔ دین</p></div>
<div class="m2"><p>جهانی خلق می‌گفتند آمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی دیوانه گفت آمین چه باشد</p></div>
<div class="m2"><p>که آگه نیستم تا این چه باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفتند آمین آن بود راست</p></div>
<div class="m2"><p>کامام خواجه از حق هرچه درخواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان باد و چنان باد و چنان باد</p></div>
<div class="m2"><p>زبان بگشاد آن مجنون بفریاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که نبود آن چنان و این چنین هیچ</p></div>
<div class="m2"><p>کامام خواجه خواهد، چند ازین پیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولیکن جز چنان نبوَد کم و بیش</p></div>
<div class="m2"><p>که حق خواهد چه می‌خواهید ازخویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت چیزی نخواهد بود روزی</p></div>
<div class="m2"><p>نباشد روزیت جز سینه سوزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر او خواهدت کاری برآید</p></div>
<div class="m2"><p>وگرنه از گلت خاری برآید</p></div></div>