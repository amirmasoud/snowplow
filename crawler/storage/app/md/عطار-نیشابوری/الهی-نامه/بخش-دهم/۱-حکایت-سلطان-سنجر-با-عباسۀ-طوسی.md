---
title: >-
    (۱) حکایت سلطان سنجر با عبّاسۀ طوسی
---
# (۱) حکایت سلطان سنجر با عبّاسۀ طوسی

<div class="b" id="bn1"><div class="m1"><p>مگر یک روز سنجر شاه عالی</p></div>
<div class="m2"><p>بر عبّاسه آمد جای خالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیامد کارِ این با کارِ آن راست</p></div>
<div class="m2"><p>چو لختی پیش او بنشست برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی گفتش چرا خاموش بودی</p></div>
<div class="m2"><p>نگفتی تو حدیثی نه شنودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوابش داد عبّاسه پس آنگاه</p></div>
<div class="m2"><p>که چشمم آن زمان کافتاد بر شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهانی پُر ز شاخ تند دیدم</p></div>
<div class="m2"><p>بدستم داسکی بس کند دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان داسک نیارستم درودن</p></div>
<div class="m2"><p>ندیدم چاره جز خاموش بودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو گر از جاهِ دنیا شادمانی</p></div>
<div class="m2"><p>ز جاه آخرت محروم مانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گرد تو برآید مال و جاهت</p></div>
<div class="m2"><p>شود مال تو مار و جاه چاهت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل تو چیست موسی، نفس فرعون</p></div>
<div class="m2"><p>چو طشتی آتشین دنیا بصد لون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر جبریل فرماید بود خوش</p></div>
<div class="m2"><p>ز موسی دست آوردن به آتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولی گوینده گر فرعون باشد</p></div>
<div class="m2"><p>عذاب آتش صد لون باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که گر در طاعتی کردی گناهی</p></div>
<div class="m2"><p>بود هر عضوِ تو بر تو گواهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه کفر آنجا و نه ایمانت باشد</p></div>
<div class="m2"><p>کز اینجا آنچه بُردی آنت باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان دروی که اینجا کشته باشی</p></div>
<div class="m2"><p>همان پوشی که اینجا رشته باشی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا آنجا زیان و سود با تو</p></div>
<div class="m2"><p>همان باشد که اینجا بود با تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیابی شادی ای درویش آنجا</p></div>
<div class="m2"><p>مگر شادی بری با خویش آنجا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر در زهر و گر در نوش میری</p></div>
<div class="m2"><p>تو هم بار خود اندر دوش گیری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو یک یک ذرّهٔ عالم حجابست</p></div>
<div class="m2"><p>ترا گر ذرّهٔ باشد حسابست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدم بر جای سرگردان چو پرگار</p></div>
<div class="m2"><p>گران جانی مکن بگذر سبکبار</p></div></div>