---
title: >-
    (۸) حکایت موسی و مرد عابد
---
# (۸) حکایت موسی و مرد عابد

<div class="b" id="bn1"><div class="m1"><p>یکی عابد نیاسودی ز طاعت</p></div>
<div class="m2"><p>نبودی بی عبادت هیچ ساعت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبانروزی عبادت بود کارش</p></div>
<div class="m2"><p>بسر شد در عبادت روزگارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بموسی وحی آمد از خداوند</p></div>
<div class="m2"><p>که عابد را بگو ای مرد خرسند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه مقصودست از طاعت مدامت</p></div>
<div class="m2"><p>که در دیوان بدبختانست نامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو موسی آمد و او را خبر کرد</p></div>
<div class="m2"><p>عبادت مرد عابد بیشتر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان جدی دران کارش بیفزود</p></div>
<div class="m2"><p>که صد کارش بیکبارش بیفزود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفتا چو تو از اشقیائی</p></div>
<div class="m2"><p>چنین مشغول در طاعت چرائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بموسی گفت آن سرگشتهٔ راه</p></div>
<div class="m2"><p>که ای طوطی طور و مرد درگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان پنداشتم من روزگاری</p></div>
<div class="m2"><p>که هیچم من نیم در هیچ کاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دانستم که آخر در شمارم</p></div>
<div class="m2"><p>بیک طاعت زیادت شد هزارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نامم ز اشقیای او برآمد</p></div>
<div class="m2"><p>همه کاری مرا نیکوتر آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگرچه آب در آتش بود آن</p></div>
<div class="m2"><p>ازو هر چیز کآید خوش بود آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آن چیزی کزان درگاه آید</p></div>
<div class="m2"><p>چه بد چه نیک زاد راه آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر نورم بود از حق وگر نار</p></div>
<div class="m2"><p>خدایست او مرا با بندگی کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمی‌اندیشم از نزدیک و دورش</p></div>
<div class="m2"><p>که دایم این چنینم در حضورش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو موسی سوی طور آمد دگر بار</p></div>
<div class="m2"><p>خطابش کرد حق از اوج اسرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که چون دیدم که این عابد چنین است</p></div>
<div class="m2"><p>ز سر تا پای او مشغول دینست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پسندیدم ازو عهد عبادت</p></div>
<div class="m2"><p>ولی شد در عمل جدش زیادت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو او در بندگی خویش بفزود</p></div>
<div class="m2"><p>خداوندی خدا زو بیش بفزود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون از نیک بختانش شمردم</p></div>
<div class="m2"><p>ز لوح اشقیا نامش ستردم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رسانیدم بصاحب دولتانش</p></div>
<div class="m2"><p>بدو از من کنون مژده رسانش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو تو آگه نهٔ از سر انسان</p></div>
<div class="m2"><p>سر موئی مکن انکار ایشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سری از جهل پر اقرار و انکار</p></div>
<div class="m2"><p>که فردا نقد خواهد شد پدیدار</p></div></div>