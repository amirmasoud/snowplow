---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش درین شوریده زندان</p></div>
<div class="m2"><p>بطاعت می‌توان شد از بلندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر خواهی بلندی بر پَر از چاه</p></div>
<div class="m2"><p>که آن از طاعتی یابی نه ازجاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیمبر گفت: آخر وصف مستور</p></div>
<div class="m2"><p>که آن از مغز صدیقان بود دور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلاشک حب جاه و حب مالست</p></div>
<div class="m2"><p>ترا این جاه جستن پس وبالست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه در ره حق خاص خاصی</p></div>
<div class="m2"><p>شوی گر جاه یابی مرد عاصی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان از تو برآرد جاه دودی</p></div>
<div class="m2"><p>که نبود از تدارک هیچ سودی</p></div></div>