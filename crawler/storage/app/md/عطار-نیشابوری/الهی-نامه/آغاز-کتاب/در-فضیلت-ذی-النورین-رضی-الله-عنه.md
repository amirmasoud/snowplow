---
title: >-
    در فضیلت ذی النورین رضی الله عنه
---
# در فضیلت ذی النورین رضی الله عنه

<div class="b" id="bn1"><div class="m1"><p>اساسی کز حیا ایمان نهادست</p></div>
<div class="m2"><p>امیرالمؤمنین عثمان نهادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک از بحر علم او بخاری </p></div>
<div class="m2"><p>زمین از کوه حلم او غباری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان معرفت جان مصوّر</p></div>
<div class="m2"><p>دو مغز آنگه ز دو نور پیمبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه می‌گویم سه مغز آمد ز انوار</p></div>
<div class="m2"><p>ازان دو نور و از قرآن زهی کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کو در حریم این سه نورست</p></div>
<div class="m2"><p>گرش روشن نه بیند خصم کورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گر خورشید نقد عین دارد</p></div>
<div class="m2"><p>مدد از نور ذوالنورین دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز او کس را بنودست این تمامی</p></div>
<div class="m2"><p>ز پیغامبر در فرزند گرامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بر اندوه نازل گشت قرآن</p></div>
<div class="m2"><p>کسی را کاهل آنست اینست برهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بر اندوه از دنیا شود دور</p></div>
<div class="m2"><p>چنین بودست آن خورشید ذوالنور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی کو این کرامت از خدا یافت</p></div>
<div class="m2"><p>که دو چشم و چراغ مصطفی یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ذوالنورین هم از خانه دان بود</p></div>
<div class="m2"><p>چگونه منکر صدقش توان بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی کز آسمانش این دو نورست</p></div>
<div class="m2"><p>مه و خورشید با او در حضورست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دم از بغضش گر از دل می بر آری</p></div>
<div class="m2"><p>مه و خورشید را گل می برآری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عصای او بزانو آنکه بشکست</p></div>
<div class="m2"><p>خوره در زانویش افتاد پیوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عصائی را که در معنی چنان شد</p></div>
<div class="m2"><p>که ثعبان وار خصم دشمنان شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر او را دشمنی در کون باشد</p></div>
<div class="m2"><p>که باشد، نایب فرعون باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین گفت او که در بیعت مرا دست</p></div>
<div class="m2"><p>چو با دست نبی الله پیوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بهر حرمت دستش از آنگاه</p></div>
<div class="m2"><p>به مکروهی نبود آن دست را راه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلش دریای اعظم بود از علم</p></div>
<div class="m2"><p>تن او کوه راسخ بود از حلم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حقیقت جامع قرآن دلش بود</p></div>
<div class="m2"><p>همه اسرار عالم حاصلش بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز جامع بود جمعیت مدامش</p></div>
<div class="m2"><p>ز فرقان فرق کردن خاص و عامش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو در قرآن امام خاص و عامست</p></div>
<div class="m2"><p>چرا در حکم خویشان ناتمامت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه عمر او نخفتی و نخوردی</p></div>
<div class="m2"><p>که تا در هر شبی ختمی نکردی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دران غوغا غلامانش بیکبار</p></div>
<div class="m2"><p>سلاح آور شدند از بهر پیگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدیشان گفت هر بنده که امروز</p></div>
<div class="m2"><p>سلاح انداخت آزادست و پیروز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو شاهد بود قرآنش همیشه</p></div>
<div class="m2"><p>مدامش جمع جامع بود پیشه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شهید قرب شاهد گشت آخر</p></div>
<div class="m2"><p>ز قرآن یافت خونش طشت آخر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو قرآن بود معشوقش ز آفاق</p></div>
<div class="m2"><p>شد آخر محو قرآن شمع عشاق</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگرچه شمع جنّت بود فاروق</p></div>
<div class="m2"><p>چو شمع او باخت سر در راه معشوق</p></div></div>