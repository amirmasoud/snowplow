---
title: >-
    در فضیلت صدیق رضی الله عنه
---
# در فضیلت صدیق رضی الله عنه

<div class="b" id="bn1"><div class="m1"><p>سر مردان دین صدیق اکبر</p></div>
<div class="m2"><p>امام صادق و سالار سرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهین رحمت مهدات او بود</p></div>
<div class="m2"><p>که در دین سابق خیرات او بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب خلوت قرین ویار غارست</p></div>
<div class="m2"><p>نثار راهش اوّل چل هزارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین بوبکر چون کردست آغاز</p></div>
<div class="m2"><p>بدو گردد همه اجر جهان باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازان ایمان او در اصل خلقت</p></div>
<div class="m2"><p>همی چربد بر ایمانها ز سبقت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر او درد دندان داشت ده سال</p></div>
<div class="m2"><p>پیمبر را نکرد آگه ازان حال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو حق گفت آن پیمبر را بتحقیق</p></div>
<div class="m2"><p>بدو گفت ای جهان حلم صدیق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا با من نکردی این حکایت</p></div>
<div class="m2"><p>ز حق گفتا نکو نبود شکایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی کو دین حق زین سان نگه داشت</p></div>
<div class="m2"><p>بسرّ جان او جز حق که ره داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همیشه بود سنگی در دهانش</p></div>
<div class="m2"><p>که تاگوهر نیفشاند زبانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میان سنگ در گوهر شنیدم</p></div>
<div class="m2"><p>ولی سنگی بگوهر در ندیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان مستغرق حق بود جانش</p></div>
<div class="m2"><p>که کم رفتی حدیثی بر زبانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو جانش بود مشغول اندر آیة</p></div>
<div class="m2"><p>ازو هجده حدیث آمد روایة</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سزد عالم اگر هجده هزارست</p></div>
<div class="m2"><p>که آن هجده حدیثش یادگارست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حدیث او چو اصل عالم افتاد</p></div>
<div class="m2"><p>براهین حدوثش محکم افتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببین تا او چه عقل و چه بصر داشت</p></div>
<div class="m2"><p>که از آبستن و طفلی خبر داشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو نابینای عاجز را دعا کرد</p></div>
<div class="m2"><p>به بینائیش حق حاجت روا کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفس هرگز در افزونی نمی‌زد</p></div>
<div class="m2"><p>که دم جز در اقیلونی نمی‌زد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو هنگام وفات آمد فرازش</p></div>
<div class="m2"><p>به پیش مصطفی بردند بازش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز صدیق آن کلید عالم راز</p></div>
<div class="m2"><p>درش بگشاد و قفل از پرده شد باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز شوقش قفل چون زنجیر بگسست</p></div>
<div class="m2"><p>باستقبال او پرده برون جست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کسی کاهن بصدقش مومن آید</p></div>
<div class="m2"><p>دل خصمش چرا چون آهن آید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو شد قفل از سر صدقش سرانداز</p></div>
<div class="m2"><p>چرا قفل دل خصمش نشد باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو اصحاب اندر آن مشهد رسیدند</p></div>
<div class="m2"><p>فرو برده یکی خاکش بدیدند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی کو در گزید مار یارست</p></div>
<div class="m2"><p>توان گفتن که این کس یار غارست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو پیغامبر ابوبکر و عمر را</p></div>
<div class="m2"><p>بصر خواند این یک و سمع آن دگر را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نبی چون هر دو را سمع و بصر خواند</p></div>
<div class="m2"><p>کسی کین دو ندارد کور و کر ماند</p></div></div>