---
title: >-
    (۳) حکایت مأمون خلیفه با غلام
---
# (۳) حکایت مأمون خلیفه با غلام

<div class="b" id="bn1"><div class="m1"><p>غلامی داشت مأمون خلیفه</p></div>
<div class="m2"><p>کزو مهمل نماندی یک لطیفه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خورشیدی به نیکوئی جمالش</p></div>
<div class="m2"><p>خلایق جمله مایل بر وصالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خَم زلفش که دام عنبرین داشت</p></div>
<div class="m2"><p>همه هندوستان در زیر چین داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلی گر زلفِ او در چین نبودی</p></div>
<div class="m2"><p>نثارش نافهٔ مشکین نبودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گویم ز ابروی همچون کمانش</p></div>
<div class="m2"><p>که زاغی بود زلف دلستانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عشق ثُقبهٔ لعلش ز لولو</p></div>
<div class="m2"><p>هزاران ثُقبه در دل مانده هر سو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن ثُقبه چرا و چون نگنجد</p></div>
<div class="m2"><p>که از تنگی نَفَس بیرون نگنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دیری گه مگر می‌خواست مأمون</p></div>
<div class="m2"><p>که آید آن غلام از پوست بیرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که تا مأمون بداند کان پری چهر</p></div>
<div class="m2"><p>قدم چون می‌زند با شاه در مهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلش در مهر مامونست یا نه</p></div>
<div class="m2"><p>ز خطّ عهد بیرونست یا نه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بمعشوقی وفای عشق دارد</p></div>
<div class="m2"><p>باستحقاق جای عشق دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر قومی دلی پُر درد و پُر سوز</p></div>
<div class="m2"><p>به بغداد آمدند از بصره فریاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کامیر المؤمنین ما را دهد داد</p></div>
<div class="m2"><p>که ماراست از امیر بصره فریاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه چندان ظلم کرد و ما کشیدیم</p></div>
<div class="m2"><p>که دیدیم از کسی یا ما شنیدیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر نستانی از وی داد ما تو</p></div>
<div class="m2"><p>مشوّش گردی از فریادِ ما تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نهان آن قوم را فرمود مأمون</p></div>
<div class="m2"><p>که خواهید این غلامم را هم اکنون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر او در پذیرد این امیری</p></div>
<div class="m2"><p>کند زین پس شما را دستگیری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز شه درخواستند آن قوم آنگاه</p></div>
<div class="m2"><p>که ما را این غلامت گر بود شاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه از حکم او دلشاد گردیم</p></div>
<div class="m2"><p>ز ظلم آن امیر آزاد گردیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگه کرد آن زمان سوی غلام او</p></div>
<div class="m2"><p>که تا در عهد عشق آید تمام او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غلام سیمبر را گفت مأمون</p></div>
<div class="m2"><p>درین منصب چه می‌گوئی تو اکنون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر مرکب سوی آن خطه رانی</p></div>
<div class="m2"><p>خطی بنویسمت در پهلوانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غلم آنجایگه می‌بود خاموش</p></div>
<div class="m2"><p>دلش آمد ز شوق بصره در جوش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدانست آن زمان مأمون که آن ماه</p></div>
<div class="m2"><p>بغایت فارغست از عشقِ آن شاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل مأمون ازان دلبر بگردید</p></div>
<div class="m2"><p>ز کار آن نگارش سر بگردید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز عشق او پشیمانی برآورد</p></div>
<div class="m2"><p>وز آن حاصل پریشانی برآورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدل می‌گفت عشق من غلط بود</p></div>
<div class="m2"><p>چه دانستم که معشوقی سقط بود؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدست خویشتن در جای خالی</p></div>
<div class="m2"><p>بعامل نامهٔ بنوشت حالی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که چون آید غلام من بآنجا</p></div>
<div class="m2"><p>خطی آرد بنام خود بر آنجا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان باید که کوی شهر و بازار</p></div>
<div class="m2"><p>همه بصره بیارائی بیکبار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جُلاب آرید و در وی زهر آنگاه</p></div>
<div class="m2"><p>برو ریزید و برگیریدش از راه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>منادی گر زهر سو برنشانید</p></div>
<div class="m2"><p>که می‌گویند واسپش می‌دوانید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که هرکش بر مَلِک مُلک اختیارست</p></div>
<div class="m2"><p>سزای او بتر زین صد هزارست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو حق از بهر خویشت آفریدست</p></div>
<div class="m2"><p>برای قربِ خویشت آوریدست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بنگذارد تو مرد بی خبر را</p></div>
<div class="m2"><p>که باشی یک نَفَس چیزی دگر را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگر بگذاردت کارت فتادست</p></div>
<div class="m2"><p>که صاعی خفیه در بارت نهادست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چرا می‌آید این رفتن گرانت</p></div>
<div class="m2"><p>که می‌گوید خداوند جهانت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که گر آئی به پیش من رونده</p></div>
<div class="m2"><p>باستقبالت آیم من دونده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خدا می‌خواندت تو خفته آخر</p></div>
<div class="m2"><p>چرا می‌پائی ای آشفته آخر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کم از اشتر نهٔ ای مردِ درگاه</p></div>
<div class="m2"><p>که بر بانگ درائی می‌رود راه</p></div></div>