---
title: >-
    المقالة الثامن عشر
---
# المقالة الثامن عشر

<div class="b" id="bn1"><div class="m1"><p>پسر گفتش چو آن خاتم عزیزست</p></div>
<div class="m2"><p>بگو باری که سرّ آن چه چیزست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گر دستم نداد آن خاتم امروز</p></div>
<div class="m2"><p>شوم از علمِ آن باری دلفروز</p></div></div>