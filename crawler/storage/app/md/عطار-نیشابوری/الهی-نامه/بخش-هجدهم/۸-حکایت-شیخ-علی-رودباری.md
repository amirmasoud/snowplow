---
title: >-
    (۸) حکایت شیخ علی رودباری
---
# (۸) حکایت شیخ علی رودباری

<div class="b" id="bn1"><div class="m1"><p>چنین گفتند جمعی هم دیاری</p></div>
<div class="m2"><p>ز لفظ بوعلیّ رودباری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در حمّام رفتم من یکی روز</p></div>
<div class="m2"><p>جوانی تازه دیدم بس دلفروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برخساره چو ماه آسمان بود</p></div>
<div class="m2"><p>به بالا همچو سرو بوستان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر زلفش بپای افکنده دیدم</p></div>
<div class="m2"><p>بروی او جهانی زنده دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خورشید رخش تابنده گشتی</p></div>
<div class="m2"><p>نگشتی آسمان تا بنده گشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزلفش صد هزاران پیچ بودی</p></div>
<div class="m2"><p>اگر بودی درو جان هیچ بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر می‌خواند بر رویش ز دو عَین</p></div>
<div class="m2"><p>بلا و رنج خود چون از صحیحَین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی دل گفت ازان دو چشم بیمار</p></div>
<div class="m2"><p>صحیحت کی شود این رنج و تیمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بیماریش در عَین اوفتادست</p></div>
<div class="m2"><p>صحیحَینم سقیمَین اوفتادست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجان و دل خطش را خط روان بود</p></div>
<div class="m2"><p>بلی باشد روان چون روی آن بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خطش سر سبزی باغ ارم داشت</p></div>
<div class="m2"><p>لب او سرخ روئی نیز هم داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدندان استخوانی لُولُوَش بود</p></div>
<div class="m2"><p>که مروارید کمتر هندوش بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بکش آورده پای آن سیم اندام</p></div>
<div class="m2"><p>نشسته از تکبّر سوی حمّام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی صوفی بخدمت ایستاده</p></div>
<div class="m2"><p>نظر بر روی آن برنا گشاده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمانی بر سرش می‌ریخت آبی</p></div>
<div class="m2"><p>زمانی سرد می کردش شرابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گهی دست و قفای او بمالید</p></div>
<div class="m2"><p>گهی بر سنگ پای او بمالید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو شد از شوخ پاک آن سیم اندام</p></div>
<div class="m2"><p>چو خورشیدی برون آمد ز حمّام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دوید آن صوفی و او را درآورد</p></div>
<div class="m2"><p>برای خشک کردن میزر آورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مصلّی نماز آنگاه خرسند</p></div>
<div class="m2"><p>بزیر پای آن دلخواه بفکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس آنگه جامه اندر بر فکندش</p></div>
<div class="m2"><p>بخور عود در مجمر فکندش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گلاب آورد و پس بر روی او ریخت</p></div>
<div class="m2"><p>ذریره بر شکنج موی او بیخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزودی باد بیزن هم روان کرد</p></div>
<div class="m2"><p>چو بادی بر سر آن گل فشان کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگرچه خدمتش هر دم فزون بود</p></div>
<div class="m2"><p>ولی درچشمِ آن زیبا زبون بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زبان بگشاد صوفی گفت ای ماه</p></div>
<div class="m2"><p>چه می‌خواهی تو زین صوفی گمراه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه باید تا پسندت آید از من</p></div>
<div class="m2"><p>بگو کین خشم چندت آید از من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بمن می ننگری از ناز هرگز</p></div>
<div class="m2"><p>چه سازد با تو این مسکین عاجز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو از صوفی پسر بشنید این راز</p></div>
<div class="m2"><p>بدو گفتا بمیر ورستی از ناز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بشنید این سخن صوفی ازان ماه</p></div>
<div class="m2"><p>یکی آهی بکرد و مرد ناگاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان مُرد از کمال عشق زود او</p></div>
<div class="m2"><p>که گفتی در جهان هرگز نبود او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو گر نتوانی ای مسکین چنین رفت</p></div>
<div class="m2"><p>چگونه خواهی اندر آن زمین رفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر تو این چنین مُردی برستی</p></div>
<div class="m2"><p>وگرنه تا قیامت پای بستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بآخر بوعلی او را کفن ساخت</p></div>
<div class="m2"><p>وز آنجا رفت و کار خویشتن ساخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مگر می‌رفت روزی بوعلی خوش</p></div>
<div class="m2"><p>میان بادیه تنها چو آتش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جوان را دید با دلقی جگر خون</p></div>
<div class="m2"><p>رخی چون زعفران حالی دگرگون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر شیخ آمد و گفت آن جوانم</p></div>
<div class="m2"><p>که از دعوی کُشنده آن فلانم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بکُشتم آن چنان مردی قوی را</p></div>
<div class="m2"><p>چنین گشتم کنون از بدخوئی را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کنون عهدیست با حق این جوان را</p></div>
<div class="m2"><p>که هر سالی کند حجّی فلان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برای او کنم حجّی پیاده</p></div>
<div class="m2"><p>دگر بر گورِ او باشم فتاده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دریغا مرد زرّ و زور بودم</p></div>
<div class="m2"><p>کمال او ندیدم کور بودم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنون هر دم ازان دردم دریغست</p></div>
<div class="m2"><p>شبانروزی ازان مردم دریغست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر تو ذرّهٔ داری ازین درد</p></div>
<div class="m2"><p>زمان عشق بازی این چنین گرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه می‌گویم تو چه مرد نبردی</p></div>
<div class="m2"><p>که تو در عاشق نه زن نه مردی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>درین مجلس نیاری جمع مُردن</p></div>
<div class="m2"><p>مگو دل سوخته چون شمع مُردن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز پیش خویشتن بر بایدت خاست</p></div>
<div class="m2"><p>نیاید عاشقی با عافیت راست</p></div></div>