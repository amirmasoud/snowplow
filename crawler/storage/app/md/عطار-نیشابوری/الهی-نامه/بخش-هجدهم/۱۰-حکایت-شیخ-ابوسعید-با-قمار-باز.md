---
title: >-
    (۱۰) حکایت شیخ ابوسعید با قمار باز
---
# (۱۰) حکایت شیخ ابوسعید با قمار باز

<div class="b" id="bn1"><div class="m1"><p>بصحرا رفت شیخ مهنه ناگاه</p></div>
<div class="m2"><p>گروهی گرم رَو را دید در راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که می‌رفتند بر یک شیوه یک جای</p></div>
<div class="m2"><p>ازار پای چرمین کرده در پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی را شاد بر گردن گرفته</p></div>
<div class="m2"><p>بسی رندانش پیرامن گرفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر پرسید آن شیخ زمانه</p></div>
<div class="m2"><p>که کیست این مرد، گفتند ای یگانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امیر جملهٔ اهل قمارست</p></div>
<div class="m2"><p>که او در پیشهٔ خود مردِ کارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازو پرسید شیخ عالم افروز</p></div>
<div class="m2"><p>که از چه یافتی این میری امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوابش داد رند نانمازی</p></div>
<div class="m2"><p>که من این یافتم از پاک بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزد یک نعره شیخ و گفت دانی</p></div>
<div class="m2"><p>که دارد پاک بازی را نشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امیرست و سرافراز جهانست</p></div>
<div class="m2"><p>که کژبازی بلای ناگهانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه شیران که مرد راه بودند</p></div>
<div class="m2"><p>جهان عشق را روباه بودند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهُش رَو، نیک بنگر، با خبر باش</p></div>
<div class="m2"><p>بلا می‌بارد اینجا، بر حذر باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر داری سر گردن نهادن</p></div>
<div class="m2"><p>برای جان فشانی تن نهادن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مسلَّم باشدت این پاک بازی</p></div>
<div class="m2"><p>وگر نه ناقصی و نانمازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر چون پاک بازان میکنی کار</p></div>
<div class="m2"><p>چو عیسی سوزنی با خود بمگذار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر جز سوزنی با تو بهم نیست</p></div>
<div class="m2"><p>جز آن سوزن حجابت بیش و کم نیست</p></div></div>