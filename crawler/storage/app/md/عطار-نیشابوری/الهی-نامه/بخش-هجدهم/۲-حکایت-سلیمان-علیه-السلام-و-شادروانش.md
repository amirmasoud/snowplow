---
title: >-
    (۲) حکایت سلیمان علیه السلام و شادروانش
---
# (۲) حکایت سلیمان علیه السلام و شادروانش

<div class="b" id="bn1"><div class="m1"><p>مگر یک روز می‌شد با سپاهی</p></div>
<div class="m2"><p>ولی بر روی شادروان براهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآمد خاطرش از ملک ناگاه</p></div>
<div class="m2"><p>که کیست امروز در عالم چو من شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرو شد گوشهٔ زان قصرِ عالی</p></div>
<div class="m2"><p>سلیمان بانگ زد بر باد حالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که شادروان چرا کردی چنین تو</p></div>
<div class="m2"><p>کرا افکند خواهی بر زمین تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم گفت ای سلیمان من گنه کار</p></div>
<div class="m2"><p>تو زان اندیشهٔ کژ دل نگه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین دارم من از درگاه فرمان</p></div>
<div class="m2"><p>که چون دل را نگه دارد سلیمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگه می‌دار شادروان او را</p></div>
<div class="m2"><p>وگرنه سر منه فرمان او را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسوی ملک چون کردی دمی رای</p></div>
<div class="m2"><p>ز شادروانت شد یک گوشه از جای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قناعت بایدت پیوسته حاصل</p></div>
<div class="m2"><p>که تا بر تو نگردد ملک زایل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که مغز ملک و ملک استطاعت</p></div>
<div class="m2"><p>نخواهد بود چیزی جز قناعت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولی مغز قناعت فقر آمد</p></div>
<div class="m2"><p>تو شاهی گر بفقرت فخر آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر خواهی تو هم ملک جهانی</p></div>
<div class="m2"><p>مکن کبر و قناعت کن زمانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قناعت بود آن خاتم که او داشت</p></div>
<div class="m2"><p>بخاتم یافت آن عالم که او داشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان ملکی عظیمش بود صافی</p></div>
<div class="m2"><p>که قانع بود در زنبیل بافی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازان خورشید سلطانی بلندست</p></div>
<div class="m2"><p>که از آفاق یک قرصش پسندست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازان در ملک مه را احترامست</p></div>
<div class="m2"><p>که او را گردهٔ ماهی تمامست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو پای از دست دادی پی چه خواهی</p></div>
<div class="m2"><p>ملک چون هست مُلک وی چه خواهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تراگر بی مَلک ملک جهانست</p></div>
<div class="m2"><p>ازاین شومیت و هردم بیم جانست</p></div></div>