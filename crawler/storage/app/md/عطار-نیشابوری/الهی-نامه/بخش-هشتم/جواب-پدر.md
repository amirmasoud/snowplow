---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گنج سخن را کرد در باز</p></div>
<div class="m2"><p>پسر را گفت ای جویندهٔ راز</p></div></div>