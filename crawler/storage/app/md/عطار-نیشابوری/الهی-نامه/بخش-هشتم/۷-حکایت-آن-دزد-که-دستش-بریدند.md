---
title: >-
    (۷) حکایت آن دزد که دستش بریدند
---
# (۷) حکایت آن دزد که دستش بریدند

<div class="b" id="bn1"><div class="m1"><p>ببریدند دزدی را مگر دست</p></div>
<div class="m2"><p>نزد دَم دستِ خود بگرفت و برجست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفتند ای محنت رسیده</p></div>
<div class="m2"><p>چه خواهی کرد این دست بریده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت او که نام دوستی خاص</p></div>
<div class="m2"><p>بر آنجا کرده بودم نقش ز اخلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون تا زنده‌ام اینم تمامست</p></div>
<div class="m2"><p>که بی این زندگی بر من حرامست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دستم گر چه قسمی جز الم نیست</p></div>
<div class="m2"><p>چو بر دستست نام دوست غم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ابلیس لعین اسرار دان بود</p></div>
<div class="m2"><p>اگر سجده نمی‌کرد او ازان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خلق خود دریغش آمد آن راز</p></div>
<div class="m2"><p>نکرد آن سجده، دعوی کرد آغاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که تا هم او وهم خلق جهان هم</p></div>
<div class="m2"><p>نه بینند آن دَر و آن آستان هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که تا نوری ازان در پردهٔ عز</p></div>
<div class="m2"><p>نگردد در نظر آلوده هرگز</p></div></div>