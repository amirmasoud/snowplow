---
title: >-
    (۱) حکایت بچّۀ ابلیس با آدم و حوّا علیه السلام
---
# (۱) حکایت بچّۀ ابلیس با آدم و حوّا علیه السلام

<div class="b" id="bn1"><div class="m1"><p>حکیم ترمذی کرد این حکایت</p></div>
<div class="m2"><p>ز حال آدم و حوا روایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بعد از توبه چون با هم رسیدند</p></div>
<div class="m2"><p>ز فردوس آمده کُنجی گزیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر آدم بکاری رفت بیرون</p></div>
<div class="m2"><p>بر حوا دوید ابلیس ملعون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی بچّه بَدش خنّاس نام او</p></div>
<div class="m2"><p>بحوا دادش و برداشت گام او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چوآدم آمد و آن بچه را دید</p></div>
<div class="m2"><p>ز حوا خشمگین شد زو بپرسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که او را از چه پذرفتی ز ابلیس</p></div>
<div class="m2"><p>دگرباره شدی مغرور تلبیس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکشت آن بچه را و پاره کردش</p></div>
<div class="m2"><p>بصحرا برد و پس آواره کردش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آدم شد دگر ره آمد ابلیس</p></div>
<div class="m2"><p>بخواند آن بچهٔ خود را بتلبیس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درآمد بچهٔ او پاره پاره</p></div>
<div class="m2"><p>بهم پیوست تا گشت آشکاره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو زنده گشت زاری کرد بسیار</p></div>
<div class="m2"><p>که تا حوا پذیرفتش دگربار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو رفت ابلیس و آدم آمد آنجا</p></div>
<div class="m2"><p>بدید آن بچهٔ او را هم آنجا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برنجانید حوا را دگر بار</p></div>
<div class="m2"><p>که خواهی سوختن ما را دگر بار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بکشت آن بچه و آتش برافروخت</p></div>
<div class="m2"><p>وزان پس بر سر آن آتشش سوخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه خاکستر او داد بر باد</p></div>
<div class="m2"><p>برفت القصه از حوا بفریاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دگر بار آمد ابلیس سیه روی</p></div>
<div class="m2"><p>بخواند آن بچهٔ خود را زهر سوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درآمد جملهٔ خاکستر از راه</p></div>
<div class="m2"><p>بهم پیوسته شد آن بچه آنگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو شد زنده بسی سوگند دادش</p></div>
<div class="m2"><p>که بپذیر و مده دیگر ببادش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که نتوانم بدادن سر براهش</p></div>
<div class="m2"><p>چو بازآیم برم زین جایگاهش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت این و برفت و آدم آمد</p></div>
<div class="m2"><p>ز خنّاسش دگر باره غم آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملامت کرد حوا را ز سر باز</p></div>
<div class="m2"><p>که از سر در شدی با دیو دمساز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمی‌دانم که شیطان ستمگار</p></div>
<div class="m2"><p>چه می‌سازد برای ما دگر بار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگفت این و بکشت آن بچه را باز</p></div>
<div class="m2"><p>پس آنگه قلیهٔ زو کرد آغاز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بخورد آن قلیه با حوا بهم خوش</p></div>
<div class="m2"><p>وزانجا شد بکاری دل پُر آتش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دگر بار آمد ابلیس لعین باز</p></div>
<div class="m2"><p>بخواند آن بچّهٔ خود را بآواز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو واقف گشت خنّاس از خطابش</p></div>
<div class="m2"><p>بداد از سینهٔ حوّا جوابش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو آوازش شنید ابلیسِ مکّار</p></div>
<div class="m2"><p>مرا گفتا میسّر شد همه کار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا مقصود این بودست ما دام</p></div>
<div class="m2"><p>که گیرم در درون آدم آرام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو خود را در درون او فکندم</p></div>
<div class="m2"><p>شود فرزند آدم مُستمندم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گهی در سینهٔ مردم ز خنّاس</p></div>
<div class="m2"><p>نهم صد دام رُسوائی زوسواس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گهی صدگونه شهوة در درونش</p></div>
<div class="m2"><p>برانگیزم شوم در رگ چو خونش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گهی از بهر طاعت خوانمش خاص</p></div>
<div class="m2"><p>وزان طاعت ریا خواهم نه اخلاص</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هزاران جادوئی آرم دگرگون</p></div>
<div class="m2"><p>که مردم را برم از راه بیرون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شیطان در درونت رخت بنهاد</p></div>
<div class="m2"><p>بسلطانی نشست وتخت بنهاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ترا در جادوئی همت قوی کرد</p></div>
<div class="m2"><p>که تا جانت هوای جادوئی کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر شیطان چنین ره زن نبودی</p></div>
<div class="m2"><p>چنین سلطان مرد و زن نبودی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در افکندست خلقی را بغم در</p></div>
<div class="m2"><p>همه گیتی برآورده بهم بر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهر کُنجی دلی در خواب کرده</p></div>
<div class="m2"><p>بهرجائی گِلی در آب کرده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ترا ره می‌زند وز درد این کار</p></div>
<div class="m2"><p>چوابرت چشم ازان گشتست خون بار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر آدم را که در یک دانه نگریست</p></div>
<div class="m2"><p>به سیصد سال می‌بایست بگریست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ببین کابلیس را در لعن و در رشک</p></div>
<div class="m2"><p>ز دیده چند باید ریختن اشک</p></div></div>