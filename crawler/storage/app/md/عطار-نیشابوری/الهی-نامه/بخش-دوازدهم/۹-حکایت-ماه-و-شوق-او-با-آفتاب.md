---
title: >-
    (۹) حکایت ماه و شوق او با آفتاب
---
# (۹) حکایت ماه و شوق او با آفتاب

<div class="b" id="bn1"><div class="m1"><p>قمر گفتا که من در عشق خورشید</p></div>
<div class="m2"><p>جهان پُر نور خواهم کرد جاوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفتند اگر هستی درین راست</p></div>
<div class="m2"><p>شبانروزی بتگ می‌بایدت خاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که تا در وی رسی و چون رسیدی</p></div>
<div class="m2"><p>درو فانی شوی در ناپدیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوزی آن زمان تحت الشُعاعش</p></div>
<div class="m2"><p>وجودت خفض گردد زارتفاعش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو ازتحت الشعاع آئی پدیدار</p></div>
<div class="m2"><p>شود خلقی جمالت را خریدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بانگشتت بیکدیگر نمایند</p></div>
<div class="m2"><p>بدیدارت نظرها برگشایند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه افتادست تا نوری بیک بار</p></div>
<div class="m2"><p>ز پیش نور می‌آید پدیدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی سرگشته فانی گشته بی باک</p></div>
<div class="m2"><p>هویدا شد ز جرم باقی خاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی خود سوخته تحت الشعاعی</p></div>
<div class="m2"><p>وصالی یافت بعد از انقطاعی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب دو گفته با چندان جمالش</p></div>
<div class="m2"><p>مدد گیرد ز نقصان هلالش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو این شب خویش آراید یقینست</p></div>
<div class="m2"><p>بدو کس ننگرد کو خویش بینست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی هر گه که بینی چون خلالش</p></div>
<div class="m2"><p>درو بینند یعنی در هلالش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو تا هستی خود در پیش داری</p></div>
<div class="m2"><p>بلای جاودان با خویش داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز چرک شرکت آنگه دل بگیرد</p></div>
<div class="m2"><p>که دل در بیخودی منزل بگیرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زشیر شرک اگر خویت شود باز</p></div>
<div class="m2"><p>بلوغت افتد از توحید آغاز</p></div></div>