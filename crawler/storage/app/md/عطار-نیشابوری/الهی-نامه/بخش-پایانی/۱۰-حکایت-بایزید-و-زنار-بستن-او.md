---
title: >-
    (۱۰) حکایت بایزید و زنّار بستن او
---
# (۱۰) حکایت بایزید و زنّار بستن او

<div class="b" id="bn1"><div class="m1"><p>چو در نزع اوفتاد آن پیر بسطام</p></div>
<div class="m2"><p>بیاران گفت کای قوم نکوکام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی زنّار آریدم هم اکنون</p></div>
<div class="m2"><p>که تا بر بندد این مسکینِ مجنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروشی از میان قوم برخاست</p></div>
<div class="m2"><p>که از زنّار ناید کار تو راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه باشد ای سلطان اسرار</p></div>
<div class="m2"><p>میان بایزید آنگاه و زنّار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر ره خواست زنّاری ز اصحاب</p></div>
<div class="m2"><p>نمی‌آورد کس آن کار را تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بآخر کرد شیخ الحاح بسیار</p></div>
<div class="m2"><p>نمی‌دانست کس درمانِ آن کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه گفتند اگر بر شیخ تقدیر</p></div>
<div class="m2"><p>شقاوة خواستست آنرا چه تدبیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی زنّار آوردند اصحاب</p></div>
<div class="m2"><p>که تا بر بست و بگشاد ازدو چشم آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس آنگه روی را در خاک مالید</p></div>
<div class="m2"><p>بسوز جان و درد دل بنالید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسی افشاند خون ازچشمِ خونبار</p></div>
<div class="m2"><p>وزان پس از میان ببرید زنّار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبان بگشاد کای قیّومِ مطلق</p></div>
<div class="m2"><p>بحقّ آنکه جاویدان توئی حق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که چون این دم بریدم بند زنّار</p></div>
<div class="m2"><p>همان هفتاد ساله گبرم انگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه گبری کو درین دم باز گردد</p></div>
<div class="m2"><p>بیک فضل تو صاحب راز گردد؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من آن گبرم که این دم بازگشتم</p></div>
<div class="m2"><p>چه گر دیر آمدم هم باز گشتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفت این و شهادة تازه کرد او</p></div>
<div class="m2"><p>بسی زاری بی اندازه کرد او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگرچه راه افزون آمدم من</p></div>
<div class="m2"><p>همان انگار کاکنون آمدم من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو میدانی که من هیچم الهی</p></div>
<div class="m2"><p>ز هیچی این همه پس می چه خواهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه دارم، درد بی اندازه دارم</p></div>
<div class="m2"><p>ز مال و ملک قلبی تازه دارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو دل دارم خرابی و کبابی</p></div>
<div class="m2"><p>چه می‌خواهی خراجی از خرابی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر توعجز می‌خواهی بسی هست</p></div>
<div class="m2"><p>ندانم تا چو من عاجز کسی هست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غمم جز تو دگر کس می‌نداند</p></div>
<div class="m2"><p>تو می‌دانی اگرکس می‌نداند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه می‌گویم چو دانم ناظری تو</p></div>
<div class="m2"><p>چه می‌جویم چو دانم حاضری تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو خود بخشی اگر جویم وگرنه</p></div>
<div class="m2"><p>تو خود دانی اگر گویم وگرنه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه بی سر تنیم افتاده در بند</p></div>
<div class="m2"><p>چه برخیزد ازین بی سر تنی چند؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو از خلقت نه سود ونه زیانست</p></div>
<div class="m2"><p>همه رحمت برای عاصیانست</p></div></div>