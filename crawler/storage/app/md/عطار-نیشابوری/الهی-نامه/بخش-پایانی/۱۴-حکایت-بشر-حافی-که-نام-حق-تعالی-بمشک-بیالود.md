---
title: >-
    (۱۴) حکایت بشر حافی که نام حق تعالی بمشک بیالود
---
# (۱۴) حکایت بشر حافی که نام حق تعالی بمشک بیالود

<div class="b" id="bn1"><div class="m1"><p>در اوّل روز می‌شد بشرِ حافی</p></div>
<div class="m2"><p>ز دُردی مست امّا جانش صافی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر یکپاره کاغذ یافت در راه</p></div>
<div class="m2"><p>بر آن کاغذ نوشته نامِ الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عالم جز جَوی حاصل نبودش</p></div>
<div class="m2"><p>بداد و مشک بستد اینت سودش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبانگه نامِ حق را مردِ حق جوی</p></div>
<div class="m2"><p>بمشک خود معطّر کرد وخوش بوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن شب دید وقت صبح خوابی</p></div>
<div class="m2"><p>که کردندی به سوی او خطابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ای برداشته نام من از خاک</p></div>
<div class="m2"><p>بحرمت کرده هم خوش بوی و هم پاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا مرد حقیقت جوی کردیم</p></div>
<div class="m2"><p>همت پاک و همت خوش بوی کردیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدایا بس که این عطّارِ خوش گوی</p></div>
<div class="m2"><p>بعطر نظم نامت کرد خوش بوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه گر عطّار ازان خوش گوی بودست</p></div>
<div class="m2"><p>که نامت جاودان خوش بوی بودست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو هم از فضل خاک آن درش کن</p></div>
<div class="m2"><p>بنام خویشتن نام آورش کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که جز از فضل تو روئی ندارد</p></div>
<div class="m2"><p>گر از طاعت سر موئی ندار</p></div></div>