---
title: >-
    (۱۲) حکایت رندی که از دکانی چیزی می‬‌خواست
---
# (۱۲) حکایت رندی که از دکانی چیزی می‬‌خواست

<div class="b" id="bn1"><div class="m1"><p>یکی رندی میان داغ و دردی</p></div>
<div class="m2"><p>ستاده بود بر دکان مردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازو می‌خواست چیزی، می ندادش</p></div>
<div class="m2"><p>بسی بر پیش دکان ایستادش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان بگشاد دکاندار پر پیچ</p></div>
<div class="m2"><p>که تا تو زخم نکنی ندهمت هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کردی زخم، از من نقد می‌جوی</p></div>
<div class="m2"><p>وگر نه همچنین می‌باش و می‌گوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برهنه کرد رند اندام حالی</p></div>
<div class="m2"><p>بدو گفتا نگه کن از حوالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بر من ز سر درگیر تا پای</p></div>
<div class="m2"><p>توانی دید بی صد زخم یک جای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو کانجایگه زخمی رسانم</p></div>
<div class="m2"><p>که بی صد زخم جائی می‌ندانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بی زخم هستم جایگاهی</p></div>
<div class="m2"><p>نباشد چشم زخم از تو گناهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نیست از پای تا سر بی جراحت</p></div>
<div class="m2"><p>بده چیزی که یابم از تو راحت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنم چون جمله مجروحست اکنون</p></div>
<div class="m2"><p>ازین پس نوبة روحست اکنون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدایا من چو آن رند گدایم</p></div>
<div class="m2"><p>که بر تن نیست بی صد زخم جایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز سر تا پای من چندان که جوئی</p></div>
<div class="m2"><p>جراحت پُر بوَد چندان که گوئی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمی هرگز براحت برنیارم</p></div>
<div class="m2"><p>که سر از صد جراحت بر نیارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دمی گر صد جراحت می‌نیابم</p></div>
<div class="m2"><p>ز عمر خویش راحت می‌نیابم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر خود پای تا سر عین دردم</p></div>
<div class="m2"><p>ز دردی کافرم گر سیر گردم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غم تو بایدم از عالم تو</p></div>
<div class="m2"><p>ندارم غم چو من دارم غم تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دریغا جان ندارم صد هزاران</p></div>
<div class="m2"><p>که در پای غمت ریزم چو باران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو حرف ها و هو آید بگوشم</p></div>
<div class="m2"><p>همه در ها و هو و در خروشم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا دیدم خودی خود ستُردم</p></div>
<div class="m2"><p>بتو زنده شدم وز خویش مُردم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگردایم چنین باشم کمالست</p></div>
<div class="m2"><p>وگر با خویشتن رفتم زوالست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خدایا دست این شوریده دل گیر</p></div>
<div class="m2"><p>خلاصم ده ازین زندان دلگیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آن ساعت که جان آید بحلقم</p></div>
<div class="m2"><p>نماند هیچ امیدی بخلقم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تنم را روشنائی لحد بخش</p></div>
<div class="m2"><p>دلم را آشنائی ابد بخش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو زایل گردد این مُلک وجودم</p></div>
<div class="m2"><p>مکن بی بهره از دریای جودم</p></div></div>