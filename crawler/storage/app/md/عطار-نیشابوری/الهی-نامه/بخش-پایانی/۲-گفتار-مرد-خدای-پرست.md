---
title: >-
    (۲) گفتار مرد خدای پرست
---
# (۲) گفتار مرد خدای پرست

<div class="b" id="bn1"><div class="m1"><p>چنین گفتست روزی حق پرستی</p></div>
<div class="m2"><p>که او را بود در اسرار دستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که هر چیزی که هست و بایدت نیز</p></div>
<div class="m2"><p>ازان چیزت فراغت بِه ازان چیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا چیزی که در هر دو جهانست</p></div>
<div class="m2"><p>به از بودش بسی نابود آنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر هر دو جهان دار السلامست</p></div>
<div class="m2"><p>تماشا گاه جانم این تمامست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جان پاکِ من فردوس باشد</p></div>
<div class="m2"><p>مرا صد مشتری در قوس باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهشتی این چنین و همدمی نه</p></div>
<div class="m2"><p>دلی پر سرِّ عشق و محرمی نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو هر همدم که می‌بینم حجابست</p></div>
<div class="m2"><p>مرا پس هر دمی همدم کتابست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کس را می‌نبینم همدم خویش</p></div>
<div class="m2"><p>در آنجا می فرو گویم غم خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا درمغزِ دل دردیست تنها</p></div>
<div class="m2"><p>کزو می‌زاید این چندین سخنها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر کم گویم و گر بیش گویم</p></div>
<div class="m2"><p>چه می‌جویم کسی، با خویش گویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برآوردم بگرد عالمی دست</p></div>
<div class="m2"><p>نداد از هیچ نوعم همدمی دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر داد ودهد یک همدمم داد</p></div>
<div class="m2"><p>نداد اوداد لیکن هم دمم داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز چندین آدمی درهیچ جائی</p></div>
<div class="m2"><p>نمی‌بینم سر موئی وفائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو در من نیز یک ذرّه وفا نیست</p></div>
<div class="m2"><p>ز غیری این وفا جُستن رو انیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو من محرم نَیَم خود را زمانی</p></div>
<div class="m2"><p>کِه باشد محرم من در جهانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز همراهان دین مردی ندیدم</p></div>
<div class="m2"><p>زاخوان صفا گردی ندیدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسی رفتم هم آنجا ام که بودم</p></div>
<div class="m2"><p>نمی‌دانم کزین رفتن چه سودم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دلا چون هم نشینانت برفتند</p></div>
<div class="m2"><p>رفیقان و قرینانت برفتند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو تا کَی باد پیمائی ز سودا</p></div>
<div class="m2"><p>برَو تا کَی کنی امروز و فردا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بخوردی همچو بیکاران جهانی</p></div>
<div class="m2"><p>غم کارت نمی‌بینم زمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگرچه صبحدم را هم دمی هست</p></div>
<div class="m2"><p>ولی صادق نداد آن همدمش دست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بکن کاری که وقت امروز داری</p></div>
<div class="m2"><p>برافروز آتشی چون سوز داری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه خفتند چه مست و چه هشیار</p></div>
<div class="m2"><p>تو کَی خواهی شدن از خواب بیدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ترا تا چند ازین باریک گفتن</p></div>
<div class="m2"><p>که می‌باید ترا با ریگ رُفتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو ابرهیم گفتار آمدی تو</p></div>
<div class="m2"><p>چرا نمرود رفتار آمدی تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو نتوانی که مرد کار میری</p></div>
<div class="m2"><p>زهی حسرت اگر مُردار میری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به گرد قال آخر چند گردی</p></div>
<div class="m2"><p>قدم در حال نِه گر شیر مردی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دل تو گر ز قال آرام گیرد</p></div>
<div class="m2"><p>کجا از حالِ مردان نام گیرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو قشری بیش نیست این قال آخر</p></div>
<div class="m2"><p>طلب کن همچومردان حال آخر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو تو عمر عزیز خود بیکبار</p></div>
<div class="m2"><p>همه در گفت کردی،کی کنی کار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بُت تو شعر می‌بینم همیشه</p></div>
<div class="m2"><p>ترا جز بت پرستی نیست پیشه</p></div></div>