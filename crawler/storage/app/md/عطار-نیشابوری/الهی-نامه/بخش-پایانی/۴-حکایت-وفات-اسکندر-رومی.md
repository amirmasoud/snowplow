---
title: >-
    (۴) حکایت وفات اسکندر رومی
---
# (۴) حکایت وفات اسکندر رومی

<div class="b" id="bn1"><div class="m1"><p>چو اسکندر ز دنیا رفت بیرون</p></div>
<div class="m2"><p>حکیمی گفت ای شاه همایون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو زیر خاک می‌گشتی چنین گم</p></div>
<div class="m2"><p>چرا می‌کردی آن چندان تنّعم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریغا و دریغا روزگارم</p></div>
<div class="m2"><p>که دایم جز دریغا نیست کارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نقد روزگار خود بدیدم</p></div>
<div class="m2"><p>امید از خویشتن کلّی بریدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه در خون جان خویش بودم</p></div>
<div class="m2"><p>که تا بودم زیان خویش بودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بامّید بهی تا کِم خبر بود</p></div>
<div class="m2"><p>همه عمرم بسر شد ور بتر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان چون صحّتم بستد مرض داد</p></div>
<div class="m2"><p>جوانی برد و پیری در عوض داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو من هم نیستم از جسم و جانی</p></div>
<div class="m2"><p>نخواهم من که من باشم زمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجز مردن مرا روئی نماندست</p></div>
<div class="m2"><p>ازان کم زندگی موئی نماندست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرچه از فنا موئی ندیدم</p></div>
<div class="m2"><p>بجز فانی شدن روئی ندیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا گه ماتمست و گاه عیدست</p></div>
<div class="m2"><p>که گاهم وعده و گاهی وعیدست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلی بود از همه مُلک جهانم</p></div>
<div class="m2"><p>همه خون گشت ودیگر می‌ندانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهی اندوهِ گوناگون که دلراست</p></div>
<div class="m2"><p>زهی این آتش و این خون که دلراست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرو رفتن بدین دریا یقینست</p></div>
<div class="m2"><p>ولی تا چون برآیم، بیمِ اینست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرا از مرگ دل پُر پیچ دارم</p></div>
<div class="m2"><p>چو بر هیچم نه دل بر هیچ دارم؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه عمرم درافسانه بسر شد</p></div>
<div class="m2"><p>کِه خواهد از پی عمری دگر شد؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تهی دستم که کارم پُر خَلَل ماند</p></div>
<div class="m2"><p>ز حیرت پای جانم در وَحَل ماند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو قوم موسی ام در تیه مانده</p></div>
<div class="m2"><p>هم از تعطیل در تشبیه مانده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی نه خوانده‌ام نه رانده‌ام من</p></div>
<div class="m2"><p>میان کفر و ایمان مانده‌ام من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون در گوشهٔ حیران نشستم</p></div>
<div class="m2"><p>ستون کردم بزیر روی دستم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرت اندوه می‌باید جهانی</p></div>
<div class="m2"><p>ننزدیک دلم بنشین زمانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که چندانی غم و اندوه دارم</p></div>
<div class="m2"><p>که گوئی بر دلی صد کوه دارم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا در دست هر ساعت هزاران</p></div>
<div class="m2"><p>که بر دل درد می‌بارد چو باران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گل عمر عزیزم بر سر خار</p></div>
<div class="m2"><p>به پایان بُردم و من بر سر کار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو نتوان داد شرح سرگذشتم</p></div>
<div class="m2"><p>نَفَس با کام بُردم گنگ گشتم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه گویم کانچه گفتم هست گفته</p></div>
<div class="m2"><p>کرا گویم، خلایق جمله خفته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زبان علم می‌جوشد چو خورشید</p></div>
<div class="m2"><p>زبان معرفت گنگست جاوید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو مستی حیرت خود باز گفتم</p></div>
<div class="m2"><p>چو مشتی خاک زیر خاک خفتم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا گوئی مگو! دیگر نگویم</p></div>
<div class="m2"><p>چه سازم من بسوزم گر نگویم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز من دایم سخن پرسید آخر</p></div>
<div class="m2"><p>ز سوز من نمی‌ترسید آخر؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عزیزا با تو گفتم ماجرائی</p></div>
<div class="m2"><p>مدار آخر دریغ ازمن دعائی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر از تو یک دعائی پاک آید</p></div>
<div class="m2"><p>مرا صد نور ازان درخاک آید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کسی را چون بچیزی دست نرسد</p></div>
<div class="m2"><p>وگر گه گه رسد پیوست نرسد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همان بهتر که بی روی و ریائی</p></div>
<div class="m2"><p>سحرگاهان بسازد با دعائی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کنون از اهل دل درخلوة خاص</p></div>
<div class="m2"><p>دعای خویش می‌خواهم باخلاص</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>غرض زین گفت و گویم جز دعا نیست</p></div>
<div class="m2"><p>که کار بی‌غرض جز از خدا نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عزیزا با تو گفتم حالِ مردان</p></div>
<div class="m2"><p>تو گر مردی فراموشم مگردان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ترا گر ذرّهٔ زین راز روزیست</p></div>
<div class="m2"><p>همه ساز تودایم سینه سوزیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر ماتم زده باشی درین کار</p></div>
<div class="m2"><p>ترا نوحه گری باشد سزاوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ولی تو خود ز رعنائی چنانی</p></div>
<div class="m2"><p>که نوحه بشنوی بازیچه دانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو نوحه لایق آزادگانست</p></div>
<div class="m2"><p>که نوحه کار کار افتادگانست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر تو عاشقی گم کرده یاری</p></div>
<div class="m2"><p>تو آن سرگشتهٔ افتاده کاری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو می‌جوئی نشان از بی‌نشان باز</p></div>
<div class="m2"><p>ازین جستن نه اِستی یک زمان باز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو چیزی گم نکردی ای عجب تو</p></div>
<div class="m2"><p>چه می‌جوئی تو با چندین طلب تو</p></div></div>