---
title: >-
    ابیات برگزیدهٔ از روایت دوم دیباچۀ الهی نامه از روی نسخه‌های دیگر
---
# ابیات برگزیدهٔ از روایت دوم دیباچۀ الهی نامه از روی نسخه‌های دیگر

<div class="b" id="bn1"><div class="m1"><p>بنام آنک ملکش بی زوالست</p></div>
<div class="m2"><p>بوصفش نطق صاحب عقل لالست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفرّح نامهٔ جانهاست نامش</p></div>
<div class="m2"><p>سر فهرست دیوانهاست نامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نامش پُر شکر شد کام جانها</p></div>
<div class="m2"><p>زیادش پر گهر تیغ زبانها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بی یادِ او بوئیست رَنگیست</p></div>
<div class="m2"><p>وگر بی نام او نامیست ننگیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خداوندی که چندانی که هستیست</p></div>
<div class="m2"><p>همه در جنب ذاتش عین پستیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ذاتش برترست از هرچه دانیم</p></div>
<div class="m2"><p>چگونه شرح آن کردن توانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدست صنع گوی مرکز خاک</p></div>
<div class="m2"><p>فکنده درخم چوگان افلاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عقل هیچ کس بالای او نیست</p></div>
<div class="m2"><p>کسی دانندهٔ آلای او نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه نفی جهان اثباتش آمد</p></div>
<div class="m2"><p>همه عالم دلیل ذاتش آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفاتش ذات و ذاتش چون صفاتست</p></div>
<div class="m2"><p>چو نیکو بنگری خود جمله ذاتست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وجود جمله ظلّ حضرت اوست</p></div>
<div class="m2"><p>همه آثار صنع قدرت اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکوگوئی نکو گفتست در ذات</p></div>
<div class="m2"><p>که التوحید اِسقاطُ الاضافات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهی رتبت که ازمه تا بماهی</p></div>
<div class="m2"><p>بود پیشش چو موئی از سیاهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهی عزّت که چندان بی نیازیست</p></div>
<div class="m2"><p>که چندین عقل و جان آنجا ببازیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهی حشمت که گر در جان درآید</p></div>
<div class="m2"><p>ز هر یک ذرّه صد طوفان برآید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زهی وحدت که موئی در نگنجد</p></div>
<div class="m2"><p>در آن وحدت جهان موئی نسنجد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زهی رحمت که گر یک ذرّه ابلیس</p></div>
<div class="m2"><p>بیابد گوی برباید ز ادریس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی غیرت که گر بر عالم افتد</p></div>
<div class="m2"><p>بیک ساعت دو عالم برهم افتد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهی هیبت که گر یک ذرّه خورشید</p></div>
<div class="m2"><p>نیابد گم شود در سایه جاوید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زهی حرمت که ازتعظیم آن جاه</p></div>
<div class="m2"><p>نیابد کس ورای اوبدان راه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زهی ملکت که واجب گشت لابد</p></div>
<div class="m2"><p>که نه نقصان یذیرد نه تزاید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زهی قوّت که گرخواهد بیک دم</p></div>
<div class="m2"><p>زمین چون موم گرداند فلک هم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهی شربت که در خون می‌زند جان</p></div>
<div class="m2"><p>بامید سَقاکُم رَبُّکُم خوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زهی ساحت که گر عالم نبودی</p></div>
<div class="m2"><p>سر موئی از آنجا کم نبودی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زهی غایت که چشم عقل و ادراک</p></div>
<div class="m2"><p>بماند از بُعدِ آن افکنده بر خاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهی مهلت که چون هنگام آید</p></div>
<div class="m2"><p>بموئی عالمی دردام آید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زهی شدّت بحجّت برگرفتن</p></div>
<div class="m2"><p>نه برگ خامشی نه روی گفتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زهی عزلت که چندانی زن و مرد</p></div>
<div class="m2"><p>دویدند و ندیدند از رهش گرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهی غفلت که ما را کرد زنجیر</p></div>
<div class="m2"><p>وگرنه نیست ازما هیچ تقصیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهی طاقت که گر ما زین امانت</p></div>
<div class="m2"><p>برون آئیم ناکرده خیانت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زهی حسرت که خواهد بود ما را</p></div>
<div class="m2"><p>ولی حسرت ندارد سود ما را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جهان عشق را پای و سری نیست</p></div>
<div class="m2"><p>بجز خون دل او را رهبری نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کسی عاشق بوَد کز پای تا فرق</p></div>
<div class="m2"><p>چوگل در خون شود اوّل قدم غرق</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خداوندا بسی بیهوده گفتم</p></div>
<div class="m2"><p>فراوان بوده و نابوده گفتم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگرچه جُرمِ عاصی صد جهانست</p></div>
<div class="m2"><p>ولی یک ذرّه فضلت بیش ازانست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو ما را نیست جز تقصیر طاعت</p></div>
<div class="m2"><p>چه وزن آریم؟ مشتی کم بضاعت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کنون چون اوفتاد این کار ما را</p></div>
<div class="m2"><p>خداوندا بما مگذار ما را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مبرا از کم و چون و چرائی</p></div>
<div class="m2"><p>ورای عالم و خلقی ورائی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خدایا رحمتت در یای عام است</p></div>
<div class="m2"><p>از آنجا قطرهٔ ما را تمام است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر آلایش خلق گنه کار</p></div>
<div class="m2"><p>در آن دریا فرو شوئی بیکبار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نگردد تیره آن دریا زمانی</p></div>
<div class="m2"><p>ولی روشن شود کارِ جهانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه کم گردد ازان دریای رحمت</p></div>
<div class="m2"><p>که یک قطره کنی بر خلق قسمت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خوشا هائی ز حق و ز بنده هوئی</p></div>
<div class="m2"><p>میان بنده و حق های و هوئی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نداری در همه عالم کسی تو</p></div>
<div class="m2"><p>چرا بر خود نمی‌گرئی بسی تو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر صد آشنا درخانه داری</p></div>
<div class="m2"><p>چو مُردی آن همه بیگانه داری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بآسانیت این اندوه ندهند</p></div>
<div class="m2"><p>بدست کاه برگی کوه ندهند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرت یک ذرّه این اندوه باید</p></div>
<div class="m2"><p>صفای بحر و صبر کوه باید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر پیش از اجل یک دم بمیری</p></div>
<div class="m2"><p>در آن یک دم همه عالم بگیری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر آگه شوی ای مردِ مهجور</p></div>
<div class="m2"><p>که ازنزد کِه ماندی این چنین دور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز حسرت داغ بر پهلو نهی تو</p></div>
<div class="m2"><p>سر تشویش بر زانو نهی تو</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر شایستهٔ راه خدا را</p></div>
<div class="m2"><p>بکلی میل کش چشم هوا را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو نابینا شود چشم هوایت</p></div>
<div class="m2"><p>بحق بینا شود چشم هُدایت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تحیّر را نهایت نیست پیدا</p></div>
<div class="m2"><p>که یابد باز یک سوزن ز دریا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهان را چون رباطی با دو در دان</p></div>
<div class="m2"><p>که چون زین در درآئی بگذری زان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>توغافل خفته وز هیچت خبر نه</p></div>
<div class="m2"><p>بخواهی مُرد اگر خواهی وگرنه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ترا گر خود گدائی ور شهنشاه</p></div>
<div class="m2"><p>سه گز کرباس و ده خشتست همراه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بسی کردست گردون شعله کاری</p></div>
<div class="m2"><p>نخواهد بود کس را رستگاری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زهر چیزی که داری کام و ناکام</p></div>
<div class="m2"><p>جدا می‌بایدت گشتن سرانجام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وگر ملکت ز ماهی تا بماهست</p></div>
<div class="m2"><p>سرانجامت بدین دروازه راهست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>وگر اسکندری، دنیای فانیت</p></div>
<div class="m2"><p>کند روزی کفن اسکندرانیت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عزیزا بی تو گنجی پادشائی</p></div>
<div class="m2"><p>برای خویشتن بنهاد جائی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر رایش بود بر دارد آن گنج</p></div>
<div class="m2"><p>وگرنه همچنان بگذارد آن گنج</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جهان بی وفا نوری ندارد</p></div>
<div class="m2"><p>دمی بی ماتمی سوری ندارد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>اگر سیمت ببخشد سنگ باشد</p></div>
<div class="m2"><p>وگر عذریت خواهد لنگ باشد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وصالی بی فراقی قسمِ کس نیست</p></div>
<div class="m2"><p>که گل بی خار و شکر بی مگس نیست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نمی‌دانم کسی را بی غمی من</p></div>
<div class="m2"><p>که تادستی برو مالم دمی من</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>برَو تن در غم بار گران نه</p></div>
<div class="m2"><p>بسی جان کَن چو جان خواهند جان ده</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نمی‌بینم ترا آن مردی و زور</p></div>
<div class="m2"><p>که بر گردون شوی نا رفته درگور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نه ششصد سال آدم ماند غمناک</p></div>
<div class="m2"><p>ز بهر گندمی خون ریخت برخاک؟</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو او را گندمی بی صد بلا نیست</p></div>
<div class="m2"><p>ترا هم لقمهٔ بی غم روا نیست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زیان آمد همه سود من و تو</p></div>
<div class="m2"><p>فغان از زاد و از بود من و تو</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جهانا کیست کز جَور تو شادست</p></div>
<div class="m2"><p>همه جَور تو و دَور تو بادست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>جهان چون نیست از کار تو غمناک</p></div>
<div class="m2"><p>چرا بر سر کنی از دستِ او خاک</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>جهان چون تو بسی داماد دارد</p></div>
<div class="m2"><p>بسی عید و عروسی یاددارد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مرا عمریست تادربندِ آنم</p></div>
<div class="m2"><p>که تا با همدمی رمزی برانم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نمی‌بینم یکی هم دم موافق</p></div>
<div class="m2"><p>فغان زین هم نشینان منافق</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو بهر خاک زادستی ز مادر</p></div>
<div class="m2"><p>درین پستی چه سازی کاخ و منظر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو جسمت سوده خواهد گشت در خاک</p></div>
<div class="m2"><p>سر منظر چه افرازی بر افلاک</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>اگر آگندهٔ از سیم و زر گنج</p></div>
<div class="m2"><p>نخواهی خورد یک دم آب بی رنج</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>غم خود خور که کس را ازتو غم نیست</p></div>
<div class="m2"><p>چه می‌گویم ترا حقّا که هم نیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>اگرچه جای تو در زیر خاکست</p></div>
<div class="m2"><p>ولیکن جان پاک از خاک پاکست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نه مسجود ملایک گوهر تست؟</p></div>
<div class="m2"><p>نه تاجی از خلافت بر سر تست؟</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>خلیفه زادهٔ گلخن رها کن</p></div>
<div class="m2"><p>بگلشن شو گران جانی رها کن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بمصر اندر برای تست شاهی</p></div>
<div class="m2"><p>تو چون یوسف چرا در قعر چاهی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ازان بر ملک خویشت نیست فرمان</p></div>
<div class="m2"><p>که دیوت هست برجای سلیمان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تو شاهی هم در آخر هم در اوّل</p></div>
<div class="m2"><p>ولی بیننده را چشمست احول</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دو می‌بینی یکی را و دو صد صد</p></div>
<div class="m2"><p>چه یک چه دو چه صد، جمله توئی خود</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تو یک دل داری ای مسکین و صد بار</p></div>
<div class="m2"><p>بیک دل چون توانی کرد صد کار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ترا اندوهِ نان و جامه تا کی</p></div>
<div class="m2"><p>ترا از نام و ننگ عامه تا کی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نهادی بوالعجب داری تو در اصل</p></div>
<div class="m2"><p>پلاسی کرده اندر اطلسی وصل</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اگر هر دم حضوری را بکوشی</p></div>
<div class="m2"><p>زواَسجدواَقتَرِب خلعت بپوشی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز بس کاندیشهٔ بیهوده کردی</p></div>
<div class="m2"><p>نهاد خویش را فرسوده کردی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>الا ای خفته گر هستی خردمند</p></div>
<div class="m2"><p>دربایست خود برخود فرو بند</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>زهی حرص دل فرزندِ آدم</p></div>
<div class="m2"><p>زهی حیران و سرگردانِ عالم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>الا ای از حریصی با دل کور</p></div>
<div class="m2"><p>بماندی در حرص را مرگست مرهم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>...</p></div>
<div class="m2"><p>تو نامرده نگردد حرصِ تو کم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چه خواهی کرد چندین مال دنیا</p></div>
<div class="m2"><p>چشیدی جامِ مالامالِ دنیا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>متاع جملهٔ دنیا بیک جَو</p></div>
<div class="m2"><p>نیرزد بالله اندر چشمِ رهرَو</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>همه چون کرکسان دربند مردار</p></div>
<div class="m2"><p>...</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>فغان زین مور طبعان سخن چین</p></div>
<div class="m2"><p>چو موران جمله نه رهبر نه ره بین</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>فغان از حرص مشتی استخوان رند</p></div>
<div class="m2"><p>همه سگ سیرتان موش پیوند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>الا ای روز و شب غمخواره مانده</p></div>
<div class="m2"><p>بدست حرص در بیچاره مانده</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>حریصی بر سرت کرده فساری</p></div>
<div class="m2"><p>ترا حرصست و اشتر را مهاری</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>تو بر رزّاق ایمن باش آخر</p></div>
<div class="m2"><p>صبوری وَرز و ساکن باش آخر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ز کافر او نگیرد رزقِ خود باز</p></div>
<div class="m2"><p>کجا گیرد ز مرد پر خرد باز</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>مکن در وقت صبح ای دوست سستی</p></div>
<div class="m2"><p>چو داری ایمنی و تن درستی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو تو بیدار باشی صبحگاهی</p></div>
<div class="m2"><p>بیابی هر چه آن ساعت بخواهی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>هر آن خلعت کز آن درگاه پوشند</p></div>
<div class="m2"><p>چو آید صبح گاه آنگاه پوشند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>در روضه سحرگاهان گشایند</p></div>
<div class="m2"><p>جمال او بمشتاقان نمایند</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>گرت باید در آن دم پادشائی</p></div>
<div class="m2"><p>ز درگاه محمد کن گدائی</p></div></div>