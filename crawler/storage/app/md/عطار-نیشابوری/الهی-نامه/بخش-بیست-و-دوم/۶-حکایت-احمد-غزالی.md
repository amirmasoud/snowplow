---
title: >-
    (۶) حکایت احمد غزالی
---
# (۶) حکایت احمد غزالی

<div class="b" id="bn1"><div class="m1"><p>به پیش پاک بازان دلفروز</p></div>
<div class="m2"><p>چنین گفت احمد غزّال یک روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون بهر جمال یوسف خوب</p></div>
<div class="m2"><p>بمصر آمد زبیت الحُزن یعقوب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآمد تنگ یوسف پیش او در</p></div>
<div class="m2"><p>گرفت آن تنگ دل را تنگ در بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فغان در بسته بُد یعقوب ناگاه</p></div>
<div class="m2"><p>که کو یوسف مگر افتاد در چاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو گفتند آخر می چه گوئی</p></div>
<div class="m2"><p>گرفته در بر او را می چه جوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کنعان بوی پیراهن شنیدی</p></div>
<div class="m2"><p>چو دیدی این دمش گوئی ندیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جواب این داد یعقوب پیمبر</p></div>
<div class="m2"><p>که من یوسف شدم امروز یکسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز یوسف لاجرم بوئی شنودم</p></div>
<div class="m2"><p>که من خود بندهٔ یعقوب بودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه من بوده‌ام، یوسف کدامست</p></div>
<div class="m2"><p>چو خود را یافتم اینم تمامست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخود گر سر فرود آری زمانی</p></div>
<div class="m2"><p>بیابی زانچه می‌گوی نشانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولی چون از همه آزاد گردی</p></div>
<div class="m2"><p>تو نه غمگین شوی نه شاد گردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز زیر چرخ گردانت بر آرند</p></div>
<div class="m2"><p>برنگ کار مردانت برآرند</p></div></div>