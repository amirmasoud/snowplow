---
title: >-
    (۹) حکایت بایزید با مرد مسافر
---
# (۹) حکایت بایزید با مرد مسافر

<div class="b" id="bn1"><div class="m1"><p>برای بایزید آمد ز جائی</p></div>
<div class="m2"><p>غریبی، در بزد چون آشنائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان خانه در شیخ نکورای</p></div>
<div class="m2"><p>بفکرت ایستاده بوده بر پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفتا نگوئی کز کجا ام؟</p></div>
<div class="m2"><p>غریبش گفت مردی آشناام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غریبم آمده بهر لقائی</p></div>
<div class="m2"><p>ببوی بایزید از دور جائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوابش داد شیخ عالم افروز</p></div>
<div class="m2"><p>که ای درویش سی سالست امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که من در آرزوی بایزیدم</p></div>
<div class="m2"><p>بسی جستم ولی گردش ندیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندانم تا چه افتاد و کجا شد</p></div>
<div class="m2"><p>نمی‌بینم مگر از چشم ما شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان در زر وجودش گشت خاموش</p></div>
<div class="m2"><p>که می‌شد قرب سی سالش فراموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی کو جاودانه محوِ زر شد</p></div>
<div class="m2"><p>ز خود هرگز نداند با خبر شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولیکن کیمیا آنست مادام</p></div>
<div class="m2"><p>که نور الله نهندش سالکان نام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بر کافری تابد زمانی</p></div>
<div class="m2"><p>فرو گیرد ز نور او جهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو زد بر سحرهٔ فرعون آن نور</p></div>
<div class="m2"><p>چنان نزدیک گشتند آن چنان دور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر بر پیرزن تابد زمانی</p></div>
<div class="m2"><p>کند چون رابعه‌ش مرد جهانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگر بر بیل زن تابد باعزاز</p></div>
<div class="m2"><p>چو خرقانیش گرداند سرافراز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر یک ذرّه با معروف گردد</p></div>
<div class="m2"><p>ز ترسائی بدین موصوف گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر پیش فُضَیل آید پدیدار</p></div>
<div class="m2"><p>شود از ره زنی ره دانِ اسرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگر درجان ابن ادهم آید</p></div>
<div class="m2"><p>دلش سلطانِ هر دو عالم آید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر بر تن زند دل گردد آن خاک</p></div>
<div class="m2"><p>وگر بر دل زند جانی شود پاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو جان در خویشتن آن نور یابد</p></div>
<div class="m2"><p>دو گیتی را ز هستی دور یابد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو جان زان نور گردد محو مطلق</p></div>
<div class="m2"><p>به سبحانی برون آید و اناالحق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو در صحن بهشت آید باخلاص</p></div>
<div class="m2"><p>خطابش این بوَد از حضرت خاص</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که هست این نامه از شاه یگانه</p></div>
<div class="m2"><p>به سوی پادشاه جاودانه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو از خاصّ خودش پوشند جامه</p></div>
<div class="m2"><p>ز قُدّوسی بقّدوسیست نامه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو قدّوسی توانی جاودان گشت</p></div>
<div class="m2"><p>همه تن دل همه دل نیز جان گشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو دادت صورة خوب و صفت هم</p></div>
<div class="m2"><p>بیا تا بدهدت این معرفت هم</p></div></div>