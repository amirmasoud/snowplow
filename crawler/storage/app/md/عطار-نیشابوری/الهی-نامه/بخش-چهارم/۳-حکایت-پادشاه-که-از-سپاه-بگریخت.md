---
title: >-
    (۳) حکایت پادشاه که از سپاه بگریخت
---
# (۳) حکایت پادشاه که از سپاه بگریخت

<div class="b" id="bn1"><div class="m1"><p>در افتادند در شهری سپاهی</p></div>
<div class="m2"><p>گریزان شد نهان زان شهر شاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشهری شد بگردانید جامه</p></div>
<div class="m2"><p>نه خاصه باز دانستش نه عامه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجای آورد او را آشنائی</p></div>
<div class="m2"><p>بدو گفتا چرائی چون گدائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگو آخر که من شاهم بایشان</p></div>
<div class="m2"><p>چرا بنشستهٔ خوار و پریشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهش گفتا مگو آی در نظاره</p></div>
<div class="m2"><p>که گر گویم کنندم پاره پاره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کو دیدهٔ سلطان ندارد</p></div>
<div class="m2"><p>به سلطان رفتنش امکان ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بی‌دیده جوئی قربت شاه</p></div>
<div class="m2"><p>شوی درخون جان خویش آنگاه</p></div></div>