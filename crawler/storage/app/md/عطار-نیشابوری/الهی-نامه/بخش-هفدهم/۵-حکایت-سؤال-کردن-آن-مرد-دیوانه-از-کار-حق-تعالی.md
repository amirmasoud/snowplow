---
title: >-
    (۵) حکایت سؤال کردن آن مرد دیوانه از کار حق تعالی
---
# (۵) حکایت سؤال کردن آن مرد دیوانه از کار حق تعالی

<div class="b" id="bn1"><div class="m1"><p>یکی پرسید ازان دیوانه ساری</p></div>
<div class="m2"><p>که ای دیوانه حق را چیست کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گفت او که لوح کودکان را</p></div>
<div class="m2"><p>اگر دیدی چنان می‌دان جهان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که گاه آن لوح بنگارد ز آغاز</p></div>
<div class="m2"><p>گهی آن نقش کُلّی بسترد باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین اشغال باشد روزگاری</p></div>
<div class="m2"><p>بجز اثبات و محوش نیست کاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغان از خلق و فریاد از زمانه</p></div>
<div class="m2"><p>نفیر از نقش لوح کودکانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگاری کان زنان بر دست دارند</p></div>
<div class="m2"><p>اگرچه زان نکوئی چون نگارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل آن بهتر کزان دربند نبوَد</p></div>
<div class="m2"><p>که آن هم بیشِ روزی چند نبوَد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگاری کان نخواهد ماند بر جای</p></div>
<div class="m2"><p>نه بر دستست زیبنده نه بر پای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگاری کان بسان درهم آید</p></div>
<div class="m2"><p>چو زهر جانست جان زو پُر غم آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرچه ذوقِ دنیا بی‌شمارست</p></div>
<div class="m2"><p>ولیکن در بقا چون آن نگارست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر مردان عالم مصطفی بود</p></div>
<div class="m2"><p>ببین تا در ره دنیا کجا بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو اندر ملک درویشی سرافراخت</p></div>
<div class="m2"><p>قبای مسکنت را در بر انداخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طعام جوع را صد خوان بگسترد</p></div>
<div class="m2"><p>بملک فقر شادروان بگسترد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان بر ملکِ دنیا خاک انداخت</p></div>
<div class="m2"><p>که رخت از خاک بر افلاک انداخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کمال ملک درویشی چنان داشت</p></div>
<div class="m2"><p>که آن طاقت ندانم تا توان داشت</p></div></div>