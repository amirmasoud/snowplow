---
title: >-
    (۲) حکایت باز با مرغ خانگی
---
# (۲) حکایت باز با مرغ خانگی

<div class="b" id="bn1"><div class="m1"><p>ز مرغ خانگی بازی برآشفت</p></div>
<div class="m2"><p>بمرغ خانگی آنگه چنین گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مَردُم داردت تیمارخانه</p></div>
<div class="m2"><p>دمی نگذاردت بی آب و دانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه می‌دارد از اعدات پیوست</p></div>
<div class="m2"><p>که تابر تو نیابد دشمنی دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو پیوسته ز مردم می‌گریزی</p></div>
<div class="m2"><p>چنین بد عهد از بهر چه چیزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وفای تست مردم را همیشه</p></div>
<div class="m2"><p>ترا جز بی‌وفائی نیست پیشه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیامیزی تو با مردم زمانی</p></div>
<div class="m2"><p>چو تو نشنیده‌ام نامهربانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا باری اگر مردم بصد بار</p></div>
<div class="m2"><p>ز پیش خویش بفرستد بصد کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درآیم عهد ایشان را بپرواز</p></div>
<div class="m2"><p>بزودی هم بر ایشان رسم باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وفایی نیست مرغ خانگی را</p></div>
<div class="m2"><p>که پیشه می‌کند بیگانگی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو مرغ خانگی بشنید این راز</p></div>
<div class="m2"><p>جواب باز داد اندر زمان باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که ای بی دانش بی قدر و مقدار</p></div>
<div class="m2"><p>نه بینی باز کُشته سر نگون سار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی صد مرغ بینی سر بریده</p></div>
<div class="m2"><p>به پای آویخته سینه دریده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وفای آدمی گر این چنینست</p></div>
<div class="m2"><p>ازان بیزار گشتم این یقینست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین عهد و وفا را در زمانه</p></div>
<div class="m2"><p>چه بهتر، خاک بر سر جاودانه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه گر این ساعتم می‌پرورد لیک</p></div>
<div class="m2"><p>برای کشتنم می‌پرورد نیک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو گر این را وفا دانی جفا بِه</p></div>
<div class="m2"><p>بسی کفر از چنین مهر و وفا به</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز دیری گه ترا ای چرخ گردان</p></div>
<div class="m2"><p>روانست آسیا برخون مردان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شگفتا کارِ تو ای چرخ ناساز</p></div>
<div class="m2"><p>که در خاک افکنی پروردهٔ ناز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهانا حاصل پروردن ما</p></div>
<div class="m2"><p>چه خواهد بود جز خون خوردن ما</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کس از خون خوردن تو نیست آگاه</p></div>
<div class="m2"><p>که پنهان می‌کنی در خاک و در چاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهانا چون حیات تو مماتست</p></div>
<div class="m2"><p>وفا ازتو طمع کردن وفاتست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جفات اوّل مرا در شور انداخت</p></div>
<div class="m2"><p>وفات آخر مرا در گور انداخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نمی‌دانم که تا این بی در وبام</p></div>
<div class="m2"><p>برای چیست گردان بام تا شام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عجایب نامهٔ این هفت پرگار</p></div>
<div class="m2"><p>مرا در خون بگردانید صد بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز سر تا پای رفتم هر زمان من</p></div>
<div class="m2"><p>نمی‌دانم سراپای جهان من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو گوئی بی سر و بی پای ازانم</p></div>
<div class="m2"><p>که سر از پای و پای از سر ندانم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو جان اینجا نفس از خود نهان زد</p></div>
<div class="m2"><p>چگونه لاف دانش می‌توان زد</p></div></div>