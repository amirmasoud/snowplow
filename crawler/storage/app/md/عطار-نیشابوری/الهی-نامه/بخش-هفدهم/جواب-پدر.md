---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش عزیزا چند گوئی</p></div>
<div class="m2"><p>ز غفلت ملک فانی چند جوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو باقی نیست ملکت جز زمانی</p></div>
<div class="m2"><p>مکن در گردنت بار جهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بار خود بتنها بر نتابی</p></div>
<div class="m2"><p>ببار خلق عالم چون شتابی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز درویشی چو مردن هست دشوار</p></div>
<div class="m2"><p>ز شاهی چون بمیری آخر کار؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو می‌بینی زوال پادشاهی</p></div>
<div class="m2"><p>عجب می‌آیدم تا می چه خواهی</p></div></div>