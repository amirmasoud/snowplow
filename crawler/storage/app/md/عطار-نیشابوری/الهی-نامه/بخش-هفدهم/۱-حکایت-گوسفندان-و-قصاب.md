---
title: >-
    (۱) حکایت گوسفندان و قصّاب
---
# (۱) حکایت گوسفندان و قصّاب

<div class="b" id="bn1"><div class="m1"><p>چنین گفت آن امیر دردمندان</p></div>
<div class="m2"><p>که نیست این بس عجب از گوسفندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که می‌آرند ایشان را بخواری</p></div>
<div class="m2"><p>که تا بُرّند سرهاشان بزاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بی عقلند و ایشان می‌ندانند</p></div>
<div class="m2"><p>ازان سوی مقابر چون روانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازان قصّاب می‌باید عجب داشت</p></div>
<div class="m2"><p>که او هم علم دارد هم طلب داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو می‌داند که او را نیز ناگاه</p></div>
<div class="m2"><p>بخواهندش بریدن سر درین راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه فارغ و ایمن نشستست</p></div>
<div class="m2"><p>نمی‌جنبد خوشی ساکن نشستست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگه کن تا بآدم پُشت بر پشت</p></div>
<div class="m2"><p>که چندین طفل عالم در شکم کُشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسی میرند جسم مور داده</p></div>
<div class="m2"><p>بسی شیرند تن در گور داده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان را ذرّهٔ در مغز هُش نیست</p></div>
<div class="m2"><p>که او جز رستمی سُهراب کُش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه می‌گویم خطا گفتم چو مستان</p></div>
<div class="m2"><p>که او زالیست سر تا پای دستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا می‌پرورد از بهر خوردن</p></div>
<div class="m2"><p>بِنِه این تیغ را ناکام گردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکش گردن، فلک سیلی زن تست</p></div>
<div class="m2"><p>که گر سیلی خوری در گردن تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسیلی کردنت پرورده گردی</p></div>
<div class="m2"><p>که تا فربه شوی وخورده گردی</p></div></div>