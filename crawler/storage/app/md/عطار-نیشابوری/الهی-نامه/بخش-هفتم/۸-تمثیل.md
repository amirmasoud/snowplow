---
title: >-
    (۸) تمثیل
---
# (۸) تمثیل

<div class="b" id="bn1"><div class="m1"><p>بزرگی گفت ازل همچون کمانست</p></div>
<div class="m2"><p>هزاران تیر هر دم زو روانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیگر سو ابد آماجگاهی</p></div>
<div class="m2"><p>نه زین سو و نه زان امکانِ راهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی هر تیر کآید از کمان راست</p></div>
<div class="m2"><p>عنایت بود تیر انداز را خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی هر تیر کآید کوژ از راه</p></div>
<div class="m2"><p>همی بر تیر نفرین بارد آنگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین حالی عجبتر می‌ندانم</p></div>
<div class="m2"><p>دلم خون گشت دیگر می‌ندانم</p></div></div>