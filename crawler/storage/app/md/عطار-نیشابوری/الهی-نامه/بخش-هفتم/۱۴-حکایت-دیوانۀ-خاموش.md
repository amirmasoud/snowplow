---
title: >-
    (۱۴) حکایت دیوانۀ خاموش
---
# (۱۴) حکایت دیوانۀ خاموش

<div class="b" id="bn1"><div class="m1"><p>یکی دیوانه در بغداد بودی</p></div>
<div class="m2"><p>که نه یک حرف گفتی نه شنودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفتند ای مجنون عاجز</p></div>
<div class="m2"><p>چرا حرفی نمی‌گوئی تو هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت او که حرفی با که گویم</p></div>
<div class="m2"><p>چو مردم نیست پاسخ از که جویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفتند خلقی کین زمانند</p></div>
<div class="m2"><p>نمی‌بینی که جمله مردمانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گفت او نه اند این قوم مردم</p></div>
<div class="m2"><p>که مردم آن بود کو از تعظم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم دی و غم فرداش نبود</p></div>
<div class="m2"><p>ز کار بیهده سوداش نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم ناآمده هرگز ندارد</p></div>
<div class="m2"><p>ز رفته خویش را عاجز ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم درویشی و روزیش نبود</p></div>
<div class="m2"><p>بجز یک غم شبانروزیش نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که غم در هر دو عالم جز یکی نیست</p></div>
<div class="m2"><p>یقینست آنچه می‌گویم شکی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرت امروز از فردا غمی هست</p></div>
<div class="m2"><p>بنقد امروز عمرت دادی از دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مخور غم چون جهان بی‌غمگسارست</p></div>
<div class="m2"><p>وگر غم می‌خوری هر دم هزارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوشی در ناخوشی بودن کمالست</p></div>
<div class="m2"><p>که نقد دل خوشی جُستن محالست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه خواهد بود آخر زین بتر نیز</p></div>
<div class="m2"><p>که صد غم هست و می‌آید دگر نیز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازان شادی که غم زاید چه خواهی</p></div>
<div class="m2"><p>وجودی کز عدم زاید چه خواهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا شادی بدو باید وگر نه</p></div>
<div class="m2"><p>غم بی دولتی می‌خور دگر نه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدو گر شاد می‌باشی زمانی</p></div>
<div class="m2"><p>تو داری نقد شادی جهانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگرنامش نگوئی یک زمان تو</p></div>
<div class="m2"><p>چه بدنامی براندی بر زبان تو</p></div></div>