---
title: >-
    (۴) حکایت دیوانه که سر بر در کعبه می‬زد
---
# (۴) حکایت دیوانه که سر بر در کعبه می‬زد

<div class="b" id="bn1"><div class="m1"><p>یکی دیوانهٔ گریان و دل سوز</p></div>
<div class="m2"><p>شبی در پیش کعبه بود تا روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشی می‌گفت اگر نگشائیم در</p></div>
<div class="m2"><p>بدین در همچو حلقه می‌زنم سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که تا آخر سرم بشکسته گردد</p></div>
<div class="m2"><p>دلم زین سوز دایم رسته گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی هاتف زبان بگشاد آنگاه</p></div>
<div class="m2"><p>که پُر بت بود این خانه دو سه راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکسته گشت آن بتها درونش</p></div>
<div class="m2"><p>شکسته گیر یک بت از برونش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر می بشکنی سر از برون تو</p></div>
<div class="m2"><p>بتی باشی که گردی سرنگون تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین راه ازچنین سر کم نیاید</p></div>
<div class="m2"><p>که دریا بیش یک شبنم نیاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزرگی چون شنید آواز هاتف</p></div>
<div class="m2"><p>بدان اسرار شد دزدیده واقف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخاک افتادو چشمش خون روان کرد</p></div>
<div class="m2"><p>بسی جان از چنین غم خون توان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو با او هیچ نتوانیم کوشید</p></div>
<div class="m2"><p>نمی‌باید بصد زاری خروشید</p></div></div>