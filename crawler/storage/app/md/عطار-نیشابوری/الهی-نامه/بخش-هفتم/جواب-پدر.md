---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش که چیزی بایدت خواست</p></div>
<div class="m2"><p>که آن در حضرت عزت بود راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گر لایق نباشد آنچه خواهی</p></div>
<div class="m2"><p>ترا آن چیز نبوَد جز تباهی</p></div></div>