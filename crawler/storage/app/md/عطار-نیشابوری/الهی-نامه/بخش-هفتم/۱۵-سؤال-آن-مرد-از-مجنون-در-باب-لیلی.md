---
title: >-
    (۱۵) سؤال آن مرد از مجنون در باب لیلی
---
# (۱۵) سؤال آن مرد از مجنون در باب لیلی

<div class="b" id="bn1"><div class="m1"><p>یکی پرسید ازان مجنونِ غمگین</p></div>
<div class="m2"><p>که از لیلی چه می‌گوئی تو مسکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخاک افتاد مجنون سر نگون سار</p></div>
<div class="m2"><p>بدو گفتا بگو لیلی دگر بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو از من چند معنی جوی باشی</p></div>
<div class="m2"><p>ترا این بس که لیلی گوی باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی گر دُرِّ معنی سفته آید</p></div>
<div class="m2"><p>چنان نبوَد که لیلی گفته آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نام و نعت لیلی بازگفتی</p></div>
<div class="m2"><p>جهانی در جهانی راز گفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دایم نام لیلی می‌توان گفت</p></div>
<div class="m2"><p>ز غیری کفرم آید یک زمان گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی کو نام لیلی کردی آغاز</p></div>
<div class="m2"><p>بر مجنون همی عاقل شدی باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر جز نام لیلی یاد کردی</p></div>
<div class="m2"><p>شدی دیوانه و فریاد کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر گم بودن خود یاد داری</p></div>
<div class="m2"><p>روا باشد که از وی یاد آری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی تا از خودی سدّیت پیشست</p></div>
<div class="m2"><p>اگر یادش کنی آن یاد خویشست</p></div></div>