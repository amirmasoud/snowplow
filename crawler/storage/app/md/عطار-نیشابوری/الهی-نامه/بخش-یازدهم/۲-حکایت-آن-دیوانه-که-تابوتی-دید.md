---
title: >-
    (۲) حکایت آن دیوانه که تابوتی دید
---
# (۲) حکایت آن دیوانه که تابوتی دید

<div class="b" id="bn1"><div class="m1"><p>یکی تابوت می‌بردند بر دست</p></div>
<div class="m2"><p>بدید از دور آن دیوانهٔ مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی را گفت این مرده که بودست</p></div>
<div class="m2"><p>که ناگه شیرِ مرگش در ربودست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفتند ای مجنون پُر شور</p></div>
<div class="m2"><p>جوانی بود کُشتی گیرِ پُر زور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدیشان گفت دیوانه که برنا</p></div>
<div class="m2"><p>اگرچه بود در کُشتی توانا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولیکن می‌ندانست آن جگرسوز</p></div>
<div class="m2"><p>که ناگه باکه در کُشتی شد امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریفی بس تواناش اوفتادست</p></div>
<div class="m2"><p>بقوّت بی محاباش اوفتادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان در خاکش افکندست و در خون</p></div>
<div class="m2"><p>که دیگر برنخواهد خاست اکنون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی الحمدلله می‌توان کرد</p></div>
<div class="m2"><p>که جائی می‌توان دید این جوانمرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو چاره نیسب ز افتادن کسی را</p></div>
<div class="m2"><p>بدین دریا درافتادن بسی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گر اینجا در افتی جان نداری</p></div>
<div class="m2"><p>چو در برخاستن ایمان نداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش آمد عالمت افراختی بال</p></div>
<div class="m2"><p>فرو بردی بدین مردار چنگال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو این ده نه گرفتی نه خریدی</p></div>
<div class="m2"><p>همان انگار کین ده را ندیدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیاید هیچ عاقل در جهانی</p></div>
<div class="m2"><p>که بر مردم سرآید در زمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرا جانت بعالم باز بستست</p></div>
<div class="m2"><p>که این عالم بیک دم باز بستست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان آنست گر تو مردِ آنی</p></div>
<div class="m2"><p>شوی آنجا که هستی آن جهانی</p></div></div>