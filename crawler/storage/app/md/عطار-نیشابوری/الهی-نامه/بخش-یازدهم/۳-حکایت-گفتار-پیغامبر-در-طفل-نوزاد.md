---
title: >-
    (۳) حکایت گفتار پیغامبر در طفل نوزاد
---
# (۳) حکایت گفتار پیغامبر در طفل نوزاد

<div class="b" id="bn1"><div class="m1"><p>چنین گفتست با یاران پیمبر</p></div>
<div class="m2"><p>که آن طفلی که می‌زاید زمادر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بر روی زمین افکنده گردد</p></div>
<div class="m2"><p>بغایت عاجز و گرینده گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی چون روشنی این جهان دید</p></div>
<div class="m2"><p>فراخی زمین و آسمان دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخواهد او رحم هرگز دگر بار</p></div>
<div class="m2"><p>نگردد نیز در ظلمت گرفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کز بندِ این تنگ آشیان رفت</p></div>
<div class="m2"><p>بصحرای فراخ آن جهان رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعینه حال آن کس همچنانست</p></div>
<div class="m2"><p>که او را از رحم قصد جهانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان کان طفل آمد در جهانی</p></div>
<div class="m2"><p>نخواهد با شکم رفتن زمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دنیا هر که سوی آن جهان شد</p></div>
<div class="m2"><p>بگفتم حال طفلت همچنان شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلا چون نیست جانت این جهانی</p></div>
<div class="m2"><p>بر آتش نه جهان گر مرد جانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر قلبت نخواهد برد ره پیش</p></div>
<div class="m2"><p>چگونه ره بری در قالب خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که گر راهی به پیشان می‌توان برد</p></div>
<div class="m2"><p>یقین می‌دان که از جان می‌توان برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درون دَیرِ دل خلوتگهی ساز</p></div>
<div class="m2"><p>وزان خلوة به سوی حق رهی ساز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر کاری کنی همرنگِ جان کن</p></div>
<div class="m2"><p>مکن آن بر سر چوبی، نهان کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو گر جامه بگردانی روا نیست</p></div>
<div class="m2"><p>که او دوزد، بدست تو قبا نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ولیکن گر توانی همچو مردان</p></div>
<div class="m2"><p>ز جامه درگذر جان را بگردان</p></div></div>