---
title: >-
    (۱۰) حکایت آن مرد که عروس خود را بکر نیافت
---
# (۱۰) حکایت آن مرد که عروس خود را بکر نیافت

<div class="b" id="bn1"><div class="m1"><p>عروسی خواست مردی چون نگاری</p></div>
<div class="m2"><p>بمهر خود ندیدش برقراری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آن شوهر بمهر خود ندیدش</p></div>
<div class="m2"><p>نشان دختر بخرد ندیدش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه تن چون گلاب آنجا عرق کرد</p></div>
<div class="m2"><p>چو گل جان را بجای جامه شق کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مرد از شرم زن را آنچنان دید</p></div>
<div class="m2"><p>وزان دلتنگی او را بیم جان دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل آن مرد خست از خجلت او</p></div>
<div class="m2"><p>بصحّت برگرفت آن علّت او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفتا که من ایمان ندارم</p></div>
<div class="m2"><p>اگر این سرِّ تو پنهان ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگردد مادرت زین راز آگاه</p></div>
<div class="m2"><p>پدر را خود کجا باشد درین راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خالی نیست از عیب آدمی زاد</p></div>
<div class="m2"><p>اگر عیبی ترا در راه افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپوشم تا بپوشد کردگارم</p></div>
<div class="m2"><p>که من بیش از تو در تن عیب دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو دل خوش دار و چندین زین مکن یاد</p></div>
<div class="m2"><p>دگر هرگز مبادت زین سخن یاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شد روز دگر بگذشت این حال</p></div>
<div class="m2"><p>بریخت آن مرغ زرّین را پر وبال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان در ورطهٔ بیماری افتاد</p></div>
<div class="m2"><p>که در یک روز در صد زاری افتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رگ و پی همچو چنگش در فغان ماند</p></div>
<div class="m2"><p>همه مغزش چو خرما استخوان ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شوهر دید روی چون زر او</p></div>
<div class="m2"><p>طبیب آورد حالی بر سر او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا یک ذرّه درمان را اثر بود</p></div>
<div class="m2"><p>که هر دم زرد روئی تازه‌تر بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زبان بگشاد شوهر در نهانی</p></div>
<div class="m2"><p>که کُشتی خویشتن را در جوانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر آن خواستی تا من بپوشم</p></div>
<div class="m2"><p>بپوشیدم وزین معنی خموشم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر آن بود رای تو کزین کار</p></div>
<div class="m2"><p>مرا نبوَد خبر نابوده انگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چرا زین غم بسی تیمار خوردی</p></div>
<div class="m2"><p>که تا خود را چینن بیمار کردی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین گفت آنگه آن زن کای نکوجُفت</p></div>
<div class="m2"><p>ز چون تو مرد ناید جز نکو گفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو آنچ از تو سزد گفتی و کردی</p></div>
<div class="m2"><p>غم جان من بیچاره خوردی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ولی من این خجالت را چه سازم</p></div>
<div class="m2"><p>که می‌دانم که میدانی تو رازم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو تو هستی خبردار از گناهم</p></div>
<div class="m2"><p>کجا برخیزد این آتش ز راهم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفت این وز خجلت بیخبر گشت</p></div>
<div class="m2"><p>سیه شد روزش و حالش دگر گشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو چیزی را که بودش آن ببخشید</p></div>
<div class="m2"><p>نماندش هیچ چیزی جان ببخشید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر یک قطره شد در بحر کل غرق</p></div>
<div class="m2"><p>چرا ریزی ازین غم خاک بر فرق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مشو چون قطره زین غم بی سر و پا</p></div>
<div class="m2"><p>که اولیتر بوَد قطره بدریا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرا زادی چو می‌مُردی چنین زار</p></div>
<div class="m2"><p>ترا نازاده مُردن به شرروار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرا برخاستی چون می‌بخفتی</p></div>
<div class="m2"><p>چرا می‌آمدی چون می‌برفتی</p></div></div>