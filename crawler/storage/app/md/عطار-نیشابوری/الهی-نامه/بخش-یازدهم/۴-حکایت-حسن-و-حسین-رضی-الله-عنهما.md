---
title: >-
    (۴) حکایت حسن و حسین رضی الله عنهما
---
# (۴) حکایت حسن و حسین رضی الله عنهما

<div class="b" id="bn1"><div class="m1"><p>حسن می‌شد حسینش بود همبر</p></div>
<div class="m2"><p>بجیحون چون رسیدند آن دو سرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن چون بنگریست او را نمی‌یافت</p></div>
<div class="m2"><p>گهی از پس گهی از پیش بشتافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بآخر زان سوی جیحونش می‌دید</p></div>
<div class="m2"><p>مقام از خویشتن افزونش می‌دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفت ای حبیب و مرد درگاه</p></div>
<div class="m2"><p>ز من آموختی آخر تو این راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین برآب چون بشتافتی تو</p></div>
<div class="m2"><p>بچه چیز این کرامت یافتی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسینش گفت ای استاد مطلق</p></div>
<div class="m2"><p>بدان این یافتم من در ره حق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که دل کردن سفیدم بود پیشه</p></div>
<div class="m2"><p>ترا کاغذ سیه کردن همیشه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر دل را بگردانی چو مردان</p></div>
<div class="m2"><p>شود خورشید عشقت چرخ گردان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلی فارغ ز تشبیه وز تعطیل</p></div>
<div class="m2"><p>مبرّا از همه تبدیل و تمثیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمانی کُل شده در قدسِ پاکی</p></div>
<div class="m2"><p>زمانی آمده در قید خاکی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهی با خود گهی بیخود دو حالش</p></div>
<div class="m2"><p>که تا هم زین بود هم زان کالش</p></div></div>