---
title: >-
    (۶) حکایت سلطان محمود با ایاز در گرمابه
---
# (۶) حکایت سلطان محمود با ایاز در گرمابه

<div class="b" id="bn1"><div class="m1"><p>مگر روزی ایاز سیم اندام</p></div>
<div class="m2"><p>چو جانها سوخت تنها شد بحمّام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفیقی گفت با محمود پیروز</p></div>
<div class="m2"><p>که محبوبت بحمّامست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شه را این سخن در گوش آمد</p></div>
<div class="m2"><p>چو دریائی دلش در جوش آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مردی حال کرده شاه عالی</p></div>
<div class="m2"><p>سوی حمّام شد خالی و حالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدید القصّه روی آن پری‌وش</p></div>
<div class="m2"><p>وزو دیوار گرمابه پُر آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عکس صورتش دیوار حمام</p></div>
<div class="m2"><p>همه رقّاص گشته از در و بام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خسرو حُسنِ سر تا پای او دید</p></div>
<div class="m2"><p>همه جان وقف یک یک جای او دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلش چون ماهئی بر تابه افتاد</p></div>
<div class="m2"><p>وزان آتش دران گرمابه افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایاز افتاد در پایش که ای شاه</p></div>
<div class="m2"><p>چه افتادت بگو امروز در راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که عقل تو که عقلی بود کامل</p></div>
<div class="m2"><p>چنان عقلی چو عقلی گشت زائل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهش گفتا چو رویت در نظر بود</p></div>
<div class="m2"><p>ز یک یک بندِ تو دل بیخبر بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون چون دیده آمد بنده بندت</p></div>
<div class="m2"><p>شدم چون بند بندت مستمندت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا از عشق رویت جان همی سوخت</p></div>
<div class="m2"><p>کنون صد آتش دیگر برافروخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو یک یک بندت آمد دلنوازم</p></div>
<div class="m2"><p>کنون من با کدامین عشق بازم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلا معشوق را در جان نشان تو</p></div>
<div class="m2"><p>نثارش کن ز چشم دُر فشان تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو او بنشست بر تخت دل تو</p></div>
<div class="m2"><p>بینداخت آن همه رخت دل تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو از شادی او از جای میرو</p></div>
<div class="m2"><p>گهی بر سر گهی بر پای میرو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تماشا می‌کن و می‌خور جهانی</p></div>
<div class="m2"><p>که تو خوردی جهانی هر زمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ولی گر خلق گرد آید هزاران</p></div>
<div class="m2"><p>کنند از جهل بر تو تیرباران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو معشوق تو با تو در حضورست</p></div>
<div class="m2"><p>اگر آهی کنی از کار دورست</p></div></div>