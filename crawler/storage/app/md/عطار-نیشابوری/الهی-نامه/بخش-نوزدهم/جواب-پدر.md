---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش که حرصت غالب آمد</p></div>
<div class="m2"><p>دلت زان کیمیا را طالب آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خواهی کرد دنیای دَنی را</p></div>
<div class="m2"><p>سرای مَکر و جای دشمنی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که دنیا هست زالی هفت پرده</p></div>
<div class="m2"><p>برای صیدِ تو هر هفت کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی بینم ز حرصت رفته آرام</p></div>
<div class="m2"><p>بیارام ای چو مرغ افتاده در دام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که مرغ حرص را خاکست دانه</p></div>
<div class="m2"><p>ز خاکش سیری آید جاودانه</p></div></div>