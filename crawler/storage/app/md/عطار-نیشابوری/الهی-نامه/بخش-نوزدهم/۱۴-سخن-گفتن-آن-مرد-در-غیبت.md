---
title: >-
    (۱۴) سخن گفتن آن مرد در غیبت
---
# (۱۴) سخن گفتن آن مرد در غیبت

<div class="b" id="bn1"><div class="m1"><p>بزرگی بود می‌گفت و شنود او</p></div>
<div class="m2"><p>بسی گرد جهان گردیده بود او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی گفتش که ای دانای دمساز</p></div>
<div class="m2"><p>کرا دیدی کزو گوئی سخن باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت او که گشتم هفت اقلیم</p></div>
<div class="m2"><p>ندیدم در جهان جز یک کس و نیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی آن بود مانده در پسی او</p></div>
<div class="m2"><p>که نه نیک و نه بد گفت از کسی او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولکین نیمهٔ آن بود کز عز</p></div>
<div class="m2"><p>بجز نیکو نگفت از خلق هرگز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا تانیک و بد همراه باشد</p></div>
<div class="m2"><p>نه دل بینا نه جان آگاه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیکن چون نه این ماند نه آنت</p></div>
<div class="m2"><p>بسرّ قدس مشغولست جانت</p></div></div>