---
title: >-
    (۱۰) حکایت شاهزاده و عروس
---
# (۱۰) حکایت شاهزاده و عروس

<div class="b" id="bn1"><div class="m1"><p>یکی شه زادهٔ خورشید فر بود</p></div>
<div class="m2"><p>که بینائی دو چشم پدر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر آن شاه بهرِ شاه زاده</p></div>
<div class="m2"><p>عروسی خواست داد حُسن داده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخوبی در همه عالم مَثَل بود</p></div>
<div class="m2"><p>سر خوبان نقّاش ازل بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرائی را مزّین کرد آن شاه</p></div>
<div class="m2"><p>سرائی نه، بهشتی بهرِ آن ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرائی پای تا سر حور در حور</p></div>
<div class="m2"><p>ز بس مهر و ز بس مه نور در نور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس شمع معنبر روی در روی</p></div>
<div class="m2"><p>معیّن گشته آن شب موی در موی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بحر شعر وصَوت رود هر دم</p></div>
<div class="m2"><p>خروش بحر و رود افتاده در هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سوق سبع الوانش اتّفاقا</p></div>
<div class="m2"><p>خَجِل سَبعَ سمواتٍ طِباقا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عروسی این چنین جشنی چنین خوش</p></div>
<div class="m2"><p>چنین جمعی همه زیبا و دلکش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشسته منتظر یک خلدِ پر حور</p></div>
<div class="m2"><p>که تا شه زاده کی آید بدان سور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر از شادئی آن شاه زاده</p></div>
<div class="m2"><p>نشسته بود با جمعی بباده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس کان شب بشادی کرد می‌نوش</p></div>
<div class="m2"><p>وجودش بر دل او شد فراموش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بجست از جای سرافکنده در بر</p></div>
<div class="m2"><p>خیال آن عروس افتاده در سر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دران غوغا ز مستی شد سواره</p></div>
<div class="m2"><p>براند او از در دروازه باره</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه پیدا بود در پیشش طریقی</p></div>
<div class="m2"><p>نه همبر در رکاب او رفیقی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگر از دور دَیری دید عالی</p></div>
<div class="m2"><p>منوّر از چراغ او را حوالی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان پنداشت آن سرمستِ مهجور</p></div>
<div class="m2"><p>که آن قصر عروس اوست از دور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولی آن دخمه گبران کرده بودند</p></div>
<div class="m2"><p>که از هر سوی خیلی مرده بودند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دران دخمه چراغی چند می‌سوخت</p></div>
<div class="m2"><p>دل آتش پرستان می بر افروخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهاده بود پیش دخمه تختی</p></div>
<div class="m2"><p>بدان تخت اوفتاده شوربختی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی زن بود پوشیده کفن را</p></div>
<div class="m2"><p>چو شه زاده بدید از دور زن را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان پنداشت از مستیِ باده</p></div>
<div class="m2"><p>که اینست آن عروس شاه زاده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز مستی پای از سر می‌ندانست</p></div>
<div class="m2"><p>ره بام از ره در می‌ندانست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کفن از روی آن نو مرده برداشت</p></div>
<div class="m2"><p>محلّ شهوتش را پرده برداشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو زیر آهنگ را در پرده افکند</p></div>
<div class="m2"><p>زبان را در دهان مرده افکند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شبی در صحبتش بگذاشت تا روز</p></div>
<div class="m2"><p>خوشی لب بر لبش میداشت تا روز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه شب منتظر صد ماه پیکر</p></div>
<div class="m2"><p>نشسته تا کی آید شاه از در</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو ناپیدا شد آن شه زادِ عالی</p></div>
<div class="m2"><p>پدر را زو خبر دادند حالی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پدر بر خاست با خیلی سواران</p></div>
<div class="m2"><p>بصحرا رفت همچون بیقراران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه ارکانِ دولت در رسیدند</p></div>
<div class="m2"><p>ز دور آن اسپ شهزاده بدیدند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پدر چون دید اسپ شاه زاده</p></div>
<div class="m2"><p>نهاد آنجا رخ آنگه شد پیاده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پسر را دید با آن مرده بر تخت</p></div>
<div class="m2"><p>بدلداری کشیده در برش سخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو خسرو با سپاه او را چنان دید</p></div>
<div class="m2"><p>تو گفتی آتشی در قعرِ جان دید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پسر چون پارهٔ با خویش آمد</p></div>
<div class="m2"><p>شهش با لشکری در پیش آمد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گشاد از خوابِ مستی چشم حالی</p></div>
<div class="m2"><p>بدید آن خلوت و آن جای خالی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرفته مردهٔ راتنگ در بر</p></div>
<div class="m2"><p>ستاده بر سر او شاه و لشکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بجای آورد آنچ افتاده بودش</p></div>
<div class="m2"><p>همی بایست مرگ خویش زودش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو الحق قصّهٔ ناکامش افتاد</p></div>
<div class="m2"><p>ز خجلت لرزه بر اندامش افتاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه آن بود میَلش از دل پاک</p></div>
<div class="m2"><p>که بشکافد زمین او را کند خاک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ولیکن کار چون افتاده بودش</p></div>
<div class="m2"><p>نبود از خجلت و تشویر سودش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا هم هست صبر ای مرد غم خور</p></div>
<div class="m2"><p>که تا آید ببالین تو لشکر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دران ساعت بدانی و به بینی</p></div>
<div class="m2"><p>که با که کردهٔ این هم نشینی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو ابرهیم در دین بت شکن باش</p></div>
<div class="m2"><p>بتان آزری را راه زن باش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که ابرهیم چون آهنگِ آن کرد</p></div>
<div class="m2"><p>خداوند جهانش امتحان کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ترا گر امتحان خواهند کردن</p></div>
<div class="m2"><p>نگونسار جهان خواهند کردن</p></div></div>