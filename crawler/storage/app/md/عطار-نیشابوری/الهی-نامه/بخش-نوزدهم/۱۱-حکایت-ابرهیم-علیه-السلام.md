---
title: >-
    (۱۱) حکایت ابرهیم علیه السلام
---
# (۱۱) حکایت ابرهیم علیه السلام

<div class="b" id="bn1"><div class="m1"><p>نوشته در قصص اینم عیان بود</p></div>
<div class="m2"><p>که ابرهیمِ پیغامبر چنان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بودی چل هزارش از غلامان</p></div>
<div class="m2"><p>سگی آن هر غلامی را بفرمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلاده جمله را زرّین ولیکن</p></div>
<div class="m2"><p>شمار گوسفندش نیست ممکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملایک چشم بر کارش گشادند</p></div>
<div class="m2"><p>ز کارش در گمانی اوفتادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که او مشغول چندین گوسفندست</p></div>
<div class="m2"><p>خدا می‌گوید او پاک و بلندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر او مستغرق ربّ جلیلست</p></div>
<div class="m2"><p>بنگذارد خلیلی چون خلیلست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجبریل امین حق گفت برخیز</p></div>
<div class="m2"><p>به پیش او زبان ز آواز کن تیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که تا چون بینی او را در ره ما</p></div>
<div class="m2"><p>چه زو بینی به پیش درگه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مردی گشت روح القدس محسوس</p></div>
<div class="m2"><p>بآوازی خوش الحان گفت قدّوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلیل الله چون بشنیدش آواز</p></div>
<div class="m2"><p>ز پای افتاد گفتی آن سرافراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو بخشید ثُلثی گوسفندان</p></div>
<div class="m2"><p>بدو گفت ای دوای دردمندان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگو یکبار دیگر نام یارم</p></div>
<div class="m2"><p>که این نامست دایم غم گسارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر ره گفت روح القدس آنگاه</p></div>
<div class="m2"><p>دگر ره اوفتاد از شوق در راه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدو بخشید آن تاج بلندان</p></div>
<div class="m2"><p>دوم ثلثی که بود از گوسفندان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دگر ره گفت نام حق دگر بار</p></div>
<div class="m2"><p>بگو چون بِه ازین نبوَد دگر کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر ره گفت قدّوسی بآواز</p></div>
<div class="m2"><p>دگر ره بی‌خودش افتاد آغاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو بخشید یکسر گوسفندان</p></div>
<div class="m2"><p>کم از میشی، همی نگذاشت چندان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درآمد جبرئیل و گفت ای پاک</p></div>
<div class="m2"><p>منم روح القُدُس در عالم خاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا این گوسفندان نیست در خور</p></div>
<div class="m2"><p>تراست این جمله ای پاک مطهّر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که جبریل امین در هیچ بابی</p></div>
<div class="m2"><p>نبودست آرزومند کبابی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خلیلش گفت آگاهی ازین راز</p></div>
<div class="m2"><p>که چیزی داده نستانم ز کس باز؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو جبریل گفت از من شبانی</p></div>
<div class="m2"><p>نیاید، من کنون رفتم تو دانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خلیلش گفت من نیز این همه پاک</p></div>
<div class="m2"><p>رهاکردم رها کردی تو بی باک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خطاب آمد ز حق سوی ملایک</p></div>
<div class="m2"><p>که هان چون بود ابرهیم مالک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که چون جبریل نام ما ندا کرد</p></div>
<div class="m2"><p>بنام ما همه نقدی فدا کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یقین تان شد که او جز بنده نبوَد</p></div>
<div class="m2"><p>بما زنده بمالی زنده نبوَد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ملایک باز گفتند ای خداوند</p></div>
<div class="m2"><p>مگر دل زندگی دارد بفرزند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس آنگه کرد حق از راهِ خوابش</p></div>
<div class="m2"><p>بتسلیم پسر کُشتن خطابش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پسر را چون برای کُشتن آورد</p></div>
<div class="m2"><p>زمین را چون فلک در گشتن آورد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برآمد از ملایک بانگ و فریاد</p></div>
<div class="m2"><p>که او از مال و فرزندست آزاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولیکن ایمنی او بخویشست</p></div>
<div class="m2"><p>بسی آن زندگی از جمله بیشست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنان تقدیر رفت از غیبِ دانش</p></div>
<div class="m2"><p>که در آتش کنند از امتحانش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بآخر چون بآتش شد گرفتار</p></div>
<div class="m2"><p>درآمد جبرئیل از اوجِ اسرار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که هان در خواه هر حاجت که داری</p></div>
<div class="m2"><p>بتو، گفتا، ندارم چون نه یاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر از غیر حاجت خواه باشم</p></div>
<div class="m2"><p>پس از اغیارِ این درگاه باشم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من از غم فارغم بشنو سخن راست</p></div>
<div class="m2"><p>خدا داند کند آنچش بوَد خواست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ملایک چون مقام او بدیدند</p></div>
<div class="m2"><p>ز صدق او خروشی برکشیدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کالهی، پاک جسم و پاک جانست</p></div>
<div class="m2"><p>بهر چش آزمودی بیش ازانست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان در حکم تو دیدیم نرمش</p></div>
<div class="m2"><p>که آتش سرد شد از عشق گرمش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بهشتی گشت دوزخ از دل او</p></div>
<div class="m2"><p>زهی خِلّت که آمد حاصل او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرش خوانی خلیل خویش شاید</p></div>
<div class="m2"><p>گرش جلوه دهی زین بیش شاید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر از دین خلیلت رهبری نیست</p></div>
<div class="m2"><p>ترا پس جز طریق آزری نیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرت بی سیمیَست و بی زری هم</p></div>
<div class="m2"><p>ترا نمرودیسَت و آزری هم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عجب داری که نمرودی چنان شد</p></div>
<div class="m2"><p>که بهر حرب حق بر آسمان شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که گر کاریت ناگه کوژ گردد</p></div>
<div class="m2"><p>دلت نمرودِ ره آن روز گردد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنان در چشم آید خشم و کینه‌ت</p></div>
<div class="m2"><p>که بر گردون رسد صندوقِ سینه‌ت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ترا چون کر گس و صندوق هم هست</p></div>
<div class="m2"><p>بنمرودیت در عالم علم هست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو هر دم می‌رسد صد تیرِ انکار</p></div>
<div class="m2"><p>چو نمرودت بدین گردنده پرگار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو پس در کارِ خود نمرودِ خویشی</p></div>
<div class="m2"><p>بنیک و بد زیان و سودِ خویشی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>توئی در بندِ افزونی بمانده</p></div>
<div class="m2"><p>ملایک غرقِ بی‌چونی بمانده</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چوعمرت رفت آخر چون کنی تو</p></div>
<div class="m2"><p>که بنشستی که زر افزون کنی تو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همه عمرت زیان بودست ای دوست</p></div>
<div class="m2"><p>که تا یک جَو زرت سودست ای دوست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو همت جای مردی یک قراضه‌ست</p></div>
<div class="m2"><p>بسی کم از زنان مستحاضه‌ست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>توانگر را پیمبر مُرده خوانده‌ست</p></div>
<div class="m2"><p>کسی کو سیم دارد مرده مانده‌ست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو سگ از پس مکن چندین جهانی</p></div>
<div class="m2"><p>که این سگ را تمامست استخوانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ترا این نفس همچون گبرِ زردشت</p></div>
<div class="m2"><p>بزیر پای ناگه خواهدت کُشت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بکاری گر نمی‌داریش مشغول</p></div>
<div class="m2"><p>شوی از دست او از کار معزول</p></div></div>