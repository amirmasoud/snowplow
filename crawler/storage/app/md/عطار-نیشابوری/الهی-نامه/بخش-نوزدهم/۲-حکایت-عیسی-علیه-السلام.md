---
title: >-
    (۲) حکایت عیسی علیه السلام
---
# (۲) حکایت عیسی علیه السلام

<div class="b" id="bn1"><div class="m1"><p>مگر روح الله آن شمع دلفروز</p></div>
<div class="m2"><p>بگورستان گذر می‌کرد یک روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز گوری نالهٔ آمد بگوشش</p></div>
<div class="m2"><p>دل از زاریِ آن آمد بجوشش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعا کرد آن زمان تا حق تعالی</p></div>
<div class="m2"><p>بیک دم زنده کردش چون خیالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی پیر خمیده چون کمانی</p></div>
<div class="m2"><p>سلامش گفت و ساکن شد زمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسیحش گفت پیرا کیستی تو</p></div>
<div class="m2"><p>چه وقتی مُردی و کَی زیستی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس آنگه گفت ای بحر پر اسرار</p></div>
<div class="m2"><p>منم حیّانِ بن معبد چنین زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار و هشتصد سالست ای پاک</p></div>
<div class="m2"><p>که تا من مرده‌ام افتاده در خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازین سختی نیاسودم زمانی</p></div>
<div class="m2"><p>ندیدم خویش را یک دم امانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسیحش گفت ای شوریده خوابت</p></div>
<div class="m2"><p>چرا کردند چندینی عذابت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفت این عذاب من کالیمست</p></div>
<div class="m2"><p>برای دانگی مال یتیمست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مسیحش گفت بی ایمان بمُردی</p></div>
<div class="m2"><p>که از دانگی تو چندین رنج بردی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین گفت او که بر اسلام مُردم</p></div>
<div class="m2"><p>که چندین سال چندین رنج بردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دعا کرد آن زمان عیسی پاکش</p></div>
<div class="m2"><p>که تا خوش خفت و شد با زیر خاکش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسلمانان مسلمانی گر اینست</p></div>
<div class="m2"><p>ندانم کانچه می‌بینم چه دینست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرت یک جَو حرام ناصوابست</p></div>
<div class="m2"><p>هزار و هشتصد سالت عذابست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر خود مال سر تا سر حرامست</p></div>
<div class="m2"><p>چگویم خود عذابت بر دوامست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عزیزا چون وفاداری نداری</p></div>
<div class="m2"><p>غم خود خور چو غم خواری نداری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نداری هیچ گردن سر میفراز</p></div>
<div class="m2"><p>حساب خصم از گردن بینداز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که چون بر سر نداری عیسی پاک</p></div>
<div class="m2"><p>بسی بینی عذاب از خصم بی باک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندانی هیچ کار خویش کردن</p></div>
<div class="m2"><p>بجز عمرت کم و زر بیش کردن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمی‌دانی که تا تو سیم کوشی</p></div>
<div class="m2"><p>بغفلت عمر زرّین می‌فروشی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مکن زر جمع چون سیماب درتاب</p></div>
<div class="m2"><p>که خواهی گشت ناگه همچو سیماب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازان زر بیشتر در زیرِ خاکست</p></div>
<div class="m2"><p>که از وی بیشتر مردم هلاکست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زری کان سنگ در کوه و کمر داشت</p></div>
<div class="m2"><p>بخیل از سنگ آن زر سخت تر داشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بده از مردمی صد گنج پیوست</p></div>
<div class="m2"><p>ولی یک جَو بمردی کم ده از دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خسی کو نان ده آمد از کسی به،</p></div>
<div class="m2"><p>که یک نان ده ز فرمان ده بسی به</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولی کُشته شدن در پای پیلان</p></div>
<div class="m2"><p>به از نان خوردن از دست بخیلان</p></div></div>