---
title: >-
    (۱۳) در معنی آن که غیبت گناهی بزرگ است
---
# (۱۳) در معنی آن که غیبت گناهی بزرگ است

<div class="b" id="bn1"><div class="m1"><p>چنین نقلست در توراة کان کس</p></div>
<div class="m2"><p>که او غیبت کند، آنگاه ازان پس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازان توبه کند، آخر کسی اوست</p></div>
<div class="m2"><p>که در صحن بهشتش ره دهد دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر خود توبه نکند اوّلین کس</p></div>
<div class="m2"><p>که در دوزخ رود او باشد و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تیغ زبانش چون زبانه</p></div>
<div class="m2"><p>شود چون رمحِ خطّی راست خانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان راستی دل بوَد آن</p></div>
<div class="m2"><p>که دل را اوّلین منزل بوَد آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین مجلس بزرگان جهان را</p></div>
<div class="m2"><p>چو خاموشی شرابی نیست جان را</p></div></div>