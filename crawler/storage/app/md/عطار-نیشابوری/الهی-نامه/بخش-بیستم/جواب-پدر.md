---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش که چون زر سایه افکند</p></div>
<div class="m2"><p>ترا از گوهر و از پایه افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیاید دُنیی و دین راست هر دو </p></div>
<div class="m2"><p>ز حق می‌دان که نتوان خواست هر دو</p></div></div>