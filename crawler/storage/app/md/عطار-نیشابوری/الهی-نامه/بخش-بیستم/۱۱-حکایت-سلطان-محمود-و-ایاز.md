---
title: >-
    (۱۱) حکایت سلطان محمود و ایاز
---
# (۱۱) حکایت سلطان محمود و ایاز

<div class="b" id="bn1"><div class="m1"><p>مگر سلطان دین محمود پیروز</p></div>
<div class="m2"><p>ایاز خویش را پرسید یک روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از چه رشک آید در جهانت</p></div>
<div class="m2"><p>جوابی راست خواهم زین میانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت او که در رشکم همه جای</p></div>
<div class="m2"><p>ازان سنگی که مالی در کف پای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم از رشکِ سنگت می‌بنالد</p></div>
<div class="m2"><p>که او رخ در کف پای تو مالد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر هرگز دهد این دولتم دست</p></div>
<div class="m2"><p>نهم سر در کف پای تو پیوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو رویم در کف پای تو باشد</p></div>
<div class="m2"><p>همیشه روی من جای تو باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر روی ایاز آید ترا جای</p></div>
<div class="m2"><p>نهد بر آسمان هفتمین پای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نه سر می‌خرد یار و نه دستار</p></div>
<div class="m2"><p>بطرّاری و دستانش بدست آر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندیدی آنکه رُستم از گزستان</p></div>
<div class="m2"><p>چه با اسفندیاری کرد دستان؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به باطن هرچه بتوان کرد میکن</p></div>
<div class="m2"><p>بظاهر ترک خواب و خورد میکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدستان و بحیلت پیش می‌رو</p></div>
<div class="m2"><p>بصدق معرفت بیخویش می‌رو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر راهی بدستان بازیابی</p></div>
<div class="m2"><p>دمی با همدمی دمساز یابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر با همدمت یک دم بهم تو</p></div>
<div class="m2"><p>نشانی خویش را، رَستی ز غم تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو بنگر کو کجا و تو کجائی</p></div>
<div class="m2"><p>عجب نبوَد اگر باشد جدائی</p></div></div>