---
title: >-
    المقالة العشرون
---
# المقالة العشرون

<div class="b" id="bn1"><div class="m1"><p>پسر گفتش که درویشی بسیار</p></div>
<div class="m2"><p>بسی باشد که آرد کافری بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزر چون دین و دنیا می‌شود راست</p></div>
<div class="m2"><p>ز حق هم کیمیا هم زر توان خواست</p></div></div>