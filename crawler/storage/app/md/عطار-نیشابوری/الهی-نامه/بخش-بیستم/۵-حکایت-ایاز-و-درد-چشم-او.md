---
title: >-
    (۵) حکایت ایاز و درد چشم او
---
# (۵) حکایت ایاز و درد چشم او

<div class="b" id="bn1"><div class="m1"><p>مگر از چشم زخم چشم اغیار</p></div>
<div class="m2"><p>بدرد چشم ایاز آمد گرفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز درد چشم چشمش همچو خون شد</p></div>
<div class="m2"><p>دو نرگسدانِ چشمش لاله گون شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علی الجمله چو روزی ده برآمد</p></div>
<div class="m2"><p>ز درد چشم چشمش در سر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان از دردِ چشمی ممتحن گشت</p></div>
<div class="m2"><p>که صفرا کردش و بی خویشتن گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی محمود را از وی خبر کرد</p></div>
<div class="m2"><p>سواره گشت محمود و گذر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببالین ایاز آمد نهان او</p></div>
<div class="m2"><p>نهاد انگشت بر لب در زمان او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان بیمارداران گفت زنهار</p></div>
<div class="m2"><p>مگردانید از شاهش خبردار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بنشست آن زمان محمود غازی</p></div>
<div class="m2"><p>بجَست از جا ایاز از دلنوازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهم بگشاد چشم و شاد بنشست</p></div>
<div class="m2"><p>زهی بنده که چون آزاد بنشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفتند ای از خویش رفته</p></div>
<div class="m2"><p>تن از پس مانده جان از پیش رفته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز درد چشم سرگردان بمانده</p></div>
<div class="m2"><p>میان جان و تن حیران بمانده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو شه بنشست بر بالینت از پای</p></div>
<div class="m2"><p>تو صفرا کرده چون برجستی ازجای؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگفتت کس نبودت چشم بر راه</p></div>
<div class="m2"><p>چگونه گشتی ازمحمود آگاه؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین گفت او که چه حاجت شنیدن</p></div>
<div class="m2"><p>ندارم احتیاجی هم بدیدن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز گوش و چشم آزادست جانم</p></div>
<div class="m2"><p>که من از جان ببویش باز دانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بوی او ز جان خود شنودم</p></div>
<div class="m2"><p>شدم زنده اگرچه مرده بودم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندیدی آنکه یعقوب پیمبر</p></div>
<div class="m2"><p>ببویش گشت روشن چشم در سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو می‌باید که چشم از درد سازی</p></div>
<div class="m2"><p>ز درد چشم تو خود می‌گدازی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بوی آشنائی یافتی تو</p></div>
<div class="m2"><p>بر آفاق دو عالم تافتی تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که آن یک ذرّه نور آشنائی</p></div>
<div class="m2"><p>چو صد خورشید دارد روشنائی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو دایم دوستی حق چنانست</p></div>
<div class="m2"><p>که یک ذرّه بِه از هر دو جهانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خدائی آنچنان میداردت دوست</p></div>
<div class="m2"><p>ازان شادی توان گنجید در پوست؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزرگانی که این پرگار دیدند</p></div>
<div class="m2"><p>بصد جان نقطهٔ دردش گُزیدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هزاران جان برای یک خطابش</p></div>
<div class="m2"><p>برافشاندند دل پر اضطرابش</p></div></div>