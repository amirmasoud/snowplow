---
title: >-
    (۹) حکایت شعیب علیه السلام
---
# (۹) حکایت شعیب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>شُعَیب از شوقِ حق ده سال بگریست</p></div>
<div class="m2"><p>ازان پس چشم پوشیده همی زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدا بیناش کرد از بعدِ آن باز</p></div>
<div class="m2"><p>که شد ده سالِ دیگر خون فشان باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر ره تیره شد دو چشمِ گریانش</p></div>
<div class="m2"><p>دگر ره چشم روزی کرد یزدانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر ره سالِ دیگر زار بگریست</p></div>
<div class="m2"><p>دگر ره نیز نتوانست نگریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نابینا شد و گریان بیفتاد</p></div>
<div class="m2"><p>خداوند جهان وَحیَش فرستاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گر از بیمِ دوزخ خون فشانی</p></div>
<div class="m2"><p>ترا آزاد کردم جاودانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر بهر بهشتی زار گریان</p></div>
<div class="m2"><p>ترا بخشم بهشت و حور و رضوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعیب آنگه زبان بگشاد حالی</p></div>
<div class="m2"><p>که ای حکم تو حکم لایزالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من از شوق تو می‌گریم چنین زار</p></div>
<div class="m2"><p>که من بس فارغم ازنور و از نار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه یکدم از بهشتم یاد آید</p></div>
<div class="m2"><p>نه از دوزخ مرا فریاد آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا قرب تو باید جاودانی</p></div>
<div class="m2"><p>بگفتم دردِ خود دیگر تو دانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خطاب آمد ز اوج آشنائی</p></div>
<div class="m2"><p>که چون گریان برای شوق مائی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون خوش می‌گری و می‌گری زار</p></div>
<div class="m2"><p>که کارت سخت دشوارست دشوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس آنگه گفت ای دانندهٔ راز</p></div>
<div class="m2"><p>مده بینائی من بعد ازین باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که تا وقتی که آن دیدار نبوَد</p></div>
<div class="m2"><p>مرا با دیدنی خود کار نبوَد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عزیزا چون نه این دیدار داری</p></div>
<div class="m2"><p>بسی بگری که عمری کار داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که چندانی که در دل رشک بیشست</p></div>
<div class="m2"><p>بچشم عاشقان در اشک بیشت</p></div></div>