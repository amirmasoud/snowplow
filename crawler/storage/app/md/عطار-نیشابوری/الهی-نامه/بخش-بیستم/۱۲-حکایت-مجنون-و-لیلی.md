---
title: >-
    (۱۲) حکایت مجنون و لیلی
---
# (۱۲) حکایت مجنون و لیلی

<div class="b" id="bn1"><div class="m1"><p>مگر یک روز مجنون در نشاطی</p></div>
<div class="m2"><p>نشسته بود در پیش رباطی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی دیوار بود از گج ببسته</p></div>
<div class="m2"><p>در آنجا لیلی ومجنون نشسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشی می‌گفت اگر عمری دویدم</p></div>
<div class="m2"><p>هم آخر هر دو را با هم بدیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر در خواب می‌بینم من اکنون</p></div>
<div class="m2"><p>نشسته پیش هم لیلی و مجنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهم این هر دو را هرگز که دیدست</p></div>
<div class="m2"><p>خدایا در جهان این عز که دیدست؟</p></div></div>