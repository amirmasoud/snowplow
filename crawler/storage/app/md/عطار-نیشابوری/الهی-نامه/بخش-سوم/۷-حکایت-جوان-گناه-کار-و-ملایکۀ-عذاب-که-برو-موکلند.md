---
title: >-
    (۷) حکایت جوان گناه کار و ملایکۀ عذاب که برو موکّلند
---
# (۷) حکایت جوان گناه کار و ملایکۀ عذاب که برو موکّلند

<div class="b" id="bn1"><div class="m1"><p>چنین خواندم که در محشر جوانی</p></div>
<div class="m2"><p>درآید وز خدا خواهد امانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بغایت جُرمِ او بسیار باشد</p></div>
<div class="m2"><p>ولی قاضی فضلش یار باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملایک می‌کنند آنجا شتابش</p></div>
<div class="m2"><p>که پیش آرند در دوزخ عذابش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی حالی خطاب آید ز درگاه</p></div>
<div class="m2"><p>که از چه می‌کشید او را درین راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گویند می‌تازیم او را</p></div>
<div class="m2"><p>که تا در دوزخ اندازیم او را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خطاب آید دگر امّا معمّا</p></div>
<div class="m2"><p>که هستیم ای عجب با او بهم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شما را این نمی‌باید شنودن</p></div>
<div class="m2"><p>که ما هر دو بهم خواهیم بودن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملایک این سخن نشنیده باشند</p></div>
<div class="m2"><p>نه هرگز این کرامت دیده باشند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین هیبت همه خاموش گردند</p></div>
<div class="m2"><p>بلرزند آنگهی بیهوش گردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطاب آید جوان را کای پریشان</p></div>
<div class="m2"><p>چه می‌پائی هلا بگریز ازیشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوان گوید خدایا در چنین جای</p></div>
<div class="m2"><p>که نه سر دارد این وادی ونه پای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجا یارم شدن از رستخیزی</p></div>
<div class="m2"><p>که نیست این جایگه راه گریزی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خطاب آید که ای در عین مستی</p></div>
<div class="m2"><p>بیا در ما گریز از جمله رستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جوان گوید مرا این یارگی نیست</p></div>
<div class="m2"><p>که نقد من به جز بیچارگی نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر تو فضل خود در کار آری</p></div>
<div class="m2"><p>مرا در پردهٔ اسرار آری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خداوندش بپوشد از کرامت</p></div>
<div class="m2"><p>کند پنهانش از خلق قیامت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدولت جای اسرارش رساند</p></div>
<div class="m2"><p>بخلوتگاه دیدارش رساند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملایک چون بهوش آیند آنگاه</p></div>
<div class="m2"><p>نه بینند آن جوان را بر سر راه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بجویندش بسی امّا نیابند</p></div>
<div class="m2"><p>بهر سوئی بمردی می‌شتابند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بحق گویند خصم ما کجا شد</p></div>
<div class="m2"><p>مگر در عالم باقی فنا شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهشت و دوزخ این ساعت بجُستیم</p></div>
<div class="m2"><p>نمی‌بینم و از وی دست شستیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو میدانی الهی کو کجا شد</p></div>
<div class="m2"><p>اگر با ما نگوئی جان ما شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خطاب آید که این از حکمت ماست</p></div>
<div class="m2"><p>که در پرده سرای عصمت ماست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو او را هست پیش ما قراری</p></div>
<div class="m2"><p>شما را نیست با او هیچ کاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنون او داند و ما جاودانه</p></div>
<div class="m2"><p>شما را رفت باید از میانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عنایت چون ز پیشان یار باشد</p></div>
<div class="m2"><p>کجا اندر میان اغیار باشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولی اوّل نبی را در هدایت</p></div>
<div class="m2"><p>نماید آفتابی در عنایت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عنایت گر ترا با خاص گیرد</p></div>
<div class="m2"><p>همه نقصان تو اخلاص گیرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کند دیدار خویشت آشکاره</p></div>
<div class="m2"><p>که تا کارت نباشد جز نظاره</p></div></div>