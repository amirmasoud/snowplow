---
title: >-
    (۶) حکایت یوسف و ابن یامین علیهما السلام
---
# (۶) حکایت یوسف و ابن یامین علیهما السلام

<div class="b" id="bn1"><div class="m1"><p>چو پیش یوسف آمد ابن یامین</p></div>
<div class="m2"><p>نشاندش هم نفس بر تخت زرّین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشسته بود یوسف در نقابی</p></div>
<div class="m2"><p>که نتوانی نهفتن آفتابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه می‌دانست هرگز ابن یامین</p></div>
<div class="m2"><p>که دارد در بر خود جان شیرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گمان برد او که سلطان عزیزست</p></div>
<div class="m2"><p>چه می‌دانست کو جان عزیزست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر او در عزیزی جان نبودی</p></div>
<div class="m2"><p>عزیز مصر جاویدان نبودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گر یوسف نشاندش در بر خویش</p></div>
<div class="m2"><p>ز حرمت بر نیاورد او سر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنها گفت یوسف خوب آنجا</p></div>
<div class="m2"><p>خبر پرسید از یعقوب آنجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی نامه بزیر پرده در داد</p></div>
<div class="m2"><p>ز سوز جان یعقوبش خبر داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو یوسف نامه بستد نام زد شد</p></div>
<div class="m2"><p>وز آنجا پیش فرزندان خود شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه گویم نامه بگشادند آخر</p></div>
<div class="m2"><p>بسی بر چشم بنهادند آخر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دران جمع اوفتاد از شوق جوشی</p></div>
<div class="m2"><p>برآمد از میان بانگ و خروشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی خونابهٔ حسرت فشاندند</p></div>
<div class="m2"><p>وزان حسرت بصد حیرت بماندند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بآخر یوسف آنجا باز آمد</p></div>
<div class="m2"><p>بتخت خود بصد اعزاز آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زمانی بود و خلقی در رسیدند</p></div>
<div class="m2"><p>میان صُفّه خوانی برکشیدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین فرمود یوسف شاه محبوب</p></div>
<div class="m2"><p>که جمع آیند فرزندان یعقوب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولی هر یک یکی را برگزینند</p></div>
<div class="m2"><p>بیک خوان دو برادر در نشینند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان کو گفت بنشستند با هم</p></div>
<div class="m2"><p>نشاندند ابن یامین را بماتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو تنها ماند آنجا ابن یامین</p></div>
<div class="m2"><p>ز یوسف یادش آمد گشت غمگین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسی بگریست از اندوه یوسف</p></div>
<div class="m2"><p>بسی خورد از فراق او تأسّف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ازو پرسید یوسف شاه احرار</p></div>
<div class="m2"><p>که ای کودک چرا گرئی چنین زار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین گفت او که چون تنها بماندم</p></div>
<div class="m2"><p>ازین اندوه خون باید فشاندم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که بودست ای عزیزم یک برادر</p></div>
<div class="m2"><p>من و او هم پدر بودیم و مادر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون او گُم شدست از دیرگاهی</p></div>
<div class="m2"><p>بسوی او کسی را نیست راهی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر او نیز با این خسته بودی</p></div>
<div class="m2"><p>بخوان با من بهم بنشسته بودی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفت این و یکی خوان داشت در پیش</p></div>
<div class="m2"><p>همه پر آب کرد از دیدهٔ خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نچندانی گریست از اشک دیده</p></div>
<div class="m2"><p>که هرگز دیده بود آن اشک دیده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو یوسف آنچنان گریان بدیدش</p></div>
<div class="m2"><p>چو جان خود دلی بریان بدیدش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو گفتا که مگوی ای جوان تو</p></div>
<div class="m2"><p>مرا چون یوسفی گیر این زمان تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که تا هم کاسه باشم من عزیزت</p></div>
<div class="m2"><p>ز من هم کاسهٔ بهتر چه چیزت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زبان بگشاد خوانسالار آنگاه</p></div>
<div class="m2"><p>که این کاسه پر اشک اوست ای شاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگو کین اشک خونین چون خوری تو</p></div>
<div class="m2"><p>روا داری که نان با خون خوری تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین گفت آنگهی یوسف که خاموش</p></div>
<div class="m2"><p>که خون من ازین غم می‌زند جوش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دلم گوئی ازین خون قوت جان یافت</p></div>
<div class="m2"><p>چنین خونی بخون خوردن توان یافت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یتیمست او و جان می‌پرورم من</p></div>
<div class="m2"><p>اگر خونی یتیمی می‌خورم من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنین گفتند فرزندان یعقوب</p></div>
<div class="m2"><p>که خُردست او اگرچه هست محبوب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نداند هیچ آداب ملوک او</p></div>
<div class="m2"><p>بخدمت چون کند زیبا سلوک او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ازان ترسیم ما و جای آنست</p></div>
<div class="m2"><p>که خردی پیش شاه خرده دانست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنین آمد جواب از یوسف خوب</p></div>
<div class="m2"><p>که شایسته بود فرزند یعقوب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کسی کو را پدر یعقوب باشد</p></div>
<div class="m2"><p>ازو هرچیز کآید خوب باشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس آنگه گفت هان ای ابن یامین</p></div>
<div class="m2"><p>چرا زردست روی تو بگو هین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین گفت او که یوسف در فراقم</p></div>
<div class="m2"><p>بکشت وزرد کرد از اشتیاقم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدو گفتا که گر شد زرد رویت</p></div>
<div class="m2"><p>پشولیده چرا شد مشک مویت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین گفت او که چون مادر ندارم</p></div>
<div class="m2"><p>پشولیدست موی و روزگارم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس آگه گفت چون دیدی پدر را</p></div>
<div class="m2"><p>که می‌گویند گُم کرد او پسر را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنین گفت او که نابینا بماندست</p></div>
<div class="m2"><p>چو یوسف نیست او تنها بماندست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جهانی آتشش بر جان نشسته</p></div>
<div class="m2"><p>میان کلبهٔ احزان نشسته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز بس کز دیده او خوناب رانده</p></div>
<div class="m2"><p>ز خون و آب در گرداب مانده</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو از یوسف فرا اندیش گیرد</p></div>
<div class="m2"><p>دران ساعت مرا در پیش گیرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چگویم من که آن ساعت بزاری</p></div>
<div class="m2"><p>چگونه گرید او از بیقراری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر حاضر بود آن روز سنگی</p></div>
<div class="m2"><p>شود در حال خونی بی درنگی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو از یعقوب یوسف را خبر شد</p></div>
<div class="m2"><p>بیکره برقعش از اشک تر شد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نهان می‌کرد آن اشک از تأسف</p></div>
<div class="m2"><p>که آمد پیگ حضرت پیش یوسف</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که رخ بنمای چندش رنجه داری</p></div>
<div class="m2"><p>که شیرین گوئی و سر پنجه داری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو از اشک آن نقاب او بر آغشت</p></div>
<div class="m2"><p>ز روی خود نقاب آخر فرو هشت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو القصّه بدیدش ابن یامین</p></div>
<div class="m2"><p>جدا شد زو تو گفتی جان شیرین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو دریائی دلش در جوش افتاد</p></div>
<div class="m2"><p>بزد یک نعره و بیهوش افتاد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بصد حیلة چو باهوش آمد آنگاه</p></div>
<div class="m2"><p>ازو پرسید یوسف کای نکو خواه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چه افتادت که بیهوش اوفتادی</p></div>
<div class="m2"><p>بیفسردی و در جوش اوفتادی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چنین گفت او ندانم تو چه چیزی</p></div>
<div class="m2"><p>که گوئی یوسفی گرچه غریزی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بجای یوسفت بگزیده‌ام من</p></div>
<div class="m2"><p>تو گوئی پیش ازینت دیده‌ام من</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به یوسف مانی از بهر خدا تو</p></div>
<div class="m2"><p>اگر هستی چه رنجانی مرا تو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من بی کس ندارم این پر و بال</p></div>
<div class="m2"><p>نمی‌دانم تو می‌دانی بگو حال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کسی کین قصّه‌ام افسانه خواند</p></div>
<div class="m2"><p>خرد او را ز خود بیگانه داند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ترا در پردهٔ جان آشنائیست</p></div>
<div class="m2"><p>که با او پیش ازینت ماجرائیست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اگر بازش شناسی یک دمی تو</p></div>
<div class="m2"><p>سبق بردی ز خلق عالمی تو</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>وگر با او دلی بیگانه داری</p></div>
<div class="m2"><p>یقین طور مرا افسانه داری</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دل تو گر ندارد آشنائی</p></div>
<div class="m2"><p>نگیرد هیچ کارت روشنائی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کسی کز آشنائی بوی دارد</p></div>
<div class="m2"><p>همو با قرب حضرت خوی دارد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو او با حق بود حق نیز جاوید</p></div>
<div class="m2"><p>ازان سایه ندارد دور خورشید</p></div></div>