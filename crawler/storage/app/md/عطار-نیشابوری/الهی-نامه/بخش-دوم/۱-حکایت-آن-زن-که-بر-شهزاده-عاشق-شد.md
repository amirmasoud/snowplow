---
title: >-
    (۱) حکایت آن زن که بر شهزاده عاشق شد
---
# (۱) حکایت آن زن که بر شهزاده عاشق شد

<div class="b" id="bn1"><div class="m1"><p>شهی را سیمبر شهزاده‌ای بود</p></div>
<div class="m2"><p>ز زلفش مه به دام افتاده‌ای بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیدی هیچ مردم روی آن شاه</p></div>
<div class="m2"><p>که روی دل نکردی سوی آن ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان اُعجوبهٔ آفاق بودی</p></div>
<div class="m2"><p>که آفاقش همه عشّاق بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو ابرویش که هم شکل کمان بود</p></div>
<div class="m2"><p>دو حاجب بر در سلطان جان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو چشمی تیر مژگانش بدیدی</p></div>
<div class="m2"><p>دلش قربان شدی کیشش گزیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که دیدی ابروی آن دلستان را</p></div>
<div class="m2"><p>که دل قربان نکردی آن کمان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهانش سی گهر پیوند کرده</p></div>
<div class="m2"><p>ز دو لعل خوشابش بند کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خطش فتوی ده عشاق بوده</p></div>
<div class="m2"><p>به زیبائی چو ابرو طاق بوده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنخدانش سر مردان فگنده</p></div>
<div class="m2"><p>بمردی گوی در میدان فگنده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنی در عشق آن بت سرنگون شد</p></div>
<div class="m2"><p>دلش بسیار کرد افغان و خون شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو هجرش دست برد خویش بنمود</p></div>
<div class="m2"><p>ازان سرگشته و دلریش بنمود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بزیر خویش خاکستر فرو کرد</p></div>
<div class="m2"><p>چو آتش بود مأواگه ازو کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه شب نوحهٔ آن ماه کردی</p></div>
<div class="m2"><p>گهی خون ریختی گه آه کردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر روزی به صحرا رفتی آن ماه</p></div>
<div class="m2"><p>دوان گشتی زن بیچاره در راه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو گوئی پیش اسپش می‌دویدی</p></div>
<div class="m2"><p>دو گیسو چون دو چوگان می‌کشیدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگه می‌کردی از پس روی آن ماه</p></div>
<div class="m2"><p>چو باران می‌فشاندی اشک بر راه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز صد چاوش پیاپی چوب خوردی</p></div>
<div class="m2"><p>که نه فریاد و نه آشوب کردی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به نظّاره جهانی خلق بودی</p></div>
<div class="m2"><p>که آن زن را به مردان می‌نمودی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه مردان ازو حیران بمانده</p></div>
<div class="m2"><p>زن بیچاره سرگردان بمانده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آخر چون ز حد بگذشت این کار</p></div>
<div class="m2"><p>دل شهزاده غمگین گشت ازین بار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پدر را گفت تا کی زین گدائی</p></div>
<div class="m2"><p>مرا از ننگ این زن ده رهائی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنین فرمود آنگه شاه عالی</p></div>
<div class="m2"><p>که در میدان برید آن کُرّه حالی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به پای کرّه در بندید مویش</p></div>
<div class="m2"><p>بتازید اسپ تیز از چارسویش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که تا آن شوم گردد پاره پاره</p></div>
<div class="m2"><p>وزین کارش جهان گیرد کناره</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کشد چون پیل مستش اسپ در راه</p></div>
<div class="m2"><p>پیاده رخ نیارد نیز در شاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به میدان رفت شاه و شاهزاده</p></div>
<div class="m2"><p>جهانی خلق بودند ایستاده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه از درد زن خون بار گشته</p></div>
<div class="m2"><p>وزان خون خاک چون گلنار گشته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو لشکر خویش را بر هم فگندند</p></div>
<div class="m2"><p>که تا مویش به پای اسپ بندند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زن سرگشته پیش شاه افتاد</p></div>
<div class="m2"><p>به حاجت خواستن در راه افتاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که چون بکشیم و آنگه بزاری</p></div>
<div class="m2"><p>مرا یک حاجتست آخر برآری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شهش گفتا ترا گر حاجت آنست</p></div>
<div class="m2"><p>که جان بخشم بتو خود قصد جانست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وگر گوئی مکن گیسو کشانم</p></div>
<div class="m2"><p>بجز در پای اسپت خون نرانم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وگر گوئی امانم ده زمانی</p></div>
<div class="m2"><p>زمانی نیست ممکن بی امانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ور از شهزاده خواهی همنشینی</p></div>
<div class="m2"><p>زمانی نیز روی او نه بینی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زنش گفتا که من جان می‌نخواهم</p></div>
<div class="m2"><p>زمانی نیز امان زان می‌نخواهم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نمی‌گویم که ای شاه نکوکار</p></div>
<div class="m2"><p>مکُش در پای اسپم سرنگونسار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مر اگر شاه عالم می‌دهد دست</p></div>
<div class="m2"><p>برون زین چار حاجت حاجتی هست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا جاوید آن حاجت تمامست</p></div>
<div class="m2"><p>شهش گفتا بگو آخر کدامست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که گر زین چار حاجت سر بتابی</p></div>
<div class="m2"><p>جزین چیزی که می‌خواهی بیابی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زنش گفتا اگر امروز ناچار</p></div>
<div class="m2"><p>بزیر پای اسپم می‌کُشی زار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا آنست حاجت ای خداوند</p></div>
<div class="m2"><p>که موی من به پای اسپ او بند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که تا چون اسپ تازد بهر آن کار</p></div>
<div class="m2"><p>بزیر پای اسپم او کُشد زار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که چون من کُشتهٔ آن ماه گردم</p></div>
<div class="m2"><p>همیشه زندهٔ این راه گردم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بلی گر کُشتهٔ معشوق باشم</p></div>
<div class="m2"><p>ز نور عشق بر عیّوق باشم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زنی‌ام مردئی چندان ندارم</p></div>
<div class="m2"><p>دلم خون گشت گوئی جان ندارم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنین وقتی چو من زن را که اهلست</p></div>
<div class="m2"><p>برآور این قدر حاجت که سهلست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز صدق و سوز او شه نرم دل شد</p></div>
<div class="m2"><p>چه می‌گویم ز اشکش خاک گل شد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ببخشید و بایوانش فرستاد</p></div>
<div class="m2"><p>چو نو جانی بجانانش فرستاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بیا ای مرد اگر با ما رفیقی</p></div>
<div class="m2"><p>در آموز از زنی عشق حقیقی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وگر کم از زنانی سر فرو پوش</p></div>
<div class="m2"><p>کم از حیزی نهٔ این قصّه بنیوش</p></div></div>