---
title: >-
    (۴) حکایت امیرالمؤمنین علی کرم الله وجهه بامور
---
# (۴) حکایت امیرالمؤمنین علی کرم الله وجهه بامور

<div class="b" id="bn1"><div class="m1"><p>علی می‌رفت روزی گرمگاهی</p></div>
<div class="m2"><p>رسید آسیب او بر مور راهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر آن مور می‌زد پا و دستی</p></div>
<div class="m2"><p>ز عجزش در علی آمد شکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بترسید و بغایت مضطرب شد</p></div>
<div class="m2"><p>چنان شیری ز موری منقلب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی بگریست و حیلت کرد بسیار</p></div>
<div class="m2"><p>که تا آن مور باز آمد برفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبانگه مصطفی را دید در خواب</p></div>
<div class="m2"><p>بدو گفت ای علی در راه مشتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که دو روز از پی یک مور دایم</p></div>
<div class="m2"><p>ز تو بود آسمانها پُر متایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباشی از سلوک خویش آگاه</p></div>
<div class="m2"><p>که موری را کنی آزرده در راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان موری که معنی دار بودست</p></div>
<div class="m2"><p>همه ذکر خدایش کار بودست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علی را لرزه بر اندام افتاد</p></div>
<div class="m2"><p> ز موری شیر حق در دام افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیمبر گفت خوش باش و مکن شور</p></div>
<div class="m2"><p>که پیش حق شفیعت شد همان مور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که یارب قصد حیدر در میان نیست</p></div>
<div class="m2"><p>اگر خصمی بمن بود این زمان نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جوانمردا بدان کز درد دین بود</p></div>
<div class="m2"><p>که با موری چنان شیری چنین بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو حیدر در شجاعت شیر زوری</p></div>
<div class="m2"><p>که دیدی بسته بر فتراک موری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خُنُک جانی که او از حق خبر داشت</p></div>
<div class="m2"><p>قدم بر امر حق بنهاد و برداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو گر بر جهل مطلق در سلوکی</p></div>
<div class="m2"><p>گدای مطلقی ور ازملوکی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نظر باید فگند آنگه قدم زد</p></div>
<div class="m2"><p>که نتوان بی‌نظر در ره قدم زد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر تو بی‌نظر در ره زنی گام</p></div>
<div class="m2"><p>نگونساریت بار آرد سرانجام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چوبر عمیا روی همچون خران تو</p></div>
<div class="m2"><p>نه ممتازی بعقل از دیگران تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدم بشمرده نه گر مرد راهی</p></div>
<div class="m2"><p>که بشمرده‌ست از مه تا به ماهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر گامی نهی بی‌هیچ فرمان</p></div>
<div class="m2"><p>بسی دردت رسد بی‌هیچ درمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر اینجا گام برگیری زمانی</p></div>
<div class="m2"><p>نباید رفت در گورت جهانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی هر کس که اینجا یک زمان رفت</p></div>
<div class="m2"><p>همان انگار کانجا صد جهان رفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگرچه حیرت اینجا یک دم افتد</p></div>
<div class="m2"><p>ولی آنجایگه صد عالم افتد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر امروز گامی می‌نهی پاک</p></div>
<div class="m2"><p>نباید رفت صد فرسنگ در خاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دریغا می‌نبینی سود بسیار</p></div>
<div class="m2"><p>که گر بینی دمی ننشینی از کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهر گامی که برگیری تو امروز</p></div>
<div class="m2"><p>ز حضرت تحفهٔ یابی دلفروز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنین سودی چو هر دم می‌توان کرد</p></div>
<div class="m2"><p>چرا از کاهلی باید زیان کرد</p></div></div>