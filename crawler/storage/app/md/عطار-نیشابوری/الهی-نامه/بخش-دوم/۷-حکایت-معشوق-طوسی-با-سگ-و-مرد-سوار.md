---
title: >-
    (۷) حکایت معشوق طوسی با سگ و مرد سوار
---
# (۷) حکایت معشوق طوسی با سگ و مرد سوار

<div class="b" id="bn1"><div class="m1"><p>مگر معشوق طوسی گرمگاهی</p></div>
<div class="m2"><p>چو بی‌خویشی برون می‌شد براهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی سگ پیش او آمد دران راه</p></div>
<div class="m2"><p>ز بی‌خویشی بزد سنگیش ناگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سواری سبزجامه دید از دور</p></div>
<div class="m2"><p>درآمد از پسش روئی همه نور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزد یک تازیانه سخت بروی</p></div>
<div class="m2"><p>بدو گفتا که هان ای بیخبر هی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌دانی که بر که می‌زنی سنگ</p></div>
<div class="m2"><p>که با او نیستی در اصل همرنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه از یک قالبی با او بهم تو</p></div>
<div class="m2"><p>چرا از خویش می‌داریش کم تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سگ از قالب قدرة جدا نیست</p></div>
<div class="m2"><p>فزونی کردنت بر سگ روا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سگان در پرده پنهانند ای دوست</p></div>
<div class="m2"><p>ببین گر پاک مغزی بیش ازین پوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که سگ گرچه بصورت ناپسندست</p></div>
<div class="m2"><p>ولیکن در صفت جانش بلندست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسی اسرار با سگ در میانست</p></div>
<div class="m2"><p>ولیکن ظاهر او سدِّ آنست</p></div></div>