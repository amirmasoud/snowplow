---
title: >-
    (۱) حکایت عزرائیل و سلیمان علیهما السلام و آن مرد
---
# (۱) حکایت عزرائیل و سلیمان علیهما السلام و آن مرد

<div class="b" id="bn1"><div class="m1"><p>شنیدم من که عزرائیل جانسوز</p></div>
<div class="m2"><p>در ایوان سلیمان رفت یک روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانی دید پیش او نشسته</p></div>
<div class="m2"><p>نظر بگشاد بر رویش فرشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو او را دید از پیشش بدر شد</p></div>
<div class="m2"><p>جوان از بیمِ او زیر و زبر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلیمان را چنین گفت آن جوان زود</p></div>
<div class="m2"><p>که فرمان ده که تا میغ این زمان زود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا زین جایگه جائی برد دور</p></div>
<div class="m2"><p>که گشتم از نهیب مرگ رنجور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیمان گفت تا میغ آن زمانش</p></div>
<div class="m2"><p>ببرد از پارس تا هندوستانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو یک روزی به سر آمد ازین راز</p></div>
<div class="m2"><p>به پیش تخت عزرائیل شد باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلیمان گفتش ای بی تیغ خون ریز</p></div>
<div class="m2"><p>چرا کردی نظر سوی جوان تیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوابش داد عزرائیل آنگاه</p></div>
<div class="m2"><p>که فرمانم چنین آمد ز درگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که او را تا سه روز از راه برگیر</p></div>
<div class="m2"><p>به هندستانش جان ناگاه برگیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو اینجا دیدمش ماندم در این سوز</p></div>
<div class="m2"><p>کز اینجا چون رود آنجا به سه روز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو میغ آورد تا هندوستانش</p></div>
<div class="m2"><p>شدم آنجا و کردم قبض جانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مدامت این حکایت حسب حال است</p></div>
<div class="m2"><p>که از حکم ازل گشتن محال است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه برخیزد ز تدبیری که کردند</p></div>
<div class="m2"><p>که ناکام است تقدیری که کردند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو اندر نقطهٔ تقدیر اول</p></div>
<div class="m2"><p>نگه می‌کن مشو در کار احول</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو کار او نه چون کار تو آید</p></div>
<div class="m2"><p>گلی گر بشکفد خار تو آید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو مشکر بود هر کو در دوئی بود</p></div>
<div class="m2"><p>بلای من منی بود و توئی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو برخیزد دو بودن ازمیان راست</p></div>
<div class="m2"><p>یکی گردد بهم این خواست و آن خواست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز هر مژه اگر صد خون گشائی</p></div>
<div class="m2"><p>فرو بستند چشمت، چون گشائی؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دستت بسته‌اند ای خسته آخر</p></div>
<div class="m2"><p>چه بگشاید ز دست بسته آخر؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرفته درد دین اهل خرد را</p></div>
<div class="m2"><p>میان جادوی خواهی تو خود را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه اجزای عالم اهل دردند</p></div>
<div class="m2"><p>سر افشانان میدان نبردند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو یک دم درد دین داری؟ نداری</p></div>
<div class="m2"><p>بجز سودای بیکاری نداری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر یک ذره درد دین بدانی</p></div>
<div class="m2"><p>بمیری ز آرزوی زندگانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ولیکن بر جگر ناخورده تیغی</p></div>
<div class="m2"><p>نه هرگز درد دانی نه دریغی</p></div></div>