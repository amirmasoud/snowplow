---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش که ای مغرور مانده</p></div>
<div class="m2"><p>ز اسرا رحقیقت دور مانده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن امروز ضایع زندگانی</p></div>
<div class="m2"><p>چو میدانی که تو فردا نمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببابل می‌روی ای مرد فرتوت</p></div>
<div class="m2"><p>که سحر آموزی از هاروت و ماروت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزاران سال شد کان دو فرشته</p></div>
<div class="m2"><p>نگونسارند در چَه تشنه گشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزیشان آنگهی تا آب آن چاه</p></div>
<div class="m2"><p>مسافت یک وجب نیست ای عجب راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو نتوانند خود را آب دادن</p></div>
<div class="m2"><p>کجا دَر می‌توانندت گشادن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو استاد این چنین باشد پریشان</p></div>
<div class="m2"><p>که خواهد کرد شاگردیِ ایشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا امروز بینم دیو گشته</p></div>
<div class="m2"><p>نخواهی گشت در فردا فرشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگرمرگت ببابل می‌دواند</p></div>
<div class="m2"><p>که سرگردان و غافل می‌دواند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر مرگ تو در بابل نبودی</p></div>
<div class="m2"><p>ترا این آرزو در دل نبودی</p></div></div>