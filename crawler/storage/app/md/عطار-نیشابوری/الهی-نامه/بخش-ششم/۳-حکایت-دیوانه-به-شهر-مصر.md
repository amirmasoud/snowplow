---
title: >-
    (۳) حکایت دیوانه به شهر مصر
---
# (۳) حکایت دیوانه به شهر مصر

<div class="b" id="bn1"><div class="m1"><p>بشهر مصر در شوریدهٔ‌ای بود</p></div>
<div class="m2"><p>که در عین الیقینش دیده‌ای بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گفت او که هر شوریدهٔ راه</p></div>
<div class="m2"><p>که میرد از غم معشوق ناگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب نیست آن، عجب اینست کین سوز</p></div>
<div class="m2"><p>گذارد عاشقی در زندگی روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر عاشق بماند زنده روزی</p></div>
<div class="m2"><p>بوَد چون شمع در اشکی و سوزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگیرد کار عاشق روشنائی</p></div>
<div class="m2"><p>مگر چون شمع سوزد در جدائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چوسوز عاشق از صد شمع بیشست</p></div>
<div class="m2"><p>چو شمعش روشنی از شمع خویشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر معشوق یابد عاشق زار</p></div>
<div class="m2"><p>روان گردد بسر مانند پرگار</p></div></div>