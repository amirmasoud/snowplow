---
title: >-
    (۱۲) حکایت گناه کار روز محشر
---
# (۱۲) حکایت گناه کار روز محشر

<div class="b" id="bn1"><div class="m1"><p>چنین نقلی درستست از پیمبر</p></div>
<div class="m2"><p>که حق گوید بشخصی روز محشر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ای بنده بیا و نامه برخوان</p></div>
<div class="m2"><p>که تا چه کردهٔ عمری فراوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بنده نامه برخواند سراسر</p></div>
<div class="m2"><p>نه بیند جز معاصی چیز دیگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو در نامه نه بیند جز سیاهی</p></div>
<div class="m2"><p>زبان بگشاید و گوید الهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدوزخ می‌روم زین عمر تاوان</p></div>
<div class="m2"><p>حقش گوید که پشت نامه برخوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو پشت نامه برخواند بیکبار</p></div>
<div class="m2"><p>چنان یابد نوشته آخر کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتوبه در پشیمان گشته باشد</p></div>
<div class="m2"><p>همه دردیش درمان گشته باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجای هر بدی دانندهٔ راز</p></div>
<div class="m2"><p>بداده باشدش ده نیکوئی باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدی را چون پشیمان گشته باشد</p></div>
<div class="m2"><p>خدا ده نیکوئی بنوشته باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بنده آن ببیند شاد گردد</p></div>
<div class="m2"><p>زهی بنده که چون آزاد گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بحق گوید که ای قیّومِ مطلق</p></div>
<div class="m2"><p>ندیدم ازکرام الکاتبین حق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که من دارم گنه زین بیش بسیار</p></div>
<div class="m2"><p>که ننوشتند بر من آن دو هشیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگو کان بر من مسکین نوشتند</p></div>
<div class="m2"><p>مگر آن می‌ستردند این نوشتند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که تا چندان که بد کردم ز آغاز</p></div>
<div class="m2"><p>بهر یک ده نکوئی می‌دهی باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر چه من گناه آلود مردم</p></div>
<div class="m2"><p>ز فضلت بر گناهان سود کردم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیمبر از چنین گفتار و کردار</p></div>
<div class="m2"><p>بخندید و شدش دندان پدیدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس آنگه گفت ای دارندهٔ پاک</p></div>
<div class="m2"><p>زهی گستاخی آخر از کفی خاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز سرّی کان میان جان پاکست</p></div>
<div class="m2"><p>اگرآگه شوی بیم هلاکست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که می‌داند که این سر عجب چیست</p></div>
<div class="m2"><p>چنین سری عجایب را سبب چیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترا در پیش چندین پیچ پیچی</p></div>
<div class="m2"><p>نه زان آمد که یعنی هیچ هیچی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی این جمله زان افتاد در راه</p></div>
<div class="m2"><p>که تا از خویش گردی بو که آگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو تو معشوق بودی او چنان کرد</p></div>
<div class="m2"><p>که از چشم خود و خلقت نهان کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هزاران پردهٔ اسباب بنهاد</p></div>
<div class="m2"><p>درون جمله تختِ خواب بنهاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو با معشوق زیر پرده بر تخت</p></div>
<div class="m2"><p>توانی خفت بی غیری زهی بخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو نتوان دید سر تا پای معشوق</p></div>
<div class="m2"><p>چنین بهتر که باشد جای معشوق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که جلوه دادن معشوق هرگز</p></div>
<div class="m2"><p>مسلَّم نیست پنهان باید از عز</p></div></div>