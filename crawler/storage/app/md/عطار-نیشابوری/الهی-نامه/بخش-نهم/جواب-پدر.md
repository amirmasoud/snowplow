---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتا که جهلت غالب آمد</p></div>
<div class="m2"><p>دلت این جام را زان طالب آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که تا چون واقف آئی از همه راز</p></div>
<div class="m2"><p>شوی برجملهٔ عالم سرافراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خود را بر فلک این جاه بینی</p></div>
<div class="m2"><p>همه خلق زمین در چاه بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عُجب جاهِ خود از خود شوی پُر</p></div>
<div class="m2"><p>بمانی جاودانی در تکبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر در پیش داری جامِ جمشید</p></div>
<div class="m2"><p>که یک یک ذره می‌بینی چو خورشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گر زان جام بینی ذرّه ذرّه</p></div>
<div class="m2"><p>که چون مرگت نهد بر فرق ارّه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نداری هیچ حاصل چون جم از جام</p></div>
<div class="m2"><p>که چون جم زار میری هم سرانجام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو هست این جام در چاه اوفتادن</p></div>
<div class="m2"><p>حرامت باد از راه اوفتادن</p></div></div>