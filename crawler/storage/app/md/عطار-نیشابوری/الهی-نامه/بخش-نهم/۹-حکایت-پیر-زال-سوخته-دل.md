---
title: >-
    (۹) حکایت پیر زال سوخته دل
---
# (۹) حکایت پیر زال سوخته دل

<div class="b" id="bn1"><div class="m1"><p>مگر یک روز در بازارِ بغداد</p></div>
<div class="m2"><p>بغایت آتشی سوزنده افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان برخاست از مردم بیکبار</p></div>
<div class="m2"><p>وزان آتش قیامت شد پدیدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بره در پیر زالی مبتلائی</p></div>
<div class="m2"><p>عصا در درست می‌آمد ز جائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی گفتش مرو دیوانهٔ تو</p></div>
<div class="m2"><p>که افتاد آتشی در خانهٔ تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنش گفتا توئی دیوانه تن زن</p></div>
<div class="m2"><p>که حق هرگز نسوزد خانهٔ من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بآخر چون بسوخت آتش جهانی</p></div>
<div class="m2"><p>نبود آن زال را ز آتش زیانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفتند هان ای زالِ دمساز</p></div>
<div class="m2"><p>بگو کز چه بدانستی چنین راز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین گفت آنگه آن زال فروتن</p></div>
<div class="m2"><p>که یا خانه بسوزد یا دل من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو سوخت از غم دل دیوانهٔ را</p></div>
<div class="m2"><p>نخواهد سوخت آخر خانهٔ را</p></div></div>