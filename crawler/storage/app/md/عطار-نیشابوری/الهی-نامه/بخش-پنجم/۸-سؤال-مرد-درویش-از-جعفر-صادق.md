---
title: >-
    (۸) سؤال مرد درویش از جعفر صادق
---
# (۸) سؤال مرد درویش از جعفر صادق

<div class="b" id="bn1"><div class="m1"><p>مگر پرسید آن درویش حالی</p></div>
<div class="m2"><p>بصدق از جعفر صادق سؤالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از چیست این همه کارت شب و روز</p></div>
<div class="m2"><p>جوابش داد آن شمع دلفروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که چون کارم یکی دیگر نمی‌کرد</p></div>
<div class="m2"><p>کسی روزی من چون من نمی‌خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کار من مرا بایست کردن</p></div>
<div class="m2"><p>فکندم کاهلی کردن ز گردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو رزق من مرا افتاد ز آغاز</p></div>
<div class="m2"><p>مرا نه حرص باقی ماند و نه آز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مرگ من مرا افتاد ناکام</p></div>
<div class="m2"><p>برای مرگ خود برداشتم گام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو در مردم وفائی می‌ندیدم</p></div>
<div class="m2"><p>بجان و دل وفای حق گزیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جزین چیزی که می‌پنداشتم من</p></div>
<div class="m2"><p>چو می‌پنداشتم بگذاشتم من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌دانم که تو با خود بس آئی</p></div>
<div class="m2"><p>ز چندین تفرقه کی واپس آئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سه پهلوست آرزوهای من و تو</p></div>
<div class="m2"><p>تو می‌خواهی که گردد چار پهلو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو کعبه یک جهت شو گر زمائی</p></div>
<div class="m2"><p>بسان کعبتین آخر چرائی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترا نه بهر بازی آفریدند</p></div>
<div class="m2"><p>ز بهر سرفرازی آفریدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مده از دست عمر خویش زنهار</p></div>
<div class="m2"><p>مخور بر عمر خود زین بیش زنهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمی‌دانی که هر شب صبح بشتافت</p></div>
<div class="m2"><p>ترا در خواب جیب عمر بشکافت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازان ترسم که چون بیدارگردی</p></div>
<div class="m2"><p>نبینی هیچ نقد و خوار گردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه کار تو بازی می‌نماید</p></div>
<div class="m2"><p>نمازت نانمازی می‌نماید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نمازی کان بغفلت کردهٔ تو</p></div>
<div class="m2"><p>بهای آن نیابی گِردهٔ تو</p></div></div>