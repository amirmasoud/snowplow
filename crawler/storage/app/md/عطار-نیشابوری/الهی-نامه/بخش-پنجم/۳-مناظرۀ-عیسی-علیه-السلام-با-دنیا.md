---
title: >-
    (۳) مناظرۀ عیسی علیه السلام با دنیا
---
# (۳) مناظرۀ عیسی علیه السلام با دنیا

<div class="b" id="bn1"><div class="m1"><p>مسیح پاک کز دنیا علو داشت</p></div>
<div class="m2"><p>بسی دیدار دنیا آرزو داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر می‌رفت روزی غرقهٔ نور</p></div>
<div class="m2"><p>بره در پیر زالی دید از دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپیدش گشته موی و پشتِ او خم</p></div>
<div class="m2"><p>فتاده جملهٔ دندانش از هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو چشمش ازرق و چون قیر رویش</p></div>
<div class="m2"><p>نجاست می‌دمید از چار سویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببر درجامهٔ صد رنگ بودش</p></div>
<div class="m2"><p>دلی پر کین میان چنگ بودش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بصد رنگی نگارین کرده یک دست</p></div>
<div class="m2"><p>دگر دستش بخون آلوده پیوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر موییش منقار عُقابی</p></div>
<div class="m2"><p>فرو هشته بروی او نقابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عیسی دید او را گفت ای زال</p></div>
<div class="m2"><p>بگو تا کیستی ای زشتِ مختال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین گفت او که چون بس راستی تو</p></div>
<div class="m2"><p>منم آن آرزو که خواستی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مسیحش گفت تو دنیای دونی</p></div>
<div class="m2"><p>منم گفتا چنین باری تو چونی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مسیحش گفت چون در پردهٔ تو</p></div>
<div class="m2"><p>چرا این جامه رنگین کردهٔ تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین گفت او که در پرده ازانم</p></div>
<div class="m2"><p>که تا هرگز نه بیند کس عیانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که گر رویم بدین زشتی به بینند</p></div>
<div class="m2"><p>کجا یک لحظه پیش من نشینند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازان این جامه رنگین کرده‌ام من</p></div>
<div class="m2"><p>که گم ره عالمی زین کرده‌ام من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا چون جامه رنگارنگ بینند</p></div>
<div class="m2"><p>همه ناکام مهر من گزینند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مسیحش گفت ای زندانِ خواری</p></div>
<div class="m2"><p>چرا یک دست خون آلوده داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جوابش داد کای صدر یگانه</p></div>
<div class="m2"><p>ز بس شوهر که کُشتم در زمانه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مسیحش گفت پس ای زال سرمست</p></div>
<div class="m2"><p>نگار از بهرِ چه کردی تو بر دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین گفت او که چون شوهر فریبم</p></div>
<div class="m2"><p>بسی باید نگار از بهرِ زیبم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مسیحش گفت چون کُشتی جهانی</p></div>
<div class="m2"><p>بر ایشان رحمتت نامد زمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین گفت او که من رحمت چه دانم</p></div>
<div class="m2"><p>من این دانم که خون جمله رانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مسیحش گفت چندان ای پریشان</p></div>
<div class="m2"><p>که ناری اندکی شفقت بر ایشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین گفت او که من شفقت شنودم</p></div>
<div class="m2"><p>ولی بر هیچکس مُشفق نبودم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منم در گردِ عالم هر زمانی</p></div>
<div class="m2"><p>که می‌افتد بدام من جهانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه کس را گلوگیر آمدم من</p></div>
<div class="m2"><p>مُرید خویش را پیر آمدم من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازو عیسی عجب ماند و چنین گفت</p></div>
<div class="m2"><p>که من بیزار گشتم از چنین جفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ببین این احمقان بیخبر را</p></div>
<div class="m2"><p>که می‌خواهند دنیا یکدگر را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نمی‌گیرند عبرت زین بلایه</p></div>
<div class="m2"><p>نمی‌سازند از تسلیم مایه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دریغا خلق این معنی ندیدند</p></div>
<div class="m2"><p>که دین از دست شد دنیا ندیدند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو حرفی چند گفت آن پاکِ معصوم</p></div>
<div class="m2"><p>بگردانید روی از دنیی شوم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو مُرداریست این دنیای غدّار </p></div>
<div class="m2"><p>تو چون سگ گشتهٔ مشغول مردار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو در بند سگ و مردار باشی</p></div>
<div class="m2"><p>پس از هر دو بتر صد بار باشی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر این سگ می‌نگردد سیرِ مردار</p></div>
<div class="m2"><p>تو زین سگ می‌نگردی سیر یکبار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر بندش کنی زو رسته باشی</p></div>
<div class="m2"><p>وگرنه روز و شب زو خسته باشی</p></div></div>