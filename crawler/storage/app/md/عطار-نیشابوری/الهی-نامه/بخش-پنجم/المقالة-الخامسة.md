---
title: >-
    المقالة الخامسة
---
# المقالة الخامسة

<div class="b" id="bn1"><div class="m1"><p>دوُم فرزند آمد با پدر گفت</p></div>
<div class="m2"><p>که من در جادوئی خواهم گهر سفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عالم جادوئی می‌خواهدم دل</p></div>
<div class="m2"><p>مرا گر جادوئی آید به حاصل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تماشا می‌کنم در هر دیاری</p></div>
<div class="m2"><p>بشادی می‌زیم بر هر کناری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی در صلح باشم گاه در حرب</p></div>
<div class="m2"><p>بوَد جولانگه، من شرق تا غرب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانی خویشتن را مرغ سازم</p></div>
<div class="m2"><p>زمانی همچو مردم سرفرازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانی کوه گیرم چون پلنگان</p></div>
<div class="m2"><p>زمانی بحر شورم چون نهنگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه صاحب جمالان را به بینم</p></div>
<div class="m2"><p>درون پرده با هر یک نشینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر چیزی که باید راه یابم</p></div>
<div class="m2"><p>ز ماهی حکمِ خود تا ماه یابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین منصب تأمّل کن نکو تو</p></div>
<div class="m2"><p>ازین خوشتر کرا باشد بگو تو</p></div></div>