---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش که دیوت غالب آمد</p></div>
<div class="m2"><p>دلت زان جادوئی را طالب آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از دیوت گر این حاصل نبودی</p></div>
<div class="m2"><p>ترا این آرزو در دل نبودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر زین دیو بگذشتی برستی</p></div>
<div class="m2"><p>وگرنه مُدبری شیطان پرستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نداری از خدا آخر خبر هیچ</p></div>
<div class="m2"><p>که کار دیو می‌خواهی دگر هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدا را گردهٔ ندهی بدرویش</p></div>
<div class="m2"><p>هوا را باز گیری صد ره از خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخی باشی ریا را و هوا را</p></div>
<div class="m2"><p>ولیکن دوزخی باشی خدا را</p></div></div>