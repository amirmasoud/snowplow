---
title: >-
    (۵) حکایت مرد ترسا که مسلمان شد
---
# (۵) حکایت مرد ترسا که مسلمان شد

<div class="b" id="bn1"><div class="m1"><p>یکی ترسا مسلمان گشت و پیروز</p></div>
<div class="m2"><p>بمی خوردن شد آن جاهل دگر روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مادر مست دید او را ز دردی</p></div>
<div class="m2"><p>بدو گفت ای پسر آخر چه کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که شد آزرده عیسی زود ازتو</p></div>
<div class="m2"><p>محمّد ناشده خشنود از تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مخنّث وار رفتن ره نکو نیست</p></div>
<div class="m2"><p>که هر رعنا مزاجی مرد او نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمردی رَو دران دینی که هستی</p></div>
<div class="m2"><p>که نامردیست در دین بت پرستی</p></div></div>