---
title: >-
    (۹) گفتار آن مجنون در نمازی که یک نان نیرزد
---
# (۹) گفتار آن مجنون در نمازی که یک نان نیرزد

<div class="b" id="bn1"><div class="m1"><p>یکی مجنون که رفتی در ملامت</p></div>
<div class="m2"><p>بدو گفتند فردای قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی باشد که ده ساله نماز او</p></div>
<div class="m2"><p>منادی می‌کند شیب و فراز او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیک گرده ازو نخرد کسی آن</p></div>
<div class="m2"><p>بگوید بر سر مجمع بسی آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوابش داد مجنون کان نیرزد</p></div>
<div class="m2"><p>نمازش آن همه یک نان نیرزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که گر بخریدی آن را خلق وادی</p></div>
<div class="m2"><p>نبودی حاجت چندان منادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر صد کار باشد در مجازت</p></div>
<div class="m2"><p>نیاید یاد ازان جز در نمازت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمازت چون چنین باشد مجازی</p></div>
<div class="m2"><p>بوَد اندر حقیقت نانمازی</p></div></div>