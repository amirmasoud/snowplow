---
title: >-
    (۱۴) حکایت دیوانه‌ای که گلیم فروخت
---
# (۱۴) حکایت دیوانه‌ای که گلیم فروخت

<div class="b" id="bn1"><div class="m1"><p>گلیمی بود آن شوریده جان را</p></div>
<div class="m2"><p>بمردی داد تا بفروشد آن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو آن مرد گفت این بس درشتست</p></div>
<div class="m2"><p>بنرمی همچو پشت خارپشتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرید آن مرد ارزان و هم آنگاه</p></div>
<div class="m2"><p>خریداری پدیدار آمد از راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفتا گلیمی نرم داری؟</p></div>
<div class="m2"><p>چنین گفتا که دارم تا زر آری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو زر القصّه پیش آورد درویش</p></div>
<div class="m2"><p>نهادش آن گلیم آن مرد در پیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفتا گلیمی بی‌نظیرست</p></div>
<div class="m2"><p>که از نرمی بعینه چون حریرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی صوفی سوی او هوش می‌داشت</p></div>
<div class="m2"><p>خریدش تا فروشش گوش می‌داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی یک نعره زد گفت ای یگانه</p></div>
<div class="m2"><p>مرا بنشان درین صندوق خانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که می‌گردد حریر اینجا گلیمی</p></div>
<div class="m2"><p>سفالی می‌شود دُرّ یتیمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که من در جوهر خود چون سفالم</p></div>
<div class="m2"><p>ز صندوقت بگردد بو که حالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بر تو نخواهد گشت حالت</p></div>
<div class="m2"><p>نخواهد بود عمرت جز وبالت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو در ظلمت گذاری زندگانی</p></div>
<div class="m2"><p>چه حیوانی چه تو چون می‌ندانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه اعضای خود در بندِ دین کن</p></div>
<div class="m2"><p>اگر خود را چنان خواهی چنین کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مبین مشنو مگو الّا بفرمان</p></div>
<div class="m2"><p>که تا کافر نمیری ای مسلمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو مَردت می نه‌بینم در هدایت</p></div>
<div class="m2"><p> ز کافر مُردنت ترسم بغایت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برای عبرتست این طاق و ایوان</p></div>
<div class="m2"><p>تو جز شهوت نمی‌بینی چو حیوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ببازاری که دائم سودِ جان بود</p></div>
<div class="m2"><p>چگونه بایدت دائم زیان بود</p></div></div>