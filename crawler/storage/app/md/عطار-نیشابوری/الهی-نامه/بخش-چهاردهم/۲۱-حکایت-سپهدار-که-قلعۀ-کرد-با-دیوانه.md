---
title: >-
    (۲۱) حکایت سپهدار که قلعۀ کرد با دیوانه
---
# (۲۱) حکایت سپهدار که قلعۀ کرد با دیوانه

<div class="b" id="bn1"><div class="m1"><p>سپهداری برای کوتوالی</p></div>
<div class="m2"><p>بجائی قلعهٔ می‌کرد عالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی دیونهٔ آمد پدیدار</p></div>
<div class="m2"><p>به پیش خویش خواندش آن سپهدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفتا ببین کین قلعه چونست</p></div>
<div class="m2"><p>ز رفعت جفت طاق سر نگونست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازین قلعه کسی کاعزاز دارد</p></div>
<div class="m2"><p>ببین تا چه بلا زو باز دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان بگشاد آن دیوانه حالی</p></div>
<div class="m2"><p>بدو گفتا تو مردی تیره حالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلا چون ز آسمان می‌افتد آغاز</p></div>
<div class="m2"><p>بقلعه می‌روی پیش بلا باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلای خویشتن چون تو تمامی</p></div>
<div class="m2"><p>بلائی نیز مطلب ای گرامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خویش و از بلای خویش آنگاه</p></div>
<div class="m2"><p>خلاصی باشدت کلّی درین راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که افتاده شوی و پست گردی</p></div>
<div class="m2"><p>نمانی زنده تا که هست گردی</p></div></div>