---
title: >-
    (۱۶) حکایت مهستی دبیر با سلطان سنجر
---
# (۱۶) حکایت مهستی دبیر با سلطان سنجر

<div class="b" id="bn1"><div class="m1"><p>مهستی دبیر آن پاک جوهر</p></div>
<div class="m2"><p>مقرَّب بود پیش تختِ سنجر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه روی او بودی نه چون ماه</p></div>
<div class="m2"><p>ولیکن داشت پیوندی بدو شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی در مرغزار رادکان بود</p></div>
<div class="m2"><p>به پیش سنجر خسرو نشان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شب بگذشت پاسی شاه سنجر</p></div>
<div class="m2"><p>برای خواب آمد سوی بستر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهستی نیز رفت از خدمت شاه</p></div>
<div class="m2"><p>بسوی خیمهٔ خاص آمد آنگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر سنجر غلامی داشت ساقی</p></div>
<div class="m2"><p>که از خوبی ببُودش هیچ باقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمالش با ملاحت یار گشته</p></div>
<div class="m2"><p>ز هر دو شاه برخوردار گشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بصد دل بود شه دیوانهٔ او</p></div>
<div class="m2"><p>حریف مهستی بد لیک مهرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درآمد شه ز خواب او را طلب کرد</p></div>
<div class="m2"><p>ندیدش، قصدِ آن یاقوت لب کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لپاچه نیم شب بر پشت انداخت</p></div>
<div class="m2"><p>بکینه تیغِ هندی بر سر افراخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درآمد کرد در خیمه نگه شاه</p></div>
<div class="m2"><p>که مهستی در آنجا بود با ماه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر او دید ساقی را نشسته</p></div>
<div class="m2"><p>مهستی دل در آن مهروی بسته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزاری می‌نواخت از عشق رودش</p></div>
<div class="m2"><p>خوشی می‌گفت با خود این سرودش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که در برگیرمت من بَر لب کِشت</p></div>
<div class="m2"><p>گر امشب بایدم دو ک کسان رشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو سنجر گشت ازان احوال آگاه</p></div>
<div class="m2"><p>گرفت این بیت را زو یاد آنگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدل گفتا گر امشب من بتندی</p></div>
<div class="m2"><p>درین خیمه روم با تیغِ هندی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نماند زهره را این هر دو بر جای</p></div>
<div class="m2"><p>شوم در خونِ این دو بی سر و پای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مشوّش گشت و شد آخر بتعجیل</p></div>
<div class="m2"><p>به سوی خیمهٔ خود کرد تحویل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو روزی ده برآمد شاه یک روز</p></div>
<div class="m2"><p>فرو آراست جشنی عالم افروز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مهستی پیش سلطان چنگ می‌زد</p></div>
<div class="m2"><p>نوائی بس بلند آهنگ می‌زد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ستاده بود ساقی نیز بر پای</p></div>
<div class="m2"><p>قدح بر دست و چشم افکنده بر جای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شه آن بیت شبانه یاد می‌داشت</p></div>
<div class="m2"><p>ازو درخواست و خویش آزاد می‌داشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مهستی چون شنید این بیت از شاه</p></div>
<div class="m2"><p>بیفتاد از کنارش چنگ در راه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو برگی لرزه افتادش بر اندام</p></div>
<div class="m2"><p>برفت از هوش و عقلش ماند در دام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شه آمد بر سر بالینش بنشست</p></div>
<div class="m2"><p>برویش بر گلاب افشاند از دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو زن باهوش آمد بارِدیگر</p></div>
<div class="m2"><p>چو اوّل بار گشت از بیمِ سنجر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو باری ده زهُش آمد بخود باز</p></div>
<div class="m2"><p>سر رشته نکرد او از خرد باز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شهش گفتا اگر می‌ترسی از من</p></div>
<div class="m2"><p>بجان تو ایمنی ای خویش دشمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زنش گفتا که من زین می‌نترسم</p></div>
<div class="m2"><p>ولی این بیت یک شب بود درسم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه شب درسِ خود تکرار کردم</p></div>
<div class="m2"><p>گهی اقرار و گه انکار کردم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از آنجا باز می‌یابم نشانی</p></div>
<div class="m2"><p>که بر من تنگ می‌گردد جهانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدان ماند که یک شب درچنان کار</p></div>
<div class="m2"><p>نهفته بودهٔ از من خبردار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا گر تو بگیری ور برانی</p></div>
<div class="m2"><p>دلت ندهد، دگر بارم بخوانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وگر بکشی مرا در تن درستی</p></div>
<div class="m2"><p>نجاتی باشدم از دستِ هستی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا این ترس چندانی از آنست</p></div>
<div class="m2"><p>که سلطانی که رزّاق جهانست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو او یک یک نفس با من همیشه‌ست</p></div>
<div class="m2"><p>مرا یک یک نفس بنگر چه پیشه‌ست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو حق پیش آورد صد ساله رازم</p></div>
<div class="m2"><p>من آن ساعت چه گویم با چه سازم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو حق می‌بیندت دائم شب و روز</p></div>
<div class="m2"><p>چو شمعی باش خوش می‌خند و می‌سوز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دمی بی شکرش از دل برمیاور</p></div>
<div class="m2"><p>نفس بی یاد غافل بر میاور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگردر شکر کوشی هر چه خواهی</p></div>
<div class="m2"><p>بیابی نقد از جود الهی</p></div></div>