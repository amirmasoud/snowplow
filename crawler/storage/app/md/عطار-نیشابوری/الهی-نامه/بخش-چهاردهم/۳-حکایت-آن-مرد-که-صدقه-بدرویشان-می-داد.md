---
title: >-
    (۳) حکایت آن مرد که صدقه بدرویشان می‬داد
---
# (۳) حکایت آن مرد که صدقه بدرویشان می‬داد

<div class="b" id="bn1"><div class="m1"><p>بزرگی گفت پر شوقست جانم</p></div>
<div class="m2"><p>که شد عمری که من دربندِ آنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از من صدقهٔ برسد بدرویش</p></div>
<div class="m2"><p>که آن صدقه نبیند کس کم و بیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رفتست این دقیقه بر زبانش</p></div>
<div class="m2"><p>چنین گفتست هاتف آن زمانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که تو باید اگر صاحب یقینی</p></div>
<div class="m2"><p>که آن صدقه که بخشیدی نه بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو همچون مُردهٔ بد می‌نمائی</p></div>
<div class="m2"><p>که خود را مُرده و زنده بلائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهی زندگانی گر بدانی</p></div>
<div class="m2"><p>که مردن بهترت زین زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر تو پیش دان و پیش بینی</p></div>
<div class="m2"><p>همه کم کاستی خویش بینی</p></div></div>