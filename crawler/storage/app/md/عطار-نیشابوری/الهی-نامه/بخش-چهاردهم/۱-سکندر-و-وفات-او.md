---
title: >-
    (۱) سکندر و وفات او
---
# (۱) سکندر و وفات او

<div class="b" id="bn1"><div class="m1"><p>سکندر در کتابی دید یک روز</p></div>
<div class="m2"><p>که هست آب حیات آبی دلفروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی کز وی خورد خورشید گردد</p></div>
<div class="m2"><p>بقای عمرِ او جاوید گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر طبلیست با او سرمه دانی</p></div>
<div class="m2"><p>که هر دو هست با او خرده دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنیدم من ز استاد مدرّس </p></div>
<div class="m2"><p>که بود آن سرمه وان طبل آنِ هرمس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر قولنجِ کس سخت اوفتادی</p></div>
<div class="m2"><p>بر آن طبل ار زدی دستی گشادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کز سرمه میلی درکشیدی</p></div>
<div class="m2"><p>ز ماهی تا بساق عرش دیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سکندر را بغایت آرزو خاست</p></div>
<div class="m2"><p>که او را گردد این سه آرزو راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان می‌گشت با خیلی گروهی</p></div>
<div class="m2"><p>که تا روزی رسید آخر بکوهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشانی داشت آنجا کوه بشکافت</p></div>
<div class="m2"><p>پس از ده روز و ده شب خانهٔ یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درش بگشاد و طاقی درمیان بود</p></div>
<div class="m2"><p>در او آن طبل بود و سرمه دان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشید آن سرمه وچشمش چنان شد</p></div>
<div class="m2"><p>که عرش و فرش در حالش عیان شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امیری بود پیشش ایستاده</p></div>
<div class="m2"><p>مگر زد دست بر طبل نهاده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رها شد زو مگر بادی بآواز</p></div>
<div class="m2"><p>بدرّید آن ز خجلت از سر ناز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سکندر گرچه خامُش کرد اما</p></div>
<div class="m2"><p>دریده گشت آن طبل معمّا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شد القصّه برای آبِ حیوان</p></div>
<div class="m2"><p>بهندستان و تاریکی چو کیوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرا با تو کنم این قصّه تکرار</p></div>
<div class="m2"><p>که این قصّه شنیدستی تو صد بار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو شد عاجز در آن تاریکی راه</p></div>
<div class="m2"><p>بمانده هم سپه حیران و هم شاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پدید آمد قوی یکپاره یاقوت</p></div>
<div class="m2"><p>که در وی خیره شد آن مردِ مبهوت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هزاران مور را می‌دید هر سوی</p></div>
<div class="m2"><p>که می‌رفتند هر یک از دگر سوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان پنداشت کان یاقوت پاره</p></div>
<div class="m2"><p>برای عجز اوشد آشکاره</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خطاب آمد که این شمع فروزان</p></div>
<div class="m2"><p>برای خیلِ مورانست سوزان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که تا بر نورِ آن موران گمراه</p></div>
<div class="m2"><p>شوند از جایگاه خویش آگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر نومید گشت آنجا سکندر</p></div>
<div class="m2"><p>که چون شد بهرِ موری سنگ گوهر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز تاریکی برون آمد جگر خون</p></div>
<div class="m2"><p>دلش را هر نفس حالی دگرگون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بجای منزلی دو منزل آمد</p></div>
<div class="m2"><p>که تا آخر بخاک بابل آمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نوشته داشت اسکندر که آنگاه</p></div>
<div class="m2"><p>که وقت مرگ برگیرندش از راه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود از جوشنش بالین نهاده</p></div>
<div class="m2"><p>ز آهن بستری زیرش فتاده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود از زمردان دیوارِ خانه</p></div>
<div class="m2"><p>ز زرّ سرخ آن را آسمانه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببابل آمدش قولنج پیدا</p></div>
<div class="m2"><p>ز درد آن فرود آمد به صحرا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیامد صبرِ چندانی براهش</p></div>
<div class="m2"><p>که کس بر پای کردی بارگاهش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی زیبا زره زیرش گشادند</p></div>
<div class="m2"><p>سرش ز اندوه بر زانو نهادند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در استادند خلقی گردِ او در</p></div>
<div class="m2"><p>سپر بستند بر هم جمله از زر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سکندر خویشتن را چون چنان دید</p></div>
<div class="m2"><p>در آن قولنج مرگ خود عیان دید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بسی بگریست امّا سود کَی داشت</p></div>
<div class="m2"><p>که مرگ بی محابا را ز پی داشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز شاگردانِ افلاطون حکیمی</p></div>
<div class="m2"><p>که ذوالقرنین را بودی ندیمی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نشست و گفت مر شاه جهان را</p></div>
<div class="m2"><p>که آن طبلی که هرمس ساخت آن را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو تو در دستِ نااهلان نهادی</p></div>
<div class="m2"><p>بدست این چنین علّت فتادی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر آن را بکس ننمودئی تو</p></div>
<div class="m2"><p>بدین غم مبتلا کی بودئی تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدان طالع که کرد آن طبل حاضر</p></div>
<div class="m2"><p>کجا آن وقت گردد نیز ظاهر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو قدر آن قدر نشناختی تو</p></div>
<div class="m2"><p>ز چشم خویش دور انداختی تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر آن همچو جان بودی عزیزت</p></div>
<div class="m2"><p>رسیدی شربتی زان چشمه نیزت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ولیکن غم مخور دو حرف بنیوش</p></div>
<div class="m2"><p>که به از آبِ حیوان گر کنی نوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین ملکی و چندینی سیاست</p></div>
<div class="m2"><p>همه موقوف بادیست از نجاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنین ملکی که کردی تو درو زیست</p></div>
<div class="m2"><p>ببین تا این زمان بنیاد بر چیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنین ملکی چرا بنیاد باشد</p></div>
<div class="m2"><p>که گر باشد وگرنه باد باشد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مخور زین غم مرو از دست بیرون</p></div>
<div class="m2"><p>که بادی میرود از پست بیرون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در آن آبِ حیوان را که جُستی</p></div>
<div class="m2"><p>اگرچه این زمان زو دست شُستی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تفکّر کن مده خود را بسی پیچ</p></div>
<div class="m2"><p>که آن علم رزینست و دگر هیچ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر آن علم بنماید بصورت</p></div>
<div class="m2"><p>بوَد آن آبِ حیوان بی کدورت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ترا این علم حق دادست بسیار</p></div>
<div class="m2"><p>چو دانستی بمیر آزاد و هشیار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو بشنید این سخن از اوستاد او</p></div>
<div class="m2"><p>دلش خون شد بشادی جان بداد او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مخور غم ای پسر تو نیز بسیار</p></div>
<div class="m2"><p>که هست آن آب علم و کشفِ اسرار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر بر جان تو تابنده گردد</p></div>
<div class="m2"><p>دلت کَوَنین را بیننده گردد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر تو راهِ علم و عین دانی</p></div>
<div class="m2"><p>ترا آنست آب زندگانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اگر تو راه دان آن نباشی</p></div>
<div class="m2"><p>در آن بینش به جز شیطان نباشی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کرامات تو شیطانی نماید</p></div>
<div class="m2"><p>همه نور تو ظلمانی نماید</p></div></div>