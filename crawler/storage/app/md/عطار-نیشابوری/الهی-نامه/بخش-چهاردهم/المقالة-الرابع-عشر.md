---
title: >-
    المقالة الرابع عشر
---
# المقالة الرابع عشر

<div class="b" id="bn1"><div class="m1"><p>پسر گفتش اگر آب حیاتم</p></div>
<div class="m2"><p>نخواهد داد از مردن نجاتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباید کم ازانم هیچ کاری</p></div>
<div class="m2"><p>که بشناسم که چیست آن آب باری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از عین الحیاتم نیست روزی</p></div>
<div class="m2"><p>بود از علمِ آنم دلفروزی</p></div></div>