---
title: >-
    (۲) حکایت هارون با بهلول
---
# (۲) حکایت هارون با بهلول

<div class="b" id="bn1"><div class="m1"><p>مگر روزی گذر می‌کرد هارون</p></div>
<div class="m2"><p>رسید آنجایگه بهلولِ مجنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان بگشاد کای هارونِ غم خوار</p></div>
<div class="m2"><p>قوی در خشم شد هارون بیکبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپه را گفت کیست این بی سر و پای</p></div>
<div class="m2"><p>که می‌خواند بنامم در چنین جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفتند بهلولست ای شاه</p></div>
<div class="m2"><p>روان شد پیشِ او هارون هم آنگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو گفتا ندانی احترامم</p></div>
<div class="m2"><p>که می‌خوانی تو بی‌حاصل بنامم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌دانی مرا ای مردِ مجنون؟</p></div>
<div class="m2"><p>که بر خاکت بریزم خون هم اکنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوابش داد مرد پُر معانی</p></div>
<div class="m2"><p>که می‌دانم تو این نیکو توانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که در مشرق اگر زالیست باقی</p></div>
<div class="m2"><p>که بر سنگ آیدش پای اتّفاقی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر جائی پُلی باشد شکسته</p></div>
<div class="m2"><p>که گرداند بُزی را پای بسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گر در مغربی از تو نترسند</p></div>
<div class="m2"><p>بترس ای بیخبر کز تو بپرسند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسی بگریست زو هارون بزاری</p></div>
<div class="m2"><p>بدو گفتا اگر تو وام داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگو تا جمله بگزارم بیکبار</p></div>
<div class="m2"><p>جوابش داد بهلول نکوکار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که تو وامی بوامی می‌گزاری</p></div>
<div class="m2"><p>چو مال خویشتن یک جَو نداری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترا گر مال مال مردمانست</p></div>
<div class="m2"><p>که نیست آن تو هر چت این زمانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برَو مال مسلمانان ز پس ده</p></div>
<div class="m2"><p>که گفتت مالِ کس بستان بکس ده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نصیحت خواست از بهلول هارون</p></div>
<div class="m2"><p>بدو گفت آن زمان بهلولِ مجنون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ای استاده در دنیا چنین راست</p></div>
<div class="m2"><p>نشان اهلِ دوزخ در تو پیداست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز رویت محو گردان آن نشانی</p></div>
<div class="m2"><p>وگرنه گفتم و رفتم، تو دانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر ره گفت اگر دوزخ نشینم</p></div>
<div class="m2"><p>کجا شد آن همه اعمالِ دینم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو گفتا ببین هر ماه و هر سال</p></div>
<div class="m2"><p>که همچون اهلِ دوزخ داری احوال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دگر ره گفت اگرچه بوالفضولم</p></div>
<div class="m2"><p>نسَب نقدست باری از رسولم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفتا که چون قرآن شنیدی</p></div>
<div class="m2"><p>فلا اَنسابَ بَینَهُم ندیدی؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر ره گفت هان ای کم بضاعت</p></div>
<div class="m2"><p>امیدم منقطع نیست از شفاعت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفتا که بی اِذن الهی</p></div>
<div class="m2"><p>شفاعت نکند او زین می چه خواهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپه را گفت هارون هین برانید</p></div>
<div class="m2"><p>که او ما را بکُشت و می ندانید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو نه ملکست اینجا و نه مالک</p></div>
<div class="m2"><p>نجات تست اگر گردی تو هالک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو سنگی صد هزاران سال برجای</p></div>
<div class="m2"><p>بمی ماند نمی‌مانی تو بر پای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه خواهی کرد درجائی درنگی</p></div>
<div class="m2"><p>که آنجا بیش ماند از تو سنگی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلا کم گیر چرخ سرنگون را</p></div>
<div class="m2"><p>چه خواهی کرد این دریای خون را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهی خوش طعم دیگ چرب روغن</p></div>
<div class="m2"><p>که از مرگش بوَد زرّین نهنبن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قدم باید بگردون بر نهادن</p></div>
<div class="m2"><p>سر این دیگ پر خون بر نهادن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو پُر خون اوفتاد این دیگ پُر جوش</p></div>
<div class="m2"><p>مزن انگشت در وی سر فرو پوش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شفق خونست و دایم چرخِ گردون</p></div>
<div class="m2"><p>سر بُرّیده می‌گردد در آن خون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهانی خلق بین در هم فتاده</p></div>
<div class="m2"><p>همه از بهرِ زیر خاک زاده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه خاک زمین خون سیاهست</p></div>
<div class="m2"><p>سیاوش وار خلقی بی گناهست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عیان بینی اگر باشی تو با هُش</p></div>
<div class="m2"><p>ز یک یک ذرّه خون صد سیاوش</p></div></div>