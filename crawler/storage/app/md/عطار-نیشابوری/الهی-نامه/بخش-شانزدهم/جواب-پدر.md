---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش که ملک این جهانی</p></div>
<div class="m2"><p>که ملکی است بی پاداشِ فانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای آن چنین بگزیدهٔ تو</p></div>
<div class="m2"><p>که ملک آخرت نشنیدهٔ تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر زان ملک تو آگاه گردی</p></div>
<div class="m2"><p>هم اینجا بر دو عالم شاه گردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگانی که ملک آن ملک دیدند</p></div>
<div class="m2"><p>بیک جَو ملکِ دنیا کی خریدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو می‌دیدند ملک جاودانی</p></div>
<div class="m2"><p>برافشاندند ملک این جهانی</p></div></div>