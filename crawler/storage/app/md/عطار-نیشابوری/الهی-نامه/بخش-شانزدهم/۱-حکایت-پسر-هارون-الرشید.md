---
title: >-
    (۱) حکایت پسر هارون الرشید
---
# (۱) حکایت پسر هارون الرشید

<div class="b" id="bn1"><div class="m1"><p>زُبَیده را ز هارون یک پسر بود</p></div>
<div class="m2"><p>که در خلوت ز عالم بیخبر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون نگذاشتی مادر ز ایوانش</p></div>
<div class="m2"><p>که زیر پرده می‌پرورد چون جانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو قوّت یافت عقل بی قیاسش</p></div>
<div class="m2"><p>به جوش آمد دل حکمت شناسش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بمادر گفت عالم این سرایست</p></div>
<div class="m2"><p>و یا بیرون این بسیار جایست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز این جائی اگر هست آشکاره</p></div>
<div class="m2"><p>بگو تا پیش گیرم من نظاره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل مادر بسوخت الحق برو سخت</p></div>
<div class="m2"><p>بدو گفت ای گرامی و نکوبخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قصر این لحظه بیرونت فرستم</p></div>
<div class="m2"><p>بصحرا و بهامونت فرستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برای او خری مصری بیاراست</p></div>
<div class="m2"><p>غلامی و دو خادم کرد درخواست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برون بُردند تنها آن پسر را</p></div>
<div class="m2"><p>که تا بگشاد بر عالم نظر را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندیده بود عالم آن یگانه</p></div>
<div class="m2"><p>تعجّب کرد از رسم زمانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قضا را دید تابوتی که در راه</p></div>
<div class="m2"><p>گروهی خلق می‌بردند ناگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه در گریه و زاری بمانده</p></div>
<div class="m2"><p>ز گریه در جگرخواری بمانده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پسر پرسید آن ساعت زخادم</p></div>
<div class="m2"><p>که مردن بر همه خلقست لازم؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جوابش داد کان جسمی که جان یافت</p></div>
<div class="m2"><p>ز دست مرگ نتواند امان یافت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نباشد مرگ را عامی و خاصی</p></div>
<div class="m2"><p>کزو ممکن نشد کس را خلاصی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پسر گفتا چنین کاریم در پیش</p></div>
<div class="m2"><p>چرا جانم نترسد سخت بر خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو سنگ از مرگ خواهد گشت چون موم</p></div>
<div class="m2"><p>بباید کرد زود این حال معلوم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو شیر مرگ را بر وی کمین بود</p></div>
<div class="m2"><p>تماشا کردن کودک چنین بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شبانگاهی چو پیش مادر آمد</p></div>
<div class="m2"><p>نشاط و دلخوشی بر وی سرآمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه شب می‌نخفت از هیبت مرگ</p></div>
<div class="m2"><p>شکسته شاخ می‌لرزید چون برگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بوقت صبحدم بگریخت از شهر</p></div>
<div class="m2"><p>بترک لطف گفت از هیبت قهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طلب می‌کرد هارون هر زمانش</p></div>
<div class="m2"><p>نمی‌یافت از کسی نام و نشانش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین گفت آنکه مردی پاک دل بود</p></div>
<div class="m2"><p>که وقتی در سرایم کارِ گِل بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز خانه چون برون رفتم ببازار</p></div>
<div class="m2"><p>یکی مزدور را گشتم طلبکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جوانی را نحیف و زرد دیدم</p></div>
<div class="m2"><p>ز سر تا پاش عین درد دیدم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نهاده تیشه و زنبیل در پیش</p></div>
<div class="m2"><p>شده واله نه با خویش و نه بی‌خویش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو گفتم توانی کار گِل کرد؟</p></div>
<div class="m2"><p>توانم گفت امّا نه بدِل کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p> بدو گفتم مرا شاید تو برخیز</p></div>
<div class="m2"><p>چنین گفت آن جوانمرد بپرهیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که من شنبه کنم کار و دگر نه</p></div>
<div class="m2"><p>مرا خواهی همین روز و اگر نه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو روز شنبهش بودی سر کار</p></div>
<div class="m2"><p>به «سبتی» زین سبب شد نام بردار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببردم آخر او را سوی خانه</p></div>
<div class="m2"><p>دو مَرده کارِ من کرد آن یگانه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شدم در هفتهٔ دیگر به بازار</p></div>
<div class="m2"><p>طلب کردم زهر سوئیش بسیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا گفتند او دیوانه باشد</p></div>
<div class="m2"><p>همیشه در فلان ویرانه باشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شدم او را در آن ویرانه دیدم</p></div>
<div class="m2"><p>ز خلق عالمش بیگانه دیدم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزاری و نزاری اوفتاده</p></div>
<div class="m2"><p>بدام مرگ و خواری اوفتاده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو گفتم که چون بیمار و زاری</p></div>
<div class="m2"><p>ز من آید ترا تیمار دادی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیا درخانهٔ ما آی امروز</p></div>
<div class="m2"><p>که کس را می نه‌بینم بر تودلسوز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اجابت می‌نکرد، القصّه برخاست</p></div>
<div class="m2"><p>برای من بجای آورد درخواست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو آمد در وثاق من چنان شد</p></div>
<div class="m2"><p>کزان سان ناتوان خود کی توان شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جهانی درد مُجرَی گشت در وی</p></div>
<div class="m2"><p>نشان مرگ پیدا گشت بر وی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا گفتا سه حاجت دارم ای دوست</p></div>
<div class="m2"><p>برون می‌باید آمد با تو از پوست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدو گفتم که هر حاجت که خواهی</p></div>
<div class="m2"><p>بخواه ای محرم سرّ الهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بمن گفت آن زمان کم جان برآید</p></div>
<div class="m2"><p>ز قعر چاه این زندان برآید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رسن در گردنم بند و برویم</p></div>
<div class="m2"><p>درافکن پس بکش بر چار سویم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگو کین کار کار اهل دینست</p></div>
<div class="m2"><p>جزای مَن عَصَی الجبار اینست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کسی کو عاصی جبّار باشد</p></div>
<div class="m2"><p>چنین هم سرنگون هم خوار باشد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دوم کهنه گلیمی هست پاکم</p></div>
<div class="m2"><p>کفن زین ساز و با این نه بخاکم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که با این طاعت بسیار کردم</p></div>
<div class="m2"><p>مگر در خاک برخوردار کردم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سیُم این مصحفم بستان و بشناس</p></div>
<div class="m2"><p>که بودست آن عبدالله عباس</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که هارون این حمایل کرده بودی</p></div>
<div class="m2"><p>ز چشم دیگران در پرده بودی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر هارون بر این مصحف ببغداد</p></div>
<div class="m2"><p>بدو گوی آنکه این مصحف بمن داد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سلامت گفت و گفتا گوش میدار</p></div>
<div class="m2"><p>که در غفلت نمیری همچو من زار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که من در غفلت و پندار مردم</p></div>
<div class="m2"><p>ندیدم زندگی مردار مردم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بگوی مادرم را کز دعائی</p></div>
<div class="m2"><p>فراموشم مکن در هیچ جائی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بگفت این و بکرد آهی و جان داد</p></div>
<div class="m2"><p>عفاالله جز چنین جان چون توان داد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدل گفتم که می‌باید رسن خواست</p></div>
<div class="m2"><p>که حالی آن وصیّت راکنم راست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رسن در گردنش کردم بزاری</p></div>
<div class="m2"><p>کشیدم روی بر خاکش بخواری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی هاتف زبان بگشاد ناگاه</p></div>
<div class="m2"><p>که ای از جهلِ محض افتاده از راه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نداری شرم تو از جهلِ بسیار</p></div>
<div class="m2"><p>کنی با دوستان ما چنین کار؟</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>رسن در گردن شخصی میفگن</p></div>
<div class="m2"><p>که چون چنبر نهادش چرخ گردن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چه می‌خواهی ازین غم کُشتهٔ راه</p></div>
<div class="m2"><p>فَلَا تَحزَن فَاِنّا قَد غَفَرنَاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو بشنیدم من آن آوازِ عالی</p></div>
<div class="m2"><p>ز هیبت شد دو دستم سُست حالی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدل گفتم که ای غافل بپرهیز</p></div>
<div class="m2"><p>چه جای این رسن بازیست، برخیز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شدم یارانِ خود را پیش خواندم</p></div>
<div class="m2"><p>سخن از حالِ آن درویش راندم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همه جمع آمدند و با دلی پاک</p></div>
<div class="m2"><p>گلیمش را کفن کردند در خاک</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو فارغ گشتم از کار جوان من</p></div>
<div class="m2"><p>گرفتم مصحف و گشتم روان من</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ستادم بر در هارون سحرگاه</p></div>
<div class="m2"><p>که تا هارون پدیدار آمد از راه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نمودم مصحف و بستد ز من شاد</p></div>
<div class="m2"><p>مرا گفتا چه کس این مصحفت داد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بدو گفتم یکی مزدورکاری</p></div>
<div class="m2"><p>جوانی لاغری زردی نزاری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو گفتم ای عجب مزدورکارش</p></div>
<div class="m2"><p>پدید آمد دو چشم سیل بارش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بسی بگریست تا شد هوش از وی</p></div>
<div class="m2"><p>چو بنشست اندکی آن جوش از وی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مرا گفتا کجاست آن سروِ آزاد</p></div>
<div class="m2"><p>بدو گفتم که سلطان را بقا باد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو این بشنید بخروشید بسیار</p></div>
<div class="m2"><p>برفت از هوش آن داننده هشیار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نه چندان ریخت اشک و کرد فریاد</p></div>
<div class="m2"><p>که آن هرگز کسی را خود بوَد یاد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بگردون می‌رسید آوازِ آهش</p></div>
<div class="m2"><p>نگه می‌داشت از هر سو سپاهش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>پس آنگه گفت آن ساعت که جان داد</p></div>
<div class="m2"><p>چه گفت از من ترا و چه نشان داد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدو گفتم که آن ساعت چنین گفت</p></div>
<div class="m2"><p>که باید با امیرالمؤمنین گفت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کزین شاهی مشو زنهار مغرور</p></div>
<div class="m2"><p>سخن بشنو ازین درویش مزدور</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>در آن کن جهد کز من پند گیری</p></div>
<div class="m2"><p>میان ملک مرداری نمیری</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که گر مردار میری ای یگانه</p></div>
<div class="m2"><p>چو مرداری بمانی جاودانه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدنیا مبتلا تا چند باشی؟</p></div>
<div class="m2"><p>پی دین گیر تا خرسند باشی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>که دنیا پردهٔ جان تو باشد</p></div>
<div class="m2"><p>ولی دین شمع ایمان تو باشد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اگرملک همه عالم بگیری</p></div>
<div class="m2"><p>همه بر تو نشنید چون بمیری</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تو مردی نازکی پرورده در ناز</p></div>
<div class="m2"><p>ز حمّالی خلقی خوی کن باز</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>کنون من گفتم و رفتم تو مپسند</p></div>
<div class="m2"><p>که ننیوشی چنین وقتی چنین پند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ز سر در درد هارون تازه‌تر شد</p></div>
<div class="m2"><p>ز حیرت هر دم از نوعی دگر شد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بآخر با وثاقش برد با خویش</p></div>
<div class="m2"><p>که تا بنشست پیش پرده درویش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زُبَیده در پس آن پرده آمد</p></div>
<div class="m2"><p>که تا پیشش حکایت کرده آمد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چنین گفت او که چون آنجا رسیدم</p></div>
<div class="m2"><p>که در خاکش فکندم می‌کشیدم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>برآمد از پس پرده خروشی</p></div>
<div class="m2"><p>چو دریا زان زنان برخاست جوشی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>زُبَیده گفت ای فریادم از تو</p></div>
<div class="m2"><p>خدا بستاند آخر دادم از تو</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>جگرگوشهٔ مرا در مستمندی</p></div>
<div class="m2"><p>نترسیدی که بر روی او فکندی؟</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خلیفه زاده را نشناختی تو</p></div>
<div class="m2"><p>رسن در گردنش انداختی تو</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دریغا ای غریب و ای جوانم</p></div>
<div class="m2"><p>دریغا نورِ چشم و شمعِ جانم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو بادی عزمِ ره ناگاه کردی</p></div>
<div class="m2"><p>که جان مادر آتشگاه کردی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>دریغا ای لطیف نازنینم</p></div>
<div class="m2"><p>که ماندی همچو گنجی در زمینم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چه گویم، گورش القصّه نشان خواست</p></div>
<div class="m2"><p>بزینت مشهدی کرد آن زمان راست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>خبر گوینده را بسیار زر داد</p></div>
<div class="m2"><p>ولی هارونش از زن بیشتر داد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>توانگر گشت آن مرد خبرگوی</p></div>
<div class="m2"><p>کنون این رفت اگر داری دگرگوی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چه خواهی کرد ملکی را که ناکام</p></div>
<div class="m2"><p>بلای جان تو باشد سرانجام</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>اگر شاهیِ عالم خانه داری</p></div>
<div class="m2"><p>شوی شهماتِ آن خانه بزاری</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چرا در کلبهٔ بنشستهٔ راست</p></div>
<div class="m2"><p>کزو ناکام بر می‌بایدت خاست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چرا معشوقهٔ خواهی که پیوست</p></div>
<div class="m2"><p>غم آن عاقبت گرداندت پست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چرا جمع آوری چیزی بصد عز</p></div>
<div class="m2"><p>که یک جَو زان نخواهی خورد هرگز</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>اگر تو دشمن ملکی پدر باش</p></div>
<div class="m2"><p>وگر در ملک هارونی پسر باش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز حال آن پسر دادم نشانیت</p></div>
<div class="m2"><p>کنون حال پدر گویم زمانیت</p></div></div>