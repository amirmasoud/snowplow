---
title: >-
    (۱۲) حکایت سلطان محمود با پیرزن
---
# (۱۲) حکایت سلطان محمود با پیرزن

<div class="b" id="bn1"><div class="m1"><p>مگر یک روز محمود نکو روی</p></div>
<div class="m2"><p>ز لشکر اوفتاده بود یک سوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بره در پیشش آمد پیرزالی</p></div>
<div class="m2"><p>عصائی چون الف قدّی چو دالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی انبان بگردن برنهاده</p></div>
<div class="m2"><p>که سوی آسیا می‌شد پیاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهش گفتا چو در تو زور و تگ نیست</p></div>
<div class="m2"><p>که در انبان رگست و در تو رگ نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیار انبان چو سر محکم ببستی</p></div>
<div class="m2"><p>به پیش اسبِ من نِه باز رستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهاد آن پیرزن انبانش در پیش</p></div>
<div class="m2"><p>چو بادی شد روان یک رانش از پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پیشی یافت اسب شاه ازان زال</p></div>
<div class="m2"><p>زبان بگشاد وشه را گفت در حال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که گر با من نه اِستی ای شه امروز</p></div>
<div class="m2"><p>نه اِستم با تو من فردا در آن سوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو اَبرَش گرم کردی در دویدن</p></div>
<div class="m2"><p>که در گرد تو می نتوان رسیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر فردا بسی مرکب بتازی</p></div>
<div class="m2"><p>تو هم در گردِ من نرسی چه سازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکن امروز این تعجیل ای شاه</p></div>
<div class="m2"><p>که تا فردا بهم باشیم در راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شه از گفتارِ آن زن خون فشان شد</p></div>
<div class="m2"><p>عنان بر تافت با او هم عنان شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر درس وفا تعلیق داری</p></div>
<div class="m2"><p>چو محمودت دهد توفیق یاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کَرَم اینست و عهد این و وفا این</p></div>
<div class="m2"><p>نکوکاری و تسلیم و رضا این</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر زین نافه هرگز بوی بردی</p></div>
<div class="m2"><p>ز نُه چوگانِ گردون گوی بردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر نه اوفتادی در ندامت</p></div>
<div class="m2"><p>که هرگز برنخیزی تا قیامت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو ای مرد گدا احسان درآموز</p></div>
<div class="m2"><p>گدائی از چینن سلطان درآموز</p></div></div>