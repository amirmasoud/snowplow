---
title: >-
    (۷) حکایت پادشاه و انگشتری
---
# (۷) حکایت پادشاه و انگشتری

<div class="b" id="bn1"><div class="m1"><p>جهان را پادشاهی پاک دین بود</p></div>
<div class="m2"><p>که ملک عالمش زیر نگین بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبودش در همه عالم نظیری</p></div>
<div class="m2"><p>که بودش از همه عالم گزیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سواد ملکش از مه تا بماهی</p></div>
<div class="m2"><p>ز شرقش تا بغربش پادشاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکیمانی که پیش شاه بودند</p></div>
<div class="m2"><p>که اجری خوارهٔ درگاه بودند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گفت ای عجب روزی بایشان</p></div>
<div class="m2"><p>که حالی می‌رود بر من پریشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم را آرزوئی بس عجب خاست</p></div>
<div class="m2"><p>نمی‌دانم که این از چه سبب خاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا سازید یک انگشتری پاک</p></div>
<div class="m2"><p>که هر وقتی که باشم نیک غمناک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو در وی بنگرم دلشاد گردم</p></div>
<div class="m2"><p>ز دست تُرکِ غم آزاد گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر دلشاد گردم نیز از بخت</p></div>
<div class="m2"><p>چو در وی بنگرم غمگین شوم سخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکیمان زو امان جستند یک چند</p></div>
<div class="m2"><p>نشستند آن بزرگان خردمند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسی اندیشه و فکرت بکردند</p></div>
<div class="m2"><p>بسی خونابه حسرت بخوردند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بآخر اتّفاقی جزم کردند</p></div>
<div class="m2"><p>بیک ره برنگینی عزم کردند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که بنگارند بر وی این رقم زود</p></div>
<div class="m2"><p>که آخر بگذرد این نیز هم زود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو ملک این جهان ملکی روندست</p></div>
<div class="m2"><p>بملک آن جهان شد هر که زندست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر آن ملک خواهی این فدا کن</p></div>
<div class="m2"><p>بابراهیمِ ادهم اقتدا کن</p></div></div>