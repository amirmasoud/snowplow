---
title: >-
    (۲) حکایت شیخ و مرغ همای
---
# (۲) حکایت شیخ و مرغ همای

<div class="b" id="bn1"><div class="m1"><p>مگر می‌رفت شیخی کاردیده</p></div>
<div class="m2"><p>بره در دید طاقی برکشیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همائی کرده از کج بر سر او</p></div>
<div class="m2"><p>بگسترده ز هم بال و پر او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان بگشاد و گفت ای مرغ ناساز</p></div>
<div class="m2"><p>تو بی شرمک بدینجا آمدی باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر یک چند بگشائی پری تو</p></div>
<div class="m2"><p>نشینی پس بقصر دیگری تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاید از تو کس را سایه داری</p></div>
<div class="m2"><p>که نا پایندگی سرمایه داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر پایندگی بودی جهان را</p></div>
<div class="m2"><p>هویدائی نبودی عقل و جان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه دنیا سرابی می‌نماید</p></div>
<div class="m2"><p>جهانی ملک خوابی می‌نماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرت در گِل ازان سخت اوفتادست</p></div>
<div class="m2"><p>که در تعبیر خر بخت اوفتادست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خر باشد کسی را بخت اینجا</p></div>
<div class="m2"><p>بلاشک کار باشد سخت اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر غربال پندار خود از آب</p></div>
<div class="m2"><p>برآری عالمی بینی همه خواب</p></div></div>