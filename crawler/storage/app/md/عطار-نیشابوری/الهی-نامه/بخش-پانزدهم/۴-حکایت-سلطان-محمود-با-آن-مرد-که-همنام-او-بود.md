---
title: >-
    (۴) حکایت سلطان محمود با آن مرد که همنام او بود
---
# (۴) حکایت سلطان محمود با آن مرد که همنام او بود

<div class="b" id="bn1"><div class="m1"><p>مگر محمود می‌شد با سپاهی</p></div>
<div class="m2"><p>ز هامون تا بگردون پایگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپه می‌راند هر سوئی شتابان</p></div>
<div class="m2"><p>که تا صیدی بیابد در بیابان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خمیده پشت پیری دید غمناک</p></div>
<div class="m2"><p>برهنه پای و سر با روی پُر خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درمنه می‌کشید و آه می‌کرد</p></div>
<div class="m2"><p>میان خار خود را راه می‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شه آمد پیشش و گفت ای گرامی</p></div>
<div class="m2"><p>زبان بگشای و بر گو تا چه نامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین گفتا که من محمود نامم</p></div>
<div class="m2"><p>چو هم نام تو ام این خود تمامم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهش گفتا که ماندم در شکی من</p></div>
<div class="m2"><p>تو یک محمود باشی و یکی من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو یک محمود و من محمودِ دیگر</p></div>
<div class="m2"><p>کجا باشیم ما هر دو برابر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوابش داد پیر و گفت ای شاه</p></div>
<div class="m2"><p>همی چون هر دو برخیزیم از راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رویم اول دو گز زینجا فروتر</p></div>
<div class="m2"><p>بمحمودی شویم آنگه برابر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برابر گر نیم با تو که خُردم</p></div>
<div class="m2"><p>برابر گردم آن ساعت که مردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو خوش بر تخت رَوکین نیلگون سقف</p></div>
<div class="m2"><p>کند از چوبِ تختت تختهٔ وقف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه خواهی کرد ملکی درجهانی</p></div>
<div class="m2"><p>که نتوانی که خوش باشی زمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنتوانی شدن تنها براهی</p></div>
<div class="m2"><p>نه کارت راست آید بی سپاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه هم بی چاشنی گیری خوری آب</p></div>
<div class="m2"><p>نه شب بی پاسبانی آیدت خواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غم ملکی چرا چندان خوری تو</p></div>
<div class="m2"><p>که نتوانی که در وی نان خوری تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر همچون کیانت تختِ عاجست</p></div>
<div class="m2"><p>وگر برتر ز نوشروانت تاجست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نصیبت زان چنان تاجی و تختی</p></div>
<div class="m2"><p>نخواهد بود الّا خاک لختی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه ملکست این و تو چه پادشائی</p></div>
<div class="m2"><p>که با میر اجل برمی نیائی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر یک گِرده هر روزت تمامست</p></div>
<div class="m2"><p>چو تو دو گرده می‌جوئی حرامست</p></div></div>