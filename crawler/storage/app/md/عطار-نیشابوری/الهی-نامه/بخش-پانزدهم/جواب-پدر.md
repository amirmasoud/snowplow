---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش چرا ملکت بکارست</p></div>
<div class="m2"><p>که گر دستت دهد ناپایدارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین ملکی چنان بِه، هم تو دانی،</p></div>
<div class="m2"><p>که در باقی کنی چون هست فانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر در ملک ظلمی کرده باشی</p></div>
<div class="m2"><p>که تا یک گِرده روزی خورده باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان چون حسرت آبادیست جمله</p></div>
<div class="m2"><p>کفی خاکست یا بادیست جمله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو غِرّه بملک باد و خاکی</p></div>
<div class="m2"><p>بجانی کرده پیوند هلاکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرا آن زندگی با برگ باشد</p></div>
<div class="m2"><p>که انجامش بزاری مرگ باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان پُر نوش داروی الهی</p></div>
<div class="m2"><p>مکُش خود را بزهر پادشاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه روستم را دل بپژمرد</p></div>
<div class="m2"><p>چه سود ازنوش دارو چون پسر مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلب کن ای پسر ملکی دگر را</p></div>
<div class="m2"><p>که سر باید بُرید آنجا پسر را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان را پادشاهانی که بودند</p></div>
<div class="m2"><p>که سر در گنبد گردنده سودند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بملک اندر نبودی پشتشان گرم</p></div>
<div class="m2"><p>مگر بر پشتی آن پارهٔ چرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه در زیرِ چرم آرام کرده</p></div>
<div class="m2"><p>درفش کاویانش نام کرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز ملکی چون نمی‌گیری کناره</p></div>
<div class="m2"><p>که بر پایست از یک چرم پاره؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شاهی از درفش لختِ چرمست</p></div>
<div class="m2"><p>بغایت کفشگر زان پشت گرمست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا ملکی که اصلش چرم باشد</p></div>
<div class="m2"><p>بدان گر فخر آرم شرم باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو سِرّ کارها معلوم گردد</p></div>
<div class="m2"><p>بسا آهن که در دم موم گردد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آن موضع که عقل آنجاست مدهوش</p></div>
<div class="m2"><p>اگر کوهست گردد عِهنِ منفوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو ملک این جهانی بس جَهانست</p></div>
<div class="m2"><p>چو نیکو بنگری ملک آن جهانست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهی آدم که پیگ عشق دریافت</p></div>
<div class="m2"><p>بیک گندم ز ملک خلد سر تافت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر خواهی که یابی ملکِ جاوید</p></div>
<div class="m2"><p>ترا قرصی ز عالم بس چو خورشید</p></div></div>