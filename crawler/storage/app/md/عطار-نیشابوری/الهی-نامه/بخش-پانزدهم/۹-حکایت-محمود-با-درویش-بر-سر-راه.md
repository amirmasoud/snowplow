---
title: >-
    (۹) حکایت محمود با درویش بر سر راه
---
# (۹) حکایت محمود با درویش بر سر راه

<div class="b" id="bn1"><div class="m1"><p>مگر محمود می‌شد با سپاهی</p></div>
<div class="m2"><p>رسیدش پیش درویشی براهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلامی کرد شاه او را دران دشت</p></div>
<div class="m2"><p>علیکی گفت آن درویش و بگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلشکر گفت شاه پاک عنصر</p></div>
<div class="m2"><p>که بینید آن گدا با آن تکبّر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو درویش گفت ار هوشمندی</p></div>
<div class="m2"><p>گدا خود چون توئی بر من چه بندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که در صد شهر وده افزون رسیدم</p></div>
<div class="m2"><p>بهر مسجد گدائی تودیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو جَوجَو نیم جَو بر هر سرائی</p></div>
<div class="m2"><p>نوشتند از پی چون تو گدائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندیدم هیچ بازار و دکانی</p></div>
<div class="m2"><p>که از ظلمت نبود آنجا فغانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون گر بینش چشمت تمامست</p></div>
<div class="m2"><p>ز ما هر دو گدا بنگر کدامست</p></div></div>