---
title: >-
    جواب پدر
---
# جواب پدر

<div class="b" id="bn1"><div class="m1"><p>پدر گفتش زهی شهوت پرستی</p></div>
<div class="m2"><p>که از شهوت پرستی مست مستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مردی که قید فرج باشد</p></div>
<div class="m2"><p>همه نقد وجودش خرج باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی هر زن که او مردانه آمد</p></div>
<div class="m2"><p>ازین شهوت بکل بیگانه آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان کان زن که از شوهر جدا شد</p></div>
<div class="m2"><p>سر مردان درگاه خدا شد</p></div></div>