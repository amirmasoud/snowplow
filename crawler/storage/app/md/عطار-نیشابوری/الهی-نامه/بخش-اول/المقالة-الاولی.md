---
title: >-
    المقالة الاولی
---
# المقالة الاولی

<div class="b" id="bn1"><div class="m1"><p>جهان گر دیده‌ای گم کرده یاری</p></div>
<div class="m2"><p>سراسیمه دلی آشفته کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر داد از کسی کان کس خبر داشت</p></div>
<div class="m2"><p>که وقتی یک خلیفه شش پسر داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه همّت بلند افتاده بودند</p></div>
<div class="m2"><p>ز سر گردن کشی ننهاده بودند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر علمی که باشد در زمانه</p></div>
<div class="m2"><p>همه بودند در هر یک یگانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو هر یک ذوفنون عالمی بود</p></div>
<div class="m2"><p>چو هر یک در دو عالم آدمی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدر بنشاندشان یک روز با هم</p></div>
<div class="m2"><p>که هر یک واقفید از علم عالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلیفه زاده‌اید و پادشاهید</p></div>
<div class="m2"><p>شما هر یک ز عالم می چه خواهید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر صد آرزو دارید و گر یک</p></div>
<div class="m2"><p>مرا فی الجمله برگوئید هر یک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو از هر یک بدانم اعتقادش</p></div>
<div class="m2"><p>بسازم کار هر یک بر مرادش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنطق آورد اول یک پسر راز</p></div>
<div class="m2"><p>که نقلست از بزرگان سرافراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که دارد شاه پریان دختری بکر</p></div>
<div class="m2"><p>که نتوان کرد مثلش ماه را ذکر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زیبایی عقل و لطف جانست</p></div>
<div class="m2"><p>نکو روی زمین و آسمانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر این آرزو یابم تمامت</p></div>
<div class="m2"><p>مرادم بس بود این تا قیامت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی با این چنین صاحب جمالی</p></div>
<div class="m2"><p>ورای این کجا جوید کمالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی کو قربت خورشید دارد</p></div>
<div class="m2"><p>بقرب ذرّه کی امّید دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مراد اینست وگر اینم نباشد</p></div>
<div class="m2"><p>بجز دیوانگی دینم نباشد</p></div></div>