---
title: >-
    (۱۷) حکایت آن مرد که پیش فضل ربیع آمد
---
# (۱۷) حکایت آن مرد که پیش فضل ربیع آمد

<div class="b" id="bn1"><div class="m1"><p>یکی پیری مشوّش روزگاری</p></div>
<div class="m2"><p>بر فضل ربیع آمد بکاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شرم وخجلت و درویشی خویش</p></div>
<div class="m2"><p>ز عجز و پیری و بی‌خویشی خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنانی تیز بود اندر عصایش</p></div>
<div class="m2"><p>نهاد از بیخودی بر پشتِ پایش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان شد خون ز پای فضل حالی</p></div>
<div class="m2"><p>برآمد سرخ و زرد آن صدرّ عالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزد دم تا سخن جمله بیان کرد</p></div>
<div class="m2"><p>بلطفی قصّه زو بستد نشان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو پیر از پیشِ او خوش دل روان شد</p></div>
<div class="m2"><p>ز زخمش فضل آنجا ناتوان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزرگی گفت آخر ای خداوند</p></div>
<div class="m2"><p>چرا بودی بدرد پای خرسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی فرتوت پایت خسته کرده</p></div>
<div class="m2"><p>تو گشته مستمع لب بسته کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو از پای تو آخر خون روان شد</p></div>
<div class="m2"><p>توان گفتن که از پس می‌توان شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین گفت او که ترسیدم که آن پیر</p></div>
<div class="m2"><p>خجل گردد خورد زان کار تشویر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز جرم خویشتن در قهر ماند</p></div>
<div class="m2"><p>ز حاجت خواستن بی بهر ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بار فقر چندان خواری او را</p></div>
<div class="m2"><p>روا نبود چنین سرباری او را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهی مهر و وفا و بُردباری</p></div>
<div class="m2"><p>وفاداری نگر گر چشم داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین فضلی که صد فصل ربیعست</p></div>
<div class="m2"><p>ز فضل حق نه از فضل ربیعست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو مردی ناجوانمردی شب و روز</p></div>
<div class="m2"><p>اگر مردی جوانمردی در آموز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مجوی ای خاک چون آتش بلندی</p></div>
<div class="m2"><p>چو توخاکی مشو آتش بتندی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر آن پیشگه می‌بایدت زود</p></div>
<div class="m2"><p>درین ره خاکِ ره می‌بایدت بود</p></div></div>