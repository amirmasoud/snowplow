---
title: >-
    (۱۶) حکایت پیغامبر و کنیزک حبشی
---
# (۱۶) حکایت پیغامبر و کنیزک حبشی

<div class="b" id="bn1"><div class="m1"><p>چنین نقلست از سلمان که یک روز</p></div>
<div class="m2"><p>نشسته بود صدر عالم افروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی حبشی کنیزک روی چون نیل</p></div>
<div class="m2"><p>درآمد از در مسجد بتعجیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ردای مصطفی بگرفت ناگاه</p></div>
<div class="m2"><p>که بامن نِه زمانی پای در راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهمّی دارم و اکنون توان کرد</p></div>
<div class="m2"><p>ندارم خواجه اینجا چون توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توئی هر بی کسی را یار امروز</p></div>
<div class="m2"><p>منم بی کس فتاده کار امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن می‌گفت و گرم آنگاه می‌رفت</p></div>
<div class="m2"><p>ردایش می‌کشید و راه می‌رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیمبر دم نزد با او روان شد</p></div>
<div class="m2"><p>وزو نستد ردا و همچنان شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خُلق خود نپرسیدش پیمبر</p></div>
<div class="m2"><p>کز اینجا تا کجا آیم بره بر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشی می‌رفت با او چون خموشی</p></div>
<div class="m2"><p>که تا بُردش بر گندم فروشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان بگشاد و گفت ای سیّد امروز</p></div>
<div class="m2"><p>ز گرسنگی دلی دارم همه سوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من اکنون رشته‌ام این پشم اندک</p></div>
<div class="m2"><p>بده وز بهرِ من گندم خر اینک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیمبر بستد و گندم خریدش</p></div>
<div class="m2"><p>برآورد و بدوش اندر کشیدش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببُرد آن تا وثاق آن کنیزک</p></div>
<div class="m2"><p>بقبله کرد پس روی مبارک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که یا رب گر درین کار پرستار</p></div>
<div class="m2"><p>مقصِّر آمدم ناکرده انگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بفضل خود درین کار و درین رای</p></div>
<div class="m2"><p>اگر تقصیر کردم عفو فرمای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برای بندهٔ گندم خریدم</p></div>
<div class="m2"><p>ز خُلق و حلم حمّالی گُزیدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز بس خجلت زبان با حق گشاده</p></div>
<div class="m2"><p>برای عذر بر پای ایستاده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جوانمردا کَرَم بنگر وفا بین</p></div>
<div class="m2"><p>نظر بگشای و خُلقِ مصطفی بین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درین موضع ز جان و تن چه خیزد</p></div>
<div class="m2"><p>زرعنایان تر دامن چه خیزد</p></div></div>