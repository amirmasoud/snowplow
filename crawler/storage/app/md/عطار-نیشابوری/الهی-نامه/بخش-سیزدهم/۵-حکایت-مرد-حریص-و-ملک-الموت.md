---
title: >-
    (۵) حکایت مرد حریص و ملک الموت
---
# (۵) حکایت مرد حریص و ملک الموت

<div class="b" id="bn1"><div class="m1"><p>حریصی در میان مست و هشیار</p></div>
<div class="m2"><p>بسی جان کند و هم کوشید بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بروز و شب زیادت بود کارش</p></div>
<div class="m2"><p>که تا دینار شد سیصد هزارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فزون از صد هزارش بود املاک</p></div>
<div class="m2"><p>فزون از صد هزارش نقد در خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فزون از صد هزار دیگرش بود</p></div>
<div class="m2"><p>که پیش مردمان کشورش بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مال خویش از حد بیش می‌دید</p></div>
<div class="m2"><p>سرای خویش و مال خویش می‌دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدل گفتا که بنشین و همه سال</p></div>
<div class="m2"><p>بخور خوش تا ازان پس چون شود حال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شد این مال خرج خورد و پوشم</p></div>
<div class="m2"><p>اگر باید دگر آنگه بکوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خوش بنشست تا زر می‌خورد خوش</p></div>
<div class="m2"><p>بشادی نفس را می‌پرورد خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو با خود کرد این اندیشه ناگاه</p></div>
<div class="m2"><p>درآمد زود عزرائیل جان خواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو عزرائیل را نزدیک دید او</p></div>
<div class="m2"><p>جهان بر چشمِ خود تاریک دید او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبان بگشاد و زاری کرد آغاز</p></div>
<div class="m2"><p>که عمری صرف کردم در تگ و تاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون بنشسته‌ام تا بهره گیرم</p></div>
<div class="m2"><p>روا داری که من بی‌بهره میرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کجا می‌گشت عزرائیل ازو باز</p></div>
<div class="m2"><p>همی جان برگرفتن کرد آغاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزاری مرد گفتا گر چنانست</p></div>
<div class="m2"><p>که ناچار این زمانت قصدِ جانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون دینار من سیصد هزارست</p></div>
<div class="m2"><p>دهم یک صد هزارت گر بکارست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سه روزم مهل ده بر من ببخشای</p></div>
<div class="m2"><p>وزان پس پیش گیر آنچت بوَد رای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کجا بشنید عزرائیل این راز</p></div>
<div class="m2"><p>کشیدش عاقبت چون شمع در گاز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دگر ره مرد گفتا دادم اقرار</p></div>
<div class="m2"><p>ترا دو صد هزار از نقد دینار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دو روزم مهل ده چون هست این سهل</p></div>
<div class="m2"><p>نداد القصه عزرائیل هم مهل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مگر می‌داد خود سیصد هزاری</p></div>
<div class="m2"><p>که تا مهلش دهد یک روز باری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بزاری گفت بسیارو شنید او</p></div>
<div class="m2"><p>نبودش مهل و مقصودی ندید او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بآخر گفت می‌خواهم امانی</p></div>
<div class="m2"><p>که تا یک حرف بنویسم زمانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امانش داد چندانی که یک حرف</p></div>
<div class="m2"><p>نوشت از خون چشم خود بشنگرف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که هان ای خلقِ عمر و روزگاری</p></div>
<div class="m2"><p>که می‌دادم بها سیصد هزاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که تا یک ساعتی دانم خریدن</p></div>
<div class="m2"><p>نبودم هیچ مقصود از چخیدن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین عمری شما گر می‌توانید</p></div>
<div class="m2"><p>نکو دارید وقدر آن بدانید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که گر از دست شد چون تیر از شست</p></div>
<div class="m2"><p>نه بفروشند و نه هرگز دهد دست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کسی کو در چنین عمری زیان کرد</p></div>
<div class="m2"><p>بغفلت عمرِ شیرین را فشان کرد</p></div></div>