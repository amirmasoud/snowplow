---
title: >-
    (۶) حکایت کشته شدن پسر مرزبان حکیم
---
# (۶) حکایت کشته شدن پسر مرزبان حکیم

<div class="b" id="bn1"><div class="m1"><p>حکیمی بود کامل مرزبان نام</p></div>
<div class="m2"><p>که نوشروان بدو بودیش آرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسر بودش یکی چون آفتابی</p></div>
<div class="m2"><p>بهر علمی دلش را فتح بابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفیهی کُشت ناگه آن پسر را</p></div>
<div class="m2"><p>بخَست از درد جان آن پدر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر آن مرزبان را گفت خاصی</p></div>
<div class="m2"><p>که باید کرد آن سگ را قصاصی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوابی داد او را مرزبان زود</p></div>
<div class="m2"><p>که الحق نیست خون ریزی چنان سود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که من شرکت کنم با او دران کار</p></div>
<div class="m2"><p>بریزم زندهٔ را خون چنان زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفتند پس بستان دِیَت را</p></div>
<div class="m2"><p>نخواهم گفت هرگز آن دیت را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی‌یارم پسر را با بها کرد</p></div>
<div class="m2"><p>که خون خوردن بوَد از خون بها خورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه آن بَد فعل کاری بس نکو کرد</p></div>
<div class="m2"><p>که می‌باید مرا هم کار او کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر از خون پسر خوردن روا نیست</p></div>
<div class="m2"><p>چرا پس خونِ خود خوردن خطا نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خون خویش آنکس خورده باشد</p></div>
<div class="m2"><p>که عمر خویش ضایع کرده باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترا از عمر باقی یک دو هفته‌ست</p></div>
<div class="m2"><p>دگر آن چیز کان به بود رفته‌ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفتم توبه کردی یک دو هفته</p></div>
<div class="m2"><p>چه سازی چارهٔ آن عمرِ رفته</p></div></div>