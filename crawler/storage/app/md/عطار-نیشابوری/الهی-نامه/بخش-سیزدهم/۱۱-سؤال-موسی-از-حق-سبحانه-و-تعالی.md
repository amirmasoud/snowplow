---
title: >-
    (۱۱) سؤال موسی از حق سبحانه و تعالی
---
# (۱۱) سؤال موسی از حق سبحانه و تعالی

<div class="b" id="bn1"><div class="m1"><p>مگر پرسید موسی ازخداوند</p></div>
<div class="m2"><p>که ای دانندهٔ بی‌مثل و مانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خلقان کیست دشمن گیر یا دوست</p></div>
<div class="m2"><p>که هم محتاج و هم درویشِ تو اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا گفت او رهین نعمت ماست</p></div>
<div class="m2"><p>کسی کو سرکشد از قسمت ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی کز قسمت ما در نفیرست</p></div>
<div class="m2"><p>اگر روزست و گر شب در ز حیرست</p></div></div>