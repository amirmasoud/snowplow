---
title: >-
    (۱۴) حکایت شعبی و آن مرد که صعوۀ گرفته بود
---
# (۱۴) حکایت شعبی و آن مرد که صعوۀ گرفته بود

<div class="b" id="bn1"><div class="m1"><p>چنین گفتست شعبی مردِ درگاه</p></div>
<div class="m2"><p>که شخصی صعوهٔ بگرفت در راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو آن صعوه گفت ازمن چه خواهی</p></div>
<div class="m2"><p>وزین ساق و سر و گردن چه خواهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم آزاد گردانی ز بندت</p></div>
<div class="m2"><p>در آموزم سه حرف سودمندت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> یکی در دست تو گویم ولیکن</p></div>
<div class="m2"><p>دُوُم چو بر پَرم بر شاخِ ایمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیُم چون جای تیغِ کوه جویم</p></div>
<div class="m2"><p>ز تیغ کوه آن با تو بگویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بصعوه گفت بر گوی اوّلین راز</p></div>
<div class="m2"><p>زبان بگشاد صعوه کرد آغاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که هرچ از دست شد گر هست جانی</p></div>
<div class="m2"><p>برو حسرت مخور هرگز زمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رها کردش بقول خویش از دست</p></div>
<div class="m2"><p>که تا شد در زمان بر شاخ بنشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوم گفتا محالی گر شنیدی</p></div>
<div class="m2"><p>مکن باور چون آن ظاهر ندیدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفت این و روان شد تا سر کوه</p></div>
<div class="m2"><p>بدو گفت ای ز بدبختی در اندوه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درونم بود دو گوهر قوی حال</p></div>
<div class="m2"><p>که هر یک داشت وزن بیست مثقال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا گر کشتئی گوهر ترا بود</p></div>
<div class="m2"><p>مرا از دست دادی بس خطا بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل آن مرد خونین شد ز غیرت</p></div>
<div class="m2"><p>گرفت انگشت در دندانِ حیرت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بصعوه گفت باری آن سیُم حرف</p></div>
<div class="m2"><p>بگو چون گشت بحر حسرتم ژرف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدو گفتا نداری ذرّهٔ هوش</p></div>
<div class="m2"><p>که شد دو حرفِ پیشینت فراموش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو زان دو حرف نشنیدی یکی راست</p></div>
<div class="m2"><p>سیُم را ازچه باید کرد درخواست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترا گفتم مخور بر رفته حسرت</p></div>
<div class="m2"><p>مکن باور محال ای پاک سیرت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو بر رفته بسی اندوه خوردی</p></div>
<div class="m2"><p>محالی گفتمت تصدیق کردی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دو مثقالم نباشد گوشت امروز</p></div>
<div class="m2"><p>چهل مثقال دو دُرّ شب افروز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چگونه نقد باشد در درونم</p></div>
<div class="m2"><p>ترا دیوانه می‌آید کنونم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفت این و بپرّید از سر کوه</p></div>
<div class="m2"><p>بماند آن مرد در افسوس و اندوه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کسی کو از محال اندیشه دارد</p></div>
<div class="m2"><p>شبانروزی تحیر پیشه دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدم نتوان نهاد آنجا که خواهی</p></div>
<div class="m2"><p>بفرمان رَو بفرمان کن نگاهی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که هر کو نه بامر حق قدم زد</p></div>
<div class="m2"><p>چو شمع از سر برآمد تا که دم زد</p></div></div>