---
title: >-
    (۱۲) پند کسری
---
# (۱۲) پند کسری

<div class="b" id="bn1"><div class="m1"><p>چنین گفتست کسری باربد را</p></div>
<div class="m2"><p>که بی‌اندوه اگر خواهی تو خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسد بیرون کن از دل شاد گشتی</p></div>
<div class="m2"><p>ز حق راضی شو و آزاد گشتی</p></div></div>