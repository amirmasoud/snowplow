---
title: >-
    (۱۹) حکایت مرد مجنون و رعنایان
---
# (۱۹) حکایت مرد مجنون و رعنایان

<div class="b" id="bn1"><div class="m1"><p>بره دربود مجنونی نشسته</p></div>
<div class="m2"><p>که می‌رفتند قومی یک دو رسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر آن قوم دنیاوار بودند</p></div>
<div class="m2"><p>که غرق جامه و دستار بودند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رعنائی و کبر و نحوت و جاه</p></div>
<div class="m2"><p>چو کبکان می‌خرامیدند در راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آن دیوانهٔ بی خان و بی مان</p></div>
<div class="m2"><p>بدید آن خیلِ خود بین را خرامان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشید از ننگ سر در جیب آنگاه</p></div>
<div class="m2"><p>که تا زان غافلان خالی شد آن راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بگذشتند سر بر کرد از جیب</p></div>
<div class="m2"><p>یکی پرسید ازو کای مردِ بی عیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا چون روی رعنایان بدیدی</p></div>
<div class="m2"><p>شدی آشفته و سر درکشیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین گفت او که سر را درکشیدم</p></div>
<div class="m2"><p>ز بس باد بروت اینجا که دیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ترسیدم که برباید مرا باد</p></div>
<div class="m2"><p>چو بگذشتند سر بر کردم آزاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی چون گندِ رعنایان شنیدم</p></div>
<div class="m2"><p>شدم بی طاقت و سر درکشیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو هفت اعضات رعنائی گرفتست</p></div>
<div class="m2"><p>جهانی از تو رسوائی گرفتست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسانی کین صفت از خویش بردند</p></div>
<div class="m2"><p>بدنیا کار عقبی پیش بردند</p></div></div>