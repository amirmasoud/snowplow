---
title: >-
    (۸) حکایت بزرجمهر با انوشیروان
---
# (۸) حکایت بزرجمهر با انوشیروان

<div class="b" id="bn1"><div class="m1"><p>چو از بوزرجمهر افتاد در خشم</p></div>
<div class="m2"><p>دل کسری، کشیدش میل در چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معمایی فرستادند از روم</p></div>
<div class="m2"><p>که گر آنجا کنند این راز معلوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراجش می‌فرستیم واگرنه</p></div>
<div class="m2"><p>جفا بیند ز ما چیزی دگر نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکیمان را بهم بنشاند کسری</p></div>
<div class="m2"><p>کسی زیشان نشد آگاهِ معنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه گفتند این راز سپهرست</p></div>
<div class="m2"><p>چنین کار از پی بوزرجمهرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون از وی کسی نشناسد این راز</p></div>
<div class="m2"><p>بپرسید این معمّا را ازو باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حکیم رانده را نوشیروان خواند</p></div>
<div class="m2"><p>بدان خواری عزیزش همچو جان خواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکایت کرد حالی آن معماش</p></div>
<div class="m2"><p>که جز تو کس نیارد کرد پیداش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حکیمش گفت یک حمّام خواهم</p></div>
<div class="m2"><p>وزان پس ساعتی آرام خواهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنم چون اعتدالی یافت یخ خواه</p></div>
<div class="m2"><p>به یخ بر من نویس این قصّه آنگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که گرچه چشمِ من کورست امّا</p></div>
<div class="m2"><p>بدین حیلت بگویم این معمّا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان کردند القصّه که او گفت</p></div>
<div class="m2"><p>که تا گفت آن معمّا و نکو گفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بغایت شادمان شد زو دل شاه</p></div>
<div class="m2"><p>بدو گفتا که از من حاجتی خواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حکیمش گفت چون این روی دیدی</p></div>
<div class="m2"><p>که کورم کردی ومیلم کشیدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون آن خواهم از تو ای سرافراز</p></div>
<div class="m2"><p>که بس سرگشته‌ام چشمم دهی باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهش گفتا که من این کی توانم</p></div>
<div class="m2"><p>تو خود دانی که من این می‌ندانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حکیمش گفت ای شاه سرافراز</p></div>
<div class="m2"><p>چو نتوانی که چشم من دهی باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مکن تندی ز کس چیزی ستان تو</p></div>
<div class="m2"><p>که گر خواهی توانی دادش آن تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چرا می‌بستدی چیزی که از عز</p></div>
<div class="m2"><p>عوض نتوانی آن را داد هرگز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترا هر یک نفس دُرّی عزیزست</p></div>
<div class="m2"><p>وزین دُرّت گرامی‌تر چه چیزست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مده بر باد این گوهر ببازی</p></div>
<div class="m2"><p>که گر خواهی که بازآری چه سازی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو می‌باید که هر دم پیش آئی</p></div>
<div class="m2"><p>تو هر دم تا بکی با خویش آئی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنفشه چون نهٔ نرگس نبودی</p></div>
<div class="m2"><p>چرا چون این و آن کور و کبودی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه چون رعد بانگی بی‌درنگی</p></div>
<div class="m2"><p>همه چون بُرجِ عقرب کور و لنگی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ترا از تو هزاران پرده در پیش</p></div>
<div class="m2"><p>چگونه ره بری یک ذرّه در خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو بی‌خویشی اگر با خویش آئی</p></div>
<div class="m2"><p>ز خیل پس روان در پیش آئی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نخواهندت بخود هرگز رها کرد</p></div>
<div class="m2"><p>ترا بس عمر می‌باید قضا کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر روزی تو زینجا دور مانی</p></div>
<div class="m2"><p>چرا بیگانه و مهجور مانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یقین می‌دان که چون آن آشنائی</p></div>
<div class="m2"><p>پدید آید نماند این جدائی</p></div></div>