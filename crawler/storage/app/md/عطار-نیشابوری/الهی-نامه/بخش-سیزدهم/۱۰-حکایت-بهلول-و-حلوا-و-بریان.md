---
title: >-
    (۱۰) حکایت بهلول و حلوا و بریان
---
# (۱۰) حکایت بهلول و حلوا و بریان

<div class="b" id="bn1"><div class="m1"><p>چو غالب گشت بر بهلول سوداش</p></div>
<div class="m2"><p>زُ بَیده داد بریانی و حلواش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشست و شاد می‌خورد، آن یکی گفت</p></div>
<div class="m2"><p>که می‌ندهی کسی را، او برآشفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که حق چون این طعامم این زمان داد</p></div>
<div class="m2"><p>چگونه این زمان با او توان داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا هرچ او دهد راضی بدان باش</p></div>
<div class="m2"><p>وگر دستت دهد هم داستان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که هر حکت که از پیشان روانست</p></div>
<div class="m2"><p>تو نشاسی و درخورد تو آنست</p></div></div>