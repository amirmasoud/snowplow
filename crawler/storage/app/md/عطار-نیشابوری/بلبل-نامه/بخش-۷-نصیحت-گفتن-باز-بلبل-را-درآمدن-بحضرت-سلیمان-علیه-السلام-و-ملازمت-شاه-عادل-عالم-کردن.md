---
title: >-
    بخش ۷ - نصیحت گفتن باز بلبل را درآمدن بحضرت سلیمان علیه السلام و ملازمت شاه عادل عالم کردن
---
# بخش ۷ - نصیحت گفتن باز بلبل را درآمدن بحضرت سلیمان علیه السلام و ملازمت شاه عادل عالم کردن

<div class="b" id="bn1"><div class="m1"><p>سپاه روز روشن چون برآمد</p></div>
<div class="m2"><p>قضا را ترک هجران بر سر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بلبل باز گفت ای خفته برخیز</p></div>
<div class="m2"><p>بیا خود را به بال من درآویز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو موری کعبه را خواهد که بیند</p></div>
<div class="m2"><p>فراز شهپر بازان نشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلیمانت همی خواهد به داور</p></div>
<div class="m2"><p>چه داری حجت قاطع بیاور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خواهی گفت با او من چه مرغم</p></div>
<div class="m2"><p>که می‌گردم به عالم فارغ از غم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برنگ و بوی گل مغرور گشتی</p></div>
<div class="m2"><p>ز نزد حضرت شه دور گشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حسن بی بقا دل خوش چرایی</p></div>
<div class="m2"><p>ز امر سروران سرکش چرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا دل بندی اندر بی وفائی</p></div>
<div class="m2"><p>شوی محروم و در خدمت نیائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر دان سر ز درگاه خداوند</p></div>
<div class="m2"><p>که سرگردان بمانی پای در بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر خواهی که گردی در جهان فرد</p></div>
<div class="m2"><p>به گرد کوی صاحب دولتان گرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که از صاحبدلان یا بی عطائی</p></div>
<div class="m2"><p>نیابی هیچ از اینها بی وفائی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن از اهل عقل و فهم بنیوش</p></div>
<div class="m2"><p>اگر داری خبر از دانش و هوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گدائی مفلس و سرگشته حیران</p></div>
<div class="m2"><p>پی روزی گرفت آمد به شروان</p></div></div>