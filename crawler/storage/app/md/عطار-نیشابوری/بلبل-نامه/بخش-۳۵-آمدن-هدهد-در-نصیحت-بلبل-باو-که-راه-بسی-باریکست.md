---
title: >-
    بخش ۳۵ - آمدن هدهد در نصیحت بلبل باو  که راه بسی باریکست
---
# بخش ۳۵ - آمدن هدهد در نصیحت بلبل باو  که راه بسی باریکست

<div class="b" id="bn1"><div class="m1"><p>بیا ای هدهد صاحب هدایت</p></div>
<div class="m2"><p>چه داری تا خبر از هر ولایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قباپوشی ولی دردی نداری</p></div>
<div class="m2"><p>گله داری ولی مردی نداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تن بیرون کن و کن خاک بر سر</p></div>
<div class="m2"><p>قبائی بی بقا تاج مزور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی باشد سزای تاجداری</p></div>
<div class="m2"><p>که باشد در تبارش شهریاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی باشد سزای قرب شاهی</p></div>
<div class="m2"><p>که باشد لائق فر الهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر اهل امل گر تاجدار است</p></div>
<div class="m2"><p>بیندیش آن برای تاجدار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرقع پوشی و تاج مرصع</p></div>
<div class="m2"><p>مرصع نی مناسب با مرقع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طریق تاجداری عقل و دادست</p></div>
<div class="m2"><p>ترا حاصل بدست از جمله بادست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا چون بر سر کوهست خورشید</p></div>
<div class="m2"><p>چه میداری بروز رفته امید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بپرهان بر درخت زندگانی</p></div>
<div class="m2"><p>وگرنه بی هنر اینجا بمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا همت بقدر هستی خویش</p></div>
<div class="m2"><p>مرا همت بقدر از آسمان بیش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمرداری فرود آوردهٔ سر</p></div>
<div class="m2"><p>چرا ننهی ز دانش بر سر افسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسان رنجند ز رنگ و بوی مردار</p></div>
<div class="m2"><p>نگه دارند مشام از گند مردار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من آن مرغم که می‌نالم بگلزار</p></div>
<div class="m2"><p>تو آن مرغی که میخاری سر خار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو کردی بی وفائی با سلیمان</p></div>
<div class="m2"><p>منش هستم دعاگو با دل و جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگر نشنیدهٔ ای مرغ کوچک</p></div>
<div class="m2"><p>خلاف امریا شد نامبارک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو تا در بند گی بی جان نگردی</p></div>
<div class="m2"><p>قبول حضرت سلطان نگردی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا از دور رمزی می‌نمایند</p></div>
<div class="m2"><p>مرا پیوسته درها می‌گشایند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نشینی بر سر پا سر کشیده</p></div>
<div class="m2"><p>سر و پایت برون هر سو بریده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روا دای که رندان خرابات</p></div>
<div class="m2"><p>برند از خون تو سازند طلسمات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ملوک ملک عالم چون سکندر</p></div>
<div class="m2"><p>ز بهر داد دارند تاج بر سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برو از سر بنه این تاج بیداد</p></div>
<div class="m2"><p>که بی دادی دهد هر تاج بر باد</p></div></div>