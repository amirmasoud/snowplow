---
title: >-
    بخش ۲۲ - آمدن مرغان بدیوان و دیدن ایشان بلبل را و از هیبت او خاموش شدن ایشان را
---
# بخش ۲۲ - آمدن مرغان بدیوان و دیدن ایشان بلبل را و از هیبت او خاموش شدن ایشان را

<div class="b" id="bn1"><div class="m1"><p>به دیوان آمدند مرغان چو دیوان</p></div>
<div class="m2"><p>همی کردند پر از آشوب دیوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بلبل را بدیدند لال گشتند</p></div>
<div class="m2"><p>در آن حالت همه از حال گشتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلیمان گفت بلبل را کجائی</p></div>
<div class="m2"><p>چرا در معرض مرغان نیایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا خاموش گشتی ای سخندان</p></div>
<div class="m2"><p>ز لعل خود بر افشان دُرّ و مرجان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان بگشای و شرح حال برگوی</p></div>
<div class="m2"><p>سراسر قصهٔ اقوال برگوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مرغان آمدند اکنون بداور</p></div>
<div class="m2"><p>چه داری حجت قاطع بیاور</p></div></div>