---
title: >-
    بخش ۴ - رفتن باز بطلب بلبل و خواندن او را بسلیمان
---
# بخش ۴ - رفتن باز بطلب بلبل و خواندن او را بسلیمان

<div class="b" id="bn1"><div class="m1"><p>روان شد باز تند و تیز منقار</p></div>
<div class="m2"><p>بخون بلبل زار کم آزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زهر آلوده کرده تیغ و چنگال</p></div>
<div class="m2"><p>به هیبت بازگسترده پر و بال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بساط خدمت سلطان ببوسید</p></div>
<div class="m2"><p>ز سر تا پای خود جوشن بپوشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان مستغرق فرمان شه شد</p></div>
<div class="m2"><p>بجای پا سرش بر خاک ره شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان بندهٔ مقبل همان است</p></div>
<div class="m2"><p>که پیش از کار کردن کاردان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مهتر کار فرمودن ز کهتر</p></div>
<div class="m2"><p>بجان کوشیدن اندر کار مهتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن کهتر که داند حق شناسی</p></div>
<div class="m2"><p>ازو هرگز نیاید ناسپاسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آن کهتر که او عقل و ادب داشت</p></div>
<div class="m2"><p>مدام اندر وفاشوق و طلب داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آن کهتر که با مهتر ستیزد</p></div>
<div class="m2"><p>چنان افتد که هرگز برنخیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی فرمان گرفت آمد به بستان</p></div>
<div class="m2"><p>چو مستان بود بلبل درگلستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوا چون نافهٔ مشگین معطر</p></div>
<div class="m2"><p>چمن چون عالم علوی منور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان خود به عیش گل ببسته</p></div>
<div class="m2"><p>چو بلبل را بدو تقوی شکسته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفای گلستان از بی بقائی</p></div>
<div class="m2"><p>نوای بلبلان از بی نوائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گوشش نالهٔ بلبل خوش آمد</p></div>
<div class="m2"><p>به چشمش رنگ و بوی گل خوش آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به چرخ آورد یک دم باز را عشق</p></div>
<div class="m2"><p>به بست از گفت و گو دم باز را عشق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو باز آمد به خود از بیخودی باز</p></div>
<div class="m2"><p>به خون بلبلان در کار شد باز</p></div></div>