---
title: >-
    بخش ۹ - جواب دادن بلبل باز را و استغنا نمودن او
---
# بخش ۹ - جواب دادن بلبل باز را و استغنا نمودن او

<div class="b" id="bn1"><div class="m1"><p>جوابش داد هشیار سخنگوی</p></div>
<div class="m2"><p>مگو ما را از این معنی بر این روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو ما را سر و سودای کس نیست</p></div>
<div class="m2"><p>ز عشقم یک نفس پروای کس نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو هرگز بر کسی عاشق نبودی</p></div>
<div class="m2"><p>هنوز آتش نهٔ مانند دودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو نادر بی خودی بیخود نمانی</p></div>
<div class="m2"><p>تو قدر عاشقان هرگز ندانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شراب عاشقی آن کس کند نوش</p></div>
<div class="m2"><p>که یاد غیر را سازد فراموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا معذور می‌دار ای خداوند</p></div>
<div class="m2"><p>که عاشق نشنود از عاقلان پند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقام عاشقان بالای عقل است</p></div>
<div class="m2"><p>طریق عاقلی در عشق جهل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلیمان را بگو ای نور یزدان</p></div>
<div class="m2"><p>عنان حکم خود از ما بگردان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا بر ما از آن دست ستم نیست</p></div>
<div class="m2"><p>که بر دیوانه و عاشق قلم نیست</p></div></div>