---
title: >-
    بخش ۲۶ - حکایت
---
# بخش ۲۶ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنیدستم که در عهد گذشته</p></div>
<div class="m2"><p>امیری بود والی عهد گشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی نیک و بد عالم بدیده</p></div>
<div class="m2"><p>ز هر دانا دلی پندی شنیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پسر را گفت تا گردی تو پیروز</p></div>
<div class="m2"><p>اگر دانا دلی پندی بیاموز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خردمندان بهشیاری دهند پند</p></div>
<div class="m2"><p>نگیرد بی خرد پند از خردمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو عاق و ببر فرمان پدر را</p></div>
<div class="m2"><p>پدر هرگز نخواهد بد پسر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسر کو ناخلف باشد پسر نیست</p></div>
<div class="m2"><p>پدر کو هم بدآموزد پدر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بقای نسل را گر زن بخواهی</p></div>
<div class="m2"><p>نگه دارد ترا از هر تباهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قول مصطفی دین در امان گیر</p></div>
<div class="m2"><p>که کاری گر نیاید بی گمان تیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پسر گفت ای پدر پند تو بند است</p></div>
<div class="m2"><p>گزیده پند تو بیرون زچند است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنان دامند و شیطان دام را ساز</p></div>
<div class="m2"><p>مرا در دام شیطانی مینداز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو ایمن باش و با من دل نگهدار</p></div>
<div class="m2"><p>که من هرگز نبندم دل درین کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو شهوت را خرد بنده نگردد</p></div>
<div class="m2"><p>دلم هرگز پراکنده نگردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا پا بر سر خاری درآمد</p></div>
<div class="m2"><p>ازین مشکل ترم کاری درآمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پدر می‌گویدم زن خواه و دل گفت</p></div>
<div class="m2"><p>مشو جفت بلا با زن مشو جفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمی‌دانم که را فرمان برم من</p></div>
<div class="m2"><p>پدر را یا بترک سر کنم من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پدر گفت این صفت از خود مکن دور</p></div>
<div class="m2"><p>مشو تلخ و مشوترش و مکن شور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز سر بیرون کنی بازار و آزار</p></div>
<div class="m2"><p>دل خود از چنین گفتار بازآر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به اول سعی کن در خیر کاری</p></div>
<div class="m2"><p>که آفتها است در تأخیر کاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هم جمع آمدند کردند عروسی</p></div>
<div class="m2"><p>مسلمان و مغ و گبر و مجوسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب اول میان شوهر و زن</p></div>
<div class="m2"><p>نهاد افسار بروی شهوت تن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر عاقل بود زن را چو استر</p></div>
<div class="m2"><p>به نرمی برکند افسار از سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وگر ابله بود زن را چو خرشد</p></div>
<div class="m2"><p>به تن تیر بلا را چون سپر شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو امشب باش تا کم زن نگردی</p></div>
<div class="m2"><p>به بی شوئی بگرد زن نگردی</p></div></div>