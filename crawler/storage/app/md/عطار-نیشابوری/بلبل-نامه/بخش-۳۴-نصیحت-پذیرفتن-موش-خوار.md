---
title: >-
    بخش ۳۴ - نصیحت پذیرفتن موش خوار
---
# بخش ۳۴ - نصیحت پذیرفتن موش خوار

<div class="b" id="bn1"><div class="m1"><p>ز من پندی فرا گیر ای خردمند</p></div>
<div class="m2"><p>عتاب و خشم را بر پای نه بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلاه فاقه را بر فرق سر نه</p></div>
<div class="m2"><p>بدان حرصی که باشد کمترش ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز قهرش دیدهٔ پر فتنه بر دوز</p></div>
<div class="m2"><p>چو باد انش به بی خوابی بیاموز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسلط کن برو صیاد خود را</p></div>
<div class="m2"><p>بجای نان مده پالوده بد را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر او را خوار کردی همچو یوسف</p></div>
<div class="m2"><p>عزیز مصر کردی همچو یوسف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببسته سدهٔ فر سعادت</p></div>
<div class="m2"><p>بیان عالم الغیب و شهادت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشعبد وار زیر حقه دارد</p></div>
<div class="m2"><p>نه چندان مهره کانراکس شمارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر یاری که وقتش اقتضا کرد</p></div>
<div class="m2"><p>بدزدد مهرهٔ عمر زن و مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی گردند پیاپی گردش او</p></div>
<div class="m2"><p>دو چاکر در رهش رومی و هندو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمین سفلیان را آسمان است</p></div>
<div class="m2"><p>سرای علویان را آستان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگوش هوش بشنو این سخن را</p></div>
<div class="m2"><p>فدای این سخن کن جان و تن را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو فرصت هست کاری بیشتر بود</p></div>
<div class="m2"><p>پشیمانی گر آید کی کند سود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چراغ دل ز شمع جان برافروز</p></div>
<div class="m2"><p>اصول علم استادان بیاموز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به جان گر خدمت استاد کردی</p></div>
<div class="m2"><p>ز خدمت برخوری استاد گردی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ولی اندیشهٔ تو آن ندارد</p></div>
<div class="m2"><p>معما گفتن تو جان ندارد</p></div></div>