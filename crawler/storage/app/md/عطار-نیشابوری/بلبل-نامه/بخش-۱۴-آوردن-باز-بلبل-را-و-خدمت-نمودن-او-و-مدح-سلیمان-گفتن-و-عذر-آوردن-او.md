---
title: >-
    بخش ۱۴ - آوردن باز بلبل را و خدمت نمودن او و مدح سلیمان گفتن و عذر آوردن او
---
# بخش ۱۴ - آوردن باز بلبل را و خدمت نمودن او و مدح سلیمان گفتن و عذر آوردن او

<div class="b" id="bn1"><div class="m1"><p>چو باز آمد به درگاه سلیمان</p></div>
<div class="m2"><p>صف اندر صف کشیده جمله مرغان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر خود بر زمین بنهاد بلبل</p></div>
<div class="m2"><p>کمر بسته زبان بگشاد بلبل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاس پادشه کرد و دعا گفت</p></div>
<div class="m2"><p>سلیمان را بسی مدح و ثنا گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آن شاهی که مار و مور و انسان</p></div>
<div class="m2"><p>دد ودام و پری داری به فرمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا زیبد به عالم پادشاهی</p></div>
<div class="m2"><p>که زیر حکم داری مرغ و ماهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباشد از تو بهتر شهریاری</p></div>
<div class="m2"><p>کریمی تاج بخش تخت داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسول پادشاه بی زوالی</p></div>
<div class="m2"><p>به همت برتر از نقص وکمالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز کویت تا گل بی خار روید</p></div>
<div class="m2"><p>چو فراشان صبا خاشاک روید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تراکام و مرادت حاصل آمد</p></div>
<div class="m2"><p>دلت از نور عزت کامل آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توئی مطلوب هر جا طالبی هست</p></div>
<div class="m2"><p>دلت از سر معنی گشته سرمست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن از خدمتت دوری گزیدم</p></div>
<div class="m2"><p>که خود را لایق خدمت ندیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر عمرم دهد یزدان ازین پس</p></div>
<div class="m2"><p>غلام حضرتت باشم از این پس</p></div></div>