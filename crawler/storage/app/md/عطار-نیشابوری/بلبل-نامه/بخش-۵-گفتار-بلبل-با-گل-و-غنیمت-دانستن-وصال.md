---
title: >-
    بخش ۵ - گفتار بلبل با گل و غنیمت دانستن وصال
---
# بخش ۵ - گفتار بلبل با گل و غنیمت دانستن وصال

<div class="b" id="bn1"><div class="m1"><p>به گل بلبل همی گفت ای دل افروز</p></div>
<div class="m2"><p>چراغ مهربانی را برافروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا کامشب شب ناز و نیاز است</p></div>
<div class="m2"><p>چو زلف ماهرویان شب دراز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنیمت دان شبی با یار تا روز</p></div>
<div class="m2"><p>به هم گفتن بسی اسرار جان سوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو یار مهربان چون راز گویند</p></div>
<div class="m2"><p>حکایتهای رفته باز گویند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهشت جاودان جز آن نفس نیست</p></div>
<div class="m2"><p>ولی کس را بدان دم دسترس نیست</p></div></div>