---
title: >-
    بخش ۲۵ - حکایت
---
# بخش ۲۵ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنید ستم من از پیر خردمند</p></div>
<div class="m2"><p>جوانی در مغاک کوه الوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفته گوشهٔ بی توشه و نوش</p></div>
<div class="m2"><p>چو مرد حیدری گشته نمد پوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سیمرغ از پس کوه قناعت</p></div>
<div class="m2"><p>قرین در وحدت ودور از جماعت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ناپاکی خود دل پاک شسته</p></div>
<div class="m2"><p>ز خود برخاسته در خود نشسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولیکن خدمت پیران نکرده</p></div>
<div class="m2"><p>ز استاد خرد سیلی نخورده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخود می‌رفت راه بی نهایت</p></div>
<div class="m2"><p>نباشد پادشاهی بی ولایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببردش خواهرش هر روز نانی</p></div>
<div class="m2"><p>همی کردی به نانی زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خواهر گفت روزی ای مراجان</p></div>
<div class="m2"><p>برو زین بیشتر ما را مرنجان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عنایت کرد با من لطف یزدان</p></div>
<div class="m2"><p>حوالت کرد خدمت را به رضوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی آرد به من حلوا و نانم</p></div>
<div class="m2"><p>روان از مطبخ دارالجنانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جواب پیر بین با خود چه گفتست</p></div>
<div class="m2"><p>مگر دیوش به دام خود گرفته است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پیر وقت گفتند این حکایت</p></div>
<div class="m2"><p>که دانم در شکست و در شکایت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسی با او بکرد ابلیس تلبیس</p></div>
<div class="m2"><p>بکار آمد کنون تلبیس ابلیس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اشارت کرد مرد نیک را پیر</p></div>
<div class="m2"><p>برو آنجا ز سر تا پای او گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگو ای با همه وی از همه فرد</p></div>
<div class="m2"><p>سلامت می‌کند پیرای جوانمرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسی گشتی تو تا گشتی بهشتی</p></div>
<div class="m2"><p>رفیقان را ز یاد خود بهشتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خداوندت بسی برگ و نوا داد</p></div>
<div class="m2"><p>نصیب ما بده ز آنچت خدا داد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به خادم داد یکتا نان و حلوا</p></div>
<div class="m2"><p>برون حلوا درونش پر ز بادا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چومرد آورد پیش پیر ره بین</p></div>
<div class="m2"><p>نجاست بود حلوا نانش سرگین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر آنکس کو ندارد پیر رهبر</p></div>
<div class="m2"><p>بود همراه شیطانش بره در</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر خواهی که با تدبیر گردی</p></div>
<div class="m2"><p>بگرد آسمان پیر گردی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جوانی کو ببوسد پای پیران</p></div>
<div class="m2"><p>به پیری دست بوسندش امیران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به خودره رفتن نادیده جهلست</p></div>
<div class="m2"><p>بره رفتن براه رفته سهلست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درخت بیشه میوه برنیاید</p></div>
<div class="m2"><p>بود رعنا ولی خوردن نشاید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درخت باغبان پرورده را بین</p></div>
<div class="m2"><p>که شکل خوب دارد بار شیرین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تنت قافست و جانت هست سیمرغ</p></div>
<div class="m2"><p>ز سیمرغی تو محتاجی به سی مرغ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حجاب کوه قافت آرد و بس</p></div>
<div class="m2"><p>چو منعت می‌کند یک نیمه شو پس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به جز نامی ز جان نشنیدهٔ تو</p></div>
<div class="m2"><p>وجود جان خود تن دیدهٔ تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه عالم پر از آثار جان است</p></div>
<div class="m2"><p>ولی جان از همه عالم نهانست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو سیمرغی ولیکن در حجابی</p></div>
<div class="m2"><p>تو خورشیدی ولیکن در نقابی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز کوه قاف جسمانی گذر کن</p></div>
<div class="m2"><p>بدار الملک روحانی سفر کن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو مرغ آشیان آسمانی</p></div>
<div class="m2"><p>چو بازان مانده دور از آشیانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو زاغان بر سر مُردار مردی</p></div>
<div class="m2"><p>ز صافی گشته خرسندی به دردی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو بازان باز کن یک دم پر و بال</p></div>
<div class="m2"><p>برون پر زین قفس وین دام آمال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو بازان ترک دام و دانه کردی</p></div>
<div class="m2"><p>قرین دست او شاهانه کردی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به پری بر فلک زین تودهٔ خاک</p></div>
<div class="m2"><p>همی گردی تو با مرغان در افلاک</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وگرنه هر زمان بی بال و بی پر</p></div>
<div class="m2"><p>چو مرغ هر دری گردی به هر در</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گهی در آب گردی همچو ماهی</p></div>
<div class="m2"><p>گهی چون آب باشی در تباهی</p></div></div>