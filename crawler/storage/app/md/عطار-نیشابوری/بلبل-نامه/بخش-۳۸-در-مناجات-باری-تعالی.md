---
title: >-
    بخش ۳۸ - در مناجات باری تعالی
---
# بخش ۳۸ - در مناجات باری تعالی

<div class="b" id="bn1"><div class="m1"><p>خداوندا توئی دانای عالم</p></div>
<div class="m2"><p>ز عالم برتری و از جان عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه گیتی بود نی ابلیس و آدم</p></div>
<div class="m2"><p>نه عالم بود و نی ذرات عالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آن پروردگار کردگاری</p></div>
<div class="m2"><p>که بی حبر و قلم صورت نگاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست خود گل آدم سرشتی</p></div>
<div class="m2"><p>به سر بر سرگذشت ما نوشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکیوان برکشی آن را که خواهی</p></div>
<div class="m2"><p>بخذلان درکشی آن راکه خواهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گناهم گر زماهی تا بماه است</p></div>
<div class="m2"><p>ولیکن رحمتت بیش از گناه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بخشی جرم عطار ای خداوند</p></div>
<div class="m2"><p>نداری جان اودر غفلت و بند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکیمی و علیمی و قدیمی</p></div>
<div class="m2"><p>غفوری و شکوری و حلیمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیامرزی برحمت جمله عالم</p></div>
<div class="m2"><p>که حی وغافر الذنبی و حاکم</p></div></div>