---
title: >-
    بخش ۳۳ - مجادله کردن بلبل با موش خوار و جواب او
---
# بخش ۳۳ - مجادله کردن بلبل با موش خوار و جواب او

<div class="b" id="bn1"><div class="m1"><p>بیا ای مرغ نابالغ کجائی</p></div>
<div class="m2"><p>ز عمر نازنین غافل چرائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریغا برگ عمرت رفت بر باد</p></div>
<div class="m2"><p>دمی ناکرده خود را از جهان شاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پرت بدی یعنی که دانش</p></div>
<div class="m2"><p>اگر بالت بدی یعنی که بینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپری تا درخت جاودانی</p></div>
<div class="m2"><p>وگرنه تا ابد اینجا بمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوق آشیان ای مرغ افلاک</p></div>
<div class="m2"><p>شدی افتان و خیزان بر سر خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن سستی که دوران سخت تند است</p></div>
<div class="m2"><p>ز پیران کار طفلان ناپسند است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزرگی و ولی آزار خواری</p></div>
<div class="m2"><p>کم آزادی ولی مردار خواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشام آکنده از گند مردار</p></div>
<div class="m2"><p>چو زاغ وسگ شوی برگند مردار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن با زاغ و با سگ هم نشینی</p></div>
<div class="m2"><p>چو خواهی گلشن سیمرغ بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو هشیاری دل چون بارداری</p></div>
<div class="m2"><p>تو از مردار خوردن دان که خواری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بمرداری فرود آوردهٔ سر</p></div>
<div class="m2"><p>چرا تازی بدانش بر سر افسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرا عاشق نباشی تا بباشی</p></div>
<div class="m2"><p>برون از زاهدان رومی خراشی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو مستی باش تا هشیار گردی</p></div>
<div class="m2"><p>ز عمر خویشتن بیزار گردی</p></div></div>