---
title: >-
    بخش ۳۲ - نصیحت بلبل طاوس را به قطع کردن زینت
---
# بخش ۳۲ - نصیحت بلبل طاوس را به قطع کردن زینت

<div class="b" id="bn1"><div class="m1"><p>برو طاوس شهوت را ببر سر</p></div>
<div class="m2"><p>که بوی آرزویت می‌برد سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز رنگین خانهٔ شهوت بپرهیز</p></div>
<div class="m2"><p>ز بند آرزوی خویش برخیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رنگ شهوت بی رنگ گردد</p></div>
<div class="m2"><p>همه عالم به چشمت تنگ گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درون خانهٔ جانت سیاه است</p></div>
<div class="m2"><p>چه سود ار بر سرت زرین کلاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رنگ و زینت دنیا چو طاوس</p></div>
<div class="m2"><p>همی پوشی سیاهی را بناموس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن شادی اگر کارت برآید</p></div>
<div class="m2"><p>که روز نیک و بد روزی سرآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نماند شادی و غم جاودانی</p></div>
<div class="m2"><p>به نیک و بد سرآید زندگانی</p></div></div>