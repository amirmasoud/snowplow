---
title: >-
    بخش ۱۶ - آوردن باد صبا بلبل را بنزد گل و وصال ایشان باهم
---
# بخش ۱۶ - آوردن باد صبا بلبل را بنزد گل و وصال ایشان باهم

<div class="b" id="bn1"><div class="m1"><p>هر دو با هم آمدند تا گلستان</p></div>
<div class="m2"><p>رفت و او را برد نزد دلستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جمال گل بدید آن مستمند</p></div>
<div class="m2"><p>از زبان خویشتن برداشت بند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مدیح گل بصوت دل ربا</p></div>
<div class="m2"><p>داستانی خواند در پیش صبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان ناله و زاری گذار</p></div>
<div class="m2"><p>گفت دورم بعد از این از خود مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل بچشم مرحمت در وی نگاه</p></div>
<div class="m2"><p>کرد و گفت ای مستمند پرگناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی را بر سرم بفروختی</p></div>
<div class="m2"><p>این چنین دستان ز که آموختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاجزا از گلستان آوارگی</p></div>
<div class="m2"><p>میکنی دیگر مکن بیچارگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز و شب در بزم ما میباش شاد</p></div>
<div class="m2"><p>باده مینوش و مده خود را بباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در وصال یار محرم باش خوش</p></div>
<div class="m2"><p>بامیی صافی تو همدم باش خوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر زمان در وصل یار گلعذار</p></div>
<div class="m2"><p>باش دور از آفت رنج وغبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در جمال گل نظر بازی مکن</p></div>
<div class="m2"><p>بر دل و بر جان خود بازی مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باغبان را چون ز بلبل شد خبر</p></div>
<div class="m2"><p>در گلستان رفت آن شوریده سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز و شب با گل همی بازد هوس</p></div>
<div class="m2"><p>با صبا و گل شده است او همنفس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باغبان را آتشی در جان فتاد</p></div>
<div class="m2"><p>پیش گلزار آمد و کین در نهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صبحگاهی بد که آمد سوی باغ</p></div>
<div class="m2"><p>دل ز دست بلبل مسکین بداغ</p></div></div>