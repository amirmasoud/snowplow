---
title: >-
    بخش ۶ - بردن صبا نامۀ گل به پیش بلبل و نیاز بلبل بحضرت گل
---
# بخش ۶ - بردن صبا نامۀ گل به پیش بلبل و نیاز بلبل بحضرت گل

<div class="b" id="bn1"><div class="m1"><p>چون صبا بشنید آن گفتار او</p></div>
<div class="m2"><p>کرد تحسین بر چنان اشعار او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای گل راست گفتی این سخن</p></div>
<div class="m2"><p>هست گفتار تو چون در عدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد منور از تو باغ و بوستان</p></div>
<div class="m2"><p>بی‌جمال تو مبادا گلستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پارهٔ زر در دهان گل نهاد</p></div>
<div class="m2"><p>لاله آمد پیش و در پایش فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لؤلؤ افشان کرد بر فرقش سحاب</p></div>
<div class="m2"><p>کین غزل خوش گفتی ای در خوشاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بگفت این بیتها را در صبوح</p></div>
<div class="m2"><p>با صبا گفتا مرا در تن چو روح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این غزل را نزد آن دیوانه بر</p></div>
<div class="m2"><p>که تو از عشق جمالم درگذر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نفرمایم ریاحین را به تیغ</p></div>
<div class="m2"><p>سر ببرند از تو ایشان بی دریغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همه شور و شر و غوغات چیست</p></div>
<div class="m2"><p>وین همه فریاد تو از بهر کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من ز تو بیزارم و آواز تو</p></div>
<div class="m2"><p>من نخواهم شد دمی همراز تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پادشاهی نیستی یا سروری</p></div>
<div class="m2"><p>خواجهٔ یا مال و ملک و زیوری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو گدائی عشق باشه باختن</p></div>
<div class="m2"><p>جوز بر گنبد بود انداختن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لقمهٔ خود تا نماند در گلوت</p></div>
<div class="m2"><p>ور نه آید سنگ خذلان برسبوت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت بسیاری از اینها با صبا</p></div>
<div class="m2"><p>چون رسی پیشش بگو این ماجرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت فرمان ترا من چاکرم</p></div>
<div class="m2"><p>هرچه گوئی جمله پیش او برم</p></div></div>