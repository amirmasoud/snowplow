---
title: >-
    بخش ۲۰ - فصل
---
# بخش ۲۰ - فصل

<div class="n" id="bn1"><p>و چون مرید خدمت درویشان کند خاطر درویشان، رسول ایشان بود بدو، باید کی مرید مخالفت آنچه بر باطن او درآید از حکم خلوص در خدمت نکند و بذل وسع و طاقت بجای آرد.</p></div>