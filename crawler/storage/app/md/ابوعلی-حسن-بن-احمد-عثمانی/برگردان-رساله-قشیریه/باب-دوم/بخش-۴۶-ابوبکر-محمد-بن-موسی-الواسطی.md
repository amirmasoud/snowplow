---
title: >-
    بخش ۴۶ - ابوبکر محمّد بن موسی الواسطی
---
# بخش ۴۶ - ابوبکر محمّد بن موسی الواسطی

<div class="n" id="bn1"><p>و از ایشان بود ابوبکر محمّدبن موسی الواسطی، باصل خراسانی بود و از فَرْغانه، صحبت جنید کرده بود و آن نوری، عالمی بزرگوار بود و بمرو نشستی و وفاة وی آنجا بود پس از سیصد و بیست.</p></div>
<div class="n" id="bn2"><p>واسطی گوید خوف و رجا دو ماهار اند کی از بی ادبی باز میدارند.</p></div>
<div class="n" id="bn3"><p>هم او گوید عوض طمع داشتن بر طاعت از فراموش کردن فضل بود.</p></div>
<div class="n" id="bn4"><p>واسطی گوید هر وقت که خدای حواری بنده خواهد او را اندرین جیفگان اندازد یعنی صحبت کودکان.</p></div>
<div class="n" id="bn5"><p>محمّدبن عبدالعزیز المَرْوَزی گوید که واسطی گفت بی ادبی خویش را اخلاص نام کرده اند و شَرَه را انبساط و دون همّتی را جلدی نام کرده اند، همه از راه برگشتند و بر راه مذموم همی روند و زندگانی اندر مشاهدة ایشان ناخوش بود و نقصان روح بود، اگر سخن گویند بخشم گویند و اگر خطاب کنند بکبر بود، نفس ایشان همی خبر دهد از ضمیر ایشان و شَرَه ایشان اندر اکل منادی همی کند از آنچه در اسرار ایشان است، قاتَلَهُمُ اللّهُ اَنّی یُْؤفَکونَ. و این آیة تفسیر کرده اند که مراد بدین لعنة است.</p></div>
<div class="n" id="bn6"><p>استاد ابوعلی گوید در مرو از پیری شنیدم که واسطی بدر دکان من بگذشت روز آدینه بود و بجامع می شدم، شِراک نعلین وی بگسست گفتم ایّهاالشیخ دستوری باشد تا نیک باز کنم نعلین تو، گفت بکن و نیکو باز کردم گفت دانی که چرا بگسست این شراک گفتم تا شیخ بگوید گفت زیرا که امروز غسل نکرده ام گفتم که گرمابه هست اینجا در آنجا شو گفت شوم، بگرمابه بردم ویرا تا غسل بکرد.</p></div>