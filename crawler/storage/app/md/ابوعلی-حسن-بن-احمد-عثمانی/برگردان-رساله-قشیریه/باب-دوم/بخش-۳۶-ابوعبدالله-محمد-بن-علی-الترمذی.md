---
title: >-
    بخش ۳۶ - ابوعبداللّه محمّد بن علیّ الترمذی
---
# بخش ۳۶ - ابوعبداللّه محمّد بن علیّ الترمذی

<div class="n" id="bn1"><p>و از ایشان بود ابوعبداللّه محمّدبن علیّ الترمذی و از بزرگان و پیران بود و ویرا تصنیفها است اندر علم این قوم، صحبت ابوتراب نخشبی و احمد خضرویه کرده بود و ازان ابن جّلا و پیران دیگر.</p></div>
<div class="n" id="bn2"><p>ویرا پرسیدند که صفت خلق چیست گفت عجزی آشکارا و دعویی بزرگ.</p></div>
<div class="n" id="bn3"><p>محمّدبن علی گفت که یک حرف تصنیف نکردم بتدبیر، و نه نیز تا گویند این تصنیف وی است ولیکن چون وقت بر من تنگ شدی بدان تسلّی بودی مرا.</p></div>
<div class="n" id="bn4"><p>و ازین طایفه بود ابوبکر محمدبن عمرالورّاق الترمذی ببلخ مقیم بود و صحبت احمد خضرویه و پیران دیگر کرده بود و ویرا اندر ریاضت تصنیفهاست.</p></div>
<div class="n" id="bn5"><p>محمّدبن محمّدالبلخی گوید از ابوبکر ورّاق شنیدم که گفت هر که راضی بود از اندامهای خویش بشهوة، اندر دلش درخت نومیدی روید.</p></div>
<div class="n" id="bn6"><p>ابوبکر بلخی گوید کی ابوبکر ورّاق گفت اگر طمع را پرسند که پدرت کیست گوید شک اندر مقدور و اگر گویند پیشۀ تو چیست گوید ذُلّ و اگر گویند غایت تو چیست گوید حرمان.</p></div>
<div class="n" id="bn7"><p>ابوبکر ورّاق شاگردان خویش را از سفر بازداشتی گفتی کلید همه برکتها صبرست اندر موضع ارادة تا آن گاه که ارادت تو درست شود چون ارادت درست شد برکتها بر تو گشاده گشت.</p></div>