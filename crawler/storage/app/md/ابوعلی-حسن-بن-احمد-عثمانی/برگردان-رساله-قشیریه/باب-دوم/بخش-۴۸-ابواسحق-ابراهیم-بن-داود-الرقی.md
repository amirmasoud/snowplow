---
title: >-
    بخش ۴۸ - ابواسحق ابراهیم بن داود الرَقَی
---
# بخش ۴۸ - ابواسحق ابراهیم بن داود الرَقَی

<div class="n" id="bn1"><p>و ازیشان بود ابواسحق ابراهیم بن داود الرَقَی از پیران بزرگ بود بشام، از اقران جُنَیْد و ابن الجَلّا بود و عمر وی تا سیصد و بیست و شش سال بکشید. و ابراهیم رَقّی گوید معرفت اثبات حق بود دور بکرده از هرچه وهم به وی رسد.</p></div>
<div class="n" id="bn2"><p>هم او گوید قدرت آشکار است و چشمها گشاد است ولیکن دیدار ضعیف است.</p></div>
<div class="n" id="bn3"><p>ابراهیم رَقّی گوید کی ضعیف ترین خلق آنست که عاجز بود از دست بداشتن شهوات و قوی ترین آن بود که قادر بود بر ترک آن.</p></div>
<div class="n" id="bn4"><p>و گوید نشان دوستی خدای بر گزیدن طاعت وی است و متابعت رسول وی صَلَّی اللّهُ عَلَیْهِ وَسَلَّمَ.</p></div>