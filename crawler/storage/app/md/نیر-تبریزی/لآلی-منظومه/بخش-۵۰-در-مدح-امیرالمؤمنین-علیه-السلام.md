---
title: >-
    بخش ۵۰ - در مدح امیرالمؤمنین علیه السلام
---
# بخش ۵۰ - در مدح امیرالمؤمنین علیه السلام

<div class="b" id="bn1"><div class="m1"><p>سحّ طرفی الدّموع حتی تخلی</p></div>
<div class="m2"><p>و فوادی من الجوی لا یسلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من یودی لصیر عنی سلاماً</p></div>
<div class="m2"><p>ان قلباً@ حواه منه تخلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا نذیر المشیب اغدرت اما</p></div>
<div class="m2"><p>کان یجدی الانذار فیه ضلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قل لطرف لم یخط فی السحر سهماً</p></div>
<div class="m2"><p>قر عیناً فقد هویت المعلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لا تصدّن طرف عینک عنی</p></div>
<div class="m2"><p>هود الله قاتلی لیس الا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سل بین الجفون منه سیوف</p></div>
<div class="m2"><p>و دم الناظرین فی البین طلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اعذرانی فی تاثر لا یراعی</p></div>
<div class="m2"><p>من زمام و لا یراقب و الا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لا یغرنک ابتام لماه</p></div>
<div class="m2"><p>علّه طارق یری الجدّ هزلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فاقض یا ذو القصاص ما انت قاص</p></div>
<div class="m2"><p>قد ملکت الرقاب عقداً و حلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاد من ذاق ما بفیک لیبلی</p></div>
<div class="m2"><p>ان روح بن مریم فیک حلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عن لی مدح من ثوی بالعزبین</p></div>
<div class="m2"><p>فان النشید من فیک اهلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حبذا وقفه بنهر المعلّی</p></div>
<div class="m2"><p>و نجوم من افقه تتجلّی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و عهود خلت بارض الغربین</p></div>
<div class="m2"><p>سقتها الحیاء و بلا و طلّا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لست انسی بها معرّس انس</p></div>
<div class="m2"><p>جمع الله للمنی فیه شملّا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>و جناناً حوت قنادیل یاقوت</p></div>
<div class="m2"><p>علی قبه الزبرجد تدلی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و عیوناً کانها نحر عین</p></div>
<div class="m2"><p>بعقود من اللآلی تحلّی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و طیوراً علی القصون نغبین</p></div>
<div class="m2"><p>لحون الزبور فصلاً ففصلا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>و ظباء یطقن حول حمامها</p></div>
<div class="m2"><p>لا کظبی الفلاء عطفاً و دلّا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هذه انما و لیکم الله</p></div>
<div class="m2"><p>نناوی به نهاراً و لیلا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بابی مصدر الوجود و لو لاه</p></div>
<div class="m2"><p>لعادت ام القوابل نکلی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صوره الزعیه من رآها</p></div>
<div class="m2"><p>کبرّ الله ذالعلی و اهلاً</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ذاک نور الله الذی خرموسی</p></div>
<div class="m2"><p>صعقاً من سناه لما تجلّی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>و کتاب الله الّذی نفخات</p></div>
<div class="m2"><p>القدس آیا علی الناس تتلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ضل من قال با التمثل فی الله</p></div>
<div class="m2"><p>ولو انّه لما کان الاّ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عرّفوه بکل نعت بدیع</p></div>
<div class="m2"><p>من معاینه و المعرف اجلی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کم لمن رام ان یصیب مداه</p></div>
<div class="m2"><p>قلت مهلا ابعدت مرماک مهلا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جل وجه الله المیهمن عن نعت</p></div>
<div class="m2"><p>سوی من براه عزّ و جلا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عیلم تستقی جد اول جدواه</p></div>
<div class="m2"><p>صودای النفوس علا و نهلا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کم له السماء آیات نصّ</p></div>
<div class="m2"><p>باهرات کالشمس بل هی اجلی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فسل النجم اذنهادی الی الارض</p></div>
<div class="m2"><p>الی بیت من هوی و ادلاّ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>و سل الشمس من اقام قناها</p></div>
<div class="m2"><p>بعد ما کورت فقام و صلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>و لمن سلمت عذاه دعاها</p></div>
<div class="m2"><p>فی حضور من الجماعه قبلا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یا لها من مناقب عی عنها</p></div>
<div class="m2"><p>یعملات النهار و اللیل ثقلا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جل نفس الرّسول عن ان تسامی</p></div>
<div class="m2"><p>ضل قوم بغو لمثلک مثلا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اوسئلت البیت المحرم عن اول</p></div>
<div class="m2"><p>من صدق الرسول وصلی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لتجیبک الحصا انّه هو</p></div>
<div class="m2"><p>و علوج الحجاز یدعون بغلا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این ماحی الاصنام من عابدیها</p></div>
<div class="m2"><p>تعس قوم قاست علی الشمس ظلاّ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یاتری این کان شیخا قریش</p></div>
<div class="m2"><p>یوم نادی جبرئیل لا سیف الا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رب امر لا یحسن الکشف و عنه</p></div>
<div class="m2"><p>سعدد عنی عن ذکر سعدی و لیلا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کم باحد و خیبر و حنین</p></div>
<div class="m2"><p>هنوات او فصلت لا ملاّ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من محی ظلمه الضلال بیدر</p></div>
<div class="m2"><p>و عتیق تحت العریش استضلا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قدماه حتی اقسام قنا</p></div>
<div class="m2"><p>الاسلام فاستاخره لما استقلا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سر حنانیک فی البلاد و باحث</p></div>
<div class="m2"><p>عن بطون الکرام جیلا فجیلا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فانظرن هل و تری لتیم بن مر</p></div>
<div class="m2"><p>اوعدی یا سعد فیها محلا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>لا و من شق جانب البیت حتی</p></div>
<div class="m2"><p>دخلت فیه امه و هی حبلی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فتخلّت عن اسجع هاشمی</p></div>
<div class="m2"><p>بورکت حاملا و بورکت حلا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>و سمی غارب النبی فخی</p></div>
<div class="m2"><p>عنه اصنامهم و حسبک نبلا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>لو امیدت باهلها الارض حتی</p></div>
<div class="m2"><p>لا یری آثر لادم نسلا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ولد تیم بن مره و عدی</p></div>
<div class="m2"><p>لا یکونان للخلافته اهلاً</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>لیت شعری اکان فیم بنی</p></div>
<div class="m2"><p>بعد حتی یکون بالناس اولی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ام اجاد صلحاً بغیر ارتضاء</p></div>
<div class="m2"><p>من ذویه اولی لهم ثم اولی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ام باجماع امته لیس فیها</p></div>
<div class="m2"><p>قول اهدی الوری الی الحق سبلاً</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اقضاء بلا حضور الخصمین</p></div>
<div class="m2"><p>قضی الله فی ذوی الجور عدلا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فلیجیز و افعال امت موسی</p></div>
<div class="m2"><p>حین ما قلّدو الا لوهیته عجلاً</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>حیث کاوا اشد و کنا و اقوی</p></div>
<div class="m2"><p>عده منهم و اعذر قولا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>لا و حق النبی اما امام</p></div>
<div class="m2"><p>امر الله بعده ان یولی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>او جهود به و قول بان</p></div>
<div class="m2"><p>الله خلی سبیلها لتصلاّ</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اثر الله ما لک الملک عن</p></div>
<div class="m2"><p>تدبیره للعبید عی و ملاّ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فصفا ملکه لرعیان منیب</p></div>
<div class="m2"><p>کسوام الهیام بهماً و جهلاً</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فعدو غب رعیهم فی المراعی</p></div>
<div class="m2"><p>زعمائ الامور عقد وحلاّ</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ام رسول الا آله ضیع دیناً</p></div>
<div class="m2"><p>طل فیه الدمآء حتی تعلی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اذ تولی و لم یخلف زعیماً</p></div>
<div class="m2"><p>یتحامی حریمه ان یحلاّ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>لا و حق الاسلام لاذی و لاذا</p></div>
<div class="m2"><p>کذب العادلون حاشا و کلا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سعد سرنحو طیبته و ائت قبراً</p></div>
<div class="m2"><p>خضعت دونه الملائک ذلا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>قبر خیر الوری و اکرم من</p></div>
<div class="m2"><p>داس تراب الغبراء حزناً و سهلاً</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زر وطف حوله تجد فیه انوار</p></div>
<div class="m2"><p>هدی من قبابه تتجلی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ثم صح صحیه الصریخ و قل یا</p></div>
<div class="m2"><p>ذالمعالی علیک ذوالعرش صلی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ان دیناً بذات نفسک فیه</p></div>
<div class="m2"><p>او دعته العلوج شیخاً عتلاً</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>فقضی فیه ما قضی ثم اوصی</p></div>
<div class="m2"><p>با البقایا الی اخیه و دلی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ثم اولی بها ثالث القوم</p></div>
<div class="m2"><p>غلولا لا حی فیما اغلاّ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>فتولاه اطهر العرب ذیلاً</p></div>
<div class="m2"><p>لم تولد له العقائل مثلاً</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>فذراه ذر و الهشیم فلم تبرک</p></div>
<div class="m2"><p>حراماً اتاه الا احلا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>فتملت بها امی و لما</p></div>
<div class="m2"><p>اوغلت اوطئته خیلا و رجلا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ثم عرج الی ضجیعیه و اسئل</p></div>
<div class="m2"><p>لمن المرقد الذی فیه حلا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یا خلیلی خلیاً عن ملامی</p></div>
<div class="m2"><p>ن و حراً فی الصدر لا زال یغلا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>افتر ذی خلافته الله عمن</p></div>
<div class="m2"><p>حفه الله ان یساجل فضلا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>و هو قطب الرحی تدور علیه</p></div>
<div class="m2"><p>ذائرات الا کوان علواً و سفلا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>خص من ربه بانوار قدس</p></div>
<div class="m2"><p>ملات خافقیه عرضاً و طولا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>و بلیها من لاله جمل فیها</p></div>
<div class="m2"><p>و لا ناقته و لا هزخیلا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>یا امیر الوری مدیحته عبد</p></div>
<div class="m2"><p>قداتی موصلا بحبلک حبلا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>فتقبل منه بضاعه عاف</p></div>
<div class="m2"><p>لم یجد للو قود غیرک اهلا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>صل وجد ایها العزیز و اوف</p></div>
<div class="m2"><p>الکیل و ازدده من نوالک کیلا</p></div></div>