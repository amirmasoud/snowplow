---
title: >-
    بخش ۵۶ - ایضا
---
# بخش ۵۶ - ایضا

<div class="b" id="bn1"><div class="m1"><p>زخاک اگر همه بعد از تو حور عین خیزد</p></div>
<div class="m2"><p>سلاله چو تو مشکل زماء وطین خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه ار زچرخ بیارد بصد قران بالله</p></div>
<div class="m2"><p>گر از زمین چو توئی ماه بیقرین خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آن فرشته جان آفرین که نقش توبست</p></div>
<div class="m2"><p>سزد که آب و گل و آدم آفرین خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تبارک الله از آن جنبش و کرشمه ناز</p></div>
<div class="m2"><p>کسی ندید که سروی بپا چنین خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود که تحفه برندش ببوستان بهشت</p></div>
<div class="m2"><p>شمامه که از این جعد عنبرین خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یقین نبود مرا تا نه کاکل تو شکست</p></div>
<div class="m2"><p>که هرچه مشک بعالم همه زچین خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمن سپرس که بر جان لاغرت چه گذشت</p></div>
<div class="m2"><p>زتیر پرس کزان بازوی سمین خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکش بروی خود آنطره چلیپائی</p></div>
<div class="m2"><p>کزین معامله غوغای کفر و دین خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآفتاب جبینت توان قسم خوردن</p></div>
<div class="m2"><p>که ماه یک شبه از جیب این جبین خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زسبزه خیزد اگر انگبین عجب نبود</p></div>
<div class="m2"><p>عجب زسبزه خطی کز انگبین خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فکند جنبش مویش مرا بدریائی</p></div>
<div class="m2"><p>که تا نگاه کند دیده موج چین خیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ستیزه جو که به تریاق زهرکس ماند</p></div>
<div class="m2"><p>جواب تلخ کزان لعل شکرین خیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو سنگ میزنی ای ترک سنگدل باری</p></div>
<div class="m2"><p>چنان بزن که تواند کس از زمین خیزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زکاوش تو مرا حرزجان غبار دریست</p></div>
<div class="m2"><p>کز آستانه سلطان هشتمین خیزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شهنشهی که بموی گر التفات کند</p></div>
<div class="m2"><p>هزار ملک سلیمان اش از نگین خیزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بقطره زیمنش اگر یسار دهد</p></div>
<div class="m2"><p>هزار بحر گهر زایش از یمین خیزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه او خدا نه خدا و ولی خدا لقب است</p></div>
<div class="m2"><p>هزار نکته دلش خود از همین خیزد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه اسم او نه مسمی دوبین که شرک آرد</p></div>
<div class="m2"><p>نه اسم عین مسمی که کفر و کین خیزد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>علیست اسم وی او اسم کردگار ودود</p></div>
<div class="m2"><p>ولیک اسم دوم نیز از اولین خیزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس ایندو اسم مرتب بوضع هر دو ازوست</p></div>
<div class="m2"><p>تغایر از حول دیده دوبین خیزد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی در آیینه اسم جز مسمی نیست</p></div>
<div class="m2"><p>چو او بجلوه درآید نمود ازین خیزد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس او علی و علی او و هیچ نیست جز او</p></div>
<div class="m2"><p>به بین در آیینه کز وی ترا یقین خیزد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فقیه شهر شکر غاید اندرین دعوی</p></div>
<div class="m2"><p>عصا کشیده بتکفیرم از کمین خیزد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مار میت صریح کلام لم یزلیست</p></div>
<div class="m2"><p>کدام کفر از این آیه مبین خیزد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لذا مع الله بیخود نگفت امام مبین</p></div>
<div class="m2"><p>زجمع و فرق بهم فرق کفر و دین خیزد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زصحبت شه عشق است نیرا که مرا</p></div>
<div class="m2"><p>زبحر طبع چنین گوهر ثمین خیزد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زنظم دلکش آنقهرمان ملک سخن</p></div>
<div class="m2"><p>بر ایندو بیت گواهم کز آستین خیزد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بغیر حظ که از آن لعل شکرین خیزد</p></div>
<div class="m2"><p>کجا شنیده کسی خار از انگبین خیزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه مشک زچین خیزد ایعجب زلفت</p></div>
<div class="m2"><p>چگونه مشک از او صد هزار چین خیزد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هزار نکته دلکش بنظم رفت هنوز</p></div>
<div class="m2"><p>از این دو بیت دلا طبع شرمگین خیزد</p></div></div>