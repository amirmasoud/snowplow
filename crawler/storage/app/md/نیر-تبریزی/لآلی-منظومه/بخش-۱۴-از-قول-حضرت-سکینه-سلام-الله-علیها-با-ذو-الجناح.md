---
title: >-
    بخش ۱۴ - از قول حضرت سکینه سلام الله علیها با ذو الجناح
---
# بخش ۱۴ - از قول حضرت سکینه سلام الله علیها با ذو الجناح

<div class="b" id="bn1"><div class="m1"><p>لیک پی اسب چرا بیرخ شاه آمدۀ</p></div>
<div class="m2"><p>پیل بودی تو چرا مات زراه آمدۀ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگ برگشته و تن خسته و بگسسته لگام</p></div>
<div class="m2"><p>هوش خود باخته با حال تباه آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایفرس قافله سالار تو کشتند مگر</p></div>
<div class="m2"><p>که تو با قافلۀ آتش و آه آمدۀ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندکی پیش تو را بال هما بر سر بود</p></div>
<div class="m2"><p>چه شد آن سایه که اینجا بپناه آمدۀ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونشد آنشاه و سپاهی که بمیدان بردی</p></div>
<div class="m2"><p>که تو تنها همه بی شاه و سپاه آمدۀ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رخ سرخ برفتی زیر ما تو کنون</p></div>
<div class="m2"><p>چه خطا رفته که با روی سپاه آمدۀ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با همان شاه که بردی تو بمیدان بلا</p></div>
<div class="m2"><p>بیگنه کشته عدو و تو گواه آمدۀ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شه ما را مگر افکندۀ ای اسب بخاک</p></div>
<div class="m2"><p>عذر جویان ز پی عفو گناه آمدۀ</p></div></div>