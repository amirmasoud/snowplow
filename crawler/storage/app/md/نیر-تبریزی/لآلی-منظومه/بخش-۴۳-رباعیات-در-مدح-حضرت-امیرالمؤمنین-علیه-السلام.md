---
title: >-
    بخش ۴۳ - رباعیات در مدح حضرت امیرالمؤمنین علیه السلام
---
# بخش ۴۳ - رباعیات در مدح حضرت امیرالمؤمنین علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ای سرّ خدا که ره بر اسرار توئی</p></div>
<div class="m2"><p>جز حیرت و صمت چاره در کار توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانسوی دگر خدای گفتن بتو کفر</p></div>
<div class="m2"><p>زین سو صفتی دگر سزاوار توئی</p></div></div>