---
title: >-
    بخش ۸ - زبانحال از قول حضرت ابی عبدالله در قتلگاه است
---
# بخش ۸ - زبانحال از قول حضرت ابی عبدالله در قتلگاه است

<div class="b" id="bn1"><div class="m1"><p>تا خبر دارم از او بیخبر از خویشتنم</p></div>
<div class="m2"><p>با وجودش ز من آواز نیاید که منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرهن گو همه پر باش ز پیکان بلا</p></div>
<div class="m2"><p>که وجودم همه او گشت و من این پیرهنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باش یکدم که کنم پیرهن شوق قبا</p></div>
<div class="m2"><p>ایکمان کش که زنی ناوک پیکان به تنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق را روز بهار است کجا شد رضوان</p></div>
<div class="m2"><p>تا برد لاله بدامن سوی خلد از چمنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز عهد است بکش اسپرم ایعقل ز پیش</p></div>
<div class="m2"><p>تا تصور نکند خصم که پیمان شکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نیاید بکمن راست تن کشتۀ عشق</p></div>
<div class="m2"><p>خصم دون بیهده گو باز ندوزد کفنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هانفم میدهد از غیب ندا شمر کجاست</p></div>
<div class="m2"><p>گوشتابی که بیاد آمده عهد کهنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخت دلتنک شدم همتی ایشهپر تیر</p></div>
<div class="m2"><p>بشکن ایندام بکش باز بسوی وطنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دایۀ عشق ز بس داد، مرا خون جگر</p></div>
<div class="m2"><p>میدمد آبلۀ زخم کنون از بدنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوی مطلع چه عجب گر برم از فارس فارس</p></div>
<div class="m2"><p>تا بمدح تو شها تیر شیرین سخنم</p></div></div>