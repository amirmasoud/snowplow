---
title: >-
    بخش ۳
---
# بخش ۳

<div class="b" id="bn1"><div class="m1"><p>آه از آنروز که در دشت بلا غوغا بود</p></div>
<div class="m2"><p>شورش روز قیامت بجهان برپا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خصم چوندایره گرد حرم شاه شهید</p></div>
<div class="m2"><p>در دل دایره چون نقطه پابرجا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرصه دشت چو دیبای منقش از خون</p></div>
<div class="m2"><p>و آنهمه صورت زیبا که در آن دیبا بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان بقربان ذبیحی که بقربانگه دست</p></div>
<div class="m2"><p>با لب تشنه روان میشد و خود دریا بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو مپندار که شاهنشه دین درگه رزم</p></div>
<div class="m2"><p>در بیابان بلا بی مدد و تنها بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انبیا و رسل و جن و ملایک هر یک</p></div>
<div class="m2"><p>جان بکف در بر شه منتظر ایما بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون هابیل که شد ریخته از سنگ جفا</p></div>
<div class="m2"><p>گر بعبرت نگری کشته آن صحرا بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده پوشان نهانخانه ملک و ملکوت</p></div>
<div class="m2"><p>همه پروانۀ آنشمع جهان آرا بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قتل عباس وعلی اکبر و قاسم ز ازل</p></div>
<div class="m2"><p>بر فرامین قضایای فلک طغرا بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورنه اندر نظر قهر شهنشاه جهان</p></div>
<div class="m2"><p>عدم هر دو جهان بسته بحرف لا بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علی اکبر برخ چونگل و باقد چو سرو</p></div>
<div class="m2"><p>فرد و تنها بسوی رزمگه اعدا بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علم الله که شقایق نه بدان لطف و سمن</p></div>
<div class="m2"><p>نه بدان بوی صنوبر نه بدان بلا بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرد شمع رخ اکبریکه صبح وداع</p></div>
<div class="m2"><p>لیلی سوخته پروانه بی پروا بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخم نر جسم علی اکبر و لیلی دل خون</p></div>
<div class="m2"><p>خونز مجنون رود آری چو رگ از لیلا بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در همه ملک بلا نیست بجز ذکر حسین</p></div>
<div class="m2"><p>قاف تا قاف جهان صوت همین عنقا بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیر آنروز که طغرای قضا می بستند</p></div>
<div class="m2"><p>سرنوشت من از این نامه همین طغرا بود</p></div></div>