---
title: >-
    بخش ۴
---
# بخش ۴

<div class="b" id="bn1"><div class="m1"><p>بازم از این واقعۀ دشت بلا یاد آمد</p></div>
<div class="m2"><p>خرمن صبر و ثباتم همه بر باد آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شگفتم ز چه در هم نشد اجزای وجود</p></div>
<div class="m2"><p>زان همه ضعف که بر علّت ایجاد آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از آندم که شه دین بهزاران تشویش</p></div>
<div class="m2"><p>بر سر قاسم ناکام بامداد آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدکاغشته تنش چونگل سیراب بخون</p></div>
<div class="m2"><p>آهش از آتش اندوه زبنیاد آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که بزانو سر حسرت که مر این صید ضعیف</p></div>
<div class="m2"><p>بچه جرمی هدف ناوک صیاد آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که بدندان لب حیرت که گه جلوه گری</p></div>
<div class="m2"><p>چشم زخمی که بر این حسن خدا داد آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس چو جان پیکرش از لطف در آغوش کشید</p></div>
<div class="m2"><p>رو بسوی حرم آورد و بفریاد آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کایعروس حسن از بخت شکایت منما</p></div>
<div class="m2"><p>حجلۀ حسن بیارای که داماد آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیر از خاک در شاه مکش روی نیاز</p></div>
<div class="m2"><p>کانکه شد حلقه بگوش درش آزاد آمد</p></div></div>