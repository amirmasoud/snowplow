---
title: >-
    بخش ۱۰ - ایضا مناجات از قول حضرت سید الشهداء علیه التحیه و الثناء
---
# بخش ۱۰ - ایضا مناجات از قول حضرت سید الشهداء علیه التحیه و الثناء

<div class="b" id="bn1"><div class="m1"><p>محبوبم الله لبیک لبیک</p></div>
<div class="m2"><p>مطلوبم الله لبیک لبیک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرگچه مین بول باشم جدایه</p></div>
<div class="m2"><p>بو تن بو تسلیم حکم قضایه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقف ایتمشم جای کوی بلایه</p></div>
<div class="m2"><p>محبوبم الله لبیک لبیک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا وار بود باشد عشقون هواسی</p></div>
<div class="m2"><p>تیغ جفادن پور خدور هراسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوک سنابدور کره منلسی</p></div>
<div class="m2"><p>محبوبم الله لبیک لبیک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عهد الستی باشد پتوردوم</p></div>
<div class="m2"><p>یتمش ایکی باش الده کوتوردوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوی وفایه قربان گتوردوم</p></div>
<div class="m2"><p>محبوبم الله لبیک لبیک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاغدورسا عشقون تا روز محشر</p></div>
<div class="m2"><p>ابر بلادن تیریله خنجر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بو حلق اصغر بو جسم اکبر</p></div>
<div class="m2"><p>محبوبم الله لبیک لبیک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گور زینبیمون اشگیله آهین</p></div>
<div class="m2"><p>پیرامننده دشمن سپاهین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عفو ایت الهی امت گناهین</p></div>
<div class="m2"><p>محبوبم الله لبیک لبیک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سن سن چو مقصود ای بی نیازم</p></div>
<div class="m2"><p>گر اولسا اعدا قتلیمه عازم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بو باش بو میدان خنجر نه لازم</p></div>
<div class="m2"><p>محبوبم الله لبیک لبیک</p></div></div>