---
title: >-
    بخش ۳۲ - رباعیات در مدح حضرت امیرالمؤمنین علیه السلام
---
# بخش ۳۲ - رباعیات در مدح حضرت امیرالمؤمنین علیه السلام

<div class="b" id="bn1"><div class="m1"><p>در وصف علی که هر که رائی دارد</p></div>
<div class="m2"><p>کفر است خدائی که خدائی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیک از نباء عظیم باید دانست</p></div>
<div class="m2"><p>کاینطرفه خبر چه مبتدائی دارد</p></div></div>