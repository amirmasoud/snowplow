---
title: >-
    بخش ۵۲ - عربیه
---
# بخش ۵۲ - عربیه

<div class="b" id="bn1"><div class="m1"><p>ابا حسن افدیک اعیت مذاهبی</p></div>
<div class="m2"><p>و عیل اصطباری من کرور النوائب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمثلت الدنیا علی فلم اجد</p></div>
<div class="m2"><p>سوابا بک السامی مناخا لراکب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فجئک و الاحشاء تهفو علی الفضاء</p></div>
<div class="m2"><p>فاحسن جواز الضیف یابن الاطائب</p></div></div>