---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>اگرم ز بی نیازی همه خوار و زار دارد</p></div>
<div class="m2"><p>چه غم آنگل دورو را که چو من هزار دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز غمت سینه این آه از آن نهفته دارم</p></div>
<div class="m2"><p>که مرا ز بی چراغی بسر مزار دارد</p></div></div>