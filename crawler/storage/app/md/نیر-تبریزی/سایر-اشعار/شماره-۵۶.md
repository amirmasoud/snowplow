---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>ز پریشانی ما کی شود آگاه کسی</p></div>
<div class="m2"><p>که گرفتار بدان زلف پریشان نشود</p></div></div>