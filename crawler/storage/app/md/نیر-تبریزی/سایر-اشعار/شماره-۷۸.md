---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>ایسر مه سای چشم بتان خاکراه تو</p></div>
<div class="m2"><p>صد حلقه دل بحلقۀ زلف سیاه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در سر است قتل جهانیت باک نیست</p></div>
<div class="m2"><p>بر پای خود کشم بقیامت گناه تو</p></div></div>