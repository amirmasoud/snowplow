---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>شمیم نافه میدهد نسیم جویبارها</p></div>
<div class="m2"><p>گذشته آهوی ختن مگر ز کوهسارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیده بوی فرودین مگر ز باد عنبرین</p></div>
<div class="m2"><p>بلب گرفته ساتکین ز لاله جو کنارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نه نرگس چمن کشیده بادۀ کهن</p></div>
<div class="m2"><p>چرا خزد بخویشتن چو کهنه میگسارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیام وصل گل مگر شنیده بلبل سحر</p></div>
<div class="m2"><p>که کرده باز ناله سر بدور شاخسارها</p></div></div>