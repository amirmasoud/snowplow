---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>بس که از آه سحر مشعله روشن کردم</p></div>
<div class="m2"><p>دزد شب را سوی دل راه معین کردم</p></div></div>