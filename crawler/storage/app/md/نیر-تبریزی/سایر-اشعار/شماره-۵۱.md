---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>دلم از یاد هجر دوست خون است</p></div>
<div class="m2"><p>ندانم حال مهجوران که چون است</p></div></div>