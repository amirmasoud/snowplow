---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>چه مژده بود که باد سحرگهی آورد</p></div>
<div class="m2"><p>هوای شور و طرب بر دلم رهی آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از سموم حوادث کنون شود ایمن</p></div>
<div class="m2"><p>که رو به سایهٔ آن قامت سهی آورد</p></div></div>