---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گر نیست مه رخت ز چه رو از شکنج زلف</p></div>
<div class="m2"><p>هر شامگه بشکل دگر سر برآورد</p></div></div>