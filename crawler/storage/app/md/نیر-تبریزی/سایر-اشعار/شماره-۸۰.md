---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>ای زلف ز بسکه خود پریشانی</p></div>
<div class="m2"><p>آشفتگی دلم نمیدانی</p></div></div>