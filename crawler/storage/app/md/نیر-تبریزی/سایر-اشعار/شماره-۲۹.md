---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>المنه الله که می وصل بجام است</p></div>
<div class="m2"><p>کار من و ساقی همه بر وفق مرام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حرمت می زاهد اگر پخته خیالی</p></div>
<div class="m2"><p>ایدردکشان عیب مگیرید که خام است</p></div></div>