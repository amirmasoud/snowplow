---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>ریزند خون من آخر خوبان بعربده جوئی</p></div>
<div class="m2"><p>نی نیست جای شکایت کاین است رسم نکوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای داده دل بجدائی تا کی قرین جفائی</p></div>
<div class="m2"><p>سوی صفا نگرانی راه وفا نپوئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهم که بخت مساعد یاری کند بدو چیزم</p></div>
<div class="m2"><p>یا ساده با لب جامی یا باده با لب جوئی</p></div></div>