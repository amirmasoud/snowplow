---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>نه تیر رفته بسوی کمان فراز آید</p></div>
<div class="m2"><p>نه روزگار جوانی که رفت باز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعیم روز جوانی مده بدست هوا</p></div>
<div class="m2"><p>که شمع شب ز هوا زود در گداز آید</p></div></div>