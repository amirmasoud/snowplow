---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>در حلقۀ گیسوی تو هر حلقه اسیری</p></div>
<div class="m2"><p>وز نرگس خمار تو هر گوشه خرابی</p></div></div>