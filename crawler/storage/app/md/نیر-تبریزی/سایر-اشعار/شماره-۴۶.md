---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>در خواب به کف داشتم این طرهٔ پرتاب</p></div>
<div class="m2"><p>ای کاش که بیدار نمی‌گشتم از این خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن طرّهٔ مشکین تو با سنبل عطشان</p></div>
<div class="m2"><p>وان لعل می‌ْآگین تو با غنچهٔ سیراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سنبل عطشان تو سیراب دل از خون</p></div>
<div class="m2"><p>وز غنچهٔ سیراب تو عطشان لب احباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آبم از سر برگذشت ای ساربان محمل بدار</p></div>
<div class="m2"><p>ترسم از اشکم فروماند شتر را پی در آب</p></div></div>