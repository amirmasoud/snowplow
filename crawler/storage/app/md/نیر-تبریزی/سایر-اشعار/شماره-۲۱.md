---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>همه شب چشم من و صبح گریبان تو بود</p></div>
<div class="m2"><p>طرفه شوری بسرم از لب خندان تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سروکارم همه با زلف پریشان تو بود</p></div>
<div class="m2"><p>بنده وارم همه شب گوش بفرمان تو بود</p></div></div>