---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>دارم اندر تن دل ویرانه‌ای</p></div>
<div class="m2"><p>واندران دل سینه سوز افسانه‌ای</p></div></div>