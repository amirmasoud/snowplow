---
title: >-
    بخش ۲۴ - رفتن آنحضرت بیاری پادشاه هند
---
# بخش ۲۴ - رفتن آنحضرت بیاری پادشاه هند

<div class="b" id="bn1"><div class="m1"><p>اندرین حال آن هژیر رزمکوش</p></div>
<div class="m2"><p>کامدش صوتی ز هندستان بگوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کایغیاث المستغیثین ای مجیر</p></div>
<div class="m2"><p>دست شیران دستگیرم دستگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای رهائی داده یونس را ز بم</p></div>
<div class="m2"><p>دست من بر گیر که گم شده پیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستگیر یوسف اندر چه توئی</p></div>
<div class="m2"><p>رهنمائی کن که خضر ره توئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه دین لبیک گویان بیدرنگ</p></div>
<div class="m2"><p>سوی هندستان شد از میدان جنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیر چون دید آنشه حیدر شکوه</p></div>
<div class="m2"><p>پای وی بوید و بر شد سوی کوه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت آنشه زاده سوی بارگاه</p></div>
<div class="m2"><p>سوی میدان باز پس گردید شاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ظالمی زد ناگهان تیری ز کین</p></div>
<div class="m2"><p>شهسوار لامکانرا بر جبین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیر چون زانجبهۀ غرّا گذشت</p></div>
<div class="m2"><p>خونش از قوسین او ادنی گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو ببالا کرد کایدادار فرد</p></div>
<div class="m2"><p>تو گواهی کاین خسان با من چه رد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناگهان تیر سه شعبه از کمین</p></div>
<div class="m2"><p>کرد قصد آن خدیو راستین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواست جا بر سینۀ آنشاه کرد</p></div>
<div class="m2"><p>جان نجست و رو بقلب الله کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد شه رنگین محاسن زانخضاب</p></div>
<div class="m2"><p>گفت چونین رفت خواهم نزد باب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گویمش تا کای شه لولاک شأن</p></div>
<div class="m2"><p>خون من خواه از فلان و از فلان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کانکه طرح بیعت شوری فکند</p></div>
<div class="m2"><p>خود همانجا طرح عاشورا فکند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرخ در یثرب رها کرد از کمان</p></div>
<div class="m2"><p>تیر کاندر نینوا شد بر نشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرد بهر تحفۀ دیدار یار</p></div>
<div class="m2"><p>دست حق آنخون ناحق را نثار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون بخود لرزید از ان خون جسم خاک</p></div>
<div class="m2"><p>دامن گردون گرفت آنخون پاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا قیامت چرخار دلخون از اوست</p></div>
<div class="m2"><p>سرخی این طاق مینا گون از اوست</p></div></div>