---
title: >-
    بخش ۲ - ورود حضرت ابی عبدالله علیه السلام بزمین کربلا و خطبهٔ آنحضرت در شب عاشورا و تفرق لشگر
---
# بخش ۲ - ورود حضرت ابی عبدالله علیه السلام بزمین کربلا و خطبهٔ آنحضرت در شب عاشورا و تفرق لشگر

<div class="b" id="bn1"><div class="m1"><p>چون در آن دشت بلا افکند یار</p></div>
<div class="m2"><p>کرد از بیگانگان خالی دیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشر ماه محرم شامگاه</p></div>
<div class="m2"><p>شد به منبر باز شاه کم سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاورانش گرد او گشتند جمع</p></div>
<div class="m2"><p>راست چون پروانگان بر دور شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهران شاه نظاره ز پی</p></div>
<div class="m2"><p>چون بنات النعش بر گرد جدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو به یاران کرد و در گفتار شد</p></div>
<div class="m2"><p>حقه یاقوت گوهر بار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد تحمید و درود آن شاه راد</p></div>
<div class="m2"><p>گفت یاران مرگ رو بر ما نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این حسین و این زمین کربلاست</p></div>
<div class="m2"><p>سوی تا سو تیر باران بلا است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی خون آید از این کهسار دشت</p></div>
<div class="m2"><p>باز گردد هر که خواهد بازگشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که او را تاب تیغ و تیر نیست</p></div>
<div class="m2"><p>باز گردد پای در زنجیر نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این شب و این دشت پهناور به پیش</p></div>
<div class="m2"><p>باز گیرید ای رفیقان رخت خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کار این قوم جفا جو با من است</p></div>
<div class="m2"><p>هر که جز من زین کشاکش ایمن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من ز تنهائی نیم یاران ملول</p></div>
<div class="m2"><p>وا هلیدم اندر این دشت مهول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وا هلیدم هین ز من یک سو شوید</p></div>
<div class="m2"><p>راست زانسو کامدید آنسو روید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وا هلیدم اندرین دریای خون</p></div>
<div class="m2"><p>تا کنم زانسوی دریا سر برون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسته ایم عهدی من و شاه وجود</p></div>
<div class="m2"><p>وا هلیدم تا روم آنجا که بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاد زی شاد ای زمین کربلا</p></div>
<div class="m2"><p>این من و این تیر باران بلا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سوی تو با شوق دیدار آمدم</p></div>
<div class="m2"><p>بردم اینجا بوئی از یار آمدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آمدم تا جسم و جان قربان کنم</p></div>
<div class="m2"><p>منزل آن سو تر ز جسم و جان کنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آمدم تا دست و پا در خون کنم</p></div>
<div class="m2"><p>کاین چنین خواهد نگار مهوشم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آمدم کز عهد در لب تر کنم</p></div>
<div class="m2"><p>با لب خنجر حدیث از سر کنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پس روید ای هم رهان زین بزم زه</p></div>
<div class="m2"><p>بزم جانان خلوت از اغیار به</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیک هر سو روی بیتابید ایفریق</p></div>
<div class="m2"><p>دورتر رانید از این دشت سحیق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کانکه فردا اندرین دشت مهول</p></div>
<div class="m2"><p>بشنود فریاد احفاد رسول</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تن زند از یاری از خبث سرشت</p></div>
<div class="m2"><p>در قیامت نشنود بوی بهشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رفت بر سر چون حدیث شهریار</p></div>
<div class="m2"><p>شد برون اغیار باقی ماند یار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عشق از اول سرکش و خوبی بود</p></div>
<div class="m2"><p>تا گریزد هر که بیرونی بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت یاران کایحیات جان ما</p></div>
<div class="m2"><p>دردهای عشق تو درمان ما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رشتۀ جانهای ما در دست تست</p></div>
<div class="m2"><p>هستی ما را وجود از هست تست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سایه از خور چون تواند شد جدا</p></div>
<div class="m2"><p>با خود از صوتی جدا افتد صدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زنده بی جان کی تواند کر ز بست</p></div>
<div class="m2"><p>زندگی را بی تو خون باید گریست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ما بساحل خفته و تو غرق خون</p></div>
<div class="m2"><p>لاو حق البیت هذا لایکون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کاش ما را صد هزاران جان بدی</p></div>
<div class="m2"><p>تا نثار جلوۀ جانان بدی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر رود از ما دو صد جان باک نیست</p></div>
<div class="m2"><p>تو بمان ای آنکه چون تو پاک نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هین مران ای پادشاه را سنان</p></div>
<div class="m2"><p>این سگان پیر را از آستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در بروی ما مبند ای شهریار</p></div>
<div class="m2"><p>خلوت از اغیار باید نی زیار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جان کلافه ما عجوز عشق کیش</p></div>
<div class="m2"><p>یوسفا از ما مگردان روی خویش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ما به بیداری هوس گم نیستیم</p></div>
<div class="m2"><p>ناز پرورد تنعم نیستیم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ما به آه خشک و چشم تر خوشیم</p></div>
<div class="m2"><p>یونس آب و خلیل آتشیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اندرین دشت بلا تا پا زدیم</p></div>
<div class="m2"><p>پای بر دنیا و ما فیها زدیم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون شهنشه دید حسن عهدشان</p></div>
<div class="m2"><p>وان بکار جان سپاری جهدشان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پرده از دیدار یک یک باز هشت</p></div>
<div class="m2"><p>جای شان بنمود در باغ بهشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حوریان دیدند در وی صف بصف</p></div>
<div class="m2"><p>سر برون آورده یکسر از غرف</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کاندرا که چشم بر راه تو ایم</p></div>
<div class="m2"><p>مشتری روی چون ماه تو ایم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای تو ما را ماه و ما برجیس تو</p></div>
<div class="m2"><p>تو سلیمانی و ما بلقیس تو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ای سلیمان هین سوی بلقیس شو</p></div>
<div class="m2"><p>همچو رامین در وثاق ویس شو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یوسفا باز آی از این زندان زفت</p></div>
<div class="m2"><p>که زلیخا را شکیب از دست رفت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اندرا کز عشق مفتون توایم</p></div>
<div class="m2"><p>گر چه لیلائیم و مجنون توایم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زان سپس شه خواند مردی را به پیش</p></div>
<div class="m2"><p>بر کف او برنهاد انگشت خویش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شد روان زاندست آبی خوشگوار</p></div>
<div class="m2"><p>جمله نوشیدند اصحاب کبار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اندر آن شب که شب عاشور بود</p></div>
<div class="m2"><p>ماه تا ماهی سراسر شور بود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شاه دین در خیمه با اصحاب راد</p></div>
<div class="m2"><p>در نیاز و راز با رب العباد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کوفیان در نقض آن عهد نخست</p></div>
<div class="m2"><p>سرخوش از پیمانۀ پیمان سست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شمر دون سرمست صهبای غرور</p></div>
<div class="m2"><p>شاه دین سرشار مینای حضور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پور سعد از ذوق ری سرگرم مست</p></div>
<div class="m2"><p>شاه از اقلیم هستی شسته دست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زینب آن در دانه درج شرف</p></div>
<div class="m2"><p>از دو چشم تر در افشان چون صدف</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دیدۀ لیلی ز دیدار پسر</p></div>
<div class="m2"><p>کرده دامن پر گل از لخت جگر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مادر قاسم ز بهر حجله گاه</p></div>
<div class="m2"><p>کرده روشن شمع ها از دود آه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شربت بیمار خون جام دل</p></div>
<div class="m2"><p>شیر پستان از لب اصغر خجل</p></div></div>