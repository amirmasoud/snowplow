---
title: >-
    بخش ۲۱ - آمدن زعفر پادشاه جن بیاری آنحضرت
---
# بخش ۲۱ - آمدن زعفر پادشاه جن بیاری آنحضرت

<div class="b" id="bn1"><div class="m1"><p>زعفر آمد با سپاه بیعدد</p></div>
<div class="m2"><p>از گروه جنیان بهر مدد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت شاها بهر یاری آمدم</p></div>
<div class="m2"><p>نی که بهر حق گذاری آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باب تو آنصاحب تاج غدیر</p></div>
<div class="m2"><p>بر گروه جنیان کردم امیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هین بفرما ای امیر غرب و شرق</p></div>
<div class="m2"><p>کاین سیه بختان بخون سازیم غرق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هین بفرما ایشه حیدر حشم</p></div>
<div class="m2"><p>تا کنم تو غزوۀ بتر العلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هین بفرما ای شه احمد شکوه</p></div>
<div class="m2"><p>تا فرو شویم بخون ایندشت و کوه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هین بفرما تا نمایم زینفریق</p></div>
<div class="m2"><p>تا فسخ ناری در ایندشت سحیق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت حاشا رحمت رب غفور</p></div>
<div class="m2"><p>کی پسندد چیره بینا را بکور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت ما نیز ایخداوند صور</p></div>
<div class="m2"><p>نک فرود آئیم در جلد بشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بکوشش با هم انبازی کنیم</p></div>
<div class="m2"><p>چون بشر مردانه جانبازی کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت من خود قدرت یزدانیم</p></div>
<div class="m2"><p>حیدر بدر و احد را ثانیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست قدرت گر بر آرم زاستین</p></div>
<div class="m2"><p>آدمی از تو برآرم زاب و طین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این بنای کهنه را فانی کنم</p></div>
<div class="m2"><p>ابتدای عالم ثانی کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک عشقم کرده سیر از جان خویش</p></div>
<div class="m2"><p>زعفرا دیر است واپس ران زپیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسته ام عهدی بجانان در الست</p></div>
<div class="m2"><p>که بجز وی بگذرم از هر چه هست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آمده سر وعدۀ سوگند من</p></div>
<div class="m2"><p>زعفرا بگذر مشو پابند من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زعفرا من شاهبار عرشیم</p></div>
<div class="m2"><p>تنگدل زبندامگاه فرشیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نک سفیرم میزنند از آسمان</p></div>
<div class="m2"><p>بگذر افسون پری با من مخوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چند باید داشت این تار تنم</p></div>
<div class="m2"><p>همچو عیسی پای بند سوزنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زعفر از میدان بزاری بازگشت</p></div>
<div class="m2"><p>شاه ماند و خصم آن پهنای دشت</p></div></div>