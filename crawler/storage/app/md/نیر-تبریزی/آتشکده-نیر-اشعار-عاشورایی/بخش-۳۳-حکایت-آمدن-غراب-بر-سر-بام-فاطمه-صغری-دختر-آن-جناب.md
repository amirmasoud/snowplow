---
title: >-
    بخش ۳۳ - حکایت آمدن غراب بر سر بام فاطمهٔ صغری دختر آن جناب
---
# بخش ۳۳ - حکایت آمدن غراب بر سر بام فاطمهٔ صغری دختر آن جناب

<div class="b" id="bn1"><div class="m1"><p>از پس قتل خدیو مستطاب</p></div>
<div class="m2"><p>آمد از گردون یکی مشگین غراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر فرو برد اندران خون رطیب</p></div>
<div class="m2"><p>شد به یثرب باز نالان با نعیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نشست آن مرغ رنگین پر و بال</p></div>
<div class="m2"><p>بر لب بام خدیو ذوالجلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دخت شه از خوابگه بیرون دوید</p></div>
<div class="m2"><p>دید مرغی لیک بس ناخوش نوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد جهان در چشم او بال غراب</p></div>
<div class="m2"><p>ریخت پروین از مژه بر آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کایدریغا شد دگرگون فال من</p></div>
<div class="m2"><p>خون نماید قرعۀ اقبال من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الله اینمرغ از کدامین گلشن ست</p></div>
<div class="m2"><p>که سفیرش آتش صد خرمن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس صدای غم فزائی میدهد</p></div>
<div class="m2"><p>بوی مرگ آشنائی میدهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طایرا آتش زدی بر جان من</p></div>
<div class="m2"><p>یاد آوردی ز هندستان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طایرا از شاخ طوبی آمدی</p></div>
<div class="m2"><p>یا ز منزلگاه عنقا آمدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مینماید که برید دوستی</p></div>
<div class="m2"><p>هدهد فرخ پیام اوستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک این رنگین بخون بال و پرت</p></div>
<div class="m2"><p>میدهد بوی پیام دیگرت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فاش گو ای طایر شکسته بال</p></div>
<div class="m2"><p>اینچه خون و شاه ما را چیست حال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی قضائی نیست این خون رطیب</p></div>
<div class="m2"><p>یا غراب البین ما حال الحبیب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ببر دا ری پیام وصل یار</p></div>
<div class="m2"><p>مرغ خوش پیغام را با خون چکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای نگارین بال مشگین فام تو</p></div>
<div class="m2"><p>بوی خون می آید از پیغام تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فاش گو ای طایر سدره مقام</p></div>
<div class="m2"><p>از کجائی وز که آوردی پیام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت پیغام فراق آورده ام</p></div>
<div class="m2"><p>وین نواها از عراق آورده ام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمع مشکوه نبوت کشته شد</p></div>
<div class="m2"><p>درّ غلطانش بخون آغشته شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کوفیان از گلشن آل مضر</p></div>
<div class="m2"><p>خوشه ها بستند از گلهای تر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نطق گفتن نیست زین روشن ترم</p></div>
<div class="m2"><p>بانو گوید شرح آن خون پرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زین خبر دخت شهنشاه شهید</p></div>
<div class="m2"><p>واصباحا گفت و شد لرزان چو بید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهر یثرب را چو نی بر ناله کرد</p></div>
<div class="m2"><p>باغ نسرین بوستان لاله کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چهره خست و معجر از سر برگرفت</p></div>
<div class="m2"><p>ارغوان در برک نیلوفر گرفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر زنان موی پریشان باز کرد</p></div>
<div class="m2"><p>حلقه ها از بهر ماتم ساز کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دختران دودۀ آل مناف</p></div>
<div class="m2"><p>سر بر آوردند از سرّ عفاف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>موکنان بر گرد او گشتند جمع</p></div>
<div class="m2"><p>دخت زهرا در میان سوزان چو شمع</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عامه گفتندش که این سحر جلی</p></div>
<div class="m2"><p>افک معهودیست در آل علی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وه چه خوش گفتند دانایان پیش</p></div>
<div class="m2"><p>هر که در آئینه بیند نقش خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نالۀ مرغی که وردش حق حق است</p></div>
<div class="m2"><p>نزد لقلق مایۀ طعن و دق است</p></div></div>