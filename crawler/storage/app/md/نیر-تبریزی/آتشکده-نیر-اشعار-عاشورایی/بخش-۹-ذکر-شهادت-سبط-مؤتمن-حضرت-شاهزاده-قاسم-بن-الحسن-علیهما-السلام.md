---
title: >-
    بخش ۹ - ذکر شهادت سبط مؤتمن حضرت شاهزاده، قاسم بن الحسن علیهما السلام
---
# بخش ۹ - ذکر شهادت سبط مؤتمن حضرت شاهزاده، قاسم بن الحسن علیهما السلام

<div class="b" id="bn1"><div class="m1"><p>قاسم آن نوباوۀ باغ حسن</p></div>
<div class="m2"><p>گوهر شاداب دریای محن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیر مست جام لبریز بلا</p></div>
<div class="m2"><p>تازه داماد شهید کربلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چارده ساله جوان نونهال</p></div>
<div class="m2"><p>برده ماه چادره شب را بسال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قامتش شمشاد باغستان عشق</p></div>
<div class="m2"><p>روش سرمشق نگارستان عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیا فرزانه فرزند حسن</p></div>
<div class="m2"><p>در شجاعت حیدر لشگر شکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با زبان لابه نزد شاه شد</p></div>
<div class="m2"><p>خواستار عزم قربانگاه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت شه کایرشک بستان ارم</p></div>
<div class="m2"><p>رو تو در باغ جوانی خوش بچم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو سرو از باغ غم آزاد باش</p></div>
<div class="m2"><p>شاد زی و شاد بال و شاد باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهلا ای زیبا تذر و خوشخرام</p></div>
<div class="m2"><p>این بیابان سر بسر بند است و دام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الله ای آهوی مشگین نثار</p></div>
<div class="m2"><p>تیر بارانست دشت و کوهسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بوی خون میآید از دامان دشت</p></div>
<div class="m2"><p>نیست کس را زان امید بازگشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی روا باشد که این رعنا نهال</p></div>
<div class="m2"><p>گردد از سم ستوران پایمال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کی روا باشد که این روی چو ورد</p></div>
<div class="m2"><p>غلطد اندر خون بمیدان نبرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت قاسم کایخدیو مستطاب</p></div>
<div class="m2"><p>ای تو ملک عشق را مالک رقاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه خود من کودک نو رسته ام</p></div>
<div class="m2"><p>لیک دست از کامرانی شسته ام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من بمهد عاشقی پرورده ام</p></div>
<div class="m2"><p>خون بجای شیر مادر خورده ام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرده در روز ولادت کام من</p></div>
<div class="m2"><p>باز با شهد شهادت مام من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر چه در دور جوانی کامها است</p></div>
<div class="m2"><p>کار من رفتن بکام اژدها است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کام عاشق غرقه در خون گشتن است</p></div>
<div class="m2"><p>سربخاک کوی جانان هشتن است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ننگ باشد در طریق بندگی</p></div>
<div class="m2"><p>بر غلامان بی شهنشه زندگی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زندگی را بیتو بر سر خاک باد</p></div>
<div class="m2"><p>کامرانی را جگر صد چاک باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لابه های آنقتیل تیر عشق</p></div>
<div class="m2"><p>می نشد بذر رفته نزد پیر عشق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بازگشت آن نوگل باغ رسول</p></div>
<div class="m2"><p>از حضور شاه نومید و ملول</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شد بسوی خیمه آن گلگون عذار</p></div>
<div class="m2"><p>از دو نرگس بر شقایق ژاله بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون نگردد گفت سیر از زندگی</p></div>
<div class="m2"><p>آنکه نپسندد شهش بر بندگی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون زبیقدری نکردن شه قبول</p></div>
<div class="m2"><p>رخت بربند از تن ایجان ملول</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر که فتراکش نبست آنشهسوار</p></div>
<div class="m2"><p>گو سر خود گیر و بر سر خاکبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر بزانوی غم آن والا نژاد</p></div>
<div class="m2"><p>کآمدش ناگه ز عهد باب یاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که بهنگام رحیل آنشاه فرد</p></div>
<div class="m2"><p>هیکلی بر بازویش تعویذ کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت هر جا سخت گردد بر تو کار</p></div>
<div class="m2"><p>نامه بگشا و نظر بر وی گمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر کجا سیل غم آرد بر تو رو</p></div>
<div class="m2"><p>اینوصیت باز کن بنگر در او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت کاری سخت تر زینکار نیست</p></div>
<div class="m2"><p>که بقربانگاه عشقم بار نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یا چه غم زین بیشتر که شاه راد</p></div>
<div class="m2"><p>ره بخلوتگاه خاصانم نداد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نامه را بگشود و دیدش کن پدر</p></div>
<div class="m2"><p>کرده عهدش کایهمایون رخ پسر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای تو نور چشم عم و جان باب</p></div>
<div class="m2"><p>وی مرا تو در وفا نایب منای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من نباشم در زمین کربلا</p></div>
<div class="m2"><p>بر تو بخشیدم من این تاج ولا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون به بینی عم خود را بیمعین</p></div>
<div class="m2"><p>در میان کارزار اهل کین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زینهار ایرو رعنای سهی</p></div>
<div class="m2"><p>لابه ها کن تا بپایش سر نهی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهد کن فردا نباشی شرمسار</p></div>
<div class="m2"><p>در حضور عاشقان جان نثار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جان بشمع عشق چون پروانه زن</p></div>
<div class="m2"><p>خود بر آتش چابک و مردانه زن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر قد موزون کفن میکن قبا</p></div>
<div class="m2"><p>اندران صحرا قیامت کن بپا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاهزاده خواند چون عهد پدر</p></div>
<div class="m2"><p>با ادب بوسید و بنهادش بسر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>می نکنجید از خوشی در پیرهن</p></div>
<div class="m2"><p>حجلۀ داماد شد بیت الحزن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عقدهای مشگلش گردید حل</p></div>
<div class="m2"><p>وان همه انده بشادی شد بدل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از شعق چونغنچۀ خندان شگفت</p></div>
<div class="m2"><p>شکر ایزد را بجای آورد و گفت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ایهمایون قرعۀ اقبال من</p></div>
<div class="m2"><p>کایۀ لا تقنط آمد فال من</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شکر لله کافتتاح این مثال</p></div>
<div class="m2"><p>کوب بختم برآورد از وبال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در فضای عشق بال افشان شدم</p></div>
<div class="m2"><p>لایق قربانی جانان شدم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عهدنامه برد شادان نزد شاه</p></div>
<div class="m2"><p>با تضرع گفت کایظلّ اله</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سوی درگاهت بکف جان آمدم</p></div>
<div class="m2"><p>تک ز شه در دست فرمان آمدم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سر خط امضاده این منشور را</p></div>
<div class="m2"><p>وز جسارت عذر نه مامور را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دید چونشاه آنخ مینو نگار</p></div>
<div class="m2"><p>شد بسیم از جزع مروارید بار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفت کایصورت نگار خوب و زشت</p></div>
<div class="m2"><p>جان فدای دست تو کاینخط نوشت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جان فدای دست تو ایدست حق</p></div>
<div class="m2"><p>که گرفته بر همه دستی سبق</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پس بگفتش شاه کای ماه تمام</p></div>
<div class="m2"><p>کرده با من نیز عهدی آن همام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که ز عقد دخت خود شادت کنم</p></div>
<div class="m2"><p>وندرین غمخانه دامادت کنم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کرده دامادیت را گلگون قبا</p></div>
<div class="m2"><p>نک زخون آماده خیاط قضا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گو بر افزوند بهر حجله گاه</p></div>
<div class="m2"><p>بانوانت شمعها از تف آه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خواهران از درج چشم اشگبار</p></div>
<div class="m2"><p>در بر افشانند از بهر نثار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از دل خونین بنات بوتراب</p></div>
<div class="m2"><p>طشت خون آرند از بهر خضاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>موبه سر گیرند از دلهای ریش</p></div>
<div class="m2"><p>عنبر افشانند از موی پریش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از خراش چهره و لخت جگر</p></div>
<div class="m2"><p>دامن آمویند از گلهای تر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>افکند لب تشنگان طرف آب</p></div>
<div class="m2"><p>عود بر مجمر ز دلهای کباب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پس بامر در درج لو کشف</p></div>
<div class="m2"><p>شد مه و خورشید در برج شرف</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خواست بستن عقد کابین بهرشان</p></div>
<div class="m2"><p>نقد جان آمد سزای مهرشان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>با همین مهر آنشه و الشمس تاج</p></div>
<div class="m2"><p>آندو کوکب را بهم داد ازدواج</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>زهره و برجیس با هم شد قرین</p></div>
<div class="m2"><p>خواست از نه پرده آهنگ حنین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کالله الله اینچه جشن است و چه سور</p></div>
<div class="m2"><p>حلقۀ ماتم بآئیین سرور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شمعهای بارگاه نه تتق</p></div>
<div class="m2"><p>ریخت اشگخون بدامان افق</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گرد چرخ آماده بهر دخت شاه</p></div>
<div class="m2"><p>از تسبیح شام دیبای سیاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>علویان از غم تراشیدند رو</p></div>
<div class="m2"><p>حوریان اندر جنان کندند مو</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زهره واپس ز دیگر دون طبل سور</p></div>
<div class="m2"><p>او فکند اندر جهان شور نشور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نرنالان همچو برگل عندلیب</p></div>
<div class="m2"><p>بسته خون جای حنا کف الخضیب</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دختران بر دور نعش اندر ثبور</p></div>
<div class="m2"><p>خون فشان از دیده شعرای عبور</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بسکه در هم بود دور روزگار</p></div>
<div class="m2"><p>شد بیک گلشن خزان جفت بهار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در میان حجله داماد و عروس</p></div>
<div class="m2"><p>رو بهم چونفرقدان با صد فسوس</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>این سر زانو گرفته در کنار</p></div>
<div class="m2"><p>وان ز درج چشم تر بیچاده بار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کامدش ناگه بگوش از دشت کین</p></div>
<div class="m2"><p>شاه دین را صیحۀ هل من معین</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گفت کای نامبرده کام از زندگی</p></div>
<div class="m2"><p>رفتم از کوی تو با شرمندگی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عذر من بپذیر و اهل دامنم</p></div>
<div class="m2"><p>تا چو بسمل دست و پا در خون زنم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دیر شد یاری فرزند رسول</p></div>
<div class="m2"><p>کن وداعم زود کن عذرم قبول</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>واهلم تا روی بقربانگه کنم</p></div>
<div class="m2"><p>جان نثار خاک پای شه کنم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مرمرا از خون خویش اورنک به</p></div>
<div class="m2"><p>که عتیق باوفا یکرنگ به</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سیر شد دوران ز عیش فرخم</p></div>
<div class="m2"><p>نوعروسا سیر بنگر بر رخم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نو عروسا تار گیسو باز کن</p></div>
<div class="m2"><p>موکنان آهنگ ماتم ساز کن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نوعروسا توشه گیر از بوی من</p></div>
<div class="m2"><p>که نخواهی دید دیگر روی من</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>در عروسی طرح رسم تازه کن</p></div>
<div class="m2"><p>از خراش چهره بر رخ غازه کن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>از سرشک دیده بر روزن گلاب</p></div>
<div class="m2"><p>بر رخ از موی پریشان کن نقاب</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>قبل غم کش بر بساط شادیم</p></div>
<div class="m2"><p>از کفن کن خلعت دامادیم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چونگل از عشقم گریبان پاره کن</p></div>
<div class="m2"><p>حلقۀ زنجیر طوق و پاره کن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>سر برهنه پا در چشم اشگریز</p></div>
<div class="m2"><p>مهد بر نه برهبون بی جهیز</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چتر بر سر از غبار راه زن</p></div>
<div class="m2"><p>بر فلک آتش ز شمع آه زن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>رو بسوی مقتلم با ناله کن</p></div>
<div class="m2"><p>سیر باغ ارغوان و لاله کن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>غرق خونم در میان حجله بین</p></div>
<div class="m2"><p>تشنه ماهی در کنار دجله بین</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مو پریشان ساز با شور و نوا</p></div>
<div class="m2"><p>عنبرستان کن زمین نینوا</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>روی خود نه بر رخ گلگون من</p></div>
<div class="m2"><p>ارغوانی کن عذار از خون م</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ناله در هر شهر و هر ویرانه کن</p></div>
<div class="m2"><p>هر کجا سوریست ماتمخانه کن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>خواست چونرفتن برون از حجله گاه</p></div>
<div class="m2"><p>دامنش بگرفت نالان دخت شاه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گفت کایجان ها اسیر موی تو</p></div>
<div class="m2"><p>کی به بینم بار دیگر روی تو</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>گفت ماند ایسر و قامت بار من</p></div>
<div class="m2"><p>بر قیامت وعدۀ دیدار من</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گفت با آنشوکت و زیب و فرت</p></div>
<div class="m2"><p>من چه سان بشناسم اندر محشرت</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>آستین زد چاک گفتش کایحبیب</p></div>
<div class="m2"><p>این نشان تست در روز حسیب</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>که گواه عاشقان را ستین</p></div>
<div class="m2"><p>پیش اهل دل بود در آستین</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>این بگفت وراند سوی رزمگاه</p></div>
<div class="m2"><p>با تعنت گفت با میر سپاه</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>کاسب خود را دادۀ آب ای لعین</p></div>
<div class="m2"><p>گفت آری گفت ویحک شرم بین</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>اسب تو سیراب و فرزند رسول</p></div>
<div class="m2"><p>نک ز تاب تشنگی از جان ملول</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>سر بزیر افکند از شرم آنعنید</p></div>
<div class="m2"><p>که بپاسخ حجتی در خور ندید</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>شامئی را گفت ساز جنگ کن</p></div>
<div class="m2"><p>سوی رزم این صبی آهنگ کن</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>گفت شامی ننگ باشد در نبرد</p></div>
<div class="m2"><p>کافکند با کودکی پیکار مرد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>خود تو دانی که مرا مردان کار</p></div>
<div class="m2"><p>بکستنه همسر شمارد با هزار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>دارم اینک چار فرزند دلبر</p></div>
<div class="m2"><p>هر یکی در جنگ زاوی شیر گیر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>نک روان دادم یکی بر جنگ او</p></div>
<div class="m2"><p>با همین از چهره شویم ننگ او</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>گفت اینان زادگان حیدرند</p></div>
<div class="m2"><p>در شجاعت وارث آنسرورند</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>خردسال ار بینیش خرده مگیر</p></div>
<div class="m2"><p>که ز مادر شیر زاید زاد شیر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>از طراز چرخ بودی جوشنش</p></div>
<div class="m2"><p>گر بخردی تن بر این دادی تنش</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>این شررها کز نژاد آتشند</p></div>
<div class="m2"><p>خرمنی هر لحظه در آتش کشند</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>نسل حیدر جملگی عمرو افکنند</p></div>
<div class="m2"><p>که به نسبت خوشه آنخرمنند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>آنکه از پستان شیری خورد شیر</p></div>
<div class="m2"><p>گر چه خرد آمد شجاع است و دلیر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>گر نبودی منع زنجیر قضا</p></div>
<div class="m2"><p>تنگ بودی بر دلبریشان فضا</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>داد شاهی از سیه بختی جواز</p></div>
<div class="m2"><p>پور را بر حرب آنماه حجاز</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>شاهزاده راند باره سوی او</p></div>
<div class="m2"><p>یافت ناگه دست بر گیسوی او</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>موکشان بربود از زین پیکرش</p></div>
<div class="m2"><p>داد جولان در مصاف لشگرش</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>آنچنانش بر زمین کوبید سخت</p></div>
<div class="m2"><p>کاستخوان با خاک یکسان گشت و پخت</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>هم یکایک آنسه دیگر زاد وی</p></div>
<div class="m2"><p>رو بمیدانگه نهاد او را زپی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>در نخستین حملۀ آن میر راد</p></div>
<div class="m2"><p>پای پیکارش نماند و سر نهاد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ساکنان ذروۀ عرش برین</p></div>
<div class="m2"><p>زآسمان خواندند بر وی آفرین</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>شامی آمد با رخ افروخته</p></div>
<div class="m2"><p>دل ز داغ سوگواری سوخته</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اهرمن چون با فرشته شد قرین</p></div>
<div class="m2"><p>کرد رو بر آسمان سلطان دین</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>کایمهین یزدان پاک ذوالمنن</p></div>
<div class="m2"><p>این فرشته چیره کن بر اهرمن</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>لب بهم ناورده شه سبط کریم</p></div>
<div class="m2"><p>کرد شاهیرا بیک ضربت دو نیم</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>زانچنان دعوت نبود این بس عجیب</p></div>
<div class="m2"><p>بود عاشق صوت داغیرا مجیب</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ایخوش آنصوتی که او جویای اوست</p></div>
<div class="m2"><p>رأی این در هر چه خواهد رأی اوست</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>نی معاذ الله خطا رفت ای عجیب</p></div>
<div class="m2"><p>صوت داعی بود خود صوت مجیب</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>داند آن کز سرّ عشق آگه بود</p></div>
<div class="m2"><p>کاین همه آوازها از شه بود</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>رو حدیث کنت سمعه باز خوان</p></div>
<div class="m2"><p>تا بیابی رمز این سرّ نهان</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>شد چو از تیغش دو نیم آنرزم کوش</p></div>
<div class="m2"><p>مرحبا آمد ز یزدانش بگوش</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>تافت شهزاده عنان از رزمگاه</p></div>
<div class="m2"><p>شکوه بر لب از عطش تا نزد شاه</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>دید چون خوشیده یاقوت ترش</p></div>
<div class="m2"><p>بر دهان بنهاد شاه انگشترش</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>در صدف گفتی نهان شد گوهری</p></div>
<div class="m2"><p>یا هلالی شد قرین مشتری</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>کرد آگاهش ز رمز عشق شه</p></div>
<div class="m2"><p>بر دهانش مهر زد یعنی که مه</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چشمۀ جوشید از آن چون سلسبیل</p></div>
<div class="m2"><p>زندگی بخش دو صد خضر دلیل</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چون لب لعلش از او سیراب شد</p></div>
<div class="m2"><p>تشنۀ دیدار جد و باب شد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>تاخت سوی رزمگه با صد شتاب</p></div>
<div class="m2"><p>باد پا چون تشنه مستعجل بر آب</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>شیر بچه تیغ مردافکن بمشت</p></div>
<div class="m2"><p>کشت از آنروباه مردان آنچه کشت</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>حیدرانه تیغ در لشکر نهاد</p></div>
<div class="m2"><p>پشته ها از کشته ها ترتیب داد</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ظالمی زد ناگهش تیغی بفرق</p></div>
<div class="m2"><p>تن ز زین برگشت در خونگشت غرق</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>نوعروس از غم گریبان چاک کرد</p></div>
<div class="m2"><p>فاطمه در خلد بر سر خاک کرد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>کرد رو با شیر حق کی داورم</p></div>
<div class="m2"><p>وقت آن آمد که آئی بر سرم</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>زد فلک در ئیل رخت شادیم</p></div>
<div class="m2"><p>خاک و خون شد حجله دامادیم</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>شاه دین آمد ببالین حبیب</p></div>
<div class="m2"><p>دید دامادی دو دست از خون خضیب</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>سر بریدنرا ستاده بر سرش</p></div>
<div class="m2"><p>قاتلی در دست خونین خنجرش</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>دست او افکند یا تیغی ز دوش</p></div>
<div class="m2"><p>لشگر از فریاد او آمد بجوش</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>زد به لشگر شاه دین با تیغ تیز</p></div>
<div class="m2"><p>گرم شد هنگامۀ جنگ و گریز</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>پیکر آن تازه داماد گزین</p></div>
<div class="m2"><p>شد لگدکوب ستور اهل کین</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>شه چو آمد بار دیگر بر سرش</p></div>
<div class="m2"><p>دید با حالی دگرگون پیکرش</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>برک برگ نوگل باغ هدی</p></div>
<div class="m2"><p>از سموم کین شده از هم جدا</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>گفت با صد حسرت و خون جگر</p></div>
<div class="m2"><p>کایهمایون فال و فرخ رخ پسر</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>قاتلانت در دو عالم خوار باد</p></div>
<div class="m2"><p>خصم شان پیغمبر مختار باد</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>سخت صعب آید بعمت زندگی</p></div>
<div class="m2"><p>که تواش خوانی که درماندگی</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>بهر یاری تو برتابد فرود</p></div>
<div class="m2"><p>یا نه بخشد بر تو آن یاریش سود</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>پس کشیدش بر کنار از لطف شاه</p></div>
<div class="m2"><p>برد نالانش بسوی خیمه گاه</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>گفت مهلا ایعزیزان گزین</p></div>
<div class="m2"><p>که هوان واپسین ماست این</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>یارب این قوم سیه دل خوار باد</p></div>
<div class="m2"><p>بر جبینشان داغ ننگ و عار باد</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>ایجهان داور ملائک هفت و چار</p></div>
<div class="m2"><p>وانمان دیار از ایشان در دیار</p></div></div>