---
title: >-
    بخش ۳۲ - کیفیت روز سیم از شهادت موافق حدیث مروی از حضرت ابی عبدالله علیه السلام
---
# بخش ۳۲ - کیفیت روز سیم از شهادت موافق حدیث مروی از حضرت ابی عبدالله علیه السلام

<div class="b" id="bn1"><div class="m1"><p>سیم عاشور چون شمع افق</p></div>
<div class="m2"><p>سر نهفت اندر پس نیلی تنق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه بقربانگاه دشت کربلا</p></div>
<div class="m2"><p>بر نشست و کشتگانرا زد صلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مسیحا دم بر آن ابدان دمید</p></div>
<div class="m2"><p>شام ماتم شد از آندم صبح عید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی که بعد آسمانست و زمین</p></div>
<div class="m2"><p>از مسیحا تا مسیحا آفرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نبودی روح او عیسی نبود</p></div>
<div class="m2"><p>روح قدس و مریم عذرا نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بگهواره سخن گفتی مسیح</p></div>
<div class="m2"><p>خود گواهی بود آن نطق فصیح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاین همه آوازها از شه بود</p></div>
<div class="m2"><p>گر چه از حلقوم عبدالله بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قصووی رفت در تعبیر من</p></div>
<div class="m2"><p>خرده گیران گو مکن تعبیر من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کتاب خود خداوند اجل</p></div>
<div class="m2"><p>نور خدا را زد ز مشکوتی مثل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود همان نور است آن سبط ذبیح</p></div>
<div class="m2"><p>در مثل مشکوه او روح مسیح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این سخن پایان ندارد لب به بند</p></div>
<div class="m2"><p>گفت با یاران خدیو ارجمند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کای براه عشق ما سر دادگان</p></div>
<div class="m2"><p>دل ز قید جسم و دل آزادگان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>والیان هفت اقلیم بلا</p></div>
<div class="m2"><p>یوسفان مصر الله اشتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تشنگان ساغر لبریز عشق</p></div>
<div class="m2"><p>سرخوشان بزم شورانگیز عشق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرده سلطان ازل مهمانتان</p></div>
<div class="m2"><p>هین فرود آئید از ابدانتان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کبریا بینید بزم انبساط</p></div>
<div class="m2"><p>اندرو گسترده گوناگون بساط</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تشنه جان دادید گر در کوی ما</p></div>
<div class="m2"><p>نک بنوشید آب خضر از جوی ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنچه کم کرد از شما جیش یزید</p></div>
<div class="m2"><p>باز پس گیرید از ما بر مزید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از دم جان بخش او ارواح پاک</p></div>
<div class="m2"><p>سر برآوردند چون گلبن ز خاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زاهتزاز آن نسیم مشگبو</p></div>
<div class="m2"><p>آبهای رفته باز آمد بجو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر شگفت از خاک تنها بعد مرگ</p></div>
<div class="m2"><p>همچو در فصل بهاران لاله برک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر برآوردند از کهف آنرقود</p></div>
<div class="m2"><p>یک بیک بردند پیش شه سجود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گلشنی دیدند پر نقش و نگار</p></div>
<div class="m2"><p>سرو و گل در وی قطار اندر قطار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نفخۀ جان بخش آن سبط سلیل</p></div>
<div class="m2"><p>کرده آتش را گلستان خلیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس بامر داور عیسی سرشت</p></div>
<div class="m2"><p>بهرشان آمد سماطی از بهشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سانکینها در وی از خمر طهور</p></div>
<div class="m2"><p>ساقی لب تشنگان رب غفور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر حواریین شد آن قربان گده</p></div>
<div class="m2"><p>عیدگاهی از نزول ما مده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با تلطف شاه ذوالا کرامشان</p></div>
<div class="m2"><p>کرد از آن خوان طعام اطعامشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان سپس کانعاشقان مهر کیش</p></div>
<div class="m2"><p>شد برخصت سوی منزلگاه خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سوی رضوی باز شد سبط زکی</p></div>
<div class="m2"><p>شد بدیهیم خلافت متکی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ما سوی الله گوش بر فرمان او</p></div>
<div class="m2"><p>آیۀ وجه اللهی در شان او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>موسی و عیسی و ابراهیم راد</p></div>
<div class="m2"><p>با گروه انبیای بار شاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کرد تخت آن ملیک مقتدر</p></div>
<div class="m2"><p>جمله گی فرمان او را منتظر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از قفای انبیای مرسلین</p></div>
<div class="m2"><p>روح پاک شیعیان پاک دین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زان سپس خیل ملائک صف بصف</p></div>
<div class="m2"><p>همچو انجم گرد آن قطب شرف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چشم بر فرمان و گوشش بر خطاب</p></div>
<div class="m2"><p>تا چه فرماید خدیو مستطاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هست بر تخت خلافت مستقر</p></div>
<div class="m2"><p>همچنان تا روز عدل منتظر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون ز پشت پرده آید در ظهور</p></div>
<div class="m2"><p>طلعت آن مظهر الله نور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آید آن سلطان اقلیم ولا</p></div>
<div class="m2"><p>بار دیگر بر زمین کربلا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جانسپارانش زده بر وی پره</p></div>
<div class="m2"><p>چون بدور نقطۀ خط دایره</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ساکنان هفت ارض و نه سما</p></div>
<div class="m2"><p>رو نهد در ظل آن فرخ هما</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جمله گرد آیند در پیراهنش</p></div>
<div class="m2"><p>برده دست التجا بر دامنش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پس بقول صادق آل خلیل</p></div>
<div class="m2"><p>آیدی زائر خداوند جلیل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>میکند آنکه تصافح بی حجیب</p></div>
<div class="m2"><p>با دو دست خود حبیبی یا حبیب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پس شود با حضرت عرش آفرین</p></div>
<div class="m2"><p>بر سریر لی مع اللهی مکین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با جهولان این حدیث ذوشجون</p></div>
<div class="m2"><p>گوش گاو است و صدای ارغنون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جاهلا اشراق وجدانیست این</p></div>
<div class="m2"><p>منطق الطیر سلیمانی است این</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ذات بیچون در خور دیدار نیست</p></div>
<div class="m2"><p>واندر ان فرگاه کس را بار نیست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رو فرو خوان ثم وجه الله را</p></div>
<div class="m2"><p>تا نبوئی بی چراغ این راه را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حق نهان در پرده وجهش مظهر است</p></div>
<div class="m2"><p>گر چه او خودروی از وی اظهر است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ظلمت اسکندر است این ممکنات</p></div>
<div class="m2"><p>وجه باقی اندر و آب حیات</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ظلمت امکان چو گردد غرق نور</p></div>
<div class="m2"><p>وجه باقی بردمد از جیب طور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لیک این غرق فنا وجدانی است</p></div>
<div class="m2"><p>نی حدیث صوفی خرقانی است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون قتیله محو عشق نار شد</p></div>
<div class="m2"><p>نار را آئینه دیدار شد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>لیک دیداری نه دیدار شهود</p></div>
<div class="m2"><p>محو وجدان فرق دارد تا وجود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>صوفی ما را چو این وجدان نبود</p></div>
<div class="m2"><p>فرق وجدانرا نشانست از وجود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چست بر جست و دم اندر بوق کرد</p></div>
<div class="m2"><p>خوبش گه عاشق گهی معشوق کرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گفت غیری نیست جز من در دیار</p></div>
<div class="m2"><p>خویش با خود عشق میبازد نگار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ژاژ کمتر خای عمرت بر مزید</p></div>
<div class="m2"><p>ای جناب خواجه سلطان با یزید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>لن ترانی گفت ایزد با کلیم</p></div>
<div class="m2"><p>تا بیفتد در طمع عبدالنعیم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>حفظ فصل و وصل با هم بایدت</p></div>
<div class="m2"><p>تا از آن توحید مطلق زایدت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آنکه جمع از فرق نشناسد درست</p></div>
<div class="m2"><p>ره بخلوتخانۀ عرفان نجست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>این سخن پایان ندارد ای عمید</p></div>
<div class="m2"><p>قصه کوته کن که شد مقصد بعید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زائر آئینه وجه باقی است</p></div>
<div class="m2"><p>کان نهان را جلوۀ اشرافی است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شه چو از اوج تجرد شد فرو</p></div>
<div class="m2"><p>زان سفر کردی نشست او را برو</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>باز چون بر شد سوی معراج عشق</p></div>
<div class="m2"><p>دست حق بر سر نهادش تاج عشق</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شد غبار از چهرۀ آئینه دور</p></div>
<div class="m2"><p>دست با هم دارد زائر با مزور</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>وانغبارش پردۀ اغیار بود</p></div>
<div class="m2"><p>ورنه او دائم قرین یار بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>من چه گویم که کم دمساز نیست</p></div>
<div class="m2"><p>گوشها پهن است اما باز نیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کی حبیبی دور ماند از حبیب</p></div>
<div class="m2"><p>رو فرو خوان در بُنی انی قریب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آنکه در بحر فنا مستغرق است</p></div>
<div class="m2"><p>تا بود حق با وی و وی با حق است</p></div></div>