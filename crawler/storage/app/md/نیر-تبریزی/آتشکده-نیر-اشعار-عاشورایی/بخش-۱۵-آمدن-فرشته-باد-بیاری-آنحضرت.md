---
title: >-
    بخش ۱۵ - آمدن فرشتهٔ باد بیاری آنحضرت
---
# بخش ۱۵ - آمدن فرشتهٔ باد بیاری آنحضرت

<div class="b" id="bn1"><div class="m1"><p>شد فرشته نار با صد دود آه</p></div>
<div class="m2"><p>که فرشته باد آمد با سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت کای دارندۀ چرخ بلند</p></div>
<div class="m2"><p>بر جفا صبر و تحمل تا بچند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سزد چون که رباید اهرمن</p></div>
<div class="m2"><p>خاتم از دست سلیمان زمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینک آوردستم از امر اله</p></div>
<div class="m2"><p>سوی تو ایشاه بک هامون سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکم کن تا باد را فرمان کنیم</p></div>
<div class="m2"><p>ملک شام و کوفه را ویران کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکم کن تا بر کنیم اینک ز بن</p></div>
<div class="m2"><p>بیخ کفر ای تاجدار امر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت میماند که بر دستی ز باد</p></div>
<div class="m2"><p>ایفرشته داستان قوم عاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورنه خود دانی که در پرده که بود</p></div>
<div class="m2"><p>انکه کند از بن دیار قوم هود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاجزان دستار این امداد را</p></div>
<div class="m2"><p>باد در دست است اینجا باد را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو گواهند این مسا و این صباح</p></div>
<div class="m2"><p>که بدست ماست تسخیر رباح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنسلیمان کش مسخر گشت باد</p></div>
<div class="m2"><p>نام مادر نقش انگشرت نهاد</p></div></div>