---
title: >-
    بخش ۱ - بسم الله الرحمن الرحیم
---
# بخش ۱ - بسم الله الرحمن الرحیم

<div class="b" id="bn1"><div class="m1"><p>آفرینش را چو فتح الباب شد</p></div>
<div class="m2"><p>نور احمد مهر عالم تاب شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رست از او نور امامان وفی</p></div>
<div class="m2"><p>شد بروج سیر آن نور صفی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس برآمد نور پاک فاطمه</p></div>
<div class="m2"><p>آن مبارک فاتحت را خاتمه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چارده هیکل چو شد از وی درست</p></div>
<div class="m2"><p>نور پاک انبیا زان نور رست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس بترتیب مراتب زان صور</p></div>
<div class="m2"><p>شد همه ذرات اکوان جلوه گر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آری آری طلعت الله نور</p></div>
<div class="m2"><p>این چنین آئینۀ دارد ضرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون پدید آرندۀ بالا و پست</p></div>
<div class="m2"><p>آزمایش خواست از قول الست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر بلی و لا زبانها باز شد</p></div>
<div class="m2"><p>نوری و ناری ز هم ممتاز شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوریان مأوی به علیین گرفت</p></div>
<div class="m2"><p>ناریان جا در تک سجین گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناگهان پیک خداوند جلیل</p></div>
<div class="m2"><p>در نفوس افکند صیت الرحیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت کی مرغان بستان الست</p></div>
<div class="m2"><p>هین فرود آئید از بالا به پست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از بیابان تجرد خم زنید</p></div>
<div class="m2"><p>خیمه در آب و گل آدم زنید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کشتزار است اینچنین این خاک و آب</p></div>
<div class="m2"><p>دانه فعل این نفوس مستطاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا نپاشد دانه را در آب و گل</p></div>
<div class="m2"><p>برزگر وقت درو ماند خجل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا نکارد تخم را در آب و خاک</p></div>
<div class="m2"><p>برنچیند باغبان از نخل و تاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نگیرد عکس در آئینه جا</p></div>
<div class="m2"><p>کس نیابد زو نشان اندر هوا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بدیواری نتابد آفتاب</p></div>
<div class="m2"><p>پرتو او کس نبیند جز بخواب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس نفوس از زیر و بالا پر گشود</p></div>
<div class="m2"><p>جمله در چاه طبیعت شد فرود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در حضیض چه شکست آن بال و پر</p></div>
<div class="m2"><p>که پریدندی بدان در اوج ذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون عجین طینت زیبا و زشت</p></div>
<div class="m2"><p>دست سلطان ازل در هم سرشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد دفین آن شمع های مشتعل</p></div>
<div class="m2"><p>در شبستان مزاج آب و گل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون هیولا شد مصور یا صور</p></div>
<div class="m2"><p>هر یک از مشکوة خود شد جلوه گر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لیک طبع اختلاط آن سرشت</p></div>
<div class="m2"><p>شد مؤثر در مزاج خوب و زشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نور و ظلمت چون بهم آمد قرین</p></div>
<div class="m2"><p>این از آن رنگی پذیرفت آن از این</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لاجرم در طبع احرار و عبید</p></div>
<div class="m2"><p>شد تقاضای تبه کاری پدید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس ندا آمد ز اوج کبریا</p></div>
<div class="m2"><p>با گروه انبیا و اوصیا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کای گروه منهیان با شکوه</p></div>
<div class="m2"><p>این سیه روئی که شوید زین وجوه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر نیامد این ندا را کس مجیب</p></div>
<div class="m2"><p>جز قتیل حق حبیب ابن الحبیب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن خلیل حلم و ایوب بلا</p></div>
<div class="m2"><p>نوح طوفان و حسین کربلا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زانکه از ارکان عرش استوا</p></div>
<div class="m2"><p>رکن عقل از نور احمد شد بپا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رکن روح از نور پاک مرتضی</p></div>
<div class="m2"><p>حکمت آموز دبستان قضا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رکن نفسی قائم از نور حسن</p></div>
<div class="m2"><p>رکن طبعی از حسین ممتحن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون در اینجا بود خلط طینتین</p></div>
<div class="m2"><p>می نبود آنجا بجز ذکر حسین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کاوست رب النوع این رکن وثیق</p></div>
<div class="m2"><p>قصه کوته به که شد معنی دقیق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این سخن در خورد فهم شام نیست</p></div>
<div class="m2"><p>راه عشق است این ره حمام نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت حق کای شافع خرد و بزرگ</p></div>
<div class="m2"><p>این شفاعت راست شرطی بس سترگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر که در این ره فنا فی الله نشد</p></div>
<div class="m2"><p>بر سریر جرم بخشی شه نشد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بایدت در راه دین ای مقتدا</p></div>
<div class="m2"><p>کرد جان بهر گنهکاران فدا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شست از فرزند و مال و عز و جاه</p></div>
<div class="m2"><p>دست تا باشی ضعیفان را پناه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آفتابا هین ز شرق نیزه سر</p></div>
<div class="m2"><p>باز کش کاین ظلمت آید مستتر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دست از دست برادر شوی چیر</p></div>
<div class="m2"><p>وین ز پا افتادگان را دست گیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پیکر فرزند کن در خون غریق</p></div>
<div class="m2"><p>می نشان از آتش دوزخ حریق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شیر بر اصغر ده از پستان تیر </p></div>
<div class="m2"><p>تشنگان را کن ز جوی شیر سیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر کف داماد از خون نِه خضاب</p></div>
<div class="m2"><p>نقش جرم عاصیان میزن بر آب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پای بیمارت بغل چون بنده کن</p></div>
<div class="m2"><p>ای مسیحا مردگان را زنده کن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خواهران و دختران میده اسیر</p></div>
<div class="m2"><p>وین اسیران را رها کن از سعیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>باز زن بر خیمه آتش ای سلیل</p></div>
<div class="m2"><p>می بکن آتش گلستان بر خلیل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هین بران کشتی بخون در کربلا</p></div>
<div class="m2"><p>نوح را برهان ز طوفان بلا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تشنه لب باز آی بیرون از فرات</p></div>
<div class="m2"><p>ده هزاران خضر را آب حیات</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>منجی افتادگان در چه توئی</p></div>
<div class="m2"><p>خون بدست آور که ثار الله توئی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پشت پای لابنه خرگاه زن</p></div>
<div class="m2"><p>خیمه در صحرای الا الله زن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>غرقه درخون با تن صدپاره باش</p></div>
<div class="m2"><p>بر گناه مجرمان کفاره باش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کاین چنین خونی بیاید ای همام</p></div>
<div class="m2"><p>تا کند این ناتمامان را تمام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قلب اکوانی تو در خون باش غرق</p></div>
<div class="m2"><p>خاک ماتم زیر عالم را بفرق</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کاین سیه روئی ز افراد بشر</p></div>
<div class="m2"><p>می نشوید غیر آب چشم تر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گفت آن شاه سریر ارتضا</p></div>
<div class="m2"><p>کانچه گفتی جمله را دارم رضا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ترک مال و ترک جان و ترک اهل</p></div>
<div class="m2"><p>چون توئی جانان بسی سهل است سهل</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من خود از خود نیستم زان تو ام</p></div>
<div class="m2"><p>هر چه گوئی بنده فرمان توام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>باده ام خونست و ساقی دست عشق</p></div>
<div class="m2"><p>مست عشقم مست عشقم مست عشق</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گفت ایزد کای شه احمد سرشت</p></div>
<div class="m2"><p>عهد خود را نامه ای باید نوشت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پس نوشت او نامۀ با دست خویش</p></div>
<div class="m2"><p>مهر بر وی برنهاد و داشت بیش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جدّ و باب و مام فرزندان راد</p></div>
<div class="m2"><p>مر گواهی را بر او خاتم نهاد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گفت حق کای شمع بزم روشنم</p></div>
<div class="m2"><p>شاد زی که خون بهای تو منم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هر چه در پاداش این عهد درست</p></div>
<div class="m2"><p>خواهی از ما خواه یکسر زان تست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گفت شه صادق نیم ای ذوالمنن</p></div>
<div class="m2"><p>در وفا گر از تو خواهد جز تو من</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پس سپرد آن عهد ز آن بزم بلا</p></div>
<div class="m2"><p>عاشقانه راند سوی کربلا</p></div></div>