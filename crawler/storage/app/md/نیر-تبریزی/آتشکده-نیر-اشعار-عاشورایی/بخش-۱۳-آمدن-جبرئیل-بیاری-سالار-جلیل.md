---
title: >-
    بخش ۱۳ - آمدن جبرئیل بیاری سالار جلیل
---
# بخش ۱۳ - آمدن جبرئیل بیاری سالار جلیل

<div class="b" id="bn1"><div class="m1"><p>جبرئیل آمد شتابان بر زمین</p></div>
<div class="m2"><p>از فراز عرش رب العالمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید صحرائی سراسر لاله زار</p></div>
<div class="m2"><p>ارغوان در وی قطار اندر قطار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهره های آتشین برگ گلشن</p></div>
<div class="m2"><p>زلفهای عنبر افشان سنبلش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چوبها در وی روان اما ز خون</p></div>
<div class="m2"><p>سروهای برلب اما سرنگون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه های تاشده از آب سیر</p></div>
<div class="m2"><p>اندر و خندان ولی از زخم تیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم نرگس رفته از مستی ز هوش</p></div>
<div class="m2"><p>سوسنان باده زبان در وی خموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عندلیبان اندر آن بستان کده</p></div>
<div class="m2"><p>در فغان هر سو رَدَه اندر رده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت کایفرمانده ملک وجود</p></div>
<div class="m2"><p>پیشت آور دستم از یزدان درود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت بر گو ای برید کوی یار</p></div>
<div class="m2"><p>تا به پیغامش کنم صد جان نثار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت فرمودت که ای سالار عشق</p></div>
<div class="m2"><p>ای ز تو بالا گرفته کار عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نبودی بود تو عالم نبود</p></div>
<div class="m2"><p>امتزاج طینت آدم نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خود توئی مقصود از خلق عباد</p></div>
<div class="m2"><p>بیتو عالم را بر کو خاک باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما نکردیم این شهادت بر تو ختم</p></div>
<div class="m2"><p>ایجلال کبریائی بر تو ختم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عزم تو بس در وفای عهد تو</p></div>
<div class="m2"><p>شد نیت قائم مقام عهد تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس ترا در خون طپیدن اکبرت</p></div>
<div class="m2"><p>خون بجای شیر خوردن اصغرت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواه کش خه کشته باش ایشاه عشق</p></div>
<div class="m2"><p>هیچ کم تاید ترا از جاه عشق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواه جان بستان و خه جان میسپار</p></div>
<div class="m2"><p>یار آن یار است و مهر آن مهر یار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر کشی جان جهان نک زان تست</p></div>
<div class="m2"><p>گوش عزرائیل بر فرمان تست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کشته گردی بر شهیدان شه توئی</p></div>
<div class="m2"><p>خون بپایت ما ذبیح الله توئی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>داد پاسخ شاه با روح الامین</p></div>
<div class="m2"><p>کای امین وحی رب العالمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسته ایم عهدی من و شاه وجود</p></div>
<div class="m2"><p>من همانم عهد آنعهدیکه بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عاشق جانانه را با جان چه کار</p></div>
<div class="m2"><p>درد کز یار است با درمان چکار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جبرئیلا اینکه بینی نی منم</p></div>
<div class="m2"><p>اوست یکسر من همین پیراهنم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زو فرودم آنچه از خود کاستم</p></div>
<div class="m2"><p>من خود این آتش بجان میخواستم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر من از هر دو جهان بیگانه ام</p></div>
<div class="m2"><p>گنج پنهانی است در ویرانه ام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت شاها خواهرانت بیکس است</p></div>
<div class="m2"><p>گفت او خود بیکسانرا مونس است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت چشم دخترانت در ره است</p></div>
<div class="m2"><p>گفت عشق از دیدن غیراکمه است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت ترسم زینبت گردد اسیر</p></div>
<div class="m2"><p>گفت بیماریش خوشدارد حبیب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت بهرت آب حیوان آورم</p></div>
<div class="m2"><p>گفت من از تشنگی آنسوترم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جبرئیلا من ز جو بگذشته ام</p></div>
<div class="m2"><p>آب حیوانرا در آنسو هشته ام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفت خواهد شد سرت زیب سنان</p></div>
<div class="m2"><p>گفت گو باش او چه میخواهد چنان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت جان باشد متاعی بس گران</p></div>
<div class="m2"><p>برخسان مفروش یوسف رایگان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفت جانیرا که جانان خونبهاست</p></div>
<div class="m2"><p>جبرئیلا رایگان خواندن خطاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفت آورد دستم از غیبت سپاه</p></div>
<div class="m2"><p>تا کنند این قوم کافر دل تباه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت مهلاً خود ز من دارد مدد</p></div>
<div class="m2"><p>جبرئیلا این سپاه بیعدد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هستی ایشان همه از هست ماست</p></div>
<div class="m2"><p>رشتۀ تدبیرشان در دست ماست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آنکه با تدبیر او گردد فلک</p></div>
<div class="m2"><p>کی بود محتاج امداد ملک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر فشانم دست ریزم زاستین</p></div>
<div class="m2"><p>صد هزاران جبرئیل راستین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جبرئیلا باب من بودت ممد</p></div>
<div class="m2"><p>که شدی حق را بپاسخ مستعد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آنزمان کت آفرید از نیستی</p></div>
<div class="m2"><p>گفت برگو من کیم تو کیستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سالها ماندی تو حیران در جواب</p></div>
<div class="m2"><p>کرد تعلیمت در آخر بوتراب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت بر گو تو خداوند جلیل</p></div>
<div class="m2"><p>من کمین عبد تو نامم جبرئیل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جبرئیلا من خلیفه آن شهم</p></div>
<div class="m2"><p>وارث اسرار آن باب اللهم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آن ستاره کت نمود آنمه جبین</p></div>
<div class="m2"><p>دیده بگشا در جبین من ببین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جبرئیلا چشم دیگر بایدت</p></div>
<div class="m2"><p>تا که حال عاشقان بنمایدت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جبرئیلا من خود از کف هشته ام</p></div>
<div class="m2"><p>دست جانانست تار رشته ام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هشته طوق عشق خود بر گردنم</p></div>
<div class="m2"><p>میبرد آنجا که خواهد بردنم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اینحدیث محنت ایّوب نیست</p></div>
<div class="m2"><p>داستان یوسف و یعقوب نیست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>صبر ایّوب از کجا و این بلا</p></div>
<div class="m2"><p>این حسین است و حدیث کربلا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دورکش زینورطه رخت ایمحتشم</p></div>
<div class="m2"><p>تا نسوزد شهپرت را آتشم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هین سپاهت دور دار از راه من</p></div>
<div class="m2"><p>که جهان سوز است برق آه من</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شد بسوی آسمان آن روح پاک</p></div>
<div class="m2"><p>که فرشتۀ آتش آمد سوزناک</p></div></div>