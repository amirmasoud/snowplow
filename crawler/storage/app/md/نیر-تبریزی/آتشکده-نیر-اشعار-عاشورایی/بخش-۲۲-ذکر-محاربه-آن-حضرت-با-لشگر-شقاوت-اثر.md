---
title: >-
    بخش ۲۲ - ذکر محاربهٔ آن حضرت با لشگر شقاوت اثر
---
# بخش ۲۲ - ذکر محاربهٔ آن حضرت با لشگر شقاوت اثر

<div class="b" id="bn1"><div class="m1"><p>ماند چون تنها بمیدان شاه دین</p></div>
<div class="m2"><p>غلغله افتاد بر چرخ برین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد از گردون فرود ارواح پاک</p></div>
<div class="m2"><p>بر نظاره آن جمال تابناک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد ممثل بر گروه مشرکین</p></div>
<div class="m2"><p>هیکل توحید رب العالمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدسیانرا شد مصور در خیال</p></div>
<div class="m2"><p>که مجسم گشته ذات ذو الجلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نبودی صیحۀ هل من معین</p></div>
<div class="m2"><p>آن گمان گشتی مبدل بر یقین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد یکایک سوی شاه شیر گیر</p></div>
<div class="m2"><p>یکهزار و نهصد و پنجه دلیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در نخستین ضربتش سر باختند</p></div>
<div class="m2"><p>با دو نیمه تن جهان پرداختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت زاد سعد باطیش و تعب</p></div>
<div class="m2"><p>ویحکم هذابن قتال العرب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سفلۀ را کش خصال روبهی است</p></div>
<div class="m2"><p>پنجه با شیران نمودن ز ابلهی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاصه شیرانی که زاد حیدرند</p></div>
<div class="m2"><p>با شجاعت زادۀ یکمادرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هین فرود آئید یکسر گرد او</p></div>
<div class="m2"><p>تیر بارانش کنید از چارسو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشرکان رو بسوی وجه الله کرد</p></div>
<div class="m2"><p>تیره ابری رو بسوی ماه کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زد پره بروی خان با طبل و کوس</p></div>
<div class="m2"><p>چون بگرد شعلۀ آتش مجوس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد پر مرغان تیر تیز پر</p></div>
<div class="m2"><p>چونسلیمان سایه گردانش بسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جنبش و جیش و غریو و هلهله</p></div>
<div class="m2"><p>او فکند اندر بیابان غلغله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چه بر وس سخت تر گشتی نبرد</p></div>
<div class="m2"><p>رخ ز شوقش سرختر گشتی چو ورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آری آری عشق را اینست حال</p></div>
<div class="m2"><p>چونشود نزدیک هنگام وصال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شیر حق با ذوالفقار حیدری</p></div>
<div class="m2"><p>برد حمله بر جنود خیبری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از شرار تیغ او چون رستخیز</p></div>
<div class="m2"><p>شد مجسم دوزخی دشت ستیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسکه شد لبریز ز اعوان یزید</p></div>
<div class="m2"><p>شد خموش از نعرۀ هل من مزید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرد طومار اجل یکباره طی</p></div>
<div class="m2"><p>صیحۀ مت یا عدو الله وی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا نظر میبرد چشم روزگار</p></div>
<div class="m2"><p>بود دشتی پر حسین و ذو الفقار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تاختی هر سو گروه کفر کیش</p></div>
<div class="m2"><p>میدویدی تیغ او صد کام پیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رسته گفتی بر سر هر کافری</p></div>
<div class="m2"><p>حیدری با ذوالفقار دیگری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بسکه خون بارید ز ابر تیغ تیز</p></div>
<div class="m2"><p>بر اجلها بسته شد راه گریز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قدسیان بر حال او گریان همه</p></div>
<div class="m2"><p>لب گران انگشت بر دندان همه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کایدریغ این شاه که بی لشگر است</p></div>
<div class="m2"><p>لب ز آبش خشک و چشم از خون تر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه حبیبی و نه مسلم نه زهیر</p></div>
<div class="m2"><p>نه ریاحی و نه عابس نه بریر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه علیّ اکبر و نه قاسمی</p></div>
<div class="m2"><p>نه علمدار آن جوان هاشمی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ایدریغ این دست و ساعد کش به تیغ</p></div>
<div class="m2"><p>ساربان خواهد بریدن بیدریغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ایدریغ این سر که با تیغ جفا</p></div>
<div class="m2"><p>یکدم دیگر برندش از قفا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ایدریغ این تن که خواهد شد سحیق</p></div>
<div class="m2"><p>آخر از سم ستور این فریق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ایدریغ این بانوان با شکوه</p></div>
<div class="m2"><p>که بخواهد هشت سر در دشت و کوه</p></div></div>