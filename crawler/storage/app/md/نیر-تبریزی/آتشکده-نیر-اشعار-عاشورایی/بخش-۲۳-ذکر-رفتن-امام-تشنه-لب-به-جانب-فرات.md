---
title: >-
    بخش ۲۳ - ذکر رفتن امام تشنه‌لب به جانب فرات
---
# بخش ۲۳ - ذکر رفتن امام تشنه‌لب به جانب فرات

<div class="b" id="bn1"><div class="m1"><p>شد چوشاه تشنه نومید از حیات</p></div>
<div class="m2"><p>تافت رو از حریکه سوی فرات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدرنگ از تشنه کامی مرکبش</p></div>
<div class="m2"><p>خواست تا آبی رساند بر لبش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت باری بس گران داری بدوش</p></div>
<div class="m2"><p>ای یراق عرش پیما آبنوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش بخور خوش گر چه هر دو تشنه ایم</p></div>
<div class="m2"><p>خستۀ زخم و خدنک و دشنه ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنسمند تیز هوش از روی شاه</p></div>
<div class="m2"><p>شرمساری برد مانا زینگناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرکشید از آب یعنی کایهمام</p></div>
<div class="m2"><p>بیتو بر من آب خوش بادا حرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه را رقت بدان مرکب گرفت</p></div>
<div class="m2"><p>جرعۀ آبی به پیش لب گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کافری تیری رها کرد از کمان</p></div>
<div class="m2"><p>وه چگویم خاک بادا بر دهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا رسیده بر لب نوشینش آب</p></div>
<div class="m2"><p>شد پر از بیچاده درج لعل ناب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با لب خشک و دهان پر ز خون</p></div>
<div class="m2"><p>امد آنشه گوهر از دریا برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد چو شیر شرزه سوی رزمگاه</p></div>
<div class="m2"><p>حیدرانه برد حمله بر سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نهیب نعره های صف شکر</p></div>
<div class="m2"><p>شد فلک بر صیحۀ ابن المفر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرم پیکار آنخدیو عشق کیش</p></div>
<div class="m2"><p>که گرفتندش صحیفه عهد پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آمد از هاتف بگوش او ندا</p></div>
<div class="m2"><p>از حجاب بارگاه کبریا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کایحسین ای نوح طوفان بلا</p></div>
<div class="m2"><p>این همان عهد است و اینجا کربلا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو بدین رو که کی جنگ آوری</p></div>
<div class="m2"><p>پس که خواهد شد بلا را مشتری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیغ اگر اینست و بازو اینکه هست</p></div>
<div class="m2"><p>در ره ما پس که خواهد داد دست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو بدین نیرو که تازی بر سپاه</p></div>
<div class="m2"><p>جان که خواهد داد جانانرا براه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هین فرود آ ایشه پیمان درست</p></div>
<div class="m2"><p>که بساط کبریائی زان تست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مصطفی و مرتضی و فاطمه</p></div>
<div class="m2"><p>چشم بر راهند با حوران همه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ایحریم وصل ما ماوای تو</p></div>
<div class="m2"><p>اندرا خالی است اینجا جای تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مغز را برگیر و ترک پوست کن</p></div>
<div class="m2"><p>اندرا سیر جمال دوست کن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندر آوجه الله باقی توئی</p></div>
<div class="m2"><p>مقصد اقصی ز خلافی توئی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون پیام دوست از هاتف شنید</p></div>
<div class="m2"><p>دست از پیکار دشمن بر کشید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت هاشا من نیم در عهدست</p></div>
<div class="m2"><p>این کشاکشها همه از بهر تست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آشنای تو ز خود بیگانه است</p></div>
<div class="m2"><p>خود توئی تو گر کسی در خانه است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عشق را با من حدیث اختیار</p></div>
<div class="m2"><p>مسئله دور است اما دور یار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عشق را نه قید نام است و نه ننگ</p></div>
<div class="m2"><p>جمله بهر تست چه صلح و چه جنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صورت آئینه عکسی بیش نیست</p></div>
<div class="m2"><p>جنبش و آرام او از خویش نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این کشاکش نیستم از نقض عهد</p></div>
<div class="m2"><p>قاتل خود را همی جویم بجهد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ورنه من بر مرک از آن تشنه ترم</p></div>
<div class="m2"><p>هین ببار ای تیرباران بر سرم</p></div></div>