---
title: >-
    بخش ۱۹ - آمدن بحرها بیاری آنحضرت
---
# بخش ۱۹ - آمدن بحرها بیاری آنحضرت

<div class="b" id="bn1"><div class="m1"><p>بحرها آمد بلب خوشیده کف</p></div>
<div class="m2"><p>کایدرخشان گوهر بحر شرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت اندر حیرت آمد زین عجب</p></div>
<div class="m2"><p>تشنگان سیراب و دریا خشک لب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشنه کاما بحر ها در جز رومد</p></div>
<div class="m2"><p>جمله از جوی تو میجوید مدد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هین بنوش از ما که از جوی توایم</p></div>
<div class="m2"><p>تشنه این لعل دلجوی تو ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی لبت ما را جگر صد چاک باد</p></div>
<div class="m2"><p>آبها را بی تو بر سر خاک باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خشک لب از مهر مادر کام تو</p></div>
<div class="m2"><p>شرم مان بادا ز روی مام تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاش دریاها شدی یکسر سراب</p></div>
<div class="m2"><p>کشت آدم سوختی از قحط آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تشنه کاما دامن از ما بر مکش</p></div>
<div class="m2"><p>تشنۀ آنکام خشگیم العطش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت شه کای بحرهای باخروش</p></div>
<div class="m2"><p>نیست جای دم زدن اینجا خموش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من به بحری تشنه ام کابم گشد</p></div>
<div class="m2"><p>همچو بیماری که بحرانش کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دمبدم آن بحر ژرف بی آمد</p></div>
<div class="m2"><p>سوی خویشم میکشد با جزر و مد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که بسوی ساحلم دارد یله</p></div>
<div class="m2"><p>که ز موجم میکشد در سلسله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق خندان از کشاکشهای او</p></div>
<div class="m2"><p>عقل را دلخون ز استغنای او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آچارۀ مستسقی عشق آب نیست</p></div>
<div class="m2"><p>درد او را چاره جز خوناب نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نکنه تشنه دور چشم ساقی است</p></div>
<div class="m2"><p>بحر خوشد او همان مستسقی است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تشنۀ کاین آتش او را در دل است</p></div>
<div class="m2"><p>چاره گر هست آب تیغ قاتلست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قاتلا زود آی که بس تشنه ام</p></div>
<div class="m2"><p>در سبو ریز آب تیغ و دشنه ام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قاتلا زود آکه روزم شام شد</p></div>
<div class="m2"><p>جان ملول از تنگنای دام شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قاتلا زود آ که بس تاخیر شد</p></div>
<div class="m2"><p>وعدۀ دیدار جانان دیر شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قاتلا هین تیزتر کن خنجرم</p></div>
<div class="m2"><p>ترسمش که دیر برد حنجرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قاتلا ایمن نیم من از بدا</p></div>
<div class="m2"><p>زودتر میکن سرم از تن جدا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس روید ای بحرها زین رهگذر</p></div>
<div class="m2"><p>بر ندارم مرهم از زخم جگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نز زمین منت پذیرم نز فلک</p></div>
<div class="m2"><p>زخمم از جای دگر دارد نمک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زیرکان کز حال دریا مخبرند</p></div>
<div class="m2"><p>قطره سوی بحر عمان کی برند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندرین آبی که ما را در خم است</p></div>
<div class="m2"><p>صد هزاران نوح با طوفان کمست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نی درو پا باب و نه پیدا کنار</p></div>
<div class="m2"><p>بحرها غرقند در وی صد هزار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیست از تاب عطش بی تا بیم</p></div>
<div class="m2"><p>کز عطش بود از ازل سیرابیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آب کم چو تشنگی آور بدست</p></div>
<div class="m2"><p>تا بجوشد آبت از بالا و پست</p></div></div>