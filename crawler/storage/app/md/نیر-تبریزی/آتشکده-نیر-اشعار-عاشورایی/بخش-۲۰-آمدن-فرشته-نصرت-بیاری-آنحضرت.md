---
title: >-
    بخش ۲۰ - آمدن فرشتهٔ نصرت بیاری آنحضرت
---
# بخش ۲۰ - آمدن فرشتهٔ نصرت بیاری آنحضرت

<div class="b" id="bn1"><div class="m1"><p>پس فرشته نصرت از امر قدیر</p></div>
<div class="m2"><p>شد فرو از ذروۀ بالا بزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید تنها تا جدار گاه عشق</p></div>
<div class="m2"><p>گفت کای اسپهبد اسپاه عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من فرشته نصرتم که باریت</p></div>
<div class="m2"><p>نک فرستاده ز بهر یاریت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست شاهان جهان محتاج من</p></div>
<div class="m2"><p>آیۀ نصرُ من الله تاج من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را من سایه اندازم بفرق</p></div>
<div class="m2"><p>یکتنه از غرب تا زد تا بشرق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بدین کافردلان خواهی ظفر</p></div>
<div class="m2"><p>حکم کن تا گسترم بالت بسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت رو بادا مبارک فال تو</p></div>
<div class="m2"><p>کز من است اینسایۀ اقبال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیۀ نصرُ من الله نک منم</p></div>
<div class="m2"><p>پر ز افرشته است یکسر جوشنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نبودی سایۀ من بر سرت</p></div>
<div class="m2"><p>سوختی برق تجلی شهپرت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سریر مالک هستی شه منم</p></div>
<div class="m2"><p>بال خود بر چین که ظل الله منم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ز سایه غیر، کی جویم نجات</p></div>
<div class="m2"><p>سایه پرورد منند این ممکنات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه سایۀ خویش خواندستش اله</p></div>
<div class="m2"><p>کی برد بر سایه پرورد آن پناه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر مرا دریای فضل آید بموج</p></div>
<div class="m2"><p>خیزد از هر سو فرشته فوج فوج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هستی افرشته از هست منست</p></div>
<div class="m2"><p>دست مبسوط خدا دست منست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه شد افرشته خود از وی پدید</p></div>
<div class="m2"><p>بر صنیع خویش کی بندد امید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من بعون و نصرت حق واثقم</p></div>
<div class="m2"><p>لیک خود من این بلا را عاشقم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در بلاها میسپرم لذات او</p></div>
<div class="m2"><p>مات اویم مات اویم مات او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایفرشته شهپر از من باز گیر</p></div>
<div class="m2"><p>تا ببارد بر سرم باران تیز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حاجتی هست ار ترا از ما بخواه</p></div>
<div class="m2"><p>ورنه خوش بخرام سوی جایگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد فرشته نصر با خیل جنود</p></div>
<div class="m2"><p>سوی آن بلا کزو آمد فرود</p></div></div>