---
title: >-
    بخش ۷ - ذکر شهادت عون فرزند عقیله العرب، حضرت زینب
---
# بخش ۷ - ذکر شهادت عون فرزند عقیله العرب، حضرت زینب

<div class="b" id="bn1"><div class="m1"><p>چونعقبله دودۀ آل مناف</p></div>
<div class="m2"><p>دخت زهرا بانوی سر عفاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طود علم و بحر علم من لدن</p></div>
<div class="m2"><p>بیمعلم عالمه اسرار کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر والای دریای شرف</p></div>
<div class="m2"><p>بطن زهرای بتول او را صدف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مظهر بانوی کبرای حجیز</p></div>
<div class="m2"><p>مریم او را دایه و هاجر کنیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست عصمت رشته تار معجرش</p></div>
<div class="m2"><p>سر ناموس نبوت چادرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه صبر و مهد تمکین و وقار</p></div>
<div class="m2"><p>کان غیرت دره التاج فخار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مام دهر از غم گشوده کام او</p></div>
<div class="m2"><p>از ازل ام المصائب نام او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دید سالار شهیدانرا فرید</p></div>
<div class="m2"><p>بسته بر قتلش کمر قوم عنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت با فرزند کایماه حجیز</p></div>
<div class="m2"><p>من بدالت داشتم چونجان عزیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاینچنین روزی رخم داری سفید</p></div>
<div class="m2"><p>جان سپاری در ره شاه شهید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان بدادم شیرت از پستان عشق</p></div>
<div class="m2"><p>کاینچنین روزت کن قربان عشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر امروزت پدر ناامید عون</p></div>
<div class="m2"><p>که شوی نک عون سالار دو کو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاین همه آوازها از شه بود</p></div>
<div class="m2"><p>گر چه از حلقوم عبدالله بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقت آن آمد که در میدان عشق</p></div>
<div class="m2"><p>سرنهی چونکوی بر چوگان عشق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همرهان رفتن هین بشکن قفس</p></div>
<div class="m2"><p>کز هم آوازان نمانی باز و پس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غبن باشد تو در این محبس خموش</p></div>
<div class="m2"><p>ببلان در بوستان گرم خروش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پر بر افشان سوی آنگلزار شو</p></div>
<div class="m2"><p>هم نشین جعفر طیار شو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هین بنه رخ پای اسب شاهرا</p></div>
<div class="m2"><p>کن شفیعش شیب عبدالله را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که کند شاهت بقربانی قبول</p></div>
<div class="m2"><p>سرخ رو آئی بدرگاه بتول</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت خوش باش ای بلاکش مام من</p></div>
<div class="m2"><p>خود همین کار است عین کام من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنده فرمان توام با رأس و عین</p></div>
<div class="m2"><p>این سر من وین کف پای حسین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مادرا من یادگار جعفرم</p></div>
<div class="m2"><p>خود ز شوق جان فشانی میپرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت زینب کایسلیل بیهمال</p></div>
<div class="m2"><p>رو که شیر مادرت بادا حلال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دست او بگرفت بردش نزد شاه</p></div>
<div class="m2"><p>گفت کایمحبوب درگاه اله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با هزاران پوزش آوردم برت</p></div>
<div class="m2"><p>هدیۀ بهر فدای اکبرت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ایخلیل کعبۀ مقصود من</p></div>
<div class="m2"><p>کن بقربانی قبول این رود من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که جز این یکتن سرور سینه ام</p></div>
<div class="m2"><p>در دیگر نیست در گنجینه ام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هین تو یوسف من عجوز یوسفم</p></div>
<div class="m2"><p>جز کلافی نیست زادی در کفم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لطف کن ای یوسف پوزش پذیر</p></div>
<div class="m2"><p>من تهی دستم بضاعت بس حقیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رخصتی ده تا کند اینک فدا</p></div>
<div class="m2"><p>جان براه اکبرت ایمقتدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شه نبیره عم خود در بر گرفت</p></div>
<div class="m2"><p>عارضش بوسید و گفتا ای شگفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اینگرامی گوهر عم من است</p></div>
<div class="m2"><p>غنچه نورستۀ آن گلشن است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون روا باشد که آن نیکو پدر</p></div>
<div class="m2"><p>سوزد از داغ چنین زیبا پسر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خواهرا داغ برادرهات بس</p></div>
<div class="m2"><p>می ببر این میوۀ دل باز پس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دست عباس جدا از پیکرت</p></div>
<div class="m2"><p>بس ز بهر سر زدن تا محشرت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پیکر من غرقه در خون دیدنت</p></div>
<div class="m2"><p>بس ز بهر اشگخون باریدنت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>داغ قاسم آنمه نادیده کام</p></div>
<div class="m2"><p>بس ز بهر ناله تا بازار شام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>داغ مرگ اکبر آنرو سهی</p></div>
<div class="m2"><p>تا قیامت بس ز بهر همرهی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شهر شام و آن هیون بی جهیز</p></div>
<div class="m2"><p>بس ترا روز سیه تا رستخیز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چشم عبدالله که یعقوب و بست</p></div>
<div class="m2"><p>در ره این یوسف فرخ پی است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون بشیر آورد به یثرب این خبر</p></div>
<div class="m2"><p>چون روا باشد که گوید با پدر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یوسفت در جنگ گرگان کشته شد</p></div>
<div class="m2"><p>پیرهن بر خون تن آغشته شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خواهرا تو بهر خود میدار باز</p></div>
<div class="m2"><p>این مهین کودک که پروردی بناز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من باسماعیلت ای هاجر فدا</p></div>
<div class="m2"><p>میدهم اکبر جدا اصغر جدا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بضعۀ زهرا ز درج چشم تر</p></div>
<div class="m2"><p>کرد دامن زینملالت پر گهر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفت کای دارای تاج سروری</p></div>
<div class="m2"><p>حق آن مهر برادر خواهری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حق آن پهلوی زهرا مام من</p></div>
<div class="m2"><p>وان لبان زهر پالای حسن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حق آن شبه پیمبر اکبرت</p></div>
<div class="m2"><p>که مبین این را روا با خواهرت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که برم این ناز پرور نزد باب</p></div>
<div class="m2"><p>با هزاران شرمساری و حجاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گویمش که نزد فرزند رسول</p></div>
<div class="m2"><p>این کمین قربانیت نآمد قبول</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شاه دین از لابۀ آن پاکزاد</p></div>
<div class="m2"><p>داد آن شهزاده را اذن جهاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دخت زهرا کرد با صد و جد و شوق</p></div>
<div class="m2"><p>هدی خود را سوی قربانگاه سوق</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شاهزاده جعفر طیار وار</p></div>
<div class="m2"><p>تیغ در کف تاخت سوی کارزار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از نژاد باب و مادر یاد کرد</p></div>
<div class="m2"><p>خرمن بیحاصلان بر باد کرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رزمگاه از کشتگان آکنده شد</p></div>
<div class="m2"><p>نام پاک جعفر از تو زنده شد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شد چو سیر از خون خصمان عنود</p></div>
<div class="m2"><p>پر بسوی جنت الماوی گشود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شد خرامان سوی فردوس برین</p></div>
<div class="m2"><p>با شقیق خود محمد شد قرین</p></div></div>