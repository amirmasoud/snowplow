---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>قلیان نه اگر آتش عشقش بسر است</p></div>
<div class="m2"><p>دائم ز چه با دود دل و چشم تر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسم است که شکرّ از نی آید بیرون</p></div>
<div class="m2"><p>قلیان بلب لعل تو نی در شکر است</p></div></div>