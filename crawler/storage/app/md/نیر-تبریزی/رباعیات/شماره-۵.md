---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>ای برده قرار و صبر و تاب از دل ریش</p></div>
<div class="m2"><p>لرزان بسر موی تو دلهای پریش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادی سر زلفین مسلسل بر باد</p></div>
<div class="m2"><p>صد خون خطا فکندی از گردن خویش</p></div></div>