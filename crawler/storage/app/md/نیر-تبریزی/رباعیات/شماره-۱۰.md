---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>رخسار بدست زلف جادو دادی</p></div>
<div class="m2"><p>بنگر سپهی را چه قدر رو دادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سادگی خویشتن ایساده نگار</p></div>
<div class="m2"><p>گنجی بکف دزدک هندو دادی</p></div></div>