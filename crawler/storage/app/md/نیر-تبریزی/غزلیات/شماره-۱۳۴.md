---
title: >-
    شمارهٔ  ۱۳۴
---
# شمارهٔ  ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>امشب اگر ای نائی آهنگ دگر داری</p></div>
<div class="m2"><p>از سوز درون ما مانا که خبر داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی قدح می ده با یاد جم و کی ده</p></div>
<div class="m2"><p>زود آر و پیاپی ده گر پاس سحر داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خون رزان ما را مستی نشود حاصل</p></div>
<div class="m2"><p>پیش آر سبوئی چند گر خون جگر داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هان روی محمر کن طرح دگری سر کن</p></div>
<div class="m2"><p>سیم رخ ما زر کن کاکسیر نظر داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیل است خطر دارد آهسته بنه پا را</p></div>
<div class="m2"><p>گر بر سر خون ما آهنگ گذر داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بیخود و سر مستم ای پیر مغان دستی</p></div>
<div class="m2"><p>بو کایندل افتاده از خاک تو برداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه سلسله جنبانی گه مشگ برافشانی</p></div>
<div class="m2"><p>ایزلف خم اندر خم بر گو چه بسرداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای پور بشر تا کی همخوابۀ حور العین</p></div>
<div class="m2"><p>هین سر به بیابان نه گر عهد پدر داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبعی که بشر دارد صد راه به شر دارد</p></div>
<div class="m2"><p>از دیو مخسب ایمن تا طبع بشر داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعلین و عصا بگذار بر وادی ایمن شو</p></div>
<div class="m2"><p>چونموسی اگر جانا آهنگ سفر داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نعل است تنت بر کن نفس است عصا بفکن</p></div>
<div class="m2"><p>زین هر دو چو بگذشتی بس شوکت وفر داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نور ید بیضا بین ثعبان سبکپا بین</p></div>
<div class="m2"><p>هی نار تجلی بین تا نور بصر داری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشقیی که ز جان خیزد از تیر نپرهیزد</p></div>
<div class="m2"><p>ایجان تو سلامت شو مهلاً تو سپر داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایجان جهول ما تو مور سبکساری</p></div>
<div class="m2"><p>بس کوه گران یارا دامن بکمرداری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایطایر لاهوتی تا چند ز مبهوتی</p></div>
<div class="m2"><p>در مجلس ناسوتی صد رشته ببرداری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این رشته زیر بر کن وین تنگ قفس بشکن</p></div>
<div class="m2"><p>از عرش برین سرزن ور دام خطر داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عاشق چو ز پا افتاد با سر برود نیّر</p></div>
<div class="m2"><p>دردا که تو بیچاره نی پا و نه سر داری</p></div></div>