---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>زد ننگ عشق کوس ملامت به نام ما</p></div>
<div class="m2"><p>ای پیک غم ببر به سلامت سلام ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی غم و جهان خم و دل جام و باده خون</p></div>
<div class="m2"><p>جم را خبر دهید ز بزم مدام ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دور چشم یار به کام است باک نیست</p></div>
<div class="m2"><p>گو دور روزگار نباشد به کام ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ ریا نبرد ز سجاده آب نیل</p></div>
<div class="m2"><p>کو آتشی که پخته کند زهد خام ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را نظر بر وزن چشم است روز و شب</p></div>
<div class="m2"><p>تا بو که سایه ز تو افتد به بام ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی چو دور باده‌گساری به ما رسد</p></div>
<div class="m2"><p>خون کن به جای باده گلگون به جام ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منت خدای را که به تلقین پیر عشق</p></div>
<div class="m2"><p>شد خانقاه گوشه ابرو مقام ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبحی اگر به بوی وصالت به شام رفت</p></div>
<div class="m2"><p>مشکل دگر به صبح رود بی‌تو شام ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمری‌ست سر به پای جوانان نهاده‌ایم</p></div>
<div class="m2"><p>ای پیر عشق نیک بدار احترام ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پر شد ز خیل ناله و آهم فضای چرخ</p></div>
<div class="m2"><p>نیر کشید سر به فلک احتشام ما</p></div></div>