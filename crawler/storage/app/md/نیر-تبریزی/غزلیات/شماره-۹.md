---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>کس نزد پای بگویت که نه سر داد آنجا</p></div>
<div class="m2"><p>روید از خاک مگر خنجر بیداد آنجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفت ار گرد برانگیخت زشهری چه عجب</p></div>
<div class="m2"><p>خاک یکسلسله دل آمده بر باد آنجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهوای گل رویت چمنی باز نماند</p></div>
<div class="m2"><p>که نیامد دل شوریده بفریاد آنجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه فتاده است در آنکوی که نگذشت بر او</p></div>
<div class="m2"><p>باری از قافله دل که نیفتاد آنجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشمقامیست فرح بخش خرابات مغان</p></div>
<div class="m2"><p>بود از مغبچه کان تا ابدآباد آنجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بصحرای جنون بگذری ای باد صبا</p></div>
<div class="m2"><p>می بیار از دل زنجیری ما باد آنجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیرا بادیه عشق عجب دامگهی است</p></div>
<div class="m2"><p>که رود سر زده صید از پی صیاد آنجا</p></div></div>