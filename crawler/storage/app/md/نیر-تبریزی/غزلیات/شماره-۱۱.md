---
title: >-
    شمارهٔ  ۱۱
---
# شمارهٔ  ۱۱

<div class="b" id="bn1"><div class="m1"><p>سالها گوشه غم بود دل ریش مرا</p></div>
<div class="m2"><p>باز عشق آمد و افکند به تشویش مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت شیخ گرفتم بنظر جلوه نداشت</p></div>
<div class="m2"><p>بعنایت نظری ای بچه درویش مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با کمند سر زلفت همه چشم است بچشم</p></div>
<div class="m2"><p>مردم ارنام کند صوفی بدکیش مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیمه جانی بستن از من و عذر مپذیر</p></div>
<div class="m2"><p>که گمان نیست که مقدی هله زین پیش مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شور است که افکنده لبانت بعراق</p></div>
<div class="m2"><p>ای همه شور نگاهی بدل ریش مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز کن طره چل تار که از یاد برفت</p></div>
<div class="m2"><p>ذکر صد دانه زهاد کچ اندیش مرا</p></div></div>