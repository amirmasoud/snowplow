---
title: >-
    شمارهٔ  ۶۵
---
# شمارهٔ  ۶۵

<div class="b" id="bn1"><div class="m1"><p>چه شدی کار من دلشده یکسر می‌شد</p></div>
<div class="m2"><p>یا تو سوی من و یا جان سوی تو بر می‌شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا ترا خون جفا با دل من برمی‌گشت</p></div>
<div class="m2"><p>یا دل غمزده بر خوی تو خوگر می‌شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا صبا خرمن موی تو به غارت می‌داد</p></div>
<div class="m2"><p>یا مرا راه بر آن خرمن عنبر می‌شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا همان دم که ترا عادت دیرین برگشت</p></div>
<div class="m2"><p>عهد دیرینهٔ من نیز از این در می‌شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مرا دیده نبود از همه بهتر بودی</p></div>
<div class="m2"><p>یا ترا روی نگویی نه نکوتر می‌شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌گشودم که از بن اشک دمادم چشمی</p></div>
<div class="m2"><p>شاد بودم به خیالی که مصوّر می‌شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد آن عهد که از خلوت انس من و دوست</p></div>
<div class="m2"><p>همه بر چشم رقیبان سر نشتر می‌شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من رام بودم و در بسته و همسایه به خواب</p></div>
<div class="m2"><p>هر زمانم از رخش باده به ساغر می‌شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده سیر قدش از سر همه تا پا می‌کرد</p></div>
<div class="m2"><p>جان فدای تنش از پا همه تا سر می‌شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به تلطف به رخم زلف معنبر می‌سود</p></div>
<div class="m2"><p>هر زمانم که رخ از اشک مرا تر می‌شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که چو گل بی‌خودم از ناز چو بلبل می‌کرد</p></div>
<div class="m2"><p>گه چو پروانه مرا شمع منور می‌شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هردم آیینهٔ رخسار به آیین دگر</p></div>
<div class="m2"><p>جلوه می‌داد مرا عالم دیگر می‌شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او مرا تکیه بر آغوش چو مستان می‌داد</p></div>
<div class="m2"><p>چون مرا دست بر آن سینهٔ چون پر می‌شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من به پاسش همه‌شب ریختمی اشک چو شمع</p></div>
<div class="m2"><p>او چو از سر خوشی خواب به بستر می‌شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منش از دیده همی لؤلؤ تر می‌دادم</p></div>
<div class="m2"><p>او ز لب قند همی‌داد و مکرر می‌شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شرح حال دل آشفته به شب‌های دراز</p></div>
<div class="m2"><p>موبه‌مو با سر آن زلف معنبر می‌شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواب بود اینکه من دلشده دیدم نیر</p></div>
<div class="m2"><p>یا خیالی که به هوشم ز برابر می‌شد</p></div></div>