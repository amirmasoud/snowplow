---
title: >-
    شمارهٔ  ۵۷
---
# شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>زلف بملک حسن بسر تا کله نهاد</p></div>
<div class="m2"><p>برداشت پای نخوت و بر فرق مه نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عادت نبود تیغ کشیدن بروی ماه</p></div>
<div class="m2"><p>این رسم تازه را خم زلف سیه نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر عالمی کشد چه عجب ترک چشم او</p></div>
<div class="m2"><p>یا سای قتل عام نه این پادشه نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر بوی خاکپای تو دل هر کجا که دید</p></div>
<div class="m2"><p>خاک رهی است روی بر آنخاکره نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهو چنین نگه نکند چشم تو مگر</p></div>
<div class="m2"><p>قانون دیگری ز برای نگه نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم هلال او بسر انگشت مینمود</p></div>
<div class="m2"><p>اینماه رسم را بشب چهارده نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمت خرابی دل ما را بخط نگفت</p></div>
<div class="m2"><p>تا رو بکشور دل ما با سپه نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طاعت بود نظاره بهر ماه تو ولیک</p></div>
<div class="m2"><p>ابروی او برای من اینرا گنه نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بالله که جز بمهر شهنشاه لو کشف</p></div>
<div class="m2"><p>هر کو نهاد دل بخیال تبه نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیر هوای روضۀ رضوان ز سر بهشت</p></div>
<div class="m2"><p>هر کس که روی بر در این بارگه نهاد</p></div></div>