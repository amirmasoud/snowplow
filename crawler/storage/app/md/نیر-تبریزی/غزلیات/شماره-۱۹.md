---
title: >-
    شمارهٔ  ۱۹
---
# شمارهٔ  ۱۹

<div class="b" id="bn1"><div class="m1"><p>امروز خرمن گل و نسرین و سوسنست</p></div>
<div class="m2"><p>وینمونه بازغالیه و مشگ ولادنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کز سرم میگذری باش یک زمان</p></div>
<div class="m2"><p>کافشانمت بپای روانی که در تن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زد آتشی به پرده ناموس سوز عشق</p></div>
<div class="m2"><p>کامروز در جهان همه افسانه من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسوده نیست از شرر آهم آسمان</p></div>
<div class="m2"><p>زلف تو تا بر آتش دل باد بیزن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیرتم که اینهمه رودادنت چراست</p></div>
<div class="m2"><p>برطره که خون جهانش بگردنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ماه به تو لاف تقابل زند چه باک</p></div>
<div class="m2"><p>حسن رخ تو بر همه چونمهر روشنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چین زلف روی تو پا در قفس همای</p></div>
<div class="m2"><p>یا برگ گل بچنبر و یامه بخرمنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد از توام چه حاجت صحرا و لاله زار</p></div>
<div class="m2"><p>چون اشک لاله گونم و صحرای دامنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش من و نصیحت دوران پیش بین</p></div>
<div class="m2"><p>زین پس حکایت شتر و چشم سوزنست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیر ملال دوست مبادا بروزگار</p></div>
<div class="m2"><p>گر روزگار ما بتمنای دشمن است</p></div></div>