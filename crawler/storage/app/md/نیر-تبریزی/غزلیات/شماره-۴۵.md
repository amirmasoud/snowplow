---
title: >-
    شمارهٔ  ۴۵
---
# شمارهٔ  ۴۵

<div class="b" id="bn1"><div class="m1"><p>حسن از آنپایه گذشته است که در وصف من آید</p></div>
<div class="m2"><p>مگر او پرده براندازد و خود رخ بنماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشگم از پرتو خورشید جهانتاب برآید</p></div>
<div class="m2"><p>که همه روز همی روی بدیوار تو ساید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه ما را بقفا عیب کنند اهل سلامت</p></div>
<div class="m2"><p>کس بروی تو نگوید دل مردم نرباید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وعده قتل من ایکاش بفردا نگذارد</p></div>
<div class="m2"><p>عهد خوبان همه دانند که بس دیر نپاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه بود زاهد اگر ذوق حضور تو ندارد</p></div>
<div class="m2"><p>در فردوس ملایک بهمه کس نگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ برافروز بگو با گل سوری که ببلبل</p></div>
<div class="m2"><p>ناز مفروش که از زشت رخان ناز نشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی آب طرب انگیز به بیدردلان ده</p></div>
<div class="m2"><p>درد جامی بمن آور که مرا درد فزاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلفی چونتو نزاید مگر از مادر گیتی</p></div>
<div class="m2"><p>تا از اینصورت زیبای دلاویز چه زاید</p></div></div>