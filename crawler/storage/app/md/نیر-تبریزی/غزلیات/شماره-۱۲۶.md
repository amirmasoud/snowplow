---
title: >-
    شمارهٔ  ۱۲۶
---
# شمارهٔ  ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>چند بیهوده دلا اینهمه افغان از من</p></div>
<div class="m2"><p>رخ ز من اشک ز من دیدۀ گریان از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنشد ایخواجه که از جانرود پای شکیب</p></div>
<div class="m2"><p>کانزمان دست زمن بود و گریبان از من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقلان با همه شوریده دلی حیرانم</p></div>
<div class="m2"><p>کز چه طفلان سرشکند هراسان از من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم پیش تو گویم غم دل ترسیدم</p></div>
<div class="m2"><p>شود آنزلف گرهگیر پریشان از من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که شبیخون زده بر کشور دل باز که چشم</p></div>
<div class="m2"><p>میبرد گوهر ناسفته بدامان از من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضعفم از پای درآورد بنال ایدل زار</p></div>
<div class="m2"><p>بلکه بیزار شود شحنۀ زندان از من</p></div></div>