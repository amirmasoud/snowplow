---
title: >-
    شمارهٔ  ۸
---
# شمارهٔ  ۸

<div class="b" id="bn1"><div class="m1"><p>صنما نه میل مسجد نه سر کنشت ما را</p></div>
<div class="m2"><p>که قمار عشق از این غم همه داد گشت ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفدای شورت ایعشق نه چنان ببر زهوشم</p></div>
<div class="m2"><p>که بدفتر جنون هم نتوان نوشت ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو هزار سنگ طفلان خورم هنوز سبزم</p></div>
<div class="m2"><p>زفرح که پیر دهقان بره تو کشت ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنظر تو شاه خوبان مفکن بهل نگارا</p></div>
<div class="m2"><p>که مصوّران کج بین بکشند زشت ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم ار گل بهاری که بگلشنم بکاری</p></div>
<div class="m2"><p>بگذار جای خاری بکنار کشت ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشم آنخدنگ مژگان بدل و خوشست وقتم</p></div>
<div class="m2"><p>که زغفلت آنکمانش زنظر نهشت ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی من بخاک غم کن چوکشی زسنگ جورم</p></div>
<div class="m2"><p>که سروش پاک طینت زغمت سرشت ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخدا یگان محشر بود ار قبول نیر</p></div>
<div class="m2"><p>تو بهل کشد بدوزخ ملک از بهشت ما را</p></div></div>