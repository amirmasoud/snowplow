---
title: >-
    شمارهٔ  ۷۹
---
# شمارهٔ  ۷۹

<div class="b" id="bn1"><div class="m1"><p>شانه هر دم که بر آن کاکل مشکین گذرد</p></div>
<div class="m2"><p>وه چه گویم که چه‌ها بر دل خونین گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاش یکسر هم بر چشم من آید ز خطا</p></div>
<div class="m2"><p>هر خدنگی که از آن ابروی پر چین گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایندل و این تو که دست از سر خود بردارد</p></div>
<div class="m2"><p>شه چو در کلبۀ رستائی مسکین گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منعم از عشق جوانان مکن ایناصح پیر</p></div>
<div class="m2"><p>بس قبیح است که پیری کهن از دین گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه بکهسار گهی راز بصحرا گویم</p></div>
<div class="m2"><p>بو که ویسی ز خدا بر سر رامین گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیر ماندی بدم ایصبح سعادی بگذار</p></div>
<div class="m2"><p>دانۀ اشک من از خوشۀ پروین گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نفس دارم از ایندرد بنالم حاشا</p></div>
<div class="m2"><p>خنک آنروز که جانانه ببالین گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حور عین گرگذرد بر سرم از کوی بهشت</p></div>
<div class="m2"><p>نه من از دوست نه فرهاد ز شیرین گذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشق از حیرت وصلت سر و جان و دل و دین</p></div>
<div class="m2"><p>همه در دست و نداند ز کدامین گذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند گفتم بدل آنروز که او چشم گشود</p></div>
<div class="m2"><p>بس کن از قهقهه ایکبک که شاهین گذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با چنین ساعد دلبند خصومت جهل است</p></div>
<div class="m2"><p>بگذارید که خون تا ز سر زین گذرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر آندل که بر او عشق ندارد نیرّ</p></div>
<div class="m2"><p>سنگ باشد ز چنین لعبت سیمین گذرد</p></div></div>