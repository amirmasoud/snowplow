---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>گیرم اندر دل پر درد هزاران غم از اوست</p></div>
<div class="m2"><p>داوری پیش که آرم که همه عالم از اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی خاطر او باد زما یکسان است</p></div>
<div class="m2"><p>دل اگر غمزده از دوست وگر خرم ازوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون بده جای می کهنه مرا ای ساقی</p></div>
<div class="m2"><p>شادی آنکه غمی تازه مرا هردم ازوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف مشگین تو را کوتهی عمر مباد</p></div>
<div class="m2"><p>گرچه زخم دل آشفته ما درهم ازوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده خود کیست که با خواجه بانکار آید</p></div>
<div class="m2"><p>تیغ از او بنده از او زخم از او مرهم ازوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه در عمر زظلمات سکندر میجست</p></div>
<div class="m2"><p>مژده ای خضر که در زلف خم اندر خم ازوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رب آن افعی بیجان بسر دانه خال</p></div>
<div class="m2"><p>که رها کرد که مرغ دل مارارم ازوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه داد دل ما زان لب میگون گیرد</p></div>
<div class="m2"><p>خط سبز است که پشت دل ما محکم ازوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب سر زلف تو آشفته مگر مستی خواب</p></div>
<div class="m2"><p>که نسیم سحر امروز مسیحا دم ازوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس نه من در ضلب آنرخ گندم گونم</p></div>
<div class="m2"><p>کاین سرشتی است که در آب گل آدم ازوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لاوه زین پیش مزن طعنه بزنار کشیش</p></div>
<div class="m2"><p>مگر این طره دستار تو زاهد کم ازوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاهد از رمز لب و نکته باریک میان</p></div>
<div class="m2"><p>چه تمتع برد اسرار نهان مبهم ازوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیرا دل زغم دور جهان تنگ مدار</p></div>
<div class="m2"><p>شادی روی حبیبی که جهان خرم ازوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نور ذات ازلی مظهر آیات علی</p></div>
<div class="m2"><p>که در احیای مسیحا نفسی مریم ازوست</p></div></div>