---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>نفس بازپسین است ز هجرت جان را</p></div>
<div class="m2"><p>مژده ای بخت که شد عمر به سر هجران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاقت باج غمش در دل ویرانه نماند</p></div>
<div class="m2"><p>باز هشتم بوی این دهکده ویران را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محترم دار غم ای دل که خداوند کرم</p></div>
<div class="m2"><p>گرچه کافر بود اکرام کند مهمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تندم از سر گذرد غافل و من غرق نگاه</p></div>
<div class="m2"><p>نیست از تلوسه غرقه خبر طوفان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شد از معتکف گوشه ابرو چه عجب</p></div>
<div class="m2"><p>کنج محراب بود بی‌سر و بی‌سامان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نوک تیمار غباری کند از زلف تو چشم</p></div>
<div class="m2"><p>دم به دم تر کند از اشک پر مژگان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خدنگم چو زنی سخت بکش بال کمان</p></div>
<div class="m2"><p>ترسم آن سو گذرد پر ز هدف پیکان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست بر گردن جانان من و خلقی نگران</p></div>
<div class="m2"><p>کو ندیدند مگر هیچ ببر چوگان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوس تسلیم فرو کوب که ویران افتد</p></div>
<div class="m2"><p>نیر آن ملک که گردن ننهد سلطان را</p></div></div>