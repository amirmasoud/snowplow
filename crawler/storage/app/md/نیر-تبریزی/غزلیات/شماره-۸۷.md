---
title: >-
    شمارهٔ  ۸۷
---
# شمارهٔ  ۸۷

<div class="b" id="bn1"><div class="m1"><p>ز تاب زلف تو نارسته خط دمید آخر</p></div>
<div class="m2"><p>بغیر روز سیاه از تو دل ندید آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک بر ابروی من خم نداد و غمزۀ تو</p></div>
<div class="m2"><p>بیک اشاره کمان مرا کشید آخر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم ز شوق دهانش میان خون میگشت</p></div>
<div class="m2"><p>بنقد بوسه لبش خون من خرید آخر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدیکه سر بسر آن خم نکرد در همه عمر</p></div>
<div class="m2"><p>بپای بوس سهی قامتی خمید آخر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال افعی زلفت بدیده دید مگر</p></div>
<div class="m2"><p>که مرغدل بفغان زاشیان پرید آخر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم از دهان تو زد غنچه گوئیا که صبا</p></div>
<div class="m2"><p>ز رشک پردۀ ناموس او درید آخر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قبای روز سیه راست شد بقامت ما</p></div>
<div class="m2"><p>مشاطه تا سر آنزلف کچ برید آخر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز مهر ورزی پنهان دلم بجان آمد</p></div>
<div class="m2"><p>جنون عشق بفریاد من رسید آخر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قرار و صبر ز دل جوبدار نشان نیرّ</p></div>
<div class="m2"><p>بگو که خونشدو از دیدگان چکید آخر</p></div></div>