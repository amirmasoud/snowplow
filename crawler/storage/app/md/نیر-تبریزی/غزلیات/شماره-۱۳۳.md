---
title: >-
    شمارهٔ  ۱۳۳
---
# شمارهٔ  ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>ایصورت زیبای تو مرآت معانی</p></div>
<div class="m2"><p>سرگشته چو پرگار به تصویر تو مانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جنس ملک گر بشری هست تو اوئی</p></div>
<div class="m2"><p>در نوع بشر گر ملکی هست تو آنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسمی اگر ایمایۀ جان جسم بهشتی</p></div>
<div class="m2"><p>جانی اگر ایقوّت تن جان جهانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سهو است بجد تنگ کشیدن بکنارت</p></div>
<div class="m2"><p>زینسان که تو نازک تن و باریک میانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرش نه ای ترک توزۀ سست کن از تبر</p></div>
<div class="m2"><p>بر ساعد سیمین نسزد سخت کمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامی ز دهان تو شنیدیم در افواه</p></div>
<div class="m2"><p>جستیم و ندیدیم از آن نام نشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر وصل دهانت طلبد دل مکنش منع</p></div>
<div class="m2"><p>نبود عجب از پیر تمنای جوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویم که شب از نالۀ دل سخت تو شد نرم</p></div>
<div class="m2"><p>چون روز شود باز ببینم که همانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتند بدور زنخش آب حیوه است</p></div>
<div class="m2"><p>خود را بچه انداختم از راه ندانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جرم از دل ما نیست که امکان سلامت</p></div>
<div class="m2"><p>از تیر نظر نیست که تیریست نهانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایجان بترازیّ بهای تو سبک سنگ</p></div>
<div class="m2"><p>درّی چو تو کس یاد ندارد بگرانی</p></div></div>