---
title: >-
    شمارهٔ  ۷۵
---
# شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>جان در خور خدنگ تو ابرو کمان نبود</p></div>
<div class="m2"><p>ورنه دل رقیق تو نامهربان نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بار کشته بود ز جورم نهیب هجر</p></div>
<div class="m2"><p>گر بوسۀ وداع مر احرز جان نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم بخواب دوش که هم بستر منی</p></div>
<div class="m2"><p>هر گر به بخت خفته مرا اینگمان نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ببوسه جان دهمت سر کشیدو گفت</p></div>
<div class="m2"><p>در ملک حسن بوسه چنان رایگان نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر صبحدم که بی تو به بستم بناقه باز</p></div>
<div class="m2"><p>فریاد من کم از جرس کاروان نبود</p></div></div>