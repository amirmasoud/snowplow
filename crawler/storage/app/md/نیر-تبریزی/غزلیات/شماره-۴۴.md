---
title: >-
    شمارهٔ  ۴۴
---
# شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>آن نه مست است که می خورد و ببازار افتاد</p></div>
<div class="m2"><p>مست آن بود که در خانه خمار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل چون فانوس خیال از اثر شعله شمع</p></div>
<div class="m2"><p>بسکه بر دور تو گردید زرفتار افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد حسن تو در پرده نهان بود هنوز</p></div>
<div class="m2"><p>که حریفان ترا پرده زاسرار افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه آئینه هوای تو پری در سر داشت</p></div>
<div class="m2"><p>همچو دیوانه چرا عور ببازار افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل سودا زده تا سلسله زلف تو دید</p></div>
<div class="m2"><p>نعره بر زد و دیوار بیکبار افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موشکافی زمیان تو بتحقیق نرفت</p></div>
<div class="m2"><p>در میان بحت در اینمسئله بسیار افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده پوشی چکنم خود زپریشانی کار</p></div>
<div class="m2"><p>همه دانند که با زلف توام کار افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بت ستائید زه عویّ انا الحق نیّر</p></div>
<div class="m2"><p>آنسیه دل که گذارش بسردار افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خنفس ار دید که بر خیل شهان نعل زنند</p></div>
<div class="m2"><p>بغلط پای برآورد نگونسار افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طبع نیر هوس نکته سرائیها داشت</p></div>
<div class="m2"><p>دید گوش شنوا نیست ز گفتار افتاد</p></div></div>