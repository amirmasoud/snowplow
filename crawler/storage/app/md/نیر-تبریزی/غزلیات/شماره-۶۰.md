---
title: >-
    شمارهٔ  ۶۰
---
# شمارهٔ  ۶۰

<div class="b" id="bn1"><div class="m1"><p>مهل آنروی که از پرده پدیدار آید</p></div>
<div class="m2"><p>ترسم از چشم بد خلق به آزار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دام نرچین که دگر نیست دلی در همه شهر</p></div>
<div class="m2"><p>که نه بر حلقۀ زلف تو گرفتار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبر خویش ترانم که مگس از سرقند</p></div>
<div class="m2"><p>نرود ور برود نیز دگربار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قامتت کرد قیامی و قیامت برخواست</p></div>
<div class="m2"><p>چه شود آه ندانم که برفتار آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقد بر جبهه میفکن که طبیبان نکند</p></div>
<div class="m2"><p>رو ترش چون بسر بستر بیمار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم الله نبرد نام سلامت بزبان</p></div>
<div class="m2"><p>خستۀ را که ز در چونتو پرستار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایدل غمزده خوابی که شب از نیمه گذشت</p></div>
<div class="m2"><p>وقت آنست که همسایه بزنهار آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایطبیب از سر نیرّ بسلامت بگذر</p></div>
<div class="m2"><p>کآتش اندر تو نگیرد چو بگفتار آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایقدت سرو اگر سرو برفتار آید</p></div>
<div class="m2"><p>وی لبت غنچه اگر غنچه بگفتار آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زلف ار برد یغما دل شهری چه عجب</p></div>
<div class="m2"><p>هر چه گویند از آن رهزن طرّار آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردو صدناو کم آید ز تو بر سینۀ ریش</p></div>
<div class="m2"><p>چشم آنست هنوزم که دگر بار آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دم جان بخش مسیحاست سحر خیز انرا</p></div>
<div class="m2"><p>سخن تلخ کز آن لعل شکر بار آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا رب آنخال که ما را شد از او روز سیاه</p></div>
<div class="m2"><p>ببلای خم زلف تو گرفتار آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه بیمار غمت کرد ز دوری نیّر</p></div>
<div class="m2"><p>دل قوی دار که خود نیز پرستار آید</p></div></div>