---
title: >-
    شمارهٔ  ۲۶
---
# شمارهٔ  ۲۶

<div class="b" id="bn1"><div class="m1"><p>چند برد زاهد انتظار قیامت</p></div>
<div class="m2"><p>گو ز قیامت گذشت جلوه قامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل زقیامت براستی نتوان کند</p></div>
<div class="m2"><p>گو بسر ما رود هزار قیامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بست نگاه تو چشم عارف و عامی</p></div>
<div class="m2"><p>سحر بتابید بر فنون کرامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل چه سلامت بدور چشم تو بیند</p></div>
<div class="m2"><p>ایخم ابرو سر تو باد سلامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون بود آیت ز زخم ماهی دریا</p></div>
<div class="m2"><p>سرخی چشمت بخون ماست علامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آهوی وحشی است دل ز دیده میفکن</p></div>
<div class="m2"><p>صید چو رفت از نظر چه سود ندامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو قدی راست کرد تا بخرامد</p></div>
<div class="m2"><p>پیش تو در گل فکند رحل اقامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیر نگاهی اگر ز چشم تو گم شد</p></div>
<div class="m2"><p>خون نشد ایشوخ جان ببر بغرامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق و ملامت کشی دو یار قدیمند</p></div>
<div class="m2"><p>لطف مبر از من ایخدنگ ملامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پای نگارین مکش ز دیده نیر</p></div>
<div class="m2"><p>کز همه بیمهریست و از تو کرامت</p></div></div>