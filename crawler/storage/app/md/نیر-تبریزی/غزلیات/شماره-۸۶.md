---
title: >-
    شمارهٔ  ۸۶
---
# شمارهٔ  ۸۶

<div class="b" id="bn1"><div class="m1"><p>بکش ای دوست نداریم ز حکم تو گزیر</p></div>
<div class="m2"><p>گر به کیش تو گناه است ترحم با سیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوست با جان من آن کرد که ماهی به کتان</p></div>
<div class="m2"><p>عشق با صبر من آن کرد که آتش به حریر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم از حسرت عناب لبت خواهم مرد</p></div>
<div class="m2"><p>گفت سیب زنخم بین و دگر بار بمیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به حکم سر زلفت ننهم گردن طوع</p></div>
<div class="m2"><p>چه کنم با که شکایت کنم از دست امیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر شوریده مپندار به خود باز آید</p></div>
<div class="m2"><p>تا نه زان ناقۀ کاکل شنود بوی عبیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غایب از ما مشو ای مهر درخشان که به عمر</p></div>
<div class="m2"><p>با چراغت نتوان یافت در آفاق نظیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای من لنگ و سر آب به صد مرحله دور</p></div>
<div class="m2"><p>چه کند تشنه نمیرد به بیابان ز هجیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب این خرمن گل چشم جهان سیر کند</p></div>
<div class="m2"><p>ز چه از روی نشود چشم من دلشده سیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلبر آمد بسر کشته خود لیک چه سود</p></div>
<div class="m2"><p>طبل واپس بزن از شست کمان رفت چو تیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طوق مویی به بناگوش زد و گفت ببوس</p></div>
<div class="m2"><p>پی نبردم که همان قصۀ چاهست و ضریر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبش زین پیش مزن بر دلم از ناوک ناز</p></div>
<div class="m2"><p>ای جوان بخت بیندیش ز آه دل پیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بعد مرگ ار شنوم بوی تو از باد صبا</p></div>
<div class="m2"><p>آن کند با من خاکی که به یعقوب بشیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من که در گوشۀ ابروی تو حبس نظرم</p></div>
<div class="m2"><p>ای شهنشه نظر از حبس نظر باز مگیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شعر سعدی همه دلبند و ملیح است ولیک</p></div>
<div class="m2"><p>نیرّّ انظم تو کو برد ز خواجو و ظهیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لب فرو بند ز نتشبیب و برافشان دُرتاب</p></div>
<div class="m2"><p>ز ثنای شه مهر افسرا و رنگ غدیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نقش برد از عمل آیینهٔ حسن ازل</p></div>
<div class="m2"><p>که ز نوک قلمش یافت هیولان تصویر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دارم امید که جرمم به عطا درگذرد</p></div>
<div class="m2"><p>که خداوند کریم است و شه عذرپذیر</p></div></div>