---
title: >-
    شمارهٔ  ۱۳
---
# شمارهٔ  ۱۳

<div class="b" id="bn1"><div class="m1"><p>بی می نتوان بردبسر فصل خزان را</p></div>
<div class="m2"><p>ساقی بده آنجام پر از خون رزان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که کند فاش دگر راز نهانرا</p></div>
<div class="m2"><p>از دیده که میریزدم این اشک روان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پیر و جوان برده دل آن ترک جفاجو</p></div>
<div class="m2"><p>تا عیب نگیرد پدر پیر جوان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر باد خزان خون رزان ریخت بگلزار</p></div>
<div class="m2"><p>بر خون رزان ده ثمن برگ خزانرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگرفت دل از صحبت زهاد سبک مغز</p></div>
<div class="m2"><p>یاران بمن آید مر آنرطل گرانرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحن چمن از چیست زر اندوه وگرنه</p></div>
<div class="m2"><p>خاصیت اکسیر بود باد وزان را</p></div></div>