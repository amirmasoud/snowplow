---
title: >-
    شمارهٔ  ۱۲۹
---
# شمارهٔ  ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>طوقی ز خط بدور زنخدان کشیده‌ای</p></div>
<div class="m2"><p>بر دور مهر و مه خط بطلان کشیده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داود را بحلقۀ خفتان نهفتۀ</p></div>
<div class="m2"><p>یوسف بدور چاه بزندان کشیده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانی بخضر در صفت ای سبزۀ عذار</p></div>
<div class="m2"><p>پیداست کاب چشمۀ حیوان کشیده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مشک تر نوعته طلسمی بسیم خام</p></div>
<div class="m2"><p>مه در کمند موی بدستان کشیده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایچشم مست باده چه خوردی که از غرور</p></div>
<div class="m2"><p>خنجر بروی مهر درخشان کشیده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایخال دل سیه دل یکشهر بردۀ</p></div>
<div class="m2"><p>خود در شکبخ زلف پریشان کشیده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پرده دیدۀ مگر آن سینۀ چو سیم</p></div>
<div class="m2"><p>ای صبحدم که سر بگریبان کشیده‌ای</p></div></div>