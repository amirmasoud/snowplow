---
title: >-
    شمارهٔ  ۵۶
---
# شمارهٔ  ۵۶

<div class="b" id="bn1"><div class="m1"><p>یاد موی توام از دیده به در می‌نرود</p></div>
<div class="m2"><p>خط محویست که از روی قمر می‌نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه گوید به سر آرم هوس زلف دراز</p></div>
<div class="m2"><p>ما هم این تجربه کردیم به سر می‌نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدق پیش آر که دایم نرود عشوه به کار</p></div>
<div class="m2"><p>اگر این بار رود بار دگر می‌نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منعم از گریه مفرمای ز دنباله دل</p></div>
<div class="m2"><p>داغ مرگ پسر از چشم پدر می‌نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طعن مردم ز من و زلف تو در دست رقیب</p></div>
<div class="m2"><p>میرم این داغ هنوزم ز جگر می‌نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر هر راه که گیرم ز پی شکوه به عمد</p></div>
<div class="m2"><p>ره بگرداند و زین راه گذر می‌نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خونبهایی ز لبت ده به شهیدان باری</p></div>
<div class="m2"><p>این همه خون قتیلان به هدر می‌نرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق را می‌نرود آب به یک جو با عقل</p></div>
<div class="m2"><p>مثل است این به زمین میخ دو سر می‌نرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده می‌دوزم و تیر تو ز دل در گذر است</p></div>
<div class="m2"><p>چه کنم کار ز پیشم به سپر می‌نرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شانه کوته کن از آن زلف که خون شد دل من</p></div>
<div class="m2"><p>هرگز این دست‌درازی ز نظر می‌نرود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل عشاق به دست آر که از جور رقیب</p></div>
<div class="m2"><p>خون دل نیست که از دیده به بر می‌نرود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگسلد دست دل خسته ز موی کمرت</p></div>
<div class="m2"><p>کوه اگر می‌رود از جای دگر می‌نرود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس نه من خواجه حدیث لب او می‌گویم</p></div>
<div class="m2"><p>موضعی نیست که این بار شکر می‌نرود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>واعظان گویدم از مهر علی دل بردار</p></div>
<div class="m2"><p>در من این عیب قدیمست به در می‌نرود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیرّا همت از او جو که کرم‌داران را</p></div>
<div class="m2"><p>هیچ خواهنده تهیدست ز در می‌نرود</p></div></div>