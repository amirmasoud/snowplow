---
title: >-
    شمارهٔ  ۶۷
---
# شمارهٔ  ۶۷

<div class="b" id="bn1"><div class="m1"><p>زلف جانان سحر از باد صبا در هم شد</p></div>
<div class="m2"><p>عاقلان مژده که زنجیر جنون محکم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی از نشئه مستی کله از سر نگرفت</p></div>
<div class="m2"><p>گل و سنبل به هم آمیخت عجب عالم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سال‌ها بود که دارا سر و سامانی بود</p></div>
<div class="m2"><p>عاقبت در سر آن زلف خم اندر خم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خط سبز تو مویی به دو عالم ندهم</p></div>
<div class="m2"><p>تا نگویی سر مویی ز ارادت کم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش خون دل عاشق بیچاره که خورد</p></div>
<div class="m2"><p>به تبسم نگهی کرد سخن مبهم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر هر گل دل صد بلبل مسکین خون گشت</p></div>
<div class="m2"><p>تا در این گلشن پر خار دلی خرم شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش هیچ سر صحبت ما داری؟ گفت</p></div>
<div class="m2"><p>کی پری را هوس انس بنی‌آدم شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشک با هیچ جراحت نشنیدم که بساخت</p></div>
<div class="m2"><p>غیر زلفت که دل ریش مرا مرهم شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کم مباد از سر من سایهٔ این غم نیر</p></div>
<div class="m2"><p>کافتتاحی شد اگر کار مرا زین غم شد</p></div></div>