---
title: >-
    شمارهٔ  ۵۵
---
# شمارهٔ  ۵۵

<div class="b" id="bn1"><div class="m1"><p>مرغی شکسته درخور تبر ستم نبود</p></div>
<div class="m2"><p>گیرم مقیم زلف تو صید حرم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدار بخت من که شدم صید شست تو</p></div>
<div class="m2"><p>پیکان جان شکار ترا صید کم نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد قاتل از سر من و غم میکشد مرا</p></div>
<div class="m2"><p>گر بود بر سرآنکه مرا کشت غم نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنرا که دلخوش است بسیری ز بوستان</p></div>
<div class="m2"><p>راندن ز در نشانۀ اهل کرم نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمشید را بسلطنت ما چه اشتباه</p></div>
<div class="m2"><p>او را ز دور اینهمه خیل و حشم نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دفتر چکامۀ خوبان روزگار</p></div>
<div class="m2"><p>جستیم نقش مهر اثری زینرقم نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسنت نگین لعل بخط بر خطا سپرد</p></div>
<div class="m2"><p>موری سزای سلطنت ملک جم نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با آنکه با تو بسته ام عهدی قدیم من</p></div>
<div class="m2"><p>نگرفتمی دل از تو گر این نیر هم نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر هر صنم نظیر تو بودی بدلبری</p></div>
<div class="m2"><p>مسجود اهل دل بجهان جز صنم نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این حسرتم کشد که چو باز امدی بناز</p></div>
<div class="m2"><p>ما را سری بپای تو در هر قدم نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجنون که گاه سلطنت وحش و طیر یافت</p></div>
<div class="m2"><p>نیرّ چو ما بملک بلامحتشم نبود</p></div></div>