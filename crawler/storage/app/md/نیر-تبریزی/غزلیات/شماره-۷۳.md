---
title: >-
    شمارهٔ  ۷۳
---
# شمارهٔ  ۷۳

<div class="b" id="bn1"><div class="m1"><p>اگر بلبل بدل داغی ز جور باغبان دارد</p></div>
<div class="m2"><p>در آتش من که با من نوگل من سرگران دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیابانی است بی پایان من آن سرگشته آهوئی</p></div>
<div class="m2"><p>که هر سو رو کند صیاد تیری در کمان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که اینحال عجب یا رب نهان با محتسب گوید</p></div>
<div class="m2"><p>که شوخی دل ز من برده است و روی از من نهان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز لب خندی مرا از گریه دامن پر گهر کردی</p></div>
<div class="m2"><p>مگر لعل لبت خاصیت شه گوهران دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بامید گهر خود را بدریا میزدی ای دل</p></div>
<div class="m2"><p>نگفتم زینهوس بگذر که دریا بیم جان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گلبانگ عراقی آتشم در پرده زن مطرب</p></div>
<div class="m2"><p>که مرغ جان ملال از خاک آذربایجان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر گامی هزاران دل بپای ناقه میغلطد</p></div>
<div class="m2"><p>کدامین دلستان یا رب در اینمحمل مکان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبا آهسته بگذر زانمعنبر زلف خم در خمر</p></div>
<div class="m2"><p>هزاران طایر پر بسته در وی آشیان دارد</p></div></div>