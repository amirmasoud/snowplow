---
title: >-
    شمارهٔ  ۹۲
---
# شمارهٔ  ۹۲

<div class="b" id="bn1"><div class="m1"><p>گه به مسجد کشدم گه به کلیسای کشیش</p></div>
<div class="m2"><p>بستۀ موی بتانست مرا تختۀ کیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد و طرۀ دستار من و زلف نگار</p></div>
<div class="m2"><p>هر کسی را هوسی در سر و کاری در پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر دیوانه مگر تا به چه پایان باشد</p></div>
<div class="m2"><p>خنک آن روز کزین سلسله گیرم سر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظری بر تو و صد بار نگه بر چپ و راست</p></div>
<div class="m2"><p>تا نیایند رقیبان توام از پس و پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کنم گر ننهم سر به بیایان جنون</p></div>
<div class="m2"><p>پنجۀ عشق قوی‌لقمه‌ام از حوصله پیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این منم کافعی زلف تو بجان می‌طلبم</p></div>
<div class="m2"><p>ورنه کس دشمن جانی ندهد راه به خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پارسایی به تغافل ز تو فکریست محال</p></div>
<div class="m2"><p>عشق و مستوری پیوند نگیرد به سریش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلقۀ زلف تو از دست دهم من هیهات</p></div>
<div class="m2"><p>تا نگیرد ز طلب دست ندارد درویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رند میخانه به کنجی خمش از آتش می</p></div>
<div class="m2"><p>سر صوفی به فلک میرود از دود حشیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوی خون آید از این چشم سیه‌دل که تو راست</p></div>
<div class="m2"><p>دلبرا دست من و دامن آن زلف پریش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جای عذر است چرا خنده به رندان نکنند</p></div>
<div class="m2"><p>زاهد صومعه را کاین همه خندند به ریش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند گفتم که مبو کاکل مشگین بتان</p></div>
<div class="m2"><p>عاقلان پند من افسانه شمرد این دل ریش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیّرا باش که تا خیمه زنم بر در شاه</p></div>
<div class="m2"><p>چرخ اگر تیر جفا پاک بپرداخت ز کیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شه اورنگ ولایت که در اقلیم وجود</p></div>
<div class="m2"><p>جز به تدبیر یمینش نرود کار از پیش</p></div></div>