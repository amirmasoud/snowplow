---
title: >-
    شمارهٔ  ۴۶
---
# شمارهٔ  ۴۶

<div class="b" id="bn1"><div class="m1"><p>چه بود با سر زلف تو کار جان بسر آید</p></div>
<div class="m2"><p>زتار او کسلد رشت وجان بدر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپر به پیش سپارد از این ستاره زمین را</p></div>
<div class="m2"><p>گر آسمان همه با آفتاب و با قمر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا به بین که چه حال است از انتظار تو ما را</p></div>
<div class="m2"><p>نه جان ز تن بدر آید نه قاصدی ز در آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر از نظر فکند تیره غمزه ام چه ملامت</p></div>
<div class="m2"><p>دگر نمانده نشانی ز من که در نظر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرشتۀ تو بدین ناز جان گداز و گر نه</p></div>
<div class="m2"><p>کجا تحمل کوهی ز طاقت بشر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار بار گرم بشکنی ز تیر جفا پر</p></div>
<div class="m2"><p>چو باز تیر تو بینم مرا ز شوق پر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو بجان ز کمان بر گذشت ناوک مژگان</p></div>
<div class="m2"><p>بیا بلب که ز جانانه پیک خوشخبر آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهال قدّ تو تا دیده دید یافت که آخر</p></div>
<div class="m2"><p>چه بار میدهد این نونهال اگر ببر آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جدا ز صورت جانان دلا ز دیده چه حاصل</p></div>
<div class="m2"><p>اگر شرشک سر آید بهل که دیده برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپر به پیش کشم من ز تیر ناز تو حاشا</p></div>
<div class="m2"><p>دریغ باشد تیری چنین که بر سپر آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمر چگونه ندزدد زبار بحر تو نیر</p></div>
<div class="m2"><p>چه طاقتی بود آنرا که کوه بر کمر آید</p></div></div>