---
title: >-
    شمارهٔ  ۱۲۳
---
# شمارهٔ  ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>عمر این گردش ایام چه خواهد بودن</p></div>
<div class="m2"><p>گر همه زهر بود کام چه خواهد بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور چشمان شما سر بسلامت بادا</p></div>
<div class="m2"><p>دور چرخ ار نشود رام چه خواهد بودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط و زلف تو بزنجیر کشیدند مرا</p></div>
<div class="m2"><p>تا ز چشمان تو پیغام چه خواهد بودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا دامن تقوی چو شد آلوده مرا</p></div>
<div class="m2"><p>در شط باده فکن جام چه خواهد بودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هین ز آغاز تو ایزلف مسلسل پیداست</p></div>
<div class="m2"><p>که مرا با تو سرانجام چه خواهد بودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیرّ این چامه که در وصف جمال تو سرود</p></div>
<div class="m2"><p>تا ز لعل لبت انعام چه خواهد بودن</p></div></div>