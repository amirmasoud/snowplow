---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>چندم از کشمکش موی تو جان اینهمه نیست</p></div>
<div class="m2"><p>عمر پیری کهن ای تازه جوان اینهمه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ما و هدف نیر نگاهت هیهات</p></div>
<div class="m2"><p>اعتبار من بی نام و نشان اینهمه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگرم از در لطفی اگر آید ساقی</p></div>
<div class="m2"><p>که سبک روحی اینرضل گران اینهمه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز ماندی زره شهوت نفس ای زاهد</p></div>
<div class="m2"><p>ذوق همخوابگی حور جنان این همه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند پوئی چو سکندر زپی آب بقا</p></div>
<div class="m2"><p>ترک ظلمات جهان کن که جهان اینهمه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالش خشت و کلاه نمد فقر کجاست</p></div>
<div class="m2"><p>لذت تخت جم و تاج کیان اینهمه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیمه ما بسر ذروه لاهوت زنید</p></div>
<div class="m2"><p>وسعت دایره کونمکان اینهمه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دمبدم ریزد اگر شهدروان زانلب نوش</p></div>
<div class="m2"><p>چه عجب حوصله تنگ دهان اینهمه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پنجه ساعد سیمین تو آزرده مباد</p></div>
<div class="m2"><p>ورنه در دل اثر تیر و کمان این همه نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حشم غمره باندازه بتاراج فرست</p></div>
<div class="m2"><p>فحت کشور غارت زدگان این همه نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیرا هلهله پیر زنان میکشدم</p></div>
<div class="m2"><p>باکم از کشمکش تیر زنان این همه نیست</p></div></div>