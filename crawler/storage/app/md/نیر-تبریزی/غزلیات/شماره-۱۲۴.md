---
title: >-
    شمارهٔ  ۱۲۴
---
# شمارهٔ  ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>دلا گر گوهر مقصود خواهی دیده دریا کن</p></div>
<div class="m2"><p>ز فیض دانۀ اشگ آستین پر درّ لالا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقوسین علایق چند چون پرگار سرگردان</p></div>
<div class="m2"><p>درون نه پای وجا از نقطۀ موهوم ادنی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فیض شمس لاهوتی در این نادوس ناسوتی</p></div>
<div class="m2"><p>بتهلیلات اکسیریه نفس مرده احیا کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدار الخیر حکمت نه رخ و در عین درویشی</p></div>
<div class="m2"><p>بنه اکلیل زر بر سر به تخت هر مسی جا کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نقش بقا خواهی در ای مرآت طبعانی</p></div>
<div class="m2"><p>هیولا را بصورت آر و صورترا هیولا کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دیو خیره در چاه طبیعت سرنگون تا کی</p></div>
<div class="m2"><p>بیا بر شکل انسانی نگاهی سوی بالا کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چون پورعمران طالب نور تجلائی</p></div>
<div class="m2"><p>عصای مسکنت بر دست گیر و سینه سینا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی کن جوهر روح و جسد با نقش لاهوتی</p></div>
<div class="m2"><p>ره توحید گیر و ترک تثلیث نصاری کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمیگویم ز ایجاد طبیعی سر بزن لیکن</p></div>
<div class="m2"><p>بتکلیف ازادای آنچه آنجا خواهی اینجا کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گنج عقل میراث پدر دست آور و بنشین</p></div>
<div class="m2"><p>بصدر علم و بر قدّوسیان تعلیم اسما کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو کرکس بر سر مردار دنیا پر زنان تا کی</p></div>
<div class="m2"><p>بقاف قرب نه پای و مکان بر فرق عنقا کن</p></div></div>