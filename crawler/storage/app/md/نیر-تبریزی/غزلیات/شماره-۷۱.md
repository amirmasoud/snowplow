---
title: >-
    شمارهٔ  ۷۱
---
# شمارهٔ  ۷۱

<div class="b" id="bn1"><div class="m1"><p>تیشه بر پا زدی ار داغ منش در دل بود</p></div>
<div class="m2"><p>آن قوی پنجه که در کوهکنی کامل بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بچه دل آینه عکس تو در آغوش کشید</p></div>
<div class="m2"><p>مگر از آه سحرگاهی ما غافل بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردم از سوز جگر لابه ز حد پیش رقیب</p></div>
<div class="m2"><p>حالت غرقه ندانست که در ساحل بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تند بگذشت و مرا سیل غم از سر بگذشت</p></div>
<div class="m2"><p>شکرها دارم از این برق که مستعجل بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بغرامت بکش ای زلف به بندم که ز عمر</p></div>
<div class="m2"><p>هر چه جز حرف جنون شد همه بیحاصل بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشگل خویش ببردم بادب پیش حکیم</p></div>
<div class="m2"><p>چون بسنجیدمش او نیز چو من جاهل بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر زلف تو فزون باد که با روی تو دوش</p></div>
<div class="m2"><p>مو بمو باز نمود آنچه مرا در دل بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد از بهر خدا پیشۀ تقوی نگرفت</p></div>
<div class="m2"><p>عشق را بار گران خواجه چو من کاهل بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم از گم شدۀ خویش نشانی جویم</p></div>
<div class="m2"><p>بر هر کس که شدم بیخود و لایعقل بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناگه از دیر برآمد صنمی باده فروش</p></div>
<div class="m2"><p>وه چگویم که چه فرخنده رخی مقبل بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای بوی من و او هر دو ز جا رفت و لیک</p></div>
<div class="m2"><p>پای او در دل وپای من از او در گل بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او روان گشت و من اندر عقبش در تک و پوی</p></div>
<div class="m2"><p>تا بدیری که در آن دیرگهش منزل بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محفلی دیدم و در وی بادب مغبچگان</p></div>
<div class="m2"><p>بسته صف در بر پیری که در آنمحفل بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیر آندیر مرا جام جمی داد کزو</p></div>
<div class="m2"><p>پیش چشم آبنه شد آنچه مرا مشگل بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنک آئینه در آنمی چو ز دودم دیدم</p></div>
<div class="m2"><p>کونهان در دل و اینکوشش من باطل بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نام مجنون ز جنون مشتهر آمد نیر</p></div>
<div class="m2"><p>هم بدین ره شدی ار ناصح ما عاقل بود</p></div></div>