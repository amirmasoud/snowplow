---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>از حد گذشت جلوه فروهل نقاب را</p></div>
<div class="m2"><p>زین تیره روز تر مپسند آفتاب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معذورمی ایصنم همه گر تندی است وجور</p></div>
<div class="m2"><p>مستی و از خطا نشناسی صواب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میگفت دل چو میزدمش بوسه بر دهان</p></div>
<div class="m2"><p>باید کشید تلخی این شکر اب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شست چشم مست تو تیر و کمان گرفت</p></div>
<div class="m2"><p>از چشم فتنه برد تمنای خواب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم میانه دولت چیست گفت هیچ</p></div>
<div class="m2"><p>ای من ببوسم آن لب شیرین جواب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بردی چو هوش من ز سر ایدوست دستگیر</p></div>
<div class="m2"><p>دانی که اختیار نباشد خراب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز درم درآید و پندارمش که اوست</p></div>
<div class="m2"><p>چون تشنه ای که آب شمارد سراب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رشگ آیدم که افتد از او سایه بر زمین</p></div>
<div class="m2"><p>ای آسمان دریچه به بند آفتاب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهد ز ذوق حور برقص است و در نماز</p></div>
<div class="m2"><p>دیگر مگو که عشق نباشد دواب را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیر شکیب از او بتغافل توان نمود</p></div>
<div class="m2"><p>از یاد تشنه گر بتوان برد آب را</p></div></div>