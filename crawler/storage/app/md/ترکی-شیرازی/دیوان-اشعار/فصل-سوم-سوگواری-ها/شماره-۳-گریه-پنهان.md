---
title: >-
    شمارهٔ  ۳ - گریهٔ پنهان
---
# شمارهٔ  ۳ - گریهٔ پنهان

<div class="b" id="bn1"><div class="m1"><p>کلک قضا نوشت چو دیوان فاطمه</p></div>
<div class="m2"><p>دست قدر گرفت گریبان فاطمه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مامش خدیجه چون به بهشت برین شتافت</p></div>
<div class="m2"><p>شد آشکار گریهٔ پنهان فاطمه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بابش نبی چو عالم فانی وداع کرد</p></div>
<div class="m2"><p>خاموش گشت شمع شبستان فاطمه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زوجش علی چو خانه نشین شد پس از نبی</p></div>
<div class="m2"><p>پژمرده گشت گلبن بستان فاطمه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرمنده گشت خاذن گنج دو دیده اش</p></div>
<div class="m2"><p>گوهر ز بسکه ریخت به دامان فاطمه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آتش فراق، تن و جان او گداخت</p></div>
<div class="m2"><p>جان ها شود فدای تن و جان فاطمه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باله سزاست کز زن و مرد جهان تمام</p></div>
<div class="m2"><p>سازند جان خویش به قربان فاطمه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فردا که آفتاب قیامت کند طلوع</p></div>
<div class="m2"><p>دارند خلق چشم، به احسان فاطمه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس که نام فاطمه را بشنود یقین</p></div>
<div class="m2"><p>سوزد دلش به حال پریشان فاطمه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>« ترکی » ز راه صدق و صفا و ادب زده است</p></div>
<div class="m2"><p>دست ولا به دامن احسان فاطمه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا رب به حق فاطمه و آل اطهرش</p></div>
<div class="m2"><p>او را شمر یکی ز غلامان فاطمه</p></div></div>