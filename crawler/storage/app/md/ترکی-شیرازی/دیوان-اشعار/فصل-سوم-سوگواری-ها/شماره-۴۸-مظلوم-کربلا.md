---
title: >-
    شمارهٔ  ۴۸ - مظلوم کربلا
---
# شمارهٔ  ۴۸ - مظلوم کربلا

<div class="b" id="bn1"><div class="m1"><p>چشمی که در عزای شه کربلا گریست</p></div>
<div class="m2"><p>کم نوریش مباد که به اله به جا گریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریان در این عزا نه همین چشم ما وتوست</p></div>
<div class="m2"><p>زین ماجرا فرشتهٔ عرش علا گریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این عزا که هست عزادارمصطفی</p></div>
<div class="m2"><p>روح القدس به بارگه کبریا گریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر کشته ای که اشرف اولاد آدم است</p></div>
<div class="m2"><p>آدم جدا گریست و، حوا جدا گریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بوالبشر گرفته و، تا ختم الانبیا</p></div>
<div class="m2"><p>هر یک جدا جدا به طریقی جدا گریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیسی در آسمان چهارم سحاب وار</p></div>
<div class="m2"><p>اندر عزای خامس آل عبا گریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوح و، خلیل و، یوشع و، خضر و، هود</p></div>
<div class="m2"><p>موسی و شیعث و، یونس از این ماجرا گریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یعقوب در مدینه کنعان علی الدوام</p></div>
<div class="m2"><p>اندر فراق یوسف کرب وبلا گریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برکشته ای که شد سر او از قفا جدا</p></div>
<div class="m2"><p>شیر خدا و، فاطمه و، مصطفی گریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر پیکری که گشت لگد کوب اسب ها</p></div>
<div class="m2"><p>زینب ، سکینه ، همچو یم موج زا گریست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آن سری که کرد سنان، بر سر سنان</p></div>
<div class="m2"><p>خیرالنسا شفیعهٔ روز جزا گریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر تشنه کامیش به کنار دو نهر آب</p></div>
<div class="m2"><p>در روضهٔ جنان، حسن مجتبی گریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر حلق نازکش که خنجر بریده شد</p></div>
<div class="m2"><p>در ساحت بهشت علی مرتضی گریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر اهل بیت او که شدند از جفا اسیر</p></div>
<div class="m2"><p>سجاد دل فگار، به شام بلا گریست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر غربت حسین امیر دیار عشق</p></div>
<div class="m2"><p>هر صبح و شام، دیدهٔ شاه و گدا گریست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>«ترکی» به یاد غربت مظلوم کربلا</p></div>
<div class="m2"><p>خون جای اشک، از مژه صبح و مسا گریست</p></div></div>