---
title: >-
    شمارهٔ  ۱۰۶ - ارض کربلا
---
# شمارهٔ  ۱۰۶ - ارض کربلا

<div class="b" id="bn1"><div class="m1"><p>ای ارض کربلا تو محل بلاستی!</p></div>
<div class="m2"><p>موسوم از این سبب تو به کرب و بلا ستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدفون به تربت تو بود گوشوار عرش</p></div>
<div class="m2"><p>برتر هزار بار، ز عرش علاستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خاک از آنکه مدفن سبط پیمبری</p></div>
<div class="m2"><p>هر درد را دوا و مرض را شفاستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنبر نی، تو بهتر ز عنبری</p></div>
<div class="m2"><p>چون قتلگاه سبط رسول خداستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ارض کربلا تو بهشتی ولی چرا؟</p></div>
<div class="m2"><p>محنت فزا و، معدن حزن و بکاستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهان کنند خاک تو را توتیای چشم</p></div>
<div class="m2"><p>آری به چشم اهل نظر توتیاستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه و گدا به سوی تو دارند التجا</p></div>
<div class="m2"><p>ای خاک پاک، ملجا شاه و گداستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>« ترکی» نوشت نام تو چون ای زمین پاک!</p></div>
<div class="m2"><p>غم بر غمش فزود، ز بس غم فزاستی</p></div></div>