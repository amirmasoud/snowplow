---
title: >-
    شمارهٔ  ۲۸ - کبوتران حرم
---
# شمارهٔ  ۲۸ - کبوتران حرم

<div class="b" id="bn1"><div class="m1"><p>به دشت کرب وبلا ازجفای شمر شریر</p></div>
<div class="m2"><p>شهید گشت عزیز دل بشیر و نذیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار ونهصد و پنجاه زخم بر تن داشت</p></div>
<div class="m2"><p>ز تیر و نیزه و زوبین وخنجر وشمشیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قتیل شد همه انصار او، ز پیر و جوان</p></div>
<div class="m2"><p>شهید شد همه اعوان، او صغیر وکبیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی فتاده ز پا، با قدی، چو سرو روان</p></div>
<div class="m2"><p>یکی طپیده به خون با رخی، چو بدر منیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست وپای گرفته یکی خضاب ز خون</p></div>
<div class="m2"><p>فرو بسته دهان، کودکی از خوردن شیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز راه کینه گروه دغا جدا کردند</p></div>
<div class="m2"><p>دو دست از تن عباس نامدار دلیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علی اصغر مه طلعتش ز شوق مکید</p></div>
<div class="m2"><p>به جای شیر، ز پستان مرگ، ناوک تیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدند آل پیمبر به کربلا سیراب</p></div>
<div class="m2"><p>همه ز چشمهٔ تیر و، ز جدول شمشیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپاه کوفی و شامی در آن زمین بلا</p></div>
<div class="m2"><p>کبوتران حرم را زدند جمله به تیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیام او همه بر باد رفت ز آتش کین</p></div>
<div class="m2"><p>عیال او همه گشتند خوار و زار و اسیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ریاض کرب و بلا شد ز بلبلان خالی</p></div>
<div class="m2"><p>به شاخه های گل و سرو ناکشیده صفیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دریغ شمع شرر پوش بزم عرفان را</p></div>
<div class="m2"><p>ز راه کینه کشیدند در غل و زنجیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فتاد شورشی اندر حریم آل رسول</p></div>
<div class="m2"><p>چنان که گوش فلک کر شد از فغان نفیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فغان اهل حرم رفت تا به عرش برین</p></div>
<div class="m2"><p>خروش اهل ولا رفت تا به چرخ اثیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبود قوت دل افسردگان در آن وادی</p></div>
<div class="m2"><p>به غیر آه سحرگاه و، نالهٔ شبگیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به غیر گریهٔ بر آل مصطفی «ترکی»</p></div>
<div class="m2"><p>خرابی دل ما را که می کند تعمیر</p></div></div>