---
title: >-
    شمارهٔ  ۱۱ - ضیاء دیدهٔ لیلا
---
# شمارهٔ  ۱۱ - ضیاء دیدهٔ لیلا

<div class="b" id="bn1"><div class="m1"><p>کیست یارب این جوان نو خط سیمین عذار؟</p></div>
<div class="m2"><p>کز شمیم طره اش گشته خجل مشگ تتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست یارب این جوان پر دل لشکر شکن؟</p></div>
<div class="m2"><p>کیست یارب این غضنفر فر دلیر نامدار؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست یارب این پلنگ افکن سوار شیرگیر؟</p></div>
<div class="m2"><p>کیست یارب این دلیر پر دل ضیغم شکار؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیست یارب این جوان ماه روی مهر چهر؟</p></div>
<div class="m2"><p>کیست یارب این هلال ابروی گردون اقتدار؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیست یارب این که از قدش بود طوبی خجل؟</p></div>
<div class="m2"><p>کیست یارب که از خدش بود مه شرمسار؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیست یارب اینکه پشت لشگری را بشکند؟</p></div>
<div class="m2"><p>یک تنه چون بر سمند با دپا گردد سوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست یارب اینکه از شمشیر تیزش روز جنگ؟</p></div>
<div class="m2"><p>دامن میدان شود از خون دشمن لاله زار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اله اله این جوان گویا بود ختم رسل</p></div>
<div class="m2"><p>یا که باشد ابن عمش حیدر دلدل سوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی غلط گفتم نه احمد نه وصی احمد است</p></div>
<div class="m2"><p>این علی اکبر «ع »بود شبه رسول کردگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این ضیاء دیدهٔ لیلا عزیز زینب است</p></div>
<div class="m2"><p>این شه لب تشنگان راهست پور نامدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حیف از یان قامت که از جور و جفای کوفیان</p></div>
<div class="m2"><p>بر زمین افتد ز پشت زین مرکب، سایه وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیق از این مویی که خون سرش رنگین شود</p></div>
<div class="m2"><p>در زمین کربلا از جور قوم بد شعار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آه از آن دم که از کین، منغذخونخوار زد</p></div>
<div class="m2"><p>تیغ بر فرق علی اکبر والا تبار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر زمین افتاد چون خورشید از بالای زین</p></div>
<div class="m2"><p>با دلی اندوهگین و با دو چشمی اشکبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از فراق مادرش بر سینه، داغ جان گداز</p></div>
<div class="m2"><p>از سنان و نیزه ها بر جسم، زخم بی شمار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روبه سوی خیمه ها کرد و کشید از دل فغان</p></div>
<div class="m2"><p>کی امام انس و جان، ای حجت پروردگار!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای پدر از مرحمت خود را ببالینم رسان!</p></div>
<div class="m2"><p>ای پدر دریاب پورت را که رفت از دست کار!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون پدر در خیمه بانگ نالهٔ اکبر شنید</p></div>
<div class="m2"><p>جست ازجا چون سپند و رفت سوی کارزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر سر بالین اکبر رفت و از دل بر کشید</p></div>
<div class="m2"><p>ناله ای کز ناله اش افتاد در گیتی شرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روی خود بنهاد بر روی جوان نو خطش</p></div>
<div class="m2"><p>وز محاسن می چکید ش خون اکبر لعل وار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دیده بر روی پدر بگوشد با حسرت پسر</p></div>
<div class="m2"><p>گفت کای سلطان مضلومان، مرا معذوردار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در کنار تو بود این کسر من ای پدر!</p></div>
<div class="m2"><p>ساعت دیگر که می گیرد سرت را در کنار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این بگفت و دیده حق بین، به روی هم نهاد</p></div>
<div class="m2"><p>کرد مرغ روح پاکش زآشیان تن فرار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شاه دین گفت ای جوانان بنی هاشم برید</p></div>
<div class="m2"><p>نعش اکبر را به سوی خیمه با حال فگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از شراره آه و، اشک و، نالهٔ اهل حرم</p></div>
<div class="m2"><p>گشت در کرب و بلا شور قیامت آشکار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>«ترکی» ار خواهی شود راضی زتو ختم رسل</p></div>
<div class="m2"><p>روز و شب در ماتم اکبر زمژگان خون ببار</p></div></div>