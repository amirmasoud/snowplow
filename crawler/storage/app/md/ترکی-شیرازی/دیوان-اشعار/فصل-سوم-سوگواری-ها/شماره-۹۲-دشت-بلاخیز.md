---
title: >-
    شمارهٔ  ۹۲ - دشت بلاخیز
---
# شمارهٔ  ۹۲ - دشت بلاخیز

<div class="b" id="bn1"><div class="m1"><p>برادر جان مگر رفته به خوابی!</p></div>
<div class="m2"><p>که زینب را نمی گویی جوابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برایت درد دل تا کی شمارد</p></div>
<div class="m2"><p>چه خواب است اینکه بیداری ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی حق داری ای جان برادر!</p></div>
<div class="m2"><p>که میدانم نداری در بدن سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا؟ ازتن جدا گشته سر تو</p></div>
<div class="m2"><p>چرا؟ صد پاره گشته پیکر تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به همراه تو از شهر مدینه</p></div>
<div class="m2"><p>در اینجا آمدم ای بی قرینه!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک آواره کرد از خانمانم</p></div>
<div class="m2"><p>فراقت زد شرر، برجسم و جانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چون رفتی از این دنیای فانی</p></div>
<div class="m2"><p>نخواهم بی تو دیگر زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امان از درد بی درمان زینب</p></div>
<div class="m2"><p>اجل کو، تا بگیرد جان زینب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از تو کوفیان، بردند یکجا</p></div>
<div class="m2"><p>تمام هستی ما را به یغما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه ظلمی کوفیان با ما نمودند</p></div>
<div class="m2"><p>عجب مهمان نوازی ها نمودند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را لب تشنه، سر از تن بریدند</p></div>
<div class="m2"><p>تن پاکت به خاک و خون کشیدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زدند آتش، سراسر خیمه ها را</p></div>
<div class="m2"><p>به بازو ریسمان، بستند ما را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو و این کوفه، این دشت بلا خیز</p></div>
<div class="m2"><p>من و شام خراب محنت انگیز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو باش اینجا مصاحب با جوانان</p></div>
<div class="m2"><p>من اندر شام، غمخوار یتیمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو و این نوخطان نورسیده</p></div>
<div class="m2"><p>من و این مادران داغدیده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو اینجا باش با عباس و اکبر</p></div>
<div class="m2"><p>من و کلثوم و لیلای مکدر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برادر جان تو اینجا باش آرام</p></div>
<div class="m2"><p>خداحافظ که من رفتم سوی شام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برادر جان در این دامان مقتل</p></div>
<div class="m2"><p>تو باش و ساربان پست و بجدل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از این آتش که «ترکی» را به جان است</p></div>
<div class="m2"><p>زغم سوز نهان او عیان است</p></div></div>