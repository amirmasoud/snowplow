---
title: >-
    شمارهٔ  ۵۵ - اهل حرم
---
# شمارهٔ  ۵۵ - اهل حرم

<div class="b" id="bn1"><div class="m1"><p>شاهی که بود آینهٔ حسن ذوالجلال</p></div>
<div class="m2"><p>صاحب عزای اوست خداوند لایزال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درکربلا ز خنجر بیداد، شد قتیل</p></div>
<div class="m2"><p>صد پاره اوفتاد تنش در صف قتال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمر از قفا برید سرش را ولی نریخت</p></div>
<div class="m2"><p>یک جرعه آب، بر لبش از چشمهٔ زلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جسمی که بود زینت آغوش مصطفی</p></div>
<div class="m2"><p>شد از سم ستور، در آن دشت پایمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در داغداشت عشق، ز جور حرامیان</p></div>
<div class="m2"><p>گردید یا رب از چه سبب خون او حلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش ز راه کین، به سراپرده اش زدند</p></div>
<div class="m2"><p>آنان که بوده اند ستمکار و بدسگال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل حرم شدند گریزان ز هر طرف</p></div>
<div class="m2"><p>زن های سال خورده و، طفلان خردسال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن میان سکینه مظلومه دخترش</p></div>
<div class="m2"><p>پیوسته بود وا ابتایش زبان حال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«ترکی» حدیث شاه شهید و شهادتش</p></div>
<div class="m2"><p>تا روز رستخیز، نخواهد شد از خیال</p></div></div>