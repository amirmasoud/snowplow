---
title: >-
    شمارهٔ  ۴۰ - مقتل خون
---
# شمارهٔ  ۴۰ - مقتل خون

<div class="b" id="bn1"><div class="m1"><p>زلف اکبر چو ز خون سر او تر گردید</p></div>
<div class="m2"><p>عقل گفتا که عجین مشک، به عنبر گردید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر زمین خون ز گلوی شه لب تشنه حسین</p></div>
<div class="m2"><p>آنقدر ریخت که با خاک، مخمر گردید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدنی را که بدی بسترش آغوش نبی</p></div>
<div class="m2"><p>عاقبت خاک زمین، بالش و بستر گردید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر فرزند نبی گشت چو پنهان به تنور</p></div>
<div class="m2"><p>زین حکایت دل صدیقه پر آذر گردید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عوض شیر که نوشد، به سر دست پدر</p></div>
<div class="m2"><p>پاره از تیر، گلوی علی اصغر گردید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد قتل پسر فاطمه در مقتل خون</p></div>
<div class="m2"><p>چشم زینب یم اشک از مژهٔ تر گردید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه کز کرب و بلا زینب محزون تا شام</p></div>
<div class="m2"><p>همسفر با پسر سعد بد اختر گردید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشته شد اکبر و لیلا ز غمش مجنون شد</p></div>
<div class="m2"><p>داغ فرزند، نصیب دل مادر گردید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت در کنج خرابه، چو مکان زینب</p></div>
<div class="m2"><p>روز روشن به وی از شام، سیه تر گردید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسکه خونابه فشاند از مژه بر رخ «ترکی»</p></div>
<div class="m2"><p>دفتر شعر، ز خوناب دلش تر گردید</p></div></div>