---
title: >-
    شمارهٔ  ۹۸ - شعلهٔ سخن
---
# شمارهٔ  ۹۸ - شعلهٔ سخن

<div class="b" id="bn1"><div class="m1"><p>پدر زهجر تو کاهیده چون هلال تنم</p></div>
<div class="m2"><p>خوشا دمی که به ماه رخت نظاره کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زکربلای تو بویی گرم رسد به مشام</p></div>
<div class="m2"><p>هزار بار بود به زنافهٔ ختنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن زمان که ز آغوش تو جدا گشتم</p></div>
<div class="m2"><p>هنوز بوی تو آید ز چاک پیرهنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خفته ای به دل خاک، با تن صد چاک</p></div>
<div class="m2"><p>من از فراق تو حیران زکار خویشتنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از کجا و مدینه کجا و شام کجا؟</p></div>
<div class="m2"><p>فلک برای چه آواره کرد از وطنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبعد قتل تو ای خسرو سلیمان جاه!</p></div>
<div class="m2"><p>زمانه کرد گرفتار ظلم اهرمنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به راه شام نبودی ببینی ای بابا!</p></div>
<div class="m2"><p>که خصم، گردن و بازو ببست با رسنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سواره ناقهٔ عریان، به کوچه های دمشق</p></div>
<div class="m2"><p>به حال زار کشاندند بین مرد و زنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شام آی و مرا از قفس رهایی بخش</p></div>
<div class="m2"><p>تو گلشن من و من عندلیب آن چمنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زخاک، روز قیامت، چو سر برون آرم</p></div>
<div class="m2"><p>هنوز بوی فراق تو آید از کفنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو آب دیدهٔ «ترکی» به آتش دل ریخت</p></div>
<div class="m2"><p>فرو نشاند در این بزم، شعلهٔ سخنم</p></div></div>