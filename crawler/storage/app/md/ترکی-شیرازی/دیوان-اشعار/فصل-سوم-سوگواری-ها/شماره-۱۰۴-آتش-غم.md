---
title: >-
    شمارهٔ  ۱۰۴ - آتش غم
---
# شمارهٔ  ۱۰۴ - آتش غم

<div class="b" id="bn1"><div class="m1"><p>چه شد که ای گل من! این چنین تو پژمردی</p></div>
<div class="m2"><p>چراغ محفل من از چه زود افسردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر به خواب چه دیدی که لب فرو بستی</p></div>
<div class="m2"><p>دل شکسته ما را دوباره بشکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوم فدای تو ای نوگل گلستانم!</p></div>
<div class="m2"><p>چرا خموش شدی بلبل خوش الحانم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو از برادر من، یادگار من بودی</p></div>
<div class="m2"><p>انیس و مونس شب های تار من بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به راه شام، که بودی ز جان همآوردم</p></div>
<div class="m2"><p>ز سوزن مژه خارت ز پا درآوردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان را چو بسیار صدمه ها دیدم</p></div>
<div class="m2"><p>تو ز شام، ز کرب و بلا رسانیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرید از قفس تن، کبوتر جانت</p></div>
<div class="m2"><p>الهی آنکه شود عمه ات به قربانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حیرتم که چرا عمه ات نمی میرد؟</p></div>
<div class="m2"><p>اجل کجاست چرا جان من نمی گیرد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه صدمه ها که ز کفار دیدی ای دختر!</p></div>
<div class="m2"><p>به روی خار پیاده دویدی ای دختر!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی نگفت که این طفل، بی مددکار است</p></div>
<div class="m2"><p>کسی نگفت پدر کشته و عزادار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخت ز ضربت سیلی خصم، گشت کبود</p></div>
<div class="m2"><p>مگر که جرم تو ای نازدانه طفل چه بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو پاره جگر و نور و دیده ام بودی</p></div>
<div class="m2"><p>گل ریاض دل غم رسیده ام بودی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گمان نداشتم ای دختر برادر من!</p></div>
<div class="m2"><p>که می روی تو بهنگام طفلی از بر من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز رفتنت نه همین جان ناتوان سوخت</p></div>
<div class="m2"><p>که آتش غم تو در دلم شرر افروخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکینه خواهرت از درد هجر، گریان است</p></div>
<div class="m2"><p>ببین ز مرگ تو چون موی خود پریشان است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون کفن، از برای تو از کجا آرم</p></div>
<div class="m2"><p>که در خرابه، تنت را به خاک بسپارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دریغ و درد که کامی ندیدی از دنیا</p></div>
<div class="m2"><p>چو مرغ دام گسسته، پریدی از دنیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کلام «ترکی» از این رهگذر اثر دارد</p></div>
<div class="m2"><p>که از مصائب زینب دلش خبر دارد</p></div></div>