---
title: >-
    شمارهٔ  ۶۵ - صاحب عزا
---
# شمارهٔ  ۶۵ - صاحب عزا

<div class="b" id="bn1"><div class="m1"><p>به بزم ماتم سبط نبی باشد خدا حاضر</p></div>
<div class="m2"><p>در آن ماتم سرا باشد جناب مصطفی حاضر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بساط زیر پای مردم از بال ملک باشد</p></div>
<div class="m2"><p>در آن بزمی که باشد شافع روز جزا حاضر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود دور از مروت، با درونی شاد بنشستن</p></div>
<div class="m2"><p>به هر بزم عزا کآنجا بود صاحب عزا حاضر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تکبر کم کن ای دل! با ادب بنشین در آن مجلس</p></div>
<div class="m2"><p>که می باشد در آن مجلس، علی مرتضی حاضر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زانوی الم سر را بنه گردن مکش زیرا</p></div>
<div class="m2"><p>که باشد مو پریشان، حضرت خیرالنساء حاضر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزای سرور لب تشنه گان، هر جا شود بر پا</p></div>
<div class="m2"><p>بود روح تمام انبیاء و اولیاء حاضر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزاداری کن از بهر حسین ابن علی زیرا</p></div>
<div class="m2"><p>بود در بزم سوگ او امام مجتبی حاضر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیاد آور گه جان دادن فرزند زهرا را</p></div>
<div class="m2"><p>که بر بالین نبودش کس، به جز شمر دغا حاضر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر در عالم صورت، کسی حاضر نبود اما</p></div>
<div class="m2"><p>به معنی بود جدش با تمام انبیا حاضر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی یارای سر ببریدنش از تن نبود آری</p></div>
<div class="m2"><p>اگر آنجا نبودی شمر شوم بی حیا حاضر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شدند از بهر قتل آن شه بی کس، در آن صحرا</p></div>
<div class="m2"><p>به حکم زادهٔ مرجانه، فوج اشقیا حاضر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پی خون ریزی آن پادشاه یثرب و بطحا</p></div>
<div class="m2"><p>سپاه کوفیان کردند تیغ و نیزه ها حاضر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیاد نینوای او بسان نی، نوا سر کن</p></div>
<div class="m2"><p>نبودی آن زمان چون در دیار نینوا حاضر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عزاداری شاه کربلا را «ترکیا» اینک</p></div>
<div class="m2"><p>غنیمت دان نبودی گر به دشت کربلا حاضر</p></div></div>