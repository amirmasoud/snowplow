---
title: >-
    شمارهٔ  ۳۵ - طایر باغ جنان
---
# شمارهٔ  ۳۵ - طایر باغ جنان

<div class="b" id="bn1"><div class="m1"><p>ای شهیدی که لب تشنه بریدند سرت را!</p></div>
<div class="m2"><p>سوختند از پی یک قطرهٔ آبی جگرت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه خواندند به مهمانیت از شهر مدینه</p></div>
<div class="m2"><p>کوفیان از چه بریدند لب تشنه سرت را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طایر باغ جنان بودی و، با سنگ شقاوت</p></div>
<div class="m2"><p>بشکستند به هم دوزخیان، بال و پرت را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه شکستند همین صدر تو را از سم اسبان</p></div>
<div class="m2"><p>که شکستند دل مادر و جد و پدرت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوفیان از پدرت شرم نکردند و بریدند</p></div>
<div class="m2"><p>با دم تیغ ستم، رشتهٔ عمر پسرت را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لعن حق باد بر آن طایفهٔ زشت نهادی</p></div>
<div class="m2"><p>که نمودند خم از مرگ برادر کمرت را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاش از کرب و بلا قاصدی از مهر رساندی</p></div>
<div class="m2"><p>در نجف نزد علی ساقی کوثر خبرت را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی علی یک نظر ازلطف سوی کرب و بلا کن!</p></div>
<div class="m2"><p>غرق در خون بنگر پیکر شمس و قمرت را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاش در آن دم آخر که ز تن، رفت روانت</p></div>
<div class="m2"><p>مادرت بود که بستی ز وفا چشم ترت را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترسم ای خسرو خوبان! که برد «ترکی» مسکین</p></div>
<div class="m2"><p>به لحد همره خود آرزوی خاک درت را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز و شب، پیشهٔ او نیست بجز نوحه سرایی</p></div>
<div class="m2"><p>ناامید از سر کویت منما نوحه گرت را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از تو آن روز که بر فرق نهی تاج شفاعت</p></div>
<div class="m2"><p>چشم دارد که ز وی باز نگیری نظرت را</p></div></div>