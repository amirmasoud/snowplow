---
title: >-
    شمارهٔ  ۱۹ - خلاصهٔ ناس
---
# شمارهٔ  ۱۹ - خلاصهٔ ناس

<div class="b" id="bn1"><div class="m1"><p>گرفت مشک وروان گشت آن خلاصهٔ ناس</p></div>
<div class="m2"><p>هژبر بیشهٔ مردی و مردمی عباس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رساند جنگ کنان خویش را به شط فرات</p></div>
<div class="m2"><p>نمود مشک پر از آب، آن سپهر اساس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدم نهاد برون تشنه لب، ز شط فرات</p></div>
<div class="m2"><p>نمود حمله بر آن کافران حق نشناس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشید تیغ و، رجز خواند و، حمله کرد چو شیر</p></div>
<div class="m2"><p>ز هم سپاه عدو را درید چون کرباس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بسکه بر زبر هم فتاده بود قتیل</p></div>
<div class="m2"><p>زمین معرکه گفتی مگر نمود آماس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ضرب بازوی او پر دلان لشکر را</p></div>
<div class="m2"><p>فتاد لرزه بر اندامشان ز بیم و هراس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بسکه بر بدن زخم تیر و نیزه رسید</p></div>
<div class="m2"><p>ازآن گروه بد آیین بدتر از خناس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بریده دست و، بدن چاک چاک و، خالی مشک</p></div>
<div class="m2"><p>ز صدر زین، به زمین خورد آن خلاصهٔ ناس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنارشط، لب تشنه سپرد جان اما</p></div>
<div class="m2"><p>گرفت از کف ساقی کوثر آب، عباس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز قتل حضرت عباس « ترکیا » شب و روز</p></div>
<div class="m2"><p>بریز اشک و ببر کن ز غم، سیاه لباس</p></div></div>