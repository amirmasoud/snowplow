---
title: >-
    شمارهٔ  ۷۰ - بادهٔ عشق
---
# شمارهٔ  ۷۰ - بادهٔ عشق

<div class="b" id="bn1"><div class="m1"><p>ای کشته ای که کعبهٔ عشق است کوی تو!</p></div>
<div class="m2"><p>روی تمام عالمیان است سوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را شراب کوثر و تنسیم گو مباش</p></div>
<div class="m2"><p>چون خورده ایم بادهٔ عشق از سبوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب حیات و کوثر و تنسیم و سلسبیل</p></div>
<div class="m2"><p>هیچند پیش خاک در مشکبوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو در دل منی و من از فرط اشتیاق</p></div>
<div class="m2"><p>هرسو روانه ام ز پی جستجوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بزم شادمانی و گر مجلس عزاست</p></div>
<div class="m2"><p>نقل مجالس است همه گفتگوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبی که مهر فاطمه بود از چه کوفیان</p></div>
<div class="m2"><p>بستند از طریق شقاوت به روی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازم به همت تو که در سجده زسر تیغ</p></div>
<div class="m2"><p>شد خون پاک حنجرت آب وضوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسیده بود زیر گلوی تو را نبی</p></div>
<div class="m2"><p>خنجر حیا نمود که برد گلوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کاینات غرقهٔ بحر فنا شوند</p></div>
<div class="m2"><p>کی خونبهاست یکسره بر تار موی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاها!هوای کرب و بلایت به سر مراست</p></div>
<div class="m2"><p>مگذار زیر خاک برم آرزوی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر جان رود ز قالب «ترکی» به کربلا</p></div>
<div class="m2"><p>جان دوباره بر تنش آید ز بوی تو</p></div></div>