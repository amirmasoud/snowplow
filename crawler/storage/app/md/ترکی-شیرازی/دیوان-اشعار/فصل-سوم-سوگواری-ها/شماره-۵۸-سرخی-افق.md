---
title: >-
    شمارهٔ  ۵۸ - سرخی افق
---
# شمارهٔ  ۵۸ - سرخی افق

<div class="b" id="bn1"><div class="m1"><p>ز چیست شور و فغان در جهان بپاست هنوز؟</p></div>
<div class="m2"><p>خروش و ولوله در عرش کبریاست هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قتل سبط نبی قرن ها گذشته ولی</p></div>
<div class="m2"><p>لوای ماتم جانسوز او به پاست هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزای سبط پیمبر کجا شود منسوخ</p></div>
<div class="m2"><p>به هر کجا نگرم مجلس عزاست هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برهنه سر، ز چه رو سر زند ز مشرق شمس</p></div>
<div class="m2"><p>سر بریده مگر روی نیزه هاست هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چیست جامهٔ نیلی سپهر را در بر؟</p></div>
<div class="m2"><p>مگر عزای گل باغ مصطفی ست هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سمت کرب وبلا سرخی افق از چیست؟</p></div>
<div class="m2"><p>مگر به روی زمین، خون کشته هاست هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه آتشی ست که دل های خلق کرده کباب</p></div>
<div class="m2"><p>مگر که شعلهٔ آتش، به خیمه هاست هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود ز خلد سوی کوفه دیدهٔ زهرا</p></div>
<div class="m2"><p>تنور خانهٔ خولی، مگر به جاست هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صدای نالهٔ زینب به گوش می آید</p></div>
<div class="m2"><p>مگر که شمر، به صحرای کربلاست هنوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسد نوای جگر سوز غم، به گوش جهان</p></div>
<div class="m2"><p>مگر نوای یتیمان، به نینواست هنوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بانگ نالهٔ طفلان داغدار حسین</p></div>
<div class="m2"><p>فضای گنبد گردون، پر از صداست هنوز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن زمان که پیاده دویده در ره شام</p></div>
<div class="m2"><p>مگر رقیه گرفتار خار پاست هنوز؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به طشت زر، سر سلطان دین و چوب یزید</p></div>
<div class="m2"><p>مگر که با لب و دندانش آشناست هنوز؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عزای آنکه عزادار او خدا باشد</p></div>
<div class="m2"><p>زیاد کس نرود بین عزا بپاست هنوز</p></div></div>