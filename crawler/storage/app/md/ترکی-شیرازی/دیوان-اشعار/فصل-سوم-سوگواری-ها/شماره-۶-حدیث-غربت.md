---
title: >-
    شمارهٔ  ۶ - حدیث غربت
---
# شمارهٔ  ۶ - حدیث غربت

<div class="b" id="bn1"><div class="m1"><p>خوش آن که درد دل خویش با صبا گویم</p></div>
<div class="m2"><p>حدیث عشق، بر آن یار آشنا گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمی که گشته نهان از زمانه دردل من</p></div>
<div class="m2"><p>بر آن سرم که به صد شور بر ملا گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر کجا که بود مجلس عزای رضا</p></div>
<div class="m2"><p>ز دیده خون بفشانم رضا رضا گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رضا ، به شهر خراسان غریب وتنها ماند</p></div>
<div class="m2"><p>حدیث غربت او را به صد نوا گویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز غم زنم به دل وجان شیعیان آتش</p></div>
<div class="m2"><p>اگر ز غربت مظلوم کربلا گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیکرش بسرایم سخن که شد پامال</p></div>
<div class="m2"><p>و یا ز راس منیرش که شد جدا گویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم یتیمی طفلش سکینه برشمرم</p></div>
<div class="m2"><p>و یا ز زینب و کلثوم بی نوا گویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جدا جدا دل اهل عزا بسوزانم</p></div>
<div class="m2"><p>اگر مصائب او را جدا جدا گویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسیده کار به جایی که رو کنم به نجف</p></div>
<div class="m2"><p>غریبی پسرش را به مرتضی گویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تشنه کامی اورو کنم به سوی بقیع</p></div>
<div class="m2"><p>برای فاطمه با چشم پر بکا گویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توجهی به مدینه کنم به چشم پرآب</p></div>
<div class="m2"><p>به خون طپیدن او را به مصطفی گویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حدیث غربت مظلوم کربلا « ترکی »</p></div>
<div class="m2"><p>به هر کجا که شود مجلسی بپا گویم</p></div></div>