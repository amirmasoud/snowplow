---
title: >-
    شمارهٔ  ۴۴ - چشم اشکبار
---
# شمارهٔ  ۴۴ - چشم اشکبار

<div class="b" id="bn1"><div class="m1"><p>شبان تیره به درگاه کردگار، بنالم</p></div>
<div class="m2"><p>ز جور چرخ و، ز بیداد روزگار، بنالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نی به حال شهیدان نینوا بخروشم</p></div>
<div class="m2"><p>به جان نثاری یاران جان نثار، بنالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو در خیال من آید نهال قامت اکبر</p></div>
<div class="m2"><p>سحاب وار، بگریم هزار وار، بنالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنم چو یاد من از حلق پارهٔ علی اصغر</p></div>
<div class="m2"><p>به تشنه کامی آن طفل شیرخوار، بنالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای آنکه خزان شد بهار گلشن طاها</p></div>
<div class="m2"><p>ز شاخ شعله برآید اگر بهار، بنالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فراز نیزه سر شاه در مقابل زینب</p></div>
<div class="m2"><p>به خاطر آرم و، از جور نیزه دار، بنالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیار شام و خرابه مرا رسد چو به خاطر</p></div>
<div class="m2"><p>به یاد زینب آواره از دیار، بنالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چکد ز دیده سرشکم ز گریه های سکینه</p></div>
<div class="m2"><p>به دختری که شد از هجر باب، زار بنالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جای اشک رود سیل خون، ز دیدهٔ «ترکی»</p></div>
<div class="m2"><p>ز سوز سینه و با چشم اشکبار، بنالم</p></div></div>