---
title: >-
    شمارهٔ  ۸۵ - چشمهٔ خنجر
---
# شمارهٔ  ۸۵ - چشمهٔ خنجر

<div class="b" id="bn1"><div class="m1"><p>از جفا جورت ای چرخ ستمگستر، دریغ!</p></div>
<div class="m2"><p>کینه و ظلم تو زد آتش به خشک و تر، دریغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبز و خرم شد نهال آرزوهای یزید</p></div>
<div class="m2"><p>شد خزان آخر نهال آل پیغمبر، دریغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میر یثرب را چو منزل شد به دشت کربلا</p></div>
<div class="m2"><p>کشتی اش افکند در بحر بلا لنگر، دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داشت منزل گر چه آن شه، بر لب شط فرات</p></div>
<div class="m2"><p>تشنه لب نوشید آب از چشمهٔ خنجر، دریغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و سر بادا فدای آن شهیدی کز وفا</p></div>
<div class="m2"><p>کرد او در راه امت، ترک جان و سر، دریغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن سری کز موی او زهرا همی شستی غبار</p></div>
<div class="m2"><p>خولی دون، جای دادش روی خاکستر، دریغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان سلیمان زمان، اهریمنی از کین برید</p></div>
<div class="m2"><p>نازنین انگشت او از بهر انگشتر، دریغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صف شکن عباس را در پای نهر علقمه</p></div>
<div class="m2"><p>هر دو دست از کین، جدا کردند از پیکر، دریغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد ز تیغ منقذ ابن مره عبدی دون</p></div>
<div class="m2"><p>غرق در خون جعد گیسوی علی اکبر ، دریغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قاسم کوکب لقا را در مصاف عاشقی</p></div>
<div class="m2"><p>شد به جای رخت دامادی، کفن در بر، دریغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راست آمد بر نشان، تیر از کمان حرمله</p></div>
<div class="m2"><p>بر گلوی نازک خشک علی اصغر دریغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زینبی را کآفتاب از ماه رویش شرم داشت</p></div>
<div class="m2"><p>شد اسیر خصم دون، در کوی و در معبر، دریغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>«ترکی» این مرثیه جان سوز را تا می نوشت</p></div>
<div class="m2"><p>سیل خوناب دو چشمش بر گذشت از سر، دریغ</p></div></div>