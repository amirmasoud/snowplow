---
title: >-
    شمارهٔ  ۱۰۲ - ستاره می شمرم
---
# شمارهٔ  ۱۰۲ - ستاره می شمرم

<div class="b" id="bn1"><div class="m1"><p>پدر ز درد فراقت خون شده جگرم</p></div>
<div class="m2"><p>ز هجر روی تو چون طایر شکسته پرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدر بیا و ز بحر غمم رهایی بخش</p></div>
<div class="m2"><p>که سیل اشک، ز هجرت رسیده تا کمرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز دیده بود از غم تو خون جاری</p></div>
<div class="m2"><p>چو طفل اشک، فکندی چرا تو از نظرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرابه منزل و لخت جگر، طعام من است</p></div>
<div class="m2"><p>ببین زمانه چسان کرده خوار و دربدرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر نظر به مه آسمان نخواهم کرد</p></div>
<div class="m2"><p>به خواب اگر شبی آید رخ تو در نظرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو آفتاب رخت در تنور کرد غروب</p></div>
<div class="m2"><p>بیاد روی تو شب ها ستاره می شمرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبان تیره دو چشمم نمی رود در خواب</p></div>
<div class="m2"><p>چگونه خواب به چشمم رود که بی پدرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ربود خواب زچشم تمام اهل حرم</p></div>
<div class="m2"><p>فغان نیم شب و، آه و نالهٔ سحرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهای قطرهٔ خون گلوی تو نشود</p></div>
<div class="m2"><p>به جای اشک، اگر خون رود ز چشم ترم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرت به طشت طلا و تنت به کرب و بلا</p></div>
<div class="m2"><p>کجا روم چکنم شکوه جانب که برم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در این خرابه تنم ز آفتاب گرم گداخت</p></div>
<div class="m2"><p>پدر بیا ز وفا سایه ای فکن به سرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا و دختر خود را از انتظار برآر</p></div>
<div class="m2"><p>روا مدار که من، از غم تو جان سپرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز گریه، دیدهٔ «ترکی» چو بحر عمان شد</p></div>
<div class="m2"><p>ببین که صفحهٔ دفتر پر است از گهرم</p></div></div>