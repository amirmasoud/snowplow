---
title: >-
    شمارهٔ  ۸۷ - سلیمان کربلا
---
# شمارهٔ  ۸۷ - سلیمان کربلا

<div class="b" id="bn1"><div class="m1"><p>فغان ز کج روی روزگار و مکر و فنش</p></div>
<div class="m2"><p>که پور فاطمه را دور کرد از وطنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریغ و درد سلیمان کربلا را کرد</p></div>
<div class="m2"><p>زمانه سخت لگدکوب ظلم اهرمنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کینه گشت تنش پایمال سم ستور</p></div>
<div class="m2"><p>شهی که جان جهانی،فدای جان و تنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حمیت عربی بین، که با تن عریان</p></div>
<div class="m2"><p>به خاک رفت و نکردند کوفیان کفنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان آنکه به پوشند بر تنش کفنی</p></div>
<div class="m2"><p>ز پیکرش بربودند کهنه پیرهنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شام رفت سرش در میان طشت طلا</p></div>
<div class="m2"><p>یزید زد ز ستم، چوب بر لب و دهنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز داس ظلم و ستم، بوستان طاها را</p></div>
<div class="m2"><p>ز بیخ و بن ببریدند سرو یا سمنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنار نهر، لب تشنه، شد جدا افسوس</p></div>
<div class="m2"><p>دو دست از تن عباس میرصف شکنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا ببین ز نجف یا علی تو زینب را</p></div>
<div class="m2"><p>که خصم بسته به بازو، ز راه کین، رسنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشسته بر سر نعش پدر، به چشم پرآب</p></div>
<div class="m2"><p>سکینه آنکه بود عندلیب خوش سخنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبس به پیکر شه، بود زخم بر روی زخم</p></div>
<div class="m2"><p>نبود جای یکی بوسه در همه بدنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگرنه سوز حسین است بر دل «ترکی»</p></div>
<div class="m2"><p>چگونه شعله زند بر دل آتش سخنش</p></div></div>