---
title: >-
    شمارهٔ  ۶۱ - سیلاب اشک
---
# شمارهٔ  ۶۱ - سیلاب اشک

<div class="b" id="bn1"><div class="m1"><p>ای دل! بیا که ناله و آه و فغان کنیم</p></div>
<div class="m2"><p>سیلاب اشک، از مژه بر رخ روان کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهی زنیم بر سر و،گه جامه بردریم</p></div>
<div class="m2"><p>گه آشکار گریه و، گاهی نهان کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد آوریم واقعهٔ دشت کربلا</p></div>
<div class="m2"><p>شوری بپا چو حشر، در این خاکدان کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آریم در نظر چو لب تشنهٔ حسین</p></div>
<div class="m2"><p>قطع نظر، ز خوردن آب روان کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گه ز آب سرد گلویی کنیم تر</p></div>
<div class="m2"><p>یاد از گلوی خشک شه انس و جان کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوزیم بهر اکبر و عباس نامدار</p></div>
<div class="m2"><p>گریه گهی بر این و، گهی بهر آن کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالیم بر اسیری زن های داغدار</p></div>
<div class="m2"><p>افغان به دستگیری آن بی کسان کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب تشنه، کودکان حسین از جفای خصم</p></div>
<div class="m2"><p>یکدم بیا که گریه بر آن کودکان کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این گریه چون معامله با سبط مصطفی ست</p></div>
<div class="m2"><p>حاشادر این معامله گر ما زیان کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این جهان دو دانهٔ اشکی که میدهیم</p></div>
<div class="m2"><p>شاید که توشهٔ سفر آن جهان کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رو آوریم جانب در گاه بی نیاز</p></div>
<div class="m2"><p>دست دعا بلند سوی آسمان کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>«ترکی» ز بعد گریهٔ بر سبط مصطفی</p></div>
<div class="m2"><p>آن به دعا به قاطبهٔ شیعیان کنیم</p></div></div>