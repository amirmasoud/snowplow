---
title: >-
    شمارهٔ  ۵۷ - شراره آه دل
---
# شمارهٔ  ۵۷ - شراره آه دل

<div class="b" id="bn1"><div class="m1"><p>دلم ز ماریه عزم سفر نکرده هنوز</p></div>
<div class="m2"><p>هوای شهر و دیار دگر نکرد هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال می کشدم گه به شام و، گه به حجاز</p></div>
<div class="m2"><p>ولی ز ماریه قطع نظر نکرده هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اهل بیت پیمبر مگر که مر شریر</p></div>
<div class="m2"><p>به دشت کرب وبلا ترک شر نکرده هنوز؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرشک دیدهٔ زین العباد خشک نشد</p></div>
<div class="m2"><p>مگر حسین لبی زآب، تر نکرده هنوز؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شراره آه دل، از تشنگی شه، با شمر</p></div>
<div class="m2"><p>مگر که بر دل سختش اثر نکرده هنوز؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برهنه سرکند از مشرق آفتاب طلوع</p></div>
<div class="m2"><p>مگر که زینب معجر به سر نکرده هنوز؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوای نالهٔ لیلا ز نینوا آید</p></div>
<div class="m2"><p>مگر کناره ز نعش پسر نکرده هنوز؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنور خانهٔ خولی ست بزم مهمانی</p></div>
<div class="m2"><p>مگر حسین از آن جا سفر نکرده هنوز؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک به عترت اطهار در خرابهٔ شام</p></div>
<div class="m2"><p>مگر شب غم شان را سحر نکرده هنوز؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر بریدهٔ سبط نبی به بزم یزید</p></div>
<div class="m2"><p>مگر مفارقت از طشت زر نکرده هنوز؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبا ز کرب وبلا در نجف ز قتل حسین</p></div>
<div class="m2"><p>مگر نرفته علی را خبر نکرده هنوز؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرشک دیدهٔ «ترکی» ز سرگذشت ولی</p></div>
<div class="m2"><p>ز دشت ماریه خاکی به سر نکرده هنوز؟</p></div></div>