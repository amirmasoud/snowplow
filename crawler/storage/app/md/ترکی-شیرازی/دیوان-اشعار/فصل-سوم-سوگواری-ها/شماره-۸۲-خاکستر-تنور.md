---
title: >-
    شمارهٔ  ۸۲ - خاکستر تنور
---
# شمارهٔ  ۸۲ - خاکستر تنور

<div class="b" id="bn1"><div class="m1"><p>فلک خراب شوی انقدر غرور چرا</p></div>
<div class="m2"><p>به اهل بیت نبی گشته ای جسور چرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزیز ساقی کوثر، به دشت کرب و بلا</p></div>
<div class="m2"><p>سرش ز تن لب خشکیده گشت دور چرا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهی که خوابگهش دامن پیمبر بود</p></div>
<div class="m2"><p>به خاک خفته، تن چاک چاک و عورچرا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهی که پرورش در کنار زهرا بود</p></div>
<div class="m2"><p>شکسته سینهٔ او از سم ستور چرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری که فاطمه شستی غبارازاو خولی</p></div>
<div class="m2"><p>رخش نهاد به خاکستر تنور چرا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنی چو زینب غمدیده تاب صبر نداشت</p></div>
<div class="m2"><p>ولی به موج بلا گشت او صبور چرا؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریم آل علی را مکان، خرابهٔ شام</p></div>
<div class="m2"><p>زنان نسل زنا را مکان قصور چرا؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهی که در ره امت، روان شیرین داد</p></div>
<div class="m2"><p>از او مضایقه «ترکی» زاشک شور چرا؟</p></div></div>