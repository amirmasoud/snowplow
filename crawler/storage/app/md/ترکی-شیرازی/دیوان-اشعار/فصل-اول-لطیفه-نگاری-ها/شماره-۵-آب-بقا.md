---
title: >-
    شمارهٔ  ۵ - آب بقا
---
# شمارهٔ  ۵ - آب بقا

<div class="b" id="bn1"><div class="m1"><p>ای همایون پی و فرخ رخ و فرخنده لقا!</p></div>
<div class="m2"><p>ای خطت چون ظلمات و دهنت آب بقا!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو اگر دم زند از راست قدی پیش قدت</p></div>
<div class="m2"><p>چون قدت را نگرد می شود از شرم دو تا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این نه خود رای صواب است خطاییست بزرگ</p></div>
<div class="m2"><p>که دهم نسبت زلفین تو با مشک خطا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک چشمت به کفش خنجر مژگان از چیست</p></div>
<div class="m2"><p>با من بی سر و پا بر سر جنگ است چرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دعا خواسته ام تا که مرا یار شوی</p></div>
<div class="m2"><p>از پی وصل تو تجدید کنم باز دعا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه مرا طاقت این تا که ببوسم دهنت</p></div>
<div class="m2"><p>نه تو را عادت آن تا که کنی بوسه عطا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستی کردن «ترکی» به تو کاری عجب است</p></div>
<div class="m2"><p>حاصلی نیست در این کار بجز رنج و عنا</p></div></div>