---
title: >-
    شمارهٔ  ۱۲۸ - قرص آفتاب
---
# شمارهٔ  ۱۲۸ - قرص آفتاب

<div class="b" id="bn1"><div class="m1"><p>جانا! چرا نپوشی بر روی خود نقابی؟</p></div>
<div class="m2"><p>تا کی گشاده رویی تا چند بی حجابی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رو، گشاده مگذار پا از سرای بیرون</p></div>
<div class="m2"><p>یا بر رخت بیفکن از زلف خود نقابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشتاقم آن چنان من بر لعل می پرستت</p></div>
<div class="m2"><p>چون شب نشین به خوابی یا تشنهٔ برآبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب ها به وعدهٔ وصل پیوسته مانده تا صبح</p></div>
<div class="m2"><p>گوشم به راه پیغام، چشمم به فتح بابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قامت دل آرا، وز عارض سمن سا</p></div>
<div class="m2"><p>چون سرو بوستانی، چون قرص آفتابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چشم سرمه سایت، وان خال دل ربایت</p></div>
<div class="m2"><p>جادوی فتنه بازی ست، هندوی بی کتابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن لعل آبدارت، وان زلف تابدارت</p></div>
<div class="m2"><p>آن درج پر ز گوهر، وین خم به خم طنابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لطفت به حالت دوست، قهرت به جای دشمن</p></div>
<div class="m2"><p>آن آیتی ز رحمت، وین آیت عذابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک وجود «ترکی» ویرانه شد ز عشق ات</p></div>
<div class="m2"><p>شه کی خراج گیرد از کشور خرابی؟</p></div></div>