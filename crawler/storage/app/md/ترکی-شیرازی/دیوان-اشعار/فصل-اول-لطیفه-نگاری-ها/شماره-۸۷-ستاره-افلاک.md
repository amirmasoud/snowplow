---
title: >-
    شمارهٔ  ۸۷ - ستاره افلاک
---
# شمارهٔ  ۸۷ - ستاره افلاک

<div class="b" id="bn1"><div class="m1"><p>چند باشی پریرخا! بی باک</p></div>
<div class="m2"><p>من که گشتم زغصهٔ تو هلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غمت ای نگار سیمین بر!</p></div>
<div class="m2"><p>می کنم همچو گل، گریبان چاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب از فراق ماه رخت</p></div>
<div class="m2"><p>می شمارم ستارهٔ افلاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز تا شب بر آستانهٔ تو</p></div>
<div class="m2"><p>می نهم روی مسکنت بر خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی رخت گر به سر، برم روزی</p></div>
<div class="m2"><p>ناله ام از سمک رسد به سماک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مژه ام چون کند به سیل سرشک</p></div>
<div class="m2"><p>در ره سیل، بسته ام خاشاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو شوی با من آشنا هیهات</p></div>
<div class="m2"><p>من شوم با تو بی وفا حاشاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو زخمم زنی به از مرهم</p></div>
<div class="m2"><p>ور تو زهرم دهی به از تریاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«ترکی» از خاک آستانهٔ تو</p></div>
<div class="m2"><p>نرود تا تنش نگردد خاک</p></div></div>