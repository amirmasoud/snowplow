---
title: >-
    شمارهٔ  ۱۱۶ - گنج شایان
---
# شمارهٔ  ۱۱۶ - گنج شایان

<div class="b" id="bn1"><div class="m1"><p>ای جمالت! زماه تابان به</p></div>
<div class="m2"><p>لب لعلت، ز آب حیوان به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی زلفت ز بوی گل خوشتر</p></div>
<div class="m2"><p>خط سبزت ز برگ ریحان به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست نخلی چو سرو در بستان</p></div>
<div class="m2"><p>نخل قدت ز سرو بستان به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دندان صاف رخشانت</p></div>
<div class="m2"><p>از لطافت ز در عمان به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه جان بهتر است از همه چیز</p></div>
<div class="m2"><p>لیک هستی مرا تو از جان به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهر هجر توام به از تریاق</p></div>
<div class="m2"><p>درد عشق توام ز درمان به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت فقر و گنج درویشی</p></div>
<div class="m2"><p>پیش «ترکی» ز گنج شایان به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«ترکیا» گر هزار دستانی</p></div>
<div class="m2"><p>ور شوی از هزاردستان به</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعر گفتن بود کمال، ولی</p></div>
<div class="m2"><p>نیست چیزی ز حفظ قرآن به</p></div></div>