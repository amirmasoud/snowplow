---
title: >-
    شمارهٔ  ۴۸ - نخل نیکی
---
# شمارهٔ  ۴۸ - نخل نیکی

<div class="b" id="bn1"><div class="m1"><p>تا کی؟ ای دل! تو گرفتار جهان خواهی شد</p></div>
<div class="m2"><p>ناوک ناز جهان را تو نشان خواهی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقتت از کف خاکی شده و، آخر کار</p></div>
<div class="m2"><p>زین جهان چون گذری، باز همان خواهی شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زینهار ای تن خاکی! نشوی غره به خویش</p></div>
<div class="m2"><p>کآخر الامر گل کوزه گران خواهی شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می برد گرگ اجل، یک یک از این گله و تو</p></div>
<div class="m2"><p>چند از دور، برایشان نگران خواهی شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیری و، می کنی از وسمه سیه موی سپید</p></div>
<div class="m2"><p>تو بر آنی که به دین شیوه جوان خواهی شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نهی تاج و، دو صد سال نشینی بر تخت</p></div>
<div class="m2"><p>آخر از تخت، به تابوت روان خواهی شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخل نیکی بنشان، بذر بدی را مفشان</p></div>
<div class="m2"><p>ورنه در روز جزا اشک فشان خواهی شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست بیچارهٔ از پای درافتاده بگیر</p></div>
<div class="m2"><p>ز آنکه یک روز، تو هم نیز چنان خواهی شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پند «ترکی» بشنو دل بکن از مهر بتان</p></div>
<div class="m2"><p>ورنه با انده و حسرت، ز جهان خواهی شد</p></div></div>