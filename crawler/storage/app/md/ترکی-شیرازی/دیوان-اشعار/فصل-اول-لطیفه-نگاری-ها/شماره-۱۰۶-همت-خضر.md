---
title: >-
    شمارهٔ  ۱۰۶ - همت خضر
---
# شمارهٔ  ۱۰۶ - همت خضر

<div class="b" id="bn1"><div class="m1"><p>نذر کردم گر از این کشور ویران بروم</p></div>
<div class="m2"><p>تا در پیر مغان شاد و غزلخوان بروم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به تنگ آمدم از همدمی مردم هند</p></div>
<div class="m2"><p>خوش به هم صحبتی مردم ایران بروم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هند زندان و من اینجا شده ام زندانی</p></div>
<div class="m2"><p>ای خوش آن روز، کزین گوشهٔ زندان بروم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل از این دیو صفت مردم هندم بگرفت</p></div>
<div class="m2"><p>هدهد آسا به سوی ملک سلیمان بروم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار الها سببی ساز کزین کشور کفر</p></div>
<div class="m2"><p>همچنان کآمده ام باز مسلمان بروم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت خضر اگر بدرقهٔ راه شود</p></div>
<div class="m2"><p>چون سکندر به سر چشمهٔ حیوان بروم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دلی پر غم و دامان پر از گوهر اشک</p></div>
<div class="m2"><p>به زیارت گه سلطان شهیدان بروم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سر افتاده مرا شوق بیابان نجف</p></div>
<div class="m2"><p>که به پابوس علی سرور مردان بروم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«ترکی» ار آنکه شود مدفن من خاک نجف</p></div>
<div class="m2"><p>از نجف یکسره تا روضهٔ رضوان بروم</p></div></div>