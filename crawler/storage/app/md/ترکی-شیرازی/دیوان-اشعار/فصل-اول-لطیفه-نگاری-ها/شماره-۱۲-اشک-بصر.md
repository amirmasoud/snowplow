---
title: >-
    شمارهٔ  ۱۲ - اشک بصر
---
# شمارهٔ  ۱۲ - اشک بصر

<div class="b" id="bn1"><div class="m1"><p>میل دارم که ببوسم رخ همچون قمرت را</p></div>
<div class="m2"><p>بمکم با لب خود آن لب همچون شکرت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا غباری ننشیند به رخ و زلف تو جانا!</p></div>
<div class="m2"><p>آب پاشی کنم از اشک بصر، رهگذرت را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ممکنم نسیت که آیم به سر کوی تو روزی</p></div>
<div class="m2"><p>تا که بر دیده کشم سرمه صفت، خاک درت را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نظر باز نگیرم ز رخت ای شه خوبان!</p></div>
<div class="m2"><p>به امیدی که ز من باز نگیری نظرت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو چنانی که ز کس باز نپرسی خبرم را</p></div>
<div class="m2"><p>من به هر کس که رسم باز بپرسم خبرت را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ تو کعبه و خالت حجرالاسود و «ترکی»</p></div>
<div class="m2"><p>دارد امید که یک روز ببوسد حجرت را</p></div></div>