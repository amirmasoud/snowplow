---
title: >-
    شمارهٔ  ۸۱ - آبشار باغ
---
# شمارهٔ  ۸۱ - آبشار باغ

<div class="b" id="bn1"><div class="m1"><p>بر خیز تا رویم به سیر بهار باغ</p></div>
<div class="m2"><p>زان جا کشیم رخت، به زیر حصار باغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باغبان ز در، نگذارد درون شویم</p></div>
<div class="m2"><p>خود را بیفکنیم به باغ، از جدار باغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنهیم گاه چشم، به سیر درخت گل</p></div>
<div class="m2"><p>بدهیم گاه گوش، به صوت هزار باغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوابیم گاه زیر درختان نارون</p></div>
<div class="m2"><p>یابیم گاه بهره ز سیب و انار باغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیریم گاه از کف ساقی سیم ساق</p></div>
<div class="m2"><p>جام پر می به لب جویبار باغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفتیم گه میان گل و لاله و شقیق</p></div>
<div class="m2"><p>خفتیم گه میان همیشه بهار باغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نساج نوبهار، زگل های رنگارنگ</p></div>
<div class="m2"><p>گویی که بافته است بهم پود و تار باغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبنم زبس نشسته و، از بس وزیده باد</p></div>
<div class="m2"><p>اشجار پاک گشته، زگرد و غبار باغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنگر به دست ابر بهاری که از تگرگ</p></div>
<div class="m2"><p>گویی نموده دانهٔ گوهر، نثارباغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشجاررا به شاخه درختان، شکوفه ها</p></div>
<div class="m2"><p>رخشنده چون ستاره به شب های تار باغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ریزان چو سیم خام بود آب زآبشار</p></div>
<div class="m2"><p>گویی که نقره می چکد از آبشار باغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای گل تو خوش خرام به باغ و کنار آب</p></div>
<div class="m2"><p>تا«ترکی» ات شود ز وفا آبیار باغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فراش نوبهار، بساط زمردین</p></div>
<div class="m2"><p>گسترده از برای تو در رهگذار باغ</p></div></div>