---
title: >-
    شمارهٔ  ۱۲۱ - در و مرجان
---
# شمارهٔ  ۱۲۱ - در و مرجان

<div class="b" id="bn1"><div class="m1"><p>ای که در حسن، ماه تابانی</p></div>
<div class="m2"><p>از لطافت چو حور و غلمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست گویم به راستی قدت</p></div>
<div class="m2"><p>سرو نبود به هیچ بستانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه طبیبی که مورث دردی</p></div>
<div class="m2"><p>نه حبیبی که دشمن جانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تو مشتاقم آن چنان که به آب</p></div>
<div class="m2"><p>تشنهٔ خسته در بیابانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامنم پر زاشک و خون دل است</p></div>
<div class="m2"><p>گر خریدار در و مرجانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شنیدم که نیست در تو وفا</p></div>
<div class="m2"><p>چون بدیدم هزار چندانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشم آلوده با من استی و بس</p></div>
<div class="m2"><p>با رقیبان مدام خندانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به طریقی که کشته ای تو مرا</p></div>
<div class="m2"><p>نکشد کافری، مسلمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک چشم تو خون «ترکی» ریخت</p></div>
<div class="m2"><p>تو مگر حکمران ترکانی؟</p></div></div>