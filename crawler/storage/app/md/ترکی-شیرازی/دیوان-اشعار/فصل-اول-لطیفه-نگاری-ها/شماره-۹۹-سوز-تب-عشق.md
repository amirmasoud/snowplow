---
title: >-
    شمارهٔ  ۹۹ - سوز تب عشق
---
# شمارهٔ  ۹۹ - سوز تب عشق

<div class="b" id="bn1"><div class="m1"><p>ساقی بده امروز یکی جام شرابم</p></div>
<div class="m2"><p>از جام شرابی بکن امروز خرابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن می ناب که تو را هست به کوزه</p></div>
<div class="m2"><p>من تشنهٔ یک جرعه ای از آن می نابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از یاد نخواهد شدنم آخر پیری</p></div>
<div class="m2"><p>آن باده که دادی تو در ایام شبابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازت بکشم بهر یکی جام شرابی</p></div>
<div class="m2"><p>صد بار اگر ناز نمایی و عتابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غصه شب و روز به چشمم نرود خواب</p></div>
<div class="m2"><p>لطفی کن و جامی ده از آن داروی خوابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند کسان لازم می، چنگ و رباب است</p></div>
<div class="m2"><p>گر می، تو دهی گو نبود چنگ و ربابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سوز تب عشق و، ز سیلاب سرشکم</p></div>
<div class="m2"><p>هم سوخته از آتش و، هم غرقه در آبم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چند عقابم کنی از جرعه ای از می</p></div>
<div class="m2"><p>می در قدحم ریز و، مترسان ز عقابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شک نیست که غفار ذنوب است خداوند</p></div>
<div class="m2"><p>«ترکی» تو مترسان عبث از روز حسابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من دانم و آن کس که مرا خلق نموده است</p></div>
<div class="m2"><p>یا عفو کند یا که کند زجر و عذابم</p></div></div>