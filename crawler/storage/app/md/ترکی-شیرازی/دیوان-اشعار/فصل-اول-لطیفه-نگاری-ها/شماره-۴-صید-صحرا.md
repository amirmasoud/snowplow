---
title: >-
    شمارهٔ  ۴ - صید صحرا
---
# شمارهٔ  ۴ - صید صحرا

<div class="b" id="bn1"><div class="m1"><p>تو را که گفت که بگشای روی زیبا را</p></div>
<div class="m2"><p>که تاب روی تو بر بود طاقت ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم نسیم صبا مشک بیز خواهد شد</p></div>
<div class="m2"><p>اگر پریش کنی زلف عنبر آسارا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو شانه به زلف سیاه خویش زنی</p></div>
<div class="m2"><p>سیاه روز کنی عاشقان شیدا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تو مایل دریا و موج دریایی</p></div>
<div class="m2"><p>بیا به دیده من بین تو موج دریا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مزن به تیر، که من خود اسیر دام توام</p></div>
<div class="m2"><p>به تیر، کس نزند مرغ رشته در پا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکرد در دل سخت تو آه من اثری</p></div>
<div class="m2"><p>که می گداخت به یک شعله، سنگ خارا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر به چشم عنایت مکن به سوی رقیب</p></div>
<div class="m2"><p>که روشنی ندهد سرمه چشم اعما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دراز دستی زلفت نگر که «ترکی» را</p></div>
<div class="m2"><p>چنان گرفت که صیاد، صید صحرا را</p></div></div>