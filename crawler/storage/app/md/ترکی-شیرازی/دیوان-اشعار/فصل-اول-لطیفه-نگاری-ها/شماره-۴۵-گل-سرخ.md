---
title: >-
    شمارهٔ  ۴۵ - گل سرخ
---
# شمارهٔ  ۴۵ - گل سرخ

<div class="b" id="bn1"><div class="m1"><p>ای عارض زیبای تو همرنگ گل سرخ!</p></div>
<div class="m2"><p>وی لعل شکرخای تو همسنگ گل سرخ!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حیرتم از بس که دهانت بود تنگ</p></div>
<div class="m2"><p>گویا که بود غنچهٔ دل تنگ گل سرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قطرهٔ باران که چکد ز ابر بهاری</p></div>
<div class="m2"><p>بزداید از اغبار هوا زنگ گل سرخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ممکن نبود تا که به دستش نخلد خار</p></div>
<div class="m2"><p>هرکس پی چیدن، کند آهنگ گل سرخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندی ست که در چنگ تو «ترکی» ست گرفتار</p></div>
<div class="m2"><p>چون بلبل بیچاره که در چنگ گل سرخ</p></div></div>