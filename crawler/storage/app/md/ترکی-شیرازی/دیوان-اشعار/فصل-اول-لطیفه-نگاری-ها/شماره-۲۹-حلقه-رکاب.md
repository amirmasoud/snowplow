---
title: >-
    شمارهٔ  ۲۹ - حلقهٔ رکاب
---
# شمارهٔ  ۲۹ - حلقهٔ رکاب

<div class="b" id="bn1"><div class="m1"><p>رویت ای صنم! ماه نخشب است</p></div>
<div class="m2"><p>جنتت رخ و، کوثرت لب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف بر رخت دیدم ای صنم!</p></div>
<div class="m2"><p>گفتم این قمر، وان چه عقرب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ببر مرا، می‌تپد مدام</p></div>
<div class="m2"><p>دارویش تو را سیب غبغب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جدایی ات روز و شب مرا</p></div>
<div class="m2"><p>ورد یا خدا، ذکر یا رب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ من تو را، عین مقصد است</p></div>
<div class="m2"><p>وصل تو مرا، اصل مطلب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فراق تو روز تا به شام</p></div>
<div class="m2"><p>شام من دراز، روز من شب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساغرم ز می، دیده ام ز خون</p></div>
<div class="m2"><p>آن بود تهی، وین لبالب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده ام تو راست حلقهٔ رکاب</p></div>
<div class="m2"><p>ابرویم تو را نعل مرکب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوکبم زبون، طالعم نگون</p></div>
<div class="m2"><p>این چه طالعی ست، وین چه کوکب است؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«ترکی» حزین، از جدائیت</p></div>
<div class="m2"><p>مونس اش غم و، همدمش تب است</p></div></div>