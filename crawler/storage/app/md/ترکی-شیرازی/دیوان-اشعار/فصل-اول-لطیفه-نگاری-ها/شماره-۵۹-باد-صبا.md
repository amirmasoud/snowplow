---
title: >-
    شمارهٔ  ۵۹ - باد صبا
---
# شمارهٔ  ۵۹ - باد صبا

<div class="b" id="bn1"><div class="m1"><p>خوش والدی که چون تویی او را ولد بود</p></div>
<div class="m2"><p>بر والدی چنین، همه کس را حسد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر والدی که می نگری هست با ولد</p></div>
<div class="m2"><p>بر هر ولد، نه خوبی این خط و خد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو ولد به خانهٔ هر والدی که هست</p></div>
<div class="m2"><p>او را ز میوه های بهشتی رسد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخ ماه آسمان و، قدت سرو بوستان</p></div>
<div class="m2"><p>بوی سیاه موی تو خوشتر زند بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ماه آسمان، نه چنین لطف و دلبری ست</p></div>
<div class="m2"><p>بر سرو بوستان، نه چنین حسن قد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن تو را به حسن نکویان، چه نسبت است</p></div>
<div class="m2"><p>بسیار فرق ها ز صنم تا صمد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامان و حبیب باد صبا پر ز عنبر است</p></div>
<div class="m2"><p>او را مگر به زلف تو داد و ستد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس که نیست در سر او شور عشق تو</p></div>
<div class="m2"><p>انسان ندانمش که کم از دیو و دد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنیاد سد، ز جای کند سیل اشک من</p></div>
<div class="m2"><p>گر در رهش ز آهن و پولاد سد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی برای تجربه از من به خواه، جان</p></div>
<div class="m2"><p>تا بنگری محبت من، تا چه حد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوری نمودن تو ز«ترکی» پری رخا!</p></div>
<div class="m2"><p>مانا حدیث دوری جان، از جسد بود</p></div></div>