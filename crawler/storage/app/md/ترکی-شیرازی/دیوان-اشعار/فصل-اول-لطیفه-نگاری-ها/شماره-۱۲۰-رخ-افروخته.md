---
title: >-
    شمارهٔ  ۱۲۰ - رخ افروخته
---
# شمارهٔ  ۱۲۰ - رخ افروخته

<div class="b" id="bn1"><div class="m1"><p>دوش آمد دلبرم با چهرهٔ افروخته</p></div>
<div class="m2"><p>وز رخ افروخته جان جهانی سوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کمان غمزه اش تیری مرا بر دل رسید</p></div>
<div class="m2"><p>شصت او نازم عجب تیرافکنی آموخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نگاهی جامهٔ صبر مرا او پاره کرد</p></div>
<div class="m2"><p>من چو حربا دیده بر خورشید رویش دوخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش جانا ز تاب عشق عالم سوز تو</p></div>
<div class="m2"><p>صبر و تاب و جسم و جان وخانمانم سوخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این وفاداری ز «ترکی» بس که با این مفلسی</p></div>
<div class="m2"><p>یک سر موی تو را با عالمی نفروخته</p></div></div>