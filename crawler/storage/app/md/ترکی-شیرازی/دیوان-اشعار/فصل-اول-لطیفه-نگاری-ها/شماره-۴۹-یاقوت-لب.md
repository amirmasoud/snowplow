---
title: >-
    شمارهٔ  ۴۹ - یاقوت لب
---
# شمارهٔ  ۴۹ - یاقوت لب

<div class="b" id="bn1"><div class="m1"><p>تا مَهِ روی تو از پرده عیان می‌گردد</p></div>
<div class="m2"><p>ماه از شرم تو در ابر، نهان می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مشام از نفس باد رسد بوی عبیر</p></div>
<div class="m2"><p>به سر زلف تو چون باد، وزان می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست یاقوت لبت قوت روان و، دل من</p></div>
<div class="m2"><p>روز و شب در پیِ آن قوت روان می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنگیِ زلف تو بگشوده هنر سوی کمند</p></div>
<div class="m2"><p>دل من کرده اسیر و، پی جان می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک چشم تو اگر راهزنی کارش نیست</p></div>
<div class="m2"><p>پس سبب چیست؟ که با تیر و کمان می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خال در گوشهٔ ابروت، شده گوشه‌نشین</p></div>
<div class="m2"><p>حالیا در پی جایی به از آن می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سختم آید عجب از لعل لب چون شکرت</p></div>
<div class="m2"><p>که چنین شهد نصیب مگسان می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من به فکر تو و، تو مانده به فکر دگران</p></div>
<div class="m2"><p>چرخ پیوسته به کام دگران می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خیال رخ چون شمع تو هرشب تا صبح</p></div>
<div class="m2"><p>اشک گرمم به رخ زرد، روان می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیر شد «ترکی» از آن روز که رفتی ز برش</p></div>
<div class="m2"><p>گر بیایی به برش، باز جوان می‌گردد</p></div></div>