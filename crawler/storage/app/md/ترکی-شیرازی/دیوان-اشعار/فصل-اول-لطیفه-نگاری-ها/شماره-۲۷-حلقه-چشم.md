---
title: >-
    شمارهٔ  ۲۷ - حلقهٔ چشم
---
# شمارهٔ  ۲۷ - حلقهٔ چشم

<div class="b" id="bn1"><div class="m1"><p>قسم جانا! به جان دوستانت</p></div>
<div class="m2"><p>که دارم دوست تر از جسم و جانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب و روز از خیال بوی زلفت</p></div>
<div class="m2"><p>شدم لاغرتر از موی میانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چکد خون دل از چشمم چو یاقوت</p></div>
<div class="m2"><p>ز هجر لعل چون قوت روانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا هر دم رسد از سینه تیری</p></div>
<div class="m2"><p>از آن ابرو ی خم تر از کمانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو شیرین خنده گر خندی به گلزار</p></div>
<div class="m2"><p>نخندد غنچه از شرم دهانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سمند خویش را آهسته تر ران</p></div>
<div class="m2"><p>مشو یکباره دور از همرهانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر در باغ ننشاند صنوبر</p></div>
<div class="m2"><p>اگر قامت به بیند باغبانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رکابت را کنم از حلقهٔ چشم</p></div>
<div class="m2"><p>به دستم گر رسد روزی عنانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رقیبان من از حسرت بمیرند</p></div>
<div class="m2"><p>اگر نام من آید بر زبانت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی من خود از آن شوقی که دارم</p></div>
<div class="m2"><p>همی خواهم که تا بوسم دهانت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکر می ریزد از کلک تو «ترکی»</p></div>
<div class="m2"><p>نی قند است گویی در بنانت</p></div></div>