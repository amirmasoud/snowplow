---
title: >-
    شمارهٔ  ۱۱۰ - نافهٔ آهو
---
# شمارهٔ  ۱۱۰ - نافهٔ آهو

<div class="b" id="bn1"><div class="m1"><p>این که داری به سردوش، کمند است نه گیسو</p></div>
<div class="m2"><p>وین که داری به بناگوش کمان است نه ابرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامت است این که تو داری نه که سروی ست خرامان</p></div>
<div class="m2"><p>کاکل است اینکه تو داری نه بود فافهٔ آهو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به دین حسن و لطافت بخرامی سوی بستان</p></div>
<div class="m2"><p>پیش بوی سر زلفت گل و سنبل ندهد بو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چمن، سرو سهی دم نزند از قد و قامت</p></div>
<div class="m2"><p>به چمن گر به خرامی، تو به این قامت دلجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جز از قامت رعنای تو و آن لیموی پستان</p></div>
<div class="m2"><p>سرو هرگز نشنیدم که دهد میوهٔ لیمو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغبان بیند اگر جلوه کنان بر لب جویت</p></div>
<div class="m2"><p>بعد از این سرو و صنوبر، ننشاند به لب جو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افکنی پیکر شیران، تو ز سرپنجهٔ سیمین</p></div>
<div class="m2"><p>شکنی پشت دلیران، تو از آن غمزهٔ جادو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن چه سر پنجهٔ سیمین تو کرده است به جانم</p></div>
<div class="m2"><p>به یقینم نکند پنجهٔ شهباز، به تیهو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک نظر زاهد اگر قبلهٔ ابروی تو بیند</p></div>
<div class="m2"><p>بی گمان او نکند جانب محراب، دگر رو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر مصور کشد از خامه شبیه تو صنم را</p></div>
<div class="m2"><p>بت پرستان به تماشای تو آیند ز هر سو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر «ترکی» نبود لایق میدان تو ورنه</p></div>
<div class="m2"><p>خواهم انداختنش در خم چوگان تو چون گو</p></div></div>