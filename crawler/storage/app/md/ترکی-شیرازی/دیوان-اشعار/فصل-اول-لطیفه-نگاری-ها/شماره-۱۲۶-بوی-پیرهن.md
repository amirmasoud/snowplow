---
title: >-
    شمارهٔ  ۱۲۶ - بوی پیرهن
---
# شمارهٔ  ۱۲۶ - بوی پیرهن

<div class="b" id="bn1"><div class="m1"><p>دلم فتاده در آن زلف پر شکن که تو داری</p></div>
<div class="m2"><p>قرار بده ز من، آن لب دهن که تو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدت چو سرو، خطت چون بنفشه، زلف چو سنبل</p></div>
<div class="m2"><p>کسی ندیده از این خوبتر چمن که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب ز فتنه این چشم فتنه بار تو دارم</p></div>
<div class="m2"><p>که فتنه بارد از این چشم پرفتن که تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش است چاه زنخدان و زلف چون رسن تو</p></div>
<div class="m2"><p>تبارک الله از این چاه و این رسن که تو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صورتت نتوانم کنم نظاره که ترسم</p></div>
<div class="m2"><p>از آن دو ترک کمان دار تیرزن که تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر راهزنی نیست کار هندوی زلفت</p></div>
<div class="m2"><p>نعوذباالله از این خال راهزن که تو داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن و بدن شود از رده ات ز پیرهن ای گل!</p></div>
<div class="m2"><p>زهی ز نازکی این تن و بدن که تو داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بوی پیرهنت زنده می شود دل مرده</p></div>
<div class="m2"><p>چه حکمت است دراین بوی پیرهن که تو داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجاست شهر و دیار تو و کجا وطن تو</p></div>
<div class="m2"><p>خوشا به مردم آن شهر و آن وطن که تو داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا غلام خودت خوان که هیچ خواجه ندارد</p></div>
<div class="m2"><p>چنین غلام هنرپیشه ای چو من که تو داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به معنی سخنت «ترکیا» نبرده کسی پی</p></div>
<div class="m2"><p>سخن شناس کند فهم این سخن که تو داری</p></div></div>