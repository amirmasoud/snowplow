---
title: >-
    شمارهٔ  ۲۶ - درای کاروان
---
# شمارهٔ  ۲۶ - درای کاروان

<div class="b" id="bn1"><div class="m1"><p>لب لعلت، مرا آرام جان است</p></div>
<div class="m2"><p>دو چشمت، فتنه آخر زمان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب از خال هندوی تو دارم</p></div>
<div class="m2"><p>که او را بر لب کوثر، مکان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدت شمشاد، یا سرو روان است</p></div>
<div class="m2"><p>لبت یاقوت، یاقوت روان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم از غصه چون موی میانت</p></div>
<div class="m2"><p>که موی است اینکه داری یا میان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهان است اینکه داری، یا که غنچه است</p></div>
<div class="m2"><p>ندانم غنچه است این یا دهان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد ترک چشمت گر سر جنگ</p></div>
<div class="m2"><p>چرا؟ پیوسته با تیر و کمان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی با تو به سر بردن به عمری</p></div>
<div class="m2"><p>مرا خوشتر ز عمر جاودان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی خواهم که آیم در وثاقت</p></div>
<div class="m2"><p>ولی خوفم همه از پاسبان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به امیدی که آیی از سفر باز</p></div>
<div class="m2"><p>دو گوشم بر درای کاروان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مران از خویش «ترکی» را که دایم</p></div>
<div class="m2"><p>تو را همچون سگی بر آستان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شهبازش نباشد چشم بر گوشت</p></div>
<div class="m2"><p>ولی قانع به مشتی استخوان است.</p></div></div>