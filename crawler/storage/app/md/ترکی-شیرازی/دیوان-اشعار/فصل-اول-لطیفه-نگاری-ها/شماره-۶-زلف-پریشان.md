---
title: >-
    شمارهٔ  ۶ - زلف پریشان
---
# شمارهٔ  ۶ - زلف پریشان

<div class="b" id="bn1"><div class="m1"><p>جانا سر و جان فدیه بر جان تو بادا</p></div>
<div class="m2"><p>جان برخی آن لعل چو مرجان تو بادا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که به جای تو گزیند دگری را</p></div>
<div class="m2"><p>هر دم به سرش تیغ سرافشان تو بادا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دیده که غیر از تو ببیند رخ غیری</p></div>
<div class="m2"><p>آن دیده نشان سر پیکان تو بادا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دل که در او نیست ز مهر تو نشانی</p></div>
<div class="m2"><p>سوراخ ز نوک نی سوزان تو بادا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سر که بود زآتش سودای تو خالی</p></div>
<div class="m2"><p>سرگشته چو گو، در خم چوگان تو بادا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهم ز خدا کاین دل دیوانه «ترکی»</p></div>
<div class="m2"><p>در سلسلهٔ زلف پریشان تو بادا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمری است که مداح و ثنا خوان شما هست</p></div>
<div class="m2"><p>خواهد که همه عمر ثناخوان تو بادا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو شیر خداوندی و داماد رسولی</p></div>
<div class="m2"><p>جان های محبان همه قربان تو بادا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان و تن یاران و محبان تو یکسر</p></div>
<div class="m2"><p>قربان تن و جان عزیزان تو بادا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاها نبود لایق قربان تو جانم</p></div>
<div class="m2"><p>جان فدیه بر قنبر و سلمان تو بادا</p></div></div>