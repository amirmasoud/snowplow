---
title: >-
    شمارهٔ  ۳۵ - دل می طپد
---
# شمارهٔ  ۳۵ - دل می طپد

<div class="b" id="bn1"><div class="m1"><p>ای چشمهٔ حیوان خجل از لعل لبانت</p></div>
<div class="m2"><p>وی آب بقا منفعل از آب دهانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروی کجت هست، کمان و مژه ات تیر</p></div>
<div class="m2"><p>دل می طپد اندر بر آن تیر و کمانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سرو ندیده است کس این قامت و رفتار</p></div>
<div class="m2"><p>خود منفعلم، خوانم اگر سرو روانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس سخنان تو بود دلکش و جان بخش</p></div>
<div class="m2"><p>ارباب سخن در عجب اند از سخنانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حسرت آن موی میانی که تو داری</p></div>
<div class="m2"><p>جسمم شده باریکتر از موی میانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیزم ز لحد رقص کنان، از پس مرگم</p></div>
<div class="m2"><p>یک روز اگر نام من آید به زبانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینم عجب آید که چه جنسی تو که عشاق</p></div>
<div class="m2"><p>جویند ز هر سو، چو هلال رمضانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر باده خوری پا منه از خانه تو بیرون</p></div>
<div class="m2"><p>ترسم که بگیرند به مستی عس سانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرینی شعر تو گرو، می برد از قند</p></div>
<div class="m2"><p>«ترکی» نی قند است مگر کلک و بنانت</p></div></div>