---
title: >-
    شمارهٔ  ۱۱۱ - مقام عاشقی
---
# شمارهٔ  ۱۱۱ - مقام عاشقی

<div class="b" id="bn1"><div class="m1"><p>فصل گل است ساقیا شیشهٔ پر شراب کو؟</p></div>
<div class="m2"><p>گل ز حجاب شد برون، شاهد بی حجاب کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطربکا تو هم چرا؟ میل طرب نمی کنی</p></div>
<div class="m2"><p>تار تو کو دفت چه شد، چنگ و نی ورباب کو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که به عشق و عاشقی شهرهٔ شهرها شدم</p></div>
<div class="m2"><p>واعظ پندگو چه شد زاهد بی کتاب کو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتیم از جدایی ام تاب بیار و صبر کن</p></div>
<div class="m2"><p>چون تو ز من جدا شوی صبر کجا و تاب کو؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوری تو ز عاشقان، هست عذاب جسم و جان</p></div>
<div class="m2"><p>عاشق بی گناه را بدتر از این عذاب کو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نافهٔ مشگ ناب را من به جوی نمی خرم</p></div>
<div class="m2"><p>خوب تر از دو زلف تو نافهٔ مشگ ناب کو؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر زلف خویشتن رشته به گردنم فکن</p></div>
<div class="m2"><p>گردن لاغر مرا خوشتر از این طناب کو؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از می عشق «ترکیا» مست و خرابم از ازل</p></div>
<div class="m2"><p>در همهٔ جهانیان همچو، منی خراب کو؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآتش عشق ات ای صنم! آه دلم کباب شد</p></div>
<div class="m2"><p>گر به کباب مایلی بهتر از این کباب کو؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهم از این سپس کنم جا به مقام عاشقی</p></div>
<div class="m2"><p>جای توان کنم ولی همت بوتراب کو؟</p></div></div>