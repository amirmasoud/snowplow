---
title: >-
    شمارهٔ  ۸۵ - سودای عشق
---
# شمارهٔ  ۸۵ - سودای عشق

<div class="b" id="bn1"><div class="m1"><p>آفرین بر نام روح افزای عشق</p></div>
<div class="m2"><p>جان فدای آنکه شد دارای عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک تجلی کرد عشق و در جهان</p></div>
<div class="m2"><p>هر کجا بینی بود غوغای عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق را جایی نباشد غیر دل</p></div>
<div class="m2"><p>در دل عشاق، باشد جای عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل را دیگر نباشد جای زیست</p></div>
<div class="m2"><p>در میان، هر کجا که آید پای عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا صف محشر کجا آید به هوش</p></div>
<div class="m2"><p>هر که جامی خورد از مینای عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لشکر غم زد شبیخون بر سرم</p></div>
<div class="m2"><p>تادلم زد خیمه در صحرای عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقا!گر طالب عشقی بزن</p></div>
<div class="m2"><p>غوطه ای در بحر گوهرزای عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«ترکی»از معشوق کی یابی مراد</p></div>
<div class="m2"><p>تا نباشد بر سرت سودای عشق</p></div></div>