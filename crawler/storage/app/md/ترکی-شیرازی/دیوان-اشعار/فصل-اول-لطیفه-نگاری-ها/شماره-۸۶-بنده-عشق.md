---
title: >-
    شمارهٔ  ۸۶ - بنده عشق
---
# شمارهٔ  ۸۶ - بنده عشق

<div class="b" id="bn1"><div class="m1"><p>خوشا عشق و خوشا دارندهٔ عشق</p></div>
<div class="m2"><p>خوشا آن کس که باشد زندهٔ عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زقید خود پرستی گشت آزاد</p></div>
<div class="m2"><p>هر آن کس گشت از جان، بندهٔ عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی ماند نشان ظلمت آنجا</p></div>
<div class="m2"><p>که تابد اختر تابندهٔ عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تجلی گر نکردی حسن معشوق</p></div>
<div class="m2"><p>کجا عاشق شدی جویندهٔ عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فضای قلب عاشق، گشت روشن</p></div>
<div class="m2"><p>عجب دندان نما شد خندهٔ عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ره عشق است بس راهی خطرناک</p></div>
<div class="m2"><p>به منزل کی رسد ترسندهٔ عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحمدالله که شد ز اغیار خالی</p></div>
<div class="m2"><p>دل «ترکی» که شد آکنده عشق</p></div></div>