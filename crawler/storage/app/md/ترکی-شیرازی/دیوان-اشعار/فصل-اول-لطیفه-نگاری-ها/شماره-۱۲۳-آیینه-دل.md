---
title: >-
    شمارهٔ  ۱۲۳ - آیینهٔ دل
---
# شمارهٔ  ۱۲۳ - آیینهٔ دل

<div class="b" id="bn1"><div class="m1"><p>تو به دین حسن که در پرده،دل از خلق ربایی</p></div>
<div class="m2"><p>پردهٔ خلق دری گر ز پس پرده درآیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پس پرده نهانی و جهانی به تو مایل</p></div>
<div class="m2"><p>آه بی پرده که رخ به خلایق بنمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه خوبانی و شاهان همه هستند گدایت</p></div>
<div class="m2"><p>بر سر کوی تو آیند شهان، بهر گدایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آدمی زاده ندیدم به چنین حسن و لطافت</p></div>
<div class="m2"><p>به که مانی تو که سر تا به قدم، نور خدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخنت زنگ دل از آیینهٔ دل بزداید</p></div>
<div class="m2"><p>به سخن لب بگشا تا غمم از دل بزدایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سر زلف گره گیر تو مشکل شده کارم</p></div>
<div class="m2"><p>گره ار باز کنی، مشکل ما را بگشایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو شرمنده شود گر تو به بستان به خرامی</p></div>
<div class="m2"><p>غنچه دل تنگ شود گر به تکلم، تو بیایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی مشک ختن از موی تو آید به مشامم</p></div>
<div class="m2"><p>نازنینا! تو مگر نافهٔ آهوی ختایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز خیالت به دلم نیست دگر هیچ خیالی</p></div>
<div class="m2"><p>جز هوایت به سرم نیست دگر هیچ هوایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به وفایت که سر از عهد و وفای تو نپیچم</p></div>
<div class="m2"><p>ز آنکه دانم که به من، بر سر عهدی و وفایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«ترکیا» حملهٔ روبه صفتان در تو نگیرد</p></div>
<div class="m2"><p>همه دانند که تو کلب در شیر خدایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیر حق، سرور مردان، علی عالی اعلا</p></div>
<div class="m2"><p>آنکه بر رهبریش کس نبرد راه به جایی</p></div></div>