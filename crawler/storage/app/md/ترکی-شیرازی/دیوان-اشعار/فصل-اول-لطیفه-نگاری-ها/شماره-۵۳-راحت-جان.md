---
title: >-
    شمارهٔ  ۵۳ - راحت جان
---
# شمارهٔ  ۵۳ - راحت جان

<div class="b" id="bn1"><div class="m1"><p>تا قد سرو تو در باغ دلم، موزون شد</p></div>
<div class="m2"><p>چشم خونبار من از اشک روان، جیحون شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به گرد مه روی تو خط سبز دمید</p></div>
<div class="m2"><p>روز من تار شد و، حسن رخت افزون شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی سبب دور شدی از برم ای راحت جان!</p></div>
<div class="m2"><p>وز غم هجر تو آهم به سوی گردون شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو علارقم من ای دوست! به همراه رقیب</p></div>
<div class="m2"><p>به چمن رفتی و از غصه مرا دل، خون شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوخ لیلی وش من، سلسلهٔ زلف گشود</p></div>
<div class="m2"><p>بستهٔ سلسله اش صد چو من مجنون شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد از گوشهٔ مسجد، به سوی میکده رفت</p></div>
<div class="m2"><p>آه و افسوس که در شرع، چه بی قانون شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجده بر قالب آدم چو عزا زیل نکرد</p></div>
<div class="m2"><p>زان سبب بود که مردود شد و، ملعون شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در میکده عشق، ننوشید قدح</p></div>
<div class="m2"><p>بی شک از دایرهٔ اهل صفا، بیرون شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر کوی تو «ترکی» به چه خواری جان داد</p></div>
<div class="m2"><p>خبر از کس نگرفتی، که فلانی چون شد</p></div></div>