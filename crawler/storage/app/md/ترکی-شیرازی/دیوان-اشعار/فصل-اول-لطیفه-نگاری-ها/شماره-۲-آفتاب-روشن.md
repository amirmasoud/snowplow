---
title: >-
    شمارهٔ  ۲ - آفتاب روشن
---
# شمارهٔ  ۲ - آفتاب روشن

<div class="b" id="bn1"><div class="m1"><p>ساقی به جام ریز ز مینا شراب را</p></div>
<div class="m2"><p>تعمیر کن به جام شراب این خراب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبها ز غصه خواب به چشمم نمی رود</p></div>
<div class="m2"><p>ساقی بیار شیشهٔ داروی خواب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می آفتاب روشن و خم مشرق وی است</p></div>
<div class="m2"><p>آور برون ز مشرق خم، آفتاب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می چون عروس و خشت سرخم حجاب وی</p></div>
<div class="m2"><p>از روی این عروس بیفکن حجاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از درس و بحث و مدرسه شد خاطرم ملول</p></div>
<div class="m2"><p>مطرب بیار بربط و چنگ و رباب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تار ساز جفت مغنی به صد نوا</p></div>
<div class="m2"><p>آوا ز ترک و شور و حجاز رهاب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهد بیا و مجلس ما را فروغ بخش</p></div>
<div class="m2"><p>یعنی فکن ز چهره به یکسو نقاب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد به کنج میکده در پای خم نشست</p></div>
<div class="m2"><p>در رهن می نهاد ردا و کتاب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما عاشقیم و رند و نظر باز و باده نوش</p></div>
<div class="m2"><p>از ما بگوی واعظ عالی جناب را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ما مقصریم ولی غافر الذنوب</p></div>
<div class="m2"><p>از ما گنه نبیند و ببیند ثواب را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چند مستحق عذابیم ما ولی</p></div>
<div class="m2"><p>ایزد به ما زلطف ببخشد عذاب را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>«ترکی» که هست مست می حب بوتراب</p></div>
<div class="m2"><p>دامن کجا ز دست دهد بوتراب را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز حساب پیش خداوند جرم پوش</p></div>
<div class="m2"><p>آرم شفیع شافع، یوم الحساب را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیر خدا، وصی رسول آنکه قنبرش</p></div>
<div class="m2"><p>گردن به زیر طوق کند شیر غاب را</p></div></div>