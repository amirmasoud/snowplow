---
title: >-
    شمارهٔ  ۹۴ - دل تنگ
---
# شمارهٔ  ۹۴ - دل تنگ

<div class="b" id="bn1"><div class="m1"><p>باز عشق آمد و زد خیمه به صحرای دلم</p></div>
<div class="m2"><p>لشکر عشق صف آراست به یغمای دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل پر حسرت من گشت گرفتار و اسیر</p></div>
<div class="m2"><p>کرد مو سلسلهٔ سلسله در پای دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من پای به زنجیر و، شه کشور عشق</p></div>
<div class="m2"><p>ایستاده به کناری به تماشای دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه از این دلک من که چه پر درد دلی ست</p></div>
<div class="m2"><p>چند گویم که دلم وای دلم وای دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشود داغ دلم به ز مداوای طبیب</p></div>
<div class="m2"><p>آه از این داغ که جا کرده به بالای دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاش با ناخنس از سینه برون می کردم</p></div>
<div class="m2"><p>هست در سینه ندانم به کجا جای دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل خون شده ام راز نهانی ست بسی</p></div>
<div class="m2"><p>فاش ترسم که شود راز نهان های دلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دلی راست یکی قطرهٔ خونی به میان</p></div>
<div class="m2"><p>جز دل من که بود خون همه اعضای دلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست گویم نبود سینهٔ من منزل دل</p></div>
<div class="m2"><p>هست در زلف بتی منزل و ماوای دلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه من سعی کنم کز غمش آسوده شوم</p></div>
<div class="m2"><p>او نهد تازه غمی بر سر غم های دلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهر خوبان نرود از دل تنگم بیرون</p></div>
<div class="m2"><p>زانکه جا کرده چو خون در همه رگهای دلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>«ترکی» از خسته دلی هیچ شکایت نکنم</p></div>
<div class="m2"><p>که لب لعل نگار است مسیحای دلم</p></div></div>