---
title: >-
    شمارهٔ  ۱۱۲ - رنگ شقایق
---
# شمارهٔ  ۱۱۲ - رنگ شقایق

<div class="b" id="bn1"><div class="m1"><p>ای شوخ دلا زار من ای دلبر مه رو!</p></div>
<div class="m2"><p>تا کی تو دل آزاری و، تا چندی بدخو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزم تو سیه کردی از آن طرهٔ شبرنگ</p></div>
<div class="m2"><p>دل از کف من بردی از آن غمزهٔ جادو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش لب لعلت چکند رنگ شقایق</p></div>
<div class="m2"><p>پیش سر زلفت چو دهد سنبل تر بو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خجلت ده یاقوتی از آن لعل شکر بار</p></div>
<div class="m2"><p>رونق شکن مشگی از آن طرهٔ گیسو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز قامت دلجوی تو و آن لیموی پستان</p></div>
<div class="m2"><p>بر سرو ندید است کسی میوهٔ لیمو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاری که غراب سر زلفت به دلم کرد</p></div>
<div class="m2"><p>هرگز نکند پنجهٔ شهباز، به تیهو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خشم و عتاب تو برم پیش که شکوه</p></div>
<div class="m2"><p>وز جور و جفای تو روم پیش که یرغو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون حلقه، به درمانده بتا دیدهٔ «ترکی»</p></div>
<div class="m2"><p>بوتا که گذاری تو برون پای ز مشکو</p></div></div>