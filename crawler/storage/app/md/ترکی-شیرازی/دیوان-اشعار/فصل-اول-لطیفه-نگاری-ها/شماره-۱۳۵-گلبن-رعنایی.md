---
title: >-
    شمارهٔ  ۱۳۵ - گلبن رعنایی
---
# شمارهٔ  ۱۳۵ - گلبن رعنایی

<div class="b" id="bn1"><div class="m1"><p>تا چند نگارا تو! بدخوی و ستمکاری</p></div>
<div class="m2"><p>این خوی و ستمکاری از دست بده باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شغل تو دل آزاری ست کار تو جفا کاریست</p></div>
<div class="m2"><p>تا چند دل آزاری، تا چند جفا کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بار غم عشق ات، پیوسته کشم بر دوش</p></div>
<div class="m2"><p>تو از غم من خوردن پیوسته سبکباری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو با منی ار دشمن، من دوستت انگارم</p></div>
<div class="m2"><p>من گر چه ترایم دوست، تو دشمنم انگاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز نتوان برکند با سهل و به آسانی</p></div>
<div class="m2"><p>آن دل که به تو بستم با سختی و دشواری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستند دو چشمانت ناخورده شراب اما</p></div>
<div class="m2"><p>از مستی چشمانت من مست، تو هشیاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چشم منش می جوی، ای گلبن رعنایی</p></div>
<div class="m2"><p>در پای سمند تو بنشیند اگر خاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خال تو به عیاری برد از کف «ترکی» دل</p></div>
<div class="m2"><p>عیار ندیدم من، با این همه عیاری</p></div></div>