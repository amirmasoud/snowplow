---
title: >-
    شمارهٔ  ۱۵ - شرار آتش عشق
---
# شمارهٔ  ۱۵ - شرار آتش عشق

<div class="b" id="bn1"><div class="m1"><p>بگویمت سخنی کاندر او نه جای اشک است</p></div>
<div class="m2"><p>که هر که طاعت حق کرد افضل از ملک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنکه بندگی حق نکرد و سهل شمرد</p></div>
<div class="m2"><p>به روز باز پسین، جایش اسفلین درک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آنکه نیست به جانش شرار آتش عشق</p></div>
<div class="m2"><p>به نزد اهل خرد، چون طعام بی نمک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلا چو بی غش و صافی درآید از بوته</p></div>
<div class="m2"><p>دگر چه باکی و اندیشه ایش از محک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روندهٔ ره کوی حبیب کی داند</p></div>
<div class="m2"><p>که در میان رهش پرنیان و یا چنک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الا! که جامهٔ ابر شمین ببر داری</p></div>
<div class="m2"><p>مکن تو فخر بر آن کس که جامه اش قدک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ذره پروری آموز کافتاب فلک</p></div>
<div class="m2"><p>ز ذره پروریش جا به چارمین فلک است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طریق بنده نوازی از آن کسی آموز</p></div>
<div class="m2"><p>که خادم در دولت سرای او ملک است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علی عالی اعلا که جبرئیل امین</p></div>
<div class="m2"><p>ضمیر هر سخنش یا علی فدیت لک است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی که مهر علی نیست ذره ای به دلش</p></div>
<div class="m2"><p>هزار مرتبه بدتر ز غاصب فدک است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی که بغض علی ذرهٔ ست در دل او</p></div>
<div class="m2"><p>یقین به گفته ی«ترکی» که کمتر از کپک است</p></div></div>