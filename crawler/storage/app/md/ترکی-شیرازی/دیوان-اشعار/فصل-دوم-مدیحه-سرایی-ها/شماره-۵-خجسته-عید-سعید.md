---
title: >-
    شمارهٔ  ۵ - خجسته عید سعید
---
# شمارهٔ  ۵ - خجسته عید سعید

<div class="b" id="bn1"><div class="m1"><p>سحر ز بانگ خروش خروس خوش گفتار</p></div>
<div class="m2"><p>شدم ز مستی خواب شبانگهی بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وضو گرفتم و کردم ادا فریضه صبح</p></div>
<div class="m2"><p>نشستم از پی تعقیب و خواندن اذکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنوز نیمی ناخوانده از دعای صباح</p></div>
<div class="m2"><p>هنوز بود کتاب دعا مرا به کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ناگهان بشنیدم صدای دق الباب</p></div>
<div class="m2"><p>ز جای جستم و نزدیک در شدم هموار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سئوال کردم ها کیستی و، کارت چیست؟</p></div>
<div class="m2"><p>ز اهل خیری یا اهل شر، تو راست چکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر گدایی و مسکین برو خدات دهد</p></div>
<div class="m2"><p>وگر که دزدی طرار، بگذر و بگذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جواب داد، نه مسکین و نی گدایم من</p></div>
<div class="m2"><p>گشای در که نیم دزد و رهزن طرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبشرم من و، دارم بشارتی نیکو</p></div>
<div class="m2"><p>کزین بشارت گردی ز خواب غم بیدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشارتی دهمت آن چنان که گر شنوی</p></div>
<div class="m2"><p>کنی دهان مرا پر ز لؤلؤ شهرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو این شنیدم در را گشودم و دیدم</p></div>
<div class="m2"><p>که بود یار دل آرام و، پیک خوش رفتار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عطف دامان، پرچیدمش ز چهره عرق</p></div>
<div class="m2"><p>ز نوک مژگان، بستردمش ز زلف غبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلام کردم و او هم به صد کرشمه و ناز</p></div>
<div class="m2"><p>جواب داد علیکم ز لعل شکربار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفتمش چه بشارت؟ خبر چه؟ واقعه چیست؟</p></div>
<div class="m2"><p>بایان نما که کنم جان به مژده تو نثار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرفت دست من و پا به صحن خانه نهاد</p></div>
<div class="m2"><p>فضای خانه من شد ز مقصدش گلزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به غمزه گفت که ای ناچشیده لذت عشق!</p></div>
<div class="m2"><p>به خنده گفت که ای ناشنیده صحبت یار!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه روی داده که هستی قرین رنج و تعب</p></div>
<div class="m2"><p>چه روی داده که گردیده ای به غصه دچار؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر ندانی که امروز عید مولود است</p></div>
<div class="m2"><p>چرا ز غصه ملولی چو مرغ بوتیمار؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هلا به شادی این عید، جام شوق بنوش</p></div>
<div class="m2"><p>به رغم دشمن بدخواه حیدر کرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برای تهنیت این خجسته عید سعید</p></div>
<div class="m2"><p>چکامه ای بسرای و، مدیحه ای بنگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدای داده به بوطالب آن چنان پسری</p></div>
<div class="m2"><p>که خیره می شود از دیدن رخش بصار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بطن فاطمه بنت سد علی ولی</p></div>
<div class="m2"><p>ظهور کرد به حکم مهیمن جبار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مه سپهر امامت، ز کعبه کرد طلوع</p></div>
<div class="m2"><p>ز چهر انور او شد جهان پر از انوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به روز سیزدهم، صبح جمعه، ماه رجب</p></div>
<div class="m2"><p>پدید شد ز حرم، آفتاب چرخ وقار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چواین بشارت، از آن گلعذار بشنیدم</p></div>
<div class="m2"><p>ز فرط شوق، شکفتم چو گل، ز باد بهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مداد و خامه و کاغذ به دست بگرفتم</p></div>
<div class="m2"><p>طلب نمودم یاری ز داور دادار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گلوی مرغک خامه، فشردم از سر شوق</p></div>
<div class="m2"><p>به قوتی که فرو ریخت مشکش از منقار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد از سیاهی مشکش به صفحه کاغذ</p></div>
<div class="m2"><p>پدید مدح علی صهر احمد مختار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>علی مروج دین محمد عربی</p></div>
<div class="m2"><p>علیست صف شکن قلب لشکر کفار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>علی مهندس این قصر لاجوردی رنگ</p></div>
<div class="m2"><p>علی که گنبد افلاک را بود معمار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>علی که قابض ارواح بی اجازه او</p></div>
<div class="m2"><p>به قبض روح کسی ناید از صغار و کبار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>علی که بی مدد لطف او نخواهد یافت</p></div>
<div class="m2"><p>کسی خلاصی روز جزا، ز شعله نار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>علی خدا نه ولی مظهر صفات خداست</p></div>
<div class="m2"><p>خرد کجا به خدائیش می کند اقرار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به امر اوست، که جرم زمین بود ساکن</p></div>
<div class="m2"><p>به حکم اوست، که چرخ برین بود دوار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز چرخ طبعم رخشنده مطلعی دیگر</p></div>
<div class="m2"><p>طلوع کرد چو تابان ستاره در شب تار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>علی است مطلع انوار پاک هشت و چهار</p></div>
<div class="m2"><p>علی است رهرو راه محمد مختار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>علی است نقطه زیرن باء بسم الله</p></div>
<div class="m2"><p>علی است پایه عرش اله را مسمار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>علی است آنکه به دشت احد ز قوم قریش</p></div>
<div class="m2"><p>نمود جاری از خونشان دو صد انهار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>علی است آنکه به سر پنجه یلی برکند</p></div>
<div class="m2"><p>دری ز قلعه خیبر چو آهنین کهسار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به فرق مرحب کافر، نواخت تیغ دو دم</p></div>
<div class="m2"><p>چنانکه راکب و مرکب دو بود گشت چهار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>علی است آنکه چو بر عمرعبدود زد تیغ</p></div>
<div class="m2"><p>خروش خواست بپا از مهاجر و انصار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز ضرب تیغ علی چهره رسول خدای</p></div>
<div class="m2"><p>شکفته شد ز فرح چون گل همیشه بهار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>علی است حاکم و محکوم اوست شمس و قمر</p></div>
<div class="m2"><p>علی است عامر و مامور اوست لیل و نهار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>علی است آنکه برآورد با حسام دودم</p></div>
<div class="m2"><p>به روز معرکه، از روزگار خصم دمار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>علی است آنکه به جای نبی به بستر خفت</p></div>
<div class="m2"><p>شبی که رفت پیمبر ز مکه جانب غار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>علی است آنکه سلاطین و خسروان زمان</p></div>
<div class="m2"><p>به بندگی غلامش همی کنند اقرار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به درگهش نشدی آدم ار پناهنده</p></div>
<div class="m2"><p>قبول می نشدی توبه اش ز استغفار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>علیست آنکه چو نوحش به یاری خود خواند</p></div>
<div class="m2"><p>رسید کشتیش از بحر بیکران به کنار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>علی است آنکه به حکم خدا مطیعش گشت</p></div>
<div class="m2"><p>عصا به دست کلیم، اژدهای آدم خوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>علی است آنکه ز تذکار نام نیکش رفت</p></div>
<div class="m2"><p>بر آسمان چهارم مسیح، از سر دار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>علی است آنکه توسل به وی چو جست خلیل</p></div>
<div class="m2"><p>بر او زحکم خداوند شد گلستان نار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>علی است باعث ایجاد کل موجودات</p></div>
<div class="m2"><p>علی است بر همه خلق جهان، سر و سردار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در این زمانه خطا رفته مادرش بی شک</p></div>
<div class="m2"><p>هر آنکسیکه کند بر امامتش انکار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر آن کسیکه علی را امام نشناسد</p></div>
<div class="m2"><p>از او خدا و رسول خدا بود بیزار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قلم شوند اگر هر چه در جهان، اشجار</p></div>
<div class="m2"><p>شود مرکب اگر هر چه در جهان، ابحار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اگر شوند تمام جهانیان کاتب</p></div>
<div class="m2"><p>ز وصف وی ننویسند عشری از اعشار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همیشه تا شعرا راست شیوه گفتن شعر</p></div>
<div class="m2"><p>همیشه تا فصحار است نظم شعر، شعار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هماره دفتر «ترکی» ز شعر خالی بود</p></div>
<div class="m2"><p>به غیر مدح رسول و ائمه اطهار</p></div></div>