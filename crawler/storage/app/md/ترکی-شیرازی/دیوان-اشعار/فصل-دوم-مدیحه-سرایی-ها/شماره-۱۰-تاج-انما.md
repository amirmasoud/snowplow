---
title: >-
    شمارهٔ  ۱۰ - تاج انما
---
# شمارهٔ  ۱۰ - تاج انما

<div class="b" id="bn1"><div class="m1"><p>شاهی که نه فلک،چو بساطی ست زیر پایش</p></div>
<div class="m2"><p>ایزد عطا نموده به سر، تاج انماش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایند سروران، به درش جبههٔ نیاز</p></div>
<div class="m2"><p>داده است سروری، به همه سروران خداش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دست صنع بافت قماش وجود خلق</p></div>
<div class="m2"><p>در کارگه نداشت از این خوبتر قماش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر خوانمش خدای بود این سخن خطا</p></div>
<div class="m2"><p>وین هم خطا بود که جدا دانم از خداش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شب که مصطفی به سوی غار ثور رفت</p></div>
<div class="m2"><p>با صد شعف به جای نبی خفت در فراش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ذوالفقار برکشد از قهر، روز جنگ</p></div>
<div class="m2"><p>افتد ز هیبتش به تن خصم ارتعاش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زد به عمر عبدوداز قهر تیغ</p></div>
<div class="m2"><p>روح الامین ز جانب حق گفت مرحباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاها! منم که «ترکی» مدحت گر توام</p></div>
<div class="m2"><p>مداحی توام شده در روزگار فاش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست من است و دامن حب تو یاعلی</p></div>
<div class="m2"><p>فردا به روز محشر، به قولم گواه باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باشد ز فرق تا قدمم غرق در گناه</p></div>
<div class="m2"><p>در محشرم به نزد خدا عذر خواه باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوز دلم به حال جگرگوشه ات حسین</p></div>
<div class="m2"><p>کوشد شهید از ستم قوم بد معاش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای شیر کردگار! حسین تو شد شهید</p></div>
<div class="m2"><p>با اشک از نجف، قدمی نه به کربلاش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بتگر که چون حسین تو رادر کنار نهر</p></div>
<div class="m2"><p>لب تشنه سر برید زکین، شمر از قفاش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن سر که جبرئیل ز مویش غبار شست</p></div>
<div class="m2"><p>خولی زکینه داد به خاک تنور جاش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عباس پور صف شکنت را نگر که چون</p></div>
<div class="m2"><p>از پیکر شریف، جداگشت دستهاش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آغشته گشت سنبل اکبربه خون و ماند</p></div>
<div class="m2"><p>چون لاله داغ بر دل لیلا و عمه هاش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در قتلگه به زینب دل خسته ات نگر</p></div>
<div class="m2"><p>ژولیده مو، سیاه به سر، چهره پرخراش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از ظلم کوفیان ستم پیشهٔ دنی</p></div>
<div class="m2"><p>یک تن به جا نماند ز انصار و اقرباش</p></div></div>