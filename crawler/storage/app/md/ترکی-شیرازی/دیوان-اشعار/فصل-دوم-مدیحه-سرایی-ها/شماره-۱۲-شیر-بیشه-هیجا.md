---
title: >-
    شمارهٔ  ۱۲ - شیر بیشهٔ هیجا
---
# شمارهٔ  ۱۲ - شیر بیشهٔ هیجا

<div class="b" id="bn1"><div class="m1"><p>خواجه فکر منزل کن، هر قدر که بتوانی</p></div>
<div class="m2"><p>پیش از آن که کاخ عمر، رو نهد به ویرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک عالم باقی، گر تورا هوس باشد</p></div>
<div class="m2"><p>این سخن شنو، بگسل دل ز عالم فانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویش را مجرد کن، از علایق دنیا</p></div>
<div class="m2"><p>وارهان دمی خود رااز خیال شیطانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند روز عمر خود در هوس مده بر باد</p></div>
<div class="m2"><p>نقد عمر خود راکن ازهوا نگهبانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در یم زلال علم، رو تو شست و شویی کن</p></div>
<div class="m2"><p>پاک کن سر و تن را از غبار نادانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل مبند بر دنیا زانکه می رود بر باد</p></div>
<div class="m2"><p>گنج و مال قارونی، حشمت سلیمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر قدر که بتوانی سر کس، مگردان فاش</p></div>
<div class="m2"><p>از ره ریا بگذر شو به خوی انسانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر که عاقلی زنهار، خلق را مکن آزار</p></div>
<div class="m2"><p>زانکه این گنه رانیست انتها و پایانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک عیش دنیا کن، کوش در ره عقبا</p></div>
<div class="m2"><p>تا ز دست دیو نفس، خویش را تو برهانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جسم پروری بگذار، روح را صفایی ده</p></div>
<div class="m2"><p>مغز را معطر کن، از شراب روحانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای گدای هر جایی! از در کسان برخیز</p></div>
<div class="m2"><p>تا بکی بری منت، بهر لقمهٔ نانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سر هوس بگذر،ره به کوی جانان بر</p></div>
<div class="m2"><p>بر درش گدایی کن، تا رسی به سلطانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه یثرب و بطحا، شیر بیشهٔ هیجا</p></div>
<div class="m2"><p>آنکه بر درش جبرئیل، یافت حکم دربانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که از سر اخلاص، بر در علی روکرد</p></div>
<div class="m2"><p>می شود ورا حاصل، قرب و جاه سلمانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشم عقل بینا کن، بر علی تولاکن</p></div>
<div class="m2"><p>تا به روضهٔ جنت، ره بری به آسانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در جهان ز نور علم، قلب را منور کن</p></div>
<div class="m2"><p>تا برون شوی یکسر، زین سرای ظلمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیر می فروشم گفت داد چون به دستم جام</p></div>
<div class="m2"><p>کای پسر مکن کاری،کآورد پشیمانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشم دل دمی بگشا، پیش پای خود بنگر</p></div>
<div class="m2"><p>تا نیفتی اندر چاه، از طریق نادانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیده بازکن ای دل، دشت کربلا را بین</p></div>
<div class="m2"><p>تا رود ز سر هوشت در کمال حیرانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کشته های بی سر بین، در میان خاک و خون</p></div>
<div class="m2"><p>در ره رضای دوست، گشته جمله قربانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بین عزیز زهرا را سر بریده از خنجر</p></div>
<div class="m2"><p>آنکه جبرئیل او را کرد مهد جنبانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نوح کربلا را بین، در میان بحر خون</p></div>
<div class="m2"><p>کشتی حیاتش را کرده چرخ طوفانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شبه مصطفی اکبر اوفتاده در میدان</p></div>
<div class="m2"><p>از جفای گل چینان هم چو سرو بستانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نام کربلا هر گه بشنود کسی بی شک</p></div>
<div class="m2"><p>دست می‌دهد او را حالت پریشانی</p></div></div>