---
title: >-
    شمارهٔ  ۴ - هفت بند نینوایی
---
# شمارهٔ  ۴ - هفت بند نینوایی

<div class="b" id="bn1"><div class="m1"><p>فلک تو رحم به اولاد بوتراب نکردی</p></div>
<div class="m2"><p>مگر به کرب و بلا ظلم بی حساب نکردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار سعی نمودی تو در خرابی یثرب</p></div>
<div class="m2"><p>چرا تو غمکده شام را خراب نکردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر حسین علی را به کربلا تو نکشتی</p></div>
<div class="m2"><p>مگر یزید لعین را تو کامیاب نکردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهی که بود ضیا دو چشم ختم رسولان</p></div>
<div class="m2"><p>چرا ز کشتن آن شاه اجتناب نکردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قتل سبط نبی کاین چنین شتاب نمودی</p></div>
<div class="m2"><p>چرا به دادن آبش چنین شتاب نکردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلوی سبط نبی را مگر ز کین نبریدی</p></div>
<div class="m2"><p>مگر محاسنش از خون سر خضاب نکردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر حسین به خاکستر تنور نهادی</p></div>
<div class="m2"><p>حیا از آن رخ چون قرص آفتاب نکردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش روی پدر تارک پسر تو شکستی</p></div>
<div class="m2"><p>چرا ز کشتن او شرم از آن جناب نکردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به قتلگاه تو سیلی زدی به روی سکینه</p></div>
<div class="m2"><p>چرا تو رحم بر آن طفل کشته باب نکردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر تو سلسله بر پای عابدین ننهادی</p></div>
<div class="m2"><p>ز تشنگی جگرش را مگر کباب نکردی</p></div></div>
<div class="b2" id="bn11"><p>ز ظلم های تو ترسم که روز محشر کبری</p>
<p>میان خلق خجالت کشی تو در بر زهرا</p></div>
<div class="b" id="bn12"><div class="m1"><p>فلک به آل نبی ظلم بی حساب نمودی</p></div>
<div class="m2"><p>مدینه را به تمنای ری خراب نمودی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز تیشهٔ ستم از پا چه نخل های فکندی</p></div>
<div class="m2"><p>چه ظلم ها که به اولاد بوتراب نمودی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کمر بخ قتل حسین بستی و حیا ننمودی</p></div>
<div class="m2"><p>برای کشتن او از چه رو شتاب نمودی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز کینه سبط نبی را به کربلا به لب آب</p></div>
<div class="m2"><p>برای قطرهٔ آبی دلش کباب نمودی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عزیز فاطمه را از قفا سرش ببریدی</p></div>
<div class="m2"><p>ز کین محاسنش از خون سر خضاب نمودی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به روی خاک فکندی تن مطهر او را</p></div>
<div class="m2"><p>به جسم انور او سایه ز آفتاب نمودی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز روی قهر تو سیلی زدی به روی سکینه</p></div>
<div class="m2"><p>زخش کبود و دلش پر ز اضطراب نمودی</p></div></div>
<div class="b2" id="bn19"><p>فلک ز ظلم تو شد سرنگون لوای امامت</p>
<p>به روز بازپسین بایدت کشید غرامت</p></div>
<div class="b" id="bn20"><div class="m1"><p>فلک به آل پیمبر مکر تو جنگ نکردی</p></div>
<div class="m2"><p>مگر به آن شه لب تشنه، عرصه تنگ نکردی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگر به کرب و بلا ابن سعد را تو ز کوفه</p></div>
<div class="m2"><p>سر سپه ننمودی و پیش هنگ نکردی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر امام زمان را تو با شتاب بریدی</p></div>
<div class="m2"><p>به قدر خوردن آبی چرا درنگ نکردی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر ز خون جوانان شاه یثرب و بطحا</p></div>
<div class="m2"><p>زمین کرب و بلا را تو لاله رنگ نکردی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گلوی اصغر شیرخوارهٔ شه را</p></div>
<div class="m2"><p>مگر ز کین، هدف ناوک خدنگ نکردی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جبین زادهٔ زهرا که داشت داغ عبادت</p></div>
<div class="m2"><p>به قتلگه مگر آزرده اش ز سنگ نکردی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به بوسه گاه نبی چوب خیزران زدی از کین</p></div>
<div class="m2"><p>مگر تو با سر از تن بریده جنگ نکردی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنین ستم که تو کردی به اهل بیت رسالت</p></div>
<div class="m2"><p>گمانم آنکه به کفار و روم و زنگ نکردی</p></div></div>
<div class="b2" id="bn28"><p>فلک به آل علی ظلم بی حساب نمودی</p>
<p>مدینه را به تمنای ری خراب نمودی</p></div>
<div class="b" id="bn29"><div class="m1"><p>فلک به آل پیمبر ببین چها کردی</p></div>
<div class="m2"><p>چه ظلم ها که به اولاد مرتضی کردی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شهی که نور دو چشم علی و فاطمه بود</p></div>
<div class="m2"><p>روانه اش ز مدینه، به کربلا کردی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در اول آب به روی حریم او بستی</p></div>
<div class="m2"><p>ز روی فاطمه نی شرم و نی حیا کردی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به روی خاک نهادی جبین سبط رسول</p></div>
<div class="m2"><p>جدا سرش ز قفا از ره جفا کردی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به خون و خاک کشیدی تن مطهر او</p></div>
<div class="m2"><p>سرش بریدی، و بر نوک نیزه ها کردی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به فرق اکبر مه طلعتش زدی شمشیر</p></div>
<div class="m2"><p>دو دست از تن عباس او جدا کردی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به زلف حضرت قاسم ز خون حنا بستی</p></div>
<div class="m2"><p>به دشت کرب و بلا شادیش عزا کردی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نخورده شیر علی اصغر صغیرش را</p></div>
<div class="m2"><p>گلوی او هدف ناوک بلا کردی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو شام تیره نمودی تو روز لیلا را</p></div>
<div class="m2"><p>به ماتم علی اکبر قدش دو تا کردی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به خیمه گاه حسینی ز کین زدی آتش</p></div>
<div class="m2"><p>به دشت کرب و بلامحشری به پا کردی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به طشت زر سر سبط رسول جا دادی</p></div>
<div class="m2"><p>ز خیزران به لبش چوب آشنا کردی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تمام اهل حرم را به ریسمان بستی</p></div>
<div class="m2"><p>بسوی شام روانشان ز کربلا کردی</p></div></div>
<div class="b2" id="bn41"><p>فلک ز جور و جفای تو داد و صد بیداد</p>
<p>ز ظلم های تو شهر مدینه، رفت به باد</p></div>
<div class="b" id="bn42"><div class="m1"><p>چو قاسمان بلا قسمت بلا کردند</p></div>
<div class="m2"><p>تمام، قسمت مظلوم کربلا کردند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چه عهد ها که به بستند کوفیان با وی</p></div>
<div class="m2"><p>عجب به عهد خود آن ناکسان وفا کردند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شه حجاز بسوی عراق با صد شور</p></div>
<div class="m2"><p>قدم نهاد و بر او کوفیان، جفا کردند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نخست آب روان را به روی او بستند</p></div>
<div class="m2"><p>به درد تشنگی اش سخت مبتلا کردند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مخالفان ز ره راست چشم پوشیدند</p></div>
<div class="m2"><p>چو نی دل حرم او پر از نوا کردند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز راه کینه نمودند تیر بارانش</p></div>
<div class="m2"><p>نه رحم بر وی و، نه شرم از خدا کردند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سری که شست ز گیسوش جبریل غبار</p></div>
<div class="m2"><p>ز تن بریده و، بر نوک نیزه ها کردند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تنی که پرورشش در کنار زهرا بود</p></div>
<div class="m2"><p>به خون کشیده و، پامال اسب ها کردند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>امیر صف شکن عباس نامدارش را</p></div>
<div class="m2"><p>کنار دجله دو دست از تنش جدا کردند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جوان تازه خطش را ز پا درآوردند</p></div>
<div class="m2"><p>ز درد فرقت او قامتش دو تا کردند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گل ریاض حسن قاسم دلیرش را</p></div>
<div class="m2"><p>در آن زمین بلا شادیش عزا کردند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خیام محترمش را ز کین زدند آتش</p></div>
<div class="m2"><p>به دشت کرببلا محشری بپا کردند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عذار دختر او را ز سیلی آزردند</p></div>
<div class="m2"><p>ز روی فاطمه نه شرم و نه حیا کردند</p></div></div>
<div class="b2" id="bn55"><p>مصیبتی که به فرزند مصطفی رو داد</p>
<p>ز جن و انس کسی در جهان ندارد یاد</p></div>
<div class="b" id="bn56"><div class="m1"><p>شنیده اید که شاهی، ز صدر زین افتاد</p></div>
<div class="m2"><p>ندیده اید که پا قاتلش به سینه نهاد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شنیده اید که شمر، از قفا برید سرش</p></div>
<div class="m2"><p>ندیده اید که از تشنگی، چسان جان داد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شنیده اید سری شد به نوک نیزه بلند</p></div>
<div class="m2"><p>ندیده اید به چرخ از دل که شد فریاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شنیده اید که از خون وضو گرفت حسین</p></div>
<div class="m2"><p>ندیده اید چسان، جبهه را به خاک نهاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شنیده اید که شد کشته نازنین پسرش</p></div>
<div class="m2"><p>ندیده اید چسان قامتش ز پا افتاد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شنیده اید که عباساوفتاد از پا</p></div>
<div class="m2"><p>ندیده اید که ساقی تشنه لب جان داد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شنیده اید که قاسم ز خون حنا بسته</p></div>
<div class="m2"><p>ندیده اید که شد کشته، با دلی ناشاد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شنیده اید که زینب ز کوفه رفت به شام</p></div>
<div class="m2"><p>ندیده اید چسان، خطبه کرد ایراد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چنین ستم که به اولاد بوتراب رسید</p></div>
<div class="m2"><p>ز جن و انس کسی در جهان ندارد یاد</p></div></div>
<div class="b2" id="bn65"><p>کنند سنگدلان ادعای دین داری</p>
<p>به کفر روی نهادند با ستمکاری</p></div>
<div class="b" id="bn66"><div class="m1"><p>به نیزه چون سر سلطان انس و جان کردند</p></div>
<div class="m2"><p>به پای نیزه به سر خاک قدسیان کردند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>صدای هلهله چون از سپاه گشت بلند</p></div>
<div class="m2"><p>میان خیمه، زنان حرم فغان کردند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دریغ و درد که از کینه، کوفیان</p></div>
<div class="m2"><p>تن امام زمان را به خون طپان کردند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شدند رو به سراپرده ها چو لشگر شام</p></div>
<div class="m2"><p>سیه لباس اسیری ببر زنان کردند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زدند چون به سراپرده آتش از ره کین</p></div>
<div class="m2"><p>به دشن کرب و بلا محشری عیان کردند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز گوش دخترکان گوشواره ها بردند</p></div>
<div class="m2"><p>رسن به گردن بیمار ناتوان کردند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز تازیانه و سیلی گروه بد آیین</p></div>
<div class="m2"><p>سیاه پشت زنان، روی کودکان کردند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>شکسته دل، چو اسیران زنگبار و فرهنگ</p></div>
<div class="m2"><p>به شام شوم، ز کرب و بلا روان کردند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بر این مصیبت عظمی خروش و آه و فغان</p></div>
<div class="m2"><p>همین نه اهل زمین، اهل آسمان کردند</p></div></div>
<div class="b2" id="bn75"><p>فلک خراب شوی ظلم تا کی و تا چند</p>
<p>به اهل بیت نبی ظلم بیش از این مپسند</p></div>