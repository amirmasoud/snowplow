---
title: >-
    شمارهٔ  ۱ - شانزده بند عاشورایی
---
# شمارهٔ  ۱ - شانزده بند عاشورایی

<div class="b" id="bn1"><div class="m1"><p>سر زد ز چرخ، باز هلال محرما</p></div>
<div class="m2"><p>پر شد جهان، ز ولوله و شور ماتما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گوشه، گشت باز لوای عزا بپا</p></div>
<div class="m2"><p>شد سینه ها پر انده و، دل ها پر از غما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد شورشی چو روز قیامت، بپا به دهر</p></div>
<div class="m2"><p>شور نشور گشت هویدا به عالما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد یا حسین خلایق ز خاص و عام</p></div>
<div class="m2"><p>افکند شور و غلغله، در عرش عظلما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم در وجود خلق جهان، گشت مضمرا</p></div>
<div class="m2"><p>انده به قلب عالمیان، گشت مد غما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دیده زین عزا نتوان دید بی بکا</p></div>
<div class="m2"><p>زین قصه نیست هیچ دلی شاد و خرما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رب عزای کیست؟در این مه که روز و شب</p></div>
<div class="m2"><p>پشت فلک، ز بار مصیبت بود خما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا رب عزای کیست؟ در این مه که انبیا</p></div>
<div class="m2"><p>ز آدم گرفته اند عزا تا به خاتما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ختم رسل برای که باشد سیاه پوش</p></div>
<div class="m2"><p>وز چیست در اشک فشاند دمادما؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باشد در آسمان چهارم،در این عزا</p></div>
<div class="m2"><p>شال عزا به گردن عیسی ابن مریما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این مه اگر نه ماه عزای حسینی است</p></div>
<div class="m2"><p>از چیست؟خلق، جمله ملولند و در هما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جا دارد ار که شیعه فشاند ز دیده خون</p></div>
<div class="m2"><p>برآنکه خاک شد کفن و، غسلش از دما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر زخم های پیکر فرزند بوتراب</p></div>
<div class="m2"><p>از اشک چشم ماتمیان است مرهما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طوفان نوح و، قصه طوفان کربلا</p></div>
<div class="m2"><p>همچون حدیث بحر محیط است و شبنما</p></div></div>
<div class="b2" id="bn15"><p>این مه، مه عزای حسین آیت هدای ست</p>
<p>مجلس نشین پیمبر و صاحب عزا خداست</p></div>
<div class="b" id="bn16"><div class="m1"><p>باز این عزای کیست؟ که صاحب عزا خداست</p></div>
<div class="m2"><p>شور فغان و ولوله در عرش کبریاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باز این عزای کیست؟ که از شرق تا به غرب</p></div>
<div class="m2"><p>در هر کجا که می نگرم ماتمی بپاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز این عزای کیست؟کزین تیره خاکدان</p></div>
<div class="m2"><p>جوش و خروش بر شده تا به هفتمین سماست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خورشید سر برهنه چرا؟ می کند طلوع</p></div>
<div class="m2"><p>در حیرتم که چرخ چرا؟ نیلگون قباست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر نیست این قیامت موعود پس ز چیست؟</p></div>
<div class="m2"><p>این انقلاب و شور که در کل ماسوای است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کروبیان عالم بالا ز غم ملول</p></div>
<div class="m2"><p>یارب عزای کیست؟ که اینگونه غم فزاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر هر کسی که می نگرم ز آشنا و غیر</p></div>
<div class="m2"><p>بیگانه با سرور و، به اندوه آشناست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ارواح انبیا همه محزون و درهمند</p></div>
<div class="m2"><p>گویا عزای سبط نبی ختم انبیاست</p></div></div>
<div class="b2" id="bn24"><p>مصباح دین، امام مبین، نور مشرقین</p>
<p>مظلوم کربلا شه لب تشنگان، حسین</p></div>
<div class="b" id="bn25"><div class="m1"><p>چون خیمه زد به کوفه، ز یثرب شه حجاز</p></div>
<div class="m2"><p>شد بسته راه صلح و، در جنگ گشت باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خواندند کوفیان ز حجازش سوی عراق</p></div>
<div class="m2"><p>تا سر نهند بر قدمش از سر نیاز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بستند آب بر رخ فرزند فاطمه</p></div>
<div class="m2"><p>گشتند آن گروه عجب میهمان نواز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کوته نظر گروه خداناشناس بین</p></div>
<div class="m2"><p>کردند دست ظلم به آل نبی دراز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بستند از جفا کمر قتل شاه دین</p></div>
<div class="m2"><p>وز کشتن امام نکردند احتراز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهر اسیر کردن اهل و عیال او</p></div>
<div class="m2"><p>آماده ساختند شتر های بی جهاز</p></div></div>
<div class="b2" id="bn31"><p>کرب و بلا چو منزل شاه شهید شد</p>
<p>دنیای بی ثبات به کام یزید شد</p></div>
<div class="b" id="bn32"><div class="m1"><p>سبط رسول شاه شهیدان کربلا</p></div>
<div class="m2"><p>بنهاد چون قدم به بیابان کربلا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با آنکه بود آب جهان، مهر مادرش</p></div>
<div class="m2"><p>لب تشنه جان سپرد به میدان کربلا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ناخورده آب قطره ای از دجلهٔ فرات</p></div>
<div class="m2"><p>خونش چو آب ریخت به بستان کربلا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شاهی که بود جای گهش دامن نبی</p></div>
<div class="m2"><p>عریان تن او فتاد به دامان کربلا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس دیو سیرتی ز پی خاتمی برید</p></div>
<div class="m2"><p>انگشت نازنین سلیمان کربلا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قاتل برید با لب تشنه، سرش ز تن</p></div>
<div class="m2"><p>خوش داشت پاس حرمت مهمان کربلا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از تیغ ظلم و خنجر اهل ستم فتاد</p></div>
<div class="m2"><p>هفتاد و دو نهال ز بستان کربلا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از کشته های غرق به خون ز آل بو تراب</p></div>
<div class="m2"><p>گردید لاله زار، گلستان کربلا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>طفل صغیر مهد ولایت، ز جان مکید</p></div>
<div class="m2"><p>پیکان به جای شیر، ز پستان کربلا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از تند باد حادثهٔ روزگار دون</p></div>
<div class="m2"><p>خاموش گشت شمع شبستان کربلا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رفتند دل شکسته، و گریان شتر سوار</p></div>
<div class="m2"><p>از کوفه تا به شام، اسیران کربلا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از ضرب تازیانه خصم لعین شدند</p></div>
<div class="m2"><p>خاموش بلبلان خوش الحان کربلا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دود از زمین، به دامن عرش برین رسید</p></div>
<div class="m2"><p>از آه آتشین یتیمان کربلا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در کربلا دمی ز نجف یا علی بیا</p></div>
<div class="m2"><p>بنگر به خون طپیده ذبیحان کربلا</p></div></div>
<div class="b2" id="bn46"><p>سیل سرشک دیدهٔ عالم فرو چکید</p>
<p>پشت فلک، ز ماتم آل عبا خمید</p></div>
<div class="b" id="bn47"><div class="m1"><p>چون شاه دین، به عزم شهادت سوار شد</p></div>
<div class="m2"><p>گفتی که خور، ز مشرق زین آشکار شد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر پشت زین، چو آن مه گردون نشین نشست</p></div>
<div class="m2"><p>عالم به چشم زینب دا خسته، تار شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفتا که ای! برادر با جان برابرم</p></div>
<div class="m2"><p>ما را غم زمانه، یکی بد هزار شد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از رفتن تو جانب میدان کارزار</p></div>
<div class="m2"><p>ما را ز دل شکیب و، ز کف اختیار شد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فکری برای قطرهٔ آبی کن ای حسین!</p></div>
<div class="m2"><p>کاصغر ز سوز تشنه لبی بی قرار شد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از زینب این شنید چو شه طفل خویش را</p></div>
<div class="m2"><p>در بر گرفت و، رو بسوی کارزار شد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفتا که ای گروه ستمکار و ددمنش</p></div>
<div class="m2"><p>بی زار از شما نبی و کردگار شد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کشتید اقربا و جوانانم از جفا</p></div>
<div class="m2"><p>از خون شان زمین بلا لاله زار شد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اکبر جوان سرو قدم شد ز کین، شهید</p></div>
<div class="m2"><p>هان! نوبت شهادت این شیرخوار شد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون می شود اگر که دهید آبش از وفا</p></div>
<div class="m2"><p>کز تاب تشنگی، جگرش داغدار شد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ناگه به جای آب، بر آن طفل شیرخوار</p></div>
<div class="m2"><p>تیری رها ز شست یکی نابکار شد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن تیر، چون به حنجر آن بی گنه رسید</p></div>
<div class="m2"><p>قنداقه اش ز خون گلویش نگار شد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آن طفل کرد بر رخ بابش تبسمی</p></div>
<div class="m2"><p>برهم نهاد دیده و، گلگون عذار شد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>روحش روان ز قالب تن شد سوی بهشت</p></div>
<div class="m2"><p>با صد شتاب، جانب جد کبار شد</p></div></div>
<div class="b2" id="bn61"><p>جز خون، گلوی تشنهٔ آن طفل تر نشد</p>
<p>چرخ از چه زین معامله زیر و زبر شد</p></div>
<div class="b" id="bn62"><div class="m1"><p>در قتلگاه چون اسرا را گذر فتاد</p></div>
<div class="m2"><p>بر نعش بی سر شهداشان نظر فتاد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از غم کشیده اند ز دل، آه آتشین</p></div>
<div class="m2"><p>کزآهشان به خرمن هستی شرر فتاد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آن یک عذار خویش به پای پدر نهاد</p></div>
<div class="m2"><p>وان یک ز پای بر سر نعش پسر فتاد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>لیلا به روی کشتهٔ اکبر، دو دست غم</p></div>
<div class="m2"><p>زد آنقدر به سر، که ز خود بی خبر فتاد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ناهگاه چشم زینب محزون در آن میان</p></div>
<div class="m2"><p>بر جسم چاک چاک شه بحر و بر فتاد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از دل کشید نالهٔ هذا اخی الحسین</p></div>
<div class="m2"><p>کز ناله اش شرر به همه خشک و تر فتاد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نزدیک شد که جان رود از پیکرش برون</p></div>
<div class="m2"><p>چون دیده اش بر آن تن ببریده سر فتاد</p></div></div>
<div class="b2" id="bn69"><p>وانگه سکینه کرد به صد خوف و اضطراب</p>
<p>با نعش چاک چاک پدر، این چنین خطاب</p></div>
<div class="b" id="bn70"><div class="m1"><p>تا سایهٔ تو بود پدر جان! به سر مرا</p></div>
<div class="m2"><p>جز عشق تو نبود خیال دگر مرا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو خشک لب شهید شدی بر لب فرات</p></div>
<div class="m2"><p>وز این قضیه خون رود از چشم تر مرا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>با حنجر بریدهٔ خود ای شهید عشق</p></div>
<div class="m2"><p>با من سخن بگوی و، مکن خون جگر مرا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>آن دم که شمر کرد تو را تشنه لب شهید</p></div>
<div class="m2"><p>ای کاش کشته بود زتو زودتر مرا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>زخم تو بی حد و، ستم خصم بی حساب</p></div>
<div class="m2"><p>زین هر دو، درد و رنج، بود بیشتر مرا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>قاتل زکشتهٔ تو جدا می کند به قهر</p></div>
<div class="m2"><p>طاقت به دل نمانده از این رهگذر مرا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>خود گفته ای یتیم نوازی بود ثواب</p></div>
<div class="m2"><p>اکون ز مهر از چه نگیری ببر مرا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گردون دون کشاند مرا سوی کربلا</p></div>
<div class="m2"><p>وز کربلا به شام کند در بدر مرا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>از کربلا به جبر، برندم به سوی شام</p></div>
<div class="m2"><p>چون نیست اختیار به کف، زین سفر مرا</p></div></div>
<div class="b2" id="bn79"><p>سوزم چو شمع زآتش داغت به قتلگاه</p>
<p>پروانه ها کشند زدل، شعله های آه</p></div>
<div class="b" id="bn80"><div class="m1"><p>از چیست؟ کرده ای تو فراموشم ای پدر!</p></div>
<div class="m2"><p>یک لحظه خود بگیر در آغوشم ای پدر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چون دوش در کنار توام بود جای خواب</p></div>
<div class="m2"><p>اکنون به یاد آمده از دوشم ای پدر!</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بر زخم های کاری تو چون نظر کنم</p></div>
<div class="m2"><p>خون جگر به سینه زند جوشم ای پدر!</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یکسو نگر که خار به پایم خلیده است</p></div>
<div class="m2"><p>یکسو ببین دریده زکین گوشم ای پدر!</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سیلی چنان زدند به رویم ستمگران</p></div>
<div class="m2"><p>از صدمه اش پریده ز سر هوشم ای پدر!</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>خامش نیم زگریه ولی خصم بدمنش</p></div>
<div class="m2"><p>از تازیانه، ساخته خاموشم ای پدر!</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خالی است جای اصغر تو در کنار من</p></div>
<div class="m2"><p>کو آن صغیر و طفل لبن نوشم ای پدر!</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>روپوش از رخم بربودند کوفیان</p></div>
<div class="m2"><p>گرد و غبار ره شده روپوشم ای پدر!</p></div></div>
<div class="b2" id="bn88"><p>می گفت راز دل به پدر، دختر حسین</p>
<p>کآمد زراه ناله کنان، خواهر حسین</p></div>
<div class="b" id="bn89"><div class="m1"><p>رو کرد سوی نعش برادر به صد ادب</p></div>
<div class="m2"><p>گفت ای گلو بریده لب آب تشنه لب!</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ای نور دیده و دل زهرا و مصطفی!</p></div>
<div class="m2"><p>ای شهسوار ملک عجم، خسرو عرب!</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>این زخم ها زچیست بر این جسم چون حریر؟</p></div>
<div class="m2"><p>وز خون چراست پیکر تو سرخ چون قصب!</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>کشتند از جفا و نکردند بر تو رحم</p></div>
<div class="m2"><p>با آنکه داشتی به رسول خدا نسب</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خصم ات به خون کشید و، نپرسیدت از نژاد</p></div>
<div class="m2"><p>سر از تنت برید و، نپرسیدت از حسب</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بنگر کنون که لالهٔ تبدار عشق را</p></div>
<div class="m2"><p>دشمن کشیده در غل و زنجیر از غضب</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>یکجا غم اسیری و، یکجا غم فراق</p></div>
<div class="m2"><p>یکجا گداخته تن زارش زسوز تب</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>من میروم به شام و، تو خوش خفته ای به خاک</p></div>
<div class="m2"><p>ای سبط سید عرب ابطحی لقب</p></div></div>
<div class="b2" id="bn97"><p>پس با دل شکسته و، با چشم اشکبار</p>
<p>رو در مدینه کرد که ای جد تا جدار!</p></div>
<div class="b" id="bn98"><div class="m1"><p>این سر جدا ز خنجر عدوان حسین توست</p></div>
<div class="m2"><p>وین پاسدار مکتب قرآن حسین توست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>این کشته ای که با تن مجروح و چاک چاک</p></div>
<div class="m2"><p>افتاده در میانهٔ میدان حسین توست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>این کشته ای که کرد سنان بر سنان سرش</p></div>
<div class="m2"><p>جسمش به خاک و خون شده غلتان حسین توست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>این آفتاب برج رسالت، که سایه وار</p></div>
<div class="m2"><p>در خون فتاده با تن عریان حسین توست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>این سرو بوستان امامت، که بر زمین</p></div>
<div class="m2"><p>افتاده از ستیزهٔ دوران حسین توست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>این گلبن ریاض شهادت، که از جفا</p></div>
<div class="m2"><p>افتاده روی خار مغیلان حسین توست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>این تشنه لب، که بر بدنش زخم تیغ و تیر</p></div>
<div class="m2"><p>باشد فزون ز ریگ بیابان حسین توست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>این کشتی نجات، که گردیده غرق خون</p></div>
<div class="m2"><p>در کربلا ز صدمهٔ طوفان حسین توست</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>این تشنهٔ فرات، که از آتش عطش</p></div>
<div class="m2"><p>دود از دلش رسیده به کیوان حسین توست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>این بسمل فتادهٔ آواره ز آشیان</p></div>
<div class="m2"><p>سیمرغ پرشکسته، ز پیکان حسین توست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>این تشنه لب شهید، که از تشنگی به خاک</p></div>
<div class="m2"><p>افتاده همچو ماهی عطشان حسین توست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>این جسم چاک چاک، که از ظلم کوفیان</p></div>
<div class="m2"><p>پامال گشته از سم اسبان حسین توست</p></div></div>
<div class="b2" id="bn110"><p>پس با زبان پر گله، ناموس کردگار</p>
<p>رو در نجف نمود که ای باب غمگسار!</p></div>
<div class="b" id="bn111"><div class="m1"><p>اکنون ز راه لطف، بیا حال ما ببین</p></div>
<div class="m2"><p>ما را اسیر، در کف اهل جفا ببین</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>یکدم به کربلا ز نجف، رنجه کن قدم</p></div>
<div class="m2"><p>ما را به صدهزار بلا مبتلا ببین</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بر ما زنان بی کس و، طفلان خردسال</p></div>
<div class="m2"><p>جور و جفای اهل ستم، بر ملا ببین</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بانگ فغان و العطش کودکان شنو</p></div>
<div class="m2"><p>فریاد و بی قراری اطفال را ببین</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>فرزند برگزیدهٔ خود را کنار نهر</p></div>
<div class="m2"><p>لب تشنه از قفا سرش از تن جدا ببین</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>آن گلبنی که فاطمه کرد آبیاریش</p></div>
<div class="m2"><p>خشکیده از ستیزهٔ قوم دغا ببین</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>آن سر که شست مادرم از گیسویش غبار</p></div>
<div class="m2"><p>پرگرد و خاک و خون، به سر نیزه ها ببین</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>آن تن که روی دامن تو یافت پرورش</p></div>
<div class="m2"><p>پامال از جفا، ز سم اسب ها ببین</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>یک دشت پر زکشته، همه لاله گون کفن</p></div>
<div class="m2"><p>در خاک و خون طپیدن این کشته ها ببین</p></div></div>
<div class="b2" id="bn120"><p>با چشم اشکبار، زمانی ز هوش شد</p>
<p>دستی ز غم به سر زد و، لختی خموش شد</p></div>
<div class="b" id="bn121"><div class="m1"><p>آتش زدند چون به سراپرده هایشان</p></div>
<div class="m2"><p>بردند سوی شام، پس از کربلایشان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>گشتند دل شکسته به جمازه ها سوار</p></div>
<div class="m2"><p>آنان که باد جان جهانی فدایشان</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>آنانکه داشت از رخشان خجلت آفتاب</p></div>
<div class="m2"><p>از آفتاب، گشت سیه چهره هایشان</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>از بس برهنه پای دویدند کودکان</p></div>
<div class="m2"><p>خاکم به سر، پرآبله گردید پایشان</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>آنان که یک به یک همه بودند در ناب</p></div>
<div class="m2"><p>دادند در خرابهٔ بی سقف جایشان</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>جز آب چشم و، لخت جگر، آل مصطفی</p></div>
<div class="m2"><p>چیز دگر نبود شراب و غذایشان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بردند اهل و شام، در آن منزل خراب</p></div>
<div class="m2"><p>خرما و نان، به رسم تصدق برایشان</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>نای وجودشان که چونی، بود پرنوا</p></div>
<div class="m2"><p>شد پرنوا تر از الم نینوایشان</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>از بس ز اهل شام شنیدند طعنه ها</p></div>
<div class="m2"><p>از یاد رفت واقعهٔ کربلایشان</p></div></div>
<div class="b2" id="bn130"><p>آسوده دشمنان امام مبین شدند</p>
<p>روزی که اهل بیت خرابه نشین شدند</p></div>
<div class="b" id="bn131"><div class="m1"><p>آه از دمی که دختر زهرا به چشم تر</p></div>
<div class="m2"><p>در مجلس یزید، کشید آه از جگر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>افکند سر به زیر در آن بزم، از حیا</p></div>
<div class="m2"><p>می کرد زیر چشم، به هر جا نبی نظر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ناگه سر برادر خود را در آن میان</p></div>
<div class="m2"><p>چون آفتاب دید درخشان، به طشت زر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>می زد ز کین، یزید لعین، چدب خیزران</p></div>
<div class="m2"><p>بر آن لبی که بود چنان درج پرگهر</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>فریاد برکشید ز دل، گفت ای یزید!</p></div>
<div class="m2"><p>بدار چوب خود ز لب این بریده سر</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>این سر، که چوب می زنی از کینه بر لبش</p></div>
<div class="m2"><p>گلبوسه ها نشاند، لبان پیامبر</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>شرمی ز روی فاطمه کن آخر ای یزید!</p></div>
<div class="m2"><p>برگو چه شد حیای تو ای شوم بد سیر!</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>افکنده ای ز کین، بدنش را به کربلا</p></div>
<div class="m2"><p>بنهاده ای به شام، سرش را به طشت زر</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>در پشت پرده ها بنشاندی زنان خویش</p></div>
<div class="m2"><p>کردی ز کینه آل علی را تو در بدر</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ما عترت پیمبر و ناموس داوریم</p></div>
<div class="m2"><p>شرمی کن ازپیمبر و خوفی ز دادگر</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>مپسند، بیش از این توبه ما ظلم و جور را</p></div>
<div class="m2"><p>آخر دمی به حال پریشان ما نگر</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ما دل شکسته گان که ز نسل پیمبریم</p></div>
<div class="m2"><p>بر ما جفا و جور مکن، ظالم آنقدر</p></div></div>
<div class="b2" id="bn143"><p>گر مصطفی به بزم تو آید کنون، یزید</p>
<p>آیا جواب چون دهی ای خصم دون، یزید</p></div>
<div class="b" id="bn144"><div class="m1"><p>ای دل بیا که نوبت آه و فغان رسید</p></div>
<div class="m2"><p>از شام غم، به کرب و بلا کاروان رسید</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>وارد چو شد امام چهارم به کربلا</p></div>
<div class="m2"><p>از شش جهت خروش، به هفت آسمان رسید</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>شوری چو شور حشر، به پا شد به کربلا</p></div>
<div class="m2"><p>بر تربت پدر، چو امام زمان رسید</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>زینب به سوی قبر برادر دوان دوان</p></div>
<div class="m2"><p>با صد هزار ناله و آه و فغان رسید</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>گفت ای به زیر خاک نهان گشته یا حیسن</p></div>
<div class="m2"><p>بنگر ز شام، زینب بی خانمان رسید</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>آخر سری ز خاک برآر و نظاره کن</p></div>
<div class="m2"><p>کز روی تو بر لبم از غصه جان رسید</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>مرگ خود از خدا طلبیدم به راه شام</p></div>
<div class="m2"><p>از بسکه ظلم و جور بر این کودکان رسید</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>گویم اگر به نزد تو ترسم شوی ملول</p></div>
<div class="m2"><p>زان ظلم ها به ما که ز شمر و سنان رسید</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>باور مکن که تا صف محشر، رود ز یاد</p></div>
<div class="m2"><p>ظلمی که از یزید، به خاندان رسید</p></div></div>
<div class="b2" id="bn153"><p>وانگه پس از سه روز، در آن دشت پربلا</p>
<p>بستند بار سوی مدینه، ز کربلا</p></div>
<div class="b" id="bn154"><div class="m1"><p>چون کاروان شام، به یثرب گشود بار</p></div>
<div class="m2"><p>اندوه و رنجشان، یکی ار بود شد هزار</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>یک کاروان زن، همه معجر به سر سیاه</p></div>
<div class="m2"><p>یک مشت کودکان، همه گیسو پر از غبار</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>تا در مدینه مژدهٔ آن کاروان، بشیر</p></div>
<div class="m2"><p>آورد شور روز قیامت، شد آشکار</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>افتاده شور و ولوله ای در میان خلق</p></div>
<div class="m2"><p>زین قصه ریخت اشک غم، از غصه روزگار</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>شد محشری که روز قیامت زیاد رفت</p></div>
<div class="m2"><p>از دست خلق، رفت برون، صبر و اختیار</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>مردان قد خمیده گروه از پی گروه</p></div>
<div class="m2"><p>زن های داغدیده قطار از پی قطار</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>روسوی کاروان، زن و مرد از چهارسو</p></div>
<div class="m2"><p>کردند پا برهنه و، با چشم اشکبار</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>خلقی به جستجوی عزیزان نوسفر</p></div>
<div class="m2"><p>جمعی به فکر تازه جوانان گلعذار</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>ناگه در آن میانه، به صد ناله و فغان</p></div>
<div class="m2"><p>برپای خاست حضرت سجاد دلفگار</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>بر چهره ریخت، از مژه خونابهٔ جگر</p></div>
<div class="m2"><p>بنمود روبه روضهٔ جد بزرگوار</p></div></div>
<div class="b2" id="bn164"><p>بر خلق گفت واقعهٔ کربلا و شام</p>
<p>افکند آتشی به دل و جان خاص و عام</p></div>
<div class="b" id="bn165"><div class="m1"><p>ای جان فدا نموده که جان ها فدای تو!</p></div>
<div class="m2"><p>وی کشته ای که هست خدا خونبهای تو!</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>لب تشنه ساختی بره دوست جان فدا</p></div>
<div class="m2"><p>قربان همت تو و، مهر و وفای تو</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>ای یادگار ساقی کوثر! کسی نگشت</p></div>
<div class="m2"><p>در کربلا به جرعهٔ آبی سقای تو</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>آبت نداده سر ببریدند کوفیان</p></div>
<div class="m2"><p>چشم جهان پرآب، بود از برای تو</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>ای در عزات خلق جهانی سیاه پوش</p></div>
<div class="m2"><p>نازم بر آن کسی که بگیرد عزای تو</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>ای متکی به دامن زهرا و مصطفی</p></div>
<div class="m2"><p>خاک زمین، برای چه شد متکای تو؟</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>در حال سجده شمر، سرت از قفا برید</p></div>
<div class="m2"><p>خاکم به سر، نکرد سوی قبله پای تو</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>مهلت نداد شمر، به زینب که تا نهد</p></div>
<div class="m2"><p>از اشک، مرهمی بسر زخم های تو</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>در حیرتم که خیمهٔ گردون، چرا نسوخت؟</p></div>
<div class="m2"><p>زان آتشی که سوخت از او خیمه های تو</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>بر دردهای عالمیان تربتت دواست</p></div>
<div class="m2"><p>ای جان فدای درد دل بی دوای تو</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>با اقربا به کوفه رسیدی تو از حجاز</p></div>
<div class="m2"><p>در کوفه کس نماند به جا، ز اقربای تو</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>شاها! روا مدار که «ترکی» تمام عمر</p></div>
<div class="m2"><p>محروم ماند از در دولت سرای تو</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>چندی است کز وطن شده دور و، علی الدوام</p></div>
<div class="m2"><p>چون نی نوا کند ز غم نینوای تو</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>افتاده خوار و، زار و، پریشان به ملک هند</p></div>
<div class="m2"><p>هر لحظه یاد می کند از کربلای تو</p></div></div>
<div class="b2" id="bn179"><p>دارد هماره سوی تو چشم امید باز</p>
<p>کاید برآستان تو ساید سر نیاز</p></div>