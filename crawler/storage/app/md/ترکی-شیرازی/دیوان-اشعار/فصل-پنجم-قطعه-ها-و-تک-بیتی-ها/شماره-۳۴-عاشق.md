---
title: >-
    شمارهٔ  ۳۴ - عاشق
---
# شمارهٔ  ۳۴ - عاشق

<div class="b" id="bn1"><div class="m1"><p>حال عاشق، ز من چه می پرسی</p></div>
<div class="m2"><p>ای که عاشق، نگشته ای روزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جهان، روزی ار شوی عاشق</p></div>
<div class="m2"><p>از جهان، چشم خویش بردوزی</p></div></div>