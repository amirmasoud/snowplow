---
title: >-
    شمارهٔ  ۴۳ - ماه صیام
---
# شمارهٔ  ۴۳ - ماه صیام

<div class="b" id="bn1"><div class="m1"><p>روزه دارا مکن به ماه صیام!</p></div>
<div class="m2"><p>خویش را از گرسنگی خسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهن خود مکن، به غیبت باز</p></div>
<div class="m2"><p>تا نباشی، سگ دهن بسته</p></div></div>