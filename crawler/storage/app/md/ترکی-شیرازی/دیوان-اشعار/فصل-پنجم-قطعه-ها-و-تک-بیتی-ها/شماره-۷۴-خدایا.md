---
title: >-
    شمارهٔ  ۷۴ - خدایا
---
# شمارهٔ  ۷۴ - خدایا

<div class="b" id="bn1"><div class="m1"><p>زطاعون و وبا، بدتر بلایی ست</p></div>
<div class="m2"><p>زن بد، در سرای نیک مردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زن بدخو، بلای جان مرد است</p></div>
<div class="m2"><p>خدایا این بلا از ما بگردان</p></div></div>