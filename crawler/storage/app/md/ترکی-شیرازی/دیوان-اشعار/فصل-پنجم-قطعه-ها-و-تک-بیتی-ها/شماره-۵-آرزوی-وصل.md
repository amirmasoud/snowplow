---
title: >-
    شمارهٔ  ۵ - آرزوی وصل
---
# شمارهٔ  ۵ - آرزوی وصل

<div class="b" id="bn1"><div class="m1"><p>راهی ست پر خطر ره عشق تو ای نگار!</p></div>
<div class="m2"><p>کس بی خطر، به منزل از این رهگذر نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی وصل تو روزم به شب رسید</p></div>
<div class="m2"><p>عمرم سر آمد و شب هجرت به سر نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت آن چه بود در نظرم جلوه گر ولی</p></div>
<div class="m2"><p>غیر از خیال روی توام کز نظر نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حسرت لطافت یاقوت لعل تو</p></div>
<div class="m2"><p>یک شب سحر نشد که زچشمم گهر نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیک نظر به دیدن چشم تو شد روان</p></div>
<div class="m2"><p>تا حد خط و خال تو نزدیکتر نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند گفتمش قدمی پیشتر گذار</p></div>
<div class="m2"><p>از ترس تیر غمزهٔ تو پیشتر نرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از برم برفت، ولی را ضیم از او</p></div>
<div class="m2"><p>کز چنین طرهٔ تو به جایی دگر نرفت</p></div></div>