---
title: >-
    شمارهٔ  ۱۸ - غلام طالع
---
# شمارهٔ  ۱۸ - غلام طالع

<div class="b" id="bn1"><div class="m1"><p>گر به صنعت عدیل داودی</p></div>
<div class="m2"><p>ور به حکمت نظیر لقمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور به زور آوری و، صف شکنی</p></div>
<div class="m2"><p>اشکبوسی و، پور دستانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا به حسن و جمال در عالم</p></div>
<div class="m2"><p>ثانی حسن ماه کنعانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو را نیست بخت و طالع نیک</p></div>
<div class="m2"><p>بی سبب، هر طرف، شتابانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زمین، آسمان، به هم دوزی</p></div>
<div class="m2"><p>باز محتاج لقمه نانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رزق بی شک تو را رسد لیکن</p></div>
<div class="m2"><p>لقمه نان به کندن جانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور تو را بخت و طالع نیک است</p></div>
<div class="m2"><p>بحر علم استی و سخندانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلق خوانند اعلم العلمات</p></div>
<div class="m2"><p>گر چه بی دانشی و نادانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«ترکیا» رو غلام طالع باش</p></div>
<div class="m2"><p>ور نه در کار خویش، حیرانی</p></div></div>