---
title: >-
    شمارهٔ  ۱۴ - جاذب خلق
---
# شمارهٔ  ۱۴ - جاذب خلق

<div class="b" id="bn1"><div class="m1"><p>ترکیا! تا زر و سیمت به کف است</p></div>
<div class="m2"><p>جاذب خلق، چو مغناطیسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظر با همه نادانیها</p></div>
<div class="m2"><p>چون فلاطون و ارسطالیسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور زرت نیست همانا به نظر</p></div>
<div class="m2"><p>خلق را شوم تر از ابلیسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر زرت هست تویی رستم زال</p></div>
<div class="m2"><p>ورنه پیرزن چرخک ریسی</p></div></div>