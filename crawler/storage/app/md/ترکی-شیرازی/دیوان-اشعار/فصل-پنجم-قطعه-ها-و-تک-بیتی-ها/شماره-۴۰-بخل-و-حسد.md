---
title: >-
    شمارهٔ  ۴۰ - بخل و حسد
---
# شمارهٔ  ۴۰ - بخل و حسد

<div class="b" id="bn1"><div class="m1"><p>عجب آید مرا که می دانیم</p></div>
<div class="m2"><p>عاقبت جای ماست زیر لحد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حق هم، در این جهان ما را</p></div>
<div class="m2"><p>نیست کاری، به غیر بخل و حسد</p></div></div>