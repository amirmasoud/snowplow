---
title: >-
    شمارهٔ  ۳۶ - غم بیهوده
---
# شمارهٔ  ۳۶ - غم بیهوده

<div class="b" id="bn1"><div class="m1"><p>اگر صد سال در این دار فانی</p></div>
<div class="m2"><p>بمانی، عاقبت بایست مردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو می دانی که باید عاقبت مرد</p></div>
<div class="m2"><p>چه حاصل، از غم بیهوده خوردن</p></div></div>