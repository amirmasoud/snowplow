---
title: >-
    شمارهٔ  ۴۶ - نرم خویی
---
# شمارهٔ  ۴۶ - نرم خویی

<div class="b" id="bn1"><div class="m1"><p>هر که با تو بدی کند روزی</p></div>
<div class="m2"><p>تو به پاداش او نکویی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تو هر کس درشت خویی کرد</p></div>
<div class="m2"><p>در مقابل، تو نرم خویی کن</p></div></div>