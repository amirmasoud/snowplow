---
title: >-
    شمارهٔ  ۳۰ - درد بی علاج
---
# شمارهٔ  ۳۰ - درد بی علاج

<div class="b" id="bn1"><div class="m1"><p>گر چه از دردها در این عالم</p></div>
<div class="m2"><p>سخت دردیست دزدی و حیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیک دردی که بی علاج بود</p></div>
<div class="m2"><p>در جهان پیری ست و بی چیزی</p></div></div>