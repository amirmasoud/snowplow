---
title: >-
    شمارهٔ  ۲ - رحمت پروردگار
---
# شمارهٔ  ۲ - رحمت پروردگار

<div class="b" id="bn1"><div class="m1"><p>ساقی بیار باده که چون نیک بنگری</p></div>
<div class="m2"><p>دنیا و هر چه هست در او اعتبار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما مست جام بادهٔ عشق و محبتیم</p></div>
<div class="m2"><p>ما را شراب و کوثر و جنت به کار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر در گه خدای جهان آفرین کسی</p></div>
<div class="m2"><p>چون من گناه کار، در این روزگار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادم از آن که با همه سنگینی گناه</p></div>
<div class="m2"><p>مایوسیم ز رحمت پروردگار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز شمار، کار چو با مرتضی علی است</p></div>
<div class="m2"><p>در دل مرا هراس ز روزشمار نیست</p></div></div>