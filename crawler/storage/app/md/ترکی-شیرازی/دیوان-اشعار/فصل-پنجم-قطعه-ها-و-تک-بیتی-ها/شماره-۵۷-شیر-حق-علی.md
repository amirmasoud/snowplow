---
title: >-
    شمارهٔ  ۵۷ - شیر حق علی
---
# شمارهٔ  ۵۷ - شیر حق علی

<div class="b" id="bn1"><div class="m1"><p>تیغ آتشبار شیر حق علی</p></div>
<div class="m2"><p>نیست آبش چون زبان وام خواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وام خواه خویش را هر کس که دید</p></div>
<div class="m2"><p>بایدش بردن به سوی حق پناه</p></div></div>