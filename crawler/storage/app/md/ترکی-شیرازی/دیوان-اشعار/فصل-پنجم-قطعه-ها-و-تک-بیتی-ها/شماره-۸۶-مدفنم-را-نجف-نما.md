---
title: >-
    شمارهٔ  ۸۶ - مدفنم را نجف نما
---
# شمارهٔ  ۸۶ - مدفنم را نجف نما

<div class="b" id="bn1"><div class="m1"><p>بارالها! به حق شاه نجف</p></div>
<div class="m2"><p>که بفرما ز لطف ممنونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدفنم را نجف نما و مکن</p></div>
<div class="m2"><p>در سناپور هند، مدفونم</p></div></div>