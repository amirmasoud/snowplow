---
title: >-
    شمارهٔ  ۲۳ - خجسته کتاب
---
# شمارهٔ  ۲۳ - خجسته کتاب

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که شد ختم این خجسته کتاب</p></div>
<div class="m2"><p>به عون خالق و جبار و قادر قهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه شیوهٔ من نظم و مدح و مرثیه بود</p></div>
<div class="m2"><p>مرا به نظم غزل کرد دوستی ناچار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غیر مدح رسول و ائمه و اطهار</p></div>
<div class="m2"><p>ز شعرهای دگر می کنم من استغفار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگوار خدایا تو خویش شاهد باش</p></div>
<div class="m2"><p>که م به معصیت خویش می کنم اقرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گناه های من از کوه هاست سنگین تر</p></div>
<div class="m2"><p>به نزد عفو تو خود ذره ای ست بی مقدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه غرق گناهم و لیک می دانم</p></div>
<div class="m2"><p>که بر تمام گناهان من تویی غفار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا که «ترکی» خوار و ذلیل و مسکینم</p></div>
<div class="m2"><p>بزرگوار خدایا! به خویش وامگذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه مجرمم و، شرمسار و، نامه سیاه</p></div>
<div class="m2"><p>تو بگذر از سر جرمم به حق هشت و چهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علی و یازده فرزند او شفیع منند</p></div>
<div class="m2"><p>وگرنه وای بر احوال من، به روز شمار</p></div></div>