---
title: >-
    شمارهٔ  ۴ - فضل خداوندی
---
# شمارهٔ  ۴ - فضل خداوندی

<div class="b" id="bn1"><div class="m1"><p>صلوه این نیست ای مرد مصلی!</p></div>
<div class="m2"><p>که گاهی خم شوی گاهی شوی راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمازت را حضور قلب باید</p></div>
<div class="m2"><p>نماز بی حضور قلب بیجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خود گویی ستون دین نماز است</p></div>
<div class="m2"><p>عمارت بی ستون سست است و بی پاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ظاهر گر چه مشغول نمازی</p></div>
<div class="m2"><p>ولیکن باطن قلبت دگر جاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنت فعلش قیام است و قعود است</p></div>
<div class="m2"><p>دلت گاهی به یثرب، گه به بطحاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو خود مشغول افعال نمازی</p></div>
<div class="m2"><p>دلت در فکر کار و بار دنیاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان مشغول ذکر حمد و سوره است</p></div>
<div class="m2"><p>دلت سیاره کوه و دشت و صحرا است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو قلب خود نمی بینی ولیکن</p></div>
<div class="m2"><p>خدا از باطن قلب تو آگاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ستادستی به پیش کردگاری</p></div>
<div class="m2"><p>که او بینندهٔ پنهان و پیداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو با بیچون خدایی روبرویی</p></div>
<div class="m2"><p>که از هر عیبی و نقصی مبراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خداوندی که علام الغیوب است</p></div>
<div class="m2"><p>اگر چه بی نیاز از طاعت ماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولیکن بندگی کردن نه این است</p></div>
<div class="m2"><p>طریق طاعت یزدان نه این هاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل خود با خدا مشغول می دار</p></div>
<div class="m2"><p>که او در کارها دانا و بیناست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو را داده است ایزد مال و دولت</p></div>
<div class="m2"><p>دلت آسوده و عیشت مهیاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر روزی تو را روزی رسان است</p></div>
<div class="m2"><p>رساند روزی ات را بی کم و کاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنین روزی رسان مهربان را</p></div>
<div class="m2"><p>خلاف بندگی کردن نه زیباست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به کار آخرت ثابت قدم باش</p></div>
<div class="m2"><p>تو را کاسباب دنیایی مهیاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برای آخرت بردار توشه</p></div>
<div class="m2"><p>که دنیایت همین امروز و فرداست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر فضل خداوندی نباشد</p></div>
<div class="m2"><p>جهنم، جاودانی منزل ماست</p></div></div>