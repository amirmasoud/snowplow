---
title: >-
    شمارهٔ  ۸۲ - ای نیک مرد!
---
# شمارهٔ  ۸۲ - ای نیک مرد!

<div class="b" id="bn1"><div class="m1"><p>دولت دنیا دو روزی بیش نیست</p></div>
<div class="m2"><p>دل بر این دنیا مبند ار عاقلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای بست دولت دنیا شدن</p></div>
<div class="m2"><p>جز پشیمانی ندارد حاصلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که دستت می رسد ای نیک مرد!</p></div>
<div class="m2"><p>سعی کن، شاید بدست آری دلی</p></div></div>