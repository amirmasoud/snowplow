---
title: >-
    شمارهٔ  ۷۱ - مرد عاقل
---
# شمارهٔ  ۷۱ - مرد عاقل

<div class="b" id="bn1"><div class="m1"><p>کسی را می توان گفتش مسلمان</p></div>
<div class="m2"><p>که از دست و زبانش کس نرنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشان مرد عاقل، غیر از این نیست</p></div>
<div class="m2"><p>که حرفی را نگوید تا نسنجد</p></div></div>