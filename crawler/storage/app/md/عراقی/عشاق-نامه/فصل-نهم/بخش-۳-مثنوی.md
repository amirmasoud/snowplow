---
title: >-
    بخش ۳ - مثنوی
---
# بخش ۳ - مثنوی

<div class="b" id="bn1"><div class="m1"><p>دیده‌ای پاک بین همی باید</p></div>
<div class="m2"><p>تا که حسنش جمال بنماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن جانان به جان توان دیدن</p></div>
<div class="m2"><p>نه به هر دیده آن توان دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که خوانی به عشق مغرورم</p></div>
<div class="m2"><p>هیچ عیبم مکن، که معذورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جمال بتم نظاره کنی</p></div>
<div class="m2"><p>بدل سیب دست پاره کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو شکل و شمایلش بینی</p></div>
<div class="m2"><p>قد و گیسو حمایلش بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو من، دل اسیر او شودت</p></div>
<div class="m2"><p>بت پرستیدن آرزو شودت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست کو را دو چشم بینا بود</p></div>
<div class="m2"><p>پس رخ خوب او دلش نربود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ کس دیدهٔ بصیر نداشت</p></div>
<div class="m2"><p>که دل و جان به حسن او نگذاشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جمالش نمی‌شکیبد دل</p></div>
<div class="m2"><p>می‌برد عقل و می‌فریبد دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن لطافت که حسن او دارد</p></div>
<div class="m2"><p>دل صاحبدلان به دام آرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق رویش همی کند پیوست</p></div>
<div class="m2"><p>حلقه در گوش عاشقان الست</p></div></div>