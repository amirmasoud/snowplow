---
title: >-
    بخش ۳ - مثنوی
---
# بخش ۳ - مثنوی

<div class="b" id="bn1"><div class="m1"><p>از تو مهرم چو در نهاد بود</p></div>
<div class="m2"><p>من کیم؟ تا مرا مراد بود ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز مرادت مرا مرادی نیست</p></div>
<div class="m2"><p>غیر ازین خاطری و یادی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه او در غم تو دل بنهاد</p></div>
<div class="m2"><p>آرزوها به آرزوی تو داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق دل‌ها ارادت تو بود</p></div>
<div class="m2"><p>ذوق جان‌ها عبادت تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که خاک درت پناه من است</p></div>
<div class="m2"><p>آستان تو سجده‌گاه من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ز کویت بدر ندانم رفت</p></div>
<div class="m2"><p>زانکه زین در کجا توانم رفت؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین سخن‌ها خلاصه دانی چیست؟</p></div>
<div class="m2"><p>آنکه: دور از تو من ندانم زیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه داری چو من هزار هزار</p></div>
<div class="m2"><p>ختم گشت این سخن برین گفتار</p></div></div>