---
title: >-
    بخش ۴ - حکایت
---
# بخش ۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>بود صاحبدلی به دانش و هوش</p></div>
<div class="m2"><p>در نواحی فارس تره‌فروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قضای خدا و صنع اله</p></div>
<div class="m2"><p>می‌گذشت او به راه خود ناگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش قصری رسید و در نگرید</p></div>
<div class="m2"><p>صورت دختر اتابک دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورتی خوب دید و حیران شد</p></div>
<div class="m2"><p>دل مجموع او پریشان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قرب سالی ز عشق می‌نالید</p></div>
<div class="m2"><p>که رخ خوب دوست باز ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایم از گریه دیده پرخون داشت</p></div>
<div class="m2"><p>چشمها چشمه‌های جیحون داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز اوصاف او نخواند و نگفت</p></div>
<div class="m2"><p>دایم از حسرتش نخورد و نخفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با سگ کوی او همی گردید</p></div>
<div class="m2"><p>سگ کویش بر آدمی بگزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بدو خادمی پیام آورد</p></div>
<div class="m2"><p>کین گذشت از حکایت آن کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر خود گیر و گوش کن سخنی</p></div>
<div class="m2"><p>چون تویی را کجا رسد چو منی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر تو سودای عاشقی داری</p></div>
<div class="m2"><p>شاید ار قصر شاه بگذاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو کجایی و ما کجا؟ هیهات!</p></div>
<div class="m2"><p>در بیابان و آرزوی فرات؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک اگر صادقی درین معنی</p></div>
<div class="m2"><p>راه برگیر و بگذر از دعوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به فلان کوه رو، مقامی ساز</p></div>
<div class="m2"><p>کنج گیر و مگوی با کس راز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طاعت کردگار عادت کن</p></div>
<div class="m2"><p>صانع خویش را عبادت کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزگاری بدین صفت می‌باش</p></div>
<div class="m2"><p>خود شود طاعت نهانی فاش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در تو مردم ارادت افزایند</p></div>
<div class="m2"><p>به تبرک به خدمتت آیند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هیچ چیزی ز کس قبول مکن</p></div>
<div class="m2"><p>نیز با هیچ کس مگوی سخن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون شوی در میان خلق علم</p></div>
<div class="m2"><p>به اتابک رسد حدیث تو هم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون اتابک تو را مرید شود</p></div>
<div class="m2"><p>اندهت را فرح پدید شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون که عاشق پیام دوست شنید</p></div>
<div class="m2"><p>امر او را به جان و دل بگزید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شد به کوهی که او اشارت کرد</p></div>
<div class="m2"><p>چار دیوارکی عمارت کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وندر آنجا، چنان که دختر گفت</p></div>
<div class="m2"><p>از عبادت نیارمید و نخفت</p></div></div>