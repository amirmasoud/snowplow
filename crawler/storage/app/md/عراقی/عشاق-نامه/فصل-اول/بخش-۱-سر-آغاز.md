---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>حبذا عشق و حبذا عشاق</p></div>
<div class="m2"><p>حبذا ذکر دوست را عشاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حبذا آن زمان که در ره عشق</p></div>
<div class="m2"><p>بیخود از سر کنند پا عشاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبرند از وفا طمع هرگز</p></div>
<div class="m2"><p>نگریزند از جفا عشاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش بلایی است عشق، از آن دارند</p></div>
<div class="m2"><p>دل و جان را درین بلا عشاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب جمال او دیدند</p></div>
<div class="m2"><p>نور دارند از آن ضیا عشاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده‌اند اندرین هوا جانها</p></div>
<div class="m2"><p>چون شکستند از آن هوا عشاق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای عراقی، چو تو نمی‌دانند</p></div>
<div class="m2"><p>این چنین درد را دوا عشاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگشادند در سرای وجود</p></div>
<div class="m2"><p>دری از عالم صفا عشاق</p></div></div>