---
title: >-
    بخش ۵ - غزل
---
# بخش ۵ - غزل

<div class="b" id="bn1"><div class="m1"><p>دل من، چون به عشق مایل شد</p></div>
<div class="m2"><p>عشق در گردنش حمایل شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل و عشق متفق گشتند</p></div>
<div class="m2"><p>دل من عشق گشت و او دل شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه بر رست چون نبات از گل</p></div>
<div class="m2"><p>از دلم عشق و گاه نازل شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی بنمود و دل ببرد و نشست</p></div>
<div class="m2"><p>کار من در فراق مشکل شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نمی‌دانم این بلا، دل را</p></div>
<div class="m2"><p>از چه افتاده وز چه حاصل شد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عراقی، مکن شکایت دل</p></div>
<div class="m2"><p>این بس او را که عشق منزل شد</p></div></div>