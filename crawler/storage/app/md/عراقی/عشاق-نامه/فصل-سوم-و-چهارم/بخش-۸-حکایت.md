---
title: >-
    بخش ۸ - حکایت
---
# بخش ۸ - حکایت

<div class="b" id="bn1"><div class="m1"><p>آن شنیدی که عاشقی جانباز</p></div>
<div class="m2"><p>وعظ گفتی به خطهٔ شیراز؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخنش منبع حقایق بود</p></div>
<div class="m2"><p>خاطرش کاشف دقایق بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی آغاز کرد بر منبر</p></div>
<div class="m2"><p>سخنی دلفریب و جان پرور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود عاشق، زد از نخست سخن</p></div>
<div class="m2"><p>سکهٔ عشق بر درست سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستمع عاشقان گرم انفاس</p></div>
<div class="m2"><p>همه مستان عشق بی می و کاس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم تازان عرصهٔ تجرید</p></div>
<div class="m2"><p>پاکبازان عالم توحید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارفی زان میان بپا برخاست</p></div>
<div class="m2"><p>گفت: عشاق را مقام کجاست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیر عاشق، که در معنی سفت</p></div>
<div class="m2"><p>از سر سوز عشق با او گفت:</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشنیدی که ایزد وهاب</p></div>
<div class="m2"><p>گفت: «طوبی لهم و حسن مب»؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این بگفت و براند از سر شوق</p></div>
<div class="m2"><p>سخن اندر میان به غایت ذوق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناگهان روستاییی نادان</p></div>
<div class="m2"><p>خالی از نور، دیدهٔ دل و جان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناتراشیده هیکلی ناراست</p></div>
<div class="m2"><p>همچو غولی از آن میان برخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لب شده خشک و دیده‌تر گشته</p></div>
<div class="m2"><p>پا ز کار اوفتاد، سر گشته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت: کای مقتدای اهل سخن</p></div>
<div class="m2"><p>غم کارم بخور، که امشب من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خرکی داشتم، چگونه خری؟</p></div>
<div class="m2"><p>خری آراسته به هر هنری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خانه‌زاد و جوان و فربه و نغز</p></div>
<div class="m2"><p>استخوانش، ز فربهی، همه مغز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من و او چون برادران شفیق</p></div>
<div class="m2"><p>روز و شب همنشین و یار و رفیق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک دم آوردم آن سبک رفتار</p></div>
<div class="m2"><p>به تفرج میانهٔ بازار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ناگهانش ز من بدزدیدند</p></div>
<div class="m2"><p>از جماعت بپرس: اگر دیدند؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مجلس گرم و غرقه در اسرار</p></div>
<div class="m2"><p>چون در آن معرض آمد این گفتار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حاضران خواستندش آزردن</p></div>
<div class="m2"><p>خر ز مسجد بپا گه آوردن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیر گفتا بدو که: ای خرجو</p></div>
<div class="m2"><p>بنشین یک زمان و هیچ مگو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نطق دربند و گوش باش دمی</p></div>
<div class="m2"><p>بنشین و خموش باش دمی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس ندا کرد سوی مجلسیان:</p></div>
<div class="m2"><p>کاندرین طایفه، ز پیر و جوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه با عشق در نیامیزد</p></div>
<div class="m2"><p>زین میانه به پای برخیزد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ابلهی، همچو خر، کریه لقا</p></div>
<div class="m2"><p>چست برخاست، از خری، برپا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیر گفتا: تویی که در یاری</p></div>
<div class="m2"><p>دل نبستی به عشق؟ گفت: آری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بانگ بر زد، بگفت: ای خر دار</p></div>
<div class="m2"><p>هان! خرت یافتم بیار افسار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ویحک! ای بی‌خبر ز عالم عشق</p></div>
<div class="m2"><p>ناچشیده حلاوت غم عشق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خر صفت، بار کاه و جو برده</p></div>
<div class="m2"><p>بی‌خبر زاده، بی‌خبر مرده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از صفاهای عشق روحانی</p></div>
<div class="m2"><p>بی‌خبر در جهان، چو حیوانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طرفه دون همتی و بی‌خبری</p></div>
<div class="m2"><p>که ندارد به دلبری نظری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر حرارت، که عقل شیدا کرد</p></div>
<div class="m2"><p>نور خورشید عشق پیدا کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر لطافت، که در جمال افزود</p></div>
<div class="m2"><p>اثر عشق پاکبازان بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر تو پاکی، نظر به پاکی کن</p></div>
<div class="m2"><p>منقطع از طباع خاکی کن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سوز اهل صفا به بازی نیست</p></div>
<div class="m2"><p>عشقبازی خیالبازی نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رو، در عشق آن نگارین زن</p></div>
<div class="m2"><p>که تو از عشق او شدی احسن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر که عشقش نپخت و خام بماند</p></div>
<div class="m2"><p>مرغ جانش اسیر دام بماند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عشق ذوقی است، همنشین حیات</p></div>
<div class="m2"><p>بلکه چشم است بر جبین حیات</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عشق افزون ز جان و دل جانی است</p></div>
<div class="m2"><p>بلکه در ملک روح سلطانی است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گاه باشد که عشق جان گردد</p></div>
<div class="m2"><p>گاه در جان جان نهان گردد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گاه جان زنده شد، حیاتش عشق</p></div>
<div class="m2"><p>گاه شد چون زمین، نباتش عشق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آب در میوهٔ خرد عشق است</p></div>
<div class="m2"><p>بلکه آب حیات خود عشق است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>لذت عشق عاشقان دانند</p></div>
<div class="m2"><p>پاکبازان جان فشان دانند</p></div></div>