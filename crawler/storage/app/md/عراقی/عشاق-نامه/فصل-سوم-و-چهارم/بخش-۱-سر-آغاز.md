---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>آن غریبان منزل دنیی</p></div>
<div class="m2"><p>آن عزیزان جنت‌الماوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محرمان سراچهٔ قدسی</p></div>
<div class="m2"><p>لوح خوانان سر نه کرسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالکان طریقهٔ علیا</p></div>
<div class="m2"><p>راه‌داران جادهٔ سفلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنده جانان مرده در غم یار</p></div>
<div class="m2"><p>مست حالان جان و دل هشیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاهان تخت روحانی</p></div>
<div class="m2"><p>غوطه‌خواران بحر نورانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهبازان در قفس مانده</p></div>
<div class="m2"><p>پیش بینان بازپس مانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حدود وجود گم گشته</p></div>
<div class="m2"><p>وز عقول و نفوس بگذشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کسی شان، ز دوست پروا، نه</p></div>
<div class="m2"><p>سوخته، چون ز شمع، پروانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو پروانه ز اشتیاق رخش</p></div>
<div class="m2"><p>خویشتن را فگنده در آتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ره دوست پا ز سر کرده</p></div>
<div class="m2"><p>ابجد عشق را ز بر کرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ز کتاب دهر جیفه شده</p></div>
<div class="m2"><p>بر سریر صفا خلیفه شده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یار خود دیده در پس پرده</p></div>
<div class="m2"><p>تن به جان مانده، جان فدا کرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می نخورده شده به بویی مست</p></div>
<div class="m2"><p>دوست نادیده دل بداده ز دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر ره یار منتظر مانده</p></div>
<div class="m2"><p>نمک شوق بر دل افشانده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بار محنت کشیده چون ایوب</p></div>
<div class="m2"><p>زهر فرقت چشیده چون یعقوب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نظر جان ز جسم بگسسته</p></div>
<div class="m2"><p>صدق «میعاد» باز دانسته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرده از جان بسوی کوش چوروی</p></div>
<div class="m2"><p>«لیس فی جبتی سوی الله» گوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان «اناالحق» زنان و تن بردار</p></div>
<div class="m2"><p>فارغ از جنت و گذشته ز نار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>علم اتحاد بر بسته</p></div>
<div class="m2"><p>لشکر خشم و آز بشکسته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بن و بیخ خیال برکنده</p></div>
<div class="m2"><p>گشته آزاد و هم چنان بنده</p></div></div>