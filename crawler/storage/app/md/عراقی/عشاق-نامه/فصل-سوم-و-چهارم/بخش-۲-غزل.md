---
title: >-
    بخش ۲ - غزل
---
# بخش ۲ - غزل

<div class="b" id="bn1"><div class="m1"><p>جنت قرب جای ایشان است</p></div>
<div class="m2"><p>نور رضوان صفای ایشان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان من در هوای ایشان است</p></div>
<div class="m2"><p>تن من خاک پای ایشان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل کل هست گنگ و لایعقل</p></div>
<div class="m2"><p>هر کجا ماجرای ایشان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی، که عرش ذرهٔ اوست</p></div>
<div class="m2"><p>مطلعش بر سمای ایشان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ازل، چون قبول یافته‌اند</p></div>
<div class="m2"><p>ابد اندر بقای ایشان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه در عشق خود فنا طلبند</p></div>
<div class="m2"><p>که بقا در فنای ایشان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلم و ترک و حیا نشانهٔ‌شان</p></div>
<div class="m2"><p>علم و تقوی لوای ایشان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جناب خدای در دو جهان</p></div>
<div class="m2"><p>این مراتب برای ایشان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این مراتب به ذات ایشان نیست</p></div>
<div class="m2"><p>کین کرم از خدای ایشان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرچه اندر جهان عراقی یافت</p></div>
<div class="m2"><p>اثری از عطای ایشان است</p></div></div>