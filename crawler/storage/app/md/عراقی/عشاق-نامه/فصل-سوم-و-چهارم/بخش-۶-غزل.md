---
title: >-
    بخش ۶ - غزل
---
# بخش ۶ - غزل

<div class="b" id="bn1"><div class="m1"><p>هر دلی کان به عشق مایل نیست</p></div>
<div class="m2"><p>حجرهٔ دیو دان، که آن دل نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاغ، گو: بی‌خبر بمیر از عشق</p></div>
<div class="m2"><p>که ز گل، عندلیب غافل نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بی‌عشق چشم بی‌نور است</p></div>
<div class="m2"><p>خود ببین حاجب دلایل نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌دلان را جز آستانهٔ عشق</p></div>
<div class="m2"><p>در ره کوی دوست منزل نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که مجنون شود درین سودا</p></div>
<div class="m2"><p>ای عراقی، مگو که عاقل نیست</p></div></div>