---
title: >-
    بخش ۴ - غزل
---
# بخش ۴ - غزل

<div class="b" id="bn1"><div class="m1"><p>ای ز روی تو آفتاب خجل</p></div>
<div class="m2"><p>وز لبت آب زندگی حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان را خیال عارض تو</p></div>
<div class="m2"><p>در شب تیره نور دیده و دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه روی تو را ز غایت لطف</p></div>
<div class="m2"><p>برگ گل شرمسار و لاله خجل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آرزوی قد تو سرو سهی</p></div>
<div class="m2"><p>خشک بر جای مانده پا در گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای لبت را اسیر آب حیات</p></div>
<div class="m2"><p>وی رخت را غلام شمع چگل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای کمند گیسویت</p></div>
<div class="m2"><p>رشتهٔ جان عاشقان مگسل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رمقی بود باقی از جانم</p></div>
<div class="m2"><p>که تو ناگه بدو شدی واصل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وای اگر خاطرت به جانب ما</p></div>
<div class="m2"><p>لحظه‌ای دیرتر شدی مایل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اتفاقی عجب: عراقی و وصل!</p></div>
<div class="m2"><p>زانکه آشفته گم کند منزل</p></div></div>