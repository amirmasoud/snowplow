---
title: >-
    بخش ۳ - مثنوی
---
# بخش ۳ - مثنوی

<div class="b" id="bn1"><div class="m1"><p>تا غمت با من آشنایی کرد</p></div>
<div class="m2"><p>دلم از جان خود جدایی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا غم تو قبول کرد مرا</p></div>
<div class="m2"><p>هستی خود ملول کرد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سماع توام، چو حال گرفت</p></div>
<div class="m2"><p>از وجود خودم ملال گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیت عشق تو چو بر خواندم</p></div>
<div class="m2"><p>مایهٔ جان و دل برافشاندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا آفتاب حسن تو تافت</p></div>
<div class="m2"><p>عاشقان را بجست و نیک بیافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر، ای آفتاب جان‌افروز</p></div>
<div class="m2"><p>شب ما از رخ تو گردد روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر آن بس بود ز روی تو تاب</p></div>
<div class="m2"><p>گو: دگر آفتاب و ماه متاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای ز عشاق گرم بازارت</p></div>
<div class="m2"><p>به ز من عالمی خریدارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من کیم، تا زنم ز عشق تو لاف؟</p></div>
<div class="m2"><p>نیست دعوای این سخن ز گزاف</p></div></div>