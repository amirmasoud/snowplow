---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>ما مقیم آستان توایم</p></div>
<div class="m2"><p>عندلیبان بوستان توایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رویم از درت و گر نرویم</p></div>
<div class="m2"><p>از تو گوییم و هم ز تو شنویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون که در دام تو گرفتاریم</p></div>
<div class="m2"><p>از تو پروای خویش چون داریم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دم از آشنایی تو زنیم</p></div>
<div class="m2"><p>میل بیگانگی چگونه کنیم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر ما و آستانهٔ در تو</p></div>
<div class="m2"><p>منتظر تا رویم در سر تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مپندار کز در تو رویم</p></div>
<div class="m2"><p>به سر تو، که در سر تو رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ز عشق تو جرعه‌ای خوردیم</p></div>
<div class="m2"><p>دل بدادیم و جان فدا کردیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به کوی تو راهبر گشتیم</p></div>
<div class="m2"><p>جز تو، از هرچه بود برگشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ز جان با غم تو پیوستیم</p></div>
<div class="m2"><p>رخت هستی خویش بربستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ز شوق تو مست و حیرانیم</p></div>
<div class="m2"><p>ره به هستی خود نمی‌دانیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون به سودای تو گرفتاریم</p></div>
<div class="m2"><p>سر سودای خود کجا داریم؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تاب حسن تو آتشی افروخت</p></div>
<div class="m2"><p>دل ما را بدان بخواهد سوخت</p></div></div>