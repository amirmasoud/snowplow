---
title: >-
    بخش ۵ - غزل
---
# بخش ۵ - غزل

<div class="b" id="bn1"><div class="m1"><p>تیری، ای دوست، برکش از ترکش</p></div>
<div class="m2"><p>پس به آبروی چون کمان درکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان! دلم گر نشانه می‌خواهی</p></div>
<div class="m2"><p>زدن از توست و از من آهی خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی ز تیرت الم رسد؟ که مرا</p></div>
<div class="m2"><p>دیده در حیرت است و دل در غش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یابم از دیدن تو آب حیات</p></div>
<div class="m2"><p>ور بسوزانیم تو در آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواه نوش است و خواه زهرآلود</p></div>
<div class="m2"><p>شربت از دست دوست خوش درکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور دهد غیر شربت نوشت</p></div>
<div class="m2"><p>نیش دان و به خاک ریز و مچش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عراقی مگو: بیا بر من</p></div>
<div class="m2"><p>خویشتن را بگوی، ای دلکش</p></div></div>