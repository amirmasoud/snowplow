---
title: >-
    بخش ۷ - سبب نظم کتاب
---
# بخش ۷ - سبب نظم کتاب

<div class="b" id="bn1"><div class="m1"><p>جان من چون به عالم دل شد</p></div>
<div class="m2"><p>با صفا جمع گشت و حامل شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت حاصل ز فیض ربانی</p></div>
<div class="m2"><p>در وجودم جنین روحانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون محبت به شوق تسویه داد</p></div>
<div class="m2"><p>قابلهٔ عشق یافت چون می‌زاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدمش، چون ز غیب روی نمود</p></div>
<div class="m2"><p>قرةالعین نیک موزون بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مهاد هواش پیوسته</p></div>
<div class="m2"><p>به قماط هوس فرو بسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داد پستان فکر من به صفا</p></div>
<div class="m2"><p>شیر «حولین کاملین» او را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب و روزش غذا ز اشواق است</p></div>
<div class="m2"><p>گر چه طفل است، پیر عشاق است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورتش همچو معنیش زیبا</p></div>
<div class="m2"><p>خالی از حشو و صافی از ایطا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ چشمی ندیده در خوابش</p></div>
<div class="m2"><p>رخ ندید آفتاب و مهتابش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راه خور از دریچه ناداده</p></div>
<div class="m2"><p>سایه‌اش بر زمین نیفتاده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساکن حجرهٔ امانت بود</p></div>
<div class="m2"><p>در پس پردهٔ صیانت بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نقش او را، ز صانعی که ببست</p></div>
<div class="m2"><p>از معانی هر آنچه خواهی هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مستم از بادهٔ هوایش، مست</p></div>
<div class="m2"><p>که جگر گوشهٔ لطیف من است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منزل او شریف جایی بود</p></div>
<div class="m2"><p>زانکه در کوی آشنایی بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>راستی هست مونسی خوش خوی</p></div>
<div class="m2"><p>نیک خاموش، لیک شیرین گوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لفظ و معنی او همه مطبوع</p></div>
<div class="m2"><p>عشق را بیت های او ینبوع</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فصل او را هزار نوع بهار</p></div>
<div class="m2"><p>گه بود گلستان و گه گلزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غزلیات و مثنویاتش</p></div>
<div class="m2"><p>چون حکایات او به غایت خوش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بی‌قدم در جهان همی پوید</p></div>
<div class="m2"><p>بی‌زبان مدح خواجه می‌گوید</p></div></div>