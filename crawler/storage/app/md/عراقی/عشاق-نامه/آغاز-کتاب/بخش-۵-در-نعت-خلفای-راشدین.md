---
title: >-
    بخش ۵ - در نعت خلفای راشدین
---
# بخش ۵ - در نعت خلفای راشدین

<div class="b" id="bn1"><div class="m1"><p>چار یارش، که مرشد دینند</p></div>
<div class="m2"><p>همه اندر مقام تحسینند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستان پیمبرند همه</p></div>
<div class="m2"><p>خلفای مطهرند همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای فضولی، چرا ز نادانی</p></div>
<div class="m2"><p>یار اینی و دشمن آنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو هوایی اگر نورزی به</p></div>
<div class="m2"><p>سه طلاق خیال فاسد ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو چه دانی درین میانه چه بود ؟</p></div>
<div class="m2"><p>کین چرا پیش ازان خلیفه نبود ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو چه دانی مصالح این کار؟</p></div>
<div class="m2"><p>چه به خود راه می‌دهی انکار؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه را نیک دان، مباش فضول</p></div>
<div class="m2"><p>جز نکو کی بود رفیق رسول؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد هزاران دریچه از رضوان</p></div>
<div class="m2"><p>مفتتح در مضاجع ایشان</p></div></div>