---
title: >-
    بخش ۶ - در نصیحت
---
# بخش ۶ - در نصیحت

<div class="b" id="bn1"><div class="m1"><p>تا کی، ای مست خواب غفلت و جهل</p></div>
<div class="m2"><p>گوش سوی مقلد نااهل؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به مقصد درین طریق تو را</p></div>
<div class="m2"><p>کی رساند دلیل نابینا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سازده، یار گیر دانش و عقل</p></div>
<div class="m2"><p>رخت بر بند ازین سراچهٔ نقل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسی از همه تبرا کن</p></div>
<div class="m2"><p>ساعتی چشم خویشتن وا کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لحظه‌ای درگذر ازین پس و پیش</p></div>
<div class="m2"><p>لمحه‌ای در نگر به عالم خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند مانی تو این چنین خفته؟</p></div>
<div class="m2"><p>همره از راه منزلی رفته؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به طلب در جهان چه می‌پویی؟</p></div>
<div class="m2"><p>چو تو گم گشته‌ای، چه می‌جویی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده بگشای، ای که در خوابی</p></div>
<div class="m2"><p>خویشتن را طلب، مگر یابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند ازین اشتغال بی‌حاصل؟</p></div>
<div class="m2"><p>دیگران را و خود ز خود غافل؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا تو در خویشتن نظر نکنی</p></div>
<div class="m2"><p>وانگه از خویشتن گذر نکنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نرسانی نظر به عین کمال</p></div>
<div class="m2"><p>نشناسی فراق را ز وصال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایزد آخر نیافریدت تن</p></div>
<div class="m2"><p>همه از بهر خوردن و خفتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندرین صورت ضعیف اساس</p></div>
<div class="m2"><p>جان معنی است، سعی کن، بشناس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا کی، ای همچو گاو سر در پیش</p></div>
<div class="m2"><p>طعمه‌ای گرگ نفس را چون میش؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تن تو خاک تیره را شد فرش</p></div>
<div class="m2"><p>دل و جان تو تاج و قبهٔ عرش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صورتی را، که جان معنی هست</p></div>
<div class="m2"><p>منجنیق اجل اگر بشکست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مغز او را ز پوست به بیند</p></div>
<div class="m2"><p>باز گشتن به دوست به بیند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای که غافل ز حال خود شده‌ای</p></div>
<div class="m2"><p>چون بدانجا روی که آمده‌ای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از تو آخر بپرسد ایزد پاک</p></div>
<div class="m2"><p>گوید: ای جرم کردهٔ ناپاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرده بودی به مردمی دعوی</p></div>
<div class="m2"><p>حاصلت کو ز صورت و معنی؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روزی اندر سراچهٔ شاهی</p></div>
<div class="m2"><p>کار ناکرده مزد می‌خواهی؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که دل در امور سفلی بست</p></div>
<div class="m2"><p>به بلاهای جاودان پیوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر دلی کو هوای دنیا خواست</p></div>
<div class="m2"><p>در تن افزود، لیک از جان کاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که در ملک جان امین نبود</p></div>
<div class="m2"><p>خازن نقد ماء و طین نبود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گوهری پیش مفلسی ننهند</p></div>
<div class="m2"><p>این بلندی به هر کسی ندهند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عاشقان راست این مقام، آری</p></div>
<div class="m2"><p>عاشقان را سزد چنین کاری</p></div></div>