---
title: >-
    بخش ۲ - اندر جوهر انسان
---
# بخش ۲ - اندر جوهر انسان

<div class="b" id="bn1"><div class="m1"><p>مبدا امر جوهر انسان</p></div>
<div class="m2"><p>قابل علم کرد در پی آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آلتی از کرم بدو بخشید</p></div>
<div class="m2"><p>که بدان نیک را ز بد بگزید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادش ایجاب و سلب هر تحقیق</p></div>
<div class="m2"><p>در جهان تصور و تصدیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون رقم بر وجود انسان راند</p></div>
<div class="m2"><p>«اعملوا صالحا» بر ایشان خواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما همه ناقصیم و اوست تمام</p></div>
<div class="m2"><p>ابدا ذوالجلال و الاکرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحدت او مقدس از تمثیل</p></div>
<div class="m2"><p>صنعت او منزه از تحلیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نگویم که جان جان است او</p></div>
<div class="m2"><p>هر چه گویم ورای آن است او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او مبراست از «هنا» و «هناک»</p></div>
<div class="m2"><p>ز اول فکر و آخر ادراک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست سوی حقیقت الله</p></div>
<div class="m2"><p>نفی و اثبات «لا» و «هو» را راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه ادراک آن کند افهام</p></div>
<div class="m2"><p>یا بود در تصور اوهام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر همه مغز هست و گر همه پوست</p></div>
<div class="m2"><p>هر چه موجود ازوست بل همه اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز وجود خدای در دو جهان</p></div>
<div class="m2"><p>دومین نقش چشم احول دان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امر را اوست اول و آخر</p></div>
<div class="m2"><p>خلق را اوست باطن و ظاهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خانه‌های تن از دریچهٔ جان</p></div>
<div class="m2"><p>هست روشن به نور «الرحمن»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست او نور آسمان و زمین</p></div>
<div class="m2"><p>پرتو نور اوست روح امین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که را در میان جان نور است</p></div>
<div class="m2"><p>مغز جانش برای آن نور است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کند اندر زجاجهٔ مصباح</p></div>
<div class="m2"><p>شام مشکوة را بدل به صباح</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان چو با نور هم‌نشین باشد</p></div>
<div class="m2"><p>آهن از آتش آتشین باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوست تشبیه نور کرد به نار</p></div>
<div class="m2"><p>نیک از آن روز گشت ما را کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون که معشوق روی بنماید</p></div>
<div class="m2"><p>بصرم را بصیرت افزاید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هیچ کس زان نظر سبق نبرد</p></div>
<div class="m2"><p>تا به نور خدای می‌نگرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر تو کردی به چشم خویش نگاه</p></div>
<div class="m2"><p>«انه ناظرا بنور الله»</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون تقرب کنی به طاعت دوست</p></div>
<div class="m2"><p>چشم و گوش و زبان و مغز تو اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون بدو گویی و بدو شنوی</p></div>
<div class="m2"><p>پیش هستی او تو نیست شوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون ز خورشید شد ضیا پیدا</p></div>
<div class="m2"><p>چون نگردد ستاره ناپیدا؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هیچ طالب به خود درو نرسید</p></div>
<div class="m2"><p>روی او هم بدو توانی دید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاک را نیست ره به عالم پاک</p></div>
<div class="m2"><p>جان مگر هم به جان کند ادراک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در ثنایش کسی که خاموش است</p></div>
<div class="m2"><p>نیش اندیشه در دلش نوش است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گنگ گشتم درو و «ما احصی»</p></div>
<div class="m2"><p>« و ثناء علیه لااحصی»</p></div></div>