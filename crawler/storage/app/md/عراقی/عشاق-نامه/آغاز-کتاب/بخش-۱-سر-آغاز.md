---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>هر که جان دارد و روان دارد</p></div>
<div class="m2"><p>واجب است آنکه درد جان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حمد بی‌حد کردگار احد</p></div>
<div class="m2"><p>صمد لم یلد و لم یولد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه ذاتش بری است از آهو</p></div>
<div class="m2"><p>الذی لا اله الا هو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مالک الملک قادر بی‌عیب</p></div>
<div class="m2"><p>صانع عالم شهادت و غیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ربنا جل قدره و علا</p></div>
<div class="m2"><p>آنکه از بدو فطرت اولی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق در دست قدرت او بود</p></div>
<div class="m2"><p>قدرتش دستبرد صنع نمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صانعی، کز مطالع ابداع</p></div>
<div class="m2"><p>او برآرد حقایق انواع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس چهل طورشان در آن اشکال</p></div>
<div class="m2"><p>برد از جا به جا و حال به حال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روح‌ها داد روح را زان راح</p></div>
<div class="m2"><p>به صبوحی اربعین صباح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امر او بر طریق کن فیکون</p></div>
<div class="m2"><p>هم چنان کاف نارسیده به نون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفرینندهٔ زمان و مکان</p></div>
<div class="m2"><p>در جهات طبایع و ارکان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خلق را در جهان کون و فساد</p></div>
<div class="m2"><p>هست او مبدا و بدوست معاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان پدر هفت کرد و مادر چار</p></div>
<div class="m2"><p>تا سه فرزند را بود اظهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صنعش از آب و خاک و آتش و باد</p></div>
<div class="m2"><p>جسم را طول و عرض و عمق او داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زان طرف روشنی و نزدیکی</p></div>
<div class="m2"><p>زین طرف بعد بود و تاریکی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون شد از خاک تیره طینت تن</p></div>
<div class="m2"><p>کرد امرش به نور جان روشن</p></div></div>