---
title: >-
    بخش ۸ - مثنوی
---
# بخش ۸ - مثنوی

<div class="b" id="bn1"><div class="m1"><p>جز حدیث تو من نمی‌دانم</p></div>
<div class="m2"><p>خامشی از سخن نمی‌دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کمند غم تو پا بستم</p></div>
<div class="m2"><p>وز می اشتیاق تو مستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدهٔ ما، اگر چه بی‌نور است</p></div>
<div class="m2"><p>لیک نزدیک بین هر دور است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساکن است او، مگر تو بشتابی</p></div>
<div class="m2"><p>در نیابد، مگر تو دریابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه ما خود نه مرد عشق توایم</p></div>
<div class="m2"><p>لیک جویان درد عشق توایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طالبان را ره طلب بگشای</p></div>
<div class="m2"><p>راه مقصود را به ما بنمای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل و دنیای خویش در کویت</p></div>
<div class="m2"><p>همه دادم به دیدن رویت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب، این دولتم میسر باد</p></div>
<div class="m2"><p>که به دیدار دوست گردم شاد</p></div></div>