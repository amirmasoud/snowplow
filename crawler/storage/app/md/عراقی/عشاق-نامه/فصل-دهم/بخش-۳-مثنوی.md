---
title: >-
    بخش ۳ - مثنوی
---
# بخش ۳ - مثنوی

<div class="b" id="bn1"><div class="m1"><p>عکس هر مویت، ای بت رعنا</p></div>
<div class="m2"><p>در دماغم رگی است از سودا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از وصال قد تو ای دلدار</p></div>
<div class="m2"><p>نیست جز گیسوی تو برخوردار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرق کردن به چشم سر نتوان</p></div>
<div class="m2"><p>موی فرق تو را، ز موی میان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد دلم، تا شدم گرفتارت</p></div>
<div class="m2"><p>به طمع طره‌های طرارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی زلفت فراز عارض خوش</p></div>
<div class="m2"><p>سوخت ما را، چو موی در آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ربوده دلم به پیشانی</p></div>
<div class="m2"><p>الحق آن نیز هم به پیشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور ماه است، یا شعاع جبین؟</p></div>
<div class="m2"><p>شمع پروانه سوز؟ یا پروین؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانده زان غمزه در شگفتم من</p></div>
<div class="m2"><p>هست بیمار و مست و مردافکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ تو خسته جان تواند دید</p></div>
<div class="m2"><p>چون بدین دیده آن تواند دید؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب لعلت، که روح بخش دل است</p></div>
<div class="m2"><p>برگ گل از لطافتش خجل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشقان تو پاکبازانند</p></div>
<div class="m2"><p>صید عشق تو شاهبازانند</p></div></div>