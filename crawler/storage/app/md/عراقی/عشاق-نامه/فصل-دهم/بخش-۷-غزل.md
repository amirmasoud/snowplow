---
title: >-
    بخش ۷ - غزل
---
# بخش ۷ - غزل

<div class="b" id="bn1"><div class="m1"><p>سهل گفتی به ترک جان گفتن</p></div>
<div class="m2"><p>من بدیدم، نمی‌توان گفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان فرهاد خسته شیرین است</p></div>
<div class="m2"><p>کی تواند به ترک جان گفتن؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوست می‌دارمت به بانگ بلند</p></div>
<div class="m2"><p>تا کی آهسته و نهان گفتن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصف حسن جمال خود خود گو</p></div>
<div class="m2"><p>حیف باشد به هر زبان گفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به حدی است شکر دهنت</p></div>
<div class="m2"><p>که نشاید سخن در آن گفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نبودی کمر، میانت را</p></div>
<div class="m2"><p>کی توانستمی نشان گفتن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آرزوی لبت عراقی را</p></div>
<div class="m2"><p>شد مسلم حدیث جان گفتن</p></div></div>