---
title: >-
    بخش ۴ - حکایت
---
# بخش ۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شیخ السلام امام غزالی</p></div>
<div class="m2"><p>آن صفا بخش حالی و قالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واله حسن خوبرویان بود</p></div>
<div class="m2"><p>در ره عشق دوست جویان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود چشم صفای آن صادق</p></div>
<div class="m2"><p>برنگاری، به جان، چنان عاشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که همی شد سوار اندر ری</p></div>
<div class="m2"><p>وز مریدان فزون ز صد در پی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلبری دید همچو بدر تمام</p></div>
<div class="m2"><p>که برون آمد از یکی حمام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده از لطف و صنع ربانی</p></div>
<div class="m2"><p>تاب حسنش جهان نورانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ را چون نظر برو افتاد</p></div>
<div class="m2"><p>صورت دوست دید، باز استاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دل و جان درو همی نگرید</p></div>
<div class="m2"><p>هر نظر او به روی دیگر دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شده مردم به شیخ در، نگران</p></div>
<div class="m2"><p>شیخ در روی آن پری حیران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صوفیان جمله منفعل گشتند</p></div>
<div class="m2"><p>همه بگذاشتند و بگذشتند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک پیری، که بود غاشیه‌دار</p></div>
<div class="m2"><p>شیخ را گفت: بگذر و بگذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تبع صورت از تو لایق نیست</p></div>
<div class="m2"><p>شرمت ازین همه خلایق نیست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شیخ گفتش: مگوی هیچ سخن</p></div>
<div class="m2"><p>«ریة الحسن راحة الاعین»</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نیفتادمی به صورت زار</p></div>
<div class="m2"><p>بودیم جیرئیل غاشیه‌دار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاشقانی که مست و مدهوشند</p></div>
<div class="m2"><p>باده از جام عشق می‌نوشند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز اندرون غافل است بیرون بین</p></div>
<div class="m2"><p>روی لیلی به چشم مجنون بین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حسن صورت چو آلت است تو را</p></div>
<div class="m2"><p>پس به کاری حوالت است تو را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مغز خود ز اندرون پوست ببین</p></div>
<div class="m2"><p>زان شعاعی ز نور دوست ببین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر تو بی مغز نام دوست بری</p></div>
<div class="m2"><p>باشی از عشق روی دوست بری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که از دوست دوست می‌خواهد</p></div>
<div class="m2"><p>جوهرش را عرض نمی‌کاهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگرت هست قوت مردان</p></div>
<div class="m2"><p>اینک اسب و سلاح و این میدان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هست آرام جان من مهرش</p></div>
<div class="m2"><p>هست سود و زیان من مهرش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دلم از حسن او لقا خواهد</p></div>
<div class="m2"><p>دیده‌ام دید، دل چرا خواهد؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پای دل را به دام او بستم</p></div>
<div class="m2"><p>وز می اشتیاق او مستم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فارغ است او ز ما و ما جویان</p></div>
<div class="m2"><p>ز اشتیاق رخش غزل گویان:</p></div></div>