---
title: >-
    لمعۀ سیزدهم
---
# لمعۀ سیزدهم

<div class="n" id="bn1"><p>محبوب هفتاد هزار حجاب از نور و ظلمت بهر آن بر روی فرو گذاشت تا محب خوی فرا کند و او را پس پرده بیند، تا چون دیده آشنا شودو عشق سلسلۀ شوق بجنباند، بمدد عشق و قوت معشوق پرده‌هایکان یکان فرو گشاید، پرتو سبحات جلال غیریت وهم را بسوزد و او بجای او بنشیند و همگی عاشق شود، چنانکه:</p></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه گیرد از او بدو گیرد</p></div>
<div class="m2"><p>هرچه بخشد از او بدو بخشد</p></div></div>
<div class="n" id="bn3"><p>اشارت مصطفی صلوات الله علیه در حدیث که:‌ صلوة بسواک خیر من سبعین صلوة بغیر سواک نخستین چیزی تواند بود، یعنی یک نماز تو بی تو، به از هفتاد نماز تو با تو، زیرا که تا تو با توئی،‌این هفتادهزار حجاب مسدول بود، و چون تو بی تو باشی هفتاد هزار حجاب کرا محجوب گرداند؟ و همچنین سر: فان لم تکن تراه، فانه یراک، چنان تواند بود که اگر تو نباشی به حقیقت ببینی.</p></div>
<div class="n" id="bn4"><p>گفته‌اند: این حجب صفات آدمی است نورانی، چنانکه علم و یقین و احوال و مقامات و جمله اخلاق حمیده، و ظلمانی، چنان که جهل و گمان و رسوم و جملۀ اخلاق ذمیمه.</p></div>
<div class="c"><p>بیت</p></div>
<div class="b" id="bn5"><div class="m1"><p>پرده‌های نور و ظلمت را ز عجز</p></div>
<div class="m2"><p>در گمان و در یقین دانسته‌اند</p></div></div>
<div class="n" id="bn6"><p>لیکن اینجا حرفی است: اگر چنانکه حجب این صفات نبودی سوخته گشتی: زیرا که:‌لو کشفهالاحترقت سبحات وجهه ماانتهی الیه بصره من خلقه. هاء- بصره- عاید با خلق تواند بود. یعنی اگر خلق و اوصاف خلق ادراک سبحان کندی سوخته شدی و می‌بینیم که با رؤیت نمی‌سوزد و حجب دایم مسدول می‌بینیم. پس این حجب،‌اسماء و صفات او تواند بود، حجب نوری: چنانکه ظهور و لطف و جمال،‌و ظلمانی: چنانکه بطون و قهر و جلال. نشاید که این حجب مرتفع شود، که اگر احدیت ذات از پردۀ صفات عزت بتابد، اشیاء بکلی متلاشی شود، چه اتصاف اشیاء بوجود بواسطۀ اسماء و صفات تواند بود، هرچند وجود اشیاء بتجلی ذات باشد، اما تجلی ذات پس پردۀ اسماء و صفات اثر کند. پس حجب او اسماء و صفات او آمده، چنانکه صاحب قوت القلوب فرمود:‌حجب الذات بالصفات، و حجب الصفات بالافعال. و اگر به حقیقت نظر کنی حجاب او همو تواند بود، بشدت ظهور محتجب است و بسطوت نورمستتر.</p></div>
<div class="c"><p>شعر</p></div>
<div class="b" id="bn7"><div class="m1"><p>لقد بطنت فلم تظهر لذیبصر</p></div>
<div class="m2"><p>فکیفیدرکمن بالعین مستتر</p></div></div>
<div class="n" id="bn8"><p>می‌بینم و نمی‌دانم که چه می‌بینم، لاجرم می‌گویم:</p></div>
<div class="c"><p>قطعه</p></div>
<div class="b" id="bn9"><div class="m1"><p>حجاب روی تو هم روی تو است در همه حال</p></div>
<div class="m2"><p>نهانی از همه عالم، ز بس که پیدائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر که می‌نگرم صورت تو می‌بینم</p></div>
<div class="m2"><p>از این بیان همه در چشم من تو می‌آئی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زر شک تا نشناسد کسی ترا هر دم</p></div>
<div class="m2"><p>جمال خود به لباسی دگر بیارائی</p></div></div>
<div class="n" id="bn12"><p>نشاید که او را غیری حجاب آید، چه حجاب محدود را باشد او را حد نیست؛ هرچه بینی در عالم از صورت و معنی او بود و او بهیچ صورتی مقید نه، در هر چه او نباشد آن چیز نباشد، و در هرچه او باشد آن چیز هم نباشد.</p></div>
<div class="b" id="bn13"><div class="m1"><p>تو جهانی لیک چون آئی پدید؟</p></div>
<div class="m2"><p>جمله جانی لیک چون گردی نهان؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون شوی پیدا که پنهانی مدام</p></div>
<div class="m2"><p>چون نهان گردی که جاویدی عیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم عیانی هم نهان، هم هر دوئی</p></div>
<div class="m2"><p>هم نه اینی هم نه آن، هم این و آن</p></div></div>