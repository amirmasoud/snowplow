---
title: >-
    شمارهٔ ۴ (در میکده می‌کشم سبویی - باشد که بیابم از تو بویی)
---
# شمارهٔ ۴ (در میکده می‌کشم سبویی - باشد که بیابم از تو بویی)

<div class="b" id="bn1"><div class="m1"><p>در میکده با حریف قلاش</p></div>
<div class="m2"><p>بنشین و شراب نوش و خوش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خط خوش نگار بر خوان</p></div>
<div class="m2"><p>سر دو جهان، ولی مکن فاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نقش و نگار فتنه گشتم</p></div>
<div class="m2"><p>زان رو که نمی‌رسم به نقاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا با خودم، از خودم خبر نیست</p></div>
<div class="m2"><p>با خود نفسی نبودمی کاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخمور میم، بیار ساقی</p></div>
<div class="m2"><p>نقل و می از آن لب شکر پاش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صومعه‌ها چو می‌نگنجد</p></div>
<div class="m2"><p>دردی کش و می‌پرست و قلاش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نیز به ترک زهد گفتم</p></div>
<div class="m2"><p>اینک شب و روز همچو اوباش</p></div></div>
<div class="b2" id="bn8"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn9"><div class="m1"><p>ای روی تو شمع مجلس افروز</p></div>
<div class="m2"><p>سودای تو آتش جگرسوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخسار خوش تو عاشقان را</p></div>
<div class="m2"><p>خوشتر ز هزار عید نوروز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگشای لبت به خنده، بنمای</p></div>
<div class="m2"><p>از لعل، تو گوهر شب افروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زنهار! از آن دو چشم مستت</p></div>
<div class="m2"><p>فریاد! از آن دو زلف کین توز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون زلف، تو کج مباز با ما</p></div>
<div class="m2"><p>از قد تو راستی بیاموز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ساقی بده، آن می طرب را</p></div>
<div class="m2"><p>بستان ز من این دل غم اندوز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن رفت که رفتمی به مسجد</p></div>
<div class="m2"><p>اکنون چو قلندران شب و روز</p></div></div>
<div class="b2" id="bn16"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn17"><div class="m1"><p>ای مطرب عشق، ساز بنواز</p></div>
<div class="m2"><p>کان یار نشد هنوز دمساز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دشنام دهد به جای بوسه</p></div>
<div class="m2"><p>و آن نیز به صد کرشمه و ناز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پنهان چه زنم نوای عشقش؟</p></div>
<div class="m2"><p>کز پرده برون فتاده این راز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در پاش کسی که سر نیفکند</p></div>
<div class="m2"><p>چون طرهٔ او نشد سرافراز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در بند خودم، بیار ساقی</p></div>
<div class="m2"><p>آن می که رهاندم ز خود باز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عمری است کز آروزی آن می</p></div>
<div class="m2"><p>چون جام بمانده‌ام دهن باز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتی که: بجوی تا بیابی</p></div>
<div class="m2"><p>اینک طلب تو کردم آغاز</p></div></div>
<div class="b2" id="bn24"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn25"><div class="m1"><p>ساقی، بده آب زندگانی</p></div>
<div class="m2"><p>اکسیر حیات جاودانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>می ده، که نمی‌شود میسر</p></div>
<div class="m2"><p>بی‌آب حیات زندگانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هم خضر خجل، هم آب حیوان</p></div>
<div class="m2"><p>چون از خط و لب شکرفشانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گوشم چو صدف شود گهر چین</p></div>
<div class="m2"><p>زان دم که ز لعل در چکانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شمشیر مکش به کشتن ما</p></div>
<div class="m2"><p>کز ناز و کرشمه در نمانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر لحظه کرشمه‌ای دگر کن</p></div>
<div class="m2"><p>بفریب مرا، چنان که دانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در آرزوی لب تو بودم</p></div>
<div class="m2"><p>چون دست نداد کامرانی</p></div></div>
<div class="b2" id="bn32"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn33"><div class="m1"><p>وقت طرب است، ساقیا، خیز</p></div>
<div class="m2"><p>در ده قدح نشاط انگیز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از جور تو رستخیز برخاست</p></div>
<div class="m2"><p>بنشان شر و شور و فتنه، برخیز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بستان دل عاشقان شیدا</p></div>
<div class="m2"><p>وز طرهٔ دلربا درآویز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خون دل ما بریز و آنگاه</p></div>
<div class="m2"><p>با خاک درت بهم برآمیز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وآن خنجر غمزهٔ دلاور</p></div>
<div class="m2"><p>هر لحظه به خون ما بکن تیز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کردم هوس لبت، ندیدم</p></div>
<div class="m2"><p>کامی چو از آن لب شکرریز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نذری کردم که: تا توانم</p></div>
<div class="m2"><p>توبه کنم از صلاح و پرهیز</p></div></div>
<div class="b2" id="bn40"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn41"><div class="m1"><p>ساقی، چه کنم به ساغر و جام؟</p></div>
<div class="m2"><p>مستم کن از می غم انجام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با یاد لب تو عاشقان را</p></div>
<div class="m2"><p>حاجت نبود به ساغر و جام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گوشم سخن لب تو بشنود</p></div>
<div class="m2"><p>خشنود شد، از لبت، به دشنام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل زلف تو دانه دید، ناگاه</p></div>
<div class="m2"><p>افتاد به بوی دانه در دام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سودای دو زلف بیقرارت</p></div>
<div class="m2"><p>برد از دل من قرار و آرام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>باشد که رسم به کام روزی</p></div>
<div class="m2"><p>در راه امید می‌زنم گام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ور زانکه نشد لب تو روزی</p></div>
<div class="m2"><p>دانی چه کنم به کام و ناکام؟</p></div></div>
<div class="b2" id="bn48"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn49"><div class="m1"><p>دست از دل بیقرار شستم</p></div>
<div class="m2"><p>وندر سر زلف یار بستم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بی‌دل شدم وز جان به یکبار</p></div>
<div class="m2"><p>چون طرهٔ یار برشکستم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گویند چگونه‌ای؟ چه گویم؟</p></div>
<div class="m2"><p>هستم ز غمش چنان که هستم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خود را ز چه غمش برآرم</p></div>
<div class="m2"><p>گر طرهٔ او فتد به دستم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در دام بلا فتاده بودم</p></div>
<div class="m2"><p>هم طرهٔ او گرفت دستم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ساقی، قدحی، که از می عشق</p></div>
<div class="m2"><p>چون چشم خوش تو نیم مستم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شد نوبت خویشتن پرستی</p></div>
<div class="m2"><p>آمد گه آنکه می‌پرستم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فارغ شوم از غم عراقی</p></div>
<div class="m2"><p>از زحمت او چو باز رستم</p></div></div>
<div class="b2" id="bn57"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn58"><div class="m1"><p>ساقی، می مهر ریز در کام</p></div>
<div class="m2"><p>بنما به شب آفتاب از جام</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آن جام جهان‌نما به من ده</p></div>
<div class="m2"><p>تا بنگرم اندرو سرانجام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بینم مگر آفتاب رویت</p></div>
<div class="m2"><p>تابان سحری ز مشرق جام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جان پیش رخ تو برفشانم</p></div>
<div class="m2"><p>گر بنگرم آن رخ غم انجام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خود ذره چو آفتاب بیند</p></div>
<div class="m2"><p>در سایه دلش نگیرد آرام</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در بند خودم، نمی‌توانم</p></div>
<div class="m2"><p>کازاد شوم ز بند ایام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کو دانهٔ می؟ که مرغ جانم</p></div>
<div class="m2"><p>یک بار خلاص یابد از دام</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کی باز رهم ز بیم و امید؟</p></div>
<div class="m2"><p>کی پاک شوم ز ننگ و از نام؟</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کی خانهٔ من خراب گردد؟</p></div>
<div class="m2"><p>تا مهر درآید از در و بام</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در صومعه مدتی نشستم</p></div>
<div class="m2"><p>بر بوی تو، چون نیافتم کام</p></div></div>
<div class="b2" id="bn68"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn69"><div class="m1"><p>ساقی بنما رخ نکویت</p></div>
<div class="m2"><p>تا جام طرب کشم به بویت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ناخورده شراب مست گردد</p></div>
<div class="m2"><p>نظارگی از رخ نکویت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر صاف نمی‌دهی، که خاکم</p></div>
<div class="m2"><p>یاد آر به دردی سبویت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مگذار ز تشنگی بمیرم</p></div>
<div class="m2"><p>نایافته قطره‌ای ز جویت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>آیا بود آنکه چشم تشنه</p></div>
<div class="m2"><p>سیراب شود ز آب رویت؟</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یا هیچ بود که ناتوانی</p></div>
<div class="m2"><p>یابد سحری نسیم کویت؟</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>از توبه و زهد توبه کردم</p></div>
<div class="m2"><p>تا بو که رسم دمی به سویت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دل جست و تو را نیافت، افسوس</p></div>
<div class="m2"><p>واماند کنون ز جست و جویت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>خوی تو نکوست با همه کس</p></div>
<div class="m2"><p>با من ز چه بدفتاد خویت؟</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>می‌گریم روز در فراقت</p></div>
<div class="m2"><p>می‌نالم شب در آرزویت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بر بوی تو روزگار بگذشت</p></div>
<div class="m2"><p>از بخت نیافتم چو بویت</p></div></div>
<div class="b2" id="bn80"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn81"><div class="m1"><p>ساقی، بده آب زندگانی</p></div>
<div class="m2"><p>پیش آر حیات جاودانی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>می ده، که کسی نیافت هرگز</p></div>
<div class="m2"><p>بی آب حیات زندگانی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>در مجلس عشق مفلسی را</p></div>
<div class="m2"><p>پر کن دو سه رطل رایگانی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>شاید که دهی به دوستداری</p></div>
<div class="m2"><p>آن ساغر مهر دوستگانی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>برخیزم و ترک خویش گیرم</p></div>
<div class="m2"><p>گر هیچ تو با خودم نشانی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ور از من غمت درآید</p></div>
<div class="m2"><p>جان پیش کشم ز شادمانی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>جان را ز دو دیده دوست دارم</p></div>
<div class="m2"><p>زان رو که تو در میان آنی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>از عاشق خود کران چه گیری؟</p></div>
<div class="m2"><p>چون با دل و جانش درمیانی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>از بهر رخ تو می‌کند چشم</p></div>
<div class="m2"><p>از دیده همیشه دیده‌بانی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>در آرزوی رخ تو بودم</p></div>
<div class="m2"><p>عمری چو نیافتم امانی</p></div></div>
<div class="b2" id="bn91"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn92"><div class="m1"><p>ساقی، ز شراب‌خانهٔ نوش</p></div>
<div class="m2"><p>یک جام بیاور و ببر هوش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>مستم کن، آنچنان که در حال</p></div>
<div class="m2"><p>از هستی خود کنم فراموش</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ور خود سوی من کنی نگاهی</p></div>
<div class="m2"><p>بی‌باده شوم خراب و مدهوش</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>سرمست شوم چو چشم ساقی</p></div>
<div class="m2"><p>گر هیچ بیابم از لبت نوش</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کی بود که ز لطف دلنوازت</p></div>
<div class="m2"><p>گیرم همه کام دل در آغوش؟</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>دارد چو به لطف دلبرم چشم</p></div>
<div class="m2"><p>می‌دار تو هم به حال او گوش</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مگذار برهنه‌ام ز لطفت</p></div>
<div class="m2"><p>در من تو ز مهر جامه‌ای پوش</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چون نیست مرا کسی خریدار</p></div>
<div class="m2"><p>مولای توام، تو نیز مفروش</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دیگ دل من، که نیز خام است</p></div>
<div class="m2"><p>بر آتش شوق سر زند جوش</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>در صومعه حشمتت ندیدم</p></div>
<div class="m2"><p>اکنون شب و روز بر سر دوش</p></div></div>
<div class="b2" id="bn102"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn103"><div class="m1"><p>ساقی، بده آب آتش افروز</p></div>
<div class="m2"><p>چون سوختیم تمام تر سوز</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>این آتش من به آب بنشان</p></div>
<div class="m2"><p>وز آب من آتشی برافروز</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>می ده، که ز بادهٔ شبانه</p></div>
<div class="m2"><p>در سر بودم خمار امروز</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>در ساغر دل شراب افکن</p></div>
<div class="m2"><p>کز پرتو آن شود شبم روز</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>گفتی که: بنال زار هر شب</p></div>
<div class="m2"><p>ماتم زده را تو نوحه ماموز</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چون با من خسته می‌نسازی</p></div>
<div class="m2"><p>چه سود ز نالهٔ من و سوز؟</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>دل را ز تو تا شکیب افتاد</p></div>
<div class="m2"><p>بر لشکر غم نگشت پیروز</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بخشای برین دل جگرخوار</p></div>
<div class="m2"><p>رحم آر بدین تن غم اندوز</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>من می‌شکنم، تو باز می‌بند</p></div>
<div class="m2"><p>من می‌درم، از کرم تو می‌دوز</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>از توبه و زهد توبه کردم</p></div>
<div class="m2"><p>اینک چو قلندران شب و روز</p></div></div>
<div class="b2" id="bn113"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn114"><div class="m1"><p>ساقی، سر درد سر ندارم</p></div>
<div class="m2"><p>بشکن به نسیم می خمارم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>یک جرعه ز جام می به من ده</p></div>
<div class="m2"><p>تا درد کشم، که خاکسارم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>از جام تو قانعم به دردی</p></div>
<div class="m2"><p>حاشا که به جرعه سر درآرم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>یادآر مرا به دردی خم</p></div>
<div class="m2"><p>کز خاک در تو یادگارم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بگذار که بر درت نشینم</p></div>
<div class="m2"><p>آخر نه ز کوی تو غبارم؟</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>از دست مده، که رفتم از دست</p></div>
<div class="m2"><p>دستیم بده، که دوستدارم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>زنده نفسی برای آنم</p></div>
<div class="m2"><p>تا پیش رخ تو جان سپارم</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>این یک نفسم تو نیز خوش دار</p></div>
<div class="m2"><p>چون با نفسی فتاد کارم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>نایافته بوی گلشن وصل</p></div>
<div class="m2"><p>در سینه شکست هجر خارم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>در سر دارم که بعد از امروز</p></div>
<div class="m2"><p>دست از همه کارها بدارم</p></div></div>
<div class="b2" id="bn124"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn125"><div class="m1"><p>ساقی، دو سه دم که هست باقی</p></div>
<div class="m2"><p>در ده مدد حیات باقی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>قد فاتنی الصبوح فادرک</p></div>
<div class="m2"><p>من قبل فوات الاعتباق</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>در کیسهٔ نقد نیست جز جان</p></div>
<div class="m2"><p>بستان قدحی، بیار ساقی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>کم اصبر قد صبرت حتی</p></div>
<div class="m2"><p>روحی بلغت الی التراق</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>دردا! که به خیره عمر بگذشت</p></div>
<div class="m2"><p>نابوده میان ما تلاقی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>فاستعذب مسمعی حدیثا</p></div>
<div class="m2"><p>مذتاب بذکر کم مذاق</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>من زان توام، تو هم مرا باش</p></div>
<div class="m2"><p>خوش باش به عشق اتفاقی</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>اشتاق الی لقاک، فانظر</p></div>
<div class="m2"><p>لی وجهک نظرةالا لاق</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بگذار که بر در تو باشد</p></div>
<div class="m2"><p>کمتر سگک درت عراقی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>استوطن بابکم عسی ان</p></div>
<div class="m2"><p>یحطی نظرا بکم حداق</p></div></div>
<div class="b2" id="bn135"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>
<div class="b" id="bn136"><div class="m1"><p>ساقی، قدحی، که نیم مستیم</p></div>
<div class="m2"><p>مخمور صبوحی الستیم</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>از صومعه پا برون نهادیم</p></div>
<div class="m2"><p>در میکده معتکف نشستیم</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>از جور تو خرقه‌ها دریدیم</p></div>
<div class="m2"><p>وز دست تو توبه‌ها شکستیم</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>جز جان گروی دگر نداریم</p></div>
<div class="m2"><p>بپذیر، که نیک تنگ دستیم</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ما را برهان ز ما، که تا ما</p></div>
<div class="m2"><p>با خویشتنیم بت پرستیم</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>ما هرچه که داشتیم پیوند</p></div>
<div class="m2"><p>از بهر تو آن همه گسستیم</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>بر درگه لطف تو فتادیم</p></div>
<div class="m2"><p>در رحمت تو امید بستیم</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>گر نیک و بدیم، ور بد و نیک</p></div>
<div class="m2"><p>هم آن توایم، هر چه هستیم</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>در ده قدحی، که از عراقی</p></div>
<div class="m2"><p>الا به شراب وا نرستیم</p></div></div>
<div class="b2" id="bn145"><p>در میکده می‌کشم سبویی</p>
<p>باشد که بیابم از تو بویی</p></div>