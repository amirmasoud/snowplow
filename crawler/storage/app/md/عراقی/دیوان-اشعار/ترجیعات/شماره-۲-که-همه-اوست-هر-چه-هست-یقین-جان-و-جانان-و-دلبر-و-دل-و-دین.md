---
title: >-
    شمارهٔ ۲ (که همه اوست هر چه هست یقین - جان و جانان و دلبر و دل و دین)
---
# شمارهٔ ۲ (که همه اوست هر چه هست یقین - جان و جانان و دلبر و دل و دین)

<div class="b" id="bn1"><div class="m1"><p>طاب روح‌النسیم بالاسحار</p></div>
<div class="m2"><p>این دورالندیم بالانوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خماریم، کو لب ساقی؟</p></div>
<div class="m2"><p>نیم مستیم کو کرشمهٔ یار؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طره‌ای کو؟ که دل درو بندیم</p></div>
<div class="m2"><p>چهره‌ای کو؟ که جان کنیم نثار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیز، کز لعل یار نوشین لب</p></div>
<div class="m2"><p>به کف آریم جان نوش گوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که جزین باده بار نرهاند</p></div>
<div class="m2"><p>نیم مستان عشق را ز خمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سر زلف یار دل بندیم</p></div>
<div class="m2"><p>تا به روز آید آخر این شب تار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آفتابی که کون ذرهٔ اوست</p></div>
<div class="m2"><p>بر فروزیم ذره‌وار عذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون که همرنگ آفتاب شویم</p></div>
<div class="m2"><p>شاید آن لحظه گر کنیم اقرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاشکار و نهان همه ماییم</p></div>
<div class="m2"><p>«لیس فی‌الدار غیرنا دیار»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور نشد این سخن تو را روشن</p></div>
<div class="m2"><p>جام گیتی‌نمای را به کف آر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ببینی درو، که جمله یکی است</p></div>
<div class="m2"><p>خواه یکصد شمار و خواه هزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر پراگنده‌ای، که جمع شود</p></div>
<div class="m2"><p>بر زبانش چنین رود گفتار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر عراقی زبان فرو بستی</p></div>
<div class="m2"><p>آشکارا نگشتی این اسرار</p></div></div>
<div class="b2" id="bn14"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn15"><div class="m1"><p>اکئوس تلاء لات بمدام</p></div>
<div class="m2"><p>ام شموس تهللت بغمام؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از صفای می و لطافت جام</p></div>
<div class="m2"><p>در هم آمیخت رنگ جام و مدام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه جام است و نیست گویی می</p></div>
<div class="m2"><p>یا مدام است و نیست گویی جام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون هوا رنگ آفتاب گرفت</p></div>
<div class="m2"><p>هر دو یکسان شدند نور و ظلام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز و شب با هم آشتی کردند</p></div>
<div class="m2"><p>کار عالم از آن گرفت نظام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر ندانی که این چه روز و شب است؟</p></div>
<div class="m2"><p>یا کدام است جام و باده کدام؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سریان حیات در عالم</p></div>
<div class="m2"><p>چون می و جام فهم کن تو مدام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>انکشاف حجاب علم یقین</p></div>
<div class="m2"><p>چون شب و روز فرض کن، وسلام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور نشد این بیان تو را روشن</p></div>
<div class="m2"><p>جمله ز آغاز کار تا انجام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جام گیتی‌نمای را به کف آر</p></div>
<div class="m2"><p>تا ببینی به چشم دوست مدام</p></div></div>
<div class="b2" id="bn25"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn26"><div class="m1"><p>آفتاب رخ تو پیدا شد</p></div>
<div class="m2"><p>عالم اندر تفش هویدا شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وام کرد از جمال تو نظری</p></div>
<div class="m2"><p>حسن رویت بدید و شیدا شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عاریت بستد از لبت شکری</p></div>
<div class="m2"><p>ذوق آن چون بیافت گویا شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شبنمی بر زمین چکید سحر</p></div>
<div class="m2"><p>روی خورشید دید و دروا شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر هوا شد بخاری از دریا</p></div>
<div class="m2"><p>باز چون جمع گشت دریا شد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غیرتش غیر در جهان نگذاشت</p></div>
<div class="m2"><p>لاجرم عین جمله اشیا شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نسبت اقتدار و فعل به ما</p></div>
<div class="m2"><p>هم از آن روی بود کو ما شد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جام گیتی‌نمای او ماییم</p></div>
<div class="m2"><p>که به ما هرچه بود پیدا شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا به اکنون مرا نبود خبر</p></div>
<div class="m2"><p>بر من امروز آشکارا شد</p></div></div>
<div class="b2" id="bn35"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn36"><div class="m1"><p>ما چنین تشنه و زلال وصال</p></div>
<div class="m2"><p>همه عالم گرفته مالامال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>غرق آبیم و آب می‌جوییم</p></div>
<div class="m2"><p>در وصالیم و بی‌خبر ز وصال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آفتاب اندرون خانه و ما</p></div>
<div class="m2"><p>در بدر می‌رویم، ذره مثال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گنج در آستین و می‌گردیم</p></div>
<div class="m2"><p>گرد هر کوی بهر یک مثقال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چند گردیم خیره گرد جهان؟</p></div>
<div class="m2"><p>چند باشیم اسیر ظن و خیال؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در ده، ای ساقی، از لبت جامی</p></div>
<div class="m2"><p>کز نهاد خودم گرفت ملال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آفتابی ز روی خود بنمای</p></div>
<div class="m2"><p>تا چو سایه رخ آورم به زوال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا ابد با ازل قرین گردد</p></div>
<div class="m2"><p>دی و فردای ما شود همه حال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در چنین حال شاید ار گویم</p></div>
<div class="m2"><p>گر چه باشد به نزد عقل محال</p></div></div>
<div class="b2" id="bn45"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn46"><div class="m1"><p>ای به تو روز و شب جهان روشن</p></div>
<div class="m2"><p>بی‌رخت چشم عاشقان روشن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به حدیث تو کام دل شیرین</p></div>
<div class="m2"><p>به جمال تو چشم جان روشن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شد به نور جمال روشن تو</p></div>
<div class="m2"><p>عالم تیره ناگهان روشن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آفتاب رخ جهانگیرت</p></div>
<div class="m2"><p>می‌کند دم به دم جهان روشن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز ابتدا عالم از تو روشن شد</p></div>
<div class="m2"><p>کز یقین می‌شود گمان روشن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>می‌نماید ز روی هر ذره</p></div>
<div class="m2"><p>آفتاب رخت عیان روشن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کی توان کرد در خم زلفت</p></div>
<div class="m2"><p>خویشتن را ز خود نهان روشن؟</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ای دل تیره، گر نگشت تو را</p></div>
<div class="m2"><p>سر توحید این بیان روشن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اندر آیینهٔ جهان بنگر</p></div>
<div class="m2"><p>تا ببینی همان زمان روشن</p></div></div>
<div class="b2" id="bn55"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn56"><div class="m1"><p>مطرب عشق می‌نوازد ساز</p></div>
<div class="m2"><p>عاشقی کو؟ که بشنود آواز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر نفس پرده‌ای دگر ساز</p></div>
<div class="m2"><p>هر زمان زخمه‌ای کند آغاز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه عالم صدای نغمه اوست</p></div>
<div class="m2"><p>که شنید این چنین صدای دراز؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>راز او از جهان برون افتاد</p></div>
<div class="m2"><p>خود صدا کی نگاه دارد راز؟</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سر او از زبان هر ذره</p></div>
<div class="m2"><p>هم تو بشنو، که من نیم غماز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چه حدیث است در جهان؟ که شنید</p></div>
<div class="m2"><p>سخن سرش از سخن پرداز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خود سخن گفت و خود شنید از خود</p></div>
<div class="m2"><p>کردم اینک سخن برت ایجاز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>عشق مشاطه‌ای است رنگ آمیز</p></div>
<div class="m2"><p>که حقیقت کند به رنگ مجاز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا به دام آورد دل محمود</p></div>
<div class="m2"><p>بترازد به شانه زلف ایاز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نه به اندازهٔ تو هست سخن</p></div>
<div class="m2"><p>عشق می‌گوید این سخن را باز</p></div></div>
<div class="b2" id="bn66"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn67"><div class="m1"><p>عشق ناگاه برکشید علم</p></div>
<div class="m2"><p>تا بهم بر زند وجود و عدم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بی‌قراری عشق شورانگیز</p></div>
<div class="m2"><p>شر و شوری فکند در عالم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در هر آیینه حسن دیگرگون</p></div>
<div class="m2"><p>می‌نماید جمال او هردم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گه برآید به کسوت حوا</p></div>
<div class="m2"><p>گه برآید به صورت آدم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گاه خرم کند دل غمگین</p></div>
<div class="m2"><p>گاه غمگین کند دل خرم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گر کند عالمی خراب چه باک؟</p></div>
<div class="m2"><p>مهر را از هلاک یک شبنم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>می‌نماید که هست و نیست جهان</p></div>
<div class="m2"><p>جز خطی در میان نور و ظلم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گر بخوانی تو این خط موهوم</p></div>
<div class="m2"><p>بشناسی حدوث را ز قدم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>معنی حرف کون ظاهر کن</p></div>
<div class="m2"><p>تا بدانی بقدر خویش تو هم</p></div></div>
<div class="b2" id="bn76"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn77"><div class="m1"><p>ای رخت آفتاب عالمتاب</p></div>
<div class="m2"><p>در فضای تو کاینات سراب</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>در نیاید به چشم تو دو جهان</p></div>
<div class="m2"><p>کی به چشم تو اندر آید خواب؟</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>پیش ازین بی‌رخت چه بود جهان؟</p></div>
<div class="m2"><p>سایه‌ای در عدم سرای خراب</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز استوا مهر طلعت تو بتافت</p></div>
<div class="m2"><p>سایه از نور مهر یافت خضاب</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مهر چون سایه از میان برداشت</p></div>
<div class="m2"><p>ما چه باشیم در میان؟ دریاب</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>اول و آخر اوست در همه حال</p></div>
<div class="m2"><p>ظاهر و باطن اوست در همه باب</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>گر صد است، ار هزار، جمله یکی است</p></div>
<div class="m2"><p>در نیاید به جز یکی به حساب</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>برف خوانند آب را، چو ببست</p></div>
<div class="m2"><p>باز چون حل شود چه گویند آب؟</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>آب چون رنگ و بوی گل گیرد</p></div>
<div class="m2"><p>لاجرم نام او کنند گلاب</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بر زبان فصیح هر ذره</p></div>
<div class="m2"><p>می‌کند عشق لحظه لحظه خطاب</p></div></div>
<div class="b2" id="bn87"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn88"><div class="m1"><p>روی جانان به چشم جان دیدن</p></div>
<div class="m2"><p>خوش بود، خاصه رایگان دیدن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خوش بود در صفای رخسارش</p></div>
<div class="m2"><p>آشکارا همه نهان دیدن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جز در آیینهٔ رخش نتوان</p></div>
<div class="m2"><p>عکس رخسار او عیان دیدن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بوی او را بدو توان دریافت</p></div>
<div class="m2"><p>روی او را بدو توان دیدن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دیدن روی دوست خوش باشد</p></div>
<div class="m2"><p>خاصه رخساره‌ای چنان دیدن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خود گرفتم که در صفای رخش</p></div>
<div class="m2"><p>نتوانی همه نهان دیدن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>می‌توان آنچه هست و بود و بود</p></div>
<div class="m2"><p>در رخ او یکان یکان دیدن</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>در خم زلف او، چه خوش باشد</p></div>
<div class="m2"><p>دل گم گشته ناگهان دیدن!</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>اندر آیینهٔ جهان باری</p></div>
<div class="m2"><p>می‌توانی به چشم جان دیدن</p></div></div>
<div class="b2" id="bn97"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn98"><div class="m1"><p>یارب، آن لعل شکرین چه خوش است؟</p></div>
<div class="m2"><p>یارب، آن روی نازنین چه خوش است؟</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>با لبش ذوق هم نفس چه نکوست؟</p></div>
<div class="m2"><p>با رخش حسن هم قرین چه خوش است ؟</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>از خط عنبرین او خواندن</p></div>
<div class="m2"><p>سخن لعل شکرین چه خوش است؟</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ور ز من باورت نمی‌افتد</p></div>
<div class="m2"><p>بوسه زن بر لبش، ببین چه خوش است؟</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مهر جانان به چشم جان بنگر</p></div>
<div class="m2"><p>در میان گمان یقین چه خوش است؟</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>من ز خود گشته غایب ، او حاضر</p></div>
<div class="m2"><p>عشق با یار هم چنین چه خوش است ؟</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>آنکه اندر جهان نمی گنجد</p></div>
<div class="m2"><p>در میان دل حزین چه خوش است ؟</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>تا فشاند بر آستان درش</p></div>
<div class="m2"><p>عاشقی جان در آستین چه خوش است ؟</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>در جهان غیر او نمی‌بینم</p></div>
<div class="m2"><p>دلم امروز هم برین چه خوش است؟</p></div></div>
<div class="b2" id="bn107"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>
<div class="b" id="bn108"><div class="m1"><p>بی‌دلی را، که عشق بنوازد</p></div>
<div class="m2"><p>جان او جلوه‌گاه خود سازد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>دل او را ز غم به جان آرد</p></div>
<div class="m2"><p>تن او را ز غصه بگدازد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>به خودش آنچنان کند مشغول</p></div>
<div class="m2"><p>که به معشوق هم نپردازد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چون کند خانه خالی از اغیار</p></div>
<div class="m2"><p>آن گهی عشق با خود آغازد</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>زلف خود را به رخ بیاراید</p></div>
<div class="m2"><p>روی خود را به حسن بترازد</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بر لب خویش بوس‌ها شمرد</p></div>
<div class="m2"><p>با رخ خویش عشق‌ها بازد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چون درون را همه فرو گیرد</p></div>
<div class="m2"><p>ناگهی از درون برون تازد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>با عراقی کرشمه‌ای بکند</p></div>
<div class="m2"><p>دل او را به لطف بنوازد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>تا به مستی ز خویشتن برود</p></div>
<div class="m2"><p>به جهان این سخن دراندازد</p></div></div>
<div class="b2" id="bn117"><p>که همه اوست هر چه هست یقین</p>
<p>جان و جانان و دلبر و دل و دین</p></div>