---
title: >-
    شمارهٔ ۱ (که به غیر از تو در جهان کس نیست - جز تو موجود جاودان کس نیست)
---
# شمارهٔ ۱ (که به غیر از تو در جهان کس نیست - جز تو موجود جاودان کس نیست)

<div class="b" id="bn1"><div class="m1"><p>ای زده خیمهٔ حدوث و قدم</p></div>
<div class="m2"><p>در سراپردهٔ وجود و عدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز تو کس واقف وجود تو نیست</p></div>
<div class="m2"><p>هم تویی راز خویش را محرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو غایب نبوده‌ام یک روز</p></div>
<div class="m2"><p>وز تو خالی نبوده‌ام یک دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن گروهی که از تو باخبرند</p></div>
<div class="m2"><p>بر دو عالم کشیده‌اند رقم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش دریای کبریای تو هست</p></div>
<div class="m2"><p>دو جهان کم ز قطره‌ای شبنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌وجودت جهان وجود نداشت</p></div>
<div class="m2"><p>از جمال تو شد جهان خرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تجلی است در همه کسوت</p></div>
<div class="m2"><p>آشکار است در همه عالم</p></div></div>
<div class="b2" id="bn8"><p>که به غیر از تو در جهان کس نیست</p>
<p>جز تو موجود جاودان کس نیست</p></div>
<div class="b" id="bn9"><div class="m1"><p>تا مرا از تو داده‌اند خبر</p></div>
<div class="m2"><p>از خودم نیست آگهی دیگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر به دیوانگی بر آوردم</p></div>
<div class="m2"><p>تا نهادم به کوی عشق تو سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ز خاک در تو دور شدم</p></div>
<div class="m2"><p>غرقه گشتم میان خون جگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک پای تو می‌کشم در چشم</p></div>
<div class="m2"><p>درس عشق تو می‌کنم از بر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز تو کس نیست در سرای وجود</p></div>
<div class="m2"><p>نظر این است پیش اهل نظر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاه واحد، گهی کثیر شوی</p></div>
<div class="m2"><p>این سخن عقل چون کند باور؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش ارباب صورت و معنی</p></div>
<div class="m2"><p>هست از آفتاب روشن‌تر</p></div></div>
<div class="b2" id="bn16"><p>که به غیر از تو در جهان کس نیست</p>
<p>جز تو موجود جاودان کس نیست</p></div>
<div class="b" id="bn17"><div class="m1"><p>گر شبی دامنت به دست آرم</p></div>
<div class="m2"><p>تا قیامت ز دست نگذارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرد کویت به فرق می‌گردم</p></div>
<div class="m2"><p>بیش ازین نیست در جهان کارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر مرا از سگان خود شمری</p></div>
<div class="m2"><p>هر دو عالم به هیچ نشمارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون خیالی شدم ز تنهایی</p></div>
<div class="m2"><p>تا خیال تو در نظر دارم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کار من جز نشاط و شادی نیست</p></div>
<div class="m2"><p>تا به دام غمت گرفتارم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون به جز تو کسی نمی‌بینم</p></div>
<div class="m2"><p>غیر ازین بر زبان نمی‌آرم</p></div></div>
<div class="b2" id="bn23"><p>که به غیر از تو در جهان کس نیست</p>
<p>جز تو موجود جاودان کس نیست</p></div>
<div class="b" id="bn24"><div class="m1"><p>همه عالم چو عکس صورت اوست</p></div>
<div class="m2"><p>بجز از او کسی ندارد دوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به مجاز این و آن نهی نامش</p></div>
<div class="m2"><p>به حقیقت چو بنگری همه اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد سبو ظرف آب در تحقیق</p></div>
<div class="m2"><p>عجب این است کاب عین سبوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قطره و بحر جز یکی نبود</p></div>
<div class="m2"><p>آب دریا، چون بنگری، از جوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر دلش کشف کی شود اسرار؟</p></div>
<div class="m2"><p>هر که راضی شود ز مغز به پوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در رخش روی دوست می‌بینم</p></div>
<div class="m2"><p>میل من با جمال او زآن روست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر چه خود غیر او وجودی نیست</p></div>
<div class="m2"><p>لیکن اثبات این حدیث نکوست</p></div></div>
<div class="b2" id="bn31"><p>که به غیر از تو در جهان کس نیست</p>
<p>جز تو موجود جاودان کس نیست</p></div>
<div class="b" id="bn32"><div class="m1"><p>تا مرا دیده شد به روی تو باز</p></div>
<div class="m2"><p>دامن از غیر تو کشیدم باز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرغ جان من شکسته درون</p></div>
<div class="m2"><p>در هوای تو می‌کند پرواز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عشق فرهاد و طلعت شیرین</p></div>
<div class="m2"><p>سر محمود و خاک پای ایاز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بکشی گر ز روی دلداری</p></div>
<div class="m2"><p>گره از کار من گشایی باز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر نفس با دل شکستهٔ من</p></div>
<div class="m2"><p>سخن عشق خود کنی آغاز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در حقیقت به جز تو نیست کسی</p></div>
<div class="m2"><p>گر چه پوشیده‌ای لباس مجاز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفتم اسرار تو بپوشانم</p></div>
<div class="m2"><p>بر زبانم روانه گشت این راز</p></div></div>
<div class="b2" id="bn39"><p>که به غیر از تو در جهان کس نیست</p>
<p>جز تو موجود جاودان کس نیست</p></div>
<div class="b" id="bn40"><div class="m1"><p>ساقیا، بادهٔ الست بیار</p></div>
<div class="m2"><p>تا به می بشکنیم رنج خمار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن چنان مستم از می عشقت</p></div>
<div class="m2"><p>که ز مستی نمی شوم هشیار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بی کمال وجود تو نبود</p></div>
<div class="m2"><p>دو جهان را به نیم جو مقدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هاتف غیب گفت در گوشم</p></div>
<div class="m2"><p>که: به تحقیق بشنو ای گفتار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اصل و فرع جهان وجود شماست</p></div>
<div class="m2"><p>لیس فی‌الدار غیرکم دیار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر زبان فصیح می‌شنوم</p></div>
<div class="m2"><p>از همه کاینات این اسرار</p></div></div>
<div class="b2" id="bn46"><p>که به غیر از تو در جهان کس نیست</p>
<p>جز تو موجود جاودان کس نیست</p></div>
<div class="b" id="bn47"><div class="m1"><p>حسن پوشیده بود زیر نقاب</p></div>
<div class="m2"><p>عشق برداشت از میانه حجاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر دو در روی خویش فتنه شدند</p></div>
<div class="m2"><p>هر دو با هم شدند مست و خراب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در خرابات عاشقی با هم</p></div>
<div class="m2"><p>هر دو خوردند بی‌قدح می ناب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر که را هست دیدهٔ بیدار</p></div>
<div class="m2"><p>نرود چشم بخت او در خواب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جزو را هست سوی کل رغیب</p></div>
<div class="m2"><p>قطره را هست سوی یم ابواب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دیدن غیر تو خطا باشد</p></div>
<div class="m2"><p>نظر این است پیش اهل صواب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون به جز خود کسی نمی‌بیند</p></div>
<div class="m2"><p>زان جهت می‌کند به خویش خطاب</p></div></div>
<div class="b2" id="bn54"><p>که به غیر از تو در جهان کس نیست</p>
<p>جز تو موجود جاودان کس نیست</p></div>
<div class="b" id="bn55"><div class="m1"><p>ای ز عکس رخت جهان روشن</p></div>
<div class="m2"><p>به خیال تو چشم جان روشن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گشته از رویت آفتاب خجل</p></div>
<div class="m2"><p>شده از نورت آسمان روشن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هست از پرتو جمال رخت</p></div>
<div class="m2"><p>از مکان تا بلامکان روشن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به زبان شرح عشق نتوان گفت</p></div>
<div class="m2"><p>که نمی‌گردد از بیان روشن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گرچه خود غیر را وجودی نیست</p></div>
<div class="m2"><p>بر عراقی شد این زمان روشن</p></div></div>
<div class="b2" id="bn60"><p>که به غیر از تو در جهان کس نیست</p>
<p>جز تو موجود جاودان کس نیست</p></div>