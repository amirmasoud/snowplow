---
title: >-
    قصیدهٔ شمارهٔ ۱ - در مدح شیخ حمیدالدین احمد واعظ
---
# قصیدهٔ شمارهٔ ۱ - در مدح شیخ حمیدالدین احمد واعظ

<div class="b" id="bn1"><div class="m1"><p>ای صبا جلوه ده گلستان را</p></div>
<div class="m2"><p>با نوا کن هزاردستان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کن از خواب چشم نرگس را</p></div>
<div class="m2"><p>تا نظاره کند گلستان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن غنچه را پر از زر کن</p></div>
<div class="m2"><p>تا دهد بلبل خوش‌الحان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل خوی کرده را کنی گر یاد</p></div>
<div class="m2"><p>کند ایثار بر تو مرجان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ژاله از روی لاله دور مکن</p></div>
<div class="m2"><p>تا نسوزد ز شعله بستان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مفشان شبنم از سر سبزه</p></div>
<div class="m2"><p>به خضر بخش آب حیوان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا معطر شود همه آفاق</p></div>
<div class="m2"><p>بگشائید زلف جانان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر تشویش خاطر ما را</p></div>
<div class="m2"><p>برفشان طرهٔ پریشان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر زلف بتان به رقص درآر</p></div>
<div class="m2"><p>تا فشانیم بر سرت جان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برقع از روی نیکویان به ربای</p></div>
<div class="m2"><p>تا ببینم ماه تابان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور تماشای خلد خواهی کرد</p></div>
<div class="m2"><p>بطلب راه کوی جانان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگذر از روضه قصد جامع کن</p></div>
<div class="m2"><p>تا ببینی ریاض رضوان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نرمکی طره از رخش وا کن</p></div>
<div class="m2"><p>بنگر آن آفتاب تابان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حسن رخسار یار را بنگر</p></div>
<div class="m2"><p>گر به صورت ندیده‌ای جان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مجلس وعظ واعظ اسلام</p></div>
<div class="m2"><p>حل کن مشکلات قرآن را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اوست اوحد حمید احمد خلق</p></div>
<div class="m2"><p>کز جلالش نمود برهان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش تو ای صبا، چه گویم مدح</p></div>
<div class="m2"><p>گر توانی ادا کنی آن را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برسان از کرم زمین بوسم</p></div>
<div class="m2"><p>ور توانی بگوی ایشان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدمت ما بدو رسان و بگو</p></div>
<div class="m2"><p>کای فراموش کرده یاران را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای ربوده ز من دل و جان را</p></div>
<div class="m2"><p>وی به تاراج داده ایمان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در سر آن دو زلف کافر تو</p></div>
<div class="m2"><p>دل و دین رفت این مسلمان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چشم تو می‌کند خرابی و ما</p></div>
<div class="m2"><p>بر فلک می‌زنیم تاوان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر خرابی همی کند چه عجب؟</p></div>
<div class="m2"><p>خود همین عادت است مستان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مردم چشم تو سیه کارند</p></div>
<div class="m2"><p>وین نه بس نسبت است انسان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه جایی تو را خوش است ولیک</p></div>
<div class="m2"><p>بی تو خوش نیست اهل ملتان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاد کن آرزوی دلها را</p></div>
<div class="m2"><p>بزدای از صدور احزان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قصهٔ درد من بیا بشنو</p></div>
<div class="m2"><p>می‌نیابم، دریغ، درمان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باز سرگشته‌ام همی خواهد</p></div>
<div class="m2"><p>تا چه قصد است چرخ گردان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خواهدم دور کردن از یاران</p></div>
<div class="m2"><p>خود همین عادت است دوران را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ما چه گویی، قضا چو چوگانی</p></div>
<div class="m2"><p>چه از آنجا که گوست چوگان را؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>می‌کند خاطرم پیاپی عزم</p></div>
<div class="m2"><p>که کند یک نظاره جانان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دیده امیدوار می‌باشد</p></div>
<div class="m2"><p>تا ببیند جمال خوبان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>منتظر مانده‌ام قدوم تو را</p></div>
<div class="m2"><p>هین وداعی کن این گران جان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آخر ای جان، غریب شهر توام</p></div>
<div class="m2"><p>خود نپرسی غریب حیران را؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر غریبی که در جهان بینی</p></div>
<div class="m2"><p>عاقبت باز یابد اوطان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جز عراقی که نیست امیدش</p></div>
<div class="m2"><p>تا ببیند وصال کمجان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من نگویم که حسنت افزون باد</p></div>
<div class="m2"><p>چون بدان راه نیست نقصان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باد عمرت فزون و دولت یار</p></div>
<div class="m2"><p>تا بود دور چرخ گردان را</p></div></div>