---
title: >-
    قصیدهٔ شمارهٔ ۷ - در مدح بهاء الدین زکریای ملتانی
---
# قصیدهٔ شمارهٔ ۷ - در مدح بهاء الدین زکریای ملتانی

<div class="b" id="bn1"><div class="m1"><p>روشنان آینهٔ دل چو مصفا بینند</p></div>
<div class="m2"><p>روی دلدار در آن آینه پیدا بینند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پس آینه دزدیده به رویش نگرند</p></div>
<div class="m2"><p>جان فشانند بر او کان رخ زیبا بینند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بدیدند جمالش دل خود را پس از آن</p></div>
<div class="m2"><p>ز آرزوی رخ او واله و شیدا بینند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عارفان چون که ز انوار یقین سرمه کشند</p></div>
<div class="m2"><p>دوست را هر نفس اندر همه اشیا بینند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حقیقت دو جهان آینهٔ ایشان است</p></div>
<div class="m2"><p>که بدو در رخ زیباش هویدا بینند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز خود یاد کنند آینه گردد تیره</p></div>
<div class="m2"><p>چون ازو یاد کنند آینه رخشا بینند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر در منظر دل دلشدگان زان شینند</p></div>
<div class="m2"><p>که تماشاگه دلدار هویدا بینند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناید اندر نظر همتشان هر دو جهان</p></div>
<div class="m2"><p>عاشقان رخ او کی به جهان وا بینند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسم جان پرور او چون به جهان یاد کنند</p></div>
<div class="m2"><p>در درون دل خود عین مسما بینند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاقلان گر چه ز هر چیز بدانند او را</p></div>
<div class="m2"><p>نه همانا بشناسند یقین تا بینند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر صفاتی که عقول بشری دریابد</p></div>
<div class="m2"><p>ذات او زان همه اوصاف مبرا بینند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوشدلان از رخش امروز بهشتی دارند</p></div>
<div class="m2"><p>نه بهشتی که دگر طایفه فردا بینند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر ببینند جمالش نفسی مشتاقان</p></div>
<div class="m2"><p>ز اشتیاقش دل خود واله و شیدا بینند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نفسی باد صبا گر به سر کوش وزد</p></div>
<div class="m2"><p>خوشدمان خوش‌تر از انفاس مسیحا بینند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تشنگان ار همه دریای محیط آشامند</p></div>
<div class="m2"><p>در دل از آتش سوداش شررها بینند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درد نوشان که همه دردی دردش نوشند</p></div>
<div class="m2"><p>مستی دردی دردش نه ز صهبا بینند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ساغر دل ز می عشق لبالب دارند</p></div>
<div class="m2"><p>دم به دم حسن رخ یار در آنجا بینند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرمی ساغرشان عکس بر افلاک زند</p></div>
<div class="m2"><p>کل افلاک چو ذرات مجزا بینند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سالکان چون که هوا را به قدم پست کنند</p></div>
<div class="m2"><p>پای خود بر زبر عرض معلا بینند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرشان بر سر زانو، رخشان بر در دوست</p></div>
<div class="m2"><p>قبلهٔ زانوی خود را که سینا بینند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باز محنت‌زدگان از غم و اندوه و فراق</p></div>
<div class="m2"><p>دل چو آتشکده و دیده چو دریا بینند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر زنند از سر حسرت نفسی وقت تموز</p></div>
<div class="m2"><p>بس که تفسیده دلان زاندم سرما بینند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور برآرند دگر باره دمی از سر شوق</p></div>
<div class="m2"><p>زآن نفس اهل زمستان همه گرما بینند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قدسیان منزلت این چو همه در نگرند</p></div>
<div class="m2"><p>رتبت قطب زمان از همه بالا بینند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از مقامات جلالش همه را رشک آید</p></div>
<div class="m2"><p>که مقامش ز مقامات خود اعلا بینند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه گویند که آیا که تواند بودن</p></div>
<div class="m2"><p>که جهان روشن از آن طلعت غرا بینند؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ناگه از لطف زمانی سوی ایشان نگرند</p></div>
<div class="m2"><p>همه مدهوش شوند، جانب بالا بینند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خاص حق، صاحب قدوس، بهاء الاسلام</p></div>
<div class="m2"><p>غوث دین، رحمت عالم زکریا بینند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زده یابند سراپردهٔ او در ملکوت</p></div>
<div class="m2"><p>هم نشینش ملک‌العرش تعالی بینند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سبحه‌اش نور و مصلاش ردای رحمان</p></div>
<div class="m2"><p>لجهٔ بحر ظهورش متوضا بینند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خاک پایش به تبرک همه در دیده کشند</p></div>
<div class="m2"><p>تا مگر از مددش نور تجلا بینند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قطب وقت اوست، همه عالم ازو آسوده</p></div>
<div class="m2"><p>بر درس زبدهٔ ابدال تولا بینند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خوبرویان به جهان شیخ هم او را دانند</p></div>
<div class="m2"><p>در جهان نیست جزو شیخ دگر تا بینند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شهسواری که به چوگان قضا گوی مراد</p></div>
<div class="m2"><p>برباید ز قدر، همت او را بینند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنکه در قبضهٔ او هر دو جهان گم گردد</p></div>
<div class="m2"><p>گر بجویند جزو را نه همانا بینند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بی‌دلان از نظر او دل بینا یابند</p></div>
<div class="m2"><p>مردگان از نفس او دم احیا بینند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خادمان در او آخرت و دنیی را</p></div>
<div class="m2"><p>بر در خدمت او لؤلؤ لالا بینند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خانگاه کهنش از فلک اعلی یابند</p></div>
<div class="m2"><p>جایگاه نو او جنت‌ماوی بینند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در جهان هر که ز خاک در او سرمه نکرد</p></div>
<div class="m2"><p>دیدهٔ بخت بدش اعمش و اعمی بینند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر سر کوش عزیزان به عراقی نگرند</p></div>
<div class="m2"><p>دل محنت‌زده‌اش در کف سودا بینند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بهر او زار بگریند، که او را پیوست</p></div>
<div class="m2"><p>از پی فعل بدش بی سر و بی‌پا بینند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دوستانش چو ببینند بمویند برو</p></div>
<div class="m2"><p>دل او را چو به کام دل اعدا بینند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مکر ما، بر در لطف تو پناه آورده است</p></div>
<div class="m2"><p>بندگان ملجا خود را در مولی بینند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز آفتاب نظرت بر سر او سایه فگن</p></div>
<div class="m2"><p>تا مگر بر مگسی سایهٔ عنقا بینند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر چوریم آهن زنگار پذیر است دلش</p></div>
<div class="m2"><p>سوی او کن نظری، کاینه سیما بینند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زار گریند بر احوال دلش نرم دلان</p></div>
<div class="m2"><p>که دلش سخت‌تر از صخرهٔ صما بینند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بگشای از دلش، ای موسی عهد، آب خضر</p></div>
<div class="m2"><p>به عصایی که تو را در ید بیضا بینند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بوسه‌گاه همه پاکان جهان باد درت</p></div>
<div class="m2"><p>کز همه درگه تو ملجا و ماوی بینند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عالم از نفس شریف تو مبادا خالی</p></div>
<div class="m2"><p>که جهان هر دم از انفاس تو بویا بینند</p></div></div>