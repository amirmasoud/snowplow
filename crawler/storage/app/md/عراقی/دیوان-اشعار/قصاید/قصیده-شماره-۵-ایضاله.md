---
title: >-
    قصیدهٔ شمارهٔ ۵ - ایضاله
---
# قصیدهٔ شمارهٔ ۵ - ایضاله

<div class="b" id="bn1"><div class="m1"><p>طرب، ای دل، که نوبهار آمد</p></div>
<div class="m2"><p>از صبا بوی زلف یار آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان نظاره که گل جمال نمود</p></div>
<div class="m2"><p>هین تماشا که نوبهار آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رخ او جمال یار ببین</p></div>
<div class="m2"><p>که گل از یار یادگار آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تماشای باغ و بستان شو</p></div>
<div class="m2"><p>که چمن خلد آشکار آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از صبا حال کوی یار بپرس</p></div>
<div class="m2"><p>که سحرگاه از آن دیار آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر در یار ما گذشت نسیم</p></div>
<div class="m2"><p>زان گل افشان و مشکبار آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا صبا زان چمن گل افشان شد</p></div>
<div class="m2"><p>چون من از ضعف بی‌قرار آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دید چون عندلیب ضعف نسیم</p></div>
<div class="m2"><p>به عیادت به مرغزار آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل سوی فاخته اشارت کرد:</p></div>
<div class="m2"><p>هین نوایی که وقت کار آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلبل از شوق گل چنان نالید</p></div>
<div class="m2"><p>که گل از وجد جان سپار آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>های و هوی فتاد در گلزار</p></div>
<div class="m2"><p>نالهٔ عاشقان زار آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گل مگر جلوه می‌کند در باغ؟</p></div>
<div class="m2"><p>کز چمن نالهٔ هزار آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زرفشان می‌کند گل صد برگ</p></div>
<div class="m2"><p>کش صبا دوش در کنار آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل زرافشان اگر کند چه عجب؟</p></div>
<div class="m2"><p>کز شمالش بسی یسار آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گل زر افشاند و ز ابر بر سر او</p></div>
<div class="m2"><p>صد هزاران گهر نثار آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غنچه از بند او نشد آزاد</p></div>
<div class="m2"><p>زان گرفتار زخم خار آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خار کز غنچه کیسه‌ای بر دوخت</p></div>
<div class="m2"><p>می زنندش که مایه دار آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست آزاده‌ای مگر سوسن</p></div>
<div class="m2"><p>که نه در بند کار و بار آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لاله را دل بسوخت بر نرگس</p></div>
<div class="m2"><p>که نصیبش ز می خمار آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ابر بگریست بر گل، از پی آنک</p></div>
<div class="m2"><p>زین جهان بر دلش غبار آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد ز یاری جدا بنفشه مگر</p></div>
<div class="m2"><p>که چنین وقت سوکوار آمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جامهٔ سوک بر بنفشه برید</p></div>
<div class="m2"><p>زان مگر لاله دل‌فگار آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نقش رنگ چمن ز لطف بهار</p></div>
<div class="m2"><p>نقش دیبای پرنگار آمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوش بهاری است، لیک آن کس را</p></div>
<div class="m2"><p>کز لب یار میگسار آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هان، عراقی، تو و نسیم بهار</p></div>
<div class="m2"><p>کز صبا بوی زلف یار آمد</p></div></div>