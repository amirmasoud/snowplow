---
title: >-
    قصیدهٔ شمارهٔ ۲ - در مدح شیخ بهاء الدین زکریا ملتانی
---
# قصیدهٔ شمارهٔ ۲ - در مدح شیخ بهاء الدین زکریا ملتانی

<div class="b" id="bn1"><div class="m1"><p>لاح صباح الوصال در شموس القراب</p></div>
<div class="m2"><p>صاح قماری الطرب دار کئوس الشراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهد سرمست من دید مرا در خمار</p></div>
<div class="m2"><p>داد ز لعل خودم در عقیق مذاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهرهٔ زیبای او برده ز من صبر و هوش</p></div>
<div class="m2"><p>جام طرب زای او کرده نهادم خراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ز جهان بی‌خبر، کرد دل من نظر</p></div>
<div class="m2"><p>دید جهانی دگر برتر ازین نه نقاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساحت آن دلگشای روضهٔ آن جانفزای</p></div>
<div class="m2"><p>ذرهٔ آن آفتاب سایهٔ آن مهر ناب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل متحیر درو کینت جهانی عظیم</p></div>
<div class="m2"><p>جان متعجب درو کینت گشاد عجاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هاتف مشکل گشای گشت مرا رهنمای</p></div>
<div class="m2"><p>گفت بگویم تو را گر نکنی اضطراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عکس جمال قدیم نور بهای قدیر</p></div>
<div class="m2"><p>کرد جمال آشکار از تتق بی‌حجاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعشعهٔ روی او کرد جهان مستنیر</p></div>
<div class="m2"><p>لخلخهٔ خوی او کرد جهان مستطاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور جبینش به روز مشرق صبح یقین</p></div>
<div class="m2"><p>صبح ضمیرش به شب مطلع صد آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیدهٔ ادراک او ناظر احکام لوح</p></div>
<div class="m2"><p>چشم دل پاک او مشرق ام‌الکتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاطر وقاد او کاشف اسرار غیب</p></div>
<div class="m2"><p>پرتو انوار او محرق نور حجاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از رغبوتش فراغ وز رهبوتش امان</p></div>
<div class="m2"><p>در ملکوتش خیم در جبروتش قباب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دم او تافته از دم عیسی نشان</p></div>
<div class="m2"><p>در دلش افروخته ز آتش موسی شهاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ساقی لطف قدم داده به جام کرم</p></div>
<div class="m2"><p>بهر دلش دم بدم از خم خلقت شراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرده دو صد بحر نوش تا شده یکدم ز هوش</p></div>
<div class="m2"><p>باز شده در خروش سینهٔ او کاب آب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اصبح مستبشرا من سبحات‌الجمال</p></div>
<div class="m2"><p>اشرق مستهترا من سطوات‌القراب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لاح من اسراره طلعت صبح‌الیقین</p></div>
<div class="m2"><p>راح بانواره ظلمت لیل ارتیاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راهبر اصفیا پیشرو اولیا</p></div>
<div class="m2"><p>هم کنف انبیا صاحب حق کامیاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شیخ شیوخ جهان قطب زمین و زمان</p></div>
<div class="m2"><p>غوث همه انس و جان معتق مالک رقاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ناشر علم‌الیقین کاشف عین‌الیقین</p></div>
<div class="m2"><p>واجد حق‌الیقین هادی مهدی خطاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مفضل فاضل پناه عالم عالم نواز</p></div>
<div class="m2"><p>مکمل کامل صفات عالی عالی‌جناب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پرسی اگر در جهان کیست امام‌الامام؟</p></div>
<div class="m2"><p>نشنوی از آسمان جز زکریا جواب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیستی ار مستحیل از پس آل رسول</p></div>
<div class="m2"><p>آمدی از حق یقین وحی بدو صد کتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در نظر همتش هر دو جهان نیم جو</p></div>
<div class="m2"><p>در کف دریا و شش هفت فلک یک حباب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سالک مسلوک را در بر او بازگشت</p></div>
<div class="m2"><p>طالب مطلوب را از در او فتح باب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سدهٔ اقبال او قبلهٔ اهل ثواب</p></div>
<div class="m2"><p>کعبهٔ افضال او مامن اهل‌العقاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نظرة انعامه روح قلوب الصدور</p></div>
<div class="m2"><p>تربت اقدامه کحل عیون النقاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای به تو روشن جهان ذره چه گوید ثنا؟</p></div>
<div class="m2"><p>خاطر من شب پره مدح تو خورشید تاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیش سلیمان چو مور تحفه‌ای آرم ملخ</p></div>
<div class="m2"><p>مجلس داود را نغمه طنین ذباب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خاک درت را از آن دردسری می‌دهم</p></div>
<div class="m2"><p>بو که دهد بوی او درد دلم را گلاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنگ به فتراک تو زان زده‌ام بنده‌وار</p></div>
<div class="m2"><p>تا کنیم روز عرض با خدمت هم رکاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در کنف لطف تو برده عراقی پناه</p></div>
<div class="m2"><p>درگه رحمان بود عاجزکان را مآب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر شنود مصطفی مدحت حسان تو</p></div>
<div class="m2"><p>گویدم احسنت قد جرت کنوزالصواب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>باد به انفاس تو زنده دل عاشقان</p></div>
<div class="m2"><p>تا بود انفاس خلق در دو جهان بی‌حساب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چاکر درگاه تو اهل سما چون ملوک</p></div>
<div class="m2"><p>خاک کف پای تو اهل زمین چون تراب</p></div></div>