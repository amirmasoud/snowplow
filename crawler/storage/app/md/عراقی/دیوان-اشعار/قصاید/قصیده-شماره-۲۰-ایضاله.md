---
title: >-
    قصیدهٔ شمارهٔ ۲۰ - ایضاله
---
# قصیدهٔ شمارهٔ ۲۰ - ایضاله

<div class="b" id="bn1"><div class="m1"><p>ای جلالت فرش عزت جاودان انداخته</p></div>
<div class="m2"><p>گوی در میدان وحدت کامران انداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رایت مهر جمالت لایزال افروخته</p></div>
<div class="m2"><p>سایهٔ چتر جلالت جاودان انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاب انوار جمالت بهر اظهار کمال</p></div>
<div class="m2"><p>پرتوی بر ظلمت‌آباد جهان انداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور خود را جلوه داده در لباس این و آن</p></div>
<div class="m2"><p>در جهان آوازهٔ کون و مکان انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی خود را گفته: ظاهر شو بهر صورت که هست</p></div>
<div class="m2"><p>پس به عالم در، ندای کن فکان انداخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فروغ روی خود روی زمین افروخته</p></div>
<div class="m2"><p>پس بهانه بر چراغ آسمان انداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود همه هستی شده وانگه برای روی پوش</p></div>
<div class="m2"><p>نام هستی گه برین و گه بر آن انداخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چیست عالم بی‌فروغ آفتاب روی تو؟</p></div>
<div class="m2"><p>کمتر از هیچ است در کنج هوان انداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش ازین بی‌تو جهان چون بود در کتم عدم؟</p></div>
<div class="m2"><p>هم بر آن حال است حالی همچنان انداخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بیابان عدم عالم سرابی بیش نیست</p></div>
<div class="m2"><p>تشنگان را بهر سود اندر زیان انداخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظاهر و باطن تویی و طالب و مطلوب تو</p></div>
<div class="m2"><p>و آن دگر نامی است اندر هر زبان انداخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در محیط هستیت عالم به جز یک موج نیست</p></div>
<div class="m2"><p>باد تقدیرت به هر جانب روان انداخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صد هزاران گوهر معنی و صورت هر نفس</p></div>
<div class="m2"><p>موج این دریا به پیدا و نهان انداخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز دریای جلالت ناگهان موجی زده</p></div>
<div class="m2"><p>جمله را در قعر بحر بی‌کران انداخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمله یک چیز است موج و گوهر و دریا ولیک</p></div>
<div class="m2"><p>صورت هریک خلافی در میان انداخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی خود بنموده هر دم در هزاران آینه</p></div>
<div class="m2"><p>در هر آیینه رخت دیگر نشان انداخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آفتابی در هزاران آبگینه تافته</p></div>
<div class="m2"><p>پس به رنگ هریکی تابی عیان انداخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در همه صورت تویی و نیست خود صورت تو را</p></div>
<div class="m2"><p>وین حقیقت حیرتی در رهروان انداخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جمله یک نور است، لیکن رنگ‌های مختلف</p></div>
<div class="m2"><p>اختلافی در میان انس و جان انداخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا جمال تو نبینند بی‌نقاب انقلاب</p></div>
<div class="m2"><p>بر رخ از غیرت ردای جاودان انداخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک کرشمه کرده با خود جنبشی عشق قدیم</p></div>
<div class="m2"><p>در دو عالم اینهمه شور و فغان انداخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در گلستان روی خود دیده به چشم بلبلان</p></div>
<div class="m2"><p>غلغلی از بلبلان در گلستان انداخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جنبش عشق قدیم از خود به خود دیده مقیم</p></div>
<div class="m2"><p>در میانه تهمتی بر بلبلان انداخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک سخن با خویشتن گفته و زان هر ذره را</p></div>
<div class="m2"><p>در زبان صد گونه تقدیر و بیان انداخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آشکارا کرده اسرار تو هم گفتار تو</p></div>
<div class="m2"><p>پس بهانه بر زبان ترجمان انداخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گشته‌ام سرگشته از وصف کمال کبریات</p></div>
<div class="m2"><p>ای کمال تو یقین را در گمان انداخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرچه از دریای توحید آب حیوان می‌کشم</p></div>
<div class="m2"><p>مانده‌ام از تشنگی بر لب زبان انداخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تهمت دریا کشم خواهم که دریایی شوم</p></div>
<div class="m2"><p>کاندرو موجی نباشد هر زمان انداخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا عراقی لنگر من شد در این دریای ژرف</p></div>
<div class="m2"><p>کشتی سیر مرا شد بادبان انداخته</p></div></div>