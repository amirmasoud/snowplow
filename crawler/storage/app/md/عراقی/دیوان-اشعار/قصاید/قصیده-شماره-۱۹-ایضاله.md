---
title: >-
    قصیدهٔ شمارهٔ ۱۹ - ایضاله
---
# قصیدهٔ شمارهٔ ۱۹ - ایضاله

<div class="b" id="bn1"><div class="m1"><p>قبلهٔ روی صوفیان بارگه صفای او</p></div>
<div class="m2"><p>سرمهٔ چشم قدسیان خاک در سرای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر بحر اجتبا، مهر سپهر اصطفا</p></div>
<div class="m2"><p>یافته نور انبیا روشنی از ضیای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تافته حسن ایزدی از رخ خوب احمدی</p></div>
<div class="m2"><p>خضر بقای سرمدی یافته از لقای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برده ز مرسلان سبق خاتم انبیا به حق</p></div>
<div class="m2"><p>طینت او ز نور حق طلعتش از بهای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حضرت عزتش وطن خلوت او در انجمن</p></div>
<div class="m2"><p>خاص و ندیم ذوالمنن هر دو جهان سرای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاکر درگهش جهان حاجبیش به انس و جان</p></div>
<div class="m2"><p>عرش مجیدش آسمان ساحت قرب جای او</p></div></div>