---
title: >-
    قصیدهٔ شمارهٔ ۲۴ - در مدح شیخ حمیدالدین
---
# قصیدهٔ شمارهٔ ۲۴ - در مدح شیخ حمیدالدین

<div class="b" id="bn1"><div class="m1"><p>که برد از من بی‌دل بر جانان خبری؟</p></div>
<div class="m2"><p>یا که آرد ز نسیم سر کویش اثری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز صبا کیست کزین خسته برد پیغامی؟</p></div>
<div class="m2"><p>جز نسیم از بر دلدار که آرد خبری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای صبا، چند روزی گرد گلستان و چمن؟</p></div>
<div class="m2"><p>چند آشفته کنی طرهٔ هر خوش پسری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای صبا، صبح دمی بر سر کویش بگذر</p></div>
<div class="m2"><p>تا معطر شود آفاق ز تو هر سحری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه زن خاک کف پای حمیدالدین را</p></div>
<div class="m2"><p>که چنو یار ندارم به جهان دگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو سحر خاک کف پای کریم‌الدین بوس</p></div>
<div class="m2"><p>تا معطر شود آفاق ز تو هر سحری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه چون من همه کس از دل و جان بندهٔ اوست</p></div>
<div class="m2"><p>گرچه در خاطر او نیست کسی را خطری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدمت بنده به وجهی که توانی برسان</p></div>
<div class="m2"><p>که: بیا، کز غم هجرانت شدم دربدری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غم هجر تو تنها نه منم، کز یاران</p></div>
<div class="m2"><p>هر کسی راست به قدر خود ازین غم قدری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برسان خدمت و گو: ای رخت از جان خوشتر</p></div>
<div class="m2"><p>چند نالد ز فراق رخ تو لابه‌گری؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو چه دانی که چها کرد فراقت با من؟</p></div>
<div class="m2"><p>داند این آنکه ازین غم بود او را قدری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غم هجران تو، ای دوست، چنان کرد مرا</p></div>
<div class="m2"><p>که ببینی نشناسی که منم یا دگری؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دو چشم تو، که چون چشم تو بیمار توام</p></div>
<div class="m2"><p>چه شود گر بفرستی ز دو عالم شکری؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دوستان منتظر مقدم میمون تواند</p></div>
<div class="m2"><p>بیش ازین خود نشکیبند، بیا زودتری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر عزیمت کنی ای دوست، به سوی ملتان</p></div>
<div class="m2"><p>چه مبارک بود آن عزم و چه نیکو سفری؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر خیال تو شب و روز همی گریم زار</p></div>
<div class="m2"><p>چه کنم؟ همرهم و می‌دهمش دردسری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا نگویی که چرا رفت سراسیمهٔ ما</p></div>
<div class="m2"><p>در نمانم ز جوابت، بشنو ماحضری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر خود و دیدهٔ خود غیرتم آمد، رفتم</p></div>
<div class="m2"><p>تا نبیند رخ زیبای تو هر مختصری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من که بر دیدهٔ خود رشک برم چون بینم؟</p></div>
<div class="m2"><p>که ببیند رخ تو دیدهٔ کوته‌نظری؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از برای دل من روی به هر کس منمای</p></div>
<div class="m2"><p>کان رخ، انصاف، دریغ است به هر دیده‌وری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از درت خسته عراقی سبب غیرت رفت</p></div>
<div class="m2"><p>ورنه بودی به سر راه تو هر بی‌بصری</p></div></div>