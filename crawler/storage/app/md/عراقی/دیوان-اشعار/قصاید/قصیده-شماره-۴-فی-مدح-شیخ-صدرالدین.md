---
title: >-
    قصیدهٔ شمارهٔ ۴ - فی مدح شیخ صدرالدین
---
# قصیدهٔ شمارهٔ ۴ - فی مدح شیخ صدرالدین

<div class="b" id="bn1"><div class="m1"><p>دل تو را دوست‌تر ز جان دارد</p></div>
<div class="m2"><p>جان ز بهر تو در میان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کند جان به تو نثار مرنج</p></div>
<div class="m2"><p>چه کند؟ دسترس همان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با غمت زان خوشم که جان مرا</p></div>
<div class="m2"><p>غمت هر لحظه شادمان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دلم بار هجر پیش منه</p></div>
<div class="m2"><p>آخر این خسته نیز جان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ ز مشتاق خود نهان چه کنی</p></div>
<div class="m2"><p>آنچنان رخ کسی نهان دارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رخ تو توان فشاندن جان</p></div>
<div class="m2"><p>راستی را رخ تو آن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خیال لب تو دوش دلم</p></div>
<div class="m2"><p>گفت: جان عزم آن جهان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسه‌ای ده مرا، که نوش لبت</p></div>
<div class="m2"><p>لذت عیش جاودان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سر خشم گفت چشم تو: دور</p></div>
<div class="m2"><p>نه کسی بوسه رایگان دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش برآشفت زلف تو که: خموش</p></div>
<div class="m2"><p>زندگانی تو را زیان دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کز شکر خواب دیده معذور است</p></div>
<div class="m2"><p>در درون جان ناتوان دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرهمی، پیش از آنکه از تو دلم</p></div>
<div class="m2"><p>پیش صدر جهان فغان دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عرش بابی، که مهر همت او</p></div>
<div class="m2"><p>برتر از عرش آشیان دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رهنمایی، که پرتو نورش</p></div>
<div class="m2"><p>روشن اطراف کن فکان دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زان سوی کاینات صحرایی است</p></div>
<div class="m2"><p>او در آن لامکان مکان دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سبق ام‌الکتاب می‌گیرد</p></div>
<div class="m2"><p>لوح محفوظ خود روان دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شمه‌ای از نسیم اخلاقش</p></div>
<div class="m2"><p>روضهٔ گلشن جنان دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ذره‌ای از فروغ انوارش</p></div>
<div class="m2"><p>آفتاب شررفشان دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بوی خلق محمد آن بوید</p></div>
<div class="m2"><p>که در آن روضه‌ای قران دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرفراز آن کسی بود که چو چرخ</p></div>
<div class="m2"><p>بر درش سر بر آستان دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خاک درگاه او کسی بوسد</p></div>
<div class="m2"><p>کز فلک هفت نردبان دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش او مهر چون زمین بوسد</p></div>
<div class="m2"><p>زیبد ار سر بر آسمان دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ریزه چینی است از سر خوانش</p></div>
<div class="m2"><p>آسمان گر چه هفت خوان دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسکه بر خوان او نواله ربود</p></div>
<div class="m2"><p>در بغل زان دوتای نان دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چاشنی گیر او بود رضوان</p></div>
<div class="m2"><p>قدسیان را چو میهمان دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرد خاک درش نگردد دیو</p></div>
<div class="m2"><p>زانکه جبریل آشیان دارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگریزد ز سایه‌اش شیطان</p></div>
<div class="m2"><p>ز آنکه از نور سایبان دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نهراسد ز بیم گرگ عدو</p></div>
<div class="m2"><p>رمه‌ای کو چو تو شبان دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر سر آمد ز جمله عالمیان</p></div>
<div class="m2"><p>بسکه او علم بی‌کران دارد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر سر آید پسر ز اهل زمان</p></div>
<div class="m2"><p>چو پدر صاحب‌الزمان دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فتح گردد ز فضل او آن در</p></div>
<div class="m2"><p>کز جهان روی سوی آن دارد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>منعما، ذکر شکر تو پیوست</p></div>
<div class="m2"><p>خاطرم بر سر زبان دارد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لیک اظهار، شرط عاشق نیست</p></div>
<div class="m2"><p>مگر از شوق دل، تپان دارد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زنده کردی شکسته را به سه بیت</p></div>
<div class="m2"><p>کز دم عیسوی نشان دارد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حرز جان ساختم سه بیت تو را</p></div>
<div class="m2"><p>که ز صد فتنه در امان دارد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خسته چون خواند نظم تو، ز طرب</p></div>
<div class="m2"><p>پی بر فرق فرقدان دارد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر کند فخر بر جهان، رسدش</p></div>
<div class="m2"><p>که مربی مهربان دارد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خواستم تا جواب گویم، عقل</p></div>
<div class="m2"><p>گفت: که طاقت و توان دارد؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عاجز آید ز دست مدح و ثنات</p></div>
<div class="m2"><p>هر که پا در ره بیان دارد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در مدح تو چون زنم؟ که ز غم</p></div>
<div class="m2"><p>خاطرم قفل بر دهان دارد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باد از انوار تو جهان روشن</p></div>
<div class="m2"><p>تا جهان نور ز اختران دارد</p></div></div>