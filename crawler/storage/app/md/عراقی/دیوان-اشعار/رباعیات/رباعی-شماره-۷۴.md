---
title: >-
    رباعی شمارهٔ ۷۴
---
# رباعی شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>دل دیدن رویت به دعا می‌خواهد</p></div>
<div class="m2"><p>وصلت به تضرع از خدا می‌خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستند شکرلبان درین ملک بسی</p></div>
<div class="m2"><p>لیکن دل دیوانه تو را می‌خواهد</p></div></div>