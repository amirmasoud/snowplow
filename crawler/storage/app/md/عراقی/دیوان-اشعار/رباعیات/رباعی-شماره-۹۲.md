---
title: >-
    رباعی شمارهٔ ۹۲
---
# رباعی شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>خاک سر کوی آن بت مشکین خال</p></div>
<div class="m2"><p>می‌بوسیدم شبی به امید وصال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنهان ز رقیب آمد و در گوشم گفت:</p></div>
<div class="m2"><p>می‌خور غم ما و خاک بر لب میمال</p></div></div>