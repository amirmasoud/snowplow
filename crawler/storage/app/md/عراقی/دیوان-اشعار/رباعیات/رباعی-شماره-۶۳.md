---
title: >-
    رباعی شمارهٔ ۶۳
---
# رباعی شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>زان پیش که این چرخ معلا کردند</p></div>
<div class="m2"><p>وز آب و گل این نقش معما کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی ز می عشق تو بر ما کردند</p></div>
<div class="m2"><p>صبر و خرد ما همه یغما کردند</p></div></div>