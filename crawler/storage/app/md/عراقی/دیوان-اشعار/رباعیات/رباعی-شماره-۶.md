---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>تا با توام، از تو جان دهم آدم را</p></div>
<div class="m2"><p>وز نور تو روشنی دهم عالم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بی‌تو بوم، قوت آنم نبود</p></div>
<div class="m2"><p>کز سینه به کام خود برآرم دم را</p></div></div>