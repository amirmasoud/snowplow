---
title: >-
    رباعی شمارهٔ ۷۸
---
# رباعی شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>این عمر، که برده‌ای تو بی‌یار بسر</p></div>
<div class="m2"><p>ناکرده دمی بر در دلدار گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا، بنشین و ماتم خود می‌دار</p></div>
<div class="m2"><p>کان رفت که آید ز تو کاری دیگر</p></div></div>