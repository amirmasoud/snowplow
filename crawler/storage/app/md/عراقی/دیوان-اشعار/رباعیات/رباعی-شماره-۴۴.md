---
title: >-
    رباعی شمارهٔ ۴۴
---
# رباعی شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>افسوس! که ایام جوانی بگذشت</p></div>
<div class="m2"><p>سرمایهٔ عیش جاودانی بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنه به کنار جوی چندان خفتم</p></div>
<div class="m2"><p>کز جوی من آب زندگانی بگذشت</p></div></div>