---
title: >-
    رباعی شمارهٔ ۱۲۸
---
# رباعی شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>ای مایهٔ اصل شادمانی غم تو</p></div>
<div class="m2"><p>خوشتر ز حیات جاودانی غم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حسن تو رازها به گوش دل من</p></div>
<div class="m2"><p>گوید به زبان بی‌زبانی غم تو</p></div></div>