---
title: >-
    رباعی شمارهٔ ۵۶
---
# رباعی شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>بازم غم عشق یار در کار آورد</p></div>
<div class="m2"><p>غم در دل من، بین، که چه گل بار آورد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سال بهار ما گل آوردی بار</p></div>
<div class="m2"><p>امسال بجای گل همه خار آورد</p></div></div>