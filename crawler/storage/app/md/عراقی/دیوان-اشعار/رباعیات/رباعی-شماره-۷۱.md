---
title: >-
    رباعی شمارهٔ ۷۱
---
# رباعی شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>مازار کسی، کز تو گزیرش نبود</p></div>
<div class="m2"><p>جز بندگی تو در ضمیرش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخشای بر آن کسی، که هر شب تا روز</p></div>
<div class="m2"><p>جز آب دو دیده دستگیرش نبود</p></div></div>