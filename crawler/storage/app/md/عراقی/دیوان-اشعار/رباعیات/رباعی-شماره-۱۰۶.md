---
title: >-
    رباعی شمارهٔ ۱۰۶
---
# رباعی شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>اندر غم تو نگار، همچون نارم</p></div>
<div class="m2"><p>می‌سوزم و می‌سازم و دم برنارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دست به گردن تو اندر نارم</p></div>
<div class="m2"><p>آکنده به غم چو دانه اندر نارم</p></div></div>