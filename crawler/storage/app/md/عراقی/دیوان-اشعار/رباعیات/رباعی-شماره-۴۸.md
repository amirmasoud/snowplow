---
title: >-
    رباعی شمارهٔ ۴۸
---
# رباعی شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>در عشق توام واقعه بسیار افتاد</p></div>
<div class="m2"><p>لیکن نه بدین سان که ازین بار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیسی چو رخت بدید دل شیدا شد</p></div>
<div class="m2"><p>از خرقه و سجاده به زنار افتاد</p></div></div>