---
title: >-
    رباعی شمارهٔ ۵۲
---
# رباعی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>گر من روزی ز خدمتت گشتم فرد</p></div>
<div class="m2"><p>صد بار دلم از آن پشیمانی خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا، به یکی گناه از بنده مگرد</p></div>
<div class="m2"><p>من آدمیم، گنه نخست آدم کرد</p></div></div>