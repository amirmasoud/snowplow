---
title: >-
    رباعی شمارهٔ ۴۰
---
# رباعی شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>دل سوختگان را خبر از عشق تو نیست</p></div>
<div class="m2"><p>مشتاق هوا را اثر از عشق تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر دو جهان نیک نظر کرد دلم</p></div>
<div class="m2"><p>زان هیچ مقام برتر از عشق تو نیست</p></div></div>