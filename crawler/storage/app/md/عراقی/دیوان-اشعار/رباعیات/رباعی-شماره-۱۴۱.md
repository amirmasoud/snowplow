---
title: >-
    رباعی شمارهٔ ۱۴۱
---
# رباعی شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>ای کاش! به سوی وصل راهی بودی</p></div>
<div class="m2"><p>یا در دلم از صبر سپاهی بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش! چو در عشق تو من کشته شوم</p></div>
<div class="m2"><p>جز دوستی توام گناهی بودی</p></div></div>