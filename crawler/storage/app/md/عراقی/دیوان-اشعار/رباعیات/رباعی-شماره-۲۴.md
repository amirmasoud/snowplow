---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>بیمار توام، روی توام درمان است</p></div>
<div class="m2"><p>جان داروی عاشقان رخ جانان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشتاب، که جانم به لب آمد بی‌تو</p></div>
<div class="m2"><p>دریاب مرا، که بیش نتوان دانست</p></div></div>