---
title: >-
    رباعی شمارهٔ ۴۹
---
# رباعی شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>چون سایهٔ دوست بر زمین می‌افتد</p></div>
<div class="m2"><p>بر خاک رهم ز رشک کین می‌افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده، تو کام خویش، باری، بستان</p></div>
<div class="m2"><p>روزیت که فرصتی چنین می‌افتد</p></div></div>