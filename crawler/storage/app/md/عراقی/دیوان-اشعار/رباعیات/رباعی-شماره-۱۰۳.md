---
title: >-
    رباعی شمارهٔ ۱۰۳
---
# رباعی شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>ای دوست، بیا، که با تو باقی دارم</p></div>
<div class="m2"><p>با هجر تو چند وثاقی دارم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در من نظری کن، که مگر باز رهم</p></div>
<div class="m2"><p>زین درد که از درد عراقی دارم</p></div></div>