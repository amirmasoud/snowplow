---
title: >-
    رباعی شمارهٔ ۹۷
---
# رباعی شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>آن وصل تو باز، آرزو می‌کندم</p></div>
<div class="m2"><p>گفتن به تو راز، آرزو می‌کندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خفتن ببرت به ناز تا روز سپید</p></div>
<div class="m2"><p>شب‌های دراز، آرزو می‌کندم</p></div></div>