---
title: >-
    رباعی شمارهٔ ۱۶۰
---
# رباعی شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>گفتم که: اگر چه آفت جان منی</p></div>
<div class="m2"><p>جان پیش کشم تو را، که جانان منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که: اگر بندهٔ فرمان منی</p></div>
<div class="m2"><p>آن دگران مباش، چون زآن منی</p></div></div>