---
title: >-
    رباعی شمارهٔ ۳۷
---
# رباعی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>بی آنکه دو دیده بر جمالت نگریست</p></div>
<div class="m2"><p>در آرزوی روی تو خونابه گریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره بمانده‌ام، دریغا! بی تو</p></div>
<div class="m2"><p>بیچاره کسی که بی تواش باید زیست</p></div></div>