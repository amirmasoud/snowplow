---
title: >-
    رباعی شمارهٔ ۱۵۷
---
# رباعی شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>گر من به صلاح خویش کوشان بدمی</p></div>
<div class="m2"><p>سالار همه کبودپوشان بدمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که اسیر و رند و می‌خوار شدم</p></div>
<div class="m2"><p>ای کاش! غلام می‌فروشان بدمی</p></div></div>