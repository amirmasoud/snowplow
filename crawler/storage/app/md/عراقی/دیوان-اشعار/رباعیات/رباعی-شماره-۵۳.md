---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>نرگس، که ز سیم بر سر افسر دارد</p></div>
<div class="m2"><p>با دیدهٔ کور باد در سر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دست عصایی ز زمرد دارد</p></div>
<div class="m2"><p>کوری به نشاط شب مکرر دارد</p></div></div>