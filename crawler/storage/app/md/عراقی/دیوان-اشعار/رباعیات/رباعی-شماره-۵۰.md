---
title: >-
    رباعی شمارهٔ ۵۰
---
# رباعی شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>غم گرد دل پر هنران می‌گردد</p></div>
<div class="m2"><p>شادی همه بر بی‌خبران می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار! که قطب فلک دایره‌وار</p></div>
<div class="m2"><p>در دیدهٔ صاحب‌نظران می‌گردد</p></div></div>