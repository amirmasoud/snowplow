---
title: >-
    رباعی شمارهٔ ۳۹
---
# رباعی شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>ای دوست بیا، که بی تو آرامم نیست</p></div>
<div class="m2"><p>در بزم طرب بی‌تو می و جامم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام دل و آرزوی من دیدن توست</p></div>
<div class="m2"><p>جز دیدن روی تو دگر کامم نیست</p></div></div>