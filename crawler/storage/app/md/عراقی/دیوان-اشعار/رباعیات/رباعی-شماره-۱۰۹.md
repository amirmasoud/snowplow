---
title: >-
    رباعی شمارهٔ ۱۰۹
---
# رباعی شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>بگذار، اگر چه رندم و اوباشم</p></div>
<div class="m2"><p>تا خاک سر کوی تو بر سر پاشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار، که بگذرم به کویت نفسی</p></div>
<div class="m2"><p>در عمر مگر یک نفسی خوش باشم</p></div></div>