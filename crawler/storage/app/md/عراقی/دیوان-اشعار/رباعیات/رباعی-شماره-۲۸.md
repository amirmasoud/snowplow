---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>چشمم ز غم عشق تو خون باران است</p></div>
<div class="m2"><p>جان در سر کارت کنم، این بار آن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دوستی تو بر دلم باری نیست</p></div>
<div class="m2"><p>محروم شدم ز خدمتت، بار آن است</p></div></div>