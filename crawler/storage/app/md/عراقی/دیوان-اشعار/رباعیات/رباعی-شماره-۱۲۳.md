---
title: >-
    رباعی شمارهٔ ۱۲۳
---
# رباعی شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>ای نفس خسیس، رو تباهی می‌کن</p></div>
<div class="m2"><p>تا جان خسته است روسیاهی می‌کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون چو امید من فگندی بر خاک</p></div>
<div class="m2"><p>خاکت به سر است، هر چه خواهی می‌کن</p></div></div>