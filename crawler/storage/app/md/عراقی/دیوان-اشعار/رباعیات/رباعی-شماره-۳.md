---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای دوست، به دوستی قرینیم تو را</p></div>
<div class="m2"><p>هر جا که قدم نهی زمینیم تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مذهب عاشقی روا نیست که ما:</p></div>
<div class="m2"><p>عالم به تو بینیم و نبینیم تو را</p></div></div>