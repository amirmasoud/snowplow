---
title: >-
    رباعی شمارهٔ ۶۲
---
# رباعی شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>در سابقه چون قرار عالم دادند</p></div>
<div class="m2"><p>مانا که نه بر مراد آدم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان قاعده و قرار، کان دور افتاد</p></div>
<div class="m2"><p>نی بیش به کس دهند و نی کم دادند</p></div></div>