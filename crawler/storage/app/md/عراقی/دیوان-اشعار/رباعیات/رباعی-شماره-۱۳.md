---
title: >-
    رباعی شمارهٔ ۱۳
---
# رباعی شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ای جملهٔ خلق را ز بالا و ز پست</p></div>
<div class="m2"><p>آورده ز لطف خویش از نیست به هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر درگه عدل تو چه درویش و چه شاه؟</p></div>
<div class="m2"><p>در سایهٔ عفو تو چه هشیار و چه مست؟</p></div></div>