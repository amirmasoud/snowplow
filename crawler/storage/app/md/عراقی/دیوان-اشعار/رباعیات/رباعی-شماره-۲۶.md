---
title: >-
    رباعی شمارهٔ ۲۶
---
# رباعی شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>پرسیدم از آن کسی که برهان دانست:</p></div>
<div class="m2"><p>کان کیست که او حقیقت جان دانست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشاد زبان و گفت: ای آصف رای</p></div>
<div class="m2"><p>این منطق طیر است، سلیمان دانست</p></div></div>