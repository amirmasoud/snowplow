---
title: >-
    رباعی شمارهٔ ۱۰۰
---
# رباعی شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>دل پیشکش نرگس مستت آرم</p></div>
<div class="m2"><p>جان تحفهٔ آن زلف چو شستت آرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرگردانم ز هجر، معلومم نیست</p></div>
<div class="m2"><p>در پای که افتم که به دستت آرم؟</p></div></div>