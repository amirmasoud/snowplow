---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>عیشی نبود چو عیش لولی و گدا</p></div>
<div class="m2"><p>افکنده کله از سر و نعلین ز پا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پا بر سر جان نهاده، دل کرده فدا</p></div>
<div class="m2"><p>بگذاشته از بهر یکی هر دو سرا</p></div></div>