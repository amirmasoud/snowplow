---
title: >-
    رباعی شمارهٔ ۸۵
---
# رباعی شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>بیزار شد از من شکسته همه کس</p></div>
<div class="m2"><p>من مانده‌ام اکنون و همان لطف تو بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد رسی ندارم، ای جان و جهان</p></div>
<div class="m2"><p>در جمله جهان به جز تو، فریادم رس</p></div></div>