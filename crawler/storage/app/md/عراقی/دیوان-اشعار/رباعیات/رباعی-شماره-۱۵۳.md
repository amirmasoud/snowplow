---
title: >-
    رباعی شمارهٔ ۱۵۳
---
# رباعی شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>گر شهره شوی به شهر شرالناسی</p></div>
<div class="m2"><p>ور گوشه گرفته‌ای، تو در وسواسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زان نبود، گر خضر و الیاسی</p></div>
<div class="m2"><p>کس نشناسد تو را، تو کس نشناسی؟</p></div></div>