---
title: >-
    رباعی شمارهٔ ۱۵۴
---
# رباعی شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>چون خاک زمین اگر عناکش باشی</p></div>
<div class="m2"><p>وز باد هوای دهر ناخوش باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار! ز دست ناکسان آب حیات</p></div>
<div class="m2"><p>بر لب ننهی، گرچه در آتش باشی</p></div></div>