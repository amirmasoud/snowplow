---
title: >-
    رباعی شمارهٔ ۱۴۹
---
# رباعی شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>هر لحظه ز چهره آتشی افروزی</p></div>
<div class="m2"><p>تا جان من سوخته‌دل را سوزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دوست نداری تو بدآموزان را</p></div>
<div class="m2"><p>ای نیک، تو این بد ز که می‌آموزی؟</p></div></div>