---
title: >-
    رباعی شمارهٔ ۱۲
---
# رباعی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>عشق تو ز دست ساقیان باده بریخت</p></div>
<div class="m2"><p>وز دیده بسی خون دل ساده بریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس زاهد خرقه پوش سجاده نشین</p></div>
<div class="m2"><p>کز عشق تو می بر سر سجاده بریخت</p></div></div>