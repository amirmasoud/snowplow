---
title: >-
    رباعی شمارهٔ ۲۹
---
# رباعی شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>اول قدم از عشق سر انداختن است</p></div>
<div class="m2"><p>جان باختن است و با بلا ساختن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول این است و آخرش دانی چیست؟</p></div>
<div class="m2"><p>خود را ز خودی خود بپرداختن است</p></div></div>