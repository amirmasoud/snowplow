---
title: >-
    رباعی شمارهٔ ۱۳۳
---
# رباعی شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>چندن که خم باده‌پرست است بده</p></div>
<div class="m2"><p>چندان که در توبه نبسته است بده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا این قفس جسم مرا طوطی عمر</p></div>
<div class="m2"><p>در هم نشکسته است و نجسته است بده</p></div></div>