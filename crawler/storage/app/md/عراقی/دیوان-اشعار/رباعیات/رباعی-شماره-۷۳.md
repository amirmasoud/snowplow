---
title: >-
    رباعی شمارهٔ ۷۳
---
# رباعی شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>حاشا! که دل از خاک درت دور شود</p></div>
<div class="m2"><p>یا جان ز سر کوی تو مهجور شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دیدهٔ تاریک من آخر روزی</p></div>
<div class="m2"><p>از خاک قدم‌های تو پر نور شود</p></div></div>