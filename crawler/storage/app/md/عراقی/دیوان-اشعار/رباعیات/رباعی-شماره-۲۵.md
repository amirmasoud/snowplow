---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>این دورهٔ سالوس، که نتوان دانست</p></div>
<div class="m2"><p>می‌باش به ناموس، که نتوان دانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکی شو و کبر را ز خود بیرون کن</p></div>
<div class="m2"><p>پای همه می‌بوس، که نتوان دانست</p></div></div>