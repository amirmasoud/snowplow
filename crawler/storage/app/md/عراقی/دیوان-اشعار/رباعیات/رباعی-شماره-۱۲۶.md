---
title: >-
    رباعی شمارهٔ ۱۲۶
---
# رباعی شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ای دل، پس زنجیر تو دیوانه نشین</p></div>
<div class="m2"><p>در دامن درد خویش مردانه نشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آمد شد بیهوده تو خود را پی کن</p></div>
<div class="m2"><p>معشوق چو خانگی است در خانه نشین</p></div></div>