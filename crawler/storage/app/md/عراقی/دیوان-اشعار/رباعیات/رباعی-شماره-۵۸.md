---
title: >-
    رباعی شمارهٔ ۵۸
---
# رباعی شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>آنجا که تویی عقل کجا در تو رسد؟</p></div>
<div class="m2"><p>خود زشت بود که عقل ما در تو رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند: ثنای هر کسی برتر ازوست</p></div>
<div class="m2"><p>تو برتر از آنی که ثنا در تو رسد</p></div></div>