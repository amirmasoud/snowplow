---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>فرزند عزیز، قرةالعین کبیر</p></div>
<div class="m2"><p>بادات خدا در همه احوال نصیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپذیر به یادگار این نسخه ز من</p></div>
<div class="m2"><p>میکن نظری درو ولی یاد بگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌خواست پدر که با تو باشد همه عمر</p></div>
<div class="m2"><p>اما چه توان کرد؟ چنین بد تقدیر</p></div></div>