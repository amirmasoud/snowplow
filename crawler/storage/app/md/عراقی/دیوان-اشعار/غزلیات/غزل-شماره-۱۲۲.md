---
title: >-
    غزل شمارهٔ ۱۲۲
---
# غزل شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>نظر ز حال من ناتوان دریغ مدار</p></div>
<div class="m2"><p>نظارهٔ رخت از عاشقان دریغ مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر سزای جمال تو نیست دیده رواست</p></div>
<div class="m2"><p>خیال روی تو باری ز جان دریغ مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پرسش من رنجور اگر نمی‌آیی</p></div>
<div class="m2"><p>عنایتی ز من ناتوان دریغ مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خوان وصل تو چون قانعم به دیداری</p></div>
<div class="m2"><p>تو نیز این قدر از میهمان دریغ مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من، که گرد درت چون سگان همی گردم</p></div>
<div class="m2"><p>نواله گر ندهی، استخوان دریغ مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دوستان را بر تخت وصل بنشانی</p></div>
<div class="m2"><p>ز من، که خاک توام، آستان دریغ مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو با ندیمان جام شراب نوش کنی</p></div>
<div class="m2"><p>نصیب جرعه‌ای از خاکیان دریغ مدار</p></div></div>