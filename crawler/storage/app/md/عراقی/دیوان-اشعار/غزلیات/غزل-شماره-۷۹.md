---
title: >-
    غزل شمارهٔ ۷۹
---
# غزل شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>بدین زبان صفت حسن یار نتوان کرد</p></div>
<div class="m2"><p>به طعمهٔ پشه عنقا شکار نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گفتگو سخن عشق دوست نتوان گفت</p></div>
<div class="m2"><p>به جست و جو طلب وصل یار نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان مخسب که در خواب روی او بینی</p></div>
<div class="m2"><p>خیال او بود آن، اعتبار نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو چشم تو، خود اگر عاشقی، پر آب بود</p></div>
<div class="m2"><p>بر آب نقش لطیف نگار نتوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چشم او رخ او بین، به دیدهٔ خفاش</p></div>
<div class="m2"><p>به آفتاب نظر آشکار نتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چشم نرگس کوته‌نظر به وقت بهار</p></div>
<div class="m2"><p>نظارهٔ چمن و لاله‌زار نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدم که بوسه زنم بر درش ادب گفتا</p></div>
<div class="m2"><p>به بوسه خاک در یار خوار نتوان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نیم جان که تو داری و یک نفس که تو راست</p></div>
<div class="m2"><p>حدیث پیشکشش زینهار نتوان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه به که پیش سگان درش فشانی جان</p></div>
<div class="m2"><p>که این متاع بر آن رخ نثار نتوان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلا به پیش خیالش شبی همی گفتم</p></div>
<div class="m2"><p>که : دشمنی همه با دوستدار نتوان کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگوی تا نکند زلف تو پریشانی</p></div>
<div class="m2"><p>که بیش ازین دل ما بی‌قرار نتوان کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تیغ غمزهٔ خون خوار، جان مجروحم</p></div>
<div class="m2"><p>هزار بار، به روزی فگار نتوان کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلی که با غم عشق تو در میان آمد</p></div>
<div class="m2"><p>بهر گنه ز کنارش کنار نتوان کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان که نام وصال تو می‌برم روزی</p></div>
<div class="m2"><p>به دست هجر مرا جان سپار نتوان کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جواب داد خیالش که، با سلیمانی</p></div>
<div class="m2"><p>برای مورچه‌ای کارزار نتوان کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میان هجر و وصالش، گر اختیار دهند</p></div>
<div class="m2"><p>ز هر دو هیچ یکی اختیار نتوان کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رموز عشق، عراقی، مگو چنین روشن</p></div>
<div class="m2"><p>که راز خویش چنین آشکار نتوان کرد</p></div></div>