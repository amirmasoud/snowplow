---
title: >-
    غزل شمارهٔ ۲۱
---
# غزل شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>مهر مهر دلبری بر جان ماست</p></div>
<div class="m2"><p>جان ما در حضرت جانان ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش او از درد می‌نالم ولیک</p></div>
<div class="m2"><p>درد آن دلدار ما درمان ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس عجب نبود که سودایی شوم</p></div>
<div class="m2"><p>کیت سودای او در شان ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان ما چوگان و دل سودایی است</p></div>
<div class="m2"><p>گوی زلفش در خم چوگان ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسب همت را چو در زین آوریم</p></div>
<div class="m2"><p>هر دو عالم گوشهٔ میدان ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود این چنین زار و نزار</p></div>
<div class="m2"><p>بر بساط معرفت جولان ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وزن می‌ننهندمان خلقان ولیک</p></div>
<div class="m2"><p>کس چه داند آنچه در خلقان ماست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ز ما برهان طلب دارد کسی</p></div>
<div class="m2"><p>نور او در جان ما برهان ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنت پر انگبین و شیر و می</p></div>
<div class="m2"><p>بی‌جمال دوست شورستان ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه در صورت گدایی می‌کنیم</p></div>
<div class="m2"><p>گنج معنی در دل ویران ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هاتف دولت مرا آواز داد:</p></div>
<div class="m2"><p>کین نوامی گو: عراقی، ز آن ماست</p></div></div>