---
title: >-
    غزل شمارهٔ ۲۹۰
---
# غزل شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>پسرا، ره قلندر سزد ار به من نمایی</p></div>
<div class="m2"><p>که دراز و دور دیدم ره زهد و پارسایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسرا، می مغانه دهی ار حریف مایی</p></div>
<div class="m2"><p>که نماند بیش ما را سر زهد و پارسایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدحی می مغانه به من آر، تا بنوشم</p></div>
<div class="m2"><p>که دگر نماند ما را سر توبهٔ ریایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می صاف اگر نباشد، به من آر درد تیره</p></div>
<div class="m2"><p>که ز درد تیره یابد دل و دیده روشنایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کم خانقه گرفتم، سر مصلحی ندارم</p></div>
<div class="m2"><p>قدح شراب پر کن، به من آر، چند پایی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه ره و نه رسم دارم، نه دل و نه دین، نه دنیی</p></div>
<div class="m2"><p>منم و حریف و کنجی و نوای بی‌نوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیم اهل زهد و توبه به من آر ساغر می</p></div>
<div class="m2"><p>که به صدق توبه کردم ز عبادت ریایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو مرا شراب در ده، که ز زهد تو به کردم</p></div>
<div class="m2"><p>ز صلاح چون ندیدم جز لاف و خودنمایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز غم زمانه ما را برهان ز می زمانی</p></div>
<div class="m2"><p>که نیافت جز به می کس ز غم زمان رهایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو ز باده مست گشتم، چه کلیسیا، چه کعبه؟</p></div>
<div class="m2"><p>چو به ترک خود بگفتم، چه وصال و چه جدایی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به قمارخانه رفتم همه پاکباز دیدم</p></div>
<div class="m2"><p>چو به صومعه رسیدم همه یافتم دغایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو شکست توبهٔ من، مشکن تو عهد، باری</p></div>
<div class="m2"><p>به من شکسته دل گو که: چگونه‌ای؟ کجایی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به طواف کعبه رفتم به حرم رهم ندادند</p></div>
<div class="m2"><p>که برون در چه کردی، که درون خانه آیی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دیر می‌زدم من، ز درون صدا بر آمد</p></div>
<div class="m2"><p>که: درآی، ای عراقی، که تو خود حریف مایی</p></div></div>