---
title: >-
    غزل شمارهٔ ۱۲۶
---
# غزل شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>سر به سر از لطف جانی ای پسر</p></div>
<div class="m2"><p>خوشتر از جان چیست؟ آنی ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل دل‌ها جمله سوی روی توست</p></div>
<div class="m2"><p>رو که شیرین دلستانی ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان به چشم من درآیی هر زمان</p></div>
<div class="m2"><p>کز صفا آب روانی ای پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از می حسن ار چه سرمستی، مکن</p></div>
<div class="m2"><p>با حریفان سرگرانی ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وعده ای می ده، اگر چه کج بود</p></div>
<div class="m2"><p>کز بهانه درنمانی ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر لب خود بوسه زن، آنگه ببین</p></div>
<div class="m2"><p>ذوق آب زندگانی ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان شدم خاک درت کز جام خود</p></div>
<div class="m2"><p>جرعه‌ای بر من فشانی ای پسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از لطیفی می‌نماند کس به تو</p></div>
<div class="m2"><p>زان یقینم شد که جانی ای پسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش جان‌ها پر گهر در حضرتت</p></div>
<div class="m2"><p>کز سخن در می‌چکانی ای پسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل و چشمم، ز حسن و لطف خویش</p></div>
<div class="m2"><p>آشکارا و نهانی ای پسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست در عالم عراقی را دمی</p></div>
<div class="m2"><p>بی لب تو زندگانی ای پسر</p></div></div>