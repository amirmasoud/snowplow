---
title: >-
    غزل شمارهٔ ۲۲۶
---
# غزل شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>ای دل و جان عاشقان شیفتهٔ جمال تو</p></div>
<div class="m2"><p>هوش و روان بی‌دلان سوختهٔ جلال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام دل شکستگان دیدن توست هر زمان</p></div>
<div class="m2"><p>راحت جان خستگان یافتن وصال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست تهی به درگهت آمده‌ام امیدوار</p></div>
<div class="m2"><p>روی نهاده بر درت منتظر نوال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود به دو چشم من شبی خواب گذر نمی‌کند</p></div>
<div class="m2"><p>ورنه به خواب دیدمی، بو که شبی وصال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به غم تو قانعم، شاد به درد تو، از آنک</p></div>
<div class="m2"><p>چیره بود به خون من دولت اتصال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو به جمال شادمان، بی‌خبر از غمم دریغ!</p></div>
<div class="m2"><p>من شده پایمال غم، از غم گوشمال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناز ز حد بدر مبر، باز نگر که: در خور است</p></div>
<div class="m2"><p>ناز تو را نیاز من، چشم مرا جمال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسکه کشید ناز تو، مرد عراقی، ای دریغ!</p></div>
<div class="m2"><p>چند کشد، تو خود بگو، خسته دلی دلال تو؟</p></div></div>