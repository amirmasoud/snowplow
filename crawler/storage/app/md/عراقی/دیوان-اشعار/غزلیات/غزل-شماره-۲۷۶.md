---
title: >-
    غزل شمارهٔ ۲۷۶
---
# غزل شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>گر به رخسار تو، ای دوست، نظر داشتمی</p></div>
<div class="m2"><p>نظر از روی خوشت بهر چه برداشتمی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من بی‌خبر از دوست دهندم خبری</p></div>
<div class="m2"><p>باری، از بی‌خبری کاش خبر داشتمی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان آمدمی چون سر زلفت با تو</p></div>
<div class="m2"><p>از سر زلف تو گر هیچ کمر داشتمی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ندادی جگرم وعدهٔ وصلت هر دم</p></div>
<div class="m2"><p>کی دل و دیده پر از خون جگر داشتمی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتیم: صبر کن، از صبر برآید کارت</p></div>
<div class="m2"><p>کردمی صبر ز روی تو، اگر داشتمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود کجا آمدی اندر نظرم آب روان؟</p></div>
<div class="m2"><p>گر ز خاک در تو کحل بصر داشتمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گم گشتهٔ خود بار دگر یافتمی</p></div>
<div class="m2"><p>بر سر کوی تو گر هیچ گذر داشتمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ز روی و لب تو هیچ نصیبم بودی</p></div>
<div class="m2"><p>بهر بیماری دل گل بشکر داشتمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کردمی بر سر کویت گهرافشانی‌ها</p></div>
<div class="m2"><p>بجز از اشک اگر هیچ گهر داشتمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر عراقی نشدی پردهٔ روی نظرم</p></div>
<div class="m2"><p>به رخ خوب تو هر لحظه نظر داشتمی</p></div></div>