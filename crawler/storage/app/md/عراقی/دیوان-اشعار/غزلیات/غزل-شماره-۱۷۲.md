---
title: >-
    غزل شمارهٔ ۱۷۲
---
# غزل شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>چه خوش بودی، دریغا، روزگارم؟</p></div>
<div class="m2"><p>اگر با من خوشستی غمگسارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آب دیده دست از خود بشویم</p></div>
<div class="m2"><p>کنون کز دست بیرون شد نگارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگارا، بر تو نگزینم کسی را</p></div>
<div class="m2"><p>تویی از جمله خوبان اختیارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا جانی، که می‌دارم تو را دوست</p></div>
<div class="m2"><p>عجب نبود که جان را دوست دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا تا کار با زلف تو باشد</p></div>
<div class="m2"><p>پریشان‌تر ز زلف توست کارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا کرامگه زلف تو باشد</p></div>
<div class="m2"><p>ببین چون باشد آرام و قرارم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بوی آنکه دامان تو گیرم</p></div>
<div class="m2"><p>نشسته بر سر ره چون غبارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آویزم به دامان تو یک شب</p></div>
<div class="m2"><p>مگر روزی سر از جیبت برآرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عراقی، دامن او گیر و خوش باش</p></div>
<div class="m2"><p>که من با تو درین اندیشه یارم</p></div></div>