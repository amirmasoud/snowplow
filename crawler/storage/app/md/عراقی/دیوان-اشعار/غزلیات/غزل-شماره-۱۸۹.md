---
title: >-
    غزل شمارهٔ ۱۸۹
---
# غزل شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>ما دگرباره توبه بشکستیم</p></div>
<div class="m2"><p>وز غم نام و ننگ وارستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرقهٔ صوفیانه بدریدیم</p></div>
<div class="m2"><p>کمر عاشقانه بر بستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات با می و معشوق</p></div>
<div class="m2"><p>نفسی عاشقانه بنشستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از می لعل یار سرمستیم</p></div>
<div class="m2"><p>وز دو چشمش خمار بشکستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاید ار شور در جهان فکنیم</p></div>
<div class="m2"><p>کز می لعل یار سر مستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بدیدیم آفتاب رخش</p></div>
<div class="m2"><p>از طرب، ذره‌وار، بر جستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنگ در دامن شعاع زدیم</p></div>
<div class="m2"><p>تا بدان آفتاب پیوستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذره بودیم، آفتاب شدیم</p></div>
<div class="m2"><p>از عراقی چو مهر بگسستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همه هست، خود نمی‌دانیم</p></div>
<div class="m2"><p>کین زمان نیستیم یا هستیم؟</p></div></div>