---
title: >-
    غزل شمارهٔ ۸۵
---
# غزل شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>دیدهٔ بختم، دریغا کور شد</p></div>
<div class="m2"><p>دل نمرده، زنده اندر گور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست گیر ای دوست این بخت مرا</p></div>
<div class="m2"><p>تا نبیند دشمنم کو کور شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بارگاه دل، که بودی جای تو</p></div>
<div class="m2"><p>بنگر اکنون جای مار و مور شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌لب شیرینت عمرم تلخ گشت</p></div>
<div class="m2"><p>شوربختی بین که: عیشم شور شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل قوی بودم به امید تو، لیک</p></div>
<div class="m2"><p>دل ندادی، خسته زان بی‌نور شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شور عشقت تا فتاد اندر جهان</p></div>
<div class="m2"><p>چون دل من عالمی پر شور شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارت آمد از عراقی، لاجرم</p></div>
<div class="m2"><p>بی‌تو، مسکین، بی‌نوا و عور شد</p></div></div>