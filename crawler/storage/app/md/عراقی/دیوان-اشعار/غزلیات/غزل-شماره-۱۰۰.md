---
title: >-
    غزل شمارهٔ ۱۰۰
---
# غزل شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>اگر شکسته دلانت هزار جان دارند</p></div>
<div class="m2"><p>به خدمت تو کمر بسته بر میان دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدند حلقه به گوش تو را چو حلقه به گوش</p></div>
<div class="m2"><p>چه خوش دلند که مثل تو دلستان دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسان که وصل تو یک دم به نقد یافته‌اند</p></div>
<div class="m2"><p>از ین طلب طرب و عیش جاودان دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بگذری به تعجب تو ماهروی به راه</p></div>
<div class="m2"><p>چو ماه ماهرخان دست بر دهان دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد از آن ز ره زلف تو پناه گرفت</p></div>
<div class="m2"><p>که چشم و ابروی تو تیر در کمان دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجاهدان رهت تا عنایت تو بود</p></div>
<div class="m2"><p>چه بیم و باک به عالم ازین و آن دارند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آب دیده و تاب دل است غمازی</p></div>
<div class="m2"><p>وگرنه راز تو بیچارگان نهان دارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلام غمزهٔ بیمارتم که از هوسش</p></div>
<div class="m2"><p>چه تندرستان خود را ناتوان دارند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر کسی به شکایت بود ز دلبر خویش</p></div>
<div class="m2"><p>ز تو عراقی و دل شکر بی‌کران دارند</p></div></div>