---
title: >-
    غزل شمارهٔ ۲۷۴
---
# غزل شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>از غم دلدار زارم، مرگ به زین زندگی</p></div>
<div class="m2"><p>وز فراقش دل فگارم، مرگ به زین زندگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش بر من ناخوش است و زندگانی نیک تلخ</p></div>
<div class="m2"><p>بی لب شیرین یارم، مرگ به زین زندگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زندگی بی‌روی خوبش بدتر است از مردگی</p></div>
<div class="m2"><p>مرگ کو تا جان سپارم؟ مرگ به زین زندگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کسی دارد ز خود آسایشی، دردا! که من</p></div>
<div class="m2"><p>راحتی از خود ندارم، مرگ به زین زندگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاشکی دیدی من مسکین چگونه در غمش</p></div>
<div class="m2"><p>عمر ناخوش می‌گذارم، مرگ به زین زندگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دمی صد بار از تن می برآید جان من</p></div>
<div class="m2"><p>وز غم دل بی‌قرارم، مرگ به زین زندگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار من جان کندن است و ناله و زاری و درد</p></div>
<div class="m2"><p>بنگرید آخر به کارم، مرگ به زین زندگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در چنین جان کندنی کافتاده‌ام، شاید که من</p></div>
<div class="m2"><p>نعره‌ها از جان برآرم، مرگ به زین زندگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ کس دیدی که خواهد در دمی صدبار مرگ؟</p></div>
<div class="m2"><p>مرگ را من خواستارم، مرگ به زین زندگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پی آن کز عراقی مرگ بستاند مرا</p></div>
<div class="m2"><p>مرگ را من دوستدارم، مرگ به زین زندگی</p></div></div>