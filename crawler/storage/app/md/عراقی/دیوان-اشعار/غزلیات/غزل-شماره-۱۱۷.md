---
title: >-
    غزل شمارهٔ ۱۱۷
---
# غزل شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>مرا درد تو درمان می‌نماید</p></div>
<div class="m2"><p>غم تو مرهم جان می‌نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا، کز جام عشقت مست باشم</p></div>
<div class="m2"><p>وصال و هجر یکسان می‌نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو من تن در بلای عشق دادم</p></div>
<div class="m2"><p>همه دشوارم آسان می‌نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان من غم تو، شادمان باد،</p></div>
<div class="m2"><p>هر آن لطفی که بتوان می‌نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر یک لحظه ننماید مرا سوز</p></div>
<div class="m2"><p>دگر لحظه دو چندان می‌نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم با اینهمه انده، ز شادی</p></div>
<div class="m2"><p>بهار و باغ و بستان می‌نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیالت آشکارا می‌برد دل</p></div>
<div class="m2"><p>اگر روی تو پنهان می‌نماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب لعل تو جانم می‌نوازد</p></div>
<div class="m2"><p>بنفشه آب حیوان می‌نماید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندانم تا چه خواهد فتنه انگیخت؟</p></div>
<div class="m2"><p>که زلفش بس پریشان می‌نماید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دوران تو زان تنگ است دل‌ها</p></div>
<div class="m2"><p>که حسن تو فراوان می‌نماید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ذره در هوای مهر رویت</p></div>
<div class="m2"><p>عراقی نیک حیران می‌نماید</p></div></div>