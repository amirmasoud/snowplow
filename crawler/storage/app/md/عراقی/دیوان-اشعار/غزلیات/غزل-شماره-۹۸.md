---
title: >-
    غزل شمارهٔ ۹۸
---
# غزل شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>نخستین باده کاندر جام کردند</p></div>
<div class="m2"><p>ز چشم مست ساقی وام کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو باخود یافتند اهل طرب را</p></div>
<div class="m2"><p>شراب بیخودی در جام کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب میگون جانان جام در داد</p></div>
<div class="m2"><p>شراب عاشقانش نام کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر صید دل‌های جهانی</p></div>
<div class="m2"><p>کمند زلف خوبان دام کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گیتی هر کجا درد دلی بود</p></div>
<div class="m2"><p>به هم کردند و عشقش نام کردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر زلف بتان آرام نگرفت</p></div>
<div class="m2"><p>ز بس دل‌ها که بی‌آرام کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو گوی حسن در میدان فکندند</p></div>
<div class="m2"><p>به یک جولان دو عالم رام کردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بهر نقل مستان از لب و چشم</p></div>
<div class="m2"><p>مهیا پسته و بادام کردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن لب، کز در صد آفرین است</p></div>
<div class="m2"><p>نصیب بی‌دلان دشنام کردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به مجلس نیک و بد را جای دادند</p></div>
<div class="m2"><p>به جامی کار خاص و عام کردند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به غمزه صد سخن با جان بگفتند</p></div>
<div class="m2"><p>به دل ز ابرو دو صد پیغام کردند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمال خویشتن را جلوه دادند</p></div>
<div class="m2"><p>به یک جلوه دو عالم رام کردند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلی را تا به دست آرند، هر دم</p></div>
<div class="m2"><p>سر زلفین خود را دام کردند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نهان با محرمی رازی بگفتند</p></div>
<div class="m2"><p>جهانی را از آن اعلام کردند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو خود کردند راز خویشتن فاش</p></div>
<div class="m2"><p>عراقی را چرا بدنام کردند؟</p></div></div>