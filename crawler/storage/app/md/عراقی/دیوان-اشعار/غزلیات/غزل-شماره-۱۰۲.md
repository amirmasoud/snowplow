---
title: >-
    غزل شمارهٔ ۱۰۲
---
# غزل شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>باز دلم عیش و طرب می‌کند</p></div>
<div class="m2"><p>هیچ ندانم چه سبب می‌کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از می عشق تو مگر مست شد</p></div>
<div class="m2"><p>کین همه شادی و طرب می‌کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سر زلف تو پریشان بدید</p></div>
<div class="m2"><p>شیفته شد ، شور و شغب می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دل من در سر زلف تو شد</p></div>
<div class="m2"><p>عیش همه در دل شب می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برد به بازی دل جمله جهان</p></div>
<div class="m2"><p>زلف تو بازی چه عجب می‌کند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرهٔ طرار تو کرد آن چه کرد</p></div>
<div class="m2"><p>فتنه نگر باز که لب می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌برد از من دل و گوید به طنز:</p></div>
<div class="m2"><p>باز فلانی چه طلب می‌کند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از لب لعلش چه عجب گر مرا</p></div>
<div class="m2"><p>آرزوی قند و طرب می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر طلبد بوسه، عراقی مرنج،</p></div>
<div class="m2"><p>گرچه همه ترک ادب می‌کند</p></div></div>