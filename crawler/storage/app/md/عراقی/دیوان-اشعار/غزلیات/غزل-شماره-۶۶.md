---
title: >-
    غزل شمارهٔ ۶۶
---
# غزل شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>راحت سر مردمی ندارد</p></div>
<div class="m2"><p>دولت دل همدمی ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز احسان زمانه دیده بردوز</p></div>
<div class="m2"><p>کو دیدهٔ مردمی ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خوان فلک نواله کم پیچ</p></div>
<div class="m2"><p>کو گردهٔ گندمی ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با درد بساز، از آنکه درمان</p></div>
<div class="m2"><p>با جان تو محرمی ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تار حیات دل چه بندی؟</p></div>
<div class="m2"><p>چون پود تو محکمی ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردا! که درین سرای پر غم</p></div>
<div class="m2"><p>کس دولت بی‌غمی ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارد همه چیز آدمی زاد</p></div>
<div class="m2"><p>افسوس که خرمی ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خوشدلیی درین جهان هست</p></div>
<div class="m2"><p>باری دل آدمی ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنمای به من دلی فراهم</p></div>
<div class="m2"><p>کو محنت درهمی ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کم خور غم این جهان، عراقی،</p></div>
<div class="m2"><p>زیرا که غمش کمی ندارد</p></div></div>