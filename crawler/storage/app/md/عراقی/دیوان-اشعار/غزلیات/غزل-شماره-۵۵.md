---
title: >-
    غزل شمارهٔ ۵۵
---
# غزل شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>با عشق تو ناز در نگنجد</p></div>
<div class="m2"><p>جز درد و نیاز در نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با درد تو درد در نیاید</p></div>
<div class="m2"><p>با سوز تو ساز در نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیچاره کسی که از در تو</p></div>
<div class="m2"><p>دور افتد و باز در نگنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با داغ غمت درون سینه</p></div>
<div class="m2"><p>جز سوز و گداز در نگنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عشق حقیقتی به هر حال</p></div>
<div class="m2"><p>سودای مجاز در نگنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میکده با حریف قلاش</p></div>
<div class="m2"><p>تسبیح و نماز در نگنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جلوه‌گه جمال حسنت</p></div>
<div class="m2"><p>خوبی ایاز در نگنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با یاد لب تو در خیالم</p></div>
<div class="m2"><p>اندیشهٔ گاز در نگنجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنجا که رود حدیث وصلت</p></div>
<div class="m2"><p>یک محرم راز در نگنجد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وآندم که حدیث زلفت افتد</p></div>
<div class="m2"><p>جز شرح دراز در نگنجد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه ناز کنی عراقی اینجا؟</p></div>
<div class="m2"><p>جان باز، که ناز در نگنجد</p></div></div>