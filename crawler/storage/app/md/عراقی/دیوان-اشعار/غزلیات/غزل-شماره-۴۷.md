---
title: >-
    غزل شمارهٔ ۴۷
---
# غزل شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>عشق شوقی در نهاد ما نهاد</p></div>
<div class="m2"><p>جان ما را در کف غوغا نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه‌ای انگیخت، شوری درفکند</p></div>
<div class="m2"><p>در سرا و شهر ما چون پا نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای خالی یافت از غوغا و شور</p></div>
<div class="m2"><p>شور و غوغا کرد و رخت آنجا نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام و ننگ ما همه بر باد داد</p></div>
<div class="m2"><p>نام ما دیوانه و رسوا نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عراقی را، درین ره، خام یافت</p></div>
<div class="m2"><p>جان ما بر آتش سودا نهاد</p></div></div>