---
title: >-
    غزل شمارهٔ ۲۴۰
---
# غزل شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>بازم از غصه جگر خون کرده‌ای</p></div>
<div class="m2"><p>چشمم از خونابه جیحون کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارم از محنت به جان آورده‌ای</p></div>
<div class="m2"><p>جانم از تیمار و غم خون کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود همیشه کرده‌ای بر من ستم</p></div>
<div class="m2"><p>آن نه بیدادی است کاکنون کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیبد ار خاک درت بر سر کنم</p></div>
<div class="m2"><p>کز سرایم خوار بیرون کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از من مسکین چه پرسی حال من؟</p></div>
<div class="m2"><p>حالم از خود پرس: تا چون کرده‌ای؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان بهر دل مجروح من</p></div>
<div class="m2"><p>مرهمی از درد معجون کرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نگریم زار؟ چون دانم که تو</p></div>
<div class="m2"><p>با عراقی دل دگرگون کرده‌ای</p></div></div>