---
title: >-
    غزل شمارهٔ ۲۸
---
# غزل شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>طرهٔ یار پریشان چه خوش است</p></div>
<div class="m2"><p>قامت دوست خرامان چه خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط خوش بر لب جانان چه نکوست</p></div>
<div class="m2"><p>سبزه و چشمهٔ حیوان چه خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از می عشق دلی مست و خراب</p></div>
<div class="m2"><p>همچو چشم خوش جانان چه خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات خراب افتاده</p></div>
<div class="m2"><p>عاشق بی سر و سامان چه خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دل شیفتهٔ ما بنگر</p></div>
<div class="m2"><p>در خم زلف پریشان چه خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف گم شدهٔ ما را بین</p></div>
<div class="m2"><p>کاندر آن چاه زنخدان چه خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لذت عشق بتم از من پرس</p></div>
<div class="m2"><p>تو از آن بی‌خبری کان چه خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو چه دانی که شکر خندهٔ او</p></div>
<div class="m2"><p>از دهان شکرستان چه خوش است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه شناسی که می و نقل بهم</p></div>
<div class="m2"><p>از لب آن بت خندان چه خوش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ببینی که به وقت مستی</p></div>
<div class="m2"><p>لب من بر لب جانان چه خوش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یار ساقی و عراقی باقی</p></div>
<div class="m2"><p>وه که این عیش بدینسان چه خوش است</p></div></div>