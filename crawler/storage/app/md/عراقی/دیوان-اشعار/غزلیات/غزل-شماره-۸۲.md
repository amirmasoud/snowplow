---
title: >-
    غزل شمارهٔ ۸۲
---
# غزل شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>اگر یکبار زلف یار از رخسار برخیزد</p></div>
<div class="m2"><p>هزاران آه مشتاقان ز هر سو زار برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر غمزه‌اش کمین سازد دل از جان دست بفشاند</p></div>
<div class="m2"><p>وگر زلفش برآشوبد ز جان زنهار برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رویش پرده بگشاید که و صحرا به رقص آید</p></div>
<div class="m2"><p>چو عشقش روی بنماید خرد ناچار برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا گر از سر زلفش به گورستان برد بویی</p></div>
<div class="m2"><p>ز هر گوری دو صد بی‌دل ز بوی یار برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیم زلفش ار ناگه به ترکستان گذر سازد</p></div>
<div class="m2"><p>هزاران عاشق از سقسین و از بلغار برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای مطرب عشقش اگر در گوش جان آید</p></div>
<div class="m2"><p>ز کویش دست بفشاند قلندروار برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو یاد او شود مونس ز جان اندوه بنشیند</p></div>
<div class="m2"><p>چو اندوهش شود غم خور ز دل تیمار برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا بی‌عشق او منشین ز جان برخیز و سر در باز</p></div>
<div class="m2"><p>چو عیاران مکن کاری که گرد از کار برخیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین دریا فگن خود را مگر دری به دست آری</p></div>
<div class="m2"><p>کزین دریای بی‌پایان گهر بسیار برخیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر موجیت برباید، زهی دولت، تو را آن به</p></div>
<div class="m2"><p>که عالم پیش قدر تو چو خدمتکار برخیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حجاب ره تویی برخیز و در فتراک عشق آویز</p></div>
<div class="m2"><p>که بی‌عشق آن حجاب تو ز ره دشوار برخیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عراقی، هر سحرگاهی بر آر از سوز دل آهی</p></div>
<div class="m2"><p>ز خواب این دیدهٔ بختت مگر یکبار برخیزد</p></div></div>