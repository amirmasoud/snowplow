---
title: >-
    غزل شمارهٔ ۷۰
---
# غزل شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>بیا بیا، که نسیم بهار می‌گذرد</p></div>
<div class="m2"><p>بیا، که گل ز رخت شرمسار می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا، که وقت بهار است و موسم شادی</p></div>
<div class="m2"><p>مدار منتظرم، وقت کار می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز راه لطف به صحرا خرام یک نفسی</p></div>
<div class="m2"><p>که عیش تازه کنم، چون بهار می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم لطف تو از کوی می‌برد هر دم</p></div>
<div class="m2"><p>غمی که بر دل این جان فگار می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جام وصل تو ناخورده جرعه‌ای دل من</p></div>
<div class="m2"><p>ز بزم عیش تو در سر خمار می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سحرگهی که به کوی دلم گذر کردی</p></div>
<div class="m2"><p>به دیده گفت دلم: کان شکار می‌گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دیده کرد نظر صدهزار عاشق دید</p></div>
<div class="m2"><p>که نعره می‌زد هر یک که: یار می‌گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گوش جان عراقی رسید آن زاری</p></div>
<div class="m2"><p>از آن ز کوی تو زار و نزار می‌گذرد</p></div></div>