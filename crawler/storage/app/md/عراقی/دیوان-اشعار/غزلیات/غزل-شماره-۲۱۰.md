---
title: >-
    غزل شمارهٔ ۲۱۰
---
# غزل شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>در کف جور تو افتادم، تو دان</p></div>
<div class="m2"><p>تن به هجران تو در دادم، تو دان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الغیاث، ای دوست، کز دست جفات</p></div>
<div class="m2"><p>در کف صد گونه بیدادم، تو دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر امید آنکه بینم روی تو</p></div>
<div class="m2"><p>لب ببستم، دیده بگشادم، تو دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل، که از دیدار تو محروم ماند</p></div>
<div class="m2"><p>بر در لطفت فرستادم، تو دان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سالها جستم، ندیدم روی تو</p></div>
<div class="m2"><p>از طلب اکنون به استادم، تو دان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نیم نومید ز امید بهی</p></div>
<div class="m2"><p>بر در امیدت افتادم، تو دان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کسی حالم نداند، گو: مدان</p></div>
<div class="m2"><p>از همه عالم چو آزادم، تو دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌گدازد تابش هجرت مرا</p></div>
<div class="m2"><p>بر یخ است ای دوست، بنیادم، تو دان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ز نام من همی ننگ آیدت</p></div>
<div class="m2"><p>خود مبر نامم، که من بادم، تو دان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور همی دانی که شادم ز اندهت</p></div>
<div class="m2"><p>هم به اندوهی بکن شادم، تو دان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند نالم، چون عراقی، در غمت؟</p></div>
<div class="m2"><p>روز و شب در سوز و فریادم، تو دان</p></div></div>