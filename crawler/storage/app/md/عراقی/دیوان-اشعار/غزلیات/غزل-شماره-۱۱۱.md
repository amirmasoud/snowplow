---
title: >-
    غزل شمارهٔ ۱۱۱
---
# غزل شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>مرا، گرچه ز غم جان می‌برآید</p></div>
<div class="m2"><p>غم عشقت ز جانم خوشتر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین تیمار گر یک دم غم تو</p></div>
<div class="m2"><p>نپرسد حال من، جانم برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا شادی گهی باشد درین غم</p></div>
<div class="m2"><p>که اندوه توام از در در آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا یک ذره اندوه تو خوشتر</p></div>
<div class="m2"><p>که یک عالم پر از سیم و زر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه هر کسی از غم گریزد</p></div>
<div class="m2"><p>مرا چون جان ، غم تو درخور آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا در سینه تاب انده تو</p></div>
<div class="m2"><p>بسی خوشتر ز آب کوثر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سر در پای اندوه تو افکند</p></div>
<div class="m2"><p>عراقی در دو عالم بر سر آید</p></div></div>