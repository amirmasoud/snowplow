---
title: >-
    غزل شمارهٔ ۱۳۶
---
# غزل شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>بی‌جمال تو، ای جهان افروز</p></div>
<div class="m2"><p>چشم عشاق، تیره بیند روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به ایوان عشق بار نیافت</p></div>
<div class="m2"><p>تا به کلی ز خود نکرد بروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بیابان عشق پی نبرد</p></div>
<div class="m2"><p>خانه پرورد لایجوز و یجوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بلا بود کان به من نرسید؟</p></div>
<div class="m2"><p>زین دل جانگداز درداندوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق گوید مرا که: ای طالب</p></div>
<div class="m2"><p>چاک زن طیلسان و خرقه بسوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر از فهم خویش قصه مخوان</p></div>
<div class="m2"><p>قصه خواهی؟ بیا ز ما آموز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنشان، ای عراقی، آتش خویش</p></div>
<div class="m2"><p>پس چراغی ز عشق ما افروز</p></div></div>