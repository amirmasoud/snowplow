---
title: >-
    غزل شمارهٔ ۱۱۲
---
# غزل شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>زان پیش که دل ز جان برآید</p></div>
<div class="m2"><p>جان از تن ناتوان برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمای جمال، تا دهم جان</p></div>
<div class="m2"><p>کان سود بر این زیان برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای کاش به جان برآمدی کار</p></div>
<div class="m2"><p>این کار کجا به جان برآید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم نه چنان فتاد مشکل</p></div>
<div class="m2"><p>کان بی‌تو به این و آن برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم از در تو گشایدم کار</p></div>
<div class="m2"><p>کامم همه زان دهان برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر درگهت آمدم به کاری</p></div>
<div class="m2"><p>کان بر تو به رایگان برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نایافته جانم از تو بویی</p></div>
<div class="m2"><p>مگذار که ناگهان برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنواز به لطف جانم، آن دم</p></div>
<div class="m2"><p>کز کالبدم روان برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کام دل خستهٔ عراقی</p></div>
<div class="m2"><p>از لطف تو بی‌گمان برآید</p></div></div>