---
title: >-
    غزل شمارهٔ ۶۴
---
# غزل شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>بیا، کاین دل سر هجران ندارد</p></div>
<div class="m2"><p>بجز وصلت دگر درمان ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وصل خود دلم را شاد گردان</p></div>
<div class="m2"><p>که خسته طاقت هجران ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا، تا پیش روی تو بمیرم</p></div>
<div class="m2"><p>که بی‌تو زندگانی آن ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه بی‌تو بتوان زیست آخر؟</p></div>
<div class="m2"><p>که بی‌تو زیستن امکان ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمردم ز انتظار روز وصلت</p></div>
<div class="m2"><p>شب هجران مگر پایان ندارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا، تا روی خوب تو ببینم</p></div>
<div class="m2"><p>که مهر از ذره رخ پنهان ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز من بپذیر، جانا، نیم جانی</p></div>
<div class="m2"><p>اگر چه قیمت چندان ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه باشد گر فراغت والهی را</p></div>
<div class="m2"><p>چنین سرگشته و حیران ندارد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصالت تا ز غم خونم نریزد</p></div>
<div class="m2"><p>عراقی را شبی مهمان ندارد</p></div></div>