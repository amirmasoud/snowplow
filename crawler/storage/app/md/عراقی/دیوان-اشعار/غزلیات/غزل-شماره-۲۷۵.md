---
title: >-
    غزل شمارهٔ ۲۷۵
---
# غزل شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>الا، قد طال عهدی بالوصال</p></div>
<div class="m2"><p>و مالی الصبر عن ذاک الجمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وصلم دست گیر، ای دوست، آخر</p></div>
<div class="m2"><p>به زیر پای هجرم چند مالی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یضیق من الفراق نطاق قلبی</p></div>
<div class="m2"><p>و یشتاق الفؤاد الی الوصال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خوش باشد که پیش از مرگ بینم!</p></div>
<div class="m2"><p>نشسته با تو یکدم جای خالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فراقک لا یفارقنی زمانا</p></div>
<div class="m2"><p>فمالی للجهر مولائی و مالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا، درمان مجو، با درد خو کن</p></div>
<div class="m2"><p>بجای وصل هجران است، حالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اما ترثی لمکتئب حزین</p></div>
<div class="m2"><p>یان من النوی طول اللیالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا، امیدوار وصل می‌باش</p></div>
<div class="m2"><p>ز درد هجر آخر چند نالی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمانا کنت لا ارضی بوصل</p></div>
<div class="m2"><p>فصرت الان ارضی بالخیال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دل نزدیکی، ار چه دوری از چشم</p></div>
<div class="m2"><p>دلم را چون همیشه در خیالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>احن الیک و العبرات تجری</p></div>
<div class="m2"><p>کما حق العطاش الی الزلال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عراقی، تا به خود می‌جویی او را</p></div>
<div class="m2"><p>یقین می‌دان که دربند محالی</p></div></div>