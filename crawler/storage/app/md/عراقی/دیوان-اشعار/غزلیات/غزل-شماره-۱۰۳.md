---
title: >-
    غزل شمارهٔ ۱۰۳
---
# غزل شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>هر که او دعوی مستی می‌کند</p></div>
<div class="m2"><p>آشکارا بت‌پرستی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی آن را می‌سزد کز نیستی</p></div>
<div class="m2"><p>هر نفس صدگونه هستی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که از خاک درش رفعت نیافت</p></div>
<div class="m2"><p>لاجرم سر سوی پستی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که خورد از جام عشقش جرعه‌ای</p></div>
<div class="m2"><p>بی‌خبر شد، شور و مستی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چو خواهم باختن در پای او</p></div>
<div class="m2"><p>جان ز شوقش پیش دستی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند گویی کو جفا تا کی کند؟</p></div>
<div class="m2"><p>ای عراقی، تا تو هستی می‌کند</p></div></div>