---
title: >-
    غزل شمارهٔ ۹۳
---
# غزل شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>آشکارا نهان کنم تا چند؟</p></div>
<div class="m2"><p>دوست می‌دارمت به بانگ بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم از جان نخست دست بشست</p></div>
<div class="m2"><p>بعد از آن دیده بر رخت افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان تو نیک معذورند</p></div>
<div class="m2"><p>زانکه نبود کسی تو را مانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده‌ای کو رخ تو دیده بود</p></div>
<div class="m2"><p>خواه راحت رسان و خواه گزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ملامت کنان مرا در عشق</p></div>
<div class="m2"><p>گوش من نشنود ازین سان پند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه من دور مانده‌ام ز برت</p></div>
<div class="m2"><p>با خیال تو کرده‌ام پیوند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن چنان در دلی که پنداری</p></div>
<div class="m2"><p>ناظرم در تو دایم، ای دلبند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو کجایی و ما کجا هیهات!</p></div>
<div class="m2"><p>ای عراقی، خیال خیره مبند</p></div></div>