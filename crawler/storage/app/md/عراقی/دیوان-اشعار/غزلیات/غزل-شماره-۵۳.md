---
title: >-
    غزل شمارهٔ ۵۳
---
# غزل شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>با عشق عقل‌فرسا دیوانه‌ای چه سنجد؟</p></div>
<div class="m2"><p>با شمع روی زیبا پروانه‌ای چه سنجد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش خیال رویت جانی چه قدر دارد؟</p></div>
<div class="m2"><p>با تاب بند مویت دیوانه‌ای چه سنجد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وصل جان‌فزایت جان را چه آشنایی؟</p></div>
<div class="m2"><p>در کوی آشنایی بیگانه‌ای چه سنجد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زلف برفشانی عالم خراب گردد</p></div>
<div class="m2"><p>دل خود چه طاقت آرد؟ویرانه‌ای چه سنجد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه خوش است و دلکش کاشانه‌ای است جنت</p></div>
<div class="m2"><p>در جنت حسن رویت کاشانه‌ای چه سنجد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با من اگر نشینی برخیزم از سر جان</p></div>
<div class="m2"><p>پیش بهشت رویت غم خانه‌ای چه سنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیرم که خود عراقی، شکرانه، جان فشاند</p></div>
<div class="m2"><p>در پیش آن چنان رو، شکرانه‌ای چه سنجد؟</p></div></div>