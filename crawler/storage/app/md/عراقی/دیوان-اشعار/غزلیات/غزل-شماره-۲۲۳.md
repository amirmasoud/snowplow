---
title: >-
    غزل شمارهٔ ۲۲۳
---
# غزل شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>بپرس از دلم آخر، چه دل؟ که قطرهٔ خون</p></div>
<div class="m2"><p>که بی‌تو زار چنان شد که: من نگویم چون؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببین که پیش تو در خاک چون همی غلتد؟</p></div>
<div class="m2"><p>چنان که هر که ببیند برو بگرید خون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمانده بی رخ زیبای خویش دشمن کام</p></div>
<div class="m2"><p>فتاده خوار و خجل در کف زمانه زبون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه پای آنکه ز پیش زمانه بگریزد</p></div>
<div class="m2"><p>نه روی آنکه ز دست بلا شود بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون چه چاره؟ که کار دلم ز چاره گذشت</p></div>
<div class="m2"><p>گذشت آب چو از سر، چه سود چاره کنون؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب دست کشید از علاج درد دلم</p></div>
<div class="m2"><p>چه سود درد دلم را علاج با معجون؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علاج درد عراقی به جز تو کس نکند</p></div>
<div class="m2"><p>تویی که زنده کنی مرده را به کن فیکون</p></div></div>