---
title: >-
    غزل شمارهٔ ۶۹
---
# غزل شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>تا کی کشم جفای تو؟ این نیز بگذرد</p></div>
<div class="m2"><p>بسیار شد بلای تو، این نیز بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرم گذشت و یک نفسم بیشتر نماند</p></div>
<div class="m2"><p>خوش باش کز جفای تو، این نیز بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیی و بگذری به من و باز ننگری</p></div>
<div class="m2"><p>ای جان من فدای تو، این نیز بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس رسید از تو به مقصود و این گدا</p></div>
<div class="m2"><p>محروم از عطای تو، این نیز بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دوست، تو مرا همه دشنام می‌دهی</p></div>
<div class="m2"><p>من می‌کنم، دعای تو، این نیز بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیم به درگهت، نگذاری که بگذرم</p></div>
<div class="m2"><p>پیرامن سرای تو، این نیز بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد دلم به کوی تو، نومید بازگشت</p></div>
<div class="m2"><p>نشنید مرحبای تو، این نیز بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذشت آنکه دوست همی داشتی مرا</p></div>
<div class="m2"><p>دیگر شده است رای تو، این نیز بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کی کشد عراقی مسکین جفای تو؟</p></div>
<div class="m2"><p>بگذشت چون جفای تو، این نیز بگذرد</p></div></div>