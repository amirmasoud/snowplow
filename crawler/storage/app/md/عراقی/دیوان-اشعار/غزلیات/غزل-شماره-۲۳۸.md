---
title: >-
    غزل شمارهٔ ۲۳۸
---
# غزل شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>در صومعه نگنجد، رند شرابخانه</p></div>
<div class="m2"><p>عنقا چگونه گنجد در کنج آشیانه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی، به یک کرشمه بشکن هزار توبه</p></div>
<div class="m2"><p>بستان مرا ز من باز زان چشم جاودانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا وارهم ز هستی وز ننگ خودپرستی</p></div>
<div class="m2"><p>بر هم زنم ز مستی نیک و بد زمانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین زهد و پارسایی چون نیست جز ریایی</p></div>
<div class="m2"><p>ما و شراب و شاهد، کنج شرابخانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خوش بود خرابی! افتاده در خرابات</p></div>
<div class="m2"><p>چون چشم یار مخمور از مستی شبانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیا بود که بختم بیند به خواب مستی</p></div>
<div class="m2"><p>او در کناره، آنگه من رفته از میانه؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی شراب داده هر لحظه جام دیگر</p></div>
<div class="m2"><p>مطرب سرود گفته هر دم دگر ترانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جام باده دیده عکس جمال ساقی</p></div>
<div class="m2"><p>و آواز او شنوده از زخمهٔ چغانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این است زندگانی، باقی همه حکایت</p></div>
<div class="m2"><p>این است کامرانی، باقی همه فسانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میخانه حسن ساقی، میخواره چشم مستش</p></div>
<div class="m2"><p>پیمانه هم لب او، باقی همه بهانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دیدهٔ عراقی جام شراب و ساقی</p></div>
<div class="m2"><p>هر سه یکی است و احول بیند یکی دوگانه</p></div></div>