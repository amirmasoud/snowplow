---
title: >-
    غزل شمارهٔ ۱۲۱
---
# غزل شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>رخ سوی خرابات نهادیم دگربار</p></div>
<div class="m2"><p>در دام خرابات فتادیم دگربار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر یکی جرعه دو صد توبه شکستیم</p></div>
<div class="m2"><p>در دیر مغان روزه گشادیم دگربار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کنج خرابات یکی مغ‌بچه دیدیم</p></div>
<div class="m2"><p>در پیش رخش سر بنهادیم دگربار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دل که به صد حیله ز خوبان بربودیم</p></div>
<div class="m2"><p>در دست یکی مغ‌بچه دادیم دگربار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک بار ندیدیم رخش وز غم عشقش</p></div>
<div class="m2"><p>صدبار بمردیم و بزادیم دگربار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدیم که بی‌عشق رخش زندگیی نیست</p></div>
<div class="m2"><p>بی‌عشق رخش زنده مبادیم دگربار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم بر دل ما تاختن آورد ز عشقش</p></div>
<div class="m2"><p>با این همه غم، بین که چه شادیم دگربار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد در سر سودای رخش دین و دل ما</p></div>
<div class="m2"><p>بنگر، دل و دین داده به بادیم دگربار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشقش به زیان برد صلاح و ورع ما</p></div>
<div class="m2"><p>اینک همه در عین فسادیم دگربار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با نیستی خود همه با قیمت و قدریم</p></div>
<div class="m2"><p>با هستی خود جمله کسادیم دگربار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا هست عراقی همه هستیم مریدش</p></div>
<div class="m2"><p>چون نیست شود، جمله مرادیم دگربار</p></div></div>