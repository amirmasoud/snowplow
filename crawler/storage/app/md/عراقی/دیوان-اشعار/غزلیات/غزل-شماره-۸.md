---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>مست خراب یابد هر لحظه در خرابات</p></div>
<div class="m2"><p>گنجی که آن نیابد صد پیر در مناجات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که راه یابی بی‌رنج بر سر گنج</p></div>
<div class="m2"><p>می‌بیز هر سحرگاه خاک در خرابات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک ذره گرد از آن خاک در چشم جانت افتد</p></div>
<div class="m2"><p>با صدهزار خورشید افتد تو را ملاقات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور عکس جام باده ناگاه بر تو تابد</p></div>
<div class="m2"><p>نز خویش گردی آگه، نز جام، نز شعاعات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بیخودی و مستی جایی رسی، که آنجا</p></div>
<div class="m2"><p>در هم شود عبادات، پی گم کند اشارات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا گم نگردی از خود گنجی چنین نیابی</p></div>
<div class="m2"><p>حالی چنین نیابد گم گشته از ملاقات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کی کنی به عادت در صومعه عبادت؟</p></div>
<div class="m2"><p>کفر است زهد و طاعت تا نگذری ز میقات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا تو ز خودپرستی وز جست وجو نرستی</p></div>
<div class="m2"><p>می‌دان که می‌پرستی در دیر عزی و لات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در صومعه تو دانی می‌کوش تا توانی</p></div>
<div class="m2"><p>در میکده رها کن از سر فضول و طامات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان باز در خرابات، تا جرعه‌ای بیابی</p></div>
<div class="m2"><p>مفروش زهد، کانجا کمتر خرند طامات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لب تشنه چند باشی، در ساحل تمنی؟</p></div>
<div class="m2"><p>انداز خویشتن را در بحر بی‌نهایات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا گم شود نشانت در پای بی‌نشانی</p></div>
<div class="m2"><p>تا در کشد به کامت یک ره نهنگ حالات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون غرقه شد عراقی یابد حیات باقی</p></div>
<div class="m2"><p>اسرار غیب بیند در عالم شهادات</p></div></div>