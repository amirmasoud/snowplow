---
title: >-
    غزل شمارهٔ ۱۵۶
---
# غزل شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>از دل و جان عاشق زار توام</p></div>
<div class="m2"><p>کشتهٔ اندوه و تیمار توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشتی کن بامن، آزرمم بدار،</p></div>
<div class="m2"><p>من نه مرد جنگ و آزار توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر گناهی کرده‌ام بر من مگیر</p></div>
<div class="m2"><p>عفو کن، من خود گرفتار توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید ار یکدم غم کارم خوری</p></div>
<div class="m2"><p>چون که من پیوسته غمخوار توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال من می‌پرس گه گاهی به لطف</p></div>
<div class="m2"><p>چون که من رنجور و بیمار توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون عراقی نیستم فارغ ز تو</p></div>
<div class="m2"><p>روز و شب جویای دیدار توام</p></div></div>