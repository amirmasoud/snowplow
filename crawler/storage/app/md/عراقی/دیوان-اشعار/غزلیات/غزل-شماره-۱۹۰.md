---
title: >-
    غزل شمارهٔ ۱۹۰
---
# غزل شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>افسوس! که باز از در تو دور بماندیم</p></div>
<div class="m2"><p>هیهات! که از وصل تو مهجور بماندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشتیم دگر باره به کام دل دشمن</p></div>
<div class="m2"><p>کز روی تو، ای دوست، چنین دور بماندیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماتم زدگانیم، بیا، زار بگرییم</p></div>
<div class="m2"><p>بر بخت بد خویش، که از سور بماندیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید رخت بر سر ما سایه نیفکند</p></div>
<div class="m2"><p>بی روز رخت در شب دیجور بماندیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بوی خوشت زندگیی یافته بودیم</p></div>
<div class="m2"><p>واکنون همه بی‌بوی تو رنجور بماندیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشن نشد این خانهٔ تاریک دل ما</p></div>
<div class="m2"><p>از شمع رخت، تا همه بی‌نور بماندیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناخورده یکی جرعه ز جام می وصلت</p></div>
<div class="m2"><p>بنگر، چو عراقی، همه مخمور بماندیم</p></div></div>