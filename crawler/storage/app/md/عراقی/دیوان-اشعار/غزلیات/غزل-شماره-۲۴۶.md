---
title: >-
    غزل شمارهٔ ۲۴۶
---
# غزل شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>نگارا، گر چه از ما برشکستی</p></div>
<div class="m2"><p>ز جانت بنده‌ام، هر جا که هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ربودی دل ز من، چون رخ نمودی</p></div>
<div class="m2"><p>شکستی پشت من، چون برشکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا پیوستی، ای جان، با دل من؟</p></div>
<div class="m2"><p>چو آخر دست، از من می‌گسستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نوش لب چو مرهم می‌ندادی</p></div>
<div class="m2"><p>ز نیش لب چرا جانم بخستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر کشتنم صد حیله کردی</p></div>
<div class="m2"><p>چو خونم ریختی فارغ نشستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه یافتی از کشتنم رنج</p></div>
<div class="m2"><p>ز محنت‌های من، باری، برستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا کشتی، به طنز آنگاه گویی:</p></div>
<div class="m2"><p>عراقی، از کف من نیک جستی!</p></div></div>