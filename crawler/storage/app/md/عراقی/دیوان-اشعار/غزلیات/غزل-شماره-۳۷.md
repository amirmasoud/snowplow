---
title: >-
    غزل شمارهٔ ۳۷
---
# غزل شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>ساقی، ار جام می، دمادم نیست</p></div>
<div class="m2"><p>جان فدای تو، دردیی کم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که در میکده کم از خاکم</p></div>
<div class="m2"><p>جرعه‌ای هم مرا مسلم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرعه‌ای ده، مرا ز غم برهان</p></div>
<div class="m2"><p>که دلم بی‌شراب خرم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خودی خودم خلاصی ده</p></div>
<div class="m2"><p>کز خودم زخم هست مرهم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون حجاب من است هستی من</p></div>
<div class="m2"><p>گر نباشد، مباش، گو: غم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آرزوی دمی دلم خون شد</p></div>
<div class="m2"><p>که شوم یک نفس درین دم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر دل درهم و پریشانم</p></div>
<div class="m2"><p>چه کنم؟ کار دل فراهم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشدلی در جهان نمی‌یابم</p></div>
<div class="m2"><p>خود خوشی در نهاد عالم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان گر خوشی کم است مرا</p></div>
<div class="m2"><p>خوش از آنم که ناخوشی هم نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشت امید را، که خشک بماند</p></div>
<div class="m2"><p>بهتر از آب چشم من نم نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساقیا، یک دمم حریفی کن</p></div>
<div class="m2"><p>کین دمم جز تو هیچ همدم نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساغری ده، مرا ز من برهان</p></div>
<div class="m2"><p>که عراقی حریف و محرم نیست</p></div></div>