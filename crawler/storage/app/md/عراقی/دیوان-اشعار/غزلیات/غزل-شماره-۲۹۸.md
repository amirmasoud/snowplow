---
title: >-
    غزل شمارهٔ ۲۹۸
---
# غزل شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>کشید کار ز تنهاییم به شیدایی</p></div>
<div class="m2"><p>ندانم این همه غم چون کشم به تنهایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس که داد قلم شرح سرنوشت فراق</p></div>
<div class="m2"><p>ز سرنوشت قلم نامه گشت سودایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا تو عمر عزیزی و رفته‌ای ز برم</p></div>
<div class="m2"><p>چو خوش بود اگر، ای عمر رفته بازآیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان گشاده، کمر بسته‌ایم، تا چو قلم</p></div>
<div class="m2"><p>به سر کنیم هر آن خدمتی که فرمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به احتیاط گذر بر سواد دیدهٔ من</p></div>
<div class="m2"><p>چنان که گوشهٔ دامن به خون نیالایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه مرد عشق تو بودم ازین طریق، که عقل</p></div>
<div class="m2"><p>درآمده است به سر، با وجود دانایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درم گشای، که امید بسته‌ام در تو</p></div>
<div class="m2"><p>در امید که بگشاید؟ ار تو نگشایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آفتاب خطاب تو خواستم کردن</p></div>
<div class="m2"><p>دلم نداد، که هست آفتاب هر جایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعادت دو جهان است دیدن رویت</p></div>
<div class="m2"><p>زهی! سعادت، اگر زان چه روی بنمایی!</p></div></div>