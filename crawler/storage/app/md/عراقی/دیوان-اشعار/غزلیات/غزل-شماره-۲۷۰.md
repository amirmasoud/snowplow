---
title: >-
    غزل شمارهٔ ۲۷۰
---
# غزل شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>لقد فاح الربیع و دار ساقی</p></div>
<div class="m2"><p>وهب نسیم روضات العراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبا بوی عراق آورد گویی</p></div>
<div class="m2"><p>که خوش گشت از نسیم او عراقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الا یا حبذا! نفحات ارض</p></div>
<div class="m2"><p>جوی المشتاق یشفی باشتیاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریغا! روزگار نوش بگذشت</p></div>
<div class="m2"><p>ندیمم بخت بود و یار ساقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلیت ان صبحی بالبلایا</p></div>
<div class="m2"><p>الاق مرور ایام التلاقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جور روزگار ناموافق</p></div>
<div class="m2"><p>جدا گشتم ز یاران وفاقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ادر، یا ایها الساقی، ارحنی</p></div>
<div class="m2"><p>زمانا من خمار الافتراق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم را شاد کن، ساقی، که نگذاشت</p></div>
<div class="m2"><p>جدایی بر من از غم هیچ باقی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و عل لعل لطیفی نار قلبی</p></div>
<div class="m2"><p>و قلبی من تراکم فی احتراق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بده جامی، که اندر وی ببینم</p></div>
<div class="m2"><p>جمال دوستان هم وثاقی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جرعت من التفرق کل یوم</p></div>
<div class="m2"><p>و اجریت الدموع من الماقی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنال، ایدل، ز درد و غم که پیوست</p></div>
<div class="m2"><p>گرفتار غم و درد فراقی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الا یا اهل العراق، تحذ قلبی</p></div>
<div class="m2"><p>الیکم و اشتمل من اشتیاقی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عراقی، خوش بموی و زار بگری</p></div>
<div class="m2"><p>که در هندوستان از جفت طاقی</p></div></div>