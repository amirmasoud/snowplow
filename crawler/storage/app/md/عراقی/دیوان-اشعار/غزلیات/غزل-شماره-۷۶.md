---
title: >-
    غزل شمارهٔ ۷۶
---
# غزل شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>روی ننمود یار چتوان کرد؟</p></div>
<div class="m2"><p>چیست تدبیر کار چتوان کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دو چشم پر آب نقش نگار</p></div>
<div class="m2"><p>چون نگیرد قرار چتوان کرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر آیینه‌ای نمی‌گنجد</p></div>
<div class="m2"><p>عکس روی نگار چتوان کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سراسیمه‌ای نمی‌یابد</p></div>
<div class="m2"><p>بر در وصل بار چتوان کرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت عمرو نرفت در همه عمر</p></div>
<div class="m2"><p>دست در زلف یار چتوان کرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشت ما را به دوستی، چه کنیم</p></div>
<div class="m2"><p>با چنان دوستدار چتوان کرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتهٔ عشق اوست بر در او</p></div>
<div class="m2"><p>چون عراقی هزار، چتوان کرد؟</p></div></div>