---
title: >-
    غزل شمارهٔ ۱۹۳
---
# غزل شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>من که هر لحظه زار می‌گریم</p></div>
<div class="m2"><p>از غم روزگار می‌گریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبری بود در کنار مرا</p></div>
<div class="m2"><p>کرد از من کنار، می‌گریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غم غمگسار می‌نالم</p></div>
<div class="m2"><p>وز فراق نگار می‌گریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش با شمع گفتم از سر سوز</p></div>
<div class="m2"><p>که: من از عشق یار می‌گریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماتم بخت خویش می‌دارم</p></div>
<div class="m2"><p>زان چنین سوکوار می‌گریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چنین خنده گریهٔ تو ز چیست؟</p></div>
<div class="m2"><p>کز تو بس دل فگار می‌گریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داشتم، گفت: دلبری شیرین</p></div>
<div class="m2"><p>زو شدم دور، زار می‌گریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون عراقی حدیث او بشنید</p></div>
<div class="m2"><p>زارتر من ز پار می‌گریم</p></div></div>