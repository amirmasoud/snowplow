---
title: >-
    غزل شمارهٔ ۲۷
---
# غزل شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>در کوی خرابات، کسی را که نیاز است</p></div>
<div class="m2"><p>هشیاری و مستیش همه عین نماز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا نپذیرند صلاح و ورع امروز</p></div>
<div class="m2"><p>آنچ از تو پذیرند در آن کوی نیاز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسرار خرابات به جز مست نداند</p></div>
<div class="m2"><p>هشیار چه داند که درین کوی چه راز است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مستی رندان خرابات بدیدم</p></div>
<div class="m2"><p>دیدم به حقیقت که جزین کار مجاز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که درون حرم عشق خرامی؟</p></div>
<div class="m2"><p>در میکده بنشین که ره کعبه دراز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان! تا ننهی پای درین راه ببازی</p></div>
<div class="m2"><p>زیرا که درین راه بسی شیب و فراز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از میکده‌ها نالهٔ دلسوز برآمد</p></div>
<div class="m2"><p>در زمزمهٔ عشق ندانم که چه ساز است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در زلف بتان تا چه فریب است؟که پیوست</p></div>
<div class="m2"><p>محمود پریشان سر زلف ایاز است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان شعله که از روی بتان حسن تو افروخت</p></div>
<div class="m2"><p>جان همه مشتاقان در سوز و گداز است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بر در میخانه مرا بار ندادند</p></div>
<div class="m2"><p>رفتم به در صومعه، دیدم که فراز است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آواز ز میخانه برآمد که: عراقی</p></div>
<div class="m2"><p>در باز تو خود را که در میکده باز است</p></div></div>