---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>این حادثه بین که زاد ما را</p></div>
<div class="m2"><p>وین واقعه کاوفتاد ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یار، که در میان جان است</p></div>
<div class="m2"><p>بر گوشهٔ دل نهاد ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خانهٔ ما نمی‌نهد پای</p></div>
<div class="m2"><p>از دست مگر بداد ما را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی به سلام یا پیامی</p></div>
<div class="m2"><p>آن یار نکرد یاد ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانست که در غمیم بی او</p></div>
<div class="m2"><p>از لطف نکرد شاد ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر ما در لطف خود فرو بست</p></div>
<div class="m2"><p>وز هجر دری گشاد ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود مادر روزگار گویی</p></div>
<div class="m2"><p>کز بهر فراق زاد ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای کاش نزادی، ای عراقی</p></div>
<div class="m2"><p>کز توست همه فساد ما را</p></div></div>