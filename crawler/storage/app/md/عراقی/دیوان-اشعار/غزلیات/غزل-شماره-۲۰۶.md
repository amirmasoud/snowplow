---
title: >-
    غزل شمارهٔ ۲۰۶
---
# غزل شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>شهری است بزرگ و ما دروییم</p></div>
<div class="m2"><p>آبی است حیات و ما سبوییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بویی به مشام ما رسیده است</p></div>
<div class="m2"><p>ما زنده بدان نسیم و بوییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازیچه مدان، تو خواجه، ما را</p></div>
<div class="m2"><p>ما از صفت جلال اوییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چوگان حیات تا بخوردیم</p></div>
<div class="m2"><p>در راه به سر دوان چو گوییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خوی صفات او گرفتیم</p></div>
<div class="m2"><p>نشناخت کسی که در چه خوییم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌گفت عراقی از سر سوز:</p></div>
<div class="m2"><p>ما نیز برای گفت و گوییم</p></div></div>