---
title: >-
    غزل شمارهٔ ۴۸
---
# غزل شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>بر من، ای دل، بند جان نتوان نهاد</p></div>
<div class="m2"><p>شور در دیوانگان نتوان نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>های و هویی در فلک نتوان فکند</p></div>
<div class="m2"><p>شر و شوری در جهان نتوان نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون پریشانی سر زلفت کند</p></div>
<div class="m2"><p>سلسله بر پای جان نتوان نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خرابی چشم مستت می‌کند</p></div>
<div class="m2"><p>جرم بر دور زمان نتوان نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو مهمان و ما را هیچ نه</p></div>
<div class="m2"><p>هیچ پیش میهمان نتوان نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیم جانی پیش او نتوان کشید</p></div>
<div class="m2"><p>پیش سیمرغ استخوان نتوان نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه گه‌گه وعدهٔ وصلم دهد</p></div>
<div class="m2"><p>غمزهٔ تو، دل بر آن نتوان نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویمت: بوسی به جانی، گوییم:</p></div>
<div class="m2"><p>بر لبم لب رایگان نتوان نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر خوان لبت، خود بی‌جگر</p></div>
<div class="m2"><p>لقمه‌ای خوش در دهان نتوان نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دلم بار غمت چندین منه</p></div>
<div class="m2"><p>برکهی کوه گران نتوان نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب در دل می‌زدم، مهر تو گفت:</p></div>
<div class="m2"><p>زود پابر آسمان نتوان نهاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا تو را در دل هوای جان بود</p></div>
<div class="m2"><p>پای بر آب روان نتوان نهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تات وجهی روشن است، این هفت‌خوان</p></div>
<div class="m2"><p>پیش تو بس، هشت خوان نتوان نهاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور عراقی محرم این حرف نیست</p></div>
<div class="m2"><p>راز با او در میان نتوان نهاد</p></div></div>