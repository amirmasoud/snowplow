---
title: >-
    غزل شمارهٔ ۲۶
---
# غزل شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ساز طرب عشق که داند که چه ساز است؟</p></div>
<div class="m2"><p>کز زخمهٔ آن نه فلک اندر تک و تاز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورد به یک زخمه، جهان را همه، در رقص</p></div>
<div class="m2"><p>خود جان و جهان نغمهٔ آن پرده‌نواز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم چو صدایی است ازین پرده، که داند</p></div>
<div class="m2"><p>کین راه چه پرده است و درین پرده چه راز است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رازی است درین پرده، گر آن را بشناسی</p></div>
<div class="m2"><p>دانی که حقیقت ز چه دربند مجاز است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معلوم کنی کز چه سبب خاطر محمود</p></div>
<div class="m2"><p>پیوسته پریشان سر زلف ایاز است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محتاج نیاز دل عشاق چرا شد</p></div>
<div class="m2"><p>حسن رخ خوبان، که همه مایهٔ ناز است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق است که هر دم به دگر رنگ برآید</p></div>
<div class="m2"><p>ناز است بجایی و یه یک جای نیاز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در صورت عاشق چو درآید همه سوزاست</p></div>
<div class="m2"><p>در کسوت معشوق چو آید همه ساز است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان شعله که از روی بتان حسن برافروخت</p></div>
<div class="m2"><p>قسم دل عشاق همه سوز و گداز است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راهی است ره عشق، به غایت خوش و نزدیک</p></div>
<div class="m2"><p>هر ره که جزین است همه دور و دراز است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مستی، که خراب ره عشق است، درین ره</p></div>
<div class="m2"><p>خواب خوش مستیش همه عین نماز است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در صومعه چون راه ندادند مرا دوش</p></div>
<div class="m2"><p>رفتم به در میکده، دیدم که فراز است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از میکده آواز برآمد که: عراقی</p></div>
<div class="m2"><p>در باز تو خود را، که در میکده باز است</p></div></div>