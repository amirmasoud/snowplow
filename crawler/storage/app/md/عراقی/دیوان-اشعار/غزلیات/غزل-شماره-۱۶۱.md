---
title: >-
    غزل شمارهٔ ۱۶۱
---
# غزل شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>دل گم شد، ازو نشان نیابم</p></div>
<div class="m2"><p>آن گم شده در جهان نیابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان یوسف گم شده به عالم</p></div>
<div class="m2"><p>پیدا و نهان نشان نیابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گوهر شب چراغ گم شد</p></div>
<div class="m2"><p>ره بر در دوستان نیابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بلبل خوشنوای گم شد</p></div>
<div class="m2"><p>بوی گل و بوستان نیابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا آب حیات رفت از جوی</p></div>
<div class="m2"><p>عیش خوش جاودان نیابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرمایه برفت و سود جویم</p></div>
<div class="m2"><p>زان است که جز زیان نیابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یوسف خویش را چه جویم؟</p></div>
<div class="m2"><p>چون در چه کن فکان نیابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم بر در دوست باشد آرام</p></div>
<div class="m2"><p>از خود به جز این گمان نیابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خاک درش چرا ننالم ؟</p></div>
<div class="m2"><p>چاره به جز از فغان نیابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون جانش عزیز دارم، آری</p></div>
<div class="m2"><p>دل، کز غم او امان نیابم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا بر من دلشده بگرید</p></div>
<div class="m2"><p>یک مشفق مهربان نیابم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا یک نفسی مرا بود یار</p></div>
<div class="m2"><p>یک یار درین زمان نیابم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یاری ده خویشتن درین حال</p></div>
<div class="m2"><p>جز دیدهٔ خون‌فشان نیابم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر خوان جهان چه می‌نشینم؟</p></div>
<div class="m2"><p>چون لقمه جز استخوان نیابم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌حاصل ازین دکان بخیزم</p></div>
<div class="m2"><p>نقدی چو درین دکان نیابم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواهم که شوم به بام عالم</p></div>
<div class="m2"><p>چه چاره، چو نردبان نیابم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواهم که کشم ز چه عراقی</p></div>
<div class="m2"><p>افسوس که ریسمان نیابم!</p></div></div>