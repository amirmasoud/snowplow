---
title: >-
    غزل شمارهٔ ۶۵
---
# غزل شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>دل، دولت خرمی ندارد</p></div>
<div class="m2"><p>جان، راحت بی‌غمی ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردا! که درون آدمی زاد</p></div>
<div class="m2"><p>آسایش و خرمی ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از راحت‌های این جهانی</p></div>
<div class="m2"><p>جز غم دل آدمی ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مرگ، بیا و مردمی کن</p></div>
<div class="m2"><p>این غم سر مردمی ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وی غم، بنشین، که شادمانی</p></div>
<div class="m2"><p>با ما سر همدمی ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وی جان، ز سرای تن برون شو</p></div>
<div class="m2"><p>کین جای تو محکمی ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منشین همه وقت با عراقی</p></div>
<div class="m2"><p>کاهلیت محرمی ندارد</p></div></div>