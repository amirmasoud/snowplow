---
title: >-
    غزل شمارهٔ ۲۲۹
---
# غزل شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>ای همه میل دل من سوی تو</p></div>
<div class="m2"><p>قبلهٔ جان چشم تو و ابروی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس مستت ربوده عقل من</p></div>
<div class="m2"><p>برده خوابم نرگس جادوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر میدان جانبازی دلم</p></div>
<div class="m2"><p>در خم چوگان ز زلف و گوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمدم در کوی امید تو باز</p></div>
<div class="m2"><p>تا مگر بینم رخ نیکوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من جگر تفتیده بر خاک درت</p></div>
<div class="m2"><p>آب حیوان رایگان در جوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای امید من، روا داری مگر؟</p></div>
<div class="m2"><p>باز گردم ناامید از کوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطف کن، دست جفا بر من مدار</p></div>
<div class="m2"><p>من ندارم طاقت بازوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزگاری بوده‌ام بر درگهت</p></div>
<div class="m2"><p>چشم امیدم بمانده سوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا مگر بینم دمی رنگ رخت</p></div>
<div class="m2"><p>تا مگر یابم زمانی بوی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ندیدم رنگ رویت، لاجرم</p></div>
<div class="m2"><p>مانده‌ام در درد بی‌داروی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر من مسکین عاجز رحم کن</p></div>
<div class="m2"><p>چون فروماندم ز جست و جوی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در غم تو روزگارم شد دریغ!</p></div>
<div class="m2"><p>ناشده یک لحظه همزانوی تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم مشام جانم آخر خوش شود</p></div>
<div class="m2"><p>از نسیم جان فزای موی تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خود عراقی جان شیرین کی دهد؟</p></div>
<div class="m2"><p>تا به کام دل نبیند روی تو</p></div></div>