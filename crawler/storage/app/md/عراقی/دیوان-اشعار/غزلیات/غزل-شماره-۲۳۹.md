---
title: >-
    غزل شمارهٔ ۲۳۹
---
# غزل شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>در صومعه نگنجد رند شرابخانه</p></div>
<div class="m2"><p>ساقی، بده مغی را، درد می مغانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره ده قلندری را، در بزم دردنوشان</p></div>
<div class="m2"><p>بنما مقامری را، راه قمارخانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بشکند چو توبه، هر بت که می‌پرستید</p></div>
<div class="m2"><p>تا جان نهد چو جرعه، شکرانه در میانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیرون شود، چو عنقا، از خانه سوی صحرا</p></div>
<div class="m2"><p>پرواز گیرد از خود، بگذارد آشیانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغ شود ز هستی وز خویشتن پرستی</p></div>
<div class="m2"><p>بر هم زند ز مستی نیک و بد زمانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خلوتی چنین خوش چه خوش بود صبوحی!</p></div>
<div class="m2"><p>با محرمی موافق، با همدمی یگانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آورده روی در روی با شاهدی شکر لب</p></div>
<div class="m2"><p>در کف می صبوحی، در سر می شبانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقی شراب داده هر لحظه از دگر جام</p></div>
<div class="m2"><p>مطرب سرود گفته هر دم دگر ترانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باده حدیث جانان، باقی همه حکایت</p></div>
<div class="m2"><p>نغمه خروش مستان دیگر همه فسانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظاره روی ساقی، نظارگی عراقی</p></div>
<div class="m2"><p>خم خانه عشق باقی، باقی همه بهانه</p></div></div>