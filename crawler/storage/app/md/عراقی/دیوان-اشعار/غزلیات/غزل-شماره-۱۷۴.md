---
title: >-
    غزل شمارهٔ ۱۷۴
---
# غزل شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>بر من نظری کن، که منت عاشق زارم</p></div>
<div class="m2"><p>دلدار و دلارام به غیر از تو ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خار غم عشق تو در پای دلم شد</p></div>
<div class="m2"><p>بی‌روی تو گلهای چمن خار شمارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی طاقت آن تا ز غمت صبر توان کرد</p></div>
<div class="m2"><p>نی فرصت آن تا نفسی با تو برآرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شام درآید، ز غمت، زار بگریم</p></div>
<div class="m2"><p>باشد که به گوش تو رسد نالهٔ زارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کم کن تو جفا بر دل مسکین عراقی</p></div>
<div class="m2"><p>ورنه، به خدا، دست به فریاد برآرم</p></div></div>