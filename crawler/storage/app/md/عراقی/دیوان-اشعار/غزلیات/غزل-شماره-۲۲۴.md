---
title: >-
    غزل شمارهٔ ۲۲۴
---
# غزل شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>چو دل ز دایرهٔ عقل بی تو شد بیرون</p></div>
<div class="m2"><p>مپرس از دلم آخر که: چون شد آن مجنون؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم، که از سر سودا به هر دری می‌شد</p></div>
<div class="m2"><p>چو حلقه بین که بمانده است بر در تو کنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که خاک درت دوست‌تر ز جان دارد</p></div>
<div class="m2"><p>چگونه جای دگر باشدش قرار و سکون؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم، که حلقه به گوش در تو شد مفروش</p></div>
<div class="m2"><p>که هیچ قدر ندارد بهای قطرهٔ خون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو رایگان است آب حیات در جویت</p></div>
<div class="m2"><p>چرا بود دل مسکین چو ریگ در جیحون؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل عراقی اگر چه هزار گونه بگشت</p></div>
<div class="m2"><p>ولی ز مهر تو هرگز نگشت دیگر گون</p></div></div>