---
title: >-
    غزل شمارهٔ ۲۲۸
---
# غزل شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>ای آرزوی جان و دلم ز آرزوی تو</p></div>
<div class="m2"><p>بیمار گشته به نشود جز به بوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری، بپرس حال دل ناتوان من</p></div>
<div class="m2"><p>بنگر: چگونه می‌تپد از آرزوی تو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آرزوی روی تو جانم به لب رسید</p></div>
<div class="m2"><p>بنمای رخ، که جان بدهم پیش روی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال دل ضعیف چنین زار کی شدی؟</p></div>
<div class="m2"><p>گر یافتی نسیم گلستان کوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در راه جست و جوی تو هر جانبی دوید</p></div>
<div class="m2"><p>در ره بماند و راه نیاورد سوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لطف تو سزد که کنون دست گیریش</p></div>
<div class="m2"><p>چون بازمانده، گمشده در جست و جوی تو</p></div></div>