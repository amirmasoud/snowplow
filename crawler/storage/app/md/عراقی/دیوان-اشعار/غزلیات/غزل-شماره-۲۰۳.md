---
title: >-
    غزل شمارهٔ ۲۰۳
---
# غزل شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>ای دوست، بیا، که ما توراییم</p></div>
<div class="m2"><p>بیگانه مشو، که آشناییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ بازنمای، تا ببینیم</p></div>
<div class="m2"><p>در بازگشای، تا درآییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند نه‌ایم در خور تو</p></div>
<div class="m2"><p>لیکن چه کنیم؟ مبتلاییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بی‌تو نه‌ایم زنده یک دم</p></div>
<div class="m2"><p>پیوسته چرا ز تو جداییم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عکس جمال تو ندیدیم</p></div>
<div class="m2"><p>بر روی تو شیفته چراییم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کس که ندیده روی خوبت</p></div>
<div class="m2"><p>در حسرت تو بمرد، ماییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماییم کنون و نیم جانی</p></div>
<div class="m2"><p>بپذیر ز ما، که بی‌نواییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دور شدیم از بر تو</p></div>
<div class="m2"><p>دور از تو همیشه در بلاییم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس لایق و در خوری تو ما را</p></div>
<div class="m2"><p>هر چند که ما تو را نشاییم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچ از تو سزد به جای ما کن</p></div>
<div class="m2"><p>نه آنچه که ما بدان سزاییم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم زان توایم، هر چه هستیم</p></div>
<div class="m2"><p>گر محتشمیم و گر گداییم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عشق رخ تو چون عراقی</p></div>
<div class="m2"><p>هر دم غزلی دگر سراییم</p></div></div>