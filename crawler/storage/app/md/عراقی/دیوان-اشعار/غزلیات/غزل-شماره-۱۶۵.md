---
title: >-
    غزل شمارهٔ ۱۶۵
---
# غزل شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>من باز ره خانهٔ خمار گرفتم</p></div>
<div class="m2"><p>ترک ورع و زهد به یک بار گرفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سجاده و تسبیح به یک سوی فکندم</p></div>
<div class="m2"><p>بر کف می چون رنگ رخ یار گرفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارم همه با جام می و شاهد و شمع است</p></div>
<div class="m2"><p>ترک دل و دین بهر چنین کار گرفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمعم رخ یار است و شرابم لب دلدار</p></div>
<div class="m2"><p>پیمانه همان لب که به هنجار گرفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خوش ساقی دل و دین برد ز دستم</p></div>
<div class="m2"><p>وین فایده زان نرگس بیمار گرفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیوسته چینین می زده و مست و خرابم</p></div>
<div class="m2"><p>تا عادت چشم خوش خونخوار گرفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیرین لب ساقی چو می و نقل فرو ریخت</p></div>
<div class="m2"><p>بس کام کز آن لعل شکربار گرفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مست شدم خواستم از پای درآمد</p></div>
<div class="m2"><p>حالی سر زلف بت عیار گرفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آویختم اندر سر آن زلف پریشان</p></div>
<div class="m2"><p>این شیفتگی بین که دم مار گرفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتی: کم سودای سر زلف بتان گیر،</p></div>
<div class="m2"><p>چندین چه نصیحت کنی؟ انگار گرفتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با توبه و تقوی تو ره خلد برین گیر</p></div>
<div class="m2"><p>من با می و معشوقه ره نار گرفتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در نار چو رنگ رخ دلدار بدیدم</p></div>
<div class="m2"><p>آتش همه باغ و گل و گلزار گرفتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>المنة لله که میان گل و گلزار</p></div>
<div class="m2"><p>دلدار در آغوش دگربار گرفتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگرفت به دندان فلک انگشت تعجب</p></div>
<div class="m2"><p>چون من به دو انگشت لب یار گرفتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دور از لب و دندان عراقی لب دلدار</p></div>
<div class="m2"><p>هم باز به دست خوش دلدار گرفتم</p></div></div>