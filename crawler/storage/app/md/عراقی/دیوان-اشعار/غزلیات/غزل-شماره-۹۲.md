---
title: >-
    غزل شمارهٔ ۹۲
---
# غزل شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ز اشتیاق تو، جانا، دلم به جان آمد</p></div>
<div class="m2"><p>بیا، که با غم تو بر نمی‌توان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا، که با لب تو ماجرا نکرده هنوز</p></div>
<div class="m2"><p>به جای خرقه دل و دیده در میان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم مست تو گفتم: دلم به جان آید</p></div>
<div class="m2"><p>لب تو گفتا: اینک دلت به جان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدید تا نظر از دور ناردان لبت</p></div>
<div class="m2"><p>بسا که چشم مرا آب در دهان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیامد از دو جهان جز رخ تو در نظرم</p></div>
<div class="m2"><p>از آنگهی که مرا چشم در جهان آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روشنایی روی تو در شب تاریک</p></div>
<div class="m2"><p>نمی‌توان به سر کوی تو نهان آمد</p></div></div>