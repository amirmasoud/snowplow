---
title: >-
    غزل شمارهٔ ۲۶۷
---
# غزل شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>الا قم، واغتنم یوم التلاقی</p></div>
<div class="m2"><p>و در بالکاس وارفق بالرفاقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده جامی و بشکن توبهٔ من</p></div>
<div class="m2"><p>خلاصم ده ازین زهد نفاقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشعشعة اذا اسکرت منها</p></div>
<div class="m2"><p>فلا اضحوا الی یوم التلاقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازان باده که اول دادی، ای دوست</p></div>
<div class="m2"><p>بده بار دگر، گر هست باقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و ان لم یبق فی‌الدن الحمیا</p></div>
<div class="m2"><p>تدارک بالرحیق من الحداقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا باده مده، بوی خودم ده</p></div>
<div class="m2"><p>که از بوی تو سرمستیم، ساقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اما تسقی کئوس الوصل یوما</p></div>
<div class="m2"><p>الی کم کاس هجران تساق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وصلت شاد کن جانم، کزین بیش</p></div>
<div class="m2"><p>ندارد طاقت هجران عراقی</p></div></div>