---
title: >-
    غزل شمارهٔ ۱۸۷
---
# غزل شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>من آن قلاش و رند بی‌نوایم</p></div>
<div class="m2"><p>که در رندی مغان را پیشوایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گدای درد نوش می پرستم</p></div>
<div class="m2"><p>حریف پاکباز کم دغایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بند زهد و قرابی برستم</p></div>
<div class="m2"><p>نه مرد زرق و سالوس و ریایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ردا و طیلسان یکسو نهادم</p></div>
<div class="m2"><p>همه زنار شد بند قبایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر خاکم ز میخانه سرشتند</p></div>
<div class="m2"><p>که هر دم سوی میخانه گرایم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجایی، ساقیا، جامی به من ده</p></div>
<div class="m2"><p>که یک دم با حریفان خوش برآیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا برهان زخود، کز جان به جانم</p></div>
<div class="m2"><p>درین وحشت سرا تا چند پایم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمانی شادمان و خوش نبودم</p></div>
<div class="m2"><p>از آنم کاندرین وحشت سرایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا از درگه پاکان براندند</p></div>
<div class="m2"><p>به صد خواری، که رند ناسزایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برون کردندم از کعبه به خواری</p></div>
<div class="m2"><p>درون بتکده کردند جایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین ره خواستم زد دست و پایی</p></div>
<div class="m2"><p>بریدند، ای دریغا، دست و پایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بماندم در بیابان تحیر</p></div>
<div class="m2"><p>نه ره پیدا کنون، نه رهنمایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امید از هر که هست اکنون بریدم</p></div>
<div class="m2"><p>فتاده بر در لطف خدایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن است این همه بیداد بر من</p></div>
<div class="m2"><p>که پیوسته ز یار خود جدایم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بیداد زمانه وارهم من</p></div>
<div class="m2"><p>عراقی گر کند از کف رهایم</p></div></div>