---
title: >-
    غزل شمارهٔ ۷۴
---
# غزل شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>می روان کن ساقیا، کین دم روان خواهیم کرد</p></div>
<div class="m2"><p>بهر یک جرعه میت این دم روان خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردیی در ده، کزین جا دردسر خواهیم برد</p></div>
<div class="m2"><p>ساغری پر کن، که عزم آن جهان خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاروان عمر ازین منزل روان شد ناگهی</p></div>
<div class="m2"><p>چون روان شد کاروان، ما هم روان خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون فشاندیم آستین بی‌نیازی بر جهان</p></div>
<div class="m2"><p>دامن ناز اندر آن عالم کشان خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کف ساقی همت ساغری خواهیم خورد</p></div>
<div class="m2"><p>جرعه‌دان بزم خود هفت آسمان خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا فتد در ساغر ما عکس روی دلبری</p></div>
<div class="m2"><p>ساغر از باده لبالب هر زمان خواهیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درچنین مجلس که می‌عشق است‌و ساغربیخودی</p></div>
<div class="m2"><p>نالهٔ مستانه نقل دوستان خواهیم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا درین عالم نگردد آشکارا راز ما</p></div>
<div class="m2"><p>ناگهی رخ را ازین عالم نهان خواهیم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزد زلف دلربایش تحفه، دل خواهیم برد</p></div>
<div class="m2"><p>پیش روی جانفزایش جان فشان خواهیم کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بگردانیم رو، زین عالم بی‌آبرو</p></div>
<div class="m2"><p>روی در روی نگار مهربان خواهیم کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سر بازار وصلش جان ندارد قیمتی</p></div>
<div class="m2"><p>تا نظر در روی خوبش رایگان خواهیم کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سالها در جستجویش دست و پایی می‌زدیم</p></div>
<div class="m2"><p>چون نشان دیدیم، خود را بی‌نشان خواهیم کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چه ما خواهیم کردن او بخواهد غیر آن</p></div>
<div class="m2"><p>آنچه آن دلبر کند ما خود همان خواهیم کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عراقی هیچ خواهد گفت: اناالحق، این زمان</p></div>
<div class="m2"><p>بر سر دارش ز غیرت ناگهان خواهیم کرد</p></div></div>