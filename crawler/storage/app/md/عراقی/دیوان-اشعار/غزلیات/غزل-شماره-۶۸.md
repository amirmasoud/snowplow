---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>نگارا، بی تو برگ جان که دارد؟</p></div>
<div class="m2"><p>سر کفر و غم ایمان که دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر عشق تو خون من نریزد</p></div>
<div class="m2"><p>غمت را هر شبی مهمان که دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من با خیالت دوش می‌گفت:</p></div>
<div class="m2"><p>که این درد مرا درمان که دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب شیرین تو گفتا: ز من پرس</p></div>
<div class="m2"><p>که من با تو بگویم: کان که دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گفتی که: فردا روز وصل است</p></div>
<div class="m2"><p>امید زیستن چندان که دارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم در بند زلف توست ور نه</p></div>
<div class="m2"><p>سر سودای بی‌پایان که دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر لطف خیال تو نباشد</p></div>
<div class="m2"><p>عراقی را چنین حیران که دارد؟</p></div></div>