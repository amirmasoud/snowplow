---
title: >-
    غزل شمارهٔ ۲۰۹
---
# غزل شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>مقصود دل عاشق شیدا همه او دان</p></div>
<div class="m2"><p>مطلوب دل وامق و عذرا همه او دان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بینایی هر دیدهٔ بینا همه او بین</p></div>
<div class="m2"><p>زیبایی هر چهرهٔ زیبا همه او دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاری ده محنت زده مشناس جز او کس</p></div>
<div class="m2"><p>فریادرس بی‌کس تنها همه او دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سینهٔ هر غمزده پنهان همه او بین</p></div>
<div class="m2"><p>در دیدهٔ هر دلشده پیدا همه او دان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چیز که دانی جز از او، دان که همه اوست</p></div>
<div class="m2"><p>یا هیچ مدان در دو جهان، یا همه او دان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر لاله و گلزار و گلت گر نظر افتد</p></div>
<div class="m2"><p>گلزار و گل و لاله و صحرا همه او دان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور هیچ چپ و راست ببینی و پس و پیش</p></div>
<div class="m2"><p>پیش و پس و راست و چپ و بالا همه او دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور آرزویی هست به جز دوست تو را هیچ</p></div>
<div class="m2"><p>بایست، عراقی، و تمنا همه او دان</p></div></div>