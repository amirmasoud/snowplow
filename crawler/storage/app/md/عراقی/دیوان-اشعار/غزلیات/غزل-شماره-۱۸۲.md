---
title: >-
    غزل شمارهٔ ۱۸۲
---
# غزل شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>شاید که به درگاه تو عمری بنشینم</p></div>
<div class="m2"><p>در آرزوی روی تو، وانگاه ببینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب که از عمر دمی بیش نمانده است</p></div>
<div class="m2"><p>بشتاب، که اندر نفس باز پسینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد! که از هجر تو جانم به لب آمد</p></div>
<div class="m2"><p>هیهات! که دور از تو همه ساله چنینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم هوس آنکه ببینم رخ خوبت</p></div>
<div class="m2"><p>پس جان بدهم، نیست تمنی به جز اینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن رفت، دریغا! که مرا دین و دلی بود</p></div>
<div class="m2"><p>از دولت عشق تو نه دل ماند و نه دینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بهر عراقی، به درت آمده‌ام باز</p></div>
<div class="m2"><p>فرمای جوابی، بروم یا بنشینم؟</p></div></div>