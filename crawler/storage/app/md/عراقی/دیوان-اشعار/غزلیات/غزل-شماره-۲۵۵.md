---
title: >-
    غزل شمارهٔ ۲۵۵
---
# غزل شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>آمد به درت امیدواری</p></div>
<div class="m2"><p>کو را به جز از تو نیست یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محنت‌زده‌ای، نیازمندی</p></div>
<div class="m2"><p>خجلت‌زده‌ای، گناهکاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گفتهٔ خود سیاه‌رویی</p></div>
<div class="m2"><p>وز کردهٔ خویش شرمساری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از یار جدا فتاده عمری</p></div>
<div class="m2"><p>وز دوست بمانده روزگاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوده به درت چنان عزیزی</p></div>
<div class="m2"><p>دور از تو چنین بمانده خواری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرسند ز خاک درگه تو</p></div>
<div class="m2"><p>بیچاره به بوی یا غباری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید ز در تو باز گردد؟</p></div>
<div class="m2"><p>نومید، چنین امیدواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیبد که شود به کام دشمن</p></div>
<div class="m2"><p>از دوستی تو دوستداری؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخشای ز لطف بر عراقی</p></div>
<div class="m2"><p>کو ماند کنون و زینهاری</p></div></div>