---
title: >-
    غزل شمارهٔ ۱۰۶
---
# غزل شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>تا کی از ما یار ما پنهان بود؟</p></div>
<div class="m2"><p>چشم ما تا کی چنین گریان بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی از وصلش نصیب بخت ما</p></div>
<div class="m2"><p>محنت و درد دل و هجران بود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چنین کز یار دور افتاده‌ام</p></div>
<div class="m2"><p>گر بگرید دیده، جای آن بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دل ما خون شد از هجران او</p></div>
<div class="m2"><p>چشم ما شاید که خون افشان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فراقش دل ز جان آمد به جان</p></div>
<div class="m2"><p>خود گرانی یار مرگ جان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر امیدی زنده‌ام، ورنه که را</p></div>
<div class="m2"><p>طاقت آن هجر بی‌پایان بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیچ بر پیچ است بی او کار ما</p></div>
<div class="m2"><p>کار ما تا کی چنین پیچان بود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محنت آباد دل پر درد ما</p></div>
<div class="m2"><p>تا کی از هجران او ویران بود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درد ما را نیست درمان در جهان</p></div>
<div class="m2"><p>درد ما را روی او درمان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون دل ما از سر جان برنخاست</p></div>
<div class="m2"><p>لاجرم پیوسته سرگردان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون عراقی هر که دور از یار ماند</p></div>
<div class="m2"><p>چشم او گریان، دلش بریان بود</p></div></div>