---
title: >-
    غزل شمارهٔ ۲۱۹
---
# غزل شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>بی‌رخت جانا، دلم غمگین مکن</p></div>
<div class="m2"><p>رخ مگردان از من مسکین، مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود ز عشقت سینه‌ام خون کرده‌ای</p></div>
<div class="m2"><p>از فراقت دیده‌ام خونین مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من مسکین ستم تا کی کنی؟</p></div>
<div class="m2"><p>خستگی و عجز من می‌بین، مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند نالم از جفا و جور تو؟</p></div>
<div class="m2"><p>بس کن و بر من جفا چندین مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه می‌خواهی بکن، بر من رواست</p></div>
<div class="m2"><p>بی نصیبم زان لب شیرین مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر من خسته، که رنجور توام</p></div>
<div class="m2"><p>گر نمی‌گویی دعا، نفرین مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در همه عالم مرا دین و دلی است</p></div>
<div class="m2"><p>دل فدای توست، قصد دین مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواه با من لطف کن، خواهی جفا</p></div>
<div class="m2"><p>من نیارم گفت: کان کن، این مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با عراقی گر عتابی می‌کنی</p></div>
<div class="m2"><p>از طریق مهر کن، وز کین مکن</p></div></div>