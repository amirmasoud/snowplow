---
title: >-
    غزل شمارهٔ ۱۱۰
---
# غزل شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>نگارینی که با ما می‌نپاید</p></div>
<div class="m2"><p>به ما دلخستگان کی رخ نماید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا، ای بخت، تا بر خود بموییم</p></div>
<div class="m2"><p>که از ما یار آرامی نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر جانم به لب آید عجب نیست</p></div>
<div class="m2"><p>به حیله نیم جانی چند پاید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نقد این لحظه جانی میکن ای دل</p></div>
<div class="m2"><p>شب هجر است، تا فردا چه زاید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر روشن شود صبح امیدم</p></div>
<div class="m2"><p>مگر خورشید از روزن برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم را از غم جان وا رهاند</p></div>
<div class="m2"><p>مر از من زمانی در رباید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عراقی، بر درش امید در بند</p></div>
<div class="m2"><p>که داند، بو که ناگه واگشاید</p></div></div>