---
title: >-
    غزل شمارهٔ ۶۲
---
# غزل شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>با درد خستگانت درمان چه کار دارد؟</p></div>
<div class="m2"><p>با وصل کشتگانت هجران چه کار دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با محنت فراقت راحت چه رخ نماید؟</p></div>
<div class="m2"><p>با درد اشتیاقت درمان چه کار دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر در دلم خیالت ناید، عجب نباشد</p></div>
<div class="m2"><p>در دوزخ پر آتش رضوان چه کار دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودای تو نگنجد اندر دلی که جان است</p></div>
<div class="m2"><p>در خانهٔ طفیلی مهمان چه کار دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را خوش است با جان گر زآن توست، یارا</p></div>
<div class="m2"><p>بی‌روی تو دل من با جان چه کار دارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر بوی وصلت، ای جان، دل بر در تو مانده است</p></div>
<div class="m2"><p>ور نه فتاده در خاک چندان چه کار دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با عشق توست جان را صد سر سر نهفته</p></div>
<div class="m2"><p>لیکن دل عراقی با جان چه کار دارد؟</p></div></div>