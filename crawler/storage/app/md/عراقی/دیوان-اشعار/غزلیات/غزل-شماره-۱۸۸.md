---
title: >-
    غزل شمارهٔ ۱۸۸
---
# غزل شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>ما چو قدر وصلت، ای جان و جهان، نشناختیم</p></div>
<div class="m2"><p>لاجرم در بوتهٔ هجران تو بگداختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما که از سوز دل و درد جدایی سوختیم</p></div>
<div class="m2"><p>سوز دل را مرهم از مژگان دیده ساختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه ما خون جگر خوردیم از دست غمت</p></div>
<div class="m2"><p>جان ما خون گشت و دل در موج خون انداختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سماع دردمندان حاضر آ، یارا، دمی</p></div>
<div class="m2"><p>بشنو این سازی که ما از خون دل بنواختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری اندر جست‌و جویت دست و پایی می‌زدیم</p></div>
<div class="m2"><p>عمر ما، افسوس، بگذشت و تو را نشناختیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان چنین ماندیم اندر ششدر هجرت، که ما</p></div>
<div class="m2"><p>بر بساط راستی نزد وفا کژ باختیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون عراقی با غمت دیدیم خوش، ما همچو او</p></div>
<div class="m2"><p>از طرب فارغ شدیم و با غمت پرداختیم</p></div></div>