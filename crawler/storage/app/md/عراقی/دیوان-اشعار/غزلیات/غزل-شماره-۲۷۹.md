---
title: >-
    غزل شمارهٔ ۲۷۹
---
# غزل شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>ای که از لطف سراسر جانی</p></div>
<div class="m2"><p>جان چه باشد؟ که تو صد چندانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چه چیزی؟ چه بلایی؟ چه کسی؟</p></div>
<div class="m2"><p>فتنه‌ای؟ شنقصه‌ای؟ فتانی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حکمت از چیست روان بر همه کس؟</p></div>
<div class="m2"><p>کیقبادی؟ ملکی؟ خاقانی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دمی زنده کنی صد مرده</p></div>
<div class="m2"><p>عیسیی؟ آب حیاتی؟ جانی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تماشای تو آید همه کس</p></div>
<div class="m2"><p>لاله‌زاری؟ چمنی؟ بستانی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی در روی تو آرند همه</p></div>
<div class="m2"><p>قبله‌ای؟ آینه‌ای؟ جانانی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مذاق همه کس شیرینی</p></div>
<div class="m2"><p>انگبینی؟ شکری؟ سیلانی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه خردی، همه را در خوردی</p></div>
<div class="m2"><p>نمکی؟ آب روانی؟ نانی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آرزوی دل بیمار منی</p></div>
<div class="m2"><p>صحتی؟ عافیتی؟ درمانی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه خمارم شکنی، گه توبه</p></div>
<div class="m2"><p>می نابی؟ فقعی؟ رمانی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیدهٔ من به تو بیند عالم</p></div>
<div class="m2"><p>آفتابی؟ قمری؟ اجفانی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه خوبان به تو آراسته‌اند</p></div>
<div class="m2"><p>کهربایی؟ گهری؟ مرجانی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مهر هر روز دمی در بنده‌ات</p></div>
<div class="m2"><p>سحری؟ صبح‌دمی؟ خندانی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه در بزم ملوکت خوانند</p></div>
<div class="m2"><p>قصه‌ای؟ مثنویی؟ دیوانی؟</p></div></div>