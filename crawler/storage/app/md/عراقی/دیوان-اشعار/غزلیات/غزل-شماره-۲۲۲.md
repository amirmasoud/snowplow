---
title: >-
    غزل شمارهٔ ۲۲۲
---
# غزل شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>چه کنم که دل نسازم هدف خدنگ او من؟</p></div>
<div class="m2"><p>به چه عذر جان نبخشم به دو چشم شنگ او من؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کدام دل توانم که تن از غمش رهانم؟</p></div>
<div class="m2"><p>به چه حیله واستانم دل خود ز چنگ او من؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خدنگ غمزهٔ او دل و جان و سینه خورده</p></div>
<div class="m2"><p>پس ازین دگر چه بازم به سر خدنگ او من؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز غمش دو دیده خون گشت و ندید رنگ او چشم</p></div>
<div class="m2"><p>نچشیده طعم شکر ز دهان تنگ او من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل و دین به باد دادم به امید آنکه یابم</p></div>
<div class="m2"><p>خبری ز بوی زلفش، اثری ز رنگ او من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو نهنگ بحر عشقش دو جهان بدم فرو برد</p></div>
<div class="m2"><p>به چه حیله جان برآرم ز دم نهنگ او من؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب او چو شکر آمد، غم عشق او شرنگی</p></div>
<div class="m2"><p>بخورم به بوی لعلش، چو شکر شرنگ او من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عتاب گفت: عراقی، سر صلح تو ندارم</p></div>
<div class="m2"><p>همه عمر صلح کردم به عتاب و جنگ او من</p></div></div>