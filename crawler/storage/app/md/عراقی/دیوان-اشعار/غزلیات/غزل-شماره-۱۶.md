---
title: >-
    غزل شمارهٔ ۱۶
---
# غزل شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>شوری ز شراب خانه برخاست</p></div>
<div class="m2"><p>برخاست غریوی از چپ و راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چشم بتم چه فتنه انگیخت؟</p></div>
<div class="m2"><p>کز هر طرفی هزار غوغاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا جام لبش کدام می داد؟</p></div>
<div class="m2"><p>کز جرعه‌اش هر که هست شیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی، قدحی، که مست عشقم</p></div>
<div class="m2"><p>و آن باده هنوز در سر ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن نعرهٔ شور هم‌چنان هست</p></div>
<div class="m2"><p>وآن شیفتگی هنوز برجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کارم، که چو زلف توست در هم</p></div>
<div class="m2"><p>بی‌قامت تو نمی‌شود راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقصود تویی مرا ز هستی</p></div>
<div class="m2"><p>کز جام، غرض می مصفاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیینهٔ روی توست جانم</p></div>
<div class="m2"><p>عکس رخ تو درو هویداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل رنگ رخ تو دارد ، ارنه</p></div>
<div class="m2"><p>رنگ رخش از پی چه زیباست ؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور سرو نه قامت تو دیده است</p></div>
<div class="m2"><p>او را کشش از چه سوی بالاست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باغی است جهان، ز عکس رویت</p></div>
<div class="m2"><p>خرم دل آن که در تماشاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در باغ همه رخ تو بیند</p></div>
<div class="m2"><p>از هر ورق گل، آن که بیناست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از عکس رخت دل عراقی</p></div>
<div class="m2"><p>گلزار و بهار و باغ و صحراست</p></div></div>