---
title: >-
    غزل شمارهٔ ۹۰
---
# غزل شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>غلام حلقه به گوش تو زار باز آمد</p></div>
<div class="m2"><p>خوشی درو بنگر، کز ره دراز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به لطف، کار دل مستمند خسته بساز</p></div>
<div class="m2"><p>که خستگان را لطف تو در کارساز آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه باشد ار بنوازی نیازمندی را؟</p></div>
<div class="m2"><p>که با خیال رخت دم به دم به راز آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کرده‌ام که ز درگاه وصل جان افزا</p></div>
<div class="m2"><p>نصیب خسته دلم هجر جانگداز آمد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آستان درت صدهزار دل دیدم</p></div>
<div class="m2"><p>مگر که خاک سر کوت دلنواز آمد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبار خاک درت بر سر کسی که نشست</p></div>
<div class="m2"><p>ز سروران جهان گشت و سرفراز آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر طرف که شدم تا که شاد بنشینم</p></div>
<div class="m2"><p>غم تو پیش دل من دو اسبه باز آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به روی خرم تو شادمان نشد افسوس!</p></div>
<div class="m2"><p>دل عراقی از آن دم که عشقباز آمد</p></div></div>