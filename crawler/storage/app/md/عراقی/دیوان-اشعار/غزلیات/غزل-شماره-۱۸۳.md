---
title: >-
    غزل شمارهٔ ۱۸۳
---
# غزل شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>شود میسر و گویی که در جهان بینم؟</p></div>
<div class="m2"><p>که باز با تو دمی شادمانه بنشینم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گوش دل سخن دلگشای تو شنوم؟</p></div>
<div class="m2"><p>به چشم جان رخ راحت فزای تو بینم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه در خور تو نیستم، قبولم کن</p></div>
<div class="m2"><p>اگر بدم و اگر نیک، چون کنم؟ اینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سوی من گذری کن، که سخت مشتاقم</p></div>
<div class="m2"><p>به حال من نظری کن که، سخت مسکینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بود من اثری در جهان نبودی، گر</p></div>
<div class="m2"><p>امید وصل ندادی همیشه تسکینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان خوشم که مرا جان به لب رسید، آری</p></div>
<div class="m2"><p>ازان سبب دو لب توست جان شیرینم</p></div></div>