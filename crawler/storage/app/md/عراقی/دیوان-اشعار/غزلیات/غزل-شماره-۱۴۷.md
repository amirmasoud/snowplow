---
title: >-
    غزل شمارهٔ ۱۴۷
---
# غزل شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>بیا، که خانهٔ دل پاک کردم از خاشاک</p></div>
<div class="m2"><p>درین خرابه تو خود کی قدم نهی؟ حاشاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار دل کنی از غم خراب و نندیشی</p></div>
<div class="m2"><p>هزار جان به لب آری، ز کس نداری باک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام دل که ز جور تو دست بر سر نیست؟</p></div>
<div class="m2"><p>کدام جان که نکرد از جفات بر سر خاک؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم، که خون جگر می‌خورد ز دست غمت،</p></div>
<div class="m2"><p>در انتظار تو صد زهر خورده بی تریاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون که جان به لب آمد مپیچ در کارم</p></div>
<div class="m2"><p>مکن، که کار من از تو بماند در پیچاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه هیچ کیسه‌بری همچو طره‌ات طرار</p></div>
<div class="m2"><p>نه هیچ راهزنی همچو غمزه‌ات چالاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به طره صید کنی صدهزار دل هر دم</p></div>
<div class="m2"><p>به غمزه بیش کشی هر نفس دو صد غمناک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل عراقی مسکین، که صید لاغر توست</p></div>
<div class="m2"><p>چو می کشیش میفگن، ببند بر فتراک</p></div></div>