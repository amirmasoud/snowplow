---
title: >-
    غزل شمارهٔ ۲۴۵
---
# غزل شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>ای دوست الغیاث! که جانم بسوختی</p></div>
<div class="m2"><p>فریاد! کز فراق روانم بسوختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بوتهٔ بلا تن زارم گداختی</p></div>
<div class="m2"><p>در آتش عنا دل و جانم بسوختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانم که: سوختی ز غم عشق خود مرا</p></div>
<div class="m2"><p>لیکن ندانم آنکه چه سانم بسوختی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌سوزیم درون و تو در وی نشسته‌ای</p></div>
<div class="m2"><p>پیدا نمی‌شود، که نهانم بسوختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاتش چگونه سوزد پروانه؟ دیده‌ای؟</p></div>
<div class="m2"><p>ز اندیشهٔ فراق چنانم بسوختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سود و زیان من، ز جهان، جز دلی نبود</p></div>
<div class="m2"><p>آتش زدی و سود و زیانم بسوختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کی ز حسرت تو برآرم ز سینه آه؟</p></div>
<div class="m2"><p>کز آه سوزناک زیانم بسوختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خاک درگه تو تپیدم بسی ز غم</p></div>
<div class="m2"><p>چو مرغ نیم کشته تپانم بسوختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا گفتمت که: کام عراقی ز لب بده</p></div>
<div class="m2"><p>کامم گداختی و زبانم بسوختی</p></div></div>