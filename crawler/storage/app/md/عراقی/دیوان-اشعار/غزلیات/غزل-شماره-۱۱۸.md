---
title: >-
    غزل شمارهٔ ۱۱۸
---
# غزل شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>ای باد صبا، به کوی آن یار</p></div>
<div class="m2"><p>گر بر گذری ز بنده یاد آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور هیچ مجال گفت یابی</p></div>
<div class="m2"><p>پیغام من شکسته بگزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با یار بگوی کان شکسته</p></div>
<div class="m2"><p>این خسته جگر، غریب و غم‌خوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون از تو ندید چارهٔ خویش</p></div>
<div class="m2"><p>بیچاره بماند بی‌تو ناچار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید رخت ندید روزی</p></div>
<div class="m2"><p>بی‌نور بماند در شب تار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی این شب تیره دید روشن</p></div>
<div class="m2"><p>نی خفته عدو، نه بخت بیدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کرد شبی به روز کاخر</p></div>
<div class="m2"><p>روزی بشود که به شود کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کارش چو به جان رسید می‌گفت:</p></div>
<div class="m2"><p>کای کرده به تیغ هجرم افگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای کرده به کام دشمنانم</p></div>
<div class="m2"><p>با یار چنین، چنین کند یار؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخر نظری به حال من کن</p></div>
<div class="m2"><p>بنگر که: چگونه بی‌توام زار؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک بارگیم مکن فراموش</p></div>
<div class="m2"><p>یاد آر ز من شکسته، یاد آر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مآزار ز من، که هیچ هیچم</p></div>
<div class="m2"><p>از هیچ، کسی نگیرد آزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من نیک بدم، تو نیکویی کن</p></div>
<div class="m2"><p>ای نیک، بدم، به نیک بردار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگذار که بگذرم به کویت</p></div>
<div class="m2"><p>یکدم ز سگان کویم انگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگذاشتم این حدیث، کز من</p></div>
<div class="m2"><p>دارند سگان کوی تو عار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پندار که مشت خاک باشم</p></div>
<div class="m2"><p>زیر قدم سگ درت خوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>القصه به جانم از عراقی</p></div>
<div class="m2"><p>مگذار، کزو نماند آثار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بالجمله تو باشی و تو گویی</p></div>
<div class="m2"><p>او کم کند از میانه گفتار</p></div></div>