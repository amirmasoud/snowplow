---
title: >-
    غزل شمارهٔ ۵
---
# غزل شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ندیدم در جهان کامی دریغا</p></div>
<div class="m2"><p>بماندم بی‌سرانجامی دریغا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوارنده نشد از خوان گیتی</p></div>
<div class="m2"><p>مرا جز غصه‌آشامی دریغا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشد از بزم وصل خوبرویان</p></div>
<div class="m2"><p>نصیب بخت من جامی دریغا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا دور از رخ دلدار دردی است</p></div>
<div class="m2"><p>که آن را نیست آرامی دریغا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرو شد روز عمر و بر نیامد</p></div>
<div class="m2"><p>از آن شیرین لبش کامی دریغا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین امید عمرم رفت کاخر:</p></div>
<div class="m2"><p>کند یادم به پیغامی دریغا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو وادیدم عراقی نزد آن دوست</p></div>
<div class="m2"><p>نمی‌ارزد به دشنامی دریغا</p></div></div>