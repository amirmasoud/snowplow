---
title: >-
    غزل شمارهٔ ۲۸۵
---
# غزل شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>بیا، تا بیدلان را زار بینی</p></div>
<div class="m2"><p>روان خستگان افکار بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن درماندگان رنجور یابی</p></div>
<div class="m2"><p>دل بیچارگان بیمار بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کوی عاشقان خود گذر کن</p></div>
<div class="m2"><p>که مشتاقان خود را زار بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان خاک و خون افتاده حیران</p></div>
<div class="m2"><p>زهر جانب دو صد خونخوار بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسا جان عزیز مستمندان</p></div>
<div class="m2"><p>که بر خاک در خود خوار بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی اندر دل زار ضعیفان</p></div>
<div class="m2"><p>نظر کن، تا غم و تیمار بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبینی هیچ شادی در دل ما</p></div>
<div class="m2"><p>ولی اندوه و غم بسیار بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا، با این همه امید دربند</p></div>
<div class="m2"><p>که هم روزی رخ دلدار بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو افتادی، عراقی، رو مگردان</p></div>
<div class="m2"><p>اگر خواهی که روی یار بینی</p></div></div>