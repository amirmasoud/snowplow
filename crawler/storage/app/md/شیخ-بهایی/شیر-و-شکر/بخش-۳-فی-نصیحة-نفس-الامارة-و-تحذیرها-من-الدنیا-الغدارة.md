---
title: >-
    بخش ۳ - فی نصیحة نفس الامارة و تحذیرها من الدنیا الغدارة
---
# بخش ۳ - فی نصیحة نفس الامارة و تحذیرها من الدنیا الغدارة

<div class="b" id="bn1"><div class="m1"><p>ای باد صبا، به پیام کسی</p></div>
<div class="m2"><p>چو به شهر خطاکاران برسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر ز محلهٔ مهجوران</p></div>
<div class="m2"><p>وز نفس و هوی ز خدا دوران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانگاه بگو به بهائی زار</p></div>
<div class="m2"><p>کای نامه سیاه و خطا کردار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کای عمر تباه گنه پیشه!</p></div>
<div class="m2"><p>تا چند زنی تو به پا تیشه؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دم به خود آی و به‌آیین چه کسی</p></div>
<div class="m2"><p>به چه بسته دل، به که همنفسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد عمر تو شصت و همان پستی</p></div>
<div class="m2"><p>وز بادهٔ لهو و لعب مستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که مگر چو به سی برسی</p></div>
<div class="m2"><p>یابی خود را، دانی چه کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درسی، درسی ز کتاب خدا</p></div>
<div class="m2"><p>رهبر نشدت به طریق هدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز سی به چهل، چو شدی واصل</p></div>
<div class="m2"><p>جز جهل از چهل، نشدت حاصل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اکنون، چو به شصت رسیدت سال</p></div>
<div class="m2"><p>یک دم نشدی فارغ ز وبال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در راه خدا، قدمی نزدی</p></div>
<div class="m2"><p>بر لوح وفا، رقمی نزدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مستی ز علایق جسمانی</p></div>
<div class="m2"><p>رسوا شده‌ای و نمی‌دانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از اهل غرور، ببر پیوند</p></div>
<div class="m2"><p>خود را به شکسته دلان بربند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیشه چو شکست، شود ابتر</p></div>
<div class="m2"><p>جز شیشهٔ دل که شود بهتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای ساقی بادهٔ روحانی</p></div>
<div class="m2"><p>زارم ز علایق جسمانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک لمعه ز عالم نورم بخش</p></div>
<div class="m2"><p>یک جرعه ز جام طهورم بخش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کز سرفکنم به صد آسانی</p></div>
<div class="m2"><p>این کهنه لحاف هیولانی</p></div></div>