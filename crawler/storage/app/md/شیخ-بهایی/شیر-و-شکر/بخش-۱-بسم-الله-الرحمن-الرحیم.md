---
title: >-
    بخش ۱ - بسم الله الرحمن الرحیم
---
# بخش ۱ - بسم الله الرحمن الرحیم

<div class="b" id="bn1"><div class="m1"><p>ای مرکز دایرهٔ امکان</p></div>
<div class="m2"><p>وی زبدهٔ عالم کون و مکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شاه جواهر ناسوتی</p></div>
<div class="m2"><p>خورشید مظاهر لاهوتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی ز علایق جسمانی</p></div>
<div class="m2"><p>در چاه طبیعت تن مانی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند، به تربیت بدنی</p></div>
<div class="m2"><p>قانع به خزف ز در عدنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد ملک ز بهر تو چشم به راه</p></div>
<div class="m2"><p>ای یوسف مصری، به در آی از چاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا والی مصر وجود شوی</p></div>
<div class="m2"><p>سلطان سریر شهود شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در روز الست، بلی گفتی</p></div>
<div class="m2"><p>امروز، به بستر لا خفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کی ز معارف عقلی دور</p></div>
<div class="m2"><p>به ز خارف عالم حس، مغرور؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از موطن اصل، نیاری یاد</p></div>
<div class="m2"><p>پیوسته، به لهو و لعب دلشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه اشک روان، نه رخ زردی</p></div>
<div class="m2"><p>الله الله، تو چه بی‌دردی!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک دم، به خود آی و ببین چه کسی</p></div>
<div class="m2"><p>به چه دل بسته‌ای، به که همنفسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین خواب گران، بردار سری</p></div>
<div class="m2"><p>برگیر ز عالم اولین، خبری</p></div></div>