---
title: >-
    بخش دوم - قسمت اول
---
# بخش دوم - قسمت اول

<div class="n" id="bn1"><p>حکیمی گفت: نیکوترین چیزها سه چیز است: زندگانی، بالاتر از نعمت زندگانی و آنچه از نعمت زندگانی برتر است. زندگانی اما آسایش و روزی فراخ است. بالاتر از نعمت زندگانی، نام نیکو و ستودگی است.</p></div>
<div class="n" id="bn2"><p>اما آنچه از نعمت زندگانی برتر است خشنودی خداوند تعالی است. بدترین چیزها نیز سه چیز است. مرگ و بالاتر از مرگ و بدتر از مرگ.</p></div>
<div class="n" id="bn3"><p>مرگ اما تنگدستی و بی چیزی است. بالاتر از مرگ سرزنش مردمان است و آنچه بدتر از مرگ است، خشم خداوندی است که از آن هم بدو پناه همی بریم.</p></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه از آتش دل چون خم می درجوشم</p></div>
<div class="m2"><p>مهر بر لب زده خون می خورم و خاموشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصد جان است طمع در لب جانان کردن</p></div>
<div class="m2"><p>تو مرا بین که در این کار به جان می کوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاش الله که نیم معتقد طاعت خویش</p></div>
<div class="m2"><p>این قدر هست که گه گه قدحی می نوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست امیدم که علیرغم عدو روز جزا</p></div>
<div class="m2"><p>فیض عفوش ننهد بار گنه بر دوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پدرم روضه ی رضوان بدو گندم بفروخت</p></div>
<div class="m2"><p>ناخلف باشم اگر من به جوی نفروشم</p></div></div>
<div class="n" id="bn9"><p>مردی را بنزد منصور خلیفه آوردند که بدگوئی وی کرده بود. منصور از او بازپرسید. مرد به گفتن حجت خویش آغاز کرد.</p></div>
<div class="n" id="bn10"><p>منصور گفت: نزد من نیز چرات سخن گفتن همی کنی؟ مرد گفت: ای امیر، خداوند عزوجل می فرماید: «یوم تأتی کل نفس تجادل عن نفسها».</p></div>
<div class="n" id="bn11"><p>آیا در عین این که تو با خداوند مجادله همیکنی من با تو سخن نگویم؟ سخن وی منصور را از پاسخ عاجز ساخت. وی را ببخشود و فرمان داد جایزه اش دهند.</p></div>
<div class="b" id="bn12"><div class="m1"><p>شبی تاریک چون دریای پر قیر</p></div>
<div class="m2"><p>به دریا در فکنده چشمه ی شیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زجنبیدن فلک بیکار گشته</p></div>
<div class="m2"><p>ستاره در رهش سیار گشته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوادش تیره چون سودای خامان</p></div>
<div class="m2"><p>بدامان قیامت بسته دامان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غنوده در عدم صبح شب افروز</p></div>
<div class="m2"><p>به قیر انباشته دروازه ی روز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به کنج صبح قفل افکنده افلاک</p></div>
<div class="m2"><p>کلید گنج را گم کرده در خاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان چون اژدهای پیچ در پیچ</p></div>
<div class="m2"><p>بجز دود سیه گردش دگر هیچ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شبی زین گونه تاریک و جگر سوز</p></div>
<div class="m2"><p>ز غم بی خواب شیرین سیه روز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر چه پاسبان بیدار باشد</p></div>
<div class="m2"><p>نه همچون عاشق بیمار باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آب دیده با شب راز می گفت</p></div>
<div class="m2"><p>ز روز بد حکایت باز می گفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کزین بی مهری و تاریک روئی</p></div>
<div class="m2"><p>شبی باری زبخت من نگوئی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیابان شو که من زین بی قراری</p></div>
<div class="m2"><p>بخواهم مرد از این شب زنده داری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر سوگند خوردی ای جهان سوز</p></div>
<div class="m2"><p>که بعد از مردن شیرین شوی روز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه خسبی، خیز ای صبح سیه روی</p></div>
<div class="m2"><p>به آب چشم من رخ را فروشوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگر کردی تو هم زآشوب غم جوش</p></div>
<div class="m2"><p>که کردی خنده را چون من فراموش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرفتم کز خمار باده ی دوش</p></div>
<div class="m2"><p>صبوحی گشت مستان را فراموش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه شد، یارب، به گه خیزان شب را</p></div>
<div class="m2"><p>که در تسبیح نگشادند لب را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگر بگسست نای مطرب پیر</p></div>
<div class="m2"><p>که برناورد امشب ناله ی زیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مگر بر نوبتی خواب اشتلم کرد</p></div>
<div class="m2"><p>که امشب خاستن را وقت گم کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مگر شد بسته مرغ صبح در دام</p></div>
<div class="m2"><p>که بانگی بر نمی آرد به هنگام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گهی باشد که این شب و روز گردد</p></div>
<div class="m2"><p>دل پرسوز من بی سوز گردد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از این ظلمات غم یابم رهائی</p></div>
<div class="m2"><p>به چشم خویش بینم روشنائی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسی می کرد زینسان ناامیدی</p></div>
<div class="m2"><p>که ناگه از افق سرزد سفیدی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو لاله گرچه بودش بر جگر داغ</p></div>
<div class="m2"><p>ز باغ صبحدم بشکفت چون باغ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه خوش بادی است باد صبحگاهی</p></div>
<div class="m2"><p>کز او در جنبش آید مرغ و ماهی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در آن دم هر دلی کافسرده باشد</p></div>
<div class="m2"><p>اگر زنده نگردد، مرده باشد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دلی کو نور صبح راستین یافت</p></div>
<div class="m2"><p>کلید کار خود در آستین یافت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همان در زن که ملک عالم آنجاست</p></div>
<div class="m2"><p>وگر زان بیشتر خواهی هم آن جاست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو شیرین یافت نور صبحدم را</p></div>
<div class="m2"><p>به روشن خاطری برزد علم را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به مسکینی جبین بر خاک مالید</p></div>
<div class="m2"><p>ز دل پیش خدای پاک نالید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که ای در هر دلی داننده ی راز</p></div>
<div class="m2"><p>به بخشایش درت بربندگان باز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز ناکامی دلم تنگ آمد از زیست</p></div>
<div class="m2"><p>تو میدانی که کام چون منی چیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو تو امید هر امیدواری</p></div>
<div class="m2"><p>امیدم هست کامیدم برآری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جز این در دل ندارم آرزوئی</p></div>
<div class="m2"><p>که یابم از وصال دوست بوئی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درونم سوخت زین حاجت نهانی</p></div>
<div class="m2"><p>گرم حاجت برآری میتوانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نشاطی ده کزین غم شاد گردم</p></div>
<div class="m2"><p>ز زندان فراق آزاد گردم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به سر کبریا در پرده ی غیب</p></div>
<div class="m2"><p>به وحی انبیا در حرف لاریب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به نور مخلصان در روسفیدی</p></div>
<div class="m2"><p>به صبر مفلسان در ناامیدی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بدان تاریک زندان مغاکی</p></div>
<div class="m2"><p>ببالین فراموشان خاکی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به خون غازیان در قطع پیوند</p></div>
<div class="m2"><p>بسوز مادران در مرگ فرزند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به آهی کز سرشوری برآید</p></div>
<div class="m2"><p>به خاری کز سرگوری برآید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به مهراندوه دل های کریمان</p></div>
<div class="m2"><p>به گرد آلوده سرهای یتیمان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به شبهای سیاه تنگدستان</p></div>
<div class="m2"><p>به دل های سفید حق پرستان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به عشق نو در آغاز جوانی</p></div>
<div class="m2"><p>به غمهای کهن در دل نهانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدان بیدل که هستی نایدش یاد</p></div>
<div class="m2"><p>بدان دل کوبود در نیستی شاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدان سینه که دارد عشق جاوید</p></div>
<div class="m2"><p>به هجرانی که هست از وصل نومید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که برداری غم از پیرامن من</p></div>
<div class="m2"><p>نهی مقصود من در دامن من</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گرفتارم بدست نفس خود رای</p></div>
<div class="m2"><p>به رحمت بر گرفتاران ببخشای</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگرچه ماجرا هست از ادب دور</p></div>
<div class="m2"><p>تو آنی کز تو نتوان داشت مستور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از تهتک مکن اندیشه و چون گل خوش باش</p></div>
<div class="m2"><p>زانکه تمکین جهان گذران این همه نیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یک چند طریق رهروان گیرم پیش</p></div>
<div class="m2"><p>وز ناز ونعیم یاد نارم کم و بیش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مردانه در این راه بپویم پس و پیش</p></div>
<div class="m2"><p>باشد که رسم به آرزوی دل خویش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ای کرده رخت غارت هوش و دل من</p></div>
<div class="m2"><p>عشق تو شده خانه فروش دل من</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سری که مقربان از آن محرومند</p></div>
<div class="m2"><p>عشق تو فرو گفت به گوش دل من</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جان در طلب وصال تو شیدائی شد</p></div>
<div class="m2"><p>دل در خم گیسوی تو سودائی شد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اندر طلب وصل تو گرد جهان</p></div>
<div class="m2"><p>بیچاره دلم بگشت و هر جائی شد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جامی که نامه ی عملش را نیامده</p></div>
<div class="m2"><p>عنوان بغیر مظلمه، مضمون بجز گناه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>موی سیاه را به هوس می کند سفید</p></div>
<div class="m2"><p>روی سفید را به گنه می کند سیاه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>حالش تب ندامت و آه و خجالت است</p></div>
<div class="m2"><p>هرگز مباد حال کسی این چنین تباه</p></div></div>
<div class="n" id="bn70"><p>با آن که جان، بجز تن است، ادراک آن هماره با ادراک این همراه است. چنان که ما به محض آن که تصور زید کنیم، تنش نیز در تصور بیاید.</p></div>
<div class="n" id="bn71"><p>و این به واسطه ی اتصال و تعلق بسیار تن وجان است. به همین سبب نیز برخی آن دو را یکی دانسته اند. جامی در این معنی نیک سروده است:</p></div>
<div class="b" id="bn72"><div class="m1"><p>زآمیزش جسم و آلایش جان</p></div>
<div class="m2"><p>چنان گشتی از جوهر خویش غافل</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که جان رابه صد فکرت از تن بدانی</p></div>
<div class="m2"><p>زهی فکر باطل زهی جهل کامل</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>کهتر و مهمتر و وضیع و شریف</p></div>
<div class="m2"><p>همه از روزگار رنجورند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دوستان گر بدوستان نرسند</p></div>
<div class="m2"><p>در چنین روزگار معذورند</p></div></div>
<div class="n" id="bn76"><p>از کتاب صفوة الصفوه ی ابوالفرج بن جوزی از امام جعفر بن محمدالصادق (ع) نقل است که فرمود: «کار نیکو جز با سه چیز کامل نیابد: تعجیلش، کوچک شمردنش، و پوشاندنش.</p></div>
<div class="n" id="bn77"><p>نیز فرمود: کسی از تندی و سخط کس نبرد، سپاسگزاری نعمت نکند. وی را از فضیلت علی(ع) پرسیدند که کسی در آن شریک وی نیست.</p></div>
<div class="n" id="bn78"><p>پاسخ داد: این که از خویشاوندان رسول به پیشی در اسلام پیش است و از غیر خویشاوندان، به خویشاوندی. نیز فرمود: قرآن را ظاهری نیکو و باطنی ژرف است.</p></div>
<div class="n" id="bn79"><p>نیز فرمود: از مشاجره ی شاعران بپرهیزید. چه در مدح گفتن دست خشک اند و در هجا گفتن دست و دل باز. کسی که فتنه ای را برانگیزد، خود بدان متبلا شود.</p></div>
<div class="n" id="bn80"><p>وی را پرسیدند در علاقه به فرزند خویش موسی(ع) به کدام حد رسیدی؟ فرمود: تا آن جا که دل همیخواست جز او فرزندیم نبود تا کسی در علاقه ی من بدو شریک نشود.</p></div>
<div class="b" id="bn81"><div class="m1"><p>اهل عصیان به تولای تو گر تکیه کنند</p></div>
<div class="m2"><p>معصیت ناز کند روز جزا بر غفران</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مرا حاجئی شانه ی عاج داد</p></div>
<div class="m2"><p>که رحمت بر اخلاق حجاج باد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شنیدم که باری سگم خوانده بود</p></div>
<div class="m2"><p>که از من به نوعی دلش رانده بود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بینداختم شانه کاین استخوان</p></div>
<div class="m2"><p>نمیبایدم، دیگرم سگ مخوان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>برگ عیش دگران روز بروز افزون است</p></div>
<div class="m2"><p>خرمن سوخته ی ماست که با خاک یکی است</p></div></div>
<div class="n" id="bn86"><p>در کافی، از امام صادق جعفر بن محمد(ع) روایت شده است که گفت: پیامبر خدا (ص) پیش از غذا بهر نماز بیرون شد. تکه نانی را که در شیرزده بود، در دست داشت. همی خورد و همی رفت و بلال مردمان را به نماز همی خواند.</p></div>
<div class="n" id="bn87"><p>نیز در همان کتاب از امیرمومنان(ع) روایت است که فرمود: اینکه شخص غذا خوران راه رود مانعی ندارد. پیامبر(ص) نیز چنین می کرد.</p></div>
<div class="b" id="bn88"><div class="m1"><p>به کم خوردن از عادت خواب و خورد</p></div>
<div class="m2"><p>توان خویشتن را ملک خوی کرد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نخست آدمی سیرتی پیشه کن</p></div>
<div class="m2"><p>پس آن گه ملک خوئی اندیشه کن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به اندازه خور زاد گر مردمی</p></div>
<div class="m2"><p>چنین پر شکم آدمی یا خمی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شکم جای قوت است و جای نفس</p></div>
<div class="m2"><p>تو پنداری از بهر نان است و بس</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دو چشم و شکم پر نگردد بهیچ</p></div>
<div class="m2"><p>تهی بهتر این روده ی پیچ پیچ</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شکم بند دست است و زنجیر پای</p></div>
<div class="m2"><p>شکم بنده، نادر پرستد خدای</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>برو اندرونی بدست آر پاک</p></div>
<div class="m2"><p>شکم پر نخواهد شد الا بخاک</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ای دست تو در جفا زلف تو دراز</p></div>
<div class="m2"><p>وی بی سببی کشیده پا از من باز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>وی دست زآستین برون کرده به عهد</p></div>
<div class="m2"><p>امروز کشیده پای در دامن ناز</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گفتی که فلان زیاد من خاموش است</p></div>
<div class="m2"><p>و زباده ی شوق دیگران مدهوش است</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>شرمت ناید هنوز خاک در تو</p></div>
<div class="m2"><p>از گرمی خون دل من در جوش است</p></div></div>
<div class="n" id="bn99"><p>از ابن مسعود نقل است که گفت: نماز پیمانه است. آن کس که ایفایش کرد، بهر خود ایفا کرده است. و آن کس که آن را کم تر ایفا کرد، شنیده اید که خداوند در مورد مطففین چه فرموده است:</p></div>
<div class="n" id="bn100"><p>زاهدی را گفتند: بهر چه دنیا را ترک گفتی؟ گفت: از آن که مرا از صافیش منع کردند و من خود از کدرش امتناع کردم.</p></div>
<div class="n" id="bn101"><p>حکیمی را گفتند: نصیب خویش از دنیا برگیر، چرا که دنیا بزودی زوال یابد. حکیم گفت: اکنون که چنین است، نباید نصیب خویش از آن برگیرم.</p></div>