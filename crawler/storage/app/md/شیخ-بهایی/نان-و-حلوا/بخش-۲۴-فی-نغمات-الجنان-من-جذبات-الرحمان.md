---
title: >-
    بخش ۲۴ - فی نغمات الجنان من جذبات الرحمان
---
# بخش ۲۴ - فی نغمات الجنان من جذبات الرحمان

<div class="b" id="bn1"><div class="m1"><p>اشف قلبی، ایها الساقی الرحیم</p></div>
<div class="m2"><p>بالتی یحیی بها العظم الرمیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زوج الصهباء بالماء الزلال</p></div>
<div class="m2"><p>واجعلن عقلی لها مهرا حلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنت کرم تجعلن الشیخ شاب</p></div>
<div class="m2"><p>من یذق منها عن الکونین غاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمرة من نار موسی نورها</p></div>
<div class="m2"><p>دنها قلبی و صدری طورها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قم فلاتمهل، فما فی‌العمر مهل</p></div>
<div class="m2"><p>لا تصعب شربها و الامر سهل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قم فلاتمهل فان الصبح لاح</p></div>
<div class="m2"><p>والثریا غربت والدیک صاح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قل لشیخ قلبه منها نفور</p></div>
<div class="m2"><p>لا تخف، فالله تواب غفور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا مغنی ان عندی کل غم</p></div>
<div class="m2"><p>قم والق النار فیها بالنغم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا مغنی قم فان العمر ضاع</p></div>
<div class="m2"><p>لا یطیب العیش الا بالسماع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انت ایضا یا مغنی لا تنم</p></div>
<div class="m2"><p>قم واذهب عن فؤادی کل غم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غن لی دورا، فقد دار القدح</p></div>
<div class="m2"><p>والصبا قد فاح والقمری صدح</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>واذکرن عندی احادیث الحبیب</p></div>
<div class="m2"><p>ان عیشی من سواها لا یطیب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>واذکرن ذکری احادیث الفراق</p></div>
<div class="m2"><p>ان ذکر البعد مما لا یطاق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روحن روحی باشعار العرب</p></div>
<div class="m2"><p>کی یتم الحظ فینا والطرب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وافتحن منها بنظم مستطاب</p></div>
<div class="m2"><p>قلته فی بعض ایام الشباب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قد صرفنا العمر فی قیل و قال</p></div>
<div class="m2"><p>یا ندیمی! قم فقد ضاق المجال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ثم اطربنی باشعار العجم</p></div>
<div class="m2"><p>و اطردن همتا علی قلبی هجم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وابتداء منها ببیت المثنوی</p></div>
<div class="m2"><p>للحکیم المولوی المعنوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>« بشنو از نی، چون حکایت می‌کند</p></div>
<div class="m2"><p>وز جداییها، شکایت می‌کند»</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قم و خاطبنی بکل الالسنه</p></div>
<div class="m2"><p>عل قلبی ینتبة من ذی السنه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>انه فی غفلة عن حاله</p></div>
<div class="m2"><p>خائض فی قیله مع قاله</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کل ان فهو فی قید جدید</p></div>
<div class="m2"><p>قائلا من جهله: هل من مزید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تائه فی الغی قد ضل الطریق</p></div>
<div class="m2"><p>قط من سکرالهوی لا یستفیق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عاکف دهرا، علی اصنامه</p></div>
<div class="m2"><p>تنفر الکفار من اسلامه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کم انادی و هو لایسقی یصغی؟ التناد</p></div>
<div class="m2"><p>وافادی، وافادی، وافاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یا بهائی اتخذ قلبا سواه</p></div>
<div class="m2"><p>فهو ما معبوده الا هواه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر چت از حق باز دارد ای پسر</p></div>
<div class="m2"><p>نام کردن، نان و حلوا، سر به سر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر همی خواهی که باشی تازه‌جان</p></div>
<div class="m2"><p>رو کتاب نان و حلوا را بخوان</p></div></div>