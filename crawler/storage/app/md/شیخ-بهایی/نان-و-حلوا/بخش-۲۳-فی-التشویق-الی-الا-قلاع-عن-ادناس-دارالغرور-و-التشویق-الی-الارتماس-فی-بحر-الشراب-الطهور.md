---
title: >-
    بخش ۲۳ - فی التشویق الی الا قلاع عن ادناس دارالغرور و التشویق الی الارتماس فی بحر الشراب الطهور
---
# بخش ۲۳ - فی التشویق الی الا قلاع عن ادناس دارالغرور و التشویق الی الارتماس فی بحر الشراب الطهور

<div class="b" id="bn1"><div class="m1"><p>یا ندیمی ضاع عمری وانقضی</p></div>
<div class="m2"><p>قم لاستدراک وقت قدمضی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واغسل الادناس عنی بالمدام</p></div>
<div class="m2"><p>واملا الاقداح منها یا غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اعطنی کأسا من الخمر الطهور</p></div>
<div class="m2"><p>انها مفتاح ابواب السرور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلص الارواح من قیدالهموم</p></div>
<div class="m2"><p>اطلق الاشباح من اسر الغموم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاندرین ویرانهٔ پر وسوسه</p></div>
<div class="m2"><p>دل گرفت از خانقاه و مدرسه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی ز خلوت کام بردم، نی ز سیر</p></div>
<div class="m2"><p>نی ز مسجد طرف بستم، نی ز دیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالمی خواهم از این عالم به در</p></div>
<div class="m2"><p>تا به کام دل کنم خاکی به سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صلح کل کردیم با کل بشر</p></div>
<div class="m2"><p>تو به ما خصمی کن و نیکی نگر</p></div></div>