---
title: >-
    بخش ۱۹ - فی ذم المتمکنین فی المناصب الدنیویة للحظوظ الواهیة الدنیة
---
# بخش ۱۹ - فی ذم المتمکنین فی المناصب الدنیویة للحظوظ الواهیة الدنیة

<div class="b" id="bn1"><div class="m1"><p>نان و حلوا چیست؟ ای فرزانه مرد</p></div>
<div class="m2"><p>منصب دنیاست، گرد آن مگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بیالایی از او دست و دهان</p></div>
<div class="m2"><p>روی آسایش نبینی در جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منصب دنیا نمی‌دانی که چیست؟</p></div>
<div class="m2"><p>من بگویم با تو، یک ساعت بایست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه بندد از ره حق پای مرد</p></div>
<div class="m2"><p>آنکه سازد کوی حرمان جای مرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه نامش مایهٔ بدنامی است</p></div>
<div class="m2"><p>آنکه کامش، سر به سر، ناکامی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه هر ساعت، نهان از خاص و عام</p></div>
<div class="m2"><p>کاسهٔ زهرت فرو ریزد به کام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر این زهر روزان و شبان</p></div>
<div class="m2"><p>چند خواهی بود لرزان و تپان؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منصب دنیاست، ای نیکونهاد!</p></div>
<div class="m2"><p>آنکه داده خرمن دینت به باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منصب دنیاست، ای صاحب فنون!</p></div>
<div class="m2"><p>آنکه کردت این چنین، خوار و زبون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خوش آن دانا که دنیا را بهشت</p></div>
<div class="m2"><p>رفت همچون شاه مردان در بهشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مولوی معنوی در مثنوی</p></div>
<div class="m2"><p>نکته‌ای گفته است، هان تا بشنوی:</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>« ترک دنیا گیر تا سلطان شوی</p></div>
<div class="m2"><p>ورنه گر چرخی تو، سرگردان شوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهر دارد در درون، دنیا چو مار</p></div>
<div class="m2"><p>گرچه دارد در برون، نقش و نگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهر این مار منقش، قاتل است</p></div>
<div class="m2"><p>می‌گریزد زو هر آن کس عاقل است»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زین سبب، فرمود شاه اولیا</p></div>
<div class="m2"><p>آن گزین اولیا و انبیا:</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حب الدنیا، رأس کل خطیة</p></div>
<div class="m2"><p>و ترک الدنیا رأس کل عبادة</p></div></div>