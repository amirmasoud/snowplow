---
title: >-
    بخش ۱۴ - فی ذم أصحاب التدریس مقصد هم مجرد أظهار الفضل و التلبیس
---
# بخش ۱۴ - فی ذم أصحاب التدریس مقصد هم مجرد أظهار الفضل و التلبیس

<div class="b" id="bn1"><div class="m1"><p>نان و حلوا چیست؟ این تدریس تو</p></div>
<div class="m2"><p>کان بود سرمایهٔ تلبیس تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر اظهار فضیلت، معرکه</p></div>
<div class="m2"><p>ساختی، افتادی اندر مهلکه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که عامی چند سازی دام خویش</p></div>
<div class="m2"><p>با صد افسون، آوری در دام خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند بگشایی سر انبان لاف؟</p></div>
<div class="m2"><p>چند پیمایی گزاف اندر گزاف؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی فروعت محکم آمد، نی اصول</p></div>
<div class="m2"><p>شرم بادت از خدا و از رسول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندرین ره چیست دانی غول تو؟</p></div>
<div class="m2"><p>این ریایی درس نامعقول تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درس اگر قربت نباشد زان غرض</p></div>
<div class="m2"><p>لیس درسا انه بئس المرض</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسب دولت، برفراز عرش تاخت</p></div>
<div class="m2"><p>آنکه خود را زین مرض آزاد ساخت</p></div></div>