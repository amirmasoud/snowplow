---
title: >-
    بخش ۱۰ - فی أن البلایا و المحن فی هذا الطریق، وان کانت عسیرة، لکنها علی المحب یسیرة بل  هی الراحة العظمی والنعمة الکبری
---
# بخش ۱۰ - فی أن البلایا و المحن فی هذا الطریق، وان کانت عسیرة، لکنها علی المحب یسیرة بل  هی الراحة العظمی والنعمة الکبری

<div class="b" id="bn1"><div class="m1"><p>ایها القلب الحزین المبتلا</p></div>
<div class="m2"><p>فی طریق العشق انواع البلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن القلب العشوق الممتحن</p></div>
<div class="m2"><p>لا یبالی بالبلایا و المحن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهل باشد در ره فقر و فنا</p></div>
<div class="m2"><p>گر رسد تن را تعب، جان را عنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنج راحت دان، چو شد مطلب بزرگ</p></div>
<div class="m2"><p>گرد گله، توتیای چشم گرگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی بود در راه عشق آسودگی؟</p></div>
<div class="m2"><p>سر به سر درد است و خون آلودگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نسازی بر خود آسایش حرام</p></div>
<div class="m2"><p>کی توانی زد به راه عشق، گام؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر ناکامی، دراین ره، کام نیست</p></div>
<div class="m2"><p>راه عشق است این، ره حمام نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترککان، چون اسب یغما پی کنند</p></div>
<div class="m2"><p>هرچه باشد، خود به غارت می‌برند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک ما، برعکس باشد کار او</p></div>
<div class="m2"><p>حیرتی دارم ز کار و بار او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کافرست و غارت دین می‌کند</p></div>
<div class="m2"><p>من نمی‌دانم چرا این می‌کند؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست جز تقوی، در این ره توشه‌ای</p></div>
<div class="m2"><p>نان و حلوا را بهل در گوشه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نان و حلوا چیست؟ جاه و مال تو</p></div>
<div class="m2"><p>باغ و راغ و حشمت و اقبال تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نان و حلوا چیست؟ این طول امل</p></div>
<div class="m2"><p>وین غرور نفس و علم بی‌عمل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نان و حلوا چیست؟ گوید با تو، فاش</p></div>
<div class="m2"><p>این همه سعی تو از بهر معاش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نان و حلوا چیست؟ فرزند و زنت</p></div>
<div class="m2"><p>اوفتاده همچو غل در گردنت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چند باشی بهر این حلوا و نان</p></div>
<div class="m2"><p>زیر منت، از فلان و از فلان؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برد این حلوا و نان، آرام تو</p></div>
<div class="m2"><p>شست از لوح تو کل نام تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هیچ بر گوشت نخورده است، ای لیم!</p></div>
<div class="m2"><p>حرف «الرزق علی الله الکریم»</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رو قناعت پیشه کن در کنج صبر</p></div>
<div class="m2"><p>پند بپذیر از سگ آن پیر گبر</p></div></div>