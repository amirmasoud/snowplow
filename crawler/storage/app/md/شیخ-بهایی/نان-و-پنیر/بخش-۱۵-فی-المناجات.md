---
title: >-
    بخش ۱۵ - فی‌المناجات
---
# بخش ۱۵ - فی‌المناجات

<div class="b" id="bn1"><div class="m1"><p>بار الها، ما ظلوم و هم جهول</p></div>
<div class="m2"><p>از تو می‌خواهیم، تسلیم عقول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانکه عقل هر که را کامل کنی</p></div>
<div class="m2"><p>خیر دارینی بدو واصل کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل، چون از علم کامل می‌شود</p></div>
<div class="m2"><p>وز تعلم، علم حاصل می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تعلم، هست دانا ناگزیر</p></div>
<div class="m2"><p>استفاضه باید از شیخ کبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس مرا، یارب، به دانایی رسان</p></div>
<div class="m2"><p>تا ز شر جمله باشم در امان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به دل فائز شود، از فیض پیر</p></div>
<div class="m2"><p>مر گرسنه، آنچه از نان و پنیر</p></div></div>