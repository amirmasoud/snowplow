---
title: >-
    بخش ۱۳ - فی ماهیة الذوات
---
# بخش ۱۳ - فی ماهیة الذوات

<div class="b" id="bn1"><div class="m1"><p>هر یک از موجود، با طوری وجود</p></div>
<div class="m2"><p>بهر او موجود شد، انسان نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود امر ممکنی از ممکنات</p></div>
<div class="m2"><p>در ازل ممتاز از غیرش به ذات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود اما بودنی علمی و بس</p></div>
<div class="m2"><p>حد علم ارچه نشد مفهوم کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مأخذ کل، قدرت بی‌منتهی است</p></div>
<div class="m2"><p>بی‌کم و بی‌کیف و أین و متی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داشت از حق، بهر حق را هم ظهور</p></div>
<div class="m2"><p>خواهی ار تمثیل وی، چون ظل و نور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظل، قدرت بود، کل، قبل الوجود</p></div>
<div class="m2"><p>هم ز حق، از بهر حق معلوم بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون معانیشان ز یکدیگر جداست</p></div>
<div class="m2"><p>گر تو ماهیاتشان خوانی، رواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زانکه ماهیت ز ماهو مشتق است</p></div>
<div class="m2"><p>زان به هر یک صدق، تشبیه حق است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه می‌گویم، همه تقریب دان</p></div>
<div class="m2"><p>نیست جز تقریب در وسع بیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این بیانات و شروح، ای حق شناس</p></div>
<div class="m2"><p>جمله تمثیل و مجاز است و قیاس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وه! چه نیکو گفت دانای حکیم</p></div>
<div class="m2"><p>از پی تمثیل قدوس و قدیم:</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای برون از فکر و قال و قیل من</p></div>
<div class="m2"><p>خاک بر فرق من و تمثیل من</p></div></div>