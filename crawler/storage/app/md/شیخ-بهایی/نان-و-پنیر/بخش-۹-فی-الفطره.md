---
title: >-
    بخش ۹ - فی‌الفطره
---
# بخش ۹ - فی‌الفطره

<div class="b" id="bn1"><div class="m1"><p>ای لوای اجتهاد افراشته</p></div>
<div class="m2"><p>روزهٔ هر روز، عادت ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل وحدت را به شقوت کرده حکم</p></div>
<div class="m2"><p>بسته‌شان در ربقهٔ صم و بکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هان، مشو مغرور بر افعال خود</p></div>
<div class="m2"><p>هان مشو مسرور بر احوال خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این عبادتهای تو مقبول نیست</p></div>
<div class="m2"><p>تا ندانی عاقبت، کار تو چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسا نعلی که وارون بسته شد</p></div>
<div class="m2"><p>شیشهٔ امن نفوس اشکسته شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گبر چندین ساله‌ای در حین نزع</p></div>
<div class="m2"><p>کرد بر حقیقت اسلام، قطع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عابدی با شد و مد و کش و فش</p></div>
<div class="m2"><p>بهر ترسا بچه‌ای شد، باده‌کش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار با انجام کار است و سرشت</p></div>
<div class="m2"><p>ختم کاشف، از سرشت خوب و زشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بسا بدطینت و نیکوخصال</p></div>
<div class="m2"><p>ای بسا خوش طینت و ناخوش فعال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طینت بد، آنکه در علم ازل</p></div>
<div class="m2"><p>رفته از وی ختم بر کفر و دغل</p></div></div>