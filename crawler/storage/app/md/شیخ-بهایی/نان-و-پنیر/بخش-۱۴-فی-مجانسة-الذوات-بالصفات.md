---
title: >-
    بخش ۱۴ - فی مجانسة الذوات بالصفات
---
# بخش ۱۴ - فی مجانسة الذوات بالصفات

<div class="b" id="bn1"><div class="m1"><p>داشت هر ذاتی، چو در علم ازل</p></div>
<div class="m2"><p>خواهش خود را به نوعی از عمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالسان حال کرد از حق سؤال</p></div>
<div class="m2"><p>تا میسر سازدش در لایزال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر میسر خیر شد، توفیق دان</p></div>
<div class="m2"><p>گر میسر شر بشد، خذلانش خوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی میسر این جز الحان سؤال</p></div>
<div class="m2"><p>گرچه بی‌مسول فعل آمد محال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لوم، پس عائد به اهل شر بود</p></div>
<div class="m2"><p>ذیل عدل حق، از آن اطهر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لم این مرموز اسرار خداست</p></div>
<div class="m2"><p>خوض دادن عقل را، در وی خطاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به علم و حکمت حق قائلی</p></div>
<div class="m2"><p>بر تو منحل می‌شود، بی‌مشکلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورنه اول رو تتبع کن علوم</p></div>
<div class="m2"><p>خاصه، تشریح و ریاضی و نجوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بین چه حکمتهاست در دور سپهر</p></div>
<div class="m2"><p>بین چه حکمتهاست در تنویر مهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بین چه حکمتهاست در خلق جهان</p></div>
<div class="m2"><p>بین چه حکمتهاست در تعلیم جان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بین چه حکمتهاست در خلق نبات</p></div>
<div class="m2"><p>بین چه حکمتهاست در این میوه‌جات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صافی این علمها خواهی اگر</p></div>
<div class="m2"><p>رو به «توحید مفضل» کن نظر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاندر آن از خان علم اله</p></div>
<div class="m2"><p>بشنوی با حق، بیان ای مرد راه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علم و دانش، جمله ارث انبیاست</p></div>
<div class="m2"><p>انبیا را علم، از نزد خداست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواندن صوری نشد صورت پذیر</p></div>
<div class="m2"><p>از معانی تنیست دانا را گزیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نفس چون گردد مهیای قبول</p></div>
<div class="m2"><p>علم از ایشان می‌کند در پی نزول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غایتش، گاهی میانجی حاصل است</p></div>
<div class="m2"><p>مثل عقلی کاو به ایشان واصل است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عقل از بند هوی چون وارهد</p></div>
<div class="m2"><p>روی وجهت سوی علیین کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>انبیا را چیست تعلیم عقول؟</p></div>
<div class="m2"><p>گوش کن گر نیستی ز اهل فضول</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کشف سر است آنچه بتوانند دید</p></div>
<div class="m2"><p>نقل ذکر است آنچه باشدشان شنید</p></div></div>