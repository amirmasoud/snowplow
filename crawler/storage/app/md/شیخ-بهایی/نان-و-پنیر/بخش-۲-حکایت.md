---
title: >-
    بخش ۲ - حکایت
---
# بخش ۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>عابدی از قوم اسرائیلیان</p></div>
<div class="m2"><p>در عبادت بود روزان و شبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی از لذات جسمی تافته</p></div>
<div class="m2"><p>لذت جان در عبادت یافته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطعه‌ای از ارض بود او را مکان</p></div>
<div class="m2"><p>کز سرای خلد می‌دادی نشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صیت عابد رفت تا چرخ کبود</p></div>
<div class="m2"><p>بس که بودی در رکوع و در سجود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدسیی از حال او شد باخبر</p></div>
<div class="m2"><p>کرد اندر لوح اجر او نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دید اجری بس حقیر و بس قلیل</p></div>
<div class="m2"><p>سر او را خواست از رب جلیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحی آمد کز برای امتحان</p></div>
<div class="m2"><p>وقتی از اوقات با وی بگذران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس ممثل گشت پیش او ملک</p></div>
<div class="m2"><p>تا کند ظاهر، عیارش بر محک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت عابد: کیستی، احوال چیست؟</p></div>
<div class="m2"><p>زانکه با ناجنس، نتوان کرد زیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت: مردی، از علایق رسته‌ای</p></div>
<div class="m2"><p>چون تو، دل بر قید طاعت بسته‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن حالت دیدم و حسن مکان</p></div>
<div class="m2"><p>آمدم تا با تو باشم، یک زمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت عابد: آری این منزل خوش است</p></div>
<div class="m2"><p>لیک با وی، عیب زشتی نیز هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عیب آن باشد که آن زیبا علف</p></div>
<div class="m2"><p>خودبخود، صد حیف می‌گردد تلف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از برای رب ما نبود حمار</p></div>
<div class="m2"><p>این علفها تا چرد فصل بهار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت قدسی: چونکه بشنید این مقال</p></div>
<div class="m2"><p>نیست ربت را خری، ای بی‌کمال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود مقصود ملک، از این کلام</p></div>
<div class="m2"><p>نفی خر اندر خصوص آن مقام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عابد این فهمید، یعنی نیست خر</p></div>
<div class="m2"><p>نه در اینجا و نه در جای دگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت: حاشا! این سخن دیوانگان</p></div>
<div class="m2"><p>این چنین بی‌ربط آمد بر زبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیش هر سبزه، خری می‌داشتی</p></div>
<div class="m2"><p>خوش بود تا در چرا بگماشتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر نبودی خر که اینها را چرید</p></div>
<div class="m2"><p>این علفها را چرا می‌آفرید؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت قدسی: هست خر، نی خلق را</p></div>
<div class="m2"><p>حق منزه از صفات خلق را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس ملک، هردم صد استغفار برد</p></div>
<div class="m2"><p>گرچه وی را ناقص و جاهل شمرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با وجود نفی اقرار وجود</p></div>
<div class="m2"><p>چون علفخوارش تصور کرده بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی‌تجارب، از کیا را علم نیست</p></div>
<div class="m2"><p>کز علف حیوان تواند کرد زیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هان، تأمل کن در این نقل شریف</p></div>
<div class="m2"><p>که در آن پنهان بود سر لطیف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عابد اول در میان خلق بود</p></div>
<div class="m2"><p>کسب آداب و عبادت می‌نمود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ورنه، چون داند عبادت چون کند؟</p></div>
<div class="m2"><p>بر چه ملت طاعت بی‌چون کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در اوان خلطه را خلق جهان</p></div>
<div class="m2"><p>دیده بود او، آنچه دیده دیگران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بعد از آن کرد او تجرد اختیار</p></div>
<div class="m2"><p>چون ندیده به ز طاعت، هیچ کار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود عقلش فاسد و ناقص ولی</p></div>
<div class="m2"><p>نه فساد ظاهر و نقص جلی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرد عابد، دیده بد خر را بسی</p></div>
<div class="m2"><p>هر یکی را لیک در دست کسی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت: اینها خود همه، از مردم است</p></div>
<div class="m2"><p>هر یک از سعی خود آورده به دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مالک ملک آمده هر کس به عقل</p></div>
<div class="m2"><p>در تمسک، دست ما را نیست دخل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون شد اینها جمله ملک دیگری</p></div>
<div class="m2"><p>پس نباشد، حضرت رب را خری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>او ندانسته که کل از حق بود</p></div>
<div class="m2"><p>جمله را حق مالک مطلق بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر که را ملکیست، از ابناء اوست</p></div>
<div class="m2"><p>هر که را مالیست، از اعطاء اوست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نزع و ایتایش به وفق حکمت است</p></div>
<div class="m2"><p>هر که را گه عزت و گه ذلت است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر کجا باشد وجود خر به کار</p></div>
<div class="m2"><p>می‌کند ایجاد، از یک تا هزار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هرچه خواهد می‌کند، پیدا بکن</p></div>
<div class="m2"><p>بی‌علاج و آلت حرف و سخن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عقل عابد را چو این عرفان نبود</p></div>
<div class="m2"><p>با ملک کرد آنچنان گفت و شنود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هان! مخند ای نفس بر عابد ز جهل</p></div>
<div class="m2"><p>هان، مدان رستن ز نقص عقل سهل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در کمین خود نشینی، گر دمی</p></div>
<div class="m2"><p>خویش را بینی کم از عابد همی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر تو این اموال دانی مال رب</p></div>
<div class="m2"><p>بهر چه در غصب داری، روز و شب؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر بود در عقد قلبت آنکه نیست</p></div>
<div class="m2"><p>مال، جز مال خدا، پس ظلم چیست؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آنچه داری مال حق دانی اگر</p></div>
<div class="m2"><p>پس به چشم عاریت، در وی نگر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زان به هر وجهی که خواهی نفع گیر</p></div>
<div class="m2"><p>داده بهر انتفاع، او را معیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لیک نه وجهی که مالک نهی کرد</p></div>
<div class="m2"><p>تا شوی از خجلت آن، روی زرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر نکردی این لوازم را ادا</p></div>
<div class="m2"><p>دعوی ملزوم کردن، دان خطا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عابد اندر عقل، گرچه بود سست</p></div>
<div class="m2"><p>بود اخلاص و عباداتش درست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کان ملک، تا آن زمان آمد پدید</p></div>
<div class="m2"><p>علت نقصان اجر وی بدید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا که آخر، در خلال گفتگو</p></div>
<div class="m2"><p>کرد استنباط ضعف عقل او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هست در عقل تو نیز این اختلال</p></div>
<div class="m2"><p>نفی خر کرد او ز حق، تو نفی مال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در تو آیا هست اخلاص و عمل؟</p></div>
<div class="m2"><p>پس چه خندی بر وی ای نفس دغل!</p></div></div>