---
title: >-
    بخش ۱۲ - فی التکلیف والشوق
---
# بخش ۱۲ - فی التکلیف والشوق

<div class="b" id="bn1"><div class="m1"><p>هان، مدان بیگار تکلیفان عام</p></div>
<div class="m2"><p>هان! مدان ضایع رسالات و پیام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید اول آید از حق نهی و امر</p></div>
<div class="m2"><p>غیر مختص، نه به زید ونه به عمرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز استماع آن دو تا بارز شده است</p></div>
<div class="m2"><p>شوق مکنونی که در نیک و بد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امر و نهی شرع و عقل و دین ز رب</p></div>
<div class="m2"><p>شرط شوق این و آن دان، نه سبب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرط اصلا محدث مشروط نیست</p></div>
<div class="m2"><p>گرچه از بهر حدوثش، بودنی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نباشد بارش نام از سما</p></div>
<div class="m2"><p>از زمین کی روید اقسام گیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل، به فیض عام، روید از زمین</p></div>
<div class="m2"><p>لیک این باشد چنان و آن چنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این یکی خارست آن یک گل به ذات</p></div>
<div class="m2"><p>هر یکی دارد ز ذات خود صفات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنبل و گل، بهر روییدن دمید</p></div>
<div class="m2"><p>خار و خس را بهر تون او آفرید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بارش اینها را چنین حالات داد</p></div>
<div class="m2"><p>پس به بارش، حال ذات از وی نزاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نکردی فهم، بگذر زین مقال</p></div>
<div class="m2"><p>خویش را ضایع مکن اندر جلال</p></div></div>