---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>در چهره ندارم از مسلمانی رنگ</p></div>
<div class="m2"><p>بر من دارد شرف، سگ اهل فرنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روسیهم که باشد از بودن من</p></div>
<div class="m2"><p>دوزخ را ننگ و اهل دوزخ را ننگ</p></div></div>