---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>تا منزل آدمی سرای دنیاست</p></div>
<div class="m2"><p>کارش همه جرم و کار حق، لطف و عطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش باش که آن سرا چنین خواهد بود</p></div>
<div class="m2"><p>سالی که نکوست، از بهارش پیداست</p></div></div>