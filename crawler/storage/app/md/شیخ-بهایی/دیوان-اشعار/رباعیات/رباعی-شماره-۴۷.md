---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>از سبحهٔ من، پیر مغان رفت ز هوش</p></div>
<div class="m2"><p>وز نالهٔ من، فتاد در شهر خروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شیخ که خرقه داد و زنار خرید</p></div>
<div class="m2"><p>تکبیر ز من گرفت، در میکده دوش</p></div></div>