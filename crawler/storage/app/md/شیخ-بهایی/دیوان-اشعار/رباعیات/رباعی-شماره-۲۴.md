---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>نقد دل خود بهائی آخر سره کرد</p></div>
<div class="m2"><p>در مجلس عشق، عقل را مسخره کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اوراق کتابهای علم رسمی</p></div>
<div class="m2"><p>از هم بدرید و کاغذ پنجره کرد</p></div></div>