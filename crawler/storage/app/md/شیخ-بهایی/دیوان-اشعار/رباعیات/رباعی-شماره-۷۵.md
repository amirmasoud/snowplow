---
title: >-
    رباعی شمارهٔ ۷۵
---
# رباعی شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>هرچند که در حسن و ملاحت، فردی</p></div>
<div class="m2"><p>از تو بنماند، در دل من دردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سویت نکنم نگاه، ای شمع اگر</p></div>
<div class="m2"><p>پروانهٔ من شوی و گردم گردی</p></div></div>