---
title: >-
    رباعی شمارهٔ ۵۱
---
# رباعی شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>از بس که زدم به شیشهٔ تقوی سنگ</p></div>
<div class="m2"><p>وز بس که به معصیت فرو بردم چنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل اسلام از مسلمانی من</p></div>
<div class="m2"><p>صد ننگ کشیدند ز کفار فرنگ</p></div></div>