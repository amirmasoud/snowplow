---
title: >-
    رباعی شمارهٔ ۱۷
---
# رباعی شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>در میکده دوش، زاهدی دیدم مست</p></div>
<div class="m2"><p>تسبیح به گردن و صراحی در دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم: ز چه در میکده جا کردی؟ گفت:</p></div>
<div class="m2"><p>از میکده هم به سوی حق راهی هست</p></div></div>