---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای عقل خجل ز جهل و نادانی ما</p></div>
<div class="m2"><p>درهم شده خلقی، ز پریشانی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت در بغل و به سجده پیشانی ما</p></div>
<div class="m2"><p>کافر زده خنده بر مسلمانی ما</p></div></div>