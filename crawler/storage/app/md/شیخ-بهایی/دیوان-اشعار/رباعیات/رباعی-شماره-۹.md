---
title: >-
    رباعی شمارهٔ ۹
---
# رباعی شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>مالی که ز تو کس نستاند، علم است</p></div>
<div class="m2"><p>حرزی که تو را به حق رساند، علم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز علم طلب مکن تو اندر عالم</p></div>
<div class="m2"><p>چیزی که تو را ز غم رهاند، علم است</p></div></div>