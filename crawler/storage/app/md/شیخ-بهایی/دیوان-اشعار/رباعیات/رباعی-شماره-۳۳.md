---
title: >-
    رباعی شمارهٔ ۳۳
---
# رباعی شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>تا نیست نگردی، ره هستت ندهند</p></div>
<div class="m2"><p>این مرتبه با همت پستت ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع قرار سوختن گر ندهی</p></div>
<div class="m2"><p>سر رشتهٔ روشنی به دستت ندهند</p></div></div>