---
title: >-
    رباعی شمارهٔ ۵۸
---
# رباعی شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>بی روی تو، خونابه فشاند چشمم</p></div>
<div class="m2"><p>کاری به جز از گریه، نداند چشمم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌ترسم از آنکه حسرت دیدارت</p></div>
<div class="m2"><p>در دیده بماند و نماند چشمم</p></div></div>