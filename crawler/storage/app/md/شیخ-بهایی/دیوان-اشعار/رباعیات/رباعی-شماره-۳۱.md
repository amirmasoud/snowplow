---
title: >-
    رباعی شمارهٔ ۳۱
---
# رباعی شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>دیدی که بهائی چو غم از سر وا کرد</p></div>
<div class="m2"><p>از مدرسه رفت و دیر را مائوا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجموع کتابهای علم درسی</p></div>
<div class="m2"><p>از هم بدرید و کاغذ حلوا کرد</p></div></div>