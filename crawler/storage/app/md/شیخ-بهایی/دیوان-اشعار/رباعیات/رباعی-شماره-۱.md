---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای صاحب مسله! تو بشنو از ما</p></div>
<div class="m2"><p>تحقیق بدان که لامکان است خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که تو را کشف شود این معنی</p></div>
<div class="m2"><p>جان در تن تو، بگو کجا دارد جا</p></div></div>