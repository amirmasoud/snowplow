---
title: >-
    رباعی شمارهٔ ۱۰
---
# رباعی شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>دنیا که دلت ز حسرت او زار است</p></div>
<div class="m2"><p>سرتاسر او تمام، محنت‌زار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالله که دولتش نیرزد به جوی</p></div>
<div class="m2"><p>تالله که نام بردنش هم عار است</p></div></div>