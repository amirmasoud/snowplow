---
title: >-
    رباعی شمارهٔ ۷۹
---
# رباعی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ای دل، قدمی به راه حق ننهادی</p></div>
<div class="m2"><p>شرمت بادا که سخت دور افتادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بار عروس توبه را بستی عقد</p></div>
<div class="m2"><p>نایافته کام از او، طلاقش دادی</p></div></div>