---
title: >-
    رباعی شمارهٔ ۳۶
---
# رباعی شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>دل درد و بلای عشقش افزون خواهد</p></div>
<div class="m2"><p>او دیدهٔ دل همیشه در خون خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین طرفه که این ز آن «بحل» می‌طلبد</p></div>
<div class="m2"><p>و آن در پی آنکه عذر او چون خواهد</p></div></div>