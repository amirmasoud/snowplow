---
title: >-
    رباعی شمارهٔ ۶۳
---
# رباعی شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>خو کرده به خلوت، دل غم فرسایم</p></div>
<div class="m2"><p>کوتاه شد از صحبت مردم، پایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تنهایم، هم نفسم یاد کسی است</p></div>
<div class="m2"><p>چون هم نفسم کسی شود، تنهایم</p></div></div>