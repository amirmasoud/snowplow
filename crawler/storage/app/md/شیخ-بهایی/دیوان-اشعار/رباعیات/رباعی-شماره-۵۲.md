---
title: >-
    رباعی شمارهٔ ۵۲
---
# رباعی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>یک چند، میان خلق کردیم درنگ</p></div>
<div class="m2"><p>ز ایشان به وفا، نه بوی دیدیم نه رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن به که ز چشم خلق پنهان گردیم</p></div>
<div class="m2"><p>چون آب در آبگینه، آتش در سنگ</p></div></div>