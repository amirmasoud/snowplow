---
title: >-
    مستزاد
---
# مستزاد

<div class="b" id="bn1"><div class="m1"><p>هرگز نرسیده‌ام من سوخته جان،</p></div>
<div class="m2"><p>روزی به امید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز بخت سیه ندیده‌ام، هیچ زمان،</p></div>
<div class="m2"><p>یک روز سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاصد چو نوید وصل با من می‌گفت،</p></div>
<div class="m2"><p>آهسته بگفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حیرتم از بخت بد خود که چه سان؟</p></div>
<div class="m2"><p>این حرف شنید</p></div></div>