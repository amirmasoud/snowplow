---
title: >-
    غزل شمارهٔ ۱۵
---
# غزل شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>تا سرو قباپوش تو را دیده‌ام امروز</p></div>
<div class="m2"><p>در پیرهن از ذوق نگنجیده‌ام امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دانم و دل، غیر چه داند که در این بزم</p></div>
<div class="m2"><p>از طرز نگاه تو چه فهمیده‌ام امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا باد صبا پیچ سر زلف تو وا کرد</p></div>
<div class="m2"><p>بر خود، چو سر زلف تو پیچیده‌ام امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هشیاریم افتاد به فردای قیامت</p></div>
<div class="m2"><p>زان باده که از دست تو نوشیده‌ام امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد خنده زند بر حلل قیصر و دارا</p></div>
<div class="m2"><p>این ژندهٔ پر بخیه که پوشیده‌ام امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسوس که برهم زده خواهد شد از آن روی</p></div>
<div class="m2"><p>شیخانه بساطی که فرو چیده‌ام امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر باد دهد توبهٔ صد همچو بهائی</p></div>
<div class="m2"><p>آن طرهٔ طرار که من دیده‌ام امروز</p></div></div>