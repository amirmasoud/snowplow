---
title: >-
    غزل شمارهٔ ۱۶
---
# غزل شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>روی تو گل تازه و خط سبزهٔ نوخیز</p></div>
<div class="m2"><p>نشکفته گلی همچو تو در گلشن تبریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد هوش دلم غارت آن غمزهٔ خونریز</p></div>
<div class="m2"><p>این بود مرا فایده از دیدن تبریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل! تو در این ورطه مزن لاف صبوری</p></div>
<div class="m2"><p>وای عقل! تو هم بر سر این واقعه مگریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرخنده شبی بود که آن خسرو خوبان</p></div>
<div class="m2"><p>افسوس کنان، لب به تبسم، شکر آمیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از راه وفا، بر سر بالین من آمد</p></div>
<div class="m2"><p>وز روی کرم گفت که: ای دلشده، برخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دیدهٔ خونبار، نثار قدم او</p></div>
<div class="m2"><p>کردم گهر اشک، من مفلس بی‌چیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون رفت دل گمشده‌ام گفت: بهائی!</p></div>
<div class="m2"><p>خوش باش که من رفتم و جان گفت که : من نیز</p></div></div>