---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>به عالم هر دلی کاو هوشمند است</p></div>
<div class="m2"><p>به زنجیر جنون عشق، بند است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جای سدر و کافورم پس از مرگ</p></div>
<div class="m2"><p>غبار خاک کوی او، پسند است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کف دارند خلقی نقد جانها</p></div>
<div class="m2"><p>سرت گردم، مگر بوسی به چند است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث علم رسمی، در خرابات</p></div>
<div class="m2"><p>برای دفع چشم بد، سپند است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از مردن، غباری زان سر کوی</p></div>
<div class="m2"><p>به جای سدر و کافورم، پسند است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طمع در میوهٔ وصلش، بهائی</p></div>
<div class="m2"><p>مکن، کان میوه بر شاخ بلند است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهائی گرچه می‌آید ز کعبه</p></div>
<div class="m2"><p>همان دردی کش زناربند است</p></div></div>