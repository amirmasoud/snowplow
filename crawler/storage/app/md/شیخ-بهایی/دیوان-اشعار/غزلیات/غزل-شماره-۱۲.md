---
title: >-
    غزل شمارهٔ ۱۲
---
# غزل شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>آتش به جانم افکند، شوق لقای دلدار</p></div>
<div class="m2"><p>از دست رفت صبرم، ای ناقه! پای بردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساربان، ! خدا را؛ پیوسته متصل ساز</p></div>
<div class="m2"><p>ایوار را به شبگیر، شبگیر را به ایوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کیش عشقبازان، راحت روا نباشد</p></div>
<div class="m2"><p>ای دیده! اشک می‌ریز، ای سینه! باش افگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سنگ و خار این راه، سنجاب دان و قاقم</p></div>
<div class="m2"><p>راه زیارت است این، نه راه گشت بازار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با زائران محرم، شرط است آنکه باشد</p></div>
<div class="m2"><p>غسل زیارت ما، از اشک چشم خونبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما عاشقان مستیم، سر را ز پا ندانیم</p></div>
<div class="m2"><p>این نکته‌ها بگیرید، بر مردمان هشیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در راه عشق اگر سر، بر جای پا نهادیم</p></div>
<div class="m2"><p>بر ما مگیر نکته، ما را ز دست مگذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در فال ما نیاید جز عاشقی و مستی</p></div>
<div class="m2"><p>در کار ما بهائی کرد استخاره صد بار</p></div></div>