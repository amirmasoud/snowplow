---
title: >-
    غزل شمارهٔ ۱۴
---
# غزل شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>الهی الهی، به حق پیمبر</p></div>
<div class="m2"><p>الهی الهی، به ساقی کوثر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الهی الهی، به صدق خدیجه</p></div>
<div class="m2"><p>الهی الهی، به زهرای اطهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الهی الهی، به سبطین احمد</p></div>
<div class="m2"><p>الهی، به شبیر الهی! به شبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الهی به عابد! الهی به باقر</p></div>
<div class="m2"><p>الهی به موسی، الهی به جعفر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الهی الهی، به شاه خراسان</p></div>
<div class="m2"><p>خراسان چه باشد! به آن شاه کشور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شنیدم که می‌گفت زاری، غریبی</p></div>
<div class="m2"><p>طواف رضا، چون شد او را میسر:</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من اینجا غریب و تو شاه غریبان</p></div>
<div class="m2"><p>به حال غریب خود، از لطف بنگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الهی به حق تقی و به علمش</p></div>
<div class="m2"><p>الهی به حق نقی و به عسکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الهی الهی، به مهدی هادی</p></div>
<div class="m2"><p>که او مؤمنان راست هادی و رهبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که بر حال زار بهائی نظر کن!</p></div>
<div class="m2"><p>به حق امامان معصوم، یکسر</p></div></div>