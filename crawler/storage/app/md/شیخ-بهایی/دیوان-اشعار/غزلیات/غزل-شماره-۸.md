---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>یک گل ز باغ دوست، کسی بو نمی‌کند</p></div>
<div class="m2"><p>تا هرچه غیر اوست، به یک سو نمی‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن نمی‌شود ز رمد، چشم سالکی</p></div>
<div class="m2"><p>تا از غبار میکده، دارو نمی‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم: ز شیخ صومعه، کارم شود درست</p></div>
<div class="m2"><p>گفتند: او به دردکشان خو نمی‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم: روم به میکده، گفتند: پیر ما</p></div>
<div class="m2"><p>خوش می‌کشد پیاله و خوش بو نمی‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتم به سوی مدرسه، پیری به طنز گفت:</p></div>
<div class="m2"><p>تب را کسی علاج، به طنزو نمی‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که پیر عشق، به ماهی کند تمام</p></div>
<div class="m2"><p>در صد هزار سال، ارسطو نمی‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد اکتفا به دنیی دون خواجه، کاین عروس</p></div>
<div class="m2"><p>هیچ اکتفا، به شوهری او نمی‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن کو نوید آیهٔ «لا تقنطوا» شنید</p></div>
<div class="m2"><p>گوشی به حرف واعظ پرگو نمی‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زرق و ریاست زهد بهائی، وگرنه او</p></div>
<div class="m2"><p>کاری کند که کافر هندو نمی‌کند</p></div></div>