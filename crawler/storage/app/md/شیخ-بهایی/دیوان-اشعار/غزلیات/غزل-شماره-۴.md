---
title: >-
    غزل شمارهٔ ۴
---
# غزل شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>بگذر ز علم رسمی، که تمام قیل و قال است</p></div>
<div class="m2"><p>من و درس عشق ای دل! که تمام وجد و حال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مراحم الهی، نتوان برید امید</p></div>
<div class="m2"><p>مشنو حدیث زاهد، که شنیدنش وبال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طمع وصال گفتی که به کیش ما حرام است</p></div>
<div class="m2"><p>تو بگو که خون عاشق، به کدام دین حلال است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جواب دردمندان، بگشا لب ای شکرخا!</p></div>
<div class="m2"><p>به کرشمه کن حواله، که جواب صد سوال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم هجر را بهائی، به تو ای بت ستمگر</p></div>
<div class="m2"><p>به زبان حال گوید که زبان قال لال است</p></div></div>