---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>به بازار محشر، من و شرمساری</p></div>
<div class="m2"><p>که بسیار، بسیار کاسد قماشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهائی، بهائی، یکی موی جانان</p></div>
<div class="m2"><p>دو کون ارستانم، بهائی نباشم</p></div></div>