---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>مستان که گام در حرم کبریا نهند</p></div>
<div class="m2"><p>یک جام وصل را دو جهان در بها دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگی که سجده‌گاه نماز ریای ماست</p></div>
<div class="m2"><p>ترسم که در ترازوی اعمال ما نهند</p></div></div>