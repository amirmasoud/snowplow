---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>راه مقصد دور و پای سعی لنگ</p></div>
<div class="m2"><p>وقت همچون خاطر ناشاد تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جذبه‌ای از عشق باید، بی‌گمان</p></div>
<div class="m2"><p>تا شود طی هم زمان و هم مکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز از دود دلم تاریک و تار</p></div>
<div class="m2"><p>شب چه روز آمد ز آه شعله بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم از هندوی زلفش واژگون</p></div>
<div class="m2"><p>روز من شب شد، شبم روز از جنون</p></div></div>