---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>از سمور و حریر بیزارم</p></div>
<div class="m2"><p>باز میل قلندری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تکیه بر بستر منقش، بس</p></div>
<div class="m2"><p>بر تنم، نقش بوریاست هوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند باشم مورع‌الخاطر</p></div>
<div class="m2"><p>ز استر و اسب و مهتر و قاطر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی از دست ساربان نالم</p></div>
<div class="m2"><p>که بود نام او گم از عالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند گویم ز خیمه و الجوق</p></div>
<div class="m2"><p>چند بینم کجاوه و صندوق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نباشد اطاق و فرش حریر</p></div>
<div class="m2"><p>کنج مسجد خوش است، کهنه حصیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مزعفر مرا رود از یاد</p></div>
<div class="m2"><p>سر نان جوین سلامت باد</p></div></div>