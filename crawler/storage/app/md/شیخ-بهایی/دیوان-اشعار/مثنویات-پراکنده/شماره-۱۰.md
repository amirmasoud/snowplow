---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>عادت ما نیست، رنجیدن ز کس</p></div>
<div class="m2"><p>ور بیازارد، نگوییمش به کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور برآرد دود از بنیاد ما</p></div>
<div class="m2"><p>آه آتش بار ناید یاد ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورنه ما شوریدگان در یک سجود</p></div>
<div class="m2"><p>بیخ ظالم را براندازیم، زود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخصت اریابد ز ما باد سحر</p></div>
<div class="m2"><p>عالمی در دم کند زیر و زبر</p></div></div>