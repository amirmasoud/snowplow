---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گر نبود خنگ مطلی لگام</p></div>
<div class="m2"><p>زد بتوان بر قدم خویش گام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نبود مشربه از زر ناب</p></div>
<div class="m2"><p>با دو کف دست، توان خورد آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور نبود بر سر خوان، آن و این</p></div>
<div class="m2"><p>هم بتوان ساخت به نان جوین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور نبود جامهٔ اطلس تو را</p></div>
<div class="m2"><p>دلق کهن، ساتر تن بس تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شانهٔ عاج ار نبود بهر ریش</p></div>
<div class="m2"><p>شانه توان کرد به انگشت خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله که بینی، همه دارد عوض</p></div>
<div class="m2"><p>در عوضش، گشته میسر غرض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه ندارد عوض، ای هوشیار</p></div>
<div class="m2"><p>عمر عزیزیست، غنیمت شمار</p></div></div>