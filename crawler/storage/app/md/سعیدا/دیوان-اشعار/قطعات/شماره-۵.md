---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ز روح پاک تو امدادها طلب دارم</p></div>
<div class="m2"><p>نه زاد راحله دارم نه قوت این راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر ز حشمت و جاهت امید آن دارم</p></div>
<div class="m2"><p>که زیر چتر تو باشد مرا به حشر پناه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اختیار از این آستان نمی رفتم</p></div>
<div class="m2"><p>که فرض بود مرا دیدن رسول الله</p></div></div>