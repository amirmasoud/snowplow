---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>به اهل راز چه نسبت رقیب بدگو را</p></div>
<div class="m2"><p>لب پیاله کجا و زبان شانه کجا</p></div></div>