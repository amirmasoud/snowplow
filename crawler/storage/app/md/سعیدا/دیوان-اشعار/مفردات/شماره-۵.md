---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ناز و کرشمه زینت و زیب ستمگر است</p></div>
<div class="m2"><p>ورنه ز دلبری است که دلاور است</p></div></div>