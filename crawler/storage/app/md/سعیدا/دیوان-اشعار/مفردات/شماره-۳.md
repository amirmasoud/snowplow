---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>متاع پاک ندارد به گفتگو حاجت</p></div>
<div class="m2"><p>گرفته است سعیدا از آن زبان صدف</p></div></div>