---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>تا با تو از آن گوشهٔ ابرو نظری هست</p></div>
<div class="m2"><p>سوز جگر و چشم تر و دردسری هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سر ننهی بی سر و سامان ننشینی</p></div>
<div class="m2"><p>چون کشتی طوفان زده هر دم خطری هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا هست در این جسم، ز جان صورت خالی</p></div>
<div class="m2"><p>بر ناصیهٔ من ز مؤثر اثری هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این هستی موهومهٔ ما شاهد بعد است</p></div>
<div class="m2"><p>پروانه کباب است که تا بال و پری هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ناوک آن سخت کمان روی نتابم</p></div>
<div class="m2"><p>چون آینه از سینه سعیدا سپری هست</p></div></div>