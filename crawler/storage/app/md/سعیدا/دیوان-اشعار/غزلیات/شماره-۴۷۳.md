---
title: >-
    شمارهٔ ۴۷۳
---
# شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>اگرچه در نظر خلق اعتبار ندارم</p></div>
<div class="m2"><p>ولی چو آینه در دل ز کس غبار ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که شهره به سرگشتگی شدم چه کنم</p></div>
<div class="m2"><p>چو جام باده به گردیدن اختیار ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه یک گل از این بوستان نچید دلم</p></div>
<div class="m2"><p>هزار شکر که در سینه خارخار ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر سایهٔ زلفین آفتاب پناهش</p></div>
<div class="m2"><p>به روز حادثه [جایی] در این دیار ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چشمم خلق نپیچم به صد هنر یک عیب</p></div>
<div class="m2"><p>گدای عشقم و از دلق پاره عار ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذشتم از سه و پنج و دویی تو ای زاهد</p></div>
<div class="m2"><p>برو برو که سر و برگ این قمار ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرش نخوانم و از دوش خود بیندازم</p></div>
<div class="m2"><p>به پای دار، سری را که پایدار ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مباد سایهٔ باد خزان کم از چمنم</p></div>
<div class="m2"><p>که تاب منت سرسبزی بهار ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کوی عشق مرا پایدار کی خوانند</p></div>
<div class="m2"><p>هزار بار اگر سر به پای دار ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به روز واقعه در زیر تاک دفن کنید</p></div>
<div class="m2"><p>مرا که طاقت یک لحظهٔ خمار ندارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیال دوست مرا بس که از نزاکت طبع</p></div>
<div class="m2"><p>دماغ بوسه سعیدا سر کنار ندارم</p></div></div>