---
title: >-
    شمارهٔ ۵۰۸
---
# شمارهٔ ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>ناخورده شراب ناب مستیم</p></div>
<div class="m2"><p>نادیده خدا خداپرستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید با ما سجود دارد</p></div>
<div class="m2"><p>هر چند که ما ز خاک پستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کفر سواد اعظم عشق</p></div>
<div class="m2"><p>زنار ز دست فقر بستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدیم جز او فنای مطلق</p></div>
<div class="m2"><p>چه تحت و چه فوق هر چه هستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بحر خیال موج زن شد</p></div>
<div class="m2"><p>در زورق فکر خود نشستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرتا سر کاینات گشتیم</p></div>
<div class="m2"><p>از قید قیود عقل جستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدیم طلسم بود و نابود</p></div>
<div class="m2"><p>با سنگ محبتش شکستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز کفر، رهی به دین ندیدیم</p></div>
<div class="m2"><p>هر چند که مؤمن الستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی نفی نشد ثبوت، واجب</p></div>
<div class="m2"><p>از کفر به زور کفر، رستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در قبضهٔ قدرتیم چون ما</p></div>
<div class="m2"><p>بیهوده به فکر قبض و بسطیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بستیم نظر ز بد سعیدا</p></div>
<div class="m2"><p>در خانهٔ عافیت نشستیم</p></div></div>