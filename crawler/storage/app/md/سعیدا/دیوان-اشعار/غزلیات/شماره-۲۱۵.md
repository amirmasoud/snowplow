---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>هر که را راه سخن وا شده موسی گردد</p></div>
<div class="m2"><p>هر که پاس دم خود داشت مسیحا گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که او پاک درون تر گرهش مشکل تر</p></div>
<div class="m2"><p>که گهر وانشود بحر اگر واگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اطلس کار جهان را نه چنان بافته اند</p></div>
<div class="m2"><p>که سررشته به تدبیر تو پیدا گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دلت یک سر مویی خبر از حق یابد</p></div>
<div class="m2"><p>هر سر موی، تو را دیدهٔ بینا گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن عشق مگویید به هر بی دردی</p></div>
<div class="m2"><p>راز ما را مگذارید که افشا گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که دل بست نه آیین محبت داند</p></div>
<div class="m2"><p>هر سعیدی که نتواند که سعیدا گردد</p></div></div>