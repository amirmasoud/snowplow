---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>یار من درد و کبابم دل و می خون جگر</p></div>
<div class="m2"><p>نیست جز زیر زمینم هوس جای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر خستهٔ من بین و گشا لب به سخن</p></div>
<div class="m2"><p>که باین ضعف دوا نیست بجز گل به شکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهر همت ذاتیش نمایان گردد</p></div>
<div class="m2"><p>پیش یاران پسری را که برد نام پدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل را نور نماند چو شود موی سفید</p></div>
<div class="m2"><p>شمع تاریک بماند چو شود وقت سحر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می رسد گر قدح بادهٔ معنی نوشیم</p></div>
<div class="m2"><p>ما که دوران دگر گشت نگشتیم دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرف آن لعل لب و تنگ دهان باز بگو</p></div>
<div class="m2"><p>سخن از بوسه و جام است مکرر خوشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسنی بست سعیدا به میان تا که شنید</p></div>
<div class="m2"><p>زیر بارند مرصع کمران تا به کمر</p></div></div>