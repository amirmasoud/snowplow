---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>نشئهٔ آب حیات از لعل شکرکام اوست</p></div>
<div class="m2"><p>هر دو عالم را فصاحت بستهٔ دشنام اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب از پرتو عکسش نشانی می دهد</p></div>
<div class="m2"><p>ساغر سرشار معنی، جرعه سنج جام اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویش را همرنگ زلفش گفت و عنبر شد خجل</p></div>
<div class="m2"><p>روسیاهی های او آخر ز فکر خام اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغبان گل را به یاد عارضش جا داده است</p></div>
<div class="m2"><p>در چمن مقصود از سرو سهی اندام اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان جام از آن روزن جدا گردیده است</p></div>
<div class="m2"><p>آفتاب افتاده خشتی از کنار بام اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد سرگردان و بحر آشفته و عالم خراب</p></div>
<div class="m2"><p>ای سرش گردم چه حال است این که در ایام اوست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخ می گوید به گوش نرگس بیچاره گل</p></div>
<div class="m2"><p>تا در این گلشن نوای شوخی بادام اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لایق حق غیر حق از کس نمی آید بجا</p></div>
<div class="m2"><p>چشم خاص و عام لیکن بر سر انعام اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست عالم ها سعیدا در خم زلفش نهان</p></div>
<div class="m2"><p>صبح امید سعادت در کنار شام اوست</p></div></div>