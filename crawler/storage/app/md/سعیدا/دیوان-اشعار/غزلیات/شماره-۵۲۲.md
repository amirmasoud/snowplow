---
title: >-
    شمارهٔ ۵۲۲
---
# شمارهٔ ۵۲۲

<div class="b" id="bn1"><div class="m1"><p>آخرت منظور اگر باشد ز دنیا آمدن</p></div>
<div class="m2"><p>کعبه رفتن نیست کمتر از کلیسا آمدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهره را افروختن در تلخکامی ناقصی است</p></div>
<div class="m2"><p>باده را خامی بود از خم به بالا آمدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطلب از ارباب دنیا می شود حاصل تو را</p></div>
<div class="m2"><p>سبزه گر آسان بود بیرون ز خارا آمدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شورش مجنون دو بالا می شود بیرون ز شهر</p></div>
<div class="m2"><p>یمن ها دیوانه را دارد به صحرا آمدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست مکتب گر جهان این بازی طفلانه چیست</p></div>
<div class="m2"><p>رفتن امروز از دنیا و فردا آمدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر به پا افکنده باید یا ز پا افتاده ای</p></div>
<div class="m2"><p>نیست آسان ای سعیدا از پی ما آمدن</p></div></div>