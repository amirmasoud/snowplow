---
title: >-
    شمارهٔ ۴۴۷
---
# شمارهٔ ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>دارد بت نازک دلم صد جان به قربان در بغل</p></div>
<div class="m2"><p>هر تار کفر زلف او پیچیده ایمان در بغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا آسمان ها می رسد فیض از قد و بالای او</p></div>
<div class="m2"><p>دارد چمن از سایه اش سرو خرامان در بغل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح بناگوشش کند صد کار روز حشر را</p></div>
<div class="m2"><p>تا خفته آن زلف دوتا خورشید تابان در بغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبحی دمی بر خستگان بگذشت آن عیسی نفس</p></div>
<div class="m2"><p>صد زخم را مرهم به کف صد درد درمان در بغل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بندگی چون زلف او صد حلقه در گوش افکنم</p></div>
<div class="m2"><p>از مهر اگر بنشیندم آن ماه تابان در بغل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی خون دل ز این بوستان یک گل نمی آید به کف</p></div>
<div class="m2"><p>هر بلبلی دارد نهان صد غنچه پیکان در بغل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چشم او دارم حذر زان رو که دارد بی گمان</p></div>
<div class="m2"><p>خنجر درون آستین شمشیر عریان در بغل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذشت چون باد صبا دامن کشان با خون دل</p></div>
<div class="m2"><p>با آن که چشمش بارها خون کرده پنهان در بغل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصد عزیمت کرده جان از دست فریاد دلم</p></div>
<div class="m2"><p>همسایه بی آرام شد ز این طفل گریان در بغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دست چشم مست او کی دل توانم برکنم</p></div>
<div class="m2"><p>دارد به قصد جان من صد دشنه مژگان در بغل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دریا به این تردامنی دل شسته از روی زمین</p></div>
<div class="m2"><p>چیزی ندارد غیر کف چون دست عریان در بغل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از داغ های عشق او امشب سعیدا در تبم</p></div>
<div class="m2"><p>جان گشته گویا بلبلی دارد گلستان در بغل</p></div></div>