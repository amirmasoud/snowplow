---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>چو بو دهد صبحم به دست باد صبا</p></div>
<div class="m2"><p>به گردن نفس افتاده می روم از جا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قسم به قبضهٔ قدرت که بر سر مردان</p></div>
<div class="m2"><p>شکوه سایهٔ شمشیر به ز بال هما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خاک تربت من بوی عشق می آید</p></div>
<div class="m2"><p>برند خاک مزار مرا عبیرآسا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا به کنج خرابات و فارغ از همه شو</p></div>
<div class="m2"><p>خوشا عبادت مخفی که نیست بوی ریا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هدایت عجبی کرده عارف سالک</p></div>
<div class="m2"><p>طریق فقر که منزل بقا و راه فنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو به یوسف دوران که از جفای رقیب</p></div>
<div class="m2"><p>گدای کوی شما شد گدای راه شما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا قسم به وفای تو ای ستیزه مزاج</p></div>
<div class="m2"><p>که بی جفای تو هرگز ندیده ایم صفا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا ز آتش عشقش جلای خاطر بس</p></div>
<div class="m2"><p>بسان شمع نسوزم برای نشو و نما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث دوست بود زاهدا دلیل فراق</p></div>
<div class="m2"><p>سخن مگو ز خدا پیش من برای خدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی زنم مژه بر هم گشاده می دارم</p></div>
<div class="m2"><p>برای دیدن او منظر تماشا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آنچه مبدأ فیاض گفت می گوید</p></div>
<div class="m2"><p>در این غزل نبود مدخلی سعیدا را</p></div></div>