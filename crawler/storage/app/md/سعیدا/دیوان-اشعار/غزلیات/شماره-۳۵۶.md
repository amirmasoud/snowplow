---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>نسخهٔ عکس رخ او، مه تابان حاضر</p></div>
<div class="m2"><p>شاهد حرف لبش لعل بدخشان حاضر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که سر از خط فرمان تو بیرون سازد</p></div>
<div class="m2"><p>تیغ ابرو، رسن زلف پریشان حاضر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به کفر خم زلفش سر سودا دارد</p></div>
<div class="m2"><p>گر قبولش نشود جان و دل ایمان حاضر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سواد خط یاقوت نداری ای دل</p></div>
<div class="m2"><p>مصحفی بر ورق گل خط ریحان حاضر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منکر یوسف ما کیست بگو ای دل زار</p></div>
<div class="m2"><p>قدمی پیش بنه چاه زنخدان حاضر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منکر چاک دل صبح نگردد خورشید</p></div>
<div class="m2"><p>تیغ آلوده به خون زخم نمایان حاضر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سر رفتن از خویش سعیدا داری</p></div>
<div class="m2"><p>شاهراه نظر و چاک گریبان حاضر</p></div></div>