---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>گرچه عالم را بنا آن ذات بیچون ساخته</p></div>
<div class="m2"><p>لیک فکری کن که عالم را بنا چون ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو عالم را معانی در تو صانع کرده فکر</p></div>
<div class="m2"><p>تا در این دیوان تو را مصراع موزون ساخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر لیلی صرف شد تا گشت مجنون ابلهی</p></div>
<div class="m2"><p>عالمی را نازم آن لیلی که مجنون ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک را آرام کی می بود آدم گر نبود</p></div>
<div class="m2"><p>این همه صنعت که حق در کار گردون ساخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ممکن عقل را بیت الحزن بی چار حد</p></div>
<div class="m2"><p>زان بنای عشق را از عقل، بیرون ساخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد عزیز مصر لیکن در فراق خویشتن</p></div>
<div class="m2"><p>یوسف ما خاطر یعقوب محزون ساخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیوه ها کردم بسی با زلف او رامم نشد</p></div>
<div class="m2"><p>خلقت این مار را گویا ز افسون ساخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کام عیش ما نشد خالی ز تلخی هیچ گه</p></div>
<div class="m2"><p>گوییا در بادهٔ ما چرخ، افیون ساخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار آسان نیست جور و ناز معشوق به کس</p></div>
<div class="m2"><p>چشم او خود خورده خون ها تا دلی خون ساخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه هست از دفتر حق نیست بیرون در جهان</p></div>
<div class="m2"><p>او به علم خویشتن نی کم نه افزون ساخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چوب را در رقص می آرد سعیدا جذب عشق</p></div>
<div class="m2"><p>جلوهٔ لیلی وش ما بید مجنون ساخته</p></div></div>