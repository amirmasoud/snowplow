---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>تا کرد آن پریرخ و نامهربان خروج</p></div>
<div class="m2"><p>جان رخت بست و کرد ز تنها روان خروج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیری فکنده طرفه فتوری حواس را</p></div>
<div class="m2"><p>گویا که کرده یوسف از این کاروان خروج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایمان به فکر زلف بتی تازه می کنند</p></div>
<div class="m2"><p>شاید که کرده مهدی آخر زمان خروج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خم نشست دختر رز با هزار ناز</p></div>
<div class="m2"><p>از شیشه کرد آفت پیر و جوان خروج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارم به دل ملاحظه دایم که بی محل</p></div>
<div class="m2"><p>تا نکته ای مباد کند از زبان خروج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای نازنین چرانشوی مهدی زمان</p></div>
<div class="m2"><p>هرگز نکرده مثل تویی در جهان خروج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا قامتش بدیدم و رخسار ماه او</p></div>
<div class="m2"><p>در حیرتم که کرده ز سرو ارغوان خروج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای لعل پاره گوهر یکدانه ای چو تو</p></div>
<div class="m2"><p>تا این زمان نکرده ز بحر و ز کان خروج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان خاکسار گشته سعیدا که کرده است</p></div>
<div class="m2"><p>آدم به این کمال از این خاکدان خروج</p></div></div>