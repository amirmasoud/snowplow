---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>مقصد از گردیدن شیب و فرازم سیر نیست</p></div>
<div class="m2"><p>کافرام اما مراد و مقصد من دیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتن خود را روا دارم که در کوی حبیب</p></div>
<div class="m2"><p>غیر من دیگر در این دارالصفا کس غیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ضد هر شی باعث عرفان آن شی می شود</p></div>
<div class="m2"><p>هر که را از شر وقوفی نیست او را خیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می رساند در نگاهی خویش را بر جان و دل</p></div>
<div class="m2"><p>تیزپرتر از خدنگ ماهرویان طیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست رحمی گفتمش با او به مژگان سیاه</p></div>
<div class="m2"><p>با زبان دشنه گفتا با سعیدا خیر نیست</p></div></div>