---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>کام تا هست تو را کامروا نتوان کرد</p></div>
<div class="m2"><p>تا تو هستی به میان، هیچ صفا نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جذبهٔ عشق چنان کرده سبک روح مرا</p></div>
<div class="m2"><p>که تنم را ز پر کاه جدا نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان از دو جهان بهر خدا بگذشتن</p></div>
<div class="m2"><p>ترک نظارهٔ خوبان جهان نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دایه چون کرد جدا لعل تو را از پستان؟</p></div>
<div class="m2"><p>که به صنعت شکر از شیر جدا نتوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغبان گر فلک و اهل جهان سیاره اند</p></div>
<div class="m2"><p>می توان خاک شد و نشو و نما نتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای دم آب و لب نان با مردم</p></div>
<div class="m2"><p>می توان مرد و مدارا و ریا نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجد و رقص و طرب و سجده و قربان گشتن</p></div>
<div class="m2"><p>گر [نمایی] تو مرا روی [چها] نتوان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه رفتم به خطا در طلب مشک خطت</p></div>
<div class="m2"><p>بازگشتم که دگرباره خطا نتوان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست می باید و طالع که سعیدا برسد</p></div>
<div class="m2"><p>ورنه کار از مدد آه رسا نتوان کرد</p></div></div>