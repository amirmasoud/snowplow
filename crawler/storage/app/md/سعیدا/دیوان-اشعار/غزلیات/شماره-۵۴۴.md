---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>از بسکه نازک است دل پر ز آرزو</p></div>
<div class="m2"><p>داریم همچو شیشهٔ می گریه در گلو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من عمرهاست خدمت میخانه کرده ام</p></div>
<div class="m2"><p>کی می رسد به داد خمار دلم سبو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه نجات را سر مویی نمانده اند</p></div>
<div class="m2"><p>کردند چاک سینهٔ ما چون به غم رفو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندوه و گریه بود طعام و شراب ما</p></div>
<div class="m2"><p>روزی که می نوشت قلم «و اشربوا کلو»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فردا عمل طلب کند از ما نه نثر و نظم</p></div>
<div class="m2"><p>کاری نرفته پیش، سعیدا به گفتگو</p></div></div>