---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>ز درد ما نه همین آشنا کند پرهیز</p></div>
<div class="m2"><p>ز بیدلی اجل از درد ما کند پرهیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تلخکامی ذاتیم بس عجب نبود</p></div>
<div class="m2"><p>که از شکستهٔ عظمم هما کند پرهیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گاه جلوه چنان نازک است آن کف پا</p></div>
<div class="m2"><p>که همچو چشم ز رنگ حنا کند پرهیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار راه تو هر دیده را که داد جلا</p></div>
<div class="m2"><p>دگر ز سرمه و از توتیا کند پرهیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون صومعه خود را هلال می سازد</p></div>
<div class="m2"><p>ریاگری که ز نشو و نما کند پرهیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن زمان که ز یاقوتی لبش خوردم</p></div>
<div class="m2"><p>دل شکسته ام از مومیا کند پرهیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که خاک شود زر به دست همت او</p></div>
<div class="m2"><p>به خاک افتد و از کیمیا کند پرهیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکیم خسته دلان غیر از این جواب نداد</p></div>
<div class="m2"><p>که گفتمش که سعید از چها کند پرهیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برای قوت ایمان و ضعف اسلامش</p></div>
<div class="m2"><p>کباب داغ مکد از هوا کند پرهیز</p></div></div>