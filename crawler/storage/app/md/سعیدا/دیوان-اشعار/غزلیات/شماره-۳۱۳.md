---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>کی فرنگی با مسلمانی نهانی می کند</p></div>
<div class="m2"><p>آنچه با ما آشکارا یار جانی می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خدنگ حادثات چرخ ای دل گوشه گیر</p></div>
<div class="m2"><p>چون کمان با پشت خم آن هم جوانی می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتح ملک دل به نام عشق ثبت است از ازل</p></div>
<div class="m2"><p>اندرین اقلیم او صاحبقرانی می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا دلداده را جان داده او بی گفتگو</p></div>
<div class="m2"><p>کار صد خضر و مسیحا را زبانی می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست بی شوری خیالم سر دیوانم بکن</p></div>
<div class="m2"><p>شعر من هم کار اشعار فغانی می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانفشانی کن سعیدا گر نداری جان دریغ</p></div>
<div class="m2"><p>از غبار خط رخش عنبرفشانی می کند</p></div></div>