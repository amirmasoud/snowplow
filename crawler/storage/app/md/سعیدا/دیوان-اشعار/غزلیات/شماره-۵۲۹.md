---
title: >-
    شمارهٔ ۵۲۹
---
# شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>از چشم خود ای دلبر شایسته حذر کن</p></div>
<div class="m2"><p>تو خسته نه ای ای نفس از خسته حذر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>[هشدار] ز چشمی که به حیرت نگران است</p></div>
<div class="m2"><p>مستی که به ره می رود آهسته حذر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خال تو سپندی بر آن آتش رخسار</p></div>
<div class="m2"><p>در آینه ز این دانهٔ برجسته حذر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز این بیش مکن جور و جفا رحم به خود کن</p></div>
<div class="m2"><p>از آه دل مردم وارسته حذر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دزدیده نگاه است که دل می برد و دین</p></div>
<div class="m2"><p>آن چشم سیه دیده و دانسته حذر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دلبر محجوب خبردار سعیدا</p></div>
<div class="m2"><p>از پنجهٔ شهباز نظر بسته حذر کن</p></div></div>