---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>ز بس بر سر خیال آن گل رخسار می‌گردد</p></div>
<div class="m2"><p>به سرگر مشت خاری می‌زنم گلزار می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشو غافل ز چرب و نرمی گردون بزم‌آرا</p></div>
<div class="m2"><p>که آخر شمع مومی، نخل آتشبار می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌دانم چه‌ها دیده است دل از گوشهٔ چشمش</p></div>
<div class="m2"><p>که دایم در قفای مردم بیمار می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قلب سبحه گردان چون نظر کردم خبر گشتم</p></div>
<div class="m2"><p>که در تسبیح ذکر و در دلش دینار می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پریشان ساز اول خویش را آن گاه عزت بین</p></div>
<div class="m2"><p>که گل چون شد پریشان بر سر دستار می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا یاد تو در دل‌ها نبخشد جان که تصویرت</p></div>
<div class="m2"><p>اگر در خواب مخمل بگذرد بیدار می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو [مرغی] نیم بسمل شد در او آرام کی باشد</p></div>
<div class="m2"><p>سر منصور از بی‌طاقتی بر دار می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نی تا عشق، بی برگ و نوایم کرد دانستم</p></div>
<div class="m2"><p>که آخر استخوانم ساز موسیقار می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه غم داری سعیدا گر ز پا افتاده‌ای امروز</p></div>
<div class="m2"><p>که فردا دستگیرت خواجهٔ احرار می‌گردد</p></div></div>