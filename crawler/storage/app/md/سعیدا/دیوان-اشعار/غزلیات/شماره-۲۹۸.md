---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>زان زمانی که تن و روح روانم دادند</p></div>
<div class="m2"><p>دل سرگشته و چشم نگرانم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشت خم گشتهٔ من گوشه نشین کرد مرا</p></div>
<div class="m2"><p>تیر همت بشکستند و کمانم دادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقصدم را ز دو عالم به کناری بردند</p></div>
<div class="m2"><p>و آن گه آن خانهٔ مقصود نشانم دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکر آن تنگ دهان کرد چو مویم باریک</p></div>
<div class="m2"><p>تا بتان همچو کمر جا به میانم دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر روشن تر از آیینهٔ جان بخشیدند</p></div>
<div class="m2"><p>غزل صاف تر از آب روانم دادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسمت هر که سعیدا به رضایش کردند</p></div>
<div class="m2"><p>آنچه از روز ازل خواستم آنم دادند</p></div></div>