---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>خاطر جمع بجز عالم یکتایی نیست</p></div>
<div class="m2"><p>عالم امن به از گوشهٔ تنهایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ازل تا به ابد منتظر جانان است</p></div>
<div class="m2"><p>دل شیدایی ما چشم تماشایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لب لعل و خم زلف و خدنگ مژگان</p></div>
<div class="m2"><p>چه خیال است که در آن سر سودایی نیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ کس طاقت نظاره ندارد او را</p></div>
<div class="m2"><p>دلبر ماست که در قید خودآرایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خدنگ غم جانان نکند رم دل ما</p></div>
<div class="m2"><p>که غزال حرم است آهوی صحرایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر در ملک وجود است سعیدا ما را</p></div>
<div class="m2"><p>راه و رسم فقرا بادیه پیمایی نیست</p></div></div>