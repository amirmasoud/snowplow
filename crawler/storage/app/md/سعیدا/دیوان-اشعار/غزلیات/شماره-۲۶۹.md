---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>مجنون به گرد خیمهٔ لیلی نمی رسد</p></div>
<div class="m2"><p>دیوانه را مقام تسلی نمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی به رود نیل زنی خرقه ای عزیز</p></div>
<div class="m2"><p>کس با خدا به جامهٔ نیلی نمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمی ز غیر بسته چو موسی عصا کشی</p></div>
<div class="m2"><p>باید وگرنه کس به تجلی نمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آب گر روی و در آتش وطن کنی</p></div>
<div class="m2"><p>موسی نمی شوی [و] خلیلی نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دست روزگار سعیدا چه گل چه خار</p></div>
<div class="m2"><p>آن رو کدام روست که سیلی نمی رسد؟</p></div></div>