---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>من و آن قامت شوخی که در هنگام جولانش</p></div>
<div class="m2"><p>نشاند سرو را در خاک، اول تیر مژگانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هنگام تبسم معجزانگیز است آن لب ها</p></div>
<div class="m2"><p>که گویا می چکد آب حیات از لعل خندانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه بی رحم است چشم فتنه انگیز کماندارش</p></div>
<div class="m2"><p>که خون خشم می جوشد چو دل از نیش پیکانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسان [جسم] آخر روح او هم خاک خواهد شد</p></div>
<div class="m2"><p>که از تن پروری ها استراحت می کند جانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گلزاری است حسن او که هر شب در نگهبانی</p></div>
<div class="m2"><p>برون برگ گل و چشم است شبنم در گلستانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گنجانش که یوسف را دهم نسبت به آن بدخو</p></div>
<div class="m2"><p>زند سرپنجه ها با دست بیضا خاک دامانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غزالان کرده اند امروز ترک خوش نگاهی را</p></div>
<div class="m2"><p>هنوز از خواب واناگشته چشم سرمه افشانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفای خاطر او را چو گل منکر که می گردد</p></div>
<div class="m2"><p>دل پاکش نمایان است از چاک گریبانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که را صبری که جان آید به لب و آن گه شود قربان</p></div>
<div class="m2"><p>سعیدا می‌شوم من پیشتر از روح قربانش</p></div></div>