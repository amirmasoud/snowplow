---
title: >-
    شمارهٔ ۳۷۹
---
# شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>می برندش به همان راه که آمد واپس</p></div>
<div class="m2"><p>هر که چون صبح زند خنده به خود نیم نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهدا سبحه بینداز که بس کوتاه است</p></div>
<div class="m2"><p>از خم حلقهٔ زلف بت ما دست هوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای کم از مورچه بر خوان لئیمان جهان</p></div>
<div class="m2"><p>چند بر سر بزنی دست تولا چو مگس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه طلسم است در این لاشهٔ دنیا که مدام</p></div>
<div class="m2"><p>می نماید به نظر نفس تو را کس، کرکس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه توان برد ز من فیض چو آن دیوانه</p></div>
<div class="m2"><p>گفت شب مونس من پشه و روز است مگس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیره شد خاطر فرهاد ز سودای مجاز</p></div>
<div class="m2"><p>ظاهر است این که شود خانه سیه ز آتش خس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل شود خسته ز تکلیف که بلبل نالد</p></div>
<div class="m2"><p>گر ز چوب گل صد برگ بساز ند قفس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهدان را ز ره آوازهٔ جنت برده است</p></div>
<div class="m2"><p>سر به صحرا زده این قافله ز آواز جرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیستم طفل رسن تاب ولیکن چون او</p></div>
<div class="m2"><p>کارم از پیشروی رفته سعیدا واپس</p></div></div>