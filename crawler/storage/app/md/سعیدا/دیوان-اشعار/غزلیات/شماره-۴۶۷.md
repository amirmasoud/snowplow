---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>حکایت گل روی تو در چمن کردم</p></div>
<div class="m2"><p>هزار طعنه از آن روی بر سمن کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به داد تشنگیم آب خضر گو نرسید</p></div>
<div class="m2"><p>عقیق لعل لب یار در دهن کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکرد غنچهٔ امید من گل از جایی</p></div>
<div class="m2"><p>چه خارها که چو ماهی به پیرهن کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر زمین که گذشتم ز اشک خونین دم</p></div>
<div class="m2"><p>شکوفه کردم و گل کردم و چمن کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بی وفایی خود دور یک سخن نشیند</p></div>
<div class="m2"><p>به صد هزار زبان همچو گل سخن کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یادگار گل عارضی است این داغم</p></div>
<div class="m2"><p>که همچو لاله به صد رنگ در کفن کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بسکه بیخودیم برده است دل ای عقل</p></div>
<div class="m2"><p>دگر میا که در این شهر من وطن کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی به کافر کتور نمی کند هرگز</p></div>
<div class="m2"><p>ز عشق آنچه سعیدا به خویشتن کردم</p></div></div>