---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>زینهار چون حباب مپرس از نشان صبح</p></div>
<div class="m2"><p>بر هم زده است موج هوا خاندان صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی مکن که بخت سیاهم سفید شد</p></div>
<div class="m2"><p>پوشیده شد ز خندهٔ بیجا دهان صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جیب خویش می کشد این قرص گرم را</p></div>
<div class="m2"><p>هر کس که می شود نفسی میهمان صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی امر روز قسمت روزی نمی کنند</p></div>
<div class="m2"><p>دیوانیان مطبخ الوان [خوان] صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شب مزن به بی ادبی دم، که آفتاب</p></div>
<div class="m2"><p>از آتش، آب داده به تیغ زبان صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جبهه حال اهل سعادت توان شناخت</p></div>
<div class="m2"><p>پوشیده کی شود ز تو راز نهان صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتا ردیف [و] قافیهٔ این غزل به فکر</p></div>
<div class="m2"><p>تا حال کس نکرده چنین امتحان صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی آه سرد دل نتواند کشیدنش</p></div>
<div class="m2"><p>خمیازهٔ شب است سعیدا کمان صبح</p></div></div>