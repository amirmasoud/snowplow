---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>به گوش عرش ز حق می رسد به لفظ فصیح</p></div>
<div class="m2"><p>که به ز روی صبیح است حسن خط ملیح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گوش هر چه رسد در نظر هر آنچه درآید</p></div>
<div class="m2"><p>دلیل وحدت ذات است و آیتی است صریح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عالمان طریقی اگر ملاحظه سازی</p></div>
<div class="m2"><p>به قلب نام کتابی که کرده حق توضیح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی ملاحظه کردیم در جهان حقیقت</p></div>
<div class="m2"><p>حقیقت همه اشیا ملیح بود ملیح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک ز سبحه شماران مهرهٔ خاک است</p></div>
<div class="m2"><p>برای آدم خاکی است بر ملک تسبیح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگیر ساغر خورشید با ید بیضا</p></div>
<div class="m2"><p>رسان به لب که شود کامیاب خضر و مسیح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای دل مده از کف برای راحت تن</p></div>
<div class="m2"><p>نه زیرکی است که این را بر آن کنی ترجیح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حجاب راه سعیدا دو چیز بود که دادم</p></div>
<div class="m2"><p>شبی به میکده سجاده و شبی تسبیح</p></div></div>