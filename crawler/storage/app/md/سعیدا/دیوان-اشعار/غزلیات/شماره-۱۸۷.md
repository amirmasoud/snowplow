---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>دلم به گرمی عشق تو باغ باغ شکفت</p></div>
<div class="m2"><p>به سینه از هوس آتش تو داغ شکفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا به سر زنم شعله را چو پروانه</p></div>
<div class="m2"><p>بدین نشاط که امشب گل چراغ شکفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بسکه جامه به تن تنگ بود زد صد چاک</p></div>
<div class="m2"><p>گمان مبر که گل از دولت فراغ شکفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گشت کشت و تماشای باغ مستغنی است</p></div>
<div class="m2"><p>به یک دو ساغر می هر که را دماغ شکفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین نه زنده سعیداست بر امید لبت</p></div>
<div class="m2"><p>که ز این هوا گل تصویر در ایاغ شکفت</p></div></div>