---
title: >-
    شمارهٔ ۴۰۲
---
# شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>کو چنان حالی که سازد از چنین حالم خلاص</p></div>
<div class="m2"><p>[یا که ] پروازی که سازد از پر و بالم خلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد عالی پایهٔ ادبار یارب زان که او</p></div>
<div class="m2"><p>کرد بی منت ز منت های اقبالم خلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالب آن هندوم کز یک کف خاکستری</p></div>
<div class="m2"><p>کرد از احرام سفید و خرقهٔ شالم خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد بالا نوخطان را دست حسن بر کمال</p></div>
<div class="m2"><p>ساختند از جامهٔ چون خامه چون نالم خلاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد جام باده بدمستی ز دین بیگانه ای</p></div>
<div class="m2"><p>از چهل تن کرد بیرون وز ابدالم خلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش در بزم خموشان یک نفس دادند جای</p></div>
<div class="m2"><p>حالتی آمد که کرد از قیل و از قالم خلاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حساب روز [و] شب عمرم سعیدا صرف شد</p></div>
<div class="m2"><p>کرد آن رخساره و زلف از مه و سالم خلاص</p></div></div>