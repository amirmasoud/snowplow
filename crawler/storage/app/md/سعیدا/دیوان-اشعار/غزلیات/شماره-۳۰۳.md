---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>دلبران چون به هوس رشتهٔ جان می سوزند</p></div>
<div class="m2"><p>بیشتر از همه کس سوختگان می سوزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اتفاقی ز ازل نیست به هم خوبان را</p></div>
<div class="m2"><p>ورنه گر جمع بیایند جهان می سوزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه خوبان جهانسوز، چراغ خوبی</p></div>
<div class="m2"><p>طرفه شمعی است که از پرتو آن می سوزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعله چون تیز شود خشک ز تر فرقی نیست</p></div>
<div class="m2"><p>عشق چون گرم شود پیر و جوان می سوزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل سنگ بتان آتش پنهانی هست</p></div>
<div class="m2"><p>که از آن پنبهٔ هر داغ نهان می سوزند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوزش مشعل خود جوهر مردی نبود</p></div>
<div class="m2"><p>این چراغی است که خود پیرزنان می سوزند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوز اغیار نه از راه کمال شوق است</p></div>
<div class="m2"><p>تا بسوزی تو سعیدا دگران می سوزند</p></div></div>