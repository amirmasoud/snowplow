---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>به هوس باز جوان پیر نگردد هرگز</p></div>
<div class="m2"><p>که کمان گر شکند تیر نگردد هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که دل بست به آن حلقهٔ گیسوی، دگر</p></div>
<div class="m2"><p>خون دل نوشد و دلگیر نگردد هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب بی شام عشا تا سحرش یک سال است</p></div>
<div class="m2"><p>روز روزی به کفان دیگر نگردد هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که در فکر خم زلف بود مجنون است</p></div>
<div class="m2"><p>عاقلی از پی زنجیر نگردد هرگز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل از بار گنه روی دگرگون نکند</p></div>
<div class="m2"><p>رنگ بر چهرهٔ تصویر نگردد هرگز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بناگوش تو می بس نتواند آمد</p></div>
<div class="m2"><p>صبح از خون شفق پیر نگردد هرگز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شام تا صبح به یک آه رسا ساز که ماه</p></div>
<div class="m2"><p>بدر بی محنت شبگیر نگردد هرگز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعلهٔ عشق به پیری ننشیند از جا</p></div>
<div class="m2"><p>کاین حرارت به تباشیر نگردد هرگز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهدا شیوهٔ گردون به ریا برپا نیست</p></div>
<div class="m2"><p>آسیا آب به تزویر نگردد هرگز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رحم در سینهٔ آن شوخ نمی باشد هیچ</p></div>
<div class="m2"><p>آب در جوهر شمشیر نگردد هرگز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طبعم از فهم سخن عجز پذیرا نشود</p></div>
<div class="m2"><p>از بریدن دم شمشیر نگردد هرگز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست در دامن تقدیر سعیدا زده ایم</p></div>
<div class="m2"><p>کار ما راست به تدبیر نگردد هرگز</p></div></div>