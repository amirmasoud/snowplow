---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>عهد کی از عهدهٔ پیمانه می آید برون</p></div>
<div class="m2"><p>توبه ها اشکسته از میخانه می آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انتظار دوستی از دشمنان باید کشید</p></div>
<div class="m2"><p>آشنا تا می رود بیگانه می آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تجلی برنمی خیزیم از خواب عدم</p></div>
<div class="m2"><p>تا نسوزد شمع کی پروانه می آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کفر اگر از کعبه در عالم نمایان شد چه شد</p></div>
<div class="m2"><p>دایماً اسلام از بتخانه می آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه دل در حلقه های زلف او گردیده بند</p></div>
<div class="m2"><p>هر سر مویش به زور از شانه می آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای طفلان است یدگر بعد از این شهر حلب</p></div>
<div class="m2"><p>هر که دارد عقل با دیوانه می آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست تخم آرزو چون لاله و گل هرزه رو</p></div>
<div class="m2"><p>تا قیامت کی ز خاک این دانه می آید برون؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای بر جسم مکدر نه جمال جان ببین</p></div>
<div class="m2"><p>بگذر از جان بعد از آن و صورت جانان ببین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند می بینی ز همدیگر سر و دستار و ریش</p></div>
<div class="m2"><p>فکر کن بگشای چشم و معنی انسان ببین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جاهلی بر نفس خویش و دم ز عرفان می زنی</p></div>
<div class="m2"><p>درد را دانسته آن گه جانب درمان ببین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخت بود از جان گذشتن بعد از آن دیدن جمال</p></div>
<div class="m2"><p>آنچه مشکل می نمودت پیش از این، آسان ببین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در محیط افتاده را کی موجه آید در نظر</p></div>
<div class="m2"><p>استقامت پیشه گردان گردش دوران ببین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می خلد بر جان سعیدا چون شود روز حساب</p></div>
<div class="m2"><p>هر سر مو را به تن امروز چون پیکان ببین</p></div></div>