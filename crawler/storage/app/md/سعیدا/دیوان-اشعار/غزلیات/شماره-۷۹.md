---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>چون دل دلیل نیست تن و [دل] برابر است</p></div>
<div class="m2"><p>آبی که [تشنگی] نبرد گل برابر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>[جایی] که بحر وصل به گرداب [بیخودی] است</p></div>
<div class="m2"><p>جام شراب و مرشد و کامل برابر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردی که [خو] به وادی حیرت گرفته است</p></div>
<div class="m2"><p>صحرا و باغ و جاده و منزل برابر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر خیر از برای عوض می کنند خلق</p></div>
<div class="m2"><p>پس در طمع کریم به سایل برابر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم اگر شوی تو به «العلم نقطة»</p></div>
<div class="m2"><p>دانی که مرکز حق و باطل برابر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه و گدا چو مرد مساوی است زیر خاک</p></div>
<div class="m2"><p>بر اسب یا پیاده به منزل برابر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با توست یار، لیک سعیدا تو غافلی</p></div>
<div class="m2"><p>دریا همیشه با لب ساحل برابر است</p></div></div>