---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>همین نه نرگس تنها گشاده چشم سفید</p></div>
<div class="m2"><p>که در فراق تو شد جام باده، چشم سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گوش نرگس از اخلاص یاسمن می گفت</p></div>
<div class="m2"><p>که نور می برد از روی ساده چشم سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمان زمان رود از خود به سیر گل نرگس</p></div>
<div class="m2"><p>عصا گرفته و بر کف نهاده چشم سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از این چمن دل نرگس از آن جهت برخاست</p></div>
<div class="m2"><p>که هیچ مادر مشفق نزاده چشم سفید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هجر یار سعیدا مگو که چون یعقوب</p></div>
<div class="m2"><p>هزار یوسف مصری فتاده چشم سفید</p></div></div>