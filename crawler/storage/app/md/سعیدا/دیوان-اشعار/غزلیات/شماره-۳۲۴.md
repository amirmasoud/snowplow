---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>چشم او را نگذارید که هشیار شود</p></div>
<div class="m2"><p>فتنه را مصلحت آن نیست که بیدار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار گیرم که نمودار شود کو چشمی</p></div>
<div class="m2"><p>که به یک مرتبه شایستهٔ دیدار شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ما رنگ تعلق نپذیرد هرگز</p></div>
<div class="m2"><p>که ز تمثال کی آیینه گرانبار شود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدف حوصلهٔ ما نشود پر هرگز</p></div>
<div class="m2"><p>گر همه دیدهٔ ما ابر گهربار شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبحه در دست سعیدا مدهید ای زهاد</p></div>
<div class="m2"><p>که مبادا رود و رشتهٔ زنار شود</p></div></div>