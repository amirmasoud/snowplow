---
title: >-
    شمارهٔ ۴۷۹
---
# شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>به گرد کوی جانان های های ساکنی دارم</p></div>
<div class="m2"><p>چو پای مور در این ره صدای ساکنی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفاقت با چو من مشکل بود کس را در این وادی</p></div>
<div class="m2"><p>که دستی بر کمر چون کوه و پای ساکنی دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر دم [آرزویی] سد راه خویش می بینم</p></div>
<div class="m2"><p>چو می آیم به سرمنزل هوای ساکنی دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی ماند نهان در خاک مشت استخوان من</p></div>
<div class="m2"><p>که چون همت به دوش خود همای ساکنی دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد چون غنچه اوراق دل من گر پریشان شد</p></div>
<div class="m2"><p>درون پردهٔ دل دلربای ساکنی دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی دانم سعیدا کی خزان رفت و بهار آمد</p></div>
<div class="m2"><p>در این وادی چو نی برگ و نوای ساکنی دارم</p></div></div>