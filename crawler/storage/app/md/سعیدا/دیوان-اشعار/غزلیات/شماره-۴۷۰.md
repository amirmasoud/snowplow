---
title: >-
    شمارهٔ ۴۷۰
---
# شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>به قطع هستی خود خوب دستیار خودم</p></div>
<div class="m2"><p>همیشه میل کش چشم اعتبار خودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه تشنه لبم دیده بحر استغناست</p></div>
<div class="m2"><p>چو آب می روم عمر در کنار خودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان ز خویش برون رفته ام که شد عمری</p></div>
<div class="m2"><p>نشسته ام شب و روز و در انتظار خودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بیم آه جگرسوز خویشتن دایم</p></div>
<div class="m2"><p>چو ابر منتظر چشم اشکبار خودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفته عقل به دست اختیار من زان ره</p></div>
<div class="m2"><p>همیشه در پی بگسستن مهار خودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن زمان که تو را عین خویشتن دیدم</p></div>
<div class="m2"><p>دگر چو آهوی چشم بتان شکار خودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فکر رفته سعیدا هوای سیر وطن</p></div>
<div class="m2"><p>از آن زمان که غریب دیار یار خودم</p></div></div>