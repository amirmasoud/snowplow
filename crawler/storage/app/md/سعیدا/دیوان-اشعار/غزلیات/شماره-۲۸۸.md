---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>آن نگاه آشنای مشکل آسانت چه شد</p></div>
<div class="m2"><p>با اسیران سر آن کوی، احسانت چه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی ترحم با فقیران نی کرم با بندگان</p></div>
<div class="m2"><p>ای سرت گردم دل و جانم به قربانت چه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوختی از گرمی خوی ای سراپا آفتاب</p></div>
<div class="m2"><p>عالمی را آن سحاب لطف بارانت چه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل پرخون و چشم اشکبارم غافلی</p></div>
<div class="m2"><p>با صراحی عهد و با پیمانه پیمانت چه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با لب خشک و دل پرخون مراد خویش را</p></div>
<div class="m2"><p>گر نمی یابی سعیدا چشم گریانت چه شد</p></div></div>