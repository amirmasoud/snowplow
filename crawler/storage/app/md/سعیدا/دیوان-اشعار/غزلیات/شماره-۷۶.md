---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>مدح رخ زیبای تو عنوان سعیداست</p></div>
<div class="m2"><p>تعریف قدرت مطلع دیوان سعیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چشمه که نامش به جهان آب حیات است</p></div>
<div class="m2"><p>یک قطره ای از دیدهٔ گریان سعیداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لخت جگر سوخته و سینهٔ بریان</p></div>
<div class="m2"><p>پیوسته کباب و مزهٔ خوان سعیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با غیر نبستیم و در دل ننشستیم</p></div>
<div class="m2"><p>اندوه و غم و درد تو مهمان سعیداست</p></div></div>