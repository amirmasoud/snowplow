---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>هر که یاد قامت آن سروبالا می کند</p></div>
<div class="m2"><p>بهر مطلع مصرع برجسته پیدا می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست در خاطر غباری از کسی آیینه ام</p></div>
<div class="m2"><p>هر که می بیند مرا خود را تماشا می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست یک حاکم به ملک حسن، شهر طرفه ای است</p></div>
<div class="m2"><p>خط اگر آزاد سازد زلف دعوا می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نگردم امت لعل لبش کان می پرست</p></div>
<div class="m2"><p>گاه کار خضر و گه کار مسیحا می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدهٔ خودبین چو عکس افتاده در آیینه ها</p></div>
<div class="m2"><p>هر که دارد چشم عبرت بین تماشا می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی حجابش کس چسان بیند سعیدا دیده ام</p></div>
<div class="m2"><p>از حیا در چشم مردم خویش را جا می کند</p></div></div>