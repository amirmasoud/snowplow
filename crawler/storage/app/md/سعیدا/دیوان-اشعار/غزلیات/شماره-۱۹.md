---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>دست گیرید ساغر غم را</p></div>
<div class="m2"><p>پاس دارید خاطر هم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ز خورشید گل عرق نکند</p></div>
<div class="m2"><p>کور سازید چشم شبنم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبلهٔ من شراب تلخ بیار</p></div>
<div class="m2"><p>چه کنم آب شور زمزم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چنین [وحشیی] چه سازد کس</p></div>
<div class="m2"><p>که به رم یاد می دهد رم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخم ما کی به خویش می گیرد</p></div>
<div class="m2"><p>منت چرب و نرم مرهم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به ما روی دست خویش نمود</p></div>
<div class="m2"><p>پشت پایی زدیم عالم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوکت خم نیامدم به نظر</p></div>
<div class="m2"><p>منمایید بادهٔ کم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با هزاران نشاط و ذوق و سرور</p></div>
<div class="m2"><p>صبح سازید شام ماتم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همت خم به جوش چون آید</p></div>
<div class="m2"><p>نتوان برد نام حاتم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معنی شعر ما بیان مکنید</p></div>
<div class="m2"><p>مگشایید زلف درهم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ فتح از کتاب روی نداد</p></div>
<div class="m2"><p>چند بینیم کسره و ضم را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم گریان ما اگر این است</p></div>
<div class="m2"><p>می نشاند به خاک و خون یم را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ساقیا جام از آن میم پرکن</p></div>
<div class="m2"><p>که به چرخ آورد سر جم را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل دیوانه ام به صحرا رفت</p></div>
<div class="m2"><p>تا دهد یاد آهوان رم را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او کجا تاب زلف می آرد</p></div>
<div class="m2"><p>می تراشد ز ناز پرچم را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک نفس پاس دم سعیدا را</p></div>
<div class="m2"><p>چند نوشی تو ساغر دم را</p></div></div>