---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>رزق و روزی را خدا چون گشته از اول کفیل</p></div>
<div class="m2"><p>پس مگردان رو ز باب الله حق نعم الوکیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی شعاع شمس باشد شمس را دیدن محال</p></div>
<div class="m2"><p>جز خدا کس کی تواند با خدا گشتن دلیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش و فرعون و طوفان امتحان را دایم است</p></div>
<div class="m2"><p>گرچه باشی در تقرب نوح و موسی و خلیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلت عقل است فکر کثرت روزی تو را</p></div>
<div class="m2"><p>می رسد روزی مقدر گر کثیر است ار قلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چار طبعت چار میخی گشته در راه طلب</p></div>
<div class="m2"><p>طول حرصت شد به پای اشتر عقلت عقیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطع کن زنجیر تعلق هستی با دم شمشیر حال</p></div>
<div class="m2"><p>کی سعیدا حل شود مشکل تو را از قال و قیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گلو طوق تعلق را بیفکن همچو شیر</p></div>
<div class="m2"><p>تا به کی در خواب می بینی تو هندستان چو فیل؟</p></div></div>