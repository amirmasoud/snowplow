---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>دیوانه شدم باز جنون می کنم امشب</p></div>
<div class="m2"><p>ای عشق مدد ساز که خون می کنم امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا عشق بنا گشته جهان یاد ندارد</p></div>
<div class="m2"><p>عیشی که به این بخت زبون می کنم امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نخل و ثمر آه و نفس سوخته آخر</p></div>
<div class="m2"><p>خود گوی که بی روی تو چون می کنم امشب؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیلی که برد زنگ ز آیینهٔ عالم</p></div>
<div class="m2"><p>از دیدهٔ خونبار برون می کنم امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شمع به خود گریهٔ جانسوز سعیدا</p></div>
<div class="m2"><p>تا کوی فنا راهنمون می کنم امشب</p></div></div>