---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>زمین شوره، ای جان، کی شناسد قدر باران را؟</p></div>
<div class="m2"><p>دلی اندوهگین داند صفای چشم گریان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزاعی نیست با شیطان فقیری را که قلاش است</p></div>
<div class="m2"><p>ز عیاران ره خوفی نباشد مرد عریان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خرمن ها که خاکستر نشد از تابش برقی</p></div>
<div class="m2"><p>کند یک جرعه می افشا هزاران راز پنهان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگردد نکته دان طفلی نباشد عیب استادش</p></div>
<div class="m2"><p>چو بی جوهر بود تیغی چه نقصان است سوهان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن از شاهد و می گو رقیبان را مگو از حق</p></div>
<div class="m2"><p>نصیحت به ز شیرینی نباشد طفل نادان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وفا مشاطهٔ حسن است لیکن کس نمی داند</p></div>
<div class="m2"><p>نگه دارید ای خوبان به عاشق عهد و پیمان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از این عالم که یک ساعت به حال خود نمی‌ماند</p></div>
<div class="m2"><p>چه جمعیت به دست آید سعیدای پریشان را؟</p></div></div>