---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>ترسم ز دیده ای که در او هیچ خواب نیست</p></div>
<div class="m2"><p>از چشم زخم دام چه دل ها کباب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی فکر نیست صوفی پشمینه پوش ما</p></div>
<div class="m2"><p>آیینه در لباس نمد بی حساب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی می رسد خیال به دامان وصل او</p></div>
<div class="m2"><p>گل نی ستاره نیست مه و آفتاب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره در حریم باده پرستان نمی دهند</p></div>
<div class="m2"><p>آن دل که در محبت جانان کباب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک کاسه ای که بر کف دریا نهاده اند</p></div>
<div class="m2"><p>ای تشنگان روید که غیر از حباب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی التفات نخوانند دلبرش</p></div>
<div class="m2"><p>آن دلبری که در بر دل بی حجاب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جا مرو ز یأس سعیدا که مهوشان</p></div>
<div class="m2"><p>خواهند زد به تیر نگه اضطراب نیست</p></div></div>