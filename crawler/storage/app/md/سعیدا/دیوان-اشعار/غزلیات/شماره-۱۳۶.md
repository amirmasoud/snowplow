---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>جنت ز سر کوی تو یک صحن خرابی است</p></div>
<div class="m2"><p>دوزخ ز غم عشق تو یک سینه کبابی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس که گلو را به دم تیغ تو تر کرد</p></div>
<div class="m2"><p>او را به نظر چشمهٔ حیوان دم آبی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چشم خرد، کوه تن و بی سر و پایی است</p></div>
<div class="m2"><p>دریا لب خشکی و گهر قطرهٔ آبی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم دو زمان بر صفت خویش نباشد</p></div>
<div class="m2"><p>ای بی خبران چتر فلک قصر حبابی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را گله از قاضی عنتاب نباشد</p></div>
<div class="m2"><p>فتوی ده این شهر شما طرفه جنابی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای چرخ به رندان جهان این همه مستی</p></div>
<div class="m2"><p>با آن که به مینای تو یک جرعه شرابی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افتاده به گردن گذاران است شب و روز</p></div>
<div class="m2"><p>آخر به کجا تا کشد این طرفه طنابی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشکل همه بر روی زمین است مترسید</p></div>
<div class="m2"><p>در زیر زمین یک دو سؤالی و جوابی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوقات حیات و نفس بازپسین است</p></div>
<div class="m2"><p>در رهگذر مرگ درنگی و شتابی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صحرا چه کند گر نکند خاک به فرقش</p></div>
<div class="m2"><p>کوه از غم او خون دل و چشم پرآبی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست از همه بردار و سبکبار شو امروز</p></div>
<div class="m2"><p>فردای قیامت به میان پای حسابی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بشنو صفت لیلی و مجنون ز سعیدا</p></div>
<div class="m2"><p>آن خانه براندازی و این خانه خرابی است</p></div></div>