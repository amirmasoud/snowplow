---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>عشق پرشور است و ما پراضطراب</p></div>
<div class="m2"><p>یار بی رحم است و ما بی آب و تاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش زلفش در خیال من گذشت</p></div>
<div class="m2"><p>تا سحر شب مار می دیدم به خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم او را در نظر آورده ام</p></div>
<div class="m2"><p>می توانم کرد عالم را خراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می روم از هوش و می گوید دلم</p></div>
<div class="m2"><p>یا شراب و یا شراب و یا شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصد جان دارد بت عیار من</p></div>
<div class="m2"><p>کافری را می کشد بهر ثواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شام گفتا صبح می آیم برون</p></div>
<div class="m2"><p>ای خدایا کی برآید آفتاب؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می پرستی از جوانان عیب نیست</p></div>
<div class="m2"><p>طرفه ایامی است ایام شباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جملهٔ آیات قرآن نازل است</p></div>
<div class="m2"><p>از برای مدح آن عالی جناب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از می عشق آنچه می خواهی بنوش</p></div>
<div class="m2"><p>هیچ کس را نیست دست احتساب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این غزل بر وزن بیت مثنوی است</p></div>
<div class="m2"><p>«خشم مردان خشک گرداند سحاب»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود دعای خود سعیدا می کند</p></div>
<div class="m2"><p>شاه ما بادا ز جانان کامیاب</p></div></div>