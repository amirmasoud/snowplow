---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>بیا به وادی بیداد تخت و چادر زن</p></div>
<div class="m2"><p>به قتل هر دو جهان دست و آستین بر زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آفتاب، سر از مشرق امید برآر</p></div>
<div class="m2"><p>گشای کاکل و طعنی به مشک و عنبر زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خضر در پی آب سیه چه می گردی</p></div>
<div class="m2"><p>بیا به میکده جام شراب احمر زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مساز گردن خود خم به پیش کس به طمع</p></div>
<div class="m2"><p>اگر دراز کنی دست خویش بر سر زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون خانه اگر تنگ شد سعیدا دل</p></div>
<div class="m2"><p>ز خود تهی شود و چون حلقه زود بر در زن</p></div></div>