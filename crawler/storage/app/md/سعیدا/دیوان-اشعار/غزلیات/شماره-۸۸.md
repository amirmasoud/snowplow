---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>در خرابات مغانم جا بس است</p></div>
<div class="m2"><p>از دو عالم ساغر و مینا بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست آرد طاقت نظاره اش</p></div>
<div class="m2"><p>یک نگاه او به عالم ها بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد همت بلندی های ما</p></div>
<div class="m2"><p>در نظر آن قامت رعنا بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دهم فردا جواب نامه را</p></div>
<div class="m2"><p>بی زبانی های ما گویا بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست مأوایی مرا جز بحر دل</p></div>
<div class="m2"><p>جای گوهر در دل دریا بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قامت او عرش را پامال کرد</p></div>
<div class="m2"><p>عشق را معراج آن بالا بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جنونم کون بر هم می خورد</p></div>
<div class="m2"><p>عالمی را این دل شیدا بس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نظر داریم دایم لعل یار</p></div>
<div class="m2"><p>شاهد ما چشم خون پالا بس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در مذاق من سعیدا تا ابد</p></div>
<div class="m2"><p>ذوق آن یکتای بی همتا بس است</p></div></div>