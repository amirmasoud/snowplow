---
title: >-
    شمارهٔ ۳۹۹
---
# شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>برداشتیم چو نظر از اعتبار خویش</p></div>
<div class="m2"><p>دیدیم بی مضایقه دیدار یار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دیده ایم جوهر شمشیر موج آب</p></div>
<div class="m2"><p>داریم ذوق غوطه زدن در کنار خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در قید تار ساختهٔ رشتهٔ سلوک</p></div>
<div class="m2"><p>افتاده ای تو چون گهر از اعتبار خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشندلان که داغ تو بردند زیر خاک</p></div>
<div class="m2"><p>از لاله کرده اند چراغ مزار خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دم هزار ناله کنم همچو عندلیب</p></div>
<div class="m2"><p>از شعبه های طالع ناسازگار خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردیم گرد هستی خود پایمال عشق</p></div>
<div class="m2"><p>برداشتیم از آینهٔ دل غبار خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چشم ناقصان چه بنایی نهاده است</p></div>
<div class="m2"><p>گردون به بام ریختهٔ بی مدار خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل شکست ناخن تدبیر خود به دل</p></div>
<div class="m2"><p>از خار گل علاج کند خارخار خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از کثرت ریاست که سرپیچ و سبحه را</p></div>
<div class="m2"><p>زاهد گذاشت بر سر سنگ مزار خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا دید هر که هست به دامی است مبتلا</p></div>
<div class="m2"><p>شهباز همتم نکند جز شکار خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از چشم روزگار سعیدا فتاده ام</p></div>
<div class="m2"><p>هرگز نکرده ام گله از روزگار خویش</p></div></div>