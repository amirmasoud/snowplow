---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>گر مطلب تو جنگ است صلح و صفا چه باشد</p></div>
<div class="m2"><p>ور آشتی است در دل جور و جفا چه باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پست و بلند دوران در چشم ناقصان است</p></div>
<div class="m2"><p>در عالمی که ماییم ارض و سما چه باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دلبری به نوعی دل می رباید از ما</p></div>
<div class="m2"><p>بیگانگان چنینند تا آشنا چه باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن را که در دل خود مهر حسین دارد</p></div>
<div class="m2"><p>بیم هلاک گشتن از کربلا چه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>[غیریتی] بجز ما مابین ما و او نیست</p></div>
<div class="m2"><p>گر ماسوا نباشیم پس ماسوا چه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چرخ بی مروت ما سرگران نباشیم</p></div>
<div class="m2"><p>با دانهٔ محبت سنگ آسیا چه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل مدار کونین از عشق گشته موجود</p></div>
<div class="m2"><p>در کارخانهٔ عشق کار شما چه باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس به خواهش خود دارد حساب و فکری</p></div>
<div class="m2"><p>تا در میان سعیدا امر خدا چه باشد</p></div></div>