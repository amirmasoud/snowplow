---
title: >-
    شمارهٔ ۴۲۹
---
# شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>بیرون ز عقل هاست در این جا شمار عشق</p></div>
<div class="m2"><p>پیداست کار عقل و هویداست کار عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد جا شکسته ام سر خم گردن سبو</p></div>
<div class="m2"><p>نشکست یک نفس ز سر من خمار عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رد می کند نخست و دگر می کند قبول</p></div>
<div class="m2"><p>اول خزان بیاید و آن گه بهار عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب بقا به ذایقه اش خون جامد است</p></div>
<div class="m2"><p>آن کس که کام یافته از چشمه سار عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این است فرق کعبه و بتخانه نزد ما</p></div>
<div class="m2"><p>آن سنگ آستانه و این سنگسار عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چشم عشق هر دو جهان است یک صدا</p></div>
<div class="m2"><p>جام جم است آینهٔ زنگ دار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازی نکرده جان و به خون تر شدی ز عجز</p></div>
<div class="m2"><p>یک داو بیش نیست جهان در قمار عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این آهوان که روی به صحرا نهاده اند</p></div>
<div class="m2"><p>دارند جملگی هوس مرغزار عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجنون چه چیز و وامق و فرهاد کیستند</p></div>
<div class="m2"><p>[جایی] که گشته لیلی ایشان شکار عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوی جنید و وادی منصور فرق هاست</p></div>
<div class="m2"><p>این دار عقل آمد و آن دار، دار عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز عشق و عاشقی ز سعیدا سؤال نیست</p></div>
<div class="m2"><p>امروز تازه آمده است از دیار عشق</p></div></div>