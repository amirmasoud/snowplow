---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>پای انداز ز پا افتاده دامان است بس</p></div>
<div class="m2"><p>دستگیر دست کوتاهان گریبان است بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طریق آخرت دنیا نمی آید به کار</p></div>
<div class="m2"><p>زاد این ره تا قیامت عهد [و] پیمان است و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ دستی بر گریبان فلک راهی نیافت</p></div>
<div class="m2"><p>این لباس تنگ سر تا پای دامان است بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف معنی را دل مجروح می داند که تیغ</p></div>
<div class="m2"><p>جلوه گر امروز در زخم نمایان است بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتی تن از نگاه تند می پیچد به خود</p></div>
<div class="m2"><p>چین پیشانی در این جا موج طوفان است بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاستن ها در پی است ای ماه بالیدن چرا</p></div>
<div class="m2"><p>هر که سودی می کند امروز نقصان است بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهل بی اندازه در دنیا کلید دولت است</p></div>
<div class="m2"><p>خصمی دنیا همین با اهل عرفان است بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لطف بی حد و حساب یار ما را کشته است</p></div>
<div class="m2"><p>شکوه از دردی که ما داریم درمان است بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست قابل فیض مطلق را سعیدا غیر تو</p></div>
<div class="m2"><p>«علم الاسما» همین در باب انسان است بس</p></div></div>