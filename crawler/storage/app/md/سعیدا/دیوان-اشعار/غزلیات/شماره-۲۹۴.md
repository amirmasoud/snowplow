---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>ز هجرش جان به کار آمد نیامد</p></div>
<div class="m2"><p>سرشکم در کنار آمد نیامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمارآلوده و در کف صراحی</p></div>
<div class="m2"><p>خزان رفت و بهار آمد نیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقیب دیوسیرت شکر ایزد</p></div>
<div class="m2"><p>[به دور] آن نگار آمد نیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من کاری که مقبول تو باشد</p></div>
<div class="m2"><p>فلک هم دستیار آمد نیامد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعیدا از تماشای خیالش</p></div>
<div class="m2"><p>مگر جانت به کار آمد نیامد</p></div></div>