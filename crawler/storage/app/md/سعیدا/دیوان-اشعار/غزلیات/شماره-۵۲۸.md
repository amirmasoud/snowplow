---
title: >-
    شمارهٔ ۵۲۸
---
# شمارهٔ ۵۲۸

<div class="b" id="bn1"><div class="m1"><p>به حال عالم افسرده دیده ای تر کن</p></div>
<div class="m2"><p>چو ابر بر سر این خاک، گریه ای سر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر دو ساغر می، مصرعی بخوان رنگین</p></div>
<div class="m2"><p>به پای دختر رز، عقد شعر، زیور کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوباره حرف مگو از برای عشق سخن</p></div>
<div class="m2"><p>سخن ز عشق اگر می کنی مکرر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشای زلف به باد صبا بگو که برو</p></div>
<div class="m2"><p>دماغ سوختگان مرا معطر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرو بهشت اگر منتی است رضوان را</p></div>
<div class="m2"><p>ز آبرو مگذر ترک آب کوثر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تیغ ناز دو ابرو اشارتی فرما</p></div>
<div class="m2"><p>به ناوک مژه قتل مرا مقرر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخوان کلام سعیدا چو میل کارت نیست</p></div>
<div class="m2"><p>ز کارها که کنی شعر حافظ از بر کن</p></div></div>