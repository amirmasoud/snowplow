---
title: >-
    شمارهٔ ۴۸۴
---
# شمارهٔ ۴۸۴

<div class="b" id="bn1"><div class="m1"><p>دور از رخش چها من بیتاب می کشم</p></div>
<div class="m2"><p>منت ز درد، خون ز رگ خواب می کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکم غبار چشم رکابش نمی شوم</p></div>
<div class="m2"><p>خود را به پای دامن احباب می کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی او کجاست خواب ولی طرح خواب را</p></div>
<div class="m2"><p>گاهی به روی بستر سنجاب می کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید که در خیال بود یوسفی نهان</p></div>
<div class="m2"><p>از چاه دل به دیدهٔ خوب آب می کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دی یک نفس قرار گرفتم به وعده ات</p></div>
<div class="m2"><p>شرمندگی ز دیدهٔ سیماب می کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سجادهٔ نماز کشیدم بسی به دوش</p></div>
<div class="m2"><p>من بعد می به گوشهٔ محراب می کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قد راست چون کنم به تواضع که چون کمان</p></div>
<div class="m2"><p>خمیازه را به قوت دریاب می کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا در حساب نیک و بد از هم شود جدا</p></div>
<div class="m2"><p>من نام خود ز دفتر انساب می کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود را غبار خاطر نیکان نمی کنم</p></div>
<div class="m2"><p>من زنده خاک خویش به سیلاب می کشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رحمت اگر به جرم سعیدا برابر است</p></div>
<div class="m2"><p>فرداست در بهشت می ناب می کشم</p></div></div>