---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>شایستهٔ درگاه تو هر بی سر و پا نیست</p></div>
<div class="m2"><p>درگاه تو جایی است که گنجایش جا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش که درون کار کند شعله ندارد</p></div>
<div class="m2"><p>آن را که دلش سوخته انگشت نما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا یک نظر از رهگذر دیده درآید</p></div>
<div class="m2"><p>یک رهگذری نیست که صد دیده ورانیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست کرمت بسته ره و رسم طلب را</p></div>
<div class="m2"><p>در شهر و دیاری که تویی نام گدا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سو که دلم رفت رهی بود به کویت</p></div>
<div class="m2"><p>این طرفه که تا کوی تو یک راهنما نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی درد به آخر نرسد مرحلهٔ شمع</p></div>
<div class="m2"><p>ماند به ره آن دل که در او آه رسا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از باطن کس حرف زدن شرک خفی دان</p></div>
<div class="m2"><p>کز سر دل آگاه کسی غیر خدا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر عاشق بیچاره ستم این همه یارب</p></div>
<div class="m2"><p>در مذهب معشوق مگر روز جزا نیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ماست سعیدا که خطاها شده پیدا</p></div>
<div class="m2"><p>ورنه ز قلم آنچه به ما رفت خطا نیست</p></div></div>