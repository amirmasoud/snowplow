---
title: >-
    شمارهٔ ۴۸۶
---
# شمارهٔ ۴۸۶

<div class="b" id="bn1"><div class="m1"><p>ندارد منت بار لباس از هیچ کس دوشم</p></div>
<div class="m2"><p>نباشد چون کمان غیر از خدنگ غم در آغوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو افلاطون به وحدت سال ها در خم کنم مسکن</p></div>
<div class="m2"><p>چو می با خام طبعان و هواخواهان نمی جوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چندین جامهٔ ارزق که ای زاهد به برداری</p></div>
<div class="m2"><p>تو عیب من نمی پوشی و من عیب تو می پوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خود رفتم نرفتم هیچ گه از یاد [و] فکر غم</p></div>
<div class="m2"><p>بسی گشتم چو دیدم در دل شادی فراموشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فریاد کجا خم می رسد یا شیشه یا ساغر</p></div>
<div class="m2"><p>مگر دریا شود می تا کند یک لحظه مدهوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حرف سخت سنگ کودکان بهتر که از دردش</p></div>
<div class="m2"><p>نشد پر خاطر دیوانه ز این پر شد از آن گوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب وصلش نبودم باخبر از نشئهٔ مجلس</p></div>
<div class="m2"><p>سعیدا بر دل امروز یاد صحبت دوشم</p></div></div>