---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>تا چون نیم به لب، لب جانان نمی‌رسد</p></div>
<div class="m2"><p>آوازه‌ای به گوش من از جان نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری است اوفتاده حمایل به گردنم</p></div>
<div class="m2"><p>این دست تا به دامن خوبان نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم‌صحبتان پخته طلب کن که چون کباب</p></div>
<div class="m2"><p>جز سوختن ز صحبت خامان نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را چه احتیاج به دارالشفای دل</p></div>
<div class="m2"><p>دردی است درد ما که به درمان نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دست کوتهی است سعیدا اشارتی</p></div>
<div class="m2"><p>تا دامنی که چاک گریبان نمی‌رسد</p></div></div>