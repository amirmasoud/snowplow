---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>بندی چرا میان عداوت به کین چرخ؟</p></div>
<div class="m2"><p>از دست ما درازتر است آستین چرخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود عجب که تیز شود تیغ آفتاب</p></div>
<div class="m2"><p>هر روز می کشد دم خود بر جبین چرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز نام بی وفایی دوران بی مدار</p></div>
<div class="m2"><p>نقشی نکنده اند دگر بر نگین چرخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یخ بسته عالم از خنکی های روزگار</p></div>
<div class="m2"><p>یک موی کم نمی شود از پوستین چرخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر چو آفتاب فرومی روی به خاک</p></div>
<div class="m2"><p>گر می شوی به جهد سعیدا قرین چرخ</p></div></div>