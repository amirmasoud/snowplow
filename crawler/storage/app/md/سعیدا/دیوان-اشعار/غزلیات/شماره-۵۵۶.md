---
title: >-
    شمارهٔ ۵۵۶
---
# شمارهٔ ۵۵۶

<div class="b" id="bn1"><div class="m1"><p>نیست با کس اتحادم همچو کوه</p></div>
<div class="m2"><p>در ازل وحدت نهادم همچو کوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شکستی یافت از من خاطری</p></div>
<div class="m2"><p>[مومیایی] باز دادم همچو کوه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بار سنگین غم عشق تو را</p></div>
<div class="m2"><p>بر دل مسکین نهادم همچو کوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لحن صوت من ز آواز کسی است</p></div>
<div class="m2"><p>ورنه بی او من جمادم همچو کوه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطر صحرا گران از من نشد</p></div>
<div class="m2"><p>گرچه بس سنگین نهادم همچو کوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرنکردم خم سعیدا پیش کس</p></div>
<div class="m2"><p>گرچه از پا اوفتادم همچو کوه</p></div></div>