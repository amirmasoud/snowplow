---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>روی تو چو از پیش نظر پرده برانداخت</p></div>
<div class="m2"><p>آتشکدهٔ مهر به دور قمر انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تو هر قطرهٔ اشکی که برون شد</p></div>
<div class="m2"><p>از دل همه را دیدهٔ من از نظر انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عین سخن خندهٔ آن لب نه ز عقل است</p></div>
<div class="m2"><p>از مستی بسیار نمک در شکر انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عاشق بیچاره چه خواهند که بلبل</p></div>
<div class="m2"><p>یک مشت پری داشت در این رهگذر انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر حلقه که کوتاه شد از دام کمندی</p></div>
<div class="m2"><p>واکرد از آن زلف و گره در کمر انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبع تو اگر کان نمک نیست سعیدا</p></div>
<div class="m2"><p>پس شور چرا شعر تو در بحر و بر انداخت؟</p></div></div>