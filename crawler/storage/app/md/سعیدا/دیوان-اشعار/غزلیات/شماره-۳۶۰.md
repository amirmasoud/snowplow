---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>خدا حاضر خدا ناظر چه در باطن چه در ظاهر</p></div>
<div class="m2"><p>نظر در دید او عاجز زبان در مدح او قاصر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبرداران عالم را یکایک باخبر گشتم</p></div>
<div class="m2"><p>نبود از او کسی آگاه، او از جملگی مخبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این برزخ اسیر ماسوای خود نگردانی</p></div>
<div class="m2"><p>چو اول یاوری کردی تو یاور باش تا آخر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قسم بر مصحف روی تو دارم راست می گویم</p></div>
<div class="m2"><p>که غیر از سورهٔ اخلاص نبود هیچ در خاطر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخش را دیده و دانسته می پوشد ز اهل دل</p></div>
<div class="m2"><p>ندارد رحم بر دل ها چه گوید کس به این کافر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سعیدا از خدا دیگر چه می خواهی که در عالم</p></div>
<div class="m2"><p>زبان در مدح او گویا به نامش گشته دل ذاکر</p></div></div>