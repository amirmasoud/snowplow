---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>عاشق از خود چو رود عزت دیگر دارد</p></div>
<div class="m2"><p>عشق بیرون ز جهان دولت دیگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالمی در طلب کعبه وز این بیخبرند</p></div>
<div class="m2"><p>یار ما غیر حرم خلوت دیگر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر هفتاد و سه مذهب که در این عالم نیست</p></div>
<div class="m2"><p>عشق راه دگر و ملت دیگر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آبرو بر سر شیرینی دوران مفشان</p></div>
<div class="m2"><p>کاین نمک چاشنی و شربت دیگر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه حسن تو و ما هر دو غریبیم غریب</p></div>
<div class="m2"><p>لیک تنهایی ما غربت دیگر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتگان را قدمش گرچه کند منت دار</p></div>
<div class="m2"><p>سایهٔ او به زمین، منت دیگر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار گل گرچه به قدر از رخ گل کمتر نیست</p></div>
<div class="m2"><p>خارخار غم او قیمت دیگر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه عمری است سعیدا که به شادی شادیم</p></div>
<div class="m2"><p>لیک غم با دل ما سبقیت دیگر دارد</p></div></div>