---
title: >-
    شمارهٔ ۴۸۹
---
# شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>از حال من مپرس [و] ندامت کشیدنم</p></div>
<div class="m2"><p>خو گشته پشت دست به دندان گزیدنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم شکست می خورم اما به چشم خلق</p></div>
<div class="m2"><p>از بس شکسته ام ننماید شکستنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیچد به خویش دشمن من بیشتر ز پیش</p></div>
<div class="m2"><p>در چشم روزگار چو مو گر شود تنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عقل رو به وادی حیرت نهاد و رفت</p></div>
<div class="m2"><p>هر کس که دید همچو تو از خویش رفتنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس شکست خورد سعیدا دلم ز خلق</p></div>
<div class="m2"><p>رنگ آن قدر نماند به رویم که بشکنم</p></div></div>