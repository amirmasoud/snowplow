---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>من نه آنم که زنم ساز و به خود گریه کنم</p></div>
<div class="m2"><p>خنده بر چرخ بد انداز و به خود گریه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست جا این قدر شمع مرا چون فانوس</p></div>
<div class="m2"><p>تا کشم شعلهٔ آواز و به خود گریه کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنما سرو قد خویش که من فاخته وار</p></div>
<div class="m2"><p>تا شوم از همه ممتاز و به خود گریه کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست آشفتگیم از گل رخسار کسی</p></div>
<div class="m2"><p>چون خیال تو کنم ناز و به خود گریه کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من حسرت زده را باده از آن خم مدهید</p></div>
<div class="m2"><p>که خبردار شوم باز و به خود گریه کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ اگر آب دهد از دم تیغ تو شوم</p></div>
<div class="m2"><p>همچو فواره سرافراز و به خود گریه کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخت ماتم زده ام نوحه گران می خواهم</p></div>
<div class="m2"><p>تا شوندم همه دمساز و به خود گریه کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بستم احرام که با نغمه روم همچو خیال</p></div>
<div class="m2"><p>در سراپردهٔ آواز و به خود گریه کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکوه چون مدعیم نیست سعیدا ز کسی</p></div>
<div class="m2"><p>گله از خود کنم آغاز و به خود گریه کنم</p></div></div>