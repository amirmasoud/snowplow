---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>دل ها به یاد صحبت مستان کباب شد</p></div>
<div class="m2"><p>معموره ها ز دولت خوبان خراب شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب بود شمع مجلس ما صبحدم چو شد</p></div>
<div class="m2"><p>بیرون شد از خرابهٔ ما آفتاب شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل پیش از این نداشت رواجی به چشم عشق</p></div>
<div class="m2"><p>این شیشه تا شکست به سنگ، انتخاب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر قطرهٔ عرق که چکید از رخش به ناز</p></div>
<div class="m2"><p>پیمانه گشت و جام شکست و گلاب شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تخمیر ما ز بادهٔ گلرنگ کرده اند</p></div>
<div class="m2"><p>افتاده هر چه در قدح ما شراب شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد چو برف زاهد و بنشست همچو یخ</p></div>
<div class="m2"><p>از همت صراحی و می رفت و آب شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبحی دمی جمال تو می خواستم ز حق</p></div>
<div class="m2"><p>برداشتی نقاب و دعا مستجاب شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کرد عرض حال سعیدا به پیش او</p></div>
<div class="m2"><p>لیکن زبان گرفت وی از اضطراب شد</p></div></div>