---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>رو به هر کوهی نماید طور می‌دانیم ما</p></div>
<div class="m2"><p>هرکه گوید حرف حق منصور می‌دانیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غایبان گر در حضور ما سخن‌ها گفته‌اند</p></div>
<div class="m2"><p>نیست ز ایشان رنجی معذور می‌دانیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرب دلبر جز فنای عاشق بیچاره نیست</p></div>
<div class="m2"><p>هرکه نزدیک است او را دور می‌دانیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کام از انگشت حلوای جهان شیرین مکن</p></div>
<div class="m2"><p>این بنا را خانهٔ زنبور می‌دانیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار اگر آن است ای زاهد که مایش دیده‌ایم</p></div>
<div class="m2"><p>از من و تو دید او مستور می‌دانیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سری کان از خیال او ندارد شورشی</p></div>
<div class="m2"><p>خشک‌تر از کاسهٔ تنبور می‌دانیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تماشای گل روی تو چشم غیر را</p></div>
<div class="m2"><p>همچو نرگس در گلستان کور می‌دانیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردمان دیده را ورد است دایم «ان یکاد»</p></div>
<div class="m2"><p>چشم بد را از جمالش دور می‌دانیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما معانی در نظر داریم نی ریش و سبیل</p></div>
<div class="m2"><p>کاسهٔ می گل بود فغفور می‌دانیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صحتی کان از طعام و می‌کند حاصل کسی</p></div>
<div class="m2"><p>در طریقت خسته و رنجور می‌دانیم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل اگر بی‌طاقتی در هجر دارد عیب نیست</p></div>
<div class="m2"><p>اندرین معنی ورا مجبور می‌دانیم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای سعیدا می‌روی بر دار چون خود گفته‌ای</p></div>
<div class="m2"><p>هرکه گوید حرف حق منصور می‌دانیم ما</p></div></div>