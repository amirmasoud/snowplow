---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>نگریم بعد از این ترسم که دل خون جگر گردد</p></div>
<div class="m2"><p>نسوزم سینه را داغش مبادا چشم تر گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خورشید جمالش نیست باکم بیم از آن دارم</p></div>
<div class="m2"><p>که خط پیدا شود بر آن رخ و دور قمر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خاک آن کف پا روی خود می مالم و شادم</p></div>
<div class="m2"><p>که مس بر کیمیا چون برخورد البته زر گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کم دارد که دارد خانه زادی همچو زلف خود</p></div>
<div class="m2"><p>که گه بر پایش افتد گه به قربان کمر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به این قدر گران ای ناز با کاکل چه می پیچی</p></div>
<div class="m2"><p>گذار این سر به پا افکنده را بر گرد سر گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوا خاکسترش را باز در پرواز می آرد</p></div>
<div class="m2"><p>مدان پروانه بعد از سوختن بی بال و پر گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال خود سعیدا دیدن نقصان خود باشد</p></div>
<div class="m2"><p>اگر بینا شوی بر عیب خود عیبت هنر گردد</p></div></div>