---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>چه شور است این که در کاشانهٔ ماست</p></div>
<div class="m2"><p>که عقل ذوفنون دیوانهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک صیاد ما صید و جهان دام</p></div>
<div class="m2"><p>نصیب و قسمت، آب و دانهٔ ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی دانم که را قسمت نمایند</p></div>
<div class="m2"><p>شرابی را که در پیمانهٔ ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکایت های آدم تا به این دم</p></div>
<div class="m2"><p>چو نیکو بنگری افسانهٔ ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعیدا را بس است این گر بگویی</p></div>
<div class="m2"><p>که این ناآشنا بیگانهٔ ماست</p></div></div>