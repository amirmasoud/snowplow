---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>صبحدم تا شده بیرون ز افق، خاور مهر</p></div>
<div class="m2"><p>رفت جان بر سر مهر و دل من در بر مهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظاهر و باطن معشوق به هم دارد جنگ</p></div>
<div class="m2"><p>دل او صورت قهر و تن او پیکر مهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهر خالی است ز خوبان و دل ظالم سخت</p></div>
<div class="m2"><p>باشد از غیب رسد بر سر ما لشکر مهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبحدم ته به ته غنچهٔ گل را دیدم</p></div>
<div class="m2"><p>همه خون بسته ز بی مهری او بر سر مهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز تجلی نبود جسم مرا رنگ وجود</p></div>
<div class="m2"><p>نیست جز ذره هواخواه به بال و پر مهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آذرش را ز ازل قوت گیرایی نیست</p></div>
<div class="m2"><p>که نشد پخته یکی خام از این اخگر مهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آتش حسن تو خورشید اگر سوخت چه باک</p></div>
<div class="m2"><p>می توان ساخت دوصد مهر ز خاکستر مهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون صبا زلف شب از چهرهٔ مقصود گشود</p></div>
<div class="m2"><p>بود طغرا خط مشکین تو بر دفتر مهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک از حلقه به گوشان در جانان است</p></div>
<div class="m2"><p>می کند سجده به پیش بت ما داور مهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی رسد بادهٔ دیدار سعیدا تا هست</p></div>
<div class="m2"><p>اندرین میکده مینا فلک و ساغر مهر؟</p></div></div>