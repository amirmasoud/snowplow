---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>بی برگ شو چو نی به نوا می توان رسید</p></div>
<div class="m2"><p>از راه نیستی به خدا می توان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وامانده از وصول، اصول ای فروع جوی</p></div>
<div class="m2"><p>از سایه گر بری به هما می توان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل به کوی یار اگر درد، همره است</p></div>
<div class="m2"><p>مردانه زن قدم به دوا می توان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سرو با کجی مکن الفت در این چمن</p></div>
<div class="m2"><p>از راستی به نشو و نما می توان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او خود مگر به جذبه سعیدا کشد تو را</p></div>
<div class="m2"><p>ورنه به جد و جهد کجا می توان رسید؟</p></div></div>