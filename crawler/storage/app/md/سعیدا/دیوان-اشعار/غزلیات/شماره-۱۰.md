---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>عشق عالمسوز ما بر هم زند تدبیر را</p></div>
<div class="m2"><p>جذبهٔ سرشار ما از هم کند زنجیر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه شورش در دماغ طفل ما جا کرده است</p></div>
<div class="m2"><p>باز در پستان مادر می کند خون شیر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه در عین جلا بر شعله می پیچد چو دود</p></div>
<div class="m2"><p>می برد از نالهٔ ما آه ما تأثیر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بیجا در شکنج زلف، دل را جستجو</p></div>
<div class="m2"><p>کعبه رو بیهوده کی سر می کند شبگیر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد چاک سینهٔ ما قدر ابرو را بلند</p></div>
<div class="m2"><p>می دهد زخم نمایان آبرو شمشیر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر گلشن با می و شاهد کند کس را جوان</p></div>
<div class="m2"><p>ورنه هر یک غنچه پیکانی است در دل، پیر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود دل خون ز فکر خنجر مژگان او</p></div>
<div class="m2"><p>سایهٔ آن زلف می پیچد به پا نخجیر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون رسد مژگان خونریزش مصور را به یاد</p></div>
<div class="m2"><p>می کند بیدار از خواب عدم تصویر را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خیال کعبهٔ دیدار و راه مشکلش</p></div>
<div class="m2"><p>می کند یک رفتن از خود کار صد شبگیر را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بند نتوان کرد ای ناصح سعیدا را به پند</p></div>
<div class="m2"><p>بارها دیوانهٔ ما کنده این زنجیر را</p></div></div>