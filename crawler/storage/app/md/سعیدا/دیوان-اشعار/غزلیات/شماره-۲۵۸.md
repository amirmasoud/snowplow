---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>دی صبا قصهٔ آن کاکل پیچان می کرد</p></div>
<div class="m2"><p>جمع می کرد دل خلق و پریشان می کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگهش هر دل آشفته که می برد از راه</p></div>
<div class="m2"><p>می گرفتش خم زلف از ره و پنهان می کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخت مشکل شده از دست خرد، کار مرا</p></div>
<div class="m2"><p>کو می صاف که می آمد و آسان می کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن از معجزهٔ آن لب خندان چه کنم</p></div>
<div class="m2"><p>سنگ را بی سخن آن لعل بدخشان می کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر شمیمی که نسیم از ره او می آورد</p></div>
<div class="m2"><p>می گرفتش گل و در چاک گریبان می کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات مغان هم دگر آن باده نماند</p></div>
<div class="m2"><p>که دو جامش به صفت جانور انسان می کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خم نشین گشته فلاطون ز مسیحا پرسید</p></div>
<div class="m2"><p>هر که درد خودی داشت چه درمان می کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موج زد خاطر بحر و به نظر زود آمد</p></div>
<div class="m2"><p>ورنه چشم گهرافشان تو طوفان می کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قابل فیض نبودیم سعیدا در اصل</p></div>
<div class="m2"><p>لیکن آن مبدأ فیض از کرم احسان می کرد</p></div></div>