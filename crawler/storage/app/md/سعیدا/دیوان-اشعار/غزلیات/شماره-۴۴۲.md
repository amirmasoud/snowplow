---
title: >-
    شمارهٔ ۴۴۲
---
# شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>از آن زمان که غم او گرفته جای خیال</p></div>
<div class="m2"><p>دگر به گوشهٔ دل کم رسیده پای خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روز وصل دلم صاف تر ز آینه بود</p></div>
<div class="m2"><p>فراق یار مرا کرد آشنای خیال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فکر گوی سخن گر مراد می خواهی</p></div>
<div class="m2"><p>که زر شود سخن مس ز کیمیای خیال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه فکرها که نکردم نمی رسد چه کنم</p></div>
<div class="m2"><p>به غیر پنجهٔ دل دست کس به پای خیال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فکر نفی سوای تو بسکه گردیدم</p></div>
<div class="m2"><p>نیافتیم در این خانه ماسوای خیال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر خیال که رفتم گرفت دردسرم</p></div>
<div class="m2"><p>نشد موافق طبع دلم هوای خیال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان اگرچه خیال است لیک حضرت حق</p></div>
<div class="m2"><p>نیافریده سعیدا تو را برای خیال</p></div></div>