---
title: >-
    شمارهٔ ۳۹۴
---
# شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>یک قامت بلند به هر می کشیدنش</p></div>
<div class="m2"><p>بالا برآید آب لطافت ز گردنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست که می رسد که گریبان او کشد</p></div>
<div class="m2"><p>صد جان و دل کشیده سر از پای دامنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میدان ترکتاز خیالات او منم</p></div>
<div class="m2"><p>جای جفاست تا به دل از دیدهٔ منش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان عزیز نیست اگر جسم او چرا</p></div>
<div class="m2"><p>نازکتر از خیال نماند به من تنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگر به خویش باز نیاید به سال ها</p></div>
<div class="m2"><p>یک بار هر که از بر خود دید رفتنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من باده نوش ساغر آن ساقیم که گاه</p></div>
<div class="m2"><p>کیفیت شراب دهد لب مکیدنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارم بتی و شکر سعیدا که روی او</p></div>
<div class="m2"><p>دیدن نمی توان مگر از دیدهٔ منش</p></div></div>