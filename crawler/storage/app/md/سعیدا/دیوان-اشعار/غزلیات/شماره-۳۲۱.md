---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>دیدم به چشم سر، که سکندر نشسته بود</p></div>
<div class="m2"><p>در چنگ انفعال دلش ریش و خسته بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه اش ز پای ستوران نمود روی</p></div>
<div class="m2"><p>او دل مثال آینه ز آیینه شسته بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احوال ملک [و] حشمت دارا ز من بپرس</p></div>
<div class="m2"><p>جم بر زمین فتاده و جامش شکسته بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدم به نقش ساده بتی سجده می نمود</p></div>
<div class="m2"><p>دی زاهدی که نقش به دیوار بسته بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر به زلف یار سعیدا اسیر شد</p></div>
<div class="m2"><p>با آن که صید زیرک از دام جسته بود</p></div></div>