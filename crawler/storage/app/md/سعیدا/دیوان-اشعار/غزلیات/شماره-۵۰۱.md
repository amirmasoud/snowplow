---
title: >-
    شمارهٔ ۵۰۱
---
# شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>دامان دل ز گرد هوس چیده می روم</p></div>
<div class="m2"><p>این کوه قاف بین که چه بریده می روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز این تنگنا چو شعلهٔ آتش به دود آه</p></div>
<div class="m2"><p>آخر ز دست حادثه پیچیده می روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شاهراه عالم معنی فتاده سیر</p></div>
<div class="m2"><p>چشم از عیان ثابته پوشیده می روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر قدم که در ره او می نهم به صدق</p></div>
<div class="m2"><p>از هر جمال خدا دیده می روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عالم فنا به جهان بقا گذر</p></div>
<div class="m2"><p>این طرفه منزلی است که خوابیده می روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود را ز چشم محرم [و] نامحرم جهان</p></div>
<div class="m2"><p>در جامهٔ برهنگی پوشیده می روم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجنون صفت سلوک نکردم طریق عشق</p></div>
<div class="m2"><p>این راه را من از همه پرسیده می روم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس که می رود ز جهان گریه می کند</p></div>
<div class="m2"><p>الا فقیر بر همه خندیده می روم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرگز در این سفر به خودم آشتی نشد</p></div>
<div class="m2"><p>دایم ز خود بریده و رنجیده می روم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بودنم رضای تو گر نیست غم مخور</p></div>
<div class="m2"><p>من دست خویش و پای تو بوسیده می روم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نیک و بد هر آنچه سعیدا در این ره است</p></div>
<div class="m2"><p>چون صانعش یکی است پسندیده می روم</p></div></div>