---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>چاک پیراهن یار و نظر پاک، یکی است</p></div>
<div class="m2"><p>گوشهٔ دامن پاک و دل غمناک یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد مردن نکشم منت آرایش قبر</p></div>
<div class="m2"><p>چون برد خواب گران تخت زر و خاک یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطلب از سیر چمن روی تو باشد ما را</p></div>
<div class="m2"><p>چون تو منظور نباشی گل و خاشاک یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست بردار ز جان چون اجلت کرد اسیر</p></div>
<div class="m2"><p>صید را گوشهٔ دام و سر فتراک یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر و بر هر دو سعیدا ز ازل یارانند</p></div>
<div class="m2"><p>دل حسرت زده و دیدهٔ نمناک یکی است</p></div></div>