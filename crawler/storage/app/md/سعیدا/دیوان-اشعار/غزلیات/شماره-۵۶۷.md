---
title: >-
    شمارهٔ ۵۶۷
---
# شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>دیده بیرون شده از پرده به دیدار کسی</p></div>
<div class="m2"><p>جان به لب آمده از لعل شکربار کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو تصویر نیم زنده ولیکن دارم</p></div>
<div class="m2"><p>چشم در راه کسی پشت به دیوار کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس از چشم و گل از دست کسی افتاده است</p></div>
<div class="m2"><p>سنبل آویخته از طرهٔ طرار کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه دیدیم ز جایی به ظهور آمده است</p></div>
<div class="m2"><p>خار از پای کسی، سرو ز رفتار کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه خود درد دل بندهٔ خود می داند</p></div>
<div class="m2"><p>نیست در پیش کسی حاجت اظهار کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غلط کی به سرچشمهٔ حیوان می رفت</p></div>
<div class="m2"><p>خضر می بود اگر تشنهٔ دیدار کسی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمرها شد که در این دایره سرگردانند</p></div>
<div class="m2"><p>ماه مشتاق کسی، مهر طلبکار کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جهان هر که به شغلی است سعیدا مأمور</p></div>
<div class="m2"><p>تو میاویز به عیب و هنر و کار کسی</p></div></div>