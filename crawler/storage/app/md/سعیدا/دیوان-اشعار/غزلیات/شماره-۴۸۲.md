---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>نگیرم گوشه از راه خدنگت گر گمان باشم</p></div>
<div class="m2"><p>زمین بوس تو می آرم بجا گر آسمان باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبردار از تو می گردم چو از خود بی خبر گردم</p></div>
<div class="m2"><p>نشانت می توانم داد چون من بی نشان باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرافرازی توانم کرد با گردون در این میدان</p></div>
<div class="m2"><p>گر اول قابل قربان تیغ امتحان باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نام خود توانم گشت عالمگیر در عالم</p></div>
<div class="m2"><p>چو عنقا من هم از چشم خلایق گر نهان باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزن آتش سعیدا در جهان رنگ و بو آخر</p></div>
<div class="m2"><p>چو بلبل چند در فکر و غم این آشیان باشم</p></div></div>