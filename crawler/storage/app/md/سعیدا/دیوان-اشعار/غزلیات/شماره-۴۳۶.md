---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>برکنید ای بت پرستان از ته دل دل ز سنگ</p></div>
<div class="m2"><p>کی شود آسان شما را حل این مشکل ز سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند ابراهیم از دل های سنگین نقش غیر</p></div>
<div class="m2"><p>گرچه آذر می تراشد صورت باطل ز سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سختگیری افکند از پا درخت خشک را</p></div>
<div class="m2"><p>کی بنای خانهٔ خود را کند عاقل ز سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنجش از اطوار دور ای دل نشان غافلی است</p></div>
<div class="m2"><p>چین نبندد بر جبین دیوانهٔ کامل ز سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هفتهٔ دوران ما از بس به سختی می رود</p></div>
<div class="m2"><p>کاروانی بسته گویا بار این محمل ز سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی شود شیرین مذاق عاشق از‌ آن سنگدل</p></div>
<div class="m2"><p>کوهکن گر مقصد خود را کند حاصل ز سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کام از آن دل یافتن مشکل سعیدا شد که کوه</p></div>
<div class="m2"><p>خوردن بس خون جگر تا لعل شد حاصل ز سنگ</p></div></div>