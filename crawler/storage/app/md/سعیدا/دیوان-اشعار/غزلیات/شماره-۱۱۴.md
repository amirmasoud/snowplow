---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>آنچه در شش جهات گردون است</p></div>
<div class="m2"><p>بهر اثبات ذات بیچون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه آورده از عدم به وجود</p></div>
<div class="m2"><p>به حقیقت نگر که موزون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیلیی را که نیستش طرفی</p></div>
<div class="m2"><p>هر طرف صدهزار مجنون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عارفان زان شدند دیوانه</p></div>
<div class="m2"><p>که شناسش ز عقل بیرون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نشانی کزو شود پیدا</p></div>
<div class="m2"><p>مرو از جا که فعل واژون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش علمش جهان و هرچه در اوست</p></div>
<div class="m2"><p>به مثل همچو نقطه در نون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه در خانهٔ قدم ره یافت</p></div>
<div class="m2"><p>از بلای حدوث مأمون است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست از معنیت خبر ورنه</p></div>
<div class="m2"><p>نیک و بد آنچه هست مضمون است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست حد قیاس ذاتش را</p></div>
<div class="m2"><p>او مبرا ز کم ز افزون است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر در چون چرا کنی ضایع؟</p></div>
<div class="m2"><p>بازگشت که سوی بیچون است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو مگو وقت رفت از دستم</p></div>
<div class="m2"><p>آنچه آن وقت بود اکنون است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاد کن خاطر سعیدا را</p></div>
<div class="m2"><p>در فراق رخ تو محزون است</p></div></div>