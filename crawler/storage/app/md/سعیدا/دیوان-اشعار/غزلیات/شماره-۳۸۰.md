---
title: >-
    شمارهٔ ۳۸۰
---
# شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>کاکل خود را ببین از پیچ و تاب ما مپرس</p></div>
<div class="m2"><p>طالع بیدار خود را بین ز خواب ما مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب ببند از گفتگو درسی ز حال ما بگیر</p></div>
<div class="m2"><p>جز خموشی نیست حرفی از کتاب ما مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینهٔ بریان دل مجروح چشم تر مزه است</p></div>
<div class="m2"><p>جای می خالی است ساقی از کباب ما مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبض ما بگرفت شد دست فلاطون رعشه دار</p></div>
<div class="m2"><p>می شود سیماب، آب از اضطراب ما مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد چو گردون در محیط عشق سرها سرنگون</p></div>
<div class="m2"><p>موج دریای قدیمیم از حباب ما مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرصت یک آب خوردن نیست عمرت بیش باد</p></div>
<div class="m2"><p>می شوی سرگشته ای خضر از شتاب ما مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف را بگشا گره بر کاکل مشکین مزن</p></div>
<div class="m2"><p>می کنی سررشته گم از پیچ و تاب ما مپرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسمان خم شد ز بار نامهٔ اعمال ما</p></div>
<div class="m2"><p>می شوی درمانده از روز حساب ما مپرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه می خواهی بکن اما مگو چون کرده ای</p></div>
<div class="m2"><p>آنچه می خواهی بگو لیک از جواب ما مپرس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرح حال ما سعیدا نیست با کس گفتنی</p></div>
<div class="m2"><p>می‌شوی دیوانه از کیف شراب ما مپرس</p></div></div>