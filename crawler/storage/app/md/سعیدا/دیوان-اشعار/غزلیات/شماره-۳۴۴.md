---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>مگر که بوی گل و لاله [از] هوا جوشید</p></div>
<div class="m2"><p>که باز شور و جنون در دماغ ما جوشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنان ز گرد تو شد ابلق نظر را گرم</p></div>
<div class="m2"><p>غبار کوی تو گویا به توتیا جوشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه الفت است خدایا که او به من دارد</p></div>
<div class="m2"><p>کسی ندیده به بیگانه آشنا جوشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حنا به آن کف پا رو به رو نشد هرگز</p></div>
<div class="m2"><p>بسی به خون شهیدان کربلا جوشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روی یار سعیدا سخن به عکس مگوی</p></div>
<div class="m2"><p>که خون شرم در آیینهٔ حیا جوشید</p></div></div>