---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>در کفر حق پرست شو آن گه خداشناس</p></div>
<div class="m2"><p>بیگانه را یگانه شو و آشنا شناس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با جغد، پاس کنج خرابی نگاه دار</p></div>
<div class="m2"><p>هر سایه ای که بر تو فتد از هما شناس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محو جمال یار شو و سیر خلد کن</p></div>
<div class="m2"><p>بر زلف پیچ عالم بی منتها شناس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منعم کسی بود که ز خود دست شسته است</p></div>
<div class="m2"><p>ناشسته روی چند جهان را گدا شناس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تشنگی بمیر و مده آبرو ز دست</p></div>
<div class="m2"><p>گوهر شو آبروی خود آب بقا شناس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندین هزار مقصد و مطلب که خلق راست</p></div>
<div class="m2"><p>در جنب فضل دوست تو یک مدعا شناس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرف بجا نمی شنوند اهل روزگار</p></div>
<div class="m2"><p>زینهار در میانهٔ ایشان تو جا شناس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این نور چشم تن شود آن نور چشم جان</p></div>
<div class="m2"><p>از خاک کوی دوست تو تا توتیا شناس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگشا به روی هر که در خلق خویش را</p></div>
<div class="m2"><p>بیگانه را و محرم از آواز پاشناس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما چون چراغ روشن [و] دنیاست همچو شب</p></div>
<div class="m2"><p>بگشای چشم باطن و ما را به ما شناس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مخلوط گشته کشته و ناکشته در جهان</p></div>
<div class="m2"><p>ناکشته کشته را تو در این کربلا شناس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیهوده آشنا چه شوی زید و عمرو را</p></div>
<div class="m2"><p>با آن که گفته است خدابین خداشناس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرهون وقت بوده سعیدا سرور [و] غم</p></div>
<div class="m2"><p>با وقت آشنا شو و ذوق و صفا شناس</p></div></div>