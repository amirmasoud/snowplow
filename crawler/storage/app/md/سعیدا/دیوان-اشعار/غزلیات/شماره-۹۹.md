---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>آن آفرین جهان که نگهدار عالم است</p></div>
<div class="m2"><p>هر دلبر زمانه هم اغیار عالم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی که وا ز کثرت غفلت نمی شود</p></div>
<div class="m2"><p>تا روز حشر دیدهٔ پندار عالم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازم به آن بتی که به هر پا گذاشتن</p></div>
<div class="m2"><p>صد گام پیش از پی آزار عالم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قید ماست عالم و ما حظ نمی کنیم</p></div>
<div class="m2"><p>ای وای بر کسی که گرفتار عالم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر ذره ای ز مهر رخش رقص می کند</p></div>
<div class="m2"><p>کافر بود کسی که در انکار عالم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردون، متاع کینه فروشد به مشتری</p></div>
<div class="m2"><p>روزی که مهر بر سر بازار عالم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم گدا به این همه تنگی و خیرگی</p></div>
<div class="m2"><p>سیری ندیده، سیر ز دیدار عالم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منت ز آب و گل نکشد دل چو شد خراب</p></div>
<div class="m2"><p>کاین خانه بی نیاز ز معمار عالم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آفتاب حشر سعیدا چه می کند</p></div>
<div class="m2"><p>آن کس که سایه پرور دیوار عالم است؟</p></div></div>