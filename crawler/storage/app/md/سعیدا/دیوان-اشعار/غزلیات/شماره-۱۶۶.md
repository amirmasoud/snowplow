---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>عاشقان را رخصت دیدار آن مه پاره نیست</p></div>
<div class="m2"><p>همچو خورشیدش کسی را طاقت نظاره نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بهای یک نگاهش دین و دل دارد طمع</p></div>
<div class="m2"><p>غیر جان دادن در این راه مفلسان را چاره نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیر می نوشد ز پستان اجل طفل دلیر</p></div>
<div class="m2"><p>در نظر تابوت، مردان را بجز گهواره نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی رسد در دامن مطلب بجز دست خیال؟</p></div>
<div class="m2"><p>ای زلیخا یوسف معنی گریبان پاره نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز خویَش گر کَنی لیکن به رویش چون کنی؟</p></div>
<div class="m2"><p>می توانی کرد این را چاره آن را چاره نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا ز پا می افکند یا دست می گیرد جهان</p></div>
<div class="m2"><p>دایهٔ این دور، خونخوار است اگر غمخواره نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که گم گردد در این صحرا به مطلب می رسد</p></div>
<div class="m2"><p>خضر این وادی سعیدا جز دل آواره نیست</p></div></div>