---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>عندلیب روح را تن آشیان گردیده است</p></div>
<div class="m2"><p>یوسفی را چاه زندان خانمان گردیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرها شد جان به گرد کوی او دارد طواف</p></div>
<div class="m2"><p>تن به عزم دیدن آن رو روان گردیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سرم گردد به گرد دل عجایب نیست این</p></div>
<div class="m2"><p>بر سر یک نقطه ای نه آسمان گردیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناتوانی بسکه ما را بر زمین افکنده است</p></div>
<div class="m2"><p>آسمان در خانهٔ ما آستان گردیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیشتر عرفان سعیدا جهل آمد بر درش</p></div>
<div class="m2"><p>بس یقین‌ها بر سر آن کو گمان گردیده است</p></div></div>