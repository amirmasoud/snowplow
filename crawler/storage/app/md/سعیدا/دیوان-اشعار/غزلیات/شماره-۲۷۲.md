---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>رخ زرد عشقبازان چمن و بهار باشد</p></div>
<div class="m2"><p>دل پر ز داغ ایشان همه لاله زار باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفکن به عین دریا خود را و امن بنشین</p></div>
<div class="m2"><p>که بلای موج طوفان همه در کنار باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل غافل آن زمان سرد شود ز دار فانی</p></div>
<div class="m2"><p>سر خویش را ببیند که به پای دار باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خبری ز کوی جانان دهد آن کسی که دایم</p></div>
<div class="m2"><p>قطرات اشک خونین به رخش قطار باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جمال گل کسی فیض برد چو چشم نرگس</p></div>
<div class="m2"><p>که تمام دیده گردد همه انتظار باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن رقیب بدگو نه پسند خاطر اوست</p></div>
<div class="m2"><p>که به حرف بادآورده چه اعتبار باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حساب روز محشر نبود غمی سعیدا</p></div>
<div class="m2"><p>که ندیده هیچ کس هیچ که در شمار باشد</p></div></div>