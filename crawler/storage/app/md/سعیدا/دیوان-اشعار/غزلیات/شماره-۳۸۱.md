---
title: >-
    شمارهٔ ۳۸۱
---
# شمارهٔ ۳۸۱

<div class="b" id="bn1"><div class="m1"><p>اشکی که به گلشن چمن‌آرا نکند کس</p></div>
<div class="m2"><p>زنهار که از دیده تمنا نکند کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محکم نشود جذبهٔ قلاب محبت</p></div>
<div class="m2"><p>تا از ته دل در دل هم جا نکند کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردار ز رخ پرده چو خورشید که هرگز</p></div>
<div class="m2"><p>از سهو دگر یاد مسیحا نکند کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش نعمت عام تو زبان همه را بست</p></div>
<div class="m2"><p>تا از تو دگر شکوهٔ بی‌جا نکند کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر عاجز و بیچاره اگر رحم ثواب است</p></div>
<div class="m2"><p>پس رحم چرا بر تو سعیدا نکند کس؟</p></div></div>