---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>به غیر از دانهٔ دل ز آب چشمش سبزتر دارم</p></div>
<div class="m2"><p>نه هر تخمی که کردم زیر خاک امید بردارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا در راه تقدیر و قضا تسلیم باید بود</p></div>
<div class="m2"><p>که از او هر چه بینم دست بر بالای سر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراغ دل به غیر از موی او از کس نمی گیرم</p></div>
<div class="m2"><p>که ز این گم گشته،‌ زلف او خبر دارد خبر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم آیینهٔ حق است من عکس جمال او</p></div>
<div class="m2"><p>چو با من او نظر دارد منش با او نظر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعیدا از رخ خورشید مانندش نمی ترسم</p></div>
<div class="m2"><p>ز زلف و کاکل و چشم سیاه او حذر دارم</p></div></div>