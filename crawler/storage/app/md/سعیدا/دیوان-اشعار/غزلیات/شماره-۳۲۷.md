---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>دل ز بیداد غم خوبان مصفا می‌شود</p></div>
<div class="m2"><p>از غبار راه یوسف، کور بینا می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو زیبا می‌نماید دل به هر نوعی بری</p></div>
<div class="m2"><p>عشوه‌گر با ناز همراه است رعنا می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه من در گریهٔ خود دیده‌ام از روی تو</p></div>
<div class="m2"><p>گر نباشد در میان روی تو دریا می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدهزاران جان به قربان تو دارد انتظار</p></div>
<div class="m2"><p>بر سر کوی تو یک روزی تماشا می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار من وابسته چون توأم به کار مردم است</p></div>
<div class="m2"><p>کام شیرین می‌کنم از هرکه حلوا می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر خدنگی را که می‌اندازد آن ابروکمان</p></div>
<div class="m2"><p>با رقیبان است منجر باز با ما می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدعی امروز از ما گفتگو پوشیده است</p></div>
<div class="m2"><p>گر خدا قاضی است فردا این سخن وامی‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد هستی را سعیدا خاک دامن می‌کنم</p></div>
<div class="m2"><p>آنچه گم کردیم در این خانه پیدا می‌شود</p></div></div>