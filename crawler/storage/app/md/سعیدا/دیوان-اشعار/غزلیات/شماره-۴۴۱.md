---
title: >-
    شمارهٔ ۴۴۱
---
# شمارهٔ ۴۴۱

<div class="b" id="bn1"><div class="m1"><p>اهل عرفان را نباشد فکر سبز و زرد و آل</p></div>
<div class="m2"><p>هر چه افتد در کف دریا بود رزق حلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد کلفت روشنی بخش ضمیر عارف است</p></div>
<div class="m2"><p>خاکساران را غبار تن بود وجه کمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان را نشکند پرهیز جز دیدار دوست</p></div>
<div class="m2"><p>روزی آیینه موقوف است بر عکس جمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک دنیا کردگان را دل ز کلفت فارغ است</p></div>
<div class="m2"><p>در نمی یابد رخ آیینه را گرد ملال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش آن جمعی که قدر وصل را دانسته اند</p></div>
<div class="m2"><p>حرف در ترک دو عالم می رود نی ترک مال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رمز خاموشی نمی دانند غیر از خامشان</p></div>
<div class="m2"><p>کس نمی فهمد زبان لال را جز گوش لال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا برو ز اهل حقیقت باش یا ز اهل مجاز</p></div>
<div class="m2"><p>کی به یک جا راست می آید سعیدا حال و قال؟</p></div></div>