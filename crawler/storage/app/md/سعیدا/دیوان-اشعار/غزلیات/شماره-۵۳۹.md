---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>هر کس شکست طرهٔ پرپیچ و تاب او</p></div>
<div class="m2"><p>زلفش چو مار گشت و درآمد به خواب او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بوسه ای که دل طلبد ز آن دهان تنگ</p></div>
<div class="m2"><p>صد بحث می کند لب حاضر جواب او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برکند صبر لنگر سنگین خویش را</p></div>
<div class="m2"><p>دل برد چون سفینه، خرام جواب او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگذاشت طاقتی که کشد کس عنان آه</p></div>
<div class="m2"><p>همچون هلال جلوهٔ پا در رکاب او</p></div></div>