---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>که طرف از این فلک فتنه بار می بندد</p></div>
<div class="m2"><p>که یک گره چو گشاید هزار می بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوا به روی گل و لاله رنگ می ریزد</p></div>
<div class="m2"><p>بهار پای چمن را نگار می بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن شکوفهٔ دستار بر سر هر شاخ</p></div>
<div class="m2"><p>به دستیاری دست بهار می بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب ز زلف تو دارم که هر زمان صد چین</p></div>
<div class="m2"><p>چگونه در شب تاریک و تار می بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک اگرچه سعیدا به طور من گردد</p></div>
<div class="m2"><p>چه می گشاید از آن و چه کار می بندد</p></div></div>