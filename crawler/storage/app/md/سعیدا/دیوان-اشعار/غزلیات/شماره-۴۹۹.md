---
title: >-
    شمارهٔ ۴۹۹
---
# شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>من نه این آب روان از لب جو می بینم</p></div>
<div class="m2"><p>نفس اوست که من از دم او می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه در جام، جم از دور تماشا می کرد</p></div>
<div class="m2"><p>گاه در شیشه و خم گه به سبو می بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجودی که نه بیش است و نه کم از کم و بیش</p></div>
<div class="m2"><p>هر طرف می نگرم جلوهٔ او می بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توبه زلف و خط و [خالی] گرو و من به نگاه</p></div>
<div class="m2"><p>تو ز گل رنگ و رخ و من همه او می بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا پیرهن دوستی و مهر و وفاست</p></div>
<div class="m2"><p>چاک گردیده ز دست تو رفو می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر از خویشتن آن روز که برداشته ام</p></div>
<div class="m2"><p>شکر ایزد که سعیدا همه او می بینم</p></div></div>