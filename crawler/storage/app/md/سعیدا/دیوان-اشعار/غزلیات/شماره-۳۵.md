---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>رفتار محال است ز کوی تو کسی را</p></div>
<div class="m2"><p>نبود گذر از یک سر موی تو کسی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عکس تو عکسی است در آیینهٔ اوهام</p></div>
<div class="m2"><p>ور نه خبری نیست ز روی تو کسی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشکل که تو را بیند و از جا نرود کس</p></div>
<div class="m2"><p>چون یاد کند شیفته بوی تو کسی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر صفت از خویش برون رفتم و دیدم</p></div>
<div class="m2"><p>جز ذات تو ره نیست به سوی تو کسی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد و فغان کار سعیداست به کویت</p></div>
<div class="m2"><p>ورنه سخنی نیست ز خوی تو کسی را</p></div></div>