---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>بیجا نمی خلد به دلی تیر آه ما</p></div>
<div class="m2"><p>مژگان چشم آبله شد خار راه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عجز ما همیشه قوی رشک می برد</p></div>
<div class="m2"><p>با برق می زند دو برابر گیاه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک نکته ای است در نظر اهل معرفت</p></div>
<div class="m2"><p>از خط سرنوشت شکست کلاه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خشم چرخ و کشمکش دست روزگار</p></div>
<div class="m2"><p>شد چون کمان شکستگی ما پناه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جایی که چشم عفو تو مژگان به هم زند</p></div>
<div class="m2"><p>در دیدهٔ [صواب] نشیند گناه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خط و خال، زینت روی دو عالم است</p></div>
<div class="m2"><p>سودای مفلسانه و فقر سیاه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بوسه دست داده سعیدا به پای او</p></div>
<div class="m2"><p>ساییده است سر به ثریا کلاه ما</p></div></div>