---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>چشمم ز گریهٔ دل ناکام روشن است</p></div>
<div class="m2"><p>تا می چکد ز شیشه میم،‌ جام روشن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تربت گرفته دماغان هجر او</p></div>
<div class="m2"><p>دایم چراغ روغن بادام روشن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع حیات صبحدم از هر که شد فنا</p></div>
<div class="m2"><p>دیدم به جای او دگری شام روشن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر تا به پا لطافت معنی است قامتش</p></div>
<div class="m2"><p>آری که شعله را همه اندام روشن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اظهار سوز دل به زبان احتیاج نیست</p></div>
<div class="m2"><p>در خانه آتشی است که تا بام روشن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیضش به خاص و عام رسد هر کجا که هست</p></div>
<div class="m2"><p>آن را که همچو شمع سرانجام روشن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوبت رسد شبی به سعیدای بینوا</p></div>
<div class="m2"><p>امروز گرچه شمع نکونام روشن است</p></div></div>