---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>می از انگور شد میخانه از کیست؟</p></div>
<div class="m2"><p>بت از کافر بود بتخانه از کیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی از جا به هر آواز پایی</p></div>
<div class="m2"><p>اگر دانی که این افسانه از کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مجنون کی گریزد طفل وحشی</p></div>
<div class="m2"><p>اگر داند که این دیوانه از کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبیند چشم او ناآشنایی</p></div>
<div class="m2"><p>اگر داند کسی بیگانه از کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این مهمانسرا کس را مکن عیب</p></div>
<div class="m2"><p>گشا چشمی ببین کاشانه از کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقیقت را تماشا کن در این بزم</p></div>
<div class="m2"><p>مبین دست و ببین پیمانه از کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو خود را صاحب خرمن چه سازی</p></div>
<div class="m2"><p>تفکر کن که اصل دانه از کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این ماتم سرای عقل آباد</p></div>
<div class="m2"><p>بجز دل شیوهٔ مستانه از کیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را با صاحب کاشانه راهی است</p></div>
<div class="m2"><p>چه می پرسی سعیدا خانه از کیست؟</p></div></div>