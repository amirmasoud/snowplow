---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گداخت سیم وش آن شوخ سیمبر ما را</p></div>
<div class="m2"><p>به پیچ و تاب درآورد آن کمر ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو جام، گردش آن چشم پرخمار امروز</p></div>
<div class="m2"><p>ز حادثات جهان کرد بی خبر ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گردباد به خود ای نفس، چه می پیچی؟</p></div>
<div class="m2"><p>چو آب برد لب خشک و چشم تر ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه طالع است که هر گاه چون نگاه به غیر</p></div>
<div class="m2"><p>فکند تا نظر، افکند از نظر ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال بی هنری انتهای بی عیبی است</p></div>
<div class="m2"><p>که خلق، عیب نسازند جز هنر ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علاج غفلت ما را که می تواند کرد</p></div>
<div class="m2"><p>که مونس رگ خواب است نیشتر ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غوطه ها که به یک قطره خون دل نزدیم</p></div>
<div class="m2"><p>که تا گمان نکند غیر، بی جگر ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرارت دل بی تاب و آتش شوقش</p></div>
<div class="m2"><p>فکنده است چو خورشید در به در ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خط و خال گناه است حسن روی ثواب</p></div>
<div class="m2"><p>که نفع نیست ز سودای بی ضرر ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه زندگی است سعیدا که از نظر چون عمر</p></div>
<div class="m2"><p>گذشت یار و نیاورد در نظر ما را</p></div></div>