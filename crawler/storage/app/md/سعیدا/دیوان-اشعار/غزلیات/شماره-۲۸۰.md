---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>سیلی که در این راه گذر داشته باشد</p></div>
<div class="m2"><p>از خانهٔ دیوانه خبر داشته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزردگی هیچ دلی را نپسندد</p></div>
<div class="m2"><p>رحمی به دل خویش اگر داشته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درمان دلم هیچ مپرسید ز جانان</p></div>
<div class="m2"><p>فکری به دل خویش مگر داشته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل در بر من نیست ز دلدار بپرسید</p></div>
<div class="m2"><p>از گمشده شاید که خبر داشته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شخصی به تو بیند که چو آیینهٔ فولاد</p></div>
<div class="m2"><p>آهن جگر و سینه سپر داشته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بهله مزن دست مبادا که میانی</p></div>
<div class="m2"><p>آن بسته کمر زیر کمر داشته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیب فلک سفله مسازید که معلوم</p></div>
<div class="m2"><p>یک بی سر و پایی چه هنر داشته باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی می گذرد از سر قربان شدهٔ خود</p></div>
<div class="m2"><p>تا در نفس خویش اثر داشته باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنهار سعیدا که ز دشمن نهراسی</p></div>
<div class="m2"><p>تا چشم کرم با تو نظر داشته باشد</p></div></div>