---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>بجز از بخل در این دور زمان چیزی نیست</p></div>
<div class="m2"><p>جز حسد پیشهٔ این آدمیان چیزی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی نهال قد وسیب ذقن و نرگس چشم</p></div>
<div class="m2"><p>سیر باغ و چمن وآب روان چیزی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامتت خم شد و از ناوک دم هیچ نماند</p></div>
<div class="m2"><p>گوشه ای گیر که بی تیر، کمان چیزی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شود چشم دل از خواب گران وا دانی</p></div>
<div class="m2"><p>مال و جاه و حشم و گنج روان چیزی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اعتمادی نبود بر کرم و لطف جهان</p></div>
<div class="m2"><p>تکیه بر سایهٔ این سرو روان چیزی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کشی منت گردون که ورا در ترکش</p></div>
<div class="m2"><p>غیر تیر نفس سوختگان چیزی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رفیقان مطلب مرهم زخم دل ریش</p></div>
<div class="m2"><p>که در این طایفه جز لطف زبان چیزی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر همان چیز که داری تو سعیدا رغبت</p></div>
<div class="m2"><p>بیشتر از همه بنگر که همان چیزی نیست</p></div></div>