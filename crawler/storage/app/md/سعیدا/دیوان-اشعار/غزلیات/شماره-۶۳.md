---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>تو جان و من به جانت گشته طالب</p></div>
<div class="m2"><p>تو نور دیده و از دیده غایب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان قانون شرعت زد بر آهنگ</p></div>
<div class="m2"><p>که از می شد لب پیمانه تایب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان بس خورده با هم فرق نتوان</p></div>
<div class="m2"><p>که در این دوران غرایب از عجایب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست خویش در آتش فتادی</p></div>
<div class="m2"><p>که در معنی اقارب شد عقارب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن در کار او عیبی که پیداست</p></div>
<div class="m2"><p>سعیدا چون قلم در دست کاتب</p></div></div>