---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>گریه در بزم یار، بی ادبی است</p></div>
<div class="m2"><p>خنده هم ز این قرار، بی ادبی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اتحادم به امر مطلوب است</p></div>
<div class="m2"><p>ورنه بوس و کنار بی ادبی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستم و جور درد عشقش را</p></div>
<div class="m2"><p>باز با او شمار بی ادبی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهانی که افتقار سزاست</p></div>
<div class="m2"><p>از جهان افتخار بی ادبی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاکم امر و نهی چون حق است</p></div>
<div class="m2"><p>گله از روزگار بی ادبی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا پابرهنگان باشند</p></div>
<div class="m2"><p>سرکشی های خار بی ادبی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا آن نگار بی نقش است</p></div>
<div class="m2"><p>باز نقش و نگار بی ادبی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خیالی که آه نتوان کرد</p></div>
<div class="m2"><p>نالهٔ زار زار بی ادبی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشقی مفلسی و جانبازی است</p></div>
<div class="m2"><p>تن زنی در قمار بی ادبی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای سعیدا محبتی در دل</p></div>
<div class="m2"><p>غیر حب نگار بی ادبی است</p></div></div>