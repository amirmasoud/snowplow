---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>خط یاقوت لعلش تا نمود آن مصحف رو را</p></div>
<div class="m2"><p>نگاهش می کند تفسیر، بسم الله ابرو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پا می پیچدش سنبل پی بوسیدن پایش</p></div>
<div class="m2"><p>به هر وادی که آن بدخو پریشان می کند مو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان تیغ می بندد نگاه تیز جادویش</p></div>
<div class="m2"><p>که را دستی که گیرد نکته آن چشم سخنگو را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تنها دل سیاهی می کند بی او در این صحرا</p></div>
<div class="m2"><p>سیه کرد انتظار جلوهٔ او چشم آهو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعیدا جوی خون گردد روان از چشم حیرانم</p></div>
<div class="m2"><p>اگر یک دم نبینم در کنار آن سرو دلجو را</p></div></div>