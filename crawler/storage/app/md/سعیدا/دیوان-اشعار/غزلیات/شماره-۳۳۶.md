---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>هر که از خود فرود می آید</p></div>
<div class="m2"><p>آسمان در سجود می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایم از خانمان سوختگان</p></div>
<div class="m2"><p>نکهت داغ و دود می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دل بی قرار من دیر است</p></div>
<div class="m2"><p>یار هر چند زود می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دکان نظر، متاع دلم</p></div>
<div class="m2"><p>آنچه بود و نبود می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زمین گر برند نام تو را</p></div>
<div class="m2"><p>ز آسمان ها درود می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرز این و ناز اگر این است</p></div>
<div class="m2"><p>آنچه زو ... شنود می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ با نیک بد کند چه عجب</p></div>
<div class="m2"><p>زین بلای کبود می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم از خاک کشتگان غمت</p></div>
<div class="m2"><p>شورش زنده رود می آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای سعیدا [یکی] است چشمهٔ دل</p></div>
<div class="m2"><p>لیکن از وی [دو] رود می آید</p></div></div>