---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>از خس و خار حوادث قلب ما را کی صفاست</p></div>
<div class="m2"><p>عقل تا در خانهٔ ما پیشوا و کدخداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی فنا کی دیدهٔ باطن شود بینا به حق</p></div>
<div class="m2"><p>نیستی گردی است کان در چشم هستی توتیاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که در چشم خلایق شد سبک در راه عشق</p></div>
<div class="m2"><p>جذبهٔ مطلوب با او همچو کاه و کهرباست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسته ما به ز قند و گل نگردد ای طبیب</p></div>
<div class="m2"><p>زان شکر لب، حرف تلخی یاد اگر داری دواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برنمی گردد کسی محروم از این در تا ابد</p></div>
<div class="m2"><p>صاحب این خانه با بیگانگان هم آشناست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت دل گر عاشقی محنت سرا را در بکوب</p></div>
<div class="m2"><p>گفتمش طاقت ندارم گفت عشقت پس هواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکرها دارد سعیدا از خدا در کارها</p></div>
<div class="m2"><p>گرچه افتاده است دستش نارسا، طبعش رساست</p></div></div>