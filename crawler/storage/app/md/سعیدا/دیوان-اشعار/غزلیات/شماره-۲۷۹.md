---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>می تلخ و چمن پرگل، ساقی نمکین باشد</p></div>
<div class="m2"><p>آیا که چنین گفتم در روی زمین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا اهرمن نفست زنده است تو غمگینی</p></div>
<div class="m2"><p>گر ملک سلیمانت در زیر نگین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز فکر خط و خالش باور نکنم هرگز</p></div>
<div class="m2"><p>در کارگه عالم نقشی به از این باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایام بهار آمد گل خیمه به صحرا زد</p></div>
<div class="m2"><p>آرام مگر امروز در خانهٔ زین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بازوی پر زورت بر حسن دل افروزت</p></div>
<div class="m2"><p>مغرور مشو زنهار نی آن و نه این باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشوقت فقیری که صفا داشته باشد</p></div>
<div class="m2"><p>پوشیده ز خود ره به خدا داشته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس را گذر از سایهٔ کوی تو محال است</p></div>
<div class="m2"><p>گر بر سر خود بال هما داشته باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خاطر من جز غم او هیچ دگر نیست</p></div>
<div class="m2"><p>تا در دل خود یار چها داشته باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>معشوقهٔ خوبی است جهان لیک به شرطی</p></div>
<div class="m2"><p>کاین هرزهٔ هر پیشه وفا داشته باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خون کرده و سرمست روان است مه من</p></div>
<div class="m2"><p>تا باز دگر عزم کجا داشته باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگریزم از آن شهر که دارند خلایق</p></div>
<div class="m2"><p>دردی که نظر سوی دوا داشته باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پوشیده تواند رود از هر دو جهان چشم</p></div>
<div class="m2"><p>از جاذبه آن کس که عصا داشته باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون من نشود صبح بدین روز گرفتار</p></div>
<div class="m2"><p>در سینه اگر آه رسا داشته باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی برگی من تا به کمالی است که بالله</p></div>
<div class="m2"><p>بیزارم از آن نی که نوا داشته باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکدل شود از هر دو جهان آن که چو خورشید</p></div>
<div class="m2"><p>در دست تهی زلف دو تا داشته باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جز سیر جمال تو سعیدا نکند هیچ</p></div>
<div class="m2"><p>تا در دل و در دیده ضیا داشته باشد</p></div></div>