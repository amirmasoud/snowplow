---
title: >-
    شمارهٔ ۵۵۷
---
# شمارهٔ ۵۵۷

<div class="b" id="bn1"><div class="m1"><p>عهد با بالابلندی بسته ای</p></div>
<div class="m2"><p>دل به شوخی خودپسندی بسته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده ای زین ابلقت را چون فلک</p></div>
<div class="m2"><p>آفتابی بر سمندی بسته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنگ شکر را گشودی از لبت</p></div>
<div class="m2"><p>بر سر هر حرف، قندی بسته ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانمودی خویش را بیچون به خلق</p></div>
<div class="m2"><p>خلق را با چون و چندی بسته ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده ای جا خال را در خاطرم</p></div>
<div class="m2"><p>در دل آتش سپندی بسته ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوش داروها در آن لب هست و لب</p></div>
<div class="m2"><p>از برای دردمندی بسته ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده ای جا در خم ابروی یار</p></div>
<div class="m2"><p>خانه بر طاق بلندی بسته ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داد از زلف فلک پیمای تو</p></div>
<div class="m2"><p>عالمی را در کمندی بسته ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوحدی خوش گفته ای بر روی یار</p></div>
<div class="m2"><p>«بر گل از عنبر کمندی بسته ای»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای سعیدا عمر می خواهی و دل</p></div>
<div class="m2"><p>بر دم اسب دوندی بسته ای</p></div></div>