---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>چه هشیار است در انداز چشم می پرست او</p></div>
<div class="m2"><p>که هرگز یک دلی بیرون نمی آید ز [شست] او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران حلقه باید در خم زلف پریرویان</p></div>
<div class="m2"><p>که تا یک دل تواند صید کردن چشم مست او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم را گوی بازی کرده اید و راضیم از جان</p></div>
<div class="m2"><p>ولیکن باخبر ای ماهرویان از شکست او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه گویم از خم زلف و قد موزون دلجویش</p></div>
<div class="m2"><p>که هرگز کی توان کردن به عمری شرح بست او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یقین معذور خواهد بود از تکلیف در عالم</p></div>
<div class="m2"><p>که تا محشر نمی آید به خود مست الست او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن جان را خوش آید ناوک مژگان که هر ساعت</p></div>
<div class="m2"><p>دل ما را خطا تا گردن و تا پر نشست او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعیدا مهر مهرویی به دل دارد که از خجلت</p></div>
<div class="m2"><p>ید بیضا دگر بیرون نمی آید ز دست او</p></div></div>