---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>هر که غافل یک نفس شد صاحب دم کی شود؟</p></div>
<div class="m2"><p>کج گذارد آن که پا در راه، اقدم کی شود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی به حرف آید اگر داند معلم در قفاست</p></div>
<div class="m2"><p>طوطی آگه چون بود ز آیینه همدم کی شود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کعبه را بگذار و طوف خانهٔ خمار کن</p></div>
<div class="m2"><p>آنچه از خم می شود پیدا ز زمزم کی شود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سینهٔ ما بی خراش دل نمی یابد صفا</p></div>
<div class="m2"><p>زخم ما بی ریزهٔ الماس مرهم کی شود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چو بد با بد بدی نیکی نخواهی دید از او</p></div>
<div class="m2"><p>جز به احسان و مروت خصم ملزم کی شود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوی از میدان سعیدا نامداران برده اند</p></div>
<div class="m2"><p>جان اگر بخشد کسی بر مرده حاتم کی شود؟</p></div></div>