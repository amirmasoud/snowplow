---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>به بستن کمرش نیست آن میان باعث</p></div>
<div class="m2"><p>که فکر ما شده چون موی در میان باعث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آن که هر چه به ما کرده از تو دانستیم</p></div>
<div class="m2"><p>چرا که غیر تو کس نیست در میان باعث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثر به نالهٔ چنگ از قد خمیده اوست</p></div>
<div class="m2"><p>که تیر را به پریدن بود کمان باعث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشیدن این همه منت ز خار و خس بلبل</p></div>
<div class="m2"><p>سبب نظارهٔ گل بود آشیان باعث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظهور مبدأ هر گل که بود دانستیم</p></div>
<div class="m2"><p>سبب نزول تو بودی و باغبان باعث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی به رزق سعیدا چرا غمین باشد</p></div>
<div class="m2"><p>که شد زمین سبب او را و آسمان باعث</p></div></div>