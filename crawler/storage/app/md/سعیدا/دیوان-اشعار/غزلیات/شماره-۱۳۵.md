---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>عالم ز دستگاه کمالش نمونه ای است</p></div>
<div class="m2"><p>عقبی ز گفتگوی وصالش نمونه ای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه از صفای چهرهٔ او فیض دیده است</p></div>
<div class="m2"><p>خورشید از شعاع جمالش نمونه ای است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دوست حرف و صوت کجا کس شنیده است؟</p></div>
<div class="m2"><p>قرآن ز لطف معنی قالش نمونه ای است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنت ز حسن خلق خدا آفریده ای است</p></div>
<div class="m2"><p>دوزخ ز های و هوی جلالش نمونه ای است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با مدعی بگوی که آب حرام ما</p></div>
<div class="m2"><p>بی ریب و شک ز نان حلالش نمونه ای است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسم جنون طریق سعیدای بینواست</p></div>
<div class="m2"><p>دیوانگی ز شورش حالش نمونه ای است</p></div></div>