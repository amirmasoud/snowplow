---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>آن [چشم] دل سیه که [زمامم] گرفته است</p></div>
<div class="m2"><p>از دست اختیار، ‌عنانم گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من تیغ نیستم که به چرخم فتاده کار</p></div>
<div class="m2"><p>پس از چه رو فلک به فسانم گرفته است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد گرد راه توسن دل، بیستون دل</p></div>
<div class="m2"><p>حق نگاه سرمه فشانم گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالله من نه نیکم و نی ز اهل دانشم</p></div>
<div class="m2"><p>بیهوده آسمان به [گمانم] گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در روز بازخواست کجا می کند قبول</p></div>
<div class="m2"><p>ترک ستمگر آنچه نهانم گرفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بربسته است کثرت خمیازه راه حرف</p></div>
<div class="m2"><p>دست خمار باده، دهانم گرفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محبوب زیر بال و پر و طوق بندگی</p></div>
<div class="m2"><p>حقا که طرز فاخته جانم گرفته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شادی کناره گیر که در بزم روزگار</p></div>
<div class="m2"><p>غم با دو دست خود ز میانم گرفته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افلاک باز بی سر و پا چرخ می زند</p></div>
<div class="m2"><p>آه دل کدام ندانم گرفته است؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلطان غم نگر که سعیدا به زور فکر</p></div>
<div class="m2"><p>اقبال [و] بخت و تخت روانم گرفته است</p></div></div>