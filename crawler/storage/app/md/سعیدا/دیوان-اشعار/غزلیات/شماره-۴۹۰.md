---
title: >-
    شمارهٔ ۴۹۰
---
# شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>باز حرف از ناز خوبان می زنم</p></div>
<div class="m2"><p>طعنه ها بر کفر و ایمان می زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو زلف از مصحف روی بتان</p></div>
<div class="m2"><p>بعد از این فال پریشان می زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کنم جان را بر آن لب پیشکش</p></div>
<div class="m2"><p>سنگ بر لعل بدخشان می زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم آنگاره لیک انگاره را</p></div>
<div class="m2"><p>تا شوم هموار، سوهان می زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در و دیوار از دستم شکست</p></div>
<div class="m2"><p>بعد از این سر در بیابان می زنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشه ام چون ابر گر پر می شود</p></div>
<div class="m2"><p>خیمه در صحن گلستان می زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشور جانان سعیدا رو نمود</p></div>
<div class="m2"><p>پشت پا بر عالم جان می زنم</p></div></div>