---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>گفتگوی حشر راحت از دل دیوانه ریخت</p></div>
<div class="m2"><p>چشم ما خواب گرانی داشت این افسانه ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون بلبل را به جوش آورد گل را رنگ داد</p></div>
<div class="m2"><p>در چمن هر قطره صهبایی که از پیمانه ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق دل ها را فشرد و چشم ها را آب داد</p></div>
<div class="m2"><p>گرچه از خم برد ساقی باده در پیمانه ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل، راه خانهٔ ما را نخواهد یافت حیف</p></div>
<div class="m2"><p>پیشتر از او در و دیوار این ویرانه ریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر دامی که زاهد از ردایش کرده پهن</p></div>
<div class="m2"><p>در فریب [و] گول مرغان سبحهٔ صد دانه ریخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ره هوش و دل و جان ای سعیدا گوش دار</p></div>
<div class="m2"><p>کاین غزل از خامهٔ صائب عجب مستانه ریخت</p></div></div>