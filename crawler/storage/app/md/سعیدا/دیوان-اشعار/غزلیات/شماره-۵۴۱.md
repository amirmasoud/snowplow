---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>به بحر شور زده غوطه آب نیل از تو</p></div>
<div class="m2"><p>به چاه رفته فرو یوسف ذلیل از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد لعل تو می در بهشت می جوشد</p></div>
<div class="m2"><p>گرفته لذت کافور سلسبیل از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم پیرهن یار بس بود ما را</p></div>
<div class="m2"><p>جمال یوسف کنعان و مصر و نیل از تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس است روز قیامت اگر به وزن آید</p></div>
<div class="m2"><p>گناه و لطف کثیر از من و قلیل از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گناه و جرم بود مظهر ظهور کریم</p></div>
<div class="m2"><p>بهشت و حور سعیدا به این دلیل از تو</p></div></div>