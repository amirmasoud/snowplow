---
title: >-
    شمارهٔ ۵۵۵
---
# شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>ابروی تو را تا قلم صنع کشیده</p></div>
<div class="m2"><p>حیرت ز مه نو سر انگشت گزیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطی که نمایان شده بر گرد رخ او</p></div>
<div class="m2"><p>صادق نفسی سورهٔ اخلاص دمیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان طرفه همایی است که بازش نتوان دید</p></div>
<div class="m2"><p>این باز ز هر شاخ درختی که پریده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راهی است ره عشق که چون ما و تو بسیار</p></div>
<div class="m2"><p>تا بوده نفس رفته به جایی نرسیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیکاری ما به ز همه کار جهان است</p></div>
<div class="m2"><p>چون عاقبت کار که دیدیم که دیده؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز نبریدی ز گنه رشتهٔ کردار</p></div>
<div class="m2"><p>ناف تو مگر دایه به این کار بریده؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل فکر گنه دارد و لب توبه سعیدا</p></div>
<div class="m2"><p>کس مثل تو بی عار ندیده نشنیده</p></div></div>