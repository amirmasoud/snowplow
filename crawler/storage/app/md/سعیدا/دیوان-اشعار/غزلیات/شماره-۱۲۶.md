---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>غم دماغم را پریشان کرده است</p></div>
<div class="m2"><p>ناله جسمم را نیستان کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گدایی را که چون از خود گذشت</p></div>
<div class="m2"><p>فقر را نازم که سلطان کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که آسان دید دور چرخ را</p></div>
<div class="m2"><p>مشکلی را بر خود آسان کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می تواند جسم ما را جان کند</p></div>
<div class="m2"><p>جان خود را آن که جانان کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معنی آدم ز هر نقشی مخواه</p></div>
<div class="m2"><p>صورتش را گرچه انسان کرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایه اش بس مهربان است و لطیف</p></div>
<div class="m2"><p>طفل ما خود را گریزان کرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تبسم دوش در بازار عشق</p></div>
<div class="m2"><p>دلبر ما شکر ارزان کرده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کعبهٔ دل را نمی داند چه سود؟</p></div>
<div class="m2"><p>شیخ گو قطع بیابان کرده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر به جیب فکر، صوفی زان برد</p></div>
<div class="m2"><p>سیر جنت در گریبان کرده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به جوش آمد سعیدا بحر دل</p></div>
<div class="m2"><p>نطق ما را گوهرافشان کرده است</p></div></div>