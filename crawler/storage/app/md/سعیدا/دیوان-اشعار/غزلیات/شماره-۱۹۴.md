---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>خوشم به این مرض و انحراف طبع و مزاج</p></div>
<div class="m2"><p>که جز تو کس نتواند مرا دوا و علاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا به مذهب عشاق فارغ از همه شو</p></div>
<div class="m2"><p>که در ولایت ما نیست دین کفر رواج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی است دنیی و عقبی چو نیک درنگری</p></div>
<div class="m2"><p>اگرچه عکس به گفتن بود مجاز مزاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرید جام شو ای دل که عبد مؤمن را</p></div>
<div class="m2"><p>فتادگی فلک و بیخودی بود معراج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک به مهرهٔ من نرد مهر کج بازد</p></div>
<div class="m2"><p>شود اگرچه مرا استخوان تن چون عاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تویی که سبز کنی خشک و خشک سبز کنی</p></div>
<div class="m2"><p>ز زنده باج ستانی دهی به مرده خراج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فیض عشق از آن روز شعر می بافم</p></div>
<div class="m2"><p>که تا شدم ز مریدان خواجهٔ نساج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو آن شهی که بود عار و ننگ ذات تو را</p></div>
<div class="m2"><p>مدد ز عسکر و عزت ز تخت و خیمه و تاج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به غیر سینهٔ عشاق تیر نازش را</p></div>
<div class="m2"><p>که راست زهره که تا دل کند بر آن آماج؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میا برون به تماشای عالم ای درویش</p></div>
<div class="m2"><p>که می برد ز ره این بت به زور استدراج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه ساحری است که دارد نگاه چشم سیاه</p></div>
<div class="m2"><p>همیشه خانهٔ دربسته می کند تاراج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمود هر دو جهان غیر یک حقیقت نیست</p></div>
<div class="m2"><p>یکی است بحر ولی مختلف بود امواج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگرچه نیست مرا پنبه دانه ای در کف</p></div>
<div class="m2"><p>ولیک دست نهادم به دامن حلاج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مکن مصاحب اهل حرص و آز [سعید]</p></div>
<div class="m2"><p>که آبرو برد از مرد، صحبت ازواج</p></div></div>