---
title: >-
    شمارهٔ ۴۸۳
---
# شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>کی جفای می و معشوقه و مهتاب کشم</p></div>
<div class="m2"><p>من که خون جگر خود چو می ناب کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظرم تشنهٔ ابر کرم کس نشود</p></div>
<div class="m2"><p>من که از چشمهٔ خورشید به چشم آب کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی نیازم کند از هر دو جهان در آغوش</p></div>
<div class="m2"><p>گر [شبی] قامت موزون تو در خواب کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منعم از گریه مکن ناصح و بگذار که من</p></div>
<div class="m2"><p>دلق آلودهٔ خود را به کفن آب کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دمم چشم سیاه تو ترحم نکند</p></div>
<div class="m2"><p>خویش را گر به دم خنجر قصاب کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پهلویم درد کند بسکه ضعیف است وجود</p></div>
<div class="m2"><p>به هوس آه چو بر بستر سنجاب کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرود لذت آغوش تو از یاد مرا</p></div>
<div class="m2"><p>هر زمان دست به خمیازهٔ دریاب کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خدا حلقه به گوش در خود ساز مرا</p></div>
<div class="m2"><p>چند چون حلقهٔ در گوش به هر باب کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیش از این رشتهٔ جان تاب ندارد دیگر</p></div>
<div class="m2"><p>تا به کی من ستم دست رسن تاب کشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردبادم نرسانید به جا خاک مرا</p></div>
<div class="m2"><p>بهتر آن است که من رخت به گرداب کشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه یارم همه جا هست در آغوش مرا</p></div>
<div class="m2"><p>بهتر آن است که در گوشهٔ محراب کشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخت هستی به در آورد سعیدا به امید</p></div>
<div class="m2"><p>که از این بحر مگر گوهر نایاب کشم</p></div></div>