---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>مگذار در حریم خرابات، پای بحث</p></div>
<div class="m2"><p>[جایی] که جام باده بود نیست جای بحث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه گشته ایم ز معنی به چشم خلق</p></div>
<div class="m2"><p>هرگز نکرده ایم زبان آشنای بحث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واعظ به زور شانه محاسن دراز کرد</p></div>
<div class="m2"><p>افکنده است تا سر زانو ردای بحث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سررشتهٔ سلوک به دست آر تا به جهد</p></div>
<div class="m2"><p>با سوزن جواب بدوزی قبای بحث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدگونه بحث هست سعیدا جواب کو</p></div>
<div class="m2"><p>زینهار با کسی نکنی ماجرای بحث</p></div></div>