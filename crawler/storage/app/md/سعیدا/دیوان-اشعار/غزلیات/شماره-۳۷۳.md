---
title: >-
    شمارهٔ ۳۷۳
---
# شمارهٔ ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>رسید نامهٔ عنبرفشان مشک آمیز</p></div>
<div class="m2"><p>به این فقیر دعاگوی بی کس و بی چیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گل گشودم و همچون صبا ز خود رفتم</p></div>
<div class="m2"><p>به آن امید که بینم مگر جمال تو نیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسید دام خط و دانه های نقطه در او</p></div>
<div class="m2"><p>چو زلف و خال محبت فزا و مهرانگیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کاغذ و چه مرکب که خط مهر و وفا</p></div>
<div class="m2"><p>به روی روز، رقم کرده خامهٔ شبدیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا به منع کسان دل ز راه او گردد</p></div>
<div class="m2"><p>که چشم مست ز پرهیز می کند پرهیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیدوار چنانم که جام و ساغر تو</p></div>
<div class="m2"><p>همیشه از می صدق و صفا بود لبریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بسکه واله و حیران کارهای توام</p></div>
<div class="m2"><p>ز شخص و عکس ندارم در آبگینه تمیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امید هست مرا از خدای دوست نواز</p></div>
<div class="m2"><p>که دشمنان تو گردند بر هوا ناچیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مراد دل به تحمل برآید از لب یار</p></div>
<div class="m2"><p>که صبر، لعل کند سنگ را نه آتش تیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو نیست یوسف کنعان به کام عاشق زار</p></div>
<div class="m2"><p>بیا بگو که زلیخا چه سود عمر عزیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به یاد وصل تو تنها نه من ز خود رفتم</p></div>
<div class="m2"><p>خیال رفته و دل رفته و سعیدا نیز</p></div></div>