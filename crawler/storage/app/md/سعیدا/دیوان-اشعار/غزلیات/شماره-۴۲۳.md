---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>گوشه ای نیست که آن جا نرسد پای فراق</p></div>
<div class="m2"><p>منزلی نیست که خالی نبود جای فراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساحل وصل کجا روی نماید در خواب</p></div>
<div class="m2"><p>کشتی هر که بیفتاد به دریای فراق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هراسم ز کف پای خیالش که بسی است</p></div>
<div class="m2"><p>حجر تفرقه در دامن صحرای فراق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز امتحان سامعه از پنبهٔ غفلت پر کرد</p></div>
<div class="m2"><p>گوش عالم نشد آسوده ز غوغای فراق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب دور دو آهنگ به هم راست نکرد</p></div>
<div class="m2"><p>نغمهٔ غیر مخالف نشد از نای فراق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ در محکمهٔ شرع قضا صاف نشد</p></div>
<div class="m2"><p>از محبان سر کوی تو دعوای فراق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا شنیدم که به هر عسر سعیدا یسری است</p></div>
<div class="m2"><p>از دل و جان شده ام طالب و جویای فراق</p></div></div>