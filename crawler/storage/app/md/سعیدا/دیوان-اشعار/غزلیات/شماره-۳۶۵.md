---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>خوابیده مست و بیخود امروز در بر ناز</p></div>
<div class="m2"><p>صد منت عظیم است امروز بر سر ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مهر، نازبالش در زیر سر نهاده</p></div>
<div class="m2"><p>چون گل به زیر پهلو افکنده [بستر] ناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسم تو از لطافت با ناز ناز می کرد</p></div>
<div class="m2"><p>آن دم که آفریدند از ناز پیکر ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز آن پریرخ بسیار نازنین است</p></div>
<div class="m2"><p>تا خود چه لعبت آرد از ناز بر سر ناز</p></div></div>