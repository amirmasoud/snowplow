---
title: >-
    شمارهٔ ۴۱۴
---
# شمارهٔ ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>خویش را یکبارگی تا سختم از دل وداع</p></div>
<div class="m2"><p>از میان ما و او برخاست رسم انقطاع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>[ذره ای] تا مهر دنیا هست یاران را به دل</p></div>
<div class="m2"><p>دایماً در کار خواهد بود آیین نزاع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس خنک بربسته زاهد بر سرش عمامه را</p></div>
<div class="m2"><p>زان برودت دایماً از نزله اش باشد صداع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جرم ما و رحمت حق هر دو خواب افتاده است</p></div>
<div class="m2"><p>تیرگی از ابر باشد خوشنما از مه شعاع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک سر مو فعل بی‌جا هست نقصان کمال</p></div>
<div class="m2"><p>جامه کوتاه است کم باشد چو اطلس [یک ذراع]</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشقبازی را سعیدا از هوسناکان مجو</p></div>
<div class="m2"><p>سالک این راه ساکن باید و مرد شجاع</p></div></div>