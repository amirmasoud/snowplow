---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>دیدهٔ غفلت گشا دریاب فیض نور صبح</p></div>
<div class="m2"><p>می شوی خورشید عالم چون شوی منظور صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خفته در آغوش مطلوبی ز رویش بی نصیب</p></div>
<div class="m2"><p>گشته ای از تیره بختی همچو شب مهجور صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاک می سازد کسی را از هوس موی سفید</p></div>
<div class="m2"><p>مشک شب بر هم خورد هر گه دمد کافور صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جوانی خدمت پیری گزین چون شب شود</p></div>
<div class="m2"><p>کس نمی آید برون از خانه بی دستور صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست تأثیری عجب صافی ضمیران را به دم</p></div>
<div class="m2"><p>چون شبی را روز می سازد دم مأمور صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود ضعف پیری بر جوانی غالب است</p></div>
<div class="m2"><p>نیست شب را طاقت سرپنجه ای با زور صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه از عصمت محل سجده گاه خلق شد</p></div>
<div class="m2"><p>شد خراب از بی حجابی خانهٔ معمور صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پاس وقت سینه صافان بر سعیدا واجب است</p></div>
<div class="m2"><p>شمع می گرید برای خاطر مسکور صبح</p></div></div>