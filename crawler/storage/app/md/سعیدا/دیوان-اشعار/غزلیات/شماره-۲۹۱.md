---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>خارخار دل کجا در دیدهٔ ما می‌خلد</p></div>
<div class="m2"><p>خار ماهی کی به چشم موج دریا می‌خلد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستگاه صنع را انگشت رد هم نیست، رو</p></div>
<div class="m2"><p>خار چون از دست افتد باز در پا می‌خلد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که یاران ملایم‌طبع سنگین‌طینتند</p></div>
<div class="m2"><p>رشتهٔ گوهر به چشم سوزن ما می‌خلد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل مفلس نباشد آرزوی هیچ چیز</p></div>
<div class="m2"><p>خارخار اکثر به جان اهل دنیا می‌خلد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ملایم ظاهران زنهار در اندیشه باش</p></div>
<div class="m2"><p>پنبه دارد نرمی و در چشم مینا می‌خلد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتگوی مدعی در بزم ما کردن خطاست</p></div>
<div class="m2"><p>حرف بدگو هم سعیدا بد به دل‌ها می‌خلد</p></div></div>