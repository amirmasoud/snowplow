---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>حرف هر کس ز فکر خام خود است</p></div>
<div class="m2"><p>جنبش هر که از مقام خود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان با وجود این همه شأن</p></div>
<div class="m2"><p>متفکر به صبح و شام خود است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده ام شیخ و پیر و ترسا را</p></div>
<div class="m2"><p>هر که در فکر کام و جام خود است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واعظی را که زیر پا کرسی است</p></div>
<div class="m2"><p>عاشق صنعت کلام خود است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که بر منبرش تو می بینی</p></div>
<div class="m2"><p>پی سیر جهان به بام خود است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافلانش نماز می خوانند</p></div>
<div class="m2"><p>آن که در قعده و قیام خود است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راستی کم ز مد کاف مباش</p></div>
<div class="m2"><p>دایما ً بر سر کلام خود است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجهٔ خویش دان سعیدا را</p></div>
<div class="m2"><p>نه چو خربندگان غلام خود است</p></div></div>