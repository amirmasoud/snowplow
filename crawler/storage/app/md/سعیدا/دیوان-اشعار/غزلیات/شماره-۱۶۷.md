---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>منظور من جز او چه بود در نظر که نیست</p></div>
<div class="m2"><p>از حال دل چه شکوه کنم بی خبر که نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرف از دهان او چه زنم راه حرف کو</p></div>
<div class="m2"><p>بر آن میان چه دست توان زد کمر که نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاده ام چو مرغ کبابی در این دیار</p></div>
<div class="m2"><p>عزم کدام شهر کنم بال و پر که نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادی روا نداشت ولی چون کند فلک</p></div>
<div class="m2"><p>در کارخانه اش غم از این بیشتر که نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک تیر آه خسته دلان کارگر نشد</p></div>
<div class="m2"><p>بر جان کاسه پشت فلک بی سپر که نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردون کجا قبول کند حرف مفلسان</p></div>
<div class="m2"><p>در خانهٔ بخیل گدا معتبر که نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیرین کند کلام سعیدا مذاق دل</p></div>
<div class="m2"><p>چون خامهٔ نی قلمش بی شکر که نیست</p></div></div>