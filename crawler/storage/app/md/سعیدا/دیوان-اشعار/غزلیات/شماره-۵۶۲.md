---
title: >-
    شمارهٔ ۵۶۲
---
# شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>وجودم را بر آتش زد لقای گرم موجودی</p></div>
<div class="m2"><p>شدم کوه تجلی از نگاه سرمه آلودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه ما بود و نه من بودم نه تن بود و نه جان بودم</p></div>
<div class="m2"><p>نه خاکی بود و نه آبی نه آتش بود و نی دودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن جامع که من بودم نه مسجد بود و نه منبر</p></div>
<div class="m2"><p>نه دیری بود و دیاری نه ساجد بود و مسجودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن دم عشق می بازم که در عشقش نهان بودم</p></div>
<div class="m2"><p>نه عاشق بود و نی عارف نه معروفی نه موجودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشا آن نشئهٔ سابق که در میخانهٔ وحدت</p></div>
<div class="m2"><p>نه می بود و نه انگوری نه مقبولی نه مردودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبایع بست چون صورت، تولد کرد خواهش ها</p></div>
<div class="m2"><p>فراوانی و ارزانی نه نقصان بود و نه سودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن بحری که من بودم نه گوهر بود و نه ماهی</p></div>
<div class="m2"><p>نه دریا بود و نی کشتی نه وارد بود و مورودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گذشت از حد سعیدا انتظار جلوهٔ آن قد</p></div>
<div class="m2"><p>قیامت می شود آخر ولیکن ای خدا زودی</p></div></div>