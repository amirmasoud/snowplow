---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>هر کجا مینا و جامی از می گلگون پر است</p></div>
<div class="m2"><p>وای بر پیمانه و مینای ما کز خون پر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیشهٔ ما را شکستی خوب کردی پر نبود</p></div>
<div class="m2"><p>خاطر ما را نگه داری ستمگر، چون پر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الفتی دارد به ما اندوه از روز الست</p></div>
<div class="m2"><p>گر درون خالی شد از غم، نیست غم، بیرون پر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه دل ها آب گشت از دست چرخ بی وفا</p></div>
<div class="m2"><p>از شراب ناب [انده] شیشهٔ گردون پر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر موزون کردن شعرم سعیدا می برد</p></div>
<div class="m2"><p>هر زمان دل، ورنه جیب سینه از مضمون پر است</p></div></div>