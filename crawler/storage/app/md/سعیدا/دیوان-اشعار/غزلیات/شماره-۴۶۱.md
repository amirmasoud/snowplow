---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>آه از آن روزی که چون یوسف نگاری داشتم</p></div>
<div class="m2"><p>در نظرهای عزیزان اعتباری داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد ایامی که در این کلفت آباد جهان</p></div>
<div class="m2"><p>همچو مینا و صراحی غمگساری داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وای از آن روزی که پایم می فشرد انگور را</p></div>
<div class="m2"><p>در خرابات مغان دست به کاری داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوخت دل یکباره ور نی از گل روی کسی</p></div>
<div class="m2"><p>پیشتر در سینه من هم خارخاری داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد دستی های من یکبارگی افشاند و رفت</p></div>
<div class="m2"><p>در دل و در سینه هر گرد و غباری داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه در غربت ز تنهایی صفاها کرده ام</p></div>
<div class="m2"><p>نیست در خاطر که من یار و دیاری داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم گردونم به خاک افکند ورنه بیش از این</p></div>
<div class="m2"><p>سینه ای چون ابر و چشم اشکباری داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمدم تا وادی خود دشمنان ره یافتند</p></div>
<div class="m2"><p>وای چون من بیخودی بر خود حصاری داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پاسبان بودم سعیدا با سگان کوی یار</p></div>
<div class="m2"><p>ای خوش آن روزی که آن جا اقتداری داشتم</p></div></div>