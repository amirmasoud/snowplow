---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>گلرخانی که سراسر رو گلزار خودند</p></div>
<div class="m2"><p>دایماً در عرق از گرمی بازار خودند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل همت ننشینند گران در دل کس</p></div>
<div class="m2"><p>این گُرُه تا رمقی هست به تن بار خودند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه چشمان تو مستند ولی هشیارند</p></div>
<div class="m2"><p>دایماً باخبر از مردم بیمار خودند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه ابنای زمان کینهٔ هم می ورزند</p></div>
<div class="m2"><p>تا رسد با دگری، در پی آزار خودند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خوش آن گوشه نشینان که به صد ناز و نیاز</p></div>
<div class="m2"><p>در حریم خود و در سایهٔ دیوار خودند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن گروهی که به کف آینهٔ جان دارند</p></div>
<div class="m2"><p>کی به فکر خود و آرایش دیدار خودند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسب جمعیت از آن قوم سعیدا می کن</p></div>
<div class="m2"><p>که پریشان شدهٔ نطق گهربار خودند</p></div></div>