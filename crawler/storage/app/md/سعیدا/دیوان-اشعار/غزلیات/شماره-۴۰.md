---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>برتر از عرش است ای عشاق، سیران شما</p></div>
<div class="m2"><p>دور گردون هست تقلیدی ز دوران شما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا قیامت باد با هم روبه‌رو ای مهوشان</p></div>
<div class="m2"><p>سینهٔ صافی‌دلان و تیغ عریان شما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نه‌اید ای دوستان از اهل عزت خود چراست</p></div>
<div class="m2"><p>آسمان کرسی به دوش و سفره [و] خوان شما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند می‌پرسی که مجنون کرد و عقلت را که برد</p></div>
<div class="m2"><p>مستی چشم سیه سرو خرامان شما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبحه و زنار یک روزی به هم خواهند زد</p></div>
<div class="m2"><p>می‌شود معلوم کفر ما و ایمان شما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوسیرت ای ملایک صورتان ملک روم</p></div>
<div class="m2"><p>خنده می‌آید مرا بر چشم گریان شما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتاب حسن بی‌اندازه گرمی می‌کند</p></div>
<div class="m2"><p>می‌مکد یاقوت تر لعل بدخشان شما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنجهٔ خورشید و صبح مطلع شمس یقین</p></div>
<div class="m2"><p>نیست ای خوبان به جز دست و گریبان شما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنگ‌ها مخفی است در صلح شما ای دوستان</p></div>
<div class="m2"><p>برتر از درد است بر این خسته درمان شما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رونق بازار خوبان چهره‌های گندمی است</p></div>
<div class="m2"><p>صدهزاران جان به یک جو پیش دکان شما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>[سرفه‌ای] در کشتن ما ای جوانان خوب نیست</p></div>
<div class="m2"><p>صدهزاران چون سعیدا باد قربان شما</p></div></div>