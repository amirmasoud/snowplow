---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>چون تیر پیچ و تاب ندارد کمان ما</p></div>
<div class="m2"><p>پاک است از ریا و حسد خاندان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر از هدف شدن نزند فال دیگری</p></div>
<div class="m2"><p>گر مهره های قرعه شود استخوان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خاطرش سخن سر مویی اثر نکرد</p></div>
<div class="m2"><p>هر چند مو کشید زبان در دهان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جایی که غیر عجز دگر هیچ نیست کم</p></div>
<div class="m2"><p>جز بار نیستی چه برد کاروان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی سیاه گشت سفید از فراق یار</p></div>
<div class="m2"><p>شد زغفران ز محنت غم ارغوان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در انتظار شعلهٔ آواز خشک شد</p></div>
<div class="m2"><p>ای عندلیب خار و خس آشیان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوش فلک چو دیدهٔ حیران نرگس است</p></div>
<div class="m2"><p>در انتظار تا شنود داستان ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منت کش بهار و خزان نیست باغ دل</p></div>
<div class="m2"><p>داغ است لاله از هوس بوستان ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ذره ای ز نور حقیقت به ما رسد</p></div>
<div class="m2"><p>خورشید همچو سایه رود در عنان ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در نیک و بد حقیقت ما می کند ظهور</p></div>
<div class="m2"><p>در مسجد و کنشت بود داستان ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اخوان صفات گرگ سعیدا گرفته اند</p></div>
<div class="m2"><p>گویا که یوسفی است در این کاروان ما</p></div></div>