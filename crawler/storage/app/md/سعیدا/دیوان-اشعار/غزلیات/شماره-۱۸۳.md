---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>دل ز تکرار بهشت حور از کوثر گرفت</p></div>
<div class="m2"><p>آن قدر سودم به صندل سر، که درد سر گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرنوشتم بود دیدم کفر غالب شد به دین</p></div>
<div class="m2"><p>حسن خط، حسن جمال یار را دربر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کثرت عاشق به بالا برد کار حسن را</p></div>
<div class="m2"><p>شمع ما از جوشش پروانه بال و پر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب گزیدن های او امروز بیجا نیست دوش</p></div>
<div class="m2"><p>بوسه ها از لعل میگونش لب ساغر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما در آن فکریم تا [راهی] به خود پیدا کنیم</p></div>
<div class="m2"><p>نامسلمان برد دین و کفر را کافر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد گیر از شمع ای پروانه رسم سوختن</p></div>
<div class="m2"><p>تن به آتش داد و آخر شعله را دربر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا سیمین تنی بینی به سیمش کش کنار</p></div>
<div class="m2"><p>کیست شیرین یوسفی را می توان با زر گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه در آتش کرد تیزها ولیکن نرم نرم</p></div>
<div class="m2"><p>بر سر این تندمشرب جای، خاکستر گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه عریان کرد جان بر کف نهاد و داد سر</p></div>
<div class="m2"><p>تا سعیدا داد دل از نیش آن خنگر گرفت</p></div></div>