---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>به آن خدا که جهان را جز او خدایی نیست</p></div>
<div class="m2"><p>جهان هر چه در او هست ماسوایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غیر چین جبین و اشارت ابرو</p></div>
<div class="m2"><p>مرا به کعبهٔ دیدار، رهنمایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن به خانهٔ خود الفتی گرفته تنم</p></div>
<div class="m2"><p>که غیر پهلوی خود نقش بوریایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکن سر دل و ای عقل هر چه خواهی کن</p></div>
<div class="m2"><p>که مهتر از تو در این خانه کدخدایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ذوق می روم از خویشتن که در این راه</p></div>
<div class="m2"><p>نشان چین جبینی و نقش پایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درد خویش نگویم جز آن طبیب به کس</p></div>
<div class="m2"><p>که رحم در دل و در خانه اش دوایی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قسم به حلقهٔ آن زلف تابدار، [سعید]</p></div>
<div class="m2"><p>که خال گوشهٔ آن چشم بی بلایی نیست</p></div></div>