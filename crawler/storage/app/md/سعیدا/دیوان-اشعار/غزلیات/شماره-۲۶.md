---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>هر که دارد دل چون آینه سیمای تو را</p></div>
<div class="m2"><p>می کند خوب ز چشم تو تماشای تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رم نکردی ز من و رام کسی هم نشدی</p></div>
<div class="m2"><p>آفرین باد دل و دیدهٔ بینای تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دایماً چشم تو انداز رمیدن دارد</p></div>
<div class="m2"><p>هیچ کس رام نکرد آهوی صحرای تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشود تیره دل پاک تو از چین جبین</p></div>
<div class="m2"><p>موج بر هم نزند صافی دریای تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشب از داغ جگر لاله چراغان دارد</p></div>
<div class="m2"><p>به هواداری گل دامن صحرای تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آب حیوان شده سرسبز خط پشت لبت</p></div>
<div class="m2"><p>خضر امت شده ز اعجاز مسیحای تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حلاوت لب ساغر ز لبت وانشود</p></div>
<div class="m2"><p>باده چون تلخ کند لعل شکرخای تو را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس از نازکی طبع تو نتوانم زد</p></div>
<div class="m2"><p>آه، صد آه، دل آینه سیمای تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو خورشید کشد باز به عریانی سر</p></div>
<div class="m2"><p>جامه گر اطلس چرخ است سعیدای تو را</p></div></div>