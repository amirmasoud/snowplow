---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>گریه را سرکرد چشمم باز طوفان کرد عشق</p></div>
<div class="m2"><p>کاسهٔ آب دلم را بحر عمان کرد عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوخت ما را گرچه از غم لیک جانان را گداخت</p></div>
<div class="m2"><p>کس به تن هرگز نسازد آنچه با جان کرد عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باعث تشویش معشوق است جوش عاشقان</p></div>
<div class="m2"><p>از هجوم بلبلان گل را پریشان کرد عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خادم طفل کلیسا ساخت چندین شیخ را</p></div>
<div class="m2"><p>ای بسا فرعون را موسای عمران کرد عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه خود را می زند بر آتش و گاهی بر آب</p></div>
<div class="m2"><p>کس نمی داند که با عاشق چه فرمان کرد عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شبستان گنه ما را ز مهر خویشتن</p></div>
<div class="m2"><p>روشناس مرد و زن چون ماه تابان کرد عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معنی «یا نار کونی» نیست مخصوص خلیل</p></div>
<div class="m2"><p>آتشی بر هر که زد آخر گلستان کرد عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل طبعم سعیدا پیش از این خاموش بود</p></div>
<div class="m2"><p>در تماشای گل رویی غزلخوان کرد عشق</p></div></div>