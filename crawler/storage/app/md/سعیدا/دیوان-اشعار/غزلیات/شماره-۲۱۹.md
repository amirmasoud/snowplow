---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>زمین از اشک سرگردان من گرداب می‌گردد</p></div>
<div class="m2"><p>زمان از ناله‌های زار من سیماب می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر خورشید در این صبح با گل گرم سر کرده</p></div>
<div class="m2"><p>که از گستاخیش در چشم شبنم آب می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم طالع ما سودهٔ الماس اگر پاشی</p></div>
<div class="m2"><p>ز چشم ما نهان ناگشته آن هم‌خواب می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا در فقر حاصل گشته اکسیر قناعت بس</p></div>
<div class="m2"><p>که خاکستر به زیر پهلوم سنجاب می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چسان مشاطه بربندد کمر یارب نمی‌داند</p></div>
<div class="m2"><p>میانی را که از تشبیه مو بی‌تاب می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر قصد شبیخونی سعیدا آسمان دارد</p></div>
<div class="m2"><p>که امشب بر سر ویرانه‌ام مهتاب می‌گردد</p></div></div>