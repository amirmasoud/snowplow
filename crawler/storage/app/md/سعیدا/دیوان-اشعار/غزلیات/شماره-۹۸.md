---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>صافیدلی چو آینه در این زمان کم است</p></div>
<div class="m2"><p>ور هست همچو آب روان در پی هم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود عجب که منت آسودگی کشم</p></div>
<div class="m2"><p>زخم صحیح ناشده در زیر مرهم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینه از تراش و خراش است پرضیا</p></div>
<div class="m2"><p>روشنگر طبیعت ما خلق عالم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب از دهان ساغر جمشید می رود</p></div>
<div class="m2"><p>صبحی دمی که غنچهٔ گل پر ز شبنم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیچد به آن کمر ز گمان شانه زلف را</p></div>
<div class="m2"><p>یک موی در حساب ز کاکل اگر کم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا حشر برنخیزد اگر بر فلک نهند</p></div>
<div class="m2"><p>[سرباریی] که بر سر فرزند آدم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زیر خاک، همت می جوش می زند</p></div>
<div class="m2"><p>این طفل نارسیده مگر نسل آدم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم طناب طول امل در گلوی توست</p></div>
<div class="m2"><p>تا میخ آز در گل حرص تو محکم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بتخانه است دهر و سعیداست بت پرست</p></div>
<div class="m2"><p>آن کاو مرید خال و خط و زلف و پرچم است</p></div></div>