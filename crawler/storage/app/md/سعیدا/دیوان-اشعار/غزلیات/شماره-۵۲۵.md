---
title: >-
    شمارهٔ ۵۲۵
---
# شمارهٔ ۵۲۵

<div class="b" id="bn1"><div class="m1"><p>شده شرمسار به دوران تو پیمانهٔ حسن</p></div>
<div class="m2"><p>نگهت مست می و چشم تو میخانهٔ حسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه بهشتی است جمال تو که هر حلقهٔ زلف</p></div>
<div class="m2"><p>شده در دیدهٔ عشاق پریخانهٔ حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تماشا رود و دیدهٔ بیدار شود</p></div>
<div class="m2"><p>گوش در خواب اگر بشنود افسانهٔ حسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست با بلبل جان بلبل گل را نسبت</p></div>
<div class="m2"><p>نیست پروانهٔ این شمع چو پروانهٔ حسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده ابروی تو از چین جبین نقش [و] نگار</p></div>
<div class="m2"><p>عکس طاقی که به دل هاست ز کاشانهٔ حسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست جز خال رخ ماهوشان در دل ما</p></div>
<div class="m2"><p>نشود سبز از این خاک بجز دانهٔ حسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط و خال، چشم سیه، زلف پریشان حاضر</p></div>
<div class="m2"><p>خاطر جمع ندیدیم بجز خانهٔ حسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشنای دل دیوانه نگردد هرگز</p></div>
<div class="m2"><p>مشرب هر که سعیدا شده بیگانهٔ حسن</p></div></div>