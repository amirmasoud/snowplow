---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>در خانه ای که جای کسی نیست جای ماست</p></div>
<div class="m2"><p>بامی که بر هواست بنایش بنای ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لنگر، جفا و صبر و هوا جذب و موج، اشک</p></div>
<div class="m2"><p>خون بحر و دل سفینه و غم ناخدای ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن را که احتیاج نباشد به بندگی</p></div>
<div class="m2"><p>از بندگان بی سر و سامان خدای ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کارگاه عشق، دل سنگ تیره را</p></div>
<div class="m2"><p>آن صیقلی که آینه سازد جلای ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را نه دوستیست به دشمن گداز ما</p></div>
<div class="m2"><p>بیگانه هر که شد ز جهان آشنای ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغی که بال و پر به هوای خدنگ او</p></div>
<div class="m2"><p>از استخوان کشیده سعیدا همای ماست</p></div></div>