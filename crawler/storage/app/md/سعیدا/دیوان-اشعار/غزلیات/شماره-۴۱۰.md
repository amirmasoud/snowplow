---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>در جهان هرگز دلا منشین ز پای احتیاط</p></div>
<div class="m2"><p>در بهشت جاودان خالی است جای احتیاط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت دنیا و عقبی هر دو می آید به دست</p></div>
<div class="m2"><p>سایه ای بر هر که اندازد همای احتیاط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دزد از هر سو کمین دارد خبردار ای رفیق</p></div>
<div class="m2"><p>نیست تدبیری متاعت را ورای احتیاط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مس خود را توانی کرد زر اول بیا</p></div>
<div class="m2"><p>یادگیر از خرده بینان کیمیای احتیاط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست بر آزادگان اندیشه ای از کفر و دین</p></div>
<div class="m2"><p>بینوایان را کجا باشد نوای احتیاط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توانی توبهٔ اشکسته را کردن درست</p></div>
<div class="m2"><p>گر کنی در کار دایم، مومیای احتیاط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیب من در این غزل کردن سعیدا خوب نیست</p></div>
<div class="m2"><p>گفته ام من این غزل را از برای احتیاط</p></div></div>