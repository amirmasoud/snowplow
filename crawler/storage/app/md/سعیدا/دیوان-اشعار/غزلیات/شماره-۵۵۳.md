---
title: >-
    شمارهٔ ۵۵۳
---
# شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>باز چشمش سرمه آمیز آمده</p></div>
<div class="m2"><p>طرفه بیماری به پرهیز آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر داغ و آه از دل برنخاست</p></div>
<div class="m2"><p>وادی ایمن بلاخیز آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه هستی را به ما هموار کرد</p></div>
<div class="m2"><p>عشق فرهاد سحرخیز آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که او مست شراب بیخودی است</p></div>
<div class="m2"><p>ساغرش بی باده لبریز آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناتوانی دیده کس جز چشم او</p></div>
<div class="m2"><p>خسته و بیمار و خونریز آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تبسم لب دگر هرگز نبست</p></div>
<div class="m2"><p>تا به کام دل نمک ریز آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد به غارت کردن جان ها سوار</p></div>
<div class="m2"><p>هر دو فتراکش دلاویز آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوخت دل گویا به داغ سینه ام</p></div>
<div class="m2"><p>آه من با شعلهٔ تیز آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیست یارب در پس آیینه ام</p></div>
<div class="m2"><p>طوطی نطقم شکرریز آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان من شد بندهٔ ملای روم</p></div>
<div class="m2"><p>دل مرید شمس تبریز آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از برای غم سعیدا غم مخور</p></div>
<div class="m2"><p>گرچه این لشکر به انگیز آمده</p></div></div>