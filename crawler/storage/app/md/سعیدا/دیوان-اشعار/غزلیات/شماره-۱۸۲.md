---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>در شکنج زلف او دل تاب نتواند گرفت</p></div>
<div class="m2"><p>چون کتان، کام از شب مهتاب نتواند گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی لب شیرین کند کار نگاه تلخ را</p></div>
<div class="m2"><p>جای می را گر دهد جان آب نتواند گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کام از کامل عیاران یافتن آسان مدان</p></div>
<div class="m2"><p>کس دمی آب از گل سیراب نتواند گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی ریاضت کی ملایم می شود طبع ثقیل</p></div>
<div class="m2"><p>نرم خویی را کس از سنجاب نتواند گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر همت سعیدا چرخ را آمد به جوش</p></div>
<div class="m2"><p>کاسه ای دارد که یک کف آب نتواند گرفت</p></div></div>