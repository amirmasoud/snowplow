---
title: >-
    شمارهٔ ۵۸۰
---
# شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>دل برده است چشم قتال جنگجویی</p></div>
<div class="m2"><p>خورشیدوضع و مهرو چون باده تندخویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منع مدام زاهد دارد دوام تلخی</p></div>
<div class="m2"><p>یا حرف تلخ بهتر یا می در این چه گویی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رمزی است نشئهٔ می، پندم به گوش خود گیر</p></div>
<div class="m2"><p>زنهار تا توانی با نیک و بد نکویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم همین نمودی است خالی ز لطف معنی</p></div>
<div class="m2"><p>رنگ وفا ندارد نقشی است بر کدویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مسجد و خرابات نشنیدم و ندیدم</p></div>
<div class="m2"><p>غیر از فغان و شوری جز بانگ های و هویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ چمن همی گفت دی با ترنم خوش</p></div>
<div class="m2"><p>زنهار دل نبندد عاقل به رنگ و بویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز این چرخ بی مروت کام دلی نخواهی</p></div>
<div class="m2"><p>کاین تشنه ای است مشتاق تا ریزد آبرویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز نمی نماید آن ماه رخ سعیدا</p></div>
<div class="m2"><p>تا این غبار هستی از خاطرت نشویی</p></div></div>