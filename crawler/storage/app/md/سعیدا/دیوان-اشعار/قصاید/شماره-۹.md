---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ز شوخی خطش غوطه زد در نگاه</p></div>
<div class="m2"><p>که شد دیده را نور بینش سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عکس هلال سر انگشت او</p></div>
<div class="m2"><p>بر اوج فلک شد نمایان دو ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رد کلام عدو از قضا</p></div>
<div class="m2"><p>دو گیسوی او بر شرافت گواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدا خواست کز آستان زمین</p></div>
<div class="m2"><p>بیاید چنین مظهر [و] پیشگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفرمود تا جبرئیل امین</p></div>
<div class="m2"><p>برد مرکب چون نگه سر به راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان دم کمند محبت گرفت</p></div>
<div class="m2"><p>بینداخت بر گردن ماه، آه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>براقی چو آدم به رنگ و به بوی</p></div>
<div class="m2"><p>که در جنتش بود آرامگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مژگان دم و یال او شانه کرد</p></div>
<div class="m2"><p>ز خورشید زین بست و نعلش ز ماه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شتابنده چون برق آمد به زیر</p></div>
<div class="m2"><p>زمین بوسه داد و بگفتا اله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلامت فرستاد یا مصطفی</p></div>
<div class="m2"><p>به خود خواند و بر عرش زد بارگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سواری فرستاده و شاطری</p></div>
<div class="m2"><p>که بیخود به خود آید این شاهراه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو جان با الف تن به قد راست کرد</p></div>
<div class="m2"><p>چو بشنید این مژده آن دوست خواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به اول چو زد دست بر زین گذاشت</p></div>
<div class="m2"><p>نظر بر خدا پای بر ماسواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندانم چسان رفت این راه را</p></div>
<div class="m2"><p>که عاجز بود در تصور، نگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برای سر و پای انداز او</p></div>
<div class="m2"><p>جهان سر نهاد و جهانبان کلاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به جایی تقرب رسانیده بود</p></div>
<div class="m2"><p>که در سایه اش بود طوبی گیاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و را در نظر هم دو عالم یکی است</p></div>
<div class="m2"><p>مساوی به چشمش سفید و سیاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نباشد بجز همتش روز حشر</p></div>
<div class="m2"><p>به افتادگان و غریبان پناه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سعیدا بد من چو نیکی کم است</p></div>
<div class="m2"><p>چهان پرگناهست او عذرخواه</p></div></div>