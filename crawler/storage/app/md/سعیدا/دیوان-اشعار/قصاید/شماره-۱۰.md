---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ز خود تهی شده ام تا نوازیم به دمی</p></div>
<div class="m2"><p>چو نی اگر کنیم بند بند، نیست غمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز همتت حبشی رتبهٔ قریشی یافت</p></div>
<div class="m2"><p>قبول رای تو خواهد کند عرب، عجمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آن که در ره تو سر نهد هنوز کم است</p></div>
<div class="m2"><p>هزار سجدهٔ شکر آورد به هر قدمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمال ذات تو را فهم کس کجا سنجد</p></div>
<div class="m2"><p>که کی احاطه تواند وجود را عدمی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز طوف کوی تو دورم فلک چرا انداخت</p></div>
<div class="m2"><p>ز دست چرخ ندارم به غیر از این المی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشم غم تو به جان تا نفس بود باقی</p></div>
<div class="m2"><p>که نیست هیچ دمی بی شکنجهٔ ستمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکسته رونق اصنام گرچه در عهدت</p></div>
<div class="m2"><p>من از خیال تو دارم به دل نهان صنمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو پادشاه جهان، من کمینه سائل تو</p></div>
<div class="m2"><p>رجا ز شاه گدا را بود دم و قدمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی فغان کنم و گاه گریه انگیزم</p></div>
<div class="m2"><p>شنو ز نغمهٔ عشاق خویش زیر و بمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گدای کوی تو را بر جبین بود پیدا</p></div>
<div class="m2"><p>نشان دولت باقی و جاه و محتشمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکسته کاسهٔ آبی که بینوا دارد</p></div>
<div class="m2"><p>کند به خارق عادات کار جام جمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشد ترشح اشکم ز دیده کم هرگز</p></div>
<div class="m2"><p>ز خون اگر به دلم بود تا گمان نمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آن زمان که چپ و راست را شناخته ام</p></div>
<div class="m2"><p>به غیر داغ ندیدم به دست خود درمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشان عشق و محبت به آل و اصحابت</p></div>
<div class="m2"><p>به خط کاتب صنع است در دلم رقمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا خیال سعیدا رسد به کنه کمالت</p></div>
<div class="m2"><p>چسان محاصره سازد وجود را عدمی؟</p></div></div>