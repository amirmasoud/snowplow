---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>کی ز کوی تو سعیدا نفسی می شد دور</p></div>
<div class="m2"><p>گر خیال تو نمی داد دلش را دستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه در صورت ظاهر ز تو دور افتادم</p></div>
<div class="m2"><p>نیست منظور خیالم بجز ایام حضور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرتو شعلهٔ دیدار منور دارد</p></div>
<div class="m2"><p>کلبه ام را مه و خورشید چه نزدیک و چه دور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر، مفتاح فرج گفته رسول عربی</p></div>
<div class="m2"><p>به امید سخن اوست که گشتیم صبور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا جدا کرده نظرهای رقیبان از یار</p></div>
<div class="m2"><p>چشم زخمی است که افتاده به رویم ناصور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حلاوت چو عسل گشته کلامم که فراق</p></div>
<div class="m2"><p>بسکه بر شان دلم نیش زده چون زنبور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطلب از سیر جنان دیدن همعصران است</p></div>
<div class="m2"><p>ورنه بالله که خواهش نبود حور قصور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مثنوی تا به جهان شرح جدایی را کرد</p></div>
<div class="m2"><p>گشت در عالم تنهایی ما نی مشهور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منتظر باش مگر یوسفی از غیب رسد</p></div>
<div class="m2"><p>که کند کلبهٔ احزان تو بیت المعمور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش در کوی خرابات گذارم افتاد</p></div>
<div class="m2"><p>مطربی ساغر می در کف و می زد تنبور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>معنی مصرع ثانی به ترنم می گفت</p></div>
<div class="m2"><p>وای بر حال کسی کاو ز تو باشد مهجور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شام شهر رمضانم شب هجران گشته</p></div>
<div class="m2"><p>می برد رشک ز شام تو مرا وقت سحور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیده از دیدهٔ من سیل روانی زان رو</p></div>
<div class="m2"><p>چرخ افکند در این گوشه به امداد بحور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقت مهر است خدایا ز سحاب کرمت</p></div>
<div class="m2"><p>چند در ابر جدایی بود این خور مستور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز هجران مرا هم شب وصلی بفرست</p></div>
<div class="m2"><p>که به این روز شوم چون شب یلدا مشهور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خیال تو نسازم چه کنم مسأله را</p></div>
<div class="m2"><p>آب چون نیست ز خاک است طهارت به ضرور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طرفه سخت است به مطلوب رسیدن در هجر</p></div>
<div class="m2"><p>این کمان را نکشیده است کسی با زر و زور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی تو درد دیدهٔ من دهر چنان تنگ [فضاست]</p></div>
<div class="m2"><p>که برم رشک چو آید به نظر دیدهٔ مور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خار صحرای فنا سائل دامن گیر است</p></div>
<div class="m2"><p>پوست بر دوش در این راه به از کرک سمور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز اتحادم سخنی چند بیان می کردم</p></div>
<div class="m2"><p>لیک ترسم که به دارم نکنی چون منصور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ذکر مهجور بجز وصل نباشد چیزی</p></div>
<div class="m2"><p>غیر می سر نزند حرف دگر از مخمور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می برد یاد تو از هوش، خیالت از خویش</p></div>
<div class="m2"><p>رفت بیجا سخنی گر ز سعیدا معذور</p></div></div>