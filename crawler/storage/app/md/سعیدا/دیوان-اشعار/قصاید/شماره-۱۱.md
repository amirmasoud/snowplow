---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>به چشم خلق شود تا عزیز گرد رهش</p></div>
<div class="m2"><p>کشد به دیدهٔ خود سرمهٔ سلیمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چها گذشته به دور خطش ز دل ها زلف</p></div>
<div class="m2"><p>مگر کند به صبا شرح این پریشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگرنه کیست که با صد زبان ادا سازد</p></div>
<div class="m2"><p>بیان به غیر سخن رمزهای پنهانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین فلک شد و گردون گشود دامن خویش</p></div>
<div class="m2"><p>چو کرد کوکب بختش ستاره افشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهعقده اش ز خم و پیچ و تاب بگشاید</p></div>
<div class="m2"><p>کسی که بسته بر آن زلف، دل به آسانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خار می خلدم گل در این چمن بی او</p></div>
<div class="m2"><p>که می کند به دلم برگ غنچه، پیکانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذشته ناوک اعجاز او ز چشم عدو</p></div>
<div class="m2"><p>نشسته در دل دشمن به قصد ویرانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بیخ کفر کند از زمین سینهٔ آن</p></div>
<div class="m2"><p>عمارت دگر انگیزد از مسلمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بوی جعد تو سنبل گشوده کاکل خویش</p></div>
<div class="m2"><p>به یاد حسن تو گل می کند گلستانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دوستان تو جنت گشاده دکان را</p></div>
<div class="m2"><p>به امتان تو بازار حسن ارزانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه احتیاج به روی کتاب، علم تو را</p></div>
<div class="m2"><p>که خوانده ای تو به اعجاز، خط پیشانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برای آن که کند بحر، ذکر خیر تو را</p></div>
<div class="m2"><p>گرفته است به کف سبحه های مرجانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار و هشت کم از صد گذشته از عهدت</p></div>
<div class="m2"><p>هنوز در همه آفاق حکم می رانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه الف و صد که هزاران سال دگر</p></div>
<div class="m2"><p>گذر کند که نگردد شریعتت فانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به حشر هم ره و رسم تو برقرار بود</p></div>
<div class="m2"><p>ز روی شرع کند کار، حکم یزدانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو بر سریر سعادت بخسب با صد ناز</p></div>
<div class="m2"><p>که چاکران تو هر سو کنند سلطانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حجازیان همه اهل صفا شدند و سرور</p></div>
<div class="m2"><p>که شد مقام ظهور تو بیت ربانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به سوی کعبه از آن پنج وقت، خلق خدا</p></div>
<div class="m2"><p>ز شش جهت به زمین می نهند پیشانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یک گرفتن نام تو پاکباز شدند</p></div>
<div class="m2"><p>که عمرها به بتی کرده اند رهبانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کند به روز جزا دامن شفاعت تو</p></div>
<div class="m2"><p>به سر کشیدن عفو و کرم گریبانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کند تلاطم عشق تو هر زمان از دل</p></div>
<div class="m2"><p>به سوی دیده عروج و به دیده عمانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ذوق آن که نثارت کند جواهر اشک</p></div>
<div class="m2"><p>دو دیده ام شده سرگرم قطره افشانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به آن امید که روز جزا تویی نوحم</p></div>
<div class="m2"><p>کند دو دیدهٔ من تا به حشر، طوفانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به کیش من ز سرانجام هر دو کون اولی</p></div>
<div class="m2"><p>تو گر پسند کنی بی سر و سامانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن جهت که به ایتام رحم فرمودی</p></div>
<div class="m2"><p>کند سحاب به اولاد خاک، پستانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن که رؤیت تو نقد نیست امروزم</p></div>
<div class="m2"><p>فتاده جان به تنم همچو جنس تاوانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولیک بلبل طبعم به یاد آن گل روی</p></div>
<div class="m2"><p>همیشه فکر سخن دارد و غزلخوانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیام گوش تو را هر زمان به دل آرم</p></div>
<div class="m2"><p>که تا ز بحر سخن در کشم به آسانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به این تسلی خاطر قبول جان کردم</p></div>
<div class="m2"><p>که در خیال، تویی نیست جز تو می دانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگرچه نام تو بردن ز من روا نبود</p></div>
<div class="m2"><p>که کفر را نرسد حرف از مسلمانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو، صبح عالم قدسی و آفتاب دو کون</p></div>
<div class="m2"><p>من فقیر، شب تار و شام ظلمانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز فیض صبح تو شامم امید آن دارم</p></div>
<div class="m2"><p>که بگذرد شب و گردم چو روز نورانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نظر به حال سعیدا کن و دریغ مدار</p></div>
<div class="m2"><p>چو سفرهٔ کرم خویش را بیفشانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به خوی و بوی و به افعال من مگیر و ببخش</p></div>
<div class="m2"><p>که لازم است سگی خانه را به دربانی</p></div></div>