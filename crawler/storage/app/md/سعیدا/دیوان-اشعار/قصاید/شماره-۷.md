---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>قرآن چو به حرف آمد و مخلوق شد انسان</p></div>
<div class="m2"><p>آن سورهٔ رحمن شد و این صورت رحمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم ایجاد به اوصاف حقیقت</p></div>
<div class="m2"><p>موصوف نگردید بجز حضرت انسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمم به رخش شیفته و دل به خم زلف</p></div>
<div class="m2"><p>بیدار پری بینم و در خواب پریشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف تو و رخسار تو معدوم شمارند</p></div>
<div class="m2"><p>غیر از دل آشفته و جز دیدهٔ حیران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چارسوی حسن چو بازار شود گرم</p></div>
<div class="m2"><p>نقصان همه سود آید و سودا همه تاوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جایی که کند عشق خریدی و فروشی</p></div>
<div class="m2"><p>صد جان به نگاهی نخرد چشم ادادان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس دل و دین کرد بها می خورد آخر</p></div>
<div class="m2"><p>افسوس ز ناکرده و از کرده پشیمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از اول و آخر همه را سیر نمودیم</p></div>
<div class="m2"><p>حق بود اگر انس نمودند و اگر جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما جمله خداییم که در دهر نمودیم</p></div>
<div class="m2"><p>هر قطره که بینی ز محیط است و ز عمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از معرفت خویش چه گوییم که مستیم</p></div>
<div class="m2"><p>نی عارف و معروف شناسیم و نه عرفان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم دلبر ما باید و هم دلبری خویش</p></div>
<div class="m2"><p>آسان نشماریم گرفتاری خوبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دردسر ایشان ز تو دانی که زیاد است</p></div>
<div class="m2"><p>ای خسته اگر راه بری حال طبیبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ما چه طمع چرخ فلک را که خراجی</p></div>
<div class="m2"><p>هرگز طلبیده است کس از خانهٔ ویران؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در کوچهٔ آن زلف منادی است ز هر سو</p></div>
<div class="m2"><p>گویید به اسلام که یک کفر و صد ایمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پا بر سر افتادهٔ دشمن نگذاریم</p></div>
<div class="m2"><p>سرسبز نسازیم بجز خار مغیلان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در مزرع دنیا ننشانیم نهالی</p></div>
<div class="m2"><p>از طالع ما گرچه شود سرو خرامان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای طالب دنیا برو و فکر دگر کن</p></div>
<div class="m2"><p>تا چند در این فکر نشد این و چه شد آن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ریزد ز شفق خون دم صبح زمانه</p></div>
<div class="m2"><p>ناکرده برون مهر سر از چاک گریبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در هر دل و هر بیشه و هر شهر که رفتیم</p></div>
<div class="m2"><p>دیدیم که غم در پی و شادی است گریزان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ما هم ز صف پیش کشیدیم عنان را</p></div>
<div class="m2"><p>ننگ آمد از این بی جگر بی سر و سامان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عمری است که با غم سر و کار است در این دهر</p></div>
<div class="m2"><p>او صاحب امر آمد و ما بندهٔ فرمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر حکم کند از جگر خویش برآریم</p></div>
<div class="m2"><p>شوری که به گردش نرسد موجهٔ طوفان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از پوست برون آی سعیدا که زمانه</p></div>
<div class="m2"><p>از روی گمان می فشرد پنجهٔ مرجان</p></div></div>