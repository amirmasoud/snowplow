---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ز دست این فلک اژدر تمام دهن</p></div>
<div class="m2"><p>بیا ز حق مگذر مشکل است جان بردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیم نیش حوادث چنان گداخته ام</p></div>
<div class="m2"><p>روم بتاب اگر آیم به دیدهٔ سوزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چاه غم چه فروکرده ای مرا ای چرخ</p></div>
<div class="m2"><p>نه یوسفم که تو را کینه است نی بیژن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیا ز چشم جهان رفته بسکه بگریزم</p></div>
<div class="m2"><p>ز خانه ای که به دیوار او بود روزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس به مردم بیگانه خو گرفته دلم</p></div>
<div class="m2"><p>چو چشم آینه از یاد رفته فکر وطن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدوز دیده به بند قبای تنگ کسی</p></div>
<div class="m2"><p>که هست نام کفن در لباس پیراهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لباس عافیت از کارخانهٔ گردون</p></div>
<div class="m2"><p>ندیده ایم به دوش کسی به غیر کفن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان به گوشهٔ سلاخ خانه می ماند</p></div>
<div class="m2"><p>یکی گرفته زر و دیگری گرفته رسن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی به بیع مقید یکی به دلالی</p></div>
<div class="m2"><p>یک بریده سر و دست و دیگری گردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی شود ز شکر خواب خود دگر بیدار</p></div>
<div class="m2"><p>اگر به خواب ببیند کسی تو را به سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به معنیت نرسم لیک این قدر دانم</p></div>
<div class="m2"><p>که جان پاک تو او گشته است روح بدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سماع پاک تو از لطف خویش اگر خواهد</p></div>
<div class="m2"><p>دهد به صورت دیوار، ذوق حرف زدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به غیر دست تو از دست هیچ کس ناید</p></div>
<div class="m2"><p>زانبیا قلم رفته را دگر کردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به داد ما نرسیدی مگر به مذهب تو</p></div>
<div class="m2"><p>درست نیست ز حال شکسته پرسیدن؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من از حیات جهان این دو مدعا دارم</p></div>
<div class="m2"><p>نظر به روی تو کردن، ز عمر برخوردن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سیر عالم معنی شراب زاد راه است</p></div>
<div class="m2"><p>که اول سفر ما بود ز خود رفتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نظر به بد مگشا دیده پاک کن از عیب</p></div>
<div class="m2"><p>که هیچ معصیتی نیست بد ز بد دیدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هوای راحت جسمت در آتش اندازد</p></div>
<div class="m2"><p>که شمع سوخته است از برای رشتهٔ تن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بحر فکر میا چون صدف برون هرگز</p></div>
<div class="m2"><p>اگرچه معنی بکرت بود چو در عدن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هزار دیده به الماس تیز کرده فلک</p></div>
<div class="m2"><p>کجا ز دست جهان می شود نهان معدن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عقیق، سکهٔ خود باخت بهر نام و نشان</p></div>
<div class="m2"><p>ولی چه سود که مشهور گشته نام یمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو مو به لقمه میاویز در گلوی کسی</p></div>
<div class="m2"><p>اگر گره خورد از لاغریت، رشتهٔ تن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمم ز پشه و نمرود نیستم ای چرخ</p></div>
<div class="m2"><p>که می کشی به روش انتقام او از من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز نقش های جهان دلنشین نشد نقشی</p></div>
<div class="m2"><p>به غیر نقش خودی از خیال دل کندن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به مذهب فقرا کم ز خودفروشی نیست</p></div>
<div class="m2"><p>به روی نقره و زر نام خویشتن بردن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان بد است به بردن طمع ز کس که بود</p></div>
<div class="m2"><p>اگر حکایت بیرون به خانه آوردن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز تاب یک سر آن موی برنمی آید</p></div>
<div class="m2"><p>کسی که تاب [همی داد] حلقه ای آهن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخن مگوی که عین خطاست غیر از شکر</p></div>
<div class="m2"><p>اگرچه هست سعیدا تو را زبان الکن</p></div></div>