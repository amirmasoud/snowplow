---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>در دیده ات از نور حقیقت اثری نیست</p></div>
<div class="m2"><p>در دل ز سراپردهٔ سرت نظری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگاه شوی گر تو ز سر نفس خویش</p></div>
<div class="m2"><p>دانی که تو را یک نفس از وی گذری نیست</p></div></div>