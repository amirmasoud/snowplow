---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>وهمی به خیال کرده جا نام جهان</p></div>
<div class="m2"><p>از وهم جهانیان شده سرگردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیر، ظهور آن توهم شده کفر</p></div>
<div class="m2"><p>در کعبه خیال آن توهم ایمان</p></div></div>