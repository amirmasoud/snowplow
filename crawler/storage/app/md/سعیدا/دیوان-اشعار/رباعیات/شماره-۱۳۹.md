---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>ای دل نفسی صاحب عرفان نشدی</p></div>
<div class="m2"><p>بر درد کسی هیچ تو درمان نشدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کشته به روی دوست حیران نشدی</p></div>
<div class="m2"><p>صد حیف گذشت عید، قربان نشدی</p></div></div>