---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>تارک الف و دال که باشد خوب است</p></div>
<div class="m2"><p>بی پا و سر و مال که باشد خوب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه و ابدال که باشد خوب است</p></div>
<div class="m2"><p>درویش به هر حال که باشد خوب است</p></div></div>