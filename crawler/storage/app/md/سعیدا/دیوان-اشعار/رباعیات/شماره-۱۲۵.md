---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>بحری پنهان چو کرد جوش و طغیان</p></div>
<div class="m2"><p>هر سو گردید موج هاست دست افشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتاد به خاک قطره ها زان افشان</p></div>
<div class="m2"><p>بعضی حیوان شدند و بعضی انسان</p></div></div>