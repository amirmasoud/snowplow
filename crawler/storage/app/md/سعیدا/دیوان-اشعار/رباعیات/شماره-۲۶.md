---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>نی در دل من غمی از آن و این است</p></div>
<div class="m2"><p>نی وسوسهٔ عادت و رسم و دین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جا که منم نه ماه و نی پروین است</p></div>
<div class="m2"><p>آن جا همه حق و دیدهٔ حق بین است</p></div></div>