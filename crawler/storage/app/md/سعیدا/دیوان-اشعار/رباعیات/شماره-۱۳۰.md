---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>ای شیخ کرامت منما رهبر شو</p></div>
<div class="m2"><p>نی چون کاغذ ز باد بالاپر شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خس بر آب رفتن از بی مغزی است</p></div>
<div class="m2"><p>در بحر یقین غوطه خور و گوهر شو</p></div></div>