---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>هستی مال تو و نیستی مال من است</p></div>
<div class="m2"><p>فعل تو همه نیک و بد افعال من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>استغنا [و] کبریاست حال تو مدام</p></div>
<div class="m2"><p>افتادگی و شکستگی حال من است</p></div></div>