---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>تا پای تو سر، سر تو تا پا نشود</p></div>
<div class="m2"><p>از رشتهٔ کار تو گره وانشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفست گوید فنا شدم حق گشتم</p></div>
<div class="m2"><p>باور نکنی که خر مسیحا نشود</p></div></div>