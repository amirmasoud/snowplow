---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>تا کرد قبای ناز را بر تن گل</p></div>
<div class="m2"><p>در پایش ریخت رنگ از دامن گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن موی میان بر آن سرین دانی چیست</p></div>
<div class="m2"><p>موری است که گشته صاحب خرمن گل</p></div></div>