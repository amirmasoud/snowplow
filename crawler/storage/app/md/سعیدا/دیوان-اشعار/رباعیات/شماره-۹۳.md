---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>از خوان غمش حواله ای دارم بس</p></div>
<div class="m2"><p>چون درد از او نواله ای دارم بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند گدازی فلک سفله نواز</p></div>
<div class="m2"><p>چون نی بالله ناله ای دارم بس</p></div></div>