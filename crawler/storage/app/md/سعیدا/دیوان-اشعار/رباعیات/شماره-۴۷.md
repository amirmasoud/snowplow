---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>صد حیف که عمرم به خسارت بگذشت</p></div>
<div class="m2"><p>در فکر عبادت و عبارت بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر به قباحتی کشید انجامش</p></div>
<div class="m2"><p>کاری که ز دیوان اشارت بگذشت</p></div></div>