---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>خود آمده زنده مرده می باید رفت</p></div>
<div class="m2"><p>شطرنج خیال برده می باید رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که سفر کنی سعیدا ز جهان</p></div>
<div class="m2"><p>خود را به خدا سپرده می باید رفت</p></div></div>