---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>دوری ز خیالات هوا تجرید است</p></div>
<div class="m2"><p>دل چون ز همه یگانه شد تفرید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم که به یاد تو روم نوروز است</p></div>
<div class="m2"><p>هر گاه که قربان تو گردم عید است</p></div></div>