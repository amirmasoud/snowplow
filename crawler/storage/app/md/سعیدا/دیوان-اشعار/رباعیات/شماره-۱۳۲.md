---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>ای نفس و هوا برده دلت را، جان کو؟</p></div>
<div class="m2"><p>ای داده به تن نقد روان جانان کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تازه مسلمان به هوای دنیا</p></div>
<div class="m2"><p>دین را بر باد داده ای، ایمان کو؟</p></div></div>