---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>ای عهدشکن قیمت ما را چه شناسی</p></div>
<div class="m2"><p>تا مهر نورزی تو وفا را چه شناسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگشته فنا خود به بقا راه نیابی</p></div>
<div class="m2"><p>تا خود نشناسی تو خدا را چه شناسی</p></div></div>