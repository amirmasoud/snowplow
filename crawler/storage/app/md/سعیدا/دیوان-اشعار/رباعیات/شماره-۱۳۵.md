---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>این عالم ما که بی نظیر افتاده</p></div>
<div class="m2"><p>هر سو طفل و جوان و پیر افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پنجهٔ تقدیر در این جسم نحیف</p></div>
<div class="m2"><p>جان چون موی است در خمیر افتاده</p></div></div>