---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>گویند به طالب که سفر باید کرد</p></div>
<div class="m2"><p>در منزل بی خودی گذر باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی که به این و آن خدا نتوان یافت</p></div>
<div class="m2"><p>فکری دگر و کار دگر باید کرد</p></div></div>