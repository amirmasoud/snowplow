---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>امروز که سنگ و سیم و زر در کار است</p></div>
<div class="m2"><p>شام و صبح و شب و سحر در کار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با جاهل پرستیز، ظالم باید</p></div>
<div class="m2"><p>چون چوب بود سخت تبر در کار است</p></div></div>