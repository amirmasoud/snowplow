---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>تا جسم مکدرت همه جان نشود</p></div>
<div class="m2"><p>خورشید معانی تو تابان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب نظران جای به پهلو ندهند</p></div>
<div class="m2"><p>تا جوهر شمشیر نمایان نشود</p></div></div>