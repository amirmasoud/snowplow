---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>جز ناله کسی همنفس خویش ندیدیم</p></div>
<div class="m2"><p>جز درد کسی محرم این ریش ندیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی جاذبهٔ خار گلی بوی نکردیم</p></div>
<div class="m2"><p>نوشی نچشیدیم که صد نیش ندیدیم</p></div></div>