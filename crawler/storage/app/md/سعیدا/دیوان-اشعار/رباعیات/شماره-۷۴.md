---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>ای از خوف تو آسمان جامه کبود</p></div>
<div class="m2"><p>وی در ره تو زمین مسکین به سجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو بی رنگ و چه رنگ ها از تو نمود</p></div>
<div class="m2"><p>جز ذات تو جملگی نمودی است نه بود</p></div></div>