---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>آن کس که حیا ندارد او انسان نیست</p></div>
<div class="m2"><p>وصفی بهتر ز پاکی دامان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو شرم و حیا از گهر آموز که او</p></div>
<div class="m2"><p>هر چند که در بحر بود عریان نیست</p></div></div>