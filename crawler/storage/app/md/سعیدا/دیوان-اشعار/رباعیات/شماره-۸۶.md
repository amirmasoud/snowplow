---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای آن که تویی ز عقل ها بالاتر</p></div>
<div class="m2"><p>پی برده به کنه ذات تو کس کمتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگاه ز عرفان تو خواهم گویم</p></div>
<div class="m2"><p>می بینم برف و سرد، اسبابم تر</p></div></div>