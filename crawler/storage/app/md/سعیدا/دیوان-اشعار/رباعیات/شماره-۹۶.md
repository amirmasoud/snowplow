---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>گویند کسانی که جهان است خیال</p></div>
<div class="m2"><p>چیزی که خیال است حرام است حلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خواب خیال رفته آگاه نیند</p></div>
<div class="m2"><p>کیفیت حال را که خرس است جوال</p></div></div>