---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>عید آمد و رفت روزه از یاد، مرا</p></div>
<div class="m2"><p>کردند بتان به غمزه دلشاد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز به بندگی قبولم کردند</p></div>
<div class="m2"><p>دل شد ز غم قیامت آزاد مرا</p></div></div>