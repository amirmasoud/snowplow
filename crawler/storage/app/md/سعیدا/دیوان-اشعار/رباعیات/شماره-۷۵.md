---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>خود را تو مبین که بی خدا نتوان بود</p></div>
<div class="m2"><p>با آن که سوا شدی سوا نتوان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او بی تو وجود است تو بی او معدوم</p></div>
<div class="m2"><p>پس تا هستی از او جدا نتوان بود</p></div></div>