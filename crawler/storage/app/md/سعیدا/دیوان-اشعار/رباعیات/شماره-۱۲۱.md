---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>یاری دارم که روش دیدن نتوان</p></div>
<div class="m2"><p>وز سلسلهٔ موش بریدن نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی پرده از او سخن شنیدن نتوان</p></div>
<div class="m2"><p>جز راه فنا به او رسیدن نتوان</p></div></div>