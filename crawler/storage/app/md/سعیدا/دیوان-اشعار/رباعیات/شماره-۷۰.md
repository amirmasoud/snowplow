---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>آنان که ندانند تو را می خوانند</p></div>
<div class="m2"><p>در لرزه چو بید بر سر ایمانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پستان [امل] گرفته هر یک به دهن</p></div>
<div class="m2"><p>گهوارهٔ طفل نفس می جنبانند</p></div></div>