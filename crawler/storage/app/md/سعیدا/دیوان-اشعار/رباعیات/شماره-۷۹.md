---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>تا از میلی دلت گریزان نشود</p></div>
<div class="m2"><p>هرگز راهی تو را به جانان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که دو خانه را عمارت سازی</p></div>
<div class="m2"><p>تا بر سر این مقیدی، آن نشود</p></div></div>