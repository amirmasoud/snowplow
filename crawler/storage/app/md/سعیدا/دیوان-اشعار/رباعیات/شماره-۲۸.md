---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>آن رشته که کیش کفر و ایمان شده است</p></div>
<div class="m2"><p>دو سر دارد ظاهر و پنهان شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سر، که نهان ز ماست آن سر، جمع است</p></div>
<div class="m2"><p>آن سر که عیان گشت پریشان شده است</p></div></div>