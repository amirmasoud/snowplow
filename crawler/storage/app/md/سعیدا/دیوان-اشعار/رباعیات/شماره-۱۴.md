---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>تا درد و غم عشق تو درمان گیر است</p></div>
<div class="m2"><p>بی دردی تو هنوز دامان گیر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک موی درون دیده، یک خربار است</p></div>
<div class="m2"><p>عیسی است که سوزنی گریبان گیر است</p></div></div>