---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>دل رفت ز خود هنوز جان نارفته</p></div>
<div class="m2"><p>آمد به کنار از میان نارفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که به سنگی نزنندت چون تیر</p></div>
<div class="m2"><p>بگریز به خانهٔ کمان نارفته</p></div></div>