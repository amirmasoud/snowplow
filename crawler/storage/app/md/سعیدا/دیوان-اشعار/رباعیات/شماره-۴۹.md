---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>بر دایرهٔ جهان چو پرگار مپیچ</p></div>
<div class="m2"><p>هیچ است جهان به هیچ زنهار مپیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس بر سر این هیچ تو از بی خبری</p></div>
<div class="m2"><p>بسیار مگرد و همچو دستار مپیچ</p></div></div>