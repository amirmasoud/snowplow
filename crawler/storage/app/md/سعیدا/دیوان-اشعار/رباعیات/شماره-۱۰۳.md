---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>گر دل گویم تو را ز جان می ترسم</p></div>
<div class="m2"><p>ور جان خوانم هم از جهان می ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان و جهان هر دو به قربان سرت</p></div>
<div class="m2"><p>حق می گویم نه زین نه زان می ترسم</p></div></div>