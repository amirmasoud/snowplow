---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>خوش حالت آن مرد که خیرانجام است</p></div>
<div class="m2"><p>سهل است نکونام اگر بدنام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک یک رفتند گرم شد تا صحبت</p></div>
<div class="m2"><p>این عالم بی وفا مگر حمام است</p></div></div>