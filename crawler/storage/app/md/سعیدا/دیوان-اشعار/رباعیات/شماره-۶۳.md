---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>روزی که مرا ز دیر بیرون کردند</p></div>
<div class="m2"><p>در ساغر من شراب گلگون کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نخل که کاشتم چو بیرون آمد</p></div>
<div class="m2"><p>خوبان به کرشمه بید مجنون کردند</p></div></div>