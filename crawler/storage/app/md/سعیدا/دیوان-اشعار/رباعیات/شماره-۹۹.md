---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>دیروز درآمدم به کاشانهٔ دل</p></div>
<div class="m2"><p>بینم که چه می کنی تو در خانهٔ دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم به بهانه ای تو را از دوری</p></div>
<div class="m2"><p>می خوردی خون دل به پیمانهٔ دل</p></div></div>