---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>تا کی به لباس عاریت بند شدن</p></div>
<div class="m2"><p>از کسب کمال خویش خرسند شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز مال و منال خود تنومند شدن</p></div>
<div class="m2"><p>محتاج سگ و گاو و خری چند شدن</p></div></div>