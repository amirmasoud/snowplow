---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>بی ناز تو من نیاز نتوانم کرد</p></div>
<div class="m2"><p>بی روی تو دیده باز نتوانم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا قبلهٔ ابروی تو ناید به نظر</p></div>
<div class="m2"><p>بالله که من نماز نتوانم کرد</p></div></div>