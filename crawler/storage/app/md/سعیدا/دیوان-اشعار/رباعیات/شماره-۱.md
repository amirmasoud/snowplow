---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>تا روی تو دیدیم یقین شد ما را</p></div>
<div class="m2"><p>بر قدرت ذات خالق ارض [و] سما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی تو می توان به معنی پی برد</p></div>
<div class="m2"><p>«سیماهم فی وجوهمم» گفت خدا</p></div></div>