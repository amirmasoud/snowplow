---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>در راه وفا سبک خرامی خوب است</p></div>
<div class="m2"><p>در منزل دور، تیزگامی خوب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویید به این سمند دوران گویید</p></div>
<div class="m2"><p>با مردم نیک بدلگامی خوب است؟</p></div></div>