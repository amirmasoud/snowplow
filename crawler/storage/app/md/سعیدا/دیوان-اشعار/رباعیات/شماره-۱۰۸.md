---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>هر ذره مدان ز خود کمش می بینم</p></div>
<div class="m2"><p>هر مور به دست، خاتمش می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر قطرهٔ شبنمی که بر روی گل است</p></div>
<div class="m2"><p>بالله که من جام جمش می بینم</p></div></div>