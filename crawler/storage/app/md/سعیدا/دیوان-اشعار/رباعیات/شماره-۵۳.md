---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>هر صبح ز گریه رخ ترم باید کرد</p></div>
<div class="m2"><p>خاک ره دوست بر سرم باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شام به سفرهٔ خیال غم یار</p></div>
<div class="m2"><p>افطار به خون جگرم باید کرد</p></div></div>