---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>با آیینه دمی شدم روی به روی</p></div>
<div class="m2"><p>گفتا همه عیب من به من موی به موی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چرا عیب کسان می گویی</p></div>
<div class="m2"><p>گفتا که نه عیب است که گویم بر روی</p></div></div>