---
title: >-
    شمارهٔ ۱ - از کرده پشیمانم ناکرده هراسانم
---
# شمارهٔ ۱ - از کرده پشیمانم ناکرده هراسانم

<div class="b" id="bn1"><div class="m1"><p>از خویش گریزانم راهیم به خود بنما</p></div>
<div class="m2"><p>سرگشته و حیرانم چون باد پریشانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سرو خرامانم راهیم به خود بنما</p></div>
<div class="m2"><p>در کعبه ثناخوانم در صومعه رهبانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مدرسه مولانا در میکده دربانم</p></div>
<div class="m2"><p>فرعونم و هامانم گه حیه و ثعبانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه موسی و عمرانم راهیم به خود بنما</p></div>
<div class="m2"><p>گه دیدهٔ گریانم پوشیده و عریانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زلف سیه بختم گه موی پریشانم</p></div>
<div class="m2"><p>گه نادر دورانم گه خار مغیلانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاهی گل و ریحانم راهیم به خود بنما</p></div>
<div class="m2"><p>گه لعل بدخشانم گه سنگ درخشانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه کوهکنم گویند گه خسرو [و] خاقانم</p></div>
<div class="m2"><p>گه شیخم و رهبانم گه صاحب عرفانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانم که نمی دانم راهیم به خود بنما</p></div>
<div class="m2"><p>گه بندهٔ سبحانم گه نوکر سلطانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه فقر فقیرانم گه عاشق خوبانم</p></div>
<div class="m2"><p>گه صاحب ایمانم گه تابع شیطانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه عارف یزدانم راهیم به خود بنما</p></div>
<div class="m2"><p>گه رند غزلخوانم گه دیو گه انسانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موسای کلیم الله یا آذر و رهبانم</p></div>
<div class="m2"><p>من هیچ نمی دانم ای اصل تن و جانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا اینم و یا آنم راهیم به خود بنما</p></div>
<div class="m2"><p>گه در بر جانانم گه منتظر آنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاهی هدف تیری گه در خم چوگانم</p></div>
<div class="m2"><p>چون بید به ایمانم از خوف تو لرزانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من بندهٔ فرمانم راهیم به خود بنما</p></div>
<div class="m2"><p>مشکل شده آسانم هر شی شده برهانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>موسی شده هامانم جهل آمده عرفانم</p></div>
<div class="m2"><p>گه عاشق جانانم گاهی ز رقیبانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نی رنجم و رنجانم راهیم به خود بنما</p></div>
<div class="m2"><p>از دشنهٔ خوبانم صد زخم نمایانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر لحظه به دل آید من روی نگردانم</p></div>
<div class="m2"><p>من بی سر و سامانم از خانه خرابانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می دانی و می دانم راهیم به خود بنما</p></div>
<div class="m2"><p>تن گفت سرابانم جان گفت که مهمانم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خود گوی به جانانم ناسوخته بریانم</p></div>
<div class="m2"><p>دل گفت سعیدا را ای عارف حق دانم</p></div></div>