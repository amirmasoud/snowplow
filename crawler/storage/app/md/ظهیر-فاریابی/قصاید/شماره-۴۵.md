---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>دادیم دل به دست تو در پای مفکنش</p></div>
<div class="m2"><p>غافل مشو ز ناله و زاری و شیونش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دست در غمت زد و پا استوار کرد</p></div>
<div class="m2"><p>گر دست می نگیری در پا میفکنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما عهد اگر نه با سر زلف تو بسته ایم</p></div>
<div class="m2"><p>بی هیچ موجبی چو سر زلف مشکنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دل که هست بسته زنجیر زلف تو</p></div>
<div class="m2"><p>نتوان نگاه داشت به زنجیر در تنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگرفت دست فتنه گریبان هیچکس</p></div>
<div class="m2"><p>تا در نبست عشق تو دامن بدامنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگ آمد از فراق تو بر من همه جهان</p></div>
<div class="m2"><p>مسکین کسی که جز در تو نیست مسکنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کی شکار عشق تو باشد دلی که هست</p></div>
<div class="m2"><p>درگاه شاه عالم عادل نشیمنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صاحب قران مظفر دین خسرو عجم</p></div>
<div class="m2"><p>گر چرخ سر کشید فرو کوفت گردنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهی که از برای گل افشان بزم اوست</p></div>
<div class="m2"><p>هر گل که مرغزار سپهرست گلشنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر هر مبارزی که نه از نام اوست حرز</p></div>
<div class="m2"><p>از سطح آب کم بود اطراف جوشنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای همت تو ساکن آن بقعه کز علو</p></div>
<div class="m2"><p>بالای هفت خطه چرخ است بر زنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رای تو رایضی است که در زیر ران حکم</p></div>
<div class="m2"><p>هر روز رامتر بود ایام توسنش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر هر که تافت روزی خورشید لطف تو</p></div>
<div class="m2"><p>خورشید همچو ذره برآید ز روزنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتش فروغ رای تو دارد از این قبل</p></div>
<div class="m2"><p>در بر گرفته اند چو جان سنگ و آهنش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آزاده ایست لطف تو شاها که هر زمان</p></div>
<div class="m2"><p>خطی به بندگی رسد از سرو و سوسنش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر جرم ماه با تو به یک جو کند خلاف</p></div>
<div class="m2"><p>هم در زند شکوه تو آتش به خرمنش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا شب ز اختران بگشاید کمین مهر</p></div>
<div class="m2"><p>بر هم زند مصادمت روز مکمنش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد از مصادمات حوادث تو را امان</p></div>
<div class="m2"><p>کامروز هر که هست در توست مامنش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر دشمنت گشاده کمین اختران نحس</p></div>
<div class="m2"><p>وز هیبت تو تیره چو شب روز روشنش</p></div></div>