---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>چو ماه یکشبه بنهفت چهره از نظرم</p></div>
<div class="m2"><p>مه دو هفته درآمد به تهنیت ز درم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بداد مژده عید از لطف چنانک گرفت</p></div>
<div class="m2"><p>ز فرق تا به قدم جمله در گل و شکرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا به شادی رویش به سینه باز آمد</p></div>
<div class="m2"><p>دلی که مرده و زنده نبود ازو اثرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خاک در کف پایش فتادم از خواری</p></div>
<div class="m2"><p>اگر چه از سر تحقیق سر به سر گهرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لابه گفتمش آخز زمانکی بنشین</p></div>
<div class="m2"><p>مگر به وصل تو بنشیند آتش جگرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک امشبی تو به مهمان من بباش که من</p></div>
<div class="m2"><p>ز روی خوب تو مهمان زهره وقمرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اهل عشق تکلف طمع نباید داشت</p></div>
<div class="m2"><p>به پیش خدمت توست آنچه هست ماحضرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم حمایتی زلف توست از او بگذر</p></div>
<div class="m2"><p>که نیست زهره آنم که سوی او نگرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث جان نکنم کان کرای آن نکند</p></div>
<div class="m2"><p>فدای یک قدمت گر بود صد دگرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسنده کن به لب خشک و چشم تر با من</p></div>
<div class="m2"><p>که در دو گیتی از این بیش نیست خشک و ترم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا امید وصال تو زنده می دارد</p></div>
<div class="m2"><p>وگرنه بی تو نه عینم بماند و نه اثرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی بگفتم ازین جنس و هیچ سود نداشت</p></div>
<div class="m2"><p>کز اشک چهره همی دید نقد سیم وزرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخاست ناله و زاری زمن چو او برخاست</p></div>
<div class="m2"><p>برفت و بر اثر او برفت دل ز برم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رخش که تابش قندیل روزه داران داشت</p></div>
<div class="m2"><p>گذاشت چون علم عید در جهان سمرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چگونه قصه من در جهان سمر نشود؟</p></div>
<div class="m2"><p>که هرکجا که نشینم بدین فسانه درم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بهر خدمتی عید خود همین قصه ست</p></div>
<div class="m2"><p>که من به نزد جهان پهلوان به قصه برم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملک نشان عضدالدین که از مدایح او</p></div>
<div class="m2"><p>همیشه بر سر گنج جواهر و دررم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طغانشه بن مؤید که گوید و رسدش:</p></div>
<div class="m2"><p>که هست منطقه چرخ حلقه کمرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من آن تهمتن دریا دلم که وقت صبوح</p></div>
<div class="m2"><p>بود ذخیره کانها عطای مختصرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سها چو برق زند گوهری ست بر تیغم</p></div>
<div class="m2"><p>قمر چو نور دهد قبه ای ست بر سپرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان مقرّ شد و ایام اعتراف آورد</p></div>
<div class="m2"><p>که من خلاصه تایید و مایه ظفرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>منم که بر رخ گیتی چو روز مشهور ست</p></div>
<div class="m2"><p>همه فضایل جد و مناقب پدرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر سپهر بپوشد ز رای من رازی</p></div>
<div class="m2"><p>چو جیب صبح همه پرده های او بدرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بیفکنند پر و بال کرکسان فلک</p></div>
<div class="m2"><p>هر آن زمان که ببینند تیر چار پرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به پیش من صف دشمن چگونه دارد پای؟</p></div>
<div class="m2"><p>که لحظه لحظه ز افبال می رسد حشرم ؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو عون و عصمت ایزد مرا سپر باشد</p></div>
<div class="m2"><p>ز زخم حادثه حاجت نیوفتد حذرم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز حرص زر چوشهان نام نیک بفروشند</p></div>
<div class="m2"><p>منم که ملک جهان را به نیم جو نخرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به پیش من ز تواضع به ساعتی صدره</p></div>
<div class="m2"><p>زمانه خاک شود تا مگر برو سپرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرآنچه گویم ازین جنس لاف و دعوی نیست</p></div>
<div class="m2"><p>که هست فر الهی گواه معتبرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خدایگانا هر چند زحمتت باشد</p></div>
<div class="m2"><p>ز حال و قصه خود چند حرف بر شمرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گمان نبود مرا پیش ازین که باقی عمر</p></div>
<div class="m2"><p>بود ز خاک جناب تو حاجت سفرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کنون زمانه برآنست کز غبار درت</p></div>
<div class="m2"><p>کند گسسته به کلی وظایف بصرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز نان برآمدم اکنون و روی آن دارد</p></div>
<div class="m2"><p>که گر نطق بزنم تا به جان بود خطرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر ضرورت ازین سان نگیردم دامن</p></div>
<div class="m2"><p>چگونه دل دهدم کز در تو درگذرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به آرزو طلبیدم همیشه خدمت تو</p></div>
<div class="m2"><p>روا مدار کزین آرزو رسد ضررم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا به چربک صاحب غرض زبیخ مکن</p></div>
<div class="m2"><p>که من به باغ فصاحت درخت بارورم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز جوی لطف و کرم آب ده مرا و ببین</p></div>
<div class="m2"><p>که عاقبت تو چه برها خوری ز بار و برم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز من ملوک جهان نام نیک زنده کنند</p></div>
<div class="m2"><p>به قول مرده دلان بر میان مزن بترم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا تو با همه عیبی خریده ای مفروش</p></div>
<div class="m2"><p>که چون به کوی حقیقت روی همه هنرم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر به چیز دگر سرافرازیم نرسد</p></div>
<div class="m2"><p>همین بس است که بر آستان توست سرم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به حضرت تو من از بهر نان نیامده ام</p></div>
<div class="m2"><p>که جایگاه دگر نیز بود این قدرم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مبر به پیش خود آب رویم از پس ازین</p></div>
<div class="m2"><p>حدیث نان به زبان آورم ز سگ بترم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو بر بخور ز جوانی و پادشاهی خویش</p></div>
<div class="m2"><p>که من به دولت تو زهر چون شکر بخورم</p></div></div>