---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>چون بر زمین طلیعۀ شب گشت آشکار</p></div>
<div class="m2"><p>آفاق ساخت کسوت عباسیان شعار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیدا شد از کرانۀ میدان آسمان</p></div>
<div class="m2"><p>شکل هلال چون سر چوگان شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم ز زر پخته برین لوح لاجورد</p></div>
<div class="m2"><p>نونی که گفتیی به قلم کرده شد نگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی فلک چو لجّه دریا و ماه نو</p></div>
<div class="m2"><p>مانند کشتیی که ز دریا کند کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا بر مثال ماهی یونس میان آب</p></div>
<div class="m2"><p>آهنگ در کشیدن او کرده از کنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا همچو یونس آمده بیرون ز بطن حوت</p></div>
<div class="m2"><p>افتاده بر کنارۀ دریا نحیف و زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در معرض خلاف جهانی ز مرد و زن</p></div>
<div class="m2"><p>قومیش در نظاره و خلقی در انتظار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من با خرد به حجره خلوت شتافتم</p></div>
<div class="m2"><p>گفتم که ای نتیجه الطاف کردگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز این چه نقش بوالعجب و نادرست شکل</p></div>
<div class="m2"><p>کز کارگاه غیب همی گردد آشکار؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن شاهد از کجاست که این چرخ شوخ چشم</p></div>
<div class="m2"><p>از گوش او برون کند این نغز گوشوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردون ز بازوی که بدزدید این طراز</p></div>
<div class="m2"><p>گیتی ز ساعد که ربوده ست این سوار؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر جرم کوکبست چرا شد چنین دو تاه</p></div>
<div class="m2"><p>ور پیکر مه است چرا شد چنین نزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت آنچه بر شمردی از ین هیچ نیست</p></div>
<div class="m2"><p>دانی که چیست ؟ با تو بگویم به اختصار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نعل سمند شاه جهان است کاسمان</p></div>
<div class="m2"><p>هر ماه بر سرش نهد از بهر افتخار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم که از مدائح ذات مبارکش</p></div>
<div class="m2"><p>رمزی بگوی تا بودم از تو یادگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر عادت کریمان بر دامنم نهاد</p></div>
<div class="m2"><p>درجی چنین که بینی پر درّ شاهوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا من ز بر تهنیت عید بی دریغ</p></div>
<div class="m2"><p>بر آستان خسرو عادل کنم نثار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاه جهان اتابک اعظم که درگهش</p></div>
<div class="m2"><p>اسلام را ز حادثه خصنی است استوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بوبکر بن محمد بن الدگز که هست</p></div>
<div class="m2"><p>چون آفتاب قاهر و چون چرخ کامگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن بحر مکرمت که ز امداد فیض او</p></div>
<div class="m2"><p>دایم غریق نعمت و امن است روزگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وان قطب معدلت که سپهر و ستاره را</p></div>
<div class="m2"><p>هموراه گرد مرکز حکمش بود مدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون مشتبه شود جهت کعبۀ نجات</p></div>
<div class="m2"><p>جز صوب درگهش نکند عقل اختیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنرا که فر تربیت او عزیز کرد</p></div>
<div class="m2"><p>اجرام آسمان نتوانند کرد خوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وانرا که از حدیقۀ لطفش گلی شکفت</p></div>
<div class="m2"><p>دوران روزگار نیارد نهاد خار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای خسروی که رای تو از روی ملک و دین</p></div>
<div class="m2"><p>هر دم به آستین کرم بسترد غبار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنکس که یکدم از می عصیانت مست شد</p></div>
<div class="m2"><p>تا نفخ صور نشکندش سورت خار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بفشار پای حزم که پیش از تو کس نشد</p></div>
<div class="m2"><p>بر ابلق زمانه بدین چابکی سوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گیتی به نزد جور تو خاکی است بی محل</p></div>
<div class="m2"><p>خورشید پیش رای تو نقدی است کم عیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگشای دست حکم که کس را نیوفتاد</p></div>
<div class="m2"><p>در مرغزار ملک بدین فر بهی شکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیش از طلوع کوکب عدل تو آسمان</p></div>
<div class="m2"><p>هرگز یمین منطقه نشناخت از یسار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در سلک دهر بود شبه همبر گهر</p></div>
<div class="m2"><p>در باغ چرخ بود کدو همسر چنار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زان لحظه باز کار جهان انتظام یافت</p></div>
<div class="m2"><p>کاندر پناه جاه تو آمد به زینهار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا روزگار خطبه اقبال تو نخواند</p></div>
<div class="m2"><p>ممکن نبود عالم شوریده را قرار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در حسب حال خود سخنی چند داشتم</p></div>
<div class="m2"><p>لیکن برین یکی کلمه کردم اختصار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کای افتاب عدل ز من نور وامگیر</p></div>
<div class="m2"><p>وی سایه خدای زمن سایه بر مدار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا از برای نظم مصالح در این جهان</p></div>
<div class="m2"><p>کس را درون پردۀ تقدیر نیست بار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دوران دولت تو که نظم جهان از اوست</p></div>
<div class="m2"><p>بادا چو نظم من ابدالدهر پایدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ملک تو همچو نعمت فردوس بی زوال</p></div>
<div class="m2"><p>عمر تو همچو مّدت افلاک بی شمار</p></div></div>