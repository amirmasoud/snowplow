---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>دوش در وقت آنک ضلّ زمین</p></div>
<div class="m2"><p>کرد بر موکب شعاع کمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست گفتی مظلّه ای ست سیاه</p></div>
<div class="m2"><p>سر برافراخته به چرخ برین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم اطراف ربع مسکون را</p></div>
<div class="m2"><p>از سیاهی چو کلبه مسکین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان چون زمین مجلس شاه</p></div>
<div class="m2"><p>جلوه گاه جمال حورالعین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدح می درو سُکرَّه ماه</p></div>
<div class="m2"><p>طبق نقل خوشه پروین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا بکردار رقعه شطرنج</p></div>
<div class="m2"><p>روی در روی کرده تاج و معین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست چون پیش شاه رخ به عری</p></div>
<div class="m2"><p>پیش تیر شهاب دیو لعین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسر واقع بعینه گفتی</p></div>
<div class="m2"><p>دو پیاده ست بند یک فرزین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من زفکرت فکنده سر در پیش</p></div>
<div class="m2"><p>بر گرفته سخن ز علیین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با خرد بر طریق استدلال</p></div>
<div class="m2"><p>بحث می کردم از علوم یقین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه می گفتم از یکی مبدع</p></div>
<div class="m2"><p>چند ابداع می کنی تعیین؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور چو مبدع یکی نهی ابداع</p></div>
<div class="m2"><p>صورت مبدعات نیست چنین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاه تربیت آفرینش را</p></div>
<div class="m2"><p>بر طریق تماین و تبیین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حداحیان دهر می جستم</p></div>
<div class="m2"><p>خالی از نسبت شهور و سنین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنین منهی خرد می کرد</p></div>
<div class="m2"><p>به نکو تر عبارتی تلقین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شمه ای از حقایق اکوان</p></div>
<div class="m2"><p>نکته ای از دقایق تکوین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا به وقتی که دست صبح گشاد</p></div>
<div class="m2"><p>از فلک عقدهای دُر ثمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برکشید آفتاب رایت نور</p></div>
<div class="m2"><p>تا دهد جرم خاک را تزیین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وز دگر سوی نیز دلبر من</p></div>
<div class="m2"><p>بر گرفت آن زمان سر از بالین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به تعجب نگاه می کردم</p></div>
<div class="m2"><p>از فروغ رخ وصفای جبین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ذره ای ز آفتاب فرق نداشت</p></div>
<div class="m2"><p>ماه من جز به فرق مشک آگین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیکن از بس غبار محنت و رنج</p></div>
<div class="m2"><p>که نیابد به عمرها تسکین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در میان دو آفتاب مرا</p></div>
<div class="m2"><p>گشت تاریک چشم عالم بین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هم در آن لحظه صورت اقبال</p></div>
<div class="m2"><p>به زبان فصیح و لفظ متین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت بر خاک سُدّه ای که ازوست</p></div>
<div class="m2"><p>سدره مانند خاک بی تمکین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خیز و یکدم چنانک من همه عمر</p></div>
<div class="m2"><p>بر طریق ملازمت بنشین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا ز برج شرف طلوع کند</p></div>
<div class="m2"><p>طلعت آفتاب روی زمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خواجه روزگار،صدر جهان</p></div>
<div class="m2"><p>شرف ملک،تاج دولت و دین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنک خورشید مهره بر چیند</p></div>
<div class="m2"><p>گر در ابروی او بیند چین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وانک گردون لگام باز کشد</p></div>
<div class="m2"><p>چون کند موکب عزیمت زین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>امن آوارگان گردون را</p></div>
<div class="m2"><p>سد اقبال اوست حصن حصین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دست افتادگان حادثه را</p></div>
<div class="m2"><p>دامن جاه اوست حبل متین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آز بر خوان بی نیازی او</p></div>
<div class="m2"><p>شکم آگند پر ز غث و سمین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کبک در عهد کامرانی او</p></div>
<div class="m2"><p>کین صد ساله خواست از شاهین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای به رتبت غبار موکب تو</p></div>
<div class="m2"><p>بسته میدان چرخ را آذین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وی ز شکرت دهان اصل هنر</p></div>
<div class="m2"><p>گشته چون کام نیشکر شیرین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم ترازوی چرخ را بشکست</p></div>
<div class="m2"><p>بارحلم تو پله و شاهین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم درختان بید بفکندند</p></div>
<div class="m2"><p>پیش قهر تو بیلک و زوبین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چرخ انگشتری صفت نامت</p></div>
<div class="m2"><p>کرده بر دیده نقش همچو نگین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>باز نقش مخالفت کم شد</p></div>
<div class="m2"><p>از جهان همچو صورت تنوین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از نسیم شمایلت پیوست</p></div>
<div class="m2"><p>درخوی خجلت است آهوی چین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وز سموم سیاستت دایم</p></div>
<div class="m2"><p>در تب محرقه ست شیر عرین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا ز نسرین و گل نشان آرند</p></div>
<div class="m2"><p>مجلست باد پر گل و نسرین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا یمین و یسار بشناسند</p></div>
<div class="m2"><p>بادت اقبال بر یسار و یمین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بخت در مجلست حریف و ندیم</p></div>
<div class="m2"><p>چرخ بر درگهت رهی و رهین</p></div></div>