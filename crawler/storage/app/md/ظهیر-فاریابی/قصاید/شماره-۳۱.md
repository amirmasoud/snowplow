---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>شرح غم تو لذت شادی به جان دهد</p></div>
<div class="m2"><p>شکر لب تو طعم شکر وادهان دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاوس جان به جلوه درآید زخرمی</p></div>
<div class="m2"><p>گر طوطی لبت به حدیثی زبان دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمعی ست چهره تو که هر شب ز نور خویش</p></div>
<div class="m2"><p>پروانه عطا به مه آسمان دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقی ز پرتو تو چو پروانه سو ختند</p></div>
<div class="m2"><p>کس نیست کز حقیقت رویت نشان دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلفت به جادوی ببرد هر کجا دلی ست</p></div>
<div class="m2"><p>وانگه به چشم و ابروی نامهربان دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هندو ندیده ام که چو ترکان جنگ جوی</p></div>
<div class="m2"><p>هرچه آیدش بدست به تیر و کمان دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز زلف و چهره تو ندیدم که هیچکس</p></div>
<div class="m2"><p>خورشید را زظلمت شب سایه بان دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقبل کسی بود که چو خورشید عارضت</p></div>
<div class="m2"><p>هجرانش تا به سایه زلفت امان دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر در رخم بخندی بر من منه سپاس</p></div>
<div class="m2"><p>کان خاصیت همی رخ چون زعفران دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماییم و آب دیده که سقای کوی دوست</p></div>
<div class="m2"><p>ده مشک از این متاع به یک تای نان دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت است اگر لب تو به عهد مزوّری</p></div>
<div class="m2"><p>بیمار عشق را شکر و ناردان دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و آن بخت کو که عاشق رنجور قوتی</p></div>
<div class="m2"><p>با این دل ضعیف و تن ناتوان دهد؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان طاقت از کجا که صدایی ز درد دل</p></div>
<div class="m2"><p>در بارگاه خسرو خسرو نشان دهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فریاد من ز طارم گردون گذشت و نیست</p></div>
<div class="m2"><p>امکان آنک ز حمت آن استان دهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه کرسی فلک نهد اندیشه زیر پای</p></div>
<div class="m2"><p>تا بوسه بر رکاب قزل ارسلان دهد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در موضعی که چون دم روح القدس ز باد</p></div>
<div class="m2"><p>نصرت همای رایت او را روان دهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیغش ز کله سر بی مغز دشمنان</p></div>
<div class="m2"><p>نسرین چرخ را چو همای استخوان دهد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در برگریز عمر عدو صرصر اجل</p></div>
<div class="m2"><p>نوروز را طبیعت فصل خزان دهد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اطراف باغ معرکه را تیغ آب رنگ</p></div>
<div class="m2"><p>از خون کشته رنگ گل ارغوان دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تر دامنی دشمنش از روی خاصیت</p></div>
<div class="m2"><p>رنگ از برون جوشن و برگستوان دهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>راه نجات بسته شود بر زمین چنانک</p></div>
<div class="m2"><p>مرگ از حذر نشان به ره کهکشان دهد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر سرگرانیی که کند خصم او به عمر</p></div>
<div class="m2"><p>بازوش وقت حمله به گرز گران دهد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای خسروی که حفظ تو از راه اهتمام</p></div>
<div class="m2"><p>گوگرد را زصولت آتش امان دهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر جا که رایت از در تدبیر در شود</p></div>
<div class="m2"><p>تقدیر بر وساده حکمش مکان دهد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیرند چرخ واختر وبخت تو نو جوان</p></div>
<div class="m2"><p>آن به که پیر دولت خود با جوان دهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرّ همای سلطنت آن را بود به حق</p></div>
<div class="m2"><p>کش حکم تو به سایه چتر آشیان دهد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرآهنی که بر سر چوبی کنند راست</p></div>
<div class="m2"><p>چون رُمح تو چگونه قرار جهان دهد ؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اعجاز موسوی نبود هرکجا کسی</p></div>
<div class="m2"><p>چوبی شعیب وار به دست شبان دهد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صد قرن بر جهان گذرد تا زمام ملک</p></div>
<div class="m2"><p>اقبال در کف چو تو صاحب قران دهد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در رزم رستمی تو و در بزم حاتمی</p></div>
<div class="m2"><p>گردون تو را عنان و قدح بهر آن دهد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با بحر برزنی چو به پیشت قدح نهد</p></div>
<div class="m2"><p>وز مهر کین کشی چو به دستت عنان دهد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر کو چو تیغ با تو زبان آوری کند</p></div>
<div class="m2"><p>قهرت جواب او به زبان سنان دهد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در گرد بارگاه تو کیوان شب یتاق</p></div>
<div class="m2"><p>تا روز بوسه بر قدم پاسبان دهد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شاها خلایق از تو عزیز و توانگرند</p></div>
<div class="m2"><p>درویشیم سزد که به دست هوان دهد؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پوشیده زُهره جامه زربفت و مشتری</p></div>
<div class="m2"><p>محتاج خرقه ای است که بر طلیلسان دهد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در عهد چون تو شاهی کز فضله سخات</p></div>
<div class="m2"><p>هر روز چرخ راتب دریا و کان دهد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاید که بعد خدمت یکساله در عراق</p></div>
<div class="m2"><p>نانم هنوز خسرو مازندران دهد؟!</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا آسمان چو کسوت شب را رفو کند</p></div>
<div class="m2"><p>گاه از شهاب سوزن و گه ریسمان دهد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بادی چنانک کسوت عمر تو را قضا</p></div>
<div class="m2"><p>یک سر طراز مملکت جاودان دهد</p></div></div>