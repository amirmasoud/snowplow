---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>چو زهره وقت صبوح از افق بسازد چنگ</p></div>
<div class="m2"><p>زمانه تیز کند ناله مرا آهنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفای چرخ بگیرد مرا به سختی نای</p></div>
<div class="m2"><p>وفای یار در آویزدم ز دامن چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برد زمانه ناساز از سرم بیرون</p></div>
<div class="m2"><p>هوای ناله نای و نوای زخمه چنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان به درد دل از سینه برکشم آهی</p></div>
<div class="m2"><p>که هفت آینه چرخ از آن برآرد زنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بضاعت سخن خویش بینم از خواری</p></div>
<div class="m2"><p>بسان آینه چین میان رسته زنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از خجالت و حیرت فتاده در کنجی</p></div>
<div class="m2"><p>که کس نشان ندهد نام دانش و فرهنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی چو عهد لئیمان نطاق صبرم سست</p></div>
<div class="m2"><p>گهی چو عذر بخیلان براق عزمم لنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابای شعر مرا نیز چاشنی مطلب</p></div>
<div class="m2"><p>که در مذاق زمانه یکیست شهد و شرنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فتاده ام به گروهی که در بیان شان هست</p></div>
<div class="m2"><p>مساق لفظ رکیک و مجال معنی تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قول نیک چو من نامشان برآرم زود</p></div>
<div class="m2"><p>به فعل بد سخنم را فرو برند به ننگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجاست رکن بساط خدایگان تا من</p></div>
<div class="m2"><p>برم چو شعری ارکان شعر بر خر چنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پیش خسرو روی زمین برآرم بانگ</p></div>
<div class="m2"><p>چنانک در خم گردون فتد غریو و غرنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدایگان سلاطین بحر و بر طغرل</p></div>
<div class="m2"><p>که در ترازوی جودش جهان ندارد سنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گرد مرکز چترش مدار هفت اقلیم</p></div>
<div class="m2"><p>چو گرد قطب شمالی مدار هفت اورنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز عدل شامل او بوی آن همی آید</p></div>
<div class="m2"><p>که در کمین گه شیران کنام سازد رنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ایا شهی که بریزد زیاد حمله تو</p></div>
<div class="m2"><p>به روز معرکه دندان پیل و کام نهنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تویی که خوشه پروین برین رواق بلند</p></div>
<div class="m2"><p>زبهر نقل جلال تو بسته اند آونگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مثال بزم تو پرداخت نقش بند ازل</p></div>
<div class="m2"><p>هنوز نازده نقش وجود را نیرنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان به دور تو کار زمانه منظوم است</p></div>
<div class="m2"><p>که پوست از سرزین باز شد به پشت پلنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر چو آتش و آبست دولت چه عجب</p></div>
<div class="m2"><p>که آمده ست برون از میان آهن و سنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آن زمان که اجل دشمنان جاه تو را</p></div>
<div class="m2"><p>شود مخالف آمال در شتاب و درنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان موافقت افتد سلاح را که کند</p></div>
<div class="m2"><p>زه گوزن زبان در دهان تیر خدنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بیلک تو به دنبال چشم کرد نگاه</p></div>
<div class="m2"><p>کمان به گوشه ابرو درآورد آژنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنان شود که ز تیری این و تندی آن</p></div>
<div class="m2"><p>قضا کرانه کند از میان به صد فرسنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کند سنان تو بازی به جان خصم چنانک</p></div>
<div class="m2"><p>به عقل دلشدگان شاهدان چابک وشنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قیامت است ز تیغ تو در ممالک روم</p></div>
<div class="m2"><p>مصیبت است ز گرز تو در دیار فرنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه تا به تجارت ز مرو شهجان کس</p></div>
<div class="m2"><p>به سوی آمل و ساری نیاورد نارنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رخ عدوت چو نارنگ زرد و آژده باد</p></div>
<div class="m2"><p>به سوزنی که نه زاتش گدازد و نه ز زنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برات بخشش تو بر وجوه عامل مرو</p></div>
<div class="m2"><p>معاش دشمنت از نقد قاضی کیرنگ</p></div></div>