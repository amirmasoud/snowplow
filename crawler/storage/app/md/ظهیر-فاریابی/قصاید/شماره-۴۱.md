---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گهی که بار دهد شاه بر سریر سرور</p></div>
<div class="m2"><p>که باد تا به قیامت به عهد او معمور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهر مجمره گردان بود به پایه تخت</p></div>
<div class="m2"><p>شمال مروحه بر دارد از برای بخور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشام چرخ معطر شود ز نکهت عود</p></div>
<div class="m2"><p>بخور عطر معطر کند دماغ طیور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فیض پرتو تاج مرصع خسرو</p></div>
<div class="m2"><p>بر آسمان چهارم رسد ز شعشعه نور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستاره بر سر مجمر فتد به جای سپند</p></div>
<div class="m2"><p>به دفع دیده خورشید هرزه گرد و غیور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون کنند در آن بزم حوریان بهشت</p></div>
<div class="m2"><p>سر از برای دعا از دریچه های قصور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیش بارگه کبریا ی شاه جهان</p></div>
<div class="m2"><p>چو صف کشند به خدمت عساکر منصور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلرزد از نفس چاوشان در گاه باد</p></div>
<div class="m2"><p>چهار حد وجود از صدای نفخه صور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنانک دور نباشد که او صوامع خاک</p></div>
<div class="m2"><p>مجاوران عدم سر نهند سوی نشور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن زمانه بقا سر در آورد به فنا</p></div>
<div class="m2"><p>در آن میانه فلک معترف شود به قصور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود به روم ز غم رعشه بر دل قیصر</p></div>
<div class="m2"><p>فتد زخوف به چین لرزه بر تن فغفور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز ترس بفسرد اندر عروق حادثه خون</p></div>
<div class="m2"><p>ز غم بپژمرد اندر دماغ فتنه غرور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدایگانا گر ز انک پیش از ین یک چند</p></div>
<div class="m2"><p>قضا به قدرت کردار خویش شد مغرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فتور و فتنه و تشویش متفق بودند</p></div>
<div class="m2"><p>کنون به عهد تو از یکدگر شدند نفور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به دام زلف بتان پای بسته شد تشویش</p></div>
<div class="m2"><p>به سوی چشم خوش دلبران گریخت فتور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون که کار خراب زمانه گشت آباد</p></div>
<div class="m2"><p>کنون که روی زمین شد به عدل تو معمور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بقای تخت تو بادا که بخت اهل هنر</p></div>
<div class="m2"><p>به سعی تربیت توست در جهان مشهور</p></div></div>