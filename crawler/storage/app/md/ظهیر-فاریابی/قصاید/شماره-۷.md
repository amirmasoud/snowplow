---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>رویت از حسن در جهان سمر است</p></div>
<div class="m2"><p>عقد زلفت نشیمن قمر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان رخ تازه ولب شیرین</p></div>
<div class="m2"><p>همه آفاق پر گل وشکر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دلم زان گل و شکر بچشید</p></div>
<div class="m2"><p>هر زمان از قضا ضعیف تر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگ روزی دلا که روزی او</p></div>
<div class="m2"><p>به دهان تو و لب تو در است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر در عشق تو به سر بردم</p></div>
<div class="m2"><p>دل ز حسرت هنوز تا به سر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی از دست عشق جان نبری</p></div>
<div class="m2"><p>الحق این خود بشارتی دگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن قضا را نهاده ام چکنم ؟!</p></div>
<div class="m2"><p>که نه بیدار تو همین قدر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در فراق تو هرکجا که دلی است</p></div>
<div class="m2"><p>تا به گردن در آتش جگر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقد رایج به رسته غم تو</p></div>
<div class="m2"><p>اشک چون سیم وچهره چو زر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقان را بهینه دستاویز</p></div>
<div class="m2"><p>آه شبگیر وناله سحر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با غمت دست در کمر کردم</p></div>
<div class="m2"><p>زان دو دستم همیشه در کمر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی من در غمت چو دامن ابر</p></div>
<div class="m2"><p>دایم از موج آب دیده تر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم من در فراق چهره تو</p></div>
<div class="m2"><p>کان یاقوت ومعدن گهر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راست گویی که در افاضت جود</p></div>
<div class="m2"><p>دست دربار شاه دادگر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاه عادل اتابک اعظم</p></div>
<div class="m2"><p>که جهان با عطاش مختصر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنک نزدیک سمع مظلومان</p></div>
<div class="m2"><p>نام او همچو مژده ظفر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وانک در نسبت جهات کمال</p></div>
<div class="m2"><p>آسمان زیرو قدر او زبر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صیت اقبال او بگرد جهان</p></div>
<div class="m2"><p>روز وشب همچو ماه در سفر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ظلمت ظلم را اشارت او</p></div>
<div class="m2"><p>چون تباشیر صبح پرده در است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای که خلوت سرای قدرت را</p></div>
<div class="m2"><p>چرخ چون حلقه از برون در است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست رایی برون پرده غیبت</p></div>
<div class="m2"><p>که نه رای تو را از آن خبر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سعی تیغ تو در معونت خلق</p></div>
<div class="m2"><p>چون مقامات دِرّه عمر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاک درگاه تو به حکم شرف</p></div>
<div class="m2"><p>افسر صد هزار تاجوَر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن همایست همتت که مقیم</p></div>
<div class="m2"><p>بیضه آسمانش زیر پر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر کجا موکب تو نهضت کرد</p></div>
<div class="m2"><p>بخت چون بندگانش بر اثر است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آتش فهر توست آنک به حجم</p></div>
<div class="m2"><p>هفت دوزخ به جنب او شرر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فیض احسان توست آنک به قدر</p></div>
<div class="m2"><p>هفت دریا به نزد او شمر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نظر همّت تو را هر شب</p></div>
<div class="m2"><p>برتُتُقهای آسمان گذر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مدتی شد که بر امید قبول</p></div>
<div class="m2"><p>بنده در انتظار آن نظر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شهریارا تو منگر آن کامروز</p></div>
<div class="m2"><p>شعر من در زمانه مشتهر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این نگه کن که نزد دانش من</p></div>
<div class="m2"><p>شعر عیبست اگر چه هم هنر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا در ادراک چشم پیکر ماه</p></div>
<div class="m2"><p>گاه چون نعل وگاه چون سپر است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون سپر باد پشت جاهت پهن</p></div>
<div class="m2"><p>که حسودت چو نعل پی سپر است</p></div></div>