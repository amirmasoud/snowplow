---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>حلقه زلف یار ، دام بلاست</p></div>
<div class="m2"><p>دل درو بسته ایم عین خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار دل بهتر است کو شب و روز</p></div>
<div class="m2"><p>در تماشا گه نسیم صباست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان بر لب رسیده را بتر است</p></div>
<div class="m2"><p>کز مقیمان آستان عناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بُت من به دلبری بنشست</p></div>
<div class="m2"><p>قلم عافیت زما برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارها گفتمش که کسوت عشق</p></div>
<div class="m2"><p>برقد هر کسی نیاید راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست در خصل می کنی هش دار</p></div>
<div class="m2"><p>مهره در ششدر و حریف دغاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه معهود آسمان، ستم است</p></div>
<div class="m2"><p>ورچه آیین روزگار، جفاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم شوخش که روزگار وَش است</p></div>
<div class="m2"><p>خط سبزش که آسمان آساست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جفا و ستم چنان شده اند</p></div>
<div class="m2"><p>کانچه ایشان کنند عدل و وفاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جور ایشان زحد گذشت کنون</p></div>
<div class="m2"><p>نوبت عدل سیدالرؤساست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدر عالی بهاء دین بوبکر</p></div>
<div class="m2"><p>که از او ملک را هزار بهاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنک در پیش فیض احسانش</p></div>
<div class="m2"><p>از خجل ماندگان یکی دریاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وانک بر آستان میمونش</p></div>
<div class="m2"><p>از کمر بستگان یکی جوزاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسند قدر و کامرانی اوست</p></div>
<div class="m2"><p>که زبر دست قبّه خضراست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش خورشید همتش خورشید</p></div>
<div class="m2"><p>از تحیّر چو دیده حرباست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرخ را امتثال فرمانش</p></div>
<div class="m2"><p>در بد و نیک مقصد اقصاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همت اوست عالمی که درو</p></div>
<div class="m2"><p>هر دو عالم چو ذرّه ناپیداست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای خضر سیرتی که همچو کلیم</p></div>
<div class="m2"><p>در معانی تو را ید بیضاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از نسیم صبای دولت تو</p></div>
<div class="m2"><p>گلبن مَکرُمت به نشو و نماست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر زبان قضا فرو بندد</p></div>
<div class="m2"><p>نوک کلک تو ترجمان قضاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور کمین فنا گشاده شود</p></div>
<div class="m2"><p>دولتت راضمان دفع فناست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نام و آوازه مکارم تو</p></div>
<div class="m2"><p>در جهان همره صباح و مساست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فتنه در عهد باز ایوانت</p></div>
<div class="m2"><p>از اسیران چنگل عنقاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای فلک در هوای تو یکتا</p></div>
<div class="m2"><p>پشتم از بار منّت تو دو تا ست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مَکرُمَتها همی کنی بی آنک</p></div>
<div class="m2"><p>از منت هیچ التماسی جزاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من به مدحت زبان نداده هنوز</p></div>
<div class="m2"><p>کرمت عذر صد قصیده بخواست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نفرتی داشت خاطرم از شعر</p></div>
<div class="m2"><p>زانک این نقض منصب فضلاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غرضم مدحت تو بود ارنی</p></div>
<div class="m2"><p>شاعری از کجا و او ز کجاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من که خلوت سرای قدر تو را</p></div>
<div class="m2"><p>جان من در مقام او ادناست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون تفاخر کنم به علم از آنک</p></div>
<div class="m2"><p>نام من در جریده شعر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شعر در نفس خویش هم بد نیست</p></div>
<div class="m2"><p>ناله من ز خست شرکاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا اسیران دست حادثه را</p></div>
<div class="m2"><p>آسمان قبله ثنا و دعاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ورد خلقان دعای جان تو باد</p></div>
<div class="m2"><p>کاستان تو آسمان سخاست</p></div></div>