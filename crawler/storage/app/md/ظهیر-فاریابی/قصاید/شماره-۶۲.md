---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>شبی به خیمه ابداعیان کن فیکون</p></div>
<div class="m2"><p>حدیث حسن تو می رفت و الحدیث شجون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشان زلف و رخت یک به یک همی دادند</p></div>
<div class="m2"><p>که بند و حلقه آن چند و حیله این،چون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان نمود که گفتی به عکس می بینند</p></div>
<div class="m2"><p>مثال طلعت تو در سپهر آینه گون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن دو عارض دلجوی تو دوصد بی دل</p></div>
<div class="m2"><p>بر آن دو گیسوی مفتول تو دوصد مفتون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد چو رونق دیوانگان عشق تو دید</p></div>
<div class="m2"><p>به صد بهانه برآورد خویشتن به جنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم حکایت زنجیر زلف تو بشیند</p></div>
<div class="m2"><p>عقال عقل بیفکند والجنون فنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز ضعف تن و سوز دل از آن شب باز</p></div>
<div class="m2"><p>نه طاقت حرکت ماند و نه مجال سکون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عشق چشمه نوش تو اندرین مدت</p></div>
<div class="m2"><p>برفت بر رخم از آب دیدگان جیحون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوز آتش سودا همی زنم در دل</p></div>
<div class="m2"><p>هنوز دامن مژگان همی کشم در خون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سوز سینه من شعله ای و صد وامق</p></div>
<div class="m2"><p>ز جام محنت من جرعه ای و صد مجنون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون ز مستی من بیش ازین دو حرف نماند</p></div>
<div class="m2"><p>دلی چو چشمه میم و قدی چو حلقه نون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخ تو می نهد این نوع زخم را مرهم</p></div>
<div class="m2"><p>لب تو می دهد این جنس درد را معجون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر به مرهم و معجون علاج نپذیرد</p></div>
<div class="m2"><p>من و مدایح صاحب قران شرع کنون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدایگان صدور زمانه صدرالدین</p></div>
<div class="m2"><p>که قامت فلک از بار شکر اوست نگون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسی نماند که گردد ز بس عمارت عدل</p></div>
<div class="m2"><p>چهار ربع زمین در پناه او مسکون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز حفظ اوست که اجرام عالم علوی</p></div>
<div class="m2"><p>ز استحالت جوهر مسلمند و مصون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز شوق اوست که دوشیزگان قصر عدم</p></div>
<div class="m2"><p>سر از دریچه امکان همی کنند برون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی ضمیر تو هر شب به یک اشارت رای</p></div>
<div class="m2"><p>گشاده در تتق غیب روی صد خاتون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به رسم خدمتی اندر پی جنیبت تو</p></div>
<div class="m2"><p>فکنده دهر ز روز اطلس و ز شب اکسون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دست حکم تو اجرام آسمان عاجز</p></div>
<div class="m2"><p>به چنگ قهر تو احداث روزگار زبون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هوای طاعت توست آن نسیم جان پرور</p></div>
<div class="m2"><p>که از میانه آذر بروید آذریون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمین بغض تو آن تربت وبِّی وعَفِن</p></div>
<div class="m2"><p>که آورد طمع اندر هوای او طاعون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جنب گوشه دستار و رکن مسند تو</p></div>
<div class="m2"><p>چه جای افسر دارا و تخت افریدون؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به علم اگرچه قیاست ز انبیا گیرند</p></div>
<div class="m2"><p>به عقل نیز بهی از هزار افلاطون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>توراست معجزه سروری به استقلال</p></div>
<div class="m2"><p>نه چون نبوت موسی به شرکت هرون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آن سخن که تو گویی برای ضبط جهان</p></div>
<div class="m2"><p>هزار لشکر جرار باشدش مضمون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر چه حادثه یک شب به خواب امن و قرار</p></div>
<div class="m2"><p>نمی نهد مژه بر هم ز بس فتور و فتون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زمان زمان قلمت شربتیش آمیزد</p></div>
<div class="m2"><p>که در مجاری مغزش پراکند افیون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فلک ز عقد عمامه ات حسابها برداشت</p></div>
<div class="m2"><p>که حشو و بارز آفاق را تویی قانون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به مهر توست اگر قطره ای ست در دریا</p></div>
<div class="m2"><p>به داغ توست اگر ذره ای ست بر هامون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بزرگوارا بعد از هزار قرعه فال</p></div>
<div class="m2"><p>مرا زمانه به صدر تو بود راهنمون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دوسال شد که برین فرخ آستانه مرا</p></div>
<div class="m2"><p>شده ست دست تفکر به زیر روی ستون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان مکن که مرا با هزار گنج هنر</p></div>
<div class="m2"><p>به روزگار تو حاجت بود به مشتی دون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه به دعوی عصمت برآمده چو ملک</p></div>
<div class="m2"><p>ولیک بوده چو ابلیس در ازل ملعون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به فعل چون عثرات زمانه نامضبوط</p></div>
<div class="m2"><p>به طبع چون حرکات سپهر ناموزون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کشیده سر سوی گردون ز کبر چون نمرود</p></div>
<div class="m2"><p>فرو شده به زمین در ز بخل چون قارون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر متابع ایشان بود فلک چه عجب؟</p></div>
<div class="m2"><p>بجز متابعت گاو کی کند گردون؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>منم که پار درین روز،هم درین مجلس</p></div>
<div class="m2"><p>همین تظلم و فریاد کرده ام کاکنون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ولیک زین همه فریاد هیچ فایده نیست</p></div>
<div class="m2"><p>چو پیش می ننهد گام روزگار حرون</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جهان به کام تو بادا که جز درین معنی</p></div>
<div class="m2"><p>دعای من به اجابت نمی شود مقرون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>طلوع کوکبه عید بر تو میمون باد</p></div>
<div class="m2"><p>وهست طلعت تو بر جهانیان میمیون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مخالف تو چو بدر از خسوف در کم و کاست</p></div>
<div class="m2"><p>ولیک دولت تو چون هلال روزافزون</p></div></div>