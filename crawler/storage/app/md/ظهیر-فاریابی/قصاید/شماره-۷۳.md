---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>نباشدت نفسی در سر آن کله داری</p></div>
<div class="m2"><p>که سر به کلبه احزان فرود آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین قدر دل ما هم نگه نخواهی داشت</p></div>
<div class="m2"><p>چه دلبری که ندانی طریق دلداری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حسن خویش بدین مایه گشته ای خرسند</p></div>
<div class="m2"><p>که سینه ای بخلی یا دلی بیازاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا که پشت من از بار محنت است دو تاه</p></div>
<div class="m2"><p>فراق روی تو در می خورد به سرباری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا ببین که ز بهر نثار مقدم تو</p></div>
<div class="m2"><p>دو چشم من ز چه سان می کند گهر باری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدانچه از رگ من خون چکد دریغی نیست</p></div>
<div class="m2"><p>که هرچه با تو کند جنس آن سزاواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تکلُّفی نبود لایق بزرگی تو</p></div>
<div class="m2"><p>اگر به خیره نگیری و عیب نشماری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخون دیده بر آنم که شربتی سازم</p></div>
<div class="m2"><p>که چشم شوخ تو را عادت است خونخواری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مُزَوِّرِ هوسی نیز می پزم حالی</p></div>
<div class="m2"><p>که در دو چشم تو پیداست ضعف بیماری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را به ناله زیرست میل و این عجب است</p></div>
<div class="m2"><p>که دست می نرسد جز به ناله و زاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز لطفها که تو با من کنی یکی اینست</p></div>
<div class="m2"><p>که یکزمانم بی این سماع نگذاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی غم از دل من پای باز پس ننهد</p></div>
<div class="m2"><p>که دست دست به دیگر غمیم نسپاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هر جفا که کنی بر زمانه بندی جرم</p></div>
<div class="m2"><p>کسی زفعل تو آگاه نیست پنداری!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عنان فتنه رها کرده ای و این خوشتر</p></div>
<div class="m2"><p>که عذر لنگ برون می بری به رهواری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمانه را همه دانند کو نیارد کرد</p></div>
<div class="m2"><p>به روزگار جهان پهلوان جفا کاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پناه ملت اسلام،فخر دولت و دین</p></div>
<div class="m2"><p>که کرد دولت و دین را به تیغ معماری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز چشم دولت او تا بجست خواب عدم</p></div>
<div class="m2"><p>دگر به خواب ندیده ست فتنه بیداری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دور او ز بس آثار عدل نتوان کرد</p></div>
<div class="m2"><p>مگر به زلف بتان نسبت ستمکاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایا رسیده به جایی که گر جهان نبود</p></div>
<div class="m2"><p>ز بهر همَّت خود قطره ای کم انگاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کلاه گوشه قدر تو از طریق نفاذ</p></div>
<div class="m2"><p>ربوده از سر گردون کلاه جبّاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فتاده جِرم زمین با همه ثبات قدم</p></div>
<div class="m2"><p>به جنب حلم تو در تهمت سبکساری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درآمده ز ازل زیر سقف همت تو</p></div>
<div class="m2"><p>چهار عنصر عالم به چار دیواری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز عصمت تو چنان تنگ شد فضای جهان</p></div>
<div class="m2"><p>که هست دم زدن دشمنت به دشواری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تویی که تا ابد از رنگ و بوی دولت تو</p></div>
<div class="m2"><p>چمن به رنگرزی شد صبا به عطاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دست ساقی لطف تو یک پیاله بود</p></div>
<div class="m2"><p>که نرگس افکند از دست جام هشیاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز صوت بلبل رفق تو یک نوا باشد</p></div>
<div class="m2"><p>که گل به پای در آرد لباس زنگاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به یک سخن دهن ظلم را فرو بندی</p></div>
<div class="m2"><p>به یک سخا شکم آز را بینباری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به قهر،آب فنا بر سر فلک ریزی</p></div>
<div class="m2"><p>به لطف،تخم وفا در دل جهان کاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زخار حادثه تا بشکفد گل انصاف</p></div>
<div class="m2"><p>به چشم خصم تو گل را مباد جز خاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو را ذخیره عمری که چون بقای ابد</p></div>
<div class="m2"><p>ورای عقد تصرف بود ز بسیاری</p></div></div>