---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>دل همی خواهد از آن پسته که شکّر گیرد</p></div>
<div class="m2"><p>جان طمع دارد از آن لعل که گوهر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسته تنگ تو از بهر علاج دل من</p></div>
<div class="m2"><p>ای بسا ورد شکفته که به شکّر گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی من از پی طرف کمرت هر لحظه</p></div>
<div class="m2"><p>ای بسا گوهر ناسفته که در زر گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان من وقت بخور سر مشکین زلفت</p></div>
<div class="m2"><p>از دل و سینه من مجمر و آذر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو تو بر ز سمن دارد و دل می خواهد</p></div>
<div class="m2"><p>که از آن سرو قدت برگ سمن بر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن من شد چو رسن زلف تو چنبر،چه شود</p></div>
<div class="m2"><p>که رسن باز دلم گوشه چنبر گیرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم هر روزه گرمم چو به تو در نگرفت</p></div>
<div class="m2"><p>آه هر صبحی سر دم به تو کی در گیرد ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه خواهد که سمن باردهد سرو او را</p></div>
<div class="m2"><p>پای یار چو تو سرو سمنی بر گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در رکاب غم تو دل به مرادی نرسد</p></div>
<div class="m2"><p>گر نه فتراک شهنشاه مظفر گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیر سرخ آنک اگر دست دهد آهو را</p></div>
<div class="m2"><p>از سر قوت دل پای غضنفر گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون سکندر شود آن روز که بر تخت شود</p></div>
<div class="m2"><p>آب حیوان کشد آنگاه که ساغر گیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای فلک قدر که گر از تو اجازت یابد</p></div>
<div class="m2"><p>نسر طائر سر تیر تو به شهپر گیرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخت از ین خیمه سر بافته سیم طناب</p></div>
<div class="m2"><p>بر سر فرق فلک سای تو افسر گیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماه از این بحر گرانمایه ناسفته دُرَر</p></div>
<div class="m2"><p>گردن ملک تو را حجله به زیور گیرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تویی آن شاه هنرمند که تیغ تو چو صبح</p></div>
<div class="m2"><p>ملک عالم به یکی ضربت خنجر گیرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک شَرر ز آتش خشم تو اگر چرخ اثیر</p></div>
<div class="m2"><p>پیش این گنبد گردنده اخضر گیرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلک از هیبت آن جنبش زیبق یابد</p></div>
<div class="m2"><p>اختر از شعله آن سوزش اخگر گیرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عنفت ار پای نهد دود ز دریا خیزد</p></div>
<div class="m2"><p>لطفت از دست کشد در ز سمندر گیرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر چه بیگانه بود مهر چو روی تو بدید</p></div>
<div class="m2"><p>نکند هیچ تکلف در خاور گیرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ورچه گمراه بود خصم که زخمت بخورد</p></div>
<div class="m2"><p>نکند هیچ توقف ره محشر گیرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لشکرت -حاطهم الله - چو پی خصم روند</p></div>
<div class="m2"><p>بخدا گر رهشان سدّ سکندر گیرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این شود رعد گه مشعله چون نعره زند</p></div>
<div class="m2"><p>وان شود برق گه حمله چو خنجر گیرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز نشان و اثر میخ سم مرکبشان</p></div>
<div class="m2"><p>چون فلک روی زمین صورت اختر گیرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شهریارا خبر باد قران می دادند</p></div>
<div class="m2"><p>که همه روی زمین زعزع و صر صر گیرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باد در عهد تو کی زهره آن داشت که او</p></div>
<div class="m2"><p>خاک پای تو نه چون تاج بسر بر گیرد ؟!</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرد از باد برانگیزی اگر فرمانت</p></div>
<div class="m2"><p>نه چو فرمان سلیمان پیمبر گیرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کامکارا چو ظهیر از شرف نظم لطیف</p></div>
<div class="m2"><p>به گه مدحت تو خامه و دفتر گیرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهر او دست زمان دفتر افلاک آرد</p></div>
<div class="m2"><p>پیش او تیر فلک خامه و محور گیرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیکن این هر دو مُسخّر شده فرمانت</p></div>
<div class="m2"><p>خوش نباشد که چو من ذره مسخر گیرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکجا دور فلک تیر جفا اندازد</p></div>
<div class="m2"><p>سپر سینه او دهر برابر دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا یقین باشد بر خلق که شیر و شمشیر</p></div>
<div class="m2"><p>خصم بی مر شکند آهوی بی مر گیرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تیغ قهر تو چنان باد که خاقان شکند</p></div>
<div class="m2"><p>شیر نام تو چنان باد که قیصر گیرد</p></div></div>