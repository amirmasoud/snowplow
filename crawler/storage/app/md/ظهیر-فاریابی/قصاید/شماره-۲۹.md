---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>نوبت ملکت شها بر هفت گردون می زنند</p></div>
<div class="m2"><p>ملک عالم را به تو فال فریدون می زنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ازل دایم زدند و تا ابد خواهند زد</p></div>
<div class="m2"><p>تا نپنداری شها کین نوبت اکنون می زنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوبت اول بهنگامی که در طشت افق</p></div>
<div class="m2"><p>تیره شب را جامه پنداری به صابون می زنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی غلط گفتم سحرگاهی که نقاشان صبح</p></div>
<div class="m2"><p>نقش تار پرنیان گویی بر اکسون می زنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان دوم نوبت نماز شام هنگام غروب</p></div>
<div class="m2"><p>کز شفق گویی هوا را جامه در خون می زنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان سوم نوبت بگاه آنک بالای زمین</p></div>
<div class="m2"><p>سایه بان نیلگون بر در مکنون می زنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نام جویان از شکوه رتبتی کان در شه ست</p></div>
<div class="m2"><p>طبل باز هیبت از بیم شبیخون می زنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا زشوق دولتت دانادلان روزگار</p></div>
<div class="m2"><p>طعنه در هر نوبتی صد نوبت افزون می زنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد همایون عهد تو عهدی که شاهان جهان</p></div>
<div class="m2"><p>لاف داد و دین از ین عهد همایون می زنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ربع مسکون از چه معمور آمد؟از جرم زمین</p></div>
<div class="m2"><p>زانک لشکرگاه تو بر ربع مسکون می زنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوه و هامون فخر دارد بر فلک تا در جهان</p></div>
<div class="m2"><p>بارگاه عالیت بر کوه و هامون می زنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هست اتابک اعظمی در مملکت میراث تو</p></div>
<div class="m2"><p>صورتش زیبد که بر طغرای میمون می زنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می بیادت با کرامت کرده مدغم می خورند</p></div>
<div class="m2"><p>زر به نامت با سعادت گشته مقرون می زنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسند رایت ز شاخ سدره بر تر می نهند</p></div>
<div class="m2"><p>خرگه قدرت زطاق چرخ بیرون می زنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا خبر در ملت از قول پیمبر می دهند</p></div>
<div class="m2"><p>تا مثل در حکمت از کتب فلاطون می زنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رسم این نوبت به رونق در جهان پاینده باد</p></div>
<div class="m2"><p>تا بدرگاه تو بر پیوسته موزون می زنند</p></div></div>