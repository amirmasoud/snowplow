---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>هرچه فرّ و جاه قدرست ای همایون بارگاه</p></div>
<div class="m2"><p>در حریم حضرتت جمع آمد از اقبال شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ازل چو نقش نیرنگ تو می زد نقش بند</p></div>
<div class="m2"><p>دولت اندر آستانت کرد خود را جایگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر فضای ساحت قدر تو گردون راست رشک</p></div>
<div class="m2"><p>در پناه کبریای توست گردون را پناه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیر شادروانت از ثور و حمل گیرد شکار</p></div>
<div class="m2"><p>آهوی ایوانت از خلد برین جوید گیاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه اندر سایه خورشید ایوانت گریخت</p></div>
<div class="m2"><p>ایمنست از خود فزون تر دارد از گردون گناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح و شام از خادمان خاص درگاه تواند</p></div>
<div class="m2"><p>از پی کاری ست آری این سپید و آن سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه گردون صد هزاران دیده دارد باک نیست</p></div>
<div class="m2"><p>از سر عزت نیارد کرد در رویت نگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که خاک درگهت را تاج سر سازد به طوع</p></div>
<div class="m2"><p>زیبدش کز روی نخوت بر فلک ساید کلاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیشگاهت گر دنان را داده تمکین وجود</p></div>
<div class="m2"><p>تا کنند از خاک درگاه تو تزیین جباه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ملوک هفت کشور بر درت حاضر شوند</p></div>
<div class="m2"><p>از مثال بارگاهت حشمت اند و زند و جاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و ر به رجعت با جهان آیند افریدون و جم</p></div>
<div class="m2"><p>پرده دارت ندهد ایشان را درون پرده راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر وضوح دعوی من آسمانت چاکرست</p></div>
<div class="m2"><p>گر گواه عدل خواهی عدل شاه آنک گواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این که می بوسند خاک درگهت را انس و جان</p></div>
<div class="m2"><p>از جلال توست گویی یا ز قدر پادشاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسرو خورشید فر کیخسرو گیتی ستان</p></div>
<div class="m2"><p>شاه کیوان قدر گردون منصب انجم سپاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>انک گر اسبش ز راه کهکشان آخور کند</p></div>
<div class="m2"><p>خوشه گندم شود در آخور خورشید کاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صدمه پاسش کزان سوی جهان صد میل رفت</p></div>
<div class="m2"><p>در دو چشم آفرینش کرد کحل انتباه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاد باش ای شاه حیدر رتبت بوبکر نام</p></div>
<div class="m2"><p>دیرمان ای خسرو دریا دل کان دستگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه در دولت رسیدی تو به جایی کز شرف</p></div>
<div class="m2"><p>درگهت را عرصه آفاق زیبد پیشگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باش کاین رتبت به نسبت با جلال قدر تو</p></div>
<div class="m2"><p>اول عهد از خروج یوسف است از قعر چاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا جهان بر پای باشد در جهان بر پای باش</p></div>
<div class="m2"><p>باده نوش و جام گیر و جان فزای و خصم کاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاد بنشین اندرین فرخنده اقبال آشیان</p></div>
<div class="m2"><p>نام جوی و کام یاب و عیش ساز و جام خواه</p></div></div>