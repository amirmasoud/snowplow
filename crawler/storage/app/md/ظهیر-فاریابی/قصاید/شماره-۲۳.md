---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>قصر هدی شد به سعی شاه مشیّد</p></div>
<div class="m2"><p>رایت اسلام سر کشید به فرقَد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه جهان شهریار عالم عادل</p></div>
<div class="m2"><p>خسرو غازی طغانشه بن موید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنک مرکب کند صواعق قهرش</p></div>
<div class="m2"><p>خاصیت زهر در دماغ طبر زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانک نشیند بعون بازوی تیغش</p></div>
<div class="m2"><p>خنجر سوسن به جای تیغ مهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فزغ قهر و شدت غضب او</p></div>
<div class="m2"><p>در دل کان پاره های خون معقد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهره سنگ از نهیب او چو برآمد</p></div>
<div class="m2"><p>گردش چرخش لقب نهاد زمرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای به ترقی ورای چار عناصر</p></div>
<div class="m2"><p>جاه تو گسترده چار بالش و مسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رای تو در یک نظر مشاهده کرده</p></div>
<div class="m2"><p>نقش قضا و قدر ز تخته ابجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل که چو درّی ست در هوای تو صافی</p></div>
<div class="m2"><p>از کرمت سرخ روی گشت چو بسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دم سرد حسود تو به طبیعت</p></div>
<div class="m2"><p>جرم هوا بفسرد چو صرح مُمَرّد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منشی حکمت نعوذ بالله اگر هیچ</p></div>
<div class="m2"><p>بر ورق حال من کشد قلم رد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روز وجودم چو روزنامه خصمت</p></div>
<div class="m2"><p>گردد از احداث روزگار مسوّد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بمثل اره بر سرم نهد ای شاه</p></div>
<div class="m2"><p>گردش ایام چون حروف مشدد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست اجل تا که در نیاردم از پای</p></div>
<div class="m2"><p>در نکشم سر زخط مدح تو چون مد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه درین شعر یک دو قافیه ذالست</p></div>
<div class="m2"><p>نی غرض از شعر قافیه ست مجرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خاصه چو این جنس گفته اند بزرگان</p></div>
<div class="m2"><p>عذر من از راه اقتداست ممهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا عرق خد نیکوان بود از لطف</p></div>
<div class="m2"><p>راست چو بر برگ گل گلاب مصعد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همچو می از قطره های خون جگر باد</p></div>
<div class="m2"><p>خصم تو را از سموم غم ،عرق خد</p></div></div>