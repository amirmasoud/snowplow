---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>شاها درِ تو قبله شاهان عالم است</p></div>
<div class="m2"><p>گردون تو را مسحر و گیتی مسلم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصود آفرینش عالم تویی از آنک</p></div>
<div class="m2"><p>ذات مطهرت سبب نظم عالم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم چشم مهر و ماه به روی تو روشن است</p></div>
<div class="m2"><p>هم جان جن وانس به یاد تو خرم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم به توست زنده که تو جان عالمی</p></div>
<div class="m2"><p>زین غصه جان خصم تو موقوف یک دم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز نزاید از تو گرانمایه تر گهر</p></div>
<div class="m2"><p>زان آب و گل که مایه ترکیب آدم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مولد مسیح قدومت مبارک است</p></div>
<div class="m2"><p>چون سجدگاه خضر جنابت مکرم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرجا که از حوادث گردون جراحت است</p></div>
<div class="m2"><p>آنرا ز بِرّ و لطف تو صدگونه مرهم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنمود خنجر تو در احیای ملک و دین</p></div>
<div class="m2"><p>آن خاصیت که در دم عیسی مریم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دین مصطفی رمقی مانده بود بس</p></div>
<div class="m2"><p>امروز زنده کرده شاه معظّم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خسروی که قصه یکروزه رزم تو</p></div>
<div class="m2"><p>صد ساله کارنامه کاووس و رستم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنجا که نعت صورت خوبان رود ، تو را</p></div>
<div class="m2"><p>دل سوی قد نیزه و گیسوی پرچم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چندان بریخت خنجر تو خون دشمنان</p></div>
<div class="m2"><p>کاجزای خاک تا به ثری جمله در نم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فتح وظفر به جوهر تیغ تو قایمند</p></div>
<div class="m2"><p>نی نی که تیغ تو همه فتح مجسم است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوک سنانت بر ورق نصرت و ظفر</p></div>
<div class="m2"><p>حرفی است کاندرو همه آفاق مُدغَم است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرصد هزار عید و عروسی است خصم را</p></div>
<div class="m2"><p>با یک سیاست تو همه عین ماتم است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صد کاسه انگبین را یک ذرّه بس بود</p></div>
<div class="m2"><p>زان چاشنی که در بن دندان ارقم است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از روی قوت ارچه جوان است بخت تو</p></div>
<div class="m2"><p>برچرخ پیر از ره رتبت مقدم است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خصمت برای ملک بسی جهد کرد لیک</p></div>
<div class="m2"><p>توفیق،اصل معتبر وباب معظم است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر رای روشن تو چو خورشید ظاهرست</p></div>
<div class="m2"><p>گردر ضمیر چرخ یکی راز مبهم است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا چون شهاب با تو فلک دل نهاد راست</p></div>
<div class="m2"><p>همچون هلال قامت اعدات پر خم است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکتا شده ست رشته شاهی به عهد تو</p></div>
<div class="m2"><p>الحمدلله ارچه که یکتاست محکم است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خصم تو گر ز ذرّه فزون است در عدد</p></div>
<div class="m2"><p>با آفتاب تیغ تو از ذرّه ای کم است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون تو به کام خویش رسیدی از ین سپس</p></div>
<div class="m2"><p>گر خصم گرددت همه گیتی که را غم است؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برتخت ملک رفت سلیمان کنون چه باک؟</p></div>
<div class="m2"><p>گرصد هزار دیو طلبکارِ خاتم است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خرم نشین همیشه وبرخور ز مملکت</p></div>
<div class="m2"><p>کاسباب خرّمی همه پیشت فراهم است</p></div></div>