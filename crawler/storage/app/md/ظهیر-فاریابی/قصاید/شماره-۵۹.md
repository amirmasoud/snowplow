---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>گیتی ز فرّ دولت فرمانده جهان</p></div>
<div class="m2"><p>ماند به عرصه حرم و روضه جنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هر طرف که چشم نهی جلوه ظفر</p></div>
<div class="m2"><p>و ز هر جهت که گوش کنی مژده امان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرام یافت در حرم امن وحش و طیر</p></div>
<div class="m2"><p>و آسوده گشت در کنف عدل انس و جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون فرو گشاد کمند از میان تیغ</p></div>
<div class="m2"><p>و ایام بر گرفت زه از گردن کمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملکی چنین مقرر و حکمی چنین مطاع</p></div>
<div class="m2"><p>دیرست تا زمانه نداد از کسی نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منسوخ گشت قصه کاووس و کیقباد</p></div>
<div class="m2"><p>و افسانه شد حکایت دارا و اردوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بالید ازین نشاط تن تخت بر زمین</p></div>
<div class="m2"><p>بگذشت ازین نوید سر تاج از آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از غصه خون گرفت چو می ظلم را جگر</p></div>
<div class="m2"><p>وز خنده بازماند چو گل عدل را دهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاید که بگذرد ز پی فرخی همای</p></div>
<div class="m2"><p>زین پس به زیر سایه چتر خدایگان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلطان شرق و غرب قزل ارسلان که نیست</p></div>
<div class="m2"><p>با صدمت رکابش ایام را توان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن شاه شیر حمله که شاهین همتش</p></div>
<div class="m2"><p>دارد فراز کنگره سدره آشیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقت طرب چو دست سوی جام می برد</p></div>
<div class="m2"><p>بر هم زند ذخیره بحر دفین دکان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هنگام کین چو نیزه برافرازد از کتف</p></div>
<div class="m2"><p>مریخ را خطر بود از صدمت سنان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاها تو یی که حمله بأس تو بر عدو</p></div>
<div class="m2"><p>چون بر بخیل سایه سایل بود گران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بحریست قهر تو که در او هر که غرقه شد</p></div>
<div class="m2"><p>هرگز نیفتد از پس آن نیز برکران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برخیز و از زمانه به یکبار نسل و حرث</p></div>
<div class="m2"><p>گر دفع فتنه را نکند تیغ تو ضمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چند کور گشت عدو دید کایزدت</p></div>
<div class="m2"><p>بگزید و کرد بر همه آفاق کامران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا حجتی چنین که ببندد زبان چرخ</p></div>
<div class="m2"><p>تیغ تو را رسد که بر اعدا کشد زمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر باد داد هیبت تو خرمن قمر</p></div>
<div class="m2"><p>و اتش زده شکوه تو در راه ترکشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وقتی که گم شود ز سر سرکشان خود</p></div>
<div class="m2"><p>روزی که بگسلد ز تن پر دلان روان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وان آب منجمد که سنان است نام او</p></div>
<div class="m2"><p>از تَفِ حمله در رگ جان ها شود روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو در میان لشکر چون مور بی عدد</p></div>
<div class="m2"><p>هر یک چو مور بسته به فرمان تو میان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در تازی از کرانه چو شیران جنگ جوی</p></div>
<div class="m2"><p>کو پال بر زمین زنی و بانگ بر زمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن لحظه کس ندارد پای تو جز رکاب</p></div>
<div class="m2"><p>وان روز کس نگیرد دست تو جز عنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بد خواه ملک را ز نهیب تو آن نفس</p></div>
<div class="m2"><p>خون در جگر بسوزد و مغز اندر استخوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای خسروی که تیغ فنا را قضای بد</p></div>
<div class="m2"><p>بر دشمنان دولت تو کرد امتحان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر گم شود پی زحل از چرخ باک نیست</p></div>
<div class="m2"><p>بخت تو آگه ست چه حاجت به پاسبان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گیتی طمع نداشت که تو سر درآوری</p></div>
<div class="m2"><p>تا سایه بر سرت فکند افسر کیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این هم تواضع است که کردی و گرنه چرخ</p></div>
<div class="m2"><p>داند که مشتری بننازد به طیلسان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دندانه اره را هنر است ار نه تیغ را</p></div>
<div class="m2"><p>عیبی است سخت ظاهر و عاری است بس عیان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>محتاج نیست طلعت زیبای تو به تاج</p></div>
<div class="m2"><p>شمشیر صبح را نبود حاجت فسان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بسترد به دست صبا دایه بهار</p></div>
<div class="m2"><p>گرد از جبین لاله و رخسار ارغوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گلزار دولت تو که دارد نسیم خلد</p></div>
<div class="m2"><p>آسوده باد تا ابد از آفت خزان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جد تو سر فراز و قبول تو دستگیر</p></div>
<div class="m2"><p>ملک تو پایدار و بقای تو جاودان</p></div></div>