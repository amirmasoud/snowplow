---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>چون برفَراخت خسرو سیارگان علم</p></div>
<div class="m2"><p>در خاک پست کرد سراپرده ظُلَم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح دوم گرفت جهان گو چرا گرفت؟</p></div>
<div class="m2"><p>اندر هوای شاه نزَد جز به صدق دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک یک ز بیم خنجر خورشید اختران</p></div>
<div class="m2"><p>همچون مخالفان شهنشه شدند کم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر روی آسمان اثر تیرگی نماند</p></div>
<div class="m2"><p>اِلا زگرد موکب فرماندهِ عجم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارای عهد نصرت دین کز علو قدر</p></div>
<div class="m2"><p>شاید که بر معارج گردون نهد قدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلطان نشان اتابک اعظم که عدل او</p></div>
<div class="m2"><p>دارد حریم مملکت از امن چون حرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوبکربن محمد کز فر طلعتش</p></div>
<div class="m2"><p>زینت گرفت افسر کرسی و تخت جم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریا به دستگاه فراخش زند مثل</p></div>
<div class="m2"><p>گردون به آستان بلندش خورد قسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ماه و مهر از قبل طاعت آمده</p></div>
<div class="m2"><p>در حلقه حواشی و در زمره خدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذات مطهر تو سپهری ست از علو</p></div>
<div class="m2"><p>طبع مبارک تو جهانی ست از کرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقتی که دیگران به حشم التجا کنند</p></div>
<div class="m2"><p>گرد تو از معونت یزدان بود حشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن را که زیر دامن توفیق پرورند</p></div>
<div class="m2"><p>از گرم و سرد چرخ بدوکی رسد الم؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گردون به موج خون در صد بار غوطه خورد</p></div>
<div class="m2"><p>هرگز زمین ملک تو در خود نچیدنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صد ره فلک به خاک فرو رفت و کس ندید</p></div>
<div class="m2"><p>بر دامن مراد تو هرگز غبار غم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا کرده دست تو محکم بنای ملک</p></div>
<div class="m2"><p>هر لحظه با عنان تو فتحی شده ست ضم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برتو بدل چگونه گزیند جهان؟!که هست</p></div>
<div class="m2"><p>عهد تو همچو موسم اقبال مغتنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روی فلک سیه شود آنگاه رای تو</p></div>
<div class="m2"><p>بر چهره زمانه ز عصیان کشد رقم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکس که چون قلم نرود پیش تو به سر</p></div>
<div class="m2"><p>تقدیر بر جریده عمرش کند قلم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پهلو تهی کند فلک از تیغ تو ولیک</p></div>
<div class="m2"><p>از دشمنان دولت تو پر کند شکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خصم تو را زمانه به تعجیل می برد</p></div>
<div class="m2"><p>از عرصه وجود سوی حیّز عدم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از حضرت تو تیره شود ساحت فلک</p></div>
<div class="m2"><p>در مجلس تو رشک برد روضه ارم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاها زمانه بیخ ستم را به آب داد</p></div>
<div class="m2"><p>زان تیغ آب رنگ ببر بیخ آن ستم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیم است کز تغابن این چرخ نیلگون</p></div>
<div class="m2"><p>خون فسرده جوش زند در رگ بقم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زین پس مکن برانجم و افلاک اعتماد</p></div>
<div class="m2"><p>کانجم شدند خاین و افلاک متهم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شمشیر تیز داری و باروی کامکار</p></div>
<div class="m2"><p>گرد از فلک برآور و از و روزگار هم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا چرخ قد خمیده نگردد تمام راست</p></div>
<div class="m2"><p>در قامت مراد تو هرگز مباد خم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون گل همیشه بادی خندان و سرخ روی</p></div>
<div class="m2"><p>خصم تو چون بنفشه سر افکنده و دژم</p></div></div>