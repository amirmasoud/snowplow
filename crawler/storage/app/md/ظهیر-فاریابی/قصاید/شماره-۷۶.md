---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>دوش آوازه درافکند نسیم سحری</p></div>
<div class="m2"><p>که عروسان چمن راست گهِ جلوه گری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل خوش خوش چو خبر یافت ازین معنی، گفت</p></div>
<div class="m2"><p>راستی خوش خبری داد نسیم سحری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چنین است یقین دان که جهان بار دگر</p></div>
<div class="m2"><p>خوش بهشتی شود آراسته تا درنگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل اندیشه چو از وصف ریاحین بشکفت</p></div>
<div class="m2"><p>نوش کن باده گلگون، به چه اندیشه دری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبحدم ناله قمری شنو از طرف چمن</p></div>
<div class="m2"><p>تا فراموش کنی محنت دور قمری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجلس بزم بیارای که آراسته اند</p></div>
<div class="m2"><p>نقشبندان طبیعت رخ گلبرگ طری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو مستان صبوحی شده افتان خیزان</p></div>
<div class="m2"><p>شاخه‌های سمن تازه و بید طبری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن سوسن آزاد نمی یارم گفت</p></div>
<div class="m2"><p>آن نه از کم سخنی دان و نه از بی هنری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش ناگه سخن او به زبان آوردم</p></div>
<div class="m2"><p>آسمان گفت سزد کز سر آن درگذری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند گویی سخن سوسن و آزادی او؟</p></div>
<div class="m2"><p>مگر از بندگی شاه جهان بی خبری؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نصرة الدین ملک عالم عادل بوبکر</p></div>
<div class="m2"><p>که جهان جمله بیاراست به عدل عمری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن جوانبخت جهان بخش که از هیبت او</p></div>
<div class="m2"><p>باد بر غنچه نیارد که کند پرده دری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خسروا گوش بنفشه ست و زبان سوسن</p></div>
<div class="m2"><p>که به عهد تو بِرَستند ز گنگی و کری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کجا در همه عالم خللی دیگر بود</p></div>
<div class="m2"><p>کرد اقبال تو بی منت گردون سپری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابر در بزم چو دست گهرافشان تو دید</p></div>
<div class="m2"><p>خویشتن زود به پیش فلک افکند و گری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که چو اسراف کقش در کرم از حد بگذشت</p></div>
<div class="m2"><p>تو به نوعی غم این کار چرا می نخوری؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلکش گفت جز این کارِ دگر هست مرا</p></div>
<div class="m2"><p>هم تو می خور غم این کار که بیکار تری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی تو خوردند بسی این غم و هم سود نداشت</p></div>
<div class="m2"><p>تو درین باب قوی تر ز قضا و قدری؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بعد ما کز طلب پایه قدرت ناگاه</p></div>
<div class="m2"><p>دیده عقل فرو ماند ز کوته نظری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خواست اندیشه که در کنه کمال تو رسد</p></div>
<div class="m2"><p>عقل گفتش که تو هم بیهده تازی دگری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شهریارا تویی آن کز قِبَل خون عدوت</p></div>
<div class="m2"><p>گل کند گاهی پیکانی و گاهی سپری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صورت فتح و ظفر معتکف حضرت توست</p></div>
<div class="m2"><p>نی غلط رفت تو خود صورت فتح و ظفری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاتم ملک در انگشت تو کرده ست خدای</p></div>
<div class="m2"><p>چه زیان دارد اگر خصم شود دیو و پری؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا جهان سر ز گریبان فنا برنارد</p></div>
<div class="m2"><p>وز حوادث نشود دامن آفاق بری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در جهانداری چندانت بقا باد ای شاه</p></div>
<div class="m2"><p>که مهندس نکند عقدش اگر بر شمری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا تو از دولت و اقبال بدان پایه رسی</p></div>
<div class="m2"><p>که به پای عظمت تارک گردون سپری</p></div></div>