---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>گل ز خرگاه چمن روی به صحرا دارد</p></div>
<div class="m2"><p>سر می خوردن آن خرگه مینا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه چون تازگی افزود به سر سبزی سال</p></div>
<div class="m2"><p>گلبن فتح مَلک سرّ ثریا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاج بخش ملکان شاه جوانبخت جوان</p></div>
<div class="m2"><p>کز همه تاجوران منصب اعلا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر فیضی که به فتوی محمد نسبی</p></div>
<div class="m2"><p>بند بر تارک این گنبد خضرا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت بیدار فلک یاور و اقبال مطیع</p></div>
<div class="m2"><p>مملکت بین که چه اسباب مهیا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چنین باغ سعادت که گل فتح شکفت</p></div>
<div class="m2"><p>شاید ار چشم ظفر عزم تماشا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دولت قاهره از جانب شه دور مباد</p></div>
<div class="m2"><p>چرخ را پی کند ار جانب اعدا دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماه نو دید عدو بر علمش شیفته شد</p></div>
<div class="m2"><p>ماه نو شیفته را بر سر سودا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیم جان دید عدویت که ولایت بگذاشت</p></div>
<div class="m2"><p>آنک او غرقه شود کی غم کالا دارد ؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی کند همسری شاه منازع طرفی</p></div>
<div class="m2"><p>کز طرف تا به طرف بنده و مولا دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنده ای چند گر از خدمت او دور شدند</p></div>
<div class="m2"><p>شه نباید که جز اقبال تمنا دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ز دریا دو سه قطره بپراکند چه باک ؟</p></div>
<div class="m2"><p>باز چون جمع شود میل به دریا دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر که از قبله اسلام بگرداند روی</p></div>
<div class="m2"><p>بی گمان روی سوی قبله ترسا دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانک در دین مسیحا شود از هیبت او</p></div>
<div class="m2"><p>نبرد جان اگر افسون مسیحا دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که در مذهب شه نیست ز دنیا و ز دین</p></div>
<div class="m2"><p>مذهب آن است که نه دین و نه دنیا دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای یمن تاب سهیلی که به ناموس عقیق</p></div>
<div class="m2"><p>زخم پولاد تو خون بر دل خارا دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت آیم به مصاف تو ز دور آسان است</p></div>
<div class="m2"><p>مرد می باید کین زهره ویارا دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قهر اگر دشمن شه راشکند گو بشکن</p></div>
<div class="m2"><p>تا کی آزرم کند چند محابا دارد؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا تو در رسته دعوی که شناسا گهری</p></div>
<div class="m2"><p>نه زمرد که فلک رشته مینا دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با چو تو صیرفیی نقد نمودن خطر است</p></div>
<div class="m2"><p>که دل روشن تو دیده بینا دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون تویی داور و فریاد رس مظلومان</p></div>
<div class="m2"><p>کیست امروز که اندیشه فردا دارد؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنده را با تو مجال است به صد نکته ولیک</p></div>
<div class="m2"><p>جامه آن به که به اندازه بالا دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو سلیمانی واین مرغ زبانی که مراست</p></div>
<div class="m2"><p>پیش تو پر بنهد گر پر عنقا دارد</p></div></div>