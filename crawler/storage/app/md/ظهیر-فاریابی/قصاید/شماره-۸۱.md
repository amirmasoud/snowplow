---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>زهی مسخّر حُکمت ز ماه تا ماهی</p></div>
<div class="m2"><p>شه ستاره سپاه و سپهر درگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چوبندگان ،مه و خورشید بر درت شب و روز</p></div>
<div class="m2"><p>نشسته اند به هر خدمتی که در خواهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی که از ره تسبیب،قسط روزی خلق</p></div>
<div class="m2"><p>به دستِ توست گر افزایی و اگر گاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آن سپهر شکاری که شیر بیشه چرخ</p></div>
<div class="m2"><p>ز بیم تیغ تو تن دردهد به روباهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حلم و پر هنری چون خرد در ارواحی</p></div>
<div class="m2"><p>به رفق و خوش سخنی چون سخن در افواهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مصر مُلک خدایت عزیز کرد و هم اوست</p></div>
<div class="m2"><p>که داد تخت عزیزی به یوسف چاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز توست چهره دین را طراوت از پی آنک</p></div>
<div class="m2"><p>به تیغ حجت،آثار صبغة اللهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برد سنان تو از چشم روز بینایی</p></div>
<div class="m2"><p>دهد ضمیر تو از راز چرخ آگاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکست نامده از هیچ روی در حشمت</p></div>
<div class="m2"><p>مگر به طره جعد بتان خرگاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا رسد مه و خورشید،چون کند می لعل</p></div>
<div class="m2"><p>به روز پیش تو خورشیدی و به شب ماهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدایگانا دانی که خدمت تو مرا</p></div>
<div class="m2"><p>مقدم است بر اغراض مالی و جاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمانه سرزنشم کرد و گفت خیره! چرا</p></div>
<div class="m2"><p>فتادی از در شاه جهان به گمراهی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جواب دادم و گفتم که نیک بازاندیش</p></div>
<div class="m2"><p>که زین زمانه منم یا تو مخطی و ساهی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر فتاده ام از خدمتش شبانروزی</p></div>
<div class="m2"><p>گزیده ام به دعا خدمت سحرگاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا چو شاه گزیده ست و شاه را یزدان</p></div>
<div class="m2"><p>نه من ز بندگی افتم نه شاه از شاهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رسید موسم نوروز و دشمنان ز حسد</p></div>
<div class="m2"><p>همی زنند نفسهای سرد دی ماهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو بر سریر ملکشه نشسته ای چه عجب...؟</p></div>
<div class="m2"><p>اگر بود همه نوروز تو ملکشاهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به رغم اعدا عمرت دراز باد از آنک</p></div>
<div class="m2"><p>نگیرد از دم خفاش روز کوتاهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به امر و نهی بران بر زمانه حکم که نیز</p></div>
<div class="m2"><p>زمانه را نبود جز تو آمر و ناهی</p></div></div>