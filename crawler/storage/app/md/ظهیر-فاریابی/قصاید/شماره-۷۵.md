---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>هر کجا تازه بخندید گل رخساری</p></div>
<div class="m2"><p>بر رخم بشکفد از خون جگر گلزاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق بازی به جهان کار چو من بی کاریست</p></div>
<div class="m2"><p>که جزین کار ندانم من ومشکل کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دل از عشق حرج نیست که نادر یابی</p></div>
<div class="m2"><p>آب بی تیرگی و آینه بی زنگاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تنی داری جانیت بباید ناچار</p></div>
<div class="m2"><p>ور دلی داری نگزیردت از دلداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندرین واقعه تنها نه منم در عالم</p></div>
<div class="m2"><p>هر کسی را به حد خویش بود تیماری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه آفاق دربن حادثه یارند مرا</p></div>
<div class="m2"><p>وین عجیب تر که در آفاق ندارم یاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم من چون گلوی کشته شد از خونین اشک</p></div>
<div class="m2"><p>تا فتادم به کف خیره کشی خونخواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به بازرار غمش دست به سودا بردم</p></div>
<div class="m2"><p>داستانی ست ز من بر سر هر بازاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طره او ز دو چشمم به حیل خواب ببرد</p></div>
<div class="m2"><p>دل به امید چه دادم به چنان طراری؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهر بر هم زد و از شحنه و والی امروز</p></div>
<div class="m2"><p>هیچ کس نی که کند دفع چنان عیاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بارها در دلم آمد که من این مظلمه را</p></div>
<div class="m2"><p>به در صدره آفاق برم یکباری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قبله و قدوه شاهان جهان نورالدین</p></div>
<div class="m2"><p>که ندارد دو جهان پیش کفش مقداری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنک حفظش ز پی دفع حوادث هر روز</p></div>
<div class="m2"><p>گرد معموره اسلام کشد دیواری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانک در کشف حقایق چو زبان بگشاید</p></div>
<div class="m2"><p>آسمان بر در تاویل زند مسماری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای به وجود تو توانگر شده هر درویشی</p></div>
<div class="m2"><p>وی به توفیق تو آسان شده هر دشواری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسته چون طوق کبوتر ز مبادی وجود</p></div>
<div class="m2"><p>طوق فرمان تو در گردن هر جباری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عاشق ذکر جمیلی تو و شاهان جهان</p></div>
<div class="m2"><p>در حدیث درمی یا سخن دیناری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرخ با آن عظمت گشت به جاه تو مقر</p></div>
<div class="m2"><p>بس بود خاصه ز خصمان قوی اقراری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نی غلط می کنم او کیست که خصم تو بود</p></div>
<div class="m2"><p>کوژ پشتی،خرفی،خیره کشی،غداری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حال بدخواه تو گر چون گل نارست رواست</p></div>
<div class="m2"><p>زود باشد که شود در دلش آن گل خاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آسمان تازه نهالی بدماند ز زمین</p></div>
<div class="m2"><p>آن چه دانی تو که تختی کندش یا داری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سالها حاصل کان ار به هم آرد خورشید</p></div>
<div class="m2"><p>کم ز یک روزه عطای تو بود بسیاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لاف دریا چه زنم قاعده کان چه نهم؟</p></div>
<div class="m2"><p>گر حدیث کرم و جود تو گویم آری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جاودان فتنه سر از خواب فنا برنارد</p></div>
<div class="m2"><p>تا در آفاق چو حزم تو بود بیداری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیش رای تو خرد با همه هشیاری خویش</p></div>
<div class="m2"><p>همچنان است که مستی به بر هشیاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صفت گلبن جاه تو دریغ است دریغ</p></div>
<div class="m2"><p>جز به الحان چو من بلبل خوش گفتاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شعر بُندار که گفتی به حقیقت وحی است</p></div>
<div class="m2"><p>این حقیقت چو ببینی بود آن پنداری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در نهانخانه طبعم به تماشا بنگر</p></div>
<div class="m2"><p>تا ز هر زاویه ای عرضه دهم بُنداری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این سخن گرچه در او صورت دعوی ست ولیک</p></div>
<div class="m2"><p>عقل داند که برینش نرسد انگاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یارب این کفر ببین باز که گویی افلاک</p></div>
<div class="m2"><p>بسته اند از بر هر منطقه ای رُنّاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من که بر خلق به صد گونه هنر دارم فخر</p></div>
<div class="m2"><p>سخره بی هنران گشته نباشد عاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آب روی از پی نان بیهده دادم بر باد</p></div>
<div class="m2"><p>کاتشم باد چرا خاک نخوردم باری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بعد از ین چون به جناب تو تَولَا کردم</p></div>
<div class="m2"><p>چشم دارم که زچرخم نرسد آزاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخت،هر حادثه ای را نهد اکنون عذری</p></div>
<div class="m2"><p>واسمان هر گنهی را کند استغفاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا چنان پست نگردد در و دیوار وجود</p></div>
<div class="m2"><p>که نماند ز رسوم و طللش آثاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خانه عمر تو معمور بماناد که نیز</p></div>
<div class="m2"><p>به ز عدل تو جهان را نبود معماری</p></div></div>