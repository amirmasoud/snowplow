---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>ای بر زده به تقویت ملک آستین</p></div>
<div class="m2"><p>سلطان بر حقیقتی و شاه راستین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهپر برای تیر تو افکند روح قدس</p></div>
<div class="m2"><p>گیسو فدای پرچم تو کرد حور عین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده سهیل سنانت کشیده میل</p></div>
<div class="m2"><p>در ابروی هلال کمانت فکنده چین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که در دیار ارمن و گه در دیار فرس</p></div>
<div class="m2"><p>دشمن ز تو هزیمت و حاسد ز تو حزین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز تو که ساخت از پی تمکین تاج وتخت؟</p></div>
<div class="m2"><p>جز تو که کرد از پی اصلاح ملک و دین؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عرصه دو ملک دو کار چنین شگرف؟</p></div>
<div class="m2"><p>در مدت دوماه دو فتح چنین مبین؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خصم ارچه نرم گشت نگوید به ترک ملک</p></div>
<div class="m2"><p>تا بر نیارد آتش تیغت قرار کین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا موم را در آتش سوزنده نفکنی</p></div>
<div class="m2"><p>از کام او برون نشود طعم انگبین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاسین نوشت خصم تو یکچند اگرچه داشت</p></div>
<div class="m2"><p>صد گونه ظلم وبغض و حسد در دلش کمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا عاقبت چو پا به صف آخر اوفتاد</p></div>
<div class="m2"><p>چون تیز کرد بأس تو دندان برو چو سین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بودند قلعه هات همه پر ز سیم و زر</p></div>
<div class="m2"><p>از جود،صرف کردی و بخریدی آفرین</p></div></div>