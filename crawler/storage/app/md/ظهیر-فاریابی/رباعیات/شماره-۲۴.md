---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>دی شاه بتان با رخ رنگین می رفت</p></div>
<div class="m2"><p>بی اسب و پیاده نغز و شیرین می رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر ز لبش به پیل بالا می ریخت</p></div>
<div class="m2"><p>وز مستی و بیخودی چو فرزین می رفت</p></div></div>