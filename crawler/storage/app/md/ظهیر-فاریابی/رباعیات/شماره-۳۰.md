---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>چشم تو که ابروی کمانکش دارد</p></div>
<div class="m2"><p>در هر مژه ای هزار ترکش دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار برات ما مفرمای برو</p></div>
<div class="m2"><p>با عارضت افکن که خطی خوش دارد</p></div></div>