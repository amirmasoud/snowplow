---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>بر خوان امید فلک جامه کبود</p></div>
<div class="m2"><p>یک گرده پر نمک چو رویت ننمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین قرص که در تنور گردون پخته ست</p></div>
<div class="m2"><p>گرم است ولیکن نمکش نیست چه سود</p></div></div>