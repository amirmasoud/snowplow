---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>بلبل چو ز عشق گل فغان در گیرد</p></div>
<div class="m2"><p>از شعله آه من جهان درگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل را به کف آورم به صد حیله و فن</p></div>
<div class="m2"><p>پندارم با توام همان درگیرد</p></div></div>