---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>گه شانه زبان در خم گیسوت کشد</p></div>
<div class="m2"><p>گه آینه روی سخت در روت کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمه که بود که آید در چشمت؟</p></div>
<div class="m2"><p>یا وسمه که او کمان ابروت کشد؟</p></div></div>