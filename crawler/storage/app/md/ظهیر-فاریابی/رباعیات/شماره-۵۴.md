---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>هر حلقه زلفت زفن یکدیگر</p></div>
<div class="m2"><p>هستند نهان در شکن یکدیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر ربودن دل و غارت جان</p></div>
<div class="m2"><p>کردند زبان در دهن یکدیگر</p></div></div>