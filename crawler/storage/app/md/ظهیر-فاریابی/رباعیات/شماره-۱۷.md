---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>هر چند که میل تو سوی بیدادی ست</p></div>
<div class="m2"><p>یک ذره غمت به از جهانی شادی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما گله می کنی و لیکن ما را</p></div>
<div class="m2"><p>از بندگی تو صد هزار آزادی ست</p></div></div>