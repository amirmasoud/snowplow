---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>با گل گفتم چو سوی گلزار آیم</p></div>
<div class="m2"><p>از عهد بد تو سست گردد رایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل سوی تو بنگرید دزدیده بگفت:</p></div>
<div class="m2"><p>بد عهدتر از خودت کسی بنمایم؟</p></div></div>