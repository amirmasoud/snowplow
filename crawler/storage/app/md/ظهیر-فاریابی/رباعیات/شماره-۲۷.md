---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>از رایت تو نور ظفر می تابد</p></div>
<div class="m2"><p>کس نسیت که از رای تو سر می تابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عفو تو چو رحمت خدای ست که خلق</p></div>
<div class="m2"><p>هر جرم که می کنند بر می تابد</p></div></div>