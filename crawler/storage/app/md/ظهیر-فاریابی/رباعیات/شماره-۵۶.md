---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>تا چند ازین حبله و زراقی عمر</p></div>
<div class="m2"><p>تا چند مرا جرعه دهد ساقی عمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقا که من از ستیزه جرعه غم</p></div>
<div class="m2"><p>چون جرعه به خاک ریزم این باقی عمر</p></div></div>