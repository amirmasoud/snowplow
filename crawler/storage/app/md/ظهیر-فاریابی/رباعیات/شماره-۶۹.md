---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>هر گز نفسی حکایت از تو نکنم</p></div>
<div class="m2"><p>کازادی بی نهایت از تو نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل نکنم شکایتی کز تو کنم</p></div>
<div class="m2"><p>از دل کنم آن شکایت از تو نکنم</p></div></div>