---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>شاها،ز تو کار ملک ودین بانسق است</p></div>
<div class="m2"><p>دریا ز خجالت کفت در عرق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عهد تو رافضی و سنی با هم</p></div>
<div class="m2"><p>کردند موافقت که بوبکر حق است</p></div></div>