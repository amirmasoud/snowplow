---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>شاها،ملکان ملک سپارند به تو</p></div>
<div class="m2"><p>وز بیم تو خان و مان گذارند به تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو کعبه اقبال جهانی لابد</p></div>
<div class="m2"><p>شاهان زمانه روی آرند به تو</p></div></div>