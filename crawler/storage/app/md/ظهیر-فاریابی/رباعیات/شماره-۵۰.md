---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>در مستی اگر زمن گناهی آید</p></div>
<div class="m2"><p>شاید که دلت سوی جفا نگراید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمت به خمار عالمی بر هم زد</p></div>
<div class="m2"><p>گر من گنهی کنم به مستی شاید</p></div></div>