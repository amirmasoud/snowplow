---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>ای روی تو از لطافت آیینه روح</p></div>
<div class="m2"><p>خواهم که قدح های خیالت به صبوح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده کشم ولی ز خار مژه ام</p></div>
<div class="m2"><p>ترسم که شود پای خیالم مجروح</p></div></div>