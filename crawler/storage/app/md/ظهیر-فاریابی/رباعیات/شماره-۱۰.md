---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>چشمی دارم همیشه بر صورت دوست</p></div>
<div class="m2"><p>با دیده مرا خوش است چون دوست دروست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیده و دوست فرق کردن نه نکوست</p></div>
<div class="m2"><p>یا اوست به جای دیده یا دیده خود اوست</p></div></div>