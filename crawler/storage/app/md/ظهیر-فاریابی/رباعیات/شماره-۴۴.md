---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>دل گرچه هلاک جان و تن می خواهد</p></div>
<div class="m2"><p>رسوایی کار خویشتن می خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من فارغم از ملامت دشمن و دوست</p></div>
<div class="m2"><p>خود حسن تو عذر دل من می خواهد</p></div></div>