---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>چو شمع تنم ز دل به جان می آید</p></div>
<div class="m2"><p>جانم به لب از تاب زبان می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین آتش دل خوشم چنان می آید</p></div>
<div class="m2"><p>کز آتش آب در دهان می آید</p></div></div>