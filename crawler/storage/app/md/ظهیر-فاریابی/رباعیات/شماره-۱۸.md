---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>گر یار بداندی کِم اندر دل چیست</p></div>
<div class="m2"><p>یا گفت بیارمی که دلدارم کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودی که به درد دل نبایستی مرد</p></div>
<div class="m2"><p>بودی که به کام دل بشایستی زیست</p></div></div>