---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>گرچه همه جهد بندگی بنمایم</p></div>
<div class="m2"><p>از عشق تو پیش کس زبان نگشایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم بر سر آب آید این قصه چو من</p></div>
<div class="m2"><p>با آب دو چشم خویش می برنایم</p></div></div>