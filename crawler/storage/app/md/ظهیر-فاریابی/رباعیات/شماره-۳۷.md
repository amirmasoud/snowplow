---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>در عشق اگر دمی قرارت باشد</p></div>
<div class="m2"><p>با صحبت نیکوان چه کارت باشد؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر تیز چو خار باش،تا یار چو گل</p></div>
<div class="m2"><p>گه در بر و گاه در کنارت باشد</p></div></div>