---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>رویش نگر ار صورت جان می خواهی</p></div>
<div class="m2"><p>وصلش طلب ار ملک جهان می خواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط وی و اشک من ببین دور مشو</p></div>
<div class="m2"><p>گر سبزه و گر آب روان می خواهی</p></div></div>