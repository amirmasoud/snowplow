---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>باد آمد و گل بر سر میخواران ریخت</p></div>
<div class="m2"><p>یار آمد و می در قدح یاران ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفش به تلطف آب عطاران برد</p></div>
<div class="m2"><p>چشمش به کرشمه خون میخواران ریخت</p></div></div>