---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>شیرین دهنش چو در سخن می آید</p></div>
<div class="m2"><p>در شیوه گری شکن شکن می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می گوید چشمی که درو می نگرد</p></div>
<div class="m2"><p>از حسرتش آب در دهان می آید</p></div></div>