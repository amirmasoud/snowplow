---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>گر یک نفست ز زندگانی گذرد</p></div>
<div class="m2"><p>مگذار که جز به شادمانی گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار که سرمایه ملکت ز جهان</p></div>
<div class="m2"><p>عمری است چنان کش گذرانی گذرد</p></div></div>