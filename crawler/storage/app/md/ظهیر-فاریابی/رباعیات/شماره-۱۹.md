---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>در پرده خوشدلی کسی را راهی ست</p></div>
<div class="m2"><p>کو را سروکار با چو تو دلخواهی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سبزه تر دمیده در سایه گل</p></div>
<div class="m2"><p>انصاف بده که خوش تماشا گاهی ست</p></div></div>