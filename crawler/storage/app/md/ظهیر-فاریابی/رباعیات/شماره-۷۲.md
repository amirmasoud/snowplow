---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>ما خانه ز خانه قلندر کردیم</p></div>
<div class="m2"><p>وز خاک در مصطبه افسر کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب بر لب ساغر چو صراحی جان را</p></div>
<div class="m2"><p>خندان خندان فدای ساغر کردیم</p></div></div>