---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بر کرده چو مه سر از گریبان می رفت</p></div>
<div class="m2"><p>در دامن خورشید خرامان می رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه گه به سخن درآمده لعل لبش</p></div>
<div class="m2"><p>گویی عرق از چشمه حیوان می رفت</p></div></div>