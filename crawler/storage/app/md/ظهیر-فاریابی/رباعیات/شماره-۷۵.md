---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ای دل مشو اندر خط این خوش پسران</p></div>
<div class="m2"><p>هر عشوه که زلفشان فروشد مخر آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این حلقه ما راست، مزن دست درین</p></div>
<div class="m2"><p>وان رشته مو رُست منه پای بر آن</p></div></div>