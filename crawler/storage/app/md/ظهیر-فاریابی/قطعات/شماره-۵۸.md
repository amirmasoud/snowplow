---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>ای خسروی که از تف تیغ تو در نبرد</p></div>
<div class="m2"><p>جان عدو فتد چو دل شمع در گداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا که می روی ظفر اندر رکاب توست</p></div>
<div class="m2"><p>در هیچ منزل از تو نخواهد فتاد باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگر شکی نماند جهان را درین که هست</p></div>
<div class="m2"><p>شاهی تو را حقیقت و خصم تو را مجاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ملک وارث پدر و عم تویی از آنک</p></div>
<div class="m2"><p>هست از تو جان عم و پدر در نعیم و ناز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلطان کسی بود که تو بخشیش تاج و تخت</p></div>
<div class="m2"><p>لشکر کسی کشد که تو سازیش برگ و ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون نماز پنج سزد نوبت تو زانک</p></div>
<div class="m2"><p>بر خلق طاعت تو فریضه ست چون نماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بادا بر آستین ظفر تا به روز حشر</p></div>
<div class="m2"><p>بو بکربن محمدبن ایلد گز طراز</p></div></div>