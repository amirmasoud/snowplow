---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>شاها عجم چو گشت مسخر به تیغ تو</p></div>
<div class="m2"><p>رو لشکری به بارگه مصطفا فرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس کعبه را خراب کن و ناودان بسوز</p></div>
<div class="m2"><p>خاک حرم چو ذرّه به سوی هوا فرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کعبه جامه می چکند در خزانه نه</p></div>
<div class="m2"><p>وانگه به سوی کعبه سه گز بوریا فرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کافری تمام شوی سوی کرخ شو</p></div>
<div class="m2"><p>وانگه سر خلیفه به سوی خطا فرست</p></div></div>