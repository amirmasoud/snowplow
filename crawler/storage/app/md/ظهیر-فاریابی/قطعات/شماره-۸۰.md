---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>بزرگوارا سالی زیادت است که من</p></div>
<div class="m2"><p>به جام نظم می مدح تو همی نوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیده ام ز تو نانی چنانک بر گویم</p></div>
<div class="m2"><p>نیافته ز تو چیزی چنانک در پوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مجلسی که ز جودت مرا سئوال کنند</p></div>
<div class="m2"><p>نهاد باید ناچار پنبه در گوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مباش غافل اگر چه من از شمایل خوب</p></div>
<div class="m2"><p>حکیم سیرت و نیکو نهاد و خاموشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گاه نظم چو من بر سخن سوار شوم</p></div>
<div class="m2"><p>کشند غاشیه اقران به عجز بر دوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مدح و هجو همه کس گه شکایت و شکر</p></div>
<div class="m2"><p>چو آفتاب بتابم چو بحر بخروشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مدح خویش مرا گر صلت همی ندهی</p></div>
<div class="m2"><p>ازین حدیث نه غمگین شوم نه بخروشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من ار ز هجو تو بیتی دو،برکسان خوانم</p></div>
<div class="m2"><p>نهند تخته دیباهمی در آغوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زر سرخ ز من چون هجای تو بخرند</p></div>
<div class="m2"><p>رضا دهی که به نرخی تمام بفروشم؟</p></div></div>