---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>خدایگان جهان شهریار روی زمین</p></div>
<div class="m2"><p>تویی که رایت عزمت همیشه منصور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زنده کردن ارواح نصرت و تایید</p></div>
<div class="m2"><p>صدای نوبت تو همچو نوبت صور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یاد بزم تو گردون صبوح کرد مگر</p></div>
<div class="m2"><p>که صوت مرغان همچون نوای طنبوراست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگ شرابی مسکین بنفشه بین که بگاه</p></div>
<div class="m2"><p>سرش فرو شد و نرگس هنوز مخمور است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شنیده ام که زبانی به ذکر من بگشود</p></div>
<div class="m2"><p>کسی که او به زبان جلال مذکور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین شرف که مرا دست دادنتوان گفت</p></div>
<div class="m2"><p>که سعی بخت و زمانه چگونه مشکور است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورای این ز سعادت مقام دیگر نیست</p></div>
<div class="m2"><p>برون از آنک ز ادراک آدمی دور است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به دانش تنها زمانه حاسد بود</p></div>
<div class="m2"><p>چنانکه در همه شهر این حدیث مشهور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون عنایت خسرو بدان اضافت شد</p></div>
<div class="m2"><p>اگر حسد برد از من زمانه معذور است؟</p></div></div>