---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>امام عالم و مفتی وقت محیی الدین</p></div>
<div class="m2"><p>تویی به اسب و رخ از کاینات گشته فره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مدح تو دو سه نوبت قصیده ها گفتم</p></div>
<div class="m2"><p>نکرد سعی تو از کار من گشاده گره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پیش منبرت امروز مردکی بر خاست</p></div>
<div class="m2"><p>که توبه می کنم از کرده ها تو گفتی زه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مردمان تو زر و جامه خواستی و همه</p></div>
<div class="m2"><p>به طبع و طوع بدادند بی لجاج و سته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر شعر چو چیزی ندادیم باری</p></div>
<div class="m2"><p>برای توبه که دادی ز شاعریم بده</p></div></div>