---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>ای حکم تو چون قضاءمبرم</p></div>
<div class="m2"><p>آسوده ز اعتراض و تبدیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از طارم سقف همت تو</p></div>
<div class="m2"><p>آویخته نه فلک چو قندیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا حشر بکرده آل عباس</p></div>
<div class="m2"><p>در آیت خسرویت تأویل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاریک شده جهان روشن</p></div>
<div class="m2"><p>در چشم عدوت میل در میل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در معرکه تیغت از سر دست</p></div>
<div class="m2"><p>مانند پیاده افکند پیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز دست کفت فرات و دجله</p></div>
<div class="m2"><p>هر لحظه زنند جامه در نیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید که کمترین و شاقی ست</p></div>
<div class="m2"><p>در موکب تو دوان به تعجیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تحویل همی کند به برجی</p></div>
<div class="m2"><p>کز عدل تو یافته ست تعدیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میمون و خجسته باد بر تو</p></div>
<div class="m2"><p>نوروز بزرگ و روز تحویل</p></div></div>