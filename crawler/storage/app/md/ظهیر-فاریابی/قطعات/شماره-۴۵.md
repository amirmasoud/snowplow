---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>پناه ملت و راعی خلق نصرة دین</p></div>
<div class="m2"><p>تویی که چرخ به نام تو نامدار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنای شرع به سعی تو مرتفع گردد</p></div>
<div class="m2"><p>اساس عدل به ملک تو استوار شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو در شب حدثان صبح دولتت بدمید</p></div>
<div class="m2"><p>چه جای صبح؟که خورشید شرمسار شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو از بزرگی جایی رسیده ای امروز</p></div>
<div class="m2"><p>که آسمان ز قبولت بزرگوار شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه وهمها که درو بسته بود مهر و سپهر</p></div>
<div class="m2"><p>که دولت تو بر اطراف کامکار شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید آن بود اکنون زمانه را از تو</p></div>
<div class="m2"><p>که نظم رونق عالم یکی هزار شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فیض نعمت تو ابر درفشان گردد</p></div>
<div class="m2"><p>ز نشر مدحت تو خاک مشکبار شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که مدح تو گوید به جای آن باشد</p></div>
<div class="m2"><p>که پیش همت او کاینات خوار شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر قبول نکردم عطات معذورم</p></div>
<div class="m2"><p>که پیش رای تو این نکته آشکار شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که ابر قطره به دریا از آن فرستد باز</p></div>
<div class="m2"><p>که تا به وقت دگر در شاهوار شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیاب کام دل از روزگار چندانی</p></div>
<div class="m2"><p>که روزگار تو تاریخ روزگار شود</p></div></div>