---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>بزرگ جهان! گر توانستمی</p></div>
<div class="m2"><p>به جان خاک صدر تو بخریدمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپرسیدمت در چنین عارضه</p></div>
<div class="m2"><p>وگر روی بودی نپرسیدمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو دانی که گر وصل ممکن بدی</p></div>
<div class="m2"><p>فراق جناب تو نگزیدمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولیکن چو من دوستدار توام</p></div>
<div class="m2"><p>تو را در چنان حال چون دیدمی</p></div></div>