---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>شنید بنده که فرمانده جهان می گفت</p></div>
<div class="m2"><p>که غم مخور تو که تیمار کارتو ببرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خوردنیها من خود همین غمی دارم</p></div>
<div class="m2"><p>چو زین برآمدم آخر ازین سپس چه خورم</p></div></div>