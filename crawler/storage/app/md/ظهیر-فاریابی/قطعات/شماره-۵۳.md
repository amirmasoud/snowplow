---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ای بر سر ساکنان گردون</p></div>
<div class="m2"><p>گسترده همای دولتت پر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پای جنیبت تو افتاد</p></div>
<div class="m2"><p>از حمله هیبت تو صرصر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد به حمایت حسامت</p></div>
<div class="m2"><p>از دست[ مواهب] تو گوهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترس از تو و بازگشت با تو</p></div>
<div class="m2"><p>پس چیست سپهر و کیست اختر؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بس دم صبح را که پاست</p></div>
<div class="m2"><p>در سینه شب شکست لشکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وی بس شب خصم را که تیغت</p></div>
<div class="m2"><p>پیوست به صبح روز محشر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان روز که بهر حفظ اسلام</p></div>
<div class="m2"><p>در دست تو نور داد خنجر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر جا که دو تن فراهم آیند</p></div>
<div class="m2"><p>این است حدیث کای برادر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی که به زخم گرز خسرو</p></div>
<div class="m2"><p>می کوفت عدوی ملک را سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون گل که برون دمد ز غنچه</p></div>
<div class="m2"><p>بر می جوشید خون ز مغفر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای چشم سپهر در تو حیران</p></div>
<div class="m2"><p>در بنده به چشم لطف بنگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مپسند که با چنین معانی</p></div>
<div class="m2"><p>کافاق شده ست ازو معطر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی عطر بود مرا شب و روز</p></div>
<div class="m2"><p>از آتش فاقه دل چو مجمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز غصه سروران ملکت</p></div>
<div class="m2"><p>هر لحظه رخم به خون شود تر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صد بار به مدح یک به یک شان</p></div>
<div class="m2"><p>بر گردن دهر بسته زیور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وین محتشمان نهاده با بخل</p></div>
<div class="m2"><p>صد منت دیگرم به سر بر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا خود به چه دانش و کفایت</p></div>
<div class="m2"><p>در ملک تو گشته اند سرور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم طبع زمانه باش زنهار</p></div>
<div class="m2"><p>جز ناکس و بی هنر مپرور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چندانک خری کری ستاند</p></div>
<div class="m2"><p>گر هیچ کری کند بده زر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا باز خرم به دولت تو</p></div>
<div class="m2"><p>خود را ز جفای این همه خر</p></div></div>