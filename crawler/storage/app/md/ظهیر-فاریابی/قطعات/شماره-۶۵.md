---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ای صاحبی که هر که در آفاق سرکش ست</p></div>
<div class="m2"><p>از طوق منت تو بفرسود گردنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که رای تو به سر مشکلی فتد</p></div>
<div class="m2"><p>حاجت نیفتد به بیان مبرهنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نوبهار تربیتت یافت رنگ و بوی</p></div>
<div class="m2"><p>هر گل که مرغزار سپهرست گلشنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغی کز آشیانه اقبال تو پرد</p></div>
<div class="m2"><p>از اختران ثابته پاشند ارزنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش فروغ عزم تو دارد ازین قبل</p></div>
<div class="m2"><p>در بر گرفته اند چون جان سنگ و آهنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای همت تو ساکن آن بقعه کز علو</p></div>
<div class="m2"><p>بیرون هفت خطه چرخ است یر زنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معلوم رای توست که داعی دولتت</p></div>
<div class="m2"><p>بازی ست کاسمان تو زیبد نشیمنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انوار مدحت تو بدیدند همگنان</p></div>
<div class="m2"><p>اندر ضمیر صافی و در طمع روشنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازانجا که لطف توست چنان کن که بعد ازین</p></div>
<div class="m2"><p>آثار نعمت تو ببینند بر تنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بادا همیشه کسوت عمرت چنانک بخت</p></div>
<div class="m2"><p>تا روز حشر دست ندارد ز دامنش</p></div></div>