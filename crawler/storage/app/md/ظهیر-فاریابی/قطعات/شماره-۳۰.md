---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>پناه ملت اسلام و قطب آل رسول</p></div>
<div class="m2"><p>تویی که قدر تو بر آسمان زبون گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو از کمان نظر تیر نطق بگشایی</p></div>
<div class="m2"><p>دل فحول جهان از نهیب خون گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر کنم به مَثَل در حکاتت تقصیر</p></div>
<div class="m2"><p>برین طریق مرا عقل رهنمون گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که وجه سباحت تمام نشناسد</p></div>
<div class="m2"><p>به گرد ساحل بحر محیط چون گردد؟</p></div></div>