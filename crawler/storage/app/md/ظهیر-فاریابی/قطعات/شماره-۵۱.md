---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>ای به شش ضرب از جهان در نرد جباری فِره</p></div>
<div class="m2"><p>تا ابد دوات روان بادا و حکمت بر نفاذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه اقبال تو از راه محابا دور چند</p></div>
<div class="m2"><p>یافت با خصمت لباساتی بسی نرد گشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخم تیغ بندگانت بس موافق بود و نیز</p></div>
<div class="m2"><p>کعبتینهایی که پیکرش آن چنان یاری بداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاجرم چون کعبتینش باز مالیدی به وقت</p></div>
<div class="m2"><p>داو افزون کرده اندر ششدر خذلان فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو زین پس دست در خصل تعدی چون کند</p></div>
<div class="m2"><p>چون یقینش شد که خصلی نیز می نتوان نهاد</p></div></div>