---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>پناه ملت و داعی خلق نصرة دین</p></div>
<div class="m2"><p>تویی که هست ضمیر تو با قضا همراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرم،حقیقی و ذاتی تو راست در عالم</p></div>
<div class="m2"><p>هرآنچه هست دگر استعارت است و مجاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به عنف زنی بانگ ناگهان بر کوه</p></div>
<div class="m2"><p>ز هیبت تو صدا را فرو شود آواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایگانا زان پس که روزگار مرا</p></div>
<div class="m2"><p>بتاخت مدت ده سال در نشیب و فراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزیمتم همه آن بود و بس که یکچندی</p></div>
<div class="m2"><p>کنم جناب تو را قبله دعا و نیاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه موجب است که از خدمت تو محرومم</p></div>
<div class="m2"><p>نه تو بخیل و نه من جاهل و نه راه دراز</p></div></div>