---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>ای گسسته قلاده پروین</p></div>
<div class="m2"><p>زهره از بهر عقد بازوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نعیم وجود پر کرده</p></div>
<div class="m2"><p>هفت کشور شکم به پهلوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در نه خزانه افلاک</p></div>
<div class="m2"><p>کسوتی کان رسد به زانوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی مگر اندکی تغیر داشت</p></div>
<div class="m2"><p>رای صافی و روی نیکوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو اختران ندا می کرد</p></div>
<div class="m2"><p>کای من و شش وشاق هندوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو عروسان خلد تا بینند</p></div>
<div class="m2"><p>گره زلف خود در ابروی تو</p></div></div>