---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>خورشید صدور عصر،صدرالدین</p></div>
<div class="m2"><p>بی لطف تو جان عدوی تن باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واندر حرم حمایت حفظت</p></div>
<div class="m2"><p>دوران سپهر مؤتمن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات تو و چار صفّه ارکان</p></div>
<div class="m2"><p>عیسی و سرای اَهرمن باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جود تو و الماس محتاجان</p></div>
<div class="m2"><p>یعقوب و نسیم پیرهن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمعی ست جلال تو که در جنبش</p></div>
<div class="m2"><p>نه طاس فلک یکی لگن باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خلق تو باد چون روا دارد</p></div>
<div class="m2"><p>که همدم نافه ختن باشد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با لطف تو آب چون در آرد سر</p></div>
<div class="m2"><p>کو معدن لؤلؤ عدن باشد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اطراف ردا و رکن دستارت</p></div>
<div class="m2"><p>آرایش صدر و انجمن باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایام کریم و عهد میمونت</p></div>
<div class="m2"><p>تاریخ مفاخر زمن باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدر تو به جای چرخ بنشیند</p></div>
<div class="m2"><p>وانگاه به جای خویشتن باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوری ز در تو اهل معنی را</p></div>
<div class="m2"><p>چون طعنه دوست دلشکن باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صدرا سر آن نداشتم کامسال</p></div>
<div class="m2"><p>جز درگه تو مرا وطن باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایام رها نکرد کان دولت</p></div>
<div class="m2"><p>روزی دوسه دافع حزن باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از کاری و خدمتی که در حضرت</p></div>
<div class="m2"><p>هرچ آن برود به دست من باشد</p></div></div>