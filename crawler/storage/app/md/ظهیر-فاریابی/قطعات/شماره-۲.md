---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای خسروی که از رخ دوشیزگان غیب</p></div>
<div class="m2"><p>هر لحظه دست فکرت تو در کشد نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عرض گاه زینت بزم تو فی المثل</p></div>
<div class="m2"><p>طاووس وقت جلوه نماید کم از غراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حفظت به هر زمین که سپر در سپر کشید</p></div>
<div class="m2"><p>ممکن بود که رخنه شود تیغ آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز بیم میل قهر تو کان دم به دم بود</p></div>
<div class="m2"><p>بر چشم دشمنانت نیارد گذشت خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاها زکوة گوش و زبان را ز راه لطف</p></div>
<div class="m2"><p>بشنو ز من سؤالی و تشریف ده جواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کس که حکم کرد به طوفان باد و گفت</p></div>
<div class="m2"><p>کاسیب آن عمارت عالم کند خراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تشریف یافت از تو و اقبال دید و کس</p></div>
<div class="m2"><p>در بند آن نشد که خطا گفت یا صواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بنده چون به پیش تو ابطال کرده ام</p></div>
<div class="m2"><p>با من چرا ز وجه دگر می رود خطاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر من و بال شد هنر من که صد بلا</p></div>
<div class="m2"><p>بر ساعتی که من به هنر کردم انتساب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گو نیست گرد عالم و گو پست شو فلک</p></div>
<div class="m2"><p>بر من به نیم جو چو فکندم درین عذاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طوفان من گذشت که نه ماه ساختم</p></div>
<div class="m2"><p>از آب دیده شربت و از خون دل شراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سهل است این سه ماه دگر نیز همچنین</p></div>
<div class="m2"><p>تن در دهم به آنک نه نانم بود نه آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیکن ز دست فاقه بترسم که عاقبت</p></div>
<div class="m2"><p>هم من ز جان بر آیم و هم خسرو از ثواب</p></div></div>