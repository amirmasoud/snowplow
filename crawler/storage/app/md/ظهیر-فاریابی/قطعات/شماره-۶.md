---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای سینه روزگار پر جوش</p></div>
<div class="m2"><p>از آتش تیغ آبدارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچ از لب آرزو برآید</p></div>
<div class="m2"><p>ایام نهاده در کنارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مدت عمر نارسیده</p></div>
<div class="m2"><p>خورشید دو اسبه در غبارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون عزم سفر درست کردی</p></div>
<div class="m2"><p>دولت که همیشه باد یارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از خدم تو می خرامد</p></div>
<div class="m2"><p>منزل منزل در انتظارت</p></div></div>