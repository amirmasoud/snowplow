---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>خدایگان جهان شهریار دریا دل</p></div>
<div class="m2"><p>توراست دست گهر بخش و لفظ لؤلؤ پاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر اسمان و زمین دست مطلق است تو را</p></div>
<div class="m2"><p>که از وظیفه جود تو یافتند معاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی به پنجه هیبت دل جهان بشکن</p></div>
<div class="m2"><p>گهی به ناخن قدرت رخ فلک بخراش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی که باد صبا در جهان نیارد کرد</p></div>
<div class="m2"><p>نسیم عارض گل بی جواز حکم تو فاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکارم تو چنان عام گشت در عالم</p></div>
<div class="m2"><p>که در سخای تو با من برابرند اوباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روی مدح برون بردم این شکایت حال</p></div>
<div class="m2"><p>اساس مظلمه ای می نهم تو حاکم باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا که باز سپیدم سزد که بسته شود</p></div>
<div class="m2"><p>ز آفتاب لقای تو دیده چون خفاش؟</p></div></div>