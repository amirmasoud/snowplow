---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>شهریارا برای مدحت تو</p></div>
<div class="m2"><p>تیغ فکرت همیشه آخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بساط هوات اسب مراد</p></div>
<div class="m2"><p>بر رخ روزگار تاخته‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه از آرزوی خدمت تو</p></div>
<div class="m2"><p>دل و جان را به غم گداخته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذکر زحمت نمی‌کنم حالی</p></div>
<div class="m2"><p>با شراب تهی بساخته‌ام</p></div></div>