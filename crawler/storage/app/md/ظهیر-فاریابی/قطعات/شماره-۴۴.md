---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>ای قضا صولتی که در عالم</p></div>
<div class="m2"><p>آنچه حکمت کند قدر نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آنچه با خلق می کند سعیت</p></div>
<div class="m2"><p>با چمن شبنم مطر نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرف ذاتیت چنان آمد</p></div>
<div class="m2"><p>کاندرو سلطنت اثر نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که خاطر گماشت بر کینت</p></div>
<div class="m2"><p>جز به جان بی گمان خطر نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد ازین رایت جهانگیرت</p></div>
<div class="m2"><p>فلک هفتمین مقر نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر شبیخون کنی به اهل عراق</p></div>
<div class="m2"><p>فتح این باب جز ظفر نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انتقام عدو بکش کامروز</p></div>
<div class="m2"><p>با تو کس دست در کمر نکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهریارا سزد که بر حالم</p></div>
<div class="m2"><p>کرم شاملت نظر نکند؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیک دانی که بر سپهر هلال</p></div>
<div class="m2"><p>نشود بدر تا سفر نکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر من رفت بر امید مگر</p></div>
<div class="m2"><p>هیچ سودی مرا مگر نکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نگشتم به خدمتی مخصوص</p></div>
<div class="m2"><p>کار طالع کند هنر نکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیش ازینم مدار بی پر و بال</p></div>
<div class="m2"><p>تا کس این قصه را سمر نکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کانچه با بنده کرد شهر سراب</p></div>
<div class="m2"><p>با قصب پرتو قمر نکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در گذرهای او [ گِلی است که] پیل</p></div>
<div class="m2"><p>جز به کشتی درو عبر نکند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر به خدمت نمی رسم چه عجب</p></div>
<div class="m2"><p>که ازو اسب ره به در نکند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخنی چند بشنو از بنده</p></div>
<div class="m2"><p>که در آن شرح مختصر نکند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که از حال زیر دستانت</p></div>
<div class="m2"><p>چون بداند تو را خبر نکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه در حال دولتی بیند</p></div>
<div class="m2"><p>بر پل عافیت گذر نکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای چنان بوده در جهانداری</p></div>
<div class="m2"><p>کز تو کس ناله سحر نکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مادحی صادقم که در مدحت</p></div>
<div class="m2"><p>خاطرم هیچ مدخر نکند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نبود روز کز ثنای تو را</p></div>
<div class="m2"><p>جبرئیل امین ز بر نکند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که بیتی شنید ازین قطعه</p></div>
<div class="m2"><p>سخن عِقد دُر،دگر نکند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفته من به فال دار از آنک</p></div>
<div class="m2"><p>مدد بحر جز شمر نکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برخور از جود کانچه عدلت کرد</p></div>
<div class="m2"><p>در نمای نبات خور نکند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جاودان باش تا مدار فلک</p></div>
<div class="m2"><p>عاقبت گرد این مدر نکند</p></div></div>