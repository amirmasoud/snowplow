---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>شاها به قدر همت و رای رفیع خویش</p></div>
<div class="m2"><p>از سقف چرخ و ساحت عرش آستانه ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین عندلیب را ز پی مدح گستری</p></div>
<div class="m2"><p>بر شاخسار سایه خویش آشیانه ساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساز و نوای جاه تو را این نوای من</p></div>
<div class="m2"><p>در خور بود که خوش نبود بی ترانه ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم قصیده ای که ز نظمش حسد برد</p></div>
<div class="m2"><p>اوهام نکته پرور و طبع فسانه ساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و آمد به حضرت تو شها بلبلی چو من</p></div>
<div class="m2"><p>دلم قبول گستر و وز لطف دانه ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا باز پس فرست ازینجا به خانه ام</p></div>
<div class="m2"><p>یا در جوار بارگه اینجام خانه ساز</p></div></div>