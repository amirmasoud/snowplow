---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>خسروا!ابر رحمت تو کجاست</p></div>
<div class="m2"><p>تا ز فیضش به فتح باب رسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه ای بر سرم فکن به کرم</p></div>
<div class="m2"><p>تا ز رفعت بر آفتاب رسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون من از فاریاب مسکن خویش</p></div>
<div class="m2"><p>سوی این مرتفع جناب رسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم دارم که با بضاعت فضل</p></div>
<div class="m2"><p>از سخای تو در نصاب رسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو از شهر ری به ساوه رسی</p></div>
<div class="m2"><p>من ازین سو به فاریاب رسم</p></div></div>