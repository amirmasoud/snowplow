---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>بزرگوارا دنیا ندارد آن عظمت</p></div>
<div class="m2"><p>که هیچکس را زیبد بدان سرافرازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرف به علم و عمل باشد و تو را همه هست</p></div>
<div class="m2"><p>بدین نعیم مزور چرا همی نازی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چیست کاهل هنر را نمی کنی تمییز؟</p></div>
<div class="m2"><p>تو نیز نه به هنر در زمانه ممتازی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سوی من تو به بازی نگه مکن که ز علم</p></div>
<div class="m2"><p>دلم به گیسوی حوران همی کند بازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه تلخ بود یک سخن ز من بشنو</p></div>
<div class="m2"><p>چنانک آن را دستور حال خودسازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو این سپر که ز دنیا کشیده ای در روی</p></div>
<div class="m2"><p>به روز عرض مظالم چنان بیندازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که از جواب سلامی که خلق را بر توست</p></div>
<div class="m2"><p>به رد مظلمه دیگری نپردازی</p></div></div>