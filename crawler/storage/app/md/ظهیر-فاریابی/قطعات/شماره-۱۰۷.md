---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>ای خسروی که درگه قدر تو را سپهر</p></div>
<div class="m2"><p>تا روز حشر مقصد اهل زمانه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غوغای فتنه دست به جایی که بر گشاد</p></div>
<div class="m2"><p>حزم تو دفع آن به سر تازیانه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرواز کرد گرد جهان طایر جلال</p></div>
<div class="m2"><p>تا در پناه دولت تو آشیانه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خون بدسگال تو حزم بهانه جوی</p></div>
<div class="m2"><p>هر قصد بد که آن بتوان بی بهانه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون طاق کبریای تو اقبال برکشید</p></div>
<div class="m2"><p>از طاق آسمانش قضا آستانه کرد</p></div></div>