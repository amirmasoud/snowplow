---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>خدایگان اکابر بهاء دولت دین</p></div>
<div class="m2"><p>تو را رسد به جهان سروری و سر داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از هوای تو خو باز چون توانم کرد؟</p></div>
<div class="m2"><p>که با حیات من آمیخته ست پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلاه گوشه حکم تو از طریق نفاذ</p></div>
<div class="m2"><p>ربوده از سر گردون کلاه جباری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دولت تو سزدگر امیدوار شوم</p></div>
<div class="m2"><p>که شاید ار به جوانان امیدها داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط کن غم مستی مخور که گاه طرب</p></div>
<div class="m2"><p>اگر چه مست نمایی به عقل هوشیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوام عمر تو باشد که آخرش نبود</p></div>
<div class="m2"><p>سزد که کار مرا آخری به دیداری</p></div></div>