---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>صدر صدور مشرق و مغرب نظام دین</p></div>
<div class="m2"><p>بر رقعه کمال تو شاهان پیاده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ بلند و همت عالیت گوییا</p></div>
<div class="m2"><p>هردو به هم ز یک رحم و صلب زاده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احباب تو به ذُوره دولت رسیده‌اند</p></div>
<div class="m2"><p>واعدات در حضیض مذلت فتاده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در امتثال حکم تو آزادگان دهر</p></div>
<div class="m2"><p>با سرو در چمن شب و روز ایستاده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری‌ست صاحبا که خطیبان خاطرم</p></div>
<div class="m2"><p>یکسر زبان به خطبه مدحت گشاده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دیدم از طریق فراست که بی‌مُکاس</p></div>
<div class="m2"><p>دست و دلت وظیفه ارزاق داده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم مگر که رسم تقاضا براوفتاد</p></div>
<div class="m2"><p>این رسم خود به طالع ثابت نهاده‌اند</p></div></div>