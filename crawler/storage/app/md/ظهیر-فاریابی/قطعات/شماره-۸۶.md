---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>خدایگانا معلوم رای روشن توست</p></div>
<div class="m2"><p>خلوص بندگی و شرط نیک خواهی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه آن کسم که مرا آن محل و مرتبه هست</p></div>
<div class="m2"><p>که کار ملک نکو نگردد از تباهی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن گدای سخن پیشه ام که وقت مدیح</p></div>
<div class="m2"><p>زنند خوش سخنان لاف پادشاهی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان مدح توام زنده و ز روی قیاس</p></div>
<div class="m2"><p>سجلّ مدح تو را در خورد گواهی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شب سیاهم ز اندوه و چشم می دارم</p></div>
<div class="m2"><p>که صبح عدل تو زایل کند سیاهی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روا مدار که عاجز شوند ماهی و مرغ</p></div>
<div class="m2"><p>بر اشک گرم و دم سرد صبحگاهی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهان به روزه و لب بر ثنای تو مپسند</p></div>
<div class="m2"><p>ز دیده تر شده رخسارهای کاهی من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا بخوان و گناهی مدان که معلومست</p></div>
<div class="m2"><p>همه جهان را احوال بی گناهی من</p></div></div>