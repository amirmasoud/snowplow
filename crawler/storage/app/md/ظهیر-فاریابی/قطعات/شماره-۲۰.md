---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای فلک سر بدان درآورده</p></div>
<div class="m2"><p>که تو گویی که خاک پای من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینت آسمان و زیور ماه</p></div>
<div class="m2"><p>عکس جام جهان نمای من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایبان سپهر نُه پوشش</p></div>
<div class="m2"><p>آستان در سرای من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حجتی کان زبان فتنه ببست</p></div>
<div class="m2"><p>سر تیغ جهانگشای من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی که عقل ذرّه اوست</p></div>
<div class="m2"><p>ذره ای ز آفتاب رای من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو جهان را به پشت پای زدی</p></div>
<div class="m2"><p>که کمین فضله سخای من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پایت آزرده شد ز صدمت آن</p></div>
<div class="m2"><p>خود همین ماجراگوی من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد در پایت اوفتاد به عذر</p></div>
<div class="m2"><p>که گناه من و خطای من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به پایت رسید آسیبم</p></div>
<div class="m2"><p>گر ببری سرم سزای من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل سوگند بر جهان می داد</p></div>
<div class="m2"><p>که اگر در سرت هوای من است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سر من که درد پاش بچین</p></div>
<div class="m2"><p>که تو دانی که بوسه جای من است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جاودان زی که چرخ می گوید</p></div>
<div class="m2"><p>که بقای تو با بقای من است</p></div></div>