---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>تاج بخش جهان سکندر وقت</p></div>
<div class="m2"><p>ای سزاوار افسر و دیهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گلستان افسرت هر دم</p></div>
<div class="m2"><p>به مشام فلک رسیده نسیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیرت اندر دل پر آتش خصم</p></div>
<div class="m2"><p>رفته گستاخ همچو ابراهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان در محیط همت تو</p></div>
<div class="m2"><p>نقطه ای در میان حلقه جیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل دشمن ز رمح چون الفت</p></div>
<div class="m2"><p>تنگ و تاریک همچو دیده میم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال من بنده هست معلومت</p></div>
<div class="m2"><p>که ز عصمت گرفته ام تعلیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدری وام کرده ام لیکن</p></div>
<div class="m2"><p>وجه یک جو ندارم از زر و سیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در من غریم کرده مقام</p></div>
<div class="m2"><p>همچو اقبال بر در تو مقیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از برای دوام این اقبال</p></div>
<div class="m2"><p>باز کن از سرم بلای غریم</p></div></div>