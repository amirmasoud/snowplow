---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای گشته تیر عشق غمت را نشانه جان</p></div>
<div class="m2"><p>وی گشته از وصال لبت جاودانه جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد سرای عشق تو جانا هزار در</p></div>
<div class="m2"><p>کو را بود به وصف نخست آستانه جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان جان یگانه ام که تو را بخشمی اگر</p></div>
<div class="m2"><p>بودی مرین شکسته دلم را دوگانه جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی بهای بوسه من هست جان و هست</p></div>
<div class="m2"><p>مقصودت آن که تا ببری بی بهانه جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بر خورند از رخ و زلف تو چشم و دل</p></div>
<div class="m2"><p>بر باد می شود به فسوس از میانه جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دل به دام حلقه زلفت در اوفتاد</p></div>
<div class="m2"><p>بر بوی آن که دید ز خال تو دانه جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین پس مدار قصد به جانم بتا،از آنک</p></div>
<div class="m2"><p>دارم فدای حرمت صدر زمانه جان</p></div></div>
<div class="b2" id="bn8"><p>عادل قوام ملک مبارک شهاب دین</p>
<p>صدری که هست طلعت او آفتاب دین</p></div>
<div class="b" id="bn9"><div class="m1"><p>ای حلقه های سنبل زلف تو دام دل</p></div>
<div class="m2"><p>وی مهر زر مهر تو دایم به نام دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تنگنای سینه که لشکر گه غم است</p></div>
<div class="m2"><p>شد دل غلام روی تو و جان غلام دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دل مقام داری و این طرفه ترکه هست</p></div>
<div class="m2"><p>پیوسته در کمند دو زلفت مقام دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیدار من به وصل لبت بیش ازین که هست</p></div>
<div class="m2"><p>شیرین شده ز یاد دهان تو کام دل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جانا صبوح عشق به آخر کجا رسد</p></div>
<div class="m2"><p>تا هست پر شراب هوای تو جام دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اسباب عیش و خرمن صبرم بسوخت پاک</p></div>
<div class="m2"><p>بر آتش غمت ز تمنای خام دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آزار جان من مطلب زانک داده ام</p></div>
<div class="m2"><p>در دست میر صدر خراسان زمام دل</p></div></div>
<div class="b2" id="bn16"><p>مقبل محمدبن ابی القاسم آنک هست</p>
<p>پیش علو همت او اوج چرخ پست</p></div>
<div class="b" id="bn17"><div class="m1"><p>بر سر بسی زدم ز غم عشق یار دست</p></div>
<div class="m2"><p>هم کار شد ز دست مرا هم ز کار دست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از پای از آن درآمده ام کز سر فسوس</p></div>
<div class="m2"><p>گویی که با تو عهد ببندم به یار دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عهد تو چون شکسته تر از بند زلف توست</p></div>
<div class="m2"><p>ای من غلام روی تو رنجه مدار دست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دارم پر از فراق تو چون کوکنار دل</p></div>
<div class="m2"><p>هستم تهی ز وصل تو همچون چنار دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه از خیال تو ببریده یمین دل</p></div>
<div class="m2"><p>نه از وصال تو بگزیده یسار دست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در پای هجر توست به بوی دو زلف توست</p></div>
<div class="m2"><p>دل چون چنار بازگشاده هزار دست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از من بدار دست که دارد به بندگی</p></div>
<div class="m2"><p>دل در رکاب صدر سپهر اقتدار دست</p></div></div>
<div class="b2" id="bn24"><p>صدری که روز ملک به رویش مبارکست</p>
<p>دم در گلوی دشمن ذاتش بلارکست</p></div>
<div class="b" id="bn25"><div class="m1"><p>صدری که بر سپهر نهاد از جلال پای</p></div>
<div class="m2"><p>نارد برو سپهر به گاه کمال پای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر خدمت درش متقاضی نیامدی</p></div>
<div class="m2"><p>بر تن نیافریدی خود ذوالجلال پای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا پایمال او شود از روی احترام</p></div>
<div class="m2"><p>دستش به جودبست جهان را به مال پای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در پیش دست حمله بخت جوان او</p></div>
<div class="m2"><p>نارد به گاه کینه کشی پور زال پای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از چرخ سیر مرکب او را شناس و بس</p></div>
<div class="m2"><p>کو را بود نهاده به تک بر هلال پای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر در میان نهاد که از خط بندگیش</p></div>
<div class="m2"><p>بیرون نهاده خصم بد بدفِعال پای</p></div></div>
<div class="b2" id="bn31"><p>صدری که بی نظیر جهان است گاه لطف</p>
<p>شخص بزرگوارش جان است گاه لطف</p></div>
<div class="b" id="bn32"><div class="m1"><p>ای ملک را ز روی تو بر آفتاب چشم</p></div>
<div class="m2"><p>گوهر فشان ز چشم تو دارد سحاب چشم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در عهد عدل توست که بر پاسبان و دزد</p></div>
<div class="m2"><p>یک چشم زخم یاد نکرده ست خواب چشم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همواره حاسدان تو را پر ز نار دل</p></div>
<div class="m2"><p>پیوسته دشمنان تو را پر ز آب چشم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در دست همچو بحر تو ماننده صدف</p></div>
<div class="m2"><p>کلک توراست مانده پر از در ناب چشم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از راه مهر جلوه گران سپهر را</p></div>
<div class="m2"><p>از گرد سم اسب تو دیده نقاب چشم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در بوستان سرای حیا همچو لاله کرد</p></div>
<div class="m2"><p>روی عدوی جاه تورا چون خضاب چشم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم وافر از ثنای تو دارد نصیب گوش</p></div>
<div class="m2"><p>هم کامل لز لقای تو دارد نصاب چشم</p></div></div>
<div class="b2" id="bn39"><p>ای خاک پایت افسر گردنکشان دهر</p>
<p>تریاق دوستی تو و دشمنیت زهر</p></div>
<div class="b" id="bn40"><div class="m1"><p>ای کرده از مدایح تو اهتزاز گوش</p></div>
<div class="m2"><p>وی داشته به در ثنایت نیاز گوش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در سر شده ز طلعت تو چشم مه نمای</p></div>
<div class="m2"><p>بر جان شده ز مدحت تو دلنواز گوش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گشت از حواس سمع ضروری و روز و شب</p></div>
<div class="m2"><p>کرد از شنید نام عدوت احتراز گوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا در کند ز قطره نیسان نطق تو</p></div>
<div class="m2"><p>از دل دهان بسان صدف کرد باز گوش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای صاحب کبیر وزیران روزگار</p></div>
<div class="m2"><p>دارند جانب شعرا را به ناز گوش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من بنده دیرمند شدم وقت فرقت است</p></div>
<div class="m2"><p>یک نکته دارد از کرمت دلنواز گوش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صدرا،زیان ندارد اگر چون منی رسد</p></div>
<div class="m2"><p>از چون تویی به قیمت یک سر دراز گوش</p></div></div>
<div class="b2" id="bn47"><p>تاذکر همتت به جهان در سمر کنم</p>
<p>گوش فلک ز مدحت تو پر گهر کنم</p></div>
<div class="b" id="bn48"><div class="m1"><p>صدرا،همیشه دست چوکانت گشاده باد</p></div>
<div class="m2"><p>پایت به قهر بر سر گردون نهاده باد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرزین ملک شاهی و رخ برده پیش پیل</p></div>
<div class="m2"><p>خصمت ز پشت اسب تحمل پیاده باد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر روز صد هزار زبان در به مدح تو</p></div>
<div class="m2"><p>در بندگی چون سوسن آزاد زاده باد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خون عدوی ذات تو مانند باده گشت</p></div>
<div class="m2"><p>عقل حسود جاه تو مانند باده باد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هرکس که نیست جان و دلش در هوای تو</p></div>
<div class="m2"><p>در دست و پای فتنه گردون فتاده باد</p></div></div>
<div class="b2" id="bn53"><p>چون فتنه در جهان ز نهایت فرو نشست</p>
<p>مانند بخت چرخ به پیشت ستاده باد</p></div>