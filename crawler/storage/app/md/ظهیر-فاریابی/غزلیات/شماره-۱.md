---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>دلم چون بر سر زلف تو جان بست</p></div>
<div class="m2"><p>برو عشقت در هر دو جهان بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر زلفت چو زین حالت خبر یافت</p></div>
<div class="m2"><p>به قصد جان من جان در میان بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را کاین رسم و آیین باشد ای جان</p></div>
<div class="m2"><p>چه بار از وصل تو بر خر توان بست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطافت در جهان روی تو آورد</p></div>
<div class="m2"><p>صبا چیزی از آن بر گلستان بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر در نی چو خطت سبز و تردید</p></div>
<div class="m2"><p>از آن خود را در آن شیرین دهان بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لبت را لعل خوانم نه ز کان خون</p></div>
<div class="m2"><p>ز سودای لبت در عرق کان بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندانم تا چه می خوانی تو زان لب</p></div>
<div class="m2"><p>مرا باری در آن معنی زبان بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دری کز فتنه بر روی تو بگشود</p></div>
<div class="m2"><p>به دست معدلت صاحب قران بست</p></div></div>