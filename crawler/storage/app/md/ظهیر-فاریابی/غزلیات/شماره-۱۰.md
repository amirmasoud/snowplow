---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>بگذشت ماه روزه به خیر و مبارکی</p></div>
<div class="m2"><p>پر کن قدح ز باده گلرنگ راوکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبی که گر برابر آتش بداریش</p></div>
<div class="m2"><p>واجب شود عبادت او نزد مزدکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای شعر بنده چو بلبل که پر شود</p></div>
<div class="m2"><p>سمع خدایگان ز نوای چکاوکی</p></div></div>