---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>یار میخواره من دی قدحی باده به دست</p></div>
<div class="m2"><p>با حریفان ز خرابات برون آمد مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر در صومعه بنشست و سلامی در داد</p></div>
<div class="m2"><p>سرِ خُم را بگشاد و در غم را بربست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل هر دیو دل از ما که بدید آن مه نو</p></div>
<div class="m2"><p>گشت آشفته و دیوانه و زنجیر گسست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف زنجیر وَشش کز سر ایمان برخاست</p></div>
<div class="m2"><p>رقم کفر به ما بر بنشاند و بنشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پشت بر صومعه کردیم و سوی بتکده روی</p></div>
<div class="m2"><p>خرقه را پاره بکردیم و همه توبه شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با حریفان قلندر به خرابات شدیم</p></div>
<div class="m2"><p>زهد بر هم زدهء کاسه به کف، کوزه به دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ظهیر از سر آن زلف گشادیم گره</p></div>
<div class="m2"><p>که کمینه گرهی دارد ازو پنجه شست</p></div></div>