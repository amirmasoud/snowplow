---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ابا حسن أیقنت حُبَک منقذی</p></div>
<div class="m2"><p>ولو بذنوب الخلق کنت محاسِبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و أنت مُنیٰ قلبی و روحی و مهجتی</p></div>
<div class="m2"><p>ولستُ أریٰ قلبی لغیرک راغبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و قام رسول الله فیک بمعشر</p></div>
<div class="m2"><p>و صادع بالوحی الجلیل و خاطبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فمن انا مولاه فهذا ولیه</p></div>
<div class="m2"><p>و ولاک علی جل الخلافه اوجبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>أتیتک یا مولی الانام و موْئلی</p></div>
<div class="m2"><p>قدمت معاذاً للطریق و مذهبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فدیتک یا دینی و دنیا و ملّتی</p></div>
<div class="m2"><p>و فی مذهب الاخلاص لست معاتبا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیا عترهٔ الاطهار من لی غیرکم</p></div>
<div class="m2"><p>و أسعد مَن أنتم رجاه و اطیبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عسی الله أن یعفو العثار بحبّکم</p></div>
<div class="m2"><p>اماط بکم رجس الذنوب و أذهبا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علقتُ یدی حبّا بحبل ولائکم</p></div>
<div class="m2"><p>فوالله بالزّلّات لستُ معاقبا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طربتُ بحان العشق مِن کأس حبّکم</p></div>
<div class="m2"><p>سقانی شراباً ما الذ و اعذبا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>أبا الله الّا أن یتمّ بنوره</p></div>
<div class="m2"><p>و لو کره الفجار طغیانهم و ابا</p></div></div>