---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>کفی الدهر برداً لارتعاد مفاصلی</p></div>
<div class="m2"><p>ألست بکافٍ عبرهًٔ للافاضل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صعدت مراقی المجد و العلم والعلیٰ</p></div>
<div class="m2"><p>فلم أرَ رحباً لیتنی فی الاسافل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سقانی کؤُوس السمّ کفّی سماحهٔ</p></div>
<div class="m2"><p>حوتنی الرّزایا باکتساب الفضائل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و فی طخیهٌٔ عمیاء عُشتُ منکّراً</p></div>
<div class="m2"><p>یُفارقنی ظلّی لفقد الاماثل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندیمی شجون السّجن فی خَبَل الهویٰ</p></div>
<div class="m2"><p>کفانی زفیری عن صفیر العنادل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تسامرنی الازمان بُعداً من الکریٰ</p></div>
<div class="m2"><p>یکرّرنی الاسجاع هزّ الغوائل</p></div></div>