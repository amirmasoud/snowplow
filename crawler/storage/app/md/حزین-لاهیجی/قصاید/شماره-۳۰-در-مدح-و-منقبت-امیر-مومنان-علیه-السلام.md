---
title: >-
    شمارهٔ ۳۰ - در مدح و منقبت امیر مومنان علیه السلام
---
# شمارهٔ ۳۰ - در مدح و منقبت امیر مومنان علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ای نگاهت به صید دل، بازی</p></div>
<div class="m2"><p>مژه ها جمله در سنان بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه دل می بری به عشوه و ناز</p></div>
<div class="m2"><p>بی نیازا، نباز در بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به ساغر کنم شراب بهشت</p></div>
<div class="m2"><p>نکند با نگاهت انبازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برفروزی ز باده چون به چمن</p></div>
<div class="m2"><p>گل سوری به بوته بگدازیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع رویت کند به محفل دل</p></div>
<div class="m2"><p>پرده سوزی و انجمن سازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده ای در مصاف شیردلان</p></div>
<div class="m2"><p>تیغ هندی به غمزهٔ غازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده سویت روان تپیدن دل</p></div>
<div class="m2"><p>نامه همراه رنگ پردازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع سر درکشد چو در محفل</p></div>
<div class="m2"><p>رخ برافروزی و قد افرازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غمت دیده ام کف طایی</p></div>
<div class="m2"><p>با خیالت دل اشعب آزی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبر، نازد به خویشتن، وقت است</p></div>
<div class="m2"><p>دست و تیغی به امتحان یازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در پریخانهٔ تو یاد گرفت</p></div>
<div class="m2"><p>باده شوخی و شیشه طنازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از می حسن و شور عشق کند</p></div>
<div class="m2"><p>جلوه، مستی و غمزه غمّازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نقش هر هفت خال راند لبت</p></div>
<div class="m2"><p>ضربه بستانکه بردهای بازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در غمت نالهٔ عراق سروش</p></div>
<div class="m2"><p>شده بر من، سموم اهوازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به دل آساییم ز غنچهٔ تر</p></div>
<div class="m2"><p>مگر آبی بر آذر اندازی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وقت آن شد که در زمانه حزین</p></div>
<div class="m2"><p>کج نهی افسر سخن سازی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وقت آن شد کز اژدهای قلم</p></div>
<div class="m2"><p>کاویانی علم، برافرازی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وقت آن شد که در مدیح کند</p></div>
<div class="m2"><p>دل پرشور، سینه پردازی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مدح تارک فراز هفت اورنگ</p></div>
<div class="m2"><p>آبرو بخش ملت تازی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه مردان علی که منقبتش</p></div>
<div class="m2"><p>خامه را می دهد سرافرازی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن که در عرصهٔ شهیدانش</p></div>
<div class="m2"><p>کرده خضر آرزوی جان بازی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آسمانش کند سلحشوری</p></div>
<div class="m2"><p>آفتابش کند سراندازی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کرده از لهجهٔ نوایش کسب</p></div>
<div class="m2"><p>نسر طایر، بلند پروازی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در ثنایش به عرشیان دارد</p></div>
<div class="m2"><p>مرغ روحم، سر هم آوازی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>می کند از نوای مدحت او</p></div>
<div class="m2"><p>خامهٔ جبرئیل دمسازی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کند از فیض او به مرده دلان</p></div>
<div class="m2"><p>نفسم پور مریم اعجازی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیش تمکین او عنان بکشد</p></div>
<div class="m2"><p>توسن عمر از سبکتازی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روز محشر به پرده داری او</p></div>
<div class="m2"><p>می نیابد زمانه، همسازی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرورا، با لب ثناگر تو</p></div>
<div class="m2"><p>کرده روح القدس هم آوازی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خنگ گردون کند فرامش، تک</p></div>
<div class="m2"><p>چون به میدان تکاور اندازی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با غبار آسمان رود از جا</p></div>
<div class="m2"><p>در مصافی که حمله آغازی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بهر خوان تو در تنور فلک</p></div>
<div class="m2"><p>مهر و مه راست، پیشه خبازی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>می کند خیل شبروان تو را</p></div>
<div class="m2"><p>قصب ماهتاب، بزازی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زخمهٔ شیونم تغافل توست</p></div>
<div class="m2"><p>می خروشم اگر تو ننوازی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لب گشایی اگر به تحسینم</p></div>
<div class="m2"><p>دل سوزان به کوثر اندازی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چه کم از کیسهٔ کرم شودت</p></div>
<div class="m2"><p>گر به حال دلم بپردازی؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون تو گیری به دست خامه حزین</p></div>
<div class="m2"><p>کلک مانی کجا و انبازی؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قلم واسطی نژاد تو کرد</p></div>
<div class="m2"><p>صفحه، همرنگ آل شیرازی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>انوری بود اگر خدیو سخن</p></div>
<div class="m2"><p>زد نوای تو کوس ممتازی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرغ آمین ز آسمان آید</p></div>
<div class="m2"><p>چون تو کف در دعا برافرازی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دل و دین در پناه عدل تو باد</p></div>
<div class="m2"><p>تا ستم راست شیوه مجتازی</p></div></div>