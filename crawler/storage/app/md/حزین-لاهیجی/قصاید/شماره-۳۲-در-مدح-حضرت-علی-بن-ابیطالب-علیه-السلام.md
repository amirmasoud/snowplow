---
title: >-
    شمارهٔ ۳۲ - در مدح حضرت علیّ بن ابیطالب علیه السلام
---
# شمارهٔ ۳۲ - در مدح حضرت علیّ بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>نظر کن در سواد صفحه ام تا گلستان بینی</p></div>
<div class="m2"><p>گذر کن دفترم را تا بهار بی خزان بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صریر خامه ام در طاق هفتم آسمان یابی</p></div>
<div class="m2"><p>صفیر ناله ام را گوشوار عرشیان بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوه عشق بخشیده ست اقبال فریدونم</p></div>
<div class="m2"><p>قلم را در بنان من، درفش کاویان بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز لفظ آهنین پیکر،که داوود خرد بافد</p></div>
<div class="m2"><p>کمیت خامه ام را بر کتف برگستوان بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین در نقطه ام تا چشم معنی گرددت روشن</p></div>
<div class="m2"><p>بگیر این لقمه را تا حکمت لقمانیان بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به لفظ آغوش واکن تا به دامانت گهر ریزد</p></div>
<div class="m2"><p>به معنی گوش بگشا تا لبم را ترجمان بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز من پیمانه بستان تا حیات جاودان یابی</p></div>
<div class="m2"><p>می از این جام جمشیدی بکش تا نور جان بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیی چون مرد معنی، یاوه سنجی چون جرس تا کی؟</p></div>
<div class="m2"><p>به دنبال زبان خود مرو ترسم زیان بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تقلید و قیامت کی فروغ معرفت خیزد؟</p></div>
<div class="m2"><p>من از آتش دخان بینم، تو آتش از دخان بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبندی دل به افسونی که طبع خفته شکل آرد</p></div>
<div class="m2"><p>ز بیداران شنو تا سر معنی را عیان بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز یک مشت استخوان سگ می کند پهلوی خود فربه</p></div>
<div class="m2"><p>به اندک مایه ای نفس دنی را شادمان بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به بوی بی بقایی مغز خامت مست می گردد</p></div>
<div class="m2"><p>به رنگ مستعاری خویشتن را بوستان بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو نرگس دیده، محو رنگ و بو کردی نمی دانی</p></div>
<div class="m2"><p>که مژگان تا زنی برهم نه این بینی نه آن بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل حسرت نصیبی ها بود چون غنچه دل بستن</p></div>
<div class="m2"><p>بهاری را که در دنباله، باد مهرگان بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازین زندان ظلمانی برون آور سر، ای غافل</p></div>
<div class="m2"><p>که انوار صفا در محفل روحانیان بینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هوای نفس و طبعت خار در جیب و بغل ریزد</p></div>
<div class="m2"><p>گل این شاخساران، دست فرسود خزان بینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سموم دوزخ از بویت نسیم خلد می گردد</p></div>
<div class="m2"><p>اگر در دل هوای پیشوای انس و جان بینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سر مردان عالم شهسوار لافتی یعنی</p></div>
<div class="m2"><p>علی مرتضی کز وی دل و جان کامران بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرم را در هوایش عرش عزت در قدم یابی</p></div>
<div class="m2"><p>دلم را از ولایش چون بهشت جاودان بینی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز زهر آلوده تیغ معصیت ایمن بود جانت</p></div>
<div class="m2"><p>چو بر بازوی ایمان حب او حزر امان بینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلیخایی کند در مصر حسنش جان آگاهان</p></div>
<div class="m2"><p>هزاران بخت پیر از دولت عشقش جوان بینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درآ در آستانش پایهٔ رفعت تماشا کن</p></div>
<div class="m2"><p>ببین در زیر پا، تا نه رواق آسمان بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشان پاکی طینت بود در سینه ها مهرش</p></div>
<div class="m2"><p>دغل رسوا شود هر جا که سنگ امتحان بینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چها باشد ز احسانش شب آهنگان طاعت را</p></div>
<div class="m2"><p>سیه روزان عصیان را چو عفوش طیلسان بینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به مسروران جنت، لطف او را مهربان یابی</p></div>
<div class="m2"><p>به مقهوران دوزخ، قهر او را قهرمان بینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنی گرگوش دل، محوکلام معجز آیاتش</p></div>
<div class="m2"><p>هزاران گنج معنی زبر هر حرفی نهان بینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غبار آستانش سرمه در چشم ملک ساید</p></div>
<div class="m2"><p>به راهش نقش پا را تاج فرق فرقدان بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ملک چاکر شهنشاها، به دل کوه غمی دارم</p></div>
<div class="m2"><p>که لب را گر گشایم، چشمه سار خون روان بینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر خواهی بگو تا آستین از دیده بردارم</p></div>
<div class="m2"><p>که مژگان مرا از گریه شاخ ارغوان بینی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز حرمان سر کویت به خاطر حسرتی دارم</p></div>
<div class="m2"><p>که داغم را چو نی در کوچه بند استخوان بینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خوش آن دولت که یکبار دگر هم آستان بوسم</p></div>
<div class="m2"><p>دلم را در تپیدن چون درای پاسبان بینی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به گرد روضه ات گردم روان از سر قدم کرده</p></div>
<div class="m2"><p>به خلدم خنده زن یابی به چرخم سرگران بینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حزین حلقه در گوشم، غلامی از غلامانت</p></div>
<div class="m2"><p>به عزّت سوی خود خوان چون اسیرم در هوان بینی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به عشق از التهاب آتش دل عاجزم عاجز</p></div>
<div class="m2"><p>اگرکمتر لبم را در ثنا، رطب اللسان بینی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ورق در دست من بال و پر پروانه می گردد</p></div>
<div class="m2"><p>قلم را در بنانم شمع سان آتش به جان بینی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به محشر چشم آن دارم که خیل جان نثاران را</p></div>
<div class="m2"><p>کنی گر گوشه ی چشمی مرا هم در میان بینی</p></div></div>