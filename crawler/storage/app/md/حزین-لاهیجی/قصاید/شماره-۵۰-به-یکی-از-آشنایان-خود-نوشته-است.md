---
title: >-
    شمارهٔ ۵۰ - به یکی از آشنایان خود نوشته است
---
# شمارهٔ ۵۰ - به یکی از آشنایان خود نوشته است

<div class="b" id="bn1"><div class="m1"><p>کشور هند که بادا بری از خوف و خلل</p></div>
<div class="m2"><p>آفتابیش فرازندهٔ قدر است و محل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز شرف سایهٔ آن باج نهد برخورشید</p></div>
<div class="m2"><p>شرف از پایهٔ او وام کند اوج زحل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب فلک اعظم دولت، دِدی دَت</p></div>
<div class="m2"><p>کآفتاب فلکش کرده رقم، عبداقل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه اش گر به صنم خانه گذار اندازد</p></div>
<div class="m2"><p>عزّت او شکند رونق عزّی و هبل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق نوشین لب او کرده به شیرین کاری</p></div>
<div class="m2"><p>دل پرشور مرا خانهٔ زنبور عسل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوق سودازدگان راه نفس راگیرد</p></div>
<div class="m2"><p>نشتر غمزهٔ او،گر نگشاید اکحل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گرانقدری او لنگر تمکین فکند</p></div>
<div class="m2"><p>تاگلو غرق کند گاو زمین را به وحل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرد پیر نیارد گره از کار گشود</p></div>
<div class="m2"><p>هست این عقده مگر نزد خرد لاینحل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از صیاح عرب و فرس و فرنگ و لُر و هند</p></div>
<div class="m2"><p>هر چه مستعمل او نیست، شمارم مهمل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یاد آن غمزه اگر در دل بسمل گذرد</p></div>
<div class="m2"><p>لذّت نوش دهد نشتر چون خار اجل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیزهٔ خطی او کرده به اجرام سپهر</p></div>
<div class="m2"><p>آن تطاول که مگر میل کند با مکحل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فیل گردون صفت او چو درآید به خرام</p></div>
<div class="m2"><p>افکند صدمهٔ او، گاو زمین را به وحل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حبّذا این چه شکوه است و چه جاه و چه جلال؟</p></div>
<div class="m2"><p>مرحبا، این چه علوّ است و چه شأن و چه محل؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نگیرد سخنم نازکی از فکر دقیق</p></div>
<div class="m2"><p>دارم اندیشهٔ آن موی میان را به بغل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای سرآمد به هنر بر همه اصحاب کمال</p></div>
<div class="m2"><p>وی سر و سرور و سردفتر ارباب محل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخنی چند سزاوار تو دارم بشنو</p></div>
<div class="m2"><p>در جهان ما صدق نکتهٔ ما قلَّ و دَل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کِشت دولت نشود سبز،گر از خون عدو</p></div>
<div class="m2"><p>جوهر تیغ تو، جاری ننماید جدول</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر تو ای جان جهان پا نگذاری به میان</p></div>
<div class="m2"><p>کار ایّام شود چون تن بی جان، مهمل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جُست در هند بسی، دیدهٔ انصاف و نیافت</p></div>
<div class="m2"><p>جز تو شایان نثار گهر مدح و غزل</p></div></div>