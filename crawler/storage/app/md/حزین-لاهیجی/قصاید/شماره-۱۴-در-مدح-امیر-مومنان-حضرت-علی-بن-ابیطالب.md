---
title: >-
    شمارهٔ ۱۴ - در مدح امیر مومنان حضرت علی بن ابیطالب
---
# شمارهٔ ۱۴ - در مدح امیر مومنان حضرت علی بن ابیطالب

<div class="b" id="bn1"><div class="m1"><p>در زیر لب آوازه شکستیم فغان را</p></div>
<div class="m2"><p>گوشی بنما تا بگشاییم زبان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد سامعه ها چشمهٔ سیماب، گشاید</p></div>
<div class="m2"><p>دیگر صدف ما به چه امّید دهان را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاده ز جمع آوری، آشفته حواسم</p></div>
<div class="m2"><p>شیرازه فروریخته اوراق خزان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صبح اگر سینه، دم سرد گشاید</p></div>
<div class="m2"><p>خاکی به دهان ریز ملامت نگران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور عجبی گردش این دایره دارد</p></div>
<div class="m2"><p>وقت است که گردون بگذارد دوران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکنون اثر تربیت دهر بر آن است</p></div>
<div class="m2"><p>تا صورت خرمهره دهد نطفه ى کان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین گاو و خرانی که درین مرتع خارند</p></div>
<div class="m2"><p>حیرت سبل نور نظر شد دبران را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برخاسته زین شور زمین، چند بخاری</p></div>
<div class="m2"><p>یک سر به کف غول هوا داده عنان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خجلت دِهِ طبع دژم از صورت شخصی</p></div>
<div class="m2"><p>بدنام کن از نسبت نوعی، حیوان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این تیره نهادان که درین دایره هستند</p></div>
<div class="m2"><p>جا تنگ نمودند میان را و کران را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کردند ز تجدید رسوم این رمه ى شوم</p></div>
<div class="m2"><p>عزل از عمل خود خرد قاعده دان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیمرغ خود و قوّت پرواز مگس نیست</p></div>
<div class="m2"><p>بال و پر این هیچ کسان همه دان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بردند ز ما مفت و به ما باز فروشند</p></div>
<div class="m2"><p>بیعانه ى این شرم توان داد جهان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یاد است مرا این سخن از تجربه کاران</p></div>
<div class="m2"><p>رخساره شجاعت نسبی حیز جبان را؟!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>افسرده دلی بر خرد پیر چه آرد؟</p></div>
<div class="m2"><p>اوضاع جهان پیر کند طبع جوان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیر خردم گفت ازین کار بکش دست</p></div>
<div class="m2"><p>سرمایه به دامان نتوان کرد زیان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این گلخنیان گرسنه از مایه ى جهلند</p></div>
<div class="m2"><p>از نکهت گل باز ندانند دخان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیو امّت دعوى ست، سلیمان نبی کو؟</p></div>
<div class="m2"><p>بنگر به کیان داده فلک جای کیان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جیب خریدار بها گرد کسادی ست</p></div>
<div class="m2"><p>سودت بود آنگه که کنی تخته دکان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با لخت جگر رخنه ى منقار فروبند</p></div>
<div class="m2"><p>دود نفس داغ، گرفته ست جهان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ناخن به خراش دل خوددارکه عار است</p></div>
<div class="m2"><p>دم لابهء روبه صفتان، شیر ژیان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خونابه مریز این همه، آن به که به خشکی</p></div>
<div class="m2"><p>بندد رگ تاکِ قلمت رَه، سیلان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر طاق بلندی قلم از دست فکندم</p></div>
<div class="m2"><p>بازوی که تا می کشد این سخت کمان را؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من دست به دل داده به پیمان خموشی</p></div>
<div class="m2"><p>عشق آمد و از سینه به لب ریخت فغان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کای صبح نفس روزنه ى فیض نبندی</p></div>
<div class="m2"><p>ز آهنگ سگان ره نگذارد سیران را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گو اشرف خر، جمع کند مظلمهٔ خلق</p></div>
<div class="m2"><p>انصاف مبدّل نکند سیرت وشان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر خربطی آواز دهد، وقت مشوران</p></div>
<div class="m2"><p>از نغمهٔ چغزان چه زیان آب روان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر خود ستمی کرده، نه بر نکهت عنبر</p></div>
<div class="m2"><p>گنده بغلی، گر شکند غالیه دان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در کشور معنی تویی امروز سکندر</p></div>
<div class="m2"><p>از صورت زشتان چه غم آیینه گران را؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر علم چه نقصان اگر از جهل بلافند</p></div>
<div class="m2"><p>این مشت عوان زاده که عارند جهان را؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خر عرعر و کبک از لب پرخنده زند دم</p></div>
<div class="m2"><p>از قهقهه فرق است فراوان غثیان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا حقد و حسد هست، پریشان سخنی هست</p></div>
<div class="m2"><p>هنجار نفس راست نباشد خفقان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رنجور حسد چاره ای از خبث ندارد</p></div>
<div class="m2"><p>بیمار نهفتن نتواند هذیان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نبود عجبی از سگ دیوانه گزیدن</p></div>
<div class="m2"><p>عقرب به سر نیش گشاید رگ جان را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>معذور بود جاهل دیوانه، که باشد</p></div>
<div class="m2"><p>اوهام خیالات بسی خواب گران را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگذار به هم بادیه و بادیه گردان</p></div>
<div class="m2"><p>در کعبه دل یافته ای امن و امان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>طوطی به شکر می تند و زاغ به جیفه</p></div>
<div class="m2"><p>گرگ است پی کاری و کاری ست شبان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بلبل به گلستان برد آغوش گشاده</p></div>
<div class="m2"><p>در بیشهٔ خود، نیک جعل بسته میان را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خر، گرم نهیق است به ارشاد طبیعت</p></div>
<div class="m2"><p>بیچاره چه سازد که نیاموخت زبان را؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در صیدگه، ارزان گوزنان شکر و شیر</p></div>
<div class="m2"><p>مه نور خورد، مور برد ذرهٔ خوان را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از قسمت فیّاض ازل تعبیه دارد</p></div>
<div class="m2"><p>معنی به لسان نی کلکت بلسان را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یا از اثر مدح شهنشاه عطابخش</p></div>
<div class="m2"><p>کردهست لبت، طبلهٔ پرنوش، دهان را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن شاه که در صید معانی ثنایش</p></div>
<div class="m2"><p>چنگال به جایی نرسد ببر بیان را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سالار هدی، عروهٔ وثقای الهی</p></div>
<div class="m2"><p>اورنگ نشین، مملکت عزت و شان را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یعسوب جهان حیدر کرّار که نامش</p></div>
<div class="m2"><p>در کام، به شیرینی جان کرده زبان را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جست از صف کین، لمعه ی خورشید ثنایش</p></div>
<div class="m2"><p>زد در بدن ابر، رگ برق دمان را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سرپنجه ی شیران عجم، مور بتابد</p></div>
<div class="m2"><p>رحمش به ضعیفان چو دهد تاب وتوان را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>منعش چو دهد حادثه را تاب عتابی</p></div>
<div class="m2"><p>بر گوشه نهد ابلق دوران جولان را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خلقش چو کند تربیت طبع رذایل</p></div>
<div class="m2"><p>رونق، ملخ حرص دهد مزرع جان را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بر کوه کند سایه اگر ابر حسامش</p></div>
<div class="m2"><p>از ژاله ستاند دیت لاله ستان را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بردارد اگر باد کفش دست تسلی</p></div>
<div class="m2"><p>گیرد دل دریا، تب و تاب عطشان را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شرع کهن ناطقه را نسخ نماید</p></div>
<div class="m2"><p>جایی که گشاید لب اعجاز بیان را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر خاک درش سرمه کند دیدهء اعمی</p></div>
<div class="m2"><p>خواند به شب از لوح قضا راز نهان را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بیجاده اگر همّت آن حوصله یابد</p></div>
<div class="m2"><p>بی وزن تر از سرمه، کشد کوه گران را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بی نشئهٔ فیض نظر خاک ره او</p></div>
<div class="m2"><p>تعمیر نکردند خرابات مغان را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خاکستر آن شمع که در روضهء او سوخت</p></div>
<div class="m2"><p>شد غالیه سا، طرّهٔ خیرات حسان را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ریزد پر جبریل به جولانگه مدحش</p></div>
<div class="m2"><p>هان، ای نفس گرم نگهدار عنان را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شاها تویی آن بنده نوازی که غلامت</p></div>
<div class="m2"><p>غیر از تو ندانسته، نه بهمان نه فلان را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در پیش من از دولت و اقبال تو گیتی</p></div>
<div class="m2"><p>خاکی س‍ت که در کاسه کنم قیصر و خان را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا داشته ای بر سر من دست حمایت</p></div>
<div class="m2"><p>بر تارک خورشید زنم چتر کیان را</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مه کاسهٔ دریوزه اگر پیش تو دارد</p></div>
<div class="m2"><p>مهتاب شود مرهم ناسور، کتان را</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گر خلق تو جانی به تن نامیه بخشد</p></div>
<div class="m2"><p>بیرون کند از باغ جهان، رسم خزان را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بیچاره نصیری چه کند، مرد یقین کیست؟</p></div>
<div class="m2"><p>پی گم شده در راه ولای تو گمان را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آوازهٔ بازوی عدو گیر تو از بیم</p></div>
<div class="m2"><p>ناخن کند از پنجه برون، شیر ژیان را</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>روزی که به ناورد هژبران قوی چنگ</p></div>
<div class="m2"><p>پرواز دهد دست تو شاهین کمان را</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گیسوی ظفر تاب دهد طرهٔ پرچم</p></div>
<div class="m2"><p>سرخاب عدو غازه کشد، مهچهٔ آن را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شمشیر نباید خم ابروی پر از چین</p></div>
<div class="m2"><p>خنجر بجهاند مژهٔ آفت جان را</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>با زخمه برد گوش به تن چرم گوزنان</p></div>
<div class="m2"><p>حلقوم درد نای پرآوازه دهان را</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>از هم گسلد خام رگ اندر تن گردان</p></div>
<div class="m2"><p>در هم شکند گرز گران بُرز یلان را</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>فتح آید و مستانه دهد بوسه رکابت</p></div>
<div class="m2"><p>چرخ آید و قربان شود آن دست و عنان را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شاها منم آن بندهٔ دیرینه که نامم</p></div>
<div class="m2"><p>چون شهرت خورشید گرفته ست جهان را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>امروز دو قرن است کزین خامه عطارد</p></div>
<div class="m2"><p>دریوزه کند فیض و برد نفع قران را</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>در شش جهت این کوس که اقبال هنر کوفت</p></div>
<div class="m2"><p>آوازهٔ بیهوده فروشد ملکان را</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در معرکه ها، بحر یسار است، یمینم</p></div>
<div class="m2"><p>بی آب کند خامهٔ من تیغ یمان را</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گردد لب جادو نفسان زخمی دندان</p></div>
<div class="m2"><p>گیرم چو به کف خامهٔ اعجاز نشان را</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از دولت مدحت همه سود است زیانم</p></div>
<div class="m2"><p>نتواند ادا کرد دلم شکر زیان را</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چون صوفی شوریده درون در طرب آرد</p></div>
<div class="m2"><p>گلبانگ صریر قلمم سرو نوان را</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هر جا که برآید دم جان پرور کلکم</p></div>
<div class="m2"><p>در طبله کند آن نفس مشک فشان را</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>در شقّ انامل چو بجنبد قلم من</p></div>
<div class="m2"><p>کور از رگ خارا بشمارد ضربان را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در تیره شب هند شود راه نفس گم</p></div>
<div class="m2"><p>با آن که لبم شعله فروز است، فغان را</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>در سرمهٔ این خاک سیه، خفته خروشم</p></div>
<div class="m2"><p>وین زمزمه شورانده زمین را و زمان را</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سرچشمهٔ حیوان کلامم به سیاهی ست</p></div>
<div class="m2"><p>وین آب روان بخش گرفته ست جهان را</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از طنطنهٔ باد بهار نفس من</p></div>
<div class="m2"><p>چون غنچه کنون قافیه تنگ است خزان را</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مجنون تو روزی که به صحرای نجف بود</p></div>
<div class="m2"><p>دل سجده بر از ذوق مکین، را و مکان را</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بر تارک عزّت گل تجرید شکفتم</p></div>
<div class="m2"><p>نشناخته پای شرفم خار هوان را</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آتش به نهاد فلک افتاد ز رشکم</p></div>
<div class="m2"><p>در قبضهٔ آوارگیم داد عنان را</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>خصمانه حسد برد برآن ناز و تنعّم</p></div>
<div class="m2"><p>بازوی قضا تیر به زِه داشت کمان را</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>القصّه، درین بتکده افتاده ام امروز</p></div>
<div class="m2"><p>مالیده به رخسار چو صندل یرقان را</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بر دوش دل عاجز بی تاب و تحمّل</p></div>
<div class="m2"><p>بربسته ز بار غم خود کوه گران را</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>خواهم که به کوی تو رسد باز غبارم</p></div>
<div class="m2"><p>پیرانه سر، آغوش گشا بخت جوان را</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دور از تو بسی تلخی ایّام چشیدم</p></div>
<div class="m2"><p>دانی تو که یارای بیان نیست زبان را</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>از رفعت شانم، هدف تیر حوادث</p></div>
<div class="m2"><p>گردن کشی از پای درآورد نشان را</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شرم عدم ناطقه و شعلهٔ شوقت</p></div>
<div class="m2"><p>ریزد عرق از ناصیه، حسّان زمان را</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>لیکن چه کنم، چون نبود صبر و قناعت</p></div>
<div class="m2"><p>در مدح و ثنایت دل شوریده بیان را؟</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مشتاب حزین این همه گستاخ، عنان کش</p></div>
<div class="m2"><p>میدان غمت هیچ ندانسته کران را</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>دستی به دل تنگ نه ای شور قیامت</p></div>
<div class="m2"><p>از خامه شدی چهره گشا باغ جنان را</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>هر حادثه بگذشته و بگذشته حساب است</p></div>
<div class="m2"><p>پایندگی این است جهان گذران را</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چندان که درین کارگه انواع موالید</p></div>
<div class="m2"><p>از عالم ارواح پذیرد سریان را</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>تا ماه برد مایهٔ اشراق ز خورشید</p></div>
<div class="m2"><p>تا مهر دهد نور سریر سرطان را</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>در پیکر والاگهران نور فزاید</p></div>
<div class="m2"><p>از فیض تولّای تو آیینهٔ جان را</p></div></div>