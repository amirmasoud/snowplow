---
title: >-
    شمارهٔ ۱۵ - در مدح حضرت علی بن ابیطالب امیر مؤمنان
---
# شمارهٔ ۱۵ - در مدح حضرت علی بن ابیطالب امیر مؤمنان

<div class="b" id="bn1"><div class="m1"><p>با همه سیلی که شسته روی زمین را</p></div>
<div class="m2"><p>طرفه غباری ست چشم حادثه بین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار الم بی حد است و گرد کدورت</p></div>
<div class="m2"><p>پشت فلک را ببین و روی زمین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشهٔ امنی که هست وادی جهل است</p></div>
<div class="m2"><p>فتنه چو بر بخردان گشاده کمین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حادثه بگرفته از دو سو به میانم</p></div>
<div class="m2"><p>کاش ندانستمی یسار و یمین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبح دهان را چرا به خنده ندرّد؟</p></div>
<div class="m2"><p>کز دم دیو است طعنه روح امین را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شام چرا زلف مشکبار نبرّد؟</p></div>
<div class="m2"><p>طفل رسنباز برده حبل متین را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش جهان از چه واژگونه نگردد؟</p></div>
<div class="m2"><p>کاهرِمَن از جم ربوده است نگین را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در همه گیتی که دیده است که افتد</p></div>
<div class="m2"><p>با دم روبه مصاف شیر عرین را؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کون خری بین که در زمانه کشیده ست</p></div>
<div class="m2"><p>خر به رخ آفتاب، داغ سرین را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دین و خرد، عزّ و جاه بود و نمانده</p></div>
<div class="m2"><p>هیچ نشانی به جا، نه آن و نه این را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونکه نیاید چنین به دهر و چنان رفت</p></div>
<div class="m2"><p>قصه کنم مختصر، چنان و چنین را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غصّه گلویم فشرده است که دادم</p></div>
<div class="m2"><p>بیهده بر باد ناله های حزین را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاش نفس یاوری کند که ببخشم</p></div>
<div class="m2"><p>فخر ثناگستری زمان و زمین را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرور عالم علی که صبح نخستین</p></div>
<div class="m2"><p>سکه به نامش زدند دولت و دین را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برق عَدو سوز اژدهای خدنگش</p></div>
<div class="m2"><p>ساخته خاکستری سپهر برین را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از لمعان سنان معرکه سوزش</p></div>
<div class="m2"><p>مجمره گردد زره طغان و نگین را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دوزخ نقدی به جانگدازی دشمن</p></div>
<div class="m2"><p>صرصر قهرش کند هوای سخین را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داده به سیل فنا روانی رُمْحَش</p></div>
<div class="m2"><p>ییکر پولاد سنج و خانهٔ زین را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ربط به هم داده است الفت عهدش</p></div>
<div class="m2"><p>چشم سیه مست و خال گوشه نشین را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد چو فراری ستم ز شحنهٔ عدلش</p></div>
<div class="m2"><p>داد به راحت قضا قرار مکین را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شه که فرامُش کند گدایی کویش</p></div>
<div class="m2"><p>خورده به دولت فریب دیو لعین را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهر سر سروری که خاک رهش نیست</p></div>
<div class="m2"><p>تیز به سوهان کنند ارهٔ سین را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر نکند تکیه روزگار به حفظش</p></div>
<div class="m2"><p>سلسله ریزد ز هم شهور و سنین را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رخش بهار از سمندِ سیل عنانش</p></div>
<div class="m2"><p>در عرق شرم، غوطه داده زمین را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بنده نوازا، صریر خامه به مدحت</p></div>
<div class="m2"><p>نغمه شکسته است مرغ سدره نشین را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صفحه نظر کن، که کرده مانی کلکم</p></div>
<div class="m2"><p>چهره گشایی نگار خانهٔ چین را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خنده زند نشئهٔ مداد و دواتم</p></div>
<div class="m2"><p>خون سیاووش و آب بسته جنین را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شب همه شب در خیالم این که نمایم</p></div>
<div class="m2"><p>صرف ثنای تو روز بازپسین را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هیچ به مهر تو سست عهد نبودم</p></div>
<div class="m2"><p>چرخ چرا برگماشت عهد چنین را؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ساختهام در امید شادی وصلت</p></div>
<div class="m2"><p>دستخوش درد و داغ، جان غمین را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خلق تو را جان فدا کنم که ندیده ست</p></div>
<div class="m2"><p>گوشهٔ ابروی دلگشای تو چین را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تیغ تو تا گوهر آب داده روا شد</p></div>
<div class="m2"><p>سجدهٔ آتش پرست، ماء معین را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بهر نثار تواست، عبث نیست</p></div>
<div class="m2"><p>پرورش خامه، نکته های متین را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در حرکت صولجان کلک تو دارد</p></div>
<div class="m2"><p>باکرهٔ لاجورد گوی زرین را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لب چو به نام کف سخای تو جنبید</p></div>
<div class="m2"><p>رخت به صحرا فتد ز لرزه، زمین را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر نه ظهور تو بود مقصد از آدم</p></div>
<div class="m2"><p>سجده نبودی قبول، قالب طین را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از طمع خام وصل، با سمِ رخشت</p></div>
<div class="m2"><p>ناشزه گردد عروس چرخ قزین را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هست به دست تو، چشم ابر بهاری</p></div>
<div class="m2"><p>یاری عاجز مذلت است معین را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چاشنی از خوی بی دریغ تو باشد</p></div>
<div class="m2"><p>لعل نمک سا، تبسم شکرین را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ناخنهٔ چرخ پشت گوش بخارد</p></div>
<div class="m2"><p>تیغ تو تا شد هلال عید زمین را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شیر سر خود گرفته است ز عدلت</p></div>
<div class="m2"><p>تاب تحمّل نداشت نقطهٔ شین را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خصم جهولت به روزگار بنازد</p></div>
<div class="m2"><p>ملک سلیمان بود مشیمه، جنین را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر نکنم سجده سوی کعبه، عجب نیست</p></div>
<div class="m2"><p>غره کند خاک درگه تو جبین را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل چو نبندد به حرز داغ تو عاشق</p></div>
<div class="m2"><p>غمزه کند در نیام، خنجر کین را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از کرمت سَروَرا شگفت نباشد</p></div>
<div class="m2"><p>قدر فزایی اگر غلام کمین را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دولت و قدر آن شبی بود که فروزد</p></div>
<div class="m2"><p>در حرم روضهٔ تو، شمع یقین را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>غیرت عاشق نگر که مطرب گردون</p></div>
<div class="m2"><p>گوش به ره بود ناله های حزین را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من به خیالی که بوی درد تو دارد</p></div>
<div class="m2"><p>راه ندادم به دل ز سینه انین را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>او نه خریدار و من نه نکته فروشم</p></div>
<div class="m2"><p>چرخ ندارد بهای در ثمین را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تیغ زبانم جهان ستان بود آری</p></div>
<div class="m2"><p>تیغ، گشادهست حصنهای حصین را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خاطر نازک، سخن نگاه ندارد</p></div>
<div class="m2"><p>کرد نثار ره تو غثّ و سمین را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شوق ثنای تو کرد غارت هوشم</p></div>
<div class="m2"><p>می نشناسم ز ناگزیده گزین را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هم تو مگر ای جهان فیض نمایی</p></div>
<div class="m2"><p>نامزد انتقاد رای رزبن را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گر قلم انوریست جادوی بابل</p></div>
<div class="m2"><p>معجزه ام اژدهاست سحر مبین را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نغمه به لب درشکن حزین که فکنده</p></div>
<div class="m2"><p>کلک تو در طاس آبنوس طنین را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وعده شها دادیم به یاری و زان شب</p></div>
<div class="m2"><p>شاد نمایم دل به وعده رهین را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کام ز فیض تو باد جان و جهان را</p></div>
<div class="m2"><p>نام ز دست تو باد تیغ و نگین را</p></div></div>