---
title: >-
    شمارهٔ ۱۷ - در مدح حضرت امیر مومنان علیه السلام
---
# شمارهٔ ۱۷ - در مدح حضرت امیر مومنان علیه السلام

<div class="b" id="bn1"><div class="m1"><p>شد جان و هوش و صبر و خرد را ز کار دست</p></div>
<div class="m2"><p>مشکل دهد دگر به هم این هر چهار دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست ای سبو مکش ز حریفان درین خمار</p></div>
<div class="m2"><p>تا عهد کهنه تازه نمایم بیار دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادم ز دست حلقهٔ درگاه کعبه را</p></div>
<div class="m2"><p>اما نمی کشم ز خم زلف یار دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پهلو به بستری ننهم دور از آن میان</p></div>
<div class="m2"><p>یکشب که با غمی نکنم در کنار دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرم به کف چگونه حریفان پیاله را؟</p></div>
<div class="m2"><p>زین سان که رعشه دار بود از خمار دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست ار نمی نهی به دلم حق به دست توست</p></div>
<div class="m2"><p>کاین دل در آتش است و تو را در نگار دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشنو مپرس قصّهٔ این تاب و تب مرا</p></div>
<div class="m2"><p>از دور هم ز آتش من دور دار دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوبت به دست بی سر و پایان نمی رسد</p></div>
<div class="m2"><p>یک طرف دامن است تو را و هزار دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمشاد من ببالکه صدبار برده است</p></div>
<div class="m2"><p>دستِ نگار بسته ات از نوبهار دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست زیار رفتهٔ ما راگناه چیست؟</p></div>
<div class="m2"><p>چون بهله کرده برکمرت استوار دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نتوان شکست بیعتِ یار قدیم را</p></div>
<div class="m2"><p>چون درکشد ز دست سبو، میگسار دست؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساقی به عشق یار که در دِه پیاله ای</p></div>
<div class="m2"><p>مطرب ترانه سرکن و در زن به تار دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>افسرده ام، بخوان غزل عاشقانه ای</p></div>
<div class="m2"><p>تا با حریف شوق کنم در کنار دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از بس نهفته گرد غمم، گر نفس کشم</p></div>
<div class="m2"><p>خورشید پیش دیده نهد از غبار دست</p></div></div>