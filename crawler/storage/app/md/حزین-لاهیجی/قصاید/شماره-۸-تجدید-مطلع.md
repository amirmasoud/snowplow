---
title: >-
    شمارهٔ ۸ - تجدید مطلع
---
# شمارهٔ ۸ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>ای نفس کجا بود تو را مولد و منشا؟</p></div>
<div class="m2"><p>بر تودهٔ غبرا چه کنی منزل و مأوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مهبط ادنیٰ به خساست چه نشینی؟</p></div>
<div class="m2"><p>ای گشته فراموش تو را، مصعد اعلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند به پیمایش این شیب و فرازی؟</p></div>
<div class="m2"><p>بالاتر از این بود تو را پایه والا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زندانی جسم کهنم رَبِّ ترحّم</p></div>
<div class="m2"><p>اقبل بقَبول حَسَنِ ربِّ دعانا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوشینه مرا بود به سر آتش شوقی</p></div>
<div class="m2"><p>می سوختم از گرم روی، خار ته پا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگه رهم افتاد به خاکی که ملایک</p></div>
<div class="m2"><p>از دیدن آن آب دهد چشم تماشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جنتکده شد دیده ز نظاره آن کوی</p></div>
<div class="m2"><p>حیرت زده شد چشم خرد، آینه آسا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پرده برافکندن این صورت مبهم</p></div>
<div class="m2"><p>لب مست سوال آمد و دل گرم تقاضا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم به بیانی همه عجز و همه زاری</p></div>
<div class="m2"><p>گفتم به زبانی همه خوف و همه بشری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای کوی فرح بخش کدامی که ز غیرت</p></div>
<div class="m2"><p>چون بیت حرم سرشکن قدسی و رضوی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روح القدسم بانگ زد و گفت که هشدار</p></div>
<div class="m2"><p>این روضه بود بارگه قبلهٔ دلها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلطان قضا، میر قدر، حیدر صفدر</p></div>
<div class="m2"><p>بازوی پیمبر علی عالی اعلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن عرش جنابی که نماید پی تعظیم</p></div>
<div class="m2"><p>بر سده او سجده بری کعبه علیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کامل ز کمال هنرش دوده آدم</p></div>
<div class="m2"><p>روشن ز جمال گهرش دیدهٔ حوا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر خاک فتد در قدمش اطلس گردون</p></div>
<div class="m2"><p>بیآب شود باکرمش همّت دربا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نازان به فروغ گهرش طینت خورشید</p></div>
<div class="m2"><p>ریان ز بهار نظرش گلشن خضرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیمار بود در هوسش نرگس اشهل</p></div>
<div class="m2"><p>بر باد رود از نفسش نطق مسیحا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روشن شود از خاک رهش دیده معنی</p></div>
<div class="m2"><p>گلشن بود از فیض ولایش دل دانا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از رشح کفش دامن نیسان گهرآگین</p></div>
<div class="m2"><p>وز خلق خوشش باد بهاران به مواسا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای جزیه ده خار رهت سدره و طوبی</p></div>
<div class="m2"><p>وی سجده برِ خاک درت مسجد اقصی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دیوان ابد ساخته از عدل تو قانون</p></div>
<div class="m2"><p>عنوان ازل یافته از نام تو طغرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از هیبت تو آب شود زهره رستم</p></div>
<div class="m2"><p>بر طاق نهد معدلتت شهرت کسری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خیره، بر تیغ و قلمت، معجز موسی</p></div>
<div class="m2"><p>دریوزه گر فیض نوالت ید بیضا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون افعی رُمح تو بکاود دل دشمن</p></div>
<div class="m2"><p>چون ضیغم تیغ تو بدرد صف هیجا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر اجری محرومی کونین اعادی</p></div>
<div class="m2"><p>آب دم تیغ تو نویسد خط اجرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از همّت والاست که هرگز نفتاده</p></div>
<div class="m2"><p>مجموعه املای تو را قافیه لا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر دوش پیمبر چو نهادی قدم، آمد</p></div>
<div class="m2"><p>معراج تو بالاتر ازو یک قد و بالا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درگاه تو را چون نکنم ناصیه سایی؟</p></div>
<div class="m2"><p>هم کعبهٔ دین آمده، هم قبلهٔ دنیا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>افکنده به آوارگیم حسرت کویت</p></div>
<div class="m2"><p>بر آتش مجنون چه زنی دامن صحرا؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>انوار دلارای تو در دیدهٔ وامق</p></div>
<div class="m2"><p>شد جلوه گر از آینهٔ طلعت عذرا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از روی تو تا مشعله ای دزدکی افروخت</p></div>
<div class="m2"><p>شد گرم به خورشید، نظربازی حربا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر شمع جمال تو نمی کرد تجلی</p></div>
<div class="m2"><p>پروانهٔ یوسف نشدی جان زلیخا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون حسن تو شد جامع اطوار نکویی</p></div>
<div class="m2"><p>مجنون دل آشوفته شد فتنه به لیلی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر رابطهٔ فیض تو پیوند نمی کرد</p></div>
<div class="m2"><p>صورت نگرفتی ره الفت به هیولی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از فیض تو گردید مخمّر گل آدم</p></div>
<div class="m2"><p>معلول پذیرد اثر، از علّت اولی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پر سوخته پروانهٔ شمع حرم توست</p></div>
<div class="m2"><p>عیسی اگر از مهر کند مسند اسنی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سیلی خور دریای نوالت، رخ امّید</p></div>
<div class="m2"><p>شوربدهٔ سودای خیالت دل شیدا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وحشی شود از خاک درت رام تسلّی</p></div>
<div class="m2"><p>شیرین شود از شهد غمت کام تمنا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>لب تشنه نوازا، ز حزین باز نگیری</p></div>
<div class="m2"><p>آن جرعه کزو چهرهٔ جان گشت مطرّا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لالای کمین است که در مدح تو گردد</p></div>
<div class="m2"><p>در گوش و کنار دو جهان لولویِ لالا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از دولت دیرینه غلامیّ تو، تا سر</p></div>
<div class="m2"><p>افراشته ام بر فلک از رفعت آبا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آزاده دلم ننگ برد ز اختر دولت</p></div>
<div class="m2"><p>شوریده سرم عار کند ز افسر دارا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>منّت که به تقلید و به تعلیم کسی نیست</p></div>
<div class="m2"><p>این شیوه که دارم به ثنای تو ز انشا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آموخته ای با قلمم طرز ستایش</p></div>
<div class="m2"><p>افروختهای در شجرم آتش موسی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شمعم ز دم سرد خسان باک ندارد</p></div>
<div class="m2"><p>خورشید ز صرصر نکند هیچ محابا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از دل چو برآید نفس شعله نهادم</p></div>
<div class="m2"><p>در خلد رسد گرمی ما خور به حورا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر سینه اعدای تو تا پای بیفشرد</p></div>
<div class="m2"><p>برکرد سنان قلمم سر ز ثریا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر خاک ره عجز، کشد پرچم رامح</p></div>
<div class="m2"><p>در مدح تو گیرم چو به کف کلک فلک سا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا فاخته بر سرو زند پردهٔ قمری</p></div>
<div class="m2"><p>تا صوت عنادل بسراید ره عنقا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در طنطنهٔ مدح سراییت همیشه</p></div>
<div class="m2"><p>گوش فلک از خامهٔ من باد پر آوا</p></div></div>