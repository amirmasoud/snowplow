---
title: >-
    شمارهٔ ۲۰ - مدح حضرت امیر مؤمنان (ع)
---
# شمارهٔ ۲۰ - مدح حضرت امیر مؤمنان (ع)

<div class="b" id="bn1"><div class="m1"><p>زین ششدرم چو بال فشانی دهد گشاد</p></div>
<div class="m2"><p>این هفت قله را، چو غباری دهم به باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سدره روح قدسی من آشیان کند</p></div>
<div class="m2"><p>این دخمه را نهم به سر گور کیقباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان بی غمانه وارهد از جسم خیره سر</p></div>
<div class="m2"><p>غیر از میانه پا کشد و افتد اتحاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریزد ز طرف بال همای سعادتم</p></div>
<div class="m2"><p>رنگ هم آشیانی این ناخجسته خاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناسازگار بخت در آشتی زند</p></div>
<div class="m2"><p>نادیده کام دل، کند اندوه خیر باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاطر کند شکایت ایّام مختصر</p></div>
<div class="m2"><p>کوته شود فسانهٔ هجران به امتداد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیدی مبارک است به عاشق وصال دوست</p></div>
<div class="m2"><p>یا حبّذا التجافی عن مربض البعاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعد است ساعتی که فتد دامنی به دست</p></div>
<div class="m2"><p>صبح سعادت است مرا ساعد سعاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرّم دمی که محمل لیلی شود پدید</p></div>
<div class="m2"><p>مجنون ز خار بادیه چیند گل مراد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان نور دیده غرّهٔ گریان شود قریر</p></div>
<div class="m2"><p>خندان دمد ز زلف شب تیره، بامداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاجز شود ز خصمی ما عالم عنود</p></div>
<div class="m2"><p>پیچد به هم دبیر فلک، دفتر عناد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردد گران کمانکش ایّام کینه توز</p></div>
<div class="m2"><p>پیچد ز درد ارقم دوران کج نهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آزادگان ز وادی حسرت کشند رخت</p></div>
<div class="m2"><p>دل خون شدن، سرشک دویدن رود ز یاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فارغ نشینم از غم هجر و خمار شب</p></div>
<div class="m2"><p>زلف صنم به دست و به دستی پیاله، شاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خندان شود به شاخ طرب غنچهٔ امید</p></div>
<div class="m2"><p>ریّان شود ز ابر کرم گلشن مراد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاد و شکفته، نغمهٔ شکرانه سر کنم</p></div>
<div class="m2"><p>رطب اللّسان به درگه آن کعبهٔ رشاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الحمد و الثّناء، لمن اذهب الحزن</p></div>
<div class="m2"><p>المجد و البهاء لمن طیّب الفؤاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر جور دیده ام، ز فلک انتقام هست</p></div>
<div class="m2"><p>دست من است و دامن دارای عدل و داد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برهان قدرت ازلی، حجت جلی</p></div>
<div class="m2"><p>نفس نبی، علیّ ولی، والی عباد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>معمار قصر جود که فیض وجود او</p></div>
<div class="m2"><p>بنیان هستی دو جهان را بود عماد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مریم شود ز نکهت او بکر پاک جیب</p></div>
<div class="m2"><p>عیسی بود به مدحت او، ذات پاک زاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی حب او قضیهٔ ایمان بود عقیم</p></div>
<div class="m2"><p>نازد به مهر او خرد دوستی نژاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وادی گرای اوست، روان وفا شیَم</p></div>
<div class="m2"><p>مدحت سرای اوست، دل خالص الوداد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سالک شد از هدایت او صافی الضّمیر</p></div>
<div class="m2"><p>صوفی شد از ارادت او واصل المراد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گلمیخ سدّه اش شرف اختر بلند</p></div>
<div class="m2"><p>نعلین بندگان درش افسر قباد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هستی کاینات ز سرجوش فیض اوست</p></div>
<div class="m2"><p>شد جوهر نخست ز تعلیمش اوستاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باشد قضا به قبضهٔ حکمش مطیع سیر</p></div>
<div class="m2"><p>دارد قدر به رایض فرمانش انقیاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یک جنبش از عتاب قیامت نهیب اوست</p></div>
<div class="m2"><p>بادی که بُرد بنگه و بنیاد قوم عاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>موجی ز بی نیازی دریای قهر اوست</p></div>
<div class="m2"><p>طوفانکی که گرد برآورد از بلاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکس به او ز خیره سری همسری کند</p></div>
<div class="m2"><p>ناکس بود به سنجش میزان طبع راد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن جا که آفتاب قیامت شود بلند</p></div>
<div class="m2"><p>ذرّات بی وجود نیایند در عداد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از مبدأ وجود نگردد عطاپذیر</p></div>
<div class="m2"><p>جان را اگر نه، جنّت کویش بود معاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در حشر هر صحیفه که آزاد نامه ای ست</p></div>
<div class="m2"><p>آن نامه را بود به تولایش استناد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن اشرفی که از شرف بندگی بود</p></div>
<div class="m2"><p>دایم قدم به تارک نه طارم شداد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نقد من است در نظر بخردان سره</p></div>
<div class="m2"><p>نقّاد لطف او سخنم کرده انتقاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مپسند چشم حیرت من خیرگی کند</p></div>
<div class="m2"><p>در کشوری که سرمه فروشی کند رماد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من بنده را به خدمت اگر اعتماد نیست</p></div>
<div class="m2"><p>غمگین نیم که بر کرم توست اعتماد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا چند جان بود به جهان پای در وحل؟</p></div>
<div class="m2"><p>تا کی کسی کمی کند از چرخ سر زباد؟!</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دنیا کجا پذیره کند چشم سیر من</p></div>
<div class="m2"><p>پس مانده ای ز خوان خسیسان با شداد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خلقی عجب، مشعبد دوران پدید کرد</p></div>
<div class="m2"><p>بی تربیت، گسسته عنان، عادم السّداد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این عهد زشت، رنج پدر را نبرده نور</p></div>
<div class="m2"><p>امروز در جهان رخ والد ندیده داد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر تخم کشته اند حریفان، درو کنند</p></div>
<div class="m2"><p>گندم نمی کند کسی ازکشت جو حصاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای خامه هوش دار، مباد از نفس رود</p></div>
<div class="m2"><p>آشفته وار طرّهٔ خاموشیت به باد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دیوار کاخ دهر بنایی ست سست پی</p></div>
<div class="m2"><p>آوخ به خفتگان بن این شکسته لاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شاها منم کمینه گدای ثناگرت</p></div>
<div class="m2"><p>کز کلک خسروانه زنم کوس انفراد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در تندباد حادثه دارد به صدق دل</p></div>
<div class="m2"><p>این دست رعشه دار به مدح تو اعتقاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر جان خصم جاه تو ثعبان موسوی ست</p></div>
<div class="m2"><p>کلک من است نایب تیغ تو در جهاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در مدحت تو شسته زبان را به سلسبیل</p></div>
<div class="m2"><p>در حضرت تو بسته میان را به اجتهاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آنجا که رای روشن من پرتو افکند</p></div>
<div class="m2"><p>افتد متاع رایج خورشید در کساد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دستان من اگر شنود گوش مدّعی</p></div>
<div class="m2"><p>با یک جهان عداوت و یک داستان عناد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بی اختیار می گذرد بر زبان او</p></div>
<div class="m2"><p>لله درّ قائلهِ نعم ما افاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در نامه ها حکایت من احسن القصص</p></div>
<div class="m2"><p>بر خامه ها انامل من فارس الجیاد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از دل چو بردمد نفس آتشین من</p></div>
<div class="m2"><p>حاسد به جان سوخته گوید که ما افاد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شادی کنان ستاره کشد زهره در بغل</p></div>
<div class="m2"><p>گیرد چو خوشنوایی من راه شاد باد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زین سنگلاخ قافیه فرسوده شد قلم</p></div>
<div class="m2"><p>بس کن حزین ترانه که خون می شود مداد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا بر سر زمانه کشد چتر نور، روز</p></div>
<div class="m2"><p>بر قلّه ها درفش فرازد چو بامداد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سر سبز باد خامهٔ مدحت نگار تو</p></div>
<div class="m2"><p>بر تارک محب تو بادا گل مراد</p></div></div>