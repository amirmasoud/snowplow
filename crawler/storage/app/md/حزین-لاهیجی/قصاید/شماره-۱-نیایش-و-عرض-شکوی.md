---
title: >-
    شمارهٔ ۱ - نیایش و عرض شکوی
---
# شمارهٔ ۱ - نیایش و عرض شکوی

<div class="b" id="bn1"><div class="m1"><p>پرتو روی تو را نیست جهان پرده دار</p></div>
<div class="m2"><p>امتلأ الخافقین شارق ضوء النهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای من و بهتر ز من، بندهٔ فرمان تو</p></div>
<div class="m2"><p>گر دل و گر دین بری، این لنا الاختیار؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم اگر دشمن است چون تو پناهی چه غم؟</p></div>
<div class="m2"><p>رد شطاط الذی، عند ذوی الاقتدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف تو بیگانه نیست، از چه شفیع آورم؟</p></div>
<div class="m2"><p>بائسک المستجیر، عزتک المستجار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاله گلزار توست، سینه اخگرفروز</p></div>
<div class="m2"><p>واله دیدار توست دیده اخترشمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد گر باهشی، باده کش و توبه کن</p></div>
<div class="m2"><p>از خرد دوربین وز هوس نابکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوش به حکم توایم، مرد زبان نیستیم</p></div>
<div class="m2"><p>طاعت اگر ردکنی، حاش لنا الاختیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عربده افزون کند حادثه با گوشه گیر</p></div>
<div class="m2"><p>لطمه زند بیشتر، موج به دریا کنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وه که ندارد درنگ، گردش گردنده چرخ</p></div>
<div class="m2"><p>شهد کند در شرنگ، ساغر لیل و نهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زحمت بیهوده دید ناخن اندیشه ام</p></div>
<div class="m2"><p>آه که جز باد نیست، در گره روزگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این به دمی بسته است وان به غمی می رود</p></div>
<div class="m2"><p>هستی بد عهد بین، شادی بی اعتبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همسر دیرینه اند، دیده گشا و ببین</p></div>
<div class="m2"><p>خنده رنگین گل، گریه ابر بهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آه چه سازد کسی با تب و تابی چنین؟</p></div>
<div class="m2"><p>چهره روز آتشین، طره شب تابدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خار به چشمم خلد، از گل و ربحان او</p></div>
<div class="m2"><p>روی جهان دیدنی نیست درین روزگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از فلک پشت خم، شد قد دونان علم</p></div>
<div class="m2"><p>کار جهان شد بهم، گشت هنر عیب و عار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تافت به فن زال دهر، دست قوی چیرگان</p></div>
<div class="m2"><p>همچو کمان حلقه شد، بازوی خنجرگذار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تاب تحمّل نماند، یا لجاالهاربین</p></div>
<div class="m2"><p>علم ستیرالجبین، جهل خلیع العذار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پشت جوانمرد را بار لئیمان شکست</p></div>
<div class="m2"><p>ریخت جو برگ خزان، پنجهٔ گوهرنثار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بار خران چون برد، دوش غزال حرم؟</p></div>
<div class="m2"><p>شیر ژیان چون کشد ناز سگ جیفه خوار؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر طرفی یکّه تاز، کودن دون فطرتی ست</p></div>
<div class="m2"><p>تکیه زنان هر خری ست جای صدور کبار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خامه همان به که رو تابد ازین گفتگو</p></div>
<div class="m2"><p>نیست به شکر نکو، حنظل ناخوشگوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رونق بستان بود شور صفیرت حزین</p></div>
<div class="m2"><p>بلبل دستان شود، چون تو یکی از هزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون که پی امتحان با مژه خون چکان</p></div>
<div class="m2"><p>خامه نهی در بنان، صفحه کشی در کنار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مایه به معدن دهد کلک جواهر رقم</p></div>
<div class="m2"><p>نکته به دامن برد طبع بدایع نگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صبح قیامت دمید از جگر سوخه</p></div>
<div class="m2"><p>خوشترم آمد درین گرم صفیر اختصار</p></div></div>