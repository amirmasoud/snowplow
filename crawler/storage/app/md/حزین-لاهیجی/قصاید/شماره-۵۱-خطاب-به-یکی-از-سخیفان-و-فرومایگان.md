---
title: >-
    شمارهٔ ۵۱ - خطاب به یکی از سخیفان و فرومایگان
---
# شمارهٔ ۵۱ - خطاب به یکی از سخیفان و فرومایگان

<div class="b" id="bn1"><div class="m1"><p>تا گشت عدل و رای تو معمار روزگار</p></div>
<div class="m2"><p>در هم شکسته شد در و دیوار روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سیل بیخ و بُن کن ظلمت نمانده است</p></div>
<div class="m2"><p>آسودگی به سایهٔ دیوار روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر از فغان و شکوه نخیزد ترانه ای</p></div>
<div class="m2"><p>با زخمهٔ مخالفت، از تار روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نشکفد چو غنچهٔ پیکان به دور تو</p></div>
<div class="m2"><p>لب خنده ای نمانده به سوفار روزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کشور تمیز تو، صد طعنه می رود</p></div>
<div class="m2"><p>از موزهٔ زمانه به دستار روزگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افتاده است از تو به کار جهان گره</p></div>
<div class="m2"><p>تنها تویی تو، عقدهٔ دشوار روزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانند ذات بی بدلت، پاره دنبه ای</p></div>
<div class="m2"><p>هرگز نبوده است به شلوار روزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رای تو گشته ناسخ احکام عقل و عرف</p></div>
<div class="m2"><p>شعری بجا نمانده ز آثار روزگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میزان معدلت، ز تو نااستوار شد</p></div>
<div class="m2"><p>جز سنگ کم نمانده به بازار روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بس به دهر، سیرت زشت است آشکار</p></div>
<div class="m2"><p>نتوان گشود دیده، به رخسار روزگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عهدت، آب و آینه از عکس عاریند</p></div>
<div class="m2"><p>از بس ندیدنی شده دیدار روزگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کو دست و تیغ حق طلبی کز میان برد</p></div>
<div class="m2"><p>ای مدّت حیات تو زنار روزگار؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترسم بنات نعش، به افسون شود یتیم</p></div>
<div class="m2"><p>از عشوهٔ تو مادر غدّار روزگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>افتاده از میامن عهد مبارکت</p></div>
<div class="m2"><p>با ریش گاو و ...ون خران، کار روزگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناید ز خامه، وصف امینان حضرتت</p></div>
<div class="m2"><p>یکسر سبک سر و همه طرار روزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خاییده خامه را عوض لقمه از شره</p></div>
<div class="m2"><p>بربوده مهره، از دهن مار روزگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جوع البقرگرفته خران را به عهد تو</p></div>
<div class="m2"><p>یک برگ کاه نیست در انبار روزگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گاو زمین ندیده گرانجان تر از تویی</p></div>
<div class="m2"><p>ای لاشهٔ خبیث تو، سربار روزگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کناس را ز نکهت کوی تو نفرت است</p></div>
<div class="m2"><p>ای نکبت تو، مایهٔ ادبار روزگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بس کشیده ای به قطار بهادران</p></div>
<div class="m2"><p>سایس نمانده است به طومار روزگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز کرم شب فروز به گیتی نیامده ست</p></div>
<div class="m2"><p>کون سوخته تر از تو، در ادوار روزگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همسنگ با گهر نهی از بخل قطره را</p></div>
<div class="m2"><p>پیشت یکی ست، اندک و بسیار روزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در خاک نرم، میخ زدن جایگیر نیست</p></div>
<div class="m2"><p>تاکی کنم به ناف تو، مسمار روزگار؟</p></div></div>