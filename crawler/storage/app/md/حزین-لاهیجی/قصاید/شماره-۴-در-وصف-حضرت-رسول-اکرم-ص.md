---
title: >-
    شمارهٔ ۴ - در وصف حضرت رسول اکرم(ص)
---
# شمارهٔ ۴ - در وصف حضرت رسول اکرم(ص)

<div class="b" id="bn1"><div class="m1"><p>جان تازه ز تردستی ابر است جهان را</p></div>
<div class="m2"><p>آبی به رخ آمد، چه زمین را چه زمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افلاک شد از عکس گل و لاله شفق رنگ</p></div>
<div class="m2"><p>مشّاطهٔ نوروز بیاراست جهان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی دم عیش است نبازی به تغافل</p></div>
<div class="m2"><p>بر آب اساس است جهان گذران را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این جوش بهار است که چون شور قیامت</p></div>
<div class="m2"><p>از خاک برانگیخت شهیدان خزان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرداخت ز تسخیر ممالک شه خاور</p></div>
<div class="m2"><p>گرداند سوی بیت شرف باز عنان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیروز گر از طَنطَنهٔ صفدریِ وی</p></div>
<div class="m2"><p>خون در بدن افسرده شدی گوهر کان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز چه شد کوکبهٔ باد خزانی؟</p></div>
<div class="m2"><p>وان جمله کجا رفت دی ملک ستان را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کیخسرو کهسار به خونریزی بهمن</p></div>
<div class="m2"><p>از سبزه به زهر آب دهد تیغ میان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نازم به فرح بخشیِ فصلی که هوایش</p></div>
<div class="m2"><p>از جام طراوت شده ساقی عطشان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تیشهٔ فرهاد که در خاره کند شق</p></div>
<div class="m2"><p>زین پیش اگر برق زدی کوه گران را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بس که عرق ریز چو ابر است، مسامش</p></div>
<div class="m2"><p>اکنون خطر از خاره بود برق دمان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوری است که در صاف می عیش کمی نیست</p></div>
<div class="m2"><p>این باده به کام است دل پیر و جوان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عام است ز بس خوشدلی عهد، عجب نیست</p></div>
<div class="m2"><p>ممسک کند از یاد فراموش زیان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عطّار صبا از پی ترکیب مفرّح</p></div>
<div class="m2"><p>آمیخت به عیش ابدی، جوهر جان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر می کشد از طوق تذروان خمیده</p></div>
<div class="m2"><p>بنگر به سر سرو غرور ریعان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از پشت لب سبزه کند ژاله تراوش</p></div>
<div class="m2"><p>تا آب دهد سوسن آزاده زبان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرکس به نوایی شده چون نی طرب انگیز</p></div>
<div class="m2"><p>هر مرغ به رامشگریی بسته میان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غیر از من مهجور دل افگارکه چشمم</p></div>
<div class="m2"><p>در خواب ندیده ست رخ بخت جوان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوکرده به غم، مرغِ قفس زاده چه داند</p></div>
<div class="m2"><p>در گلشن ایجاد نشاط طیران را؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلتنگ تر از غنچه به گلزار گذشتم</p></div>
<div class="m2"><p>تا جلوه به نظّاره دهم لاله ستان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتم به نسیم سحر، این داغ جگرسوز</p></div>
<div class="m2"><p>بر دل که نهاد این همه خونین کفنان را؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بلبل ز سر شاخ زد این نغمه به گوشم</p></div>
<div class="m2"><p>عشق است،که فارغ نگذارد دل و جان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این عشق چه چیز است بگوییدکه نامش</p></div>
<div class="m2"><p>ای مجلسیان، شمع صفت سوخت زبان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرکرد سرایندهٔ مجلس سخن از عشق</p></div>
<div class="m2"><p>شست از ورق سینه، حدیث حدثان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یاران سبکروح، گرانبار خمارند</p></div>
<div class="m2"><p>ساقی غم دل بین و بده رطل گران را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با ابر عطایت چه نماید نم فیضی؟</p></div>
<div class="m2"><p>تن در ندهد بحر کفت حد و کران را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خشک است لبم، رفع خمار رمضان کن</p></div>
<div class="m2"><p>بگشاده مه عید به خمیازه دهان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مطرب نی محزون نفسی خوش نکشیده ست</p></div>
<div class="m2"><p>در راه تو دارد دل و چشم نگران را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عیسی نفسی، چاره او کن که نباشد</p></div>
<div class="m2"><p>غیر از دم گرم تو علاجی، خفقان را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زندانی جسمم، برهانم به سماعی</p></div>
<div class="m2"><p>آزادکن از تیره گل، این آب روان را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>القصه که دارم دل آغشته به خونی</p></div>
<div class="m2"><p>رحمی که ز کف باخته ام تاب و توان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ازآتش آهم دل سخت تو نشد نرم</p></div>
<div class="m2"><p>ره نیست مگر در دل سنگ تو فغان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیداست که فکر دل افگار نداری</p></div>
<div class="m2"><p>دانم که ندانی غم خونین جگران را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نای قلمم را دم جان بخش دمیدم</p></div>
<div class="m2"><p>تا عرضه دهم سرور قوسین مکان را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سالار رسل احمد مرسل که ز نامش</p></div>
<div class="m2"><p>اندوخته کونین، حیات دل و جان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن آیت رحمت که گل خُلق کریمش</p></div>
<div class="m2"><p>از حلم، سبک سنگ کند کوه گران را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برق غضبش جوشن افلاک دراند</p></div>
<div class="m2"><p>چون مه که ز هم بگسلد اوتار کتان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رضوان به دو صد عزت و تعظیم فرستد</p></div>
<div class="m2"><p>از خاک درش غالیه، خیرات حسان را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای شاهسواری که ز عزت سگ کویت</p></div>
<div class="m2"><p>نشمرده کمین چاکر خود، قیصر و خان را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همچون گلهء میش که در حکم شبان است</p></div>
<div class="m2"><p>سر بر خط فرمان تو شیران ژیان را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تهدید تو، خون از مژهء تیر چکانده</p></div>
<div class="m2"><p>تأدیب تو مالیده بسی گوش کمان را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>افکنده نظر تا به کمین پایه قصرت</p></div>
<div class="m2"><p>دهشت نبرد از سر گردون دوران را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از صلب شرفیاب صدف، دُرّ یتیمت</p></div>
<div class="m2"><p>چون بست به ساحل تتق عزت و شان را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از آب و می آتشکده ها گشت فسرده</p></div>
<div class="m2"><p>وز تاب و می آموخت کواکب سیران را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر ناخن فکر تو کند عقده گشایی</p></div>
<div class="m2"><p>بیرون برد ازکام سنان عقد لسان را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آوازه عدلت ز کران تا به کران رفت</p></div>
<div class="m2"><p>گرگ آمد و گردید سگ گله، شبان را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر ذره کند تندنظر، بر شه خاور</p></div>
<div class="m2"><p>خالی کند از بیم تو تخت سرطان را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از نقش سمش تارک گردون نهد افسر</p></div>
<div class="m2"><p>خنگی که مزین کند از داغ تو، ران را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در بندگیت صدق من از جبهه عیان است</p></div>
<div class="m2"><p>ای پیش تو سیمای عیان راز نهان را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از شهرت کلکم سر گردون به سماع است</p></div>
<div class="m2"><p>سیمرغ پر آوازه کند قاف جهان را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از داغ غلامی تو خورشید مکانم</p></div>
<div class="m2"><p>نام از تو علم شد من بی نام و نشان را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از شرم شکرخایی من نکته رنگین</p></div>
<div class="m2"><p>شد مهر خموشی لب شیرین دهنان را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نسبت نکنی منطق طوطی به مقالم</p></div>
<div class="m2"><p>با وحی سماوی چه شباهت هذیان را؟</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حاسد ز کلامم به شگفت آمد و می گفت</p></div>
<div class="m2"><p>کاین مایه گهر کو کف بحر و دل کان را؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ناید عجبش گر شود از فیض تو واقف</p></div>
<div class="m2"><p>نعت تو کند پر ز گهر درج دهان را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای خاک درت قبله آمال دو عالم</p></div>
<div class="m2"><p>گردی برسان چشم حزین نگران را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>افتاد گذر در شب ظلمانی هستی</p></div>
<div class="m2"><p>از راه خطیری من بی تاب و توان را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نه قوت پایی نه رفیقی نه دلیلی</p></div>
<div class="m2"><p>سر خاک رهت باد، سپردم به تو جان را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>با دیدهٔ گریان، دل بریان من امشب</p></div>
<div class="m2"><p>افروخت به محراب دعا شمع زبان را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا تیرگی از هجر کشد دیده عاشق</p></div>
<div class="m2"><p>تا روشنی از مهر بود، چشم جهان را</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>روشن شود از پرتو دیدار تو دیده</p></div>
<div class="m2"><p>راحت رسد از دولت وصل تو روان را</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خورشید ولای تو بود نور ضمیرم</p></div>
<div class="m2"><p>تا سایه کند پرچم جاهت ثقلان را</p></div></div>