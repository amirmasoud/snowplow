---
title: >-
    شمارهٔ ۳۱ - در ثنای حضرت امیر مؤمنان علی بن ابیطالب (ع)
---
# شمارهٔ ۳۱ - در ثنای حضرت امیر مؤمنان علی بن ابیطالب (ع)

<div class="b" id="bn1"><div class="m1"><p>زده ام طبل عشق و رسوایی</p></div>
<div class="m2"><p>شهرهٔ شهرتم به شیدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و دین داده ام به مغ بچگان</p></div>
<div class="m2"><p>همه جادو و شان یغمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه آرام جان دلشدگان</p></div>
<div class="m2"><p>همه درمان ناشکیبایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می زنم جرعه، می کشم ساغر</p></div>
<div class="m2"><p>با خراباتیان هرجایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مده از دست ای حریف دمی</p></div>
<div class="m2"><p>ذوق مستی و باده پیمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز خرابات، دل نیاساید</p></div>
<div class="m2"><p>نشوی هرزه گرد و هرجایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لوحش الله ز اهل آنکه به زهد</p></div>
<div class="m2"><p>ننمایند دامن آلایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه آزادگان خوف و رجا</p></div>
<div class="m2"><p>همه ویرانیان و ترسایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه نو خط عذار و سیمین تن</p></div>
<div class="m2"><p>همه سروِ ریاض رعنایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از فروغ جمالشان گردد</p></div>
<div class="m2"><p>آب در دیدهٔ تماشایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه روح روان و مونس دل</p></div>
<div class="m2"><p>راحت افزای کنج تنهایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه مرهم نه جراحت دل</p></div>
<div class="m2"><p>همگی مایهٔ تن آسایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرده سرگشتگان دلشده را</p></div>
<div class="m2"><p>خضَری خطّ و لب مسیحایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خطّشان مایهٔ دل آشوبی</p></div>
<div class="m2"><p>لبشان شهرهٔ شکرخایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غمزه ها جمله در سپهداری</p></div>
<div class="m2"><p>مژه ها جمله، در صف آرایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طرّه سنبل، جبین سمن پیرا</p></div>
<div class="m2"><p>غنچه لب چهره ورد حمرایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گونه چون لاله، لاله نعمانی</p></div>
<div class="m2"><p>مژه خونی نگاه و یغمایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شمع روی و بیاض گردنشان</p></div>
<div class="m2"><p>غیرت بدر و رشک بیضایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قد قیامت خرام غارتگر</p></div>
<div class="m2"><p>مژه ناوک، اشارت ایمایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه مدهوش جام مهر و وفا</p></div>
<div class="m2"><p>همه در جوش باده پیمایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رشک طورست مجلسی که کند</p></div>
<div class="m2"><p>شفقی باده، مجلس آرایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ساقی آن بادهٔ صبوح بیار</p></div>
<div class="m2"><p>که سرآرد، شب جگرخایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بده آن می که جان بیاساید</p></div>
<div class="m2"><p>که ندارم سر تن آسایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ساقی آن ساغر طهور بیار</p></div>
<div class="m2"><p>که دهد سینه را مصفّایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بده آن آتش خرد سوزم</p></div>
<div class="m2"><p>که ملولم از این تبه رایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ساقی آن آب لاله رنگ بیار</p></div>
<div class="m2"><p>که کند خانهٔ دل آرایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بده آن صیقلی که پردازد</p></div>
<div class="m2"><p>دل از آلایش هیولایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ساقی آن مایهٔ سرور بیار</p></div>
<div class="m2"><p>چند ازین خون دیده پالایی؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چند کورانه راه کج سپرم؟</p></div>
<div class="m2"><p>بده آن نور چشم بینایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا رَهِ نعت سروری سپرم</p></div>
<div class="m2"><p>که رسولش بود تولّایی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاه مردان علی که بر خاکش</p></div>
<div class="m2"><p>فخر عرش است، جبهه فرسایی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>افتتاح صحیفهٔ کن را</p></div>
<div class="m2"><p>نام نامیش کرده طغرایی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مردگان مغاک گیتی را</p></div>
<div class="m2"><p>دم پاکش کند مسیحایی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در رَهِ مدحت گران قدرش</p></div>
<div class="m2"><p>خامه ام را رسد شکیبایی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نُه فلک را به رقص می آرد</p></div>
<div class="m2"><p>نفسم از بلند آوایی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شهسوارا، ز گرد شبرنگت</p></div>
<div class="m2"><p>مشک بیز است زلف حورایی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دین پناها، ز خاک درگاهت</p></div>
<div class="m2"><p>سرمه زیب است چشم بینایی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کرده صبح ازل به لوح قضا</p></div>
<div class="m2"><p>کلک حکم تو، صفحه آرایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>با حدوث تو عقل کل گوید</p></div>
<div class="m2"><p>به قدم ناز کن که می شایی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آسمانت چو چاکران گوید</p></div>
<div class="m2"><p>بنده فرمانم، آنچه فرمایی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کرده با یاد ماه طلعت تو</p></div>
<div class="m2"><p>همه یوسف وشان، زلیخایی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به هوای تو می زند قطره</p></div>
<div class="m2"><p>آه دشتی و اشک دریایی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مردگان را به یک نفس بخشد</p></div>
<div class="m2"><p>دم صدق تو فیض احیایی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به دو انگشت، یک اشارت تو</p></div>
<div class="m2"><p>ذوالفقاری کند ز برّایی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عِقد قندیل روضهٔ تو کند</p></div>
<div class="m2"><p>طارم عرش را ثریّایی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سومناتِ محبّت تو بود</p></div>
<div class="m2"><p>فارغ از رسم محفل آرایی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زلفِ حورانش کرده فرّاشی</p></div>
<div class="m2"><p>رخ خوبانش فرش دیبایی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دل شوریدگانش ناقوسی</p></div>
<div class="m2"><p>رگ جان جهان چلیپایی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خاطر قدسیانش مرآتی</p></div>
<div class="m2"><p>دل سیمین برانش خارایی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جرم بخشا، ترانه ای سنجم</p></div>
<div class="m2"><p>خالی از شرح و بسط انشایی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رشک مانیّ و نسخهٔ ارژنگ</p></div>
<div class="m2"><p>کلک فکرم، به صفحه آرایی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون برآرم نفس فرومانند</p></div>
<div class="m2"><p>همهٔ دودمان زگویایی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زادهٔ طبع نشئه زا کلکم</p></div>
<div class="m2"><p>زده بر صفحه، موج صهبایی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بر سپهر سخنوری، شعرم</p></div>
<div class="m2"><p>کرده هر نقطه ایش شعرایی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>لیک نتوانم از خجالت زد</p></div>
<div class="m2"><p>در مدیح تو لاف غرایی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حوریان ریاض مدحت تو</p></div>
<div class="m2"><p>بس که دارند شور زیبایی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برده بند نقاب شاهد فکر</p></div>
<div class="m2"><p>از سر انگشت خامه گیرایی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شهریارا حزین جان بازت</p></div>
<div class="m2"><p>که سراپا سریست سودایی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه یک جان بود فدایی وش</p></div>
<div class="m2"><p>همه یکدل بود تمنّایی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چه شود گر خط غلامی خویش</p></div>
<div class="m2"><p>برساند به زبب امضایی؟</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نبود با من دل آزرده</p></div>
<div class="m2"><p>غم دنیاو فکر دنیایی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نه به کفرم سری نه با ایمان</p></div>
<div class="m2"><p>نه به تقوی، نه باده پیمایی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نه به شاهد خوشم نه با زاهد</p></div>
<div class="m2"><p>نه به مسجد نه دیر ترسایی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نبرد دل، به هیچ شیوه ز من</p></div>
<div class="m2"><p>لب لعلی وچشم شهلایی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از دو عالم رمیده خاطر من</p></div>
<div class="m2"><p>هستم آن تو، هر چه فرمایی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ای پنجاه در خواب بودی</p></div>
<div class="m2"><p>نی کلکم کند شکرخایی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>باد در دیدهٔ محبّانت</p></div>
<div class="m2"><p>نور رای تو، شمع بینایی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در جگرگاه دشمنانت باد</p></div>
<div class="m2"><p>دم تیغ تو در جگر خایی</p></div></div>