---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>پی برده هر که، وادیِ دل جلوه گاه کیست</p></div>
<div class="m2"><p>داند که چاک سینهٔ ما شاهراه کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کوچه ز انتظار تو، تار نظاره ای ست</p></div>
<div class="m2"><p>هر جاده در ره تو، گریبان پاره ای ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون موج، سرگران گذرم ز آب زندگی</p></div>
<div class="m2"><p>در سایهٔ قد تو،که عمر دوباره ای ست</p></div></div>