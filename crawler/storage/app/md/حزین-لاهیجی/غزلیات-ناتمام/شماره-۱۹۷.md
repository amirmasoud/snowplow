---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>از نالهٔ من خامه خوش آهنگ برآمد</p></div>
<div class="m2"><p>وز نام بلندم، سخن از ننگ برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نغمه،که زیر لب داوود شکستند</p></div>
<div class="m2"><p>ما را ز نی خامه، به این رنگ برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انصاف چو بگرفت عیار سخنم را</p></div>
<div class="m2"><p>با لعل گران قدر تو همسنگ برآمد</p></div></div>