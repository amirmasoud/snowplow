---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>ز خط شده ست عذارش بنفشه زار امروز</p></div>
<div class="m2"><p>کرشمهٔ عجبی می کند بهار امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفته ام به سخن لعل می چکانش را</p></div>
<div class="m2"><p>به خون توبه چرا نشکنم خمار امروز؟</p></div></div>