---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>شود چون جوهر آیینه پیدا، تار می‌افتد</p></div>
<div class="m2"><p>نگردد روشناس آن کس که جوهردار می‌افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند یغما نگاه ناتوان او توانایی</p></div>
<div class="m2"><p>به بستر بوی گل، زان نرگس بیمار می‌افتد</p></div></div>