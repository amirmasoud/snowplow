---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>با چشم سیر، نعمت دنیا چه حاجت است؟</p></div>
<div class="m2"><p>تا آبرو به جاست، به دریا چه حاجت است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری ست کز تپانچه، رخی سرخ می کنیم</p></div>
<div class="m2"><p>ما را به سرخ رویی صهبا چه حاجت است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ژولیده موی، بر سر ما تاج خسروی ست</p></div>
<div class="m2"><p>شوریده را به افسر دارا چه حاجت است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر اجل، به کام من آب حیات ریخت</p></div>
<div class="m2"><p>دنیاگزیده را به مسیحا چه حاجت است؟</p></div></div>