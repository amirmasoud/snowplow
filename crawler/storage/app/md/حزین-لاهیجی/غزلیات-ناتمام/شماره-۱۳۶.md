---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>تپش سینهٔ ما، بانگ درایی دارد</p></div>
<div class="m2"><p>جادهٔ نالهٔ ما راه به جایی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فیضی از میکدهٔ چشم تو برده ست مگر</p></div>
<div class="m2"><p>جام آیینه، می هوش ربایی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر تیغ تو به من دولت جاوید رسید</p></div>
<div class="m2"><p>سایه گویا به سرم، بال همایی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبع وحشی سخنان، می رمد از هر طرفی</p></div>
<div class="m2"><p>فهم هر مصرع ما، فکر جدایی دارد</p></div></div>