---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>تیغ ستمت از می پرزور گران تر</p></div>
<div class="m2"><p>از نشئهٔ خون شد، سر منصور گران تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاطر آزردهٔ من بی غمی امروز</p></div>
<div class="m2"><p>از ترک شراب است به مخمور گران تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر همّت من منت یک حبهٔ دونان</p></div>
<div class="m2"><p>از کوه بود بر کمر مور، گران تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگینی تن بیش شد از طول حیاتم</p></div>
<div class="m2"><p>این بار گران شد ز ره دور گران تر</p></div></div>