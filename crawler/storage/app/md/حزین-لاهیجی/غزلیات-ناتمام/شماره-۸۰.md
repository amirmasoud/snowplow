---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>گذرد گرم ز دل، آه سحرگاهی ما</p></div>
<div class="m2"><p>باربر جاده نگردد، قدم راهی ما</p></div></div>