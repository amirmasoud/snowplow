---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>خاک آسوده، چو سیماب شد ازگریهٔ ما</p></div>
<div class="m2"><p>آستین، حلقهٔ گرداب شد از گریهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنقدر نیست که بر دیدهٔ دشمن ریزیم</p></div>
<div class="m2"><p>خاک این غمکده نایاب شد، ازگریهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه عجب گر فلک، از زاری ما گردد نرم؟</p></div>
<div class="m2"><p>دل سنگین بتان، آب شد از گریهٔ ما</p></div></div>