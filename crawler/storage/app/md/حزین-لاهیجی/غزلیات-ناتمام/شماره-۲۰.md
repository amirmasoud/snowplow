---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>یاسمین بنده شود چاک گریبان تو را</p></div>
<div class="m2"><p>برگ گل جزیه دهد، شقّهٔ دامان تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد، این خرقه به دوشم خنکیهای تو داد</p></div>
<div class="m2"><p>کرد پشمیهٔ من، فکر زمستان تو را</p></div></div>