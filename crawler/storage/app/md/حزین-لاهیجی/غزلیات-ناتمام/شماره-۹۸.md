---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>تا بود داغها، دل آزرده حال داشت</p></div>
<div class="m2"><p>این مرغ پرشکسته، چمن زیر بال داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلشن از جمال تو، ای آفتاب روی</p></div>
<div class="m2"><p>شبنم نبود، گل عرق انفعال داشت</p></div></div>