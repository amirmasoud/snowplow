---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>شمع سان، دیده پرآتش مژه پرنم دارم</p></div>
<div class="m2"><p>داغها بر جگر، از الفت مرهم دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسبم کاش چو یاران دگر جعلی بود</p></div>
<div class="m2"><p>غم عالم ز نسب نامهٔ آدم دارم</p></div></div>