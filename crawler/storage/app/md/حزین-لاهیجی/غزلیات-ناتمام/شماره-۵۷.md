---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>سخنور چون شدی، خاموش بنشین چون نگین اینجا</p></div>
<div class="m2"><p>گل شهرت بود، چون حرف باشد دلنشین اینجا</p></div></div>