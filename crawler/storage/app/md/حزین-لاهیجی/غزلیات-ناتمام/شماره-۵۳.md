---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>نبود آرامشی، شیب و شباب زندگانی را</p></div>
<div class="m2"><p>تپیدنهای دل موجی ست، آب زندگانی را</p></div></div>