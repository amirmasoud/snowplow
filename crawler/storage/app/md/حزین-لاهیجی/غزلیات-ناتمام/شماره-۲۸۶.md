---
title: >-
    شمارهٔ ۲۸۶
---
# شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>زانوی بی کسی هاست، بالین خستهٔ من</p></div>
<div class="m2"><p>شد مومیایی دل، رنگ شکستهٔ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاس ادب به عاشق، نگذاشت اختیاری</p></div>
<div class="m2"><p>کاری نمی گشاید، از دست بستهٔ من</p></div></div>