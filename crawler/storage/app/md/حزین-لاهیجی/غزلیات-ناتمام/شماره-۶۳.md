---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>با اشک روان، قطره زنان است دل ما</p></div>
<div class="m2"><p>ازکهنه سواران جهان است دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرورده ز طفلی، عوض شیر، شرابش</p></div>
<div class="m2"><p>در میکده ها، پیر مغان است دل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سرو روانت چه خیال است، جدایی</p></div>
<div class="m2"><p>در پای تو چون سایه روان است دل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شورش دریا، نکند موج هراسی</p></div>
<div class="m2"><p>پروردهٔ آشوب جهان است دل ما</p></div></div>