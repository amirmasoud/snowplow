---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>ای امّت نگاه تو، جادو خیال‌ها</p></div>
<div class="m2"><p>صحرانورد گردش چشمت، غزال‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افشانده‌اند بال و پر، از بس که می‌زنند</p></div>
<div class="m2"><p>پر در هوای بام تو فرسوده‌بال‌ها</p></div></div>