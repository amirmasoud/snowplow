---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>دمهای زنده را، از اجل کی زیان بود؟</p></div>
<div class="m2"><p>گیرم چو خود کناره، سخن در میان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو آن زبان که صرف سپاس زبان کنم؟</p></div>
<div class="m2"><p>مفتاح گنج خانهٔ معنی، زبان بود</p></div></div>