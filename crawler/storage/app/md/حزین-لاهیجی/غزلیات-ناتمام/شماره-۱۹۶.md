---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>تا حرفی از آن لعل می آلود برآمد</p></div>
<div class="m2"><p>لخت دلم، از دیده نمک سود برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که دلم آتش عشق تو نهان کرد</p></div>
<div class="m2"><p>رفتم نفس از سینه کشم، دود برآمد</p></div></div>