---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>تیغ نگه افتاده گران قاتل ما را</p></div>
<div class="m2"><p>یارای تپیدن نبود، بسمل ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طینتم آن مهر و وفایی که سرشتند</p></div>
<div class="m2"><p>خشت سر خم می کند آخر، گل ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امّیدم از آن چشم سیه، بود نگاهی</p></div>
<div class="m2"><p>دردا که تغافل کده کردی دل ما را</p></div></div>