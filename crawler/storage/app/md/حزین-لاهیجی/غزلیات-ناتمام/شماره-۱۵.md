---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>سلطان همّتم، ز جهان شسته دست را</p></div>
<div class="m2"><p>چون سیل، پشت پا زده ام خاک پست را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف، کار محتسب روزگار نیست</p></div>
<div class="m2"><p>یکسان کند معامله، هشیار و مست را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشکل که پر کند ز تهی کاسگی حزین</p></div>
<div class="m2"><p>این مشت خاک، دیده دنیاپرست را</p></div></div>