---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>نی می دهد از اصل مقامات صدایی</p></div>
<div class="m2"><p>پیچیده ز کلکم به سماوات صدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مسجد اگر مست سماعم، عجبی نیست</p></div>
<div class="m2"><p>خورده ست به گوشم، ز خرابات صدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عین اشارات تو، گویای خموشم</p></div>
<div class="m2"><p>معنیست مقامات و مقالات، صدایی</p></div></div>