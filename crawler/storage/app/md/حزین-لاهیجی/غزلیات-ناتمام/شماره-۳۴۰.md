---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>بنواخت نی را، لبهای نایی</p></div>
<div class="m2"><p>ما بی نواییم، آه از جدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کعبهٔ دل، مانده ست داغم</p></div>
<div class="m2"><p>چون فلس ماهی، از ناروایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شام هجرت، چون شمع کشته</p></div>
<div class="m2"><p>مانده ست چشمم، بی روشنایی</p></div></div>