---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>خار رهت، به روضهٔ رضوان برابر است</p></div>
<div class="m2"><p>خاک درت، به چشمه ی حیوان برابر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شوخی نگاه تو آموختم سخن</p></div>
<div class="m2"><p>هر نقطه ام، به چشم غزالان برابر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود را به چنگ لطمهٔ دنیا نیفکنی</p></div>
<div class="m2"><p>این موجهٔ سراب، به طوفان برابر است</p></div></div>