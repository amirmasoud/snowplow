---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>تا زلف تو بر دوش و برم سایه فکن شد</p></div>
<div class="m2"><p>هر چاک دلم، جادهٔ صحرای ختن شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدهٔ بخت سیاهم چو گران خواب شود</p></div>
<div class="m2"><p>تیغ مژگان رسای تو سیه تاب شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر تسلیم، پی سجدهٔ مستانه به خاک</p></div>
<div class="m2"><p>می گذارم، اگر ابروی تو محراب شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا به خاک چو مژگان اشکبار شود</p></div>
<div class="m2"><p>کفن پر آب تر از ابر مایه دار شود</p></div></div>