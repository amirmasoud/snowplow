---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>از سر من چرا کشد، سرو قد تو پای را</p></div>
<div class="m2"><p>خاک ره تو کرده ام، فرق سپهرسای را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله به خس نمی کند، اینهمه سرگران وشی</p></div>
<div class="m2"><p>تا به کی از کفم کشی، دامن کبریای را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمهٔ خامشی دهد، بلبل خوش نوای را</p></div>
<div class="m2"><p>چون به سخن درآورم، خامهٔ مشکسای را</p></div></div>