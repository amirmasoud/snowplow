---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>چهره نما که در چمن، شور هزار گل کند</p></div>
<div class="m2"><p>طرّه گشا که در خزان، بوی بهار گل کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپند آتش خویشم، کسی دوا چه کند؟</p></div>
<div class="m2"><p>به بی قراری من صبر بی نوا چه کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حزین سوخته دل، می دهد به حسرت جان</p></div>
<div class="m2"><p>زمانه عهد شکن، یار بی وفا چه کند؟</p></div></div>