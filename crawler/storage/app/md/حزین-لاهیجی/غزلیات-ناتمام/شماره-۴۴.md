---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>مردان کنند خوش، غم و هجر همیشه را</p></div>
<div class="m2"><p>آب بقاست آتش تب، شیر بیشه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بحر ریزیش به گلو، العطش زند</p></div>
<div class="m2"><p>جایی که نخل حرص فروبرد ریشه را</p></div></div>