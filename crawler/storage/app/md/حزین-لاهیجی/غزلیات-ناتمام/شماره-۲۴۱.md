---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>از چرخ تنک حوصله، پروا چه کند کس؟</p></div>
<div class="m2"><p>با دشمن نامرد، مدارا چه کند کس؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل کندن و کام دل ازو، هر دو محال است</p></div>
<div class="m2"><p>با قحبهٔ مستورهٔ دنیا چه کند کس؟</p></div></div>