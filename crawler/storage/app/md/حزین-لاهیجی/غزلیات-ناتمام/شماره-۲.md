---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>آوارهٔ عالم، نگهی، ساخته ما را</p></div>
<div class="m2"><p>آن گوشه نشین، دربه در انداخته ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مهرهٔ ششدر شده، در هجر تو ماتیم</p></div>
<div class="m2"><p>دریاب که نیرنگ غمت باخته ما را</p></div></div>