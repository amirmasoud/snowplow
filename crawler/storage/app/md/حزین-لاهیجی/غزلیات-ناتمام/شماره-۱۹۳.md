---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>یکایک از نظرم نور پیکران رفتند</p></div>
<div class="m2"><p>ستاره های شب افروزم از میان رفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زمهریر جهان، هم صفیر زاغانم</p></div>
<div class="m2"><p>خزان رسید و گل افسرد و بلبلان رفتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خون دل شکنم بعد از این خمار، مگر</p></div>
<div class="m2"><p>به سنگ، لاله قدح زد که می کشان رفتند</p></div></div>