---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>شد قسمت خال تو،که مشک ختن ماست</p></div>
<div class="m2"><p>بوسیدن آن لب، که زیاد از دهن ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژگان تر، به هجر تو، ابر بهار ماست</p></div>
<div class="m2"><p>در جوش داغ، سینهٔ ما، لاله زار ماست</p></div></div>