---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>بی باده سیه مست، شب از یاسمن کیست؟</p></div>
<div class="m2"><p>فیض سحر از سینهٔ گل پیرهن کیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظّاره، خیال کِه در آغوش کشیده ست؟</p></div>
<div class="m2"><p>حیران نگهی، آینه دار بدن کیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد صفحهٔ من، جزیه ستان ورق گل</p></div>
<div class="m2"><p>این مشک تر از ناف غزال ختن کیست؟</p></div></div>