---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>با آنکه نیست از تو بتی دلنوازتر</p></div>
<div class="m2"><p>از روز حشر، شد شب هجرم درازتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل شکوه از کدام جفای تو سر کند؟</p></div>
<div class="m2"><p>هر شیوهٔ تو از دگری جانگدازتر</p></div></div>