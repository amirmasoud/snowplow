---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>ای تهیدست، به امّید و امل غره مشو</p></div>
<div class="m2"><p>مزرعی را که نکشتی، نتوان کرد درو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من تنک مایه ام و پیر مغان مستغنی</p></div>
<div class="m2"><p>وای اگر خرقهٔ سالوس نگیرد به گرو</p></div></div>