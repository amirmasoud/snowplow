---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>نماید بی سبب حاصل، مسبب مدعای من</p></div>
<div class="m2"><p>چو موج آید به ساحل، کشتی بی ناخدای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دنیا، خانهٔ از نقش پا برچیده ای دارم</p></div>
<div class="m2"><p>چه خواهد برد سیلاب حوادث، از سرای من</p></div></div>