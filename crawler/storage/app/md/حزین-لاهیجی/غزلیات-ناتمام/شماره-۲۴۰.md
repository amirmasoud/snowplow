---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>بسته پای چو من بی پر و بالی که مپرس</p></div>
<div class="m2"><p>زیر لب دارم ازین عقده، سؤالی که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوهٔ شمع تجلّی، شب هجران تو داشت</p></div>
<div class="m2"><p>با خیال تو، مرا بود وصالی که مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت از آن کوی پی عزم سفر می بستم</p></div>
<div class="m2"><p>دل به دامان من آویخت، به حالی که مپرس</p></div></div>