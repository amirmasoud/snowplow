---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>قامت شده‌ست خم، من دیرینه سال را</p></div>
<div class="m2"><p>باید به روی تیغ تو دید، این هلال را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخی که کاست، وقت تمامی، هلال را</p></div>
<div class="m2"><p>کی نقص شان ما ننماید کمال را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر خموشیم، سپر زخم ذلت است</p></div>
<div class="m2"><p>با دست رد چه کار لب بی سؤال را؟</p></div></div>