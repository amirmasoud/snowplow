---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>رنگین بود سخن، دل در خون تپیده را</p></div>
<div class="m2"><p>کردم روانه نامه رنگ پریده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت است اگر نصیب شود خواب راحتی</p></div>
<div class="m2"><p>بالین کنیم دستِ ز دنیا بریده را</p></div></div>