---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>در کشور ایجاد، ندانم چه گلستم</p></div>
<div class="m2"><p>دانم که صنمگاه بتان چگلستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بعد، بود دست من و چاک گریبان</p></div>
<div class="m2"><p>نه دامن دلدار به دست و نه دلستم</p></div></div>