---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>شوریده سرم، طرهٔ پیچان تو دارد</p></div>
<div class="m2"><p>آشفته دلم، زلف پریشان تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبن گونه، فرومانده و بی تاب و توانم</p></div>
<div class="m2"><p>پنهان چه کنم؟ سستی پیمان تو دارد</p></div></div>