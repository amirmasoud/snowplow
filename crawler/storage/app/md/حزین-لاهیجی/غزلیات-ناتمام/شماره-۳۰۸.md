---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>دردت به دوای دل بی تاب رسیده</p></div>
<div class="m2"><p>از غیب، رسولیست، به اصحاب رسیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نی به خروش از نفس سینه خراشم</p></div>
<div class="m2"><p>تاری ست تن من که به مضراب رسیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد دلم از گریهٔ مستانه، طرب ها</p></div>
<div class="m2"><p>عید است که ویرانه به سیلاب رسیده</p></div></div>