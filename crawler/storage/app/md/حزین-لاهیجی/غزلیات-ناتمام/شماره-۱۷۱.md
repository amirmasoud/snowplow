---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>آزاده، از حیات خود آزار می‌کشد</p></div>
<div class="m2"><p>باری‌ست این، که دوش سبکبار می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خصم تندخوست دلم، کورهٔ گداز</p></div>
<div class="m2"><p>زین خون گرم، نیشتر آزار می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنها نه کفر زلف تو زد راه تقویم</p></div>
<div class="m2"><p>زاهد به سبحه، رشتهٔ زنّار می‌کشد</p></div></div>