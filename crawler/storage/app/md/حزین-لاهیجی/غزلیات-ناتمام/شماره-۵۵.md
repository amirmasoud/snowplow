---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>ز دوری خاطرم تنگ است و نتوانم رسید آنجا</p></div>
<div class="m2"><p>گشاد دل در آن ابروست، قفل اینجا، کلید آنجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهم تا کوی او دور و ندارم طاقت پایی</p></div>
<div class="m2"><p>رهی زافتادگی، چون جاده می باید کشید آنجا</p></div></div>