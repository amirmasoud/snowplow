---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>زلف سیهش، آتش بیداد برآورد</p></div>
<div class="m2"><p>دود از شکن طرّهٔ شمشاد برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخاست مرا از قفس سینه صفیری</p></div>
<div class="m2"><p>شور از دل مرغان چمن زاد برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخساره نمودیّ و مرا مردمک چشم</p></div>
<div class="m2"><p>در دیده سپندی شد و فریاد برآورد</p></div></div>