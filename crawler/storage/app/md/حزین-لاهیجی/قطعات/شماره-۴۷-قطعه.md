---
title: >-
    شمارهٔ ۴۷ - قطعه
---
# شمارهٔ ۴۷ - قطعه

<div class="b" id="bn1"><div class="m1"><p>دوش دلم چون کشید خنجر آه از قراب</p></div>
<div class="m2"><p>پشت حواصل پدیدگشت ز پر عقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کین فلک مهر شد، از نفس پاک من</p></div>
<div class="m2"><p>ترک سمن چهر شد، هندوی مشکین نقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داور رومی به شب، تیغ کشید از غضب</p></div>
<div class="m2"><p>بر سپه زنگ بُرد حملهٔ افراسیاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رایت پیکار زد، خسرو خاور زمین</p></div>
<div class="m2"><p>برق به کهسار زد، خیمهٔ زرّین طناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیت آتوا الزکاه از صحف پاک خواند</p></div>
<div class="m2"><p>سیم کواکب فشاند خسرو صاحب نصاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلخن نمرود را، دود نشست از صعود</p></div>
<div class="m2"><p>آتش بی دود را، داد فلک التهاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برقع مشکین پرند، شاهد گیتی گشود</p></div>
<div class="m2"><p>مهر به گردش فکند، ساغر لعل مذاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لیلی شب راکه بود، در بر یرقان کبود</p></div>
<div class="m2"><p>غازه به رخساره زد، ماشطهٔ آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلک نواساز رفت، بر سر رامشگری</p></div>
<div class="m2"><p>پیر خرد باز رفت، بر سر عهد شباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باربدی پرده را، دید به هنجار نیست</p></div>
<div class="m2"><p>خسرو دل شد خجل، زین روش ناصواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زخمه به هر سان زدم، بر رگ تار نفس</p></div>
<div class="m2"><p>نغمه به سامان نگشت، کام نشد ذوق یاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفحه روح القدس، در دل افسرده ام</p></div>
<div class="m2"><p>پرده سرایی گرفت، این متعالی خطاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کای گهر افروز دل، تیره چرایی چنین؟</p></div>
<div class="m2"><p>محفل شمع چگل، روشن و تو در حجاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شانه رقم را بکش، از دل صد چاک خویش</p></div>
<div class="m2"><p>طرّه انفاس را، چند دهی پیچ و تاب؟</p></div></div>