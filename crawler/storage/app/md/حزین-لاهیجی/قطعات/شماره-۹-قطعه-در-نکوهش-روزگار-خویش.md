---
title: >-
    شمارهٔ ۹ - قطعه در نکوهش روزگار خویش
---
# شمارهٔ ۹ - قطعه در نکوهش روزگار خویش

<div class="b" id="bn1"><div class="m1"><p>روزگاریست، عقل می کوبد</p></div>
<div class="m2"><p>کنج آسایش اختیارکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در به روی جهانیان بندم</p></div>
<div class="m2"><p>کنج آسایش اختیارکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفر دور مرگ، نزدیک است</p></div>
<div class="m2"><p>فکر سامان آن دیار کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زر داغی، کنم به کیسهٔ دل</p></div>
<div class="m2"><p>گهر اشک در کنار کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست از خوان آرزو بکشم</p></div>
<div class="m2"><p>به همین خون دل مدار کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق بازی به خویشتن فکنم</p></div>
<div class="m2"><p>ترک یاران بدقمارکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنگم از شهر، رو به کوه آرم</p></div>
<div class="m2"><p>خانه در سنگ، چون شرار کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لیک چون کارها به دست خداست</p></div>
<div class="m2"><p>نتوانم به خویش کار کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین سپس فرصت از خدا طلبم</p></div>
<div class="m2"><p>دیده در راه انتظار کنم</p></div></div>