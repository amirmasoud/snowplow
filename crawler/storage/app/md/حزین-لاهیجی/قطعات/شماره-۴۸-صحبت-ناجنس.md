---
title: >-
    شمارهٔ ۴۸ - صحبت ناجنس
---
# شمارهٔ ۴۸ - صحبت ناجنس

<div class="b" id="bn1"><div class="m1"><p>دارم آزرده دلی، تنگ تر از دیدهٔ میم</p></div>
<div class="m2"><p>شده ام داغ درین دایره چون نقطه جیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشد از شش جهتم راه گشادی پیدا</p></div>
<div class="m2"><p>آه از آن زر، که فتد در گره بخل لئیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم از الفت تن شکوه، که نیکو گفتند</p></div>
<div class="m2"><p>روح را صحبت ناجنس عذابی ست الیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می رود دم به نوا نالهٔ فرخنده مقام</p></div>
<div class="m2"><p>یاد آن روز که بودم به خرابات، مقیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم از صحبت این نکته فروشان خون شد</p></div>
<div class="m2"><p>بی سوادند جهان، نسخه ایام سقیم</p></div></div>