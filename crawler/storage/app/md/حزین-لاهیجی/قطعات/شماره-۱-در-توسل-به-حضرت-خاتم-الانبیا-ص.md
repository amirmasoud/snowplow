---
title: >-
    شمارهٔ ۱ - در توسل به حضرت خاتم الانبیاء (ص)
---
# شمارهٔ ۱ - در توسل به حضرت خاتم الانبیاء (ص)

<div class="b" id="bn1"><div class="m1"><p>یا خاتم النبییّن، غمخوار عالمی تو</p></div>
<div class="m2"><p>پیش تو چون ننالم؟ از جور آسمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عرض شکوه هرچند، خالی نمی شود دل</p></div>
<div class="m2"><p>از من سخن طرازی، از خامه خون چکانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناید نهفتن از من، با لطف شامل تو</p></div>
<div class="m2"><p>رازی که می نماید، در سینه ام سنانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیرینه شد چو مخلص، در حضرت است گستاخ</p></div>
<div class="m2"><p>نتوانم از تو کردن، اسرار دل نهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچشم کوثر از توست، پیمانهٔ املها</p></div>
<div class="m2"><p>لبریزگوهر ازتوست، گنجینهٔ امانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماهیچهٔ لوایت، آرد به درع و خفتان</p></div>
<div class="m2"><p>کاری که می کند مه، با پیکر کتانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریادرس خدیوا، بیداد بین که کرده ست</p></div>
<div class="m2"><p>هندوی چرخ ما را، تاراج ترکمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور از حمایت تو، دور سپهر بشکست</p></div>
<div class="m2"><p>پشت خمیده ام را، از بار زندگانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بالین و بستر من، خشتیّ و بوریایی ست</p></div>
<div class="m2"><p>این است در بساطم، ز اسباب این جهانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نقد در کنارم، رنگ طلایی ای هست</p></div>
<div class="m2"><p>ز الوان نعمتم نیست، جز اشک ارغوانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگسسته الفت من، از خیل بی وفایان</p></div>
<div class="m2"><p>پوشیده همّت من، چشم از نعیم فانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آواره همچو من نیست، خاکی نهاد دیگر</p></div>
<div class="m2"><p>تا این کهن بنا را، افلاک گشته بانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ده سال شد که در هند، عمرم به رایگان رفت</p></div>
<div class="m2"><p>زین سان کسی نداده، بر باد زندگانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دم سردی زمانه، خرم بهارم افسرد</p></div>
<div class="m2"><p>عریان تن است نخلم، از باد مهرگانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای سر غبار راهت، زان خاک سرمه واری</p></div>
<div class="m2"><p>خونبار دیدهام را، بفرست ارمغانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جایی که نور رویت، گلگونه برفروزد</p></div>
<div class="m2"><p>از ذره کمتر آید، خورشید خاورانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در خون نشسته دارد، هند جگر فشارم</p></div>
<div class="m2"><p>من داد شکوه دادم، باقی دگر تو دانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه قوّتی که آیم تا خاک آستانت</p></div>
<div class="m2"><p>نه طاقتی که سازم، با حرقت چنانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از باد سرد مهری، شاخ خزان رسیده</p></div>
<div class="m2"><p>رخساره در زریری، ز اغصان ضیمرانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نفس بلند همّت، تاکی کند تحمّل</p></div>
<div class="m2"><p>با طعنهٔ اراذل، با نخوت ادانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در سومنات دهلی، مدح تو می سرایم</p></div>
<div class="m2"><p>زان پیشتر که آید، بلبل به زندخوانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر فردی از مدیحت، باشد حدیث منزل</p></div>
<div class="m2"><p>من اسرت المعلّی، من سرحهٔ المعانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر سو صریر کلکم، طبل سکندری زد</p></div>
<div class="m2"><p>تا گشت در هوایت، سرگرم مدح خوانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنگر به مایه داری، نیسان خامه ام را</p></div>
<div class="m2"><p>جز من کسی نیارد، زین سان گهر فشانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر خاک عجز ریزد، سرپنجهٔ تهمتن</p></div>
<div class="m2"><p>چون خامه ام گشاید، بازوی پهلوانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لب برگشا و گوهر، در جیب بحر و کان کن</p></div>
<div class="m2"><p>کف برگشا و بفشان، صدگنج شایگانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از داغ مهرت امروز، محفل فروزِ دهرم</p></div>
<div class="m2"><p>کمتر دهد چو من یاد، آثار باستانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از مصرعی توان یافت، طبع هنر طرازم</p></div>
<div class="m2"><p>جان را به تن نباشد، این جودت و روانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرگز نداشت حسان، رطب اللسانی من</p></div>
<div class="m2"><p>هرگز نکرد سحبان، این معجز البیانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از صولت مدیحت، ملک سخن گرفته</p></div>
<div class="m2"><p>گردن فرازکلکم، با چتر کاویانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر رخصت تو باشد، از لخت دل نمایم</p></div>
<div class="m2"><p>مستان معنوی را، تا حشر میزبانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قدر سخن بلند است، زیرا که دارد آباد</p></div>
<div class="m2"><p>تا حشر سروران را، قصر رفیع شانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از معجز سخن ماند، روح اللهی به عیسی</p></div>
<div class="m2"><p>موسی کلیمِ حق شد، از فیضِ نکته دانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شدکاخ ملک و ملت، ازکلک نکته پرور</p></div>
<div class="m2"><p>مستهدم المفاسد، مستحکم المبانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از عنصری بود نام، شاهان غزنوی را</p></div>
<div class="m2"><p>از گنجوی بود یاد، بهرام شاه ثانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن آل بویه رفتند، امّا به روزگاران</p></div>
<div class="m2"><p>دارد روانشان شاد، مهیار دیلمانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سلجوقیان گذشتند، امّا ز انوری ماند</p></div>
<div class="m2"><p>نام بلند ایشان، بر لوح این جهانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دور اتابکان رفت، امّا کلام سعدی</p></div>
<div class="m2"><p>پرورده نامشان را، با آب زندگانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ذکر اوبس باقیست، از نکته های سلمان</p></div>
<div class="m2"><p>نام تکش دهد یاد، خلّاق اصفهانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شاه مظفری را، نسلی نماند لیکن</p></div>
<div class="m2"><p>هر مصرعی ز حافظ، شد شمع دودمانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>راه سخن نبودی، در حضرتت حزین را</p></div>
<div class="m2"><p>از عفو اگر نبودی، امّید طیلسانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کلکم ز فیض لطفت زانسان به جلوه آید</p></div>
<div class="m2"><p>کز جنبش بهاران، شمشاد بوستانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا سرفراز کرده ست، نام تو خامه ام را</p></div>
<div class="m2"><p>با گوی مهر دارد، دعویّ صولجانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر صفحه ام بنازد، جمشید و نقش خاتم</p></div>
<div class="m2"><p>از خامه ام ببالد، ارژنگ و کلکِ مانی</p></div></div>