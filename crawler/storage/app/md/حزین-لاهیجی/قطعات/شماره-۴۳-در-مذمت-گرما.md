---
title: >-
    شمارهٔ ۴۳ - در مذمّت گرما
---
# شمارهٔ ۴۳ - در مذمّت گرما

<div class="b" id="bn1"><div class="m1"><p>در جهنمکده هند که از تاب هوا</p></div>
<div class="m2"><p>شعله ور چون پر پروانه بود، بال ملخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد افسرده تو را، شعبده چرخ حزین</p></div>
<div class="m2"><p>چه توان کرد کنون، ماهیت افتاده به فخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که گرم است هوا، آید اگر دمسردی</p></div>
<div class="m2"><p>می دهم گوش، زند بیهده چندان که زنخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکسی را شطی از هر بن مویی جاریست</p></div>
<div class="m2"><p>شاید ار سیل عرق، شوید ازین خاک، وسخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه همین جان اسیر، از تف ایام گداخت</p></div>
<div class="m2"><p>تن هم از کاهش آلام، نحیف است چو نخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشنان فلکِ مجمره گردان بخیل</p></div>
<div class="m2"><p>خنک آن دم، که نویسند برات تو به یخ</p></div></div>