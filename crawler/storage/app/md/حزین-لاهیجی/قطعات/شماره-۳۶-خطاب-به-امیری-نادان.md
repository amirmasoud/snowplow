---
title: >-
    شمارهٔ ۳۶ - خطاب به امیری نادان
---
# شمارهٔ ۳۶ - خطاب به امیری نادان

<div class="b" id="bn1"><div class="m1"><p>چارپایی شنیده ام مرده است</p></div>
<div class="m2"><p>از امیرکبیر، طال بقاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون که سنجیدم این سخن، گفتم</p></div>
<div class="m2"><p>غلط افتاده است در افواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد خویش، آن که چون امیر گذاشت</p></div>
<div class="m2"><p>کی وجودش شود به مرگ، تباه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلف آن را که هست، خود باقی است</p></div>
<div class="m2"><p>خرد آمد برین حدیث گواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنده را، مرده کی توان گفتن؟</p></div>
<div class="m2"><p>خود حکم باش حسبهًٔ لِلّٰه</p></div></div>