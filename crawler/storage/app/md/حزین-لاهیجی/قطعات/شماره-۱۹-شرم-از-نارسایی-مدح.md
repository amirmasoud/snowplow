---
title: >-
    شمارهٔ ۱۹ - شرم از نارسایی مدح
---
# شمارهٔ ۱۹ - شرم از نارسایی مدح

<div class="b" id="bn1"><div class="m1"><p>گشته ست صفحه، دامن دشت ختن حزین</p></div>
<div class="m2"><p>نازم خرام کبک همایون مثال را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حکم ماست ملک سلیمانی سخن</p></div>
<div class="m2"><p>گوییم شکر سلطنت بی زوال را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیروی کلک ماست که بالیده از غرور</p></div>
<div class="m2"><p>بر خاک عجز، ناصیهٔ پور زال را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اوج فلک در آب گهر گشته غوطه ور</p></div>
<div class="m2"><p>کلکم گشوده تاکف دریا نوال را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیکن ز شرم کوتهی از مدح مرتضی</p></div>
<div class="m2"><p>غسلی برآورم، عرق انفعال را</p></div></div>