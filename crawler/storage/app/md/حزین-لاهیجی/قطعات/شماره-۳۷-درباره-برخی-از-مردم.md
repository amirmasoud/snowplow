---
title: >-
    شمارهٔ ۳۷ - دربارهٔ برخی از مردم
---
# شمارهٔ ۳۷ - دربارهٔ برخی از مردم

<div class="b" id="bn1"><div class="m1"><p>گفت یاری، حزین بی دل را</p></div>
<div class="m2"><p>خلق را در فساد می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه مست شراب کبر و حسد</p></div>
<div class="m2"><p>همه غرق عناد می ببنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه چه آمد چه شد که نیکان را</p></div>
<div class="m2"><p>بدتر از قوم عاد می بینم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ای دوست، ترک عربده کن</p></div>
<div class="m2"><p>در تغافل، سداد می بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمی از هیچ نیست یاران را</p></div>
<div class="m2"><p>جنس غیرت کساد می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>...خرشان اگر حواله کنی</p></div>
<div class="m2"><p>از دهنشان زیاد می بینم</p></div></div>