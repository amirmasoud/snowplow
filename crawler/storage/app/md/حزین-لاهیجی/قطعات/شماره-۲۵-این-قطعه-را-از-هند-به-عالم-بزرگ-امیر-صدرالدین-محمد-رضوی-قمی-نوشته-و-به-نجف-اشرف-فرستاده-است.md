---
title: >-
    شمارهٔ ۲۵ - این قطعه را از هند به عالم بزرگ امیر صدرالدین محمد رضوی قمی نوشته و به نجف اشرف فرستاده است
---
# شمارهٔ ۲۵ - این قطعه را از هند به عالم بزرگ امیر صدرالدین محمد رضوی قمی نوشته و به نجف اشرف فرستاده است

<div class="b" id="bn1"><div class="m1"><p>حزین، از تقاضای همّت برآنم</p></div>
<div class="m2"><p>که خوان سخن را به اخوان فرستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شوری که از سینه ام موج زن شد</p></div>
<div class="m2"><p>به زخم جگرها، نمکدان فرستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زکلک عراقی نژاد خود، از هند</p></div>
<div class="m2"><p>سوادی به خاک صفاهان فرستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه پوشم گهر را زگوهرشناسان؟</p></div>
<div class="m2"><p>از این لعل، درجی به گیلان فرستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکنج قفس، تنگ دارد دلم را</p></div>
<div class="m2"><p>صفیری به مرغ گلستان فرستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خاک ره کلک آهو خرامم</p></div>
<div class="m2"><p>شمیمی به ناف غزالان فرستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رطب های شیرین تر از قند مصری</p></div>
<div class="m2"><p>به رطب اللسانان عدنان فرستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این قحط سال بلاغت، حدیثی</p></div>
<div class="m2"><p>به معجز بیانان قحطان فرستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو برقع گشایم ز رخسار معنی</p></div>
<div class="m2"><p>فروغی به خورشید تابان فرستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلام من از فهم شاعر فزون است</p></div>
<div class="m2"><p>مگر ارمغان حکیمان فرستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تراشیدم از دل سخن را که شاید</p></div>
<div class="m2"><p>به دریادلی، زادهٔ کان فرستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر آنم که اوراق اشعار خود را</p></div>
<div class="m2"><p>چو شیرازه بندم، به لقمان فرستم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخن های من گرچه جان است یکسر</p></div>
<div class="m2"><p>همان به که جان را به جانان فرستم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپهر فضایل، ملاذ افاضل</p></div>
<div class="m2"><p>که سویش تحیّت فراوان فرستم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شبل نبی و ولی، صدر اعظم</p></div>
<div class="m2"><p>جگر پاره ای چند، شایان فرستم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز ابر قلم، تحفهٔ محفلِ او</p></div>
<div class="m2"><p>به خاک نجف، دُرّ غلتان فرستم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گذارم من این رسم،کز تنگدستی</p></div>
<div class="m2"><p>کمین قطره را سوی عمّان فرستم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو خود دورم از وصل آن یار دیرین</p></div>
<div class="m2"><p>ستم نامهٔ جور هجران فرستم</p></div></div>