---
title: >-
    شمارهٔ ۴۲ - در ذمّ بعضی از اصحاب غرور
---
# شمارهٔ ۴۲ - در ذمّ بعضی از اصحاب غرور

<div class="b" id="bn1"><div class="m1"><p>ای صاحبی که مایهٔ تفریح عالمی</p></div>
<div class="m2"><p>ذات مبارکت، سبب کامرانی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشنو سه چار مصرع غرّا، ز خامه ام</p></div>
<div class="m2"><p>اکنون که فطرتت به سر نکته دانی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسمی ست مبتذل، گلهٔ دوستان ز هم</p></div>
<div class="m2"><p>نبود ز دل شکایت یاران، زبانی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنجانده ای ز ما دل نامهربان خویش</p></div>
<div class="m2"><p>با ما مگر فلک به سر مهربانی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر نجات، یا ملک الموت می زند</p></div>
<div class="m2"><p>آن را که اختلاط تو، در جان ستانی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپسند، برگ ریزِ حواس معاشران</p></div>
<div class="m2"><p>ای خوش نفس نسیم، دمت مهرگانی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش بی تکلّفانه به هر بزم می شدی</p></div>
<div class="m2"><p>اکنون چه شدکه ناز تو در سرگرانی است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض از حریص گشتنِ اصحاب برده ای</p></div>
<div class="m2"><p>خودداریت نه شرم بود، شخ کمانی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر هفت کردن تو، مکرّر شده ست لیک</p></div>
<div class="m2"><p>در مذهب تو فرض، چو سبع مثانی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد طعنه می زنی به هما شهپران عشق</p></div>
<div class="m2"><p>بوم تو در هوای بلند آشیانی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با بخردان، جفای فلک رسم کهنه ای ست</p></div>
<div class="m2"><p>بر ما ترفّعت، ستم آسمانی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبود حماقت تو شگفتی که از ازل</p></div>
<div class="m2"><p>روح حمار با جسدت، یار جانی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بانگ کلاب با مه تابنده، تازه نیست</p></div>
<div class="m2"><p>خفّاش را ستیزه به خور، باستانی است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وارونه است کار تو، باشد ز هر قماش</p></div>
<div class="m2"><p>بی شبهه، تار و پود تو هندوستانی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی صرفه است، عربده با سرگذشتگان</p></div>
<div class="m2"><p>در رزم، خامهام، علم کاویانی است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بایست پاس خاطر رندان نگاه داشت</p></div>
<div class="m2"><p>اکنون چه سود؟ سیل بلا، در روانی است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حیرانم ز غرابت ذات شریف تو</p></div>
<div class="m2"><p>این جوهر لطیف، نه بحری، نه کانی است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>الوان ریش مختلفت را شمرده ام</p></div>
<div class="m2"><p>سبز و بنفش، زرد و کبود، ارغوانی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رنگین افاده ها و خرافات مضحکت</p></div>
<div class="m2"><p>طامات بن هبنقه را ، شکل ثانی است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای بی قرینه، جفت تو باشد مگر حمار</p></div>
<div class="m2"><p>منکر مشو، دلالت این اقترانی است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>احیای نام نیک توکردیم در جهان</p></div>
<div class="m2"><p>کلکم همان به راه تو در جان فشانی است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نظمم سبک مسنج به میزان اعتبار</p></div>
<div class="m2"><p>هر چند کاین متاع گران رایگانی است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر مایل ستایش خویشی، اشاره کن</p></div>
<div class="m2"><p>از خرمن، این نمونه برای نشانی است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با خود بسنج، وسعت میدان خویش را</p></div>
<div class="m2"><p>ما را کمیت خامه به چابک عنانی است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اینک محقری گذراندم، علی الحساب</p></div>
<div class="m2"><p>از مخلصان خود بپذیر، ارمغانی است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آسوده باد تارک قدرت ز حادثات</p></div>
<div class="m2"><p>در ظلّ خامه ام که درفش کیانی است</p></div></div>