---
title: >-
    بخش ۵ - رخ طاعت به خاک ضراعت سودن و لب سوال به منتهی الآمال گشودن
---
# بخش ۵ - رخ طاعت به خاک ضراعت سودن و لب سوال به منتهی الآمال گشودن

<div class="b" id="bn1"><div class="m1"><p>به هجران، زاری دلهای خونین</p></div>
<div class="m2"><p>ز حد بگذشت یا ختم النبیین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اشک و آه مهجوران بی تاب</p></div>
<div class="m2"><p>جهانی غوطه زد در آتش و آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاه درد، با جان در ستیز است</p></div>
<div class="m2"><p>لب هر زخم دل، خونابه ریز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان از جلوهٔ جان پرورت دور</p></div>
<div class="m2"><p>به ما شد تنگتر از دیدهٔ مور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدی تا گنج خلوتخانهٔ خاک</p></div>
<div class="m2"><p>ز داغ، اندوخت صد گنجینه، افلاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قد محراب، زین محنت دوتا شد</p></div>
<div class="m2"><p>که از سرو سرافرازت جدا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قدرش، سایه بر عرش برین بود</p></div>
<div class="m2"><p>که بر پای تو، منبر، پایه می سود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون در گوشه ای افتاده مدهوش</p></div>
<div class="m2"><p>به حسرت یک دهن خمیازه آغوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جدا از پرتو آن روی دلکش</p></div>
<div class="m2"><p>به دل، قندیل را افتاده آتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز داغ هجرت ای شمع شب افروز</p></div>
<div class="m2"><p>به شبها، شمع می گرید به صد سوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برافروز ای چراغ چشم ایجاد</p></div>
<div class="m2"><p>جهان شد بی فروغت ظلمت آباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به رخ، آرایش شمس و قمر کن</p></div>
<div class="m2"><p>شب تاربک هجران را سحرکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به کام دل رسید آخر نقابت</p></div>
<div class="m2"><p>درین خلوت، ز حد بگذشت خوابت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خواب ای مهر عالمتاب برخیز</p></div>
<div class="m2"><p>تو بخت عالمی، از خواب برخیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خلاصی ده ز هجران جان ما را</p></div>
<div class="m2"><p>به جان منّت نه و بنما لقا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلند آوازه گردان طبل شاهی</p></div>
<div class="m2"><p>ز نو، زن نوبت عالم پناهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قدم بر تارک کروبیان زن</p></div>
<div class="m2"><p>علم بر بام هفتم آسمان زن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مشرف کن بساط خاکیان را</p></div>
<div class="m2"><p>منور منزل افلاکیان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر، ای خورشید جان از خواب برکن</p></div>
<div class="m2"><p>کنار خاک را جیب سحر کن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چراغ افروز بزم قدسیان شو</p></div>
<div class="m2"><p>رواج آموزِ کارِ انس و جان شو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو از جا، هول رستاخیز خیزد</p></div>
<div class="m2"><p>رخ از شرمندگیها، رنگ ریزد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نظر بگشا بر احوال تباهم</p></div>
<div class="m2"><p>بجنبان لب، پی عذر گناهم</p></div></div>