---
title: >-
    بخش ۷ - چمن طرازی این صحیفهٔ لا ریب به ذکر اشارت غیب
---
# بخش ۷ - چمن طرازی این صحیفهٔ لا ریب به ذکر اشارت غیب

<div class="b" id="bn1"><div class="m1"><p>درپن خلوتسرای عاری از عیب</p></div>
<div class="m2"><p>دل است آیینه دار شاهد غیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند حل، هر چه پیشت مشکل است آن</p></div>
<div class="m2"><p>ز جام جم چه می پرسی؟ دل است آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروغ دل، چو گردد پرتو افکن</p></div>
<div class="m2"><p>چراغ روز گردد شمع ایمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی از محرمان کعبه دل</p></div>
<div class="m2"><p>جرس جنبان این فیروزه محمل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کلک فکر، کشاف حقایق</p></div>
<div class="m2"><p>رصد بند سطرلاب دقایق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلش آیینه دار حسن معنی</p></div>
<div class="m2"><p>ضمیرش طور انوار تجلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعادت خانه زاد دودمانش</p></div>
<div class="m2"><p>رخ دولت به خاک آستانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل خوش بوی باغ آشنایی</p></div>
<div class="m2"><p>ازو گلبو، دماغ آشنایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نواسنج گلستان محبّت</p></div>
<div class="m2"><p>چو بلبل مست دستان محبت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جان آگه، به تن فرخنده تخمیر</p></div>
<div class="m2"><p>چو بخت خود جوان، چون عقل خود، پیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هر وصفی که گویم، نام او به</p></div>
<div class="m2"><p>چراغ دیده ی ادراک واله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حکایت کرد آن سنجیده گفتار</p></div>
<div class="m2"><p>که درگنجینه بودش درج اسرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز جام عشق بودم مست و مدهوش</p></div>
<div class="m2"><p>که مژگان گشت با خواب آشنا، دوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین دیدم که زیبا منزلی بود</p></div>
<div class="m2"><p>درآن خلوت ز خاصان محفلی بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه صاحب دلان، روشن خیالان</p></div>
<div class="m2"><p>مصفّا خاطران، طوطی مقالان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی زان زمره ی شیرین تکلّم</p></div>
<div class="m2"><p>چو بلبل زد بر آهنگ ترنّم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز گوهر داشت در درج دهن، گنج</p></div>
<div class="m2"><p>در این بحر از سخن شد داستان سنج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو درّی چند، کرد آویزهٔ گوش</p></div>
<div class="m2"><p>به او گفتم که ای میخانهٔ هوش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دل آشفتی به یک پیمانه از من</p></div>
<div class="m2"><p>خرد را ساختی بیگانه از من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نوای کیست این ابیات دلکش</p></div>
<div class="m2"><p>که چون نی، زد به هر بند من آتش؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کدامین بلبل رنگین ترانه است</p></div>
<div class="m2"><p>که دستان سنج این شیرین فسانه ست؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به پاسخ زد به گوشم آن گهرسنج</p></div>
<div class="m2"><p>که ای گنجینه ات را از گهر گنج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نوای کلک جان بخش حزین است</p></div>
<div class="m2"><p>که گنج معنیش در آستین است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دوات از ناف آهوی ختن کرد</p></div>
<div class="m2"><p>چو تحریر از چمن وز انجمن کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به فیضی، زنده شد دل زبن سروشم</p></div>
<div class="m2"><p>که صبح آمد به استقبال هوشم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صباحی چون جبین حور، بیضا</p></div>
<div class="m2"><p>دمش افسرده جانان را مسیحا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گریبان چاک، یوسف در هوایش</p></div>
<div class="m2"><p>نسیم مصر مشتاق لقایش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به کنج بی کسی بودم غزل خوان</p></div>
<div class="m2"><p>چو بلبل، آشیان را برگ و سامان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گهی بلبل صفت در خوش سروشی</p></div>
<div class="m2"><p>گهی چون غنچه لبریز خموشی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که ناگه از در آن یار دل افروز</p></div>
<div class="m2"><p>درآمد با رخی چون صبح نوروز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو غنچه، لب ز شکّر خنده رنگین</p></div>
<div class="m2"><p>به گوشم زد سروش خواب دوشین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رگ اندیشه دیدم، زخمه مایل</p></div>
<div class="m2"><p>نهادم در میان، این راز با دل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اشارت شد لب رنگین سخن را</p></div>
<div class="m2"><p>که آراید چمن را و انجمن را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>محبّت بر رگ جان می زند نیش</p></div>
<div class="m2"><p>نوایی می سرایم با دل خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بیا ساقی هوای برشکال است</p></div>
<div class="m2"><p>سبوی غنچه لبریز زلال است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رخ زیبا چوگل بی پرده بنمای</p></div>
<div class="m2"><p>گره از ابروان، مستانه بگشای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خمارم بشکن از جام صبوحی</p></div>
<div class="m2"><p>مگر پیش آید از مستی فتوحی</p></div></div>