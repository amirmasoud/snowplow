---
title: >-
    بخش ۲ - کف نیاز به دربار بی نیاز به دعا گشودن و گوهر مدعا از نیسان عطا ربودن
---
# بخش ۲ - کف نیاز به دربار بی نیاز به دعا گشودن و گوهر مدعا از نیسان عطا ربودن

<div class="b" id="bn1"><div class="m1"><p>خداوندا درین دیرینه منزل</p></div>
<div class="m2"><p>دری نشناختم غیر از در دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانستم رهی جز راه عشقت</p></div>
<div class="m2"><p>گواه من، دلِ آگاه عشقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برین در، حلقه کردم چشم امّید</p></div>
<div class="m2"><p>ازین در، رخ نخواهم تافت جاوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توبه را از جانب مغرب دری</p></div>
<div class="m2"><p>نه رَه پیدا بود نه راه پیما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا شد روز دیر و دور فرسنگ</p></div>
<div class="m2"><p>گران افتاده بار و بارکش لنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه آید از کف بی دست و پایی؟</p></div>
<div class="m2"><p>ز رَه واماندهٔ سرگشته رایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون دریاب، کار افتاده ای را</p></div>
<div class="m2"><p>زبون مگذار، زار افتادهای را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پا افتاده ای از خاک بردار</p></div>
<div class="m2"><p>دل ازکف دادهای را زار مگذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین رسم است، نخجیرافکنان را</p></div>
<div class="m2"><p>که چون خستند، صید ناتوان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خاکش، چُست برگیرند و چالاک</p></div>
<div class="m2"><p>کنندش زینت آغوش فتراک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین وادی من آن صید زبونم</p></div>
<div class="m2"><p>که تیغت ازترحّم ریخت خونم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تپان در خاک و خونم، مضطرب حال</p></div>
<div class="m2"><p>زبان از شرم ناشایستگی لال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو شمع ازپای تا سر، اشک و آهی</p></div>
<div class="m2"><p>به راه مرحمت، عاجز نگاهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که گردد سایه گستر نخل آمال</p></div>
<div class="m2"><p>گشاید پر، همای اوج اقبال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به این خوش می کنم کام دل خویش</p></div>
<div class="m2"><p>که خواهی برگرفتن، بسمل خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولیکن صبر کم، دل ناشکیباست</p></div>
<div class="m2"><p>درین یک قطره خون آشوب دریاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دلی کز داغ دوری ریش باشد</p></div>
<div class="m2"><p>اگر زاری کند عذریش باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دوری ساختن، کاری ست دشوار</p></div>
<div class="m2"><p>دلی یا رب مباد از هجر افگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو خود برداشتی اول ز خاکم</p></div>
<div class="m2"><p>دمیدی درگریبان، روح پاکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به راز خود امانت دار کردی</p></div>
<div class="m2"><p>دلم را مخزن اسرار کردی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آخر هم، ز خاک تیره برگیر</p></div>
<div class="m2"><p>رَهِ عاجز نوازی ها ز سرگیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نمودی شرط، مسکین پروری را</p></div>
<div class="m2"><p>رسانیدی به شاهی، لشکری را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه نعمت ها کشیدی بی قیاسم</p></div>
<div class="m2"><p>به کام حقِّ نعمت ناشناسم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه گوهرها که از بحر سخایت</p></div>
<div class="m2"><p>فروبارید، نیسان عطایت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تراوش های فیضت را کران نیست</p></div>
<div class="m2"><p>شمار نعمتت حدِّ زبان نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز خواب نیستی بیدار کردی</p></div>
<div class="m2"><p>کرم بی حد، عطا بسیار کردی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دلی دادی چو جام جم، مصفّا</p></div>
<div class="m2"><p>جمال غیب را مجلای اوفا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تنی آراستی زیبا و طنّاز</p></div>
<div class="m2"><p>طلسمی ساختی بر مخزن راز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به خاک انباشتم آیینهٔ خویش</p></div>
<div class="m2"><p>نپالم خون چه سان از سینهٔ خویش؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شکاف افتاده در کاخ تن از رنج</p></div>
<div class="m2"><p>شکستم گر طلسم، انباشتم گنج</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خوش آنکو بشکند زندان تن را</p></div>
<div class="m2"><p>ولی چیند به گلشن انجمن را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من بی طاقت، آن کج نغمه زاغم</p></div>
<div class="m2"><p>که مردود قفس، محروم باغم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تنم از ناتوانی گشته رنجور</p></div>
<div class="m2"><p>بود سرپنجه ام چون بهله، بی زور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز کار افتاده شست ناوک انداز</p></div>
<div class="m2"><p>ز ساعد شاهبازم کرده پرواز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>میسر نیست دیگر صید کامم</p></div>
<div class="m2"><p>نمی گردد شکاری، گرد دامم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چه باشد حال آن سرگشته صیّاد</p></div>
<div class="m2"><p>که عمر از کف دهد در وحشت آباد؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اجل چون گرددش غافل گلوگیر</p></div>
<div class="m2"><p>نفس گردد به کیش سینه اش تیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تهی باشد کفش، از صید مقصود</p></div>
<div class="m2"><p>کمین بیهوده، سعیش جمله نابود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به رنگی اشک سرخ از دیده جاری ست</p></div>
<div class="m2"><p>که رشک افزای گلهای بهاری ست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>غبار خاطرم گردیده انبوه</p></div>
<div class="m2"><p>غمی دارم درون سینه چون کوه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چه فیض از زندگانی می توان دید؟</p></div>
<div class="m2"><p>که نگشاید دری، از صبح امّید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه حاصل ازتماشای رخ حور؟</p></div>
<div class="m2"><p>به چشمی چون چراغ صبح بی نور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چه لذت کام را از شکر و شیر؟</p></div>
<div class="m2"><p>که باشد زهر جانکاهش گلوگیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه آسایش، تن بیمار دارد؟</p></div>
<div class="m2"><p>که پهلو بر گُل بی خار دارد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کجا گیرد قرار آشفته بلبل؟</p></div>
<div class="m2"><p>که دارد درگریبان خرمن گل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه آتش کرده ساقی در ایاغم</p></div>
<div class="m2"><p>که مرهم، گشته زنهاری ز داغم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مزن بر شیشهٔ بیناییم سنگ</p></div>
<div class="m2"><p>که آگاهی ز احوال دل تنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حلاوت بخش، زهر فرقتم را</p></div>
<div class="m2"><p>تسلّی کن دل بی طاقتم را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وصالت می کند دل را تسلی</p></div>
<div class="m2"><p>بود مهر لب موسی، تجلی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به عالم قطره را باشد همین کام</p></div>
<div class="m2"><p>که در آغوش دریا گیرد آرام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زبانم را ازین گستاخ گویی</p></div>
<div class="m2"><p>به عفو خود عطا کن سرخ رویی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه شد، گر نیستم لایق به جودت؟</p></div>
<div class="m2"><p>که مقصود از خریدن نیست، سودت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کرم ها کرده ای بر ناپسندان</p></div>
<div class="m2"><p>نوازشهاستت، با مستمندان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه باک از ناقبولیهای خویشم؟</p></div>
<div class="m2"><p>که هستی بی نیاز از کفر و کیشم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دهانم چون صدف، از بی نوایی</p></div>
<div class="m2"><p>ز نیسان، قطرهای دارد گدایی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به عالم تا در فیض تو باز است</p></div>
<div class="m2"><p>کف امّیدواری ها فراز است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر بگذاریم در قهر جاوید</p></div>
<div class="m2"><p>نمی گردد دلم، یک ذره نومید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به امّیدی،که در جان و دل از توست</p></div>
<div class="m2"><p>به آشوبی که در آب و گل از توست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که بخشایی دلم را فیض سرمد</p></div>
<div class="m2"><p>به سر خیل سرافرازان، محمّد</p></div></div>