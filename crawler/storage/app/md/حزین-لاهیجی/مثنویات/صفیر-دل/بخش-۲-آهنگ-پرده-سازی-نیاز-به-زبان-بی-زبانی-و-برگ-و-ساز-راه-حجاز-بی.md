---
title: >-
    بخش ۲ - آهنگ پرده سازی نیاز به زبان بی زبانی و برگ و ساز راه حجاز بی...
---
# بخش ۲ - آهنگ پرده سازی نیاز به زبان بی زبانی و برگ و ساز راه حجاز بی...

<div class="b" id="bn1"><div class="m1"><p>خدایا دلی ده حقیقت شناس</p></div>
<div class="m2"><p>زبانی سزاوار حمد و سپاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا جز تو کس، یاور و یار نیست</p></div>
<div class="m2"><p>چه گویم که یارای گفتار نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فیض تو آید دلم در خروش</p></div>
<div class="m2"><p>که نی از دم نایی آید به جوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم رشحه ی بحر انعام تست</p></div>
<div class="m2"><p>چو ماهی، زبان زنده از نام توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد فروغی ز خود مشت گل</p></div>
<div class="m2"><p>مگر پرتو فیضت افتد به دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وجود تو نگشاید ار دست جود</p></div>
<div class="m2"><p>عدم پیکران را چه یارای بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهی خامه ی صنع را سروری</p></div>
<div class="m2"><p>به معنی طرازی و صورتگری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن چهره پرداز چین اوا چگل</p></div>
<div class="m2"><p>گل از گِل دمد، داغ عشقت ز دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبخشی اگر گمرهان را سراغ</p></div>
<div class="m2"><p>نیفروزد از داغ عشقت چراغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین تیره کاخی که ظلمت سراست</p></div>
<div class="m2"><p>نفس راه لب را چه داند کجاست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازل تا ابد، مد احسان توست</p></div>
<div class="m2"><p>به خوان کرم، دل نمکدان توست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می عشق روشنگر سینه شد</p></div>
<div class="m2"><p>به خمخانه ات، چشم آیینه شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توکردی زبان مرا یاوری</p></div>
<div class="m2"><p>که زد از سخن کوس اسکندری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به معنی، شدی رهبر خامه ام</p></div>
<div class="m2"><p>زدی غازه بر چهرهٔ نامه ام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کند از تو در دامن روزگار</p></div>
<div class="m2"><p>رگ ابر کلکم دُرِ شاهوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زهی لوح فکر و خوشا کام من</p></div>
<div class="m2"><p>سجلِّ قبول تو دارد سخن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من زار، مرد ثنایت کیم؟</p></div>
<div class="m2"><p>نواپرور خویش کردی نیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دمد از رگم نغمهٔ چنگ و رود</p></div>
<div class="m2"><p>صفیرم زند ارغنونی سرود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به دستان زنم راه دور غمت</p></div>
<div class="m2"><p>به داوود خوانم، زبور غمت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبان است دستان زن باغ تو</p></div>
<div class="m2"><p>دلم طور و شمعش بود داغ تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حدیث من و ما نمی شایدم</p></div>
<div class="m2"><p>به این خیرگی خنده می آیدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ندانسته ام کیستم، چیستم</p></div>
<div class="m2"><p>تویی عین هستیّ و من نیستم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فنا را کجا لاف دعوی رسد؟</p></div>
<div class="m2"><p>مگر دست دعوی به معنی رسد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حزین، از می بیخودی جام کش</p></div>
<div class="m2"><p>زبان مست دعوی ست، در کام کش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر محو کثرت و گر وحدتی</p></div>
<div class="m2"><p>به هر صورت، آیینهٔ حیرتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قلم بر فسونهای نیرنگ زن</p></div>
<div class="m2"><p>زند راهت، آیینه بر سنگ زن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو از خویش و بیگانه تنها شوی</p></div>
<div class="m2"><p>قبول خداوند یکتا شوی</p></div></div>