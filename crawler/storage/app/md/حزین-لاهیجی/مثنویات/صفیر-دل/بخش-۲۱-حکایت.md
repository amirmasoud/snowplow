---
title: >-
    بخش ۲۱ - حکایت
---
# بخش ۲۱ - حکایت

<div class="b" id="bn1"><div class="m1"><p>فرود آمد از تخت شاهی قباد</p></div>
<div class="m2"><p>که عمر است کاه و اجل تندباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاراست پیرایه بخش جهان</p></div>
<div class="m2"><p>سریر کیانی به نوشیروان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوان بود شهزادهٔ شیرگیر</p></div>
<div class="m2"><p>به بازو تهمتن، به همّت دلیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نیرنگ ایام نادیده رنج</p></div>
<div class="m2"><p>سپه بیکران بود و آماده گنج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک رام بود و جهانش به کام</p></div>
<div class="m2"><p>زمین زیر فرمان، زمانش غلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو پیکر خط بندگی داده بود</p></div>
<div class="m2"><p>به خدمت کمر بسته استاده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دولت جهاندار باهوش و رای</p></div>
<div class="m2"><p>خدا بنده بود و خرد آزمای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبودی سرش پای بند غرور</p></div>
<div class="m2"><p>سلیمان، گران سر نباشد به مور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بنشست بر تخت فرماندهی</p></div>
<div class="m2"><p>ره عدل بگزید و رسم مهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز عدل قویدست کشورگشای</p></div>
<div class="m2"><p>کشید از میان جور، یکباره پای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همایون فرخنده بگشود بال</p></div>
<div class="m2"><p>بیاراست ملک و ببخشید مال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شدی تلخ گر عیش یک تن ز خلق</p></div>
<div class="m2"><p>گره می شدش آب شیرین به حلق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی گفتش ای خسرو دادگر</p></div>
<div class="m2"><p>به عدل این چنین کس نبسته کمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به رنج اندری در رفاه عباد</p></div>
<div class="m2"><p>تو را شهریاری که تعلیم داد؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهاندار، گفتش به عهد صغر</p></div>
<div class="m2"><p>که بودم به نخجیرگه با پدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سنگی سگی را یکی پا شکست</p></div>
<div class="m2"><p>به چُستی قضا نیز بگشاد دست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شکست از لگد پای آن سنگ زن</p></div>
<div class="m2"><p>یکی باره، با سُمّ خارا شکن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به تقدیر فرمانده دادگر</p></div>
<div class="m2"><p>چه دیدم پس از چند گام دگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که شد در زمین پای یکران نهان</p></div>
<div class="m2"><p>نیامد برون، تا شکست استخوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دیدم به اندک زمان این سه چیز</p></div>
<div class="m2"><p>مهیا مکافات را با ستیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا باز شد دیدهٔ اعتبار</p></div>
<div class="m2"><p>عجب ماندم ازگردش روزگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مروّت کشید آستین دلم</p></div>
<div class="m2"><p>شد انصاف نقش نگین دلم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برآنم که تا عمر بخشد خدای</p></div>
<div class="m2"><p>برون ننهم از جادهٔ عدل پای</p></div></div>