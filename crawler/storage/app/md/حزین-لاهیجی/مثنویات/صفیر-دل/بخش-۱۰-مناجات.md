---
title: >-
    بخش ۱۰ - مناجات
---
# بخش ۱۰ - مناجات

<div class="b" id="bn1"><div class="m1"><p>خدایا به جاه خداوندیت</p></div>
<div class="m2"><p>که بخشی مقام رضامندیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طمع نیست از کشت بی حاصلم</p></div>
<div class="m2"><p>به خشنودیت کار دارد دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی شرمسارم ز نفس فضول</p></div>
<div class="m2"><p>ز طاعت مکدّر، ز عصیان ملول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که نیک و بدم هر دو نبود روا</p></div>
<div class="m2"><p>چو عصیان بود طاعتم ناسزا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارم بجز عفو چیزی به کف</p></div>
<div class="m2"><p>شد از کف مرا نقد فرصت تلف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبخشید سودی جگرخوارگی</p></div>
<div class="m2"><p>من و دست و دامان بیچارگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به درگاهت آورده ام عجز خویش</p></div>
<div class="m2"><p>سر از شرم بی برگی افکنده پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگیری چسان دست افتاده ای</p></div>
<div class="m2"><p>که خود از کرم هستیش داده ای؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یک عمر در نعمتت زیستم</p></div>
<div class="m2"><p>گدای درت نیستم، کیستم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر هست بنما دَرِ دیگرم</p></div>
<div class="m2"><p>وگرنه به حرمان مران زین درم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در افتادگی از که خواهم مدد؟</p></div>
<div class="m2"><p>مدد از کِه افتادگان را رسد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خروشان خراشم جگر در قفس</p></div>
<div class="m2"><p>کسی نیست غیر از تو فریادرس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز چاک قفس ارمغان بهار</p></div>
<div class="m2"><p>فرستم صفیر دل سوگوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکیب از دلم رفته نیرو ز چنگ</p></div>
<div class="m2"><p>برم مانده چون سبزه در زیر سنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نماندهست امیدم به چیزی مگر</p></div>
<div class="m2"><p>به چاک گریبان و دامان تر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که عصیان به کوی کریمان برند</p></div>
<div class="m2"><p>گنه هدیه آرند و غفران برند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هر حاجتم از تو امّیدوار</p></div>
<div class="m2"><p>که هم فیض بخشی هم آمرزگار</p></div></div>