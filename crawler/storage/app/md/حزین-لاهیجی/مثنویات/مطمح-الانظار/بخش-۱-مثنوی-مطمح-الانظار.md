---
title: >-
    بخش ۱ - مثنوی مطمح الانظار
---
# بخش ۱ - مثنوی مطمح الانظار

<div class="b" id="bn1"><div class="m1"><p>ای دل افسرده خروشت کجاست؟</p></div>
<div class="m2"><p>خامشی از زمزمه، جوشت کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک سخن زیر لوای تو بود</p></div>
<div class="m2"><p>رامش دلها ز نوای تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طنطنهٔ پرده گشاییت کو؟</p></div>
<div class="m2"><p>دبدبهٔ نغمه سراییت کو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمزمهٔ سینه خراشت چه شد؟</p></div>
<div class="m2"><p>ناله ی الماس تراشت چه شد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طرز نوایت زده از تازگی</p></div>
<div class="m2"><p>مقرعه، بر کوس خوش آوازگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر نگین، ملک سخن داشتی</p></div>
<div class="m2"><p>معجز هاروت شکن داشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شور قیامت ز نیت می دمید</p></div>
<div class="m2"><p>فیض طرب در چمنت می چمید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود تو را خامهٔ مشکین رقم</p></div>
<div class="m2"><p>ملک گشاتر، ز کیانی علم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رعشه قلم را ز بنانت فکند</p></div>
<div class="m2"><p>صرصر دی سرو جوانت فکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش غم نالهٔ جانکاه سوخت</p></div>
<div class="m2"><p>در نفس آباد گلو، آه سوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آتش پنهان تو را دود نیست</p></div>
<div class="m2"><p>لعل لبت خون دل آلود نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شعله برافروزی داغت نماند</p></div>
<div class="m2"><p>پیهِ دماغی به چراغت نماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آوخ ازین کلفت و افسردگی</p></div>
<div class="m2"><p>با همه آتش نفسی، مردگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محرم دل کو؟ که سرایم غمی</p></div>
<div class="m2"><p>همنفسی کو؟ که برآرم دمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک نشین است حزین، اخگرت</p></div>
<div class="m2"><p>خاک نهاده ست به بالین سرت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرکز خاکی نپذیرد ثبات</p></div>
<div class="m2"><p>خیز ازبن رهگذر حادثات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صاف سلوکش همه آلایش است</p></div>
<div class="m2"><p>رفتن ازین مرحله آسایش است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون تو همایی، پر همّت برآر</p></div>
<div class="m2"><p>این ده ویرانه به جغدان گذار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هان نشوی از هوس دیده تنگ</p></div>
<div class="m2"><p>شیفته لیل و نهار دو رنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز ابرص روز و شب این کهنه دهر</p></div>
<div class="m2"><p>غیر دو رنگی نتوان یافت بهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دیدهٔ پهناور بینش فروز</p></div>
<div class="m2"><p>باز کن و دیدهٔ حیلت بسوز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پردهٔ شب باز به پیش چراغ</p></div>
<div class="m2"><p>شعبده انگیز بود در دماغ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باصره کالیوه، کند، هوش دنگ</p></div>
<div class="m2"><p>لعبت این پرده بود ریو و رنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لولی دنیا چه وفایی کند؟</p></div>
<div class="m2"><p>گردش گردون چه بقایی کند؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عهد سبکسر نکشیده ست دیر</p></div>
<div class="m2"><p>مهر فلک سست و جهان زود سیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از رَهِ سیلاب، خطر داشتن</p></div>
<div class="m2"><p>ناگزران است گذر داشتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ره سپر عمر ز پنجه گذشت</p></div>
<div class="m2"><p>خاتمه بر دفتر هستی نوشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیر شیب تو دمید از شباب</p></div>
<div class="m2"><p>صبح برافکند ز عارض نقاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سبزه خزان گشت و سمن زار رست</p></div>
<div class="m2"><p>موی چو مشک تو به کافور شُست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شمع فروزندهٔ سیّاره نیست</p></div>
<div class="m2"><p>هوش به سر، نور به نظّاره نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گوهر ارزنده ات از تاج رفت</p></div>
<div class="m2"><p>خیز که سرمایه به تاراج رفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جلوهٔ تو شمع سحرگاهی است</p></div>
<div class="m2"><p>قافله سالار نفس راهی است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در دلت آن شعله که افروخت درد</p></div>
<div class="m2"><p>جسم گدازان تو را جمله خورد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شمع صفت، تیرگیت نور شد</p></div>
<div class="m2"><p>بوتهٔ خارت شجر طور شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پرده به دستان دگر ساز کن</p></div>
<div class="m2"><p>خطبهٔ دیوان نو آغاز کن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تازه نما، بار بدی پرده را</p></div>
<div class="m2"><p>شهد چشان، کام جگر خورده را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خیمه به رامشگهِ تجرید زن</p></div>
<div class="m2"><p>وجدکنان نغمهٔ توحید زن</p></div></div>