---
title: >-
    بخش ۲ - فی التوحید
---
# بخش ۲ - فی التوحید

<div class="b" id="bn1"><div class="m1"><p>ای رقمت سلسله بند وجود</p></div>
<div class="m2"><p>در خط فرمان تو اقلیم جود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راتبه خوار قلمت، مغز جان</p></div>
<div class="m2"><p>مغزپذیر کرمت استخوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقطه ای از خامهٔ تو کاینات</p></div>
<div class="m2"><p>رشحه ای از چشمهٔ فیضت حیات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده گشای نفس راستان</p></div>
<div class="m2"><p>مرسله بندِ گهرِ داستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نغمه طراز چمن جان و دل</p></div>
<div class="m2"><p>جرعه دِهِ انجمن آب و گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مصطبه آرای صبوحی کشان</p></div>
<div class="m2"><p>مشغله افزای غم مهوشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غازه کش چهرهٔ تابنده حور</p></div>
<div class="m2"><p>مایه دِهِ چشمهٔ پاینده نور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غالیه سای قلم مشک ریز</p></div>
<div class="m2"><p>نافه گشای نفس مشک بیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روشنی چشم بلند اختران</p></div>
<div class="m2"><p>شاهد دلهای نکومحضران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرمه کش چشم جهان بین عقل</p></div>
<div class="m2"><p>عاشقی آموز دل و دین عقل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بارقه افروز چراغ یقین</p></div>
<div class="m2"><p>برق به خرمن فکن کفر و دین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لعل طراز خزف جزء و کل</p></div>
<div class="m2"><p>از شرف گوهر ختم رسل</p></div></div>