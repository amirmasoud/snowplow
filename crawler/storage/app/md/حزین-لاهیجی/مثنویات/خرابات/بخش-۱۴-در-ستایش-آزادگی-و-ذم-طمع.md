---
title: >-
    بخش ۱۴ - در ستایش آزادگی و ذمّ طمع
---
# بخش ۱۴ - در ستایش آزادگی و ذمّ طمع

<div class="b" id="bn1"><div class="m1"><p>یکی مرد دانا دل هوشیار</p></div>
<div class="m2"><p>دو کودک به هم دید در رهگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی با عسل نانش آمیخته</p></div>
<div class="m2"><p>دگررا به نان دوغکی ربخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کودک مزاجیش کو دوغ داشت</p></div>
<div class="m2"><p>به سوی عسل دست خواهش فراشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خداوند شهدش همی گفت باز</p></div>
<div class="m2"><p>که ای بر کفم دوخته چشم آز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگ من شو اکنون که داری طمع</p></div>
<div class="m2"><p>وگر نه میالا لباس ورع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد آخر سگش کودک طبع کیش</p></div>
<div class="m2"><p>ندیده عسل جوی بی زهر نیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر سو کشیدش به صد درد جفت</p></div>
<div class="m2"><p>شنیدم که داننده با خویش گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابا دوغ، کودک اگر ساختی</p></div>
<div class="m2"><p>کجا سگ کشان از پیش تاختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فراغ دو عالم طمع کیش باخت</p></div>
<div class="m2"><p>خنک آن که با قسمت خویش ساخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین پرده ویرانی آبادی است</p></div>
<div class="m2"><p>رجا بندگی یاس آزادی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مده دامن یاس آسان ز دست</p></div>
<div class="m2"><p>بنازم دلی کز تمنا برست</p></div></div>