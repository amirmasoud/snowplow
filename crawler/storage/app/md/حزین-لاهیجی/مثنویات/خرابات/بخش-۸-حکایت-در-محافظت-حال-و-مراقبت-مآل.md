---
title: >-
    بخش ۸ - حکایت در محافظت حال و مراقبت مآل
---
# بخش ۸ - حکایت در محافظت حال و مراقبت مآل

<div class="b" id="bn1"><div class="m1"><p>یکی بار دل در گل افتاده ای</p></div>
<div class="m2"><p>سخن راند در خبث آزاده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن چین، حدیثش، به آزاده گفت</p></div>
<div class="m2"><p>نگر تا چه سان گوهر راز سفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بگذار بیهوده گفتار را</p></div>
<div class="m2"><p>به کج نغمه، مگشای منقار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا هست در پیش، راهی شگرف</p></div>
<div class="m2"><p>به صد حیرتم غرق و دریاست ژرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ساحل اگر بخت شد رهنمون</p></div>
<div class="m2"><p>و زین لجه، رخت من آمد برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارم ز بد گفتنش هیچ باک</p></div>
<div class="m2"><p>کجا گیرد آلودگی، جان پاک؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر برنیاید سبویم درست</p></div>
<div class="m2"><p>شود رشته ها پنبه و کار سست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آنم نکوتر نگوید کسی</p></div>
<div class="m2"><p>سزاوار ناخوشترم زان بسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حزین، سیرت رهروان یادگیر</p></div>
<div class="m2"><p>سراسر حدیث جهان بادگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را با خود افتاده، آموزگار</p></div>
<div class="m2"><p>به نیک و بد کس مبر روزگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حریفان دغلباز و ره پیچ پیچ</p></div>
<div class="m2"><p>مبادا که فرصت ببازی به هیچ</p></div></div>