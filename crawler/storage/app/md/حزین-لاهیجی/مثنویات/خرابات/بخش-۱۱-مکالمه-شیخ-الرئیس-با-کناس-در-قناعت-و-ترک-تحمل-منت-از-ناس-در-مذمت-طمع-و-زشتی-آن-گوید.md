---
title: >-
    بخش ۱۱ - مکالمهٔ شیخ الرئیس با کنّاس در قناعت و ترک تحمّل منّت از ناس در مذمّت طمع و زشتی آن گوید
---
# بخش ۱۱ - مکالمهٔ شیخ الرئیس با کنّاس در قناعت و ترک تحمّل منّت از ناس در مذمّت طمع و زشتی آن گوید

<div class="b" id="bn1"><div class="m1"><p>شبی سر برآوردم از جیب خویش</p></div>
<div class="m2"><p>چو آهی که خیزد ز دلهای ریش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگارندهٔ قصّهٔ باستان</p></div>
<div class="m2"><p>رقم کرده بر دفتر راستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که از پور سینا شنیدم که گفت</p></div>
<div class="m2"><p>در ایّام خود، آشکار و نهفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگردیده ام مُلزم از هیچ کس</p></div>
<div class="m2"><p>مگر از یکی گبرِ کنّاس و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که پویان به راهی شدم بامداد</p></div>
<div class="m2"><p>گذر بر یکی از مزابل فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شغل خود، آن گبر مشغول بود</p></div>
<div class="m2"><p>تفاخر کنان، نغمه ای می سرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفاد سخنش اینکه ای نفس از آن</p></div>
<div class="m2"><p>به عزّت تو را داشتم در جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که شایان حرمت تو را یافتم</p></div>
<div class="m2"><p>به بَر حلّهٔ عزتّت بافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شگفت آمد از وی، مرا این کلام</p></div>
<div class="m2"><p>بدو گفتم ای یاوه گفتار خام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانسته ای چون ز گوهر خزف</p></div>
<div class="m2"><p>سزد گر بلافی به عزّ و شرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگه کرد بر روی من خیرخیر</p></div>
<div class="m2"><p>بگفتا که ابله تویی، نه فقیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تقاضای روزی ز شغلِ خسیس</p></div>
<div class="m2"><p>بسی بهتر از امتنان رئیس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندانسته ای عزّت خود ز ذلّ</p></div>
<div class="m2"><p>سفیهانه بر ما چه خندی چو گُل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرو ماندم از راندنِ پاسخش</p></div>
<div class="m2"><p>بدزدید شرمم، نگاه از رخش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان مهر بر لب مرا زد سکوت</p></div>
<div class="m2"><p>که دل گفت: یا لیتَ انّی اَمُوت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طمع جلوه گر شد مرا در نظر</p></div>
<div class="m2"><p>ز هر زشت رو پیکری، زشت تر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفتم ای راندهٔ بخردان</p></div>
<div class="m2"><p>پدر کیستت؟ بازگو، در جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتا که شک در قضا و قدر</p></div>
<div class="m2"><p>نظر بستن از خالق نفع و ضر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتم که از پیشهٔ خود بگو</p></div>
<div class="m2"><p>چه بافی درین کارگاه دو رو؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه صنعتگری داری از جزء و کل؟</p></div>
<div class="m2"><p>بگفتا: زبونی و خواری و ذُل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدو گفتم از حاصل خود خبر</p></div>
<div class="m2"><p>بگو شمّه ای باز، ای خیره سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مآلت کدام است و غایت کدام؟</p></div>
<div class="m2"><p>بگفتا که حرمان بود والسّلام</p></div></div>