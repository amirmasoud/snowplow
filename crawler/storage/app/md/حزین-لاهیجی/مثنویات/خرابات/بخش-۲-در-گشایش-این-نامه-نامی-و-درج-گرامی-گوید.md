---
title: >-
    بخش ۲ - در گشایش این نامهٔ نامی و درج گرامی گوید
---
# بخش ۲ - در گشایش این نامهٔ نامی و درج گرامی گوید

<div class="b" id="bn1"><div class="m1"><p>مغنّی نوایی بیا ساز کن</p></div>
<div class="m2"><p>جهان را پر از گوهر راز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان تازه کن داغ دیرینهام</p></div>
<div class="m2"><p>که دوزخ برد آتش از سینه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی استخوانم، دم صور کن</p></div>
<div class="m2"><p>چو منقار بلبل پر از شور کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بخشم قلم را پرآوازگی</p></div>
<div class="m2"><p>نهال سخن را دهم تازگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشم پردهای معنی بکر را</p></div>
<div class="m2"><p>دهم جلوه ای، شاهد فکر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه از دیده گویم بَرِ راستان</p></div>
<div class="m2"><p>گهی از شنیده کنم داستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن را به سر تاج شاهی نهم</p></div>
<div class="m2"><p>زلال خضر در سیاهی نهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بده ساقی آن جام یاقوت رنگ</p></div>
<div class="m2"><p>که چون گل درم خرقهٔ نام و ننگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آتش نهم دلق پندار را</p></div>
<div class="m2"><p>بر آرم سر از پیرهن یار را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا تا نمانده ست در زیر گل</p></div>
<div class="m2"><p>بر آریم دستی به اقبال دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به راه وفا جانفشانی کنیم</p></div>
<div class="m2"><p>به ملک بقا کامرانی کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرآریم در خطّ فرمان عشق</p></div>
<div class="m2"><p>بریزیم خون را به میدان عشق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر نافه بگشا حزین، دیر شد</p></div>
<div class="m2"><p>تامّل دگر چیست؟ خون شیر شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیا بازکن دفتر راز را</p></div>
<div class="m2"><p>بگو خامهٔ نکته پرداز را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که آهوی چین عزم جولان کند</p></div>
<div class="m2"><p>بسیط زمین عنبر افشان کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخن راندن نغز، کار من است</p></div>
<div class="m2"><p>سخن در جهان یادگار من است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فروغی که کردم ز دل اقتباس</p></div>
<div class="m2"><p>سپردم به انصاف گوهرشناس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود از دم پاک اهل حضور</p></div>
<div class="m2"><p>زکید حسودان ناپاک دور</p></div></div>