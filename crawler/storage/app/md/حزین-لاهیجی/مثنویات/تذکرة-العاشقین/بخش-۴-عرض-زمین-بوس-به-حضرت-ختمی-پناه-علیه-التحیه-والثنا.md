---
title: >-
    بخش ۴ - عرض زمین بوس به حضرت ختمی پناه علیه التّحیّه والثّناء
---
# بخش ۴ - عرض زمین بوس به حضرت ختمی پناه علیه التّحیّه والثّناء

<div class="b" id="bn1"><div class="m1"><p>ای زادهٔ اوّلینِ قدرت</p></div>
<div class="m2"><p>قدر تو ورای فهم و فکرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدم ز تو یافت سربلندی</p></div>
<div class="m2"><p>نوح از تو، طراز ارجمندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معمار حرمسرا، خلیلت</p></div>
<div class="m2"><p>جان و دل قدسیان سبیلت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در طور، کلیم یک شبانت</p></div>
<div class="m2"><p>کونین، نواله خوار خوانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیسی به بشارت تو دم زد</p></div>
<div class="m2"><p>زان دم، به عطای جان رقم زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاتم تویی وتویی سلیمان</p></div>
<div class="m2"><p>جبریل تو راست، هدهد از جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی در خور توست، عرش بلقیس</p></div>
<div class="m2"><p>اول قدمت به عرش تقدیس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرمانده ی وحش و طیر بودن</p></div>
<div class="m2"><p>رخسار ددان به خاک سودن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سهل است، ولی به عرشِ رفعت</p></div>
<div class="m2"><p>نتوان چو تو یافت، اوج عزت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای صدرنشین بزم لولاک</p></div>
<div class="m2"><p>در خاک مذلت تو افلاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خرگه زده ای به بی نشانی</p></div>
<div class="m2"><p>بیرون ز مکان لامکانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرم است ز بس به حق شتابت</p></div>
<div class="m2"><p>ماندند ملایک از رکابت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نُه خنگ سپهر لاجوردی</p></div>
<div class="m2"><p>از شوق تو گرم رهنوردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دایرهٔ سپهر مینا</p></div>
<div class="m2"><p>باشد مَهِ نو، رکاب آسا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا آنکه ز لطف فیض گستر</p></div>
<div class="m2"><p>پای تو مگر درآورد سر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرنه ز رخ تو نور می تافت</p></div>
<div class="m2"><p>کی مشعل مهر، نور می یافت؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طوبی بود از قد تو سایه</p></div>
<div class="m2"><p>سدره، ز درت، نخست پایه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عزّت ز تو، زمرهٔ ملک را</p></div>
<div class="m2"><p>رفعت ز تو، منبرفلک را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای شمع طراز هفت قندیل</p></div>
<div class="m2"><p>پروانگی تو کرده جبریل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پاس تو دریده کوس ناهید</p></div>
<div class="m2"><p>چتر تو فراز فرق خورشید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نقش قدم تو، تاج عرش است</p></div>
<div class="m2"><p>بر خاک رهِ تو، عرش فرش است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مسجود تویی و قبله آدم</p></div>
<div class="m2"><p>در پیش تو، پشت راستان خم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مملوک صفت، سپهر اخضر</p></div>
<div class="m2"><p>بسته ست حمایل از دو پیکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بو که شود دخیل خیلت</p></div>
<div class="m2"><p>بیند یک ره، به خویش میلت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شد قصر نبوّتت چو بنیاد</p></div>
<div class="m2"><p>کسر، از تو به قصر کسری افتاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون بود به زیر سایه ات مهر</p></div>
<div class="m2"><p>ننمود به خلق، سایه ات چهر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سرگشتگی فلک خوش از تو</p></div>
<div class="m2"><p>نعل مه نو در آتش از تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در دست تو سنگ، سبحه خوانی</p></div>
<div class="m2"><p>با لعل تو نخل، نکته دانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای یثربیِ حجاز مطلع</p></div>
<div class="m2"><p>وز حلّهٔ کبریات، برقع</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زیبندهٔ قرب قاب قوسین</p></div>
<div class="m2"><p>خاک رهت آبروی کونین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>املاک، رهین بحر جودت</p></div>
<div class="m2"><p>افلاک، طفیلی وجودت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کی نعت تو حدّ خاکیان است؟</p></div>
<div class="m2"><p>زیب دم پاک قدسیان است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ما جسم دنی، تو جان پاکی</p></div>
<div class="m2"><p>ما در سمک و تو بر سماکی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حرفی نتوان زدن سزایت</p></div>
<div class="m2"><p>ای جان مقدسان فدایت</p></div></div>