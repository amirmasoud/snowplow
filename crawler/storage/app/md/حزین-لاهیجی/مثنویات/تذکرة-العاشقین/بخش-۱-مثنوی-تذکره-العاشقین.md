---
title: >-
    بخش ۱ - مثنوی تذکرهٔ العاشقین
---
# بخش ۱ - مثنوی تذکرهٔ العاشقین

<div class="b" id="bn1"><div class="m1"><p>ساقی ز می موحّدانه</p></div>
<div class="m2"><p>ظلمت بَرِ شرک، از میانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تیره دلان چو لمعهٔ نور</p></div>
<div class="m2"><p>در نیم ‎شبان تجلّی طور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ده که ز خود کرانه گیریم</p></div>
<div class="m2"><p>بیخود، رَهِ آن یگانه گیریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطرب، دم دلکشی به نی کن</p></div>
<div class="m2"><p>این تیره شب فراق طی کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از صبح وصال، پرده برگیر</p></div>
<div class="m2"><p>شام غم هجر، در سحر گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا باز رهم ازین جدایی</p></div>
<div class="m2"><p>گیرم سرکوی آشنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی، قدحی می مغانه</p></div>
<div class="m2"><p>سرجوش خم شرابخانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کام حزین تشنه لب کن</p></div>
<div class="m2"><p>نذر دل آتشین نسب کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا رخت کشم به عالم آب</p></div>
<div class="m2"><p>آسوده شوم ازین تب و تاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مطرب، نفست جلای جانهاست</p></div>
<div class="m2"><p>با مرده دلان دمت مسیحاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تنگم چو خون مرده، در پوست</p></div>
<div class="m2"><p>نشتر به رگ فسرده نیکوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل مرده، تن فسرده گور است</p></div>
<div class="m2"><p>آواز نی تو، بانگ صور است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ساقی، قدحی که ناصبورم</p></div>
<div class="m2"><p>صد مرحله از شکیب دورم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق است و هزار سوگواری</p></div>
<div class="m2"><p>یک جان و هزار بی قراری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا رام شود دل رمیده</p></div>
<div class="m2"><p>با یار نشیند آرمیده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای مطرب عاشقان، نوایی</p></div>
<div class="m2"><p>آرام رمیده را، صلایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کز فیض دمت سرور یابیم</p></div>
<div class="m2"><p>ما تفرقگان، حضور یابیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در رقص آییم، کف فشانان</p></div>
<div class="m2"><p>بر نطع سپهر، پای کوبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ساقی سر ماست، خاک نعلین</p></div>
<div class="m2"><p>بردار غبار هستی از بین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا آینه ام صفا پذیرد</p></div>
<div class="m2"><p>عکس رخ دلربا پذیرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گردید چو جلوه گاه دلدار</p></div>
<div class="m2"><p>آیینه گذار و عکس مگذار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای مطرب جان، رَهِ دگر گیر</p></div>
<div class="m2"><p>یک ره ز ترانه پرده برگیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دستان زنِ دل، شکسته بال است</p></div>
<div class="m2"><p>مشتاق به ناله های حال است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کز ذوق سماع، پر برآرد</p></div>
<div class="m2"><p>این کهنه قفس بجا گذارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ساقی بده آن می مروّق</p></div>
<div class="m2"><p>تا جان، کُند از قیود، مطلق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از خود بفشاند آب و گل را</p></div>
<div class="m2"><p>بیند رخ آن بت چگل را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گردد ز شراب وصل مدهوش</p></div>
<div class="m2"><p>از هر چه جز او، کند فراموش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مطرب، دل ما اسیر رنج است</p></div>
<div class="m2"><p>مرغ سحری ترانه سنج است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بنشین و تو هم ترانه سر کن</p></div>
<div class="m2"><p>افسانهٔ عاشقانه سر کن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا راه دیار یار گیریم</p></div>
<div class="m2"><p>از هر دو جهان کنار گیریم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ساقی می عاشقانه پیش آر</p></div>
<div class="m2"><p>جان داروی جاودانه پیش آر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عشق است و هزار نامرادی</p></div>
<div class="m2"><p>کالای وفاست در کسادی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا نغمهٔ خوشدلی سراییم</p></div>
<div class="m2"><p>یکدم با یار، خوش برآییم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مطرب، نی خوش نوا به دم گیر</p></div>
<div class="m2"><p>گو آتشم از درون عَلم گیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ازکف شده نقد عمر بیرون</p></div>
<div class="m2"><p>آهنگ حُدی، بزن به قانون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باشد کم عمر رفته گیرم</p></div>
<div class="m2"><p>تاوانش، ازین دو هفته گیرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ساقی، بده آن می دلارا</p></div>
<div class="m2"><p>کش طور خم است، رشک سینا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا ساعتی از خودی رهاند</p></div>
<div class="m2"><p>یکدم ما را ز ما ستاند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جان، مست لقای دوست گردد</p></div>
<div class="m2"><p>باقی به بقای دوست گردد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای مطرب عاشقان، سرودی</p></div>
<div class="m2"><p>شاهنشه عشق را درودی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یاران قدیم را سلامی</p></div>
<div class="m2"><p>مستان وصال را پیامی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کاین سوختهٔ تف جدایی</p></div>
<div class="m2"><p>دارد نظر از شما، گدایی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ساقی، به چراغ مسجد و دیر</p></div>
<div class="m2"><p>روشن نشود مرا رَهِ سیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>صعب است رَهِ خطیر هستی</p></div>
<div class="m2"><p>گردد سپری، مگر به مستی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>برق قدحی به راه من گیر</p></div>
<div class="m2"><p>در شعله، شب سیاه من گیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مطرب، چه فسرده ای، سرودی</p></div>
<div class="m2"><p>برکُن ز خَسَم به شعله، دودی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سدکن ره ناله ای، خدا را</p></div>
<div class="m2"><p>بی پرده کن، آتشین نوا را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کز گریه غبار دل نشانیم</p></div>
<div class="m2"><p>بر چرخ سرآستین فشانیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ساقی، می آفتاب وش کو؟</p></div>
<div class="m2"><p>بر جبههٔ شعله، داغ کش کو؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تاریک شبم فرو گرفته</p></div>
<div class="m2"><p>مار سیهم،گلو گرفته</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شمع ره کفر و دین برافروز</p></div>
<div class="m2"><p>صبح شفقی جبین، برافروز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مطرب، نفسی برشته داری</p></div>
<div class="m2"><p>دُردانه بسی به رشته داری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در جیب و کنار گوش ما کن</p></div>
<div class="m2"><p>تاراج متاع هوش ما کن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مشکین نفسیّ و آتشین لعل</p></div>
<div class="m2"><p>افکنده لبت در آتشم نعل</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مطرب، دم جان فزات نازم</p></div>
<div class="m2"><p>مستانه ترانه هات نازم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مگذار به حال خویش ما را</p></div>
<div class="m2"><p>سر کن رَهِ دلکشی، خدا را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا روز خیال رخ نماید</p></div>
<div class="m2"><p>بختم به فلک رکاب ساید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رخش تک و پوی را کنم پی</p></div>
<div class="m2"><p>آسوده کنم مقام در حی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ساقی، سر همّت تو گردم</p></div>
<div class="m2"><p>پروانهٔ طلعت تو گردم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شیدی دو سه، صوفیانه بردار</p></div>
<div class="m2"><p>این ما و من از میانه بردار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شمع رخت انجمن فروز است</p></div>
<div class="m2"><p>پروانهٔ زهد و عقل سوز است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دیرینه گدای می پرستم</p></div>
<div class="m2"><p>از ساغر می تهیست دستم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مطرب، نفسی به کار نی کن</p></div>
<div class="m2"><p>جانی به تن نزار نی کن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دی ماه جهان، بهارم افسرد</p></div>
<div class="m2"><p>دم سردی روزگارم افسرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بنواز به بانگ آشنایی</p></div>
<div class="m2"><p>در زن به دل، آتشین نوایی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ساقی، به صفای می پرستان</p></div>
<div class="m2"><p>کز شرم برآ، به بزم مستان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>می کن به قدح جبین گشاده</p></div>
<div class="m2"><p>چون گل، کف نازنین گشاده</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ما تشنه لب زلال فیضیم</p></div>
<div class="m2"><p>دریوزه گر نوال فیضیم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ای مطرب عاشقان، خروشی</p></div>
<div class="m2"><p>ای هاتف قدسیان، سروشی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خون در تن من فتاده از جوش</p></div>
<div class="m2"><p>بردار ز راز عشق، سرپوش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بخراش به ناخنی رگِ چنگ</p></div>
<div class="m2"><p>بگشا نَمِ خونم از دل تنگ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ساقی، گل و جوش نوبهار است</p></div>
<div class="m2"><p>چون چرخ، زمین شفق نگار است</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از صوت هزار، در چمنها</p></div>
<div class="m2"><p>نسرین زده چاک، پیرهنها</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مپسند مرا به دلق سالوس</p></div>
<div class="m2"><p>مگذار به قید نام و ناموس</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مطرب، ز خموشیت به رنجم</p></div>
<div class="m2"><p>خون شد دل و جانِ نکته سنجم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سنجیده رهی به گوش ما زن</p></div>
<div class="m2"><p>آتش به نهاد هوش ما زن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>فریادرسی کجاست جز تو؟</p></div>
<div class="m2"><p>عیسی نفسی کجاست جز تو؟</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ساقی، به صفای طینت می</p></div>
<div class="m2"><p>بزدا غم دل، به همّت می</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مگذار درین خمار ما را</p></div>
<div class="m2"><p>افسرده و سوگوار ما را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در ده قدحی به رغم اختر</p></div>
<div class="m2"><p>روشنگر آفتاب انور</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مطرب، به ترانه های دلکش</p></div>
<div class="m2"><p>در خرمن کفر و دین زن آتش</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>آزردهٔ نیش و کفر و کیشم</p></div>
<div class="m2"><p>آزاد کن از طلسم خویشم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>هستی، غم و درد جان گزایی ست</p></div>
<div class="m2"><p>این عمر، دراز اژدهایی ست</p></div></div>