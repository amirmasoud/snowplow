---
title: >-
    بخش ۸ - اندرز به شاه صفوی
---
# بخش ۸ - اندرز به شاه صفوی

<div class="b" id="bn1"><div class="m1"><p>ای وارث کشور سلیمان</p></div>
<div class="m2"><p>خورشید چو خاتمت به فرمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای روشنی چهار اقلیم</p></div>
<div class="m2"><p>از توست فروغ تخت و دیهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اقبال ز توست، عرش معراج</p></div>
<div class="m2"><p>شاهی ز تو یافت، دُرّهٔ التّاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم دو سه حرف بخردانه</p></div>
<div class="m2"><p>گر تو نشماریش فسانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند مجال حرف تنگ است</p></div>
<div class="m2"><p>معنی دریا و ظرف تنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرمایهٔ دل زیان ندارد</p></div>
<div class="m2"><p>لیک ار شنوی زبان ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخ است حدیث راستگویان</p></div>
<div class="m2"><p>این زهر به کام، شهد گردان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اول سخنی که دارم این است</p></div>
<div class="m2"><p>هش دار که مهر دهر، کین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غره مشو از فراخ دستی</p></div>
<div class="m2"><p>رنج است گران خمار مستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غافل منشین درین گذرگاه</p></div>
<div class="m2"><p>غفلت چاه است و آگهی راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زلف املت که پیچ پیچ است</p></div>
<div class="m2"><p>مفتون نشوی به او که هیچ است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دفتر دهر سست پیمان</p></div>
<div class="m2"><p>افسانه ی باستان فروخوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عبرتکدهٔ جهان نظرکن</p></div>
<div class="m2"><p>سودای هوا ز سر بدر کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاکی که قدم زنی بر آن چُست</p></div>
<div class="m2"><p>فرق پدران رفتهٔ توست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش تو برین سریر و خرگاه</p></div>
<div class="m2"><p>بنگر به نشست و خاست ناگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر تخت شهی فراغتت چیست؟</p></div>
<div class="m2"><p>دانی که مکان هشتهٔ کیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش از تو، گروه سرفرازان</p></div>
<div class="m2"><p>بودند به تاج و تخت نازان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیش از تو،درین بساط نیرنگ</p></div>
<div class="m2"><p>بستند کمر به خسروی، تنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بستند و گشاد، آسمان چُست</p></div>
<div class="m2"><p>دربست و گشاد، نوبت توست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای خفته بمال چشم و برخیز</p></div>
<div class="m2"><p>کایّام زند به باره، مهمیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آسایش عمر، بی درنگ است</p></div>
<div class="m2"><p>بشتاب که وقت کار تنگ است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>امروز وسیلهٔ امانی</p></div>
<div class="m2"><p>مردم رمه اند و تو شبانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیک و بد کار خویش دریاب</p></div>
<div class="m2"><p>عیب است به چشم پاسبان خواب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آگاه نشین و با خدا باش</p></div>
<div class="m2"><p>آمادهٔ پرسش جزا باش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عمری که دو اسبه ره سپار است</p></div>
<div class="m2"><p>خودگو، که به وی چه اعتبار است؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از عدل اگر توانی امروز</p></div>
<div class="m2"><p>زاد سفر از جهان بیندوز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیک و بد اگر کنون گذاراست</p></div>
<div class="m2"><p>پاداش عمل ولی خدا راست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر کرده ثواب و گر گناه است</p></div>
<div class="m2"><p>اندوختهٔ تو، زاد راه است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیکیّ تو است نوش، یا نیش</p></div>
<div class="m2"><p>هان تا نکنی زیان، بیاندیش</p></div></div>