---
title: >-
    بخش ۱۷ - در توصیف دارالسلطنه اصفهان گوید
---
# بخش ۱۷ - در توصیف دارالسلطنه اصفهان گوید

<div class="b" id="bn1"><div class="m1"><p>گرامی ترین عضو انسان دل است</p></div>
<div class="m2"><p>سواد جهان را سپاهان دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنبر زمینش، به مینو زند</p></div>
<div class="m2"><p>اساسش، به افلاک پهلو زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشام، از شمیمش، مروّح نشان</p></div>
<div class="m2"><p>نسیمش به فردوس، دامن فشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی از دل افتادگانش، حرم</p></div>
<div class="m2"><p>ز گلخن نشینان کویش، ارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خاکش نخیزد غبار خطی</p></div>
<div class="m2"><p>که از سبزه دارد بهار خطی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذشته ست هر برج او زآسمان</p></div>
<div class="m2"><p>چو مستان میخانه کش، سرگران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن باره، نظّاره ماند ز تک</p></div>
<div class="m2"><p>فرازش سماک و نشیبش سمک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حصاری بود، در حصارش سپهر</p></div>
<div class="m2"><p>یکی ذرّه، در عرصه اش ماه و مهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدیدی اگر، سدّ زاینده رود</p></div>
<div class="m2"><p>سکندر خجل از سد خویش بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر تر کند خضر، از آن آب لب</p></div>
<div class="m2"><p>سکندر کند در دل خاک، تب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پلش، لجّه پیمای پایندگیست</p></div>
<div class="m2"><p>که هر چشمه اش، چشمهٔ زندگی ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طرب خیز خاکش، روان پرورد</p></div>
<div class="m2"><p>هوایش، مسیحا دمان پرورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اویس، ار درین شهر جا داشتی</p></div>
<div class="m2"><p>پرستش، هوا را روا داشتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به هرکوچه او، دو صد کشور است</p></div>
<div class="m2"><p>که شهری به هر خانهٔ او، در است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز خاک رهش، سرمه مردمک</p></div>
<div class="m2"><p>براو، دیدهء روشنان فلک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تماشای هر قصر عالی جناب</p></div>
<div class="m2"><p>فکنده کلاه از سر آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هر کلبه، هر حجره و هر رواق</p></div>
<div class="m2"><p>به موزونی و دلپذیریست طاق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زند فال سعد از خیابان خویش</p></div>
<div class="m2"><p>که دارد جداول ز تقویم، بیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به چشمی که سروش شود جلوه گر</p></div>
<div class="m2"><p>ز بالا بلندان، بپوشد نظر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گلش، چون بهار تماشا شود</p></div>
<div class="m2"><p>تماشا، به صد شیوه شیدا شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنارش که چون صوفیان است، مست</p></div>
<div class="m2"><p>فشاند به کونین، از وجد دست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز تر میوه های لطافت سرشت</p></div>
<div class="m2"><p>به باغش، توان یافت کام از بهشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان جوست آن خاک فیروزمند</p></div>
<div class="m2"><p>بود مصر، در هر دِهَش، شهربند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به هر گام او سلسبیلی سبیل</p></div>
<div class="m2"><p>بجا خشک ماند ازآن خاک، نیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اساسش نگردد ز دوران، خراب</p></div>
<div class="m2"><p>گرفته ست گل عدل و دادش در آب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرافراز، از آن خطّه شد تخت و تاج</p></div>
<div class="m2"><p>خُوَرنق به کاخش فرستد، خراج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شکوهش، شگرف است سنجیده را</p></div>
<div class="m2"><p>کند خیره، چشم جهان دیده را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه گویم ز دانش پژوهان او؟</p></div>
<div class="m2"><p>بود گوهر دانش، از کان او</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حقیقت شناسان هر خوب و زشت</p></div>
<div class="m2"><p>ملک کیش، مردان قدسی سرشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جواهر فروشان کلک و زبان</p></div>
<div class="m2"><p>فلک سیرهوشان روشن روان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نکو محضران پسندیده کیش</p></div>
<div class="m2"><p>مراقب حضوران غایب ز خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مَهِ نو رکابان خورشید رخش</p></div>
<div class="m2"><p>سکندر گدایان اقلیم بخش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خلیل آیتان مسیحا نفس</p></div>
<div class="m2"><p>دلیلان سرگشته فریادرس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان سرورانند، روشن روان</p></div>
<div class="m2"><p>که خالی مبادا، از ایشان جهان</p></div></div>