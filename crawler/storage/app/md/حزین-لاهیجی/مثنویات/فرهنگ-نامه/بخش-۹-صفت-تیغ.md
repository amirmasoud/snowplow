---
title: >-
    بخش ۹ - صفت تیغ
---
# بخش ۹ - صفت تیغ

<div class="b" id="bn1"><div class="m1"><p>تناور نهنگی ست شمشیر او</p></div>
<div class="m2"><p>سر شرزه شیر است، نخجیر او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضا را به کشور بود مرزبان</p></div>
<div class="m2"><p>زبان اجل را بود ترجمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدانسان که گل، جامه سازد، کفن</p></div>
<div class="m2"><p>کند لخت چرم شخ کرگدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یک حملهاش، در سپنجی سرای</p></div>
<div class="m2"><p>طرفدار پنجم، درافتد ز پای؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو لقمه به دم، قاف را بشکرد</p></div>
<div class="m2"><p>جگرگاه البرز را بر دَرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط سرنوشت یلان راست، کیش</p></div>
<div class="m2"><p>تراشیدن بیستون راست، نیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازو خاک در لرزه چون برگ بید</p></div>
<div class="m2"><p>به یک جو روان، آب و آتش که دید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سهمش قد تیر گردون، کمان</p></div>
<div class="m2"><p>برش، پیکر فتح را پشتوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خون در برش ارغوانی پرند</p></div>
<div class="m2"><p>سران، از خم جوهرش در کمند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به صیدافکنی، چون درآید دلیر</p></div>
<div class="m2"><p>فتد لرزه بر گردهٔ نرّه شیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمش، بارگاه ظفر را رواق</p></div>
<div class="m2"><p>دمش، از دو پیکر ببرد نطاق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کند نام هستی ز بد کیش، حک</p></div>
<div class="m2"><p>دو یک پنج نوبت زند بر فلک</p></div></div>