---
title: >-
    بخش ۱۰
---
# بخش ۱۰

<div class="b" id="bn1"><div class="m1"><p>هستی بر مقیدات از حق</p></div>
<div class="m2"><p>بی مقیّد نمی شود مطلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دو جانب فتاده استلزام</p></div>
<div class="m2"><p>حاجت از یک طرف بود به دوام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واجب از ممکن است مستغنی</p></div>
<div class="m2"><p>بی نیاز است و احتیاجی نی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاصه ممکن افتقار بود</p></div>
<div class="m2"><p>او ز هر سلک و هر شمار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گدا مضحک است، استغنا</p></div>
<div class="m2"><p>همه هیچ و خدای راست، غنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لازم مطلق است اگر ممکن</p></div>
<div class="m2"><p>نیست شکی درین سخن لیکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست لازم مقید مخصوص</p></div>
<div class="m2"><p>نتواند شدن یکی منصوص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدلی چون که نیست مطلق را</p></div>
<div class="m2"><p>نبود چون تعددی حق را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قبلهٔ احتیاج ها همه اوست</p></div>
<div class="m2"><p>جزء و کل را بر آستانش روست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی نیازی مطلق از ذات است</p></div>
<div class="m2"><p>ذات او قبله گاه حاجات است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک الوهیت و ربوبیت</p></div>
<div class="m2"><p>بی مقید کجا دهد صورت؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شده شمس ظهور اسمایی</p></div>
<div class="m2"><p>همه ذرات را تقاضایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طالبیت مقام تفضیل است</p></div>
<div class="m2"><p>نصِّ احَببتُ را چه تأویل است؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ذات کامل، کمال خود خواهد</p></div>
<div class="m2"><p>هر جمیلی جمال خود خواهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لاجرم در مظاهر آفاق</p></div>
<div class="m2"><p>متجلی شود، علی الاطلاق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اوج اطلاق و قید تنزیلی</p></div>
<div class="m2"><p>حصر اجمالی است و تفصیلی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جمله ابیات من که می خوانی</p></div>
<div class="m2"><p>همه بیت الله است اگر دانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتمت از دل معارف زای</p></div>
<div class="m2"><p>لیک هش دار تا نلغزد پای</p></div></div>