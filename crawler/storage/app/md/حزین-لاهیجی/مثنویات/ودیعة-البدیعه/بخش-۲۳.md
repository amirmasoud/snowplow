---
title: >-
    بخش ۲۳
---
# بخش ۲۳

<div class="b" id="bn1"><div class="m1"><p>فی الوجود الّذی هو المالک</p></div>
<div class="m2"><p>از ازل تا ابد بود هالک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر هستی که والی قدم است</p></div>
<div class="m2"><p>هر چه هستش گمان کنی عدم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات هستی ست هست و دیگر هیچ</p></div>
<div class="m2"><p>چون زِه افتاده ای به پیچا پیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه دارد نمود، بود دل است</p></div>
<div class="m2"><p>جلوه آرایی از شهود دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نوردد چو این فکنده بساط</p></div>
<div class="m2"><p>می کند این تعیّنات اسقاط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک حق باقی است الی الآباد</p></div>
<div class="m2"><p>دامَ دار الوجود وَ الایجاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض حق را بقا بود دایم</p></div>
<div class="m2"><p>عین مقبول هم تویی قائم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اثر فیض یافت چون قابل</p></div>
<div class="m2"><p>ابدیّت ورا شود حاصل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیستی اراا رهی ز هستی نیست</p></div>
<div class="m2"><p>نیستی، مرد چیره دستی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واجب او را که شد وجود از حق</p></div>
<div class="m2"><p>لم یزل وجَهه و لم یزهق ¹</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عدم، او را نمی شود طاری</p></div>
<div class="m2"><p>یستمدّ البقا مِنَ الباری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>متقوم به عین ذات شود</p></div>
<div class="m2"><p>متجدّد تعیّنات شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ماقَضیٰ نحبهُ و ما هُو آت</p></div>
<div class="m2"><p>همه باشد تبدّل نشآت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر گذارد تعیّنی از بر</p></div>
<div class="m2"><p>باز پوشد تعیّنی دیگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر تعیّن که می شود زایل</p></div>
<div class="m2"><p>نوبت دیگری شود حاصل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کلک عاقل بود به مَرِّ دهور</p></div>
<div class="m2"><p>نقش بند ظهور بعد ظهور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>موجهٔ چشمهٔ حیات است این</p></div>
<div class="m2"><p>اتّصال تجلّیات است این</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طیّ اطوار دان، تبدّل حال</p></div>
<div class="m2"><p>همچنین طیّ برزخی و مثال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس از آن است حشر انسانی</p></div>
<div class="m2"><p>زال سپس.........</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قصّه کوته، به سمع خامه حزین</p></div>
<div class="m2"><p>آستین زن که آفتاب است این</p></div></div>