---
title: >-
    بخش ۲۸
---
# بخش ۲۸

<div class="b" id="bn1"><div class="m1"><p>غفلت از خویش داشت بی خبرت</p></div>
<div class="m2"><p>ما کشیدیم پرده از نظرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این زمان چشم تیزبین داری</p></div>
<div class="m2"><p>پرتو صبح راستین داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه در پرده داشتی پنهان</p></div>
<div class="m2"><p>جان کتاب الله است، ناطق از آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش بربسته ایم کار تو را</p></div>
<div class="m2"><p>کرده در دامنت ثمار تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسخه کردیمت آشکار و ضمیر</p></div>
<div class="m2"><p>نه صغیری برون از آن، نه کبیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه خودکشتهای به بار آید</p></div>
<div class="m2"><p>خرمن کشت درکنار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متبدل نگشته کشته کس</p></div>
<div class="m2"><p>جو ز جو روید و عدس ز عدس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خارخاری به باغ گل ندهد</p></div>
<div class="m2"><p>شاخ گل هم، ز حدّ خود نجهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انقلاب حقایق است محال</p></div>
<div class="m2"><p>خودکتابی و می گشایی فال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد ره ار فال برزنی کم و بیش</p></div>
<div class="m2"><p>خود به فال خود آیی ای درویش</p></div></div>