---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>چون ز من پیش، خواجهٔ عارف</p></div>
<div class="m2"><p>آن به اسرار سالکان واقف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد معنی حکیم ربانی</p></div>
<div class="m2"><p>باده پیمای فیض یزدانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کهن مست جرعه های ازل</p></div>
<div class="m2"><p>هوشیار دیار علم و عمل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و جان دادهٔ ولای علی</p></div>
<div class="m2"><p>نکته پرداز، بر خفی و جلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارس عرصهٔ سخن سازی</p></div>
<div class="m2"><p>پیشتاز فوارس تازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زده کلکش بر آسمان بیرق</p></div>
<div class="m2"><p>فارسی را کلام او رونق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجهٔ غزنوی سنایی راد</p></div>
<div class="m2"><p>که به روحش تحیت حق باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زد در این بحر نقش گویایی</p></div>
<div class="m2"><p>که کند کوثرش جبین سایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاهی ابیات برگزیدهٔ او</p></div>
<div class="m2"><p>که سجلّ است بر جریدهٔ او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوربخش دماغ من گشتی</p></div>
<div class="m2"><p>نمک افشان داغ من گشتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عندلیب قلم ز طبع حزین</p></div>
<div class="m2"><p>طلبیدی حدیقهٔ دومین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک از افسردگی قبول نشد</p></div>
<div class="m2"><p>حالی طبع بوالفضول نشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا در این تنگنای وقت رحیل</p></div>
<div class="m2"><p>خامه سر کرد، صور اسرافیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نقش چندی به ارتجال زدم</p></div>
<div class="m2"><p>آهی از تنگی مجال زدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بو که منظور اهل دید افتد</p></div>
<div class="m2"><p>وین سیه نامه، روسفید افتد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعد پنجه هزار شعر گزین</p></div>
<div class="m2"><p>که درآمد به دفتر تدوین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عمر هم در جوار هفتاد است</p></div>
<div class="m2"><p>مشت خالی مرا پر از باد است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رنجه کلکم شد از شکرخایی</p></div>
<div class="m2"><p>شاعری چیست؟ بادپیمایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عمر در مستی گذاره گذشت</p></div>
<div class="m2"><p>مستعارم به استعاره گذشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا یکی وزن و قافیه سنجم</p></div>
<div class="m2"><p>حق به دست من است اگر رنجم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیشهٔ مبتذل نه کار من است</p></div>
<div class="m2"><p>شاعری عار اعتبار من است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاصه در روزگار بی نصفت</p></div>
<div class="m2"><p>وندرین عصر بی تمیز صفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که بجا مانده پشم بافی چند</p></div>
<div class="m2"><p>ژاژ خایان هرزه لافی چند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خام و بی مغز و بی ادب یکسر</p></div>
<div class="m2"><p>در خریّت ز کون خر پَس تر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خِردَم بانگ زد که ای خیره</p></div>
<div class="m2"><p>صفحه تا چند می کنی تیره؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کم نوشتی به صفحهٔ ایّام؟</p></div>
<div class="m2"><p>داستان سنجیت نگشت تمام؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل نفرسودت از سخن سازی؟</p></div>
<div class="m2"><p>داستان دگر چه آغازی؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چند بر خامه می توان پیچید؟</p></div>
<div class="m2"><p>صفحه کردی سیاه و موی سفید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وقت خاموشی است هرز مجوش</p></div>
<div class="m2"><p>بر زبان بستگان سخن مفروش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرزه در... می دهی به خمیر</p></div>
<div class="m2"><p>به خران درخور است مشت شعیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هم ضمیران با وفا رفتند</p></div>
<div class="m2"><p>سینه صافان باصفا رفتند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اندرین عرصه گمرهی از هوش</p></div>
<div class="m2"><p>نغمه پرداز گشته ای کو گوش؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتی و حرف مدعا گفتی</p></div>
<div class="m2"><p>گهری سفتی و به جا سفتی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لیک از آغاز این کساد هنر</p></div>
<div class="m2"><p>به هنرپروران نبودم سر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو قلم زیر تیغ بالیدم</p></div>
<div class="m2"><p>زخم خوردم سخن سراییدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مزد و منّت نداشتم خواهش</p></div>
<div class="m2"><p>خاطر آسوده بود با کاهش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو خروشی که می سراید گوش</p></div>
<div class="m2"><p>کار نبود مرا به ناله نیوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نالم و ناله سنج خوبش خودم</p></div>
<div class="m2"><p>نمک افشان و سینه ریش خودم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاید آبی به روی کار آید</p></div>
<div class="m2"><p>خشک دی بگذرد بهار آید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیند ایّام روی یاران را</p></div>
<div class="m2"><p>پرده سنجان و خوش عیاران را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گوش صاحبدلی نیوشد راز</p></div>
<div class="m2"><p>حسن انجام یابد این آغاز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سخنم بشنود سخندانی</p></div>
<div class="m2"><p>هدیه سازد دعای غفرانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زیر هر حرف خویش پنهانم</p></div>
<div class="m2"><p>تن گفتار خویش را جانم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر که ما را به خیر یاد کند</p></div>
<div class="m2"><p>غم و اندوه، جزو باد کند</p></div></div>