---
title: >-
    بخش ۴۱
---
# بخش ۴۱

<div class="b" id="bn1"><div class="m1"><p>شکر لله که هفته ای مهلت</p></div>
<div class="m2"><p>یافتم از حیات کم فرصت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که برین چند صفحه، ریخت قلم</p></div>
<div class="m2"><p>از خط مشک فام، طرح ارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلمم، سرو جویباران است</p></div>
<div class="m2"><p>رقمم خطّ گلعذاران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قرنها بگذرد که طبع و قلم</p></div>
<div class="m2"><p>از یم فیض بخش، گیرد نم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نیابی پس از هزاران سال</p></div>
<div class="m2"><p>دل دریاکش و زبان مقال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه گویا و گنگ، از کِه و مَه</p></div>
<div class="m2"><p>منفذ سِفلیند، خامش به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر و مغز مقلّدان پوچ است</p></div>
<div class="m2"><p>جوز نغز مقلدان پوچ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این به خود بستگان که می بینی</p></div>
<div class="m2"><p>خام لفظند و خاسر معنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه لالند و خوش مقال این کلک</p></div>
<div class="m2"><p>پیر زالند و پور زال این کلک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خامه البرز کوه لرزاند</p></div>
<div class="m2"><p>رگ خارا به مصرعم ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فلتانم که در مجاز افتد</p></div>
<div class="m2"><p>لخت کوه است کز فراز افتد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>موج معنی، ز کلک دریا دل</p></div>
<div class="m2"><p>ریخت چندان که بحر شد، ساحل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خامه ام قصر خلد کرد بنا</p></div>
<div class="m2"><p>کس چه داند، درین سپنج سرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قصرهای ریاض رضوانی</p></div>
<div class="m2"><p>می نگنجد به کاخ دهقانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرد باید، حریف نامهٔ من</p></div>
<div class="m2"><p>نفخ صور است، بانگ خامهٔ من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جان کند زنده، تن بمیراند</p></div>
<div class="m2"><p>تن چه داند، قلم چه می راند؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این نگفتم که عامیان خوانند</p></div>
<div class="m2"><p>قدر این نامه، عارفان دانند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در جهان نکته رس نمی بینم</p></div>
<div class="m2"><p>مرد این نامه کس نمی بینم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آتش است این نوا که می دارد</p></div>
<div class="m2"><p>شعله را، مرد باد پندارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زنده را نان غذای تن باشد</p></div>
<div class="m2"><p>مرده را، خاک در دهن باشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرغ سدره ست، کلک دستان زن</p></div>
<div class="m2"><p>گوش عامی ست، روزن گلخن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شکن نامه، رشک چین دارم</p></div>
<div class="m2"><p>در رقم، مشک و انگبین دارم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نافه، دریوزه گر، دوات مراست</p></div>
<div class="m2"><p>نیل، لب تشنهٔ فرات مراست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جوی شهدی که از قلم جاری ست</p></div>
<div class="m2"><p>نه شراب عوام بازاریست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل دریاکشی همی باید</p></div>
<div class="m2"><p>که ازین جرعه، بحر پیماید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوالهوس را، مزاج صفراوی ست</p></div>
<div class="m2"><p>شهد بنمودنش، جگرکاویست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نبرد هر کسی ز حلوا بهر</p></div>
<div class="m2"><p>که بود در مزاج بر وی، زهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>طفل شش روزه را طعام ترید</p></div>
<div class="m2"><p>می فشارد گلو، به عصر شدید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لقمه ای گر دهی به کودک خرد</p></div>
<div class="m2"><p>طمع از وی ببرکه کودک مرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیر کودک مزاج، بی حصر است</p></div>
<div class="m2"><p>بالغ الرشد، نادر العصر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ک...ون کشان، قد کشیده اند امروز</p></div>
<div class="m2"><p>همه مالابکور و ریش به گوز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ریش گاوند، لافیان جهان</p></div>
<div class="m2"><p>دم گاو است به، ز ریشِ چنان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرد بالغ، به ریش و سبلت نیست</p></div>
<div class="m2"><p>مردی و مردمی به حیلت نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مکر و تلبیس، خوی ابلیس است</p></div>
<div class="m2"><p>همه ریش تو قحبه، تلبیس است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تیز بازیگران بازاری</p></div>
<div class="m2"><p>ریشخندی به ریش خود داری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پَرِ پندار، عرش پرواز است</p></div>
<div class="m2"><p>پشّه، در چشم خویش شهباز است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قید پندار، نشکند آسان</p></div>
<div class="m2"><p>جز به عون خدای ذوالاحسان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تلخ اگر حرف ماست، در کامت</p></div>
<div class="m2"><p>عقل، شیرین کناد، در جامت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>داروی تلخ، رنج بزداید</p></div>
<div class="m2"><p>سخن تلخ سودمند آید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صدق محضم، کتاب مرقومم</p></div>
<div class="m2"><p>لب مدزد، از رحیق مختومم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رشح این خامه، موج تسنیم است</p></div>
<div class="m2"><p>طعنه بر خود مدان،که تعلیم است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه کنم چون تو فرق نگذاری؟</p></div>
<div class="m2"><p>سخن راست، طعنه پنداری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نفس رعناست، خصم جانی تو</p></div>
<div class="m2"><p>به زیان داد، زندگانی تو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کرد، ای دوغ خورده ی بد مست</p></div>
<div class="m2"><p>خودپرستی تو را سپاس پرست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هیچ در دیدهٔ تو نیست فروغ</p></div>
<div class="m2"><p>هجی راست، به ز مدح دروغ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دل آزاده، فارغ از مدح است</p></div>
<div class="m2"><p>هرچه گویم زمانه را قدح است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه هجا پیشه ام نه مدح شعار</p></div>
<div class="m2"><p>خفته ای را مگر کنم بیدار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خیرخواهیست مقصد و نیت</p></div>
<div class="m2"><p>پاک نیت چه باکش از تهمت؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>راست بینی و راست گفتاری</p></div>
<div class="m2"><p>می کند برکجان، گرانباری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>این نگویم که نیک و دانا شو</p></div>
<div class="m2"><p>هرچه هستی، به خویش بینا شو</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خواه نسناس باش و خواهی ناس</p></div>
<div class="m2"><p>هرچه هستی، تو حد خود بشناس</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کار مردان به خود مبند به زور</p></div>
<div class="m2"><p>دل به دعوی گری مکن مغرور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نیست کار تو، بینش معنی</p></div>
<div class="m2"><p>اینقدر بس که پیش پا بینی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نرساندی به هیچ دل، جز درد</p></div>
<div class="m2"><p>لاف مردی چه می زنی، نامرد؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نزده نقش بوربابافی</p></div>
<div class="m2"><p>در شگفتم که از چه می لافی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پا ته و گام، خوش فراخی زن</p></div>
<div class="m2"><p>با خریت، به ابر شاخی زن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مانده در کار خویش بیچاره</p></div>
<div class="m2"><p>وندرین کارگاه، هر کاره</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کار پاکان به خویشتن بستی</p></div>
<div class="m2"><p>دستی و پشت دستی و پستی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نکنی درک، معرب و مبنی</p></div>
<div class="m2"><p>از تو دور است راه تا معنی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چه کنی فکر در حدوث و قدم؟</p></div>
<div class="m2"><p>باش درفکر فرج خویش و شکم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نشکیبد زکار خود مزدور</p></div>
<div class="m2"><p>کار فرج و شکم تو راست، ضرور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>لایق هرکسی بود کاری</p></div>
<div class="m2"><p>به مثل، هر لُریّ و بازاری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در نگیرد تو را چو هیچ سخن</p></div>
<div class="m2"><p>وقت تنگ است، هر چه خواهی کن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یک دو روزیست مهلت دنیا</p></div>
<div class="m2"><p>چند پاید حیات سست بنا؟</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>حرف حق را، اگر رواجی نیست</p></div>
<div class="m2"><p>مرض جهل را علاجی نیست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مرده از فیض عیسی، احیا گشت</p></div>
<div class="m2"><p>عیسی از جاهلان گریخت به دشت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>عامی خیره سر، بلاخیز است</p></div>
<div class="m2"><p>دشمنی در جهان بداحیز است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نوش جاهل، همیشه نیش آمد</p></div>
<div class="m2"><p>انبیا را ببین چه پیش آمد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همسریها به اولیا کردند</p></div>
<div class="m2"><p>عهد بدگوهری ادا کردند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>حق، بدان را بلای نیکان کرد</p></div>
<div class="m2"><p>آفرینش، برای نیکان کرد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز گزندی کزین گروه کشند</p></div>
<div class="m2"><p>شهد جان پروری ز دوست چشند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دولتی را کز ابتدا دارند</p></div>
<div class="m2"><p>همه زین خلق جانگزا دارند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خامه فرسوده شد ز رَه سپری</p></div>
<div class="m2"><p>وه چه سازم به آتشین جگری</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دل من تنگ بود و فرصت تنگ</p></div>
<div class="m2"><p>غنچه را فصل دی چه بوی و چه رنگ؟</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>آنچه من دیدم از زمانهٔ خویش</p></div>
<div class="m2"><p>آن ندیده ست از نمک، دل ریش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بار دردی که دل به دوش کشید</p></div>
<div class="m2"><p>می نیارم تو را به گوش کشید</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مدتی رفت و روزگاری شد</p></div>
<div class="m2"><p>کز هنر، دل به زیر باری شد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بارها عهد بستمی با دل</p></div>
<div class="m2"><p>که نریزد گهر،کف با دل</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>شکنم خامه، صفحه پاره کنم</p></div>
<div class="m2"><p>سینه می جوشدم، چه چاره کنم؟</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چه کنم؟ موج می زند دل ریش</p></div>
<div class="m2"><p>موجهٔ بحر را، که بندد پیش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>شور دل، چون لبم بجنباند</p></div>
<div class="m2"><p>زور این لطمه، کوه غلتاند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نه به فکرم سر است و نه گفتار</p></div>
<div class="m2"><p>سخنم چیست؟ موج دریا بار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>حق علیم است کاندرین پیشه</p></div>
<div class="m2"><p>روز اول، نبودم اندیشه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>لبم از شیر شست، آب سخن</p></div>
<div class="m2"><p>لوح پیشانیم، کتاب سخن</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سخن از ماست جاودان، زنده</p></div>
<div class="m2"><p>وز سخن های ماست، جان زنده</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به من از چین رسید، قافله ها</p></div>
<div class="m2"><p>پر شد از بوی مشک، مرحله ها</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بوشناسان دماغ بگشایند</p></div>
<div class="m2"><p>برو آغوش داغ بگشایند</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>صفحه ها، طبله های عطار است</p></div>
<div class="m2"><p>نقطه ها، نافه های تاتار است</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>غیر مشک ختن طراز قلم</p></div>
<div class="m2"><p>نبود داغ عشق را مرهم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>می کند می، به کام مخمورم</p></div>
<div class="m2"><p>مشک پرورده، داغ ناسورم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>رگ جان تار و ناله مضراب است</p></div>
<div class="m2"><p>ساغرم داغ و باده خوناب است</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چکد آتش ز نالهٔ سردم</p></div>
<div class="m2"><p>همه دردم، که عشق پروردم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خلفم، عشق کیمیاگر را</p></div>
<div class="m2"><p>شعله می پرورد، سمندر را</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>مایه دار، از محیط بوی من است</p></div>
<div class="m2"><p>آتشین باده در سبوی من است</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مرحبا عشق جان و دل پرور</p></div>
<div class="m2"><p>پخته نام من است از آن آذر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>از بهارش، شکفته باغ دلم</p></div>
<div class="m2"><p>آتشین لاله است، داغ دلم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>دیده هر جا فشاند مژگانی</p></div>
<div class="m2"><p>چهره افروز شد، گلستانی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>از کنارم که خلوت یار است</p></div>
<div class="m2"><p>ز جگر پاره پاره گلزار است</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>شد کمان گرچه قامت چو خدنگ</p></div>
<div class="m2"><p>چنگ عشقم خمیده باشد تنگ</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>می خروشد، دل خراشیده</p></div>
<div class="m2"><p>غمم از بهر آن تراشیده</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چنگ باید که در خروش بود</p></div>
<div class="m2"><p>نپرم سازم ار خموش بود</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>روم از خود به نالهٔ سحری</p></div>
<div class="m2"><p>ناقه را، از حدی ست، رَه سپری</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بی سماعم نمی شود رَه طی</p></div>
<div class="m2"><p>می روم هم عنان ناله، چون نی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>نی منم، نایی آن مسیح نفس</p></div>
<div class="m2"><p>دم اقبال فیضه الاقدس</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>عشق می ‎گویم و زبان سوزد</p></div>
<div class="m2"><p>لب ز تبخاله، خرمن اندوزد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نفسم آتش و زبان عشق است</p></div>
<div class="m2"><p>تب گرمم در استخوان عشق است</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>در نیستانم آتش افتاده</p></div>
<div class="m2"><p>کار با عشق سرکش افتاده</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نیستان رفت و آتش است بلند</p></div>
<div class="m2"><p>همه آتش بود، کجاست سپند؟</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>از سپند است، یک خروشیدن</p></div>
<div class="m2"><p>چاره نبود به جز که جوشیدن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>صبح در سینه، یک نفس دارد</p></div>
<div class="m2"><p>دستگاه فغان، جرس دارد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>فرصت گفتن و شنیدن کو؟</p></div>
<div class="m2"><p>طاقت یک نفس کشیدن کو؟</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>رفته از جوش رعشهٔ پیری</p></div>
<div class="m2"><p>از کفم قدرت قلم گیری</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>گوهری چند، از قلم سفتم</p></div>
<div class="m2"><p>چند ساعت، ز هفته ای گفتم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>داشتم گر مجال یک شبه ای</p></div>
<div class="m2"><p>می رساندم سخن، به مرتبه ای</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>که به سالی نیاریش خواندن</p></div>
<div class="m2"><p>لیک واماندم از سخن راندن</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به کلیمی که آفریده سخن</p></div>
<div class="m2"><p>که ندارم سر سخن گفتن</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>قلم اکنون به دیده ام خار است</p></div>
<div class="m2"><p>صفحه بر طبع نازکم بار است</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سر و برگ سخن سرایی کو</p></div>
<div class="m2"><p>کلّ شیءٍ یزول اِلّا هو</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>غفر الله ربنا و عفی</p></div>
<div class="m2"><p>حسبنا الله ربّنا وکفیٰ</p></div></div>