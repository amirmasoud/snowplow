---
title: >-
    بخش ۲
---
# بخش ۲

<div class="b" id="bn1"><div class="m1"><p>ای رخت در نقاب اسمایی</p></div>
<div class="m2"><p>عین پنهان و محض پیدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ظهورت منزّه از تأویل</p></div>
<div class="m2"><p>وی بطونت مقدس از تعطیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و ما شبهه های تحصیل است</p></div>
<div class="m2"><p>موج آب روان تنزیل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کَرَم های بی نهایت تو</p></div>
<div class="m2"><p>قطره، بحری ست از عنایت تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زده هر ذره، کوس خورشیدی</p></div>
<div class="m2"><p>هرکدام است جام جمشیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم لیلی کرشمه زار از توست</p></div>
<div class="m2"><p>دل دیوانه داغدار از توست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بهارت حدوث برگ گلی ست</p></div>
<div class="m2"><p>هستی از ساغر تو قطره مُلی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ دل، غنچهٔ بهارانت</p></div>
<div class="m2"><p>اشک خونین، ز لاله کارانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل دیگر ز عشق و حُسن دمید</p></div>
<div class="m2"><p>سبزه خط و شکوفه موی سفید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چتر خورشید سایه پرور توست</p></div>
<div class="m2"><p>پرتو او غبار لشکر توست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می کند بر خطت نهاده سری</p></div>
<div class="m2"><p>کاروان وجود ره سپری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امر و نهی تو لوح تعلیم است</p></div>
<div class="m2"><p>سجده تعظیم و جبهه تسلیم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فیض عامت رسا به خاره و گِل</p></div>
<div class="m2"><p>حرم خاص توست کعبهٔ دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از خیالت که چشمهٔ جوش است</p></div>
<div class="m2"><p>دل خونین من قدح نوش است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غم عشق اتوا راح ریحانی</p></div>
<div class="m2"><p>خشک لب زین میم نگردانی</p></div></div>