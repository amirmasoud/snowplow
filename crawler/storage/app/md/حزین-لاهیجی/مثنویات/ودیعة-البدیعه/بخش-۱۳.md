---
title: >-
    بخش ۱۳
---
# بخش ۱۳

<div class="b" id="bn1"><div class="m1"><p>می نیارد عدم شود موجود</p></div>
<div class="m2"><p>این عیان است پیش اهل شهود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل در کشوری که هست حکم</p></div>
<div class="m2"><p>نتواند شدن وجود عدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم آنجا که صورت علمی ست</p></div>
<div class="m2"><p>نسبت آن به هست و نیست یکی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذات ممکن که هست محض قبول</p></div>
<div class="m2"><p>کرده تسلیم امر و ترک فضول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود قابل وجود را و سبب</p></div>
<div class="m2"><p>قابلیت بود زبان طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب اقضای روز کند</p></div>
<div class="m2"><p>همه ذرات ازو بروز کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلوه گر گشت اقتدار قدیر</p></div>
<div class="m2"><p>که از آن می شود به کن تعبیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیکون است آن قبول وجود</p></div>
<div class="m2"><p>معنیش امتثال خواهد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قابل کون بود چون ممکن</p></div>
<div class="m2"><p>کون در وی نهفته و کامن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>متعلق چو شد ارادهٔ حی</p></div>
<div class="m2"><p>کون و کاین بروز کرد از وی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باطن و ظاهر است اسم خدا</p></div>
<div class="m2"><p>لیک نسبت به ذات هست جدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم چنین فاعل است و هم قابل</p></div>
<div class="m2"><p>فهم این نکته کی کند جاهل؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در وجود اجتماع فعل و قبول</p></div>
<div class="m2"><p>پیش روشندلان بود معقول</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن وجودی که عینی ذات است</p></div>
<div class="m2"><p>دو شئون است و دو اضافات است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غیر مجعول عین حق باشد</p></div>
<div class="m2"><p>جعل مخصوص ما خلق باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو بدین است عاجل و کامل</p></div>
<div class="m2"><p>از یکی فاعل از یکی قابل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قابل و فاعل از یسار و یمین</p></div>
<div class="m2"><p>هر چه آن داد می پذیرد این</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فعل او جز به نور او نشود</p></div>
<div class="m2"><p>مظهرش جز ظهور او نشود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مطلق از جملهٔ صفات خود است</p></div>
<div class="m2"><p>متعین به عین ذات خود است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این حقیقت به عین محجوبان</p></div>
<div class="m2"><p>چشم کور است و روی محبوبان</p></div></div>