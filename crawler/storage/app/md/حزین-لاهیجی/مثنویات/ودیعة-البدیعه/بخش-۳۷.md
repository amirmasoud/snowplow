---
title: >-
    بخش ۳۷
---
# بخش ۳۷

<div class="b" id="bn1"><div class="m1"><p>هست بر فسق، حمل کفر سزای</p></div>
<div class="m2"><p>ترک حج را شمرده کفر، خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر هم در کلام مصطفوی</p></div>
<div class="m2"><p>شده مُسند، به فاسقان غوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون که این چاره شد تو را معلوم</p></div>
<div class="m2"><p>می شود استفاده از مفهوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که اگر مطلبی ز شرع محیط</p></div>
<div class="m2"><p>می نداند کسی به جهل بسیط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رگی از کفر جهل با وی هست</p></div>
<div class="m2"><p>شسته است از کمال ایمان دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان کسی را که روی دل به خداست</p></div>
<div class="m2"><p>منکر اتباع نفس و هواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امتثال عوام، حق دارد</p></div>
<div class="m2"><p>امر و نهیش ز دست نگذارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور شود کس ز دوری توفیق</p></div>
<div class="m2"><p>منکر حق واجب التصدیق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست با وی، رگی ز کفر و جحود</p></div>
<div class="m2"><p>تیغ باید زدش ز نفس عنود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور کسی چیزی آورد به زبان</p></div>
<div class="m2"><p>که نباشد به دل، مصدق آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست با وی، رگی ز کفر و نفاق</p></div>
<div class="m2"><p>پای دارد دل و زبان به وفاق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و آنکه انکار می کند به جهان</p></div>
<div class="m2"><p>آنچه را نیست میل طبعش آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و آنچه دارد موافقت به هوس</p></div>
<div class="m2"><p>هست مقبول، پیش آن ناکس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عرقِ کفر یهودیش باشد</p></div>
<div class="m2"><p>رخ ایمان به تیشه بخراشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وآنکه گیرد، زرای خوبش سبق</p></div>
<div class="m2"><p>نکند اتباع حجت حق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست با وی رگی ز کفر و ضلال</p></div>
<div class="m2"><p>ز آنکه فرمان نبرده، در همه حال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و آنکه گرد حرام و شبهه تند</p></div>
<div class="m2"><p>قدمی لاابالیانه زند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عِرقی از کفر و فسق با وی هست</p></div>
<div class="m2"><p>شسته است از کمال ایمان دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر نفس در شمار کار خود است</p></div>
<div class="m2"><p>حافظ سر و آشکار خود است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس اگر سر زند گناهی ازو</p></div>
<div class="m2"><p>سرزند در ندامت، آهی ازو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مؤمن خالص حقیقی اوست</p></div>
<div class="m2"><p>او به دنیا و آخرت نیکوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نم ایمان در آب و گِلها باد</p></div>
<div class="m2"><p>نور ایمان چراغ دلها باد</p></div></div>