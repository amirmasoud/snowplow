---
title: >-
    بخش ۱۱
---
# بخش ۱۱

<div class="b" id="bn1"><div class="m1"><p>گر به هستی نظر کند عارف</p></div>
<div class="m2"><p>چون به علم الیقین بود واقف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیند اول فراخور حالش</p></div>
<div class="m2"><p>ذات حق و صفات افعالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به عین الیقین رسد زان پس</p></div>
<div class="m2"><p>همه ذات و صفات بیند و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونکه حق الیقین نماید رو</p></div>
<div class="m2"><p>گوید آنگاه لیس الا هو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او نگوید جز آنکه اوست که اوست</p></div>
<div class="m2"><p>همه را بنگرد چه مغز و چه پوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معنی مختلف به هستی نیست</p></div>
<div class="m2"><p>ذهنی و خارجی به یک معنی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیک دارد مراتب بسیار</p></div>
<div class="m2"><p>منکشف باد بر اولواالابصار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درجاتش یکی ز یک برتر</p></div>
<div class="m2"><p>اختلاف مراتبش بنگر!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر مقامی به وصفی و نامی</p></div>
<div class="m2"><p>هر یکی راست خاصه احکامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون الوهیت و ربوبیت</p></div>
<div class="m2"><p>پس عبودیت است و خلقیت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر کنی حفظ هر یک آگاهی</p></div>
<div class="m2"><p>نکنی حفظ اگر تو، گمراهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر یکی را ببین به جای خودش</p></div>
<div class="m2"><p>بنگر احکام آن، سزای خودش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه جز نقطه نیست آنهمه حرف</p></div>
<div class="m2"><p>قبض و بسطیست در میانه شگرف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نام آنها فقط نقط نکنی</p></div>
<div class="m2"><p>اعتبارات را غلط نکنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنین ظاهر است اینکه مداد</p></div>
<div class="m2"><p>واحد است و نباشدش تعداد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پنج خواهی نویس و خواهی بیست</p></div>
<div class="m2"><p>حرف ها را کرانه پیدا نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این کثیر و مداد یک باشد</p></div>
<div class="m2"><p>وحدتش کی مقام شک باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لاتکون الحروف مفقوده</p></div>
<div class="m2"><p>کلّها بالمداد موجوده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حیثُ لولا المداد، لیس الحرف</p></div>
<div class="m2"><p>زیر حرف من است بحری ژرف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیس الّا المداد فی الالواح</p></div>
<div class="m2"><p>بی نیاز است صبح از مصباح</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با همه حرفهای گوناگون</p></div>
<div class="m2"><p>از تغیر بود مداد مصون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر بگویند نیست غیر حروف</p></div>
<div class="m2"><p>در سراپای لوح بخش نشوف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این کلامی ست صدق و نیست گزاف</p></div>
<div class="m2"><p>می نگنجد در این قضیه خلاف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لیس الا المداد اگر گویی</p></div>
<div class="m2"><p>راه کوی درست می پویی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در تمامی مظاهر آفاق</p></div>
<div class="m2"><p>نیست غیر از منزه از اطلاق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دیدهٔ عارفان بزم شهود</p></div>
<div class="m2"><p>می ندیده ست غیر از او موجود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بیند اعیان ممکنات همه</p></div>
<div class="m2"><p>متلاشی به عین ذات همه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زآن که غیر از مداد پیدا نیست</p></div>
<div class="m2"><p>گفته ای حرف، لیک معنا نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اعتبارات وصفی و اسما</p></div>
<div class="m2"><p>غیر از این نیست گر شوی بینا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مثبت لوح می شود منفی</p></div>
<div class="m2"><p>منفیش هم ز نفی مستعفی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>می شود مهرهٔ دلم ماتش</p></div>
<div class="m2"><p>خیره در لوح نفی و اثباتش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>غیر از این نامه آشکار و نهفت</p></div>
<div class="m2"><p>حرف روشن چنین نباید گفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روشنی داد اگر چراغ دلم</p></div>
<div class="m2"><p>سوخت برق سخن، دماغ دلم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لیک شرمندهٔ مقال خودم</p></div>
<div class="m2"><p>خنده می آید از مثال خودم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سینه را چاک اگر کند زیبد</p></div>
<div class="m2"><p>به دهن خاک اگر کند زیبد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ذره پوید کجا و مهر کجا؟</p></div>
<div class="m2"><p>هو شمس الضحی و نحن فنا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نفس ای دل، قرین تاب و تب است</p></div>
<div class="m2"><p>عجز بالذات، مهر ما به لب است</p></div></div>