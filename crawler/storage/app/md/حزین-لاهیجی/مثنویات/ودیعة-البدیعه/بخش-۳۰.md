---
title: >-
    بخش ۳۰
---
# بخش ۳۰

<div class="b" id="bn1"><div class="m1"><p>ز ابتدای حدوث خود انسان</p></div>
<div class="m2"><p>تا به هنگام رفتنش ز جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرکات طبیعتش باشد</p></div>
<div class="m2"><p>انتقالات فطرتش باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد به هر صورتی که بزم آرا</p></div>
<div class="m2"><p>لَم یَزل ینتقل الی الاُخرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به همین بعد رحلت از دنیا</p></div>
<div class="m2"><p>به صراط خود است، رَه پیما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نماید مساعدت توفیق</p></div>
<div class="m2"><p>اِنّهُ خیرُ صاحبٍ و رفیق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند از هر مقام و منزل نقل</p></div>
<div class="m2"><p>تا شود متصل به عالم عقل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این به شرطی که اهل آن باشد</p></div>
<div class="m2"><p>جوهرش از مقربان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا به اهل یمین کند پیوند</p></div>
<div class="m2"><p>به توسط اگر بود خرسند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور بود مثل طبع شیطانش</p></div>
<div class="m2"><p>بخت سازد قرین خذلانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حشر او را کنند با حشرات</p></div>
<div class="m2"><p>عاقبت واجب است جمع شتات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود آخر، به دیو و دد محشور</p></div>
<div class="m2"><p>در ظلام نشیبگاه غرور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معنی مطلق صراط این است</p></div>
<div class="m2"><p>پیش چشمی که پاک و حق بین است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از صراط آنچه مستقیم آید</p></div>
<div class="m2"><p>رهبر جنّت و نعیم آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سالکش ساکن جنان گردد</p></div>
<div class="m2"><p>مورد رحمت و امان گردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ره توحید، معرفت باشد</p></div>
<div class="m2"><p>جاده تنگ معدلت باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که توسط میان اضداد است</p></div>
<div class="m2"><p>انحراف از توسط، ایجاد است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شرط این ره شناس در هر حال</p></div>
<div class="m2"><p>التزام صوالح اعمال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شرع و ملت، صراط حق باشد</p></div>
<div class="m2"><p>سالکش بر سماط حق باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون دم تیغ تیز، باریک است</p></div>
<div class="m2"><p>بی چراغ دلیل، تاریک است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیده راگر خدای نور دهد</p></div>
<div class="m2"><p>در ریاضت، دل صبور دهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>می تواند سلوک این ره کرد</p></div>
<div class="m2"><p>خامه این گفت و قصه کوته کرد</p></div></div>