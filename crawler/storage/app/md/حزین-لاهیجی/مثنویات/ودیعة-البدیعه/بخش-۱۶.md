---
title: >-
    بخش ۱۶
---
# بخش ۱۶

<div class="b" id="bn1"><div class="m1"><p>هستی از غیب خویش و از اطلاق</p></div>
<div class="m2"><p>نگذارد چو آفتاب اشراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متزلزل شود به سوی بعید</p></div>
<div class="m2"><p>یفعل اللّه ما یشا و یرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>متنزل شود علی الترتیب</p></div>
<div class="m2"><p>قضی الامر و النصیب یُصیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایهٔ اوّل اشرف و اعلی</p></div>
<div class="m2"><p>دومین از سوم به علم کذا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به آن پایه ای که از خسّت</p></div>
<div class="m2"><p>بنهایت رسد نزول سمت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس از آن سیر در عروج کند</p></div>
<div class="m2"><p>به شرف طیّ آن بروج کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به جایی رسد کزان افضل</p></div>
<div class="m2"><p>نبود حیث کانَ فی الاوّل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بفهمی مقاصد قرآن</p></div>
<div class="m2"><p>ثم لا الله را برخوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پست تر، هر چه دورتر زِ اَحَد</p></div>
<div class="m2"><p>نحن بالذل نعرف الابعد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اوّلین پایه در وجود و بقا</p></div>
<div class="m2"><p>نبود حاجتش به غیر خدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این مقام عقول و انوار است</p></div>
<div class="m2"><p>بو اگر برده ای سخن زار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه فرو خوانمت که بس خامی؟</p></div>
<div class="m2"><p>تو از ایشان شنیده ای نامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>معنی آسان نمی شود حاصل</p></div>
<div class="m2"><p>چه گشاید ز لفظ لاطایل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفته آن زمرهٔ تمام صفا</p></div>
<div class="m2"><p>حسبنا اللّه ربّنا و کفی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دومین پایه ای که نفسانی ست</p></div>
<div class="m2"><p>در تقوم رهین فوقانیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لیک در جملهٔ صفات و فعال</p></div>
<div class="m2"><p>بی کم از خود نماید استقلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سومین پایه با تفاوت قدر</p></div>
<div class="m2"><p>شده بعضی نجوم و برخی بدر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مهوشانِ برازخند، نفوس</p></div>
<div class="m2"><p>که در انظار عالمند عروس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وان ملایک که اهل تدبیرند</p></div>
<div class="m2"><p>هر یکی در دیار خود میرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه زین زمره اند تا دانی</p></div>
<div class="m2"><p>جرعه نوشان فیض یزدانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وان دوم پایه در تقوّم هم</p></div>
<div class="m2"><p>حاجتش با فروتران محکم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این مقام طبایع صور است</p></div>
<div class="m2"><p>دیده ها بازکن که در نظر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چارمین پایه را چو درنگری</p></div>
<div class="m2"><p>جای بیهوشی است و بی خبری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>غیر امکان و قوّه، چیزی نیست</p></div>
<div class="m2"><p>ذوق فعلیت وتمیزی نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>می پذیرد عطای بالا را</p></div>
<div class="m2"><p>می نماید قبول شیا را</p></div></div>