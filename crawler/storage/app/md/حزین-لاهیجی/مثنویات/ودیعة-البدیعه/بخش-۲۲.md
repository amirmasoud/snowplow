---
title: >-
    بخش ۲۲
---
# بخش ۲۲

<div class="b" id="bn1"><div class="m1"><p>اشرف و احسن صحف، قرآن</p></div>
<div class="m2"><p>می نماید ادای حق بیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما نماینده ایم، اسما را</p></div>
<div class="m2"><p>همه ذات و صفات اشیا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تقیّد گرفته تا اطلاق</p></div>
<div class="m2"><p>در مزایای انفس و آفاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شود روشن این که آنچه عیان</p></div>
<div class="m2"><p>شده در دیده های دیده وران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذات حق است در مظاهر خویش</p></div>
<div class="m2"><p>جلوه فرمای نور باهر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهد نور، نور بس باشد</p></div>
<div class="m2"><p>دیده را این ظهور بس باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دمید آفتاب، شک چه بود؟</p></div>
<div class="m2"><p>زر خورشید را محک چه بود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او شناسندهٔ عیار خود است</p></div>
<div class="m2"><p>این انها کار کسی ست، کار خود است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیک از آنجا که شد احاطه مناط</p></div>
<div class="m2"><p>چاره نبود محیط را ز محاط</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز بدینسان، لقای اوست محال</p></div>
<div class="m2"><p>ذاتش از این و آن بود متعال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفته زین رو، که هر طرف به نگاه</p></div>
<div class="m2"><p>قد تولّوا فَثَّمَ وَجهُ الله</p></div></div>