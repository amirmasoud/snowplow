---
title: >-
    بخش ۱۷
---
# بخش ۱۷

<div class="b" id="bn1"><div class="m1"><p>پس نماید عروج را آهنگ</p></div>
<div class="m2"><p>آن نخستین که می پذیرد رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسم و نامش ز جمع اسما شد</p></div>
<div class="m2"><p>جامع صورت و هیولا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود، چون سترد داغ کلف</p></div>
<div class="m2"><p>متشخّص به صورت اشرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اغتذا و نمو پدید آید</p></div>
<div class="m2"><p>آن تخصص چو بر مزید آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حس و جنبش شود هم آغوشش</p></div>
<div class="m2"><p>گه شرنگش دهند و گه نوشش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم از آنجا چو ره گرا گردد</p></div>
<div class="m2"><p>صورتش عین مدّعا گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نطق پیدا کند که انسان شد</p></div>
<div class="m2"><p>نوع افضل ز جنس حیوان شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدّ انسان رهی ست دور و دراز</p></div>
<div class="m2"><p>دایر اندر حقیقت است و مجاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرق هر فرد تا به فرد دگر</p></div>
<div class="m2"><p>کم نباشد ز بعد خیر از شر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به جایی رسد که شاد شود</p></div>
<div class="m2"><p>صاحب عقل مستفاد شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کاملان انتهای معراجند</p></div>
<div class="m2"><p>تارک افتخار را تاجند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد وجود ابتدا به فعل و همان</p></div>
<div class="m2"><p>منتهی شد به فعل در سیران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>افضل اول بود نفوس نزول</p></div>
<div class="m2"><p>در عروج اولین بود مفضول</p></div></div>