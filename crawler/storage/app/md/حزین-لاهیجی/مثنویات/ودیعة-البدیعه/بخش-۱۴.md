---
title: >-
    بخش ۱۴
---
# بخش ۱۴

<div class="b" id="bn1"><div class="m1"><p>هر حقیقت که برگشود جمال</p></div>
<div class="m2"><p>باشدش پایه را ز نقص وکمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کمال از وجود او باشد</p></div>
<div class="m2"><p>تابع و فرع بود او باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست شرحی کمال را حاجت</p></div>
<div class="m2"><p>چون حیات است و دانش و قدرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک هر عین درقبول وجود</p></div>
<div class="m2"><p>متفاوت بود به چشم شهود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مختلف در قبول هستی شد</p></div>
<div class="m2"><p>وین تفاوت علوّ و پستی شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه قابل بود، به وجه اتم</p></div>
<div class="m2"><p>باید اکمل بودکمالش هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باعث و منشأ تفاوت و فرق</p></div>
<div class="m2"><p>هست روشن چو آفتاب از شرق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منشأش بر تو گر چه پنهان است</p></div>
<div class="m2"><p>غلبات وجوب و امکان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکجا غالب است حکم وجوب</p></div>
<div class="m2"><p>شد قبولش وجوب را مرغوب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب آنجا که حکم امکان است</p></div>
<div class="m2"><p>نقص در هستیش فراوان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با خرد کن درآنچه گفتم شور</p></div>
<div class="m2"><p>چون نمودی درین حقیقت غور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر رخت واشود در اسرار</p></div>
<div class="m2"><p>کشف گردد حقایق بسیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر تو ناگفته ها شود روشن</p></div>
<div class="m2"><p>چون خلیل آذرت شود گلشن</p></div></div>