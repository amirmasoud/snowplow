---
title: >-
    شمارهٔ ۸۷۶
---
# شمارهٔ ۸۷۶

<div class="b" id="bn1"><div class="m1"><p>دو خصم داده به هم دست و این فگار یکی</p></div>
<div class="m2"><p>یکی تو دشمن جانیّ و روزگار یکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون من دو زبردست، هم زبان شده اند</p></div>
<div class="m2"><p>نگاه مست یکی، چشم میگسار یکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو فتنه گر به کمین دل رمیدهٔ ماست</p></div>
<div class="m2"><p>کمند طره یکی، زلف تابدار یکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی، دو کرده غمم را فریب وعدهٔ تو</p></div>
<div class="m2"><p>بلای هجر یکی، درد انتظار یکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه در دلی و نه در دیدهٔ خراب مرا</p></div>
<div class="m2"><p>ازین دو خانه، نیامد تو را به کار یکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیم به هجر تو تنها، دو همنشین دارم</p></div>
<div class="m2"><p>دل شکسته یکی، جان بی قرار یکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عندلیب چمن نوبت فغان نرسد</p></div>
<div class="m2"><p>حدیث جورت اگر گویم، از هزار یکی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون دو سلسله جنبان بود جنون مرا</p></div>
<div class="m2"><p>خط عبیر شمیمت یکی، بهار یکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدنگ های تغافل خطا نمی گردد</p></div>
<div class="m2"><p>ز شست غمزه ات، ای نازنین سوار، یکی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گدا و شاه به تنهایی از جهان رفتند</p></div>
<div class="m2"><p>درین دیار، به یاری نشد دچار، یکی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دهر، الفت و انصاف نیست یاران را</p></div>
<div class="m2"><p>یکی حریف نشاط است و سوگوار، یکی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زگرد حادثه میدان روزگار پر است</p></div>
<div class="m2"><p>خدا کند که برآید ازین غبار، یکی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بزم وصل، حزین این قدر خبر دارم</p></div>
<div class="m2"><p>که بیخودانه، سرم داشت درکنار، یکی</p></div></div>