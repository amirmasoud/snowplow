---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>هنوز آغاز رعنایی ست عشق سرکش ما را</p></div>
<div class="m2"><p>فروزان تر کند دامان محشر آتش ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگر خون از خمار بوسهٔ آن لعل میگونم</p></div>
<div class="m2"><p>ازین سر جوش جامی ده، لب دردی کش ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تمناها شهید، از فیض آه بی اثر دارم</p></div>
<div class="m2"><p>فراوان است بسمل، تیر روی ترکش ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خجل شد در امیدش سینه ی چاک و ندانستم</p></div>
<div class="m2"><p>که حسرت، هاله آغوش باشد مهوش ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزین ، از گریه ام چون شمع کاری برنمی آید</p></div>
<div class="m2"><p>که آب دیده نتواند نشاند آتش ما را</p></div></div>