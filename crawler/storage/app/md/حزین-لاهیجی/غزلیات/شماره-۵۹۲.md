---
title: >-
    شمارهٔ ۵۹۲
---
# شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>کرده عشق شعله خوب ریشه در جانم چو شمع</p></div>
<div class="m2"><p>از زبان آتشین خود گدازانم چو شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آستین نبود حریف دیده خونبار من</p></div>
<div class="m2"><p>کز تف دل آتش‌آلود است مژگانم چو شمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست غیر از تیغ، محرابی، سر تسلیم را</p></div>
<div class="m2"><p>می‌خورم صد زخم جانفرسا و خندانم چو شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم از چشم تر خود منت ابر بهار</p></div>
<div class="m2"><p>اشک گرمی می‌کند مژگان به دامانم چو شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو من بخت سیه را کس نمی‌پوشد حزین</p></div>
<div class="m2"><p>با وجود تیره‌روزی‌ها فروزانم چو شمع</p></div></div>