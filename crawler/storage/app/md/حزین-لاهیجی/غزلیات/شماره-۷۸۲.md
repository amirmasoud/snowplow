---
title: >-
    شمارهٔ ۷۸۲
---
# شمارهٔ ۷۸۲

<div class="b" id="bn1"><div class="m1"><p>ای آب خضر، سایهٔ سرو روان تو</p></div>
<div class="m2"><p>آتش به جان، گل از رخ چون ارغوان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محو سبک عنان مژه کافرت شوم</p></div>
<div class="m2"><p>رنگین نشد به خون دو عالم سنان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد به رنگ جوشش پروانه، گرد شمع</p></div>
<div class="m2"><p>دلها به دام طرهٔ عنبرفشان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق، تیر بال هما بود بر سرم</p></div>
<div class="m2"><p>هرگز نداشتم غم جان را به جان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد خط تو برد قرار از دل حزین</p></div>
<div class="m2"><p>این بود، جوش فتنهٔ آخر زمان تو</p></div></div>