---
title: >-
    شمارهٔ ۶۷۸
---
# شمارهٔ ۶۷۸

<div class="b" id="bn1"><div class="m1"><p>در آب دیده یا در سینهٔ پرآذر اندازم</p></div>
<div class="m2"><p>دل بیمار خود را بر کدامین بستر اندازم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان افسرده شد از عشق خون آشام، اشارت کن</p></div>
<div class="m2"><p>که این دل مردگان را در رگ جان نشتر اندازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کف خاکستر تفسیده ام در کار محشر کن</p></div>
<div class="m2"><p>که دوزخ در بهشت و العطش در کوثر اندازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نامهربانت کینهٔ عاشق چرا دارد؟</p></div>
<div class="m2"><p>اگر رسم وفا عیب است از عالم براندازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدح پیمای من، داری اگر ذوق کباب دل</p></div>
<div class="m2"><p>بفرما تا به داغ دوستی بر اخگر اندازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبار دل بود، تا کی، کهن ویرانهٔ دنیا؟</p></div>
<div class="m2"><p>بگو تا کار عالم را به مژگان تر اندازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بساط عشق بازان گرمی هنگامه می خواهد</p></div>
<div class="m2"><p>تو چوگان کن کمند زلف را تا من سراندازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حزین از عشق دارم در رگ جان گرمی خونی</p></div>
<div class="m2"><p>که در شمشیر قاتل پیچ و تاب جوهر اندازم</p></div></div>