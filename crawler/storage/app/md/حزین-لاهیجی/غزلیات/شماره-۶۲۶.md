---
title: >-
    شمارهٔ ۶۲۶
---
# شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>عشق تو ملک خسروی، داغ تو چتر شاهیم</p></div>
<div class="m2"><p>در صف سروران رسد، دعوی کج کلاهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوثر تیغت ار کند، رحم به حال مجرمان</p></div>
<div class="m2"><p>دوزخ جاودان شود، خجلت بیگناهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرنه خوش است خاطرت، با غم سینه کوب من</p></div>
<div class="m2"><p>گوش نمی دهی چرا هیچ به داد خواهیم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نگهی که نرگست کرد به کار عاشقان</p></div>
<div class="m2"><p>صافی لای باده شد، خرقهٔ خانقاهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو حرز جان بود، این همه امتحان چرا؟</p></div>
<div class="m2"><p>گاه در آتش افکنی گاه به کام ماهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه چه چاره کز دلم گرد الم نمی برد</p></div>
<div class="m2"><p>شورش اشک نیم شب، ناله صبحگاهیم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه شکارلاغرم لیک به یمن دل حزین</p></div>
<div class="m2"><p>کشته تیغ ناز آن عربده جو سپاهیم</p></div></div>