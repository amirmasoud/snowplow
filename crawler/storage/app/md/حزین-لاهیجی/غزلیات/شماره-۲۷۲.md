---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>کار دل و خراش، به هم عشق واگذاشت</p></div>
<div class="m2"><p>این عقده را به ناخن مشکل گشا گذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنداشت چون سپند،که میدان آتش است</p></div>
<div class="m2"><p>هرجا به سینه، شعلهٔ داغ تو، پا گذاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صرف لب تو کرد قضا، صاف رنگ و بو</p></div>
<div class="m2"><p>دردی که ماند در قدح غنچه وا گذاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زیر سنگ، سبزه سبک بارتر از اوست</p></div>
<div class="m2"><p>هرکس به دوش، منّت نشو و نما گذاشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گام نخست وحشت مجنون به گرد رفت</p></div>
<div class="m2"><p>راهی که شور عشق، مرا پیش پا گذاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناید برون چو فاخته از طوق بندگی</p></div>
<div class="m2"><p>زلفت ز حلقه ای که به گوش صبا گذاشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبود حزین ، کم از رگ ابر گهر نثار</p></div>
<div class="m2"><p>هر خامه ای که مصرع رنگین به جا گذاشت</p></div></div>