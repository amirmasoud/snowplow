---
title: >-
    شمارهٔ ۴۱۶
---
# شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>لبت به پیرهن تنگ غنچه خار کند</p></div>
<div class="m2"><p>عبیر خطّ تو خون در دل بهار کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب نرگس شوخت شدم که از نگهی</p></div>
<div class="m2"><p>سراسر دو جهان را کرشمه زار کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رود چو موج ز دستش، عنان خودداری</p></div>
<div class="m2"><p>خرام ناز تو آن را که بی قرار کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گسست در خم زلفت کمند تدبیرم</p></div>
<div class="m2"><p>تو را به من کشش دل مگر دچار کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیاه خشک، بهار و خزان چه می داند؟</p></div>
<div class="m2"><p>دگر چه با من افسرده، روزگار کند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز کوتهی دست آرزو باقیست</p></div>
<div class="m2"><p>ز خون کشته من، تیغش ار نگار کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خار خارِ گلی آشیان من قفس است</p></div>
<div class="m2"><p>زمانه با دل تنگم، دگر چکار کند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش آن خزان زده بلبل که در فراق چمن</p></div>
<div class="m2"><p>ز چاک سینهٔ خود گشتِ لاله زار کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهر با همه سامان ترکتاز، حزین</p></div>
<div class="m2"><p>حذر ز ناوک آن طفل نی سوار کند</p></div></div>