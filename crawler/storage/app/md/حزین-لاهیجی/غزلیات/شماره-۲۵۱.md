---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>تا شمع من ز دیدهٔ شب زنده دار رفت</p></div>
<div class="m2"><p>دود از سرم برآمد و اشک از کنار رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پیچ و تاب حلقه آن زلف خم به خم</p></div>
<div class="m2"><p>کاری که کرد، دست و دل من ز کار رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسانه کم کنید که جوشید گریه ام</p></div>
<div class="m2"><p>خوابم کنون ز دیده اختر شمار رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشفته است حلقهٔ شوریدگان، مگر</p></div>
<div class="m2"><p>حرفی از آن دو سلسلهٔ تابدار رفت؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش ز ناله ام به خس آشیان فتاد</p></div>
<div class="m2"><p>خاری که بود از چمنم یادگار، رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگر مگر میم چو سبو، در گلو کنید</p></div>
<div class="m2"><p>دست من از کرشمهٔ ساقی ز کار رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای ساده دل، وفای حریفان نظاره کن</p></div>
<div class="m2"><p>گل ناکشیده ساغر خود را، بهار رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک ره، گذر به خاک نشینان نمی کنی</p></div>
<div class="m2"><p>عمرم چو نقش پا به ره انتظار رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین جان بی نفس چه نوا خیزدت حزین</p></div>
<div class="m2"><p>از ساز نغمهای نتراود، چو تار رفت</p></div></div>