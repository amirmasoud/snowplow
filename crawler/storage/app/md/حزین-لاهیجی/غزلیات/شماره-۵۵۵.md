---
title: >-
    شمارهٔ ۵۵۵
---
# شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>جز خون به بزم ما می نابی ندید کس</p></div>
<div class="m2"><p>غیر از دل برشته کبابی ندید کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیا کدام شیوه، دل آشوب عاشق است؟</p></div>
<div class="m2"><p>روی تو را ز طرف نقابی ندید کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دهر گوشه ای که توان زیستن کجاست؟</p></div>
<div class="m2"><p>اینجا به کام جغد، خرابی ندید کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حیرتم که شادی و غم را مدار چیست؟</p></div>
<div class="m2"><p>لطفی عیان نگشت و عتابی ندید کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز مهر اوکه در دل صدپارهٔ من است</p></div>
<div class="m2"><p>در شیشهٔ شکسته، شرابی ندید کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دل نشد ز چرخ سیه کاسه کامیاب</p></div>
<div class="m2"><p>زین جام سرنگون دم آبی ندید کس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مژگان چو خار در قدم اشک گرم سوخت</p></div>
<div class="m2"><p>آتش فشان چو دیده، سحابی ندید کس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باشد بهشت، صحبت دیوانگان حزین</p></div>
<div class="m2"><p>کز پند عاقلانه عذابی ندید کس</p></div></div>