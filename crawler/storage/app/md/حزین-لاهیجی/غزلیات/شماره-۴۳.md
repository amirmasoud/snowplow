---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>شور دلها بود ترانه ما</p></div>
<div class="m2"><p>نمک دیده ها فسانه ی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست پروردگان صیادیم</p></div>
<div class="m2"><p>قفس ماست، آشیانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر رفعت به عرش می ساید</p></div>
<div class="m2"><p>علم آه عاشقانهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرد افتاده بود صبح ازل</p></div>
<div class="m2"><p>بی خود از باده شبانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یادگار هزار رنگ گل است</p></div>
<div class="m2"><p>خس و خاشاک آشیانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده سودای عشق خانه خراب</p></div>
<div class="m2"><p>چین زلفی، نگار خانهٔ ما!؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در محبت دراز باد حزین</p></div>
<div class="m2"><p>عمر غمهای جاودانه ما</p></div></div>