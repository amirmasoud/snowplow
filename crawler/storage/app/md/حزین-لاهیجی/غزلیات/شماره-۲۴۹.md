---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>گل داغ است که صحرای دلم خرّم ازوست</p></div>
<div class="m2"><p>خون گرم است که ناسور مرا مرهم ازوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه از دوست رسد ناخوش و خوش، خوش باشد</p></div>
<div class="m2"><p>شربت وصل ازو، تلخی هجران هم ازوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقهٔ بندگی عشق به ما ارزانی</p></div>
<div class="m2"><p>که در انگشت سلیمانی ما خاتم ازوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به که تا وعدهٔ دیدار، وفا سازد یار</p></div>
<div class="m2"><p>نگران چشم دل محرم و نامحرم ازوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منّت ابر بهار از رگ مژگان داریم</p></div>
<div class="m2"><p>کشت امید جگر تشنهٔ ما را نم ازوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق کو شد به سرانجام دل آب شده</p></div>
<div class="m2"><p>قفل گنجینهٔ گل، در گره شبنم ازوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهتر آن است که سازم به پریشانی دل</p></div>
<div class="m2"><p>سر آن زلف بنازم که جهان درهم ازوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه صدف گشت پی گوهر عرفان پیدا</p></div>
<div class="m2"><p>احترام ملک و منزلت آدم ازوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طاق ابروی تو، تا قبلهٔ عشّاق شده ست</p></div>
<div class="m2"><p>پشت افلاک به تعظیم دل ما خم ازوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر سودا زدگان زلف تو را نیست چرا؟</p></div>
<div class="m2"><p>مگر آشفتگی خاطر دلها کم ازوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این جواب غزل دلکش سعدی ست حزین</p></div>
<div class="m2"><p>که نی خامهٔ آتش نفسم را دم ازوست</p></div></div>