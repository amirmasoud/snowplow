---
title: >-
    شمارهٔ ۴۹۰
---
# شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>خوشا دمی که مرا دیده از غبار برآید</p></div>
<div class="m2"><p>ز گرد هستیم آن نازنین سوار برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین بس است که خود چاک می زنم به گریبان</p></div>
<div class="m2"><p>ز دست کوته ما بیش ازین چه کار برآید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سرگذشته، به راهت نشسته ایمکه تاکی</p></div>
<div class="m2"><p>نگه به عربده، زان چشم میگسار برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغیر از اینکه به سرگشتگی جهان به سر آری</p></div>
<div class="m2"><p>دگر چه کام دل از دور روزگار برآید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه آتشی ست حزین ، این که در جگر زده عشقت؟</p></div>
<div class="m2"><p>به یک صفیر تو، دود از دل بهار برآید</p></div></div>