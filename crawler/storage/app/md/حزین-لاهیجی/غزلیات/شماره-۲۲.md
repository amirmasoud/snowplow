---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>ز عشق، شور جنون شد، یک از هزار مرا</p></div>
<div class="m2"><p>سواد سنبل خط، شد سیه بهار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وادیی زده عشق تو پنجه در خونم</p></div>
<div class="m2"><p>که شمع، دیدهٔ شیر است، بر مزار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکار بسمل من زندگی ز سر گیرد</p></div>
<div class="m2"><p>اگر رسد به سر آن نازنین سوار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیار عشق بود جلوه گاه شاهد حسن</p></div>
<div class="m2"><p>به دیده سرمه کشد خاک این دیار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سیل حادثه، ویرانه ام چه غم دارد؟</p></div>
<div class="m2"><p>غبار خاطر من سازد استوار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حسرت گل رخسارهٔ سمن بویی</p></div>
<div class="m2"><p>نگه به پیرهن دیده، گشته خار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین اگر خلفی زیب دودمانم نیست</p></div>
<div class="m2"><p>بس است این غزل تازه، یادگار مرا</p></div></div>