---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>چند به غمزه خون کنی خاطر ناشکیب را</p></div>
<div class="m2"><p>بر رگ جانم افکنی، طرّهٔ دلفریب را؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این ستم دگر بود، کز تف خوی گرم تو</p></div>
<div class="m2"><p>گریه به کام دل نشد، عاشق بی نصیب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله به زیر لب گره، چند کنم که می زند</p></div>
<div class="m2"><p>باد بهار دامنی، آتش عندلیب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از اثر تبسّم غنچهٔ ناشکفته اش</p></div>
<div class="m2"><p>بلبل گلستان کند، نوگل من ادیب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنده په زخم من چرا، شور لبت نمی زند؟</p></div>
<div class="m2"><p>از نمک کرشمه ات، نیست خبر، رقیب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست اگر پسند تو، شیوهٔ بی گنه کشی</p></div>
<div class="m2"><p>از گنهم حساب کن، شکوهٔ بی حسیب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مددی کند حزین ، فیض دم مسیح ما</p></div>
<div class="m2"><p>نیم شبی قضا کنم، نالهٔ عندلیب را</p></div></div>