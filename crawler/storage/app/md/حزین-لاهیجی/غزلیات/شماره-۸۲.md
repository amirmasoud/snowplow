---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>نوشیده چمن دردی جام طربش را</p></div>
<div class="m2"><p>با دامن گل پاک نموده ست، لبش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش کرده ام ای دیده به پیوند دل خویش</p></div>
<div class="m2"><p>از سلسله ها، طرّه ٔ عالی نسبش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رهگذر پیرهن ار دیده سفید است</p></div>
<div class="m2"><p>نگذاشته ام دست ز دامان، طلبش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمگین نیم احوالم اگر یار نپرسد</p></div>
<div class="m2"><p>از شمع نپرسیده کسی، تاب و تبش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیرون ز سویدای دل ما نتوان کرد</p></div>
<div class="m2"><p>سودای سیه خانهٔ خال عربش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریاد، که کردند جدا، تلخ دهانم</p></div>
<div class="m2"><p>از سایهٔ نخلی که نچیدم رطبش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگرفت کنار از برم آن ماه سمن بر</p></div>
<div class="m2"><p>کز پردهٔ دل بافته بودم قصبش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کوتهی بخت نباشد ز چه باشد؟</p></div>
<div class="m2"><p>رنجیده ز ما یار و ندانم سببش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دوزخ عشقیم، اگر عشق گناه است</p></div>
<div class="m2"><p>انصاف چه شد شعله فروز غضبش را؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاری به تماشای گل و لاله نداریم</p></div>
<div class="m2"><p>خوش کرده ام از باغ، شراب عنبش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد تیره دل، از تیرگی روز فراقت</p></div>
<div class="m2"><p>بی رحم بگو چون به سر آریم شبش را؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شوریده سر انداخت به صحرای قیامت</p></div>
<div class="m2"><p>دیوانهٔ صحرای تو، شور و شغبش را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی اصل و نسب، بوالبشر ایجاد از آن شد</p></div>
<div class="m2"><p>تا از گهر خویش طرازد حسبش را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شوق تو حزین ، ازکشش کعبهٔ گل نیست</p></div>
<div class="m2"><p>دل کعبهٔ عشق است، نگهدار ادبش را</p></div></div>