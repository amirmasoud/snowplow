---
title: >-
    شمارهٔ ۷۴۹
---
# شمارهٔ ۷۴۹

<div class="b" id="bn1"><div class="m1"><p>خودی بردار از پیش نظر حسن دلارا بین</p></div>
<div class="m2"><p>بکش بر چشم خواب آلود دست، آن چشم شهلا بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی سوزد دلم بر حال دل، مستی تماشا کن</p></div>
<div class="m2"><p>نمی سازد سرم با شور سودا، شور سودا بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بیدادش نکردم شیوه ای، سیر تحمّل کن</p></div>
<div class="m2"><p>ز هجرانش ندارم شکوه ای جان شکیبا بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآ یکشب ز منزل ماه مشتاقان تماشا کن</p></div>
<div class="m2"><p>پریشان یک جهان شوریده و یک شهر شیدا بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دشت سینه ام گشتی بزن، دیر مغان بنگر</p></div>
<div class="m2"><p>به فریاد دلم گوشی بکن، ناقوس ترسا بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذر بر سینه چاکم فکن گلگشت صحرا کن</p></div>
<div class="m2"><p>قدم بگذار بر چشم ترم، آشوب دریا بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رنگین جلوه نازی طلسم هستیم بشکن</p></div>
<div class="m2"><p>درین یک مشت گل چندین هزار آشوب و غوغا بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر بر کشتگانش نیست چشم مست را بنگر</p></div>
<div class="m2"><p>خبر از خستگانش نیست حسن بی محابا بین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غمش در هر سر کویی به خون غلتیده ای دارد</p></div>
<div class="m2"><p>خبر از حسن بی پروا ندارد یار، پروا بین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر خواهی بدانی قدر کوی نیکنامی را</p></div>
<div class="m2"><p>حزین را در خرابات محبت مست و رسوا بین</p></div></div>