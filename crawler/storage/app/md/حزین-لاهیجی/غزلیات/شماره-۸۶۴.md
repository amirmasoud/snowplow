---
title: >-
    شمارهٔ ۸۶۴
---
# شمارهٔ ۸۶۴

<div class="b" id="bn1"><div class="m1"><p>در قید غمم، خاطر آزاد کجایی؟</p></div>
<div class="m2"><p>تنگ است دلم، قوت فریاد کجایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیری ست که دارم سر راه نگهی را</p></div>
<div class="m2"><p>صیدی سر تیر آمده، صیّاد کجایی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون وجود، امن و امان عجبی بود</p></div>
<div class="m2"><p>هستی رهِ ما زد، عدم آباد کجایی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو همنفسی، تا نفسی شاد برآرم؟</p></div>
<div class="m2"><p>مجنون تو کجا رفتی و فرهاد کجایی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیری ست که رفتیّ و ندارم خبر از تو</p></div>
<div class="m2"><p>بازآ، دل آواره، خوشت باد، کجایی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ناوک تأثیر که کردی سفر از دل</p></div>
<div class="m2"><p>می خواست تو را ناله به امداد کجایی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آنکه نیاوردی، یک بار ز ما یاد</p></div>
<div class="m2"><p>ای آنکه نرفتی دمی از یاد کجایی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسوای جهان می کندم، هند جگرخوار</p></div>
<div class="m2"><p>غم پرده در افتاده، دل شاد، کجایی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می خواستی آزرده ببینی دل ما را</p></div>
<div class="m2"><p>اکنون که غمت داد ستم داد، کجایی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم دوشی آن سروقد، اندیشهٔ دوری ست</p></div>
<div class="m2"><p>شرمی بکن، ای جلوهٔ شمشاد کجایی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عشق به یک جلوه، حزین کار تمام است</p></div>
<div class="m2"><p>من برق به خرمن زدم، ای باد کجایی؟</p></div></div>