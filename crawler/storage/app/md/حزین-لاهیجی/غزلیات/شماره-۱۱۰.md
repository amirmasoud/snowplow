---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>نگاه ناز او فهمید، راز سینه جوشی را</p></div>
<div class="m2"><p>رساند آخر به جایی، عشق فریاد خموشی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه پروا گر در میخانه ها را محتسب گل زد؟</p></div>
<div class="m2"><p>نبندد نرگس مستش، دکان می فروشی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قیامت هم سر از خواب پریشان بر نمی دارم</p></div>
<div class="m2"><p>که دارم یادگار طره آشفته هوشی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تغافل شیوهٔ من، گر به فریادم دهد گوشی</p></div>
<div class="m2"><p>کنم نازکتر ازگل، پرده بلبل سروشی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر از سر بگذرد گلزار را خون دل تنگم</p></div>
<div class="m2"><p>لبش چون غنچه نگذارد ز کف، پیمانه نوشی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا داده ست درکیش طریقت شیوه فقرم</p></div>
<div class="m2"><p>من از کتم عدم چون نافه دارم خرقه پوشی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزبن افسانه سنج شمع کلک شعله آشوبم</p></div>
<div class="m2"><p>نیم درآستین می پرورد، آتش خروشی را</p></div></div>