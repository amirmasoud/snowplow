---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>زد حلقه عشق، بر در دولتسرای ما</p></div>
<div class="m2"><p>نقش مراد شد، شکن بوریای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غمزهٔ تو رفت ز خونم فسردگی</p></div>
<div class="m2"><p>جوش نشاط زد، می مرد آزمای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیل عنان گسسته به دنبال می تپد</p></div>
<div class="m2"><p>در وادیی که شوق بود، رهنمای ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون موج پی گسسته زند موجِ اضطراب</p></div>
<div class="m2"><p>خاک از تپیدن دل بی دست و پای ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوابت شد از فسانهٔ زاهد گران، حزین</p></div>
<div class="m2"><p>بشنو نوایی از دل درد آشنای ما</p></div></div>