---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>بیابان مرگ حسرت کرده ای مشت غبارم را</p></div>
<div class="m2"><p>به باد دامنی روشن نما، شمع مزارم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی آید به لب افسانهٔ بخت سیاه من</p></div>
<div class="m2"><p>نگاه سرمه سایی، تیره دارد روزگارم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاهی کن که فارغ گردم از درد سر هستی</p></div>
<div class="m2"><p>بیا ساقی به یک پیمانه می بشکن خمارم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین بستان سرا از سرد مهری، چون گل رعنا</p></div>
<div class="m2"><p>خزان رنگ زردی در میان دارد بهارم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزین از اضطراب دل به کوی یار می ترسم</p></div>
<div class="m2"><p>تپیدنها به باد آخر دهد، مشت غبارم را</p></div></div>