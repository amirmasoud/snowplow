---
title: >-
    شمارهٔ ۸۰۱
---
# شمارهٔ ۸۰۱

<div class="b" id="bn1"><div class="m1"><p>از ما نهان ز فرط ظهوری، چه فایده؟</p></div>
<div class="m2"><p>دایم میان جانی و دوری، چه فایده؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام و لبی کجاست که نوشد شراب تو؟</p></div>
<div class="m2"><p>خود مست و خود شراب طهوری چه فایده؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس چون حریف جلوهٔ هر جایی تو نیست</p></div>
<div class="m2"><p>گه نوری و گه آتش طوری چه فایده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیرم کنند چارهٔ شوریدگان تو</p></div>
<div class="m2"><p>ای نوبهار، مایه شوری چه فایده؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانسوز ناله های حزین بی اثر نبود</p></div>
<div class="m2"><p>از جام حسن مست غروری چه فایده؟</p></div></div>