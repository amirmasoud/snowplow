---
title: >-
    شمارهٔ ۸۹۵
---
# شمارهٔ ۸۹۵

<div class="b" id="bn1"><div class="m1"><p>می گرفتیم به جانان، سر راهی، گاهی</p></div>
<div class="m2"><p>او هم از لطف نهان داشت نگاهی، گاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه عجب گر نگهش داشت سر الفت ما؟</p></div>
<div class="m2"><p>برق را، هست نوازش به گیاهی، گاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو سه روزیست که دزدید نگه، وین عجب است</p></div>
<div class="m2"><p>نه ثوابی زمن آمد، نه گناهی، گاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این قدر هست،که در سختی تاب و تب عشق</p></div>
<div class="m2"><p>درد، می داد به دل رخصت آهی گاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این گران آمده باشد، به دل نازک او</p></div>
<div class="m2"><p>می شود بار به خاطر، پرکاهی، گاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل مسکین چه کند گر نتپد از دهشت؟</p></div>
<div class="m2"><p>ریزد از خوی شهان، خون سپاهی، گاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیک نومید نیم زان نگه بنده نواز</p></div>
<div class="m2"><p>می شود روز، شب بخت سیاهی، گاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر به خاک قدمش لابه کنان می گفتم</p></div>
<div class="m2"><p>نشود تیره ز آهی، چو تو ماهی،گاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گنهم گر چه عظیم است، ببخشای به عشق</p></div>
<div class="m2"><p>شاد گردان دل زارم، به نگاهی، گاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به وفای تو، که از هستی خود بی خبرم</p></div>
<div class="m2"><p>در غم عشق، بود حال تباهی، گاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت خاموش، که محتاج نبوده ست حزین</p></div>
<div class="m2"><p>دعوی عشق، به سوگند و گواهی، گاهی</p></div></div>