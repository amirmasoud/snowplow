---
title: >-
    شمارهٔ ۶۹۹
---
# شمارهٔ ۶۹۹

<div class="b" id="bn1"><div class="m1"><p>جهان را رونق از شادابی گفتار می آرم</p></div>
<div class="m2"><p>زکلک این صفحه را آبی به روی کار می آرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به درد آورده ام پیمانهٔ مستانه گویی را</p></div>
<div class="m2"><p>به رقص افلاک را زین ساغر سرشار می آرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفیر خون چکانم تازه دارد نوبهاران را</p></div>
<div class="m2"><p>چمن را آب و رنگ، از غنچهٔ منقار می آرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برون از گلشنم امّا دماغ حسرت آلودی</p></div>
<div class="m2"><p>در آغوش شکنج رخنهٔ دیوار می آرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس پرورده ام امّا نوایی می زنم گاهی</p></div>
<div class="m2"><p>که مرغان چمن را بر سر گفتار می آرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سراغی می دهم زان یار کنعانی که خوبان را</p></div>
<div class="m2"><p>گریبان پاره چون گل بر سر بازار می آرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تهیدستی مرا شرمنده دارد از چمن پیرا</p></div>
<div class="m2"><p>نهال بید مجنونم، خجالت بار می آرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپند من ندارد برگ و ساز شکوه پردازی</p></div>
<div class="m2"><p>مگر آهی که گاهی بر لب اظهار می آرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کینم جبهه های غمزه خالی گشت و خاموشم</p></div>
<div class="m2"><p>اگر تیغ تغافل می کشی زنهار می آرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حزین آزادی از بام فلک دارد سبک دوشم</p></div>
<div class="m2"><p>غلام همتم، در بندگی اقرار می آرم</p></div></div>