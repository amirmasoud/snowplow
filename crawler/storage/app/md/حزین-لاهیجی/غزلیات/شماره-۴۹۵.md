---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>به سنگ حادثه خونم چو پایمال شود</p></div>
<div class="m2"><p>ز وحشتم رگ خارا، رم غزال شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو طور، بوم و بر من شود تجلی زار</p></div>
<div class="m2"><p>رخت چو شمع پریخانهٔ خیال شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهفته ایم به حیرت ز رشک، نام تو را</p></div>
<div class="m2"><p>میانهٔ لب و دل تا به کی جدال شود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان ز دیدهٔ بلبل دپن چمن باید</p></div>
<div class="m2"><p>هزار جدول خون، تا قدی نهال شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به وعده نام وفا می بری و می ترسم</p></div>
<div class="m2"><p>میانهٔ غم و دل، آشتی ملال شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود ز رخنهٔ لب، آفت قلمرو دل</p></div>
<div class="m2"><p>گرفتنی ست دهانی که هرزه نال شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شود کلید در خلد بی طلب فردا</p></div>
<div class="m2"><p>به عرض حال زبان گسسته لال شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به لب شراب سخن صاف اگر نمی آید</p></div>
<div class="m2"><p>چو من به پرد هٔ دل ریز تا زلال شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حزین ز سینه ی صد چاک دل برون افکن</p></div>
<div class="m2"><p>قفس وبال به مرغ شکسته بال شود</p></div></div>