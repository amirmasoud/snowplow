---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>نیم ز افسردگی عاشق ولی دل یاد او دارد</p></div>
<div class="m2"><p>شرابی نیست امّا این سفال کهنه، بو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن ته جرعه ای کز ناز بر خاک ره افشاندی</p></div>
<div class="m2"><p>هنوزم آرزو خوناب حسرت در گلو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشارت چیست؟ بسپارد به لب یا بشکند در دل؟</p></div>
<div class="m2"><p>خروش دلخراشی بلبل ما درگلو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد طاقت هر شیشه دل، تاب فروغ او</p></div>
<div class="m2"><p>شراب خام سوزی عشق در جام و سبو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین صوفی وشم، دردی کش کوی خراباتم</p></div>
<div class="m2"><p>ز می چون گل هنوز این خرقهٔ صد پاره بو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر انصاف اگر داری، بیا بنمایمت ناصح</p></div>
<div class="m2"><p>که جیب دلق شیخ شهر ما صد جا رفو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر افسانه بگشا از نگاه آشنا رویی</p></div>
<div class="m2"><p>لب خاموش عاشق با تو ذوق گفتگو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم از عمر بی حاصل حزین افسرده خاطر شد</p></div>
<div class="m2"><p>چراغ کلبهٔ ما، آستینی آرزو دارد</p></div></div>