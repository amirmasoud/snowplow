---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>سبز شد خط لبّ یار، بهار است بهار</p></div>
<div class="m2"><p>ای جنون من سرشار، بهار است بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه گو چاک زند زاهد محراب نشین</p></div>
<div class="m2"><p>سر ما و ره خمّار، بهار است بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده بحری ست پر آشوب، جنون است جنون</p></div>
<div class="m2"><p>مژه ابریست گهربار، بهار است بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطربا، ناله جانسوز، که شوری ست به سر</p></div>
<div class="m2"><p>ساقیا، ساغر سرشار، بهار است بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری از زیر پر خویش برون آر حزین</p></div>
<div class="m2"><p>بگشا غنچه ی منقار، بهار است بهار</p></div></div>