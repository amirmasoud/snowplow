---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>اشک چشم من و شراب یکیست</p></div>
<div class="m2"><p>دل گرم من و کباب یکی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر، بحر است و موج در تکرار</p></div>
<div class="m2"><p>ذرّه بسیار و آفتاب یکی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش موهوم کارگاه وجود</p></div>
<div class="m2"><p>صد هزار است و در حساب یکی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کفر و دین را چه فرق، با دوری</p></div>
<div class="m2"><p>نور و ظلمت چو شد حجاب یکی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشکن از بوسه ای خمار حزین</p></div>
<div class="m2"><p>لب لعل تو و شراب یکی ست</p></div></div>