---
title: >-
    شمارهٔ ۸۰۰
---
# شمارهٔ ۸۰۰

<div class="b" id="bn1"><div class="m1"><p>نسرین بری گلگون قبا، از جلوه جانم سوخته</p></div>
<div class="m2"><p>سودای مشکین طرّه اش سود و زیانم سوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگ سفر روی وطن، دیگر ندارم هیچ یک</p></div>
<div class="m2"><p>پرواز بالم ریخته، برق آشیانم سوخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شمع سودای کسی، می سوزد آتش در سرم</p></div>
<div class="m2"><p>نام محبت برده ام، کام و زبانم سوخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک دمادم از نظر، بارم، به خون ز آن غرقه ام</p></div>
<div class="m2"><p>دریای آتش در جگر دارم، از آنم سوخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقص عیار من حزین نبود اگر افغان کنم</p></div>
<div class="m2"><p>در بوتهٔ هجران او تاب و توانم سوخته</p></div></div>