---
title: >-
    شمارهٔ ۵۱۲
---
# شمارهٔ ۵۱۲

<div class="b" id="bn1"><div class="m1"><p>من چشمم و عالم همه خار است ببینید</p></div>
<div class="m2"><p>چشمی که به خارش سر و کار است ببینید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نشود پی نفس سوخته را گم</p></div>
<div class="m2"><p>دل تا لب من آبله دار است ببینید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نرگس او دیده وران مست و خرابند</p></div>
<div class="m2"><p>این نشئه که در جام خمار است ببینید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردیده زره، پوست براندام شهیدان</p></div>
<div class="m2"><p>مژگان کسی دشنه گذار است ببینید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخشیده خط سبز که، تشریف قبولش؟</p></div>
<div class="m2"><p>این حله که بر دوش بهار است ببینید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر برگ خزان، دفتر صد رنگ گشاده ست</p></div>
<div class="m2"><p>طراح بهاران به چه کار است ببینید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجت به گواهی نبود قتل حزین را</p></div>
<div class="m2"><p>دستی که ز خونش به نگار است ببینید</p></div></div>