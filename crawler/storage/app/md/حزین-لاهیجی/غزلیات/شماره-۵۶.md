---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>ای سلسلهٔ زلف تو بر پای دل ما</p></div>
<div class="m2"><p>سودایی خال تو، سویدای دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد به گریبان تمنا، گل امّید</p></div>
<div class="m2"><p>از خار رهت، آبلهٔ پای دل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون برگ خزان دیده به هم ربط نگیرد</p></div>
<div class="m2"><p>از بس که ز هم ریخته اجزای دل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونین جگر لالهٔ صحرای تو لیلی</p></div>
<div class="m2"><p>داغ تو، سیه خانهٔ صحرای دل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشود ز گردن رگ جان و نگشاید</p></div>
<div class="m2"><p>زنّار سر زلف تو، ترسای دل ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگشای حزین ، پرده ازین ساز که سازد</p></div>
<div class="m2"><p>از ناله، نیِ کلک تو احیای دل ما</p></div></div>