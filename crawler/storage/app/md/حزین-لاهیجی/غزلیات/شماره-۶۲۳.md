---
title: >-
    شمارهٔ ۶۲۳
---
# شمارهٔ ۶۲۳

<div class="b" id="bn1"><div class="m1"><p>عشاق تو را قافله سالار نگردیم</p></div>
<div class="m2"><p>تا کشتهٔ مژگان سپهدار نگردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نرگس مخمور تو ای شور قیامت</p></div>
<div class="m2"><p>مستیم و چنان مست که هشیار نگردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانا نظر پاک تر از آینه داریم</p></div>
<div class="m2"><p>ظلم است که ما محرم دیدار نگردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ناصیهٔ طالع ما نقش مرادی ست</p></div>
<div class="m2"><p>آن نیستکه خاک قدم یار نگردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا سر نشود خاک سر کوی تو ما را</p></div>
<div class="m2"><p>در خیل شهیدان تو سردار نگردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تسلیم نماییم در اول نگهت جان</p></div>
<div class="m2"><p>پروانه صفت گرد تو بسیار نگردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محو تو چنانیم که خون ریز نگاهت</p></div>
<div class="m2"><p>گر بگذرد از سینه، خبردار نگردیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ویرانهٔ عشق است حزین جان و دل ما</p></div>
<div class="m2"><p>شرمنده ی غمهای وفادار نگردیم</p></div></div>