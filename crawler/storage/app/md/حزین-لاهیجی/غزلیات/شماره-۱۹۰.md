---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>شمع سان با تو شبم رفت و تمنا مانده ست</p></div>
<div class="m2"><p>همه تن صرف نظر گشت و تماشا مانده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به امیدی که فتد در دل برقی رحمی</p></div>
<div class="m2"><p>خرمن ما گره خاطر صحرا مانده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح محشر شد و افسانهٔ زلفش باقی ست</p></div>
<div class="m2"><p>شب درین قصه به سر رفت و سخنها مانده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره عشق هنوزم سر سودا باقی ست</p></div>
<div class="m2"><p>دستم ار گشته تهی، آبلهٔ پا مانده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشئهٔ باده دهد، ذکر مدامی که مراست</p></div>
<div class="m2"><p>رشتهٔ سبحه ام از پنبهٔ مینا مانده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن حسن، ملامت کش آلایش نیست</p></div>
<div class="m2"><p>یوسف آزاده و تهمت به زلیخا مانده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بی طاقتی از عشق به جا مانده حزین</p></div>
<div class="m2"><p>خاطر نازکی از باده به مینا مانده ست</p></div></div>