---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>چون گرد باد حیرت، از خود رهاند ما را</p></div>
<div class="m2"><p>سرگشتگی به جایی، آخر رساند ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار ترم که بارم، بر دوش باغ و گلبن</p></div>
<div class="m2"><p>دهقان بی مروت، بیجا دماند ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسایشی که دیدم، از چشم خون فشان بود</p></div>
<div class="m2"><p>مژگان تر به بالین، گل می فشاند ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد طفل مکتب ما، دوشیزگان معنی</p></div>
<div class="m2"><p>تا عشق سالخورده، فرزند خواند ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک مراد بخشید، کامی که دل هوس داشت</p></div>
<div class="m2"><p>در خاطر از دو عالم، حسرت نماند ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر فرش سنبل و گل، بودم حزین خرامان</p></div>
<div class="m2"><p>چون داغ لاله در خون، هجران نشاند ما را</p></div></div>