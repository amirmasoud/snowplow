---
title: >-
    شمارهٔ ۵۳۷
---
# شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>بی تو در پیرهن نامیه، خار است بهار</p></div>
<div class="m2"><p>چشم مخمور تو را گرد و غبار است بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تمنّای تو ای نسترن آرای بهشت</p></div>
<div class="m2"><p>پای تا سر همه آغوش و کنار است بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که دنبال تو ای سرو خرامان گشتهست</p></div>
<div class="m2"><p>پایش از شبنم گل آبله دار است بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ ازو، بوی از او، حسن و لطافت همه او</p></div>
<div class="m2"><p>بیخود از جلوه ی آن لاله عذار است بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکیه بر بستر نسرین و سمن نتواند</p></div>
<div class="m2"><p>بس که از دست غمت زار و نزار است بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن قدر نیست که گل ساغر می را بکشد</p></div>
<div class="m2"><p>حیف و صد حیف که بی صبر و قرار است بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو رعنای مرا حلّه طراز است چمن</p></div>
<div class="m2"><p>ماه زیبای مرا آینه دار است بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنچه در پوست نگنجید ز تأثیر نسیم</p></div>
<div class="m2"><p>زاهد از خرقه برون آی، بهار است بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعله خوی تو حزین آفت گلزار نگشت</p></div>
<div class="m2"><p>جگرش داغ از آن لاله عذار است بهار</p></div></div>