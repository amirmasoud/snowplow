---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>سحاب خامه من جز در خوشاب ندارد</p></div>
<div class="m2"><p>سفینهٔ غزلم موجهٔ سراب ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیقراری هجران رسد نوید وصالم</p></div>
<div class="m2"><p>در امید بود دیده ای که خواب ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پرده داری ابر نقاب شکوه ندارم</p></div>
<div class="m2"><p>کتان طاقت من تاب ماهتاب ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشوده است به راه نگه چو آینه آغوش</p></div>
<div class="m2"><p>گشاده رویی حسن تو آفتاب ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کدام کار دل از برق جلوه تو برآید؟</p></div>
<div class="m2"><p>چراغ عمرکسی اینقدر شتاب ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنان کشیده تر افغان کن ای جنون زده بلبل</p></div>
<div class="m2"><p>کدام گل به چمن پای در رکاب ندارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همین قدر ز تو باید که دیده ای به کف آری</p></div>
<div class="m2"><p>کدام روزنه، راهی به آفتاب ندارد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلند نشئه حزین ، ازکدام رطل گرانی؟</p></div>
<div class="m2"><p>سیاه مستی کلک تو را شراب ندارد</p></div></div>