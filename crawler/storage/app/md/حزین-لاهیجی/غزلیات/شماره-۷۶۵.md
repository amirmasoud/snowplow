---
title: >-
    شمارهٔ ۷۶۵
---
# شمارهٔ ۷۶۵

<div class="b" id="bn1"><div class="m1"><p>جانا میاموز، فارغ نشستن</p></div>
<div class="m2"><p>باید دلی را از غمزه خستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار ربزد آزادی اش خون</p></div>
<div class="m2"><p>صیدی که آموخت از دام جستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وادی عشق گام نخست است</p></div>
<div class="m2"><p>از جان گذشتن، از جسم رستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سبحه گیرم بر کف؟ که ننگ است</p></div>
<div class="m2"><p>آلودگان را، زنار بستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در راه عشقت کار حزین است</p></div>
<div class="m2"><p>از خویش رفتن بیخود نشستن</p></div></div>