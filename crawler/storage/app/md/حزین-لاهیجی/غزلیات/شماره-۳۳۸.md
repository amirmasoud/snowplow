---
title: >-
    شمارهٔ ۳۳۸
---
# شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>قاصدی کو که پیامی، بر دلدار برد؟</p></div>
<div class="m2"><p>سوی گلشن، خبر مرغ گرفتار برد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسفی کو که به گلبانگ خریداری خویش</p></div>
<div class="m2"><p>سینه چاکم، چو گل از خانه به بازار برد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قوّتی داده به فرهاد و به مجنون ضعفی</p></div>
<div class="m2"><p>هرکه را عشق، ز راهی به سر کار برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عکس خواهش ز بس از مردم دنیا دیدم</p></div>
<div class="m2"><p>جوهر آینه ام، حسرت زنگار برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر مشاطگی چهرهٔ گل باد صبا</p></div>
<div class="m2"><p>بویی از پیرهنت، جانب گلزار برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که چون نقش قدم محو سراپای توام</p></div>
<div class="m2"><p>رشک بر حیرت من، صورت دیوار برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار دل رفت ز دست از غم ایّام حزین</p></div>
<div class="m2"><p>جلوهٔ عشوه گری کو که دل از کار برد؟</p></div></div>