---
title: >-
    شمارهٔ ۴۳۸
---
# شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>چشمت چرا حریف شرابم نمی کند؟</p></div>
<div class="m2"><p>اریک دو جرعه مست و خرابم نمی کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ماهیم که از تف عشق تو سینه ام</p></div>
<div class="m2"><p>دریای آتش است و کبابم نمی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسودهٔ فسانهٔ شوریده مغزیم</p></div>
<div class="m2"><p>غوغای حشر چارهٔ خوابم نمی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهرم، که باد را به چراغم گذار نیست</p></div>
<div class="m2"><p>چرخم که سیل فتنه خرابم نمی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل چراست این همه ساقی ز کار من؟</p></div>
<div class="m2"><p>افشرده است و باده ی نابم نمی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محرومتر مباد کس از من به عاشقی</p></div>
<div class="m2"><p>رنجیده آن نگاه و عتابم نمی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه خار رهگذار و نه خاک قدم حزین</p></div>
<div class="m2"><p>آن سرگران به هیچ حسابم نمی کند</p></div></div>