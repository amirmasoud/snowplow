---
title: >-
    شمارهٔ ۴۶۹
---
# شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>عشق آمد و از سینهٔ من دود برآورد</p></div>
<div class="m2"><p>گلزار خلیل، آتش نمرود برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آه سریع الاثر خویش چه گویم؟</p></div>
<div class="m2"><p>جانی که به لب بود مرا زود برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاقوت صفت دود نبود آتش ما را</p></div>
<div class="m2"><p>دود از دلم آن لعل خط آلود برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیغمبر حُسنیّ و کتاب الله خطّت</p></div>
<div class="m2"><p>اسرار که در پرده نهان بود برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا رنجه حزین از ستم عشق نگردی</p></div>
<div class="m2"><p>ایّام تو را حادثه فرسود، برآورد</p></div></div>