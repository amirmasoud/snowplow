---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>دمیدن از سمنش مشک ناب نزدیک است</p></div>
<div class="m2"><p>به شب نهان شدن آفتاب نزدیک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم ز وعده بر آتش فکندی و رفتی</p></div>
<div class="m2"><p>بیا که سوختن این کباب نزدیک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس شمرده زدنهای صبح روشندل</p></div>
<div class="m2"><p>کنایتی ست که روز حساب نزدیک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش است ساقی اگر مستیی گذاره کنم</p></div>
<div class="m2"><p>گذشتن گل پا در رکاب نزدیک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عمر با تک و تاز نفس مباش ایمن</p></div>
<div class="m2"><p>که راه دور، به پای شتاب نزدیک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فسانه ای ز هوس های نفس دون کافی ست</p></div>
<div class="m2"><p>دل فسردهٔ جاهل، به خواب نزدیک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از شکنجهٔ هستی غمین مدار حزین</p></div>
<div class="m2"><p>گشادِ عقدهٔ کار حباب نزدیک است</p></div></div>