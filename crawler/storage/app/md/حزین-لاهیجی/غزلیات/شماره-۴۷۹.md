---
title: >-
    شمارهٔ ۴۷۹
---
# شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>اگر دست مرا ساقی به یک رطل گران گیرد</p></div>
<div class="m2"><p>الهی در جهان کام دل از بخت جوان گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سعادتمند را باشد گوارا، سختی عالم</p></div>
<div class="m2"><p>هما را در گلو هرگز ندیدم، استخوان گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه سان در سینه ام جا می تواند کرد، حیرانم</p></div>
<div class="m2"><p>خدنگت را که دل از خانهء تنگ کمان گیرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش شمع رویت منصب پروانگی دارم</p></div>
<div class="m2"><p>تو چون عارض برافروزی، مرا آتش به جان گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی را هر قدر دل شهره باشد در جگر داری</p></div>
<div class="m2"><p>سر ره چون به آن بیگانه خوی سرگران گیرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گداز شرم یکسرژاله سازد نرگسستان را</p></div>
<div class="m2"><p>نظر چون کام خاطر، زان جبین خوی فشان گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین از پای ننشینم به راه انتظار او</p></div>
<div class="m2"><p>چو مجنون بر سر شوریده گر مرغ آشیان گیرد</p></div></div>