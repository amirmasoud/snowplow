---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>از مزرع آمال چه امّید برآید</p></div>
<div class="m2"><p>نخلی که در آن ریشه کند، بید برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه جلوهٔ برقی، نه هواداری ابری</p></div>
<div class="m2"><p>بی برگ گیاهم به چه امّید برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی فیض تر از میکدهٔ ماه صیامم</p></div>
<div class="m2"><p>تا از افق جام، مه عید برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جام کند جلوه گری در کف ساقی</p></div>
<div class="m2"><p>بانگ طرب از دخمهٔ جمشید برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد سخنی درگره گوشهٔ ابرو</p></div>
<div class="m2"><p>مقصود از این بیت به تعقید برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساغر چو زند، شیشهٔ گردون شکند می</p></div>
<div class="m2"><p>ساقی چو شود، جام به جمشید برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما راست حزین ، سرو ریاض دل حیران</p></div>
<div class="m2"><p>آزاده جوانی که به تجرید برآید</p></div></div>