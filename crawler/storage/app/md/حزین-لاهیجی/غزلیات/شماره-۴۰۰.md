---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>حریفان هر که را دیدیم در دل کلفتی دارد</p></div>
<div class="m2"><p>بنازم شیشهٔ می را که صافی طینتی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عبث بر دوش آزادی کشیدم رخت هستی را</p></div>
<div class="m2"><p>ندانستم که بار زندگانی منتی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال نرگسش پیمانه پیما بود در خوابم</p></div>
<div class="m2"><p>هنوز از بادهٔ دوشینه دل کیفیتی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملامت در قمار عشق نبود پاکبازان را</p></div>
<div class="m2"><p>غم دنیا و دینش نیست هرکس همّتی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بقایی نیست چون گل، نوبهار شادکامی را</p></div>
<div class="m2"><p>چو عمر سست پیمان دشمن کم فرصتی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اثر در انجمن نگذاشت حسنت از نظر بازان</p></div>
<div class="m2"><p>همین آیینه بر دیوار، پشت حیرتی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بزم اختلاط چرخ چون تیر از کمان جستم</p></div>
<div class="m2"><p>بساط الفت بیگانه کیشان وحشتی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث ما شنو،گر قصهٔ عالی سند خواهی</p></div>
<div class="m2"><p>غلط افسانه لیلی و مجنون شهرتی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو من بر خوان غم داند کسی کز سیر چشمان شد</p></div>
<div class="m2"><p>که خون دل ز نعمتهای الوان لذتی دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرت گردم چرا حالم نمی پرسی، نمی گویی؟</p></div>
<div class="m2"><p>که زنار سر زلفم، برهمن سیرتی دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو غمخواران کند از درد بی دردی سپرداری</p></div>
<div class="m2"><p>همانا دودمان داغ، با دل نسبتی دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طرب خیز است هر تار رگم چون چنگ، پنداری</p></div>
<div class="m2"><p>کف شوقم به دامان وصالش وصلتی دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز گلزار محبت چون روم با کیسهٔ خالی؟</p></div>
<div class="m2"><p>به جیب از گل عذاران، لاله داغ حسرتی دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلم در حلقهٔ زلف تو جمع است از پریشانی</p></div>
<div class="m2"><p>شبستان خیال زلف، خواب راحتی دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حزین آشوبگاه زهد و رندی را وداعی کن</p></div>
<div class="m2"><p>همین دارالامان بیخودی، امنیتی دارد</p></div></div>