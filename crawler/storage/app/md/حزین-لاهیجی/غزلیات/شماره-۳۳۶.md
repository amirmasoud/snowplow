---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>در دیدهٔ من غیر رخ یار نگنجد</p></div>
<div class="m2"><p>در آینه جز پرتو دیدار نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او گرم عتاب است و مرا غم که مبادا</p></div>
<div class="m2"><p>در حوصله ام این همه آزار نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد که غمهای تو ز اندازه برون است</p></div>
<div class="m2"><p>ترسم همه در سینه به یکبار نگنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از طرز سخن ساز نگاه تو، شنیدم</p></div>
<div class="m2"><p>آن راز که در پردهٔ اظهار نگنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان بیخود و مستیم که هرگز می توحید</p></div>
<div class="m2"><p>در جام دل مردم هشیار نگنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما چون خم می، رند خرابات نشینیم</p></div>
<div class="m2"><p>در مجلس ما زاهد دیندار نگنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد تو و فردوس، که سرمست محبّت</p></div>
<div class="m2"><p>جز در صف رندان گنه کار نگنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرمست حزین از می منصوری عشق است</p></div>
<div class="m2"><p>شوریده سرش، جز به سردار نگنجد</p></div></div>