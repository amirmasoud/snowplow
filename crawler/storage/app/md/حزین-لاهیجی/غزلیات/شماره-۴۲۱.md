---
title: >-
    شمارهٔ ۴۲۱
---
# شمارهٔ ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>ساقی بگو چکیدهٔ دل در سبو کنند</p></div>
<div class="m2"><p>تا صاف مشربان به خرابات رو کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو از هوس بتاب که مردان راه عشق</p></div>
<div class="m2"><p>محراب طاعت از دل بی آرزو کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کارگاه عشق حریفان سینه چاک</p></div>
<div class="m2"><p>از تار ماهتاب، کتان را رفو کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دفع خمار نرگس خوبان نمی شود</p></div>
<div class="m2"><p>خون مرا چو باده اگر در سبو کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سازند مشکبو دهن زخم را حزین</p></div>
<div class="m2"><p>حسرت کشان اگر گل داغ تو بو کنند</p></div></div>