---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>تیغت به سرم خمار نگذاشت</p></div>
<div class="m2"><p>حسرت به دل فگار نگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر مژه در گهر نثاری</p></div>
<div class="m2"><p>ما را ز تو شرمسار نگذاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادیم که گریه های مستی</p></div>
<div class="m2"><p>بر خاطر ما غبار نگذاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن سبزهٔ خط و آن بناگوش</p></div>
<div class="m2"><p>ناموس گل و بهار نگذاشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ دل خسته را به مرهم</p></div>
<div class="m2"><p>آن طرهٔ مشک بار نگذاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دوش و برم ردای تقوی</p></div>
<div class="m2"><p>آن نرگس میگسار نگذاشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر لوح دلم ز غیر نقشی</p></div>
<div class="m2"><p>یاد تو به یادگار نگذاشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیداد تغافلت مرا کشت</p></div>
<div class="m2"><p>با خنجر غمزه، کار نگذاشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان نذر وصال کرده بودم</p></div>
<div class="m2"><p>هجران ستیزه کار نگذاشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر بر قدمت نهاده بودم</p></div>
<div class="m2"><p>افسوس که روزگار نگذاشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یادت دل و دیدهٔ حزین را</p></div>
<div class="m2"><p>شرمندهٔ انتظار نگذاشت</p></div></div>