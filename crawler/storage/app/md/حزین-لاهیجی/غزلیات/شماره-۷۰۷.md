---
title: >-
    شمارهٔ ۷۰۷
---
# شمارهٔ ۷۰۷

<div class="b" id="bn1"><div class="m1"><p>می گریزم ز جهان بار چرا بردارم؟</p></div>
<div class="m2"><p>سر درین معرکه اندازم و پا بردارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بویگل نیستم از بارگران جانیها</p></div>
<div class="m2"><p>تا پی قافلهٔ باد صبا بردارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گره از خاطر اگر گریه کند باز چرا</p></div>
<div class="m2"><p>منت بیهده از عقده گشا بردارم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیرتم تکیه به دیوار که گیرد که هنوز</p></div>
<div class="m2"><p>گر بود کوه به این پشت دو تا بردارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناتوانم ولی آن مایه نفس هست حزین</p></div>
<div class="m2"><p>کآسمان را به یکی ناله ز جا بردارم</p></div></div>