---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>عشقت آمیخت به دل درد فراوانی را</p></div>
<div class="m2"><p>ریخت در پیرهنم، خار بیابانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام پروانه مکن یاد که نسبت نبود</p></div>
<div class="m2"><p>با من سوخته دل، سوخته دامانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه خواهی بکن، از دوری دیدار مگو</p></div>
<div class="m2"><p>وحشت آباد مکن خاطر ویرانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق در دل چه خیال است که پنهان گردد؟</p></div>
<div class="m2"><p>پرده پوشی نتوان، آتش سوزانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه آسودهٔ خاک است برآید چو سپند</p></div>
<div class="m2"><p>آه اگر شرح دهم گرمی جولانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نازم آشفتگی عشق که خوش می سازد</p></div>
<div class="m2"><p>بخت شوریده سرم، طرّه پریشانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستم از دامن دلدار جدا ماند حزین</p></div>
<div class="m2"><p>چه کنم گر نکنم پاره، گریبانی را؟</p></div></div>