---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>از وصل، دل بی سر و پا را که خبر کرد؟</p></div>
<div class="m2"><p>در خلوت خورشید، سها را که خبر کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بودم و او فارغ از اندیشهٔ غیری</p></div>
<div class="m2"><p>اینجا ادب ناصیه سا را که خبر کرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاد است به جان دادنم از محنت هجران</p></div>
<div class="m2"><p>از حال من آن شوخ بلا را که خبر کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوری عجب افکنده به دلهای پریشان</p></div>
<div class="m2"><p>در پردهٔ زلف تو صبا را که خبر کرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس نیست حزین ، پرسد از احوال غریبان</p></div>
<div class="m2"><p>در ماتم ما، مهر و وفا را که خبر کرد؟</p></div></div>