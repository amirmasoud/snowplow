---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>بزمی که مست ناز مرا جلوه گاه بود</p></div>
<div class="m2"><p>بادام چشم، نقل شراب نگاه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مأوای حادثات، شبستان زندگی ست</p></div>
<div class="m2"><p>فانوس شمع ما نفس صبحگاه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مفتی ناز، کرد جفا را چرا حلال؟</p></div>
<div class="m2"><p>در ملّتی که شکوهٔ عاشق گناه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت میان حسن و محبّت چنین خوش است</p></div>
<div class="m2"><p>با ما نگاه گرم تو برق و گیاه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشن نگشت چشم حزین از جمال تو</p></div>
<div class="m2"><p>روزش تمام چون شب زلفت سیاه بود</p></div></div>