---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>مجنون مرا شور تو بی پا و سر انداخت</p></div>
<div class="m2"><p>کوه غم عشق تو مرا از کمر انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل که به کویت رسد این رنگ پریده</p></div>
<div class="m2"><p>سیمرغ درین راه خطرناک، پرانداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چشم سیه مست تو عاشق کشی آموخت</p></div>
<div class="m2"><p>از هر دو جهان، قاعدهٔ داد برانداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک درت پارهٔ دل ریخت سرشکم</p></div>
<div class="m2"><p>در کوی تو این قافله، بار سفر انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچون جرس افسانه فروش است خروشم</p></div>
<div class="m2"><p>بیتابی دل آه مرا از اثر انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از زخم شود جوهر شمشیر، نمایان</p></div>
<div class="m2"><p>دانست تو را هر که به حالم نظر انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بوسهٔ آن حسن گلوسوز چه باشد</p></div>
<div class="m2"><p>نام لب تو، کام مرا در شکر انداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشناخته بودیم دری غیر در دل</p></div>
<div class="m2"><p>ما را به چه تقصیر، فلک در به در انداخت؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عشق ندانم که وفا چون و جفا چیست</p></div>
<div class="m2"><p>این درد گرانمایه، مرا بی خبر انداخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خلوتیان الحذر از عشق فسونگر</p></div>
<div class="m2"><p>ما را به زبان همه کس چون خبر انداخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق است حزین ، فاش بگویم که بدانند</p></div>
<div class="m2"><p>این شعله، که در خرمن جانم شرر انداخت</p></div></div>