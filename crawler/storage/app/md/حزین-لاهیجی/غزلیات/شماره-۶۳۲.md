---
title: >-
    شمارهٔ ۶۳۲
---
# شمارهٔ ۶۳۲

<div class="b" id="bn1"><div class="m1"><p>ز خود دور آن دلارا را نمی دانم نمی دانم</p></div>
<div class="m2"><p>جدا از موج، دریا را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمید از مشرق هر ذره ای، سر زد ز هر خاری</p></div>
<div class="m2"><p>نهان آن نور پیدا را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبالب از می دیدار بینم آسمانها را</p></div>
<div class="m2"><p>حجاب باده، مینا را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم جمله ذرات جهان هم سنگ می آید</p></div>
<div class="m2"><p>عیار لعل و خارا را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریب وعدهٔ امروز و فردا کار نگشاید</p></div>
<div class="m2"><p>که من امروز و فردا را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرت گردم زبان من شو و با من حکایت کن</p></div>
<div class="m2"><p>بیان رمز و ایما را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهانی تا به کی در پرده با دل نکته می سنجی</p></div>
<div class="m2"><p>اشارتهای پیدا را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر جرمی مگیر، ارشاد کن، بیگانهٔ کیشم</p></div>
<div class="m2"><p>هنوز آیین ترسا را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا و در عوض بپذیر از من شیوهء رندی</p></div>
<div class="m2"><p>رسوم زهد و تقوا را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گر خواهی صمد ، خواهی صنم ، ره گم نمی گردد</p></div>
<div class="m2"><p>ز اسما جز مسما را نمی دانم نمی دانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حزین ، جایی که دارد در بغل هر ذرّه خورشیدی</p></div>
<div class="m2"><p>نزاع شیخ و ملّا را نمی دانم نمی دانم</p></div></div>