---
title: >-
    شمارهٔ ۶۵۶
---
# شمارهٔ ۶۵۶

<div class="b" id="bn1"><div class="m1"><p>اگر من بیستون عشق را تعمیر می کردم</p></div>
<div class="m2"><p>به آهی سنگ را چون سینه ناخن گیر می کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر همّت ز من می خواست دل های سحرخیزان</p></div>
<div class="m2"><p>دم گرمی به کار آه بی تاثیر می کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی ز اندیشه فارغ داشتم در می پرستیها</p></div>
<div class="m2"><p>به یک ساغر علاج عقل پرتزوبر می کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد حسن لیلی چون من، از خود رفته مجنونی</p></div>
<div class="m2"><p>سواد زلف او می گفتم و شبگیر می کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یاد زلف مشکینش من شوریده سر، شبها</p></div>
<div class="m2"><p>مسلسل قصه ای در حلقهٔ زنجیر می کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل عاشق سخن، میشد اگر یک ره دچار من</p></div>
<div class="m2"><p>حکایت ها از آن مژگان خوش تقریر می کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین گر می گشودم پرده از کار جم و جامش</p></div>
<div class="m2"><p>دل دنیاپرستان را، ز عالم سیر می کردم</p></div></div>