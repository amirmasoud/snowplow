---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>درکشوری که مهر و وفا می فروختند</p></div>
<div class="m2"><p>خوبان، متاع جور و جفا می فروختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بیعگاه خنجر ناز نگاه او</p></div>
<div class="m2"><p>جان، قدسیان به نرخ گیا می فروختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من زان ولایتم که به یک جو نمی خرید</p></div>
<div class="m2"><p>شاهنشهی اگر به گدا می فروختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ننگ آیدش وگر نه مکرر به التماس</p></div>
<div class="m2"><p>دولت به رند بیسر وپا می فروختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواری کشان کوی خرابات از غرور</p></div>
<div class="m2"><p>چین جبین به بال هما می فروختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل می دمید یکسر ازین دشت آتشین</p></div>
<div class="m2"><p>خاری اگر به آبله پا می فروختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دون همّتان سفله شعار جهان حزین</p></div>
<div class="m2"><p>ما را چه می شدی که به ما می فروختند؟</p></div></div>