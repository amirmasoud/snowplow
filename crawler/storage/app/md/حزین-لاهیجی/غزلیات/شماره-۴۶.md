---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>تا شفقی کرده ای رخ نمکین را</p></div>
<div class="m2"><p>گل عرق آلود شرم کرده جبین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وحشت دلهای آرمیده عجب نیست</p></div>
<div class="m2"><p>غمزهٔ صید افکنت گشاده کمین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده خرابات، چشم باده پرستت</p></div>
<div class="m2"><p>خاطر پاک هزار گوشه نشین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرش برین شد زمین،که رفعت کویت</p></div>
<div class="m2"><p>قاعده بر هم زد، آسمان و زمین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چه حریفم که ازتطاول زلفت</p></div>
<div class="m2"><p>متقیان باختند ملت و دین را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل نشود چون ز تاب رشک گزیده؟</p></div>
<div class="m2"><p>مور خط افتاده آن لب شکرین را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در صف بزم تو نیست حاجت مطرب</p></div>
<div class="m2"><p>زمزمه گرم است ناله های حزین را</p></div></div>