---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>بهار شد که چمن جام ارغوان گیرد</p></div>
<div class="m2"><p>ز جوش سبزه زمین رنگ آسمان گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به طرف باغ بساط زمردی فکنند</p></div>
<div class="m2"><p>ز لاله برهمن خاک، طیلسان گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دوش نامیه دیبای بهمنی فکنند</p></div>
<div class="m2"><p>ز غنچه تارک شاخ، افسر کیان گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا ز جیب سمن بوی پیرهن آرد</p></div>
<div class="m2"><p>نشان نکهت گل، گرد کاروان گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آفتاب زند خیمه لاله در هامون</p></div>
<div class="m2"><p>سحاب بر سر کهسار سایبان گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مغنی، از دم گرمت ترانهای خواهم</p></div>
<div class="m2"><p>که آتشم به نیستان استخوان گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا رواست درین فصل غم زدا، دل را</p></div>
<div class="m2"><p>غبار کلفت ایام در میان گیرد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر عنایت ساقی کند سبکدستی</p></div>
<div class="m2"><p>پیاله، کین من از دور آسمان گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سهی قدان چمن جلوه های ناز کنند</p></div>
<div class="m2"><p>نهال رقص به گلبانگ بلبلان گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شود به لخلخه سایی، نسیم نوروزی</p></div>
<div class="m2"><p>مشام عالم افسرده بوی جان گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به من ستیزهٔ چرخ کهن نه رسم نوی ست</p></div>
<div class="m2"><p>که شاهباز فلک، صید ناتوان گیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشاط، غاشیه دار سبک روی ست حزین</p></div>
<div class="m2"><p>که چون نسیم صبا راه گلستان گیرد</p></div></div>