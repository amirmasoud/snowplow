---
title: >-
    شمارهٔ ۳۹۳
---
# شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>گلستان محبّت سرو آزادی نمی دارد</p></div>
<div class="m2"><p>بهار عاشقی، مرغ چمن زادی نمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحر می خواند بلبل در گلستان از کتاب گل</p></div>
<div class="m2"><p>که علم عاشقی حاجت به استادی نمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر مرغ چمن سیر است و گر کبک بیابانی</p></div>
<div class="m2"><p>که را از دست دل دیدی، که فریادی نمی دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین صحرا به صیدی رحمم آید کز زبونیها</p></div>
<div class="m2"><p>سری در حلقهٔ فتراک صیادی نمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه تنها غارت ناز است در اسلام پردازی</p></div>
<div class="m2"><p>دیار برهمن هم، دیر آبادی نمی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدامین فتنه دیدی در قیامتگاه بخت ما</p></div>
<div class="m2"><p>که سر در دامن زلف پریزادی نمی دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین آن دل قرارش چون بود در سینه؟ حیرانم</p></div>
<div class="m2"><p>که زخم از غمزهٔ مژگان جلادی نمی دارد</p></div></div>