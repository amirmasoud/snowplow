---
title: >-
    شمارهٔ ۶۰۶
---
# شمارهٔ ۶۰۶

<div class="b" id="bn1"><div class="m1"><p>بر سر زدیم لالهٔ داغی به جای گل</p></div>
<div class="m2"><p>داریم گریه ای که بود خونبهای گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما و سرود ناله درد آشنای خوبش</p></div>
<div class="m2"><p>تا کی رسد به خاطر دیر آشنای گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الفت به ساده لوحی ما خنده می زند</p></div>
<div class="m2"><p>ما تکیه کرده ایم به عهد و وفای گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ته جرعهٔ شراب صبوحی کشیده است</p></div>
<div class="m2"><p>از جام غنچهٔ تو، لب دلگشای گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرح حدیث ناز و نیاز من است و تو</p></div>
<div class="m2"><p>بلبل ترانه ای که سراید برای گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوران به کام ماست که مرغان مست را</p></div>
<div class="m2"><p>خوش دولتی ست سایهٔ بال همای گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ابر نوبهار ز تاراج دی، حزین</p></div>
<div class="m2"><p>گریم به های های که خالی ست جای گل</p></div></div>