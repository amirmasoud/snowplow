---
title: >-
    شمارهٔ ۷۵۰
---
# شمارهٔ ۷۵۰

<div class="b" id="bn1"><div class="m1"><p>نیست دل را هوس دل شکنی بهتر ازین</p></div>
<div class="m2"><p>صنمی را نبود برهمنی بهتر ازین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرفه دستی ست غمت را به خراش جگرم</p></div>
<div class="m2"><p>تیشه سعی نزد کوهکنی، بهتر ازین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز حدیث لب لعلت به زبانم نگذشت</p></div>
<div class="m2"><p>چه کنم یاد ندارم سخنی بهتر ازین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غوطه درخون خود از فرق زند تا به قدم</p></div>
<div class="m2"><p>به شهید تو نزیبد کفنی بهتر ازین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم از خانهٔ آیینه صفا تاب تر است</p></div>
<div class="m2"><p>یوسف حسن ندارد وطنی بهتر ازین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و یاد تو به هم الفت خاصی دارند</p></div>
<div class="m2"><p>نیست در کوی وفا انجمنی بهتر ازین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو قد، سبزه خط و لاله رخ و غنچه دهن</p></div>
<div class="m2"><p>کشور حسن ندارد چمنی بهتر ازین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دعای تو مرا دست نیاز است بلند</p></div>
<div class="m2"><p>چه برآید زکف همچو منی بهتر ازین؟</p></div></div>