---
title: >-
    شمارهٔ ۶۳۷
---
# شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>شیر و شکر ز تلخی ایّام می کشم</p></div>
<div class="m2"><p>از زهر چشم، روغن بادام می کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم عیش دور به ما دیر می رسد</p></div>
<div class="m2"><p>یکسال در میانه چو گل جام می کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در موج خیز عشق گران است لنگرم</p></div>
<div class="m2"><p>باری که بر دل است به آرام می کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از طایر مراد کنارم نشد تهی</p></div>
<div class="m2"><p>تا در غبار خاطر خود دام می کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی، کجاست بادهٔ آتش مزاج تو؟</p></div>
<div class="m2"><p>صد رنگ خواری از خرد خام می کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چشم روزنم نخلیده ست پرتوی</p></div>
<div class="m2"><p>منّت ز بخت تیره سرانجام می کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عاشقی ندیده بهارم خزان، حزین</p></div>
<div class="m2"><p>ساغر به یاد آن رخ گل فام می کشم</p></div></div>