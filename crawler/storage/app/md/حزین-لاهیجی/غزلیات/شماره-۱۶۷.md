---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>به باغ راه خزان و بهار نتوان بست</p></div>
<div class="m2"><p>به روی بخت، در روزگار نتوان بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنار کشت چه خوش می سرود دهقانی</p></div>
<div class="m2"><p>که سیل حادثه را، رهگذار نتوان بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر کسی دهن شیشه وا کند ور نه</p></div>
<div class="m2"><p>دهان شکوهٔ ما در خمار نتوان بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوفه رفت و قلندروش این کنایت گفت</p></div>
<div class="m2"><p>که برگ تا نفشانند، بار نتوان بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی ست نوبت ما بی بضاعتان ساقی</p></div>
<div class="m2"><p>که عقد دختر رز در بهار نتوان بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی توان به شب آتش نهفته داشت، حزین</p></div>
<div class="m2"><p>نهان به زلف، دل داغدار نتوان بست</p></div></div>