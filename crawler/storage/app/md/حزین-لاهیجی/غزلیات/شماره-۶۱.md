---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>درین دریای بی پایان، درین طوفان شورافزا</p></div>
<div class="m2"><p>دل افکندیم، بسم الله مجریها و مرسیها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر این بحر بی پایان، حریف درد دل گردد</p></div>
<div class="m2"><p>که دارد در جگر دریای آتش، حرص استسقا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز راه فیض، نتوان دیدهٔ امّید پوشیدن</p></div>
<div class="m2"><p>که باشد کاروان مصر، بوی پیرهن کالا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکونامان، سر شوریده ای دارم به ننگ اندر</p></div>
<div class="m2"><p>غم آشامان، دل دریاکشی دارم نهنگ آسا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاسودم به سر مستی، نیاشفتم به مخموری</p></div>
<div class="m2"><p>به یک حالت سرآوردم، چه در سرّا، چه در ضرّا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تهی دستیم، از سود و زیان ما چه می پرسی؟</p></div>
<div class="m2"><p>درین بازار قلّابی، نه دین داریم و نی دنیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دنیا نفرتی دارم، ز عقبا وحشتی دارم</p></div>
<div class="m2"><p>به این سامان، منم سلطان دارالملک استغنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تراشد از دل سنگین من بتخانه را آزر</p></div>
<div class="m2"><p>فروزد از شرار من، چراغ دیر را ترسا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به تهمت بوالهوس بر خویش می بندد، نمی داند</p></div>
<div class="m2"><p>که داغ عشق باشد بر جگر چون لاله، مادر زا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرم از خشک مغزی های زهد آسوده می گردد</p></div>
<div class="m2"><p>به مستی گر دهد ساقی به دستم گردن مینا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به افسونِ لبی، چون نی حزین از خود تهی گشتم</p></div>
<div class="m2"><p>تو آگاهی ز حال بیخودان، یا عالم النّجوا</p></div></div>