---
title: >-
    شمارهٔ ۳۲۵
---
# شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>درکارگاه غیب چو طرح لباس شد</p></div>
<div class="m2"><p>گل را حریر قسمت و ما را پلاس شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حربا نکرد روی به محراب آفتاب</p></div>
<div class="m2"><p>در خاک نقش پای تو تا روشناس شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خاک حسرت، از دم شمشیر ناز تو</p></div>
<div class="m2"><p>یک قطره خون چکید و دل بی هراس شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما جمله مظهریم جمال تو را ولی</p></div>
<div class="m2"><p>آیینه در میانهٔ ما روشنانس شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخشید جان ز باده مرا پیر میفروش</p></div>
<div class="m2"><p>دردش درین سبوی سفالین حواس شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخشد به کام جان، اثر آب زندگی</p></div>
<div class="m2"><p>هر دانه ای که با کف افسوس، آس شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکسان به خاک گشته رواق خرد، حزین</p></div>
<div class="m2"><p>بنیاد عشق بین که چه عالی اساس شد</p></div></div>