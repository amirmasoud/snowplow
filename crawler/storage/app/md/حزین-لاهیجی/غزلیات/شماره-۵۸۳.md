---
title: >-
    شمارهٔ ۵۸۳
---
# شمارهٔ ۵۸۳

<div class="b" id="bn1"><div class="m1"><p>سالک، ز سراغ ره مقصود خمش باش</p></div>
<div class="m2"><p>هر سنگ نشان، سنگ ره توست بهش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ساقی قسمت نتوان عربده انگیخت</p></div>
<div class="m2"><p>چون گل همه دم، کاسهٔ خون می کش و خوش باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بند زبان، گوش سخندان چو نیابی</p></div>
<div class="m2"><p>جایی که خرد پرده شنو نیست، خمش باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عهد تو خونی که بریزد، دیتش نیست</p></div>
<div class="m2"><p>مجنون شده عشق تو گو عاقله کش باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نوش حزین وشکرین نکته فروربز</p></div>
<div class="m2"><p>گو سرکه جبین زاهد، ازین شیوه ترش باش</p></div></div>