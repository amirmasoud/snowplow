---
title: >-
    شمارهٔ ۵۸۰
---
# شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>چو موج می جدا از باده نتوان کرد پیوستش</p></div>
<div class="m2"><p>بود میخانه زیر دست مژگان سیه مستش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آن کافر که اسلام آورد از بی نواییها</p></div>
<div class="m2"><p>ره دین می رود زاهد، که دنیا نیست در دستش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذر کرد از گلویم ناوکش چون قطرهٔ آبی</p></div>
<div class="m2"><p>چه منّتهاست برگردن مرا از صافی شستش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به امّید نگاهی دل به دنبالش فرستادم</p></div>
<div class="m2"><p>به تیر غمزه نامهربان، آن بی وفا خستش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه لذت بود از قاتل حزین نیم بسمل را</p></div>
<div class="m2"><p>که در خون می تپید و آفرین می ‎گفت بر دستش؟</p></div></div>