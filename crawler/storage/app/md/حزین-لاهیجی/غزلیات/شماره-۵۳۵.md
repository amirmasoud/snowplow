---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>ای صبا نکته ای از لعل شکربار بیار</p></div>
<div class="m2"><p>گهری تحفه ز گجینه ی اسرار بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جبینش اثر مهری اگر هست بگوی</p></div>
<div class="m2"><p>مژده ی پرتوی از عالم انوار بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن آلوده به بوی گل فردوس مکن</p></div>
<div class="m2"><p>هر چه می آوری از خاک ره یار بیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با اسیران وفاکیش چه سر داشت؟ بگوی</p></div>
<div class="m2"><p>خبر دلکشی از ناوک دلدار بیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرنوشت غم جانسوز من و شمع یکی ست</p></div>
<div class="m2"><p>جای گل آتشی، آرایش دستار بیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل باغم نکنی گر به گریبان باری</p></div>
<div class="m2"><p>بوی جان بخشی از آن رخنه ی دیوار بیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که از سیر چمن بال فشان می گذری</p></div>
<div class="m2"><p>برگ سبزی سوی مرغان گرفتار بیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب مخمور مرا جرعه نبندد ساقی</p></div>
<div class="m2"><p>چون رسد دور به من، میکده بردار بیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند بر دوش توان خرقه ی ناموس کشید؟</p></div>
<div class="m2"><p>مست از صومعه ام تا سر بازار بیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هواداری از آن سیب زنخدان بویی</p></div>
<div class="m2"><p>گر توانی به مشام من بیمار بیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قول حافظ برد از دل غم دیرینه ی ما</p></div>
<div class="m2"><p>ای صبا نکهتی از خاک ره یار بیار</p></div></div>