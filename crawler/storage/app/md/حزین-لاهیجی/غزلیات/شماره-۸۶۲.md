---
title: >-
    شمارهٔ ۸۶۲
---
# شمارهٔ ۸۶۲

<div class="b" id="bn1"><div class="m1"><p>حین طفت حول الحی اذ مررت بالجانی</p></div>
<div class="m2"><p>رهزن دل و دین شد، چشم نامسلمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفت مسلمانی، زلف دین براندازش</p></div>
<div class="m2"><p>زیر هر شکنجش دل، دیر و پیر رهبانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده ام به خونریزی، غمزه و نگاهش را</p></div>
<div class="m2"><p>ترک سخت بازویی، شوخ سست پیمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب که با هزار افغان، در فراق یوسف خویش</p></div>
<div class="m2"><p>داشتم به سینه دلی، رشک پیرکنعانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیرتم صلا زد و گفت، دامنی بزن به میان</p></div>
<div class="m2"><p>تا به کى فرومانده، در طلسم حیرانی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکر زاد راه طلب، رسم ره نوردان نیست</p></div>
<div class="m2"><p>بس بود شکسته دلی، با درست پیمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین سروش فرخنده، هوش در سماع آمد</p></div>
<div class="m2"><p>تن ز شوق جانان شد، پای تا به سر جانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ادب به جای قدم، دیده قطره زن کردم</p></div>
<div class="m2"><p>ناگهان به پیش آمد، سهمگین بیابانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورد هرکف خاکش، مغز شرزه شیران را</p></div>
<div class="m2"><p>جادهٔ خطرناکش، اژدهای پیچانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حالتی غریب افتاد، حیرتی عجب، رو داد</p></div>
<div class="m2"><p>کشتی تحمّل شد، لطمه سنج طوفانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در تف تب و تابم، درد دوری، افکنده</p></div>
<div class="m2"><p>نه رهی نه همراهی، نه دلی نه درمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>موج خیز وحشت را، بی کرانه می دیدم</p></div>
<div class="m2"><p>پهن دشت حیرت را، نه سری نه پایانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داشتم در آن حیرت، برگ و ساز جمعیت</p></div>
<div class="m2"><p>حسرت فراوانی، خاطر پریشانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گشته شمع بالینم، تیره شام دیجوری</p></div>
<div class="m2"><p>کرده اشک پروینم، پیش پا چراغانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لاله داغ دیرینم، سینه سوزی آیینش</p></div>
<div class="m2"><p>گل، کنار خونینم، غنچه اشک غلتانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خانه سوز هستی شد، آه آتش آلودم</p></div>
<div class="m2"><p>انما الحشی ذابت، من لهیب نیرانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عاشقانه نالیدم، عاجزانه می گفتم</p></div>
<div class="m2"><p>این جمع اصحابی، وین ربع خلّانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خضر پی خجستهٔ من، وقت دستگیری هاست</p></div>
<div class="m2"><p>هر طرف دد و دامی، هر قدم مغیلانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ساکنی ربا نجد، این رکب ربعکم</p></div>
<div class="m2"><p>کان شوق حضرتکم، سائقاً لأضعانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوری اختیاری نیست، عشق و دل گواه منند</p></div>
<div class="m2"><p>ما طویت کشح القلب، عنکم بسلوانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پر در عدن چشمم، کرده بود وادی را</p></div>
<div class="m2"><p>اذ بدت خیام الحی، من اهیل عدنانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیخودی ز خاطر داشت، لوح وصل و هجران را</p></div>
<div class="m2"><p>در سرم هوا نگذاشت، ذوق کفر و ایمانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کاروان مصر آمد، بوی پیرهن کالا</p></div>
<div class="m2"><p>قال لی لک البشری، یا بکیت احزانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رایگان برافشانند، خسروان عطایا را</p></div>
<div class="m2"><p>نقّلوا مطایاکم، یا کرامَ جیرانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شب حزین لایعقل، شیخ و برهمن را گفت</p></div>
<div class="m2"><p>اینما تولیتم، ثم وجه عرفانی</p></div></div>