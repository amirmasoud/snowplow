---
title: >-
    شمارهٔ ۸۴۱
---
# شمارهٔ ۸۴۱

<div class="b" id="bn1"><div class="m1"><p>بر دیده کشم سرمه ز خاک کف پایی</p></div>
<div class="m2"><p>شاید که دهد اشک مرا رنگ حنایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می در قدح و باد صبا بر سر لطف است</p></div>
<div class="m2"><p>دارد چمن امروز عجب آب و هوایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دولت طلبی، دامن دل را مده از دست</p></div>
<div class="m2"><p>شاید که برون آید ازین بیضه همایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نالیدن بلبل ز نو آموزی عشق است</p></div>
<div class="m2"><p>هرگز نشنیدیم ز پروانه صدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده ست بهار عجبی خار بیابان</p></div>
<div class="m2"><p>از دشت گذشته ست مگر آبله پایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده ست غمت، رخصت شبگیر به آهم</p></div>
<div class="m2"><p>شاید رسد این قاصد بی درد به جایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود را برسانید به یاران سبک پی</p></div>
<div class="m2"><p>می آید ازین قافله آواز درایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلشن به نسیمی شکند عهد هزاران</p></div>
<div class="m2"><p>در کشور خوبان نبود رسم وفایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور از گل رویت نفسی نیست حزین را</p></div>
<div class="m2"><p>مانده ست به جا، بلبل بی برگ و نوایی</p></div></div>