---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>دلِ شاد، رند می آشام دارد</p></div>
<div class="m2"><p>جم دور خوبش است تا جام دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گوهر، دل عارف از لنگر خویش</p></div>
<div class="m2"><p>درین بحر پر شورش، آرام دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلانند در دیده صد نیش خارش</p></div>
<div class="m2"><p>ز یک چشم خوابی، که بادام دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آیینهٔ طلعت یار پیداست</p></div>
<div class="m2"><p>به ما، هر چه در پرده ایام دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه از بخت دارم شکایت نه از چرخ</p></div>
<div class="m2"><p>مرا یار بی رحم، ناکام دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گرد عذارش خط کافر است این</p></div>
<div class="m2"><p>که صبح امید مرا شام دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود ننگ از نام رندی که در عشق</p></div>
<div class="m2"><p>غم ننگ دارد، سر نام دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حزین ، از کران تا کران حرف عشق است</p></div>
<div class="m2"><p>نه آغاز دارد، نه انجام دارد</p></div></div>