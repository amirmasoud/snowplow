---
title: >-
    شمارهٔ ۶۹۲
---
# شمارهٔ ۶۹۲

<div class="b" id="bn1"><div class="m1"><p>به مستی مرده ام ساقی، مهل مخمور در خاکم</p></div>
<div class="m2"><p>چو خم بسپار زیر طارم انگور در خاکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجل مستور اگر سازد مرا از دیده مردم</p></div>
<div class="m2"><p>ولی چون گنج قارون همچنان مشهور در خاکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تجلی، خانه زاد خلوت گور است عاشق را</p></div>
<div class="m2"><p>فروزد عقل روشن دل چراغ طور در خاکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزاران باغ و بستان دانهٔ من در گره دارد</p></div>
<div class="m2"><p>دو روزی هم چه خواهد شد اگر مستور در خاکم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکستن نیست در طالع طلسم پیکر ما را</p></div>
<div class="m2"><p>اگر عالم شود ویرانه، من معمور در خاکم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وفا و غیرت داغ محبت را تماشا کن</p></div>
<div class="m2"><p>که دارد سرخ رو، خونابهٔ ناسور در خاکم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیه بختم ولی چشم از غبارم می شود روشن</p></div>
<div class="m2"><p>نهان چون در سواد سرمه، بینی نور در خاکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وفاکردیکه شمع تربت پروانهات گشتی</p></div>
<div class="m2"><p>نمی گردم اگر گرد سرت، معذور در خاکم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گداز عشق دارد شرمسار از بی نوایانم</p></div>
<div class="m2"><p>ز ضعف تن نگردد سیر، چشم مور در خاکم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نماید گردباد وادی وحشت غبارم را</p></div>
<div class="m2"><p>دمی آسوده نگذارد سر پرشور در خاکم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی گردد حزین ، از شیوهٔ دل تربتم خالی</p></div>
<div class="m2"><p>که باشد ناله ای چون کاسه فغفور در خاکم</p></div></div>