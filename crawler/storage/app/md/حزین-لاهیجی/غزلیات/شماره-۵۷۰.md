---
title: >-
    شمارهٔ ۵۷۰
---
# شمارهٔ ۵۷۰

<div class="b" id="bn1"><div class="m1"><p>شادیم که شد جهان فراموش</p></div>
<div class="m2"><p>جانان نشود ز جان فراموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیون نرود به وصلم از یاد</p></div>
<div class="m2"><p>بلبل نکند فغان فراموش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دور نگاه فتنه خیزت</p></div>
<div class="m2"><p>آشوب کند جهان فراموش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دشمن جان که هرگزت نیست</p></div>
<div class="m2"><p>ازکینهٔ دوستان فراموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تیغ به عاشقان کشیدی</p></div>
<div class="m2"><p>ما را مکن از میان فراموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر یاد کند شکنج زلفت</p></div>
<div class="m2"><p>بلبل کند آشیان فراموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نام حزین به خاطرت نیست</p></div>
<div class="m2"><p>نامت نشد از زبان فراموش</p></div></div>