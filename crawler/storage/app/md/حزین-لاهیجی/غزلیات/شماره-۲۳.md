---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>تا فکند از نظرآن سروسرافراز مرا</p></div>
<div class="m2"><p>شده هر شاخ گلی، چنگل شهباز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه سپند است، ندانم دل بی طاقت کیست؟</p></div>
<div class="m2"><p>سوخت در بزم تو، از شعلهٔ آواز مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که از دل شده ام در غم صیاد اسیر</p></div>
<div class="m2"><p>چه ضرور است شکستن، پر پروز مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون دل خواستم از عشق تو درپرده خورم</p></div>
<div class="m2"><p>کرد رسوای جهان، دیدهٔ غمّاز مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کششی کز نگه کافر او می بینم</p></div>
<div class="m2"><p>ترسم از کعبه به بتخانه برد باز مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می برد نغمه حافظ، دلم از هوش حزین</p></div>
<div class="m2"><p>اینقدر نشئه نبخشد، می شیراز مرا</p></div></div>