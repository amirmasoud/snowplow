---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>مرا آزادگی شیرازهٔ آمال می باشد</p></div>
<div class="m2"><p>گلستان زیر بال مرغ فارغ بال می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کتاب هفت ملت، مانده در طاق فراموشی</p></div>
<div class="m2"><p>مرا سی پارهٔ دل، بس که نیکو فال می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سکندر گو نبیند، دولت غم دستگاهان را</p></div>
<div class="m2"><p>سر زانو مرا آیینهٔ اقبال می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیمی کرده گویا، آشیان بلبلی ویران</p></div>
<div class="m2"><p>بهار آشفته سامان، گل پریشان حال می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر وادی که ریزد رنگ وحشت، کلک پرشورم</p></div>
<div class="m2"><p>رم آهوی صحراگرد، در دنبال می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند دریوزه، تا کامل نگردیده ست ماه نو</p></div>
<div class="m2"><p>علاج تنگدستان جام مالامال می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین ، آیینه را حرف شکایت نیست در خاطر</p></div>
<div class="m2"><p>زبان جرأت حیرت نصیبان لال می باشد</p></div></div>