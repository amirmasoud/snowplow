---
title: >-
    شمارهٔ ۶۵۴
---
# شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>من صبر ز مژگان سیه تاب ندارم</p></div>
<div class="m2"><p>لب تشنهٔ تیغم، به گلو آب ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در، خانهٔ غارت زده را باز گذارند</p></div>
<div class="m2"><p>تا روی تو رفت از نظرم خواب ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسوده ام ازکعبه و آزاده ام از دیر</p></div>
<div class="m2"><p>جز قبلهٔ ابروی تو محراب ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جایی که نگاه تو بود حاجت می نیست</p></div>
<div class="m2"><p>پروای چراغ شب مهتاب ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق آمد و من همسفر خانه به دوشان</p></div>
<div class="m2"><p>ویران کده ای در خور سیلاب ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر رفت گل اشک، دل خون شده دریاست</p></div>
<div class="m2"><p>این نیست که خار مژه شاداب ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشک است دماغ من و ذوق چمنم نیست</p></div>
<div class="m2"><p>مخمورم و پروای می ناب ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آرام حزین از دل من شور لبت برد</p></div>
<div class="m2"><p>چشم نمک انباشته ام، خواب ندارم</p></div></div>