---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>کی صرفه ز ما خصم سبک سر به دغا برد؟</p></div>
<div class="m2"><p>خود باخت، دغل باز حریفی که ز ما برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر دو جهان باز نیامد خبر از او</p></div>
<div class="m2"><p>دل را کشش عشق ندانم به کجا برد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسرده ز دم سردی ایّام نگردید</p></div>
<div class="m2"><p>آتشکده آتش مگر از سینهٔ ما برد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از منّت پیری ست گرانباری دوشم</p></div>
<div class="m2"><p>لب را به قدم بوس تو این پشت دوتا برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک جلوه خیال تو در اندیشهٔ ما کرد</p></div>
<div class="m2"><p>دل لذت دیدار جدا، دیده جدا برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید نبردهست به چوگان سعادت</p></div>
<div class="m2"><p>گویی که ز میدان شهادت سر ما برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تردامنی مشرب رندانه حزین را</p></div>
<div class="m2"><p>از توبه پشیمانی و از خرقه صفا برد</p></div></div>