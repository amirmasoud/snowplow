---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>نبود عجب که دیده به دیدار می‌رسد</p></div>
<div class="m2"><p>فیض چمن، به رخنه دیوار می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردد قبول عذر گریبان پاره‌ام</p></div>
<div class="m2"><p>دستم اگر به دامن دلدار می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیبم مکن که حوصله‌سوز است مستیم</p></div>
<div class="m2"><p>پیمانهٔ نگاه تو، سرشار می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزادگی گزین که ازین دشت پرفریب</p></div>
<div class="m2"><p>گر می‌رسد به جای، سبکبار می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلتنگی از فغان من ای غنچه‌لب چرا؟</p></div>
<div class="m2"><p>یک ناله هم به مرغ گرفتار می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارد امیدوار مرا بخت سبز خویش</p></div>
<div class="m2"><p>آخر به وصل آینه، زنگار می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز ندیده است ز دشمن کسی، حزین</p></div>
<div class="m2"><p>آن‌ها که بر من از ستم یار می‌رسد</p></div></div>