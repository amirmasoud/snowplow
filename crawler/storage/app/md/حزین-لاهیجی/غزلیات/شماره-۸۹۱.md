---
title: >-
    شمارهٔ ۸۹۱
---
# شمارهٔ ۸۹۱

<div class="b" id="bn1"><div class="m1"><p>بردم به لحد زان رخ افروخته، داغی</p></div>
<div class="m2"><p>حاجت نبود تربت ما را به چراغی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خشک لبم، بادهکش ساغر عشقم</p></div>
<div class="m2"><p>دل را به لب، از هرگل داغی ست ایاغی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیفیت صهباست به جام سخن من</p></div>
<div class="m2"><p>ای باده گساران، برسانید دماغی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه سر آن چشمه که گم کرد سکندر</p></div>
<div class="m2"><p>ما تا در میخانه رساندیم سراغی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تربت ما می گذرد یار، سبکبار</p></div>
<div class="m2"><p>ای بارکشان غم دل، لابه و لاغی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمعی که نه در پرتو رخسار تو سوزد</p></div>
<div class="m2"><p>در دیدهٔ پروانه نماید، پر زاغی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصل ار نبود، راه خیال تو نبسته ست</p></div>
<div class="m2"><p>باز است به روی دل تنگم، در باغی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ دل ما، از نفس گرم شکفته ست</p></div>
<div class="m2"><p>ای لاله، تو افروختهای دامن راغی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرسی چه ز آتشکدهٔ عشق، حزین را؟</p></div>
<div class="m2"><p>زاهد، تو به راحتکدهٔ کنجِ فراغی</p></div></div>