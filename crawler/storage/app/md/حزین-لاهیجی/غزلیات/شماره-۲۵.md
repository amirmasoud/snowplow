---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>جان و دل غفلت زده باری شده ما را</p></div>
<div class="m2"><p>این خواب گران، سنگ مزاری شده ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا قدر جفای تو ندانی که ندانیم</p></div>
<div class="m2"><p>هر زخم، لب شکرگزاری شده ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما از دل صد پاره چه فیضی که نبردیم!</p></div>
<div class="m2"><p>درگنج قفس، باغ و بهاری شده ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسایش ما در غم آن موی میان است</p></div>
<div class="m2"><p>کز محنت ایام کناری شده ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دهر حزین ، از نی کلکت به نواییم</p></div>
<div class="m2"><p>امروز درین غمکده یاری شده ما را</p></div></div>