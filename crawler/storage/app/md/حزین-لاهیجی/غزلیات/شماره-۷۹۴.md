---
title: >-
    شمارهٔ ۷۹۴
---
# شمارهٔ ۷۹۴

<div class="b" id="bn1"><div class="m1"><p>در ملک جسم روشنی جان به نیم جو</p></div>
<div class="m2"><p>آیینه در ولایت کوران به نیمجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم به دستگاه قناعت نمی رسد</p></div>
<div class="m2"><p>در چشم مور، ملک سلیمان به نیم جو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده ای که جلوه کند کبریای عشق</p></div>
<div class="m2"><p>این طمطراق عالم امکان به نیمجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جسم فسرده را بر جانان چه اعتبار؟</p></div>
<div class="m2"><p>دلق گدا به حضرت شاهان به نیمجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چبود سراب دهر؟ که بگذشتن از دو کون</p></div>
<div class="m2"><p>در پیش پای همّت مردان به نیم جو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کشوری که حکم به زور شکستگی ست</p></div>
<div class="m2"><p>گرز گران و رستم دوران به نیم جو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد، زیاده جلوه مده زهد خشک را</p></div>
<div class="m2"><p>اینها به پیش باده پرستان به نیم جو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک روز یوسفم غم کنعانیان نداشت</p></div>
<div class="m2"><p>در مصر حسن، جان عزیزان به نیم جو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر رفت در رهت، به فدای سر تو باد</p></div>
<div class="m2"><p>در کیش عاشقان، سر و سامان به نیم جو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را متاع لایق بازار عشق نیست</p></div>
<div class="m2"><p>آنجا دل دو نیم اسیران به نیم جو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش تو غرق خجلت جانبازی خودم</p></div>
<div class="m2"><p>سر در قمارخانه رندان به نیم جو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاهد اگر به عشق ندارد سری چه باک؟</p></div>
<div class="m2"><p>خورشید پیش شب پره طبعان به نیم جو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دارم حزین به زیر نگین ملک فقر را</p></div>
<div class="m2"><p>ایران به نیم حبه و توران به نیم جو</p></div></div>