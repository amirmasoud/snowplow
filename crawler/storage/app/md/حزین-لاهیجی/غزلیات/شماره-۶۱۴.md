---
title: >-
    شمارهٔ ۶۱۴
---
# شمارهٔ ۶۱۴

<div class="b" id="bn1"><div class="m1"><p>شدم ز توبهٔ بی صرفه در بهار خجل</p></div>
<div class="m2"><p>مباد از رخ پیمانه میگسار خجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مایه داری اشکم خوش است خاطر دوست</p></div>
<div class="m2"><p>خدا کند، نکند دل مرا ز یار خجل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکردمش گرو باده از گرانجانی</p></div>
<div class="m2"><p>شدم ز خرقهٔ پشمینه در خمار خجل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکنده مهره به ششدر مرا تهیدستی</p></div>
<div class="m2"><p>نشسته ام ز حریفان بد قمار خجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل فسرده مرا کرده ز آب دیدهٔ خویش</p></div>
<div class="m2"><p>چو تخم سوخته، از ابر نوبهار خجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه دست عقده گشایی نه ذوق تسلیمی</p></div>
<div class="m2"><p>چو من مباد کس از جبر و اختیار خجل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به این دو قطره ی خون می کنم گل افشانی</p></div>
<div class="m2"><p>اگر نگردم از آن نازنین سوار خجل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلوی تشنه من موج خیز کوثر شد</p></div>
<div class="m2"><p>چرا نباشم از آن تیغ آبدار خجل؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدای را لب پیمانه بر لبم دارید</p></div>
<div class="m2"><p>گران خمارم و از دست رعشه دار خجل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه شکرها که ندارم ز بی سرانجامی</p></div>
<div class="m2"><p>چو دیگران نیم از روی روزگار خجل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زیر تیغ تو از شرم ناشکیبایی</p></div>
<div class="m2"><p>چو شمع می گزم انگشت زینهار خجل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه دل به جا و نه دین، تا کنم نثار، حزین</p></div>
<div class="m2"><p>نشسته ام به سر راه انتظار خجل</p></div></div>