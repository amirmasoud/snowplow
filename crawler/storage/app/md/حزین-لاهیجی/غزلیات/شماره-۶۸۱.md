---
title: >-
    شمارهٔ ۶۸۱
---
# شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>ما چاک به دامن زدهٔ تهمت عشقیم</p></div>
<div class="m2"><p>واعظ سر خود گیر که ما امّت عشقیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاری بود از عکس خودی آینهٔ ما</p></div>
<div class="m2"><p>آتش به دل و جان زدهٔ غیرت عشقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس را نرسد درحق ما ردّ و قبولی</p></div>
<div class="m2"><p>ما گر بد، اگر نیک، که از حضرت عشقیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیرون نتوانیم شد از کوی محبت</p></div>
<div class="m2"><p>پروانه صفت سوختهٔ خلوت عشقیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود خطر از برق فنا حاصل ما را</p></div>
<div class="m2"><p>ما خود دل و دین باختهٔ همّت عشقیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسایش دل هاست حزین ، زمزمهٔ ما</p></div>
<div class="m2"><p>ما نغمه طراز چمن عشرت عشقیم</p></div></div>