---
title: >-
    شمارهٔ ۸۹۹
---
# شمارهٔ ۸۹۹

<div class="b" id="bn1"><div class="m1"><p>کند خون دل من، چشم تر را خانه آرایی</p></div>
<div class="m2"><p>که دیگر می کند بهتر ز می، پیمانه آرایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغانی ز داغت، رخنه های سینه ام دارد</p></div>
<div class="m2"><p>کند شبها دل دیوانه ام، ویرانه آرایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوشت در نمی آید، حدیث نکته پردازان</p></div>
<div class="m2"><p>کنند از قصهٔ زلفت، مگر افسانه آرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اخلاص محبّت، رونق دل را حوالت کن</p></div>
<div class="m2"><p>نیاز برهمن، بهتر کند بتخانه آرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزین ، از کلفت دل خاطرم خشنود می باشد</p></div>
<div class="m2"><p>کند گرد یتیمی، گوهر یک دانه آرایی</p></div></div>