---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>پخته به حکمتی کنم، بادهٔ نارسای را</p></div>
<div class="m2"><p>بر سر خم نهاده ام، خشت کلیسیای را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بودت به عاشقی، لخت دلی نیازکن</p></div>
<div class="m2"><p>توشه ببند بر میان، نالهٔ ره گرای را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محمل لیلی از نظر رفت اوا نشان پی گم است</p></div>
<div class="m2"><p>گوش به راه حیرتم، زمزمهٔ درای را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برهمنی کمینه ام، سجده بر صنمکده</p></div>
<div class="m2"><p>چین بگشا ز ابروان، قبله من، خدای را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام صبوح کش چو گل، تا که به جلوه آورد</p></div>
<div class="m2"><p>مشرق چاک پیرهن، سینه دلگشای را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فصل بهار روی تو، کلک زبان بریده ام</p></div>
<div class="m2"><p>نغمه شکسته در گلو، بلبل خوش نوای را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلوه نو خطان حزین ، ازرخ ساده خوشتراست</p></div>
<div class="m2"><p>غالیه ساز صفحه کن، خامهٔ مشک سای را</p></div></div>