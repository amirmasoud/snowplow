---
title: >-
    شمارهٔ ۷۲۱
---
# شمارهٔ ۷۲۱

<div class="b" id="bn1"><div class="m1"><p>نشد فغان به اثر تا ره جنون نزدم</p></div>
<div class="m2"><p>سخن به نشئه نشد تا نفس به خون نزدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفته است سبوی مرا به سنگ چرا؟</p></div>
<div class="m2"><p>گلی به شیشهٔ این چرخ آبگون نزدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نزد شعبده بازان پیاده فرزین است</p></div>
<div class="m2"><p>منم که نقش دغل با سپهر دون نزدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبک سران پی کلکم روند و افسوس است</p></div>
<div class="m2"><p>که نعل رخش سخن را چرا نگون نزدم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سلک نظم جگر پاره ها گسسته حزین</p></div>
<div class="m2"><p>گره به رشته این اشک لاله گون نزدم</p></div></div>