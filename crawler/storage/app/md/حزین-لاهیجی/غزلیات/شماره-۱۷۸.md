---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>صبح را لمعهٔ نور از ید بیضای دل است</p></div>
<div class="m2"><p>آتش طور، فروغ رخ موسای دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهره، حوران بهشتی عبث آراسته اند</p></div>
<div class="m2"><p>چشم صاحب نظران محو تماشای دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای هشیار نه، ای پیک خیال رخ دوست</p></div>
<div class="m2"><p>سینه تا دیده، پر از باده ی مینای دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطرهٔ اشک مرا ای گل تر خوار مبین</p></div>
<div class="m2"><p>این گرانمایه گهر، زاده ی دریای دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه عجب گر شنوی بوی کباب از سخنم</p></div>
<div class="m2"><p>نفسم سوختهٔ آتش سودای دل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات خم بادهٔ پر زور یکیست</p></div>
<div class="m2"><p>مستی نه فلک از ساغر صهبای دل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر مجمر نکند جای دگر، گرم سپند</p></div>
<div class="m2"><p>سینه ی سوختگان، منزل و مأوای دل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر از لیلی سرگشته ی خود باز نیافت</p></div>
<div class="m2"><p>سالها شدکه جنون بادیه پیمای دل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاب حیوان غمت ، زندهٔ جاوید شدیم</p></div>
<div class="m2"><p>کمترین معجزهٔ عشق تو احیای دل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می شناسد همه کس طرز نوای تو حزین</p></div>
<div class="m2"><p>دم جان بخش زدن کار مسیحای دل است</p></div></div>