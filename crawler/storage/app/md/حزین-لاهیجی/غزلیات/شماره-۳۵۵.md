---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>جایی که از سپند نگردد فغان بلند</p></div>
<div class="m2"><p>ما را بود چو شعلهٔ آتش، زبان بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بال و پری کجاست که با همّت رسا</p></div>
<div class="m2"><p>پرواز گیرم از سر این خاکدان بلند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درگلشنی که بانگ صفیرم فکنده شور</p></div>
<div class="m2"><p>بلبل ز خوی گل ننماید فغان بلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با پستی سپهر نیامد فرو سرم</p></div>
<div class="m2"><p>عنقا صفت فتاده مرا آشیان بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا شد دلم به حلقهٔ گلدام زلف اسیر</p></div>
<div class="m2"><p>شد شور محشر از قفس بلبلان بلند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رحم است بر درازی اندوه قمریان</p></div>
<div class="m2"><p>پرواز پست و جلوهٔ سرو روان بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش می کشند دامن ناز این سهی قدان</p></div>
<div class="m2"><p>دست ستمکشی نشود از میان بلند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامش حبن که ناله به جایی نمی رسد</p></div>
<div class="m2"><p>پست آفریده اند زمین، آسمان بلند</p></div></div>