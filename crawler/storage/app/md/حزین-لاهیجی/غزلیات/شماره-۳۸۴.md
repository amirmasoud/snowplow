---
title: >-
    شمارهٔ ۳۸۴
---
# شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>امشب که دل در آتش آن گلعذار بود</p></div>
<div class="m2"><p>هر موی بر تنم رگ ابر بهار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل نمود چهره و دیدار، رو نداد</p></div>
<div class="m2"><p>چشمی که داشتم به ره انتظار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محرومی وصال همین در فراق نیست</p></div>
<div class="m2"><p>تا یار بود دیده به حیرت دچار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز طبع در پی فکر بلند نیست</p></div>
<div class="m2"><p>شهباز ما همیشه همایون شکار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شاخ گل ز حال که پرسد درین چمن؟</p></div>
<div class="m2"><p>چون من هزار عاشق بی اعتبار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای گریه گرد غم ننشاندی چه فایده؟</p></div>
<div class="m2"><p>بسیار خاطرم به تو امّیدوار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبود به غیر سینهٔ خونین دلان حزین</p></div>
<div class="m2"><p>دشتی که لاله اش جگر داغدار بود</p></div></div>