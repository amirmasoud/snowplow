---
title: >-
    شمارهٔ ۶۲۷
---
# شمارهٔ ۶۲۷

<div class="b" id="bn1"><div class="m1"><p>برخیز سوی عالم بالا برون رویم</p></div>
<div class="m2"><p>از خود به یاد آن قد رعنا برون رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطرب رهی بسنج که از جا برون رویم</p></div>
<div class="m2"><p>تا دست دل گرفته ز دنیا برون روبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این خاکمال، قطرهٔ ما را سزا نبود</p></div>
<div class="m2"><p>ما را که گفته بود ز دریا برون رویم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهری تمام طالب سودای یوسفند</p></div>
<div class="m2"><p>ما هم بیا به عزم تماشا برون رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پرده بیش ازاین نتوان جام می زدن</p></div>
<div class="m2"><p>ساغر زنان ز میکده، رسوا برون رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف به وصل زال جهان تن نمی دهد</p></div>
<div class="m2"><p>دامن کشان ز چنگ زلیخا برون رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در رقص شوق، خردهٔ جان از پی نثار</p></div>
<div class="m2"><p>بر کف نهیم و چون شرر از جا برون رویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق به شهر بند خرد چون بود؟ بیا</p></div>
<div class="m2"><p>دیوانه وار روی به صحرا برون رویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوراق رنگ و بوی به باد فنا دهیم</p></div>
<div class="m2"><p>از زیر منّت چمن آرا برون رویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مستانه جلوه های جنون راه می زند</p></div>
<div class="m2"><p>از قید عقل ، سرخوش و شیدا برون رویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبنم صفت به ذیل وفایی زنیم چنگ</p></div>
<div class="m2"><p>زمبن خاکدان به همّت والا برون رویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما را به رنگ غنچه دل از گلستان گرفت</p></div>
<div class="m2"><p>چون لاله سینه چاک به صحرا برون رویم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این می حزین ، افاضهٔ مینای جامی است</p></div>
<div class="m2"><p>بر کف گرفته جام مصفّا برون رویم</p></div></div>