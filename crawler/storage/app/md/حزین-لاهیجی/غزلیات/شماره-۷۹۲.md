---
title: >-
    شمارهٔ ۷۹۲
---
# شمارهٔ ۷۹۲

<div class="b" id="bn1"><div class="m1"><p>هله من جان جهانم، تنه ناها یاهو</p></div>
<div class="m2"><p>مظهر آیت شانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو دلجوی تو تا دیده ام ای نخل مراد</p></div>
<div class="m2"><p>همه در رقص روانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو را می نگرم، جمله تو را می نگرم</p></div>
<div class="m2"><p>همه بینم، همه دانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست سودای تو جانم، تنه ناها تنه نا</p></div>
<div class="m2"><p>محو نام تو زبانم، تنه ناها یاهو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرتو روی تو ای مهر جهانتاب گرفت</p></div>
<div class="m2"><p>جمله پیدا و نهانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساغر میکده عشق خرد پرداز است</p></div>
<div class="m2"><p>مست و دیوانه از آنم تنه ناها یاهو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که از خود خبرم نیست، چه دوزخ چه بهشت</p></div>
<div class="m2"><p>فارغ از سود و زیانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس عشوه گر مغبچه ای ساغر داد</p></div>
<div class="m2"><p>در خرابات مغانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا می نگرم، جانب هر کس بینم</p></div>
<div class="m2"><p>به جمالش نگرانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر طرف می کشدم جلوه مستانه او</p></div>
<div class="m2"><p>رفته از دست عنانم، تنه ناها یاهو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهتر آن است که ساغر سر بازار زنم</p></div>
<div class="m2"><p>من که رسوای جهانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مست در آینهٔ خویش نظر می کردم</p></div>
<div class="m2"><p>رخ او گشت عیانم تنه ناها یاهو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنچنان مست لقا گشته ام امروز، حزین</p></div>
<div class="m2"><p>که خود از یار ندانم تنه ناها یاهو</p></div></div>