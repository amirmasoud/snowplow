---
title: >-
    شمارهٔ ۶۴۰
---
# شمارهٔ ۶۴۰

<div class="b" id="bn1"><div class="m1"><p>ترسم که پریشان شود از ناله غبارم</p></div>
<div class="m2"><p>در کوی تو خاموشی از آن است شعارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این مژده ز من بال فشانان چمن را</p></div>
<div class="m2"><p>کنج قفس امسال گذشته ست بهارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نارس نگهی دیدم و آشفته ترم ساخت</p></div>
<div class="m2"><p>ساقی می کم داد و فزون گشت خمارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیداست که خواهی به سر تربتم آمد</p></div>
<div class="m2"><p>چون دل نتپد بی سببی سنگ مزارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صبح بیا هم نفسم باش زمانی</p></div>
<div class="m2"><p>شاید به صفا با تو دمی چند برآرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محویم حزین از دل چون آینه خویش</p></div>
<div class="m2"><p>افتاده به دیدار پرستی سر و کارم</p></div></div>