---
title: >-
    شمارهٔ ۷۴۴
---
# شمارهٔ ۷۴۴

<div class="b" id="bn1"><div class="m1"><p>کار دل خام شد از سوزش بسیار چنین</p></div>
<div class="m2"><p>عشق افکنده مرا از نظر یار چنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد آن قامت موزون نرود از دل ما</p></div>
<div class="m2"><p>مصرع سرو کند فاخته، تکرار چنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش یوسف ندرد پرده زلیخا، چه کند؟</p></div>
<div class="m2"><p>دل بی تاب چنان، ناز خریدار چنین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که زد بر رگ جان نشتر کاری نگهت</p></div>
<div class="m2"><p>آه من می کند آخر به دلت کار چنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهل باشد اگرم قدر ندانی لیکن</p></div>
<div class="m2"><p>عشق را خوار مکن ای گل بی خار چنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چه امّید قرار دل مهجور دهم</p></div>
<div class="m2"><p>خصمی بخت چنان، دوستی یار چنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگهی سر زده از چشم توکآشوب دل است</p></div>
<div class="m2"><p>هیچ مستی نرود از در خمّار چنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر وزدباد به زلف تو، دلم می لرزد</p></div>
<div class="m2"><p>هیچ کافر نکشد غیرت زنّار چنین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دود آهم به سرکوی تو منزل دارد</p></div>
<div class="m2"><p>ابر گستاخ نبوده ست به گلزار چنین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طرفه فیضی ست خط طرف بناگوش تو را</p></div>
<div class="m2"><p>یاسمین جلوه ندارد به سمن زار چنین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این غزل ریخت حزین ، از مژهٔ خامه و گفت</p></div>
<div class="m2"><p>قطره با ابر زند، کلک گهربار چنین</p></div></div>