---
title: >-
    شمارهٔ ۷۳۵
---
# شمارهٔ ۷۳۵

<div class="b" id="bn1"><div class="m1"><p>دل به آب خضر و عمر جاودان نسپرده ایم</p></div>
<div class="m2"><p>جز به خاک آستانت نقد جان نسپرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاش لله گل کند بوی شکایت از لبم</p></div>
<div class="m2"><p>ما وفاداری به آن نامهربان نسپرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حریم آشنایی جان و دل بیگانه اند</p></div>
<div class="m2"><p>راز پنهان را به این نامحرمان نسپرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می خلد از نیشتر افزون رگ غفلت به دل</p></div>
<div class="m2"><p>نبض آگاهی به این خواب گران نسپرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرزوی جنت از کوی تو ما را ره نزد</p></div>
<div class="m2"><p>درکف اندیشهٔ باطل، عنان، نسپرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوری از حد رفت، رحمی بر دل زار حزین</p></div>
<div class="m2"><p>اینقدرها، ما به خود تاب و توان نسپرده ایم</p></div></div>