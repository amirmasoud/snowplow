---
title: >-
    شمارهٔ ۶۲۵
---
# شمارهٔ ۶۲۵

<div class="b" id="bn1"><div class="m1"><p>دل تنگ از ستمت رشک گلستان کردم</p></div>
<div class="m2"><p>لب زخمی ز دم تیغ تو خندان کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر شوریده دلان در خم چوگان من است</p></div>
<div class="m2"><p>بس که آشفتگی از زلف تو سامان کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کام جانی که به زهر ستم انباشته بود</p></div>
<div class="m2"><p>به خیال لب نوشت شکرستان کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بساط من دل دادهٔ دیدارپرست</p></div>
<div class="m2"><p>دیده ای بود که بر روی تو حیران کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سفر وادی امّید به جایی نرسید</p></div>
<div class="m2"><p>مدتی همرهی آبله پایان کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گبر دیرینهٔ عشقم چه شد ار قدرم نیست؟</p></div>
<div class="m2"><p>عمرها خدمت آن آتش سوزان کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذره در همّتم آویخت، به خورشید رسید</p></div>
<div class="m2"><p>مور اگر رو به من آورد سلیمان کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از فغان دل شوریده، به منقار مرا</p></div>
<div class="m2"><p>پرده ای بود که پیرایهٔ بستان کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاطر پیر مغان شاد که از همّت او</p></div>
<div class="m2"><p>کوری محتسبان باده فراوان کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه گفتم چو نی، از دولت آن لب گفتم</p></div>
<div class="m2"><p>هر چه کردم به هواداری جانان کردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل جمعی نگران سخنم بود حزین</p></div>
<div class="m2"><p>سر زلف رقمی تازه، پریشان کردم</p></div></div>