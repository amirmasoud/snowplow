---
title: >-
    شمارهٔ ۸۳۹
---
# شمارهٔ ۸۳۹

<div class="b" id="bn1"><div class="m1"><p>افشاند نسیم سحری زلف نگاری</p></div>
<div class="m2"><p>می خواست دماغ دل ما، بوی بهاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بخت نصیب نظر پاک که سازد</p></div>
<div class="m2"><p>برداشت صبا از سر کوی تو غباری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی فایده رفت، آن همه اشکی که فشاندم</p></div>
<div class="m2"><p>سیراب نکردم گل باغی، سر خاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مملکت طالع ما، صبح نخندد</p></div>
<div class="m2"><p>ماییم و سواد سر زلف و شب تاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیم است که بی پرده کنم فاش غمت را</p></div>
<div class="m2"><p>هجران تو نگذاشت به دل صبر و قراری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار از نظر انداخت، دل زار حزین را</p></div>
<div class="m2"><p>ای نالهٔ بی درد، نیامد ز تو کاری</p></div></div>