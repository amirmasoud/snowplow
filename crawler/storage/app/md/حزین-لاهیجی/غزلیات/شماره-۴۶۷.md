---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>ز آهم بیستون چرخ، آتش تاب می‌گردد</p></div>
<div class="m2"><p>ز برق تیشهٔ من کوه آهن آب می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس در خود پی آن گوهر نایاب می‌گردم</p></div>
<div class="m2"><p>گریبان من از سرگشتگی گرداب می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یاد روی آن گل‌پیرهن شب چون کشم آهی</p></div>
<div class="m2"><p>کتان طاقتم را پرتو مهتاب می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سازد با دل افسردگان شور و نوای من؟</p></div>
<div class="m2"><p>نمک در دیدهٔ غافل‌نهادان خواب می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزین از جوی خاطر سرو کلک جلوه زیب من</p></div>
<div class="m2"><p>چه خون‌ها می‌خورد تا مصرعی سیراب می‌گردد</p></div></div>