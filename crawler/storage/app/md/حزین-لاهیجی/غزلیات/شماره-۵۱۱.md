---
title: >-
    شمارهٔ ۵۱۱
---
# شمارهٔ ۵۱۱

<div class="b" id="bn1"><div class="m1"><p>مردان نظر از نرگس فتّان تو یابند</p></div>
<div class="m2"><p>فیض سحر از چاک گریبان تو یابند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشّاق جگر سوخته، جمعیّت دل را</p></div>
<div class="m2"><p>در سلسلهٔ زلف پریشان تو یابند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف صفتان با همه بی باکی و شوخی</p></div>
<div class="m2"><p>آسودگی ازگوشهٔ زندان تو یابند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک چو از نازکشی زلف گره گیر</p></div>
<div class="m2"><p>سرها همه را در خم چوگان تو یابند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر تازه نهالی که به جولانگه ناز است</p></div>
<div class="m2"><p>خاک قدم سرو خرامان تو یابند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن شهد گلوسوز، که دلهاست کبابش</p></div>
<div class="m2"><p>شیرین دهنان ز شکرستان تو یابند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر غنچه که در پیرهن باغ و بهار است</p></div>
<div class="m2"><p>خمیازه کش چاک گریبان تو یابند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر جا گذرد حرف، ز خورشید قیامت</p></div>
<div class="m2"><p>صاحبنظران چهرهٔ تابان تو یابند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخشید حیات تن اگر آب سکندر</p></div>
<div class="m2"><p>دل زندگی از چشمهٔ حیوان تو یابند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر ناوک دلدوز که درکیش قضا بود</p></div>
<div class="m2"><p>خونین جگران در صف مژگان تو یابند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدّ نگه حسرت و آه دل گرم است</p></div>
<div class="m2"><p>شمعی که سر خاک شهیدان تو یابند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون قفل حزین ، از لب افسانه گشایی</p></div>
<div class="m2"><p>آشفته دلان حال پریشان تو یابند</p></div></div>