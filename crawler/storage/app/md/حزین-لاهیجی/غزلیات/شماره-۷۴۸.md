---
title: >-
    شمارهٔ ۷۴۸
---
# شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>زهد ما با می گلفام چه خواهد بودن؟</p></div>
<div class="m2"><p>آبروی خرد خام چه خواهد بودن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شود نیم نفس فرصت بال افشانی</p></div>
<div class="m2"><p>انتقام قفس و دام چه خواهد بودن؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر دامن کش و گلشن خوش و ساقی ست کریم</p></div>
<div class="m2"><p>خارخار غم ایام چه خواهد بودن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در محیطی که زند موج عطا، گوهر فیض</p></div>
<div class="m2"><p>آرزوی من ناکام چه خواهد بودن؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت خود خوش گذران با می و معشوق حزین</p></div>
<div class="m2"><p>کس چه داند که سرانجام چه خواهد بودن؟</p></div></div>