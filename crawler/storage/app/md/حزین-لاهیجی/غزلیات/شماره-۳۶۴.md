---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>اگر نسیم نباشد که زلف بگشاید؟</p></div>
<div class="m2"><p>به عاشقان رخ معشوق را که بنماید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شمع، شب نشود روز، قدر وقت بدان</p></div>
<div class="m2"><p>طلوع شعشعه ی آفتاب می باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معاشران به نشاط بهار، خنده زنید</p></div>
<div class="m2"><p>مجال نیست که گل ساغری بپیماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست کوتهم آن طرّهٔ رسا نفتاد</p></div>
<div class="m2"><p>چه شد که پرچم آهم به عرش می ساید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بانگ بربط و می بادهٔ مغانه بکش</p></div>
<div class="m2"><p>که واعظ نفس افسرده، ژاژ می خاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسد چو دور به زاهد، قدح برافشانید</p></div>
<div class="m2"><p>پیاله گر نکشد، دامنی بیالاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم ز غنچه پیکان او شکفت حزین</p></div>
<div class="m2"><p>خوشا دلی که ز فیضش دلی بیاساید</p></div></div>