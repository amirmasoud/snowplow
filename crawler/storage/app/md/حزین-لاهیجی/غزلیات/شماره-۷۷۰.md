---
title: >-
    شمارهٔ ۷۷۰
---
# شمارهٔ ۷۷۰

<div class="b" id="bn1"><div class="m1"><p>بی تو چسان به سر برد جان امیدوار من؟</p></div>
<div class="m2"><p>ای بت دلفریب من، صبر من و قرار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر شاهوار من، مایه افتخار من</p></div>
<div class="m2"><p>باغ من و بهار من، راحت روزگار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان من و جهان من، امن من و امان من</p></div>
<div class="m2"><p>عین من و عیان من، سرّ من، آشکار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر غم تو در جهان نوش و نشاط خستگان</p></div>
<div class="m2"><p>تلخ تو در مذاق جان باده ی خوشگوار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز خم و سبوی تو، مست به های و هوی تو</p></div>
<div class="m2"><p>مقصد دیده روی تو عشق تو اختیار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرور سرفراز من، مایهٔ سوز و ساز من</p></div>
<div class="m2"><p>دلبر و دلنواز من، مونس و غمگسار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلبر بی نظیر من، مهر تو در ضمیر من</p></div>
<div class="m2"><p>لطف تو دستگیر من، خواریت اعتبار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل به هوای روی تو، رفته به جستجوی تو</p></div>
<div class="m2"><p>مانده در آرزوی تو، دیدهٔ اشکبار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش که شمع سان تنم مایهٔ اشک و آه بود</p></div>
<div class="m2"><p>آمد وکرد پرسشم، هوش ربا نگار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت چگونه ای بگو، در غم من حزین من؟</p></div>
<div class="m2"><p>بیکس من غریب من، خستهٔ سوگوار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم اگر وفا کنی، هست در انتظار تو</p></div>
<div class="m2"><p>سینهٔ داغدار من، خاطر بی قرار من</p></div></div>