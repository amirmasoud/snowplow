---
title: >-
    شمارهٔ ۷۰۱
---
# شمارهٔ ۷۰۱

<div class="b" id="bn1"><div class="m1"><p>می شود دل چو گل از عیش پریشان چه کنم؟</p></div>
<div class="m2"><p>غنچه سان گر نکشم سر به گریبان چه کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده جمعیت دلهای اسیران بر باد</p></div>
<div class="m2"><p>نکنم شکوه از آن زلف پریشان چه کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به آن چشم فسون ساز که چشمش مرساد</p></div>
<div class="m2"><p>من گرفتم ندهم، با صف مژگان چه کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طعنه بر بی دل و دینان مزن ای زاهد شهر</p></div>
<div class="m2"><p>دل و دین می برد آن نرگس فتان چه کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر و سامان بود ارزانی ناقص خردان</p></div>
<div class="m2"><p>من که دیوانهٔ عشقم سر و سامان چه کنم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند گویی که به دل مهر بتان پنهان دار</p></div>
<div class="m2"><p>بوی یوسف رود از مصر به کنعان چه کنم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نه آنم که به دنبال دل از جا بروم</p></div>
<div class="m2"><p>می کشد سوی خود آن سرو خرامان چه کنم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می زنم خویش به آن شعلهٔ بی باک حزین</p></div>
<div class="m2"><p>بیش ازین نیست مرا طاقت هجران چه کنم؟</p></div></div>