---
title: >-
    شمارهٔ ۷۳۲
---
# شمارهٔ ۷۳۲

<div class="b" id="bn1"><div class="m1"><p>خرقه را در گرو ساغر لبریز کنیم</p></div>
<div class="m2"><p>ما خراباتی و رندیم چه پرهیز کنیم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر صبا بگذرد از تربت ما سوختگان</p></div>
<div class="m2"><p>به هوای رخ زیبای تو گلبیز کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما که موریم مددگر رسد از خسرو عشق</p></div>
<div class="m2"><p>تختهٔ مشق ستم، سینهٔ پروبزکنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر رسد بر سر ما خسرو شیرین حرکات</p></div>
<div class="m2"><p>سرچه باشد که غبار ره شبدیز کنیم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون ما ریزد اگر ساقی گل چهره به خاک</p></div>
<div class="m2"><p>نوحه بر خویش به بانگ طرب انگیز کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتنه می بارد از آن نرگس مستانه حزین</p></div>
<div class="m2"><p>به که جا در شکن زلف دلاویز کنیم</p></div></div>