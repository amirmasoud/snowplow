---
title: >-
    شمارهٔ ۷۷۵
---
# شمارهٔ ۷۷۵

<div class="b" id="bn1"><div class="m1"><p>که خواهد رسانید پیغام من</p></div>
<div class="m2"><p>به بیگانهٔ آشنا نام من؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون با حریفان خوری باده ام</p></div>
<div class="m2"><p>به سنگ جفا نشکنی جام من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کار آیدت چون رگ تلخ می</p></div>
<div class="m2"><p>به یاد آوری تلخی کام من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خوش زی که فرخنده مرغ مراد</p></div>
<div class="m2"><p>پریده ست از گوشهٔ بام من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه دل مانده بر جا نه لخت جگر</p></div>
<div class="m2"><p>جگر پارهٔ من، دلارام من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیچ و خم روزگارم اسیر</p></div>
<div class="m2"><p>رمیده ست آسایش از دام من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آتش سپندی ست جان حزین</p></div>
<div class="m2"><p>چه می پرسی از صبر و آرام من؟</p></div></div>