---
title: >-
    شمارهٔ ۳۶۳
---
# شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>کشم چو آه، دل ناتوان بیاساید</p></div>
<div class="m2"><p>خدنگ چون سفری شد کمان بیاساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجال دیده گشودن درین غبارکجاست؟</p></div>
<div class="m2"><p>مگر که از تک و تاز آسمان بیاساید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو موج قافلهٔ عمر را درنگی نیست</p></div>
<div class="m2"><p>کسی چگونه درین کاروان بیاساید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بساط سبزه وگل را نچیده برچیدند</p></div>
<div class="m2"><p>چگونه بلبل این بوستان بیاساید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغان که در غم عشق، اضطراب دل نگذاشت</p></div>
<div class="m2"><p>خدنگ غمزهٔ نامهربان بیاساید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گوش، رشک برد دل، حدیثت ار شنوم</p></div>
<div class="m2"><p>برم چو نام خوشت را زبان بیاساید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین از آن سگِ کو، تا برید پیوندم</p></div>
<div class="m2"><p>چو نی نشد ز فغان استخوان بیاساید</p></div></div>