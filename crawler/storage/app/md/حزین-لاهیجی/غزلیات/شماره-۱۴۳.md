---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>کشم خط از سواد خامه زلف عنبرافشان را</p></div>
<div class="m2"><p>به داغ رشک سوزد خامهام ناف غزالان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چاک سینه چون خورشید محشر بشکفد داغم</p></div>
<div class="m2"><p>گر آن گل پیرهن چون صبح بگشاید گریبان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اشک خود از آن کان ملاحت کام می گیرم</p></div>
<div class="m2"><p>گلو شیرین کند شوراب زمزم، کعبه جویان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم کم مبین ای کج نظر زخم نمایانم</p></div>
<div class="m2"><p>گلستان کرده آب خنجر او، این خیابان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلند است از تپیدنهای دل گلبانگ ناقوسم</p></div>
<div class="m2"><p>گذار افتد به این بتخانه کاش، آن نامسلمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پریشان دل، به شام تیره بختی الفتی دارد</p></div>
<div class="m2"><p>خیال زلف لیلی می کند، خواب پریشان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خروش سینه، کام زخم دل در لذت اندازد</p></div>
<div class="m2"><p>نمک چش داغ مجنون است شور این بیابان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شفق پرورده اشک است، رنگ زعفران زارم</p></div>
<div class="m2"><p>ز رشک سرخ رویی داغ کردم لاله زاران را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو دستم زیر سنگ سرگرانی مانده از عمری</p></div>
<div class="m2"><p>مگرگیرد نیازم دامن ناز خرامان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز غمخواری فتد در لجّه خون سینه چاکم</p></div>
<div class="m2"><p>که طوفان است موج بخیه، این زخم نمایان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه آنم کز جفای عشق آسان دست بردارم</p></div>
<div class="m2"><p>به دامان قیامت میبرم چاک گریبان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل و جان از خموشی سوخت، باید شمع محفل شد</p></div>
<div class="m2"><p>شکستن درگلو زین بیش نتوان آه سوزان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز شادی بسته می گردد زبان شکوه آلودم</p></div>
<div class="m2"><p>تبسم گر به زخمم بشکند مهر نمکدان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکستی راه در غربت نیابد نونهال من</p></div>
<div class="m2"><p>پی قتل که دیگر بر شکستی طرف دامان را؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ستم در دور چشمت میر دیوان مروّت شد</p></div>
<div class="m2"><p>به خون بیگناهان آب دادی تیغ مژگان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفرما شمع من پروانه گرد سرت گردم</p></div>
<div class="m2"><p>به دل مپسند داغ حسرت رنگ پرافشان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شب حسرت‌نصیبی‌های بخت من سحر گردد</p></div>
<div class="m2"><p>کنی گر جادهٔ نظاره‌ام، صبح گریبان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حزین از خود شدم در حیرت سنبل بناگوشی</p></div>
<div class="m2"><p>ز بوی گل بود افسانه، خواب نوبهاران را</p></div></div>