---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>طی می شود از مصرع آهی گلهٔ ما</p></div>
<div class="m2"><p>طالع به وصال تو نویسد صلهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران سبک سیر، رسیدند به منزل</p></div>
<div class="m2"><p>چون نقش قدم، مانده به جا قافلهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شایستهٔ برق است به صحرای ملامت</p></div>
<div class="m2"><p>خاری که به خون تر نشد از آبلهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیرانه سر، آزادگی از عثثت نداربم</p></div>
<div class="m2"><p>رگها شده درگردن ما، سلسلهٔ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بی خبران پای طلب رنجه مسازید</p></div>
<div class="m2"><p>نزدیکتر از ماست به ما، مرحلهٔ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر موج زند بر لب ما، تلخی عالم</p></div>
<div class="m2"><p>هرگز نزند چین به جبین حوصلهٔ ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستان زن مستیم حزین ، تا نفسی هست</p></div>
<div class="m2"><p>از عشق، نکونام بود سلسلهٔ ما</p></div></div>