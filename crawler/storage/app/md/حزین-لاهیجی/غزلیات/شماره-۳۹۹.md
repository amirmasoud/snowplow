---
title: >-
    شمارهٔ ۳۹۹
---
# شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>دلم در خم زلف او سودای دگر دارد</p></div>
<div class="m2"><p>با سلسله دیوانه غوغای دگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با جذبهٔ مشتاقی، باشد دو جهان گامی</p></div>
<div class="m2"><p>در دامن دل عاشق، صحرای دگر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحرای طلب دارد، در هر قدمی طوری</p></div>
<div class="m2"><p>هر سنگ دربن وادی موسای دگر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر عشق نهان بازد با خود عجبی نبود</p></div>
<div class="m2"><p>در پردهٔ دل مجنون، لیلای دگر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افلاک نگهبان عشق تو نمی باشد</p></div>
<div class="m2"><p>این بادهٔ زورآور مینای دگر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مجلس ما یک کس هشیار نمی گردد</p></div>
<div class="m2"><p>در جام مگر ساقی صهبای دگر دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیداست حزین ما از دلق می آلودش</p></div>
<div class="m2"><p>کاین رند خراباتی، تقوای دگر دارد</p></div></div>