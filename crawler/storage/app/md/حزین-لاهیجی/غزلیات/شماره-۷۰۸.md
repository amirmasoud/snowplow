---
title: >-
    شمارهٔ ۷۰۸
---
# شمارهٔ ۷۰۸

<div class="b" id="bn1"><div class="m1"><p>چه پروا توشهٔ واماندگی چون در کمر دارم</p></div>
<div class="m2"><p>به جایی می رسم اکنون که سامان سفر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد در عاشقی بر من عبث افسانه می خواند</p></div>
<div class="m2"><p>درین مکتب کتاب هفت ملت را ز بر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یتیمان محبت را وفایی دایه نگذارد</p></div>
<div class="m2"><p>که با هر قطره اشک گرم خود لخت جگر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب نبود اگر زرّین چو خورشید است مژگانم</p></div>
<div class="m2"><p>خیال آتشین رخساره ای شمع نظر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کهن ویرانهٔ عالم، حزین از من خطر دارد</p></div>
<div class="m2"><p>که طوفانی نهان در آستین از چشم تر دارم</p></div></div>