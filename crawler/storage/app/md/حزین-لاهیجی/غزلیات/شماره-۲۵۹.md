---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>هر زخم که از ناوک آن تازه نهال است</p></div>
<div class="m2"><p>بر پیکر من شوختر از چشم غزال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی شده سرمست مرا، بس که تغافل</p></div>
<div class="m2"><p>یک بار نپرسید ز حالم که چه حال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هجران،گل حرمان حجاب نظر توست</p></div>
<div class="m2"><p>گر دیده گشایی همه جا بزم وصال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دام خیالت شده ام، شکل خیالی</p></div>
<div class="m2"><p>یک ره به خیالت نرسد کاین چه خیال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینه آن صنع بود ناقص وکامل</p></div>
<div class="m2"><p>این قصه چرا طول دهم، عرض کمال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردی کش میخانهٔ ما شو که نیابی</p></div>
<div class="m2"><p>در جام جم این باده که ما را به سفال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرواز حزین از پی آرام اسیری ست</p></div>
<div class="m2"><p>بر معتکف دام و قفس بال وبال است</p></div></div>