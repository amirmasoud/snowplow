---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>از پرده چو خواهد، گل رخسار برآرد</p></div>
<div class="m2"><p>پوشد به لباس گل و از خار برآرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از خم زلفش چه خیال است برآرم؟</p></div>
<div class="m2"><p>چون آینه کز سبزه ی زنگار برآرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز مگر همّت مردانه ی ساقی</p></div>
<div class="m2"><p>بنیاد غم از ساغر سرشار برآرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افسرده دلی رفت ز حد، شور جنون کو؟</p></div>
<div class="m2"><p>تا بی خودم از خانهٔ خمار برآرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی سر زلف تو دهد طرح به سنبل</p></div>
<div class="m2"><p>آهی که حزین از دل افگار برآرد</p></div></div>