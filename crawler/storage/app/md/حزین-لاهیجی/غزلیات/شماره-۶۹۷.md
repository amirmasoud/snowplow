---
title: >-
    شمارهٔ ۶۹۷
---
# شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>دو روزی کز قضا بایست با این کاروان باشم</p></div>
<div class="m2"><p>مرا کم قیمتی نگذاشت بر طبعی گران باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قید سخت رویانم، ملایم طینتی دارم</p></div>
<div class="m2"><p>چو مغز از چرب و نرمی در شکنج استخوان باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آب و گل نشاند از باغ جان، قدسی نهالم را</p></div>
<div class="m2"><p>فلک می خواست چون گل دست فرسودِ خزان باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر تسلیم و خاک عجز و آداب رضاجویی</p></div>
<div class="m2"><p>اگر باید که دور از کوی آن آرام جان باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین غربت به افسونهای مهر آشنارویان</p></div>
<div class="m2"><p>اگر بندم دلی، از بی وفایان جهان باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیندازم به فرش سنبل و گل، طرح آسایش</p></div>
<div class="m2"><p>درین بستان سرا، هم مشرب آب روان باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی باشم زیان خواه کسی چون شمع در محفل</p></div>
<div class="m2"><p>اگر باشم، زیان خویش و سود دیگران باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز همراهان ندارم بار منّت یک سر سوزن</p></div>
<div class="m2"><p>درین وادی چه افتاده ست،از خواری کشان باشم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم رنجد حزین ازگفتگوی صورت آرایان</p></div>
<div class="m2"><p>اگر سنجد لب معنی، حدیثی ترجمان باشم</p></div></div>