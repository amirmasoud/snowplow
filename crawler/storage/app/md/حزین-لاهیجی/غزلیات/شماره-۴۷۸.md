---
title: >-
    شمارهٔ ۴۷۸
---
# شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>بر سر تربتم آن نوگل خندان آرید</p></div>
<div class="m2"><p>سست پیمان مرا بر سر پیمان آرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاک این سینه به دامان قیامت رفته ست</p></div>
<div class="m2"><p>تاری از زلفش و آن سوزش مژگان آرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بود منتظر و شوق نمی آید باز</p></div>
<div class="m2"><p>هدهد شهر سبا را به سلیمان آرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهد و تقوا به در آرید سر، از خرقهٔ من</p></div>
<div class="m2"><p>کفر زلفی به کفم آمده، ایمان آرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موقع شادی اصحاب و غم اغیار است</p></div>
<div class="m2"><p>محرمان را به سراپردهٔ سلطان آرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده نوشان مغان، دیدهٔ انجم شور است</p></div>
<div class="m2"><p>نور چشم قدح از کوری ایشان آرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بادهٔ سرختر از خون سیاووش کجاست؟</p></div>
<div class="m2"><p>که رخ زرد مرا رنگ به عنوان آرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه شود خاطر آشفتهٔ ما جمع شود؟</p></div>
<div class="m2"><p>خبری از سر آن زلف پریشان آرید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خامه شکّرشکن از عارف روم است حزین</p></div>
<div class="m2"><p>طوطیان را به صلا در شکرستان آرید</p></div></div>