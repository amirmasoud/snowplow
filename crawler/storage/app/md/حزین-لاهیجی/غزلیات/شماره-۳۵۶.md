---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>در دل غم آن لاله عذار است ببینید</p></div>
<div class="m2"><p>این باده که بی رنج خمار است ببینید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد چشم مرا نکهت پیراهن یوسف</p></div>
<div class="m2"><p>گردی که از آن راهگذار است ببینید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان تازه کند لفظ خوش و معنی رنگین</p></div>
<div class="m2"><p>حسنی که در آن خط غبار است ببینید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یار که چاک است ازو جامهٔ جان ها</p></div>
<div class="m2"><p>آسایش آغوش وکنار است ببینید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستغرق وصلند درین بزم حریفان</p></div>
<div class="m2"><p>دل آینه، یار آینه دار است ببینید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آرزوی بلبل بی بال و پر ما</p></div>
<div class="m2"><p>گلها همه آغوش و کنار است ببینید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پردهء زلف است تجلّی گه رویش</p></div>
<div class="m2"><p>شمعی که فروغ شب تار است ببینید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در راه وفا حال پریشان حزین را</p></div>
<div class="m2"><p>کاشفته تر از طره یار است ببینید</p></div></div>