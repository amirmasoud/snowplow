---
title: >-
    شمارهٔ ۸۷۷
---
# شمارهٔ ۸۷۷

<div class="b" id="bn1"><div class="m1"><p>حیران لقایی شدم امروزکه دانی</p></div>
<div class="m2"><p>باقی به بقایی شدم امروز که دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار آمد و جان گشت فدای قدم او</p></div>
<div class="m2"><p>قربان وفایی شدم امروز که دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فیض نظر پیر خرابات، بنازم</p></div>
<div class="m2"><p>خاک کف پایی شدم امروز که دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنگ تن، از آیینهٔ جان پاک زدودم</p></div>
<div class="m2"><p>یعنی به صفایی شدم که امروز که دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگرفت مرا از خود و خود را به عوض داد</p></div>
<div class="m2"><p>ممنون عطایی شدم امروز که دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شرک دویی، ترک خودی کرد خلاصم</p></div>
<div class="m2"><p>از خود به خدایی شدم امروز که دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوته نظری حلقهٔ بیرون درم داشت</p></div>
<div class="m2"><p>محرم، به سرایی شدم امروز که دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فقر شب هستی، چو گدا دربدرم داشت</p></div>
<div class="m2"><p>آسوده به جایی شدم امروز که دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شیوهٔ آن حسن، خبردار نبودم</p></div>
<div class="m2"><p>مفتون ادایی شدم امروز که دانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر پرده که نی راست، حزین از دم نایی ست</p></div>
<div class="m2"><p>بیخود به نوایی شدم امروز که دانی</p></div></div>