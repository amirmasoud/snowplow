---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>حیرانی من محرم آن روی چو ماه است</p></div>
<div class="m2"><p>این دیده چراغی ست که بی دود سیاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رونق ده حسن است فراوانی عاشق</p></div>
<div class="m2"><p>آرایش رخساره ی شه، گرد سپاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل خانه تهی کرده ز خود، تا تو درآیی</p></div>
<div class="m2"><p>چون حلقه ی در، دیده ی ما چشم به راه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید که اثر شانه زند، زلف اجابت</p></div>
<div class="m2"><p>تا پارهٔ دل در شکن طرهٔ آه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تهمت به اجل بسته عبث مفتی ملت</p></div>
<div class="m2"><p>بر محضر جانبازی ما عشق گواه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صیاد مرا دیده ی من حلقه ی دامی ست</p></div>
<div class="m2"><p>مژگان تماشا نگهان، مهر گیاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جایی که دهد پیر مغان جام صبوحی</p></div>
<div class="m2"><p>عذریست تو را توبه، که بدتر ز گناه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دامن عزلت بشکن پای طلب را</p></div>
<div class="m2"><p>غربال صفت عرصه ی گیتی همه چاه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم بار گشاید چو به سر وقت من آید</p></div>
<div class="m2"><p>در ره گذرد هر که، دلم قافله گاه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تلخی کش پیمانه ی مرد افکن عمرم</p></div>
<div class="m2"><p>هر مو به تن خستهٔ من مار سیاه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون شمع دل و دیده کدام است حزین را</p></div>
<div class="m2"><p>چشم و دل عاشق همه اشک و همه آه است</p></div></div>