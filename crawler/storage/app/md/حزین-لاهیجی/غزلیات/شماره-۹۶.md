---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>چه گیرایی ست یارب جلوه گیسوکمندان را؟</p></div>
<div class="m2"><p>که بگسست از صنم، پیوند جان زناربندان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیامت پیش ازین می ریخت بر دل طرح آشوبی</p></div>
<div class="m2"><p>کنون چون سایه در خاک است، این بالابلندان را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود تخت روان، هر جا رمیدن بستر اندازد</p></div>
<div class="m2"><p>سر زانو بود بالین راحت، دردمندان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا در عشق او، دل گر فغان برداشت معذورم</p></div>
<div class="m2"><p>در آتش ناله ای ناچار، می باشد سپندان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبسم ریز شد گلبرک یار و شرم رسوایی</p></div>
<div class="m2"><p>لب از دندان شبنم می گزد، گل های خندان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود هم نسبتان را عقد جمعیت به هم، فطری</p></div>
<div class="m2"><p>نباشد رشته ای درکار، گوهرهای دندان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهشت نقد در بر، حسن آن سیمین بدن دارد</p></div>
<div class="m2"><p>که بینم سیر چشم نعمتش، مشکل پسندان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حزین ، افتاد دل را در بغل گنجینه داغی</p></div>
<div class="m2"><p>که دولت خود به خود رو آورد اقبال مندان را</p></div></div>