---
title: >-
    شمارهٔ ۶۹۵
---
# شمارهٔ ۶۹۵

<div class="b" id="bn1"><div class="m1"><p>آن نرگس میگسار دیدم</p></div>
<div class="m2"><p>آسودگی از خمار دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل جز ز خط و رخ تو نشکفت</p></div>
<div class="m2"><p>بسیار گل و بهار دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شانه تمام، چاک شد دل</p></div>
<div class="m2"><p>تا زلف تو در کنار دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را به قرار عشقبازی</p></div>
<div class="m2"><p>صد شکر که بی قرار دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتشکده های دین ودل سوز</p></div>
<div class="m2"><p>در سینهٔ داغدار دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پیچ و خم شکنج زلفت</p></div>
<div class="m2"><p>آسایش روزگار دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای دل خویش درگل اشک</p></div>
<div class="m2"><p>درکوی تو استوار دیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسانه عشق خود چو مجنون</p></div>
<div class="m2"><p>افسانهٔ روزگار دیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطرب ز نوای عارف روم</p></div>
<div class="m2"><p>این پرده بزن که یار دیدم</p></div></div>