---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>شب که در خلوت اندیشه تمنّای تو بود</p></div>
<div class="m2"><p>گل داغ دل من انجمن آرای تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه در آینه ام پرتو رخسار تو داشت</p></div>
<div class="m2"><p>سینه آتشکدهٔ حسن دلارای تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مژه بر هم نزدم آینه سان در همه عمر</p></div>
<div class="m2"><p>بس که در دیدهٔ من ذوق تماشای تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل شیدا شده ام داغ تمنّای تو داشت</p></div>
<div class="m2"><p>سر سودا زده ام خاک کف پای تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید آهونگهان، غمزهٔ غمّاز تو کرد</p></div>
<div class="m2"><p>دام جادوصفتان، زلف چلیپای تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق سرکش اثر از حسن گلوسوز تو داشت</p></div>
<div class="m2"><p>داغ حسرت گلی از دامن صحرای تو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفر و دین را به کسی فتنهٔ چشمت نگذاشت</p></div>
<div class="m2"><p>در سواد حرم و بتکده غوغای تو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باده در ساغر دل نرگس مخمور تو ریخت</p></div>
<div class="m2"><p>مستی ما همه از جام مصفّای تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل باغ نظرم غنچهٔ سیراب تو شد</p></div>
<div class="m2"><p>سرو بستان دلم قامت رعنای تو بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوهر عاشق سرگشته و معشوق یکیست</p></div>
<div class="m2"><p>در حقیقت من و ما، موجهٔ دریای تو بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشئه ها داشت حزین ، سجدهٔ مستانهٔ تو</p></div>
<div class="m2"><p>دُرد میخانه مگر خاک مصلّای تو بود؟</p></div></div>