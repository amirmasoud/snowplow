---
title: >-
    شمارهٔ ۸۳۵
---
# شمارهٔ ۸۳۵

<div class="b" id="bn1"><div class="m1"><p>که گفتت گرد سر آن طرهٔ عنبرفشان بندی؟</p></div>
<div class="m2"><p>ز ابر خط به خورشید قیامت سایه بان بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی آموزمت منع نگاه از دشمنان کردن</p></div>
<div class="m2"><p>خدا ناکرده میترسم که چشم از دوستان بندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلید فتح مطلبها لب خاموش می باشد</p></div>
<div class="m2"><p>در اقبال بگشاید، اگر قفل زبان بندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خون خواهد نشاندن، تیغ بی باک مکافاتش</p></div>
<div class="m2"><p>چرا باید به کین خصم سنگین دل میان بندی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صباح شادمانی تحفه آرد، شکر و شیرت</p></div>
<div class="m2"><p>اگر از خوردن غم های بی حاصل دهان بندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حجاب از راه برخیزد، نقاب آن ماه بگشاید</p></div>
<div class="m2"><p>اگر یک دم در دل را، به روی این و آن بندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین ازگوشهٔ بیت الحزن بیرون منه پا را</p></div>
<div class="m2"><p>تو با این بسته بالیها، چه طرف از بوستان بندی؟</p></div></div>