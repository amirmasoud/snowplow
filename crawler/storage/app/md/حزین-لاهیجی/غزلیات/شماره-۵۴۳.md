---
title: >-
    شمارهٔ ۵۴۳
---
# شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>از کمال خویش نالم نی ز جور روزگار</p></div>
<div class="m2"><p>زیر بار خود بود دستم، چو شاخ میوه دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معصیت را خرد مشمر در دیار بندگی</p></div>
<div class="m2"><p>عالمی را می توان آتش زدن از یک شرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد منگر نگذرد از خاطر او دور نیست</p></div>
<div class="m2"><p>آفتاب آن جا که باشد، سایه را نبود گذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهمت عیش از می گلرنگ، بیجا می کشم</p></div>
<div class="m2"><p>گریهٔ خونین بود چون شیشه ما را درکنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هوای آنکه بنماید رخ، آن صبح امید</p></div>
<div class="m2"><p>جان به کف دارد حزین ، چون شمع از بهر نثار</p></div></div>