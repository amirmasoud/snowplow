---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>لب تشنهٔ تیغیم، ز کوثر چه گشاید؟</p></div>
<div class="m2"><p>دریا کش زخمیم، ز ساغر چه گشاید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سایهٔ داغیم، ز خورشید چه منّت؟</p></div>
<div class="m2"><p>همسایهٔ بختیم، ز اختر چه گشاید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارو ندهد سود به بیمار محبت</p></div>
<div class="m2"><p>عمر ار گذرد تلخ ز شکر چه گشاید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تمکین رود از دست، دل آید چو به طوفان</p></div>
<div class="m2"><p>دریا چو به هم خورد ز لنگر چه گشاید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناصح چه دهد بیهده بر باد نفس را</p></div>
<div class="m2"><p>دیوانهٔ عشقم، ز فسونگر چه گشاید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در طالع خود بیند اگر دولت وصلت</p></div>
<div class="m2"><p>آیینه، نظر پیش سکندر چه گشاید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر زخم به روی دل عاشق در فتحی ست</p></div>
<div class="m2"><p>زین بیش ز تیغ تو ستمگر چه گشاید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا یار شد از دیده، نهادم مژه بر هم</p></div>
<div class="m2"><p>شهباز نظر دوخته ام پر چه گشاید؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بزم گشایند چو دیوان حزین را</p></div>
<div class="m2"><p>خمّار، خم میکده را سر چه گشاید؟</p></div></div>