---
title: >-
    شمارهٔ ۸۶۶
---
# شمارهٔ ۸۶۶

<div class="b" id="bn1"><div class="m1"><p>طبیب من، حرا از خسته جان خود نمی پرسی؟</p></div>
<div class="m2"><p>توان پرسیدنی، وز ناتوان خود نمی پرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قلم کی محرم و قاصد کجا درد سخن دارد</p></div>
<div class="m2"><p>چرا احوال ما را، از زبان خود نمی پرسی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر آگه نیی از سوختن ای شمع بی پروا</p></div>
<div class="m2"><p>که از پروانهٔ آتش به جان خود نمی پرسی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم آشفته می گوید، سراغ نافهٔ چین را</p></div>
<div class="m2"><p>چرا از طرّهٔ عنبرفشان خود نمی پرسی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر باور نداری شرح جور از من، چرا باری</p></div>
<div class="m2"><p>حدیثی از دل نامهربان خود نمی پرسی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکار خسته می داند، عیار سختی بازو</p></div>
<div class="m2"><p>چرا از زخم دل، زور کمان خود نمی پرسی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرت گردم، چه دیدی کز حزین گردانده ای دل را</p></div>
<div class="m2"><p>ز دستان سنج دیرین، داستان خود نمی پرسی؟</p></div></div>