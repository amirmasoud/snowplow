---
title: >-
    شمارهٔ ۶۰۴
---
# شمارهٔ ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>جاری چو به یاد رخ جانان شودم اشک</p></div>
<div class="m2"><p>گلپوش تر از صحن گلستان شودم اشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی قدر شود رشته چو خالی ز گهر شد</p></div>
<div class="m2"><p>کو عشق که آویزهٔ مژگان شودم اشک؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جلوهٔ مستانهٔ آن سرو قباپوش</p></div>
<div class="m2"><p>چالاکتر از سیل بهاران شودم اشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستانه رگ ابر تری از مژه برخاست</p></div>
<div class="m2"><p>تا صف شکن زهد فروشان شودم اشک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حسرت نظارهٔ آن ناوک مژگان</p></div>
<div class="m2"><p>در سینه گره گردد و پیکان شودم اشک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ویرانهٔ عالم شده محتاج به سیلی</p></div>
<div class="m2"><p>بگذار حزین آفت دوران شودم اشک</p></div></div>