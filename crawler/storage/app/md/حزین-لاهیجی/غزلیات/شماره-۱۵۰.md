---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>عاشق مهجور، وصل دلستان بیند به خواب</p></div>
<div class="m2"><p>دیدهٔ محتاج، گنج شایگان بیند به خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد این چشم من آن سرو روان بیند به خواب</p></div>
<div class="m2"><p>دیدهٔ عاشق مگر بخت جوان بیند به خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل کجا و طرهٔ نازک نهالان از کجا؟</p></div>
<div class="m2"><p>مرغ بی بال و پر ما، آشیان بیند به خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرگ عاشق گفتم او را مهربان سازد نشد</p></div>
<div class="m2"><p>قمری ما سرو او را سرگران بیند به خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت بیدار را در دیده ریزم خاک خشک</p></div>
<div class="m2"><p>گرجبینم سجدهٔ آن آستان بیند به خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرگ هر کس در حقیقت نقش حال زندگی ست</p></div>
<div class="m2"><p>هر چه کس بیند به بیداری، همان بیند به خواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح محشر سرگران برخیزد از خواب لحد</p></div>
<div class="m2"><p>گر شبی زاهد، خرابات مغان بیند به خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصا ازکف رفته را دیگرکجا یابی حزین ؟</p></div>
<div class="m2"><p>در خزان بلبل بهار بی خزان بیند به خواب</p></div></div>