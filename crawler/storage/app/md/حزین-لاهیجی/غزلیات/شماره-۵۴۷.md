---
title: >-
    شمارهٔ ۵۴۷
---
# شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>سحر ز بستر نسرین سبک عنان برخیز</p></div>
<div class="m2"><p>به پای گل بنشین، مست و می کشان برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرشمه می برد از حد نهال و جلوه، سمن</p></div>
<div class="m2"><p>نگار من، پی تاراج گلستان برخیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چین جبهه نیرزد چو گل دو روز حیات</p></div>
<div class="m2"><p>شکفته با همه بنشین و مهربان برخیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا به میکده بنشین به کام دل زاهد</p></div>
<div class="m2"><p>شراب کهنه ما نوش کن ، جوان برخیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آستان گدایان شبی سری بگذار</p></div>
<div class="m2"><p>به مدعای دل خویش کامران برخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اساس عشق من و حسن یار محکم باد</p></div>
<div class="m2"><p>بهار گو برو و مرغ از آشیان برخیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلاست رشک محبت بر اهل درد حزین</p></div>
<div class="m2"><p>چو شد وصال میسر، خود از میان برخیز</p></div></div>