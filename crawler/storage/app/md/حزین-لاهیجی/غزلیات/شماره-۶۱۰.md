---
title: >-
    شمارهٔ ۶۱۰
---
# شمارهٔ ۶۱۰

<div class="b" id="bn1"><div class="m1"><p>جهان ساده پر کار است از نقش و نگار دل</p></div>
<div class="m2"><p>سر زانوی حیرانی بود، آیینه دار دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود نازک تر از دل پرده گوش گران گل</p></div>
<div class="m2"><p>اگر بلبل نواسنجی کند در نوبهار دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مجنون کرده لیلی دستگاهان را بیابانی</p></div>
<div class="m2"><p>که را تا رام گردد آهوی وحشت شعار دل؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آن شمعی که سازد پرتو خورشید ناچیزش</p></div>
<div class="m2"><p>فروغ مهر تابان محو گردد در شرار دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمال غیب را بی پرده منظور نظر دارد</p></div>
<div class="m2"><p>چراغ طور باشد دیدهٔ شب زنده دار دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خود پیچد ز شرم اندیشهٔ کوته کمند اینجا</p></div>
<div class="m2"><p>سر رفعت به بام عرش می ساید حصار دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبک چون گرد برخیزد، دو عالم از سر راهش</p></div>
<div class="m2"><p>به میدانی که گردد جلوه گر چابک سوار دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حباب شوخ نتواند کشیدن جام دریا را</p></div>
<div class="m2"><p>به دست دیده نگذاری عنان اختیار دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غبار تن که می شد توتیای چشم آگاهی</p></div>
<div class="m2"><p>چو خاک انباشتی غافل، به چشم اعتبار دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو تخمی سوخت، بی حاصل بود از ابر میزانی</p></div>
<div class="m2"><p>مگر اشک ندامت بشکفاند نوبهار دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فتد چون عقده مشکل، ناخن تدبیر خود گردد</p></div>
<div class="m2"><p>غم دیرینه خواهدگشت آخر، غمگسار دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صلا از من تهیدستان بازار محبت را</p></div>
<div class="m2"><p>ز داغ عشق دارم پرگهر جیب و کنار دل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو ابر از سیر گلشن گر صبوحی کرده بازآیی</p></div>
<div class="m2"><p>به سیل جلوه خواهد رفت بنیاد خمار دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کند سیل بلاگرکشتی افلاک طوفانی</p></div>
<div class="m2"><p>نمی افتد تزلزل در بنای استوار دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به رخ هرگز ز خاک خشک مغزش گرد ننشیند</p></div>
<div class="m2"><p>خوشا سیلی که گردد غرق بحر بی کنار دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به امّیدی که نخل عاشقی روزی به بار آید</p></div>
<div class="m2"><p>به خون می پرورم نخل تو را در جویبار دل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حزین از ناله عاشق تسلی می شود عاشق</p></div>
<div class="m2"><p>اسیران را صفیری می زنم، از شاخسار دل</p></div></div>