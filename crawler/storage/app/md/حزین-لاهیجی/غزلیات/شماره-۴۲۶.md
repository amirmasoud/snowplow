---
title: >-
    شمارهٔ ۴۲۶
---
# شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>کار رسوایی ما حیف به پایان نرسید</p></div>
<div class="m2"><p>نارسا طالع چاکی که به دامان نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر آن شبنم لب تشنه مرا می سوزد</p></div>
<div class="m2"><p>که به سرچشمهٔ خورشید درخشان نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به پای علم دار نیاوردش عشق</p></div>
<div class="m2"><p>سر شوریدهٔ منصور به سامان نرسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع بالین من خسته شد آن گاه رخش</p></div>
<div class="m2"><p>کز ضعیفی نگهم تا سر مژگان نرسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم دارم که رسد گریهٔ مستانه به داد</p></div>
<div class="m2"><p>گر به سرمنزل ما سیل بهاران نرسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده دیری ست که در راه غبار در توست</p></div>
<div class="m2"><p>نکهت مصر سفر کرد و به کنعان نرسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من گرفتم به قفس تن زنم از دوری گل</p></div>
<div class="m2"><p>چون ننالم که فغانم به گلستان نرسید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگه عجز عجب قوّت تقریری داشت</p></div>
<div class="m2"><p>این ستم شد که به آن چشم سخندان نرسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس صبح قیامت علم افراشت حزین</p></div>
<div class="m2"><p>شب افسانهٔ ما خوش که به پایان نرسید</p></div></div>