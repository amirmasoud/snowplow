---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>نمی‌گوید کسی امروز چرخ بی‌مروت را</p></div>
<div class="m2"><p>که تا کی می‌خوری چون آب، خون اهل غیرت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صف برگشته مژگانی که من سرگشتهٔ اویم</p></div>
<div class="m2"><p>چو مجنون برده از چشم غزالان خواب راحت را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود هرگوشه برپا محشر داغ نمک سودی</p></div>
<div class="m2"><p>ببین در سینه من شور صحرای قیامت را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک را فارغ از تدبیر کار رزق خود کردم</p></div>
<div class="m2"><p>گزیدم شمع سان از بس که انگشت ندامت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عادت اینکه در هر لمحه مژگان می زنی برهم</p></div>
<div class="m2"><p>کف افسوس باشد چشم خواب آلود غفلت را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حزین گر می کنی، پیش از رقیبان جان نثارش را</p></div>
<div class="m2"><p>مکن چون غافلان از کف رها دامان فرصت را</p></div></div>