---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>از غم، دل حیران چه خبر داشته باشد؟</p></div>
<div class="m2"><p>محو تو، ز هجران چه خبر داشته باشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سرو گل اندام که دلها چمن اوست</p></div>
<div class="m2"><p>از خانه به دوشان چه خبر داشته باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حال تذروان پر و بال شکسته</p></div>
<div class="m2"><p>آن سرو خرامان چه خبر داشته باشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن شوخ که در خانهٔ آیینه کند سیر</p></div>
<div class="m2"><p>از آبله پایان چه خبر داشته باشد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طفلی که ز مستی نشناسد سر و پا را</p></div>
<div class="m2"><p>از بیسر و پایان چه خبر داشته باشد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هستی ست،که در عشق فراموش شد اول</p></div>
<div class="m2"><p>مجنون تو از جان چه خبر داشته باشد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بهله کف از کار فتاده ست حزین را</p></div>
<div class="m2"><p>از دامن جانان چه خبر داشته باشد؟</p></div></div>