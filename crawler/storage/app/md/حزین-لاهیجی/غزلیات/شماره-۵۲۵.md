---
title: >-
    شمارهٔ ۵۲۵
---
# شمارهٔ ۵۲۵

<div class="b" id="bn1"><div class="m1"><p>بدآموز وفا کی قدر ناز یار می داند؟</p></div>
<div class="m2"><p>دل من لذّت آن غمزهٔ خونخوار می داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم من می کند تکلیف چشمش باده پیمایی</p></div>
<div class="m2"><p>غبار خاطرم را ابر دامندار می داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یک ساغر برافکن پرده شرم و حیا ساقی</p></div>
<div class="m2"><p>حجاب عشق را دل در میان دیوار می داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشم امّت مشرب، اگر کام امید من</p></div>
<div class="m2"><p>شکرخند تو را از تلخی گفتار می داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گل چینم من آزرده دل از روضهٔ رضوان؟</p></div>
<div class="m2"><p>که دوش بی دماغان بوی گل را خار می داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کف در عاشقی سر رشتهٔ دانش رها کردم</p></div>
<div class="m2"><p>دل من، کافرم گر سبحه و زنّار می داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین تایید دل دیدار بینم روشناس او</p></div>
<div class="m2"><p>نگاه بی ادب را در میان بیکار می داند</p></div></div>