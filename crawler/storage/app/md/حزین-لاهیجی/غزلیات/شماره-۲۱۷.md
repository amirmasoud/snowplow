---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>دل در حریم وصل تو، پا را نگه نداشت</p></div>
<div class="m2"><p>داغم ازین سپند که جا را نگه نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن نشد چراغ دل و دیده اش چو شمع</p></div>
<div class="m2"><p>هر سر که زیر تیغ تو پا را نگه نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنهان نگشت در دل صد چاک، راز عشق</p></div>
<div class="m2"><p>این خانهٔ شکسته، هوا را نگه نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریوزهٔ نگاهی از آن شاه داشتم</p></div>
<div class="m2"><p>بگذشت سرگران و گدا را نگه نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب تشنه تر ز غیرت عشقم به خون اشک</p></div>
<div class="m2"><p>در دیده خاک آن کف پا را، نگه نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرسود از اشتیاق سگت استخوان من</p></div>
<div class="m2"><p>افسوس ازو،که حق وفا را نگه نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلکت نشد خموش حزین ، در بهار و دی</p></div>
<div class="m2"><p>این عندلیب مست، نوا را نگه نداشت</p></div></div>