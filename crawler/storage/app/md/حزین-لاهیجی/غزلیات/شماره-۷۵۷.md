---
title: >-
    شمارهٔ ۷۵۷
---
# شمارهٔ ۷۵۷

<div class="b" id="bn1"><div class="m1"><p>نمودی جلوه ای شیرین شمایل در خیال من</p></div>
<div class="m2"><p>حنای پای گلگونت شود، خون حلال من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرانی می کشد از تار کاکل، سرو ناز تو</p></div>
<div class="m2"><p>نداری طاقت بار دلی، نازک نهال من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به این ضعفی که نتوانم نمودن راست، قامت را</p></div>
<div class="m2"><p>کشیدی بر سرم تیغ جفا، ابرو هلال من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زتیغت بسمل من زخم دیگر آرزو دارد</p></div>
<div class="m2"><p>هلاک خویت ای بیدادگر، رحمی به حال من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنم دل شد، دل من جان، بنازم همّتت ساقی</p></div>
<div class="m2"><p>به یک پیمانهٔ می، جام جم کردی سفال من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی یابد به جنت عاشق از قید غم آزادی</p></div>
<div class="m2"><p>نمی گردد زگلشن شاد، مرغ بسته بال من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین چون غنچه برلب می زنم مهرخموشی را</p></div>
<div class="m2"><p>مبادا در دلش رحم آورد عرض ملال من</p></div></div>