---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>چون نخل تو از ناز گرانبار برآید</p></div>
<div class="m2"><p>شمشاد ز جا، سرو ز رفتار برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل می رود از سینه و پیکان تو باقی ست</p></div>
<div class="m2"><p>رحم است بر آن یار که از یار برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرمندهٔ عشقیم که بی چاره و تدبیر</p></div>
<div class="m2"><p>آسان کند آن کار که دشوار برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ناخن عشقم رگ جان زمزمه ساز است</p></div>
<div class="m2"><p>بی زخمه صدا کی شود، از تار برآید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذار حزین از کف خود بادهٔ پندار</p></div>
<div class="m2"><p>تا ساغرت از میکده سرشار برآید</p></div></div>