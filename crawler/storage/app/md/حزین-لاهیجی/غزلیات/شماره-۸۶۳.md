---
title: >-
    شمارهٔ ۸۶۳
---
# شمارهٔ ۸۶۳

<div class="b" id="bn1"><div class="m1"><p>برده ست غمت، دست و دل ازکار، کجایی؟</p></div>
<div class="m2"><p>ای مونس دلهای گرفتار کجایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر غنچه ز بویت به شکرخند بهار است</p></div>
<div class="m2"><p>ای چشم و چراغ دل بیدارکجایی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند سرآریم به تاریکی هجران</p></div>
<div class="m2"><p>ای شمع فروزان شب تار، کجایی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی بی من ونه با منی ازناز، چه حال است</p></div>
<div class="m2"><p>ای عهدشکن یار وفادار کجایی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل های گلستان، همه پروردهٔ خارند</p></div>
<div class="m2"><p>عارض بنما، ای گل بی خار کجایی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قدّ و رخت، بلبل و قمری به سرودند</p></div>
<div class="m2"><p>ای جلوه طراز گل و گلزار کجایی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آنکه بود جلوه گهت، کوچه و بازار</p></div>
<div class="m2"><p>ای یار نه در کوچه و بازار، کجایی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر هم زده ام خانه ی دل را به سراغت</p></div>
<div class="m2"><p>چون نیست کسی غیر تو در دار، کجایی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگشا گره از کار فروبستهٔ دل ها</p></div>
<div class="m2"><p>ای عقده گشایندهٔ هر کار کجایی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای نور یقین، چشم جهان بین دو عالم</p></div>
<div class="m2"><p>ای جان حزین ، ای دل و دلدار کجایی؟</p></div></div>