---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>یک دل به دیاری که وفا صاحب تاج است</p></div>
<div class="m2"><p>بی سکهٔ داغت نبود، آنچه رواج است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهنشهیم باج ز افتاده نگیرد</p></div>
<div class="m2"><p>هر سرکه بلند است، مرا زیر خراج است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کودک یونان کدهٔ صاف دلانم</p></div>
<div class="m2"><p>لوح سبقم ساده تر از صفحهٔ عاج است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیماری عشق است، چه آید ز مسیحا؟</p></div>
<div class="m2"><p>بی فایده جان می کنم و مرگ علاج است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر لحظه فلک لعبتی از پرده برآرد</p></div>
<div class="m2"><p>این پیر خرف، بین چقدر طفل مزاج است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دولت از این عرصه که ماییم کران گیر</p></div>
<div class="m2"><p>از ما سر پا خورده، به هر جا سر و تاج است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گم شد ره بیرون شد، از آن زلف حزین را</p></div>
<div class="m2"><p>ای دل بفروز آتش آهی، شب داج است</p></div></div>