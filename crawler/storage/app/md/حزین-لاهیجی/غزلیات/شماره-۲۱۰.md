---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>دل خوردن عشاق تو کار دگران نیست</p></div>
<div class="m2"><p>این لقمه، به اندازه هر کام و دهان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بیهده بستیم به نیرنگ بهاران</p></div>
<div class="m2"><p>آن رنگ کدام است که در برگ خزان نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دایره گردش افلاک ندیدیم</p></div>
<div class="m2"><p>چشمی که به دنبال نگاهت نگران نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرگرم سراغش، عبث اندیشه خورد تاب</p></div>
<div class="m2"><p>آن موی کمر چون رگ جانم به میان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنقا نگرفته ست چو من گوشه عزلت</p></div>
<div class="m2"><p>در وادی آوارگیم، نام و نشان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کم سخن است آن دهن تنگ، معاف است</p></div>
<div class="m2"><p>راه سخنی هیچ به آن غنچه دهان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسیار به دام و قفس افتاده گذارم</p></div>
<div class="m2"><p>صیاد، به بی رحمیت ای دشمن جان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردم نه همین از اثر چشم تو مستند</p></div>
<div class="m2"><p>آن شیوه کدام است که آشوب جهان نیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرین من، از تلخ عتاب تو به شکرم</p></div>
<div class="m2"><p>با لعل تو دل را شکرابی به میان نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک رنگیت ای شوخ، چها کرد به جانم</p></div>
<div class="m2"><p>این شیوه، کم از صحبت مهتاب و کتان نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با راست روان صحبت گردون نشود راست</p></div>
<div class="m2"><p>بیش از نفسی، تیر در آغوش کمان نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلطان که بود در پی آزار رعیّت</p></div>
<div class="m2"><p>گرگی ست در افتاده درین گلّه، شبان نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در سینه حزین ، آه من سوخته پیداست</p></div>
<div class="m2"><p>چون شمع که در پردهٔ فانوس نهان نیست</p></div></div>