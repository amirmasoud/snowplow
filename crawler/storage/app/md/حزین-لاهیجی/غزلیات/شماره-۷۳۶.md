---
title: >-
    شمارهٔ ۷۳۶
---
# شمارهٔ ۷۳۶

<div class="b" id="bn1"><div class="m1"><p>لب عرض شکوه خامش نه ز بیم غیر دارم</p></div>
<div class="m2"><p>ز تو بی وفا ستمگر چه امید خیر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من کعبه رانده را دل به کجا فروشد آیا؟</p></div>
<div class="m2"><p>نه لیاقت برهمن نه سزای دیر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه جا روم ولیکن ننهم برون ز دل پای</p></div>
<div class="m2"><p>قدمی چو نقطه بر جا قدمی به سیر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من ز نور احمد به چراغ طور خندد</p></div>
<div class="m2"><p>نه قفای طلحه گیرم نه سر زبیر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر سدره برفرازد ز حزین نیم بسمل</p></div>
<div class="m2"><p>هله عرشیان که از دل پر و بال طیر دارم</p></div></div>