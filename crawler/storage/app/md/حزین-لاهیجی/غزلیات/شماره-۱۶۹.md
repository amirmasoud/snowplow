---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>آب حیات در رقم مشک فام ماست</p></div>
<div class="m2"><p>از خضر خامه، زندهٔ جاوید نام ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با لذت است کام جگرهای سوخته</p></div>
<div class="m2"><p>از شور عشق تا نمکی درکلام ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر نقطهای چو خال لب یار، مشکبوست</p></div>
<div class="m2"><p>این نافه ز آهوی قلم خوش خرام ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از باده کهن سخن تازه خوشتر است</p></div>
<div class="m2"><p>پیمانه لفظ و معنی رنگین مدام ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا پیر جام جرعه به ما می دهد حزین</p></div>
<div class="m2"><p>سرجوش فیض بادهٔ معنی به جام ماست</p></div></div>