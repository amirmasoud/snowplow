---
title: >-
    شمارهٔ ۷۵۹
---
# شمارهٔ ۷۵۹

<div class="b" id="bn1"><div class="m1"><p>از اشک لاله رنگ گلی در کنار کن</p></div>
<div class="m2"><p>شاخ خزان رسیدهٔ خود را بهار کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار رزق خاک شود مشت خون من</p></div>
<div class="m2"><p>ای شوخ سرگران، کف پایی نگار کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ساغر کرام نصیبی ست خاک را</p></div>
<div class="m2"><p>ته جرعه ای به کار من خاکسار کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازکار دل به عشق گره باز می شود</p></div>
<div class="m2"><p>این دانهٔ سپند به آتش نثار کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی طاقتی کمال دهد کار عشق را</p></div>
<div class="m2"><p>اوّل به غمزه غارت صبر و قرار کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوانه را ز بند، شکوه دگر بود</p></div>
<div class="m2"><p>دل را اسیر سلسله ی تابدار کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچون سبو به جرعه، می ام در گلو مریز</p></div>
<div class="m2"><p>میخانه را به کام من میگسار کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خالی کفت ز دامن مطلب حزین چراست؟</p></div>
<div class="m2"><p>دستی چو شانه، در شکن زلف یارکن</p></div></div>