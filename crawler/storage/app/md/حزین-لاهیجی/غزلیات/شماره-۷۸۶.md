---
title: >-
    شمارهٔ ۷۸۶
---
# شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>من نه حریف وعده ام طاقت انتظار کو؟</p></div>
<div class="m2"><p>تا به اجل سپارمش جان امیدوار کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رسی ای صبا اگر از سرکوی یار من</p></div>
<div class="m2"><p>بویی از آن چمن چه شد، برگی از آن بهار کو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صف منکران کنم دعوی عشق و زنده ام</p></div>
<div class="m2"><p>تلخی حرف حق چه شد، آن همه گیر و دار کو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر که در حساب هم، فارغم از تلافیت</p></div>
<div class="m2"><p>دعوی دل به یک طرف، داغ مرا شمار کو؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی سرگران من، کشت مرا تغافلت</p></div>
<div class="m2"><p>تلخی عیش تا به کی، بادهٔ خوشگوار کو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش در توبه می زند ناصح بی خبر ولی</p></div>
<div class="m2"><p>اشک ندامت ازکجا، تهمت اختیارکو؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چارهٔ رنگ زرد من، باده نمی کند حزین</p></div>
<div class="m2"><p>نیست دلی که خون کنم، دیدهٔ اشکبار کو؟</p></div></div>