---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>لازم بود مکان طربناک، شیشه را</p></div>
<div class="m2"><p>کردم نهفته، در بغل تاک، شیشه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکم خرد به میکده جاری نمی شود</p></div>
<div class="m2"><p>اینجا ز محتسب نبود باک، شیشه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غم چو ناتوانی این خسته حال دید</p></div>
<div class="m2"><p>برداشت پیر میکده چالاک، شیشه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردت اگر شکافت دلم را شگفت نیست</p></div>
<div class="m2"><p>از زور باده، سینه شود چاک شیشه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت دلم به گوشهُ ابرو نهاده است</p></div>
<div class="m2"><p>غافل منه به طاق خطرناک شیشه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن ز بزم باده کشیدی و موج می</p></div>
<div class="m2"><p>در جیب پیرهن شده خاشاک شیشه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرقی میانه ی دل و یادت پدید نیست</p></div>
<div class="m2"><p>از می نکرد مستیم ادراک، شیشه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر شراب، بدرقه دل برده ای ز من</p></div>
<div class="m2"><p>زلف تو بسته است به فتراک، شیشه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هشیار دیده است چو ما را، ستیزه خوست</p></div>
<div class="m2"><p>باید کنون نمود به افلاک شیشه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می بایدم چو منزل بی آب را برید</p></div>
<div class="m2"><p>همراه می برم، به دل خاک شیشه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساقی، چنین به صرفه چرا باده می دهی؟</p></div>
<div class="m2"><p>سازی مباد، شهره به امساک شیشه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیدم به بزم باده، سرافکنده زاهدی</p></div>
<div class="m2"><p>محراب دیده، ساخته ناپاک شیشه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دزدی ست دست بسته، مبادا نهان کند</p></div>
<div class="m2"><p>در آستین خرقهُ ناپاک شیشه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از بزم، تا نهفته رخ، آن دلربا، حزین</p></div>
<div class="m2"><p>افتاده است دیده به کاواک شیشه را</p></div></div>