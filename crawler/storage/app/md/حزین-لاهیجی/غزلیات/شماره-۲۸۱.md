---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>نبود خطری در ره بی پا و سران هیچ</p></div>
<div class="m2"><p>رهزن نزند قافلهٔ ریگ روان هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حشمان تو مست می نازند، مبادا</p></div>
<div class="m2"><p>قسمت نرسانند به خونین جگران هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر هم زن دلها نشود موی میانت</p></div>
<div class="m2"><p>پا گر نگذارد سر زلفت به میان هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درماندهٔ سامان تهی دستی خویشم</p></div>
<div class="m2"><p>دردا که نگیرند ز عاشق دل و جان هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر جوهر خوی تو فتادهست ستمگر</p></div>
<div class="m2"><p>با ما ز چه رو جور و جفا با دگران هیچ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه رسم سلامی نه کلامی، نه پیامی</p></div>
<div class="m2"><p>دل را خبری نیست از آن غنچه دهان هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناکامی وکام تو حزین ، نقش بر آب است</p></div>
<div class="m2"><p>امید نبندی به جهان گذران هیچ</p></div></div>