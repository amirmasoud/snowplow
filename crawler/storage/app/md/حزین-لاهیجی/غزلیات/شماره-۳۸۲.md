---
title: >-
    شمارهٔ ۳۸۲
---
# شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>طاق میخانهٔ مستان خم ابروی تو بود</p></div>
<div class="m2"><p>صاف پیمانهٔ عرفان، رخ نیکوی تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرویها به هوایت دل مسکینم کرد</p></div>
<div class="m2"><p>گنج بادآور من خاک سر کوی تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح دیوانه ی آن چاک گریبان می گشت</p></div>
<div class="m2"><p>شب سیه مست خیال خط هندوی تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلبران در خم زلف تو گرفتار شدند</p></div>
<div class="m2"><p>آفت شیر شکاران، شکن موی تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار آشفته دلان، راست به ایمای تو شد</p></div>
<div class="m2"><p>شب که محراب دعا قبلهٔ ابروی تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشئه در طینت می چشم فسون سازت ریخت</p></div>
<div class="m2"><p>ساقی میکده ها نرگس جادوی تو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو قدّان همه در سایهٔ دیوار تُواند</p></div>
<div class="m2"><p>چشم آهو نگهان محو سگ کوی تو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیشه بودیم که صهبای تو بیرون زد رنگ</p></div>
<div class="m2"><p>دیده بودیم که همراه صبا بوی تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب که در بتکده نالیدی از اخلاص حزین</p></div>
<div class="m2"><p>حق پرستان همه را گوش به یاهوی تو بود</p></div></div>