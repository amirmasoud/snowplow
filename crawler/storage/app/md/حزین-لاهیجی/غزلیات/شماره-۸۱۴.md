---
title: >-
    شمارهٔ ۸۱۴
---
# شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>دل داغ تو را به جان گرفته</p></div>
<div class="m2"><p>جان درد تو جاودان گرفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال دل ناتوان چه پرسی؟</p></div>
<div class="m2"><p>حیرت زده را زبان گرفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من شده تنگ کوه و صحرا</p></div>
<div class="m2"><p>سودای توام عنان گرفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر شیشه دل صبا بود سنگ</p></div>
<div class="m2"><p>دل بی توام از جهان گرفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد که دور چرخ ما را</p></div>
<div class="m2"><p>چون دایره در میان گرفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک غنچه صبا نمی گشاید</p></div>
<div class="m2"><p>گویا دل باغبان گرفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش از داغ لاله رویی</p></div>
<div class="m2"><p>ای مجلسیان به جان گرفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر تن چه زنی گلاب و کافور</p></div>
<div class="m2"><p>این شعله در استخوان گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی بال و پرت حزین مسکین</p></div>
<div class="m2"><p>در کنج غم آشیان گرفته</p></div></div>