---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>آسان نه به پیمانهٔ سرشار شود سرخ</p></div>
<div class="m2"><p>رخسار به خون خوردن بسیار شود سرخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرف حق منصور به من سبز شد امروز</p></div>
<div class="m2"><p>وقت است ز خونم علم دار شود سرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردون نکند چارهٔ رخسارهٔ زردم</p></div>
<div class="m2"><p>آن گونه، به یک جرعه چه مقدار شود سرخ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجنون من آراسته صحرای جنون را</p></div>
<div class="m2"><p>از فیض گل آبله ام خار شود سرخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزمی که تو از می، چو گل از پرده درآیی</p></div>
<div class="m2"><p>از جام وصالت در و دیوار شود سرخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریزی به صنم خانه اگر رنگ تجلی</p></div>
<div class="m2"><p>از خون برهمن رگ زنّار شود سرخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد می لعلی عرق از چهرهٔ آلت</p></div>
<div class="m2"><p>از عکس تو در آینه زنگار شود سرخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آید به نظر چون رگ گل، تیر نگاهت</p></div>
<div class="m2"><p>دل شد چو هدف، تا لب سوفار شود سرخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاود قلمم کان بدخشان جگر را</p></div>
<div class="m2"><p>از گوهر من روی خریدار شود سرخ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین باده که من کرده ام از پردهٔ دل صاف</p></div>
<div class="m2"><p>بینید خدا را، رخ اغیار شود سرخ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون تیغ حزین ، بس که چکد از قلمت خون</p></div>
<div class="m2"><p>روی ورق ساده چو گلزار، شود سرخ</p></div></div>