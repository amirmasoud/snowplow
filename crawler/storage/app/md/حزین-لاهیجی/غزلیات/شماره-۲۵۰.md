---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>اسرار تو با زاهد و ملّا نتوان گفت</p></div>
<div class="m2"><p>با کوردلان، نور تجلّا نتوان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آینه، کز جلوهٔ دیدار شود گم</p></div>
<div class="m2"><p>ما را به تماشای تو پیدا نتوان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آمدن پیک صبا می رود از هوش</p></div>
<div class="m2"><p>پیغام تو با عاشق شیدا نتوان گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز، ازین مرحله سامان سفر کن</p></div>
<div class="m2"><p>در مذهب ما امشب و فردا نتوان گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرمستی آن طرّه به حدّی ست که با وی</p></div>
<div class="m2"><p>احوال پریشانی دلها نتوان گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیماری من از اثر مستی چشمی ست</p></div>
<div class="m2"><p>درد دل من پیش مسیحا نتوان گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این آن غزل قاسم انوار که فرمود</p></div>
<div class="m2"><p>با عشق زتسبیح مصلا نتوان گفت</p></div></div>