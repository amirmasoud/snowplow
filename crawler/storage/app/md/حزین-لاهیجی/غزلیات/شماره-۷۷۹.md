---
title: >-
    شمارهٔ ۷۷۹
---
# شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>نامه ات خواندم و می بایدم افشان کردن</p></div>
<div class="m2"><p>قطره ای چند سرشک از مژه غلتان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد ازین شکوه کنم پیشه که معلومم شد</p></div>
<div class="m2"><p>در دلت کرده اثر، شکوهٔ هجران کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زده ای طعنه به حالم که چرا صبرت نیست؟</p></div>
<div class="m2"><p>هجر را صبر نیارد به دل آسان کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفته ای پیر شدی دل ز جوانان برگیر</p></div>
<div class="m2"><p>کافر عشق محال است مسلمان کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داده ای بیم من از غمزه که خونت هدر است</p></div>
<div class="m2"><p>نرخ جان کس نتواند چو من ارزان کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده ای پند که باید ز کسان راز نهفت</p></div>
<div class="m2"><p>غم دل را نتوانم ز تو پنهان کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته ای در غم ما ترک مراد خود کن</p></div>
<div class="m2"><p>تو و بخشایش بی حد، من و عصیان کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده ای منع که دیدارپرستی کفر است</p></div>
<div class="m2"><p>عاشق از عشق محال است پشیمان کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفته ای وصل محال است، تمنا چه کنی؟</p></div>
<div class="m2"><p>چه کنم؟ ترک تمنای تو نتوان کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کردهای امر که دامان ورع پاک بشوی</p></div>
<div class="m2"><p>از جگر خون شدن و از مژه طوفان کردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفته بودی که چه خواهد دلت ای سرگردان؟</p></div>
<div class="m2"><p>گرد سر گردمت، آن طره پریشان کردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو و آن جلوهٔ مستانهٔ نظاره فریب</p></div>
<div class="m2"><p>من و جان در سر آن سرو خرامان کردن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من به خونین جگری جان و دل از کف دادن</p></div>
<div class="m2"><p>تو به جادونگهی غارت ایمان کردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این جواب غزل خواجه سنایی‌ست حزین</p></div>
<div class="m2"><p>خواهد این تازه غزل، ناز به دیوان کردن</p></div></div>