---
title: >-
    شمارهٔ ۸۲۶
---
# شمارهٔ ۸۲۶

<div class="b" id="bn1"><div class="m1"><p>در پرده خط، خال به صد ناز گرفتی</p></div>
<div class="m2"><p>از مرغ دلم دانه چرا بازگرفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردی ز شکنج قفس امروز برونم</p></div>
<div class="m2"><p>کز بال و پرم قوّت پرواز گرفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست تو به تعمیر دل ای عشق مبارک</p></div>
<div class="m2"><p>هر رخنه که بود از گهر راز گرفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیداست که ریزد پر و بال طلب ما</p></div>
<div class="m2"><p>زین اوج که در جلوه گه ناز گرفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد نغمهٔ کلک تو حزین آفت هوشم</p></div>
<div class="m2"><p>زین شعبده، کار از کف اعجاز گرفتی</p></div></div>