---
title: >-
    شمارهٔ ۴۳۳
---
# شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>تن دیده اند از من و جانم ندیده اند</p></div>
<div class="m2"><p>نامم شنیده اند و نشانم ندیده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که آورند سبک در نظر مرا</p></div>
<div class="m2"><p>بیچارگان، به کوی مغانم ندیده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قومی که سرکشند ز نخوت بر آسمان</p></div>
<div class="m2"><p>بر آستان میکده شانم ندیده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آوارگان دهر شمارندم ابلهان</p></div>
<div class="m2"><p>در لامکان قدس مکانم ندیده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمعی که شک به شان سلیمانیم کنند</p></div>
<div class="m2"><p>زیر نگین، زمین و زمانم ندیده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب تشنگان بادیهٔ شوق سلسبیل</p></div>
<div class="m2"><p>آب حیات شعر روانم ندیده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنها زنند لاف به میدان گفتگو</p></div>
<div class="m2"><p>آنان که ذوالفقار زبانم ندیده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مانده اند در صف دعوی، گران رکاب</p></div>
<div class="m2"><p>چالاکیی ز دست و عنانم ندیده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پوشیده است دیدهٔ نادیدگان حزین</p></div>
<div class="m2"><p>عنقای مغربم که نشانم ندیده اند</p></div></div>