---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه ساقی مجلس نقاب بردارد</p></div>
<div class="m2"><p>غبار توبهام از دل، شراب بردارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهین منّت دریا نمی توان گشتن</p></div>
<div class="m2"><p>بگو به ابر، ز چشم من آب بردارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رنگ نافه کند خون به دل اسیران را</p></div>
<div class="m2"><p>چو عارضت اثر از مشک ناب بردارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دل دگر چه توقع، نگاه گرم تو را؟</p></div>
<div class="m2"><p>بگو خراج ز ملک خراب بردارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو چنگ، پشت حزین شد ز غم دوتا و هنوز</p></div>
<div class="m2"><p>نشد که گوش ز چنگ و رباب بردارد</p></div></div>