---
title: >-
    شمارهٔ ۷۵۱
---
# شمارهٔ ۷۵۱

<div class="b" id="bn1"><div class="m1"><p>ز فیض آبرو سبز است، نخل مدعای من</p></div>
<div class="m2"><p>به آب خویش می گردد، چو گرداب آسیای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به معراجی رسانیده ست سروت سرفرازی را</p></div>
<div class="m2"><p>که ترسم کوته افتد طره ی آه رسای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی دانم به دام کیستم، لیک این قدر دانم</p></div>
<div class="m2"><p>که در خون زد گلستان را صفیرآشنای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ازکثرت نمی باشد دلیلی راه وحدت را</p></div>
<div class="m2"><p>نماید هر سر خاری، چراغی پیش پای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشاید شاهد مقصودم، آغوش اجابت را</p></div>
<div class="m2"><p>حزین از سینهٔ چاک است محراب دعای من</p></div></div>