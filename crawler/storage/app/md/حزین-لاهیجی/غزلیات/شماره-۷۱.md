---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>از نالهٔ عاشق چه خبر بوالهوسی را</p></div>
<div class="m2"><p>آری خبر از درد کسی نیست کسی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر خیره سری چاشنی درد نداند</p></div>
<div class="m2"><p>از مائدهٔ عشق، چه قسمت مگسی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخم دل نالان مرا، چاره محال است</p></div>
<div class="m2"><p>مرهم چه نهی سینهٔ چاک جرسی را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمندهٔ یک بوسه نیم زان لب جان بخش</p></div>
<div class="m2"><p>هرگز نپذیرفت ز ما، ملتمسی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلگشت چمن گر به زغن گشت مسلّم</p></div>
<div class="m2"><p>در بسته به ما داد محبت قفسی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفتند چو باد سحری، خرده شناسان</p></div>
<div class="m2"><p>چون گل به دعا می طلبم، همنفسی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با پردهٔ گوشی، نشود ساز خروشم</p></div>
<div class="m2"><p>در خاک برم حسرت فریاد رسی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با سفله، سری همّت آزاده ندارد</p></div>
<div class="m2"><p>هرگز گل دستار نسازیم خسی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفته ست حزین از گرهت تا زده ای دم</p></div>
<div class="m2"><p>حیف است غنیمت نشماری نفسی را</p></div></div>