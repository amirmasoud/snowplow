---
title: >-
    شمارهٔ ۵۸۴
---
# شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>از چشم خویش باشد، باغ و بهار درویش</p></div>
<div class="m2"><p>صد رنگ گل برآرد، اشک از کنار درویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سیل فتنه گیرد، روی زمین سراسر</p></div>
<div class="m2"><p>از جای خود نجنبد، کوه وقار درویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر، آیت جمالش، کین جلوه جلالش</p></div>
<div class="m2"><p>هستند چرخ و انجم در اختیار درویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای منکر طریقت، بر جان خود ببخشای</p></div>
<div class="m2"><p>تیغ برهنه باشد، جسم فگار درویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر باد فتنه عالم، بر یکدگر برآرد</p></div>
<div class="m2"><p>حاشا شود پریشان، مشت غبار درویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم عاشق است و معشوق،هم شاهد است و مشهود</p></div>
<div class="m2"><p>عقل آگهی ندارد، ازکار و بار درویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان حزین مسکین، از فقر زندگی یافت</p></div>
<div class="m2"><p>آب حیات باشد، در جویبار درویش</p></div></div>