---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>کامم چشید هر چه نگاهش عتاب داشت</p></div>
<div class="m2"><p>زخمم مکید تا دم شمشیر آب داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک رخنه نیست بی گل داغی به سینه ام</p></div>
<div class="m2"><p>در خانه چشم روزن من آفتاب داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میزد قدم به وادی وصف رخت، مگر</p></div>
<div class="m2"><p>کامشب به کوچه، خامهٔ ما ماهتاب داشت؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان پیشترکه چهره به می ارغوان کنی</p></div>
<div class="m2"><p>داغت چو برگ لاله دلم را کباب داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمگین نیم که لب نگشودی به پرسشم</p></div>
<div class="m2"><p>این بی زبان کجا سر و برگ جواب داشت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیرت هم از تحمّل دیدار عاجز است</p></div>
<div class="m2"><p>از عارض تو، آینه چشم پر آب داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان را پی نثار رخت شمع دیده ور</p></div>
<div class="m2"><p>در آستین گریهٔ پا در رکاب داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بود فکر خال و خطی در خیال من</p></div>
<div class="m2"><p>هر نقطه ام چو نافهٔ چین مشک ناب داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد موج زن به قلزم اندیشه مطلعی</p></div>
<div class="m2"><p>از بس که نبض خامهٔ من اضطراب داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در میکشی نگار من از بس حجاب داشت</p></div>
<div class="m2"><p>پیمانه در کفش عرق آفتاب داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیشب به کوی او شدم از رشک بدگمان</p></div>
<div class="m2"><p>رنگ شکستهٔ عجبی ماهتاب داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زلفش به قتلم این همه بی رحم دل نبود</p></div>
<div class="m2"><p>تا تیغ آه، جوهری از پیچ و تاب داشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان پیشتر که طرح شود نقش آب و گل</p></div>
<div class="m2"><p>معمار عشق، خانهٔ ما را خراب داشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روزی که نقش دولتم از بوریا نشست</p></div>
<div class="m2"><p>مخمل به چشم دولت بیدار خواب داشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مخفی نماندی از نظر نکتهسنج من</p></div>
<div class="m2"><p>دیوان عمر اگر ورق انتخاب داشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سردی رسیده می کند آتش طلب، حزین</p></div>
<div class="m2"><p>سرمای زهد خشک، مرا بر شراب داشت</p></div></div>