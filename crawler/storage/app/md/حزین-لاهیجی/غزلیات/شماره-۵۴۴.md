---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>داریم به کف زلفی، محشر به کمین اندر</p></div>
<div class="m2"><p>در هر شکن است آن را صد نافهٔ چین اندر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر چو قدم کردم در راه سرکویش</p></div>
<div class="m2"><p>دوزخ به یسار افتاد، جنت به یمین اندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیمانهٔ لعلش را کوثر ز سیه مستان</p></div>
<div class="m2"><p>میخانه چشمش را، صد کعبه دین اندر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتخانهٔ مویش را صد باخته دین بنده</p></div>
<div class="m2"><p>آتشگه رویش را صد شعله جبین اندر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناخن مزن ای غیرت بر سینهٔ پرداغم</p></div>
<div class="m2"><p>حسرتکده ها دارم، هر گوشه دفین اندر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلیس شود خیره، آدم چو رخ افروزد</p></div>
<div class="m2"><p>حیرتکده ها داری در یک کف طین اندر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزاده روی سر کن بنیوش حزین از ما</p></div>
<div class="m2"><p>عیسی به فلک بر شد، قارون به زمین اندر</p></div></div>