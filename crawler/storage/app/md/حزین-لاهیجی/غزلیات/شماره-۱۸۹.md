---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>حق را بطلب مسجد و میخانه کدام است؟</p></div>
<div class="m2"><p>از باده بگو، شیشه و پیمانه کدام است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محراب دل آن جلوهٔ آغوش فریب است</p></div>
<div class="m2"><p>نشناختهام کعبه و بتخانه کدام است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بند از مژه برداشت خیال رخ ساقی</p></div>
<div class="m2"><p>ای ابر ببین، گریهٔ مستانه کدام است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صحبت صوفی منشان سوخت دماغم</p></div>
<div class="m2"><p>ای باده پرستان، ره میخانه کدام است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرتا سر این دشت، پر از جلوهٔ لیلی ست</p></div>
<div class="m2"><p>امّا نتوان گفت که جانانه کدام است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با هر سر خاری کششی هست، ندانم</p></div>
<div class="m2"><p>کآشوب فزای دل دیوانه کدام است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بزم، حریفان همگی واقف رازند</p></div>
<div class="m2"><p>از یار ندانیم که بیگانه کدام است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن جلوه برد ره به سوای دل ما</p></div>
<div class="m2"><p>با برق مگویید سیه خانه کدام است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شمع حزین ، از مژه ات دود برآید</p></div>
<div class="m2"><p>بنمایم اگر گرمی افسانه کدام است؟</p></div></div>