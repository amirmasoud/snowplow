---
title: >-
    شمارهٔ ۷۹۹
---
# شمارهٔ ۷۹۹

<div class="b" id="bn1"><div class="m1"><p>تیغت از فرق مبتلا رفته</p></div>
<div class="m2"><p>از سرم سایه هما رفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که بیگانه مشربان دیدم</p></div>
<div class="m2"><p>از لبم حرف آشنا رفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفته بر پیکرم ز گردش چرخ</p></div>
<div class="m2"><p>آنچه بر دانه ز آسیا رفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از میان رفته ایم تا من و دل</p></div>
<div class="m2"><p>جم و جام جهان نما رفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاق ابروی اوست کعبه ما</p></div>
<div class="m2"><p>دل به آن قبلهٔ دعا رفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگهم تا به خاک درگه او</p></div>
<div class="m2"><p>به تکاپوی توتیا رفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی افزاست نغمهٔ تو حزین</p></div>
<div class="m2"><p>دل ازین طرز آشنا رفته</p></div></div>