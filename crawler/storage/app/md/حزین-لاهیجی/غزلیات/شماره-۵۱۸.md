---
title: >-
    شمارهٔ ۵۱۸
---
# شمارهٔ ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>ازین دهشت که هجرانی مبادا در کمین باشد</p></div>
<div class="m2"><p>ز حسرت هر نگاه من نگاه واپسین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره سازد زبان شعله شمع انجمن پیرا</p></div>
<div class="m2"><p>به هر محفل که حرفی زان عذار آتشین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود در موج آب زندگانی سبزه اش غلتان</p></div>
<div class="m2"><p>در آن گلشن که ابروی تو را از ناز چین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازین آشفته حالی سر نمی پیچم، سرت گردم</p></div>
<div class="m2"><p>چنین خواهد اگرزلف پریشانت چنین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریب حرف و صوت خضرم از جا برنمی آرد</p></div>
<div class="m2"><p>که آب زندگی لعل تو را پر نگین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی افتد به دست مدّعی سرمایهٔ معنی</p></div>
<div class="m2"><p>که این گنج گهر، کلک مرا در آستین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خود می‌خورد مورش، حزین از تنگدستی‌ها</p></div>
<div class="m2"><p>در آن خرمن که برق بی‌مروت خوشه‌چین باشد</p></div></div>