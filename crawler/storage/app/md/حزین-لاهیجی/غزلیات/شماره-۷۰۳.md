---
title: >-
    شمارهٔ ۷۰۳
---
# شمارهٔ ۷۰۳

<div class="b" id="bn1"><div class="m1"><p>ز مستی های صهبای ازل میخانهٔ خویشم</p></div>
<div class="m2"><p>چو چشم خوش نگاهان سرخوش از پیمانهٔ خویشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تجلی کرده در جانم جمال شعله رخساری</p></div>
<div class="m2"><p>ز ایمانم چه پرسی؟ گبر آتش خانه خویشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم چون شعله جواله با خود عشق می بازد</p></div>
<div class="m2"><p>چراغ خلوت خاص خود و پروانه خویشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک عکس است چشم، آیینهٔ تصویر را دایم</p></div>
<div class="m2"><p>همین محو تماشای رخ جانانه خویشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امّید اسیری رفته ام از خود بیابانها</p></div>
<div class="m2"><p>به ذوق آشنایی های او بیگانهٔ خویشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون از من نباشد جلوه گاهی حق و باطل را</p></div>
<div class="m2"><p>خرابات دلم، هم کعبه هم بتخانهٔ خویشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل صد چاکم آراید حواس آشفتگیها را</p></div>
<div class="m2"><p>که هم زلف پریشان خود و هم شانه خویشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فسونی از نفس هر دم به گوشم می زند هستی</p></div>
<div class="m2"><p>گران بالین خواب غفلت از افسانهٔ خویشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکستم قدر خود را در جهان از خوش عنانیها</p></div>
<div class="m2"><p>من سرگشته، آب آسیای دانه خویشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به آبا فخرکردن کار کودک مشربان باشد</p></div>
<div class="m2"><p>فراموش است درس ابجد طفلانهٔ خویشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خروش سینه چون سیلاب دارد پای کوبانم</p></div>
<div class="m2"><p>طربناک از سماع نالهٔ مستانهٔ خویشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مطرب نیست حاجت چون جرس شوریده مغزان را</p></div>
<div class="m2"><p>فغان خیز است دیوار و در کاشانهٔ خویشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حزین از گوشهٔ دل پا برون ننهاده ام هرگز</p></div>
<div class="m2"><p>اگر گنجم اگر دیوانه، در ویرانهٔ خویشم</p></div></div>