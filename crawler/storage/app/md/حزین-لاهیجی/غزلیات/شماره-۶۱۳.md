---
title: >-
    شمارهٔ ۶۱۳
---
# شمارهٔ ۶۱۳

<div class="b" id="bn1"><div class="m1"><p>ای از رخت مشاطه را، صد چشم حیران در بغل</p></div>
<div class="m2"><p>مانند صبح آیینه را خورشید تابان در بغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هندوی خالت را بود چین و ختن زیر نگین</p></div>
<div class="m2"><p>زنّار زلفت را بود، صد کافرستان در بغل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل قدح نوش تو را، میخانهها در آستین</p></div>
<div class="m2"><p>خطّ زره پوش تو را صد جوش طوفان در بغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح بناگوش تو را خورشید تابان خوشه چین</p></div>
<div class="m2"><p>داغ سیه پوش تو را صد شام هجران در بغل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت عجب نبود اگر، دل را نگهداری کند</p></div>
<div class="m2"><p>در می پرستی شیشه را دارند مستان در بغل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی محبت می شود پوشیده ما را در سخن</p></div>
<div class="m2"><p>گر بوی گل پنهان کند باد بهاران در بغل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دست جورت در چمن ای یوسف گل پیرهن</p></div>
<div class="m2"><p>دارد دل صد پاره ای هر غنچه پنهان در بغل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاک گریبان می کند چون لاله رسوا عشق را</p></div>
<div class="m2"><p>چندان که می سازد دلم داغ تو پنهان در بغل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیگر کجا عشق و جنون چون لاله پنهان می توان؟</p></div>
<div class="m2"><p>داغم هم آغوش جگر، چاک گریبان در بغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دارم دلی کز ناله اش نالد به صد شیون، حزین</p></div>
<div class="m2"><p>اسلامیان کعبه را ناقوس رهبان در بغل</p></div></div>