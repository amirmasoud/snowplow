---
title: >-
    شمارهٔ ۷۶۱
---
# شمارهٔ ۷۶۱

<div class="b" id="bn1"><div class="m1"><p>بالین نهاده ام به سر کوی خویشتن</p></div>
<div class="m2"><p>دارم سری چو غنچه به زانوی خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آغوش دایه، بود مرا کام اژدها</p></div>
<div class="m2"><p>در آتشم ز خیرگی خوی خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنها ز دوستان نیم امروز شرمسار</p></div>
<div class="m2"><p>دارد فلک مرا خجل از روی خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستی ز همرهان نبود زیر بار ما</p></div>
<div class="m2"><p>آورده ایم زور به بازوی خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در موج خیز دهر ز طوفان حادثات</p></div>
<div class="m2"><p>چینی ندیده ایم در ابروی خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جرعه های زهر که پیمود روزگار</p></div>
<div class="m2"><p>شیرین نمودم از شکرین خوی خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریوزه پیش بحر نصیب حباب باد</p></div>
<div class="m2"><p>چون تیغ تر بود لبم از جوی خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبود نظر به سرمهٔ مردم، سیه مرا</p></div>
<div class="m2"><p>چشم من است و خاک سرکوی خویشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پنجهٔ غمی که فشارد گلو، حزین</p></div>
<div class="m2"><p>در حیرتم ز کلک سخن گوی خویشتن</p></div></div>