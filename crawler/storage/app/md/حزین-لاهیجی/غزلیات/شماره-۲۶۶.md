---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>بار ستم یار، گران است و گران نیست</p></div>
<div class="m2"><p>جانبازی عشاق، زیان است و زیان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب چه شنیده ست ز اغیارکه امروز</p></div>
<div class="m2"><p>با ما نگه یار همان است و همان نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرفی ز دهانش به زبان است دهان کو؟</p></div>
<div class="m2"><p>رازی ز میانش به زبان است و زبان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بویی نه و رنگی ست به رخساره جهان را</p></div>
<div class="m2"><p>درگلشن تصویر خزان است و خزان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردد به گلو بس که گره، سوخت نفس را</p></div>
<div class="m2"><p>ما را سبق گریه روان است و روان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرگشته کوی تو شدند آبله پایان</p></div>
<div class="m2"><p>این راه پر از سنگ نشان است و نشان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیداست حزین از نفست بوی محبت</p></div>
<div class="m2"><p>در جیب تو این مشک نهان است و نهان نیست</p></div></div>