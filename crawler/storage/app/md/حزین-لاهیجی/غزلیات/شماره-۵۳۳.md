---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>کام طمع ز لذت دنیا نگاه دار</p></div>
<div class="m2"><p>امروز، پاس دولت فردا نگاه دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گوشه جوش جلوهٔ یار است، دیده را</p></div>
<div class="m2"><p>آیینه وار محو تماشا نگاه دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر عقده ای به عهده تدبیر ناخنی ست</p></div>
<div class="m2"><p>خاری برای آبلهٔ پا نگاه دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا وجه بی قراری ما روشنت شود</p></div>
<div class="m2"><p>آیینه پیش آن رخ زیبا نگاه دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایجاد نور فیض، دل زنده می کند</p></div>
<div class="m2"><p>این شمع را به پرده ی شب ها نگاه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی چو داغ لاله بهار تو گل کند</p></div>
<div class="m2"><p>دامان دل به رنگ سویدا نگاه دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک سر چو شمع جسم تو خواهی که جان شود</p></div>
<div class="m2"><p>آیینه پیش آن رخ زیبا نگاه دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ وفا مباد، ز دل پا کشد حزین</p></div>
<div class="m2"><p>این لاله ی غریب، به صحرا نگاه دار</p></div></div>