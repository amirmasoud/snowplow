---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>کوته نظران زلف سیه کار ندانند</p></div>
<div class="m2"><p>این مرده دلان فیض شب تار ندانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانسوز دیاریست محبت، که طبیبان</p></div>
<div class="m2"><p>رسم است که حال دل بیمار ندانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما باخته دینان، ادب کفر ندانیم</p></div>
<div class="m2"><p>نو برهمنان بستن زنار ندانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مغروری حسن است که در جلوه گه او</p></div>
<div class="m2"><p>جانبازی یاران وفادار ندانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی پرده تماشایی آن حسن لطیفند</p></div>
<div class="m2"><p>بالغ نظران، پرده پندار ندانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارند حریفان هوس خاطر شادی</p></div>
<div class="m2"><p>دل باختگان غیر غم یار ندانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستان زن دیرینه ی گلزار، حزین است</p></div>
<div class="m2"><p>این نوسخنان شیوهٔ گفتار ندانند</p></div></div>