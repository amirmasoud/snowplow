---
title: >-
    شمارهٔ ۶۰۱
---
# شمارهٔ ۶۰۱

<div class="b" id="bn1"><div class="m1"><p>یک مشت سفله مانده بجا از کرام خلق</p></div>
<div class="m2"><p>ننگ است در زمانه زبان را ز نام خلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زهر جانگزای، گلوگیر می شود</p></div>
<div class="m2"><p>نتون زلال خضر کشیدن ز جام خلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز در لباس کمالند ناقصان</p></div>
<div class="m2"><p>پوشیده ناتمامی خود را، تمام خلق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعظیم گاو و خرکه به مردم حرام بود</p></div>
<div class="m2"><p>اکنون فریضه گشته به ما، احترام خلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزدیک من چو طعن سنان است جان گسل</p></div>
<div class="m2"><p>زینسان که دور شد ز مسلمان سلام خلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درگوش جزر و مد نظرها هزار پاست</p></div>
<div class="m2"><p>آزرده است بس که صماخ از کلام خلق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقل گریزد از دهن اژدها حزین</p></div>
<div class="m2"><p>هش دار تا که مفت نیفتی به دام خلق</p></div></div>