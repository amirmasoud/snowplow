---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>یاد وصلی که دل از هجر خبردار نبود</p></div>
<div class="m2"><p>در میان این تن ویران شده دیوار نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن درپیرهن عشق تجلّی می کرد</p></div>
<div class="m2"><p>پردهٔ دیده حجاب رخ دیدار نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داشت جا، فاخته در جامهٔ یکتایی سرو</p></div>
<div class="m2"><p>طوق گردن به گلو، حلقهٔ زنّار نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیلی پرده نشین این همه دیوانه نداشت</p></div>
<div class="m2"><p>یوسف مصر سراسر رو بازار نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدهٔ احول ادراک نمی دید دویی</p></div>
<div class="m2"><p>در میان من و یار، اسم من و یار نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع من پیرهنی جز پر پروانه نداشت</p></div>
<div class="m2"><p>کار بر سوختگان اینهمه دشوار نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل از غنچهٔ منقار، به دامن گل داشت</p></div>
<div class="m2"><p>خار اندیشه به پیراهن گلزار نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب که می زد رقم این تازه غزل، خامه حزین</p></div>
<div class="m2"><p>مستیی بود رگش را، که خبردار نبود</p></div></div>