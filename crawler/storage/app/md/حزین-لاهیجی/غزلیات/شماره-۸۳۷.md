---
title: >-
    شمارهٔ ۸۳۷
---
# شمارهٔ ۸۳۷

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه بزم حریفان کنون بیارایی</p></div>
<div class="m2"><p>ز عکس چهره می لاله گون بیارایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون ز پرده گر آیی، جهان بیاساید</p></div>
<div class="m2"><p>به خاطری که درآیی، درون بیارایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همین قدر ز تو نامهربان طمع دارم</p></div>
<div class="m2"><p>که خاک تربت ما را به خون بیارایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را فتاده غم جان کوهکن ورنه</p></div>
<div class="m2"><p>به کاوش مژه ای، بیستون بیارایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امیدم این بود ای چشم خون فشان از تو</p></div>
<div class="m2"><p>ز لاله دامن دشت جنون بیارایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم خراب رخ بی تکلفانهٔ توست</p></div>
<div class="m2"><p>به حیرتم چه شود، چهره چون بیارایی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرود مجلس دیر مغان ز توست حزین</p></div>
<div class="m2"><p>به نغمه ای چه شود، ارغنون بیارایی؟</p></div></div>