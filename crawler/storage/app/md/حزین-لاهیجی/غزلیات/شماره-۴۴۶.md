---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>دهد ساقی اگر ساغر چنین، مخمور نگذارد</p></div>
<div class="m2"><p>بود گر جلوهٔ مستانه این، مستور نگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به افسونی طبیب عشق درمان کرد دردم را</p></div>
<div class="m2"><p>محبّت را دم عیسی بود، رنجور نگذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن بزمی که من پیمانهٔ توحید پیمایم</p></div>
<div class="m2"><p>خمارم قطره ای در ساغر منصور نگذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمارت برنمی تابد کهن ویرانهٔ دنیا</p></div>
<div class="m2"><p>چرا سازم؟ که سیلاب فنا معمور نگذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نگذارد از کف، کاسه کشکول قناعت را</p></div>
<div class="m2"><p>گدا از ناز پا را بر سر فغفور نگذارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صدق دل گر آید جانب میخانه، من ضامن</p></div>
<div class="m2"><p>که ساقی عقدهای در خاطر انگور نگذارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین در عشق از کف لنگر تسلیم نگذاری</p></div>
<div class="m2"><p>مجال دست و پا این قلزم پرشور نگذارد</p></div></div>