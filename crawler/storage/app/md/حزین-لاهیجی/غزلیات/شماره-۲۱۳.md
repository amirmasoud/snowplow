---
title: >-
    شمارهٔ ۲۱۳
---
# شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>نخلم از گریه در آب است و ثمر پیدا نیست</p></div>
<div class="m2"><p>تا فلک آتش آه است و اثر پیدا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعده دل را به دعاهای سحر می دادم</p></div>
<div class="m2"><p>وه چه سازم که شب هجر، سحر پیدا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط اگر بود، دلم پی به دهانش می برد</p></div>
<div class="m2"><p>خضر راه من تفسیده جگر پیدا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مو شکافان جهان در تب و تابند تمام</p></div>
<div class="m2"><p>در خم زلف تو آن موی کمر پیدا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل و دین رفت در اوّل نگه از دست حزین</p></div>
<div class="m2"><p>به کجا تا بکشدکار نظر، پیدا نیست</p></div></div>