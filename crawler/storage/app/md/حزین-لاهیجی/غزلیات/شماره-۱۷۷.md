---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>احساس مبدل شد و محسوس همان است</p></div>
<div class="m2"><p>صد شمع فزون سوخته، فانوس همان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل کافر دیر است، ز لبیک چه حاصل؟</p></div>
<div class="m2"><p>گر زمزمه دیگر شده، ناقوس همان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب بر لب او دارم و حسرت کش عشقم</p></div>
<div class="m2"><p>دلبر به کنار و هوس بوس همان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با رب چه علاج است، پریشانی دل را؟</p></div>
<div class="m2"><p>زلفش به کف و خاطر مأیوس همان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دوست به کونین نگردیم تسلی</p></div>
<div class="m2"><p>این هر دو به دست و کف افسوس همان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیزد ز دری هر نفس، آوازه ی دولت</p></div>
<div class="m2"><p>کاووس شد و زمزمه ی کوس همان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد چوکند جامه ز مصحف، مفریبید</p></div>
<div class="m2"><p>ای ساده دلان خرقهٔ سالوس همان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بارگه پادشه عشق حزین را</p></div>
<div class="m2"><p>سر خاک شد وذوق زمین بوس همان است</p></div></div>