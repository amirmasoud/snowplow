---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>تن سختی کشم نزار دل است</p></div>
<div class="m2"><p>کمر کوه زیر بار دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از آن طرّه در پریشانی است</p></div>
<div class="m2"><p>سر این فتنه در کنار دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکند ناوک دعا اثری</p></div>
<div class="m2"><p>گره مدّعا، به کار دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم تا کار می کند ما را</p></div>
<div class="m2"><p>گل اشک است و نوبهار دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چمن عشق را خزانی نیست</p></div>
<div class="m2"><p>گل پاینده، خار خار دل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرق شرم ابر، از دریاست</p></div>
<div class="m2"><p>دیده تا هست شرمسار دل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صف دشمن، زبان بسته شکست</p></div>
<div class="m2"><p>لب خاموش ذوالفقار دل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می گدازد چو رشتهٔ گوهر</p></div>
<div class="m2"><p>ناتوانی که زیر بار دل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دم، آیینه پاس دار حزین</p></div>
<div class="m2"><p>نفس پاک هم غبار دل است</p></div></div>