---
title: >-
    شمارهٔ ۸۸۲
---
# شمارهٔ ۸۸۲

<div class="b" id="bn1"><div class="m1"><p>کند گردآوری زلفش، دل شوریده بسیاری</p></div>
<div class="m2"><p>که زندان را نباشد بهتر از زنجیر، دیواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تغافل می کند تیغ تو تا کی با رگ جانم؟</p></div>
<div class="m2"><p>ز کفر بی سرانجامم، به جا مانده ست زنّاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروشی دلخراش از رخنه های سینه می آید</p></div>
<div class="m2"><p>صفیری می سراید در قفس، مرغ گرفتاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار تربتم، در چشم شیران خاک می ریزد</p></div>
<div class="m2"><p>خدنگی خورده ام از کیش مژگان ستمکاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خورشید جهان آرای رخسار نگه سوزش</p></div>
<div class="m2"><p>در آتشخانه ى دل، هر طرف گرم است بازاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تپان درخاک و خون، چون نیم بسمل جان گسل دارد</p></div>
<div class="m2"><p>دل آزرده را، بیماری چشم جگر داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین ، آخر زیان عشقبازی، سود می گردد</p></div>
<div class="m2"><p>که بازار نگه، گرم است، با خورشید رخساری</p></div></div>