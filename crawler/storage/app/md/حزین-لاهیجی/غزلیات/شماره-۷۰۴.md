---
title: >-
    شمارهٔ ۷۰۴
---
# شمارهٔ ۷۰۴

<div class="b" id="bn1"><div class="m1"><p>چشم تو را ز جور، پشیمان نیافتم</p></div>
<div class="m2"><p>این کافر فرنگ مسلمان نیافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه خون هر دو جهان را به خاک ریخت</p></div>
<div class="m2"><p>تیغ کرشمهٔ تو پشیمان نیافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هر طرف که دیده گشودم گشاده بود</p></div>
<div class="m2"><p>جایی به فیض کلبهٔ ویران نیافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفتم که از شکنجه گردون برون روم</p></div>
<div class="m2"><p>راهی بغیر چاک گریبان نیافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مورم سری به غمکدهٔ خاک می کشد</p></div>
<div class="m2"><p>آسایشی به ملک سلیمان نیافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون لاله غیر داغ مرا درکنار نیست</p></div>
<div class="m2"><p>هرگز گل امید به دامان نیافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید دری ز غیب گشاید جنون عشق</p></div>
<div class="m2"><p>فیضی ز فضل حکمت یونان نیافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبود عجب اگر نفکندم به راه تو</p></div>
<div class="m2"><p>این سر سزای آن خم چوگان نیافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امشب که تیر آه حزین در جگر شکست</p></div>
<div class="m2"><p>ناقوس دیر و بتکده نالان نیافتم</p></div></div>