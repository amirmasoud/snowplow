---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>دیشب که چشم مست تو خاطر نواز بود</p></div>
<div class="m2"><p>تا صبح بر رخم در میخانه باز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که عشق، خاک دیار نیاز گشت</p></div>
<div class="m2"><p>سرو تو خوشخرام، به گلگشت ناز بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دلخراش بلبل من ذوق ناله داشت</p></div>
<div class="m2"><p>گلبن به سرفرازی و گلشن به ساز بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بینش نگر که آینه محرم گرفته است</p></div>
<div class="m2"><p>رویی که از نگاه منش احتراز بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طرفی نبسته ایم ازان آتشین عذار</p></div>
<div class="m2"><p>واسوختن تلافی سوز و گداز بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزدیک شد که از نفس ناله بشکفد</p></div>
<div class="m2"><p>مهر لبم که غنچهٔ بستان راز بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک موی در هلاک حزین کوتهی نکرد</p></div>
<div class="m2"><p>زلفی که سایه پرور عمر دراز بود</p></div></div>