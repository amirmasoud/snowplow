---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>به تن ز بادهٔ عشق تو رنگ و بو کافی ست</p></div>
<div class="m2"><p>همین قدر که نمی هست در سبو، کافی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باک ساقی، اگر دور می به ما نرسد</p></div>
<div class="m2"><p>ز جرعهٔ تو، لبم مست آرزو کافی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رنگ شمع به سر نیست فکر سامانم</p></div>
<div class="m2"><p>که آه در جگر و گریه در گلو کافی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین نیم که رسد تن به وصل یا نرسد</p></div>
<div class="m2"><p>همین که عمر شود صرف جستجو کافی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبق چو آینه حیرانیم نمی خواهد</p></div>
<div class="m2"><p>همین قدر که شوم با تو روبرو کافی ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ز تصفیه مطلب صفاست صوفی را</p></div>
<div class="m2"><p>همین که خرقه به می داد شستشو کافی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوای سنبل و ریحان بس است بلبل را</p></div>
<div class="m2"><p>مرا شمیمی از آن جعد مشک بو کافی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به دوزخ هجر، ای صنم عذاب مکن</p></div>
<div class="m2"><p>برای سوختنم، عشق شعله خو کافی ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهان شکوهٔ زخمی که در دل است مرا</p></div>
<div class="m2"><p>اگر به تار نگاهی کنی رفو، کافی ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شراب اگر نبود، آتشم به ساغر کن</p></div>
<div class="m2"><p>گدای میکده را شعله در کدو کافی ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برای جلوهٔ یار است شیشه خانهٔ دل</p></div>
<div class="m2"><p>ز گرد هستی اگر یافت رفت و رو کافی ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر جواب نیامد غمین مباش حزین</p></div>
<div class="m2"><p>به طور عشق تو را ذوق های و هو کافی ست</p></div></div>