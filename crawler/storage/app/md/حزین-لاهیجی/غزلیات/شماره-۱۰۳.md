---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>درین فکرم، که تعلیم جبین سازم سجودش را</p></div>
<div class="m2"><p>به داغ دل دهم یاد عذار مشک سودش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به من در خامشی و گرم سوزی نسبتی بودش</p></div>
<div class="m2"><p>توانستی اگر پروانه، پنهان کرد دودش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلیدی خار خار هجر،کی در دیدهٔ بلبل؟</p></div>
<div class="m2"><p>به گل پیوند اگر می کرد، خاشاک وجودش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدی چون من اگر گرد کسادی سرمهٔ چشمش</p></div>
<div class="m2"><p>متاع یوسفی دیدی، زیان خویش سودش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مشکین طره ی او کی تواند همسری کردن؟</p></div>
<div class="m2"><p>عبث سنبل به دعوی شانه زد، زلف کبودش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قفس پروردهٔ عشق است گلبانگ دل افگاران</p></div>
<div class="m2"><p>چه می سنجد به ما مرغ چمن پرور، سرودش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین ، آه مرا با نالهٔ زاهد مکن نسبت</p></div>
<div class="m2"><p>اگر صد بار سوزد، بوی دردی نیست عودش را</p></div></div>