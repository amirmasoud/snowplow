---
title: >-
    شمارهٔ ۶۱۹
---
# شمارهٔ ۶۱۹

<div class="b" id="bn1"><div class="m1"><p>چون شاخ گل از باد سحر، بار فشاندم</p></div>
<div class="m2"><p>در دامن مطرب، سر و دستار فشاندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنیاد هوس ریخت، ز پا کوفتن دل</p></div>
<div class="m2"><p>بر هر دو جهان دست به یکبار فشاندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فیض کرم ابر سیه کاسه چه باشد؟</p></div>
<div class="m2"><p>مژگان تر خویش به گلزار فشاندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا از مژه خالی نبود مائدهٔ خون</p></div>
<div class="m2"><p>مشت نمکی بر دل افگار فشاندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرمندهٔ کس نیستم از کلک چو نیسان</p></div>
<div class="m2"><p>یکسان گهر خود به گل و خار فشاندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فیض، تهی بود کنار گل و نسرین</p></div>
<div class="m2"><p>دامان نقاب تو به گلزار فشاندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حوصلهٔ دل قدری بیشتر آمد</p></div>
<div class="m2"><p>خونابه اشکی که به ناچار فشاندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جبریل به این مرگ نمرده ست که جان را</p></div>
<div class="m2"><p>پروانه صفت در قدم یار فشاندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کردم به چمن یاد بهار خط سبزت</p></div>
<div class="m2"><p>در بستر نسرین و سمن خار فشاندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازشکوه غرض مرحمت یار، حزین نیست</p></div>
<div class="m2"><p>گردیست که از خاطر افگار فشاندم</p></div></div>