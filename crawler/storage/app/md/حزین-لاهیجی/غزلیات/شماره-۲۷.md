---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>داغ سودای تو دارد، دل دیوانهٔ ما</p></div>
<div class="m2"><p>کعبه لبیک زند بر در بتخانهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق را کعبهٔ مقصود، سویدای دل است</p></div>
<div class="m2"><p>لیلی از خودکند ایجاد، سیه خانهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شور دیوانگی و شیوهٔ اطفال یکی ست</p></div>
<div class="m2"><p>هست سربازی ما، بازی طفلانهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع ظلمتکده و کعبه و بتخانه یکی ست</p></div>
<div class="m2"><p>عالم آراست، فروغ رخ جانانهٔ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه هستی، غمی از نیک و بد خویش مخور</p></div>
<div class="m2"><p>دُرد را صاف کند، ساقی میخانهٔ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما و دل از دو جهان دور، کناری داریم</p></div>
<div class="m2"><p>سیل را راه نیفتاده به ویرانهٔ ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاوش دیده، دل از سینهٔ ما بیرون کرد</p></div>
<div class="m2"><p>خانه پرداز بود گریهٔ مستانهٔ ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر نیاری بدر، از حرف پریشان سخنان</p></div>
<div class="m2"><p>آشنا تا نشود، معنی بیگانهٔ ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو جهان تنگتر از دیدهٔ مور است حزین</p></div>
<div class="m2"><p>در گشاد نظر همّت مردانهٔ ما</p></div></div>