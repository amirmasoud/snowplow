---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>در طینتم از بس که رگ و ریشه، وفا داشت</p></div>
<div class="m2"><p>خاکم چه بهاران و چه دی، مهر گیا داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مرگ من آن زلف چرا موی نژولید؟</p></div>
<div class="m2"><p>یک دلشده از سلسلهٔ اهل وفا داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر از دل ما کز سر کونین گذشته ست</p></div>
<div class="m2"><p>هر درد که دیدیم سر کوی دوا داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی سخن اینجا به حریفی ست که فهمد</p></div>
<div class="m2"><p>با هر که نگه عربده ای داشت، به ما داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو رسیده ست به فریاد وگر نه</p></div>
<div class="m2"><p>این حوصله را صبر تنک ظرف، کجا داشت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز نبود جز به هدف دیدهٔ ناوک</p></div>
<div class="m2"><p>با ما نگهت هر ستمی داشت به جا داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک بوالعجبی دیده ام و جای شگفت است</p></div>
<div class="m2"><p>تلخابهٔ این چرخ سیه کاسه، گدا داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا آمده ز ایام نخورده ست فریبی</p></div>
<div class="m2"><p>دل تجربه ای داشت، ندانم ز کجا داشت؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از کوی غم آواز حزینی که شنیدی</p></div>
<div class="m2"><p>نالیدن دل بود، ندانم چه بلا داشت؟</p></div></div>