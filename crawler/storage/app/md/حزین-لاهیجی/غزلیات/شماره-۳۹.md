---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>دهقان نبرد حاصلی از بوم و بر ما</p></div>
<div class="m2"><p>سرویم و بود عقده خاطر ثمر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ناز، کله گوشه به خورشید شکستیم</p></div>
<div class="m2"><p>افکنده جنون، سایهٔ داغی به سر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگر لبش از شادی دل غنچه نگردید</p></div>
<div class="m2"><p>هر زخم که خندید، به روی جگر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوب آمدی ای شور نمکدان قیامت</p></div>
<div class="m2"><p>می جست تو را داغ پریشان نظر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قطره زدن باز فتد گام نخستین</p></div>
<div class="m2"><p>گر ابر شود همنفس چشم تر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما چون ز خرابات جهان پاک برآییم؟</p></div>
<div class="m2"><p>آلوده برون رفت ز جنت، پدر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستی که می ام داد، تو را بست به خشکی</p></div>
<div class="m2"><p>زاهد چه زنی طعنه به دامان تر ما؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهیم حزین ، آنقدر از خویش رمیدن</p></div>
<div class="m2"><p>کاوازه به جایی نرساند خبر ما</p></div></div>