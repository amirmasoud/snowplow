---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>بنواز مغنی، دل غم پیشهٔ ما را</p></div>
<div class="m2"><p>از شعله بشو دفتر اندیشهٔ ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن آتش سوزنده که پنداشتمش گل</p></div>
<div class="m2"><p>ازجلوه به هم سوخت، رگ و ریشهٔ ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیرم که به انجام رسد خاره تراشی</p></div>
<div class="m2"><p>کار است به جان سختی ما، تیشهٔ ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دست تو چندان که بر آید، به جفا کوش</p></div>
<div class="m2"><p>شرمنده مکن جان وفا پیشهٔ ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشک و تر اندیشه حزین ، از تف ما سوخت</p></div>
<div class="m2"><p>آتش ز تب شیر بود، بیشهٔ ما را</p></div></div>