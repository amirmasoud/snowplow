---
title: >-
    شمارهٔ ۳۸۰
---
# شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>محرومی وصال تو دل را نوید بود</p></div>
<div class="m2"><p>صبح امید آینه، چشم سفید بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده می تپید چو بسمل به خون دل</p></div>
<div class="m2"><p>کز تیغ دوری تو نگاهم شهید بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب داشتیم بزم خوشی با خیال تو</p></div>
<div class="m2"><p>هوشم خراب بادهٔ گفت و شنید بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ما گذشت و بگذرد امّا ز حق مرنج</p></div>
<div class="m2"><p>کز شیوهٔ وفای تو دوری بعید بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی بیا که پیری و مخموریم بلاست</p></div>
<div class="m2"><p>دل از تو شیر مست شراب امید بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می داد می، به کشتی افلاک جبرئیل</p></div>
<div class="m2"><p>جایی که پیر میکد هٔ ما مرید بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رب که آب میکده از ما دریغ داشت؟</p></div>
<div class="m2"><p>مفتی درین معامله گویا یزید بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یعقوب اگر ز یوسف خود داشت آگهی</p></div>
<div class="m2"><p>پیراهنش ز پردهٔ چشم سفید بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلها شکفته می شود از گفتگوی عشق</p></div>
<div class="m2"><p>درهای بسته را نفس ما کلید بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشکم که داشت آینهٔ خسروی حزین</p></div>
<div class="m2"><p>امّیدوار یک نظر اهل دید بود</p></div></div>