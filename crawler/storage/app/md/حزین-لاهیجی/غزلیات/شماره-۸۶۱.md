---
title: >-
    شمارهٔ ۸۶۱
---
# شمارهٔ ۸۶۱

<div class="b" id="bn1"><div class="m1"><p>به قید آب وگل ای جان ناتوان چونی؟</p></div>
<div class="m2"><p>دپن کهن قفس ای سدره آشیان، چونی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شمع محفل انسی به تیره وحشتگاه</p></div>
<div class="m2"><p>تو زبب ىسند قدسی، بر آستان چونی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنان گسسته، تو را بحر جود میجوید</p></div>
<div class="m2"><p>به ریگ بادیه، ای ماهی تپان چونی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلال خضر، تو را، سینه چاک می طلبد</p></div>
<div class="m2"><p>نفس گداخته، دنبال کاروان چونی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جلوه بود، مدار تو شوخ چشم شرار</p></div>
<div class="m2"><p>نشسته در دل سنگ، ای سبک عنان چونی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو رشک یوسف مصری، فتاده در چه تن</p></div>
<div class="m2"><p>تو بازکنگر عرشی، به خاکدان چونی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فروغ حسن تو را آفت زوال نبود</p></div>
<div class="m2"><p>به عقده ذنب ای مهر خاوران چونی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هلاک شیوه شوخی شوم که گفت، حزین</p></div>
<div class="m2"><p>جدا ز وصل من ای زار خسته جان چونی؟</p></div></div>