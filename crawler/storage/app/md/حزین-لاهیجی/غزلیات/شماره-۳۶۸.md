---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>هوای عشق برونم ز ننگ و نام کشید</p></div>
<div class="m2"><p>به توبه نامهٔ من، یار خط جام کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا حریف شرابی که فکر شام نداشت</p></div>
<div class="m2"><p>نهاد لب به شط باده و تمام کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشق پاک به هر شیوه تو مشتاقم</p></div>
<div class="m2"><p>به خشم و کین نتوان از من انتقام کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز از آن خط مشکین خبر نداشت دلم</p></div>
<div class="m2"><p>هوای دانه خالت مرا به دام کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز من حدیث وفا و جفای خویش مپرس</p></div>
<div class="m2"><p>که پاس راز، زبان مرا ز کام کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کوی انجم و افلاک رخت خویش برآر</p></div>
<div class="m2"><p>برای جا نتوان منت از لئام کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهار عیش در آغوش غنچه خسبان است</p></div>
<div class="m2"><p>نسیم صبح به گوش من این پیام کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>متاع عنصر و افلاک واسپار حزین</p></div>
<div class="m2"><p>که خوار شد، ز فرومایه هر که وام کشید</p></div></div>