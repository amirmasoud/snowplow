---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>از سوز ناله ام، دل جانان خبر نداشت</p></div>
<div class="m2"><p>آن شاخ گل، ز مرغ خوش الحان خبر نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده سینه بر در و بام قفس زدیم</p></div>
<div class="m2"><p>صیاد ما ز حال اسیران خبر نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر لب گذشت اگر چه به مستی حدیث زهد</p></div>
<div class="m2"><p>امّا، دل ز توبه پشیمان، خبر نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه وار اگر نتپیدم، غریب نیست</p></div>
<div class="m2"><p>از جلوهٔ تو دیدهٔ حیران خبر نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوریده را به زیر قدم خار و گل یکی ست</p></div>
<div class="m2"><p>سیل از بلند و پست بیابان خبر نداشت</p></div></div>