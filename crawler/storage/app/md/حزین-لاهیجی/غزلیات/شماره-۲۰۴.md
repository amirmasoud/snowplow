---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>در خاطر خدنگ قضا هر نهان که هست</p></div>
<div class="m2"><p>کرد آن چنان نگاه تو خاطرنشان که هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب چه آفتی تو که دارد به صد زبان</p></div>
<div class="m2"><p>داد از دل تو، هر دل نامهربان که هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان رفت و سرگرانی نازت چنان که بود</p></div>
<div class="m2"><p>دل خون شد و غرور نگاهم همانکه هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انجام کار عشق ز آغاز به نشد</p></div>
<div class="m2"><p>بود این چنین به ما نگهت سرگران که هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستانسرای خامهٔ جان پرورت حزین</p></div>
<div class="m2"><p>سنجد حدیث شوق به هر داستان که هست</p></div></div>