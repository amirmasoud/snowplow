---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>از ما فلک دون چه به یغما بستاند؟</p></div>
<div class="m2"><p>این سفله چه داده ست که از ما بستاند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوثر جگر تشنه فرستد به سوالش</p></div>
<div class="m2"><p>خاری که نم از آبله ی پا بستاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نیست تبسم، سر دشنام سلامت</p></div>
<div class="m2"><p>دل کام خود از لعل شکرخا بستاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودای کریمان همه سود است که نیسان</p></div>
<div class="m2"><p>گوهر عوض قطره ز دریا بستاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گرسنه چشمان به حذر باش که ساغر</p></div>
<div class="m2"><p>هر قطره که خم داد، ز مینا بستاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این است حزین ، از کرم ساقی امیدم</p></div>
<div class="m2"><p>ما را به یکی جرعه می، از ما بستاند</p></div></div>