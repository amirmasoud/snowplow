---
title: >-
    شمارهٔ ۶۷۷
---
# شمارهٔ ۶۷۷

<div class="b" id="bn1"><div class="m1"><p>به یاد جلوهٔ شوخی، سبک ز جا رفتم</p></div>
<div class="m2"><p>چو بوی گل همه جا همره صبا رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میانهٔ من و آن تیر غمزه عهدی بود</p></div>
<div class="m2"><p>به این نشانه که از خاطر وفا رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گدا سرشت وصالم، گرسنه چشم نگاه</p></div>
<div class="m2"><p>ز کوی او همه جا، روی در قفا رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز محفل سر زلفش خبر نبود مرا</p></div>
<div class="m2"><p>به رهنمونی دلهای مبتلا رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روا مدار که بیگانگی به پیش آید</p></div>
<div class="m2"><p>که من ز ره به نگه های آشنا رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر ارادت همّت به پای تسلیم است</p></div>
<div class="m2"><p>ز دیر و صومعه بی عرض مدّعا رفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دیر چشم دلم فیض کعبه یافت حزین</p></div>
<div class="m2"><p>که آمدم هوس آلود و پارسا رفتم</p></div></div>