---
title: >-
    شمارهٔ ۷۶۲
---
# شمارهٔ ۷۶۲

<div class="b" id="bn1"><div class="m1"><p>چشمت از ناز نبستهست در راز به من</p></div>
<div class="m2"><p>رسد از جنبش مژگان تو آواز به من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر را ذره ناچیز نمی گردد بار</p></div>
<div class="m2"><p>چون خریدی مده ای شوخ، مرا باز به من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرنوشت دلم از داغ سویدا پیداست</p></div>
<div class="m2"><p>روشن انجام شد از نقطهٔ آغاز به من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهد بی منّت کوثر نسب مرگ کجاست؟</p></div>
<div class="m2"><p>تا به کی زندگی تلخ، کند ناز به من؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست احسان کمی ای فلک تنگ فضا</p></div>
<div class="m2"><p>این که نگذاشته ای حسرت پرواز به من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ادای سخنم گوش نگهدار حزین</p></div>
<div class="m2"><p>چشم جادو نگه، آموخته اعجاز به من</p></div></div>