---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>شق کرده ایم پردهٔ پندار خویش را</p></div>
<div class="m2"><p>بی پرده دیده ایم رخ یار خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بیعگاه عشق به نرخ هزار جان</p></div>
<div class="m2"><p>ما می خریم ناز خریدار خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرهم چه احتیاج؟ که عاشق ز سوز عشق</p></div>
<div class="m2"><p>خواباند در نمک، دل افگار خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نقش پا به خاک رهت ما فتادگان</p></div>
<div class="m2"><p>افزوده ایم پستی دیوار خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن بلبلم که می گذرانم به زیر بال</p></div>
<div class="m2"><p>ایام شادمانی گلزار خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شمعم ای صبا، دم افسرده دور دار</p></div>
<div class="m2"><p>بگذار تا تمام کنم کار خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از برگ و بار عاریت ای نخل باد دست</p></div>
<div class="m2"><p>سنگین مساز، دوش سبکبار خویش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جذبه همّتی که درین دشت پُر فریب</p></div>
<div class="m2"><p>گم کرده ایم قافله سالار خویش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کام زاغ، طعمهٔ طوطی مکن، حزین</p></div>
<div class="m2"><p>بشناس قدر کلک شکر بار خویش را</p></div></div>