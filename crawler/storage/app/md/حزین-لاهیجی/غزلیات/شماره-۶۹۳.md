---
title: >-
    شمارهٔ ۶۹۳
---
# شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>ز آواز خوش آن غنچه لب، تا دور شد گوشم</p></div>
<div class="m2"><p>به خون آغشته تر از پنبه ناسور شد گوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سان با اختلاط این منافق پیشگان سازم؟</p></div>
<div class="m2"><p>که از ساز مخالف کاسه طنبور شد گوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارم چاره چون با ابلهان جز مستمع بودن</p></div>
<div class="m2"><p>چو صحرای قیامت، عرصه گاه شور شد گوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کم از کژدم نباشد اختلاط تلخ گفتاران</p></div>
<div class="m2"><p>ز بس نیش زبان خورد از خسان، رنجور شد گوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو با این مرده طبعان زنده درگورم درین محفل</p></div>
<div class="m2"><p>عجب نبود اگر سوراخ مار و مور شدگوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد صرفه جز شوریده مغزی فیض صحبتها</p></div>
<div class="m2"><p>ز حرف ریزه خوانان خانهٔ زنبور شد گوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر زمهریر صحبت گرم اختلاطانم</p></div>
<div class="m2"><p>ز دم سردان عالم مخزن کافور شد گوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی افتد خلل در وقتم از آشفته گفتاران</p></div>
<div class="m2"><p>ز بانگ دوست چون دارالحضور طور شد گوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حزین از بس که دادم درجهان داد سخن سنجی</p></div>
<div class="m2"><p>به گوهرپروری ها چون صدف مشهور شد گوشم</p></div></div>