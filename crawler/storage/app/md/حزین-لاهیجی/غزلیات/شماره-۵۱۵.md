---
title: >-
    شمارهٔ ۵۱۵
---
# شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه یار، کله گوشهٔ وفا شکند</p></div>
<div class="m2"><p>صف کرشمه، نگه های آشنا شکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دیر و کعبه نماند درست پیمانی</p></div>
<div class="m2"><p>به دوش و بر اگر آن طرّهٔ دوتا شکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکسته رنگی عشقم رسیده تا جایی</p></div>
<div class="m2"><p>که شرم چهرهٔ من، رنگ کهربا شکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآورد به تماشا، سر از دریچه مهر</p></div>
<div class="m2"><p>چو من به دامن عزلت کسی که پا شکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال دولتم از عشق گشته سکّه به زر</p></div>
<div class="m2"><p>ز رنگ کاهی من، نرخ کیمیا شکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چاره عقدهٔ دل در میان منه ترسم</p></div>
<div class="m2"><p>که مفت، ناخن فکر گره گشا شکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک به دردکشان، سنگ فتنه می بارد</p></div>
<div class="m2"><p>دنی چو مست شود کاسهٔ گدا شکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین که می نگرم خون عالمیست هدر</p></div>
<div class="m2"><p>رواج جور تو، بازار خون بها شکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ فرنگ تو ایمان به رو نماگیرد</p></div>
<div class="m2"><p>شراب رنگ تو ناموس پارسا شکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خموشی تو از آن شکوه خوشتر است حزین</p></div>
<div class="m2"><p>که زلف آه تو را بخت نارسا شکند</p></div></div>