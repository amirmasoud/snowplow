---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>از فیض ریزش مژه، تر شد دماغ ما</p></div>
<div class="m2"><p>افتاد سایهٔ رگ ابری به باغ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودکامیی ز تلخی دشنام داشتیم</p></div>
<div class="m2"><p>شیرین تبسّمی نمکی زد به داغ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما گر فسرده ایم صبا را چه می شود؟</p></div>
<div class="m2"><p>ره گم نکرده بوی گلی، تا دماغ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستش به داغ عشق، همان دور از آتش است</p></div>
<div class="m2"><p>پروانه ای که خویش نزد بر چراغ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ دلم چو لاله پر از خون بود حزین</p></div>
<div class="m2"><p>یا رب مباد خالی ازین می ایاغ ما</p></div></div>