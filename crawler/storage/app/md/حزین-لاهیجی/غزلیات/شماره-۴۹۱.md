---
title: >-
    شمارهٔ ۴۹۱
---
# شمارهٔ ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>نسیم حالت آور، پای کوبان، تردماغ آمد</p></div>
<div class="m2"><p>به دل ها ذوق دست افشانی گل های باغ آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدوی خشک زاهد را، دماغ از بوی می تر شد</p></div>
<div class="m2"><p>بحمدالله که آب رفته، ما را در ایاغ آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رگ برق قدح، ره می زند خلوت گزینان را</p></div>
<div class="m2"><p>بشارت، زاهد گم کرده ایمان را چراغ آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا صوفی ببین وجد گل و رقص درختان را</p></div>
<div class="m2"><p>برآ از خرقهٔ سالوس زاهد، فصل باغ آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزین ، از قطره ریزی تا نمانده ست ابر آذاری</p></div>
<div class="m2"><p>مگر دردانهٔ دل را توانی در سراغ آمد</p></div></div>