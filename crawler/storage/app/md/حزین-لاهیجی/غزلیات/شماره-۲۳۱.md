---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>ما را تن ضعیف به زندان عالم است</p></div>
<div class="m2"><p>این هم که زنده ایم ز دستان عالم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شورش جهان سر زلف حواس من</p></div>
<div class="m2"><p>آشفته تر ز حال پریشان عالم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کامش به غیر دانهٔ دل آشنا نشد</p></div>
<div class="m2"><p>مور قناعتم، که سلیمان عالم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناموس روزگار به گردن گرفته است</p></div>
<div class="m2"><p>سلطان غیرتم، که نگهبان عالم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سودای عشق از سر ما کم نمی شود</p></div>
<div class="m2"><p>زنجیر زلف، سلسله جنبان عالم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فیض خطّ و خال تو ای نازنین غزال</p></div>
<div class="m2"><p>کلکم یکی ز مشک فروشان عالم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز مبند دل به فریب جهان حزین</p></div>
<div class="m2"><p>دنیای سفله، دشمن مردان عالم است</p></div></div>