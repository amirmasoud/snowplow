---
title: >-
    شمارهٔ ۵۶۹
---
# شمارهٔ ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>بی نشانی همه شان است به عنقا مفروش</p></div>
<div class="m2"><p>کنج عزلت چو دهد دست، به دنیا مفروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونبها صید تو را حلقهٔ فتراک بس است</p></div>
<div class="m2"><p>سر شوریده به آن زلف چلیپا مفروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده ای مست، تو را از پی عبرت دادند</p></div>
<div class="m2"><p>شوخ چشمانه به دنبال تماشا مفروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ما مرگ به از ناز طبیبانه بود</p></div>
<div class="m2"><p>خلوت خاک به آغوش مسیحا مفروش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه خواهی ببر ای ابر بهار از مژه ام</p></div>
<div class="m2"><p>به عبث آب رخ خویش به دریا مفروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستی آسان نبود، حوصله ای می خواهد</p></div>
<div class="m2"><p>توبه این شیشه دلی هوش به صهبا مفروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون گل هرزه درا، دفتر دل باد مده</p></div>
<div class="m2"><p>خاطر جمع، به یک خندهٔ بیجا مفروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طور دل نیست، کجا طاقت دیدار آرد؟</p></div>
<div class="m2"><p>جلوه ای برق جهانسوز به خارا مفروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به فسون سازی زاهد مرو از راه حزین</p></div>
<div class="m2"><p>مذهب عشق به تسبیح و مصلّا مفروش</p></div></div>