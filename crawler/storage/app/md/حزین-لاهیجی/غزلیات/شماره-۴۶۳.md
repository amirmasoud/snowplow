---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>هرکس به خاک میکده مست و خراب مُرد</p></div>
<div class="m2"><p>آسوده از ثواب و خلاص از عذاب مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی به دور دهر سیه کاسه سیر نیست</p></div>
<div class="m2"><p>اسکندرش به حسرت یک جرعه آب مُرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوضاع زشت عالم دون دیدنی نبود</p></div>
<div class="m2"><p>آسوده آنکه در شب مستی به خواب مُرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جور بی حساب تو جاوبد زنده ایم</p></div>
<div class="m2"><p>زاهد ز بیم پرسش روز حساب مُرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون بی بهاست عاشق حاضر جواب را</p></div>
<div class="m2"><p>جان خواست از حزین لب او، در جواب مُرد</p></div></div>