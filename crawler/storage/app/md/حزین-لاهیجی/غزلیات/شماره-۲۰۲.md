---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>کام، آشنا به ماحضر روزگار نیست</p></div>
<div class="m2"><p>جز زهر غصه در شکر روزگار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داندکسی که محنت هستی کشیده است</p></div>
<div class="m2"><p>دردی بتر، ز دردسر روزگار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسوده اند از غم ایام، بیخودان</p></div>
<div class="m2"><p>در ملک وحشتم، خبر روزگار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعلم چو آفتاب ز جایی در آتش است</p></div>
<div class="m2"><p>سودی امیدم، از سفر روزگار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درباب، فیض صبح بناگوش یار را</p></div>
<div class="m2"><p>تاثیر فیض، با سحر روزگار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلفش حوالهٔ دل شوریدگان کند</p></div>
<div class="m2"><p>هر فتنه ای که زیر سر روزگار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خود جدا نشسته و آسوده خاطرم</p></div>
<div class="m2"><p>کاری مرا به شور و شر روزگار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داری طمع ز دیدهٔ شوخ ستارگان</p></div>
<div class="m2"><p>آب حیا، که در گهر روزگار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حشم بد زمانه بود درکمین ما</p></div>
<div class="m2"><p>خرم کسی که، در نظر روزگار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دارد حزین ، اگر چه ره عشق خارها</p></div>
<div class="m2"><p>امّا، چو راه پر خطر روزگار نیست</p></div></div>