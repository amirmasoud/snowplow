---
title: >-
    شمارهٔ ۸۹۳
---
# شمارهٔ ۸۹۳

<div class="b" id="bn1"><div class="m1"><p>راه دل و دین را زدی ای طرفه صنم، های</p></div>
<div class="m2"><p>مژگان تو خواباند به ما، تیغ ستم های</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آوارهٔ کوی تو ندانم به چه حال است؟</p></div>
<div class="m2"><p>یعنی دلم، آن کافر گم کرده صنم، های</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر من و تمکین تو، ای عهد فراموش</p></div>
<div class="m2"><p>ما را و تو را ساخته بیگانه ز هم، های</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو تو صلایی به شهادت طلبان زد</p></div>
<div class="m2"><p>خود را برسانید به این پای علم، های</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با فیض کریمان کف محتاج حریف است</p></div>
<div class="m2"><p>محرومی چشمم، عجب، ای خاک قدم، های</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افتاده به دل، زخم به بالای هم از تو</p></div>
<div class="m2"><p>ای غمزه، مبادا شکنی قدر ستم، های</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز به پیچ و خم آزادی خویشم</p></div>
<div class="m2"><p>یاد تو به خیر، ای شکن زلفِ به خم، های</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تار نفس من به گلو، قید اسیریست</p></div>
<div class="m2"><p>از حلقهٔ دامم برهان، وحشت رم، های</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهد، خبر از ریزش مژگان منت نیست</p></div>
<div class="m2"><p>دامان تری دارم ازین ابرِ کرم، های</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فیض عجبی یافتم از پای خم می</p></div>
<div class="m2"><p>ای سایه نشینان گلستان ارم، های</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل، بتکدهٔ ما و ادب سجده بَرِ اوست</p></div>
<div class="m2"><p>ای ناصیه سایانِ حرمگاه صنم، های</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما برهمنان را، همه جا طور تجلّی ست</p></div>
<div class="m2"><p>از یار نداری خبر، ای شیخ حرم، های</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سامان خودی نیست به کف یک پر کاهم</p></div>
<div class="m2"><p>شرمندهٔ هستی نکنی، های عدم، های</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرغ دل ما در پی پرواز فراغی ست</p></div>
<div class="m2"><p>تا چند تپد در قفس شادی و غم، های</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در بزم، حزین این همه خاموش چرایی؟</p></div>
<div class="m2"><p>شوریده نوایی بزن از نای قلم، های</p></div></div>