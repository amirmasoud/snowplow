---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>کدامین آتشین رخسار گرم خودنمایی شد؟</p></div>
<div class="m2"><p>که اخلاص مغانی ملّتم، در جبهه سایی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم از بس خیال آن کف پا نقش می بندم</p></div>
<div class="m2"><p>بیاض دیده روشن سواد من حنایی شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من شکّر سخن، پرورده ام با شیرهٔ جانش</p></div>
<div class="m2"><p>که سروش مصرع برجستهٔ شیرین ادایی شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم تا سر به صحرا دادهٔ وحشی نگاه او</p></div>
<div class="m2"><p>غبارم سرمهٔ چشم غزالان ختایی شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیه روزم که از کف داده ام دامان زلفش را</p></div>
<div class="m2"><p>ز بخت تیرهٔ من کوتهی شد، نارسایی شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رواجی نقد ما را نیست در بازار حسن او</p></div>
<div class="m2"><p>زر داغم به کف سرمایهٔ حسرت فزایی شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در الفت میان جسم و جان با گل برآوردم</p></div>
<div class="m2"><p>از آن روزی که دل را با محبّت آشنایی شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ذوق وصل موج شور محشر می زند خاکش</p></div>
<div class="m2"><p>به خون غلتیده ای کو، زخمی تیغ جدایی شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل از دیرینه غمها برگرفتن نیست کار من</p></div>
<div class="m2"><p>چرا باید عبث بدنام ننگ بی وفایی شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کف چون شمع ما را در شب هجران به کار آید</p></div>
<div class="m2"><p>سر انگشتی که در گستاخی برقع گشایی شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دریا شد حباب، از ننگ ناچیزی برون آید</p></div>
<div class="m2"><p>گداز تن، شکست قدر ما را مومیایی شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبود اول درین میخانه قدری خرقه پوشان را</p></div>
<div class="m2"><p>شراب آلوده دلقم، آبروی پارسایی شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دل، بتخانه های آرزو را کرده ام ویران</p></div>
<div class="m2"><p>که چاک سینهٔ من قبلهٔ حاجت روایی شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فراموشم مکن گر معنی بیگانه می فهمی</p></div>
<div class="m2"><p>که عمرم صرف تفسیر کتاب آشنایی شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رگ سنگش ز شوخی موجهٔ دریای خون گردد</p></div>
<div class="m2"><p>به میدانی که مژگان تو در تیغ آزمایی شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو نی جز باد نبود در شکنج آستین من</p></div>
<div class="m2"><p>نفس بیهوده صرف نغمه های بی نوایی شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حزین ازگردش پیمانهٔ چشم سخن سازی</p></div>
<div class="m2"><p>سیه مستانه کلکم بر سر دستان سرایی شد</p></div></div>