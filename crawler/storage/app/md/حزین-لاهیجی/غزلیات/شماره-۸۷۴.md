---
title: >-
    شمارهٔ ۸۷۴
---
# شمارهٔ ۸۷۴

<div class="b" id="bn1"><div class="m1"><p>توکز رخ شمع طور و چشم جان، نور نظر باشی</p></div>
<div class="m2"><p>چه خواهد شد سرت گردم، شب ما را سحر باشی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو عالم از فروغ روی او، یک چشم بینا شد</p></div>
<div class="m2"><p>نبینی روی هجران را، اگر صاحب نظر باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سروش مقدم جانان رسید، از بال پروازت</p></div>
<div class="m2"><p>مرا ای هدهد جان زنده کردی، خوش خبر باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآ از خود، فضای بیخودی را هم تماشا کن</p></div>
<div class="m2"><p>چرا چون برق، درقید حیات مختصر باشی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سَرِ پایی بزن مستانه، سامان دو عالم را</p></div>
<div class="m2"><p>چرا از فکر صندل، در خمار دردسر باشی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پریشانی بود موج خطر، پرشور دریا را</p></div>
<div class="m2"><p>کنی گردآوری گر قطرهٔ خود را، گهر باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین ، افشاندن دامن، ندارد این قدر کاری</p></div>
<div class="m2"><p>برای خردهٔ جان، چند لرزان، چون شرر باشی؟</p></div></div>