---
title: >-
    شمارهٔ ۸۱۵
---
# شمارهٔ ۸۱۵

<div class="b" id="bn1"><div class="m1"><p>در دیده نگاه تو که از جوش فتاده</p></div>
<div class="m2"><p>مستی ست که در میکده خاموش فتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکیست که دارد جگر نافه پر از خون</p></div>
<div class="m2"><p>خالی که بر آن عارض گلپوش فتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غارتگر جمعیت دلهاست ببینید</p></div>
<div class="m2"><p>زلفی که پریشان به بر و دوش فتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مایوس مکن چشم به راهان چمن را</p></div>
<div class="m2"><p>از شوق تو گل یک چمن آغوش فتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو صاحب هوشی که کند فهم، سروشم</p></div>
<div class="m2"><p>کار سخنم با لب خاموش فتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جرعه این غمکده را باده به رنگی ست</p></div>
<div class="m2"><p>ته شیشه عشق است که سر جوش فتاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دولت بیدار، هماغوش کند خواب</p></div>
<div class="m2"><p>چشمی که بر آن صبح بناگوش فتاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کو عشق که از داغ چراغی بفروزم؟</p></div>
<div class="m2"><p>بختم چو شب هجر سیه پوش فتاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر تو خموشی ست حزین از سخن عشق</p></div>
<div class="m2"><p>این کهنه شرابی ست که از جوش فتاده</p></div></div>