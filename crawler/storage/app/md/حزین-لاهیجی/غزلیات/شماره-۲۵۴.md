---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>دل در هوس نرگس مستانه اسیر است</p></div>
<div class="m2"><p>مرغ حرم امروز به بتخانه اسیر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آبله ام بود دلی در کف و اکنون</p></div>
<div class="m2"><p>در دست تو بد مست چو پیمانه اسیر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغی نفتد بی طمع دانه به دامی</p></div>
<div class="m2"><p>عنقای دل ماست که بی دانه اسیر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد که این مرغ دل بال شکسته</p></div>
<div class="m2"><p>در دام سر زلف تو چون شانه اسیر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوریده دلم بازگرفتار جنون شد</p></div>
<div class="m2"><p>زنجیر بیارید که دیوانه اسیر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرگش مگر آزاد کند ورنه حزین را</p></div>
<div class="m2"><p>خاطر به غم فرقت جانانه اسیر است</p></div></div>