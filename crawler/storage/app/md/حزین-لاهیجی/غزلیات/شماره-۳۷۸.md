---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>بزم وصل است و غم هجر همان است که بود</p></div>
<div class="m2"><p>دل پر از حسرت دیدار، چنان است که بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکهت وصل چه حاصل که چمن پیراشد؟</p></div>
<div class="m2"><p>بر رخ کاهیم آن رنگ خزان است که بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خماری ست که از خون دو عالم نشکست؟</p></div>
<div class="m2"><p>چشم مخمور همان دشمن جان است که بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبحه در گردن من مصلحت وقت فکند</p></div>
<div class="m2"><p>ور نه زنار من آن موی میان است که بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش عشق همان است ولی از چه سبب</p></div>
<div class="m2"><p>گرمی داغ تو با دل نه چنان است که بود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب فرو بست نی از ناله، نفس سوخت سپند</p></div>
<div class="m2"><p>دل بی تاب همان گرم فغان است که بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لذّتی نیست به از رقصِ به خون غلتیدن</p></div>
<div class="m2"><p>همچنان بسمل ما، بال فشان است که بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق اگر زیب دهد تخت سلیمانی را</p></div>
<div class="m2"><p>خاتم ملک به آن نام و نشان است که بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لبت اکنون به فسون می برد از خویش مرا</p></div>
<div class="m2"><p>ورنه این باده به کام دگران است که بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیرت از هجر تو نگذاشت خبردار شوم</p></div>
<div class="m2"><p>همچنان دیده به رویت نگران است که بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حرفی از سوز دل اول به لب آورده حزین</p></div>
<div class="m2"><p>یک سخن شمع صفت ورد زبان است که بود</p></div></div>