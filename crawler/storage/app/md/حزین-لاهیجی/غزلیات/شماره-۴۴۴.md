---
title: >-
    شمارهٔ ۴۴۴
---
# شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>به خون هر چند دستی غمرهٔ بیدادگر دارد</p></div>
<div class="m2"><p>شهید خنجر مژگان شدن اجر دگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دور آسمان افتادگان را نیست امّیدی</p></div>
<div class="m2"><p>مگر ما را ز خاک آن حلقهٔ فتراک بردارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی آرد برون هرگز سر از صبح قیامت هم</p></div>
<div class="m2"><p>که می گوید شب حسرت نصیبی ها سحر دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کوی عشق یک طرار می باشد، خبر دارم</p></div>
<div class="m2"><p>به هر جا گم شود دل، طرهٔ شب زو خبر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزین نیم بسمل را به طالع نیست پروازی</p></div>
<div class="m2"><p>که این بلبل قفسها در شکنج بال و پر دارد</p></div></div>