---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>به آب از آتش می داده ام خاک مصلّا را</p></div>
<div class="m2"><p>به باد، از نالهٔ نی دادهام، ناموس تقوا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جبین را سجده فرسای در پیر مغان کردم</p></div>
<div class="m2"><p>به بام کعبهٔ دل می زنم، ناقوس ترسا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برهمن زادهٔ زنّاربندی برده ایمانم</p></div>
<div class="m2"><p>که سودا می کنم باکفر زلفش دبن و دنیا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه ماضی هست پیش من نه مستقبل، خوشا حالم</p></div>
<div class="m2"><p>یکی از قطع خواهش کرده ام، امروز و فردا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رنج و راحت گیتیگل مقصود می چینم</p></div>
<div class="m2"><p>برون آورده ام از پای دل، خار تمنّا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مصفا می کند آیینهٔ دل را نظر بستن</p></div>
<div class="m2"><p>تماشاهاست در هر پرده ای، ترک تماشا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محبّت بر سر هر سنگ فرهاد دگر دارد</p></div>
<div class="m2"><p>چها در عالم امر است، عشق کارفرما را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به لیلی می رساند نسبت آخر، تربت مجنون</p></div>
<div class="m2"><p>به خاک کشتگان عشق، بی پروا منه پا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گوش اهل صورت کی رسد آوازهٔ معنی؟</p></div>
<div class="m2"><p>نوای بلبل دیبا، سزد گلهای دیبا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیات آن را شمارم، کز خودی بستاندم ساقی</p></div>
<div class="m2"><p>به جام می فروشم، شربت خضر و مسیحا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حزین، چون موی آتش دیده می گردد رگ خوابم</p></div>
<div class="m2"><p>به مخمل گر شبی سودا کنم، بالین دیبا را</p></div></div>