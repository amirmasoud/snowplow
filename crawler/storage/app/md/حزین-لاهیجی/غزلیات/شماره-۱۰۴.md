---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>در کوچهٔ آن زلف مده راه صبا را</p></div>
<div class="m2"><p>آشفته مکن مشت غبار دل ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محروم گلستان نبود مرغ اسیرم</p></div>
<div class="m2"><p>تا سوی قفس راه نبسته ست صبا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز ناز تو کز لطف دهد تن به نیازم</p></div>
<div class="m2"><p>با شاهکه دیدهست هم آغوش گدا را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مغروری شمع تو به حدّیست که در بزم</p></div>
<div class="m2"><p>پروانهٔ سوزش ندهد بال هما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشتند ز حسن تو تسلّی به تجلّی</p></div>
<div class="m2"><p>کوته نظران مهر گرفتند، سها را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوبان، چه گروهید که با دعوی انصاف</p></div>
<div class="m2"><p>در شهر شما، کس نخرد جنس وفا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظالم برسان مژده، گر افتاد گذارت</p></div>
<div class="m2"><p>از کوی کسی کش سر ما نیست، خدا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیچیده حزین غلغله در گنبد گردون</p></div>
<div class="m2"><p>از بس که رسا زد نی کلک تو نوا را</p></div></div>