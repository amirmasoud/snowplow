---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>زهر غم هجر تو به جان کارگر افتاد</p></div>
<div class="m2"><p>امّید وصال تو به عمر دگر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قلزم دل نیست همانا، نم خونی</p></div>
<div class="m2"><p>کز دیده به دامان همه لخت جگر افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دامن شب طره سیه مست، گشودی</p></div>
<div class="m2"><p>بویى به دماغ آمد و شوری به سر افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق تو زند راه خراباتی و زاهد</p></div>
<div class="m2"><p>این شعله چه شوخ است که در خشک و تر افتاد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا باکه رخ از باده برافروخته بودی</p></div>
<div class="m2"><p>کآتش به دل عاشق خونین جگر افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هفت صدف گوهر غلتانی اگر هست</p></div>
<div class="m2"><p>اشکی ست که از دامن مژگان تر افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتشکده عشق، دل سوختگان است</p></div>
<div class="m2"><p>بیزارم از آن شعله که در بال و پر افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای آنکه کنی آتش دل تند به دامن</p></div>
<div class="m2"><p>خوش باش که در خرمن جانم شرر افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماند به دل تنگ، نه آزاد و نه بسمل</p></div>
<div class="m2"><p>هر صید که در دام تو بیدادگر افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمد به خیالش، به غلط نکهت زلفی</p></div>
<div class="m2"><p>سنبل به بغل، باد صبا، بی خبر افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد به میان قصهای از سلسله موبی</p></div>
<div class="m2"><p>در حلقهٔ سودازدگان شور و شر افتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این آن غزل نغمه سرایان عراق است</p></div>
<div class="m2"><p>کزکلک حزین تو، چه رنگین گهر افتاد</p></div></div>