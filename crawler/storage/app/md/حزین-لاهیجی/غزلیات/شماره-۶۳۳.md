---
title: >-
    شمارهٔ ۶۳۳
---
# شمارهٔ ۶۳۳

<div class="b" id="bn1"><div class="m1"><p>غم دنیا ندارم، در پی عقبا نمی مانم</p></div>
<div class="m2"><p>به شغل دشمنان، از دوست هرگز وا نمی مانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی گردد گره، مجنون صفت مشت غبار من</p></div>
<div class="m2"><p>خراب وحشتم، زندانی صحرا نمی مانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز امشب مگذران، گر می کنی فکری به روز من</p></div>
<div class="m2"><p>من آتش به جان، چون شمع تا فردا نمی مانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گسستن نیست در پی کاروان بی قراران را</p></div>
<div class="m2"><p>چو موج از خود به هر جانب روم تنها نمی مانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به این ضعفی که نتوانم به سعی از خویشتن رفتن</p></div>
<div class="m2"><p>چرا در خاطر آن یار بی پروا نمی مانم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو طفل اشک، آغوشم به آسایش نمی سازد</p></div>
<div class="m2"><p>گره در دامن مژگان خون پالا نمی مانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرامی گوهرم گرد یتیمی آرزو دارد</p></div>
<div class="m2"><p>حزین از سیر چشمی در دل دریا نمی مانم</p></div></div>