---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>غم توگونه ی گلنار را کهربا سازد</p></div>
<div class="m2"><p>به عشق هر چه مس آرند کیمیا سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوباره زندگی حشر مرگ موعودیست</p></div>
<div class="m2"><p>ز خاک کوی تو ما را اگر جدا سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرور ناز تو دارد ز لطف مأیوسم</p></div>
<div class="m2"><p>عجب که بوی تو با قاصد صبا سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گل به سینهٔ صد چاک من چه می خندی؟</p></div>
<div class="m2"><p>غم تو پیرهن غنچه را قبا سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جدا به مرگ نگردم ز آشنا روبی</p></div>
<div class="m2"><p>که از لبم به سخنهای آشنا سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حزین به سینه دلی فارغ از دوا دارم</p></div>
<div class="m2"><p>که درد عشق به دلهای مبتلا سازد</p></div></div>