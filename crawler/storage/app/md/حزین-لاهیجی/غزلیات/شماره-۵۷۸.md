---
title: >-
    شمارهٔ ۵۷۸
---
# شمارهٔ ۵۷۸

<div class="b" id="bn1"><div class="m1"><p>بود یارم غم دیرینهء خویش</p></div>
<div class="m2"><p>پریزادم دل بی کینهٔ خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنانم درکف طفلی ست خود رای</p></div>
<div class="m2"><p>ندانم شنبه و آدینهٔ خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود عمری که می سازد چو شیران</p></div>
<div class="m2"><p>تن آزاده با پشمینهء خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به امّیدگشاد تیر نازی</p></div>
<div class="m2"><p>هدف دارم به حسرت سینهٔ خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاراید بساطم را متاعی</p></div>
<div class="m2"><p>چو داغم، گوهر گنجینهٔ خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی باشد خماری مستیم را</p></div>
<div class="m2"><p>خرابم از می پارینهٔ خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین از هر دو عالم تافتم روی</p></div>
<div class="m2"><p>ز دل کردم چو آب، آیینه خویش</p></div></div>