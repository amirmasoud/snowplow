---
title: >-
    شمارهٔ ۴۵۸
---
# شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>دور عذار تو، خط وجود ندارد</p></div>
<div class="m2"><p>آتش سوزان برق، دود ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت ز فریبت گرفته کیش برهمن</p></div>
<div class="m2"><p>کیست که پیشت سر سجود ندارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش تعلّق ضمیر من نپذیرد</p></div>
<div class="m2"><p>عکس در آیینهام، نمود ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه تلف می کنی به طور چه حاصل؟</p></div>
<div class="m2"><p>جز دل ما طاقت شهود ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن تو بست از بهار چشم حزین را</p></div>
<div class="m2"><p>پیش جمال توِ گل نمود ندارد</p></div></div>