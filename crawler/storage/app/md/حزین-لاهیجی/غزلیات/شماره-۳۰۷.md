---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>ای وای بر اسیری، کز یاد رفته باشد</p></div>
<div class="m2"><p>در دام مانده باشد، صیاد رفته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آه دردناکی سازم خبر دلت را</p></div>
<div class="m2"><p>روزی که کوه صبرم، بر باد رفته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رحم است بر اسیری، کز گرد دام زلفت</p></div>
<div class="m2"><p>با صد امیدواری، ناشاد رفته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادم که از رقیبان دامن کشان گذشتی</p></div>
<div class="m2"><p>گو مشت خاک ما هم، بر باد رفته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه از دمی که تنها با داغ او چو لاله</p></div>
<div class="m2"><p>در خون نشسته باشم، چون باد رفته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونش به تیغ حسرت یارب حلال بادا</p></div>
<div class="m2"><p>صیدی که از کمندت، آزاد رفته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرشور از حزین است امروز کوه و صحرا</p></div>
<div class="m2"><p>مجنون گذشته باشد، فرهاد رفته باشد</p></div></div>