---
title: >-
    شمارهٔ ۸۵۴
---
# شمارهٔ ۸۵۴

<div class="b" id="bn1"><div class="m1"><p>ز دام طره، شکنهای دلربا بنمای</p></div>
<div class="m2"><p>نوازشی به من محنت آزما بنمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث نرگس مست تو می کنم، عمریست</p></div>
<div class="m2"><p>ز یک نگه، گل صد گونه مرحبا، بنمای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار عقده فزون است، در رگ جانم</p></div>
<div class="m2"><p>ز چین زلف، نسیم گره گشا، بنمای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زهد خشک، به تنگ است خاطرم، ساقی</p></div>
<div class="m2"><p>هلال ابروی جام جهان نما، بنمای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دور نرگس او، محتسب، مرنج از من</p></div>
<div class="m2"><p>جهانیان همه مستند، پارسا بنمای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علاج درد من از پرسشی توان کردن</p></div>
<div class="m2"><p>فسونی از لب لعل کرشمه زا بنمای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حزین ، چو غنچه، چرا مهر بر دهان زده ای؟</p></div>
<div class="m2"><p>ترنمی به هزاران خوشنوا، بنمای</p></div></div>