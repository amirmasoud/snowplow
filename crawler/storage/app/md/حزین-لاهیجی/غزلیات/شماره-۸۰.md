---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>داغند، ز رخسار تو ای رشک چمن‌ها</p></div>
<div class="m2"><p>چون لاله، شهیدان به سمن‌زار کفن‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شرم، صدف را به دهان مهر خموشی ست</p></div>
<div class="m2"><p>تا شد صدف گوهر نام تو، دهن‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون در جگر نافهٔ دل چون نشود خشک؟</p></div>
<div class="m2"><p>در هر شکن زلف تو افتاده ختن‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چاشنی لذت زندان غمت رفت</p></div>
<div class="m2"><p>از خاطر یوسف صفتان، یاد وطن‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگذاشت به جا آتش عشق تو سپندی</p></div>
<div class="m2"><p>من مانده‌ام از سوخته‌جان‌ها تن تنها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارد لب خاموش، هم‌آغوشی معنی</p></div>
<div class="m2"><p>بر چهرهٔ اندیشه نقاب است سخن‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خاک، حزین یاد عقیق لب او برد</p></div>
<div class="m2"><p>گرد سر این خاک شود، خون یمن‌ها</p></div></div>