---
title: >-
    شمارهٔ ۶۳۶
---
# شمارهٔ ۶۳۶

<div class="b" id="bn1"><div class="m1"><p>پیش از ظهور جلوهٔ جانانه سوختیم</p></div>
<div class="m2"><p>آتش به سنگ بود که ما خانه سوختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب ناچشیده از نفس آتشین خویش</p></div>
<div class="m2"><p>چون داغ لاله، باده به پیمانه سوختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بوده است محفل شمع طراز ما</p></div>
<div class="m2"><p>خود را عبث به کعبه وبتخانه سوختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک شعله برق خرمن دلها بود ولی</p></div>
<div class="m2"><p>ما گرمتر ز سوزش پروانه سوختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوابم حزین ز مصرع وحدت به دیده سوخت</p></div>
<div class="m2"><p>ما خود نفس ز گفتن افسانه سوختیم</p></div></div>