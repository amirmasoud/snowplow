---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>نخواهد برد از ما صرف‌های خصم عنید ما</p></div>
<div class="m2"><p>جبین از خون قاتل سرخ می‌سازد شهید ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گوش نغمه سنجان چمن بیگانه می‌آید</p></div>
<div class="m2"><p>برون از پرده دل چون فتد گفت و شنید ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثمر در عالم انصاف ازین بهتر نمی‌باشد</p></div>
<div class="m2"><p>تن آزادگان می‌پرورد در سایه، بید ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مغانی باده ریزد، خانقاهی می به دور آرد</p></div>
<div class="m2"><p>اگر پیر خرابات مغان گردد مرید ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیه‌روزی ما را اعتباری نیست چندانی</p></div>
<div class="m2"><p>به بازی جامه را در نیل زد، بخت سفید ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا گر مرد ساز و سوز عشقی ناله‌ای بشنو</p></div>
<div class="m2"><p>که آتش می‌زند در خشک و تر، طرز نشید ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشاد کار خود را دیده‌ام در عشق و رسوایی</p></div>
<div class="m2"><p>حزین ، از سینهٔ چاک است درگاه امید ما</p></div></div>