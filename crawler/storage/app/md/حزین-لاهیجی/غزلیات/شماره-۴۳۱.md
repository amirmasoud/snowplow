---
title: >-
    شمارهٔ ۴۳۱
---
# شمارهٔ ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>شور سودای تو در کودکی، استادم بود</p></div>
<div class="m2"><p>کوه و صحرا همه جا عرصهٔ فریادم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سختی هجر، نزد شیشهٔ ناموس به سنگ</p></div>
<div class="m2"><p>قاف تا قاف جهان بزم پریزادم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رم آهوی ختن پیش دلم زانو زد</p></div>
<div class="m2"><p>سینه تا جلوه گه شوخی صیّادم بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک یادآوریش دفتر نسیانم داد</p></div>
<div class="m2"><p>آه اگر عهد فراموشی او یادم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعل وارون من از حلقهٔ گیسوی کسی ست</p></div>
<div class="m2"><p>که سری با شکن طرّهٔ شمشادم بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر شوریده سر صومعهٔ قدس منم</p></div>
<div class="m2"><p>یاد آن سلسله مو، حلقهٔ اورادم بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بیدادگری، جرعه به خونم می زد</p></div>
<div class="m2"><p>مژه در قبضهٔ او، خنجر فولادم بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چارهٔ عقدهٔ خاطر نتوانستی کرد</p></div>
<div class="m2"><p>چون جرس در کف اگر پنجهٔ فولادم بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب که این تازه غزل نقش، حزین ، می بستم</p></div>
<div class="m2"><p>قلمی سوخته از خامهٔ بهزادم بود</p></div></div>