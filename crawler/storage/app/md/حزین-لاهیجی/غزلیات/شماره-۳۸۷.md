---
title: >-
    شمارهٔ ۳۸۷
---
# شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>ز حشر مستی ما را چه باک خواهد بود؟</p></div>
<div class="m2"><p>چو نامه در کف ما برگ تاک خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دستبرد نگاهت، چو صبح روشن شد</p></div>
<div class="m2"><p>که تا به حشر، مرا سینه چاک خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان شانه سر حرف کی به چنگ آرد؟</p></div>
<div class="m2"><p>چنین که طرّه تو را، تابناک خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا به سجدهٔ اهریمنان به خاک نهی</p></div>
<div class="m2"><p>سری که در قدم دوست خاک خواهد بود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزین اگر رخ ساقی عرق فشان گردد</p></div>
<div class="m2"><p>تو را ز دل صدف سینه پاک خواهد بود</p></div></div>