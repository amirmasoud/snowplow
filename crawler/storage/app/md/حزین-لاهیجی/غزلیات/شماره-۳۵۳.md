---
title: >-
    شمارهٔ ۳۵۳
---
# شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>اهل نظر، از آن در یکتا چه دیده اند</p></div>
<div class="m2"><p>با دیدهٔ حباب ز دریا چه دیده اند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن بتان به ساده دلی ها نمی رسد</p></div>
<div class="m2"><p>آیینه خاطران، ز تماشا چه دیده اند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حجّ قبول، کعبهٔ دیدار دیدن است</p></div>
<div class="m2"><p>از پای سعی آبله فرسا چه دیده اند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد چشم ما ز نعمت عمر دو روزه سیر</p></div>
<div class="m2"><p>از روزگار، خضر و مسیحا چه دیده اند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل، سراغ لیلی صحرانشین شود</p></div>
<div class="m2"><p>خواری کشان ز آبلی پا چه دیده اند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارند هر طرف چو صفت جرگه در میان</p></div>
<div class="m2"><p>صیاد پیشگان ز دل ما چه دیده اند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خون دیده پرورش تاک می کنند</p></div>
<div class="m2"><p>رندان میگسار ز صهبا چه دیده اند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما نقش خود ز خال لب یار دیده ایم</p></div>
<div class="m2"><p>تا اهل دل ز خال سویدا چه دیده اند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون می توان ز ترک طلب، کام دل گرفت</p></div>
<div class="m2"><p>دون همّتان، ز عرض تمنا چه دیده اند؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیدا دلان، ندانم از آن بی نشان حزین</p></div>
<div class="m2"><p>پنهان کدام شیوه و پیدا چه دیده اند؟</p></div></div>