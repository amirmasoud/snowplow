---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>آماده است تا مژهٔ ما به هم خورد</p></div>
<div class="m2"><p>سیلی کزو خرابهٔ دنیا به هم خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل تلاطم و ز تو دامن فشاندنی</p></div>
<div class="m2"><p>از یک نسیم لنگر دریا به هم خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد قیمتم شکسته ز انصاف طالبان</p></div>
<div class="m2"><p>لب در همین دعاست که سودا به هم خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاشد چنین اگر فلک، احباب را ز هم</p></div>
<div class="m2"><p>نَبوَد عجب که عقد ثریّا به هم خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل به عهد سست حیات اعتماد نیست</p></div>
<div class="m2"><p>امروز گیرد الفت و فردا به هم خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پهلوی سخن گسلد ربط همدمان</p></div>
<div class="m2"><p>پیوسته الفتِ لبِ گویا به هم خورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک دست شیشه داری و دستی دل حزین</p></div>
<div class="m2"><p>ساقی چنان مکن که دو مینا به هم خورد</p></div></div>