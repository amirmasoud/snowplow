---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>برآ از خویش زاهد، وقت شبگیر خرابات است</p></div>
<div class="m2"><p>علاج زهد خشکت، ساغر پیر خرابات است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دام عنکبوت سبحه و سجاده دل برکن</p></div>
<div class="m2"><p>بیا صید بط میکن که نخجیر خرابات است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراب گردش ساغر، فدای جلوهٔ ساقی</p></div>
<div class="m2"><p>مرا تلقین این ذکر خوش ازپیر خرابات است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرنج ای شیخ از من گر سخن بی پرده می گویم</p></div>
<div class="m2"><p>که این بی پرده گفتنها ز تاثیر خرابات است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزین دُردنوش مست را چون خود نپنداری</p></div>
<div class="m2"><p>تو زاهد گربهٔ محراب و او شیر خرابات است</p></div></div>