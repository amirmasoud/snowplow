---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>خورشید و ماه آینهٔ حسن یار نیست</p></div>
<div class="m2"><p>عینک حجاب گردد، اگر دیده تار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشتی که شوق آبله پا قطره می زند</p></div>
<div class="m2"><p>یک خار، زیر منّت ابر بهار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موسی صفت ز آتش غیرت نمی روم</p></div>
<div class="m2"><p>در سایهٔ نهالی، اگر شعله بار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانع نمی شود کف بی مایه، سیل را</p></div>
<div class="m2"><p>دامن حریف گریهٔ بی اختیار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوده است خاطر، اگر درد بی دواست</p></div>
<div class="m2"><p>طوفان غم خوش است، اگر غمگسار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصح ز ناله منع دلم چون جرس مکن</p></div>
<div class="m2"><p>آسوده خاطرت که دمی بیقرار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست تغافلی به حزین نیازمند</p></div>
<div class="m2"><p>هرگز تو را غم دل امّیدوار نیست</p></div></div>