---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>هر چه بستیم و گشودیم عبث</p></div>
<div class="m2"><p>هر چه گفتیم و شنودیم عبث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه مقصود به جایی نرسید</p></div>
<div class="m2"><p>پای پر آبله سودیم عبث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غفلت از حادثهٔ دهر بلاست</p></div>
<div class="m2"><p>در ره سیل غنودیم عبث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرصهٔ هر دو جهان تنگ فضاست</p></div>
<div class="m2"><p>بال پرواز گشودیم عبث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی چهره به ما گشته حزین</p></div>
<div class="m2"><p>عبث آیینه زدودیم، عبث</p></div></div>