---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>امّید گداست، تا در بازی هست</p></div>
<div class="m2"><p>معشوق غنیّ و عشق را آزی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسته به دواتند، نه با خسته دوا</p></div>
<div class="m2"><p>بیچاره نیاز و چاره را نازی هست</p></div></div>