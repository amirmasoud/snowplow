---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>وبرانهٔ هند،کز صفا پاک بود</p></div>
<div class="m2"><p>خاکش نمک دیدهٔ ادراک بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبش به بغل، شیشهٔ ساعت دارد</p></div>
<div class="m2"><p>مینای حباب او پر از خاک بود</p></div></div>