---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>کی بود که دل بستهٔ زنار نبود؟</p></div>
<div class="m2"><p>جان در شکن طره، گرفتار نبود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر در قدم پیر مغان می سودم</p></div>
<div class="m2"><p>آن روزکه در بتکده، دیار نبود</p></div></div>