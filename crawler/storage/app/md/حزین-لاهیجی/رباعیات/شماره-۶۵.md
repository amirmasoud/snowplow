---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>دانی که به من در غمت آیا چه گذشت؟</p></div>
<div class="m2"><p>بر سرچون شمع،بی تو شبها چه گذشت؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درد فراق، ما ز خود بی خبریم</p></div>
<div class="m2"><p>آیا خبرت هست که بر ما چه گذشت؟</p></div></div>