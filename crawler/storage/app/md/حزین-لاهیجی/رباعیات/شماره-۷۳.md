---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>اندوه جهان و شادمانی همه هیچ</p></div>
<div class="m2"><p>سرمایهٔ روزگارِ فانی همه هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد این سخن از تجربه کاران دارم</p></div>
<div class="m2"><p>غیر از غم یار جاودانی همه هیچ</p></div></div>