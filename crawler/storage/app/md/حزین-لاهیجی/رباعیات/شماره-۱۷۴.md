---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>ای عوج زمانه، قد چالاک بکش</p></div>
<div class="m2"><p>گردن به عروج قبهٔ خاک بکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی قوت چرا نشسته ای بسته دهان</p></div>
<div class="m2"><p>برخیز سری به ک.ن افلاک بکش</p></div></div>