---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>زهرم به قدح دهی که می نوش مکن</p></div>
<div class="m2"><p>در آتشم افکنی که هان جوش مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری چو ز خون عاشقان می نوشی</p></div>
<div class="m2"><p>این لخت کباب را فراموش مکن</p></div></div>