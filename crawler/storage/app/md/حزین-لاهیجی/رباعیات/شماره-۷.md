---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>سرمایه دهر، خاک بیزی ست،که هست</p></div>
<div class="m2"><p>در مزرع حسرت اشک ریزی ست، که هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگاهی و دریافت، گران است که نیست</p></div>
<div class="m2"><p>ارزانِ زمانه بی تمیزی ست، که هست</p></div></div>