---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>از داغ فراق سینه ام جوشان است</p></div>
<div class="m2"><p>هوش من شوریده ز مدهوشان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم تو، شمع گوید احوال مرا</p></div>
<div class="m2"><p>این چرب زبان وکیل خاموشان است</p></div></div>