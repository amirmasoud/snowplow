---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>افسوس که درد عشق و درمان هم نیست</p></div>
<div class="m2"><p>داغ دل گرم و مهر جانان هم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون در طلب نعمت الوان نخورم</p></div>
<div class="m2"><p>تنها نه که نان نمانده، دندان هم نیست</p></div></div>