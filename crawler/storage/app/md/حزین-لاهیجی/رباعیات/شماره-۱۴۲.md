---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>ابنای زمان دُرد و صفا را ندهند</p></div>
<div class="m2"><p>هرگز پر کاه کهربا را ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این قوم، ولینعمت امثال خودند</p></div>
<div class="m2"><p>تا سگ بود، استخوان هما را ندهند</p></div></div>