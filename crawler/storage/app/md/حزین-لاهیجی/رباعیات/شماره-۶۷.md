---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>دیشب طربی بر دل غمناکم ریخت</p></div>
<div class="m2"><p>هر بخیه که داشت، سینهٔ چاکم ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبنم به کنار چشم نمناکم ریخت</p></div>
<div class="m2"><p>ابری دو سه قطره اشک بر خاکم ریخت</p></div></div>