---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>زان پیش که دی آفت بستان گردد</p></div>
<div class="m2"><p>اوراق گل از خزان پریشان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی، تو که ابر رحمتی، رشحه ببار</p></div>
<div class="m2"><p>تا بلبل طبع من غزل خوان گردد</p></div></div>