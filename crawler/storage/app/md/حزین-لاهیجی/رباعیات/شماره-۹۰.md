---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>یا رب چه شود گرکرمت یار افتد؟</p></div>
<div class="m2"><p>لطفت به شکستگان پرستار افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمخوارگی خلق جهان را دیدم</p></div>
<div class="m2"><p>مگذار که با غیر توام کار افتد</p></div></div>