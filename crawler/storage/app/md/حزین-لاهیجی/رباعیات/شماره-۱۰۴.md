---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>از عکس رخ تو، گلستان پیدا شد</p></div>
<div class="m2"><p>وز سایه تو، سرو روان پیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود جمله جهان صورت یکتایی توست</p></div>
<div class="m2"><p>از هر دو کف تو، بحر و کان پیدا شد</p></div></div>