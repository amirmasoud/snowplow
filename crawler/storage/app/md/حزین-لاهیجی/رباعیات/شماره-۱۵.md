---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>آن را که نصیب از خرد ادراک است</p></div>
<div class="m2"><p>در معرکهٔ جهاد خود چالاک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چندکه زنده پاک و مرده ست پلید</p></div>
<div class="m2"><p>این نفس پلید چون بمیرد پاک است</p></div></div>