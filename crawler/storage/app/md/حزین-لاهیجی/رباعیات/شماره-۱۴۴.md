---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>گردد چو خراب تن، چه غم؟ جان باشد</p></div>
<div class="m2"><p>ویران چو شود حباب، عمّان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد و ستد عشق زیانش سود است</p></div>
<div class="m2"><p>گر جان برود چه باک؟ جانان باشد</p></div></div>