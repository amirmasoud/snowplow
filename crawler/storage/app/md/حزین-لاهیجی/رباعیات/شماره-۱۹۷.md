---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>زیبا صنما ز بی قراران توایم</p></div>
<div class="m2"><p>ما دلشدگان سینه فگاران توایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود ز چه رو گوشه ی چشمیت به ما؟</p></div>
<div class="m2"><p>ای ساقی بزم، میگساران توایم</p></div></div>