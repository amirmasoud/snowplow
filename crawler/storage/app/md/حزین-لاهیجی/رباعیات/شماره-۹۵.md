---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>نظّارهٔ زشت، دیده را میل کشید</p></div>
<div class="m2"><p>سرمایهٔ عزّتم به تنزیل کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درّاعه بخت سبز ما را گردون</p></div>
<div class="m2"><p>از خاک سیاه هند، در نیل کشید</p></div></div>