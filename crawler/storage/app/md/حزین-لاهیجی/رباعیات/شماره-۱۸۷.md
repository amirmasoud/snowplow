---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>اندوه چو بیش شد گرفتم کم دل</p></div>
<div class="m2"><p>دل ماتم من گرفت و من ماتم دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز کجاست ور بود همدم دل؟</p></div>
<div class="m2"><p>گفتن نتوان به غمگساران غم دل</p></div></div>