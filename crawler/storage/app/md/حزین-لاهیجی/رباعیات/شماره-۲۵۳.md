---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>تا چهره ز اشک ارغوانی نکنی</p></div>
<div class="m2"><p>در محفل عیش، گل فشانی نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز چون شمع، جا به بزمت ندهند</p></div>
<div class="m2"><p>گر با همه کس چرب زبانی نکنی</p></div></div>