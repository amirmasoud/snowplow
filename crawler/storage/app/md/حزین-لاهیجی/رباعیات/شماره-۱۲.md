---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>در محفل آسمان سُها و خور هست</p></div>
<div class="m2"><p>در بحر جهان هم صدف و هم در هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خود چه بود قسمت روزی طلبان</p></div>
<div class="m2"><p>هم مائدهٔ عیسی و هم آخور هست</p></div></div>