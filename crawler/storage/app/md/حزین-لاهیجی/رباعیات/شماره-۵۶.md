---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>دردا که دری نسفته می باید رفت</p></div>
<div class="m2"><p>راز دل خود نگفته می باید رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می باید داد، جان شیرین بی تو</p></div>
<div class="m2"><p>تلخی ز تو ناشنفته می باید رفت</p></div></div>