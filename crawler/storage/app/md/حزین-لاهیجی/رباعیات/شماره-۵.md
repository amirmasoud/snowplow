---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>در دیدهٔ هر که شق کند پردهٔ خواب</p></div>
<div class="m2"><p>سرتاسر آفاق بود موج سراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی، قدحی در ده از آن بادهٔ ناب</p></div>
<div class="m2"><p>سرّ دو جهان بشنو ازین مست خراب</p></div></div>