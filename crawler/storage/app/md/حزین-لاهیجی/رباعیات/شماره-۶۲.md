---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>دیوانه دلم، یارِِ دل آسایی نیست</p></div>
<div class="m2"><p>شوریده سرم دامن صحرایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لحن داوود و حسن یوسف، خوار است</p></div>
<div class="m2"><p>گوش شنوا و چشم بینایی نیست</p></div></div>