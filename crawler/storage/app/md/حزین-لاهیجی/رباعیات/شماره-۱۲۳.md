---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>بر پای بت، از نیاز پیشانی زد</p></div>
<div class="m2"><p>ناقوس فرنگ، در صنم خوانی زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حیرتم از دل، که به این سیرت و شان</p></div>
<div class="m2"><p>بی شرم چه سان لاف مسلمانی زد؟</p></div></div>