---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>پیوسته دی و تموز دم سردی کرد</p></div>
<div class="m2"><p>با دردکشان زمانه بی دردی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه سحر و ناله به جایی نرسید</p></div>
<div class="m2"><p>طالع پستی و دهر نامردی کرد</p></div></div>