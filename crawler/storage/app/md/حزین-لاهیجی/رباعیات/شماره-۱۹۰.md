---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>آنم که به ملک نیستی سلطانم</p></div>
<div class="m2"><p>با سامانم اگر چه بی سامانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیری ست چو آسیا درین کهنه سرا</p></div>
<div class="m2"><p>سرگردانم که از چه سرگردانم</p></div></div>