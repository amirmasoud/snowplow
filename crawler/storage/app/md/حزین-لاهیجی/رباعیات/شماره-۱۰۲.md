---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>تیغم به زبون کشی چو مانوس نبود</p></div>
<div class="m2"><p>در قبضهٔ قدرتم جز افسوس نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنگار گرفته گر ببینی چه عجب؟</p></div>
<div class="m2"><p>شمشیر زدن به گربه ناموس نبود</p></div></div>