---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>دم سردی زاهدان کافورمزاج</p></div>
<div class="m2"><p>افسرد حرارت به عروق و اوداج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر بی مزه گشته دور گردون، چه شدند</p></div>
<div class="m2"><p>آنها که دهند دور پیمانه رواج؟</p></div></div>