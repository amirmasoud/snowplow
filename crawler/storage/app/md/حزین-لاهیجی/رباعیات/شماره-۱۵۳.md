---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>در هجر حزین، از غم جانکاه بمیر</p></div>
<div class="m2"><p>چون شمع سحرگاه، به یک آه بمیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن قدر نداری که درآیی به نجف</p></div>
<div class="m2"><p>جان تو درآید، تو درین راه بمیر</p></div></div>