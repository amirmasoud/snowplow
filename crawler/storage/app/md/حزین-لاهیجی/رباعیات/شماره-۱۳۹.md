---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>کمتر به وصال، قرعهٔ کار افتد</p></div>
<div class="m2"><p>هجر است که در میانه بسیار افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکبار تو را دیدم و از خویش شدم</p></div>
<div class="m2"><p>تا کی دگر اتفاق دیدار افتد</p></div></div>