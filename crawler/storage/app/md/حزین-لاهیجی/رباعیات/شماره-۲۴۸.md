---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>هم درد و دوای دل افگار تویی</p></div>
<div class="m2"><p>عاشق تویی و عشق تویی، یار تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرگار تویی، نقطه تویی، دایره تو</p></div>
<div class="m2"><p>یعنی که ز هر پرده، پدیدار تویی</p></div></div>