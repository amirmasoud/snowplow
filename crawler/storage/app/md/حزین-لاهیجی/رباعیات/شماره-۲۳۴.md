---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>ای صیت بزرگی به جهان افکنده</p></div>
<div class="m2"><p>دین را به درم داده، شکم آکنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فردا نبود یکی به معیارِ قبول</p></div>
<div class="m2"><p>مقدارِ خدابنده و دنیابنده</p></div></div>