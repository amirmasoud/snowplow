---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>پنهان به سیه خانهٔ حرف است نقط</p></div>
<div class="m2"><p>با آنکه ز نقطه شد پدید آن همه خط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک نقطهٔ توحید درین دایره هاست</p></div>
<div class="m2"><p>گر مردمک دیده نیفتد به غلط</p></div></div>