---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>هر چند سپهر فکرم اختربار است</p></div>
<div class="m2"><p>بر دوش زبان، سخنوری سربار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خامهٔ تیره بخت خود ممنونم</p></div>
<div class="m2"><p>این ابر سیاهی ست که گوهربار است</p></div></div>