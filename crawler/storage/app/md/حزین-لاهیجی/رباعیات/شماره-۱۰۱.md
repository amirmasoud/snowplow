---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>عاقل تحصیل علم بیجا چه کند؟</p></div>
<div class="m2"><p>در خرکده زمانه، دانا چه کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که به عیش بگذرد، زر به کف آر</p></div>
<div class="m2"><p>معشوقهٔ نان، قُلْتُ و قُلنا چه کند؟</p></div></div>