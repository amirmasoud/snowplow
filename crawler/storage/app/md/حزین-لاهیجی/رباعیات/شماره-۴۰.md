---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>هستی بزمی ست، انجمن سازی هست</p></div>
<div class="m2"><p>عالم نطعی ست، پنج و شش بازی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جام جم و مهر سلیمان این بود</p></div>
<div class="m2"><p>ما کارگهیم، کارپردازی هست</p></div></div>