---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>آلودهٔ زهد کرده ام دامانی</p></div>
<div class="m2"><p>وجّهت مِنَ المسجدِ نحو الحانِ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما رخت ز کوی نیکنامی بردیم</p></div>
<div class="m2"><p>نستودِعُکُم معاشر الاخوانِ</p></div></div>