---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>صوفی برخیز های و هوبی بزنیم</p></div>
<div class="m2"><p>آتش در دل به یاد رویی بزنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سینهٔ تنگ، نعرهٔ مستانه</p></div>
<div class="m2"><p>در نیم شبان بر سر کویی بزنیم</p></div></div>