---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>غمنامهٔ ما خواند و جوابی ننوشت</p></div>
<div class="m2"><p>از لطف گذشتیم و عتابی ننوشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر به امید ستمش خوش می بود</p></div>
<div class="m2"><p>بی رحم، خراجی به خرابی ننوشت</p></div></div>