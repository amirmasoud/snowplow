---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>جانا چه بود که خاطری شاد کنی</p></div>
<div class="m2"><p>وز لطف، دل خرابی آباد کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرگی نبود غیر فراموشی تو</p></div>
<div class="m2"><p>در خاک شوم زنده، گرم یاد کنی</p></div></div>