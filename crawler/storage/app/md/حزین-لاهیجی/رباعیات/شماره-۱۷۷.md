---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>تا عشق فکند در دلم تاب چو شمع</p></div>
<div class="m2"><p>یک لمحه ندید دیده‌ام خواب چو شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد ز مشرب سمندر زادم</p></div>
<div class="m2"><p>زآتش رگ جان من خورد آب چو شمع</p></div></div>