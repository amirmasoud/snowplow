---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>از دیده به دیده، ناوک اندازی هست</p></div>
<div class="m2"><p>از سینه به سینه، قاصد رازی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوانده ام رقم دفتر دلها، این بود</p></div>
<div class="m2"><p>ما کارگهیم، کارپردازی هست</p></div></div>