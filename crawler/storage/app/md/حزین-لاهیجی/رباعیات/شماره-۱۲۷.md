---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>عالی گهران، بنده نژادان منند</p></div>
<div class="m2"><p>خونین جگران، مایه کسادان منند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کشور خود، سلطنت ماست قدیم</p></div>
<div class="m2"><p>پیران مغانه، خانه زادان منند</p></div></div>