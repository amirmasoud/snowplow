---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>در ماتم تو، ملک و ملک شیون کرد</p></div>
<div class="m2"><p>گردون کفن کبود در گردن کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست غم تو ز ما مصیبت زدگان</p></div>
<div class="m2"><p>هر جیب که داشت، چاک تا دامن کرد</p></div></div>