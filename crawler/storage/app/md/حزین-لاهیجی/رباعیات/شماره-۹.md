---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>هر چند که ما رهرو و دنیا راه است</p></div>
<div class="m2"><p>در راه نشستن خطر آگاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین شرم نشسته ام که پیرایهٔ تن</p></div>
<div class="m2"><p>گر برخیزم به قامتم کوتاه است</p></div></div>