---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>ای گل تو به بویی دل خود شاد مکن</p></div>
<div class="m2"><p>با رنگ پریده جلوه بنیاد مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل، تو هم افسانه فروشی بگذار</p></div>
<div class="m2"><p>کار دل ماست عشق، فریاد مکن</p></div></div>