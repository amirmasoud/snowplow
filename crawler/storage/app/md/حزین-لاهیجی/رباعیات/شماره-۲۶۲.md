---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>هر دم ز تو عمر می کَنَد بیخ و بنی</p></div>
<div class="m2"><p>جز وعده به فردا نشناسی سخنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیروز تو را که هست فردا، امروز</p></div>
<div class="m2"><p>بنگر که چه کرده ای که فردا نکنی</p></div></div>