---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>تا ناله، درفش کاویانی نکنی</p></div>
<div class="m2"><p>در کشور دهر، قهرمانی نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جان طلبند، منت از بخت بدار</p></div>
<div class="m2"><p>در مسلخ عشق، سخت جانی نکنی</p></div></div>