---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>تا عشق تو گشت از ازل روزی دل</p></div>
<div class="m2"><p>بربست میان را به غم اندوزی دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد تو کند مگر پرستاری جان</p></div>
<div class="m2"><p>داغ تو کند مگر جگرسوزی دل</p></div></div>