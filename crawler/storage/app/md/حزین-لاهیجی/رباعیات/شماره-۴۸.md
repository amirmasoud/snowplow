---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>دل خوش نکند نالهٔ زاری که مراست</p></div>
<div class="m2"><p>وز گریه نمی رود غباری که مراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همّت من دولت دنیا چه کند؟</p></div>
<div class="m2"><p>این میکده نشکند خماری که مراست</p></div></div>