---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>از حوصلهٔ صبر، غمت بیرون است</p></div>
<div class="m2"><p>هر لحظه دل از فراق، دیگرگون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دیده چه سازیم که چون شب باز است؟</p></div>
<div class="m2"><p>از شوق چه گوییم که روزافزون است</p></div></div>