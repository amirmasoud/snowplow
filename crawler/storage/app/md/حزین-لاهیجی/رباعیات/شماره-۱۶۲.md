---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>گر ترک کم و بیش کنی اولی تر</p></div>
<div class="m2"><p>خو با دل درویش کنی، اولی تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند دوی بر درِ دونان پی وام؟</p></div>
<div class="m2"><p>وام از شکم خویش کنی اولی تر</p></div></div>