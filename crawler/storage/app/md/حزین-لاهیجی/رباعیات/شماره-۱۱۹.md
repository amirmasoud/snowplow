---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>شاهنشهی خلق جهان نتوان کرد</p></div>
<div class="m2"><p>حمّالی این بار گران نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر در ره این گنده کسان نتوان کرد</p></div>
<div class="m2"><p>پاکاری این کونِ خران نتوان کرد</p></div></div>