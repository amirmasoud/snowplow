---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>از خصمی مردمان مرا حال نکوست</p></div>
<div class="m2"><p>یاران همه دشمنند، خصمان همه دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هر که دل آرمید از دوست رمید</p></div>
<div class="m2"><p>وز هر که بتافت روی دل جانب اوست</p></div></div>