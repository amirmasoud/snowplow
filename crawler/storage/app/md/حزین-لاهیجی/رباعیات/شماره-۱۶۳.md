---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>از خصمی روزگار بی مهر و تمیز</p></div>
<div class="m2"><p>تا چند زنیم سینه بر خنجرِ تیز؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی ناخن تدبیر و نه بازوی ستیز</p></div>
<div class="m2"><p>نه جای شکیبایی و نه پای گریز</p></div></div>