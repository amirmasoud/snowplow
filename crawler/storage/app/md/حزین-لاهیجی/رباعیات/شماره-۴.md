---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>کردی دلم از حسن گلوسوز کباب</p></div>
<div class="m2"><p>نه پرتو لطف دیده نه برق عتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهیم به عشق، نیم بسمل شده ماند</p></div>
<div class="m2"><p>کز گرمی خون ماست، شمشیر تو آب</p></div></div>