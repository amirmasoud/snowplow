---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>باطل کیشان بر اهل حق چیر شدند</p></div>
<div class="m2"><p>روبه بازان سگ صفت، شیر شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دجّال وشان، نام مسیحا کردند</p></div>
<div class="m2"><p>کودک طبعان بوالهوس پیر شدند</p></div></div>