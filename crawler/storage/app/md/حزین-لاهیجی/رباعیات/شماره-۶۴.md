---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>دلبر بسیار و دل نگهدار کم است</p></div>
<div class="m2"><p>دلدار کم و چه کم که بسیار کم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند به عالم تو چرا بی یاری؟</p></div>
<div class="m2"><p>یاران چه کنم؟ یار وفادار کم است</p></div></div>