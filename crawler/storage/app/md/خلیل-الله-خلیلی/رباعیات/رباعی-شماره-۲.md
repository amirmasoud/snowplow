---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>اگر دانی زبان اختران را</p></div>
<div class="m2"><p>شبانه بشنوی راز جهان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سکوت شب به صد آهنگ خواند</p></div>
<div class="m2"><p>به گوشت قصه های آسمان را</p></div></div>