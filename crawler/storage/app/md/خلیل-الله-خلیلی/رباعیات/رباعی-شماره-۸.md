---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای غره به اینکه دهر فرمانبر توست</p></div>
<div class="m2"><p>وین ماه و ستاره و فلک چاکر توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که ترا چاکر خویش پندارند</p></div>
<div class="m2"><p>آن مورچگان که رزقشان پیکر توست</p></div></div>