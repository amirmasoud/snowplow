---
title: >-
    رباعی شمارهٔ ۴۸
---
# رباعی شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>چون در کف روزگار گشتیم زبون</p></div>
<div class="m2"><p>چون ساغر عشق و آرزو گشت نگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جاسوس خرد دگر چه جوید از ما</p></div>
<div class="m2"><p>گوید کزین شهر کشد رخت برون</p></div></div>