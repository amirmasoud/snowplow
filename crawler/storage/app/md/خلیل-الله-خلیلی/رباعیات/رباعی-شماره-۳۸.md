---
title: >-
    رباعی شمارهٔ ۳۸
---
# رباعی شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>ای بار خدای پاک دانای قدیر</p></div>
<div class="m2"><p>دارم به تو حاجتی به فضیلت بپذیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که به لطف خویش عزت دادی</p></div>
<div class="m2"><p>تا زنده بود به خواریش باز مگیر</p></div></div>