---
title: >-
    رباعی شمارهٔ ۱۶
---
# رباعی شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>هر ذرهٔ خاک من زبانی دارد</p></div>
<div class="m2"><p>از گردش دهر دوستانی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این کهنه‌ردای من نهان در هر چین</p></div>
<div class="m2"><p>تاج و کله جهان‌سِتانی دارد</p></div></div>