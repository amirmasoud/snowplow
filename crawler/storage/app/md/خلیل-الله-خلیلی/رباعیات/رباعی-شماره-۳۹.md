---
title: >-
    رباعی شمارهٔ ۳۹
---
# رباعی شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>طفلی بودم غنوده بر بستر ناز</p></div>
<div class="m2"><p>برخاست ز دور نغمه های دمساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گوش نهادم نه صدا بود و نه ساز</p></div>
<div class="m2"><p>ای شور جوانی! تو کجا رفتی باز</p></div></div>