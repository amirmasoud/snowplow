---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>گر علت مرگ را دوا می‌کردند</p></div>
<div class="m2"><p>گر چارهٔ این نوع دو پا می‌کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌دیدی کاین جماعت تیره‌نهاد</p></div>
<div class="m2"><p>بر روی زمین چه فتنه‌ها می‌کردند</p></div></div>