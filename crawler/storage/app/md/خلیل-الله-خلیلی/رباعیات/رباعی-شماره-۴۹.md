---
title: >-
    رباعی شمارهٔ ۴۹
---
# رباعی شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>شب است ساقی! ساغرت کو؟</p></div>
<div class="m2"><p>فروغ ماه و نور اخترت کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دور آید صدای مرغ شبگیر</p></div>
<div class="m2"><p>نوا و نغمهٔ جان‌پرورت کو؟</p></div></div>