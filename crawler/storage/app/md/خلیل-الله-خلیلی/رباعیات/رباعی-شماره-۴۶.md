---
title: >-
    رباعی شمارهٔ ۴۶
---
# رباعی شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>این صبح همان و آن شب تار همان</p></div>
<div class="m2"><p>ما شش در و این چهار دیوار همان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>استاد زمانه یک سبق داده به ما</p></div>
<div class="m2"><p>تکرار همان و باز تکرار همان</p></div></div>