---
title: >-
    رباعی شمارهٔ ۴۱
---
# رباعی شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>ای مرغ شباهنگ دل‌انگیز، بنال</p></div>
<div class="m2"><p>قربان تو، ای طایر شب‌خیز، بنال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نالهٔ تو مرغ دلم نالد زار</p></div>
<div class="m2"><p>این ناله به آن ناله درآمیز، بنال</p></div></div>