---
title: >-
    رباعی شمارهٔ ۳۰
---
# رباعی شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>در گلشن زندگی به جز خار نبود</p></div>
<div class="m2"><p>جز درد و غم و محنت و آزار نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید نکرد گل که یاس آمد بار</p></div>
<div class="m2"><p>سرتاسر زندگی جز این کار نبود</p></div></div>