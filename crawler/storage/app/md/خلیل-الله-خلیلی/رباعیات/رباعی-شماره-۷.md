---
title: >-
    رباعی شمارهٔ ۷
---
# رباعی شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>با خلق نکو بِزی که زیور این است</p></div>
<div class="m2"><p>در آینهٔ جمال، جوهر این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن قطرهٔ اشکی که بریزد بر خاک</p></div>
<div class="m2"><p>بردار که گنج لعل و گوهر این است</p></div></div>