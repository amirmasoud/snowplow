---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>این سنگ ملون که گهر می‌نامند</p></div>
<div class="m2"><p>وآن آهن زردگون که زر می‌خوانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌گوهر ارزندهٔ معنی همه را</p></div>
<div class="m2"><p>مردان گهرسنج هدر می‌دانند</p></div></div>