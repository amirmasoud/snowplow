---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>چو گم شد پرتو عشق از دل من</p></div>
<div class="m2"><p>خدایا چیست جز غم حاصل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحاب عشق اگر یکدم نبارد</p></div>
<div class="m2"><p>بسوزان خرمن آب و گل من</p></div></div>