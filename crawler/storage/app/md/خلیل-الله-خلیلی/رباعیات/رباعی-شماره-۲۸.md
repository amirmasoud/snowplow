---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>تا این خرد خام تو، معیار بود</p></div>
<div class="m2"><p>این ساختن و شکستت کار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنها نه سرت به پای من خورد به سنگ</p></div>
<div class="m2"><p>هر جا که روی تو سنگ و دیوار بود</p></div></div>