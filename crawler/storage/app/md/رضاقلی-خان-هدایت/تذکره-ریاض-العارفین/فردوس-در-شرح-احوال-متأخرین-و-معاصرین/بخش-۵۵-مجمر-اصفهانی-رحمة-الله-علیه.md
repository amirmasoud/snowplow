---
title: >-
    بخش ۵۵ - مجمر اصفهانی رَحْمَةُ اللّهِ عَلَیه
---
# بخش ۵۵ - مجمر اصفهانی رَحْمَةُ اللّهِ عَلَیه

<div class="n" id="bn1"><p>وهُوَ زبدة الفصحاء المعاصرین آقا سید حسین. سیدی عزیز القدر و عالمی متشرح الصدر از اهل اصفهان بهشت نشان بود و مراتب علمی را در خدمت علمای معاصرین اکتساب نمود. از آغاز شباب پا در دایرهٔ اهل سخن نهاد وبدین واسطه به دربار خلافت مدار شاهنشاه صاحبقران و دارای معدلت نشان مغفور شتافت و به سبب اجتهاد در فنون شاعری مجتهد الشعرا لقب یافت. به تشریف و منشور سلطانی سرافراز شد. سالها در آن درگاه عرش اشتباه داد سخن داد و قفل بیان از درِ گنجینهٔ زبان گشاد. قصاید فصیحه و غزلیات ملیحه از مخزن خاطر شریف بیرون آورد و مشمول عنایات بی غایات خسروانه گردید و هم در عهد شباب در سنهٔ ۱۲۲۵ به روضهٔ رضوان خرامید. غرض، سیدی عالی گهر و شاعری ستوده سیر. به حسن خلق و حسن صورت محبوب القلوب خواص و عوام بود و در طرز سخن فنی مرغوب تتبع نمود. قصایدش مطبوع اهل آفاق وغزلیاتش نقل مجلس عشاق. پنج هزار بیت دیوان دارد. مثنوی به سیاق تحفة العراقین و املح از آن فرموده است. از اوست:</p></div>
<div class="c"><p>در توحید و تحمید ایزد تعالی گوید</p></div>
<div class="b" id="bn2"><div class="m1"><p>خارج ز هرچه آن بجز او لیک از آن پدید</p></div>
<div class="m2"><p>داخل به هر چه آن بجز او لیک از آن جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که بزم جلوهٔ او هرچه آن صور</p></div>
<div class="m2"><p>آنجا که صوت هستی او هرچه آن صدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که شکر او همه دم عجز را وجود</p></div>
<div class="m2"><p>آنجا که وصف او همه دم نطق را فنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل پرورید و از پی آن درد آفرید</p></div>
<div class="m2"><p>حسن آفرید و از پی آن عشق مرحبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس را چه جای شکوه کز آغاز داده است</p></div>
<div class="m2"><p>زین عشق دردپرور و زان درد بی دوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی طاقتی به عاشق و آسودگی به غیر</p></div>
<div class="m2"><p>فرزانگی به ناصح و دیوانگی به ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس نقطه‌های خال و همه دانهٔ فریب</p></div>
<div class="m2"><p>بس دام‌های زلف و همه حلقهٔ بلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ار خط این نمود و ترا کرد ناشکیب</p></div>
<div class="m2"><p>بر روی آن گشود ترا کرد مبتلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر درگهش امینی سرخیل قدسیان</p></div>
<div class="m2"><p>از حضرتش رسولی سردار انبیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیرایهٔ کرامت و آرایش ادب</p></div>
<div class="m2"><p>شیرازهٔ سعادت و مجموعهٔ حیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم حرف اول از ورق فیض لم یزل</p></div>
<div class="m2"><p>هم نقش آخر از قلم صنع کبریا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دین آشکار کرد به تأیید جبرئیل</p></div>
<div class="m2"><p>شرع استوار کرد به نیروی مرتضی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن قائل سَلُونی و گویای لَوکُشِف</p></div>
<div class="m2"><p>آن خاصهٔ یداللّه و مخصوص اِنّما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من معتقد به قولش و او خوانده خویش را</p></div>
<div class="m2"><p>رزّاق آفرینش و خلاق ماسوا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بندگی چون نکنی ظلم مخوان فرمان را</p></div>
<div class="m2"><p>گوی شو تا که ببینی اثر چوگان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من ندانم که به دستان روم از ره به عبث</p></div>
<div class="m2"><p>آستین بر مزن ای شیخ و مشو دامان را</p></div></div>
<div class="c"><p>مِنْ غزلیّاتِهِ رَحمةُ اللّهِ عَلَیه</p></div>
<div class="b" id="bn18"><div class="m1"><p>نه قابل تکلیفی و نه لایق حشری</p></div>
<div class="m2"><p>نه در غم امروزی و نه در غم فردا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین بانگ جرس راه به جایی نتوان برد</p></div>
<div class="m2"><p>کو خضر رهی تا که شود راهبر ما</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو ره درست روی گو بمان که گم شدگان</p></div>
<div class="m2"><p>چه سود ازین که چنین می‌روند چابک و چست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شکوه‌ام از بخت نافرجام نیست</p></div>
<div class="m2"><p>هر که را عشق است او را کام نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طی نشد این راه و افتادم ز پای</p></div>
<div class="m2"><p>وین عجب کافزون تر از یک گام نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر برآید بانگ بدنامی ز خلق</p></div>
<div class="m2"><p>نیک نام آن کس که او را نام نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر بیاشامند خون او رواست</p></div>
<div class="m2"><p>هر که او در عشق خون آشام نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عالم ترا و ما همه بی خانمان و نیست</p></div>
<div class="m2"><p>غیر از دل خرابی و آن نیز جای تست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر با درون شاه و اگر با دل گدا</p></div>
<div class="m2"><p>در هرچه باز جستم و جویم هوای تست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جز جان نداده‌ایم که گویم برای کیست</p></div>
<div class="m2"><p>کاری نکرده‌ایم که گویم برای تست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر یک ازین همرهان رهبر یکدیگرند</p></div>
<div class="m2"><p>قافلهٔ عشق را قافله سالار نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مقیمان حرم را حلقه بر دست</p></div>
<div class="m2"><p>من اندر حلقهٔ دردی کشان مست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شدم از کعبه در بتخانه کز دوست</p></div>
<div class="m2"><p>پرستش را بتی بر یاد او هست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه در بالا نه در پست است و جمعی</p></div>
<div class="m2"><p>به جستجویش از بالا و از پست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به صحرا مرغ و در دریا مرا دام</p></div>
<div class="m2"><p>به دریا حوت و در صحرا مرا شست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز سرّ عشق خبر نیست پیر کنعان را</p></div>
<div class="m2"><p>که دل نداده به طفلی که غیر فرزند است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زاهد که عیب برهمنان گفت پیر ما</p></div>
<div class="m2"><p>با او مگر چه گفت که با برهمن نگفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جان دادم و رفتم به سلامت ز ره عشق</p></div>
<div class="m2"><p>راهی است ره عشق که هیچش خطری نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از حقیقت هیچ کس آگه نشد</p></div>
<div class="m2"><p>هر کسی حرفی ز جایی می‌زند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ما و آن وادی که از گم گشتگی</p></div>
<div class="m2"><p>هر طرف خضری صدایی می‌زند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تیغ ناپیدا و قاتل ناپدید</p></div>
<div class="m2"><p>کشته در خون دست و پایی می‌زند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا چه پیش آید که در کوی توام</p></div>
<div class="m2"><p>هرکه می‌بیند قفایی می‌زند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خرم آن کشور که سلطانی در او</p></div>
<div class="m2"><p>بوسه بر دست گدایی می‌زند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا دل از دیده فرو ریخت فزون گشت سرشکم</p></div>
<div class="m2"><p>چشمه پیداست که چون پاک شدآبش بفزاید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگرد هم پی درمان هم لیک</p></div>
<div class="m2"><p>چه تدبیر آید از دیوانه‌ای چند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فزاید کاش آن آهی که هر شب</p></div>
<div class="m2"><p>ازو روشن شود کاشانه‌ای چند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نیاساید دلی یارب کزان هست</p></div>
<div class="m2"><p>همه شب یارب اندرخانه‌ای چند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جهان بی دانه صید او چه می‌کرد</p></div>
<div class="m2"><p>اگر در دام بودش دانه‌ای چند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فغان ما ز هشیاریست مجمر</p></div>
<div class="m2"><p>دریغ ازنالهٔ مستانه‌ای چند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>باز از پی خرابی ما از چه می‌رسد</p></div>
<div class="m2"><p>سیلی که صد ره آمد و ما را خراب دید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نه گرفتار بود هر که فغانی دارد</p></div>
<div class="m2"><p>نالهٔ مرغ گرفتار نشانی دارد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>راز عشق آن نبود کش به اشارت گویی</p></div>
<div class="m2"><p>سِرِّ این نکتهٔ سربسته بیانی دارد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شدم انگشت نما در همهٔ شهر مگر</p></div>
<div class="m2"><p>هر که از چشم تو افتاد نشانی دارد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر که بگذشت آفرین برناوک صیادخواند</p></div>
<div class="m2"><p>کس نمی‌پرسد که ما را از چه بسمل کرده‌اند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عاقلی گویند شددیوانهٔ طفلان ولی</p></div>
<div class="m2"><p>گرمن آن دیوانه‌ام دیوانه عاقل کرده‌اند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا چیست ندانم که در این قافله هرکس</p></div>
<div class="m2"><p>از پای درافتد ز همه پیشتر آید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از خاکِ پای دوست مگر آفریده‌اند</p></div>
<div class="m2"><p>کاین عاشقان به دیدهٔ ما جا گزیده‌اند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دامن مگیرشان به ملامت که داده‌اند</p></div>
<div class="m2"><p>از دست دامنی که گریبان دریده‌اند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زاهد کند ملامتشان وه که گمرهی</p></div>
<div class="m2"><p>خندد به آن کسان که به منزل رسیده‌اند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>انکارشان کنند و ندانند کاین گروه</p></div>
<div class="m2"><p>گویند آنچه از لب جانان شنیده‌اند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بنگر بدین که با غم عشقند یار و دوست</p></div>
<div class="m2"><p>بر این مبین که خاک ره و خار دیده‌اند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عشق شد ازراه زهدم سوی رندی رهنمون</p></div>
<div class="m2"><p>تا چه ره بود آنکه جزگم گشته تا منزل نبود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زاهداازتوچه نفرین چه دعاکی بوده است</p></div>
<div class="m2"><p>که ازین طایفه صاحب نفسی برخیزد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عشق را چاره محال است و ندانم که چرا</p></div>
<div class="m2"><p>بیشتر جا به دلِ مردم بیچاره کند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نالم به شام هجر و خوشم زانکه عاشقان</p></div>
<div class="m2"><p>شادند ازین که نالهٔ مرغ سحر بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بی سروپایی ما بین که گدایان ما را</p></div>
<div class="m2"><p>می‌نمایند به مردم که چه بی پا و سرند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نبودی حاصل عقل ار جنون گشت</p></div>
<div class="m2"><p>چرا دیوانه هرجا عاقلی بود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>برآن سرچشمه آخر جان سپردیم</p></div>
<div class="m2"><p>که می‌گفتند جان بخشد زلالش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>خرد بندی است محکم لیک گاهی</p></div>
<div class="m2"><p>توان با ناتوانی‌ها شکستش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همه آتشم چه ترسم که سرِ عذاب داری</p></div>
<div class="m2"><p>همه رحمتی چه پیچم که چرا گناه دارم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ترو خشک عالمی سوخت ز عشق و سادگی بین</p></div>
<div class="m2"><p>که به پیش برق دستی به سر گیاه دارم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>من مست را چه پرسی زخرد که نیست مجمر</p></div>
<div class="m2"><p>خبرم ز سر که گویم خبر از کلاه دارم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پیکان او گذر کند ازسنگ ومن دلی</p></div>
<div class="m2"><p>آورده‌ام که پیش خدنگش سپر کنم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>غمش به ملک جهان خواجه می‌خرد زمن اما</p></div>
<div class="m2"><p>غمی که بندهٔ آنم بگو چگونه فروشم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نفس را دام هوا داده پی صید جهان</p></div>
<div class="m2"><p>شاهبازی به شکار مگسی داشته‌ایم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>جای مالب تشنگان برساحل بحر است و باز</p></div>
<div class="m2"><p>خویشتن را از پی موجِ سراب افکنده‌ایم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ترا کمند ز پرواز ما بلندتر آمد</p></div>
<div class="m2"><p>که باز رشته به دست تو بود هرچه پریدم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>میان شهر به دوشم برند و محتسب از پی</p></div>
<div class="m2"><p>خدای را به که گویم که من نه مست نبیذم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به جرم عاشقی روز جزا در دوزخم بردن</p></div>
<div class="m2"><p>از آن بهتر بود زاهد که در افسردگی مردن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>در که گریزم که ز دستت نهم</p></div>
<div class="m2"><p>روی به هر سو بود آن سوی تو</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بر هر که بنگرم ز تو کامیش حاصل است</p></div>
<div class="m2"><p>آن را که زنده کرده و آن را که کشته‌ای</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>از هیچ دیده نیست که خوابی نبرده‌ای</p></div>
<div class="m2"><p>در هیچ سینه نیست که تابی نهشته‌ای</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دلم جای غم او شد که می‌گفت</p></div>
<div class="m2"><p>نمی‌گنجد محیطی در حبابی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>باتوام لیک از تو بی خبرم</p></div>
<div class="m2"><p>چون در آیینهٔ چشم بی بصری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>باز از همه به حدیث عشق است</p></div>
<div class="m2"><p>صد بار اگر شنیده باشی</p></div></div>
<div class="c"><p>مِنْ مثنویاتِهِ فی صِفَةِ العشقِ والحُسنِ</p></div>
<div class="b" id="bn83"><div class="m1"><p>ای سوز درون سینه ریشان</p></div>
<div class="m2"><p>سوزان ز تو سینه‌های ایشان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دامن زن آتشِ دلِ ریش</p></div>
<div class="m2"><p>آتشکده ساز منزل خویش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ساز از تو به هرکجا که سوزی است</p></div>
<div class="m2"><p>شام از تو به هر کجا که روزی است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>یک آتشی و چو نیک تابی</p></div>
<div class="m2"><p>افتاده به هر تن از تو تابی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>حرفی است مگر میان جمع است</p></div>
<div class="m2"><p>کاتش همه در زبان شمع است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سوزی به حدیث این نهادی</p></div>
<div class="m2"><p>کاتش ز زبان آن گشادی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>صد جان ز من و ز تو شراری</p></div>
<div class="m2"><p>خشم ملکی و خوی یاری</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>خود یاری و یار آتشین خوی</p></div>
<div class="m2"><p>جایت دل و جای دل به پهلوی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>گر پهلوی مات دل نشین است</p></div>
<div class="m2"><p>بنشین که خویت آتشین است</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>من آتشم و تو آتشین خوی</p></div>
<div class="m2"><p>آن به که نشینی‌ام به پهلوی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ای پرده نشین نگار غماز</p></div>
<div class="m2"><p>آن پرده دل و تو اندران راز</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>فاش از تو به هر دلی که رازی است</p></div>
<div class="m2"><p>عجز تو به هر سری که نازی است</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>صد پرده اگر به روی بستی</p></div>
<div class="m2"><p>پیداتر از آن شدی که هستی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>شوخی که به پرده آشکار است</p></div>
<div class="m2"><p>با پرده نشینی‌اش چه کار است</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>در سینهٔ هر که جا گزیدی</p></div>
<div class="m2"><p>در کوی ملامتش کشیدی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بر خاطر هر که برگذشتی</p></div>
<div class="m2"><p>دیوانگی‌ای بر او نوشتی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>آن را که به روی درگشادی</p></div>
<div class="m2"><p>هوشش به برون در نهادی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>آن را که ز آستانه راندی</p></div>
<div class="m2"><p>بیگانهٔ عالمیش خواندی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ما خاک درِ توایم ما را</p></div>
<div class="m2"><p>از خاک درت مران خدا را</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>من خاک و تو مهر تابناکی</p></div>
<div class="m2"><p>گو باش به سایهٔ تو خاکی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>تو شاهی و من گدای درگاه</p></div>
<div class="m2"><p>گاهی به گدا نظر کند شاه</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>تو شاهی و ما ترا گداییم</p></div>
<div class="m2"><p>رحمی رحمی که بینواییم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>تو شاهی و غم بر آستانت</p></div>
<div class="m2"><p>یعنی که فغان ز پاسبانت</p></div></div>
<div class="c"><p>و له ایضاً درخطاب به عشق</p></div>
<div class="b" id="bn106"><div class="m1"><p>ای خسرو تخت گاه جان‌ها</p></div>
<div class="m2"><p>فرماندهٔ کشور روان‌ها</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>در هم شکن سپاه هستی</p></div>
<div class="m2"><p>ویران کن ملک خودپرستی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>انگیخته رخش ناشکیبی</p></div>
<div class="m2"><p>افراشته چتر بی نصیبی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>شمشیر اجل کشیده از تو</p></div>
<div class="m2"><p>پیوندِ امل بریده از تو</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>غارتگر ملک عقل و دینی</p></div>
<div class="m2"><p>گر عشق نه‌ای چرا چنینی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>آنجا که زنند بارگاهت</p></div>
<div class="m2"><p>عجز است مقیم پیشگاهت</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>هر گه ره کارزار گیری</p></div>
<div class="m2"><p>صد ملک به یک سوار گیری</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>آن ملک ولی خراب گشته</p></div>
<div class="m2"><p>خاکش به غم و بلا سرشته</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>زان ملک خراب تاج خواهی</p></div>
<div class="m2"><p>چند از دل ما خراج خواهی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>در حکم تو هر ستیزه جویی</p></div>
<div class="m2"><p>جز حسن که زیر حکم اویی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>با آن در آشتیت باز است</p></div>
<div class="m2"><p>او را ناز و ترا نیاز است</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>آن نیز ترا نیازمند است</p></div>
<div class="m2"><p>این ناز و نیاز تابه چند است</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>آن قوم که محرمان رازند</p></div>
<div class="m2"><p>آگاه ازین نیاز و نازند</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>آنان که مقیم پیشگاهند</p></div>
<div class="m2"><p>آگاه زسر پادشاهند</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>من خود ز برون دل از درونست</p></div>
<div class="m2"><p>دانیم که کار هر دو چون است</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>رحم آر اگر شکایتی رفت</p></div>
<div class="m2"><p>بخشای اگر جنایتی رفت</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>مسکینم و از تو این نوایی است</p></div>
<div class="m2"><p>رنجورم و از تو این شفایی است</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>می‌میرم و از تو این حیاتی است</p></div>
<div class="m2"><p>می‌لغزم و از تو این ثباتی است</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>هریأس که از تو آن مرادی است</p></div>
<div class="m2"><p>هربند که از تو آن گشادی است</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>هر نقص که از تو آن کمالی است</p></div>
<div class="m2"><p>هر درد که از تو آن زلالی است</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>می‌سوزم و بر لب از تو آبم</p></div>
<div class="m2"><p>می‌نوشم و زان به سینه تابم</p></div></div>
<div class="c"><p>ایضاً مخاطبهٔ دیگر به عشق</p></div>
<div class="b" id="bn127"><div class="m1"><p>ای چشمهٔ زندگی که مردند</p></div>
<div class="m2"><p>آن تشنه لبان که از تو خوردند</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>مردند ولیک جاودانی</p></div>
<div class="m2"><p>از تو همه راست زندگانی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>آبی به سبوی و زهر در جام</p></div>
<div class="m2"><p>نیشی به درون و نوش در کام</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>از آب که دیده زهر ریزد</p></div>
<div class="m2"><p>از نوش که دیده نیش خیزد</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>هر نخل که از تو بارور شد</p></div>
<div class="m2"><p>هجرش برگ و غمش ثمر شد</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>هر کشته که یافتی نم از تو</p></div>
<div class="m2"><p>شد سوخته خرمن آن دم از تو</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بر هر گیهی که نشو دادی</p></div>
<div class="m2"><p>برقی شدی و در آن فتادی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>کس آب ندیده آتش انگیز</p></div>
<div class="m2"><p>آبی سوزان و آتشی تیز</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>من آن گیهم که از تو رستم</p></div>
<div class="m2"><p>آب خود و ز آتش تو جستم</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>زان برق که سوختی جهانی</p></div>
<div class="m2"><p>مگذار ازین گیه نشانی</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>حیف است که باده دُرد آمیز</p></div>
<div class="m2"><p>خاصه اگر آن بود طرب خیز</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>از خاک چه کم شود غباری</p></div>
<div class="m2"><p>برخیزد اگر ز رهگذاری</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>گر زانکه تبه شود حبابی</p></div>
<div class="m2"><p>در بحر نیارد اضطرابی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>هرگز نرسد زیان به باغی</p></div>
<div class="m2"><p>کز ساحت آن پرید زاغی</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>با هستی تو وجود من چیست</p></div>
<div class="m2"><p>آنجا که فرشته، اهرمن کیست</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>خورشید چو در میان جمع است</p></div>
<div class="m2"><p>حاجت نه به روشنی شمع است</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>از کار من این جهان بپرداز</p></div>
<div class="m2"><p>کارم به جهان دیگر انداز</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>رویم سوی وادی جنون کن</p></div>
<div class="m2"><p>مجنونم ازین جهان برون کن</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>چون راه سوی دیار لیلی است</p></div>
<div class="m2"><p>مجنون شدنم در این ره اولیست</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>بیگانه کن آن چنان ز عقلم</p></div>
<div class="m2"><p>کاشفته شود جهان ز نقلم</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>آن به که ز عقل دور باشم</p></div>
<div class="m2"><p>در غیبت ازین حضور باشم</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>این عقل که رهبر جهان است</p></div>
<div class="m2"><p>خضر ره و دزد کاروان است</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>رهبر شودت که عاشقی به</p></div>
<div class="m2"><p>پس ره زندت که عاشقی چه</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>لیک آن نه من آن دگر کسانند</p></div>
<div class="m2"><p>کز همرهی‌اش ز واپسانند</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>زین رهبر رهزنم جدا کن</p></div>
<div class="m2"><p>در بادیه گمرهم رها کن</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>باشد که یکی ز ره درآید</p></div>
<div class="m2"><p>این گمشده را رهی نماید</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>بر درگه دوست راه جویم</p></div>
<div class="m2"><p>از هرچه جز او پناه جویم</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چون حلقه نتابم از درش سر</p></div>
<div class="m2"><p>نالم ز برون چو حلقه بر در</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>گویم سخنی به یار دارم</p></div>
<div class="m2"><p>باگل سخنی ز خار دارم</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>تا کی غم خود به محرم راز</p></div>
<div class="m2"><p>ناگفته همان که گویدت باز</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>آهسته که غیر در کنار است</p></div>
<div class="m2"><p>خاموش که خصم پرده دار است</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>تا چند به هر خرابه مجمر</p></div>
<div class="m2"><p>سر بر سر خاک و خاک بر سر</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>تا کی غم خود ز دل نهفتن</p></div>
<div class="m2"><p>تا کی غم خود به دل نگفتن</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>گویم غم خویش لیک با یار</p></div>
<div class="m2"><p>نه غیر و نه پاسبان زهی کار</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>افسانهٔ خود به دوست گویم</p></div>
<div class="m2"><p>با مغز حدیث پوست گویم</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>نی نی غلطم چه مغز و چه پوست</p></div>
<div class="m2"><p>این هر دو یکی و آن یکی اوست</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>تا چند حدیث موج و دریا</p></div>
<div class="m2"><p>تا چند دلیل مور و بیضا</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>از هستی این و آن چه گویی</p></div>
<div class="m2"><p>هیچی پی هیچ از چه پویی</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>جز او همه نیستند ور نیست</p></div>
<div class="m2"><p>قایم به وجود خود جز او کیست</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>ممتاز نه ذاتش از صفاتش</p></div>
<div class="m2"><p>بیرون ز دویی صفات و ذاتش</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>پنهان به حجاب نور خویش است</p></div>
<div class="m2"><p>اندر تتق ظهور خویش است</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>هستیش به روی پرده بسته</p></div>
<div class="m2"><p>در پردهٔ هستی‌اش نشسته</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>تا ذره همه خداش دانند</p></div>
<div class="m2"><p>تا قطره همه خداش خوانند</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>گر سنگ بود به گفتگویش</p></div>
<div class="m2"><p>در خاک بود به جستجویش</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>دریا ز نهیب اوست درموج</p></div>
<div class="m2"><p>گردون به هوای اوست در اوج</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>بیم از همه و ازو امید است</p></div>
<div class="m2"><p>قفل از همه و ازو کلید است</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>از چشمهٔ او حیات جویی</p></div>
<div class="m2"><p>وز گلشن او بهشت بویی</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>هر جا که خطی، نوشتهٔ اوست</p></div>
<div class="m2"><p>هر جا که گلی سرشتهٔ اوست</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>می‌خوان و مگو که بد نوشتند</p></div>
<div class="m2"><p>می‌بین و مگو که بد سرشتند</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>کز خامهٔ قدرت او نبشتش</p></div>
<div class="m2"><p>وز پنجهٔ حکمت او سرشتش</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>چون خامه چنان ازو چه خیزد</p></div>
<div class="m2"><p>چون پنجه چنین ازو چه ریزد</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>خیزد که بجز تو نیست معبود</p></div>
<div class="m2"><p>ریزد که بجز تو نیست موجود</p></div></div>
<div class="c"><p>رباعی</p></div>
<div class="b" id="bn179"><div class="m1"><p>یارب به سبوکشان مستم بخشا</p></div>
<div class="m2"><p>بر مغبچگان می‌پرستم بخشا</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>بر این منگر که باده در دست من است</p></div>
<div class="m2"><p>بر آنکه دهد به دستم بخشا</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>ای دل همه را نالهٔ جانکاهی هست</p></div>
<div class="m2"><p>از ضعف اگر نیست گهی گاهی هست</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>تا چند نشسته‌ای بدان در خاموش</p></div>
<div class="m2"><p>گر ناله نمی‌توان کشید آهی هست</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>در عشق بتان چاره بجز مردن نیست</p></div>
<div class="m2"><p>بی مهر بتان نیز نمی‌شاید زیست</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>ای وای بر آن دل که در آن سوزی هست</p></div>
<div class="m2"><p>ای خاک بر آن سر که در آن شوری نیست</p></div></div>