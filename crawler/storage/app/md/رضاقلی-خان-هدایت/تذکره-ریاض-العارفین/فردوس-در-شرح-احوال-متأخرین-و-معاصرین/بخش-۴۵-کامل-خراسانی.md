---
title: >-
    بخش ۴۵ - کامل خراسانی
---
# بخش ۴۵ - کامل خراسانی

<div class="n" id="bn1"><p>اسم شریفش ملامحمد اسماعیل و اصلش از قریهٔ ارغد بوده و سال‌ها تحصیل کمالات نموده، به صحبت اکابر دین مبین و ناهجان مناهج یقین رسیده و طریقهٔ مشایخ سلسلهٔ علیّهٔ ذهبیّهٔ کبرویه را برگزید و در کمالات نفسانی و روحانی مرتبهٔ عالی یافته و ارادت به جناب سید عالم شاه هندی که از فحول فضلا و علما و عرفای عهد بوده داشته است و به صحبت جناب سید قطب الدین نیریزی فارسی و آقا محمد هاشم خلیفهٔ او و آقا محمد کازرونی نیز رسیده به مرتبهٔ کمال ترقی نموده۸ و در نظم وجدی تخلص می‌فرموده و در سنهٔ]۱۲۳۲[رایت سفر آخرت برافراشته. گاهی به طریق مثنوی طبع آزمایی می‌فرموده. مثنوی مختصری از او دیده شد و این چند بیت از آن گزیده شده:</p></div>
<div class="c"><p>منتخب مثنوی اوست</p></div>
<div class="b" id="bn2"><div class="m1"><p>آتش عشق از درون شد شعله‌ور</p></div>
<div class="m2"><p>پرتوش اندیشه‌ها بر گِردِ سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر نفس از پردهٔ ساز دگر</p></div>
<div class="m2"><p>گونه گونه آرد آوازِدگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌دهد آواز یعنی که منم</p></div>
<div class="m2"><p>که نهان گردیده در نای تنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پردهٔ نی چون که دورافکن شود</p></div>
<div class="m2"><p>جمله عالم پر زما و من شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عجب لطفش به آید یا که قهر</p></div>
<div class="m2"><p>شهد او فایق‌تر آید یا که زهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر و کین در دیدهٔ بیگانه است</p></div>
<div class="m2"><p>آشنا را چشم بر جانانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شکست تو مرا آرام و کام</p></div>
<div class="m2"><p>وی درستی‌ها شکستت را غلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کنی ابرو ترش با من به کین</p></div>
<div class="m2"><p>غوطه ور گردم به بحر انگبین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق آمد بر وجودم چیر شد</p></div>
<div class="m2"><p>دست از کار و دل از جان سیر شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جملهٔ عالم نمودار حقند</p></div>
<div class="m2"><p>آینه صافی و دیدار حقند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم بگشا تا ببینی آشکار</p></div>
<div class="m2"><p>جلوه گردر پردهٔ اغیار یار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنچه را گویند نامش کیمیا</p></div>
<div class="m2"><p>نیست الا صحبت مرد خدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مهر ایشان جنت و انوار دان</p></div>
<div class="m2"><p>قهر ایشان را جحیم و نار دان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای تو غره گشته اندر نیستی</p></div>
<div class="m2"><p>هیچ بندیشی که آخر کیستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کارساز ما به فکر کار ما</p></div>
<div class="m2"><p>فکر ما و کار ما آزار ما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رنگ کان از رنگ باشد در جهان</p></div>
<div class="m2"><p>دل منه بر وی که می‌گردد جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صبغة اللّه رنگ بی رنگی بود</p></div>
<div class="m2"><p>شو به رنگ او که مانی تا ابد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رنگ کو ار حق نشانت می‌دهد</p></div>
<div class="m2"><p>رنگ بی رنگست جانت می‌دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بندهٔ حق شو که آزادی کنی</p></div>
<div class="m2"><p>با غمش درساز تا شادی کنی</p></div></div>
<div class="c"><p>رباعیّات</p></div>
<div class="b" id="bn21"><div class="m1"><p>ای آنکه به دلبری تویی بالادست</p></div>
<div class="m2"><p>عمری است که گشته‌ام به دامت پابست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پایی به سرم نه که فتادم از پا</p></div>
<div class="m2"><p>دستی به دلم رسان که رفتم از دست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا بتوانی به جان بکش بار دلی</p></div>
<div class="m2"><p>می‌کوش که تا دلت شود یار دلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آزار دلی مکن که ناگاه کنی</p></div>
<div class="m2"><p>کاردو جهان در سر آزار دلی</p></div></div>