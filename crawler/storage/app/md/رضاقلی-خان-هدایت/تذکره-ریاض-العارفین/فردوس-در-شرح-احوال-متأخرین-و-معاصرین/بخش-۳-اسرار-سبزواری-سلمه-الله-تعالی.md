---
title: >-
    بخش ۳ - اسرار سبزواری سَلَّمَهُ اللّهُ تَعالَی
---
# بخش ۳ - اسرار سبزواری سَلَّمَهُ اللّهُ تَعالَی

<div class="n" id="bn1"><p>و هُوَ فخرالمحققین و قدوة المتکلمین الحاج میرزا هادی حَفَظَهُ اللّه تعالی ابن الحاج ملامهدی السّبزواری. والد ماجد آن جناب از علمای عهد و صاحب مکنت بوده. به مکّهٔ معظمه رفته(ره). در مراجعت از راه دریا به شیراز رحلت یافته. جناب مولانا تا عشرهٔ کامله از عمر خود در سبزوار می‌زیسته. به اصرار جناب عالم عابد ملاحسین سبزواری که با والدش رفیق بوده به مشهد مقدس رضوی رفته به تحصیل کوشید. بعد از ریاضات شرعیه و تکمیل فقه و اصول و کلام و حکمت به شوق اقتباس حکمت اشراق به خدمت حکمای اصفهان رفته، هشت سال در نزد مولانا اسماعیل اصفهانی و ملاعلی النوری حکمت دیدند. بعد از مراجعت به خراسان به زیارت مکه رفته به سبزوار برگشتند. تا این ایام که هزار و دویست و هفتاد و هشت است بیست و هشت سال است که در آنجا به تألیف و تصنیف و تدریس و تحقیق علوم الهیه مشغول و از عمر شریفش شصت و سه سال رفته. شرح منظومه در حکمت و نبراس محفل التفقیه از طهارت تا انتهای حج با متن منظوم و شرح معلوم، همچنین شرح جوشن کبیر و دعای صباح و منظومه در منطق به قدر سیصد بیت موقوم فرمودند. حواشی بسیار خاصه بر کتب صدرالدین شیرازی و غیره نگاشته. رسالهٔ هدایت الطالبین وغزلیات نیز مرقوم فرمودند. صاحب کرامات و مقامات عالیه می‌باشد. تیمّناً به بعضی از غزلیات آن جناب می‌پردازم:</p></div>
<div class="c"><p>مِنْغزلیّات</p></div>
<div class="b" id="bn2"><div class="m1"><p>ایزد بسرشت چون گل ما</p></div>
<div class="m2"><p>مهر تو نهفت در دل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دیده ز بس که خون فشاندی</p></div>
<div class="m2"><p>در خون دل است منزل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خویشتن بدید عیان شاهد الست</p></div>
<div class="m2"><p>هر کو درید پردهٔ پندار خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا پر فشانیی نکند وقت قتل هم</p></div>
<div class="m2"><p>بربست بال مرغ گرفتار خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای امیر کاروان کاندیشهٔ ما نبودت</p></div>
<div class="m2"><p>یک نظرهم می‌رسد افتاده در دنبال را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اختران پرتو مرآت دل انور ما</p></div>
<div class="m2"><p>دل ما مظهر کلّ کلّ همگی مظهر ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو ملک طریقت به حقیقت ماییم</p></div>
<div class="m2"><p>کله از فقر به تارک ز فنا افسر ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهدان در پرده مستورند لیک</p></div>
<div class="m2"><p>ماه ما بی پرده باشد در نقاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیدم اندر بزم می‌خواران شدی</p></div>
<div class="m2"><p>هم تو ساقی هم تو ساغر هم شراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر دل رندان شکستی زاهدا آسان مگیر</p></div>
<div class="m2"><p>جای حق باشد حذر فرما شکستن مشکل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>موسئی نیست که دعوی اناالحق شنود</p></div>
<div class="m2"><p>ورنه این داعیه اندر شجری نیست که نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتش آن نیست که در وادی ایمن زده‌اند</p></div>
<div class="m2"><p>آتش آنست که اندر دل درویشان است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا کی ز غمت ناله و فریاد توان کرد</p></div>
<div class="m2"><p>ز افتاده به کنج قفسی یاد توان کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آغوش و کنار از تو نداریم توقع</p></div>
<div class="m2"><p>از نیم نگاهی دل ما شاد توان کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از ملک ازل سوی ابد رخت کشیدیم</p></div>
<div class="m2"><p>آری چه کنم قسمت ما در به دری بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شهری پر از آیینهٔ الوان نگریدیم</p></div>
<div class="m2"><p>اسرار به هر آینه در جلوه گری بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پارسایان ریایی ز هوا بنشینند</p></div>
<div class="m2"><p>گر به خاک درِ میخانه چو ما بنشینند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بت پیمان شکن عهد گسل یادت باد</p></div>
<div class="m2"><p>که به دل بست سر زلف توپیمانی چند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه جوید حرمش گو به سر کوی دل آی</p></div>
<div class="m2"><p>نیست حاجت که کنی قطع بیابانی چند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیستم در خور لطفت طمع از حد نبرم</p></div>
<div class="m2"><p>دو سه دشنام به پاداش دعا ما را بس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر چه آن معبر هستی است بود معدن حسن</p></div>
<div class="m2"><p>هرچه آن مظهر حسنی است بود مصدر عشق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تاج اسرار علی قطب مدار عشق است</p></div>
<div class="m2"><p>او بود دایره و مرکز او محور عشق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نشد افسرده ز آب هفت دریا</p></div>
<div class="m2"><p>چو آتش بود اندر مجمر دل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز اسرار خویش آگهی اسرار را دهم</p></div>
<div class="m2"><p>چون با خود آیم و سفر از خویشتن کنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنگ در دامن دلدار زدم دوش به خواب</p></div>
<div class="m2"><p>بود دستم به دل خویش که بیدار شدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر خم زلف که بر گونهٔ گلگونی بود</p></div>
<div class="m2"><p>دام صیاد ازل بود و گرفتار شدم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای که با نور خرد نور خدامی‌جویی</p></div>
<div class="m2"><p>خویش بین، عکس نظر کن بکجا می‌پویی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پادشاهی دُرّ ثمینی داشت</p></div>
<div class="m2"><p>بهر انگشتری نگینی داشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خواست نقشی که باشدش دوثمر</p></div>
<div class="m2"><p>هر نفس کافکند به نقش نظر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گاه شادی نگیردش غفلت</p></div>
<div class="m2"><p>گاه انده نباشدش محنت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرچه فرزانه بود در ایام</p></div>
<div class="m2"><p>کرد اندیشه و ولی همه خام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ژنده پوشی پدید شد آن دم</p></div>
<div class="m2"><p>گفت بنگار بگذرد این هم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شاه را این سخن فتاد پسند</p></div>
<div class="m2"><p>برنگینش همین عبارت کند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زانکه شادی و عیش و محنت و غم</p></div>
<div class="m2"><p>بگذرد هر دو بر بنی آدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جز نور علی نیست اگر درک بود</p></div>
<div class="m2"><p>با غیر علی کی ام سر برگ بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گویند دم مرگ علی را بینیم</p></div>
<div class="m2"><p>ای کاش که هر دمم دم مرگ بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای ذات تو ز اعراض صفات آمده پاک</p></div>
<div class="m2"><p>کوتاه ز دامن تو دست ادراک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در هر چه نظر کنم توآیی به نظر</p></div>
<div class="m2"><p>لا ظاهِرَ فی الوجودِ واللّهِ سِواک</p></div></div>