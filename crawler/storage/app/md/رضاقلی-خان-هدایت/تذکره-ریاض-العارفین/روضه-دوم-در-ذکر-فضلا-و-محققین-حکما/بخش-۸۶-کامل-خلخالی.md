---
title: >-
    بخش ۸۶ - کامل خلخالی
---
# بخش ۸۶ - کامل خلخالی

<div class="n" id="bn1"><p>اسم سعیدش ملک سعید. از فضلای خلخال و از شعرای صاحب حال، علوم عقلی و نقلی را با تصوف جمع کرده. از قال به حال رسیده و شراب معرفت چشیده. مدتها در شیراز از خلق انزوا گزیده و به ذکر حق آرمیده. اوقات را صرف کتب عرفانیه و حکمیّه می‌نمود و در همانا رحلت فرموده. این دو بیت و رباعیات از او نوشته شد:</p></div>
<div class="b" id="bn2"><div class="m1"><p>کرّوبیان چو نالهٔ من گوش می‌کنند</p></div>
<div class="m2"><p>تسبیح ذکر خویش فراموش می‌کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کامل زبان ببند که خاصان بزم خاص</p></div>
<div class="m2"><p>عرض مراد از لب خاموش می‌کنند</p></div></div>
<div class="c"><p>رباعیات</p></div>
<div class="b" id="bn4"><div class="m1"><p>ای آینهٔ ذات تو ذات همه کس</p></div>
<div class="m2"><p>مرآت صفات تو صفاتِ همه کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ضامن شدم ازبهر نجات همه کس</p></div>
<div class="m2"><p>بر من بنویس سیئات همه کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنهار ای دل هزار زنهار ای دل</p></div>
<div class="m2"><p>پندی دهمت نیک نگهدار ای دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فردا که کند رحمت او جلوه‌گری</p></div>
<div class="m2"><p>خود را برسان به خیل کفار ای دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر من نه ز شرع تو حیا می‌کردم</p></div>
<div class="m2"><p>خود می‌دانی که تا چه‌ها می‌کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر قدرت من به قدر عفوت می‌بود</p></div>
<div class="m2"><p>مقدار عطایِ تو خطا می‌کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما طیّ بساط ملک هستی کردیم</p></div>
<div class="m2"><p>بی نقص خودی خداپرستی کردیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر ما می وصل نیک می‌پیمودند</p></div>
<div class="m2"><p>تف بر رخ ما که زود مستی کردیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ننوشت برای ورد روز وشب من</p></div>
<div class="m2"><p>جز ذکر علی معلّم مکتب من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر غیر علی کسی بود مطلب من</p></div>
<div class="m2"><p>ای وای من و کیش من و مذهب من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا رب به دلم راز نهانی گفتی</p></div>
<div class="m2"><p>اسرار بقای جاودانی گفتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با هستی خود مگر لقایت طلبید</p></div>
<div class="m2"><p>موسی که جواب لن ترانی گفتی</p></div></div>