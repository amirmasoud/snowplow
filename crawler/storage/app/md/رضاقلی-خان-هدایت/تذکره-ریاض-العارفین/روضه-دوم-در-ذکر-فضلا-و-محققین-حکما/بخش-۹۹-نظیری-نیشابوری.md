---
title: >-
    بخش ۹۹ - نظیری نیشابوری
---
# بخش ۹۹ - نظیری نیشابوری

<div class="n" id="bn1"><p>اسمش محمد حسین. اصلش از جوین. به سبب توطن در نشابور از اهل نشابور مشهور. به هندوستان رفته و بادرویشان انس گرفته. در زمان اکبرشاه در گجرات وفات یافت. دیوانش دیده شد و چند بیتی از آن جناب گزیده شد:</p></div>
<div class="c"><p>غزلیّات</p></div>
<div class="b" id="bn2"><div class="m1"><p>بگو منصور از زندان اناالحق گو برون آید</p></div>
<div class="m2"><p>که دین عشق ظاهرگشت و باطل کردمذهب‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اندک التفاتی زنده دارد آفرینش را</p></div>
<div class="m2"><p>اگرنازی کند در دم فرو ریزند قالب‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهود بت ز پراکندگیم باز آورد</p></div>
<div class="m2"><p>دلیل راه حقیقت برهمنی است مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر چهرهٔ حقیقت اگر مانده پرده‌ای</p></div>
<div class="m2"><p>جرم نگاه دیدهٔ صورت پرست ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا یک دلت قبول کند قرب حق مجو</p></div>
<div class="m2"><p>سرمایهٔ قبول در انکار عالمی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ کس نامهٔ سر بستهٔ ما فهم نکرد</p></div>
<div class="m2"><p>نه همین خاتمه‌اش نیست که عنوانش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرو سامان سخن گفتن این جمعم نیست</p></div>
<div class="m2"><p>پهلوی من بنشانید پریشانی چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کسی از تو نشانی به گمان می‌گوید</p></div>
<div class="m2"><p>کس ندیدم که در بزم تو محرم باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیازارم ز خود هرگز دلی را</p></div>
<div class="m2"><p>که می‌ترسم در آن جای تو باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را به صد افسانه در خواب چو می‌کردی</p></div>
<div class="m2"><p>از بهرچه می‌کردی بیدار ز خواب اول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نظر گردد حجاب آنجا که من دیدار می‌بینم</p></div>
<div class="m2"><p>نهان از چشم ظاهربین تماشای دگر دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قومی ترا زخلوت و عزلت طلب کنند</p></div>
<div class="m2"><p>تو شور شهر و فتنهٔ بازار بوده‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرسش چه می‌کنی ز خطا و ثواب ما</p></div>
<div class="m2"><p>چون هرچه کرده‌ایم خبردار بوده‌ای</p></div></div>
<div class="c"><p>رباعی</p></div>
<div class="b" id="bn15"><div class="m1"><p>رو جانب حق از همه سو باید بود</p></div>
<div class="m2"><p>در کوشش نفی مو به مو باید بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از بهر ظهور تو نهان کرده ترا</p></div>
<div class="m2"><p>در سرّ خود و ظهور او باید بود</p></div></div>