---
title: >-
    بخش ۵۴ - شفایی اصفهانی
---
# بخش ۵۴ - شفایی اصفهانی

<div class="n" id="bn1"><p>نامش حکیم شرف الدین حسن و افضل فضلای زمن بوده. میرداماد او را تمجید نمود و جامع کمالات صوری و معنوی و حاوی حکمت علمی وعملی. از عالم توحید و تجرید بهره برداشته و در طریقهٔ شعر و شاعری لوای شهرت افراشته. قصاید و غزلیات دلکش به رشتهٔ نظم کشیده و بادهٔ معرفت چشیده. مثنویات متعدده دارد و از جمله مثنوی به بحر حدیقه موسوم به نمکدان حقیقت که الحق کمال فصاحت و بلاغت حکیم از آن ظاهر است و از غایت لطف بعضی آن را از حکیم سنائی دانسته‌اند و نسخهٔ آن متداول است و غالب خلق از سنائی دانند. لیکن آنچه بر فقیر از کتب تذکره، خاصه تذکرهٔ علیقلی خان لکزی معلوم شده از حکیم شفایی(ره) است. به هر صورت چون نهایت ملاحت دارد اغلبی از آن نوشته شد:</p></div>
<div class="b" id="bn2"><div class="m1"><p>نظر به جانب او بی نظر توان کردن</p></div>
<div class="m2"><p>حجاب چهرهٔ عشّاق عین بینایی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببین و هیچ مبین و بدان و هیچ مدان</p></div>
<div class="m2"><p>که خاکپایِ ادب کیمیای دانایی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ردّ و قبول دگرانش چه تفاوت</p></div>
<div class="m2"><p>آن بنده که در چشم خریدار درآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شیخ که از خانه به بازار نمی‌رفت</p></div>
<div class="m2"><p>مست است به حدی که رهِ خانه نداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرستاری ندارم بر سرِبالینِ بیماری</p></div>
<div class="m2"><p>مگر آهم ازین پهلو به آن پهلو بگرداند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هرکس می‌رسد عاشق دل دیوانه می‌جوید</p></div>
<div class="m2"><p>دلش را آشنا برده است و ازبیگانه می‌جوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم عالم پریشانم نمی‌کرد</p></div>
<div class="m2"><p>سرِ زلف پریشان آفریدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌ترسید از دوزخ شفایی</p></div>
<div class="m2"><p>غم جان سوزِ هجران آفریدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ناامیدی از آن خوش دلم که چرخ نیافت</p></div>
<div class="m2"><p>بهانه‌ای که توان از من انتقام کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردیم و حرف یاری ما در جهان بماند</p></div>
<div class="m2"><p>رفتیم در کنار و سخن در میان بماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این کعبه و آن مسجد آدینه طلب کرد</p></div>
<div class="m2"><p>ره سویِ تو آن برد که در سینه طلب کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌راندم از ناز چو مرغی که به بازی</p></div>
<div class="m2"><p>پایش بگشایند پریدن نگذارند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غیرت نه همین لازم عشقست که لیلی</p></div>
<div class="m2"><p>از رشک نخواهد که به مجنون نگرد کس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شغل عاشقی غم‌های عالم رفت از یادم</p></div>
<div class="m2"><p>چه می‌کردم اگر کاری چنین پیدا نمی‌کردم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان درِ توفیق نگشایند بر رویت که تو</p></div>
<div class="m2"><p>از همه کاری چو درمانی توکل می‌کنی</p></div></div>
<div class="c"><p>مِنَ المَثْنَوِیِّ المَوسُومِ به نمکدان حقیقت</p></div>
<div class="b" id="bn17"><div class="m1"><p>نَحْمِدُ اللّهَ عَنْلِسانِ الْعِشْقِ</p></div>
<div class="m2"><p>ثُمَّ نَشْکُرُهُ عَنْجَنانِ الْعِشْقِ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اَبَداً لایقاً بالایِهِ</p></div>
<div class="m2"><p>کامِلاً شامِلاً لِنَعْمائهِ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ثنایی سزای او باشد</p></div>
<div class="m2"><p>از لبِ کبریایِ او باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فکرت جان و دل چه اندیشد</p></div>
<div class="m2"><p>خاطرِ آب و گل چه اندیشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در ثنایش که کارِ امکان نیست</p></div>
<div class="m2"><p>در هوایش که حد عرفان نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقل عاجز شود که لا اُحصی</p></div>
<div class="m2"><p>نطق اَبْکَم شود که لاأَدْرِی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باطن و ظاهر اول و آخر</p></div>
<div class="m2"><p>همه جا غایب از همه حاضر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اولی نه که سابقش قدم است</p></div>
<div class="m2"><p>وآخری نه که لاحقش عدم است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این سخن خود سزای او نبود</p></div>
<div class="m2"><p>جای اینگونه گفتگو نبود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر وی اطلاق و چند و چون ستم است</p></div>
<div class="m2"><p>جَلْشأنه بری ز کیف و کم است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کفر و دین جلوه گاه وحدتِ او</p></div>
<div class="m2"><p>لا و اِلّا گواهِ وحدت او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کفر و دین خاکروب این راه‌اند</p></div>
<div class="m2"><p>تشنه بی دلو بر سر چاه‌اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کفر غافل که در عبارتِ اوست</p></div>
<div class="m2"><p>بی خبر لا که هست طاعتِ اوست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>می‌کند بر یگانگیش ندی</p></div>
<div class="m2"><p>وصف لم یولدی و لم یلدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر برهمن وگر خداخوان است</p></div>
<div class="m2"><p>روش زی بارگاه سلطان است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای حجاب رخت نقابِ ظهور</p></div>
<div class="m2"><p>پردهٔ هستی‌ات تجلی نور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ما و هل را به حضرتت ره نه</p></div>
<div class="m2"><p>هیچ کس از تو جز تو آگه نه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قدم از خویش چون نهادی پیش</p></div>
<div class="m2"><p>جلوه کردی به پیش دیدهٔ خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای تو در جلوه‌گاهِ یکتایی</p></div>
<div class="m2"><p>هم تماشا و هم تماشایی</p></div></div>
<div class="c"><p>فِی المناجات</p></div>
<div class="b" id="bn36"><div class="m1"><p>در رهت عقل پیش پای ندید</p></div>
<div class="m2"><p>قدمی چند رفت و برگردید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون اصولی ز دور کرد نگاه</p></div>
<div class="m2"><p>ورقی چند دید کرد سیاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عشق چون مشعل یقین افروخت</p></div>
<div class="m2"><p>اوّلش دفتر خیال بسوخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عقلِ اوّل چو طفلِ چوب به مشت</p></div>
<div class="m2"><p>بر سر حرف اولش انگشت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هرکه را سر به جیب عرفان است</p></div>
<div class="m2"><p>از تو بر تو هزار برهان است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>معرفت کی ز قال می زاید</p></div>
<div class="m2"><p>رهبر کور کور کی شاید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حبس در دام احتمالِ همه</p></div>
<div class="m2"><p>موم در دست قیل و قال همه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برگ این راه را ز اهل کمال</p></div>
<div class="m2"><p>دیده بستان نه پای استدلال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به خیالش رسید نتوانی</p></div>
<div class="m2"><p>قدم دل مگر بجنبانی</p></div></div>
<div class="c"><p>در بیان تقاضای اسماء و صفات به ظهور ذات</p></div>
<div class="b" id="bn45"><div class="m1"><p>مبدء اصل و فرع جل جلال</p></div>
<div class="m2"><p>همدم خویش بود در آزال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خویشتن را به خویشتن می‌دید</p></div>
<div class="m2"><p>عشق با روی خویش می‌ورزید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هیچ در سر هوایِ سیر نداشت</p></div>
<div class="m2"><p>احتیاج ظهور غیر نداشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بس که مغرور بود و بی پروا</p></div>
<div class="m2"><p>از دو عالمش بود استغنا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جوش زد چون کمالِ اسمائی</p></div>
<div class="m2"><p>حسن شد طالب تماشایی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شوق نگذاشت حسن را مستور</p></div>
<div class="m2"><p>جلوه گر شد به جلوه گاه ظهور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون صفات مقابل باری</p></div>
<div class="m2"><p>متقاضی شوند در کاری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آنچه اکمل بود ز پیش برد</p></div>
<div class="m2"><p>از میان مدعای خویش برد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز آتش آن مایهٔ صفات کمال</p></div>
<div class="m2"><p>بود چون منبع جمال و جلال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>وین دو را حکم بود دیگرگون</p></div>
<div class="m2"><p>در ظهور و خفا بروز و کمون</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دوست دارد خفا جلالیت</p></div>
<div class="m2"><p>رو به عاشق نما جمالیت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر خفا بود چون ظهور اشرف</p></div>
<div class="m2"><p>که ز تاریکی است نور اشرف</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رحمتش سبق یافت بر غضبش</p></div>
<div class="m2"><p>یافت عشق آنچه بود در طلبش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا زبردستی وجود بود</p></div>
<div class="m2"><p>نیستی زیردست بود بود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گفت احببت تا زحُبِّ ظهور</p></div>
<div class="m2"><p>پی بری سوی آن شرف به شعور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>این صفت‌ها چو لازمِ ذاتند</p></div>
<div class="m2"><p>بین اندر مقام اثباتند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گر یکی بر یکی شود غالب</p></div>
<div class="m2"><p>مطلب خوش را شود طالب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آن دگر بالتمام مخفی نیست</p></div>
<div class="m2"><p>نقص در شأن حق تعالی نیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر بود یک دو فرد انسانی</p></div>
<div class="m2"><p>تحت اسماء ضِدّ ربانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نبود آن دو را به هم الفت</p></div>
<div class="m2"><p>از دو سو تا ابد بود کلفت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تربیت گر نه این چنین باشد</p></div>
<div class="m2"><p>کارِ این هر دو عکس این باشد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چون شود بنده‌ای به لطف ازل</p></div>
<div class="m2"><p>مظهر لطف المعزّ به مثل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر که زی او رود به صدق و نیاز</p></div>
<div class="m2"><p>یابد او نیز همچو او اعزاز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>آن نبینی که پرتوِ مهتاب</p></div>
<div class="m2"><p>چون بتابد بر آینه یا آب</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>صیقلی گر بود مقابل او</p></div>
<div class="m2"><p>گیرد آن نور نقش در دل او</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همچنین نور نیّرِ ازلی</p></div>
<div class="m2"><p>چون فتد بر دلِ خفی و جلی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آنکه هم جعل اوست آب وگلش</p></div>
<div class="m2"><p>روشنی گیرد از فروغِ دلش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بهرهٔ او ز مایهٔ عزّت</p></div>
<div class="m2"><p>هست بر قدر پایهٔ همت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هرکه با صبح هم نشین باشد</p></div>
<div class="m2"><p>نورِ دلت در آستین باشد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ور بود انتظام او با شام</p></div>
<div class="m2"><p>همچو شب رویِ دل کند شب فام</p></div></div>
<div class="c"><p>در مناجات حضرت باری تعالی</p></div>
<div class="b" id="bn75"><div class="m1"><p>ای به مغز خرد زده اورنگ</p></div>
<div class="m2"><p>خویش را گنج داده در دلِ تنگ</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در دو عالمت نیست گنجایی</p></div>
<div class="m2"><p>جز دلِ عاشقان شیدایی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مغز را عقل و دیده را نوری</p></div>
<div class="m2"><p>در نقابِ ظهور مستوری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>حضرت عشق آفریدستی</p></div>
<div class="m2"><p>وز دو عالمش برگزیدستی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>خانهٔ دل چو شد تمام و کمال</p></div>
<div class="m2"><p>گستریدی درو بِساط جمال</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>یعنی این خلوت خدایی ماست</p></div>
<div class="m2"><p>حرمِ خاص کبریاییِ ماست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نیستی را بجز تو هست که کرد</p></div>
<div class="m2"><p>شب و روز و بلند و پست که کرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>برتر از کار این جهانی تو</p></div>
<div class="m2"><p>حاشَ للسّامعین نه آنی تو</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>هر کسی در خیالِ داورِ خویش</p></div>
<div class="m2"><p>صورتی ساخته است در خورِ خویش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چون شود مغزِ معرفت بی پوست</p></div>
<div class="m2"><p>همه دانند کاین قفاست نه روست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هر چه گفتند و هر چه می‌گویند</p></div>
<div class="m2"><p>همه راهِ خیال می‌پویند</p></div></div>
<div class="c"><p>فی اظهار الشّوق و الطلب الی المحبوب</p></div>
<div class="b" id="bn86"><div class="m1"><p>ای درون و برون ز تو لبریز</p></div>
<div class="m2"><p>عشقت از خاک تیره وجد انگیز</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>در نقاب ظهور مستوری</p></div>
<div class="m2"><p>بس که نزدیک گشته‌ای دوری</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تو نهانی و شوق دیدارت</p></div>
<div class="m2"><p>این چنین گرم کرده بازارت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>غم پنهانی تو دزدیده</p></div>
<div class="m2"><p>سینه از سینه دیده از دیده</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نالهٔ مست ترانهٔ غم تو</p></div>
<div class="m2"><p>خاطرم وجد خانهٔ غم تو</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>داغ عشق تو خانه زاد دلم</p></div>
<div class="m2"><p>نرود یار تو ز یاد دلم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>شوق تو چون فزون کند دردم</p></div>
<div class="m2"><p>گرد هر موی خویشتن گردم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ظاهر وباطن از تو درد آمیز</p></div>
<div class="m2"><p>همه جا خالی از تو و لبریز</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ای توصهبایِ ساغرِ همه کس</p></div>
<div class="m2"><p>نَشأَهٔ تست در سرِ همه کس</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ملک توحید را تو پادشهی</p></div>
<div class="m2"><p>خاصهٔ تست لا شریک لهی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ذات پاکت که ارفع از پستی است</p></div>
<div class="m2"><p>محض هستی است گرچه نه هستی است</p></div></div>
<div class="c"><p>فی صفتِ ظهور الحقّ و تجلّیاته</p></div>
<div class="b" id="bn97"><div class="m1"><p>یک زبان بینی و سخن بسیار</p></div>
<div class="m2"><p>یک نسیم است و موج در تکرار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>هر زمانیش جلوه‌ای دگراست</p></div>
<div class="m2"><p>لیک چشم علیل بی خبر است</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بخل در مبدء حقیقت نیست</p></div>
<div class="m2"><p>دو تجلی به یک طریقت نیست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>آب در بحر بی کران آبست</p></div>
<div class="m2"><p>چون کنی در سبو همان آبست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>هست توحید مردم بی درد</p></div>
<div class="m2"><p>حصرِ نوعِ وجود در یک فرد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>لیک غیر خدایِ جل و جلال</p></div>
<div class="m2"><p>نیست موجود نزد اهل کمال</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هر که داند بجز خدا موجود</p></div>
<div class="m2"><p>هست مشرک به کیش اهلِ شهود</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>وحدت خاصهٔ شهود اینست</p></div>
<div class="m2"><p>معنیِ وحدتِ وجود این است</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>حق چو هستی بود به مذهب حق</p></div>
<div class="m2"><p>غیر حق نیستی بود مطلق</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نیستی را وجود کی باشد</p></div>
<div class="m2"><p>بهره‌ور از نمود کی باشد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ذات در مرتبه مقدم ذات</p></div>
<div class="m2"><p>بی‌نیاز است ز اعتبار صفات</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>وحدتِ بحت بی کم و چه و چون</p></div>
<div class="m2"><p>ز اعتبارات وهمی است مصون</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>آنکه از اعتبار هر جهتی</p></div>
<div class="m2"><p>متصف می‌شود به هر صفتی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چون به خود عرض حسن خویش کند</p></div>
<div class="m2"><p>هر زمان وصفِ خویش بیش کند</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>دیده ور شو به حسن لم یزلی</p></div>
<div class="m2"><p>گو ز غیرت بتاب معتزلی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بعدِ کان مایهٔ وبال بود</p></div>
<div class="m2"><p>سبلِ چشم اعتزال بود</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ارنی گوی باش همچو کلیم</p></div>
<div class="m2"><p>لیک ناری ز لن ترانی بیم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>لن ترانی چه از سرِناز است</p></div>
<div class="m2"><p>درِامید همچنان باز است</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>لن ترا مهر بر لب ادب است</p></div>
<div class="m2"><p>مر مرا تازیانهٔ طلب است</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>این غرور است لایق گله نیست</p></div>
<div class="m2"><p>که بجز امتحان حوصله نیست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>آنکه سرمستِ جام دیدار است</p></div>
<div class="m2"><p>لا به چشمش نعم پدیدار است</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>آن زمان بزم قرب را شایی</p></div>
<div class="m2"><p>کت برانند پیشتر آیی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>نَحْنُ أَقْرَب دلیل نزدیکی است</p></div>
<div class="m2"><p>لیک چشمت به روی تاریکی است</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>می‌کند با لقاش روی به رو</p></div>
<div class="m2"><p>دیده را سرمهٔ فَمَنْیَرجُو</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>آنکه باشد به گاه گفت و شنید</p></div>
<div class="m2"><p>به تو نزدیکتر ز حبل ورید</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به چه بیگانگی ازو دوری</p></div>
<div class="m2"><p>قدمی پیش نه که مهجوری</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چشمت از آفتاب خیره شود</p></div>
<div class="m2"><p>کی بر آن آفتاب چیره شود</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چهرهٔ آفتاب خود فاش است</p></div>
<div class="m2"><p>بی نصیبی گناه خفاش است</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>دیده از آفتاب پر سازی</p></div>
<div class="m2"><p>چشم خفاش گر بیندازی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چشمِ خودبین خدای بین نشود</p></div>
<div class="m2"><p>حنظل از سعی انگبین نشود</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>دیده کو خویش را نمی‌بیند</p></div>
<div class="m2"><p>هیچ دانی چرا نمی‌بیند</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>قربِ بسیار مایهٔ دوریست</p></div>
<div class="m2"><p>وصلِ بی حد دلیل مهجوریست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>آن نبیی که از پی ابصار</p></div>
<div class="m2"><p>اندکی دوریت بود ناچار</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>آینه پای تا به سر بصر است</p></div>
<div class="m2"><p>لیک از عکسِ خویش بی خبر است</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>می‌کند جلوه در هزار لباس</p></div>
<div class="m2"><p>چه کند چون نه‌ای لباس شناس</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چون نداری نشانه‌ای از ذات</p></div>
<div class="m2"><p>می شوی گم در ازدحامِ صفات</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>زان گرفتار دامِ وسواسی</p></div>
<div class="m2"><p>که به هر کسوتیش نشناسی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>تو نظر کن به حسنِ روزافزون</p></div>
<div class="m2"><p>منگر بر لباس گوناگون</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>گر به چشمِ شهود بنشینی</p></div>
<div class="m2"><p>هر چه بینی نخست او بینی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>مرو آزرده گر ز خانهٔ ناز</p></div>
<div class="m2"><p>اندکی دیر می‌رسد آواز</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>روز شوق تو چون زیاده شود</p></div>
<div class="m2"><p>خود به خود بر تو درگشاده شود</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>آن زمان بر رخ طلب خندی</p></div>
<div class="m2"><p>کش ببینی و چشم بربندی</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>نه که نادیده چشم بگشایی</p></div>
<div class="m2"><p>که به نامحرمانش بنمایی</p></div></div>
<div class="c"><p>در نعت حضرت ختمی پناهؐ</p></div>
<div class="b" id="bn140"><div class="m1"><p>بر جبین دارم از خود نسبی</p></div>
<div class="m2"><p>داغِ طوع محمد عربیؐ</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>نقش هستیم چون برآمد راست</p></div>
<div class="m2"><p>احمد احمد زبند بندم خاست</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>هیچ کس را چو او ندارم دوست</p></div>
<div class="m2"><p>که سزاوار دوستاری اوست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>زده در پیشگاهِ آگاهی</p></div>
<div class="m2"><p>کوس تفرید لِیْمَعَ اللّهی</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بود بزم یگانگی را شمع</p></div>
<div class="m2"><p>شد از آتش مقام جمع الجمع</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بوده از وحدت جلالی او</p></div>
<div class="m2"><p>ما رَمَیْتَ إذ رَمَیْتَ حالی او</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>هرچه گفت از شهود مطلق گفت</p></div>
<div class="m2"><p>مَن رَآنی فَقَدْرَأَی الحق گفت</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بی نیازیش گردِ امکان شوی</p></div>
<div class="m2"><p>فقر ذاتیش اِنّما أنا گوی</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>مهر او چون زمشرقِ آدم</p></div>
<div class="m2"><p>ساخت روشن تمامی عالم</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>هریک از انبیا چو سایهٔ او</p></div>
<div class="m2"><p>می‌نمودند پایه پایهٔ او</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>رفته رفته بلند می‌گردید</p></div>
<div class="m2"><p>تا به نصف النهارِ عدل رسید</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>یافت در اعتدالِ نفسانی</p></div>
<div class="m2"><p>غایتِ استوایِ روحانی</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>سایه در خط استوا نبود</p></div>
<div class="m2"><p>ظلمتِ سایه زو روا نبود</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>آنکه جسمش تمام جان باشد</p></div>
<div class="m2"><p>روح پاکش ببین چه سان باشد</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>کی کند روح سایه انگیزی</p></div>
<div class="m2"><p>مگرش با گلی بیامیزی</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بر سر خلق بود ظلّ اللّه</p></div>
<div class="m2"><p>سایه را سایه کی بود همراه</p></div></div>
<div class="c"><p>در بیان فضیلت شاه اولیاء امیرالمؤمنین علی مرتضیؑ</p></div>
<div class="b" id="bn156"><div class="m1"><p>بعد حمدِ محمد آنکه ولی است</p></div>
<div class="m2"><p>ثالث خالق و رسول علی است</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>عقل و برهان و نفس هرسه گواست</p></div>
<div class="m2"><p>کین دو را غیر او سیم نه رواست</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>چون گروهی یگانه‌اش دیدند</p></div>
<div class="m2"><p>به خداییش می‌پرستیدند</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>حبّذا مایه‌ای بلند کمال</p></div>
<div class="m2"><p>که شود مشتبه به حق متعال</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>دید معبود را به دیدهٔ جان</p></div>
<div class="m2"><p>نپرستید تا ندید عیان</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>معبد از مقصدش نبد خالی</p></div>
<div class="m2"><p>بود اِیّاکَ نَعْبُدَش حالی</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>ساختی با خدا چو بزمِ حضور</p></div>
<div class="m2"><p>جامهٔ تن ز خود فکندی دور</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>پر به سودای تن نکوشیدی</p></div>
<div class="m2"><p>گاه کندی و گاه پوشیدی</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>در نماز آن چنان ز جا رفتی</p></div>
<div class="m2"><p>که دعاوار بر هوا رفتی</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>بود غفلت ز سلخِ پیکانش</p></div>
<div class="m2"><p>که به تن بود آن نه برجانش</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>خندق آسا به روز بدر و حنین</p></div>
<div class="m2"><p>ضربتش رشکِ طاعتِ ثقلین</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>گرد شرک ازوجود چون رفتی</p></div>
<div class="m2"><p>هر دم اللّه اکبری گفتی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>به هوا روز چون نگشت شبش</p></div>
<div class="m2"><p>شد خیو آب آتشِ غضبش</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چون هوای شکستِ عزّی کرد</p></div>
<div class="m2"><p>مصطفی کتف خویش کرسی کرد</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>آنکه مُهرِ نُبوّتش خوانی</p></div>
<div class="m2"><p>نقش پای علی است تا دانی</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>بر کمالات او بود برهان</p></div>
<div class="m2"><p>حجّت هَلْأَتَی عَلَی الإنْسانِ</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>متحد با نبی است در همه چیز</p></div>
<div class="m2"><p>جز نبوت که اوست اصل تمیز</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>اشجع و اصلح، افضل و اکرم</p></div>
<div class="m2"><p>از همه اعدل از همه اعلم</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>نَفَسی از سرِ هوا نزدی</p></div>
<div class="m2"><p>بی عبودیت خدا نزدی</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>غذی از مغز معرفت کردی</p></div>
<div class="m2"><p>روزی از سفرهٔ غنا خوردی</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>در لیالی چو شمع قائم بود</p></div>
<div class="m2"><p>چون فرشته مدام صائم بود</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>بنده او بود و دیگران خلقند</p></div>
<div class="m2"><p>والهٔ حلق و بستهٔ دلقند</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>بی مدیحش نمی‌زنم نفسی</p></div>
<div class="m2"><p>لیک نتوان شناخت قدرِ کسی</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>که نهفتند حالتش امت</p></div>
<div class="m2"><p>نیمی از بیم ونیمی از خست</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>سائلی در نماز اگر دیدی</p></div>
<div class="m2"><p>خاتمش در رکوع بخشیدی</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>یکی از فضل او غدیرخم است</p></div>
<div class="m2"><p>دیگری اِنّما وَلیّکُمْاست</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>در شب غارِ ثور زوج بتول</p></div>
<div class="m2"><p>خفت آسوده بر فراش رسول</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>حفظ از کید دشمنانش کرد</p></div>
<div class="m2"><p>جان خود را فدایِ جانش کرد</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>خواندیش اَنْزَعُ البطین شهِ دین</p></div>
<div class="m2"><p>اَنْزَعْاز شرک و از علوم بَطین</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>بد سرِ مصطفاش بر زانو</p></div>
<div class="m2"><p>سجده ناکرده مهر رفته فرو</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>دعوتش را کریم اجابت کرد</p></div>
<div class="m2"><p>رَدِّ خورشید یک دو نوبت کرد</p></div></div>
<div class="c"><p>در بیان فضائل و خلافت انسان کامل</p></div>
<div class="b" id="bn187"><div class="m1"><p>از سَلُونی حدیث چون گفتی</p></div>
<div class="m2"><p>گردِ جهل از جهانیان رُفتی</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>ای تو آئینهٔ تجلی ذات</p></div>
<div class="m2"><p>نسخهٔ جامعِ جمیع صفات</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>درنمودِ تو ذات مستور است</p></div>
<div class="m2"><p>ذاتِ مخفی صفات مذکور است</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>هم تو مخصوص لطف کَرَمنا</p></div>
<div class="m2"><p>هم تو منصوص عَلَّم اَلاسماء</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>خلقت ایزد به صورت خود کرد</p></div>
<div class="m2"><p>دست ساز محبت خود کرد</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>دادت از جامه خانهٔ تکریم</p></div>
<div class="m2"><p>خلقت خاص احسن التقویم</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>جز تو کس قابل امانت نیست</p></div>
<div class="m2"><p>وان امانت به جز خلافت نیست</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>زان ترا کار مشکل افتاده است</p></div>
<div class="m2"><p>که صفاتت مقابل افتاده است</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>این ظلومی چو ازتو یافت حصول</p></div>
<div class="m2"><p>لقبت کرد کردگار جهول</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>تا ابد زین خطر ملومی تو</p></div>
<div class="m2"><p>هم جهولی و هم ظلومی تو</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>به تو از ملک ماه تا ماهی</p></div>
<div class="m2"><p>نامزد شد خلیفة اللهی</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>مرحبا ای خلیفة الرّحمن</p></div>
<div class="m2"><p>حبّذا ای ودیعة السّبحان</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>افضل از زمرهٔ ملک ز آنی</p></div>
<div class="m2"><p>که ولایت به توست ارزانی</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>معتدل بود چون مزاج جهان</p></div>
<div class="m2"><p>از وجود تو یافت در تن جان</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>زنده از توست شخص عالم پیر</p></div>
<div class="m2"><p>گر نمانی تو می‌نماند دیر</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>تا ترا پردهٔ تو ساخته‌اند</p></div>
<div class="m2"><p>عالم از کردهٔ تو ساخته‌اند</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>هرچه در آسمان گردان هست</p></div>
<div class="m2"><p>در تو چیزی مقابل آن هست</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>نسخهٔ عالم کبیر تویی</p></div>
<div class="m2"><p>گرچه در آب و گل صغیر تویی</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>کبریایِ تو از ره دگر است</p></div>
<div class="m2"><p>از تو جزوی جهان مختصر است</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>جنس عالی یکان یکان منزل</p></div>
<div class="m2"><p>طی کند تا رسد سویِ سافل</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>غایت این تنزل انسان است</p></div>
<div class="m2"><p>برزخی بر وجوب امکان است</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>وحدت از مطلعت هویدا شد</p></div>
<div class="m2"><p>در تو گم گشت و از تو پیدا شد</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>ابتدای ظلام کثرت تو</p></div>
<div class="m2"><p>وانتهای صباح وحدت تو</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>گر شب کثرتی و بس تاری</p></div>
<div class="m2"><p>مطلع الفجر هم تویی باری</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>خویشتن را نکرده‌ای غربال</p></div>
<div class="m2"><p>زانی از خود فتاده در دنبال</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>خویش را گر ز خود فرو بیزی</p></div>
<div class="m2"><p>به دو چنگال در خود آویزی</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>تو امانت نگاهدار حقی</p></div>
<div class="m2"><p>سرّ بپوشان که رازدار حقی</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>آنکه جوییش آشکار و نهفت</p></div>
<div class="m2"><p>خویشتن را به پردهٔ تو نهفت</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>اندرین پرده بایدش نگری</p></div>
<div class="m2"><p>که خوش آینده نیست پرده دری</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>آن که شوقت براش در بدر است</p></div>
<div class="m2"><p>از تو پنهان به خانهٔ تو در است</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>دل که جا داده‌ایش در سینه</p></div>
<div class="m2"><p>در کف اوست همچو آیینه</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>در تو انوار خویش می‌بیند</p></div>
<div class="m2"><p>عکس رخسار خویش می‌بیند</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>تو که آیینهٔ جمال ویی</p></div>
<div class="m2"><p>از چه محروم از کمال ویی</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>از رخ خویش پرده کن یک سوی</p></div>
<div class="m2"><p>گلی از روی آفتاب بشوی</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>از تو تا آنکه طالب آنی</p></div>
<div class="m2"><p>یک دو گام است و تو نمی‌دانی</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>هم متاعی و هم خریداری</p></div>
<div class="m2"><p>با خودت هست طرفه بازاری</p></div></div>