---
title: >-
    بخش ۴ - ابوالقاسم فندرسکی قُدِّسَ سِرُّهُ
---
# بخش ۴ - ابوالقاسم فندرسکی قُدِّسَ سِرُّهُ

<div class="n" id="bn1"><p>اسم شریف آن جناب میرابوالقاسم و فندرسک قریه‌ای است مِنْاعمال استراباد. وی وحید عصر و فرید عهد خود بوده، بلکه در هیچ عهدی در مراتب علمی خاصه در حکمت الهی به پایه و مایهٔ ایشان هیچ یک از حکما نرسیده. جامع معقول و منقول و فروع و اصول بود وبا وجود فضل و کمال اغلب اوقات مجالس و موانس فقرا و اهل حال بود و از مصاحبت و معاشرت اهل جاه و جلال احتراز می‌فرمودو بیشتر لباس فرومایه و پشمینه می‌پوشید و به تحلیه و تصفیهٔ نفس نفیس خویش می‌کوشید. همواره از مجالست اعزّه و اعیان مجانب و با اجامره و اوباش مصاحب بود. این معنی را به سمع شاه عباس صفوی رسانیدند. روزی در اثنای صحبت، شاه به میر گفت که شنیده‌ام بعضی از طلبهٔ علوم در سلک اوباش حاضر و به مزخرفات ایشان ناظر می‌شوند. جناب میر، مطلب را دریافته، گفت: من هر روزه در کنار معرکه‌ها حاضرم. کسی را از طلاب در آنجا نمی‌بینم. شاه شرمسار شده، دم در کشید، مدّتی به سفر هندوستان رفت و در آن بلاد به اندک چیزی ملازمت می‌کرد. چون سرّ حالش فاش گردیده راه بلد دیگر می‌پیمود. غرض، آن جناب حکیمی بزرگوار و فاضلی والاتبار بود و کمال تجرد را داشت. در دبستان آمده که بدو گفتند که چرا به حج نمی‌روی؟ گفت: در آنجا باید به دست خود گوسفندی کشت و مرا دشوار است که جانداری بی جان کنم. کرامات و مقامات آن جناب زیاده از حد تحریر است. مرقدش در اصفهان مشهور است:</p></div>
<div class="c"><p>مِنْقصایده قُدِّسَ سِرُّه</p></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ با این اختران نغز و خوش زیباستی</p></div>
<div class="m2"><p>صورتی در زیر دارد هرچه بر بالاستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت زیرین اگر با نردبان معرفت</p></div>
<div class="m2"><p>بر رود بالا همان با اصل خود یکتاستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سخن را درنیابد هیچ فهم ظاهری</p></div>
<div class="m2"><p>گر ابونصرستی وگر بوعلی سینا ستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان اگر نه عارضستی زیر این چرخ کهن</p></div>
<div class="m2"><p>این بدن‌ها نیز دایم زنده و برپاستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه عارض باشد آن را جوهری باید نخست</p></div>
<div class="m2"><p>عقل بر این دعوی ما شاهدی گویاستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌توانی گر زخورشید این صفت‌ها کسب کرد</p></div>
<div class="m2"><p>روشن است و بر همه تابان و خود تنهاستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورت عقلی که بی پایان و جاویدان بود</p></div>
<div class="m2"><p>با همه هم بی همه مجموعه و یکتاستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان عالم خوانمش گر ربط جان داری به تن</p></div>
<div class="m2"><p>در دل هر ذره هم پنهان و هم پیداستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هفت ره بر آسمان از فوق ما فرمود حق</p></div>
<div class="m2"><p>هفت در از سوی دنیا جانب عقباستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌توانی از ره آسان شدن بر آسمان</p></div>
<div class="m2"><p>راست باش و راست رو کانجا نباشد کاستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکه فانی شد به او یابد حیات جاودان</p></div>
<div class="m2"><p>ور به خود افتاد کارش بی شک از موتاستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این گهر در رمز دانایان پیشین سفته‌اند</p></div>
<div class="m2"><p>پی برد در رمزها هر کس که او داناستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین سخن بگذر که او مهجور اهل عالم است</p></div>
<div class="m2"><p>راستی پیا کن و این راه رو گر راستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرچه بیرونست از ذاتش نیابد سودمند</p></div>
<div class="m2"><p>خویش را او ساز اگر امروز وگر فرداستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست حدی و نشانی کردگارپاک را</p></div>
<div class="m2"><p>نی برون از ما و نی با ما و نی بی ماستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قول زیبا نیست بی کردار نیکو سودمند</p></div>
<div class="m2"><p>قول با کردارِ زیبا لایق و زیباستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتن نیکو به نیکویی نه چون کردن بود</p></div>
<div class="m2"><p>نام حلوا بر زبان بردن نه چون حلواستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این جهان و آن جهان و بی جهان و با جهان</p></div>
<div class="m2"><p>هم توان گفتن مر او را هم از آن بالاستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقل کشتی، آرزو گرداب و دانش بادبان</p></div>
<div class="m2"><p>حق تعالی ساحل و عالم همه دریاستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نفس را چون بندها بگسست یابد نام عقل</p></div>
<div class="m2"><p>چون به بی بندی رسی بند دگر برجاستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت دانا نفس ما را بعد ما حشر است و نشر</p></div>
<div class="m2"><p>هر عمل کامروز کرد او را چرا فرداستی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت دانا نفس ما را بعد ما باشد وجود</p></div>
<div class="m2"><p>در جزا و در عمل آزاد و بی همتاستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نفس را نتوان ستود او را ستودن مشکلست</p></div>
<div class="m2"><p>نفس بنده عاشق و معشوق آن مولاستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت دانا نفس هم با جاه و هم بی جاه بود</p></div>
<div class="m2"><p>گفت دانا نفس نی بی جاه نی با جاستی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت دانا نفس را آغاز و انجامی بود</p></div>
<div class="m2"><p>گفت دانا نفس بی انجام و بی مبداستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این سخن‌ها گفت دانا و کسی از وهم خویش</p></div>
<div class="m2"><p>در نیابد این سخن‌ها کاین سخن معماستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت دانا نفس را وصفی بیارم گفت هیچ</p></div>
<div class="m2"><p>نه به شرط شیء باشد نه به شرط لاستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیتکی از بومعین آرم در استشهادِ وی</p></div>
<div class="m2"><p>گرچه او در باب دیگر لایق اینجاستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر یکی بر دیگری دارد دلیل از گفته‌ای</p></div>
<div class="m2"><p>در میان، بحث و نزاع و شورش و غوغاستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کاش دانایان پیشین می‌بگفتندی تمام</p></div>
<div class="m2"><p>تا خلاف ناتمامان از میان برخاستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر کسی چیزی همی گویدبه تیره رای خویش</p></div>
<div class="m2"><p>تا گمان آید که او قسطای بن لوقاستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خواهشی اندر جهان هر خواهشی را در پی است</p></div>
<div class="m2"><p>خواستی باید که بعد از وی نباشد خواستی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn34"><div class="m1"><p>ندانم کز کجا آمد شد خلق است می‌دانم</p></div>
<div class="m2"><p>که هردم از سرای این جهان این رفت و آن آمد</p></div></div>
<div class="c"><p>رباعی</p></div>
<div class="b" id="bn35"><div class="m1"><p>کافر شده‌‌ام به دست پیغمبر عشق</p></div>
<div class="m2"><p>جنت چه کنم جان من و آذر عشق</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شرمندهٔ عشق روزگارم که شدم</p></div>
<div class="m2"><p>درد دل روزگار و درد سر عشق</p></div></div>