---
title: >-
    بخش ۴۷ - حسینی هروی نَوَّرَ اللّهُ مَرقَدَهُ
---
# بخش ۴۷ - حسینی هروی نَوَّرَ اللّهُ مَرقَدَهُ

<div class="n" id="bn1"><p>امیر حسین ابن عالم بن ابی الحسین و جامع علوم ظاهریه و باطنیه و حاوی فضایل عقلیه و نقلیه. پس از ترک سلطنت به مولتان رفته خدمت شیخ رکن الدین ابوالفتح که به یک واسطه ازمریدان شیخ بهاءالدین زکریای ملتانی است، رسیده. بعضی گویند که به خدمت شیخ بهاءالدین زکریا فایض گردیده. عَلی أَیُّ حال از اماجد ارباب مقامات و از اکابر اصحاب کرامات و از محققین زمان خود بوده. نثراً و نظماً کتب محققانه تصنیف فرموده. مِنْجمله در منثورات: نزهت الارواح و صراط المستقیم و روح الارواح و درمنظومات: کنزالرموز و زاد المسافرین و طبع فقیر را به طرز زادالمسافرین کمال انس است. لهذا بر سنن آن انیس العاشقین را پرداخته. گویند طرب المجالس نیز منسوب به اوست. دیده‌ام سوؤالات گلشن راز شیخ محمود از ایشان و آن هفده سوؤال و افتتاحش بدین منوال است:</p></div>
<div class="c"><p>نظم</p></div>
<div class="b" id="bn2"><div class="m1"><p>ز اهل دانش وارباب معنی</p></div>
<div class="m2"><p>سؤوالی دارم اندر باب معنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخست از فکر خویشم در تحیر</p></div>
<div class="m2"><p>چه چیزاست آن که خوانندش تفکر</p></div></div>
<div class="n" id="bn4"><p>الی آخره.</p></div>
<div class="n" id="bn5"><p>گلشن در جواب این سؤال‌ها است. غرض، وفاتش در سنهٔ ۷۳۸ در هرات و از آن جناب است:</p></div>
<div class="n" id="bn6"><p>مِنْ مثنوی زادالمسافرین</p></div>
<div class="b" id="bn7"><div class="m1"><p>آنجا که حریم بی نیازی است</p></div>
<div class="m2"><p>اندیشهٔ ما خیال بازی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرفی که رود ز راه تقلید</p></div>
<div class="m2"><p>خرسندی طبع دان نه توحید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قومی که ز جمله پیش دیدند</p></div>
<div class="m2"><p>در آینه عکس خویش دیدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همواره به گرد خود تنی تو</p></div>
<div class="m2"><p>آن گه دم معرفت زنی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آینه دیده‌ای هوا را</p></div>
<div class="m2"><p>گویی که شناختم خدا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او را چو همیشه او تمام است</p></div>
<div class="m2"><p>گستاخ مرو که کار خام است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنهار به حجت و قیاسی</p></div>
<div class="m2"><p>غره نشوی به حق شناسی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مشکل بود ای غریب گمراه</p></div>
<div class="m2"><p>گند بغل و ندیمی شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شبلی چو در این تحیر افتاد</p></div>
<div class="m2"><p>روزی دَرِ این سوؤال بگشاد</p></div></div>
<div class="c"><p>حکایت</p></div>
<div class="b" id="bn16"><div class="m1"><p>آمد برِ آن جهان پر شور</p></div>
<div class="m2"><p>مقبول ازل حسین منصور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پرسید که این چه کار سازیست</p></div>
<div class="m2"><p>در حقه نگر چه مهره بازیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اللّه چه لفظ یا چه نام است</p></div>
<div class="m2"><p>کو ورد زبان خاص و عام است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتا نی‌ام از حقیقت آگاه</p></div>
<div class="m2"><p>لیکن همه در تو بینم این راه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تحقیق تو چیست بی تو بودن</p></div>
<div class="m2"><p>زین بیش نمی‌توان نمودن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر یک به اشارتی دویدند</p></div>
<div class="m2"><p>کردند بیان چنانکه دیدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در دیدن‌شان شکی نباشد</p></div>
<div class="m2"><p>لیک آن همه جز یکی نباشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن دیده که او دویی نبیند</p></div>
<div class="m2"><p>جز وحدت معنوی نبیند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در راه تو ای غریب دلتنگ</p></div>
<div class="m2"><p>بیرون ز تو نیست هیچ فرسنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیگانه ز آشنایی ماست</p></div>
<div class="m2"><p>پیوستن او جدایی ماست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آه این چه ترانه می‌زنم من</p></div>
<div class="m2"><p>عمری است همی که جان کنم من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از خویشتنم خبر نیامد</p></div>
<div class="m2"><p>جز یک دم سرد برنیامد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسیار دویدم از چپ و راست</p></div>
<div class="m2"><p>حاصل نشد آنچه دل همی خواست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر طایفه را بیازمودم</p></div>
<div class="m2"><p>گه پیر و گهی مرید بودم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با هر که دلم زد این نفس را</p></div>
<div class="m2"><p>آسوده ندیده هیچ کس را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کس را به حقیقتش گذر نیست</p></div>
<div class="m2"><p>وز رفتن و آمدن خبر نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گویند عنان خود چه تابی</p></div>
<div class="m2"><p>گم شو که چو گم شوی بیابی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این نکته نمود ناصوابم</p></div>
<div class="m2"><p>چون گم شوم آنگهی چه یابم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نایافته را کسی چه جوید</p></div>
<div class="m2"><p>گم گشته ز یافتن چه گوید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا کی طلبم در این ره او را</p></div>
<div class="m2"><p>این چیست که گم کنم من او را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می‌سوزم و زهرهٔ نفس نیست</p></div>
<div class="m2"><p>درمان چه کنم که دسترس نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این سوخته چند کاهد آخر</p></div>
<div class="m2"><p>از سوختنم چه خواهد آخر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر دم غمش آتشی فروزد</p></div>
<div class="m2"><p>تا سوخته را دوباره سوزد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای هم تو ز چشم خود نهانی</p></div>
<div class="m2"><p>نادان شده‌ای و می‌ندانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای پنج و دو را شمار با تو</p></div>
<div class="m2"><p>تو غافل و جمله کار با تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر خود نظر از حواس کردی</p></div>
<div class="m2"><p>حیوانِ دگر قیاس کردی</p></div></div>
<div class="c"><p>خطاب به حضرت جامعهٔ انسان</p></div>
<div class="b" id="bn42"><div class="m1"><p>ای قطره تو غافلی ز دریا</p></div>
<div class="m2"><p>در جوی تو می‌رود هویدا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اللّه به حول در نهاد است</p></div>
<div class="m2"><p>اما نه حلول و اتحاد است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آیینهٔ هر دو عالمی تو</p></div>
<div class="m2"><p>بندیش که با که همدمی تو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ای صورت خوب و زشت با تو</p></div>
<div class="m2"><p>هم دوزخ و هم بهشت با تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در برج تو آفتاب و ماه است</p></div>
<div class="m2"><p>لیکن پس پردهٔ سحاب است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>داری تو زمین و آسمانی</p></div>
<div class="m2"><p>گر یافته‌ای بده نشانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پیدا و نهان و بود و نابود</p></div>
<div class="m2"><p>در لوح تو هست جمله موجود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر دیدهٔ دیده را گشایی</p></div>
<div class="m2"><p>در خود همه را به خود نمایی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دانی چو ببینی از چپ و راست</p></div>
<div class="m2"><p>کاین هجده هزار عالم اینجاست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای بی خبر از جهان معنی</p></div>
<div class="m2"><p>با تو چه کنم بیان معنی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا در نظرت امید و بیم است</p></div>
<div class="m2"><p>راهت نه صراط مستقیم است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عمری سر و پا برهنه رفتی</p></div>
<div class="m2"><p>لیکن قدمی به ره نرفتی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چندین تک و پوی تو دو گام است</p></div>
<div class="m2"><p>بردار قدم که ره تمام است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اول ز تو رفتن است و دیدن</p></div>
<div class="m2"><p>آخر همه بردن و رسیدن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فانی شو اگر بقات باید</p></div>
<div class="m2"><p>بگذر ز خود ار خدات باید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر مردن تو ز تو تمام است</p></div>
<div class="m2"><p>حشر تو هم اندرین مقام است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مردان که ره خدا سپردند</p></div>
<div class="m2"><p>در عالم زندگی بمردند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اوصاف ذمیمه چون بدل شد</p></div>
<div class="m2"><p>هر عقده که بود در توحل شد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در شیب و فراز این مقامات</p></div>
<div class="m2"><p>صد گم شده بینی از کرامات</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مردان همه اصل پاک دارند</p></div>
<div class="m2"><p>نسبت نه به آب و خاک دارند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون آب روند بی علایق</p></div>
<div class="m2"><p>آمیخته با همه خلایق</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>این ره نه به خرقه و گلیم است</p></div>
<div class="m2"><p>اول قدمش دل سلیم است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نزدیک کسی که راه بین است</p></div>
<div class="m2"><p>نفرینِ خلایق آفرین است</p></div></div>
<div class="c"><p>در صفت عشق</p></div>
<div class="b" id="bn65"><div class="m1"><p>ای پرده نشین این گذرگاه</p></div>
<div class="m2"><p>بی عشق به سر نمی‌رسد راه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اول قدمی که عشق دارد</p></div>
<div class="m2"><p>ابری است که جمله کفر بارد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>منصور نه مرد سرسری بود</p></div>
<div class="m2"><p>از تهمت کافری بری بود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون نکتهٔ اصل گفت با فرع</p></div>
<div class="m2"><p>ببرید سرش سیاست شرع</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در عشق نه شک و نه یقین است</p></div>
<div class="m2"><p>نه چون و چرا نه کفر و دین است</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آنان که ز جام عشق مستند</p></div>
<div class="m2"><p>حق را ز برای حق پرستند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دل حق طلبید و نفس باطل</p></div>
<div class="m2"><p>این عربده‌ایست سخت مشکل</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چون در نظر تو ما و من نیست</p></div>
<div class="m2"><p>او باشد و او دگر سخن نیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>می بین و مپرس تا بدانی</p></div>
<div class="m2"><p>می‌دان و مگوی تا نمانی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سر بر قدم و قدم به سر نه</p></div>
<div class="m2"><p>وانگه قدم از قدم به در نه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بی نام و نشان شو و نشان کن</p></div>
<div class="m2"><p>بی کام و بیان شو و بیان کن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو جام جهان نمای خویشی</p></div>
<div class="m2"><p>از هرچه قیاس تست بیشی</p></div></div>
<div class="c"><p>رباعی</p></div>
<div class="b" id="bn77"><div class="m1"><p>ای سایه، تو مرد صحبت نور نه‌ای</p></div>
<div class="m2"><p>رو ماتم خود گیر کزین سور نه‌ای</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>اندیشهٔ وصل آفتابت رسد</p></div>
<div class="m2"><p>می‌ساز بدین قدر کزو دور نه‌ای</p></div></div>