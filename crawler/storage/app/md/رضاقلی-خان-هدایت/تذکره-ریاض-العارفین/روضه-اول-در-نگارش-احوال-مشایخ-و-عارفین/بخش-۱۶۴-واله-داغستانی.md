---
title: >-
    بخش ۱۶۴ - واله داغستانی
---
# بخش ۱۶۴ - واله داغستانی

<div class="n" id="bn1"><p>اسمش علیقلی خان و ازاعاظم لکزیهٔ داغستان. اجداد و اعمامش در دولت صفویه صاحب مناصب عالیه بودند و درآن ولایت حکومت می‌نمودند. وی در سنهٔ ۱۱۲۴ متولد شد. چندی در پیش سلطان حسین صفوی بود. پس از طغیان طایفهٔ افاغنه و فتور آن دولت علیه به هندوستان رفت و به خدمت خلیفه ابراهیم بدخشانی ارادت داشت. با وجود منصب، درویش مشرب همواره با درویشان مجالس و با صفاکیشان موأنس. تذکرة الشعرایی هم در آن ولایت نگاشته. دیوانش تخمیناً چهار هزار بیت می‌شود، در سنهٔ ۱۱۶۵ فوت شد. چندبیتی از غزلیات و رباعیات او نوشته شد:</p></div>
<div class="c"><p>مِنْغزلیّاته</p></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشه کسی راه به کنه تو ندارد</p></div>
<div class="m2"><p>هرچیز که هست از تو نشان هست ونشان نیست</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn3"><div class="m1"><p>یک نغمه تراود ز لب قمری و بلبل</p></div>
<div class="m2"><p>قانون وفامختلف آواز نباشد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn4"><div class="m1"><p>عشق بازان سخن حق همه جا می‌گویند</p></div>
<div class="m2"><p>از که ترسند سردار سلامت باشد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn5"><div class="m1"><p>کفر کافر به ز دین ناقص است</p></div>
<div class="m2"><p>این چنین فرمود پیر کاملم</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn6"><div class="m1"><p>چون به قاف عدمم راه تماشا افتاد</p></div>
<div class="m2"><p>هر کجادیده گشودم همه عنقا دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطره بودم سر هم چشمی بحرم می‌بود</p></div>
<div class="m2"><p>نظر از خویش چو بستم رهِ دریا دیدم</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn8"><div class="m1"><p>چاک می‌شد به برت خرقهٔ تقوی چون ما</p></div>
<div class="m2"><p>گر تو هم می‌شدی ای شیخ گرفتارِ کسی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn9"><div class="m1"><p>بگشای سر ترکش مژگان جگردوز</p></div>
<div class="m2"><p>شاید که رسد چاکِ دل ما به رفویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش آنکه به طوف حرم میکده آیم</p></div>
<div class="m2"><p>گه پای خمی بوسم و گه دست سبویی</p></div></div>
<div class="c"><p>مِنْرباعیّاته</p></div>
<div class="b" id="bn11"><div class="m1"><p>در معرکهٔ عشق ستیز دگر است</p></div>
<div class="m2"><p>فتح دگر اینجا و گریز دگر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فریاد و فغان و گریه و ناله و آه</p></div>
<div class="m2"><p>این‌ها هوس است و عشق چیز دگر است</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn13"><div class="m1"><p>ذرات جهان که جمله مرآت تواند</p></div>
<div class="m2"><p>چون قطره به بحر، غرق در ذات تواند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون موج که هر نفس کشد سر در جیب</p></div>
<div class="m2"><p>در نفی وجود خویش و اثبات تواند</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn15"><div class="m1"><p>من زنده به دوستم، نمیرم هرگز</p></div>
<div class="m2"><p>مغزی بی پوستم نمیرم هرگز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کس که نه اوست مرده‌اش دان ز ازل</p></div>
<div class="m2"><p>من خود همه اوستم نمیرم هرگز</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn17"><div class="m1"><p>گاهی به فلک مهر درخشان بودم</p></div>
<div class="m2"><p>گاهی به هوا ذرّهٔ پویان بودم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گاهی دل و گاه تن، گهی جان بودم</p></div>
<div class="m2"><p>زین پس همه آن شوم که هم آن بودم</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn19"><div class="m1"><p>مرآت جمال حق تعالی شده‌ام</p></div>
<div class="m2"><p>در مملکتِ وجود والی شده‌ام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در بحر خدا شکسته ظرفم چو حباب</p></div>
<div class="m2"><p>از دوست پر و ز خویش خالی شده‌ام</p></div></div>