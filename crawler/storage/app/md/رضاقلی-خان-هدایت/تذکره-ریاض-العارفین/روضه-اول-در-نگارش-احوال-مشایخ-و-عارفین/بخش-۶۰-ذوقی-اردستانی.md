---
title: >-
    بخش ۶۰ - ذوقی اردستانی
---
# بخش ۶۰ - ذوقی اردستانی

<div class="n" id="bn1"><p>از قصبهٔ مذکور و به علی شاه مشهور بوده. در اصفهان گیوه دوزی می‌نموده. تحصیلی نکرده اما ذوقی داشته. مردی درویش مشرب و از اهل طلب است. با حکیم شفایی معاصر بوده. از اشعار اوست:</p></div>
<div class="b" id="bn2"><div class="m1"><p>نه شکوفه‌ای نه برگی نه ثمر نه سایه دارم</p></div>
<div class="m2"><p>همه حیرتم که دهقان به چه کار کشت ما را</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه کعبه نپوشد لباس ماتمیان</p></div>
<div class="m2"><p>که خانه‌ای چو دلش در مقابل افتاده است</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn4"><div class="m1"><p>از جنون عشق زنجیری که در پای منست</p></div>
<div class="m2"><p>چشم‌ها بگشوده وحیران سودای منست</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn5"><div class="m1"><p>از خود برون نرفتم و آوردمش به دست</p></div>
<div class="m2"><p>ممنون همتم که مرادر به در نکرد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn6"><div class="m1"><p>روزگارم ز چه رو منصب نادانی داد</p></div>
<div class="m2"><p>گر نمی‌خواست که من مرشد کامل باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمزه در تیر زدن بود که مژگان دریافت</p></div>
<div class="m2"><p>قسمت این بود که مقتول دو قاتل باشم</p></div></div>
<div class="c"><p>رباعی</p></div>
<div class="b" id="bn8"><div class="m1"><p>آیینهٔ مهر، روشن از یاد علی است</p></div>
<div class="m2"><p>اوراد ملک بر آسمان ناد علی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر سلطنت دو کون خواهی ذوقی</p></div>
<div class="m2"><p>در بندگی علی و اولاد علی است</p></div></div>