---
title: >-
    بخش ۲۹ - ابوذر بوزجانی
---
# بخش ۲۹ - ابوذر بوزجانی

<div class="n" id="bn1"><p>از اعاظم مشایخ متقدمین و از اماجد محققین بوده. از آن جناب است:</p></div>
<div class="c"><p>عربیه</p></div>
<div class="b" id="bn2"><div class="m1"><p>یَعْرِفُنا مَنْکانَ مِنْجِنْسِنا</p></div>
<div class="m2"><p>وَسائِرُ النّاسِ لَنَا مُنْکَروُن</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn3"><div class="m1"><p>تو به علم ازل مرا دیدی</p></div>
<div class="m2"><p>دیدی آنگه به عیب بخریدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو به علم آن و من به عیب همان</p></div>
<div class="m2"><p>رد مکن آنچه خود پسندیدی</p></div></div>