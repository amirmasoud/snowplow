---
title: >-
    بخش ۷۵ - سحابی استرابادی قُدِّسَ سِرُّه
---
# بخش ۷۵ - سحابی استرابادی قُدِّسَ سِرُّه

<div class="n" id="bn1"><p>عارفی است کامل و عاشقی است واصل. شهودش مدام و حضورش بر دوام. فکرش خالی ازوسواس و ذکرش عاری از حواس. طبعش عالی و قولش حالی. بعضی او را از اهل شوشتر دانسته. تحقیق آن است که مولدش در شوشتر و اصل از جرجان است. موطنش نجف اشرف علی ساکنها الف التحیّة و التحف. ظهورش در زمان شاه عباس صفوی. چهل سال در نجف، انزوا اختیار کرد و روی توجه به عبادت آورده. هم در آنجا فوت و مدفون شد. علاوه بر غزلیات شش هزار رباعی محققانه فرموده است:</p></div>
<div class="c"><p>مِنْغزلیّاته</p></div>
<div class="b" id="bn2"><div class="m1"><p>دیده پوشیدم چو در دل یافتم دلدار را</p></div>
<div class="m2"><p>در ببندد هرکه او در خانه یابد یار را</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn3"><div class="m1"><p>عالمان را علم هست و ره به اوج راز نیست</p></div>
<div class="m2"><p>هست مرغِ خانه را بال و پرِ پرواز نیست</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق که جمله عشق شود ره به او برد</p></div>
<div class="m2"><p>چون پر شود پیاله به می سر فرو برد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn5"><div class="m1"><p>آنان که فقر را به تنعم فروختند</p></div>
<div class="m2"><p>فردوس را به دانهٔ گندم فروختند</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn6"><div class="m1"><p>عشق پیدا کن که گردی از غم عالم خلاص</p></div>
<div class="m2"><p>نی غلط گفتم که عالم را کنی از غم خلاص</p></div></div>
<div class="c"><p>رباعیات</p></div>
<div class="b" id="bn7"><div class="m1"><p>بشتاب پی دیده گشودن خود را</p></div>
<div class="m2"><p>زنگار ز آیینه زدودن خود را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچند تو او را نتوانی دیدن</p></div>
<div class="m2"><p>او بتواند به تو نمودن خود را</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn9"><div class="m1"><p>تحقیق گهی که رونماید خود را</p></div>
<div class="m2"><p>حق از همه رو نکو نماید خود را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان رو خودبین به خود اسیر است که حق</p></div>
<div class="m2"><p>در صورت او به او نماید خود را</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn11"><div class="m1"><p>او آب جمال داد گلزار ترا</p></div>
<div class="m2"><p>او آتش قهر زد خس و خار ترا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای آمده در شورگه او کو او کو</p></div>
<div class="m2"><p>این کیست که کرده گرم بازار ترا</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn13"><div class="m1"><p>حق است در این تفرقه کیشان پیدا</p></div>
<div class="m2"><p>هم در حق این جمعِ پریشان پیدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حق بینش و آیینه و شخصند همه</p></div>
<div class="m2"><p>ایشان در حق و حق در ایشان پیدا</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn15"><div class="m1"><p>هر قرعه که زد حکیم دربارهٔما</p></div>
<div class="m2"><p>دیدیم نبود غیرِ آن چارهٔ ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی حکمت نیست هرچه از ما سر زد</p></div>
<div class="m2"><p>مأمورهٔ اوست نفس امارهٔ ما</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn17"><div class="m1"><p>آن گنج خفی نکرد ظاهرشان را</p></div>
<div class="m2"><p>تا خلق نکرد حضرت انسان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شمع است نمایندهٔ کس در شب تار</p></div>
<div class="m2"><p>هر چند که خود ساخته باشد آن را</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn19"><div class="m1"><p>عالم به خروش لااله الا هوست</p></div>
<div class="m2"><p>غافل به گمان که دشمن است این یا دوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دریا به وجود خویش موجی دارد</p></div>
<div class="m2"><p>خس پندارد که این کشاکش با اوست</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn21"><div class="m1"><p>زین سو همه طعنهٔ رقیب بدگوست</p></div>
<div class="m2"><p>زان سو همه تیغ ناز و بی مهری اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حاصل به جهان عشق کان عرصهٔ ماست</p></div>
<div class="m2"><p>گه کشتهٔ دشمنیم و گه کشتهٔ دوست</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn23"><div class="m1"><p>دانی غافل کی از خدا یاد کند</p></div>
<div class="m2"><p>آن دم که جلال صیحه بنیاد کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از خواب که، خفته را کند کس بیدار</p></div>
<div class="m2"><p>آهسته چو برنخاست فریاد کند</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn25"><div class="m1"><p>پس ساده دلی کزین ره آگاه افتاد</p></div>
<div class="m2"><p>بس اهلِ خرد که در تکِ چاه افتاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این کار حوالتی نه علم و عملی است</p></div>
<div class="m2"><p>چون گنج که تا که را به او راه افتاد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn27"><div class="m1"><p>هر گه به جهانِ جاودان خواهی شد</p></div>
<div class="m2"><p>از جزو نهان ز کُلّ عیان خواهی شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گویی که چو میرم ز جهان خواهم رفت</p></div>
<div class="m2"><p>این طرفه که آن دم تو جهان خواهی شد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn29"><div class="m1"><p>عالم همه فرعِ تست ای اصل وجود</p></div>
<div class="m2"><p>هر چند وجود تو در آن خورد نمود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پرتو مر شمع را محیط افتد و بس</p></div>
<div class="m2"><p>هر چند ز شمع باشدش بود و نبود</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn31"><div class="m1"><p>گر از حرمِ عشق خطابت آید</p></div>
<div class="m2"><p>وارستگی از خیال و خوابت آید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ناخوانده کتاب صد علومت بخشند</p></div>
<div class="m2"><p>ناکرده سئوال صد جوابت آید</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn33"><div class="m1"><p>بس فتنه که خلق در گمانش باشند</p></div>
<div class="m2"><p>عاقل که چو لقمه در دهانش باشند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن آتش دوزخی کزان می‌ترسند</p></div>
<div class="m2"><p>چون وابینند در میانش باشند</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn35"><div class="m1"><p>نه با هر کس نکوست می‌باید بود</p></div>
<div class="m2"><p>بد را هم مغز و پوست می‌باید بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کاری سهل است دوست بودن با دوست</p></div>
<div class="m2"><p>با دشمن نیز دوست می‌باید بود</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn37"><div class="m1"><p>مطلوب حقیقی تو با تست متاز</p></div>
<div class="m2"><p>هر سو به هوای مطلبی چند مجاز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر بر سر افلاک شوی مسند ساز</p></div>
<div class="m2"><p>ترسم که همین مقام را جویی باز</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn39"><div class="m1"><p>از هر دو جهان زیاده‌ای می‌خواهم</p></div>
<div class="m2"><p>از پرده برون افتاده‌ای می‌خواهم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صوفی تو به کار خویش رو کاین ره را</p></div>
<div class="m2"><p>پا بر سر خود نهاده‌‌ای می‌خواهم</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn41"><div class="m1"><p>نه علم و عمل نه عز و جاهی داریم</p></div>
<div class="m2"><p>جان محو جمال پادشاهی داریم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ما از سخن دنیی و دین خاموشیم</p></div>
<div class="m2"><p>بر یاد کسی ناله و آهی داریم</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn43"><div class="m1"><p>در راه خدا نه جان نه تن می‌بینم</p></div>
<div class="m2"><p>هرچیز نه او خیال ظن می‌بینم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دورند تمام خلق عالم از راه</p></div>
<div class="m2"><p>گر راه چنین است که من می‌بینم</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn45"><div class="m1"><p>ای عاشق زار ترک آب و گل کن</p></div>
<div class="m2"><p>یعنی که گدایی جهانِ دل کن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از کوچهٔ تنگ تو شهی می‌گذرد</p></div>
<div class="m2"><p>برخیز و سرِ شاهرهی منزل کن</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn47"><div class="m1"><p>باید به همه خلق چو خویشان بودن</p></div>
<div class="m2"><p>یا بی همه همچو فردکیشان بودن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بی انصافی و کوری و مرده دلی است</p></div>
<div class="m2"><p>رد کردن خلق همچو ایشان بودن</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn49"><div class="m1"><p>ای دعوی عشق کرده، آیین تو کو</p></div>
<div class="m2"><p>قطعِ نظر از عقل، دل و دینِ تو کو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای دم زده از داغ وفا لاله صفت</p></div>
<div class="m2"><p>پیراهن چاک چاکِ خونین تو کو</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn51"><div class="m1"><p>تن از تو و دل از تو، جان هم از تو</p></div>
<div class="m2"><p>جان از تو چه حرف است جهان هم از تو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هرچند که برهستی خود می‌گویم</p></div>
<div class="m2"><p>ماییم و حدیث چند آن هم از تو</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn53"><div class="m1"><p>از جزو و کُلّ که در تخیل گردی</p></div>
<div class="m2"><p>بشنو سخنی کاهل تحمل گردی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در هستیِ خویش گر بمانی جزوی</p></div>
<div class="m2"><p>خود را همه جا نظر کنی کُلّ گردی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn55"><div class="m1"><p>آیینه صفت به دست آن نیکویی</p></div>
<div class="m2"><p>زین سوی نموده‌ای ولی آن سویی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اودیده ترا که عین هستی تواست</p></div>
<div class="m2"><p>زانش تو ندیده‌ای که عکس اویی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn57"><div class="m1"><p>هان تا که درین آینه آن رو بینی</p></div>
<div class="m2"><p>این هستی این سوی از آن سو بینی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>این پردهٔ پندار ز پیشت چو رود</p></div>
<div class="m2"><p>هرچند به خلق بنگری او بینی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn59"><div class="m1"><p>آنم که ندارم به دو عالم کامی</p></div>
<div class="m2"><p>نایافته جز به یک وجود آرامی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر خلق جهان جمله چو من بودندی</p></div>
<div class="m2"><p>لازم نشدی رسولی و پیغامی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn61"><div class="m1"><p>بشتاب که آزاده نهادی باشی</p></div>
<div class="m2"><p>مپسند که بندهٔ مرادی باشی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گر راه بدو بری همه جان گردی</p></div>
<div class="m2"><p>ور درمانی به خود جمادی باشی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn63"><div class="m1"><p>گم گردم اگر تو جستجویم نکنی</p></div>
<div class="m2"><p>آیینه صفت روی به رویم نکنی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در حق خود از لطف تو گفتم بسیار</p></div>
<div class="m2"><p>یارب یارب دروغگویم نکنی</p></div></div>