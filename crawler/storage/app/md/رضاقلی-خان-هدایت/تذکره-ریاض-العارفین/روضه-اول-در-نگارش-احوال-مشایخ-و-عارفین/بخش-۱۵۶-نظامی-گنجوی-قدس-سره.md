---
title: >-
    بخش ۱۵۶ - نظامی گنجوی قُدِّسَ سِرُّه
---
# بخش ۱۵۶ - نظامی گنجوی قُدِّسَ سِرُّه

<div class="n" id="bn1"><p>وهُوَ نظام الدین ابومحمد الیاس بن یوسف بن موید القمی، اصلش از تفرش قم و موطنش گنجه بوده. لهذا به شیخ نظامی گنجوی شهرت نموده. سلسلهٔ ارادت وی به جناب شیخ اخی فرج زنجانی که از مشاهیر مشایخ است می‌رسد، و از آغاز شباب معاشرت و مجالست اعاظم و سلاطین را قبول نفرمود و در زاویهٔ خود منزوی بود و خواقین هوشیار و سلاطین روزگار به خدمتش مشرف و از صحبتش مستفیض می‌شدند و هر یک از مثنویات خود را به استدعای یکی از ایشان گفته. گویند قزل ارسلان امتحاناً لباطنه به خانقاه وی رفته، شیخ مقصد وی را دریافته تجمل باطنی وحشمت معنوی خود را به وی نمود. چنانکه سلطان خدم و حشم او را بیش از خود دیده و از جلال آن جناب ترسیده، به ادب هرچه تمام‌تر به محفل رفته به اشارت او نشسته، بعد از اندک ساعتی دید که آنچه دید. مانند عالم همگی نمودی می‌بود و بجزا او احدی در میانه نبود. شیخ بر سجاده به تلاوت مشغول و خود بر روی خاک مسکن دارد. از این کرامت از اهل ارادت شد. غرض، اگرچه به سبب معارف و حقایق شاعری، پایه‌ای دون به جهت اوست، اما در این فن مرتبهٔ اعلی دارد. وفاتش در سنهٔ ۵۹۶. این اشعار از آن جناب است:</p></div>
<div class="c"><p>مِنْقصایده فی المعارف و الحقایق</p></div>
<div class="b" id="bn2"><div class="m1"><p>شحنهٔ ما دانش، آنگه حرص در همسایگی</p></div>
<div class="m2"><p>رستم ما زنده وانگه دیو در مازندران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه نز قرآن طرازی برفشان از آستین</p></div>
<div class="m2"><p>هرچه نز ایمان بساطی درنورد از آستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرق ها باشد میان آدمی و آدمی</p></div>
<div class="m2"><p>کز یک آهن نعل سازند از یکی دیگرسنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اصل هندو در سیاهی یک نسب دارد ولیک</p></div>
<div class="m2"><p>هندویی را دزد خوانی هندویی را پاسبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند ازین سلطان و سلطان از تو سلطان بنده‌تر</p></div>
<div class="m2"><p>بندهٔ او شو که آن شد صاحبِ سلطان نشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده بردار از زمین بنگر چه بازی می‌رود</p></div>
<div class="m2"><p>با عزیزان زمانه زیر پرده هر زمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به خرمن خار یابی بر کلاه یزدجرد</p></div>
<div class="m2"><p>تا به دامن خاک بینی بر سرِ نوشیروان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیم را رونق نخیزد تا به درناید ز سنگ</p></div>
<div class="m2"><p>لعل را قیمت نباشد تا برون ناید ز کان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک الملوک فضلم به فضیلت معانی</p></div>
<div class="m2"><p>ز من و زمان گرفته به مثال آسمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس بلند صوتم جرس بلند صیتم</p></div>
<div class="m2"><p>قلمِ جهان نوردم علم جهان ستانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر همتم رسیده به کلاه کیقبادی</p></div>
<div class="m2"><p>برِ حشمتم گذشته ز پرند جوزجانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حرکات اختران را منم اصل و او طفیلی</p></div>
<div class="m2"><p>طبقات آسمان را منم آب او اوانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مه‌ام و چو مه نگیرم کلف سیاه رویی</p></div>
<div class="m2"><p>زرم وچو زر ندارم برص سپید زایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملکا و پادشاها روشی کرامتم کن</p></div>
<div class="m2"><p>که بدان روش بگردم ز بدی و بدگمانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل و دین شکسته آنگه هوسم ز نام جویی</p></div>
<div class="m2"><p>سر و پا برهنه آنگه سخنم ز مرزبانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ادبم مکن که خوردم، خللم مبین که خاکم</p></div>
<div class="m2"><p>ببر از نهاد طبعم دو دلی و ده زبانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حرم تو آمد این دل ز حسد نگاهدارش</p></div>
<div class="m2"><p>که فرشته با شیاطین نکند هم آشیانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز گناه و عذر بگذر بنواز و رحمتی کن</p></div>
<div class="m2"><p>به خجالتی که بینی به ضرورتی که دانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به طفیل طاعتِ تو تن خویش زنده دارم</p></div>
<div class="m2"><p>چو نباشد این سعادت نه من و نه زندگانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هه ممکن الوجودی رقمِ هلاک دارد</p></div>
<div class="m2"><p>تو که واجب الوجودی ابدالابد بمانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر از نظامی آمد گنهی عفوش گردان</p></div>
<div class="m2"><p>که کس ایمنی ندارد ز قضای آسمانی</p></div></div>
<div class="c"><p>مِنْغزلیّاته رحمة اللّه علیه</p></div>
<div class="b" id="bn23"><div class="m1"><p>هزار بار به جان آمد ار چه کار مرا</p></div>
<div class="m2"><p>نگشت عشق تو الا یکی هزار مرا</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn24"><div class="m1"><p>خوشا جانی کزو جانی بیاسود</p></div>
<div class="m2"><p>نه درویشی که سلطانی بیاسود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به عمر خود پریشانی نبیند</p></div>
<div class="m2"><p>دلی کز وی پریشانی بیاسود</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn26"><div class="m1"><p>نفس اگر پیر شود سهل نباشد زان رو</p></div>
<div class="m2"><p>کاژدها گردد ماری که قوی‌تر گردد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو خدا را شو اگر جمله جهان گیرد آب</p></div>
<div class="m2"><p>به خدا گر سر مویی قدمت تر گردد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn28"><div class="m1"><p>یاوری کن همه را تا همه یار تو شوند</p></div>
<div class="m2"><p>تو همه یار کشی با تو که یاور گردد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن چنان زی که اگر نیز دروغی گویی</p></div>
<div class="m2"><p>راست گویان جهان را ز تو باور گردد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn30"><div class="m1"><p>برمیاور سراز آن سان که دروغ انگارند</p></div>
<div class="m2"><p>هر کجا راستی‌ای از تو مشهر گردد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn31"><div class="m1"><p>گر تو خواهی که دل و دین به سلامت ببری</p></div>
<div class="m2"><p>خاک پای همه شو تا که بیابی مقصود</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn32"><div class="m1"><p>شبی‌تیره است وره مشکل جنیبت را عنان درکش</p></div>
<div class="m2"><p>زمانی رختِ هستی را به خلوتگاه‌جان‌درکش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طریقش بی قدم می‌رو، جمالش بی بصر می‌بین</p></div>
<div class="m2"><p>حدیثش بی زبان می‌گو،شرابش بی‌دهان‌درکش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نظامی این چه اسرار است کز خاطر برون دادی</p></div>
<div class="m2"><p>کسی رمزت نمی‌داند زبان درکش،‌زبان‌درکش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چوخاص‌الخاص‌او گشتی ز صورت پای بیرون نه</p></div>
<div class="m2"><p>هزاران شربتِ معنی به یک دم رایگان درکش</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn36"><div class="m1"><p>هم باز شود این در، هم روز شود این شب</p></div>
<div class="m2"><p>دلبر نه چنین ماند دلدار شود روزی</p></div></div>
<div class="c"><p>رباعی</p></div>
<div class="b" id="bn37"><div class="m1"><p>چون نیست امیدِ عمر از شام به چاشت</p></div>
<div class="m2"><p>باری همه تخم نیکویی باید کاشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون عالم را به کس نخواهند گذاشت</p></div>
<div class="m2"><p>باری دل دوستان نگه باید داشت</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn39"><div class="m1"><p>آن را که غمی بود که نتواند گفت</p></div>
<div class="m2"><p>غم از دل خود به گفت، نتواند رُفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این طرفه گلی نگر که ما را بشکفت</p></div>
<div class="m2"><p>نه رنگ توان نمود و نه بوی نهفت</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn41"><div class="m1"><p>گرآه کشم کجاست فریاد رسی</p></div>
<div class="m2"><p>ور صبر کنم عمر نمانده است بسی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر یادِ تو می‌زنم به هر دم نفسی</p></div>
<div class="m2"><p>کس را ندهد خدای سودای کسی</p></div></div>
<div class="c"><p>فی التوحید مِنْمثنوی مخزن الاسرار</p></div>
<div class="b" id="bn43"><div class="m1"><p>ای همه هستی ز تو پیدا شده</p></div>
<div class="m2"><p>خاکِ ضعیف از تو توانا شده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هستی تو صورت و پیوند نه</p></div>
<div class="m2"><p>تو به کس و کس به تو مانند نه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زیرنشین عَلَمت کاینات</p></div>
<div class="m2"><p>ما به تو قائم چو تو قائم به ذات</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آنچه تغیر نپذیرد تویی</p></div>
<div class="m2"><p>آنکه نمرده است ونمیرد تویی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ما همه فانی و بقا بس تراست</p></div>
<div class="m2"><p>ملک تعالی و تقدس تراست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر که نه گویا به تو خاموش به</p></div>
<div class="m2"><p>هر چه نه یاد تو فراموش به</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بی بدل است آنکه تو آویزیش</p></div>
<div class="m2"><p>بی دیت است آنکه تو خون ریزیش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای به ازل بوده و نابوده ما</p></div>
<div class="m2"><p>وی به ابد زنده و فرسوده ما</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اول و آخر به وجود و حیات</p></div>
<div class="m2"><p>هست کن و نیست کن کاینات</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>با جبروتت که دو عالم کم است</p></div>
<div class="m2"><p>اول ما آخر ما یک دم است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چارهٔ ما ساز که بی یاوریم</p></div>
<div class="m2"><p>گر تو برانی به که رو آوریم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حلقه زن خانه فروش توایم</p></div>
<div class="m2"><p>چون درِ تو حلقه به گوش توایم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>قافله شد واپسیِ ما ببین</p></div>
<div class="m2"><p>ای کس ما بی کسی ما ببین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر که پناهیم تویی بی نظیر</p></div>
<div class="m2"><p>در که گریزیم تویی دستگیر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>جز درِ تو قبله نخواهیم ساخت</p></div>
<div class="m2"><p>گر ننوازی تو، که خواهد نواخت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دست چنین پیش که دارد که ما</p></div>
<div class="m2"><p>زاری ازین بیش، که دارد که ما</p></div></div>
<div class="c"><p>فی النصیحة و الموعظه</p></div>
<div class="b" id="bn59"><div class="m1"><p>این چه زبان، این چه زبان دانی است</p></div>
<div class="m2"><p>گفته و ناگفته پشیمانی است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نقد غریبی و جهان شهر تست</p></div>
<div class="m2"><p>نقد جهان یک به یک از بهرتست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>با همه چون خاک زمین پست باش</p></div>
<div class="m2"><p>وز همه چون باد تهی دست باش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یک درم است آنچه بدان بنده‌ای</p></div>
<div class="m2"><p>یک نفس است آنچه بدان زنده‌ای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هرچه درین پرده نه میخی است</p></div>
<div class="m2"><p>بازی این لعبت زرنیخی است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سایهٔ خورشید سواران طلب</p></div>
<div class="m2"><p>رنج خود و راحت یاران طلب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>حکم چو بر عاقبت اندیشی است</p></div>
<div class="m2"><p>محتشمی بندهٔ درویشی است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>حجله همان است که عذراش بست</p></div>
<div class="m2"><p>بزم همان است که وامق نشست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>حجله و بزم اینک تنها شده</p></div>
<div class="m2"><p>وامقش افتاده و عذرا شده</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>صحبت گیتی که تمنا کند</p></div>
<div class="m2"><p>با که وفا کرده که با ما کند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هر ورقی چهرهٔ آزاده‌ایست</p></div>
<div class="m2"><p>هر قدمی فرق ملک زاده‌ایست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گفته گروهی که به صحرا درند</p></div>
<div class="m2"><p>کی خنک آنان که به دریا درند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>وانکه به دریا در سختی کش است</p></div>
<div class="m2"><p>نعل در آتش که بیابان خوش است</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بیشتر از مرتبهٔ عاقلی</p></div>
<div class="m2"><p>غافلی‌ای بود و خوش آن غافلی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چون نظر عقل به غایت رسید</p></div>
<div class="m2"><p>دولت شادی به نهایت رسید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>غافل منشین ورقی می‌خراش</p></div>
<div class="m2"><p>گر ننویسی قلمی می‌تراش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>با نفس هر که در آمیختم</p></div>
<div class="m2"><p>مصلحت آن بود که بگریختم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هست در این دایرهٔ لاجورد</p></div>
<div class="m2"><p>مرتبهٔ مرد به مقدارِ مرد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>لعبت بازی پسِ این پرده هست</p></div>
<div class="m2"><p>ورنه بر او این همه لعبت که بست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دیده و دل محرمِ این پرده ساز</p></div>
<div class="m2"><p>تا چه برون آید از این پرده باز</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ختم سفیدی و سیاهی تویی</p></div>
<div class="m2"><p>محرمِ اسرارِ الهی تویی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>راه دو عالم که دو منزل شده است</p></div>
<div class="m2"><p>نیم ره یک نفس دل شده است</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تن چه بود ریزشِ مشتی گل است</p></div>
<div class="m2"><p>هم دل و هم دل که سخن در دل است</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بندهٔ دل باش که سلطان شوی</p></div>
<div class="m2"><p>خواجهٔ عقل و ملک جان شوی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سرو شو از بند خود آزاد باش</p></div>
<div class="m2"><p>شمع شو از خوردن خود شاد باش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بردرِ او شو که ازینان به اوست</p></div>
<div class="m2"><p>روزی ازو خواه که روزی ده اوست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هرچه خلاف آمدِ عادت بود</p></div>
<div class="m2"><p>قافله سالارِ سعادت بود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گر به خورش بیش کسی زیستی</p></div>
<div class="m2"><p>هر که بسی خورد بسی زیستی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>خاکِ تو آمیختهٔ رنج‌هاست</p></div>
<div class="m2"><p>در دلِ این خاک بسی گنج‌هاست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زآمدنت رنگ چرا چون می‌است</p></div>
<div class="m2"><p>کامدنی را شدنی در پی است</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>راه عدم را نپسندیده‌ای</p></div>
<div class="m2"><p>زانکه به چشم دگران دیده‌ای</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جملهٔ دنیا ز کهن تا به نو</p></div>
<div class="m2"><p>چون گذران است نیرزد دو جو</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>پای درین بحر نهادن که چه</p></div>
<div class="m2"><p>بار درین موج گشادن که چه</p></div></div>
<div class="c"><p>وله ایضاً قُدِّسَ سِرُّه</p></div>
<div class="b" id="bn92"><div class="m1"><p>آنچه درین مائدهٔ خرگهی است</p></div>
<div class="m2"><p>کاسهٔ آلوده و خون تهی است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هر که درو دید دهانش بدوخت</p></div>
<div class="m2"><p>هر که از او گفت زبانش بسوخت</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>هیچ نه در محمل و چندین جرس</p></div>
<div class="m2"><p>هیچ نه در سفره و چندین مگس</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دور فلک همچو تو بس یار گشت</p></div>
<div class="m2"><p>دست قوی تر ز تو بسیار گشت</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>خواه بنه مایه و خواهی بباز</p></div>
<div class="m2"><p>کانچه دهند از تو ستانند باز</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>هر نفس این پردهٔ چابک رقیب</p></div>
<div class="m2"><p>بازیی از پرده برآرد غریب</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نطع پر از زخمه و رقاص نه</p></div>
<div class="m2"><p>بحر پر از گوهر و غواص نه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>هر دم ازین باغ بری می‌رسد</p></div>
<div class="m2"><p>تازه‌تر از تازه‌تری می‌رسد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>رشتهٔ دل‌ها که در این گوهر است</p></div>
<div class="m2"><p>مرسله از مرسله زیباتر است</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>راهروان کز پسِ یکدیگرند</p></div>
<div class="m2"><p>طایفه از طایفه والاترند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>عقل شرف جز به معانی نداد</p></div>
<div class="m2"><p>قدر به پیری و جوانی نداد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>گرچه جوانی همه فرزانگی است</p></div>
<div class="m2"><p>هم نه یکی شعبه ز دیوانگی است؟</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>می‌کشدت دیو تو افکنده‌ای</p></div>
<div class="m2"><p>دست بزن مرده نه‌ای زنده‌ای</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>شیر شو از گربهٔمطبخ مترس</p></div>
<div class="m2"><p>طلق شو از آتش دوزخ مترس</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>باده تو خوردی گنه دهر چیست</p></div>
<div class="m2"><p>جور تو کردی گنه دهر چیست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>گر درِ دولت زنی افتاده شو</p></div>
<div class="m2"><p>وز گرهٔ کار جهان ساده شو</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>معرفتی در گل آدم نماند</p></div>
<div class="m2"><p>اهل دلی در همه عالم نماند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>دشمنِ دانا که غم جان بود</p></div>
<div class="m2"><p>بهتر از آن دوست که نادان بود</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>کیسه برانند در این رهگذر</p></div>
<div class="m2"><p>هرکه تهی کیسه‌تر آسوده‌تر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>غارتیانی که رهِ دل زنند</p></div>
<div class="m2"><p>راه به نزدیکی منزل زنند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>تا به جهان در نفسی می‌زنی</p></div>
<div class="m2"><p>به که درِ عشق کسی می‌زنی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>جهد بدان کن که خدا را شوی</p></div>
<div class="m2"><p>خود نپرستی و هوا را شوی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>خاک دلی شو که وفایی دروست</p></div>
<div class="m2"><p>وز گِلِ انصاف صفایی دروست</p></div></div>
<div class="c"><p>ولَهُ رحمة اللّه علیه مِنْمثنوی خسروشیرین فِی الاستدلال و الاستدراک</p></div>
<div class="b" id="bn115"><div class="m1"><p>خبر داری که سیاحان افلاک</p></div>
<div class="m2"><p>چرا گردند گردِ خطّهٔ خاک</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>درین محرابگه معبودشان کیست</p></div>
<div class="m2"><p>وزین آمد شدن مقصودشان کیست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چه می‌خواهند از این محمل کشیدن</p></div>
<div class="m2"><p>چه می‌جویند ازین منزل بریدن</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چرا این ثابت است آن منقلب نام</p></div>
<div class="m2"><p>که گفت این را بجنب آن را بیارام</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>مرا حیرت بدان آورد صد بار</p></div>
<div class="m2"><p>که بندم اندرین بتخانه زنار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ولی چون کرد حیرت تیزگامی</p></div>
<div class="m2"><p>عنایت بانگ برزد کی نظامی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>مشو فتنه بدین بت‌ها که هستند</p></div>
<div class="m2"><p>که این بت‌ها نه خود را می‌پرستند</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>همه هستند سرگردان چه پرگار</p></div>
<div class="m2"><p>پدید آرندهٔ خود را طلبکار</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چو ابراهیم با بتخانه می‌ساز</p></div>
<div class="m2"><p>ولی بتخانه را از بت بپرداز</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>نظر بر بت نهی صورت پرستی</p></div>
<div class="m2"><p>قدم بر بت نهی رفتی و رستی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نموداری که از مه تا به ماهی است</p></div>
<div class="m2"><p>طلسمی بر سر گنج الهی است</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>طلسم بسته را با رنج یابی</p></div>
<div class="m2"><p>چو بشکستی به زیرش گنج یابی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>مرا بر سیرِ گردون رهبری نیست</p></div>
<div class="m2"><p>چرا کان سیرِ دانم سرسری نیست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اگر دانستنی بودی خود این راز</p></div>
<div class="m2"><p>یکی زین نقش‌ها در دادی آواز</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ازین گردنده گنبدهایِ پرنور</p></div>
<div class="m2"><p>بجز گردش چه شاید دید از دور</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>ولی در عقل هر داننده‌ای هست</p></div>
<div class="m2"><p>که با گردنده، گرداننده‌ای هست</p></div></div>
<div class="c"><p>مِنْمثنوی لیلی و مجنون در نصیحت گوید</p></div>
<div class="b" id="bn131"><div class="m1"><p>ای ناظر نقش آفرینش</p></div>
<div class="m2"><p>بردار خلل ز راهِ بینش</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>کاین هفت حصارِ برکشیده</p></div>
<div class="m2"><p>بر هزل نباشد آفریده</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>هر ذره که هست اگر غبار است</p></div>
<div class="m2"><p>در پردهٔ مملکت به کار است</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>در راه تو هر که با وجود است</p></div>
<div class="m2"><p>مشغول پرستش و سجود است</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>کارِ من و تو بدین درازی</p></div>
<div class="m2"><p>کوتاه کنم که نیست بازی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>به در نگریم و راز جوییم</p></div>
<div class="m2"><p>سر رشتهٔ کار باز جوییم</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>آن آینه در جهان که دیده است</p></div>
<div class="m2"><p>کاول نه به صیقلی رسیده است</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>هر نقش بدیع کایدت پیش</p></div>
<div class="m2"><p>جز مبدع او ازو میندیش</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>زین هفت پرند پرنیان رنگ</p></div>
<div class="m2"><p>گر پای برون نهی خوری سنگ</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>سر رشتهٔ راز آفرینش</p></div>
<div class="m2"><p>دیدن نتوان به چشم بینش</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>این رشته قضا نه آن چنان بافت</p></div>
<div class="m2"><p>کو را سر رشته‌ای توان یافت</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>در پردهٔ راز آسمانی</p></div>
<div class="m2"><p>سریست ز چشم ما نهانی</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>اندیشه چو سر به خط رساند</p></div>
<div class="m2"><p>جز باز پس آمدن نداند</p></div></div>
<div class="c"><p>فِی التوحید مِنْمثنوی هفت پیکر</p></div>
<div class="b" id="bn144"><div class="m1"><p>ای جهان دیده بود خویش از تو</p></div>
<div class="m2"><p>هیچ بودی نبوده پیش از تو</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>در بدایت، بدایت همه چیز</p></div>
<div class="m2"><p>در نهایت نهایت همه نیز</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>سازمند از تو گشته کارِ همه</p></div>
<div class="m2"><p>ای همه و آفریدگار همه</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>هرچه هست از دقیقه‌های علوم</p></div>
<div class="m2"><p>با یکایک نهفته‌های نجوم</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>خواندم و سر هر ورق جستم</p></div>
<div class="m2"><p>چون ترا یافتم ورق شستم</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>همه را روی در خدادیدم</p></div>
<div class="m2"><p>وی خدایِ همه ترا دیدم</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>چون ز عهدِ جوانی از درِ تو</p></div>
<div class="m2"><p>به درِ کس نرفتم از برِ تو</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>همه را بر درم فرستادی</p></div>
<div class="m2"><p>من نمی‌خواستم تو می‌دادی</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چه سخن کاین سخن خطاست همه</p></div>
<div class="m2"><p>تو مرایی جهان مراست همه</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>غرض آن به که از تو می‌جویم</p></div>
<div class="m2"><p>سخن آن به که با تو می‌گویم</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>بازگویم به خلق خوار شوم</p></div>
<div class="m2"><p>با تو گویم بزرگوار شوم</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>هرکه خود را چنانکه بود شناخت</p></div>
<div class="m2"><p>تا ابد سر به زندگی افراخت</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>فانی آن شد که نقش خویش نخواند</p></div>
<div class="m2"><p>هر که این نقش خواند، باقی ماند</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>هست خوشنود هر کس از دل خویش</p></div>
<div class="m2"><p>نکند کس عمارتِ گلِ خویش</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>هر کسی در بهانه تیز هش است</p></div>
<div class="m2"><p>کس نگوید که دوغ من ترش است</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>آن چنان زی که گر رسد خواری</p></div>
<div class="m2"><p>نخوری طعن دشمنان باری</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>این نگوید سرآمد آفاتش</p></div>
<div class="m2"><p>وان نخندد که هان مکافاتش</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>آنکه رفق تواش به یاد بود</p></div>
<div class="m2"><p>به از آن کز غم تو شاد بود</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>آدمی نز پی علف خواری است</p></div>
<div class="m2"><p>از پی زیرکی و هشیاری است</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>سگ بدان آدمی شرف دارد</p></div>
<div class="m2"><p>که چو خر دیده بر علف دارد</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>هر که بدخو بود گهِ زادن</p></div>
<div class="m2"><p>هم بدان خو بود به جان دادن</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>نشندی که آن حکیم چه گفت</p></div>
<div class="m2"><p>خوابِ خوش دید هر که او خوش خفت</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>چون گل آن به که خوی خوش داری</p></div>
<div class="m2"><p>تادر آفاق بویِ خوش داری</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>ابلهی بین که از پیِ سنگی</p></div>
<div class="m2"><p>دوست با دوست می‌کند جنگی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>تو به زر چشم روشنی و بد است</p></div>
<div class="m2"><p>چشم روشن کن جهان خرد است</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>آنچه زو بگذری و بگذاری</p></div>
<div class="m2"><p>چند بندی و چند برداری</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>نیست چون کار بر مراد کسی</p></div>
<div class="m2"><p>بی مرادی به از مراد بسی</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>گر مریدی چنانکه رانندت</p></div>
<div class="m2"><p>به رهی رو که پیر خوانندت</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>از مریدانِ بی مراد مباش</p></div>
<div class="m2"><p>در توکل بداعتقاد مباش</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>یک ره از دیده‌ها فرامش کن</p></div>
<div class="m2"><p>محرم راز گرد و خامش کن</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>تا بدانی که هرچه می‌دانی</p></div>
<div class="m2"><p>غلطی یا غلط همی خوانی</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>بنگر اول که آمدی زنخست</p></div>
<div class="m2"><p>آنچه داری چه داشتی به درست</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>آن بری زان دو مشک ناوردی</p></div>
<div class="m2"><p>کاولین روز با خود آوردی</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>کوش تا وامِ جمله باز دهی</p></div>
<div class="m2"><p>تا تو مانی و یک ستور تهی</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>راهرو را بسیجِ ره شرط است</p></div>
<div class="m2"><p>ناقه راندن ز بیم گه شرط است</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>صحبتی جوی کز نکونامی</p></div>
<div class="m2"><p>در تو آرد نکو سرانجامی</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>همنشینی که نافه خوی بود</p></div>
<div class="m2"><p>خوب تر زانکه یافه گوی بود</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>رقص مرکب مبین که رهواراست</p></div>
<div class="m2"><p>راه بین تا چگونه دشوار است</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>آهنت گرچه آهنیست نفیس</p></div>
<div class="m2"><p>راه، سنگ است و سنگ مغناطیس</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>آن قدر بار بر ستور آویز</p></div>
<div class="m2"><p>که نماند برین گریوهٔ تیز</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>چون رسد تنگی ای ز دورِدورنگ</p></div>
<div class="m2"><p>راه بر دل فراخ دار نه تنگ</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>بس گره کو کلید پنهانی است</p></div>
<div class="m2"><p>بس درشتی که در وی آسانی است</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>هرکه ز آموختن ندارد ننگ</p></div>
<div class="m2"><p>دُرّ برآرد ز آب و لعل از سنگ</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>وانکه دانش نباشدش روزی</p></div>
<div class="m2"><p>ننگ دارد ز دانش آموزی</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>سگ به دانش چو راست رشته بود</p></div>
<div class="m2"><p>آدمی شاید ار فرشته بود</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>آب حیوان نه آب حیوان است</p></div>
<div class="m2"><p>جان با عقل و عقل با جان است</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>ره به جان ده که کالبد کند است</p></div>
<div class="m2"><p>بار کم کن که بارگی تند است</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>مرده‌ای را که حال بد باشد</p></div>
<div class="m2"><p>میلِ جان سویِ کالبد باشد</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>وانکه داند که اصلِ جانش چیست</p></div>
<div class="m2"><p>جان او بی جسد تواند زیست</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>خانه را خوار کن خورش را خُرد</p></div>
<div class="m2"><p>از جهان، جان چنین توانی برد</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>در دو چیز است رستگاریِ مرد</p></div>
<div class="m2"><p>آنکه بسیار داد و اندک خورد</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>حکم هر نیک و بد که در دهر است</p></div>
<div class="m2"><p>زهر در نوش و نوش در زهر است</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>کیست کو بر زمین فرازد تخت</p></div>
<div class="m2"><p>کآخرش هم زمین نگیرد سخت</p></div></div>
<div class="c"><p>و له ایضاً رحمة اللّه علیه فی المثنویّ اسکندرنامه</p></div>
<div class="b" id="bn197"><div class="m1"><p>دو در دارد این باغِ آراسته</p></div>
<div class="m2"><p>در و بند ازین هر دو برخاسته</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>درآ از در باغ و بنگر تمام</p></div>
<div class="m2"><p>ز دیگر در باغ بیرون خرام</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>اگر زیرکی با گلی خو مگیر</p></div>
<div class="m2"><p>که باشد به جا ماندنش ناگزیر</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>نه‌ایم آمده از پیِ دلخوشی</p></div>
<div class="m2"><p>مگو کز پی رنج و سختی کشی</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>خران را کسی در عروسی نخواند</p></div>
<div class="m2"><p>مگر وقت آن کاب و هیزم نماند</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>درین دم که داری به شادی بسیج</p></div>
<div class="m2"><p>که آینده و رفته هیچ است هیچ</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>چنین است رسم این گذرگاه را</p></div>
<div class="m2"><p>که دارد به آمد شد این راه را</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>یکی رادرآرد به هنگامه تیز</p></div>
<div class="m2"><p>یکی را ز هنگامه گوید که خیز</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>اگر شاه ملک است و گر ملک شاه</p></div>
<div class="m2"><p>همه راه رنج است و با رنج راه</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>چو اندوهی آمد مشو ناسپاس</p></div>
<div class="m2"><p>ز محکم‌تر اندوهی اندر هراس</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>ز کم خوارگی کم شود رنج مرد</p></div>
<div class="m2"><p>نه بسیار ماند آنکه بسیار خورد</p></div></div>