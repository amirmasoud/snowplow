---
title: >-
    بخش ۶۳ - رضی غزنوی قُدِّسَ سِرُّه العزیز
---
# بخش ۶۳ - رضی غزنوی قُدِّسَ سِرُّه العزیز

<div class="n" id="bn1"><p>وهُوَ شیخ رضی الدّین علی لالاخلف الصدق شیخ سعید بن عبدالجلیل لالای غزنوی است و شیخ سعید مذکور عم زادهٔ جناب حکیم مشهور سنائی غزنوی است و شیخ علی لالا مرید حضرت شیخ نجم الدین کبری. گویند در پانزده سالگی شیخ نجم الدّین را به خواب دید و به طلب او سالها گردید. به خدمت صد شیخ، زیاده رسید. آخر جناب شیخ نجم الدّین را دریافت و به امر او به هندوستان رفته به خدمت شیخ ابورضای رتن، به قولی از حواریون حضرت عیسی و به قولی از اصحاب جناب ختمی مآب بوده و یکهزار و چهارده سال عمر نموده. تفصیل این اجمال در کتب این طایفه تصریح و تصحیح یافته است. غرض، شیخ از اعاظم مشایخ بود و از صد و شصت و چهار شیخ، خرقهٔ تبرک گرفته. آخرالامر در سنهٔ ۶۴۳ به حق پیوست. مدفنش در حوالی اصفهان و به گنبد لالا مشهور است. آن جناب گاهی خیال نظمی می‌فرموده‌اند. این دو رباعی از ایشان است:</p></div>
<div class="b" id="bn2"><div class="m1"><p>عشق ارچه بسی خون جگرها دهدت</p></div>
<div class="m2"><p>می‌خور چو صدف که هم گهرها دهدت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند که بار عشق باری است عظیم</p></div>
<div class="m2"><p>چون شاخ بکش یار، که برها دهدت</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn4"><div class="m1"><p>هم جان به هزار دل گرفتار تواست</p></div>
<div class="m2"><p>هم دل به هزار جان خریدار تو است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر طلبت نه خواب دارد نه قرار</p></div>
<div class="m2"><p>هرکس که در آرزوی دیدار تواست</p></div></div>