---
title: >-
    بخش ۱۰۰ - عراقی همدانی قُدِّسَ سِرُّه
---
# بخش ۱۰۰ - عراقی همدانی قُدِّسَ سِرُّه

<div class="n" id="bn1"><p>نامش فخرالدین ابراهیم. گفته‌اند که او و شمس الدین تبریزی در چلّهٔ خانهٔ رکن الدین سجاسی اربعین به سر می‌آوردند وبرخی گفته‌اند به شیخ شهاب الدین سهروردی رسیده و ارادت خلیفهٔ آن جناب شیخ بهاء الدین زکریای ملتانی را گزیده. تحقیق آن است که مرید بهاءالدین زکریا وبه مصاهرت آن جناب اختصاص یافته است. غرض، شیخی است مجرد و پیری است موحد، عارفی عاشق، عاشقی صادق. سلوکش محبوبانه و سیرش مجذوبانه، عشقش بر عقلش غالب و ادراک ظهورات صفات را ازمظاهر طالب. جانش پرشور و دلش پرنور. سینه‌اش مخزن اسرار و دیده‌اش مطلع انوار. از لمعاتش لوامع حقیقت لامع و از مطالع ابیاتش طوالع اسرار طریقت طالع. وفاتش در سنهٔ ۶۸۸ در دمشق شام و در زیر پای محی الدین عربی‌اش مقام و این از اشعار آن جناب است:</p></div>
<div class="c"><p>مِنْرسالهٔ موسوم به ده فصل</p></div>
<div class="b" id="bn2"><div class="m1"><p>از جمالت نمی‌شکیبد دل</p></div>
<div class="m2"><p>می‌برد عقل و می‌فریبد دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقت ای دوست می‌کند پیوست</p></div>
<div class="m2"><p>حلقه در گوش عاشقانِ الست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقان تو پاک بازانند</p></div>
<div class="m2"><p>صیدِ عشق تو شاهبازانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای غم تو مجاورِ دلِ من</p></div>
<div class="m2"><p>وز دو عالم غمِ تو حاصل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تادلم هست مبتلای تو باد</p></div>
<div class="m2"><p>دایماً بستهٔ بلای تو باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده را دیدن تو می‌باید</p></div>
<div class="m2"><p>اگرم قصد جان کنی شاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل ما را فراغت از جان است</p></div>
<div class="m2"><p>زندگانی ما به جانان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتشِ عشق در دل ما جو</p></div>
<div class="m2"><p>عاشقان ضعیف را واجو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقان را ز جان گرفته ملال</p></div>
<div class="m2"><p>خونشان بر تو همچو شیر حلال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فارغی از درون صاحب درد</p></div>
<div class="m2"><p>مکن ای دوست هرچه بتوان کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخ به ما می‌نما و جان می‌بخش</p></div>
<div class="m2"><p>بر دل و جان عاشقان می بخش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست عشق آتشی که شعلهٔ آن</p></div>
<div class="m2"><p>سوزد از دل حجابِ هرحدثان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بسوزد هوای پیچا پیچ</p></div>
<div class="m2"><p>او بماند جز او نماند هیچ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عشق و اوصاف کردگار یکیست</p></div>
<div class="m2"><p>عاشق و عشق و حسن یار یکیست</p></div></div>
<div class="c"><p>حکایت حجّة الاسلام امام محمد غزالی قُدِّسَ سِرُّه</p></div>
<div class="b" id="bn16"><div class="m1"><p>شیخ الاسلام امام غزالی</p></div>
<div class="m2"><p>آن صفابخش حالی و قالی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>والهٔ حسن ماهرویان بود</p></div>
<div class="m2"><p>در رهِ عشقِ دوست پویان بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>او همی شد سواره اندر ری</p></div>
<div class="m2"><p>از مریدان صدش فزون در پی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلبری دید همچو ماه تمام</p></div>
<div class="m2"><p>که برون آمد از درِ حمام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شیخ را چشم چون بر آن افتاد</p></div>
<div class="m2"><p>صورتِ دوست دید باز استاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شده مردم به شیخ در، نگران</p></div>
<div class="m2"><p>شیخ در رویِ آن پری حیران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صوفیان جمله منفعل گشتند</p></div>
<div class="m2"><p>همه بگذاشتند و بگذشتند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لیک مردی که بود غاشیه دار</p></div>
<div class="m2"><p>شیخ را گفت بگذر وبگذار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیدن صورت از تو لایق نیست</p></div>
<div class="m2"><p>شرمت از این همه خلایق نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شیخ گفتش مگوی هیچ سَخُن</p></div>
<div class="m2"><p>رُؤیَةُ الْحُسْنِ رُؤیَةُ الأَعْیُن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عاشقانی که مست و مدهوشند</p></div>
<div class="m2"><p>باده از جامِ حسن می‌نوشند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز اندرون غافل است بیرون بین</p></div>
<div class="m2"><p>رویِ لیلی به چشم مجنون بین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگرت هست قوت مردان</p></div>
<div class="m2"><p>اینک اسب و سلاح و این میدان</p></div></div>
<div class="c"><p>مِنْغزلیّاته قُدِّسَ سِرُّه</p></div>
<div class="b" id="bn29"><div class="m1"><p>ساز طرب عشق چه داند که چه ساز است</p></div>
<div class="m2"><p>کز زخمهٔ او نُه فلک اندر تک و تاز است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عشق است که هر دم به دگر رنگ برآید</p></div>
<div class="m2"><p>ناز است یکی جای و دگر جای نیاز است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>درخرقهٔ عاشق چو درآید همه سوز است</p></div>
<div class="m2"><p>در کسوتِ معشوق چو آمد همه ساز است</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn32"><div class="m1"><p>رخ تو برقع چشم من است لیک چه سود</p></div>
<div class="m2"><p>که برقع از رخ تو بر نمی‌توان انداخت</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn33"><div class="m1"><p>به نور طلعت تو یافتم وجودِ ترا</p></div>
<div class="m2"><p>به آفتاب توان یافت کافتاب کجاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عشق شوری در نهاد ما نهاد</p></div>
<div class="m2"><p>جان ما در بوتهٔ سودا نهاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفتگویی در زبانِ ما فکند</p></div>
<div class="m2"><p>جستجویی در درون ما نهاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دمبدم در هر لباسی رخ نمود</p></div>
<div class="m2"><p>لحظه لحظه جای دیگر پا نهاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر مثال خویشتن حرفی نوشت</p></div>
<div class="m2"><p>نام آن حرف آدم و حوا نهاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حسن خود بر دیدهٔ خود جلوه داد</p></div>
<div class="m2"><p>منتی بر عاشق شیدا نهاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هم به چشم خود جمال خود بدید</p></div>
<div class="m2"><p>تهمتی بر چشم نابینا نهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا کمال علم او ظاهر شود</p></div>
<div class="m2"><p>این همه اسرار بر صحرا نهاد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn41"><div class="m1"><p>نخستین باده کاندر جام کردند</p></div>
<div class="m2"><p>ز چشمِ مستِ ساقی وام کردند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به گیتی هر کجا درد دلی بود</p></div>
<div class="m2"><p>به هم کردند و عشقش نام کردند</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn43"><div class="m1"><p>غمت هر لحظه جانی خواهد ازمن</p></div>
<div class="m2"><p>چه انصاف است چندین جان که دارد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نشان عشق می‌خواهی عراقی</p></div>
<div class="m2"><p>ببین تا چشمِ خون افشان که دارد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn45"><div class="m1"><p>هم دیدهٔ او باید تا حسن رخش بیند</p></div>
<div class="m2"><p>آنجا که جمال اوست ابصار نمی‌گنجد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جانم درِ دل می‌زد دل گفت برو کاین دم</p></div>
<div class="m2"><p>با یار در این خلوت دیار نمی‌گنجد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn47"><div class="m1"><p>با عاشقانِ شیدا سلطان کجا برآید</p></div>
<div class="m2"><p>در پیش آشنایان بیگانه‌ای چه سنجد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از صد هزار خرمن یکدانه است عالم</p></div>
<div class="m2"><p>با صد هزار عالم پس دانه‌ای چه سنجد</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn49"><div class="m1"><p>مرا مکش که نیازِ منت به کار آید</p></div>
<div class="m2"><p>چو من نباشم حسن تو بر که ناز کند</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn50"><div class="m1"><p>بروم ز چشم مستش نظری به وام گیرم</p></div>
<div class="m2"><p>که به آن نظر ببینم رخ خوب لاله رنگش</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn51"><div class="m1"><p>پیوسته شد چو شبنم جانم به آفتاب</p></div>
<div class="m2"><p>شاید که آن زمان ز اناالشمس دم زنم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آری چو آفتاب بیفتد در آینه</p></div>
<div class="m2"><p>گوید هر آینه که همه مهرِ روشنم</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn53"><div class="m1"><p>اگر جهان همه زیر و زبر شود ز غمت</p></div>
<div class="m2"><p>ترا چه غم که تو خو کرده‌ای به تنهایی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حجابِ روی تو هم روی تست در همه حال</p></div>
<div class="m2"><p>نهانی از همه عالم ز بس که پیدایی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عروسِ حسن ترا هیچ در نمی‌یابد</p></div>
<div class="m2"><p>به گاه جلوه مگر دیدهٔ تماشایی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn56"><div class="m1"><p>از آن خوشست چو نی ناله‌ام به گوش جهان</p></div>
<div class="m2"><p>که هیچ دم نزنم تا توام نه بنوازی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn57"><div class="m1"><p>به قمارخانه رفتم همه پاکباز دیدم</p></div>
<div class="m2"><p>چو به صومعه رسیدم همه زاهد ریایی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn58"><div class="m1"><p>عراقی طالب درد است و آن هم</p></div>
<div class="m2"><p>به بویِ اینکه درمانش تو باشی</p></div></div>
<div class="c"><p>رباعی</p></div>
<div class="b" id="bn59"><div class="m1"><p>هرچند که دل را غمِ عشق آیین است</p></div>
<div class="m2"><p>چشم است که آفتِ دلِ مسکین است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>من معترفم که شاهد دل معنی است</p></div>
<div class="m2"><p>اما چه کنم که چشم صورت بین است</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn61"><div class="m1"><p>ره گم شده رهنمای می‌باید بود</p></div>
<div class="m2"><p>در بند و گره گشای می‌باید بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یکسال و هزار سال می‌باید زیست</p></div>
<div class="m2"><p>یک جای و هزار جای می‌باید بود</p></div></div>