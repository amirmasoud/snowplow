---
title: >-
    بخش ۶۵ - رضی آرتیمانی قُدِّسَ سِرُّه
---
# بخش ۶۵ - رضی آرتیمانی قُدِّسَ سِرُّه

<div class="n" id="bn1"><p>اسم شریفش میرزا محمد رضی از سادات رفیع الدرجات آرتیمان، مِنْمحال توسرکان مِنْتوابع همدان. سیدی است صاحب ذوق و حال و عارفی باافضال. در معارف الهیه، مسلم افاق و در مدارج حقانیه درعالم، طاق. معاصر شاه عباس ماضی صفوی و والد میرزا ابراهیم متخلص به ادهم است که از شعراست. یک هزار بیت دیوان دارند. تیمناً و تبرّکاً برخی از اشعارش نوشته می‌شود:</p></div>
<div class="c"><p>مِنْقصایده فی المواجید</p></div>
<div class="b" id="bn2"><div class="m1"><p>بس که بر سر زدم ز فرقت یار</p></div>
<div class="m2"><p>کارم از دست رفت و دست از کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشربم ننگ و عشقِ شورانگیز</p></div>
<div class="m2"><p>مرکبم لنگ و راه ناهموار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که در عشق دم زنی به دروغ</p></div>
<div class="m2"><p>خویش را هرزه می‌کنی آزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این قدر شور نیست در سرِ تو</p></div>
<div class="m2"><p>که پریشان شود ترا دستار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنده زان رو کنی چو بی دردان</p></div>
<div class="m2"><p>کت ندادند ذوق گریهٔ زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره دوست پوست پوشیدیم</p></div>
<div class="m2"><p>تا فکندیم هفت پوست چو مار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ کس زو به ما نداد نشان</p></div>
<div class="m2"><p>خاطر از هیچ جا نیافت قرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به جایی رسید شور جنون</p></div>
<div class="m2"><p>که بر افتاد پردهٔ پندار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوست دیدم همه به صورت دوست</p></div>
<div class="m2"><p>یار دیدم همه به صورت یار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خانهٔ او ز هر که جستم، گفت</p></div>
<div class="m2"><p>لیسَ فِی الدار غیره الدیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای که گویی که دل ازو برگیر</p></div>
<div class="m2"><p>گر توانی تو چشم ازو بردار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دور اگر نیست بر مراد مرنج</p></div>
<div class="m2"><p>که نه در دست ماست این پرگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صوفی ار سجدهٔ صنم نکنی</p></div>
<div class="m2"><p>خرقه خصمت شود، کمر زنار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرگ بهتر که صحبت بی دوست</p></div>
<div class="m2"><p>گور خوشتر که خلوت بی یار</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn16"><div class="m1"><p>کوی عشق است این و در وی صدبلا</p></div>
<div class="m2"><p>راه عشق است این و در وی صد خطر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آسمان اینجا ببوسد آستان</p></div>
<div class="m2"><p>جبرئیل اینجا بریزد بال و پر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان دهند اینجا برای دردِ دل</p></div>
<div class="m2"><p>سر نهند اینجا برای دردسر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیده بردوز از خود و او را ببین</p></div>
<div class="m2"><p>خود مبین اندر میان او را نگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خود بسوز و هرچه می‌خواهی بساز</p></div>
<div class="m2"><p>خود بباز و هرچه می‌خواهی ببر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در کلاه فقر می‌باید سه ترک</p></div>
<div class="m2"><p>ترک دین و ترک دنیا، ترک سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوالعجب طوریست طور عاشقان</p></div>
<div class="m2"><p>جمله باهم دوست‌تر از یکدگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جای در زندان و دایم در سرود</p></div>
<div class="m2"><p>پای در دامان و دایم در سفر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در فراق یکدگر اشکند و آه</p></div>
<div class="m2"><p>در مذاق یکدگر شیر و شکر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نامه و پیغام گو هرگز مباش</p></div>
<div class="m2"><p>می‌دهند اینجا به دل از دل خبر</p></div></div>
<div class="c"><p>مِنْرباعّیاته فی المعارف</p></div>
<div class="b" id="bn26"><div class="m1"><p>در عشق اگر جان بدهی جان آنست</p></div>
<div class="m2"><p>ای بی سر و سامان، سرو سامان آنست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر در ره او دل تو دردی دارد</p></div>
<div class="m2"><p>آن درد نگهدار که درمان آنست</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn28"><div class="m1"><p>گر بویی از آن زلف معنبر یابی</p></div>
<div class="m2"><p>مشکل که دگر پای خود از سریابی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از خجلت دانایی خود آب شوی</p></div>
<div class="m2"><p>گر لذت نادانی ما دریابی</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn30"><div class="m1"><p>از دوری راه تا به کی آه کنی</p></div>
<div class="m2"><p>از رهرو و رهزن طلب راه کنی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یارب چه شود که بر سر هستی خود</p></div>
<div class="m2"><p>یک گام نهی و قصّه کوتاه کنی</p></div></div>
<div class="c"><p>ساقی نامه</p></div>
<div class="b" id="bn32"><div class="m1"><p>الهی به مستان میخانه‌ات</p></div>
<div class="m2"><p>به عقل آفرینان دیوانه‌ات</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به میخانهٔ وحدتم راه ده</p></div>
<div class="m2"><p>دل زنده و جان آگاه ده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دماغم ز میخانه بویی شنید</p></div>
<div class="m2"><p>حذر کن که دیوانه هویی شنید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزن هر قدر خواهیم پا به سر</p></div>
<div class="m2"><p>سر مست از پا ندارد خبر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به میخانه آی و صفا را ببین</p></div>
<div class="m2"><p>مبین خویشتن را خدا را ببین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بس آلوده‌ام آتش می کجاست</p></div>
<div class="m2"><p>بر آسوده‌ام نالهٔ نی کجاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو شادی بدین زندگی عار کو</p></div>
<div class="m2"><p>گشودند گیرم درت بار کو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهان منزل راحت اندیش نیست</p></div>
<div class="m2"><p>ازل تا ابد یک نفس بیش نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همه مستی وشور و حالیم ما</p></div>
<div class="m2"><p>نه چون توهمه قیل و قالیم ما</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مغنی سحر شد خروشی برآر</p></div>
<div class="m2"><p>ز خامان افسرده جوشی برآر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بیا تا سری در سر خم کنیم</p></div>
<div class="m2"><p>من و تو، تو و من همه گم کنیم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کدورت کشی از کف کوفیان</p></div>
<div class="m2"><p>صفا خواهی اینک صف صوفیان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ازین دین به دنیافروشان مباش</p></div>
<div class="m2"><p>بجز بندهٔ ژنده پوشان مباش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به شوریدگان گر شبی سر کنی</p></div>
<div class="m2"><p>وزان می که مستند لب تر کنی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جمال محالی که حاشا کنی</p></div>
<div class="m2"><p>ببندی دو چشم و تماشا کنی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که گفتت که چندین ورق را ببین</p></div>
<div class="m2"><p>ورق را بگردان و حق را ببین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>رخ ای زاهد از می پرستان متاب</p></div>
<div class="m2"><p>تو در آتش افتاده‌ای من در آب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نماز ار نه از روی مستی کنی</p></div>
<div class="m2"><p>به مسجد درون بت پرستی کنی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دلم گه از آن گه ازین جویدش</p></div>
<div class="m2"><p>ببین کاسمان از زمین جویدش</p></div></div>