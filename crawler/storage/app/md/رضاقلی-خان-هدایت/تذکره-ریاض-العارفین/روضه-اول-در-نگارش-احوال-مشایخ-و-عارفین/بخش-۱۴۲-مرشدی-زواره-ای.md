---
title: >-
    بخش ۱۴۲ - مرشدی زواره‌ای
---
# بخش ۱۴۲ - مرشدی زواره‌ای

<div class="n" id="bn1"><p>اسمش مولانا محمد، اصلش از قصبهٔ زَواره و آن از مضافات اردستان از بلوکات اصفهان. از سالکان مسالک حقیقت و مالکان ممالک طریقت، و برادر مولانا سپهری زواره‌ای است. در سنهٔ ۱۰۳۰ وفات یافت. این رباعیات از اوست:</p></div>
<div class="b" id="bn2"><div class="m1"><p>نقش خم ابروی ترا در محراب</p></div>
<div class="m2"><p>عکسِ لبِ میگون ترا در میِ ناب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد چو بدید بی خود آمد به سجود</p></div>
<div class="m2"><p>می‌خواره چو یافت مست گردید و خراب</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn4"><div class="m1"><p>در مذهب عشق علم و دانش رندی است</p></div>
<div class="m2"><p>دانشمندی مایهٔ هر خرسندی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک چهره ز روی عجز بر خاکِ نیاز</p></div>
<div class="m2"><p>بهتر ز هزار گونه دانشمندی است</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn6"><div class="m1"><p>من دل به غم تو بسته دارم ای دوست</p></div>
<div class="m2"><p>دردِ تو به جان خسته دارم ای دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی به دل شکسته ما نزدیکیم</p></div>
<div class="m2"><p>من نیز دلی شکسته دارم ای دوست</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn8"><div class="m1"><p>ما نفس خسیس را ملامت کردیم</p></div>
<div class="m2"><p>در بقعهٔ نیستی اقامت کردیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نیک و بد زمانه یک سو رفتیم</p></div>
<div class="m2"><p>وز خلق کناره تا قیامت کردیم</p></div></div>
<div class="c"><p>٭٭٭</p></div>
<div class="b" id="bn10"><div class="m1"><p>گاهی ز لب تو همچو می در جوشم</p></div>
<div class="m2"><p>وز چشم تو گه چو می کشان مدهوشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ذکر توام اگر دمی گویایم</p></div>
<div class="m2"><p>در فکر توام گر نفسی خاموشم</p></div></div>