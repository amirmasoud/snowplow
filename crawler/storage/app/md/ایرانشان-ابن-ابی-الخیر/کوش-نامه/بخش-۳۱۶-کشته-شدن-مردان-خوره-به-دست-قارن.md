---
title: >-
    بخش ۳۱۶ - کشته شدن مردان خورّه به دست قارن
---
# بخش ۳۱۶ - کشته شدن مردان خورّه به دست قارن

<div class="b" id="bn1"><div class="m1"><p>سپیده دمان قارن رزمخواه</p></div>
<div class="m2"><p>بفرمود تا صف کشید آن سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غو کوس برخاست و آواز نای</p></div>
<div class="m2"><p>چو دریا سپاه اندر آمد ز جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمود تا شد به جایش قباد</p></div>
<div class="m2"><p>چو سالار بر میمنه بایستاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گزین کرد برگستوانور هزار</p></div>
<div class="m2"><p>نبرده دلیران خنجرگزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان پیش قلب اندرون تا گروه</p></div>
<div class="m2"><p>رده برکشیدند مانند کوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وزآن پس جهاندار باهوش کوش</p></div>
<div class="m2"><p>بیاراست لشکر به آیین دوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درفشش به مردان خورّه سپرد</p></div>
<div class="m2"><p>کز ایران گوی بود با دستبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پسندیده بودش به هنگام رزم</p></div>
<div class="m2"><p>گرامی بُد او نیز هنگام بزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شاهی به طنجه ش فرستاده بود</p></div>
<div class="m2"><p>همه سوس الاقصی بدو داده بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قلب آمد از میمنه شهریار</p></div>
<div class="m2"><p>پسِ پُشتِ او لشکری نامدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خروش آمد و زخم شمشیر تیز</p></div>
<div class="m2"><p>برآمد ز هر گوشه ای رستخیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یلان و دلیران پرخاشجوی</p></div>
<div class="m2"><p>به تندی به روی اندر آورده روی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به نیزه به یکدیگران تاختند</p></div>
<div class="m2"><p>گهی گرز و گه تیغ کین آختند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو برگ از درختان، سواران ز زین</p></div>
<div class="m2"><p>همی ریختند اندر آن دشت کین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپه بیشتر بی سر و دست شد</p></div>
<div class="m2"><p>ز خون، خاک تیره همی مست شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دژم گشت گردون از آن دشت رزم</p></div>
<div class="m2"><p>فروماند مر زُهره را دل ز بزم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو شد خاک گرم از دم آفتاب</p></div>
<div class="m2"><p>سر جنگیان گرم گشت از شتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی خیره گشتند ایرانیان</p></div>
<div class="m2"><p>به لشکر نگه کرد کوش از میان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بزد خویشتن بر سپاه قباد</p></div>
<div class="m2"><p>قباد از دلیری بغل برگشاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر او حمله آورد و آمد به پیش</p></div>
<div class="m2"><p>برآویخت با نامداران خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تبه کرد کوش از سواران اوی</p></div>
<div class="m2"><p>بسی نامداران و یاران اوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه میمنه گشت زیر و زبر</p></div>
<div class="m2"><p>ز شمشیر آن شاه پرخاشخر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وزآن روی قارن ز قلب سپاه</p></div>
<div class="m2"><p>بزد خویشتن سخت بر قلبگاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمان و دنان بر لب آورده کف</p></div>
<div class="m2"><p>برافراخت یال و بدرّید صف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه قلب دشمن به هم برزدند</p></div>
<div class="m2"><p>گهی بر بر و گاه بر سر زدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو مردان خورّه چنان دید زود</p></div>
<div class="m2"><p>برآورد غیو و دلیری نمود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برادرش را داد جای و درفش</p></div>
<div class="m2"><p>خود و نامداران زرّینه کفش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که بودند با او ز ایران سوار</p></div>
<div class="m2"><p>کمر بسته بر کین چهاران هزار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عنان تیز کرده بیامد دوان</p></div>
<div class="m2"><p>کشیده همی تیغ بر پهلوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بمالید ایرانیان را درشت</p></div>
<div class="m2"><p>فزون از هزاران دلیران بکُشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو دید آن چنان پهلوان سپاه</p></div>
<div class="m2"><p>که از دست مردان سپه شد تباه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خروشان برآن نامور حمله کرد</p></div>
<div class="m2"><p>همی تاخت باره، همی کُشت مرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جنین تا به مردان خورّه رسید</p></div>
<div class="m2"><p>بدو گفت کای دیوزاده پلید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از ایران زمین روی برگاشتی</p></div>
<div class="m2"><p>چنان شاه را خوار بگذاشتی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>براندی بیکباره از بود و زاد</p></div>
<div class="m2"><p>شدی پیشکار یکی دیوزاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان شادمانی بدان اهرمن</p></div>
<div class="m2"><p>بیابی تو پاداش این بد ز من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدو گفت مردان گر آهنگری</p></div>
<div class="m2"><p>سپهبد شود بر چنین لشکری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا می رسد بی گمان مهتری</p></div>
<div class="m2"><p>که بودیم همواره با سروری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو تا چاکرم بود بیش از هزار</p></div>
<div class="m2"><p>به گاه کیان تا بدین روزگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بشنید قارن، برآشفت سخت</p></div>
<div class="m2"><p>بلرزید مانند شاخ درخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رها کرد خشتی پر از خشم و کین</p></div>
<div class="m2"><p>درآمد سر خشت بر پُشت زین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گذر کرد بر زین و شد در شکم</p></div>
<div class="m2"><p>نگون شد ز باره سوار دژم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هم اندر زمان بارگی جان بداد</p></div>
<div class="m2"><p>سپهدار برجست مانند باد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دلیران ایران بدو تاختند</p></div>
<div class="m2"><p>همه نیزه و تیغ کین آختند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از آن پس که مردان از او درگذشت</p></div>
<div class="m2"><p>دل قارن از درد رنجور گشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کشیدند، پیش وی اسبی بلند</p></div>
<div class="m2"><p>نشست از برش پهلوان بی گزند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز کینه برآشفت و تندی نمود</p></div>
<div class="m2"><p>سپه را برانگیخت مانند دود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>میان سپه چون به مردان رسید</p></div>
<div class="m2"><p>بزد دست و گرز گران برکشید</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بدو گفت کای ننگ بر انجمن</p></div>
<div class="m2"><p>کجا رفت خواهی تو از چنگ من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز بس کرد مردان خوره زکاه</p></div>
<div class="m2"><p>برآویخت با او میان سپاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گهی خنجر و تیر بر بر زدند</p></div>
<div class="m2"><p>گهی گرز پولاد بر سر زدند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بفرجام گرزی زدش پهلوان</p></div>
<div class="m2"><p>که مغزش ز بینی برون شد روان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو شد پهلوان از برش زاستر</p></div>
<div class="m2"><p>از اسب اندر افتاد پرخاشخر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به خواریش بستند از آن جا به اسب</p></div>
<div class="m2"><p>همی تاختندش چو آذرگشسب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به راه اندرون جان شیرین بداد</p></div>
<div class="m2"><p>شد آن نامور گُرد پهلونژاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چنین گردد این گنبد تیزرو</p></div>
<div class="m2"><p>همی بازی آرایدت نو به نو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رود گرد اندر پس تیز مرد</p></div>
<div class="m2"><p>سرانجام کار اندر آید به گَرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو مردان بیفتاد برخاست شور</p></div>
<div class="m2"><p>دلیران ایران گرفتند زور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز دشمن بکشتند چندان که دشت</p></div>
<div class="m2"><p>سراسر که چندان تل و توده گشت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز روی دگر کوش پرخاشخر</p></div>
<div class="m2"><p>همه میمنه کرد زیر و زبر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیامد برآویخت با او قباد</p></div>
<div class="m2"><p>به رزم اندرون داد مردی بداد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زمانی بکوشید و چاره ش نماند</p></div>
<div class="m2"><p>خروشان تگاور ز پیشش براند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هزیمت شد از میمنه هر که بود</p></div>
<div class="m2"><p>چو سلم آن چنان دید مردی نمود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز رزم آزمایان رومی سوار</p></div>
<div class="m2"><p>به یاری فرستادشان صد هزار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دلیران ز کینه برآویختند</p></div>
<div class="m2"><p>به یکدیگران اندر آمیختند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنین تا شب آمد چکاچاک بود</p></div>
<div class="m2"><p>زمین پُر زخون، چرخ پر خاک بود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شب آمد جدا شد دو لشکر ز هم</p></div>
<div class="m2"><p>همه کوفته دل پُر از رنج و غم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فزون کُشته بود از سپاه قباد</p></div>
<div class="m2"><p>دو ره ده هزار، آن یل نامدار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گشاد از میان کوش جنگی کمر</p></div>
<div class="m2"><p>بیامد نشست از بر تخت زر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سران سپه را همه بار داد</p></div>
<div class="m2"><p>وزآن رزم هرکس همی کرد یاد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز مردان چو آگاه شد شاه کوش</p></div>
<div class="m2"><p>که بر دست قارن برآمدش هوش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>برآشفت و ز خشم سوگند خورد</p></div>
<div class="m2"><p>که فردا نیارامم اندر نبرد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مگر کین مردان بجای آورم</p></div>
<div class="m2"><p>سر پهلوان زیر پای آورم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بفرمود خواندن برادرش را</p></div>
<div class="m2"><p>درفشش بدو داد و لشکرش را</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>فراوانش بستود و گرمی نمود</p></div>
<div class="m2"><p>همش خلعت و مهربانی نمود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به خوردن نشست آنگهی با سران</p></div>
<div class="m2"><p>می روشن آورد و رامشگران</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>وزآن روی با مهتران و گوان</p></div>
<div class="m2"><p>سوی سلم شد قارن پهلوان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدو گفت کای خسرو نیکنام</p></div>
<div class="m2"><p>برآمدت امروز رزمی ز کام</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز دشمن بکشتیم چندان که دشت</p></div>
<div class="m2"><p>سراسر به خون تن آلوده گشت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو مردان خورّه بیامد دمان</p></div>
<div class="m2"><p>دو دیده ز خون کرده چون قهرمان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>برآویخت با من میان سپاه</p></div>
<div class="m2"><p>به یک زخم کردم مر او را تباه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تو گفتی نه آن بود جنگی سوار</p></div>
<div class="m2"><p>کز ایران برفت از بر شهریار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>دو چندان فزون بود شاها به زور</p></div>
<div class="m2"><p>یکی آهنین کرده بودش ستور</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>فراوان به هر گوشه ای تاختم</p></div>
<div class="m2"><p>چو از رزم آن یل بپرداختم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>مگر دیوزاد آیدم پیش چشم</p></div>
<div class="m2"><p>ندیدمش و افزون شدم کین و خشم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چنین گفت با سلم و قارن قباد</p></div>
<div class="m2"><p>که آن کینه کش بدرگ دیوزاد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>همه روز با ما همی رزم کرد</p></div>
<div class="m2"><p>ز گردان فراوان برآورد گَرد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به کوشش چو با او برآویختیم</p></div>
<div class="m2"><p>بسنده نبودیم و بگریختیم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سپاهی فرستاد، چون دید شاه</p></div>
<div class="m2"><p>سوی ما به یاری ز قلب سپاه</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگر نه شب تیره پیش آمدی</p></div>
<div class="m2"><p>همه کام آن تیره کیش آمدی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شکسته شدی بی گمان میمنه</p></div>
<div class="m2"><p>نه سالار ماندی و نه یک تنه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بدو گفت قارن که فردا ببین</p></div>
<div class="m2"><p>کز او خون ستانم به شمشیر کین</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گر او جان رهاند ز شمشیر من</p></div>
<div class="m2"><p>دروغ است خواندن مرا رزمزن</p></div></div>