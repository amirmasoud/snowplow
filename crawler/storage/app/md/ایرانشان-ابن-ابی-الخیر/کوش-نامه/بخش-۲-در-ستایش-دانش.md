---
title: >-
    بخش ۲ - در ستایش دانش
---
# بخش ۲ - در ستایش دانش

<div class="b" id="bn1"><div class="m1"><p>بدین سر همه دانش آموز و بس</p></div>
<div class="m2"><p>که جز دانشت نیست فریادرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دانش به یزدان توانی رسید</p></div>
<div class="m2"><p>چو دانش جهان آفرین نافرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درختی ست دانش به پروین سرش</p></div>
<div class="m2"><p>همه راستکاری ست بار و برش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که سرمایه ی مرد دانش بود</p></div>
<div class="m2"><p>دل دانشی پر ز رامش بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دانش گریزان بود اهرمن</p></div>
<div class="m2"><p>ز دانش فروزان بود انجمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر دانش از خود بدانستمی</p></div>
<div class="m2"><p>به مینو رسیدن توانستمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد پرور و هوش و آموزگار</p></div>
<div class="m2"><p>رسیدن بدین هر سه زی کردگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین گفت شاگرد را سند باد</p></div>
<div class="m2"><p>که شاگرد شو تا شوی اوستاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه گفته ست پیغمبر پاکدین</p></div>
<div class="m2"><p>که دانش بجوی ار بیابی به چین</p></div></div>