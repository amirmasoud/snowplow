---
title: >-
    بخش ۳۵۰ - دیدن کاخی آباد و گفتگوی کوش آفریننده ی جهان با پیری فرزانه
---
# بخش ۳۵۰ - دیدن کاخی آباد و گفتگوی کوش آفریننده ی جهان با پیری فرزانه

<div class="b" id="bn1"><div class="m1"><p>چهل روز بر گردِ آن بیشه کوش</p></div>
<div class="m2"><p>همی تاخت، نیرو نماندش نه توش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ناگاه روزی به تلّی رسید</p></div>
<div class="m2"><p>عنان تگاور بدان سو کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی کاخ آباد دید از برش</p></div>
<div class="m2"><p>ز سنگ سیه برنهاده سرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهاندیده کوش از در آواز داد</p></div>
<div class="m2"><p>که این در به ما بر بباید گشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآمد یکی پیر با فرّ و هوش</p></div>
<div class="m2"><p>بدید آن سر و روی و دیدار کوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفت کای اهرمن روی مرد</p></div>
<div class="m2"><p>برِ ما تو را رهنمونی که کرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه چیزی؟ بگو، وز کجا آمدی؟</p></div>
<div class="m2"><p>بدین بیشه اندر چرا آمدی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خداوند روزی دهم، گفت، کوش</p></div>
<div class="m2"><p>به دست من است از جهان زهر و نوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخندید دانا ز گفتار اوی</p></div>
<div class="m2"><p>وزآن ناسزاوار دیدار اوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفت کای مرد ناهوشیار</p></div>
<div class="m2"><p>پس ایدر چرا آمدی و چه کار؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چهل روز بیش است، گفتا که من</p></div>
<div class="m2"><p>جدا ماندم از لشکر و انجمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در این بیشه گُم کرده ام راه خویش</p></div>
<div class="m2"><p>جدا مانده از کشور و گاه خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر باره از وی بخندید مرد</p></div>
<div class="m2"><p>بدو گفت کوش از دلی پُر ز درد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که خندیدن تو در این جای چیست</p></div>
<div class="m2"><p>چنین خنده ی نادلارای چیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ورا گفت از این روی و دیدار تو</p></div>
<div class="m2"><p>همی خنده آید ز گفتار تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که یک بار گویی که هستم خدای</p></div>
<div class="m2"><p>دگر باره گویی که راهم نمای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خداوند روزی دهِ مردمان</p></div>
<div class="m2"><p>چرا راه خواهد ز مردم نهان؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که بیراه را او به راه آورد</p></div>
<div class="m2"><p>شب و روز و خورشید و ماه آورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدو گفت ای پیرِ اندک خرد</p></div>
<div class="m2"><p>برآمد همی سالیان هشتصد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که تا من خداوند روزی دهم</p></div>
<div class="m2"><p>ز کار جهان سربسر آگهم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدو گفت پیر ای همه سرسری</p></div>
<div class="m2"><p>نگویی مرا تاکنون ایدری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرآن مردمان را که روزی دهد؟</p></div>
<div class="m2"><p>کشان نیکی و دلفروزی دهد؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین داد پاسخ که دستور هست</p></div>
<div class="m2"><p>دبیران بسیار و گنجور هست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که روزی به مردم رساند فراخ</p></div>
<div class="m2"><p>ز گنج من آباد دارند کاخ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت دانا کز این پرورش</p></div>
<div class="m2"><p>تو را داد بایست لختی خورش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که تا اندر این بی سپاه و کسی</p></div>
<div class="m2"><p>ز ناخوردن اندُه ندیدی بسی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز تو دور گشته ست نیروی تو</p></div>
<div class="m2"><p>وز این شکل زشت و چنین خوی تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنون مرمرا بازگوی آشکار</p></div>
<div class="m2"><p>که چون تو نبودی در آن روزگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خداوند روزی ده مردمان</p></div>
<div class="m2"><p>که بود و کرا دانی ای بدگمان؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنین داد پاسخ که دیگر بُدند</p></div>
<div class="m2"><p>خدایان که با تخت و افسر بُدند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدو گفت پیر، از کجا آمدند</p></div>
<div class="m2"><p>بدین تیره گیتی چرا آمدند؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین داد پاسخ که از مرد و زن</p></div>
<div class="m2"><p>پدید آمدند آن همه تن به تن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو پیر گفت ای سزاوار بد</p></div>
<div class="m2"><p>مگوی آنچه نپذیرد او را خرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نشاید که آن کاو جهان آفرید</p></div>
<div class="m2"><p>پدید آمد از جای تنگ و پلید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که همچون من و تو بود بی گمان</p></div>
<div class="m2"><p>تو را کی رسد دست بر آسمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو کوش گفت ای سخنگوی مرد</p></div>
<div class="m2"><p>مرا مغز، گفتار تو خیره کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر من نه روزی دهم، پس چه ام؟</p></div>
<div class="m2"><p>وگرنه خدای جهانم، که ام؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی بندهای گفت خود کام و زشت</p></div>
<div class="m2"><p>به تن ناسپاسی و دور از بهشت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گنهکار و بیره به فرمان دیو</p></div>
<div class="m2"><p>کشیده دل از راه گیهان خدیو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرفتار در خشم یزدان پاک</p></div>
<div class="m2"><p>تو را جای در دوزخ سهمناک</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدو کوش گفت این سخنها ز چیست</p></div>
<div class="m2"><p>که بر یافه گرفتار باید گریست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنین پاسخش داد کای شوربخت</p></div>
<div class="m2"><p>همی بر تو بخشایش آریم سخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جهان را و ما را همان آفرید</p></div>
<div class="m2"><p>که این بی کران آسمان آفرید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر او ماه و خورشید کرده ست چند</p></div>
<div class="m2"><p>ستاره که هرگز ندانی که چند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زمین کرد و کوه بلند از برش</p></div>
<div class="m2"><p>درختان و آن روان در خورش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو را بر زمین کرد سالار و شاه</p></div>
<div class="m2"><p>بزرگی و فرمانت داد و سپاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به گیتی چنین زندگانیت داد</p></div>
<div class="m2"><p>همه زندگانی، جوانیت داد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدان تا همه بندگان خدای</p></div>
<div class="m2"><p>تو باشی سوی راه او رهنمای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو را هرچه داد ایزد، ای اهرمن</p></div>
<div class="m2"><p>سراسر همی بینی از خویشتن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شدن اندر این نیکوی ناسپاس</p></div>
<div class="m2"><p>زهی بنده ی دیو ناحق شناس!</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو را گر تبی آید ای یافه گوی</p></div>
<div class="m2"><p>میان تو گردد چو باریک موی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نبینی کس از خویشتن خوارتر</p></div>
<div class="m2"><p>نه بیچاره تر نیز و غمخواره تر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدین ناتوانی و بیچارگی</p></div>
<div class="m2"><p>خدایی توان کرد یکبارگی؟</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدو گفت کای پیر ناهوشیار</p></div>
<div class="m2"><p>برآمد مرا سالیانی هزار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که روزی تنم را نیامد تبی</p></div>
<div class="m2"><p>نَه رنجی کشیدم، نَه دردی شبی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگر بنده ام چون کسانی دگر</p></div>
<div class="m2"><p>تنم دردمندی کشیدی مگر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدو پیر گفت ای سبکسار مرد</p></div>
<div class="m2"><p>بجای تو آن نیکوی پس که کرد؟</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تن آسان شدی، مست گشتی چنان</p></div>
<div class="m2"><p>که گویی منم کردگار جهان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کنون گر خدایی، برو، بازگرد</p></div>
<div class="m2"><p>که سیر آمدم من ز گفتار سرد</p></div></div>