---
title: >-
    بخش ۳۶۱ - خواهش دیگر شاه جابلق از کوش برای روستای بی آب کشورش
---
# بخش ۳۶۱ - خواهش دیگر شاه جابلق از کوش برای روستای بی آب کشورش

<div class="b" id="bn1"><div class="m1"><p>چو برگشتن آراست شاه و سپاه</p></div>
<div class="m2"><p>زمین را ببوسید جابلق شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورا گفت کای شاه آزاده خوی</p></div>
<div class="m2"><p>نمانده ست کاری جز این آرزوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که در کشورم روستایی ست خَوش</p></div>
<div class="m2"><p>در او مردمانی همه شیرفش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه ساله از آب تنگی دژم</p></div>
<div class="m2"><p>جز از آب باران نبینند نم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که از فرّ سالار دانش پژوه</p></div>
<div class="m2"><p>یکی آب پیدا شود پیش کوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود کشور آباد و من شادمان</p></div>
<div class="m2"><p>برآساید از غم دل مردمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا آرزو گردد از تو تمام</p></div>
<div class="m2"><p>به گیتی بماندت جاوید نام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کوش سرافراز مر آن شنید</p></div>
<div class="m2"><p>همان گه سپه را بدان سو کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی روستا دید همچون سراب</p></div>
<div class="m2"><p>در و دشت و کهسار او خشک از آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو رویه ز هر روی شش پاره ده</p></div>
<div class="m2"><p>مرآن هر دهی را همه مرد مه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که آن هر دهی بود مانند شهر</p></div>
<div class="m2"><p>ز بازار، وز برزن و کوی بهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همان بود کز آب بی مایه بود</p></div>
<div class="m2"><p>ز باغ و ز کاریز بی سایه بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسی کرده بر راه سیل آبگیر</p></div>
<div class="m2"><p>همه ساله زو خورده برنا و پیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنزدیک ایشان یکی کوه ژرف</p></div>
<div class="m2"><p>که هرگز نبودی برآن کوه برف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رسیده سرش سوی ابر سیاه</p></div>
<div class="m2"><p>وزآن روستا بود بر کوه راه</p></div></div>