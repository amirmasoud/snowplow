---
title: >-
    بخش ۱۹۴ - هدایای کارم به نزد فریدون و وصف اسبان آبی نژاد
---
# بخش ۱۹۴ - هدایای کارم به نزد فریدون و وصف اسبان آبی نژاد

<div class="b" id="bn1"><div class="m1"><p>در گنجهای پدر کرد باز</p></div>
<div class="m2"><p>فراوان برون کرد از آن گنج و ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاورد سالار فرخنده بخت</p></div>
<div class="m2"><p>ز گنج پدر شایگانی سه تخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی تخت پیروزه بود، آن که کوش</p></div>
<div class="m2"><p>فرستاد نزدیک طیهور زوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبرجد یکی تخت دیگر گزید</p></div>
<div class="m2"><p>که اندر جهان آن چنان کس ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی دیگر از زرّ خورشید رنگ</p></div>
<div class="m2"><p>که هرگز چنان، کس نیارد به چنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یاقوت و پیروزه آراسته</p></div>
<div class="m2"><p>به لعل و به مرجان بپیراسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهاده بر آن تختها هر سه تاج</p></div>
<div class="m2"><p>بیاورده ده تخت دیگر ز عاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم از تخته دیبای چینی هزار</p></div>
<div class="m2"><p>هزار دگر تیغ زهر آبدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیاورد از آن پس هزاران زره</p></div>
<div class="m2"><p>بدو هیچ پیدا نه بند و گره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان تخت دیبای چینی هزار</p></div>
<div class="m2"><p>هزار دگر مرکب شاهوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس آن نافه ی مشک تبّت هزار</p></div>
<div class="m2"><p>هزار دگر نیزه ی جان گذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز یاقوت صد پاره همچون کناغ</p></div>
<div class="m2"><p>که در شب همی تافتی چون چراغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر بود صد دانه درّ خوشاب</p></div>
<div class="m2"><p>که از شهریاران ندید آن به خواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سنجاب موی و سمور سیاه</p></div>
<div class="m2"><p>گزین کرد و آورد گنجور شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمردند از آن مویها ده هزار</p></div>
<div class="m2"><p>صد از خوبرویان چین و تتار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنیزی صد و بیست شمشاد تن</p></div>
<div class="m2"><p>همه پای کوب و همه چنگ زن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به غمزه همه چشمه ی و دلربای</p></div>
<div class="m2"><p>به فتنه همه چهره ی و جان کرای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صد از تازی اسبان آبی نژاد</p></div>
<div class="m2"><p>گزین کرد شاه آن به آیین و داد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گه شیهه آشفته ی بیدلی</p></div>
<div class="m2"><p>مر او را تگی دورتر منزلی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه راه در زیر مرد خرد</p></div>
<div class="m2"><p>بریدی شبانروز فرسنگ صد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دریا گرفته یکی اسب نر</p></div>
<div class="m2"><p>چو سیل روان و چو مرغ به پر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هیکل چو کوه و قوائم چو سنگ</p></div>
<div class="m2"><p>چو گوران به موی و چو دیوان به رنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تنش چون شبه، رخ چو سیم حلال</p></div>
<div class="m2"><p>چو جعد بتان موی و دنبال و یال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به گشتن چو چرخ و به جستن چو تیر</p></div>
<div class="m2"><p>کز او خیره شد مردم تیزویر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به رفتن چو باد و دویدن چو ببر</p></div>
<div class="m2"><p>به جولان چو آتش به شیهه چو ابر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تگاور چو عنقا، دلاور چو شیر</p></div>
<div class="m2"><p>به میدان دَوان و به جولان دلیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روانتر به دریا چو آبی نهنگ</p></div>
<div class="m2"><p>دوانتر به کوه او ز کوهی پلنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز رنگ سیاهیش تیره غراب</p></div>
<div class="m2"><p>ز باد دویدنش خیره عقاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو آتش به گرمی، چو طاووس کش</p></div>
<div class="m2"><p>کشان بر زمین موی یال و برش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سیاه سیاوش، بهزاد نام</p></div>
<div class="m2"><p>از این اسب بوده ست و این نیست خام</p></div></div>