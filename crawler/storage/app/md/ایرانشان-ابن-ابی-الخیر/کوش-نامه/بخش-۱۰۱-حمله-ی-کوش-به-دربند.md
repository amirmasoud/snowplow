---
title: >-
    بخش ۱۰۱ - حمله ی کوش به دربند
---
# بخش ۱۰۱ - حمله ی کوش به دربند

<div class="b" id="bn1"><div class="m1"><p>نهادندش آن پاسخ نامه پیش</p></div>
<div class="m2"><p>همی هرچه دیدند بی کمّ و بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو باز گفتند و نامه بخواند</p></div>
<div class="m2"><p>ز کینه همی خون به لب برفشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمود تا کشتی آرند و ساز</p></div>
<div class="m2"><p>که سوی بسیلا شود رزمساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرستاده گفت ای نبرده دلیر</p></div>
<div class="m2"><p>مگر گشتی از لشکر خویش سیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر هیج پذرفت خواهی تو پند</p></div>
<div class="m2"><p>ستیزه مکن با بلا و گزند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تو شوی باد و لشکرْت ابر</p></div>
<div class="m2"><p>وگر تو شوی شیر و یارانت ببر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کوه بسیلا نیابی تو رنگ</p></div>
<div class="m2"><p>گر آن جا شوی بازگردی به ننگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بشنید گفتار آن پندده</p></div>
<div class="m2"><p>به ابرو برافگند از چین گره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دریا بسی کشتی آراست باز</p></div>
<div class="m2"><p>نهاد اندر او هرچه بایست ساز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کشتی نشست آن دلاور سوار</p></div>
<div class="m2"><p>گزیده دلیران ده و دو هزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به کوه بسیلا نهادند روی</p></div>
<div class="m2"><p>زبان یاوه گوی و روان جنگجوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرستاده از کوه چون بازگشت</p></div>
<div class="m2"><p>ز کوش آن جزیره پرآواز گشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به طیهور پیغام کرد آتبین</p></div>
<div class="m2"><p>که اندیشه ی شاه باید دراین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تنی چند را پیش دربندیان</p></div>
<div class="m2"><p>که خسرو فرستد ندارد زیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که بس گربز است آن بد دیو زاد</p></div>
<div class="m2"><p>فسون داند و چاره داند نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان ریمن و تند و بدگوهر است</p></div>
<div class="m2"><p>که تنها یکی نامور لشکر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دربندیان چون رسد یارگر</p></div>
<div class="m2"><p>شویم ایمن از کار آن بدگهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو طیهور بشنید پیغام اوی</p></div>
<div class="m2"><p>بخندید و زی موبد آورد روی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که پنداری ایرانی از بیم کوش</p></div>
<div class="m2"><p>بترسید و شد از دلش رای و هوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نداند که گر کوش گردد چو باد</p></div>
<div class="m2"><p>نیارد بدین کوه پایش نهاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روان پس برآمد بر آن چند روز</p></div>
<div class="m2"><p>پراندیشه شد شاه گیتی فروز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پسند آمدش گفته ی آتبین</p></div>
<div class="m2"><p>ز لشکر تنی چند کردش گزین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستاد نزدیک دربندیان</p></div>
<div class="m2"><p>ز هرگونه ای داد او پندشان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که هشیار باشید روز و شبان</p></div>
<div class="m2"><p>نگهبان دربند با باژبان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر چاه دیگر فرستم سپاه</p></div>
<div class="m2"><p>شما باز گردید یکسر ز راه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین بود خواهد سپه را چنین</p></div>
<div class="m2"><p>چنین تا کند دشمن آهنگ چین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون او بازگردد ز دریا کنار</p></div>
<div class="m2"><p>شوم ایمن از گردش روزگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز سیصد تن افزون نبود آن سپاه</p></div>
<div class="m2"><p>که زی باژبانشان فرستاد شاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سوی باژبان نارسیده هنوز</p></div>
<div class="m2"><p>که بخث بد باژبان گشت کوز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز دریا برآمد شب تیره کوش</p></div>
<div class="m2"><p>ز یاران بسی کرده پولادپوش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نهانی نهاد او به دربند روی</p></div>
<div class="m2"><p>سری پر ز پرخاش و دل کینه جوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مر آن باژبان را همه خفته مست</p></div>
<div class="m2"><p>کشیدند تیغ و گشادند دست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکایک بکشتند بر جایشان</p></div>
<div class="m2"><p>نجنبید بر تن سر و پایشان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنین است کار می هوش بر</p></div>
<div class="m2"><p>تو گر هوشمندی چو یانی مخَور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدی را جز از می ندیدم کلید</p></div>
<div class="m2"><p>کلید در دوزخ آمد نبید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به لشکر فرستاد کوش آگهی</p></div>
<div class="m2"><p>که در بند کردم ز دشمن تهی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز دریا هلا تیزتر بگذرید</p></div>
<div class="m2"><p>مگر راه دربند را بسپرید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو یک نیمه افزون بریدند راه</p></div>
<div class="m2"><p>ز طیهوریان پیشش آمد سپاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدید آن که در بند پر لشکر است</p></div>
<div class="m2"><p>اگر کوش اگر دشمنی دیگر است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سپه را به یک جای بر گرد کرد</p></div>
<div class="m2"><p>بر آمد خروشیدن دار و برد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سواری دوان رفت نزدیک شاه</p></div>
<div class="m2"><p>که در بند بگرفت کوش و سپاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه باژبان را بکشته ست پاک</p></div>
<div class="m2"><p>تو فریادرس گرنه آمد هلاک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به پیش آمدش جای تنگ و درشت</p></div>
<div class="m2"><p>سپاهش بدان جایگه داد پشت</p></div></div>