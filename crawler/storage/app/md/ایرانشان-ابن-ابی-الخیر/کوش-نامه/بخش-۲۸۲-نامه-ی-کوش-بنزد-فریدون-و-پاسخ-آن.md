---
title: >-
    بخش ۲۸۲ - نامه ی کوش بنزد فریدون و پاسخ آن
---
# بخش ۲۸۲ - نامه ی کوش بنزد فریدون و پاسخ آن

<div class="b" id="bn1"><div class="m1"><p>فرستاد نامه بدان آگهی</p></div>
<div class="m2"><p>بنزدیک آن بارگاه مهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از نوبیان مرز کردیم پاک</p></div>
<div class="m2"><p>برآوردم از شهرشان تیره خاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فرّ شهنشاه والاگهر</p></div>
<div class="m2"><p>چنین کردم این کشور باختر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه با باز قمری هم آشیان</p></div>
<div class="m2"><p>همی خانه دارد، ندارد زیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریدون از آن نامه شد شادمان</p></div>
<div class="m2"><p>یکی پاسخش کرد هم در زمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که بادی همه ساله فیروز و شاد</p></div>
<div class="m2"><p>که در نیکنامی بدادی تو داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان دیوساران کسی آن نکرد</p></div>
<div class="m2"><p>که تو کردی ای شیردل در نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بماناد با تو دل و رای رزم</p></div>
<div class="m2"><p>همه ساله باشی تو با جام و بزم</p></div></div>