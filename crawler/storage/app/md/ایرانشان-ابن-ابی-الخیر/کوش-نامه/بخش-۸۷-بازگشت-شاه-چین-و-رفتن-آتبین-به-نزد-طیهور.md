---
title: >-
    بخش ۸۷ - بازگشت شاه چین و رفتن آتبین به نزد طیهور
---
# بخش ۸۷ - بازگشت شاه چین و رفتن آتبین به نزد طیهور

<div class="b" id="bn1"><div class="m1"><p>بدانست سالار چین کان سخُن</p></div>
<div class="m2"><p>چنان است کافگند آن مرد بُن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود تا برنهادند بار</p></div>
<div class="m2"><p>سوی چین کشیدند از مرغزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد از رفتنش آتبین شادمان</p></div>
<div class="m2"><p>تو گفتی سرآمد مر او را غمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرستاد لختی سواران ز پس</p></div>
<div class="m2"><p>بجستند بیشه، ندیدند کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آن کوه یک هفته کرد او درنگ</p></div>
<div class="m2"><p>نیامد دلش سیر از آن خاره سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شبگیر هشتم بنه برگرفت</p></div>
<div class="m2"><p>فرود آمد از کوه و ره برگرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی رفت تا پیش دریا کنار</p></div>
<div class="m2"><p>سراپرده زد بر لب جویبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپاه بهک دید و کشتی و ساز</p></div>
<div class="m2"><p>ز ماچین همان گه رسیده فراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی گونه گون هدیه و خوردنی</p></div>
<div class="m2"><p>ز پوشیدنی هم ز گستردنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسانی که دانند در آب راه</p></div>
<div class="m2"><p>تنی ده فرستاد نزدیک شاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبشته به طیهور شه نامه ای</p></div>
<div class="m2"><p>به دست سرافراز خودکامه ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن، آتبین سخت شادی نمود</p></div>
<div class="m2"><p>بدان نامدار آفرین برفزود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی هفته دیگر به دریا کنار</p></div>
<div class="m2"><p>درنگ امدش تا بر آراست کار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به هشتم به کشتی نشستند شاد</p></div>
<div class="m2"><p>روان گشت کشتی بکردار باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شب و روز یک ماه کشتی چو تیر</p></div>
<div class="m2"><p>به دریا همی راند ملّاح پیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز عقرب چو بنمود رخساره ماه</p></div>
<div class="m2"><p>جزیره پدید آمد و دید شاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرو داشت کشتی به یک منزلی</p></div>
<div class="m2"><p>شد آشفته مردم ز خیره دلی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بماندند از آن کوه و دیا شگفت</p></div>
<div class="m2"><p>همی هر کس اندیشه ای درگرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی آتبین گفت کای کردگار</p></div>
<div class="m2"><p>توانا و دانا و پروردگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز دریا تو آری چنین کُه برون</p></div>
<div class="m2"><p>نهان داری آتش به سنگ اندرون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سراسر شگفت است کردار تو</p></div>
<div class="m2"><p>جهانی همه خیره از کار تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز آن جا یکی نامزد کرد شاه</p></div>
<div class="m2"><p>فرستاد ..................................</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی نامه فرمود خسرو درست</p></div>
<div class="m2"><p>چو پیش بهک کرده بود از نخست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دست فرستاده ی خویش داد</p></div>
<div class="m2"><p>بشد تا بر شاه ماچین چو باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز کار خودش یکسر آگاه کرد</p></div>
<div class="m2"><p>گله هرچه کردش ز بدخواه کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رسیدند نزدیک دربند شاد</p></div>
<div class="m2"><p>نگهبانِ دربند آواز داد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که این نامداران که اند و چه اند؟</p></div>
<div class="m2"><p>بدین آمدن نزد ما برچه اند؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرسته ی بهک پاسخ آورد باز</p></div>
<div class="m2"><p>که ای نامور مهتر سرفراز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز ماچین رسولیم نزدیک شاه</p></div>
<div class="m2"><p>اگر رای بینی کنون راه خواه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همان گه سواری فرستاد مرد</p></div>
<div class="m2"><p>به طیهور، وز کارش آگاه کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فرستاد طیهور مردی بزرگ</p></div>
<div class="m2"><p>به دربند با صد سوار سترگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیامد، به دربندشان راه داد</p></div>
<div class="m2"><p>همان راهشان تا درِ شاه داد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فرستادگان چون بدیدند تخت</p></div>
<div class="m2"><p>همی هرکسی آفرین خواند سخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدادند پس نامه هر دو بدوی</p></div>
<div class="m2"><p>سوی ترجمان کرد طیهور روی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بفرمود تا نامه برخواندند</p></div>
<div class="m2"><p>ز هر گونه ای داستان راندند</p></div></div>