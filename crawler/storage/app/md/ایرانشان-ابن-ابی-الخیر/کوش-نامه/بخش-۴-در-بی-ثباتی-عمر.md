---
title: >-
    بخش ۴ - در بی ثباتی عمر
---
# بخش ۴ - در بی ثباتی عمر

<div class="b" id="bn1"><div class="m1"><p>نماید همی کاین جهان یک دم است</p></div>
<div class="m2"><p>اگر شادکامی و گر خود غم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یک دم زدن نیست خواهی شدن</p></div>
<div class="m2"><p>به نیکی به آید همی دم زدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چندان به کاخ اندری کدخدای</p></div>
<div class="m2"><p>کجا با تو دارد روانِ تو پای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو جان گرامی رها شد ز تنگ</p></div>
<div class="m2"><p>نیابی به کاخ خود اندر درنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن مایگی از جهان کن پسند</p></div>
<div class="m2"><p>دل اندر سرای سپنجی مبند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ورزیدن گنج و مایه چه سود</p></div>
<div class="m2"><p>کجا مایه عمر است و گیتی ربود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روان را ز بهر کسان سوختن</p></div>
<div class="m2"><p>چرا بایدم دشمن اندوختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر از خرد اندکی دارمی</p></div>
<div class="m2"><p>به عمری جهان هیچ نگذارمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سی خسروان را بدیدیم نیز</p></div>
<div class="m2"><p>که رفتند و زیدر نبردند چیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شمشیر خرسندی ای نیکمرد</p></div>
<div class="m2"><p>بزن گردنِ آز و گِردش مگرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پشمین، تن تیره گون را بپوش</p></div>
<div class="m2"><p>که کشکینِ بی بیم بهتر ز نوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهان کردن و پس بماندن بجای</p></div>
<div class="m2"><p>همانا نفرمود ما را خدای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو پنهان کنی خواسته زیر خاک</p></div>
<div class="m2"><p>بِهْ از تو کسی دیگر اندر هلاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو گیتی شود راست بر نیکبخت</p></div>
<div class="m2"><p>برآردش چون نوبهاران درخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شاخ و شکوفه بیارایدش</p></div>
<div class="m2"><p>گل کامگاری ببار آیدش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کند شاخ و برگش چنان آبدار</p></div>
<div class="m2"><p>که از بوی و رنگش بخندد بهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شود هرچه پیرامنش سرخ و زرد</p></div>
<div class="m2"><p>کند سرْش بر گنبد لاجورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر از بار او بهره یابد کسی</p></div>
<div class="m2"><p>به گیتی از او نام ماند بسی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وگر کس نیابی از او بهره مند</p></div>
<div class="m2"><p>به گیتی نماندش نام بلند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان ناگهان باد و سرمای سرد</p></div>
<div class="m2"><p>کند برگ و بارش چو دینار زرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تهیدست ماند، نه برگ و نه بار</p></div>
<div class="m2"><p>به تاراج داده همه روزگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گه بازگشتن به جای دگر</p></div>
<div class="m2"><p>فزونی ست هم رنج و هم دردسر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به گاه شدن زین سپنجی سرای</p></div>
<div class="m2"><p>پشیمان شود گر گذارد بجای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو را خورد و پوشش چو آمد بجای</p></div>
<div class="m2"><p>فزون برد نتوان همی زین سرای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مکن گِردْ دردِ دل و بارِ تن</p></div>
<div class="m2"><p>ببخش و بخور، پند بشنو ز من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه فرّختری تو ز فرّخ همای</p></div>
<div class="m2"><p>چه کرده ست روزی مر او را خدای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مریز آبروی گرانمایه نیز</p></div>
<div class="m2"><p>به پیش که و مه تو از بهر چیز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسنده کن آنچ ایزد آراسته ست</p></div>
<div class="m2"><p>وگر تو نخواهی خود او خواسته ست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو خواهی بسنده کن و خواه نه</p></div>
<div class="m2"><p>سوی خواست او مر تو را راه نه</p></div></div>