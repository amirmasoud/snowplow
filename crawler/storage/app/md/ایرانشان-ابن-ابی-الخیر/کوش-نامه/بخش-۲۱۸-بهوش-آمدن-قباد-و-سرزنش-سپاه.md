---
title: >-
    بخش ۲۱۸ - بهوش آمدن قباد و سرزنش سپاه
---
# بخش ۲۱۸ - بهوش آمدن قباد و سرزنش سپاه

<div class="b" id="bn1"><div class="m1"><p>قباد سپهبد چو آمد بهوش</p></div>
<div class="m2"><p>برآورد با لشکر خویش جوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که برگاشتن روی بهر چرا؟</p></div>
<div class="m2"><p>رها کرد بایست مرده مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شما را بکوشید از بهر نام</p></div>
<div class="m2"><p>کز او شاد گشتی شه شادکام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سازم بهانه کنون پیش شاه</p></div>
<div class="m2"><p>چو گوید چرا بازگشت آن سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا کاشک یکباره هوش از تنم</p></div>
<div class="m2"><p>برفتی چه سازم چه پاسخ کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که مردن نکوتر ز ناکرده کار</p></div>
<div class="m2"><p>گذشتن چنین بر در شهریار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپه گفت کاین کار ما کرده ایم</p></div>
<div class="m2"><p>که زنده تو را بازپس برده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر شاه با ما کند آشتی</p></div>
<div class="m2"><p>وگرنه کجا راست پنداشتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکایک بگوییم هر کس به شاه</p></div>
<div class="m2"><p>کز این بازگشتن تویی بیگناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بخشایش آرد، نورزد همی</p></div>
<div class="m2"><p>گنه سوی ما بازگردد همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر پهلوان تا سپیده دمان</p></div>
<div class="m2"><p>نرستی سواری ز کام و زبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شمشیرمان کوش نگذاشتی</p></div>
<div class="m2"><p>سپه را به خاک اندر انباشتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همانا همان دوستر نزد شاه</p></div>
<div class="m2"><p>که زنده سوی وی رسد این سپاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دژم شد سپهبد ز گفتارشان</p></div>
<div class="m2"><p>وز آن ناسزاوار کردارشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلش بود از آن بازگشتن دژم</p></div>
<div class="m2"><p>سپاهش رسیدند روز سوم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خون دست شسته دلیران همه</p></div>
<div class="m2"><p>وز اسبان دشمن گرفته رمه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر نامجویان به فتراک بر</p></div>
<div class="m2"><p>ز پیشش فگندند بر خاک بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکایک بگفتندش از سر گذشت</p></div>
<div class="m2"><p>که کردند با لشکر چین به دشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخندید و شد شادمانه دلش</p></div>
<div class="m2"><p>درخشان شد آن پژمریده گلش</p></div></div>