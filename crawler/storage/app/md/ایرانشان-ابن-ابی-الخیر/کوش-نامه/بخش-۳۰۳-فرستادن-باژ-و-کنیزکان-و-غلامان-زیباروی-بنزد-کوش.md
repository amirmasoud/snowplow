---
title: >-
    بخش ۳۰۳ - فرستادن باژ و کنیزکان و غلامان زیباروی بنزد کوش
---
# بخش ۳۰۳ - فرستادن باژ و کنیزکان و غلامان زیباروی بنزد کوش

<div class="b" id="bn1"><div class="m1"><p>فرستاده چون پیش فاروق شد</p></div>
<div class="m2"><p>ز شادی کلاهش به عیوق شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم اندر زمان پاسخش کرد باز</p></div>
<div class="m2"><p>که ای شاه گردنکش رزمساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه هرچه درخواستی آن کنم</p></div>
<div class="m2"><p>بدین آرزو جان گروگان کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز آن آمدن مرمرا پیش شاه</p></div>
<div class="m2"><p>همی دل ز اندیشه گردد تباه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر شاه بیند، نفرمایدم</p></div>
<div class="m2"><p>به چشن بزرگی ببخشایدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر هرچه گوید میان بسته ام</p></div>
<div class="m2"><p>چو از خشم شاه جهان رسته ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو کوش آن چنان خواهش و لابه دید</p></div>
<div class="m2"><p>بر او بر ببخشود و دَم درکشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرستاده را گفت کای مرد راست</p></div>
<div class="m2"><p>بگویش که گر تو نیایی رواست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برِ ما فرست آنچه ما خواستیم</p></div>
<div class="m2"><p>بدین خواستن نامه آراستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو خشنود شد کوش، فاروق گفت</p></div>
<div class="m2"><p>که اکنون از او گنج نتوان نهفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گزین کرد از آن مرزداران هزار</p></div>
<div class="m2"><p>که با تاج بودند و با گوشوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکایک بفرمود تا هر سری</p></div>
<div class="m2"><p>غلامی بیارند با دلبری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رخساره ماه و به دندان چون قند</p></div>
<div class="m2"><p>به غمزه همه جادوی را گزند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صد اسب دونده بیاورد نیز</p></div>
<div class="m2"><p>یکی تاج و با تخت و هرگونه چیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صد اشتر همه زرّ کرده به بار</p></div>
<div class="m2"><p>همان سیصد از جامه ی زرنگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو فرزند بودش دلیر و جوان</p></div>
<div class="m2"><p>برادر دو از گوهر خسروان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرستادشان با چنین خواسته</p></div>
<div class="m2"><p>شد از خواسته لشکر آراسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین داد پیغام کای شهریار</p></div>
<div class="m2"><p>برآمد همه کامت از روزگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سزد گر کنون بازگردی ز راه</p></div>
<div class="m2"><p>بیامرزی این بنده ی نیکخواه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان شاه بشکوبش آن من است</p></div>
<div class="m2"><p>گریزنده از بد به خانِ من است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیارست رفتن بر تخت شاه</p></div>
<div class="m2"><p>ز گیتی مرا کرد از این بد پناه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر شاه بیند به من بخشدش</p></div>
<div class="m2"><p>مگر بخت تاریک بدرخشدش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو فرزند فاروق و آن خواسته</p></div>
<div class="m2"><p>به درگاه کوش آمد آراسته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پذیره شدندش همه سروران</p></div>
<div class="m2"><p>دلیران لشکرش و گندآوران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرآن کس که رخساره ی کوش دید</p></div>
<div class="m2"><p>روان و دل خویش بیهوش دید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرود آمد و خاک را بوسه داد</p></div>
<div class="m2"><p>چو بیدل همی آفرین کرد یاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بفرمود تا برنشستند، شاه</p></div>
<div class="m2"><p>خرامان و شادان گرفتند راه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز فاروق از آن پس بپرسیدشان</p></div>
<div class="m2"><p>چو باهوش و با رای دل دیدشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی راند تا پیش پرده سرای</p></div>
<div class="m2"><p>مرآن سرکشان را بدادند جای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو روز آمد، آن خواسته بنگرید</p></div>
<div class="m2"><p>همان دلبران را یکایک بدید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دیدار ایشان دلش گشت شاد</p></div>
<div class="m2"><p>مرآن سرکشان را بسی چیز داد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی عهد فرمود به پرنیان</p></div>
<div class="m2"><p>به نام دلیران فاروقیان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو داد بشکوبش و هرچه بود</p></div>
<div class="m2"><p>بر آن نیکوی، نیکویها فزود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت عجلسکس آباد کن</p></div>
<div class="m2"><p>تو را دارم، از خویشتن داد کن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو کرد آن یلان را به خوبی گسی</p></div>
<div class="m2"><p>شد از خواسته بهره ور هر کسی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سپاهش چو با برگ و با ساز شد</p></div>
<div class="m2"><p>از آن جا سوی اندلش باز شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به شادی و بگماز بنشست و می</p></div>
<div class="m2"><p>نیامدش یاد فریدون کی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مر آن دختران را بدان دلبری</p></div>
<div class="m2"><p>به یک سال بستد همه دختری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از ایشان هر آن کس که آمدش خوش</p></div>
<div class="m2"><p>به مشکوی زرّینش بردند کش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ببخشید دیگر بر آن مهتران</p></div>
<div class="m2"><p>بدان نامداران و فرمانبران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همان ریدکان را همی بود کار</p></div>
<div class="m2"><p>همین کرد با این بتان شهریار</p></div></div>