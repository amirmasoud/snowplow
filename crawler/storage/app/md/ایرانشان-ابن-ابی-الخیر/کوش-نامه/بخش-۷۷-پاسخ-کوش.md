---
title: >-
    بخش ۷۷ - پاسخ کوش
---
# بخش ۷۷ - پاسخ کوش

<div class="b" id="bn1"><div class="m1"><p>....................................</p></div>
<div class="m2"><p>....................................</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین سان که داری دلی گشته ریش</p></div>
<div class="m2"><p>یکی تیر کرد او برون را بپیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانی بگردیم و بازی کنیم</p></div>
<div class="m2"><p>بدین دشتِ کین اسب تازی کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببینیم تا چون برآید نبرد</p></div>
<div class="m2"><p>زمانه کرا اندر آرد به گرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تو کِشی کین فرزند خویش</p></div>
<div class="m2"><p>وگر من کنم جان بدخواه ریش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود کار یکرویه زین کارزار</p></div>
<div class="m2"><p>اگر تو شوی شاد، اگر شهریار</p></div></div>