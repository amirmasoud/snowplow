---
title: >-
    بخش ۱۴۲ - پیشکش فرستادنها
---
# بخش ۱۴۲ - پیشکش فرستادنها

<div class="b" id="bn1"><div class="m1"><p>به دستور فرمود تا کرد ساز</p></div>
<div class="m2"><p>در گنجهای کهن کرد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاراست کارش چنانچون سزید</p></div>
<div class="m2"><p>کس آن ساز و آن شادکامی ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان آتبین گنجها برگشاد</p></div>
<div class="m2"><p>سه هفته همی ساز و مهمان نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرستاد چندان گهر نزد شاه</p></div>
<div class="m2"><p>کز آن خیره گشتند شاه و سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بیجاده تاج و ز پیروزه تخت</p></div>
<div class="m2"><p>فرستاد نزد شه نیکبخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم از تخت رومی و از پارسی</p></div>
<div class="m2"><p>ز هر جامه صد بار و ده بار، سی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ده استر برایشان، ده از مهد زر</p></div>
<div class="m2"><p>نشانده در او هر یکی صدگهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زیور همه مهدها کرده پر</p></div>
<div class="m2"><p>دو صندوق پر لعل و یاقوت و دُر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که از خسروان جهان آن ندید</p></div>
<div class="m2"><p>که شاه آتبین پیش خسرو کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه جمشید را بود چندان گهر</p></div>
<div class="m2"><p>نه کس گفت هرگز که دیدم دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزرگانِ شه را ز هرگونه چیز</p></div>
<div class="m2"><p>فرستاد، خویشان او را بنیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه خواهران فرارنگ را</p></div>
<div class="m2"><p>سپاهی و شهری و فرهنگ را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باندازه، هدیه فرستادشان</p></div>
<div class="m2"><p>گرانمایه تر چیزها دادشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نماند از بسیلا جوانی دگر</p></div>
<div class="m2"><p>که از شاه نستد کلاه و کمر</p></div></div>