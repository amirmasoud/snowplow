---
title: >-
    بخش ۱۶۴ - آگاه کردن کوش، ضحاک را از رفتن آتبین به ایران
---
# بخش ۱۶۴ - آگاه کردن کوش، ضحاک را از رفتن آتبین به ایران

<div class="b" id="bn1"><div class="m1"><p>دل پیل دندان ز غم یافت درد</p></div>
<div class="m2"><p>به ضحّاک جادو سبک نامه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بگریخت آن بدنژاد آتبین</p></div>
<div class="m2"><p>ز کوه بسیلا، نه از راه چین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راهی که بر قاف دارد گذر</p></div>
<div class="m2"><p>به بلغار و سقلاب رفت او به در</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی دخت طیهور با خود ببرد</p></div>
<div class="m2"><p>که خورشید را رنگ تیره شمرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاید کسی را جز او شاه را</p></div>
<div class="m2"><p>بجوید نکن با بدان راه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که در هفت کشور نیاید چنین</p></div>
<div class="m2"><p>دریغ آن چنان روی با آتبین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرستادگان را چو کرد او گسی</p></div>
<div class="m2"><p>به دریا فرستاد دیگر کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپه باز خواند او ز دریا کنار</p></div>
<div class="m2"><p>همی بود تا چون بود روزگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نامه به ضحاک جادو رسید</p></div>
<div class="m2"><p>برآشفت و لب را به دندان گزید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بلغار و سقلاب و دربند روم</p></div>
<div class="m2"><p>فرستاد نامه به هر مرز و بوم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که در کوه و دریا درنگ آورید</p></div>
<div class="m2"><p>مگر آتبین را به چنگ آورید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپاهی به دریای الهم رسید</p></div>
<div class="m2"><p>بدان مرز آب آتبین را بدید</p></div></div>