---
title: >-
    بخش ۱۱۸ - لشکرکشی آتبین به چین و گرفتن شهر خمدان
---
# بخش ۱۱۸ - لشکرکشی آتبین به چین و گرفتن شهر خمدان

<div class="b" id="bn1"><div class="m1"><p>چو کشتی روان شد، سپه برکشید</p></div>
<div class="m2"><p>سوی مرز چین لشکر اندر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود تا پیشرو با سپاه</p></div>
<div class="m2"><p>همی رفت یک منزل از پیش شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کاو نیاورد فرمان بجای</p></div>
<div class="m2"><p>سر بخت او اندر آمد ز پای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن شهر و کشور برآورد خاک</p></div>
<div class="m2"><p>به تاراج داد آن بر و بوم پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بترسید از او مردم چین همه</p></div>
<div class="m2"><p>کجا بی شبان بود یکسر رمه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس از مرزداران نیامدش پیش</p></div>
<div class="m2"><p>بترسید از او مردم از جان خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هر سو روان گشت یک ساله باز</p></div>
<div class="m2"><p>ز هر جای گنجی نو آمد فراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان شد سپاه از فزونی و گنج</p></div>
<div class="m2"><p>که شد چارپای از کشیدن به رنج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی کاو ز ضحاک برگشته بود</p></div>
<div class="m2"><p>نهان گشته از بیم و سرگشته بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از این آگهی چاره جوی آمدند</p></div>
<div class="m2"><p>از ایران همه سوی او آمدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپاهش فزون شد ز پنجه هزار</p></div>
<div class="m2"><p>نبرده سواران نیزه گزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی رفت تا شهر خمدان رسید</p></div>
<div class="m2"><p>کز آن مرز جایی سواری ندید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خمدان بزاری برآمد خروش</p></div>
<div class="m2"><p>چو نوشان چنان دید، دستورِ کوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزرگان شهر و سپه را بخواند</p></div>
<div class="m2"><p>وز این داستان چند گونه براند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کز این کار دلها مدارید تنگ</p></div>
<div class="m2"><p>که این بی بنان را نباشد درنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمان تا زمان مژده آید ز کوش</p></div>
<div class="m2"><p>شود زهر این شهر یکباره نوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همانا که باشد فزون از سه ماه</p></div>
<div class="m2"><p>کجا بازگشت او ز درگاه شاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دشمن چو آگاهی آید از اوی</p></div>
<div class="m2"><p>بدین مرز بودن نباشدش روی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گریزان شود دشمن از پیش شاه</p></div>
<div class="m2"><p>وگرنه شود با سپاه او تباه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شما ماهیانی درنگ آورید</p></div>
<div class="m2"><p>به دروازه ی شهر جنگ آورید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که در شهرمان خوردنی نیست تنگ</p></div>
<div class="m2"><p>بکوشید باید به نام و به ننگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که پیروزی از مردمان دور نیست</p></div>
<div class="m2"><p>همان آتبین است، فغفور نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که ده باره از بیشه ی چین گریخت</p></div>
<div class="m2"><p>سپاهش همه ترگ و جوشن بریخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپه را چو دل داد و رخ رخش کرد</p></div>
<div class="m2"><p>همان روز دروازه ها بخش کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرستاد بر هر دری سه هزار</p></div>
<div class="m2"><p>گزیده سوار از درِ کارزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دیوار بر گونه گونه درفش</p></div>
<div class="m2"><p>برافراخت سرخ و سیاه و بنفش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو آمد به نزدیک شهر آتبین</p></div>
<div class="m2"><p>بپوشید خرگاه روی زمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه خیمه ها دیبه و پرنیان</p></div>
<div class="m2"><p>سراپرده ی آتبین در میان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همان گاه برخاست از شهر غو</p></div>
<div class="m2"><p>به دروازه ی شهر شد پیشرو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بغرید کوس و بنالید نای</p></div>
<div class="m2"><p>چو دریا سپاه اندر آمد ز جای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برون آمد از شهر چندان سپاه</p></div>
<div class="m2"><p>که بر باد گفتی ببستند راه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو دید آن سپاه آتبین خیره ماند</p></div>
<div class="m2"><p>پر اندیشه شد، سروران را بخواند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه شهر است گفتا یکی کشور است</p></div>
<div class="m2"><p>که چندین بدو اندرون لشکر است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بفرمود پس تا سپه برنشست</p></div>
<div class="m2"><p>سوی نیزه و تیغ بردند دست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برآمد خروش ده و دار و گیر</p></div>
<div class="m2"><p>بنالید نیزه، ببارید تیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان شد چکاچاک شمشیر تیز</p></div>
<div class="m2"><p>که کیوان همی جست راه گریز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان میغ پیوست و گرد سپاه</p></div>
<div class="m2"><p>که پیدا نبُد چرخ و از گردماه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز خون یلان بر زمین جوی شد</p></div>
<div class="m2"><p>به هامون سر سرکشان گوی شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شد از تیرگی دشت یکسان و کوه</p></div>
<div class="m2"><p>رسید آن سپه را ز خمدان ستوه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدان سان سپاهش بهم درفتاد</p></div>
<div class="m2"><p>کجا شاخ بر هم زند تند باد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو دید آتبین آن ستوهی، بجَست</p></div>
<div class="m2"><p>به شمشیر زد دست پس بر نشست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی با دلیرن خود حمله کرد</p></div>
<div class="m2"><p>ز گردان خمدان برآورد گرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان برگرفت آن سپه را ز جای</p></div>
<div class="m2"><p>که گفتی ببستندشان دست و پای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو تیغ وی از تارک آلوده شد</p></div>
<div class="m2"><p>سپه بر در شهر بر دوده شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدان حمله افزونتر از دو هزار</p></div>
<div class="m2"><p>تبه شد گریزنده اسب و سوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فگندند پس خویشتن را به شهر</p></div>
<div class="m2"><p>غم و درد دیدند از آن رزم بهر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نیامد کس از شهر بیرون دگر</p></div>
<div class="m2"><p>سپاهی نگهداشت دیوار و در</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرفت آتبین شهر خمدان حصار</p></div>
<div class="m2"><p>سر راه و بیراه کرد استوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شب و روز با شهریان جنگ بود</p></div>
<div class="m2"><p>ز بالا همه ناوک و سنگ بود</p></div></div>