---
title: >-
    بخش ۳۰ - کشته شدن ماهنگ پادشاه چین در جنگ با مهراج
---
# بخش ۳۰ - کشته شدن ماهنگ پادشاه چین در جنگ با مهراج

<div class="b" id="bn1"><div class="m1"><p>چو نامه بدادند، بر خواند زود</p></div>
<div class="m2"><p>فرستاد هر جا که لشکرش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به درگاه خواند او سپاهی گران</p></div>
<div class="m2"><p>گزین کرد پانصد هزاران سران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاد نزدیک مهراج شاه</p></div>
<div class="m2"><p>چو مهراج دید آن گزیده سپاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آراست لشکر، سوی چین کشید</p></div>
<div class="m2"><p>ز ماهنگ وز لشکرش کین کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان دو لشکر بسی بود رزم</p></div>
<div class="m2"><p>تهی کرده گردان سر از خواب و بزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس از کابل و زابل آمد سپاه</p></div>
<div class="m2"><p>فراوان مدد پیش مهراج شاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو با شاه چین شد زمانه درشت</p></div>
<div class="m2"><p>مر او را به خون پسر باز کُشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کُشتی تو همچون خودی را نژند</p></div>
<div class="m2"><p>چنان دان که کشتندت، ای مستمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه گنج ماهنگ برداشت پاک</p></div>
<div class="m2"><p>وز آن شهرِ خرّم برآورد خاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زن و بچه و گنج او هر چه بود</p></div>
<div class="m2"><p>فرستاد نزدیک ضحّاک زود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز آن جا سوی شهر خود بازگشت</p></div>
<div class="m2"><p>زمانه چنو خواست دمساز گشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان را چنین است آیین و کار</p></div>
<div class="m2"><p>گهی دشمن است و گهی دوستدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گهی با تو و گاه با تو نبود</p></div>
<div class="m2"><p>گهی نیکخو، گاه بدخواه بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شد نونک و فارک آگاه از این</p></div>
<div class="m2"><p>برفتند و بگذاشتند آن زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گریزان برفتند یک ماهه راه</p></div>
<div class="m2"><p>به جُستن بدان بیشه آمد سپاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی را ندیدند و گشتند باز</p></div>
<div class="m2"><p>جهان را دگرگونه تر بود ساز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آگاهی آمد به ضحاک از این</p></div>
<div class="m2"><p>که شد کشته دارای ماچین و چین</p></div></div>