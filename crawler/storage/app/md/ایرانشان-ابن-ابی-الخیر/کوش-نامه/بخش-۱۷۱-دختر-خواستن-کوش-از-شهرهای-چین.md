---
title: >-
    بخش ۱۷۱ - دختر خواستن کوش از شهرهای چین
---
# بخش ۱۷۱ - دختر خواستن کوش از شهرهای چین

<div class="b" id="bn1"><div class="m1"><p>جهاندیده گوید که در مرز چین</p></div>
<div class="m2"><p>بود سیصد و شصت شهر گزین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه شهرها یکسر آباد بود</p></div>
<div class="m2"><p>اگرچه ز چین رنج و بیداد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر شهر شاهی ز درگاه کوش</p></div>
<div class="m2"><p>نشسته به شادی و با نای و نوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو فرخار و چون تبّت و قندهار</p></div>
<div class="m2"><p>پر از خوبرویان چون نوبهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر مرزبانی یکی نامه کرد</p></div>
<div class="m2"><p>وزآن غم ستم بر سر خامه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که باید که رنجی به تن برنهید</p></div>
<div class="m2"><p>مرا هر کسی ماهچهری دهید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلارام و دوشیزه و خوبروی</p></div>
<div class="m2"><p>گل اندام و سیمینبر و مشکموی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نامه بدان مرزداران رسید</p></div>
<div class="m2"><p>کس از رای او هیچ چاره ندید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یک بار سیصد بت و شصت ماه</p></div>
<div class="m2"><p>به فرمان رسیدند نزدیک شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آن دلبران را به دیده بدید</p></div>
<div class="m2"><p>دلش را همی شادکامی رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببودی شب و روز با دلبری</p></div>
<div class="m2"><p>بدادیش هرگونه ای زیوری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو از بستر شاه برخاستی</p></div>
<div class="m2"><p>تنش را به زیور بیاراستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستادی او را برِ دخترش</p></div>
<div class="m2"><p>نمودی بدان جامه و زیورش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو دیدی مرآن دلبران را به چشم</p></div>
<div class="m2"><p>فزودیش بر اختر خویش خشم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخستی دلش سخت و بگریستی</p></div>
<div class="m2"><p>بدان شور بختی همی زیستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر از گردن آویخته روز و شب</p></div>
<div class="m2"><p>ز تیمار بسته ز گفتار لب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زن ارکام جوید نیایدش باک</p></div>
<div class="m2"><p>گر اندازد او خویشتن در هلاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر تیغ بیند، رسیده به کام</p></div>
<div class="m2"><p>نه برگردد او نارسیده به کام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یک سال نوبت بپایان رسید</p></div>
<div class="m2"><p>که آن دختران را یکایک بدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر سال نو سیصد و شصت یار</p></div>
<div class="m2"><p>رسیدند از آن مرزداران بار</p></div></div>