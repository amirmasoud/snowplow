---
title: >-
    بخش ۱۳۴ - گوی زدن آتبین با طیهور شاه
---
# بخش ۱۳۴ - گوی زدن آتبین با طیهور شاه

<div class="b" id="bn1"><div class="m1"><p>دگر روز برخاست جوینده شاه</p></div>
<div class="m2"><p>چو بیدل همی رفت تا بارگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به طیهور گفت ای شه نیکخوی</p></div>
<div class="m2"><p>مرا گوی و چوگان شده ست آرزوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر رای داری که فردا یکی</p></div>
<div class="m2"><p>بگردیم و بازی کنیم اندکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین داد پاسخ که فرمان تو راست</p></div>
<div class="m2"><p>به نزدیک من کام و رایت رواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان چون ز روشن ستاره ببست</p></div>
<div class="m2"><p>بفرمود تا لشکرش برنشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرستنده چون هوش بر خواب زد</p></div>
<div class="m2"><p>به میدان شد و گرد را آب زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهاندیده طیهور پاکیزه دین</p></div>
<div class="m2"><p>به میدان خرامید با آتبین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه میدان، نو آیین جهانی فراخ</p></div>
<div class="m2"><p>گشاده در او راه و میدان کاخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز زین آتبین رسته چون زاد سرو</p></div>
<div class="m2"><p>سمندش خرامان بسان تذرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطش عنبر آگین، بناگوش عاج</p></div>
<div class="m2"><p>به سر بر ز یاقوت و پیروزه تاج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هر پرده پوشیده رویان شاه</p></div>
<div class="m2"><p>سوی بامها برگرفتند راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به انگشت یکدیگران را نهان</p></div>
<div class="m2"><p>نمودند کاینک چراغ جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که میدان ز رخسار او گلشن است</p></div>
<div class="m2"><p>ستاره ز دیدار او روشن است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی هر کسی گفت از این دختران</p></div>
<div class="m2"><p>چه پوشیده رویان شاه و سران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خنک هر که را آتبین در کنار</p></div>
<div class="m2"><p>شبی گیرد، آرام باشدش یار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرا آتبین شوی باشد به مهر</p></div>
<div class="m2"><p>ببوسد سرش بی گمان ماه و مهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز دروازه چون پیش میدان رسید</p></div>
<div class="m2"><p>سبک شاه طیهوریان برگزید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به یک سو شد او با ده و دو پسر</p></div>
<div class="m2"><p>ز گردان کرا دید با او هنر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز طیهوریان هر که بایست برد</p></div>
<div class="m2"><p>یکایک به بازی برِ خویش برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس اندیشه کرد آتبین از میان</p></div>
<div class="m2"><p>که گر با سواران ایرانیان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان سرکشان دست یابد به گوی</p></div>
<div class="m2"><p>از آن کینه آزار گیرند از اوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به طیهور گفت ای سرِ سرفراز</p></div>
<div class="m2"><p>جز این نیست آیین این کارزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه هر کاو به اسب اندر آورد پای</p></div>
<div class="m2"><p>هنر داده باشد مر او را خدای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فراوان نماید سلیح و سُوار</p></div>
<div class="m2"><p>هنر دور و بر باره مانند بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از این نامداران که فرزند تند</p></div>
<div class="m2"><p>بزرگان کجا خویش پیوند تند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی نیمه از سوی من کن نخست</p></div>
<div class="m2"><p>که بخشش چنین است شاها، درست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو دوری کند بخشش از راستی</p></div>
<div class="m2"><p>به کار اندر آید بسی کاستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر آن کاو نداده ست از بخش رای</p></div>
<div class="m2"><p>از آن بخش هرگز نبوده ست جای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز موبد شنیدم که بخشنده بخش</p></div>
<div class="m2"><p>دل مرد دارد دلارای رخش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان کرد طیهور کاو رای دید</p></div>
<div class="m2"><p>چو رایش ز دانش دلارای دید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز فرزند شش تن سوی آتبین</p></div>
<div class="m2"><p>فرستاد و کردند یاران گزین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به میدان چو گوی اندر انداختند</p></div>
<div class="m2"><p>ز هر سو سواران بدو تاختند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در آمد به مغز دلیران ستیز</p></div>
<div class="m2"><p>هوا شد پر از گرد و خاک فریز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همی آتبین بود با یک دو دست</p></div>
<div class="m2"><p>ببردند طیهوریان شاد و مست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وزآن پس درآمد بر افراخت یال</p></div>
<div class="m2"><p>برانگیخت اسب آن یل بی همال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز چوگان چنان داد پرتاب گوی</p></div>
<div class="m2"><p>که نیز آتبین را ندیدند روی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان گوی بر چرخ پرواز کرد</p></div>
<div class="m2"><p>که با ماه گفتی همی راز کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نبودی رسیده به نزد زمین</p></div>
<div class="m2"><p>که بر چرخش انداختی آتبین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان تاختی زیر هنجارگوی</p></div>
<div class="m2"><p>که هم در هوا در رسیدی بر اوی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از این سان همی تاخت تا هفت بار</p></div>
<div class="m2"><p>ز میدان برون کرد گوی آن سوار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از او هر کسی کآن سواری بدید</p></div>
<div class="m2"><p>همی دست خود را به دندان گزید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دلیران ز چوگان کشیدند دست</p></div>
<div class="m2"><p>که گفتی زمین پای اسبان ببست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی هر کسی گفت با این سُوار</p></div>
<div class="m2"><p>نه بازی توان کرد و نه کارزار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر آن جا یکی مرد جنگی شود</p></div>
<div class="m2"><p>وگر مرد نامی ست ننگی شود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ببوسید طیهور رخسار او</p></div>
<div class="m2"><p>چنان شادمان شد ز کردار او</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو گفت کای خسرو سرکشان</p></div>
<div class="m2"><p>تو از فرّ جمشید داری نشان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دل من ز مهرت مبادا تهی</p></div>
<div class="m2"><p>که زیبای تاجی و آن شهی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز تو چشم بدخواه تو دورباد</p></div>
<div class="m2"><p>دل بدسگال تو رنجور باد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بزرگان کوه بسیلا همه</p></div>
<div class="m2"><p>یکایک فتادند در دمدمه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کز این سان به کشّی نباشد سوار</p></div>
<div class="m2"><p>نه چون او پدید آورد روزگار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مر او را به دل نیکخواه آمدند</p></div>
<div class="m2"><p>ز میدان به ایوان شاه آمدند</p></div></div>