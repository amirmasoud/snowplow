---
title: >-
    بخش ۳۲۸ - پذیرفتن کوش پینشهاد تور و سلم را
---
# بخش ۳۲۸ - پذیرفتن کوش پینشهاد تور و سلم را

<div class="b" id="bn1"><div class="m1"><p>چو نزدیک کوش آمد آن تیزکوش</p></div>
<div class="m2"><p>برآن نامه بر کوش بنهاد هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گفتار آن هر دو خسرو شنید</p></div>
<div class="m2"><p>ز شادی تو گفتی دلش برپرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاده را شادمان پیش خواند</p></div>
<div class="m2"><p>در آن نامور پیشگاهش نشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپرسید از آن نامداران نخست</p></div>
<div class="m2"><p>که هستند شادان دل و تندرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرستاده گفت ای جهانگیر شاه</p></div>
<div class="m2"><p>درستند هر دو تو را نیکخواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیغام بگشاد ازآن پس زبان</p></div>
<div class="m2"><p>همی گفت با شاه ازآن ترجمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شادی رخان ارغوان رنگ کرد</p></div>
<div class="m2"><p>به بگماز و جام می آهنگ کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرایی نو آیین بپرداختند</p></div>
<div class="m2"><p>فرستاده را جایگه ساختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی بود یک هفته با رود و می</p></div>
<div class="m2"><p>به کام فرستاده ی نیک پی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هشتم نویسنده را پیش خواند</p></div>
<div class="m2"><p>به کرسی زر پیکرش برنشاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخوبی یکی پاسخ نامه کرد</p></div>
<div class="m2"><p>همه مردمی بر سر خامه کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که همداستانم به گفتارتان</p></div>
<div class="m2"><p>نجویم دگر رنج و آزارتان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر با فریدون بود داوری</p></div>
<div class="m2"><p>به گنج و به لشکر دهم یاوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندارم دریغ از شما اسب و ساز</p></div>
<div class="m2"><p>نه مردان گردنکش کینه ساز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو خواهید کآرام گیرد دلم</p></div>
<div class="m2"><p>بهانه ز روی زمین بگسلم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به پیش فرستاده ی من نخست</p></div>
<div class="m2"><p>.................................</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که چون گردد این پادشاهی به کام</p></div>
<div class="m2"><p>نسازید در پیش من پای دام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان را بدان سان که گفتید نیز</p></div>
<div class="m2"><p>ببخشید و از ما نخواهید چیز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نوشته به مشک این سخنها که هست</p></div>
<div class="m2"><p>گوا کرده بر خویشتن خط دست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو این کرده باشید ایزد گواست</p></div>
<div class="m2"><p>که جان و تن و گنج پیش شماست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرستاده ای کرد با او برون</p></div>
<div class="m2"><p>سخنگوی و داننده و پُرفسون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرآن هر دو پوینده را چیز داد</p></div>
<div class="m2"><p>درم داد و دینارشان نیز داد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شتابان بر سلم و تور آمدند</p></div>
<div class="m2"><p>بنزدیک راهی برون آمدند</p></div></div>