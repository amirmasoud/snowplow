---
title: >-
    بخش ۳۱۱ - رفتن سلم و قارن به کارزار کوش
---
# بخش ۳۱۱ - رفتن سلم و قارن به کارزار کوش

<div class="b" id="bn1"><div class="m1"><p>چو اردیبهشت آمد و روز جوش</p></div>
<div class="m2"><p>کشیدند لشکر سوی رزم کوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهاندیده گوید که خورشید و ماه</p></div>
<div class="m2"><p>ندیده ست تا هست چندان سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزاران هزار و چهارصد هزار</p></div>
<div class="m2"><p>گزیده سپه بود و نیزه گزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین از گرانیش ناله گرفت</p></div>
<div class="m2"><p>از آتش زمین رنگ لاله گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خورشید بر شد ز هامون غبار</p></div>
<div class="m2"><p>برآمد ز گاو و ز ماهی دمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی شیهه اسبان تازی ز بند</p></div>
<div class="m2"><p>ز دل هوش مرّیخ و کیوان بکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهان گشت خاک زمین زیر نعل</p></div>
<div class="m2"><p>هوا گشته از پرنیان زرد و لعل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گاو و به ماهی رسیده ستوه</p></div>
<div class="m2"><p>درخت گران گشته هامون چو کوه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آوای شیپور وز نای نیز</p></div>
<div class="m2"><p>نمود اهرمن را همی رستخیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از این سان سوی اندلس کرد روی</p></div>
<div class="m2"><p>به پیش اندرون قارن رزمجوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وزآن روی کوش جهاندیده باز</p></div>
<div class="m2"><p>یکی لشکر آورده بود او فراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپاهش ندانست گردون که چند</p></div>
<div class="m2"><p>فزون از ستاره به چرخ بلند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دفتر دوباره هزاران هزار</p></div>
<div class="m2"><p>نوشته دبیران دانا شمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان بود کز آبها بگذرد</p></div>
<div class="m2"><p>تن سلم در زیر پی بسپرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو آگاه شد او ز سلم و سپاه</p></div>
<div class="m2"><p>بخندید و گستاخ شد او به راه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفرمود تا مردم رزمساز</p></div>
<div class="m2"><p>همه سوی طارق کشیدند باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سراسر بدان جا کشیدند رخت</p></div>
<div class="m2"><p>نهانی نهادند و کردند سخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زن و بچّه و مردم و چارپای</p></div>
<div class="m2"><p>نماندند کس ریسمانی بجای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شدند ایمن از کار فرزند و زن</p></div>
<div class="m2"><p>کشاورز و دهقان و شمشیر زن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به طارق فرستاد چندان خورش</p></div>
<div class="m2"><p>کز آن بودشان سالیان پرورش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز گاوان، وز گوسفندان گله</p></div>
<div class="m2"><p>برآن کوه بر بی شبان شد یله</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس از راه افرنجه سلم سترگ</p></div>
<div class="m2"><p>بیامد به دریای روم بزرگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به آب اندر افگند کشتی هزار</p></div>
<div class="m2"><p>نخست از همه قارن نامدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دریا گذر کرد با آن سپاه</p></div>
<div class="m2"><p>کز ایران و توران بدو داد شاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرستاد کشتی سوی سلم باز</p></div>
<div class="m2"><p>نشاند اندر او لشکر رزمساز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو یک نیمه لشکر گذر کرد از آب</p></div>
<div class="m2"><p>به کوش آگهی آمد اندر شتاب</p></div></div>