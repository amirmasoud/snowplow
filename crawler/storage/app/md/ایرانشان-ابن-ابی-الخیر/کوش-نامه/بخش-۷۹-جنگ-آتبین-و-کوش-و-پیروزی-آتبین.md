---
title: >-
    بخش ۷۹ - جنگ آتبین و کوش و پیروزی آتبین
---
# بخش ۷۹ - جنگ آتبین و کوش و پیروزی آتبین

<div class="b" id="bn1"><div class="m1"><p>بگفت این و شد تا به نزدیک کوش</p></div>
<div class="m2"><p>بر او حمله آورد چون شیر زوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان هردوان بر هم آویختند</p></div>
<div class="m2"><p>کز اسبان همی خون و خوی ریختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی این برآن و گهی آن بر این</p></div>
<div class="m2"><p>گهی آن به تندی، گهی این به کین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهیب از چنان زخم دو کینه ور</p></div>
<div class="m2"><p>گهی بر سر آمد گهی بر سپر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکستند نیزه، کشیدند تیغ</p></div>
<div class="m2"><p>گهی در نبرد و گهی در گریغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گمانش چنان بود بر آتبین</p></div>
<div class="m2"><p>که با او برابر نیاید به کین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخست از سر زین چو شیر ژیان</p></div>
<div class="m2"><p>به یک دست تیغ و کمر بر میان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو فرّ کیان دید و زور گوان</p></div>
<div class="m2"><p>هنر دید از آن مایه ی خسروان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنر بیشتر کوشش افزون نمود</p></div>
<div class="m2"><p>بدو اندر آمد بکردار دود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرو هشت تیغ و سپر پیش کرد</p></div>
<div class="m2"><p>بزد تیغ کوش از سرش خود برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر او حمله آورد پس آتبین</p></div>
<div class="m2"><p>کشیده چو الماس شمشیر کین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بالا چو تیغ اندر آمد درشت</p></div>
<div class="m2"><p>به یک زخم مربارگی را بکشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به یک زخم برگستوان با سرش</p></div>
<div class="m2"><p>بیفگند وز آن خیره شد لشکرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس آهنگ او کرد با تیغ تیز</p></div>
<div class="m2"><p>بترسید و برداشت راه گریز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن سو تگ آورد سوی سپاه</p></div>
<div class="m2"><p>که بپسود پایش ز خاک سیاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پذیره شدندش سواران چین</p></div>
<div class="m2"><p>رهانید جان از بدِ آتبین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر باره بر پشت باره نشست</p></div>
<div class="m2"><p>بیامد برآورد زه را به شست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببارید بر آتبین بر خدنگ</p></div>
<div class="m2"><p>جهان بر دل لشکرش کرد تنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو دید آتبین آن دلیری ز کوش</p></div>
<div class="m2"><p>بر آورد چون ابر تندر خروش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به تیر آتبین آن زمان دست برد</p></div>
<div class="m2"><p>نمود آن هنرمند را دستبرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نخستین خدنگش رها شد ز شست</p></div>
<div class="m2"><p>به پیشانی بارگی درنشست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خروشید و زد کوش را بر زمین</p></div>
<div class="m2"><p>به زخم دگر دست برد آتبین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به بازو درآمدش زخم درشت</p></div>
<div class="m2"><p>چو شد خسته زو کوش بنمود پشت</p></div></div>