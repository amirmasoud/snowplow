---
title: >-
    بخش ۱۱۶ - گذشتن آتبین از دریا در شب و شکست دادن چینیان
---
# بخش ۱۱۶ - گذشتن آتبین از دریا در شب و شکست دادن چینیان

<div class="b" id="bn1"><div class="m1"><p>وز آن روی بر آتبین با سپاه</p></div>
<div class="m2"><p>درافگند کشتی به دریا و چاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دریا شب تیره آمد برون</p></div>
<div class="m2"><p>ببستند کشتی به آب اندرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی بود ماننده ی آبنوس</p></div>
<div class="m2"><p>بنالید نای و بغرّید کوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دریا برآمد شب تیره میغ</p></div>
<div class="m2"><p>کشید آتبین با سوارانش تیغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لشکر به گردون برآمد خروش</p></div>
<div class="m2"><p>ز خواب اندر آمد سپهدارِ کوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپاهش چنانچون رمه ی بی شبان</p></div>
<div class="m2"><p>بجای طلایه نه کس پاسبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی هرکس از خواب آسیمه سر</p></div>
<div class="m2"><p>بجَست و دوان شد به نزدیک در</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به اسب برهنه برآورد پای</p></div>
<div class="m2"><p>نه بر سر کلاه و نه در بر قبای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلیران گرفتند راه گریغ</p></div>
<div class="m2"><p>شبی سهمناک از پس و پیش تیغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درخشیدن نیزه و تیغ جنگ</p></div>
<div class="m2"><p>همی از شب تیره بزدود رنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکار دلیران جگر بود و سر</p></div>
<div class="m2"><p>که شمشیر سر جُست و زوبین جگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر نیزه و تیغ الماسگون</p></div>
<div class="m2"><p>به دریا رسانید هنجار خون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بپوشید دیهیم خفتان رزم</p></div>
<div class="m2"><p>سر از خواب سنگین، دل از جام بزم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندیدند جای درنگ و پیام</p></div>
<div class="m2"><p>کشیدند شمشیر کین از نیام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برآویختند و برآمیختند</p></div>
<div class="m2"><p>ز خون دلیران گِل انگیختند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو زر آبگون گشت روی زمین</p></div>
<div class="m2"><p>سپهدارِ چین را بدید آتبین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپاه انجمن شد بر او ده هزار</p></div>
<div class="m2"><p>دلیران رزم آزموده سُوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برآویخته با گروهی سپاه</p></div>
<div class="m2"><p>فراوان سپه گشت پیشش تباه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز سر خود برداشت و بر گفت نام</p></div>
<div class="m2"><p>بدانست هر یک که آن یک کدام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز زین کوهه گرز گران برکشید</p></div>
<div class="m2"><p>از او هر کسی خویشتن درکشید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی خشت رز آبداده به دست</p></div>
<div class="m2"><p>ز کینه برآشفته چون پیل مست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزد خویشتن را بدان رزمجوی</p></div>
<div class="m2"><p>سپهدار سوی وی آورد روی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زبان را گشاده به دشنام زشت</p></div>
<div class="m2"><p>برآورد یال و بینداخت خشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گذر کرد خشت گران بر سپهر</p></div>
<div class="m2"><p>نیامد گزندی بر آن تاجور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهانجوی یک زخم زد بر سرش</p></div>
<div class="m2"><p>به خاک اندر آمد سر و مغفرش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپهدار را چون سپه کشته دید</p></div>
<div class="m2"><p>تنش را به خاک اندر آغشته دید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گریزان شدند آن دلیران همه</p></div>
<div class="m2"><p>چو از گرگ گردد گریزان رمه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهانجوی بر پی همی راند اسب</p></div>
<div class="m2"><p>دمان با گروهی چو آذرگشسب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی کشت و چندان که مور و ملخ</p></div>
<div class="m2"><p>ز خون دلیران چین بست یخ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو باد دمان اسب آن سروران</p></div>
<div class="m2"><p>ستوران چین پیششان چون خران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز شمشیر ایشان جز آن کس نَرست</p></div>
<div class="m2"><p>که زنهار گویان ز باره بجست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نهان گشت جایی ز راه گریغ</p></div>
<div class="m2"><p>که دیده برد بر درفشد تیغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شبانگاه چون آتبین گشت باز</p></div>
<div class="m2"><p>به لشکرگه دشمن آمد فراز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به تخت سپهدار چین بر نشست</p></div>
<div class="m2"><p>ز شادی به دل برنهاده دو دست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی گفت کینِ گرامی سُوار</p></div>
<div class="m2"><p>کشیدم یکی بهره از روزگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بزرگان طیهوریان را بخواند</p></div>
<div class="m2"><p>بدان پیشگهشان به رامش نشاند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آن کامگاری یکی سور کرد</p></div>
<div class="m2"><p>که رنج از دل سرکشان دور کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دگر روز فرمود تا خواسته</p></div>
<div class="m2"><p>همه پیش بردندش آراسته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز چیزی کجا از درِ شاه بود</p></div>
<div class="m2"><p>ز سازی که اندر خورِ راه بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جدا کرد و دیگر همه بخش کرد</p></div>
<div class="m2"><p>رخ لشکر از خواسته رخش کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز هشتاد کشتی کجا بار کرد</p></div>
<div class="m2"><p>چهل زو درم کرد و دینار کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چهل پر ز دیبا و از پرنیان</p></div>
<div class="m2"><p>فرستاد بر دست ایرانیان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به نزدیک طیهور کاین بهر توست</p></div>
<div class="m2"><p>اگر چند از این مایه ور شهر توست</p></div></div>