---
title: >-
    بخش ۱۸۷ - نامه ی فرارنگ به پدر درباره ی پیروزی فریدون و بند کردن ضحاک
---
# بخش ۱۸۷ - نامه ی فرارنگ به پدر درباره ی پیروزی فریدون و بند کردن ضحاک

<div class="b" id="bn1"><div class="m1"><p>ز ناگه بیامد سرِ ماهِ نو</p></div>
<div class="m2"><p>فرستاده ی مادرِ شاهِ نو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرارنگ کاو دخت طیهور بود</p></div>
<div class="m2"><p>که از تخت یک چند رنجور بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبشته چنین بود دست دبیر</p></div>
<div class="m2"><p>که فرخنده شاها، تو رامش پذیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که فرزندِ من فر خجسته نهاد</p></div>
<div class="m2"><p>فریدون که بر شاه فرخنده باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز یزدان همه کامگاری بیافت</p></div>
<div class="m2"><p>چو از کوه بر جنگ دشمن شتافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک زخم ضحاک را کرد پست</p></div>
<div class="m2"><p>دو دست و دو پایش به آهن ببست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز یونان کنون جای زندان اوست</p></div>
<div class="m2"><p>همه قلعه ی پرگزند آنِ اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی غلّ برگردنش برنهاد</p></div>
<div class="m2"><p>که تا جاودان کس نداند گشاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه افسون کند کار و نه جادوی</p></div>
<div class="m2"><p>ز یزدان شناسیم این نیکوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که گیتی ز جادوپرستان برست</p></div>
<div class="m2"><p>ز ضحّاکیان کس نیاید بدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه گنج او کرد تاراج، شاه</p></div>
<div class="m2"><p>به ارزانیان بخش کرد و سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو از نامه بشنید شاه این سخن</p></div>
<div class="m2"><p>رمید از روانش غمان کهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز شادی دل و جانش آمد بجوش</p></div>
<div class="m2"><p>خروشی خروشید وزو رفت هوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرستنده بر روی او زد گلاب</p></div>
<div class="m2"><p>چو هشیار شد خسرو نیمخواب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بفرمود تا پیش لشکر بخواند</p></div>
<div class="m2"><p>چو دستور شاه این سخنها براند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز لشکر برآمد به خورشید غو</p></div>
<div class="m2"><p>که پیروز بادا جهاندار نو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرستاده را داد بسیار چیز</p></div>
<div class="m2"><p>چنان شد که چیزش نبایست نیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببخشید گنجی به ارزانیان</p></div>
<div class="m2"><p>کرا بیشتر بود رنج و زیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن مایه ور گنج، بسیار شهر</p></div>
<div class="m2"><p>شد آباد و مردم بسی یافت بهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه شهر می خورد با رود و نای</p></div>
<div class="m2"><p>به یاد فریدون گیهان خدای</p></div></div>