---
title: >-
    بخش ۲۱۵ - آگاهی ایرانیان از نیرنگ کوش و کارزار با دشمن
---
# بخش ۲۱۵ - آگاهی ایرانیان از نیرنگ کوش و کارزار با دشمن

<div class="b" id="bn1"><div class="m1"><p>چو بزدود هور از هوا لاجورد</p></div>
<div class="m2"><p>پراگند بر دشت یاقوت زرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلایه نگه کرد و لشکر بدید</p></div>
<div class="m2"><p>رمیده روان زی سپهبد دوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروشید کای نامداران کین</p></div>
<div class="m2"><p>ز لشکر نه پیداست روی زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم که دشمن گرفته ست راه</p></div>
<div class="m2"><p>وگر خود مدد باشد از پیش شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گفتار بشنید فرّخ قباد</p></div>
<div class="m2"><p>شتابان به اسب اندر آمد چو باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیامد، بدید آن سپاه گران</p></div>
<div class="m2"><p>که پیدا نبودش میان و کران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدانست کآن لشکر دشمن است</p></div>
<div class="m2"><p>ز کردار کوش هزبر افگن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین گفت کآن بدرگ باد سار</p></div>
<div class="m2"><p>نه خیره همی دادمان روزگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدان لشکران گران گوش داشت</p></div>
<div class="m2"><p>که چندین سواران زره پوش داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون کشته و خاک گشته بنام</p></div>
<div class="m2"><p>به ارزنده و دشمنان شادکام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون آمد ای سرکشان کار پیش</p></div>
<div class="m2"><p>نمایید هر یک دلیری ز خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کز ایدر که ماییم تا پیش شاه</p></div>
<div class="m2"><p>فزون است فرسنگ پانصد ز راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر سستی آریم در کارزار</p></div>
<div class="m2"><p>برآرد ز ما دشمن ایدر دمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه روی مدارا نه راه گریز</p></div>
<div class="m2"><p>که دشمن بود در پی و تیغ تیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بفرمود تا پس سپه برنشست</p></div>
<div class="m2"><p>شتابان همی رفت گرزی به دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بجز مردی و رزم چاره نیافت</p></div>
<div class="m2"><p>نخستین بدین لشکر نو شتافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ببینیم تا چند و چونند، گفت</p></div>
<div class="m2"><p>برون آورم رازشان از نهفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بپرسید کاین لشکر گشن چیست؟</p></div>
<div class="m2"><p>بدین لشکری شاه و سالار کیست؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپه را چنین پاسخ آموخت کوش</p></div>
<div class="m2"><p>که دارای مکران بدین دشت دوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرود آمده ست از پی رزم و کین</p></div>
<div class="m2"><p>گزیده سپاهی دلاور ز چین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یاری دارای چین آمده ست</p></div>
<div class="m2"><p>همه دل پر از شور و کین آمده ست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شمرده سوار است سیصد هزار</p></div>
<div class="m2"><p>همه نامدار از در کارزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدین آرزو نیز برخاسته ست</p></div>
<div class="m2"><p>که از کوش رزم شما خواسته ست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>........................................</p></div>
<div class="m2"><p>........................................</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به رزم اندر آمد دو رویه سپاه</p></div>
<div class="m2"><p>نظاره شد از چرخ خورشید و ماه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی میغ پیوست همرنگ قیر</p></div>
<div class="m2"><p>ببارید از آن میغ باران تیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چکاچاک شمشیر و گرز و سنان</p></div>
<div class="m2"><p>همی بستد از دست گردان عنان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به یک زخم چندان سپه کشته شد</p></div>
<div class="m2"><p>که از کشته هامون زمین بسته شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلیران مکران چو شیر ژیان</p></div>
<div class="m2"><p>فتادند در قلب ایرانیان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فروان بکشتند و کردند اسیر</p></div>
<div class="m2"><p>پر از خون همه نیزه و تیغ و تیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سواران چین کنده بگذاشتند</p></div>
<div class="m2"><p>همه رزم را تیغ برداشتند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو دریای چین لشکر از چپّ و راست</p></div>
<div class="m2"><p>درآمد، ز خون بر زمین موج خاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بماندند ایرانیان در میان</p></div>
<div class="m2"><p>نه نیروی گردان نه زور کیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قباد آن چنان با ده و دو هزار</p></div>
<div class="m2"><p>زره دار و برگستوانور سوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزد خویشتن را بکردار کوه</p></div>
<div class="m2"><p>برآن بیکران لشکر همگروه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به یک زخم شد کشته چندان سوار</p></div>
<div class="m2"><p>که از خون همی جوی شد مرغزار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بمالید بر چینیان بر درشت</p></div>
<div class="m2"><p>هزاران بخست و هزاران بکشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو باز آمد از حمله آن کینه خواه</p></div>
<div class="m2"><p>سپه یافت افتاده در قلبگاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دلیران مکران برآورده جوش</p></div>
<div class="m2"><p>چو شیر دمان از پسِ پشت کوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشیده همه نیزه و تیغ جنگ</p></div>
<div class="m2"><p>گیا کرده از خون چو مرجان به رنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>غمی گشت و گرز گران برکشید</p></div>
<div class="m2"><p>یلان را بر او پیش لشکر کشید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو آتش بر آن دشمنان حمله کرد</p></div>
<div class="m2"><p>برآمد چکاچاک تیغ نبرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جهان از تف جنگشان میغ بست</p></div>
<div class="m2"><p>ز خون اخگری بر سر تیغ بست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سوار ار جنان تار و هم پود بود</p></div>
<div class="m2"><p>زمین را ز خون یلان رود بود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سپهبد بدان حمله بدخواه خویش</p></div>
<div class="m2"><p>برون کرد یکسر ز بنگاه خویش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز مکرانیان چند مردان بکشت</p></div>
<div class="m2"><p>که بفسرد بر دسته ی تیغ مشت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بمالید چندان سپه را چنان</p></div>
<div class="m2"><p>که روباه گشتند شیر افگنان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو کوش دلاور چنان دید، گفت</p></div>
<div class="m2"><p>که اکنون چرا باشم اندر نهفت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برون تاخت از قلب با شش هزار</p></div>
<div class="m2"><p>ز برگستوانور دلاور سوار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بزد خویشتن بر سپاه قباد</p></div>
<div class="m2"><p>برافراخت گرز و بغل برگشاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بکشتند نخست از سپاهش بسی</p></div>
<div class="m2"><p>درنگی نیامد به پیشش کسی</p></div></div>