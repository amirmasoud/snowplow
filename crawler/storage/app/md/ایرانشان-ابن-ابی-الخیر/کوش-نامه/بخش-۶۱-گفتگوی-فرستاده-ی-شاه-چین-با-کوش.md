---
title: >-
    بخش ۶۱ - گفتگوی فرستاده ی شاه چین با کوش
---
# بخش ۶۱ - گفتگوی فرستاده ی شاه چین با کوش

<div class="b" id="bn1"><div class="m1"><p>چو دستور، گفتار خسرو شنید</p></div>
<div class="m2"><p>برآمد ز جای، آفرین گسترید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گفت کز دانش ورای شاه</p></div>
<div class="m2"><p>چنین زیبد و جز چنین نیست راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شگفتی یکی داستان بینم این</p></div>
<div class="m2"><p>کجا چرخ رانده ست بر شاه چین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون من به فرّ جهانجوی شاه</p></div>
<div class="m2"><p>مرا او را بیارم بدین بارگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزرگان و دستور گشتند شاد</p></div>
<div class="m2"><p>بر او هر کسی آفرین کرد یاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو داد شاه اسب و بر گستوان</p></div>
<div class="m2"><p>همان ساز رزم و سلیح گران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپوشید به مرد و پس برنشست</p></div>
<div class="m2"><p>خروشان همی رفت نیزه به دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکایک چو پیش طلایه رسید</p></div>
<div class="m2"><p>خروشی چو شیر ژیان برکشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که آن پیل دندان وارونه مرد</p></div>
<div class="m2"><p>بگویید کآید به دشت نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که امروز چون دی نگردد رها</p></div>
<div class="m2"><p>به کام اندر آردش تند اژدها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوار از طلایه بشد همچو باد</p></div>
<div class="m2"><p>شنیده همی کرد بر کوش یاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برآشفت و بر گستوان برفگند</p></div>
<div class="m2"><p>کمان خواست و شمشیر و گرز و کمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهاد از سپه سوی به مرد روی</p></div>
<div class="m2"><p>بغرّید چون شد به نزدیک اوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که دی جان رهانیدی از چنگ من</p></div>
<div class="m2"><p>شوی کشته امروز در جنگ من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدو گفت نرم ای دلاور سوار</p></div>
<div class="m2"><p>که با تو ندارم سرِ کارزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا شاه خاور فرستاد پیش</p></div>
<div class="m2"><p>که از تو بدانم یکی کمّ و بیش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشانی همی جویم از تو نخست</p></div>
<div class="m2"><p>اگر یابم از تو نشانی درست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان دان که کوتاه شد داوری</p></div>
<div class="m2"><p>همه بندگانیم و تو مهتری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به کام تو و ما شود روزگار</p></div>
<div class="m2"><p>شوی بر جهان سربسر کامگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو گفتار به مرد بشنید کوش</p></div>
<div class="m2"><p>عنانش گران شد، بدو داد گوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بتندی به روی وی آورد روی</p></div>
<div class="m2"><p>که از من چه خواهی نشانی بگوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفت به مرد تندی مکن</p></div>
<div class="m2"><p>چو نرمی نمایم بلندی مکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همان مردی و یال دارم که تو</p></div>
<div class="m2"><p>همان گرز و گوپال دارم که تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه در آفرینش ز من برتری</p></div>
<div class="m2"><p>نه در گوهر از آدمی بگذری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر هیچ دانی نشانی دگر</p></div>
<div class="m2"><p>جز این آفرینش به روی تو بر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نشانی که از مادر آورده ای</p></div>
<div class="m2"><p>بدان آفرینش بپرورده ای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگو، کاندر آن بوی بینی و رنگ</p></div>
<div class="m2"><p>وگر خود نداری بجای است جنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین داد پاسخ که بر کتف راست</p></div>
<div class="m2"><p>نشانی ست کافزون نیاید نه کاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی مُهر همچون نگینی سیاه</p></div>
<div class="m2"><p>نشانی دگر زین فزونتر مخواه</p></div></div>