---
title: >-
    بخش ۳۲۷ - نامه فرستادن تور و سلم بنزد کوش و پینشهاد بخش کردن زمین بین کوش و تور و سلم
---
# بخش ۳۲۷ - نامه فرستادن تور و سلم بنزد کوش و پینشهاد بخش کردن زمین بین کوش و تور و سلم

<div class="b" id="bn1"><div class="m1"><p>پسندیده آمد سخنهای تور</p></div>
<div class="m2"><p>یکی نامه کردش دلارای تور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نامه از تور و سلم سترگ</p></div>
<div class="m2"><p>بنزد جهاندیده کوش بزرگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان ای نبرده شه نامدار</p></div>
<div class="m2"><p>که با شاه ما را بدافتاد کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان کرد با ما کجا با تو کرد</p></div>
<div class="m2"><p>ز کردار بی روی و گفتار سرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیفگند ما را از آن مرز و بوم</p></div>
<div class="m2"><p>یکی را به تُرک و یکی را به روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ایرج سپرد آنگهی تاج و تخت</p></div>
<div class="m2"><p>پسندد چنین مردم نیکبخت!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین بد بسنده نکرده ست باز</p></div>
<div class="m2"><p>همی خواست از ما دو تن ساو و باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا گفت و شاید چنین داوری</p></div>
<div class="m2"><p>که فرزند کهتر کند مهتری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو با ما نمودند خوی پلنگ</p></div>
<div class="m2"><p>خود از خویشتن دور کردیم ننگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان را از ایرج بپرداختیم</p></div>
<div class="m2"><p>کنون رای و رَسم دگر ساختیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ضحاک ما را نیاز بربند</p></div>
<div class="m2"><p>همی بگسلد زیر چرم کمند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز مادر تویی خویش و هم خال ما</p></div>
<div class="m2"><p>به تو سخت گردد بر و یال ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو ما هر دو یکدل شدیم اندر این</p></div>
<div class="m2"><p>ز شاه بد آیین بخواهیم کین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو ما را شود کوه و هامون و شهر</p></div>
<div class="m2"><p>ببخشیم روی زمین بر سه بهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهین بهر، بخش تو باد از زمین</p></div>
<div class="m2"><p>گر ایران و گر هند و گر ترک و چین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر باختر هرچه داری به دست</p></div>
<div class="m2"><p>تو را باد از آن به نیاری به دست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی بهره ایران و چین است و هند</p></div>
<div class="m2"><p>همان کشور نیمروز است و سند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به سلم دلاور دهیم آن همه</p></div>
<div class="m2"><p>شبان گردد و نامداران رمه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر بهره سقلاب و روم است باز</p></div>
<div class="m2"><p>همه مرز ترکان و چین و طراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان خاور و ماورالنّهر نیز</p></div>
<div class="m2"><p>ز گیتی مرا باشد آن بهر نیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوم بهره شام است و مصر و یمن</p></div>
<div class="m2"><p>سراسر همه تازیان تا عدن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه باختر هرچه در پیش توست</p></div>
<div class="m2"><p>تورایست کآن جا کم و بیش توست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدان منگر اکنون که سلم جوان</p></div>
<div class="m2"><p>کمر بست بر رزم تو با گوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فراوان از آن رنج دیدی به دشت</p></div>
<div class="m2"><p>که آن روزگاران کنون درگذشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که ما هر دو با دل پر از خون بُدیم</p></div>
<div class="m2"><p>همه زیر بند فریدون بُدیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به فرمان او کرد بایست کار</p></div>
<div class="m2"><p>کنون درگذشت آن چنان روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو امروز گیر، آن گذشته مگیر</p></div>
<div class="m2"><p>ز ما هر دو خویشان درودی پذیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نهادند بر نامه هر دو نگین</p></div>
<div class="m2"><p>فرستاده بسپرد روی زمین</p></div></div>