---
title: >-
    بخش ۸ - در مدح پادشاه اسلام
---
# بخش ۸ - در مدح پادشاه اسلام

<div class="b" id="bn1"><div class="m1"><p>به نام شهنشاه گیتی گشای</p></div>
<div class="m2"><p>فریدون به دانش، سکندر به رای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیومرث نامِ منوچهر چهر</p></div>
<div class="m2"><p>سیاوخش دیدارِ هوشنگ مهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرامرز گردن، فریبرز یال</p></div>
<div class="m2"><p>تهمتن دلِ زال تن، سام یال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو موبد به دین و چو کسری به داد</p></div>
<div class="m2"><p>ز تخم پشنگ و به خوی قباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نیرو چو پیل و به زَهره چو ببر</p></div>
<div class="m2"><p>به کوشش چو دریا، به بهره چو ابر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببیند همه بودنیها به رای</p></div>
<div class="m2"><p>چو کیخسرو از جام گیتی نمای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدوزد به یک تیر با شیر، گور</p></div>
<div class="m2"><p>چو آهوی پی خسته، بهرام گور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستاره سپاه بزرگان او</p></div>
<div class="m2"><p>شهاب است شمشیر ترکان او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گاهِ گیومرث تا گاهِ ما</p></div>
<div class="m2"><p>نبوده ست برتر کس از شاهِ ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز رویین دزم چند رانی سخن</p></div>
<div class="m2"><p>که گشت آن سخن باستانی کهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرستنده ی تخت این شهریار</p></div>
<div class="m2"><p>به مردی فزون است از اسفندیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که از شاهد ز چون برآورد خاک</p></div>
<div class="m2"><p>حصاری و کوهی چنان هولناک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر کوه بر برج پرواز داشت</p></div>
<div class="m2"><p>مگر با ستاره یکی راز داشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه دز بود کآن چرخ را باره بود</p></div>
<div class="m2"><p>بدو اندورن مرد خونخواره بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپاهی مر او را چو دیو رجیم</p></div>
<div class="m2"><p>جهان از تف تیغشان پر ز بیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به شب در سپاهان نیارست خفت</p></div>
<div class="m2"><p>سپاهی و شهری به نزدیک جفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی شاه یک سال رزم آزمود</p></div>
<div class="m2"><p>که با کوه پتیاره چاره نبود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بالا چو سنگ اندر آمد همی</p></div>
<div class="m2"><p>هلاک یکی لشکر آمد همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به زور خدای آن چنان هول جای</p></div>
<div class="m2"><p>درآورد شاه دلاور ز جای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرفتار شد خواجه ی بدگمان</p></div>
<div class="m2"><p>برون کردش از پوست هم در زمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آن بادساران نماندند کس</p></div>
<div class="m2"><p>زهی شیردل، شاه فریادرس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان ایمنی یافت از رنجشان</p></div>
<div class="m2"><p>فرو خورد خاک زمین گنجشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپاهان ز فرّش بنازد همی</p></div>
<div class="m2"><p>که شاه اندر او گاه سازد همی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به تن شاد باد و به دل شادباد</p></div>
<div class="m2"><p>همه بار شمشیر او داد باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هنوز از لبش شیر بوید همی</p></div>
<div class="m2"><p>سخن جز به گفتن نگوید همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو می گرددش سال خود چون بود</p></div>
<div class="m2"><p>فلک زیر پایش چو هامون بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که نه زار دستور هم راز نه</p></div>
<div class="m2"><p>همه رای او با هم آواز نه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بیمش بدرّد دل شیرِ جنگ</p></div>
<div class="m2"><p>ز دادش ننازد به ماهی نهنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دو چندان که شاهان روی زمین</p></div>
<div class="m2"><p>کشیدند صف پیش جویای کین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدین اندکی سال خسرو کشید</p></div>
<div class="m2"><p>که روزی، شکن بر سپاهش ندید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ملکشاه را چشم روشن شود</p></div>
<div class="m2"><p>چو شاه جوان زیر جوشن شود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بنازد دل و جان بنازد همی</p></div>
<div class="m2"><p>به میدان چو شاه اسب تازد همی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به بزم اندرون ماه گردون کش است</p></div>
<div class="m2"><p>به رزم اندرون شاه دشمن کش است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان روشن از مایه ی بخت اوست</p></div>
<div class="m2"><p>زمین ایمن از پایه ی تخت اوست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شنیدی که بر لشکر تازیان</p></div>
<div class="m2"><p>چه آمد ز کردار خسرو زیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر بازگویم یکی دفتر است</p></div>
<div class="m2"><p>ولیکن مرا رای از این دیگر است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بر مرد بیدین سرآمد زمان</p></div>
<div class="m2"><p>گریزان شد آن کس که بُد بدگمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از ایران یکی سوی ایشان شتافت</p></div>
<div class="m2"><p>نهانی چنان کش نشان کس نیافت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شه تازیانش بپذرفت و گفت</p></div>
<div class="m2"><p>که گشتم به چیز و به جان با تو جفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر شهریارت بخواهد ز من</p></div>
<div class="m2"><p>سپر دارم از پیش تو خویشتن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ندانست بیمایه کآن گفت و گوی</p></div>
<div class="m2"><p>چه مایه گزند آردش پیش روی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ندانست کز آفتاب بلند</p></div>
<div class="m2"><p>چگونه گریزد تن مستمند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو آگاه شد شاه شد خواستار</p></div>
<div class="m2"><p>نیامدش گفتار شاه استوار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بپچید گردن ز فرمان و پند</p></div>
<div class="m2"><p>نیامدش پند کسان سودمند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ندانست کآن کس که گردن کشید</p></div>
<div class="m2"><p>همی چادر سرخ در تن کشید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همی گفت او زینهار من است</p></div>
<div class="m2"><p>سپاه شهنشه شکار من است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خروشید کوس از در شهریار</p></div>
<div class="m2"><p>که شاها ز دشمن بر آور دمار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تبیره همی گفت کز بهر دین</p></div>
<div class="m2"><p>به گردن برآور گران گرز کین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ببستند بر کینه ی تازیان</p></div>
<div class="m2"><p>دلیران و گردان خسرو میان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به گردون بر آمد یکی تیره گرد</p></div>
<div class="m2"><p>چو تند اژدها شاه شد رهنورد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو آگاهی آمد به شاه عرب</p></div>
<div class="m2"><p>که آمد شهنشاه والانسب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بجوشید تازی چو مور و ملخ</p></div>
<div class="m2"><p>سیه گشت هامون و دریا و شخ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز نیزه جهان گشت چون بیشه ای</p></div>
<div class="m2"><p>دل هر سواری در اندیشه ای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنان تازیان حمله کردند تیز</p></div>
<div class="m2"><p>که گفتی برآمد یکی رستخیز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بر آن بادپایان چو پرّنده زاغ</p></div>
<div class="m2"><p>دمان و دنان پیش دریا و راغ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همی هرکسی خویشتن را ستود</p></div>
<div class="m2"><p>ز راز زمانه کس آگه نبود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو گفتی به مردی ندارندشان</p></div>
<div class="m2"><p>سواران خسرو شکارندشان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جهانجوی برداشت از سر کلاه</p></div>
<div class="m2"><p>بجای نماز آمد او دادخواه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی گفت کای پاک برتر خدای</p></div>
<div class="m2"><p>تویی آفریننده و رهنمای</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه ساله دانی که از بهر دین</p></div>
<div class="m2"><p>منم برکشیده چنین تیغ کین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو دادی مرا پادشاهی و تخت</p></div>
<div class="m2"><p>کنون هم تو آسان کن این کار سخت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چنان کز زبانش سخن شد رها</p></div>
<div class="m2"><p>پدید آمد از آسمان اژدها</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکی اژدها همچو کوهی سیاه</p></div>
<div class="m2"><p>بترسید از او شهریار و سپاه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سوی خسرو آورد روی از هوا</p></div>
<div class="m2"><p>پراندیشه شد شاه فرمانروا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همانا یکی راز کردش پدید</p></div>
<div class="m2"><p>که از دشمنت جان بخواهم کشید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>وز آن پس سوی دشمن آورد روی</p></div>
<div class="m2"><p>دو لشکر کشیده دو دیده در اوی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو گفتی ذنب جفت مریخ شد</p></div>
<div class="m2"><p>به گیتی چنین روز تاریخ شد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هم اندر زمان نامداری ز راه</p></div>
<div class="m2"><p>سر دشمن آورد نزدیک شاه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>برآمد یکی غلغل و تیره گرد</p></div>
<div class="m2"><p>دل تازیان کرد از آن پر ز درد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سواران تازی گریزان شدند</p></div>
<div class="m2"><p>چو برگ از سر شاخ ریزان شدند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همه دشت پر کُشته و خسته شد</p></div>
<div class="m2"><p>تو گفتی پی اسبشان بسته شد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز پس تیرباران و از پیش آب</p></div>
<div class="m2"><p>به خوردن دل ماهیان را شتاب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بر آن دشت تا سالیان گرگ و شیر</p></div>
<div class="m2"><p>از آن تازیان خورد یابند سیر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>که کینه ز فرّ شهنشاه بود</p></div>
<div class="m2"><p>کرا اژدها هوش بدخواه بود</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شمردن نبایست از آن دیگران</p></div>
<div class="m2"><p>که سالارِ دور است و شاهِ قران</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر آن کس که باشد به دل دشمنش</p></div>
<div class="m2"><p>چو شاه عرب باد بی سر تنش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>من اندر جهان بر مهان سرورم</p></div>
<div class="m2"><p>که شاه جهان را ستایشگرم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چنان داستانی بگفتم ز پیش</p></div>
<div class="m2"><p>به نام جهاندار پاکیزه کیش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>پراگنده گشته ست گرد جهان</p></div>
<div class="m2"><p>نشاید همی جز کنار مهان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بخوانید و بر شه ستایش کنید</p></div>
<div class="m2"><p>ز یزدان بر او بر نیایش کنید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>همین نامه را تاجدار وی است</p></div>
<div class="m2"><p>که گردنده گردون به کام وی است</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همان نامه کش نام شاه آور است</p></div>
<div class="m2"><p>به گوینده بر آفرین درخور است</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چنین داستان تا سرآید جهان</p></div>
<div class="m2"><p>ز من یادگار است نزد مهان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تو دریایی ای شاه و من دُرّ جوی</p></div>
<div class="m2"><p>نهاده به دریا به امّید روی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اگر درّ یابم یکی زین میان</p></div>
<div class="m2"><p>همانا نیاید به دریا زیان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چنان بخششم ده که شاهان دهند</p></div>
<div class="m2"><p>به فرزانه و نیکخواهان دهند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>که من بنده و نیکخواه توام</p></div>
<div class="m2"><p>ستاینده ی تاج و گاه توام</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز یزدان همی خواهمت روز و شب</p></div>
<div class="m2"><p>به نفرین دشمن گشاده دولب</p></div></div>