---
title: >-
    بخش ۲۲۸ - بازگشت جاسوس کوش از ایران
---
# بخش ۲۲۸ - بازگشت جاسوس کوش از ایران

<div class="b" id="bn1"><div class="m1"><p>سواری که از چین فرستاده بود</p></div>
<div class="m2"><p>به ایران و پندش بسی داده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد، بیاورد یکسر نشان</p></div>
<div class="m2"><p>ز شاه و دلیران و گردنکشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گفت کان شهریار بلند</p></div>
<div class="m2"><p>ندارد سرِ رزم و رای گزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به داد و دهش دل نهاده ست شاه</p></div>
<div class="m2"><p>پراگنده بر گرد گیتی سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ایران همه کشور آباد کرد</p></div>
<div class="m2"><p>جهان را پر از بخشش و داد کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ایران زمین بر پراگند گنج</p></div>
<div class="m2"><p>تهی کرد گنج و بکاهید رنج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب و روز دل بسته در کار مرد</p></div>
<div class="m2"><p>شکسته دلِ کین به آزار و درد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل کوش از آن شادمان گشت و گفت</p></div>
<div class="m2"><p>که با اختر نیک بادی تو جُفت</p></div></div>