---
title: >-
    بخش ۳۶۳ - ساختن حوضها برای درمان مصروعان
---
# بخش ۳۶۳ - ساختن حوضها برای درمان مصروعان

<div class="b" id="bn1"><div class="m1"><p>به راه بیابان به جایی رسید</p></div>
<div class="m2"><p>که آبی که بود ایستاده بدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسانی که مصروع بودند و سست</p></div>
<div class="m2"><p>به دل ناتوان و به تن نادرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه خنده ای خوش بر ایشان فتاد</p></div>
<div class="m2"><p>به آب اندر افتاد مانند باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مرده در آن آب بیهوش شد</p></div>
<div class="m2"><p>ازآن آگهی چون برِ کوش شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شتابان بیامد به دیدار آب</p></div>
<div class="m2"><p>کشیدند بیرون هم اندر شتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منادیگری بانگ زد بر سپاه</p></div>
<div class="m2"><p>که دارید از این آب خود را نگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبادا کز این آب هرگز چشید</p></div>
<div class="m2"><p>که پس چادر مرگ در سرکشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بفرمود تا مردگان را ز آب</p></div>
<div class="m2"><p>کشیدند هم در زمان بر شتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو باد شمالی بر ایشان وزید</p></div>
<div class="m2"><p>بهوش آمد و این شگفتی که دید؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبک گشته آن کس که بودی گران</p></div>
<div class="m2"><p>به رنگ و به تن بهتر از دیگران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه سستی و رنج از او گشته دور</p></div>
<div class="m2"><p>شد آن ماتم سخت مانند سور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بفرمود تا ز اندلس هرکه هست</p></div>
<div class="m2"><p>به تن سست و مصروع و بی پای و دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بریدش برآن آب تا مرد سست</p></div>
<div class="m2"><p>بشوید تن و زود گردد درست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسی حوضها نام خود برنبشت</p></div>
<div class="m2"><p>دگر کرد از این سان و دیگر بهشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سلیمان گذر کرد روزی برآن</p></div>
<div class="m2"><p>بدید آن همه حوضها بیکران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفرمود تا آصف برخیا</p></div>
<div class="m2"><p>که دانست کردن همی کیمیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآورد از آن شارستانی به رنج</p></div>
<div class="m2"><p>نهاد اندر او هرچه بودش ز گنج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درآورد دیوارش از گِردِ آب</p></div>
<div class="m2"><p>کشیده سر کنگره بر سحاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نهادش به نیرنگ ازآن سان نهاد</p></div>
<div class="m2"><p>که هرگز درش کس نداند گشاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دیوار او گر برآید همی</p></div>
<div class="m2"><p>زند خنده بر روی مردم همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان شارستان اندر افتد نگون</p></div>
<div class="m2"><p>نداند کسی کان چرا است و چون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به گیتی مر او را نبینند باز</p></div>
<div class="m2"><p>ندانست کس را که چون است راز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرآن کس که او بگذرد بر درش</p></div>
<div class="m2"><p>ز بانگ سگان خیره گردد سرش</p></div></div>