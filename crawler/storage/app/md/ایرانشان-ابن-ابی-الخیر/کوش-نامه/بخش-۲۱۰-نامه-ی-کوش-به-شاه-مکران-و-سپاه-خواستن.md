---
title: >-
    بخش ۲۱۰ - نامه ی کوش به شاه مکران و سپاه خواستن
---
# بخش ۲۱۰ - نامه ی کوش به شاه مکران و سپاه خواستن

<div class="b" id="bn1"><div class="m1"><p>شب آمد طلایه برون کرد کوش</p></div>
<div class="m2"><p>نبیسنده ای خواست بسیار هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی شاه مکران یکی نامه کرد</p></div>
<div class="m2"><p>سخن را روان از سر خامه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که تو راه خوبی همه نسپری</p></div>
<div class="m2"><p>همانا نداری سر کهتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوباره ببستند گردان میان</p></div>
<div class="m2"><p>به رزم و به پیگار ایرانیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپه خواستم از تو هنگام کار</p></div>
<div class="m2"><p>نیامد ز نزدیک تو یک سوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون بار دیگر سوار آمده ست</p></div>
<div class="m2"><p>از ایران یکی کینه خواه آمده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی رزم کردیم و بودیم شاد</p></div>
<div class="m2"><p>شکن بر سپاه قباد اوفتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون مرد جاسوسم آگاه کرد</p></div>
<div class="m2"><p>که دشمن سواری سوی راه کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدد خواست و دانم همی بی گمان</p></div>
<div class="m2"><p>که آید سپاهی زمان تا زمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ایشان سپه دل شکسته شود</p></div>
<div class="m2"><p>همه کارها سخت بسته شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو باید که چندان که داری سپاه</p></div>
<div class="m2"><p>سوی ما فرستی بدین رزمگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدین کار اگر تو درنگ آوری</p></div>
<div class="m2"><p>جهان بر دل خویش تنگ آوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو پرداخته باشم از ایرانیان</p></div>
<div class="m2"><p>به یزدان اگر من گشایم میان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر داد بستانم از تو به کین</p></div>
<div class="m2"><p>به آتش بسوزم تو را بر زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تن خویش دریاب و لختی سپاه</p></div>
<div class="m2"><p>بساز و گسی کن بدین رزمگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگو تا گذر سوی خمدان کنند</p></div>
<div class="m2"><p>سپاهی که آن جا شتابان کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به یک جا بیایند هر دو سپاه</p></div>
<div class="m2"><p>مگر بشکند زآن دل کینه خواه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرستاده سوی ره آورد روی</p></div>
<div class="m2"><p>بدو گفت با شاه مکران بگوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر سستی آری تو در کار باز</p></div>
<div class="m2"><p>بدین بار پرخاش ما را بساز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که بتّرترین دشمن اندر جهان</p></div>
<div class="m2"><p>تویی مرمرا آشکار و نهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرستاده برداشت گرز و کمند</p></div>
<div class="m2"><p>شتابان بشد بر ستور نوند</p></div></div>