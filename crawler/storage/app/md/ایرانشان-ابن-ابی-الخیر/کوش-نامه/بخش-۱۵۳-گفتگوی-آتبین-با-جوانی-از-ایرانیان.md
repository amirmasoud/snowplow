---
title: >-
    بخش ۱۵۳ - گفتگوی آتبین با جوانی از ایرانیان
---
# بخش ۱۵۳ - گفتگوی آتبین با جوانی از ایرانیان

<div class="b" id="bn1"><div class="m1"><p>همی بود در بیشه شاه آتبین</p></div>
<div class="m2"><p>ز گردون دژم وز زمانه به کین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیشه یکی روز شد سوی دشت</p></div>
<div class="m2"><p>جوانی پیاده بر او برگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر او را به بیشه درآورد شاه</p></div>
<div class="m2"><p>به چربی بپرسیدش از رنج راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که آگاهی از کار ضحاک چیست</p></div>
<div class="m2"><p>خبرها از آن مرد ناباک چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوان گفت گیتی به کام وی است</p></div>
<div class="m2"><p>زمین هفت کشور به نام وی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تباه است کار بزرگان دین</p></div>
<div class="m2"><p>تو گویی نبوده ست کس بر زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبینیم مردم که ناباک نیست</p></div>
<div class="m2"><p>نه کس را به فرمان ضحاک نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر سلکت آن مایه ی روزگار</p></div>
<div class="m2"><p>که کوه دماوند دارد حصار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هواخواه جمشیدیان اوست بس</p></div>
<div class="m2"><p>که ضحاک را خود ندارد به کس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرستاد زی او سپه چند بار</p></div>
<div class="m2"><p>ز فرسنگ دیدند روی حصار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوبار او سپه برد زی جنگ نیز</p></div>
<div class="m2"><p>نشایست کردن بدو هیچ چیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی جای دارد که خورشید و باد</p></div>
<div class="m2"><p>به تندی بدو روی نتوان نهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپاهی نشانده ست ضحاک باز</p></div>
<div class="m2"><p>به نزدیک آمل همه رزمساز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان تا از ایرانیان زین سپس</p></div>
<div class="m2"><p>به نزدیک سلکت نیایند کس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدو گفت داری تو جایی نشان</p></div>
<div class="m2"><p>ز فرزند جمشید و آن سرکشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنین داد پاسخ که شد سالیان</p></div>
<div class="m2"><p>که پرسیدم این را ز ایرانیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشان داد مردی که فرزند اوی</p></div>
<div class="m2"><p>به دریای بی بُن نهاده ست روی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نهان گشته بر تیغ کوهی بزرگ</p></div>
<div class="m2"><p>ز دست بداندیش کوش سترگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخندید و پس گفتش ای نیکخواه</p></div>
<div class="m2"><p>به نزدیک سلکت چه مایه ست راه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین داد پاسخ که مرد سوار</p></div>
<div class="m2"><p>به سه روز رفتن توان زی حصار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدو گفت دانی از ایدر تو راه</p></div>
<div class="m2"><p>به کوه دماوند و آن جایگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدانم که راهی نبهره ست، گفت</p></div>
<div class="m2"><p>همه بیشه و جایگاه نهفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو مرد که ای، گفت و این گفت و گوی</p></div>
<div class="m2"><p>ز بهر که داری مرا بازگوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>.................................</p></div>
<div class="m2"><p>.................................</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت گوینده کای پاک رای</p></div>
<div class="m2"><p>من از نیمروزم وز آباد جای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی نامه بردم به ضحّاک شاه</p></div>
<div class="m2"><p>ز گرشاسپ یل، پهلوان سپاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کنون بازگشتم سوی نیمروز</p></div>
<div class="m2"><p>به نزدیک گرشاسپ لشکرفروز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدادش جهاندار دینار چند</p></div>
<div class="m2"><p>زبانش به سوگندها کرد بند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که رازم نگویی تو هرگز به کس</p></div>
<div class="m2"><p>نگویی که دیدم کسی را و بس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جوان گفت کای شاد دل مرزبان</p></div>
<div class="m2"><p>کنون کم به سوگند بستی زبان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سزد گر کنی راز خود آشکار</p></div>
<div class="m2"><p>مرا بازگویی که چون است کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه نامی و تخم و نژادت ز کیست</p></div>
<div class="m2"><p>بدین بیشه بودن تو را رای چیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از ایرانیانیم، گفت آتبین</p></div>
<div class="m2"><p>به ما بر ز ضحّاک، تنگ این زمین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در این بیشه باشم از این سان نهان</p></div>
<div class="m2"><p>مگر مُردری مانَد از وی جهان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جوان گفت از ایران بسی سرفراز</p></div>
<div class="m2"><p>نهان است هر جای و او نیست راز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو شاید که دارای ایران ز کوش</p></div>
<div class="m2"><p>به دریا گریزد، نه جنگ و نه جوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چه آهو بود، ار تو ای ارجمند</p></div>
<div class="m2"><p>به بیشه گریزی ز بهر گزند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برفت آن جوان، شاه در بیشه ماند</p></div>
<div class="m2"><p>همی روزگارش به پرهیز راند</p></div></div>