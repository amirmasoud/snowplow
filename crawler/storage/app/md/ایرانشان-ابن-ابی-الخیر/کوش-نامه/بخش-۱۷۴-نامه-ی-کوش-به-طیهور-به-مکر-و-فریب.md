---
title: >-
    بخش ۱۷۴ - نامه ی کوش به طیهور به مکر و فریب
---
# بخش ۱۷۴ - نامه ی کوش به طیهور به مکر و فریب

<div class="b" id="bn1"><div class="m1"><p>چو آگاه شد کآتبین شد درست</p></div>
<div class="m2"><p>به طیهور پرداخت و بر چاره جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی نامه فرمود با مهر و داد</p></div>
<div class="m2"><p>نخست از همه نام خود کرد یاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دارای خاور شه شیرگیر</p></div>
<div class="m2"><p>به شاه بسیلا هشومند پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان ای سرافراز شاه دلیر</p></div>
<div class="m2"><p>که من گشتم از رزم و پر خاش سیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا با تو پرخاش از آن بود و جنگ</p></div>
<div class="m2"><p>که بود آتبین را برِ تو درنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی خواستم کآن سترگ از میان</p></div>
<div class="m2"><p>شود دور و بر تو نیاید زیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه ما را بماند دگر دردسر</p></div>
<div class="m2"><p>چنانچون نموده ست بار دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از او باز ویران نگردد جهان</p></div>
<div class="m2"><p>ز دریا نیاید برون از نهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو کرد او بسیلا و دریا یله</p></div>
<div class="m2"><p>چرا کینه جویم، چه دارم گله؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا تو به جای پدر باش، و من</p></div>
<div class="m2"><p>چو فرزند دارم تن خویشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان باش با من که شاهان پیش</p></div>
<div class="m2"><p>به هم ساخته همچو پیوند خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیاگان تو نیکدل بوده اند</p></div>
<div class="m2"><p>کسی را به تن رنج ننموده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه نیز از نیاگانت جستند کین</p></div>
<div class="m2"><p>بزرگان ایران و شاهان چین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا این درست است و دانم درست</p></div>
<div class="m2"><p>که پرخاش و کین با بسیلا نجست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مردی و نیکی تو از خویشتن</p></div>
<div class="m2"><p>بگردانی آن بیکران انجمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولیکن تو دانی که گر آن سپاه</p></div>
<div class="m2"><p>فرستم که دارند دریا نگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برنجد بسیلا، چو رنجید پیش</p></div>
<div class="m2"><p>همان به که پیوند باشیم و خویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر لشکر روی گیتی به جنگ</p></div>
<div class="m2"><p>به دربند آید نیابد درنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به نوّی ز سر باز پیمان کنیم</p></div>
<div class="m2"><p>به سوگند دلها گروگان کنیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که ما بد نخواهیم با یکدگر</p></div>
<div class="m2"><p>بود جان یکی گرچه باشد دو سر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو جان اندر آید به سوگند و بند</p></div>
<div class="m2"><p>به دل هر دو ایمن شویم از گزند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرستم بسی ساز و کردار چین</p></div>
<div class="m2"><p>کنم تازه دریا چو بازار چین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنان کز همه سازما هرچه هست</p></div>
<div class="m2"><p>به شهر بسیلا به آید به دست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو دانی که من چون شنیدم ز چین</p></div>
<div class="m2"><p>که آواره شد زآن زمین آتبین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپه باز خواندم همی بی درنگ</p></div>
<div class="m2"><p>ازیرا که با تو مرا نیست جنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وگر تو مرا بی بهانه کنی</p></div>
<div class="m2"><p>به یک دخترم شادمانه کنی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به فرمان خویش اندر آری مرا</p></div>
<div class="m2"><p>به فرنزد کهتر شماری مرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرستم دو چندان که خواهی ز گنج</p></div>
<div class="m2"><p>وگر جان بخواهی، ندارم به رنج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه چین کنم زیر فرمان تو</p></div>
<div class="m2"><p>نیارم برون سر ز پیمان تو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرستمت منشور ماچین به پیش</p></div>
<div class="m2"><p>که آن جا فرستی کس از دست خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بمیرم، چو گویی که پیشم بمیر</p></div>
<div class="m2"><p>بدین آرزو گر شوی دستگیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو در نامه بنمود این کهتری</p></div>
<div class="m2"><p>نهاد از برش مهر انگشتری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بخواند از بزرگان او سرکشی</p></div>
<div class="m2"><p>سخن ساز مردی و جادووشی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت برکش به دریا تو راه</p></div>
<div class="m2"><p>ببر نامه ی من به طیهور شاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سخن هرچه گوید همه یاد دار</p></div>
<div class="m2"><p>ازآن سان که باید تو پاسخ گزار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فرستاده دریا به یک مه برید</p></div>
<div class="m2"><p>ز دربند چون باژبانش بدید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فرود آمد از دور و زورق بداشت</p></div>
<div class="m2"><p>یکی مرد پرسنده را برگماشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز ملّاح پرسید کاین مرد کیست</p></div>
<div class="m2"><p>نشستن بدین مرز از بهر چیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرسته چنین پاسخ آورد باز</p></div>
<div class="m2"><p>که پرخاش کمتر کن ای سرفراز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرستاده ی شاه چینیم، گفت</p></div>
<div class="m2"><p>به دریا بیاییم با رنج جفت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سواری فرستاد سالار بار</p></div>
<div class="m2"><p>به طیهور و آگاه کردش ز کار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برآشفت طیهور و گفت آن چه چیز</p></div>
<div class="m2"><p>مر او را چه کار است با من بنیز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرستاده را پیش من راه نیست</p></div>
<div class="m2"><p>بتر زو مرا هیچ بدخواه نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو گفت دستور کای شهریار</p></div>
<div class="m2"><p>همی خوار داری تو دشمن، مدار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فرستاده ی دشمنت را بخوان</p></div>
<div class="m2"><p>سخن گوی با او و بنشان به خوان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که گر غرقه خواهد شدن دشمنت</p></div>
<div class="m2"><p>دو دست اندر آویزد از دامنت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بمان تا شود آبش اندر جهان</p></div>
<div class="m2"><p>پس آن گه ز دستش تو دامن رهان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فرستاده ی شاه را پیش خویش</p></div>
<div class="m2"><p>ببین تا چه دارد از آن تیره کیش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز دستور چون پند بشنید شاه</p></div>
<div class="m2"><p>پذیره فرستاد لختی سپاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فرستاده چون اندر آمد به شهر</p></div>
<div class="m2"><p>تنش رنج دریا بسی یافت بهر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سه روزش گرامی همی داشتند</p></div>
<div class="m2"><p>چهارمش ز یتخت بگذاشتند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زمین را ببوسید و نامه بداد</p></div>
<div class="m2"><p>به مُهرش نگه کرد و پس برگشاد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یکایک ز سر تا به پایان بخواند</p></div>
<div class="m2"><p>از آن کار، طیهور خیره بماند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فرستاده را گفت رو بازگرد</p></div>
<div class="m2"><p>برآسای یک هفته از رنج و درد</p></div></div>