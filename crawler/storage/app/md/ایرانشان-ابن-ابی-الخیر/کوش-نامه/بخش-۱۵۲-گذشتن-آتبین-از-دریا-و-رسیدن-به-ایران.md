---
title: >-
    بخش ۱۵۲ - گذشتن آتبین از دریا و رسیدن به ایران
---
# بخش ۱۵۲ - گذشتن آتبین از دریا و رسیدن به ایران

<div class="b" id="bn1"><div class="m1"><p>از آن آتبین شادمان گشت سخت</p></div>
<div class="m2"><p>جهان تیره گون گشت و بربست رخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون کرد پس جامه ی شاهوار</p></div>
<div class="m2"><p>چو بازارگانان برآراست کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب همی تیز راندی و روز</p></div>
<div class="m2"><p>فرود آمدی شاه لشکر فروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پرهیز خود را همی داشتند</p></div>
<div class="m2"><p>همه مرز سقلاب بگذاشتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسیدند نزدیک دریای ژرف</p></div>
<div class="m2"><p>خزان آمد و کوه بگرفت برف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بازارگانان بسیار ساز</p></div>
<div class="m2"><p>ز سقلاب و بلغار گشتند باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه پیش دریا کشیدند رخت</p></div>
<div class="m2"><p>به نزدیک آن شاه پیروز بخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهانجوی از ایشان سه کشتی خرید</p></div>
<div class="m2"><p>به دریا درافگند و ره درکشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی راند پیوسته با آن گروه</p></div>
<div class="m2"><p>زمانی ز رفتن نیامد ستوه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دریا برون شد به مرز خزر</p></div>
<div class="m2"><p>نیارست بودن در آن دشت و در</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر باره اسمه ز آن جا بجست</p></div>
<div class="m2"><p>به دریای گیلان شد و در نشست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی راند کشتی سه هفته فزون</p></div>
<div class="m2"><p>به نزدیک آمل چو آمد برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بیشه درون رفت و پنهان ببود</p></div>
<div class="m2"><p>از این زندگانی نگویی چه سود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر بد همان بودی اندر زمین</p></div>
<div class="m2"><p>که آمد به روی گزین آتبین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همانا نماندی کسی بی گزند</p></div>
<div class="m2"><p>نه کم سایه مردم نه شاه بلند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه سخت است جان و تن آدمی</p></div>
<div class="m2"><p>که از رنج و سختی نیابد کمی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز بد، بی گمانی گریزنده به</p></div>
<div class="m2"><p>همی با هزاران بلا زنده به</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که خویشی ندارد کسی با خدای</p></div>
<div class="m2"><p>نه آسانی افزون به دیگر سرای</p></div></div>