---
title: >-
    بخش ۲۹۵ - گردیدن کوش به گردِ کشور
---
# بخش ۲۹۵ - گردیدن کوش به گردِ کشور

<div class="b" id="bn1"><div class="m1"><p>وزآن پس بزد نای رویین به دشت</p></div>
<div class="m2"><p>ز مغرب سوی اندلس بازگشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی گردِ کشور برآمد نخست</p></div>
<div class="m2"><p>ز بیداد کشور سراسر بشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سه شهر دگر کرد از آن سنگ خار</p></div>
<div class="m2"><p>که نتوان گشادن به مردان کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی را از آن طلطمه نام کرد</p></div>
<div class="m2"><p>بدان مرز یکچند آرام کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوم شهر ترجاله، شیرین، دگر</p></div>
<div class="m2"><p>همه راه بر آب دریا گذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی بر لب آب عنبر بیافت</p></div>
<div class="m2"><p>ز لشکر همه کس بجُستن شتافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان عنبرین بود کز بوی اوی</p></div>
<div class="m2"><p>همه ساله بودی دمان بوی اوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی جانور دید نیز اندر آب</p></div>
<div class="m2"><p>دو دیده فروزنده چون آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنومند چون گربه ی تیز چنگ</p></div>
<div class="m2"><p>همه موی بر پشت خورشید رنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن موی، بافنده ی تیز ویر</p></div>
<div class="m2"><p>یکی جامه بافند همچون حریر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دریا گر انداختی سالیان</p></div>
<div class="m2"><p>برون آمدی باز خشک از میان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگشتی یکی تار از آن موی تر</p></div>
<div class="m2"><p>چنین جامه ای بود بازیب و فر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ده رنگ گشتی همی هر زمان</p></div>
<div class="m2"><p>ز دیدار او دل شدی شادمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی گر بجوید همانا هزار</p></div>
<div class="m2"><p>ز دینار سرمایه آید به کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به دریا از آن جانور هست نوز</p></div>
<div class="m2"><p>همان عنبر خوش خزان و تموز</p></div></div>