---
title: >-
    بخش ۲۰۸ - رزم کردن کوش با قباد و پیروزی قباد
---
# بخش ۲۰۸ - رزم کردن کوش با قباد و پیروزی قباد

<div class="b" id="bn1"><div class="m1"><p>گزین کرد کوش از سپه صد هزار</p></div>
<div class="m2"><p>زره پوش و رزم آزموده سوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه با سلیح و سواران جنگ</p></div>
<div class="m2"><p>همه تیز کرده چو الماس چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوم شب درفش مهی برفراخت</p></div>
<div class="m2"><p>چو سیل روان آن سپه را بتاخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سواری فرستاد تا بنگرید</p></div>
<div class="m2"><p>بدان دشت هامون طلایه ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه بانگ یلان و نه آوای پاس</p></div>
<div class="m2"><p>جهان را دل از تیرگی در هراس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی هول چون دود دوزخ سیاه</p></div>
<div class="m2"><p>به پرده نهان کرده رخسار ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوار آمد و کشو را بازگفت</p></div>
<div class="m2"><p>رخ کوش مانند گل برشکفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رکیبش گران گشت و ران زد بر اسب</p></div>
<div class="m2"><p>بسی تیزتر گشت از آذرگشسب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دشمن چو تنگ اندر آورد شاه</p></div>
<div class="m2"><p>زمانی فرو داشت یکسر سپاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز آن پس برآورد لشکر غریو</p></div>
<div class="m2"><p>یکی حمله کردند مانند دیو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دم نای رویین برآمد به ماه</p></div>
<div class="m2"><p>هوا تیره تر شد ز گرد سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تگ تازی اسبان و آواز تیز</p></div>
<div class="m2"><p>تو گفتی برآمد یکی رستخیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فروغ سر نیزه و تیغ جنگ</p></div>
<div class="m2"><p>زمین را همی روشنی داد و رنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به رزم اندر آن را که بر دشت برد</p></div>
<div class="m2"><p>به کنده درافتاد و بشکست خرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی کاو سوی راه رست اوفتاد</p></div>
<div class="m2"><p>همی بازخورد او به تیغ قباد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپهبد به گردن برآورد گرز</p></div>
<div class="m2"><p>به نام فریدون با فرّ و برز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خروش آمد از لشکر و بانگ کوس</p></div>
<div class="m2"><p>ز گرد آسمان بر زمین داد بوس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو بانگ سپاه آمد از دست کین</p></div>
<div class="m2"><p>دورویه گشادند گردان کمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چپ و راست شمشیر خونریز بود</p></div>
<div class="m2"><p>به پیش اندرون نیزه ی تیز بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپهبد چو بر چینیان چیر گشت</p></div>
<div class="m2"><p>زمین را ز خون یلان سیر گشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه شب همی داد تا روز پاک</p></div>
<div class="m2"><p>ز خون سپه رنگ مرجان به خاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نظاره شده بر یکی گوشه کوش</p></div>
<div class="m2"><p>به لشکر سپرده دل و رای و هوش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو دید آن که از راست وز چپ سپاه</p></div>
<div class="m2"><p>درآمد بجوشید بر جایگاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گمانی چنان برد سالار چین</p></div>
<div class="m2"><p>که گردان وی ساختند آن کمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی داد دلشان به گفتار خوش</p></div>
<div class="m2"><p>دو لشکر در آن شب چنان کینه کش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز کوس غریوان، ز آواز نای</p></div>
<div class="m2"><p>ندانست لشکر همی سر ز پای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز شمشیر از آن سان چکاچاک بود</p></div>
<div class="m2"><p>کز آن هول مریخ را باک بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دوان بی سوار اسب و گردان به جنگ</p></div>
<div class="m2"><p>گسسته لگام و شکسته خدنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز خون یلان گشته رنگ ستور</p></div>
<div class="m2"><p>چو ابلق، تن خنگ و ابلق چو بور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرفته دل کوش را آن هوس</p></div>
<div class="m2"><p>که از دشمن آن شب نمانده ست کس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپیده دم از کوه چون بر فروخت</p></div>
<div class="m2"><p>چو آتش شد و کوش را دل بسوخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگه کرد و لشکر همه کشته دید</p></div>
<div class="m2"><p>ز خون دشت و در یکسر آغشته دید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سواران ایران همه ساخته</p></div>
<div class="m2"><p>همه نیزه و تیغ کین آخته</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی مر یکی را همی زد به تیر</p></div>
<div class="m2"><p>دگر دیگری را همی بست اسیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز مغزش برآمد یکی تیره دود</p></div>
<div class="m2"><p>که گفتی از این گیتی آگه نبود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همان گه بدانست کاو را قباد</p></div>
<div class="m2"><p>چو مردم یکی دام بر ره نهاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز کنده شد آگاه و کار کمین</p></div>
<div class="m2"><p>بلرزید بر جای دارای چین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنین گفت از آن پس که بگشاد لب</p></div>
<div class="m2"><p>که شوم است کار شبیخون شب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر آن رزم پیشین به نزدیک شهر</p></div>
<div class="m2"><p>نبودی همین خواستم بود بهر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس از درد دل حمله آورد کوش</p></div>
<div class="m2"><p>تبه کرد بسیار پولاد پوش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو از کوش دید آن ستیزه قباد</p></div>
<div class="m2"><p>بدو تاخت و گفت ای بد دیوزاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپه را کشیدی به دام هلاک</p></div>
<div class="m2"><p>کنونت نه شرم است از ایزد نه باک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از این لشکر گشن رای تو نیست</p></div>
<div class="m2"><p>که زنده بماند سواری دویست!</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بماناد تخت تو از تو تهی</p></div>
<div class="m2"><p>مبادی تو با شادی و فرّهی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگفت این و با ویژگان حمله کرد</p></div>
<div class="m2"><p>ز گردان چینی برآورد گرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به زخم گران برهم افگندشان</p></div>
<div class="m2"><p>بدان حمله از جای برکندشان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گروهی سواران گریزان شدند</p></div>
<div class="m2"><p>دگر یکسر از باره ریزان شدند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپه را همی کوش دل داد و پند</p></div>
<div class="m2"><p>به پندش نرفتند پیش گزند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گریزان همه بازگشتند تفت</p></div>
<div class="m2"><p>چو کوش آن چنان دید بر پی برفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وزآن جای برگشت پیروز و شاد</p></div>
<div class="m2"><p>سپاه آفرین خوان شده بر قباد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>قباد و دلیران پس اندر دمان</p></div>
<div class="m2"><p>زنان تیغ و زوبین و کفک افگنان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چنین تا به لشکرگه چین برفت</p></div>
<div class="m2"><p>چه مایه برفت و چه مایه گرفت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از آن صد هزاران گزیده سوار</p></div>
<div class="m2"><p>که با کوش بودند در آن کارزار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جز از سی هزاران نیامد بجای</p></div>
<div class="m2"><p>شکسته کلاه و گسسته قبای</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برآمد خروش از میان سپاه</p></div>
<div class="m2"><p>همی هر کسی بر زمین زد کلاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو کوش آن چنان بخت برگشته دید</p></div>
<div class="m2"><p>سران سپه را همه کشته دید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خروش سپه دید و زاری شنید</p></div>
<div class="m2"><p>همه مویه و سوگواری بدید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی گِرد لشکر برآمد به دشت</p></div>
<div class="m2"><p>به هر خیمه و خرگهی برگذشت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که گر سوگ دارد کسی در سپاه</p></div>
<div class="m2"><p>کنم در زمانش بسختی تباه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>برآرم دمار از روان کسی</p></div>
<div class="m2"><p>که برکشته زاری نماید بسی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از آن غم نیارست مردم چخید</p></div>
<div class="m2"><p>ز بیمش همه کس دَم اندر کشید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همه شب بفرمود خواندن سران</p></div>
<div class="m2"><p>سواران جنگی و گندآوران</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به گفتارشان کرد خرسند و شاد</p></div>
<div class="m2"><p>چنین گفت کاین بدنژاده قباد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فریبنده بوده ست و من بی گمان</p></div>
<div class="m2"><p>ز کنده سرآمد سپه را زمان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نه ما را به مردی نمود این نهیب</p></div>
<div class="m2"><p>بگسترد در پیش دام فریب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بدو خواستم کردن این بد که کرد</p></div>
<div class="m2"><p>دگرگونه شد گنبد لاجورد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در این داستان مرد را رامشی ست</p></div>
<div class="m2"><p>که بالای هر دانشی، دانشی ست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گر از کنده بودی مرا آگهی</p></div>
<div class="m2"><p>شبیخون ببودی مر از ابلهی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کنون بودنی بود و شد کینه سخت</p></div>
<div class="m2"><p>به یزدان اگر من برآیم به تخت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مگر کشته یا بسته پیشم قباد</p></div>
<div class="m2"><p>سراپرده و تخت داده بباد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شما دل مدارید از این کار تنگ</p></div>
<div class="m2"><p>قباد و من و تیغ و میدان جنگ</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>فزون نیست پنجه هزارش سوار</p></div>
<div class="m2"><p>برآید سپاهم دو ره صد هزار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ندارد قباد آن دل از هیچ روی</p></div>
<div class="m2"><p>که آرد سپه سوی ما جنگجوی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>وگر بیش باید شما را سپاه</p></div>
<div class="m2"><p>ز تبّت بخواهم نه دور است راه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز خمدان و مکران دگر لشکری</p></div>
<div class="m2"><p>بخواهم از آن هر سپاهی سری</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به یک ماه چندان بیارم سپاه</p></div>
<div class="m2"><p>که خورشید گم گردد از گرد راه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو برگیرم از راه رنج قباد</p></div>
<div class="m2"><p>سپه را به ایران درآرم چو باد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>سر تخت و تاج فریدون به خاک</p></div>
<div class="m2"><p>برآرم، ندارم ز کس ترس و باک</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>از آن گشت شادان و خرّم سپاه</p></div>
<div class="m2"><p>گرفتند نیرو ز گفتار شاه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سپهبد چو برگشت پیروز بخت</p></div>
<div class="m2"><p>برون کرد جوشن برآمد به تخت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سران را بخواند و به خوردن نشست</p></div>
<div class="m2"><p>به یاد فریدون یزدان پرست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>یلان را مستی فراوان ستود</p></div>
<div class="m2"><p>بسی چیز بخشید و خوبی نمود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>دلیری نمودید امروز گفت</p></div>
<div class="m2"><p>که با جانتان آفرید باد جفت</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>از این رنج و سختی کنون لاجرم</p></div>
<div class="m2"><p>شما شاد خوارید و دشمن به غم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همه رنجها بی گمان بر دهد</p></div>
<div class="m2"><p>چو کوشا شوی، تخت و زیور دهد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نهان است یاقوت در کان سنگ</p></div>
<div class="m2"><p>ولیکن به رنج آورندش به چنگ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به رنح آن بزرگان فرّخ نیا</p></div>
<div class="m2"><p>همی ساختند از گیا کیمیا</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>شما را فریدون به پاداش این</p></div>
<div class="m2"><p>ببخشد همه کشور و مرز چین</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کنون کینِ نستوه و ایرانیان</p></div>
<div class="m2"><p>کشیدیم از این لشکر چینیان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>امیدم چنان است از کردگار</p></div>
<div class="m2"><p>که یاری دهد تا برآرم دمار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>از این دیو دیدار دارای چین</p></div>
<div class="m2"><p>که گم باد نامش به روی زمین</p></div></div>