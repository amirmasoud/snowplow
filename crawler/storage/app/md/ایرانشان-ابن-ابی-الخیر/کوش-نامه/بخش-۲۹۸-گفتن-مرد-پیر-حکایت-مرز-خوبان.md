---
title: >-
    بخش ۲۹۸ - گفتن مرد پیر حکایت مرز خوبان
---
# بخش ۲۹۸ - گفتن مرد پیر حکایت مرز خوبان

<div class="b" id="bn1"><div class="m1"><p>چنین گفت گوینده ی داستان</p></div>
<div class="m2"><p>ز گفتار آن پاکدل راستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که روزی به بگماز بنشست کوش</p></div>
<div class="m2"><p>جهان شد پُر از غلغل و نای و نوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشستند با او بزرگان بسی</p></div>
<div class="m2"><p>سخن رفت هرگونه از هر کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خوبان هر کشور و مرز و بوم</p></div>
<div class="m2"><p>هم از ترک، وز چین، وز مرز روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جایی رسید از درازی سخن</p></div>
<div class="m2"><p>کز آن انجمن گفت مردی کهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که یکسر جهان را بگشتم همه</p></div>
<div class="m2"><p>زمین زیر پی بر نوشتم همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندیدم به خوبی و دیدار اوی</p></div>
<div class="m2"><p>به رنگ و به گفتار و بالا و موی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنانچون به مرز خلایق زنان</p></div>
<div class="m2"><p>چنان ماهرویان و سیمینبران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو سروند اگر ماه تابد ز سرو</p></div>
<div class="m2"><p>سرینها چو گور و میانها چو غرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل اندام همچون گل اندر بهار</p></div>
<div class="m2"><p>سپیدی چو برف و سیاهی چو قار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه ریدکان دلبر و خوش زبان</p></div>
<div class="m2"><p>چو خورشید روی و چو مرجان لبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز دیدارشان دل نماند به جای</p></div>
<div class="m2"><p>بپوشد همی زلفشان زیر پای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه غالیه زلف و خورشید خدّ</p></div>
<div class="m2"><p>همه یاسمنبر، همه سرو قدّ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن مرد گویا چو بشنید کوش</p></div>
<div class="m2"><p>دلش خیره شد، پهن بگشاد گوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدو گفت کاین کشور دل گداز</p></div>
<div class="m2"><p>مرا بازگو تا کدام است باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که آباد بادا به خوبان زمین</p></div>
<div class="m2"><p>بویژه که باشند خوبان چنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین گفت گوینده کاین مرز و بوم</p></div>
<div class="m2"><p>شمارد همی مرد دانا زروم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز دریا گذشتن بباید نخست</p></div>
<div class="m2"><p>پس آن ره به عجلسکس آید درست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وزآن جایگه باز یک ماهه راه</p></div>
<div class="m2"><p>بباید بریدن به بیگاه و گاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر ماه بشکوبش آیدش پیش</p></div>
<div class="m2"><p>زمینی خوش و مردمی خوب کیش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزآن جا بباید شدن بیست روز</p></div>
<div class="m2"><p>پس آن مرز پیش آیدت دلفروز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پُر از خوبرویان آراسته</p></div>
<div class="m2"><p>پُر از بادپایان و پُرخواسته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین گفت، چون کوش از آن سان شنید</p></div>
<div class="m2"><p>که ما را بدان مرز باید کشید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمار از دلیران برآریمشان</p></div>
<div class="m2"><p>به فرمان خویش اندر آریمشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببینیم تا شهریاران که اند</p></div>
<div class="m2"><p>همان ماهرویان چه خوب و چه اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز گوینده هرکس به دل کین گرفت</p></div>
<div class="m2"><p>بدو مرد و زن باز نفرین گرفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که این مرد ما را به راهی فگند</p></div>
<div class="m2"><p>کز آن راه یابیم بیم و گزند</p></div></div>