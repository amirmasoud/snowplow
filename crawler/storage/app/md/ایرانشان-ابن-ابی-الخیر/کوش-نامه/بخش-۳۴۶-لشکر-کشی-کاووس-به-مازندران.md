---
title: >-
    بخش ۳۴۶ - لشکر کشی کاووس به مازندران
---
# بخش ۳۴۶ - لشکر کشی کاووس به مازندران

<div class="b" id="bn1"><div class="m1"><p>بدان ره کشیدش فزونی و آز</p></div>
<div class="m2"><p>که لشکر به کشور همی خواند باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان لشکرش بر در انبوه شد</p></div>
<div class="m2"><p>که روی زمین آهن و کوه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمرده برآمد همی هفت بار</p></div>
<div class="m2"><p>ز گردان و گردنکشان صد هزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره مرز مازندران برگرفت</p></div>
<div class="m2"><p>سپاهش همه دست بر سر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی رفت در پیشِ کاووس، کوش</p></div>
<div class="m2"><p>سپاهش چنان گشن و پولادپوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ایران به مصر آمد آن شاه کی</p></div>
<div class="m2"><p>همی بود یک هفته با رود و می</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی روستا بود دور از گروه</p></div>
<div class="m2"><p>ز رقّه به یک سو میان دو کوه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی ریف خواندند مردان به نام</p></div>
<div class="m2"><p>بدو اندرون مردمان شادکام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان دو کوه اندرون است راه</p></div>
<div class="m2"><p>همی دامن کوه سنگ سیاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درازی آن کوه ده منزل است</p></div>
<div class="m2"><p>همه سنگ خارا، نه خاک و گِل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برآن راه کاووسِ کی را ببرد</p></div>
<div class="m2"><p>همان لشکر و نامداران گُرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدان، تا سرِ راههای سوان</p></div>
<div class="m2"><p>ببندد برآن دشمنان گر توان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان تیز لشکر گرفتند راه</p></div>
<div class="m2"><p>که کردند چندان سپاهش تباه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فسونی به کاووس کی بردمید</p></div>
<div class="m2"><p>ز راه سوانش به نوبه کشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرآن مرزها کرد ویران و پَست</p></div>
<div class="m2"><p>بسی گوهر و زرش آمد به دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بکُشتند چندان از آن بی بنان</p></div>
<div class="m2"><p>که ماندند بی شوی و کودک زنان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آگه شد شاه مازندران</p></div>
<div class="m2"><p>که آورد کوش آن سپاه گران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>.................................</p></div>
<div class="m2"><p>.................................</p></div></div>