---
title: >-
    بخش ۲۳ - اسکندر در برابر پیکر کوشِ پیل دندان، سالار چین
---
# بخش ۲۳ - اسکندر در برابر پیکر کوشِ پیل دندان، سالار چین

<div class="b" id="bn1"><div class="m1"><p>همی راند یک روز و یک شب سیاه</p></div>
<div class="m2"><p>رسیدند نزدیک سنگی سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتی بر سر سنگ دید از رخام</p></div>
<div class="m2"><p>به نزدیک او شد شه نیکنام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبشته چنین یافت بر دست اوی</p></div>
<div class="m2"><p>که این پیکر کوش وارونه خوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه پیل دندان و سالار چین</p></div>
<div class="m2"><p>خداوند فرمان و تاج و نگین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی کامرانی که اندر جهان</p></div>
<div class="m2"><p>نبیند کس آن کاو بُدند از مهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نراند کس آن کاو بر انده ست نیز</p></div>
<div class="m2"><p>سرانجام بگذشت و بگذاشت چیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برفت و به دستش همه باد ماند</p></div>
<div class="m2"><p>خراب آمد و گیتی آباد ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر آگاه گردید از کار من</p></div>
<div class="m2"><p>ز فرمان و نیرو و کردار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز رای و ز مردی و گنج و سپاه</p></div>
<div class="m2"><p>ز رزم و ز بزم و ز تخت و کلاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نباید که بندد دل اندر جهان</p></div>
<div class="m2"><p>که نوش آشکار و شرنگ از نهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سه پانصد به گیتی بماندم درون</p></div>
<div class="m2"><p>همه شهریاران به تیغم زبون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکسته به دستم همی شد درست</p></div>
<div class="m2"><p>ز خاک پی اسب من زر برست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز سندان گذر کرد زوبین من</p></div>
<div class="m2"><p>چنین آمدی باد نوشین من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنونم فروریخت اندامها</p></div>
<div class="m2"><p>چنین بود خواهد سرانجامها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکندر فرو ریخت از دیده آب</p></div>
<div class="m2"><p>همی گفت گیتی فسانه ست و خواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر صد بمانیم وگر صد هزار</p></div>
<div class="m2"><p>همین بود خواهد سرانجام کار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا کاشک زین دانشی راستان</p></div>
<div class="m2"><p>کسی کردی آگاه از این داستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدانستمی کار و کردار کوش</p></div>
<div class="m2"><p>که از سنگ دیدیم دیدار کوش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پر اندیشه ز آن جایگه برگرفت</p></div>
<div class="m2"><p>شب و روز یک ماه دیگر برفت</p></div></div>