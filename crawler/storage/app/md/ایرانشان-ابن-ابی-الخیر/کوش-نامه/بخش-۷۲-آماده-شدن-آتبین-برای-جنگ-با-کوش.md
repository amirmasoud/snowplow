---
title: >-
    بخش ۷۲ - آماده شدن آتبین برای جنگ با کوش
---
# بخش ۷۲ - آماده شدن آتبین برای جنگ با کوش

<div class="b" id="bn1"><div class="m1"><p>همان گه بفرمود تا زآن گروه</p></div>
<div class="m2"><p>تنی صد شدند از بر تیغ کوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی راه دشمن نگهداشتند</p></div>
<div class="m2"><p>خروش از بر چرخ بگذاشتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان شب یکی کنده فرمود شاه</p></div>
<div class="m2"><p>کشیدند بیل و تبرها به راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آن ره یکی کنده کردند ژرف</p></div>
<div class="m2"><p>درازا و بالا و پهنا شگرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنه بر سر کُه کشیدند نیز</p></div>
<div class="m2"><p>جز از خیمه دیگر نماندند چیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوی کوه دیدند پر میوه دار</p></div>
<div class="m2"><p>سراسر همه چشمه و مرغزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مژده سوی آتبین آمدند</p></div>
<div class="m2"><p>همی زآسمان بر زمین آمدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که این کوه سرتاسر آب و گیاست</p></div>
<div class="m2"><p>به گیتی چنین جایگاهی کجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین داد پاسخ که گر کوش دوش</p></div>
<div class="m2"><p>رها کرد ما را و شد سوی کوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نکرد آفریننده ما را یله</p></div>
<div class="m2"><p>ز یزدان نداریم هرگز گله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نبسته ست هرگز دری بر کسی</p></div>
<div class="m2"><p>که نشگاد درها بر او بر بسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین جای را کی تواند ستد</p></div>
<div class="m2"><p>مگر بخت بد بر من آرد لگد</p></div></div>