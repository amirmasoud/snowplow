---
title: >-
    بخش ۱۰۹ - فرستادن دیهیم و پیام کوش به بهک
---
# بخش ۱۰۹ - فرستادن دیهیم و پیام کوش به بهک

<div class="b" id="bn1"><div class="m1"><p>چو پیغام بشنید و نامه بخواند</p></div>
<div class="m2"><p>ز شادی بخندید و خیره بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفت اکنون برو ساز کن</p></div>
<div class="m2"><p>در گنجهای پدر بازکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاد نامه به هر کشوری</p></div>
<div class="m2"><p>به هر نامداری و هر مهتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سراسر سپه را به درگاه خواند</p></div>
<div class="m2"><p>بدان سان که در چین سورای نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گزین کرد از آن لشکری چل هزار</p></div>
<div class="m2"><p>ستوده سوار از درِ کارزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سالارْ دیهیم داد آن سپاه</p></div>
<div class="m2"><p>یکی نیکدل، شاه را نیکخواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت از ایدر به دریا شتاب</p></div>
<div class="m2"><p>نگهدار یکسر گذرگاه آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان کن که پرّنده مرغ هوا</p></div>
<div class="m2"><p>شود زی جزیره، نداری روا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که آن دشمنان گر بدانند باز</p></div>
<div class="m2"><p>از ایشان شود کار بر ما دراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ماچین بخواه آنچه باید تو را</p></div>
<div class="m2"><p>بهک مردمیها نماید تو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی نامه فرمود کردن بدوی</p></div>
<div class="m2"><p>همه مهربانی، همه رنگ و بوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون رفت خواهم همی بامهان</p></div>
<div class="m2"><p>بزودی به درگاه شاه جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببینمش و رنک و کردیم باز</p></div>
<div class="m2"><p>تو ای نیکدل، نیکخو، نیک ساز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه ساله هستی نکوخواه ما</p></div>
<div class="m2"><p>به دل دوست بودی تو را شاه ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرستاده ام سرکشی با سپاه</p></div>
<div class="m2"><p>که دارد گذرگاه دریا نگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آنچ او بخواهد دریغی مدار</p></div>
<div class="m2"><p>ز کشتی و از آلت کارزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز پوشیدنی و ز گستردنی</p></div>
<div class="m2"><p>هم از چارپایان، هم از خوردنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان کن که من باز گردم ز شاه</p></div>
<div class="m2"><p>به پیش من آزادی آرد به راه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فرستاده را داد و لشکر برفت</p></div>
<div class="m2"><p>چو دیهیم و لشکر به دریا برفت</p></div></div>