---
title: >-
    بخش ۳۰۵ - گفتگوی فریدون با قارن در کار کوش
---
# بخش ۳۰۵ - گفتگوی فریدون با قارن در کار کوش

<div class="b" id="bn1"><div class="m1"><p>غمی شد فریدون چو آگاه شد</p></div>
<div class="m2"><p>سوی چاره ی رزم بدخواه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود تا قارن آمدش پیش</p></div>
<div class="m2"><p>بدو گفت کای پهلو خوب کیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سستی نمودیم با دیوزاد</p></div>
<div class="m2"><p>کلاه از بر چرخ گردون نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر کشوری بهره ای بستده ست</p></div>
<div class="m2"><p>زمین خلایق بهم بر زده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستاند همی باژ یک نیمه روم</p></div>
<div class="m2"><p>جهان گشت زیر نگینش چو موم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپه سوی خود خواند و روزی بداد</p></div>
<div class="m2"><p>در گنج و تیغ و زره برگشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو تو کارها را نگیری نگاه</p></div>
<div class="m2"><p>شود کارت از دست و گردد تباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهدار قارن همی ساز کرد</p></div>
<div class="m2"><p>در گنج پُرمایه را باز کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپه سوی خود خواند و روزی بداد</p></div>
<div class="m2"><p>در گنج و شمشیر کین برگشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو در کار گردون نگه کرد شاه</p></div>
<div class="m2"><p>ستاره شمر گشت بیگاه و گاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کامه ی کوش بدخواه دید</p></div>
<div class="m2"><p>سر تاج او برتر از ماه دید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو با او چخیدن ندید ایچ رای</p></div>
<div class="m2"><p>چنین گفت با قارن نیکرای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که هرچ اندر این آسمان اختر است</p></div>
<div class="m2"><p>پرستنده ی کوش بداختر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان است صد سال و هشتاد سال</p></div>
<div class="m2"><p>که او را به گیتی نباشد همال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه آسیب یابد ز چرخ بلند</p></div>
<div class="m2"><p>نه از هیچ روی آید او را گزند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز پیگار بگشاد قارن میان</p></div>
<div class="m2"><p>سوی خانه رفتند ایرانیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وزآن روی شاهی همی راند کوش</p></div>
<div class="m2"><p>گهی رزم در پیش و گه نای و نوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به شمشیر بگشاد سقلاب و روم</p></div>
<div class="m2"><p>زمین گشت پیشش چو بر مُهر موم</p></div></div>