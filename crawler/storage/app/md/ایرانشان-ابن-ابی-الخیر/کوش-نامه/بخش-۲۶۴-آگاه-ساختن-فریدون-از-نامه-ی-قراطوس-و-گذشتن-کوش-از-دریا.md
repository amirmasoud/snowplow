---
title: >-
    بخش ۲۶۴ - آگاه ساختن فریدون از نامه ی قراطوس و گذشتن کوش از دریا
---
# بخش ۲۶۴ - آگاه ساختن فریدون از نامه ی قراطوس و گذشتن کوش از دریا

<div class="b" id="bn1"><div class="m1"><p>چو آمد بر کوش و نامه بداد</p></div>
<div class="m2"><p>یکایک بر او ترجمان کرد یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآشفت سخت و پسایید دست</p></div>
<div class="m2"><p>پس آن نامه بر نامه ی خویش بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیونی برافگند نزدیک شاه</p></div>
<div class="m2"><p>به نامه درون گفت کای پیشگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قراطوس گردن بپیچد همی</p></div>
<div class="m2"><p>کنون رزم ما را بسیچد همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آن نامه کردم به نزدیک شاه</p></div>
<div class="m2"><p>ز دریا گذر خواست کردن سپاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازآن پس رسانم به شاه آگهی</p></div>
<div class="m2"><p>اگر رنج بینم، اگر فرّهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیون رفت و او رفتن آغاز کرد</p></div>
<div class="m2"><p>به دریا گذشتن بسی ساز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نپنداشت هرگز قراطوس شاه</p></div>
<div class="m2"><p>که او بگذراند به دریا سپاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این داستان هیچ با کش نبود</p></div>
<div class="m2"><p>نه بر چشم او کوش چیزی نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دو مه چهل پاره کشتی بساخت</p></div>
<div class="m2"><p>به دریا بسی بادبان برفراخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بازارگانان بسی ساخت نیز</p></div>
<div class="m2"><p>بخواست و به مردم بینباشت چیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخستین گذر کرد با سی هزار</p></div>
<div class="m2"><p>از ایران، وز چین گزیده سوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس از پس فرستاد کشتی سپاه</p></div>
<div class="m2"><p>گذر کرد یکسر به نزدیک شاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سراپرده برزد برآن پهن دشت</p></div>
<div class="m2"><p>که دیده ز دیدار او خیره گشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لب آب پیوسته بادامنش</p></div>
<div class="m2"><p>سپاه اندر آمد به پیرامنش</p></div></div>