---
title: >-
    بخش ۲۴۳ - جنگ تن به تن قارن و کوش و گرفتار شدن کوش
---
# بخش ۲۴۳ - جنگ تن به تن قارن و کوش و گرفتار شدن کوش

<div class="b" id="bn1"><div class="m1"><p>سپیده چو بر چرخ پرواز کرد</p></div>
<div class="m2"><p>درِ روشنی بر جهان باز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاورد گنجور ساز نبرد</p></div>
<div class="m2"><p>برِ کوش بنهاد و برگشت مرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان گه بپوشید دارای چین</p></div>
<div class="m2"><p>سلیحی سگالیده از بهر کین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تبّت یکی آبداده زره</p></div>
<div class="m2"><p>بپوشید و برزد به بندش گره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که بر وی نکردی سلیح ایچ کار</p></div>
<div class="m2"><p>نه شمشیر و نه تیر جوشن گذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی خود چینی به سر برنهاد</p></div>
<div class="m2"><p>کمر بر میان بست و دل برگشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برافگند بر بور برگستوان</p></div>
<div class="m2"><p>برون رفت با لشکری از گوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سواری هزار از پسِ پشت او</p></div>
<div class="m2"><p>یکی هندوی تیغ در مشت او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کنده گذر کرد و آمد به دشت</p></div>
<div class="m2"><p>به ناورد روی زمین برنوشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قارن فرستاد پیغام زود</p></div>
<div class="m2"><p>که خورشید بر چرخ بالا نمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمانی ست تا من به ناوردگاه</p></div>
<div class="m2"><p>همی چشم دارم که آیی به راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخندید قارن ز پیغام او</p></div>
<div class="m2"><p>تو گفتی همی دید فرجام او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی گفت گیتی سرآید همی</p></div>
<div class="m2"><p>که کوش این دلیری نماید همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدین پیشدستی بترساندم</p></div>
<div class="m2"><p>کز این سان به ناوردگه خواندم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کمر خواست و ساز یلی پهوان</p></div>
<div class="m2"><p>برافگند بر خنگ برگستوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سواری هزار از میان سپاه</p></div>
<div class="m2"><p>گزین کرد و آمد به آوردگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قباد سپهدار و نستوه پشت</p></div>
<div class="m2"><p>یکی نیزه چون مارپیچان به مُشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قباد دلاور همی لابه کرد</p></div>
<div class="m2"><p>که بگذار تا من شوم همنبرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مگر زو بخواهم همی کین خویش</p></div>
<div class="m2"><p>رسانم به کام این جهان بین خویش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو گفت قارن که این خود مگوی</p></div>
<div class="m2"><p>که کمتر کنی نزد ما آبروی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی آن که با او نداری تو پای</p></div>
<div class="m2"><p>چو با گرز و کین اندر آید ز جای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و دیگر که ما خواستیم این نبرد</p></div>
<div class="m2"><p>تو گرد در بیوفایی مگرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه با تو نبرد آزماید یکی</p></div>
<div class="m2"><p>بدین دشت گیرد درنگ اندکی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بهانه کند بازگردد به شهر</p></div>
<div class="m2"><p>نماند به ما جز غم و رنج بهر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفت این و پس جنگ را زان نمود</p></div>
<div class="m2"><p>سوی لشکر چینیان شد چو دود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برون رفت کوش از میان سپاه</p></div>
<div class="m2"><p>بیامد بر قارن رزمخواه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ببستند پیمان که از لشکری</p></div>
<div class="m2"><p>نیاید کسی پیش این داوری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وزآن پس به شمشیر بردند دست</p></div>
<div class="m2"><p>دو شیر شکاری، دو پیلان مست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی حمله کردند چون باد و گرد</p></div>
<div class="m2"><p>بنالید از ایشان زمین نبرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بانگ دلیران و از زخم تیغ</p></div>
<div class="m2"><p>ز سینه همی جست راه گریغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو شد تیغ رخنه، بینداختند</p></div>
<div class="m2"><p>به نیزه نبردی دگر ساختند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سنانها شکستند بر یکدگر</p></div>
<div class="m2"><p>نیامد همی زخمشان کارگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به هر بند و چاره پسودند دست</p></div>
<div class="m2"><p>سنان یکی دیگری را نخست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنان خسته گشتند یکبارگی</p></div>
<div class="m2"><p>که بیهوش گشتند بر بارگی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زمانی ز هم بازگشتند و چشم</p></div>
<div class="m2"><p>کشیدند بر یکدیگر بر ز خشم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو آسوده گشتند، جوشان شدند</p></div>
<div class="m2"><p>چو شیران جنگی خروشان شدند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دگر باره بر هم گشادند دست</p></div>
<div class="m2"><p>سر از زخم گرز گران کرده مست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان شد که بگسست خونشان ز رگ</p></div>
<div class="m2"><p>نه در مرد زور و نه در باره تگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدان سستی آویخته شاه چین</p></div>
<div class="m2"><p>چو باد اندر آمد سری پر ز کین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بزد گرز و قارن سپر پیش داشت</p></div>
<div class="m2"><p>که زور از هماورد خود بیش داشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو دارای چین از وی اندر گذشت</p></div>
<div class="m2"><p>خروشید قارن برآن پهن دشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برآورد گرز و برافراخت یال</p></div>
<div class="m2"><p>هماورد او سر بدزدید و یال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بزد بر سر اسب گرز گران</p></div>
<div class="m2"><p>بیفتاد و در زیر او ماند ران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپهبد هم از باد گرزی دگر</p></div>
<div class="m2"><p>بزد بر سر کوش پرخاشخر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سراسیمه شد، هوش از او دور گشت</p></div>
<div class="m2"><p>ز زخم گران شاه رنجور گشت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپهبد به اسب اندر آمد چو باد</p></div>
<div class="m2"><p>بینداخت گرز و بدو بر فتاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو دیدند از آن سان سواران چین</p></div>
<div class="m2"><p>یکی حمله کردند با درد و کین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وز این روی نستوه و فرّخ قباد</p></div>
<div class="m2"><p>برانگیختند اسب مانند باد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بدان چینیان خویشتن بر زدند</p></div>
<div class="m2"><p>همه زخم بر سینه و سر زدند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنین تا لب کنده هفتاد مرد</p></div>
<div class="m2"><p>بکشتند و برخاست گرد نبرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>قباد آمد و هر دو دستش ببست</p></div>
<div class="m2"><p>بر اسبش فگندند مانند مست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مبادا که شاه آزماید نبرد</p></div>
<div class="m2"><p>مگر بخت گویدش کو راه برد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو پیروز برگشت سالار گو</p></div>
<div class="m2"><p>از ایران سپه پاک برخاست غو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که پیروز بادا فریدون به جنگ</p></div>
<div class="m2"><p>تن دشمنانش ز خون لاله رنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هم اندر زمان کوش را بند کرد</p></div>
<div class="m2"><p>زمانه دل خویش را پند کرد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به بندی ببستش که یک پاره بود</p></div>
<div class="m2"><p>نه هرگز گشادنش را چاره بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نگهبان او کرد مردی هزار</p></div>
<div class="m2"><p>ز لشکر گزیده دلیران کار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنین است فرجام کار جهان</p></div>
<div class="m2"><p>چو نوش آشکارا، شرنگ از نهان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکی را برآرد به چرخ از مغاک</p></div>
<div class="m2"><p>همو بازگرداندش زیر خاک</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از او گرچه برداشتی بهر خویش</p></div>
<div class="m2"><p>به تو بر گمارد همان زهر خویش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اگر دل به کامت بیاراید اوی</p></div>
<div class="m2"><p>چنان دان که رویت بگزاید اوی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو گر هوشیاری در او دل مبند</p></div>
<div class="m2"><p>که راهش تباه است و کارش گزند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جهان پهلوان قارن شاه گیر</p></div>
<div class="m2"><p>بیامد بجای نیایش چو تیر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همی گفت کای برتر از برتران</p></div>
<div class="m2"><p>به فرمان تو باد و کوه گران</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو دادی مرا زور و این دسترس</p></div>
<div class="m2"><p>تو پیروز کردی، ننازم به کس</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو برگشت از جایگاه نماز</p></div>
<div class="m2"><p>ز لشکر دلیران گردنفراز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بیاورد و رامشگران را بنیز</p></div>
<div class="m2"><p>بخواند و بخورد و ببخشید چیز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بدان شادمانی شبش روز کرد</p></div>
<div class="m2"><p>که یزدانش از بخت پیروز کرد</p></div></div>