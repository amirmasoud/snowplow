---
title: >-
    بخش ۱۴۹ - گفتگوی آتبین و طیهور و پیر ملّاح
---
# بخش ۱۴۹ - گفتگوی آتبین و طیهور و پیر ملّاح

<div class="b" id="bn1"><div class="m1"><p>دگر روز شد پیش شاه، آتبین</p></div>
<div class="m2"><p>فراوانش بستود و کرد آفرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گفت کای شهریار دلیر</p></div>
<div class="m2"><p>دل از دیدن تو نگشته ست سیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیکن همی ترسم از روزگار</p></div>
<div class="m2"><p>که پیش اندر آید دگرگونه کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بمانیم بی نام تا جاودان</p></div>
<div class="m2"><p>بماند جهان هم به دست بدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود پادشاهی از این تخمه پاک</p></div>
<div class="m2"><p>همه نام ما بازگردد به خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین گفت پس آتبین پیش شاه</p></div>
<div class="m2"><p>چرا رفتمی خود بدین ژرف چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که ایدر مرا ایمنی هست و کام</p></div>
<div class="m2"><p>شب و روز با شادکامی و جام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به من تافته سایه ی تاجِ شاه</p></div>
<div class="m2"><p>به هر مردمی کرده زی من نگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فراوان از این گونه پوزش نمود</p></div>
<div class="m2"><p>سخن پوزش آلود بتوان شنود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفت طیهور کای نامجوی</p></div>
<div class="m2"><p>ز من بخت یکباره برگاشت روی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان بود کامم که تا زنده ام</p></div>
<div class="m2"><p>نماند جدا از تو بیننده ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که از موبدان سخن کس نراند</p></div>
<div class="m2"><p>که اندر جهان چند خواهیم ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو پیشم نهادی کنون چند چیز</p></div>
<div class="m2"><p>که پاسخ ندارم من آن را بنیز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سگالش چنان کن که از رفتنت</p></div>
<div class="m2"><p>ندارد رگ آگاهی اندر تنت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس آن پیر ملّاح را پیش خواند</p></div>
<div class="m2"><p>که بر تنش جز پوست چیزی نماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز زردی و خشکی چو نالی شده</p></div>
<div class="m2"><p>ز پیری دو تا همچو دالی شده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر و دست لرزان چو از باد بید</p></div>
<div class="m2"><p>بریده ز خرّم بهاران امید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه توش و توان و نه نیرو و رگ</p></div>
<div class="m2"><p>نه در دست جنبش نه در پای تگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندانست طیهور از آتبین</p></div>
<div class="m2"><p>همی خواند بر هر دوان آفرین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو بنواخت او را و بنشاخت شاه</p></div>
<div class="m2"><p>بدو گفت کای پیر گشته دو تاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دریا شناسان تویی اوستاد</p></div>
<div class="m2"><p>یکی بر تنت رنج باید نهاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تنی چند از ایران زمین ایدرند</p></div>
<div class="m2"><p>که هم خویش مایند و هم سرورند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به ایران همی رفت خواهند باز</p></div>
<div class="m2"><p>به راه کُه قاف و راهی دراز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازیرا که نتوان شد از راه چین</p></div>
<div class="m2"><p>که پرلشکر دشمن است آن زمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از این راه رو، گر نداری به رنج</p></div>
<div class="m2"><p>که گنج است با رنج و با رنج گنج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر این مردمان را رسانی به کوه</p></div>
<div class="m2"><p>بیفزایدت پیش هر کس شکوه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به دریا دهم مر تو را مهتری</p></div>
<div class="m2"><p>هَمَت گنج باشد هَمَت برتری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهاندیده پیر شکسته زبان</p></div>
<div class="m2"><p>به پاسخ چنین گفت کای مرزبان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا سال سیصد برآمد فزون</p></div>
<div class="m2"><p>نماند اندر اندام من هیچ خون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>توانایی و دانش از من رمید</p></div>
<div class="m2"><p>چگونه توانم همی ره برید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دانش توان رفت بر راه راست</p></div>
<div class="m2"><p>کند راه گم، هرکه دانش بکاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز پیری و درویشی ای شهریار</p></div>
<div class="m2"><p>بتر نیست پتیاره در روزگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رسیده ست از این هر دو بر من ستم</p></div>
<div class="m2"><p>نه پای و نه دست و نه زرّ و درم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همان چار دختر رسیده به شوی</p></div>
<div class="m2"><p>چو ماه و چو عنبر به روی و به موی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز درویشی ای خسرو پر خرد</p></div>
<div class="m2"><p>بدیشان همی هیچ کس ننگرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر شهریار این غم از جان من</p></div>
<div class="m2"><p>کند دور و یازد به درمان من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مر آن دختران را بسازد جهیز</p></div>
<div class="m2"><p>ز دریا نترسم نه از رستخیز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از این آب بی بُن برآرم دمار</p></div>
<div class="m2"><p>شوم تازه بر راه دریا گذار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>غم دختر و رنج و بی مایگی</p></div>
<div class="m2"><p>به مردم درآرد سبک سایگی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همی سالیان صد بر آمد فزون</p></div>
<div class="m2"><p>که آن راه را من ندیدم که چون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ولیکن به نیروی یزدان پاک</p></div>
<div class="m2"><p>به فرّ شهنشاه بی بیم و باک</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مر ایرانیان را به خشکی برم</p></div>
<div class="m2"><p>به دریای زنگارگون بگذرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بخندید طیهور، گفت اینت مرد!</p></div>
<div class="m2"><p>به پیری کسی این دلیری نکرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو گفت برخیز و اندُه مدار</p></div>
<div class="m2"><p>که ما دختران را بسازیم کار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به دستور فرمود تا کرد ساز</p></div>
<div class="m2"><p>بیاراست آن دختران را جهاز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو برتافت فرّ مهی بر سرش</p></div>
<div class="m2"><p>ز خواهنده انبوه شد بر درش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ببردند، هر مهتری دختری</p></div>
<div class="m2"><p>بدادی اگر داشتی دیگری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جهاندیده ملّاح بی رنج شد</p></div>
<div class="m2"><p>جوان شد چو بی رنج، با گنج شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درم کژّها راست دارد همی</p></div>
<div class="m2"><p>درم کارها را برآرد همی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>.............................</p></div>
<div class="m2"><p>.............................</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وزآن پس بیاراست کشتی چهار</p></div>
<div class="m2"><p>به بالا یکایک بسان حصار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سه کشتی همی خوردنی بار کرد</p></div>
<div class="m2"><p>چهارم درم کرد و دینار کرد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به طیهور داد آن دگر هرچه بود</p></div>
<div class="m2"><p>برآن بر بسی مهربانی فزود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بنه برنهادند و بربست بار</p></div>
<div class="m2"><p>فرستاد یکسر به دریا کنار</p></div></div>