---
title: >-
    بخش ۳۰۷ - فرستادن فریدون، سلم را به روم و فرمانبرداری شاهان روم از وی
---
# بخش ۳۰۷ - فرستادن فریدون، سلم را به روم و فرمانبرداری شاهان روم از وی

<div class="b" id="bn1"><div class="m1"><p>به هنگام رفتن به سلم سترگ</p></div>
<div class="m2"><p>چنین گفت پس شهریار بزرگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون راست گردد تو را مرز و بوم</p></div>
<div class="m2"><p>یکی آگهی جوی از آن دیو شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز آن ستمکاره آگاه کن</p></div>
<div class="m2"><p>سواری تو با نامه بر راه کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که من قارن رزم زن را ز گاه</p></div>
<div class="m2"><p>فرستم به نزدیک تو با سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببندید یکباره او را میان</p></div>
<div class="m2"><p>مگر زآن میانش سرآید زمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر او را ز روی زمین کم کنید</p></div>
<div class="m2"><p>دل من بیکبار بی غم کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز درگاه خسرو چو سلم سترگ</p></div>
<div class="m2"><p>سوی روم شد با سپاهی بزرگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب نامداران و شاهان روم</p></div>
<div class="m2"><p>به خاک اندر آمد ببوسید بوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه زیردستی نمودند پاک</p></div>
<div class="m2"><p>به فرمان او شد بلند و مغاک</p></div></div>