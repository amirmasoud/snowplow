---
title: >-
    بخش ۱۷۵ - فریب خوردن طیهور و پیمان کردن با کوش
---
# بخش ۱۷۵ - فریب خوردن طیهور و پیمان کردن با کوش

<div class="b" id="bn1"><div class="m1"><p>وزآن پس یکی ترجمان را بخواند</p></div>
<div class="m2"><p>نهانی سخن چند با او براند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفت امشب تو با رود و می</p></div>
<div class="m2"><p>به نزد فرستاده رو تیز پی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببین و سخن گوی با او بسی</p></div>
<div class="m2"><p>ز شاه و ز سالار، وز هرکسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وزآن پس بپرسش که چون است این</p></div>
<div class="m2"><p>که جوید همی آشتی شاه چین؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزآن پس که بوده ست بدخواه ما</p></div>
<div class="m2"><p>همی آشتی جوید از شاه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه امید دارد بدو شاه چین؟</p></div>
<div class="m2"><p>ز کارش چه بارآید او بر زمین؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پاسخ گزارد همه یاد دار</p></div>
<div class="m2"><p>مشو تیز و مستیز و دل شاددار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشد در زمان با می آن نیکخواه</p></div>
<div class="m2"><p>سخن گفت از آن سان که فرمود شاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرستاده دریافت گفتار اوی</p></div>
<div class="m2"><p>چنان پرسش سخت و بازار اوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین داد پاسخ که بشنو ز من</p></div>
<div class="m2"><p>چو آگه شدی راز دار این سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون کز میان دور گشت آتبین</p></div>
<div class="m2"><p>ز طیهور دارد همی شاه چین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که گر او سپه سوی خاور کشد</p></div>
<div class="m2"><p>به چین، شاه طیهور را برکشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر بخت کوش اندر آرد به خواب</p></div>
<div class="m2"><p>سراسر کند چین و ماچین خراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو چین سوخت و برگشت و برداشت چیز</p></div>
<div class="m2"><p>به طیهور کین کرد حیران به تیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر سوی دریا فرستد سپاه</p></div>
<div class="m2"><p>که دارند طیهور و ره را نگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فزون بایدش لشکری صد هزار</p></div>
<div class="m2"><p>که هر ماه گنجیش ناید بکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگر مهتری همچو شاه آتبین</p></div>
<div class="m2"><p>از او سر کشد آید ایدر به چین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نماید همان درد سر کاو نمود</p></div>
<div class="m2"><p>در این آشتی چند گونه ست سود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر شاه ما این بدیدی به خواب</p></div>
<div class="m2"><p>رسیدی سرِ وی سوی آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو بشنید از او این سخن ترجمان</p></div>
<div class="m2"><p>سوی شاه طیهور شد در زمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مر این داستان را بدو بازگفت</p></div>
<div class="m2"><p>رخ شاه مانند گل بر شکفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخورد آن سخنهای جادو فشان</p></div>
<div class="m2"><p>فریبش بپذرفت چون بیهشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستاده را روز دیگر بخواند</p></div>
<div class="m2"><p>فراوانش بنواخت و پیشش نشاند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت دارای چین را ز من</p></div>
<div class="m2"><p>درودی ده از پیش آن انجمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگویش که پیوند را نیست روی</p></div>
<div class="m2"><p>دگر هرچه خواهی تو از من بجوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که تا آتبین دخترانم را ببرد</p></div>
<div class="m2"><p>دل من همه راه انده سپرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی سخت سوگند کردیم یاد</p></div>
<div class="m2"><p>به دارای گیتی که گیتی نهاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که از دخترم دگر زین سپس</p></div>
<div class="m2"><p>از ایدر نیاید برون هیچ کس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ولیکن ز خویشان فراوان کَسَند</p></div>
<div class="m2"><p>که شایسته ی شاه لشکر کشند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرستم من او را که خواهدش پیش</p></div>
<div class="m2"><p>به پیوند او هر دو گردیم خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگر هرچه خواهی بجای آورم</p></div>
<div class="m2"><p>همه گنجها زیر پای آورم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز چیزی کز این مرز خیزد نخست</p></div>
<div class="m2"><p>فرستم بسی تا نبایدت جست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>میان بسته دارم به فرمان تو</p></div>
<div class="m2"><p>دگر نشکنم هیچ پیمان تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدین گونه پاسخ بدادش، بنیز</p></div>
<div class="m2"><p>فرستاده را داد هرگونه چیز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو از خواسته شادمان گشت، گفت</p></div>
<div class="m2"><p>که بادی تو با کام و با نام جفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نهانی مرا گفت دارای چین</p></div>
<div class="m2"><p>که گر شاه برگردد از خشم و کین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر آید سوی آشتی از گزند</p></div>
<div class="m2"><p>به سوگند و پیوند او را ببند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که از ما نباشد بداندیش بیش</p></div>
<div class="m2"><p>به ما خواهد او هرچه خواهد به خویش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ندارد ره کاروانی به بند</p></div>
<div class="m2"><p>نه بازارگان را رساند گزند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه بردارد از چیزشان هیچ چیز</p></div>
<div class="m2"><p>نه بدخواهد و نه پسندد بنیز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به دستور گفت آن شه شادبخت</p></div>
<div class="m2"><p>که از ما بترسد همی کوش، سخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که شد زود سوگند را خواستار</p></div>
<div class="m2"><p>چنین نیکرای آمد و بردبار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همان گه فرستاده را داد دست</p></div>
<div class="m2"><p>فرستاده را او به پیمان ببست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فرستاد با او سواری بهوش</p></div>
<div class="m2"><p>که بستاند او نیز پیمان ز کوش</p></div></div>