---
title: >-
    بخش ۱۵۱ - رسیدن آتبین به خشکی
---
# بخش ۱۵۱ - رسیدن آتبین به خشکی

<div class="b" id="bn1"><div class="m1"><p>همی راند تا کوه شد بر دو شاخ</p></div>
<div class="m2"><p>برآمد به خشکی و جای فراخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شهر و زمین دید و دشت آتبین</p></div>
<div class="m2"><p>نهاد آن سر تا جور بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رخساره خاک سیه را پسود</p></div>
<div class="m2"><p>بسی پیش یزدان نیایش نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گفت کای برتر از آفتاب</p></div>
<div class="m2"><p>تویی آفریننده ی خاک و آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فرمان توست آب دریا و باد</p></div>
<div class="m2"><p>چنین آفرینش تو دانی نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپاس از تو دارم که ما بی گزند</p></div>
<div class="m2"><p>ز دریا گذشتیم و کوه بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراوان به درویش بخشید چیز</p></div>
<div class="m2"><p>به ملّاح فرتوت و یارانش نیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه خوردنیها کز او بازماند</p></div>
<div class="m2"><p>بدو داد و برگشت و کشتی براند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به طیهور نامه فرستاد شاه</p></div>
<div class="m2"><p>که بگذاشتیم ایمن این ژرف راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دریا به خشکی رسیدیم شاد</p></div>
<div class="m2"><p>ز ملّاح خشنود و بی غم ز باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برآمد مرا این یکی آرزوی</p></div>
<div class="m2"><p>به فرّ جهاندار آزاده خوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امیدم چنان کآرزویی دگر</p></div>
<div class="m2"><p>برآید به فرّ شه تاجور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو آن مردمان را گسی کرد شاه</p></div>
<div class="m2"><p>به دریا کنار اندر آمد ز راه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برآورد خیمه در آن مرغزار</p></div>
<div class="m2"><p>یکی دشت مانند خرّم بهار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه سبزه و جوی و آب روان</p></div>
<div class="m2"><p>یکی جایگاه از در خسروان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهاران و نخچیر بسیار بود</p></div>
<div class="m2"><p>در و دشت مانند گلزار بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن شهرها پیش شاه آمدند</p></div>
<div class="m2"><p>ستایشگر و نیکخواه آمدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر آن کس که شد پیش بنواختش</p></div>
<div class="m2"><p>ز نوئین همه میهمان ساختش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی کرد هر کس خرید و فروخت</p></div>
<div class="m2"><p>ز شادی رخ مردمان برفروخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گیا دید و جایی خنک دید شاه</p></div>
<div class="m2"><p>درنگی شد آن جایگه چارماه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فرستاد بر کوه پنجاه مرد</p></div>
<div class="m2"><p>دلیران ایران، سران نبرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نهان تا برون ناید آوازشان</p></div>
<div class="m2"><p>ندارد کس آگاهی از رازشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سه تن را فرستاد از آن روی کوه</p></div>
<div class="m2"><p>یلان و دلیران دانش پژوه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدان تا به دانش بدانند راه</p></div>
<div class="m2"><p>که چون است راه سرافراز شاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شتابان دویدند بر کوه قاف</p></div>
<div class="m2"><p>چو بر شاخ گل بچّه ی زندواف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان کوه با رنج بگذاشتند</p></div>
<div class="m2"><p>ز هامون یکی راه برداشتند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شب و روز پویان و ترسان ز راه</p></div>
<div class="m2"><p>ز که و ز هامون و هر جایگاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به ماهی شدند آن دلیران برون</p></div>
<div class="m2"><p>به بلغار کآن را تو خوانی برون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که پیوسته با مرز سقلاب بود</p></div>
<div class="m2"><p>در و دشت او سبزه و آب بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در آن مرز یک ماه رفتند باز</p></div>
<div class="m2"><p>بدیدند شهر و نشیب و فراز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دریا رسیدند مردان شاه</p></div>
<div class="m2"><p>ندیدند از آن پیشتر نیز راه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر آن مرز مردم چو آرام کرد</p></div>
<div class="m2"><p>دمندان مرآن آب را نام کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سوی آتبین بازگشتند زود</p></div>
<div class="m2"><p>نشانی بدادند از آن سو که بود</p></div></div>