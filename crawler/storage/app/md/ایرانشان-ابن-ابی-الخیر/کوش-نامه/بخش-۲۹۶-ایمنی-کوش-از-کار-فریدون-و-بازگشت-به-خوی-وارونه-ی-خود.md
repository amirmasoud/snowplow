---
title: >-
    بخش ۲۹۶ - ایمنی کوش از کار فریدون، و بازگشت به خوی وارونه ی خود
---
# بخش ۲۹۶ - ایمنی کوش از کار فریدون، و بازگشت به خوی وارونه ی خود

<div class="b" id="bn1"><div class="m1"><p>پراندیشه بود و همی سال چند</p></div>
<div class="m2"><p>بدان کز فریدونش آید گزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بگذشت بر وی بسی سالیان</p></div>
<div class="m2"><p>سپاهی نیامد از ایرانیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد ایمن ز کار فریدون و رزم</p></div>
<div class="m2"><p>به بگماز و آرام پرداخت و بزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگان که بودند از لشکرش</p></div>
<div class="m2"><p>ز هر جای گرد آمده بر درش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفرمود تا بازگشتند نیز</p></div>
<div class="m2"><p>درم داد و اسبان و هر گونه چیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود و سرکشانش به گوی و شکار</p></div>
<div class="m2"><p>همی راند شادان چنان روزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نزد فریدون بسیار دان</p></div>
<div class="m2"><p>سواری فرستاد وی کاردان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی نامه با پوزش و کهتری</p></div>
<div class="m2"><p>فرستاد بی جنگ و بی داوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرستاده را گفت بر نیک و بد</p></div>
<div class="m2"><p>نهانی گر آگاه گردی سزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببین تا چه سر دارد آن شاه زوش</p></div>
<div class="m2"><p>به در، مرد چند است پولادپوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بشد مرد چون باد و آمد چو دود</p></div>
<div class="m2"><p>بگفت آنچه پرسید و پاسخ شنود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت از امروز تا سالیان</p></div>
<div class="m2"><p>تو را از فریدون نیاید زیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندارد سرِ کین و پرخاش و رزم</p></div>
<div class="m2"><p>نشسته ست با نامداران به بزم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو گویی که ماه است تاج از برش</p></div>
<div class="m2"><p>ستاده ست رویین به گرد اندرش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بازار و لشکر بپرسیدم این</p></div>
<div class="m2"><p>فریدون ندارد سر رزم و کین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل کوش از این آگهی گشت شاد</p></div>
<div class="m2"><p>فرستاده را چیز بسیار داد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمود تا پس دبیران شاه</p></div>
<div class="m2"><p>به زندان بکشتندشان بیگناه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از ایشان به شادی و خوردن نشست</p></div>
<div class="m2"><p>سر از گنج وز ایمنی گشته مست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان گشت گردنکش و تیره خوی</p></div>
<div class="m2"><p>که جز خون و کشتن نکرد آرزوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه بستدی هرچه بودیش رای</p></div>
<div class="m2"><p>زن و کودک خوب و هم بادپای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان خوی وارون خود باز شد</p></div>
<div class="m2"><p>بدان کار و کردار خود باز شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه بخشایش آورد بر کس نه مهر</p></div>
<div class="m2"><p>دگرگونه تر شد به آیین و چهر</p></div></div>