---
title: >-
    بخش ۷۸ - آهنگ جنگ
---
# بخش ۷۸ - آهنگ جنگ

<div class="b" id="bn1"><div class="m1"><p>بدو آتبین گفت دادی تو داد</p></div>
<div class="m2"><p>از این بیش گفتار دیگر مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپه باز گردان تو ایدر بپای</p></div>
<div class="m2"><p>که با تو بکوشم به زور خدای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمود تا بازپس شد سپاه</p></div>
<div class="m2"><p>سوی لشکر خویشتن رفت شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپوشید خفتان و ساز نبرد</p></div>
<div class="m2"><p>دل لشکر از رنج او شد بدرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپاه از عنانش برآویختند</p></div>
<div class="m2"><p>خروش از دل و جان برانگیختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که شاها، مخور با سپه زینهار</p></div>
<div class="m2"><p>بمان تا کند دیگری کارزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که پرگست، اگر بر تو آید گزند</p></div>
<div class="m2"><p>بمانیم بیچاره و مستمند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برآرد ز خرد و بزرگ او دمار</p></div>
<div class="m2"><p>نیابیم از او کس به جان زینهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پاسخ چنین گفت شیر دلیر</p></div>
<div class="m2"><p>که از گرگ، هرگز نترسید شیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شما در مدارید از این کار ننگ</p></div>
<div class="m2"><p>که من دیده ام رزم مردان جنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکوشم بدان سان که دارم توان</p></div>
<div class="m2"><p>دل شیر دارم تن خسروان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به نیروی یزدان بر آرم دمار</p></div>
<div class="m2"><p>از این دیو چهره در این کارزار</p></div></div>