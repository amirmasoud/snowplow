---
title: >-
    بخش ۳۴۴ - پیروزی سیاهان و گریختن کوش
---
# بخش ۳۴۴ - پیروزی سیاهان و گریختن کوش

<div class="b" id="bn1"><div class="m1"><p>چو آگاهی آمد به کوش سترگ</p></div>
<div class="m2"><p>که آمد سپاهی بدان سان بزرگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیامد به دلش اندر اندیشه هیچ</p></div>
<div class="m2"><p>بفرمود تا کرد لشکر بسیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمانی چنان برد کآن سروران</p></div>
<div class="m2"><p>زبونند مانند آن دیگران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذر کرد بر آب ششصد هزار</p></div>
<div class="m2"><p>بیاورد رزم آزموده سوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن بیرکان لشکر آگه نبود</p></div>
<div class="m2"><p>کران در زمین همچو دودی نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنزدیکی شاه مازندران</p></div>
<div class="m2"><p>سراپرده زد کوش با سروران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سنجه چنان دید شد کار خام</p></div>
<div class="m2"><p>سپاهی فرستاد نزدیک سام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان تا بدیشان بگیرند راه</p></div>
<div class="m2"><p>وز آن دشت برداشت یکسر سپاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو حلقه به گردش در آمد به شب</p></div>
<div class="m2"><p>نه آواز کوس و نه شور و جلب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه راه بگرفت بر لشکرش</p></div>
<div class="m2"><p>گزند آمد از آسمان بر سرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو گردون ز رنگ سیه پاک شد</p></div>
<div class="m2"><p>جهان را سیه پیرهن چاک شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برآویختند و برانگیختند</p></div>
<div class="m2"><p>ز خون خاک با گِل برآمیختند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چپ و راست، پیش و پس سروران</p></div>
<div class="m2"><p>گرفته سیاهان مازندران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی گفت کوش ای دلیران من</p></div>
<div class="m2"><p>ستوده سواران و شیران من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه هنگام سستی ست، کوشش کنید</p></div>
<div class="m2"><p>بر این دشمنان خاک پوشش کنید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلیران به کف برنهادند جان</p></div>
<div class="m2"><p>بببستند یکسر عنان در عنان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سیاهان مازندران با فرسب</p></div>
<div class="m2"><p>به یک چوب گردان فگندند از اسب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بس های و هوی وز بس چاک چاک</p></div>
<div class="m2"><p>همی گشت مریخ را زهره چاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برآویخت با کوش، دیو سفید</p></div>
<div class="m2"><p>پسِ پشت پولاد و غندی و بید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرفته به دو دست چوب فرسب</p></div>
<div class="m2"><p>پیاده همی کوفت بر مرد و اسب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کس از چنگ آن تیز چنگ اژدها</p></div>
<div class="m2"><p>نیامد به جان، ای شگفت، رها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رجاموس کرده ست گفتی مگر</p></div>
<div class="m2"><p>که آهن نیامد بدو کارگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بکوشید با او سپهدار کوش</p></div>
<div class="m2"><p>نه با اسب پا و نه با کوش توش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپاهش همه روی برگاشتند</p></div>
<div class="m2"><p>سپهدار را خوار بگذاشتند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بریده ز سالار لشکر امید</p></div>
<div class="m2"><p>که جان کی رهاند ز دیو سپید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو هنگام شب گشت، برگشت کوش</p></div>
<div class="m2"><p>همی تا نماندش در اندام توش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برون رفت با لشکری زآن میان</p></div>
<div class="m2"><p>مر آن دیگران را سرآمد زمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به مصر آمد و لشکر آن جا بماند</p></div>
<div class="m2"><p>تنی چند با خویشتن برنشاند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بیم سیاهان نیارست بود</p></div>
<div class="m2"><p>بسی رنج برخویشتن برفزود</p></div></div>