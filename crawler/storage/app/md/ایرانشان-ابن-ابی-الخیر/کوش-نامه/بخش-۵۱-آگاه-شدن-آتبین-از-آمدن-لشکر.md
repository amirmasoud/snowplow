---
title: >-
    بخش ۵۱ - آگاه شدن آتبین از آمدن لشکر
---
# بخش ۵۱ - آگاه شدن آتبین از آمدن لشکر

<div class="b" id="bn1"><div class="m1"><p>وز آن سوی رود از پس کارزار</p></div>
<div class="m2"><p>فرستاده بود آتبین ده سوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشانده همه بر درخت بلند</p></div>
<div class="m2"><p>که ناگه ز دشمن نیاید گزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکایک چو دیدند ناگه سپاه</p></div>
<div class="m2"><p>سوی آتبین بر گرفتند راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که روی هوا سرخ و زرد و بنفش</p></div>
<div class="m2"><p>ز تابیدن رنگهای درفش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوا سربسر بود نالد همی</p></div>
<div class="m2"><p>زمین از گرانی بنالد همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همانا که خود شاه چین آمده ست</p></div>
<div class="m2"><p>کجا چین و ماچین به کین آمده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیاراست پس آتبین با گروه</p></div>
<div class="m2"><p>فرستاد یکسر بنه سوی کوه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب رود بگرفت و راه سپاه</p></div>
<div class="m2"><p>کشیدند دیده همه سوی راه</p></div></div>