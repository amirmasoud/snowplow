---
title: >-
    بخش ۱۳۲ - بازگشت آتبین به بسیلا به نزد طیهور شاه
---
# بخش ۱۳۲ - بازگشت آتبین به بسیلا به نزد طیهور شاه

<div class="b" id="bn1"><div class="m1"><p>وز آن روی چون آتبین بازگشت</p></div>
<div class="m2"><p>بسیلا ز شادی پر آواز گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی کرد و بازار برخاستند</p></div>
<div class="m2"><p>بسیلا به دیبا بیاراستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی تا به نزدیک دربند تفت</p></div>
<div class="m2"><p>جهاندیده طیهور و لشکر برفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بر درگرفت آتبین را به مهر</p></div>
<div class="m2"><p>بدو گفت کای شاه فرخنده چهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من به دیدار تو گشت شاد</p></div>
<div class="m2"><p>که از دشمن خود کشیدی تو داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو آتبین گفت کای شهریار</p></div>
<div class="m2"><p>همی خواستم تا به دریا کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر ماهیانی درنگ آورم</p></div>
<div class="m2"><p>چو ایدر رسد کوش جنگ آورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر باره اندیشه کردم که شاه</p></div>
<div class="m2"><p>شود تنگدل گر شود کس تباه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت طیهور کای نامجوی</p></div>
<div class="m2"><p>به گِرد فزونی و بیشی مپوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز یزدان تو را دستگاه این بس</p></div>
<div class="m2"><p>که از کاخ کس مویه نشنید کس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شادی سوی شهر باز آمدی</p></div>
<div class="m2"><p>به کاخ گل افشان فراز آمدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گهر ریخت از مایه ها بر سرش</p></div>
<div class="m2"><p>درم داد درویش را لشکرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز یزدان همی داشت، هر کس سپاس</p></div>
<div class="m2"><p>که باز آمد آن شاه یزدان شناس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان بر دل شاه برگشت دوست</p></div>
<div class="m2"><p>که بیگانه پنداشت گر پوست اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه روز با وی سخن گفت شاه</p></div>
<div class="m2"><p>وگر دیرتر شد برآشفت شاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به طیهوریان بر همه خواسته</p></div>
<div class="m2"><p>پراگند و شد کارش آراسته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان مهربان شد بر او مرد و زن</p></div>
<div class="m2"><p>که بودی به بام و درش انجمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرع را کجا ترجمان بود نیز</p></div>
<div class="m2"><p>فراوان فرستاد، هرگونه چیز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از اسبان پر مایه و ساز و زین</p></div>
<div class="m2"><p>ز دینار وز تخت دیبای چین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب و روز با او نشستی بهم</p></div>
<div class="m2"><p>بهم بودشان شادمانی و غم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز خوبی چنان بود شاه آتبین</p></div>
<div class="m2"><p>هم از چابکی روز میدان کین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که هرگه که بیرون شدی از سرای</p></div>
<div class="m2"><p>نبودی از انبوه نظّاره جای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به دیدار او آمدی مرد و زن</p></div>
<div class="m2"><p>همی برفتادی بهم تن به تن</p></div></div>