---
title: >-
    بخش ۲۳۱ - داد خواستن مردمان از دست کوش، شاه چین
---
# بخش ۲۳۱ - داد خواستن مردمان از دست کوش، شاه چین

<div class="b" id="bn1"><div class="m1"><p>چنان بود یک روز کز مرز چین</p></div>
<div class="m2"><p>ز فرزانه پنجاه مرد گزین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خروشان به درگاه شاه آمدند</p></div>
<div class="m2"><p>ستمدیدگان دادخواه آمدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که زنهار، شاها، به فریادرس</p></div>
<div class="m2"><p>که جز تو نداریم فریادرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان پاک کردی ز ضحاکیان</p></div>
<div class="m2"><p>به نیروی یزدان و فرّ کیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز داد تو آباد شد هند و روم</p></div>
<div class="m2"><p>نمانده ست ویران یک انگشت بوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین را تو آباد کردی به گرز</p></div>
<div class="m2"><p>تو کردی به هر جای تابنده برز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تیغ تو پنهان ستم در جهان</p></div>
<div class="m2"><p>ز داد تو بدخواه یکسر نهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان اندر آسانی و ما به رنج</p></div>
<div class="m2"><p>نه فرزند ماند و نه کاخ و نه گنج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرازی نشسته ست بر تخت چین</p></div>
<div class="m2"><p>به رنج اندر از دست او آن زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درم دارِ آن مرز درویش گشت</p></div>
<div class="m2"><p>ستمکار و بدخو و بدکیش گشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخواند همی خویشتن جز خدای</p></div>
<div class="m2"><p>تو مپسند از وی شه پاکرای</p></div></div>