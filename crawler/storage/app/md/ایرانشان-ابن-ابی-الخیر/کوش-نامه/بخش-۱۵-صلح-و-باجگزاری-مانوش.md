---
title: >-
    بخش ۱۵ - صلح و باجگزاری مانوش
---
# بخش ۱۵ - صلح و باجگزاری مانوش

<div class="b" id="bn1"><div class="m1"><p>به یک هفته آمد به روم آن نوند</p></div>
<div class="m2"><p>چو از نامه بگشاد مانوش بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخواند و فگند آستین بر زمین</p></div>
<div class="m2"><p>نهاد از بر آستین بر جبین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی گفت کای کردگار سپهر</p></div>
<div class="m2"><p>تو افگندی اندر دل شاه مهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو کردی مر او را بدین مایه رام</p></div>
<div class="m2"><p>وگرنه براندی بدین مرزکام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز آن پس چو دستورش اندر رسید</p></div>
<div class="m2"><p>همه باز گفت آنچه گفت و شنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دستور فرمود تا کرد ساز</p></div>
<div class="m2"><p>به یک ماه گِرد آمد آن ساو و باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی چیز دیگر بر آن برفزود</p></div>
<div class="m2"><p>غلامان و اسبان چنان کِم شنود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گزین کرد سی تن ز گردان شهر</p></div>
<div class="m2"><p>کسی را که بود از بزرگیش بهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرستادشان تا بر آرد به راه</p></div>
<div class="m2"><p>که باشید یکسر نوا نزد شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سرکش بر شاه ایران رسید</p></div>
<div class="m2"><p>به رومی بر او آفرین گسترید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر شاه بگذاشت آن خواسته</p></div>
<div class="m2"><p>غلامان و اسبان آراسته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن شاد شد شاه پیروز بخت</p></div>
<div class="m2"><p>همان گه ز طرطوس بربست رخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به نیرنگ چون بستد آن ساو و باز</p></div>
<div class="m2"><p>به بغداد شد شاه نیرنگ ساز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشست از بر تخت با کام، کی</p></div>
<div class="m2"><p>بینداخت رنج و بر افراخت می</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شاهان شد اندیشه از کار او</p></div>
<div class="m2"><p>ولیکن نه آگه ز کردار او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل هر کس از شاه ترسنده گشت</p></div>
<div class="m2"><p>به تن هر کسی شاه را بنده گشت</p></div></div>