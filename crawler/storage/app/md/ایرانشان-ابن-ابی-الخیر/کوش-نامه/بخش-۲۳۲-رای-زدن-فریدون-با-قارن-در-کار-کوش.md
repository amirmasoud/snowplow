---
title: >-
    بخش ۲۳۲ - رای زدن فریدون با قارن در کار کوش
---
# بخش ۲۳۲ - رای زدن فریدون با قارن در کار کوش

<div class="b" id="bn1"><div class="m1"><p>ز گفتار ایشان دژم گشت شاه</p></div>
<div class="m2"><p>همی از تن خویش دید آن گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی گفت کای برتر از ماه و مهر</p></div>
<div class="m2"><p>به دل در تو آراستی کین و مهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گیتی تو دادیش چندان زمان</p></div>
<div class="m2"><p>که در خویشتن گردد او بد گمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود ناسپاس از تو یکبارگی</p></div>
<div class="m2"><p>نه یاد آیدش مرگ و بیچارگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفرمود تا قارن رزم زن</p></div>
<div class="m2"><p>بیامد شتابان بدان انجمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپرسید و بنشاند و بنواختش</p></div>
<div class="m2"><p>به چرخ برین سر برافراختش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت کای نیک یار من</p></div>
<div class="m2"><p>دلیر و سوار و سپهدار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی تیز گردان بدان رای و هوش</p></div>
<div class="m2"><p>که جانم بپردازی از رنج کوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان پُر ز داد است بر کامگر</p></div>
<div class="m2"><p>مگر چین و مکران و آن بوم و بر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه روی گیتی به خواب اندراند</p></div>
<div class="m2"><p>مگر چین کزآن سگ به تاب اندراند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز رنج کسان و ز روزِ دراز</p></div>
<div class="m2"><p>چنان گشت گردنکش و بی نیاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که گوید همی در جهان سربسر</p></div>
<div class="m2"><p>چو من کیست روزی ده جانور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر آن را بمانم بجای نشست</p></div>
<div class="m2"><p>بدین بیهده باز دارمش مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپرسد مرا داور کردگار</p></div>
<div class="m2"><p>ز بیداد آن دیو، روز شمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرستادم او را دوباره سپاه</p></div>
<div class="m2"><p>گریزان بیامد بدین بارگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر چند رنجور بینم تنت</p></div>
<div class="m2"><p>بفرسود یال و بر از جوشنت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبینم کسی را از ایرانیان</p></div>
<div class="m2"><p>که امروز بندد بدین کین میان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبردی که با کوش روز نبرد</p></div>
<div class="m2"><p>درآورد گوید که از راه برد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپاه من و گنج من پیش توست</p></div>
<div class="m2"><p>همان گنجم اندر کم و بیش توست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز دینار و اسب و قبا و کلاه</p></div>
<div class="m2"><p>ببخش و ببر چند خواهی به راه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگر رنج او کم کنی زین میان</p></div>
<div class="m2"><p>که او ماند از تخم ضحّاکیان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان آنگهی گردد از رنج پاک</p></div>
<div class="m2"><p>که آن بدگهر نیز گردد هلاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر او را فرستی تو ایدر به بند</p></div>
<div class="m2"><p>کلاهت برآید به چرخ بلند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ببینم یکی روی آن دیو زشت</p></div>
<div class="m2"><p>که چندان دلیران ایران بکشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو پیروز گردی تو بر مرز چین</p></div>
<div class="m2"><p>به نستوهِ شیر و سپار آن زمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدو ده تو آن کشور و تاج و تخت</p></div>
<div class="m2"><p>تو برگرد شادان و پیروز بخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو گفت قارن که فرمان کنم</p></div>
<div class="m2"><p>بدین آرزو جان گروگان کنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فزون زآن کنم کز جهاندار شاه</p></div>
<div class="m2"><p>شنیدم در آن نامور پیشگاه</p></div></div>