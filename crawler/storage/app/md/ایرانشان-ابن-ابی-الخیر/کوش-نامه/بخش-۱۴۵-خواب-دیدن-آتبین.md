---
title: >-
    بخش ۱۴۵ - خواب دیدن آتبین
---
# بخش ۱۴۵ - خواب دیدن آتبین

<div class="b" id="bn1"><div class="m1"><p>شبی شادمان خفته بُد آتبین</p></div>
<div class="m2"><p>چنان دید در خواب شاه زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که فرزند پیش آمدش چون سروش</p></div>
<div class="m2"><p>سُوار آن که شد کشته بر دست کوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی چوب در دست او داد خشک</p></div>
<div class="m2"><p>همان گاه شد سبز و بویا چو مشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکِشت از بر کوه شاه آتبین</p></div>
<div class="m2"><p>فرو برد بیخش به زیر زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم اندر زمان شد درختی بلند</p></div>
<div class="m2"><p>بگسترد از او شاخ و سایه فگند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببالید تا سر به گردون کشید</p></div>
<div class="m2"><p>ز گردون همانا که افزون کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان سر بسر زیر سایه گرفت</p></div>
<div class="m2"><p>ز سبزی و از شاخ مایه گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وزآن پس یکی باد نوشین دمید</p></div>
<div class="m2"><p>از آن شاخها برگ بیرون کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پراگند گرد جهان چون چراغ</p></div>
<div class="m2"><p>شده روشن از وی همه کوه و راغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان گشت از آن روشنی شادمان</p></div>
<div class="m2"><p>همی گفت هرکس سرآمد غمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو از خواب بیدار شد بامداد</p></div>
<div class="m2"><p>بفرمود تا شد برش کامداد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برآورد راز نهان از نهفت</p></div>
<div class="m2"><p>همه خواب یکسر بدو بازگفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت شاها، تو رامش فزای</p></div>
<div class="m2"><p>که یک ره ببخشود ما را خدای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو را داد در خواب دوشین نشان</p></div>
<div class="m2"><p>که گیتی نماند ز جادو فشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر و تاج ضحاک ناهوشیار</p></div>
<div class="m2"><p>به خاک اندر آرد همی روزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان بازگردد به فرزند تو</p></div>
<div class="m2"><p>شود شاد از او خویش و پیوند تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سُوار بهشتی که آن چوب خشک</p></div>
<div class="m2"><p>تو را داد بویاتر از عود و مشک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز پشت تو او را برادر بود</p></div>
<div class="m2"><p>که با تخت شاهی و افسر بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو در دست تو سبز شد چوب سخت</p></div>
<div class="m2"><p>جوان گشت خواهد ز فرّ تو بخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بلندی بود چون نشاندی به کوه</p></div>
<div class="m2"><p>همان پایه داری و فرّ و شکوه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو سر برکشید و ببالید شاخ</p></div>
<div class="m2"><p>بگیرد به تیغ این جهان فراخ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو گسترد سایه به گرد جهان</p></div>
<div class="m2"><p>به فرمان شوندت کهان و مهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرآن برگها را که همچون چراغ</p></div>
<div class="m2"><p>پراگند بر کشور و کوه و راغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به فرّ تو باشند شاهانِ داد</p></div>
<div class="m2"><p>همه با دل شاد و با دست راد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زمین هفت کشور به چنگ آورند</p></div>
<div class="m2"><p>فراوان به گیتی درنگ آورند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گزارش چو بشنید از او آتبین</p></div>
<div class="m2"><p>بسی خواند بر دانشش آفرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدو گفت کاین خواب من راز دار</p></div>
<div class="m2"><p>ز بیگانه مردم سخن باز دار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فراوانش بخشید هرگونه چیز</p></div>
<div class="m2"><p>ز اسب و ستام و ز دینار نیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر این راز بر سالیان برگذشت</p></div>
<div class="m2"><p>بسی نیک و بد نیز بر سر گذشت</p></div></div>