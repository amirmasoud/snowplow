---
title: >-
    بخش ۱۸۸ - در عبرت گرفتن از کارِ جهان و گریز به مدح ممدوح
---
# بخش ۱۸۸ - در عبرت گرفتن از کارِ جهان و گریز به مدح ممدوح

<div class="b" id="bn1"><div class="m1"><p>چنین است کار جهان کرد کرد</p></div>
<div class="m2"><p>گهی تندرستی بود گاه درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی شادمانی و گاهی غمان</p></div>
<div class="m2"><p>میان غم و شادیش یک زمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم و شادمانیش چون درگذشت</p></div>
<div class="m2"><p>چو بادی بود کاو سبک برگذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو در کارها ژرف بربنگری</p></div>
<div class="m2"><p>دراز است با تو مرا داوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی باغبان است و چندین درخت</p></div>
<div class="m2"><p>چرا گشت سست این و آن گشت سخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی چون بکِشت، از بنه خود به دست</p></div>
<div class="m2"><p>یکی رُست و تا سالیان گشت مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا میوه آرد یکی همچو مشک</p></div>
<div class="m2"><p>برابر یکی شاخه ها گشته خشک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>براین همگنان را همی خاک و آب</p></div>
<div class="m2"><p>یکی بینم و باد و نیز آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درختی که یابد به کام این چهار</p></div>
<div class="m2"><p>تهی پس چرا ماند از بیخ و بار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیری یکی هست مانند تیر</p></div>
<div class="m2"><p>چو آید یکی کوژ مانند پیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از این باغبان هرچه خسرو بخواست</p></div>
<div class="m2"><p>بدید آنچه پیش آمدش کژّ و راست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم از راستان ساخت پر مایه تخت</p></div>
<div class="m2"><p>به دیبا بیاراست و شد نیکبخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز کژّان بلند آتشی برفروخت</p></div>
<div class="m2"><p>یکایک همه پیش تختش بسوخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همانا چنین آمد او رهنمای</p></div>
<div class="m2"><p>کزآن سان همی کرد خواهد خدای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگر تا نتابی سر از راستی</p></div>
<div class="m2"><p>اگر هیچ ناسوختن خواستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که آتش نسوزد تن راستان</p></div>
<div class="m2"><p>چنین داستان آمد از باستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز تن راستی خواه و نیز از روان</p></div>
<div class="m2"><p>بهانه مکن گشتِ چرخ روان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبینی که دارای روشنروان</p></div>
<div class="m2"><p>همی باز جوید ز مردم نهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>محمّد شهنشاه یزدان پرست</p></div>
<div class="m2"><p>همی راست خواهد از دین پرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی آیت فاستقم خواند او</p></div>
<div class="m2"><p>ز دینی کجا متهم داند او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو باد سمندش بدو بگذرد</p></div>
<div class="m2"><p>همی آنچه گردون بدو ننگرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در این راه آیین بجوید همی</p></div>
<div class="m2"><p>که گیتی ز بددین بشوید همی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من ایدر رسانیده بودم سخن</p></div>
<div class="m2"><p>که بشکفت شاخ درخت کهن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بیاراست دستور بر پیشگاه</p></div>
<div class="m2"><p>بر او مهربان گشت فرخنده شاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو داد دیوان و جای پدر</p></div>
<div class="m2"><p>چه زیباست جای پدر بر پسر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ندید و نبیند دگر چرخ پیر</p></div>
<div class="m2"><p>شهی چون محمّد، چو احمد وزیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ملکشاه و خواجه مگر زنده شد</p></div>
<div class="m2"><p>لب فلسفی زین پر از خنده شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از او داد شد دُر چو دریای رنگ</p></div>
<div class="m2"><p>وزاین رای زاید چو نافه ز رنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به خشم افگند سنگ مانند آب</p></div>
<div class="m2"><p>به حکم آن بپوشد رخ آفتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به تیغ او برآرد ز دریا دمار</p></div>
<div class="m2"><p>به کلک این کند قاع باغ بهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سر تخت از آن و رخ بخت از این</p></div>
<div class="m2"><p>فروزنده بادا همیشه چنین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از او جان بدخواه، رنجور باد</p></div>
<div class="m2"><p>وز این چشم بد، سال و مه دور باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ایا شهریاری که هنگام کین</p></div>
<div class="m2"><p>ز سمّ سمندت بلرزد زمین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به شمشیر خشم و به رای ردان</p></div>
<div class="m2"><p>جهان بستدی تو ز دست بدان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به شمشیر بخشش تویی سرفراز</p></div>
<div class="m2"><p>مرا نیز بستان ز دست نیاز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز دستور پیشین به من بد رسید</p></div>
<div class="m2"><p>چو بد کرد، دیدم که چون بد کشید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نداد آنچه فرمودی ای شهریار</p></div>
<div class="m2"><p>به من بنده، دستور ناسازگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که هر کاو کند نام مردی بلند</p></div>
<div class="m2"><p>نیاید ز بدگوهران جز گزند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بخشش کرا نیست یک پاره جو</p></div>
<div class="m2"><p>نیابد از او هیچ کس آرزو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنون کار دیوان بدان بازگشت</p></div>
<div class="m2"><p>که گیتی زنامش پرآواز گشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو جای پدر داری و رای او</p></div>
<div class="m2"><p>به فرزند خواجه سزد جای او</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از این به همانا ندیدی تو رای</p></div>
<div class="m2"><p>که دادی بدو این گرانمایه جای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که هم پاکدین است و هم مهربان</p></div>
<div class="m2"><p>دلش با گمان راست و با دل زبان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بماناد در پیش تخت بلند</p></div>
<div class="m2"><p>به تو شاد و تو شاد و دور از گزند</p></div></div>