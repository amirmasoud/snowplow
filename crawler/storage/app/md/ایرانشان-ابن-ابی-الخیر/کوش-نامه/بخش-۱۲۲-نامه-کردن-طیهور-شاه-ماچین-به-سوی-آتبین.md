---
title: >-
    بخش ۱۲۲ - نامه کردن طیهور شاه ماچین به سوی آتبین
---
# بخش ۱۲۲ - نامه کردن طیهور شاه ماچین به سوی آتبین

<div class="b" id="bn1"><div class="m1"><p>ز ماچین سر ماه نامه رسید</p></div>
<div class="m2"><p>سوی آتبین، کاین شگفتی که دید!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خمدان چرا کرد، باید درنگ</p></div>
<div class="m2"><p>چو دانی که نتوان گشادن به جنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن شهر یکباره دیده بخواب</p></div>
<div class="m2"><p>نگه کن سوی شهر دیگر شتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که چین با سپاه است و با ساز و گنج</p></div>
<div class="m2"><p>به خمدان چرا برد بایدت رنج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر از گنج کوش است شهر و حصار</p></div>
<div class="m2"><p>که آسان توان یافت بی کارزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان دان که بدخواه تو گشت سست</p></div>
<div class="m2"><p>چو در دستِ تو گنج او شد درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بر دوغ باشد تو را دسترس</p></div>
<div class="m2"><p>فزونتر بود هر زمانی مگس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند سیم، خمّیده را پشت راست</p></div>
<div class="m2"><p>خمیده شود هر که را سیم کاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سباهی روان را به رنج آورد</p></div>
<div class="m2"><p>همی روی از ایران به کنج آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آن نامه برخواند خسرو به راز</p></div>
<div class="m2"><p>پشیمان شد از نستدن ساو و باز</p></div></div>