---
title: >-
    بخش ۱۹۶ - فرستاده ی کارم در پیشگاه فریدون
---
# بخش ۱۹۶ - فرستاده ی کارم در پیشگاه فریدون

<div class="b" id="bn1"><div class="m1"><p>فرستاده برداشت آن خواسته</p></div>
<div class="m2"><p>یکی کاروانی شد آراسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی رفت تا شهر آمل رسید</p></div>
<div class="m2"><p>فرود آمد و خیمه ها برکشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاده ی شاه شد پیش شاه</p></div>
<div class="m2"><p>سخن راند از کارم نیکخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که چون من رسیدم شتابان ز کوه</p></div>
<div class="m2"><p>شده بود طیهور دور از گروه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرافراز کارم نشسته بجای</p></div>
<div class="m2"><p>جوانی خردمند و پاکیزه رای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو نام شهنشاه با نامه دید</p></div>
<div class="m2"><p>ببوسید و شاد آفرین گسترید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپاهش همه گوهر افشاندند</p></div>
<div class="m2"><p>چو نام شهنشاه برخواندند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو هفته فزونتر می تیزجوش</p></div>
<div class="m2"><p>به یاد شهنشاه کردند نوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیاورد پس هرچه بودش ز گنج</p></div>
<div class="m2"><p>فرستاد و بر دل نیامدش رنج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سواری هزار است با خواسته</p></div>
<div class="m2"><p>یکی گنج دارند آراسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همانا که از شهریاران پیش</p></div>
<div class="m2"><p>ندیدند چندین فزونی به خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن، شادمان شد جهاندار شاه</p></div>
<div class="m2"><p>فرستاد پیشش سپه را به راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستاده ی کارم آمد به در</p></div>
<div class="m2"><p>ببوسید تخت شه تاجور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فریدون چو آن خواسته دید و تخت</p></div>
<div class="m2"><p>ز کارم دلش شادمان گشت سخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس آن تخت فیروزه شاهوار</p></div>
<div class="m2"><p>نهادند در خانه ی زرنگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بر تخت فیروزه بنشست شاه</p></div>
<div class="m2"><p>چنین گفت خندان ز پیش سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که پیروز گردم به پیروز بخت</p></div>
<div class="m2"><p>به هر هفت کشور ز پیروزه تخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بپرسید کاین تخت فیروزه گون</p></div>
<div class="m2"><p>ز کارم جا اوفتاده ست و چون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پاسخ چنین کرد گوینده یاد</p></div>
<div class="m2"><p>که این، پیل دندان به طیهور داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدان گه که دادش چو مردان فریب</p></div>
<div class="m2"><p>وزآن پس رسانید چندان نهیب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین فال زد گفت بر تخت بزم</p></div>
<div class="m2"><p>بر او نیز پیروز گردم به رزم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس آن چیز در گنج بنهاد شاه</p></div>
<div class="m2"><p>فرستاده را داشت مهمان دو ماه</p></div></div>