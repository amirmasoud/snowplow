---
title: >-
    بخش ۳۲۱ - کارزار سلم با کوش و حیلت کردن قارن و هزیمت شدن کوش
---
# بخش ۳۲۱ - کارزار سلم با کوش و حیلت کردن قارن و هزیمت شدن کوش

<div class="b" id="bn1"><div class="m1"><p>چو شد ماه نو، مرد جنگ آزمای</p></div>
<div class="m2"><p>تبیره همی آرزو کرد و نای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گفت با سلم قارن به شب</p></div>
<div class="m2"><p>کز این راز با کس تو مگشای لب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپه پیش بر، رزم را ساز کن</p></div>
<div class="m2"><p>به رزم اندرون سستی آغاز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی کنده فرمای پیش سپاه</p></div>
<div class="m2"><p>خبر کن که گشته ست قارن تباه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن زخم پولاد سستی نمود</p></div>
<div class="m2"><p>بمُرد و سپاه را به انده سپرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر گردد ایمن ز من دیوزاد</p></div>
<div class="m2"><p>نیارد مرا بیش در جنگ یاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که آمد یکی رای ما را فراز</p></div>
<div class="m2"><p>امید است کآن بدرگ دیور ساز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتار گردد به کردار خویش</p></div>
<div class="m2"><p>تبه بیند این تیز بازار خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به فرّ فریدون همه کام تو</p></div>
<div class="m2"><p>برآید، به پروین شود نام تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو پیوسته جنگ آوری چند روز</p></div>
<div class="m2"><p>به من چشم دار، ای شه نیوسوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدانگه که گردد بلند آفتاب</p></div>
<div class="m2"><p>شود همچو زر آب دریای آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفت این و با لشکری بیکران</p></div>
<div class="m2"><p>به دریای آب اندرون شد نهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی سلم یک هفته رنج آزمود</p></div>
<div class="m2"><p>به رزم اندرون نیز سستی نمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وزآن پس یکی کنده کندن گرفت</p></div>
<div class="m2"><p>همی خاک را برفگندن گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گمانی بد اندر دل کوش شد</p></div>
<div class="m2"><p>که قارن همانا کفن پوش شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن دل گرفتند مردان او</p></div>
<div class="m2"><p>همان جنگجویان و گردان او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکُشتند چندان از ایران سپاه</p></div>
<div class="m2"><p>که بر لشکری تنگ شد جایگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هشتم کی همروز ای شگفت</p></div>
<div class="m2"><p>جهان با نوید زر آیین گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز دریا برآمد به کشتی نهنگ</p></div>
<div class="m2"><p>ز کین همچو الماس دندان و چنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پسِ پُشتِ کوش اندر آمد سپاه</p></div>
<div class="m2"><p>جهان پهلوان قارن رزمخواه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی گرزه ی گاو پیکر به دست</p></div>
<div class="m2"><p>نوندش همی کوه را کرد پَست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه چندان نمود آن سپه را نهیب</p></div>
<div class="m2"><p>ندانست مردم عنان از رکیب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز تیغ دلیران روان گشت خون</p></div>
<div class="m2"><p>ندانست کس کآن سبب چیست و چون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سواری سوی کوش شد همچو باد</p></div>
<div class="m2"><p>که دشمن به لشکرگه اندر فتاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه کوه و هامون پُر از لشکر است</p></div>
<div class="m2"><p>سراسر همه دشت بی تن سراست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بترسید و بر کس نکرد او پدید</p></div>
<div class="m2"><p>رخش گشت ماننده ی شنبلید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بفرمود تا از سپه صدهزار</p></div>
<div class="m2"><p>برفتند کار آزموده سوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدان تا بدانند کاین کار چیست</p></div>
<div class="m2"><p>سپاه از چه کردار و سالار کیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی گفت کاین کار آهنگر است</p></div>
<div class="m2"><p>کمین است و بر لشکر او مهتر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو پیش سراپرده آمد سپاه</p></div>
<div class="m2"><p>ز شمشیر و نیزه ندیدند راه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سراپرده و خیمه و رخت پاک</p></div>
<div class="m2"><p>گرفته یکی آتش سهمناک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه روی گیتی پُر از دود بود</p></div>
<div class="m2"><p>گریزنده را سر همی سود بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان دید لشکرش آسیمه مست</p></div>
<div class="m2"><p>برفتند و از هم بدادند دست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بماندند با کوش سیصد هزار</p></div>
<div class="m2"><p>همه بسته خوش اندر آن کارزار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه دشت دید آتش افروخته</p></div>
<div class="m2"><p>سراپرده و خیمه ها سوخته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سپه کُشته و قارن کینه جوی</p></div>
<div class="m2"><p>روان کرده خون دلیران به جوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شکسته همه تخت و خرگاه را</p></div>
<div class="m2"><p>گرفته بر او بر سرِ راه را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سپه را در آن هول، دل داد و گفت</p></div>
<div class="m2"><p>که اکنون دلیری نباید نهفت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر سستی آریم، گردیم اسیر</p></div>
<div class="m2"><p>به دست چنین بی بنان حقیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همان به که تن کشته گردد به جنگ</p></div>
<div class="m2"><p>که در سال بسته سر پالهنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بگفت و برآویخت چون اژدها</p></div>
<div class="m2"><p>که تا چون کند جان شیرین رها</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به شمشیر، دشمن ز ره دور کرد</p></div>
<div class="m2"><p>روان اندر آن رزم رنجور کرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان باز پس کرد مردم ز راه</p></div>
<div class="m2"><p>گریزان فراوان سپه شد تباه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بکُشت و بخست از دلیران بسی</p></div>
<div class="m2"><p>نبرده نیامد به پیشش کسی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به تاراج داد آن همه هرچه بود</p></div>
<div class="m2"><p>اگر اسب اگر جامه ی ناپسود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همان بدره و پرده و چارپای</p></div>
<div class="m2"><p>ببردند گردان رزم آزمای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنان تا شب تیره شد ساز گشت</p></div>
<div class="m2"><p>بدان تیرگی پهلوان بازگشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپاه فریدون چو آمد فرود</p></div>
<div class="m2"><p>همی داد مر پهلوان را درود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنین گفت پس پهلوان با قباد</p></div>
<div class="m2"><p>که رو با سپاه از پی دیوزاد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گزین کن ز گندآوران صد هزار</p></div>
<div class="m2"><p>سر دیو پیکر به نزد من آر</p></div></div>