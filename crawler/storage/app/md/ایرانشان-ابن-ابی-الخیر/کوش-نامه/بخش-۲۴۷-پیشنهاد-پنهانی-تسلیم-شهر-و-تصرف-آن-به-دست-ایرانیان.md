---
title: >-
    بخش ۲۴۷ - پیشنهاد پنهانی تسلیم شهر، و تصرف آن به دست ایرانیان
---
# بخش ۲۴۷ - پیشنهاد پنهانی تسلیم شهر، و تصرف آن به دست ایرانیان

<div class="b" id="bn1"><div class="m1"><p>نهانی فرستاده ای تاختند</p></div>
<div class="m2"><p>ز هرگونه گفتارها ساختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گر پهلوان کینه ماند بجای</p></div>
<div class="m2"><p>به سوگند پیمان کند با خدای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ما را نیازارد از هیچ روی</p></div>
<div class="m2"><p>نه یاد آورد جنگ و نه گفت و گوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب تیره او را سپاریم شهر</p></div>
<div class="m2"><p>که بر ما زمانه پراگند بهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همانگاه قارن بدو داد دست</p></div>
<div class="m2"><p>به سوگند، شمشیر و لشکر ببست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که کس را نیارد زمانی به روی</p></div>
<div class="m2"><p>جز آن را که گردد سرش جنگجوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرستاده گفت ای جهان پهلوان</p></div>
<div class="m2"><p>کنون رازها را گشادن توان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مر این شهر ما را فراوان دَرَست</p></div>
<div class="m2"><p>برآن هر دری بر یکی مهتر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مر او را سپاه است پنجه هزار</p></div>
<div class="m2"><p>دلیران چین و سُواران ار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهانی به بالا دری دیگر است</p></div>
<div class="m2"><p>که از ماست آن کاو بدان در سر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب تیره با لشکری رزمساز</p></div>
<div class="m2"><p>بیا تا گشاییم دروازه باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خانه درآیید بی داوری</p></div>
<div class="m2"><p>تو دانی همی کوش با لشکری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل پهلوان گشت از آن مرد شاد</p></div>
<div class="m2"><p>نهانی ورا جامه و زر بداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدو گفت چون اندر آیم به شهر</p></div>
<div class="m2"><p>ببخشمت یک مرده از گنج بهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرستاد با او یکی نامور</p></div>
<div class="m2"><p>بدان تا نماید به دروازه در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شب آمد برافگند برگستوان</p></div>
<div class="m2"><p>کمر بست با صد هزاران گوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیامد بدان در که چینی نمود</p></div>
<div class="m2"><p>در شهر بگشاد چینی چو دود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپاه اندر آورد و آوای نای</p></div>
<div class="m2"><p>چنان شد که کیوان یله کرد جای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تبیره زنان زخم برداشتند</p></div>
<div class="m2"><p>خروشیدن از ابر بگذاشتند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب تیره و نیزه و تیغ و گبر</p></div>
<div class="m2"><p>خروش دلیران رسیده به ابر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپاهی چو بشنید از آن سان خروش</p></div>
<div class="m2"><p>رمید از تنش توش، وز مغز هوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سراسیمه از جای بر جَست مرد</p></div>
<div class="m2"><p>بدانست کایدر زمانه چه کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گروهی نهان شد به خانه درون</p></div>
<div class="m2"><p>گروهی همی تاخت تا پیش خون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو خورشید بر خاک زد رنگ خویش</p></div>
<div class="m2"><p>نمود او به چرخ روان سنگ خویش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپه دیده بگشاد و لشکر بدید</p></div>
<div class="m2"><p>چو از باد لاله فرو پژمرید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه تیغ و جوشن فرو ریختند</p></div>
<div class="m2"><p>زبانها به لابه برانگیختند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سپهدار قارن ببخشودشان</p></div>
<div class="m2"><p>از این بیش کُشتن نفرمودشان</p></div></div>