---
title: >-
    بخش ۵۸ - اندیشه ی شاه چین درباره ی فرزند دیو چهره اش
---
# بخش ۵۸ - اندیشه ی شاه چین درباره ی فرزند دیو چهره اش

<div class="b" id="bn1"><div class="m1"><p>ز هر گونه گفتار، گوینده گفت</p></div>
<div class="m2"><p>شه چین از اندیشه ی این نخفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روانش همیشه در اندیشه بود</p></div>
<div class="m2"><p>از آن بچّه کافگنده در بیشه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلش ز آن، نشان داد هرگه درست</p></div>
<div class="m2"><p>ورا راز گردون همی بازجست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که این دیو چهره ز پشت من است</p></div>
<div class="m2"><p>که گفتم نژادش ز آهرمن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز آن نیست کافگندمش زار و خوار</p></div>
<div class="m2"><p>در آن بیشه در پیش مردارخوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بردادِ یزدان نکردم پسند</p></div>
<div class="m2"><p>هم از وی رسانید بر من گزند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه شب همی بود پیچان چو مار</p></div>
<div class="m2"><p>گه اندر شگفتی گه اندر شمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که چند است کآن کودک آمد پدید؟</p></div>
<div class="m2"><p>که این روز زین سان کمر برکشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چهل سال، سالش نباشد فزون</p></div>
<div class="m2"><p>یکی بود همچو[ن] کُه بیستون</p></div></div>