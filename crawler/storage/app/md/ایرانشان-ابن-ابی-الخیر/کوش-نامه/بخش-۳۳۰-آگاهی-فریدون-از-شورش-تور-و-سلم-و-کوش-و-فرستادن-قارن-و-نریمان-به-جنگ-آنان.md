---
title: >-
    بخش ۳۳۰ - آگاهی فریدون از شورش تور و سلم و کوش و فرستادن قارن و نریمان به جنگ آنان
---
# بخش ۳۳۰ - آگاهی فریدون از شورش تور و سلم و کوش و فرستادن قارن و نریمان به جنگ آنان

<div class="b" id="bn1"><div class="m1"><p>وز آن جا سوی روم برگشت شاد</p></div>
<div class="m2"><p>به داد و دهش دست و دل برگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون کرد ازآن پس ز دست پدر</p></div>
<div class="m2"><p>همان آذرآبایگان سربسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی سلم شد تور با آن سپاه</p></div>
<div class="m2"><p>خراسان همی بستد از دست شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذر کرد کوش دلاور به آب</p></div>
<div class="m2"><p>همه مصر و شامات کرد آرباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی دید کاو را نباشد ز بیش</p></div>
<div class="m2"><p>ازآن مرز برداشت آن بهر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریدون از آن شورش آگاه شد</p></div>
<div class="m2"><p>بپیچید و شادیش کوتاه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرستاد و لشکر ز هر سو بخواند</p></div>
<div class="m2"><p>سخن با نریمان و قارن براند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان گفتشان خسرو پُر خرد</p></div>
<div class="m2"><p>که رنجورم از دست فرزند بد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببینید غمها که بر من رسید</p></div>
<div class="m2"><p>که پیران سرم زهر خواهند چشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرامی سرِ ایرج نامدار</p></div>
<div class="m2"><p>بُریده بدیدیم و افگنده خوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گنهکار و خونی دو فرزند نیز</p></div>
<div class="m2"><p>از این درد بتّر کسی را چه چیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون آمد آگاهی از موبدان</p></div>
<div class="m2"><p>که با کوش یکدل شدند آن بدان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سه بخش کردند گیتی همه</p></div>
<div class="m2"><p>شبان گشته از خویش همچون رمه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سزد گر شما رنج بر تن نهید</p></div>
<div class="m2"><p>سپاسی بر این رنج بر من نهید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که من پیر سر جوشن رزم و کین</p></div>
<div class="m2"><p>بپوشم که نپسنددم مرد دین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که روزان و شب پیش پروردگار</p></div>
<div class="m2"><p>بنالم همی تا مگر کردگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منوچهر را برکشد بی گزند</p></div>
<div class="m2"><p>کند نامور را و تختش بلند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگر دل کند تیز و پر کیمیا</p></div>
<div class="m2"><p>بخواهد ازآن هر دو کین نیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شما برگزینید چندان سپاه</p></div>
<div class="m2"><p>که از گرد، گردون بماند سیاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ببخشیدشان خواسته هرچه هست</p></div>
<div class="m2"><p>کران آب رویست نیروی دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوی تو ز بی مایه تر برکشید</p></div>
<div class="m2"><p>به اره به دو نیمش اندر کشید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نریمان و قارن به هنگام خواب</p></div>
<div class="m2"><p>گرفتند سوی خراسان شتاب</p></div></div>