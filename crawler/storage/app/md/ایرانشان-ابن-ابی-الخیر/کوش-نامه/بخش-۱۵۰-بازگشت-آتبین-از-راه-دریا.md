---
title: >-
    بخش ۱۵۰ - بازگشت آتبین از راه دریا
---
# بخش ۱۵۰ - بازگشت آتبین از راه دریا

<div class="b" id="bn1"><div class="m1"><p>چو رفت آتبین از بسیلا برون</p></div>
<div class="m2"><p>ز دیده زن و مرد راندند خون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانان به دل زار و بریان شدند</p></div>
<div class="m2"><p>زنان از فرارنگ گریان شدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروش آمد از کوی و برزن به دشت</p></div>
<div class="m2"><p>همی دود دل زآسمان بر گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه خواهران فرارنگ، دست</p></div>
<div class="m2"><p>زنان بر گُل و نرگس نیم مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرافراز طیهور و پیوند او</p></div>
<div class="m2"><p>همان هرچه بودند فرزند او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوان سوی دریا کنار آمدند</p></div>
<div class="m2"><p>به دیده چو ابر بهار آمدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی هرکس از دردشان خون گریست</p></div>
<div class="m2"><p>که داند که طیهور خود چون گریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفت آن گهی هر دو را در کنار</p></div>
<div class="m2"><p>فراوان ببوسید و بگریست زار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شما را به یزدان سپاریم گفت</p></div>
<div class="m2"><p>که همراهتان ایمنی باد جفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهانجوی بر شاه کرد آفرین</p></div>
<div class="m2"><p>فرارنگ بوسید روی زمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به کشتی نشستند و شه بازگشت</p></div>
<div class="m2"><p>یکی باد نوشین دمساز گشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همان گه جهاندیده ملّاح پیر</p></div>
<div class="m2"><p>روان کرد کشتی بکردار تیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه بادبانها برافراشتند</p></div>
<div class="m2"><p>جزیره به یک هفته بگذاشتند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی راند، یزدان نگه داشتش</p></div>
<div class="m2"><p>که بی باد یک روز نگذاشتش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه بی باد و نه نیز بادی درشت</p></div>
<div class="m2"><p>چنین باشد آن کس که یزدانش پشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی راند ملّاح تا پنج ماه</p></div>
<div class="m2"><p>نه آسود و نه نیز برتافت راه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پدید آمد از سوی چپ کوه قاف</p></div>
<div class="m2"><p>کشیده ست با چرخ گفتی مصاف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گذشتند ابر کوه ماهی چهار</p></div>
<div class="m2"><p>چنین تا به ده ماه شد روزگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رسیدند نزدیک یأجوج باز</p></div>
<div class="m2"><p>گروهی فراوان و کوهی دراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرفته سر کوه مانند مور</p></div>
<div class="m2"><p>بدیدند کشتی و برخاست شور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خروش اندر آن کوه و دریا فتاد</p></div>
<div class="m2"><p>چو در باغ گاه بهاران ز باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ملّاح پرسید کاین شور چیست</p></div>
<div class="m2"><p>گروهی از این گونه چون مور چیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین پاسخش داد ملّاح پیر</p></div>
<div class="m2"><p>کزاین مرزِ یأجوج بگذر چو تیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که این جانور دارد این کوه و دشت</p></div>
<div class="m2"><p>بکوشیم تا زود بتوان گذشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از این جانور بتّر اندر زمین</p></div>
<div class="m2"><p>نکرده ست پیدا جهان آفرین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر آب دریا نبودی ز پیش</p></div>
<div class="m2"><p>وگر آب، ماهی ندادی ز خویش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهان را از ایشان گزند آمدی</p></div>
<div class="m2"><p>گزندش که داند که چند آمدی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به یک روز ویران شدی این جهان</p></div>
<div class="m2"><p>از این پرگزندان و این بیرهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فزونند صدبار از آدمی</p></div>
<div class="m2"><p>نه نیکی شناسند و نه مردمی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مهین کشور از هفت کشور زمین</p></div>
<div class="m2"><p>بدین جانور داد جان آفرین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز دریاش روزی و از کوه قاف</p></div>
<div class="m2"><p>نهادِ جهان نیست، شاها، گزاف</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفت این و کشتی براندند تفت</p></div>
<div class="m2"><p>دگرباره راه دو ماهه برفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ندیدند از ایشان تهی کوه و دشت</p></div>
<div class="m2"><p>همی زآسمان بانگشان برگذشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گذشتند از ایشان و دیگر سه ماه</p></div>
<div class="m2"><p>از آن ژرف دریا براندند راه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهان را ندانم که چند است خَود</p></div>
<div class="m2"><p>که چندین ابر بخش دریا رسد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر بازجویی هنوز اندکی ست</p></div>
<div class="m2"><p>که از هفت دریا هنوز این یکی ست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فراوان شگفتی بدید اندر آب</p></div>
<div class="m2"><p>که ترسان شدی گر بدیدی به خواب</p></div></div>