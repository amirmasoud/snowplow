---
title: >-
    بخش ۲۵۵ - گفتگوی قارن با کوش درباره ی بخشایش فریدون
---
# بخش ۲۵۵ - گفتگوی قارن با کوش درباره ی بخشایش فریدون

<div class="b" id="bn1"><div class="m1"><p>وزآن پس بدو گفت قارن که شاه</p></div>
<div class="m2"><p>ببخشود و بگذشت از تو گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگر تا چه مایه نمودی گزند</p></div>
<div class="m2"><p>بجای نیاگان شاه بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو پاداش، آمرزش آمد پدید</p></div>
<div class="m2"><p>ز فرمان او سر نباید کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از این پس چنین رو که فرمود شاه</p></div>
<div class="m2"><p>تو آسان بمانی و خشنود شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بندی کمر پیش و فرمان کنی</p></div>
<div class="m2"><p>چنانچون بفرمایدت آن کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را مایه ی شهریاری دهد</p></div>
<div class="m2"><p>سرافرازی و کامگاری دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خشنود باشد ز تو شهریار</p></div>
<div class="m2"><p>کند پیش تو بندگی روزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو گفت کوش: ای سرافراز مرد</p></div>
<div class="m2"><p>چو شاه همایون مرا زنده کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه من شیر سگ خوردم و گوشت گرگ</p></div>
<div class="m2"><p>که گردن بپیچم ز شاه بزرگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر آن نیکویها ندانم نهان</p></div>
<div class="m2"><p>سزاوار کشتن منم در جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وزآن پس به خوردن کشیدند دست</p></div>
<div class="m2"><p>شب و روز با رامش و می به دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهدار قارن در ایوان خویش</p></div>
<div class="m2"><p>سرایی سزاوار مهمان خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیاراست مانند باغ ارم</p></div>
<div class="m2"><p>همه راست کرد اندر او بیش و کم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زنان شبستان او را همه</p></div>
<div class="m2"><p>بدو بازبخشید شاه رمه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگهداشت قارن همه کار او</p></div>
<div class="m2"><p>خور و خواب و پوشش سزاوار او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بتان را ز پیشش بیاراستی</p></div>
<div class="m2"><p>بدو دادی او را که او خواستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدان سان همی داشت او را بناز</p></div>
<div class="m2"><p>به بگماز و خوبان بربط نواز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که یک روز ننشست بر دلش گرد</p></div>
<div class="m2"><p>نه بر رویش آمد دم باد سرد</p></div></div>