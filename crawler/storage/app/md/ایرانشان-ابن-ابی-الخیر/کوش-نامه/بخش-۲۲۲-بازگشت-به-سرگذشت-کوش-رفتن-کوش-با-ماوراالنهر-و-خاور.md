---
title: >-
    بخش ۲۲۲ - بازگشت به سرگذشت کوش رفتن کوش با ماوراءالنهر و خاور
---
# بخش ۲۲۲ - بازگشت به سرگذشت کوش رفتن کوش با ماوراءالنهر و خاور

<div class="b" id="bn1"><div class="m1"><p>بر این داستان دگر دار گوش</p></div>
<div class="m2"><p>نگه کن به کردار و بازار کوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کوش فریبنده دیگر سخن</p></div>
<div class="m2"><p>چنین ساخت داننده مرد کهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بشکست ایران سپه را بدرد</p></div>
<div class="m2"><p>سوی ماوراالنّهر آغاز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از او شاه مکران چو آگاه شد</p></div>
<div class="m2"><p>دژم گشت و شادیش کوتاه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پذیره شدش با سران سپاه</p></div>
<div class="m2"><p>یکی مایه ور برد گنجش به راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از اسبان تازی و دیبای چین</p></div>
<div class="m2"><p>ز تخت و ز تاج و کلاه و نگین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم از خوردنی برد چندان ز شهر</p></div>
<div class="m2"><p>که یک ماه لشکرش را بود بهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز مکران گذر کرد بی جنگ و جوش</p></div>
<div class="m2"><p>بشد با سواران پولادپوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی رفت تا خاور و تیره میغ</p></div>
<div class="m2"><p>گرفت آن همه مرزها را به تیغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین تا به دریای خاور رسید</p></div>
<div class="m2"><p>سراپرده ای نو بدان لب کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مر او را چنین گفت گوینده ای</p></div>
<div class="m2"><p>به دریا شب و روز پوینده ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که شاها، یکی جایگاه است خوش</p></div>
<div class="m2"><p>در این ژرف دریا از او سرمکش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جزیره ست با آبهای روان</p></div>
<div class="m2"><p>نشستنگهی از در خسروان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پر از میوه و سایه ی بید و سرو</p></div>
<div class="m2"><p>همه کوه نخچیر و کبک و تذرو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهار و خزان سربسر گل بود</p></div>
<div class="m2"><p>ز طاووس و درّاج غلغل بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روان بر سر سبزه و بوی و رنگ</p></div>
<div class="m2"><p>نه تیمار شیر و نه بیم پلنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بشنید از او شاه وارونه خوی</p></div>
<div class="m2"><p>کشیدش بدان جایگه آرزوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درآمد به دریا و برشد به کوه</p></div>
<div class="m2"><p>بزرگان چین از پسش همگروه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برآن کوه بر کان پیروزه یافت</p></div>
<div class="m2"><p>همان زر که مانند آتش بتافت</p></div></div>