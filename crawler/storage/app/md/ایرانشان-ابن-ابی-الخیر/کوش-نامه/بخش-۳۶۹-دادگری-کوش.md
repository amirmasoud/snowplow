---
title: >-
    بخش ۳۶۹ - دادگری کوش
---
# بخش ۳۶۹ - دادگری کوش

<div class="b" id="bn1"><div class="m1"><p>وزآن پس جهاندیده کوش سترگ</p></div>
<div class="m2"><p>رها کرد راه بد و خوی گرگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو برگِرد آن پادشاهی بگَشت</p></div>
<div class="m2"><p>به فرمان او شد همه کوه و دشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر کشوری ساو و باژ آمدش</p></div>
<div class="m2"><p>که هر ماه گنجی فراز آمدش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مغرب سوی قرطبه بازگشت</p></div>
<div class="m2"><p>به آرام بنشست و دمساز گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چیز کسان در نیاویختی</p></div>
<div class="m2"><p>نه با کودک و با زن آمیختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر داد جُستی کسی زانجمن</p></div>
<div class="m2"><p>همه داد دادی میان دو تن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه ساله دور از بد بدگمان</p></div>
<div class="m2"><p>شده زیر دستان او شادمان</p></div></div>