---
title: >-
    بخش ۲۱۱ - جنگ تن به تن قباد با کوش
---
# بخش ۲۱۱ - جنگ تن به تن قباد با کوش

<div class="b" id="bn1"><div class="m1"><p>چو خورشید بر زد سر از برج گاو</p></div>
<div class="m2"><p>خروشان همی بر هوا شد چکاو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو لشکر برآمد به میدان کین</p></div>
<div class="m2"><p>بتوفید از آواز گردان زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چپ و راست، قلب و جناح سپاه</p></div>
<div class="m2"><p>بیاراست کوش و سپهدار شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تبیره به زخم آمد و بانگ کوس</p></div>
<div class="m2"><p>جهان کرد لشکر ز گرد آبنوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی خواند مردان رزم آزمای</p></div>
<div class="m2"><p>سوی رزم از آواز شیپور و نای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآمد خروش ده و دار و گیر</p></div>
<div class="m2"><p>چکاچاک زوبین و باران تیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هر دو سپه کشته آمد بسی</p></div>
<div class="m2"><p>به خون کشور آغشته آمد بسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کوش آن چنان دید با صد سوار</p></div>
<div class="m2"><p>بزرگان چین و دلیران کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهان خویشتن زد بر ایران سپاه</p></div>
<div class="m2"><p>همی بردشان سوی قلب سپاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآن حمله اندر فراوان بکشت</p></div>
<div class="m2"><p>کسی کاو توانست بنمود پشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ایرانیان هرکه او را بدید</p></div>
<div class="m2"><p>چو از گرگ آهو همی زو رمید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو دید آن سپه را گریزان قباد</p></div>
<div class="m2"><p>گرازان به تندی بهم برفتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جنگاوران اندر افتاد شور</p></div>
<div class="m2"><p>گریزان و ترسان چو از شیر گور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برآشفت و گفتا شما را چه بود</p></div>
<div class="m2"><p>کز این لشکر گشن برخاست دود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه شمشیر دشمن کنون گشت چیز</p></div>
<div class="m2"><p>کز این سان گرفتید راه گریز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شکسته سپاهی نه دست و نه پای</p></div>
<div class="m2"><p>نه نیزند مردان رزم آزمای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ستوهی نمودن کنون چیست باز</p></div>
<div class="m2"><p>نداریم شرم از شه سرفراز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سواری گریزنده آواز داد</p></div>
<div class="m2"><p>که ما را بمالد همی دیوزاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نهان اندر آمد میان سپاه</p></div>
<div class="m2"><p>گروهی پسِ پشت او کینه خواه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهم برزد آن بیکرانه سپاه</p></div>
<div class="m2"><p>بسی کرد یاران ما را تباه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل چینیان یافت پیروز کوش</p></div>
<div class="m2"><p>شدند از دلیری بدو سخت کوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سپهبد چو پاسخ چنین یافت گفت</p></div>
<div class="m2"><p>که با دیوزاده غمان باد جفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همان گه گزین کرد هفتاد مرد</p></div>
<div class="m2"><p>برافگند بر گستوان نبرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزد خویشتن بر سواران چین</p></div>
<div class="m2"><p>ز خون لاله گون کرد روی زمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپاه فریدون ز زخم قباد</p></div>
<div class="m2"><p>دلیری نمودند و دادند داد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برآویختند و برآمیختند</p></div>
<div class="m2"><p>ز خون دلیرا گِل انگیختند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه خاک با لاله همرنگ شد</p></div>
<div class="m2"><p>ز کشته زمین بر یلان تنگ شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به زخمی سواری همی کشت کوش</p></div>
<div class="m2"><p>قباد دلاور شده سخت کوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که هر زخم کز یال وی شد روان</p></div>
<div class="m2"><p>جدا کرد از اندام دشمن روان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دو لشکر بدان سان برآمد بهم</p></div>
<div class="m2"><p>که گردون شد از زخم ایشان دژم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو از نیمه ی روز بگذشت هور</p></div>
<div class="m2"><p>بماندند یکسر سوار و ستور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برابر فتادند کوش و قباد</p></div>
<div class="m2"><p>سپهبد بدو تاخت مانند باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو گفت کای بد رگ بدستیز</p></div>
<div class="m2"><p>نخواهی همی مرد تا رستخیز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نخواهد جهان از بلای تو رست</p></div>
<div class="m2"><p>کنون تا به کی کشّی ای دیو مست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من امروز برهانم از تو جهان</p></div>
<div class="m2"><p>به زخم دلیران و فرّ مهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو کوش آن سخنها شنید از قباد</p></div>
<div class="m2"><p>برآشفت و شبرنگ را چرخ داد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درآمد بکردار آذرگشسب</p></div>
<div class="m2"><p>بزد تیغ و آمدش بر یال اسب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سر بارگی چون ز تن دور شد</p></div>
<div class="m2"><p>سپهبد به دل سخت رنجور شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پیاده سوی دشمن آهنگ کرد</p></div>
<div class="m2"><p>زمانی به گرز گران جنگ کرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدو تاخت بار دگر کوش تنگ</p></div>
<div class="m2"><p>بدان تا زند تیغ الماس رنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سپهبد برآورد یکباره شور</p></div>
<div class="m2"><p>بینداخت گرز از پس وی به زور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به کتفش درآمد سر گرز راست</p></div>
<div class="m2"><p>بدان سان که از زین همی گشت خواست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز سستی بیفتاد تیغش ز دست</p></div>
<div class="m2"><p>سپهبد به اسب دگر برنشست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>برانگیخت و آهنگ او کرد باز</p></div>
<div class="m2"><p>برآویخت با او زمانی دراز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نبودند بر یکدگر دستیاب</p></div>
<div class="m2"><p>شب آمد گران شد سر از رنج خواب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جدا شد ز یکدیگران دو سپاه</p></div>
<div class="m2"><p>ز هم بازگشتند سالار و شاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه سرکشان پیش کوش آمدند</p></div>
<div class="m2"><p>به خوان و خورشهای نوش آمدند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدیشان چنین گفت کامروز رزم</p></div>
<div class="m2"><p>به چشمم چنان بود چون جای بزم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سپهدار ایران به ما باز خورد</p></div>
<div class="m2"><p>ز جانش برآورده بودیم گرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بکشتم به زیر اندرش بارگی</p></div>
<div class="m2"><p>نهاد اندر او روی بیچارگی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو سستی نمودند پر مایگان</p></div>
<div class="m2"><p>بجست او ز شمشیر ما رایگان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دویدند یارانش یکباره پیش</p></div>
<div class="m2"><p>کشیدند باره سوارانش پیش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رها شد ز دست من آن کینه جوی</p></div>
<div class="m2"><p>اگر باز فردا ببینمش روی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>من او را به یک زخم بیجان کنم</p></div>
<div class="m2"><p>دل لشکر از درد پیچان کنم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>می روشن آورد تا نیمشب</p></div>
<div class="m2"><p>به بازی و رامش گشادند لب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو دیده شد از خواب و باده گران</p></div>
<div class="m2"><p>سوی خیمه رفتند گندآوران</p></div></div>