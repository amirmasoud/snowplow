---
title: >-
    بخش ۲۷۳ - بازگشت به مغرب و کشتن قراطوس
---
# بخش ۲۷۳ - بازگشت به مغرب و کشتن قراطوس

<div class="b" id="bn1"><div class="m1"><p>چو آمد به دشت اریله فرود</p></div>
<div class="m2"><p>سراپرده و خیمه زد پیش رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان یافت مغرب که هرگز نبود</p></div>
<div class="m2"><p>همه باغ و پالیز و کشت و درود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درخت برومند و آب روان</p></div>
<div class="m2"><p>همه سبزه اندر خور خسروان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه مرغزارش پر از گوسفند</p></div>
<div class="m2"><p>همه جویبارش درخت بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن شادمانی یکی بزم ساخت</p></div>
<div class="m2"><p>ز گردون همی تاج را برفراخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قراطوسیان را بدان بزم خواند</p></div>
<div class="m2"><p>سران سپه را به خوردن نشاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قراطوس غمخواره در بند بود</p></div>
<div class="m2"><p>به بند اندرون نیز خرسند بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زن و بچّه و خویش و پیوند او</p></div>
<div class="m2"><p>همه خوار و بیچاره در بند او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو سرمست شد خواست او را به پیش</p></div>
<div class="m2"><p>بدو گفت کای بدرگِ زشت کیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز فرمان ما سر چرا تافتی</p></div>
<div class="m2"><p>چرا رزم را خیره پنداشتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قراطوس از آن هول و تندی و خشم</p></div>
<div class="m2"><p>نه پاسخش داد و نه بر کرد چشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برآورد می در سرِ کوش جوش</p></div>
<div class="m2"><p>بزد تیغ و انداختش سر ز دوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل مردم بزم از آن زخم تیغ</p></div>
<div class="m2"><p>رمید و گرفتند راه گریغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی هرکسی گفت از این دو سپاه</p></div>
<div class="m2"><p>که این دیو گم باد از تخت و گاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدان روز کاو این سپه یافت و گنج</p></div>
<div class="m2"><p>که کشت آن سپاه دلاور برنج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدین سان نکشته است اسیر ایچ کس</p></div>
<div class="m2"><p>تو ای داور پاک، فریادرس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز کار قراطوس و کردر کوش</p></div>
<div class="m2"><p>همی هرکه بشنید از او رفت هوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پراگنده شد سهم او در جهان</p></div>
<div class="m2"><p>به نزد کهان و به نزد مهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وز آن جایگه لشکر اندر کشید</p></div>
<div class="m2"><p>همه باختر را سراسر بدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نبودی جز این خواسته بس بدی</p></div>
<div class="m2"><p>از او پوشش و بخش هرکس بدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان خوب و آباد بود آن زمین</p></div>
<div class="m2"><p>که گفتند هرگز نبود این چنین</p></div></div>