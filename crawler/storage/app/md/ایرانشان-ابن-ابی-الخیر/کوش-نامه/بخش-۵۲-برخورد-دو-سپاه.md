---
title: >-
    بخش ۵۲ - برخورد دو سپاه
---
# بخش ۵۲ - برخورد دو سپاه

<div class="b" id="bn1"><div class="m1"><p>شه چین چو با لشکر آن جا رسید</p></div>
<div class="m2"><p>چنان ساخته آتبین را بدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم از پشت شبرنگش آواز داد</p></div>
<div class="m2"><p>که ای بدکنش ریمن بدنژاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این بیشه چندی توانید بود</p></div>
<div class="m2"><p>چو آمد زمانه، ز چاره چه سود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی ره شتابد ز سوراخ مار</p></div>
<div class="m2"><p>چو از وی برآرد زمانه دمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بر آتبین افگنم چشم خویش</p></div>
<div class="m2"><p>برانم بر او بی گمان خشم خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جان برادر که شاه من است</p></div>
<div class="m2"><p>به هر کار پشت و پناه من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که زنده نمانم ز جمشیدیان</p></div>
<div class="m2"><p>سواری که بندد به کینه میان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شما را همه پیش این شوربخت</p></div>
<div class="m2"><p>بیاویزم از شاخهای درخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کُشم هر گُره را به پادافرهی</p></div>
<div class="m2"><p>فرستم به شهری سر هر مهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین گفته بود آتبین با سپاه</p></div>
<div class="m2"><p>که بر پاسخ او ببندند راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود کز شتاب اندر آید به آب</p></div>
<div class="m2"><p>که از کار دیو است کار شتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو لختی از این سو بیاید سپاه</p></div>
<div class="m2"><p>برایشان بگیریم هر گونه راه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که اسبانشان خسته و مانده اند</p></div>
<div class="m2"><p>ز چین روز و شب بیرکان رانده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود کایزد پاک پرورگار</p></div>
<div class="m2"><p>دگر بار از ایشان بر آرد دمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شه چین از ایشان چو پاسخ نیافت</p></div>
<div class="m2"><p>بر آشفت و سوی گذر گه شتافت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدو گفت گوینده کای شهریار</p></div>
<div class="m2"><p>در این روز تنگ است ما را شمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به یکباره صد مرد نتوان شکست</p></div>
<div class="m2"><p>فرو باید آمد بر این پهن دشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک امشب گر ایدر درنگ آوری</p></div>
<div class="m2"><p>از آن به که نامت به ننگ آوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که دشمن بسی کرده باشد بنه</p></div>
<div class="m2"><p>به شب رفت خواهد همی یک تنه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شبگیر ما بگذرانیم رود</p></div>
<div class="m2"><p>از آن سو نیاییم از اسبان فرود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسازیم بر پی چو تیر از کمان</p></div>
<div class="m2"><p>بیابیم بر دشمنان بی گمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر آن گه که شد آتبین خود اسیر</p></div>
<div class="m2"><p>بدوزیم پشت سپاهی به تیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وگر بر لب رود گیرد درنگ</p></div>
<div class="m2"><p>به کشتی توانی شدن سوی جنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو گفتار گوینده بشنید شاه</p></div>
<div class="m2"><p>همان جا فرود آورید آن سپاه</p></div></div>