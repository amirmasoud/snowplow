---
title: >-
    بخش ۱۷۸ - نیرنگ کوش با طیهور
---
# بخش ۱۷۸ - نیرنگ کوش با طیهور

<div class="b" id="bn1"><div class="m1"><p>چو برداشت کوش از میان کین و جنگ</p></div>
<div class="m2"><p>نمود اندر این روزگاری درنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکرد او به کار بسیلا نگاه</p></div>
<div class="m2"><p>وز او ایمنی یافت طیهور شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی گشت با لشکری سال چند</p></div>
<div class="m2"><p>چو شد پادشاهی همه بی گزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی مایه ور مرد با دستگاه</p></div>
<div class="m2"><p>ز چین آمدی پیش او سال و ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبودش به چین کس جز او راز دار</p></div>
<div class="m2"><p>زبانش به سوگند کرد استوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفت مگشای بر کس تو راز</p></div>
<div class="m2"><p>از ایدر برو کاروانی بساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نزدیک طیهور شو بارخواه</p></div>
<div class="m2"><p>مر او را همی هدیه بر چند گاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گستاخ گشتی پرستش نمای</p></div>
<div class="m2"><p>بر او هر گهی مهربانی فزای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببر هرچه خواهد ز ماچین و چین</p></div>
<div class="m2"><p>ز چیزی که خیزد ز مکران زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مر آن ساده دل را چنانی نمای</p></div>
<div class="m2"><p>که من خود یکی چاکرم زین سرای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دانی که ایمن شد او برتو بر</p></div>
<div class="m2"><p>مرا بازگوی آن همه دربدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین کرد کاو گفت، بازارگان</p></div>
<div class="m2"><p>همی برد هر گه ز چین کاروان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو با شاه، مرد آشنایی فگند</p></div>
<div class="m2"><p>خرد یافت با مرد و بودش پسند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی خواست هر چیز و یا می خرید</p></div>
<div class="m2"><p>همی برد نزدیک او هر چه دید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پرستش مر او را از آن سان نمود</p></div>
<div class="m2"><p>که هر بار هدیه بسی برفزود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان استوارش همی داشت شاه</p></div>
<div class="m2"><p>که بی باز بگذشتی از باژگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسی آزمودش به سود و زیان</p></div>
<div class="m2"><p>ندید او همی کژّی اندر میان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان خواجه با شاه دمساز گشت</p></div>
<div class="m2"><p>که با او شب و روز همراز گشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کسی را که گردون رساند گزند</p></div>
<div class="m2"><p>دل و هوش او اندر آرد به بند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز هشیاری و دانش او را چه سود</p></div>
<div class="m2"><p>که گردون همی رنج خواهد نمود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دوان رفت بازارگان پیش کوش</p></div>
<div class="m2"><p>سخنها همه بازگفتش بهوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخندید و گفت ای جهاندیده مرد</p></div>
<div class="m2"><p>یکی خویشتن رنجه بایدت کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو آمیختی رنگ و نیرنگ و ساز</p></div>
<div class="m2"><p>بگویش که ای شاه گردنفراز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چرا این همه ساز خواهی ز چین</p></div>
<div class="m2"><p>نخواهی تو هرگز سلیح گزین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که در چین همی تیغ و جوشن کنند</p></div>
<div class="m2"><p>که مانند خورشید روشن کنند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز برگستوان و ز خود و زره</p></div>
<div class="m2"><p>که پیدا نباشدش بند و گره</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر گویدت زآن نخواهم همی</p></div>
<div class="m2"><p>که رنج تن تو بکاهم همی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>و دیگر مگر کوش دارد دریغ</p></div>
<div class="m2"><p>که از شهر او جوشن آری و تیغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگویش که او سخت دور است ازاین</p></div>
<div class="m2"><p>بگو تا چه خواهی که آرم ز چین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه آن نیکدل راه دارد نگاه</p></div>
<div class="m2"><p>پدر خواند او مر تو را سال و ماه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس آن چیزها کاو بخواهد، ببر</p></div>
<div class="m2"><p>ز بر گستوان و ز تیغ و تبر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو این چیزها برده باشی سه بار</p></div>
<div class="m2"><p>مرا بازگو تا بسازیم کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فریبنده چون پیش طیهور شد</p></div>
<div class="m2"><p>زبانش به گفتار رنجو شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدان هدیه ها بیشتر برفزود</p></div>
<div class="m2"><p>بس از خویشتن مهربانی نمود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنین گفت با او میان سخن</p></div>
<div class="m2"><p>که خیره بماندستم از شاه، من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که هر چیز خواهد ز ماچین و چین</p></div>
<div class="m2"><p>نخواهد همی جوشن و تیغ کین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که در هفت کشور زمین هیچ جای</p></div>
<div class="m2"><p>نباشد چنان خنجر جانگزای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان جوشن و تیغ و بر گستوان</p></div>
<div class="m2"><p>به گیتی ندیده ست پیر و جوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین است، گفتا که گویی همی</p></div>
<div class="m2"><p>جز از مهربانی نجویی همی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرا هرگه اندیشه آید فراز</p></div>
<div class="m2"><p>که از تو بخواهم ز هرگونه ساز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دگر باره گویم نباید که کوش</p></div>
<div class="m2"><p>بیازارد از تو، برآید بجوش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نخواهد که ایدر سلاح آوری</p></div>
<div class="m2"><p>پس از خشم او رنج و کیفر بری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فریبنده بازارگان گفت، شاه</p></div>
<div class="m2"><p>نه همواره دارد همی ره نگاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو گفت کای نیکدل، روز و شب</p></div>
<div class="m2"><p>پدر خواندت چون گشاید دو لب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همی هرچه آرم ندارد دریغ</p></div>
<div class="m2"><p>دریغ از چه دارد همی گرز و تیغ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بخندید طیهور، گفتا رواست</p></div>
<div class="m2"><p>مرا خود همین آرزو و هواست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی برد بازارگان ساز جنگ</p></div>
<div class="m2"><p>که روزی به منزل نبودش درنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنین تا برفت و بیامد سه بار</p></div>
<div class="m2"><p>همه بار او آهن آبدار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رسانید از آن پس به کوش آگهی</p></div>
<div class="m2"><p>که کرد آنچه فرموده بودی رهی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دل پیل دندان از آن گشت شاد</p></div>
<div class="m2"><p>فراوان براو آفرین کرد یاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به چین اندر افگند یکسر خبر</p></div>
<div class="m2"><p>که دارای مکران برآورد سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بگشت از ره داد و فرمانبری</p></div>
<div class="m2"><p>همی سر بتابد بدین داوری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>وزآن پس گزین کرد صد مرد گرد</p></div>
<div class="m2"><p>دلیران هشیار با دستبرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که بودند در لشکرش نامدار</p></div>
<div class="m2"><p>همه رزمزن درخور کارزار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مر آن هر یکی را یکی اسب داد</p></div>
<div class="m2"><p>در گنج پرمایه تر برگشاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فرستاد پس هر یکی از هزار</p></div>
<div class="m2"><p>ز دینار وز جامه ی زرنگار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>غلامی نکو روی با دلبری</p></div>
<div class="m2"><p>فرستاد نزدیک هر مهتری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به جامه نکودار بازارگان</p></div>
<div class="m2"><p>برآوردشان شاه خوارزم کان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از آن تیغ هندی و صد از زره</p></div>
<div class="m2"><p>نهادند در بار و برزد گره</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هم از جوشن و خود صدصد بنیز</p></div>
<div class="m2"><p>بدو داد هرگونه ای ساز و چیز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یلان را چنان جامه پوشیده شاه</p></div>
<div class="m2"><p>که دارند بازارگانان به راه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بسی سازهای گرانمایه نیز</p></div>
<div class="m2"><p>همه بار کردند و هرگونه چیز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به راه بسیلا گسی کرد و گفت</p></div>
<div class="m2"><p>که باید که با باد باشید جفت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به بازارگان چشم دارید و گوش</p></div>
<div class="m2"><p>همه بشنوید آنچه گوید بهوش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کسی کاو ز فرمان او سرکشید</p></div>
<div class="m2"><p>ز پادافرهم سخت کیفر کشید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنین گفت با خواجه زآن پس به راز</p></div>
<div class="m2"><p>که این سرکشان را بدین برگ و ساز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همه زیر فرمان تو کرده ام</p></div>
<div class="m2"><p>ز بهر چنین روز پرورده ام</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو رفتند نزدیک در بندیان</p></div>
<div class="m2"><p>بیایند ده شیر را زان میان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شما را برآرند بر تیغ کوه</p></div>
<div class="m2"><p>به پایان نباشند دیگر گروه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو شب تیره گردد شما بیدرنگ</p></div>
<div class="m2"><p>همه بر کشید از میان تیغ جنگ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مرآن همگنان را ببرّید سر</p></div>
<div class="m2"><p>که من کرده باشم به دریا گذر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>یلان را چو از خاک بستر کنید</p></div>
<div class="m2"><p>همان گه یکی آتشی برکنید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که من چون ببینم بیایم به راه</p></div>
<div class="m2"><p>به دربندیان برسپارم سپاه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو غلغل رسد بر سر تیغ کوه</p></div>
<div class="m2"><p>بباشید پنجاه تن زین گروه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز بهر نگهداشت آن ژرف چاه</p></div>
<div class="m2"><p>که ره را همی داشت باید نگاه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دگر مردم آن جا ممانید دیر</p></div>
<div class="m2"><p>شتابنده چون سوی نخچیر، شیر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>برآرید از آن باژبانان دمار</p></div>
<div class="m2"><p>به شمشیر و زوبین زهر آبدار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>یکی تا گشاده شود راه من</p></div>
<div class="m2"><p>برآیم به دربند با انجمن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بشد کاروان و سوم روز کوش</p></div>
<div class="m2"><p>بشد با سواران پولاد پوش</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو آمد به دربند بازارگان</p></div>
<div class="m2"><p>پذیره شدندش همه باژبان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ز دریا به خشکی کشیدند بار</p></div>
<div class="m2"><p>بسی خواسته دادشان بیشمار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بدان سان که هر بار دادی همی</p></div>
<div class="m2"><p>سپاسی بدیشان نهادی همی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>همه بارها را بجستند زود</p></div>
<div class="m2"><p>همه ساز و آرایش جنگ بود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به بازارگان گفت سالار بار</p></div>
<div class="m2"><p>که این بار یکسر سلیح است و کار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بدو گفت کاین شاه تو خواسته ست</p></div>
<div class="m2"><p>بدین آرزو دل بیاراسته است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدیدی که زین پیش بردم سه بار</p></div>
<div class="m2"><p>همان ساز و آرایش کارزار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بیاراست پرمایه گنجی براین</p></div>
<div class="m2"><p>بسیلا توانگرتر اکنون زچین</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو پاسخ چنین یافت، خرسند شد</p></div>
<div class="m2"><p>ز کشتی به نزدیک دربند شد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تنی ده فرستاد با کاروان</p></div>
<div class="m2"><p>چه از سالخورده چه مرد جوان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز دربند بگذشت بر تیغ کوه</p></div>
<div class="m2"><p>فرود آمدند آن یلان همگروه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جوانی ز شهر بسیلا دوید</p></div>
<div class="m2"><p>که انبوهی از دور مردم بدید</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بدان تا برد کاروان سوی شهر</p></div>
<div class="m2"><p>فزون زآن نیابد کس از رنج بهر</p></div></div>