---
title: >-
    بخش ۱۹ - داستان دقیانُس
---
# بخش ۱۹ - داستان دقیانُس

<div class="b" id="bn1"><div class="m1"><p>همان داستانی دگر یار اوی</p></div>
<div class="m2"><p>ز کردار دقیانُس و کار اوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز آن هفت تن همنشست وندیم</p></div>
<div class="m2"><p>که بودند اصحاب کهف ورقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی کوه بینی رسیده به ابر</p></div>
<div class="m2"><p>همه جای شیر و پلنگان و ببر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نزدیک طرطوس رُسته چنان</p></div>
<div class="m2"><p>چو دیوار پیوسته با آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سرداب جایی بیابان و کوه</p></div>
<div class="m2"><p>که از دیدنش دیو گردد ستوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی غار تاریک نا دلفروز</p></div>
<div class="m2"><p>کجا شب همانش، همان است روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی روشنایی کشد باز راه</p></div>
<div class="m2"><p>یکی نردبانی ز سنگ سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تراشیده هشتاد پایه فزون</p></div>
<div class="m2"><p>که داند به گیتی که چندست و چون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بالا درآیی به کهف اندر آی</p></div>
<div class="m2"><p>یکی کاخ پیش آیدت دلشای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو اندرون سیزده با سگی</p></div>
<div class="m2"><p>بر آن سیزده بر نجنبد رگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روان رفته و مانده تنشان بجای</p></div>
<div class="m2"><p>چگونه شگفت است کار خدای!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن سیزده هفت بودند و سگ</p></div>
<div class="m2"><p>که برداشتند از بر شهر تگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ایشان یکی خوبچهره غلام</p></div>
<div class="m2"><p>به هنگام مردی رسیده تمام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دقیانُس از روم بگریختند</p></div>
<div class="m2"><p>به دام بلا درنیاویختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو در کهف رفتند، یزدان پاک</p></div>
<div class="m2"><p>برآورد از اندامشان جان پاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شد دین عیسی به روم آشکار</p></div>
<div class="m2"><p>بر آن هفت تن، شش دگر گشت یار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز یزدان پرستان و پاکان دین</p></div>
<div class="m2"><p>که بودند یار مسیح گزین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو مشتی به کهف اندر افزودشان</p></div>
<div class="m2"><p>یکایک به دارو بیندودشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که تا کالبدشان نگردد تباه</p></div>
<div class="m2"><p>چنین است کهف و چنین است راه</p></div></div>