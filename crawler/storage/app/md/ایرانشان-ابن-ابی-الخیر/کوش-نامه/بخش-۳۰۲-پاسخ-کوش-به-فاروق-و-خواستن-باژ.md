---
title: >-
    بخش ۳۰۲ - پاسخ کوش به فاروق و خواستن باژ
---
# بخش ۳۰۲ - پاسخ کوش به فاروق و خواستن باژ

<div class="b" id="bn1"><div class="m1"><p>منا دیگران را بفرمود شاه</p></div>
<div class="m2"><p>که برگشت تازان به پیش سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که هر کس کز این مایه ور لشکرید</p></div>
<div class="m2"><p>زمین خلایق به پی مسپرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباید که آن خاک بیند سوار</p></div>
<div class="m2"><p>جز آن گه که فرمان دهد شهریار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وزآن پس نویسنده را پیش خواند</p></div>
<div class="m2"><p>به پاسخ فراوان سخنها براند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گفت کاین نامه برخواندم</p></div>
<div class="m2"><p>فرستاده را پیش بنشاندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپرسیدم از دانش و رای تو</p></div>
<div class="m2"><p>پسندیدم این جای بر جای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را گر نبودی دل هوشیار</p></div>
<div class="m2"><p>برآوردی از تو زمانه دمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولیکن خرد کار بستی نخست</p></div>
<div class="m2"><p>کنون تاج و تخت مهی آنِ توست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا آن جفا پیشه ی تیره هوش</p></div>
<div class="m2"><p>نیامد کمر بسته نزدیک کوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو پیش آمدی همچو تو بنده وار</p></div>
<div class="m2"><p>همان گه ز ما یافتی زینهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو در شهر با من برآراست جنگ</p></div>
<div class="m2"><p>بکندمش کاخ و ندادم درنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو پنداشت کان باره گردون شده ست</p></div>
<div class="m2"><p>کنون پست مانند هامون شده ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سزای وی این بود و پاداشش این</p></div>
<div class="m2"><p>تو نیز اندر این کار بهتر ببین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کمر بر میان بند و پیش من آی</p></div>
<div class="m2"><p>چو خواهی که مانَد به تو تخت و جای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر رزم بودی تو را نیز رای</p></div>
<div class="m2"><p>بهای زمانه ببردی ز جای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر سال فرّخ چو آید فراز</p></div>
<div class="m2"><p>فرستی سوی گنج ما ساو و باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غلام و کنیزک ز هر یک هزار</p></div>
<div class="m2"><p>به بالا چو سرو و به رخ چون بهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که مشکوی ما را بشایند و بس</p></div>
<div class="m2"><p>به لب نارسیده لب هیچ کس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر هرچه پُرمایه داری ز گنج</p></div>
<div class="m2"><p>سوی ما فرستی که دیدیم رنج</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو کردی چنین، رَستی از چنگ من</p></div>
<div class="m2"><p>نبینی سر تیغ و آهنگ من</p></div></div>