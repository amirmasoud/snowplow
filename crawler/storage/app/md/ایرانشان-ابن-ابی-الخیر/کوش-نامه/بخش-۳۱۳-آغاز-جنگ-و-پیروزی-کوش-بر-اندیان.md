---
title: >-
    بخش ۳۱۳ - آغاز جنگ و پیروزی کوش بر اندیان
---
# بخش ۳۱۳ - آغاز جنگ و پیروزی کوش بر اندیان

<div class="b" id="bn1"><div class="m1"><p>دو لشکر برابر بر آن دشت کین</p></div>
<div class="m2"><p>تو گفتی که شد تنگ روی زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان گفتی از غم به جوش آمده ست</p></div>
<div class="m2"><p>ستاره به بانگ و خروش آمده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلایه ز لشکر بهم بازخورد</p></div>
<div class="m2"><p>برآمد خروش ده و دار و برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک حمله ایرانیان را ز جای</p></div>
<div class="m2"><p>ببردند مردان رزم آزمای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه مایه بکشتند و کردند اسیر</p></div>
<div class="m2"><p>چه مایه دگر خسته ی تیغ و تیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیح و سواران و اسبان جنگ</p></div>
<div class="m2"><p>سوی کوش بردند مردان جنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپاهش بدان پیشدستی گرفت</p></div>
<div class="m2"><p>همه شادمان گشت و نیرو گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزد کوس و برداشت از جایگاه</p></div>
<div class="m2"><p>به یک منزلی اندر آمد سپاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی جای بگزید دشوار و تنگ</p></div>
<div class="m2"><p>فرود آمد و کرد لشکر درنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طلایه برون آمد از هر دو روی</p></div>
<div class="m2"><p>دو سالار گردنکش کینه جوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز ایرانیان اندیان دلیر</p></div>
<div class="m2"><p>برون تاخت با لشکری همچو شیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو از دور گرد طلایه بدید</p></div>
<div class="m2"><p>بزد دست و شمشیر کین برکشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به یک حمله از جای برکندشان</p></div>
<div class="m2"><p>گریزان به لشکرگه افگندشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان غلغل اندر سپاه اوفتاد</p></div>
<div class="m2"><p>که کوش اندر آمد به باره چو باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز مردم بپرسید کاین بار چیست</p></div>
<div class="m2"><p>خروشیدن و رزم و آواز چیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی گفت کای شهریار دلیر</p></div>
<div class="m2"><p>مگر بر طلایه سپه گشت چیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهانجوی بی مغفر و بی زره</p></div>
<div class="m2"><p>به ابرو برافگند ازآن سان گره</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزد دست و گرز گران برکشید</p></div>
<div class="m2"><p>چو باد از میان سپه بردمید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان حمله آورد بر اندیان</p></div>
<div class="m2"><p>که بر گور پی خسته شیر ژیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به یکبار برداشت لشکر ز جای</p></div>
<div class="m2"><p>فراوان در آوردشان زیر پای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ستوه آمد آن لشکر از زخم اوی</p></div>
<div class="m2"><p>نهادند زی لشکر خویش روی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فراوان بکوشید گرد اندیان</p></div>
<div class="m2"><p>مگر بازگردند ایرانیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیامد کسی پیش آن پیل مست</p></div>
<div class="m2"><p>چه مایه بکشت و چه مایه بخست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز خشم اندیان تاخت نزدیک او</p></div>
<div class="m2"><p>چو دید آن چنان چهر تاریک او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بترسید و جوشن بیفگند و تفت</p></div>
<div class="m2"><p>سوی لشکر آورد روی و برفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی تاخت کوش و همی کشت مرد</p></div>
<div class="m2"><p>چنین تا هوا رنگ رخ تیره کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وزآن جا سوی تخت شد شادکام</p></div>
<div class="m2"><p>به گردون گردان برآورده نام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو آمد بنزدیک سلم، اندیان</p></div>
<div class="m2"><p>چنین گفت کای شهریار کیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو ما با طلایه برآویختیم</p></div>
<div class="m2"><p>یکی گَرد تیره برانگیختیم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به یک حمله از بن بکندیمشان</p></div>
<div class="m2"><p>به لشکرگه اندر فگندیمشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز ناگه سواران برون تاختند</p></div>
<div class="m2"><p>به ما بر ز کین، تاختن ساختند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سواری برون رفت پیش از همه</p></div>
<div class="m2"><p>چو ارغنده شیر از میان رمه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدان سهم و آن هول و نیروی دست</p></div>
<div class="m2"><p>نه شیر ژیان است و نه پیل مست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همی شد به گردون سر خنجرش</p></div>
<div class="m2"><p>همانا که از دیو بُد گوهرش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو آهنگ گردان کشید آن نهنگ</p></div>
<div class="m2"><p>ندیدم به پیشش سپه را درنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من از خشم نزدیک او تاختم</p></div>
<div class="m2"><p>چو دیدمش، جوشن بینداختم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گریزان، وز پس، دمان اژدها</p></div>
<div class="m2"><p>از او کرد یزدان تن من رها</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز گفتار او سلم خیره بماند</p></div>
<div class="m2"><p>همی گفت کاین داستان کس نراند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین گفت قارن که او کوش بود</p></div>
<div class="m2"><p>کز او چین همه ساله پُر جوش بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نبیره ی جهانگیر بیوَرْسْب شاه</p></div>
<div class="m2"><p>پناه دلیران و پشت سپاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر او آن چنان شیرفش نیستی</p></div>
<div class="m2"><p>از او شاه ما کینه کش نیستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من او را به مردی ندیدم همال</p></div>
<div class="m2"><p>بدان برز و بالا و آن شاخ و یال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنانش ربایم ز زین خدنگ</p></div>
<div class="m2"><p>کجا باز تیهو رباید به چنگ</p></div></div>