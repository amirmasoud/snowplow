---
title: >-
    بخش ۳۰۹ - نامه فرستادن سلم به فریدون برای جنگ با کوش
---
# بخش ۳۰۹ - نامه فرستادن سلم به فریدون برای جنگ با کوش

<div class="b" id="bn1"><div class="m1"><p>چو سلم اندر آن مرز خود کامه شد</p></div>
<div class="m2"><p>به نزد فریدون یکی نامه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که روم از همه رویها گشت راست</p></div>
<div class="m2"><p>به کام شهنشاه شد هرچه خواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از بهر رفتن به پیگار کوش</p></div>
<div class="m2"><p>به قارن بسی داشتم چشم و گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر آید وگرنه من با سپاه</p></div>
<div class="m2"><p>کشیدم به پیگار آن کینه خواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریدون از آن نامه شد تافته</p></div>
<div class="m2"><p>بدانست شاه خرد یافته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که در رزم با کوش، سلم دلیر</p></div>
<div class="m2"><p>چو آهو بود زیر چنگال شیر</p></div></div>