---
title: >-
    بخش ۲۴۰ - گریختن کوش و پناه بردن به شهر خمدان
---
# بخش ۲۴۰ - گریختن کوش و پناه بردن به شهر خمدان

<div class="b" id="bn1"><div class="m1"><p>شهنشاه چین با سپاهش نژند</p></div>
<div class="m2"><p>فرود آمد از پشت اسب سمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سران سپه را همه کشته دید</p></div>
<div class="m2"><p>سپاه تبت پاک برگشته دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکسته دلِ لشکر خویشتن</p></div>
<div class="m2"><p>ز بس کُشته و خسته زآن انجمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلیری و نیروی ایرانیان</p></div>
<div class="m2"><p>شده هر سواری چو شیر ژیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از بازگشتن ندید ایچ رای</p></div>
<div class="m2"><p>بماندند خرگاه و خیمه به جای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپه برگرفت و بُنه برنهاد</p></div>
<div class="m2"><p>همی رفت تا شهر خمدان چو باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو روز آمد و قارن آگاه شد</p></div>
<div class="m2"><p>به لشکرگه کوشِ بدخواه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بفرمود تا از پسش سی هزار</p></div>
<div class="m2"><p>برفتند از ایران گزیده سوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپه رفت و کرد آن نبرده درنگ</p></div>
<div class="m2"><p>ببخشید ایشان که بودند به جنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خرگاه وز خیمه و چارپای</p></div>
<div class="m2"><p>که ماندند گردان چینی به جای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوم روز بازآمدند آن سپاه</p></div>
<div class="m2"><p>گرفته فراوان اسیران به راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز کوش آگهی داد خسرو پرست</p></div>
<div class="m2"><p>که در شهر خمدان شد و در ببست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهدار از دادگر یاد کرد</p></div>
<div class="m2"><p>اسیران چین را تن آزاد کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی را بفرمود کشتن ز کین</p></div>
<div class="m2"><p>همه بازگشتند شادان به چین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به روز چهارم سپه برگرفت</p></div>
<div class="m2"><p>در و دشت و کهسار لشکر گرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به فرسنگ خمدان فرود آمدند</p></div>
<div class="m2"><p>در آن بیشه و دشت و رود آمدند</p></div></div>