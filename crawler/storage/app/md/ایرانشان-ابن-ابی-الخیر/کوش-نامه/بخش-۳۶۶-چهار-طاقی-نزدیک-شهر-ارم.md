---
title: >-
    بخش ۳۶۶ - چهار طاقی نزدیک شهر ارم
---
# بخش ۳۶۶ - چهار طاقی نزدیک شهر ارم

<div class="b" id="bn1"><div class="m1"><p>یکی چار طاقی بنزدیک شهر</p></div>
<div class="m2"><p>شده زهرِ آن شهر از او پادزهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآورده دیوارش از زرّ پاک</p></div>
<div class="m2"><p>نه چوب و نه سنگ و نه خشت و نه لاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زر بر نهاده بر او چار در</p></div>
<div class="m2"><p>دو در سوی خاور دو را باختر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوم بر جنوب و دگر بر شمال</p></div>
<div class="m2"><p>سه در بسته دارند از آن ماه و سال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهارم در آنک ارشوی شهریاز</p></div>
<div class="m2"><p>گشاده همه ساله و کرده باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگهبان نشاند بر او مرد چند</p></div>
<div class="m2"><p>ز بیم تباهی و بیم گزند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خواهند باد و هوای خنک</p></div>
<div class="m2"><p>در خاوران برگشاید سبک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگهبان چو بگشاد هم در زمان</p></div>
<div class="m2"><p>شود باد و سرما از آن در دمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهنگام میوه که گرمای گرم</p></div>
<div class="m2"><p>بود در خور کشور و باد نرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در باختر برگشایند باز</p></div>
<div class="m2"><p>رسد میوه و خوردنیها فراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گاه بهاران که باران و آب</p></div>
<div class="m2"><p>بود درخور، آن در که بر آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشایند تا ابر همی زآسمان</p></div>
<div class="m2"><p>ببندد، ببارد هم اندر زمان</p></div></div>