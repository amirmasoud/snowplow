---
title: >-
    بخش ۹۱ - آتبین و طیهور در نخچیرگاه
---
# بخش ۹۱ - آتبین و طیهور در نخچیرگاه

<div class="b" id="bn1"><div class="m1"><p>چو ده روز بگذشت طیهور شاه</p></div>
<div class="m2"><p>به پرسش سوی آتبین شد ز گاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دیدار او آتبین گشت شاد</p></div>
<div class="m2"><p>نشستند و کردند هرگونه یاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز آن روزگاران که اندر گذشت</p></div>
<div class="m2"><p>بگفت آتبین این همه سرگذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دژم گشت طیهور از گفت شاه</p></div>
<div class="m2"><p>همی تنگدل شد در آن جایگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو گفت شاهنشه ای شهریار</p></div>
<div class="m2"><p>تویی تنگدل زین بد روزگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر رای داری به نخچیر و دشت</p></div>
<div class="m2"><p>بر این شت فردا بباید گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز گفتار او شاد شد آتبین</p></div>
<div class="m2"><p>بخندید و بر شاه کرد آفرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر دید طیهور بخت سپاه</p></div>
<div class="m2"><p>بر اسبان نشستند و آمد به راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان آتبین با سواران خویش</p></div>
<div class="m2"><p>دلیران و خنجر گزاران خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برفتند با یوز و شاهین و باز</p></div>
<div class="m2"><p>فراوان شکاری سگ تیز تاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به میدان به پیش سپاه آمدند</p></div>
<div class="m2"><p>چو نزدیک طیهور شاه آمدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه سگ بود با او نه یوز و نه باز</p></div>
<div class="m2"><p>فروماند از آن خسرو سرفراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر بود کار و گمانی دگر</p></div>
<div class="m2"><p>که پیش از سپه رفته باشد مگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو رفتند نزدیک نخچیرگاه</p></div>
<div class="m2"><p>شکار اندر آید ز هر سو به راه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکاری نه آهو نه غرم و نه گور</p></div>
<div class="m2"><p>سپه دید بر یک برآورده نور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلیران ایران ز یوز و ز سگ</p></div>
<div class="m2"><p>فرس باز کردند و شد تیز تگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندیدند در دشت گرد شکار</p></div>
<div class="m2"><p>خجل بازگشتند و دلخسته خوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زمانی دگر بود طیهور شاه</p></div>
<div class="m2"><p>گرازان همی آمد از پس سپاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن هر سواری دو بربسته بود</p></div>
<div class="m2"><p>چه کشته چه از تیرشان خسته بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خجل گشت خسرو چو زآن سان بدید</p></div>
<div class="m2"><p>در اسبانشان تیزتر بنگرید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو دست و دو پای از ستوران فزون</p></div>
<div class="m2"><p>درازی گردن بسان هیون</p></div></div>