---
title: >-
    بخش ۲۱۲ - پیروزی قباد بر کوش
---
# بخش ۲۱۲ - پیروزی قباد بر کوش

<div class="b" id="bn1"><div class="m1"><p>سپیده دمان رزم را ساز کرد</p></div>
<div class="m2"><p>تبیره خروشیدن آغاز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مرد جنگی برآمد ز جای</p></div>
<div class="m2"><p>از آواز شیپور و هندی درای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیران چین برکشیدند صف</p></div>
<div class="m2"><p>ز کینه به لبها برآورده کف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین گفت با ویژگانش قباد</p></div>
<div class="m2"><p>که امروز تیز آمد این دیوزاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب همی دوش خوردم دریغ</p></div>
<div class="m2"><p>که گر باره کُشته نگشتی به تیغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگشتی ز من پیل دندان رها</p></div>
<div class="m2"><p>وگر خویشتن ساختی اژدها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر امروز پیش آیدم در نبرد</p></div>
<div class="m2"><p>سر پیل چهرش در آرم به گرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز گفتار او شادمان شد سپاه</p></div>
<div class="m2"><p>خروش تبیره برآمد به ماه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفتند نیزه سواران جنگ</p></div>
<div class="m2"><p>کشیدند شمشیر الماس رنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکایک همی پیش صف تاختند</p></div>
<div class="m2"><p>همه نیزه و تیغ کین آختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی دام نو ساخت دارای چین</p></div>
<div class="m2"><p>مگر چیره گردد به هنگام کین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آن کس که بود از سپه زورمند</p></div>
<div class="m2"><p>به مردی شده نام ایشان بلند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستادشان پیش دشمن به جنگ</p></div>
<div class="m2"><p>گرفته همه تیغ و زوبین به چنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بکوشید و مردی نمایید، گفت</p></div>
<div class="m2"><p>که با دشمنان بخت بد باد جفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو دشمن گمانی برد کاین سپاه</p></div>
<div class="m2"><p>که با من بمانده ست در قلبگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه سرکشانند و مردان کین</p></div>
<div class="m2"><p>نبهره سپاهش چو هست این چنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که هر یک چو دریا بجوشد همی</p></div>
<div class="m2"><p>چو پیل دمنده بکوشد همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر آن سپاه اندر آید به جنگ</p></div>
<div class="m2"><p>همه نام ایران شود زیر ننگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس آن گه چنان حمله آرم درشت</p></div>
<div class="m2"><p>که ایرانیان را ببینیم پشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به یک حمله از جایشان برکنم</p></div>
<div class="m2"><p>دل و پشت سالارشان بشکنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپهبد چو زآن سوی، صف کرد راست</p></div>
<div class="m2"><p>سپه ده هزاران دلیران بخواست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی بود در قلب با آن سپاه</p></div>
<div class="m2"><p>سپاه دگر شد سوی رزمگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پراگنده رزمی همی ساختند</p></div>
<div class="m2"><p>دلیران ز هر سوی همی تاختند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهی برشکستندشان چینیان</p></div>
<div class="m2"><p>گهی چینیان را رسیدی زیان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گه ایرانیان چیرگی یافتند</p></div>
<div class="m2"><p>گه از چینیان روی برتافتند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هوا تیره همچون شب تار بود</p></div>
<div class="m2"><p>چکاچاک شمشیر خونخوار بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز نیزه نیستان شده روی دشت</p></div>
<div class="m2"><p>ز خون دشت گفتی همه لاله گشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو خورشید بر نیمه ی روز شد</p></div>
<div class="m2"><p>بر ایرانیان کوش پیروز شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو دید آن که شد لشکرش چیره دست</p></div>
<div class="m2"><p>یکی حمله آورد چون پیل مست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نهان اندر آمد میان سپاه</p></div>
<div class="m2"><p>گروهی پس پشت او کینه خواه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو آتش بر ایرانیان زد درشت</p></div>
<div class="m2"><p>به شمشیر و نیزه فراوان بکشت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپه را چو از یوز و آهو بره</p></div>
<div class="m2"><p>به یک حمله بر قلب زد یکسره</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قباد دلاور چو دید آن چنان</p></div>
<div class="m2"><p>برون زد ز قلب سپاه آن عنان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پی پشت او نامور ده هزار</p></div>
<div class="m2"><p>زره دار و برگستوانور سوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برافگند بر چینیان خویشتن</p></div>
<div class="m2"><p>نه شمشیر زن ماند و نه نیزه زن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ببارید شمشیر بر خود و ترگ</p></div>
<div class="m2"><p>چو از میغ بارد بهاران تگرگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شکسته دلان چون خروش یلی</p></div>
<div class="m2"><p>بدیدند با خنجر کابلی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بتندی همه باز پس تاختند</p></div>
<div class="m2"><p>همه نیزه و تیغ کین آختند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی ریخت پولاد زهر آبدار</p></div>
<div class="m2"><p>چو برگ درخت و سر زین سوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سپه را به لشکرگه اندر فگند</p></div>
<div class="m2"><p>سراپرده ی دشمن از بُن بکند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بانگ چکاچاک گرز گران</p></div>
<div class="m2"><p>زمین شد چو بازار آهنگران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روان گشت بر دشت و در جوی خون</p></div>
<div class="m2"><p>ز کشته زمین چون که بیستون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدان حمله اندر چهاران هزار</p></div>
<div class="m2"><p>بکشتند رزم آزموده سوار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از آن رزم شد کوش خسته دو جای</p></div>
<div class="m2"><p>ولیکن ز مردی بیفشرد پای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو دشمن به لشکرگه خویش دید</p></div>
<div class="m2"><p>تن خویشتن خسته و ریش دید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همی سود دندان بسان گراز</p></div>
<div class="m2"><p>ز کینه همی حمله آورد باز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دلیران چین از پسش همچو باد</p></div>
<div class="m2"><p>خروشان ز اسبان تازی نژاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دو لشکر چنان بر هم آمیختند</p></div>
<div class="m2"><p>که از تن همی خون و خوی ریختند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همه دست و سر بود اگر یال بود</p></div>
<div class="m2"><p>شکسته همه تیغ و گوپال بود</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز برگستوان و ز غوری زره</p></div>
<div class="m2"><p>در و دشت سیمابگون بر کره</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به شمشیر، دارای چین با سپاه</p></div>
<div class="m2"><p>برون کرد دشمن ز تاراجگاه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شب آمد، ابا چینیان گفت کوش</p></div>
<div class="m2"><p>که امروز باد شما گشت نوش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>من امروز و فردا و دشت نبرد</p></div>
<div class="m2"><p>جهان تیره گردانم از باد و گرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بگفت این و اندر سراپرده شد</p></div>
<div class="m2"><p>ز رنج زمانه دل آزرده شد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بزرگان و گردنکشان را بخواند</p></div>
<div class="m2"><p>ز کار زمانه فراوان براند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که امروز بر ما چه آمد گزند</p></div>
<div class="m2"><p>چه خواهیم دیدن ز چرخ بلند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برآشفت بر ما نبهره جهان</p></div>
<div class="m2"><p>ندانم چه دارد همی در نهان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز دشمن سپاهم شکسته دل است</p></div>
<div class="m2"><p>کجا سرکشی بود زیر گل است</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ندانم همی چاره ی کار خویش</p></div>
<div class="m2"><p>شده خیره از بخت هشیار خویش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نبینم جز آن کوه کز روی ژرف</p></div>
<div class="m2"><p>که بارانش برف است و بالاش ژرف</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پس پشت خویش آرمش چندگاه</p></div>
<div class="m2"><p>درنگی شوم تا بیاید سپاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که آن جایگاهی ست سخت استوار</p></div>
<div class="m2"><p>ز یک روی او دارد از کوهسار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>وز این سو که ایرانیانند راه</p></div>
<div class="m2"><p>یکی کنده سازیم پیش سپاه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بباشیم تا لشکر آید ز چین</p></div>
<div class="m2"><p>دلیران ماچین و مکران زمین</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پس آن گه به یک بار پیگار ما</p></div>
<div class="m2"><p>سرآید، برآید همه کار ما</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به جان هر یکی کوشش آریم سخت</p></div>
<div class="m2"><p>مگر باز بنمایدم روی بخت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کنارنگ گفتند کاین است رای</p></div>
<div class="m2"><p>زهی شاه گردنکش رهنمای</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همان شب کشیدند بیل و تبر</p></div>
<div class="m2"><p>دلیران و گردان پرخاشخر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یکی کنده ی سهمگن ساختند</p></div>
<div class="m2"><p>به یک روز و یک شب بپرداختند</p></div></div>