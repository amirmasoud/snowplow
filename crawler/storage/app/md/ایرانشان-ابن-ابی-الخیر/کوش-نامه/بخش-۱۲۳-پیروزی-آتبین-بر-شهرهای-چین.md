---
title: >-
    بخش ۱۲۳ - پیروزی آتبین بر شهرهای چین
---
# بخش ۱۲۳ - پیروزی آتبین بر شهرهای چین

<div class="b" id="bn1"><div class="m1"><p>برآمد دگرباره غلغل ز شهر</p></div>
<div class="m2"><p>که مردم ز سختی همی یافت بهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان مرد پیغام دادند باز</p></div>
<div class="m2"><p>سوی آتبین کای شه سرفراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو دانی که هستیم ما زیردست</p></div>
<div class="m2"><p>به بازار داریم خاست و نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه بیگناهیم از آزار شاه</p></div>
<div class="m2"><p>هم ایدر فزونند از ما سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو ساله ز ما باژ بستان و چیز</p></div>
<div class="m2"><p>وز این بر میفزای شاها، بنیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شهر پر لشکر و جوشن است</p></div>
<div class="m2"><p>گر ایوان شهر است اگر برزن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان دسترس نیست ما را که شاه</p></div>
<div class="m2"><p>همی جوید از ما ز بیم سپاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که این داستان نزد هرکس رواست</p></div>
<div class="m2"><p>که بر چیز خود هر کسی پادشاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بشنید گفتارشان آتبین</p></div>
<div class="m2"><p>پسند آمدش، گشت خشنو بر این</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس آن خواسته بستد و برگرفت</p></div>
<div class="m2"><p>به قصرین و افریقیه رفت تفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آن شهر را بود گنجور کوش</p></div>
<div class="m2"><p>سپه بر سر گنج شد سخت کوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شمشیر بگشاد پس هر دو شهر</p></div>
<div class="m2"><p>وز آن گنجها شاه برداشت بهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بماند اندر آن مرز دو سال و نیم</p></div>
<div class="m2"><p>سپاهش توانگر شد از زرّ و سیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو ساله ز هر جای بستد خراج</p></div>
<div class="m2"><p>به گردون برافراخت یکباره تاج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه خواسته برهیون کردبار</p></div>
<div class="m2"><p>فرستاد یکسر به دریا کنار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صد و بیست کشتی پر از بار کرد</p></div>
<div class="m2"><p>پر از جامه ی چین و دینار کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرستاد سوی جزیره همه</p></div>
<div class="m2"><p>بماندند از آن خیره خیره همه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که گر زر به دریا درون ریختی</p></div>
<div class="m2"><p>یکی کوه زرّین برانگیختی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وگر باز کردی ز هم پرنیان</p></div>
<div class="m2"><p>ز رنگ آسمان را رسیدی زیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وگر برزدی دیبه چین بهم</p></div>
<div class="m2"><p>رسیدی به گاو و به ماهی ستم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزآن خواسته گشت طیهور شاد</p></div>
<div class="m2"><p>بر آن شیردل آفرین کرد یاد</p></div></div>