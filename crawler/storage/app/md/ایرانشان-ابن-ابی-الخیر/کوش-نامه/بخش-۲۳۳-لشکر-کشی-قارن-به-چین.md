---
title: >-
    بخش ۲۳۳ - لشکر کشی قارن به چین
---
# بخش ۲۳۳ - لشکر کشی قارن به چین

<div class="b" id="bn1"><div class="m1"><p>برون آمد از پیش تخت بلند</p></div>
<div class="m2"><p>به هر سو درافگند پویان نوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزرگان شه را بدرگاه خواند</p></div>
<div class="m2"><p>چنان کاندر ایران سواری نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن نامداران گزیده سوار</p></div>
<div class="m2"><p>برآمد گه عرض سیصد هزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یلانی که قارن پسندیده بود</p></div>
<div class="m2"><p>گه رزمشان یک به یک دیده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه سرکش و صفدر و تیغ زن</p></div>
<div class="m2"><p>همه لشکر آرای و لشکرشکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کراساز بد بود، اگر بارگی</p></div>
<div class="m2"><p>ز لشکر برون کرد یکبارگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز پیران بیکار و ناهار و سست</p></div>
<div class="m2"><p>به دیوان نشد نام یک تن درست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از این سان چو کرد آن سپه را گزین</p></div>
<div class="m2"><p>سلیح و درم داد با اسب و زین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آمل سراپرده بیرون کشید</p></div>
<div class="m2"><p>بسی ساز و آرایشی برکشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریدون بیامد بدید آن سپاه</p></div>
<div class="m2"><p>چنان ساز و آرایش و دستگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پسند آمدش سخت و کرد آفرین</p></div>
<div class="m2"><p>بدان نیکدل پهلوان گزین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که پیروز بادی و دور از غمان</p></div>
<div class="m2"><p>شدن تندرست، آمدن شادمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روان کرد لشکر سر ماه مهر</p></div>
<div class="m2"><p>که بنمود ماه نو از چرخ چهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز منزل چو برداشتی پیشرو</p></div>
<div class="m2"><p>سپاهی فرود آمدی، باز نو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی راند بر ساقه بر پهلوان</p></div>
<div class="m2"><p>چو دریای با موج لشکر روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو گفتی مگر گشت جنبان زمین</p></div>
<div class="m2"><p>وگر کوه و هامون شده ست آهنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو در مرز توران نهادند پی</p></div>
<div class="m2"><p>فتادند مانند آتش به نی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به تاراج و کشتن کشیدند دست</p></div>
<div class="m2"><p>ز شمشیرشان بی زیان کس نرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گروهی شدند از بد اندر حصار</p></div>
<div class="m2"><p>ندادی از آن پس به جان زینهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسی کاو هوای فریدون نمود</p></div>
<div class="m2"><p>سپهبد بدو مهربانی نمود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>.................................</p></div>
<div class="m2"><p>.................................</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>علف ساخت و آمد بر پهلوان</p></div>
<div class="m2"><p>خلیده دل از رنج کوش و نوان</p></div></div>