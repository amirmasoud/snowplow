---
title: >-
    بخش ۶۵ - در لشکرگاه ایرانیان و چینیان
---
# بخش ۶۵ - در لشکرگاه ایرانیان و چینیان

<div class="b" id="bn1"><div class="m1"><p>از آوردگه کوش چون گشت باز</p></div>
<div class="m2"><p>سوی آتبین رفت و بردش نماز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو آتبین زود بر پای خاست</p></div>
<div class="m2"><p>بر آورد و بنشاند بر دست راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفت کامروز با چینیان</p></div>
<div class="m2"><p>چه کردی تو ای زنده پیل دمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین پاسخ آورد کآن شیر مرد</p></div>
<div class="m2"><p>که با من برابر همی رزم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر باره آمد به آوردگاه</p></div>
<div class="m2"><p>مرا خواست زین بیکرانه سپاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه روزه با او بر آویختم</p></div>
<div class="m2"><p>خوی و خاک و خون بر هم آمیختم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه کردن توانستم او را زکار</p></div>
<div class="m2"><p>نه برگاشت روی آن نبرده سوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن پیل دارد دل شیر نر</p></div>
<div class="m2"><p>همی بارد از وی تو گویی هنر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی بانگ زد بر پیم کای سوار</p></div>
<div class="m2"><p>به ناورد فردا بر آرای کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو روز آید، آید چو شیر دژم</p></div>
<div class="m2"><p>بگردیم تا بر که آید ستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر او آتبین آفرین کرد و گفت</p></div>
<div class="m2"><p>که با دشمن تو غمان باد جفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو پشتِ منی و پناهِ سپاه</p></div>
<div class="m2"><p>ستون دلیران و خورشیدِ گاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به تو دیده و کام من روشن است</p></div>
<div class="m2"><p>امیدم ز تیغ تو در جوشن است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان رنج کامروز بر تن نهی</p></div>
<div class="m2"><p>سپاسی بی اندازه بر من نهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر دشمنی کاندر آری ز پای</p></div>
<div class="m2"><p>ز من خواه پاداش و مزد از خدای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز آن روی به مرد با شهریار</p></div>
<div class="m2"><p>چنین گفت شاها به کام است کار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که فرزند توست این یل نامجوی</p></div>
<div class="m2"><p>دگر گرد بیداد و کژّی مپوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی سخت سوگند خواهد همی</p></div>
<div class="m2"><p>ز کین و ز کژّی بکاهد همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا او یکی سخت سوگند داد</p></div>
<div class="m2"><p>که از شاه پیمان ستانی به داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرفت آن زمان دست سالار چین</p></div>
<div class="m2"><p>گوا شد بر او آسمان و زمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که تا زنده ام هیچ نگزایمش</p></div>
<div class="m2"><p>نه بد خواهمش خود، نه فرمایمش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو جان گرامیش دارم مدام</p></div>
<div class="m2"><p>شب و روز با شادکامی و کام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپارم بدو لشکر و گنج و ساز</p></div>
<div class="m2"><p>به رویش نیارم گنه هیچ باز</p></div></div>