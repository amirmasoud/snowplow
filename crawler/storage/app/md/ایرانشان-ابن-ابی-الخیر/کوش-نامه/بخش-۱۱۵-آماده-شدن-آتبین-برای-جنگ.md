---
title: >-
    بخش ۱۱۵ - آماده شدن آتبین برای جنگ
---
# بخش ۱۱۵ - آماده شدن آتبین برای جنگ

<div class="b" id="bn1"><div class="m1"><p>چو بر خواند نامه بدو ترجمان</p></div>
<div class="m2"><p>بخواند آتبین را هم اندر زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن نامه و رازش آگاه کرد</p></div>
<div class="m2"><p>دلش را به کین خواستن راه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفت کاندیشه ی کار کن</p></div>
<div class="m2"><p>خرد را بدین داروی یار کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر کینه ی خویش باز آوری</p></div>
<div class="m2"><p>دل دشمنان در گداز آوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو آتبین گفت کای سرفراز</p></div>
<div class="m2"><p>توان رزم جستن به مردان و ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا گر تو نیرو دهی نیست باک</p></div>
<div class="m2"><p>به پیشم چه چینی چه یک مشت خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آن بدگهر نیست اندر سپاه</p></div>
<div class="m2"><p>نیابد سواری سوی خانه راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر سی هزارند وگر صد هزار</p></div>
<div class="m2"><p>به شمشیر از ایشان برآرم دمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت گنج و سپه پیش توست</p></div>
<div class="m2"><p>که گنج و سپه داروی ریش توست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دستور فرمود تا هرچه خواست</p></div>
<div class="m2"><p>ز گنج و ز ساز سپه کرد راست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهانجوی هشتاد کشتی بخواست</p></div>
<div class="m2"><p>گزیده سپاهی بدو در نشاخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز گردان کوهی دو ره ده هزار</p></div>
<div class="m2"><p>همه با سلیح از درِ کارزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زن و بچه و گنجش آن جا بماند</p></div>
<div class="m2"><p>به روز همایون سپه را براند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دستور ماچین بسی چیز داد</p></div>
<div class="m2"><p>ز دینار و اسبان تازی نژاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرستادش از پیش تا پیش شاه</p></div>
<div class="m2"><p>برد آگهی کاینک آمد سپاه</p></div></div>