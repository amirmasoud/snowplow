---
title: >-
    بخش ۳۴۵ - فریب دادن کوش، کاووس شاه را
---
# بخش ۳۴۵ - فریب دادن کوش، کاووس شاه را

<div class="b" id="bn1"><div class="m1"><p>همی تاخت تا پیش کاووس شاه</p></div>
<div class="m2"><p>ببردندش و برگشادند راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببوسید پس پایه ی تخت اوی</p></div>
<div class="m2"><p>بسی آفرین خواند بربخت اوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شاهانش بستود و بردش نماز</p></div>
<div class="m2"><p>همی گفت کای خسرو سرفراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چهر تو اندر فلک ماه نیست</p></div>
<div class="m2"><p>به فرّ تو اندر زمین شاه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جایی تو را رهنمونی کنم</p></div>
<div class="m2"><p>که در گنج و گاهت فزونی کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه سنگ او زمرد و لعل پاک</p></div>
<div class="m2"><p>بجای گیا زرّ روید ز خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه گرماش گرم و نه سرماش سرد</p></div>
<div class="m2"><p>شده زآن هوا مردم ایمن ز درد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس از زمرد و لعل صد پاره بیش</p></div>
<div class="m2"><p>برون کرد و بر تخت او ریخت پیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که یک مُهره زآن گوهر وز آن نشان</p></div>
<div class="m2"><p>ندیدند شاهان و گردنکشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آن دید کاووسِ کی خیره ماند</p></div>
<div class="m2"><p>وزآن روشنی چشم او تیره ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی گفت با دل کز این سرزمین</p></div>
<div class="m2"><p>که زرّش گیا باشد و سنگ این</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا دید باید به دیده بسی</p></div>
<div class="m2"><p>که دل برگشایم برآن اندکی</p></div></div>