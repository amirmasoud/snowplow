---
title: >-
    بخش ۳۱۲ - جاسوس فرستادن کوش به لشکرگاه ایرانیان، و پیغام قارن به کوش
---
# بخش ۳۱۲ - جاسوس فرستادن کوش به لشکرگاه ایرانیان، و پیغام قارن به کوش

<div class="b" id="bn1"><div class="m1"><p>یکی مرد پوینده را همچو دود</p></div>
<div class="m2"><p>فرستاد کآگاهی آردش زود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بداند که لشکر چه مایه گذشت</p></div>
<div class="m2"><p>کدام است کآمد بر این روی دشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتند و بردند او را کشان</p></div>
<div class="m2"><p>که از ترس بر چهره بودش نشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو پهلوان گفت کای خیره هوش</p></div>
<div class="m2"><p>به چه کار رفتی تو از پیش کوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی گفت و خستو نیامد به راز</p></div>
<div class="m2"><p>چنین گفت کای خسرو سرفراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندانم من این را که بردی تو نام</p></div>
<div class="m2"><p>نه هرگز شنیدم که او خود کدام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی مرد بیگانه ام کارجوی</p></div>
<div class="m2"><p>ز من کار خواه و فزونی مجوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخندید قارن، بدو گفت رو</p></div>
<div class="m2"><p>به نزدیک آن بدگهر باز شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه ما از نهانی به جنگ آمدیم</p></div>
<div class="m2"><p>که با لشکری تیز چنگ آمدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگویش که ای تیره دیو نژند</p></div>
<div class="m2"><p>که گردون بیاراد بر تو گزند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بجای تو آن نیکویهای شاه</p></div>
<div class="m2"><p>چرا خیره بایست کردن تباه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان خسته بودی به زندان و بند</p></div>
<div class="m2"><p>که بگریستی بر تو هر مستمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی مرغ بودی تو بی بال و پر</p></div>
<div class="m2"><p>بگسترد بر تو شهنشاه فر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیاوردت آن شاه آزاد مرد</p></div>
<div class="m2"><p>جهانی چنین زیر دست تو کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون چون برآورد بخت تو بال</p></div>
<div class="m2"><p>شدی شاه فرخنده را بدسگال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدین بار اگر زنده یابم تو را</p></div>
<div class="m2"><p>سر از تاجداری بتابم تو را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سزای تو با تو میان سپاه</p></div>
<div class="m2"><p>بگویم، فرستم از آن پس به شاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدان تا دگر بندگان بیش از این</p></div>
<div class="m2"><p>نتابند روی و نجویند کین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همانا که کار تو آمد به تنگ</p></div>
<div class="m2"><p>از این پس به گیتی نیابی درنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کز ایران، وز ترک و تازی گوان</p></div>
<div class="m2"><p>ز سقلاب، وز روم، وز هندوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه گرزداران پرخاشجوی</p></div>
<div class="m2"><p>به کینه نهادند سوی تو روی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدانی چو آییم هر دو بهم</p></div>
<div class="m2"><p>که من داد گفتم، نگویم ستم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بفرمود تا دست از آن مرد نیز</p></div>
<div class="m2"><p>بدارند و برداشت راه گریز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شتابان بیامد بنزدیک کوش</p></div>
<div class="m2"><p>ز تن رفته جان و ز دل رفته هوش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکایک چو پیغام قارن بگفت</p></div>
<div class="m2"><p>به پیش بزرگان نه ایدر نهفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن نامداران لشکر پناه</p></div>
<div class="m2"><p>خجل گشت و پرسید از آن مرد راه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که لشکر چه مایه گذشته ست از آب</p></div>
<div class="m2"><p>سپه با درنگ است اگر با شتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو گفت یک بهره بگذشت پیش</p></div>
<div class="m2"><p>ز لشکر همه دشت بترست کیش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه شب سپه زیر خفتان و ترگ</p></div>
<div class="m2"><p>همه ساخته، دل نهاده به مرگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدان سان همی از تو لرزید سخت</p></div>
<div class="m2"><p>که از باد نیسان بلرزد درخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدان روی مانده دو بهره سپاه</p></div>
<div class="m2"><p>سراسر بنه هست نزدیک شاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدان نامداران چنین گفت کوش</p></div>
<div class="m2"><p>که قارن سواری ست با فرّ و هوش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگرنه وی استی شبان رمه</p></div>
<div class="m2"><p>ز دریا گذر کرده بودی همه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم امشب یکی تاختن کردمی</p></div>
<div class="m2"><p>دمار از دلیران برآوردمی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ولیکن چه سود است کآن بدنژاد</p></div>
<div class="m2"><p>دلیر است، با رای و با فرّ و داد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همان گه طلایه برون کرد زود</p></div>
<div class="m2"><p>که از قارن و مکرش ایمن نبود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دو ماه بگذاشت دریا سپاه</p></div>
<div class="m2"><p>سر ماه بگذشت بر ساقه شاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز کارآگهان گربزی برگزید</p></div>
<div class="m2"><p>برفت و سپه را یکایک بدید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بیامد بر سلم و گفت آن سپاه</p></div>
<div class="m2"><p>فزون است از این ژرفتر کن نگاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو گویی که مردان پولادپوش</p></div>
<div class="m2"><p>ز روی زمین گرد کرده ست کوش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همان است گویی به گیتی سپاه</p></div>
<div class="m2"><p>که من یافتم پیش آن کینه خواه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وزآن روی مردان بیامد نخست</p></div>
<div class="m2"><p>همه ره ز ایران سپه باز جست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو آگاه شد بازگشت او به جای</p></div>
<div class="m2"><p>چنین گفت کای شاه فرخنده رای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دو چندان که دشمن، تو را لشکر است</p></div>
<div class="m2"><p>بدین رزم گردون تو را چاکر است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز گفتار او شادمان گشت کوش</p></div>
<div class="m2"><p>غو کوس برخاست و بانگ و خروش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همی راند بر مرز دریا سپاه</p></div>
<div class="m2"><p>شده روی دریا چو گرد سیاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>میان دو لشکر چو دو میل ماند</p></div>
<div class="m2"><p>جهان در کف گرد چون نیل ماند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز دیده بشد خواب و ز دل شکیب</p></div>
<div class="m2"><p>نهیب آمد و بود جای نهیب</p></div></div>