---
title: >-
    بخش ۱۷ - فرستادن مانوش نه پاره کتاب پیش کوش
---
# بخش ۱۷ - فرستادن مانوش نه پاره کتاب پیش کوش

<div class="b" id="bn1"><div class="m1"><p>ز کردار شه گشت مانوش شاد</p></div>
<div class="m2"><p>به دستور خویش این سخن برگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرستاد نُه پاره دفتر به شاه</p></div>
<div class="m2"><p>که هر یک سوی دانشی برد راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دریای دانش مر آن هر یکی</p></div>
<div class="m2"><p>فزون آمدی هر یکی بر یکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن نُه، دو اندر پزشکی نمود</p></div>
<div class="m2"><p>که اندر جهان آن پزشکی نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی انکست عرش را بود نام</p></div>
<div class="m2"><p>که دانا ز خواندن شود شادکام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر علم بقراط اندر فصول</p></div>
<div class="m2"><p>که مردم ز خواندن نگردد ملول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهارم کتابی که خوانی علل</p></div>
<div class="m2"><p>که آن را نیابی به گیتی بدل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بَلیناس گفته ست و گفتار اوی</p></div>
<div class="m2"><p>همی جان فزاید به دیدار اوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز اخبار شاهان فرستاد پنج</p></div>
<div class="m2"><p>که هر کس ز گیتی چه دیده ست رنج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه داستانهای شاهان روم</p></div>
<div class="m2"><p>ز کردارشان اندر این مرز و بوم</p></div></div>