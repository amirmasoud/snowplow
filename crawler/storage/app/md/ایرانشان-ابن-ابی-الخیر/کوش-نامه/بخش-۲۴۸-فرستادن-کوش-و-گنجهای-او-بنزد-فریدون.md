---
title: >-
    بخش ۲۴۸ - فرستادن کوش و گنجهای او بنزد فریدون
---
# بخش ۲۴۸ - فرستادن کوش و گنجهای او بنزد فریدون

<div class="b" id="bn1"><div class="m1"><p>وز آن جا بشد تا به ایوان کوش</p></div>
<div class="m2"><p>سپاه از پس و پیش پولادپوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرود آمد و گنجها مُهر کرد</p></div>
<div class="m2"><p>نگهبان بر او کرد مردان مرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن شهر ماهی درنگ آمدش</p></div>
<div class="m2"><p>همه گنج شاهان به چنگ آمدش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شتر خواست از در ده و دو هزار</p></div>
<div class="m2"><p>سراسر درآوردشان زیر بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه گوهر و زرّشان بار کرد</p></div>
<div class="m2"><p>همه جامه ی چین و دینار کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تخت و ز تاج و ز طوق و کمر</p></div>
<div class="m2"><p>برآراسته کاروانی دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خوبان چینی چهاران هزار</p></div>
<div class="m2"><p>که با یاره بودند و با گوشوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ایوان کوش آمدندی برون</p></div>
<div class="m2"><p>زجان ار فریب و لبان از فسون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان ده هزاران غلامان خرد</p></div>
<div class="m2"><p>که هر یک زخورشید خوبی ببرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنان شبستان همه با خروش</p></div>
<div class="m2"><p>به ایران فرستاد همراه کوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دست تلیمان فرخ نژاد</p></div>
<div class="m2"><p>سپه سی هزار از یلانش بداد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدین آگهی نیز نامه نبشت</p></div>
<div class="m2"><p>به نزد فریدون، چراغ بهشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه سرگذشت اندر او یاد کرد</p></div>
<div class="m2"><p>تلیمان روان گشت مانند گرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرآن کاو به صد سال گرد آورید</p></div>
<div class="m2"><p>تلیمان به یکبار زی شه کشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خردمند اگر گیردی پند از این</p></div>
<div class="m2"><p>نکردی نهان گنج، زیر زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر دستگاهی تو داری به دست</p></div>
<div class="m2"><p>که شادان و ایمن توانی نشست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز کوش و فریدون تو را به کام به</p></div>
<div class="m2"><p>وز ایشان بدان سر تو را نام به</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که گنج است خرسندی ای نیکمرد</p></div>
<div class="m2"><p>که آهنگ او هیچ دشمن نکرد</p></div></div>