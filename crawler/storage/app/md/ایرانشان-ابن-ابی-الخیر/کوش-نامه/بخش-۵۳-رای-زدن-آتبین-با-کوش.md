---
title: >-
    بخش ۵۳ - رای زدن آتبین با کوش
---
# بخش ۵۳ - رای زدن آتبین با کوش

<div class="b" id="bn1"><div class="m1"><p>شب تیره گیتی چو یکسان نمود</p></div>
<div class="m2"><p>برفت آتبین با سپاهش چو دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به لشکرگه آمد به نزدیک کوه</p></div>
<div class="m2"><p>بخواند آن زمان کوش را از گروه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بنشست با او در آمد به راز</p></div>
<div class="m2"><p>که هست این سپاهی گران رزمساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگویی مرا تا چه چاره کنم؟</p></div>
<div class="m2"><p>بدین کین بلا را کرانه کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدین ساز و مردان و اسبان جنگ</p></div>
<div class="m2"><p>بترسم که نامم شود زیر ننگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو کوش گفت ای سرافراز شاه</p></div>
<div class="m2"><p>میندیش از این بیکرانه سپاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که ما پشت را سوی کوه آوریم</p></div>
<div class="m2"><p>همه لشکرش را ستوه آوریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر هرچه در روی گیتی سپاه</p></div>
<div class="m2"><p>بیاید، نیابد بدین کوه راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر کآسمانی بود کار ما</p></div>
<div class="m2"><p>بخوابد سربخت بیدار ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو فرمان پدید آمد از آسمان</p></div>
<div class="m2"><p>به کوه و به ماهون سرآید زمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همانا که بر ما شمرده ست دَم</p></div>
<div class="m2"><p>نباشد به یک دم زدن بیش و کم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل آتبین گشت خرسند و خوش</p></div>
<div class="m2"><p>ز گفتار آن شیردل پیرفش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فراوانش بستود و کردش گُسی</p></div>
<div class="m2"><p>سوی خیمه ی خویش شد هر کسی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از اندرز جمشید شاه آتبین</p></div>
<div class="m2"><p>پر اندیشه بودی همه سال از این</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا گفت فرزند خود را به راز</p></div>
<div class="m2"><p>ز دشمن به پرهیز باشید باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان بود باید شما را نهان</p></div>
<div class="m2"><p>که گویند کس نیست اندر جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آرد به روی شما روی بخت</p></div>
<div class="m2"><p>نبیره ی مرا بر نشاند به تخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز ضحاکیان کس نماند بجای</p></div>
<div class="m2"><p>شما را دهد پادشاهی خدای</p></div></div>