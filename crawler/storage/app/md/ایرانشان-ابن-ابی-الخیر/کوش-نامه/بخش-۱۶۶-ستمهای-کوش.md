---
title: >-
    بخش ۱۶۶ - ستمهای کوش
---
# بخش ۱۶۶ - ستمهای کوش

<div class="b" id="bn1"><div class="m1"><p>سراینده دهقان بسیار هوش</p></div>
<div class="m2"><p>چنین یافت از کار و کردار کوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که با مردمان سخت بد باز گشت</p></div>
<div class="m2"><p>ز درگاه ضحاک چون بازگشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی بستد از هر کسی هرچه یافت</p></div>
<div class="m2"><p>به خون گرانمایه مردم شتافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنان را سوی بستر خویش برد</p></div>
<div class="m2"><p>همان کودکان را بر خویش برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه بر خواسته مرد را دسترس</p></div>
<div class="m2"><p>نه ایمن به فرزند و زن ایچ کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بیداد از اندازه اندر گذشت</p></div>
<div class="m2"><p>زمانه ره نیکوی بر نوشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهانی برفتند مردم هزار</p></div>
<div class="m2"><p>به درگاه ضحّاک ناهوشیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنالید هرکس ز بیداد کوش</p></div>
<div class="m2"><p>به گردون برآمد غریو و خروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که زنهار، شاها، تو فریاد رس</p></div>
<div class="m2"><p>ستمها که برماست بس کن تو بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو ضحّاک آگه شد از کار کوش</p></div>
<div class="m2"><p>ز درخورد دیدار و کردار کوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شنیدن همان بود و گفتن همان</p></div>
<div class="m2"><p>دلش شد ز کردار او شادمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا گفت، اگر در همه کشوری</p></div>
<div class="m2"><p>چنو پیشکاری بُدی کهتری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبودی مرا دشمن اندر جهان</p></div>
<div class="m2"><p>نه بر آشکارا نه اندر نهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ستمدیدگان را چنین گفت شاه</p></div>
<div class="m2"><p>که دیگر مباشید فریاد خواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که آن مرز یکسر بدو داده ام</p></div>
<div class="m2"><p>مر او را به شاهی فرستاده ام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کند هرچه خواهد، نترسد ز کس</p></div>
<div class="m2"><p>شما را ز من پاسخ این است و بس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ستمدیده نومید چون بازگشت</p></div>
<div class="m2"><p>از ایشان همه چین پر آواز گشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به کوش آمد این آگهی زآن گروه</p></div>
<div class="m2"><p>ز خمدان برون رفت تا پیش کوه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکایک ز برزن برآوردشان</p></div>
<div class="m2"><p>به شاخ درخت اندر آوردشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شهر اندر افتاد از آن زلزله</p></div>
<div class="m2"><p>دگر کس نیارست کردن گله</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی هر زمان برفزودی ستم</p></div>
<div class="m2"><p>همه زیر دستانش با رنج و غم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمانه نبینی بتر زآن که شاه</p></div>
<div class="m2"><p>همه راه بیداد دارد نگاه</p></div></div>