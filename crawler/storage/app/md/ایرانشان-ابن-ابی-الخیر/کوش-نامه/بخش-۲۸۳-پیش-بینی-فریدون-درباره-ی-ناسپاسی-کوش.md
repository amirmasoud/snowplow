---
title: >-
    بخش ۲۸۳ - پیش بینی فریدون درباره ی ناسپاسی کوش
---
# بخش ۲۸۳ - پیش بینی فریدون درباره ی ناسپاسی کوش

<div class="b" id="bn1"><div class="m1"><p>وزآن پس دبیران و گردان شاه</p></div>
<div class="m2"><p>که با کوش رفتند از آن پیشگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرستاد هر کس همی آگهی</p></div>
<div class="m2"><p>از آن تخت و آن بارگاه مهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کوه کلنگان و دریای ژرف</p></div>
<div class="m2"><p>از آن شهرهای بزرگ و شگرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وزآن گنج پرمایه و کان زر</p></div>
<div class="m2"><p>وزآن لشکر گشن پرخاشخر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریدون به قارن نگه کرد و گفت</p></div>
<div class="m2"><p>که با ناسپاسی شود کوش جفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپیچد همانا دل از راستی</p></div>
<div class="m2"><p>دل آرد به پیمان ما کاستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت قارن که او تاکنون</p></div>
<div class="m2"><p>نیامد ز پیمان خسرو برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندانم کزاین پس چه پیش آورد</p></div>
<div class="m2"><p>مبادا که آیین خویش آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فریدون بدو گفت کاین دیوزاد</p></div>
<div class="m2"><p>ز تخمی ست کآن تخم هرگز مباد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پسر مر پدر را به زخم درشت</p></div>
<div class="m2"><p>ز بهر سرای سپنجی بکشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرشتی درشت و نهادی ست بد</p></div>
<div class="m2"><p>ببیند ز پیش آن که دارد خرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که پیدا کند گوهر خویش مار</p></div>
<div class="m2"><p>گر او را بخوابانی اندر کنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر روغن گُل به آتش دهی</p></div>
<div class="m2"><p>بسوزد چو انگشت بر وی نهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگر بچّه ی گرگ داری به ناز</p></div>
<div class="m2"><p>شود هم بدان گوهر خویش باز</p></div></div>