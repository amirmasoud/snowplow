---
title: >-
    بخش ۲۰۱ - نامه فرستادن نستوه به نزد کوش پیل دندان
---
# بخش ۲۰۱ - نامه فرستادن نستوه به نزد کوش پیل دندان

<div class="b" id="bn1"><div class="m1"><p>یکی نامه فرمود نزدیک کوش</p></div>
<div class="m2"><p>همه داوری و همه جنگ و جوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دانم که آگاه گشتی ز کار</p></div>
<div class="m2"><p>که چون بست ضحاک را شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شنیدی که با دیوسازان چه کرد</p></div>
<div class="m2"><p>کز آن تخمه یکسر برآورد گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بندی که آن مارفش بسته شد</p></div>
<div class="m2"><p>جهان از بد جاودان رسته شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن تخمه جز تو نمانده ست کس</p></div>
<div class="m2"><p>تو را نیز مرگ آمد از پیش و پس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آیین تو چون شد آگاه شاه</p></div>
<div class="m2"><p>وز این رنج بر زیردست و سپاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدانم فرستاد تا هم زمان</p></div>
<div class="m2"><p>بلای تو بردارم از مردمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون گر به فرمان شاه اندری</p></div>
<div class="m2"><p>مرا با تو کوته شود داوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان بندگی را ببند و مپای</p></div>
<div class="m2"><p>یکی با سواران خویش ایدر آی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ایدر بیایی، شوی نزد شاه</p></div>
<div class="m2"><p>بگو تا دهم نزد ایرانت راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که بخشایش شاه از آن برتر است</p></div>
<div class="m2"><p>فراوان چو تو چاکرش بر در است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدارد تو را نیز چون دیگران</p></div>
<div class="m2"><p>یکی مهتری باشی از مهتران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که شاه همایون چو ضحّاک نیست</p></div>
<div class="m2"><p>ستمکاره خونریز و ناباک نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آمرزش آری ببخشایدت</p></div>
<div class="m2"><p>از آن پس چنان کن که نگزایدت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنم نامه نزدیک شاه زمین</p></div>
<div class="m2"><p>که چون ما رسیدیم در مرز چین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز فرمان تو کوش گردن نتافت</p></div>
<div class="m2"><p>یله کرد شهر و به درگه شتافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیامرزدت بی گمان شهریار</p></div>
<div class="m2"><p>که بخشنده شاهی ست و آمرزگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر خود نخواهی که آیی به پیش</p></div>
<div class="m2"><p>بترسی همی خیره بر جان خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به راهی برون رفت باید نهان</p></div>
<div class="m2"><p>گرفتن یکی گوشه ای زین جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برو، پیش از آن کاین گزیده سپاه</p></div>
<div class="m2"><p>بیایند و بر تو ببندند راه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وز آن پس من از بیم شاه جهان</p></div>
<div class="m2"><p>رها کی کنم تا که گردی نهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه بر پوزش آن را توانی بشست</p></div>
<div class="m2"><p>نه گر چاره ای جویی آید بدست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پشیمانی آن گه نداردت سود</p></div>
<div class="m2"><p>همان به که دریابی این کار زود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نویسنده از نامه چون گشت سیر</p></div>
<div class="m2"><p>سخنگوی مردی جوان و دلیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گزین کرد و نامه بدو داد و گفت</p></div>
<div class="m2"><p>که با باد باید که باشی تو جفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شتابان فرستاده ی تیزهوش</p></div>
<div class="m2"><p>برفت و رسانید نامه به کوش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سخنها بر او ترجمان چون بخواند</p></div>
<div class="m2"><p>ز گفتار نستوه خیره بماند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برآشفت و آن نامه اندر ربود</p></div>
<div class="m2"><p>بدرّید و بسیار تندی نمود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرستاده را گفت کای بدنژاد</p></div>
<div class="m2"><p>سر خویشتن داده بودی بباد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرا گر نبودی چنان آرزوی</p></div>
<div class="m2"><p>که تو پاسخ من رسانی بدوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سرت بی گمان دور گشتی ز تن</p></div>
<div class="m2"><p>هم امروز در پیش این انجمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فرستاده گفت ای خردمند شاه</p></div>
<div class="m2"><p>مرا گر کنی بی گناهی تباه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه ترسد فریدون از این زشت کار</p></div>
<div class="m2"><p>نه نستوه برگردد از کارزار</p></div></div>