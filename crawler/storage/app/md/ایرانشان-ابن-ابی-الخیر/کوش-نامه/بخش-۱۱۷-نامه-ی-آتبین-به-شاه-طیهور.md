---
title: >-
    بخش ۱۱۷ - نامه ی آتبین به شاه طیهور
---
# بخش ۱۱۷ - نامه ی آتبین به شاه طیهور

<div class="b" id="bn1"><div class="m1"><p>نویسنده را نامه فرمود و گفت</p></div>
<div class="m2"><p>که با نیکمردان هنر باد جفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین نامه، شاها تو رامش پذیر</p></div>
<div class="m2"><p>که بر ما دگر گشت گردون پیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فگنده همه دشت پر دشمن است</p></div>
<div class="m2"><p>لب ژرف دریا سر بی تن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی حمله آورد سالارِ کوش</p></div>
<div class="m2"><p>که از نامداران رمانید هوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزد خشت سوزنده بر جوشنم</p></div>
<div class="m2"><p>از آن بد نگهداشت یزدان تنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرش زخم کردم به یک زخم گرز</p></div>
<div class="m2"><p>رمید از سپهدار یکباره برز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دیهیم چون روی برگاشت بخت</p></div>
<div class="m2"><p>سپاهش چو ریزنده برگ درخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پشت تگاور همی ریختند</p></div>
<div class="m2"><p>نبودند صد تن که بگریختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه کشته گشتند در کارزار</p></div>
<div class="m2"><p>دگر خسته و بسته بیچاره زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپه یافت چندان فزونی و ساز</p></div>
<div class="m2"><p>که هرکس شد از خواسته بی نیاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از این مایه ور گنج و بار و بنه</p></div>
<div class="m2"><p>چو سالار پرمایه شد یک تنه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی بهره از هرچه در خورد شاه</p></div>
<div class="m2"><p>فرستادم اینک بدان بارگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دریا من آهنگ کردم به چین</p></div>
<div class="m2"><p>ببینم که چون است چندان زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی بر گرایم یلان را به جنگ</p></div>
<div class="m2"><p>ببینم که پیشم که دارد درنگ</p></div></div>