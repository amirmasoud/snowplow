---
title: >-
    بخش ۶۹ - نامه ی شاه چین به ضحاک در کشتن پسر آتبین
---
# بخش ۶۹ - نامه ی شاه چین به ضحاک در کشتن پسر آتبین

<div class="b" id="bn1"><div class="m1"><p>همان گه نویسنده را پیش خواند</p></div>
<div class="m2"><p>وزاین داستان چند با او براند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بر پرنیان بر گذر کرد نی</p></div>
<div class="m2"><p>یکی مژده را شادی افگند پی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که شاه جهان تا جهان شاه باد</p></div>
<div class="m2"><p>از او دست بدخواه کوتاه باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمرگاه او سخت باد و تنش</p></div>
<div class="m2"><p>همه ساله پیروز بر دشمنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان، شهریارا، که این روزگار</p></div>
<div class="m2"><p>شگفتی فراوان نهد در کنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرامی یکی پور من دور شد</p></div>
<div class="m2"><p>دل من ز دوریش رنجور شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کامم در آمد سرِ شست او</p></div>
<div class="m2"><p>سپاهم شکسته شد از دست او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی تاخت بر دشمنان چون همای</p></div>
<div class="m2"><p>سپاهم همی برد هر یک ز جای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی نامور پور ما را بکشت</p></div>
<div class="m2"><p>به زوبین پولاد و زخم درشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمانه کنون روشنایی نمود</p></div>
<div class="m2"><p>مرا با پسر آشنایی نمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به من باز بخشیدش این روزگار</p></div>
<div class="m2"><p>به کام جهاندار گشته ست کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از این ناسپاسان همه کین کشید</p></div>
<div class="m2"><p>سر دشمن شاه گیتی بُرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستادم اینک برِ شهریار</p></div>
<div class="m2"><p>بریده سر نامبرده سُوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان شد ستوه آتبین با گروه</p></div>
<div class="m2"><p>که بیرون نیامد شد از پیش کوه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه بیرون توان شد به راهی دگر</p></div>
<div class="m2"><p>نه فریادش آید سپاهی دگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن پس به فرّ جهان کدخدای</p></div>
<div class="m2"><p>من آن کوه را اندر آرم ز پای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرستم به درگاه شاهش اسیر</p></div>
<div class="m2"><p>به دست گرامی یل شیر گیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گزین کرد صد مرد را زآن سپاه</p></div>
<div class="m2"><p>گزینان رزم و دلیران گاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدادند یک ساله شان برگ و ساز</p></div>
<div class="m2"><p>همان گه گرفتند راه دراز</p></div></div>