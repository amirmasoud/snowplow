---
title: >-
    بخش ۴۱ - جنگ کوش پیل دندان با چینیان
---
# بخش ۴۱ - جنگ کوش پیل دندان با چینیان

<div class="b" id="bn1"><div class="m1"><p>چو شاه آتبین را چنان دید کوش</p></div>
<div class="m2"><p>برآورد مانند تندر خروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان داشت و ده چوبه تیر خدنگ</p></div>
<div class="m2"><p>نبودش سلیحی جز این خود به چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان دَه بیفگند دَه نامدار</p></div>
<div class="m2"><p>ز چینی دلیران خنجرگزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمی شد ز ترکش چو تیرش نماند</p></div>
<div class="m2"><p>ز لشکر یکی خیره سر را بخواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه بستدش جوشن و خود و ترگ</p></div>
<div class="m2"><p>چو بر ساخت خود را ز بیگانه برگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خروشان به اسب اندر آورد پای</p></div>
<div class="m2"><p>به یک حمله برداشت لشکر ز جای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو گفتی یکی پیل سرمست بود</p></div>
<div class="m2"><p>همه دشت پای و سر و دست بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ایران سپه کوششِ کوش دید</p></div>
<div class="m2"><p>مر او را بدان کین و آن جوش دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه حمله کردند و برداشتند</p></div>
<div class="m2"><p>سپه را و از رود بگذاشتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرندش ز خون بر زمین جوی کرد</p></div>
<div class="m2"><p>سمندش سرسرکشان گوی کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هرکس که گرزش برآمد درشت</p></div>
<div class="m2"><p>همی جان به تن درش بنمود پشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هوا گشته بی جان ز پیکان او</p></div>
<div class="m2"><p>زمین گشته مرجان ز میدان او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آن کس که او را ز ناگه بدید</p></div>
<div class="m2"><p>دلش همچو دیوانه درهم رمید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گریزان همی برد نام خدای</p></div>
<div class="m2"><p>نیامد دلش ماهیان بازجای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آن حمله افزون ز پانصد دلیر</p></div>
<div class="m2"><p>بکشتند و شد آتبین شاه، چیز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن ننگ سالار چین شد درشت</p></div>
<div class="m2"><p>همی جان به تن درش بنمود پشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین گفت کای نامداران جنگ</p></div>
<div class="m2"><p>همی بر تن خویش کردید ننگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از این مایه لشکر چه دارید باک</p></div>
<div class="m2"><p>که بی تیغ شایند گشتن به خاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برامد کنون سال سیصد درست</p></div>
<div class="m2"><p>که این دشمنان را همی شاه جُست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی باز گردید، چون یافتیم</p></div>
<div class="m2"><p>ز چین ما بدین کار بشتافتیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه گویید فردا به نزدیک شاه</p></div>
<div class="m2"><p>اگر بازگردید از ایران سپاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گفتار او لشکر آمد بجوش</p></div>
<div class="m2"><p>برآمد ز کوس و تبیره خروش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کشیدند شمشیر کین از نیام</p></div>
<div class="m2"><p>نه جای درنگ و نه جای پیام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هوا پر شد از جان گردان کین</p></div>
<div class="m2"><p>ز خون گشت سیراب روی زمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تنی چند از ایران سپه کشته شد</p></div>
<div class="m2"><p>سر هر یک از رزم برگشته شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین بود تا شب سپه تاز گشت</p></div>
<div class="m2"><p>پس آن هر دو لشکر ز هم بازگشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرود آمد آن جا سپهدار چین</p></div>
<div class="m2"><p>به سوی سراپرده رفت آتبین</p></div></div>