---
title: >-
    بخش ۳۸ - یافتن آتبین کوش را
---
# بخش ۳۸ - یافتن آتبین کوش را

<div class="b" id="bn1"><div class="m1"><p>دگر روز چون آتبین با سپاه</p></div>
<div class="m2"><p>ز بهر شکار آمد آن جایگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن بیشه آواز کودک شنید</p></div>
<div class="m2"><p>به نزدیک او تاخت، او را بدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرو ماند خیره ز دیدار اوی</p></div>
<div class="m2"><p>روانش پر اندیشه از کار اوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی هر زمان گفت با خویشتن</p></div>
<div class="m2"><p>که این نیست جز بچّه ی اهرمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرستنده را گفت تا برگرفت</p></div>
<div class="m2"><p>به سوی سراپرده ره برگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بینداخت و افگند در پیش سگ</p></div>
<div class="m2"><p>گریزان شد از وی سگ تیزتگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برِ شیر افگند و شیرش نخورد</p></div>
<div class="m2"><p>رخ آتبین گشت از آن هول زرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آتش نهادند و آتش نسوخت</p></div>
<div class="m2"><p>رخ هرکس از خیرگی برفروخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرا پاک دادار دارد نگاه</p></div>
<div class="m2"><p>به شمشیر و آتش نگردد تباه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بفرمود کاو را به در افگنند</p></div>
<div class="m2"><p>وگرنه سرش را ز تن برکنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنش گفت: ای نامور شهریار</p></div>
<div class="m2"><p>ستیزه مکن خیره با کردگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود کاندر این کار، رازی بود</p></div>
<div class="m2"><p>که او در جهان سرفرازی بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به من بخش تا همچو جان دارمش</p></div>
<div class="m2"><p>یکی دایه ی مهربان آرمش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیرزد بدو گفت پروردنش</p></div>
<div class="m2"><p>نبینی چو خوکان سر و گردنش؟</p></div></div>