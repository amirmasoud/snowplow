---
title: >-
    بخش ۲۴۶ - خوار کردن فرستاده ی قارن توسط سپاهیان چینی
---
# بخش ۲۴۶ - خوار کردن فرستاده ی قارن توسط سپاهیان چینی

<div class="b" id="bn1"><div class="m1"><p>چو گفتار آن مرد شیرین سخُن</p></div>
<div class="m2"><p>یکایک شنیدند سر تا به بن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شهر و ز بازار و مردم که بود</p></div>
<div class="m2"><p>سراسر هوای فریدون نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاهی ز فرمان برون برد سر</p></div>
<div class="m2"><p>همه پاسخش تیغ و تیر و تبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به لشکر چنین گفت شهری که شاه</p></div>
<div class="m2"><p>گرفتار گشت و تهی ماند گاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد یکی نام برده پسر</p></div>
<div class="m2"><p>که تاج پدر برنهادی به سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بهر که پیگار و جنگ آوریم</p></div>
<div class="m2"><p>جهان بر دل خویش تنگ آوریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی لشکری را بدادند پند</p></div>
<div class="m2"><p>نیامد همی پندشان سودمند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چخیدن نیارست بازار و شهر</p></div>
<div class="m2"><p>که یک بهر بودند و لشکر دو بهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرستاده را خوار کرد آن سپاه</p></div>
<div class="m2"><p>نکردند گفتار او را نگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سنگ و به دشنام بردند دست</p></div>
<div class="m2"><p>جوان دلاور بجست و نخست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی قارن آمد بگفت آنچه دید</p></div>
<div class="m2"><p>از ایشان دل پهلوان بررمید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سه ماه دگر کرد بر در درنگ</p></div>
<div class="m2"><p>به شهر اندرون خوردنی گشت تنگ</p></div></div>