---
title: >-
    بخش ۲۲۶ - عشق کوش به نگارین و کشتن او
---
# بخش ۲۲۶ - عشق کوش به نگارین و کشتن او

<div class="b" id="bn1"><div class="m1"><p>به خود کامگی شاه بر تخت شد</p></div>
<div class="m2"><p>به کار زمان دلش پردخت شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی هر شبی دختری خوبروی</p></div>
<div class="m2"><p>بدیدی و روزش بدادی به شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر هیچ بودی مر او را پسند</p></div>
<div class="m2"><p>همی در شبستانش کردی به بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن کس کز او بار برداشتی</p></div>
<div class="m2"><p>همی تا نکشتیش نگذاشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زن از بیم تیغ بداندیش شاه</p></div>
<div class="m2"><p>همی در شکم بچّه کردی تباه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مکرانیان دختری نوش لب</p></div>
<div class="m2"><p>همی داشت شادان به روز و به شب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنش چون گل و ناف و پستان حریر</p></div>
<div class="m2"><p>لبان شکّر و گیسوانش عبیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به غمزه شه جاودان را گزند</p></div>
<div class="m2"><p>به چهره دل ماه از او زیر بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل از راستی، دیده از ناز و شرم</p></div>
<div class="m2"><p>ز شمشاد بالا، ز سوسنْش چرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن شهد و رفتار طاووس وار</p></div>
<div class="m2"><p>نگارین همی خواندش شهریار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگارش چو آرایش جان نبود</p></div>
<div class="m2"><p>جز از خویشِ دارای مکران نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برادرش را دخت بود آن نگار</p></div>
<div class="m2"><p>نهان داشت راز از دل شهریار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که هرکس کزآن تخمه دید او، بکُشت</p></div>
<div class="m2"><p>بر آن تخمه بر بختِ بد شد درست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز مهر نگارین که بودش ز پیش</p></div>
<div class="m2"><p>هنوزش به دل مانده اندوه و ریش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشسته به بگماز روزی بهم</p></div>
<div class="m2"><p>دل از رنج دور و روان از ستم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدو شادمانه دل شهریار</p></div>
<div class="m2"><p>گهی ناز و گه بوسه و گه کنار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت کای گنج پرخواسته</p></div>
<div class="m2"><p>ز ما آرزو هیچ ناخواسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی آرزو خواه و دل برمپیچ</p></div>
<div class="m2"><p>که هرگز ندارم دریغ از تو هیچ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگارین چنین پاسخش داد و گفت</p></div>
<div class="m2"><p>که بادی همه ساله با بخت جفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به فرّ تو شاها، مرا کام هست</p></div>
<div class="m2"><p>بزرگی و نیکی و آرام هست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی بر شبستانت فرمان دهم</p></div>
<div class="m2"><p>به خودکامگی پیش تو جان دهم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا آرزو کام شاه است و بس</p></div>
<div class="m2"><p>که بر آرزو هست خود دسترس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت کز من رهایی مجوی</p></div>
<div class="m2"><p>یکی آرزو خواه بی گفت و گوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت شاها، میفزای رنج</p></div>
<div class="m2"><p>که دارم همی هرچه باید ز گنج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی را بود آرزو، کش نیاز</p></div>
<div class="m2"><p>به چیزی بود کآن نیابد فراز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من آری نیازی ندارم کنون</p></div>
<div class="m2"><p>که گنجی که دارم ندانم که چون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر شاه بیند، نفرمایدم</p></div>
<div class="m2"><p>که ترسم که گفتار بگزایدم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر شهریاری بخواهی تو، گفت</p></div>
<div class="m2"><p>ندارم دریغ از تو ای نیک جفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو گفت کاکنون که گفتار شاه</p></div>
<div class="m2"><p>چنین است با بنده ی نیکخواه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیازم نیاید به گیتی ز چیز</p></div>
<div class="m2"><p>ولیکن یکی پرسش آرم بنیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که از دیرباز این سخن در دلم</p></div>
<div class="m2"><p>همی دارم و دل همی بگسلم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو گفت خسرو میندیش هیچ</p></div>
<div class="m2"><p>بپرس آنچه خواهی و دل برمپیچ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نگارین بدو گفت کای نیکخوی</p></div>
<div class="m2"><p>مرا این یکی داستانی بگوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کزآن پس که از چین سپه راندی</p></div>
<div class="m2"><p>به دست بداندیش درماندی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپاه فریدون و زخم درشت</p></div>
<div class="m2"><p>یکی کوهپایه گرفتی تو پشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه بر دشمنان چاره ای ساختی</p></div>
<div class="m2"><p>نه تیری سوی دشمن انداختی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو از شاه مکران سپه خواستی</p></div>
<div class="m2"><p>بدین آرزو نامه آراستی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سپاهی فرستاد با ساز جنگ</p></div>
<div class="m2"><p>که بشکست دشمن برای درنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو در پادشاهی بگشتی همی</p></div>
<div class="m2"><p>به مکران زمین برگذشتی همی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به پیش تو آورد چندان ز گنج</p></div>
<div class="m2"><p>که پیلان شدند از کشیدن به رنج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همی کرد یک ماه ساز سپاه</p></div>
<div class="m2"><p>که از خوردنی تنگ شد جایگاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وزآن پس چو برگشتی از خاوران</p></div>
<div class="m2"><p>پذیره شدت پیش با سروران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دگر باره چندان به پیشت کشید</p></div>
<div class="m2"><p>که از هیچ کهتر چنان کس ندید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درِ گنجهای پدر باز کرد</p></div>
<div class="m2"><p>چهل روز لشکر ورا ساز کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو برداشتی کاندر آیی به چین</p></div>
<div class="m2"><p>همی رفت پیشت دو منزل زمین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از آن پس که او خواست گشتن ز راه</p></div>
<div class="m2"><p>به دو نیم کردش سرافراز شاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو کردار پاداش او این نمود</p></div>
<div class="m2"><p>جهان ناامید از شه چین ببود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همی خواهم اکنون که فرخنده شاه</p></div>
<div class="m2"><p>نماید به من بنده او را گناه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که هرگز چنین شهریاران، پیش</p></div>
<div class="m2"><p>نکردند با زیردستان خویش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نه زآن سان پرستش کسی کرد نیز</p></div>
<div class="m2"><p>که دارای چین دشمنش کشت نیز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به هنگام پاداش تیغ آمدش</p></div>
<div class="m2"><p>یکی دخمه از وی دریغ آمدش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز پرسش برآشفت یکباره کوش</p></div>
<div class="m2"><p>بدو گفت کای بدرگ خیره هوش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو را با چنین داستان خود چه کار</p></div>
<div class="m2"><p>پسودن به بیهوده دنبال مار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنان مست شتی تو اندر نواخت</p></div>
<div class="m2"><p>که هرگونه پرسش توانی تو ساخت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زمانه دل ما پر از خون کند</p></div>
<div class="m2"><p>گر این پرسش از من فریدون کند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو را پایه پیدا که چند است و چون</p></div>
<div class="m2"><p>بدین رهنمون تو بوده ست خون</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر من ز پاسخ به یک سو شوم</p></div>
<div class="m2"><p>به نادانی خویش خستو شوم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نخستین تو را پاسخ آرم درست</p></div>
<div class="m2"><p>دهم آرزوی دلت را نخست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سزای تو زآن پس رسانم به تو</p></div>
<div class="m2"><p>کزاین گفته من بد گمانم به تو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو شاه جهان را فریدون بکشت</p></div>
<div class="m2"><p>به کام نهنگ اندر افتاد شست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز کوه بسیلا گریزان شدم</p></div>
<div class="m2"><p>به آرامگه اشک ریزان شدم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نخستین کس او بُد ز فرمان برون</p></div>
<div class="m2"><p>شد و دیگران را ببُد رهنمون</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نگه کرد هر کس به کردار او</p></div>
<div class="m2"><p>بزرگان شدند از پی کار او</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>وزایران دوبار ایدر آمد سپاه</p></div>
<div class="m2"><p>از او خواستم لشکر و دستگاه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نه آن کرد روزی که بیند رخم</p></div>
<div class="m2"><p>نه یاور فرستاد و نه پاسخم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بدان گه که رفتم به درگاه شاه</p></div>
<div class="m2"><p>ز کوه بسیلا بیامد سپاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چه مایه سپه بود با آتبین</p></div>
<div class="m2"><p>کز او گشت ویران همه مرز چین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چه بودی اگر تاختی پیش اوی</p></div>
<div class="m2"><p>وگر لشکری ساختی پیش اوی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نه بودی به چین اندر از وی گزند</p></div>
<div class="m2"><p>نه کار بداندیش گشتی بلند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از او بود نوشان همه یاوری</p></div>
<div class="m2"><p>نبودش خود اندیشه ی داوری</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به چوپان اسب و شبانان گله</p></div>
<div class="m2"><p>توانست کرد آتبین را حله</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نکرد و بدین روزگار آنچه کرد</p></div>
<div class="m2"><p>هم از بیم کرد آن فرومایه مرد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کنون پاسخ این است و پاداش این</p></div>
<div class="m2"><p>بگفت و بزد بر سرش تیغ کین</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به دو نیم زد خرمن گل به تیغ</p></div>
<div class="m2"><p>از آن خوبچهرش نیامد دریغ</p></div></div>