---
title: >-
    بخش ۳۴ - پاسخ مهانش
---
# بخش ۳۴ - پاسخ مهانش

<div class="b" id="bn1"><div class="m1"><p>مهانش شنید و سرافگند پیش</p></div>
<div class="m2"><p>از اندیشه ی او دلش گشت ریش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس آن گاه سرکرد بر آسمان</p></div>
<div class="m2"><p>همی گفت کای کردگار جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشاید همی گر نه زین سان کنی</p></div>
<div class="m2"><p>که گردنکشان را هراسان کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباید تو ای شاه گیتی گشای</p></div>
<div class="m2"><p>که بندی دل اندر سپنجی سرای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر دَه بمانیم اگر ده هزار</p></div>
<div class="m2"><p>وگر بنده باشیم و گر شهریار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مرگ آید آن روزگار اندکی ست</p></div>
<div class="m2"><p>همان بنده و شاه گیتی یکی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مر این کوش را آتبین پرورید</p></div>
<div class="m2"><p>چو در بیشه ی چین مر او را بدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه سرگذشتش به پیش من است</p></div>
<div class="m2"><p>اگرچه نه از خون و خویش من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون شاه را بخشم این داستان</p></div>
<div class="m2"><p>بسی دفتر دیگر از باستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که من آرزو خواهم از کردگار</p></div>
<div class="m2"><p>که بر من سر آرد کنون روزگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که گشتم بر این کوهساران غمی</p></div>
<div class="m2"><p>نبینم پس از تو دگر آدمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امیدم چنین است کاین آرزو</p></div>
<div class="m2"><p>بیابم به فرمان، تو ای نیکخو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو یزدان سرآرد به من بر زمان</p></div>
<div class="m2"><p>بدین خانه اندر کنیدم نهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت این و کرد آنگهی آبدست</p></div>
<div class="m2"><p>بشد پیش یزدان به زانو نشست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو رخساره را بر زمین بر نهاد</p></div>
<div class="m2"><p>نیایش همی کرد تا جان بداد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سکندر ز گفتار او خیره ماند</p></div>
<div class="m2"><p>همی آب دیده به رخ بر براند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بشستند و کردند بروی نماز</p></div>
<div class="m2"><p>در خانه ی تنگ کردند باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مر آن خانه را دخمه ای ساختند</p></div>
<div class="m2"><p>مر او را به خاک اندر انداختند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همین کرد خواهد به هر کس جهان</p></div>
<div class="m2"><p>همین است جای کهان و مهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وز آن جا بسی دفتر آورد شاه</p></div>
<div class="m2"><p>همی کرد هر یک به هر یک نگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی دفتر از پوست آهو بیافت</p></div>
<div class="m2"><p>چو دستور خسرو به خواندن شتافت</p></div></div>