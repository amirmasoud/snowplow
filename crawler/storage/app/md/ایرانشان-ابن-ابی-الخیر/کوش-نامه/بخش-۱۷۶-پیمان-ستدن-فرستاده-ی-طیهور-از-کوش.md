---
title: >-
    بخش ۱۷۶ - پیمان ستدن فرستاده ی طیهور از کوش
---
# بخش ۱۷۶ - پیمان ستدن فرستاده ی طیهور از کوش

<div class="b" id="bn1"><div class="m1"><p>ز دریا چو بر شد فرستاده مرد</p></div>
<div class="m2"><p>همان گه ز پیش وی آهنگ کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامد همه کوش را بازگفت</p></div>
<div class="m2"><p>دل کوش مانند گل برشکفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر او را نهانی بسی چیز داد</p></div>
<div class="m2"><p>در نیکبختی بر او برگشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو روز آمد آن مرد رنجور را</p></div>
<div class="m2"><p>فرستاده ی شاه طیهور را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاورد و بر کرسی زر نشاند</p></div>
<div class="m2"><p>بخوبی فراوان سخنها براند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرستاده از هول رخسار او</p></div>
<div class="m2"><p>وزآن هیبت زشت و دیدار او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه کرد آفرین و نه بردش نماز</p></div>
<div class="m2"><p>چو شوریده ای پیش او شد فراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فراوانْش بنواخت دارای چین</p></div>
<div class="m2"><p>چو ایمن شد از شاه، کرد آفرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس آن نامه در پیش تختش نهاد</p></div>
<div class="m2"><p>چو نوشان سرِ نامه را برگشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سراسر همه مردمی بود و مهر</p></div>
<div class="m2"><p>دلش گشت شادان، برافروخت چهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مر او را گرامی همی داشتند</p></div>
<div class="m2"><p>زمانیش بی بزم نگذاشتند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوم روز فرمود تا رفت پیش</p></div>
<div class="m2"><p>سخن گفت با او ز اندازه بیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وزآن پس بدو گفت فرمان شاه</p></div>
<div class="m2"><p>بجای آر و آن گاه بردار راه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر شاه را، گفت، باشد گران</p></div>
<div class="m2"><p>مرا گر دهد دست پیش سران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدان سان که سوگند طیهور خورد</p></div>
<div class="m2"><p>خورَد شاه و از من نباشدش درد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرستاده را داد شه، دست راست</p></div>
<div class="m2"><p>ببستش به سوگند از آن سان که خواست</p></div></div>