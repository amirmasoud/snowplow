---
title: >-
    بخش ۱۰۰ - پاسخ به کوش
---
# بخش ۱۰۰ - پاسخ به کوش

<div class="b" id="bn1"><div class="m1"><p>چو رفتند نزدیک طیهور شاه</p></div>
<div class="m2"><p>نهادند نامه در آن پیشگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو در نامه آن ناسزا خواند گفت</p></div>
<div class="m2"><p>که با دیو زاده خرد نیست جفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آشفت و دژخیم را گفت شاه</p></div>
<div class="m2"><p>که این هر دو را کرد باید تباه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از این، آتبین را رسید آگهی</p></div>
<div class="m2"><p>فرستاد کس پیش تخت مهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که شاه سرافراز دانا تراست</p></div>
<div class="m2"><p>فرستاده را چون چه اندر خور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارند شاهان گیتی به نام</p></div>
<div class="m2"><p>کجا بر فرستاده رانند کام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که ایشان همه بنده و چاکرند</p></div>
<div class="m2"><p>نه فرماندهانند که فرمانبرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی پاسخ نامه فرمای کرد</p></div>
<div class="m2"><p>که آرد دل پیل دندان به درد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرستاد نامه بر آتبین</p></div>
<div class="m2"><p>که ما را فرستاد کوش این چنین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مر این بی بنان را تباهی سزاست</p></div>
<div class="m2"><p>همان است رای تو پیشم رواست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببخشم به تو خشم این ناکسان</p></div>
<div class="m2"><p>تو از من بدان دیو پاسخ رسان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بشنید پیغام شاه، آتبین</p></div>
<div class="m2"><p>یکی نامه کرد او به سالار چین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که بر خواندم این نامه ی بدنهاد</p></div>
<div class="m2"><p>خود این آید از مردم بدنژاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز نادانی تو بمانم شگفت</p></div>
<div class="m2"><p>ز موبد نجستی تو راز نهفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندادت کس آگاهی از کار ما</p></div>
<div class="m2"><p>وز این کوه و زین بخت بیدار ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بپرس ار نداری از این آگهی</p></div>
<div class="m2"><p>که تو نو رسیده یکی ابلهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من از خویشتن برتر اندر جهان</p></div>
<div class="m2"><p>ندانم کسی آشکار و نهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که بود از نیاگان ما زیر دست</p></div>
<div class="m2"><p>همه شاه بودند و یزدان پرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا چون بهک دانی و دیگران</p></div>
<div class="m2"><p>که هستند شاه تو را کهتران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهانی اگر نزدم آرند روی</p></div>
<div class="m2"><p>به یزدان کز ایوان نیایم به کوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یک مرد و سنگی ز کوه سیاه</p></div>
<div class="m2"><p>بگردانم این لشکر کینه خواه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدین کار هشیارتر درنگر</p></div>
<div class="m2"><p>چو بردی به مابر گمانی مبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون نامه بپایان رسانیده بود</p></div>
<div class="m2"><p>فرستاد نزدیک طیهور زود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بخواند و بر او آفرین خواند و گفت</p></div>
<div class="m2"><p>که با جان پاکت خرد باد جفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرستاده را داد و گفتا که رو</p></div>
<div class="m2"><p>که رستی تو از تیغ گردن درو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر بار دیگر فرستد کسی</p></div>
<div class="m2"><p>درنگی نباشد نبینم بسی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هم از ره به آب اندر اندازمش</p></div>
<div class="m2"><p>به دریا خور ماهیان سازمش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرستادگان راه برداشتند</p></div>
<div class="m2"><p>پراندیشه آن راه بگذاشتند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سوی راست و چپ کرد هر دو نگاه</p></div>
<div class="m2"><p>که یابند جایی بر آن کوه راه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ندیدند، خیره فرو ماندند</p></div>
<div class="m2"><p>ز دربند ترسان فرو راندند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به کشتی نشستند، بادی چو نوش</p></div>
<div class="m2"><p>به یک مه رسانیدشان پیش کوش</p></div></div>