---
title: >-
    بخش ۳۶۵ - شارستانی به نام ارم
---
# بخش ۳۶۵ - شارستانی به نام ارم

<div class="b" id="bn1"><div class="m1"><p>دگر شارسْتانی ست از سیم ناب</p></div>
<div class="m2"><p>به شکل عقابی سر اندر سحاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که اندازه زآن بر نشاید گرفت</p></div>
<div class="m2"><p>طلسمی بزرگ است و جایی شگفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی نغز سیمرغ پرداخته</p></div>
<div class="m2"><p>ز سیم سپید از برش ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر روی ده روزه ریگ روان</p></div>
<div class="m2"><p>رسیدن بدان شارْسْتان کی توان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارم خوانده یزدان و کس آن ندید</p></div>
<div class="m2"><p>که مانند آن هیچ راه نافرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان من و اندلس جای اوست</p></div>
<div class="m2"><p>نه دشمن همی بیند آن را نه دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درختش همه زرّ و یاقوت بار</p></div>
<div class="m2"><p>همه برگ او زُمْرُد آبدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمینش همه خشت زرّین و سیم</p></div>
<div class="m2"><p>شکوفه ز مرجان و درّ یتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می و شیر در جوی با انگبین</p></div>
<div class="m2"><p>که سازد چنین، جز جهان آفرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی شهر نزدیک دریای ژرف</p></div>
<div class="m2"><p>نهادی تو بازارگاه شگرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خمدان به فرسنگها هشتصد</p></div>
<div class="m2"><p>برآید همی تا کس آن جا رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برآرنده ماروق کرده ست نام</p></div>
<div class="m2"><p>در او مردمانی همه شادکام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه پوشش بانها استخوان</p></div>
<div class="m2"><p>ز پهلوی ماهی گرفتند خوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرآن شاه کآهنگ شهر آورد</p></div>
<div class="m2"><p>ز بیداد و تاراج بهر آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نماند همی زنده سالی فزون</p></div>
<div class="m2"><p>چنین ساخته مردم پُر فسون</p></div></div>