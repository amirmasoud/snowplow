---
title: >-
    بخش ۲۶۱ - حرکت کوش و سپاه از آمل برای پیگار با سیاهان مازندران
---
# بخش ۲۶۱ - حرکت کوش و سپاه از آمل برای پیگار با سیاهان مازندران

<div class="b" id="bn1"><div class="m1"><p>از آمل روان گشت لشکر به راه</p></div>
<div class="m2"><p>همی رفت یکی میل با کوش، شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز آن جایگه راه موصل گرفت</p></div>
<div class="m2"><p>بیابان و کهسار و منزل گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاد با نامه پنجه سوار</p></div>
<div class="m2"><p>سوی شهر موصل بدان مرزدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ما را گذر بر تو آمد نخست</p></div>
<div class="m2"><p>نباید که باشی تو در کار سست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علف ساز چندان که داری توان</p></div>
<div class="m2"><p>که هست این سپاهی چو سیل روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباید که در راه تنگی بود</p></div>
<div class="m2"><p>بدین مرز لشکر درنگی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آمد به موصل، بسی ساز دید</p></div>
<div class="m2"><p>همان مرزبانی سزاوار دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان مرزبانی درنگ آمدش</p></div>
<div class="m2"><p>بسی ساز شاهان به چنگ آمدش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز آن جا سوی مصر بنهاد روی</p></div>
<div class="m2"><p>همه راه شادان دل و پوی پوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز لشکر سواری فرستاد پیش</p></div>
<div class="m2"><p>نبشته یکی نامه بی کمّ و بیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که ما را همی شاه گندآواران</p></div>
<div class="m2"><p>فرستد به پیگار مازندران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو ساله علف ساز با خوردنی</p></div>
<div class="m2"><p>فراز آر هر گونه آوردنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که آن کشوری هست ویران شده</p></div>
<div class="m2"><p>کنام پلنگان و شیران شده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نباید که تنگی کند لشکرم</p></div>
<div class="m2"><p>چو از مرز آباد تو بگذرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرستاد هنگام بانگ خروس</p></div>
<div class="m2"><p>بیامد شتابان به نزد کیوس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که در شهر بوصیر بودش نشست</p></div>
<div class="m2"><p>همه ساله از بخت شادان و مست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو فسطاط ناکرده بودند نوز</p></div>
<div class="m2"><p>به بوصیر بودی بهار و تموز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو نامه به فرزند نوشان رسید</p></div>
<div class="m2"><p>ز فرمان او هیچ چاره ندید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فراز آورید آنچه بودش به شهر</p></div>
<div class="m2"><p>دگر خواست از لشکر و شهر بهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه دشت پُر خوردنی کرده بود</p></div>
<div class="m2"><p>که از شهر، وز کشور آورده بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به راه جزیره همی راند کوش</p></div>
<div class="m2"><p>همه لشکر گشن پولادپوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو از راه برخاست آوای کوس</p></div>
<div class="m2"><p>پذیره شدش با بزرگان کیوس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو چشمش برآمد برآن رزمساز</p></div>
<div class="m2"><p>پیاده شدش پیش و بردش نماز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بترسید از آن هول و سختی دلش</p></div>
<div class="m2"><p>گهر کهربا کرد گِرد گلش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو کوش آن چنان دید بنواختش</p></div>
<div class="m2"><p>بپرسید و بر باره بنشاختش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به فرسنگ بوصیر آمد فرود</p></div>
<div class="m2"><p>به یک دست باغ و دگر دست رود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کیوس سرافراز را پیش خواند</p></div>
<div class="m2"><p>به نزدیکی پیشگاهش نشاند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو گفت کز ساز وز خوردنی</p></div>
<div class="m2"><p>چه مایه فراز آمد آوردنی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنین پاسخ آورد کای تاجور</p></div>
<div class="m2"><p>از این آمدن دیر بودم خبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خورش هست چندان که یک ساله شاه</p></div>
<div class="m2"><p>ببخشد بر این نامبرده سپاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دل کوش از آن نیکدل شاد شد</p></div>
<div class="m2"><p>وز اندیشه ی لشکر آزاد شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو گفت یک ساله داریم نیز</p></div>
<div class="m2"><p>دو ساله نیازم نیاید به چیز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نباید فراوان زمین جز گیا</p></div>
<div class="m2"><p>و گر هست کشور همه کیمیا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو این خوردنی سر بسر بار کن</p></div>
<div class="m2"><p>بر اشتر تو در کشور انبار کن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وزآن پس دگر هرچت آید به دست</p></div>
<div class="m2"><p>به دست کسانت سوی مافرست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کیوس جهاندیده خواهش نمود</p></div>
<div class="m2"><p>بر آن خواهش او را ستایش نمود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که ایدر بر آسای یکچندگاه</p></div>
<div class="m2"><p>نپذرفت و برداشت یکسر سپاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شتابان بیامد به شهر سماب</p></div>
<div class="m2"><p>درخت و گیا دید و آب و تباب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که اکنون همی برقه خوانی به نام</p></div>
<div class="m2"><p>یکی مرزبان اندر او شادکام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مر آن شهر و آن شاه با دستبرد</p></div>
<div class="m2"><p>جهاندیده از حد مغرب شمرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سراپرده زد کوش در مرغزار</p></div>
<div class="m2"><p>پراگند هر جای مردان کار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کرا یافت آواره از شهر و جای</p></div>
<div class="m2"><p>درم داد چندان که شد کدخدای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرستاد هر کس سوی شهر خویش</p></div>
<div class="m2"><p>چو از گنج بستد همی بهر خویش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به هر مرزبر کارداری گماشت</p></div>
<div class="m2"><p>که هر کار داری یکی گنج داشت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بفرمود تا هر کرا یافتند</p></div>
<div class="m2"><p>ز بیگانگی روی برتافتند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدادند چندان که بایست چیز</p></div>
<div class="m2"><p>سه ساله خراجش یله کرد نیز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از آن شهرها هفت بودند و پنج</p></div>
<div class="m2"><p>که از بجّه و نوبه دیدند رنج</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از ایشان یکی روبله کرد شهر</p></div>
<div class="m2"><p>که گفتی که دارد ز فردوس بهر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دگر یونس و طَرفه و قیروان</p></div>
<div class="m2"><p>مر آن هر سه را باغ و آب روان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بصیره دگر بیش و ناکور بود</p></div>
<div class="m2"><p>که پیوسته از بجّه رنجور بود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زویله دگر بود و ماهی دگر</p></div>
<div class="m2"><p>دگر شازده کشور نامور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دگر فاس و بجنک بودند و هوم</p></div>
<div class="m2"><p>همه مرز پیوسته بامرز روم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کنون هوم را گر ندانی همی</p></div>
<div class="m2"><p>جزیره ی بنی رعم خوانی همی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دگر شهر تاهرت و شهر مبات</p></div>
<div class="m2"><p>گلاب آب و زعفرانش نبات</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مر این شهرها را که کردیم یاد</p></div>
<div class="m2"><p>همی داد هرگاه نوبی به باد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نبود اندر این کشور آباد جای</p></div>
<div class="m2"><p>نه بر پای دیدند جایی سرای</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مگر برقه کآن خوب و آباد بود</p></div>
<div class="m2"><p>گریزنده را جای فریاد بود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز مردم هرآن کس که بگذاشت جای</p></div>
<div class="m2"><p>سوی اندلس رفت بی پرّ و پای</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو آگاه شد کوش، شد شادمان</p></div>
<div class="m2"><p>که در اندلس یافت آن مردمان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>قراطوس بود اندر آن شهر شاه</p></div>
<div class="m2"><p>جوانی سرافراز با دستگاه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سپاهش فزونتر ز موران خُرد</p></div>
<div class="m2"><p>وگر چون ستاره که نتوان شمرد</p></div></div>