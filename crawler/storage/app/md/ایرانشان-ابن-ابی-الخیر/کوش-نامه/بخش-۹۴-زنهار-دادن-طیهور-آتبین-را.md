---
title: >-
    بخش ۹۴ - زنهار دادن طیهور آتبین را
---
# بخش ۹۴ - زنهار دادن طیهور آتبین را

<div class="b" id="bn1"><div class="m1"><p>به طیهور گفت آتبین از میان</p></div>
<div class="m2"><p>که من با تنی چند از ایرانیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زنهار نزدیک شاه آمدیم</p></div>
<div class="m2"><p>بدین مایه ور بارگاه آمدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجای من آن کن که از تو سزاست</p></div>
<div class="m2"><p>که نا خوبی از پادشا ناسزاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفت طیهور کای پاک هوش</p></div>
<div class="m2"><p>بگفتی، کنون سوی من دارگوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یزدان که پروردگار من است</p></div>
<div class="m2"><p>به سختی و اندوه یار من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که هرگز نجویم من آزار تو</p></div>
<div class="m2"><p>وگر جان شود در سرِ کار تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجای آورم تا توانم وفا</p></div>
<div class="m2"><p>نمانم که آید به رویت جفا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دشمنت نسپارم از هیچ روی</p></div>
<div class="m2"><p>تو بر گردِ اندیشه ی بد مپوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیاگان ما، شهریاران پیش</p></div>
<div class="m2"><p>کسی را ندیدند برتر ز خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه گردن نهادند کین راستر</p></div>
<div class="m2"><p>نه ماچین و چین داشتند و نه خر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا پادشاهی نه ز آن کشور است</p></div>
<div class="m2"><p>جزیره ست و خود کشوری دیگر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر این کوه جز یک گذرگاه نیست</p></div>
<div class="m2"><p>همانا که دیدی جز آن راه نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دریا که گرد جهان اندر است</p></div>
<div class="m2"><p>بزرگ است وز گوهران برتر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو پهلوش دریا که ندهد گذار</p></div>
<div class="m2"><p>نه زورق، نه کشتی توان کردکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دگر پهلو از راه ماچین و چین</p></div>
<div class="m2"><p>همی کشتی آید ز ایران زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چهارم به پهلو به یک سال و نیم</p></div>
<div class="m2"><p>به دریا همی راند باید به بیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پدید آید آن گاه دریا کنار</p></div>
<div class="m2"><p>همان کوه قاف اندر او مرغزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز دریا برآید ستمدیده مرد</p></div>
<div class="m2"><p>دل از رنج دریا رسیده به درد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>براردت و در چند شهر است باز</p></div>
<div class="m2"><p>همه در غم و رنج و ننگ و نیاز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برابرش یأجوج و مأجوج نیز</p></div>
<div class="m2"><p>شب و روز از ایشان دمانند چیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وز آن جا بباید گذشتن به قاف</p></div>
<div class="m2"><p>نهاد جهان را مبین از گزاف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز قاف اندر آید به سقلاب و روم</p></div>
<div class="m2"><p>رسد بوی آباد آن مرز و بوم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز آن شهرها کز پس کوهسار</p></div>
<div class="m2"><p>بیایند بازارگان بیشمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به هر بیست سالی بیایند نیز</p></div>
<div class="m2"><p>از آن جا بیارند هرگونه چیز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو این خانه را خانه ی خویش دان</p></div>
<div class="m2"><p>مرا هم پدر دان و هم خویش دان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به روز آهو افگن، به شب جام گیر</p></div>
<div class="m2"><p>بت ماه پیکر در آغوش گیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان دان تو ای پرهنر شهریار</p></div>
<div class="m2"><p>که سال اندر آمد فزون از هزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که بنیاد این شهر ما کرده ایم</p></div>
<div class="m2"><p>بدین سان به گردون برآورده ایم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیای من افگند بنیاد شهر</p></div>
<div class="m2"><p>که او را همی روشنی باد بهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مر این شهر را نامِ خود برنهاد</p></div>
<div class="m2"><p>بدان تا بداریم نامش به یاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدین کوه کس هیچ ننهاد پای</p></div>
<div class="m2"><p>مر این رزم با کس نکرده ست رای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر او آتبین آفرین کرد و گفت</p></div>
<div class="m2"><p>که با جان خسرو خرد باد جفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه هرچه گویی چنین است راست</p></div>
<div class="m2"><p>از اندرز جمشید چیزی نکاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که تو مهربانی و یزدان پرست</p></div>
<div class="m2"><p>پناه از تو بهتر نیاید به دست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به دستور فرمود کاندرزِ شاه</p></div>
<div class="m2"><p>هم اکنون بیاور براین پیشگاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیاورد دستور هم در زمان</p></div>
<div class="m2"><p>به طیهور برخواندش ترجمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نبشته چو بشنید از آن پیش بین</p></div>
<div class="m2"><p>همی خواند بر دانشش آفرین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر او بوسه داد و به زاری گریست</p></div>
<div class="m2"><p>بدین دانش اندر جهان، گفت کیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دریغا که بردست آن بدنژاد</p></div>
<div class="m2"><p>چنین دانش خسروی شد به باد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ببوسید پس دیده ی آتبین</p></div>
<div class="m2"><p>بدو گفت کای شهریار زمین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جهان آفرین از تو خشنود باد</p></div>
<div class="m2"><p>دل کوش و ضحاک پر دود باد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو این جا رسیدی میندیش هیچ</p></div>
<div class="m2"><p>می و جام و شادی و رامش بسیچ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که سوگند خوردم به ماه و به مهر</p></div>
<div class="m2"><p>به جان گرامی و خورشید چهر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که تا باشی ایدر نیازارمت</p></div>
<div class="m2"><p>چو جان گرامی همی دارمت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نمانم که دشمن ببیند رخت</p></div>
<div class="m2"><p>نه هرگز به زشتی کنم پاسخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به پیش تو دارم تن و خواسته</p></div>
<div class="m2"><p>همین لشکر گشن آراسته</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بفرمود تا سی کنیزک چو ماه</p></div>
<div class="m2"><p>پرستنده آورد نزدیک شاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همه پایکوب و همه چنگ ساز</p></div>
<div class="m2"><p>همه دلفریب و همه دلنواز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به چهره سراسر گل زاولی</p></div>
<div class="m2"><p>به غمزه همه جادوی بابلی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به بالا بکردار سرو روان</p></div>
<div class="m2"><p>به رخسار همچون گل و ارغوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کمان کرده از مشک سارا به زه</p></div>
<div class="m2"><p>کمند عبیری زده بر گره</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به بازی و رامش کشیدند دست</p></div>
<div class="m2"><p>از ایشان همه خیره شد مرد مست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به پایان بزم آن بتان را رمه</p></div>
<div class="m2"><p>بداد آن بتان آتبین را همه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دو اسب گرانمایه دادش بنیز</p></div>
<div class="m2"><p>ز دیبای زربفت و هرگونه چیز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یکی تخت با افسر شاهوار</p></div>
<div class="m2"><p>ز یاقوت و پیروزه او را نگار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو با آتبین هوش بیگانه شد</p></div>
<div class="m2"><p>بدان شادکامی سوی خانه شد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همه شب ز دیدارشان بود شاد</p></div>
<div class="m2"><p>ز شادی و مستی همی داد داد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>قمر دید چون دیدگان باز کرد</p></div>
<div class="m2"><p>بشد هوش یکبارگی را ز مرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فزون بودشان خوبی از یکدگر</p></div>
<div class="m2"><p>همین زآن، همان زین بسی خوبتر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زن و مرد آن شهر مانند ماه</p></div>
<div class="m2"><p>رخی همچو خورشید و رویی چو ماه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به دیدن پری و به بالا شگرف</p></div>
<div class="m2"><p>به رخساره ماننده ی خون و برف</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خوش آواز و نازکدل و تندرست</p></div>
<div class="m2"><p>کدامین که پیوند ایشان نجست</p></div></div>