---
title: >-
    بخش ۲۶۷ - چاره ی دیگر کوش و گرفتار شدن قراطوس
---
# بخش ۲۶۷ - چاره ی دیگر کوش و گرفتار شدن قراطوس

<div class="b" id="bn1"><div class="m1"><p>چو پردخته شد او به کار سپاه</p></div>
<div class="m2"><p>میان سپاهش نهان گشت شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآمد خروش و بپیوست جنگ</p></div>
<div class="m2"><p>شتاب اندر آمد بجای درنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غو کوس و آواز گرز یلان</p></div>
<div class="m2"><p>نهان کرده بر چهره ی بددلان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدنگ دو پیکان دل و دیده خست</p></div>
<div class="m2"><p>کمند گوان دست و گردن ببست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنان درخشان روان را بسوخت</p></div>
<div class="m2"><p>چو آتش ز خون تیغها برفروخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآن دشت خون یلان جوی کرد</p></div>
<div class="m2"><p>زمانه سر بددلان گوی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان شد که اسب نبرد آزمای</p></div>
<div class="m2"><p>بجز بر سر و سینه ننهاد پای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قراطوسیان دسترس یافتند</p></div>
<div class="m2"><p>شب آمد همه روی برتافتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلایه برون رفت و بنشست جوش</p></div>
<div class="m2"><p>سران سپه را بپرسید کوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که امروز چون بودتان روزگار</p></div>
<div class="m2"><p>چگونه ست دشمن گه کارزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان پاسخ آورد گردنکشی</p></div>
<div class="m2"><p>که دشمن چنان است چون آتشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زهره دلیر و به تن زورمند</p></div>
<div class="m2"><p>فراوان نمودند ما را گزند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که ما سست بودیم در کارزار</p></div>
<div class="m2"><p>چنانچون تو فرمودی ای شهریار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندانیم کاین رای چون دیده ای</p></div>
<div class="m2"><p>که سستی ز لشکر پسندیده ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین پاسخش داد آری رواست</p></div>
<div class="m2"><p>کند هرکسی آن که او را هواست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی چاره ای خواستم ساختن</p></div>
<div class="m2"><p>دل از رنج دشمن بپرداختن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنون کار از آن چاره اندر گذشت</p></div>
<div class="m2"><p>ببینید فردا بر این پهن دشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که من با قراطوس جنگ آورم</p></div>
<div class="m2"><p>بگیرمش و ایدر به تنگ آورم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اسیران آن روز را پیش خواست</p></div>
<div class="m2"><p>بپرسید و گفتا بگویید راست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که پیش قراطوس گستاختر</p></div>
<div class="m2"><p>کدام است تا زو بپرسم خبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمودند پس مهتری را بدوی</p></div>
<div class="m2"><p>ز بند گران زعفران کرده روی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کز این مرد نزدیکتر نیست کس</p></div>
<div class="m2"><p>دگر کهترانیم با دسترس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مر او را بر خویشتن بازداشت</p></div>
<div class="m2"><p>برآن دیگران بر نگهبان گماشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت فردا اگر ز انجمن</p></div>
<div class="m2"><p>نمایی قراطوس را تو به من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به جان و به تن زینهارت دهم</p></div>
<div class="m2"><p>بسی گوهر شاهوارت دهم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو یابم بر این شهر بر دسترس</p></div>
<div class="m2"><p>نیازارم از مردمان تو کس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو را افسر خسروانی دهم</p></div>
<div class="m2"><p>ز کشور یکی مرزبانی دهم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز گفتار او شادمان گشت مرد</p></div>
<div class="m2"><p>بدو گفت بنمایمش در نبرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که جان خوشتر از شاه وز شهر نیز</p></div>
<div class="m2"><p>هم از خویش و پیوند و فرزند و چیز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهنگام طوفان زنی هوشمند</p></div>
<div class="m2"><p>نه فرزند زیر پی اندر فگند؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به مردان خورّه چنین گفت کوش</p></div>
<div class="m2"><p>که امشب نهانی برو بی خروش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سوار و پیاده ببر شش هزار</p></div>
<div class="m2"><p>سر راه دشمن بگیر استوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو دیدی که آمد هزیمت سپاه</p></div>
<div class="m2"><p>درِ شهر باید که داری نگاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نمان تا به شهر اندر آید یکی</p></div>
<div class="m2"><p>که با رنج ازآن پس نماند یکی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپه را به ره کرد و مردم برفت</p></div>
<div class="m2"><p>درِ شهر با رهبری برگرفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو در پرده پنهان شدند اختران</p></div>
<div class="m2"><p>ز خاور برافروخت شمعی گران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قراطوس لشکر بیاراست زود</p></div>
<div class="m2"><p>برآن سان که آیین دوشینه بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همی بود در قلبگه با سپاه</p></div>
<div class="m2"><p>برآویخت و تیره شد از گَرد ماه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از ایرانیان کشته شد چند مرد</p></div>
<div class="m2"><p>به انگُشت بنمودش اندر نبرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بشناختش کوش، نزدیک شد</p></div>
<div class="m2"><p>ز گرد سپه روز تاریک شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدو حمله آورد با ده هزار</p></div>
<div class="m2"><p>از ایران سواران نیزه گزار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپاه قراطوس برداشتند</p></div>
<div class="m2"><p>بکُشتند بسیار و برگاشتند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بزد اسب کوش و برآورد جوش</p></div>
<div class="m2"><p>خروشید کای شاه ناپاک هوش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو نیروی مردی نداری به جنگ</p></div>
<div class="m2"><p>چرا پیش مردان روی تیز چنگ</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بزد چنگ و برداشت او را ز زین</p></div>
<div class="m2"><p>به بالا برآورد و زد بر زمین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ببستند فرمانبرانش دو دست</p></div>
<div class="m2"><p>وزآن پس بغرّید چون پیل مست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز زین کوهه گرز گران برکشید</p></div>
<div class="m2"><p>چو آتش بدان دشمنان بررسید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو دیدند نیرو و آهنگ اوی</p></div>
<div class="m2"><p>گرفتار سالار در چنگ اوی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رمید از نهیبش بدان سان سپاه</p></div>
<div class="m2"><p>که خورشید در گرد گُم کرد راه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکایک نهادند سر سوی شهر</p></div>
<div class="m2"><p>از آن رزم رنج و غمان دید بهر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هزیمت به مردان خورّه رسید</p></div>
<div class="m2"><p>بر این روی دروازه صف برکشید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو پیش آمد آن لشکر کینه خواه</p></div>
<div class="m2"><p>ز دروازه برگشت یکسر سپاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پراگنده هر کس همی تاختند</p></div>
<div class="m2"><p>همه تیغ و ترکش بینداختند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نرفت اندر آن شهر از ایشان یکی</p></div>
<div class="m2"><p>ز ایرانیان کشته شد اندکی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شتابان همی تاخت تا شهر، کوش</p></div>
<div class="m2"><p>سپاه از پسِ پشت پولادپوش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دل اندیشه، مغزش گرفتار بود</p></div>
<div class="m2"><p>که دشمن چنان گشن و بسیار بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همی گفت کایرانیان زین سپاه</p></div>
<div class="m2"><p>نباید که گردند خیره تباه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو مردان خورّه بیامد درست</p></div>
<div class="m2"><p>گُل از روی کوش دلاور بُرست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدو گفت مردان که ای نامجوی</p></div>
<div class="m2"><p>چو دشمن تو را دید برگاشت روی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه نیزه و تیغ برداشتیم</p></div>
<div class="m2"><p>سواری در این شهر نگذاشتیم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پراگنده گشتند گردان همه</p></div>
<div class="m2"><p>که گرگان ببینند روز دمه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از آن شادمان شد دل کوش، گفت</p></div>
<div class="m2"><p>که مردی ز مردان نشاید نهفت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>برآن شیر دل مهربانی فزود</p></div>
<div class="m2"><p>فرود آمد او بر در شهر زود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فرستاد تا لشکری رخت خویش</p></div>
<div class="m2"><p>بیاورد با مایه ور تخت خویش</p></div></div>