---
title: >-
    بخش ۲۳۰ - بازگشتن قارن از سقلاب و روم بنزد فریدون
---
# بخش ۲۳۰ - بازگشتن قارن از سقلاب و روم بنزد فریدون

<div class="b" id="bn1"><div class="m1"><p>چو برگشت قارن ز سقلاب و روم</p></div>
<div class="m2"><p>گشاده به نیرو همه مرز و بوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسیده سوی خاور و باختر</p></div>
<div class="m2"><p>شده بجّه و نوبه زیر و زبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشانده به هر کشوری مهتری</p></div>
<div class="m2"><p>سپرده بدو نامور لشکری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیروزی آمد به درگاه باز</p></div>
<div class="m2"><p>گرفته ز هر کشوری ساو و باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآسود یک سال نزدیک شاه</p></div>
<div class="m2"><p>به گردون گردان کشیده کلاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه رزمها کرد با شاه یاد</p></div>
<div class="m2"><p>به کردار و دیدار او شاه شاد</p></div></div>