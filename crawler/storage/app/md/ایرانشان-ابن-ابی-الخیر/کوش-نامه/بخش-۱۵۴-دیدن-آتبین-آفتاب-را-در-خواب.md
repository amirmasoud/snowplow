---
title: >-
    بخش ۱۵۴ - دیدن آتبین آفتاب را در خواب
---
# بخش ۱۵۴ - دیدن آتبین آفتاب را در خواب

<div class="b" id="bn1"><div class="m1"><p>پراندیشه خسرو شبی خفته بود</p></div>
<div class="m2"><p>جهان دید کز میغ آشفته بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین تیره گشتی چو دریای قیر</p></div>
<div class="m2"><p>به پرده درون ماه و کیوان و تیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب آشفته بودی، هوا هولناک</p></div>
<div class="m2"><p>گرفته جهان سربسر آب و خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن هول ترسان شدی مرد و زن</p></div>
<div class="m2"><p>نیایش نمودی همی تن بتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشیده همه دست بر آسمان</p></div>
<div class="m2"><p>تن اندر نژندی، دل اندر غمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی خویشتن دید جایی بلند</p></div>
<div class="m2"><p>به دل شادمان و به جان ارجمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانی بدو اندر آورده روی</p></div>
<div class="m2"><p>کشیده همه دیدگان اندر اوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی آفتاب از رخانش بتافت</p></div>
<div class="m2"><p>کز آن روشنی هر کسی بهره یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رمید از جهان سربسر تیرگی</p></div>
<div class="m2"><p>برفت از دل مردمان خیرگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهادند سر یک بیک در زمین</p></div>
<div class="m2"><p>بسان شمن پیش شاه آتبین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپیده چو برخاست، با کامداد</p></div>
<div class="m2"><p>مر آن خواب را یک بیک کرد یاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت، شاها، تو رامش فزای</p></div>
<div class="m2"><p>که آمد که یزدان گیتی نمای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پدید آورد آن که جمشید شاه</p></div>
<div class="m2"><p>امید جهان کرد پشت و پناه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمانده ست با مردمان نیکوی</p></div>
<div class="m2"><p>شده ست آشکارا بدو جادوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دگر آن بلندی و آن جایگاه</p></div>
<div class="m2"><p>که مردم همی کرد زی تو نگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی چشم دارند و امیدوار</p></div>
<div class="m2"><p>که باشد شما را یکی روزگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی از شما برنشیند به تخت</p></div>
<div class="m2"><p>که مردم رها گردد از رنج سخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دگر آفتاب، آن که دیدی به خواب</p></div>
<div class="m2"><p>که تابان شد از روی تو آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز عکسش همی میغ شد ناپدید</p></div>
<div class="m2"><p>وز او روشنایی به هر کس رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز پشت تو شاها، نه تا دیرگاه</p></div>
<div class="m2"><p>یکی شهریاری برآید به گاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که تاجش چو خورشید روشن شود</p></div>
<div class="m2"><p>ز دانش جهان زیر جوشن شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بَدان را برآرد ز گیتی دمار</p></div>
<div class="m2"><p>شود کام نیکان بدو کامگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بوَد مرد دینی از او شادمان</p></div>
<div class="m2"><p>غم و رنج بیند از او بدگمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بیاراید آن شاه گیتی به داد</p></div>
<div class="m2"><p>چو هنگام جمشیدِ فرّخ نهاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز گیتی همه کس شود بهره مند</p></div>
<div class="m2"><p>از او جادوی و دیو بیند گزند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برومند گردد ز فرّش زمین</p></div>
<div class="m2"><p>جهان را بیاراید از داد و دین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز گفتار او آتبین گشت شاد</p></div>
<div class="m2"><p>در شادکامی به دل برگشاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نگه کرد در اختر و کار خویش</p></div>
<div class="m2"><p>همه کام دل دید و بازار خویش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فزونی و شاهی و شادی و گنج</p></div>
<div class="m2"><p>سوی دشمنان درد و اندوه و رنج</p></div></div>