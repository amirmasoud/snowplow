---
title: >-
    بخش ۳۵۱ - راندن پیر، کوش را، و پشیمان شدن کوش
---
# بخش ۳۵۱ - راندن پیر، کوش را، و پشیمان شدن کوش

<div class="b" id="bn1"><div class="m1"><p>ز گفتار او کوش شد تنگدل</p></div>
<div class="m2"><p>بزد اسب و برگشت خوار و خجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بهری از آن بیشه ببرید راه</p></div>
<div class="m2"><p>پُر اندیشه شد مغزِ سرگشته راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آباد جایی ست، این مرد پیر</p></div>
<div class="m2"><p>نگوید همی این سخن خیره خیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همانا که دیر است تا ایدر است</p></div>
<div class="m2"><p>هم از بهر کاری به رنج اندر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی داند او راه آباد جای</p></div>
<div class="m2"><p>نباشد همی مرمرا رهنمای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر من از او بازگردم چنین</p></div>
<div class="m2"><p>زیانی نباشد مر او را از این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباید که این خانه بار دگر</p></div>
<div class="m2"><p>نیابم، نیارم من این ره به در</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر من در این بیشه گردم هلاک</p></div>
<div class="m2"><p>چنین مرد را از هلاکم چه باک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ایدر گذشتن مرا روی نیست</p></div>
<div class="m2"><p>که در بیشه بیش از تگاپوی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکوشم به هر چاره تا مرد پیر</p></div>
<div class="m2"><p>مگر باشد از بد مرا دستگیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگشت از ره و بر در کاخ شد</p></div>
<div class="m2"><p>چو با پیر فرزانه گستاخ شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آواز دادش، برآمد به بام</p></div>
<div class="m2"><p>بدو گفت ای تیره دل مرد خام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بودت، چرا بازگشتی ز راه</p></div>
<div class="m2"><p>نیابی همانا همی تخت و گاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدو گفت کای مرد پاکیزه رای</p></div>
<div class="m2"><p>یکی مردمی کن مرا ره نمای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه باشد مرا گر تو یاری دهی</p></div>
<div class="m2"><p>وز این بیشه ام رستگاری دهی</p></div></div>