---
title: >-
    بخش ۳۲۵ - رسیدن نامه ی تور به سلم و بازگشت سلم از روم
---
# بخش ۳۲۵ - رسیدن نامه ی تور به سلم و بازگشت سلم از روم

<div class="b" id="bn1"><div class="m1"><p>برابر همی بود با سال هشت</p></div>
<div class="m2"><p>سواری شتابان برآمد ز دشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی نامه از تور شوریده هوش</p></div>
<div class="m2"><p>که چندین چه باشی تو در بند کوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ایرج ز ما باژ جوید همی</p></div>
<div class="m2"><p>سخنهای بیهوده گوید همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماید به ما بر همی مهتری</p></div>
<div class="m2"><p>پدر داد ما را چنین کهتری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که او را به ایرانیان شاه کرد</p></div>
<div class="m2"><p>مرا و تو را سر سوی راه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برادر که از ما دو تن کهتر است</p></div>
<div class="m2"><p>چرا او بجای پدر در خور است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر تو روا داری این بندگی</p></div>
<div class="m2"><p>بمانی بدو تاج و فرخندگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا نیست در دل چنین داستان</p></div>
<div class="m2"><p>نباشم بدین کار همداستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگرنه سبک باش و نزد من آی</p></div>
<div class="m2"><p>چو با من یکی گشته باشی به رای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سوگند هر دو پساییم دست</p></div>
<div class="m2"><p>که هرگز نباشیم ایرج پرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که بهتر بود مرگ از آن زندگی</p></div>
<div class="m2"><p>کجا کهتران را کنی بندگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آن نامه سلم دلاور بخواند</p></div>
<div class="m2"><p>بزد کوس و شبگیر لشکر براند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حصاری شد از ننگ و سختی رها</p></div>
<div class="m2"><p>به زیر آمد از کوه چون اژدها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپاه پراگنده آمد برش</p></div>
<div class="m2"><p>بپوشید روی زمین لشکرش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه اندلس باز تا باختر</p></div>
<div class="m2"><p>گرفت و به پیروزی آورد سر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر باره ویرانی آباد کرد</p></div>
<div class="m2"><p>به بخشش دل مردمان شاد کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نیرو فزونتر شد از بار پیش</p></div>
<div class="m2"><p>به مردان جنگاور و گنج خویش</p></div></div>