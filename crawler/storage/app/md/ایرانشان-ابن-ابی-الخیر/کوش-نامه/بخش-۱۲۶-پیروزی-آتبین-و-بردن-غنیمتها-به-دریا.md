---
title: >-
    بخش ۱۲۶ - پیروزی آتبین و بردن غنیمتها به دریا
---
# بخش ۱۲۶ - پیروزی آتبین و بردن غنیمتها به دریا

<div class="b" id="bn1"><div class="m1"><p>چو انبوه شد رزم و دید آتبین</p></div>
<div class="m2"><p>کجا چیره گشتند گردان چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزد ران و شبرنگ را تیز کرد</p></div>
<div class="m2"><p>ز خون گریزنده پرهیز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گروهی دلیران پس پشت او</p></div>
<div class="m2"><p>درفشان یکی تیغ بر دست او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهم برفگند آن سپه را چنان</p></div>
<div class="m2"><p>که نشناخت دشمن رکاب از عنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکشت اندر آن حمله چندان سوار</p></div>
<div class="m2"><p>که خون شد روان چون یکی جویبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب آمد ز کشتن کشیدند دست</p></div>
<div class="m2"><p>سپهدار چین باز پستر نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن رزم بُد آتبین شادکام</p></div>
<div class="m2"><p>ز بهر بزرگان می آورد و جام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بزم اندرون مهربانی نمود</p></div>
<div class="m2"><p>چو در رزم جوش جوانی نمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین گفت کامروز دادیم داد</p></div>
<div class="m2"><p>و زاین رزم پیروز گشتیم شاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر پیشم آید چنین صد سپاه</p></div>
<div class="m2"><p>چو بی کوش باشد ندارم به کاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولیکن چنین است ما را گمان</p></div>
<div class="m2"><p>که آن دیو چهره زمان تا زمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به ما گر سپاهی گران آورد</p></div>
<div class="m2"><p>از ایران بسی سروران آورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که داند که چون باشد این کار ِ زار</p></div>
<div class="m2"><p>که پیروز برگردد از کارزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگرچه بود تیز جنگی پلنگ</p></div>
<div class="m2"><p>نه هر بار پیروز باشد به جنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به سنگ ارچه ماند به سختی سبوی</p></div>
<div class="m2"><p>نه همواره باز آورندش ز جوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همان به که پیروز و آراسته</p></div>
<div class="m2"><p>به دریا برانیم با خواسته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همانا فزون است کشتی دویست</p></div>
<div class="m2"><p>بدین مرز بودن کنون روی نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزرگان پسندیده دیدند رای</p></div>
<div class="m2"><p>بر او خواندند آفرین خدای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان شب به کشتی کشیدند بار</p></div>
<div class="m2"><p>به دریا کشیدند با شهریار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روان گشت کشتی به یک ره دویست</p></div>
<div class="m2"><p>تو گفتی به دریا کنون راه نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو خورشید بر دشت لشکر کشید</p></div>
<div class="m2"><p>طلایه نگه کرد و کس را ندید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز رفتن سپه را چو آگاه کرد</p></div>
<div class="m2"><p>برآن سرکشان رنج کوتاه کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپه تا به نزدیک دریا بتاخت</p></div>
<div class="m2"><p>همی نعره از چرخ برتر فراخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز چیزی که بردند، بگذاشتند</p></div>
<div class="m2"><p>از او چینیان بهره برداشتند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدیشان رسیدند روز دهم</p></div>
<div class="m2"><p>جهانگیر کوش و سپاهش بهم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی دست بر دست زد از دریغ</p></div>
<div class="m2"><p>کز او آتبین یافت راه گریغ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سپاهش فرود آمد آن جایگاه</p></div>
<div class="m2"><p>برآسود یک روز از رنج راه</p></div></div>