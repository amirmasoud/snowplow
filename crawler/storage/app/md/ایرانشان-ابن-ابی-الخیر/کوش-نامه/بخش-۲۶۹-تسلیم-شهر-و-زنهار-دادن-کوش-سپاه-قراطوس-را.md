---
title: >-
    بخش ۲۶۹ - تسلیم شهر و زنهار دادن کوش سپاه قراطوس را
---
# بخش ۲۶۹ - تسلیم شهر و زنهار دادن کوش سپاه قراطوس را

<div class="b" id="bn1"><div class="m1"><p>بترسید مردم ز پیغام اوی</p></div>
<div class="m2"><p>بدادند هم در زمان کام اوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشادند شهر و شدندش به پیش</p></div>
<div class="m2"><p>همی هدیه ای ساخت هر یک ز خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراوان گوهر بر سرش ریختند</p></div>
<div class="m2"><p>بسی لابه و پوزش انگیختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپرسیدشان کوش و پس با سپاه</p></div>
<div class="m2"><p>به شهر اندر آمد به ایوان شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرای قراطوس پُر گنج دید</p></div>
<div class="m2"><p>که دست نویسنده بر رنج دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شتر خواست از دشت و مردان کار</p></div>
<div class="m2"><p>بفرمود تا ساربان کرد بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یک هفته گنج قراطوس شاه</p></div>
<div class="m2"><p>تهی کرد و برداشتش تاج و گاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هشتم به لشکرگه آورد گنج</p></div>
<div class="m2"><p>رها کرد در شهر مردان رنج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سواران شمشیر زن سی هزار</p></div>
<div class="m2"><p>چنین گفت با لشکری نامدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که بی مُهر و فرمان من زآن سپاه</p></div>
<div class="m2"><p>نباید که در شهر یابند راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپاه قراطوس را زآن سپس</p></div>
<div class="m2"><p>فرستاد با نامه ها چند کس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که دادم شما را به جان زینهار</p></div>
<div class="m2"><p>پیاده هر آن کس که هست و سوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به لشکرگه آرید خرگاه خویش</p></div>
<div class="m2"><p>به جان ایمن از تیغ بدخواه خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر پیش تو سرد گوید کسی</p></div>
<div class="m2"><p>ز ما خواری آید بر او بر بسی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر خویش و پیوند آن تیره هوش</p></div>
<div class="m2"><p>که گشتند با ما چنین سخت کوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشیدن سر از راه شاه بلند</p></div>
<div class="m2"><p>نگر، تا چه بار آورد جز گزند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ایشان گنهکار و بدگوهرند</p></div>
<div class="m2"><p>به پادافراه تیغ ما در خورند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو از تیغ کوش ایمنی یافتند</p></div>
<div class="m2"><p>به لشکرگه خویش بشتافتند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دگر روز گردنکشان و سپاه</p></div>
<div class="m2"><p>بسی هدیه بردند نزدیک شاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهادند رخسارگان بر زمین</p></div>
<div class="m2"><p>همی خواند هرکس بر او آفرین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزآن پس، چنین گفت هرکس که شاه</p></div>
<div class="m2"><p>بیامرزد این بار ما را گناه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قراطوس بود و نیاگان او</p></div>
<div class="m2"><p>نیاگان ما زیر فرمان او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه رای و نه آیین بد داشتیم</p></div>
<div class="m2"><p>که راه نیاگان خود داشتیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو گردن کشد بنده از شهریار</p></div>
<div class="m2"><p>نباشد پسندیده ی هوشیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنون چون چنین روی برتافت بخت</p></div>
<div class="m2"><p>از او بستد و شاه را داد تخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه بندگانیم بسته میان</p></div>
<div class="m2"><p>وگر پیش تختش سرآید زمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رمانش بی زفر ننهیم پای</p></div>
<div class="m2"><p>گر آمرزش آرَد به ما بر بجای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به رخ تازه شد کوش و بنواختشان</p></div>
<div class="m2"><p>به اندازه بر پایگه ساختشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از این بار، گفتا، شما را گناه</p></div>
<div class="m2"><p>ببخشودم و برنهادم به شاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر بار دیگر ز فرمان من</p></div>
<div class="m2"><p>بتابید گردن ز فرمان من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شما را بنزدیک من زینهار</p></div>
<div class="m2"><p>نباشد، نه آمرزش آرم به کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کنون شاد و خشنود گردید باز</p></div>
<div class="m2"><p>سراسر بدین لشکر آرید ساز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به چیزی که آن دیگری راست، دست</p></div>
<div class="m2"><p>نباید کشیدن چو بیهوش و مست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جز آن چیز کآن خود شما را بود</p></div>
<div class="m2"><p>مگر بند کز سنگ خارا بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو برداری آن را که بنهاده ای</p></div>
<div class="m2"><p>به بند اندری گرچه آزاده ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سپاه آفرین کرد و گشتند باز</p></div>
<div class="m2"><p>کشیدند نزدیک او رخت و ساز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وزآن پس به ایرانیان گفت شاه</p></div>
<div class="m2"><p>که دشمن کنون با شما گشت راه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برایشان نباید که خواری کنید</p></div>
<div class="m2"><p>همه خوبی و سازگاری کنید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بباشید با آن سپه یکدله</p></div>
<div class="m2"><p>نخواهم که باشد کسی را گله</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیامیخت آن هر دو لشکر بهم</p></div>
<div class="m2"><p>بهم بودشان شادمانی و غم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سراپرده ی دشمن و هرچه بود</p></div>
<div class="m2"><p>بدیشان ببخشید و خوبی نمود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زن و بچه و گنج او گاه خواب</p></div>
<div class="m2"><p>به کشتی فرستاد ازآن سوی آب</p></div></div>