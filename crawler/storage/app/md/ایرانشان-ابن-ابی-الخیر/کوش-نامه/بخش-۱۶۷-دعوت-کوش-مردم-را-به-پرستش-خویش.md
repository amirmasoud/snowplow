---
title: >-
    بخش ۱۶۷ - دعوت کوش مردم را به پرستش خویش
---
# بخش ۱۶۷ - دعوت کوش مردم را به پرستش خویش

<div class="b" id="bn1"><div class="m1"><p>جهاندیده گوید بدان روزگار</p></div>
<div class="m2"><p>نبوده ست کس بر ره کردگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهانی همه غمر و نادان و مست</p></div>
<div class="m2"><p>رها کرده راه و شده بت پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مردم به راه اندر آمد همی</p></div>
<div class="m2"><p>مر او را زمانه سر آمد همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگان دیندار را پیش خواند</p></div>
<div class="m2"><p>بسی پندها پیش ایشان براند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزآن پس چنان بد که آیین ما</p></div>
<div class="m2"><p>شما نیک دانید و هم دین ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس از ما همین راه دارید و کیش</p></div>
<div class="m2"><p>همان صورت ما نگاریده پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبینی که فرزند آدم چه گفت</p></div>
<div class="m2"><p>چه بیش آمد از راز ما در نهفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر از دانش خویش رانی سخن</p></div>
<div class="m2"><p>گشادنش نتوان به هیچ انجمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به اندازه ی دانش هر کسی</p></div>
<div class="m2"><p>همی گوی تا بد نبینی بسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر تازیانت نبینی چه گفت</p></div>
<div class="m2"><p>چو بگشاد راز از نهان و نهفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه هر جای جای سخن گفتن است</p></div>
<div class="m2"><p>نه کردار ما بهره ی هر تن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن را نگهداشت باید همی</p></div>
<div class="m2"><p>به هر جای گفتن نشاید همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو من راز گویم بدان سان که بود</p></div>
<div class="m2"><p>دو گوشت نداند شنودن، چه سود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پسندیده فرزند آدم چه گفت</p></div>
<div class="m2"><p>چنین گفت مرد و زن اندر نهفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که آیین ما پیش دارید و راه</p></div>
<div class="m2"><p>که تا بر شما دین نگردد تباه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برفتند وز سنگ بت ساختند</p></div>
<div class="m2"><p>دو صورت چو مردم بپرداختند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که شیث است این و دگر آدم است</p></div>
<div class="m2"><p>چنین داستان در خور ماتم است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پرستش نمودندشان سالیان</p></div>
<div class="m2"><p>برآمد یکی کافری زآن میان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز گیتی چو آن مردم اندر گذشت</p></div>
<div class="m2"><p>دل هر کس از راه ایشان بگشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی خواندند آن بتان را خدای</p></div>
<div class="m2"><p>به گیتی نمانْد هیچ کس رهنمای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>براین بود ضحّاک تازی نژاد</p></div>
<div class="m2"><p>که گفتیم و کردیم در نامه یاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز تازی اگر باز پرسیش نام</p></div>
<div class="m2"><p>ورا قیس لهبوب خواند مدام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر نام او پارسی پهلوی</p></div>
<div class="m2"><p>همی بیوراسب آید ار بگروی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برادرش را نام حفران نهاد</p></div>
<div class="m2"><p>چنین دارم از مرد گوینده یاد</p></div></div>