---
title: >-
    ۴۷- مولود ابوالقاسم پسر آقا سید محمدباقر
---
# ۴۷- مولود ابوالقاسم پسر آقا سید محمدباقر

<div class="b" id="bn1"><div class="m1"><p>دوش آمد جناب آقا را</p></div>
<div class="m2"><p>پوری از جود کردگار جواد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آسمانی عطاردی سر زد</p></div>
<div class="m2"><p>آفتابی ز چرخ دیگر زاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواست سال ولادتش ز رهی</p></div>
<div class="m2"><p>لله الحمد کش به وفق مراد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین دو مصرع برآمدش تاریخ</p></div>
<div class="m2"><p>بر سرجمع چون فزائی هاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقدم میرزا ابوالقاسم</p></div>
<div class="m2"><p>جاودان بر پدر همایون باد</p></div></div>
<div class="c"><p>۱۲۹۵ق</p></div>