---
title: >-
    ۸۰- تاریخ وفات آخوندملاقربانعلی جندقی
---
# ۸۰- تاریخ وفات آخوندملاقربانعلی جندقی

<div class="b" id="bn1"><div class="m1"><p>صاحب دین و دل آخوندآنکه الحق</p></div>
<div class="m2"><p>بود در جندق به عصر خود یگانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت رحلت بست سوی هشت رضوان</p></div>
<div class="m2"><p>بعد هفتاد و سه از این رنج خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت تاریخ وفاتش را صفایی</p></div>
<div class="m2"><p>نور بارد بر مزارش جاودانه</p></div></div>
<div class="c"><p>۱۲۸۲ق</p></div>