---
title: >-
    ۳۰- تاریخ وفات ابوالحسن یغما پدر شاعر
---
# ۳۰- تاریخ وفات ابوالحسن یغما پدر شاعر

<div class="b" id="bn1"><div class="m1"><p>اوستاد سخن ابوالحسن آنک</p></div>
<div class="m2"><p>بی سخن یک تنش نظیر نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هفت اقلیم را به نوک قلم</p></div>
<div class="m2"><p>قفل درهای نظم و نثر گشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چار گوهر یکی چو او به صفات</p></div>
<div class="m2"><p>قرن ها نارد از عدم به وجود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدح وی نیست حد ما زیرا</p></div>
<div class="m2"><p>که برون است مدحتش ز حدود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از سر شوق شد به پای رضا</p></div>
<div class="m2"><p>از سرای فنا به دار خلود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار الها به حق پاک نبی</p></div>
<div class="m2"><p>علت غائی اساس وجود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبب آفرینش همه خلق</p></div>
<div class="m2"><p>زیب بخشای ملک غیب و شهود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کآن ضعیف فتاده را از پای</p></div>
<div class="m2"><p>دستگیری کنی به حسن ورود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خامه عفو درکش از کف لطف</p></div>
<div class="m2"><p>بر گناهانش از فراز و فرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حشر فرمائیش به اهل وداد</p></div>
<div class="m2"><p>از در رحمت ای کریم و دود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرمرا هم به لطف خود می بخش</p></div>
<div class="m2"><p>زنده پیش از قضا چه دیر و چه زود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون به حکم مغایرت همه عمر</p></div>
<div class="m2"><p>فاش و پنهان ز خلق می فرسود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پی تاریخ وی صفائی گفت</p></div>
<div class="m2"><p>جان یغما ز نیک و بد آسود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>۱۲۷۶ق</p></div>