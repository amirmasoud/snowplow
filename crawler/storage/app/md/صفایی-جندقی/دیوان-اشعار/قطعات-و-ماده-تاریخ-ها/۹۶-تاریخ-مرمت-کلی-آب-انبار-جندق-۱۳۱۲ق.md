---
title: >-
    ۹۶- تاریخ مرمت کلی آب انبار جندق (۱۳۱۲ق)
---
# ۹۶- تاریخ مرمت کلی آب انبار جندق (۱۳۱۲ق)

<div class="b" id="bn1"><div class="m1"><p>داشت یغما قصد خطبی همه ثواب</p></div>
<div class="m2"><p>خواست آب انبار را پاس از خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به صفائی امر فرمود از نخست</p></div>
<div class="m2"><p>که مگیر این کار را زنهار سست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او هم از هر در که بودش پیشرفت</p></div>
<div class="m2"><p>سی و دو کوشید تا هفتاد و هفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنج بار از که گل اندودش نمود</p></div>
<div class="m2"><p>چار نوبت از لجن پاکش زدود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به امسالش که تعمیری به جا</p></div>
<div class="m2"><p>کرد بر وجهی که خود دیدش سزا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواست تاریخش وفائی از رهی</p></div>
<div class="m2"><p>بی غرض از روی صدق و فرهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد آب انبار چون مملو از آب</p></div>
<div class="m2"><p>دل شد از انده تهی راندم جواب</p></div></div>