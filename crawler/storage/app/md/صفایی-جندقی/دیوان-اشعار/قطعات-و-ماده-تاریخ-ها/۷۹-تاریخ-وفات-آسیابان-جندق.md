---
title: >-
    ۷۹- تاریخ وفات آسیابان جندق
---
# ۷۹- تاریخ وفات آسیابان جندق

<div class="b" id="bn1"><div class="m1"><p>آسیابان جندق از پس شصت</p></div>
<div class="m2"><p>ناوک مرگ را شد آماده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین سپنجی سرا گذشت و گذاشت</p></div>
<div class="m2"><p>همه اسباب را ز کف داده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر و طوق و تغاره و تبره</p></div>
<div class="m2"><p>تخت و احرام و چرخ و سنباده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر یکی فوت شد مگو تو زیاد</p></div>
<div class="m2"><p>این بنا را خدای بنهاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی را از او بگیر و بگوی</p></div>
<div class="m2"><p>آسیابش ز گردش افتاده</p></div></div>
<div class="c"><p>۱۲۹۷ق</p></div>