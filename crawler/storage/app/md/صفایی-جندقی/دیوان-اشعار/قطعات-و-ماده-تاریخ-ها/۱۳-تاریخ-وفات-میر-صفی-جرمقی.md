---
title: >-
    ۱۳- تاریخ وفات میر صفی جرمقی
---
# ۱۳- تاریخ وفات میر صفی جرمقی

<div class="b" id="bn1"><div class="m1"><p>چون میر صفی به ملک باقی</p></div>
<div class="m2"><p>از دار فنا نمود رحلت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسود خود از وصول و ما را</p></div>
<div class="m2"><p>فرسود به ابتلای هجرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذشت و گذاشت دوستان را</p></div>
<div class="m2"><p>تا حشر به سینه داغ حسرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاریخ وفاتش از صفائی</p></div>
<div class="m2"><p>درخواست هنر ز روی رحمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت آه و فرانگاشت فی الفور</p></div>
<div class="m2"><p>شد میرصفی به سوی جنت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>۱۲۷۲ق</p></div>