---
title: >-
    ۷- مرد سید شجاع و هرکه شنید
---
# ۷- مرد سید شجاع و هرکه شنید

<div class="b" id="bn1"><div class="m1"><p>مرد سید شجاع و هرکه شنید</p></div>
<div class="m2"><p>شکر آینده حمد ماضی گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بشارت سپاس های شگرف</p></div>
<div class="m2"><p>از خدا و رسول راضی گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی تاریخ این چس اندر قبر</p></div>
<div class="m2"><p>به جهنم که مرد قاضی گفت</p></div></div>
<div class="c"><p>۱۲۸۰ق</p></div>