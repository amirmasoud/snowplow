---
title: >-
    ۵۴- تاریخ رحلت ملاباشی
---
# ۵۴- تاریخ رحلت ملاباشی

<div class="b" id="bn1"><div class="m1"><p>داغ ملاباشی آن دانشورم</p></div>
<div class="m2"><p>سوخت دل در سینهٔ غم‌پرورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریخت بر خاک هبا آب نشاط</p></div>
<div class="m2"><p>داد بر باد هدر خاکسترم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برد از جان غم آگین رنگ جشن</p></div>
<div class="m2"><p>کاست در تاب تحسر پیکرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سال سوکش را یکی آمد به جمع</p></div>
<div class="m2"><p>گفت باد ای خاک عالم بر سرم</p></div></div>
<div class="c"><p>۱۲۸۳ق</p></div>