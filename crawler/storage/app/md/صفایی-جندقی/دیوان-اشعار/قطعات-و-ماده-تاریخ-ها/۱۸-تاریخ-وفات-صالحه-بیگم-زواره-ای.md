---
title: >-
    ۱۸- تاریخ وفات صالحه بیگم زواره ای
---
# ۱۸- تاریخ وفات صالحه بیگم زواره ای

<div class="b" id="bn1"><div class="m1"><p>به فردوس شد بیگم از کربلا</p></div>
<div class="m2"><p>خبر چو از وفاتش صفائی شنفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اندیشه درخواست تاریخ وی</p></div>
<div class="m2"><p>به جنات از نینوا رفت گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>۱۲۶۱ق</p></div>