---
title: >-
    ۶۸- مولود پسر میرزا عبدالحسین
---
# ۶۸- مولود پسر میرزا عبدالحسین

<div class="b" id="bn1"><div class="m1"><p>میرزا عبدالحسین آن مرد راد</p></div>
<div class="m2"><p>زیور دل زیب دین و آذین زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستش پیوسته در اعزاز و ناز</p></div>
<div class="m2"><p>دشمنش همواره در آشوب و شین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعمت دنیا و دین پنهان و فاش</p></div>
<div class="m2"><p>حق فرا پیش آردش قرب الیدین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی اکلیل ار فروزد چشم هوش</p></div>
<div class="m2"><p>در فزاید بر فروغ فرقدین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فضلش از شش سوی عالم گیر باد</p></div>
<div class="m2"><p>تا فراگیرد فضای خافقین ...</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سال مولودش صفائی برنگاشت</p></div>
<div class="m2"><p>زانکه بود این نکته بر وی فرض عین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد آن کودک الهی جاودان</p></div>
<div class="m2"><p>نور چشم میرزا عبدالحسین</p></div></div>
<div class="c"><p>۱۳۱۳ق</p></div>