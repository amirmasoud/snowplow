---
title: >-
    ۳۴- تاریخ ولادت سلیمان خوری
---
# ۳۴- تاریخ ولادت سلیمان خوری

<div class="b" id="bn1"><div class="m1"><p>دی سلیمان به حلق داودی</p></div>
<div class="m2"><p>با من آورد راز دل انشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مرا بر سرای تاریخی</p></div>
<div class="m2"><p>تا بدانند این خلف کی زاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواستم پاسخش ز خامه ی خویش</p></div>
<div class="m2"><p>دورها خورد تا جوابم داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کش صفائی به سال مه بنگار</p></div>
<div class="m2"><p>مقدمت بر پدر مبارک باد</p></div></div>
<div class="c"><p>۱۲۶۴ق</p></div>