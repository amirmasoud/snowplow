---
title: >-
    ۴۵- تاریخ وفات میرزا عبدالحسین جندقی
---
# ۴۵- تاریخ وفات میرزا عبدالحسین جندقی

<div class="b" id="bn1"><div class="m1"><p>رفت عبدالحسین و جندق را</p></div>
<div class="m2"><p>ساخت از مرگ خویش حزن آباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریخت آب قرارها بر خاک</p></div>
<div class="m2"><p>رفت نار وقارها بر باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواستیم از صفائیش تاریخ</p></div>
<div class="m2"><p>که بفرمای مصرعی انشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت برادر پای شرک و بگو</p></div>
<div class="m2"><p>میرزا را خدا بیامرزاد</p></div></div>
<div class="c"><p>۱۳۰۹ق</p></div>