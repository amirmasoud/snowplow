---
title: >-
    ۸۹- تاریخ وفات میرزا محمد خان سپهسالار
---
# ۸۹- تاریخ وفات میرزا محمد خان سپهسالار

<div class="b" id="bn1"><div class="m1"><p>چو ناکام ای سپهسالار ایران</p></div>
<div class="m2"><p>به عقبی رخت از دنیا کشیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اموردیگران فرصت ندادت</p></div>
<div class="m2"><p>به فکر کار خویش اکنون فتیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نار و جنت آنچت گفته بودند</p></div>
<div class="m2"><p>تمامش را به رأی العین دیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ما بود مشکوک آن سخن ها</p></div>
<div class="m2"><p>کمابیش آنچه صدقش را رسیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تاریخ وفاتت صرف امید</p></div>
<div class="m2"><p>صفایی گفت از غم ها رهیدی</p></div></div>