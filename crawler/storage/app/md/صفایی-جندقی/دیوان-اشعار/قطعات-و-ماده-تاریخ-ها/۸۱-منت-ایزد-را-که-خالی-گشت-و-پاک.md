---
title: >-
    ۸۱- منت ایزد را که خالی گشت و پاک
---
# ۸۱- منت ایزد را که خالی گشت و پاک

<div class="b" id="bn1"><div class="m1"><p>منت ایزد را که خالی گشت و پاک</p></div>
<div class="m2"><p>کفش اصفاهان ز ریگ شیره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از اجاق کامرانی در کرند</p></div>
<div class="m2"><p>سرنگون گردید دیگ شیره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نهیب ترک تاز خیل مرگ</p></div>
<div class="m2"><p>در هزیمت شد چریک شیره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر وارث یک جهان نفرین و لعن</p></div>
<div class="m2"><p>ماند بر جا مرده ریگ شیره‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت کیهان جاودان بیدار باد</p></div>
<div class="m2"><p>تا به خواب مرگ دیگ شیره‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامزد شد هر غذائی خاص را</p></div>
<div class="m2"><p>پای تا سر دیگ دیگ شیره‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اول و ثانی به حکم اتحاد</p></div>
<div class="m2"><p>در جحیم آمد شریک شیره‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سال مرگش را صفائی زد رقم</p></div>
<div class="m2"><p>چاک آمد... خیک شیره‌ای</p></div></div>