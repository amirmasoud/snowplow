---
title: >-
    ۴۳- تاریخ وفات مرحوم مصطفی
---
# ۴۳- تاریخ وفات مرحوم مصطفی

<div class="b" id="bn1"><div class="m1"><p>مصطفی از جهان چو رخت رحیل</p></div>
<div class="m2"><p>بست و بگشود پا به سیر معاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی تاریخ وی صفائی گفت</p></div>
<div class="m2"><p>با رضا مشفق خجسته نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان آر پای عفو و بگو</p></div>
<div class="m2"><p>مصطفی را خدا بیامرزاد</p></div></div>
<div class="c"><p>۱۳۰۶ق</p></div>