---
title: >-
    ۱۹- تاریخ بنای بالاخانه و برج بهجت
---
# ۱۹- تاریخ بنای بالاخانه و برج بهجت

<div class="b" id="bn1"><div class="m1"><p>ز مشکوی بهشت آئین یغما</p></div>
<div class="m2"><p>صفائی را رسید اینک بشارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رقم کردش پی تاریخ انجام</p></div>
<div class="m2"><p>مبارک باد بر وی این عمارت</p></div></div>
<div class="c"><p>۱۲۶۰ق</p></div>