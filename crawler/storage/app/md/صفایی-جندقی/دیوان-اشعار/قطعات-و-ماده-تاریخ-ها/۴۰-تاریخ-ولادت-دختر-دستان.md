---
title: >-
    ۴۰- تاریخ ولادت دختر دستان
---
# ۴۰- تاریخ ولادت دختر دستان

<div class="b" id="bn1"><div class="m1"><p>دختی آمد جناب دستان را</p></div>
<div class="m2"><p>که به حسنش جهان ندیده ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی تاریخ وی صفائی گفت</p></div>
<div class="m2"><p>آفتابش ز برج زهره دمید</p></div></div>
<div class="c"><p>۱۲۷۲ق</p></div>