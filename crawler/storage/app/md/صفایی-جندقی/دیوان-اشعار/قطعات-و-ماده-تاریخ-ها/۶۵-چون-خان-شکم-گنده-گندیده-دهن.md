---
title: >-
    ۶۵- چون خان شکم گنده گندیده دهن
---
# ۶۵- چون خان شکم گنده گندیده دهن

<div class="b" id="bn1"><div class="m1"><p>چون خان شکم گنده گندیده دهن</p></div>
<div class="m2"><p>از ساحت سمنان به سقر سخت سکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتند به سال مرگش ارباب سخن</p></div>
<div class="m2"><p>پر کرد جهنم را از گنده تن</p></div></div>
<div class="c"><p>۱۲۶۲ق</p></div>