---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>هر که آید به سیر جولانت</p></div>
<div class="m2"><p>نبرد ره برون ز میدانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ما را به حلقه ی گیسوی</p></div>
<div class="m2"><p>هست زندان به از گلستانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز از آن چهر هر نظر دارد</p></div>
<div class="m2"><p>صد گلستان به کنج زندانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه بندد دلت به چنبر زلف</p></div>
<div class="m2"><p>غافل است از چه زنخدانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خلقی ز دست بردی و نیست</p></div>
<div class="m2"><p>خطره ای ز احتساب سلطانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز دیوان شه نداری باک</p></div>
<div class="m2"><p>باری اندیشه ای ز یزدانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساخت کار دو عالم از چپ و راست</p></div>
<div class="m2"><p>تیغ ابروی و تیر مژگانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتی ای دل به گوشه ای که مگر</p></div>
<div class="m2"><p>مشکل عشق گردد آسانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدی آخر که احتمال شکیب</p></div>
<div class="m2"><p>با غم او نبود امکانت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون صفایی ضرورت است به جان</p></div>
<div class="m2"><p>احتمال جفای جانانت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جاودان جز به درد خو کردن</p></div>
<div class="m2"><p>مکن ارمان که نیست درمانت</p></div></div>