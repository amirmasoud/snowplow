---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>جز از لب جانانه که چندین شکر آید</p></div>
<div class="m2"><p>کشنید که تنگ شکر از لعل تر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا صدق نباشد چه اثر ناله ی کس را</p></div>
<div class="m2"><p>نادر بود ار تیره خطا کارگر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پیکر خارا سر خار ار بتوان کوفت</p></div>
<div class="m2"><p>حاشا که به دل آه منت پی سپر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افغان مرا در دل سندان تو ره نیست</p></div>
<div class="m2"><p>کاری مگر از ناله ی صاحب اثر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر نایبه ی عشق نپاید مگر آن دل</p></div>
<div class="m2"><p>کز روی رضا تیغ بلا را سپر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در راه وفای تو دیغ از سر و جان نیست</p></div>
<div class="m2"><p>تحصیل مراد تو گر از جان و سر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عشق تو یک دور به پایان شد و غم نیست</p></div>
<div class="m2"><p>عمی همه شادم که بدین دست سر آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برتابد اگر در شب تارم رخ چون صبح</p></div>
<div class="m2"><p>چون روز شبم روشن و شامم سحر آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پامال تو خواهد سر و تن بی همه افسوس</p></div>
<div class="m2"><p>از دست صفایی اگر این کار برآید</p></div></div>