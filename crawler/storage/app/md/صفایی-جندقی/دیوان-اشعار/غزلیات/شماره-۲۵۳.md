---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>کاش مغزی داشتی تا جای خاک</p></div>
<div class="m2"><p>پیر دهقان سرفکندی پای تاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نبودی سفله پرور چرخ دون</p></div>
<div class="m2"><p>غیر رز هرگز نروییدی زخاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برده دل ها در ید آن شوخ چشم</p></div>
<div class="m2"><p>مست را اری چه باک از انتهاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوش تا نبود ز بدنامی چه ننگ</p></div>
<div class="m2"><p>عقل تا نبود ز رسوایی چه باک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازم آن قاتل که با چندین قتیل</p></div>
<div class="m2"><p>دامنش ز آلایش خون است پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نگردد متهم در خون من</p></div>
<div class="m2"><p>گو به بالینم بیا بعد از هلاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان و تن وقف تو شد قلبی لدیک</p></div>
<div class="m2"><p>دین و دل رهن تو شد روحی فداک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آستین بر اشک ما مفکن که نیست</p></div>
<div class="m2"><p>این علاج سینه های زخمناک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به تلبیس از تو پوشم حال دل</p></div>
<div class="m2"><p>چیست حالی چاره این جیب چاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داغ جانان بر جبین من به حشر</p></div>
<div class="m2"><p>چون صفایی را برآرند از مغاک</p></div></div>