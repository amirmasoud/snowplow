---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>بنگر به تیره روزی من در شمار هجر</p></div>
<div class="m2"><p>نوری فشان ز وصل به شبهای تار هجر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند به زنجیری و با صیقل وصال</p></div>
<div class="m2"><p>ز آیینه ی دلم ننشانی غبار هجر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغان به باغ نغمه سراسر خوش از وصول</p></div>
<div class="m2"><p>تبدیل کند تو هم به گل وصل خار هجر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنعت مران به نقص کفایت همی مرا</p></div>
<div class="m2"><p>گر بستم از وثاق وصال تو بار هجر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دست من به عنف برون شد زمام وصل</p></div>
<div class="m2"><p>چو نانکه در کف تو نبود اختیار هجر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد باز وقت آنکه علی رغم مدعی</p></div>
<div class="m2"><p>بسپارمت ز بارگی وصل بار هجر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با قرب دادها برم از امتداد بعد</p></div>
<div class="m2"><p>در وصل شکوه ها کنم از روزگار هجر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کارم تمام خواست رقیب از فراق و من</p></div>
<div class="m2"><p>دیدی چگونه ساختم از وصل کار هجر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستم رسد اگر به میان وصال باز</p></div>
<div class="m2"><p>کامل نهم جزای عمل در کنار هجر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یابی ره وصول صفایی به بزم قرب</p></div>
<div class="m2"><p>برگردد ار طبیعت ناسازگار هجر</p></div></div>