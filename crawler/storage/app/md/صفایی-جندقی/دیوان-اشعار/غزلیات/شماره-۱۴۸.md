---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>کس نیست کش از قصه ی دل راز توان کرد</p></div>
<div class="m2"><p>شرحی ز غم دوست بدو باز توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انجام ندارد خبر عشق چه پرسی</p></div>
<div class="m2"><p>این نیست بیانی که خود آغاز توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نیست ز بی بال و پری طایر دل را</p></div>
<div class="m2"><p>پنداشت که از قید تو پرواز توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز دل که مرا در شکن زلف تو آسود</p></div>
<div class="m2"><p>گنجشک کجا همدم شهباز توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وی نه چنان بسته تعلق که به زنجیر</p></div>
<div class="m2"><p>از موی تو خوی دل ما باز توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم رسم تو صیاد رسن تاب توان گفت</p></div>
<div class="m2"><p>هم اسم وی استاد رسن باز توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک ره ندهی کام من از لعل خود آخر</p></div>
<div class="m2"><p>انصاف کجا شد چقدر ناز توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چند به خاک اشک زمین گرد توان ریخت</p></div>
<div class="m2"><p>تاچند به چرخ آه فلک تاز توان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون زاد و غمم راحله ره آه و رفیق اشک</p></div>
<div class="m2"><p>از خاک درت برگ سفر ساز توان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با شرط تمامی مه تابان فلک را</p></div>
<div class="m2"><p>نسبت نه بدان سرو سرافراز توان کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با خویش مکن نیز صفایی غم دل فاش</p></div>
<div class="m2"><p>در برزخ بیگانه کجا باز توان کرد</p></div></div>