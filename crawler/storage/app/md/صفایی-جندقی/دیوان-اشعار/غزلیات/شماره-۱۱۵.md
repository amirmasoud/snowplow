---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>یار در طرف چمن پرده ز رخسارگرفت</p></div>
<div class="m2"><p>عرق شرم ز روی گل وگلنار گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه از نازش قد صولت شمشاد شکست</p></div>
<div class="m2"><p>گاه ازتابش خد رونق گلزار گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب از دیده ی دیوانه و عاقل برداشت</p></div>
<div class="m2"><p>تاب از سینه ی آزاد و گرفتار گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قیام از همه جا شور قیامت انگیخت</p></div>
<div class="m2"><p>به خرام از همه کس قوت رفتار گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهر و زلفش نه ز من سبحه و سجاده ربود</p></div>
<div class="m2"><p>کز کف و کتف برهمن بت و زنار گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه دید آن بت لیلی شکر شهر آشوب</p></div>
<div class="m2"><p>بی خود از خانه چو مجنون ره کهسار گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمش ار برد به شوخی دل مردم نه عجب</p></div>
<div class="m2"><p>عجب آن است که مست آمد وهشیار گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در رهش گر سر و جان رفت میندیش دلا</p></div>
<div class="m2"><p>می توان داد جهانی که چنین یار گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرح تیمار صفایی قلم از دوده نگاشت</p></div>
<div class="m2"><p>باز دود دلش از خامه ی و طومار گرفت</p></div></div>