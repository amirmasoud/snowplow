---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>مردم عوض اشک فتد دیده ی تر را</p></div>
<div class="m2"><p>تا از نظر انداخته ای اهل نظر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زاری ما شد به جفا سخت ترش دل</p></div>
<div class="m2"><p>وین طرفه که باران نکند نرم حجر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با اشک من از دجله مگو باز که با بحر</p></div>
<div class="m2"><p>هرگز بشماری نشمارند شمر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم نشد از ناله من سخت دلت نرم</p></div>
<div class="m2"><p>گفتا نه اثر نیست در این خاره شرر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل نگذرد از ابروی آن ترک کمان کش</p></div>
<div class="m2"><p>ز آن رو که ز شمشیر گذر نیست سپر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دیده مردم ز چه هر روز نهان است</p></div>
<div class="m2"><p>بر مهر رخت غیرت اگر نیست قمر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خجلت دندان بتان بسکه عرق ریخت</p></div>
<div class="m2"><p>یک بحر ز سر آب گذشته است گهر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش دهن او دهن غنچه خود از شرم</p></div>
<div class="m2"><p>وامانده گل آسا چه کند باد سحر را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جایش به فراخای جهان تنگ نمی بود</p></div>
<div class="m2"><p>گرتلخ نکردی لبت اوقات شکر را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گو آتش عشق تو به بادم دهد ای دوست</p></div>
<div class="m2"><p>با خاک درت می نخرم آب خضر را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی رحم تر آمد دلت از آه صفایی</p></div>
<div class="m2"><p>شک نیست که در سنگ تو ره نیست اثر را</p></div></div>