---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>ای در دلم زکاوش عشق تو خارها</p></div>
<div class="m2"><p>دستان سرای گلشن حسنت هزارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رشک سرو قامت شمشاد سایه ات</p></div>
<div class="m2"><p>سیلاب اشک می رود از جویبارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بویی مگر ز زلف توآرد برای وی</p></div>
<div class="m2"><p>دارد چمن به راه صبا انتظارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ره برند بر سر کویت بهار و دی</p></div>
<div class="m2"><p>پهلو تهی کنند ز گلشن هزارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازم به چهره و زلف تو کز حسن رنگ و بوی</p></div>
<div class="m2"><p>آزرم تبت آمد و شرم تتارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن کشان به خاک شهیدان چو بگذری</p></div>
<div class="m2"><p>آید هزار دست برون از مزارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه لابه گاه گریه گهی ناله گه خروش</p></div>
<div class="m2"><p>عمری به بوی وصل تو کردم چه کارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما درمیان موج چنین قلزمی غریق</p></div>
<div class="m2"><p>خلقی به ما نظاره کنان ازکنارها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتاج تیپ غمزه و توپ دو زلف نیست</p></div>
<div class="m2"><p>آن کو به یک اشاره گشاید حصارها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوشین لب تو چیست میان سواد خط</p></div>
<div class="m2"><p>شهدی که مور بسته به دورش قطارها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خالش به غمزدگان کند اغرای دلبری</p></div>
<div class="m2"><p>در حکم یک پیاده ی او بین سوارها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک بار اگر به فرق صفایی نهی قدم</p></div>
<div class="m2"><p>از جان و سر به پای تو ریزد نثارها</p></div></div>