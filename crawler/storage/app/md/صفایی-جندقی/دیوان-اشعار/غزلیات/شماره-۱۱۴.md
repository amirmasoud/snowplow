---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>خجلت مشتری آن مه ره بازار گرفت</p></div>
<div class="m2"><p>مهر و مه را به یکی جلوه خریدار گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد کی نسیه کجا مفت نه چون گفت به چند</p></div>
<div class="m2"><p>دل و دین همه بی درهم و دینار گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیش و کم جان به تن بنده و آزاد فزود</p></div>
<div class="m2"><p>پیش و پس دل ز کف خفته و بیدار گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجر خون خوار تو تب بر تن بی تاب گماشت</p></div>
<div class="m2"><p>شوق دیدار تو صبر از دل بیمار گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشدم غیرت آمیزش با اغیارت</p></div>
<div class="m2"><p>غیر گل چون تو که الفت همه با خار گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن سرگشته ز خاک قدمت دور مباد</p></div>
<div class="m2"><p>که دلم جای در آن زلف نگون سار گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شبی وصل تو بهبود نشد روزی ما</p></div>
<div class="m2"><p>بیش از اینها به فراق دلم آزار گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مداوای حکیمش که کند چاره ی درد</p></div>
<div class="m2"><p>هرکه مانند من از عشق تو تیمار گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خشک و تر خامه و اوراق به هم خواهد سوخت</p></div>
<div class="m2"><p>ز آتش طبع صفایی که در اشعار گرفت</p></div></div>