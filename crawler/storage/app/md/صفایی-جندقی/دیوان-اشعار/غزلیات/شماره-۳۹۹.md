---
title: >-
    شمارهٔ ۳۹۹
---
# شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>بدین اخلاق و اوصاف خدایی</p></div>
<div class="m2"><p>ترا نبود گریز از دل ربایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دام و آشیان مرغ غمت راست</p></div>
<div class="m2"><p>بسی بهتر اسیری از رهایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از قیدم رهان کاین رشته برپای</p></div>
<div class="m2"><p>ندارد فرق بندی یا گشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوی تر بر وفایت عهد بستم</p></div>
<div class="m2"><p>ز خوبان هر چه دیدم بی وفایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکیبایی مدار از ما تمنا</p></div>
<div class="m2"><p>گذشت از صبر کارم در جدایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه دولت ها دهد دستم که یکبار</p></div>
<div class="m2"><p>به سر وقتم به پای پرسش آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو زلفت بار غم در هم شکستم</p></div>
<div class="m2"><p>مرا ز آن حقه باید مومیایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرم بر عرش ساید گر کند باز</p></div>
<div class="m2"><p>به چشمم خاک پایت توتیایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا باشد شفا از آن لب تو مپسند</p></div>
<div class="m2"><p>که من گردم هلاک از بی دوایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دوزخ رفتنش مشکل نباشد</p></div>
<div class="m2"><p>توگر همراه باشی با صفایی</p></div></div>