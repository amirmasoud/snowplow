---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>چشم و ابرویش به قهرم دل رباید</p></div>
<div class="m2"><p>گوهر و لعلش به لطفم جان فزاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آن دو تلخی ها به کارم برد و خود را</p></div>
<div class="m2"><p>زین دو صد چندان به شیرینی ستاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دو رنگ رامش از نفسم ستاند</p></div>
<div class="m2"><p>این دو رنگ انده از عقلم زداید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن به رخ ابواب تقدیمم فرازد</p></div>
<div class="m2"><p>وین به گوش آیات تکریمم سراید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دو با دل شق گمانی می سگالد</p></div>
<div class="m2"><p>این دو با جان مهربانی می نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن از آن تا پای دارد می گریزد</p></div>
<div class="m2"><p>جان به این تا جای بیند می گراید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست گر قصد جفا آن بی وفا را</p></div>
<div class="m2"><p>از چه این را بست و آن را می گشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای تا سر دل نشین آمد دریغا</p></div>
<div class="m2"><p>کز مناعت عهد یاری می نیاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل به وصلم باز جو کز رنج هجران</p></div>
<div class="m2"><p>بیش از این بالله مگر خوردن نشاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه فرمایی بپیچم سر ز فرمان</p></div>
<div class="m2"><p>جز شکیبایی که هیچ از من نیاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر منت دل سوختی با وصف سختی</p></div>
<div class="m2"><p>گر ز حالم با تو کس رمزی سراید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در تو کافر دل تولای صفایی</p></div>
<div class="m2"><p>هرکه بیند پای تا سر حیرت آید</p></div></div>