---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>جز کلبه ی عشاق تو بیت الحزنی نیست</p></div>
<div class="m2"><p>وین فرق که برما گذر از پیرهنی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درباره ی یوسف نکنم عیب زلیخا</p></div>
<div class="m2"><p>پیروز جوانی که کم از پیر زنی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد صف مژگان تو باشد مگر آن چشم</p></div>
<div class="m2"><p>ز آنرو که ترا بهتر از این صف شکنی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدکور خرابات چنان امن که در وی</p></div>
<div class="m2"><p>جز غمزه خون ریز بتان راهزنی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرکشته ام ازکوی تو بیرون نبردکس</p></div>
<div class="m2"><p>دیگر به توجز کشتن خویشم سخنی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شادم به شهادت که مرا بهر مباهات</p></div>
<div class="m2"><p>از سایه ی دیوار تو بهتر کفنی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شه را خبر از حال گدا نیست علی الرسم</p></div>
<div class="m2"><p>دانم چوتویی را غم مانند منی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتم به جوانی و هنوز از غمت افسوس</p></div>
<div class="m2"><p>کافسانه ی ما قصه ی هر انجمنی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین دست که امروز خوری خون عزیزان</p></div>
<div class="m2"><p>فرداست که در شهر ز عشاق تنی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بستی دلما را به رخ آویز خم زلف</p></div>
<div class="m2"><p>صیدی چو صفایی قفسش در چمنی نیست</p></div></div>