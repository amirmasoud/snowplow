---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>در گلستان رخ ار گشاید بار</p></div>
<div class="m2"><p>گلبن اید خجل ز روی هزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک بیزد صبا به فرق چمن</p></div>
<div class="m2"><p>چون کشدپرده ماهم از رخسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شد از چشم و لب نهان و پدید</p></div>
<div class="m2"><p>روز و شب باده بخش و باده گمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ز رخسار و خط بهم پیوست</p></div>
<div class="m2"><p>انگبین با کبست وگل با خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان لب و چشم دیده ها خون ریز</p></div>
<div class="m2"><p>زان خط و چهر سینه ها افگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب و زلفش بتی است در زنجیر</p></div>
<div class="m2"><p>رخ و خطش مهی است در زنجار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بدین سان بود کشاکش حسن</p></div>
<div class="m2"><p>جان بدر ناورد یکی ز هزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر این است موج قلزم عشق</p></div>
<div class="m2"><p>عجب ار کشتی ای رسد به کنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل صفایی دگر به کف ناید</p></div>
<div class="m2"><p>برد او را نبودمی انکار</p></div></div>