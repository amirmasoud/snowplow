---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>تن از تیمار عشقم مانده بیمار</p></div>
<div class="m2"><p>توچندی بایدم پایی پرستار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی پرسش به سر وقتم شتابی</p></div>
<div class="m2"><p>اگر گردی ز احوالم خبر دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا بنگر که چون این زار مهجور</p></div>
<div class="m2"><p>به بستر خفته با یک عالم آزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شیرین شربت آن لعل نوشین</p></div>
<div class="m2"><p>ز بس نوشیدم آمد طبع من حار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عناب و سپستان تو باید</p></div>
<div class="m2"><p>به تبریدم دوایی برد درکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر زان لب شفا جویم وگویی</p></div>
<div class="m2"><p>نخواهم برد جان زین تاب و تیمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل آنجا با وصالت رفته از دست</p></div>
<div class="m2"><p>تن اینجا در فراقت مانده از کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل آنجا با خم زلفت هم آغوش</p></div>
<div class="m2"><p>من اینجا با غم هجرت گرفتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم واپس ده ار خونم نریزی</p></div>
<div class="m2"><p>صفایی را از این سودا برون آر</p></div></div>