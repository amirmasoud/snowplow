---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>تا سیر سرو و سوری آن سیم تن کنم</p></div>
<div class="m2"><p>حاشا که یاد سرو و هوای سمن کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کین که می کشد فلک از من ولی دگر</p></div>
<div class="m2"><p>محکم به مهر آن مه پیمان شکن کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد پیرهن قبا کنم امروز تا شبی</p></div>
<div class="m2"><p>با دوست جایگه به یکی پیرهن کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک در تو گردم اگر آسمان ز رشک</p></div>
<div class="m2"><p>راضی شود که بر سر کویت وطن کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندان به زخم تیغ تو شادم که وقت نیست</p></div>
<div class="m2"><p>تا فکر چاره دل خونین بدن کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دست دلبر است دل اما قیاس حال</p></div>
<div class="m2"><p>از درد و داغ لاله گلگون کفن کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو فرصتی که با غم عشق و خیال دوست</p></div>
<div class="m2"><p>تدبیر حال شیفته خویشتن کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد جام زهر بی تو فرو ریزدم به کام</p></div>
<div class="m2"><p>هر دم که یاد آن لب نوشین دهن کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دل قدم برون نهد از خط بندگیت</p></div>
<div class="m2"><p>بر گردنش از آن خم گیسو رسن کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان خار تن صفایی و تن خاک راه باد</p></div>
<div class="m2"><p>با او اگر مضایقه از جان و تن کنم</p></div></div>