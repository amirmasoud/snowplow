---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای آتش از رخت به درون لاله زار را</p></div>
<div class="m2"><p>صد داغ از بهشت تو بر دل بهار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر گل تو رخصت نالیدنش دهند</p></div>
<div class="m2"><p>از شوق ناله جان به لب آید هزار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاری به دل خلیده مپندار بی جهت</p></div>
<div class="m2"><p>در مرغزار ناله این مرغ زار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانی کجاست تا به معانی نظر کند</p></div>
<div class="m2"><p>در طلعت تو صنعت صورت نگار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی میم از آن لب شیرین چنانکه کرد</p></div>
<div class="m2"><p>مستغنی از شراب و شکر باده خوار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دوستی جفای تو سهل است چون کنم</p></div>
<div class="m2"><p>بد عهدی زمانه ناسازگار را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک محیط زا چو گذشت از گهر چه فرق</p></div>
<div class="m2"><p>فرقی اگر ز لجه ندانم کنار را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن به موج دیده کنم شرم زنده رود</p></div>
<div class="m2"><p>تا جای زیبد این لب جو سرو یار را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل به لعبتی چه درافتی که ترک وی</p></div>
<div class="m2"><p>از یک نگه پیاده کند صد سوار را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا در صفات یار صفایی نکو رسید</p></div>
<div class="m2"><p>نشناخت سر حکمت پروردگار را</p></div></div>