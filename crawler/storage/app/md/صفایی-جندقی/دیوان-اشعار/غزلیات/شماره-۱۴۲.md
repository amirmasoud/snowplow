---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>صبا یک عقده از جعد تو وا کرد</p></div>
<div class="m2"><p>هوا را عطر بیز و مشک سا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر خا لعلت از نوشین تبسم</p></div>
<div class="m2"><p>دهن نگشوه مشت غنچه وا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل از تشویر رویت هر سحرگاه</p></div>
<div class="m2"><p>گریبان شکیبایی قبا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دستم نیم جانی هست و در پات</p></div>
<div class="m2"><p>خورم حسرت که نتوانم فدا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحمدالله که هجرم گشت وآسود</p></div>
<div class="m2"><p>خوشم فارغ ز فکر خون بها کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشارت باد اعدا را که محبوب</p></div>
<div class="m2"><p>همه بیگانگی با آشنا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کیش کفر و دین منت روا نیست</p></div>
<div class="m2"><p>اگر کس حاجتی از کس روا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جان صد درد بی درمانم افزود</p></div>
<div class="m2"><p>ز دل گر سفله یک دردم دوا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملامت نیست برشاهی که گاهی</p></div>
<div class="m2"><p>رعایت از گدایی بینوا کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو مفتی می خورد خود خون ایتام</p></div>
<div class="m2"><p>چرا نهی من ازاکل ربا کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گزند دوستان مپسند زین بیش</p></div>
<div class="m2"><p>به دشمن کی توان چندین جفا کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترا داد آنکه این حسن صفا خیز</p></div>
<div class="m2"><p>صفایی را عجب عشقی عطا کرد</p></div></div>