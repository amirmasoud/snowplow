---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>هزار کام ز لعل تو یک دقیقه برآید</p></div>
<div class="m2"><p>مرا که هیچ کلیدی گره ز دل نگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جزای حسن عمل پس مرا همان بر و بالا</p></div>
<div class="m2"><p>که بی تو طوبی و فردوس جز غمم نفزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثواب طاعت خویش از خدای جز تو نخواهم</p></div>
<div class="m2"><p>خوش است نعمت باقی و لیک بی تو نشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز طلعت تو نبندم نظر به عارض رضوان</p></div>
<div class="m2"><p>وگر هزار در از روضه ام به روی گشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ذوق نوش لبت کوثرم نماند تمنا</p></div>
<div class="m2"><p>مریض عشق ترا شربت از زلال تو باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هفت کشوم ار سروری دهند چه حاصل</p></div>
<div class="m2"><p>که بی تو هشت بهشتم دو جو به کار نیاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به ساحت جنت بدین جمال درآیی</p></div>
<div class="m2"><p>کس از حضور تو هرگز به حور عین نگراید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر به گلشن رضوان ریاض رخ بگشایی</p></div>
<div class="m2"><p>ز شرم بلبل مینو دگر به گل نسراید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب وصال ملولم به یاد روز جدایی</p></div>
<div class="m2"><p>مگر به مردنم این طرفه زندگی به سر آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مزن تو رای علاجم که غیر یار صفایی</p></div>
<div class="m2"><p>کسی غبار غم از لوح خاطرم نزداید</p></div></div>