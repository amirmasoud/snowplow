---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>ای دل امشب چاره هجران به مردن می کنم</p></div>
<div class="m2"><p>آخر این دشوار را آسان به مردن می کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق را درمان به هجران هجر را درمان به صبر</p></div>
<div class="m2"><p>کردم اینک صبر را درمان به مردن می کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوست فرمانم به هجران داد و هجرانم به مرگ</p></div>
<div class="m2"><p>ناگزیر امضای این فرمان به مردن می کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دم از نو بهر ما اندیشه آزاری چرا</p></div>
<div class="m2"><p>نفی این غم از دل جانان به مردن می کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جدایی شد شکیبایی ز مرگم صعب تر</p></div>
<div class="m2"><p>سلب یک گردون ملال از جان به مردن می کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان ندارم بی تو و خوانند خلقم زنده باز</p></div>
<div class="m2"><p>از خود اکنون رفع این بهتان به مردن می کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه بر دل تنگ شد چون جایگه برتن مرا</p></div>
<div class="m2"><p>جان خویش آزاد ازین زندان به مردن می کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راز عشق دوست کز دشمن نهفتم سال ها</p></div>
<div class="m2"><p>آشکارا بر سر میدان به مردن می کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زندگی را در قفا جز مرگ نبود دیر و زود</p></div>
<div class="m2"><p>مشق عمر جاودانی زان به مردن می کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نالش سر پنجه ی هجرم چنان در هم شکست</p></div>
<div class="m2"><p>کش صفایی ناگزر جبران به مردن می کنم</p></div></div>