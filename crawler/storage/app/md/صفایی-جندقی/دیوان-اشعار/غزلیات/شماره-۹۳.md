---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>با دو ابروی توام بر دل شمشیر چیست</p></div>
<div class="m2"><p>پیش آن مژگان مرا در دیده نوک تیر چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من به ذوق جان سپاری و تو خون ریزیت کار</p></div>
<div class="m2"><p>آفت تعجیل چبود علت تأخیر چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف لیلی بند برگردن گذارد عقل را</p></div>
<div class="m2"><p>در دل مجنون من سودای این زنجیر چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ننالیدم گهی بر سر مرا می بود پای</p></div>
<div class="m2"><p>جز تغافل سود افغان من از تأثیر چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز ترکش مهر جو غافل ز کین غمزگان</p></div>
<div class="m2"><p>در کفش صد قبضه خنجر فکر این نخجیر چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست عادت چون تو ای دل با گرفتاری مرا</p></div>
<div class="m2"><p>بهر استخلاص این قیدم بگو تدبیر چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غمت زین چشم و دل جز دامن تر کام خشک</p></div>
<div class="m2"><p>سود اشک شامگاه و نالهٔ شبگیر چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن دو قوت داد جان را وین درآمد قوت تن</p></div>
<div class="m2"><p>پیش آن یاقوت و گوهر می چه باشد شیر چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او غیوری سخت دل من ناصبوری سست جان</p></div>
<div class="m2"><p>با جوانی همچو او سودای چون من پیر چیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فتنه ی جان جهانی گر نبودی از نخست</p></div>
<div class="m2"><p>عشق ما بر یک طرف این حسن عالمگیر چیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در هجوم غم صفایی رامش از دل رفت و گفت</p></div>
<div class="m2"><p>این خراب‌آباد را خاصیت تعمیر چیست</p></div></div>