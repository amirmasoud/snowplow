---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>تا فتنه ی قامت توبرخاست</p></div>
<div class="m2"><p>شد هر طرفی قیامتی راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان خاست و زین نشست لابد</p></div>
<div class="m2"><p>بنشست امان و فتنه برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که تویی غریب نبود</p></div>
<div class="m2"><p>در مرد وزن ار غریو و غوغاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعظیم قد ترا به ننشست</p></div>
<div class="m2"><p>هر سرو که در چمن به پا خاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا حرف تو در میانه آمد</p></div>
<div class="m2"><p>آشوب میان پیر و برناست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنان که بلای عشق دیده اند</p></div>
<div class="m2"><p>دانند که حق به جانب ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خود به غم تو شادم ای دوست</p></div>
<div class="m2"><p>دل را چکنم که ناشکیباست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر شب که نه با توام به سر رفت</p></div>
<div class="m2"><p>از رنگ رخم چو روز پیداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار است و خسک به زیر پهلو</p></div>
<div class="m2"><p>درخوابگهم حریر و دیباست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کوی تو کی رود اسیری</p></div>
<div class="m2"><p>کز زلف تواش کمند برپاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ساغرم از خیال لعلت</p></div>
<div class="m2"><p>خوناب جگر به جای صهباست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دندان بکن از لبش صفایی</p></div>
<div class="m2"><p>مالک دارد نه مال یغماست</p></div></div>