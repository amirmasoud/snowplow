---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>گردون ماهی جوان ندارد</p></div>
<div class="m2"><p>بستان سروی روان ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه فلکی زبان نداند</p></div>
<div class="m2"><p>سرو چمنی چمان ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل در خم زلف سر کجت راست</p></div>
<div class="m2"><p>یک مو سر این وآن ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برچهر تو بلبل از تماشا</p></div>
<div class="m2"><p>هرگز غم گلستان ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را چو بهار عارضت کو</p></div>
<div class="m2"><p>باغی که ز پی خزان ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با درج تو غنچه را چه دعوی</p></div>
<div class="m2"><p>تنگ است ولی دهان ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هر نگهت دلی است صد چاک</p></div>
<div class="m2"><p>از غمزه کس این سنان ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل سوخت تمام و کس ندانست</p></div>
<div class="m2"><p>یا آتش ما دخان ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسوای جهان شود صفایی</p></div>
<div class="m2"><p>سر تو اگر نهان ندارد</p></div></div>