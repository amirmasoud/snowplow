---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>شادم که شمارم اشک وآه است</p></div>
<div class="m2"><p>تا در غمت این دوام گواه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با درد جدائیت صبوری</p></div>
<div class="m2"><p>خود بی کم و بیش کوه وکاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با عمر دراز زلف کج راست</p></div>
<div class="m2"><p>فرقی که نه جای اشتباه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موی من از آن سفید چون روز</p></div>
<div class="m2"><p>روز من از آن چو شب سیاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تابش روی و تاب گیسوی</p></div>
<div class="m2"><p>مشکوی تو پر ز مار و ماه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آن ماه چه نعل ها در آتش</p></div>
<div class="m2"><p>ز آن مار چه پشت ها دو تاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل با همه زخم های کاری</p></div>
<div class="m2"><p>باز از مژه ی تو عذرخواه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تیر نگاه و تیغ ابروی</p></div>
<div class="m2"><p>گیسوی توام گریزگاه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل در صف غمزه اش صفایی</p></div>
<div class="m2"><p>صیدی به مصاف یک سپاه است</p></div></div>