---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>از دست برد دین و دل ار دلستان من</p></div>
<div class="m2"><p>سهل است گو به پای فکن جسم و جان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم خزم ز فتنه ی مژگان به گوشه ای</p></div>
<div class="m2"><p>گفتا مدار چشم امان در زمان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرایش رخ تو فزود اضطراب دل</p></div>
<div class="m2"><p>در خاصیت بهار تو آمد خزان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازم به دولت سر عشقت که پخت و ساخت</p></div>
<div class="m2"><p>از اشک چشم و لخت جگر آب و نان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باری برون خرام به دشت از وثاق خویش</p></div>
<div class="m2"><p>وز سنگ و خاک بادیه بشنو فغان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا حشر سر ز زانوی غم برنیاوری</p></div>
<div class="m2"><p>یک ره به گوشت ار برسد داستان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این آب چشم و آتش دل تا خبر شوی</p></div>
<div class="m2"><p>خاکم دهد به باد و نبینی نشان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برتربتم گذر کن و بنشین و گوش دار</p></div>
<div class="m2"><p>بشنو چو نی نوای غم از استخوان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان را صفایی این سفر از ملک عشق نبست</p></div>
<div class="m2"><p>جز قطع دل ز کون و مکان ارمغان من</p></div></div>