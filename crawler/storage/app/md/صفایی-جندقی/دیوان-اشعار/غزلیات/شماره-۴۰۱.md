---
title: >-
    شمارهٔ ۴۰۱
---
# شمارهٔ ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>ترا آخر چه شدکز بی وفایی</p></div>
<div class="m2"><p>نکردی در محبت عهد پایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا از دین و دل دیوانه بودم</p></div>
<div class="m2"><p>که کردن از تو آهنگ جدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فردوسم مخوان زنهار از این در</p></div>
<div class="m2"><p>که آنجا نیست چندین دل گشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم آمیخت از لعلت به خوناب</p></div>
<div class="m2"><p>قدم آموخت از زلفت دوتایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر خود از وفاداری و رحمت</p></div>
<div class="m2"><p>علاج رنج مهجوران نمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که فرقی نیست شرح درد و غم را</p></div>
<div class="m2"><p>بگوش غیر با دستان سرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین سامان و ثروت کم فراهم</p></div>
<div class="m2"><p>به کویت نایدم عار از گدایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صنوبر گو مکش سر پیش بالاش</p></div>
<div class="m2"><p>که حسنت چیست الا یک رسایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا زین پس نزیبد پارس موطن</p></div>
<div class="m2"><p>که عشقم توبه داد از پارسایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رود در دوستی از وی ستم ها</p></div>
<div class="m2"><p>که از دشمن نیاید بر صفایی</p></div></div>