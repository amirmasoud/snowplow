---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>صاحب دلی به سیر کمانت نظر نکرد</p></div>
<div class="m2"><p>تا سینه پیش آن صف پیکان سیر نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکش به آستین تو از رخ نکرده پاک</p></div>
<div class="m2"><p>تا خاک آستان تو سرمه ی بصر نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صوفی ما به ترک کله صرفه ای نبرد</p></div>
<div class="m2"><p>مسکین که خاکپای ترا تاج سر نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صیدت به یاد بال افشانی آشیان</p></div>
<div class="m2"><p>از خوشدلی به دام تو سر زیر پر نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف خود ار نبود گرفتار کس چرا</p></div>
<div class="m2"><p>بر وی گذشت قرنی و یاد از پدر نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چهری که آستین تو بر دیدگان نسود</p></div>
<div class="m2"><p>چشمی که آستان تو از گریه تر نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردا که کس به حضرت جانان نجست جای</p></div>
<div class="m2"><p>کاو را ز رشک دور فلک دربدر نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آتش به سنگ رخنه نیفتاده کس ندید</p></div>
<div class="m2"><p>جز در دل تو کآه صفایی اثر نکرد</p></div></div>