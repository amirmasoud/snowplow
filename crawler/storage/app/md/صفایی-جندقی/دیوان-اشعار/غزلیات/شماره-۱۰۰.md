---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>سگی به از من سرگشته پاسبان تو نیست</p></div>
<div class="m2"><p>ولی چه سود که رویم بر آستان تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه از سر تیر تو اوفتادم دور</p></div>
<div class="m2"><p>ولیک همچو ندانی که دل نشان تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر هزار خدنگ از کفت خورم دارم</p></div>
<div class="m2"><p>هنوزحسرت تیری که درکمان تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رشک غارت گلچین به هیچ دام و قفس</p></div>
<div class="m2"><p>شکسته بال تر از مرغ آشیان تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گلبنی که در دی هم استی چو بهار</p></div>
<div class="m2"><p>میان بلبل وگلچین و باغبان تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرور و خشم و تغافل، جفا و ناز و عتاب</p></div>
<div class="m2"><p>به ملک دلبری امروز جز به شان تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود از قفای تو آمد هر آنکه دل به تو داد</p></div>
<div class="m2"><p>گناه جادوی خون ریز دل ستان تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خوش بود که به اهل وفا جفا نکنند</p></div>
<div class="m2"><p>ولی چه فایده کاین رسم در زمان تو نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود از نخست صفایی به بی وفایی هات</p></div>
<div class="m2"><p>چنان شناخت که حاجت به امتحان تونیست</p></div></div>