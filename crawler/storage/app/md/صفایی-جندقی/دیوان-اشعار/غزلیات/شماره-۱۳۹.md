---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>مرا خود از سر کوی تو ترسم آب برد</p></div>
<div class="m2"><p>وگرنه گریه من سبقت از سحاب برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود به عشوه ی ساقی ز مغز پایه ی هوش</p></div>
<div class="m2"><p>کجا ز دست مرا نشأه شراب برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه احتساب نکردت به خون بی گنهان</p></div>
<div class="m2"><p>عجب که ترک تو از محتسب حساب برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فقیه کفر مرا گر به عدل فتوی داد</p></div>
<div class="m2"><p>بدین عمل ز خدا اجر بی حساب برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهان و فاش به عهد تو خوبرو دگری</p></div>
<div class="m2"><p>نه دین ز شیخ رباید نه دل ز شاب برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مراست طالع بیدار و کوکبی فیروز</p></div>
<div class="m2"><p>شبی که فکر توام از دو دیده خواب برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان نبرد عتابش ز من سکون و ثبات</p></div>
<div class="m2"><p>که لطفش از تن و جانم توان و تاب برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبا کجاست که عرض نیاز و ذوق حضور</p></div>
<div class="m2"><p>یکی از جانب یاران به آن جناب برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرم به قصد رهایی ز هجر خواهد کشت</p></div>
<div class="m2"><p>به کیش من چه قدر زین گنه ثواب برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفایی ازتف دل دست و خامه خواهدسوخت</p></div>
<div class="m2"><p>اگر زعشق تو یک نکته درکتاب برد</p></div></div>