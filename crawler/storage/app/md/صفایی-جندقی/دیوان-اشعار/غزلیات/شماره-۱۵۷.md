---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>بخت بد کآخر دمم شمشیر زد</p></div>
<div class="m2"><p>جان بر آمد زود و جانان دیر زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواستم مجروح تر بودن ز غیر</p></div>
<div class="m2"><p>هر چه زد دردا که بی توفیر زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بندی بین که ترکی شیرگیر</p></div>
<div class="m2"><p>از دوآهو یک جهان نخجیر زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این غزال از بس جری در صید خاست</p></div>
<div class="m2"><p>هم پلنگ آویز شد هم شیر زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتگوها داشتم با وی هنوز</p></div>
<div class="m2"><p>زخم کاری بود و بی تأخیر زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نازم استادی صیادی که زه</p></div>
<div class="m2"><p>بر کمان نابسته آنسان تیر زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پی زلفش نرفتن دست کو</p></div>
<div class="m2"><p>آنکه بر بازوی دل زنجیر زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی جوانان جان از او دارند صف</p></div>
<div class="m2"><p>طفل خردی کو ره ی صد پیر زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهرش در لطف و لعلش در صفا</p></div>
<div class="m2"><p>طنزها برانگبین و شیر زد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرم باد از صورتش نقاش را</p></div>
<div class="m2"><p>تا قلم بر صفحه ی تصویر زد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق تر دستم عیار عقل زد</p></div>
<div class="m2"><p>یار سر مستم ره تدبیر زد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ترا گردد صفایی خاک پای</p></div>
<div class="m2"><p>پای در این خاک دامنگیر زد</p></div></div>