---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>چون کنم یاد توای دوست من آلوده</p></div>
<div class="m2"><p>نام پاک تو کجا وین دهن آلوده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرگ عشقت اگرم یوسف دل پاره نکرد</p></div>
<div class="m2"><p>چیست پس ز اشک من این پیرهن آلوده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در قیامت مگرم خلعت رحمت پوشند</p></div>
<div class="m2"><p>ورنه سر بر نکنم از کفن آلوده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیرم از خلق نهفتم دل ناپاک ولی</p></div>
<div class="m2"><p>خود چه تدبیر کنم با بدن آلوده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خونین به کفم ماند و در آن رسته ترا</p></div>
<div class="m2"><p>چون خریدار شوم زین ثمن آلوده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما بماندیم درین کوی خوشا وقت کسی</p></div>
<div class="m2"><p>که به غربت رود از این وطن آلوده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی برتابم از این زال که لایق نبود</p></div>
<div class="m2"><p>صحبت همسر پیمان شکن آلوده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدی از برگ گلش خار خط آخر سر زد</p></div>
<div class="m2"><p>دست در شوی دلا ز آن ذقن آلوده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسمان گو پس از این تخم میفشان به زمین</p></div>
<div class="m2"><p>حاصلت چیست ازین مرد و زن آلوده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وصف تنزیه ترا عقل صفایی شیداست</p></div>
<div class="m2"><p>خاک بر فرق من و این سخن آلوده</p></div></div>