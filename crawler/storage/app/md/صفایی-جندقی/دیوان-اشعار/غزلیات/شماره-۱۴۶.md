---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>که جز من کاشکم اینسان دربدر کرد</p></div>
<div class="m2"><p>انیس خویش اشک پرده تر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنازم غمزه ات کز یک کرشمه</p></div>
<div class="m2"><p>بنای تقویم زیر و زبر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جزاک الله کنی منعم ز فریاد</p></div>
<div class="m2"><p>کجا فریاد از این بهتر اثر کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبارک اختری فرخنده بختی</p></div>
<div class="m2"><p>که روزی با تو شب شامی سحر کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تاب لعلت آن کس کام دل یافت</p></div>
<div class="m2"><p>که پی گیریش پیکانت سپر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلم غماز رازم شد ندانم</p></div>
<div class="m2"><p>که از اسرار ما او را خبر کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل کشتم درختی کش ز هر شاخ</p></div>
<div class="m2"><p>به جای هر گلم خاری به در کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهال عشق زاد این میوه ما را</p></div>
<div class="m2"><p>که سر تا پا ملامت برگ برکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه جز فرقت به فرقم سایه انداخت</p></div>
<div class="m2"><p>نه جز حسرت بر احوالم ثمر کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رقیب از بس گمان های غلط برد</p></div>
<div class="m2"><p>صفایی از سر کویت سفر کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا ازدست او کوپای تمکین</p></div>
<div class="m2"><p>که با دشمن تواند چون تو سر کرد</p></div></div>