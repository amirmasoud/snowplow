---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>چون صبح عید هفتم ماه رجب رسید</p></div>
<div class="m2"><p>از شش طرف نوید هزاران طرب رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گفت جان به مقدم او کن فدا که باز</p></div>
<div class="m2"><p>غارتگر عجم شه ی ترکان عرب رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرگرم قتل کیست که چون مهر بامداد</p></div>
<div class="m2"><p>با جامه ی غضب همه تن در غضب رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر اسیری دل و داغ درون من</p></div>
<div class="m2"><p>با طره ی به تاب و عذاری به تن رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستم زبان شکوه به شکرانه ی وصال</p></div>
<div class="m2"><p>زان غم که در فراق تو هر روز و شب رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح و صال قصه نرانم زشام هجر</p></div>
<div class="m2"><p>کاین یک شکنجه آمد و آن یک تعب رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگر ره از مشاهده دل زنده شد مرا</p></div>
<div class="m2"><p>هر چند در مجاهده جانم به لب رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر نخل نازت از شکرین بوس های تر</p></div>
<div class="m2"><p>کو زهر خود چسود که ما را طلب رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تکیف می مکن به صفایی کش این دو روز</p></div>
<div class="m2"><p>بی می ز لعل یار نشاطی عجب رسید</p></div></div>