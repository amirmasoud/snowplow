---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>صرف شد عمر گرانمایه به جمع زر و سیم</p></div>
<div class="m2"><p>و اندرین رسته نگیرند به جز قلب سلیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وای زین خجلت و خوف و خطر و خبط و خطا</p></div>
<div class="m2"><p>که امانم همه باک است و امیدم همه بیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفرکوی تو حد نیست گدایی چو مرا</p></div>
<div class="m2"><p>مگرم بدرقه ی راه کنی لطف قدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهل من بین که بدین ضعف و حقارت رفتم</p></div>
<div class="m2"><p>زیر باری که ابا جست از آن عرش عظیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق را خلق کریم تو و اوصاف حسن</p></div>
<div class="m2"><p>حجتی بود مبین ز آیت احسن تقویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی دل در دو جهان تافت زهر رتبه مقام</p></div>
<div class="m2"><p>هر که گردید به خاک سر کوی تو مقیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بهشتم همه جا تا بودم قرب جوار</p></div>
<div class="m2"><p>که شراری است مرا ز آتش هجر تو جحیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نام پرهیز مبر لاف کرامات مباف</p></div>
<div class="m2"><p>که صفایی تو نه ای قابل اکرام کریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرک را عار ز توحید تو با این تمکین</p></div>
<div class="m2"><p>کفر را ننگ ز اسلام تو با این تسلیم</p></div></div>