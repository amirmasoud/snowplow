---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>یک رشحه از تراوش لعلت به کام من</p></div>
<div class="m2"><p>خوشتر که ریزد آب خضر جم به جام من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیر و شراب و شکر و شهدم به کار نیست</p></div>
<div class="m2"><p>تا کوثر دهان تو باشد به کام من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسواییم ز عشق تو افزود و ننگ نیست</p></div>
<div class="m2"><p>این قرعه از الست برآمد به نام من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیداست انقلاب من از اضطراب وی</p></div>
<div class="m2"><p>پیک صبا چو پیش تو آرد پیام من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ویران ترم نسازد ازین باش گو سپهر</p></div>
<div class="m2"><p>تا روز حشر در صدد انهدام من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سعی من از تو بیش بود در هلاک خویش</p></div>
<div class="m2"><p>دیگر تو بگذر ای فلک از انتقام من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ادبار بین که مهر من افزود کین او</p></div>
<div class="m2"><p>اقبال بین که دانه ی او گشت دام من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرقم برآستان تو پامال غیر شد</p></div>
<div class="m2"><p>تاب لگد ندارد ازین بیش بام من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر مشتری به مهر من آن ماه خرگهی است</p></div>
<div class="m2"><p>گردد هزار توسن بهرام رام من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا خاک فقر را چو صفایی شدم مقیم</p></div>
<div class="m2"><p>بس رشک برده شاه و گدا برمقام من</p></div></div>