---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>دردا که ز دوران به غم خویشتن افتاد</p></div>
<div class="m2"><p>روزی که دلارام به سودای من افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف که به چاه فتن افتاد برآمد</p></div>
<div class="m2"><p>بیچاره زلیخا که به چاه ذقن افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منعم مکن از ناله که پنهان نتوان کرد</p></div>
<div class="m2"><p>آن راز که افسانه ی هر انجمن افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدر قفس آن مرغ گفتار شناسد</p></div>
<div class="m2"><p>کز قید تو یک بار رهش در چمن افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل که به گل نغمه سرودی به صد آهنگ</p></div>
<div class="m2"><p>تا غنچه گویای تو دید از سخن افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سرو و سمن دیده ی امید فرو دوخت</p></div>
<div class="m2"><p>چشمی که بر آن سرو قد سیم تن افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر بازوی عشاق ز هر سو رسن افکند</p></div>
<div class="m2"><p>چون طره به دوش تو شکن در شکن افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان ها ز علایق همه زنجیر گسستند</p></div>
<div class="m2"><p>تا زلف تو بر گردن دل ها رسن افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با غنچه ی نوشین دهنت از عرق شرم</p></div>
<div class="m2"><p>گل را چو من آتش همه در پیرهن افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در باغ ز داغ رخ گلرنگ تو جاوید</p></div>
<div class="m2"><p>بر خاک سیه لاله ی خونین کفن افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سور و طرب از شور تو بر پیر و جوان رفت</p></div>
<div class="m2"><p>شور و شغب از شوق تو در مرد و زن افتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در سینه چه پوشم دگر آن درد صفایی</p></div>
<div class="m2"><p>کز دل به زبان رفت و به چندین دهن افتاد</p></div></div>