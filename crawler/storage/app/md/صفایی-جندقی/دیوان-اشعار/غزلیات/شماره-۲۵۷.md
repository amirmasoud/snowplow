---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>به مهر روی ماهی عهد بستم</p></div>
<div class="m2"><p>که عهد مهر مه رویان شکستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم تابنده ی آن سرو آزاد</p></div>
<div class="m2"><p>ز بند بنده و آزاد رستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا تنها کجا بود اینقدر دل</p></div>
<div class="m2"><p>که چندین دل بهر عضو تو بستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر نتوانم از روی تو برداشت</p></div>
<div class="m2"><p>بخوان گو شیخ و صوفی بت پرستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشم کاین خاکساری سربلندی است</p></div>
<div class="m2"><p>اگر بالای سروت ساخت پستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دامن سر به پایت سودمی باز</p></div>
<div class="m2"><p>رسیدی گر به دامان تو دستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یاد آید ترا ز اول که گفتی</p></div>
<div class="m2"><p>چو دیی ر کمندت پای بستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نوش وصل مرهم خواهمت ساخت</p></div>
<div class="m2"><p>چو از نیش فراقت سینه خستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین امید در راهت شب و روز</p></div>
<div class="m2"><p>گهی برخاستم گاهی نشستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جان تن زندگی دارد دریغا</p></div>
<div class="m2"><p>عجب دارم که من چون بی تو هستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر ننهم ز خلوت پای بیرون</p></div>
<div class="m2"><p>گر از دست تو ای صیاد جستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روا نبود ملامت بر صفایی</p></div>
<div class="m2"><p>که من هم رشته طاقت گسستم</p></div></div>