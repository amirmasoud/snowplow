---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>مرا چون شمع هرشب سوختن و آنگه سحر مردن</p></div>
<div class="m2"><p>بس آسان است و مشکل بی تو روزی را به شب بردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی کردم تمنای هلاک خویشتن باری</p></div>
<div class="m2"><p>اگر می بود ممکن در فراقت زندگی کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو صیادم تویی از بخت خود شادم خوشا خونی</p></div>
<div class="m2"><p>که زیبد آب آن شمشیر و گردد زیب آن گردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا صیدی اسیر افتد چو من در قید صیادان</p></div>
<div class="m2"><p>که غم ناکم به آزادی و خرسندم به آزردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست دل ستان بادت حلال ار زهر بستانی</p></div>
<div class="m2"><p>که جام از دوست نگرفتن حرامستی نه می خوردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه نسبت با چو من پیری جوانی چون ترا جانا</p></div>
<div class="m2"><p>ترا آغاز سرگرمی مرا انجام افسردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغان کافکند بر سر سایه روزی ابر نوروزی</p></div>
<div class="m2"><p>که آمد چون گل چیده مرا هنگام پژمردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبر نام گنه خط بر خطا کش خجلتم مفزا</p></div>
<div class="m2"><p>که جرم عذرخواهان را سزاوار است نشمردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفایی باغبانم بست ره زین بوستان وقتی</p></div>
<div class="m2"><p>که بود آن گلبن نوخیز را آغاز پروردن</p></div></div>