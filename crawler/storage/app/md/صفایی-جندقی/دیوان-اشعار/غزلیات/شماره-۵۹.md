---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>مرا امروز اختر سازگار است</p></div>
<div class="m2"><p>زمین پامرد و گردون دستیار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سعادت همدم و دولت هم آغوش</p></div>
<div class="m2"><p>که آن زرین میانم درکنار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی یاری که کینش بی ثبات است</p></div>
<div class="m2"><p>خهی ماهی که مهرش ا ستوار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وفا پرور حیا خونی هوا خواه</p></div>
<div class="m2"><p>که دلجویی و دلداریش کار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مهرش پایه برتر بینم اما</p></div>
<div class="m2"><p>ز فرط مهربانی خاکسار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی با این کمال از غایت حسن</p></div>
<div class="m2"><p>تنی نی کز کمندش رستگار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه دل ها کز فراقش در فغان است</p></div>
<div class="m2"><p>چه تن ها کز برایش بی قرار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه لب ها کز دو لعلش پر خروش است</p></div>
<div class="m2"><p>چه سرها کز دو چشمش پر خمار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه جان ها کز جمالش ناصبور است</p></div>
<div class="m2"><p>چه رخ ها کز هوایش پر غبار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از این جمع پریشان گرفتار</p></div>
<div class="m2"><p>صفایی هم به جانش خواستار است</p></div></div>