---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>بر تو از دست صبا شام و سحر غیرتم آید</p></div>
<div class="m2"><p>کو چرا دست تطاول به سر زلف تو ساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوده بر سنبل گیسوی تمنای تو دستی</p></div>
<div class="m2"><p>که به بستان دگر از جیب صبا بوی گل آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغ گلبرگ رخت ماند چنان بر دل خونین</p></div>
<div class="m2"><p>که گل سرخ ز خاکم عوض سبزه برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داند ار واعظ ما شادی قرب و غم دوری</p></div>
<div class="m2"><p>دیگر از دوزخ و فردوس حدیثی نسراید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفسی چند به کنج قفس آسوده کشیدی</p></div>
<div class="m2"><p>لاجرم سیر چمن جز غمت ای دل نفزاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور ریزد به گلو صد خم خونش به تلافی</p></div>
<div class="m2"><p>هرکه یک دم ز شکر پاره ی لعل تو بخاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکشم منت دربان، ندهم زحمت حاجب</p></div>
<div class="m2"><p>دانم آن در که تو بندی دگرم کس نگشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه به کامم برسانی چه به حسرت بنشانی</p></div>
<div class="m2"><p>رو به غیر تو درآیین من ای دوست نشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون صفایی همه ایام نشینی به تحیر</p></div>
<div class="m2"><p>اگر آن حور جنانت نظری رخ بنماید</p></div></div>