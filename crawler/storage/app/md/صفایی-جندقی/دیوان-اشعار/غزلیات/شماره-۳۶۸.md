---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>ای قد یار بسکه دل آرای و دلبری</p></div>
<div class="m2"><p>برخاست از خرام تو هر گام محشری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاخ کدام سروی و سرو کدام باغ</p></div>
<div class="m2"><p>کز فرق تا قدم همه دل جوی و دلبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانست و دل ز شاخ تو جو شد اگر گلی</p></div>
<div class="m2"><p>روی است و سر به پای تو ریزد اگر بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نوبری به خانه ز باغ آید ای شگفت</p></div>
<div class="m2"><p>کز خانه آمدی تو و در باغ نوبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قامت مگو که دیده ی دهقان سال خورد</p></div>
<div class="m2"><p>هرگز ندیده چون تو به سیما صنوبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشویش صد هزار فلک ماه نخشبی</p></div>
<div class="m2"><p>تشویر صد هزار چمن سرو کشمری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بالای دلبری نه که غوغای خاص و عام</p></div>
<div class="m2"><p>آزرم کشمری نه که آشوب کشوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سروی هنوز چون تو ز کشمر نیامده است</p></div>
<div class="m2"><p>سروی ولی به زعم من از جای دیگری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گیتی نپروریده نهالی نظیر تو</p></div>
<div class="m2"><p>مانا مگر ز خاک جنان و آب کوثری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاخی فزون نه ای و به اوصاف مختلف</p></div>
<div class="m2"><p>شمشاد و بید وگلبن و آزاد و عرعری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخلی که از شمامه ی گیسوی شاخ شاخ</p></div>
<div class="m2"><p>شرم هزار چین و تتر مشک و عنبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوران محشر از توبه سر کی رسد که هست</p></div>
<div class="m2"><p>در محشر از خرام تو هرگام محشری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز در تو این دو جمع بهم نامد ای عجب</p></div>
<div class="m2"><p>کز چهر و جبهه جامع خورشید و اختری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای سرو پیش سرو دلارام سرمکش</p></div>
<div class="m2"><p>با وی مکن تصور باطل که همسری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دعوی همسری همه نبود به عرض و طول</p></div>
<div class="m2"><p>مفتی بیار اگر چه به صورت رساتری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک قامتی و بیش صفایی بهر قیام</p></div>
<div class="m2"><p>صد بار با قیام قیامت برابری</p></div></div>