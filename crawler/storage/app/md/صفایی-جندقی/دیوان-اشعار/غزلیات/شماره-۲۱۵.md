---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>فقیه شهر که سجاده ها برآب کشید</p></div>
<div class="m2"><p>شکست توبه و با صوفیان شراب کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوید باد شما را به باده خوردن فاش</p></div>
<div class="m2"><p>که محتسب به جماعت شراب ناب کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرود مژده ی رحمت وصول می همه را</p></div>
<div class="m2"><p>دگر نبایدم از حسرتش عذاب کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمید اختری از آسمان تاک امروز</p></div>
<div class="m2"><p>که ماه از خجالت در احتجاب کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کاخ ناز درآی و بکوب پای نشاط</p></div>
<div class="m2"><p>که شاخ دختر رز را ز رخ نقاب کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیده روی ترا دیده اشک ها افشاند</p></div>
<div class="m2"><p>گلی نچید از این باغ و بس گلاب کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خون ما کف و سر پنجه را نگار نمای</p></div>
<div class="m2"><p>ترا دریغ بود منت ازخضاب کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غمت به غارت ملک دلم شبیخون برد</p></div>
<div class="m2"><p>شهی قوی سپهی بر دهی خراب کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا مجال فرار از کمند عشق نماند</p></div>
<div class="m2"><p>که بی درنگ فرو بست و با شتاب کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبود پای گریزم ز جنگ کاکل و زلف</p></div>
<div class="m2"><p>که سخت بست به زنجیر و با طناب کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روم به کوکب ناسعد خویش گریم زار</p></div>
<div class="m2"><p>کنون که چرخ ترا دور مه سحاب کشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دل نماند صفایی توانی و تاب نماند</p></div>
<div class="m2"><p>دلی به سینه ام از بس که درد و تاب کشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حضور تا نفتد کی میسر است مرا</p></div>
<div class="m2"><p>بیان حالت دل زانچه در غیاب کشید</p></div></div>