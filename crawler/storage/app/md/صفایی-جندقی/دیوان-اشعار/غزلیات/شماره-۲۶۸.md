---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>تنی جز خاطر لیلی به غم دلشاد نشنیدم</p></div>
<div class="m2"><p>سری جز گردن مجنون به بند آزاد نشنیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلاکم در صفوف غمزه ات نبود غریب اما</p></div>
<div class="m2"><p>مروت بین که من یک صید و صد صیاد نشنیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدد جست از میانت طرگان در دل ربایی ها</p></div>
<div class="m2"><p>طناب از موی دارد چشم استمداد نشنیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنای عشق محکم تر ز سیل دیده شد دل را</p></div>
<div class="m2"><p>ز ویرانی بلادی را چنین آباد نشنیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جز یاد تو کآمد منتهای کام ناکامان</p></div>
<div class="m2"><p>عروسی را به خلوتگاه صد داماد نشنیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به قتلم خاستی کز ناله ام آسوده بنشینی</p></div>
<div class="m2"><p>بدین زودی چنین تأثیری از فریاد نشنیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین دستم که دل پامال در میدان آن مژگان</p></div>
<div class="m2"><p>شهیدی در سیاستگاه صد جلاد نشنیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ترک عشق بس تهدید ها راندم صفایی را</p></div>
<div class="m2"><p>جوابی از وی الا هر چه بادا باد نشنیدم</p></div></div>