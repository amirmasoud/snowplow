---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>از توچون نام برم کز دهن آلوده</p></div>
<div class="m2"><p>لاجرم سر نزند جز سخن آلوده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خبر پرسی از احوال دل خون شده ام</p></div>
<div class="m2"><p>که شهادت دهد این پیرهن آلوده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلشن از خار و بهارش به خزان ارزانی</p></div>
<div class="m2"><p>بایدم رفت به در زین چمن آلوده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخ رنگین بتان را خط مشکین ز قفاست</p></div>
<div class="m2"><p>خارم آید به نظر نسترن آلوده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخت برون بر ازین بحر گل آگین که دلا</p></div>
<div class="m2"><p>نتوان شست بدین لای تن آلوده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کعبه بسپار بدان مجتهد پاک زبان</p></div>
<div class="m2"><p>دیر بگذار بدین برهمن آلوده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه تنعم کنی از آن صمد مظنونی</p></div>
<div class="m2"><p>چه تمتع بری از آن وشن آلوده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیمه برتر زنم از نه فلک ان شاء الله</p></div>
<div class="m2"><p>غربت پاک به ازین وطن آلوده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک مست تو ندارد به صفایی سر صدق</p></div>
<div class="m2"><p>دل بپردازم ازین راهزن آلوده</p></div></div>