---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>ماه کنعانی اگر در مصر معنی روت دیدی</p></div>
<div class="m2"><p>زال‌سان از فرط حیرت خویش را فرتوت دیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ابد بستی در دکان سحاری و افسون</p></div>
<div class="m2"><p>چشم هاروت ار به خواب آن نرگس جادوت دیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوختی از تاب خجلت خون شدی از غیرتش دل</p></div>
<div class="m2"><p>گر یکی لعل شکرخای ترا یاقوت دیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده ز اول گر بدیدی خود بدین دستت خرامان</p></div>
<div class="m2"><p>سَروْبُن را دار خواندی نخل را تابوت دیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شدی چون نافه خون از شرم آهوی ختن را</p></div>
<div class="m2"><p>گر عبیرانگیزی آن زلف چون هندوت دیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برد چشمت شیرمردان را به تحریک تو از ره</p></div>
<div class="m2"><p>اوستادی خود و صیادی آهوت دیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار چندین دل کشیدی مدتی بر دوش و کس را</p></div>
<div class="m2"><p>نیست تقصیر این تطاول‌ها خود از گیسوت دیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخم را کاری زدی هان یک دم از بهر تماشا</p></div>
<div class="m2"><p>مفکنم یک باره از پا قوت بازوت دیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاش آن کو پهلو از غیر تو خالی کرده یارب</p></div>
<div class="m2"><p>جای دل یک شب به کام خویش در پهلوت دیدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک نفس دور از تو از زانوی حسرت برندارم</p></div>
<div class="m2"><p>آن سری کش گه به پا گه بر سر زانوت دیدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود نپرسیدی صفایی را که کبود چیست کارش</p></div>
<div class="m2"><p>روز و شب با آنکه عمری شد کش اندر کوت دیدی</p></div></div>