---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>سرخوشم با لبت که نوشین است</p></div>
<div class="m2"><p>ای دریغ از دلت که سنگین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در میان نیست و ز بیان پیداست</p></div>
<div class="m2"><p>این میان نیست نون تنوین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ ابروی و نافه گیسویت</p></div>
<div class="m2"><p>دشت قبچاق و رسته ی چین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر زلفت به سرقت دل و دین</p></div>
<div class="m2"><p>مو به مو مظهر شیاطین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شبستان گشایی ار بر و روی</p></div>
<div class="m2"><p>چمن اندر چمن ریاحین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راغ ها تاب سنبل و سوری</p></div>
<div class="m2"><p>باغ ها داغ نخل و نسرین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنشین پیش سرو کاین رفتار</p></div>
<div class="m2"><p>رشک آن شور چشم خودبین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز ز تأثیر لعل و کام تو نیست</p></div>
<div class="m2"><p>که کلامم لطیف و شیرین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من دیوانه چون به چنگ آرم</p></div>
<div class="m2"><p>لعبتی را که عقل کابین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو صفایی کجا و دعوی عشق</p></div>
<div class="m2"><p>پشه را کی مقام شاهین است</p></div></div>