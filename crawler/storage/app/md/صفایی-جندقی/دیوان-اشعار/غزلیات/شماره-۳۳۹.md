---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>به طور عم خون بریزی یا به اکراه</p></div>
<div class="m2"><p>نتابم گردن از تیغت علی الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان گردم به خاک مقدمت پست</p></div>
<div class="m2"><p>که گردم می نتابی رفتن از راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریغا حسرتا کم برنتابد</p></div>
<div class="m2"><p>به آمال دراز این عمر کوتاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عوض کردی قضا کاش ازتفضل</p></div>
<div class="m2"><p>به وصل بهجت افزا هجر جانکاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جور خلق خوکردم درین عهد</p></div>
<div class="m2"><p>چه منت ها به گردن دارم از شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیابد حظ معنی اهل صورت</p></div>
<div class="m2"><p>شناسد فرق زفتی کی ز آماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مغز از پوست حاشا چون گراید</p></div>
<div class="m2"><p>جز آن کز این به آن پیداکند راه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان عشقم سبب شد ورنه هرگز</p></div>
<div class="m2"><p>از این غفلت نکردی عقلم آگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزن پایی و در فرگاه رحمت</p></div>
<div class="m2"><p>برآر از جیب خجلت دست در خواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنه روی پریشانی به حضرت</p></div>
<div class="m2"><p>بریز اشک پشیمانی به درگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه سود آخر صفایی جز زیانت</p></div>
<div class="m2"><p>از این خورد شب و خواب سحرگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بشور این نامه آلوده از اشک</p></div>
<div class="m2"><p>بسوز این چامه فرسوده از آه</p></div></div>