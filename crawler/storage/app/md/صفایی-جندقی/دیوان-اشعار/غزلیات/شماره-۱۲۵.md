---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>به پای بوس توام دست و دل ز کار شد آوخ</p></div>
<div class="m2"><p>ز تن قرارم از آن زلف بی قرار شد آوخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانمت چه رسد بر سرم ز فتنه ی مژگان</p></div>
<div class="m2"><p>که چار فوج سپه با دلم و چار شد آوخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وفا و شفقتم افزود وکاست تاب صبوری</p></div>
<div class="m2"><p>جفا و جور چندانکه پایدار شد آوخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اختیار نهفتم به سینه مهرت و دیدم</p></div>
<div class="m2"><p>سزای خویش که صبرم به اضطرار شد آوخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشید عشق به ملک وجود جیش غمت را</p></div>
<div class="m2"><p>دلم مسخر سلطان نابه کار شد آوخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که ساده و نقشش به دیده فرق نکردی</p></div>
<div class="m2"><p>اسیر پنجه ی سر پنجه ی نگار شد آوخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چهر و زلف تو پیچم به خود چو مار گزیده</p></div>
<div class="m2"><p>که باغ نسترنت خوابگاه مار شد آوخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خارها که ز غیرت زند خطت به جگرها</p></div>
<div class="m2"><p>که گلشنی چو رخت پایمال خار شد آوخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفوف غمزه کجا و دل ضعیف صفایی</p></div>
<div class="m2"><p>تنی پیاده گرفتار صد سوار شد آوخ</p></div></div>