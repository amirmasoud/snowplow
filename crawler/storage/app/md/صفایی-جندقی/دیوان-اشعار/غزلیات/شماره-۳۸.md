---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>در پی وصل جفا جوی ستم پاره ی ما</p></div>
<div class="m2"><p>تا دل از هجر نشد پاره نشد چاره ی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا فراقم بکشد یا به وصالت برسیم</p></div>
<div class="m2"><p>تا چه اندیشه کند رای تو درباره ی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده در پوست نگنجد گه دیدار مگر</p></div>
<div class="m2"><p>عین شادی شده پیوند به نظاره ی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگر ایام بهار آمد و وقت است که باز</p></div>
<div class="m2"><p>عذر صد توبه بخواهد لب می خواره ی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم آشفته به عفوی کآید روشن</p></div>
<div class="m2"><p>چشم صد یوسف از این پیراهن پاره ی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشترم ز آن مژه بر سینه چنان زد که هنوز</p></div>
<div class="m2"><p>می رود خون دل از دیده به رخساره ی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم آرم به ترحم دلت از زاری گفت</p></div>
<div class="m2"><p>چه کند قطره ی باران تو باخاره ی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم از نوش لبت بخش من آخرکوگفت</p></div>
<div class="m2"><p>می نسازد به مزاج توشکر پاره ی ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الفتی هست صفایی به سهی قدانم</p></div>
<div class="m2"><p>بوده از سرو مگرتخته ی گهواره ی ما</p></div></div>