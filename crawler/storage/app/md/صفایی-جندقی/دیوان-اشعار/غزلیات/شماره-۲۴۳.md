---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>ز مهرم خوب تر آن مهر مهوش</p></div>
<div class="m2"><p>از آن زر خوشترم این سیم بی غش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا مگذار با این مایه تردید</p></div>
<div class="m2"><p>مرا مپسند در چندین کشاکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مده خاکم به باد از خشم و خواری</p></div>
<div class="m2"><p>بزن از رحمتم آبی برآتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی سیرابم آر از لعل خوشاب</p></div>
<div class="m2"><p>گهی سرمستم آر از چشم سرخوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خاک افکنده چندین صید و صیاد</p></div>
<div class="m2"><p>هنوزش تیرها قایم به ترکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پریشان تر شود زلفت که چون خویش</p></div>
<div class="m2"><p>مرا پیوسته می دارد مشوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم از طیبش مرا بادامه در پاش</p></div>
<div class="m2"><p>هم از تابش مرا رخساره زرپش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا برکتف و کش خوش رام و آرام</p></div>
<div class="m2"><p>مرا چون مار خشمین سخت و سرکش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منت کافی ز عشاق وفا کیش</p></div>
<div class="m2"><p>ترا تنها صفایی بس جفا کش</p></div></div>