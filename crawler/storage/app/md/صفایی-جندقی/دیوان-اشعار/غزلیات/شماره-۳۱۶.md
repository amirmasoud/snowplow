---
title: >-
    شمارهٔ ۳۱۶
---
# شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>نه از حکمت توانم سر کشیدن</p></div>
<div class="m2"><p>نه مهرت را قلم برسر کشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه امکان داشت در بزمت مرا بار</p></div>
<div class="m2"><p>نه بار از آستان بردر کشیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه کامم حاصل از سیب زنخدانت</p></div>
<div class="m2"><p>به ماتم خویش از این چه برکشیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تن ماندن تواند زنده بی دل</p></div>
<div class="m2"><p>نه دل ممکن ز زلفش در کشیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چشم از دل توان پوشیدنم باز</p></div>
<div class="m2"><p>نه دست از دامن دلبر کشیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود این رسم جز ما را در اسلام</p></div>
<div class="m2"><p>مسلمان خواری از کافر کشیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندانی بایدم در هر نگاهی</p></div>
<div class="m2"><p>چها زان مست افسونگر کشیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که یادت داد در کشور گشایی</p></div>
<div class="m2"><p>رقیب غمزه این لشکر کشیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نپندارم خود از نقاش ایجاد</p></div>
<div class="m2"><p>از این به صورتی دیگر کشیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر با نرگس مست دلارام</p></div>
<div class="m2"><p>نزیبد نازم از عبهر کشیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سزد نظم درروارت صفایی</p></div>
<div class="m2"><p>به گوش هوش چون گوهر کشیدن</p></div></div>