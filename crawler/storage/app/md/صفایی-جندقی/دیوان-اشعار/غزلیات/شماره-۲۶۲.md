---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>وفا و حسن در یاری ندیدم</p></div>
<div class="m2"><p>به هم حسن و وفا آری ندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظیرت را پری رویی نجستم</p></div>
<div class="m2"><p>ندیدت را وفا داری ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه از بیگانگان نز آشنایان</p></div>
<div class="m2"><p>ز فضل حسنت انکاری ندیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ترکان چون تو با این مایه خوبی</p></div>
<div class="m2"><p>رضا جویی کم آزاری ندیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال صورت و معنی که در تست</p></div>
<div class="m2"><p>دگر با هیچ دلداری ندیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عین هوشیاری اینقدر مست</p></div>
<div class="m2"><p>به جز چشم تو بیماری ندیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آب و رنگ آن رخسار و مژگان</p></div>
<div class="m2"><p>گلی نشنیدم و خاری ندیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مشکین خال و زلف تابدارت</p></div>
<div class="m2"><p>سر مور و دم ماری ندیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دین وکفر کاندر کعبه و دیر</p></div>
<div class="m2"><p>چنین تسبیح و زناری ندیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به طرز طره ات هرگز کمندی</p></div>
<div class="m2"><p>به دستان تو طراری ندیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا خود روز و روشن تیره شب ساخت</p></div>
<div class="m2"><p>چو گیسویت سیه کاری ندیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به غیر دل که در قید تو سرخوش</p></div>
<div class="m2"><p>رها از غم گرفتاری ندیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفایی راستی کز خیل خوبان</p></div>
<div class="m2"><p>بدین صدق وصفا یاری ندیدم</p></div></div>