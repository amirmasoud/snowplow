---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>باشد از آنروز طالعم فیروز</p></div>
<div class="m2"><p>که در آیی به چهر مهر افروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز روشن شود مرا شب تار</p></div>
<div class="m2"><p>به محرم در آیدم نوروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سال ها ز آب و تاب دیده و دل</p></div>
<div class="m2"><p>در فراق تو هر شبم تا روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر زمین ریخت اشک کوکب ساز</p></div>
<div class="m2"><p>بر فلک رفت آه کیوان سوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه از جان و سر عزیزتری</p></div>
<div class="m2"><p>تو بدان طلعت ستاره فروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر به پایت چرا نهم هر شب</p></div>
<div class="m2"><p>جان برایت چرا دهم همه روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیفم آید ترا که بار آیی</p></div>
<div class="m2"><p>خشم پرور ستمگری کین توز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قهر بر ما مران که کس نکند</p></div>
<div class="m2"><p>جور با دوستان مهر اندوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جفا از تو رو نگرداند</p></div>
<div class="m2"><p>از صفایی وفای عهدآموز</p></div></div>