---
title: >-
    شمارهٔ ۲۱۳
---
# شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>آوخ که یار برسر این ناتوان رسید</p></div>
<div class="m2"><p>وقتی که از شکنجه هجران به جان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرصت نیافت یار و به پایان شتافت عمر</p></div>
<div class="m2"><p>مهلت نداد مرگ و اجل بی امان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانان به پرسش آمد و جان گرم رفتن آه</p></div>
<div class="m2"><p>خارم به دیده فصل بهارم خزان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی کن به پهنه ی هوس ای دل رکاب عیش</p></div>
<div class="m2"><p>حالی که وصل و هجر عنان بر عنان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کینه ی چرخ وکاوش اختر به ما نرفت</p></div>
<div class="m2"><p>چندان جفا که زان مه نامهربان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل با نگاه اولش از کار شد مگر</p></div>
<div class="m2"><p>یک تیر سهم سینه ی ما ز آن کمان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن فتنه از زمانه بر اهل زمین نخاست</p></div>
<div class="m2"><p>گر چشم او به فتنه آخر زمان رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در فرقت تو ناله ام از گریه بازداشت</p></div>
<div class="m2"><p>اقبال و بخت من بین که به دام فغان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرها به از کفم چو صفایی به پای رفت</p></div>
<div class="m2"><p>سودم ز شکوه چیست که چندین زیان رسید</p></div></div>