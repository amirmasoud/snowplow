---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>نگارینا تو با چندین ملاحت</p></div>
<div class="m2"><p>مرا بهبود یابد کی جراحت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت مایل به راحت رنج خود را</p></div>
<div class="m2"><p>تو در رنجم پسندی یا به راحت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلالت باد خونم زانکه هجران</p></div>
<div class="m2"><p>حرامم ساخت برجان استراحت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ناصح نصیحت گو مفرمای</p></div>
<div class="m2"><p>که جز حرصم نیفزود این نصاحت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدت را سرو گفتم در روانی</p></div>
<div class="m2"><p>رخت را ماه خواندم در صباحت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی داند کجا مه نکته دانی</p></div>
<div class="m2"><p>ولی دارد کجا سرو این فصاحت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر از مه به مهرت مشتری خاست</p></div>
<div class="m2"><p>خرید آخر تحسر زین وقاحت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر بالایت ار پایش بدی باز</p></div>
<div class="m2"><p>نماندی در چمن سرو از قباحت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مفکن از نظر در بینواییم</p></div>
<div class="m2"><p>ترا می باد جاوید استراحت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یاد لطف و قهرت بگذرد عمر</p></div>
<div class="m2"><p>صفایی را خود این بس رنج و راحت</p></div></div>