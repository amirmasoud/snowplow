---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>عشق توگرفت جای جان را</p></div>
<div class="m2"><p>جان نیست به جز غمت روان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بخت بهار حسنت آراست</p></div>
<div class="m2"><p>بستند براو ره ی خزان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر مزن آستین اکراه</p></div>
<div class="m2"><p>نسپارده سر برآستان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دادم به گدایی در دوست</p></div>
<div class="m2"><p>سلطانی ملک جاودان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون پیش توکز زمانه شادی</p></div>
<div class="m2"><p>اظهار کنم غم نهان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با هجر کشیدگان توان گفت</p></div>
<div class="m2"><p>رنج دل ماجرای جان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیری است که مرغ دل به دامت</p></div>
<div class="m2"><p>بدرود سروده آشیان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کردی به خموشیم ترحم</p></div>
<div class="m2"><p>منت نکشم دگر فغان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کنج قفس دگر صفایی</p></div>
<div class="m2"><p>بگذار هوای گلستان را</p></div></div>