---
title: >-
    شمارهٔ ۳۷۹
---
# شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>رخ نمودی اگر از پرده پری</p></div>
<div class="m2"><p>سجده کردی به تو در خوب تری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن آمد همه را کوری مهر</p></div>
<div class="m2"><p>کرد تا پیش مهت جلوه گری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسبت سرو به بالای تو کرد</p></div>
<div class="m2"><p>باغبان از در کوته نظری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدمی در نظرش باز خرام</p></div>
<div class="m2"><p>تا خجل گرد از آن بی بصری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با گل روی تو مسکین بلبل</p></div>
<div class="m2"><p>سوی گلشن رود از بی خبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محو رفتار توآمد که نهاد</p></div>
<div class="m2"><p>جاودان سر به کمر کبک دری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم از لعل تو دندان نکند</p></div>
<div class="m2"><p>گر چه حاصل بودش خون جگری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انس و جان نیست به غیر تو مرا</p></div>
<div class="m2"><p>انس و جان اگر حور و پری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفتی از منع صفایی در عشق</p></div>
<div class="m2"><p>ریش گاوی کند از کوی خری</p></div></div>