---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>کس ره به دیار جان ندارد</p></div>
<div class="m2"><p>تا روی به دلستان ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور از تو به دوش تن بود بار</p></div>
<div class="m2"><p>آن سر که بر آستان ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز پوست مخوان و استخوانی</p></div>
<div class="m2"><p>جسمی که ز عشق جان ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مهر تو جا گرفت در جان</p></div>
<div class="m2"><p>جای غم دیگران ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذشت بهار وگل بیاراست</p></div>
<div class="m2"><p>باغ تو مگر خزان ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاب و تب اشتیاق و دوری</p></div>
<div class="m2"><p>صعب است ولی بیان ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در راندن این حدیث خونین</p></div>
<div class="m2"><p>مسکین قلمم زبان ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرفی ننگاشت تا ز مژگان</p></div>
<div class="m2"><p>خوناب سیه روان ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل داد وگرفت جان صفایی</p></div>
<div class="m2"><p>سودای وفا زیان ندارد</p></div></div>