---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>تیر عشق تو بهر دل نرسد</p></div>
<div class="m2"><p>سهم دیوانه به عاقل نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست خرمن گیسو که از آن</p></div>
<div class="m2"><p>خوشه ای بهره به سایل نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانده در لجه ی اشکم مردم</p></div>
<div class="m2"><p>چون غریقی که به ساحل نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میرد از ذوق گرفتاری دام</p></div>
<div class="m2"><p>کار صیدت به سلاسل نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق کوه است و شکیبایی کاه</p></div>
<div class="m2"><p>هرگز این بار به منزل نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خطت خار خطر رست مرا</p></div>
<div class="m2"><p>کشت این باغ به حاصل نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وای برحال قتیلی کاو را</p></div>
<div class="m2"><p>دست بر دامن قاتل نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سعادت نشود کارم ختم</p></div>
<div class="m2"><p>کرم او بر سر بسمل نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک دم از حسرت نوشت نکشیم</p></div>
<div class="m2"><p>کم به لب زهر هلاهل نرسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناوک ناله ی آتشبارم</p></div>
<div class="m2"><p>درتو هشدار که غافل نرسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگسلد از تو صفایی به جفا</p></div>
<div class="m2"><p>در دلت خطره باطل نرسد</p></div></div>