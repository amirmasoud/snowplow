---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>ز صد ملک جم این جام خوشتر</p></div>
<div class="m2"><p>صبوحی بی گزاف از شام خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر عمرم رود درگردش جام</p></div>
<div class="m2"><p>بسی از گردش ایام خوشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپیماییم مستان را بطی چند</p></div>
<div class="m2"><p>که الاکرام بالاتمام خوشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر مهر تو کفر آمد در اسلام</p></div>
<div class="m2"><p>مرا این کفر از آن اسلام خوشتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود از رسوایی عشقم چه پروا</p></div>
<div class="m2"><p>چنین ننگی ز چندین نام خوشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو واعظ که دشنام از اعادی</p></div>
<div class="m2"><p>مرا زین وعظ بی هنگام خوشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتوحی نز حرم نز دیر دیدیم</p></div>
<div class="m2"><p>به کیش ما صمد ز اصنام خوشتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرامیدن خوش از شمشاد قدان</p></div>
<div class="m2"><p>وی زان سرو سیم اندام خوشتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زلفت قید خالم نیست یک موی</p></div>
<div class="m2"><p>از آن صد دانه این یک دام خوشتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفایی منشأ شادی چو غم هاست</p></div>
<div class="m2"><p>بگو ناکامیم از کام خوشتر</p></div></div>