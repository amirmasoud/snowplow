---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>گر دل هوای عشق تو بر سر نداشتی</p></div>
<div class="m2"><p>ما را چون خویش واله و مضطر نداشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قتل من بس آن هم ابروی و قد راست</p></div>
<div class="m2"><p>حاشا نکن که نیزه و خنجر نداشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال درون چه گویمت ازعشق و کی ترا</p></div>
<div class="m2"><p>باشد خبر که دست در آذر نداشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ها به زلف بستی و یک مو در این عمل</p></div>
<div class="m2"><p>خوف از صراط و شورش محشر نداشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازم غرور حسن که خون های بی گناه</p></div>
<div class="m2"><p>با خاک راه خویش برابر داشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنگر به تاب زلف پریشان بی قرار</p></div>
<div class="m2"><p>احوال دل که گفتم و باور نداشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سیل اشک رو به من آورده ای چرا</p></div>
<div class="m2"><p>ویران تری ز من مگر آخر نداشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمری است کم ز لعل لب آورده ای خراب</p></div>
<div class="m2"><p>با آنکه هیچ باده به ساغر نداشتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دل به پا فتاد صفایی ز دست دوست</p></div>
<div class="m2"><p>دلخور مباش چاره ی دیگر نداشتی</p></div></div>