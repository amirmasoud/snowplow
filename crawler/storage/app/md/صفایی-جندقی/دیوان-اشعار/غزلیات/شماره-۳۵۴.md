---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>میثاق دوش را ز اول وفا نکردی</p></div>
<div class="m2"><p>و آخر به جای پاداش الا جفا نکردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل است جور خوبان زان غم خورم که هرگز</p></div>
<div class="m2"><p>بربی وفایی خویش نیز اعتنا نکردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندان ستم که راندی بر بیدلان ناکام</p></div>
<div class="m2"><p>جبران ما مضی را باری قضا نکردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک ره به آب یاری و ز باب غم گساری</p></div>
<div class="m2"><p>گردم ز رخ نشستی دردم دوا نکردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بزم باده هر شب تا دل شکسته گردم</p></div>
<div class="m2"><p>دشنام روبرو را مستی بهانه کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درحق اهل تسلیم از روی لطف و رأفت</p></div>
<div class="m2"><p>برترک تندخویی خود را رضا نکردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قانون مهربانی دانم که نیک دانی</p></div>
<div class="m2"><p>دردا که جز تغافل نسبت به ما نکردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در وصل کشته بودی به کز فراق ما را</p></div>
<div class="m2"><p>بودیم ما سزاوار لیکن به جا نکردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفتی به حکم باطل می ریخت خون به خاکم</p></div>
<div class="m2"><p>با احتساب خسروحق گر ابا نکردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیدی که چون صفایی در راه قرب جانان</p></div>
<div class="m2"><p>دارای دل نگشتی تاجان فدا نکردی</p></div></div>