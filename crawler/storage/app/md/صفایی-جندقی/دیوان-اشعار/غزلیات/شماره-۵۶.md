---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>خورشید تو رشک آفتاب است</p></div>
<div class="m2"><p>خط تو سواد مشک ناب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل در خم زلف سرکشت راست</p></div>
<div class="m2"><p>چون موی به عنبرین طناب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پرتو کوکب رخش تن</p></div>
<div class="m2"><p>فرسوده کنان به ماهتاب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصل تو مرا کمال تکریم</p></div>
<div class="m2"><p>هجران تو غایت عذاب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر من اگر سبک عنان خاست</p></div>
<div class="m2"><p>غم نیست غمت گران رکاب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صلح چو نیستت درنگی</p></div>
<div class="m2"><p>برجنگ چرا چنین شتاب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست ارچه خلاف رسم اسلام</p></div>
<div class="m2"><p>در کیش تو این رویه باب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم خواری دوستان روا نیست</p></div>
<div class="m2"><p>دل جویی دشمنان ثواب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این قطره ی خون که خوانیش دل</p></div>
<div class="m2"><p>زین بیش نه لایق عتاب است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امید صفایی از خط یار</p></div>
<div class="m2"><p>چون حسرت تشنه بر سراب است</p></div></div>