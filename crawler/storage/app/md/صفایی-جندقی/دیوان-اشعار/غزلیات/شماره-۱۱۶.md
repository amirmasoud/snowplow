---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>صفایی مگر ز نو نگاری دگر گرفت</p></div>
<div class="m2"><p>که پیرانه سر دگر جوانی ز سر گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین رای و رو ترا که یارد ملک شمرد</p></div>
<div class="m2"><p>بدین خلق و خو ترا که تاند بشر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو با این وفا و مهر دل و دیده اش نبود</p></div>
<div class="m2"><p>به مهر آن بی وفا که دل ز مهر تو برگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان کوکش از بیان شکر ریزد از دهان</p></div>
<div class="m2"><p>خجل آنکه از عمی رخت را قمر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زلف ورخت مباح که مالم هبا شمرد</p></div>
<div class="m2"><p>به چشم و لبت حلال که خونم هدر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفایی زیان نکرد به سودای عشق تو</p></div>
<div class="m2"><p>دلی داد و از غمت دو عالم جگر گرفت</p></div></div>