---
title: >-
    شمارهٔ ۳۵۱
---
# شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>برخاست از قیام تو ز آنسان قیامتی</p></div>
<div class="m2"><p>کآن را بود قیام قیامت علامتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی بند برده ای دل و بی تیغ کرده خون</p></div>
<div class="m2"><p>بی دعوی امامت از اهل کرامتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پای رفت جان و سر از دست دل مرا</p></div>
<div class="m2"><p>برچشم بیگناه نباشد غرامتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازتاب آفتاب حوادث مرا چه غم</p></div>
<div class="m2"><p>تا بر سر است سایه ی شمشاد قامتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتند نرخ بوسه به جان بسته است یار</p></div>
<div class="m2"><p>زین وعده باز ترسمش آید ندامتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز صبر ما به جور چنیت جوی نکرد</p></div>
<div class="m2"><p>بر ما رواست گر به تو باید ملامتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان خواست تا به پای تو بازد که دیر ماند</p></div>
<div class="m2"><p>دل را نبود ورنه درین ره لآمتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک درت نگشتم و دردا که دور چرخ</p></div>
<div class="m2"><p>ما را نداد بر سر کویت اقامتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بستی ره ی رقیب صفایی ز کوی تو</p></div>
<div class="m2"><p>بودی گرش به نزد تو چون وی مقامتی</p></div></div>