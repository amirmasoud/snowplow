---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>گفتم آخر ز چه برعهد تو بنیاد نماند</p></div>
<div class="m2"><p>گفت یک حرفم از آن عهد وفا یاد نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شور سودای تو ای خسرو شیرین دهنان</p></div>
<div class="m2"><p>در جهان ذکری از افسانه فرهاد نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده ی طره ی دلبند تو کاندر همه شهر</p></div>
<div class="m2"><p>به غلط هرگز از او گردنی آزاد نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو زنار سر زلف فکندی بر دوش</p></div>
<div class="m2"><p>ساکن کعبه به کف سبحه ی اوراد نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا پادشه حسن تو زد نوبت عشق</p></div>
<div class="m2"><p>دل خیلی به خیال غم او شاد نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به سرو و سمنت دیده فرو دوخت نظر</p></div>
<div class="m2"><p>در نظر نام و نشان از گل و شمشاد نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد رقم نقش وجود تو چو بر لوح شهود</p></div>
<div class="m2"><p>جفت بیرنگ تو در صفحه ی ایجاد نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه ره داشت در امکان همه زیباتر ازین</p></div>
<div class="m2"><p>صنعتی در قلم قدرت استاد نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه لب از ناله به آرامش دل گشته خروش</p></div>
<div class="m2"><p>بی تو از گریه مرا فرصت فریاد نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جدایی همه گویند کنم کسب شکیب</p></div>
<div class="m2"><p>غافل از آنکه مرا صبر خداداد نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی فراموش کند از تو صفایی حاشا</p></div>
<div class="m2"><p>تا تو در خاطری از خویشتنم یاد نماند</p></div></div>