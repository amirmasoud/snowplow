---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ندارم فرصت از شوق گرفتاری تماشا را</p></div>
<div class="m2"><p>مکن تعجیل صیاد اینقدر خون ریزی ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین سودا چه سود اندرز من کز فرط حیرانی</p></div>
<div class="m2"><p>نیابم فرق پای از سر که دانم زشت و زیبا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کفر و دین مفرما دعوت از عشقم که می ندهم</p></div>
<div class="m2"><p>به صد تسبیح و زنار آن سر زلف چلیپا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نامیزد بتی شیرین که یکدم با دو نوشین لب</p></div>
<div class="m2"><p>شفا بخشد ز صد عالم مرض چندین مسیحا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو در فردوس بخرامی بدین بالا عجب نبود</p></div>
<div class="m2"><p>اگر با تیشه ی غیرت کنند از ریشه طوبی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جنت گر در آرندت بدین طلعت یقین دارم</p></div>
<div class="m2"><p>که رضوان در حجاب شرم پوشد روی حورا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر او چون تو دختی رشک حورالعین خدا داند</p></div>
<div class="m2"><p>که بر دوش بنی آدم چه منتهاست حوا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز شرم چشم گیرا بت مرارت ماند بر باده</p></div>
<div class="m2"><p>ز رشک لعل سیرابت حلاوت رفت حلوا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفایی صبرم از دیدار مه رویان چه فرمایی</p></div>
<div class="m2"><p>که نتوان دوخت چشم از دیدن خورشید حربا را</p></div></div>