---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>چون بگسلم نه زلف توکز هر شکنج و بند</p></div>
<div class="m2"><p>تنها ز من هزار دل آوره در کمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان بریدنم ز تو پیوند دوستی</p></div>
<div class="m2"><p>ور بند بندژ من همه از هم جدا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منع از گدای خود مکن ای شه که عیب نیست</p></div>
<div class="m2"><p>هستیم اگر به دولت حسنت نیازمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکسر برون خرام که پنهان کنند روی</p></div>
<div class="m2"><p>از خجلت جمال تو خوبان خود پسند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد برفراز تا همه آیند سر به زیر</p></div>
<div class="m2"><p>سرو و صنوبر و گل و شمشاد و ناروند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پستم چنین به خاک ره ی خویشتن مبین</p></div>
<div class="m2"><p>باشد به دستبوس تو روزی شوم بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان رایگان به پای تو ریزم که بی دریغ</p></div>
<div class="m2"><p>در باب این متاع مرا نیست چون و چند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چل سال یک نفس غمت از من جدا نزیست</p></div>
<div class="m2"><p>بودم به چشم مردم اگر شاد اگر نژند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور از تو خود بگو که صفایی کند چه کار</p></div>
<div class="m2"><p>ناچار اگر همی نتواند دل از تو کند</p></div></div>