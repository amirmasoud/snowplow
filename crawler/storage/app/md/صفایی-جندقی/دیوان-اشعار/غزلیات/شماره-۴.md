---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>اگر رخ تو بدین دست دلبری کندا</p></div>
<div class="m2"><p>به مهر خود مه و خورشید مشتری کندا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن دو جادوی بیمار صد چو جالینوس</p></div>
<div class="m2"><p>به یک کرشمه جانتاب بستری کندا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو چشم کافرت ار خون عالمی بخورند</p></div>
<div class="m2"><p>نه شه نه مفتی اسلام داوری کندا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز روی شرم به زیر افکند سر اول پی</p></div>
<div class="m2"><p>اگر بر سرو تو شمشاد همسری کندا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خود بری به دهان جام و رنه باده تلخ</p></div>
<div class="m2"><p>کجا به آن لب نوشین برابری کندا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرم به پای تو ساید به وقت جان سپری</p></div>
<div class="m2"><p>گرم ستاره مسعود رهبری کندا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بوی قرب درت زنده ام ولی غم هجر</p></div>
<div class="m2"><p>ز جان خویشتنم هر زمان بری کندا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر کند فلک از رشک کام عیشم تلخ</p></div>
<div class="m2"><p>مرا که تنگ دهان تو شکری کندا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز صاف و درد صفایی دهن نیالاید</p></div>
<div class="m2"><p>گرش تو ساقی و لعل تو ساغری کندا</p></div></div>