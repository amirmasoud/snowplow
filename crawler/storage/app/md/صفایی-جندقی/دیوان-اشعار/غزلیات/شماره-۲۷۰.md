---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>تعالی الله یکی دلدار دارم</p></div>
<div class="m2"><p>که با رویش ز گلشن عار دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چهر تابناک آبدارش</p></div>
<div class="m2"><p>گلستان ها گل بی خار دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سرو قد خورشید جمالش</p></div>
<div class="m2"><p>به محفل نخل آتشبار دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم شوخش اندر هر نگاهی</p></div>
<div class="m2"><p>دو مست عاشق بیمار دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مژگان دلاشوبش بهر چشم</p></div>
<div class="m2"><p>دو جعبه تیر بی سوفار دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ابروی کماندارش به سینه</p></div>
<div class="m2"><p>دو قبضه تیغ جوهر دارم دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز درج کامبخش نوشخندش</p></div>
<div class="m2"><p>دو یاقوت شکر گفتار دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سیمین کوی پستانش دو نارنگ</p></div>
<div class="m2"><p>که آن خون ها به دل چون مار دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تأثیر لب شیرین زبانش</p></div>
<div class="m2"><p>نی آسا خامه شکر بار دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گیسوی رسای مشک سایش</p></div>
<div class="m2"><p>چه افعی های مردم خوار دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دست وگردن از زلفش شب و روز</p></div>
<div class="m2"><p>عجب هم سبحه هم زنار دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه نعمت های جان بخش از وجودش</p></div>
<div class="m2"><p>که بایستم نهان ز اغیار دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل از مهرش نپردازم صفایی</p></div>
<div class="m2"><p>که من با او هزاران کار دارم</p></div></div>