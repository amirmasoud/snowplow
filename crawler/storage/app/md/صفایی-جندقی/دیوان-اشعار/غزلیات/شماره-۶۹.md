---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>با ساقی و مطرب همه را کار به کام است</p></div>
<div class="m2"><p>کاشانه ی ما بزم جم از دولت جام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقصان به غم افتاد و روان را طرب افزود</p></div>
<div class="m2"><p>تا شاهد روز و شبم آن ماه تمام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدگیسوی او سور مرا، سیرت سوکی</p></div>
<div class="m2"><p>بی طلعت او صبح مرا صورت شام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فسق همه از رأفت او پرده ی پرهیز</p></div>
<div class="m2"><p>ننگ همه از نعمت او شوکت نام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی سرمه ی خاک در او چشم خرد را</p></div>
<div class="m2"><p>دیدار خطا، عدل جفا، نور ظلام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دیده ی و دل از دو جهان نام و نشان نیست</p></div>
<div class="m2"><p>تا گوش و زبان راهی از نام توکام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با عشق امل سوز دگر علم و عمل چیست</p></div>
<div class="m2"><p>یا عاقبت اندیشی و تدبیر کدام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مرغ دل از خال لبش خام نگردی</p></div>
<div class="m2"><p>هشدار و بیندیش از این دانه که دام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با سابقه ی لطف قدیمم چو صفایی</p></div>
<div class="m2"><p>معشوق رضا، شاه گدا، خواجه غلام است</p></div></div>