---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>سروقامت قیام دیگر کرد</p></div>
<div class="m2"><p>دو قیامت به ساعتی برکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم قیامت ز قد دلکش ساخت</p></div>
<div class="m2"><p>هم کرامت ز چهر انور کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک در چشم ماه نخشب ریخت</p></div>
<div class="m2"><p>بند بر پای سرو کشمر کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه با عقل کرد سطوت عشق</p></div>
<div class="m2"><p>کافرم مسلم ار به کافر کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرم بادش ز نوش خنده ی تو</p></div>
<div class="m2"><p>هرکه یاد از نبات و شکر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکند صد قرابه می با ما</p></div>
<div class="m2"><p>آنچه آن چشم سحر پرور کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لعل سیراب و چهر سمائیت</p></div>
<div class="m2"><p>قانعم از بهشت وکوثر کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب و دندان شهد پرور تو</p></div>
<div class="m2"><p>حلقه در گوش لعل و گوهر کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نی خامه ام به ذکر لبت</p></div>
<div class="m2"><p>هر دم از نو بیان دیگر کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه عجب هرکه قند ریزد باز</p></div>
<div class="m2"><p>خوب تر هر چه را مکرر کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلک نامحرم صفایی باز</p></div>
<div class="m2"><p>داستان ها ز خون دل سر کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می ندانم که این بریده زبان</p></div>
<div class="m2"><p>چون سر از سر سینه ام در کرد</p></div></div>