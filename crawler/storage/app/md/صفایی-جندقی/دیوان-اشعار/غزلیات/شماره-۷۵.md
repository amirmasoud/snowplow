---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>مرا درد از شکیبایی فزون است</p></div>
<div class="m2"><p>که دل در سینه چندین بی سکون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز احوالم چه پرسی کاشک خونین</p></div>
<div class="m2"><p>گواهم بر جراحات درون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگوخود بی تو چون پایم که در هجر</p></div>
<div class="m2"><p>عنان طاقت از دستم برون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهل بار فراقم بردل ریش</p></div>
<div class="m2"><p>که آن غم بیش از این یک قطره خون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا دور از لبت هر لحظه در جام</p></div>
<div class="m2"><p>به جای می سرشک لاله گون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عین رحمتم یک ره نظر کن</p></div>
<div class="m2"><p>که اشکم در رهت رشک عیون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدا را از دل خود پرس باری</p></div>
<div class="m2"><p>که شوقم بر وصالت چند و چون است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا گه درد و گه درمان فرستاد</p></div>
<div class="m2"><p>ندانستم که عشقت ذوفنون است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوری در دوستی خونم دریغا</p></div>
<div class="m2"><p>که چرخم خصم و بختم باژگون است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سودایت نشاطی دارم اما</p></div>
<div class="m2"><p>نشاطی کم به صد غم رهنمون است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عهد زر به مهرت بسته میثاق</p></div>
<div class="m2"><p>نه این سودا مرا در سر کنون است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفایی را ز رسوایی مترسان</p></div>
<div class="m2"><p>که با عشقت خردمندی جنون است</p></div></div>