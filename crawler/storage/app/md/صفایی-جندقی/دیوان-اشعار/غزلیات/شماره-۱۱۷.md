---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>نقاب از تمام رخ بتی شوخ برگرفت</p></div>
<div class="m2"><p>دل نیم سوز ما از این شعله در گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتان با کمان و تیر ز مردم ربوده دل</p></div>
<div class="m2"><p>بت من هزار دل به لعل و گهر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرآنان دل کسان به حنظل برند و زهر</p></div>
<div class="m2"><p>دل از من نگار من به شهد و شکر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وفا داریم چو دید پسندید و برگزید</p></div>
<div class="m2"><p>ز دل دادگان همه مرا در نظر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خشک و تر از دو کون کسی سود و صرفه برد</p></div>
<div class="m2"><p>که خاک در تو را به آب خضر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرد خیره و خجل شد از نقص این کمال</p></div>
<div class="m2"><p>که خاک ره ترا برابر به زر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفایی یکی به چرخ بگو دیگر آفتاب</p></div>
<div class="m2"><p>نتابد که شهر ما فروغ از قمر گرفت</p></div></div>