---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>تاهمچو من از عشق گرفتار نگردی</p></div>
<div class="m2"><p>از کشمکش هجر خبردار نگردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نشماری غم عشاق به یک مو</p></div>
<div class="m2"><p>تا بسته ی آن طره طرار نگردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارت به طبیبان جفا پیشه نیفتاد</p></div>
<div class="m2"><p>کآگاه ز حال دل بیمار نگردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زلف سیه روز و پریشان شوی ای دل</p></div>
<div class="m2"><p>زنهار دگر گرد رخ یار نگردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن مکن آلوده به خون دلم ای خواب</p></div>
<div class="m2"><p>پیرامن این دیدهٔ بیدار نگردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسم که بسوزی تو هم از آتشم ای غم</p></div>
<div class="m2"><p>گرد دل پرتاب و شرربار نگردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر در قدم یار عزیزت نکند جای</p></div>
<div class="m2"><p>تا خاک صفت در ره او خوار نگردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر حیرت من بین و از آن روی نظر بند</p></div>
<div class="m2"><p>تا فتنهٔ آن جادوی سحار نگردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا سینه ز فکر رخ و زلفش نکنی پر</p></div>
<div class="m2"><p>خالی ز خیال بت و زنار نگردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آن می که صفایی اثرش بی خودی آمد</p></div>
<div class="m2"><p>شرط خرد آن است که هشیار نگردی</p></div></div>