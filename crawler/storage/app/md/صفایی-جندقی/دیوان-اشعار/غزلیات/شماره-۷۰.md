---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>آن باده که در ملت اسلام حرام است</p></div>
<div class="m2"><p>از دست تو نوشم خود اگر ماه صیام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوته نظر ار ماه تمامت به مثل خواند</p></div>
<div class="m2"><p>این نقص بس آنرا که نه از اهل کلام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوبی به قدت حیرت و حورا به رخت مات</p></div>
<div class="m2"><p>پیش تو صنوبر که و خورشیدکدام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالای ترا پست بود نسبت شمشاد</p></div>
<div class="m2"><p>با آن چم و خم حیف که قاصر ز خرام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با قامت موزون چه کند سروکه ناچار</p></div>
<div class="m2"><p>همواره به یک پای گرفتار قیام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک رشحه ز لعل تو و صد ساغر لبریز</p></div>
<div class="m2"><p>با نشأه وی مستی می را چه دوام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگشا سوی گلشن ز قفس بال و پرم را</p></div>
<div class="m2"><p>کز باغ ارم خوشترم این گوشه ی دام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر صدر دل از دیر و حرم نیست مقیمی</p></div>
<div class="m2"><p>تا خیل غمت را به صف سینه مقام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز از چه کشیدی خط زنگار برابرو</p></div>
<div class="m2"><p>افزون کشی امروزکه تیغت به نیام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خشک و ترم از آتش دل سوخت صفایی</p></div>
<div class="m2"><p>با عشق چه جای سخن از پخته و خام است</p></div></div>