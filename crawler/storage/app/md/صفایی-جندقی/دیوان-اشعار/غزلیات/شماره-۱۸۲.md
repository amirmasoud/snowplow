---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>باید دلی که درک معانی کند ز پند</p></div>
<div class="m2"><p>باید سری که گوش دهد پند سودمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوشی مرا که فهم سخن می نمود نیست</p></div>
<div class="m2"><p>دیوانه را چه سود سرون به گوش پند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او خود نبود حور و بر این حیرتم که بود</p></div>
<div class="m2"><p>پیدا چو سمع ساعد سیمینش از پرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آن زلف مشکسا گرهی بر رخم گشای</p></div>
<div class="m2"><p>گر بسته ای کمر که رهانی دلم ز بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آن چهر و قد چو سرو و چو سوری که حسن یار</p></div>
<div class="m2"><p>حجت فراشت در نظر کوته و بلند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوارم اگر به چشم تو دارم ولی امید</p></div>
<div class="m2"><p>کز عز خاکبوس درت گردم ارجمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افغان و اشک و انده و آسیب تا به کی</p></div>
<div class="m2"><p>تاب و توان و طاقت و تسلیم تا به چند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامد صفایی ار مژگان منع اشک ما</p></div>
<div class="m2"><p>کس کی به راه سیل ز خاشاک بسته بند</p></div></div>