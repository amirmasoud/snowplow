---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>دور پیمانه را بقایی نیست</p></div>
<div class="m2"><p>عهد جانانه را وفایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزد خوبان وفای عاشق را</p></div>
<div class="m2"><p>جاودان جز جفا جزایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوق را پایه سخت و پنجه قوی</p></div>
<div class="m2"><p>صبر را پای از آن به جایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصل یا مردن است چاره ی هجر</p></div>
<div class="m2"><p>دیگر این درد را دوایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریز خونم که در شریعت عشق</p></div>
<div class="m2"><p>کشته را برتو خون بهایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر لله که از عنایت دوست</p></div>
<div class="m2"><p>به دو کیهانم اعتنایی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تغافل ملامتت نکنم</p></div>
<div class="m2"><p>پادشه را غم از گدایی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش بازار حسن زهره ما</p></div>
<div class="m2"><p>مشتری را به کف بهایی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با سهیل ستاره سوز رخش</p></div>
<div class="m2"><p>ماه را تابش سهایی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی تواند کشید در آغوش</p></div>
<div class="m2"><p>مهر و مه را که دست و پایی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه شهرش به جان طلبکارند</p></div>
<div class="m2"><p>با صفایی ترا صفایی نیست</p></div></div>