---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>گر به سودای محبت رفت در پا جان مرا</p></div>
<div class="m2"><p>نیست غم کآمد بحمدالله به سر جانان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاگدای لعل شیرینت شدیم از یاد رفت</p></div>
<div class="m2"><p>حشمت اسکندر وسرچشمه ی حیوان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست برخاک درت با این سرشک تلخ و شور</p></div>
<div class="m2"><p>شوق جوی سلسبیل و روضه ی رضوان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به حشرم بی تو در فرخای جنت جا دهند</p></div>
<div class="m2"><p>شکر باشد به جان از گوشه ی زندان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلشنم بی چهر و خطت گرنه گلخن از چه روی</p></div>
<div class="m2"><p>خارها در دیده گوید لاله و ریحان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز تنگ لعل نوشینت چرا نامد برون</p></div>
<div class="m2"><p>گر نه خون آلود آن یاقوت شد دندان مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک دل از من پیش نگرفتی چرا از سیم اشک</p></div>
<div class="m2"><p>در عوض هر چشمزد پر میکنی دامان مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نز تو کام من بد آمد نز دل من کار تو</p></div>
<div class="m2"><p>داشتی در کوی خود یک عمر سرگردان مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صدر اسلام ار صفایی کافرم خواند چه نقص</p></div>
<div class="m2"><p>حق نخواهد برخلاف ظن او بطلان مرا</p></div></div>