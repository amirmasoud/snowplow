---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>در هجر توام سری به جان نیست</p></div>
<div class="m2"><p>با وصل تویادم از جهان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکم به جروح دل گواه است</p></div>
<div class="m2"><p>محتاج به شرح ترجمان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تادر قفس غمت فتادم</p></div>
<div class="m2"><p>دیگر هوسم به بوستان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با یاد تو ذوق گلستان نه</p></div>
<div class="m2"><p>در دام تو شوق آشیان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر وصل چو خود فشانمت جان</p></div>
<div class="m2"><p>حاجت به فراق جانستان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاخی که بریزد از بهاران</p></div>
<div class="m2"><p>محتاج به غارت خزان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیری که نه چون تواش دلارام</p></div>
<div class="m2"><p>چون سعد من اخترش جوان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل دادم و جان به دل ستادم</p></div>
<div class="m2"><p>سود آور عشق را زیان نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در راه محبتت صفایی</p></div>
<div class="m2"><p>در بند حیات جاودان نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پا بر سر جان و تن چو بنهاد</p></div>
<div class="m2"><p>زین بیش مجال امتحان نیست</p></div></div>