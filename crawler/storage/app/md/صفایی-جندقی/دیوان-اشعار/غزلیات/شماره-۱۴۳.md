---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>قلم تا سر ز سودای تو درکرد</p></div>
<div class="m2"><p>ورق را رخ ز خون دیده تر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان دود سیه کز خامه برخاست</p></div>
<div class="m2"><p>از این آتش جهانی را خبر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فر حسن جانان عشق جان سوز</p></div>
<div class="m2"><p>مرا با فرط گم نامی سمر کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم آن یار هر جایی دریغا</p></div>
<div class="m2"><p>که آخر همچو خویشم دربدر کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نامیزد مهی کز یک تجلی</p></div>
<div class="m2"><p>جهان رامات و حیران سر به سر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زنجیر سر زلفش در آن روی</p></div>
<div class="m2"><p>مرا از زلف خود دیوانه تر کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز غم شادم که هر دم ز اشک و رخسار</p></div>
<div class="m2"><p>کنار و دامنم پر سیم و زر کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم تا بر کمر دیدت سر زلف</p></div>
<div class="m2"><p>چو زلف سرکشت رو برکمر کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه من تنها ز لعلت می خورم خون</p></div>
<div class="m2"><p>لبت یاقوت را خون درجگر کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهانت بس که مطبوع است و شیرین</p></div>
<div class="m2"><p>ز غیرت زهره در کام شکر کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفایی در غمش مردیم و رستیم</p></div>
<div class="m2"><p>اجل غوغای ما را مختصر کرد</p></div></div>