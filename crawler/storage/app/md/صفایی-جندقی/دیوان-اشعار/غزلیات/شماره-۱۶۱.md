---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>کس را چه تمنا که خریدار تو باشد</p></div>
<div class="m2"><p>کش نیست بهایی که به مقدار تو باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان چبود و تن کیست دریغا که متاعی</p></div>
<div class="m2"><p>در دست ندارم که سزاوار تو باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن طالب صورت خبرش نیست ز معنی</p></div>
<div class="m2"><p>بیش و کم اگر در غم آزار تو باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دادن یک دل به تو حاصل نکند کام</p></div>
<div class="m2"><p>از جان گذرد هر که طلب کار تو باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنهار اگر دست گشایی پی خونریز</p></div>
<div class="m2"><p>اول بکش آن را که به زنهار تو باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دام از چمنش بهتر و بندش ز رهایی</p></div>
<div class="m2"><p>خوش بخت اسیری که گرفتار توباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیشش همه نوش آمد و دردش همه داروست</p></div>
<div class="m2"><p>دردی کش غم کیش که بیمار تو باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کالای وفا قحط شد از فقد خریدار</p></div>
<div class="m2"><p>این جنس مگر در صف بازار تو باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستی ز دلازاری احباب نگهدار</p></div>
<div class="m2"><p>تا پاس وفا حرز و نگهدار تو باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می باش خدا را به صفا یار صفایی</p></div>
<div class="m2"><p>الطاف خدایی همه جا یار تو باشد</p></div></div>