---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>با صبا همره است نکهت یار</p></div>
<div class="m2"><p>یا به جیبش نهفته مشک تتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر از خاک یار کرده عبور</p></div>
<div class="m2"><p>که وزد بوی خون ز باد بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق در دل مرا نهایی گشت</p></div>
<div class="m2"><p>کو غمم برگ و رنجم آرد بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روید از شاخه هاش بند به بند</p></div>
<div class="m2"><p>عوض هر گلم هزاران خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشت بر ما ز سختی غم هجر</p></div>
<div class="m2"><p>مردن آسان و زندگی دشوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل عنانم گرفت از کف و رفت</p></div>
<div class="m2"><p>من ز پی نیز رفتمش ناچار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کجا پای او به سنگ آید</p></div>
<div class="m2"><p>کاین چنین می رود گسسته مهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش بیگانگان نامحرم</p></div>
<div class="m2"><p>لب نشاید گشودن از اسرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو صفایی زبن ببند و مگوی</p></div>
<div class="m2"><p>از حدیث دل اندک و بسیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشفقی اهل دل رفیق طریق</p></div>
<div class="m2"><p>تا نیابی خموش زی زنهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راستی را چو مدعی کژ خواند</p></div>
<div class="m2"><p>بی سخن خامشی به از گفتار</p></div></div>