---
title: >-
    شمارهٔ ۳۱۹
---
# شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>نگویم با من از روی حقیقت دوست داری کن</p></div>
<div class="m2"><p>به دل نیز ار نباشد گاه گاه اظهار یاری کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا دانم نداری دوست در بزم رقیب اما</p></div>
<div class="m2"><p>به رغم دشمنم گاهی حدیث غم گساری کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چشم خود مدار دل درین خون خوردنم بنگر</p></div>
<div class="m2"><p>ز زلف خود قیاس حالتم در بی قراری کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تیر غمزه ات هرگز چنین از پای نفتادم</p></div>
<div class="m2"><p>بیا تدبیر من با این جراحت های کاری کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار خود مگر با گریه از دل ها فرو شویم</p></div>
<div class="m2"><p>تو نیز ای دیده امدادی مرا در اشکباری کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رقیبان خفته ناصح رفته یاران غافل ای کوکب</p></div>
<div class="m2"><p>بیا یک شب خلاف عهد ترک تیره کاری کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ششدر ماتم از نرد شش و پنجت یکی با من</p></div>
<div class="m2"><p>تو ای چرخ مشعبد ترک چندین بد قماری کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا در عمر خود نگذاشتم تنها تو نیز امشب</p></div>
<div class="m2"><p>به پاداش رفاقت با من ای غم حق گزاری کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بتم در فکر دل جویی و من سرگرم جان بازی</p></div>
<div class="m2"><p>تو هم یک امشب ای بخت صفایی سازگاری کن</p></div></div>