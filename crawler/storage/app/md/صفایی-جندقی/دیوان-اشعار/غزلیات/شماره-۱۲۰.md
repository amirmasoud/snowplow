---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>گر به خواری خون من ریزد به خاک آستانت</p></div>
<div class="m2"><p>به که بردارم به حسرت سر ز پای پاسبانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور به من داری روا جوری که بر دشمن پسندی</p></div>
<div class="m2"><p>آن قدر کآید مصور دوست دارم بیش از آنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایمردی گر کند لطف قدیمم روزگاران</p></div>
<div class="m2"><p>روی مالم بآستینت فرق سایم بآستانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با همه کین در ره ی عشق تو دادم دین و دنیا</p></div>
<div class="m2"><p>وه چه خواهم کرد با خود ببینم مهربانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد ما را در غم عشقت عجب صبری سبک پا</p></div>
<div class="m2"><p>آنکه کرد از اقتضای حسن چندین سرگرانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه دقت ها ز مو باریک تر کردیم عمری</p></div>
<div class="m2"><p>باز دانم نکته ها سر بسته در دل از میانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصف ما پیش کمالت ناقص است اما چه حاصل</p></div>
<div class="m2"><p>میهمان خود که ز آن معنی که می زیبد به شانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش چشم شور بختان تا توانی رو ترش کن</p></div>
<div class="m2"><p>کز سخن تلخی برد بیرون لب شیرین زبانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنج هجران را صفایی صبر ورزیدن چه جبران</p></div>
<div class="m2"><p>چاره ی این غم ندانم جز به جانان بذل جانت</p></div></div>