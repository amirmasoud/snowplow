---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>فغان کآغاز عشق و آشنایی</p></div>
<div class="m2"><p>ز جانان بایدم جستن جدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رقیبم مدعی شد ورنه در سر</p></div>
<div class="m2"><p>نبود او را هوای بی وفایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>براو آسان رسوم مهربانی</p></div>
<div class="m2"><p>براو روشن رموز آشنایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا در کوی جانان گر گذارند</p></div>
<div class="m2"><p>گدایی خوشتر است از پادشایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک تن عشق او صد گنج غم داد</p></div>
<div class="m2"><p>از این در بایدم کردن گدایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاک پای خود در خون من کاش</p></div>
<div class="m2"><p>فرو بردی سرانگشت حنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به قتلم حکم کن کز قید فرمان</p></div>
<div class="m2"><p>نپیچم سر به هر بی اعتنایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بازار غمت جان را چه قیمت</p></div>
<div class="m2"><p>که نقشش می دهند از ناروایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر صورت خدا بخشد ترا کام</p></div>
<div class="m2"><p>که من خو کرده ام با بینوایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وفا خوب است و از خوبان نکوتر</p></div>
<div class="m2"><p>جفا بد باشد الا بر صفایی</p></div></div>