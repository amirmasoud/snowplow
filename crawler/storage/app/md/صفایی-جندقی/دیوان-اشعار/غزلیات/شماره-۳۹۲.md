---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>راه دیار یا مرا ای صبا بپوی</p></div>
<div class="m2"><p>از تاب هجر و حسرت وصل منش بگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای سست عهد ماه ستمکار کند مهر</p></div>
<div class="m2"><p>وی سخت خشم یار دلازار تند خوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شاه زود رنج من ای یار دیر صلح</p></div>
<div class="m2"><p>ای ماه مهر سوز من ای ترک جنگجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار رقیب بازم و شاه عدو نواز</p></div>
<div class="m2"><p>ترک بهانه سازم و ماه لطیفه گوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی شکستگی دل از حسرتم اگر</p></div>
<div class="m2"><p>وقتی به تجربت زده ای سنگ بر سبوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزدیک شد به گردن جانم طناب مرگ</p></div>
<div class="m2"><p>تا دورم از کشاکش آن زلف مشکبوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زد زخمه ی فراق تو زخمی بدل مرا</p></div>
<div class="m2"><p>کز تار موی و سوزن مژگان سزد رفوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق تو و تن من باد وزان و خاک</p></div>
<div class="m2"><p>شوق تو و دل من آب روان و جوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با فر عشق کش همه شاهان گدای در</p></div>
<div class="m2"><p>شد جان و سر به کوی تو کمتر ز خاک کوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکبار در تو آه صفایی اثر نکرد</p></div>
<div class="m2"><p>آه از دلت که سخت تر از آهن است و روی</p></div></div>