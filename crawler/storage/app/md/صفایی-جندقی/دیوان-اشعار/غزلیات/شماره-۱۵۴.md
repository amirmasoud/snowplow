---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>سحرگهان که صبا نافه گستری می کرد</p></div>
<div class="m2"><p>به باغ گل ز غمت پیرهن دری می کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بوی موی تو سنبل به خویش می پیچید</p></div>
<div class="m2"><p>به شوق روی تو بلبل سخنوری میکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوا به رنگرزی درچمن چو بر می خاست</p></div>
<div class="m2"><p>غمت به روی من آغاز زرگری میکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان امید که گردد بهای خاک درت</p></div>
<div class="m2"><p>رخم مقابله با زر جعفری می کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توخود به دست کرامت ز پای بنشستی</p></div>
<div class="m2"><p>وگرنه سرو کجا با توهمسری می کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر بدو توخود انداختی وگرنه کجا</p></div>
<div class="m2"><p>به چشم شوخ تو نرگس برابری میکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفایی از همه خیل خط فروشان کاش</p></div>
<div class="m2"><p>خودی ز روی صفا از ریا بری می کرد</p></div></div>