---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>کنون که سایهٔ تیغ تو بر سر است مرا</p></div>
<div class="m2"><p>چه غم ز تابش خورشید محشر است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قتل من نهراسی ز داد خواهی غیر</p></div>
<div class="m2"><p>که خون بها ز تو یک زخم دیگر است مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کفن قبا کنم از شوق در قیامت نیز</p></div>
<div class="m2"><p>که این لباس در آنجا نه در خور است مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرم به حلقه ی فتراک اگر چه بربندی</p></div>
<div class="m2"><p>که باز شور وفای تو در سر است مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به حشر هم از لعل و رخ دهی کامم</p></div>
<div class="m2"><p>چه احتیاج به فردوس وکوثر است مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمال شوق عنانم زکف رها نکند</p></div>
<div class="m2"><p>وگرنه نقض وفای تو باور است مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریغ کز شش و پنج سپهر شعبده باز</p></div>
<div class="m2"><p>مدام مهره ی طالع به ششدر است مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عتاب خشم پسنان عنایت است و خوشم</p></div>
<div class="m2"><p>به بخت خویش که یاری ستمگر است مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم خوش است صفایی به صبح وصل هنوز</p></div>
<div class="m2"><p>که شام هجر صبوری میسر است مرا</p></div></div>