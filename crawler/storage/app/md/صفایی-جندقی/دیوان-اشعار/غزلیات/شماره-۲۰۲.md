---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>ازین پس دور من چندی نپاید</p></div>
<div class="m2"><p>مگر دوران هجرانم سرآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا زان حسن روز افزون که جان کاست</p></div>
<div class="m2"><p>به دل هر روز صد حسرت فزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از مرگم به بالین آمد آری</p></div>
<div class="m2"><p>قیامت گر چه دیر آید بیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل بی خار آن رخ دید و بلبل</p></div>
<div class="m2"><p>بلادت بین که بر گلبن سراید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرانگشت نگارین هر که دیدش</p></div>
<div class="m2"><p>سر انگشت از حیرت بخاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر دیدی است مه را پیش آن چهر</p></div>
<div class="m2"><p>چرا خود را به مردم می نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقاب از روی دلبند ار کند باز</p></div>
<div class="m2"><p>در فردوس بر عالم گشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن حیرت که آن حور پری زاد</p></div>
<div class="m2"><p>مرا دیوانه و حیران نماید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرم جویی شفا زین دردمندی</p></div>
<div class="m2"><p>شرابم شربت آن لعل باید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یاقوت تو مرجان ماندی حیف</p></div>
<div class="m2"><p>که زو این نکته پردازی نیاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفایی را چه سود از پند ناصح</p></div>
<div class="m2"><p>بگوتا بندم از دل برگشاید</p></div></div>