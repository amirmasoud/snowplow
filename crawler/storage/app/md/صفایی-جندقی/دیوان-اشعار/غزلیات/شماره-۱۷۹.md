---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>برس به داد دل ما که پادشاهی چند</p></div>
<div class="m2"><p>رسیده اند به فریاد داد خواهی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی به زخمی و گاهم به مرهمی بنواز</p></div>
<div class="m2"><p>به سوی ما فکن از مهر و کین نگاهی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشد فراق چو ما را بدو سپار چرا</p></div>
<div class="m2"><p>به گردن تو فتد خون بی گناهی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خون من چه بود پاسخت که هست مرا</p></div>
<div class="m2"><p>ز زلف و غمزه و خال و رخت گواهی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کدام شب به امیدی که روز از آن گذری</p></div>
<div class="m2"><p>به سر نکرده ام از عمد خاک راهی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صید صوفی و زاهد به دیر و کعبه خرام</p></div>
<div class="m2"><p>خراب ساز کلیسا و خانقاهی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حلقه حلقه ی گیسو بری ز ره دل ما</p></div>
<div class="m2"><p>به راه خلق کنی از طناب چاهی چند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دیده طره ی ترکان مرا نماید راست</p></div>
<div class="m2"><p>ز خجلت خم زلف تو رو سیاهی چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مبند باد به چنبر، به هاون آب مسای</p></div>
<div class="m2"><p>چسود زین دو صفایی در اشک و آهی چند</p></div></div>