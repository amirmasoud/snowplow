---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>هر شب از یاد زلف و طره ی یار</p></div>
<div class="m2"><p>پر بود بسترم ز عقرب و مار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز مژگان و سینه ام ز ابروی</p></div>
<div class="m2"><p>همه شمشیر بار و پیکان زار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر و بالا بلند و پست نگر</p></div>
<div class="m2"><p>خال و خطش قرین زلف و عذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خال مشکین و چهر گلشن سیر</p></div>
<div class="m2"><p>زلف پرتاب و خط سوسن سار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر موری فتاده در برگل</p></div>
<div class="m2"><p>دم ماری نهاده بر سر خار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا خلیلی طپیده در آتش</p></div>
<div class="m2"><p>یا کلیمی گرفته در کف مار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هندویی دست بسته با زنجیر</p></div>
<div class="m2"><p>و آفتابی نشسته در زنجار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چهر او بین اگر ندیدستی</p></div>
<div class="m2"><p>آتش سرد و آب آتشبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب و دندان او مگو به مثل</p></div>
<div class="m2"><p>در خندان و لعل شکر خوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقل شور و شراب شیرین است</p></div>
<div class="m2"><p>امتحان را چشیده ام صد بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا صفایی اسیر عشق نشد</p></div>
<div class="m2"><p>نشد از بخت خویش برخوردار</p></div></div>