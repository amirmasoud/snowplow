---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>ز میر کعبه زرقی چند دیدم</p></div>
<div class="m2"><p>که زین کافر مسلمانی رمیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکندم خرقهٔ تلبیس و طامات</p></div>
<div class="m2"><p>به تن پیراهن تقوی دریدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعار زهد آوردم به بر چاک</p></div>
<div class="m2"><p>قبای ننگ و بدنامی بریدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مفتی بسکه بردم بوی سالوس</p></div>
<div class="m2"><p>سر از دلق ریایی برکشیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشیدم خط رسوایی به رخسار</p></div>
<div class="m2"><p>ز بدگویان ملامت ها شنیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خراباتم برید از خانقه راه</p></div>
<div class="m2"><p>که زین سو سیرت انصاف دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه پیر دیر از میر حرم خاست</p></div>
<div class="m2"><p>که حسرت حاصل آمد از امیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن مغرور این رندم خوش افتاد</p></div>
<div class="m2"><p>که از وی بوی غم خواری شنیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحمدالله که محکم پنجه زد دست</p></div>
<div class="m2"><p>به ذیل مرد آگاهی رسیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سربازی صفایی شد سرافراز</p></div>
<div class="m2"><p>از آن خواری بدین عزت رسیدم</p></div></div>