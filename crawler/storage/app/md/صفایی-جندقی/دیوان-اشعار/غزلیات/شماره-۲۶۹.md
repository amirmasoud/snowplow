---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>بنامیزد بتی عیار دارم</p></div>
<div class="m2"><p>که با او از دو عالم عار دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه غم گر دارم از وی دیده خونبار</p></div>
<div class="m2"><p>که چونان لعبتی خونخوار دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تیغش تا قیامت سینه مجروح</p></div>
<div class="m2"><p>ز لعلش دیده گوهر بار دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عکس عادت از یک سرو بالاش</p></div>
<div class="m2"><p>به یک کاشانه صد گلزار دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزارش گل به بار اما چه حاصل</p></div>
<div class="m2"><p>که از هر غنچه اش صد خار دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم با خود نبردی زانکه دیدی</p></div>
<div class="m2"><p>غم صد مرده بر وی بار دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراقم کاست از هر در ولی شکر</p></div>
<div class="m2"><p>که غم از دولتت بسیار دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه خلق جهان نالند ز اغیار</p></div>
<div class="m2"><p>خلاف من که داد از یار دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو ناصح به زهد و پارسایی</p></div>
<div class="m2"><p>مخوانم غیر این هم کار دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الا یار وفا پرور ندانی</p></div>
<div class="m2"><p>کت از غم تاکجا تیمار دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شرح تاب و تیمار جدایی</p></div>
<div class="m2"><p>بیا برخوان که صد طومار دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفایی فسق و زهد از من میاموز</p></div>
<div class="m2"><p>که زین کفر و دین انکار دارم</p></div></div>