---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>دوش از آن باده که پیمود بطی جانانم</p></div>
<div class="m2"><p>نه چنان بیخود و مستم که سر از پا دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست از دامنم ای بدرقه بردار و برو</p></div>
<div class="m2"><p>که سفر کردن از این در قدمی نتوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دولت و زندگیم جان و دلی بیش نبود</p></div>
<div class="m2"><p>دل فکندیم به پا تا چه رود بر جانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات مغان گم شده ای هست مرا</p></div>
<div class="m2"><p>بی جهت نیست که آشفته و سرگردانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر از مرگ کنم چاره غم هجران را</p></div>
<div class="m2"><p>که جز این صارفه مشکل نکند آسانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده بودند مرا از دو جهان دین و دلی</p></div>
<div class="m2"><p>این خود ازکف شد و دل کند بباید زآنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رایگان داده ز کف گوهر و بر خامی خویش</p></div>
<div class="m2"><p>مضطر و سوخته و غمزده و حیرانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد و درمان همه از تست خدا را مپسند</p></div>
<div class="m2"><p>که من از چاره ی درد تو به خود درمانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحر جادوت ز خود بی خبرش کرد و ربود</p></div>
<div class="m2"><p>ورنه دل بود کجا ایمن از آن مژگانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تب و تابم مکن انکار صفایی زنهار</p></div>
<div class="m2"><p>کآب چشم آمده بر آتش دل برهانم</p></div></div>