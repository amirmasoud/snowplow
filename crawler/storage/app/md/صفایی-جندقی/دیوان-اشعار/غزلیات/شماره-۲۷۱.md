---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>به گردون رشته تا زان دو زلف پر شکن دارم</p></div>
<div class="m2"><p>چرا بر وش یک مومنت از مشک ختن دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از چاه زنخدانت برآید کی معاذ الله</p></div>
<div class="m2"><p>ز پیچان طره ات با آنکه صد مشکین رسن دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشد چشم درماندم ز جایی درد عشقت را</p></div>
<div class="m2"><p>کجا مرهم پذیر افتد چنین زخمی که من دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دل تا در بغل نارم به یک پیراهنت با خود</p></div>
<div class="m2"><p>چو گل هر روز چاک از داغ رویت پیرهن دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آنجا در وثاقت کام بخشایی رقیبان را</p></div>
<div class="m2"><p>من اینجا در فراقت سر به زانوی محن دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بوی پیرهن مشتاق و محتاجم بشیری کو</p></div>
<div class="m2"><p>که درکوی غمت هر گام صد بیت الحزن دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سروقت دلم در لب رهی بنما که من با وی</p></div>
<div class="m2"><p>حکایت های خونین از درون خویشتن دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بالینم شبی تا روز بنشین و آتشم بنشان</p></div>
<div class="m2"><p>که با لعلت نهانی بی زبان چندین سخن دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث حسنت از مردم نهان ماند مگر چندی</p></div>
<div class="m2"><p>به بزم خویش و بیگانه از آن پاس دهن دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غریب آسا هنوز از نو سر بیگانگی داری</p></div>
<div class="m2"><p>به من با آنکه عمری شد که در کویت وطن دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفایی سال و ماه و هفته چون باید جدایی را</p></div>
<div class="m2"><p>تواند زیست او یک روز بی رویت نپندارم</p></div></div>