---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>بیا بنگر سرشک اشکباران</p></div>
<div class="m2"><p>که تا چون برده آب از اشک باران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز راز ما مگر آگه نکردند</p></div>
<div class="m2"><p>بهر نام مرا کمتر ز یاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا را تا ز هجرانم رهانی</p></div>
<div class="m2"><p>مرا اول بکش زین جان نثاران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به امیدی به پایت سر سپردم</p></div>
<div class="m2"><p>که سایی پا به فرق سر سپاران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خواهی که نومیدت نمانند</p></div>
<div class="m2"><p>ترحم کن براین امیدواران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و دین تا به یک مجلس ببازند</p></div>
<div class="m2"><p>در آ در خلوت پرهیزگاران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جای باده بنهادی از آن لب</p></div>
<div class="m2"><p>چه منت ها به دوش باده خواران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرامت را خجل نبود اگر کبک</p></div>
<div class="m2"><p>چرا ناید به شهر از کوهساران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس کز خجلت قدت عرق ریخت</p></div>
<div class="m2"><p>به گل شد پای سرو جویباران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا بس چهرت از بهر تماشا</p></div>
<div class="m2"><p>گلستان را گذارم با هزاران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفایی را ز زاری می کنی منع</p></div>
<div class="m2"><p>غریق بحر کی ترسد ز باران</p></div></div>