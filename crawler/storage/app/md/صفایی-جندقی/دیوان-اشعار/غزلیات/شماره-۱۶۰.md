---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>بی تکلف دل ندارد هر که دلدارش نباشد</p></div>
<div class="m2"><p>از حیاتش چیست حاصل هر که او یارش نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل گل را هر که نارد احتمال خار هجران</p></div>
<div class="m2"><p>از دی و دی زین گلستان بهره جز خارش نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف را لابد شکست است از پریشانی ولیکن</p></div>
<div class="m2"><p>این سه فیروز افتد گر چه سردارش نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو بستانی که همسر می ستایندت به بالا</p></div>
<div class="m2"><p>یک قدم همراه سروت پای رفتارش نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش عذر رقیب است امشب بخواه از بزم گفتا</p></div>
<div class="m2"><p>روز هم دارد خطرها گنج اگر مارش نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه خاموشی گزید ازبی زبانی یا ز خجلت</p></div>
<div class="m2"><p>پیش آن نوشین دهان یارای گفتارش نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پنجه افکندم به سیمین ساعدی کز خیل مژگان</p></div>
<div class="m2"><p>رستم رویین تن افکن مرد پیکارش نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی جمالت بر صفایی روز روشن تیره شب شد</p></div>
<div class="m2"><p>تا توهم خوابش نگردی بخت بیدارش نباشد</p></div></div>