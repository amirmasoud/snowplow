---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>دلا خواهی توجه با خدا کرد</p></div>
<div class="m2"><p>تغافل بایدت از ماسوا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدی از دوست با نیکی ز بدخواه</p></div>
<div class="m2"><p>خطا بردی گمان کاینها خدا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا نشناخت پیغمبر ندانست</p></div>
<div class="m2"><p>هر آنکس کاین دو را از هم جدا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ادای شکر حق باید دریغا</p></div>
<div class="m2"><p>که نتوان شکر احسانش ادا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نومیدی مگردان روی از این باب</p></div>
<div class="m2"><p>که هر کاری که کرد آخر دعا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیاز عاشق است و ناز معشوق</p></div>
<div class="m2"><p>که عاجز را قوی شه را گدا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز آن بیگانه مشرب آشنایی</p></div>
<div class="m2"><p>کجا چندین جفا با آشنا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قفس به ز آشیان و... به از باغ</p></div>
<div class="m2"><p>مرا عشق تو این حالت عطا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به قید خویشتن سختم نگهدار</p></div>
<div class="m2"><p>چرا باید چنین صیدی رها کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گلگشت بهارانت گمان داشت</p></div>
<div class="m2"><p>ز بیرون آمدن عبهر حیا کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دی را در شبستان آرمیدی</p></div>
<div class="m2"><p>به چشم مردم از رخ پرده وا کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفای دل محقر کلبه ام را</p></div>
<div class="m2"><p>صفایی روضه ی دار الصفا کرد</p></div></div>