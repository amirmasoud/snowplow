---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>به نام آنکه روان آفرید تن ها را</p></div>
<div class="m2"><p>زبان نهاد به کام از کرم دهن ها را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهان و کام و زبانی به کار برد و در او</p></div>
<div class="m2"><p>به لطف تعبیه فرمود این سخن ها را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صلای عشق به عشاق زد ز پرده ی شور</p></div>
<div class="m2"><p>پر از خروش و فغان ساخت انجمن ها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زچرخ در گذرد تا سرو بلبل و سار</p></div>
<div class="m2"><p>فزود فرهی از سرو وگل چمن ها را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهاد در کف ترکان مست ز ابرو تیغ</p></div>
<div class="m2"><p>که خفته در دل خون بنگر دیدن ها را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگاشت بر رخ خوبان زخط نقوش شگرف</p></div>
<div class="m2"><p>که دل به چاه فتنه ها فتد و فتن ها را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپاه غمزه به چشم سیه سپرد و شکست</p></div>
<div class="m2"><p>به صرف نیم نظر قلب صف شکن ها را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عذار گل بدنان را لباس نسرین دوخت</p></div>
<div class="m2"><p>که لاله گون کند از خون دل کفن ها را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسیر زلف پریشان شوند تا جمعی</p></div>
<div class="m2"><p>قرار داد به موی بتان شکن ها را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قد سیم بران از حریر حسن برید</p></div>
<div class="m2"><p>قبای ناز و قباکرد پیرهن ها را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفایی از صفت تنهای خصم جان نبرد</p></div>
<div class="m2"><p>مگر تو یار شوی این غریب تنها را</p></div></div>