---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>منتی دارم به یاران کز بس افغان کرده ام</p></div>
<div class="m2"><p>خاطر از آزار یارانش پشیمان کرده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سرم یک ره نسودی پای از این سودا چه سود</p></div>
<div class="m2"><p>بهر من کاندر رهت با خاک یکسان کرده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کمان ابروی تیر انداز ما صورت نمود</p></div>
<div class="m2"><p>بالله ار تشویش از شمشیر و پیکان کرده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیف باشد بر زمین رفتن ترا با آنکه من</p></div>
<div class="m2"><p>بهر جولانت فضای سینه میدان کرده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطر دل جمع شد در حلقه ی زلفت ولی</p></div>
<div class="m2"><p>حلقه های جمع از این سودا پریشان کرده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر قدومت جان فشانی نیست مقدور ار نه من</p></div>
<div class="m2"><p>جد و جهدی در خور افزون ز آنچه نتوان کرده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بر این مجنون فرو ناید ز دیگر جای ها</p></div>
<div class="m2"><p>کودکان را سنگ کوی او به دامان کرده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرح غم ننگاشتم خود کی سرایم پیش غیر</p></div>
<div class="m2"><p>منکه سر عشق یار از خامه پنهان کرده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناقه و چهر تو در چشم صفایی رخ گشود</p></div>
<div class="m2"><p>خجلتم باد ار نظر بر سرو و بستان کرده ام</p></div></div>