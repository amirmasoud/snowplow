---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>مرا سودای جانان بر سر افتاد</p></div>
<div class="m2"><p>جنون را باز طرح دیگر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو عشقم بار در کاشانه بگشود</p></div>
<div class="m2"><p>خرد را رخت هستی بر در افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یاد در و لعلت دامنی چند</p></div>
<div class="m2"><p>مرا از جزع مرجان پرور افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فزود از اشک چشمم تابش دل</p></div>
<div class="m2"><p>از این آب آتشم سوزان تر افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شش و پنجی فلک در کار من کرد</p></div>
<div class="m2"><p>به بازی مهره ام در ششدر افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل از کف روبرو بردی ندانم</p></div>
<div class="m2"><p>کی آن جادو چنین افسون گر افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا کز رستخیز انکارها بود</p></div>
<div class="m2"><p>از آن قامت قیامت باور افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر ساقی شراب از چشم خود ریخت</p></div>
<div class="m2"><p>که از دست حریفان ساغر افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جدا از خاک پایت ماندم افسوس</p></div>
<div class="m2"><p>رخم بی فر سرم بی افسر افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بس دین و دل اندر پا فکندی</p></div>
<div class="m2"><p>نشان از دین و نام از دل برافتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرانجامم ولی پیداست ز اول</p></div>
<div class="m2"><p>که با ترکان کافر دل در افتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر از پا باز نشناسد صفایی</p></div>
<div class="m2"><p>به سودای تو از پا و سر افتاد</p></div></div>