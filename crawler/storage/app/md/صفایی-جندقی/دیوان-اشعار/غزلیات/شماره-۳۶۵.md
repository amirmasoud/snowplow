---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>ای زاغ زلف یار از آن رخ در آذری</p></div>
<div class="m2"><p>با وصف بال و پر غرابی سمندری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاوس باغ قدسی و چون من مشوشی</p></div>
<div class="m2"><p>شهباز راغ خلدی و چون من مکدری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رنگ و تاب زاغ و پرستو سرایمت</p></div>
<div class="m2"><p>زاغ و پرستویی که پر از پای تا سری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه بر جبین برآیی و گه در روی به جیب</p></div>
<div class="m2"><p>خوش می پری و لیک ندانم چه طایری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دوش همنشینی و با گوش هم سخن</p></div>
<div class="m2"><p>با سینه هم سرایی و با ساق همسری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گردنش معلق و در دامنش نگون</p></div>
<div class="m2"><p>با ساعدش همال و به سیماش هم بری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعلش یکی ببوس چو دستت همی رسد</p></div>
<div class="m2"><p>لعلش یکی بخای چو نزدیک شکری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرگشته ای تو نیز چون من در غمش چرا</p></div>
<div class="m2"><p>با آنکه روز و شب به کنار وی اندری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان حلقه حلقه کز پی دلها فراهم است</p></div>
<div class="m2"><p>با صد هزار دیده بر آن روی ناظری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من دور از او فتاده که شوریده ام چرا</p></div>
<div class="m2"><p>با قرب او تو اینقدر آشفته خاطری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر خویش می طپی مگرت سر بریده اند</p></div>
<div class="m2"><p>یا عقربت گزیده که پیچان و مضطری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مانی به عود سوخته برطرف عارضش</p></div>
<div class="m2"><p>یا حلقه حلقه دود بر اطراف مجمری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مانا ز تیپ غمزه و توپ نگاه یار</p></div>
<div class="m2"><p>شاه شکست خورده ی برگشته لشکری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا خود به بوی چشمه ی نوشین دهان دوست</p></div>
<div class="m2"><p>در جستجوی آب بقا چون سکندری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخت صفایی ار ز تو باری سیه تراست</p></div>
<div class="m2"><p>لیکن تو در سلوک از آن کج روش تری</p></div></div>