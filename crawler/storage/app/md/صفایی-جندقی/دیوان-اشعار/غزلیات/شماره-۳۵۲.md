---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>گفتم بگویمت که صنوبر به قامتی</p></div>
<div class="m2"><p>دیدم در آن نبود چنان استقامتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژگان ز موج اشک کنم رشک آبشار</p></div>
<div class="m2"><p>تا جا به جوی دیده کند سرو قامتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سحر زلف و چهر تو با آن عصا و دست</p></div>
<div class="m2"><p>بالله نبود معجز موسی کرامتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها نه من سر از تو نپیچم که هیچ کس</p></div>
<div class="m2"><p>از جان خویش بر تو ندارد لآمتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونی که ریخت چشم تو نبود سیاستی</p></div>
<div class="m2"><p>نهبی که کرد ترک تو نبود غرامتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امری که نهی تست نباشد تأسفی</p></div>
<div class="m2"><p>کاری که بهر تست ندارد ندامتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مدعی عیان بود آثار صدق و کذب</p></div>
<div class="m2"><p>دعوی عاشقی نبود بی علامتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردیم اجل نیامده بر سر بلی نبود</p></div>
<div class="m2"><p>کس را به شهر عشق تو چندان اقامتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیرم نخواست بلکه صفایی ز رشک بود</p></div>
<div class="m2"><p>راند ار کسم ز شیوه ی رندی ملامتی</p></div></div>