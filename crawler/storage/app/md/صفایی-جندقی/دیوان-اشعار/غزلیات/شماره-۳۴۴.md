---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>عقلم از آن چه برد بو قصه ی عشق یار به</p></div>
<div class="m2"><p>وز همه نشاط ها ذکر غم نگار به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقدی اگر به کف ترا به سر او فدا نکو</p></div>
<div class="m2"><p>جائی اگر به لب ترا در قدمش نثار به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله ی ما نوای نی خون جگر به جای می</p></div>
<div class="m2"><p>کیست که نوش و نای وی باشد ازین شمار به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشاق صادق ترا در پی کام چشم و لب</p></div>
<div class="m2"><p>اشک زمین گذر سزاه آه فلک گذار به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با غم هجر خویشتن خنده مجویم از دهن</p></div>
<div class="m2"><p>کاین دل داغ دار را دیده اشکبار به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام عقیق لعل خود از لب ما به دورخط</p></div>
<div class="m2"><p>منع مکن که بزم می طرف بنفشه زار به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غمت اشک من ببین آی و به دیده ام نشین</p></div>
<div class="m2"><p>ز آنکه صنوبری چنین بر لب جویبار به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نگرم ز هر طرف لشکر غم کشیده صف</p></div>
<div class="m2"><p>پاس وجود را به دف ساغر می حصار به</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست صفایی آرزو، زاری و خاکساریم</p></div>
<div class="m2"><p>کز همه کارها مرا بیش وکم این دوکار به</p></div></div>