---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>دهد هرچشمی به وجهی طلعت جانانه را</p></div>
<div class="m2"><p>فرق ها زینجا عیان شد کعبه و بتخانه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره های اشک از آن پیوسته بارم متصل</p></div>
<div class="m2"><p>تا زنم زنجیر بر گردن دل دیوانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنج تنهایی اگر تاریک باشد بر سرشک</p></div>
<div class="m2"><p>گو مرو کز آه روشن میکنم کاشانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم به صد زنجیر پیوند از دل ما نگسلد</p></div>
<div class="m2"><p>خوب خوکرده است این دیوانه این ویرانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بیاص رخ بخوان سودای دل کآمد نقط</p></div>
<div class="m2"><p>قطره قطره اشک ما تحریر این افسانه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چهر معشوق چو نبود در مقابل کور به</p></div>
<div class="m2"><p>چشم عاشق گو نبیند مردم هم خانه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای رقیب از من بگو باری به طفلان تا کنند</p></div>
<div class="m2"><p>سنگ کوی دلستان در دامن این دیوانه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع بزم غیرش از غیرت بگو چون بنگرم</p></div>
<div class="m2"><p>من که رشک آرم بر او جان بازی پروانه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر نشاطی را ملالی باشد از پی لاجرم</p></div>
<div class="m2"><p>گریهٔ مستانه خیزد خندهٔ پیمانه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشتهٔ عمرم صفایی این بود و آن قوت روح</p></div>
<div class="m2"><p>کی دهم نسبت به زلف و خال، دام و دانه را</p></div></div>