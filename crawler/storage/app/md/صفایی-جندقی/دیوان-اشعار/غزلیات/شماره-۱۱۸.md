---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>قیامت از چه ترا گفته اند در قد و قامت</p></div>
<div class="m2"><p>که نیست فتنه همی در حسابگاه قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفات حور و صفای قصور باورش افتد</p></div>
<div class="m2"><p>به خانه ی تو کندکافر ار دو روز اقامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ذل و مسکنت ار زندگی گذشت چه نقصان</p></div>
<div class="m2"><p>مرا که دولت عشق است فزود عز و شهادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بوی حور جنان هر که بگذرد ز تو اینجا</p></div>
<div class="m2"><p>عجب نه کارش اگر منتهی شود به ندامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فراق با همه سختی مرا نکشت دریغا</p></div>
<div class="m2"><p>بیا که در قدمت جان دهم به وجه غرامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آن که شد به رهت کشته زنده ابدی شد</p></div>
<div class="m2"><p>ترا برای تفاخر همین بس است کرامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتی که دل به نگاهش سپرد عارف و عامی</p></div>
<div class="m2"><p>به ترک او مگشا لب که نیست جای ملامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جدایی از رخ جانان مرا کدام صبوری</p></div>
<div class="m2"><p>رهایی از غم هجران مرا کدام مقامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر به دعوی هجران مرا چه کار صفایی</p></div>
<div class="m2"><p>از این مخاطره گرجان برون برم به سلامت</p></div></div>