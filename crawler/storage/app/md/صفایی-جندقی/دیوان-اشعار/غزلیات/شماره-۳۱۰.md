---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>وه که نگذاری به جای خویشتن</p></div>
<div class="m2"><p>یک دل از زلف دو تای خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک تیرانداز چشمت عنقریب</p></div>
<div class="m2"><p>زنده نگذارد سوای خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من به غم سازم تو با بیگانگان</p></div>
<div class="m2"><p>هرکسی با آشنای خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما شدیم از جان شیرین سرد و او</p></div>
<div class="m2"><p>سیر نامد از جفای خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود بی حاصل در اما پیش عشق</p></div>
<div class="m2"><p>سرفرازیم از وفای خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غمت هرچند بیمارم ولی</p></div>
<div class="m2"><p>زین مرض یابم شفای خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکشم از دارو فروشان منتی</p></div>
<div class="m2"><p>من که خون سازم غذای خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با نگاه آخرینت گاه نزع</p></div>
<div class="m2"><p>صلح کردم خونبهای خویشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش دلم کاندر قیامت هم به دوست</p></div>
<div class="m2"><p>فارغیم از ماجرای خویشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل بهر گامی که پوید سوی یار</p></div>
<div class="m2"><p>می نهد بندی به پای خویشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شه به جانم شد صفایی خواستار</p></div>
<div class="m2"><p>خواند تا یارم گدای خویشتن</p></div></div>