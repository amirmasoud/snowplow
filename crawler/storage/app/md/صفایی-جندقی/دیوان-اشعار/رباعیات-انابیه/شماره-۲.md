---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>غیر از نیکی ز نیک ناید کاری</p></div>
<div class="m2"><p>کشنید ز بد به جز بدی کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تسلیم رضای تو شدم بی اکراه</p></div>
<div class="m2"><p>در ساختن و سوختنم مختاری</p></div></div>