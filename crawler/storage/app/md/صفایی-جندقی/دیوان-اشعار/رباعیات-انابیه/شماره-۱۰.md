---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>آن روز که سوی توست راه همه کس</p></div>
<div class="m2"><p>بر ماهی و ماه اشک و آه همه کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر سر ما تاج کرامت ننهی</p></div>
<div class="m2"><p>افتد پس معرکه کلاه همه کس</p></div></div>