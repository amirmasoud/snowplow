---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>کس را نبود حال تباهی که مراست</p></div>
<div class="m2"><p>تاریک تر از روز سیاهی که مراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فضل تو مگر شفیعم آید که گذشت</p></div>
<div class="m2"><p>دشوار نماید از گناهی که مراست</p></div></div>