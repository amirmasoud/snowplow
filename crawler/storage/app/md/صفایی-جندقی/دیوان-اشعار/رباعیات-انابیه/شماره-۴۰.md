---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>عصیان تو پیش خلق خوارم کرده است</p></div>
<div class="m2"><p>رسوا و زبون و شرمسارم کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوف و خطر و خجلت و خسران و خطا</p></div>
<div class="m2"><p>الله الله ببین چه کارم کرده است</p></div></div>