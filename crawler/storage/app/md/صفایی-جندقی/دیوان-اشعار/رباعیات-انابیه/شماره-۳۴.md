---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>بس اشک ز دیده متصل رفت مرا</p></div>
<div class="m2"><p>پا در ره جستجو به گل رفت مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول قدم از کمال بی تابی و شوق</p></div>
<div class="m2"><p>تن ماند میان راه و دل رفت مرا</p></div></div>