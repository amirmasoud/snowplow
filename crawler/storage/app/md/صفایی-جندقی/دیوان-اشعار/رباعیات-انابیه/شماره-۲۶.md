---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>یارب مأیوسم از در خویش مکن</p></div>
<div class="m2"><p>مأنوس به نفس دوزخ اندیش مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغرور کرامت تو بودن سهل است</p></div>
<div class="m2"><p>ما را مفتون طاعت خویش مکن</p></div></div>