---
title: >-
    بخش ۲
---
# بخش ۲

<div class="b" id="bn1"><div class="m1"><p>داشت شاه تشنه کامان دختری</p></div>
<div class="m2"><p>دختری خورشید رخ فرخ فری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختری فرخنده کی فردوس فال</p></div>
<div class="m2"><p>کش به دامان پروریدی ماه و سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و آن رقیه نامش ناکامی صغیر</p></div>
<div class="m2"><p>کآمدی از لب هنوزش بوی شیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وجود کودکی آن مستمند</p></div>
<div class="m2"><p>بازویش در بند وگردن در کمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نوای ناله ی بیگاه و گاه</p></div>
<div class="m2"><p>شد درای کاروان در عرض راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر بس کوتاه و اندوهش دراز</p></div>
<div class="m2"><p>ساز بی برگیش خوش با برگ ساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در صبی گیسویش از غم شد سفید</p></div>
<div class="m2"><p>شام عیدش صبح عاشورا دمید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست بردش زد خزان ها بر بهار</p></div>
<div class="m2"><p>سوختی پیش از شکفتن برگ و بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر قدم جای تسلی سیلی اش</p></div>
<div class="m2"><p>از طپانچه رخ چو برقع نیلی اش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از زمین نینوا تا باب شام</p></div>
<div class="m2"><p>باب جستی زان اسیران گام گام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پدر هر لحظه کردی گفتگوی</p></div>
<div class="m2"><p>گفتگویی ناف تا لب جستجوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با خیالش نرد حسرت باختی</p></div>
<div class="m2"><p>بر وصالش خاطری خوش ساختی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر نفس نام از پر بردی به وای</p></div>
<div class="m2"><p>وز هوایش گریه کردی های های</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمه اش گفتی جواب ای دل فروز</p></div>
<div class="m2"><p>کز سفر باز آیدت باب این دو روز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عنقریب از در فراز آید ترا</p></div>
<div class="m2"><p>آب از جو رفته باز آید ترا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چه ز آن بهتر نباشد در جهان</p></div>
<div class="m2"><p>زین سفر بهر تو آرد ارمغان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برگ آرامش فرا پیش آردت</p></div>
<div class="m2"><p>ساز آرایش کما بیش آردت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شیر مرغ و جان آدم از جهات</p></div>
<div class="m2"><p>بی تعب آماده فرماید برات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت ای یاران چرا ناید به سر</p></div>
<div class="m2"><p>این سفر را چیست تأخیر این قدر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خود مسافر را مگر برگشت نیست</p></div>
<div class="m2"><p>علت تعویق چندین بهر چیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>می نخواهم در دو گیتی جز پدر</p></div>
<div class="m2"><p>نیست از دورم تمنایی دگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رشته ی مهرش مرا باید بنای</p></div>
<div class="m2"><p>عقده ای نگشاید از گوی طلای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقد گوهر گو نیارد کس مرا</p></div>
<div class="m2"><p>طوق اتباعش به گردن بس مرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هست در گوشم چو حلقه انقیاد</p></div>
<div class="m2"><p>حاش لله گوشوارم گو مباد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باید این زنجیر بازو بند من</p></div>
<div class="m2"><p>زیب گردن مر مرا زیبد رسن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بند بر پا خوشترین خلخال ماست</p></div>
<div class="m2"><p>قید تقوی قاید آمال ماست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیته ام بر جبهه داغ بندگی است</p></div>
<div class="m2"><p>این مرا پیرایه فرخنده گی است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پای را حاجت بدین خلخال نیست</p></div>
<div class="m2"><p>حلقه ی زر زیور اقبال نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لخت دل نانم سرشک دیده آب</p></div>
<div class="m2"><p>آب و نانم چیست گو باز آی باب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر قدم می رفت و اختر می فشاند</p></div>
<div class="m2"><p>تا رکاب از بار و خیل از کار ماند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون به شهر شام بار افتادشان</p></div>
<div class="m2"><p>خصم در ویرانه منزل دادشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در خرابه ی شام آن خونین جگر</p></div>
<div class="m2"><p>سوخت آن شب شمع آسا تا سحر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آه آتشبارش از گردون گذشت</p></div>
<div class="m2"><p>و اشک طوفان زایش از دریا و دشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خستگی ها از روانش تاب برد</p></div>
<div class="m2"><p>چشم را در عین زاری خواب برد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>باب ماجد جلوه گر در خواب دید</p></div>
<div class="m2"><p>دید نقش خود ولی بر آب دید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>با دلی خونین لبی خندان و شاد</p></div>
<div class="m2"><p>با روانی بسته با رویی گشاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در طرب زان که دست از روزگار</p></div>
<div class="m2"><p>در کرب زان در که اهلش خوار و زار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ازعنایات خدایی با سرور</p></div>
<div class="m2"><p>وز مقاسات جدایی بی حضور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شادیش بر فضل های لایزال</p></div>
<div class="m2"><p>اندهش بر حسرت فرزند وآل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیمه ی دل ز اهل بیتش در تعب</p></div>
<div class="m2"><p>نیم دیگر ز امتانش در طرب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رخ به تعظیم پدر برخاک سود</p></div>
<div class="m2"><p>وز تشرف موزه بر افلاک سود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سود چون بر خاک اقدامش جبین</p></div>
<div class="m2"><p>بوسه ی چندین زد برآستین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برق سان بگرفت طرف دامنش</p></div>
<div class="m2"><p>وز فغان زد آتش اندر خرمنش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفت از روی تشکی با ادب</p></div>
<div class="m2"><p>ای عجب ثم العجب ثم العجب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این تویی باب وفا آیین من</p></div>
<div class="m2"><p>کآمدستی بر سر بالین من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تن به جانم از جدایی های تو</p></div>
<div class="m2"><p>حیرتم بر بی وفایی های تو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>باورم ناید ز بخت خویشتن</p></div>
<div class="m2"><p>کاین من استم با تو در یک انجمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این تغافل های پی در پی چه بود</p></div>
<div class="m2"><p>کز شما درباره ی ما رخ نمود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از شما نامهربانی ناد راست</p></div>
<div class="m2"><p>ترک احسان از توام کی باور است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای پدر چون شدکه در این ماجرا</p></div>
<div class="m2"><p>هجرت اندر کربلا جستی ز ما</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر رقیه لایق الطاف نیست</p></div>
<div class="m2"><p>جرم دیگر خواهرانم بر تو چیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از زن و فرزند مهجوری چرا</p></div>
<div class="m2"><p>بی گناه از بی کسان دوری چرا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زان سوی پل در جهاندی بارگی</p></div>
<div class="m2"><p>چشم پوشیدی ز ما یکبارگی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ای پدر یک دم به عرضم گوش دار</p></div>
<div class="m2"><p>تا چه پیش آورد ما را روزگار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ظهر عاشورا که اندر کربلا</p></div>
<div class="m2"><p>طلعتت مخفی شد از انظار ما</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شامی و کوفی چو طوفان سپه</p></div>
<div class="m2"><p>جمله آوردند سوی خیمگه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دوزخی از خشم و کین افروختند</p></div>
<div class="m2"><p>چون دل ما خیمه ها را سوختند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بس سراری تا جواری سر به سر</p></div>
<div class="m2"><p>شد اسیر آن گروه دد سیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بعد سلب سلوت و تاراج مال</p></div>
<div class="m2"><p>جمله را بستند بر بازو حبال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خسته جان خونین جگر خاطر نژند</p></div>
<div class="m2"><p>پور در زنجیر و دختر در کمند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>آنکه نتوانستی اش در پای خار</p></div>
<div class="m2"><p>دید اینک بین به زنجیرش فگار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سایه آن را کش ز خورشید احتجاب</p></div>
<div class="m2"><p>سر برهنه بنگرش در آفتاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آنکه را دست تو عقد نای بود</p></div>
<div class="m2"><p>حلق بین فرسوه از قید حسود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آنکه پروردی چو دل در دامنش</p></div>
<div class="m2"><p>پیرهن بیگانه بربود از تنش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خواب بد دیدی که امشب بی خبر</p></div>
<div class="m2"><p>بر یتیمان بلا کردی گذر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>صحبت جد و پدر بگذاشتی</p></div>
<div class="m2"><p>بر یتیمانت نظر بگماشتی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ترک مام و جده گفتی در جهان</p></div>
<div class="m2"><p>روی آوردی بدین آوارگان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دامنت غلمان چسان از دست داد</p></div>
<div class="m2"><p>خازنت بر هجر چون گردن نهاد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خود ملاقات بزرگان در بهشت</p></div>
<div class="m2"><p>در پی ما چون دلت ازدست هشت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>با عموم نعمت دار الصفا</p></div>
<div class="m2"><p>ای عجب یادآوری کردی ز ما</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زلف حورا هرکرا باشد کمند</p></div>
<div class="m2"><p>نیست نسبت با غل و زنجیر و بند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بر بهر وجهم فدایت جان و تن</p></div>
<div class="m2"><p>خاک پایت توتیای چشم من</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چون میان دشمنانم بی پناه</p></div>
<div class="m2"><p>رفتی و بگذاشتی این چندگاه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گاه و بی گه چون درین ربع و دمن</p></div>
<div class="m2"><p>روز یا شب بین این سهل و حزن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کردمی زاری به حال زار خویش</p></div>
<div class="m2"><p>بودمی آسیمه سر درکار خویش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>جای دل جویی به سیلی های سخت</p></div>
<div class="m2"><p>خستیم هر لحظه خصم تیره بخت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ضجر هر ساعت به زجری دیگرم</p></div>
<div class="m2"><p>زخم کردی دل شکستی خاطرم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>پایم از رفتار چون سنگین شدی</p></div>
<div class="m2"><p>شمر دون تا زانه ام بر سر زدی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>شامگاهان تا سحر در ولوله</p></div>
<div class="m2"><p>بود هر روزم درای قافله</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>خسته از رفتن چو می آمد تنم</p></div>
<div class="m2"><p>کعب نی برکتف می زد دشمنم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>جای چتر دیبه ام در آفتاب</p></div>
<div class="m2"><p>تامگر کمتر بسوزم ز التهاب</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هر دم از دست عنودی شوم پی</p></div>
<div class="m2"><p>سایه گستردی به فرقم تیغ و نی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>گر بپرسی صبح و شامم ز آب و نان</p></div>
<div class="m2"><p>لخت دل نان بود وآب اشک روان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>جز دلم کس فکر غم خواری نکرد</p></div>
<div class="m2"><p>غیر قیدم کس نگهداری نکرد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ناله همدم هم نشین زنجیر و بند</p></div>
<div class="m2"><p>آفتابم سایه بر سر می فکند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>هر کجا این کاروان محمل گشود</p></div>
<div class="m2"><p>منزل و مأوای ما ویرانه بود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>خود نبودی تا ببینی این سفر</p></div>
<div class="m2"><p>حال ما ز آنسان که گفتم صد بتر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بی گزاف از فرط سختی دیر و زود</p></div>
<div class="m2"><p>وز فراقم هر نفس صد قرن بود</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بر شما چون هست حالاتم عیان</p></div>
<div class="m2"><p>بستن اولی از مقالاتم زبان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ای پدر آن دم که زی میدان شدی</p></div>
<div class="m2"><p>بر نگشتی وز نظر پنهان شدی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>عمه ام گفتی مسافر شد حسین</p></div>
<div class="m2"><p>خود سفر را نیست در خور شور و شین</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هر دمم ز آن پس که در حرمان گذشت</p></div>
<div class="m2"><p>دل ز تن بگسست و تن از جان گذشت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هر چه از احوال شما پرسیدمی</p></div>
<div class="m2"><p>جز نوید رجعتت نشنیدمی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>می شنیدم گاهی از گوشه کنار</p></div>
<div class="m2"><p>که حسینی کشته شد در کارزار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>باورم می نامد اما یک به یک</p></div>
<div class="m2"><p>زین خبر اعضا سراپا داشت شک</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>شکر لله کآن سخن ها شد دروغ</p></div>
<div class="m2"><p>حرف دشمن بود دودی بی فروغ</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>و اینک از فر جمال روشنم</p></div>
<div class="m2"><p>مهروش سر بر زدی از روزنم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ناامیدی عاقبت امید زاد</p></div>
<div class="m2"><p>شاخ حسرت خاطر دل خواه داد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>کامشب از اقبال بخت مقبلم</p></div>
<div class="m2"><p>گشت رخسارت سراج محفلم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>خود ندانم دیگر آن کودک چه گفت</p></div>
<div class="m2"><p>یا جواب از باب خود هر چه شنفت</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>دل تهی ناکرده ازتیمار و درد</p></div>
<div class="m2"><p>بخت خواب آلوده اش بیدارکرد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>برجهید از جای و هر سو بنگرید</p></div>
<div class="m2"><p>یک مثالی از پدر با خویش دید</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>گفت واویلا دگر بابم چه شد</p></div>
<div class="m2"><p>آنکه رخ بنمود در خوابم چه شد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ای دریغا ای دریغا ای دریغ</p></div>
<div class="m2"><p>کآفتابم رفت دیگر زیر میغ</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>محو و مات از هر طرف کردی نگاه</p></div>
<div class="m2"><p>هی زدی بر فرق گفتی وا اباه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>آتشی بر رستش از دل شعله بار</p></div>
<div class="m2"><p>سوخت مغز و پوستش اسپندوار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>هی گرستی زار و هی بستی نظر</p></div>
<div class="m2"><p>هی کشیدی آه و هی گفتی پدر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>گوش افلاک از خروشش کر فتاد</p></div>
<div class="m2"><p>تخته ی خاک از سرشکش تر فتاد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>آتشی از تابش دل برفروخت</p></div>
<div class="m2"><p>کآن غریبان را به داغی تازه سوخت</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>دید چون این حالت از آن طفل خرد</p></div>
<div class="m2"><p>زینب از خواب وی اندک بوی برد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ناصبوری طاقت از دستش ربود</p></div>
<div class="m2"><p>مویه گر رخ کند و گیسو برگشود</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>زار و نالان اهل بیت از هر کنار</p></div>
<div class="m2"><p>شعله سان پیرامنش اخگر شمار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>شام را صبح نشور آن نیم شب</p></div>
<div class="m2"><p>اجتماع صبح و شام آمد عجب</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>شیون از در شورش از دیوار خاست</p></div>
<div class="m2"><p>شام گفتی صبح محشر کرده راست</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>شام را صبح قیامت شد قیام</p></div>
<div class="m2"><p>با هم آمد ای شگفت این صبح و شام</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>گبر کافر کین یزید کفر کیش</p></div>
<div class="m2"><p>فارغ از ذکر خدا غافل ز خویش</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>در خمار خمر آن دوزخ درون</p></div>
<div class="m2"><p>از کسالت تا دمی آید برون</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سر به دامان ندیم اندر نهاد</p></div>
<div class="m2"><p>خفت پاسی تن به خواب مرگ داد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>خواب سنگین است آری مرده را</p></div>
<div class="m2"><p>خاصه آن مردود شیطان برده را</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ماند سر بر زانوی طاهر بلی</p></div>
<div class="m2"><p>دامن طاهر بلند آمد ولی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>و آن سر آموده از خون خاکسود</p></div>
<div class="m2"><p>در کنار تخت آن دل سخت بود</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>از کرامت های شاه کربلا</p></div>
<div class="m2"><p>شد بلند از طشت زرین در هوا</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>ایستادش روبه رو بالای سر</p></div>
<div class="m2"><p>گفت ای بد عهد از حق بی خبر</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>از چه فرموش آمدت ای با نهی</p></div>
<div class="m2"><p>نکته ی اولادنا اکبادنا</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>من چه کردم با تو باری ای لئیم</p></div>
<div class="m2"><p>که نمودی طفلکانم را یتیم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چیست خود جرم من ای مشترک نهاد</p></div>
<div class="m2"><p>کز جفا خاک مرا دادی به باد</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>در عمل بندیش هان غافل مپای</p></div>
<div class="m2"><p>اندکی بیدار شو باهوش آی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>آنچه کردی بیش و کم ازخبث طیب</p></div>
<div class="m2"><p>جمع گردد بر تو بی شک عنقریب</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>هر چه کاری بدروی روزجزا</p></div>
<div class="m2"><p>تخم را آری برویاند خدا</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>تلخ خواهی کام خود حنظل بکار</p></div>
<div class="m2"><p>جویی از شیرین بیا خرما بیار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>طاهر آن غوغای شهر آشوب را</p></div>
<div class="m2"><p>کز شکیب آرد برون ایوب را</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>وز تکلم کردن آن کام و لب</p></div>
<div class="m2"><p>هوشش از سر کاست و افزودش عجب</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>لختی اندیشید و با خود شد فرو</p></div>
<div class="m2"><p>رخت اشکش ز التهاب دل برو</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>قطره ی چندش چو از مژگان دمید</p></div>
<div class="m2"><p>قطره ای بر چهر آن ملحد چکید</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>سر برآوردش ز دامان شعله سار</p></div>
<div class="m2"><p>سخت جان پیچید برخود ماروار</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گفت این غوغا و شورش بهر چیست</p></div>
<div class="m2"><p>داعی این داستان در شهر کیست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>گفت طاهر این سؤال از ما چرا</p></div>
<div class="m2"><p>با تو باید گفتگو زین ماجرا</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>خود تو این بیداد بر پا کرده ای</p></div>
<div class="m2"><p>هر کرا با تست رسوا کرده ای</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>نیک بنگر کآتشی افروختی</p></div>
<div class="m2"><p>خرمن اسلامیان را سوختی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ظلم خود کی بوده بر کافر روا</p></div>
<div class="m2"><p>وانگهی بر آل پیغمبر روا</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>این گناهی کز تو سر زد در جهان</p></div>
<div class="m2"><p>کس نخواهد دید دیگر از انس و جان</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>نز فرنگی نز مجوسی نز هنود</p></div>
<div class="m2"><p>نز نواصب نز نصاری نز یهود</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>کس بهم کیش خود این استم نکرد</p></div>
<div class="m2"><p>این جفاها کس به کافر هم نکرد</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>دعوی اسلام و با حق کبر و کین</p></div>
<div class="m2"><p>کفر را ننگ است خود ز اینگونه دین</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>آری آنان دل ز رحمت کنده اند</p></div>
<div class="m2"><p>کاین ستم ها در جهان افکنده اند</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>باز آگه نامد آن خوک عنود</p></div>
<div class="m2"><p>رست و خواهد بود بر حالی که بود</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بندگی در مغز آن کافر رود</p></div>
<div class="m2"><p>میخ آهن چون به سنگ اندر شود</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>لحظه ای باخود براندیشید و خواند</p></div>
<div class="m2"><p>کس پی تحقیق زی ویرانه راند</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>رفت و باز آمد که طفلی از حسین</p></div>
<div class="m2"><p>دارد امشب بهر باب این شور و شین</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>کوه و صحرا از دمش بریان همه</p></div>
<div class="m2"><p>دشت و دریا بر دلش گریان همه</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>مادران از بچگان گشته نفور</p></div>
<div class="m2"><p>شوی و زن زنده گراید سوی گور</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>این جفا را حق اگر کیفر کند</p></div>
<div class="m2"><p>هر دمت صدبار خاکستر کند</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>دست تا نفتاده ناچارت ز کار</p></div>
<div class="m2"><p>تا ز دستت کاری آید زینهار</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>سنگ ها بر سینه می زن زین غرور</p></div>
<div class="m2"><p>پیش از آن که سنگ چینندت به گور</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بر سر افشان خاک ها ز آن شور و شر</p></div>
<div class="m2"><p>پیش از آنکه خاک ریزندت به سر</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>ما مضی را کن تلافی پاره ای</p></div>
<div class="m2"><p>بهر این طفلک بفرما چاره ای</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>هان بیندیش از مکافات ای یزید</p></div>
<div class="m2"><p>شام ماتم زایدت زین صبح عید</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>این نصایح چون به گوشش باد بود</p></div>
<div class="m2"><p>از کجا بئس المصیرش یاد بود</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>گفت این سر مجلس آرایی نکوست</p></div>
<div class="m2"><p>رنج او را چاره فرمایی از اوست</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>طفل نارد مرده از زنده شناخت</p></div>
<div class="m2"><p>شایدش زین راه دردی چاره خاست</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>برد باید تا فرا پیشش نهند</p></div>
<div class="m2"><p>طفل را زینسان تسلی ها دهند</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>برد خادم سر بدان ویران سرای</p></div>
<div class="m2"><p>گنج را آری به ویرانه است جای</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>روی پوش از طشت زر برداشتند</p></div>
<div class="m2"><p>پیش رویش بر زمین بگذاشتند</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>نیم شب از مشرق آن طشت زر</p></div>
<div class="m2"><p>همچو شعرا بر غریبان تاخت سر</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>بی کسان پروانه سان و آن سر چو شمع</p></div>
<div class="m2"><p>با پریشانی به دورش گشته جمع</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>ظلمت شبشان از آن سر نور شد</p></div>
<div class="m2"><p>ز آنسر آن ماتم سرا معمور شد</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>صفحه تقویمشان آن خط و روی</p></div>
<div class="m2"><p>بخت خود خواندند در وی مو به موی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>نخل امید رقیه جای بر</p></div>
<div class="m2"><p>بس که آبش داد بار آور سر</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چون سری خون سود و خاک آلود دید</p></div>
<div class="m2"><p>جامه ی جان جای پیراهن درید</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>چشم افکندش به چشم و روبه رو</p></div>
<div class="m2"><p>دوخت لختی دیده ی حسرت بدو</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>آتشی دیگر به جانش در گرفت</p></div>
<div class="m2"><p>واحسینا را فغان از سر گرفت</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>گشت دریا ز آتش آهش سراب</p></div>
<div class="m2"><p>گشت صحرا ز انجمش دریای آب</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>وحش از مرتع به هامون در خزید</p></div>
<div class="m2"><p>طیر در بستان سر اندر پر کشید</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>رخ به سر بنهاد و گفت ای وای باب</p></div>
<div class="m2"><p>وه که کرد از خون سر ریشت خضاب</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>یا رب آن کافر درون کی بودکی</p></div>
<div class="m2"><p>کاو یتیمم ساخت در این کودکی</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>ای پدر آن کت رگ گردن گشاد</p></div>
<div class="m2"><p>کیست دستانش الهی قطع باد</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>هان مرا وقت یتیمی زود برد</p></div>
<div class="m2"><p>سنگدل بود آنکه این جرأت نمود</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>هر که ما را ساخت زینسان دل دو نیم</p></div>
<div class="m2"><p>یا رب اولادش چو ما گردد یتیم</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>کاش نابینا ز مادر زادمی</p></div>
<div class="m2"><p>بر سرت زینسان نظر نگشادمی</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>در جهان پشت و پناهم بعد از این</p></div>
<div class="m2"><p>کیست ای پشت و پناه عالمین</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>کشته تو من زنده و شرمندگی</p></div>
<div class="m2"><p>خاک بر فرق من واین زندگی</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>قتل من والله ثوابستی به تیغ</p></div>
<div class="m2"><p>تیغ کو قاتل کجا جویم دریغ</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>حق مگر درد مرا درمان کند</p></div>
<div class="m2"><p>فضل وی دشوار من آسان کند</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>چاره فرمایی به از مرگم کجاست</p></div>
<div class="m2"><p>ای دریغ آ نهم برون از دست ماست</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>تا دم مردن ادب از کف نداد</p></div>
<div class="m2"><p>سر به خاک پای آن سر در نهاد</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>شمع وش برتار و پودش دل فروخت</p></div>
<div class="m2"><p>پای تا سر بی نفیر و ناله سوخت</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>سر بدین سرماند و او آن سر فتاد</p></div>
<div class="m2"><p>جان شیرین بر سر آن سر نهاد</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>گفت پیغمبر که چون کوبی دری</p></div>
<div class="m2"><p>عاقبت زان در برون آید سری</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>لیک در کوبنده ای از هیچ در</p></div>
<div class="m2"><p>سر نیاوردش به در اینگونه سر</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>هر تنی دارد ز سر پایندگی</p></div>
<div class="m2"><p>هر دم از سر گیرد از سر زندگی</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>وین یتیمک را عجب زین ماجرا</p></div>
<div class="m2"><p>عمر بر سرآمد از این سر چرا</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>زینب از این مرگ نو گیسوی کند</p></div>
<div class="m2"><p>ام کلثوم از تحسر روی کند</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>ماتم آرا مویه گر دیگر زنان</p></div>
<div class="m2"><p>نوحه افزا کودکان بر سر زنان</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>آه دردا حسرتا کاین طفل ما</p></div>
<div class="m2"><p>زندگی رفتش به سر زین سر هلا</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>پیر و برنا زنده از سر روز و شب</p></div>
<div class="m2"><p>وین صبی را مرگ از سر ای عجب</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>اهل بیت از داغ وی مبهوت و مات</p></div>
<div class="m2"><p>مرگ خود را خواستاران از جهات</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>بر فنای خویشتن راغب همه</p></div>
<div class="m2"><p>سوختن را بر به جان طالب همه</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>ز آن میان آمد سکینه خواهرش</p></div>
<div class="m2"><p>زار نالید و نشست اندر برش</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>که خوشا حال تو ای فرخ لقا</p></div>
<div class="m2"><p>کآمدی از زحمت عالم رها</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>تو به گور آسوده خواهی خفت و من</p></div>
<div class="m2"><p>روز و شب از جور اعدا در محن</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>تا به حشرم جای دارد کز غمت</p></div>
<div class="m2"><p>زندگی پوشد سیه در ماتمت</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>کاش خواهر پیش مرگت گشتمی</p></div>
<div class="m2"><p>از جهان پیش از تو در بگذشتمی</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>راست با این زندگی کن باورم</p></div>
<div class="m2"><p>رشک گر بر مرگت ای خواهر برم</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>ممکنات از مرگ وی بر سر زدند</p></div>
<div class="m2"><p>بیرق ماتم به گردون بر زدند</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>مصطفی محو از ملال مرتضی</p></div>
<div class="m2"><p>مرتضی مات از خیال مجتبی</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>مجتبی اندوه ناک از فاطمه</p></div>
<div class="m2"><p>نوح آدم عذر خواهان از همه</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>انبیا انگشت از حیرت به لب</p></div>
<div class="m2"><p>که عجب یک طفل و صد گیتی تعب</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>زین خبر مریم ز سر معجز کشید</p></div>
<div class="m2"><p>آسیه پیراهن اندر تن درید</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>ماه شاماخ ملمع چاک زد</p></div>
<div class="m2"><p>مهر خود در نشان بر خاک زد</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>تیر طومار حساب چون و چند</p></div>
<div class="m2"><p>پاک در پیچید و برطاق اوفکند</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>لخشه ها مریخ را رخ برگشاد</p></div>
<div class="m2"><p>رخش خود پی کرد و تیغ از دست داد</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>مشتری زد بر زمین دستار خویش</p></div>
<div class="m2"><p>ماند حیران زین قضا در کار خویش</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>زهره آهنگ حسینی برنواخت</p></div>
<div class="m2"><p>سینه را بربط فغان را نغمه ساخت</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>بریکایک جمله ذرات وجود</p></div>
<div class="m2"><p>زین تعب کیوان نحوست ها فزود</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>در جنان زهرا عقیق از جزع ریخت</p></div>
<div class="m2"><p>رشته ی یاقوت در دامان گسیخت</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>عاشقان از یاد معشوقان ملول</p></div>
<div class="m2"><p>در عزا آرایی آل رسول</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>ورقه و گلشاد سیر از جان و تن</p></div>
<div class="m2"><p>ویسه و رامین نفور از خویشتن</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>شام گشت از کار دل بازی خجل</p></div>
<div class="m2"><p>شد برون مهر پری دختش ز دل</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>جان مهر از عشق ماهش سرد شد</p></div>
<div class="m2"><p>چهر ماه از این رزیت زرد شد</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>بیژن از مهر منیژه شرمسار</p></div>
<div class="m2"><p>عیش شیرین هر دو را شد زهرمار</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>نام عفرا عروه را از یاد رفت</p></div>
<div class="m2"><p>آب و خاک و آتشش برباد رفت</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>مهر رامین از رخ شهرویه کاست</p></div>
<div class="m2"><p>رایت این تعزیت کردند راست</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>ناله ی نل را زین عزا گردون سپار</p></div>
<div class="m2"><p>ز اشک دامان دمن چون جویبار</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>این غم از جمشید سوز عشق برد</p></div>
<div class="m2"><p>ذکر خورشیدی به صدر سینه برد</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>عشق بازی بر همه بس تلخ گشت</p></div>
<div class="m2"><p>غره عیش همایون تلخ گشت</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>سر به دریا برد زین داغ اندروس</p></div>
<div class="m2"><p>گشت هارورا رخ از غم سندروس</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>تاب بر گلچهره و اورنگ خورد</p></div>
<div class="m2"><p>شیشه ی تسکینشان بر سنگ خورد</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>لیلی از رخسار مجنون شرمسار</p></div>
<div class="m2"><p>قیس بر داغ جوانان بی قرار</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>زین جفا فرهاد و شیرین دل پریش</p></div>
<div class="m2"><p>تلخ کام خسرو از سودای خویش</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>وامق و عذار سر اندر جیب غم</p></div>
<div class="m2"><p>ناز آن ناله، نیاز این ندم</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>بر غریبی چند جوقی سوگوار</p></div>
<div class="m2"><p>بر یتیمی چند فوجی داغدار</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>بر اسیری چند و جمعی دل پریش</p></div>
<div class="m2"><p>بر مریضی چند و خیلی سینه ریش</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>بر صغیری چند و مشتی دردمند</p></div>
<div class="m2"><p>خود چه گویم تا چه کرد این کوب و کند</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>مرگ آن کودک بلند آوازه شد</p></div>
<div class="m2"><p>مرد و زن را داستانی تازه شد</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>با همه بی اعتباری هایشان</p></div>
<div class="m2"><p>خلق را دل سوخت از سودایشان</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>تا به گوش نحس آن بدبخت خورد</p></div>
<div class="m2"><p>برق سوزانی به کوهی سخت خورد</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>نی دلش یک مو به رحمت نرم گشت</p></div>
<div class="m2"><p>نز سر مظلوم آزاری گذشت</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>گفت تا وی را به مغسل آورند</p></div>
<div class="m2"><p>مرغ چون مرد از قفس بیرون برند</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>برد غسالش چو صرصر کز چمن</p></div>
<div class="m2"><p>فصل دی بیرون برد برگ سمن</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>کرد چون پیراهنش از تن برون</p></div>
<div class="m2"><p>پای تا سر دیدش از خون لاله گون</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>پشت و پهلو کتف و بازو دست و پای</p></div>
<div class="m2"><p>یافتش آموده از خون هر کجای</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>آن بدن عضوی سیه عضوی کبود</p></div>
<div class="m2"><p>ساق تا سر یا ورم یا زخم بود</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>گشت واویلا بمیرد مادرت</p></div>
<div class="m2"><p>چیست اینها کآمدستی بر سرت</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>اشک ریزان با زبانی پر گله</p></div>
<div class="m2"><p>زار جویان با دلی پر ولوله</p></div></div>
<div class="b" id="bn245"><div class="m1"><p>روی از مغسل به آن ویرانه کرد</p></div>
<div class="m2"><p>جای در ویرانه چون دیوانه کرد</p></div></div>
<div class="b" id="bn246"><div class="m1"><p>گفت وای ای خواهران ممتحن</p></div>
<div class="m2"><p>از عناد خصم ممنوع از وطن</p></div></div>
<div class="b" id="bn247"><div class="m1"><p>موجبات مرگ این طفلک چه شد</p></div>
<div class="m2"><p>علت بیماریش ز اول چه بد</p></div></div>
<div class="b" id="bn248"><div class="m1"><p>کش بدن گر ناله زنبور نیست</p></div>
<div class="m2"><p>لاغر اندامی و چندین زخم چیست</p></div></div>
<div class="b" id="bn249"><div class="m1"><p>پای تا سر پیکری مجروح و ریش</p></div>
<div class="m2"><p>زخمش از ذرات صد خورشید بیش</p></div></div>
<div class="b" id="bn250"><div class="m1"><p>این هزال و نوبت و ضعف از چه خاست</p></div>
<div class="m2"><p>این جراحت ها بر اندامش چراست</p></div></div>
<div class="b" id="bn251"><div class="m1"><p>خود مگر این بچه را مادر نبود</p></div>
<div class="m2"><p>یا پرستارش پدر برسر نبود</p></div></div>
<div class="b" id="bn252"><div class="m1"><p>تا گشودم دیده بر وی ز التهاب</p></div>
<div class="m2"><p>دل کبابم دل کبابم دل کباب</p></div></div>
<div class="b" id="bn253"><div class="m1"><p>کاش خود بی بهره بودم از بصر</p></div>
<div class="m2"><p>تا بر این طفلم نیفتادی نظر</p></div></div>
<div class="b" id="bn254"><div class="m1"><p>زینب آن سرگشته ی بی خانمان</p></div>
<div class="m2"><p>ترجمان حالت آن بی کسان</p></div></div>
<div class="b" id="bn255"><div class="m1"><p>از جروح یثرب و آسیب راه</p></div>
<div class="m2"><p>تا ورود کربلا در خیمه گاه</p></div></div>
<div class="b" id="bn256"><div class="m1"><p>از قدوم دشمنان رحم سوز</p></div>
<div class="m2"><p>وز هجوم آن سپاه کینه توز</p></div></div>
<div class="b" id="bn257"><div class="m1"><p>ز ازدحام شامیان بی حیا</p></div>
<div class="m2"><p>ز اجتماع کوفیان بی وفا</p></div></div>
<div class="b" id="bn258"><div class="m1"><p>منع آب و قطع امید از جهات</p></div>
<div class="m2"><p>تشنه لب بر شاطی شط فرات</p></div></div>
<div class="b" id="bn259"><div class="m1"><p>قتل مردان بلاکش بیش و کم</p></div>
<div class="m2"><p>خفته در میدان کین شه تا حشم</p></div></div>
<div class="b" id="bn260"><div class="m1"><p>اسر نسوان سلب سامان نهب مال</p></div>
<div class="m2"><p>دستگیر اینان و آنان پایمال</p></div></div>
<div class="b" id="bn261"><div class="m1"><p>زیر پی تن های بی سر چاک چاک</p></div>
<div class="m2"><p>زیب نی سرهای خون آلود پاک</p></div></div>
<div class="b" id="bn262"><div class="m1"><p>ز اتفاق ناصبین پر نفاق</p></div>
<div class="m2"><p>ز افتراق ناصرین کم دقاق</p></div></div>
<div class="b" id="bn263"><div class="m1"><p>با وی از هر ماجرایی طرح کرد</p></div>
<div class="m2"><p>شطری از احوال خود را شرح کرد</p></div></div>
<div class="b" id="bn264"><div class="m1"><p>باز آن ویرانه شد ماتم سرای</p></div>
<div class="m2"><p>شور غوغایی ز نو آمد به پای</p></div></div>
<div class="b" id="bn265"><div class="m1"><p>صابری رخت از جهان بیرون کشید</p></div>
<div class="m2"><p>ایمنی پیراهن اندر خون کشید</p></div></div>
<div class="b" id="bn266"><div class="m1"><p>از نوا شد نینوا آن غم سرا</p></div>
<div class="m2"><p>آری آری کل ارض کربلا</p></div></div>
<div class="b" id="bn267"><div class="m1"><p>تا صفایی زین مصیبت دم زدی</p></div>
<div class="m2"><p>آتش اندر دوده ی آدم زدی</p></div></div>
<div class="b" id="bn268"><div class="m1"><p>بردی ازتن مرد و زن را صبر و تاب</p></div>
<div class="m2"><p>کردی از غم انس و جان را دل کباب</p></div></div>
<div class="b" id="bn269"><div class="m1"><p>به که بر سوزی بنان و خامه را</p></div>
<div class="m2"><p>به که در شویی کتاب و نامه را</p></div></div>
<div class="b" id="bn270"><div class="m1"><p>بار الها محض فضل خویشتن</p></div>
<div class="m2"><p>گوشه ی چشمی فراز آور به من</p></div></div>
<div class="b" id="bn271"><div class="m1"><p>امر این سرگشته حال شرمسار</p></div>
<div class="m2"><p>با رقیه ی شاه مظلومان گذار</p></div></div>
<div class="b" id="bn272"><div class="m1"><p>تا به رستاخیز از این رو سیه</p></div>
<div class="m2"><p>آورد پیش پدر عذر گنه</p></div></div>
<div class="b" id="bn273"><div class="m1"><p>بوکه زین غرقابه ام بیرون کشند</p></div>
<div class="m2"><p>پیش از آنکه زورقم در خون کشند</p></div></div>