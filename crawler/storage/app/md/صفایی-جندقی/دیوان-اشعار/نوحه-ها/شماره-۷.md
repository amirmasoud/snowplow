---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>ای تازه جوان جان جهانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان جهان تازه جوانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذاشتیم درکف اشرار و گذشتی</p></div>
<div class="m2"><p>ای مرغ بهشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردا که خطا بود گمانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز آی و مرا از قفس غم بگسل دام</p></div>
<div class="m2"><p>زان بیش که ناکام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرواز کند مرغ روانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون بدنت جای کفن در تن صد چاک</p></div>
<div class="m2"><p>ای بر سر من خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد از تو اگر زنده بمانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با آنکه روان کرده ام ای کشته ی عطشان</p></div>
<div class="m2"><p>یک دجله به دامان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب تر نشدت ز آب روانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای سرو روان خیز و به چشمم قدمی تاز</p></div>
<div class="m2"><p>کت بار دگر باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دامن این چشمه نشانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا رب چه شود مهر دل افروز تو روشن</p></div>
<div class="m2"><p>سازد نظر من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنما رخ و از غم برهانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تدبیر علاج دل غم پرور ما کن</p></div>
<div class="m2"><p>این درد دوا کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا باز به لب نامده جانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باکم ز گنه چیست بدین نوحه سرایی</p></div>
<div class="m2"><p>تا همچو صفایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ماتم تو مرثیه رانم علی اکبر</p></div>
<div class="m2"><p>جانم علی اکبر</p></div></div>