---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>جنبشت ای آسمان گردد سکون</p></div>
<div class="m2"><p>با چنین دوران شوی یارب نگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین پس ار گردی چو ما گردی زبون</p></div>
<div class="m2"><p>زورق خضرات گردد بحر خون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان ای آسمان تا کی ستمکاری</p></div>
<div class="m2"><p>فغان ای آسمان امان ای آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تو یک مونی حمیت نی حیا</p></div>
<div class="m2"><p>نز نبی باکت نه خوفی از خدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرگه سلطان دین را ز ابتدا</p></div>
<div class="m2"><p>کندی از یثرب زدی در کربلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چیست منظورت ندانم هرچه هست</p></div>
<div class="m2"><p>زشت و زیبا پیش و پس بالا و پست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تو اعدا را نه جز نصرت به دست</p></div>
<div class="m2"><p>اهل بیت مصطفی را جز شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به کی در حق باطل اهتمام</p></div>
<div class="m2"><p>تا سرانجامت چه باشد انتقام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی جنایت مکیان را تشنه کام</p></div>
<div class="m2"><p>خواستی کشتن به کام اهل شام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاکنون هرچه از بنی آدم گذشت</p></div>
<div class="m2"><p>این ستم را هیچ کس صادر نگشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشته از خون شهیدان لاله گشت</p></div>
<div class="m2"><p>چون فضای گلستان دامان دشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بغض حق بودت هرآنچ اندر درون</p></div>
<div class="m2"><p>ریختی یکباره بیرون تاکنون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اختر عباس را کردی نگون</p></div>
<div class="m2"><p>اخترت گردد نگون ای چرخ دون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تارک تابان اکبر زیب نی</p></div>
<div class="m2"><p>قامت عریان قاسم زیر پی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن بهارش را دمید آغاز دی</p></div>
<div class="m2"><p>این شمارش را رسید انجام طی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نعش شاه تشنه کامان بی کفن</p></div>
<div class="m2"><p>همچو بط در لجه ی خون غوطه زن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تن جدا افتاده از سر، سر ز تن</p></div>
<div class="m2"><p>چاک چاک از تیغ زوبینش بدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این تطاول بس نبودت خود که باز</p></div>
<div class="m2"><p>بر حریمش دست آوری دراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جور و کین را خوب کردی برگ و ساز</p></div>
<div class="m2"><p>یک طرف خون ریز و یکسو ترکتاز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بعد قتل و غارت برنا و پیر</p></div>
<div class="m2"><p>آل احمد را نمودی دستگیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نز صغیرش در گذشتی نز کبیر</p></div>
<div class="m2"><p>ساختی در چنگ نامحرم اسیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خود پس از آشوب آن انداز و افت</p></div>
<div class="m2"><p>که نه کس گفتن تواند نه شنفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برزنان کردی مهیا طاق و جفت</p></div>
<div class="m2"><p>خاک خواری و اشک خونین خورد و خفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پای تا سر، دست و پا در غل و بند</p></div>
<div class="m2"><p>پور در زنجیر و دختر در کمند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پای رحمت لنگ و دست کین بلند</p></div>
<div class="m2"><p>هردم از صد ره برآنان صد گزند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرکه بود اندر حریمش روز و شب</p></div>
<div class="m2"><p>بیش و کم مشتاق مرگ از بس تعب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مادران را تن به جان، جان ها به لب</p></div>
<div class="m2"><p>کودکان را تاب در تن دل به تب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنچه کردی عاجز آمد جاودان</p></div>
<div class="m2"><p>پایه ی اوهام از ادراک آن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا قیامت رانم ار با صد زبان</p></div>
<div class="m2"><p>عشری از معشار نارم در میان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رفت خاک عصمت از جورت به باد</p></div>
<div class="m2"><p>تا به غبرا سایه از گردون فتاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کس چنین ظلمی ز کس نارد به یاد</p></div>
<div class="m2"><p>از تو نیز این ظلم هرگز رو نداد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کیفر کردار را ویران شوی</p></div>
<div class="m2"><p>همچو ما تا حشر بی سامان شوی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون صفایی واله و حیران شوی</p></div>
<div class="m2"><p>آس مان همواره سرگردان شوی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گرچه تشویشم همی از محشر است</p></div>
<div class="m2"><p>لیک رحمت مجرمین را در خور است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خاصه آن کش روی ذلت بردر است</p></div>
<div class="m2"><p>وز تواش ظل عنایت برسر است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آسمان ای آسمان تا کی ستمکاری</p></div>
<div class="m2"><p>فغان ای آسمان امان ای آسمان</p></div></div>