---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ای اکبر ای رعنا جوان وای وای</p></div>
<div class="m2"><p>درمان دل آرام جان وای وای</p></div></div>
<div class="b2" id="bn2"><p>سوی عدو آهسته ران وای وای</p></div>
<div class="b" id="bn3"><div class="m1"><p>سرو آزادم برادر</p></div>
<div class="m2"><p>شاخ شمشادم برادر</p></div></div>
<div class="b2" id="bn4"><p>جان ناشادم برادر</p></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم نیایی زین سفر وای وای</p></div>
<div class="m2"><p>باری بدین بیکس نگر وای وای</p></div></div>
<div class="b2" id="bn6"><p>یک لحظه رو آهسته تر وای وای</p></div>
<div class="b" id="bn7"><div class="m1"><p>دلخونم از هجران تو وای وای</p></div>
<div class="m2"><p>ترسم بسی بر جان تو وای وای</p></div></div>
<div class="b2" id="bn8"><p>دست من و دامان تو وای وای</p></div>
<div class="b" id="bn9"><div class="m1"><p>رحمی به حال زار من وای وای</p></div>
<div class="m2"><p>بر دیده ی خونبار من وای وای</p></div></div>
<div class="b2" id="bn10"><p>اندیشه ای در کار من وای وای</p></div>
<div class="b" id="bn11"><div class="m1"><p>عباس را رایت نگون وای وای</p></div>
<div class="m2"><p>قاسم به میدان غرق خون وای وای</p></div></div>
<div class="b2" id="bn12"><p>احباب کم اعداد فزون وای وای</p></div>
<div class="b" id="bn13"><div class="m1"><p>قومی به جنگ اندرکمین وای وای</p></div>
<div class="m2"><p>بر قصد جانت تیغ کین وای وای</p></div></div>
<div class="b2" id="bn14"><p>این از یسار آن از یمین وای وای</p></div>
<div class="b" id="bn15"><div class="m1"><p>پا ز اشک خونین در گلم وای وای</p></div>
<div class="m2"><p>صد کوه زین غم بر دلم وای وای</p></div></div>
<div class="b2" id="bn16"><p>افتاده کاری مشکلم وای وای</p></div>
<div class="b" id="bn17"><div class="m1"><p>زن های بی یاور ببین وای وای</p></div>
<div class="m2"><p>اطفال غم پرور ببین وای وای</p></div></div>
<div class="b2" id="bn18"><p>مادر نگر خواهر ببین وای وای</p></div>
<div class="b" id="bn19"><div class="m1"><p>رفتی و افتاد از غمم وای وای</p></div>
<div class="m2"><p>بر سینه داغ ماتمم وای وای</p></div></div>
<div class="b2" id="bn20"><p>بشکست هجران درهمم وای وای</p></div>
<div class="b" id="bn21"><div class="m1"><p>پر خون دل مینای من وای وای</p></div>
<div class="m2"><p>از جزع گوهرزای من وای وای</p></div></div>
<div class="b2" id="bn22"><p>ای وای من ای وای من ای وای</p></div>
<div class="b" id="bn23"><div class="m1"><p>بارد صفایی لعل تر وای وای</p></div>
<div class="m2"><p>در دامن از لخت جگر وای وای</p></div></div>
<div class="b2" id="bn24"><p>بنشسته در خون تاکمر وای وای</p></div>
<div class="b" id="bn25"><div class="m1"><p>دارد به سوگت متصل وای وای</p></div>
<div class="m2"><p>دستی به دل پایی به گل وای وای</p></div></div>
<div class="b2" id="bn26"><p>دیگر نپاید محتمل وای وای</p></div>