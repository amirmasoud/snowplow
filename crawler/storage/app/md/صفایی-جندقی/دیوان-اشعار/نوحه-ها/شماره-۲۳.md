---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>افراشت علم در صف گردون شه ماتم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورد شبیخون سوی دل ها سپه غم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیتی ز شفق خنجری آورد برون باز</p></div>
<div class="m2"><p>خون ریز و درون تاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذاشته نامش به غلط ماه محرم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برچید بساط فرح از ساحت دنیا</p></div>
<div class="m2"><p>چه پست و چه بالا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر داشت نشاط فرح از دوده ی آدم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پوشید ز نو ثوب سیه در بر افلاک</p></div>
<div class="m2"><p>زد جیب سکون چاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وافشاند دگر خاک عزا بر سر عالم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سینه چو مشعل رود این آه پیاپی</p></div>
<div class="m2"><p>جاوید نه تا کی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز دیده چو اخگر دمد این اشک دمادم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دهر نیابی ز ملایک لب خندان</p></div>
<div class="m2"><p>یک خاطر شادان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در شهر نبینی ز جفا یک دل خرم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این خوک منش خرس هوس چرخ دغل باز</p></div>
<div class="m2"><p>گرگ شره انباز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صید سگ و روباه نگر آهوی و ضیغم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون زلف بتان چون دل شوریده ی عشاق</p></div>
<div class="m2"><p>چون خاطر مشتاق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین تاب و تب احوال جهان آمده درهم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این قتل که بودش ز قفا نهب و اسیری</p></div>
<div class="m2"><p>تا سست نگیری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کاسباب غم از شش جهت آورد فراهم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خونین بدن افتاده به دشت آل علی آه</p></div>
<div class="m2"><p>زان فرقه ی گمراه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گلگون کفن از خاک دمد کاش سپرغم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خود سود سرشکم چه درین ماتم جانکاه</p></div>
<div class="m2"><p>یا فایده ی آه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تنها نه من اینگونه در این بزم، شما هم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جیحون نکنم راغ گر از چشم شمرزای</p></div>
<div class="m2"><p>با اشک گرانپای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کانون نکنم باغ اگر ز آه شرر دم</p></div>
<div class="m2"><p>بر رخ همه ایام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درویش و غنی شاه و گدا بنده و آزاد</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تنظیم عزا را به فغان آمده همدم</p></div>
<div class="m2"><p>پیدا و نهان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گردون چکد از دیده اگر اشک شفق فام</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا حشر در این تعزیه بسیار بود کم</p></div>
<div class="m2"><p>سرگشته و حیران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کانون و شمر ز اشک و نوا ماتمیان را</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چندان عجبی نیست که جمع آمده با هم</p></div>
<div class="m2"><p>وین شرح غم اندوز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در تیه بلا ماند به وا موسی عمران</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در نیل عزا بود قبا عیسی مریم</p></div>
<div class="m2"><p>بی هیچ غرامت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دانی چه بود در بر این وقعه ی جانسوز</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تحریر و بیان تو و من قلزم و شبنم</p></div>
<div class="m2"><p>اسپید و سیاهم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ضنت مکن از گریه که صد بحر کرامت</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هم وزن برآید به ترازوی تو یک نم</p></div>
<div class="m2"><p>هر چند خطا نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>منع از دگری می نکند اشکم و آهم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این آتش و آبی است که پیدا شده توأم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این نظم که نقصان و خطایش ز صفایی است</p></div>
<div class="m2"><p>غم دیده و دل شاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ور هست ثوابی بود از حضرت مریم</p></div>
<div class="m2"><p>در ماه محرم</p></div></div>