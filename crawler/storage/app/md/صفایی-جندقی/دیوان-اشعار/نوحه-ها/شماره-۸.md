---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای قاسم ای سرو روان ای مادر</p></div>
<div class="m2"><p>سوی عدو آهسته ران ای مادر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را به هجرانت شکیبایی کو</p></div>
<div class="m2"><p>ای نور چشم دلستان ای مادر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد کجا آرام و طاقت جان را</p></div>
<div class="m2"><p>بی رویت ای آرام جان ای مادر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصد قتال قوم طاغی کردی</p></div>
<div class="m2"><p>بر قتل خود بستی میان ای مادر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندان مکن تعجیل و این سفر را</p></div>
<div class="m2"><p>تأخیر فرما یک زمان ای مادر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رجعت نبینم در پی این رفتن را</p></div>
<div class="m2"><p>صبر از فراقت کی توان ای مادر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود چون شود گر رخش جانبازی را</p></div>
<div class="m2"><p>یکدم نگهداری عنان ای مادر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بهر تیراندازی تو امروز</p></div>
<div class="m2"><p>خم گشته قدم چون کمان ای مادر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هر نفس مرگی فراهم آید</p></div>
<div class="m2"><p>بعد از تو ما را در جهان ای مادر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکدم بیا و قامت سرو آسا</p></div>
<div class="m2"><p>بر چشمه ی چشمم نشان ای مادر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مخرام و چون سرو چمان زمانی</p></div>
<div class="m2"><p>بنشین بر این جوی روان ای مادر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد کشته ی تیغ جفای اعدا</p></div>
<div class="m2"><p>اعوان ما پیر و جوان ای مادر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی سر نگر تن های یاوران را</p></div>
<div class="m2"><p>یکسر به خاک و خون تپان ای مادر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی تن ببین سرهای سروران را</p></div>
<div class="m2"><p>آویزه ی نوک سنان ای مادر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درهای غم بگشای بر صفایی</p></div>
<div class="m2"><p>از نوحه گر بندد زبان ای مادر</p></div></div>