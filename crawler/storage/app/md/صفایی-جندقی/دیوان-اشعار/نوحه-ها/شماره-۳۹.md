---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>خصم جان کوکب تو دشمن تن اختر ما</p></div>
<div class="m2"><p>رنج قتل اول تو خواری بند آخر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل نهاد از تو به دوری تن غم پرور ما</p></div>
<div class="m2"><p>ما برفتیم و تو دانی و دل غم خور ما</p></div></div>
<div class="b2" id="bn3"><p>بخت بد تا به کجا می برد آبشخور ما</p></div>
<div class="b" id="bn4"><div class="m1"><p>سر چو ناکام ز خاک قدمت برگیرم</p></div>
<div class="m2"><p>بند برپا به اسیری پی لشکر گیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نیفتد که سر زلف تو از سر گیرم</p></div>
<div class="m2"><p>از نثار مژه چون زلف تو در زر گیرم</p></div></div>
<div class="b2" id="bn6"><p>قدمی کز تو سلامی برساند برما</p></div>
<div class="b" id="bn7"><div class="m1"><p>فرقتم برد فرو دم به دعا دست برآر</p></div>
<div class="m2"><p>هجرتم ساخت طرب غم به دعا دست برآر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوه بسیار و زمان کم به دعا دست برآر</p></div>
<div class="m2"><p>به وداع آمده ام هم به دعا دست برآر</p></div></div>
<div class="b2" id="bn9"><p>که وفا با تو قرین باد و خدا یاور ما</p></div>
<div class="b" id="bn10"><div class="m1"><p>ملک ار بیش و اگر کم به سرم تیغ کشند</p></div>
<div class="m2"><p>پای تا سر بنی آدم به سرم تیغ کشند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شرق تا غرب مسلم به سرم تیغ کشند</p></div>
<div class="m2"><p>به سرت گر همه عالم به سرم تیغ کشند</p></div></div>
<div class="b2" id="bn12"><p>نتوان برد هوای تو برون از سر ما</p></div>
<div class="b" id="bn13"><div class="m1"><p>دهر محروم از این رو کندم میدانم</p></div>
<div class="m2"><p>بخت مهجور از این کو کندم می دانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خصم زنجیر به بازو کندم می دانم</p></div>
<div class="m2"><p>چرخ آواره بهر سو کندم می دانم</p></div></div>
<div class="b2" id="bn15"><p>رشک می آیدش از صحبت جان پرور ما</p></div>
<div class="b" id="bn16"><div class="m1"><p>مرد و زن پیر و جوان بر من و تو حیف خورند</p></div>
<div class="m2"><p>بیش و کم فاش و نهان بر من و توحیف خورند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه ابنای زمان برمن و تو حیف خورند</p></div>
<div class="m2"><p>گر همه خلق جهان بر من و توحیف خورند</p></div></div>
<div class="b2" id="bn18"><p>بکشد از همه انصاف ستم داور ما</p></div>
<div class="b" id="bn19"><div class="m1"><p>تا از آن رو چه دم افزون و چه کم زد حافظ</p></div>
<div class="m2"><p>تا از آن حسن دلاویز قلم زد حافظ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا از آن چهر چمن خیز رقم زد حافظ</p></div>
<div class="m2"><p>تا ز وصف رخ زیبای تو دم زد حافظ</p></div></div>
<div class="b2" id="bn21"><p>ورق گل خجل است از ورق دفتر ما</p></div>
<div class="b" id="bn22"><div class="m1"><p>در مقامی که پسر سخت گریزد ز پدر</p></div>
<div class="m2"><p>پدر از ماتم خود سست گراید به پسر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون صفایی همه از تابش خور در آذر</p></div>
<div class="m2"><p>نارد ار قامت اقبال توام سایه به سر</p></div></div>
<div class="b2" id="bn24"><p>خالی از قصه قیامت گذرد محشر ما</p></div>