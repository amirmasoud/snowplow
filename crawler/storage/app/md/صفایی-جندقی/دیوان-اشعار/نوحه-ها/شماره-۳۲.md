---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>از حرم تا شام گردون بر زمین گسترد دامی</p></div>
<div class="m2"><p>که نه شاهینی ز بندش رست خواهد نه حمامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاهر آمد دست کین بازوی کفر افتاد چیره</p></div>
<div class="m2"><p>نه نشانی ماند از ایمان نه از اسلام نامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی ای دست عتاب و عدل حق در آستینی</p></div>
<div class="m2"><p>تا کی ای تیغ قصاص دولت و دین در نیامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آل احمد تا کجا خواری کشند از خیل مروان</p></div>
<div class="m2"><p>قهری ای عنف سپهر ای خشم اختر انتقامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دماء پاک پرور ای فرات عذب گوهر</p></div>
<div class="m2"><p>بی گنه تا کی حلالی بی جهت تا کی حرامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قحط آب است ای دریغا اشک تلخ و شور من</p></div>
<div class="m2"><p>دست گیرد تا مگر دریای رحمت را به جامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داوری را دیده در راه تو داریم ای قیامت</p></div>
<div class="m2"><p>لنگ لنگان پویه تا کی؟ شل نه ای برادر گامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقعه ی کبری مگر کیفر کند این کفر و کین را</p></div>
<div class="m2"><p>ای سپهر آخر درنگی، ای زمین آخر خرامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک نایی زین تحکم خون نگردی زین تعدی</p></div>
<div class="m2"><p>جان نه پولادی و آهن دل نه سندان و رخامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صحن کیهان برنتابد با دو رستاخیز کبری</p></div>
<div class="m2"><p>این تطاول را تقاعد یا قیامت را قیامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای جهان آویز غوغا ای سلامت سوز ماتم</p></div>
<div class="m2"><p>نینوایی یا قیامت کوفه یا آشوب شامی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر سران خسته تن هر گام رستاخیز خاصی</p></div>
<div class="m2"><p>بر زنان بسته پر هر چشم زد غوغای عامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از حریم تست خاکم در دهان بی هیچ حرمت</p></div>
<div class="m2"><p>گر سگی جوی کنیزی یا خسی خواهد غلامی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با چنان حق سوز باطل و آن مسلمان تاز کافر</p></div>
<div class="m2"><p>دولت و دین را کجا قوت بماند یا قوامی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هان صفایی بر لب خشک شهیدان خون گری خون</p></div>
<div class="m2"><p>تا بود آن زخم ازین مرهم پذیرد التیامی</p></div></div>