---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>قلم از ازل ندانم چه نوشته بر سر من</p></div>
<div class="m2"><p>مگرم برای ماتم همه زاد مادر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای فلک ای فلک ای فلک داد</p></div>
<div class="m2"><p>از دست جفای تو فریاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مصاف حق و باطل به خلاف حق گذاری</p></div>
<div class="m2"><p>ز چه پاره پاره افتد بدن برادر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زمین ظلم کسوت ز سپهر خصم جامه</p></div>
<div class="m2"><p>نه کفن به پیکر او نه نقاب بر سر من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حضر ز تف وادی به سفر ز خیل ماتم</p></div>
<div class="m2"><p>تب و تاب همدم وی غم و رنج یاور من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عناد دهر بی سر تن چاک قاسم او</p></div>
<div class="m2"><p>ز فساد خصم بی تن سر پاک اکبر من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ستیزه های کیوان همه خاک بستر وی</p></div>
<div class="m2"><p>ز زبانه های افغان همه دود معجر من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن یاوران به یک سر، سر سروران به یکسو</p></div>
<div class="m2"><p>چه مصیبت است یا رب که بسوزد اختر من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوی کوفه ره سپارم به اسیری ای برادر</p></div>
<div class="m2"><p>غم تست توشه ی تن سر تست رهبر من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ثبات جان مرا بس عجب است و این عجب تر</p></div>
<div class="m2"><p>که در آتش جدایی نگداخت پیکر من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو به بزم غم بسر شد ز تو دور باده خواران</p></div>
<div class="m2"><p>کند آسمان را ز خون همه دوره ساغر من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر از لحد برآید پی دادخواهی ما</p></div>
<div class="m2"><p>برسان صبا از این غم خبری به مادر من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غم قتل و رنج غارت لب خشک و چشم گریان</p></div>
<div class="m2"><p>بنه ار یقین نداری قدمی برابر من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عجب است اگر نسوزد چو سپند از آتش جان</p></div>
<div class="m2"><p>تن چاک چاک او را دل داغ پرور من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به عنایت شهیدان چه غم از گنه صفایی</p></div>
<div class="m2"><p>که ولای اهل بیت است شفیع محشر من</p></div></div>