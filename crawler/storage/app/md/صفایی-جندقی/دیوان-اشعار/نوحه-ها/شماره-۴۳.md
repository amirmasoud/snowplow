---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>دل همی خواست که ریزد سر و جان در پایت</p></div>
<div class="m2"><p>اینک اندر پی خون تیغ به کف اعدایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخصت حرب گر از آن لب جان بخشایت</p></div>
<div class="m2"><p>سر تسلیم نهادیم به حکم و رایت</p></div></div>
<div class="b2" id="bn3"><p>تا چه اندیشه کند رای جهان آرایت</p></div>
<div class="b" id="bn4"><div class="m1"><p>جان سپردن به تولای تو در سر داریم</p></div>
<div class="m2"><p>سر نهادن به کف پای تو در سر داریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه کنون شوق تماشای تو در سر داریم</p></div>
<div class="m2"><p>روزگاری است که سودای تو در سر داریم</p></div></div>
<div class="b2" id="bn6"><p>مگر این سر برود تا برود سودایت</p></div>
<div class="b" id="bn7"><div class="m1"><p>چشم خاصان به تو مشغول ز هر چهر و قدی</p></div>
<div class="m2"><p>جان پاکان به تو مشعوف ز هر نیک و بدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو به هردل که گذشتی و درو جای شدی</p></div>
<div class="m2"><p>تو به هرجا که فرود آمدی و خیمه زدی</p></div></div>
<div class="b2" id="bn9"><p>کس دگر می نتواند که بگیرد جایت</p></div>
<div class="b" id="bn10"><div class="m1"><p>عمر بردن همه بی روی تو رنج است و ملال</p></div>
<div class="m2"><p>زار مردن همه در کوی تو عیش است و وصال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محو نقش بر و بالات سراپای خیال</p></div>
<div class="m2"><p>همچو مستسقی و سرچشمه ی نوشین زلال</p></div></div>
<div class="b2" id="bn12"><p>سیر نتوان شدن از دیدن مهرافزایت</p></div>
<div class="b" id="bn13"><div class="m1"><p>جان سپاری رهت خواست دل از عهد الست</p></div>
<div class="m2"><p>حاصل از عمر نداریم جز این مایه به دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کیست آن کس که از او دل نتوانیم گسست</p></div>
<div class="m2"><p>دیگری نیست که مهر تو بدو شاید بست</p></div></div>
<div class="b2" id="bn15"><p>هم در آیینه توان دید مگر همتایت</p></div>
<div class="b" id="bn16"><div class="m1"><p>وقت آن شد که رفیقان سر هیجا گیرند</p></div>
<div class="m2"><p>بگذرند از سر جان راه براعدا گیرند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دم دیگر به صف خلد برین جا گیرند</p></div>
<div class="m2"><p>روز آن است که یاران ره صحرا گیرند</p></div></div>
<div class="b2" id="bn18"><p>خیز تا سرو بماند خجل از بالایت</p></div>
<div class="b" id="bn19"><div class="m1"><p>سر و تن چیست که در پای تو گردد ایثار</p></div>
<div class="m2"><p>دل و جان نیز درین راه سزاوار نثار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مردمان طعن زنندم که نگشتی بیدار</p></div>
<div class="m2"><p>دوستان عیب کنندم که نبودی هشیار</p></div></div>
<div class="b2" id="bn21"><p>تا فرو رفت به گل پای جهان پیمایت</p></div>
<div class="b" id="bn22"><div class="m1"><p>غافل از آن که درین خاک به خون باید خفت</p></div>
<div class="m2"><p>جز به خون ریزی دل غنچه شادی نشکفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخن از هرکه صفایی نه سزاوار شنفت</p></div>
<div class="m2"><p>دوش در واقعه دیدم که نگاری می گفت</p></div></div>
<div class="b2" id="bn24"><p>سعدیا گوش مکن بر سخن اعدایت</p></div>