---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>دردا که از زین سرنگون افتاد بر خاک اکبرم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک خواری اوفتاد از فرق امروز افسرم</p></div>
<div class="m2"><p>خاک دوعالم برسرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با زندگانی زین سپس ارمان ندارم یک نفس</p></div>
<div class="m2"><p>درمان من مرگ است و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جای این افسر سپهر ای کاش بربودی سرم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک بهی و آب بقا زین آتشم برباد شد</p></div>
<div class="m2"><p>وین خانه از بنیاد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آری نباشد سختر از سنگ و سندان پیکرم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این صرصر طوفان ثمر وین شعله نیران شرر</p></div>
<div class="m2"><p>کم سوخت تا پایان ز سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نینوا بر باد داد از بیخ و بن خاکسترم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آهم دمادم همزبان، هم زانویم اشک روان</p></div>
<div class="m2"><p>داغ جوانم ارمغان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم هم سفر، ره راحله، دل زاد و سرها رهبرم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشمن همی دانی چرا بی ساز و سامانم کند</p></div>
<div class="m2"><p>وز عمد عریانم کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا کسوت کحلی فلک آراید از نو در برم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گردون سراندازم ربود از دست خصم شوم پی</p></div>
<div class="m2"><p>و اینها گمانم بود کی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا از نو اندازد به سر این کهنه نیلی معجرم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو تشنه لب جان بسپری من زنده با این چشم تر</p></div>
<div class="m2"><p>ماندن ز مردن تلخ تر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کاش از جهان دریا و جو خشک آمدی آبشخورم</p></div>
<div class="m2"><p>خاک دو عالم برسرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صد بحر مرجانم برفت از کف که مرجانش بها</p></div>
<div class="m2"><p>داد از که جویم زین جفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در لجه ی کین تا فرو شد این گرامی گوهرم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صد آسمان کیوان نحس از برج اقبالم سیه</p></div>
<div class="m2"><p>سر زد که زان حالم تبه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در خاک تا بنهفت رخ رخشنده تابان اخترم</p></div>
<div class="m2"><p>خاک دو عالم برسرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دوران چو شد ساقی همی بر سنگ زد مینای من</p></div>
<div class="m2"><p>کز غم کند صهبای من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در بزم عاشورا کنون انباشت از خون ساغرم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر آستان بندگی تا رخ نهاد از مسکنت</p></div>
<div class="m2"><p>دارد صفایی سلطنت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ور در قیامت نشمری باز از سگان این درم</p></div>
<div class="m2"><p>خاک دو عالم بر سرم</p></div></div>