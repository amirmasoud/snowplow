---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>مدر پیراهن طاقت غریبان را</p></div>
<div class="m2"><p>مزن دامن بر آتش ناشکیبان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برادر جان علی اکبر</p></div>
<div class="m2"><p>از این رفتن بیا بگذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفر خواهی اگر خاکم به سر خواهی</p></div>
<div class="m2"><p>اگر خاکم به سر خواهی سفر خواهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ما پیر و جوان هرکس به میدان شد</p></div>
<div class="m2"><p>در اول پی به خون و خاک یکسان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن سر دادگان در خون تپان بنگر</p></div>
<div class="m2"><p>سر آزادگان بر نی روان بنگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی را سر به نوک نیزه عریان بین</p></div>
<div class="m2"><p>ز چوگان ستم چون گوی غلطان بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی را تن به خاک ازچرخ دون بنگر</p></div>
<div class="m2"><p>چو خاک از بخت وارونش زبون بنگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ما هفتاد تن یک یک برون آمد</p></div>
<div class="m2"><p>که خاک از خون پاکش لاله گون آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پدر تنها عدو بی رحم و ما بی کس</p></div>
<div class="m2"><p>غریبان را همان داغ شهیدان بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس از خود خواری ما را توهم کن</p></div>
<div class="m2"><p>بر این مشت اسیر آخر ترحم کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رعایت کن زمهر این جمع مضطر را</p></div>
<div class="m2"><p>برادر را و خواهر را و مادر را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل ازکف داده گانت را صبوری کو</p></div>
<div class="m2"><p>تنی کش دل نباشد تاب دوری کو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو لایق باشد الطاف خدایی را</p></div>
<div class="m2"><p>چه خوف از فتنه محشر صفایی را</p></div></div>