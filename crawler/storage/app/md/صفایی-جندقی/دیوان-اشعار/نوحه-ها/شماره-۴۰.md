---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>دردا که ماند در دل بس حسرت از جوانان</p></div>
<div class="m2"><p>آوخ که سوخت جان ها بر داغ مهربانان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن کی سبک پی از بند دل گرانان</p></div>
<div class="m2"><p>خفته خبر ندارد سر در کنار جانان</p></div></div>
<div class="b2" id="bn3"><p>کاین شب دراز باشد بر چشم پاسبانان</p></div>
<div class="b" id="bn4"><div class="m1"><p>در مرگ دوستداران دل چون تحمل آرد</p></div>
<div class="m2"><p>کی ز آستین توان بست چشمی که خون ببارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تاب تن چه تشویش آن را که جان سپارد</p></div>
<div class="m2"><p>دل داده را ملامت کردن چسود دارد</p></div></div>
<div class="b2" id="bn6"><p>می باید این نصیحت کردن به دل ستانان</p></div>
<div class="b" id="bn7"><div class="m1"><p>با درد هجر نشگفت گر هردمت بگریم</p></div>
<div class="m2"><p>بی چهر عالم آرا یک عالمت بگریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برکشته ات بنالم در ماتمت بگریم</p></div>
<div class="m2"><p>بر اشک من بخندی گر در غمت بگریم</p></div></div>
<div class="b2" id="bn9"><p>کاین کارهای مشکل افتد به کاردانان</p></div>
<div class="b" id="bn10"><div class="m1"><p>چرخم به قید و چنبر زین آستان کشاند</p></div>
<div class="m2"><p>وز نعش کشتگانم با تازیانه راند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور ساعتی بپایم خصمم بسر دواند</p></div>
<div class="m2"><p>شکر فروش مصری حال مگس چه داند</p></div></div>
<div class="b2" id="bn12"><p>این دست شوق برسر آن آستین فشانان</p></div>
<div class="b" id="bn13"><div class="m1"><p>بعد از تو ای برادر هرچند دستگیرم</p></div>
<div class="m2"><p>وز پیش دوستداران دشمن برد اسیرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیکن به داغ و حسرت تا در غمت نمیرم</p></div>
<div class="m2"><p>چشم از تو برنگیرم ور می زنند تیرم</p></div></div>
<div class="b2" id="bn15"><p>مشتاق گل بسازد با خوی باغبانان</p></div>
<div class="b" id="bn16"><div class="m1"><p>ای ساربان زمانی بار سفر مبندم</p></div>
<div class="m2"><p>کاین تشنه کشتگان را چون ناقه پای بندم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور باشد از اعادی هرگام صد گزندم</p></div>
<div class="m2"><p>من ترک مهر اینان برخود نمی پسندم</p></div></div>
<div class="b2" id="bn18"><p>بگذار تا بیاید برمن جفای آنان</p></div>
<div class="b" id="bn19"><div class="m1"><p>هست از کشاکش خصم این ره که می سپارم</p></div>
<div class="m2"><p>ورنی چنین نبایست نعشت بجا گذارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن نیستم که بی دوست آنی تحمل آرم</p></div>
<div class="m2"><p>باور مکن که من دست از دامنت بدارم</p></div></div>
<div class="b2" id="bn21"><p>شمشیر نگسلاند پیوند مهربانان</p></div>
<div class="b" id="bn22"><div class="m1"><p>غم نیست گر به مهرت انباز داغ و دردیم</p></div>
<div class="m2"><p>طومار شادمانی بهر تو درنوردیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با جان و سر نپیچیم وز راه برنگردیم</p></div>
<div class="m2"><p>ما اختیار خود را تسلیم عشق کردیم</p></div></div>
<div class="b2" id="bn24"><p>همچون زمام اشتر در دست ساربانان</p></div>
<div class="b" id="bn25"><div class="m1"><p>آنجا که حق پرستان صف برزنند سعدی</p></div>
<div class="m2"><p>وز باده ی شهادت ساغر زنند سعدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باید که چون صفاییت خنجر زنند سعدی</p></div>
<div class="m2"><p>شاید که آستینت برسر زنند سعدی</p></div></div>
<div class="b2" id="bn27"><p>تا چون مگس نگردی گرد شکر دهانان</p></div>