---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ای شه مظلوم حسین وای وای</p></div>
<div class="m2"><p>بی کس و محروم حسین وای وای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره تسلیم و رضا جز توکیست</p></div>
<div class="m2"><p>جان به وفا کرده فدا جز تو کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر بازار ولا جز تو کیست</p></div>
<div class="m2"><p>مشتری جنس بلا جز تو کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غرقه ی دریای فنا وای وای</p></div>
<div class="m2"><p>تشنه ی صحرای بلا وای وای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر ز بدن مانده جدا وای وای</p></div>
<div class="m2"><p>رفته سوی ملک بقا وای وای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیل زنا صف به صف آراستند</p></div>
<div class="m2"><p>قتل ترا یک تنه برخاستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرمت و جاه تو فروکاستند</p></div>
<div class="m2"><p>ذل تو عزت خود خواستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو اقلیم الم وای وای</p></div>
<div class="m2"><p>سرور بی خیل و حشم وای وای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشته ی شمشیر ستم وای وای</p></div>
<div class="m2"><p>تشنه لب وادی غم وای وای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قودم دغا قدر تو نشناختند</p></div>
<div class="m2"><p>رایت حرب تو برافراختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخل تو از پای در انداختند</p></div>
<div class="m2"><p>اسب ستم بر بدنت تاختند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یوسف گل پیرهنم وای وای</p></div>
<div class="m2"><p>کشته خونین کفنم وای وای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلبل شیرین سخنم وای وای</p></div>
<div class="m2"><p>طوطی شکر شکنم وای وای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اهل جفا بهر تو اندوختند</p></div>
<div class="m2"><p>هر چه جفا و ستم آموختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز آتش خشمی که برافروختند</p></div>
<div class="m2"><p>خیمه و خرگاه ترا سوختند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میر علمدار توکو وای وای</p></div>
<div class="m2"><p>لشکر و انصار توکو وای وای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مادر غم خوار تو کو وای وای</p></div>
<div class="m2"><p>باب وفادار تو کو وای وای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون تو به سوادی غم افتاده کیست</p></div>
<div class="m2"><p>درد و بلا را چو تو آماده کیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تن به دواهی همه در داده کیست</p></div>
<div class="m2"><p>دل به شهادت چو تو بنهاده کیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه ملایک سپهم وای وای</p></div>
<div class="m2"><p>غرقه به خون بی گنهم وای وای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر به زیر خاک رهم وای وای</p></div>
<div class="m2"><p>خفته به خاک سپهم وای وای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رخش به عزم جدل انگیختی</p></div>
<div class="m2"><p>گرد عزا بر رخ ما ریختی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاک به خون بدن آمیختی</p></div>
<div class="m2"><p>خاک سیه بر سر ما بیختی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی تو چه سازدم به جهان وای وای</p></div>
<div class="m2"><p>کز توصبوری نتوان وای وای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خاک مرا بر سر جان وای وای</p></div>
<div class="m2"><p>زیست کنم بی تو چسان وای وای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روی بدان وجه خدایی کنم</p></div>
<div class="m2"><p>زین پس از آن باب گدایی کنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در غم او نوحه سرایی کنم</p></div>
<div class="m2"><p>نوحه سرایی چو صفایی کنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا ز غمم باز خرد وای وای</p></div>
<div class="m2"><p>بنده ی خویشم شمرد وای وای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از سر جرمم گذرد وای وای</p></div>
<div class="m2"><p>نام گناهم نبرد وای وای</p></div></div>