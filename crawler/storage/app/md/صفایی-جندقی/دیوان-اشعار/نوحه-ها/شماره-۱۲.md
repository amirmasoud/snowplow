---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>آنکه آمد نه فلک نیم آستانش</p></div>
<div class="m2"><p>بی دریغ افکند بر خاک آسمانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فخر بودی جاودان روح القدس را</p></div>
<div class="m2"><p>فرق سودی گر به پای پاسبانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه جان از پرتو جسمش جهان را</p></div>
<div class="m2"><p>شد لگدکوب مخالف جسم و جانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خصم از بی مغزی و هیچ استخوانی</p></div>
<div class="m2"><p>نرم کرد از نعل مرکب استخوانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه قطب دایره امکان گرفتند</p></div>
<div class="m2"><p>ای دریغا مرکز آسا در میانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصبی تن خست از نوک خدنگش</p></div>
<div class="m2"><p>ملحدی دل سوخت از رمح سنانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کافری بگداخت بر داغ صغیرش</p></div>
<div class="m2"><p>مشرکی بشکست برمرگ جوانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب بست این سگ به روی اهل بیتش</p></div>
<div class="m2"><p>آتش افکند آن دد اندر دودمانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آه از آن طوفان که نوح نینوا را</p></div>
<div class="m2"><p>غرق شد زورق به بحر بی کرانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا علم سازد عدو نامی به عالم</p></div>
<div class="m2"><p>خواست کز عالم براندازد نشانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکر لله زین عمل جز لعن و نفرین</p></div>
<div class="m2"><p>نیست تا محشر جزایی در جهانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون در آرندش به محشر نیز باشد</p></div>
<div class="m2"><p>کیفر کفران عذاب جاودانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر تولای و تبرای صفایی</p></div>
<div class="m2"><p>کاین معانی هست پیدا از بیانش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنگر ای شاهنشه ملک شفاعت</p></div>
<div class="m2"><p>رحمتی هم آشکارا هم نهانش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر زمان در کاه عمر دشمنان را</p></div>
<div class="m2"><p>هر نفس بفزای عز دوستانش</p></div></div>