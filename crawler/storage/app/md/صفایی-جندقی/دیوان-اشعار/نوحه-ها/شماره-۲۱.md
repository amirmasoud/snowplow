---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>حسین ای خسرو لب تشنگانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn2"><p>برادر جان برادر</p></div>
<div class="b" id="bn3"><div class="m1"><p>فروغ چشم و بازوی توانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn4"><p>برادر جان برادر</p></div>
<div class="b" id="bn5"><div class="m1"><p>فلک تا از حجازت پرده افراخت</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn6"><p>رهی جانسوز بنواخت</p></div>
<div class="b" id="bn7"><div class="m1"><p>عراقی ساخت آهنگ فغانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn8"><p>برادر جان برادر</p></div>
<div class="b" id="bn9"><div class="m1"><p>اجل از پشت زین با جسم صد چاک</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn10"><p>فکندت بر سر خاک</p></div>
<div class="b" id="bn11"><div class="m1"><p>فلک زد بر زمین از آسمانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn12"><p>برادر جان برادر</p></div>
<div class="b" id="bn13"><div class="m1"><p>به دام روزگارت بال و پر ریخت</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn14"><p>فلک خاکم به سر بیخت</p></div>
<div class="b" id="bn15"><div class="m1"><p>همایون طایر عرش آشیانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn16"><p>برادر جان برادر</p></div>
<div class="b" id="bn17"><div class="m1"><p>شفق فام آمد از خونت زمین آه</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn18"><p>فلک سوز آتشین آه</p></div>
<div class="b" id="bn19"><div class="m1"><p>ستاره سوخت در هفت آسمانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn20"><p>برادر جان برادر</p></div>
<div class="b" id="bn21"><div class="m1"><p>ز شاخ دولتت تا برگ و بر ریخت</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn22"><p>قضا طوفان برانگیخت</p></div>
<div class="b" id="bn23"><div class="m1"><p>وز آن صرصر بهار آمد خزانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn24"><p>برادر جان برادر</p></div>
<div class="b" id="bn25"><div class="m1"><p>سموم غم چنانم خشک و تر سوخت</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn26"><p>که تا آتش برافروخت</p></div>
<div class="b" id="bn27"><div class="m1"><p>گلی نگذاشت از یک گلستانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn28"><p>برادر جان برادر</p></div>
<div class="b" id="bn29"><div class="m1"><p>ترا تا قامت از پیکان خونریز</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn30"><p>کمان ناوک آویز</p></div>
<div class="b" id="bn31"><div class="m1"><p>ز ناله ناوک از قامت کمانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn32"><p>برادر جان برادر</p></div>
<div class="b" id="bn33"><div class="m1"><p>گلت ماند از عطش نیلوفری رنگ</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn34"><p>به دامان این دل تنگ</p></div>
<div class="b" id="bn35"><div class="m1"><p>فشاند از دیده صد باغ ارغوانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn36"><p>برادر جان برادر</p></div>
<div class="b" id="bn37"><div class="m1"><p>مرا افکنده بود اندوه اکبر</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn38"><p>به دل یک دوزخ آذر</p></div>
<div class="b" id="bn39"><div class="m1"><p>غمت یک باره زد آتش به جانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn40"><p>برادر جان برادر</p></div>
<div class="b" id="bn41"><div class="m1"><p>صفایی زین غم ار گویم بیانی</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn42"><p>نویسم داستانی</p></div>
<div class="b" id="bn43"><div class="m1"><p>زبان سوزد فرو ریزد بنانم</p></div>
<div class="m2"><p>برادر</p></div></div>
<div class="b2" id="bn44"><p>برادر جان برادر</p></div>