---
title: >-
    بند ۲۵
---
# بند ۲۵

<div class="b" id="bn1"><div class="m1"><p>کاندیشه ناکم از غم بی یاری شما</p></div>
<div class="m2"><p>در تابم از خیال گرفتاری شما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادم تنی به مرگ و نهادم دلی به صبر</p></div>
<div class="m2"><p>هرچند دل خورم ز جگر خواری شما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناچار خاطر همه آزردم ارنه من</p></div>
<div class="m2"><p>هرگز رضا نیم به دل آزاری شما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عباسم از جهان شد و با آه و اشک ماند</p></div>
<div class="m2"><p>سقایی بنات و علمداری شما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطع نظر کنید ز منهم که بعد از این</p></div>
<div class="m2"><p>با نیزه است نوبت سرداری شما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیبد گلی ز خون گلو روی زرد من</p></div>
<div class="m2"><p>تا کاهی از عطش رخ گلناری شما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمتر کنید سینه و کمتر به سر زنید</p></div>
<div class="m2"><p>کاین لحظه نیست وقت عزاداری شما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آبی بر آتشم نتوانید زد ز اشک</p></div>
<div class="m2"><p>افزود تابش دلم از زاری شما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این بود سود گریه که اعدای رحم سوز</p></div>
<div class="m2"><p>خون خوارتر شدند ز خون باری شما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اطفال بی پدر همه را مادری کنید</p></div>
<div class="m2"><p>تا شادمان زیند به غم خواری شما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در حق این مریض پریشان مستمند</p></div>
<div class="m2"><p>جمع است خاطرم به پرستاری شما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کم نیست اگر به ذل اسیری کنید صبر</p></div>
<div class="m2"><p>از عزت شهادت ما خواری شما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درکارها خداست وکیل و کفیل من</p></div>
<div class="m2"><p>کافی است حفظ او به نگهداری شما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم خشم اوکند طلب خون ما ز خصم</p></div>
<div class="m2"><p>هم نصر او رسد به مددکاری شما</p></div></div>
<div class="b2" id="bn15"><p>پس پیش راند و رو به سپاه ستم نهاد</p>
<p>صد داغ تازه بردل اهل حرم نهاد</p></div>