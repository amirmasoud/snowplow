---
title: >-
    بند ۹۵
---
# بند ۹۵

<div class="b" id="bn1"><div class="m1"><p>آنانکه نفس خویش جری بر جفا کنند</p></div>
<div class="m2"><p>چون صبر بر شکنجه روز جزا کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر قتال آل نبی تیغ ها به دست</p></div>
<div class="m2"><p>با آنکه دین وی به زبان ادعا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک تغافل اهل ستم را چو رسم نیست</p></div>
<div class="m2"><p>کاش اندکی به حالت خویش اعتنا کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قومی که در عقوبتشان افتراق نیست</p></div>
<div class="m2"><p>دارند اگر نماز به پا یا زنا کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخویش گشته مانع نعمای سرمدی</p></div>
<div class="m2"><p>منع نوا ز مالک منع و عطا کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه خجلت از بتول و نه خشیت ز بوتراب</p></div>
<div class="m2"><p>نه حرمت از رسول و نه شرم از خدا کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردا که خیل فصل خداوند وصل را</p></div>
<div class="m2"><p>در خاک و خون فکنده سر از تن جدا کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون حرام وی به تهور هدر دهند</p></div>
<div class="m2"><p>مال حلال وی به تقلب هبا کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیراهنی که فاطمه اش رشته پود و تار</p></div>
<div class="m2"><p>بر تن سگان گرگ شعارش قبا کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و آن کشته را به نعل ستوران خاره سای</p></div>
<div class="m2"><p>در چشم خاک سرمه روش توتیا کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دارند پاس حرمت قرآن ولی چه سود</p></div>
<div class="m2"><p>تا خود نه امتیاز خلوص از ریا کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز الفاظ اگر مراد معانی است بی گزاف</p></div>
<div class="m2"><p>معنی ندارد آنکه به لفظ اکتفا کنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز اهل ضلال یک سر مو کوتهی نشد</p></div>
<div class="m2"><p>در دفع حق هر آنچه توان دست و پا کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>واسومتا به حالت امت که در نشور</p></div>
<div class="m2"><p>خود با چه رو به روی نبی دیده واکنند</p></div></div>
<div class="b2" id="bn15"><p>جز نفی حق نخواسته در هر نشست و خاست</p>
<p>دعوی کنند باز که حق درمیان ماست</p></div>