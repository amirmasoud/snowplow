---
title: >-
    بند ۵
---
# بند ۵

<div class="b" id="bn1"><div class="m1"><p>تنها نه خاکیان به تو جیحون گریستند</p></div>
<div class="m2"><p>در ماتم تو جن و ملک خون گریستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکم به سر، برآر سر از خاک و درنگر</p></div>
<div class="m2"><p>تا بر تو آسمان و زمین چون گریستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سیل خون نشست زمین را که عرش و فرش</p></div>
<div class="m2"><p>از حد و نظم و ضابطه بیرون گریستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا از عطش کبود شدت لب فرات و نیل</p></div>
<div class="m2"><p>از رود دیده سیل جگرگون گریستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بر سنان سرت سوی گردون بلند شد</p></div>
<div class="m2"><p>بر فرشیان ملائک گردون گریستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچند خود ز اهل زمین سر زد این عمل</p></div>
<div class="m2"><p>افلاکیان بر اهل زمین خون گریستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک تن ز صد هزار کست جرعه ای نداد</p></div>
<div class="m2"><p>با آنکه برتو آن فرق دون گریستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر تشنگان کشته ی کوی تو کاینات</p></div>
<div class="m2"><p>از زخم کشتگان تو افزون گریستند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد جیب روزگار به خون رشک لاله زار</p></div>
<div class="m2"><p>خلقی ز بس به پهنه و هامون گریستند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>افسردگان بزم عزایت به جای اشک</p></div>
<div class="m2"><p>از آتش درون همه کانون گریستند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عبهر به دشت و لاله به بستان و گل به باغ</p></div>
<div class="m2"><p>از جویبار دیده طبرخون گریستند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد این عزای خاص چنان عام تا به هم</p></div>
<div class="m2"><p>هشیار و مست و عاقل و مجنون گریستند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن روز خون خود به رکاب ار کست نریخت</p></div>
<div class="m2"><p>در ماتم تو عالمی اکنون گریستند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بعد از تو زندگان جهان را کم است باز</p></div>
<div class="m2"><p>صد بحر اگر به طالع وارون گریستند</p></div></div>
<div class="b2" id="bn15"><p>سوزند آفرینش اگر در غمت سزاست</p>
<p>برداغ ابتلای تو این سوختن بجاست</p></div>