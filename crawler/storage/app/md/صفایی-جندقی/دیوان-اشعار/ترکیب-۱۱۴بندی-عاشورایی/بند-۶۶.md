---
title: >-
    بند ۶۶
---
# بند ۶۶

<div class="b" id="bn1"><div class="m1"><p>گفتی که خود نکرد کس آن کشته را کفن</p></div>
<div class="m2"><p>با آنکه بود پیکر او را دو پیرهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بادش ز خاک بادیه پرداخت خلعتی</p></div>
<div class="m2"><p>ز آن پس که گشت کسوت خونش طراز تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد قرن بل فزون نتواند نگاشت نیز</p></div>
<div class="m2"><p>یک نکته زین نوائب جانکاه کلک من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ابنای احمد آن تن رنجور رنج کوب</p></div>
<div class="m2"><p>جمعی دگر زنان پریشان ممتحن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و آن کودکان نورس ناکام خردسال</p></div>
<div class="m2"><p>برجای مانده باز ز دوران پر فتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن رنجه جان به جامعه چون شمس در کسوف</p></div>
<div class="m2"><p>و آنان بتاب نایبه چون موی درشکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرگشتگان چو صید حوادث به صد هراس</p></div>
<div class="m2"><p>پر بستگان چو عقد جواهر به یک رسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بسته را خیال اسیران نه فکر خویش</p></div>
<div class="m2"><p>وین دسته را غم وی و سودای خویشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سازی ز سینه ها به فلک دود شعله تاب</p></div>
<div class="m2"><p>جاری ز دیده ها به مدر سیل خانه کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسبت به خاندان نبی آن فریق را</p></div>
<div class="m2"><p>جز حرف های سخت نرفتی به لب سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفرین و لعن و شنعت و دشنام برزبان</p></div>
<div class="m2"><p>تکفیر و طنز و طعنه و توبیخ در دهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرگام صد قیامت کبری بدو نمود</p></div>
<div class="m2"><p>غوغای خاص و عام و تماشای مرد و زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک تن کجا و کوب دو کشور پر از کلال</p></div>
<div class="m2"><p>یک دل کجا و حمل دو محشر پر از حزن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زار و زبون به محضر بیگانه و آشنا</p></div>
<div class="m2"><p>شمعی چو او نسوخته در هیچ انجمن</p></div></div>
<div class="b2" id="bn15"><p>والی امر سخره ی اشرار ناس آه</p>
<p>حق پایمال ز فرقه ی حق ناشناس آه</p></div>