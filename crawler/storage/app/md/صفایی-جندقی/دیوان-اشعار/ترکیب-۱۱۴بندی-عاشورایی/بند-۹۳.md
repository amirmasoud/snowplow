---
title: >-
    بند ۹۳
---
# بند ۹۳

<div class="b" id="bn1"><div class="m1"><p>صدری که سوده روح به پا صد رهش جبین</p></div>
<div class="m2"><p>در صف به خون و خاک نگون شد ز صدر زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افراخت اختر تعب از خاک تا سماک</p></div>
<div class="m2"><p>انداخت فرش تعزیت از عرش تا زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر پیکری ز رامش و آرام و امن و فرد</p></div>
<div class="m2"><p>هر خاطری به حسرت و تیمار و غم قرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوگ سکون و صبر ز امکان و کون برد</p></div>
<div class="m2"><p>تمکین شد از مکان و تمکن شد از مکین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد جعبه تیر از پی یک سینه درکمان</p></div>
<div class="m2"><p>صد قبضه تیغ در ره یک جثه در کمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نادیده ظلمی از همه کیهان کس اینقدر</p></div>
<div class="m2"><p>نشنیده جوری از همه دوران کس این چنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جویبار شرع قلم شد چه سروها</p></div>
<div class="m2"><p>با تیشه ی تطاول ارباب کفر و کین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتحی مبین شکست نمازادت این مصاف</p></div>
<div class="m2"><p>با آنکه بودت از همه سو بی کس معین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با فرط استغاثه یاریت جز دو دست</p></div>
<div class="m2"><p>یک تن نه از یسار برآمد نه از یمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر اهل بیت از همه حالات غم فزون</p></div>
<div class="m2"><p>حسرت خورم به حالت بدرود واپسین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دور ماجرای تو تا تن فتاده دور</p></div>
<div class="m2"><p>با آه هم نشانم و با اشک هم نشین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قتلم نبود قسمت و مرگم نداد دست</p></div>
<div class="m2"><p>از بی سعادتی نه نصیبم شد آن نه این</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز اسلام ناصبین نبود تا در این عمل</p></div>
<div class="m2"><p>چون بنگری بهر یک ازین تخمه مسلمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سبع المثانیش به لسان لیک عقد قلب</p></div>
<div class="m2"><p>ایاک نعبدش نه و ایاک نستعین</p></div></div>
<div class="b2" id="bn15"><p>اسلام خلق را به ریا خود صلا زدند</p>
<p>وآتش به دودمان رسول خدا زدند</p></div>