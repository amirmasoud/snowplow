---
title: >-
    بند ۷۴
---
# بند ۷۴

<div class="b" id="bn1"><div class="m1"><p>بیع و شرای آب همانا روا نبود</p></div>
<div class="m2"><p>یا مال و جان آل نبی را بها نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک جرعه کس چرا به عوض یا گرو نداد</p></div>
<div class="m2"><p>گر خون و مالشان این هدر آن هبا نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میثاق و مهربانی و مردانگی مگر</p></div>
<div class="m2"><p>در کار بستگان پیمبر سزا نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کربلا چو باطل و حق روبرو شدند</p></div>
<div class="m2"><p>مولا و عبد فارغ از آن ماجرا نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون ریز یک تن از شهدا را که پیش خصم</p></div>
<div class="m2"><p>از هیچ ره مجال عوض یا فدا نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این یک مریض هم نه خود از باب رحم زیست</p></div>
<div class="m2"><p>کس را به او ز فرط غرور اعتنا نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برقتلش اهتمام ندانم چرا نرفت</p></div>
<div class="m2"><p>جز آنقدر که حکم قوی از قضا نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقتول تیغ عمد چو نامد مگر خطا</p></div>
<div class="m2"><p>کو در خور زیاده از آن ابتلا نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفتی ز دست یکسره غیب و شهاده پاک</p></div>
<div class="m2"><p>بعد از پدر اگر پسر آنی به پا نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خصمش نخواست زنده ولی در وجود وی</p></div>
<div class="m2"><p>دیگر برای جور بداندیش جا نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوته شد از قصور بر او دست روزگار</p></div>
<div class="m2"><p>یا دهر را گشایش چندان جفا نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بستند پا که باز کنندش دری ز درد</p></div>
<div class="m2"><p>افتادنش ز ناقه و ماندن بهانه بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی بال کی توان ز قفس به آشیان پرید</p></div>
<div class="m2"><p>حاجت به قید بازو و زنجیر پا نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون یافتی شفا که خود از اشک وخون دل</p></div>
<div class="m2"><p>هر روز و شب جز این دو شرابش دوا نبود</p></div></div>
<div class="b2" id="bn15"><p>عفریت کفر شاد و سلیمان دین غمین</p>
<p>با این مدار اف به فلک وای بر زمین</p></div>