---
title: >-
    بند ۱۱۰
---
# بند ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>گو خصم دل چرا به ستم رام می کنی</p></div>
<div class="m2"><p>هر لحظه اش به یک ستم آرام می کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر چرا شکنجه جاوید این دو روز</p></div>
<div class="m2"><p>از بهر جان خویش سرانجام می کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ قتال قاسم ناشاد می نهی</p></div>
<div class="m2"><p>ساز مصاف اکبر ناکام می کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با مذهبی که کفر تبری کند از آن</p></div>
<div class="m2"><p>خاکت به حلق دعوی اسلام میکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون خدا به خاک خطا ریختی و باز</p></div>
<div class="m2"><p>کورانه ازکتاب وی اعظام می کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی از در صمد که محل وقوف بود</p></div>
<div class="m2"><p>برتافتی و سجده ی اصنام می کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با اهل صدق گر بودت عمر سرمدی</p></div>
<div class="m2"><p>طبعت عداوت است که مادام می کنی</p></div></div>
<div class="b2" id="bn8"><p>روز جزا که قطع شد از هر درت امید</p>
<p>حشر تو با یزید و عذاب تو بایزید</p></div>