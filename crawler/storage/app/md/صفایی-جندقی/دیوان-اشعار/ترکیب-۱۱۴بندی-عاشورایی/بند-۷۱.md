---
title: >-
    بند ۷۱
---
# بند ۷۱

<div class="b" id="bn1"><div class="m1"><p>این کشته سر نهاد به دامان کربلا</p></div>
<div class="m2"><p>کافزود بر حرم شرف و شان کربلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح القدس ز قدر وی ار می نبود کی</p></div>
<div class="m2"><p>سودی جبین به مقدم دربان کربلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر تربتش ملایکه ساجد نبود اگر</p></div>
<div class="m2"><p>او پا نسوده بود به سامان کربلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهد شهادت شهدا شاید ار به عرش</p></div>
<div class="m2"><p>دعوی برتری کند ایوان کربلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تفریق کفر و دین به تبرای قوم کرد</p></div>
<div class="m2"><p>دارای دین مبارز میدان کربلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقور بندگان جسور از چه ره شدی</p></div>
<div class="m2"><p>خود گر نخواستی دل سلطان کربلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معمار کائنات نهاد از نخست عهد</p></div>
<div class="m2"><p>بر سبک سوگ و تعزیه بنیان کربلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی که سرنوشت مکان ها نگاشتند</p></div>
<div class="m2"><p>شد داغ و درد قسمت دیوان کربلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با صد محیط نایبه طوفان نوح نیست</p></div>
<div class="m2"><p>یک نیم قطره در بر طوفان کربلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک باره هر بلا که در امکان دهر بود</p></div>
<div class="m2"><p>آورد سر برون ز گریبان کربلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جاه شرف به چاه تلف نسبیتش نیست</p></div>
<div class="m2"><p>زندان مصر بنگر و زندان کربلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگرفت قالبش به بغل تا به جای قلب</p></div>
<div class="m2"><p>مانند روح تازه نشد جان کربلا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد زلف و خط و خال جوانان چو خاک سود</p></div>
<div class="m2"><p>تعبیر یافت خواب پریشان کربلا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل های لخت لخت و جگرهای ریش ریش</p></div>
<div class="m2"><p>جوشید جای لاله ز بستان کربلا</p></div></div>
<div class="b2" id="bn15"><p>زقوم کفر و زندقه و شاداب و شادخوار</p>
<p>طوبای حق ز تشنگیش خشک برگ و بار</p></div>