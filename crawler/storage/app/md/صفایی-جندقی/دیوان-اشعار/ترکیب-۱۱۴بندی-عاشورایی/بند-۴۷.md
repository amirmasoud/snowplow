---
title: >-
    بند ۴۷
---
# بند ۴۷

<div class="b" id="bn1"><div class="m1"><p>برخیز و وضع حال من از روزگار بین</p></div>
<div class="m2"><p>با صد جهان شکنجه و دردم دچار بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه در زمانه خود امروز مرکزم</p></div>
<div class="m2"><p>بیرون در ز دایره ام حلقه وار بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه از رضا چو تن به زمین با سکون نگر</p></div>
<div class="m2"><p>گه از قضا چو سر به سنان بی قرار بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تابش غم خود و اصحاب و اهل بیت</p></div>
<div class="m2"><p>چون شمع آتشم همه در پود و تار بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عطشان تو جان سپردی و با کام خشک و تر</p></div>
<div class="m2"><p>عذب فرات را به لبم زهرمار بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خور نبود رفع عطش را وگرنه ز اشک</p></div>
<div class="m2"><p>برخیز و جیب و دامن من جویبار بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی چهر و قامت گل و سروم به باغ و جوی</p></div>
<div class="m2"><p>بردل خدنگ بنگر و بردیده خار بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باقی گذاشت کافکندم از در تو دور</p></div>
<div class="m2"><p>بد عهدی زمانه ناسازگار بین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اولاد خویش را که خود اهل مودتند</p></div>
<div class="m2"><p>خورد و درشت و دخت و پسر خوار و زار بین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خال و خط و زلف جوانان ساده روی</p></div>
<div class="m2"><p>دامان دشت ماریه را پر نگار بین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز خون نو خطان خداخوان خاک خفت</p></div>
<div class="m2"><p>چون طرف باغ بادیه را لاله زار بین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این خیل داغ دیده ی هجران کشیده را</p></div>
<div class="m2"><p>سوی مزار مادر خود ره سپار بین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زن ها و خواهران و یتیمان خویش را</p></div>
<div class="m2"><p>چون اشک دل ز دیده روان زین دیار بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از صدمه پیاده روی ها برهنه پای</p></div>
<div class="m2"><p>پای برهنگان یتیمت فگار بین</p></div></div>
<div class="b2" id="bn15"><p>چون قطع جان و دل به رضا زان بدن نکرد</p>
<p>گفت این مقاله دیگر و قطع سخن نکرد</p></div>