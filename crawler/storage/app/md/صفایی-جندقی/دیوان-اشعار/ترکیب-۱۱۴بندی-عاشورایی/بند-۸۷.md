---
title: >-
    بند ۸۷
---
# بند ۸۷

<div class="b" id="bn1"><div class="m1"><p>ای رفته از ازل به مصیبت قضای تو</p></div>
<div class="m2"><p>وضع بلا نشد به جهان جز برای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سخت و سست جمله بلایای انبیاء</p></div>
<div class="m2"><p>کاهی فزون نبود ز کوه بلای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگذاشت در زمانه به جا جز بلا و کرب</p></div>
<div class="m2"><p>تا رستخیز واقعه ی کربلای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الا بهر بلا که ترا بیش و کم رسید</p></div>
<div class="m2"><p>جاری نشد قضای خدا بی رضای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردی فدای دین خدا جان و مال خویش</p></div>
<div class="m2"><p>جاوید انس و جان همه را جان فدای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاشا که کس ز عهده بر آید قصاص را</p></div>
<div class="m2"><p>ز آن در که نیست ملک دو کیهان بهای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از صفحه ی وجوب اگر امکان محو داشت</p></div>
<div class="m2"><p>هر روز تازه تر نشدی ماجرای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نیل غم زدند سراپرده ی سپهر</p></div>
<div class="m2"><p>و افراختند بر سر ماتم سرای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مهر و ماه مشعل و شمع ضیاء و نور</p></div>
<div class="m2"><p>افروختند در خور بزم عزای تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خلق و امر قدر تو گر برتری نداشت</p></div>
<div class="m2"><p>در حکم حق نبود خدا خون بهای تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حق خواست کاین مصایب جانکاه تا ابد</p></div>
<div class="m2"><p>باد ایت از فضایل حیرت فزای تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از ذره ذره ملک نه تنها شنیده اند</p></div>
<div class="m2"><p>از نی نوای نایبه ی نینوای تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با فرط امتداد هنوز آیدم به گوش</p></div>
<div class="m2"><p>افغان استغاثه و بانگ نوای تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا باشدش به خاک درت فر مسکنت</p></div>
<div class="m2"><p>سلطانی دو کون نخواهد گدای تو</p></div></div>
<div class="b2" id="bn15"><p>بایع خدا متاع بلا مشتری حسین</p>
<p>شد راست زین معامله تا حشر شور و شین</p></div>