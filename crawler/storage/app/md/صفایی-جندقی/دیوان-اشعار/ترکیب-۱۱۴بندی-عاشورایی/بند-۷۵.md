---
title: >-
    بند ۷۵
---
# بند ۷۵

<div class="b" id="bn1"><div class="m1"><p>در شرع رسم رأفت و رحمت بنا نبود</p></div>
<div class="m2"><p>یا بود همان به آل پیمبر روا نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید یکی ز مردم دنیاش بشمرند</p></div>
<div class="m2"><p>گیرم حسین در ره دین پیشوا نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از منبع عطاو نوا منع آب چیست</p></div>
<div class="m2"><p>گیرم حسین مالک منع و عطا نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شد حقوق مذهب و اسلام زادگی</p></div>
<div class="m2"><p>گیرم حسین زاده ی خیر النساء نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عفو ازگناه بی گنهیش از چه ره نکرد</p></div>
<div class="m2"><p>گیرم حسین شافع روز جزا نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلبش ز سر عمامه و تن بی ردا به خاک</p></div>
<div class="m2"><p>گیرم حسین خامس آل عبا نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حب بتول و حرمت قرب رسول کو</p></div>
<div class="m2"><p>گیرم حسین محرم خلوت سرا نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تجویز طنز و طعنه و تهجین بر اوکه راند</p></div>
<div class="m2"><p>گیرم حسین لایق مدح و ثنا نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسناد کفر و جهل و جنون بروی از چه وجه</p></div>
<div class="m2"><p>گیرم حسین قاید راه هدا نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوار اینقدر عزیز خدا در میان خلق</p></div>
<div class="m2"><p>گیرم حسین نزد شما اوصیا نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پامال کردن آن تن بسمل چه وجه داشت</p></div>
<div class="m2"><p>گیرم حسین تاج سر انبیاء نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خاک و خون چرا کفن آراست دشمنش</p></div>
<div class="m2"><p>گیرم حسین کشته ی کوی وفا نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دندان گزای خوک دریغ آهوی حرم</p></div>
<div class="m2"><p>گیرم حسین وارث شیر خدا نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برکشته ای چنین نسزد قطع اشک و آه</p></div>
<div class="m2"><p>گیرم حسین رهن گناهان ما نبود</p></div></div>
<div class="b2" id="bn15"><p>آبی مگر برای خود آری به روی کار</p>
<p>ای دیده در مصیبت این کشته خون ببار</p></div>