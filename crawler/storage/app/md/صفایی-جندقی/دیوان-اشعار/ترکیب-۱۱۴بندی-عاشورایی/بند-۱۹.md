---
title: >-
    بند ۱۹
---
# بند ۱۹

<div class="b" id="bn1"><div class="m1"><p>با گریه گفت کای پدر نامدار من</p></div>
<div class="m2"><p>ای مایه ی قرار دل بی قرار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشین و آن مخواه که زین باد فتنه خیز</p></div>
<div class="m2"><p>برخیزدت ز گوشه ی دامن غبار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک ره به دامنم بنشان تا به صد زبان</p></div>
<div class="m2"><p>بر شاخ گلبن تو بنالد هزار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را نظر همین به تو باشد رضا مشو</p></div>
<div class="m2"><p>کافتد به خاک نخل تو از جویبار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم ز انتظار عزیزان سفید ماند</p></div>
<div class="m2"><p>دیگر تو خود سپاه مکن روزگار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش از شکفتن گل و گلبانگ عندلیب</p></div>
<div class="m2"><p>از تند باد فتنه خزان شد بهار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فصل بهار ز آتش بی آبیم دریغ</p></div>
<div class="m2"><p>پیش از شکوفه سوخت همه شاخسار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب بین کبودم از عطش این بس دگر مخواه</p></div>
<div class="m2"><p>نیلی کند طپانچه اعدا عذار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وا خجلتا ز فاطمه گر بی تو اوفتد</p></div>
<div class="m2"><p>بار دگر به جانب یثرب گذار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر زد به سنگ و خاک به سر ریخت کای پدر</p></div>
<div class="m2"><p>صبر از غم فراق شما نیست کار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برداشت از زمینش و بگرفت در کنار</p></div>
<div class="m2"><p>کی زیب بخش دامن و جیب کنار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داغ برادران وجوانان مرا نسوخت</p></div>
<div class="m2"><p>چندانکه ناله های تو ای دل فگار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صبر از خدای خواه و بپرهیز بیش از این</p></div>
<div class="m2"><p>دامن مزن بر آتش دوزخ شرار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تسلیم امر حق شو و با هجر من بساز</p></div>
<div class="m2"><p>خواهد ترا یتیم چو پروردگار من</p></div></div>
<div class="b2" id="bn15"><p>پس پیش خوانده خواهر برگشته حال خویش</p>
<p>بروی شمرد شمه ای از شرح حال خویش</p></div>