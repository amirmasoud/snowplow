---
title: >-
    بند ۹۸
---
# بند ۹۸

<div class="b" id="bn1"><div class="m1"><p>ناکس نه درک خشم خدا از رضا کند</p></div>
<div class="m2"><p>کی ترک اعتراض به امر قضا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرواز چون تواند ازین دام مرغ دل</p></div>
<div class="m2"><p>الا ز خوف بالی و بالی جدا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجه حسینیش آنکه نه وجهه است در اصول</p></div>
<div class="m2"><p>گر در فروع هم به یزید اقتدا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی سایه آفتاب و سرابش نماید آب</p></div>
<div class="m2"><p>صاحب دلی که فهم اله از هوا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن وفاق و قبح نفاق از هزار سوی</p></div>
<div class="m2"><p>پی برد چون تمیز خلوص از ریا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقیت امام چو بطلان مدعین</p></div>
<div class="m2"><p>دانست آنکه فرق امام از ورا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون حسین هر که نه خون خدای خواند</p></div>
<div class="m2"><p>در امر این قضیه کجا اعتنا کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین بیش اگر چه نسیت جفا ممتنع ولی</p></div>
<div class="m2"><p>دشمن چو خسته شد ستم از کف رها کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اکوان دگر به بنگه امکان برد فرو</p></div>
<div class="m2"><p>زین خون اگر خدای تقاصی بجا کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در معرض سوال و جوابم امیدوار</p></div>
<div class="m2"><p>کز روی فضل پرسشی از حال ما کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز انعام عام بو که خود از یک نگاه خاص</p></div>
<div class="m2"><p>سازد ریا تقدس و رویم طلا کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منع عطا عجب ز کریمی نظیر تست</p></div>
<div class="m2"><p>گر خاک را به فر نظر کیمیا کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تاج شرف به تارک فخرش فرانهی</p></div>
<div class="m2"><p>چون سایه هر که زیر لوای تو جا کند</p></div></div>
<div class="b2" id="bn14"><p>گر ره به پای بوس سگانت دهی مرا</p>
<p>دیهیم دولتی است که بر سر نهی مرا</p></div>