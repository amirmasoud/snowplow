---
title: >-
    بند ۳۲
---
# بند ۳۲

<div class="b" id="bn1"><div class="m1"><p>اکنون سپهر خاک سیه ریخت بر سرم</p></div>
<div class="m2"><p>کافکند برزمین ز سر آن طرفه افسرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس مرا وسیله ی حیرت شد این حدیث</p></div>
<div class="m2"><p>گویی هنوز نامده قتل تو باورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آنکه پیش چشم من افتاده ای به خاک</p></div>
<div class="m2"><p>باز این صور به مد نظر در نیاورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن برزمین طپان و سرت برسنان ولی</p></div>
<div class="m2"><p>حاشا که من گمان چنین حال ها برم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از برج بخت صد فلکم ماه تیره رست</p></div>
<div class="m2"><p>تا رخ نهفتی از نظر ای مهر انورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هفتاد چرخه کوکب نحسم گشود روی</p></div>
<div class="m2"><p>تا شد فروز برج شرف سعد اکبرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود میر مجلسم و عجب کز نخست دور</p></div>
<div class="m2"><p>جز خون نریخت ساقی دوران به ساغرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتاق مرگ خویش چو عطشان به آب ساخت</p></div>
<div class="m2"><p>ذل اسیری خود و ذبح برادرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بودی به دست آنقدرم کاش مهلتی</p></div>
<div class="m2"><p>تا جان چو سر به پای تو یکباره بسپرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مقهور خصم و نیست پناهم به هیچ سو</p></div>
<div class="m2"><p>جز راه مرگ چاره نمانده است دیگرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما ره سپار شام و تو قاصر ز همرهی</p></div>
<div class="m2"><p>این خطره ها خطور نکردی به خاطرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داغی که شرح آن نتوانم به قرن ها</p></div>
<div class="m2"><p>بر دل نهاد واقعه ی عون و جعفرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا رستخیر هردو جهانم زیاد برم</p></div>
<div class="m2"><p>هنگامه ی شهادت عباس و اکبرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جد و پدر به خلد و تو غلطان به خون و خاک</p></div>
<div class="m2"><p>داد از غرور خصم جفا جو کجا برم</p></div></div>
<div class="b2" id="bn15"><p>بنشست باز در بر آن جسم چاک چاک</p>
<p>رخ کند و گفت وای اخی روحنا فداک</p></div>