---
title: >-
    بند ۱
---
# بند ۱

<div class="b" id="bn1"><div class="m1"><p>ای از ازل به ماتم تو در بسیط خاک</p></div>
<div class="m2"><p>گیسوی شام باز و گریبان صبح چاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذات قدیم بهر عزاداری تو بس</p></div>
<div class="m2"><p>هستی پس از حیات تو یکسر سزد هلاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود نام آسمان و زمین آنچه اندر او</p></div>
<div class="m2"><p>از نامه ی وجود چه باک ار کنند پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا جسم چاک چاک تو عریان به روی دشت</p></div>
<div class="m2"><p>جان جهانیان همه زیبد به زیرخاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارواح شاید ار همه قالب تهی کنند</p></div>
<div class="m2"><p>تا رفت جان پاک تو از جسم تابناک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر و جوان پدید و نهان مرد و زن همه</p></div>
<div class="m2"><p>آن به که بی تو جای گزینند در مغاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تخت زمین به جنبش اگر اوفتد چه بیم</p></div>
<div class="m2"><p>رخش سپهر از حرکت ایستد چه باک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم آه سفلیان به فلک خیزد از زمین</p></div>
<div class="m2"><p>هم اشک علویان به سمک ریزد از سماک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون تو آمده است امان بخش خون خلق</p></div>
<div class="m2"><p>خون را به خون که گفته نشاید نمود پاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن ها مقیم بارگهت قلبنا لدیک</p></div>
<div class="m2"><p>سرها نثار خاک رهت روحنا فداک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برگرگ چرخ و شیر سپهرش غضنفری است</p></div>
<div class="m2"><p>آن گربه را که با سگ کوی تو اشتراک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک سیه به فرق قدح خواره ای که فرق</p></div>
<div class="m2"><p>نشناخت خون پاک تو از شیر دخت تاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آری درند پرده ی شرع رسول خویش</p></div>
<div class="m2"><p>قومی که بیم و باک ندارند از انتهاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاکم به سر ستور به نعش تو تاختند</p></div>
<div class="m2"><p>وآنگاه کشته چو تو آن گونه چاک چاک</p></div></div>
<div class="b2" id="bn15"><p>خوناب دل ز دیده صفایی بیا ببار</p>
<p>شرحی ز سرگذشت شهیدان کن آشکار</p></div>