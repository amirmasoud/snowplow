---
title: >-
    بند ۶۸
---
# بند ۶۸

<div class="b" id="bn1"><div class="m1"><p>بر تشنه کامیش نه همین بحر و بر گداخت</p></div>
<div class="m2"><p>گیتی تمام زیر و زبر خشک و تر گداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنها فرات ز آتش خجلت بر او نسوخت</p></div>
<div class="m2"><p>از شرم وی محیط همی تا شمر گداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریا و رود از دم وی برمدر گریست</p></div>
<div class="m2"><p>صحرا و کوه از غم او برحجر گداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ثابت به جای خود ز سها تا سهیل سوخت</p></div>
<div class="m2"><p>سایر به پای خود ز زحل تا قمر گداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این آتش از چه خاست که تا شعله برکشید</p></div>
<div class="m2"><p>ذرات ملک جمره صفت سر به سر گداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فرقه تا ردا همه را تار و پود سوخت</p></div>
<div class="m2"><p>از صعوه تا هما همه را بال و پر گداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیهان به کید عشق بتان را بهانه کرد</p></div>
<div class="m2"><p>عشاق تن به تن همه را آن شرر گداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تیشه ی تحسر و تأثیر سوگ اوست</p></div>
<div class="m2"><p>فرهاد اگر به سر زد و مجنون اگر گداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اصناف خلق، دیو و پری تا بهیمه سوخت</p></div>
<div class="m2"><p>افراد نوع جن و ملک تا بشر گداخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیوان خود از نبات و نبات از جماد بیش</p></div>
<div class="m2"><p>انسان ز جن و جن ز ملک سخت تر گداخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تنها به خوف خواهر و دختر نخورد خون</p></div>
<div class="m2"><p>هم سوخت بر برادر و هم بر پسر گداخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر خواهران خوار غریبش کمر خمید</p></div>
<div class="m2"><p>بر دختران خرد یتیمش جگر گداخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآن نساء یاره و سنجوق سیم سوخت</p></div>
<div class="m2"><p>برآن بنات خاتم و خلخال زر گداخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن بزم را که سقف و زمین عرش و فرش بود</p></div>
<div class="m2"><p>از اشک و آه زیر فرو شد زبر گداخت</p></div></div>
<div class="b2" id="bn15"><p>از کج مداری فلک این فتنه راست شد</p>
<p>درباره ی حسین، آنچه خواست شد</p></div>