---
title: >-
    بند ۷۸
---
# بند ۷۸

<div class="b" id="bn1"><div class="m1"><p>نفسی که خواند از در حشمت پیمبرش</p></div>
<div class="m2"><p>خون خدا ببین که چها رفت بر سرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تن سرش به نوک سنان رفت در هوا</p></div>
<div class="m2"><p>پس پایمال پهنه ی کین گشت پیکرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خونی که نسبتش به خدا بود ز احترام</p></div>
<div class="m2"><p>آمیخت خصم خیره به خون های دیگرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهی که روز رزم سزاوار شأن اوست</p></div>
<div class="m2"><p>چندین هزار فوج ملک در معسکرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کربلا برابر یک دشت کینه خواه</p></div>
<div class="m2"><p>هفتاد و یک تن از همگان بود لشکرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوری که در لطافتش از تن به تب</p></div>
<div class="m2"><p>عریان در آفتاب تن افتاد بی سرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جسمی که بود خاک رهش بوسه گاه روح</p></div>
<div class="m2"><p>کردند شرحه شرحه به شمشیر و خنجرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمعی که کرد روح قدس اخذ نور از او</p></div>
<div class="m2"><p>افکند دست حادثه در راه صرصرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روشن شد آتشی به بلایای او که سوخت</p></div>
<div class="m2"><p>شش سوی تا نهم فلک از نیم اخگرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرسود خیزران شد و آمود خاک و خون</p></div>
<div class="m2"><p>لعلی که بوسه داد پیمبر مکررش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان بر شهادتش همه تعجیل و حرص بود</p></div>
<div class="m2"><p>دل سوختی ولیک بر احوال خواهرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر طفل شیرخوار شهیدش جگر نسوخت</p></div>
<div class="m2"><p>چندانکه در غم دو یتیم برادرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندیشه ی اسیری فرزند و زن به جان</p></div>
<div class="m2"><p>نگذاشت جای ماتم عباس و اکبرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست از سرش عدوی ستم باره برنداشت</p></div>
<div class="m2"><p>با آنکه استخوان شده با خاک همسرش</p></div></div>
<div class="b2" id="bn15"><p>خونش به خاک معرکه از جسم پاک ریخت</p>
<p>زین آتش آب حق همه الحق به خاک ریخت</p></div>