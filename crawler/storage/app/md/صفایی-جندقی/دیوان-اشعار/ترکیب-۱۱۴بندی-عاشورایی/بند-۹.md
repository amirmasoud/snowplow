---
title: >-
    بند ۹
---
# بند ۹

<div class="b" id="bn1"><div class="m1"><p>شه زاده آن زمان که چو خورشید شد سوار</p></div>
<div class="m2"><p>پیرامنش نوازن زن و فرزند ره سپار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسیمه سر به دامنش آویخت پور و دخت</p></div>
<div class="m2"><p>او بدر و اهل بیت بر اطراف هاله وار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او غرق اشک جاریه چون قطب و اهل بیت</p></div>
<div class="m2"><p>چون پره های چرخ سراسیمه ز اضطرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنان به باد بعد خزان گونه برگ ریز</p></div>
<div class="m2"><p>و او را شکفته رخ به بوی قرب چون بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل حرم چو جمع عزا سر به جیب غم</p></div>
<div class="m2"><p>او در میان چو شمع به رخساره اشک یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او را به یاد وصل چو معشوق دل قوی</p></div>
<div class="m2"><p>و آنان به تاب هجر چو عشاق تن نزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او چهر برفروخته چون گل به شاخ زین</p></div>
<div class="m2"><p>و آنان چون عندلیب خروشان ز هرکنار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دیده موج اشک و به دل کوه های درد</p></div>
<div class="m2"><p>بر سینه خیل داغ و به لب ناله های زار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از فرط بی قراریشان گر کنم حدیث</p></div>
<div class="m2"><p>معنی به لفظ و لفظ نگیرد به لب قرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم چرخ را به یاری اشرار اهتمام</p></div>
<div class="m2"><p>هم خصم را ز خواری اخیار افتخار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غی و غرور باطل و صبر و سکون حق</p></div>
<div class="m2"><p>ماند این دو جاودان ز فریقین یادگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گلبن چو نخله خار برآوردش از خدنگ</p></div>
<div class="m2"><p>برجای گل چرا ندمد خار لاله زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا تلخ شد زبان شکر بارش از عطش</p></div>
<div class="m2"><p>زهر است در مذاق جهان آب خوشگوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این آتش ار به آب رضا می نشد خموش</p></div>
<div class="m2"><p>بی وقفه سوختی همه کیهان به یک شرار</p></div></div>
<div class="b2" id="bn15"><p>بر دورش اهل بیت خروشان کشیده صف</p>
<p>گریان به گرد چشم چو مژگان زهر طرف</p></div>