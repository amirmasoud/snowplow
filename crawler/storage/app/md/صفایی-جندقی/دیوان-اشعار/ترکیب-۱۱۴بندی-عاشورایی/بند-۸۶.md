---
title: >-
    بند ۸۶
---
# بند ۸۶

<div class="b" id="bn1"><div class="m1"><p>از گل تهی فتاد چو گلزار کربلا</p></div>
<div class="m2"><p>سهم جهانیان همه شد خار کربلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمیخت خون پاک وی آنسان به خاک دشت</p></div>
<div class="m2"><p>کانگیخت بوی نافه ز اقطار کربلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل به اشک خون گره خاک می بشوی</p></div>
<div class="m2"><p>کاین گونه گریه نیست سزاوار کربلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فیروزه فام پهنه شد از خون عقیق گون</p></div>
<div class="m2"><p>شنگرف بردمید ز زنگار کربلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وین آب دیده آتش دوزخ خموش کرد</p></div>
<div class="m2"><p>شد موجبات نور جنان نار کربلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان در بهای آب روان می فروختند</p></div>
<div class="m2"><p>کس مشتری نداشت به بازار کربلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینجا به عدل و داد دلش را ندادکس</p></div>
<div class="m2"><p>در رستخیز تا چه شود کار کربلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طومار عمر طی شد و ناگفته این حدیث</p></div>
<div class="m2"><p>کو دهر را تحمل تیمار کربلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با طول روز حشر هم ای دل به هیچ وجه</p></div>
<div class="m2"><p>نبود مجال خواندن طومار کربلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل وارهد ز بار و فتد بازم از فتوح</p></div>
<div class="m2"><p>بندیم اگر به عزم سفر بار کربلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از هر مصیبه دست به دامان صبر زن</p></div>
<div class="m2"><p>پایی اگر مجاور دربار کربلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر بر ندارم از در خاکش مگر به مرگ</p></div>
<div class="m2"><p>ور تیغ بارد از در و دیوار کربلا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با این زبان به طرف دهان چون بیان کنم</p></div>
<div class="m2"><p>الا کم از مصایب بسیار کربلا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا رب خود اعتماد صفایی به فضل تست</p></div>
<div class="m2"><p>محشورش آر در صف زوار کربلا</p></div></div>
<div class="b2" id="bn15"><p>منگر کمال ذلت و نقصان طاعتم</p>
<p>کز شاه کربلاست امید شفاعتم</p></div>