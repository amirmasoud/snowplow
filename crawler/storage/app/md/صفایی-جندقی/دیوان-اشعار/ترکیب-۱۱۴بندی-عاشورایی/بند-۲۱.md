---
title: >-
    بند ۲۱
---
# بند ۲۱

<div class="b" id="bn1"><div class="m1"><p>کای یار بی کسان سخن جان گزا زدی</p></div>
<div class="m2"><p>کآتش به جان دوده ی آل عبا زدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داری سرشکستن دل های ما که تیغ</p></div>
<div class="m2"><p>بستی به قصد دشمن و بر قلب ما زدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغی که بود در خور اعدای سخت جان</p></div>
<div class="m2"><p>از ماجرای خود به دل ما چرا زدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن ضربتی که سینه ی بیگانه را سزاست</p></div>
<div class="m2"><p>بی دست و تیغ بر جگر آشنا زدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش زدی به خلوتیان حریم قدس</p></div>
<div class="m2"><p>بی پرده زین سخن که به ما بر ملا زدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم درگلوی ما همه شد آتشین گره</p></div>
<div class="m2"><p>در پاسخ تو تا دم از این ماجرا زدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن کشیدی از من و کلثوم گوییا</p></div>
<div class="m2"><p>دامن بر آتش دل خیر النساء زدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود را قتیل و اهل حرم را اسیر گیر</p></div>
<div class="m2"><p>پندار خود که بر صف قوم دغا زدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفتی به سوی مقتل وگشتی نگون ز اسب</p></div>
<div class="m2"><p>خفتی به روی خاک و به خون دست و پا زدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایل حجاز ساز حسینی کنند راست</p></div>
<div class="m2"><p>تا با مخالفان عراق این نوا زدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پرده ی جگر همه چون نی نوا زنند</p></div>
<div class="m2"><p>تا تو قدم به ناحیه ی نینوا زدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کیوان فراشت رایت کرببلای ما</p></div>
<div class="m2"><p>تا تو علم به بادیه ی کربلا زدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کردی به دست خویش سر خویش و اقربا</p></div>
<div class="m2"><p>گوی ولا و بر سر کوی بلا زدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در پای دوست دست فشاندی ز ماسوا</p></div>
<div class="m2"><p>در راه قرب بر دو جهان پشت پا زدی</p></div></div>
<div class="b2" id="bn15"><p>این گفت و رفت از خود و افتاد بر زمین</p>
<p>برخاست بار دیگر و با گریه گفت این</p></div>