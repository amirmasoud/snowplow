---
title: >-
    بند ۷۲
---
# بند ۷۲

<div class="b" id="bn1"><div class="m1"><p>نوح غریق یونس عمان کربلا</p></div>
<div class="m2"><p>خضر ذبیح یوسف زندان کربلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقهور دیو کفر سلیمان ملک و دین</p></div>
<div class="m2"><p>سلطان یک سواره ی میدان کربلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میخ خیام تا به زمین استوار کوفت</p></div>
<div class="m2"><p>شد بی قرار چون فلک ارکان کربلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفروخت خون و مال و خرید ابتلا و کرب</p></div>
<div class="m2"><p>چبود جز این متاع به دکان کربلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان های فدای همت آنان که از نخست</p></div>
<div class="m2"><p>دست ولا زدند به دامان کربلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا للعجب که صدر زمان روز رزم دید</p></div>
<div class="m2"><p>هفتاد و یک تن از همه جا سان کربلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با صوت رود رود برآن طفلکان هنوز</p></div>
<div class="m2"><p>خواند مدام مرغ خوش الحان کربلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شط با کمال قرب و وفور از چه رو نکرد</p></div>
<div class="m2"><p>سیراب دم یکی لب عطشان کربلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از داغ لعل لاله رخان سر زند هنوز</p></div>
<div class="m2"><p>گل های آتشین ز گلستان کربلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا سر ز ساق برگ و برش آتش است و دود</p></div>
<div class="m2"><p>نخلی که بردمد ز خیابان کربلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوشی اگر به گوش تو ره دارد از ازل</p></div>
<div class="m2"><p>خواهی شنید تا ابد افغان کربلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نازم به درس عشق که جز ترک جان و سر</p></div>
<div class="m2"><p>حرفی نخواهد کس ز دبستان کربلا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در برکشید جان جهان خواستی چو دل</p></div>
<div class="m2"><p>صورت گرفت عاقبت ارمان کربلا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ریزد ز تاب دل به رخم اشک آتشین</p></div>
<div class="m2"><p>هرشب ز رشک شمع شبستان کربلا</p></div></div>
<div class="b2" id="bn15"><p>دیو زمانه دست به ابن زیاد داد</p>
<p>تا تخت و تاج خاتم جم را به باد داد</p></div>