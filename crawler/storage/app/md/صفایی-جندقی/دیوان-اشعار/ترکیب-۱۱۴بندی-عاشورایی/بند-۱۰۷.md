---
title: >-
    بند ۱۰۷
---
# بند ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>آن ناکسان که پخته ز جان خام میکنند</p></div>
<div class="m2"><p>پیداست ز اول آنچه سرانجام می کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبش نداده تشنه لبش سر بریده باز</p></div>
<div class="m2"><p>یا للعجب که دعوی اسلام می کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عار یهود و ننگ نصاراست دینشان</p></div>
<div class="m2"><p>اسلام را به مغلطه بدنام میکنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک جرعه اش ز صاحب تسنیم شد دریغ</p></div>
<div class="m2"><p>آبی که منقسم به دد و دام می کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سلب حق نباشد اگر سعیشان چرا</p></div>
<div class="m2"><p>در بأس باطل این همه ابرام میکنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بایستشان مکان ملکوت اینک از غرور</p></div>
<div class="m2"><p>خود را به چهل اضل از انعام می کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا پیش اهل ملعنت آیند رو سفید</p></div>
<div class="m2"><p>روز خود از ستیزه شبه فام می کنند</p></div></div>
<div class="b2" id="bn8"><p>واحیرتا که در ره دین لاف مهتری است</p>
<p>آن راکه کفر و شرک از اسلام او بری است</p></div>