---
title: >-
    بند ۶۱
---
# بند ۶۱

<div class="b" id="bn1"><div class="m1"><p>دردا که بعد واقعه ی کربلا هنوز</p></div>
<div class="m2"><p>از کین پر است سینه ی اهل جفا هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این خطا که خواست ندانم برای چیست</p></div>
<div class="m2"><p>تأخیر امر محشر و حکم جزا هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون دو عالم ار همه ریزند در قصاص</p></div>
<div class="m2"><p>این قتل را وفا نکند خون بها هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود گر نبود جان جهان آن جهان جان</p></div>
<div class="m2"><p>پس چیست کز میان نرود این عزا هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر قصه های کهنه و نو قرن ها گذشت</p></div>
<div class="m2"><p>هر روز تازه تر بود این ماجرا هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مشک بوی خون به مشام آمد ای عجب</p></div>
<div class="m2"><p>از خاک و ریگ ناحیه ی نینوا هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گوش هوش سوی حریمش فرا دهی</p></div>
<div class="m2"><p>خواهی شنید صیحه ی وا احمدا هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اعضای وی به تیغ ستم منقطع ز هم</p></div>
<div class="m2"><p>اجزای آسمان و زمین جا به جا هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دشمن به قصد بر تن چاکش ستور تاخت</p></div>
<div class="m2"><p>خاکم به سر نکرده سر از تن جدا هنوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برکشته اش به عمد فرس راند و نرم ساخت</p></div>
<div class="m2"><p>و این را به کیش خویش نخواندی خطا هنوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>معمار عرش و فرش درآمد ز پا دریغ</p></div>
<div class="m2"><p>وین عرش و فرش سایر و ثابت به جا هنوز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرم اسیری حرمش خصم و او زدی</p></div>
<div class="m2"><p>چون مرغ سر بریده به خون دست و پا هنوز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا آسمان کشیده کمان درکمین حق</p></div>
<div class="m2"><p>الا به عمد نامده تیری خطا هنوز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نعش تو پایمال و بنات تو دستگیر</p></div>
<div class="m2"><p>اما بنات نعش فلک خودنما هنوز</p></div></div>
<div class="b2" id="bn15"><p>کوری نگر که روز به خون خدا بلند</p>
<p>دستی که بود شب به خدا در دعا بلند</p></div>