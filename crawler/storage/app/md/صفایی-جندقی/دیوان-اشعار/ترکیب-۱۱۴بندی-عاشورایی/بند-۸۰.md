---
title: >-
    بند ۸۰
---
# بند ۸۰

<div class="b" id="bn1"><div class="m1"><p>گیتی پس از تو دایره اش بی مدار باد</p></div>
<div class="m2"><p>افلاک با درنگ و زمین بی قرار باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تلخ شد زبان به دهان تو از عطش</p></div>
<div class="m2"><p>شهد و شکر به کام جهان ناگوار باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حسرت تو شربت تسنیم و سلسبیل</p></div>
<div class="m2"><p>غلمان و حور را به دهان زهرمار باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وصف تشنه کامیت اندر کنار شط</p></div>
<div class="m2"><p>جاری به دجله خون دل از چشمه سار باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردا چوکربلا به میان پا نهاد گفت</p></div>
<div class="m2"><p>از شرق وغرب امن و امان بر کنار باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا پود و تار جسم تو پامال پهنه گشت</p></div>
<div class="m2"><p>موجود را گسسته ز هم پود و تار باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفع عطش چو از تو نشد جاودان چه سود</p></div>
<div class="m2"><p>کز اشک دیده دامن ما جویبار باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ممنوع از آب مالک آب است وا دریغ</p></div>
<div class="m2"><p>ز آن خانواده دجله و شط شرمسار باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اهل دغا تقاص جفا تا کشند زود</p></div>
<div class="m2"><p>تیغ قصاص حق ز قراب آشکار باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه از قبول داغ تو پهلو کند تهی</p></div>
<div class="m2"><p>جاوید با شکنج دو کیهان دچار باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز اندیشه ی حدیث تو هر دل که وارهید</p></div>
<div class="m2"><p>محصور حکم حادثه روزگار باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر هر تنی که سوگ تو ناسازگار شد</p></div>
<div class="m2"><p>فرسوده ی زمانه ی ناسازگار باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر در غمت ندیده صفایی دوام عیش</p></div>
<div class="m2"><p>مفتون این سراچه ناپایدار باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم شفاعت ار ز تو دارد به دیگری</p></div>
<div class="m2"><p>دور از جوار رحمت پروردگار باد</p></div></div>
<div class="b2" id="bn15"><p>شاها به خویشم از همه کس بی نیاز خوا ه</p>
<p>در حشرم از شفاعت خود سرفراز خواه</p></div>