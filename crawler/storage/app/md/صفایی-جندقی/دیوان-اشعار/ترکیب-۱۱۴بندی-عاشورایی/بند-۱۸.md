---
title: >-
    بند ۱۸
---
# بند ۱۸

<div class="b" id="bn1"><div class="m1"><p>گشتند کشته چون همه انصار اهل بیت</p></div>
<div class="m2"><p>شد نوبت شهادت سالار اهل بیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل پر خروش از غم یاران و لب خموش</p></div>
<div class="m2"><p>آمد به خیمه بر سر بیمار اهل بیت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برداشت سر ز بستر و بگذاشتش به بر</p></div>
<div class="m2"><p>کای بعد من معین و مددکار اهل بیت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدی که زین سموم خزان خیز غیر تو</p></div>
<div class="m2"><p>دیگر گلی نماند به گلزار اهل بیت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خفتند اقربا همه در خون خویشتن</p></div>
<div class="m2"><p>زین پس تویی به محنت و غم یار اهل بیت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکم قضا و امر قدر خوش زدند راه</p></div>
<div class="m2"><p>بر نجم ما و بخت نگون سار اهل بیت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بود سرنوشت که زین سرزمین مرا</p></div>
<div class="m2"><p>افتد به حشر وعده دیدار اهل بیت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتند هم رهان و من اینک روانه ام</p></div>
<div class="m2"><p>ای مونس نهان و پدیدار اهل بیت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حالی که شد صغیر وکبیر سپه هلاک</p></div>
<div class="m2"><p>زیبد به خاک خفته سپهدار اهل بیت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفتم کنون که بود شهادت نصیب ما</p></div>
<div class="m2"><p>پاس خدا نصیر و نگهدار اهل بیت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عرش آن زمان طپید که میگفت و می گریست</p></div>
<div class="m2"><p>بیمار اهل بیت به سردار اهل بیت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من خود هلاک داغ عزیزانم ای پدر</p></div>
<div class="m2"><p>بعد از توکیست محرم و غمخوار اهل بیت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حرمان و حسرتم همه در باب کشتگان</p></div>
<div class="m2"><p>تشویش و حیرتم همه درکار اهل بیت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نالید و سخت وگفت به پاسخ صبور باش</p></div>
<div class="m2"><p>باشد خدا کفیل و پرستار اهل بیت</p></div></div>
<div class="b2" id="bn15"><p>ناگه سکینه آمد و باچشم اشکبار</p>
<p>سد بست پیش پای وی از خیمه سیل وار</p></div>