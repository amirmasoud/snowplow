---
title: >-
    بند ۹۲
---
# بند ۹۲

<div class="b" id="bn1"><div class="m1"><p>ای طایف حریم تو اعیان کاینات</p></div>
<div class="m2"><p>ذرات ملک زایر کوی تو از جهات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گشته زیر خاک و من اسوده بر زمین</p></div>
<div class="m2"><p>ما را هلاک در همه حالت به از حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را ز داغ خود زدی آتش به دلی ولی</p></div>
<div class="m2"><p>تا بخشی از شکنجه جاویدمان نجات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودند سر به تیغ اعادی مجاهدینت</p></div>
<div class="m2"><p>دادند تن به قید اسیری مخدرات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بودند ای عجب همه با این گنه هنوز</p></div>
<div class="m2"><p>دارنده ی صیام و گزارنده ی صلات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با قصد قتل و غارت حق چیست حال کس</p></div>
<div class="m2"><p>صد قرن اگر نماز کند یا دهد زکات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دعوت امام مبین دعوی یزید</p></div>
<div class="m2"><p>فرقان حق کجا و کجا آن مزخرفات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باطل نایستد بر حق نیست مشتبه</p></div>
<div class="m2"><p>آن ترهات سست بدین طرفه محکمات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این صید زار خسته ی بشکسته بال و پر</p></div>
<div class="m2"><p>از آشیان فتاده ی سرگشته در فلات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تر کردی ار کست به یکی جام کام خشک ما</p></div>
<div class="m2"><p>یک غرقه بیش کم نشدی بالله از فرات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد راست دود و خیمه ی گردون سیاه کرد</p></div>
<div class="m2"><p>ز آن آتشی که خصم زدت در سرادقات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشتند دراقامه ی سوگ تو متفق</p></div>
<div class="m2"><p>از صدر بزم صومعه تا صف سومنات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برخاست شور و لوله از کعبه تا کنشت</p></div>
<div class="m2"><p>ارباب کفر و دین همه بنشسته در عزات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم وحش و طیر غم زده در محنت تو محو</p></div>
<div class="m2"><p>هم جن و انس دل شده در ماتم تو مات</p></div></div>
<div class="b2" id="bn15"><p>درباره ی تو هر که کند بیش و کم ستم</p>
<p>کیفر ز هفت دوزخش افزون بود نه کم</p></div>