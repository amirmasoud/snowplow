---
title: >-
    بند ۶۵
---
# بند ۶۵

<div class="b" id="bn1"><div class="m1"><p>اقسام ظلم ها که در امکان خلق بود</p></div>
<div class="m2"><p>امت به خاندان نبودت قضا نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین مردم آنچه رفت بر احفاد مصطفی</p></div>
<div class="m2"><p>با وصف کفر سر نزد از عاد یا ثمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مسلمین ناصبی این اهتمام رفت</p></div>
<div class="m2"><p>بر نفی وی نخاست نصارا و نه یهود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آبش نداد جز دم خنجر که دیده آه</p></div>
<div class="m2"><p>خیلی چنان بخیل و عداتی چنان عنود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شامی نکرده شرم دل از دشنه اش شکافت</p></div>
<div class="m2"><p>کوفی نبرده رحم رگ از خنجرش گشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم سنگ خاک شد تنش از پایمال اسب</p></div>
<div class="m2"><p>آن کآسمان به خاک جنابش برد سجود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عریان فتاد پیکر بی سر در آفتاب</p></div>
<div class="m2"><p>آن را که سایبان خم گیسوی حور بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خاک پهنه زرد شد از خون جبهه سرخ</p></div>
<div class="m2"><p>گلنار لب کش آمده بود از عطش کبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تیغ و نی درید چه دل های حق نگر</p></div>
<div class="m2"><p>در خاک و خون طپید چه رخ های حق نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرسوده شد به تیغ چه لب های لعل تاب</p></div>
<div class="m2"><p>آلوده شد ز خاک چه خطهای مشک سود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حق در زمان اگر کند این قتل را قصاص</p></div>
<div class="m2"><p>تا حشر خون رود به زمین صد چو زنده رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باید عذاب نامتناهی جزای قوم</p></div>
<div class="m2"><p>دور است این معامله از عرصه ی حدود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین غم شکیب ماتمیان لحظه لحظه کاست</p></div>
<div class="m2"><p>چندانکه ماتمش غم ما دمبدم فزود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا جای بر غمش نشود تنگ جاودان</p></div>
<div class="m2"><p>آثار شادی از دو جهان ماتمش زدود</p></div></div>
<div class="b2" id="bn15"><p>کورانه هر که دعوی پرهیز می کند</p>
<p>خنجر به قصد خون خدا تیز می کند</p></div>