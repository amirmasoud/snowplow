---
title: >-
    بند ۱۴
---
# بند ۱۴

<div class="b" id="bn1"><div class="m1"><p>تخم جفا به خاک شقا ریخت تا یزید</p></div>
<div class="m2"><p>کس حاصلی به جز غم از آن زرع ندروید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت خدای را که خود از ریشه ای که کاشت</p></div>
<div class="m2"><p>جز بار لعن و خار ملامت گلی نچید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا رفته وصف ظلمت و نور این جفا نرفت</p></div>
<div class="m2"><p>برهیچ آفریده شقی بوده یا سعید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحم از زمانه شست عدو ورنه اهل بیت</p></div>
<div class="m2"><p>نزدیک و دور ویله ی وا العطش شنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آن فرقه یک تنش همه فریاد رس نخواست</p></div>
<div class="m2"><p>ورنه به گوش ها همه فریاد او رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنها نه راست قامت مه زین عزا خم است</p></div>
<div class="m2"><p>برداغ این ستم زده نه آسمان خمید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کش دو کون قیمت یک مشت خاک پای</p></div>
<div class="m2"><p>از پا به سر درآمد و در خاک و خون طپید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد از صدور این ستم از شرم انبیا</p></div>
<div class="m2"><p>روح القدس به بنگه غم انزوا گزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیبق به گوش ریخته بود آن فریق را</p></div>
<div class="m2"><p>ز آنان تنیش ورنه به فریاد می رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با آن خروش و شورش و آشوب و انقلاب</p></div>
<div class="m2"><p>عالم چه شد که باز برین وضع آرمید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از داغ چهر و زلف جوانان نسوخت باز</p></div>
<div class="m2"><p>دیگر ز باغ سوری و سنبل چرا دمید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا در میان جان من این غم گرفت جای</p></div>
<div class="m2"><p>شادی ز دل به گوشه ی افسردگی خزید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ضنت مکن ز گریه صفایی درین عزا</p></div>
<div class="m2"><p>کز دیده بایدت عوض اشک خون چکید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بفروش قلب غفلت و نقد سرشک خر</p></div>
<div class="m2"><p>کز نیم قطره دولت باقی توان خرید</p></div></div>
<div class="b2" id="bn15"><p>دشمن ستیزه کرد و به رویش کشید تیغ</p>
<p>تیغ از بریدن آه که سروانزد دریغ</p></div>