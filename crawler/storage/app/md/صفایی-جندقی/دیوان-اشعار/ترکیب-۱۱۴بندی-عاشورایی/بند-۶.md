---
title: >-
    بند ۶
---
# بند ۶

<div class="b" id="bn1"><div class="m1"><p>برعون باطل آه که ابنای روزگار</p></div>
<div class="m2"><p>در نفی و سلب حق همه جویند اعتبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کربلا ز کوفه به خونریز یک بدن</p></div>
<div class="m2"><p>پر تا به پر پیاده و سر تا به سر سوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دعوی خدای پرستی خدای سوز</p></div>
<div class="m2"><p>از التزام ظلم به رحمت امیدوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذکر رسول بر لب و بغض ولی به دل</p></div>
<div class="m2"><p>در چشم ها کتاب عزیز اهل بیت خوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هیچ امتی عملی سرنزد چنین</p></div>
<div class="m2"><p>ای شرک و کفر را خود از این کیش و ننگ عار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا راز درم و رسم جدل در جهان که دید</p></div>
<div class="m2"><p>آید برون برابر یک مرد صد هزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می بین ستیز باطل و بنگر سکون حق</p></div>
<div class="m2"><p>این صبر و این ستم به جهان ماند یادگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شد که عدل حق نکشید انتقام ظلم</p></div>
<div class="m2"><p>ز آن قوم کفر کیش خطاکوش نابکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان پلید کاش تنی زان شرار قوم</p></div>
<div class="m2"><p>بیرون نبردی از دم شمشیر آبدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تاب تشنه کامی او جاودان کم است</p></div>
<div class="m2"><p>جوشد به جای آب اگر خون ز چشمه سار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین غم مگر شکسته سراپای آب نهر</p></div>
<div class="m2"><p>بس تن برهنه سرزده برسنگ آبشار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سبطیان تشنه لبت ای فرات شرم</p></div>
<div class="m2"><p>تا کی به کام قبطی و این گونه سازگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاش ای سحر شبت نشود روز هان مخند</p></div>
<div class="m2"><p>شرمی بدار باری از آن چشم اشکبار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دیده ی تر و لب خشکت نصیب من</p></div>
<div class="m2"><p>اشک زمین گذر شد و آه فلک گذار</p></div></div>
<div class="b2" id="bn15"><p>ناحق به خاک با بدن چاک چاک خفت</p>
<p>الحق که حق ز فرقه ی ناحق به خاک خفت</p></div>