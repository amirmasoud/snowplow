---
title: >-
    بند ۹۰
---
# بند ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای صحن بارگاه توام روضه ی نعیم</p></div>
<div class="m2"><p>دور از درت مراست جهان حفره ی جحیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تختگاه چرخ زمینش بود مقام</p></div>
<div class="m2"><p>آن سالکی که بر سر قبر تو شد مقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاشا که حالت تو فرامش فتد مرا</p></div>
<div class="m2"><p>آندم که ناگزر دل خود ساختی دو نیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خالی ز هر علاقه و مملو ز هر ملال</p></div>
<div class="m2"><p>نیمی سوی حرامی و نیمی سوی حریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بی گزاف شادی خود در غم تو یافت</p></div>
<div class="m2"><p>جان را ز فیض تو رزقی بود کریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتت چو ز اعتساف عدو و انقلاب عصر</p></div>
<div class="m2"><p>حسرت کشیده بیوه زنان کودکان یتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیر فلک خرف شده کاش ایستد عنن</p></div>
<div class="m2"><p>زال زمین فلک زده کاش اوفتد عقیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا شد به پهنه پیکر پاک تو پایمال</p></div>
<div class="m2"><p>بایست عرش و فرش سرا پا شدی رمیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خاک بربدن کفن آراستت صبا</p></div>
<div class="m2"><p>بادش نوید گر تو جزایی برد جسیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر جا ز شرح بزم تو سوگی کنند طرح</p></div>
<div class="m2"><p>خوناب دل به رخ رود از دیده ی ندیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باطل به ظلم حق کند افشای راز دل</p></div>
<div class="m2"><p>وین نیست طرح تازه که رسمی بود قدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشمم به بخشش تو و خوفم ز خشم تست</p></div>
<div class="m2"><p>شادم که نیست از دگرانم امید و بیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواهی اگر سلامت دنیا وآخرت</p></div>
<div class="m2"><p>قلبی بجو صفایی از الطاف حق سلیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بغض عدو چو حب ولی فرض می شمار</p></div>
<div class="m2"><p>زین سوی زن قدم که صراطی است مستقیم</p></div></div>
<div class="b2" id="bn15"><p>نبود عجب که عذر گناهان پذیردم</p>
<p>در هر سه جا شفاعت او دستگیردم</p></div>