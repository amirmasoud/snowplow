---
title: >-
    بند ۸۸
---
# بند ۸۸

<div class="b" id="bn1"><div class="m1"><p>اقطار اشک می دمد از تاک کربلا</p></div>
<div class="m2"><p>جای عنب زهی عجب از خاک کربلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گردش سپهر سها تا سهیل ریخت</p></div>
<div class="m2"><p>در خاک و خون کواکب افلاک کربلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظلمی که برنژاد علی ز آل حرب رفت</p></div>
<div class="m2"><p>کی درک گنه آن کند ادراک کربلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر نسخه ی نشاط دو کیهان قلم کشید</p></div>
<div class="m2"><p>یک حرف از حدیث تعب ناک کربلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلزار دین ز تاب عطش خشک و تر دریغ</p></div>
<div class="m2"><p>از خون نوخطان خس و خاشاک کربلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هفتاد روضه لاله و گل داد بی دریغ</p></div>
<div class="m2"><p>بر باد فتنه صرصر هتاک کربلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه ابطال را گذاشت نه ز اطفال درگذشت</p></div>
<div class="m2"><p>جلاد رحم خواره ی بی باک کربلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بذل آب و ریزش خون های محترم</p></div>
<div class="m2"><p>اسراف صرف بنگر و امساک کربلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شهد شوق کوی شهادت بیا که برد</p></div>
<div class="m2"><p>شیرینی تو تلخی تریاک کربلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جیحون و نیل و دجله کند کی برابری</p></div>
<div class="m2"><p>با بحر اشک دیده ی نمناک کربلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در محشرش سمند سعادت کنند زین</p></div>
<div class="m2"><p>سر هر که ساخت زینت فتراک کربلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بطحا و یثرب و نجف و کوفه خود مگر</p></div>
<div class="m2"><p>شفع گنه کنند بر اشراک کربلا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزی که میر کعبه کشد ز انتقام کین</p></div>
<div class="m2"><p>از خیره خصم خونی صفاک کربلا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شایدکه دست عدل و عطا مرهمی نهند</p></div>
<div class="m2"><p>بر زخم های سینه صد چاک کربلا</p></div></div>
<div class="b2" id="bn15"><p>ترسم که کربلا چو به محشر قدم زند</p>
<p>نگشوده لب صفوف قیامت بهم زند</p></div>