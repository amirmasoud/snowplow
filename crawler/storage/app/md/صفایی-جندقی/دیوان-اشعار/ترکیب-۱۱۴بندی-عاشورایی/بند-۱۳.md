---
title: >-
    بند ۱۳
---
# بند ۱۳

<div class="b" id="bn1"><div class="m1"><p>برحالت غریبی او آسمان گریست</p></div>
<div class="m2"><p>تنها نه آسمان همه کون و مکان گریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سوی مقتل آمد و برکشتگان گذشت</p></div>
<div class="m2"><p>بر هر جوان و پیر خروشان چنان گریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کزتاب شرم در رخ او ماسوا گداخت</p></div>
<div class="m2"><p>و ز باب رحم بردل او کن فکان گریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم بر رجال کشته بی کفن و دفن سوخت</p></div>
<div class="m2"><p>هم برنساء زنده ی بیخانمان گریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ناله اش خروش به خلد برین فتاد</p></div>
<div class="m2"><p>وز گریه اش بتول به باغ جنان گریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برسینه و لبش همه صحرا و باغ سوخت</p></div>
<div class="m2"><p>بر دیده و دلش همه دریا و کان گریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل ها به خاک ریخت چو گلشن به باد رفت</p></div>
<div class="m2"><p>بلبل به حسرت آمد و بر باغبان گریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل هرچه داشت خون جگر سوی دیده راند</p></div>
<div class="m2"><p>چشم از پی نثار رهش ناتوان گریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون انس جان برید ز جان جان انس و جان</p></div>
<div class="m2"><p>تن انس جان گذارده بر انس و جان گریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب تشنه نحر شد لب نهر فرات آه</p></div>
<div class="m2"><p>با آنکه نهر در غمش آب روان گریست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا پیکر امام زمان برزمین فتاد</p></div>
<div class="m2"><p>روح الامین به حال زمین و زمان گریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جسم جهان فتاد تهی ز آن جهان جان</p></div>
<div class="m2"><p>جان جهانیان به عزای جهان گیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بریک جوان و پیر وی ابقا نکرد خصم</p></div>
<div class="m2"><p>هرچند زان سپه همه پیر و جوان گریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>براین غریب دشت بلا نفس و عقل سوخت</p></div>
<div class="m2"><p>براین قتیل تیغ جفا جسم و جان گریست</p></div></div>
<div class="b2" id="bn15"><p>جانم به تاب ز انده بی تابیت حسین</p>
<p>خاکم به باد ز آتش بی آبیت حسین</p></div>