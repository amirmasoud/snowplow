---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>واعظ به خدا که رفته از کف دل من</p></div>
<div class="m2"><p>وز پند تو آسان نشود مشکل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این موعظه ها به گوش آن باید راند</p></div>
<div class="m2"><p>کآمیخته عشق دلبران در گل من</p></div></div>