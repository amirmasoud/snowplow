---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>چون چشم من از فراق بینی باران</p></div>
<div class="m2"><p>زنهار ملامتم مکن چون یاران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگریه ام ار خنده ات آید نه شگفت</p></div>
<div class="m2"><p>رسم است که باغ بشکفد از باران</p></div></div>