---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دی یارم رفت و شد رقیبش ز قفا</p></div>
<div class="m2"><p>دردا که نداشت آگهی از دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فکرت همراهی آن دیو و پری</p></div>
<div class="m2"><p>مشکل اگر امروز کشم تا فردا</p></div></div>