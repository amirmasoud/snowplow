---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>صد شکر خدا را که وفا دارستی</p></div>
<div class="m2"><p>نه همچو دگر بتان جفا کارستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان جای کند چو دل به زلف تو اگر</p></div>
<div class="m2"><p>از چنگ رقیب جنگجو وارستی</p></div></div>