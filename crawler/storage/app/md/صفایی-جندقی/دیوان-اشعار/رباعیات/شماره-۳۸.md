---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>یک لحظه رقیب برنخیزد ز برت</p></div>
<div class="m2"><p>تا بنشینم به سایه سرو برت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شاخ امید ترسم از خار خسان</p></div>
<div class="m2"><p>زین باغ برون رویم ناخورده برت</p></div></div>