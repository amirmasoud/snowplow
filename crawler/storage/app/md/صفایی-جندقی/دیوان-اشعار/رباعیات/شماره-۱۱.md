---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>فریاد ز ضرب دست آن چشم سیاه</p></div>
<div class="m2"><p>کافکند مرا به خاک ناکرده گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صد نگهم نراند کس زخمی و تو</p></div>
<div class="m2"><p>صد زخم زدی بر دلم از نیم نگاه</p></div></div>