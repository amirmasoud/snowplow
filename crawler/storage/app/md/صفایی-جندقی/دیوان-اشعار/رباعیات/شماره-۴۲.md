---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>برجور رقیب کش خوشی کاوش ماست</p></div>
<div class="m2"><p>خرسندم اگر چه دورم از بزم تو خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز قرب ورا پایه ی عزت نفزود</p></div>
<div class="m2"><p>وز بعد مرا مایه خواری ها کاست</p></div></div>