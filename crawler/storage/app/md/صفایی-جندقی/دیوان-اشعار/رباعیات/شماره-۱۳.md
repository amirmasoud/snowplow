---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>گفتی مکن اینقدر صفائی زاری</p></div>
<div class="m2"><p>باری چه رسیدت که چنین می باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پائی به سرم سپار و دستی به دلم</p></div>
<div class="m2"><p>تا برمنت از دیده شود خون جاری</p></div></div>