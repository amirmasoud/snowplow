---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>برآتش غم جگر کباب است مرا</p></div>
<div class="m2"><p>از دود درون دیده پر آب است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجران قوی پنجه و اعضای ضعیف</p></div>
<div class="m2"><p>افسانه ی صعوه و عقاب است مرا</p></div></div>