---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>مستان همه کام یاب از دختر رز</p></div>
<div class="m2"><p>سرخوش همه شیخ و شاب از دختر رز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آباد بنای طرب از دولت جام</p></div>
<div class="m2"><p>بنیاد ورع خراب از دختر رز</p></div></div>