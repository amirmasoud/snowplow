---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>از لعل تو خون جگرم ماند به دل</p></div>
<div class="m2"><p>وز دست رقیب پای امید به گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهی گریم از تو و گه نالم از او</p></div>
<div class="m2"><p>تا آه و سرشک را چه باشد حاصل</p></div></div>