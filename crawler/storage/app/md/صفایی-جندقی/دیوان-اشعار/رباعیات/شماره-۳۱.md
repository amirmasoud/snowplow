---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ای دوست ز دست تو به دردم شب و روز</p></div>
<div class="m2"><p>با هجر قرین ز وصل فردم شب و روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این گشت رقیب وآن دهد پند مرا</p></div>
<div class="m2"><p>با دشمن و دوست در نبردم شب و روز</p></div></div>