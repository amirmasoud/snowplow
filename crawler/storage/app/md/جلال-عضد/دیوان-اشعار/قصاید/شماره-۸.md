---
title: >-
    شمارهٔ  ۸
---
# شمارهٔ  ۸

<div class="b" id="bn1"><div class="m1"><p>به صحن گلشن گیتی ز اعتدال بهار</p></div>
<div class="m2"><p>صبا بساط زمرّد فکند دیگر بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عاشقان گل و سنبل همی دهند نشان</p></div>
<div class="m2"><p>ز رنگ و چهره معشوق و بوی طرّه یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بساط سبزه نمودار چرخ وانجم شد</p></div>
<div class="m2"><p>ز بس گهر که ببارید ابر گوهربار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عروس غنچه سوی حجله می رود گویی</p></div>
<div class="m2"><p>که فرش جمله حریر است و راه جمله نثار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خاک تیره هوا را به دل غباری بود</p></div>
<div class="m2"><p>درآمد ابر و به کلّی فرونشاند غبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپیده دم گذری کن به باغ تا بینی</p></div>
<div class="m2"><p>ز رنگ و بوی ریاحین طراوت گلزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه غالیه سوده ست خاکدان چمن</p></div>
<div class="m2"><p>چراست غالیه بو هر گلی که آرد بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شکل لاله نظر کن که گویی افکندند</p></div>
<div class="m2"><p>درون حلقه لعل بدخش مشک تتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فروغ چهره گل بین و مهد زنگاری</p></div>
<div class="m2"><p>چو آفتاب برین چرخ لاجوردی کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شده ست تازه مگر خون میان لاله و گل</p></div>
<div class="m2"><p>که هست آب زره پوش و بید خنجردار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنند شام و سحر همچو عاشق و معشوق</p></div>
<div class="m2"><p>شکوفه خنده شیرین و ابر گریه زار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدین صفت که جهان سبز گشت و خرّم شد</p></div>
<div class="m2"><p>نه از نسیم شمال است و اعتدال بهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دور تربیت عدل شاه ملک آرای</p></div>
<div class="m2"><p>به چارسوی جهان سبز شد در و دیوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جمال چهره اقبال شیخ ابواسحق</p></div>
<div class="m2"><p>که آستان در اوست قبله احرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدا یگان فلک حشمت ستاره حشم</p></div>
<div class="m2"><p>جهان پناه ممالک ستان گیتی دار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قضا نفاذ و قدر قدرت و فلک شوکت</p></div>
<div class="m2"><p>زمانه حکم و زمین حلم و آسمان مقدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سماک رمح و سها ناوک و هلال کمان</p></div>
<div class="m2"><p>زحل مکان و قمر عزم و مشتری دیدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ستاره شرف و کان جود و بحر سخا</p></div>
<div class="m2"><p>سپهر لطف و جهان وفا و کوه وقار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بشسته چشمه تیغش سپیدی از رخ روز</p></div>
<div class="m2"><p>زدوده خنجر تیزش سیاهی از شب تار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زهی نعوت جلالت برون ز حدّ بیان</p></div>
<div class="m2"><p>خهی صفات کمالت فزون ز حدّ شمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز خطبه تو بلند است پایه منبر</p></div>
<div class="m2"><p>ز کنیت تو درست است سکه دینار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مثال کلک تو جاری ست بر سیاه و سپید</p></div>
<div class="m2"><p>ز شرق و غرب جهان تا به روم و هندوبار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز رشک سرعت کلک و نفاذ منشورت</p></div>
<div class="m2"><p>همی بپیچد بر خود سپهر چون طومار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به نزد همّت تو نه فلک چه وقع آرد</p></div>
<div class="m2"><p>بلی حباب چه وزن آورد به جنب بحار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز حکم و حلم تو پیدا شد آسمان و زمین</p></div>
<div class="m2"><p>بدان دلیل که آن ثابت است و این سیّار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قضا که هیچ کس آگه نشد ز اسرارش</p></div>
<div class="m2"><p>نهاده است کنون با تو در میان اسرار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زفان فتنه ز قدر تو هست یک نقطه</p></div>
<div class="m2"><p>محیط دایره این هفت قبّه دوّار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منم که مادح این حضرتم به شام و سحر</p></div>
<div class="m2"><p>منم که داعی این دولتم به لیل و نهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مراست در دو جهان نیم جانی و آن نیز</p></div>
<div class="m2"><p>به خاک نعل سمند تو کرده ام ایثار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درین جهان به وجود تو دارم آسایش</p></div>
<div class="m2"><p>در آن جهان به تولاّت دارم استظهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو خاطر تو بر اسرار غیب مطّلع است</p></div>
<div class="m2"><p>چه حاجت است که اخلاص خود کنم اظهار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به حضرت تو فلک را مجال قربت نیست</p></div>
<div class="m2"><p>هزار شکر که دارم بر آستان تو بار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا که طوق غلامی ات هست بر گردن</p></div>
<div class="m2"><p>عجب مدار ز من کز سپهر دارم عار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هزار سال اگر شکر نعمتت گویم</p></div>
<div class="m2"><p>هنوز هرچه بگویم یکی بود ز هزار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا که سایه تو بر سر است غم نخورم</p></div>
<div class="m2"><p>که چرخ با من و بخت من کند پیکار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به خواب در نرود چشم بخت من هرگز</p></div>
<div class="m2"><p>که پاسبانی من کرد دولت بیدار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همیشه تا که بود خاک را ثبات و درنگ</p></div>
<div class="m2"><p>همیشه تا که بود چرخ را مسیر و مدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به رکن تخت تو بادا قرار دولت و دین</p></div>
<div class="m2"><p>به گرد چتر تو بادا مدار هفت و چهار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهان به کام و ستاره غلام و دولت رام</p></div>
<div class="m2"><p>فلک مطیع و سعادت قرین و دولت یار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هزار قرن تو سلطان و من کمینه غلام</p></div>
<div class="m2"><p>هزار سال تو مخدوم و بنده خدمتکار</p></div></div>