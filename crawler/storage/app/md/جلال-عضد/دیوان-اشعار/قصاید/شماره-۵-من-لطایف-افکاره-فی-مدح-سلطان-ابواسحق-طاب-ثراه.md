---
title: >-
    شمارهٔ  ۵ -  من لطایف افکاره فی مدح سلطان ابواسحق طاب ثراه
---
# شمارهٔ  ۵ -  من لطایف افکاره فی مدح سلطان ابواسحق طاب ثراه

<div class="b" id="bn1"><div class="m1"><p>پیش ازین کاین چارطاق هفت منظر کرده‌اند</p></div>
<div class="m2"><p>وز فروغ مهر عالم را منوّر کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از آن کانواع موجودات در صبح وجود</p></div>
<div class="m2"><p>سر ز بالین کمین گاه عدم بر کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محضر فرماندهی بر نام خسرو بسته‌اند</p></div>
<div class="m2"><p>پادشاهی جهان بر وی مقرّر کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مالک ملک و جمال دین که او را در ازل</p></div>
<div class="m2"><p>حامی ملک حق و دین پیمبر کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برده‌اند اوّل نموداری ز طاق درگهش</p></div>
<div class="m2"><p>بعد از آن بنیاد این پیروزه منظر کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یافتند از آفتاب خاطر وقّاد او</p></div>
<div class="m2"><p>ذره‌ای و نام او خورشید انور کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده‌اند از بحر دست راد گوهربخش او</p></div>
<div class="m2"><p>قطره‌ای و نام او دریای اخضر کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده‌اند از حلم و لطف و عزم و مهرش بازپس</p></div>
<div class="m2"><p>اقتضای خاک و باد و آب و آذر کرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خطبه اقبال او بر چار عنصر خوانده‌اند</p></div>
<div class="m2"><p>سکه القاب او بر هفت کشور کرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک و دین را در پناه او سکونت داده‌اند</p></div>
<div class="m2"><p>بحر و کان را از نوال او توانگر کرده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ضمیر روشن او جام جم انگیختند</p></div>
<div class="m2"><p>وز ثبات حزم او سدّ سکندر کرده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خسروا! عالم ز بهر جاه تو پرداختند</p></div>
<div class="m2"><p>لیکن اندر خورد جاهت بس محقّر کرده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پایه قدر ترا آنان که گردون خوانده‌اند</p></div>
<div class="m2"><p>آسمان را با زمین گویی برابر کرده‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فی المثل چندان که از چرخ معلّی تا زمین</p></div>
<div class="m2"><p>قبّه قدر ترا از چرخ برتر کرده‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز و شب دایم مه و سال از برای این‌هاست</p></div>
<div class="m2"><p>جامه کافوری و مشکین که در بر کرده‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا برو چوگان تو باشد که آرد سر فرود</p></div>
<div class="m2"><p>هیأت افلاک چون گویی مدوّر کرده‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز پی آیینه زین کمیت توسنت</p></div>
<div class="m2"><p>قرص خور را کن تأمّل تا چه در خور کرده‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ازل لشکرکشانت را به سان لشکری</p></div>
<div class="m2"><p>نام‌ها پیروز و منصور و مظّفر کرده‌اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از نهیب گرز عالم سوز و لطف و قهر تو</p></div>
<div class="m2"><p>دوزخ و نار و بهشت و حوض کوثر کرده‌اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از برای کسب دولت خاصّ و عام از شرق و غرب</p></div>
<div class="m2"><p>همچو اقبال و سعادت رخ بدین در کرده‌اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کامرانی کن که در دیوان فطرت بهر تو</p></div>
<div class="m2"><p>هر مرادی را که می‌خواهی میسّر کرده‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیک‌خواهان ترا افلاک نیکی داده‌اند</p></div>
<div class="m2"><p>بدسگالان ترا انجم بداختر کرده‌اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خسروا! در خدمتت تقصیر کردم عفو کن</p></div>
<div class="m2"><p>مجرمان هم تکیه‌ای بر عفو داور کرده‌اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خسروان ملک و دین شاهان اقلیم کرم</p></div>
<div class="m2"><p>جرم بی‌حد دیده‌اند و عفو بی‌مر کرده‌اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من که دارم حلقه اخلاص تو در گوش جان</p></div>
<div class="m2"><p>پس چرا دایم مرا چون حلقه بر در کرده‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خود چه حدّ من که شاه از چون منی رنجش کند</p></div>
<div class="m2"><p>لیکن از بی‌التفاتی خلق باور کرده‌اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چار عنصر باد در فرمان جاهت تا ابد</p></div>
<div class="m2"><p>گرچه اعدا با خود این هر چار همبر کرده‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آتش اندر دل گرفته مانده اندر دست باد</p></div>
<div class="m2"><p>آب از دیده روان و خاک بر سر کرده‌اند</p></div></div>