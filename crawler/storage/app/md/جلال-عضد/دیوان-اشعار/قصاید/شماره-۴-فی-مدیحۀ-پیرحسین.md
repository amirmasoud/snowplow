---
title: >-
    شمارهٔ  ۴ -  فی مدیحۀ پیرحسین
---
# شمارهٔ  ۴ -  فی مدیحۀ پیرحسین

<div class="b" id="bn1"><div class="m1"><p>روز آنست که عالم طرب از سر گیرد</p></div>
<div class="m2"><p>و آسمان کام دل خود ز زمین برگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رایت فتح سر و قد کشد اندر گردون</p></div>
<div class="m2"><p>گلبن باغ سعادت ز ظفر برگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون عروسان به سر تخت برآید خورشید</p></div>
<div class="m2"><p>در و دیوار جهان در زر و زیور گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه در بزم خدیو آید و مجمر سوزد</p></div>
<div class="m2"><p>زهره در مجلس شاه آید و مزمر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش از ناله نی در جگر خشک افتد</p></div>
<div class="m2"><p>سوز در جان ز سماع غزل تر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنگ را زهره مجلس بزند اوّل بار</p></div>
<div class="m2"><p>باز بنشاند و بنوازد و در برگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ از ناله خلخال و خم چنبر دف</p></div>
<div class="m2"><p>به سرانگشت صدا حلقه چنبر گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفخه هایی که زند شمع معنبر هر دم</p></div>
<div class="m2"><p>همچو فردوس جهان نکهت عنبر گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درِ این بزم اگر باز شود بر رضوان</p></div>
<div class="m2"><p>ترک جنّت کند و حلقه این درگیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخت هر ساعتی از بهر وفاق میمون</p></div>
<div class="m2"><p>بر زبان تهنیت شاه مظفّر گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>داور دور زمان پیرحسین آن که به بزم</p></div>
<div class="m2"><p>آفتابی ست درخشنده چو ساغر گیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به خورشید کنم نسبت او نیست عجب</p></div>
<div class="m2"><p>که چو خورشید همه ملک به خنجر گیرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن سبک دست گران حمله که در روز مصاف</p></div>
<div class="m2"><p>نُه فلک را به سرنیزه ز جا برگیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسروا عقل تأمّل چه کند در ذاتت</p></div>
<div class="m2"><p>یک تنه ذات ترا عالم دیگر گیرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شیر از هیبت تو خدمت روباه کند</p></div>
<div class="m2"><p>مور با عون تو چنگال غضنفر گیرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زعفران را کند اقبال تو ماننده گل</p></div>
<div class="m2"><p>لاله از هیبت تو رنگ معصفر گیرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هر اقلیم که آوازه عدلت برسد</p></div>
<div class="m2"><p>باز را زهره نباشد که کبوتر گیرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جرعه ای جام چو بر خاک سکندر ریزی</p></div>
<div class="m2"><p>آب حیوان مدد از خاک سکندر گیرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تاجداری که نه خاک در تو افسر اوست</p></div>
<div class="m2"><p>زود باشد که به ترک سر و افسر گیرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هفت کشور که از این گونه پرآوازه تست</p></div>
<div class="m2"><p>تو مشو رنجه که خود صیت تو کشور گیرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اختر سعد ز اقبال تو می گیرد فال</p></div>
<div class="m2"><p>گرچه دایم همه کس فال ز اختر گیرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حکم جزم تو هر آنگه که قدم بفشارد</p></div>
<div class="m2"><p>خاک را باد کند آب ز آذر گیرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گوی نُه چرخ فلک را چو یکی بیضه مرغ</p></div>
<div class="m2"><p>طایر قدر تو اندر خم شهپر گیرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من نگویم که خدایی تو ولیکن پیداست</p></div>
<div class="m2"><p>که قضا تخت تو از عرش چه کمتر گیرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندرین عهد اگر زنده شود رستم زال</p></div>
<div class="m2"><p>بر سر از خجلت مردی تو چادر گیرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خسروا بهر نثار تو همی خواست جلال</p></div>
<div class="m2"><p>که شبستان تو اندر زر و گوهر گیرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درّجی از گوهر دریای سخن کرد نثار</p></div>
<div class="m2"><p>طبع او را سزد ار لطف تو در زر گیرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرگز از بهر کسی مدح نگفتم جز تو</p></div>
<div class="m2"><p>خود سخن جز به مدیح تو کجا درگیرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر کسی را دهد این دست که نظمی گوید</p></div>
<div class="m2"><p>معنیی آرد و با لفظ برابر گیرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در ثنای تو کسی نام برآرد که چو من</p></div>
<div class="m2"><p>طرزی از نو بنهد ترک مکرّر گیرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سحرپردازی کلکم چو ببیند دوران</p></div>
<div class="m2"><p>ای بسا نکته که بر خامه آزر گیرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تربیت فرما و آنگه سخنم بنگر از آنک</p></div>
<div class="m2"><p>خاک از تربیتت قیمت جوهر گیرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا صبا چون به چمن بر گذرد فصل بهار</p></div>
<div class="m2"><p>بوستان را همه در حلّه اخضر گیرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همچو دامان بتی در کف دلسوخته ای</p></div>
<div class="m2"><p>لاله برخیزد و دامان صنوبر گیرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حکم تیغ تو چنان باد که تا عهد ابد</p></div>
<div class="m2"><p>سر ز خاقان طلبد باج ز قیصر گیرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ذات تو جوهر محصول جهان باد عرض</p></div>
<div class="m2"><p>تا عرض بهر بقا دامن جوهر گیرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دولت و عمر تو پاینده و باقی بادا</p></div>
<div class="m2"><p>تا بدانگه که جهان دامن محشر گیرد</p></div></div>