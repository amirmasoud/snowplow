---
title: >-
    شمارهٔ  ۹ -  فی مدح السلطان ابواسحق اناراللّه برهانه
---
# شمارهٔ  ۹ -  فی مدح السلطان ابواسحق اناراللّه برهانه

<div class="b" id="bn1"><div class="m1"><p>نسیم غالیه سای است و صبح غالیه بار</p></div>
<div class="m2"><p>کجاست ساقی و کو باده گو بیا و بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بزم دور به گردش در آور آن خورشید</p></div>
<div class="m2"><p>که مقطع ظلمات است و مطلع انوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میی که در شب تاری جهان کند روشن</p></div>
<div class="m2"><p>چنانکه از شکن زلف عکس چهره یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مفتّح در شادی رحیق لعل آسا</p></div>
<div class="m2"><p>مفرّح همه غمها شراب نوش گوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گران معامله شر نهاد شورانگیز</p></div>
<div class="m2"><p>سبک محاوره تلخ خوی شیرین کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو هجر تلخ ولیکن چو وصل جان پرور</p></div>
<div class="m2"><p>چو شوق ذوق فزا و چو عیش عقل شکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میی که وقت صبوحی بود ز روی صفا</p></div>
<div class="m2"><p>چو آفتاب در آن آبگینه دوّار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میی که چون فکند عکس خویش بر گردون</p></div>
<div class="m2"><p>عیان شود همه اطباق چرخ را اسرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میی که باشد و بر کف به یاد مجلس شاه</p></div>
<div class="m2"><p>به رنگ لعل بدخشان و بوی مشک تتار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمال چهره اقبال شیخ ابواسحق</p></div>
<div class="m2"><p>که می کنند سلاطین به بندگیش اقرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپهر رفعت و خورشید رای و انجم خیل</p></div>
<div class="m2"><p>قضا نفاذ و قدر قدرت و جهان مقدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غمام طبع و زمین حلم و آسمان شوکت</p></div>
<div class="m2"><p>زمانه حکم و فلک قدر و آفتاب شعار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو کینه دشمن سوز و چو مهر عالم گیر</p></div>
<div class="m2"><p>چو بخت ملک ستان و چو چرخ گیتی دار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشانه ای ز دل اوست بحر گوهر ریز</p></div>
<div class="m2"><p>نمونه ای ز کف اوست ابر گوهربار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایا سپهر جنابی که هست یک نقطه</p></div>
<div class="m2"><p>به جنب قبّه قدرت سپهر دایره وار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک به مهر تو دارد هزار دل گرمی</p></div>
<div class="m2"><p>جهان به ذات تو دارد هزار استظهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نزد بخشش تو قطره را ز دریا ننگ</p></div>
<div class="m2"><p>به جنب قدر تو خورشید را ز گردون عار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هر دیار که امر تو آبْ آتش فعل</p></div>
<div class="m2"><p>به هر طرف که نفاذ تو خاک باد آثار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تویی گزیده ایّام و حاصل دوران</p></div>
<div class="m2"><p>تویی خلاصه تکوین و زبده اطوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز کنه قدر تو قاصر شده اولوالالباب</p></div>
<div class="m2"><p>چو از اشعه خور دیده اولو الابصار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زهی بساط زمان را به کلک داده نظام</p></div>
<div class="m2"><p>زهی بسیط زمین را به تیغ داده قرار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز طلعت تو جهان فال سعد می گیرد</p></div>
<div class="m2"><p>به جای طالع مسعود و اختر مختار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در آن مقام که لطفت شود مجاهز دهر</p></div>
<div class="m2"><p>دهد طبیعت دی را مزاج فصل بهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپهر تند نیابد جمال صف نعال</p></div>
<div class="m2"><p>در آن زمان که نشینی به صدر صفّه بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رسید پایه قدرت به آن مقام که چرخ</p></div>
<div class="m2"><p>به جنب پایه او چون یکی بود ز هزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به خدمت تو کمر بسته اند خُرد و بزرگ</p></div>
<div class="m2"><p>چنانکه کوه به صحرا و کاه بر دیوار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه غم که چرخ شود پست و عقل کل سرمست</p></div>
<div class="m2"><p>که همّت تو بلند است و رای تو هشیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان جاه تو آن طول و عرض یافته است</p></div>
<div class="m2"><p>که آفرینش ازو قطره ای بود ز بحار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نخست روز قضا حلّ و عقد با تو گشاد</p></div>
<div class="m2"><p>که من ندارم با کار و بار عالم کار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دو خادمند یکی رومی و دگر هندو</p></div>
<div class="m2"><p>ملازم در تو سال و ماه و لیل و نهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نجوم را ز تبدّل تو کرده ای ایمن</p></div>
<div class="m2"><p>سپهر را ز تغیّر تو داده ای زنهار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عجب که خصم تو را در نخواهد افکندن</p></div>
<div class="m2"><p>چنین که خنجر تو تیز می کند بازار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در آشیان جهان طایری ست همّت تو</p></div>
<div class="m2"><p>چو دانه هفت فلک را گرفته در منقار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عروس جاه تو آن دستگاه یافته است</p></div>
<div class="m2"><p>که مهرش آینه است و سپهر آینه دار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا مدیح و ثنای تو گفتن اولیتر</p></div>
<div class="m2"><p>که وصف غمزه غمّاز و طرّه طرّار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قضا نفاذا پیش تو می کنم فریاد</p></div>
<div class="m2"><p>ز دست چرخ جفاکار و دهر مردم خوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زمانه فایض درد است و ناشر اندوه</p></div>
<div class="m2"><p>سپهر معطی رنج است و مولع آز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز خیط اسود و ابیض که آن شب و روز است</p></div>
<div class="m2"><p>ببین به گردن دوران قلاده ادبار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زمانه شعبده باز است و طاس و مهره او</p></div>
<div class="m2"><p>سپهر حقّه وش است و کواکب سیّار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدان قدر که کسی دیده را زند بر هم</p></div>
<div class="m2"><p>هزار شعبده مختلف کند اظهار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به صبح و شام ببین سحر چرخ را ز شفق</p></div>
<div class="m2"><p>که موج می زند از خون دیده احرار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مراست در غم آن روزگار دون پرور</p></div>
<div class="m2"><p>دلی شکسته تنی خسته خاطری افگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هزار بار بود بیم آن به هر روزی</p></div>
<div class="m2"><p>که بندم از ستم چرخ بر میان زنّار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز تنگنای دلم بر فلک رسد هر دم</p></div>
<div class="m2"><p>نوای ناله زیر و فغان و گریه زار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به بوی صبح صفا هر شبی بود نزدیک</p></div>
<div class="m2"><p>که آب دیده بشوید سیاهی از شب تار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپهر می برد از حد چرا تو تن زده ای</p></div>
<div class="m2"><p>به سوی عجز گراید تحمّل بسیار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به آب تیغ فرو شوی از زمانه فتور</p></div>
<div class="m2"><p>به باد گرز برآور ز روزگار دمار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپهر و دور زمان را به جای خود بنشان</p></div>
<div class="m2"><p>ستارگان را هر یک به جای خویش بدار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من و زمانه تو را هر دو بنده ایم اگر</p></div>
<div class="m2"><p>زمانه بر من مسکین جفا کند مگذار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در آفتاب حوادث بسوخت پیکر من</p></div>
<div class="m2"><p>تو آفتابی و از بنده سایه باز مدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همیشه تا که به فصل بهار بر جوشد</p></div>
<div class="m2"><p>چو خون دشمن جاه تو لاله از کهسار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو لطف تو نهد بند رشک بر دل گل</p></div>
<div class="m2"><p>هزار بار نهد بار بر دل اشجار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جهان ز سبزه نماید چنانکه پنداری</p></div>
<div class="m2"><p>که آسمان و زمین حقّه ای ست از زنگار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ببندد ابربسی کلّه های رنگارنگ</p></div>
<div class="m2"><p>که می رسند سوی باغ لعبتان بهار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز عندلیب و چکاوک به گوش جان آید</p></div>
<div class="m2"><p>نوای غلغله چنگ و لحن موسیقار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درون حجله زنگار هر سپیده دمی</p></div>
<div class="m2"><p>عروس گل شود از بانگ بلبلان بیدار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز فیض میغ شود تیغ کوه زنگاری</p></div>
<div class="m2"><p>بسان تیغ که از نم برآورد زنگار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو سرو گلشن بختت همیشه بادا سبز</p></div>
<div class="m2"><p>چو برگ بید حسودت به باد داده قرار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مدام مجلست از خرّمی چو طرف چمن</p></div>
<div class="m2"><p>همیشه بزم تو از رنگ و بوی چون گلزار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عدوّت همچو بنفشه کبود پوشیده</p></div>
<div class="m2"><p>تو همچو نرگس مست و چو لاله باده گسار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به کامرانی و دولت بمان فراوان سال</p></div>
<div class="m2"><p>ز عمر و ملک و جوانی و جاه برخوردار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فلک متابع تو بِالغُدُوِّ وَالا صال</p></div>
<div class="m2"><p>ظفر ملازم تو بِالعَشِی والابکار</p></div></div>