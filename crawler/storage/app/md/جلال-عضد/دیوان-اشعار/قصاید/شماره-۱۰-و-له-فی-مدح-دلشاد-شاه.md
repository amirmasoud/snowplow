---
title: >-
    شمارهٔ  ۱۰ -  و له فی مدح دلشاد شاه
---
# شمارهٔ  ۱۰ -  و له فی مدح دلشاد شاه

<div class="b" id="bn1"><div class="m1"><p>ای ز گل روی تو رونق بستان دل</p></div>
<div class="m2"><p>وی ز هوای رخت تازه گلستان دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ز رخ مهوشت فُسحت بستان جان</p></div>
<div class="m2"><p>وی ز دهان خوشت تنگی میدان دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای زِ خَم زلف تو سلسله در پای جان</p></div>
<div class="m2"><p>وی شکن زلف تو سلسله جنبان دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست شبستان جان طرّه شبرنگ تو</p></div>
<div class="m2"><p>عارض مه پیکرت شمع شبستان دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دل سلطان وشم بنده عشق تو شد</p></div>
<div class="m2"><p>گشته ام از جان کنون بنده سلطان دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای تو زلیخای حسن مصر جهان را عزیز</p></div>
<div class="m2"><p>کرده به چاه زنخ یوسف کنعان دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بت عشوه فروش تا به ابد نشکند</p></div>
<div class="m2"><p>گرمی بازار تو از در دکان دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دل من دست زد در خم گیسوی تو</p></div>
<div class="m2"><p>می نگذارد بلا دست ز دامان دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می گذری ای صبا بر سر بالین دوست</p></div>
<div class="m2"><p>از خم زلفش بپرس حال پریشان دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم از احوال دل هیچ نگویم به کس</p></div>
<div class="m2"><p>وه که به دامن رسید چاک گریبان دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرغ صبا! صبحدم خیز و برو عرضه دار</p></div>
<div class="m2"><p>در بر بلقیسِ عهد حالِ سلیمان دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بانوی جمشید عهد اعظمه دلشاد شاه</p></div>
<div class="m2"><p>آنکه شد از نام او شاد دل و جان دل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه اگر تا ابد کسب صفاتش کند</p></div>
<div class="m2"><p>هیچ نداند هنوز سرّ سخندان دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وآنکه نگنجد به دل حشتمش از کبریا</p></div>
<div class="m2"><p>گرچه نگنجد به وهم حدّ بیابان دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا نگذارد درون آرزوی نفس را</p></div>
<div class="m2"><p>حاجب عصمت نشان بر در ایوان دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در حرم هیچ دل بار هوس ره نیافت</p></div>
<div class="m2"><p>تا به حشر عصمتش گشت نگهبان دل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای قد و خدّ ترا خوانده عروسان خلد</p></div>
<div class="m2"><p>سرو گلستان جان لاله بستان دل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وصف تو گوید همه طوطی خوش نطق جان</p></div>
<div class="m2"><p>مدح تو خواند همه بلبل خوشخوان دل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدمت تو کسوتی ست راست به بالای جان</p></div>
<div class="m2"><p>مدحت تو آیتی ست آمده درشان دل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر نرسیدی به کس نکهتی از خُلق تو</p></div>
<div class="m2"><p>کی بشکفتی ز غم غنچه خندان دل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عرصه ملک جهان هست به کام دلت</p></div>
<div class="m2"><p>گرچه کسی را جهان نیست به فرمان دل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که نخواهد ترا خرّم و خندان و شاد</p></div>
<div class="m2"><p>تا ابدش باد غم ساکن زندان دل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای شه والاگهر بنده کمتر جلال</p></div>
<div class="m2"><p>خون دل آورده است پیش تو بر خوان دل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سیل جفای سپهر کرد دلم را خراب</p></div>
<div class="m2"><p>دل ده و معمور کن خانه ویران دل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دارم از اندوه دهر بی سر و سامان دلی</p></div>
<div class="m2"><p>حال من اندازه کن از سر و سامان دل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو ز سر مرحمت بر دلم آبی بزن</p></div>
<div class="m2"><p>ورنه بسوزد مرا آتش پنهان دل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سهل مدان نظم من زانکه بسش نکته هست</p></div>
<div class="m2"><p>لؤلؤیی از بحر جان گوهری از کان دل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گوهر نظم مرا جوهرییی در خور است</p></div>
<div class="m2"><p>تا که برآرد گهر زین گهرافشان دل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا که به میدان دهر دلشدگان را بود</p></div>
<div class="m2"><p>زلف کمند بتان موضع جولان دل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا متزلزل شود طارم ایوان جان</p></div>
<div class="m2"><p>تا متخلّل شود جمله ارکان دل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باد سر کوی تو منزل و مأوای جان</p></div>
<div class="m2"><p>با خم گیسوی تو بیعت و پیمان دل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا فلک و آفتاب هست چو چوگان و گوی</p></div>
<div class="m2"><p>گوی مراد تو باد در خم چوگان دل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باد دل دشمنت مکمن طوفان غم</p></div>
<div class="m2"><p>هم تن و هم جان او غرقه طوفان دل</p></div></div>