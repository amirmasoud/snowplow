---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>من با تو نیامدم که صحرا بینم</p></div>
<div class="m2"><p>یا بر لب جویی به هوس بنشینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصود من آنست که تو لاله و گل</p></div>
<div class="m2"><p>می چینی و من درد تو بر می چینم</p></div></div>