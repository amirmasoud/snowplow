---
title: >-
    شمارهٔ  ۲۰
---
# شمارهٔ  ۲۰

<div class="b" id="bn1"><div class="m1"><p>آنی که ابد ز عمر تو مایه کند</p></div>
<div class="m2"><p>وز خاک درت سپهر پیرایه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز چتر تو عرش را نباشد این قدر</p></div>
<div class="m2"><p>کاو بر سر سایۀ خدا سایه کند</p></div></div>