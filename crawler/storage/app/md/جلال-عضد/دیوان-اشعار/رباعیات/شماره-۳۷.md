---
title: >-
    شمارهٔ  ۳۷
---
# شمارهٔ  ۳۷

<div class="b" id="bn1"><div class="m1"><p>گویند که جمشید به کف داشت مدام</p></div>
<div class="m2"><p>جامی که جهان نمای بود آن را نام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پارس که تخت گاه جمشید آمد</p></div>
<div class="m2"><p>امروز تو جمشیدی و بر دست تو جام</p></div></div>