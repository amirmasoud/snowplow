---
title: >-
    شمارهٔ  ۳۶
---
# شمارهٔ  ۳۶

<div class="b" id="bn1"><div class="m1"><p>تا چرخْ صفت تو راست خندان لب لعل</p></div>
<div class="m2"><p>جز باده نگشت همدم آنِ لب لعل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لطف لب خوشت نماید دندان</p></div>
<div class="m2"><p>گوهر بگزد بسی به دندان لب لعل</p></div></div>