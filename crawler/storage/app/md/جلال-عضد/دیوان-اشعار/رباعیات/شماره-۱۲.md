---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>افسوس که آن یار پسندیده برفت</p></div>
<div class="m2"><p>ناکرده مرا وداع و نادیده برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم همه پیش چشم من تاریک است</p></div>
<div class="m2"><p>تا روشنی دیده ام از دیده برفت</p></div></div>