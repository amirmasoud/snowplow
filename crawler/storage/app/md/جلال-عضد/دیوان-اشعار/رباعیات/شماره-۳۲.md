---
title: >-
    شمارهٔ  ۳۲
---
# شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>شد دهر خرف جوان نگیرد عیبش</p></div>
<div class="m2"><p>کز عطر عروسانه بر آمد جیبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد از دو سه طهر ریاضت بستان</p></div>
<div class="m2"><p>هر لحظه گلی می شکفد از عینش</p></div></div>