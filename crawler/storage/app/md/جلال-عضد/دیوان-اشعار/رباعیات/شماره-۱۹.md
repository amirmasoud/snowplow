---
title: >-
    شمارهٔ  ۱۹
---
# شمارهٔ  ۱۹

<div class="b" id="bn1"><div class="m1"><p>تا نادرۀ حسن رخت پیدا شد</p></div>
<div class="m2"><p>گردون به نظارۀ رخت برپا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با روی تو صبح لاف خوبی می زد</p></div>
<div class="m2"><p>در چشم جهانیان از آن رسوا شد</p></div></div>