---
title: >-
    شمارهٔ  ۳۸
---
# شمارهٔ  ۳۸

<div class="b" id="bn1"><div class="m1"><p>دیشب که مراجعت فتاد از سفرم</p></div>
<div class="m2"><p>دلدار به تهنیت درآمد ز درم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرفت در آغوشم و بوسید سرم</p></div>
<div class="m2"><p>از لعل و بلور کرد تاج و کمرم</p></div></div>