---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>چون مهر پری ز خاتم جم برداشت</p></div>
<div class="m2"><p>دلتنگی از اهل عالم برداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسیدم از او که ای صنم نام تو چیست</p></div>
<div class="m2"><p>در حال نگین از سر خاتم برداشت</p></div></div>