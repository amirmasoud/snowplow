---
title: >-
    شمارهٔ  ۴۳
---
# شمارهٔ  ۴۳

<div class="b" id="bn1"><div class="m1"><p>من گوهر تو به قیمتی کم ندهم</p></div>
<div class="m2"><p>خاک در تو به ملکت جم ندهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد تو به صد هزار مرهم ندهم</p></div>
<div class="m2"><p>یک موی تو را به هر دو عالم ندهم</p></div></div>