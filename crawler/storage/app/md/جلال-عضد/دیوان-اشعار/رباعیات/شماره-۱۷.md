---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>هر سرو که در بسیط عالم باشد</p></div>
<div class="m2"><p>شاید که به پیش قامتت خم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سرو بلند! هر دمی چشم برآر</p></div>
<div class="m2"><p>بالای دراز را خرد کم باشد</p></div></div>