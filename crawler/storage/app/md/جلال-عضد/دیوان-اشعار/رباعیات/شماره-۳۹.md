---
title: >-
    شمارهٔ  ۳۹
---
# شمارهٔ  ۳۹

<div class="b" id="bn1"><div class="m1"><p>تا از جگر خصم کبابی نخورم</p></div>
<div class="m2"><p>وز کاسه کلّه اش شرابی نخورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقّا که نخسبم و نگیرم آرام</p></div>
<div class="m2"><p>باللّه که نیاسایم و آبی نخورم</p></div></div>