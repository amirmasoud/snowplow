---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>ای کرده ز لطف و قهر تو صنع خدا</p></div>
<div class="m2"><p>در عهد ازل بهشت و دوزخ پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزم تو بهشت است و مرا جرمی نیست</p></div>
<div class="m2"><p>چون است که در بهشت ره نیست مرا</p></div></div>