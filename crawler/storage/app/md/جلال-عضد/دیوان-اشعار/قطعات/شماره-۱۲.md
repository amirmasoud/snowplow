---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>پیشوای زمانه زین الدّین</p></div>
<div class="m2"><p>سرور اهل روزگاری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان را غلام می خوانی</p></div>
<div class="m2"><p>چرخ را بنده می شماری تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان آن یگانه‌ای امروز</p></div>
<div class="m2"><p>که دوم در زمین نداری تو</p></div></div>