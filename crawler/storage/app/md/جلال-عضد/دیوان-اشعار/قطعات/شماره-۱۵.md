---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>گر تو می‌گویی نبودَستَم تو را</p></div>
<div class="m2"><p>پیش ازین مفعول، الحق بوده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوده‌ام من فاعل مختار تو</p></div>
<div class="m2"><p>تو مرا مفعول مطلق بوده‌ای</p></div></div>