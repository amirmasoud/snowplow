---
title: >-
    شمارهٔ  ۱ - خوش
---
# شمارهٔ  ۱ - خوش

<div class="b" id="bn1"><div class="m1"><p>آفتابی که بنده سرگردان</p></div>
<div class="m2"><p>نام او را چو ذرّه در طلب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف بر رخ نهاد و من گفتم</p></div>
<div class="m2"><p>سر خورشید در کنار شب است</p></div></div>