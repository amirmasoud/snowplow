---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای وزیر جهان به فرزندان</p></div>
<div class="m2"><p>التفاتی نمی کنی چندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسرت هر کجا که بیند...یر</p></div>
<div class="m2"><p>از شعف در زند بدو دندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دخترت را به کو... همی... ایند</p></div>
<div class="m2"><p>کاین بود عادت خردمندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پی ما فتاده اند از آنک</p></div>
<div class="m2"><p>...یر ما هست سخت چون سندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیب ایشان بدین نشاید کرد</p></div>
<div class="m2"><p>چون به در می‌روند فرزندان</p></div></div>