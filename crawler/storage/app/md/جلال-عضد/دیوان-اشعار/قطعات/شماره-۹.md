---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>تشنه گشتم به راه کعبه شبی</p></div>
<div class="m2"><p>قحط بود آب و من بترسیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهبری را که بود نام عماد</p></div>
<div class="m2"><p>از همه رهبرانش بگزیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشش افکندم و من از پی او</p></div>
<div class="m2"><p>راست مانند باد پوییدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب تاریک راهبر گم شد</p></div>
<div class="m2"><p>من به تنها بسی بگردیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگهان بر کناره درّه</p></div>
<div class="m2"><p>چشمه آب و راهبر دیدم</p></div></div>