---
title: >-
    شمارهٔ  ۷ - امیر
---
# شمارهٔ  ۷ - امیر

<div class="b" id="bn1"><div class="m1"><p>در میان آر می که نام بتی ست</p></div>
<div class="m2"><p>عقل هر کس ز کنه آن قاصر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اوّل اوّل است اوّل او</p></div>
<div class="m2"><p>آخر اوست آخر آخر</p></div></div>