---
title: >-
    شمارهٔ  ۴
---
# شمارهٔ  ۴

<div class="b" id="bn1"><div class="m1"><p>خداوند! تویی ک اوراد مدحت</p></div>
<div class="m2"><p>بر افلاک و عناصر فرض باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جنب سرعت عزم عنانت</p></div>
<div class="m2"><p>سما سنگین عنان چون ارض باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون است از کمیّت جودت آری</p></div>
<div class="m2"><p>غرض عاری ز طول و عرض باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غرض از مایۀ تصدیع بیت است</p></div>
<div class="m2"><p>که بر رأی منیرت عرض باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کز تیغ او خور مایه دار است</p></div>
<div class="m2"><p>روا داری که او را قرض باشد</p></div></div>