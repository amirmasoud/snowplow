---
title: >-
    شمارهٔ  ۲۶۴
---
# شمارهٔ  ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>چنین کرشمه کنان گر به شهر برگذری</p></div>
<div class="m2"><p>هزار دل بربایی هزار جان ببری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا دلی ست پرآتش و زان همی ترسم</p></div>
<div class="m2"><p>که دامن تو بسوزد چو در دلم گذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه جای وعظ حکیم است و پند هشیاران</p></div>
<div class="m2"><p>مرا که عمر به مستی گذشت و بی خبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو روشنایی چشمی و هر کجا که منم</p></div>
<div class="m2"><p>چو روشنایی چشمم همیشه در نظری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پرده داری خود باد را مجال مده</p></div>
<div class="m2"><p>که دید گل هم ازین پرده دار، پرده دری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا و ناله بلبل ببین و گریه ابر</p></div>
<div class="m2"><p>در آن زمان که بخندد شکوفه سحری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلال! بر لب دریا به دست ناید کام</p></div>
<div class="m2"><p>درون بحر قدم نه چو طالب گهری</p></div></div>