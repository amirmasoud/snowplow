---
title: >-
    شمارهٔ  ۲۸۱
---
# شمارهٔ  ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>هر کس به جست و جویی هر گوشه گفت و گویی</p></div>
<div class="m2"><p>در پرده وصالش کس ره نبرده بویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک چند بت شکستند یک چند بت پرستند</p></div>
<div class="m2"><p>در هر سری هوایی ما را هوای رویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جایی که حاضر آید خوبان هر دو عالم</p></div>
<div class="m2"><p>روی دلم نگردد زان رو به هیچ رویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عاشقان برآید هر لحظه های هایی</p></div>
<div class="m2"><p>وان شوخ می فزاید هر روز های و هویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر جان در آرزویش بر لب رسد چه باشد</p></div>
<div class="m2"><p>سهل است اگر برآید جانی در آرزویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون باد صبحگاهی بگشود بند زلفش</p></div>
<div class="m2"><p>دیدم دل غمین را آویخته به مویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کوی دوست زنهار! آهسته نِهْ قدم را</p></div>
<div class="m2"><p>کز کوی او به در نیست راهی به هیچ سویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دم خیال قدّش از دیده نیست غایب</p></div>
<div class="m2"><p>سروی چنین نروید برطرف هیچ جویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی جلال می گفت از راز عشق رمزی</p></div>
<div class="m2"><p>افتاد در زبان‌ها زان روز گفت و گویی</p></div></div>