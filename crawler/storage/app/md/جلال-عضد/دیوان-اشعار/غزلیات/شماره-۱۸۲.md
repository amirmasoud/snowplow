---
title: >-
    شمارهٔ  ۱۸۲
---
# شمارهٔ  ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>در ره کعبه مقصود و بیابان حرم</p></div>
<div class="m2"><p>هر که از سر نکند پای زهی سست قدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زندگی با الم و زخم نخواهد مجروح</p></div>
<div class="m2"><p>گر کشی عین کرامت بود و محض کرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبرد خواب مرا هیچ شب از دست خیال</p></div>
<div class="m2"><p>دوست در پیش نظر چون بنهم دیده به هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نوشتم صفت شوق تو بر لوح درون</p></div>
<div class="m2"><p>آتشی خاست که نه لوح بماند و نه قلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که با دوست نشستم به مراد دل خویش</p></div>
<div class="m2"><p>اگرم جمله جهان دشمن جانند چه غم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیف باشد که کسی محرم این راز شود</p></div>
<div class="m2"><p>نیست با درد تو دل در حرم جان محرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صنما روی بپوشان ز نظرها ورنه</p></div>
<div class="m2"><p>بیم آنست که مردم بپرستند صنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که در خاک سر کوی تو مسکن دارم</p></div>
<div class="m2"><p>فارغ از گلشن فردوسم و گلزار ارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که با درد تو خوش گشت نجوید درمان</p></div>
<div class="m2"><p>دل که با زخم تو خو کرد نخواهد مرهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ببینم دهن تنگ ترا بیم بود</p></div>
<div class="m2"><p>که به یک آه کنم عالم موجود عدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم و دل لایق آن نیست که جای تو بود</p></div>
<div class="m2"><p>تنگنایی ست پر از آتش و جایی پُرنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دعوی عشق هر آن کس که کند همچو جلال</p></div>
<div class="m2"><p>مدّعی باشد اگر هیچ بنالد ز الم</p></div></div>