---
title: >-
    شمارهٔ  ۱۲۷
---
# شمارهٔ  ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>هیهات که نامم به زبان تو برآید</p></div>
<div class="m2"><p>یا همچو تویی را چو منی در نظر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر روز اجل بر سر بالین من آیی</p></div>
<div class="m2"><p>من زنده شوم باز چو عمرم به سرآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کام تو اینست که جانم به لب آری</p></div>
<div class="m2"><p>مقصود من آنست که کام تو برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدهوش شود عاشق اگر چشم تو بیند</p></div>
<div class="m2"><p>مستی که به میخانه رود بی خبر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ساغر سودای تو هر سر که شود مست</p></div>
<div class="m2"><p>زان سان رود از دست که از پای درآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر تیر که بر خسته زدی کارگر افتاد</p></div>
<div class="m2"><p>هر آه که مجروح زند کارگر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچون قد و خدّ تو مپندار که در باغ</p></div>
<div class="m2"><p>یک سرو کشید قامت و یک گل به برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن کاو چو جلال است گدای سر کویت</p></div>
<div class="m2"><p>شاهی جهان در نظرش مختصر آید</p></div></div>