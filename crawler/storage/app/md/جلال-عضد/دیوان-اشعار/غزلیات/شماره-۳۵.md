---
title: >-
    شمارهٔ  ۳۵
---
# شمارهٔ  ۳۵

<div class="b" id="bn1"><div class="m1"><p>نگویم در تو عیبی ای پسر هست</p></div>
<div class="m2"><p>ولیکن بی وفایی این قدر هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه در هجر توام خواب و قرار است</p></div>
<div class="m2"><p>نه در عشق تو از خویشم خبر هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن ناوک که زد چشم تو بر من</p></div>
<div class="m2"><p>هنوزم زخم پیکان در جگر هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی غایب نه ای از پیش چشمم</p></div>
<div class="m2"><p>وگر دوری خیالت در نظر هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبک باشد سری خالی ز سودا</p></div>
<div class="m2"><p>من و سودای جانان تا که سر هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نپندازم که در گلزار فردوس</p></div>
<div class="m2"><p>ز رخسارت گلی پاکیزه تر هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تعالی اللّه قباپوشی که او را</p></div>
<div class="m2"><p>کمر بر موی و مویی تا کمر هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمنّای دلم کردی و دادم</p></div>
<div class="m2"><p>بفرما گر تمنّایی دگر هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلال! ار چه شب هجران دراز است</p></div>
<div class="m2"><p>مشو غمگین که امّید سحر هست</p></div></div>