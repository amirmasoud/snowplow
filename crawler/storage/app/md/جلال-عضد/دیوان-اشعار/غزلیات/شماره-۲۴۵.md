---
title: >-
    شمارهٔ  ۲۴۵
---
# شمارهٔ  ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>قدحی نوش کن و جرعه به مخموران ده</p></div>
<div class="m2"><p>نوشدارو ز لب لعل به رنجوران ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا رود عافیت مردم مستور به باد</p></div>
<div class="m2"><p>خبر مستی آن غمزه به مستوران ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته ای هر که کشد هجر به وصلم برسد</p></div>
<div class="m2"><p>ای صبا! لطف کن این مژده به مهجوران ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ما گرم و هوا گرم و بیابان در پیش</p></div>
<div class="m2"><p>شربتی سرد اگرت هست به محروران ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده یک لحظه مدار از رخ خوبان خالی</p></div>
<div class="m2"><p>چشم را روشنی از طلعت منظوران ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب ما تیره شد ای ماه دل افروز برآی</p></div>
<div class="m2"><p>نور رویت به شب تیره بی نوران ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گدایی درت نام برآورد جلال</p></div>
<div class="m2"><p>منصب سلطنت وصل به مشهوران ده</p></div></div>