---
title: >-
    شمارهٔ  ۲۵۸
---
# شمارهٔ  ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>چه جرم رفت که یکباره مِهر ببریدی</p></div>
<div class="m2"><p>چه اوفتاد که از ما عنان بپیچیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قول و عهد تو دیگر که اعتماد کند</p></div>
<div class="m2"><p>که هر سخن که بگفتی از آن بگردیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو باز بر سر میدان عشق پای منه</p></div>
<div class="m2"><p>از آن جهت که به اوّل قدم بلغزیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر تو غنچه نورسته ای و من ابرم</p></div>
<div class="m2"><p>که زار زار چو بگریستم بخندیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار بار بگفتم ترا که مشنو هیچ</p></div>
<div class="m2"><p>ز دشمنان بدی دوستان و نشنیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جفا نمودی و رفتی و دل ندادی باز</p></div>
<div class="m2"><p>ز آه و ناله شبهای من نترسیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگفتیم که به کام دلت رسانم زود</p></div>
<div class="m2"><p>به کام دل نه ولیکن به جان رسانیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه حالتی ست که احوال ما نمی پرسی</p></div>
<div class="m2"><p>چه دشمنی ست که از دوستان برنجیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگفتمت که ز خوبان وفا مجوی جلال</p></div>
<div class="m2"><p>چو پند من نشنیدی سزای خود دیدی</p></div></div>