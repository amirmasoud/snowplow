---
title: >-
    شمارهٔ  ۵۴
---
# شمارهٔ  ۵۴

<div class="b" id="bn1"><div class="m1"><p>شوق توام باز گریبان گرفت</p></div>
<div class="m2"><p>اشک دوان آمد و دامان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل بود ترک دو عالم، ولی</p></div>
<div class="m2"><p>ترک رخ و زلف تو نتوان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان منی، بی تو نفس چون زنم</p></div>
<div class="m2"><p>زان که مرا بی تو دل از جان گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که چنین فرصتی از دست داد</p></div>
<div class="m2"><p>بس که سرانگشت به دندان گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارض او تا به درآورد خط</p></div>
<div class="m2"><p>خرده بسی بر مه تابان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خال تو بر لعل لبت دست یافت</p></div>
<div class="m2"><p>مورچه ای ملک سلیمان گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل طلب کعبه روی تو کرد</p></div>
<div class="m2"><p>حلقه آن زلف پریشان گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما و می و طرف گلستان که باز</p></div>
<div class="m2"><p>باد صبا راه گلستان گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی مه رخسار و شب زلف تو</p></div>
<div class="m2"><p>خاطرم از شمع و شبستان گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان جلال از همه عالم بِرست</p></div>
<div class="m2"><p>وز دو جهان دامن جانان گرفت</p></div></div>