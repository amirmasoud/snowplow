---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>این چه شمع است که در مجلس مستان برپاست</p></div>
<div class="m2"><p>وین چه شور است که از باده پرستان برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه نور است که اندیشه در و حیران است</p></div>
<div class="m2"><p>آن نه روی است که آن آینه لطف خداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی به سودای تو در باغ گذر می کردم</p></div>
<div class="m2"><p>راستی سرو به بالای تو می ماند راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و پروانه اگر چند در آتش باشیم</p></div>
<div class="m2"><p>کار پروانه که از شمع جدا نیست جداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه از زلف توام شام بلا روی نمود</p></div>
<div class="m2"><p>هم ز حسنش اثر صبح سعادت پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی به دست من درویش فتد خاک رهت</p></div>
<div class="m2"><p>زان که یک ذرّه از آن ملک دو دنیاش بهاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برود جان جلال از تن و عشقت نرود</p></div>
<div class="m2"><p>تازه آن گلبن عشقی که چنین پابرجاست</p></div></div>