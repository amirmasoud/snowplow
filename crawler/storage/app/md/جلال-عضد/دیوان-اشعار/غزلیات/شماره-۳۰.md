---
title: >-
    شمارهٔ  ۳۰
---
# شمارهٔ  ۳۰

<div class="b" id="bn1"><div class="m1"><p>چو سلطان فلک را بار بشکست</p></div>
<div class="m2"><p>مه من ماه را بازار بشکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شادُرْوان چو گل بیرون خرامید</p></div>
<div class="m2"><p>ز حسرت در دل گل خار بشکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب تاریک شد چون روز روشن</p></div>
<div class="m2"><p>چو سنبل در گل فرخار بشکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به لب یاقوت را خون در دل افکند</p></div>
<div class="m2"><p>به رخ خورشید را مقدار بشکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمر بربست و سرو از پا درافتاد</p></div>
<div class="m2"><p>کله بنهاد و بدر انوار بشکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز رعنا پیشگی بر قرص خورشید</p></div>
<div class="m2"><p>سر زنجیر عنبربار بشکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دور نرگس باده پرستش</p></div>
<div class="m2"><p>درِ خم خانه خمّار بشکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خون اشک رندان خرابات</p></div>
<div class="m2"><p>خمار نرگس خونخوار بشکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر ناموس عیّاران شبرو</p></div>
<div class="m2"><p>به چین طرّه عیّار بشکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فراوان توبه پرهیزکاران</p></div>
<div class="m2"><p>که آن جادوگر بیمار بشکست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جلال از توبه کردن کرد توبه</p></div>
<div class="m2"><p>که توبت کرد و دیگربار بشکست</p></div></div>