---
title: >-
    شمارهٔ  ۱۵۷
---
# شمارهٔ  ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>آن دم که می گذشتی ای سرو سبزپوش</p></div>
<div class="m2"><p>از غمزه ناوک افکن و از چهره گل فروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم چو بر شمایل و قدّ تو اوفتاد</p></div>
<div class="m2"><p>دودم به سر برآمد و از من برفت هوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنم که دلبران ز غمم جوش می زنند</p></div>
<div class="m2"><p>مغزم درآمدست ز سودای تو به جوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تو مرا مباد مَی و زندگی حلال</p></div>
<div class="m2"><p>بی من ترا مباد یکی شربت آب نوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پرده ای و پرده عشّاق می دری</p></div>
<div class="m2"><p>بردار پرده از رخ و بر عاشقان مپوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوشت به خواب دیدم که دوشم به دوش تست</p></div>
<div class="m2"><p>امروز جان همی دهم از آرزوی دوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی بیا به گردن مقصود حلقه کن</p></div>
<div class="m2"><p>دست جلال را که ترا هست حلقه گوش</p></div></div>