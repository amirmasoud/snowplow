---
title: >-
    شمارهٔ  ۱۰۵
---
# شمارهٔ  ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>اگر نسیم صبا زلف او برافشاند</p></div>
<div class="m2"><p>هزار جان مقیّد ز بند برهاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منش ببینم و از دور رخ نهم بر خاک</p></div>
<div class="m2"><p>مرا ببیند و از دور رخ بگرداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قد خمیده خود را همی کنم سجده</p></div>
<div class="m2"><p>از آن جهت که به ابروی دوست می ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر مراد تو جان است کار جان سهل است</p></div>
<div class="m2"><p>چه حاجت است که چشمت به زور بستاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز زلف خویش نسیمی فرست عاشق را</p></div>
<div class="m2"><p>که بی حدیث به بوی تو جان برافشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساز چاره بیچارگان خود امروز</p></div>
<div class="m2"><p>که کار وعده فردا کسی نمی داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال لعل تو در دل نهفته نتوان داشت</p></div>
<div class="m2"><p>که آبگینه کجا رنگ می بپوشاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز روی دوست صبوری نمی توانم کرد</p></div>
<div class="m2"><p>چرا که تشنه صبوری ز آب نتواند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون که کار من خسته از دوا بگذشت</p></div>
<div class="m2"><p>بگو طبیب مرا تا قدم نرنجاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جلال اگر نفسی پیش بخت بنشیند</p></div>
<div class="m2"><p>بسی گناه که بر بخت خویش بنشاند</p></div></div>