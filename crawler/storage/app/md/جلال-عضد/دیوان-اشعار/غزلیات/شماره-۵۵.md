---
title: >-
    شمارهٔ  ۵۵
---
# شمارهٔ  ۵۵

<div class="b" id="bn1"><div class="m1"><p>رفت عمر و غم عشقش ز دل ریش نرفت</p></div>
<div class="m2"><p>هیچ کاری به مراد دلم از پیش نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد و زنده شدم رفت و بر آمد نفسم</p></div>
<div class="m2"><p>نفسی بیش نیامد نفسی بیش نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بنالم، من دلسوخته عیبم مکنید</p></div>
<div class="m2"><p>رفت درمانم و دردم ز دل ریش نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالها خاطر درویش تمنّایی داشت</p></div>
<div class="m2"><p>عمر بگذشت و امید از دل درویش نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از وفا بود که ترکش نگرفتم ورنه</p></div>
<div class="m2"><p>چه ستم بر من از آن شوخ جفاکیش نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر شدم بی خبر از عشق مدارید عجب</p></div>
<div class="m2"><p>باده عشق که نوشید که از خویش نرفت؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوک مژگان تو در چشم من آمد روزی</p></div>
<div class="m2"><p>خون چکانست که از چشم من آن نیش نرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیز ازین در به سلامت سر خود گیر جلال!</p></div>
<div class="m2"><p>سرنهاد آنکه ازین در به سر خویش نرفت</p></div></div>