---
title: >-
    شمارهٔ  ۱۷۹
---
# شمارهٔ  ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>منم کز درت سر به راهی نکردم</p></div>
<div class="m2"><p>به جز آستانت پناهی نکردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آنی که خونم به باطل بریزی</p></div>
<div class="m2"><p>گناهی مکن چون گناهی نکردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن نیستم کز خدنگت بنالم</p></div>
<div class="m2"><p>که صد تیر خوردم که آهی نکردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم سیر ازین زندگانی که هرگز</p></div>
<div class="m2"><p>دمی سیر در تو نگاهی نکردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چشمم نیامد گل و چشمه بی تو</p></div>
<div class="m2"><p>قناعت به آب و گیاهی نکردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از هر مرادی ترا خواستم بس</p></div>
<div class="m2"><p>تمنّای مالی و جاهی نکردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگفتم جفاهای زلف تو با کس</p></div>
<div class="m2"><p>شکایت ز دست سیاهی نکردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چون جلال از در خود چه رانی</p></div>
<div class="m2"><p>که من جز درت قبله گاهی نکردم</p></div></div>