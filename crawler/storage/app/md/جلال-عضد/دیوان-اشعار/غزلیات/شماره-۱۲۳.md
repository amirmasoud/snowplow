---
title: >-
    شمارهٔ  ۱۲۳
---
# شمارهٔ  ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>تیری کز آن دو غمزه پرفن برون جهد</p></div>
<div class="m2"><p>تنها نه از دلم که ز آهن برون جهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ساعتی به موج دگرگون در اوفتم</p></div>
<div class="m2"><p>از سیل دیده ام که ز دامن برون جهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین سان که در شکنجه هجران در افتاده ام</p></div>
<div class="m2"><p>بیم است جان خسته که از تن برون جهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر صبح و شام کلّه ببندد بر آسمان</p></div>
<div class="m2"><p>این دود آه من که ز روزن برون جهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم حدیثی از دهن خویشتن بگوی</p></div>
<div class="m2"><p>گفت این سخن کی از دهن من برون جهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح است و مهر دم زده زین صحن دودناک</p></div>
<div class="m2"><p>مانند شعله که ز [روزن] گلخن برون جهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان پرورد نسیم که از زلف او وزد</p></div>
<div class="m2"><p>چون باد صبحدم که ز گلشن برون جهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقی بگو به بلبل تا برکشد نوا</p></div>
<div class="m2"><p>باشد که زاغ غم ز نشیمن برون جهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان سان گداخته ست ز هجران او جلال</p></div>
<div class="m2"><p>کز لاغری ز چشمه سوزان برون جهد</p></div></div>