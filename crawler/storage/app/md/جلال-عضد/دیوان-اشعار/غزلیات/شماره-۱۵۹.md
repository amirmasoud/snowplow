---
title: >-
    شمارهٔ  ۱۵۹
---
# شمارهٔ  ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>تا کی از دست جفایت تیره بینم روز خویش</p></div>
<div class="m2"><p>ساعتی دلسوز شو بر عاشق دلسوز خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت پیروزم تو باشی گر در آیی از درم</p></div>
<div class="m2"><p>من شوم از جان غلام طالع پیروز خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی بی روی تو در ظلمت غم سوختند</p></div>
<div class="m2"><p>برفکن معجر ز رخسار جهان افروز خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ملامتگر ! ز جان من چه می خواهی بگو</p></div>
<div class="m2"><p>من خود اندر آتشم زین جان درد اندوز خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگارم تار و روزم تیره دست از من بدار</p></div>
<div class="m2"><p>تابگریم ساعتی بر روزگار و روز خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که را گوشی بود موقوف پیغام بلاست</p></div>
<div class="m2"><p>کی تواند گوش کردن پند نیک آموز خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جوانی پیر گشتم از غم تیمار عشق</p></div>
<div class="m2"><p>برگ ریزان خزانی دیدم از نوروز خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر شبی بنشینم و با شمع گویم درد دل</p></div>
<div class="m2"><p>هم به نزد سوخته گر زآنکه گویی سوز خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای کمان ابرو! بترس از ناوک آه جلال</p></div>
<div class="m2"><p>بردلش چند آزمایی ناوک دل دوز خویش</p></div></div>