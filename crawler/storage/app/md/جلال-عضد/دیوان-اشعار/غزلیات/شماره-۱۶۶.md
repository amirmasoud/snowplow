---
title: >-
    شمارهٔ  ۱۶۶
---
# شمارهٔ  ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>ای ز دوری رخت جامه صبرم شده چاک</p></div>
<div class="m2"><p>شخص عقلم شده در چنگ هوای تو هلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دو عالم اگرم هیچ نباشد سهل است</p></div>
<div class="m2"><p>چون تو هستی اگرم هیچ دگر نیست چه باک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه دیگر ز خجالت نزند خرگه حسن</p></div>
<div class="m2"><p>کز فروغ رخ تو خیمه زند بر افلاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست از دامن عشق تو ندارم هرگز</p></div>
<div class="m2"><p>ور زند دست اجل دامن عمرم را چاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طرب و عشق نیامد ز من این هر دو برفت</p></div>
<div class="m2"><p>که مرا بود دلی خسته و جانی غمناک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر محک غم عشقت همه دلها قلبند</p></div>
<div class="m2"><p>زان که یک دل چو دل خویش نمی بینم پاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه کاندر دل شوریده چه حسرت ماند</p></div>
<div class="m2"><p>گر برد آرزوی روی تو با خویش به خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ز خورشید رخت چشم خرد حیران ماند</p></div>
<div class="m2"><p>کی تواند که کند دیده ز همّت ادراک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد از غیر تو خالی دل پردرد جلال</p></div>
<div class="m2"><p>بر گذرگاه تو حاشا که بماند خاشاک</p></div></div>