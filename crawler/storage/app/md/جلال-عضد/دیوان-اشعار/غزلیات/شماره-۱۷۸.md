---
title: >-
    شمارهٔ  ۱۷۸
---
# شمارهٔ  ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>باز از چمن غیب برآورد صبا دم</p></div>
<div class="m2"><p>ساقی منشین خیز و بده جام دمادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جام صفاهاست که بی‌جام جهان‌تاب</p></div>
<div class="m2"><p>کی صبح برآرد ز سر صدق و صفا دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من می خورم و جرعه بدین دخمه فشانم</p></div>
<div class="m2"><p>کاندر خم این خمکده بگرفت مرادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از اهل جهان یافت نشد اهل وفایی</p></div>
<div class="m2"><p>کز رنگ وفا بوی ندارد گل آدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دم می دهدت عمر دم از عمر مزن هیچ</p></div>
<div class="m2"><p>در کوی فنا چند توان زد ز بقا دم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرّی ست ز جان، چرخ ز بس جان که ربوده ست</p></div>
<div class="m2"><p>کز رنگ رخ کاه زند کاه رُبا دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خاک جلال ار گذری ای که یقینی</p></div>
<div class="m2"><p>تقصیر مکن فاتحه‌ای بر گِل ما دم</p></div></div>