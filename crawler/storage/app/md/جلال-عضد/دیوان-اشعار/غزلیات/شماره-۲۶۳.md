---
title: >-
    شمارهٔ  ۲۶۳
---
# شمارهٔ  ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>صبحدم می گفت نالان بلبلی بر شاخساری:</p></div>
<div class="m2"><p>گل بخواهد رفت تا دیگر که بیند نوبهاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که را روزی صفایی رو نماید در زمانه</p></div>
<div class="m2"><p>روزگارش تیره گرداند به اندک روزگاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله هر سال از چمن یک بار روید وین عجب بین</p></div>
<div class="m2"><p>کز سرشک دیده ام هر دم بروید لاله زاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع بر بالین من تا روز هر شب زنده دارد</p></div>
<div class="m2"><p>بر من دل خسته می گرید زهی دلسوز یاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر من آن کس را بسوزد دل که همچون شمع باشد</p></div>
<div class="m2"><p>شب نشینی تن گدازی زرد رویی اشکباری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصحم گوید به یکبار اختیار از دست مگذار</p></div>
<div class="m2"><p>این نصیحت گو کسی را کن که دارد اختیاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیوه طوطی هوس دان عاشقی از بلبل آموز</p></div>
<div class="m2"><p>کآن یکی با شهد الفت دارد این با نوک خاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در میان بحر محنت غرقم از شوق میانش</p></div>
<div class="m2"><p>با کنار افتادمی گر بودی امّید کناری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سالها بر خاک کویش زندگانی صرف کردم</p></div>
<div class="m2"><p>عمر بگذشت و ازین در برنیامد هیچ کاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای جلال! اندر پی هر سختیی آسانی است</p></div>
<div class="m2"><p>هر بهاری را خزانی هر خزانی را بهاری</p></div></div>