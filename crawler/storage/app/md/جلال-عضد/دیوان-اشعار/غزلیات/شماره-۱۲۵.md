---
title: >-
    شمارهٔ  ۱۲۵
---
# شمارهٔ  ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>بی روی دل افروزت عمرم به چه کار آید</p></div>
<div class="m2"><p>با لعل جهان سوزت جان در چه شمار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد کشتی عمر من در بحر هوایت غرق</p></div>
<div class="m2"><p>هیهات که آن کشتی روزی به کنار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم بوی بهار آید از چین سر زلفت</p></div>
<div class="m2"><p>در باغ سحرگاهان چون باد بهار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن لحظه که تو برقع از چهره براندازی</p></div>
<div class="m2"><p>در دیده مشتاقان گل راست چو خار آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شد ز دیار خود و اکنون به دیاری نیست</p></div>
<div class="m2"><p>باشد که ز کوی تو روزی به دیار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی که نباشم من، از خاک من غمگین</p></div>
<div class="m2"><p>باشد ورقش خونین هر گل که به بار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر جان جلال از غم هر لحظه منه باری</p></div>
<div class="m2"><p>کآن عاشق مسکین هم روزیت به کار آید</p></div></div>