---
title: >-
    شمارهٔ  ۱۶۱
---
# شمارهٔ  ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>آتش دل چند سوزد رشته جانم چو شمع؟</p></div>
<div class="m2"><p>ای صبا! تشریف ده تا جان برافشانم چو شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز من چون شمع روشن گشت در هر محفلی</p></div>
<div class="m2"><p>بس که سیل آتشین از دیده می‌رانم چو شمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم امشب گرمیی در سر که ننشینم ز پای</p></div>
<div class="m2"><p>تا سراپای وجود خود نسوزانم چو شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شبم تا صبحدم آتش به سر بر می رود</p></div>
<div class="m2"><p>و آب حسرت می رود از رخ به دامانم چو شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک زمان میرم زمانی خانه را روشن کنم</p></div>
<div class="m2"><p>تا به جان کندن شبی را روز گردانم چو شمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست دلسوزی که بر بالین من گرید دمی</p></div>
<div class="m2"><p>در شب تنهایی ار بر لب رسد جانم چو شمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سرم از دست خواهد رفت ننشینم ز پای</p></div>
<div class="m2"><p>تا ترا در بزم خود یک دم بننشانم چو شمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدسیان آیند پیرامون کویم در طواف</p></div>
<div class="m2"><p>گر شبی از نور خود بفروزی ایوانم چو شمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناصحم گوید نیاری برد جان از سوز عشق</p></div>
<div class="m2"><p>گرنه سوز عشق باشد زنده کی مانم چو شمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر سحرگاهی چه آیم سوی مسجد چون جلال</p></div>
<div class="m2"><p>من که هر شامی حریف بزم رندانم چو شمع</p></div></div>