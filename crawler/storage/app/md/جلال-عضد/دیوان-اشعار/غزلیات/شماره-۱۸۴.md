---
title: >-
    شمارهٔ  ۱۸۴
---
# شمارهٔ  ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>ترا از هر دو عالم برگزیدم</p></div>
<div class="m2"><p>به صد ناز و نیازت پروریدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی بر دیده خود جات کردم</p></div>
<div class="m2"><p>گهی جان پیش پایت گستریدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عشقت ترک نام و ننگ گفتم</p></div>
<div class="m2"><p>هوایت را به جان و دل خریدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سختی ها که در هجر تو دیدم</p></div>
<div class="m2"><p>چه محنت ها که در عشقت کشیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه مایه طعنه های دشمن و دوست</p></div>
<div class="m2"><p>که گاه و بی گه از بهرت شنیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فراوان اشک در هجرت فشاندم</p></div>
<div class="m2"><p>فراوان جامه بر یادت دریدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهت بر آستان سر می نهادم</p></div>
<div class="m2"><p>گهی بر گرد کویت می دویدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه یک ساعت جدا می گشتم از تو</p></div>
<div class="m2"><p>نه یک دم بی رخت می آرمیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون نامهربانی پیشه کردی</p></div>
<div class="m2"><p>امید از مهر و پیمانت بریدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر دیگر کسان حالم ندانند</p></div>
<div class="m2"><p>تو می دانی که از بهرت چه دیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنونم خود پرو بالی نمانده است</p></div>
<div class="m2"><p>که وقتی در هوایت می پریدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخورده شربتی شیرین ز لعلت</p></div>
<div class="m2"><p>چه تلخیها که از دوران چشیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رسد گفتی جلال از من به کامی</p></div>
<div class="m2"><p>حقیقت خوش به کام دل رسیدم</p></div></div>