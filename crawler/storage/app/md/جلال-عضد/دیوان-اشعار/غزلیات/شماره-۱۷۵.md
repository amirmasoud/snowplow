---
title: >-
    شمارهٔ  ۱۷۵
---
# شمارهٔ  ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>تو آفتاب بلندی و من چنین پستم</p></div>
<div class="m2"><p>به دامنت چه عجب گر نمی رسد دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدح به دست حریفان باده پیما ده</p></div>
<div class="m2"><p>مرا به باده چه حاجت که روز و شب مستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درون کعبه دل در شدم طواف کنان</p></div>
<div class="m2"><p>درو هر آنچه به جز دوست بود بشکستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن زمان که تو برخاستی ز مجلس انس</p></div>
<div class="m2"><p>به جست و جوی تو یک دم ز پای ننشستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بند زلف تو نگشاد جز پریشانی</p></div>
<div class="m2"><p>مرا که از همه عالم دل اندرو بستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از تجلّی حُسن تو گم شدم در خود</p></div>
<div class="m2"><p>بسان ذرّه که با آفتاب پیوستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر دو پای مقیّد شدم به دام بلا</p></div>
<div class="m2"><p>اگرچه بود گمانم که از بلا رستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه صبر ماند و نه هوش و نه عقل ماند و نه دین</p></div>
<div class="m2"><p>سعادتی ست که از دست دشمنان جَستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو جلال خردمند و عاقل است که نیست</p></div>
<div class="m2"><p>اگر به عاشق و دیوانه خوانی‌ام هستم</p></div></div>