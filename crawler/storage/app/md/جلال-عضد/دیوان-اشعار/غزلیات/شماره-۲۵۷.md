---
title: >-
    شمارهٔ  ۲۵۷
---
# شمارهٔ  ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>به یاد آور که در ایّام خُردی</p></div>
<div class="m2"><p>قدم در دوستی چون می فشردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی دانستی آیین جفا را</p></div>
<div class="m2"><p>طریق مهربانی می سپردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بوسه دردم از دل می کشیدی</p></div>
<div class="m2"><p>به گیسو گردم از رخ می سُتردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خُردی داشتی خوی بزرگان</p></div>
<div class="m2"><p>گرفتی در بزرگی خوی خُردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پایان آر دل جویی چو دل را</p></div>
<div class="m2"><p>به اوّل دستبرد از دست بردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنونت عشوه با من در نگیرد</p></div>
<div class="m2"><p>که با گردان نشاید کرد گُردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلال آن باده کآن بد مهر پیمود</p></div>
<div class="m2"><p>به اوّل صاف و آخر بود دُردی</p></div></div>