---
title: >-
    شمارهٔ  ۲۷۶
---
# شمارهٔ  ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>ای به بازار غم عشق تو صد جان به جوی</p></div>
<div class="m2"><p>خود ترا نیست غم حال اسیران به جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که دلاّل غمت حلقه جانبازان دید</p></div>
<div class="m2"><p>می زند نعره و فریاد که صد جان به جوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کند داس فنا خرمن هستی جوجو</p></div>
<div class="m2"><p>بر من بی خبر واله حیران به جوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار عالم همه گر بی سر و سامان گردد</p></div>
<div class="m2"><p>بر من سوخته بی سرو سامان به جوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام جمشید به من ده که نیرزد برِ من</p></div>
<div class="m2"><p>گنج قارون به دو جو مُلک سلیمان به جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ما جز سخن باده و پیمانه مگوی</p></div>
<div class="m2"><p>که نیرزد همه عالم بر رندان به جوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای فلک! گرمی بازار به یک نان چه کنی؟</p></div>
<div class="m2"><p>هست در ملک دل ما صد ازین نان به جوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر توان دید به بازار قیامت رخ دوست</p></div>
<div class="m2"><p>هیچ عاشق نخرد روضه رضوان به جوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان بدادیم ز عشق و برِ جانان هیچ است</p></div>
<div class="m2"><p>سوخت در درد جلال و بر درمان به جوی</p></div></div>