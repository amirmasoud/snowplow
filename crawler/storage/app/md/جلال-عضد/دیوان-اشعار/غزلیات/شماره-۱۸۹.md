---
title: >-
    شمارهٔ  ۱۸۹
---
# شمارهٔ  ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>گرچه غریق بحر مضایق چو لنگرم</p></div>
<div class="m2"><p>آخر دلی وسیع چو بحری ست در برم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بحر اگر چه نیست به جز باد در کفم</p></div>
<div class="m2"><p>از گوهر نسفته چو دریا توانگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من صافی اندرون و حریفان دور ما</p></div>
<div class="m2"><p>خونم همی خورند به دستان که ساغرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سوز سینه دامن عودی آسمان</p></div>
<div class="m2"><p>دود دلم گرفته تو گویی که مجمرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تیغ اگر برهنه ام از جور روزگار</p></div>
<div class="m2"><p>زانم زبان دراز که پاکیزه گوهرم</p></div></div>