---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>بس که جانم ز تمنّای رخ یار بسوخت</p></div>
<div class="m2"><p>دل هر سوخته بر زاری من زار بسوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منِ آتش نفس اندر طلبش آه زنان</p></div>
<div class="m2"><p>هر کجا گام نهادم در و دیوار بسوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک نظر حسن رخش پرده برانداخت ز پیش</p></div>
<div class="m2"><p>عالمی را دل و جان از تف انوار بسوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با طبیب من دل خسته بگویید آخر</p></div>
<div class="m2"><p>که ز تاب تب هجران تو بیمار بسوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیشب از سرّ اناالحق خبری یافت دلم</p></div>
<div class="m2"><p>بزد آهی و سراپرده اسرار بسوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیخ چون حالت رندان خرابات بدید</p></div>
<div class="m2"><p>خرقه تقوی خود بر در خمّار بسوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز پروانه به جز بال نسوزد تف عشق</p></div>
<div class="m2"><p>شمع را بین که سراپای به یک بار بسوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش شوق که اندر دل مشتاقان زد</p></div>
<div class="m2"><p>آتشی بود کز آن دیده اغیار بسوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه جز دوست به بازار دل ما بگذشت</p></div>
<div class="m2"><p>غیر او بود و هم از گرمی بازار بسوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود عمری که درین پرده همی سوخت جلال</p></div>
<div class="m2"><p>چاره کار نمی‌دید و به ناچار بسوخت</p></div></div>