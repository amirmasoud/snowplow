---
title: >-
    شمارهٔ  ۶۵
---
# شمارهٔ  ۶۵

<div class="b" id="bn1"><div class="m1"><p>گل رنگ نگار ما ندارد</p></div>
<div class="m2"><p>بوی خوش یار ما ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماییم و دیار بی نشانی</p></div>
<div class="m2"><p>کس میل دیار ما ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما کار به کار کس نداریم</p></div>
<div class="m2"><p>کس کار به کار ما ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با ما سخن سمن مگویید</p></div>
<div class="m2"><p>کاو بوی بهار ما ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ما صفت چمن مخوانید</p></div>
<div class="m2"><p>کاو نقش نگار ما ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله ز چه سرخ گشت گر شرم</p></div>
<div class="m2"><p>از لاله عذار ما ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون بار جلال در کنارت</p></div>
<div class="m2"><p>کاو میل کنار ما ندارد</p></div></div>