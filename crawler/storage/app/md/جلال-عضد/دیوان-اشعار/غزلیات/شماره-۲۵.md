---
title: >-
    شمارهٔ  ۲۵
---
# شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>آن چه شمع است که از چهره برافروخته است</p></div>
<div class="m2"><p>که چو پروانه دل سوختگان سوخته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهن دوست که تنگی ز وی آموخت دلم</p></div>
<div class="m2"><p>دُرفشانی مگر از چشم من آموخته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتشی از دل او در دل من می افتد</p></div>
<div class="m2"><p>که دلش آهن و سنگ است و دلم سوخته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلبرا طرّه دزد تو چه پُردل دزدی ست</p></div>
<div class="m2"><p>کآن همه دل به یکی شب به هم اندوخته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل سیه پوش شد از زلف تو چون جا بنماند</p></div>
<div class="m2"><p>جامه ماتم جان است که بر دوخته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ز دیوانگی باد صبا در عجبم</p></div>
<div class="m2"><p>که به یک جان که ستد بوی تو بفروخته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوییا هم اثری هست ز هستی جلال</p></div>
<div class="m2"><p>ورنه این آتش سوزان ز چه افروخته است</p></div></div>