---
title: >-
    شمارهٔ  ۱۵۶
---
# شمارهٔ  ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>گل خودروی و شمشاد قصب پوش</p></div>
<div class="m2"><p>در آمد شاد و خندان از درم دوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقاب مشک بو بگشاده از ماه</p></div>
<div class="m2"><p>کمند عنبرین افکنده بر دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بند پرنیان آزاده سَروش</p></div>
<div class="m2"><p>ولی دربند هندویی زره پوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی دانم چه در گوشش همی گفت</p></div>
<div class="m2"><p>که حاجب بُرده بودش سر سوی گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من لطف سراپایش بدیدم</p></div>
<div class="m2"><p>در افتادم ز پای و رفتم از هوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا چون دید بر خاک اوفتاده</p></div>
<div class="m2"><p>به بوی وصل او سرمست و مدهوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صد لطفم ز روی خاک بر داشت</p></div>
<div class="m2"><p>کشید از یکدلی تنگم در آغوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان مستغرق وصلش شدم من</p></div>
<div class="m2"><p>که کردم دین و دنیا را فراموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا گویند چون دیدی وصالش</p></div>
<div class="m2"><p>جلال از دست غم من بعد مخروش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی چون گل برون آید ز غنچه</p></div>
<div class="m2"><p>کجا بلبل تواند بود خاموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو وقت گل ز مَی منعت کند شیخ</p></div>
<div class="m2"><p>ز می بنیوش و پند شیخ منیوش</p></div></div>