---
title: >-
    شمارهٔ  ۴۴
---
# شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>جلوه حسن ترا محرم به جز آیینه نیست</p></div>
<div class="m2"><p>سرّ سودای خیالت محرم هر سینه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حُسن خود خواهی که بینی در دل ما کن نظر</p></div>
<div class="m2"><p>اندرون پاکبازان کمتر از آیینه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته ای امروز خواهم کرد کار تو تمام</p></div>
<div class="m2"><p>لطف تو در حقّ ما ای دوست امروزینه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مار زلفش گنج حُسن آشفته می دارد مگر</p></div>
<div class="m2"><p>حسن او را بهتر از این مار در گنجینه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهربان شو، عاشق دیرینه خود را مکش</p></div>
<div class="m2"><p>زانکه مهر هیچ کس چون عاشق دیرینه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر می ورزد جلال آخر تو نیز ای کینه جوی</p></div>
<div class="m2"><p>مهربانی کن سزای مهربانان کینه نیست</p></div></div>