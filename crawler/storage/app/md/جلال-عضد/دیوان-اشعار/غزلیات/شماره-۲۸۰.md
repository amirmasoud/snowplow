---
title: >-
    شمارهٔ  ۲۸۰
---
# شمارهٔ  ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>گر گریه من بشنوی ای یار بگریی</p></div>
<div class="m2"><p>ور زاری من گوش کنی زار بگریی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر حال من سوخته زار بدانی</p></div>
<div class="m2"><p>بر درد من سوخته زار بگریی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی که چو پروانه به داغ تو بمیرم</p></div>
<div class="m2"><p>چون شمع فراوان به شب تار بگریی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیار میازار مرا ورنه پس از من</p></div>
<div class="m2"><p>یک روز به یاد آری و بسیار بگریی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فردا چه کنی، تو قدمی رنجه کن امروز</p></div>
<div class="m2"><p>باشد که دمی بر سر بیمار بگریی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کشته خود گرچه ترا گریه نیاید</p></div>
<div class="m2"><p>چون خلق بگریند به ناچار بگریی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دیده خونبار جلال ار شوی آگاه</p></div>
<div class="m2"><p>بسیار برین دیده خونبار بگریی</p></div></div>