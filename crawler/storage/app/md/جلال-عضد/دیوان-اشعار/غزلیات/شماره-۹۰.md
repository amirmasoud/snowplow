---
title: >-
    شمارهٔ  ۹۰
---
# شمارهٔ  ۹۰

<div class="b" id="bn1"><div class="m1"><p>برقی از حُسن تو پیدا شد و ناپیدا شد</p></div>
<div class="m2"><p>دهر پرفتنه و ایّام پُر از غوغا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بر آیینه فتاد از رخ و زلفت عکسی</p></div>
<div class="m2"><p>یافت نوری که درو هر دو جهان پیدا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنبرین زلف مسلسل که فکندی بر ماه</p></div>
<div class="m2"><p>کافری بود که فردوس برینش جا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم دیده به دامن گهر از چشمم یافت</p></div>
<div class="m2"><p>هندو از بهر گهر معتکف دریا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن چه بالاست که با موی تو هم قد آمد</p></div>
<div class="m2"><p>وین چه موری ست که با قدّ تو هم بالا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح با طلعت تو دعوی خوبی می کرد</p></div>
<div class="m2"><p>لاجرم در نظر خلق جهان رسوا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سحرگاه برافروخت چراغ ملکوت</p></div>
<div class="m2"><p>تف آهم که برین آینه خضرا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم به دل دوستی نرگس خون ریز تو بود</p></div>
<div class="m2"><p>ساغر چشمم ازین گونه که خون پالا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بویی از نافه زلفت به چمن برد صبا</p></div>
<div class="m2"><p>مشک ساگشت چمن، خاک عبیر آسا شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر زبان نام لب لعل تو تا راند جلال</p></div>
<div class="m2"><p>چه عجب نامش اگر طوطی شکرخا شد</p></div></div>