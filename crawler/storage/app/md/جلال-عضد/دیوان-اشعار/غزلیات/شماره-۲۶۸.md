---
title: >-
    شمارهٔ  ۲۶۸
---
# شمارهٔ  ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>بده ساقی شراب لایزالی</p></div>
<div class="m2"><p>به دست عاشقان لاابالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تموّج فی السّفینَه بَحرُ خَمر</p></div>
<div class="m2"><p>کاَنَّ الشّمس فی جَوف الهلالِ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبادا چشم ما بی باده روشن</p></div>
<div class="m2"><p>مبادا جان ما از عشق خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم خفته شب کوته نماید</p></div>
<div class="m2"><p>سَلوا عَن مُقلتی طولَ اللّیالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه چیزی زوالی یابد آخر</p></div>
<div class="m2"><p>وَ عشقی قَد تبرّء عَنْ زَوالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر بگذشته ای بر خاک کویش</p></div>
<div class="m2"><p>که جان می بخشی ای باد شمالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بی خویشی نمی دانم پس و پیش</p></div>
<div class="m2"><p>وَ ما اَدْریٰ یمینی عنْ شِمالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر در آب باشم ور در آتش</p></div>
<div class="m2"><p>خیالُک مونسی فی کلّ حالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اردتُ المالَ مالی غَیرَ قلب</p></div>
<div class="m2"><p>و هذا القلبُ فی دُنیای مالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا از دوستی دل برگرفتی</p></div>
<div class="m2"><p>اگر نه دشمن جان جلالی</p></div></div>