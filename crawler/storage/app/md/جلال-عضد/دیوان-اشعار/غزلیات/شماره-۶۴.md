---
title: >-
    شمارهٔ  ۶۴
---
# شمارهٔ  ۶۴

<div class="b" id="bn1"><div class="m1"><p>در میان چشم و دل خون اوفتاد</p></div>
<div class="m2"><p>راز ما از پرده بیرون اوفتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم مستت دلربایی پیشه کرد</p></div>
<div class="m2"><p>فتنه ای در ربع مسکون اوفتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرتوی زد عکس رویت بر فلک</p></div>
<div class="m2"><p>غلغلی در اوج گردون اوفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرحبا ای جان جانها کز رخت</p></div>
<div class="m2"><p>فال مشتاقان همایون اوفتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سحر کآید صبا از کوی دوست</p></div>
<div class="m2"><p>همچو غنچه در دلم خون اوفتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرفه می دارم که عشق چون تویی</p></div>
<div class="m2"><p>در دماغ چون منی چون اوفتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به چشم من درآمد اشک من</p></div>
<div class="m2"><p>از دو چشمم درّ مکنون اوفتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حُسن لیلی یک نظر بنمود روی</p></div>
<div class="m2"><p>هر دو کون از چشم مجنون اوفتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سماع از شعر شیرین جلال</p></div>
<div class="m2"><p>هر زمان شوری دگرگون اوفتاد</p></div></div>