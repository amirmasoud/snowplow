---
title: >-
    شمارهٔ  ۱۹۶
---
# شمارهٔ  ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>شکر است که سجّاده درافتاد ز دوشم</p></div>
<div class="m2"><p>تا من نتوانم که دگر زهد فروشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل هر نفسم پند همی داد که هش دار</p></div>
<div class="m2"><p>المنّة لِلّه که نه دل ماند و نه هوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین پس من و رندی و سر کوی خرابات</p></div>
<div class="m2"><p>وز هر که جهان پند دهندم ننیوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیخم سخنی گفت و دلم زو ننیوشید</p></div>
<div class="m2"><p>ساقی قدحی ده که به روی تو بنوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند که پوشیده ام این دلق مرقّع</p></div>
<div class="m2"><p>زنّار هوس می کندم از تو چه پوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر صبح به مسجد چه نهم پای که هر شب</p></div>
<div class="m2"><p>از میکده آرند سوی خانه به دوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غلغل به صف قدس درافتد چو درآید</p></div>
<div class="m2"><p>در نیم شبان از در میخانه خروشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش زندم اندر خم و خمخانه افلاک</p></div>
<div class="m2"><p>بر خود چو خُم باده هر آن گه که بجوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای زاهد شب خیز مده دردسرِ ما</p></div>
<div class="m2"><p>کز یا رب تو خواب نیامد شب دوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وی چاکر سرحلقه رندان خرابات</p></div>
<div class="m2"><p>من حلقه به گوشان ترا حلقه به گوشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویند که باز آی جلال از مَی و معشوق</p></div>
<div class="m2"><p>تقدیر چو یاری ندهد هرزه چه کوشم</p></div></div>