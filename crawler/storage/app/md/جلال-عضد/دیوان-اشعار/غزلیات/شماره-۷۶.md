---
title: >-
    شمارهٔ  ۷۶
---
# شمارهٔ  ۷۶

<div class="b" id="bn1"><div class="m1"><p>صبا ز زلف تو بویی به عاشقان آورد</p></div>
<div class="m2"><p>نسیم آن به تن رفته باز جان آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار جان سزد از مژده گر به باد دهند</p></div>
<div class="m2"><p>که نزد دلشدگان بوی دلستان آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر ز چین سر زلف مشکبوی تو داد</p></div>
<div class="m2"><p>صبا چو از دل گمگشته ام نشان آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نه جان عزیزی چرا دمی بی تو</p></div>
<div class="m2"><p>به کام دل نفسی بر نمی توان آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم ز لطف تو رمزی به گوش جان می گفت</p></div>
<div class="m2"><p>ز شوق مردم چشم آب در دهان آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار بوسه لبم زد ز شوق بر دهنم</p></div>
<div class="m2"><p>از آن که نام دهان تو بر زبان آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز وصل تو کمرت همچو من به هیچ برست</p></div>
<div class="m2"><p>وگرچه با تو پدر دست در میان آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز اشک چهره من هست دشت رود آور</p></div>
<div class="m2"><p>عجب نباشد اگر باد زعفران آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دست هجر تو بر جان بی قرارم زد</p></div>
<div class="m2"><p>هر آن خدنگ که ایّام در کمان آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی به حضرت تو قرب یافت همچو جلال</p></div>
<div class="m2"><p>که روی خود به سوی راست ترجمان آورد</p></div></div>