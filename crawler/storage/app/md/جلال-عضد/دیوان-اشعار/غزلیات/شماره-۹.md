---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>زلفی که چو دود است بر آتش وطن او را</p></div>
<div class="m2"><p>مشکل نبود در دلم آتش زدن او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعی که دو عالم به یکی شعله بسوزد</p></div>
<div class="m2"><p>کی غم بود از سوختن صد چو من او را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بلبل شوریده خبر یابد از آن گل</p></div>
<div class="m2"><p>دیگر نتوان یافتن اندر چمن او را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور غنچه کند دعوی تنگی بِنَماند</p></div>
<div class="m2"><p>پیش دهن دوست مجال سخن او را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی دل ما را شکند زلف سیاهش</p></div>
<div class="m2"><p>ای باد صبا می رو و سر می شکن او را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درّی ست گرانمایه که در هیچ نگنجد</p></div>
<div class="m2"><p>گر زان که نگنجد سخن اندر دهن او را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لعل تو شکر داد به خروار به هر کس</p></div>
<div class="m2"><p>عار است مگر، دادن شکر به من او را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخسار تو شمعی ست که چون شعله برآرد</p></div>
<div class="m2"><p>پروانه بود شمع زمرّد لگن او را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی جان جلال از کف محنت به درآید</p></div>
<div class="m2"><p>تا زلف پریشان تو باشد وطن او را</p></div></div>