---
title: >-
    شمارهٔ  ۱۶۵
---
# شمارهٔ  ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>مدّعی در عشق او گر طعنه زد بر من چه باک</p></div>
<div class="m2"><p>طالبان دوست را از طعنه دشمن چه باک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سیاهی را که دارد جامه پاک از طعنه نیست</p></div>
<div class="m2"><p>لاله را از ده زبانی کردن سوسن چه باک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان را هر دو عالم گرد خاطر گو مگرد</p></div>
<div class="m2"><p>گر نگردد شمع را پروانه پیرامن چه باک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمنان و دوستان کردند بر من پشت، لیک</p></div>
<div class="m2"><p>گر بود روی عنایت دوست را با من چه باک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار در دل دارم و بر دوختم ز اغیار چشم</p></div>
<div class="m2"><p>خانه پرنور است اگر تاریک شد روزن چه باک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سلامت ترک کردیم از ملامت باک نیست</p></div>
<div class="m2"><p>هر که را در دیده پیکان است از سوزن چه باک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که در باطن چو غنچه مهر دارم سر به مُهر</p></div>
<div class="m2"><p>گر به ظاهر می درم بر خویش پیراهن چه باک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه فردای قیامت دامنش خواهم گرفت</p></div>
<div class="m2"><p>گر بود خون مَنش امروز در گردن چه باک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در میان ما و او پیوند جانی رفته است</p></div>
<div class="m2"><p>جان اگر عزم جدایی می کند از تن چه باک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاک خون دل ز راه دیده بر دامن چکید</p></div>
<div class="m2"><p>چون دلم شد پاک اگر آلوده شد دامن چه باک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر جلال از گنج وصلش یافت مقصودی چه شد</p></div>
<div class="m2"><p>خوشه‌چینی خوشه‌ای گر بُرد از خرمن چه باک</p></div></div>