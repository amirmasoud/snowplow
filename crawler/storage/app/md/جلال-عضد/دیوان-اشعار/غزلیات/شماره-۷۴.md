---
title: >-
    شمارهٔ  ۷۴
---
# شمارهٔ  ۷۴

<div class="b" id="bn1"><div class="m1"><p>هوای باده و آهنگ جام خواهم کرد</p></div>
<div class="m2"><p>به کوی باده فروشان مقام خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک کوی خرابات و آب دیده جام</p></div>
<div class="m2"><p>مقاصد دو جهانی تمام خواهم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صبح و شام نخواهم نهاد جام از دست</p></div>
<div class="m2"><p>شراب خوارگیی بر دوام خواهم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جور دور دلم ساغری ست پرخوناب</p></div>
<div class="m2"><p>دوای او به می لعل فام خواهم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بریختم به ستم خون پیر جام و کنون</p></div>
<div class="m2"><p>هر آنچه هست مرا وقف جام خواهم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرابخانه چو دار سلامت است مرا</p></div>
<div class="m2"><p>طواف گلشن دارالسّلام خواهم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلال باد مرا می که بعد از او بر خود</p></div>
<div class="m2"><p>نعیم دنیی و عقبی حرام خواهد کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازین خیال که بیهوده در سرم پیداست</p></div>
<div class="m2"><p>گه عمر در سر سودای خام خواهم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جام عشق تو مدهوش و مست خواهم بود</p></div>
<div class="m2"><p>ز خاک چون به قیامت قیام خواهم کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین که شرط ثبات و وفا به جای آورد</p></div>
<div class="m2"><p>جلال را سگ کوی تو نام خواهم کرد</p></div></div>