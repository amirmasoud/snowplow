---
title: >-
    شمارهٔ  ۱۷۷
---
# شمارهٔ  ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>صبحدم بر بوی جانان سوی گلزار آمدم</p></div>
<div class="m2"><p>همچو بلبل پیش گل در ناله زار آمدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ببینم نرگس و سنبل چو چشم و زلف او</p></div>
<div class="m2"><p>این چنین سرمست و آشفته به بازار آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جفای غمزه مستش سوی گل با خروش</p></div>
<div class="m2"><p>از درِ بُستان شدم در کوی خمّار آمدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حُسن روی او طلب کردم به بتخانه شدم</p></div>
<div class="m2"><p>تار زلفش دَر گرفتم پیش زنّار آمدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه زن شد حُسن جانان از همه سو بر جلال</p></div>
<div class="m2"><p>من که بودم تا برین دولت سزاوار آمدم</p></div></div>