---
title: >-
    شمارهٔ  ۱۸۱
---
# شمارهٔ  ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>در آرزوی وصالت اگرچه با غم و دردم</p></div>
<div class="m2"><p>امیدوار چنانم که ناامید نگردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم چو شمع که هر شب ز سوز هجر تو تا روز</p></div>
<div class="m2"><p>سرشک گرم فرو می رود به چهره زردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن زمان که مرا بخت خفته از تو جدا کرد</p></div>
<div class="m2"><p>نماند شربت دردی که از زمانه نخوردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سهیل هجر برآمد، مه وصال فرو شد</p></div>
<div class="m2"><p>ز روز تیره فزون شد درازی شب دردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اشتیاق جمالت چه سیل ها که نراندم</p></div>
<div class="m2"><p>ز روزگار وصالت چه یادها که نکردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار ناله برآرم ز دست هجر تو هر روز</p></div>
<div class="m2"><p>هزار قطره ببارم به یاد وصل تو هر دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب فراقت ازین سان که بوی صبح ندارد</p></div>
<div class="m2"><p>چراغ صبح همانا بمرد از دم سردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین که هجر تو گَرد از من شکسته برآورد</p></div>
<div class="m2"><p>مگر نسیم صبا آورد به کوی تو گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیال روی تو همواره همنشین جلال است</p></div>
<div class="m2"><p>که با خیال تو جفتم گر از وصال تو فردم</p></div></div>