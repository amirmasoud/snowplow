---
title: >-
    شمارهٔ  ۲۴۹
---
# شمارهٔ  ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>ای ز نور رخت افتاده به شک پروانه</p></div>
<div class="m2"><p>شمع رخسار ترا شمع فلک پروانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدسیان باز گرفتند ز رویت شمعی</p></div>
<div class="m2"><p>جور فرّاش شد آن را و ملک پروانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع رخسار تو یک نوبت اگر شعله زدی</p></div>
<div class="m2"><p>بگرفتی ز سما تا به سمک پروانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار دل راست کن ای دوست به یک پروانه</p></div>
<div class="m2"><p>کار شمعی نشود راست به یک پروانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع بنهاده و پروانه شده مایل تو</p></div>
<div class="m2"><p>گویی افتاد ز روی تو به شک پروانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش آن چهره نباشد عجب ای شمع اگر</p></div>
<div class="m2"><p>کند از صفحه دل مهر تو حک پروانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع آتش شد و آتش محک عاشق از آنک</p></div>
<div class="m2"><p>می زند قلب دل خود به محک پروانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع دولت بفروزد دگر اقبال جلال</p></div>
<div class="m2"><p>گر دهد لعل تو او را به نمک پروانه</p></div></div>