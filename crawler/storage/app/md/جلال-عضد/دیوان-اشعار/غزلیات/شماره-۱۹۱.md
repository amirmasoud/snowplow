---
title: >-
    شمارهٔ  ۱۹۱
---
# شمارهٔ  ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>در پایت اوفتادم ای دوست! دست گیرم</p></div>
<div class="m2"><p>می کن بزرگی خود منگر که من حقیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شمع جمع خوبان! پروانه وار روزی</p></div>
<div class="m2"><p>گرد سرت بگردم در پیش پات میرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست چشم و زلفت پیش که داد خواهم؟</p></div>
<div class="m2"><p>[این] می کشد به بندم [وان] می کشد به تیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیز ای طبیب نادان! ترک معالجت کن</p></div>
<div class="m2"><p>من درد عشق دارم درمان نمی پذیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کشته تو گردم گر بگذری به خاکم</p></div>
<div class="m2"><p>از خاک سر برآرم تا دامنت بگیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دوستان! مجویید از بند او خلاصم</p></div>
<div class="m2"><p>آزادم از دو عالم تا در کفش اسیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی از تو برنچینم گر می کشی به تیغم</p></div>
<div class="m2"><p>چشم از تو برندوزم گر می زنی به تیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من ترک او نگویم ور جان رَود درین سر</p></div>
<div class="m2"><p>کز جان گزیر باشد وز دوست ناگزیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر شب به خار و خارا غلتم ز درد دوری</p></div>
<div class="m2"><p>وز شوق وصل گویی پهلوست بر حریرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا از جلال دوری دوری نکرد یک دم</p></div>
<div class="m2"><p>نام تو از زبانم، یاد تو از ضمیرم</p></div></div>