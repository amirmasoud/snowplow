---
title: >-
    شمارهٔ  ۸۰
---
# شمارهٔ  ۸۰

<div class="b" id="bn1"><div class="m1"><p>باد صبا به نافه چینت نمی رسد</p></div>
<div class="m2"><p>بویی به عاشقان غمینت نمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک توایم و چشم تو بر ما نمی فتد</p></div>
<div class="m2"><p>ماهی و پرتوی به زمینت نمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمعی که آسمان و زمین زو منوّر است</p></div>
<div class="m2"><p>در روشنی به عکس جبینت نمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که کام دل بستانم ز لعل تو</p></div>
<div class="m2"><p>دستم به پسته شکرینت نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دُرج لعل دوست مگر خاتم جمی</p></div>
<div class="m2"><p>زین سان که دست کس به نگینت نمی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز ترا چنان که تویی کس نشان نداد</p></div>
<div class="m2"><p>پای گمان به حدّ یقینت نمی رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای زلف دوست! بر رخ او مسکنت چراست</p></div>
<div class="m2"><p>تو کافری بهشت برینت نمی رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفتی مپوی در پی رندان که امر و نهی</p></div>
<div class="m2"><p>بر عاشقان بی دل و دینت نمی رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل ! خوش است ناله بلبل ز شوق گل</p></div>
<div class="m2"><p>لیکن به ناله های حزینت نمی رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با خار غم بساز اگرت گل به دست نیست</p></div>
<div class="m2"><p>کز گلشن زمانه جُزینت نمی رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بردی جلال گوی فصاحت ز روزگار</p></div>
<div class="m2"><p>شعر کسی به نظم متینت نمی رسد</p></div></div>