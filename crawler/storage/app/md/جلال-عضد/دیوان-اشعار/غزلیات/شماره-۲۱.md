---
title: >-
    شمارهٔ  ۲۱
---
# شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>دلم متاب که هجران سینه تاب بس است</p></div>
<div class="m2"><p>چه دورم از رخ خوبت همین عذاب بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان دو حلقه که حلق دلم همی تابی</p></div>
<div class="m2"><p>چو جان ز حلق برآمد دگر متاب بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درون حلقه دلم تابه کی ز خون جگر</p></div>
<div class="m2"><p>بیاورید کبابی مرا شراب بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز با غم و بخت بدم قرین گرچه</p></div>
<div class="m2"><p>مرا ز دوری رویت ز خورد و خواب بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب وصال تو گو مه متاب بر گردون</p></div>
<div class="m2"><p>به ماهتاب چه حاجت که آفتاب بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمت به قصد خرابی جان کمر در بست</p></div>
<div class="m2"><p>چو کرد خانه معمور دل خراب بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محیط و قلزم سوز جلال را نکشد</p></div>
<div class="m2"><p>نه آتشی ست که او را دو قطره آب بس است</p></div></div>