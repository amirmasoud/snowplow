---
title: >-
    شمارهٔ  ۹۴
---
# شمارهٔ  ۹۴

<div class="b" id="bn1"><div class="m1"><p>گدای کوی تو از پادشا نیندیشد</p></div>
<div class="m2"><p>که پادشاه ز حال گدا نیندیشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که عاشقم از ترک سر چه اندیشه</p></div>
<div class="m2"><p>اسیر عشق ز تیر قضا نیندیشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کند به ملامت جزای بی خبران</p></div>
<div class="m2"><p>که از ملامت روز جزا نیندیشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خود فرو روم از فکر زلف تو هر شب</p></div>
<div class="m2"><p>که هیچ کس نبود کز بلا نیندیشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه باک چشم ترا گر بریخت خون دلم</p></div>
<div class="m2"><p>که مست عربده جو از خطا نیندیشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضرورت است بر آن کس که عشق می بازد</p></div>
<div class="m2"><p>که از تحمّل بار جفا نیندیشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که روی تو بیند دعا کند زیرا</p></div>
<div class="m2"><p>که پیش قبله کسی جز دعا نیندیشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسان غمزه تو نیست ظالمی دیگر</p></div>
<div class="m2"><p>که خون خلق خورد و از خدا نیندیشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلال را همه اندیشه از بداندیش است</p></div>
<div class="m2"><p>تو چاره ایش بیندیش تا نیندیشد</p></div></div>