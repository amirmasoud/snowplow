---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>پیش تو عرضه می کنم حال تباه خویش را</p></div>
<div class="m2"><p>تا تو نصیحتی کنی چشم سیاه خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرزنشم مکن که تو، شیفته تر ز من شوی</p></div>
<div class="m2"><p>بینی اگر به ناگهان چشم سیاه خویش را</p></div></div>