---
title: >-
    شمارهٔ  ۲۰۳
---
# شمارهٔ  ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>روزی کزین سراچه سفلی گذر کنم</p></div>
<div class="m2"><p>وانگه به سوی عالم علوی سفر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرّوبیان عرش و مقیمان قدس را</p></div>
<div class="m2"><p>از درد خویش و حسن تو یک یک خبر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گریه فرش را همه در موج خون کشم</p></div>
<div class="m2"><p>وز ناله عرش را همه زیر و زبر کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نار جحیم را بازنشانم به آب چشم</p></div>
<div class="m2"><p>یک بار چون برآتش دوزخ گذر کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وآنگه چنان ز درد تو آهی برآورم</p></div>
<div class="m2"><p>کاهْ لِ بهشت را همه خونین جگر کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر من نعیم جنّت اعلی کنند عرض</p></div>
<div class="m2"><p>کوته نظر نِیَم که بر آنها نظر کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نو درون خاطر خود جا کنم ترا</p></div>
<div class="m2"><p>و آنها که غیر تُست ز خاطر بِدَر کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در روز رستخیز که سر بر کنم ز خاک</p></div>
<div class="m2"><p>نامَردم ار ز هول قیامت حذر کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عرصه گاه حشر درآیم خراب و مست</p></div>
<div class="m2"><p>شوریده وار رایت عشق تو بر کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر لحظه ساز عشق به سوزی دگر زنم</p></div>
<div class="m2"><p>هر دم سماع شوق به حالی دگر کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن دم جمال اگر بنمایی جلال را</p></div>
<div class="m2"><p>یابم کمال وصل و سخن مختصر کنم</p></div></div>