---
title: >-
    شمارهٔ  ۱۹۴
---
# شمارهٔ  ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>همی خواهم که تا من زنده باشم</p></div>
<div class="m2"><p>تو سلطان باشی و من بنده باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز غم مُردم که جان دیگرانی</p></div>
<div class="m2"><p>به جان دیگران چون زنده باشم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روا نبود که جان داروی لعلت</p></div>
<div class="m2"><p>برند اغیار و من جان کنده باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برین در من چو سروم دیگران گل</p></div>
<div class="m2"><p>روند ایشان و من پاینده باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزن آبی بر این دل ورنه بینی</p></div>
<div class="m2"><p>که آتش در جهان افکنده باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسان غنچه ام در بند ناموس</p></div>
<div class="m2"><p>که دل پرخون و لب پرخنده باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمیرم چون جلال الا به دردت</p></div>
<div class="m2"><p>اگر با طالعی فرخنده باشم</p></div></div>