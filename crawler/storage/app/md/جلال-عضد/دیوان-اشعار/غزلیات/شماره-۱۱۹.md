---
title: >-
    شمارهٔ  ۱۱۹
---
# شمارهٔ  ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>دل از بند زلفت رها کی شود</p></div>
<div class="m2"><p>ز یار قدیمی جدا کی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگویی که از لعل سیراب تو</p></div>
<div class="m2"><p>مراد دل ما روا کی شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی مرهم لعل خود کام تو</p></div>
<div class="m2"><p>به کام دل ریش ما کی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی دانم این جان پُر درد خویش</p></div>
<div class="m2"><p>که با خرّمی آشنا کی شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدین عمرِ کم کی توان یافت وصل</p></div>
<div class="m2"><p>که وصل رخش کم بها کی شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی شد دل از بند زلفش رها</p></div>
<div class="m2"><p>کنون دل نهادیم تا کی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدنگ قضا هم خطا می شود</p></div>
<div class="m2"><p>ولی ناوک او خطا کی شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی دانم این رند مدهوش مست</p></div>
<div class="m2"><p>به کام دل پارسا کی شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا همدم یار گردی جلال</p></div>
<div class="m2"><p>که شه همنشین گدا کی شود</p></div></div>