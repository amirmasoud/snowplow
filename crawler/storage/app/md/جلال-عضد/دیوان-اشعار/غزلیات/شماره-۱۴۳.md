---
title: >-
    شمارهٔ  ۱۴۳
---
# شمارهٔ  ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>دردی از هجر تو دیدم، که ندیدم هرگز</p></div>
<div class="m2"><p>و آنچه این بار کشیدم، نکشیدم هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کامم این بود که در پای تو میرم روزی</p></div>
<div class="m2"><p>مُردم از حسرت و این کام ندیدم هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر تو بس که شنیدم سخن ناخوش خلق</p></div>
<div class="m2"><p>وز تو روزی سخن خوش نشنیدم هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که را حال نکو بود به کامی برسید</p></div>
<div class="m2"><p>من بدحال به کامی نرسیدم هرگز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغبانا! مکن از گوشه باغم بیرون</p></div>
<div class="m2"><p>که من از باغ تو یک میوه نچیدم هرگز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم آن بلبل عاشق که چو مرغ خانه</p></div>
<div class="m2"><p>بر درت ماندم و جایی نپریدم هرگز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان شیرین ز فراقت به لب آمد چو جلال</p></div>
<div class="m2"><p>کز می وصل تو جامی نچشیدم هرگز</p></div></div>