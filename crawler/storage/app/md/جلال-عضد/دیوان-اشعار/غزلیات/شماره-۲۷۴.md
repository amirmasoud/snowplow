---
title: >-
    شمارهٔ  ۲۷۴
---
# شمارهٔ  ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>جهان پیر را نو شد جوانی</p></div>
<div class="m2"><p>منه ساغر ز دست ار می توانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون هر روز بستان می کند عرض</p></div>
<div class="m2"><p>به روی دوستان گنج نهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شقایق از سیاهی می درخشد</p></div>
<div class="m2"><p>چو از ابر سیه برق یمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو از دوران گل دستور خود ساز</p></div>
<div class="m2"><p>که بنیادی ندارد زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرادی از بهار عمر برگیر</p></div>
<div class="m2"><p>که ناگه در رسد باد خزانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماند آن روز و آن دولت که با یار</p></div>
<div class="m2"><p>همی کردیم عیش رایگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به وصل روی یکدیگر شب و روز</p></div>
<div class="m2"><p>ممتّع گشته از عمر و جوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا از یار دور افکند ایّام</p></div>
<div class="m2"><p>چه چاره با قضای آسمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلال! احوال کس در هیچ حالی</p></div>
<div class="m2"><p>به یک حالت نماند جاودانی</p></div></div>