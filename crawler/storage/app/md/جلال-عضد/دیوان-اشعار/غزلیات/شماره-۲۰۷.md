---
title: >-
    شمارهٔ  ۲۰۷
---
# شمارهٔ  ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>در هجر تو زین گونه که بی صبر و سکونم</p></div>
<div class="m2"><p>دادند همه خلق گواهی به جنونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران ز من دل شده پرسند که چونی؟</p></div>
<div class="m2"><p>من بیخودم از خود خبرم نیست که چونم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهم که به چنگ آورم آن زلف نگونسار</p></div>
<div class="m2"><p>اینست که یاری ندهد بخت نگونم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر من گذری کن که به دیدار جمالت</p></div>
<div class="m2"><p>ز اندازه برون است تمنّای درونم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناخورده یکی جرعه ز سرچشمه نوشَت</p></div>
<div class="m2"><p>چشم تو چرا گشت چنین تشنه به خونم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل درشکن زلف پریشان تو بستم</p></div>
<div class="m2"><p>زیرا نه قرار است ازین پس نه سکونم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ظلم که بتواند و هر جور که باشد</p></div>
<div class="m2"><p>با من کند ایّام که دیوار زبونم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پروانه رخسار چو شمع تو جلال است</p></div>
<div class="m2"><p>چون سوختی از چشم مینداز کنونم</p></div></div>