---
title: >-
    شمارهٔ  ۲۶۰
---
# شمارهٔ  ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>در زمستان بر امید آنکه باز آید بهاری</p></div>
<div class="m2"><p>عاشق گل را بباید ساختن با نوک خاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستان پرسند کآخر در چه کاری در چه کارم</p></div>
<div class="m2"><p>می گذارم عمر خود را بر امید انتظاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بارها بار فراقت برده‌ام بر گردن جان</p></div>
<div class="m2"><p>من بدین سختیّ و دشواری ندیدم هیچ باری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمگساری هست هر کس را به روز شادی و غم</p></div>
<div class="m2"><p>وای من کاندر غمت جز غم ندارم غمگساری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش رویت نیست غایب یک زمان از پیش چشمم</p></div>
<div class="m2"><p>لاله زین سان بر نروید بر کنار جویباری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دهی تشریف در پایت فشانم جان و دل</p></div>
<div class="m2"><p>برنخیزد بیش ازین از دست درویشی نثاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک راهت شد جلال و پیش خود راهش ندادی</p></div>
<div class="m2"><p>باد را گر هست راهی خاک ره را نیست باری</p></div></div>