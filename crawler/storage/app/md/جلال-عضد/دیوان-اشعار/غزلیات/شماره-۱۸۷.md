---
title: >-
    شمارهٔ  ۱۸۷
---
# شمارهٔ  ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>به جز ذکر لبت کاری ندارم</p></div>
<div class="m2"><p>به یادت عمر شیرین می گذارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو چشم ناتوانت ناتوانم</p></div>
<div class="m2"><p>چو زلف بی قرارت بی قرارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دیده خون دل تا کی فشانم</p></div>
<div class="m2"><p>ز مژگان سیل خون تا چند بارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر روزی ببینی زاری من</p></div>
<div class="m2"><p>کنی هم رحمتی بر حال زارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه دشمن جان و دلی تو</p></div>
<div class="m2"><p>ز جان و دل هنوزت دوست دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چشمم اختر افشانی عجب نیست</p></div>
<div class="m2"><p>که شب تا روز اختر می شمارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین وادی که گرد از من برآید</p></div>
<div class="m2"><p>مگر سوی تو باد آرد غبارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر صد نوبت از پیشم برانی</p></div>
<div class="m2"><p>به الطافت هنوز امّیدوارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلال خسته را دادی امیدی</p></div>
<div class="m2"><p>کنون عمری ست تا در انتظارم</p></div></div>