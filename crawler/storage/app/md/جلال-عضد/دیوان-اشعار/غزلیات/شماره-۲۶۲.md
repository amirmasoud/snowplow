---
title: >-
    شمارهٔ  ۲۶۲
---
# شمارهٔ  ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>بکوش تا دل آزرده ای به دست آری</p></div>
<div class="m2"><p>که اهل دل نپسندند مردم آزاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سود عهده عهدی که می کنی با من</p></div>
<div class="m2"><p>تو عهد می کنی امّا به جا نمی آری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو یار منی دور شو ز اغیارم</p></div>
<div class="m2"><p>نه دوستی ست که با دشمنان کنی یاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دور نرگس مخمور باده پیمایت</p></div>
<div class="m2"><p>ز روزگار برافتاد نام هشیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو حال دوش ز من پرس از آنکه تا به سحر</p></div>
<div class="m2"><p>ترا به خواب گذشت و مرا به بیداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ظاهرم نگری سرّ سینه کی دانی</p></div>
<div class="m2"><p>که پاره های دل است این که اشک پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز با همه سختی امید می دارم</p></div>
<div class="m2"><p>که هم به روز مبدّل شود شب تاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه بر سر خاکم نشانده ای سهل است</p></div>
<div class="m2"><p>امید هست که بازم ز خاک برداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا که در قدمت جان دهم به آسانی</p></div>
<div class="m2"><p>که در فراق تو جان می دهم به دشواری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جلال! زور و زر و زاری است چاره وصل</p></div>
<div class="m2"><p>چو زور و زر نبود چاره نیست جز زاری</p></div></div>