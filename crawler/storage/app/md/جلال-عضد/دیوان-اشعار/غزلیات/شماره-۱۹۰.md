---
title: >-
    شمارهٔ  ۱۹۰
---
# شمارهٔ  ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>بخت برگشت ز من تا تو برفتی ز برم</p></div>
<div class="m2"><p>کی بود باز که چون بخت درآیی ز درم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از این یک نفسم بی تو نمی رفت به سر</p></div>
<div class="m2"><p>بعد ازین تا ز فراق تو چه آید به سرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم احوال دل خویش نگویم با کس</p></div>
<div class="m2"><p>لیکن از بی خبری رفت به عالم خبرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان سپر ساخته ام ناوک مژگان ترا</p></div>
<div class="m2"><p>تا همه خلق بدانند که من جان سپرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی گل روی تو چون غنچه دلم تنگ آمد</p></div>
<div class="m2"><p>بیم آنست که بر خویش گریبان بدرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو گفتم که به بالای تو ماند امروز</p></div>
<div class="m2"><p>زهره ام نیست کزین شرم به بالا نگرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خود می طلبم باز و یقین می دانم</p></div>
<div class="m2"><p>که من از دست تو گر دل ببرم، جان نبرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترک دنیا کنم ار سوی خودم راه دهی</p></div>
<div class="m2"><p>کو سر کوی تو تا من ز جهان در گذرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا خیال رخ خوب تو مرا در نظر است</p></div>
<div class="m2"><p>می نیاید به حقیقت دو جهان در نظرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخت هجر تو اثر کرد و از آن می ترسم</p></div>
<div class="m2"><p>که در اندوه فراق تو نماند اثرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به صبوری نتوان کرد مداوای جلال</p></div>
<div class="m2"><p>بِهی ام نیست که هر روز که آید بترم</p></div></div>