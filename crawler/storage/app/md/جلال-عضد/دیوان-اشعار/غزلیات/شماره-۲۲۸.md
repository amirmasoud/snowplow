---
title: >-
    شمارهٔ  ۲۲۸
---
# شمارهٔ  ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>ای بت سرو قد سیمین تن</p></div>
<div class="m2"><p>وی گل نوبهار و سرو چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دهی حسن خویش را تزیین</p></div>
<div class="m2"><p>نکته ای گوش کن لطیف از من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرّه را سر ببُر بگردانش</p></div>
<div class="m2"><p>در میانش میان پنج افکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمع کن طرّه پریشان را</p></div>
<div class="m2"><p>بر سر طرّه خال عنبرزن</p></div></div>