---
title: >-
    شمارهٔ  ۹۷
---
# شمارهٔ  ۹۷

<div class="b" id="bn1"><div class="m1"><p>من سرو ندیدم که به بالای تو ماند</p></div>
<div class="m2"><p>بالای تو سروی ست که گل می شکفاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار که این عاشق دلسوخته بی تو</p></div>
<div class="m2"><p>یک لحظه بماند که به یک لحظه نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترسم که به کام دل دشمن بنشینم</p></div>
<div class="m2"><p>تا آن که فلک با تو به کامم بنشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد که از تشنگی ام جان به لب آمد</p></div>
<div class="m2"><p>کس نیست که آبی به لب تشنه چکاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم که یک امشب که تو در خانه مایی</p></div>
<div class="m2"><p>از بوی سر زلف تو همسایه بداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریاد که بیداد ز حد بردی و از تو</p></div>
<div class="m2"><p>فریاد رسی نیست که دادم بستاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت است که بیدار شود دیده بختم</p></div>
<div class="m2"><p>وز چنگ غم و درد و عذابم برهاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانه در سلسله گر بوی تو یابد</p></div>
<div class="m2"><p>دیوانه شود سلسله در هم گسلاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشتاق پریشان که دلش پیش تو باشد</p></div>
<div class="m2"><p>خواهد که کند صبر ولیکن نتواند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسان شود این مشکل و روشن شود این شب</p></div>
<div class="m2"><p>کاحوال جهان جمله به یک حال نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مانند جلال آنکه به سختی بنهد دل</p></div>
<div class="m2"><p>هم عاقبتش بخت به مقصود رساند</p></div></div>