---
title: >-
    شمارهٔ  ۱۰۳
---
# شمارهٔ  ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>آن را که غمی باشد و گفتن نتواند</p></div>
<div class="m2"><p>شب تا به سحر نالد و خفتن نتواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما بشنو قصّه ما ورنه چه حاصل</p></div>
<div class="m2"><p>پیغام که باد آرد و گفتن نتواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی بوی وصالت نگشاید دل تنگم</p></div>
<div class="m2"><p>بی باد صبا غنچه شکفتن نتواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از اشک زدم آب همه کوی تو تا باد</p></div>
<div class="m2"><p>خاشاک سر کوی تو رُفتن نتواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوریده تواند که کند ترک سر خویش</p></div>
<div class="m2"><p>ترک سر زلف تو گرفتن نتواند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر دل ما عکس رخ خوب تو پیداست</p></div>
<div class="m2"><p>در آینه کس چهره نهفتن نتواند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوینده چه سختی ست که بر خود نکند سهل</p></div>
<div class="m2"><p>فرهاد چه سنگ است که سفتن نتواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن شد که جلال از سر کوی تو شود دور</p></div>
<div class="m2"><p>کز ضعف چنان است که رفتن نتواند</p></div></div>