---
title: >-
    شمارهٔ  ۱۱۸
---
# شمارهٔ  ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>مرا خیال وصالش ز سر به در نرود</p></div>
<div class="m2"><p>اگر سرم برود عشق او ز سر نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من این معاینه با خود به خاک خواهم برد</p></div>
<div class="m2"><p>که حسرت است که هرگز ز دل به در نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دست هجر تو یک روز نگذرد که مرا</p></div>
<div class="m2"><p>هزار غصّه و خونابه در جگر نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب صرف محبّت حرام باد بر آن</p></div>
<div class="m2"><p>که چون رود ز جهان مست و بی خبر نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار سال بخسبم به زیر خاک و هنوز</p></div>
<div class="m2"><p>ز لوح سینه من نقش آن به سر نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تیغ و سنان قصد جان کند محبوب</p></div>
<div class="m2"><p>محبّت از دل عاشق بی غم و غصه نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رواست کز همه عالم نظر فروبندم</p></div>
<div class="m2"><p>که نقش آنکه بود در دل از نظر نرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث روضه رضوان مکن که خاطر را</p></div>
<div class="m2"><p>ز کوی دوست به سر منزل دگر نرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوز خرمن عمرت جلال از آتش عشق</p></div>
<div class="m2"><p>که خرمنی که بسوزد به باد بر نرود</p></div></div>