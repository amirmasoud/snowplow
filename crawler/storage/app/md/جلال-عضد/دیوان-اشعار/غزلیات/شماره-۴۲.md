---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>به بالین غریبانت گذر نیست</p></div>
<div class="m2"><p>ز حال مستمندانت خبر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تو پروای هستی نیست ما را</p></div>
<div class="m2"><p>تو را پروای ما گر هست وگر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی منظور من در هر دو عالم</p></div>
<div class="m2"><p>مرا بر دنیی و عقبی نظر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکایک تلخی دوران چشیدم</p></div>
<div class="m2"><p>ز هجران هیچ شربت تلخ تر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر هجر و نومید از وصالم</p></div>
<div class="m2"><p>شبم تاریک و امّید سحر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی خواهم که رویت باز بینم</p></div>
<div class="m2"><p>جز اینم در جهان کامی دگر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلی خالی نمی بینم ز دردت</p></div>
<div class="m2"><p>کدامین دل که خونش در جگر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین میدان سرافرازی کسی راست</p></div>
<div class="m2"><p>که او را بیم جان و خوف سر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جز جانان نیاید در دل ما</p></div>
<div class="m2"><p>که خلوتخانه است این رهگذر نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخ و چشم تو تا غایب شد از چشم</p></div>
<div class="m2"><p>من شوریده دل را خواب و خور نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جلال خسته را از در مکن دور</p></div>
<div class="m2"><p>که او را خود جز این در هیچ در نیست</p></div></div>