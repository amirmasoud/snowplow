---
title: >-
    شمارهٔ  ۹۱
---
# شمارهٔ  ۹۱

<div class="b" id="bn1"><div class="m1"><p>جانم از عشق تو بی صبر و سکون خواهد شد</p></div>
<div class="m2"><p>دلم از آتش سودای تو خون خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من تنها نه که با حسن و جمالی که تراست</p></div>
<div class="m2"><p>عالمی در کف عشق تو زبون خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من آن زلف تو شوریده نمی شد و اکنون</p></div>
<div class="m2"><p>گوییا بخت منش راهنمون خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دم آید ز هوای دو لبت جان بر لب</p></div>
<div class="m2"><p>تا سرانجامِ من از عشق تو چون خواهد شد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسا شب که ز تاب غم هجران رخت</p></div>
<div class="m2"><p>آه من بر فلک آینه گون خواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان سر زلف چو زنجیر معنبر که تراست</p></div>
<div class="m2"><p>عاقلان را همگی میل جنون خواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل جان من از شوق گلستان رخت</p></div>
<div class="m2"><p>ناگهان از قفس سینه برون خواهد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داستان غم تو کم نشود در عالم</p></div>
<div class="m2"><p>کاین حدیثی ست که هر روز فزون خواهد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ازل با رخ تو عشق همی باخت جلال</p></div>
<div class="m2"><p>جان او بسته سودا نه کنون خواهد شد</p></div></div>