---
title: >-
    شمارهٔ  ۲۲۶
---
# شمارهٔ  ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>امروز درین شهر هر آن دل که شود گم</p></div>
<div class="m2"><p>در حلقه گیسوی تو باید طلبیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سرو ثبات قدمی گیر که رشته ست</p></div>
<div class="m2"><p>چون بلبل ازین شاخ بدان شاخ پریدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باید قدمی بر سر هستی زدن اوّل</p></div>
<div class="m2"><p>وانگه رقم عشق تو بر خویش کشیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس هوسی دارد و مقصود و مرادی</p></div>
<div class="m2"><p>مقصود من از دوست به مقصود رسیدن</p></div></div>