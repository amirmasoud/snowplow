---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>حدیث عشق میسّر کجا شود به کتابت</p></div>
<div class="m2"><p>که نام عشق بسوزد سر قلم ز مهابت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی سعادت آن کس که پای بند کسی شد</p></div>
<div class="m2"><p>بریده از همه پیوند و خویش و اهل و قرابت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شب رسید دگر بار روزم از غم هجران</p></div>
<div class="m2"><p>بسان تیغ شود موی بر تنم ز صلابت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآر کامم از آن لعل بی کرشمه و ابرو</p></div>
<div class="m2"><p>که خود سؤال گدا را چه حاجت است کتابت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهد فروغ جمال جهان فروز تو هر روز</p></div>
<div class="m2"><p>جهان فروزی خود را به آفتاب نیابت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان عاشق و معشوق شهوتی ست نظر را</p></div>
<div class="m2"><p>کز آب دیده خونین کنند غسل جنابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلال اگرچه همیشه دعای وصل تو خواند</p></div>
<div class="m2"><p>ولی چه سود که مقرون نمی شود به اجابت</p></div></div>