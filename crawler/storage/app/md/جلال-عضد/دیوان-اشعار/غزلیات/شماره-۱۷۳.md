---
title: >-
    شمارهٔ  ۱۷۳
---
# شمارهٔ  ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>رسید وقت صبوحی بیار ساقی جام</p></div>
<div class="m2"><p>به یاد بزم صبوحی کشان دُرد آشام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به طرفِ باغ برآرای بزم چون فردوس</p></div>
<div class="m2"><p>که باد صبح ز فردوس می دهد پیغام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آتش دل ما ریز آبِ چون آتش</p></div>
<div class="m2"><p>که کار سوختگان پخته گردد از می خام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیار باده که ارباب ذوق را به صبوح</p></div>
<div class="m2"><p>گشایشی نشود جز ز پیر خلوت جام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقام عشق مگو با خرد که ره نبرد</p></div>
<div class="m2"><p>درون بزمگه خاصّ و عام کالانعام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقام عالی اگر جویی از خرابی جوی</p></div>
<div class="m2"><p>که ما به کوی خرابات یافتیم مقام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از ملامت مردم ندارم اندیشه</p></div>
<div class="m2"><p>مراد چون نتوان یافت از ملال و ملام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که کام دل از زمانه می خواهد</p></div>
<div class="m2"><p>همین که باده به کامش رسد رسید به کام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل جلال به زنجیر عشق دربند است</p></div>
<div class="m2"><p>که کی خلاص شود مرغ بال بسته به دام</p></div></div>