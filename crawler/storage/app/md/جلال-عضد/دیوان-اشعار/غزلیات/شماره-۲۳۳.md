---
title: >-
    شمارهٔ  ۲۳۳
---
# شمارهٔ  ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>چه نکهت است مگر بوی بوستان است این</p></div>
<div class="m2"><p>چه دولت است مگر روی دوستان است این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علاج این تن رنجور ناتوان است آن</p></div>
<div class="m2"><p>دوای این دل مهجور پر فغان است این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب که جوشش صفرای عشق افزون است</p></div>
<div class="m2"><p>ز اشک دیده که مانند ناودان است این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برفت بلبل شیدا چو من به طرف چمن</p></div>
<div class="m2"><p>ز دست دوست به دستان چه داستان است این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون کف من و جام شراب، ای زاهد!</p></div>
<div class="m2"><p>مراست سود در آن گر ترا زیان است این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشا کسی که به غفلت ز دست نگذارد</p></div>
<div class="m2"><p>عنان عمر که با باد هم عنان است این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن که دید به فصل بهار آه مرا</p></div>
<div class="m2"><p>گمان برد که مگر موسم خزان است این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که را فرستم تا با لبش سخن گوید</p></div>
<div class="m2"><p>مگر نسیم سَحَر را که کار جان است این</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلال! طرف گلستان و صحبت یاران</p></div>
<div class="m2"><p>مده ز دست که خود حاصل جهان است این</p></div></div>