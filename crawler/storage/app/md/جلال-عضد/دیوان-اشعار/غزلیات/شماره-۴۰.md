---
title: >-
    شمارهٔ  ۴۰
---
# شمارهٔ  ۴۰

<div class="b" id="bn1"><div class="m1"><p>در این ایّام کس غمخوار ما نیست</p></div>
<div class="m2"><p>به جز غم کاو ز ما یک دم جدا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سری دارم فدای وصل، لیکن</p></div>
<div class="m2"><p>مرا با دستبرد هجر پا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا! در آتش دوری همی ساز</p></div>
<div class="m2"><p>که جانان را سر پیوند ما نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان عاشقان بیگانه دانش</p></div>
<div class="m2"><p>هر آن عاشق که با درد آشنا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش است از درد و غم آزاد بودن</p></div>
<div class="m2"><p>ولی در مذهب عاشق روا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشو مغرور حُسن ای صاحب حُسن</p></div>
<div class="m2"><p>که روز حُسن را چندان بقا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو عمری نیست در عهدت وفایی</p></div>
<div class="m2"><p>که عهد عمر را چندان وفا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ترکستان رویت خال هندو</p></div>
<div class="m2"><p>عجب دارم اگر اصلش خطا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نقد ای جان بیا تا باده نوشیم</p></div>
<div class="m2"><p>که عمر رفته را دیگر قضا نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بوسی زان دهان عمری نُوَم بخش</p></div>
<div class="m2"><p>که بنیاد بقا جز بر فنا نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جلال از عشقت ار روزی نماند</p></div>
<div class="m2"><p>نیاری بر زبان کاو هست یا نیست</p></div></div>