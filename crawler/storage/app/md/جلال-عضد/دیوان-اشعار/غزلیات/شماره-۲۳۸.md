---
title: >-
    شمارهٔ  ۲۳۸
---
# شمارهٔ  ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>ای صبا! احوال من با او بگو</p></div>
<div class="m2"><p>حال بلبل با گل خودرو بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه با من می کند هجران او</p></div>
<div class="m2"><p>بشنو از من یک به یک با او بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامتم بین چون کمانی در غمش</p></div>
<div class="m2"><p>این سخن با آن کمان ابرو بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال سودا و پریشانی من</p></div>
<div class="m2"><p>مو به مو با آن شکنج موبگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون فشانی دو چشمم دیده ای</p></div>
<div class="m2"><p>پیش آن دو نرگس جادو بگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه ما از بوی زلفش زنده ایم</p></div>
<div class="m2"><p>پیش آن گیسوی عنبربو بگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وانکه جان در تن به یاد لعل اوست</p></div>
<div class="m2"><p>پیش آن یاقوت پر لؤلؤ بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوستان ما را ملامت می کنند</p></div>
<div class="m2"><p>این سخن را با ملامت گو بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما نمی گوییم ترک یار خویش</p></div>
<div class="m2"><p>هر که خواهد هر چه خواهد گو بگو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد جلال افتاده بازوی عشق</p></div>
<div class="m2"><p>این سخن با آن سمن بازو بگو</p></div></div>