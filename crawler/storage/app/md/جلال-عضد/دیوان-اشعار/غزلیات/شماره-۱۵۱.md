---
title: >-
    شمارهٔ  ۱۵۱
---
# شمارهٔ  ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>می دهم جان ز پی لعل زمرّد پوشش</p></div>
<div class="m2"><p>گرچه حاصل نشود کام به سعی و کوشش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم مستش که به هر غمزه بریزد خونی</p></div>
<div class="m2"><p>گو بخور خون من خسته که بادا نوشش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چه لطف است، در آن شکل و شمایل که شده ست</p></div>
<div class="m2"><p>دیده حیران و خرد واله و جان مدهوشش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو سکون از دل سودازده من مَطَلب</p></div>
<div class="m2"><p>کاین نه دیگی ست که هرگز بنشیند جوشش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمزه اش هر نفسی خون دلم می ریزد</p></div>
<div class="m2"><p>باز جان می دهدم لعل لب خاموشش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باچنین صورت زیبا و قد و خد که تراست</p></div>
<div class="m2"><p>گر ملک پیش تو آید ببری از هوشش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگرش پند کسان جای نگیرد در گوش</p></div>
<div class="m2"><p>هر که آکنده شد از پنبه غفلت گوشش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر که دارم چو به پای تو فدا خواهد شد</p></div>
<div class="m2"><p>من دلسوخته تا چند کشم بر دوشش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جلال! ار سر زلفش به کف آری روزی</p></div>
<div class="m2"><p>یک سر موی به ملک دو جهان مفروشش</p></div></div>