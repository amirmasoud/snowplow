---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>خطّ تو که در عین خرد عین جمال است</p></div>
<div class="m2"><p>خطّی ست که بر خوبی رخسار تو دال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن لطف میانت را بنمای به مردم</p></div>
<div class="m2"><p>تا خلق بدانند که ما را چه خیال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان دوست ملامت مکن ای دوست که او را</p></div>
<div class="m2"><p>از هر دو جهان بی رخ خوب تو ملال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت نظر مرحمت تست که ما را</p></div>
<div class="m2"><p>نه طاقت هجران و نه امّید وصال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ناصح ازین بیش ملامت مکنم زانک</p></div>
<div class="m2"><p>در پرده تقدیر کسی را چه مجال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که کشیده ست قضا میل شقاوت</p></div>
<div class="m2"><p>در دیده او کحل سعادات محال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای نی به نوا ساز ده این ناله دلسوز</p></div>
<div class="m2"><p>کز چنگ غمان شخص مرا ناله چو نال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پرده عشّاق بِدَر پرده عشّاق</p></div>
<div class="m2"><p>بی ذوق چه داند که درین پرده چه حال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از وصل تو یک روز به درمان رسد آخر</p></div>
<div class="m2"><p>دردی که ز هجران تو بر جان جلال است</p></div></div>