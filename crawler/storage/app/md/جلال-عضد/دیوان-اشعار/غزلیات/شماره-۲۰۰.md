---
title: >-
    شمارهٔ  ۲۰۰
---
# شمارهٔ  ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>ز سودای غم عشقت چنانم</p></div>
<div class="m2"><p>که سر از پا و پا از سر ندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر از دستم بخواهد رفت روزی</p></div>
<div class="m2"><p>همان بهتر که در پایت فشانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان افتاده ام پیش درت خوار</p></div>
<div class="m2"><p>که پنداری که خاک آستانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر هجران تو عمرم سرآرد</p></div>
<div class="m2"><p>دهد وصلت حیات جاودانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل مهرت ز خاک من بروید</p></div>
<div class="m2"><p>چو زیر گل بریزد استخوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من روز قیامت هر چه پرسند</p></div>
<div class="m2"><p>به غیر از دوست ناید بر زبانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا از بهر عشقت آفریدند</p></div>
<div class="m2"><p>چه کار دیگر است اندر جهانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلت ای یار! بر حالم بسوزد</p></div>
<div class="m2"><p>اگر درد دل خود بر تو خوانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بده کام جلال خسته امروز</p></div>
<div class="m2"><p>که تا فردا بمانم یا نمانم</p></div></div>