---
title: >-
    شمارهٔ  ۲۷۰
---
# شمارهٔ  ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>خیز که شب رفت و صبح کرد تجلّی</p></div>
<div class="m2"><p>بزم برآرای همچو جنّت اعلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روضه مینو ست یا مدینه شیراز</p></div>
<div class="m2"><p>نکهت خلد است یا هوای مصلّی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ سبق برده از حظیره فردوس</p></div>
<div class="m2"><p>سرو خرامان شکست رونق طوبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خم کاکل فروغ طلعت ساقی</p></div>
<div class="m2"><p>چون شب دیجور عکس نور تجلّی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساعد و لعلش به قتل عاشق و احباب</p></div>
<div class="m2"><p>هم ید بیضا نموده هم دم عیسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قبله ما نیست جز درت که نباشد</p></div>
<div class="m2"><p>قبله مجنون جز آستانه لیلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه رندان بود حریم خرابات</p></div>
<div class="m2"><p>توبه مستان بود ز توبه و تقوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدمت دردی کشان بی دل و دین کن</p></div>
<div class="m2"><p>تا بدهندت مراد دنیی و عقبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهره مجلس به بارگاه شهنشاه</p></div>
<div class="m2"><p>نغمه شعر جلال برده به شعری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مالک دین و جمال م لّت ابواسحق</p></div>
<div class="m2"><p>آنکه به فرمان اوست عرصه دنیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسرو اعظم خدایگان سلاطین</p></div>
<div class="m2"><p>جان جهان پادشاه صورت و معنی</p></div></div>