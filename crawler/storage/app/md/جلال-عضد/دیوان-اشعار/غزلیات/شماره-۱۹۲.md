---
title: >-
    شمارهٔ  ۱۹۲
---
# شمارهٔ  ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>تو مپندار که از عشق تو دل برگیرم</p></div>
<div class="m2"><p>یا به جای تو کسی جویم و در بر گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد صد سال اگر بر سر خاکم گذری</p></div>
<div class="m2"><p>پاره سازم کفن و زندگی از سر گیرم</p></div></div>