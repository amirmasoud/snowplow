---
title: >-
    شمارهٔ  ۲۰۱
---
# شمارهٔ  ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>من بنده آن قامت و بالا و میانم</p></div>
<div class="m2"><p>من عاشق و شوریده و شیدای فلانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من واله عیّاری آن نرگس مستم</p></div>
<div class="m2"><p>حیران خرامیدن آن سرو روانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عمر گرامی! خبرت نیست که بی تو</p></div>
<div class="m2"><p>عمری به چه خونابه دل می گذرانم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>احوال مرا هر که در آفاق شنیدند</p></div>
<div class="m2"><p>وان بخت ندارم که به گوش تو رسانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک روز به بالین من خسته قدم نه</p></div>
<div class="m2"><p>بنگر که ز تیمار فراقت به چه سانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی صبر که بی روی تو یک دم بنشینم</p></div>
<div class="m2"><p>نی بخت که در پهلوی خویشت بنشانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شوق تو صد بوسه زنم بر دهن خویش</p></div>
<div class="m2"><p>هرگاه که نام تو برآید به زبانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابروی تو با غمزه خلایق چو ببینند</p></div>
<div class="m2"><p>دانند که من کشته آن تیر و کمانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهم که درآییم من و تو به سماعی</p></div>
<div class="m2"><p>تو دست برافشانی و من جان بفشانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پرتو رخسار تو ای شمع جهانسوز</p></div>
<div class="m2"><p>آتشکده ای ساخته ای بر دل و جانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جورت بکشم تا ز وجودم رمقی هست</p></div>
<div class="m2"><p>ورتاب و توانم نبود تا بتوانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از من ببریدی و نه این بود امیدم</p></div>
<div class="m2"><p>از عهد بگشتی و نه این بود گمانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مشنو که جلال از تو بپیچد سر مویی</p></div>
<div class="m2"><p>یک موی تو بهتر ز همه ملک جهانم</p></div></div>