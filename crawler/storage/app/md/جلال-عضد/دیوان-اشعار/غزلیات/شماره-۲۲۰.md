---
title: >-
    شمارهٔ  ۲۲۰
---
# شمارهٔ  ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>از آن ساعت که افتادم جدا از صحبت یاران</p></div>
<div class="m2"><p>همی بارم همه روزه سرشک از دیده چون باران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنم جمله جهان گردید و جان در خدمت جانان</p></div>
<div class="m2"><p>قدم ملک زمین پیمود و دل در صحبت یاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی خواهم که در کویش وطن سازم همه عمری</p></div>
<div class="m2"><p>ولی چتوان چو جنّت نیست مأوای گنهکاران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بِهل تا بر سر کویش به سختی می دهم جانی</p></div>
<div class="m2"><p>مگر روزی گذار آرد به بالین دل افگاران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر زاهد نداند ذوق جام باده معذور است</p></div>
<div class="m2"><p>که ذوق جرعه مستی نمی دانند هشیاران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشا اوقات آن شوریده مست خراباتی</p></div>
<div class="m2"><p>که هم پهلوی رندان است و هم زانوی خمّاران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الا ای باد نوروزی! گذر کن بر سر زلفش</p></div>
<div class="m2"><p>به قید او ببین تا چیست احوال گرفتاران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بی دردی و معذوری اگر بر من نبخشایی</p></div>
<div class="m2"><p>هر آن کاو نیستش دردی چه داند حال بیماران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دماغم جز به بوی زلف مشکینت نمی شورد</p></div>
<div class="m2"><p>وگرنه مشک بسیار است در بازار عطّاران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گر مانند بخت عاشقانِ خسته در خوابی</p></div>
<div class="m2"><p>رقیبت آگه است آخر از آب چشم بیداران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به عیّاری جلالا تا گرفتی زلف عیّارش</p></div>
<div class="m2"><p>کنون نام تو نقشی گشت بر بازوی عیّاران</p></div></div>