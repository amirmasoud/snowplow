---
title: >-
    شمارهٔ  ۱۳۸
---
# شمارهٔ  ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>سیه چاهی ست زلفت تار و دلگیر</p></div>
<div class="m2"><p>در او دیوانگان بسته به زنجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشد تدبیر و عقل و رایم از دست</p></div>
<div class="m2"><p>چه تدبیر ای مسلمانان، چه تدبیر!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و جان دادن اندر جُست و جویش</p></div>
<div class="m2"><p>چو یاور نیست بخت از من، چه تقصیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غزل چون می نویسم از سر سوز</p></div>
<div class="m2"><p>همی سوزد قلم هنگام تحریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو از ما فارغ و ما در تک و پوی</p></div>
<div class="m2"><p>چه چاره چون چنین رفته ست تقدیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه دیده بر دوزم ز رویت</p></div>
<div class="m2"><p>وگر خود می زنی بر دیده ام تیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ربودی عقل و جان و صبر و هوشم</p></div>
<div class="m2"><p>وگر خواهی حساب اکنون ز سر گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک را هست سودای تو در سر</p></div>
<div class="m2"><p>چو سودای جوانی در سر پیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلال! از بخت خود، کامی ندیدی</p></div>
<div class="m2"><p>که خوابت را به جز غم نیست تعبیر</p></div></div>