---
title: >-
    شمارهٔ  ۹۹
---
# شمارهٔ  ۹۹

<div class="b" id="bn1"><div class="m1"><p>عهد ما مشکن و بر باد مده خاکی چند</p></div>
<div class="m2"><p>آتشی در زده انگار به خاشاکی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما چو غنچه همه دل تنگ و تو چون باد صبا</p></div>
<div class="m2"><p>شادمان می گذری بر سر غمناکی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکشی از سخن تلخ و به دَم زنده کنی</p></div>
<div class="m2"><p>درهم آمیخته ای زهری و تریاکی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از وجود من و از عشق جز این بیش نماند</p></div>
<div class="m2"><p>آتشی چند در آمیخته با خاکی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهسوارا! بگذر بر صف ما صیدکنان</p></div>
<div class="m2"><p>تا سری چند ببندیم به فتراکی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای صبا! بویی از آن غنچه خندان به من آر</p></div>
<div class="m2"><p>تا به پیراهن جان در فکنم چاکی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غم از طعن حسودان که مکدّر نکند</p></div>
<div class="m2"><p>نظر پاک مرا طعنه ناپاکی چند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس کن ای خواجه که ما باک نداریم ز جان</p></div>
<div class="m2"><p>چندگویی سخن جان بر بی باکی چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق و تنهایی و غم چند بود صبر جلال</p></div>
<div class="m2"><p>چه بود زور زبونی بر چالاکی چند</p></div></div>