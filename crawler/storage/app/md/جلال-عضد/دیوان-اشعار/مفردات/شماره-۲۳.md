---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>دیروز نداستم و بتوانستم</p></div>
<div class="m2"><p>امروز بدانستم و نتوانستم</p></div></div>