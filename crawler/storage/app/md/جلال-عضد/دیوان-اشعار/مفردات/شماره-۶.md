---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>تا چند سوز عشقش پنهان کنم به حیله</p></div>
<div class="m2"><p>هم عاقبت ز جایی، این سوز سر برآرد</p></div></div>