---
title: >-
    شمارهٔ  ۲۴
---
# شمارهٔ  ۲۴

<div class="b" id="bn1"><div class="m1"><p>عمری ست که ما به بوی درمان</p></div>
<div class="m2"><p>در دستِ هزار گونه دردیم</p></div></div>