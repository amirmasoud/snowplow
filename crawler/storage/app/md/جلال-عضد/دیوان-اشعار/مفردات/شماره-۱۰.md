---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>حقّا که عجب بی سر و پای است فلان</p></div>
<div class="m2"><p>کاو بر سر و پای خویش جل می پوشد</p></div></div>