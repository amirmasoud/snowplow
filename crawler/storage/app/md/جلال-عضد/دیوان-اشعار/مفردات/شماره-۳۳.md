---
title: >-
    شمارهٔ  ۳۳
---
# شمارهٔ  ۳۳

<div class="b" id="bn1"><div class="m1"><p>زری چندم به دست آمد ز بصره</p></div>
<div class="m2"><p>ببستم جمله بر پنجاه صرّه</p></div></div>