---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>سحر کردی مگر تو خواجه بشیر</p></div>
<div class="m2"><p>کز سر بز روانه کردی شیر</p></div></div>