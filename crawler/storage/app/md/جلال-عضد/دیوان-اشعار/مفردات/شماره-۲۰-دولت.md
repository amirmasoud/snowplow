---
title: >-
    شمارهٔ  ۲۰ - دولت
---
# شمارهٔ  ۲۰ - دولت

<div class="b" id="bn1"><div class="m1"><p>زان دوایی که نیست انجامش</p></div>
<div class="m2"><p>سر علّت ببر که شد نامش</p></div></div>