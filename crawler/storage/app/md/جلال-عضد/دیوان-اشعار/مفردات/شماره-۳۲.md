---
title: >-
    شمارهٔ  ۳۲
---
# شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>نپویم بیش ازین دیگر در این راه</p></div>
<div class="m2"><p>ازین گفتارها استغفراللّه</p></div></div>