---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>گفتم که چو دریاست گهرپرور میر</p></div>
<div class="m2"><p>شک نیست که دریاست ولی خس پرور</p></div></div>