---
title: >-
    شمارهٔ  ۲۲
---
# شمارهٔ  ۲۲

<div class="b" id="bn1"><div class="m1"><p>چشمه آب از میانه نیل</p></div>
<div class="m2"><p>گر جدا می‌کنی بگردد پیل</p></div></div>