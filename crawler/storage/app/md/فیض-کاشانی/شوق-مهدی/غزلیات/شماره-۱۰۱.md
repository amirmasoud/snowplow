---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>سلوک راه حق و خدمت امام شفیق</p></div>
<div class="m2"><p>گرت مدام میسر شود زهی توفیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان و کار جهان جمله هیچ در هیچ است</p></div>
<div class="m2"><p>که نیست راهنمائی به حق جدیر و حقیق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی مسائل دینیه خورده است گره</p></div>
<div class="m2"><p>مگر امام گشاید به ناوک تحقیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجاست راهنمائی به سوی منزل او</p></div>
<div class="m2"><p>که ما به خویش نبردیم ره به هیچ طریق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خفای او ز نظرهای غیبتش در چاه</p></div>
<div class="m2"><p>به غور آن نرسد صد هزار فکر عمیق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون خراما اماما و راه حق بنمای</p></div>
<div class="m2"><p>که در کمینگه دینند قاطعان طریق</p></div></div>
<div class="b2" id="bn7"><p>دریغ و درد که بگذشت عمر فیض و نیافت</p>
<p>سعادت شرف خدمتش به هیچ طریق</p></div>