---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>کس نیست که او منتظر وصل شما نیست</p></div>
<div class="m2"><p>جان نیست که آن خاک ره آل عبا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق معرفت مهر شما در دل ما کاشت</p></div>
<div class="m2"><p>این شدت شوق و شعف از جانب ما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس نیست که آن قدر شما را نشناسد</p></div>
<div class="m2"><p>در هیچ سری نیست که سِرّی ز خدا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل تیره شد از ظلمت شبهای فراقت</p></div>
<div class="m2"><p>بازآی که بی‏مهر رخت نور و ضیا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازآ که نمانده است ز اسلام مگر نام</p></div>
<div class="m2"><p>در روی زمین بی تو به جز جور و جفا نیست‏</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هجر تو گر فیض بمیرد چه توان کرد</p></div>
<div class="m2"><p>با هیچ دلاور سپر تیر قضا نیست</p></div></div>
<div class="b2" id="bn7"><p>در صومعه و خانقه و خلوت و مسجد</p>
<p>جائی نتوان یافت که دستی به دعا نیست</p></div>