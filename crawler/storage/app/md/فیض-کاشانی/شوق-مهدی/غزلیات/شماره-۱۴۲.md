---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>گر از روش حافظ و قرآن به در آئی</p></div>
<div class="m2"><p>هر ره که روی باز پشیمان به در آئی‏</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردار سرودی ز کلامش طرب انگیز</p></div>
<div class="m2"><p>شاید دمی از غصه هجران به در آئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان می‏دهم از حسرت دیدار تو چون صبح</p></div>
<div class="m2"><p>باشد که چو خورشید درخشان به در آئی‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی چو صبا بر تو گمارم دم همت</p></div>
<div class="m2"><p>کز غنچه چو گل خرم و خندان به در آئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تیر شب هجر تو جانم به لب آمد</p></div>
<div class="m2"><p>وقت است که همچون مه تابان به درآئی</p></div></div>
<div class="b2" id="bn6"><p>ای فیض مخور غصه که این پرده غیبت</p>
<p>برخیزد و از کلبه احزان به درآئی</p></div>