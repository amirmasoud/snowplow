---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>زهی خجسته زمانی که یار باز آید</p></div>
<div class="m2"><p>به کام غمزدگان غمگسار باز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر فدای امام زمان نخواهد شد</p></div>
<div class="m2"><p>ز سر چه گویم و سر خود چکار باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیش خیل خیالش کشیدم ابلق چشم</p></div>
<div class="m2"><p>به آن امید که آن شهسوار باز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی خفا چه جفاها که کرد و دل بکشید</p></div>
<div class="m2"><p>به بوی آن که دگر نوبهار باز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمین مباد که عمرت در انتظار گذشت</p></div>
<div class="m2"><p>که جان عمر پس از انتظار باز آید</p></div></div>
<div class="b2" id="bn6"><p>ز نقش بند قضا هست امید آن ای فیض</p>
<p>که آن امید دل بی‏قرار باز آید</p></div>