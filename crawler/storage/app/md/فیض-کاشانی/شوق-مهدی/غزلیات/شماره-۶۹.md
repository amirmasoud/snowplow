---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>دوش از جناب مهدی پیک بشارت آمد</p></div>
<div class="m2"><p>کز حضرت الهی عشرت اشارت آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی حضور باشد جسم زمانه را کام</p></div>
<div class="m2"><p>ویرانه جهان را وقت عمارت آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این شرح بی‏نهایت کز وصف ما شنیدی</p></div>
<div class="m2"><p>حرفی است از هزاران کاندر عبارت آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز جای هر کس پیدا شود ز خوبان</p></div>
<div class="m2"><p>کان ماه مجلس‏افروز اندر صدارت آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آلودگان عصیان در آب توبه غسلی</p></div>
<div class="m2"><p>معصوم منتظر را وقت زیارت آمد</p></div></div>
<div class="b2" id="bn6"><p>از یمن مقدم او رونق گرفت طاعت</p>
<p>اجناس معصیت را هنگام غارت آمد</p></div>