---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>غم زمانه که هیچش گران نمی‌‏بینم</p></div>
<div class="m2"><p>دواش غیر امام زمان نمی‏‌بینم‏</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب بود که در ایام ما ظهور کند</p></div>
<div class="m2"><p>چرا که طالع خویش آن چنان نمی‌‏بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دامن غم او دست بر نمی‏‌دارم</p></div>
<div class="m2"><p>چرا که مصلحت خود در آن نمی‌‏بینم‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین دو دیده حیران من هزار افسوس</p></div>
<div class="m2"><p>که با دو آینه رویش عیان نمی‌‏بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان دوستی اهل بیت پرهیز است</p></div>
<div class="m2"><p>در اهل دانش عصر این نشان نمی‌‏بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپرس فیض ز من سرّ غیبتش که در آن</p></div>
<div class="m2"><p>به هیچ‌جا سخن دل نشان نمی‌‏بینم</p></div></div>
<div class="b2" id="bn7"><p>کسی که گوش کند ناله‏‌ام در این غم کو؟</p>
<p>خموش که اهل دلی در جهان نمی‏‌بینم</p></div>