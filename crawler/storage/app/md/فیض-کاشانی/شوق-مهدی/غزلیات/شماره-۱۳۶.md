---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>تو را می‏گویم ای جویای حق هی</p></div>
<div class="m2"><p>ز جان بنده بشنو این نه از وی‏</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غذای روح کن گفت پیمبر</p></div>
<div class="m2"><p>مخوان غیر از حدیث از درس‏ها شی‏ء</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شراب حبّ اهل البیت درکش</p></div>
<div class="m2"><p>به آب زندگانی بوده‏ام پی‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را شرع پیمبر رهنما بس</p></div>
<div class="m2"><p>چه می‏جوئی ز اسرار جم و کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جز حرف خدا و دوستانش</p></div>
<div class="m2"><p>هر آن حرفی که گوئی هست لاشی‏ء</p></div></div>
<div class="b2" id="bn6"><p>مده از دست امر شرع ای فیض</p>
<p>اگر خواهی به جان و دل شوی حی‏</p></div>