---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>روی بنمای اماما و ره منبر گیر</p></div>
<div class="m2"><p>بگشا منطق و دل از در دل‏ها برگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دین اسلام و شریعت که کهن گشت و خراب</p></div>
<div class="m2"><p>یک به یک در دل ما تازه کن و از سر گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر کن از امن و امان عالم آشفته ما</p></div>
<div class="m2"><p>ظلم و طغیان و خرابی ز ممالک برگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر رفع ستم و جور به لطف شمشیر</p></div>
<div class="m2"><p>از موالی همه نصرت ز اعادی برگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حقِّ آنست جهان کو همگی دشمن گیر</p></div>
<div class="m2"><p>او چو یاری کندت روی زمین لشکر گیر</p></div></div>
<div class="b2" id="bn6"><p>کن شفیعأ لموالیک خصوصا للفیض</p>
<p>چون شود کشته به پای تو ز خاکش برگیر</p></div>