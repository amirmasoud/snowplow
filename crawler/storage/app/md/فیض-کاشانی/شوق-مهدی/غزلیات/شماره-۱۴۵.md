---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>ای پادشه خوبان داد از غم تنهائی</p></div>
<div class="m2"><p>دل بی تو به جان آمد وقتست که باز آئی‏</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی رویت بنشسته به هر راهی</p></div>
<div class="m2"><p>صد زاهد و صد عابد سرگشته سودائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشتاقی و مهجوری دور از تو چنانم کرد</p></div>
<div class="m2"><p>کز دست نخواهد شد پایان شکیبائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای درد توام درمان در بستر ناکامی</p></div>
<div class="m2"><p>وی یاد توام مونس در گوشه تنهائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر خود ورای خود در امر تو کی گنجد</p></div>
<div class="m2"><p>کفر است در این وادی خودبینی و خودرائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دائره فرمان ما نقطه تسلیمیم</p></div>
<div class="m2"><p>لطف آنچه تو اندیشی حکم آنچه تو فرمائی</p></div></div>
<div class="b2" id="bn7"><p>گستاخی و پرگوئی تا چند کنی ای فیض</p>
<p>بگذر تو از این وادی تن ده به شکیبائی</p></div>