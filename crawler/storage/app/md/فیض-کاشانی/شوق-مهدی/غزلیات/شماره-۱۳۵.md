---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>وصال او ز عمر جاودان به</p></div>
<div class="m2"><p>خداوندا مرا آن ده که آن به‏</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو سرّ وجودش با مخالف</p></div>
<div class="m2"><p>که راز دوست از دشمن نهان به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خلدم زاهدا دعوت مفرما</p></div>
<div class="m2"><p>که شوق صاحب الامرم از آن به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا دائم به فکر و ذکر او باش</p></div>
<div class="m2"><p>به حکم آن که طاعت جاودان به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هر حرفی که گویی فیض جایی</p></div>
<div class="m2"><p>حدیث صاحب عصر و زمان به‏</p></div></div>