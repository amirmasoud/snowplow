---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>منم که مهر نبی و ولی پناه من است</p></div>
<div class="m2"><p>دعای نایب حق ورد صبحگاه من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پادشاه و گدا فارغم بحمد اللّه</p></div>
<div class="m2"><p>گدای خاک ره دوست پادشاه من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز وصل او نشکیبم گرم به تیغ زنند</p></div>
<div class="m2"><p>رمیدن از در دولت نه رسم و راه من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حال من نظری می‏کن ای امام زمان</p></div>
<div class="m2"><p>که التفات تو کفاره گناه من است‏</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز دنیی و عقبی غرض وصال شماست</p></div>
<div class="m2"><p>جز این خیال ندارم خدا گواه من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آستان شما رو نهاده‌‏ام زان روی</p></div>
<div class="m2"><p>فرا ز مسند خورشید تکیه‏‌گاه من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز موج‏های حوادث مرا چه باک ای فیض</p></div>
<div class="m2"><p>چو مهر حیدر و اولاد او پناه من است</p></div></div>
<div class="b2" id="bn8"><p>وگرنه ذکر حقم بر زبان خروشی نیست</p>
<p>به دل محبت این قوم عذرخواه من است</p></div>