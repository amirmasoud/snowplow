---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>تو را امام ز اعدا خدا نگه دارد</p></div>
<div class="m2"><p>فرشته‌‏ات به دو دست دعا نگه دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیده خواه نهان باش و خواه عیان</p></div>
<div class="m2"><p>خدات در همه حال از بلا نگه دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا به درگه او گر دل مرا بینی</p></div>
<div class="m2"><p>ز روی لطف بگویش که جا نگه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز درد دوست نگویم حدیث جز با دوست</p></div>
<div class="m2"><p>که آشنا سخن آشنا نگه دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر و زر و دل و جانم فدای مهدی باد</p></div>
<div class="m2"><p>که حقِّ خدمت اهل وفا نگه دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبار راه گذارش نشان دهید که فیض</p></div>
<div class="m2"><p>به یادگار نسیم صبا نگه دارد</p></div></div>
<div class="b2" id="bn7"><p>خرد ز فتنه آخر زمان حذر فرمود</p>
<p>ز دست بنده چه خیزد خدا نگه دارد</p></div>