---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>سلامی چو بوی خوش آشنائی</p></div>
<div class="m2"><p>بدان مردم دیده روشنائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درودی چو نور دل پارسایان</p></div>
<div class="m2"><p>بدان شمع خلوتگه پارسائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان زبده دودمان نبوت</p></div>
<div class="m2"><p>بدان نخبه عترت مصطفائی‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امام زمان مقتدای خلایق</p></div>
<div class="m2"><p>حفیظ زمین بحر علم سمائی‏</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا باش دائم گدای در او</p></div>
<div class="m2"><p>از ایشان طلب رازهای خدائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درگاه آل نبی رو مگردان</p></div>
<div class="m2"><p>که آنجاست مفتاح مشکل گشائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیاموزمت کیمیای سعادت</p></div>
<div class="m2"><p>ز اعدای آل پیمبر جدائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی‏بینم از همدمان هیچ یاری</p></div>
<div class="m2"><p>دلم خون شد از غصه مهدی کجائی</p></div></div>
<div class="b2" id="bn9"><p>مکن فیض از غیبت خود شکایت</p>
<p>چه دانی تو ای بنده کار خدائی‏</p></div>