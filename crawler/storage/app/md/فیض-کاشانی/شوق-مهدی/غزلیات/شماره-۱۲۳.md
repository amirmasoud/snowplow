---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>مژده وصل تو کو کز سر جان برخیزم</p></div>
<div class="m2"><p>خاک کوی تو شوم از دو جهان برخیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ولای تو که گر بنده خویشم خوانی</p></div>
<div class="m2"><p>از سر خواجگی کون و مکان برخیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب از ابر هدایت برسان بارانی</p></div>
<div class="m2"><p>پیشتر زانکه چو گردی ز میان برخیزم‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذرم گر ز جهان بر سر خاکم آرش</p></div>
<div class="m2"><p>... ان برخیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فاش کن سرّ قیامت ز قیام قائم</p></div>
<div class="m2"><p>قامتش را بنما کز سر و جان برخیزم</p></div></div>
<div class="b2" id="bn6"><p>قامت قائم حق را چو ببینم قائم</p>
<p>همچو فیض از سر اسباب جهان برخیزم</p></div>