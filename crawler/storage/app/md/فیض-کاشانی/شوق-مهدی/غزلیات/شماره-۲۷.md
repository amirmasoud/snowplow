---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>سئوال طلعت از آن حضرت ارچه بی‏ادبی است</p></div>
<div class="m2"><p>زبان خموش ولیکن دهان پر از عربی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهفته حق رخ و باطل به عشوه جلوه‏کنان</p></div>
<div class="m2"><p>بسوخت عقل ز حیرت که این چه بوالعجبی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شوق نور حضورش بسوخت دل آری</p></div>
<div class="m2"><p>چراغ مصطفوی با شرار بولهبی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نیم جو نخرم طاق قیصر و کسری</p></div>
<div class="m2"><p>مرا که درگهش ایوان و سایه‏اش طلبی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علاج درد دل ما شراب وصل شماست</p></div>
<div class="m2"><p>نه در صراحی و چینی و شیشه و حلبی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز فیض مهر تو دل را امیدواری‏ها</p></div>
<div class="m2"><p>به گریه سحری و نیاز نیم‏شبی است</p></div></div>
<div class="b2" id="bn7"><p>مپرس سرّ نهان بودن امام ای فیض</p>
<p>که کارهای خدا را سئوال بی‏ادبی است</p></div>