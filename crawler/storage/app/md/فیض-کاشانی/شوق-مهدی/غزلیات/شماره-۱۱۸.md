---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>من که باشم که بر آن خاطر عاطر گذرم</p></div>
<div class="m2"><p>لطف‏ها می‏کنی ای خاک درت تاج سرم‏</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاش راهی به سر کوی تو می‏داشتمی</p></div>
<div class="m2"><p>تا به سر سوی تو می‏آمدم از هر گذرم‏</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نتوان قطع بیابان فراق تو نمود</p></div>
<div class="m2"><p>مگر آگه کنی از رسم و ره این سفرم‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه منزلگه خویشم بنما تا پس از این</p></div>
<div class="m2"><p>پیش گیرم ره آن کوی و به سر می‏سپرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همتم بدرقه راه کن ای طایر قدس</p></div>
<div class="m2"><p>که دراز است ره مقصد و من نوسفرم‏</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرم آن روز کزین مرحله بربندم رخت</p></div>
<div class="m2"><p>وز سر کوی تو پرسند رفیقان خبرم‏</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای نسیم سحری بندگی ما برسان</p></div>
<div class="m2"><p>گو فراموش مکن وقت دعای سحرم</p></div></div>
<div class="b2" id="bn8"><p>شاید ای فیض اگر در طلب گوهر وصل</p>
<p>دیده دریا کنم از اشک و در او غوطه خورم</p></div>