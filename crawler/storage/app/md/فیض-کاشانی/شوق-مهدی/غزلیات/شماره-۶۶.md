---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>صاحب الامر مگر باز گذاری بکند</p></div>
<div class="m2"><p>راه بنماید و با عدل قراری بکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غمش هر در و لعلی که دلم داشت بریخت</p></div>
<div class="m2"><p>مگر از گریه شادیش نثاری بکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش گفتم بکند وعده وفا، قائم حق</p></div>
<div class="m2"><p>هاتف غیب ندا داد که آری بکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عصر خالی شده از عدل بود کز طرفی</p></div>
<div class="m2"><p>مهدی از غیب برون آید و کاری بکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ره ندارم بر او تا بدهم شرح غمش</p></div>
<div class="m2"><p>مگرش باد صبا گوش و گذاری بکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل او یا خبر مرگ اعادی یا عدل</p></div>
<div class="m2"><p>یک دعائی ز کسی زین دو سه کاری بکند</p></div></div>
<div class="b2" id="bn7"><p>تو در این غمکده ای فیض بمان روزی چند</p>
<p>که گذر بر سرت از گوشه کناری بکند</p></div>