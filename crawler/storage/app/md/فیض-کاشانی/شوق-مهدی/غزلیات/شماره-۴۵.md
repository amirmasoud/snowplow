---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>نفس باد صبا مشک‏‌فشان خواهد شد</p></div>
<div class="m2"><p>عالم پیر دگرباره جوان خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی این تیره شب غیبت مهدی روزی</p></div>
<div class="m2"><p>از دم صبح حضورش لمعان خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم ار پیر شد از جور و ستم باکی نیست</p></div>
<div class="m2"><p>از قدوم شه دین امن و امان خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکلاتی که به دل‏ ها شده عمری است گره</p></div>
<div class="m2"><p>حل آنها همه در لحظه آن خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانش کسبی صد ساله این مدعیان</p></div>
<div class="m2"><p>نزد علمش به مثل برگ خزان خواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این اباطیل و اکاذیب که شایع شده است</p></div>
<div class="m2"><p>همه را حضرت او محوکنان خواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طعنه بر حق چه زنی ای که به باطل غرقی</p></div>
<div class="m2"><p>تو به این غرّه مشو نوبت آن خواهد شد</p></div></div>
<div class="b2" id="bn8"><p>فیض اگر در قدم حضرت او جان بخشد</p>
<p>زین جهان تا به جنان رقص‌‏کنان خواهد شد</p></div>