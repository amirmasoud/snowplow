---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>مردم دیده ما جز به رهت ناظر نیست</p></div>
<div class="m2"><p>دل سرگشته ما غیر تو را ذاکر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکم احرام طواف حرمت می‏بندد</p></div>
<div class="m2"><p>گرچه از خون دل خویش دمی طاهر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته دام قفس باد چو مرغ وحشی</p></div>
<div class="m2"><p>طایر سدره اگر در طلبت طایر نیست‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت راه بیابد به جناب عالیت</p></div>
<div class="m2"><p>هر که اندر طلبت همت او قاصر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از روان‏بخشی عیسی نزنم پیش تو دم</p></div>
<div class="m2"><p>زان که در روح‏فزائی چو لبت ماهر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوق خدام تو تنها نه همین در دل ماست</p></div>
<div class="m2"><p>مَلَکی نیست که در شوق رخت طایر نیست</p></div></div>
<div class="b2" id="bn7"><p>فیض اگر قلب و دلش کرد به راه تو نثار</p>
<p>مکنش عیب که بر نقد روان قادر نیست</p></div>