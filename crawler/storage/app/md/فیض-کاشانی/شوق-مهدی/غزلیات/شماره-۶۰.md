---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>گداخت جان که شود کار دل تمام و نشد</p></div>
<div class="m2"><p>بس سوختیم در این آرزوی خام و نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار حیله برانگیختیم تا شاید</p></div>
<div class="m2"><p>بریم ره به سراپرده امام و نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به معرفت نرسی تا به وصل او نرسی</p></div>
<div class="m2"><p>که من به خویش نمودم صد اهتمام و نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فغان که در طلبش عمر رفت و یک ساعت</p></div>
<div class="m2"><p>ز وصل دلکش او کام خواستیم و نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغ و درد که در جستجو سرآمد عمر</p></div>
<div class="m2"><p>شباب و شیب در این کار شد تمام و نشد</p></div></div>
<div class="b2" id="bn6"><p>در آرزوی لقایش بسوختیم ای فیض</p>
<p>گداخت جان که شود کار دل تمام و نشد</p></div>