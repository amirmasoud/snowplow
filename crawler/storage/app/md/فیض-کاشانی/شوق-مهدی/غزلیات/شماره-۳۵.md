---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>رواق منظر چشم من آشیانه تست</p></div>
<div class="m2"><p>کرم نما و فرود آ که خانه خانه تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نداده‏ام به کسی نقد دل به جز مهرت</p></div>
<div class="m2"><p>درِ خزانه به مهر تو و نشانه تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تن مقصرم از دولت ملازمتت</p></div>
<div class="m2"><p>ولی خلاصه جان خاک آستانه تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو قطب عالمی ای شهسوار ورنه چراست</p></div>
<div class="m2"><p>که توسنی چو فلک رام تازیانه تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا زیاد تو یاد خدا کنیم اگر</p></div>
<div class="m2"><p>کلید گنج سعادت نه در خزانه تست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی جلال و جمال و زهی صفات و کمال</p></div>
<div class="m2"><p>که در جهان همه گلبانگ عاشقانه تست!</p></div></div>
<div class="b2" id="bn7"><p>چو فیض طالب فیضم ز خاک درگه تو</p>
<p>که فیض‏های الهی در آستانه تست!</p></div>