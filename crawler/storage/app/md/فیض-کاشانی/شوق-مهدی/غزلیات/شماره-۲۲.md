---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>روضه خلد برین تربت اهل البیت است</p></div>
<div class="m2"><p>مایه محتشمی خدمت اهل البیت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه زر می‏شود از پرتو آن قلب سیاه</p></div>
<div class="m2"><p>کیمیائی است که در صحبت اهل البیت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که پیشش بنهد تاج تکبر خورشید</p></div>
<div class="m2"><p>کبریائیست که در حشمت اهل البیت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولتی را که نباشد غم از آسیب زوال</p></div>
<div class="m2"><p>بی‏تکلف بشنو دولت اهل البیت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصر فردوس جزای عمل ظاهر نیست</p></div>
<div class="m2"><p>که خصوص از جهت شیعت اهل البیت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلماتی که به آن توبه آدم پذیرفت</p></div>
<div class="m2"><p>نام‏ها و لقب و کنیت اهل البیت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کران تا به کران لشکر ظلم است ولی</p></div>
<div class="m2"><p>سبب بود جهان عصمت اهل البیت است</p></div></div>
<div class="b2" id="bn8"><p>فیض اگر آب حیات ابدی می‏طلبی</p>
<p>منبعش پیروی سنت اهل البیت است</p></div>