---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>حجاب چهره جان می‏شود غبار تنم</p></div>
<div class="m2"><p>خوشا دمی که از این چهره پرده برفکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا و هستی من در وجود من کم کن</p></div>
<div class="m2"><p>که با وجود تو کس نشنود ز من که منم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی ز عمر گذشت و نیافتم کامی</p></div>
<div class="m2"><p>دریغ و درد که غافل ز کار خویشتنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چو شمع ببارم سرشک نیست عجیب</p></div>
<div class="m2"><p>که سوزهاست نهانی درون پیرهنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که خدمت صاحب زمان بود معذور</p></div>
<div class="m2"><p>چرا زمین ستم پیشگان بود وطنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سزای همچو منی نیست دوری از در او</p></div>
<div class="m2"><p>که پای تا سر من مهر اوست و من نه منم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محبت علی و عترتش حیات من است</p></div>
<div class="m2"><p>ولای آل نبی همچو جان و من بدنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان محبت و مهر شما به دل دارم</p></div>
<div class="m2"><p>که گر زنند به تیغم دل از شما نکنم</p></div></div>
<div class="b2" id="bn9"><p>ز بس حدیث شما فیض گفت نزدیکست</p>
<p>که غیر حرف شما نشنود کس از سخنم‏</p></div>