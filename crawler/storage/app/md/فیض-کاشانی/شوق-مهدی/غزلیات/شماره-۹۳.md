---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>جان نخواهد که شود ز آتش شوق تو خلاص</p></div>
<div class="m2"><p>ماهی دلشده در بحر خیالت غواص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هواداری تو شمع صفت از سر سوز</p></div>
<div class="m2"><p>کردم ایثار تن خویش ز روی اخلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم چو پروانه بر شمع جمال تو تمام</p></div>
<div class="m2"><p>تا نسوزم نشوم ز آتش آن نشأه خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیمیای نظر آل نبی خاک مرا</p></div>
<div class="m2"><p>زر خالص کند ار چند بود همچو رصاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر این طایفه را تا نشناسد مؤمن</p></div>
<div class="m2"><p>نشود در حرم حضرت حق خاص الخاص</p></div></div>
<div class="b2" id="bn6"><p>قدر این قوم مقرب نشناسد عامی</p>
<p>بعد از این فیض مگو این سخنان جز به خواص</p></div>