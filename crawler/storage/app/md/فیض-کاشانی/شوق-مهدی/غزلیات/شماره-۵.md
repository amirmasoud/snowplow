---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>به ملازمان مهدی که رساند این دعا را</p></div>
<div class="m2"><p>که به شکر پادشاهی ز نظر مران گدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فریب دیو مردم، به جناب او پناهم</p></div>
<div class="m2"><p>مگر آن شهاب ثاقب نظری کند سها را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو قیامتی دهد رو که به دوستان نمائی</p></div>
<div class="m2"><p>برکات مصطفی را، حرکات مرتضی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بدان شمائل و خو که ز جد خویش داری</p></div>
<div class="m2"><p>به جهان در افکنی شور، چو کنی حدیث ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل دشمنان بسوزی چو عذار برفروزی</p></div>
<div class="m2"><p>تن دوستان سراسر، همه جان شود خدا را</p></div></div>
<div class="b2" id="bn6"><p>چه شود اگر نسیمی ز در تو بوی آرد</p>
<p>به پیام آشنائی بنوازد آشنا را</p></div>