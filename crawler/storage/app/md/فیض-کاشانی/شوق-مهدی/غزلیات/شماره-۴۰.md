---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>ز هجر مهدی هادی است کار و بارم تلخ</p></div>
<div class="m2"><p>گذشت زین غم جانسوز روزگارم تلخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب وصل تو روزی شود مگر روزی</p></div>
<div class="m2"><p>که در خیال جز اینست هرچه آرم تلخ‏</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلاوتی ز عبادت نمی‏چشم بی تو</p></div>
<div class="m2"><p>کجا دهد بر شیرین چو تخم کارم تلخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بری ز عمر نخوردم که لذتی بخشد</p></div>
<div class="m2"><p>به غیر حرف تو باشد هر آنچه آرم تلخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهان به ذکر تو شیرین کنم مگر که به لب</p></div>
<div class="m2"><p>به غیر حرف تو باشد هر آنچه آرم تلخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه ذکر تست همانا حلاوت سخنم</p></div>
<div class="m2"><p>که هست حرف دگر هرچه می‏نگارم تلخ</p></div></div>
<div class="b2" id="bn7"><p>اگرچه شهد خورم زهر باشدم در کام</p>
<p>که همچو فیض بود بی تو کار و بارم تلخ‏</p></div>