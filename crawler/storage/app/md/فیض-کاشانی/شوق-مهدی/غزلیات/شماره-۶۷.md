---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>مهدی چو به عدل دست گیرد</p></div>
<div class="m2"><p>بازار ستم شکست گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رایت حق بلند گردد</p></div>
<div class="m2"><p>باطل همه راه پست گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم و تقوی چون کمال یابد</p></div>
<div class="m2"><p>جهل و عدوان شکست گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این هست و شان شوند فانی</p></div>
<div class="m2"><p>زان نیست نما چو هست گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیصل یابد همه مهمات</p></div>
<div class="m2"><p>دستش چو گشاد بست گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پای درآورد عدو را</p></div>
<div class="m2"><p>چون تیغ علی به دست گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستیم همه ز جام غفلت</p></div>
<div class="m2"><p>کو محتسبی که مست گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پاش فتاده‏ام به زاری</p></div>
<div class="m2"><p>آیا بود آن که دست گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بحر فتاده‏ام چو ماهی</p></div>
<div class="m2"><p>باشد که مرا به شست گیرد</p></div></div>
<div class="b2" id="bn10"><p>از فیض امام فیض شاید</p>
<p>جامی ز می الست گیرد</p></div>