---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>شوقت نه سرسریست که از سر به در شود</p></div>
<div class="m2"><p>مهرت نه عارضی است که جای دگر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق تو در ضمیرم و مهر تو در دلم</p></div>
<div class="m2"><p>نوعی نیامده است که با جان به در شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردیست درد هجرت تو کاندر علاج آن</p></div>
<div class="m2"><p>هرچند سعی بیش نمائی بتر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول منم که در غم هجر تو هر شبی</p></div>
<div class="m2"><p>دود دلم به گنبد افلاک پر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانی که بوی برد ز گلزار وصل تو</p></div>
<div class="m2"><p>او را چگونه بی‏گل رویت به سر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشی که شرح وصف کمال رخت شنید</p></div>
<div class="m2"><p>شاید که از حدیث لبت پرگهر شود</p></div></div>
<div class="b2" id="bn7"><p>چون کیمیای مهر تو با فیض همره است</p>
<p>روزی امید هست که این خاک زر شود</p></div>