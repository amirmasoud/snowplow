---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>خدای عزّوجل کارساز بنده‌‏نواز!</p></div>
<div class="m2"><p>مهمّ ما به قدوم ولیّ خویش بساز!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست خویش اسیریم فکک الاسرا</p></div>
<div class="m2"><p>میان خلق غریبیم ای غریب‌نواز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جماعتی که ز ابلیس برده‌‏اند گرو</p></div>
<div class="m2"><p>به مکر و حیله و کبر و حسد به رمز و لماز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اهل علم شبیهند در میان عوام</p></div>
<div class="m2"><p>زنند دم ز حقیقت به نزد اهل مجاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه اثیم و زنمیند، خیر را منّاع</p></div>
<div class="m2"><p>نمیم را همه مشّاء بری‏ء را همّاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماز را همه دشمن هماز را همه دوست</p></div>
<div class="m2"><p>نیاز را همه کاره ز بس رعونت و ناز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به اهل دین همه با قتل و قمع همراهند</p></div>
<div class="m2"><p>که تا به فسق و ستم دست و پا کنند دراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهیمنا تو به زودی امام را بفرست</p></div>
<div class="m2"><p>که تا رهد دل ارباب دین ز سوز و گداز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو او ظهور کند اولین مطیع منم</p></div>
<div class="m2"><p>چرا که در ره تسلیم می‌‏کنم پرواز</p></div></div>
<div class="b2" id="bn10"><p>ز فیض فتنه آخر زمان کفایت کن</p>
<p>خدای عزوجل! کارساز بنده‏‌نواز!</p></div>