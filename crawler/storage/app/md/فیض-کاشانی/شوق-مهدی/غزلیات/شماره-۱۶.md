---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>تو حق‏شناس نئی ای عدو خطا اینجاست</p></div>
<div class="m2"><p>چو بشنوی سخن اهل دل مگو که خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سری به دنیی و عقبی فرو نمی‏آمد</p></div>
<div class="m2"><p>چرا که دوستی اهل بیت در سر ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در اندرون من خسته دل خیال امام</p></div>
<div class="m2"><p>خموش کرد مرا و به خویش در غوغاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز پرده برون شد کنون امیدی هست</p></div>
<div class="m2"><p>اگر ز ناله و فریاد کار ما بنو است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر طرف من سرگشته چند پویم، چند</p></div>
<div class="m2"><p>ره دیار امام زمان کجاست، کجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود میل جهانم ولیک در نظرم</p></div>
<div class="m2"><p>امید آمدن او چنین خوشش آراست‏</p></div></div>
<div class="b2" id="bn7"><p>چو شعله زاتش شوقت مدام سوزد فیض</p>
<p>که آتشی که نمیرد همیشه در دل ماست</p></div>