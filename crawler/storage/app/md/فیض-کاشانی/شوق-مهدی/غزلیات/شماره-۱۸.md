---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>برا امام که بنیاد عمر بر باد است</p></div>
<div class="m2"><p>بیا که قصر امل سخت سست بنیاد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام همت آنم که جز محبت تو</p></div>
<div class="m2"><p>ز هرچه رنگ تعلق پذیرد آزاد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شنیده‏اید که در حق دوستان علی</p></div>
<div class="m2"><p>سروش هاتف غیبم چه مژده‏ها داد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ای ولیّ ولیّ خدا و عترت او</p></div>
<div class="m2"><p>نشیمن تو نه این کنج محنت آباد است‏</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را ز کنگره عرش می‏زنند صفیر</p></div>
<div class="m2"><p>ندانمت که در این دامگه چه افتاد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکایتی کنمت بشنو و شناسا شو</p></div>
<div class="m2"><p>که این حدیث ز پیر شریعتم یاد است:</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجو طهارت مولد ز دشمنان علی</p></div>
<div class="m2"><p>که حمل مادر این قوم از دو داماد است!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی پدر دگر ابلیس هر دو کرده دخول</p></div>
<div class="m2"><p>از اختلاط دو آب آن عدوی من زاد است‏</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پای خود به جهنم رود عدو تو مگوی</p></div>
<div class="m2"><p>که بر من و تو درِ اختیار نگشاد است</p></div></div>
<div class="b2" id="bn10"><p>حسد چه می‏بری ای دشمن علی بر فیض</p>
<p>ولای آل نبی روزی خداداد است‏</p></div>