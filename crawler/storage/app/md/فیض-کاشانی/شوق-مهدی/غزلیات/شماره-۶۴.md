---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>لاف محبت او، بر قدسیان توان زد</p></div>
<div class="m2"><p>از سوز شوقش آتش، در انس و جان توان زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آستان مهدی، گر سر توان نهادن</p></div>
<div class="m2"><p>گلبانگ سربلندی، بر آسمان توان زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دولت وصالش، خواهد دری گشادن</p></div>
<div class="m2"><p>سرها بدین تخیل، بر آستان توان زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جویبار چشمم، گر سایه افکند دوست</p></div>
<div class="m2"><p>بر خاک رهگذارش، آب روان توان زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عدلش چو رو نماید، ظلم و ستم بسوزد</p></div>
<div class="m2"><p>بی‏حضرتش چکاری، بر ظالمان توان زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم و کتاب و سنت، بی او چه ذوق بخشد</p></div>
<div class="m2"><p>جام می مغانه، هم با مغان توان زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبط رسول و قرآن، فهم درست و ایمان</p></div>
<div class="m2"><p>چون جمع شد معانی، گوی بیان توان زد</p></div></div>
<div class="b2" id="bn8"><p>یا رب به وصل مهدی بر فیض مرحمت کن</p>
<p>باشد که گوی دولت، با دوستان توان زد</p></div>