---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>به حدیث آی و علوم خودم از یاد ببر</p></div>
<div class="m2"><p>خرمن دانش ما را همه گو باد ببر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که از لفظ دُر افشان تو علم آموزیم</p></div>
<div class="m2"><p>گو بیا سیل و کتب خانه ز بنیاد ببر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ولایت زده‏ام دست شفاعت محکم</p></div>
<div class="m2"><p>همره خود به بهشت آیدم و شاد ببر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز مرگم نفسی وعده دیدار بده</p></div>
<div class="m2"><p>وانگهم تا به لحد فارغ و آزاد ببر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بمیرم من از این درد به وصلش نرسم</p></div>
<div class="m2"><p>به سر رهگذرش خاک من ای باد ببر</p></div></div>
<div class="b2" id="bn6"><p>سخت در حیرتی از غیبت مهدی ای فیض</p>
<p>این وساوس ز دل، این دغدغه از یاد ببر</p></div>