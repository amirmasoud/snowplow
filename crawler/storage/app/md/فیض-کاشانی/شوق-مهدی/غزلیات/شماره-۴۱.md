---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گر من از باغ تو یک میوه بچینم چه شود</p></div>
<div class="m2"><p>پیش پائی به چراغ تو ببینم چه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب اندر کنف سایه مولای زمان</p></div>
<div class="m2"><p>که من سوخته یک دم بنشینم چه شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر ای خاتم انوار هدایت آثار</p></div>
<div class="m2"><p>گر فتد عکس تو بر لعل نگینم چه شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک نفس جلوه کنی تا که به مرآت رخت</p></div>
<div class="m2"><p>صورت و سیرت جد تو ببینم چه شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به دل مهر تو دارم منگر نیک و بدم</p></div>
<div class="m2"><p>گر چنانم چه بود یا که چنینم چه شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صرف شد عمر گرانمایه به امید واسف</p></div>
<div class="m2"><p>تا از آنم چه به پیش آید و اینم چه شود</p></div></div>
<div class="b2" id="bn7"><p>عمرت ای فیض گر اینسان گذرد روز به روز</p>
<p>دانم از پیش که احوال نسیمم چه شود</p></div>