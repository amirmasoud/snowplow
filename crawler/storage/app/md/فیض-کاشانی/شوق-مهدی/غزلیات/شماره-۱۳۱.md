---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>آرم ای مولای من یک قطره از دریای تو</p></div>
<div class="m2"><p>گفته گویا حافظ این ابیات در سودای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای قبای پادشاهی راست بر بالای تو</p></div>
<div class="m2"><p>زینت تاج و نگین از گوهر والای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب فتح را هر دم طلوعی می‏دهد</p></div>
<div class="m2"><p>در لباس خسروی رخسار مه سیمای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه خورشید فلک چشم و چراغ عالمست</p></div>
<div class="m2"><p>روشنائی‌بخش چشم اوست خاک پای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه‌گاه طایر اقبال گردد هر کجا</p></div>
<div class="m2"><p>سایه اندازد همای چتر گردون‌سای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رسوم شرع و حکمت با هزاران اختلاف</p></div>
<div class="m2"><p>نکته هرگز نشد فوت از دل دانای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه اسکندر طلب کرد و ندادش روزگار</p></div>
<div class="m2"><p>جرعه‌‏ای بود از زلال لعل جان‌افزای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرض حاجت در حریم حضرتت محتاج نیست</p></div>
<div class="m2"><p>راز کس مخفی نماند با فروغ رای تو</p></div></div>
<div class="b2" id="bn9"><p>خسروا پیرانه‌سر فیضت جوانی می‏کند</p>
<p>بر امید عفو جان‏بخش گنه‌فرسای تو</p></div>