---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>زین جهان پیروی آل نبی ما را بس</p></div>
<div class="m2"><p>در قیامت ثمر حب علی ما را بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست ما را که کم و کیف از ایشان پرسیم</p></div>
<div class="m2"><p>ما و تسلیم که آن نصّ جلی ما را بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر اعمال نکوهیده وگر نانیکوست</p></div>
<div class="m2"><p>اعتقادات خوش ما به وصی ما را بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر للّه که ما با همه عالم صلحیم</p></div>
<div class="m2"><p>جنگ با وسوسه نفس دنی ما را بس‏</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذره‏ای بغض کسی در دل ما کی گنجد</p></div>
<div class="m2"><p>بغض هر سه لعین ابدی ما را بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما نخواهیم در این نشأه که سلطان باشیم</p></div>
<div class="m2"><p>لذت بندگی از دار دنی ما را بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض از مال و بزرگی و ریاست بگذر</p></div>
<div class="m2"><p>کنجی و ذوق حدیث نبوی ما را بس‏</p></div></div>