---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>مژده آمدنت داد صبا دوران را</p></div>
<div class="m2"><p>رونق عهد شبابست دگر ایمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صبا گر به مقیمان درش بازرسی</p></div>
<div class="m2"><p>برسان بندگی و خدمت مشتاقان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به منزلگه آن نایب حق ره یابم</p></div>
<div class="m2"><p>خاک‏ روب در آن خانه کنم مژگان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفعت پایه ما خدمت اهل البیت است</p></div>
<div class="m2"><p>نیست حاجت که بر افلاک کشیم ایوان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده آل نبی باش که در کشتی آل</p></div>
<div class="m2"><p>هست خاکی که به آبی نخرد طوفان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسم آن خیره که بر شیعه او می‏خندد</p></div>
<div class="m2"><p>در سر کار تشیع کند آخر جان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه کنعانی من! مسند مصر آن تو شد</p></div>
<div class="m2"><p>وقت آنست که بدرود کنی زندان را</p></div></div>
<div class="b2" id="bn8"><p>یک نظر دیدن رویت ز خدا خواهد فیض</p>
<p>در سرش آن که به پای تو فشاند جان را</p></div>