---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>بخت از قدوم دوست نشانم نمی‏‌دهد</p></div>
<div class="m2"><p>دولت خبر ز راز نهانم نمی‌‏دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان می‏‌دهم برای لقایش به صدق دل</p></div>
<div class="m2"><p>اینم نمی‏‌ستاند و آنم نمی‌‏دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم ز اشتیاق در این پرده راه نیست</p></div>
<div class="m2"><p>یا هست پرده‏‌دار نشانم نمی‌‏دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانم به صبر دست دهد کام دل ولی</p></div>
<div class="m2"><p>بدعهدی زمانه امانم نمی‌‏دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندان که بر کنار چو پرگار می‌‏روم</p></div>
<div class="m2"><p>دوران چو نقطه ره به میانم نمی‏‌دهد</p></div></div>
<div class="b2" id="bn6"><p>گفتم روم به خواب و ببینم جمال دوست</p>
<p>از آه و ناله فیض امانم نمی‏‌دهد</p></div>