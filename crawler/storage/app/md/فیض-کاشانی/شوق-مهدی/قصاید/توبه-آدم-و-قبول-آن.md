---
title: >-
    توبه آدم و قبول آن
---
# توبه آدم و قبول آن

<div class="b" id="bn1"><div class="m1"><p>آدم قبول کرد و دگر توبه تازه کرد</p></div>
<div class="m2"><p>از روی عجز و گفت که اغفر ذنوبنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس نام مصطفا به زبان راند و مرتضی</p></div>
<div class="m2"><p>آنگاه نام فاطمه آن زبدة النسا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام حسن بگفت و حسین و شمردشان</p></div>
<div class="m2"><p>تا چارده تمام شد از آل مصطفا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حق نیز لطف کرد و نوازش نمود و باز</p></div>
<div class="m2"><p>بودش به جای خویش و فزودش در اصطفا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچش ز راه برد هم آن راهبر شدش</p></div>
<div class="m2"><p>زانجا که در دخالت از آنجا شدش دوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردش خلیفه خود و تعلیم او نمود</p></div>
<div class="m2"><p>اسماش با مظاهر از عرش تا ثرا</p></div></div>