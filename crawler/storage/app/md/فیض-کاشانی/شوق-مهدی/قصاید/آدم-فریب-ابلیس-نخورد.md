---
title: >-
    آدم فریب ابلیس نخورد
---
# آدم فریب ابلیس نخورد

<div class="b" id="bn1"><div class="m1"><p>ابلیس دید کآدم خاکی بزرگ شد</p></div>
<div class="m2"><p>از حق نیافت منزلت و جاه و اجتبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیچید همچو مار و شد اندر دهان مار</p></div>
<div class="m2"><p>مارش کشید تا به جنان از ره خفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد به پیش آدم و گفت از ره فریب</p></div>
<div class="m2"><p>ای آنکه سجده کرد تو را اهل اصطفا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان نهی کرده‏اند شما را از این درخت</p></div>
<div class="m2"><p>تا علم غیب حق نشود کشف بر شما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا آنکه در جهان بنمانید جاودان</p></div>
<div class="m2"><p>باشید در بلای بلا معرض فنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تأکید حرف خویش به ایمان نمود و گفت</p></div>
<div class="m2"><p>واللّه ناصح توام و حق بدین گوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آدم بدین گمان که نصیحت گرست مار</p></div>
<div class="m2"><p>غافل از این که دیو در این مار کرده جا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتا که مار! بازی ابلیس خورده‏ای؟</p></div>
<div class="m2"><p>کی بر خدای پاک خیانت بود روا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر به نام او تو قسم یاد می‏کنی</p></div>
<div class="m2"><p>تعظیم چون کنیش چو خائن بود خدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من هم به غیر اذن تناول چسان کنم</p></div>
<div class="m2"><p>کی بی‏رضای او شود این حاجتم روا</p></div></div>