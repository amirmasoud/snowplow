---
title: >-
    عظمت مقام ائمه هدى (ع)
---
# عظمت مقام ائمه هدى (ع)

<div class="b" id="bn1"><div class="m1"><p>آدم ز رشک کرد تمنای علمشان</p></div>
<div class="m2"><p>آورد در خیال بدی کاشکی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد ندا ز غیب به آدم که زینهار</p></div>
<div class="m2"><p>از ما مخواه رتبه این قوم در دعا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نزدیک این درخت مرو آرزو مکن</p></div>
<div class="m2"><p>حدّ تو نیست منزلت سید الوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردیم ما ملائکه را ساجدان تو</p></div>
<div class="m2"><p>زیرا که بود صلب تو این قوم را وعا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این قوم راست جاه زیاد از حد بشر</p></div>
<div class="m2"><p>هستند هرچه هست به عالم به جز خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آوردم از اسمامی خود اسمشان برون</p></div>
<div class="m2"><p>هر جا که حاجتیست بدیشان کنم روا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس را اگر ثواب رسد یا عقوبتی</p></div>
<div class="m2"><p>ایشان سبب شوند و بدیشان کنم قضا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس و قمر نجوم و ملائک سما و ارض</p></div>
<div class="m2"><p>از مهرشان بود همه را گردش رحا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرگه بلا و داهیه‏ای رونهد به تو</p></div>
<div class="m2"><p>این قوم را شفیع خودآور به نزد ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا دفع آن بلا و مصیبت شود ز تو</p></div>
<div class="m2"><p>از بهر قرب و منزلت آل مصطفا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عهدی گرفت ز آدم و حوا به مهرشان</p></div>
<div class="m2"><p>با انقیاد جاه و مقامات ارتضا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عهدی گرفت ز آدم و تأکید آن نمود</p></div>
<div class="m2"><p>تا بر علوّ رتبه ایشان دهد رضا</p></div></div>