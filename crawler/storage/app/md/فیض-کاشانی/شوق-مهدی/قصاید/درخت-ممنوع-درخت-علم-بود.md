---
title: >-
    درخت ممنوع درخت علم بود
---
# درخت ممنوع درخت علم بود

<div class="b" id="bn1"><div class="m1"><p>آدم چو دید مکرمت و سجده و بهشت</p></div>
<div class="m2"><p>گفتا در آسمان و زمین کیست مثل ما؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد ندا که سر به سوی عرش کن ببین</p></div>
<div class="m2"><p>آمد چو دید در نظرش نور مصطفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با نور اهل بیت ز اشباح منعکس</p></div>
<div class="m2"><p>اشباح کرده بود به صلبش ز عرش جا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انوار بس غریب ز صلبش نمود عکس</p></div>
<div class="m2"><p>تا منتهای عرش درخشان و با ضیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عکس آن فتاده مثال و شبح به عرش</p></div>
<div class="m2"><p>تا چارده شبح همه جان‏بخش و دلگشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از علمشان بدید درختی کشیده سر</p></div>
<div class="m2"><p>چو سدره منتهی شده تا اوج منتها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نیک بنگریست همان آن درخت بود</p></div>
<div class="m2"><p>یا عکس آن درخت کزان نهی شد ورا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا نور آن درخت که بودش شمار جمع</p></div>
<div class="m2"><p>بود آن درخت علم در او جمله میوه‏ها</p></div></div>