---
title: >-
    رباعی شمارهٔ ۲۲
---
# رباعی شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>گه در غزلم سخن کشد جانب راز</p></div>
<div class="m2"><p>گاهی بقصیده میشود دور و دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازم برباعی سخن کوته کن</p></div>
<div class="m2"><p>تا باز شود بحرف لب بندد باز</p></div></div>