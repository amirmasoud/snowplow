---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>یا رب تو مرا بکردهٔ زشت مگیر</p></div>
<div class="m2"><p>از معصیتم بگذر و طاعت به پذیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مهر تو و نبی و اولاد نبی</p></div>
<div class="m2"><p>نزد تو شفاعتم کند دستم گیر</p></div></div>