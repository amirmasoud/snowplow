---
title: >-
    رباعی شمارهٔ ۵۲
---
# رباعی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>نی اهل دلی که بشنوم زو رازی</p></div>
<div class="m2"><p>نی هم نفسی که باشدم دمسازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی باشد و کی که با پر و بال فنا</p></div>
<div class="m2"><p>در عالم لامکان کنم پروازی</p></div></div>