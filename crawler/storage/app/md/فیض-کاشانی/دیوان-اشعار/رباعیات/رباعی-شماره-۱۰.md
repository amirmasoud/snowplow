---
title: >-
    رباعی شمارهٔ ۱۰
---
# رباعی شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای فیض بسی موعظه گفتی بعبث</p></div>
<div class="m2"><p>در گوش نکردی درو سفتی بعبث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوری بدل کسی نمی‌بینم من</p></div>
<div class="m2"><p>بس خانهٔ تاریک که رفتی بعبث</p></div></div>