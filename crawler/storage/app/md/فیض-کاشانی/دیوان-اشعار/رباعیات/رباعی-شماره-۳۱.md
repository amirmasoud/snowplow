---
title: >-
    رباعی شمارهٔ ۳۱
---
# رباعی شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>از صحبت خلق سخت دلتنگ شدم</p></div>
<div class="m2"><p>وز دمها چون آینه در زنگ شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس نام نکوی بی مسمی دیدم</p></div>
<div class="m2"><p>از نام نکوی خویش در ننگ شدم</p></div></div>