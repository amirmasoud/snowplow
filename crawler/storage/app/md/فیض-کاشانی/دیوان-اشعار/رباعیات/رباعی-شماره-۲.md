---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>در عهد صبی کرد جهالت پستت</p></div>
<div class="m2"><p>ایام شباب کرد غفلت مستت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پیر شدی رفت نشاط از دستت</p></div>
<div class="m2"><p>کی صید کند مرغ سعادت شستت</p></div></div>