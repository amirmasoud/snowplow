---
title: >-
    رباعی شمارهٔ ۴۱
---
# رباعی شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>ای فیض بیا که عزم می خانه کنیم</p></div>
<div class="m2"><p>پیمان شکنیم و می بپیمانه کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل در ره عشوه‌های ساقی فکنیم</p></div>
<div class="m2"><p>جان در سر غمزهای جانانه کنیم</p></div></div>