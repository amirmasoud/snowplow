---
title: >-
    رباعی شمارهٔ ۴۳
---
# رباعی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ای فیض بس است آنچه خواندی بس کن</p></div>
<div class="m2"><p>از هیچ فن اندرز نماندی بس کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا قوت گفتگوی بودت گفتی</p></div>
<div class="m2"><p>اکنون که ز گفتگوی ماندی بس کن</p></div></div>