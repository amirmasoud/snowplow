---
title: >-
    رباعی شمارهٔ ۳۹
---
# رباعی شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>از نور نبی واقف این راه شدیم</p></div>
<div class="m2"><p>وز مهر علی عارف الله شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پیروی نبی و‌ آلش کردیم</p></div>
<div class="m2"><p>ز اسرار حقایق همه آگاه شدیم</p></div></div>