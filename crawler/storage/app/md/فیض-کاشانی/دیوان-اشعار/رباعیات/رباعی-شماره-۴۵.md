---
title: >-
    رباعی شمارهٔ ۴۵
---
# رباعی شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>شادی و طرب بغمسرائی میکن</p></div>
<div class="m2"><p>تحصیل نوا به بینوائی میکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشین چه ترا برگ شود بی برگی</p></div>
<div class="m2"><p>بر مسند فقر پادشاهی میکن</p></div></div>