---
title: >-
    رباعی شمارهٔ ۴۲
---
# رباعی شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>نبکست بکس بخویش نیکی کردن</p></div>
<div class="m2"><p>آزار کسست خویشتن آزردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصه بخویش میکنی آنچه کنی</p></div>
<div class="m2"><p>نیکی و بدی بکس نشاید کردن</p></div></div>