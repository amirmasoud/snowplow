---
title: >-
    رباعی شمارهٔ ۴۰
---
# رباعی شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>از شرم گناه شاید از خون گریم</p></div>
<div class="m2"><p>از ابر بهار بر خود افزون گریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشگی باید که نامه‌ام شسته شود</p></div>
<div class="m2"><p>چون عمر وفا نمی‌کند چون گریم</p></div></div>