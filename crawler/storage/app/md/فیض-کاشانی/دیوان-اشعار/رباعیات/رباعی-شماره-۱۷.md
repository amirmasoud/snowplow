---
title: >-
    رباعی شمارهٔ ۱۷
---
# رباعی شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>دیدم دیدم که هر چه دیدم حق بود</p></div>
<div class="m2"><p>دیدم دیدم که دید دیدم حق بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم دیدم که می شنیدم از حق</p></div>
<div class="m2"><p>دیدم دیدم که آن شنیدم حق بود</p></div></div>