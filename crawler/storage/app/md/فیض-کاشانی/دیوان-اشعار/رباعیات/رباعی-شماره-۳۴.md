---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>یکچند بگرد خویشتن گردیدم</p></div>
<div class="m2"><p>یکچند ز این و آن خبر پرسیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر بدر خویش بدیدم مقصود</p></div>
<div class="m2"><p>دیدم دیدم که آخرین در دیدم</p></div></div>