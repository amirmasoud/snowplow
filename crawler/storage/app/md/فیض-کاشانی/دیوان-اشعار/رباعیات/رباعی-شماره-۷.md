---
title: >-
    رباعی شمارهٔ ۷
---
# رباعی شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>تا چند ز آب و نان سخن خواهی گفت</p></div>
<div class="m2"><p>خواهی خوردن بروز و شب خواهی خفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز تو را ز تو اگر حق نخرید</p></div>
<div class="m2"><p>در روز جزا نخواهی ارزید بمفت</p></div></div>