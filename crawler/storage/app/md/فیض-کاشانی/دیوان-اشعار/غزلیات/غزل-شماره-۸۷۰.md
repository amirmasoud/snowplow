---
title: >-
    غزل شمارهٔ ۸۷۰
---
# غزل شمارهٔ ۸۷۰

<div class="b" id="bn1"><div class="m1"><p>گه نقاب از رخ کشیدی گه نقاب انداختی</p></div>
<div class="m2"><p>تهمتی بر سایه و بر آفتاب انداختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه نمائی روی و گه پنهان کنی در زیر زلف</p></div>
<div class="m2"><p>زین کشاکش خلقرا در پیچ و تاب انداختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس نشانهای غلط دادی بکوی خویشتن</p></div>
<div class="m2"><p>تشنگان وادیت را در سراب انداختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرم بی‌اندازه‌ات سرهای ما افکند پیش</p></div>
<div class="m2"><p>از حجاب خویش ما را در حجاب انداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف را کردی پریشان پر عذار آتشین</p></div>
<div class="m2"><p>رشتهٔ جان مرا در پیچ و تاب انداختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر امید وعدهٔ فردا ز خود راندی بنقد</p></div>
<div class="m2"><p>عابدانرا در ثواب و در عقاب انداختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق بیچاره را مهجور در عین وصال</p></div>
<div class="m2"><p>چشم گریان سینه بریان دل کباب انداختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهل دل را صاف دادی اهل گلرا درُد درد</p></div>
<div class="m2"><p>عاقلانرا در حساب و در کتاب انداختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض گفتی بس غزل هریک ز دیگر خوبتر</p></div>
<div class="m2"><p>حیرتی در طالبان انتخاب انداختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میشود آخر دلت غواص بحر من لدن</p></div>
<div class="m2"><p>بس در و گوهر که از چشم بر آب انداختی</p></div></div>