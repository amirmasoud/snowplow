---
title: >-
    غزل شمارهٔ ۶۵۱
---
# غزل شمارهٔ ۶۵۱

<div class="b" id="bn1"><div class="m1"><p>از شراب عشق مستی میکنم</p></div>
<div class="m2"><p>با خیالی بت‌پرستی میکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش چشمی و لبی هر دم غزل</p></div>
<div class="m2"><p>میسرایم شور و مستی میکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شراب نرگس مستانهٔ</p></div>
<div class="m2"><p>بیخودی و می پرستی میکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شدم بیمار چشمی کی دگر</p></div>
<div class="m2"><p>یاد روزی تندرستی میکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ندارم بر وصال دوست پای</p></div>
<div class="m2"><p>چارها از تنگدستی میکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تغافلهای او خون میخورم</p></div>
<div class="m2"><p>وز بلندیهاش پستی میکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض از خود لاف هستی کی زنم</p></div>
<div class="m2"><p>هستیم چون اوست هستی میکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میشوم عالی چو پستم میکند</p></div>
<div class="m2"><p>هستی از بالای پستی میکنم</p></div></div>