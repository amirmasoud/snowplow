---
title: >-
    غزل شمارهٔ ۶۷۰
---
# غزل شمارهٔ ۶۷۰

<div class="b" id="bn1"><div class="m1"><p>اگر بدیم و گر نیک خاکسار توایم</p></div>
<div class="m2"><p>فتاده بر ره تو خاک رهگذار توایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلندی سرما خاکساری در تست</p></div>
<div class="m2"><p>بنزد خلق عزیز بم از آنکه خوار توایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی قرار دل ما اگر قراری هست</p></div>
<div class="m2"><p>و گر قرار نداریم بیقرار توایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوی تست بهر سو که میکنیم سفر</p></div>
<div class="m2"><p>بهر دیار که باشیم در دیار توایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر اطاعت تو میکنیم مخلص تو</p></div>
<div class="m2"><p>و گر کنیم گناهی گناه کار توایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر چه در دل ما بگذرد تو آگاهی</p></div>
<div class="m2"><p>اگر ز خلق نهانیم آشکار توایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کردهای بد خویشتن بسی خجلیم</p></div>
<div class="m2"><p>بپوش پردهٔ عفوی که شرمسار توایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه نامه سیاهیم از اطاعت تو</p></div>
<div class="m2"><p>چو فیض دشمن دیویم و دوستدار توایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگوش هوش شنیدم که هاتفی میگفت</p></div>
<div class="m2"><p>غمین مباش که ما یار غمگسار توایم</p></div></div>