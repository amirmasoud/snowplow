---
title: >-
    غزل شمارهٔ ۷۱۴
---
# غزل شمارهٔ ۷۱۴

<div class="b" id="bn1"><div class="m1"><p>بهار آمد بهار آمد چمن شد پر گل و ریحان</p></div>
<div class="m2"><p>نگار آمد نگار آمد دو عالم شد درو حیران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار آمد بهار آمد روانرا تازه کن ای دل</p></div>
<div class="m2"><p>نگار آمد نگار آمد بجانان زنده شو ای جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مفاتیح جنان آمد نعیم جاودان آمد</p></div>
<div class="m2"><p>نسیم جان جان آمد ز سوی روضهٔ رضوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوید خرمی آمد ز بهر سینهٔ غمگین</p></div>
<div class="m2"><p>برات خوشدلی آمد برای دیدهٔ گریان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرح آمد فرح آمد بیرون آ از غم و اندوه</p></div>
<div class="m2"><p>سرور آمد سرور آمد برا از کلبهٔ احزان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشاط آمد نشاط آمد غم و اندوه دل طی شد</p></div>
<div class="m2"><p>بگوشم زان دیار آمد نوید عیش جاویدان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معطر شد دماغ من منور گشت چشم جان</p></div>
<div class="m2"><p>ز بوی زلف دلدار و فروغ طلعت جانان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دستت داد این نعمت بکن از هر دو عالم دل</p></div>
<div class="m2"><p>اثر مگذار ازفیض و برا از عالم امکان</p></div></div>