---
title: >-
    غزل شمارهٔ ۴۷۷
---
# غزل شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>دلبرا درد مرا درمان تو باش</p></div>
<div class="m2"><p>عاشقانرا سر توئی سامان تو مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد بی‌درمان مرا در جان ز تست</p></div>
<div class="m2"><p>هم دوای درد بی درمان تو باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد دل بریانم از تو داغدار</p></div>
<div class="m2"><p>مرهم داغ دل بریان تو باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره تو جان و دل کردم فدا</p></div>
<div class="m2"><p>مر مرا هم دل تو و مرهم تو باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل برفت و جان برفت ایمان برفت</p></div>
<div class="m2"><p>دل تو باش و جان تو باش ایمان تو باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی دلانرا دلبر و دلدار تو</p></div>
<div class="m2"><p>عاشقانرا جان تو و جانان تو باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر هر دو جهان برخواستم</p></div>
<div class="m2"><p>فیض را هم این و هم آن تو باش</p></div></div>