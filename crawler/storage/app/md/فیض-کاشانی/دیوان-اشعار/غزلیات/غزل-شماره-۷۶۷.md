---
title: >-
    غزل شمارهٔ ۷۶۷
---
# غزل شمارهٔ ۷۶۷

<div class="b" id="bn1"><div class="m1"><p>باز آی جان من و جانان من</p></div>
<div class="m2"><p>داغ عشقت تازه شد بر جان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق شورانگیز عالم سوز باز</p></div>
<div class="m2"><p>آتش اندازد بخان و مان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزهٔ شوخ بلای مست تو</p></div>
<div class="m2"><p>شد دگر بر همزن سامان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن نگاه دلفریبت تازه کرد</p></div>
<div class="m2"><p>در دل من درد بی‌درمان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر مژگانت بلای دین و دل</p></div>
<div class="m2"><p>کرد صد جا رخنه در ایمان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش عشق رخت بالا گرفت</p></div>
<div class="m2"><p>شعله زن شد در درون جان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از می لعل لبت جامی بده</p></div>
<div class="m2"><p>آب زن بر آتش سوزان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا بسوز از من به جز نقش خودت</p></div>
<div class="m2"><p>وارهان این مرا از آن من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد هزاران آفرین بر جان عشق</p></div>
<div class="m2"><p>کو نهاد این داغها بر جان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باقی آن آفرین بر جان فیض</p></div>
<div class="m2"><p>گر نگوید این من یا آن من</p></div></div>