---
title: >-
    غزل شمارهٔ ۵۹۷
---
# غزل شمارهٔ ۵۹۷

<div class="b" id="bn1"><div class="m1"><p>دل و جان منزل جانانه کردم</p></div>
<div class="m2"><p>می توحید در پیمانه کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از این افسانها طرفی نبستم</p></div>
<div class="m2"><p>بمستی ترک هر افسانه کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عقل و عاقلان یکسر بریدم</p></div>
<div class="m2"><p>علاج این دل دیوانه کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم در ژنده پنهان از نظرها</p></div>
<div class="m2"><p>چو گنجی جای در ویرانه کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود تا آشنا آن دوست با من</p></div>
<div class="m2"><p>ز هر کس خویش را بیگانه کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر جانب که دیدم مست نازی</p></div>
<div class="m2"><p>نگاهی سوی او مستانه کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر جا حسن او افروخت شمعی</p></div>
<div class="m2"><p>بگردش خویش را پروانه کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم شد فانی اندر عشق باقی</p></div>
<div class="m2"><p>بآخر قطره را دردانه کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر جزو دلم جای بتی بود</p></div>
<div class="m2"><p>بمستی ترک این بتخانه کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیک پیمانه دادم هر دو عالم</p></div>
<div class="m2"><p>چو فیض این کار را مردانه کردم</p></div></div>