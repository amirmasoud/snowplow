---
title: >-
    غزل شمارهٔ ۵۱۹
---
# غزل شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>هر چه نبود سخن یار دروغ است دروغ</p></div>
<div class="m2"><p>جز حدیث لب دلدار دروغ است دروغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار آنست که او با تو بود در همه حال</p></div>
<div class="m2"><p>گوید ار غیر منم یار دروغ است دروغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچکس را بجهان نیست جز او غمخواری</p></div>
<div class="m2"><p>حرف غمخواری اغیار دروغ است دروغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار با ماست بهر جای تو از جای مرو</p></div>
<div class="m2"><p>حرف اغیار دل آزار دروغ است دروغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آید از حسن فروشی چو سروشی در گوش</p></div>
<div class="m2"><p>که چو من نیست ببازار دروغ است دروغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اعتمادی نبود بر سخن نوش لبان</p></div>
<div class="m2"><p>آنچه گفت آن بت عیار دروغ است دروغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن آن یار وفاپیشه باقی حسن است</p></div>
<div class="m2"><p>حسن اغیار جفاکار دروغ است دروغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار یکتا بگزین وز دو جهان دل برگیر</p></div>
<div class="m2"><p>وصف یک چیز به بسیار دروغ است دروغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از من راست شنو فیض ز هر کج مشنو</p></div>
<div class="m2"><p>اوست حق هستی اغیار دروغ است دروغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محرم راز به جز عاشق صادق نبود</p></div>
<div class="m2"><p>زاهد و دعوی این کار دروغ است دروغ</p></div></div>