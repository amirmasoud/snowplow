---
title: >-
    غزل شمارهٔ ۸۳۳
---
# غزل شمارهٔ ۸۳۳

<div class="b" id="bn1"><div class="m1"><p>یا رب این مخمور را در بزم مستان بار ده</p></div>
<div class="m2"><p>وز شراب لایزالی ساغر سرشار ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکدو غمزه زان دو چشمم ساقیا هر بامداد</p></div>
<div class="m2"><p>یکدو بوسه زان لبانم در شبان تار ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور عقل آمد بسر گفتار واعظ شد کساد</p></div>
<div class="m2"><p>عشق را بگشا دکان و رونق بازار ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت مستی و طرب آمد خرد را عذرخواه</p></div>
<div class="m2"><p>بزم مستان را بیارا مطربان را بار ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفر صادق خوشتر از ایمان کاذب آیدم</p></div>
<div class="m2"><p>سبحه بستان از کف من در عوض زنار ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسجد و محراب و منبر پر شد از زرق و ریا</p></div>
<div class="m2"><p>هان در میخانه بگشا راستان را بار ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتشی از عشق افروز اهل غفلت را بسوز</p></div>
<div class="m2"><p>دردها را کن دوا بیمار را تیمار ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهدان خشک را بگذار با جهل و غرور</p></div>
<div class="m2"><p>خیل رندان را می از جام هوالغفار ده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهدان را نیست در خور عشقبازی کار ماست</p></div>
<div class="m2"><p>عام را زین باده کم ده خاص را بسیار ده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میکشد ساقی خمارم باده را تعجیل کن</p></div>
<div class="m2"><p>فیض را از جام باقی عیش بی آزار ده</p></div></div>