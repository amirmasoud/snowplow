---
title: >-
    غزل شمارهٔ ۲۰۸
---
# غزل شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>یا ندیمی قم فان الدّیک صاح </p></div>
<div class="m2"><p>غن لی بیتاً و ناول کاس راح </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لست اصبر عن حبیبی لحظهٔ</p></div>
<div class="m2"><p>هل الیه نظرهٔ منی تباح </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بذل روحی فی هواه هین</p></div>
<div class="m2"><p>یحمد القوم السری عند الصباح </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رام قتلی لحظه من غیرسیف </p></div>
<div class="m2"><p>اسکرتنی عینه من دون راح </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد کفتنی نظرهٔ منی الیه </p></div>
<div class="m2"><p>من بها لی فی غداه اورواح </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هام قلبی فی هواه کیف فاطمان </p></div>
<div class="m2"><p>راح روحی فی قفاه فاستراح </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لم یفارقنی خیال منه قط </p></div>
<div class="m2"><p>لم یزل هو فی فؤادی لایراح </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ان یشا یحرق فؤادی فی النوی </p></div>
<div class="m2"><p>اویشا یقتل له قتلی مباح </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاتنج یا فیض اسرار الحبیب </p></div>
<div class="m2"><p>لیس فی شرع الهوی سریباح </p></div></div>