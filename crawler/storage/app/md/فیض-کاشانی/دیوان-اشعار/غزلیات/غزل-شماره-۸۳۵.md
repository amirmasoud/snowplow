---
title: >-
    غزل شمارهٔ ۸۳۵
---
# غزل شمارهٔ ۸۳۵

<div class="b" id="bn1"><div class="m1"><p>از خودی ای خدا نجاتم ده</p></div>
<div class="m2"><p>زین محیط بلا نجاتم ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکدم از من مرا رهائی بخش</p></div>
<div class="m2"><p>از غم ما سوی نجاتم ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم از وحشت جهان بگرفت</p></div>
<div class="m2"><p>زین دیار فنا نجاتم ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس اماره قصد من دارد</p></div>
<div class="m2"><p>زین دم اژدها نجاتم ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد خاکسترم بباد هوس</p></div>
<div class="m2"><p>از بلای هوا نجاتم ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبت عامه سوخت جانم را</p></div>
<div class="m2"><p>ز آتش بی‌ضیا نجاتم ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلقی افتاده در پی جانم</p></div>
<div class="m2"><p>زین ددان دغا نجاتم ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهل بگرفته سر بسر عالم</p></div>
<div class="m2"><p>زین جنود عما نجاتم ده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نتوانم ز راستی دم زد</p></div>
<div class="m2"><p>زین کجان دغا نجاتم ده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غرقه در بحر غم شدم چون فیض</p></div>
<div class="m2"><p>میزنم دست و پا نجاتم ده</p></div></div>