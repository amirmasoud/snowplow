---
title: >-
    غزل شمارهٔ ۱
---
# غزل شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>فیض نور خداست در دل ما </p></div>
<div class="m2"><p>از دل ماست نور منزل ما </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقل ما نقل حرف شیرینش </p></div>
<div class="m2"><p>یاد آن روی شمع محفل ما </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل از دوست عقدهٔ مشکل </p></div>
<div class="m2"><p>در کف اوست حلّ مشکل ما </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخم محنت بسینهٔ ما کشت </p></div>
<div class="m2"><p>آنکه مهرش سرشته در گل ما </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سالها در جوار او بودیم </p></div>
<div class="m2"><p>سایهٔ دوست بود منزل ما </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در محیط فراق افتادیم </p></div>
<div class="m2"><p>نیست پیدا کجاست ساحل ما </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر بود و وفا که میکشتیم </p></div>
<div class="m2"><p>از چه جور و جفاست حاصل ما </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست و پا بس زدیم بیهوده </p></div>
<div class="m2"><p>داغ دل گشت سعی باطل ما </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل بتیغ فراق شد بسمل </p></div>
<div class="m2"><p> چند خواهد طپید بسمل ما </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونکه خواهد فکند در پایش </p></div>
<div class="m2"><p>سر ما دستمزد قاتل ما </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طپش دل ز شوق دیدار است </p></div>
<div class="m2"><p>به از این چیست فیض حاصل ما </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در سفر تا به کِی تپد دل ما </p></div>
<div class="m2"><p>نیست پیدا کجاست منزل ما </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوی جان میوزد در این وادی </p></div>
<div class="m2"><p>ساربانا بدار محمل ما </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کجا میرویم او با ماست </p></div>
<div class="m2"><p>اوست در جان ما و در دل ما </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان چو هاروت و دل چو ماروتست </p></div>
<div class="m2"><p>ز آسمان اوفتاده در گل ما </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زهرهٔ ماست زهرهٔ دنیا </p></div>
<div class="m2"><p>شهواتست چاه بابل ها </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از الم های این چه بابل </p></div>
<div class="m2"><p>نیست واقف درون غافل ما </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کچک درد تا بسر نخورد</p></div>
<div class="m2"><p>نرود فیل نفس کاهل ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p> فیض از نفس خویشتن ما را </p></div>
<div class="m2"><p>نیست ره سوی شیخ کامل ما </p></div></div>