---
title: >-
    غزل شمارهٔ ۴۴۰
---
# غزل شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>گشتم به بحر و بر پی یار بی سیر</p></div>
<div class="m2"><p>تا پای سعی آبله شد ماندم از سفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خشک و بر گذشتم و جستم نشان وی</p></div>
<div class="m2"><p>از وی نشان نداد نه خشکی مرا نه تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هر که شد دچار گرفتم سراغ او</p></div>
<div class="m2"><p>کز یار بی‌نشان چه دهد بی‌خبر خبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم به لب رسید و نیامد بسر مرا</p></div>
<div class="m2"><p>کس دیده مردهٔ نرسد عمر او بسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد سحر بخواب من آن دزد خواب من</p></div>
<div class="m2"><p>هم دزد را گرفتم و هم خواب را سحر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم ز من چه خواهی و گفتا که جان و دل</p></div>
<div class="m2"><p>گفتم که حاضر است بیا هر دو را ببر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگرفت جان و دل ز من آن یار دلنواز</p></div>
<div class="m2"><p>او جای خود گرفت و شدم من ز خود بدر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیم اگر بخویش دگر باره جان دهم</p></div>
<div class="m2"><p>آن خواب را که روزی من شد در آن سحر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم به فیض خواب ز بیداریت بهست</p></div>
<div class="m2"><p>اینک بخواب دیدی بیداری دگر</p></div></div>