---
title: >-
    غزل شمارهٔ ۶۲۸
---
# غزل شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>بشست یار و زلف یار در بندم خوشا حالم</p></div>
<div class="m2"><p>بدرد بی‌دوای دوست خرسندم خوشا حالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیدم چون وفائی در گلی در گلشن عالم</p></div>
<div class="m2"><p>ز دل خار تعلق یک بیک کندم خوشا حالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون کردم سر از خاک و ندیدم جای آسایش</p></div>
<div class="m2"><p>دگر خود را درون خاک افکندم خوشا حالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجز عشقم نیامد در نظر چیزی درین عالم</p></div>
<div class="m2"><p>از آنرو عشق در جان و دل آکندم خوشا حالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمال دوست در صحرای هستی چون تجلی کرد</p></div>
<div class="m2"><p>وجود خویش را از خویشتن کندم خوشا حالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیالش در نظر پیوسته هست اما پسندم نیست</p></div>
<div class="m2"><p>بدیدار جمالش آرزومندم خوشا حالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی حیران آن رویم گهی آشفته زان رویم</p></div>
<div class="m2"><p>گهی گریم بحال خودگهی خندم خوشا حالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو حرف یار می‌گویم دهانم می‌شود شیرین</p></div>
<div class="m2"><p>دهان چه پای تا سر آنزمان قندم خوشا حالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن خوشنود می‌باشم چو فیض از گفتهای خود</p></div>
<div class="m2"><p>که حرف اوست کان بر خویشتن بندم خوشا حالم</p></div></div>