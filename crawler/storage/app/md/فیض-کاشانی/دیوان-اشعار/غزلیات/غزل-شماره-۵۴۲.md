---
title: >-
    غزل شمارهٔ ۵۴۲
---
# غزل شمارهٔ ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>عاشق که بود غلام معشوق</p></div>
<div class="m2"><p>سرمست علی‌الدوام معشوق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خویشتنش خبر نباشد</p></div>
<div class="m2"><p>دایم مست مدام معشوق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی نکند ز آب انگور</p></div>
<div class="m2"><p>مستیش همه ز جام معشوق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برخواسته از سر دو عالم</p></div>
<div class="m2"><p>پابنده شده بدام معشوق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کام و هوای خویش رسته</p></div>
<div class="m2"><p>کامش همه گشته کام معشوق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گامی ننهاده هیچ جائی</p></div>
<div class="m2"><p>جز بر آثار گام معشوق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گم کرده نشان و نام خود را</p></div>
<div class="m2"><p>گشته است نشان بنام معشوق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحشی صفت از جهان رمیده</p></div>
<div class="m2"><p>وز جان و دلست رام معشوق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش هر قوم با سروشی است</p></div>
<div class="m2"><p>گوش فیض و پیام معشوق</p></div></div>