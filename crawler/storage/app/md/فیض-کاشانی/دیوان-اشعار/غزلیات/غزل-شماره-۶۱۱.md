---
title: >-
    غزل شمارهٔ ۶۱۱
---
# غزل شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>صنمی ماه رو هوس دارم</p></div>
<div class="m2"><p>دو بدو روبرو هوس دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جای دل تا بیابم از زلفش</p></div>
<div class="m2"><p>جستن موبمو هوس دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینچنین صحبتی هوس دارم</p></div>
<div class="m2"><p>می و جام و سبو هوس دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو سر مست چون شد از باده</p></div>
<div class="m2"><p>نعرهٔ های و هو هوس دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب مست تا سحر گشتن</p></div>
<div class="m2"><p>در بدر کو بکو هوس دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کشیدن بنغمهٔ دف و نی</p></div>
<div class="m2"><p>بر سر چار سو هوس دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض چیزی دگر اگر خواهد</p></div>
<div class="m2"><p>من همین آرزو هوس دارم</p></div></div>