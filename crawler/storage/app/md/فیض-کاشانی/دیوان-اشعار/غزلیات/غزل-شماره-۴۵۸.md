---
title: >-
    غزل شمارهٔ ۴۵۸
---
# غزل شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>میبرد دل را هوا دستم تو گیر</p></div>
<div class="m2"><p>پای می‌لغزد ز جا دستم تو گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای دل در دام دنیا بند شد</p></div>
<div class="m2"><p>اوفتادم در بلا دستم تو گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز روشن در ره افتادم به چاه</p></div>
<div class="m2"><p>کور گشتم از قضا دستم تو گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره عصیان بسر گشتم بسی</p></div>
<div class="m2"><p>تا که افتادم ز پا دستم تو گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار چون از دست شد آگه شدم</p></div>
<div class="m2"><p>سر نهادم مر ترا دستم تو گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمدم بر درگهت ای کان لطف</p></div>
<div class="m2"><p>ناتوان گشتم بیا دستم تو گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیکس و بیچاره و درمانده‌ام</p></div>
<div class="m2"><p>عاجز و بی‌دست و پا دستم تو گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست و پائی میزدم تا پای بود</p></div>
<div class="m2"><p>چونکه پایم شد ز جا دستم تو گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون تو دل را سر بصحرا دادهٔ</p></div>
<div class="m2"><p>هم تو خود راهش نما دستم تو گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنگ در لطفت زنم هر دم مباد</p></div>
<div class="m2"><p>کردم از وصلت جدا دستم تو گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیض را بیگانگان افکنده‌اند</p></div>
<div class="m2"><p>ای رحیم آشنا دستم تو گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر سر خاک رهت افتاده خار</p></div>
<div class="m2"><p>یا معز الاولیا دستم تو گیر</p></div></div>