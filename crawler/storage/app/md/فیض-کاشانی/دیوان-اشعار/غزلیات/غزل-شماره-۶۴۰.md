---
title: >-
    غزل شمارهٔ ۶۴۰
---
# غزل شمارهٔ ۶۴۰

<div class="b" id="bn1"><div class="m1"><p>از دور بر خرامش قدت ثنا کنم</p></div>
<div class="m2"><p>نزدیک چون رسی دل و جانرا فدا کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم بزیر پرده ناموس مستیی</p></div>
<div class="m2"><p>تا آنزمان که پرده بر افتد چها کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد راه عقل بسته شود اهل هوش را</p></div>
<div class="m2"><p>گر یک ورق ز دفتر عشق تو وا کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم بسوزد از نفس آتشین من</p></div>
<div class="m2"><p>حرفی ز سوز سینه خود گر ادا کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ریشهٔ ز جان بودم در زمین تن</p></div>
<div class="m2"><p>حاشا ز دست دامن مستی رها کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند ترک عشق و ره عقل پیش گیر</p></div>
<div class="m2"><p>دیوانه‌ام مگر که چنین کارها کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ذره در را بدوائی خریده‌ایم</p></div>
<div class="m2"><p>من آن نیم که درد بدرمان دوا کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آستان دوست نهادم سر نیاز</p></div>
<div class="m2"><p>شاید بروی خویش در فیض وا کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خاک مسکنت فتم و ناله سر کنم</p></div>
<div class="m2"><p>باشد که در دلش ز ره عجز جا کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بهر یکنظر که بسوی من افکند</p></div>
<div class="m2"><p>جا دارد ار هزار سحرگه دعا کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در بحر آتشین بود ار گوهی مراد</p></div>
<div class="m2"><p>تا نایدم بکف بدل و جان شنا کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فیضم گرفته است جهانرا فروغ من</p></div>
<div class="m2"><p>در یوزهٔ علوم ز دفتر چرا کنم</p></div></div>