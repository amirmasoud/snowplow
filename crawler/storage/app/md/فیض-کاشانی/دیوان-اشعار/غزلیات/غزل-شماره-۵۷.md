---
title: >-
    غزل شمارهٔ ۵۷
---
# غزل شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>گفتمش دل بر آتش تو کباب </p></div>
<div class="m2"><p>گفت جانها زماست در تب و تاب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش اضطراب دلها چیست</p></div>
<div class="m2"><p>گفت آرام سینه های کباب </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش اشک راه خوابم بست </p></div>
<div class="m2"><p>گفت کی بود عاشقانرا خواب </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش بهر عاشقان چکنی </p></div>
<div class="m2"><p>گفت بر گیرم از جمال نقاب </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش پرده جمال تو چیست </p></div>
<div class="m2"><p>گفت بگذر زخویشتن در ایاب </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش تاب آن جمالم نیست </p></div>
<div class="m2"><p>گفت چون بی تو گردی اری تاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش باده لب لعلت </p></div>
<div class="m2"><p>گفت از حسرتش توان شد آب </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش تشنهٔ وصال توام </p></div>
<div class="m2"><p>گفت زین می کسی نشد سیراب </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش جان و دل فدا کردم </p></div>
<div class="m2"><p>گفت آری چنین کنند احباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش مرد فیض در غم تو </p></div>
<div class="m2"><p>گفت طوبی لهم و حسن مآب </p></div></div>