---
title: >-
    غزل شمارهٔ ۶۴۸
---
# غزل شمارهٔ ۶۴۸

<div class="b" id="bn1"><div class="m1"><p>گر دل به عشق من دهی بهر تو دلداری کنم</p></div>
<div class="m2"><p>ور تن بحکم من نهی جان ترا یاری کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی شود گر آرزوت از عشق خود مستت کنم</p></div>
<div class="m2"><p>مخمور اگر باشی ترا از غمزه خماری کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاری اگر خواهی جلیس من باشمت یار و انیس</p></div>
<div class="m2"><p>خواهد دلت گر گفتگو بهر تو گفتاری کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم دلت روشن شود خار گلت گلشن شود</p></div>
<div class="m2"><p>چون روی سوی من کنی من هم ترا یاری کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک لحظه هشیار ار شوی ساقی شوم ساغر دهم</p></div>
<div class="m2"><p>دور از تو بیمار ار شوی من رسم تیماری کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد ترا درمان کنم کار ترا سامان کنم</p></div>
<div class="m2"><p>عیب ترا پنهان کنم بهر تو ستاری کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیل ترا قوه دهم چند ترا نصرت دهم</p></div>
<div class="m2"><p>بر دشمنان جان تو آئین پیکاری کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون یار غمخوارت منم کف بر مدار از دامنم</p></div>
<div class="m2"><p>تا من ترا یاری کنم تا لطف و غمخواری کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض اینجواب آنغزل از شعر مولانا که گفت</p></div>
<div class="m2"><p>کاری ندارد این جهان تا چند گل کاری کنم</p></div></div>