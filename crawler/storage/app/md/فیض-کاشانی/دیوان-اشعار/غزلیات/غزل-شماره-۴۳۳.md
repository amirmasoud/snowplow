---
title: >-
    غزل شمارهٔ ۴۳۳
---
# غزل شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>از نکاه نیم مستت العیاذ</p></div>
<div class="m2"><p>وز بلای زلف شستت العیاذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر صف دلها زد و تاراج کرد</p></div>
<div class="m2"><p>فتنهای چشم مستت العیاذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ز من بردی و قصد جان کنی</p></div>
<div class="m2"><p>کی برم من جان ز دستت العیاذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف بگشا موبمو وارس به بین</p></div>
<div class="m2"><p>هیچ دل از دام رستت العیاذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از میانت نیست چیزی در میان</p></div>
<div class="m2"><p>وز دهان نیست هستت العیاذ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سرا پا هرچه داری الحذر</p></div>
<div class="m2"><p>پای تا سر هر چه هستت العیاذ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض از تو هم پناه آرد بتو</p></div>
<div class="m2"><p>گرنه پروای منست العیاذ</p></div></div>