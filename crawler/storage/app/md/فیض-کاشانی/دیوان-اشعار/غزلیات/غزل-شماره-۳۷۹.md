---
title: >-
    غزل شمارهٔ ۳۷۹
---
# غزل شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>زنده‌دل از چه رو بدن عالم نور می‌رود</p></div>
<div class="m2"><p>جاهل مرده‌دل ز گور هم سوی گور می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طالب نشئهٔ بقا سوی خدا روان شود</p></div>
<div class="m2"><p>دستهٔ نشئهٔ فنا سوی ثبور می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه درین سرا بدید نشئه آخرت، بدید</p></div>
<div class="m2"><p>در ره او ز پیش و پس رایت نور می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان که ز نشئهٔ دگر کور بود درین سرا</p></div>
<div class="m2"><p>چون برود ازین سرا هم کر و کور می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه ز تقویش لباس افسر علم بر سرش</p></div>
<div class="m2"><p>وان که به معصیت تنید ناقص و عور می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه ز کینه و حسد آتش خشم برفروخت</p></div>
<div class="m2"><p>او ز تنور آتشی سوی تنور می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی بهشتی میرود هرکه به اختیار مرد</p></div>
<div class="m2"><p>قعر جحیم جا کند آن که به زور می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه بخشم مبتلا راست چو مار می‌شود</p></div>
<div class="m2"><p>وآن که اسیر حرص شد خوار چو مور می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غفلت فیض بین که چون غره گفتگو شده</p></div>
<div class="m2"><p>ماتم خود گذاشته در پی سور می‌رود</p></div></div>