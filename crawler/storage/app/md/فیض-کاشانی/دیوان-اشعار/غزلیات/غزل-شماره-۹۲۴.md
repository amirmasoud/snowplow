---
title: >-
    غزل شمارهٔ ۹۲۴
---
# غزل شمارهٔ ۹۲۴

<div class="b" id="bn1"><div class="m1"><p>ز رویت حاصل عشاق حیرانیست حیرانی</p></div>
<div class="m2"><p>از آن زلف و از آن کاکل پریشانی پریشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بزم عشرت وصلت همه حرمان و نومیدی</p></div>
<div class="m2"><p>ز جام شربت هجرت همه خون دل ارزانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانستم که مه رویان بعهد خود نمیپایند</p></div>
<div class="m2"><p>از آن عهد و از آن پیمان پشیمانی پشیمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبادا هیچ کافر را چنین حالی که من دارم</p></div>
<div class="m2"><p>جفا تا کی کنی جانا مسلمانی مسلمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تغافل میکنی یعنی که دردت را نمیدانم</p></div>
<div class="m2"><p>نه میدانی و میدانی که میدانم که میدانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاور بر سرم جانا سپاه بی‌کران غم</p></div>
<div class="m2"><p>ز بیداد و جفا و محنت و جور آنچه بتوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو تا بی‌صبر باشی فیض او بی‌رحم خواهد بود</p></div>
<div class="m2"><p>دلت را شیشکی آئین دلش را پیشه سندانی</p></div></div>