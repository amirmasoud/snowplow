---
title: >-
    غزل شمارهٔ ۶۸۱
---
# غزل شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>ما را ره توفیق نمودند و بریدیم</p></div>
<div class="m2"><p>بر ما در تحقیق گشودند و رسیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکچند بهر صومعه بدیم ارادت</p></div>
<div class="m2"><p>یکچند بهر مدرسه گفتیم و شنیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اقلیم معارف همه را سیر نمودیم</p></div>
<div class="m2"><p>در باغ حقایق بهمه سبزه چریدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس عطر روانبخش ز گلها که گرفتیم</p></div>
<div class="m2"><p>بس میوه دلپرور دلخواه که چیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردیم نظر در شجر زینت دنیا</p></div>
<div class="m2"><p>نه سایه نه برداشت ازو مهر بریدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگاه شد از قرب نمودار درختی</p></div>
<div class="m2"><p>مقصود دل آن بود بکنهش چو رسیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دادند بما عیش مصفای مؤبد</p></div>
<div class="m2"><p>در سایهٔ آن رحل اقامت چو کشیدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدیم چو ما ساقی میخانهٔ توحید</p></div>
<div class="m2"><p>یکجرعه از آن بادهٔ بیرنگ چشیدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد شکر دل آسود ز تشویش کشاکش</p></div>
<div class="m2"><p>چون فیض نه پیر و نه فقیه و نه مریدیم</p></div></div>