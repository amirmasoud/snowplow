---
title: >-
    غزل شمارهٔ ۴۸۸
---
# غزل شمارهٔ ۴۸۸

<div class="b" id="bn1"><div class="m1"><p>بتی از دور اگر بینی مرو پیش</p></div>
<div class="m2"><p>که من دیدم سزای خویش از خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکوی دلبری افتد گذارت</p></div>
<div class="m2"><p>بهر دو دست گیر ای دل سر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن کو صد بلا می‌آید از پس</p></div>
<div class="m2"><p>در آن کو صد خطر میخیزد از پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود تن زار و جان مأوای انوار</p></div>
<div class="m2"><p>جگر از غصه خون دل از جفا ریش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی از غمزهٔ بر دل خورد نیر</p></div>
<div class="m2"><p>گه از مژگانی آید بر جگر نیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه از زلفی بجان آید کمندی</p></div>
<div class="m2"><p>گه از گیسوئی افتد دل بتشویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چها از عشق اینان من کشیدم</p></div>
<div class="m2"><p>هنوزم تا چه آید بعد ازین پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبیبانرا ز غم دل خون شود خون</p></div>
<div class="m2"><p>اگر دستی نهندم بر دل ریش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برسوائی کشد آخر مرا کار</p></div>
<div class="m2"><p>ندارم طاقت کتمان ازین پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر عشق خدائی گیردم دست</p></div>
<div class="m2"><p>که سازم عاشقی را مذهب و کیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رساند تا مرا آخر بجائی</p></div>
<div class="m2"><p>که نبود حد انسانی ازین بیش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدایا فیض را عشق رسائی</p></div>
<div class="m2"><p>کرم کن از محبت خانهٔ خویش</p></div></div>