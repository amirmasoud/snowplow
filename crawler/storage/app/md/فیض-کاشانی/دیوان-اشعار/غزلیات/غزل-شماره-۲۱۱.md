---
title: >-
    غزل شمارهٔ ۲۱۱
---
# غزل شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>خطیب عشق ندا کرد با زبان فصیح </p></div>
<div class="m2"><p>که خلق جمله مریضند و عاشق است صحیح </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان گشاد دگر بار بر سر منبر </p></div>
<div class="m2"><p>که اهل عشق جوادند و اهل زهد شحیح </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگرچه خوش نگین گفت خلق بی نمکند </p></div>
<div class="m2"><p>مگر سری که زشور محبت است ملیح </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شجاع نیست مگر عاشقی که جان بخشد </p></div>
<div class="m2"><p>شود صحیح که گردد بتیغ عشق جریح </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسروری رسد آخر زپافتاده عشق </p></div>
<div class="m2"><p>شود رفیع که افتد ز راه دوست طریح </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یمدح عاشق و معشوق و عشق در قرآن </p></div>
<div class="m2"><p>یحبهم و یحبونه کند تصریح </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذلیل دوست بود عاشق و عزیز عدو </p></div>
<div class="m2"><p>اذله و اعزّه بفیض گفت صریح </p></div></div>