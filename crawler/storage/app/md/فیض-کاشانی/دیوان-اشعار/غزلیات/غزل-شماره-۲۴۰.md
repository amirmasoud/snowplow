---
title: >-
    غزل شمارهٔ ۲۴۰
---
# غزل شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>کسی کو چشم دل بیدار دارد </p></div>
<div class="m2"><p>نظر پیوسته با دلدار دارد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر جا بنگرد چشم خدا بین </p></div>
<div class="m2"><p>تماشای جمال یار دارد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تماشا در تماشا باشد آن را </p></div>
<div class="m2"><p>که در دل دیده بیدار دارد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل هشیار هر جا افکند چشم </p></div>
<div class="m2"><p>روان چشم را بیدار دارد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تماشا باشدش پیوسته آنکو </p></div>
<div class="m2"><p>سرمست و دل هشیار دارد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی کو میتواند عشق ورزید </p></div>
<div class="m2"><p>نشاید خویش را بیکار دارد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درون شادست و خرم عاشقانرا </p></div>
<div class="m2"><p>برونشان گرچه حال زار دارد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلش با دوست تن با غیر عاشق </p></div>
<div class="m2"><p>دل خرم تن بیمار دارد </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه پروا دارد از تاریکی زلف </p></div>
<div class="m2"><p>که از شمع رخش انوار دارد </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو روزی فیض را مهلت ده ای عمر </p></div>
<div class="m2"><p>دلش با عشقبازی کار دارد </p></div></div>