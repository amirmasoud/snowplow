---
title: >-
    غزل شمارهٔ ۶۶۶
---
# غزل شمارهٔ ۶۶۶

<div class="b" id="bn1"><div class="m1"><p>دل میکنمت فدا و جان هم</p></div>
<div class="m2"><p>از تست اگر چه این و آن هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را بر تو چه قدر باشد</p></div>
<div class="m2"><p>یا جان کسی و یا جهان هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر روی زمین ندیده چشمی</p></div>
<div class="m2"><p>ماهی چو زتو بر آسمان هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ملک و ملک نظیر تو نیست</p></div>
<div class="m2"><p>در هشت بهشت جاودان هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جائی که نهی تو پای آنجا</p></div>
<div class="m2"><p>ما سر بنهیم و قدسیان هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهمان شوی ار شبی مارا تو</p></div>
<div class="m2"><p>دل پیش کشم ترا و جان هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بر سر خوان به جز تو نبود</p></div>
<div class="m2"><p>مهمان باشی و میزبان هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گم گشتهٔ وادی غمت را</p></div>
<div class="m2"><p>بی‌نام بمان و بی‌نشان هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض از تو و جان و دل هم از تو</p></div>
<div class="m2"><p>این باد فنای تو و آن هم</p></div></div>