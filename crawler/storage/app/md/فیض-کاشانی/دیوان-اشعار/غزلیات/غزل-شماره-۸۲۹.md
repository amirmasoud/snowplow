---
title: >-
    غزل شمارهٔ ۸۲۹
---
# غزل شمارهٔ ۸۲۹

<div class="b" id="bn1"><div class="m1"><p>بیا زاهد مرا با حضرت تو کار افتاده</p></div>
<div class="m2"><p>ز کردارت نگویم کار با گفتار افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا جمع است خاطر از ره عقبی دلت خوش باد</p></div>
<div class="m2"><p>مرا زین ره ولیکن عقدهٔ بسیار افتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنزد تست آسان زهد چون او را ندیدستی</p></div>
<div class="m2"><p>بنزد من ولی این کار بس دشوار افتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو پنداری به جز راه تو راهی نیست سوی حق</p></div>
<div class="m2"><p>دلت در پردهٔ پندار از این پندار افتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حسن روی ساقی و ز صوت دلکش مطرب</p></div>
<div class="m2"><p>مرا سر رفته از دوش ار ترا دستار افتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا زهد و مرا مستی ترا تقوی مرا رندی</p></div>
<div class="m2"><p>ترا آن کار افتاده مرا این کار افتاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا راه مسلمانی گوارا باد و ارزانی</p></div>
<div class="m2"><p>مرا گبری خوش آمد کار با زنار افتاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توئی در بند آرایش منم در بند افزایش</p></div>
<div class="m2"><p>توئی بر مسند عزت من اینجا خوار افتاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توئی در بند دستار و منم در بستن زنار</p></div>
<div class="m2"><p>توئی بر منبر و من بر در خمار افتاده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم چون فیض بر کاری که آن تقدم بکار آید</p></div>
<div class="m2"><p>تو از کاری که کار آید ترا بیکار افتاده</p></div></div>