---
title: >-
    غزل شمارهٔ ۵۹۸
---
# غزل شمارهٔ ۵۹۸

<div class="b" id="bn1"><div class="m1"><p>گران شد بر دل من تن بیا تن گرد جان گردم</p></div>
<div class="m2"><p>همه تن می‌شوم شاید بر جانان روان گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو جانرا او بود جانان ز سر تا پای گردم جان</p></div>
<div class="m2"><p>جهانرا چون بود او جان بجان گیرد جهان گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گران جان نیستم گر من سبک بیرون روم از تن</p></div>
<div class="m2"><p>زمین تا کی توان بودن بیا تا آسمان گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر آنکه تا بینم رخ پیدای پنهانش</p></div>
<div class="m2"><p>نشان از وی چو نتوان یافت هم خود بی‌نشان گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس جستم نشان او نشان گشتم بجست و جو</p></div>
<div class="m2"><p>ز سر تا پا زان باشم ز پا تا سر بیان گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اوصاف جمال او کنم تا نکتهٔ روشن</p></div>
<div class="m2"><p>بپیچ و تاب چون زلفان بگرد گلرخان گردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدور آتش روئی پریشان چون دخان باشم</p></div>
<div class="m2"><p>ندارد عشق چون پیری بیا من هم جوان گردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدم در عشق پیر و او جوانی می‌کند با من</p></div>
<div class="m2"><p>چو تیرم میکند تیرم کمان خواهد کمان گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهادم سر بفرمانش چو گویم پیش چوگانش</p></div>
<div class="m2"><p>گر این خواهد من این باشم ورآنخواهد من آن گردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجم گر می‌کند گر راست فزونم میکند گر کاست</p></div>
<div class="m2"><p>چنین خواهد چنین باشم چنان خواهد چنان گردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان بودم که میدانی چنین گشتم که می‌بینی</p></div>
<div class="m2"><p>خزان خواهد بسوی اصل بی‌برگی خزان گردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهارم خواهد او از جان برویم لاله و ریحان</p></div>
<div class="m2"><p>ز من خیری که او جوید همان باشم همان گردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنم او را که او گوید روم آنجا که او پوید</p></div>
<div class="m2"><p>چو اینم میکند اینم چو آنم کرد آن گردم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهی هشیار و گه مستم گهی بالا گهی پستم</p></div>
<div class="m2"><p>؟؟؟ان گردم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلم یک شعله بود از عشق بیرون رفت از دستم</p></div>
<div class="m2"><p>بیا ای فیض تا در ماتم دل مشتغل گردم</p></div></div>