---
title: >-
    غزل شمارهٔ ۱۴۶
---
# غزل شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>نیست از ما غیر نامی اوست خود را دوست دوست </p></div>
<div class="m2"><p>نیست ما را مائی اگر مائی هست اوست اوست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه در عالم بود او راست مغز و پوستی </p></div>
<div class="m2"><p>مغز او معلای او و صورت او پوست پوست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت ار چه شد هویدا لیک سرتا پا قفاست </p></div>
<div class="m2"><p>معنی ار چه شد نهان لیکن سراسر روست روست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی هر چیز تسبیح خدا و حمد او </p></div>
<div class="m2"><p>صورت او پرده او سر معنی اوست اوست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با زبان فطرت اصلی است تسبیح همه </p></div>
<div class="m2"><p>نیست تکلیفی برایشان طبعشانرا خوست خوست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارفانند اهل معنی مغز می بینند مغز </p></div>
<div class="m2"><p>جاهلانند اهل صورت ناظران پوست پوست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نیم از عارفان و نیستم از جاهلان </p></div>
<div class="m2"><p>ازکف بحر معانی روزی من جوست جوست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ندارم ره بدریا کرده ام با جوی خوی</p></div>
<div class="m2"><p>چون ندارم ره بمجلس مسکن من کوست کوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض را دیدم بگلزار حقیقت در طواف </p></div>
<div class="m2"><p>گفتمش دریافتی گفتا نصیب بوست بوست </p></div></div>