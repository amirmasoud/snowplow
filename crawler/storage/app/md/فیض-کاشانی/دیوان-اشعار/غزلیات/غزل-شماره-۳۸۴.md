---
title: >-
    غزل شمارهٔ ۳۸۴
---
# غزل شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>من در او میزنم امروز، باشد وا شود</p></div>
<div class="m2"><p>گر تو داری صبر زاهد، باش تا فردا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میزنم بر شمع رویش خویش را پروانه‌وار</p></div>
<div class="m2"><p>تا بسوزم در جمالش لای من الا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب چشم آخر بخواهد بردنم تا کوی دوست</p></div>
<div class="m2"><p>قطره قطره جمع گردد عاقبت دریا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتوی از مهر رویت گر بتابد بر زمین</p></div>
<div class="m2"><p>بگذرد از آسمان عرش برینش جا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف اگر از روی چون خورشید یکسو افکنی</p></div>
<div class="m2"><p>از فروغ نور رویت هر دو عالم لا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر صبا از زلف مشگینت نسیمی آورد</p></div>
<div class="m2"><p>عاشقان را مو بمو آشفته و شیدا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بمیدان دست آری سوی چوگان در زمان</p></div>
<div class="m2"><p>صد هزاران گوی سر از هر طرف پیدا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از غلاف مهر تیغ قهر چون بیرون کشی</p></div>
<div class="m2"><p>بهر سبقت در میان عاشقان غوغا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم مستت گر نظر بر نرگسستان افکند</p></div>
<div class="m2"><p>دیدهٔ نرگس ز فیض آن نظر بینا شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در وجودش کی تواند کرد شک دیگر کسی</p></div>
<div class="m2"><p>آن دهان نیست هستت گر بحرفی وا شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناصحا عیب من بی‌دل برسوائی مکن</p></div>
<div class="m2"><p>هر کسی کو عشق ورزد لاجرم دانا شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان بخواهی داد فیض آخر تو در سودای او</p></div>
<div class="m2"><p>آری آری اهل دلرا سر درین سودا شود</p></div></div>