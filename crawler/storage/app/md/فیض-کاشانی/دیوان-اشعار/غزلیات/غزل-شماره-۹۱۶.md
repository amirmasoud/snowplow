---
title: >-
    غزل شمارهٔ ۹۱۶
---
# غزل شمارهٔ ۹۱۶

<div class="b" id="bn1"><div class="m1"><p>سرکشیهای جوانان تا کی</p></div>
<div class="m2"><p>دل ما در غم اینان تا کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پریشانی زلف ایشان</p></div>
<div class="m2"><p>دل عشاق پریشان تا کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از فسونهای خوش خوش چشمان</p></div>
<div class="m2"><p>چشم و دل واله و حیران تا کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سراپای سراپا سوزان</p></div>
<div class="m2"><p>شعله سان سینه فروزان تاکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دل ریش و تن خستهٔ زار</p></div>
<div class="m2"><p>دور از آن مرهم و درمان تا کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوست را راتبه حرمان تا چند</p></div>
<div class="m2"><p>غیر را وصل فراوان تا کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض از سرّ حقیقت دم زن</p></div>
<div class="m2"><p>این سخنهای پریشان تا کی</p></div></div>