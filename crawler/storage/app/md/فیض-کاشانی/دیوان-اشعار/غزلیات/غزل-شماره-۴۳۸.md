---
title: >-
    غزل شمارهٔ ۴۳۸
---
# غزل شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>از پای تا سر چشم شو حسن و جمالش را نگر</p></div>
<div class="m2"><p>ور ره نیایی سوی او بنشین جمالش را نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نرمش ار بارت دهد از چشم مستش باده کش</p></div>
<div class="m2"><p>زان باده چون دل خوش شوی غنج و دلالش را نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیسوی عنبر بوی او وان زلف تو بر توی او</p></div>
<div class="m2"><p>وان نرگس جادوی او آن خط و خالش را نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افسونگری‌ها را به بین وان جادوئیها را ببین</p></div>
<div class="m2"><p>صد فتنه آرد یکنظر چشم غزالش را نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خنده شیرین او بس زهره بین شادی کنان</p></div>
<div class="m2"><p>وز عارض و ابروی او بدر و هلالش را نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکدم به پیش او نشین کان حیا و شرم بین</p></div>
<div class="m2"><p>یکره برویش کن نظر وان انفعالش را نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک بوسه از لعلش بگیر زان زندهٔ جاوید شو</p></div>
<div class="m2"><p>لعل مذابش را بچش آب زلالش را نگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خنده زیر لبش رمز جمالش فهم کن</p></div>
<div class="m2"><p>وز ناز واز تمکین او جاه و جلالش را نگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای پند گوی هوشمند جان و دلم را شد پسند</p></div>
<div class="m2"><p>از روی و مویش بند و بند پندی مگو بندی میار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من واله جانانه‌ام از خویشتن بیگانه‌ام</p></div>
<div class="m2"><p>عاقل نیم دیوانه‌ام دیوانه را کاری مدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیوانه را تدبیر چیست جزبند و جززنجیر چیست</p></div>
<div class="m2"><p>این وعظ و این تذکیر چیست یکدم مرا با من گذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل از جهان بگسسته‌ام در زلف جانان بسته‌ام</p></div>
<div class="m2"><p>از خویشتن هم رسته‌ام با غیر یارم نیست کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من ترک مستی چون کنم روسوی پستی چون کنم</p></div>
<div class="m2"><p>در عشق سستی چون کنم عشقست عالم را مدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از من مجو صبر و درنک بگذار حرف عار و ننگ</p></div>
<div class="m2"><p>نی صبر دارم نی در نک نه ننگ میدانم عار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاشق ملامت جو بود راه سلامت کی رود</p></div>
<div class="m2"><p>رسوائی او را میسزد با وعظ و پند او را چکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای واعظ عاقل نما فیض از کجا پند از کجا</p></div>
<div class="m2"><p>بگذر تو از تقصیر ما جرم از مجانین در گذر</p></div></div>