---
title: >-
    غزل شمارهٔ ۲۳۱
---
# غزل شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>مرا دردیست در دل نه چو هر درد</p></div>
<div class="m2"><p>دوای آن نه در گرمست و نه سرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوای درد من دردیست سوزان</p></div>
<div class="m2"><p>که آتش در زند در گرم و در سرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوای درد من درد رسائیست</p></div>
<div class="m2"><p>که از هر درد بیرون آورد گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوای درد من دردیست شافی</p></div>
<div class="m2"><p>که روبد از دل و جان گرد هر درد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیبی مشفقی ربانیی کو</p></div>
<div class="m2"><p>که دردم را تواند چارهٔ کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوایی خواهم از دست طبیبی</p></div>
<div class="m2"><p>که تا گردم سراپا جملگی درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی‌بینم بعالم سرخ روئی</p></div>
<div class="m2"><p>نهم تا بر در او چهرهٔ زرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسوی اولیای حق نشانی</p></div>
<div class="m2"><p>بنزد کیست یا رب از زن و مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی گشتم بسی جستم ندیدم</p></div>
<div class="m2"><p>کسی کو باشدش یکذره زین درد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه عمرم درین سودا بسر شد</p></div>
<div class="m2"><p>نه مقصودم بدست آمد نه هم درد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنه دل فیض بر دردی که داری</p></div>
<div class="m2"><p>خوشا حال کسی کو دارد این درد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طبیب حق دوا جز درد حق نیست</p></div>
<div class="m2"><p>بدرد او شفا یابی ز هر درد</p></div></div>