---
title: >-
    غزل شمارهٔ ۸۸۳
---
# غزل شمارهٔ ۸۸۳

<div class="b" id="bn1"><div class="m1"><p>دل آواره را در کوی خود آواره تر کردی</p></div>
<div class="m2"><p>من بیچاره را در عشق خود بیچاره تر کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم خو کارهٔ ذوق شراب حسن خوبان بود</p></div>
<div class="m2"><p>ز چشم و لب شرابم دادی و خو کاره تر کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مردم چشم مستت خون دل میخورد مژگانرا</p></div>
<div class="m2"><p>بزهر آلودی و آنمست را خونخواره تر کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل مردم ربودن بیخبر هاروت نتواند</p></div>
<div class="m2"><p>ازو این غمزه را در دلبری سحاره تر کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بغیر از عشق مه رویان نمیکردم دگر کاری</p></div>
<div class="m2"><p>تو کردی کارها با من مرا این کاره تر کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بچشم دانشت نظاره بودم تاکنون اکنون</p></div>
<div class="m2"><p>ز بینش سرمهٔ بخشیدیم نظاره تر کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگاهت هر زمان از فیض نوعی میرباید دل</p></div>
<div class="m2"><p>مگر چشمانت را در دلبری عیاره تر کردی</p></div></div>