---
title: >-
    غزل شمارهٔ ۶۹۴
---
# غزل شمارهٔ ۶۹۴

<div class="b" id="bn1"><div class="m1"><p>ای دل بیا که تا بخدا التجا کنیم</p></div>
<div class="m2"><p>وین درد خویش را ز در او روا کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید بگسلیم ز بیگانگان تمام</p></div>
<div class="m2"><p>زین پس دگر معامله با آشنا کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر در نهیم در ره او هرچه باد باد</p></div>
<div class="m2"><p>تن در دهیم و هر چه رسد مر جفا کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دوست دوست دارد و ما خون دل خوریم</p></div>
<div class="m2"><p>از دشمن حسود شکایت چرا کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او هرچه میکند چه صوابست و محض خیر</p></div>
<div class="m2"><p>پس ما چرا حدیث ز چون و چرا کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون امر و نهی او همه نهی صلاح ماست</p></div>
<div class="m2"><p>فاسد شویم گر ز اطاعت ابا کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرمانبریم گفتهٔ حق را ز جان و دل</p></div>
<div class="m2"><p>هرچه آن نکرده‌ایم ازین پس قضا کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنرا که حق نکرده قضا چون نمیشود</p></div>
<div class="m2"><p>هیچست ما ز هیچ دل بسته وا کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیهوده است خوردن غم بهر قوه هیچ</p></div>
<div class="m2"><p>شادی بیا ز دل گره غصه وا کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تغییر حکم چون سخط ما نمیکند</p></div>
<div class="m2"><p>کوشیم تا بسعی سخط را رضا کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راضی شویم حکم قضای قدیم را</p></div>
<div class="m2"><p>چون عاجزیم از آنکه خلاف قضا کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر کارها چو بند مشیت نهاد حق</p></div>
<div class="m2"><p>ما نیز کار خود بمشیت رها کنیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خویش میکشیم جفائی که میکشیم</p></div>
<div class="m2"><p>بر خویش میکنیم چو بر کس جفا کنیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای فیض گفتهٔ تو همه محض حکمت است</p></div>
<div class="m2"><p>کوشیم تا به پند تو دردی دوا کنیم</p></div></div>