---
title: >-
    غزل شمارهٔ ۵۴
---
# غزل شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>آنکه را هستی همیشه در طلب </p></div>
<div class="m2"><p>در تو پنهان است از خود می طلب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانچه میجوئی بروز و شب نشان </p></div>
<div class="m2"><p>در بر تو حاضر است او روز و شب </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تار و پود هیکلت او می تند </p></div>
<div class="m2"><p>در دلت از وی فتد شور و شغب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فراق او تن تو در گداز </p></div>
<div class="m2"><p>رشتهٔ جانت از او در تاب و تب </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی او سوی تو ای غافل زخود </p></div>
<div class="m2"><p>چشم بگشا هان چه شدپاس ادب </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مایهٔ شادی درون جان تست </p></div>
<div class="m2"><p>از چه غم داری تو ای کان طرب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکنفس از دیدنش فارغ مباش </p></div>
<div class="m2"><p>در لقا یکدم میاسا از طلب </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاضر و غایب بغیر از وی که دید </p></div>
<div class="m2"><p>من هرب منه الیه قد رغب </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حکمت او بس غرایب را مناط </p></div>
<div class="m2"><p>قدرت او بس عجایب را سبب </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای زسر تا پا همه خلقت غریب </p></div>
<div class="m2"><p>ای ز پا تا سر همه امرت عجب </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جامع اضداد جز حق نیست فیض </p></div>
<div class="m2"><p>ره بحق بنمودمت زین ره طلب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کسی در غور این کم میرسد </p></div>
<div class="m2"><p>گر رسیدی تو بدین مگشای لب </p></div></div>