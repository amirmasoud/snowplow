---
title: >-
    غزل شمارهٔ ۲۳۳
---
# غزل شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>حاشا که مداوای من از پند توان کرد</p></div>
<div class="m2"><p>دیوانه به افسانه خردمند توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شور از سر مجنون به نصیحت نشود کم</p></div>
<div class="m2"><p>ای لیلیش افزون بشکر خند توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنهان نتوان داشت جنون در دل عاشق</p></div>
<div class="m2"><p>در سوخته آتش بچه سان بند توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واعظ سخن بیهده تا چند توان گفت</p></div>
<div class="m2"><p>یا گوش به افسانه تو چند توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود چشم ندارد که دهد توبه از آنروی</p></div>
<div class="m2"><p>با چشم چسان گوش باین پند توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با موج محیط غمش آرام توان داشت</p></div>
<div class="m2"><p>شوریده بصحرای جنون بند توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای هم نفسان حال دل زار مپرسید</p></div>
<div class="m2"><p>نوعی نشکسته است که پیوند توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شهد سخن‌های شکر بار تو ای فیض</p></div>
<div class="m2"><p>عالم همه پرشکر و پرقند توان کرد</p></div></div>