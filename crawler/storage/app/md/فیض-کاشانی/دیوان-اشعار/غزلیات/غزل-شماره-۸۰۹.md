---
title: >-
    غزل شمارهٔ ۸۰۹
---
# غزل شمارهٔ ۸۰۹

<div class="b" id="bn1"><div class="m1"><p>ای عشق رسوا کن مرا گو نام بر من ننگ شو</p></div>
<div class="m2"><p>باید که من عشرت کنم گو ناصحم دلتنگ شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغزم برون آمد ز پوست افتادم اندر راه دوست</p></div>
<div class="m2"><p>ای شوق رهبر شو مرا ای عشق پیش آهنگ شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شوق رهبر باشدم از دوری منزل چه غم</p></div>
<div class="m2"><p>چون عشق در پیش است گوهر گام صد فرسنگ شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عقل از دوری مگو در راه مهجوری مپو</p></div>
<div class="m2"><p>گوینده اینجا گنگ شو پوینده اینجا گنگ شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهد زدین گردی بری از عشق اگر بوئی بری</p></div>
<div class="m2"><p>در حلقهٔ مستان در آ با عاشقان همرنگ شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر مرد عشقی درد جو خاکی شو و گلها بروی</p></div>
<div class="m2"><p>بیدردی ار خواهد دلت روسنگ شو روسنگ شو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مرد عشقی جام گیر ترک رسوم خام گیر</p></div>
<div class="m2"><p>ور عاقلی خوش آیدت در بند نام و ننگ شو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاری کز آن نگشود در بر همزن او را زودتر</p></div>
<div class="m2"><p>گر عاقلی دیوانه شو دیوانه فرهنگ شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی ز رویش بر خوری وز لعل او شکر خوری</p></div>
<div class="m2"><p>موئی شو ای فیض از غمش در زلف او آونگ شو</p></div></div>