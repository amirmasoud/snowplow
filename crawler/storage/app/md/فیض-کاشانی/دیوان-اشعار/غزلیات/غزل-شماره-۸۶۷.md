---
title: >-
    غزل شمارهٔ ۸۶۷
---
# غزل شمارهٔ ۸۶۷

<div class="b" id="bn1"><div class="m1"><p>بماندم چیز و کس را انت حسبی</p></div>
<div class="m2"><p>براندم خار و خس را انت حسبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر و بالی گشادم در هوایت</p></div>
<div class="m2"><p>شکستم این قفس را انت حسبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا خواهم ترا خواهم به جز تو</p></div>
<div class="m2"><p>نخواهم هیچکس را انت حسبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین خواهم که حیران تو باشم</p></div>
<div class="m2"><p>نه بینم پیش و پس را انت حسبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون دل نمیدانم چه غوغاست</p></div>
<div class="m2"><p>نخواهم این جرس را انت حسبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درون سر نمیدانم چه سوداست</p></div>
<div class="m2"><p>نخواهم بوالهوس را انت حسبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس بی یاد تو گر میزند فیض</p></div>
<div class="m2"><p>نخواهم آن نفس را انت حسبی</p></div></div>