---
title: >-
    غزل شمارهٔ ۷۸۴
---
# غزل شمارهٔ ۷۸۴

<div class="b" id="bn1"><div class="m1"><p>از سرّ وحدت دم زدم هذا جنون العاشقین</p></div>
<div class="m2"><p>کونین را برهم زدن هذا جنون العاشقین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر طرهٔ پر خم زدم بر حرف لا و لم زدم</p></div>
<div class="m2"><p>شادی کنان بر غم زدم هذا جنون العاشقین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر شور و بر غوغا زدم بر لا و بر الا زدم</p></div>
<div class="m2"><p>برجا و بر بیجا زدم هذا جنون العاشقین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عشق سرمست‌ آمدم وز نیست در هست آمدم</p></div>
<div class="m2"><p>در رفعت او پست آمدم هذا جنون العاشقین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشتم زعشق دوست‌مست‌شستم‌زغیردوست دست</p></div>
<div class="m2"><p>تا رو نماید هر چه هست هذا جنون العاشقین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش زدم افلاک را بر باد دادم خاک را</p></div>
<div class="m2"><p>شستم دل غمناک را هذا جنون العاشقین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرگشتهٔ کوئی شدم آشفتهٔ موئی شدم</p></div>
<div class="m2"><p>حیران مه روی شدم هذا جنون العاشقین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عشق گشتم بیقرار زنجیر من شد زلف یار</p></div>
<div class="m2"><p>چشم خرد از من مدار هذا جنون العاشقین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در من نگیرد پند کس سوزم نصیحت را چو خس</p></div>
<div class="m2"><p>پندم جمال یار بس هذا جنون العاشقین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش زدم من پند را وین خشک خام چند را</p></div>
<div class="m2"><p>پختم دل خرسند را هذا جنون العاشقین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خود بریدم پند را بگسستم این پیوند را</p></div>
<div class="m2"><p>بشکستم این الوند را هذا جنون العاشقین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نام در ننگ آمدم وز صلح در جنگ آمدم</p></div>
<div class="m2"><p>از عاقلی تنگ آمدم هذا جنون العاشقین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نی ننگ میدانم نه عار دست از من بیدل بدار</p></div>
<div class="m2"><p>یکدم مرا با من گذار هذا جنون العاشقین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتش زدم در جان و تن وز خود فکندم ما و من</p></div>
<div class="m2"><p>بر هم زدم این انجمن هذا جنون العاشقین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای آنکه در عقلی گرو درفیض و در شعرش مکاو</p></div>
<div class="m2"><p>از شرو شورم دور شو هذا جنون العاشقین</p></div></div>