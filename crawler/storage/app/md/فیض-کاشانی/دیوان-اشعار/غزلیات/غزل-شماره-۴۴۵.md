---
title: >-
    غزل شمارهٔ ۴۴۵
---
# غزل شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>فروغ نور جمال تو در دل بیدار</p></div>
<div class="m2"><p>ز دود ز آینه کون ظلمت اغیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوخت غیر سراسر در آتش غیرت</p></div>
<div class="m2"><p>منادی لمن الملک واحد قهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سیل قهر جلال احد هجوم آرد</p></div>
<div class="m2"><p>چه چاره جز که بجولان او رود اغیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بساحت جبروتش کجا رسد اوهام</p></div>
<div class="m2"><p>چو عقل را ملکوتش ببسته راه گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شروق نور ازل شد چو در دلی تابان</p></div>
<div class="m2"><p>ز اهل دل برباید بصیرت و ابصار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه‌سان توان بجمالی نظر توان افکند</p></div>
<div class="m2"><p>که صف کشیده پی دور باش او انوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کند طلوع چه خورشید ما حی الاعیان</p></div>
<div class="m2"><p>چه جای نور سنا برق بذهب الابصار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه دست باز شود عز فرد بی‌پایان</p></div>
<div class="m2"><p>کجا بماند از اغیار در جهان آثار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ثنای او مشنو فیض خرز گفتهٔ او</p></div>
<div class="m2"><p>که نیست درد و جهان غیر ذات او دیار</p></div></div>