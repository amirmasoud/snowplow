---
title: >-
    غزل شمارهٔ ۸۴۷
---
# غزل شمارهٔ ۸۴۷

<div class="b" id="bn1"><div class="m1"><p>ندارم خان و مانی حسبی الله</p></div>
<div class="m2"><p>نخواهم آب و نانی حسبی الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از کون و مکان بیزار گشتم</p></div>
<div class="m2"><p>شدم در لامکانی حسبی الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهانرا خط بیزاری کشیدم</p></div>
<div class="m2"><p>چو خود گشتم جهانی حسبی الله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بستی طرفی از جان و نه از دل</p></div>
<div class="m2"><p>نه دل خواهم نه جانی حسبی الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا جانان پسند آمد نخواهم</p></div>
<div class="m2"><p>نه اینی و نه آنی حسبی الله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمیگیرم چو در دست من آمد</p></div>
<div class="m2"><p>بموی او جهانی حسبی الله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در این آتش خوشم رضوان میارا</p></div>
<div class="m2"><p>برای من جنانی حسبی الله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نعیم آتش عشقش مرا بس</p></div>
<div class="m2"><p>بهشت جاودانی حسبی الله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو یار آمد ز در خاموش شو فیض</p></div>
<div class="m2"><p>عیان شد هر بیانی حسبی الله</p></div></div>