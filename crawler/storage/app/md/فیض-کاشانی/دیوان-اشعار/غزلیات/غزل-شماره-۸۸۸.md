---
title: >-
    غزل شمارهٔ ۸۸۸
---
# غزل شمارهٔ ۸۸۸

<div class="b" id="bn1"><div class="m1"><p>یا من هو اقرب بی من حبل و ریدی</p></div>
<div class="m2"><p>فی حبک فارقت قریبی و بعیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کندم دل از اغیار و بدادم بتو ای یار</p></div>
<div class="m2"><p>زانروی که قفل دل ما را تو کلیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من سافر لاید له زاد بلاغ</p></div>
<div class="m2"><p>الا سفری عندک زادی و مزیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انعامک قدتم و احسانک قدغم</p></div>
<div class="m2"><p>عصیانک یا رب بنا غیر سدیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ان نحن عصینا فیه معترفو نا</p></div>
<div class="m2"><p>غفرانک یا رب لنا غیر بعیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو دوختی آن را که بیهوده بریدیم</p></div>
<div class="m2"><p>هم دوختهٔ بیهدهٔ ما تو دریدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خواهش تو خواهش ما را نگذارد</p></div>
<div class="m2"><p>خواهی بتو دادیم کن آنرا که مزیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر قدم تو شد خاک سر فیض</p></div>
<div class="m2"><p>تا بشنود از تو شهدائی و عبیدی</p></div></div>