---
title: >-
    غزل شمارهٔ ۳۵۶
---
# غزل شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>افلاک را جلالت تو پست میکند</p></div>
<div class="m2"><p>املاک را مهابت تو پست میکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا دلی که عشق تو در وی کند نزول</p></div>
<div class="m2"><p>هوشش رباید و خردش مست میکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر پست را عبادت تو میکند بلند</p></div>
<div class="m2"><p>مر نیست را ارادت تو هست میکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان را ز آسمان بزمین لطفت آورد</p></div>
<div class="m2"><p>لطف خفی شمارهٔ هر مست میکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم رسا احاطه ذرات کاینات</p></div>
<div class="m2"><p>تا قدر او بلند شود پست میکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سازد ز نطفه قدرت تو صورت عجب</p></div>
<div class="m2"><p>تا جان کند شکار ز تن شست میکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دیده‌اش گشاید در ظلمت افکند</p></div>
<div class="m2"><p>تا سر بلند گردد پا پست میکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر اهل خیر چون بگشاید دری بهشت</p></div>
<div class="m2"><p>بر دوزخ آن گشاد دری بست میکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آزاری ارچه میرسد از گردش سپهر</p></div>
<div class="m2"><p>لیکن تلافی چو دلی خست میکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جای حوادث است جهان بلند و پست</p></div>
<div class="m2"><p>گاهی کند بلند و گهی پست میکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیماریی چو دست دهد یا عدو دعا</p></div>
<div class="m2"><p>سعی طبیب و کار زبر دست میکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر دم که فیض میل جهان دگر کند</p></div>
<div class="m2"><p>دستش گرفته است تو پا پست میکند</p></div></div>