---
title: >-
    غزل شمارهٔ ۷۰
---
# غزل شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>دلم گرفت ازین خاکدان پر وحشت </p></div>
<div class="m2"><p>ره بهشت کدامست و منزل راحت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلاست صحبت بیگانه و دیار غریب </p></div>
<div class="m2"><p>کجاست منزل مألوف و یار بی کلفت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زسینه گشت جدا و نیافت محرم راز </p></div>
<div class="m2"><p>نفس گره شده در کام ماند از غیرت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بعالم غیبم دریچهٔ بودی </p></div>
<div class="m2"><p>زدودمی بنسیمی دمی ز دل کربت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر سروش رحیلی بگوش جان آمد </p></div>
<div class="m2"><p>دل گرفته گشاید زکربت غربت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زوصل دوست نسیمی بیار باد صبا </p></div>
<div class="m2"><p>که سخت شعله کشیده است آتش فرقت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز کتاب انیسی دلم نمیخواهد </p></div>
<div class="m2"><p>زهی انیس و زهی خامشی زهی صحبت </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر اجل دهدم مهلت و خدا توفیق</p></div>
<div class="m2"><p>من و خدا و کتابی و گوشهٔ خلوت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار شکر که کاری بخلق نیست مرا </p></div>
<div class="m2"><p>خدا پسند بود فیض را زهی همت </p></div></div>