---
title: >-
    غزل شمارهٔ ۶۲۰
---
# غزل شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>من آن نیم که توانم ز تو جدا باشم</p></div>
<div class="m2"><p>جدا شوم ز تو در معرض فنا باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بغیر سایهٔ لطف تو جای دیگر هست</p></div>
<div class="m2"><p>جدا اگر ز تو باشم بگو کجا باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدایرا مپسند ای تو زندگانی من</p></div>
<div class="m2"><p>که یکنفس بفراق تو مبتلا باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جدا ز تو زیم ارمن تنی بوم بیجان</p></div>
<div class="m2"><p>و گر بیاد تو میرم ابوالبقا باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای تو زیم و در ره تو میمیرم</p></div>
<div class="m2"><p>ترا نباشم اگر من بگو کرا باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بآسمان برسم گر ترا زمین گردم</p></div>
<div class="m2"><p>سر شهانم اگر من ترا گدا باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا نه بیند اگر چشم من چکار آید</p></div>
<div class="m2"><p>فدای تو نشوم در جهان چرا باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر ندای تعال تو نشنود گوشم</p></div>
<div class="m2"><p>بدوش حامل گوش چنین چرا باشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو پای من نرود در ره تو گو بشکن</p></div>
<div class="m2"><p>ترا چه نیست چه در بند دست و پا باشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خموش فیض که هر بد که بر سرم آید</p></div>
<div class="m2"><p>بود سزای من و من سزای آن باشم</p></div></div>