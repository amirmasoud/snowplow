---
title: >-
    غزل شمارهٔ ۲۱
---
# غزل شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>وصل با دلدار میباید مرا </p></div>
<div class="m2"><p>فصل از اغیار می باید مرا </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نی ام از اصل خود ببریده اند </p></div>
<div class="m2"><p>نالهای زار می باید مرا </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کجا و رسم عقل و دین کجا </p></div>
<div class="m2"><p>مست یارم یار می باید مرا </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی وصال او نمیخواهم بهشت </p></div>
<div class="m2"><p>دار بعد از جار میباید مرا </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق از نام نکو ننگ آیدش </p></div>
<div class="m2"><p>عاشقم من عار می باید مرا </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل دادم بستدم دیوانگی </p></div>
<div class="m2"><p>شیوهٔ این کار میباید مرا </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بکی این راز را پنهان کنم </p></div>
<div class="m2"><p>مستی و اظهار میباید مرا </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر زمن سر میزند بی اختیار </p></div>
<div class="m2"><p>محرم اسرار میباید مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتگو بگذار فیض و کار کن </p></div>
<div class="m2"><p>در ره او کار میباید مرا </p></div></div>