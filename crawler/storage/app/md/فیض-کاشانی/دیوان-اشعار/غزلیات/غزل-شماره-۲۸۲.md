---
title: >-
    غزل شمارهٔ ۲۸۲
---
# غزل شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>همه را خود نوازد و سازد</p></div>
<div class="m2"><p>گرچه از خود بکس نپردازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه او او همه است خود با خود</p></div>
<div class="m2"><p>جاودان نرد عشق می‌بازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسوت نو بهر زمان پوشد</p></div>
<div class="m2"><p>مرکب تازه دم بدم تازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه شاهد شود کرشمه کند</p></div>
<div class="m2"><p>گاه با شاهدان نظر بازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که نیاز آورد بدرگه خود</p></div>
<div class="m2"><p>گاه بر خود بخویشتن نازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه سوزد بقهر دلها را</p></div>
<div class="m2"><p>گاه سازد بلطف و بنوازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست درمان هر دلی دردی</p></div>
<div class="m2"><p>فیض را درد عشق می‌سازد</p></div></div>