---
title: >-
    غزل شمارهٔ ۴۵۹
---
# غزل شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>ای که در گل زار حسنش میخرامی مست ناز</p></div>
<div class="m2"><p>میفکن گاهی نگاهی جانب اهل نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که سر تا بپا روئی چو خور بنمای رو</p></div>
<div class="m2"><p>تا به بینم شاهد حق ز آینهٔ ارباب راز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی دارم سوی آنکو روی دارد سوی او</p></div>
<div class="m2"><p>روی او پیداست در روی اسیران نیاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیشها دارند ز الطاف نهانی مخلصان</p></div>
<div class="m2"><p>قصه الطاف محمود است و اخلاص ایاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه از ینصورت پرستان تهی از معرفت</p></div>
<div class="m2"><p>از جمال شاهد معنی بصورت مانده باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند و چند از صورت صورت پرستی شرمدار</p></div>
<div class="m2"><p>شاهد معنی است حاضر تو بصورت عشقباز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو بشهرستان معنی آر از این صورتکده</p></div>
<div class="m2"><p>تا که باشی در میان اهل معنی سر فراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که دستش کوته از معنی است در صورت زند</p></div>
<div class="m2"><p>لیک باید کرد معنی را ز صورت امتیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نداری معرفت لب را ببنداز گفتگو</p></div>
<div class="m2"><p>العیاذ از آستین کوته و دست دراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ریا و غل و غش خالی شو ای طاعت‌پرست</p></div>
<div class="m2"><p>صدق و اخلاص و امانت بهتر است از صد نماز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شستن ظاهر ز انواع نجاستها چه سود</p></div>
<div class="m2"><p>باطن آکنده است چون از شرک و کین و حرص و آز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از ره عجز و نیاز آمد بدرگاه تو فیض</p></div>
<div class="m2"><p>بر دلش بگشا دری ای بی‌نیاز چاره‌ساز</p></div></div>