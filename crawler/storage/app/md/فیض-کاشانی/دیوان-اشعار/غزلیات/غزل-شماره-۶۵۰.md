---
title: >-
    غزل شمارهٔ ۶۵۰
---
# غزل شمارهٔ ۶۵۰

<div class="b" id="bn1"><div class="m1"><p>شب‌ها حدیث زلف تو تکرار می‌کنم</p></div>
<div class="m2"><p>تسبیح روز وصل تو بسیار می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دم زند صباح ز انوار طلعتت</p></div>
<div class="m2"><p>جان را ز عکس روی تو گلزار می‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پای تا به سر همه تن دیده می‌شوم</p></div>
<div class="m2"><p>جان را به دیده قابل دیدار می‌کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غمزهٔ نگاه تو بیهوش می‌شوم</p></div>
<div class="m2"><p>دل را ز چشم مست تو هشیار می‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس تو چون در آینهٔ دل درآیدم</p></div>
<div class="m2"><p>بی‌خود حدیث واحد قهار می‌کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترجیع‌بند هر سخنم ذکر خیر تست</p></div>
<div class="m2"><p>در هر کلام نام تو تکرار می‌کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با مردمان حدیث تو گویم در انجمن</p></div>
<div class="m2"><p>تنها حدیث با در و دیوار می‌کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیرم سنای دل ز سنا برق روی تو</p></div>
<div class="m2"><p>دریوزه‌ای ز قاسم انوار می‌کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم را به یاد روی تو از سینه می‌برم</p></div>
<div class="m2"><p>دم را به ذکر موی تو عطار می‌کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب را به یاد زلف تو می‌آورم به روز</p></div>
<div class="m2"><p>چون روز شد ستایش رخسار می‌کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آهنگ من چو کرد بر آهنگ می‌زنم</p></div>
<div class="m2"><p>دل را ز غم به ناله سبکبار می‌کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر سرّ من به غیر نگوید رفیق من</p></div>
<div class="m2"><p>زودش به لطف خازن اسرار می‌کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکس که گوش جان به سخن‌های من دهد</p></div>
<div class="m2"><p>او را به صور موعظه بیدار می‌کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کار خوب را که ز کردار عاجزم</p></div>
<div class="m2"><p>تحسین هرکه کرد به گفتار می‌کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پنهان کن از خلایق گر عاشقی کنی</p></div>
<div class="m2"><p>با فیض هم مگوی که این کار می‌کنم</p></div></div>