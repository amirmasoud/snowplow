---
title: >-
    غزل شمارهٔ ۴۹۵
---
# غزل شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>بر جمال تو هست خالت نص</p></div>
<div class="m2"><p>خط بود نیز بر کمالت نص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزد بینا دو شاهد عدلند</p></div>
<div class="m2"><p>خال و خط هر دو بر جمالت نص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد خط شود چو شاهد روز</p></div>
<div class="m2"><p>خال کتمان کند بحالت نص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزد قاضی شود شهادت رد</p></div>
<div class="m2"><p>محو گردد ز خط و خالت نص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونکه آن زور کرد این کتمان</p></div>
<div class="m2"><p>چون توان کرد بر جمالت نص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نون ابرو وصاد چشمت نیز</p></div>
<div class="m2"><p>هر دو هستند بر جمالت نص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک بیک زین دو چون نکول کند</p></div>
<div class="m2"><p>هر دو باشد بر انفعالت نص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز چون خال و خط شود بیرنگ</p></div>
<div class="m2"><p>هر دو باشند بر زوالت نص</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر ثبات خیالت اما هست</p></div>
<div class="m2"><p>صورت اول خیالت نص</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سر فیض نقش اول حسن</p></div>
<div class="m2"><p>هست بر حسن بیزوالت نص</p></div></div>