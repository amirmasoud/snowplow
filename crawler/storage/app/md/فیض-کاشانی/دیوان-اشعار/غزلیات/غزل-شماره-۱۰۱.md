---
title: >-
    غزل شمارهٔ ۱۰۱
---
# غزل شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>دمبدم دل ما را از الست پیغام است </p></div>
<div class="m2"><p>از بلی بلی جانرا تازه تازه اسلامست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاص می نه پندارد کاین بروز اول بود </p></div>
<div class="m2"><p>بعد از آن سخن بگسست این عقیده عامست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش هر خدا بینی مستمع بود از حق </p></div>
<div class="m2"><p>وانکه او نمی بیند بی گمان که او خامست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو کون را ایزد دم بدم در ایجاد است </p></div>
<div class="m2"><p>خلق راست راز کن مستی که بی جام است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر صید جان پاک دامها کنند از خاک </p></div>
<div class="m2"><p>تا شود به از املاک یا رب این چه انعامست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغهای جان آید در شباک تن افتد </p></div>
<div class="m2"><p>در بلا شود پخته زانکه بی بلا خام است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فوج فوج از آن عالم آورند جانها را </p></div>
<div class="m2"><p>تا کدام ناکام و تا کدام را کامست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمرهٔ سعید آیند زمرهٔ شقی گردند </p></div>
<div class="m2"><p>تا چه در قضا رفته تا چه هر کرا نامست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآتش غم عشقی جان و دل نشد پخته </p></div>
<div class="m2"><p>ساز ره نکردی فیض کار بارو تو خامست </p></div></div>