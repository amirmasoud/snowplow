---
title: >-
    غزل شمارهٔ ۴۲۳
---
# غزل شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>شکر افشان دهانش نگرید</p></div>
<div class="m2"><p>لبن آلوده لبانش نگرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگاهی بجهان جان بخشد</p></div>
<div class="m2"><p>حکم بر جان و جهانش نگرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلقی از مستی چشمش مستند</p></div>
<div class="m2"><p>می بی کام و دهانش نگرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر میگیرد از ابرو مژگان</p></div>
<div class="m2"><p>سهمگین تیر و کمانش نگرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطرش با من و رو باد گران</p></div>
<div class="m2"><p>سوی من لطف نهانش نگرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لب من سخن او میگوید</p></div>
<div class="m2"><p>در بیانم به بیانش نگرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر هر پرده نهانش بینید</p></div>
<div class="m2"><p>پرده هم اوست عیانش نگرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض دل با حق و رو در خلقست</p></div>
<div class="m2"><p>شرح حالش ز بیانش نگرید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ندانید کز اهل درد است</p></div>
<div class="m2"><p>درد او در سخنانش نگرید</p></div></div>