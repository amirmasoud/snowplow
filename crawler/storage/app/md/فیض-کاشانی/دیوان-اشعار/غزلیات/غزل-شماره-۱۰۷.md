---
title: >-
    غزل شمارهٔ ۱۰۷
---
# غزل شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>دل که ویران اوست آباد است</p></div>
<div class="m2"><p>جان چو غمناک از او بود شاد است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موبمو خویش را بدو بندم </p></div>
<div class="m2"><p>هر که در بند اوست آزاد است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این سعادت بسعی می نشود </p></div>
<div class="m2"><p>غم او روزی خداداد است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابی بود عمارت دل </p></div>
<div class="m2"><p>خانهٔ دل زعشق آباد است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق استاد کار خانهٔ ماست </p></div>
<div class="m2"><p>کوشش از ما زعشق ارشاد است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کاری نمیکنیم بخود </p></div>
<div class="m2"><p>همه او میکند که استاد است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار کن کار و گفتگو بگذار </p></div>
<div class="m2"><p>فیض بنیاد حرف برباد است </p></div></div>