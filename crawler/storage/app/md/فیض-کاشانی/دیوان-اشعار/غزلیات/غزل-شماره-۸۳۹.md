---
title: >-
    غزل شمارهٔ ۸۳۹
---
# غزل شمارهٔ ۸۳۹

<div class="b" id="bn1"><div class="m1"><p>شهید علینا من رجونا شهوده</p></div>
<div class="m2"><p>رقیب علینا من نعبنا وفوده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علینا له عهد وثیق مؤکد</p></div>
<div class="m2"><p>علی رفض شرک مخلصین سجوده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسینا عهودا قد عهدنا بمشهد</p></div>
<div class="m2"><p>شهود عدول ذاکرون شهوده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعاهدها حیی غیور مطالب</p></div>
<div class="m2"><p>فواهالنا ادارام منا عهوده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تعالوا الی بعض مافاتنا نفضها</p></div>
<div class="m2"><p>و نسعی لیرضی من ضمنا عقوده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تحاذربه یوماً عبوساً لقاؤه</p></div>
<div class="m2"><p>نمهد لات قد علمنا ردوده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>له نحونا نظرهٔ بعد اخری بها</p></div>
<div class="m2"><p>ینعم قوما ناظرین شهوده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و اوقد نارا فی الجحیم اعرها</p></div>
<div class="m2"><p>لمن کان منا ناکثین عهوده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تعالوا تحاذر ناره بسجودنا</p></div>
<div class="m2"><p>ولهفی لهیبا مسرعین خموده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تعالوا یحاسب نفوساً و لما اتی</p></div>
<div class="m2"><p>علینا حساب ما قدرنا جحوده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تعالوا نزن انفا قبل ان تؤزنا</p></div>
<div class="m2"><p>علی الموت نقدم با درین و روده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تعالوا الی فیض فیض سنا برقه</p></div>
<div class="m2"><p>تخطف به الابصار نمنع هموده</p></div></div>