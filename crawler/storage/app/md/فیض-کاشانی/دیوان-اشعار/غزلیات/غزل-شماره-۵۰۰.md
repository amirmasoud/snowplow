---
title: >-
    غزل شمارهٔ ۵۰۰
---
# غزل شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>ای رهنمای گم شدگان اهدنا الصراط</p></div>
<div class="m2"><p>وی چشم راه روان اهدنا الصراط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دوزخ هوا و هوس مانده‌ایم زار</p></div>
<div class="m2"><p>گم کرده‌ایم راه جنان اهدنا الصراط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذشت عمر در لعب و لهو بی خودی</p></div>
<div class="m2"><p>شاید تدارکی بتوان اهدنا الصراط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره دور وقت دیر و شب تار و صد خطر</p></div>
<div class="m2"><p>مرکب ضعیف و جاده نهان اهدنا الصراط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غولی ز هر طرف ره وا مانده زنده</p></div>
<div class="m2"><p>آه از صفیر راهزنان اهدنا الصراط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی ره بسوی سود و نه سوی زیان بریم</p></div>
<div class="m2"><p>ای از تو سود و از تو زیان اهدنا الصراط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شارع هوا و هوس در نمی‌رویم</p></div>
<div class="m2"><p>گاهی در این و گاه در آن اهدنا الصراط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتند اهل دل همه با کاروان جان</p></div>
<div class="m2"><p>ما مانده‌ایم بی‌دل و جان اهدنا الصراط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گم گشت فیض و راه بجائی نمیبرد</p></div>
<div class="m2"><p>ای رهنمای گمشدگان اهدنا الصراط</p></div></div>