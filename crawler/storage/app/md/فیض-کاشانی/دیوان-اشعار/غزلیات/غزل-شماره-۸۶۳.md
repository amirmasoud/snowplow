---
title: >-
    غزل شمارهٔ ۸۶۳
---
# غزل شمارهٔ ۸۶۳

<div class="b" id="bn1"><div class="m1"><p>ای آنکه با دلم ز ازل یار بوده‌ای</p></div>
<div class="m2"><p>پیوسته راحت دل بیمار بوده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه لطف کرده با من دلخسته گاه قهر</p></div>
<div class="m2"><p>در غیر لطف گاهی و قهار بوده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاهی وفا و گاه جفا با دلم کنی</p></div>
<div class="m2"><p>هم یار بوده‌ای و هم اغیار بوده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افروختی رخ و ز مژه نیش می‌زنی</p></div>
<div class="m2"><p>گل بوده‌ای بروی و بمو خار بوده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از راه مهر آمدی و سوختی مرا</p></div>
<div class="m2"><p>آسان نموده اول و دشوار بوده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بوده‌ای نداشته‌ای دست از دلم</p></div>
<div class="m2"><p>این عشق جان گداز چه غمخوار بوده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دل زمین شده است بدورش تو آسمان</p></div>
<div class="m2"><p>گر نقطه گشته است تو پرگار بوده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان دلی ببوده که در وی نبوده‌ای</p></div>
<div class="m2"><p>ای عشق کم نموده چه بسیار بوده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای فیض کس ندیده ز کردار تو اثر</p></div>
<div class="m2"><p>کاری نکرده‌ای همه گفتار بوده‌ای</p></div></div>