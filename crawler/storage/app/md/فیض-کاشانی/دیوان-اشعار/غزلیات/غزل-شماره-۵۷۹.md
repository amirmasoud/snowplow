---
title: >-
    غزل شمارهٔ ۵۷۹
---
# غزل شمارهٔ ۵۷۹

<div class="b" id="bn1"><div class="m1"><p>در دل توئی در جان توئی ای مونس دیرینه‌ام</p></div>
<div class="m2"><p>در سینهٔ بریان توئی ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای تو روان اندر بدن ای هم تو جان و هم تو تن</p></div>
<div class="m2"><p>ای هم تو حسن و هم حسن ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم دل تو و هم سینه تو گوهر تو و گنجینه تو</p></div>
<div class="m2"><p>دینه تو و دیرینه تو ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بارم دهی آیم برت ورنه بمانم بر درت</p></div>
<div class="m2"><p>ای لم یزل من چاکرت ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارم دهی خرم شوم ردم کنی درهم شوم</p></div>
<div class="m2"><p>از تو زیاد و کم شوم ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راهم دهی بینا شوم ردم کنی اعما شوم</p></div>
<div class="m2"><p>از تو بدو زیبا شم ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطفم کنی گلشن شوم قهرم کنی گلخن شوم</p></div>
<div class="m2"><p>گه جان شوم گه تن شوم ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهی‌بخوان خواهی‌بران دل در تو دل‌بست ازازل</p></div>
<div class="m2"><p>گشتم ز تو مست از ازل ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان لم یزل در وصل بود یکچند هجرانش ربود</p></div>
<div class="m2"><p>آخر همان گردد که بود ای مونس دیرینه‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فیض است و گفتگوی تو شیدای جستجوی تو</p></div>
<div class="m2"><p>شیء الهی کوی تو ای مونس دیرینه‌ام</p></div></div>