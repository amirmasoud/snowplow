---
title: >-
    غزل شمارهٔ ۴۴۳
---
# غزل شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>شهر یارم آرزو شد در دیار در دیار</p></div>
<div class="m2"><p>در دیارم برد آخر تا دیار شهریار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود عقل و هوش یارم بردم از سر هوش یار</p></div>
<div class="m2"><p>در طریق عشقبازی هستم اما هوشیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارزو بوئی صبا سویم که جانم آرزوست</p></div>
<div class="m2"><p>هم بیار از من خبر بر هم خبر از وی بیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت آن مهرو که هر مهرو نمایم همچو بدر</p></div>
<div class="m2"><p>روی بنمود و هلالی گشتم اندر انتظار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارها گفتم که بارت میکشم باری بده</p></div>
<div class="m2"><p>بر درت یکبار بارم داردم در زیر بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون از آن گلزار گشتم سوی گلزار آمدم</p></div>
<div class="m2"><p>چون هزاران صد هزاران ناله کردم زار زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزگار من گذشت و روزگار من گذشت</p></div>
<div class="m2"><p>حالیا در ماتم خود میگذارم روزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راح روحی فی هواه راح قلبی من هموم</p></div>
<div class="m2"><p>مرحبا بالموت راحاً لیس فیها من خمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فاض قلب الفیض من فیض الحکم فیضوضه</p></div>
<div class="m2"><p>کالسحاب الماطر الفیاض او فیض البحار</p></div></div>