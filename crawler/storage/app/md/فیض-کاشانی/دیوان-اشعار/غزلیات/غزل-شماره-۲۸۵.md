---
title: >-
    غزل شمارهٔ ۲۸۵
---
# غزل شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>کم عطا یا اعطیت من عطا یاک فزد</p></div>
<div class="m2"><p>کم هدایا اهدیت من عطا یاک فزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم خطایای غفرت کم مساوی سترت</p></div>
<div class="m2"><p>کم لسؤای صبرت من عطا یاک فزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرمها بخشیدهٔ و عیب‌ها پوشیدهٔ</p></div>
<div class="m2"><p>در وفا کوشیدهٔ من عطا یاک فزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عفوها فرمودهٔ لطف‌ها بنمودهٔ</p></div>
<div class="m2"><p>در کرم افزودهٔ من عطا یاک فزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طعم عرفان دادهٔ ذوق ایمان دادهٔ</p></div>
<div class="m2"><p>داد احسان دادهٔ من عطا یاک فزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفریدی به کرم پروریدی به نعم</p></div>
<div class="m2"><p>مگذارم در غم من عطا یاک فزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض را گر دادهٔ شوق بیحد دادهٔ</p></div>
<div class="m2"><p>عشق سرمد دادهٔ من عطایاک فزد</p></div></div>