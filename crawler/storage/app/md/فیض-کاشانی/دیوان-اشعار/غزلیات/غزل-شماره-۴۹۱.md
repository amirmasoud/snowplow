---
title: >-
    غزل شمارهٔ ۴۹۱
---
# غزل شمارهٔ ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>عالم چو خاتمیست که این است عشق قص</p></div>
<div class="m2"><p>از قصه‌است قصهٔ عشق احسن القصص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق در کلام خویش بآیات مستبین</p></div>
<div class="m2"><p>در شأن عشق و رتبه عالیش کرد نص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارواح ما ز عالم قدسست و کان عشق</p></div>
<div class="m2"><p>محبوس در بدن شده کالطیر فی القفص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی چو کرد حصه مقسم قرار داد</p></div>
<div class="m2"><p>خون جگر وظیفهٔ عشاق زان حصص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس دور شد که دور فتادیم ز اصل خویش</p></div>
<div class="m2"><p>طول النوی بحر عنا هذه الغصص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق فنای خویش طلب میکند مدام</p></div>
<div class="m2"><p>اهل عزیمتست نمیجوید او رخص</p></div></div>