---
title: >-
    غزل شمارهٔ ۱۸۵
---
# غزل شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>دوش از من رمیده می‌رفت</p></div>
<div class="m2"><p>دامان ز کفم کشیده می‌رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌رفت و مرا به حسرت از پی</p></div>
<div class="m2"><p>دریا دریا ز دیده می‌رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌رفت به ناز و رفته‌رفته</p></div>
<div class="m2"><p>آرام دل رمیده می‌رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌رفت و دل شکسته از پی</p></div>
<div class="m2"><p>نالان نالان تپیده می‌رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌رفت و روان روان به دنبال</p></div>
<div class="m2"><p>تن در عقبش خمیده می‌رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌رفت سرور و شادمانی</p></div>
<div class="m2"><p>از سینه مرا و دیده می‌رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌رفت به یاد هجرش از پی</p></div>
<div class="m2"><p>هوش از سر من پریده می‌رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌رفت و فغان من به دنبال</p></div>
<div class="m2"><p>او فارغ و ناشنیده می‌رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌رفت و منش فتاده در پی</p></div>
<div class="m2"><p>صد پرده من دردیده می‌رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌رفت و جهان جهان تغافل</p></div>
<div class="m2"><p>گفتی که مرا ندیده می‌رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌رفت به صدهزار تمکین</p></div>
<div class="m2"><p>سنجیده و آرمیده می‌رفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کس سرو چمن چمان ندیده است</p></div>
<div class="m2"><p>آن سو روان چمیده می‌رفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حیف است که بر زمین نهد پای</p></div>
<div class="m2"><p>ای کاش فرا ز دیده می‌رفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس فیض ز رفتنش غزل کاش</p></div>
<div class="m2"><p>در آمدنش قصیده می‌رفت</p></div></div>