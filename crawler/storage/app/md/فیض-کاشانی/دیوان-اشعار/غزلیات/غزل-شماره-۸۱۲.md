---
title: >-
    غزل شمارهٔ ۸۱۲
---
# غزل شمارهٔ ۸۱۲

<div class="b" id="bn1"><div class="m1"><p>ای عاقلان دیوانه‌ام زنجیر زلف یار کو</p></div>
<div class="m2"><p>بر شعلهای شوق دل پروانه‌ام دلدار کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مست او جان مست او تن هم سرا پا مو بمو</p></div>
<div class="m2"><p>در جملهٔ ذرات من یکذره هشیار کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل رفت جان هم میرود روح روان هم میرود‌</p></div>
<div class="m2"><p>جانانه را آگه کنید آن دلبر غمخوار کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بستم اندر زلف او واعظ ز دستم دست شو</p></div>
<div class="m2"><p>کافر شدم کافر شدم زنار کو زنار کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قربانیم قربانیم عید وصال او کجاست</p></div>
<div class="m2"><p>مشتاق جانم افشانیم آن غمزهٔ خونخوار کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیرم بر اندازی نقاب بنمائی آنرخ بی‌حجاب</p></div>
<div class="m2"><p>لیکن سرت گردم مرا یارائی دیدار کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که چون بینم ترا شرح غم دل سر کنم</p></div>
<div class="m2"><p>آندم که بینم روی او آن طاقت گفتار کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب با خیال زلف تو کی خواب آید فیض را</p></div>
<div class="m2"><p>در خواب هم کی بینمت آندولت بیدار کو</p></div></div>