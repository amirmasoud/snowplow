---
title: >-
    غزل شمارهٔ ۷۱۶
---
# غزل شمارهٔ ۷۱۶

<div class="b" id="bn1"><div class="m1"><p>ای برون از سرای کون و مکان</p></div>
<div class="m2"><p>برتر از هرچه میدهند نشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم زبان از ثنای تو قاصر</p></div>
<div class="m2"><p>هم خرد در سپاس تو حیران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای منزه ز شبه و مثل و نظیر</p></div>
<div class="m2"><p>وی مقدس ز نعت و وصف و بیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوته از دامن تو دست قیاس</p></div>
<div class="m2"><p>قاصر از ساحت تو پای گمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ثبات هر آنچه راست ثبات</p></div>
<div class="m2"><p>وی حیاهٔ هر آنچه دارد جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان در جمال تو واله</p></div>
<div class="m2"><p>عارفان در جلال تو حیران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه را این و آن توان گفتن</p></div>
<div class="m2"><p>برتری زان نه اینی و نه آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم جهان از تو خالی و هم پر</p></div>
<div class="m2"><p>ای ورای جهان خدای جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفرینندهٔ سپهر برین</p></div>
<div class="m2"><p>گسترانندهٔ زمین و زمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دلم آنکه با تو پیوندم</p></div>
<div class="m2"><p>بخدائی که از خودم برهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برسانم باوج علیین</p></div>
<div class="m2"><p>در عروج مراتب امکان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دمبدم حال من نکوتر کن</p></div>
<div class="m2"><p>تا مقامی که نیست بهتر از آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عفو کن یک بیک بدیها را</p></div>
<div class="m2"><p>بر خطاها بکش خط غفران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قطرهٔ از سحاب مغفرتت</p></div>
<div class="m2"><p>نگذارد نشانی از عصیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نور مهر تو هست در دل فیض</p></div>
<div class="m2"><p>از خودش تا بخویشتن برسان</p></div></div>