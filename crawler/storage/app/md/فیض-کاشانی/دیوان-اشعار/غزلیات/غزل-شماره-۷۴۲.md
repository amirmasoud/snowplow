---
title: >-
    غزل شمارهٔ ۷۴۲
---
# غزل شمارهٔ ۷۴۲

<div class="b" id="bn1"><div class="m1"><p>دلا برخیز و پائی بر بساط خود نمائی زن</p></div>
<div class="m2"><p>برندی سر برار آتش درین زهد دریائی زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آدر حلقه مستان و در کش یکدو پیمانه</p></div>
<div class="m2"><p>بمستی ترک هستی کن دم از فرمانروائی زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمر بر بند در خدمت چونی از خویش خالی مشو</p></div>
<div class="m2"><p>ز بی برگی بجو برگ و نوای بی نوائی زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسیر نفس بودن در خراب آباد تن تا کی</p></div>
<div class="m2"><p>قدم در عالم جان نه در از خود رهائی زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخلوتخانه وحدت درا از خویش یکتا شو</p></div>
<div class="m2"><p>بسوز این خرقه یا چاکی برین دلق دو تائی زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زره گم گشتن اندر ظلمت آباد هوس تا چند</p></div>
<div class="m2"><p>براه آی آتش اندر آرزوهای هوائی زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیفکن آنچه در سر داری و پای اندرین ره نه</p></div>
<div class="m2"><p>گدائی کن درین درگاه و کوس پادشائی زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمردی وارهان خود را ازین بیگانگان بگسل</p></div>
<div class="m2"><p>بشهر آشنائی آ صلای‌ آشنائی زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز پا افتادهٔ در راه وصل دوست خیزای فیض</p></div>
<div class="m2"><p>دو دست استعانت در جناب کبریائی زن</p></div></div>