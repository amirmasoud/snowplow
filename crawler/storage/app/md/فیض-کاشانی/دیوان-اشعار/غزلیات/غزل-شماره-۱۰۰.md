---
title: >-
    غزل شمارهٔ ۱۰۰
---
# غزل شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>در غمزه مستانه ساقی چه شرابست </p></div>
<div class="m2"><p>کز نشأه آنجان جهان مست و خرابست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هشیار نه یک زاهد و مخمور نه یک مست </p></div>
<div class="m2"><p>مستست تر و خشک جهان اینچه شرابست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقست روان در رک و در ریشه جانها </p></div>
<div class="m2"><p>ذرات جهان مست ازین بادهٔ ناب است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عشق زمین جام شرابی است لبا لب </p></div>
<div class="m2"><p>وین چرخ نگونسار برین جام حباب است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان می طلبد غمزه ات ای ساقی مستان </p></div>
<div class="m2"><p>پیمانهٔ ما پرنشده است این چه شتاب است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چشم سیه مست تو هستند جهانی </p></div>
<div class="m2"><p>زان میکده ویران و خرابات خراب است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگذار که یکذره بماند زوجودش </p></div>
<div class="m2"><p>خورشید دل آرای ترا فیض نقابست </p></div></div>