---
title: >-
    غزل شمارهٔ ۵۰۵
---
# غزل شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>اهل دنیا را ز جان کندن چه حظ</p></div>
<div class="m2"><p>از عنای جان و برنج تن چه حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرگ را نشناختن تا وقت مرگ</p></div>
<div class="m2"><p>غافلان را از چنین مردن چه حظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعی کردن بهر دنیا روز و شب</p></div>
<div class="m2"><p>ناگهانی مردن و ماندن چه حظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواجه را از جمع کردنها چسود</p></div>
<div class="m2"><p>تخم حسرت در جهان کشتن چه حظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقلانرا از مراعات رسوم</p></div>
<div class="m2"><p>جز مشقتهای جان و تن چه حظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل عزت را ز عزوّ سروری</p></div>
<div class="m2"><p>جز مراعات گران کردن چه حظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار عقبا را پس افکندن چه سود</p></div>
<div class="m2"><p>فوت کردن وقت تا رفتن چه حظ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زینت دنیا ندارد چون بقا</p></div>
<div class="m2"><p>عاقلانرا دل در آن بستن چه حظ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض را زین پندهای بیهده</p></div>
<div class="m2"><p>گفتن و بنوشتن و خواندن چه حظ</p></div></div>