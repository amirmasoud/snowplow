---
title: >-
    غزل شمارهٔ ۳۸۷
---
# غزل شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>تا بکی این نفس کافر کیش کافرتر شود </p></div>
<div class="m2"><p>تا بچند این دیدهٔ بی شرم ننگ سر شود </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس فسون خواندم برین نفس دغا فرمان نبرد</p></div>
<div class="m2"><p>بس نصیحت کردمش شاید بحق رهبر شود </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر خودرا صرف کردم درفنون علم وفضل</p></div>
<div class="m2"><p>تا بود چشم دلم از علم روشن تر شود </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر من این علم و هنردرهای رحمت را ببست </p></div>
<div class="m2"><p>دیده هرگز کس کلید قفل قفل در شود </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم آخر میکنم کاری که بهتر باشد آن </p></div>
<div class="m2"><p>من چه دانستم که آخر کار من بدتر شود </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خدا رحمی بکن بر بنده بیچاره ات </p></div>
<div class="m2"><p>بد بود نیکوش کن نیکوست نیکوتر شود </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده را ارشاد کن شاید رسد در دولتی </p></div>
<div class="m2"><p>هر کرا مرشد تو باشی زآسمان برتر شود </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه قابل نیست زار شاد تو قابل می شود </p></div>
<div class="m2"><p>ور بود قابل زارشاد تو قابل تر شود </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانشی را لطف کن کزوی محبت سرزند </p></div>
<div class="m2"><p>شاید از اکسیر عشقت این مس من زر شود </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عزم و اخلاصی بده تا معرفت گیرد کمال </p></div>
<div class="m2"><p>معرفت کامل چوشد اخلاص کاملتر شود </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نشود اخلاص کاملتر رسد سلطان عشق </p></div>
<div class="m2"><p>آنچه بود افسار درسربعد از این افسر شود </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سهل وآسان کی دهددست اینچنین گنجی مگر</p></div>
<div class="m2"><p>پای تا سر زاری و افغان چشم تر شود </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا نباشد بندهٔ را عزم و اخلاص علی </p></div>
<div class="m2"><p>کی امیرالمومنین و نفس پیغمبر شود </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سالها باید بگردد آفتاب و مشتری </p></div>
<div class="m2"><p>تا که در برج سعادت نطفه حیدر شود </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در زمین دل بکار ای فیض تخم معرفت </p></div>
<div class="m2"><p>پس زچشمش آب ده تا ریشه محکمتر شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس بچین از شاخسارش میوه های گونه گون </p></div>
<div class="m2"><p>کز لطافت رشک باغ و جنت و کوثر شود </p></div></div>