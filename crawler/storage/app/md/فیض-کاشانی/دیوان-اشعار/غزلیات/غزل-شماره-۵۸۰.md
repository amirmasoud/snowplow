---
title: >-
    غزل شمارهٔ ۵۸۰
---
# غزل شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>میدمد هر دم خیالت روحی اندر قالبم</p></div>
<div class="m2"><p>روز میگردد ز خودرشید دلفروزت شبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میتپد دل شمع رویت را چو می‌بینم ز دور</p></div>
<div class="m2"><p>چون شدی نزدیک چون پروانه در تاب و تبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که تاب دیدن رویت نمی‌آرم چسان</p></div>
<div class="m2"><p>طاقت آن باشدم تا لب گداری بر لبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خیالت دم بدم در اضطراب آرد مرا</p></div>
<div class="m2"><p>پس وصالت تا چه خواهد کود تا روز و شبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و دل سوزد فراقت وصل دین غارت کند</p></div>
<div class="m2"><p>ای فدایت جان و دل وصل تو دین و مذهبم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو بدن بیتو بودن هیچیک مقدور نیست</p></div>
<div class="m2"><p>چارهٔ سازد مگر فریاد یارب یاربم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست پایانی رهت را راه خود مقصود نیست</p></div>
<div class="m2"><p>مانده‌ام حیران ندانم چیست آخر مطلبم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض عشقست این شکایت ترک کن تسلیم شو</p></div>
<div class="m2"><p>مهر ورزم جان کنم تا هست جان در قالبم</p></div></div>