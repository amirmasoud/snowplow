---
title: >-
    غزل شمارهٔ ۷۲۱
---
# غزل شمارهٔ ۷۲۱

<div class="b" id="bn1"><div class="m1"><p>نیست چو من واپسی در همه واپسان</p></div>
<div class="m2"><p>چو نیست من بیکسی در همه بیکسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واپسی من ببین بیکسی من ببین</p></div>
<div class="m2"><p>همرهیت کرده پس پیشروان واپسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم تو دهی نعمت و هم تو تمامش کنی</p></div>
<div class="m2"><p>ره تو نمودی مرا هم تو بمنزل رسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در همه دیدم بسی هیچ ندیدم کسی</p></div>
<div class="m2"><p>کرد روانم ملول دیدن این ناکسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست درین دیر کس تا شودم هم نفس</p></div>
<div class="m2"><p>همنفس من تو باش ای تو کس بیکسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا که نمیرد دلم از نفس سرد غیر</p></div>
<div class="m2"><p>نفخهٔ گرم از دمت دم بدمم میرسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر خدا هیچکس مونس جان تو نیست</p></div>
<div class="m2"><p>دست توقع بکش فیض ز خیر کسان</p></div></div>