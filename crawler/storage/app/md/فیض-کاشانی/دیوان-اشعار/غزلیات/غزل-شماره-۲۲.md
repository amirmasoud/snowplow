---
title: >-
    غزل شمارهٔ ۲۲
---
# غزل شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>آنچه را از بهر من او خواست آن آید مرا </p></div>
<div class="m2"><p>خواستش از راز پنهان ناگهان آید مرا </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سعی در تحصیل دنیا و فضولش بیهده است </p></div>
<div class="m2"><p>در ازل قدری که روزی شد همان آید مرا </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی مشرق گر روم یا راه مغرب بسپرم </p></div>
<div class="m2"><p>برجبینم آنچه بنوشته است آن آید مرا </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سرم گرچه نمیدانم چه خواهد آمدن </p></div>
<div class="m2"><p>اینقدر دانم که مردن بی امان آید مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ تمهیدی نکردم بهر مهمان اجل </p></div>
<div class="m2"><p>با وجود آنکه دانم ناگهان آید مرا </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگانی شد تلف سودی نیامد زان بکف </p></div>
<div class="m2"><p>نیست از کس شکوه ام از خود زیان آید مرا </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که خیری میکند اضعاف آن یابد جزا </p></div>
<div class="m2"><p>میدهم جان در رهش تا جان جان آید مرا </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که بخشد جرمی از کس بگذرند ازجرم او </p></div>
<div class="m2"><p>میکنم من اینچنین تا آنچنان آید مرا </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که بر تن میفزاید نور جان کم میکند </p></div>
<div class="m2"><p>میگذارم فیض تن تا نور جان آید مرا </p></div></div>