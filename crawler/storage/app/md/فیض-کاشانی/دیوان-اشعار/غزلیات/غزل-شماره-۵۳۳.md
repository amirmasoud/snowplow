---
title: >-
    غزل شمارهٔ ۵۳۳
---
# غزل شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>تا بکی حسرت برم بر کشتگان زار عشق</p></div>
<div class="m2"><p>هرچه باداباد گویان میروم بردار عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آشنایان جهان بیگانه گشتم در غمش</p></div>
<div class="m2"><p>از جهان بیزار گردد هرکه باشد زار عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که با عشق آشنا شد خویش را بیگانه دید</p></div>
<div class="m2"><p>عافیت را پشت پا زد هر که شد بیمار عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ازین هم گرچه بودم مست وار خود بیخبر</p></div>
<div class="m2"><p>مستی دیگر چشیدم تا شدم هشیار عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند ترسانی مرا از رستخیز خواب مرگ</p></div>
<div class="m2"><p>صد قیامت پیش دیدم تا شدم بیدار عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کتابی خوانده باشد جمله از یادش رود</p></div>
<div class="m2"><p>هر که او خواند چو من یکحرف از طومار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که میپرسی که یارت کیست یار کیستی</p></div>
<div class="m2"><p>یار من عشقست و من هم نیستم جز یار عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میفروشم صد هزاران دانهٔ تسبیح زهد</p></div>
<div class="m2"><p>تا خرم ازاهل دل یکرشته از زنار عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار من عشقست و بیکاریم عشق کار ساز</p></div>
<div class="m2"><p>بهتر است از صد هزاران کاروان بیکار عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الصلا یاران کشید از هرچه جز عشقست دست</p></div>
<div class="m2"><p>نیست کار و بار الا کار عشق و بار عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس به تنگ آمد مرا از هرچه جز عشقست دل</p></div>
<div class="m2"><p>میفروشم خویش را یک تنگه در بازار عشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که پرسد فیض زار کیست میگویم بلند</p></div>
<div class="m2"><p>زار عشقم زار عشقم زار عشقم زار عشق</p></div></div>