---
title: >-
    غزل شمارهٔ ۴۳۲
---
# غزل شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>هدهدی کو که از سبا گوید</p></div>
<div class="m2"><p>خبر یار آشنا گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو سلیمان که رمز منطق طیر</p></div>
<div class="m2"><p>از خدا گیرد و بما گوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو خضر تا که موسی جانرا</p></div>
<div class="m2"><p>از لدنا اشاره‌ها گوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوح کو تا که کشتی سازد</p></div>
<div class="m2"><p>من رکب فیه قد نجا گوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو خلیلی که رو بحق آرد</p></div>
<div class="m2"><p>لا احبی بما سوی گوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو کلیم اللهی لقا جوئی</p></div>
<div class="m2"><p>روبرو حرف با خدا گوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو مسیحی که مرده زنده کند</p></div>
<div class="m2"><p>خبری چند از سما گوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کو محمد که سرّ ما او حی</p></div>
<div class="m2"><p>با احبا و اولیا گوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کو علی آن در مدینه علم</p></div>
<div class="m2"><p>تا ز حق شمهٔ بما گوید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا چو جامی ز هل اتی نوشد</p></div>
<div class="m2"><p>رمزی از سرّ انما گوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اهل بیت نبی کجا رفتند</p></div>
<div class="m2"><p>و آنکه ز ایشان حدیث واگوید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همدمی کو که آشنا باشد</p></div>
<div class="m2"><p>با دلم حرف آشنا گوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا دل از مدعی نهان با او</p></div>
<div class="m2"><p>چند حرفی بمدعا گوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کو طبیب دلی درین عالم</p></div>
<div class="m2"><p>خستهٔ درد دل کرا گوید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا بگوشم رسد ندای الست</p></div>
<div class="m2"><p>هر سر موی من بلی گوید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا شوم مست بادهٔ توحید</p></div>
<div class="m2"><p>تا سرا پای من خدا گوید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با دل از مدعی نهان با دوست</p></div>
<div class="m2"><p>چند حرفی بمدعا گوید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا چو آن فانیان سبحانی</p></div>
<div class="m2"><p>بزبان خدا ثنا گوید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بس کن ای دل که حرف نازک شد</p></div>
<div class="m2"><p>فیض را گوی تا دعا گوید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شکوه بس فیض اهل دردی کو</p></div>
<div class="m2"><p>تا طبیبش از او دوا گوید</p></div></div>