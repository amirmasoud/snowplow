---
title: >-
    غزل شمارهٔ ۵۹۶
---
# غزل شمارهٔ ۵۹۶

<div class="b" id="bn1"><div class="m1"><p>رسید از دوست پیغامی که مستان را نظر کردم</p></div>
<div class="m2"><p>شدم من مست پیغامش ز خود بی‌خود سفر کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چوره بردم بکوی دوست کی گنجم دگر درپوست</p></div>
<div class="m2"><p>بیفکندم ز خود خود را رهش را پا ز سر کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چوجان آهنگ‌جانان کرد وصل دوست شد نزدیک</p></div>
<div class="m2"><p>ز پا تا سر بصر گشتم سراسر تن نظر کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیاد دوست چون افتم ز چشمانم گهر ریزد</p></div>
<div class="m2"><p>سرشگم را بدریای خیال او گهر کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جانم بر زبان گر چشمهٔ حکمت شود جاری</p></div>
<div class="m2"><p>از آن زاری مدد یابم که در وقت سحر کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا افکند هر گه سوی من تیر فراموشی</p></div>
<div class="m2"><p>بیادش تازه کردم جان خیالش را سپر کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدستم خیری ار جاری شود زان منبع خیر است</p></div>
<div class="m2"><p>ز من گر طاعتی آید نه پنداری هنر کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراری از دمم تا کم نگردد از دم سردی</p></div>
<div class="m2"><p>بهر جا زاهد خشکی که دیدم زو حذر کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر بیوقت و بیجا فیض رازی گفت معذور است</p></div>
<div class="m2"><p>هجوم غم چو جا را تنگ کرد از دل به در کردم</p></div></div>