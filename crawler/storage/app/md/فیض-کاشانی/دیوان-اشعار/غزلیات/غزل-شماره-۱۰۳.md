---
title: >-
    غزل شمارهٔ ۱۰۳
---
# غزل شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>زغفلت تو ترا صد حجاب در پیش است</p></div>
<div class="m2"><p>صفای چهرهٔ جانرا نقاب در پیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی کتاب بخواندی کتاب خویش بخوان </p></div>
<div class="m2"><p>زکردهای تو جانرا کتاب در پیش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حساب کردهٔ خود کن حساب در چه کنی </p></div>
<div class="m2"><p>که ماند از پس و روز حساب در پیش است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عذاب روح مکن بهر مال دنیی دون </p></div>
<div class="m2"><p>عذاب قبر و سوال و جواب در پیش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبهر آنچه زپس ماندت چه میسوزی </p></div>
<div class="m2"><p>زمین و حشر و تف آفتاب در پیش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جواب پرسش اعمال خود مهیا کن </p></div>
<div class="m2"><p>شدن ز شرم و خجالت چو آب در پیشست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توانی ار بعبادت شبی بروز آری </p></div>
<div class="m2"><p>بکن بکن که بهشت و ثواب در پیشست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توانی ار نکنی معصیت بدار فنا</p></div>
<div class="m2"><p>مکن مکن که جحیم و عقاب در پیش است </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمانی ارنکنی خواب در دل شب ها </p></div>
<div class="m2"><p>شود که در لحدت وقت خواب در پیش است </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نصیحت تو یکی فیض در دلت نگرفت</p></div>
<div class="m2"><p>ترا زگفتهٔ خود صد حجاب در پیش است </p></div></div>