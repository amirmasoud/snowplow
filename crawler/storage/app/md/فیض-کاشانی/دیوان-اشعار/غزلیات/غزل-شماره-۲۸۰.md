---
title: >-
    غزل شمارهٔ ۲۸۰
---
# غزل شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>بکوی سرّ قدر گر گذر توانی کرد</p></div>
<div class="m2"><p>به پیش تیر قضا جان سپر توانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنانکه هست اگر سرّ کار دریابی</p></div>
<div class="m2"><p>ز دل شکایت بیجا بدر توانی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دانی آنچه بتو میرسد نوشته شده است</p></div>
<div class="m2"><p>ز خار خار تاسف حذر توانی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدای را بعدالت اگر شناختهٔ</p></div>
<div class="m2"><p>بخویش نسبت اسباب شر توانی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز آینهٔ سر غبار بزدائی</p></div>
<div class="m2"><p>بچشم سر برخ او نظر توانی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نقاب بر افتد ز طلعت ازلی</p></div>
<div class="m2"><p>بیک نگاه ابد را بسر توانی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آستانهٔ جانان اگر دهد بارت</p></div>
<div class="m2"><p>سر و تن و دل و جان خاک در توانی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر ز عالم صورت ز صدق دل نکنی</p></div>
<div class="m2"><p>بجان بعالم معنی سفر توانی کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چگونه ثبت توان کرد فیض در اوراق</p></div>
<div class="m2"><p>حدیث عشق چه سان مختصر توانی کرد</p></div></div>