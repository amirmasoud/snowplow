---
title: >-
    غزل شمارهٔ ۱۶۹
---
# غزل شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>چون توان بود در آنجای که آسایش نیست </p></div>
<div class="m2"><p>یا بگنجید بسوفار که گنجایش نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه دهی دل بسرائی که دل از وی بکند </p></div>
<div class="m2"><p>یا نهی رخت بدان خانه که آسایش نیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او عاقبت اندیش بود دل ننهد </p></div>
<div class="m2"><p>در مقامی که بقا را ره گنجایش نیست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعمت دینی دون هیچ نگیرد دستت </p></div>
<div class="m2"><p>بعبث دست میالا که جز آلایش نیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مال و جاهی که بر آن روز بروز افزائی </p></div>
<div class="m2"><p>کاهش جان بود آن مایهٔ افزایش نیست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خویشتن را بفسون و حیل آراسته است </p></div>
<div class="m2"><p>نخری عشوهٔ دنیا که جز آرایش نیست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را زینت این زال دل از جا ببرد </p></div>
<div class="m2"><p>خون رود از نظر و فرصت پالایش نیست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست مشاطه نیارد رخ دنیا آراست </p></div>
<div class="m2"><p>زال بد منظر دون قابل آرایش نیست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست زندان خردمند و بهشت نادان </p></div>
<div class="m2"><p>نزد ارباب بصر قابل آسایش نیست </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه در دین کندت سود بجا آور زود </p></div>
<div class="m2"><p>ور زیانست بمان حاجت فرمایش نیست </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طاعت حق کن و بگذر ز شمار طاعت </p></div>
<div class="m2"><p>ره مپیما و برو فرصت پیمایش نیست </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منشین شاد و مجو خاطر جمع و دل خوش </p></div>
<div class="m2"><p>فیض ازین مرحله کاین منزل آسایش نیست </p></div></div>