---
title: >-
    غزل شمارهٔ ۶۶۹
---
# غزل شمارهٔ ۶۶۹

<div class="b" id="bn1"><div class="m1"><p>مرا هرچند رانی دیگر آیم</p></div>
<div class="m2"><p>اگر از پا در آیم از سر آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم از در برانی آیم از بام</p></div>
<div class="m2"><p>ورم از بام رانی از در آیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیارم صبر کردن بی تو یکدم</p></div>
<div class="m2"><p>که نتوانم بهجرانت بر آیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فراقت سخت خونریز است و بیباک</p></div>
<div class="m2"><p>وصالت را کجا من در خور آیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه با تو میتوان بودن نه بی تو</p></div>
<div class="m2"><p>ندانم تا بعشقت چون بر آیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکش خنجر بقصد کشتن من</p></div>
<div class="m2"><p>که تا رقصان به پیش خنجر آیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهم سر پیش تیغت بهر بسمل</p></div>
<div class="m2"><p>بقربانت شوم گردت بر آیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توئی خور منم از ذره کمتر</p></div>
<div class="m2"><p>چو ذره از عدم هم کمتر آیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر لطف تو دست فیض گیرد</p></div>
<div class="m2"><p>و گرنه در رهت از پا در آیم</p></div></div>