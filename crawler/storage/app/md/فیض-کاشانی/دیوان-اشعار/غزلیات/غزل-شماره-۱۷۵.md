---
title: >-
    غزل شمارهٔ ۱۷۵
---
# غزل شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>دل گرفتار ماه سیما ئیست </p></div>
<div class="m2"><p>جان هوادار سرو بالائیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه جنون گاه عقل و گه مستی </p></div>
<div class="m2"><p>در دل تنگ ما تماشائیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غم عشق هر پری روئی </p></div>
<div class="m2"><p>سرشوریده سر بصحرا ئیست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سرراه هر هلال ابروی </p></div>
<div class="m2"><p>از هجوم نظاره غوغائیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سرکوی هر بتی مه روی </p></div>
<div class="m2"><p>هر طرف زآب چشم دریائیست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لب لعل هر شکر دهنی </p></div>
<div class="m2"><p>در دل هر کسی تمنائیست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه همین فیض مست و شیدا شد </p></div>
<div class="m2"><p>که بهر گوشه مست و شیدائیست </p></div></div>