---
title: >-
    غزل شمارهٔ ۵۳۰
---
# غزل شمارهٔ ۵۳۰

<div class="b" id="bn1"><div class="m1"><p>هی نیاری بر زبان جز حرف حق</p></div>
<div class="m2"><p>نیست لایق زان دهان جز حرف حق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لا اوحش الله زان دهان شکرین</p></div>
<div class="m2"><p>حیف باشد زان لبان جز حرف حق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر وفای عهد و پیمان دل منه</p></div>
<div class="m2"><p>بر زبانت مگذران جز حرف حق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من چو حق گویم تو هم حق گوی باش</p></div>
<div class="m2"><p>تا نباشد در میان جز حرف حق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هی چه می‌گویم از آن حقه دهان</p></div>
<div class="m2"><p>گفتگو کی می‌توان جز حرف حق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باطل اندر آن دهان حق می‌شود</p></div>
<div class="m2"><p>کی برون آید از آن جز حرف حق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حق و باطل زان دهان شیرین بود</p></div>
<div class="m2"><p>فیض مشنو زاندهان جز حرف حق</p></div></div>