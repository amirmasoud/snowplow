---
title: >-
    غزل شمارهٔ ۷۴۱
---
# غزل شمارهٔ ۷۴۱

<div class="b" id="bn1"><div class="m1"><p>بود بدتر ز هر زهری مزیدن</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا عاقل کند کاری که باید</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخست اندیشه می‌کن تا نیاید</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جز بحر گنه لایق نباشد</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای معصیت باشد عقوبت</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بد کردی نباشد چاره الا</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا باید گنه کردن پس آنگه</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنازم طاعت حق کان ندارد</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز من بشنو که کار جاهلانست</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو واقع شد زیان سودی ندارد</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لکیلاتاسون کی می‌گذارد</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بر وفق قضا آمد چه حاصل</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس است ای فیض تن زن تا نباید</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن که می‌کشد جایی که باید</p></div>
<div class="m2"><p>سرانگشت پشیمانی گزیدن</p></div></div>