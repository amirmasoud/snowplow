---
title: >-
    غزل شمارهٔ ۲۳۰
---
# غزل شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>نمی‌بینم در این میدان یکی مرد</p></div>
<div class="m2"><p>زنانند این سبک عقلان بیدرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیدم مرد حق هر چند بردم</p></div>
<div class="m2"><p>بگرد این جهان چشم جهان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفته گرد گرداگرد عالم</p></div>
<div class="m2"><p>نمی‌بینم سواری زیر آن کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سواری هست پنهان از نظرها</p></div>
<div class="m2"><p>زنا محرم زنان پنهان بود مرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود مرد آنکه حق را بنده باشد</p></div>
<div class="m2"><p>به داغ بندگی بر دست هر مرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود مرد آنکه او زد بر هوا پای</p></div>
<div class="m2"><p>رگ و ریشه هوس از سربدر کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود مرد آنکه دل کند از دو عالم</p></div>
<div class="m2"><p>بیکجا داد و گشت از خویشتن فرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود مردآنکه با حق انس بگرفت</p></div>
<div class="m2"><p>باو پیوست و ترک ما سوا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود مرد آنکه اورست از من و ما</p></div>
<div class="m2"><p>برآورد از نهاد خویشتن گرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود مرد آنکه فانی گشت از خود</p></div>
<div class="m2"><p>ز تشریف بقای حق قبا کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرافشانی ز گرد خویش خود را</p></div>
<div class="m2"><p>بگردش کی رسی تا برخوری گرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز گرد خود برا در گرد اورس</p></div>
<div class="m2"><p>سراغی یابی ازگرد چنین مرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خداوندا بفضل خود مدد کن</p></div>
<div class="m2"><p>که ره یابم بمردی تا شوم مرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بمردی میرسی ای فیض و مردی</p></div>
<div class="m2"><p>بشرط آنکه کردی از خودی فرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خودی گردیست بر آینهٔ دل</p></div>
<div class="m2"><p>بمردی وارهان خود را ازین گرد</p></div></div>