---
title: >-
    غزل شمارهٔ ۱۷۱
---
# غزل شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>عاشقانرا در بهشت آرام نیست </p></div>
<div class="m2"><p>عشقبازی کار هر خود کام نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پختهٔ باید بلای عشق را </p></div>
<div class="m2"><p>کار این سودا پزان خام نیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چارهٔ عاشق همین بیچارگیست </p></div>
<div class="m2"><p>همدمش جز بخت نافرجام نیست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کام نتوان یافتن در راه عشق </p></div>
<div class="m2"><p>غیر ناکامی درین ره کام نیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست باید داشتن از ننگ و نام </p></div>
<div class="m2"><p>عشق را عاری چو ننک و نام نیست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین شب و روز مکرر دل گرفت </p></div>
<div class="m2"><p>ایخوش آنجائی که صبح و شام نیست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوبتر از خال و زلف دلبران </p></div>
<div class="m2"><p>دانهٔ مردم ربا و دام نیست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آبروی نیکوان دلدار ماست </p></div>
<div class="m2"><p>لیک با این خاک شینان رام نیست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا وصالش دست ندهد فیض را </p></div>
<div class="m2"><p>این دل سرگشته را آرام نیست </p></div></div>