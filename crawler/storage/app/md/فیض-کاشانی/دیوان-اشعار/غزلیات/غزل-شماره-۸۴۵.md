---
title: >-
    غزل شمارهٔ ۸۴۵
---
# غزل شمارهٔ ۸۴۵

<div class="b" id="bn1"><div class="m1"><p>ساقی باقی ما داد صلا بسم الله</p></div>
<div class="m2"><p>هر کرا هست سرانجام فنا بسم الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی ساقی بصفا سینه ما با هم صاف</p></div>
<div class="m2"><p>می مصفا شده اخوان صفا بسم الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد دوا درد غذا خون جگر عشق طبیب</p></div>
<div class="m2"><p>هر که جوید ز سر صدق شفا بسم الله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی عشق گرفته است بکف ساغر درد</p></div>
<div class="m2"><p>هر که دارد سر این جام بلا بسم الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایکه خواهی که نماز از سر اخلاص کنی</p></div>
<div class="m2"><p>سوی حق عشق بود قبله‌نما بسم الله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دلت آرزوی عکس جمالش دارد</p></div>
<div class="m2"><p>بنگر آینه سینهٔ ما بسم الله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منزل دوست بپرسیدم از آنشاه عرب</p></div>
<div class="m2"><p>کرد اشارت بدل و گفت عنا بسم الله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی دل رفتم و گفتم که بگو یار کجاست</p></div>
<div class="m2"><p>گفت اینجاست تو بیخویش درآ بسم الله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر درش رفتم و گفتم که دهی بار مرا</p></div>
<div class="m2"><p>گفت بگذار خود ترا و بیا بسم الله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فیض خواهد بره دوست روان افشاند</p></div>
<div class="m2"><p>هر که دارد سر همراهی ما بسم الله</p></div></div>