---
title: >-
    غزل شمارهٔ ۷۷
---
# غزل شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>گل بنفشه دمیدن گرفت گرد عذارت </p></div>
<div class="m2"><p>نه چشم بد نگریدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلط نه این ونه آن دودآه عاشق زارت</p></div>
<div class="m2"><p>بلند گشت و رسیدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه آنجمال دلاویز بس که داشت حلاوت</p></div>
<div class="m2"><p>سپاه مور چریدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلط که آهوی چشم توکرد نافه گشائی </p></div>
<div class="m2"><p>نسیم مشک وزیدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه خال گوشه چشم از نگاه گرم تو گردید </p></div>
<div class="m2"><p>تمام آب و چکیدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غلط که طوطی جان در هوای قند لب تو </p></div>
<div class="m2"><p>قفس شکست و پریدن گرفت گرد عذارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بحر خس بساحل فکند عنبر سارا </p></div>
<div class="m2"><p>مریض دل چو طپیدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که عکس غلط در آینهٔ جمال تو افتاد </p></div>
<div class="m2"><p>زلاله چون نگریدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه در هوای رخت بود ذرهٔ سان همه دلها </p></div>
<div class="m2"><p>بسوخت چونکه رسیدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غلط که آن مژهای سیاه سایه فکن شد </p></div>
<div class="m2"><p>چو سایه عکس فتیدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غلط که حسن نقابی بروی خویشتن افکند </p></div>
<div class="m2"><p>زشرم دیده چو دیدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه ترک ناز ملوکانه نرگس مستت </p></div>
<div class="m2"><p>سپاه آه خریدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غلط نداشت دل سوخته چو تاب فراقت </p></div>
<div class="m2"><p>زسینه جست و تنیدن گرفت گرد عذارت </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به باغ روی ترا آب داد فیض ز دیده </p></div>
<div class="m2"><p>چنانکه سبزه دمیدن گرفت گرد عذارت </p></div></div>