---
title: >-
    غزل شمارهٔ ۹۱۳
---
# غزل شمارهٔ ۹۱۳

<div class="b" id="bn1"><div class="m1"><p>نکنی گر تووفا حسبی الله کفی </p></div>
<div class="m2"><p>ورنهی روبجفا حسبی الله کفی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدتو نخل بلند بر آن شکروقند</p></div>
<div class="m2"><p>نکنی گر تو عطا حسبی الله کفی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو برویت نگرم حق بودش در نظرم </p></div>
<div class="m2"><p>نیم از اهل هو احسبی الله کفی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه زخمی می زنم گاه مرهم می نهم </p></div>
<div class="m2"><p>تاچه راخواست خداحسبی الله کفی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو درد و تو دوا از تو رنج تو شفا </p></div>
<div class="m2"><p> حق چنین ساخت تراحسبی الله کفی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرنهم بر درتو جان نهم بر سر تو </p></div>
<div class="m2"><p>تا شوم از شهدا حسبی الله کفی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل من بسته تو جان من خستهٔ تو </p></div>
<div class="m2"><p>نکنی گر تو دوا حسبی الله کفی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانده از من نفسی میروم سوی کسی </p></div>
<div class="m2"><p>تا رهم از من و ما حسبی الله کفی </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو کام ار نبرم ره دیگر سپرم </p></div>
<div class="m2"><p>یار فیض است خدا حسبی الله کفی</p></div></div>