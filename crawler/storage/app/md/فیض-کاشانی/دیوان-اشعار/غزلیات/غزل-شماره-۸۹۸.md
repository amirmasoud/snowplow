---
title: >-
    غزل شمارهٔ ۸۹۸
---
# غزل شمارهٔ ۸۹۸

<div class="b" id="bn1"><div class="m1"><p>رو بر در تو آریم رانی و گر نوازی</p></div>
<div class="m2"><p>جز تو کسی نداریم سازی و گر نسازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چاره ساز هر چند سازی تو چاره ما</p></div>
<div class="m2"><p>دیگر بتو گرائیم از بهر چاره سازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو شویم‌آباد وز تو شویم ویران</p></div>
<div class="m2"><p>شرمنده‌ایم تا کی ویران کنیم و سازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهد دلم بهر دم جانی کند فدایت</p></div>
<div class="m2"><p>کو جان بی‌نهایت عمری بدین درازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند شویم از خود آلایش هوسها</p></div>
<div class="m2"><p>یارب لباس تقوی کی میشود نمازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکس گرفته یاری ما و خیال جانان</p></div>
<div class="m2"><p>هر کس بفکر کاری مائیم و عشقبازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون رو نهد بمیدان در کف گرفته چوگان</p></div>
<div class="m2"><p>از ما فکندن سر از دوست گوی بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر پای او نهد سر جویای سر بلندی</p></div>
<div class="m2"><p>بر خاک او نهد رو خواهان سر فرازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمر دراز باید تا صرف عشق گردد</p></div>
<div class="m2"><p>برفیض مرحمت کن یا رب بجان درازی</p></div></div>