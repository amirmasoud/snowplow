---
title: >-
    غزل شمارهٔ ۹۳۸
---
# غزل شمارهٔ ۹۳۸

<div class="b" id="bn1"><div class="m1"><p>خوشا فال آن کو دوچارش شوی</p></div>
<div class="m2"><p>خوشا ‌حال آنکو نگارش شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آن بیدلیرا که پرسش کنی</p></div>
<div class="m2"><p>خوش آن بی کسیرا که یارش شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آشفته‌ایرا که آئی برش</p></div>
<div class="m2"><p>قرار دل بی‌قرارش شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شفا یابد آن دردمندی که تو</p></div>
<div class="m2"><p>انیس دل سوگوارش شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشا روز آن عاشق زار، تو</p></div>
<div class="m2"><p>شبی آئی و در کنارش شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه بیخود شود از لب و چشم تو</p></div>
<div class="m2"><p>تو هوش دل هوشیارش شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوی گاه خورشید روز خوشش</p></div>
<div class="m2"><p>گهی شمع شبهای تارش شوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند فیض چون جان بقربان تو را</p></div>
<div class="m2"><p>خوش آنکو تو شمع مزارش شوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه می‌آئی ایجان درین خاکدان</p></div>
<div class="m2"><p>خوشا حال تو گر نثارش شوی</p></div></div>