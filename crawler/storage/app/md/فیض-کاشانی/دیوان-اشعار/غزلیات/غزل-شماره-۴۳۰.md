---
title: >-
    غزل شمارهٔ ۴۳۰
---
# غزل شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>یاران میم ز بهر خدا در سبو کنید</p></div>
<div class="m2"><p>آلوده غمم بمیم شست و شو کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام لبالب می از آن دستم آرزوست</p></div>
<div class="m2"><p>بهر خدا شفاعت من نزد او کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مست می شوید ز شرب مدام دوست</p></div>
<div class="m2"><p>مستی بنده هم بدعا آرزو کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابریق می دهید مرا تا وضو کنم</p></div>
<div class="m2"><p>در سجده‌ام بجانب میخانه رو کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیمار چون شوم ببریدم بمیکده</p></div>
<div class="m2"><p>از بهر صحتم بخم پی فرو کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خویش چون روم بمیم باز آورید</p></div>
<div class="m2"><p>آیم به خویش باز میم در گلو کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت رحیل سوی من آرید ساغری</p></div>
<div class="m2"><p>رنگم چو زرد شد بمیم سرخ رو کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تابوت من ز تاک و کفن هم ز برگ تاک</p></div>
<div class="m2"><p>در میکده بباده مرا شست و شو کنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا زنده‌ام نمیروم از میکده برون</p></div>
<div class="m2"><p>بعد از وفات نیز بدان سوم رو کنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خاکدان من بگذارید یک دو خم</p></div>
<div class="m2"><p>دفنم چو میکنید میم در گلو کنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از مرقدم بمیکده‌ها جویها کنید</p></div>
<div class="m2"><p>از هر خم و سبوی رهی هم بجو کنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دردی کشان ز هم چو بپاشد وجود من</p></div>
<div class="m2"><p>در گردن شما که ز خاکم سبو کنید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ناید بغیر ریزهٔ خم یا سبو بدست</p></div>
<div class="m2"><p>هر چند خاکدان مرا جست‌وجو کنید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی بادگان چو مستیتان آرزو شود</p></div>
<div class="m2"><p>آئید و خاک مقبرهٔ فیض بو کنید</p></div></div>