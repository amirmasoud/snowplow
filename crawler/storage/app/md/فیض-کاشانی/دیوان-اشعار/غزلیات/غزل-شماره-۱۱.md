---
title: >-
    غزل شمارهٔ ۱۱
---
# غزل شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>از دل که برد آرام حسن بتان خدا را </p></div>
<div class="m2"><p>ترسم دهد بغارت رندی صلاح ما را </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساز و شراب و شاهد نی محتسب نه زاهد </p></div>
<div class="m2"><p>عیشی است بی کدورت بزمیست بی مدارا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس ببانک نی ساز مطرب سرود پرداز </p></div>
<div class="m2"><p>ساقی مهٔ دل افروز شاهد بت دل آرا </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با اینهمه چسان دین در دل قرار گیرد </p></div>
<div class="m2"><p>تقوی چگونه باشد در کام کس گوارا </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از محتسب که ما را منع از شراب فرمود </p></div>
<div class="m2"><p>ساغر گرفت بر کف میخورد آشکارا </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن زاهدی که با ما خشم و ستیزه میکرد </p></div>
<div class="m2"><p>شاهد کشید در بر فی زمره السّکارا </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فهمید عشق زاهد شاهد گرفت عابد </p></div>
<div class="m2"><p>میخانه گشت مسجد واعظ بماند جا را </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون طبع ما جوان شد با پیر کی توان بود</p></div>
<div class="m2"><p>کر چلّه را بماندیم معذور دار ما را </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض از کلام حافظ میخوان برای تعوید </p></div>
<div class="m2"><p>دل میرود زدستم صاحبدلان خدا را </p></div></div>