---
title: >-
    غزل شمارهٔ ۴۴۸
---
# غزل شمارهٔ ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>شب همه شب زاری بر در پروردگار</p></div>
<div class="m2"><p>روز چو شد یاری خسته دلان فکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد گدائی بده بر در الله دوست</p></div>
<div class="m2"><p>داد گدایان بده از مدد کردگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم ز دل خستگان تا بتوانی ببر</p></div>
<div class="m2"><p>بر در حق ناله‌ها تا بتوانی بیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد قیامت بروز تا بتوانی بکن</p></div>
<div class="m2"><p>اشک ندامت بشب تا بتوانی ببار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیسهٔ پر زر به‌روز در ره مسکین بریز</p></div>
<div class="m2"><p>کاسهٔ چوبین فقر بر در حق شب بدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب همه شب جان بده در طلب مغفرت</p></div>
<div class="m2"><p>روز چو شد نان بده از طلب کسب و کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کن سبک از ناله شب دوش ز بار گناه</p></div>
<div class="m2"><p>روز ز بهر کسان دوش بنه زیر بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش نگردد سبک از غم یک معصیت</p></div>
<div class="m2"><p>تا نکشی از خسان جور گرانی هزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باش چو در محفلی دل بخدا رو بخلق</p></div>
<div class="m2"><p>چونکه بخلوت رَوی رویِ دلت سوی یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچه نمودم بتو راه صوابست فیض</p></div>
<div class="m2"><p>گر روی اینره رسی زود بپروردگار</p></div></div>