---
title: >-
    غزل شمارهٔ ۳۶۸
---
# غزل شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>یار را با تو کار خواهد بود</p></div>
<div class="m2"><p>کارها را شمار خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه پنهان بود شود پیدا</p></div>
<div class="m2"><p>لیل جانرا نهار خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه خفته است شب چه روز شود</p></div>
<div class="m2"><p>آگه و هوشیار خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که کاری نمیکند امروز</p></div>
<div class="m2"><p>حال فرداش زار خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار اگر زار باشدت فردا</p></div>
<div class="m2"><p>با خودت کار زار خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌حسابی که میکنی یک یک</p></div>
<div class="m2"><p>در حساب و شمار خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که با نفس خود جهاد نکرد</p></div>
<div class="m2"><p>حسرتش بی‌شمار خواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در کارهای خود نرسید</p></div>
<div class="m2"><p>سخرهٔ انتظار خواهد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که میران کار خود سنجید</p></div>
<div class="m2"><p>خاطرش را قرار خواهد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که مست شراب دنیا شد</p></div>
<div class="m2"><p>تا ابد در خمار خواهد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که مشتاق آخرت باشد</p></div>
<div class="m2"><p>رحمت او را نثار خواهد بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اختیار ار بحق سپرد اینجا</p></div>
<div class="m2"><p>مالک اختیار خواهد بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور بود اختیار را مالک</p></div>
<div class="m2"><p>سخرهٔ اختیار خواهد بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ازل شرم اگر نشد روزیش</p></div>
<div class="m2"><p>تا ابد شرمسار خواهد بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این غزل در جواب مولانا</p></div>
<div class="m2"><p>فیض را یادگار خواهد بود</p></div></div>