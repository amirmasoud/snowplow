---
title: >-
    غزل شمارهٔ ۸۳
---
# غزل شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>یارب چمن حسن تو خرم زچه آبست </p></div>
<div class="m2"><p>کاندر نظرم هر چه به جز تست سرابست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر از دل عشاق تو معمور ندیدیم </p></div>
<div class="m2"><p>گشتیم سراپای جهان جمله خرابست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس که چشید از می عشق تو نشد پیر </p></div>
<div class="m2"><p>مستان غمترا همهٔ عمرشبابست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عهد صبا توبه شکستیم بصهبا </p></div>
<div class="m2"><p>دیریست که سجاده مارهن شرابست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رندی که بمستی گذراند همه عمر </p></div>
<div class="m2"><p>فارغ زغم پرسش و اندوه حسابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هشیار کجا گردد زآشوب قیامت </p></div>
<div class="m2"><p>آن مست که از نشأهٔ چشم تو خراب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر بحرو بر و خشک و تر دهر گذشتیم </p></div>
<div class="m2"><p>جز آب رخ دوست جهان جمله سرابست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرکن ز می صاف غزل ساغر دیوان </p></div>
<div class="m2"><p>جانرا می بی دردسر ای فیض کتابست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر میکده ویران و خرابات خرابست </p></div>
<div class="m2"><p>در هر نگه چشم تو صد گونه شرابست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم گردش چشم تو مگر با خودش آرد </p></div>
<div class="m2"><p>آن مست که از گردش چشم تو خرابست </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدار کجا گردد از آشوب قیامت </p></div>
<div class="m2"><p>آن دیده که با فتنه چشم تو بخوابست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پروا نکند زآتش جانسوز جهنم </p></div>
<div class="m2"><p>آن سینه که بر آتش عشق توکبابست </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با آنهمه تمکین که سراپای تو دارد </p></div>
<div class="m2"><p>چون عمر زما میگذری این چوشتابست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان لطف نهان با دل ما هیچ نکردی </p></div>
<div class="m2"><p>باری همه گر قهر و عتابست حسابست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تنها نه دل فیض خراب از نگه تست </p></div>
<div class="m2"><p>کو دل که نه زآن غمزه مستانه خرابست </p></div></div>