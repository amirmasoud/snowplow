---
title: >-
    غزل شمارهٔ ۱۸۳
---
# غزل شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>هر که در دوست زد دامن احسان گرفت </p></div>
<div class="m2"><p>و آنکه در دوستی مایة عرفان گرفت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستی کردگار معرفت آرد بیار </p></div>
<div class="m2"><p>هر که از این تخم کشت حاصل از آن گرفت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ار در احسان هر انک روی بمقصود کرد </p></div>
<div class="m2"><p>دید جمال خدا حسن زاحسان گرفت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که بدو داد تن مایة ایمان ستد </p></div>
<div class="m2"><p>وانکه بدو داد دل در عوضش جان گرفت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه بدو داد جان زندة جاوید شد </p></div>
<div class="m2"><p>عمر دو روزینه داد عمر فراوان گرفت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که زدنیا گذشت لذت عقبی چشید </p></div>
<div class="m2"><p>وانکه زعقبی گذشت کام زجانان گرفت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه باخلاص داد در ره او هر چه داشت </p></div>
<div class="m2"><p>قطره بدریا گذشت بهره زعمان گرفت </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیک و بد هر که هست سوی خودش عایدست </p></div>
<div class="m2"><p>هر چه در امروز کرد روز جزا آن گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ره عرفان و عشق فیض بسی سعی کرد </p></div>
<div class="m2"><p>تا که بتوفیق حق عشق زعرفان گرفت </p></div></div>