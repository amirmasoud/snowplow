---
title: >-
    غزل شمارهٔ ۷۱۱
---
# غزل شمارهٔ ۷۱۱

<div class="b" id="bn1"><div class="m1"><p>آرامت از تن میرود زین شاهدان سیمتن</p></div>
<div class="m2"><p>یا رب چو مستیها کنی ز آن ساقی جان پیرهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گلرخان بیوفا دل میرود ار جا ترا</p></div>
<div class="m2"><p>گر جور بنماید لقا جانت نگنجد در بدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حسن جان لذت بری تا حسن جانت چون کند</p></div>
<div class="m2"><p>از حسن جانان خود مگو کز تو نماند ما و من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه داری صد طرب از نشاه نبت العنب</p></div>
<div class="m2"><p>گر تر کنی ز آن باده لب جانت برقصد در بدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین آب تلخ ناگوار گر بگذری روزی سه چار</p></div>
<div class="m2"><p>از سلسبیل خوشگوار جان گرددت هر ذره تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احزای تن چون‌جان شود جان‌تاچه سرمستان شود</p></div>
<div class="m2"><p>مستغرق جانان شود در عالم بی ما و من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این می چو در تن جا کند جانرا چنین شیدا کند</p></div>
<div class="m2"><p>آن می چو با جانها کند چون جان اگر آید بتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز الایش تن پاک شو چالاک بر افلاک شو</p></div>
<div class="m2"><p>تا جان ز جانان برخورد نزدیک او گیرد وطن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای فیض در دنیا بچش از جام عشقش جرعهٔ</p></div>
<div class="m2"><p>در خاک تا مستی کنی تا عشق بازی در کفن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر دیدهٔ جانرا جلی سازی بانوار علی</p></div>
<div class="m2"><p>نزد حسینت جا دهد بنمایدت روی حسن</p></div></div>