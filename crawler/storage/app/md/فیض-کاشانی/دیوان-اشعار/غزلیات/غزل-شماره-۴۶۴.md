---
title: >-
    غزل شمارهٔ ۴۶۴
---
# غزل شمارهٔ ۴۶۴

<div class="b" id="bn1"><div class="m1"><p>برون آی و خورشید رخ بر افروز</p></div>
<div class="m2"><p>شب فرقت ماست مشتاق روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هجر تو تا چند سوزد دلم</p></div>
<div class="m2"><p>جمالی بر افروز و هجران بسوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراق تو تاکی گهی وصل هم</p></div>
<div class="m2"><p>همه شب مده گاه شب گاه روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا وصل و هجران شب و روزیست</p></div>
<div class="m2"><p>گهی این گهی آن بساز و بسوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی مست شو گاه مخمور باش</p></div>
<div class="m2"><p>گهی پرده در باش که پرده روز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زاهد ز مستیت پرسد بگو</p></div>
<div class="m2"><p>مرا جایز آمد ترا لایجوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجو وصل دایم تو ای فیض ازو</p></div>
<div class="m2"><p>نهٔ قابل این سعادت هنوز</p></div></div>