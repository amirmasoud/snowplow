---
title: >-
    غزل شمارهٔ ۳۴۲
---
# غزل شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>در روی چه خورشید تو دیدن نگذارند</p></div>
<div class="m2"><p>گرد سر شمع تو پریدن نگذارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بدر جبین تو هلالی ننمایند</p></div>
<div class="m2"><p>گل گل شکفد زان رخ و چیدن نگذارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد بار نظر افکنم آن سوی و مکرر</p></div>
<div class="m2"><p>از شرم و حیای تو رسیدن نگذارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعل تو مگر خمر بهشتست که کس را</p></div>
<div class="m2"><p>زان باده درین نشاه چشیدن نگذارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آب حیات است که جز خضر خط تو</p></div>
<div class="m2"><p>کس را بحوالیش چریدن نگذارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تیغ زدی جان طلبی قاعدهٔ کیست</p></div>
<div class="m2"><p>بسمل شدگانرا بطپیدن نگذارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دام تو افتاد دل فیض و مر او را</p></div>
<div class="m2"><p>زین سلسله تا حشر رهیدن نگذارند</p></div></div>