---
title: >-
    غزل شمارهٔ ۵۷۳
---
# غزل شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>ای خوشا وقت عاشق بد نام</p></div>
<div class="m2"><p>حبذا حال رند درد آشام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبری خواهم و لب کشتی</p></div>
<div class="m2"><p>تا زمانی ز عمر گیرم کام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذتی نیست درد و کون مگر</p></div>
<div class="m2"><p>لذت عاشقی و باده و جام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دود و خاکستر حریق فراق</p></div>
<div class="m2"><p>به ز جان و دل فسردهٔ خام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نخواهی گل سبو گردی</p></div>
<div class="m2"><p>صاف کن دل بدردی ته جام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیض اگر کام جاودان خواهی</p></div>
<div class="m2"><p>مست میباش و عاشق و بدنام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حقیقت بگوی در پرده</p></div>
<div class="m2"><p>گو سخن را مجاز باشد نام</p></div></div>