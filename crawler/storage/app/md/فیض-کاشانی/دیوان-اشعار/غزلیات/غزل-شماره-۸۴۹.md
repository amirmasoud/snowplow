---
title: >-
    غزل شمارهٔ ۸۴۹
---
# غزل شمارهٔ ۸۴۹

<div class="b" id="bn1"><div class="m1"><p>رفتم بخرابات توکلت علی‌الله</p></div>
<div class="m2"><p>وارستم از آفات توکلت علی‌الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خرقه و سجاده و تسبیح گذشتم</p></div>
<div class="m2"><p>در کشف و کرامات توکلت علی‌الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرقه سالوس نهان چند توان داشت</p></div>
<div class="m2"><p>بتخانهٔ طاعات توکلت علی‌الله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزی بدر آوردم و بر خاک فکندم</p></div>
<div class="m2"><p>بر سنگ زدم لات توکلت علی‌الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آب و گل خویش سبک گشتم و رفتم</p></div>
<div class="m2"><p>تا بام سموات توکلت علی‌الله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه سفر طامه کبراست توکل</p></div>
<div class="m2"><p>تا چند ز طامات توکلت علی‌الله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویم سخنی فیض اگرنه خرفی تو</p></div>
<div class="m2"><p>بگذر ز خرافات توکلت علی‌الله</p></div></div>