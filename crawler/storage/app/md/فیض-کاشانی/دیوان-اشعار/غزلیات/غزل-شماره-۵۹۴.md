---
title: >-
    غزل شمارهٔ ۵۹۴
---
# غزل شمارهٔ ۵۹۴

<div class="b" id="bn1"><div class="m1"><p>من ببوی خوش تو دلشادم</p></div>
<div class="m2"><p>ور نه از خود گرهی بر بادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوم از خویش بهر لحظه خراب</p></div>
<div class="m2"><p>کند آن لطف خفی آبادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی نسیمت بردم باد صبا</p></div>
<div class="m2"><p>لطف کن تا نهی بر بادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خوش آندم که مرا یاد کنی</p></div>
<div class="m2"><p>ای که یکدم نروی از یادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف پنهان ز دلم باز مگیر</p></div>
<div class="m2"><p>که درین لطف نهانی زادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطف تو گر نبود با غم تو</p></div>
<div class="m2"><p>قهر این غم بکند بنیادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرسی گر تو بفریاد دلم</p></div>
<div class="m2"><p>از فلک هم گذرد فریادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیستون غمت و تیشهٔ صبر</p></div>
<div class="m2"><p>که تو شیرینی و من فرهادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمر بندگیت بست چو فیض</p></div>
<div class="m2"><p>از غم هر دو جهان آزادم</p></div></div>