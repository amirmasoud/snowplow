---
title: >-
    غزل شمارهٔ ۶۷۵
---
# غزل شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>ما سر مستان مست مستیم</p></div>
<div class="m2"><p>با ساقی و می یکی شدستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ساقی و یار محو گشتیم</p></div>
<div class="m2"><p>از ننگ وجود خویش رستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دست بدست دوست دادیم</p></div>
<div class="m2"><p>پیوند ز خویشتن گسستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چشم بروی او گشادیم</p></div>
<div class="m2"><p>زان نرگس مست مست مستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا پای بکوی او نهادیم</p></div>
<div class="m2"><p>از دست ببوی او شدستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با باده زدیم جوش در خم</p></div>
<div class="m2"><p>تا باده شدیم و خم شکستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما باده و باده ما دوئی نیست</p></div>
<div class="m2"><p>ما رسم دوئی بهم ز دستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما از مستی و مستی است از ما</p></div>
<div class="m2"><p>در روز الست عهد بستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما از ساقی و ساقی است از ما</p></div>
<div class="m2"><p>در عیش بکام دل نشستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مستی نکنیم از آب انگور</p></div>
<div class="m2"><p>ما مست ز بادهٔ الستیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما بی می مستی دمی نبودیم</p></div>
<div class="m2"><p>بودیم همیشه مست و هستیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از ما مطلب صلاح و تقوی</p></div>
<div class="m2"><p>ما عاشق و رند و می پرستیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برخواسته‌ایم از دو عالم</p></div>
<div class="m2"><p>تا در صف میکشان نشستیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کس پای بما ندارد ایفیض</p></div>
<div class="m2"><p>ما سر مستان مست مستیم</p></div></div>