---
title: >-
    غزل شمارهٔ ۶۳
---
# غزل شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>بیمار زارم دریاب دریاب </p></div>
<div class="m2"><p>جز تو ندارم دریاب دریاب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه عشقت از پا فتادم </p></div>
<div class="m2"><p>رحمی که زارم دریاب دریاب </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو از رخ تو در خاک و در خون </p></div>
<div class="m2"><p>جان می سپارم دریاب دریاب </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان شد خیالی تن شد هلالی </p></div>
<div class="m2"><p>زار و نزارم دریاب دریاب </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سخت جانی ابرو کمانی </p></div>
<div class="m2"><p>افتاده کارم دریاب دریاب </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد زاشک خونین رویم منقّش </p></div>
<div class="m2"><p>زیبا نگارم دریاب دریاب </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل شد زشوق آب بشتاب بشتاب </p></div>
<div class="m2"><p>طاقت ندارم دریاب دریاب </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد در فراقت نامهربانا</p></div>
<div class="m2"><p>از دست کارم دریاب دریاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشکل که فیضت زین غم برد جان </p></div>
<div class="m2"><p>بیمار و زارم دریاب دریاب </p></div></div>