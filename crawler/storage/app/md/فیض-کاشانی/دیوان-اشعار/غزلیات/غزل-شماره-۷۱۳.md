---
title: >-
    غزل شمارهٔ ۷۱۳
---
# غزل شمارهٔ ۷۱۳

<div class="b" id="bn1"><div class="m1"><p>بهار آمد بهار آمد بهار طلعت جانان</p></div>
<div class="m2"><p>نگار آمد نگار آمد نگار شاهد پنهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار آمد بهار آمد بهار دل بهار دل</p></div>
<div class="m2"><p>نگار آمد نگار آمد نگار جان نگار جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشب خورشید جان آمد ضیای جاودان آمد</p></div>
<div class="m2"><p>بجان بگشای چشم دل که پیدا گشت هر پنهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم از کوی یار آمد نسیم مشکبار آمد</p></div>
<div class="m2"><p>معطر کن دماغ دل منور ساز چشم جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلافی کن تلافی کن ز بیعت آنچه ضایع شد</p></div>
<div class="m2"><p>ترقی کن ترقی کن درآ در مشهد عرفان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گمان تا کی گمان تا کی یقین آمد یقین آمد</p></div>
<div class="m2"><p>برون آ از حضیض شک برا بر آسمان جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیفکن بار تن از جان سبک کن دوش دل از گل</p></div>
<div class="m2"><p>چه ماندی در زمین تن برا بر آسمان جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سراپا دیده شو ای فیض همچون آب و آئینه</p></div>
<div class="m2"><p>که تا به بینی عیان هر جا جمال طلعت یزدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیفشان گرد خود از خود دل و جانرا جلائی ده</p></div>
<div class="m2"><p>جهان بگرفت سرتاسر به بینش ظاهر و پنهان</p></div></div>