---
title: >-
    غزل شمارهٔ ۲۲۹
---
# غزل شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>دل از ادنی کند آن کس که بر اعلی نظر بندد</p></div>
<div class="m2"><p>شکوفه برگ افشاند که تا بادام تر بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا رفعت اگر باید ره افتادگی بسپر</p></div>
<div class="m2"><p>ز بالا قطره می‌بندد که در پائین گهر بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسوزد تا دل از عشقی به سر شوری نمی‌افتد</p></div>
<div class="m2"><p>ندارد درد سر چون کس چرا چیزی به سر بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمیگنجد بیکدل غیر یک معشوق، ممکن نیست</p></div>
<div class="m2"><p>نه بندد تا بمعشوقی ز معشوقی نظر بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر اندر راه آن بازو کمر در خدمت آن بند</p></div>
<div class="m2"><p>که فرقت را نهد تاج و میانت را کمر بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهی سر بر درش بخشد ترا از معرفت تاجی</p></div>
<div class="m2"><p>بفرمانش کمر بندی ترا مهرش کمر بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یدالله دست جان گیرد یحب‌الله دهد جانش</p></div>
<div class="m2"><p>اگر بعد از قل الله همتی بر ثم در بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلی با حق به پیوندد که اخلاصی در آن باشد</p></div>
<div class="m2"><p>کسی مخلص تواند شد که خود را بر خطر بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا ای فیض دست از خویشتن بردار یکباره</p></div>
<div class="m2"><p>که تا دست خدا بر رویت ازاغبار در بندد</p></div></div>