---
title: >-
    غزل شمارهٔ ۹۲۷
---
# غزل شمارهٔ ۹۲۷

<div class="b" id="bn1"><div class="m1"><p>جانم اسیر تا کی در خنگ زندگانی</p></div>
<div class="m2"><p>کاش از عدم نکردی آهنگ زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای مرگ پردهٔ تن از روی جان برافکن</p></div>
<div class="m2"><p>تا دل ز دوده گردد از زنگ زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیدوست گر سرا آری‌ای عمر من بفردا</p></div>
<div class="m2"><p>سر بر ندارم از خشت از ننگ زندگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زندگی نچیدم هرگز گلی از آنروی</p></div>
<div class="m2"><p>یا رب مباد مرگم در رنگ زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیش مکدر تن بر عیش صاف جان زد</p></div>
<div class="m2"><p>بشکست آیئنه جان از سنگ زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل تنگ شد زرنگش در ننگ صلح و جنگش</p></div>
<div class="m2"><p>یا رب خلاصیم ده از چنگ زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این نیم جان خود را در راه دوست در باز</p></div>
<div class="m2"><p>تا چند باشی ای فیض در ننگ زندگانی</p></div></div>