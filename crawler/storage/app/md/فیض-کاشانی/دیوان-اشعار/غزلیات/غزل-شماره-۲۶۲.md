---
title: >-
    غزل شمارهٔ ۲۶۲
---
# غزل شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>خوشا آنکو انابت با خدا کرد</p></div>
<div class="m2"><p>بحق پیوست و ترک ماسوا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا آنکو دلش شد از جهان سرد</p></div>
<div class="m2"><p>گذشت از هر هوس ترک هوا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا آنکسکه دامن چید از غبار</p></div>
<div class="m2"><p>بیار واحد فرد اکتفا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا آنکسکه فانی گشت از خود</p></div>
<div class="m2"><p>ز تشریف بقای حق قبا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشا آنکو در بلا ثابت قدم ماند</p></div>
<div class="m2"><p> بجان و دل بعهد او وفا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش آنکو لذت دار الفنا را</p></div>
<div class="m2"><p>فدای لذت دار البقا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش آن دانا که هر دانش که اندوخت</p></div>
<div class="m2"><p>یکایک را عمل بر مقتضا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشا آنکو بحدس صایب عقل</p></div>
<div class="m2"><p>مهم و نامهم از هم جدا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشا آنکو به تنهائی گرفت انس</p></div>
<div class="m2"><p>چو فیض ایام بگذشته قضا کرد</p></div></div>