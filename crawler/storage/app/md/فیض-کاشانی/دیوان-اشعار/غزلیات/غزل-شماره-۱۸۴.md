---
title: >-
    غزل شمارهٔ ۱۸۴
---
# غزل شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>بر سر راهش فتاده غرق اشگم دید و رفت </p></div>
<div class="m2"><p>زیرلب بر گریهٔ خونین من خندید و رفت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دو عالم بود در دستم همین دین و دلی </p></div>
<div class="m2"><p>یکنظر دردیده کردآن هر دون را دزدید و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه دل از پا درآمد در ره عشقش ولی </p></div>
<div class="m2"><p>اندرین ره میتوان در خاک و خون غلطید و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سربالینم آمد گفتمش یکدم بایست </p></div>
<div class="m2"><p>تا که جان بر پایت افشانم ز من نشنید و رفت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان به لب آمد ز یاد آن لبم لیکن گرفت </p></div>
<div class="m2"><p>از خیالش بوسهٔ دل جان نو بخشید و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جهان جای اقامت نیست جای عبرتست </p></div>
<div class="m2"><p>زینتش را دل نباید بست، باید دید و رفت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض آمد تا ز وصل دوست یابد کام جان </p></div>
<div class="m2"><p>یک نظر نادیده رویش جان و دل بخشید و رفت</p></div></div>