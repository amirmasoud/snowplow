---
title: >-
    غزل شمارهٔ ۹۵۴
---
# غزل شمارهٔ ۹۵۴

<div class="b" id="bn1"><div class="m1"><p>ای شاهد شاهدان کجائی</p></div>
<div class="m2"><p>وی آب رخ بتان کجائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان هر آنچه در جهانست</p></div>
<div class="m2"><p>وز تو روشن جهان کجائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای هیچ مکان ز تو تهی نه</p></div>
<div class="m2"><p>وی پر ز تو لامکان کجائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای چشم و چراغ عالم دل</p></div>
<div class="m2"><p>ای جان جهان و جان کجائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من تاب فراق تو ندارم</p></div>
<div class="m2"><p>ای از نظرم نهان کجائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای کام دل شکسته من</p></div>
<div class="m2"><p>وی آرزوی روان کجائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدار بکس نمی‌نمائی</p></div>
<div class="m2"><p>ای در همه جا عیان کجائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیروی تو دل بود فسرده</p></div>
<div class="m2"><p>ای گرمی عاشقان کجائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از فیض تو سوخت فیض دلرا</p></div>
<div class="m2"><p>او را تو میان جان کجائی</p></div></div>