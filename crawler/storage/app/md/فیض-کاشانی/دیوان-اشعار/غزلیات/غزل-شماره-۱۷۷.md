---
title: >-
    غزل شمارهٔ ۱۷۷
---
# غزل شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>بمرد رستم زال و زتن غبار گذاشت </p></div>
<div class="m2"><p>ببرد حسرت و عبرت بیادگار گذاشت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا کسی که چو رو کرد سوی او دنیا </p></div>
<div class="m2"><p>باختیار گذشت و باختیار گذاشت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدا کسی که طلب کرد و دل بدنیا بست </p></div>
<div class="m2"><p>باختیار گرفت و باضطرار گذاشت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذاشت هر که به جز کرد گار حسرت بود </p></div>
<div class="m2"><p>خوشا کسی که دلش را بکردگار گذاشت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک نگردد الا بمدعای کسی </p></div>
<div class="m2"><p>که کار خویش بخلاق کار و بار گذاشت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو اختیار ندادند بنده را در کار </p></div>
<div class="m2"><p>خنک کسی که بمختار اختیار گذاشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو فیض هر که بدنیا نبست دل جان برد </p></div>
<div class="m2"><p>دعای خیر زنیکان بیادگار گذاشت </p></div></div>