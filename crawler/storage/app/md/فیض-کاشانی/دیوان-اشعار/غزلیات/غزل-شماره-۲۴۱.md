---
title: >-
    غزل شمارهٔ ۲۴۱
---
# غزل شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>ز شراب وصل جانان سر من خمار دارد</p></div>
<div class="m2"><p>سر خود گرفته دل هم سر آن دیار دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه کند دیگر جهانرا چو رسید جان بجانان</p></div>
<div class="m2"><p>چو رسید جان بجانان بجهان چه کار دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر من ندارد این سر غم من ندارد این دل</p></div>
<div class="m2"><p>که باین سرو باین دل غم کار و بار دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببر از سرم نصیحت ببر از برم گرانی</p></div>
<div class="m2"><p>نه سرم خرد پذیرد نه دلم قرار دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر من پر از جنون و دل من پر است از عشق</p></div>
<div class="m2"><p>نه سرم مجال عقل و نه دل اختیار دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر پر غرور زاهد بیخیال حور خرسند</p></div>
<div class="m2"><p>دل بی‌قرار عاشق سر زلف یار دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر زاهدان نخوانی غزل و قصیده‌ای فیض</p></div>
<div class="m2"><p>که تراست شعر و زاهد همه خشک بار دارد</p></div></div>