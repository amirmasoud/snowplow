---
title: >-
    غزل شمارهٔ ۳۴۸
---
# غزل شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>آنکه باشد مست زهد او عیب مستان چون کند</p></div>
<div class="m2"><p>خود بت خود گشته منع بت‌پرستان چون کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چنین روئی مکن بیهوده منعم زاهدا</p></div>
<div class="m2"><p>هر که دارد چشم با این گوش با آن چون کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاعت حق بهر کام خود کنی گوئی مرا</p></div>
<div class="m2"><p>روز و شب گرد بتان گشتن مسلمان چون کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قبله من گرچه اینانند مقصودم خداست</p></div>
<div class="m2"><p>ور نه مرد ره دل اندر بند طفلان چون کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خدا را میپرستی بهر شیر و انگبین</p></div>
<div class="m2"><p>بندگی از بهر خوردن اهل ایمان چون کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خدا می بینم اندر روی شاهد خط گواه</p></div>
<div class="m2"><p>زانکه نا پاینده نور خویش رخشان چون کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو خدا را بهر خود خواهی من اینان بهر او</p></div>
<div class="m2"><p>زاهدا انصاف خواهم منع این آن چون کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میکنم دعوی حق بینی ولی اثبات آن</p></div>
<div class="m2"><p>شاهد نابالغ و خط پریشان چون کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض بس کن گفتگو شعر تر مستانه گو</p></div>
<div class="m2"><p>شاعر صوفی سخن با خشک مغزان چون کند</p></div></div>