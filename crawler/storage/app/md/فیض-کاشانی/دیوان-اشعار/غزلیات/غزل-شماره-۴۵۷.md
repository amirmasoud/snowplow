---
title: >-
    غزل شمارهٔ ۴۵۷
---
# غزل شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>تجلی حسنه من معدن النور</p></div>
<div class="m2"><p>فدّک القلب منی دکه الطور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرزت صاعقا ثم استفقت</p></div>
<div class="m2"><p>رأیت الموت و الاحیا بلاصور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخرب فی هواه دار جسمی</p></div>
<div class="m2"><p>و لکن بیت قلبی فیه معمور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و من ینظر الی آیات وجهه</p></div>
<div class="m2"><p>یجده مصحفّافی الحسن مسطور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حوالی خده شعرات خضر</p></div>
<div class="m2"><p>کان المسک ممزوج بکافور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و ما الخضراء شعرا حول فیه</p></div>
<div class="m2"><p>فراهم آمدهٔ گرد شکر مور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهنگ عشق دل را لقمهٔ کرد</p></div>
<div class="m2"><p>چو افتادم در این در یاری پر شور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دموعی بحر و الهجران نیران</p></div>
<div class="m2"><p>من بیچاره غرق بحر مسجور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازینسان شعرها میگوئی ای فیض</p></div>
<div class="m2"><p>نویسد تا ملک بر رق منشور</p></div></div>