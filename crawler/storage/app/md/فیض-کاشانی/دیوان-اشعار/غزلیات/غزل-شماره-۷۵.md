---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>بکجا روم زدستت بچه سان رهم زشستت </p></div>
<div class="m2"><p>همه جا رسیده شستت همه را گرفته دستت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکشی بشست خویشم بکشی بدست خویشم </p></div>
<div class="m2"><p>بکش و بکش که جانم بفدای دست و شستت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمن فقیر مسکین چو گذر کنی بیفکن </p></div>
<div class="m2"><p>نظری چنانکه دانی بزکوهٔ چشم مستت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنوازی ار گدائی به تفقد و عطائی </p></div>
<div class="m2"><p>نکنی ازین زبانی نرسد از آن شکستت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگهی بناز میکن در فتنه باز میکن </p></div>
<div class="m2"><p>بره نظاره بس دل بامید فتنه هستت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنیم خراب و گوئی زچه اینچین شدستی </p></div>
<div class="m2"><p>زنگاه نیم مستت زدو چشم می پرستت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسخن حیات بخشی بنگاه جان ستانی </p></div>
<div class="m2"><p>بکن آنچه خواهدت دل چه زنیکوئی گستت </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند آرزو کسی کو سر همتش بلندست </p></div>
<div class="m2"><p>که نهد سری بپایت که شود چه خاک پستت </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدرت شکسته آیم تو نپرسیم که چونی </p></div>
<div class="m2"><p>برهت فتاده نالم تو نگوئیم چه استت </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه شود گر التفاتی بکنی بجانب فیض </p></div>
<div class="m2"><p>سر لطف اگر نداری ره قهر را که بستت </p></div></div>