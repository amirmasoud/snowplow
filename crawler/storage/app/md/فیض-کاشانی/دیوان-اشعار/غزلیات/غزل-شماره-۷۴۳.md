---
title: >-
    غزل شمارهٔ ۷۴۳
---
# غزل شمارهٔ ۷۴۳

<div class="b" id="bn1"><div class="m1"><p>در جهان افکندهٔ غوغای حسن</p></div>
<div class="m2"><p>عاشقانرا کردهٔ شیدای حسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن روی تست دریای محیط</p></div>
<div class="m2"><p>ماه رویان شبم دریای حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک استغنا مسلم مر تراست</p></div>
<div class="m2"><p>جان استغناست استغنای حسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرش بر خاک مذلت رو نهد</p></div>
<div class="m2"><p>پیش آن کرسی که باشد جای حسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هوا سرگشته دلها ذره‌سان</p></div>
<div class="m2"><p>پیش خورشید جهان آرای حسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه صبر پردلان را بر کند</p></div>
<div class="m2"><p>گردش چشم خوش شهلای حسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان اهل دل ز تن بیرون کشد</p></div>
<div class="m2"><p>قوت بازوی استیلای حسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون عشاق ار بریزد گو بریز</p></div>
<div class="m2"><p>لشکر سلطان بی‌پروای حسن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتش افروزی و عاشق سوزیست</p></div>
<div class="m2"><p>مقتضای خوی مادرزای حسن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق خوبان در دلت جا دادهٔ</p></div>
<div class="m2"><p>زان خیالت فیضفیض شد ماوای حسن</p></div></div>