---
title: >-
    غزل شمارهٔ ۶۲۱
---
# غزل شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>چه میشود که مقیم در جناب تو باشم</p></div>
<div class="m2"><p>سگ جناب تو باشم رقیب باب تو باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه میشود که شب و روز گرد کوی تو گردم</p></div>
<div class="m2"><p>در انتظار بر افکندن نقاب تو باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه میشود که گهی از در عتاب در آئی</p></div>
<div class="m2"><p>که از قصور نه شایسته خطاب تو باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه می‌شود که بتلقین حجتم بنوازی</p></div>
<div class="m2"><p>که چون سوال کنی واقف جواب تو باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه می‌شود که ببزم وصال خود دهیم جا</p></div>
<div class="m2"><p>جزای کرده چه شایسته ثواب تو باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه میشود که بهجران خویش نگذاریم</p></div>
<div class="m2"><p>سزای کرده چه مستوجب عقاب تو باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه میشود که نجوئی ز من حساب و کتابی</p></div>
<div class="m2"><p>غریق بحر کرمهای بیحساب تو باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه میشود چو مرا فیض دادهٔ لقب از لطف</p></div>
<div class="m2"><p>مدام سرخوش فیض شراب ناب تو باشم</p></div></div>