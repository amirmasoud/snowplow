---
title: >-
    غزل شمارهٔ ۷۷۹
---
# غزل شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>ای فتنها انگیخته آخر چه آشوبست این</p></div>
<div class="m2"><p>ای خون عالم ریخته آخر چه آشوبست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زلف شور انگیخته بر ماه عنبر بیخته</p></div>
<div class="m2"><p>دلها در او آویخته آخر چه آشوبست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشم سحر انگیخته مژگان بزهر آمیخته</p></div>
<div class="m2"><p>خون خلایق ریخته آخر چه آشوبست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لعل شکر ریخته جان در شکر آمیخته</p></div>
<div class="m2"><p>شور از جهان انگیخته آخر چه آشوبست این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لطف قهر انگیخته با قهر لطف آمیخته</p></div>
<div class="m2"><p>وین هر دو درهم ریخته آخر چه آشوبست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عشق شور انگیخته با جان فیض آمیخته</p></div>
<div class="m2"><p>زو این جواهر ریخته آخر چه آشوبست این</p></div></div>