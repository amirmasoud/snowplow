---
title: >-
    غزل شمارهٔ ۸۵۵
---
# غزل شمارهٔ ۸۵۵

<div class="b" id="bn1"><div class="m1"><p>از دست شد ز شوقت دستی بر این دلم نه</p></div>
<div class="m2"><p>بر باد رفت خاکم پائی بر این گلم نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محصول عمر خود را در کار خویش کردم</p></div>
<div class="m2"><p>یک پرتو از جمالت در کار و حاصلم نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پیچ و تاب زلفت بس تیره روز گارم</p></div>
<div class="m2"><p>گرد سرت از آن روی شمع مقابلم نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فیض یکه آهی شد قابل نگاهی</p></div>
<div class="m2"><p>منت بیک نگاهی بر جان قابلم نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان چابکان که دایم مستغرق وصالند</p></div>
<div class="m2"><p>برق عنایتی خوش بر جان کاهلم نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد را به نیک بخشند چون نیکوان مرا نیز</p></div>
<div class="m2"><p>از خاک تیره بر گیر در صدر منزلم نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قومی شکوه دارند صبری چه کوه دارند</p></div>
<div class="m2"><p>یکذره صبر از ایشان بستان و در دلم نه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گم گشت در رهش دل شد کار فیض مشکل</p></div>
<div class="m2"><p>بوی صبا ز زلفش در راه مشکلم نه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این شد جواب آن نظم از گفتهای ملا</p></div>
<div class="m2"><p>ای پاک از آب وا ز گل پای در اینگلم نه</p></div></div>