---
title: >-
    غزل شمارهٔ ۸۵۱
---
# غزل شمارهٔ ۸۵۱

<div class="b" id="bn1"><div class="m1"><p>در کشور حسن آن یگانه</p></div>
<div class="m2"><p>شد ساخته صدهزار خانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این طرفه که نیست هیچ دیار</p></div>
<div class="m2"><p>در هیچ سرا جز آن یگانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیار خود است و دار هم خود</p></div>
<div class="m2"><p>کردیم سراغ خانه خانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک نکته بگویمت از این راز</p></div>
<div class="m2"><p>در حسن ز عشق بود دانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنبید درو چو دانهٔ عشق</p></div>
<div class="m2"><p>برخاست حجاب از میانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرواز نمود طایر حسن</p></div>
<div class="m2"><p>بیرون آمد ز آشیانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آئینه عشق پیش بنهاد</p></div>
<div class="m2"><p>افکند دو زلف و کرد شانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از عکس رخش در آینهٔ عشق</p></div>
<div class="m2"><p>شد کشور حسن بیکرانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرمن خرمن بدید شد عشق</p></div>
<div class="m2"><p>از دانهٔ عشق آن یگانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس خرمن حسن گشت پیدا</p></div>
<div class="m2"><p>چون جلوهٔ او فکند دانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس قلزم عشق شد هویدا</p></div>
<div class="m2"><p>زان جنبش عشق جاودانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زد جوش چو بحر عشق برخاست</p></div>
<div class="m2"><p>طوفان طوفان زهر کرانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قلزم قلزم بدید گردید</p></div>
<div class="m2"><p>از جوشش بحر بیکرانه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاکستر عقل داد بر باد</p></div>
<div class="m2"><p>چون آتش عشق زد زبانه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صد دل بربود یک نگاهش</p></div>
<div class="m2"><p>یک تیر آمد بصد نشانه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر جا در فقر بود در بست</p></div>
<div class="m2"><p>بگشاد چو جود را خزانه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با اینهمه نیست غیر او کس</p></div>
<div class="m2"><p>زد مطرب عشق این ترانه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر تختهٔ گون نرد عشقی</p></div>
<div class="m2"><p>با زد با خویش جاودانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خود عاشق حسن خویش و معشوق</p></div>
<div class="m2"><p>این ما و شما همه بهانه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای فیض ازین حدیث بگذر</p></div>
<div class="m2"><p>ترسم بجنون شوی فسانه</p></div></div>