---
title: >-
    غزل شمارهٔ ۴۶۲
---
# غزل شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>دامن از دوستان کشیدی باز</p></div>
<div class="m2"><p>مهر از عاشقان بریدی باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانکه پیوند با تو محکم کرد</p></div>
<div class="m2"><p>بی سبب مهر بگسلیدی باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می ندانم دگر چه بد کردم</p></div>
<div class="m2"><p>می نگوئی ز تن چه دیدی باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسته کردی دلم بجور و جفا</p></div>
<div class="m2"><p>وز سر رحم ننگر یدی باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حق دوستان مخلص خود</p></div>
<div class="m2"><p>سخن دشمنان شنیدی باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نهم از غم تو سر در کوه</p></div>
<div class="m2"><p>جامهٔ صبر من دریدی باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته بودی وفا کنم با فیض</p></div>
<div class="m2"><p>گفتی و مصلحت ندیدی باز</p></div></div>