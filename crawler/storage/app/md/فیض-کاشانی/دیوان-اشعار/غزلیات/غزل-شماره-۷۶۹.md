---
title: >-
    غزل شمارهٔ ۷۶۹
---
# غزل شمارهٔ ۷۶۹

<div class="b" id="bn1"><div class="m1"><p>ای عمر من ایجان من ای جان و ای جانان من</p></div>
<div class="m2"><p>ای مرهم و درمان من ایجان و ای جانان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم شادی از تو غم ز تو زخم از تو و مرهم ز تو</p></div>
<div class="m2"><p>جان بلاکش هم ز تو ای جان و ای جانان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در دلم کردی وطن جان نوم آمد بتن</p></div>
<div class="m2"><p>هم نو فدایت هم کهن ای جان و ای جانان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاهی ز وصل افروزیم گاهی ز هجران سوزیم</p></div>
<div class="m2"><p>گاهی دری گه دوزیم ای جان و ای جانان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت به تیرم میزند زلفت اسیرم میکند</p></div>
<div class="m2"><p>هجرت ز بیخم میکند ای جان و ای جانان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با من جفا تا کی کنی ترک وفا تا کی کن</p></div>
<div class="m2"><p>این کارها تا کی کنی ای جان و ای جانان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکبار هم مهر و وفا یکچند هم ترک جفا</p></div>
<div class="m2"><p>گو کرده باشی یک خطا ای جان و ای جانان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکدم تو هم ای جان من شکر بیفشان ز آن دهن</p></div>
<div class="m2"><p>تا فیض بگذارد سخن ای جان و ای جانان من</p></div></div>