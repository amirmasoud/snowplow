---
title: >-
    غزل شمارهٔ ۴۸۲
---
# غزل شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>در عشق دیدم غوغای آتش</p></div>
<div class="m2"><p>زین پس ندادم پروای آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو‌ آشنا شو با عشق آن کو</p></div>
<div class="m2"><p>خواهد به بیند دریای آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آتش عشق هر کس که سوزد</p></div>
<div class="m2"><p>کی باشد او را پروای آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوزخ ندارد بر عاشقان پای</p></div>
<div class="m2"><p>کاین دست عشق است بالای آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عالم عشق من هر دو دیدم</p></div>
<div class="m2"><p>دریای آتش صحرای آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر سرم من بهر تماشا</p></div>
<div class="m2"><p>بشنو در آنجا هیهای آتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا هر که آید جز دوست سوزد</p></div>
<div class="m2"><p>شد این دل فیض مأوای آتش</p></div></div>