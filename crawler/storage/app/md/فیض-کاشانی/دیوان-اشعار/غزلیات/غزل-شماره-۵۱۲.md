---
title: >-
    غزل شمارهٔ ۵۱۲
---
# غزل شمارهٔ ۵۱۲

<div class="b" id="bn1"><div class="m1"><p>یار بما نکرد صبر و شکیب را وداع</p></div>
<div class="m2"><p>ناله ما اثر نکرد صبر و شکیب را وداع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار نظر نمیکند ناله اثر نمیکند</p></div>
<div class="m2"><p>غصه سفر نمیکند صبر و شکیب را وداع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار زما کرانه کرد شرم و حیا بهانه کرد</p></div>
<div class="m2"><p>صبر مرا روانه کرد صبر و شکیب را وداع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار بعشق اشاره کرد عشق بناله چاره کرد</p></div>
<div class="m2"><p>جامهٔ صبر پاره کرد صبر و شکیب را وداع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش عشق درگرفت ناطقه رخت بر گرفت</p></div>
<div class="m2"><p>عقل ره سفر گرفت صبر و شکیب را وداع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش عشق تیز شد جان بره گریز شد</p></div>
<div class="m2"><p>باقی صبر نیز شد صبر و شکیب را وداع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق شکیب میبرد جامهٔ صبر می‌درد</p></div>
<div class="m2"><p>کس غم ما نمیخورد صبر و شکیب را وداع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض ز عشق مست شد مست می الست شد</p></div>
<div class="m2"><p>دین و دلش ز دست شد صبر و شکیب را وداع</p></div></div>