---
title: >-
    غزل شمارهٔ ۲۴
---
# غزل شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>آفتاب وصل جانان بر نمی آید مرا </p></div>
<div class="m2"><p>وین شب تاریک هجران سرنمی آید مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل همیخواهد که جان در پایش افشانم ولی </p></div>
<div class="m2"><p>یکنفس آن بیوفا بر سر نمی اید مرا </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالع شوریده بین کان مایهٔ شوریدگی </p></div>
<div class="m2"><p>بی خبر یکبار از در در نمی اید مرا </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازطرب شیرینترست آن نوش لب لیکن حسود</p></div>
<div class="m2"><p>قامت چون نخل او در بر نمی آید مرا </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت بدبین کز پیامی خاطر ما خوش نکرد </p></div>
<div class="m2"><p>آرزوئی از نکویان بر نمی آید مرا </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زرد شد برک نهال عیش در دل سالهاست </p></div>
<div class="m2"><p>لاله رخساری بچشم تر نمی آید مرا </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من زرندی و نظر بازی نخواهم توبه کرد </p></div>
<div class="m2"><p>هیچ کاری فیض ازین خوشتر نمی آید مرا </p></div></div>