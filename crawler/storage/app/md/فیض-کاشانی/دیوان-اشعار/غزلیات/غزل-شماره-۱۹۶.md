---
title: >-
    غزل شمارهٔ ۱۹۶
---
# غزل شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>دلی که عشق ندارد وجود اوست عبث </p></div>
<div class="m2"><p>چو پرتوی ندهد شمع دور اوست عبث </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجود خلق برای پرستش حقست</p></div>
<div class="m2"><p>کسی که حق نپرستد وجود اوست عبث </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که سود و زیانش نه در ره عشق است</p></div>
<div class="m2"><p>زیان اوست بسی سهل و سود اوست عبث </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عبادتت نکند سود معرفت چون نیست </p></div>
<div class="m2"><p>چو جامه را نبود تار و پود اوست عبث </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گمان مبر زسراب جهان شوی سیراب </p></div>
<div class="m2"><p>که هر چه بود ندارد نمود اوست عبث </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا عبادت حق کن زباطلان بگریز </p></div>
<div class="m2"><p>که مهر باطل باطل درود اوست عبث </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرنه فیض برای خدا سخن گوید </p></div>
<div class="m2"><p>سخن سرائی او چون وجود اوست عبث </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خموش باش محیط جهان پر از سخن است </p></div>
<div class="m2"><p>ببحر هر که گهر ریخت جود اوست عبث </p></div></div>