---
title: >-
    غزل شمارهٔ ۵۹
---
# غزل شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>زان دو چشمم مدام مست و خراب </p></div>
<div class="m2"><p>میکشم لحظه لحظه جام شراب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میشوی از فورغ حسن آتش </p></div>
<div class="m2"><p>میشوم از نگاه حسرت آب </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزهٔ شوخ چشم فتّانت </p></div>
<div class="m2"><p>میبرباید دل از اولوا الالباب </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوشمندان زنرگس مستت </p></div>
<div class="m2"><p>بیخود افتاده اند مست و خراب </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قامتی خواهد آمدم در بر </p></div>
<div class="m2"><p>دوش دیدم قیامتی در خواب </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون دل تا بکی بدیده برم</p></div>
<div class="m2"><p>چون کنم در جگر ندارم آب </p></div></div>
<div class="b" id="bn7"><div class="m1"><p> در وصلت چو بستهٔ بر فیض </p></div>
<div class="m2"><p>افتح یا مفتح الابواب </p></div></div>