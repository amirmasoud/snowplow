---
title: >-
    غزل شمارهٔ ۲۳۷
---
# غزل شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>در سرم عشق تو غوفا دارد</p></div>
<div class="m2"><p>عشق تو قصد سر ما دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌خودم کرد نگاه مستت</p></div>
<div class="m2"><p>چشم تو نشاه صهبا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکند عارض تو عرض خطی</p></div>
<div class="m2"><p>با دل ما سر سودا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانهٔ خال تو بهر صیدم</p></div>
<div class="m2"><p>دامی از زلف چلیپا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمره سرو قدان را پیشت</p></div>
<div class="m2"><p>قد شمشاد تو بر پا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش روی تو قمر را چه محل</p></div>
<div class="m2"><p>کی قمر لعل شکر خا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشک خال و خطت از خور چه عجب</p></div>
<div class="m2"><p>رخ خورشید کی اینها دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه عجب گر بردم مجنون رشک</p></div>
<div class="m2"><p>این صفا کی رخ لیلا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه عجب گر دل من روز ندید</p></div>
<div class="m2"><p>زلف تو صد شب یلدا دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیر مژگان تو گر هر لحظه</p></div>
<div class="m2"><p>جا کند در دل من جا دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می نداند که چه با ما کردی</p></div>
<div class="m2"><p>زاهد از ما گله بیجا دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمکیدست لب شیرینت</p></div>
<div class="m2"><p>تلخ گوئی که غم ما دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الحذر ای که سر دین داری</p></div>
<div class="m2"><p>غمزه‌اش روی بیغما دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وصف آن یار مکن دیگر فیض</p></div>
<div class="m2"><p>زاهد ما سر تقوی دارد</p></div></div>