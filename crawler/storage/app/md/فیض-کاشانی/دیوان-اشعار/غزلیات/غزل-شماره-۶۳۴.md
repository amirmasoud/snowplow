---
title: >-
    غزل شمارهٔ ۶۳۴
---
# غزل شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>شب تار است روز من بیا خورشید تابانم</p></div>
<div class="m2"><p>روان سوز است سوز من بیا ای راحت جانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا ای یار دیرینم بیا ای جان شیرینم</p></div>
<div class="m2"><p>دمی بنشین ببالینم که جان بر پایت افشانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا خواهم ترا خواهم بغیر از تو کرا خواهم</p></div>
<div class="m2"><p>بغیر از تو چرا خواهم توئی جانم توئی جانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شادی چون شوم خندان توئی پیدا در آن خنده</p></div>
<div class="m2"><p>ز غم چون میکنم افغان توئی پنهان در افغانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنی در من گهی آتش کنی گاهی دلم را خوش</p></div>
<div class="m2"><p>کدامین بهتر است از لطف یا قهرت نمیدانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من پرسی که مرد دنییی ای فیض یا عقبی</p></div>
<div class="m2"><p>نه مرد این نه مرد آن پریشانم پریشانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گروهی عالم و عاقل گروهی غافل و جاهل</p></div>
<div class="m2"><p>من دیوانهٔ بیدل نه با اینم نه با آنم</p></div></div>