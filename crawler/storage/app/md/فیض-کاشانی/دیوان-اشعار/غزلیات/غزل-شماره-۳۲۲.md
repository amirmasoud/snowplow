---
title: >-
    غزل شمارهٔ ۳۲۲
---
# غزل شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>بوی رحمان از یمن آمد دل و جان تازه شد</p></div>
<div class="m2"><p>دل چه و جان چه جهان از بوی رحمان تازه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شراب کهنه چون بر سر دوید از لطف آن</p></div>
<div class="m2"><p>هم دماغ و هم دل و هم عقل و هم جان تازه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفخهٔ بگذشت زان بو بر زمین و آسمان</p></div>
<div class="m2"><p>هم زمین و هم زمان هم چرخ گردان تازه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان نسیمی در چمن شد سرو از رفتار ماند</p></div>
<div class="m2"><p>گل تجلی کرد و بانگ عندلیبان تازه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفخهٔ زان رفت تا عقبی قیامت زان طپید</p></div>
<div class="m2"><p>عالمی از نو بنا شد جان بجانان تازه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفخهٔ زان در نعیمستان جنت اوفتاد</p></div>
<div class="m2"><p>هم بهشت و هم حور و غلمان تازه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نقاب زلف از روی چو مه یکسو فکند</p></div>
<div class="m2"><p>ظلمت کفر از میان برخواست ایمان تازه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض در طور حقیقت شعرهای تازه گفت</p></div>
<div class="m2"><p>شاعرانرا هم ز نظمش طرز دیوان تازه شد</p></div></div>