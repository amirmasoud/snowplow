---
title: >-
    غزل شمارهٔ ۸۵۶
---
# غزل شمارهٔ ۸۵۶

<div class="b" id="bn1"><div class="m1"><p>کی پسندی تو جفا بر من مسکین کی کی</p></div>
<div class="m2"><p>تو و اندیشه ین کار خدا را هی هی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معدن مهر و وفا ز آنکه ازو جور و جفا</p></div>
<div class="m2"><p>حاش‌لله کی آید ز تو اینها کی کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردیم وعدهٔ وصلت ببهار اندازی</p></div>
<div class="m2"><p>باز چون فصل بهار آید گوئی دی دی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخم بر من زنی و دست من آلوده کنی</p></div>
<div class="m2"><p>تا چه گویند که زد زخم بگوئی وی وی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که با ناله و زاری دل من خو کرده است</p></div>
<div class="m2"><p>چون شوم خاک نروید ز گل من جز نی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می انگور نخواهم که بود تلخ و پلید</p></div>
<div class="m2"><p>لب شیرین تو خواهم بمکم پی در پی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جرعهٔ از لب لعلت اگرم دست دهد</p></div>
<div class="m2"><p>تا ابد موی بمویم همه گوید می می</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بخاکم گذری رقص کنان برخیزم</p></div>
<div class="m2"><p>وز سر شوق زنم نعرهٔ یاحی یاحی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هی و هوئی بکن ای فیض بود کز طرفی</p></div>
<div class="m2"><p>ناگهان بر سرت آید که رسیدم هی هی</p></div></div>