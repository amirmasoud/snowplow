---
title: >-
    غزل شمارهٔ ۳۰۷
---
# غزل شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>دل مرا ز اندیشه اسباب دنیا سرد شد</p></div>
<div class="m2"><p>آخرت با یادم آمد آرزوها سرد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شدم آگه ز اسرار علوم آخرت</p></div>
<div class="m2"><p>بر دلم دنیا و ما فیها سرا پا سرد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گهم دل گرم گردید از تماشای جهان</p></div>
<div class="m2"><p>یادم آمد آخرت دل از تماشا سرد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدن گلزار و صحرا طبع را چون بر فروخت</p></div>
<div class="m2"><p>مردنم یاد آمد آن گلزار و صحرا سرد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست دنیا جای آرام آنکه را هوشی بود</p></div>
<div class="m2"><p>بر دلش در زندگی لذات دنیا سرد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دل ارباب عقبا لذت دنیاست سرد</p></div>
<div class="m2"><p>نزد اهل معرفت لذات عقبا سرد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که دید ارباب دنیا را کلاب دوزخند</p></div>
<div class="m2"><p>سروری را ماند و از مردار دنیا سرد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش مهر زر و زیور چو در دلها گرفت</p></div>
<div class="m2"><p>زمهریر مرگ آمد حوش دلها سرد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر تو کندی دل ز دنیا ورنه او خود میکند</p></div>
<div class="m2"><p>زین سبب اهل خرد را دل ز دنیا سرد شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکسی را وقت مردن دل شود سرد از هوا</p></div>
<div class="m2"><p>فیض را در زندگی دل از هواها سرد شد</p></div></div>