---
title: >-
    غزل شمارهٔ ۵۴۳
---
# غزل شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>ارانی اراک و لست اراک</p></div>
<div class="m2"><p>ارانی سواک و لست سواک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارانی اراک و انت بمرای</p></div>
<div class="m2"><p>ارانی و انت سوی ما اراک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارانی و لست اری غیر وجهک</p></div>
<div class="m2"><p>ارانی اری ما سواک سواک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هواک اراک ولست بمرای</p></div>
<div class="m2"><p>ارانی و انت سوی ما رآک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواک سواک اراک و انی</p></div>
<div class="m2"><p>فلست اری فی سواک سواک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اری ما سواک طلالا و فینا</p></div>
<div class="m2"><p>فما هو سواک و ما انت ذاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اراک اراک سواک سوائی</p></div>
<div class="m2"><p>و لست سوائی و لست سواک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ارانی و انی کسانا لباسا</p></div>
<div class="m2"><p>ارانی سواک و لست بذاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوای ارانی سواک و انی</p></div>
<div class="m2"><p>فلست اری فی وجودی سواک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و انی و انی فدآء لانک</p></div>
<div class="m2"><p>و ما نیتی دون انی فداک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لقاک هوای و حق اللقاء</p></div>
<div class="m2"><p>هوای فیض افناؤه فی لقاک</p></div></div>