---
title: >-
    غزل شمارهٔ ۱۳۰
---
# غزل شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>جنونی در سرم ماوا گرفتست</p></div>
<div class="m2"><p>سرم را سر بسر سودا گرفتست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خردگر این بود کاین عاقلانراست </p></div>
<div class="m2"><p>خوش آنسرکش جنون مأوا گرفتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد چشم مجنون کس و گرنه</p></div>
<div class="m2"><p>دو عالم را رخ لیلا گرفتست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بچشم خلق چون طفلان نمایند </p></div>
<div class="m2"><p>که باز یشان زسرتا پا گرفتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسلمانان ره عقبی کدامست </p></div>
<div class="m2"><p>دلم از وحشت دنیا گرفتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپای جان زغفلت هست بندی </p></div>
<div class="m2"><p>و گرنه ره سوی عقبا گرفتست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشو ای فیض بابیگانه همراز </p></div>
<div class="m2"><p>چو وابینی ترا ازما گرفتست </p></div></div>