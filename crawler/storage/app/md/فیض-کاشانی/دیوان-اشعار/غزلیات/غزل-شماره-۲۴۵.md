---
title: >-
    غزل شمارهٔ ۲۴۵
---
# غزل شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>با هیچکس این کش مکش آن یار ندارد</p></div>
<div class="m2"><p>جز با دل سر گشتهٔ ما کار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دوش من افکند فلک بار امانت</p></div>
<div class="m2"><p>زان چرخ زنان است که این بار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیمارم و بیماریم از دست طبیب است</p></div>
<div class="m2"><p>دردا که طبیبم سر بیمار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند که رنج تو ز دیدار شود به</p></div>
<div class="m2"><p>این چشم ترم طاقت دیدار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمخواری یار است علاج دل بیمار</p></div>
<div class="m2"><p>آن یار و لیکن دل غمخوار ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهلست اگر مهر تو آرایش جان کرد</p></div>
<div class="m2"><p>بگذر ز دلم این همه آزار ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد کندم سرزنش عشق که عار است</p></div>
<div class="m2"><p>عار است که از زهد کسی عار ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زهد گذر کن گرت اندیشه خار است</p></div>
<div class="m2"><p>کاین گلشن قدسی گل بی‌خار ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غمخوار بود چارهٔ آن دل که غمینست</p></div>
<div class="m2"><p>بیچاره دل فیض که غمخوار ندارد</p></div></div>