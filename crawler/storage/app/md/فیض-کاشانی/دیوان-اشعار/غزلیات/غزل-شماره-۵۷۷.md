---
title: >-
    غزل شمارهٔ ۵۷۷
---
# غزل شمارهٔ ۵۷۷

<div class="b" id="bn1"><div class="m1"><p>از بوی می عشق برنگ آمده‌ام</p></div>
<div class="m2"><p>باز شه عشق را بچنگ آمده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی باشد عاشقی دچارم گردد</p></div>
<div class="m2"><p>از صحبت عاقالان بتنگ آمده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد خسته بخار زهد اول قدمم</p></div>
<div class="m2"><p>ره را همگی بپای لنگ آمده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقصد بنگر ز سختی راه مپرس</p></div>
<div class="m2"><p>در هر قدمی پای بسنگ آمده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمرم به شتاب رفت هنگام شباب</p></div>
<div class="m2"><p>پیرانه سر این ره به درنگ آمده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صورت اگر بعاقلان می مانم</p></div>
<div class="m2"><p>در معنی لیک شوخ و شنگ آمده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سینهٔ دوستان سردوم چونفیض</p></div>
<div class="m2"><p>در دیدهٔ دشمنان خدنگ آمده‌ام</p></div></div>