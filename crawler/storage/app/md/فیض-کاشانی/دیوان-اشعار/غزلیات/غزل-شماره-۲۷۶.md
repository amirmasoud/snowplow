---
title: >-
    غزل شمارهٔ ۲۷۶
---
# غزل شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>گر یار بما رخ ننماید چه توان کرد</p></div>
<div class="m2"><p>ز آنروی نقاب ار نگشاید چه توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنهان ز نظرها اگر آید بتماشا</p></div>
<div class="m2"><p>در دیده دل از ما بزداید چه توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن حسن و جمالی که نگنجد بعبارت</p></div>
<div class="m2"><p>این دیده مرآنرا چو نشاید چه توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دیدهٔ عشاق چه خورشید عیانست</p></div>
<div class="m2"><p>گر در نظر غیر نیاید چه توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون روی نماید دل و دین را برباید</p></div>
<div class="m2"><p>یک لحظه ولیکن چه نیاید چه توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آید بر این خسته دمی چون بعیادت</p></div>
<div class="m2"><p>عمرم اگر آندم بسر آید چه توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای فیض گرت یار نخواهد چه توان گفت</p></div>
<div class="m2"><p>ور خواهد و رخ می‌ننماید چه توان کرد</p></div></div>