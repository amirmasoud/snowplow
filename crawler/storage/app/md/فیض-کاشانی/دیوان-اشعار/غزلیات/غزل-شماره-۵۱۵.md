---
title: >-
    غزل شمارهٔ ۵۱۵
---
# غزل شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>عشق بر اکوان محیطست و وسیع</p></div>
<div class="m2"><p>عشق در عالم مطاع است و مطیع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق در دلها حیاتست و روان</p></div>
<div class="m2"><p>عشق در سرها سماع است و سمیع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق در مردان حق آئینه است</p></div>
<div class="m2"><p>مینماید پرتو حسن منیع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق در سالک رهست و راهبر</p></div>
<div class="m2"><p>میرساند تا بدرگاه رفیع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق در املاک واله بودنست</p></div>
<div class="m2"><p>عشق در افلاک جولان سریع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق آتش سوختن افروختن</p></div>
<div class="m2"><p>عشق در انجم نظرهای بدیع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق در کوی زمین افتادگی است</p></div>
<div class="m2"><p>عشق در انهار جریان سریع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق در بحرست امواج غریب</p></div>
<div class="m2"><p>عشق در بّر است دامان وسیع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق در کوهست تمکین و ثبات</p></div>
<div class="m2"><p>عشق در باد هوا سیر سریع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق در مرغان خوش الحان نعم</p></div>
<div class="m2"><p>عشق در گلهاست الوان بدیع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق در اطفال لهوست و لعب</p></div>
<div class="m2"><p>در زنان ازواج را بودن مطیع</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق در نادان ز دانایان سوال</p></div>
<div class="m2"><p>عشق دانا دانش و خلق وسیع</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق دلهای تهی از عشق حق</p></div>
<div class="m2"><p>پر شدن از مهر رخسار بدیع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق در شاعر معانی بستن است</p></div>
<div class="m2"><p>عشق در فیض است احصای جمیع</p></div></div>