---
title: >-
    غزل شمارهٔ ۹۰
---
# غزل شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>کعبهٔ وصل تو پناه منست </p></div>
<div class="m2"><p>طاق ابروت قبله گاه منست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم فتان مست خونریزت </p></div>
<div class="m2"><p>خود زبیداد خود پناه منست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود ره لشگر غمت دادم </p></div>
<div class="m2"><p>غارت خان و مان گناه منست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگاهی اگر خراب شدم </p></div>
<div class="m2"><p>چشم مست تو عذرخواه منست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد دلم خون زروی گلگونت </p></div>
<div class="m2"><p>اشک خونین من گواه من است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی و راه دگر نمیدانم </p></div>
<div class="m2"><p>لطف و قهر روی و راه منست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض روز تو هم تیره از آنست </p></div>
<div class="m2"><p>بخت من هم سیه ز آه منست </p></div></div>