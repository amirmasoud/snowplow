---
title: >-
    غزل شمارهٔ ۲۴۶
---
# غزل شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>غمی هست در دل که گفتن ندارد</p></div>
<div class="m2"><p>شنفتن ندارد نهفتن ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گفتن ندارد غم دل چگویم</p></div>
<div class="m2"><p>چگویم غم دل که گفتن ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهفتن ندارد غم دل چه پوشم</p></div>
<div class="m2"><p>چه پوشم غم دل نهفتن ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنفتن ندارد غم دل چه پرسی</p></div>
<div class="m2"><p>چه پرسی غم دل شنفتن ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم چون غبار از تو دارد چه روبم</p></div>
<div class="m2"><p>چه روبم غباری که رفتن ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکفتن ندارد دلی کز تو گیرد</p></div>
<div class="m2"><p>دلی کز تو گیرد شکفتن ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه خوابی بچشمم نیاید چه خسبم</p></div>
<div class="m2"><p>چه خسبم که این دیده خفتن ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز درد نهان لب فروبند ای فیض</p></div>
<div class="m2"><p>فرو بند لب را که گفتن ندارد</p></div></div>