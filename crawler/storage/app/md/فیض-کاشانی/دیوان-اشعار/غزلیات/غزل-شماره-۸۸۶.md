---
title: >-
    غزل شمارهٔ ۸۸۶
---
# غزل شمارهٔ ۸۸۶

<div class="b" id="bn1"><div class="m1"><p>حبیبی انت ذو من وجودی</p></div>
<div class="m2"><p>فلا تبخل علینا بالرفودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>؟؟ ما را وعدهای وصل دادی</p></div>
<div class="m2"><p>فئی یا مونسی تلک الوعودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب یلدای هجران کشت ما را</p></div>
<div class="m2"><p>الا ایام وصل الحب عودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه صبر از خدمت تو میتوان کرد</p></div>
<div class="m2"><p>و لا فی الخدمهٔ امکان الورودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و فی قلبی جوی من حب حب</p></div>
<div class="m2"><p>کنار اضرمت ذات الوقودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر آبی میزنی بر آتش ما</p></div>
<div class="m2"><p>تلطف لا الی حد الحمودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهشت عدن خواهی عاشقی کن</p></div>
<div class="m2"><p>فان العشق جنات الخلودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عهود عشق را مگذار ای فیض</p></div>
<div class="m2"><p>نه حق فرمود اوفوا بالعقودی</p></div></div>