---
title: >-
    غزل شمارهٔ ۴۸۵
---
# غزل شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>سحر رسید ز غیبم بکوش هوش سروش</p></div>
<div class="m2"><p>که خیز و از لب ما بادهٔ طهور بنوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن سروش شدم مست و بیخود افتادم</p></div>
<div class="m2"><p>شراب تا چه کند چون سروش برد از هوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذاشتم تن و با پای جان روانه شدم</p></div>
<div class="m2"><p>روان روان شد و تن تن زد از سماع سروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بقدسیان چو رسیدم مرا گرفت از من</p></div>
<div class="m2"><p>صلای ساقی ارواح و بانگ نوشانوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندا رسید دگر بار کای قتیل فراق</p></div>
<div class="m2"><p>بیا و از لب ما شربت حیات بنوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پای تا سر من مو بمو دهانی شد</p></div>
<div class="m2"><p>چشید ذوق حیاتی از آن خجسته سروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا گرفت ز من خود بجای من بنشست</p></div>
<div class="m2"><p>فؤاد من شد و چشم من و مرا شد گوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهاد بر سر من زان حیات سرپوشی</p></div>
<div class="m2"><p>که مرگ دست ندارد بزیر آن سرپوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیاهٔ غیب رسید و سر مماهٔ رسید</p></div>
<div class="m2"><p>چنان برید که ننشست دیک فیض از جوش</p></div></div>