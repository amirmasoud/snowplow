---
title: >-
    غزل شمارهٔ ۴۱۸
---
# غزل شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>زهر فراق نوشم تا کام من برآید</p></div>
<div class="m2"><p>بهر وصال کوشم تا جان ز تن برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر جفا نهادم تا میتوان جفا کن</p></div>
<div class="m2"><p>از جان کشم جفایت تا کام من برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخم وفات در جان کشتم که چون بمیرم</p></div>
<div class="m2"><p>شام وفا پس از مرگ از خاک من برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بی‌وفاست معشوق کان وفاست عاشق</p></div>
<div class="m2"><p>عاشق وفا کند تا از خویشتن برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقف تو کرده‌ام من جان و دل و سر و تن</p></div>
<div class="m2"><p>در خدمتم سراپا تا جان ز تن برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کو سفر گزیند تا مقصدی بیابد</p></div>
<div class="m2"><p>باید غریب گردد ز اهل و وطن برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون در سفر تو باشی صد جان فدای غربت</p></div>
<div class="m2"><p>گر ره‌زنی تو مقصود از راهزن برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حضرتت برد فیض پیوسته ظن نیکو</p></div>
<div class="m2"><p>انجاح هر مهمی از حسن و ظن برآید</p></div></div>