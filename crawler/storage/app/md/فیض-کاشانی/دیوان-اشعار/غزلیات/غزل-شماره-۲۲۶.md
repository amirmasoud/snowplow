---
title: >-
    غزل شمارهٔ ۲۲۶
---
# غزل شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>کشد هر جنس جنس خود سخن گرد سخن گردد</p></div>
<div class="m2"><p>دل از خود چون بتنک آید بگرد آن دهن گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گردم تشنهٔ معنی دلم ز آن لب سخن گوید</p></div>
<div class="m2"><p>چو آب زندگی جویم در آن خط و ذقن گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می و مستی اگر خواهم ز چشمانش دهد ساغر</p></div>
<div class="m2"><p>ز حال دل خبر گیرم در آن زلف و شکن گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که از ضد دل بضد آید که ضد گردد بضد پیدا</p></div>
<div class="m2"><p>ز قد راستش پرسم بدور قد من گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر در انجمن باشم کشد دل جانب خلوت</p></div>
<div class="m2"><p>چو در خلوت نشینم دل بگرد انجمن گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روم سوی چمن گر من ز آهم میشود صحرا</p></div>
<div class="m2"><p>بصحرا گر روم صحرا ز اشک من چمن گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنانم از پریشانی که گر خواهم بلب آرم</p></div>
<div class="m2"><p>زبان از حرف جمعیت پریشان در دهن گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم گم کرده چیزی را نمیداند چه چیز است آن</p></div>
<div class="m2"><p>اگر بوئی برد از خود بگرد خویشتن گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلی کو در جهان گل نباشد وصل را قابل</p></div>
<div class="m2"><p>بیاد صاحب منزل بر اطلال و دمن گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حجابش ما و من باشد چو بشناسد من و ما را</p></div>
<div class="m2"><p>شناسد گر من و ما را بگرد ما و من گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود حب وطن ز ایمان وطن جان را بود جانان</p></div>
<div class="m2"><p>وطن را گر شناسد جان بقربان وطن گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز یاران فیض میخواهد جوابی چون غزل گوید</p></div>
<div class="m2"><p>دهن گرد سخن گردد سخن گرد دهن گردد</p></div></div>