---
title: >-
    غزل شمارهٔ ۸۰۶
---
# غزل شمارهٔ ۸۰۶

<div class="b" id="bn1"><div class="m1"><p>من نزد توام حاضر هر جای چه جوئی تو</p></div>
<div class="m2"><p>واندر همه جا هستم بیهوده چه پوئی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده نمی پویم ای دوست قرارم نیست</p></div>
<div class="m2"><p>یکجا بچه سان باشم چون در همه سوئی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که شدم دیدم نقشی ز جمال تو</p></div>
<div class="m2"><p>چون نیک نظر کردم گفتم مگر اوئی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتا همه اویم من ایرا همه رویم من</p></div>
<div class="m2"><p>آری تو نداری پشت آری همه روئی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور تو جهان بگرفت عالم همه روشن شد</p></div>
<div class="m2"><p>ای آب حیات جان یا رب ز چه جوئی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه آب و چه جو چه جان بگذار تو اسما را</p></div>
<div class="m2"><p>اسما همه روپوش است خود پرده اوئی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سو کشدت میرو هر جا بردت میدو</p></div>
<div class="m2"><p>اندر خم چوگانش ای فیض چه گوئی تو</p></div></div>