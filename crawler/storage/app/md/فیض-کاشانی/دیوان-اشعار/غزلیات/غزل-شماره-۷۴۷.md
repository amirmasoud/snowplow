---
title: >-
    غزل شمارهٔ ۷۴۷
---
# غزل شمارهٔ ۷۴۷

<div class="b" id="bn1"><div class="m1"><p>خدایا مرا از من آزاد کن</p></div>
<div class="m2"><p>ضمیرم بعشق خود آباد کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرم را بیاد خودت زنده کن</p></div>
<div class="m2"><p>روان مرا منبع یاد کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بروی خودت باز کن دیده‌ام</p></div>
<div class="m2"><p>دلم را بنظاره‌ات شاد کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرابم کن از مستی و بیخودی</p></div>
<div class="m2"><p>وجودم بویرانی آباد کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفردوس اعلام راهی نما</p></div>
<div class="m2"><p>بعلم لدنیم ارشاد کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درونم باسرار معمور دار</p></div>
<div class="m2"><p>برونم بطاعات آباد کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شیطان و نفسم پناهی بده</p></div>
<div class="m2"><p>ز جور اعادیم آزاد کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس اندوه و غم بر سر هم نشست</p></div>
<div class="m2"><p>گشادی بده سینه را شاد کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود فیض دربند خود تا بکی</p></div>
<div class="m2"><p>خدایا دلی از من آزاد کن</p></div></div>