---
title: >-
    غزل شمارهٔ ۴۴۱
---
# غزل شمارهٔ ۴۴۱

<div class="b" id="bn1"><div class="m1"><p>ای بهار جان و ای جان بهار</p></div>
<div class="m2"><p>ز ابر رحمت جان ما را تازه دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاب قهری بر هوای دل بزن</p></div>
<div class="m2"><p>آب لطفی بر زمین دل ببار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای توفیق از سر ما وا مگیر</p></div>
<div class="m2"><p>دست تائید از دل ما بر ندار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریشه جان را از آن کن آبکش</p></div>
<div class="m2"><p>میوهٔ دلرا ازین کن آبدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبتلای محنت هجرم مکن</p></div>
<div class="m2"><p>بر سر من هرچه میخواهی بیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه میخواهی بکن آن توام</p></div>
<div class="m2"><p>لیکن از خود یکنفس دورم مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای ز تو سر سبز باغ عاشقان</p></div>
<div class="m2"><p>سایه خود از سر ما بر مدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست غفران چون برون آری ز جیب</p></div>
<div class="m2"><p>این سر شوریده ما را بخار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ره بسوی خود نمودی فیض را</p></div>
<div class="m2"><p>از کرم دارش درین ره استوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ازل لطفی عنایت کردهٔ</p></div>
<div class="m2"><p>تا ابد این رحمت پاینده دار</p></div></div>