---
title: >-
    غزل شمارهٔ ۹۰۱
---
# غزل شمارهٔ ۹۰۱

<div class="b" id="bn1"><div class="m1"><p>مثل حسنت بجهان نور ندیده است کسی</p></div>
<div class="m2"><p>همچه عشقت غم پر زور ندیده است کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتوت تافته بر عالم و نورت پنهان</p></div>
<div class="m2"><p>شاهد ظاهر و مستور ندیده است کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو جهان شیفته دارد رخ ننمودهٔ تو</p></div>
<div class="m2"><p>حسن در پرده و مشهور ندیده است کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخت دوریم ز تو با همه نزدیکی‌ها</p></div>
<div class="m2"><p>بار نزدیک چنین دور ندیده است کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو جهان مست و خرابست ز یک جام الست</p></div>
<div class="m2"><p>این چنین بادهٔ پر زور ندیده است کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز دل اهل خرابات که جولانگه تواست</p></div>
<div class="m2"><p>در جهان خانهٔ معمور ندیده است کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نصرت و یاریت آنست که بردار کشی</p></div>
<div class="m2"><p>همچه منصور تو منصور ندیده است کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میخورم زهر غمت را بحلاوت دلشاد</p></div>
<div class="m2"><p>ماتمی را که بود سود ندیده است کسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا مرده دلی زنده جاوید شود</p></div>
<div class="m2"><p>چون صدای سخنت صور ندیده است کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون غزلهای دل افروز و جهانسوز تو فیض</p></div>
<div class="m2"><p>سخنیرا که دهد نور ندیده است کسی</p></div></div>