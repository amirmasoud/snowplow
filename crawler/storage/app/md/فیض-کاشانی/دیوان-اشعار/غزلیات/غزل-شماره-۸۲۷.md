---
title: >-
    غزل شمارهٔ ۸۲۷
---
# غزل شمارهٔ ۸۲۷

<div class="b" id="bn1"><div class="m1"><p>من آشفته را در راه یاری کار افتاده</p></div>
<div class="m2"><p>که در راهش چو من بی با و سر بسیار افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر آمد عمر بیحاصل نشد پیموده یک منزل</p></div>
<div class="m2"><p>میان راه هم خر مرده و هم بار افتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شده بودم همه نابود و گم گشته ره مقصود</p></div>
<div class="m2"><p>سرم گردیده سودائی قدم از کار افتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد طی راه و پایم ماند از رفتار و ره گم شد</p></div>
<div class="m2"><p>دلم شد خسته جان افکار و تن بیمار افتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر خضر رهی گردد دوچار من درین وادی</p></div>
<div class="m2"><p>که در تاریکی حیرت رهم دشوار افتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبستم طرفی از علم و عمل تا بود آلاتم</p></div>
<div class="m2"><p>سر آمد عمر شد آلات کار از کار افتاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنهای جلی گفتم شنیدم نیک فهمیدم</p></div>
<div class="m2"><p>کنونم کار با فهمیدن اسرار افتاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل نورانی باید که اسرار سخن فهمد</p></div>
<div class="m2"><p>بر آئینهٔ دل من سربسر زنگار افتاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیابد شست و شو الا بآب چشم و سوز جان</p></div>
<div class="m2"><p>دلم را که با زاری و استغفار افتاده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندارم آب و تاب و زاری و برگ فغان کردن</p></div>
<div class="m2"><p>زبان و دیده هم چون من بحال زار افتاده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببخشا بارالها بر من بی‌دست و پا اکنون</p></div>
<div class="m2"><p>که دست و پایم از کردار و از رفتار افتاده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ببخشا بر تن و جانم در آنساعت که درمانم</p></div>
<div class="m2"><p>دل از جان کنده و با کندن جان کار افتاده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان باقیم پیش نظر افراخته قامت</p></div>
<div class="m2"><p>جهان فانیم از دیده خونبار افتاده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه وقت عذر خواهی و نه عذر رو سیاهی را</p></div>
<div class="m2"><p>سراپا غرق عصیان کار با غفار افتاده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطی از خامه غفران بکش بر نامهٔ عصیان</p></div>
<div class="m2"><p>که کار فیض با کردار خود دشوار افتاده</p></div></div>