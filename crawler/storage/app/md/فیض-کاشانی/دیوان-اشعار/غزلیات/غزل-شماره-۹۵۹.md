---
title: >-
    غزل شمارهٔ ۹۵۹
---
# غزل شمارهٔ ۹۵۹

<div class="b" id="bn1"><div class="m1"><p>هر آن دلرا که با یاریست خوئی</p></div>
<div class="m2"><p>ز گلذار حقیقت هست بوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد او سر دنیا و عقبی</p></div>
<div class="m2"><p>که دارد پای آمد شد بکوئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی کوشد اسیر زلف یاری</p></div>
<div class="m2"><p>دو عالم را نمی‌گیرد بموئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود خاطر پریشان هر که او را</p></div>
<div class="m2"><p>رسید از زلف عنبر بوی بوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کوشد ز راه عشق آگاه</p></div>
<div class="m2"><p>نمیخواهد دگر راهی بسوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سری کو مست عشقی شد ز خود رست</p></div>
<div class="m2"><p>بود آن می ز دریا یا بسوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل فیض از غم عشقی زند های</p></div>
<div class="m2"><p>مگر روزی به پیوندد بهوئی</p></div></div>