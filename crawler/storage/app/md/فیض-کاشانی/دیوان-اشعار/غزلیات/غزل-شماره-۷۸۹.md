---
title: >-
    غزل شمارهٔ ۷۸۹
---
# غزل شمارهٔ ۷۸۹

<div class="b" id="bn1"><div class="m1"><p>ای که دانی سرّ ما را مو بمو</p></div>
<div class="m2"><p>شمهٔ احوال ما با ما بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیستیم و از چه و بهر چه‌ایم</p></div>
<div class="m2"><p>کیست نحن کیست کنت کیست هو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحرهای راز پنهان کردهٔ</p></div>
<div class="m2"><p>درطلب افکنده ما را جو بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه میگوئیم پنهان ما بما</p></div>
<div class="m2"><p>بیش میدانیش پیدا مو بمو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آگهی ز احوال تنها تا بتا</p></div>
<div class="m2"><p>واقفی ز اسرار جانها تو بتو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماهیان بحر تو جانهای ما</p></div>
<div class="m2"><p>بحر جویان جابجا و جوبجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما شده جویای تو از هر طرف</p></div>
<div class="m2"><p>تو نشسته در برابر روبرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی تو دایم بسوی ما و ما</p></div>
<div class="m2"><p>در طلب حیران و جویان سو بسو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با دل ما در تکلم روز و شب</p></div>
<div class="m2"><p>در سراغت میدود دل کو بکو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در همه جا هستی و جائی نهٔ</p></div>
<div class="m2"><p>سر برآریم از تو و گوئیم کو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عطر بوی تو گرفته عالمی</p></div>
<div class="m2"><p>بیخود آن گشته ما نشنیده بو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غمزهای مست پنهان میرسد</p></div>
<div class="m2"><p>سوی جان ز آن چشم جادو موبمو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان ما افتان و خیزان می‌دود</p></div>
<div class="m2"><p>دست و پا گم کرده بهر جستجو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از حضورت دل اگر آگه شدی</p></div>
<div class="m2"><p>خویش را از خویش کردی رفت و رو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با دل من در عتابی دم بدم</p></div>
<div class="m2"><p>عذر ما را لیک دانی مو بمو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عذر تقصیرات ما در کار تو</p></div>
<div class="m2"><p>توبه از ما دانی ای نعم العفو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرچه از ما پردهٔ خود میدریم</p></div>
<div class="m2"><p>میکند خیاط عفو تو رفو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دم بدم آلودهٔ عصیان شویم</p></div>
<div class="m2"><p>ابتلای تو کندمان شست و شو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p> فیض جان ده در رهش تسلیم شو</p></div>
<div class="m2"><p>لن تنالوا البر حتی تنفقو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت و گو بسیار شد خامش شویم</p></div>
<div class="m2"><p>تا کند دلدار با ما گفتگو</p></div></div>