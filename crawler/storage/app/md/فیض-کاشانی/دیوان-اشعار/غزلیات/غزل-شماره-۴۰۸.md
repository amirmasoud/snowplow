---
title: >-
    غزل شمارهٔ ۴۰۸
---
# غزل شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>مژدهٔ از هاتف غیبم رسید</p></div>
<div class="m2"><p>قفل جهانرا غم ما شد کلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوی ز میدان سعادت ربود</p></div>
<div class="m2"><p>هر که غم ما بدل و جان خرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاف می عشق ننوشد مگر</p></div>
<div class="m2"><p>آنکه ز هستیش تواند برید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه ازین باده بنوشد زند</p></div>
<div class="m2"><p>تا با بد نعرهٔ هل من مزید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیر نگردد بسبو یا بخم</p></div>
<div class="m2"><p>کار وی از جام بدریا کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چه کند در دل و در جان مرد</p></div>
<div class="m2"><p>نشاه این باده چو در سر دوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی از آن نشاه تجلی کند</p></div>
<div class="m2"><p>عاشق بیچاره شود نابدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حد و نهایت نبود عشق را</p></div>
<div class="m2"><p>کی برسد وصف شه بی‌نذید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوش که تا صاحب معنی شوی</p></div>
<div class="m2"><p>فیض نسازد بتو گفت و شنید</p></div></div>