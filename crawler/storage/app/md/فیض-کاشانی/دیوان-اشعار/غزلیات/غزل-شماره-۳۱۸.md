---
title: >-
    غزل شمارهٔ ۳۱۸
---
# غزل شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>چو مهر دوست بر دل تافت این ویرانه روشن شد</p></div>
<div class="m2"><p>سراسر مشعلی شد دل تمام خانه روشن شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون روز من از دل دل از مهرش روشنی دارد</p></div>
<div class="m2"><p>ز نور شبچراغ عشق این کاشانه روشن شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی پروانهٔ جانم بگرد شمع او گردید</p></div>
<div class="m2"><p>ز عشق شمع آتش خو دل پروانه روشن شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجامم ریخت ساقی در سحر گه تا شدم بیدار</p></div>
<div class="m2"><p>شرابی کز صفای آن دل دیوانه روشن شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشیدم جام گردید از فروغ می روانم صاف</p></div>
<div class="m2"><p>صفا بیرون تراوید از رخم میخانه روشن شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذشتم بر در بتخانه دلهای سیه دیدم</p></div>
<div class="m2"><p>ز توحید آیتی خواندم بت و بتخانه روشن شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث فیض دلهای سپهرا میکند روشن</p></div>
<div class="m2"><p>دل زهاد را دیدم کزین افسانه روشن شد</p></div></div>