---
title: >-
    غزل شمارهٔ ۹۳
---
# غزل شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>شوریدگان عشق را ای مطرب آهنگ فناست </p></div>
<div class="m2"><p>برگ نوا را ساز کن ساز ره مستان نواست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخ طرب در چنک ما اندوه و غم دلتنگ ما </p></div>
<div class="m2"><p>لذات دنیا ننگ ما ما را ببزم دوست جاست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهدزجنت دم زندسلطان زتاج وتخت وملک </p></div>
<div class="m2"><p>مارانه این زیبدنه آن فوق دوعالم جای ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جا درزمین گوتنگ باش ماراکه درعرش است دل </p></div>
<div class="m2"><p> در زیر سرگوسنگ باش ما را چو برافلاک پاست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیگانه‌ای ای مشتری ما را تو ارزان میخری </p></div>
<div class="m2"><p>کی میشناسد جنس ما الا کسی کو آشناست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر شناسد مشتری کی داندش هر گوهری </p></div>
<div class="m2"><p>آنرا که باشد معرفت داند که این درّ پر بهاست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پروردهٔ عشقیم ما دادیم در دل عیشها</p></div>
<div class="m2"><p>ما را زمعشوق ازل در جان و دل پیغامهاست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گو غیرما باجنگ باش از عاشقی درننگ باش </p></div>
<div class="m2"><p>آن یار با ما آشتی زین عشق ما را فخرهاست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر درد در عالم بود این فیض میدارد دوا </p></div>
<div class="m2"><p>هم درد من از عشق خواست هم عشق دردم را دواست </p></div></div>