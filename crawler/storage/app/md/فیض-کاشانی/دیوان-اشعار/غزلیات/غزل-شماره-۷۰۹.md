---
title: >-
    غزل شمارهٔ ۷۰۹
---
# غزل شمارهٔ ۷۰۹

<div class="b" id="bn1"><div class="m1"><p>در حریم قدس جانرا نیست بار از ننگ تن</p></div>
<div class="m2"><p>ننگ تن یا رب بیفکن از روان پاک من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخیه تا کی بر تن بر حیف خود خواهیم زد</p></div>
<div class="m2"><p>کی بود کز دوش جان افتاده باشد بار تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی غلط کردم که بی تن جان نمییابد کمال</p></div>
<div class="m2"><p>چند روزی بهر جان باید کشیدن رنج تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نبودی تن چسان جان علم و فصل اندوختی</p></div>
<div class="m2"><p>گر نبودی تن چسان جان در جنان کردی وطن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آلتی جانرا بود ناچار در کسب و کمال</p></div>
<div class="m2"><p>آلت کسب کمال جانست سر تا پای تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرکب جانست تن در راه صعب آخرت</p></div>
<div class="m2"><p>در سفر ناچار باشد پاس مرکب داشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه جان آسوده بود از جور تن پیش از سفر</p></div>
<div class="m2"><p>لیک در وی بود پنهان عجب ولاف ما و من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاکساری دید و عجز و محنت و رنج و شکیب</p></div>
<div class="m2"><p>شد تمام و پخته و دانا و بینا ممتحن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز در جای قدیم خویش می گیرد قرار</p></div>
<div class="m2"><p>رسته از آزار تن خالص زلاف ما و من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میشود قربان جان ما تن ما عاقبت</p></div>
<div class="m2"><p> فیض چندی صبر کن بر رنج تن ای جان من</p></div></div>