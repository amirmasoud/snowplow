---
title: >-
    غزل شمارهٔ ۶۹۳
---
# غزل شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>از دم صبح ازل با عشق یار و همدمیم</p></div>
<div class="m2"><p>هر دو با هم زاده‌ایم از دهر با هم توامیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو از پستان فطرت شیر با هم خورده‌ایم</p></div>
<div class="m2"><p>یک صدف پرورده ما را هر دو دّر یک یمیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میدرخشد نور عرفان از سواد داغ دل</p></div>
<div class="m2"><p>چشم ما این داغ و ما چشم و چراغ عالمیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان ما را اتحادی هست با سلطان عشق</p></div>
<div class="m2"><p>نیستیم ازهم جدا هرگز همیشه با همیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حریم دوست ما را نیز چون او بار هست</p></div>
<div class="m2"><p>هر کجا عشقست محرم ما هم آنجا محرمیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میرساند عشق ما را تا جناب کبریا</p></div>
<div class="m2"><p>گرچه جسم ما ز خاک و ما ز نسل آدمیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز اول گر ملک از سایهٔ ما میرمید</p></div>
<div class="m2"><p>ما کنون از نارسیهای ملایک درهمیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خم قتلست ما را گر فلک از کجروی</p></div>
<div class="m2"><p>پا نهادن بر سرش را راست ما هم در خمیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ازل شیر غم از پستان مادر خورده‌ام</p></div>
<div class="m2"><p>ما چه غم داریم از غم دست پرورد غمیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کهنه غربال فلک گر بر سرما ریخت غم</p></div>
<div class="m2"><p>بر سر خاک غم اکنون یکدو دم در ماتمیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست ما را مختلف احوال در سیر و سلوک</p></div>
<div class="m2"><p>که زهر بیشیم بیش و گاهی از هر کم کمیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه بر فوق سمواتیم و گه بر روی خاک</p></div>
<div class="m2"><p>گاه دریای محیطیم و گهی دیگر نمیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاشقانرا نطق و خاموشی بدست خویش نیست</p></div>
<div class="m2"><p>ما چو نی در ناله و فریاد در بند دمیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بنطق آئیم پیش از وقت چون روح اللهیم</p></div>
<div class="m2"><p>ور خمش باشیم هنگام سخن چون مریمیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون گشاد سینه را پیوند با غم کرده‌اند</p></div>
<div class="m2"><p>ما بهم پیوسته با غم چون دو حرف مدغمیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>راز دلرا بر زبان ای فیض آوردن خطاست</p></div>
<div class="m2"><p>گوشها گویند پنهان ما کی اینرا محرمیم</p></div></div>