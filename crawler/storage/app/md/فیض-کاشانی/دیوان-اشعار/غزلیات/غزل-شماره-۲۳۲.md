---
title: >-
    غزل شمارهٔ ۲۳۲
---
# غزل شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>بر دل و جان رواست درد در سروتن چراست درد</p></div>
<div class="m2"><p>تا که رسد ز تن بجان تا نپرد تمام مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میرسد از بدن بجان میکشد این بسوی آن</p></div>
<div class="m2"><p>گر بتنست و گر بجان هر چه بود سزاست درد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مغز ز پوست میکشد هر دو بدوست میکشد</p></div>
<div class="m2"><p>مرد چو گرم درد شد شد دلش از دو کون سرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد دواست مرد را مرد دواست درد را</p></div>
<div class="m2"><p>رد بود آنکه نبودش بیگه و گاه رنج و درد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد بود غذای روح مایهٔ شادی و فتوح</p></div>
<div class="m2"><p>هر که بدرد گشت جفت شد ز غم زمانه فرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علت و سقم آب و گل هست شفای جان و دل</p></div>
<div class="m2"><p>سرخی روی جان بود روی تنت چو گشت زرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد تن و سوار جان این شده پردهٔ بر آن</p></div>
<div class="m2"><p>در طلب سوار تاز یاوه مگرد گرد گرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد چو در تو نیست هیچ بیهده در سخن مپیچ</p></div>
<div class="m2"><p>گرم سخن شدی تو فیض هست سخن ولیک سرد</p></div></div>