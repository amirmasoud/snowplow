---
title: >-
    غزل شمارهٔ ۵۴۱
---
# غزل شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>درد دل مرا نکند به دوای خلق</p></div>
<div class="m2"><p>بیماری خدای بهست از شفای خلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنج از خداست راحت و راحت ز خلق رنج</p></div>
<div class="m2"><p>قربان یک بلای خدا صد عطای خلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحرا و کوه خوشترم آید ز شهر و ده</p></div>
<div class="m2"><p>صد ره صدای کوه بهست از ندای خلق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر یک ترا بدام بلای دگر کشد</p></div>
<div class="m2"><p>ای چشم بسته روی مکن در قفای خلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند خلق راه حق ایسنت زینهار</p></div>
<div class="m2"><p>مشنو مرد بسوی جهنم بپای خلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکن حذر ز پیروی دیو سیرتان</p></div>
<div class="m2"><p>زنهار سیلی نخوری ز ابتلای خلق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار گرانشان بدل و جان به و برو</p></div>
<div class="m2"><p>میکش برای حق دو سه روزی بلای خلق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آزار خلق روی دلت سوی حق کند</p></div>
<div class="m2"><p>راهیست سوی معرفت حق جفای خلق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانی تو فیض آنکه نیاید ز خلق هیچ</p></div>
<div class="m2"><p>بگذر ز گفتگوی ملالت فزای خلق</p></div></div>