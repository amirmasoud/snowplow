---
title: >-
    غزل شمارهٔ ۳۸۰
---
# غزل شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>زحمت مکش طبیب که این تب نمیرود</p></div>
<div class="m2"><p>بی شربت بنفشهٔ آن لب نمیرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بی ز مهر در دلم آنمه فکنده است</p></div>
<div class="m2"><p>تا تاب او ز دل نرود تب نمیرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیمار عشق به نشود جز به وصل دوست</p></div>
<div class="m2"><p>این درد دل بناله یا رب نمیرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند در فراق برم انتظار وصل</p></div>
<div class="m2"><p>آن روز خود نیامد و این شب نمیرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار فراق چند تواند کشید دل</p></div>
<div class="m2"><p>این جان سخت بین که ز قالب نمیرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی بیا و لب به لبم نه که این خمار</p></div>
<div class="m2"><p>از سر بغیر جام لبالب نمیرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق بتان وسیله عشق خداست لیک</p></div>
<div class="m2"><p>فیض از وسیله جانب مطلب نمیرود</p></div></div>