---
title: >-
    غزل شمارهٔ ۵۷۲
---
# غزل شمارهٔ ۵۷۲

<div class="b" id="bn1"><div class="m1"><p>بخوشی بگذریم از هر کام</p></div>
<div class="m2"><p>بر سر خود نهیم اول گام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رای باش برای آن حق رای</p></div>
<div class="m2"><p>کام باشد بکام آن خود کام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونکه رستی ز خود رسی در خود</p></div>
<div class="m2"><p>کام یابی چو بگذری از کام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشوی هست تا نگردی نیست</p></div>
<div class="m2"><p>نشوی مست تا تو بینی جام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در فکن خویش را در آتش عشق</p></div>
<div class="m2"><p>تا نسوزی تمام خامی خام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیخ غم را نمیکند جز عشق</p></div>
<div class="m2"><p>ظلمت شام کی برد جز نام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بند عشقت گشاید از هر بند</p></div>
<div class="m2"><p>دام عشقت رهاند از هر دام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق سازد ز سرّ کار آگه</p></div>
<div class="m2"><p>عشق آرد ترا ز حق پیغام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ معنی شکار کی شودت</p></div>
<div class="m2"><p>تا نگردی تمام چشم چه دام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون زنان تا برنک و بو گروی</p></div>
<div class="m2"><p>ننهی در حریم مردان گام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بچشی جرعهٔ ز بادهٔ عشق</p></div>
<div class="m2"><p>تا نگردی چو جام خون آشام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خویشتن را بحق سپار ای فیض</p></div>
<div class="m2"><p>جز بحق دل نگیردت آرام</p></div></div>