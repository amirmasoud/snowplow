---
title: >-
    غزل شمارهٔ ۷۱۰
---
# غزل شمارهٔ ۷۱۰

<div class="b" id="bn1"><div class="m1"><p>در دل هر ذره مهر جان ما دارد وطن</p></div>
<div class="m2"><p>میکشد بهر گل جان خارهای جور تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینجهان و آنجهان از جان گریبان چاک کرد</p></div>
<div class="m2"><p>تا دهد جا جان ما را در درون خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که قدر جان پاک ما شناسد چون ملک</p></div>
<div class="m2"><p>سجده آرد جان ما را ز آنکه شد جانرا وطن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک ما دارد شرف بر جان ابلیس لعین</p></div>
<div class="m2"><p>ز آنکه این تن داد حق را آن ز حق دزدید تن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور حق پنهان شد اندر خاک از چشم عدو</p></div>
<div class="m2"><p>نور آتش دید در خود گفت کی باشد چومن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اجر چندین ساله طاعت رفت از دستش برون</p></div>
<div class="m2"><p>چونکه پا بیرون نهاد از انقیاد ذوالمنن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حسد برد و تکبر کافر شد رجیم</p></div>
<div class="m2"><p>در پناه حق گریزای فیض زین دو همچو من </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعیها دارد بسی ابلیس در اهلاک ما</p></div>
<div class="m2"><p>تاتوانی سعی میکن در نجات خویشتن</p></div></div>