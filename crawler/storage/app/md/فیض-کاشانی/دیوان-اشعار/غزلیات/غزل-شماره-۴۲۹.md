---
title: >-
    غزل شمارهٔ ۴۲۹
---
# غزل شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>خویش را اول سزاوارش کنید</p></div>
<div class="m2"><p>آنگهی جان در سر کارش کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمزهٔ از چشم شوخش وا کشید</p></div>
<div class="m2"><p>فتنه در خوابست بیدارش کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ندارد از غم عاشق خبر</p></div>
<div class="m2"><p>ساغری از عشق در کارش کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش روی او نهید آئینهٔ</p></div>
<div class="m2"><p>در کمند خود گرفتارش کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بپرهیزد دل بیمار ازو</p></div>
<div class="m2"><p>شربتی زان چشم در کارش کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یابه بیماری جان تن در دهید</p></div>
<div class="m2"><p>یا حذر از چشم بیمارش کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار منعی گر زند دل خسی</p></div>
<div class="m2"><p>بادهٔ گلرنگ در کارش کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نسازد با جفای دوست دل</p></div>
<div class="m2"><p>با فراق او شبی یارش کنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بار عشق ار بر ندارد دوش فیض</p></div>
<div class="m2"><p>کارهای عاقلان بارش کنید</p></div></div>