---
title: >-
    غزل شمارهٔ ۳۵۰
---
# غزل شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>باده‌ای خواهم بدن مستی کند</p></div>
<div class="m2"><p>چون بجام آید بدن مستی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رسد بر لب نرفته در دهن</p></div>
<div class="m2"><p>مو بمویم جان و تن مستی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده‌ای خواهم که جان بیخود کند</p></div>
<div class="m2"><p>سهل باشد گر بدن مستی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده‌ای خواهم که از بوی خوشش</p></div>
<div class="m2"><p>عشق حق در جان من مستی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سرم بیرون کند ما و منی</p></div>
<div class="m2"><p>ما و من بی‌ما و من مستی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کفر و ایمان هر دو گردد مست از آن</p></div>
<div class="m2"><p>هم یقین هم شک و ظن مستی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده‌ای کان بیخ غم را بر کند</p></div>
<div class="m2"><p>حزن در بیت‌الحزن مستی کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلغل آن چون فتد در آسمان</p></div>
<div class="m2"><p>هم زمین و هم زمن مستی کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ملک نوشد فلک بیخود شود</p></div>
<div class="m2"><p>عرش و کرسی بی‌بدن مستی کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جرعهٔ بر خلق اگر قسمت کنند</p></div>
<div class="m2"><p>پیر و برنا مرد و زن مستی کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زاهد و عابد اگر نوشند از آن</p></div>
<div class="m2"><p>هر دو را سر و علن مستی کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در چمن گر نفخهٔ زان بگذرد</p></div>
<div class="m2"><p>بلبل و گل در چمن مستی کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بدریا قطرهٔ افتد از آن</p></div>
<div class="m2"><p>در صدف دُر عدن مستی کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر وزد بوئی از آن بر کوه قاف</p></div>
<div class="m2"><p>جان عنقا در بدن مستی کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جرعهٔ زان می اگر روزی شود</p></div>
<div class="m2"><p>فیض را بی‌ما و من مستی کند</p></div></div>