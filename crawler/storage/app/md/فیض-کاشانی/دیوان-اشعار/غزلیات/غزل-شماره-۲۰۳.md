---
title: >-
    غزل شمارهٔ ۲۰۳
---
# غزل شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>عشق بری پیکران می نپذیرد علاج </p></div>
<div class="m2"><p>شورش دیوانگان می نپذیرد علاج </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نظر افکنده دین و دلت رفته است </p></div>
<div class="m2"><p>دلبری دلبران می نپذیرد علاج </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصد دل وجان ما ، تا چه بایمان کنند </p></div>
<div class="m2"><p>فتنه این رهزنان می نپذیرد علاج </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برصف دلها زنند غارت جانها کنند </p></div>
<div class="m2"><p>این ستم شاهدان می نپذیرد علاج </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل خارا چه سان رخنه کند آب چشم </p></div>
<div class="m2"><p>این دل سنگین دلان می نپذیرد علاج </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوزش دل کم نکرد اشگ چو باران من </p></div>
<div class="m2"><p>آتش عشق بتان می نپذیرد علاج </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض ازین قصه بس ناله مکن چون جرس </p></div>
<div class="m2"><p>عشق بآه و فغان می نپذیرد علاج </p></div></div>