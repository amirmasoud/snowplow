---
title: >-
    غزل شمارهٔ ۵۲۴
---
# غزل شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>ای که با ما وعدها کردی خلاف</p></div>
<div class="m2"><p>از وفا و عهد و پیمانت ملاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعدهای تو دروغ اندر دروغ</p></div>
<div class="m2"><p>لافهای تو گزاف اندر اندر گزاف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند غم را سر بجان من دهی</p></div>
<div class="m2"><p>در دل من بهر غم سازی مصاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند غم در دور من گرد آوری</p></div>
<div class="m2"><p>تا بگردم روز و شب آرد طواف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند بافی بهر من از غم پلاس</p></div>
<div class="m2"><p>چند سازی بهر من از غم لحاف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاهم از شادی لباسی هم بدوز</p></div>
<div class="m2"><p>بستری از شادمانی هم بباف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان نخواهی برد از دست غمش</p></div>
<div class="m2"><p>فیض گفتم با تو حرف پاک و صاف</p></div></div>