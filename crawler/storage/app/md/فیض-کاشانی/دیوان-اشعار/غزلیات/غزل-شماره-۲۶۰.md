---
title: >-
    غزل شمارهٔ ۲۶۰
---
# غزل شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>جان گذر میکند آن به که بجانان گذرد</p></div>
<div class="m2"><p>قطره شد بیمدد آن به که بعّمان گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل چو غم میخورد آن به که غم دوست خورد</p></div>
<div class="m2"><p>عمر چون میگذرد به که بسامان گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بکی وقت بلاطایل و بیهوده رود</p></div>
<div class="m2"><p>تا بکی عمر بلایعنی و خسران گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند اوقات شود صرف جهان فانی</p></div>
<div class="m2"><p>نه در اندیشهٔ آغاز و نه پایان گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف از این عمر گرانمایه که هر لحظه از آن</p></div>
<div class="m2"><p>صرف طاعات توان کرد و بعصیان گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوش جان وصف حدیث تو کنم تا جانرا</p></div>
<div class="m2"><p>لحظه لحظه بنظر حوری و غلمان گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان و دل هر دو نثار تو کنم تا بر من</p></div>
<div class="m2"><p>متصل لشکر دل قافله جان گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل بعشق تو دهم تا رمقی در دل هست</p></div>
<div class="m2"><p>جان برای تو دهم تا بجهان جان گذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که در کشتی عشق آمد ازین قلزم دهر</p></div>
<div class="m2"><p>کی دگر در دلش اندیشهٔ طوفان گذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فیض دشوار شود کار چو گیری دشوار</p></div>
<div class="m2"><p>ور تو آسان شمری مشکلت آسان گذرد</p></div></div>