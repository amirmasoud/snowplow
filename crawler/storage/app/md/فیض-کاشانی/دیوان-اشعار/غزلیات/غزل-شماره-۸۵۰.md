---
title: >-
    غزل شمارهٔ ۸۵۰
---
# غزل شمارهٔ ۸۵۰

<div class="b" id="bn1"><div class="m1"><p>دل گیرد و جان بخشد آن دلبر جانانه</p></div>
<div class="m2"><p>ویران چو کند بخشد صد گنج بویرانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل شد ببر دلبر جان رفت ز تن یکسر</p></div>
<div class="m2"><p>وز عقل تهی شد سر کس نیست درین خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس زلف دهد بر باد آنزلف خم اندر خم</p></div>
<div class="m2"><p>بس عقل کند غارت آن نرگس مستانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سویم بنگر مستان هوش و خردم بستان</p></div>
<div class="m2"><p>دیوانه و مستم کن مستم کن و دیوانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه پند دهد واعظ گه توبه دهد زاهد</p></div>
<div class="m2"><p>یارب که مرا افکند در صحبت بیگانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم میکشدم مطرب بر تار بزن دستی</p></div>
<div class="m2"><p>دیوانه شدم ساقی در ده دو سه پیمانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن منبع آگاهی گفتا که چه میخواهی</p></div>
<div class="m2"><p>گفتم که چه میخواهم جانانه و پیمانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیمانه و جانانی جانانه و پیمانی</p></div>
<div class="m2"><p>این نشکندم پیمان آن از کف جانانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیمانه بکف کردم در مجمع بیهوشان</p></div>
<div class="m2"><p>گویند کئی گویم دیوانهٔ فرزانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیغ ار بصدف ناید دردانه بکف ناید</p></div>
<div class="m2"><p>بشکن صدف هستی ای طالب دردانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای در دل و جان من تا چند نهان از من</p></div>
<div class="m2"><p>نشنیده کسی هرگز خمخانهٔ بیگانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکبار دو چارم شو روزی دو سه یارم شو</p></div>
<div class="m2"><p>فیض از تو بود تا کی چون استن حنانه</p></div></div>