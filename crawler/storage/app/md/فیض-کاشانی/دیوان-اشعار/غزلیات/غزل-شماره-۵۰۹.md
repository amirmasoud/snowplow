---
title: >-
    غزل شمارهٔ ۵۰۹
---
# غزل شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>خورشید روئی گردید طالع</p></div>
<div class="m2"><p>دردم نهان شد چون برق لامع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ایستادی آتش فنادی</p></div>
<div class="m2"><p>هم در مدارس هم در صوامع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنرا که دیدش طالع قوی بود</p></div>
<div class="m2"><p>وانکو ندیدش از ضعف طالع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این ماه رویان کم رو نمایند</p></div>
<div class="m2"><p>آنماه چرخست کان هست طالع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس عزیزند از کس گریزند</p></div>
<div class="m2"><p>دیدارشانرا باشد موانع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر زمین را مه مه توان دید</p></div>
<div class="m2"><p>مهر فلک هست هر روز طالع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید رویان هرجا نباشند</p></div>
<div class="m2"><p>خورشید چرخست کان هست واسع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقی بده می بیگانهٔ نیست</p></div>
<div class="m2"><p>از خویش رفتم دیگر چه مانع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذار ای فیض اشعار باطل</p></div>
<div class="m2"><p>از حق سخن گو کان هست نافع</p></div></div>