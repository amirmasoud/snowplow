---
title: >-
    غزل شمارهٔ ۸۸
---
# غزل شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>زمستان خراباتیم پند است </p></div>
<div class="m2"><p>که هر کو عشق بازد هوشمند است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا آندل که در زلفی اسیر است </p></div>
<div class="m2"><p>بزنجیر جنون عشق بندست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرو ناریم سر جز بر در دوست </p></div>
<div class="m2"><p>فقیران را سرهمت بلند است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه عالم طلبکارند او را </p></div>
<div class="m2"><p>اگر مومن و گر زنار بندست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا زاسباب عیش اینجهانی </p></div>
<div class="m2"><p>دل پردرد عشق او پسند است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهم از کمند او رهائی </p></div>
<div class="m2"><p>که جانرا رشته عمر این کمند است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدامم چشم بر لطف نهانی است </p></div>
<div class="m2"><p>زعیش جاودان اینهم پسند است </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همین دانم که تاریکست روزم </p></div>
<div class="m2"><p>نمیدانم شمار عمر چند است </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مزن از عشق دم بی عشق ای فیض </p></div>
<div class="m2"><p>چو معنی نیست دعوی ناپسندست </p></div></div>