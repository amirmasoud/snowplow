---
title: >-
    غزل شمارهٔ ۶۳۸
---
# غزل شمارهٔ ۶۳۸

<div class="b" id="bn1"><div class="m1"><p>چنان شدم که قبیح از حسن نمی‌دانم</p></div>
<div class="m2"><p>مپرس مسئله از من که من نمی‌دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنون عشق سراپای من گرفت از من</p></div>
<div class="m2"><p>چنان که پای ز سر سر ز تن نمی‌دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر از خویش برون کرد و جای من بنشست</p></div>
<div class="m2"><p>کنون رهی بسوی خویشتن نمی‌دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب حسن از وصاف میکشم بیظرف</p></div>
<div class="m2"><p>صراحی و قدح و جام و دن نمی‌دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر کجا نگرم روی خوب او بینم</p></div>
<div class="m2"><p>خصوص گلشن و طرف چمن نمی‌دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو وصف او کنم از پای تا بسر سخنم</p></div>
<div class="m2"><p>زبان و لب نشناسم دهن نمی‌دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث او همه جا آشکار می‌گویم</p></div>
<div class="m2"><p>درون خلوت از انجمن نمی‌دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند چو معنی او جلوه میشوم معنی</p></div>
<div class="m2"><p>حروف را نشناسم سخن نمی‌دانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود تنم همه جان صورتش چه جلوه کند</p></div>
<div class="m2"><p>چه جان شدم همه تن جان ز تن نمی‌دانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو یاد او کنم از پای تا بسر شوم او</p></div>
<div class="m2"><p>چو او شدم همه من ما و من نمی‌دانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو من شدم همه او و شد او تمامی من</p></div>
<div class="m2"><p>روان ز قالب جان از بدن نمی‌دانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصال او همه جا چون میسرست مرا</p></div>
<div class="m2"><p>طلل نجویم و ربع و دمن نمی‌دانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا وطن چو شد آنجا که یار من آنجاست</p></div>
<div class="m2"><p>دگر دیار غریب از وطن نمی‌دانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببوی او همه کس را عزیز می‌دارم</p></div>
<div class="m2"><p>چو فیض خاک رهم ما و من نمی‌دانم</p></div></div>