---
title: >-
    غزل شمارهٔ ۱۱۶
---
# غزل شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>منوش ساغر دنیا که درد ناب نماست </p></div>
<div class="m2"><p>درونش خون دلست از برون شراب نماست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنچه در نظر آید ز زینت دنیا </p></div>
<div class="m2"><p>بنزد اهل بصیرت سراب آب نماست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببر مگیر عروس جهان که غدار است </p></div>
<div class="m2"><p>مرو بجامه خوابش که پیر شاب نماست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدوز کیسهٔ نفعش که نفع او ضرر است </p></div>
<div class="m2"><p>مخور فریب خطایش جهان صواب نماست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درونش تیره و تنگ از برون بود روشن </p></div>
<div class="m2"><p>ز ذره کمتر و در دیده آفتاب نماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برونش عیش و طرب وز درون غم و محنت</p></div>
<div class="m2"><p>درون فسرده و بیرونش آب و تاب نماست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمزه اش کند اندوه جلوهٔ راحت </p></div>
<div class="m2"><p>زعشوه اش دل ناکام کامیاب نماست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رین سرا دل اشکسته بیت معمور است </p></div>
<div class="m2"><p>اگرچه در نظر بی بصر خراب نماست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهانست خواب پریشان گهی شوی بیدار </p></div>
<div class="m2"><p>که زیر خاک نخسبی اگر چه خواب نماست </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظر بصورت دنیاست آنچه گفتی فیض </p></div>
<div class="m2"><p>بمعنی ارنگری سوی حق شتاب نماست </p></div></div>