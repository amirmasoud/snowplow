---
title: >-
    غزل شمارهٔ ۸۱۴
---
# غزل شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>میفزاید جان حدیث عاشقان بسیار گو</p></div>
<div class="m2"><p>بگذر از افسانه اغیار و حرف یار گو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرف وصل یار گلزار است و حرف هجر خار</p></div>
<div class="m2"><p>خار خار گفتنی گر داری از گلزار گو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن حدیثی کآورد درد طلب تکرار کن</p></div>
<div class="m2"><p>یکه حرفی کان دهد جانرا طرب صد بار گو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زمین و آسمان تا چند خواهی گفت حرف</p></div>
<div class="m2"><p>یکزمان بگذار ذکر یارو از دیار گو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر طهارت خواهی از غیر خدا بیزار شو</p></div>
<div class="m2"><p>ور تجارت خوشترت میآید از بازار گو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرف دی گربه بود ز امروز دم میزن زدی</p></div>
<div class="m2"><p>حرف پار ار به بود ز امسال حرف پار گو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر کران باش و گران گوش از دم بیگانگان</p></div>
<div class="m2"><p>چون حدیث یار آمد در میان بسیار گو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرف اهل عشق را مستانه گوئی باک نیست</p></div>
<div class="m2"><p>چون بحرف عاقلان گویا شوی هشیار گو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا تو هشیاری ز سر اهل عرفان دم مزن</p></div>
<div class="m2"><p>مست چون گردی ز اسرار آنکه از ستار گو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوئی از شیرین لبان حرفی شکر نامش بنه</p></div>
<div class="m2"><p>وز کرانان چون سخن گویند زهرمار گو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایکه مینازی به نظم و نثر رنگارنگ خویش</p></div>
<div class="m2"><p>چند از گفتار گوئی یکره از کردار گو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این سخنها را بیان بیش ازین در کار هست</p></div>
<div class="m2"><p>بعد از این ای فیض اگر گوئی سخن طومار گو</p></div></div>