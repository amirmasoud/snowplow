---
title: >-
    غزل شمارهٔ ۸۴
---
# غزل شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>آهنگ جانان کرد جان ایمطرب آهنگی بس است </p></div>
<div class="m2"><p>دیوانه شد دل زان پری دیوانه را رنگی بسست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما مست پیغام وئیم شیدای دشنام وئیم </p></div>
<div class="m2"><p>صلح از برای مدعی ما را از او جنگی بس است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی بیخودان بوی او دارند تاب روی او </p></div>
<div class="m2"><p>در دست ما آشفتگان از زلفش آونگی بس است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطرب نوا را ساز کن برگ و نوا آغاز کن </p></div>
<div class="m2"><p>گو جان و دل پرواز کن ما را بت سنگی بس است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگین دلا سنگین دلا با ما مکن جور و جفا </p></div>
<div class="m2"><p>ماخستگان نازک دلیم این شیشه راسنگی بس است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بیخودی آغاز کرد آهنگ رفتن ساز کرد </p></div>
<div class="m2"><p>یا آه درد آلوده یا نغمه چنگی بس است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عشق جانان سرخوشیم بگذار تا خواری کشیم </p></div>
<div class="m2"><p>تا می نمیخواهیم ما عشاق را ننگی بس است </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما در درون دل خوشیم گودر برون تنگی کشیم </p></div>
<div class="m2"><p>وسعت جه باشد سینه را جا کلبهٔ تنگی بس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس بود در کار خود فیض و خیال یار خود </p></div>
<div class="m2"><p>زهاد را بوئی بس و عباد را رنگی بس است </p></div></div>