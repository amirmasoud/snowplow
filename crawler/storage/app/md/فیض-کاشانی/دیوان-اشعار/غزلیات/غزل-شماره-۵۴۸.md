---
title: >-
    غزل شمارهٔ ۵۴۸
---
# غزل شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>در دلم تا جای کرد از لطف آن رشگ ملک</p></div>
<div class="m2"><p>غیر او تا ثبت کردم غیرت او کرد حک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت فارغ ساز بهر من فان القلب لی</p></div>
<div class="m2"><p>گفتمش از جان برم فرمان فان الامر لک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو بوصل تو نبردم چند گشتم کو بگو</p></div>
<div class="m2"><p>ای دل سرگشته خون شووزره چشمم بچک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشگ خونین از جگر میریز بر روی زمین</p></div>
<div class="m2"><p>آه آتشناک ار جان میرسان سوی فلک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جحیم نفس باشی چند با شیطان قرین</p></div>
<div class="m2"><p>در بهشت جان در آی و همنشین شو با ملک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو مردی با هوای نفس میکن کارزار</p></div>
<div class="m2"><p>ور نه مانند زنان چادر بسر بند و لچک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر از دنیای دون وسعی کن بهر جنان</p></div>
<div class="m2"><p>بهر حورالعین گذر کن زین عجوز مشترک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او بدور تو محیطست و توئی غافل ازو</p></div>
<div class="m2"><p>در میان آب و غافل ز آب میباشد سمک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب و تابی در سخن باید که تاثیری کند</p></div>
<div class="m2"><p>اشک و آهی بایدت ای فیض آوردن کمک</p></div></div>