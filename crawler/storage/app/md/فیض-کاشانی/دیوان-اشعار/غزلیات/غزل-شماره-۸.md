---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>زخود سری بدرآرم چه خوش بود بخدا </p></div>
<div class="m2"><p>زپوست مغز برآرم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکنده ام دل و جانرا بقلزم غم عشق </p></div>
<div class="m2"><p>اگر دری بکف آرم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنم زخویش تهی خویشرا ازخود برهم </p></div>
<div class="m2"><p>زغم دمار بر آرم چه خوش بود بخدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زدیم از رخ جان زنک نقش هر دو جهان </p></div>
<div class="m2"><p>که روبروی توآرم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنم زصورت هر چیز رو بمعنی آن </p></div>
<div class="m2"><p>عدد دگر نشمارم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنور عشق کنم روشن آینه رخ جان </p></div>
<div class="m2"><p>مقابل تو بدارم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زپای تا سرمن گر تمام دیده شود </p></div>
<div class="m2"><p>بحسن دوست گمارم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آن خیال کنم وقف دیده و دل جان </p></div>
<div class="m2"><p>بجز تو یاد نیارم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درون خانهٔ دل روبم از غبار سوی </p></div>
<div class="m2"><p>بجز تو کس نگذارم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود که رحم کنی بر دل شکستهٔ من</p></div>
<div class="m2"><p>بسوز سینه بزارم چه خوش بود به خدا </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهم چین مذلّت بخاک درگه دوست</p></div>
<div class="m2"><p>زدیده اشک ببارم چه خوش بود بخدا </p></div></div>
<div class="b" id="bn12"><div class="m1"><p> برای سوختن فیض آتش غم عشق </p></div>
<div class="m2"><p>زجان خویش برآرم چه خوش بود بخدا </p></div></div>