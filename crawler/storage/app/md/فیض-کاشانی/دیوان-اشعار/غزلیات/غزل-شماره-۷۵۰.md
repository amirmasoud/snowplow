---
title: >-
    غزل شمارهٔ ۷۵۰
---
# غزل شمارهٔ ۷۵۰

<div class="b" id="bn1"><div class="m1"><p>الهی ز عصیان مرا پاک کن</p></div>
<div class="m2"><p>در اعمال شایسته چالاک کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بآبی بسر ریزم از بهر غسل</p></div>
<div class="m2"><p>دلم را چو اعضای تن پاک کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هجوم شیاطین ز دل دوردار</p></div>
<div class="m2"><p>قرین دلم خیل املاک کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب طهوری بکامم رسان</p></div>
<div class="m2"><p>سراپای جانرا طربناک کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند شاد اگر سازدم العیاذ</p></div>
<div class="m2"><p>پشیمانیم بخش و غمناک کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگریان مرا در غم آخرت</p></div>
<div class="m2"><p>ازین درد آهم بر افلاک کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خوفت بخون دلم ده وضو</p></div>
<div class="m2"><p>ز احداث باطن دلم پاک کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بریزان ز من اشک تا اشک هست</p></div>
<div class="m2"><p>چو آبم نماند مرا خاک کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و قلبی ففزعه عمن سواک</p></div>
<div class="m2"><p>دهانم بذکراک مسواک کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعصیان سراپای آلوده‌ام</p></div>
<div class="m2"><p>سراپا ز آلودگی پاک کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو پاکیزه گردد ز لوث گنه</p></div>
<div class="m2"><p>دلم آینه صاف ادراک کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلم را بده عزم بر بندگی</p></div>
<div class="m2"><p>نه چون بیغمانم هوسناک کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخاک درت گر نیارم سجود</p></div>
<div class="m2"><p>مکافات آن بر سرم خاک کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلم راز پندار دانش بشوی</p></div>
<div class="m2"><p>بجان قایل ما عرفناک کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بعجب عمل مبتلایم مساز</p></div>
<div class="m2"><p>زبان ناطق ما عبدناک کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگه دارم از شر آفات نفس</p></div>
<div class="m2"><p>بتلبیس ابلیس دراک کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشاطی بده در عبادت مرا</p></div>
<div class="m2"><p>دل لشکر دیو غمناک کن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بحشرم بده نامه در دست راست</p></div>
<div class="m2"><p>ز هولم در آنروز بی باک کن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز یمن ولای علی فیضرا</p></div>
<div class="m2"><p>قرین مکرم بلولاک کن</p></div></div>