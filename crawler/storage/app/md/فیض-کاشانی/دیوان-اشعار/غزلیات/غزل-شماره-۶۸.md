---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>تن خاک راه دوست کنم حسبی الحبیب</p></div>
<div class="m2"><p>جان نیز در رهش فکنم حسبی الحبیب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عشق در سرای وجودم نزول کرد </p></div>
<div class="m2"><p>از خویشتن طمع بکنم حسبی الحبیب </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل سوخت چون در آتش سودای عشق او </p></div>
<div class="m2"><p>جان هم در آتشش فکنم حسبی الحبیب </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ناصر من اوست چو منصور میروم </p></div>
<div class="m2"><p>خود را بدار عشق زنم حسبی الحبیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلاج عشق چون بزند پنبهٔ تنم </p></div>
<div class="m2"><p>بر دست و بازوی که تنم حسبی الحبیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهرش چو ذره ذره کند پیکر مرا </p></div>
<div class="m2"><p>من در هواش رقص کنم حسبی الحبیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بر کنم چو فیض زبود و نبود خویش </p></div>
<div class="m2"><p>بر هر چه رای اوست تنم حسبی الحبیب</p></div></div>