---
title: >-
    غزل شمارهٔ ۴۶۰
---
# غزل شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>گوشهٔ چشمی بسوی دردمندان کن بناز</p></div>
<div class="m2"><p>تا به بینی روی ناز خود بمرآت نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه از خود رفت از دیدار تو بازار رخت</p></div>
<div class="m2"><p>باز می‌آید بخود چشمی کند گر باز باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناز کن هرچند بتوانی که عاشق میکشد</p></div>
<div class="m2"><p>عاشقان را مغتنم باشند ز اهل ناز ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بخاطر بگذرانی اینکه راهی سر دهی</p></div>
<div class="m2"><p>در زمان آن ناز را‌ آیند جان‌ها بیشواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست بیرون آی و از مستان عشقت جان طلب</p></div>
<div class="m2"><p>نا کند جان‌ها بسویت بهر سبقت تر کتاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مستت را بگو تا بنگرد از هر طرف</p></div>
<div class="m2"><p>چون گذر آری بعمری بر اسیران نیاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون گذر آری بر اهل دل توقف کن دمی</p></div>
<div class="m2"><p>تا شود چشم نظر بازان بر آن رخساره باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر درت خوار ایستاده از تو خواهم یکنظر</p></div>
<div class="m2"><p>ای بصد تمکین نشسته بر سریر عزّ و ناز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه رویت دیده یکباره دگر بنماش روی</p></div>
<div class="m2"><p>تا کند چشمی بروی دلگشایت باز باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند باشم در امید و بیم وصل و هجر تو</p></div>
<div class="m2"><p>دل مبر یا جان ببر ای دلنواز جان گداز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در فراق خود مسوزانم بده کامم ز وصل</p></div>
<div class="m2"><p>رحم کن بر زاریم جز تو ندارم چاره ساز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی آتشناک بنما تا بسوزد بیخ غم</p></div>
<div class="m2"><p>در فراقت فیض را تا چند داری در گداز</p></div></div>