---
title: >-
    غزل شمارهٔ ۲۵۰
---
# غزل شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>خوشا آن سر که سودای تو دارد</p></div>
<div class="m2"><p>خوشا آندل که غوغای تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک غیرت برد افلاک حسرت</p></div>
<div class="m2"><p>جنونی را که شیدای تو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم در سر تمنای وصالت</p></div>
<div class="m2"><p>سرم در دل تماشای تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرود آید به جز وصل تو هیهات</p></div>
<div class="m2"><p>سر شوریده سودای تو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم کی باز ماند چون بپرواز</p></div>
<div class="m2"><p>هوای قاف عنقای تو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ماهی می‌طپم بر ساحل هجر</p></div>
<div class="m2"><p>که جانم عشق در پای تو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل و جانرا کنم ماوای آن کو</p></div>
<div class="m2"><p>دل و جان بهر ماوای تو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهم در پای آن شوریده سر کو</p></div>
<div class="m2"><p>سر شوریده در پای تو دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فدایت چون کنم بپذیر جانا</p></div>
<div class="m2"><p>چرا کاین سر تمنای تو دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چگونه تن زند از گفت‌وگویت</p></div>
<div class="m2"><p>چو در سر فیض هیهای تو دارد</p></div></div>