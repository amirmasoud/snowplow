---
title: >-
    غزل شمارهٔ ۲۴۷
---
# غزل شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>سرم سودای سودائی ندارد</p></div>
<div class="m2"><p>دلم پروای پروائی ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز سودای عشق لا ابالی</p></div>
<div class="m2"><p>سر شوریده سودائی ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز پروای بی‌پروا نگاری</p></div>
<div class="m2"><p>دل دیوانه پروائی ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل آزاده‌ام از هر دو عالم</p></div>
<div class="m2"><p>تمنای تمنائی ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم از زندگانی سرد از آن نیست</p></div>
<div class="m2"><p>که دیک عیش حلوائی ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم از زندگانی سرد از آنست</p></div>
<div class="m2"><p>که غم در دل دگر جائی ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل عاشق نمی‌اندیشد از مرگ</p></div>
<div class="m2"><p>که بر آزادگان پائی ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عیسی جای او در آسمانست</p></div>
<div class="m2"><p>که در روی زمین جائی ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگردنیات باید دل بکن زو</p></div>
<div class="m2"><p>که دنیا دوست دنیائی ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نباشد هیچ عقیائی به ار عشق</p></div>
<div class="m2"><p>نگوئی فیض عقبائی ندارد</p></div></div>