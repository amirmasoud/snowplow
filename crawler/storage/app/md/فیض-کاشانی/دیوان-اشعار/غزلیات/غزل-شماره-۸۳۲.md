---
title: >-
    غزل شمارهٔ ۸۳۲
---
# غزل شمارهٔ ۸۳۲

<div class="b" id="bn1"><div class="m1"><p>یا رب این مهجور را در بزم وصلت بار ده</p></div>
<div class="m2"><p>ار می روحانیانش ساغر سرشار ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بجان آمد مرا زین عالم پر شور و شر</p></div>
<div class="m2"><p>راه بنما سوی قدسم عیش بی آزار ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخت می‌ترسم که عالم گردد از اشگم خراب</p></div>
<div class="m2"><p>یا رب این سیلاب خون را ره بدریا بار ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در فراقت مردم ایجان جهان رحمی بکن</p></div>
<div class="m2"><p>یا دلم خوش کن موعدی با به وصلم بار ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل همیخواهد که قربانت شود در عید وصل</p></div>
<div class="m2"><p>جام لاغر را بپرور شیوهٔ این کار ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیره شد جان و دلم از امتزاج آب و گل</p></div>
<div class="m2"><p>سینه را اسرار بخش و دیده را انوار ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل جزئی از سرم کن دور و عقل کل فرست</p></div>
<div class="m2"><p>زنگ غم بزدای از دل شادی غمخوار ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بکی مخمور باشند از می روز الست</p></div>
<div class="m2"><p>عاکفان کوی خود را باده اسرار ده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر گروهی را ز فضلت نعمتی شایسته بخش</p></div>
<div class="m2"><p>زاهدان را وعد جنت عاشقان را بار ده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا رب آنساعت که از دهشت زبان ماند ز کار</p></div>
<div class="m2"><p>فیض را الهام حق کن طاقت گفتار ده</p></div></div>