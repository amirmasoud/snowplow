---
title: >-
    غزل شمارهٔ ۳۲۹
---
# غزل شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>دست از دلم بدار که تابم دگر نماند</p></div>
<div class="m2"><p>از بس سرشک ریختم آبم دگر نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند و چند با دل خونین کنم عتاب</p></div>
<div class="m2"><p>گشتم خجل ز خویش عتابم دگر نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای یار غمگسار دگر حال دل مپرس</p></div>
<div class="m2"><p>بستم زبان ز حرف جوابم دگر نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پندم دگر مده که نمانده است جای پند</p></div>
<div class="m2"><p>لب را به بند تاب خطابم دگر نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسودگی نماند دگر در سرای تن</p></div>
<div class="m2"><p>بیزار گشتم از خود و خوابم دگر نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایم فتاد از ره و دستم ز کار ماند</p></div>
<div class="m2"><p>پیری شتاب کرد و شتابم دگر نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیریست درد میکشم از عیش روزگار</p></div>
<div class="m2"><p>در جام خوشدلی می نابم دگر نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جست‌وجوی آب کرم بر و بحر را</p></div>
<div class="m2"><p>گشتم بسی بسر که سرابم دگر نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای یار فیض برده ز باران صحبتم</p></div>
<div class="m2"><p>دامان بگش ز فیض سحابم دگر نماند</p></div></div>