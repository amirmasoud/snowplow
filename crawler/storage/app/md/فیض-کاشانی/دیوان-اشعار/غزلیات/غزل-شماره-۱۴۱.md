---
title: >-
    غزل شمارهٔ ۱۴۱
---
# غزل شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>آن ملاحت که تو داری گهر حسن آنست </p></div>
<div class="m2"><p>ببهایش نرسد هیچ متاع ار همه جانست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما نداریم متاعی که بود در خور وصلت </p></div>
<div class="m2"><p>تو گران قیمتی و هر چه ترا هست گرانست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو سودا نتوانیم مگر لطف کنی تو </p></div>
<div class="m2"><p>کانچه ما رابه ازآن نه همه چیزت به از آنست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسهٔ گر برباید زلبت سوخته جانی </p></div>
<div class="m2"><p>شود او زنده و جاوید و لب لعل همانست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهل باشد ز تو سودی ببرد عاشق مسکین </p></div>
<div class="m2"><p>کز عطای تو ترا هیچ نه نقصان نه زیانست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میزند بر لب من دست ادب قفل خموشی </p></div>
<div class="m2"><p>ورنه بسیار سخن هست که محتاج بیانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرف سودا سخن سود و زیان هیچ مگو فیض</p></div>
<div class="m2"><p>کاین سخن چون سر سودا زده گوید هذیانست </p></div></div>