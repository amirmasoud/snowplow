---
title: >-
    غزل شمارهٔ ۳۳۳
---
# غزل شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>در دیگ عشق باده کشان جوش کرده‌اند</p></div>
<div class="m2"><p>بر خود ز پختگی همه سرپوش کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بادا حلالشان که بحرمت گرفته‌اند</p></div>
<div class="m2"><p>هر مستی که زان می سر جوش کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی جناب عشق به پرهیز رفته‌اند</p></div>
<div class="m2"><p>پرهیز را برندی روپوش کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جرعهٔ کز آن می بیغش کشیده‌اند</p></div>
<div class="m2"><p>جان در عوض بداده و خون نوش کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بهر بارهای گران در ره حبیب</p></div>
<div class="m2"><p>سر تا بپای روح همه دوش کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پای تا بسر همه روح مجردند</p></div>
<div class="m2"><p>از لطف طبع ترک تن و توش کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارند گفت‌وگوی نهان با جناب دوست</p></div>
<div class="m2"><p>بر خویش پرده از لب خاموش کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنهان بریز پرده رندی روان خویش</p></div>
<div class="m2"><p>در معرض سروش همه گوش کرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکدم نیند غافل و غافل گمان کند</p></div>
<div class="m2"><p>کاینان ز اصل خویش فراموش کرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دیک ابتلاء بسی کفجه خورده‌اند</p></div>
<div class="m2"><p>تا لقمهٔ ز کاسهٔ سر نوش کرده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم عقل را ز عشقش دیوانه ساخته</p></div>
<div class="m2"><p>هم هوش را بیادش بیهوش کرده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از ما سوی چو دست ارادت کشیده‌اند</p></div>
<div class="m2"><p>با شاهد مراد در آغوش کرده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهاد خام را بنظر کی در آورند</p></div>
<div class="m2"><p>آنان که در محبت حق جوش کرده‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با درد نوش شاید اگر مرحمت کنند</p></div>
<div class="m2"><p>آنان که صاف باده حق نوش کرده‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا شعر فیض اهل بصیرت شنیده اند</p></div>
<div class="m2"><p> اشعار خویش جمله فراموشش کرده اند</p></div></div>