---
title: >-
    غزل شمارهٔ ۶۸۴
---
# غزل شمارهٔ ۶۸۴

<div class="b" id="bn1"><div class="m1"><p>چشم بر هر چه گشادیم رخ خوب تو دیدیم</p></div>
<div class="m2"><p>گوش بر هر چه نهادیم حدیث تو شنیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردمان چشم گشودند و ندیدند به جز غیر</p></div>
<div class="m2"><p>ما ببستیم دو چشم و بجمالت نگریدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لوح دلرا که بر آن نقش و نگار دگران بود</p></div>
<div class="m2"><p>پاک شستیم و بر آن صورت خوب تو کشیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن خوبان فریبنده ز دریای تو موج است</p></div>
<div class="m2"><p>ابروی همه از حسن روانبخش تو دیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سراب دو جهان رهزن دین و دل ما شد</p></div>
<div class="m2"><p>آخرالامر بسر چشمهٔ مقصود رسیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارفان وصف تو از دفتر و اسناد شنیدید</p></div>
<div class="m2"><p>ما ز یاقوت گهربار لبان تو شنیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تشنه یکچند دویدیم درین وادی خونخوار</p></div>
<div class="m2"><p>آخر از چشمه حیوان تو یکجرعه چشیدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطرهٔ مستی ما را ز می عشق تو بس بود</p></div>
<div class="m2"><p>لله الحمد بدریای وصال تو رسیدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بایع و بیع و ثمن مشتری و جنس تو بودی</p></div>
<div class="m2"><p>سر بسر کوچه و بازار جهان را همه دیدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند بر خرقهٔ پرهیز زدن پنبهٔ توبه</p></div>
<div class="m2"><p>آفرین باد ترا عشق کزین خرقه رهیدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بارها جامهٔ تقوی بگنه چاک ز دستیم</p></div>
<div class="m2"><p>از بی حلّهٔ عفو تو بسی جامه دریدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پای سعیت همه شد آبله در راه طلب فیض</p></div>
<div class="m2"><p>بار ما در دل ما بود عبث می‌طلیدیم</p></div></div>