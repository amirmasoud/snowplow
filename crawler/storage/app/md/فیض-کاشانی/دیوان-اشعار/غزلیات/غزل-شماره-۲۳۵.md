---
title: >-
    غزل شمارهٔ ۲۳۵
---
# غزل شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>تا جان نشود ز این و آن فرد</p></div>
<div class="m2"><p>بر دل نشود غم جهان فرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل نشود بعشق او جفت</p></div>
<div class="m2"><p>جان کی گردد در این و آن فرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آتش عشق تا نجوشی</p></div>
<div class="m2"><p>جان می نتوان فدای آن کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیدردی از آن تمام دردی</p></div>
<div class="m2"><p>در دست دوای مرد بیدرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد است دوای هر فسرده</p></div>
<div class="m2"><p>بفروش متاع جان بخردرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مرد زنان و رهزنانی</p></div>
<div class="m2"><p>در راه خدای نیستی فرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزدای ز دل غبار کثرت</p></div>
<div class="m2"><p>بنگر بجمال واحد فرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی فیض رسد بگرد مردان</p></div>
<div class="m2"><p>تا زو باقیست ذرهٔ گرد</p></div></div>