---
title: >-
    غزل شمارهٔ ۱۳۸
---
# غزل شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>مگو که چهره او را نقاب در پیشست </p></div>
<div class="m2"><p>ترا زهستی و همی حجاب در پیشست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حجاب دیدن آن روی شرک و خودبینی است </p></div>
<div class="m2"><p>زهستی تو رخش را نقاب در پیشست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجود او بمثل همچو آب و تو ماهی </p></div>
<div class="m2"><p>خبر زآب نداری و آب در پیشست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی به پرده دنیی دری گهی عقبی </p></div>
<div class="m2"><p>بسی زظلمت و نورت حجاب در پیشست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نماید آنکه بود او نه اوست غره مشو</p></div>
<div class="m2"><p>تو تا به آب رسی بس سراب در پیشست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر باو نتوان کرد چون زعکس رخش </p></div>
<div class="m2"><p>بدور باش هزار آفتاب در پیشست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگه باو نتواند رسید چون برهش </p></div>
<div class="m2"><p>زتار زلف بسی پیچ و تاب در پیشست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کتاب حسن بتان صورت است و او معنی </p></div>
<div class="m2"><p>بهوش باش گرت این کتاب در پیشست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو هوش ماند چون جلوه کرد اینمعنی </p></div>
<div class="m2"><p>اگر محیط شوی اضطراب در پیشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس است فیض زاین فن سخن که سامع را </p></div>
<div class="m2"><p>ز شبهه صد سخن بیحساب در پیشست </p></div></div>