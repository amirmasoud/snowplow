---
title: >-
    غزل شمارهٔ ۴۴۲
---
# غزل شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>آمدم کآتش زنم در بیخ جبر و اختیار</p></div>
<div class="m2"><p>تا بسوزد شرک و گردد نور توحید آشکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمدم تا خویش را بر لا و بر الا زنم</p></div>
<div class="m2"><p>تا نماند غیر یار اغیار گردد تار و مار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمدم فانی شوم در ساقی جام الست</p></div>
<div class="m2"><p>تا بقا یابم بدان ساقی بمانم پایدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمدم تا سر گشایم باده‌های کهنه را</p></div>
<div class="m2"><p>تا نماند در میان عاقلان یک هوشیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمدم تا توپهای خشک و مغزان بشکنم</p></div>
<div class="m2"><p>تلخشان شیرین کنم زین آب تلخ خوشکوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمدم تا بر سر رندان بریزم بادها</p></div>
<div class="m2"><p>تا نه در میخانها مخمور ماند نی خمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمدم بر گیرم از روی معانی پرده‌ها</p></div>
<div class="m2"><p>تا شو اسرار پنهان بر خلایق آشکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمدم پس میروم تا منبع هر هستی</p></div>
<div class="m2"><p>تا به بینم ز آینه آغاز کار انجام کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میروم تا باز جویم معدن این شور و شر</p></div>
<div class="m2"><p>از کجا این مستی آمد چیست اصل این خمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میروم تا باز جویم اصل این جوش و خروش</p></div>
<div class="m2"><p>تا مگر واقف شوم از منبع این چشمه سار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به بینم باده و مستی و مستی بخش را</p></div>
<div class="m2"><p>می کدام و چیست مستی کیست آنجا میگسار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میروم تا باز بینم روح را ماوا کجاست</p></div>
<div class="m2"><p>از کجا آمد کجا خواهد گرفت آخر قرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز می‌آیم بدینجا تا نشان‌ها آورم</p></div>
<div class="m2"><p>از دیار شهریار و شهریاران دیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز می‌آیم که تا آگه کنم زان رازها</p></div>
<div class="m2"><p>آنکه را نبود خبر از کار سر و از سرکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باز می‌آیم که نگذارم به عالم کج روی</p></div>
<div class="m2"><p>رهزنانرا رهبرانیم رهروانرا راهوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باز می‌آیم که تا ارواح در ابدان دمم</p></div>
<div class="m2"><p>مردگانرا زنده سازم در دم اسرافیل وار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باز می‌آیم که تا از خود نمایم رستخیز</p></div>
<div class="m2"><p>تا شود سر قیامت هم در اینجا آشکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز می‌آیم که تا با فیض گیرم الفتی</p></div>
<div class="m2"><p>تا کنم جمعیتی حاصل ز بود مستعار</p></div></div>