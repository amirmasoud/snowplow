---
title: >-
    غزل شمارهٔ ۷۷۳
---
# غزل شمارهٔ ۷۷۳

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی بده آن آب گلگون</p></div>
<div class="m2"><p>که دل تنگ آمد از اوضاع گردون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد را از سرای سر بدر کن</p></div>
<div class="m2"><p>بر افکن پرده از اسرار مکنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگوش جان صلای عشق در ده</p></div>
<div class="m2"><p>رسوم عاقلان را کن دگرگون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکنج درد و غم تا کی نشینم</p></div>
<div class="m2"><p>شکیبائی شد از اندازه بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببا تا آه آتشناک از دل</p></div>
<div class="m2"><p>روان سازیم سوی چرخ گردون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک را سقف بشکافیم شاید</p></div>
<div class="m2"><p>رویم از تنگنای دهر بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل و جانرا نثار دوست سازیم</p></div>
<div class="m2"><p>که غیر دوست افسانه است و افسون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقم کن بر دل و بر جانت ای فیض</p></div>
<div class="m2"><p>برات سرخ روئی ز اشگ گلگون</p></div></div>