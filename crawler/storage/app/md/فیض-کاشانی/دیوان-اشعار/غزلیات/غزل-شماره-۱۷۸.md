---
title: >-
    غزل شمارهٔ ۱۷۸
---
# غزل شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>عشق آمد و اختیار نگذاشت </p></div>
<div class="m2"><p>در کشور دل قرار نگذاشت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جان اثری نماند در تن </p></div>
<div class="m2"><p>وزخاک تنم غبار نگذاشت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیفیت چشم پرخمارت </p></div>
<div class="m2"><p>در هیچ سری خمار نگذاشت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنهان میخواست دل غمت را </p></div>
<div class="m2"><p>این دیدهٔ اشگبار نگذاشت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا جلوه کند درو جمالت </p></div>
<div class="m2"><p>اشگم در دل غبار نگذاشت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عبرت نتوان گرفت از دهر </p></div>
<div class="m2"><p>چون فرصت اعتبار نگذاشت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشگفته بریخت غنچه دل </p></div>
<div class="m2"><p>تعجیل خزان بهار نگذاشت </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتم که بپاش جان فشانم</p></div>
<div class="m2"><p>دستم بگرفت و یار نگذاشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفتم که کنم شکایت از فیض </p></div>
<div class="m2"><p>کوتاهی روزگار نگذاشت </p></div></div>