---
title: >-
    غزل شمارهٔ ۸۶۵
---
# غزل شمارهٔ ۸۶۵

<div class="b" id="bn1"><div class="m1"><p>در عشق دوست ای دل شیدا چگونه‌ای</p></div>
<div class="m2"><p>ای قطره کشاکش دریا چگونه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یادآور ای عدم ز نهانخانه‌ای قدم</p></div>
<div class="m2"><p>پنهان چگونه بودی و پیدا چگونه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بحر بی کنار کنارم کشید و گفت</p></div>
<div class="m2"><p>بی ما چگونه بودی و با ما چگونه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من جلوه نا نموده تواز خویش میشدی</p></div>
<div class="m2"><p>امروز غرق بحر تجلا چگونه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمعی بساحل از کشش ما در اضطراب</p></div>
<div class="m2"><p>ای غرق بحر عاطفت ما چگونه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازم ز خویش‌ راند و بکنج غمم نشاند</p></div>
<div class="m2"><p>گفت ای نشانه تیر بلا را چگونه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چاه بابلم موی خود ببست</p></div>
<div class="m2"><p>گفت ای اسیر زلف چلیپا چگونه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خانه زاد عشرت و پرورده‌ای طرب</p></div>
<div class="m2"><p>در لجه محیط غم ما چگونه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای فیض خویش را بغم عشق ما سپار</p></div>
<div class="m2"><p>و آنگه ببین که در کنف ما چگونه‌ای</p></div></div>