---
title: >-
    غزل شمارهٔ ۱۶۰
---
# غزل شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>در پردهٔ حسن دلربا کیست </p></div>
<div class="m2"><p>این رشته بدست شاهدان نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بیخبرم زخویش و او مست </p></div>
<div class="m2"><p>هشیار میان ما و او کیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معشوق که عشق چیست یا رب </p></div>
<div class="m2"><p>این می زکجا و این چه مستی است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چشم خوش بتان چه نشأه است </p></div>
<div class="m2"><p>این می زکف کدام ساقیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این روشنی از کدام خورشید </p></div>
<div class="m2"><p>این آب زچشمهٔ که جاریست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده است برآب کس چنین نقش </p></div>
<div class="m2"><p>مشاطهٔ حسن نو خطان کیست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیماری چشم گلرخان را </p></div>
<div class="m2"><p>در پردهٔ دلبری سبب چیست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هر نگهی هزار فتنه </p></div>
<div class="m2"><p>این معجزهٔ کدام عیسی است </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک تیر آید بصد نشانه </p></div>
<div class="m2"><p>زه زه زکمان و بازوی کیست </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هشدار که دیگریست دلبر </p></div>
<div class="m2"><p>دریاب که عشق ما حقیقی است </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در حسن بتان تجلی اوست </p></div>
<div class="m2"><p>حق باشد این عشق و حق پرستیست </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حسن از حق است و عشق از حق </p></div>
<div class="m2"><p>نامی بر ما زعشق بازیست </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای شاهد شاهدان عالم </p></div>
<div class="m2"><p>معشوق به جز تو در جهان کیست </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرهاد تو صد هزار شیرین </p></div>
<div class="m2"><p>مجنون تو صد هزار دلیل است </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای فیض خراب عشق میباش </p></div>
<div class="m2"><p>آبادی ما در این خرابیست </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از خود بگذر بعشق پیوند </p></div>
<div class="m2"><p>باقی عشقست و جمله فانیست </p></div></div>