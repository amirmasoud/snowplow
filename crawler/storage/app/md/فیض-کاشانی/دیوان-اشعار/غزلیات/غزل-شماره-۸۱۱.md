---
title: >-
    غزل شمارهٔ ۸۱۱
---
# غزل شمارهٔ ۸۱۱

<div class="b" id="bn1"><div class="m1"><p>هجران جانان تا بچند آ» یار کو آن یار کو</p></div>
<div class="m2"><p>وین شورش دل تا بکی دلدار کو دلدار کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه دلها شد طپان جانها ز تنها شد زوان</p></div>
<div class="m2"><p>تا کی بود این رو نهان دیدار کو دیدار کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذرات عالم مست او خورده شراب از دست او</p></div>
<div class="m2"><p>نغمه‌سرایان کو بکو خمار کو خمار کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افلاک سر گردان و مست خاکست مدهوش الست</p></div>
<div class="m2"><p>در عالم بالا و پست هشیار کو هشیار کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلاج محو آن جمال دستک زنان در وجد و حال</p></div>
<div class="m2"><p>نغمه‌سرا کای ذوالجلال آندار کو آندار کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دنیی و عقبی مپیچ جز حق همه هیچست هیچ</p></div>
<div class="m2"><p>در دار عالم غیر حق دیار کو دیار کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حق در برابر روبرو بنموده رو از چار سو</p></div>
<div class="m2"><p>کوران گرفته جستجو کان یار کو کان یار کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منصور انالحق میزند من صور حق حق میزنم</p></div>
<div class="m2"><p>زینصور انا شاهد فنا جز یار کو جز یار کو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر راست میگوئی تو فیض دم در کش و خاموش باش</p></div>
<div class="m2"><p>آنرا که باشد محو یار گفتار کو گفتار کو</p></div></div>