---
title: >-
    غزل شمارهٔ ۲۶
---
# غزل شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ترا سزاست خدایی نه جسم را و نه جان را</p></div>
<div class="m2"><p>تو را سزد که خودآیی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی تویی که تویی و منی و مایی و اویی</p></div>
<div class="m2"><p>منی نشاید و مایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی که تای ندارد وحید و فردی و یکتا</p></div>
<div class="m2"><p>نبود غیر دوتایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را رسد که در آیینهٔ رسالت احمد</p></div>
<div class="m2"><p>جمال خویش نمایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را رسد به نسیم کلام آل محمد ص</p></div>
<div class="m2"><p>زر از چهره گشایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را رسد که هزاران هزار نقش بدایع</p></div>
<div class="m2"><p>ز کلک صنع نمایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا رسد که دو صدساله زنگ کفر و گنه را</p></div>
<div class="m2"><p>ز لوح دل بزدایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا رسد که چو جا نشد ز جسم جسم ز هم ریخت</p></div>
<div class="m2"><p>دگر اعاده نمایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا رسد که در آیینهٔ نعیم و عقوبت</p></div>
<div class="m2"><p>به لطف و قهر در آیی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به لطف خویش ببخشا اسیر قهر خودت را</p></div>
<div class="m2"><p>چو نیست از تو رهایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه‌ایم از تو جدا موج‌های بحر وجودیم</p></div>
<div class="m2"><p>نباشد از تو جدایی نه جسم را و نه جان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز ما و من چون بپرداخت فیض خانهٔ دل را</p></div>
<div class="m2"><p>تو را رسد که در آیی نه جسم را و نه جان را</p></div></div>