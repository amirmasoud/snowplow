---
title: >-
    غزل شمارهٔ ۲۷۱
---
# غزل شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>یار آمد از درم سحری در فراز کرد</p></div>
<div class="m2"><p>برقع گشود و روی چو خورشید باز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم بر دل شکسته در خرمی گشاد</p></div>
<div class="m2"><p>هم بر روان خسته در عیش باز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول ز راه لطف در آمد به دلبری</p></div>
<div class="m2"><p>آخر ربود چون دلم آهنگ ناز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افتادمش به پا ز ره عجز و مسکنت</p></div>
<div class="m2"><p>کف بر سرم نهاد و مرا سرفراز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی خزان عمر خزان بردم آن بهار</p></div>
<div class="m2"><p>صد در برویم از گل رخسار باز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم چه میکنند بدلهای عاشقان</p></div>
<div class="m2"><p>گفت آنچه باروان و دل صید باز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که میرسد بسرا پردهٔ قبول</p></div>
<div class="m2"><p>گفت آنکه از قبول کسان احتراز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم بکنه سرّ حقایق که میرسد</p></div>
<div class="m2"><p>گفتا کسی که از دو جهان جوی باز کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پا از گلیم خویش مکش کی توان رسید</p></div>
<div class="m2"><p>در گرد آنکه بر دو جهان در فراز کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامان نگاه دار و گریبان، نمی‌توان</p></div>
<div class="m2"><p>با آستین کوته دستی دراز کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگذار کبریا ز در مسکنت در آ</p></div>
<div class="m2"><p>خاتم بعرش هم به تضرع نماز کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر جان گداز یافت ز سوزی و جان فیض</p></div>
<div class="m2"><p>دل بوتهٔ محبت جانان گداز کرد</p></div></div>