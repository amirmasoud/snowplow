---
title: >-
    غزل شمارهٔ ۷۵۳
---
# غزل شمارهٔ ۷۵۳

<div class="b" id="bn1"><div class="m1"><p>جان ز من مستان دل ببر خون کن</p></div>
<div class="m2"><p>اینچنین که باشد دردم افزون کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کنی صیدم غمزه را سرده</p></div>
<div class="m2"><p>تا روم از خود چهره میگون کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه‌ام بریان دیده‌ام گریان</p></div>
<div class="m2"><p>هوش را حیران عقل مفتون کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای فدایت من خیز بسم‌الله</p></div>
<div class="m2"><p>قصد جانم را تیغ بیرون کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی افسون من از تو بنیوشم</p></div>
<div class="m2"><p>یا بکش ورنه ترک افسون کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پای دل بگشا از سر زلفت</p></div>
<div class="m2"><p>سر بصحرا ده تای مجنون کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان من آن کن کان دلت خواهد</p></div>
<div class="m2"><p>حاش لله من گویمت چون کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده را از آن رو روشنائی ده</p></div>
<div class="m2"><p>ور نه از اشگش رشک جیحون کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش حکم تو سر نهادم من</p></div>
<div class="m2"><p>خواهیم کم کن خواهی افزون کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> فیض میخواهد آنچه را خواهی</p></div>
<div class="m2"><p>خواهیش خرم ور نه محزون کن</p></div></div>