---
title: >-
    غزل شمارهٔ ۵۹۹
---
# غزل شمارهٔ ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>نگاهی کن که شیدای تو گردم</p></div>
<div class="m2"><p>خرابم کن که ماوای تو گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراپا در سراپای تو محوم</p></div>
<div class="m2"><p>بقربان سراپای تو گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بالایت بلائی کس ندیده</p></div>
<div class="m2"><p>بلا گردان بالای تو گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیثی زان لب شیرین بفرما</p></div>
<div class="m2"><p>که شورستان سودای تو گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برقص آجلوهٔ مستانهٔ کن</p></div>
<div class="m2"><p>که مدهوش تماشای تو گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بغمزه آب ده تیغ نگه را</p></div>
<div class="m2"><p>فدای چشم شهلای تو گردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیفکن سایهٔ خود بر سر فیض</p></div>
<div class="m2"><p>اسیر قد رعنای تو گردم</p></div></div>