---
title: >-
    غزل شمارهٔ ۲۵۳
---
# غزل شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>شهره شهر شود هر که جمالی دارد</p></div>
<div class="m2"><p>کشد آزار خسان هر که کمالی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن را جلوه مده در نظر بی‌دردان</p></div>
<div class="m2"><p>جلوه آفت بود آنرا که جمالی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خمش ای مرغ خوش آواز که در سر صیاد</p></div>
<div class="m2"><p>بهر تدبیر شکار تو خیالی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط و خالش چکند جلوه و بالی شودش</p></div>
<div class="m2"><p>دل طاوس بدان شاد که بالی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر دل مده از کف بمتاع دنیا</p></div>
<div class="m2"><p>که نیرزد بگهی هر چه زوالی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو به بیهوده مکن سعی که در دار فنا</p></div>
<div class="m2"><p>هر که راحت طلبد فکر محالی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان کند در طلب دنیی و بیگانه خورد</p></div>
<div class="m2"><p>خواجه شاد است که مالی و منالی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاید از قدر ضروریش وبالست و بال</p></div>
<div class="m2"><p>ای خوش آنکس که کفافی ز حلالی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض را بر سر آن کوی چو بینی بیخود</p></div>
<div class="m2"><p>بگذارش بهمان حال که حالی دارد</p></div></div>