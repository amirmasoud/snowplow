---
title: >-
    غزل شمارهٔ ۸۵۷
---
# غزل شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی بده جامی از آن می</p></div>
<div class="m2"><p>که جان عاشقان از وی بود حی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن می کآورد جان در تن من</p></div>
<div class="m2"><p>کند یکجرعه‌اش لاشیء را شیء</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر زاهد کشد در رقص آید</p></div>
<div class="m2"><p>بخاک مرده گر ریزی شود حی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن می کز فروغش شب شود روز</p></div>
<div class="m2"><p>سیه دل را کند خورشید بی فی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مئی کز من مرا بخشد خلاصی</p></div>
<div class="m2"><p>سرا پایم شود فانی از آن می</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا ساقی مرا از خویش برهان</p></div>
<div class="m2"><p>مگر طرفی ببندم از خود وی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه تاب وصل او دارم نه هجران</p></div>
<div class="m2"><p>نه با وی می‌توان بودن نه بی وی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا می ده مرا از خویش بستان</p></div>
<div class="m2"><p>مگو چون و مگو چند و مگو کی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیاپی ده که عشق آندم گواراست</p></div>
<div class="m2"><p>که در کف جام می‌آرد پیاپی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن داغم مگو کی، دمبدم ده</p></div>
<div class="m2"><p>دل مستان ندارد طاقت وی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه می‌پائی بده ساقی شرابی</p></div>
<div class="m2"><p>چه میخواری قفا مطرب بزن نی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا مطرب بزن بر تار دستی</p></div>
<div class="m2"><p>بیا ساقی بده جامی پر از می</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بده ساقی شرابی از بط و خم</p></div>
<div class="m2"><p>بزن مطرب نوای بربط و نی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میفکن عیش فصلی را بفصلی</p></div>
<div class="m2"><p>ز کف مگذار می در بهمن و دی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهاری کن سراسر عمر را فیض</p></div>
<div class="m2"><p>ز روی ساقی و جام پیاپی</p></div></div>