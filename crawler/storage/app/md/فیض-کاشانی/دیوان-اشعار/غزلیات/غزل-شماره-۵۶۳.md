---
title: >-
    غزل شمارهٔ ۵۶۳
---
# غزل شمارهٔ ۵۶۳

<div class="b" id="bn1"><div class="m1"><p>ای جمال هر جمیل و ای جمالت بی‌مثال</p></div>
<div class="m2"><p>هر جمال از تست زانرو دوست میداری جمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جمالت پرتوی بر هر جمیل افکندهٔ</p></div>
<div class="m2"><p>زین سبب دل میبرد هر جانبی صاحبجمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بود اهل نظر را حسن خوبان دلربا</p></div>
<div class="m2"><p>میرسد هر دم تجلی از جمال بی زوال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میرباید ز اهل دل دلرا بصد افسونگری</p></div>
<div class="m2"><p>حسنهای ذوالجمال و جلوه‌های ذوالجلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه تقوی خراب از سطوت سلطان حسن</p></div>
<div class="m2"><p>ملک دین ویران ز تیغ لشکر غنج و دلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن صورت دلفریب و حسن سیرت دلپذیر</p></div>
<div class="m2"><p>این بود پاینده آن در کاهش و در انتقال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن نباشد حسن کان کاهد ز دوران سپهر</p></div>
<div class="m2"><p>حسن آن باشد که افزاید بهر روزی کمال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن نباشد کز وی کام دل گردد روا</p></div>
<div class="m2"><p>حسن آن باشد که خون از دل بریزد بی‌قتال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن آن باشد که جانها را بسوزد بی‌نظیر</p></div>
<div class="m2"><p>حسن آن باشد که تنها را گدازد ز انفعال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن آن باشد که مهرش چون کند در سینه جا</p></div>
<div class="m2"><p>با دل آمیزد چو جان آسوده از بیم زوال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن آن باشد که بشناسد محبت از هوس</p></div>
<div class="m2"><p>تا دهد آنرا سرافرازی و این را پایمال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حسن نشناسد مگر صاحب کمالی کوچو فیض</p></div>
<div class="m2"><p>در ترقی باشد او هر روز و هفته ماه و سال</p></div></div>