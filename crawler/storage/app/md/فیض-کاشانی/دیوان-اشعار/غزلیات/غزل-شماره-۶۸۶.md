---
title: >-
    غزل شمارهٔ ۶۸۶
---
# غزل شمارهٔ ۶۸۶

<div class="b" id="bn1"><div class="m1"><p>ما دیدهٔ اشکبار داریم</p></div>
<div class="m2"><p>در سینه دلی فکار داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی بجفا اگر گشائی</p></div>
<div class="m2"><p>آهسته که شیشه بار داریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آتش عشق او کبابیم</p></div>
<div class="m2"><p>رو سرخ و درون زار داریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شعلهٔ آتشیم در رقص</p></div>
<div class="m2"><p>مستیم و هوای یار داریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوئی چو ز شهر یار آمد</p></div>
<div class="m2"><p>ما روی بدان دیار داریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را با شهر نیست کاری</p></div>
<div class="m2"><p>ما کار بشهر یار داریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آنروز که وعدهٔ لقا کرد</p></div>
<div class="m2"><p>ما چشم در انتظار داریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر مقدم یار لعل و گوهر</p></div>
<div class="m2"><p>از دیده و دل نثار داریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهد ار عشق ننگ داند</p></div>
<div class="m2"><p>ما نیز از زهد عار داریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو رطل گران سبک بما ده</p></div>
<div class="m2"><p>با خشک گران چه کار داریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پر کن جامی که این سر ما</p></div>
<div class="m2"><p>چون گشت تهی خمار داریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرمی اینست ساقی امسال</p></div>
<div class="m2"><p>ما دعوی غبن پار داریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما را تو غلام خویش مشمر</p></div>
<div class="m2"><p>در خیل سگان شمار داریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر درگه تو برای عزت</p></div>
<div class="m2"><p>خود را چون فیض خوار داریم</p></div></div>