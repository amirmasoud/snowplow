---
title: >-
    غزل شمارهٔ ۱۳۳
---
# غزل شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>نه فلک چرخ زنان سودائی تست </p></div>
<div class="m2"><p>بیخود افتاده زمین یکتن شیدائی تست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز تماشای جمال تو تماشائی نیست </p></div>
<div class="m2"><p>هر که حیران جمالیست تماشائی تست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که افراخت بدعوائی نکوئی کردن </p></div>
<div class="m2"><p>گر بود راست همان سایة زیبائی تست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سروقدان که زبالائی بالا بالند </p></div>
<div class="m2"><p>آن زبالای برازندة بالائی تست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گلی را که بود رنگ درینگلشن و بوی </p></div>
<div class="m2"><p>شمة از گل خود رستة زیبائی تست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ازل تاباند بینش هر بینائی </p></div>
<div class="m2"><p>همه یک بینش در پردة بینائی تست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه را دردو جهان نور هویدائی هست </p></div>
<div class="m2"><p>همه یک ذره خورشید هویدائی تست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرّ پنهان شدن روح نهان بودن تو </p></div>
<div class="m2"><p>رمز پیدا شدن قالب پیدائی تست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا رسم توانائی و دانائی هست</p></div>
<div class="m2"><p>نور دانائی تو زور توانائی تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنده خود کیست که خود رأی بود در کاری </p></div>
<div class="m2"><p>لاف خودرائی ما پرتو خود رائی تست </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسزای تو نکردیم دمی بندگیت</p></div>
<div class="m2"><p>آنچه هست سزاوار تو آقائی تست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فیض خود را تو بکردار خوش آراسته کن </p></div>
<div class="m2"><p>حسن گفتار نه در خورد خود آرائی تست </p></div></div>