---
title: >-
    غزل شمارهٔ ۱۰۹
---
# غزل شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>چراغ کلبه عاشق خیال دلدار است </p></div>
<div class="m2"><p>سری که عشق درونیست خانه تار است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار خرمن شادی به نیم جو نخرد </p></div>
<div class="m2"><p>بجان دلی که غم عشق را خریدار است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعشق زنده بود هر چه هست در عالم </p></div>
<div class="m2"><p>جهان نئیست درو جان عشق درکار است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو همتی طلبی از جناب عشق طلب </p></div>
<div class="m2"><p>که هر دو کون جنودند و عشق سردار است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حوالی دل عاشق نه بگذرد غفلت </p></div>
<div class="m2"><p>که عشق بر سر او پاسبان بیدار است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسد چو شادی بیجا براندش شه عشق </p></div>
<div class="m2"><p>سپاه غم چو کند زور عشق غمخوار است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر زپای درائیم عشق گیرد دست </p></div>
<div class="m2"><p>اگر خطای برائیم عشق ستار است </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو و حماقت و انکارحرف هر یاری </p></div>
<div class="m2"><p>من و معارف این کار جمله در کار است </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو ای فلان و ریاست که هر کس و کاری </p></div>
<div class="m2"><p>مرا بخاک ره او بشمرند بسیار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فکندگی بتو دشوار و بر من آسانست </p></div>
<div class="m2"><p>قلندری بمن آسان و برتو دشوار است </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی که راه ندارد بچارهٔ دردش </p></div>
<div class="m2"><p>زبهر چاره دگر چاره ایش ناچار است </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاختیار کم از اضطرار آزاد است </p></div>
<div class="m2"><p>چوفیض هر که بفرمان عشق قهار است</p></div></div>