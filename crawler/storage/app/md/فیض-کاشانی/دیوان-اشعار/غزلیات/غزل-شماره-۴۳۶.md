---
title: >-
    غزل شمارهٔ ۴۳۶
---
# غزل شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>زاهد گر ترا ریاست لذیذ</p></div>
<div class="m2"><p>من دلداده را هواست لذیذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ترا عافیت بود مطلوب</p></div>
<div class="m2"><p>من دیوانه را بلاست لذیذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ترا جوی شیر خوش آید</p></div>
<div class="m2"><p>نزد من اشک بی‌بهاست لذیذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو با جوی خمر خوش داری</p></div>
<div class="m2"><p>مر مرا خون دیدهاست لذیذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ترا انگبین دهد لذت</p></div>
<div class="m2"><p>حرف شیرین او مراست لذیذ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو حور و قصور میخواهی</p></div>
<div class="m2"><p>عاشقانرا ازو لقاست لذیذ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض با زاهدان جدال مکن</p></div>
<div class="m2"><p>عشق نزد خسان کجاست لذیذ</p></div></div>