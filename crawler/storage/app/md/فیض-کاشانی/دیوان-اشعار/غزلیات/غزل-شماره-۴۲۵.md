---
title: >-
    غزل شمارهٔ ۴۲۵
---
# غزل شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>عیش دنیا به جز خسان نرسید</p></div>
<div class="m2"><p>جز ریاضت به عاقلان نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سود کرد آنکه دل ز دنیا کند</p></div>
<div class="m2"><p>مرد آزاده را زیان نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت بیچاره آنکه دنیا خواست</p></div>
<div class="m2"><p>هرچه را بست دل بآن نرسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سود دنیا زیان، زیانش سود</p></div>
<div class="m2"><p>زین دو چیزی بعارفان نرسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان عارف گذشت از دو جهان</p></div>
<div class="m2"><p>دو جهانش بگرد جان نرسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هوا و هوس کسی نگذشت</p></div>
<div class="m2"><p>که بعشرتگه جهان نرسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که دل در سرای فانی بست</p></div>
<div class="m2"><p>همت کوتهش بآن نرسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که روزی زیاده خواست ز قوت</p></div>
<div class="m2"><p>دست جانش بقوت جان نرسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچکس سر بنان فرو نارد</p></div>
<div class="m2"><p>که بنانش بآب و نان نرسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که دنیا بآخرت نفروخت</p></div>
<div class="m2"><p>هم ازین ماند و هم بآن نرسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همت هیچکس نشد عالی</p></div>
<div class="m2"><p>که بفضل علوشان نرسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نتوان شرح این معانی فیض</p></div>
<div class="m2"><p>نه بدیعست گر بیان نرسید</p></div></div>