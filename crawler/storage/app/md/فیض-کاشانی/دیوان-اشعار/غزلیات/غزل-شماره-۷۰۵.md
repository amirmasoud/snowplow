---
title: >-
    غزل شمارهٔ ۷۰۵
---
# غزل شمارهٔ ۷۰۵

<div class="b" id="bn1"><div class="m1"><p>هر که میخواهد سخن گستر بود در انجمن</p></div>
<div class="m2"><p>اولش باید تامل در سخن آنگه سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سخن هر جا نتوان گفت با هر مستمع</p></div>
<div class="m2"><p>پاس وقت و جا و گوش و هوش باید داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که میخواهد که باشد در شمار عاقلان</p></div>
<div class="m2"><p>لب فرو بندد مگر وقتی که باید دمزدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه سخن خالی کن دلهای اندوه پر است</p></div>
<div class="m2"><p>گاه در دلهاست اندوه پشیمانی فکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه میریزد چو باران از سحاب معرفت</p></div>
<div class="m2"><p>تا دلی کان مرده باشد زنده گردد از سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه چو آبی در چهی یا شیر در پستان بود</p></div>
<div class="m2"><p>تا کشش نبود برون ناید ز جای خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوش و هوش مستمع چون باز شد بگشای لب</p></div>
<div class="m2"><p>ور به بینی بسته‌اش زنهار نگشائی دهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دری در دل نهان داری برون آر از صدف</p></div>
<div class="m2"><p>ور نداری حرف نیکی لب فروبند از سخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاجتی داری بگو یا سائلی را ده جواب</p></div>
<div class="m2"><p>حکمتی داری بیان کن ور نداری دم مزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حرف بسیار است در عالم ولی نیکش کمست</p></div>
<div class="m2"><p>هر که گوید حرف نیک ای فیض ازو بشنو سخن</p></div></div>