---
title: >-
    غزل شمارهٔ ۹۴۸
---
# غزل شمارهٔ ۹۴۸

<div class="b" id="bn1"><div class="m1"><p>الا ای که دلها نهان میربائی</p></div>
<div class="m2"><p>کجائی کجائی کجائی کجائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان من و بزم وصل تو تا کی</p></div>
<div class="m2"><p>جدائی جدائی جدائی جدائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو با این لطافت چنین بیمروت</p></div>
<div class="m2"><p>چرائی چرائی چرائی چرائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که گفتت سراپا وفائی غلط گفت</p></div>
<div class="m2"><p>جفائی جفائی جفائی جفائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکام کسی چون نهٔ می نگوئی</p></div>
<div class="m2"><p>کرائی کرائی کرائی کرائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خواهد شدن ایشب هجر اگر تو</p></div>
<div class="m2"><p>سرائی سرائی سرائی سرائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه پرسی که فیض از غم ما چه خواهد</p></div>
<div class="m2"><p>رهائی رهائی رهائی رهائی</p></div></div>