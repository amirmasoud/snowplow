---
title: >-
    غزل شمارهٔ ۳۸۱
---
# غزل شمارهٔ ۳۸۱

<div class="b" id="bn1"><div class="m1"><p>اگر سوی شام ار بری میرود</p></div>
<div class="m2"><p>اجل آدمی را ز پی میرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا سازره کن که معلوم نیست</p></div>
<div class="m2"><p>کزین خاکدان روح کی میرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی عمر آمد بهاران گذشت</p></div>
<div class="m2"><p>بهاران گذشتند و دی میرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر جا دلت رفت آنجاست جان</p></div>
<div class="m2"><p>سرا پای دل را ز پی میرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل تو چه شخص و تنت سایه است</p></div>
<div class="m2"><p>بهر جا رود شخص فی میرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل اندر خدا بند و بگسل ز خلق</p></div>
<div class="m2"><p>که آخر همه سوی وی میرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن روی دل در خدا کرد فیض</p></div>
<div class="m2"><p>که لاشیء دنبال شیء میرود</p></div></div>