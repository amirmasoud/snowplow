---
title: >-
    غزل شمارهٔ ۱۲۰
---
# غزل شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>عرصه لامکان سرای من است </p></div>
<div class="m2"><p>این کهن خاکدان چه جای من است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم از غصه خون شدی گر نه </p></div>
<div class="m2"><p>مونس جان من خدای من است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه او خسته داردم شب و روز </p></div>
<div class="m2"><p>خود هم او مرهم و شفای من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که زو بوی درد می آید </p></div>
<div class="m2"><p>صحبتش مایهٔ دوای من است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که او از دو کون بیگانه است </p></div>
<div class="m2"><p>در ره دوست آشنای من است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقصدم حق و مرکبم عشقست </p></div>
<div class="m2"><p>شعر من ناله درای من است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست با من کسی همیشه کزو </p></div>
<div class="m2"><p>تار و پود من و بقای من است </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سازدم هر چه قابل آنم </p></div>
<div class="m2"><p>دهدم هر چه آن سزای من است </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوبی من همه ز پرتو اوست </p></div>
<div class="m2"><p>گر بدی هست مقتضای من است </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من اگر هستم اوست هستی من </p></div>
<div class="m2"><p>ور شوم نیست او بجای من است </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خود ار بگذرم رسم بخدا </p></div>
<div class="m2"><p>بخدائی که منتهای من است </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بقضا فیض اگر شود راضی </p></div>
<div class="m2"><p>هر دو عالم بمدعای من است </p></div></div>