---
title: >-
    غزل شمارهٔ ۳۱۵
---
# غزل شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>ندادم دل بعشق و جان روان شد</p></div>
<div class="m2"><p>دریغا حاصل عمرم زیان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتن تا میرسیدم جان شد از دست</p></div>
<div class="m2"><p>بجان تا میرسیدم از جهان شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس تا میزدم می شد بغفلت</p></div>
<div class="m2"><p>مکان تا گرم میکردم زمان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا در خواب کرد انفاس و بگذشت</p></div>
<div class="m2"><p>ز خود غافل شدم تا کاروان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم تا بر خدا بندم هوا برد</p></div>
<div class="m2"><p>چنین میخواستم دل را چنان شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه عمرم درین اندیشه بگذشت</p></div>
<div class="m2"><p>که عمرم صرف باطل شد همانشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بغفلت رفت عمر و فکر غفلت</p></div>
<div class="m2"><p>ندانستم چه سان آمد چه سان شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه فکر غفلت هوشیاری است</p></div>
<div class="m2"><p>ولی راضی بآن کی میتوان شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبردم بهرهٔ از عمر صد حیف</p></div>
<div class="m2"><p>که جان فیض بیجان از جهانشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش آنکو گشت دلدارش دلارام</p></div>
<div class="m2"><p>غم جانانش جان افزای جان شد</p></div></div>