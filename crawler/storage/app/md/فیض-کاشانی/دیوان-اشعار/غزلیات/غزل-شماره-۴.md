---
title: >-
    غزل شمارهٔ ۴
---
# غزل شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>هر دمم نیشی ز خویشی میرسد با آشنا </p></div>
<div class="m2"><p>عمر شد در آشنائیها و خویشی ها هبا </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کینه ها در سینه ها دارند خویشان ازحسد</p></div>
<div class="m2"><p>آشنایان در پی گنجینه های عمرها </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ آزاری ندیدم هرگز از بیگانهٔ </p></div>
<div class="m2"><p>هر غمی کامد بدل از خویش بود و آشنا </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر دل را تیره گرداند چو خویشی بگذرد </p></div>
<div class="m2"><p>میزند بر دل بگد چون آشنا کرد آشنا </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویش میخواهد نباشد خویش بر روی زمین</p></div>
<div class="m2"><p>تا بریزد روزی آن بر سر این از سما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> چون سلامی می کند سنگیست بر دل میخورد </p></div>
<div class="m2"><p>بی سلام ار بگذرد بر جان خلد زان خارها </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راحتی مر آشنا را زآشنائی کم رسد </p></div>
<div class="m2"><p>نیست راضی آشنائی از سلوک آشنا </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوه کم کن فیض از یاران ودر خودکن نظر</p></div>
<div class="m2"><p>تا چگونه میکنی در بحر دلها آشنا </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر زمن پرسی زخویش و آشنا بیگانه شو</p></div>
<div class="m2"><p>با خدای خویش میباش آشنا و آشنا </p></div></div>