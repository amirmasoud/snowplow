---
title: >-
    غزل شمارهٔ ۱۴۲
---
# غزل شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>عشق در راه طلب راهبر مردانست </p></div>
<div class="m2"><p>وقت مستی و طرب بال و پر مردانست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفر آن نیست که از مصر ببغداد روی </p></div>
<div class="m2"><p>رفتن از جان سوی جانان سفر مردانست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظفر آن نیست که در معرکه غالب گردی </p></div>
<div class="m2"><p>از سرخویش گذشتن ظفر مردانست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنر آن نیست که در کسب و فضایل گوشی </p></div>
<div class="m2"><p>به پر عشق پریدن هنر مردانست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه دلهاست فسرده همه جانها تیره </p></div>
<div class="m2"><p>گرم و افروخته آه سحر مردانست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمهٔ کوثر و سرسبزی بستان بهشت </p></div>
<div class="m2"><p>خبری از اثر چشم تر مردانست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهر اشک ندامت بقیامت ریزد </p></div>
<div class="m2"><p>هر که در فکر شکست گهر مردانست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض اگر آب حیات از گهر نظم چکاند </p></div>
<div class="m2"><p>هم از آنروست که او خاک در مردانست </p></div></div>