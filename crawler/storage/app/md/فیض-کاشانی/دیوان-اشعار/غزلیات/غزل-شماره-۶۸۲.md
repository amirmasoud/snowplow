---
title: >-
    غزل شمارهٔ ۶۸۲
---
# غزل شمارهٔ ۶۸۲

<div class="b" id="bn1"><div class="m1"><p>بس جور کشیدیم در این ره که بریدیم</p></div>
<div class="m2"><p>المنة لله که بمقصود رسیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طی شد الم فرقت و برخواست غم از دل</p></div>
<div class="m2"><p>با دوست نشستیم و می وصل چشیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از علم یقین آمد و از کوش بآغوش</p></div>
<div class="m2"><p>دیدیم عنان آنچه بگفتار شنیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا صاف شود عیش ز آلایش عصیان</p></div>
<div class="m2"><p>با دوست یکی گشته سر مرگ بریدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس عقده مشکل که در این راه گشودیم</p></div>
<div class="m2"><p>بس گم شدگانرا که بفریاد رسیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با پای برفتند گروهی ره جنت</p></div>
<div class="m2"><p>ما با پر عرفان بره قدس پریدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر وحدت حق فاش و نهان داده شهادت</p></div>
<div class="m2"><p>تا ساغری از باده توحید چشیدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفان ولی را ز ره وحی گرفتیم</p></div>
<div class="m2"><p>فرمان نبی را بدل و جان گرویدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با پای دوم راه سفر رفت محبش</p></div>
<div class="m2"><p>ما سر به تبرهای تبرّاش بریدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قومی سپر خویش نمودند سوم را</p></div>
<div class="m2"><p>ما تیغ براءت بسر هر شه کشیدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون فیض رسیدیم بسر چشمه حیوان</p></div>
<div class="m2"><p>از مرگ رهیدیم و ز آفات جهیدیم</p></div></div>