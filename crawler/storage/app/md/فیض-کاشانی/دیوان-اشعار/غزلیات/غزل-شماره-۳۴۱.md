---
title: >-
    غزل شمارهٔ ۳۴۱
---
# غزل شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>صد جلوه کنی هر دم و دیدن نگذارند</p></div>
<div class="m2"><p>گل گل شکفد زان رخ و چیدن نگذارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ جمالت گل و ریحان فراوان</p></div>
<div class="m2"><p>یک مردم چشمی بچریدن نگذارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آرزوی آب حیات از لب لعلت</p></div>
<div class="m2"><p>لب تشنه بمردیم و مکیدن نگذارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشاق جگر سوخته داغ غمت را</p></div>
<div class="m2"><p>در حسن و جمالت نگریدن نگذارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرواز کند طایر جان سوی جنابت</p></div>
<div class="m2"><p>در آرزوی وصل و رسیدن نگذارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیهوده پر و بال معارف چه گشائیم</p></div>
<div class="m2"><p>در ساحت عزتو پریدن نگذارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرب تو و حرمان مرا تشنه لبی گفت</p></div>
<div class="m2"><p>نزدیک لب آرند و چشیدن نگذارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سر سویدای دل و رخ ننمایند</p></div>
<div class="m2"><p>در مردمک دیده دویدن نگذارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو در نظر و فیض ز دیدار تو محروم</p></div>
<div class="m2"><p>غرق می وصلیم و چشیدن نگذارند</p></div></div>