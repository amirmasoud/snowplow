---
title: >-
    غزل شمارهٔ ۸۳۱
---
# غزل شمارهٔ ۸۳۱

<div class="b" id="bn1"><div class="m1"><p>بار الها راستان را در حریمت بار ده</p></div>
<div class="m2"><p>جان آگاهی کرامت کن دل بیدار ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح پاکی را که شد آلودهٔ لوث گنه</p></div>
<div class="m2"><p>بادهٔ ناب طهور از جام استغفار ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واصلان را محو کن اندر جمال خویشتن</p></div>
<div class="m2"><p>سالکان را جان هشیار و دل بیدار ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکنظر کن در جهان آب و گل از روی لطف</p></div>
<div class="m2"><p>دوستان را گل برافشان دشمنان را خار ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل گل را روز روز از زور وزر معمور دار</p></div>
<div class="m2"><p>اهل دل را در دل شب نالهای زار ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل بی‌سیرتان آتش بر افروز از جحیم</p></div>
<div class="m2"><p>نیکوان را جان خرم چهرهٔ گلنار ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یکی را در وصالت عارض چون ارغوان</p></div>
<div class="m2"><p>و آن دگر را در فراقت دیده خونبار ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوستان را ده لوای عز و تاج افتخار</p></div>
<div class="m2"><p>دشمنان را ژندهٔ دل و لباس عار ده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکسی را هرچه می‌خواهد دلش آماده کن</p></div>
<div class="m2"><p>عاشقان را بار ده افسردگان را کار ده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فیض را چون ره نمودی سوی خود از روی لطف</p></div>
<div class="m2"><p>مرحمت فرما ز عشقش مرکب رهوار ده</p></div></div>