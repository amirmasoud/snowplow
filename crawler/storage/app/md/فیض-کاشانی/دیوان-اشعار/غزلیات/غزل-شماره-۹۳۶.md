---
title: >-
    غزل شمارهٔ ۹۳۶
---
# غزل شمارهٔ ۹۳۶

<div class="b" id="bn1"><div class="m1"><p>مرحبا ای نسیم عنبر بوی</p></div>
<div class="m2"><p>خبری از دیار یار بگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبر دیدیم در مقابل شوق</p></div>
<div class="m2"><p>آتش و پنبه بود و سنگ و سبوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشنهٔ وصل راست بیم هلاک</p></div>
<div class="m2"><p>پیش از آن کاید آب رفته بجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجر را هم نهایتی باید</p></div>
<div class="m2"><p>یار با یار کی کند یکروی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده طغرای بیوفائی ختم</p></div>
<div class="m2"><p>برده از خیل بی‌وفایان گوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل از آتش غمش صد داغ</p></div>
<div class="m2"><p>بررخ از آب دیده‌ام صدجوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من سرا پای درد و او فارغ</p></div>
<div class="m2"><p>بوده هرگز محبت از یکسوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر سرا پا ز غم شوم موئی</p></div>
<div class="m2"><p>ندهم زو بعالمی یکموی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> فیض بگذر ز وادی وصلش</p></div>
<div class="m2"><p>بنشین کنجی و زغم می موی</p></div></div>