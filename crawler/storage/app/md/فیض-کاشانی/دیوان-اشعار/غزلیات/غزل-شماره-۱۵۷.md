---
title: >-
    غزل شمارهٔ ۱۵۷
---
# غزل شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>بهر گلی اگرم ناله و نوائی هست </p></div>
<div class="m2"><p>بجان تو اگرم جز تو مدعائی هست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو مگو زکجا آمدی کجا رفتی </p></div>
<div class="m2"><p>ببین ببین که به جز سایه تو جائی هست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو مگو بجهان آشنا کرا داری </p></div>
<div class="m2"><p>ببین ببین بجهان جز تو آشنائی هست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بغیر هوای تو و رضای تو </p></div>
<div class="m2"><p>هوای دیگر اگر هست و مدعائی هست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوا بسر نرسانم بمدعا نرسم </p></div>
<div class="m2"><p>چه مدعا چه هوا جز تو روی ورائی هست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخاک درگه تو گر روم بجای دگر </p></div>
<div class="m2"><p>کجا روم به جز این آستانه جائی هست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقابل گل رویت نشینم و نالم </p></div>
<div class="m2"><p>چو عندلیب که در گلشن نوائی هست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصال دوست چو خواهی بساز با غم دوست </p></div>
<div class="m2"><p>چو گنج باشد ناچار اژدهائی هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر جهان همه بیگانه شد زفیض چه باک </p></div>
<div class="m2"><p>چو التفات نهان تو آشنائی هست </p></div></div>