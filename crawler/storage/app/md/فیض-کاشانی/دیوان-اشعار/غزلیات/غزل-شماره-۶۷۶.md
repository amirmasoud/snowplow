---
title: >-
    غزل شمارهٔ ۶۷۶
---
# غزل شمارهٔ ۶۷۶

<div class="b" id="bn1"><div class="m1"><p>بکوی یار بی‌پروا گذشتیم</p></div>
<div class="m2"><p>دل آنجا ماند و ما ز آنجا گذشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلط کی میتوان ز آنجا گذشتن</p></div>
<div class="m2"><p>مگر ما بیخود و بی ما گذشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه ما ماند و نه سر ماند و نه پا ماند</p></div>
<div class="m2"><p>هم از ما هم ز سر هم پا گذشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از یار حقیقی بوی بردیم</p></div>
<div class="m2"><p>ز هر گلدستهٔ رعنا گذشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیان دیدیم خورشید ازل را</p></div>
<div class="m2"><p>ز هر مه طلعت زیبا گذشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث از شاهد و ساقی مگوئید</p></div>
<div class="m2"><p>که این را خط زدیم و آنرا گذشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجان و دل غم مولی گزیدیم</p></div>
<div class="m2"><p>هم از دنیا هم از عقبا گذشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی‌پیچیم در زهاد و عباد</p></div>
<div class="m2"><p>هم از اینها هم از آنها گذشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه از دنیا و عقبا طرف بستیم</p></div>
<div class="m2"><p>بماندیم این دو را برجا گذشتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو در اقلیم بیجانی رسیدیم</p></div>
<div class="m2"><p>ز راه و منزل و ماوا گذشتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخلوت خانهٔ توحید رفتیم</p></div>
<div class="m2"><p>هم از لا و هم از الا گذشتیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل و جانرا بحق دادیم چون فیض</p></div>
<div class="m2"><p>ز گفت و گو و از غوغا گذشتیم</p></div></div>