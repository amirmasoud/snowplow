---
title: >-
    غزل شمارهٔ ۳۶۹
---
# غزل شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>غم فراق تو ای دوست بی‌شمار بود</p></div>
<div class="m2"><p>بدل چو غصه گره شد یکی هزار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهان کنم غم عشق تو را چو جانم سوخت</p></div>
<div class="m2"><p>که تا دلم بدرون شمع این مزار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هیچ چیز به جز وصل یار خوش نشود</p></div>
<div class="m2"><p>دل ار خوش است بغیری ببوی یار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا دلی که به جز حق بکس نگیرد انس</p></div>
<div class="m2"><p>اگر غمی رسدش دوست غمگسار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سینه می نگذارم که غم برون آید</p></div>
<div class="m2"><p>چو غمگسار بود دوست خوشگوار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درون سینه بدل راز خویش می‌گویم</p></div>
<div class="m2"><p>دمست پرده در او سینه رازدار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بآب و تاب چو آید برون ز دل سخنی</p></div>
<div class="m2"><p>بمعنی آتش و در صورت آبدار بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چه حد که کنم دعوی محبت و قرب</p></div>
<div class="m2"><p>بدر گهی که سر سروران بدار بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود عزیز ابد آنکرا دهی عزت</p></div>
<div class="m2"><p>نهی چو داغ مذلت همیشه خوار بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو لطف تو نبود سعی کس ندارد سود</p></div>
<div class="m2"><p>اگر صیام و قیامش یکی هزار بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دعا اثر نکند تا عنایتت نبود</p></div>
<div class="m2"><p>هزار اختر سعد ارچه در گذار بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر نخواسته باشی نجات عاصی را</p></div>
<div class="m2"><p>شفاعت شفعا را چه اعتبار بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدست خواهش تست اختیار مختار است</p></div>
<div class="m2"><p>چو تو نخواهی کس را چه اختیار بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلم چو سیر کند در حقایق ملکوت</p></div>
<div class="m2"><p>ز وعظ واعظی و شاعریم عار بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بمجلسی که شمارند اهل عرفان را</p></div>
<div class="m2"><p>ز فیض دم نتوان زد چه در شمار بود</p></div></div>