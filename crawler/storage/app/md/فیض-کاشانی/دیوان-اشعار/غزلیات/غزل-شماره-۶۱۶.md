---
title: >-
    غزل شمارهٔ ۶۱۶
---
# غزل شمارهٔ ۶۱۶

<div class="b" id="bn1"><div class="m1"><p>اگر آهی کشم دریا بسوزم</p></div>
<div class="m2"><p>و گر شوری کنم دریا بسوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود دل شعله چون بر یاد روئی</p></div>
<div class="m2"><p>شوم تن شمع و سر تا پا بسوزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنم هرچند پنهان آتش جان</p></div>
<div class="m2"><p>میان انجمن پیدا بسوزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشم با سوختن در آتش عشق</p></div>
<div class="m2"><p>بهل تامن در این سودا بسوزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آنجا هر که باشد هر چه باشد</p></div>
<div class="m2"><p>بسوزد ز آتشم هرجا بسوزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنم گر اقتباسی ز آتش قدس</p></div>
<div class="m2"><p>وجود خویش سر تا بسوزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیارم تاب یا آرم ندانم</p></div>
<div class="m2"><p>تجلی کنم بسازم تا بسوزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه من ماند نه ما ماند چو آئی</p></div>
<div class="m2"><p>بیا تا بی من و بی ما بسوزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا خواهم مرا گر تو نخواهی</p></div>
<div class="m2"><p>تجلی بیشتر کن تا بسوزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسوزد ظاهر و باطن ز سوزم</p></div>
<div class="m2"><p>اگر پیدا و گر پنهان بسوزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سوز جان اگر حرفی نویسم</p></div>
<div class="m2"><p>ورق را سربسر یکجا بسوزم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو فیض ار دم زنم از آتش دل</p></div>
<div class="m2"><p>زبان و کام با لبها بسوزم</p></div></div>