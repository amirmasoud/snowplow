---
title: >-
    غزل شمارهٔ ۴۱۰
---
# غزل شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>خدای عزوجل گر ببخشدم شاید</p></div>
<div class="m2"><p>سزای بندگیش چون ز من نمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر چه بستم جز حق شکسته باز آمد</p></div>
<div class="m2"><p>دل مرا به جز از یاد حق نمی‌شاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای توشهٔ عقبی بسی نمودم سعی</p></div>
<div class="m2"><p>ز من نیامد کاری که آن بکار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بیم آنکه مبادا خجل شود فردا</p></div>
<div class="m2"><p>دلم بطاعتی امروز می نیاساید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرفته‌ام بره حق چنانکه باید رفت</p></div>
<div class="m2"><p>نکرده هیچ عبادت چنانکه می‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر بهیچ ببخشند جرم هیچان را</p></div>
<div class="m2"><p>ز هیچ هیچ نیابد ز هیچ هیچ آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تمام روز درین غم بسر برم که صباح</p></div>
<div class="m2"><p>برای من شب آبستنم چه می‌زاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم رمید وز من بهتری نمی‌یابد</p></div>
<div class="m2"><p>اگر دو چار گردد بگوش باز آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث واعظ پر گو نه در خور فیض است</p></div>
<div class="m2"><p>بیا بخوان غزلی تا دلم بیاساید</p></div></div>