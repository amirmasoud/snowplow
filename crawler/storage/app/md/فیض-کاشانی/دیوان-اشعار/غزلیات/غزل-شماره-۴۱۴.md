---
title: >-
    غزل شمارهٔ ۴۱۴
---
# غزل شمارهٔ ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>زهد و تقوی ز من نمی‌آید</p></div>
<div class="m2"><p>میکنم آنچه عشق فرماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده‌ام خویش را بدو تسلیم</p></div>
<div class="m2"><p>میکند با من آنچه می‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکف عشق داده‌ام خود را</p></div>
<div class="m2"><p>کشدم خواه و خواه بخشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم بدم صور عشق در دل من</p></div>
<div class="m2"><p>عقدهٔ را به نفخه بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نفس از جهان جان دل را</p></div>
<div class="m2"><p>شاهدی تازه روی بنماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر صباحی بتازگی شوری</p></div>
<div class="m2"><p>شب آبست عاشقان زاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان فزون میشود ز شورش عشق</p></div>
<div class="m2"><p>تن اگر چه ز غصه فرساید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق تن گیرد و روان بخشد</p></div>
<div class="m2"><p>عشق کل کاهد و دل افزاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض هر دو ز غیب معنی بکر</p></div>
<div class="m2"><p>آورد نظم تازه ار آید</p></div></div>