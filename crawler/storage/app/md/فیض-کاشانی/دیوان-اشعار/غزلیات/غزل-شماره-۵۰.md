---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>پژمرده شد دل ز آلودگی‌ها</p></div>
<div class="m2"><p>کاری نکردم ز افسردگی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل برد از من گه این و گه آن</p></div>
<div class="m2"><p>عمرم هبا شد از سادگی‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند شستم دامان تقوی</p></div>
<div class="m2"><p>زایل نگردید آلودگی‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پا فتادم و از غم نرستم</p></div>
<div class="m2"><p>نگرفت دستم افتادگی‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین آشنایان خیری ندیدم</p></div>
<div class="m2"><p>خوش باد وقت بیگانگی‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سامان نخواهم ایوان نخواهم</p></div>
<div class="m2"><p>بیچارگی‌ها آوارگی‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای فیض بگسل از عقل و تدبیر</p></div>
<div class="m2"><p>بر عشق تن جان آشفتگی‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جمله تقصیر در بندگی‌ها</p></div>
<div class="m2"><p>رو آب شو از شرمندگی‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد حق منادی قل یا عبادی</p></div>
<div class="m2"><p>تو جان ندادی کو بندگی‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در راه یوسف کف‌ها بریدند</p></div>
<div class="m2"><p>ای در رهش گم زان پردگی‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد قیامت کو استقامت</p></div>
<div class="m2"><p>زین بندگی‌ها شرمندگی‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صوری دمیدند موتی شنیدند</p></div>
<div class="m2"><p>مرگست خوش‌تر زین زندگی‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کو عشق و زورش کو شر و شورش</p></div>
<div class="m2"><p>طرفی نبستم ز آسودگی‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از خود به در شو شوریده‌سر شو</p></div>
<div class="m2"><p>صحرای پهنی‌ست شوریدگی‌ها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای آنکه داری در سر غم عشق</p></div>
<div class="m2"><p>ارزانیت باد آشفتگی‌ها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا رب کجا شد عیش جوانی</p></div>
<div class="m2"><p>خوش عالمی بود آن کودگی‌ها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای فیض برخیز خاکی به سر ریز</p></div>
<div class="m2"><p>در ماتم آن آسودگی‌ها</p></div></div>