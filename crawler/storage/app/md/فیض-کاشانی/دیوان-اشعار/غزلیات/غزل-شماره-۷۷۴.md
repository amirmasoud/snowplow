---
title: >-
    غزل شمارهٔ ۷۷۴
---
# غزل شمارهٔ ۷۷۴

<div class="b" id="bn1"><div class="m1"><p>غم پنهان سمر شد چون کنم چون</p></div>
<div class="m2"><p>محبت پرده در شد چون کنم چون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگردابی فرو شد پای دل را</p></div>
<div class="m2"><p>که آب از سر بدر شد چون کنم چون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آنروزی که دل در دست من بود</p></div>
<div class="m2"><p>دل از دستم بدر شد چون کنم چون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجنبانید تا زنجیر زلفش</p></div>
<div class="m2"><p>جنونم بیشتر شد چون کنم چون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد در دلش تاثیر فریاد</p></div>
<div class="m2"><p>فغانم بی اثر شد چون کنم چون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چسان امید بهبودی توان داشت</p></div>
<div class="m2"><p>که کار از بدبتر شد چون کنم چون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتاده بر در دلها بلی فیض</p></div>
<div class="m2"><p>گدای در بدر شد چون کنم چون</p></div></div>