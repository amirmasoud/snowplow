---
title: >-
    غزل شمارهٔ ۸۷۲
---
# غزل شمارهٔ ۸۷۲

<div class="b" id="bn1"><div class="m1"><p>پرتوی از مهر رویت در جهان انداختی</p></div>
<div class="m2"><p>آتشی در خرمن شورید گان انداختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکنظر کردی بسوی دل ز چشم شاهدان</p></div>
<div class="m2"><p>زان نظر بس فتنها در جسم و جان انداختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دلم جا کردی و کردی مرا از من تهی</p></div>
<div class="m2"><p>تا مرا از هستی خود در گمان انداختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعله حسن تو دوش افروخت دلها را چو شمع</p></div>
<div class="m2"><p>این چه آتش بود کامشب در جهان انداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کنارم بودی و میسوخت جانم در میان</p></div>
<div class="m2"><p>آتش سوزان نهان چون در میان انداختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا قیامت قالبم خواهد طپید از ذوق آن</p></div>
<div class="m2"><p>تیر مژگان سوی من تا بیکمان انداختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده از خواب عدم نگشوده گردیدند مست</p></div>
<div class="m2"><p>چون ندای «کن» بگوش انس و جان انداختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی «او ادنی» روان گشتند مشتاقان وصل</p></div>
<div class="m2"><p>تا خطاب «ارجعی» در ملک و جان انداختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکسی پشت و پناه عالمی شد تا ز لطف</p></div>
<div class="m2"><p>سایهٔ خود بر سر این بیکسان انداختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد کنار همدمان دریای خون از اشگ فیض</p></div>
<div class="m2"><p>قصهٔ پر غصه‌اش تا در میان انداختی</p></div></div>