---
title: >-
    غزل شمارهٔ ۱۱۳
---
# غزل شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>یار ما گر میل صحرا میکند صحرا خوش است</p></div>
<div class="m2"><p>میل دریا گر کند در چشم ما دریا خوش است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نماید روی او خود رفتن دلها نکوست </p></div>
<div class="m2"><p>وربپوشد رخ زحسرت شور در سرها خوش است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وصالش چون نوازد مستی ما خوش بود </p></div>
<div class="m2"><p>در فراقش گر گدازد نالهای ما خوش است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه خواهد خاطرش ما آن شویم و آن کنیم </p></div>
<div class="m2"><p>هر کجا ما را دهد جا جای ما آنجا خوش است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدانرا زهد و تقوی عاقلانرا ننگ و نام </p></div>
<div class="m2"><p>عاشقانرا غمزهای یار بی پروا خوش است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقانرا باغ و بستان عارض جانان بود </p></div>
<div class="m2"><p>داغ سوداشان بجای لاله حمرا خوش است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که خواهی شور دریا آب چشم ما به بین </p></div>
<div class="m2"><p>درّو لعل از خون دل در قعر این دریا خوش است </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که هستی میفروشی در جهان جای تو خوش </p></div>
<div class="m2"><p>بی سر و پایان کوی نیستی را جا خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کرا چون فیض وحشت باشد از ابنای دهر </p></div>
<div class="m2"><p>گوش بسته لب خموش و چشم نابینا خوش است </p></div></div>