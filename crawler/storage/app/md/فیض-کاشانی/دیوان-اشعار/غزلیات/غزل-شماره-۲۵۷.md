---
title: >-
    غزل شمارهٔ ۲۵۷
---
# غزل شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>از بهر من شراب بوامی که می خرد </p></div>
<div class="m2"><p>مخموری از خمار بجامی که میخرد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زاهدی بدست من افتد فرو شمش </p></div>
<div class="m2"><p>تا می بدست آرم خامی که میخرد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین قوم عرض خود بسلامی توان خرید </p></div>
<div class="m2"><p>زیشان و لیک جان بسلامی که میخرد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن کیست عذرخواه شود رندی مرا </p></div>
<div class="m2"><p>از زاهدان مرا بکلامی که میخرد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو آنکه حرف خاص تواند بعام گفت </p></div>
<div class="m2"><p>جز بار خاص بنده عامی که می خرد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز یار سرو قد که دلم شد اسیر او </p></div>
<div class="m2"><p>آزاده چو من بخرامی که میخرد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز چشم مست او که رباید بغمزه هوش </p></div>
<div class="m2"><p>عیش مدام من بمدامی که میخرد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p> آن کیست کو بدوست رساند سلام من </p></div>
<div class="m2"><p>باری از و مرا بسلامی که میخرد </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن را که نامهٔ بمن آرد زیار من </p></div>
<div class="m2"><p>سرمیدهم سری به پیامی که میخرد </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کو زمن بجانب او نامهٔ برد </p></div>
<div class="m2"><p>او را شوم غلام غلامی که میخرد </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناکامی فراق تو جانا زحد گذشت </p></div>
<div class="m2"><p>از بهر من زوصل تو کامی که میخرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن کیست حال فیض بگوید بلطف او </p></div>
<div class="m2"><p>از قهر او مرا بکلامی که میخرد</p></div></div>