---
title: >-
    غزل شمارهٔ ۲۶۹
---
# غزل شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>ای مسلمانان مرا عشق جوانی پیر کرد</p></div>
<div class="m2"><p>پای دل را کافری در زلف خود زنجیر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی غلط، گردد جوان از عشق‌بازی اهل دل</p></div>
<div class="m2"><p>غم که باشد تا تواند عاشقانرا پیر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی غلط هم نیست سوزد مغز را در استخوان</p></div>
<div class="m2"><p>هم جوان هم پیر را از جان شیرین سیر کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بنی‌آدم چه میخواهند این قوم پری</p></div>
<div class="m2"><p>یا رب این بیداد خوبان را که بر ما چیر کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دچار من شده است ابرو کمانی در کمین</p></div>
<div class="m2"><p>بهر قصد جان من مژگان خود را تیر کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عزیزان با دل من نازنینانرا چه کار</p></div>
<div class="m2"><p>در شمار چیستم تا بایدم تسخیر کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی غلط کردم که اینان نیز چون من سخره‌اند</p></div>
<div class="m2"><p>پادشه عشق است معشوقی کجا تقصیر کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز اول پای دل را فیض میبایست بست</p></div>
<div class="m2"><p>کار چون از دست شد کی میتوان تدبیر کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بودنی چون بایدش بودن پشیمانی چه سود</p></div>
<div class="m2"><p>رو نماید آخرآن کاول قضا تقدیر کرد</p></div></div>