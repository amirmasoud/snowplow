---
title: >-
    غزل شمارهٔ ۲۰۵
---
# غزل شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>این جهان بی بقا هیچست هیچ </p></div>
<div class="m2"><p>هر چه میگردد فنا هیچست هیچ </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرجهانرا سر بسر بگرفته </p></div>
<div class="m2"><p>چون نمیماند بجا هیچست هیچ </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد مرا یک نکته از غیب آشکار </p></div>
<div class="m2"><p>در دو عالم جز خدا هیچست هیچ </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرنه سردر راه عشق او رود </p></div>
<div class="m2"><p>آن سرکمتر زپا هیچست هیچ </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه صرف طاعت و خدمت شود </p></div>
<div class="m2"><p>حاصل این عمرها هیچست هیچ </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرنه سوزد جان و دل در عشق او </p></div>
<div class="m2"><p>در تن این افسردها هیچست هیچ </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بعشق گلرخان ای دل مده </p></div>
<div class="m2"><p>مهر یار بی وفا هیچست هیچ </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبت بیگانگان بیگانگیست </p></div>
<div class="m2"><p>جز ندیدم آشنا هیچست هیچ </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر سخن گوئی دگر از حق بگو </p></div>
<div class="m2"><p>فیض جز حرف خدا هیچست هیچ </p></div></div>