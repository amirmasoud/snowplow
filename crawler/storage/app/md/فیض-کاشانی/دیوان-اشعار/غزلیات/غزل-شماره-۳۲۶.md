---
title: >-
    غزل شمارهٔ ۳۲۶
---
# غزل شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>چو من کسی که ره مستقیم میداند</p></div>
<div class="m2"><p>صفای صوفی و قدر حکیم میداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طریق اهل جدل جمله آفتست و علل</p></div>
<div class="m2"><p>ره سلامت قلب سلیم می‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چشم مست تو بر داشت نسخهٔ عارف</p></div>
<div class="m2"><p>و لیک منتسخش را سقیم میداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسیکه حسن تو دیده است و عشق فهمیده است</p></div>
<div class="m2"><p>مزاج طبع مرا مستقیم میداند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو عشق مظهر حسنست قدر من دانی</p></div>
<div class="m2"><p>از آنکه قدر گدا را کریم میداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رموز سر محبت حبیب می‌فهمد</p></div>
<div class="m2"><p>کنوز کنه سخن را کلیم میداند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدوست دارد امید و ز خویش دارد بیم</p></div>
<div class="m2"><p>کسی که معنی امید و بیم میداند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>براه مرگ روانست جاهل غافل</p></div>
<div class="m2"><p>مسافریست که خود را مقیم میداند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوی حق بود آهنگ عارف حق‌بین</p></div>
<div class="m2"><p>نه حزن باشد او را نه بیم می‌داند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی که لذت دیدار دوست را یابد</p></div>
<div class="m2"><p>نعیم هر دو جهان کی نعیم می‌داند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندیده است جمال و شنیده است نوال</p></div>
<div class="m2"><p>که ترک لذت دنیا عظیم می‌داند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان خوف و رجا زاهد است سر گردان</p></div>
<div class="m2"><p>دو دل شده دل خود را دو نیم میداند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگریه رفت ز خود فیض و طفل اشگش را</p></div>
<div class="m2"><p>حساب‌دان هم درّ یتیم میداند</p></div></div>