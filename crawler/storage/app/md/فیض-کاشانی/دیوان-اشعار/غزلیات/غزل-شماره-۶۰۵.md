---
title: >-
    غزل شمارهٔ ۶۰۵
---
# غزل شمارهٔ ۶۰۵

<div class="b" id="bn1"><div class="m1"><p>خم ابروی تو محراب رکوع است و سجودم</p></div>
<div class="m2"><p>بی‌خیال تو نباشد نه قیامم نه قعودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه حسن تو دیدم طمع از خویش بریدم</p></div>
<div class="m2"><p>تا که شد محو در انوار وجود تو وجودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکند تازه بتازه سپه حسن شهیدم</p></div>
<div class="m2"><p>چشم و ابرو و لب و خال و خط تست شهودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیر مهرت بازل داده مرا دایه لطفت</p></div>
<div class="m2"><p>نرود تا با بد مهر تو بیرون ز وجودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو در عیشم و عشرت همه سودم همه نورم</p></div>
<div class="m2"><p>بی تو در رنجم و محنت همه آهم همه دودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود همه فقرم و حاجت همه بخلم همه حاجت</p></div>
<div class="m2"><p>ز تو بخشایش وجودم ز تو سرمایه و سودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جاهل و مرده بخود زنده و دانا بتو باشم</p></div>
<div class="m2"><p>بخودم هیچ نباشم بتو باشم همه بودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکدم ار بگذردم بیتو سراپای زیانم</p></div>
<div class="m2"><p>بگذرانم نفسی با تو سراسر همه سودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی بر رهگذر دوست باخلاص نهادم</p></div>
<div class="m2"><p>بر ملک منزلت خویش بدینگونه فزودم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچه را علم گمان داشتم از سینه ستردم</p></div>
<div class="m2"><p>عقده جهل بلا حول ولا قوه گشودم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ بودم بخودم بود چو پندار وجودی</p></div>
<div class="m2"><p>همه کشتم چو شدم بیخبر از بود و نبودم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توبه کردم ز خود و نامه اعمال دریدم</p></div>
<div class="m2"><p>نیک اگر کشتم و گر بد همه را نیک درودم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاشق ورندم و میخواره بگلبانگ علا لا</p></div>
<div class="m2"><p>زاهد ار نیست چنین بنده چنینم که نمودم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر بسر خواب پریشان بود این عالم فانی</p></div>
<div class="m2"><p>بهر جمعیت دل نالهٔ بیهوده سرودم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فیض را نعمت بسیار چو دادی مددی کن</p></div>
<div class="m2"><p>تا کند شکر عطایای تو بر رغم حسودم</p></div></div>