---
title: >-
    غزل شمارهٔ ۵۵۷
---
# غزل شمارهٔ ۵۵۷

<div class="b" id="bn1"><div class="m1"><p>گذر کن ز بیغولهٔ نام و ننگ</p></div>
<div class="m2"><p>بشه راه مردان درآبی درنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسوم سفیهان ابله بمان</p></div>
<div class="m2"><p>که رسم سفیهان کند کار تنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراخست و هموار راه خرد</p></div>
<div class="m2"><p>در اینراه نه خار باشد نه سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدست آوری گر تو میزان عقل</p></div>
<div class="m2"><p>نباشد ترا با خود و غیر جنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آهنگ جان تو آرد هوا</p></div>
<div class="m2"><p>به حبل هوای خدا زن تو چنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوس بر سرت چون نزول آورد</p></div>
<div class="m2"><p>فرو بر هوس را بدم چون نهنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بقدر ضرورت ز دنیا بگیر</p></div>
<div class="m2"><p>مکن بار بر خود گران و ملنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمی مال افزونی راحت است</p></div>
<div class="m2"><p>کمی جاه آسایش از نام و ننگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پذیرفتی این نکته را گرچه فیض</p></div>
<div class="m2"><p>وگرنه سر خالی از عقل و سنگ</p></div></div>