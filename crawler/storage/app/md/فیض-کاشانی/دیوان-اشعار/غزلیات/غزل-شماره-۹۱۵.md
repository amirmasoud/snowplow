---
title: >-
    غزل شمارهٔ ۹۱۵
---
# غزل شمارهٔ ۹۱۵

<div class="b" id="bn1"><div class="m1"><p>دل و جانم اسیر غم تا کی</p></div>
<div class="m2"><p>خستهٔ محنت و الم تا کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر را صرف هرزه کردن چند</p></div>
<div class="m2"><p>مایهٔ حسرت و ندم تا کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم از فکرهای بیهوده</p></div>
<div class="m2"><p>دایم الحزن و النقم تا کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش بی‌اصل آرزو و امل</p></div>
<div class="m2"><p>بر دل و جان رقم زدن تا کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محنت رنج تو بتو تا چند</p></div>
<div class="m2"><p>غصه و درد دمبدم تا کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردها منتج پشیمانی</p></div>
<div class="m2"><p>گفتها مورث ندم تا کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره دین و در طریق هدی</p></div>
<div class="m2"><p>اعمی و ابکم و اصم تا کی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان علوی بقید تن تا چند</p></div>
<div class="m2"><p>دشمنان شاد و محترم تا کی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ان حق تا بچند خار و سبک</p></div>
<div class="m2"><p>و ان باطل ولی نعم تا کی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غفلت از یاد ‌آخرت تا چند</p></div>
<div class="m2"><p>غم دنیا و بیش و کم تا کی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حرف جمشید و تخت کی تا چند</p></div>
<div class="m2"><p>یاد آفرید و جام جم تا کی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتن حرف‌های بیهوده</p></div>
<div class="m2"><p>به نواهای زیر و بم تا کی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیش ازین شاعری مکن ای فیض</p></div>
<div class="m2"><p>این سخنهای کم ز کم تا کی</p></div></div>