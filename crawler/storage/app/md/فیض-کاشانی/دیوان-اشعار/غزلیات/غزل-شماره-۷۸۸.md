---
title: >-
    غزل شمارهٔ ۷۸۸
---
# غزل شمارهٔ ۷۸۸

<div class="b" id="bn1"><div class="m1"><p>ای صبا با یار سنگین دل بگو</p></div>
<div class="m2"><p>چون رسانیدی سلام من بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستحقم من زکوه حسن را</p></div>
<div class="m2"><p>لن تنالوا البر حتی تنفقو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من اگر هرگز نیایم بر درت</p></div>
<div class="m2"><p>تو نگوئی که گدائی بود کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بمیرم در غم عشق تو من</p></div>
<div class="m2"><p>تو نخواهی کردم آخر جستجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو مروت کو وفا کو مرحمت</p></div>
<div class="m2"><p>حق خدمتها چه شد انصاف کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر راهت فتم وز خود روم</p></div>
<div class="m2"><p>تو نگوئی کوست این با خاک کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من گرفتم نیستت مهر و وفا</p></div>
<div class="m2"><p>باری از روی جفا حرفی بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر سلاممرا نمیگوئی علیک</p></div>
<div class="m2"><p>در جواب بنده دشنامی بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل من چاکها کردی بعمد</p></div>
<div class="m2"><p>وز خطا هرگز نکردی یک رفو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرسشی هرگز نکردی بنده را</p></div>
<div class="m2"><p>در قفاهم بگذریم از رو برو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آهن سردی مکوب ای فیض رو</p></div>
<div class="m2"><p>زینسخن بگذر رها کن گفتگو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آرزوی من بود این بعد از این</p></div>
<div class="m2"><p>گر نباشد بعد از اینم آرزو</p></div></div>