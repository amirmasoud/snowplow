---
title: >-
    غزل شمارهٔ ۵۹۱
---
# غزل شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>بیاد منزل سلمی بر اطلال و دمن گردم</p></div>
<div class="m2"><p>ببوی آن گل رعنا بر اطراف چمن گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش من برفت او با دل صد جای ریش من</p></div>
<div class="m2"><p>ز حسرت در فراقش چون غریبان در وطن گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیابم زو اثر هر چند کوه و دشت پیمایم</p></div>
<div class="m2"><p>نگوید زو خبر هرچند گرد مرد و زن گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه پیکی میرسد ز آن کو نه بادی میوزد زانسو</p></div>
<div class="m2"><p>بهر سو هر دم آرم رو بگرد خویشتن گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو می نگذاردم غیرت که نامش بر زبان آرم</p></div>
<div class="m2"><p>چسان در جستجوی او میان انجمن گودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیالش چون ببر گیرم ز سر تا پای گردم او</p></div>
<div class="m2"><p>ز خود بیرون روم از خویشتن بیخویشتن گودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدش را چون بیاد آرم تو گوئی سرو شمشادم</p></div>
<div class="m2"><p>رخش چون در خیال آرم شوم گل نسترن گردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث زلف و گیسویش کنم در انجمن چون من</p></div>
<div class="m2"><p>جهانی را بدام آرم کمند مرد و زن گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خالش در نظر آرم سراسر نافه مشکم</p></div>
<div class="m2"><p>مزاج آهوان گیرم بصحرای ختن گردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو چشمش در نظر آرم گهی بیمار و گه مستم</p></div>
<div class="m2"><p>در آن مستی شوم صیاد صید خویشتن گردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لبش چون در ضمیر آرم یکی ساغر شوم پر می</p></div>
<div class="m2"><p>ز دندانش چو یاد آرم همه درّ عدن گردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بفکر آن دهان چون اوفتم اثباتم و نفیم</p></div>
<div class="m2"><p>محالی را کنم جا بر محل صید سخن گردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حدیث آن میان چون در میان آید شوم موئی</p></div>
<div class="m2"><p>ندانم نیستم هستم میان شک و ظن گردم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو دور از کار می‌بویم بهرجا فیض بیهوده</p></div>
<div class="m2"><p>بیا بهر سراغ دوست گرد خویش گردم</p></div></div>