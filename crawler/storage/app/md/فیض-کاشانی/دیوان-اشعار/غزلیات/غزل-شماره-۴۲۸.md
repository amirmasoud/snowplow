---
title: >-
    غزل شمارهٔ ۴۲۸
---
# غزل شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>دیده از نور جمال دوست چون بینا کنید</p></div>
<div class="m2"><p>سر بلندان گوشه چشمی بسوی ما کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوجوانان چون بیاد نرگسش نوشیدمی</p></div>
<div class="m2"><p>اول هر جرعهٔ یاد من شیدا کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شب زلف نگار دل فریبی گشت گم</p></div>
<div class="m2"><p>بهر من روزی دل گم گشتهٔ پیدا کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی نظارهٔ دیوانگان دادند عقل</p></div>
<div class="m2"><p>در گذشتن ای پری‌رویان سری بالا کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل پر غصه ما تا گره‌ها وا شود</p></div>
<div class="m2"><p>خوب‌رویان یک بیک بند قباها وا کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بتنگ آمد مرا از نام و ننگ عاقلان</p></div>
<div class="m2"><p>یار بی‌مستان مرا در عاشقی رسوا کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض میخواهد که با مستان کند هم مشربی</p></div>
<div class="m2"><p>بر در میخانه آمد بهر او در وا کنید</p></div></div>