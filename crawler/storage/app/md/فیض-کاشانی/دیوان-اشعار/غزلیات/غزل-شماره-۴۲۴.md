---
title: >-
    غزل شمارهٔ ۴۲۴
---
# غزل شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>گذشت موسم غم فصل وصل یار رسید</p></div>
<div class="m2"><p>نوای دلکش بلبل به نوبهار رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحاب خرمی آبی بر وی کار آورد</p></div>
<div class="m2"><p>نوید عیش بفریاد روزگار رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شگفته شد گل سوری فلک بهوش آمد</p></div>
<div class="m2"><p>بیار می بملک مستی هزار رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا پیام وصالی ز کوی یار آورد</p></div>
<div class="m2"><p>شفا بخسته قراری به بی‌قرار رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قرار گیر دلا مایهٔ قرار آمد</p></div>
<div class="m2"><p>کنار باز کن ای جان که آن نگار رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب فراق به صبح وصال انجامید</p></div>
<div class="m2"><p>شکفته شو چو گل ای دل که گلعذار رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراق دیدهٔ مخمور از شراب وصال</p></div>
<div class="m2"><p>ز لعل یار به صهبای خوشگوار رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپای لنگ و دل تنگ رفتم این ره را</p></div>
<div class="m2"><p>دلم بیار و روانم بدان دیار رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی جفا که ز اغیار بر دلم آمد</p></div>
<div class="m2"><p>کنون نماند غمی یار غمگسار رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آنچه خواست دلم شد بمدّعا حاصل</p></div>
<div class="m2"><p>هزار شکر به امید امیدوار رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دعای نیمشب فیض را که رد می‌شد</p></div>
<div class="m2"><p>کنون اجابتی از لطف کردگار رسید</p></div></div>