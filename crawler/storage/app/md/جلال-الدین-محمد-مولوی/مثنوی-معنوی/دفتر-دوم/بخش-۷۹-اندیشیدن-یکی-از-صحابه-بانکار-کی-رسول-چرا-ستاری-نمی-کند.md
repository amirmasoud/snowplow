---
title: >-
    بخش ۷۹ - اندیشیدن یکی از صحابه بانکار کی رسول چرا ستاری نمی‌کند
---
# بخش ۷۹ - اندیشیدن یکی از صحابه بانکار کی رسول چرا ستاری نمی‌کند

<div class="b" id="bn1"><div class="m1"><p>تا یکی یاری ز یاران رسول</p></div>
<div class="m2"><p>در دلش انکار آمد زان نکول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چنین پیران با شیب و وقار</p></div>
<div class="m2"><p>می‌کندشان این پیمبر شرمسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو کرم کو سترپوشی کو حیا</p></div>
<div class="m2"><p>صد هزاران عیب پوشند انبیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز در دل زود استغفار کرد</p></div>
<div class="m2"><p>تا نگردد ز اعتراض او روی‌زرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شومی یاری اصحاب نفاق</p></div>
<div class="m2"><p>کرد مؤمن را چو ایشان زشت و عاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز می‌زارید کای علام سر</p></div>
<div class="m2"><p>مر مرا مگذار بر کفران مصر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به دستم نیست همچون دید چشم</p></div>
<div class="m2"><p>ورنه دل را سوزمی این دم ز خشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندرین اندیشه خوابش در ربود</p></div>
<div class="m2"><p>مسجد ایشانش پر سرگین نمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنگهاش اندر حدث جای تباه</p></div>
<div class="m2"><p>می‌دمید از سنگها دود سیاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دود در حلقش شد و حلقش بخست</p></div>
<div class="m2"><p>از نهیب دود تلخ از خواب جست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در زمان در رو فتاد و می‌گریست</p></div>
<div class="m2"><p>کای خدا اینها نشان منکریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خلم بهتر از چنین حلم ای خدا</p></div>
<div class="m2"><p>که کند از نور ایمانم جدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بکاوی کوشش اهل مجاز</p></div>
<div class="m2"><p>تو بتو گنده بود همچون پیاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر یکی از یکدگر بی مغزتر</p></div>
<div class="m2"><p>صادقان را یک ز دیگر نغزتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صد کمر آن قوم بسته بر قبا</p></div>
<div class="m2"><p>بهر هدم مسجد اهل قبا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو آن اصحاب فیل اندر حبش</p></div>
<div class="m2"><p>کعبه‌ای کردند حق آتش زدش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قصد کعبه ساختند از انتقام</p></div>
<div class="m2"><p>حالشان چون شد فرو خوان از کلام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مر سیه‌رویان دین را خود جهاز</p></div>
<div class="m2"><p>نیست الا حیلت و مکر و ستیز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر صحابی دید زان مسجد عیان</p></div>
<div class="m2"><p>واقعه تا شد یقینشان سر آن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>واقعات ار باز گویم یک بیک</p></div>
<div class="m2"><p>پس یقین گردد صفا بر اهل شک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لیک می‌ترسم ز کشف رازشان</p></div>
<div class="m2"><p>نازنینانند و زیبد نازشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شرع بی تقلید می‌پذرفته‌اند</p></div>
<div class="m2"><p>بی محک آن نقد را بگرفته‌اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حکمت قرآن چو ضالهٔمؤمنست</p></div>
<div class="m2"><p>هر کسی در ضالهٔ خود موقنست</p></div></div>