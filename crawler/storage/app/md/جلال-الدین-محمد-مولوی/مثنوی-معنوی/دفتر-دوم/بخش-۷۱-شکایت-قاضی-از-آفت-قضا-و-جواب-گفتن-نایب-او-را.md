---
title: >-
    بخش ۷۱ - شکایت قاضی از آفت قضا و جواب گفتن نایب او را
---
# بخش ۷۱ - شکایت قاضی از آفت قضا و جواب گفتن نایب او را

<div class="b" id="bn1"><div class="m1"><p>قاضیی بنشاندند و می‌گریست</p></div>
<div class="m2"><p>گفت نایب قاضیا گریه ز چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نه وقت گریه و فریاد تست</p></div>
<div class="m2"><p>وقت شادی و مبارک‌باد تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت اه چون حکم راند بی‌دلی</p></div>
<div class="m2"><p>در میان آن دو عالم جاهلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دو خصم از واقعهٔ خود واقفند</p></div>
<div class="m2"><p>قاضی مسکین چه داند زان دو بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جاهلست و غافلست از حالشان</p></div>
<div class="m2"><p>چون رود در خونشان و مالشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت خصمان عالم‌اند و علتی</p></div>
<div class="m2"><p>جاهلی تو لیک شمع ملتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانک تو علت نداری در میان</p></div>
<div class="m2"><p>آن فراغت هست نور دیدگان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وان دو عالم را غرضشان کور کرد</p></div>
<div class="m2"><p>علمشان را علت اندر گور کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهل را بی‌علتی عالم کند</p></div>
<div class="m2"><p>علم را علت کژ و ظالم کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا تو رشوت نستدی بیننده‌ای</p></div>
<div class="m2"><p>چون طمع کردی ضریر و بنده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از هوا من خوی را وا کرده‌ام</p></div>
<div class="m2"><p>لقمه‌های شهوتی کم خورده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چاشنی‌گیر دلم شد با فروغ</p></div>
<div class="m2"><p>راست را داند حقیقت از دروغ</p></div></div>