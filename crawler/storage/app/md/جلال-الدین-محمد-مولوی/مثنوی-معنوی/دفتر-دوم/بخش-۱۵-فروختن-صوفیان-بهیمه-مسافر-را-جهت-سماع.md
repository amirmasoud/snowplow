---
title: >-
    بخش ۱۵ - فروختن صوفیان بهیمهٔ مسافر را جهت سماع
---
# بخش ۱۵ - فروختن صوفیان بهیمهٔ مسافر را جهت سماع

<div class="b" id="bn1"><div class="m1"><p>صوفیی در خانقاه از ره رسید</p></div>
<div class="m2"><p>مرکب خود برد و در آخر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبکش داد و علف از دست خویش</p></div>
<div class="m2"><p>نه چنان صوفی که ما گفتیم پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احتیاطش کرد از سهو و خباط</p></div>
<div class="m2"><p>چون قضا آید چه سودست احتیاط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صوفیان تقصیر بودند و فقیر</p></div>
<div class="m2"><p>کاد فقراً ان یکن کفراً یبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای توانگر که تو سیری هین مخند</p></div>
<div class="m2"><p>بر کژی آن فقیر دردمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر تقصیر آن صوفی رمه</p></div>
<div class="m2"><p>خرفروشی در گرفتند آن همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کز ضرورت هست مرداری مباح</p></div>
<div class="m2"><p>بس فسادی کز ضرورت شد صلاح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم در آن دم آن خرک بفروختند</p></div>
<div class="m2"><p>لوت آوردند و شمع افروختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولوله افتاد اندر خانقه</p></div>
<div class="m2"><p>کامشبان لوت و سماعست و وله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند ازین صبر و ازین سه روزه چند</p></div>
<div class="m2"><p>چند ازین زنبیل و این دریوزه چند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما هم از خلقیم و جان داریم ما</p></div>
<div class="m2"><p>دولت امشب میهمان داریم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تخم باطل را از آن می‌کاشتند</p></div>
<div class="m2"><p>کانک آن جان نیست جان پنداشتند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان مسافر نیز از راه دراز</p></div>
<div class="m2"><p>خسته بود و دید آن اقبال و ناز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صوفیانش یک بیک بنواختند</p></div>
<div class="m2"><p>نرد خدمتهای خوش می‌باختند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت چون می‌دید میلانش بوی</p></div>
<div class="m2"><p>گر طرب امشب نخواهم کرد کی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لوت خوردند و سماع آغاز کرد</p></div>
<div class="m2"><p>خانقه تا سقف شد پر دود و گرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دود مطبخ گرد آن پا کوفتن</p></div>
<div class="m2"><p>ز اشتیاق و وجد جان آشوفتن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گاه دست‌افشان قدم می‌کوفتند</p></div>
<div class="m2"><p>گه به سجده صفه را می‌روفتند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیر یابد صوفی آز از روزگار</p></div>
<div class="m2"><p>زان سبب صوفی بود بسیارخوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جز مگر آن صوفیی کز نور حق</p></div>
<div class="m2"><p>سیر خورد او فارغست از ننگ دق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از هزاران اندکی زین صوفیند</p></div>
<div class="m2"><p>باقیان در دولت او می‌زیند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون سماع آمد ز اول تا کران</p></div>
<div class="m2"><p>مطرب آغازید یک ضرب گران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خر برفت و خر برفت آغاز کرد</p></div>
<div class="m2"><p>زین حرارت جمله را انباز کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زین حرارت پای‌کوبان تا سحر</p></div>
<div class="m2"><p>کف‌زنان خر رفت و خر رفت ای پسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از ره تقلید آن صوفی همین</p></div>
<div class="m2"><p>خر برفت آغاز کرد اندر حنین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون گذشت آن نوش و جوش و آن سماع</p></div>
<div class="m2"><p>روز گشت و جمله گفتند الوداع</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خانقه خالی شد و صوفی بماند</p></div>
<div class="m2"><p>گرد از رخت آن مسافر می‌فشاند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رخت از حجره برون آورد او</p></div>
<div class="m2"><p>تا بخر بر بندد آن همراه‌جو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا رسد در همرهان او می‌شتافت</p></div>
<div class="m2"><p>رفت در آخر خر خود را نیافت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت آن خادم به آبش برده است</p></div>
<div class="m2"><p>زانک خر دوش آب کمتر خورده است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خادم آمد گفت صوفی خر کجاست</p></div>
<div class="m2"><p>گفت خادم ریش بین جنگی بخاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت من خر را به تو بسپرده‌ام</p></div>
<div class="m2"><p>من ترا بر خر موکل کرده‌ام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از تو خواهم آنچ من دادم به تو</p></div>
<div class="m2"><p>باز ده آنچ فرستادم به تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بحث با توجیه کن حجت میار</p></div>
<div class="m2"><p>آنچ من بسپردمت وا پس سپار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت پیغمبر که دستت هر چه برد</p></div>
<div class="m2"><p>بایدش در عاقبت وا پس سپرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ور نه‌ای از سرکشی راضی بدین</p></div>
<div class="m2"><p>نک من و تو خانهٔ قاضی دین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفت من مغلوب بودم صوفیان</p></div>
<div class="m2"><p>حمله آوردند و بودم بیم جان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو جگربندی میان گربگان</p></div>
<div class="m2"><p>اندر اندازی و جویی زان نشان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در میان صد گرسنه گرده‌ای</p></div>
<div class="m2"><p>پیش صد سگ گربهٔ پژمرده‌ای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفت گیرم کز تو ظلما بستدند</p></div>
<div class="m2"><p>قاصد خون من مسکین شدند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو نیایی و نگویی مر مرا</p></div>
<div class="m2"><p>که خرت را می‌برند ای بی‌نوا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا خر از هر که بود من وا خرم</p></div>
<div class="m2"><p>ورنه توزیعی کنند ایشان زرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>صد تدارک بود چون حاضر بدند</p></div>
<div class="m2"><p>این زمان هر یک به اقلیمی شدند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من که را گیرم که را قاضی برم</p></div>
<div class="m2"><p>این قضا خود از تو آمد بر سرم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون نیایی و نگویی ای غریب</p></div>
<div class="m2"><p>پیش آمد این چنین ظلمی مهیب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفت والله آمدم من بارها</p></div>
<div class="m2"><p>تا ترا واقف کنم زین کارها</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو همی‌گفتی که خر رفت ای پسر</p></div>
<div class="m2"><p>از همه گویندگان با ذوق‌تر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>باز می‌گشتم که او خود واقفست</p></div>
<div class="m2"><p>زین قضا راضیست مردی عارفست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفت آن را جمله می‌گفتند خوش</p></div>
<div class="m2"><p>مر مرا هم ذوق آمد گفتنش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مر مرا تقلیدشان بر باد داد</p></div>
<div class="m2"><p>که دو صد لعنت بر آن تقلید باد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خاصه تقلید چنین بی‌حاصلان</p></div>
<div class="m2"><p>خشم ابراهیم با بر آفلان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عکس ذوق آن جماعت می‌زدی</p></div>
<div class="m2"><p>وین دلم زان عکس ذوقی می‌شدی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عکس چندان باید از یاران خوش</p></div>
<div class="m2"><p>که شوی از بحر بی‌عکس آب‌کش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عکس کاول زد تو آن تقلید دان</p></div>
<div class="m2"><p>چون پیاپی شد شود تحقیق آن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا نشد تحقیق از یاران مبر</p></div>
<div class="m2"><p>از صدف مگسل نگشت آن قطره در</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>صاف خواهی چشم و عقل و سمع را</p></div>
<div class="m2"><p>بر دران تو پرده‌های طمع را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زانک آن تقلید صوفی از طمع</p></div>
<div class="m2"><p>عقل او بر بست از نور و لمع</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>طمع لوت و طمع آن ذوق و سماع</p></div>
<div class="m2"><p>مانع آمد عقل او را ز اطلاع</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گر طمع در آینه بر خاستی</p></div>
<div class="m2"><p>در نفاق آن آینه چون ماستی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر ترازو را طمع بودی به مال</p></div>
<div class="m2"><p>راست کی گفتی ترازو وصف حال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هر نبیی گفت با قوم از صفا</p></div>
<div class="m2"><p>من نخواهم مزد پیغام از شما</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من دلیلم حق شما را مشتری</p></div>
<div class="m2"><p>داد حق دلالیم هر دو سری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چیست مزد کار من دیدار یار</p></div>
<div class="m2"><p>گرچه خود بوبکر بخشد چل هزار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چل هزار او نباشد مزد من</p></div>
<div class="m2"><p>کی بود شبه شبه در عدن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یک حکایت گویمت بشنو بهوش</p></div>
<div class="m2"><p>تا بدانی که طمع شد بند گوش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هر که را باشد طمع الکن شود</p></div>
<div class="m2"><p>با طمع کی چشم و دل روشن شود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>پیش چشم او خیال جاه و زر</p></div>
<div class="m2"><p>همچنان باشد که موی اندر بصر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>جز مگر مستی که از حق پر بود</p></div>
<div class="m2"><p>گرچه بدهی گنجها او حر بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هر که از دیدار برخوردار شد</p></div>
<div class="m2"><p>این جهان در چشم او مردار شد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>لیک آن صوفی ز مستی دور بود</p></div>
<div class="m2"><p>لاجرم در حرص او شبکور بود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>صد حکایت بشنود مدهوش حرص</p></div>
<div class="m2"><p>در نیاید نکته‌ای در گوش حرص</p></div></div>