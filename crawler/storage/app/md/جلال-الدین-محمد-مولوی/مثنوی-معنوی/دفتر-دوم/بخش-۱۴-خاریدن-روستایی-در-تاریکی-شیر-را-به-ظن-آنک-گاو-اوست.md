---
title: >-
    بخش ۱۴ - خاریدن روستایی در تاریکی شیر را به ظن آنک گاو اوست
---
# بخش ۱۴ - خاریدن روستایی در تاریکی شیر را به ظن آنک گاو اوست

<div class="b" id="bn1"><div class="m1"><p>روستایی گاو در آخر ببست</p></div>
<div class="m2"><p>شیر گاوش خورد و بر جایش نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روستایی شد در آخر سوی گاو</p></div>
<div class="m2"><p>گاو را می‌جست شب آن کنج‌کاو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست می‌مالید بر اعضای شیر</p></div>
<div class="m2"><p>پشت و پهلو گاه بالا گاه زیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت شیر از روشنی افزون شدی</p></div>
<div class="m2"><p>زهره‌اش بدریدی و دل خون شدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چنین گستاخ زان می‌خاردم</p></div>
<div class="m2"><p>کو درین شب گاو می‌پنداردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق همی‌گوید که ای مغرور کور</p></div>
<div class="m2"><p>نه ز نامم پاره پاره گشت طور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که لو انزلنا کتابا للجبل</p></div>
<div class="m2"><p>لانصدع ثم انقطع ثم ارتحل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از من ار کوه احد واقف بدی</p></div>
<div class="m2"><p>پاره گشتیّ و دلش پرخون شدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پدر وز مادر این بشنیده‌ای</p></div>
<div class="m2"><p>لاجرم غافل درین پیچیده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر تو بی‌تقلید ازین واقف شوی</p></div>
<div class="m2"><p>بی نشان از لطف چون هاتف شوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بشنو این قصه پی تهدید را</p></div>
<div class="m2"><p>تا بدانی آفت تقلید را</p></div></div>