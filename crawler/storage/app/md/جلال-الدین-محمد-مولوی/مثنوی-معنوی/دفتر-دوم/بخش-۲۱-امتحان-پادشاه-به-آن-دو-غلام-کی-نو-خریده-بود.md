---
title: >-
    بخش ۲۱ - امتحان پادشاه به آن دو غلام کی نو خریده بود
---
# بخش ۲۱ - امتحان پادشاه به آن دو غلام کی نو خریده بود

<div class="b" id="bn1"><div class="m1"><p>پادشاهی دو غلام ارزان خرید</p></div>
<div class="m2"><p>با یکی زان دو سخن گفت و شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافتش زیرک‌دل و شیرین جواب</p></div>
<div class="m2"><p>از لب شکر چه زاید شکرآب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آدمی مخفیست در زیر زبان</p></div>
<div class="m2"><p>این زبان پرده‌ست بر درگاه جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونک بادی پرده را در هم کشید</p></div>
<div class="m2"><p>سر صحن خانه شد بر ما پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاندر آن خانه گهر یا گندمست</p></div>
<div class="m2"><p>گنج زر یا جمله مار و کزدمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا درو گنجست و ماری بر کران</p></div>
<div class="m2"><p>زانک نبود گنج زر بی پاسبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تامل او سخن گفتی چنان</p></div>
<div class="m2"><p>کز پس پانصد تامل دیگران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتیی در باطنش دریاستی</p></div>
<div class="m2"><p>جمله دریا گوهر گویاستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نور هر گوهر کزو تابان شدی</p></div>
<div class="m2"><p>حق و باطل را ازو فرقان شدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور فرقان فرق کردی بهر ما</p></div>
<div class="m2"><p>ذره ذره حق و باطل را جدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نور گوهر نور چشم ما شدی</p></div>
<div class="m2"><p>هم سؤال و هم جواب از ما بدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم کژ کردی دو دیدی قرص ماه</p></div>
<div class="m2"><p>چون سؤالست این نظر در اشتباه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راست گردان چشم را در ماهتاب</p></div>
<div class="m2"><p>تا یکی بینی تو مه را نک جواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فکرتت که کژ مبین نیکو نگر</p></div>
<div class="m2"><p>هست هم نور و شعاع آن گهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر جوابی کان ز گوش آید بدل</p></div>
<div class="m2"><p>چشم گفت از من شنو آن را بهل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گوش دلاله‌ست و چشم اهل وصال</p></div>
<div class="m2"><p>چشم صاحب حال و گوش اصحاب قال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در شنود گوش تبدیل صفات</p></div>
<div class="m2"><p>در عیان دیده‌ها تبدیل ذات</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز آتش ار علمت یقین شد از سخن</p></div>
<div class="m2"><p>پختگی جو در یقین منزل مکن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا نسوزی نیست آن عین الیقین</p></div>
<div class="m2"><p>این یقین خواهی در آتش در نشین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گوش چون نافذ بود دیده شود</p></div>
<div class="m2"><p>ورنه قل در گوش پیچیده شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این سخن پایان ندارد باز گرد</p></div>
<div class="m2"><p>تا که شه با آن غلامانش چه کرد</p></div></div>