---
title: >-
    بخش ۹۱ - قصهٔ تیراندازی و ترسیدن او از سواری کی در بیشه می‌رفت
---
# بخش ۹۱ - قصهٔ تیراندازی و ترسیدن او از سواری کی در بیشه می‌رفت

<div class="b" id="bn1"><div class="m1"><p>یک سواری با سلاح و بس مهیب</p></div>
<div class="m2"><p>می‌شد اندر بیشه بر اسپی نجیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیراندازی بحکم او را بدید</p></div>
<div class="m2"><p>پس ز خوف او کمان را در کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زند تیری سوارش بانگ زد</p></div>
<div class="m2"><p>من ضعیفم گرچه زفتستم جسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هان و هان منگر تو در زفتی من</p></div>
<div class="m2"><p>که کمم در وقت جنگ از پیرزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت رو که نیک گفتی ورنه نیش</p></div>
<div class="m2"><p>بر تو می‌انداختم از ترس خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس کسان را کلت پیگار کشت</p></div>
<div class="m2"><p>بی رجولیت چنان تیغی به مشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بپوشی تو سلاح رستمان</p></div>
<div class="m2"><p>رفت جانت چون نباشی مرد آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان سپر کن تیغ بگذار ای پسر</p></div>
<div class="m2"><p>هر که بی سر بود ازین شه برد سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن سلاحت حیله و مکر توست</p></div>
<div class="m2"><p>هم ز تو زایید و هم جان تو خست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نکردی هیچ سودی زین حیل</p></div>
<div class="m2"><p>ترک حیلت کن که پیش آید دول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون یکی لحظه نخوردی بر ز فن</p></div>
<div class="m2"><p>ترک فن گو می‌طلب رب المنن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون مبارک نیست بر تو این علوم</p></div>
<div class="m2"><p>خویشتن گولی کن و بگذر ز شوم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون ملایک گو که لا علم لنا</p></div>
<div class="m2"><p>یا الهی غیر ما علمتنا</p></div></div>