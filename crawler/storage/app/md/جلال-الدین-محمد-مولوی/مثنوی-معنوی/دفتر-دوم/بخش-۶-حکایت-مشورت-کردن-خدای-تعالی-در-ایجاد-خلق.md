---
title: >-
    بخش ۶ - حکایت مشورت کردن خدای تعالی  در ایجاد خلق
---
# بخش ۶ - حکایت مشورت کردن خدای تعالی  در ایجاد خلق

<div class="b" id="bn1"><div class="m1"><p>مشورت می‌رفت در ایجاد خلق</p></div>
<div class="m2"><p>جانشان در بحر قدرت تا به حلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ملایک مانع آن می‌شدند</p></div>
<div class="m2"><p>بر ملایک خفیه خنبک می‌زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطلع بر نقش هر که هست شد</p></div>
<div class="m2"><p>پیش از آن کین نفس کل پابست شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیشتر ز افلاک کیوان دیده‌اند</p></div>
<div class="m2"><p>پیشتر از دانه‌ها نان دیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی دماغ و دل پر از فکرت بدند</p></div>
<div class="m2"><p>بی سپاه و جنگ بر نصرت زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن عیان نسبت بایشان فکرتست</p></div>
<div class="m2"><p>ورنه خود نسبت بدوران رؤیتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکرت از ماضی و مستقبل بود</p></div>
<div class="m2"><p>چون ازین دو رست مشکل حل شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده چون بی‌کیف هر باکیف را</p></div>
<div class="m2"><p>دیده پیش از کان صحیح و زیف را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیشتر از خلقت انگورها</p></div>
<div class="m2"><p>خورده میها و نموده شورها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تموز گرم می‌بینند دی</p></div>
<div class="m2"><p>در شعاع شمس می‌بینند فی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دل انگور می را دیده‌اند</p></div>
<div class="m2"><p>در فنای محض شی را دیده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسمان در دور ایشان جرعه‌نوش</p></div>
<div class="m2"><p>آفتاب از جودشان زربفت‌پوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون ازیشان مجتمع بینی دو یار</p></div>
<div class="m2"><p>هم یکی باشند و هم ششصد هزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر مثال موجها اعدادشان</p></div>
<div class="m2"><p>در عدد آورده باشد بادشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مفترق شد آفتاب جانها</p></div>
<div class="m2"><p>در درون روزن ابدان ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون نظر در قرص داری خود یکیست</p></div>
<div class="m2"><p>وانک شد محجوب ابدان در شکیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تفرقه در روح حیوانی بود</p></div>
<div class="m2"><p>نفس واحد روح انسانی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چونک حق رش علیهم نوره</p></div>
<div class="m2"><p>مفترق هرگز نگردد نور او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یک زمان بگذار ای همره ملال</p></div>
<div class="m2"><p>تا بگویم وصف خالی زان جمال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در بیان ناید جمال حال او</p></div>
<div class="m2"><p>هر دو عالم چیست عکس خال او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چونک من از خال خوبش دم زنم</p></div>
<div class="m2"><p>نطق می‌خواهد که بشکافد تنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همچو موری اندرین خرمن خوشم</p></div>
<div class="m2"><p>تا فزون از خویش باری می‌کشم</p></div></div>