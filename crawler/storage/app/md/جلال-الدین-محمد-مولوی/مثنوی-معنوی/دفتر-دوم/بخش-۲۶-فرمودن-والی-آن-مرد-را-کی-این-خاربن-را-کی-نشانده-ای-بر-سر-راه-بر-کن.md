---
title: >-
    بخش ۲۶ - فرمودن والی آن مرد را کی این خاربن را کی نشانده‌ای بر سر راه بر کن
---
# بخش ۲۶ - فرمودن والی آن مرد را کی این خاربن را کی نشانده‌ای بر سر راه بر کن

<div class="b" id="bn1"><div class="m1"><p>همچو آن شخص درشت خوش‌سخن</p></div>
<div class="m2"><p>در میان ره نشاند او خاربن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره گذریانش ملامت‌گر شدند</p></div>
<div class="m2"><p>پس بگفتندش بکن این را نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دمی آن خاربن افزون شدی</p></div>
<div class="m2"><p>پای خلق از زخم آن پر خون شدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جامه‌های خلق بدریدی ز خار</p></div>
<div class="m2"><p>پای درویشان بخستی زار زار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بجد حاکم بدو گفت این بکن</p></div>
<div class="m2"><p>گفت آری بر کنم روزیش من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدتی فردا و فردا وعده داد</p></div>
<div class="m2"><p>شد درخت خار او محکم نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت روزی حاکمش ای وعده کژ</p></div>
<div class="m2"><p>پیش آ در کار ما واپس مغژ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت الایام یا عم بیننا</p></div>
<div class="m2"><p>گفت عجل لا تماطل دیننا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو که می‌گویی که فردا این بدان</p></div>
<div class="m2"><p>که بهر روزی که می‌آید زمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن درخت بد جوان‌تر می‌شود</p></div>
<div class="m2"><p>وین کننده پیر و مضطر می‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاربن در قوت و برخاستن</p></div>
<div class="m2"><p>خارکن در پیری و در کاستن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاربن هر روز و هر دم سبز و تر</p></div>
<div class="m2"><p>خارکن هر روز زار و خشک تر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او جوان‌تر می‌شود تو پیرتر</p></div>
<div class="m2"><p>زود باش و روزگار خود مبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاربن دان هر یکی خوی بدت</p></div>
<div class="m2"><p>بارها در پای خار آخر زدت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بارها از خوی خود خسته شدی</p></div>
<div class="m2"><p>حس نداری سخت بی‌حس آمدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر ز خسته گشتن دیگر کسان</p></div>
<div class="m2"><p>که ز خلق زشت تو هست آن رسان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غافلی باری ز زخم خود نه‌ای</p></div>
<div class="m2"><p>تو عذاب خویش و هر بیگانه‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا تبر بر گیر و مردانه بزن</p></div>
<div class="m2"><p>تو علی‌وار این در خیبر بکن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا به گلبن وصل کن این خار را</p></div>
<div class="m2"><p>وصل کن با نار نور یار را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا که نور او کشد نار ترا</p></div>
<div class="m2"><p>وصل او گلشن کند خار ترا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو مثال دوزخی او مؤمنست</p></div>
<div class="m2"><p>کشتن آتش به مؤمن ممکنست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مصطفی فرمود از گفت جحیم</p></div>
<div class="m2"><p>کو به مؤمن لابه‌گر گردد ز بیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گویدش بگذر ز من ای شاه زود</p></div>
<div class="m2"><p>هین که نورت سوز نارم را ربود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس هلاک نار نور مؤمنست</p></div>
<div class="m2"><p>زانک بی ضد دفع ضد لا یمکنست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نار ضد نور باشد روز عدل</p></div>
<div class="m2"><p>کان ز قهر انگیخته شد این ز فضل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر همی خواهی تو دفع شر نار</p></div>
<div class="m2"><p>آب رحمت بر دل آتش گمار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چشمهٔ آن آب رحمت مؤمنست</p></div>
<div class="m2"><p>آب حیوان روح پاک محسنست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بس گریزانست نفس تو ازو</p></div>
<div class="m2"><p>زانک تو از آتشی او آب خو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز آب آتش زان گریزان می‌شود</p></div>
<div class="m2"><p>کآتشش از آب ویران می‌شود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حس و فکر تو همه از آتشست</p></div>
<div class="m2"><p>حس شیخ و فکر او نور خوشست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آب نور او چو بر آتش چکد</p></div>
<div class="m2"><p>چک چک از آتش بر آید برجهد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون کند چک‌چک تو گویش مرگ و درد</p></div>
<div class="m2"><p>تا شود این دوزخ نفس تو سرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا نسوزد او گلستان ترا</p></div>
<div class="m2"><p>تا نسوزد عدل و احسان ترا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بعد از آن چیزی که کاری بر دهد</p></div>
<div class="m2"><p>لاله و نسرین و سیسنبر دهد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>باز پهنا می‌رویم از راه راست</p></div>
<div class="m2"><p>باز گرد ای خواجه راه ما کجاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اندر آن تقریر بودیم ای حسود</p></div>
<div class="m2"><p>که خرت لنگست و منزل دور زود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سال بیگه گشت وقت کشت نی</p></div>
<div class="m2"><p>جز سیه‌رویی و فعل زشت نی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کرم در بیخ درخت تن فتاد</p></div>
<div class="m2"><p>بایدش بر کند و در آتش نهاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هین و هین ای راه‌رو بیگاه شد</p></div>
<div class="m2"><p>آفتاب عمر سوی چاه شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این دو روزک را که زورت هست زود</p></div>
<div class="m2"><p>پیر افشانی بکن از راه جود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این قدر تخمی که ماندستت بباز</p></div>
<div class="m2"><p>تا بروید زین دو دم عمر دراز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا نمردست این چراغ با گهر</p></div>
<div class="m2"><p>هین فتیلش ساز و روغن زودتر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هین مگو فردا که فرداها گذشت</p></div>
<div class="m2"><p>تا بکلی نگذرد ایام کشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پند من بشنو که تن بند قویست</p></div>
<div class="m2"><p>کهنه بیرون کن گرت میل نویست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>لب ببند و کف پر زر بر گشا</p></div>
<div class="m2"><p>بخل تن بگذار و پیش آور سخا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ترک شهوتها و لذتها سخاست</p></div>
<div class="m2"><p>هر که در شهوت فرو شد برنخاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>این سخا شاخیست از سرو بهشت</p></div>
<div class="m2"><p>وای او کز کف چنین شاخی بهشت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عروة الوثقاست این ترک هوا</p></div>
<div class="m2"><p>برکشد این شاخ جان را بر سما</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا برد شاخ سخا ای خوب‌کیش</p></div>
<div class="m2"><p>مر ترا بالاکشان تا اصل خویش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یوسف حسنی و این عالم چو چاه</p></div>
<div class="m2"><p>وین رسن صبرست بر امر اله</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یوسفا آمد رسن در زن دو دست</p></div>
<div class="m2"><p>از رسن غافل مشو بیگه شدست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>حمد لله کین رسن آویختند</p></div>
<div class="m2"><p>فضل و رحمت را بهم آمیختند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا ببینی عالم جان جدید</p></div>
<div class="m2"><p>عالم بس آشکار ناپدید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این جهان نیست چون هستان شده</p></div>
<div class="m2"><p>وان جهان هست بس پنهان شده</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خاک بر بادست و بازی می‌کند</p></div>
<div class="m2"><p>کژنمایی پرده‌سازی می‌کند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اینک بر کارست بی‌کارست و پوست</p></div>
<div class="m2"><p>وانک پنهانست مغز و اصل اوست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خاک همچون آلتی در دست باد</p></div>
<div class="m2"><p>باد را دان عالی و عالی‌نژاد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چشم خاکی را به خاک افتد نظر</p></div>
<div class="m2"><p>بادبین چشمی بود نوعی دگر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اسپ داند اسپ را کو هست یار</p></div>
<div class="m2"><p>هم سواری داند احوال سوار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چشم حس اسپست و نور حق سوار</p></div>
<div class="m2"><p>بی‌سواره اسپ خود ناید به کار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پس ادب کن اسپ را از خوی بد</p></div>
<div class="m2"><p>ورنه پیش شاه باشد اسپ رد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چشم اسپ از چشم شه رهبر بود</p></div>
<div class="m2"><p>چشم او بی‌چشم شه مضطر بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چشم اسپان جز گیاه و جز چرا</p></div>
<div class="m2"><p>هر کجا خوانی بگوید نی چرا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نور حق بر نور حس راکب شود</p></div>
<div class="m2"><p>آنگهی جان سوی حق راغب شود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اسپ بی راکب چه داند رسم راه</p></div>
<div class="m2"><p>شاه باید تا بداند شاه‌راه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سوی حسی رو که نورش راکبست</p></div>
<div class="m2"><p>حس را آن نور نیکو صاحبست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نور حس را نور حق تزیین بود</p></div>
<div class="m2"><p>معنی نور علی نور این بود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نور حسی می‌کشد سوی ثری</p></div>
<div class="m2"><p>نور حقش می‌برد سوی علی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زانک محسوسات دونتر عالمیست</p></div>
<div class="m2"><p>نور حق دریا و حس چون شب‌نمیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>لیک پیدا نیست آن راکب برو</p></div>
<div class="m2"><p>جز به آثار و به گفتار نکو</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نور حسی کو غلیظست و گران</p></div>
<div class="m2"><p>هست پنهان در سواد دیدگان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چونک نور حس نمی‌بینی ز چشم</p></div>
<div class="m2"><p>چون ببینی نور آن دینی ز چشم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نور حس با این غلیظی مختفیست</p></div>
<div class="m2"><p>چون خفی نبود ضیائی کان صفیست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>این جهان چون خس به دست باد غیب</p></div>
<div class="m2"><p>عاجزی پیش گرفت و داد غیب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گه بلندش می‌کند گاهیش پست</p></div>
<div class="m2"><p>گه درستش می‌کند گاهی شکست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گه یمینش می‌برد گاهی یسار</p></div>
<div class="m2"><p>گه گلستانش کند گاهیش خار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دست پنهان و قلم بین خط‌گزار</p></div>
<div class="m2"><p>اسپ در جولان و ناپیدا سوار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تیر پران بین و ناپیدا کمان</p></div>
<div class="m2"><p>جانها پیدا و پنهان جان جان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تیر را مشکن که این تیر شهیست</p></div>
<div class="m2"><p>نیست پرتاوی ز شصت آگهیست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ما رمیت اذ رمیت گفت حق</p></div>
<div class="m2"><p>کار حق بر کارها دارد سبق</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خشم خود بشکن تو مشکن تیر را</p></div>
<div class="m2"><p>چشم خشمت خون شمارد شیر را</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بوسه ده بر تیر و پیش شاه بر</p></div>
<div class="m2"><p>تیر خون‌آلود از خون تو تر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>آنچ پیدا عاجز و بسته و زبون</p></div>
<div class="m2"><p>وآنچ ناپیدا چنان تند و حرون</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ما شکاریم این چنین دامی کراست</p></div>
<div class="m2"><p>گوی چوگانیم چوگانی کجاست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>می‌درد می‌دوزد این خیاط کو</p></div>
<div class="m2"><p>می‌دمد می‌سوزد این نفاط کو</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ساعتی کافر کند صدیق را</p></div>
<div class="m2"><p>ساعتی زاهد کند زندیق را</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زانک مخلص در خطر باشد ز دام</p></div>
<div class="m2"><p>تا ز خود خالص نگردد او تمام</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زانک در راهست و ره‌زن بی‌حدست</p></div>
<div class="m2"><p>آن رهد کو در امان ایزدست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>آینه خالص نگشت او مخلص است</p></div>
<div class="m2"><p>مرغ را نگرفته است او مقنص است</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چونک مخلص گشت مخلص باز رست</p></div>
<div class="m2"><p>در مقام امن رفت و برد دست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هیچ آیینه دگر آهن نشد</p></div>
<div class="m2"><p>هیچ نانی گندم خرمن نشد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هیچ انگوری دگر غوره نشد</p></div>
<div class="m2"><p>هیچ میوهٔ پخته با کوره نشد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>پخته گرد و از تغیر دور شو</p></div>
<div class="m2"><p>رو چو برهان محقق نور شو</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چون ز خود رستی همه برهان شدی</p></div>
<div class="m2"><p>چونک بنده نیست شد سلطان شدی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ور عیان خواهی صلاح الدین نمود</p></div>
<div class="m2"><p>دیده‌ها را کرد بینا و گشود</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>فقر را از چشم و از سیمای او</p></div>
<div class="m2"><p>دید هر چشمی که دارد نور هو</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>شیخ فعالست بی‌آلت چو حق</p></div>
<div class="m2"><p>با مریدان داده بی گفتی سبق</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دل به دست او چو موم نرم رام</p></div>
<div class="m2"><p>مهر او گه ننگ سازد گاه نام</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>مهر مومش حاکی انگشتریست</p></div>
<div class="m2"><p>باز آن نقش نگین حاکی کیست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>حاکی اندیشهٔ آن زرگرست</p></div>
<div class="m2"><p>سلسلهٔ هر حلقه اندر دیگرست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>این صدا در کوه دلها بانگ کیست</p></div>
<div class="m2"><p>گه پرست از بانگ این که گه تهیست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>هر کجا هست او حکیمست اوستاد</p></div>
<div class="m2"><p>بانگ او زین کوه دل خالی مباد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هست که کوا مثنا می‌کند</p></div>
<div class="m2"><p>هست که کآواز صدتا می‌کند</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>می‌زهاند کوه از آن آواز و قال</p></div>
<div class="m2"><p>صد هزاران چشمهٔ آب زلال</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چون ز که آن لطف بیرون می‌شود</p></div>
<div class="m2"><p>آبها در چشمه‌ها خون می‌شود</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>زان شهنشاه همایون‌نعل بود</p></div>
<div class="m2"><p>که سراسر طور سینا لعل بود</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>جان پذیرفت و خرد اجزای کوه</p></div>
<div class="m2"><p>ما کم از سنگیم آخر ای گروه</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نه ز جان یک چشمه جوشان می‌شود</p></div>
<div class="m2"><p>نه بدن از سبزپوشان می‌شود</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>نی صدای بانگ مشتاقی درو</p></div>
<div class="m2"><p>نی صفای جرعهٔ ساقی درو</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>کو حمیت تا ز تیشه وز کلند</p></div>
<div class="m2"><p>این چنین که را بکلی بر کنند</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بوک بر اجزای او تابد مهی</p></div>
<div class="m2"><p>بوک در وی تاب مه یابد رهی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چون قیامت کوهها را برکند</p></div>
<div class="m2"><p>بر سر ما سایه کی می‌افکند</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>این قیامت زان قیامت کی کمست</p></div>
<div class="m2"><p>آن قیامت زخم و این چون مرهمست</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>هر که دید این مرهم از زخم ایمنست</p></div>
<div class="m2"><p>هر بدی کین حسن دید او محسنست</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>ای خنک زشتی که خوبش شد حریف</p></div>
<div class="m2"><p>وای گل‌رویی که جفتش شد خریف</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نان مرده چون حریف جان شود</p></div>
<div class="m2"><p>زنده گردد نان و عین آن شود</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>هیزم تیره حریف نار شد</p></div>
<div class="m2"><p>تیرگی رفت و همه انوار شد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>در نمکلان چون خر مرده فتاد</p></div>
<div class="m2"><p>آن خری و مردگی یکسو نهاد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>صبغة الله هست خم رنگ هو</p></div>
<div class="m2"><p>پیسها یک رنگ گردد اندرو</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چون در آن خم افتد و گوییش قم</p></div>
<div class="m2"><p>از طرب گوید منم خم لا تلم</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>آن منم خم خود انا الحق گفتنست</p></div>
<div class="m2"><p>رنگ آتش دارد الا آهنست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>رنگ آهن محو رنگ آتشست</p></div>
<div class="m2"><p>ز آتشی می‌لافد و خامش وشست</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چون بسرخی گشت همچون زر کان</p></div>
<div class="m2"><p>پس انا النارست لافش بی زبان</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>شد ز رنگ و طبع آتش محتشم</p></div>
<div class="m2"><p>گوید او من آتشم من آتشم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>آتشم من گر ترا شکیست و ظن</p></div>
<div class="m2"><p>آزمون کن دست را بر من بزن</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>آتشم من بر تو گر شد مشتبه</p></div>
<div class="m2"><p>روی خود بر روی من یک‌دم بنه</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>آدمی چون نور گیرد از خدا</p></div>
<div class="m2"><p>هست مسجود ملایک ز اجتبا</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>نیز مسجود کسی کو چون ملک</p></div>
<div class="m2"><p>رسته باشد جانش از طغیان و شک</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>آتش چه آهن چه لب ببند</p></div>
<div class="m2"><p>ریش تشبیه مشبه را مخند</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>پای در دریا منه کم‌گوی از آن</p></div>
<div class="m2"><p>بر لب دریا خمش کن لب گزان</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>گرچه صد چون من ندارد تاب بحر</p></div>
<div class="m2"><p>لیک می‌نشکیبم از غرقاب بحر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>جان و عقل من فدای بحر باد</p></div>
<div class="m2"><p>خونبهای عقل و جان این بحر داد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>تا که پایم می‌رود رانم درو</p></div>
<div class="m2"><p>چون نماند پا چو بطانم درو</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>بی‌ادب حاضر ز غایب خوشترست</p></div>
<div class="m2"><p>حلقه گرچه کژ بود نی بر درست</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ای تن‌آلوده بگرد حوض گرد</p></div>
<div class="m2"><p>پاک کی گردد برون حوض مرد</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>پاک کو از حوض مهجور اوفتاد</p></div>
<div class="m2"><p>او ز پاکی خویش هم دور اوفتاد</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>پاکی این حوض بی‌پایان بود</p></div>
<div class="m2"><p>پاکی اجسام کم میزان بود</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>زانک دل حوضست لیکن در کمین</p></div>
<div class="m2"><p>سوی دریا راه پنهان دارد این</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>پاکی محدود تو خواهد مدد</p></div>
<div class="m2"><p>ورنه اندر خرج کم گردد عدد</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>آب گفت آلوده را در من شتاب</p></div>
<div class="m2"><p>گفت آلوده که دارم شرم از آب</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>گفت آب این شرم بی من کی رود</p></div>
<div class="m2"><p>بی من این آلوده زایل کی شود</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ز آب هر آلوده کو پنهان شود</p></div>
<div class="m2"><p>الحیاء یمنع الایمان بود</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>دل ز پایهٔ حوض تن گلناک شد</p></div>
<div class="m2"><p>تن ز آب حوض دلها پاک شد</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>گرد پایهٔ حوض دل گرد ای پسر</p></div>
<div class="m2"><p>هان ز پایهٔ حوض تن می‌کن حذر</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بحر تن بر بحر دل بر هم زنان</p></div>
<div class="m2"><p>در میانشان برزخ لا یبغیان</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>گر تو باشی راست ور باشی تو کژ</p></div>
<div class="m2"><p>پیشتر می‌غژ بدو واپس مغژ</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>پیش شاهان گر خطر باشد بجان</p></div>
<div class="m2"><p>لیک نشکیبند ازو با همتان</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>شاه چون شیرین‌تر از شکر بود</p></div>
<div class="m2"><p>جان به شیرینی رود خوشتر بود</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>ای ملامت‌گر سلامت مر ترا</p></div>
<div class="m2"><p>ای سلامت‌جو رها کن تو مرا</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>جان من کوره‌ست با آتش خوشست</p></div>
<div class="m2"><p>کوره را این بس که خانهٔ آتشست</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>همچو کوره عشق را سوزیدنیست</p></div>
<div class="m2"><p>هر که او زین کور باشد کوره نیست</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>برگ بی برگی ترا چون برگ شد</p></div>
<div class="m2"><p>جان باقی یافتی و مرگ شد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>چون ترا غم شادی افزودن گرفت</p></div>
<div class="m2"><p>روضهٔ جانت گل و سوسن گرفت</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>آنچ خوف دیگران آن امن تست</p></div>
<div class="m2"><p>بط قوی از بحر و مرغ خانه سست</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>باز دیوانه شدم من ای طبیب</p></div>
<div class="m2"><p>باز سودایی شدم من ای حبیب</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>حلقه‌های سلسلهٔ تو ذو فنون</p></div>
<div class="m2"><p>هر یکی حلقه دهد دیگر جنون</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>داد هر حلقه فنونی دیگرست</p></div>
<div class="m2"><p>پس مرا هر دم جنونی دیگرست</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>پس فنون باشد جنون این شد مثل</p></div>
<div class="m2"><p>خاصه در زنجیر این میر اجل</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>آنچنان دیوانگی بگسست بند</p></div>
<div class="m2"><p>که همه دیوانگان پندم دهند</p></div></div>