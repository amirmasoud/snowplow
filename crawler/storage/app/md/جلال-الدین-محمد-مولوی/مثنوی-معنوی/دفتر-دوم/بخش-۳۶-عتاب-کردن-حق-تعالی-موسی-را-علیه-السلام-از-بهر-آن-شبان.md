---
title: >-
    بخش ۳۶ - عتاب کردن حق تعالی موسی را علیه  السلام از بهر آن شبان
---
# بخش ۳۶ - عتاب کردن حق تعالی موسی را علیه  السلام از بهر آن شبان

<div class="b" id="bn1"><div class="m1"><p>وحی آمد سوی موسی از خدا</p></div>
<div class="m2"><p>بندهٔ ما را ز ما کردی جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو برای وصل کردن آمدی</p></div>
<div class="m2"><p>یا برای فصل کردن آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا توانی پا منه اندر فراق</p></div>
<div class="m2"><p>ابغض الاشیاء عندی الطلاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کسی را سیرتی بنهاده‌ام</p></div>
<div class="m2"><p>هر کسی را اصطلاحی داده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حق او مدح و در حق تو ذم</p></div>
<div class="m2"><p>در حق او شهد و در حق تو سم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما بری از پاک و ناپاکی همه</p></div>
<div class="m2"><p>از گرانجانی و چالاکی همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نکردم امر تا سودی کنم</p></div>
<div class="m2"><p>بلک تا بر بندگان جودی کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هندوان را اصطلاح هند مدح</p></div>
<div class="m2"><p>سندیان را اصطلاح سند مدح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من نگردم پاک از تسبیحشان</p></div>
<div class="m2"><p>پاک هم ایشان شوند و درفشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما زبان را ننگریم و قال را</p></div>
<div class="m2"><p>ما روان را بنگریم و حال را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناظر قلبیم اگر خاشع بود</p></div>
<div class="m2"><p>گرچه گفت لفظ ناخاضع رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زانک دل جوهر بود گفتن عرض</p></div>
<div class="m2"><p>پس طفیل آمد عرض جوهر غرض</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چند ازین الفاظ و اضمار و مجاز</p></div>
<div class="m2"><p>سوز خواهم سوز با آن سوز ساز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتشی از عشق در جان بر فروز</p></div>
<div class="m2"><p>سر بسر فکر و عبارت را بسوز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>موسیا آداب‌دانان دیگرند</p></div>
<div class="m2"><p>سوخته جان و روانان دیگرند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عاشقان را هر نفس سوزیدنیست</p></div>
<div class="m2"><p>بر ده ویران خراج و عشر نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر خطا گوید ورا خاطی مگو</p></div>
<div class="m2"><p>گر بود پر خون شهید او را مشو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خون شهیدان را ز آب اولی ترست</p></div>
<div class="m2"><p>این خطا از صد صواب اولی ترست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در درون کعبه رسم قبله نیست</p></div>
<div class="m2"><p>چه غم از غواص را پاچیله نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو ز سرمستان قلاوزی مجو</p></div>
<div class="m2"><p>جامه‌چاکان را چه فرمایی رفو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ملت عشق از همه دینها جداست</p></div>
<div class="m2"><p>عاشقان را ملت و مذهب خداست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لعل را گر مهر نبود باک نیست</p></div>
<div class="m2"><p>عشق در دریای غم غمناک نیست</p></div></div>