---
title: >-
    بخش ۳۵ - انکار کردن موسی علیه السلام بر مناجات شبان
---
# بخش ۳۵ - انکار کردن موسی علیه السلام بر مناجات شبان

<div class="b" id="bn1"><div class="m1"><p>دید موسی یک شبانی را براه</p></div>
<div class="m2"><p>کو همی‌گفت ای گزیننده اله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو کجایی تا شوم من چاکرت</p></div>
<div class="m2"><p>چارقت دوزم کنم شانه سرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامه‌ات شویم شپشهاات کشم</p></div>
<div class="m2"><p>شیر پیشت آورم ای محتشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستکت بوسم بمالم پایکت</p></div>
<div class="m2"><p>وقت خواب آید بروبم جایکت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای فدای تو همه بزهای من</p></div>
<div class="m2"><p>ای بیادت هیهی و هیهای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نمط بیهوده می‌گفت آن شبان</p></div>
<div class="m2"><p>گفت موسی با کی است این ای فلان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت با آنکس که ما را آفرید</p></div>
<div class="m2"><p>این زمین و چرخ ازو آمد پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت موسی های بس مدبر شدی</p></div>
<div class="m2"><p>خود مسلمان ناشده کافر شدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این چه ژاژست این چه کفرست و فشار</p></div>
<div class="m2"><p>پنبه‌ای اندر دهان خود فشار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گند کفر تو جهان را گنده کرد</p></div>
<div class="m2"><p>کفر تو دیبای دین را ژنده کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چارق و پاتابه لایق مر تراست</p></div>
<div class="m2"><p>آفتابی را چنینها کی رواست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نبندی زین سخن تو حلق را</p></div>
<div class="m2"><p>آتشی آید بسوزد خلق را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتشی گر نامدست این دود چیست</p></div>
<div class="m2"><p>جان سیه گشته روان مردود چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر همی‌دانی که یزدان داورست</p></div>
<div class="m2"><p>ژاژ و گستاخی ترا چون باورست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوستی بی‌خرد خود دشمنیست</p></div>
<div class="m2"><p>حق تعالی زین چنین خدمت غنیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با کی می‌گویی تو این با عم و خال</p></div>
<div class="m2"><p>جسم و حاجت در صفات ذوالجلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شیر او نوشد که در نشو و نماست</p></div>
<div class="m2"><p>چارق او پوشد که او محتاج پاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور برای بنده‌شست این گفت تو</p></div>
<div class="m2"><p>آنک حق گفت او منست و من خود او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنک گفت انی مرضت لم تعد</p></div>
<div class="m2"><p>من شدم رنجور او تنها نشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنک بی یسمع و بی یبصر شده‌ست</p></div>
<div class="m2"><p>در حق آن بنده این هم بیهده‌ست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی ادب گفتن سخن با خاص حق</p></div>
<div class="m2"><p>دل بمیراند سیه دارد ورق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر تو مردی را بخوانی فاطمه</p></div>
<div class="m2"><p>گرچه یک جنس‌اند مرد و زن همه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قصد خون تو کند تا ممکنست</p></div>
<div class="m2"><p>گرچه خوش‌خو و حلیم و ساکنست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فاطمه مدحست در حق زنان</p></div>
<div class="m2"><p>مرد را گویی بود زخم سنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دست و پا در حق ما استایش است</p></div>
<div class="m2"><p>در حق پاکی حق آلایش است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لم یلد لم یولد او را لایق است</p></div>
<div class="m2"><p>والد و مولود را او خالق است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرچه جسم آمد ولادت وصف اوست</p></div>
<div class="m2"><p>هرچه مولودست او زین سوی جوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زانک از کون و فساد است و مهین</p></div>
<div class="m2"><p>حادثست و محدثی خواهد یقین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت ای موسی دهانم دوختی</p></div>
<div class="m2"><p>وز پشیمانی تو جانم سوختی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جامه را بدرید و آهی کرد تفت</p></div>
<div class="m2"><p>سر نهاد اندر بیابانی و رفت</p></div></div>