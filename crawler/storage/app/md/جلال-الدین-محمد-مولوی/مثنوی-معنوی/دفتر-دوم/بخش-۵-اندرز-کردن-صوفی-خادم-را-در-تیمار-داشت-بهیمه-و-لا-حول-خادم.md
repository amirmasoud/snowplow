---
title: >-
    بخش ۵ - اندرز کردن صوفی خادم را در تیمار داشت بهیمه و لا حول خادم
---
# بخش ۵ - اندرز کردن صوفی خادم را در تیمار داشت بهیمه و لا حول خادم

<div class="b" id="bn1"><div class="m1"><p>صوفیی می‌گشت در دور افق</p></div>
<div class="m2"><p>تا شبی در خانقاهی شد قنق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بهیمه داشت در آخر ببست</p></div>
<div class="m2"><p>او به صدر صفه با یاران نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس مراقب گشت با یاران خویش</p></div>
<div class="m2"><p>دفتری باشد حضور یار پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دفتر صوفی سواد حرف نیست</p></div>
<div class="m2"><p>جز دل اسپید همچون برف نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاد دانشمند آثار قلم</p></div>
<div class="m2"><p>زاد صوفی چیست آثار قدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو صیادی سوی اشکار شد</p></div>
<div class="m2"><p>گام آهو دید و بر آثار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندگاهش گام آهو در خورست</p></div>
<div class="m2"><p>بعد از آن خود ناف آهو رهبرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونک شکر گام کرد و ره برید</p></div>
<div class="m2"><p>لاجرم زان گام در کامی رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفتن یک منزلی بر بوی ناف</p></div>
<div class="m2"><p>بهتر از صد منزل گام و طواف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دلی کو مطلع مهتابهاست</p></div>
<div class="m2"><p>بهر عارف فتحت ابوابهاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با تو دیوارست و با ایشان درست</p></div>
<div class="m2"><p>با تو سنگ و با عزیزان گوهرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنچ تو در آینه بینی عیان</p></div>
<div class="m2"><p>پیر اندر خشت بیند بیش از آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیر ایشانند کین عالم نبود</p></div>
<div class="m2"><p>جان ایشان بود در دریای جود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش ازین تن عمرها بگذاشتند</p></div>
<div class="m2"><p>پیشتر از کشت بر برداشتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیشتر از نقش جان پذرفته‌اند</p></div>
<div class="m2"><p>پیشتر از بحر درها سفته‌اند</p></div></div>