---
title: >-
    بخش ۱۱۴ - قصهٔ بط بچگان کی مرغ خانگی پروردشان
---
# بخش ۱۱۴ - قصهٔ بط بچگان کی مرغ خانگی پروردشان

<div class="b" id="bn1"><div class="m1"><p>تخم بطی گر چه مرغ خانه‌ات</p></div>
<div class="m2"><p>کرد زیر پر چو دایه تربیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مادر تو بط آن دریا بدست</p></div>
<div class="m2"><p>دایه‌ات خاکی بد و خشکی‌پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میل دریا که دل تو اندرست</p></div>
<div class="m2"><p>آن طبیعت جانت را از مادرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میل خشکی مر ترا زین دایه است</p></div>
<div class="m2"><p>دایه را بگذار کو بدرایه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایه را بگذار در خشک و بران</p></div>
<div class="m2"><p>اندر آ در بحر معنی چون بطان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ترا مادر بترساند ز آب</p></div>
<div class="m2"><p>تو مترس و سوی دریا ران شتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بطی بر خشک و بر تر زنده‌ای</p></div>
<div class="m2"><p>نی چو مرغ خانه خانه‌گنده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو ز کرمنا بنی آدم شهی</p></div>
<div class="m2"><p>هم به خشکی هم به دریا پا نهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که حملناهم علی البحر بجان</p></div>
<div class="m2"><p>از حملناهم علی البر پیش ران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مر ملایک را سوی بر راه نیست</p></div>
<div class="m2"><p>جنس حیوان هم ز بحر آگاه نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بتن حیوان بجانی از ملک</p></div>
<div class="m2"><p>تا روی هم بر زمین هم بر فلک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بظاهر مثلکم باشد بشر</p></div>
<div class="m2"><p>با دل یوحی الیه دیده‌ور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قالب خاکی فتاده بر زمین</p></div>
<div class="m2"><p>روح او گردان برین چرخ برین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما همه مرغابیانیم ای غلام</p></div>
<div class="m2"><p>بحر می‌داند زبان ما تمام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس سلیمان بحر آمد ما چو طیر</p></div>
<div class="m2"><p>در سلیمان تا ابد داریم سیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با سلیمان پای در دریا بنه</p></div>
<div class="m2"><p>تا چو داود آب سازد صد زره</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن سلیمان پیش جمله حاضرست</p></div>
<div class="m2"><p>لیک غیرت چشم‌بند و ساحرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا ز جهل و خوابناکی و فضول</p></div>
<div class="m2"><p>او بپیش ما و ما از وی ملول</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تشنه را درد سر آرد بانگ رعد</p></div>
<div class="m2"><p>چون نداند کو کشاند ابر سعد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشم او ماندست در جوی روان</p></div>
<div class="m2"><p>بی‌خبر از ذوق آب آسمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرکب همت سوی اسباب راند</p></div>
<div class="m2"><p>از مسبب لاجرم محجوب ماند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنک بیند او مسبب را عیان</p></div>
<div class="m2"><p>کی نهد دل بر سببهای جهان</p></div></div>