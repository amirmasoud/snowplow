---
title: >-
    بخش ۴۲ - تتمهٔ حکایت خرس و آن ابله کی بر وفای او اعتماد کرده بود
---
# بخش ۴۲ - تتمهٔ حکایت خرس و آن ابله کی بر وفای او اعتماد کرده بود

<div class="b" id="bn1"><div class="m1"><p>خرس هم از اژدها چون وا رهید</p></div>
<div class="m2"><p>وآن کرم زان مرد مردانه بدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سگ اصحاب کهف آن خرس زار</p></div>
<div class="m2"><p>شد ملازم در پی آن بردبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن مسلمان سر نهاد از خستگی</p></div>
<div class="m2"><p>خرس حارس گشت از دل‌بستگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یکی بگذشت و گفتش حال چیست</p></div>
<div class="m2"><p>ای برادر مر ترا این خرس کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصه وا گفت و حدیث اژدها</p></div>
<div class="m2"><p>گفت بر خرسی منه دل ابلها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوستی ابله بتر از دشمنیست</p></div>
<div class="m2"><p>او بهر حیله که دانی راندنیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت والله از حسودی گفت این</p></div>
<div class="m2"><p>ورنه خرسی چه نگری این مهر بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت مهر ابلهان عشوه‌ده است</p></div>
<div class="m2"><p>این حسودی من از مهرش به است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هی بیا با من بران این خرس را</p></div>
<div class="m2"><p>خرس را مگزین مهل هم‌جنس را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت رو رو کار خود کن ای حسود</p></div>
<div class="m2"><p>گفت کارم این بد و رزقت نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من کم از خرسی نباشم ای شریف</p></div>
<div class="m2"><p>ترک او کن تا منت باشم حریف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر تو دل می‌لرزدم ز اندیشه‌ای</p></div>
<div class="m2"><p>با چنین خرسی مرو در بیشه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این دلم هرگز نلرزید از گزاف</p></div>
<div class="m2"><p>نور حقست این نه دعوی و نه لاف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مؤمنم ینظر بنور الله شده</p></div>
<div class="m2"><p>هان و هان بگریز ازین آتشکده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این همه گفت و به گوشش در نرفت</p></div>
<div class="m2"><p>بدگمانی مرد سدیست زفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دست او بگرفت و دست از وی کشید</p></div>
<div class="m2"><p>گفت رفتم چون نه‌ای یار رشید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت رو بر من تو غمخواره مباش</p></div>
<div class="m2"><p>بوالفضولا معرفت کمتر تراش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز گفتش من عدوی تو نیم</p></div>
<div class="m2"><p>لطف باشد گر بیابی در پیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت خوابستم مرا بگذار و رو</p></div>
<div class="m2"><p>گفت آخر یار را منقاد شو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا بخسپی در پناه عاقلی</p></div>
<div class="m2"><p>در جوار دوستی صاحب‌دلی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در خیال افتاد مرد از جد او</p></div>
<div class="m2"><p>خشمگین شد زود گردانید رو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کین مگر قصد من آمد خونیست</p></div>
<div class="m2"><p>یا طمع دارد گدا و تونیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یا گرو بستست با یاران بدین</p></div>
<div class="m2"><p>که بترساند مرا زین همنشین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خود نیامد هیچ از خبث سرش</p></div>
<div class="m2"><p>یک گمان نیک اندر خاطرش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ظن نیکش جملگی بر خرس بود</p></div>
<div class="m2"><p>او مگر مر خرس را هم‌جنس بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عاقلی را از سگی تهمت نهاد</p></div>
<div class="m2"><p>خرس را دانست اهل مهر و داد</p></div></div>