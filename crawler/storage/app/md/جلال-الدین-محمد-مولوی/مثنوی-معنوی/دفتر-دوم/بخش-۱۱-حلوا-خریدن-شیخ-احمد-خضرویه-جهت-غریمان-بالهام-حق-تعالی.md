---
title: >-
    بخش ۱۱ - حلوا خریدن شیخ احمد خضرویه جهت غریمان بالهام حق تعالی
---
# بخش ۱۱ - حلوا خریدن شیخ احمد خضرویه جهت غریمان بالهام حق تعالی

<div class="b" id="bn1"><div class="m1"><p>بود شیخی دایما او وامدار</p></div>
<div class="m2"><p>از جوامردی که بود آن نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ده هزاران وام کردی از مهان</p></div>
<div class="m2"><p>خرج کردی بر فقیران جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم بوام او خانقاهی ساخته</p></div>
<div class="m2"><p>جان و مال و خانقه در باخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وام او را حق ز هر جا می‌گزارد</p></div>
<div class="m2"><p>کرد حق بهر خلیل از ریگ آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت پیغامبر که در بازارها</p></div>
<div class="m2"><p>دو فرشته می‌کنند ایدر دعا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کای خدا تو منفقان را ده خلف</p></div>
<div class="m2"><p>ای خدا تو ممسکان را ده تلف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاصه آن منفق که جان انفاق کرد</p></div>
<div class="m2"><p>حلق خود قربانی خلاق کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلق پیش آورد اسمعیل‌وار</p></div>
<div class="m2"><p>کارد بر حلقش نیارد کرد کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس شهیدان زنده زین رویند و خوش</p></div>
<div class="m2"><p>تو بدان قالب بمنگر گبروش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خلف دادستشان جان بقا</p></div>
<div class="m2"><p>جان ایمن از غم و رنج و شقا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیخ وامی سالها این کار کرد</p></div>
<div class="m2"><p>می‌ستد می‌داد همچون پای‌مرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تخمها می‌کاشت تا روز اجل</p></div>
<div class="m2"><p>تا بود روز اجل میر اجل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چونک عمر شیخ در آخر رسید</p></div>
<div class="m2"><p>در وجود خود نشان مرگ دید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وام‌داران گرد او بنشسته جمع</p></div>
<div class="m2"><p>شیخ بر خود خوش گدازان همچو شمع</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وام‌داران گشته نومید و ترش</p></div>
<div class="m2"><p>درد دلها یار شد با درد شش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شیخ گفت این بدگمانان را نگر</p></div>
<div class="m2"><p>نیست حق را چار صد دینار زر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کودکی حلوا ز بیرون بانگ زد</p></div>
<div class="m2"><p>لاف حلوا بر امید دانگ زد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شیخ اشارت کرد خادم را بسر</p></div>
<div class="m2"><p>که برو آن جمله حلوا را بخر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا غریمان چونک آن حلوا خورند</p></div>
<div class="m2"><p>یک زمانی تلخ در من ننگرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در زمان خادم برون آمد بدر</p></div>
<div class="m2"><p>تا خرد او جمله حلوا را بزر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت او را کوترو حلوا بچند</p></div>
<div class="m2"><p>گفت کودک نیم دینار و ادند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت نه از صوفیان افزون مجو</p></div>
<div class="m2"><p>نیم دینارت دهم دیگر مگو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او طبق بنهاد اندر پیش شیخ</p></div>
<div class="m2"><p>تو ببین اسرار سر اندیش شیخ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کرد اشارت با غریمان کین نوال</p></div>
<div class="m2"><p>نک تبرک خوش خورید این را حلال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون طبق خالی شد آن کودک ستد</p></div>
<div class="m2"><p>گفت دینارم بده ای با خرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شیخ گفتا از کجا آرم درم</p></div>
<div class="m2"><p>وام دارم می‌روم سوی عدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کودک از غم زد طبق را بر زمین</p></div>
<div class="m2"><p>ناله و گریه بر آورد و حنین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>می‌گریست از غبن کودک های های</p></div>
<div class="m2"><p>کای مرا بشکسته بودی هر دو پای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کاشکی من گرد گلخن گشتمی</p></div>
<div class="m2"><p>بر در این خانقه نگذشتمی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صوفیان طبل‌خوار لقمه‌جو</p></div>
<div class="m2"><p>سگ‌دلان و همچو گربه روی‌شو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از غریو کودک آنجا خیر و شر</p></div>
<div class="m2"><p>گرد آمد گشت بر کودک حشر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پیش شیخ آمد که ای شیخ درشت</p></div>
<div class="m2"><p>تو یقین دان که مرا استاد کشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر روم من پیش او دست تهی</p></div>
<div class="m2"><p>او مرا بکشد اجازت می‌دهی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وان غریمان هم بانکار و جحود</p></div>
<div class="m2"><p>رو به شیخ آورده کین باری چه بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مال ما خوردی مظالم می‌بری</p></div>
<div class="m2"><p>از چه بود این ظلم دیگر بر سری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا نماز دیگر آن کودک گریست</p></div>
<div class="m2"><p>شیخ دیده بست و در وی ننگریست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شیخ فارغ از جفا و از خلاف</p></div>
<div class="m2"><p>در کشیده روی چون مه در لحاف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>با ازل خوش با اجل خوش شادکام</p></div>
<div class="m2"><p>فارغ از تشنیع و گفت خاص و عام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آنک جان در روی او خندد چو قند</p></div>
<div class="m2"><p>از ترش‌رویی خلقش چه گزند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آنک جان بوسه دهد بر چشم او</p></div>
<div class="m2"><p>کی خورد غم از فلک وز خشم او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در شب مهتاب مه را بر سماک</p></div>
<div class="m2"><p>از سگان و وعوع ایشان چه باک</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سگ وظیفهٔ خود بجا می‌آورد</p></div>
<div class="m2"><p>مه وظیفهٔ خود برخ می‌گسترد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کارک خود می‌گزارد هر کسی</p></div>
<div class="m2"><p>آب نگذارد صفا بهر خسی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خس خسانه می‌رود بر روی آب</p></div>
<div class="m2"><p>آب صافی می‌رود بی اضطراب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مصطفی مه می‌شکافد نیم‌شب</p></div>
<div class="m2"><p>ژاژ می‌خاید ز کینه بولهب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آن مسیحا مرده زنده می‌کند</p></div>
<div class="m2"><p>وان جهود از خشم سبلت می‌کند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بانگ سگ هرگز رسد در گوش ماه</p></div>
<div class="m2"><p>خاصه ماهی کو بود خاص اله</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>می خورد شه بر لب جو تا سحر</p></div>
<div class="m2"><p>در سماع از بانگ چغزان بی خبر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هم شدی توزیع کودک دانگ چند</p></div>
<div class="m2"><p>همت شیخ آن سخا را کرد بند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا کسی ندهد به کودک هیچ چیز</p></div>
<div class="m2"><p>قوت پیران ازین بیش است نیز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شد نماز دیگر آمد خادمی</p></div>
<div class="m2"><p>یک طبق بر کف ز پیش حاتمی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>صاحب مالی و حالی پیش پیر</p></div>
<div class="m2"><p>هدیه بفرستاد کز وی بد خبیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چارصد دینار بر گوشهٔ طبق</p></div>
<div class="m2"><p>نیم دینار دگر اندر ورق</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خادم آمد شیخ را اکرام کرد</p></div>
<div class="m2"><p>وان طبق بنهاد پیش شیخ فرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون طبق را از غطا وا کرد رو</p></div>
<div class="m2"><p>خلق دیدند آن کرامت را ازو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آه و افغان از همه برخاست زود</p></div>
<div class="m2"><p>کای سر شیخان و شاهان این چه بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>این چه سرست این چه سلطانیست باز</p></div>
<div class="m2"><p>ای خداوند خداوندان راز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ما ندانستیم ما را عفو کن</p></div>
<div class="m2"><p>بس پراکنده که رفت از ما سخن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ما که کورانه عصاها می‌زنیم</p></div>
<div class="m2"><p>لاجرم قندیلها را بشکنیم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ما چو کران ناشنیده یک خطاب</p></div>
<div class="m2"><p>هرزه گویان از قیاس خود جواب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ما ز موسی پند نگرفتیم کو</p></div>
<div class="m2"><p>گشت از انکار خضری زردرو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>با چنان چشمی که بالا می‌شتافت</p></div>
<div class="m2"><p>نور چشمش آسمان را می‌شکافت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کرده با چشمت تعصب موسیا</p></div>
<div class="m2"><p>از حماقت چشم موش آسیا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شیخ فرمود آن همه گفتار و قال</p></div>
<div class="m2"><p>من بحل کردم شما را آن حلال</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سر این آن بود کز حق خواستم</p></div>
<div class="m2"><p>لاجرم بنمود راه راستم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گفت آن دینار اگر چه اندکست</p></div>
<div class="m2"><p>لیک موقوف غریو کودکست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تا نگرید کودک حلوا فروش</p></div>
<div class="m2"><p>بحر رحمت در نمی‌آید به جوش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای برادر طفل طفل چشم تست</p></div>
<div class="m2"><p>کام خود موقوف زاری دان درست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر همی‌خواهی که آن خلعت رسد</p></div>
<div class="m2"><p>پس بگریان طفل دیده بر جسد</p></div></div>