---
title: >-
    بخش ۱۰۴ - بیان دعویی که عین آن دعوی گواه  صدق خویش است
---
# بخش ۱۰۴ - بیان دعویی که عین آن دعوی گواه  صدق خویش است

<div class="b" id="bn1"><div class="m1"><p>گر تو هستی آشنای جان من</p></div>
<div class="m2"><p>نیست دعوی گفت معنی‌لان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بگویم نیم‌شب پیش توم</p></div>
<div class="m2"><p>هین مترس از شب که من خویش توم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این دو دعوی پیش تو معنی بود</p></div>
<div class="m2"><p>چون شناسی بانگ خویشاوند خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیشی و خویشی دو دعوی بود لیک</p></div>
<div class="m2"><p>هر دو معنی بود پیش فهم نیک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قرب آوازش گواهی می‌دهد</p></div>
<div class="m2"><p>کین دم از نزدیک یاری می‌جهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لذت آواز خویشاوند نیز</p></div>
<div class="m2"><p>شد گوا بر صدق آن خویش عزیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز بی الهام احمق کو ز جهل</p></div>
<div class="m2"><p>می‌نداند بانگ بیگانه ز اهل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش او دعوی بود گفتار او</p></div>
<div class="m2"><p>جهل او شد مایهٔ انکار او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش زیرک کاندرونش نورهاست</p></div>
<div class="m2"><p>عین این آواز معنی بود راست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا به تازی گفت یک تازی‌زبان</p></div>
<div class="m2"><p>که همی‌دانم زبان تازیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عین تازی گفتنش معنی بود</p></div>
<div class="m2"><p>گرچه تازی گفتنش دعوی بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا نویسد کاتبی بر کاغدی</p></div>
<div class="m2"><p>کاتب و خط‌خوانم و من امجدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این نوشته گرچه خود دعوی بود</p></div>
<div class="m2"><p>هم نوشته شاهد معنی بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا بگوید صوفیی دیدی تو دوش</p></div>
<div class="m2"><p>در میان خواب سجاده‌بدوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من بدم آن وآنچ گفتم خواب در</p></div>
<div class="m2"><p>با تو اندر خواب در شرح نظر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گوش کن چون حلقه اندر گوش کن</p></div>
<div class="m2"><p>آن سخن را پیشوای هوش کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون ترا یاد آید آن خواب این سخن</p></div>
<div class="m2"><p>معجز نو باشد و زر کهن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه دعوی می‌نماید این ولی</p></div>
<div class="m2"><p>جان صاحب‌واقعه گوید بلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس چو حکمت ضالهٔمؤمنبود</p></div>
<div class="m2"><p>آن ز هر که بشنود موقن بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونک خود را پیش او یابد فقط</p></div>
<div class="m2"><p>چون بود شک چون کند او را غلط</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تشنه‌ای را چون بگویی تو شتاب</p></div>
<div class="m2"><p>در قدح آبست بستان زود آب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هیچ گوید تشنه کین دعویست رو</p></div>
<div class="m2"><p>از برم ای مدعی مهجور شو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یا گواه و حجتی بنما که این</p></div>
<div class="m2"><p>جنس آبست و از آن ماء معین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یا به طفل شیر مادر بانگ زد</p></div>
<div class="m2"><p>که بیا من مادرم هان ای ولد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طفل گوید مادرا حجت بیار</p></div>
<div class="m2"><p>تا که با شیرت بگیرم من قرار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در دل هر امتی کز حق مزه‌ست</p></div>
<div class="m2"><p>روی و آواز پیمبر معجزه‌ست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون پیمبر از برون بانگی زند</p></div>
<div class="m2"><p>جان امت در درون سجده کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زانک جنس بانگ او اندر جهان</p></div>
<div class="m2"><p>از کسی نشنیده باشد گوش جان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن غریب از ذوق آواز غریب</p></div>
<div class="m2"><p>از زبان حق شنود انی قریب</p></div></div>