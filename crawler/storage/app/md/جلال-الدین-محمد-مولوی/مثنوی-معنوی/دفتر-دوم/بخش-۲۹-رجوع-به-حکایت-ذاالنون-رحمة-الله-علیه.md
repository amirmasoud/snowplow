---
title: >-
    بخش ۲۹ - رجوع به حکایت ذاالنون رحمة الله علیه
---
# بخش ۲۹ - رجوع به حکایت ذاالنون رحمة الله علیه

<div class="b" id="bn1"><div class="m1"><p>چون رسیدند آن نفر نزدیک او</p></div>
<div class="m2"><p>بانگ بر زد هی کیانید اتقو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ادب گفتند ما از دوستان</p></div>
<div class="m2"><p>بهر پرسش آمدیم اینجا بجان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونی ای دریای عقل ذو فنون</p></div>
<div class="m2"><p>این چه بهتانست بر عقلت جنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دود گلخن کی رسد در آفتاب</p></div>
<div class="m2"><p>چون شود عنقا شکسته از غراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وا مگیر از ما بیان کن این سخن</p></div>
<div class="m2"><p>ما محبانیم با ما این مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مر محبان را نشاید دور کرد</p></div>
<div class="m2"><p>یا بروپوش و دغل مغرور کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راز را اندر میان آور شها</p></div>
<div class="m2"><p>رو مکن در ابر پنهانی مها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما محب و صادق و دل خسته‌ایم</p></div>
<div class="m2"><p>در دو عالم دل به تو در بسته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فحش آغازید و دشنام از گزاف</p></div>
<div class="m2"><p>گفت او دیوانگانه زی و قاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر جهید و سنگ پران کرد و چوب</p></div>
<div class="m2"><p>جملگی بگریختند از بیم کوب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قهقهه خندید و جنبانید سر</p></div>
<div class="m2"><p>گفت باد ریش این یاران نگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوستان بین کو نشان دوستان</p></div>
<div class="m2"><p>دوستان را رنج باشد همچو جان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کی کران گیرد ز رنج دوست دوست</p></div>
<div class="m2"><p>رنج مغز و دوستی آن را چو پوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نی نشان دوستی شد سرخوشی</p></div>
<div class="m2"><p>در بلا و آفت و محنت‌کشی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوست همچون زر بلا چون آتشست</p></div>
<div class="m2"><p>زر خالص در دل آتش خوشست</p></div></div>