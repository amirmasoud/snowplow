---
title: >-
    بخش ۱۶ - تعریف کردن منادیان قاضی مفلس را گرد شهر
---
# بخش ۱۶ - تعریف کردن منادیان قاضی مفلس را گرد شهر

<div class="b" id="bn1"><div class="m1"><p>بود شخصی مفلسی بی خان و مان</p></div>
<div class="m2"><p>مانده در زندان و بند بی امان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لقمهٔ زندانیان خوردی گزاف</p></div>
<div class="m2"><p>بر دل خلق از طمع چون کوه قاف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهره نه کس را که لقمهٔ نان خورد</p></div>
<div class="m2"><p>زانک آن لقمه‌ربا گاوش برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که دور از دعوت رحمان بود</p></div>
<div class="m2"><p>او گداچشمست اگر سلطان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مر مروت را نهاده زیر پا</p></div>
<div class="m2"><p>گشته زندان دوزخی زان نان‌ربا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گریزی بر امید راحتی</p></div>
<div class="m2"><p>زان طرف هم پیشت آید آفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ کنجی بی دد و بی دام نیست</p></div>
<div class="m2"><p>جز بخلوتگاه حق آرام نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنج زندان جهان ناگزیر</p></div>
<div class="m2"><p>نیست بی پامزد و بی دق الحصیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>والله ار سوراخ موشی در روی</p></div>
<div class="m2"><p>مبتلای گربه چنگالی شوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آدمی را فربهی هست از خیال</p></div>
<div class="m2"><p>گر خیالاتش بود صاحب‌جمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور خیالاتش نماید ناخوشی</p></div>
<div class="m2"><p>می‌گدازد همچو موم از آتشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در میان مار و کزدم گر ترا</p></div>
<div class="m2"><p>با خیالات خوشان دارد خدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مار و کزدم مر ترا مونس بود</p></div>
<div class="m2"><p>کان خیالت کیمیای مس بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صبر شیرین از خیال خوش شدست</p></div>
<div class="m2"><p>کان خیالات فرج پیش آمدست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن فرج آید ز ایمان در ضمیر</p></div>
<div class="m2"><p>ضعف ایمان ناامیدی و زحیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صبر از ایمان بیابد سر کله</p></div>
<div class="m2"><p>حیث لا صبر فلا ایمان له</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت پیغامبر خداش ایمان نداد</p></div>
<div class="m2"><p>هر که را صبری نباشد در نهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن یکی در چشم تو باشد چو مار</p></div>
<div class="m2"><p>هم وی اندر چشم آن دیگر نگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زانک در چشمت خیال کفر اوست</p></div>
<div class="m2"><p>وان خیال مؤمنی در چشم دوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاندرین یک شخص هر دو فعل هست</p></div>
<div class="m2"><p>گاه ماهی باشد او و گاه شست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیم او مؤمن بود نیمیش گبر</p></div>
<div class="m2"><p>نیم او حرص‌آوری نیمیش صبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت یزدانت فمنکم مؤمن</p></div>
<div class="m2"><p>باز منکم کافر گبر کهن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همچو گاوی نیمهٔ چپش سیاه</p></div>
<div class="m2"><p>نیمهٔ دیگر سپید همچو ماه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که این نیمه ببیند رد کند</p></div>
<div class="m2"><p>هر که آن نیمه ببیند کد کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یوسف اندر چشم اخوان چون ستور</p></div>
<div class="m2"><p>هم وی اندر چشم یعقوبی چو حور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از خیال بد مرورا زشت دید</p></div>
<div class="m2"><p>چشم فرع و چشم اصلی ناپدید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چشم ظاهر سایهٔ آن چشم دان</p></div>
<div class="m2"><p>هرچه آن بیند بگردد این بدان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو مکانی اصل تو در لامکان</p></div>
<div class="m2"><p>این دکان بر بند و بگشا آن دکان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شش جهت مگریز زیرا در جهات</p></div>
<div class="m2"><p>ششدره‌ست و ششدره ماتست مات</p></div></div>