---
title: >-
    بخش ۹۳ - کرامات ابراهیم ادهم قدس الله سره بر لب دریا
---
# بخش ۹۳ - کرامات ابراهیم ادهم قدس الله سره بر لب دریا

<div class="b" id="bn1"><div class="m1"><p>هم ز ابراهیم ادهم آمدست</p></div>
<div class="m2"><p>کو ز راهی بر لب دریا نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلق خود می‌دوخت آن سلطان جان</p></div>
<div class="m2"><p>یک امیری آمد آنجا ناگهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن امیر از بندگان شیخ بود</p></div>
<div class="m2"><p>شیخ را بشناخت سجده کرد زود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیره شد در شیخ و اندر دلق او</p></div>
<div class="m2"><p>شکل دیگر گشته خلق و خلق او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو رها کرد آنچنان ملکی شگرف</p></div>
<div class="m2"><p>بر گزید آن فقر بس باریک‌حرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک کرد او ملک هفت اقلیم را</p></div>
<div class="m2"><p>می‌زند بر دلق سوزن چون گدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ واقف گشت از اندیشه‌اش</p></div>
<div class="m2"><p>شیخ چون شیرست و دلها بیشه‌اش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون رجا و خوف در دلها روان</p></div>
<div class="m2"><p>نیست مخفی بر وی اسرار جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل نگه دارید ای بی حاصلان</p></div>
<div class="m2"><p>در حضور حضرت صاحب‌دلان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش اهل تن ادب بر ظاهرست</p></div>
<div class="m2"><p>که خدا زیشان نهان را ساترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش اهل دل ادب بر باطنست</p></div>
<div class="m2"><p>زانک دلشان بر سرایر فاطنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو بعکسی پیش کوران بهر جاه</p></div>
<div class="m2"><p>با حضور آیی نشینی پایگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش بینایان کنی ترک ادب</p></div>
<div class="m2"><p>نار شهوت را از آن گشتی حطب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نداری فطنت و نور هدی</p></div>
<div class="m2"><p>بهر کوران روی را می‌زن جلا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش بینایان حدث در روی مال</p></div>
<div class="m2"><p>ناز می‌کن با چنین گندیده حال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شیخ سوزن زود در دریا فکند</p></div>
<div class="m2"><p>خواست سوزن را به آواز بلند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صد هزاران ماهی اللهیی</p></div>
<div class="m2"><p>سوزن زر در لب هر ماهیی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سر بر آوردند از دریای حق</p></div>
<div class="m2"><p>که بگیر ای شیخ سوزنهای حق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رو بدو کرد و بگفتش ای امیر</p></div>
<div class="m2"><p>ملک دل به یا چنان ملک حقیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این نشان ظاهرست این هیچ نیست</p></div>
<div class="m2"><p>تا بباطن در روی بینی تو بیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوی شهر از باغ شاخی آورند</p></div>
<div class="m2"><p>باغ و بستان را کجا آنجا برند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاصه باغی کین فلک یک برگ اوست</p></div>
<div class="m2"><p>بلک آن مغزست و این عالم چو پوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر نمی‌داری سوی آن باغ گام</p></div>
<div class="m2"><p>بوی افزون جوی و کن دفع زکام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا که آن بو جاذب جانت شود</p></div>
<div class="m2"><p>تا که آن بو نور چشمانت شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت یوسف ابن یعقوب نبی</p></div>
<div class="m2"><p>بهر بو القوا علی وجه ابی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهر این بو گفت احمد در عظات</p></div>
<div class="m2"><p>دائما قرة عینی فی الصلوة</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پنج حس با همدگر پیوسته‌اند</p></div>
<div class="m2"><p>رسته این هر پنج از اصلی بلند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قوت یک قوت باقی شود</p></div>
<div class="m2"><p>ما بقی را هر یکی ساقی شود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیدن دیده فزاید عشق را</p></div>
<div class="m2"><p>عشق در دیده فزاید صدق را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صدق بیداری هر حس می‌شود</p></div>
<div class="m2"><p>حسها را ذوق مونس می‌شود</p></div></div>