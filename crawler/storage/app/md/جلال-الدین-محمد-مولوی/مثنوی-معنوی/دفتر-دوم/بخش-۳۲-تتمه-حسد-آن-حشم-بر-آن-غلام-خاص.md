---
title: >-
    بخش ۳۲ - تتمهٔ حسد آن حشم بر آن غلام خاص
---
# بخش ۳۲ - تتمهٔ حسد آن حشم بر آن غلام خاص

<div class="b" id="bn1"><div class="m1"><p>قصهٔ شاه و امیران و حسد</p></div>
<div class="m2"><p>بر غلام خاص و سلطان خرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور ماند از جر جرار کلام</p></div>
<div class="m2"><p>باز باید گشت و کرد آن را تمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبان ملک با اقبال و بخت</p></div>
<div class="m2"><p>چون درختی را نداند از درخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن درختی را که تلخ و رد بود</p></div>
<div class="m2"><p>و آن درختی که یکش هفصد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی برابر دارد اندر تربیت</p></div>
<div class="m2"><p>چون ببیندشان به چشم عاقبت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کان درختان را نهایت چیست بر</p></div>
<div class="m2"><p>گرچه یکسانند این دم در نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ کو ینظر بنور الله شد</p></div>
<div class="m2"><p>از نهایت وز نخست آگاه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم آخربین ببست از بهر حق</p></div>
<div class="m2"><p>چشم آخربین گشاد اندر سبق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن حسودان بد درختان بوده‌اند</p></div>
<div class="m2"><p>تلخ گوهر شوربختان بوده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حسد جوشان و کف می‌ریختند</p></div>
<div class="m2"><p>در نهانی مکر می‌انگیختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا غلام خاص را گردن زنند</p></div>
<div class="m2"><p>بیخ او را از زمانه بر کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون شود فانی چو جانش شاه بود</p></div>
<div class="m2"><p>بیخ او در عصمت الله بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه از آن اسرار واقف آمده</p></div>
<div class="m2"><p>همچو بوبکر ربابی تن زده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در تماشای دل بدگوهران</p></div>
<div class="m2"><p>می‌زدی خنبک بر آن کوزه‌گران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مکر می‌سازند قومی حیله‌مند</p></div>
<div class="m2"><p>تا که شه را در فقاعی در کنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پادشاهی بس عظیمی بی کران</p></div>
<div class="m2"><p>در فقاعی کی بگنجد ای خران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از برای شاه دامی دوختند</p></div>
<div class="m2"><p>آخر این تدبیر ازو آموختند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نحس شاگردی که با استاد خویش</p></div>
<div class="m2"><p>همسری آغازد و آید به پیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با کدام استاد استاد جهان</p></div>
<div class="m2"><p>پیش او یکسان هویدا و نهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشم او ینظر بنور الله شده</p></div>
<div class="m2"><p>پرده‌های جهل را خارق بده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از دل سوراخ چون کهنه گلیم</p></div>
<div class="m2"><p>پرده‌ای بندد به پیش آن حکیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پرده می‌خندد برو با صد دهان</p></div>
<div class="m2"><p>هر دهانی گشته اشکافی بر آن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گوید آن استاد مر شاگرد را</p></div>
<div class="m2"><p>ای کم از سگ نیستت با من وفا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خود مرا استا مگیر آهن‌گسل</p></div>
<div class="m2"><p>همچو خود شاگرد گیر و کوردل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه از منت یاریست در جان و روان</p></div>
<div class="m2"><p>بی منت آبی نمی‌گردد روان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس دل من کارگاه بخت تست</p></div>
<div class="m2"><p>چه شکنی این کارگاه ای نادرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گوییش پنهان زنم آتش‌زنه</p></div>
<div class="m2"><p>نی به قلب از قلب باشد روزنه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آخر از روزن ببیند فکر تو</p></div>
<div class="m2"><p>دل گواهیی دهد زین ذکر تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گیر در رویت نمالد از کرم</p></div>
<div class="m2"><p>هرچه گویی خندد و گوید نعم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>او نمی‌خندد ز ذوق مالشت</p></div>
<div class="m2"><p>او همی‌خندد بر آن اسگالشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس خداعی را خداعی شد جزا</p></div>
<div class="m2"><p>کاسه زن کوزه بخور اینک سزا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر بدی با تو ورا خندهٔ رضا</p></div>
<div class="m2"><p>صد هزاران گل شکفتی مر ترا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون دل او در رضا آرد عمل</p></div>
<div class="m2"><p>آفتابی دان که آید در حمل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زو بخندد هم نهار و هم بهار</p></div>
<div class="m2"><p>در هم آمیزد شکوفه و سبزه‌زار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صد هزاران بلبل و قمری نوا</p></div>
<div class="m2"><p>افکنند اندر جهان بی‌نوا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چونک برگ روح خود زرد و سیاه</p></div>
<div class="m2"><p>می‌ببینی چون ندانی خشم شاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آفتاب شاه در برج عتاب</p></div>
<div class="m2"><p>می‌کند روها سیه همچون کتاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آن عطارد را ورقها جان ماست</p></div>
<div class="m2"><p>آن سپیدی و آن سیه میزان ماست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>باز منشوری نویسد سرخ و سبز</p></div>
<div class="m2"><p>تا رهند ارواح از سودا و عجز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سرخ و سبز افتاد نسخ نوبهار</p></div>
<div class="m2"><p>چون خط قوس و قزح در اعتبار</p></div></div>