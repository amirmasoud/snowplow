---
title: >-
    بخش ۶۴ - باز جواب گفتن ابلیس معاویه را
---
# بخش ۶۴ - باز جواب گفتن ابلیس معاویه را

<div class="b" id="bn1"><div class="m1"><p>گفت ما اول فرشته بوده‌ایم</p></div>
<div class="m2"><p>راه طاعت را بجان پیموده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالکان راه را محرم بدیم</p></div>
<div class="m2"><p>ساکنان عرش را همدم بدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشهٔ اول کجا از دل رود</p></div>
<div class="m2"><p>مهر اول کی ز دل بیرون شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سفر گر روم بینی یا ختن</p></div>
<div class="m2"><p>از دل تو کی رود حب الوطن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما هم از مستان این می بوده‌ایم</p></div>
<div class="m2"><p>عاشقان درگه وی بوده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناف ما بر مهر او ببریده‌اند</p></div>
<div class="m2"><p>عشق او در جان ما کاریده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز نیکو دیده‌ایم از روزگار</p></div>
<div class="m2"><p>آب رحمت خورده‌ایم اندر بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی که ما را دست فضلش کاشتست</p></div>
<div class="m2"><p>از عدم ما را نه او بر داشتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بسا کز وی نوازش دیده‌ایم</p></div>
<div class="m2"><p>در گلستان رضا گردیده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سر ما دست رحمت می‌نهاد</p></div>
<div class="m2"><p>چشمه‌های لطف از ما می‌گشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت طفلی‌ام که بودم شیرجو</p></div>
<div class="m2"><p>گاهوارم را کی جنبانید او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از کی خوردم شیر غیر شیر او</p></div>
<div class="m2"><p>کی مرا پرورد جز تدبیر او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوی کان با شیر رفت اندر وجود</p></div>
<div class="m2"><p>کی توان آن را ز مردم واگشود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر عتابی کرد دریای کرم</p></div>
<div class="m2"><p>بسته کی گردند درهای کرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اصل نقدش داد و لطف و بخششست</p></div>
<div class="m2"><p>قهر بر وی چون غباری از غشست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از برای لطف عالم را بساخت</p></div>
<div class="m2"><p>ذره‌ها را آفتاب او نواخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرقت از قهرش اگر آبستنست</p></div>
<div class="m2"><p>بهر قدر وصل او دانستنست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا دهد جان را فراقش گوشمال</p></div>
<div class="m2"><p>جان بداند قدر ایام وصال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت پیغامبر که حق فرموده است</p></div>
<div class="m2"><p>قصد من از خلق احسان بوده است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آفریدم تا ز من سودی کنند</p></div>
<div class="m2"><p>تا ز شهدم دست‌آلودی کنند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه برای آنک تا سودی کنم</p></div>
<div class="m2"><p>وز برهنه من قبایی بر کنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چند روزی که ز پیشم رانده‌ست</p></div>
<div class="m2"><p>چشم من در روی خوبش مانده‌ست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کز چنان رویی چنین قهر ای عجب</p></div>
<div class="m2"><p>هر کسی مشغول گشته در سبب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من سبب را ننگرم کان حادثست</p></div>
<div class="m2"><p>زانک حادث حادثی را باعثست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لطف سابق را نظاره می‌کنم</p></div>
<div class="m2"><p>هرچه آن حادث دو پاره می‌کنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ترک سجده از حسد گیرم که بود</p></div>
<div class="m2"><p>آن حسد از عشق خیزد نه از جحود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر حسد از دوستی خیزد یقین</p></div>
<div class="m2"><p>که شود با دوست غیری همنشین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هست شرط دوستی غیرت‌پزی</p></div>
<div class="m2"><p>همچو شرط عطسه گفتن دیر زی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چونک بر نطعش جز این بازی نبود</p></div>
<div class="m2"><p>گفت بازی کن چه دانم در فزود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن یکی بازی که بد من باختم</p></div>
<div class="m2"><p>خویشتن را در بلا انداختم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در بلا هم می‌چشم لذات او</p></div>
<div class="m2"><p>مات اویم مات اویم مات او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون رهاند خویشتن را ای سره</p></div>
<div class="m2"><p>هیچ کس در شش جهت از ششدره</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جزو شش از کل شش چون وا رهد</p></div>
<div class="m2"><p>خاصه که بی چون مرورا کژ نهد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر که در شش او درون آتشست</p></div>
<div class="m2"><p>اوش برهاند که خلاق ششست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خود اگر کفرست و گر ایمان او</p></div>
<div class="m2"><p>دست‌باف حضرتست و آن او</p></div></div>