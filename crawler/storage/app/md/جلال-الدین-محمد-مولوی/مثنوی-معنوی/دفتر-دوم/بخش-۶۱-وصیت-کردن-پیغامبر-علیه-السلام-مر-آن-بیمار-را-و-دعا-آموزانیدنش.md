---
title: >-
    بخش ۶۱ - وصیت کردن پیغامبر علیه السلام مر آن بیمار را و دعا آموزانیدنش
---
# بخش ۶۱ - وصیت کردن پیغامبر علیه السلام مر آن بیمار را و دعا آموزانیدنش

<div class="b" id="bn1"><div class="m1"><p>گفت پیغامبر مر آن بیمار را</p></div>
<div class="m2"><p>این بگو کای سهل‌کن دشوار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتنا فی دار دنیانا حسن</p></div>
<div class="m2"><p>آتنا فی دار عقبانا حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه را بر ما چو بستان کن لطیف</p></div>
<div class="m2"><p>منزل ما خود تو باشی ای شریف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مؤمنان در حشر گویند ای ملک</p></div>
<div class="m2"><p>نی که دوزخ بود راه مشترک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مؤمن و کافر برو یابد گذار</p></div>
<div class="m2"><p>ما ندیدیم اندرین ره دود و نار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نک بهشت و بارگاه آمنی</p></div>
<div class="m2"><p>پس کجا بود آن گذرگاه دنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس ملک گوید که آن روضهٔ خضر</p></div>
<div class="m2"><p>که فلان جا دیده‌اید اندر گذر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوزخ آن بود و سیاستگاه سخت</p></div>
<div class="m2"><p>بر شما شد باغ و بستان و درخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شما این نفس دوزخ‌خوی را</p></div>
<div class="m2"><p>آتشی گبر فتنه‌جوی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهدها کردید و او شد پر صفا</p></div>
<div class="m2"><p>نار را کشتید از بهر خدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آتش شهوت که شعله می‌زدی</p></div>
<div class="m2"><p>سبزهٔ تقوی شد و نور هدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آتش خشم از شما هم حلم شد</p></div>
<div class="m2"><p>ظلمت جهل از شما هم علم شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتش حرص از شما ایثار شد</p></div>
<div class="m2"><p>و آن حسد چون خار بد گلزار شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون شما این جمله آتشهای خویش</p></div>
<div class="m2"><p>بهر حق کشتید جمله پیش پیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نفس ناری را چو باغی ساختید</p></div>
<div class="m2"><p>اندرو تخم وفا انداختید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلبلان ذکر و تسبیح اندرو</p></div>
<div class="m2"><p>خوش سرایان در چمن بر طرف جو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>داعی حق را اجابت کرده‌اید</p></div>
<div class="m2"><p>در جحیم نفس آب آورده‌اید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دوزخ ما نیز در حق شما</p></div>
<div class="m2"><p>سبزه گشت و گلشن و برگ و نوا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چیست احسان را مکافات ای پسر</p></div>
<div class="m2"><p>لطف و احسان و ثواب معتبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نی شما گفتید ما قربانییم</p></div>
<div class="m2"><p>پیش اوصاف بقا ما فانییم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ما اگر قلاش و گر دیوانه‌ایم</p></div>
<div class="m2"><p>مست آن ساقی و آن پیمانه‌ایم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر خط و فرمان او سر می‌نهیم</p></div>
<div class="m2"><p>جان شیرین را گروگان می‌دهیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا خیال دوست در اسرار ماست</p></div>
<div class="m2"><p>چاکری و جانسپاری کار ماست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر کجا شمع بلا افروختند</p></div>
<div class="m2"><p>صد هزاران جان عاشق سوختند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عاشقانی کز درون خانه‌اند</p></div>
<div class="m2"><p>شمع روی یار را پروانه‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای دل آنجا رو که با تو روشنند</p></div>
<div class="m2"><p>وز بلاها مر ترا چون جوشنند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر جنایاتت مواسا می‌کنند</p></div>
<div class="m2"><p>در میان جان ترا جا می‌کنند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زان میان جان ترا جا می‌کنند</p></div>
<div class="m2"><p>تا ترا پر باده چون جا می‌کنند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در میان جان ایشان خانه گیر</p></div>
<div class="m2"><p>در فلک خانه کن ای بدر منیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون عطارد دفتر دل وا کنند</p></div>
<div class="m2"><p>تا که بر تو سرها پیدا کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیش خویشان باش چون آواره‌ای</p></div>
<div class="m2"><p>بر مه کامل زن ار مه پاره‌ای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جزو را از کل خود پرهیز چیست</p></div>
<div class="m2"><p>با مخالف این همه آمیز چیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جنس را بین نوع گشته در روش</p></div>
<div class="m2"><p>غیبها بین عین گشته در رهش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا چو زن عشوه خری ای بی‌خرد</p></div>
<div class="m2"><p>از دروغ و عشوه کی یابی مدد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چاپلوس و لفظ شیرین و فریب</p></div>
<div class="m2"><p>می‌ستانی می‌نهی چون زن به جیب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مر ترا دشنام و سیلی شهان</p></div>
<div class="m2"><p>بهتر آید از ثنای گمرهان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صفع شاهان خور مخور شهد خسان</p></div>
<div class="m2"><p>تا کسی گردی ز اقبال کسان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زانک ازیشان خلعت و دولت رسد</p></div>
<div class="m2"><p>در پناه روح جان گردد جسد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر کجا بینی برهنه و بی‌نوا</p></div>
<div class="m2"><p>دان که او بگریختست از اوستا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا چنان گردد که می‌خواهد دلش</p></div>
<div class="m2"><p>آن دل کور بد بی‌حاصلش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر چنان گشتی که استا خواستی</p></div>
<div class="m2"><p>خویش را و خویش را آراستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر که از استا گریزد در جهان</p></div>
<div class="m2"><p>او ز دولت می‌گریزد این بدان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پیشه‌ای آموختی در کسب تن</p></div>
<div class="m2"><p>چنگ اندر پیشهٔ دینی بزن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در جهان پوشیده گشتی و غنی</p></div>
<div class="m2"><p>چون برون آیی ازینجا چون کنی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پیشه‌ای آموز کاندر آخرت</p></div>
<div class="m2"><p>اندر آید دخل کسب مغفرت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آن جهان شهریست پر بازار و کسب</p></div>
<div class="m2"><p>تا نپنداری که کسب اینجاست حسب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حق تعالی گفت کین کسب جهان</p></div>
<div class="m2"><p>پیش آن کسبست لعب کودکان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همچو آن طفلی که بر طفلی تند</p></div>
<div class="m2"><p>شکل صحبت‌کن مساسی می‌کند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کودکان سازند در بازی دکان</p></div>
<div class="m2"><p>سود نبود جز که تعبیر زمان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شب شود در خانه آید گرسنه</p></div>
<div class="m2"><p>کودکان رفته بمانده یک تنه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>این جهان بازی‌گهست و مرگ شب</p></div>
<div class="m2"><p>باز گردی کیسه خالی پر تعب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کسب دین عشقست و جذب اندرون</p></div>
<div class="m2"><p>قابلیت نور حق را ای حرون</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کسب فانی خواهدت این نفس خس</p></div>
<div class="m2"><p>چند کسب خس کنی بگذار بس</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نفس خس گر جویدت کسب شریف</p></div>
<div class="m2"><p>حیله و مکری بود آن را ردیف</p></div></div>