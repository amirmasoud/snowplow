---
title: >-
    بخش ۱۱۲ - منازعت چهار کس جهت انگور کی هر یکی به نام دیگر فهم کرده بود آن را
---
# بخش ۱۱۲ - منازعت چهار کس جهت انگور کی هر یکی به نام دیگر فهم کرده بود آن را

<div class="b" id="bn1"><div class="m1"><p>چار کس را داد مردی یک درم</p></div>
<div class="m2"><p>آن یکی گفت این بانگوری دهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یکی دیگر عرب بد گفت لا</p></div>
<div class="m2"><p>من عنب خواهم نه انگور ای دغا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یکی ترکی بد و گفت این بنم</p></div>
<div class="m2"><p>من نمی‌خواهم عنب خواهم ازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یکی رومی بگفت این قیل را</p></div>
<div class="m2"><p>ترک کن خواهیم استافیل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تنازع آن نفر جنگی شدند</p></div>
<div class="m2"><p>که ز سر نامها غافل بدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشت بر هم می‌زدند از ابلهی</p></div>
<div class="m2"><p>پر بدند از جهل و از دانش تهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صاحب سری عزیزی صد زبان</p></div>
<div class="m2"><p>گر بدی آنجا بدادی صلحشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس بگفتی او که من زین یک درم</p></div>
<div class="m2"><p>آرزوی جمله‌تان را می‌دهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونک بسپارید دل را بی دغل</p></div>
<div class="m2"><p>این درمتان می‌کند چندین عمل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک درمتان می‌شود چار المراد</p></div>
<div class="m2"><p>چار دشمن می‌شود یک ز اتحاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت هر یکتان دهد جنگ و فراق</p></div>
<div class="m2"><p>گفت من آرد شما را اتفاق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس شما خاموش باشید انصتوا</p></div>
<div class="m2"><p>تا زبانتان من شوم در گفت و گو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر سخنتان می‌نماید یک نمط</p></div>
<div class="m2"><p>در اثر مایهٔ نزاعست و سخط</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرمی عاریتی ندهد اثر</p></div>
<div class="m2"><p>گرمی خاصیتی دارد هنر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرکه را گر گرم کردی ز آتش آن</p></div>
<div class="m2"><p>چون خوری سردی فزاید بی گمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زانک آن گرمی او دهلیزیست</p></div>
<div class="m2"><p>طبع اصلش سردیست و تیزیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور بود یخ‌بسته دوشاب ای پسر</p></div>
<div class="m2"><p>چون خوری گرمی فزاید در جگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس ریای شیخ به ز اخلاص ماست</p></div>
<div class="m2"><p>کز بصیرت باشد آن وین از عماست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از حدیث شیخ جمعیت رسد</p></div>
<div class="m2"><p>تفرقه آرد دم اهل جسد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون سلیمان کز سوی حضرت بتاخت</p></div>
<div class="m2"><p>کو زبان جمله مرغان را شناخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در زمان عدلش آهو با پلنگ</p></div>
<div class="m2"><p>انس بگرفت و برون آمد ز جنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شد کبوتر آمن از چنگال باز</p></div>
<div class="m2"><p>گوسفند از گرگ ناورد احتراز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او میانجی شد میان دشمنان</p></div>
<div class="m2"><p>اتحادی شد میان پرزنان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو چو موری بهر دانه می‌دوی</p></div>
<div class="m2"><p>هین سلیمان جو چه می‌باشی غوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دانه‌جو را دانه‌اش دامی شود</p></div>
<div class="m2"><p>و آن سلیمان‌جوی را هر دو بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرغ جانها را درین آخر زمان</p></div>
<div class="m2"><p>نیستشان از همدگر یک دم امان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هم سلیمان هست اندر دور ما</p></div>
<div class="m2"><p>کو دهد صلح و نماند جور ما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قول ان من امة را یاد گیر</p></div>
<div class="m2"><p>تا به الا و خلا فیها نذیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت خود خالی نبودست امتی</p></div>
<div class="m2"><p>از خلیفهٔ حق و صاحب‌همتی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرغ جانها را چنان یکدل کند</p></div>
<div class="m2"><p>کز صفاشان بی غش و بی غل کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مشفقان گردند همچون والده</p></div>
<div class="m2"><p>مسلمون را گفت نفس واحده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نفس واحد از رسول حق شدند</p></div>
<div class="m2"><p>ور نه هر یک دشمن مطلق بدند</p></div></div>