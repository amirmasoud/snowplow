---
title: >-
    بخش ۹۶ - بقیهٔ قصهٔ ابراهیم ادهم بر لب آن دریا
---
# بخش ۹۶ - بقیهٔ قصهٔ ابراهیم ادهم بر لب آن دریا

<div class="b" id="bn1"><div class="m1"><p>چون نفاذ امر شیخ آن میر دید</p></div>
<div class="m2"><p>ز آمد ماهی شدش وجدی پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت اه ماهی ز پیران آگهست</p></div>
<div class="m2"><p>شه تنی را کو لعین درگهست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهیان از پیر آگه ما بعید</p></div>
<div class="m2"><p>ما شقی زین دولت و ایشان سعید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سجده کرد و رفت گریان و خراب</p></div>
<div class="m2"><p>گشت دیوانه ز عشق فتح باب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس تو ای ناشسته‌رو در چیستی</p></div>
<div class="m2"><p>در نزاع و در حسد با کیستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دم شیری تو بازی می‌کنی</p></div>
<div class="m2"><p>بر ملایک ترک‌تازی می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بد چه می‌گویی تو خیر محض را</p></div>
<div class="m2"><p>هین ترفع کم شمر آن خفض را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بد چه باشد مس محتاج مهان</p></div>
<div class="m2"><p>شیخ کی بود کیمیای بی‌کران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مس اگر از کیمیا قابل نبد</p></div>
<div class="m2"><p>کیمیا از مس هرگز مس نشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بد چه باشد سرکشی آتش‌عمل</p></div>
<div class="m2"><p>شیخ کی بود عین دریای ازل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دایم آتش را بترسانند از آب</p></div>
<div class="m2"><p>آب کی ترسید هرگز ز التهاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در رخ مه عیب‌بینی می‌کنی</p></div>
<div class="m2"><p>در بهشتی خارچینی می‌کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بهشت اندر روی تو خارجو</p></div>
<div class="m2"><p>هیچ خار آنجا نیابی غیر تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌بپوشی آفتابی در گلی</p></div>
<div class="m2"><p>رخنه می‌جویی ز بدر کاملی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آفتابی که بتابد در جهان</p></div>
<div class="m2"><p>بهر خفاشی کجا گردد نهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عیبها از رد پیران عیب شد</p></div>
<div class="m2"><p>غیبها از رشک ایشان غیب شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باری ار دوری ز خدمت یار باش</p></div>
<div class="m2"><p>در ندامت چابک و بر کار باش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا از آن راهت نسیمی می‌رسد</p></div>
<div class="m2"><p>آب رحمت را چه بندی از حسد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرچه دوری دور می‌جنبان تو دم</p></div>
<div class="m2"><p>حیث ما کنتم فولوا وجهکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون خری در گل فتد از گام تیز</p></div>
<div class="m2"><p>دم بدم جنبد برای عزم خیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جای را هموار نکند بهر باش</p></div>
<div class="m2"><p>داند او که نیست آن جای معاش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حس تو از حس خر کمتر بدست</p></div>
<div class="m2"><p>که دل تو زین وحلها بر نجست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در وحل تاویل و رخصت می‌کنی</p></div>
<div class="m2"><p>چون نمی‌خواهی کز آن دل بر کنی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کین روا باشد مرا من مضطرم</p></div>
<div class="m2"><p>حق نگیرد عاجزی را از کرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خود گرفتستت تو چون کفتار کور</p></div>
<div class="m2"><p>این گرفتن را نبینی از غرور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>می‌گوند اینجایگه کفتار نیست</p></div>
<div class="m2"><p>از برون جویید کاندر غار نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این همی‌گویند و بندش می‌نهند</p></div>
<div class="m2"><p>او همی‌گوید ز من بی آگهند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر ز من آگاه بودی این عدو</p></div>
<div class="m2"><p>کی ندا کردی که آن کفتار کو</p></div></div>