---
title: >-
    بخش ۶۰ - تتمهٔ نصیحت رسول علیه السلام بیمار را
---
# بخش ۶۰ - تتمهٔ نصیحت رسول علیه السلام بیمار را

<div class="b" id="bn1"><div class="m1"><p>گفت پیغامبر مر آن بیمار را</p></div>
<div class="m2"><p>چون عیادت کرد یار زار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مگر نوعی دعایی کرده‌ای</p></div>
<div class="m2"><p>از جهالت زهربایی خورده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد آور چه دعا می‌گفته‌ای</p></div>
<div class="m2"><p>چون ز مکر نفس می‌آشفته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت یادم نیست الا همتی</p></div>
<div class="m2"><p>دار با من یادم آید ساعتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حضور نوربخش مصطفی</p></div>
<div class="m2"><p>پیش خاطر آمد او را آن دعا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تافت زان روزن که از دل تا دلست</p></div>
<div class="m2"><p>روشنی که فرق حق و باطلست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت اینک یادم آمد ای رسول</p></div>
<div class="m2"><p>آن دعا که گفته‌ام من بوالفضول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون گرفتار گنه می‌آمدم</p></div>
<div class="m2"><p>غرقه دست اندر حشایش می‌زدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو تهدید و وعیدی می‌رسید</p></div>
<div class="m2"><p>مجرمان را از عذاب بس شدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مضطرب می‌گشتم و چاره نبود</p></div>
<div class="m2"><p>بند محکم بود و قفل ناگشود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی مقام صبر و نی راه گریز</p></div>
<div class="m2"><p>نی امید توبه نی جای ستیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من چو هاروت و چو ماروت از حزن</p></div>
<div class="m2"><p>آه می‌کردم که ای خلاق من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خطر هاروت و ماروت آشکار</p></div>
<div class="m2"><p>چاه بابل را بکردند اختیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا عذاب آخرت اینجا کشند</p></div>
<div class="m2"><p>گربزند و عاقل و ساحروشند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیک کردند و بجای خویش بود</p></div>
<div class="m2"><p>سهل‌تر باشد ز آتش رنج دود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حد ندارد وصف رنج آن جهان</p></div>
<div class="m2"><p>سهل باشد رنج دنیا پیش آن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای خنک آن کو جهادی می‌کند</p></div>
<div class="m2"><p>بر بدن زجری و دادی می‌کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا ز رنج آن جهانی وا رهد</p></div>
<div class="m2"><p>بر خود این رنج عبادت می‌نهد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من همی‌گفتم که یا رب آن عذاب</p></div>
<div class="m2"><p>هم درین عالم بران بر من شتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا در آن عالم فراغت باشدم</p></div>
<div class="m2"><p>در چنین درخواست حلقه می‌زدم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این چنین رنجوریی پیدام شد</p></div>
<div class="m2"><p>جان من از رنج بی آرام شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مانده‌ام از ذکر و از اوراد خود</p></div>
<div class="m2"><p>بی‌خبر گشتم ز خویش و نیک و بد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر نمی‌دیدم کنون من روی تو</p></div>
<div class="m2"><p>ای خجسته وی مبارک بوی تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>می‌شدم از بند من یکبارگی</p></div>
<div class="m2"><p>کردیم شاهانه این غمخوارگی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت هی هی این دعا دیگر مکن</p></div>
<div class="m2"><p>بر مکن تو خویش را از بیخ و بن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو چه طاقت داری ای مور نژند</p></div>
<div class="m2"><p>که نهد بر تو چنان کوه بلند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت توبه کردم ای سلطان که من</p></div>
<div class="m2"><p>از سر جلدی نلافم هیچ فن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این جهان تیهست و تو موسی و ما</p></div>
<div class="m2"><p>از گنه در تیه مانده مبتلا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قوم موسی راه می‌پیموده‌اند</p></div>
<div class="m2"><p>آخر اندر گام اول بوده‌اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سالها ره می‌رویم و در اخیر</p></div>
<div class="m2"><p>همچنان در منزل اول اسیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر دل موسی ز ما راضی بدی</p></div>
<div class="m2"><p>تیه را راه و کران پیدا شدی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ور بکل بیزار بودی او ز ما</p></div>
<div class="m2"><p>کی رسیدی خوانمان هیچ از سما</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کی ز سنگی چشمه‌ها جوشان شدی</p></div>
<div class="m2"><p>در بیابان‌مان امان جان شدی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بل به جای خوان خود آتش آمدی</p></div>
<div class="m2"><p>اندرین منزل لهب بر ما زدی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون دو دل شد موسی اندر کار ما</p></div>
<div class="m2"><p>گاه خصم ماست و گاهی یار ما</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خشمش آتش می‌زند در رخت ما</p></div>
<div class="m2"><p>حلم او رد می‌کند تیر بلا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کی بود که حلم گردد خشم نیز</p></div>
<div class="m2"><p>نیست این نادر ز لطفت ای عزیز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مدح حاضر وحشتست از بهر این</p></div>
<div class="m2"><p>نام موسی می‌برم قاصد چنین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ورنه موسی کی روا دارد که من</p></div>
<div class="m2"><p>پیش تو یاد آورم از هیچ تن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عهد ما بشکست صد بار و هزار</p></div>
<div class="m2"><p>عهد تو چون کوه ثابت بر قرار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عهد ما کاه و به هر بادی زبون</p></div>
<div class="m2"><p>عهد تو کوه و ز صد که هم فزون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حق آن قوت که بر تلوین ما</p></div>
<div class="m2"><p>رحمتی کن ای امیر لونها</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خویش را دیدیم و رسوایی خویش</p></div>
<div class="m2"><p>امتحان ما مکن ای شاه بیش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا فضیحتهای دیگر را نهان</p></div>
<div class="m2"><p>کرده باشی ای کریم مستعان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بی‌حدی تو در جمال و در کمال</p></div>
<div class="m2"><p>در کژی ما بی‌حدیم و در ضلال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بی حدی خویش بگمار ای کریم</p></div>
<div class="m2"><p>بر کژی بی حد مشتی لئیم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هین که از تقطیع ما یک تار ماند</p></div>
<div class="m2"><p>مصر بودیم و یکی دیوار ماند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>البقیه البقیه ای خدیو</p></div>
<div class="m2"><p>تا نگردد شاد کلی جان دیو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بهر ما نی بهر آن لطف نخست</p></div>
<div class="m2"><p>که تو کردی گمرهان را باز جست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون نمودی قدرتت بنمای رحم</p></div>
<div class="m2"><p>ای نهاده رحمها در لحم و شحم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>این دعا گر خشم افزاید ترا</p></div>
<div class="m2"><p>تو دعا تعلیم فرما مهترا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آنچنان کادم بیفتاد از بهشت</p></div>
<div class="m2"><p>رجعتش دادی که رست از دیو زشت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دیو کی بود کو ز آدم بگذرد</p></div>
<div class="m2"><p>بر چنین نطعی ازو بازی برد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در حقیقت نفع آدم شد همه</p></div>
<div class="m2"><p>لعنت حاسد شده آن دمدمه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بازیی دید و دو صد بازی ندید</p></div>
<div class="m2"><p>پس ستون خانهٔ خود را برید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنشی زد شب بکشت دیگران</p></div>
<div class="m2"><p>باد آتش را بکشت او بران</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چشم‌بندی بود لعنت دیو را</p></div>
<div class="m2"><p>تا زیان خصم دید آن ریو را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خود زیان جان او شد ریو او</p></div>
<div class="m2"><p>گویی آدم بود دیو دیو او</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لعنت این باشد که کژبینش کند</p></div>
<div class="m2"><p>حاسد و خودبین و پر کینش کند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا نداند که هر آنک کرد بد</p></div>
<div class="m2"><p>عاقبت باز آید و بر وی زند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جمله فرزین‌بندها بیند بعکس</p></div>
<div class="m2"><p>مات بر وی گردد و نقصان و وکس</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زانک گر او هیچ بیند خویش را</p></div>
<div class="m2"><p>مهلک و ناسور بیند ریش را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>درد خیزد زین چنین دیدن درون</p></div>
<div class="m2"><p>درد او را از حجاب آرد برون</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا نگیرد مادران را درد زه</p></div>
<div class="m2"><p>طفل در زادن نیابد هیچ ره</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>این امانت در دل و دل حامله‌ست</p></div>
<div class="m2"><p>این نصیحتها مثال قابله‌ست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>قابله گوید که زن را درد نیست</p></div>
<div class="m2"><p>درد باید درد کودک را رهیست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آنک او بی‌درد باشد ره‌زنست</p></div>
<div class="m2"><p>زانک بی‌دردی انا الحق گفتنست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>آن انا بی وقت گفتن لعنتست</p></div>
<div class="m2"><p>آن انا در وقت گفتن رحمتست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آن انا منصور رحمت شد یقین</p></div>
<div class="m2"><p>آن انا فرعون لعنت شد ببین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>لاجرم هر مرغ بی‌هنگام را</p></div>
<div class="m2"><p>سر بریدن واجبست اعلام را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سر بریدن چیست کشتن نفس را</p></div>
<div class="m2"><p>در جهاد و ترک گفتن تفس را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آنچنانک نیش کزدم بر کنی</p></div>
<div class="m2"><p>تا که یابد او ز کشتن ایمنی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بر کنی دندان پر زهری ز مار</p></div>
<div class="m2"><p>تا رهد مار از بلای سنگسار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هیچ نکشد نفس را جز ظل پیر</p></div>
<div class="m2"><p>دامن آن نفس‌کش را سخت گیر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چون بگیری سخت آن توفیق هوست</p></div>
<div class="m2"><p>در تو هر قوت که آید جذب اوست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ما رمیت اذ رمیت راست دان</p></div>
<div class="m2"><p>هر چه کارد جان بود از جان جان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دست گیرنده ویست و بردبار</p></div>
<div class="m2"><p>دم بدم آن دم ازو اومید دار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نیست غم گر دیر بی او مانده‌ای</p></div>
<div class="m2"><p>دیرگیر و سخت‌گیرش خوانده‌ای</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دیر گیرد سخت گیرد رحمتش</p></div>
<div class="m2"><p>یک دمت غایب ندارد حضرتش</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ور تو خواهی شرح این وصل و ولا</p></div>
<div class="m2"><p>از سر اندیشه می‌خوان والضحی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ور تو گویی هم بدیها از ویست</p></div>
<div class="m2"><p>لیک آن نقصان فضل او کیست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>آن بدی دادن کمال اوست هم</p></div>
<div class="m2"><p>من مثالی گویمت ای محتشم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کرد نقاشی دو گونه نقشها</p></div>
<div class="m2"><p>نقشهای صاف و نقشی بی صفا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>نقش یوسف کرد و حور خوش‌سرشت</p></div>
<div class="m2"><p>نقش عفریتان و ابلیسان زشت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هر دو گونه نقش استادی اوست</p></div>
<div class="m2"><p>زشتی او نیست آن رادی اوست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>زشت را در غایت زشتی کند</p></div>
<div class="m2"><p>جمله زشتیها به گردش بر تند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تا کمال دانشش پیدا شود</p></div>
<div class="m2"><p>منکر استادیش رسوا شود</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ور نداند زشت کردن ناقص است</p></div>
<div class="m2"><p>زین سبب خلاق گبر و مخلص است</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>پس ازین رو کفر و ایمان شاهدند</p></div>
<div class="m2"><p>بر خداوندیش و هر دو ساجدند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>لیک مؤمن دان که طوعا ساجدست</p></div>
<div class="m2"><p>زانک جویای رضا و قاصدست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هست کرها گبر هم یزدان‌پرست</p></div>
<div class="m2"><p>لیک قصد او مرادی دیگرست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>قلعهٔ سلطان عمارت می‌کند</p></div>
<div class="m2"><p>لیک دعوی امارت می‌کند</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گشته یاغی تا که ملک او بود</p></div>
<div class="m2"><p>عاقبت خود قلعه سلطانی شود</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>مؤمن آن قلعه برای پادشاه</p></div>
<div class="m2"><p>می‌کند معمور نه از بهر جاه</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>زشت گوید ای شه زشت‌آفرین</p></div>
<div class="m2"><p>قادری بر خوب و بر زشت مهین</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>خوب گوید ای شه حسن و بها</p></div>
<div class="m2"><p>پاک گردانیدیم از عیبها</p></div></div>