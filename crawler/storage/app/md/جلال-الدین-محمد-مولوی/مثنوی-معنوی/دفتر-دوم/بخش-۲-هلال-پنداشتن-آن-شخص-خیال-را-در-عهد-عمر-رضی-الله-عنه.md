---
title: >-
    بخش ۲ - هلال پنداشتن آن شخص خیال را در عهد عمر رضی الله عنه
---
# بخش ۲ - هلال پنداشتن آن شخص خیال را در عهد عمر رضی الله عنه

<div class="b" id="bn1"><div class="m1"><p>ماه روزه گشت در عهد عمر</p></div>
<div class="m2"><p>بر سر کوهی دویدند آن نفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هلال روزه را گیرند فال</p></div>
<div class="m2"><p>آن یکی گفت ای عمر اینک هلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون عمر بر آسمان مه را ندید</p></div>
<div class="m2"><p>گفت کین مه از خیال تو دمید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورنه من بیناترم افلاک را</p></div>
<div class="m2"><p>چون نمی‌بینم هلال پاک را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت تر کن دست و بر ابرو بمال</p></div>
<div class="m2"><p>آنگهان تو در نگر سوی هلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونک او تر کرد ابرو مه ندید</p></div>
<div class="m2"><p>گفت ای شه نیست مه شد ناپدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آری موی ابرو شد کمان</p></div>
<div class="m2"><p>سوی تو افکند تیری از گمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون یکی مو کژ شد او را راه زد</p></div>
<div class="m2"><p>تا به دعوی لاف دید ماه زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موی کژ چون پردهٔ گردون بود</p></div>
<div class="m2"><p>چون همه اجزات کژ شد چون بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راست کن اجزات را از راستان</p></div>
<div class="m2"><p>سر مکش ای راست‌رو ز آن آستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم ترازو را ترازو راست کرد</p></div>
<div class="m2"><p>هم ترازو را ترازو کاست کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که با ناراستان هم‌سنگ شد</p></div>
<div class="m2"><p>در کمی افتاد و عقلش دنگ شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رو اشداء علی‌الکفار باش</p></div>
<div class="m2"><p>خاک بر دلداری اغیار پاش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر سر اغیار چون شمشیر باش</p></div>
<div class="m2"><p>هین مکن روباه‌بازی شیر باش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا ز غیرت از تو یاران نسکلند</p></div>
<div class="m2"><p>زانک آن خاران عدو این گلند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آتش اندر زن به گرگان چون سپند</p></div>
<div class="m2"><p>زانک آن گرگان عدو یوسفند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان بابا گویدت ابلیس هین</p></div>
<div class="m2"><p>تا بدم بفریبدت دیو لعین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این چنین تلبیس با بابات کرد</p></div>
<div class="m2"><p>آدمی را این سیه‌رخ مات کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر سر شطرنج چستست این غراب</p></div>
<div class="m2"><p>تو مبین بازی به چشم نیم‌خواب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زانک فرزین‌بندها داند بسی</p></div>
<div class="m2"><p>که بگیرد در گلویت چون خسی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در گلو ماند خس او سالها</p></div>
<div class="m2"><p>چیست آن خس مهر جاه و مالها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مال خس باشد چو هست ای بی‌ثبات</p></div>
<div class="m2"><p>در گلویت مانع آب حیات</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر برد مالت عدوی پر فنی</p></div>
<div class="m2"><p>ره‌زنی را برده باشد ره‌زنی</p></div></div>