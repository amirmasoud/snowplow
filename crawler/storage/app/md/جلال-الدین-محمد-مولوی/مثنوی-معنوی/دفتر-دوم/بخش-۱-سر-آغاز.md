---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>مدتی این مثنوی تاخیر شد</p></div>
<div class="m2"><p>مهلتی بایست تا خون شیر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نزاید بخت تو فرزند نو</p></div>
<div class="m2"><p>خون نگردد شیر شیرین خوش شنو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ضیاء الحق حسام الدین عنان</p></div>
<div class="m2"><p>باز گردانید ز اوج آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به معراج حقایق رفته بود</p></div>
<div class="m2"><p>بی‌بهارش غنچه‌ها ناکفته بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ز دریا سوی ساحل بازگشت</p></div>
<div class="m2"><p>چنگ شعر مثنوی با ساز گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثنوی که صیقل ارواح بود</p></div>
<div class="m2"><p>باز گشتش روز استفتاح بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطلع تاریخ این سودا و سود</p></div>
<div class="m2"><p>سال اندر ششصد و شصت و دو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبلی زینجا برفت و بازگشت</p></div>
<div class="m2"><p>بهر صید این معانی بازگشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساعد شه مسکن این باز باد</p></div>
<div class="m2"><p>تا ابد بر خلق این در باز باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفت این در هوا و شهوتست</p></div>
<div class="m2"><p>ورنه اینجا شربت اندر شربتست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این دهان بر بند تا بینی عیان</p></div>
<div class="m2"><p>چشم‌بند آن جهان حلق و دهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای دهان تو خود دهانهٔ دوزخی</p></div>
<div class="m2"><p>وی جهان تو بر مثال برزخی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نور باقی پهلوی دنیای دون</p></div>
<div class="m2"><p>شیر صافی پهلوی جوهای خون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون درو گامی زنی بی احتیاط</p></div>
<div class="m2"><p>شیر تو خون می‌شودر از اختلاط</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک قدم زد آدم اندر ذوق نفس</p></div>
<div class="m2"><p>شد فراق صدر جنت طوق نفس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچو دیو از وی فرشته می‌گریخت</p></div>
<div class="m2"><p>بهر نانی چند آب چشم ریخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرچه یک مو بد گنه کو جسته بود</p></div>
<div class="m2"><p>لیک آن مو در دو دیده رسته بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود آدم دیدهٔ نور قدیم</p></div>
<div class="m2"><p>موی در دیده بود کوه عظیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر در آن آدم بکردی مشورت</p></div>
<div class="m2"><p>در پشیمانی نگفتی معذرت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زانک با عقلی چو عقلی جفت شد</p></div>
<div class="m2"><p>مانع بد فعلی و بد گفت شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نفس با نفس دگر چون یار شد</p></div>
<div class="m2"><p>عقل جزوی عاطل و بی‌کار شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون ز تنهایی تو نومیدی شوی</p></div>
<div class="m2"><p>زیر سایهٔ یار خورشیدی شوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رو بجو یار خدایی را تو زود</p></div>
<div class="m2"><p>چون چنان کردی خدا یار تو بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنک در خلوت نظر بر دوختست</p></div>
<div class="m2"><p>آخر آن را هم ز یار آموختست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خلوت از اغیار باید نه ز یار</p></div>
<div class="m2"><p>پوستین بهر دی آمد نه بهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عقل با عقل دگر دوتا شود</p></div>
<div class="m2"><p>نور افزون گشت و ره پیدا شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نفس با نفس دگر خندان شود</p></div>
<div class="m2"><p>ظلمت افزون گشت و ره پنهان شود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یار چشم تست ای مرد شکار</p></div>
<div class="m2"><p>از خس و خاشاک او را پاک دار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هین بجاروب زبان گردی مکن</p></div>
<div class="m2"><p>چشم را از خس ره‌آوردی مکن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون که مؤمن آینهٔ مؤمن بود</p></div>
<div class="m2"><p>روی او ز آلودگی ایمن بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یار آیینست جان را در حزن</p></div>
<div class="m2"><p>در رخ آیینه ای جان دم مزن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا نپوشد روی خود را در دمت</p></div>
<div class="m2"><p>دم فرو خوردن بباید هر دمت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کم ز خاکی چونک خاکی یار یافت</p></div>
<div class="m2"><p>از بهاری صد هزار انوار یافت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن درختی کو شود با یار جفت</p></div>
<div class="m2"><p>از هوای خوش ز سر تا پا شکفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در خزان چون دید او یار خلاف</p></div>
<div class="m2"><p>در کشید او رو و سر زیر لحاف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت یار بد بلا آشفتنست</p></div>
<div class="m2"><p>چونک او آمد طریقم خفتنست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس بخسپم باشم از اصحاب کهف</p></div>
<div class="m2"><p>به ز دقیانوس آن محبوس لهف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یقظه‌شان مصروف دقیانوس بود</p></div>
<div class="m2"><p>خوابشان سرمایهٔ ناموس بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خواب بیداریست چون با دانشست</p></div>
<div class="m2"><p>وای بیداری که با نادان نشست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چونک زاغان خیمه بر بهمن زدند</p></div>
<div class="m2"><p>بلبلان پنهان شدند و تن زدند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زانک بی گلزار بلبل خامشست</p></div>
<div class="m2"><p>غیبت خورشید بیداری‌کشست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آفتابا ترک این گلشن کنی</p></div>
<div class="m2"><p>تا که تحت الارض را روشن کنی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آفتاب معرفت را نقل نیست</p></div>
<div class="m2"><p>مشرق او غیر جان و عقل نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خاصه خورشید کمالی کان سریست</p></div>
<div class="m2"><p>روز و شب کردار او روشن‌گریست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مطلع شمس آی گر اسکندری</p></div>
<div class="m2"><p>بعد از آن هرجا روی نیکو فری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بعد از آن هر جا روی مشرق شود</p></div>
<div class="m2"><p>شرقها بر مغربت عاشق شود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حس خفاشت سوی مغرب دوان</p></div>
<div class="m2"><p>حس درپاشت سوی مشرق روان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>راه حس راه خرانست ای سوار</p></div>
<div class="m2"><p>ای خران را تو مزاحم شرم دار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پنج حسی هست جز این پنج حس</p></div>
<div class="m2"><p>آن چو زر سرخ و این حسها چو مس</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اندر آن بازار کاهل محشرند</p></div>
<div class="m2"><p>حس مس را چون حس زر کی خرند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>حس ابدان قوت ظلمت می‌خورد</p></div>
<div class="m2"><p>حس جان از آفتابی می‌چرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای ببرده رخت حسها سوی غیب</p></div>
<div class="m2"><p>دست چون موسی برون آور ز جیب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ای صفاتت آفتاب معرفت</p></div>
<div class="m2"><p>و آفتاب چرخ بند یک صفت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گاه خورشیدی و گه دریا شوی</p></div>
<div class="m2"><p>گاه کوه قاف و گه عنقا شوی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو نه این باشی نه آن در ذات خویش</p></div>
<div class="m2"><p>ای فزون از وهمها وز بیش بیش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>روح با علمست و با عقلست یار</p></div>
<div class="m2"><p>روح را با تازی و ترکی چه کار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از تو ای بی نقش با چندین صور</p></div>
<div class="m2"><p>هم مشبه هم موحد خیره‌سر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گه مشبه را موحد می‌کند</p></div>
<div class="m2"><p>گه موحد را صور ره می‌زند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گه ترا گوید ز مستی بوالحسن</p></div>
<div class="m2"><p>یا صغیر السن یا رطب البدن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گاه نقش خویش ویران می‌کند</p></div>
<div class="m2"><p>آن پی تنزیه جانان می‌کند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چشم حس را هست مذهب اعتزال</p></div>
<div class="m2"><p>دیدهٔ عقلست سنی در وصال</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سخرهٔ حس‌اند اهل اعتزال</p></div>
<div class="m2"><p>خویش را سنی نمایند از ضلال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر که بیرون شد ز حس سنی ویست</p></div>
<div class="m2"><p>اهل بینش چشم عقل خوش‌پیست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر بدیدی حس حیوان شاه را</p></div>
<div class="m2"><p>پس بدیدی گاو و خر الله را</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گر نبودی حس دیگر مر ترا</p></div>
<div class="m2"><p>جز حس حیوان ز بیرون هوا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پس بنی‌آدم مکرم کی بدی</p></div>
<div class="m2"><p>کی به حس مشترک محرم شدی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نامصور یا مصور گفتنت</p></div>
<div class="m2"><p>باطل آمد بی ز صورت رفتنت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نامصور یا مصور پیش اوست</p></div>
<div class="m2"><p>کو همه مغزست و بیرون شد ز پوست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر تو کوری نیست بر اعمی حرج</p></div>
<div class="m2"><p>ورنه رو کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پرده‌های دیده را داروی صبر</p></div>
<div class="m2"><p>هم بسوزد هم بسازد شرح صدر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آینهٔ دل چون شود صافی و پاک</p></div>
<div class="m2"><p>نقشها بینی برون از آب و خاک</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هم ببینی نقش و هم نقاش را</p></div>
<div class="m2"><p>فرش دولت را و هم فراش را</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چون خلیل آمد خیال یار من</p></div>
<div class="m2"><p>صورتش بت معنی او بت‌شکن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شکر یزدان را که چون او شد پدید</p></div>
<div class="m2"><p>در خیالش جان خیال خود بدید</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>خاک درگاهت دلم را می‌فریفت</p></div>
<div class="m2"><p>خاک بر وی کو ز خاکت می‌شکیفت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گفتم ار خوبم پذیرم این ازو</p></div>
<div class="m2"><p>ورنه خود خندید بر من زشت‌رو</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چاره آن باشد که خود را بنگرم</p></div>
<div class="m2"><p>ورنه او خندد مرا من کی خرم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>او جمیلست و محب للجمال</p></div>
<div class="m2"><p>کی جوان نو گزیند پیر زال</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>خوب خوبی را کند جذب این بدان</p></div>
<div class="m2"><p>طیبات و طیبین بر وی بخوان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در جهان هر چیز چیزی جذب کرد</p></div>
<div class="m2"><p>گرم گرمی را کشید و سرد سرد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>قسم باطل باطلان را می‌کشند</p></div>
<div class="m2"><p>باقیان از باقیان هم سرخوشند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ناریان مر ناریان را جاذب‌اند</p></div>
<div class="m2"><p>نوریان مر نوریان را طالب‌اند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چشم چون بستی ترا جان کندنیست</p></div>
<div class="m2"><p>چشم را از نور روزن صبر نیست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چشم چون بستی ترا تاسه گرفت</p></div>
<div class="m2"><p>نور چشم از نور روزن کی شکفت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تاسهٔ تو جذب نور چشم بود</p></div>
<div class="m2"><p>تا بپیوندد به نور روز زود</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چشم باز ار تاسه گیرد مر ترا</p></div>
<div class="m2"><p>دانک چشم دل ببستی بر گشا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>آن تقاضای دو چشم دل شناس</p></div>
<div class="m2"><p>کو همی‌جوید ضیای بی‌قیاس</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چون فراق آن دو نور بی‌ثبات</p></div>
<div class="m2"><p>تاسه آوردت گشادی چشمهات</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>پس فراق آن دو نور پایدار</p></div>
<div class="m2"><p>تا سه می‌آرد مر آن را پاس دار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>او چو می‌خواند مرا من بنگرم</p></div>
<div class="m2"><p>لایق جذبم و یا بد پیکرم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>گر لطیفی زشت را در پی کند</p></div>
<div class="m2"><p>تسخری باشد که او بر وی کند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>کی ببینم روی خود را ای عجب</p></div>
<div class="m2"><p>تا چه رنگم همچو روزم یا چو شب</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نقش جان خویش من جستم بسی</p></div>
<div class="m2"><p>هیچ می‌ننمود نقشم از کسی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گفتم آخر آینه از بهر چیست</p></div>
<div class="m2"><p>تا بداند هر کسی کو چیست و کیست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>آینهٔ آهن برای پوستهاست</p></div>
<div class="m2"><p>آینهٔ سیمای جان سنگی‌بهاست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>آینهٔ جان نیست الا روی یار</p></div>
<div class="m2"><p>روی آن یاری که باشد زان دیار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گفتم ای دل آینهٔ کلی بجو</p></div>
<div class="m2"><p>رو به دریا کار بر ناید بجو</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>زین طلب بنده به کوی تو رسید</p></div>
<div class="m2"><p>درد مریم را به خرمابن کشید</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>دیدهٔ تو چون دلم را دیده شد</p></div>
<div class="m2"><p>شد دل نادیده غرق دیده شد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>آینهٔ کلی ترا دیدم ابد</p></div>
<div class="m2"><p>دیدم اندر چشم تو من نقش خود</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گفتم آخر خویش را من یافتم</p></div>
<div class="m2"><p>در دو چشمش راه روشن یافتم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گفت وهمم کان خیال تست هان</p></div>
<div class="m2"><p>ذات خود را از خیال خود بدان</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>نقش من از چشم تو آواز داد</p></div>
<div class="m2"><p>که منم تو تو منی در اتحاد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>کاندرین چشم منیر بی زوال</p></div>
<div class="m2"><p>از حقایق راه کی یابد خیال</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>در دو چشم غیر من تو نقش خود</p></div>
<div class="m2"><p>گر ببینی آن خیالی دان و رد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>زانک سرمهٔ نیستی در می‌کشد</p></div>
<div class="m2"><p>باده از تصویر شیطان می‌چشد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چشمشان خانهٔ خیالست و عدم</p></div>
<div class="m2"><p>نیستها را هست بیند لاجرم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چشم من چون سرمه دید از ذوالجلال</p></div>
<div class="m2"><p>خانهٔ هستیست نه خانهٔ خیال</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>تا یکی مو باشد از تو پیش چشم</p></div>
<div class="m2"><p>در خیالت گوهری باشد چو یشم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>یشم را آنگه شناسی از گهر</p></div>
<div class="m2"><p>کز خیال خود کنی کلی عبر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>یک حکایت بشنو ای گوهر شناس</p></div>
<div class="m2"><p>تا بدانی تو عیان را از قیاس</p></div></div>