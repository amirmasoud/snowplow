---
title: >-
    بخش ۷۷ - قصهٔ منافقان و مسجد ضرار ساختن ایشان
---
# بخش ۷۷ - قصهٔ منافقان و مسجد ضرار ساختن ایشان

<div class="b" id="bn1"><div class="m1"><p>یک مثال دیگر اندر کژروی</p></div>
<div class="m2"><p>شاید ار از نقل قرآن بشنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چنین کژ بازیی در جفت و طاق</p></div>
<div class="m2"><p>با نبی می‌باختند اهل نفاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز برای عز دین احمدی</p></div>
<div class="m2"><p>مسجدی سازیم و بود آن مرتدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چنین کژ بازیی می‌باختند</p></div>
<div class="m2"><p>مسجدی جز مسجد او ساختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سقف و فرش و قبه‌اش آراسته</p></div>
<div class="m2"><p>لیک تفریق جماعت خواسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزد پیغامبر بلابه آمدند</p></div>
<div class="m2"><p>همچو اشتر پیش او زانو زدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کای رسول حق برای محسنی</p></div>
<div class="m2"><p>سوی آن مسجد قدم رنجه کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا مبارک گردد از اقدام تو</p></div>
<div class="m2"><p>تا قیامت تازه بادا نام تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسجد روز گلست و روز ابر</p></div>
<div class="m2"><p>مسجد روز ضرورت وقت فقر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا غریبی یابد آنجا خیر و جا</p></div>
<div class="m2"><p>تا فراوان گردد این خدمت‌سرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا شعار دین شود بسیار و پر</p></div>
<div class="m2"><p>زانک با یاران شود خوش کار مر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساعتی آن جایگه تشریف ده</p></div>
<div class="m2"><p>تزکیه‌مان کن ز ما تعریف ده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مسجد و اصحاب مسجد را نواز</p></div>
<div class="m2"><p>تو مهی ما شب دمی با ما بساز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا شود شب از جمالت همچو روز</p></div>
<div class="m2"><p>ای جمالت آفتاب جان‌فروز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای دریغا کان سخن از دل بدی</p></div>
<div class="m2"><p>تا مراد آن نفر حاصل شدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لطف کاید بی دل و جان در زبان</p></div>
<div class="m2"><p>همچو سبزهٔ تون بود ای دوستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم ز دورش بنگر و اندر گذر</p></div>
<div class="m2"><p>خوردن و بو را نشاید ای پسر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سوی لطف بی وفایان هین مرو</p></div>
<div class="m2"><p>کان پل ویران بود نیکو شنو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر قدم را جاهلی بر وی زند</p></div>
<div class="m2"><p>بشکند پل و آن قدم را بشکند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر کجا لشکر شکسته میشود</p></div>
<div class="m2"><p>از دو سه سست مخنث می‌بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در صف آید با سلاح او مردوار</p></div>
<div class="m2"><p>دل برو بنهند کاینک یار غار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رو بگرداند چو بیند زخم را</p></div>
<div class="m2"><p>رفتن او بشکند پشت ترا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این درازست و فراوان می‌شود</p></div>
<div class="m2"><p>وآنچ مقصودست پنهان می‌شود</p></div></div>