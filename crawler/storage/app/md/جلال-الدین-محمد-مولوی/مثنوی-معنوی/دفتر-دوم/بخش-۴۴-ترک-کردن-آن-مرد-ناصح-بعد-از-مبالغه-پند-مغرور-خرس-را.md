---
title: >-
    بخش ۴۴ - ترک کردن آن مرد ناصح بعد از مبالغهٔ پند مغرور خرس را
---
# بخش ۴۴ - ترک کردن آن مرد ناصح بعد از مبالغهٔ پند مغرور خرس را

<div class="b" id="bn1"><div class="m1"><p>آن مسلمان ترک ابله کرد و تفت</p></div>
<div class="m2"><p>زیر لب لاحول گویان باز رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت چون از جد و بندم وز جدال</p></div>
<div class="m2"><p>در دل او پیش می‌زاید خیال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس ره پند و نصیحت بسته شد</p></div>
<div class="m2"><p>امر اعرض عنهم پیوسته شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دوایت می‌فزاید درد پس</p></div>
<div class="m2"><p>قصه با طالب بگو بر خوان عبس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونک اعمی طالب حق آمدست</p></div>
<div class="m2"><p>بهر فقر او را نشاید سینه خست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو حریصی بر رشاد مهتران</p></div>
<div class="m2"><p>تا بیاموزند عام از سروران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>احمدا دیدی که قومی از ملوک</p></div>
<div class="m2"><p>مستمع گشتند گشتی خوش که بوک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این رئیسان یار دین گردند خوش</p></div>
<div class="m2"><p>بر عرب اینها سرند و بر حبش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذرد این صیت از بصره و تبوک</p></div>
<div class="m2"><p>زانک الناس علی دین الملوک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین سبب تو از ضریر مهتدی</p></div>
<div class="m2"><p>رو بگردانیدی و تنگ آمدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کندرین فرصت کم افتد این مناخ</p></div>
<div class="m2"><p>تو ز یارانی و وقت تو فراخ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مزدحم می‌گردیم در وقت تنگ</p></div>
<div class="m2"><p>این نصیحت می‌کنم نه از خشم و جنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>احمدا نزد خدا این یک ضریر</p></div>
<div class="m2"><p>بهتر از صد قیصرست و صد وزیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یاد الناس معادن هین بیار</p></div>
<div class="m2"><p>معدنی باشد فزون از صد هزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>معدن لعل و عقیق مکتنس</p></div>
<div class="m2"><p>بهترست از صد هزاران کان مس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>احمدا اینجا ندارد مال سود</p></div>
<div class="m2"><p>سینه باید پر ز عشق و درد و دود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اعمیی روشن‌دل آمد در مبند</p></div>
<div class="m2"><p>پند او را ده که حق اوست پند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر دو سه ابله ترا منکر شدند</p></div>
<div class="m2"><p>تلخ کی گردی چو هستی کان قند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر دو سه ابله ترا تهمت نهد</p></div>
<div class="m2"><p>حق برای تو گواهی می‌دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت از اقرار عالم فارغم</p></div>
<div class="m2"><p>آنک حق باشد گواه او را چه غم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر خفاشی را ز خورشیدی خوریست</p></div>
<div class="m2"><p>آن دلیل آمد که آن خورشید نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نفرت خفاشکان باشد دلیل</p></div>
<div class="m2"><p>که منم خورشید تابان جلیل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر گلابی را جعل راغب شود</p></div>
<div class="m2"><p>آن دلیل ناگلابی می‌کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر شود قلبی خریدار محک</p></div>
<div class="m2"><p>در محکی‌اش در آید نقص و شک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دزد شب خواهد نه روز این را بدان</p></div>
<div class="m2"><p>شب نیم روزم که تابم در جهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فارقم فاروقم و غلبیروار</p></div>
<div class="m2"><p>تا که که از من نمی‌یابد گذار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آرد را پیدا کنم من از سبوس</p></div>
<div class="m2"><p>تا نمایم کین نقوشست آن نفوس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من چو میزان خدایم در جهان</p></div>
<div class="m2"><p>وا نمایم هر سبک را از گران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گاو را داند خدا گوساله‌ای</p></div>
<div class="m2"><p>خر خریداری و در خور کاله‌ای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من نه گاوم تا که گوسالم خرد</p></div>
<div class="m2"><p>من نه خارم که اشتری از من چرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>او گمان دارد که با من جور کرد</p></div>
<div class="m2"><p>بلک از آیینهٔ من روفت گرد</p></div></div>