---
title: >-
    بخش ۹۷ - دعوی کردن آن شخص کی خدای تعالی مرا نمی‌گیرد به گناه و جواب گفتن شعیب علیه السلام مرورا
---
# بخش ۹۷ - دعوی کردن آن شخص کی خدای تعالی مرا نمی‌گیرد به گناه و جواب گفتن شعیب علیه السلام مرورا

<div class="b" id="bn1"><div class="m1"><p>آن یکی می‌گفت در عهد شعیب</p></div>
<div class="m2"><p>که خدا از من بسی دیدست عیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند دید از من گناه و جرمها</p></div>
<div class="m2"><p>وز کرم یزدان نمی‌گیرد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حق تعالی گفت در گوش شعیب</p></div>
<div class="m2"><p>در جواب او فصیح از راه غیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بگفتی چند کردم من گناه</p></div>
<div class="m2"><p>وز کرم نگرفت در جرمم اله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس می‌گویی و مقلوب ای سفیه</p></div>
<div class="m2"><p>ای رها کرده ره و بگرفته تیه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند چندت گیرم و تو بی‌خبر</p></div>
<div class="m2"><p>در سلاسل مانده‌ای پا تا بسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنگ تو بر توت ای دیگ سیاه</p></div>
<div class="m2"><p>کرد سیمای درونت را تباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دلت زنگار بر زنگارها</p></div>
<div class="m2"><p>جمع شد تا کور شد ز اسرارها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر زند آن دود بر دیگ نوی</p></div>
<div class="m2"><p>آن اثر بنماید ار باشد جوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانک هر چیزی بضد پیدا شود</p></div>
<div class="m2"><p>بر سپیدی آن سیه رسوا شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون سیه شد دیگ پس تاثیر دود</p></div>
<div class="m2"><p>بعد ازین بر وی که بیند زود زود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرد آهنگر که او زنگی بود</p></div>
<div class="m2"><p>دود را با روش هم‌رنگی بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرد رومی کو کند آهنگری</p></div>
<div class="m2"><p>رویش ابلق گردد از دودآوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس بداند زود تاثیر گناه</p></div>
<div class="m2"><p>تا بنالد زود گوید ای اله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون کند اصرار و بد پیشه کند</p></div>
<div class="m2"><p>خاک اندر چشم اندیشه کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توبه نندیشد دگر شیرین شود</p></div>
<div class="m2"><p>بر دلش آن جرم تا بی‌دین شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن پشیمانی و یا رب رفت ازو</p></div>
<div class="m2"><p>شست بر آیینه زنگ پنج تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آهنش را زنگها خوردن گرفت</p></div>
<div class="m2"><p>گوهرش را زنگ کم کردن گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون نویسی کاغد اسپید بر</p></div>
<div class="m2"><p>آن نبشته خوانده آید در نظر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون نویسی بر سر بنوشته خط</p></div>
<div class="m2"><p>فهم ناید خواندنش گردد غلط</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کان سیاهی بر سیاهی اوفتاد</p></div>
<div class="m2"><p>هر دو خط شد کور و معنیی نداد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور سِیُم باره نویسی بر سرش</p></div>
<div class="m2"><p>پس سیه کردی چو جان پر شرش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پس چه چاره جز پناه چاره‌گر</p></div>
<div class="m2"><p>ناامیدی مس و اکسیرش نظر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ناامیدیها به پیش او نهید</p></div>
<div class="m2"><p>تا ز درد بی‌دوا بیرون جهید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون شعیب این نکته‌ها با وی بگفت</p></div>
<div class="m2"><p>زان دم جان در دل او گل شکفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جان او بشنید وحی آسمان</p></div>
<div class="m2"><p>گفت اگر بگرفت ما را کو نشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت یا رب دفع من می‌گوید او</p></div>
<div class="m2"><p>آن گرفتن را نشان می‌جوید او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت ستارم نگویم رازهاش</p></div>
<div class="m2"><p>جز یکی رمز از برای ابتلاش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یک نشان آنک می‌گیرم ورا</p></div>
<div class="m2"><p>آنک طاعت دارد و صوم و دعا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وز نماز و از زکات و غیر آن</p></div>
<div class="m2"><p>لیک یک ذره ندارد ذوق جان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>می‌کند طاعات و افعال سنی</p></div>
<div class="m2"><p>لیک یک ذره ندارد چاشنی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طاعتش نغزست و معنی نغز نی</p></div>
<div class="m2"><p>جوزها بسیار و در وی مغز نی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ذوق باید تا دهد طاعات بر</p></div>
<div class="m2"><p>مغز باید تا دهد دانه شجر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دانهٔ بی‌مغز کی گردد نهال</p></div>
<div class="m2"><p>صورت بی‌جان نباشد جز خیال</p></div></div>