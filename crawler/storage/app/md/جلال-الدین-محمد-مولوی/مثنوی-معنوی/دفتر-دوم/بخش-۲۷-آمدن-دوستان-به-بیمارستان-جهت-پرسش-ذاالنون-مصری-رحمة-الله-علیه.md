---
title: >-
    بخش ۲۷ - آمدن دوستان به بیمارستان جهت پرسش ذاالنون مصری رحمة الله علیه
---
# بخش ۲۷ - آمدن دوستان به بیمارستان جهت پرسش ذاالنون مصری رحمة الله علیه

<div class="b" id="bn1"><div class="m1"><p>این چنین ذالنون مصری را فتاد</p></div>
<div class="m2"><p>کاندرو شور و جنونی نو بزاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شور چندان شد که تا فوق فلک</p></div>
<div class="m2"><p>می‌رسید از وی جگرها را نمک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هین منه تو شور خود ای شوره‌خاک</p></div>
<div class="m2"><p>پهلوی شور خداوندان پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلق را تاب جنون او نبود</p></div>
<div class="m2"><p>آتش او ریشهاشان می‌ربود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونک در ریش عوام آتش فتاد</p></div>
<div class="m2"><p>بند کردندش به زندانی نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست امکان واکشیدن این لگام</p></div>
<div class="m2"><p>گرچه زین ره تنگ می‌آیند عام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده این شاهان ز عامه خوف جان</p></div>
<div class="m2"><p>کین گره کورند و شاهان بی‌نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونک حکم اندر کف رندان بود</p></div>
<div class="m2"><p>لاجرم ذاالنون در زندان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکسواره می‌رود شاه عظیم</p></div>
<div class="m2"><p>در کف طفلان چنین در یتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در چه دریا نهان در قطره‌ای</p></div>
<div class="m2"><p>آفتابی مخفی اندر ذره‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفتابی خویش را ذره نمود</p></div>
<div class="m2"><p>واندک اندک روی خود را بر گشود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جملهٔ ذرات در وی محو شد</p></div>
<div class="m2"><p>عالم از وی مست گشت و صحو شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون قلم در دست غداری بود</p></div>
<div class="m2"><p>بی گمان منصور بر داری بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون سفیهان‌راست این کار و کیا</p></div>
<div class="m2"><p>لازم آمد یقتلون الانبیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>انبیا را گفته قومی راه گم</p></div>
<div class="m2"><p>از سفه انا تطیرنا بکم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهل ترسا بین امان انگیخته</p></div>
<div class="m2"><p>زان خداوندی که گشت آویخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون بقول اوست مصلوب جهود</p></div>
<div class="m2"><p>پس مرورا امن کی تاند نمود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون دل آن شاه زیشان خون بود</p></div>
<div class="m2"><p>عصمت و انت فیهم چون بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زر خالص را و زرگر را خطر</p></div>
<div class="m2"><p>باشد از قلاب خاین بیشتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یوسفان از رشک زشتان مخفی‌اند</p></div>
<div class="m2"><p>کز عدو خوبان در آتش می‌زیند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یوسفان از مکر اخوان در چهند</p></div>
<div class="m2"><p>کز حسد یوسف به گرگان می‌دهند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از حسد بر یوسف مصری چه رفت</p></div>
<div class="m2"><p>این حسد اندر کمین گرگیست زفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لاجرم زین گرگ یعقوب حلیم</p></div>
<div class="m2"><p>داشت بر یوسف همیشه خوف و بیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرگ ظاهر گرد یوسف خود نگشت</p></div>
<div class="m2"><p>این حسد در فعل از گرگان گذشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رحم کرد این گرگ وز عذر لبق</p></div>
<div class="m2"><p>آمده که انا ذهبنا نستبق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صد هزاران گرگ را این مکر نیست</p></div>
<div class="m2"><p>عاقبت رسوا شود این گرگ بیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زانک حشر حاسدان روز گزند</p></div>
<div class="m2"><p>بی گمان بر صورت گرگان کنند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حشر پر حرص خس مردارخوار</p></div>
<div class="m2"><p>صورت خوکی بود روز شمار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زانیان را گند اندام نهان</p></div>
<div class="m2"><p>خمرخواران را بود گند دهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گند مخفی کان به دلها می‌رسید</p></div>
<div class="m2"><p>گشت اندر حشر محسوس و پدید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیشه‌ای آمد وجود آدمی</p></div>
<div class="m2"><p>بر حذر شو زین وجود ار زان دمی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در وجود ما هزاران گرگ و خوک</p></div>
<div class="m2"><p>صالح و ناصالح و خوب و خشوک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حکم آن خوراست کان غالبترست</p></div>
<div class="m2"><p>چونک زر بیش از مس آمد آن زرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سیرتی کان بر وجودت غالبست</p></div>
<div class="m2"><p>هم بر آن تصویر حشرت واجبست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ساعتی گرگی در آید در بشر</p></div>
<div class="m2"><p>ساعتی یوسف‌رخی همچون قمر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می‌رود از سینه‌ها در سینه‌ها</p></div>
<div class="m2"><p>از ره پنهان صلاح و کینه‌ها</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلک خود از آدمی در گاو و خر</p></div>
<div class="m2"><p>می‌رود دانایی و علم و هنر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اسپ سکسک می‌شود رهوار و رام</p></div>
<div class="m2"><p>خرس بازی می‌کند بز هم سلام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رفت اندر سگ ز آدمیان هوس</p></div>
<div class="m2"><p>تا شبان شد یا شکاری یا حرس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در سگ اصحاب خویی زان وفود</p></div>
<div class="m2"><p>رفت تا جویای الله گشته بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر زمان در سینه نوعی سر کند</p></div>
<div class="m2"><p>گاه دیو و گه ملک گه دام و دد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زان عجب بیشه که هر شیر آگهست</p></div>
<div class="m2"><p>تا به دام سینه‌ها پنهان رهست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دزدیی کن از درون مرجان جان</p></div>
<div class="m2"><p>ای کم از سگ از درون عارفان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چونک دزدی باری آن در لطیف</p></div>
<div class="m2"><p>چونک حامل می‌شوی باری شریف</p></div></div>