---
title: >-
    بخش ۹۰ - ترسیدن کودک از آن شخص صاحب جثه و گفتن آن شخص کی ای کودک مترس کی من نامردم
---
# بخش ۹۰ - ترسیدن کودک از آن شخص صاحب جثه و گفتن آن شخص کی ای کودک مترس کی من نامردم

<div class="b" id="bn1"><div class="m1"><p>کنک زفتی کودکی را یافت فرد</p></div>
<div class="m2"><p>زرد شد کودک ز بیم قصد مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ایمن باش ای زیبای من</p></div>
<div class="m2"><p>که تو خواهی بود بر بالای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من اگر هولم مخنث دان مرا</p></div>
<div class="m2"><p>همچو اشتر بر نشین می‌ران مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت مردان و معنی این چنین</p></div>
<div class="m2"><p>از برون آدم درون دیو لعین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دهل را مانی ای زفت چو عاد</p></div>
<div class="m2"><p>که برو آن شاخ را می‌کوفت باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روبهی اشکار خود را باد داد</p></div>
<div class="m2"><p>بهر طبلی همچو خیک پر ز باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ندید اندر دهل او فربهی</p></div>
<div class="m2"><p>گفت خوکی به ازین خیک تهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روبهان ترسند ز آواز دهل</p></div>
<div class="m2"><p>عاقلش چندان زند که لا تقل</p></div></div>