---
title: >-
    بخش ۹۵ - طعن زدن بیگانه در شیخ و جواب گفتن مرید شیخ او را
---
# بخش ۹۵ - طعن زدن بیگانه در شیخ و جواب گفتن مرید شیخ او را

<div class="b" id="bn1"><div class="m1"><p>آن یکی یک شیخ را تهمت نهاد</p></div>
<div class="m2"><p>کو بدست و نیست بر راه رشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شارب خمرست و سالوس و خبیث</p></div>
<div class="m2"><p>مر مریدان را کجا باشد مغیث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یکی گفتش ادب را هوش دار</p></div>
<div class="m2"><p>خرد نبود این چنین ظن بر کبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور ازو و دور از آن اوصاف او</p></div>
<div class="m2"><p>که ز سیلی تیره گردد صاف او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چنین بهتان منه بر اهل حق</p></div>
<div class="m2"><p>کین خیال تست برگردان ورق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نباشد ور بود ای مرغ خاک</p></div>
<div class="m2"><p>بحر قلزم را ز مرداری چه باک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست دون القلتین و حوض خرد</p></div>
<div class="m2"><p>که تواند قطره‌ایش از کار برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش ابراهیم را نبود زیان</p></div>
<div class="m2"><p>هر که نمرودیست گو می‌ترس از آن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس نمرودست و عقل و جان خلیل</p></div>
<div class="m2"><p>روح در عینست و نفس اندر دلیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این دلیل راه ره‌رو را بود</p></div>
<div class="m2"><p>کو بهر دم در بیابان گم شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>واصلان را نیست جز چشم و چراغ</p></div>
<div class="m2"><p>از دلیل و راهشان باشد فراغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر دلیلی گفت آن مرد وصال</p></div>
<div class="m2"><p>گفت بهر فهم اصحاب جدال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر طفل نو پدر تی‌تی کند</p></div>
<div class="m2"><p>گرچه عقلش هندسهٔ گیتی کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کم نگردد فضل استاد از علو</p></div>
<div class="m2"><p>گر الف چیزی ندارد گوید او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از پی تعلیم آن بسته‌دهن</p></div>
<div class="m2"><p>از زبان خود برون باید شدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در زبان او بباید آمدن</p></div>
<div class="m2"><p>تا بیاموزد ز تو او علم و فن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس همه خلقان چو طفلان ویند</p></div>
<div class="m2"><p>لازمست این پیر را در وقت پند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن مرید شیخ بد گوینده را</p></div>
<div class="m2"><p>آن به کفر و گمرهی آکنده را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت خود را تو مزن بر تیغ تیز</p></div>
<div class="m2"><p>هین مکن با شاه و با سلطان ستیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حوض با دریا اگر پهلو زند</p></div>
<div class="m2"><p>خویش را از بیخ هستی بر کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست بحری کو کران دارد که تا</p></div>
<div class="m2"><p>تیره گردد او ز مردار شما</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کفر را حدست و اندازه بدان</p></div>
<div class="m2"><p>شیخ و نور شیخ را نبود کران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیش بی حد هرچه محدودست لاست</p></div>
<div class="m2"><p>کل شیء غیر وجه الله فناست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کفر و ایمان نیست آنجایی که اوست</p></div>
<div class="m2"><p>زانک او مغزست و این دو رنگ و پوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این فناها پردهٔ آن وجه گشت</p></div>
<div class="m2"><p>چون چراغ خفیه اندر زیر طشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس سر این تن حجاب آن سرست</p></div>
<div class="m2"><p>پیش آن سر این سر تن کافرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کیست کافر غافل از ایمان شیخ</p></div>
<div class="m2"><p>کیست مرده بی خبر از جان شیخ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جان نباشد جز خبر در آزمون</p></div>
<div class="m2"><p>هر که را افزون خبر جانش فزون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جان ما از جان حیوان بیشتر</p></div>
<div class="m2"><p>از چه زان رو که فزون دارد خبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پس فزون از جان ما جان ملک</p></div>
<div class="m2"><p>کو منزه شد ز حس مشترک</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وز ملک جان خداوندان دل</p></div>
<div class="m2"><p>باشد افزون تو تحیر را بهل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زان سبب آدم بود مسجودشان</p></div>
<div class="m2"><p>جان او افزونترست از بودشان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ورنه بهتر را سجود دون‌تری</p></div>
<div class="m2"><p>امر کردن هیچ نبود در خوری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کی پسندد عدل و لطف کردگار</p></div>
<div class="m2"><p>که گلی سجده کند در پیش خار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جان چو افزون شد گذشت از انتها</p></div>
<div class="m2"><p>شد مطیعش جان جمله چیزها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرغ و ماهی و پری و آدمی</p></div>
<div class="m2"><p>زانک او بیشست و ایشان در کمی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ماهیان سوزن‌گر دلقش شوند</p></div>
<div class="m2"><p>سوزنان را رشته‌ها تابع بوند</p></div></div>