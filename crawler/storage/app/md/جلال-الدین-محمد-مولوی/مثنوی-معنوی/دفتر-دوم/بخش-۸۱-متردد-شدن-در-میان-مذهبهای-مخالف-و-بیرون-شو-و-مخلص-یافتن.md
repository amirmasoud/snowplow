---
title: >-
    بخش ۸۱ - متردد شدن در میان مذهبهای مخالف و بیرون‌شو و مخلص یافتن
---
# بخش ۸۱ - متردد شدن در میان مذهبهای مخالف و بیرون‌شو و مخلص یافتن

<div class="b" id="bn1"><div class="m1"><p>همچنانک هر کسی در معرفت</p></div>
<div class="m2"><p>می‌کند موصوف غیبی را صفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلسفی از نوع دیگر کرده شرح</p></div>
<div class="m2"><p>باحثی مر گفت او را کرده جرح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وآن دگر در هر دو طعنه می‌زند</p></div>
<div class="m2"><p>وآن دگر از زرق جانی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر یک از ره این نشانها زان دهند</p></div>
<div class="m2"><p>تا گمان آید که ایشان زان ده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این حقیقت دان نه حق‌اند این همه</p></div>
<div class="m2"><p>نه به کلی گمرهانند این رمه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانک بی حق باطلی ناید پدید</p></div>
<div class="m2"><p>قلب را ابله به بوی زر خرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نبودی در جهان نقدی روان</p></div>
<div class="m2"><p>قلبها را خرج کردن کی توان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نباشد راست کی باشد دروغ</p></div>
<div class="m2"><p>آن دروغ از راست می‌گیرد فروغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر امید راست کژ را می‌خرند</p></div>
<div class="m2"><p>زهر در قندی رود آنگه خورند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نباشد گندم محبوب‌نوش</p></div>
<div class="m2"><p>چه برد گندم‌نمای جو فروش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس مگو کین جمله دمها باطل‌اند</p></div>
<div class="m2"><p>باطلان بر بوی حق دام دل‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس مگو جمله خیالست و ضلال</p></div>
<div class="m2"><p>بی‌حقیقت نیست در عالم خیال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حق شب قدرست در شبها نهان</p></div>
<div class="m2"><p>تا کند جان هر شبی را امتحان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه همه شبها بود قدر ای جوان</p></div>
<div class="m2"><p>نه همه شبها بود خالی از آن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در میان دلق‌پوشان یک فقیر</p></div>
<div class="m2"><p>امتحان کن وانک حقست آن بگیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مؤمن کیس ممیز کو که تا</p></div>
<div class="m2"><p>باز داند حیزکان را از فتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرنه معیوبات باشد در جهان</p></div>
<div class="m2"><p>تاجران باشند جمله ابلهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس بود کالاشناسی سخت سهل</p></div>
<div class="m2"><p>چونک عیبی نیست چه نااهل و اهل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور همه عیبست دانش سود نیست</p></div>
<div class="m2"><p>چون همه چوبست اینجا عود نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنک گوید جمله حق‌اند احمقیست</p></div>
<div class="m2"><p>وانک گوید جمله باطل او شقیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تاجران انبیا کردند سود</p></div>
<div class="m2"><p>تاجران رنگ و بو کور و کبود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می‌نماید مار اندر چشم مال</p></div>
<div class="m2"><p>هر دو چشم خویش را نیکو بمال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منگر اندر غبطهٔ این بیع و سود</p></div>
<div class="m2"><p>بنگر اندر خسر فرعون و ثمود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اندرین گردون مکرر کن نظر</p></div>
<div class="m2"><p>زانک حق فرمود ثم ارجع بصر</p></div></div>