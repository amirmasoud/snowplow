---
title: >-
    بخش ۸۲ - بقیهٔ حکایت نابینا و مصحف
---
# بخش ۸۲ - بقیهٔ حکایت نابینا و مصحف

<div class="b" id="bn1"><div class="m1"><p>مرد مهمان صبرکرد و ناگهان</p></div>
<div class="m2"><p>کشف گشتش حال مشکل در زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیم‌شب آواز قرآن را شنید</p></div>
<div class="m2"><p>جست از خواب آن عجایب را بدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ز مصحف کور می‌خواندی درست</p></div>
<div class="m2"><p>گشت بی‌صبر و ازو آن حال جست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت آیا ای عجب با چشم کور</p></div>
<div class="m2"><p>چون همی‌خوانی همی‌بینی سطور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچ می‌خوانی بر آن افتاده‌ای</p></div>
<div class="m2"><p>دست را بر حرف آن بنهاده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اصبعت در سیر پیدا می‌کند</p></div>
<div class="m2"><p>که نظر بر حرف داری مستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ای گشته ز جهل تن جدا</p></div>
<div class="m2"><p>این عجب می‌داری از صنع خدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من ز حق در خواستم کای مستعان</p></div>
<div class="m2"><p>بر قرائت من حریصم همچو جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیستم حافظ مرا نوری بده</p></div>
<div class="m2"><p>در دو دیده وقت خواندن بی‌گره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز ده دو دیده‌ام را آن زمان</p></div>
<div class="m2"><p>که بگیرم مصحف و خوانم عیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد از حضرت ندا کای مرد کار</p></div>
<div class="m2"><p>ای بهر رنجی به ما اومیدوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حسن ظنست و امیدی خوش ترا</p></div>
<div class="m2"><p>که ترا گوید بهر دم برتر آ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر زمان که قصد خواندن باشدت</p></div>
<div class="m2"><p>یا ز مصحفها قرائت بایدت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من در آن دم وا دهم چشم ترا</p></div>
<div class="m2"><p>تا فرو خوانی معظم جوهرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنان کرد و هر آنگاهی که من</p></div>
<div class="m2"><p>وا گشایم مصحف اندر خواندن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن خبیری که نشد غافل ز کار</p></div>
<div class="m2"><p>آن گرامی پادشاه و کردگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باز بخشد بینشم آن شاه فرد</p></div>
<div class="m2"><p>در زمان همچون چراغ شب‌نورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین سبب نبود ولی را اعتراض</p></div>
<div class="m2"><p>هرچه بستاند فرستد اعتیاض</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بسوزد باغت انگورت دهد</p></div>
<div class="m2"><p>در میان ماتمی سورت دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن شل بی‌دست را دستی دهد</p></div>
<div class="m2"><p>کان غمها را دل مستی دهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لا نسلم و اعتراض از ما برفت</p></div>
<div class="m2"><p>چون عوض می‌آید از مفقود زفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چونک بی آتش مرا گرمی رسد</p></div>
<div class="m2"><p>راضیم گر آتشش ما را کشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بی چراغی چون دهد او روشنی</p></div>
<div class="m2"><p>گر چراغت شد چه افغان می‌کنی</p></div></div>