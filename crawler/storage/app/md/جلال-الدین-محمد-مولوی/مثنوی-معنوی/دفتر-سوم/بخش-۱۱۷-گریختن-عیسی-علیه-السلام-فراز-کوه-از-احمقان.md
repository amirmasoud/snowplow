---
title: >-
    بخش ۱۱۷ - گریختن عیسی علیه السلام فراز کوه از احمقان
---
# بخش ۱۱۷ - گریختن عیسی علیه السلام فراز کوه از احمقان

<div class="b" id="bn1"><div class="m1"><p>عیسی مریم به کوهی می‌گریخت</p></div>
<div class="m2"><p>شیرگویی خون او می‌خواست ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یکی در پی دوید و گفت خیر</p></div>
<div class="m2"><p>در پیت کس نیست چه گریزی چو طیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با شتاب او آنچنان می‌تاخت جفت</p></div>
<div class="m2"><p>کز شتاب خود جواب او نگفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دو میدان در پی عیسی براند</p></div>
<div class="m2"><p>پس بجد جد عیسی را بخواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کز پی مرضات حق یک لحظه بیست</p></div>
<div class="m2"><p>که مرا اندر گریزت مشکلیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کی این سو می‌گریزی ای کریم</p></div>
<div class="m2"><p>نه پیت شیر و نه خصم و خوف و بیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت از احمق گریزانم برو</p></div>
<div class="m2"><p>می‌رهانم خویش را بندم مشو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت آخر آن مسیحا نه توی</p></div>
<div class="m2"><p>که شود کور و کر از تو مستوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت آری گفت آن شه نیستی</p></div>
<div class="m2"><p>که فسون غیب را ماویستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بخوانی آن فسون بر مرده‌ای</p></div>
<div class="m2"><p>برجهد چون شیر صید آورده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت آری آن منم گفتا که تو</p></div>
<div class="m2"><p>نه ز گل مرغان کنی ای خوب‌رو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت آری گفت پس ای روح پاک</p></div>
<div class="m2"><p>هرچه خواهی می‌کنی از کیست باک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با چنین برهان که باشد در جهان</p></div>
<div class="m2"><p>که نباشد مر ترا از بندگان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت عیسی که به ذات پاک حق</p></div>
<div class="m2"><p>مبدع تن خالق جان در سبق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حرمت ذات و صفات پاک او</p></div>
<div class="m2"><p>که بود گردون گریبان‌چاک او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کان فسون و اسم اعظم را که من</p></div>
<div class="m2"><p>بر کر و بر کور خواندم شد حسن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر که سنگین بخواندم شد شکاف</p></div>
<div class="m2"><p>خرقه را بدرید بر خود تا بناف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برتن مرده بخواندم گشت حی</p></div>
<div class="m2"><p>بر سر لاشی بخواندم گشت شی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خواندم آن را بر دل احمق بود</p></div>
<div class="m2"><p>صد هزاران بار و درمانی نشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سنگ خارا گشت و زان خو بر نگشت</p></div>
<div class="m2"><p>ریگ شد کز وی نروید هیچ کشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت حکمت چیست کآنجا اسم حق</p></div>
<div class="m2"><p>سود کرد اینجا نبود آن را سبق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن همان رنجست و این رنجی چرا</p></div>
<div class="m2"><p>او نشد این را و آن را شد دوا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت رنج احمقی قهر خداست</p></div>
<div class="m2"><p>رنج و کوری نیست قهر آن ابتلاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ابتلا رنجیست کان رحم آورد</p></div>
<div class="m2"><p>احمقی رنجیست کان زخم آورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنچ داغ اوست مهر او کرده است</p></div>
<div class="m2"><p>چاره‌ای بر وی نیارد برد دست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز احمقان بگریز چون عیسی گریخت</p></div>
<div class="m2"><p>صحبت احمق بسی خونها که ریخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اندک اندک آب را دزدد هوا</p></div>
<div class="m2"><p>دین چنین دزدد هم احمق از شما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرمیت را دزدد و سردی دهد</p></div>
<div class="m2"><p>همچو آن کو زیر کون سنگی نهد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن گریز عیسی نه از بیم بود</p></div>
<div class="m2"><p>آمنست او آن پی تعلیم بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمهریر ار پر کند آفاق را</p></div>
<div class="m2"><p>چه غم آن خورشید با اشراق را</p></div></div>