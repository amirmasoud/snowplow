---
title: >-
    بخش ۸ - فریفتن روستایی شهری را و بدعوت  خواندن بلابه و الحاح بسیار
---
# بخش ۸ - فریفتن روستایی شهری را و بدعوت  خواندن بلابه و الحاح بسیار

<div class="b" id="bn1"><div class="m1"><p>ای برادر بود اندر ما مضی</p></div>
<div class="m2"><p>شهریی با روستایی آشنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روستایی چون سوی شهر آمدی</p></div>
<div class="m2"><p>خرگه اندر کوی آن شهری زدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو مه و سه ماه مهمانش بدی</p></div>
<div class="m2"><p>بر دکان او و بر خوانش بدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر حوایج را که بودش آن زمان</p></div>
<div class="m2"><p>راست کردی مرد شهری رایگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو به شهری کرد و گفت ای خواجه تو</p></div>
<div class="m2"><p>هیچ می‌نایی سوی ده فرجه‌جو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الله الله جمله فرزندان بیار</p></div>
<div class="m2"><p>کین زمان گلشنست و نوبهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا بتابستان بیا وقت ثمر</p></div>
<div class="m2"><p>تا ببندم خدمتت را من کمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیل و فرزندان و قومت را بیار</p></div>
<div class="m2"><p>در ده ما باش سه ماه و چهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بهاران خطهٔ ده خوش بود</p></div>
<div class="m2"><p>کشت‌زار و لالهٔ دلکش بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وعده دادی شهری او را دفع حال</p></div>
<div class="m2"><p>تا بر آمد بعد وعده هشت سال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او بهر سالی همی‌گفتی که کی</p></div>
<div class="m2"><p>عزم خواهی کرد کامد ماه دی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او بهانه ساختی کامسال‌مان</p></div>
<div class="m2"><p>از فلان خطه بیامد میهمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سال دیگر گر توانم وا رهید</p></div>
<div class="m2"><p>از مهمات آن طرف خواهم دوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت هستند آن عیالم منتظر</p></div>
<div class="m2"><p>بهر فرزندان تو ای اهل بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باز هر سالی چو لکلک آمدی</p></div>
<div class="m2"><p>تا مقیم قبهٔ شهری شدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواجه هر سالی ز زر و مال خویش</p></div>
<div class="m2"><p>خرج او کردی گشادی بال خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آخرین کرت سه ماه آن پهلوان</p></div>
<div class="m2"><p>خوان نهادش بامدادان و شبان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از خجالت باز گفت او خواجه را</p></div>
<div class="m2"><p>چند وعده چند بفریبی مرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت خواجه جسم و جانم وصل‌جوست</p></div>
<div class="m2"><p>لیک هر تحویل اندر حکم هوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آدمی چون کشتی است و بادبان</p></div>
<div class="m2"><p>تا کی آرد باد را آن بادران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باز سوگندان بدادش کای کریم</p></div>
<div class="m2"><p>گیر فرزندان بیا بنگر نعیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دست او بگرفت سه کرت بعهد</p></div>
<div class="m2"><p>کالله الله زو بیا بنمای جهد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بعد ده سال و بهر سالی چنین</p></div>
<div class="m2"><p>لابه‌ها و وعده‌های شکرین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کودکان خواجه گفتند ای پدر</p></div>
<div class="m2"><p>ماه و ابر و سایه هم دارد سفر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حقها بر وی تو ثابت کرده‌ای</p></div>
<div class="m2"><p>رنجها در کار او بس برده‌ای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>او همی‌خواهد که بعضی حق آن</p></div>
<div class="m2"><p>وا گزارد چون شوی تو میهمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بس وصیت کرد ما را او نهان</p></div>
<div class="m2"><p>که کشیدش سوی ده لابه‌کنان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت حقست این ولی ای سیبویه</p></div>
<div class="m2"><p>اتق من شر من احسنت الیه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دوستی تخم دم آخر بود</p></div>
<div class="m2"><p>ترسم از وحشت که آن فاسد شود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صحبتی باشد چو شمشیر قطوع</p></div>
<div class="m2"><p>همچو دی در بوستان و در زروع</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صحبتی باشد چو فصل نوبهار</p></div>
<div class="m2"><p>زو عمارتها و دخل بی‌شمار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حزم آن باشد که ظن بد بری</p></div>
<div class="m2"><p>تا گریزی و شوی از بد بری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حزم سؤ الظن گفتست آن رسول</p></div>
<div class="m2"><p>هر قدم را دام می‌دان ای فضول</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>روی صحرا هست هموار و فراخ</p></div>
<div class="m2"><p>هر قدم دامیست کم ران اوستاخ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن بز کوهی دود که دام کو</p></div>
<div class="m2"><p>چون بتازد دامش افتد در گلو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آنک می‌گفتی که کو اینک ببین</p></div>
<div class="m2"><p>دشت می‌دیدی نمی‌دیدی کمین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بی کمین و دام و صیاد ای عیار</p></div>
<div class="m2"><p>دنبه کی باشد میان کشت‌زار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آنک گستاخ آمدند اندر زمین</p></div>
<div class="m2"><p>استخوان و کله‌هاشان را ببین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون به گورستان روی ای مرتضا</p></div>
<div class="m2"><p>استخوانشان را بپرس از ما مضی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا بظاهر بینی آن مستان کور</p></div>
<div class="m2"><p>چون فرو رفتند در چاه غرور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چشم اگر داری تو کورانه میا</p></div>
<div class="m2"><p>ور نداری چشم دست آور عصا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن عصای حزم و استدلال را</p></div>
<div class="m2"><p>چون نداری دید می‌کن پیشوا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ور عصای حزم و استدلال نیست</p></div>
<div class="m2"><p>بی عصاکش بر سر هر ره مه‌ایست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گام زان سان نه که نابینا نهد</p></div>
<div class="m2"><p>تا که پا از چاه و از سگ وا رهد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>لرز لرزان و بترس و احتیاط</p></div>
<div class="m2"><p>می‌نهد پا تا نیفتد در خباط</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای ز دودی جسته در ناری شده</p></div>
<div class="m2"><p>لقمه جسته لقمهٔ ماری شده</p></div></div>