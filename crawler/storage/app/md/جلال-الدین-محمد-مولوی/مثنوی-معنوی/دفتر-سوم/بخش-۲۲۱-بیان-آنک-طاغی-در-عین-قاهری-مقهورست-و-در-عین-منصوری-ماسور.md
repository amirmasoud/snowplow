---
title: >-
    بخش ۲۲۱ - بیان آنک طاغی در عین قاهری مقهورست و در عین منصوری ماسور
---
# بخش ۲۲۱ - بیان آنک طاغی در عین قاهری مقهورست و در عین منصوری ماسور

<div class="b" id="bn1"><div class="m1"><p>دزد قهرخواجه کرد و زر کشید</p></div>
<div class="m2"><p>او بدان مشغول خود والی رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز خواجه آن زمان بگریختی</p></div>
<div class="m2"><p>کی برو والی حشر انگیختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاهری دزد مقهوریش بود</p></div>
<div class="m2"><p>زانک قهر او سر او را ربود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غالبی بر خواجه دام او شود</p></div>
<div class="m2"><p>تا رسد والی و بستاند قود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که تو بر خلق چیره گشته‌ای</p></div>
<div class="m2"><p>در نبرد و غالبی آغشته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن به قاصد منهزم کردستشان</p></div>
<div class="m2"><p>تا ترا در حلقه می‌آرد کشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هین عنان در کش پی این منهزم</p></div>
<div class="m2"><p>در مران تا تو نگردی منخزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کشانیدت بدین شیوه به دام</p></div>
<div class="m2"><p>حمله بینی بعد از آن اندر زحام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل ازین غالب شدن کی گشت شاد</p></div>
<div class="m2"><p>چون درین غالب شدن دید او فساد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیزچشم آمد خرد بینای پیش</p></div>
<div class="m2"><p>که خدایش سرمه کرد از کحل خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت پیغامبر که هستند از فنون</p></div>
<div class="m2"><p>اهل جنت در خصومتها زبون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از کمال حزم و سؤ الظن خویش</p></div>
<div class="m2"><p>نه ز نقص و بد دلی و ضعف کیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در فره دادن شنیده در کمون</p></div>
<div class="m2"><p>حکمت لولا رجال مومنون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست‌کوتاهی ز کفار لعین</p></div>
<div class="m2"><p>فرض شد بهر خلاص مؤمنین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قصهٔ عهد حدیبیه بخوان</p></div>
<div class="m2"><p>کف ایدیکم تمامت زان بدان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیز اندر غالبی هم خویش را</p></div>
<div class="m2"><p>دید او مغلوب دام کبریا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان نمی‌خندم من از زنجیرتان</p></div>
<div class="m2"><p>که بکردم ناگهان شبگیرتان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زان همی‌خندم که با زنجیر و غل</p></div>
<div class="m2"><p>می‌کشمتان سوی سروستان و گل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای عجب کز آتش بی‌زینهار</p></div>
<div class="m2"><p>بسته می‌آریمتان تا سبزه‌زار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از سوی دوزخ به زنجیر گران</p></div>
<div class="m2"><p>می‌کشمتان تا بهشت جاودان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر مقلد را درین ره نیک و بد</p></div>
<div class="m2"><p>همچنان بسته به حضرت می‌کشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جمله در زنجیر بیم و ابتلا</p></div>
<div class="m2"><p>می‌روند این ره بغیر اولیا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>می‌کشند این راه را بیگاروار</p></div>
<div class="m2"><p>جز کسانی واقف از اسرار کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهد کن تا نور تو رخشان شود</p></div>
<div class="m2"><p>تا سلوک و خدمتت آسان شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کودکان را می‌بری مکتب به زور</p></div>
<div class="m2"><p>زانک هستند از فواید چشم‌کور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون شود واقف به مکتب می‌دود</p></div>
<div class="m2"><p>جانش از رفتن شکفته می‌شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>می‌رود کودک به مکتب پیچ پیچ</p></div>
<div class="m2"><p>چون ندید از مزد کار خویش هیچ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون کند در کیسه دانگی دست‌مزد</p></div>
<div class="m2"><p>آنگهان بی‌خواب گردد شب چو دزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهد کن تا مزد طاعت در رسد</p></div>
<div class="m2"><p>بر مطیعان آنگهت آید حسد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ائتیا کرها مقلد گشته را</p></div>
<div class="m2"><p>ائتیا طوعا صفا بسرشته را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این محب حق ز بهر علتی</p></div>
<div class="m2"><p>و آن دگر را بی غرض خود خلتی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این محب دایه لیک از بهر شیر</p></div>
<div class="m2"><p>و آن دگر دل داده بهر این ستیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طفل را از حسن او آگاه نه</p></div>
<div class="m2"><p>غیر شیر او را ازو دلخواه نه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>و آن دگر خود عاشق دایه بود</p></div>
<div class="m2"><p>بی غرض در عشق یک‌رایه بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس محب حق باومید و بترس</p></div>
<div class="m2"><p>دفتر تقلید می‌خواند بدرس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>و آن محب حق ز بهر حق کجاست</p></div>
<div class="m2"><p>که ز اغراض و ز علتها جداست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر چنین و گر چنان چون طالبست</p></div>
<div class="m2"><p>جذب حق او را سوی حق جاذبست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر محب حق بود لغیره</p></div>
<div class="m2"><p>کی ینال دائما من خیره</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یا محب حق بود لعینه</p></div>
<div class="m2"><p>لاسواه خائفا من بینه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر دو را این جست و جوها زان سریست</p></div>
<div class="m2"><p>این گرفتاری دل زان دلبریست</p></div></div>