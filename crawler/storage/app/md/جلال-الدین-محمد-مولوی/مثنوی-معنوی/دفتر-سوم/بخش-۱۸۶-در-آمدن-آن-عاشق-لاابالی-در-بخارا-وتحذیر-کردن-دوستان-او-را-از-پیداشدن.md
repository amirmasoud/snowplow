---
title: >-
    بخش ۱۸۶ - در آمدن آن عاشق لاابالی در بخارا وتحذیر کردن دوستان او را از پیداشدن
---
# بخش ۱۸۶ - در آمدن آن عاشق لاابالی در بخارا وتحذیر کردن دوستان او را از پیداشدن

<div class="b" id="bn1"><div class="m1"><p>اندر آمد در بخارا شادمان</p></div>
<div class="m2"><p>پیش معشوق خود و دارالامان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو آن مستی که پرد بر اثیر</p></div>
<div class="m2"><p>مه کنارش گیرد و گوید که گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه دیدش در بخارا گفت خیز</p></div>
<div class="m2"><p>پیش از پیدا شدن منشین گریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ترا می‌جوید آن شه خشمگین</p></div>
<div class="m2"><p>تا کشد از جان تو ده ساله کین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الله الله درمیا در خون خویش</p></div>
<div class="m2"><p>تکیه کم کن بر دم و افسون خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شحنهٔ صدر جهان بودی و راد</p></div>
<div class="m2"><p>معتمد بودی مهندس اوستاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غدو کردی وز جزا بگریختی</p></div>
<div class="m2"><p>رسته بودی باز چون آویختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بلا بگریختی با صد حیل</p></div>
<div class="m2"><p>ابلهی آوردت اینجا یا اجل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که عقلت بر عطارد دق کند</p></div>
<div class="m2"><p>عقل و عاقل را قضا احمق کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نحس خرگوشی که باشد شیرجو</p></div>
<div class="m2"><p>زیرکی و عقل و چالاکیت کو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست صد چندین فسونهای قضا</p></div>
<div class="m2"><p>گفت اذا جاء القضا ضاق الفضا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صد ره و مخلص بود از چپ و راست</p></div>
<div class="m2"><p>از قضا بسته شود کو اژدهاست</p></div></div>