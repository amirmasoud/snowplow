---
title: >-
    بخش ۱۳۲ - حکایت نذر کردن سگان هر زمستان کی این تابستان چون بیاید خانه سازیم از بهر زمستان را
---
# بخش ۱۳۲ - حکایت نذر کردن سگان هر زمستان کی این تابستان چون بیاید خانه سازیم از بهر زمستان را

<div class="b" id="bn1"><div class="m1"><p>سگ زمستان جمع گردد استخوانش</p></div>
<div class="m2"><p>زخم سرما خرد گرداند چنانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو بگوید کین قدر تن که منم</p></div>
<div class="m2"><p>خانه‌ای از سنگ باید کردنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونک تابستان بیاید من بچنگ</p></div>
<div class="m2"><p>بهر سرما خانه‌ای سازم ز سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونک تابستان بیاید از گشاد</p></div>
<div class="m2"><p>استخوانها پهن گردد پوست شاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوید او چون زفت بیند خویش را</p></div>
<div class="m2"><p>در کدامین خانه گنجم ای کیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زفت گردد پا کشد در سایه‌ای</p></div>
<div class="m2"><p>کاهلی سیری غری خودرایه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویدش دل خانه‌ای ساز ای عمو</p></div>
<div class="m2"><p>گوید او در خانه کی گنجم بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>استخوان حرص تو در وقت درد</p></div>
<div class="m2"><p>درهم آید خرد گردد در نورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی از توبه بسازم خانه‌ای</p></div>
<div class="m2"><p>در زمستان باشدم استانه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بشد درد و شدت آن حرص زفت</p></div>
<div class="m2"><p>همچو سگ سودای خانه از تو رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکر نعمت خوشتر از نعمت بود</p></div>
<div class="m2"><p>شکرباره کی سوی نعمت رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکر جان نعمت و نعمت چو پوست</p></div>
<div class="m2"><p>ز آنک شکر آرد ترا تا کوی دوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نعمت آرد غفلت و شکر انتباه</p></div>
<div class="m2"><p>صید نعمت کن بدام شکر شاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نعمت شکرت کند پرچشم و میر</p></div>
<div class="m2"><p>تا کنی صد نعمت ایثار فقیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیر نوشی از طعام و نقل حق</p></div>
<div class="m2"><p>تا رود از تو شکم‌خواری و دق</p></div></div>