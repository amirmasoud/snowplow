---
title: >-
    بخش ۲۰ - چرب کردن مرد لافی لب و سبلت خود را هر بامداد به پوست دنبه و بیرون آمدن میان  حریفان کی من چنین خورده‌ام و چنان
---
# بخش ۲۰ - چرب کردن مرد لافی لب و سبلت خود را هر بامداد به پوست دنبه و بیرون آمدن میان  حریفان کی من چنین خورده‌ام و چنان

<div class="b" id="bn1"><div class="m1"><p>پوست دنبه یافت شخصی مستهان</p></div>
<div class="m2"><p>هر صباحی چرب کردی سبلتان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در میان منعمان رفتی که من</p></div>
<div class="m2"><p>لوت چربی خورده‌ام در انجمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بر سبلت نهادی در نوید</p></div>
<div class="m2"><p>رمز یعنی سوی سبلت بنگرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کین گواه صدق گفتار منست</p></div>
<div class="m2"><p>وین نشان چرب و شیرین خوردنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشکمش گفتی جواب بی‌طنین</p></div>
<div class="m2"><p>که اباد الله کید الکاذبین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاف تو ما را بر آتش بر نهاد</p></div>
<div class="m2"><p>کان سبال چرب تو بر کنده باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نبودی لاف زشتت ای گدا</p></div>
<div class="m2"><p>یک کریمی رحم افکندی به ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور نمودی عیب و کژ کم باختی</p></div>
<div class="m2"><p>یک طبیبی داروی او ساختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت حق که کژ مجنبان گوش و دم</p></div>
<div class="m2"><p>ینفعن الصادقین صدقهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت اندر کژ مخسپ ای محتلم</p></div>
<div class="m2"><p>آنچ داری وا نما و فاستقم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور نگویی عیب خود باری خمش</p></div>
<div class="m2"><p>از نمایش وز دغل خود را مکش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو نقدی یافتی مگشا دهان</p></div>
<div class="m2"><p>هست در ره سنگهای امتحان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سنگهای امتحان را نیز پیش</p></div>
<div class="m2"><p>امتحانها هست در احوال خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت یزدان از ولادت تا بحین</p></div>
<div class="m2"><p>یفتنون کل عام مرتین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امتحان در امتحانست ای پدر</p></div>
<div class="m2"><p>هین به کمتر امتحان خود را مخر</p></div></div>