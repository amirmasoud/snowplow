---
title: >-
    بخش ۹۲ - باز شدن آن شمعها هفت درخت
---
# بخش ۹۲ - باز شدن آن شمعها هفت درخت

<div class="b" id="bn1"><div class="m1"><p>باز هر یک مرد شد شکل درخت</p></div>
<div class="m2"><p>چشمم از سبزی ایشان نیکبخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانبهی برگ پیدا نیست شاخ</p></div>
<div class="m2"><p>برگ هم گم گشته از میوهٔ فراخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر درختی شاخ بر سدره زده</p></div>
<div class="m2"><p>سدره چه بود از خلا بیرون شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیخ هر یک رفته در قعر زمین</p></div>
<div class="m2"><p>زیرتر از گاو و ماهی بد یقین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیخشان از شاخ خندان‌روی‌تر</p></div>
<div class="m2"><p>عقل از آن اشکالشان زیر و زبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میوه‌ای که بر شکافیدی ز زور</p></div>
<div class="m2"><p>همچو آب از میوه جستی برق نور</p></div></div>