---
title: >-
    بخش ۱۹۱ - ملامت کردن اهل مسجد مهمان عاشق را از شب خفتن در آنجا و تهدید کردن مرورا
---
# بخش ۱۹۱ - ملامت کردن اهل مسجد مهمان عاشق را از شب خفتن در آنجا و تهدید کردن مرورا

<div class="b" id="bn1"><div class="m1"><p>قوم گفتندش که هین اینجا مخسپ</p></div>
<div class="m2"><p>تا نکوبد جانستانت همچو کسپ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که غریبی و نمی‌دانی ز حال</p></div>
<div class="m2"><p>کاندرین جا هر که خفت آمد زوال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اتفاقی نیست این ما بارها</p></div>
<div class="m2"><p>دیده‌ایم و جمله اصحاب نهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که آن مسجد شبی مسکن شدش</p></div>
<div class="m2"><p>نیم‌شب مرگ هلاهل آمدش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از یکی ما تابه صد این دیده‌ایم</p></div>
<div class="m2"><p>نه به تقلید از کسی بشنیده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت الدین نصیحه آن رسول</p></div>
<div class="m2"><p>آن نصیحت در لغت ضد غلول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این نصیحت راستی در دوستی</p></div>
<div class="m2"><p>در غلولی خاین و سگ‌پوستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی خیانت این نصیحت از وداد</p></div>
<div class="m2"><p>می‌نماییمت مگرد از عقل و داد</p></div></div>