---
title: >-
    بخش ۱۵۹ - خجل گشتن خروس پیش سگ به سبب دروغ شدن در آن سه وعده
---
# بخش ۱۵۹ - خجل گشتن خروس پیش سگ به سبب دروغ شدن در آن سه وعده

<div class="b" id="bn1"><div class="m1"><p>چند چند آخر دروغ و مکر تو</p></div>
<div class="m2"><p>خود نپرد جز دروغ از وکر تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت حاشا از من و از جنس من</p></div>
<div class="m2"><p>که بگردیم از دروغی ممتحن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما خروسان چون مؤذن راست‌گوی</p></div>
<div class="m2"><p>هم رقیب آفتاب و وقت‌جوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاسبان آفتابیم از درون</p></div>
<div class="m2"><p>گر کنی بالای ما طشتی نگون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاسبان آفتابند اولیا</p></div>
<div class="m2"><p>در بشر واقف ز اسرار خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اصل ما را حق پی بانگ نماز</p></div>
<div class="m2"><p>داد هدیه آدمی را در جهاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بناهنگام سهوی‌مان رود</p></div>
<div class="m2"><p>در اذان آن مقتل ما می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت ناهنگام حی عل فلاح</p></div>
<div class="m2"><p>خون ما را می‌کند خوار و مباح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنک معصوم آمد و پاک از غلط</p></div>
<div class="m2"><p>آن خروس جان وحی آمد فقط</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن غلامش مرد پیش مشتری</p></div>
<div class="m2"><p>شد زیان مشتری آن یکسری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او گریزانید مالش را ولیک</p></div>
<div class="m2"><p>خون خود را ریخت اندر یاب نیک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک زیان دفع زیانها می‌شدی</p></div>
<div class="m2"><p>جسم و مال ماست جانها را فدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش شاهان در سیاست‌گستری</p></div>
<div class="m2"><p>می‌دهی تو مال و سر را می‌خری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اعجمی چون گشته‌ای اندر قضا</p></div>
<div class="m2"><p>می‌گریزانی ز داور مال را</p></div></div>