---
title: >-
    بخش ۵۹ - عقول خلق متفاوتست در اصل فطرت و نزد  معتزله متساویست تفاوت عقول از تحصیل  علم است
---
# بخش ۵۹ - عقول خلق متفاوتست در اصل فطرت و نزد  معتزله متساویست تفاوت عقول از تحصیل  علم است

<div class="b" id="bn1"><div class="m1"><p>اختلاف عقلها در اصل بود</p></div>
<div class="m2"><p>بر وفاق سنیان باید شنود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خلاف قول اهل اعتزال</p></div>
<div class="m2"><p>که عقول از اصل دارند اعتدال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تجربه و تعلیم بیش و کم کند</p></div>
<div class="m2"><p>تا یکی را از یکی اعلم کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باطلست این زانک رای کودکی</p></div>
<div class="m2"><p>که ندارد تجربه در مسلکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دمید اندیشه‌ای زان طفل خرد</p></div>
<div class="m2"><p>پیر با صد تجربه بویی نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود فزون آن به که آن از فطرتست</p></div>
<div class="m2"><p>تا ز افزونی که جهد و فکرتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بگو دادهٔ خدا بهتر بود</p></div>
<div class="m2"><p>یاکه لنگی راهوارانه رود</p></div></div>