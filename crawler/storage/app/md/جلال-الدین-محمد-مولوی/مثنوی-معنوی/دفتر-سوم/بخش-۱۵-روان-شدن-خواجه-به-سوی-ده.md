---
title: >-
    بخش ۱۵ - روان شدن خواجه به سوی ده
---
# بخش ۱۵ - روان شدن خواجه به سوی ده

<div class="b" id="bn1"><div class="m1"><p>خواجه در کار آمد و تجهیز ساخت</p></div>
<div class="m2"><p>مرغ عزمش سوی ده اشتاب تاخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل و فرزندان سفر را ساختند</p></div>
<div class="m2"><p>رخت را بر گاو عزم انداختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادمانان و شتابان سوی ده</p></div>
<div class="m2"><p>که بری خوردیم از ده مژده ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقصد ما را چراگاه خوشست</p></div>
<div class="m2"><p>یار ما آنجا کریم و دلکشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هزاران آرزومان خوانده است</p></div>
<div class="m2"><p>بهر ما غرس کرم بنشانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما ذخیرهٔ ده زمستان دراز</p></div>
<div class="m2"><p>از بر او سوی شهر آریم باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلک باغ ایثار راه ما کند</p></div>
<div class="m2"><p>در میان جان خودمان جا کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجلوا اصحابنا کی تربحوا</p></div>
<div class="m2"><p>عقل می‌گفت از درون لا تفرحوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من رباح الله کونوا رابحین</p></div>
<div class="m2"><p>ان ربی لا یحب الفرحین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>افرحوا هونا بما آتاکم</p></div>
<div class="m2"><p>کل آت مشغل الهاکم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاد از وی شو مشو از غیر وی</p></div>
<div class="m2"><p>او بهارست و دگرها ماه دی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چه غیر اوست استدراج تست</p></div>
<div class="m2"><p>گرچه تخت و ملکتست و تاج تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاد از غم شو که غم دام لقاست</p></div>
<div class="m2"><p>اندرین ره سوی پستی ارتقاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غم یکی گنجیست و رنج تو چو کان</p></div>
<div class="m2"><p>لیک کی در گیرد این در کودکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کودکان چون نام بازی بشنوند</p></div>
<div class="m2"><p>جمله با خر گور هم تگ می‌دوند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای خران کور این سو دامهاست</p></div>
<div class="m2"><p>در کمین این سوی خون‌آشامهاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیرها پران کمان پنهان ز غیب</p></div>
<div class="m2"><p>بر جوانی می‌رسد صد تیر شیب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گام در صحرای دل باید نهاد</p></div>
<div class="m2"><p>زانک در صحرای گل نبود گشاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایمن آبادست دل ای دوستان</p></div>
<div class="m2"><p>چشمه‌ها و گلستان در گلستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عج الی القلب و سر یا ساریه</p></div>
<div class="m2"><p>فیه اشجار و عین جاریه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ده مرو ده مرد را احمق کند</p></div>
<div class="m2"><p>عقل را بی نور و بی رونق کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قول پیغامبر شنو ای مجتبی</p></div>
<div class="m2"><p>گور عقل آمد وطن در روستا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که را در رستا بود روزی و شام</p></div>
<div class="m2"><p>تا بماهی عقل او نبود تمام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بماهی احمقی با او بود</p></div>
<div class="m2"><p>از حشیش ده جز اینها چه درود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وانک ماهی باشد اندر روستا</p></div>
<div class="m2"><p>روزگاری باشدش جهل و عمی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ده چه باشد شیخ واصل ناشده</p></div>
<div class="m2"><p>دست در تقلید و حجت در زده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیش شهر عقل کلی این حواس</p></div>
<div class="m2"><p>چون خران چشم‌بسته در خراس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این رها کن صورت افسانه گیر</p></div>
<div class="m2"><p>هل تو دردانه تو گندم‌دانه گیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر بدر ره نیست هین بر می‌ستان</p></div>
<div class="m2"><p>گر بدان ره نیستت این سو بران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ظاهرش گیر ار چه ظاهر کژ پرد</p></div>
<div class="m2"><p>عاقبت ظاهر سوی باطن برد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اول هر آدمی خود صورتست</p></div>
<div class="m2"><p>بعد از آن جان کو جمال سیرتست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اول هر میوه جز صورت کیست</p></div>
<div class="m2"><p>بعد از آن لذت که معنی ویست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اولا خرگاه سازند و خرند</p></div>
<div class="m2"><p>ترک را زان پس به مهمان آورند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صورتت خرگاه دان معنیت ترک</p></div>
<div class="m2"><p>معنیت ملاح دان صورت چو فلک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بهر حق این را رها کن یک نفس</p></div>
<div class="m2"><p>تا خر خواجه بجنباند جرس</p></div></div>