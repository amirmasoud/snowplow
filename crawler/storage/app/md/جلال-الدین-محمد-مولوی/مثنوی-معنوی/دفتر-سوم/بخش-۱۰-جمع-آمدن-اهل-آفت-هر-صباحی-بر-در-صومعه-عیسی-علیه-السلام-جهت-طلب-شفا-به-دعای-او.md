---
title: >-
    بخش ۱۰ - جمع آمدن اهل آفت هر صباحی بر در صومعهٔ  عیسی علیه السلام جهت طلب شفا به دعای او
---
# بخش ۱۰ - جمع آمدن اهل آفت هر صباحی بر در صومعهٔ  عیسی علیه السلام جهت طلب شفا به دعای او

<div class="b" id="bn1"><div class="m1"><p>صومعهٔ عیسیست خوان اهل دل</p></div>
<div class="m2"><p>هان و هان ای مبتلا این در مهل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمع گشتندی ز هر اطراف خلق</p></div>
<div class="m2"><p>از ضریر و لنگ و شل و اهل دلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر در آن صومعهٔ عیسی صباح</p></div>
<div class="m2"><p>تا بدم اوشان رهاند از جناح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او چو فارغ گشتی از اوراد خویش</p></div>
<div class="m2"><p>چاشتگه بیرون شدی آن خوب‌کیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوق جوقی مبتلا دیدی نزار</p></div>
<div class="m2"><p>شسته بر در در امید و انتظار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی ای اصحاب آفت از خدا</p></div>
<div class="m2"><p>حاجت این جملگانتان شد روا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هین روان گردید بی رنج و عنا</p></div>
<div class="m2"><p>سوی غفاری و اکرام خدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جملگان چون اشتران بسته‌پای</p></div>
<div class="m2"><p>که گشایی زانوی ایشان برای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش دوان و شادمانه سوی خان</p></div>
<div class="m2"><p>از دعای او شدندی پا دوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آزمودی تو بسی آفات خویش</p></div>
<div class="m2"><p>یافتی صحت ازین شاهان کیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند آن لنگی تو رهوار شد</p></div>
<div class="m2"><p>چند جانت بی غم و آزار شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای مغفل رشته‌ای بر پای بند</p></div>
<div class="m2"><p>تا ز خود هم گم نگردی ای لوند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ناسپاسی و فراموشی تو</p></div>
<div class="m2"><p>یاد ناورد آن عسل‌نوشی تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لاجرم آن راه بر تو بسته شد</p></div>
<div class="m2"><p>چون دل اهل دل از تو خسته شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زودشان در یاب و استغفار کن</p></div>
<div class="m2"><p>همچو ابری گریه‌های زار کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا گلستانشان سوی تو بشکفد</p></div>
<div class="m2"><p>میوه‌های پخته بر خود وا کفد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم بر آن در گرد کم از سگ مباش</p></div>
<div class="m2"><p>با سگ کهف ار شدستی خواجه‌تاش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون سگان هم مر سگان را ناصح‌اند</p></div>
<div class="m2"><p>که دل اندر خانهٔ اول ببند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن در اول که خوردی استخوان</p></div>
<div class="m2"><p>سخت گیر و حق گزار آن را ممان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می‌گزندش تا ز ادب آنجا رود</p></div>
<div class="m2"><p>وز مقام اولین مفلح شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>می‌گزندش کای سگ طاغی برو</p></div>
<div class="m2"><p>با ولی نعمتت یاغی مشو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر همان در همچو حلقه بسته باش</p></div>
<div class="m2"><p>پاسبان و چابک و برجسته باش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صورت نقض وفای ما مباش</p></div>
<div class="m2"><p>بی‌وفایی را مکن بیهوده فاش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مر سگان را چون وفا آمد شعار</p></div>
<div class="m2"><p>رو سگان را ننگ و بدنامی میار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی‌وفایی چون سگان را عار بود</p></div>
<div class="m2"><p>بی‌وفایی چون روا داری نمود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حق تعالی فخر آورد از وفا</p></div>
<div class="m2"><p>گفت من اوفی بعهد غیرنا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی‌وفایی دان وفا با رد حق</p></div>
<div class="m2"><p>بر حقوق حق ندارد کس سبق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حق مادر بعد از آن شد کان کریم</p></div>
<div class="m2"><p>کرد او را از جنین تو غریم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صورتی کردت درون جسم او</p></div>
<div class="m2"><p>داد در حملش ورا آرام و خو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همچو جزو متصل دید او ترا</p></div>
<div class="m2"><p>متصل را کرد تدبیرش جدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حق هزاران صنعت و فن ساختست</p></div>
<div class="m2"><p>تا که مادر بر تو مهر انداختست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس حق حق سابق از مادر بود</p></div>
<div class="m2"><p>هر که آن حق را نداند خر بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آنک مادر آفرید و ضرع و شیر</p></div>
<div class="m2"><p>با پدر کردش قرین آن خود مگیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای خداوند ای قدیم احسان تو</p></div>
<div class="m2"><p>آنکه دانم وانکه نه هم آن تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو بفرمودی که حق را یاد کن</p></div>
<div class="m2"><p>زانک حق من نمی‌گردد کهن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یاد کن لطفی که کردم آن صبوح</p></div>
<div class="m2"><p>با شما از حفظ در کشتی نوح</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پیله بابایانتان را آن زمان</p></div>
<div class="m2"><p>دادم از طوفان و از موجش امان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آب آتش خو زمین بگرفته بود</p></div>
<div class="m2"><p>موج او مر اوج که را می‌ربود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حفظ کردم من نکردم ردتان</p></div>
<div class="m2"><p>در وجود جد جد جدتان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون شدی سر پشت پایت چون زنم</p></div>
<div class="m2"><p>کارگاه خویش ضایع چون کنم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون فدای بی‌وفایان می‌شوی</p></div>
<div class="m2"><p>از گمان بد بدان سو می‌روی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من ز سهو و بی‌وفاییها بری</p></div>
<div class="m2"><p>سوی من آیی گمان بد بری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>این گمان بد بر آنجا بر که تو</p></div>
<div class="m2"><p>می‌شوی در پیش همچون خود دوتو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بس گرفتی یار و همراهان زفت</p></div>
<div class="m2"><p>گر ترا پرسم که کو گویی که رفت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یار نیکت رفت بر چرخ برین</p></div>
<div class="m2"><p>یار فسقت رفت در قعر زمین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو بماندی در میانه آنچنان</p></div>
<div class="m2"><p>بی‌مدد چون آتشی از کاروان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دامن او گیر ای یار دلیر</p></div>
<div class="m2"><p>کو منزه باشد از بالا و زیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نه چو عیسی سوی گردون بر شود</p></div>
<div class="m2"><p>نه چو قارون در زمین اندر رود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>با تو باشد در مکان و بی‌مکان</p></div>
<div class="m2"><p>چون بمانی از سرا و از دکان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>او بر آرد از کدورتها صفا</p></div>
<div class="m2"><p>مر جفاهای ترا گیرد وفا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون جفا آری فرستد گوشمال</p></div>
<div class="m2"><p>تا ز نقصان وا روی سوی کمال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون تو وردی ترک کردی در روش</p></div>
<div class="m2"><p>بر تو قبضی آید از رنج و تبش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آن ادب کردن بود یعنی مکن</p></div>
<div class="m2"><p>هیچ تحویلی از آن عهد کهن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیش از آن کین قبض زنجیری شود</p></div>
<div class="m2"><p>این که دلگیریست پاگیری شود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رنج معقولت شود محسوس و فاش</p></div>
<div class="m2"><p>تا نگیری این اشارت را بلاش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در معاصی قبضها دلگیر شد</p></div>
<div class="m2"><p>قبضها بعد از اجل زنجیر شد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نعط من اعرض هنا عن ذکرنا</p></div>
<div class="m2"><p>عیشة ضنک و نجزی بالعمی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دزد چون مال کسان را می‌برد</p></div>
<div class="m2"><p>قبض و دلتنگی دلش را می‌خلد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>او همی‌گوید عجب این قبض چیست</p></div>
<div class="m2"><p>قبض آن مظلوم کز شرت گریست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون بدین قبض التفاتی کم کند</p></div>
<div class="m2"><p>باد اصرار آتشش را دم کند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>قبض دل قبض عوان شد لاجرم</p></div>
<div class="m2"><p>گشت محسوس آن معانی زد علم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>غصه‌ها زندان شدست و چارمیخ</p></div>
<div class="m2"><p>غصه بیخست و بروید شاخ بیخ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بیخ پنهان بود هم شد آشکار</p></div>
<div class="m2"><p>قبض و بسط اندرون بیخی شمار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چونک بیخ بد بود زودش بزن</p></div>
<div class="m2"><p>تا نروید زشت‌خاری در چمن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>قبض دیدی چارهٔ آن قبض کن</p></div>
<div class="m2"><p>زانک سرها جمله می‌روید ز بن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بسط دیدی بسط خود را آب ده</p></div>
<div class="m2"><p>چون بر آید میوه با اصحاب ده</p></div></div>