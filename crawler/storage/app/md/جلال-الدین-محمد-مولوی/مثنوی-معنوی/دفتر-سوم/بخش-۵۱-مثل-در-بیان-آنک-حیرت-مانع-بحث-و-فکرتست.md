---
title: >-
    بخش ۵۱ - مثل در بیان آنک حیرت مانع بحث و فکرتست
---
# بخش ۵۱ - مثل در بیان آنک حیرت مانع بحث و فکرتست

<div class="b" id="bn1"><div class="m1"><p>آن یکی مرد دومو آمد شتاب</p></div>
<div class="m2"><p>پیش یک آیینه دار مستطاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت از ریشم سپیدی کن جدا</p></div>
<div class="m2"><p>که عروس نو گزیدم ای فتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریش او ببرید و کل پیشش نهاد</p></div>
<div class="m2"><p>گفت تو بگزین مرا کاری فتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سؤال و آن جوابست آن گزین</p></div>
<div class="m2"><p>که سر اینها ندارد درد دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن یکی زد سیلیی مر زید را</p></div>
<div class="m2"><p>حمله کرد او هم برای کید را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت سیلی‌زن سؤالت می‌کنم</p></div>
<div class="m2"><p>پس جوابم گوی وانگه می‌زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر قفای تو زدم آمد طراق</p></div>
<div class="m2"><p>یک سؤالی دارم اینجا در وفاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این طراق از دست من بودست یا</p></div>
<div class="m2"><p>از قفاگاه تو ای فخر کیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت از درد این فراغت نیستم</p></div>
<div class="m2"><p>که درین فکر و تفکر بیستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو که بی‌دردی همی اندیش این</p></div>
<div class="m2"><p>نیست صاحب‌درد را این فکر هین</p></div></div>