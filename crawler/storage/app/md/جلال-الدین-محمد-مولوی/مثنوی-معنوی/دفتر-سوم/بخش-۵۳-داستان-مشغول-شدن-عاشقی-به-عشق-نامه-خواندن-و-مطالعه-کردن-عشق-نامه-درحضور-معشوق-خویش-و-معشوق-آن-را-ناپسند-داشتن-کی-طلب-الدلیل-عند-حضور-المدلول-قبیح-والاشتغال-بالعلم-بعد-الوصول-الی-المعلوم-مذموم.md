---
title: >-
    بخش ۵۳ - داستان مشغول شدن عاشقی به عشق‌نامه خواندن  و مطالعه کردن عشق‌نامه درحضور معشوق خویش و معشوق آن را ناپسند داشتن کی طلب الدلیل عند حضور المدلول  قبیح والاشتغال بالعلم بعد الوصول الی المعلوم مذموم
---
# بخش ۵۳ - داستان مشغول شدن عاشقی به عشق‌نامه خواندن  و مطالعه کردن عشق‌نامه درحضور معشوق خویش و معشوق آن را ناپسند داشتن کی طلب الدلیل عند حضور المدلول  قبیح والاشتغال بالعلم بعد الوصول الی المعلوم مذموم

<div class="b" id="bn1"><div class="m1"><p>آن یکی را یار پیش خود نشاند</p></div>
<div class="m2"><p>نامه بیرون کرد و پیش یار خواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیتها در نامه و مدح و ثنا</p></div>
<div class="m2"><p>زاری و مسکینی و بس لابه‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت معشوق این اگر بهر منست</p></div>
<div class="m2"><p>گاه وصل این عمر ضایع کردنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من به پیشت حاضر و تو نامه خوان</p></div>
<div class="m2"><p>نیست این باری نشان عاشقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت اینجا حاضری اما ولیک</p></div>
<div class="m2"><p>من نمی‌یایم نصیب خویش نیک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچ می‌دیدم ز تو پارینه سال</p></div>
<div class="m2"><p>نیست این دم گرچه می‌بینم وصال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ازین چشمه زلالی خورده‌ام</p></div>
<div class="m2"><p>دیده و دل ز آب تازه کرده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمه می‌بینم ولیکن آب نی</p></div>
<div class="m2"><p>راه آبم را مگر زد ره‌زنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت پس من نیستم معشوق تو</p></div>
<div class="m2"><p>من به بلغار و مرادت در قتو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقی تو بر من و بر حالتی</p></div>
<div class="m2"><p>حالت اندر دست نبود یا فتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس نیم کلی مطلوب تو من</p></div>
<div class="m2"><p>جزو مقصودم ترا اندرز من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خانهٔ معشوقه‌ام معشوق نی</p></div>
<div class="m2"><p>عشق بر نقدست بر صندوق نی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست معشوق آنک او یکتو بود</p></div>
<div class="m2"><p>مبتدا و منتهاات او بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بیابی‌اش نمانی منتظر</p></div>
<div class="m2"><p>هم هویدا او بود هم نیز سر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>میر احوالست نه موقوف حال</p></div>
<div class="m2"><p>بندهٔ آن ماه باشد ماه و سال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بگوید حال را فرمان کند</p></div>
<div class="m2"><p>چون بخواهد جسمها را جان کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منتها نبود که موقوفست او</p></div>
<div class="m2"><p>منتظر بنشسته باشد حال‌جو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کیمیای حال باشد دست او</p></div>
<div class="m2"><p>دست جنباند شود مس مست او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بخواهد مرگ هم شیرین شود</p></div>
<div class="m2"><p>خار و نشتر نرگس و نسرین شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنک او موقوف حالست آدمیست</p></div>
<div class="m2"><p>کو بحال افزون و گاهی در کمیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صوفی ابن الوقت باشد در منال</p></div>
<div class="m2"><p>لیک صافی فارغست از وقت و حال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حالها موقوف عزم و رای او</p></div>
<div class="m2"><p>زنده از نفخ مسیح‌آسای او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عاشق حالی نه عاشق بر منی</p></div>
<div class="m2"><p>بر امید حال بر من می‌تنی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنک یک دم کم دمی کامل بود</p></div>
<div class="m2"><p>نیست معبود خلیل آفل بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وانک آفل باشد و گه آن و این</p></div>
<div class="m2"><p>نیست دلبر لا احب الافلین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنک او گاهی خوش و گه ناخوشست</p></div>
<div class="m2"><p>یک زمانی آب و یک دم آتشست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برج مه باشد ولیکن ماه نه</p></div>
<div class="m2"><p>نقش بت باشد ولی آگاه نه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هست صوفی صفاجو ابن وقت</p></div>
<div class="m2"><p>وقت را همچون پدر بگرفته سخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هست صافی غرق عشق ذوالجلال</p></div>
<div class="m2"><p>ابن کس نه فارغ از اوقات و حال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غرقهٔ نوری که او لم یولدست</p></div>
<div class="m2"><p>لم یلد لم یولد آن ایزدست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رو چنین عشقی بجو گر زنده‌ای</p></div>
<div class="m2"><p>ورنه وقت مختلف را بنده‌ای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>منگر اندر نقش زشت و خوب خویش</p></div>
<div class="m2"><p>بنگر اندر عشق و در مطلوب خویش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>منگر آنک تو حقیری یا ضعیف</p></div>
<div class="m2"><p>بنگر اندر همت خود ای شریف</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو به هر حالی که باشی می‌طلب</p></div>
<div class="m2"><p>آب می‌جو دایما ای خشک‌لب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کان لب خشکت گواهی می‌دهد</p></div>
<div class="m2"><p>کو بآخر بر سر منبع رسد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خشکی لب هست پیغامی ز آب</p></div>
<div class="m2"><p>که بمات آرد یقین این اضطراب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کین طلب‌کاری مبارک جنبشیست</p></div>
<div class="m2"><p>این طلب در راه حق مانع کشیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این طلب مفتاح مطلوبات تست</p></div>
<div class="m2"><p>این سپاه و نصرت رایات تست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این طلب همچون خروسی در صیاح</p></div>
<div class="m2"><p>می‌زند نعره که می‌آید صباح</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرچه آلت نیستت تو می‌طلب</p></div>
<div class="m2"><p>نیست آلت حاجت اندر راه رب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که را بینی طلب‌کار ای پسر</p></div>
<div class="m2"><p>یار او شو پیش او انداز سر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کز جوار طالبان طالب شوی</p></div>
<div class="m2"><p>وز ظلال غالبان غالب شوی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر یکی موری سلیمانی بجست</p></div>
<div class="m2"><p>منگر اندر جستن او سست سست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هرچه داری تو ز مال و پیشه‌ای</p></div>
<div class="m2"><p>نه طلب بود اول و اندیشه‌ای</p></div></div>