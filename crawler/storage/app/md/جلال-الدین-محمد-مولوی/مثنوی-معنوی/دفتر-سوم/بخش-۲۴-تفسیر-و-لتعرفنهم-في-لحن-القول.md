---
title: >-
    بخش ۲۴ - تفسیر وَ لَتَعْرِفَنَّهُم في لَحْنِ القَوْلِ
---
# بخش ۲۴ - تفسیر وَ لَتَعْرِفَنَّهُم في لَحْنِ القَوْلِ

<div class="b" id="bn1"><div class="m1"><p>گفت یزدان مر نبی را در مساق</p></div>
<div class="m2"><p>یک نشانی سهل‌تر ز اهل نفاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر منافق زفت باشد نغز و هول</p></div>
<div class="m2"><p>وا شناسی مر ورا در لحن و قول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سفالین کوزه‌ها را می‌خری</p></div>
<div class="m2"><p>امتحانی می‌کنی ای مشتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌زنی دستی بر آن کوزه چرا</p></div>
<div class="m2"><p>تا شناسی از طنین اشکسته را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بانگ اشکسته دگرگون می‌بود</p></div>
<div class="m2"><p>بانگ چاووشست پیشش می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بانگ می‌آید که تعریفش کند</p></div>
<div class="m2"><p>همچو مصدر فعل تصریفش کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حدیث امتحان رویی نمود</p></div>
<div class="m2"><p>یادم آمد قصهٔ هاروت زود</p></div></div>