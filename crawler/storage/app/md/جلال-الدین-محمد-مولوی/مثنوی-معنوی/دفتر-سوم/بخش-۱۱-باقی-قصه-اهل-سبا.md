---
title: >-
    بخش ۱۱ - باقی قصهٔ اهل سبا
---
# بخش ۱۱ - باقی قصهٔ اهل سبا

<div class="b" id="bn1"><div class="m1"><p>آن سبا ز اهل صبا بودند و خام</p></div>
<div class="m2"><p>کارشان کفران نعمت با کرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد آن کفران نعمت در مثال</p></div>
<div class="m2"><p>که کنی با محسن خود تو جدال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که نمی‌باید مرا این نیکوی</p></div>
<div class="m2"><p>من برنجم زین چه رنجم می‌شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف کن این نیکوی را دور کن</p></div>
<div class="m2"><p>من نخواهم چشم زودم کور کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس سبا گفتند باعد بیننا</p></div>
<div class="m2"><p>شیننا خیر لنا خذ زیننا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما نمی‌خواهیم این ایوان و باغ</p></div>
<div class="m2"><p>نه زنان خوب و نه امن و فراغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهرها نزدیک همدیگر بدست</p></div>
<div class="m2"><p>آن بیابانست خوش کانجا ددست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یطلب الانسان فی الصیف الشتا</p></div>
<div class="m2"><p>فاذا جاء الشتا انکر ذا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فهو لا یرضی بحال ابدا</p></div>
<div class="m2"><p>لا بضیق لا بعیش رغدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قتل الانسان ما اکفره</p></div>
<div class="m2"><p>کلما نال هدی انکره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس زین سانست زان شد کشتنی</p></div>
<div class="m2"><p>اقتلوا انفسکم گفت آن سنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خار سه سویست هر چون کش نهی</p></div>
<div class="m2"><p>در خلد وز زخم او تو کی جهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتش ترک هوا در خار زن</p></div>
<div class="m2"><p>دست اندر یار نیکوکار زن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ز حد بردند اصحاب سبا</p></div>
<div class="m2"><p>که بپیش ما وبا به از صبا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناصحانشان در نصیحت آمدند</p></div>
<div class="m2"><p>از فسوق و کفر مانع می‌شدند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قصد خون ناصحان می‌داشتند</p></div>
<div class="m2"><p>تخم فسق و کافری می‌کاشتند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون قضا آید شود تنگ این جهان</p></div>
<div class="m2"><p>از قضا حلوا شود رنج دهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت اذا جاء القضا ضاق الفضا</p></div>
<div class="m2"><p>تحجب الابصار اذ جاء القضا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چشم بسته می‌شود وقت قضا</p></div>
<div class="m2"><p>تا نبیند چشم کحل چشم را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکر آن فارس چو انگیزید گرد</p></div>
<div class="m2"><p>آن غبارت ز استغاثت دور کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سوی فارس رو مرو سوی غبار</p></div>
<div class="m2"><p>ورنه بر تو کوبد آن مکر سوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت حق آن را که این گرگش بخورد</p></div>
<div class="m2"><p>دید گرد گرگ چون زاری نکرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او نمی‌دانست گرد گرگ را</p></div>
<div class="m2"><p>با چنین دانش چرا کرد او چرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گوسفندان بوی گرگ با گزند</p></div>
<div class="m2"><p>می‌بدانند و بهر سو می‌خزند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مغز حیوانات بوی شیر را</p></div>
<div class="m2"><p>می‌بداند ترک می‌گوید چرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوی شیر خشم دیدی باز گرد</p></div>
<div class="m2"><p>با مناجات و حذر انباز گرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وا نگشتند آن گروه از گرد گرگ</p></div>
<div class="m2"><p>گرگ محنت بعد گرد آمد سترگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر درید آن گوسفندان را بخشم</p></div>
<div class="m2"><p>که ز چوپان خرد بستند چشم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چند چوپانشان بخواند و نامدند</p></div>
<div class="m2"><p>خاک غم در چشم چوپان می‌زدند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که برو ما از تو خود چوپان‌تریم</p></div>
<div class="m2"><p>چون تبع گردیم هر یک سروریم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>طعمهٔ گرگیم و آن یار نه</p></div>
<div class="m2"><p>هیزم ناریم و آن عار نه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حمیتی بد جاهلیت در دماغ</p></div>
<div class="m2"><p>بانگ شومی بر دمنشان کرد زاغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بهر مظلومان همی‌کندند چاه</p></div>
<div class="m2"><p>در چه افتادند و می‌گفتند آه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پوستین یوسفان بشکافتند</p></div>
<div class="m2"><p>آنچ می‌کردند یک یک یافتند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کیست آن یوسف دل حق‌جوی تو</p></div>
<div class="m2"><p>چون اسیری بسته اندر کوی تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جبرئیلی را بر استن بسته‌ای</p></div>
<div class="m2"><p>پر و بالش را به صد جا خسته‌ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پیش او گوساله بریان آوری</p></div>
<div class="m2"><p>گه کشی او را به کهدان آوری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که بخور اینست ما را لوت و پوت</p></div>
<div class="m2"><p>نیست او را جز لقاء الله قوت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زین شکنجه و امتحان آن مبتلا</p></div>
<div class="m2"><p>می‌کند از تو شکایت با خدا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کای خدا افغان ازین گرگ کهن</p></div>
<div class="m2"><p>گویدش نک وقت آمد صبر کن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>داد تو وا خواهم از هر بی‌خبر</p></div>
<div class="m2"><p>داد کی دهد جز خدای دادگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>او همی‌گوید که صبرم شد فنا</p></div>
<div class="m2"><p>در فراق روی تو یا ربنا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>احمدم در مانده در دست یهود</p></div>
<div class="m2"><p>صالحم افتاده در حبس ثمود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای سعادت‌بخش جان انبیا</p></div>
<div class="m2"><p>یا بکش یا باز خوانم یا بیا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>با فراقت کافران را نیست تاب</p></div>
<div class="m2"><p>می‌گود یا لیتنی کنت تراب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حال او اینست کو خود زان سوست</p></div>
<div class="m2"><p>چون بود بی تو کسی کان توست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حق همی‌گوید که آری ای نزه</p></div>
<div class="m2"><p>لیک بشنو صبر آر و صبر به</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صبح نزدیکست خامش کم خروش</p></div>
<div class="m2"><p>من همی‌کوشم پی تو تو مکوش</p></div></div>