---
title: >-
    بخش ۱۵۶ - وحی آمدن از حق تعالی به موسی کی بیاموزش چیزی کی استدعا کند یا بعضی از آن
---
# بخش ۱۵۶ - وحی آمدن از حق تعالی به موسی کی بیاموزش چیزی کی استدعا کند یا بعضی از آن

<div class="b" id="bn1"><div class="m1"><p>گفت یزدان تو بده بایست او</p></div>
<div class="m2"><p>برگشا در اختیار آن دست او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختیار آمد عبادت را نمک</p></div>
<div class="m2"><p>ورنه می‌گردد بناخواه این فلک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردش او را نه اجر و نه عقاب</p></div>
<div class="m2"><p>که اختیار آمد هنر وقت حساب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله عالم خود مسبح آمدند</p></div>
<div class="m2"><p>نیست آن تسبیح جبری مزدمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ در دستش نه از عجزش بکن</p></div>
<div class="m2"><p>تا که غازی گردد او یا راه‌زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانک کرمنا شد آدم ز اختیار</p></div>
<div class="m2"><p>نیم زنبور عسل شد نیم مار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مومنان کان عسل زنبوروار</p></div>
<div class="m2"><p>کافران خود کان زهری همچو مار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زانک مؤمن خورد بگزیده نبات</p></div>
<div class="m2"><p>تا چو نحلی گشت ریق او حیات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز کافر خورد شربت از صدید</p></div>
<div class="m2"><p>هم ز قوتش زهر شد در وی پدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اهل الهام خدا عین الحیات</p></div>
<div class="m2"><p>اهل تسویل هوا سم الممات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در جهان این مدح و شاباش و زهی</p></div>
<div class="m2"><p>ز اختیارست و حفاظ آگهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله رندان چونک در زندان بوند</p></div>
<div class="m2"><p>متقی و زاهد و حق‌خوان شوند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چونک قدرت رفت کاسد شد عمل</p></div>
<div class="m2"><p>هین که تا سرمایه نستاند اجل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قدرتت سرمایهٔ سودست هین</p></div>
<div class="m2"><p>وقت قدرت را نگه دار و ببین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آدمی بر خنگ کرمنا سوار</p></div>
<div class="m2"><p>در کف درکش عنان اختیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باز موسی داد پند او را بمهر</p></div>
<div class="m2"><p>که مرادت زرد خواهد کرد چهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترک این سودا بگو وز حق بترس</p></div>
<div class="m2"><p>دیو دادستت برای مکر درس</p></div></div>