---
title: >-
    بخش ۴۲ - جواب فرعون موسی را و وحی  آمدن موسی را علیه‌السلام
---
# بخش ۴۲ - جواب فرعون موسی را و وحی  آمدن موسی را علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>گفت نه نه مهلتم باید نهاد</p></div>
<div class="m2"><p>عشوه‌ها کم ده تو کم پیمای باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق تعالی وحی کردش در زمان</p></div>
<div class="m2"><p>مهلتش ده متسع مهراس از آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چهل روزش بده مهلت بطوع</p></div>
<div class="m2"><p>تا سگالد مکرها او نوع نوع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بکوشد او که نی من خفته‌ام</p></div>
<div class="m2"><p>تیز رو گو پیش ره بگرفته‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیله‌هاشان را همه برهم زنم</p></div>
<div class="m2"><p>و آنچ افزایند من بر کم زنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب را آرند من آتش کنم</p></div>
<div class="m2"><p>نوش و خوش گیرند و من ناخوش کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر پیوندند و من ویران کنم</p></div>
<div class="m2"><p>آنک اندر وهم نارند آن کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو مترس و مهلتش ده دم‌دراز</p></div>
<div class="m2"><p>گو سپه گرد آر و صد حیلت بساز</p></div></div>