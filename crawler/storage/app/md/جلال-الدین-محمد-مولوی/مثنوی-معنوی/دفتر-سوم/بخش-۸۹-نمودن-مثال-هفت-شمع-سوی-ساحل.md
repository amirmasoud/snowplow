---
title: >-
    بخش ۸۹ - نمودن مثال هفت شمع سوی ساحل
---
# بخش ۸۹ - نمودن مثال هفت شمع سوی ساحل

<div class="b" id="bn1"><div class="m1"><p>هفت شمع از دور دیدم ناگهان</p></div>
<div class="m2"><p>اندر آن ساحل شتابیدم بدان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور شعلهٔ هر یکی شمعی از آن</p></div>
<div class="m2"><p>بر شده خوش تا عنان آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیره گشتم خیرگی هم خیره گشت</p></div>
<div class="m2"><p>موج حیرت عقل را از سر گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چگونه شمعها افروختست</p></div>
<div class="m2"><p>کین دو دیدهٔ خلق ازینها دوختست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق جویان چراغی گشته بود</p></div>
<div class="m2"><p>پیش آن شمعی که بر مه می‌فزود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم‌بندی بد عجب بر دیده‌ها</p></div>
<div class="m2"><p>بندشان می‌کرد یهدی من یشا</p></div></div>