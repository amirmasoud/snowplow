---
title: >-
    بخش ۸۳ - صفت بعضی اولیا کی راضی‌اند باحکام و لابه نکنند کی این حکم را بگردان
---
# بخش ۸۳ - صفت بعضی اولیا کی راضی‌اند باحکام و لابه نکنند کی این حکم را بگردان

<div class="b" id="bn1"><div class="m1"><p>بشنو اکنون قصهٔ آن ره‌روان</p></div>
<div class="m2"><p>که ندارند اعتراضی در جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اولیا اهل دعا خود دیگرند</p></div>
<div class="m2"><p>که همی‌دوزند و گاهی می‌درند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قوم دیگر می‌شناسم ز اولیا</p></div>
<div class="m2"><p>که دهانشان بسته باشد از دعا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رضا که هست رام آن کرام</p></div>
<div class="m2"><p>جستن دفع قضاشان شد حرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در قضا ذوقی همی‌بینند خاص</p></div>
<div class="m2"><p>کفرشان آید طلب کردن خلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن ظنی بر دل ایشان گشود</p></div>
<div class="m2"><p>که نپوشند از عمی جامهٔ کبود</p></div></div>