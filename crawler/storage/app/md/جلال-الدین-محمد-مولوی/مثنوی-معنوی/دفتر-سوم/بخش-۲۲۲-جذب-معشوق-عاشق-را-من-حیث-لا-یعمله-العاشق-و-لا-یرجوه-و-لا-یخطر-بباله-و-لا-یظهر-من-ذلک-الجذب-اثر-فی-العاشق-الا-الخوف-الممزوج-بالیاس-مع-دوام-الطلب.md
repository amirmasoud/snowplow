---
title: >-
    بخش ۲۲۲ - جذب معشوق عاشق را من حیث لا یعمله العاشق و لا یرجوه و لا یخطر بباله و لا یظهر من ذلک الجذب اثر فی العاشق الا الخوف الممزوج بالیاس مع دوام الطلب
---
# بخش ۲۲۲ - جذب معشوق عاشق را من حیث لا یعمله العاشق و لا یرجوه و لا یخطر بباله و لا یظهر من ذلک الجذب اثر فی العاشق الا الخوف الممزوج بالیاس مع دوام الطلب

<div class="b" id="bn1"><div class="m1"><p>آمدیم اینجا که در صدر جهان</p></div>
<div class="m2"><p>گر نبودی جذب آن عاشق نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناشکیبا کی بدی او از فراق</p></div>
<div class="m2"><p>کی دوان باز آمدی سوی وثاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میل معشوقان نهانست و ستیر</p></div>
<div class="m2"><p>میل عاشق با دو صد طبل و نفیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک حکایت هست اینجا ز اعتبار</p></div>
<div class="m2"><p>لیک عاجز شد بخاری ز انتظار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک آن کردیم کو در جست و جوست</p></div>
<div class="m2"><p>تاکه پیش از مرگ بیند روی دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا رهد از مرگ تا یابد نجات</p></div>
<div class="m2"><p>زانک دید دوستست آب حیات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که دید او نباشد دفع مرگ</p></div>
<div class="m2"><p>دوست نبود که نه میوه‌ستش نه برگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار آن کارست ای مشتاق مست</p></div>
<div class="m2"><p>کاندر آن کار ار رسد مرگت خوشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد نشان صدق ایمان ای جوان</p></div>
<div class="m2"><p>آنک آید خوش ترا مرگ اندر آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نشد ایمان تو ای جان چنین</p></div>
<div class="m2"><p>نیست کامل رو بجو اکمال دین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که اندر کار تو شد مرگ‌دوست</p></div>
<div class="m2"><p>بر دل تو بی کراهت دوست اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون کراهت رفت آن خود مرگ نیست</p></div>
<div class="m2"><p>صورت مرگست و نقلان کردنیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون کراهت رفت مردن نفع شد</p></div>
<div class="m2"><p>پس درست آید که مردن دفع شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دوست حقست و کسی کش گفت او</p></div>
<div class="m2"><p>که توی آن من و من آن تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گوش دار اکنون که عاشق می‌رسد</p></div>
<div class="m2"><p>بسته عشق او را به حبل من مسد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بدید او چهرهٔ صدر جهان</p></div>
<div class="m2"><p>گوییا پریدش از تن مرغ جان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همچو چوب خشک افتاد آن تنش</p></div>
<div class="m2"><p>سرد شد از فرق جان تا ناخنش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرچه کردند از بخور و از گلاب</p></div>
<div class="m2"><p>نه بجنبید و نه آمد در خطاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه چون دید آن مزعفر روی او</p></div>
<div class="m2"><p>پس فرود آمد ز مرکب سوی او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت عاشق دوست می‌جوید بتفت</p></div>
<div class="m2"><p>چونک معشوق آمد آن عاشق برفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عاشق حقی و حق آنست کو</p></div>
<div class="m2"><p>چون بیاید نبود از تو تای مو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صد چو تو فانیست پیش آن نظر</p></div>
<div class="m2"><p>عاشقی بر نفی خود خواجه مگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سایه‌ای و عاشقی بر آفتاب</p></div>
<div class="m2"><p>شمس آید سایه لا گردد شتاب</p></div></div>