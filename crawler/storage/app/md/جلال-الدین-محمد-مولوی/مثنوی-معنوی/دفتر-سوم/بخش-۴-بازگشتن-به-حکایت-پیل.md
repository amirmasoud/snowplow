---
title: >-
    بخش ۴ - بازگشتن به حکایت پیل
---
# بخش ۴ - بازگشتن به حکایت پیل

<div class="b" id="bn1"><div class="m1"><p>گفت ناصح بشنوید این پند من</p></div>
<div class="m2"><p>تا دل و جانتان نگردد ممتحن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با گیاه و برگها قانع شوید</p></div>
<div class="m2"><p>در شکار پیل‌بچگان کم روید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من برون کردم ز گردن وام نصح</p></div>
<div class="m2"><p>جز سعادت کی بود انجام نصح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من به تبلیغ رسالت آمدم</p></div>
<div class="m2"><p>تا رهانم مر شما را از ندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هین مبادا که طمع رهتان زند</p></div>
<div class="m2"><p>طمع برگ از بیخهاتان بر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این بگفت و خیربادی کرد و رفت</p></div>
<div class="m2"><p>گشت قحط و جوعشان در راه زفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناگهان دیدند سوی جاده‌ای</p></div>
<div class="m2"><p>پور پیلی فربهی نو زاده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر افتادند چون گرگان مست</p></div>
<div class="m2"><p>پاک خوردندش فرو شستند دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن یکی همره نخورد و پند داد</p></div>
<div class="m2"><p>که حدیث آن فقیرش بود یاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کبابش مانع آمد آن سخن</p></div>
<div class="m2"><p>بخت نو بخشد ترا عقل کهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس بیفتادند و خفتند آن همه</p></div>
<div class="m2"><p>وان گرسنه چون شبان اندر رمه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دید پیلی سهمناکی می‌رسید</p></div>
<div class="m2"><p>اولا آمد سوی حارس دوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوی می‌کرد آن دهانش را سه بار</p></div>
<div class="m2"><p>هیچ بویی زو نیامد ناگوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند باری گرد او گشت و برفت</p></div>
<div class="m2"><p>مر ورا نازرد آن شه‌پیل زفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مر لب هر خفته‌ای را بوی کرد</p></div>
<div class="m2"><p>بوی می‌آمد ورا زان خفته مرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از کباب پیل‌زاده خورده بود</p></div>
<div class="m2"><p>بر درانید و بکشتش پیل زود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در زمان او یک بیک را زان گروه</p></div>
<div class="m2"><p>می‌درانید و نبودش زان شکوه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر هوا انداخت هر یک را گزاف</p></div>
<div class="m2"><p>تا همی‌زد بر زمین می‌شد شکاف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای خورندهٔ خون خلق از راه برد</p></div>
<div class="m2"><p>تا نه آرد خون ایشانت نبرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مال ایشان خون ایشان دان یقین</p></div>
<div class="m2"><p>زانک مال از زور آید در یمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مادر آن پیل‌بچگان کین کشد</p></div>
<div class="m2"><p>پیل بچه‌خواره را کیفر کشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیل‌بچه می‌خوری ای پاره‌خوار</p></div>
<div class="m2"><p>هم بر آرد خصم پیل از تو دمار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بوی رسوا کرد مکر اندیش را</p></div>
<div class="m2"><p>پیل داند بوی طفل خویش را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنک یابد بوی حق را از یمن</p></div>
<div class="m2"><p>چون نیابد بوی باطل را ز من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مصطفی چون برد بوی از راه دور</p></div>
<div class="m2"><p>چون نیابد از دهان ما بخور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم بیابد لیک پوشاند ز ما</p></div>
<div class="m2"><p>بوی نیک و بد بر آید بر سما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو همی‌خسپی و بوی آن حرام</p></div>
<div class="m2"><p>می‌زند بر آسمان سبزفام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همره انفاس زشتت می‌شود</p></div>
<div class="m2"><p>تا به بوگیران گردون می‌رود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بوی کبر و بوی حرص و بوی آز</p></div>
<div class="m2"><p>در سخن گفتن بیاید چون پیاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر خوری سوگند من کی خورده‌ام</p></div>
<div class="m2"><p>از پیاز و سیر تقوی کرده‌ام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن دم سوگند غمازی کند</p></div>
<div class="m2"><p>بر دماغ همنشینان بر زند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس دعاها رد شود از بوی آن</p></div>
<div class="m2"><p>آن دل کژ می‌نماید در زبان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اخسؤا آید جواب آن دعا</p></div>
<div class="m2"><p>چوب رد باشد جزای هر دغا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر حدیثت کژ بود معنیت راست</p></div>
<div class="m2"><p>آن کژی لفظ مقبول خداست</p></div></div>