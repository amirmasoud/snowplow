---
title: >-
    بخش ۹۸ - اقتدا کردن قوم از پس دقوقی
---
# بخش ۹۸ - اقتدا کردن قوم از پس دقوقی

<div class="b" id="bn1"><div class="m1"><p>پیش در شد آن دقوقی در نماز</p></div>
<div class="m2"><p>قوم همچون اطلس آمد او طراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اقتدا کردند آن شاهان قطار</p></div>
<div class="m2"><p>در پی آن مقتدای نامدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونک با تکبیرها مقرون شدند</p></div>
<div class="m2"><p>همچو قربان از جهان بیرون شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی تکبیر اینست ای امام</p></div>
<div class="m2"><p>کای خدا پیش تو ما قربان شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت ذبح الله اکبر می‌کنی</p></div>
<div class="m2"><p>همچنین در ذبح نفس کشتنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن چو اسمعیل و جان همچون خلیل</p></div>
<div class="m2"><p>کرد جان تکبیر بر جسم نبیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت کشته تن ز شهوتها و آز</p></div>
<div class="m2"><p>شد به بسم الله بسمل در نماز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون قیامت پیش حق صفها زده</p></div>
<div class="m2"><p>در حساب و در مناجات آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایستاده پیش یزدان اشک‌ریز</p></div>
<div class="m2"><p>بر مثال راست‌خیز رستخیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حق همی‌گوید چه آوردی مرا</p></div>
<div class="m2"><p>اندرین مهلت که دادم من ترا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمر خود را در چه پایان برده‌ای</p></div>
<div class="m2"><p>قوت و قوت در چه فانی کرده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوهر دیده کجا فرسوده‌ای</p></div>
<div class="m2"><p>پنج حس را در کجا پالوده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم و هوش و گوش و گوهرهای عرش</p></div>
<div class="m2"><p>خرج کردی چه خریدی تو ز فرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست و پا دادمت چون بیل و کلند</p></div>
<div class="m2"><p>من ببخشیدم ز خود آن کی شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنین پیغامهای دردگین</p></div>
<div class="m2"><p>صد هزاران آید از حضرت چنین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در قیام این کفتها دارد رجوع</p></div>
<div class="m2"><p>وز خجالت شد دوتا او در رکوع</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قوت استادن از خجلت نماند</p></div>
<div class="m2"><p>در رکوع از شرم تسبیحی بخواند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز فرمان می‌رسد بردار سر</p></div>
<div class="m2"><p>از رکوع و پاسخ حق بر شمر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر بر آرد از رکوع آن شرمسار</p></div>
<div class="m2"><p>باز اندر رو فتد آن خام‌کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باز فرمان آیدش بردار سر</p></div>
<div class="m2"><p>از سجود و وا ده از کرده خبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر بر آرد او دگر ره شرمسار</p></div>
<div class="m2"><p>اندر افتد باز در رو همچو مار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باز گوید سر بر آر و باز گو</p></div>
<div class="m2"><p>که بخواهم جست از تو مو بمو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قوت پا ایستادن نبودش</p></div>
<div class="m2"><p>که خطاب هیبتی بر جان زدش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس نشیند قعده زان بار گران</p></div>
<div class="m2"><p>حضرتش گوید سخن گو با بیان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نعمتت دادم بگو شکرت چه بود</p></div>
<div class="m2"><p>دادمت سرمایه هین بنمای سود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رو بدست راست آرد در سلام</p></div>
<div class="m2"><p>سوی جان انبیا و آن کرام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یعنی ای شاهان شفاعت کین لئیم</p></div>
<div class="m2"><p>سخت در گل ماندش پای و گلیم</p></div></div>