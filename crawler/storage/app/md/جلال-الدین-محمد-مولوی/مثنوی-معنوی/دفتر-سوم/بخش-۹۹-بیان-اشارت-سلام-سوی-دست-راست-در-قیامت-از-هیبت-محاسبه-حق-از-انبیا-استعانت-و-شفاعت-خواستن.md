---
title: >-
    بخش ۹۹ - بیان اشارت سلام سوی دست راست در قیامت از هیبت محاسبه حق از انبیا استعانت  و شفاعت خواستن
---
# بخش ۹۹ - بیان اشارت سلام سوی دست راست در قیامت از هیبت محاسبه حق از انبیا استعانت  و شفاعت خواستن

<div class="b" id="bn1"><div class="m1"><p>انبیا گویند روز چاره رفت</p></div>
<div class="m2"><p>چاره آنجا بود و دست‌افزار زفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ بی‌هنگامی ای بدبخت رو</p></div>
<div class="m2"><p>ترک ما گو خون ما اندر مشو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو بگرداند به سوی دست چپ</p></div>
<div class="m2"><p>در تبار و خویش گویندش که خپ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هین جواب خویش گو با کردگار</p></div>
<div class="m2"><p>ما کییم ای خواجه دست از ما بدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ازین سو نه از آن سو چاره شد</p></div>
<div class="m2"><p>جان آن بیچاره‌دل صد پاره شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از همه نومید شد مسکین کیا</p></div>
<div class="m2"><p>پس برآرد هر دو دست اندر دعا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کز همه نومید گشتم ای خدا</p></div>
<div class="m2"><p>اول و آخر توی و منتها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نماز این خوش اشارتها ببین</p></div>
<div class="m2"><p>تا بدانی کین بخواهد شد یقین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بچه بیرون آر از بیضه نماز</p></div>
<div class="m2"><p>سر مزن چون مرغ بی تعظیم و ساز</p></div></div>