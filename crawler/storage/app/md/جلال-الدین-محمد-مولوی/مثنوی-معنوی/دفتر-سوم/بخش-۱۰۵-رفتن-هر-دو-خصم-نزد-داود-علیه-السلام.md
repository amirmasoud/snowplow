---
title: >-
    بخش ۱۰۵ - رفتن هر دو خصم نزد داود علیه السلام
---
# بخش ۱۰۵ - رفتن هر دو خصم نزد داود علیه السلام

<div class="b" id="bn1"><div class="m1"><p>می‌کشیدش تا به داود نبی</p></div>
<div class="m2"><p>که بیا ای ظالم گیج غبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حجت بارد رها کن ای دغا</p></div>
<div class="m2"><p>عقل در تن آور و با خویش آ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چه می‌گویی دعا چه بود مخند</p></div>
<div class="m2"><p>بر سر و و ریش من و خویش ای لوند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت من با حق دعاها کرده‌ام</p></div>
<div class="m2"><p>اندرین لابه بسی خون خورده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من یقین دارم دعا شد مستجاب</p></div>
<div class="m2"><p>سر بزن بر سنگ ای منکرخطاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت گرد آیید هین یا مسلمین</p></div>
<div class="m2"><p>ژاژ بینید و فشار این مهین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای مسلمانان دعا مال مرا</p></div>
<div class="m2"><p>چون از آن او کند بهر خدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چنین بودی همه عالم بدین</p></div>
<div class="m2"><p>یک دعا املاک بردندی بکین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چنین بودی گدایان ضریر</p></div>
<div class="m2"><p>محتشم گشته بدندی و امیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز و شب اندر دعااند و ثنا</p></div>
<div class="m2"><p>لابه‌گویان که تو ده‌مان ای خدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا تو ندهی هیچ کس ندهد یقین</p></div>
<div class="m2"><p>ای گشاینده تو بگشا بند این</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکسب کوران بود لابه و دعا</p></div>
<div class="m2"><p>جز لب نانی نیابند از عطا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خلق گفتند این مسلمان راست‌گوست</p></div>
<div class="m2"><p>وین فروشندهٔ دعاها ظلم‌جوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این دعا کی باشد از اسباب ملک</p></div>
<div class="m2"><p>کی کشید این را شریعت خود بسلک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیع و بخشش یا وصیت یا عطا</p></div>
<div class="m2"><p>یا ز جنس این شود ملکی ترا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در کدامین دفترست این شرع نو</p></div>
<div class="m2"><p>گاو را تو باز ده یا حبس رو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او به سوی آسمان می‌کرد رو</p></div>
<div class="m2"><p>واقعهٔ ما را نداند غیر تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در دل من آن دعا انداختی</p></div>
<div class="m2"><p>صد امید اندر دلم افراختی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من نمی‌کردم گزافه آن دعا</p></div>
<div class="m2"><p>همچو یوسف دیده بودم خوابها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دید یوسف آفتاب و اختران</p></div>
<div class="m2"><p>پیش او سجده‌کنان چون چاکران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اعتمادش بود بر خواب درست</p></div>
<div class="m2"><p>در چه و زندان جز آن را می‌نجست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز اعتماد او نبودش هیچ غم</p></div>
<div class="m2"><p>از غلامی وز ملام و بیش و کم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اعتمادی داشت او بر خواب خویش</p></div>
<div class="m2"><p>که چو شمعی می‌فروزیدش ز پیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون در افکندند یوسف را به چاه</p></div>
<div class="m2"><p>بانگ آمد سمع او را از اله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که تو روزی شه شوی ای پهلوان</p></div>
<div class="m2"><p>تا بمالی این جفا در رویشان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قایل این بانگ ناید در نظر</p></div>
<div class="m2"><p>لیک دل بشناخت قایل را ز اثر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قوتی و راحتی و مسندی</p></div>
<div class="m2"><p>در میان جان فتادش زان ندا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چاه شد بر وی بدان بانگ جلیل</p></div>
<div class="m2"><p>گلشن و بزمی چو آتش بر خلیل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر جفا که بعد از آنش می‌رسید</p></div>
<div class="m2"><p>او بدان قوت بشادی می‌کشید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همچنانک ذوق آن بانگ الست</p></div>
<div class="m2"><p>در دل هر مؤمنی تا حشر هست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا نباشد در بلاشان اعتراض</p></div>
<div class="m2"><p>نه ز امر و نهی حقشان انقباض</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لقمهٔ حکمی که تلخی می‌نهد</p></div>
<div class="m2"><p>گلشکر آن را گوارش می‌دهد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گلشکر آن را که نبود مستند</p></div>
<div class="m2"><p>لقمه را ز انکار او قی می‌کند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر که خوابی دید از روز الست</p></div>
<div class="m2"><p>مست باشد در ره طاعات مست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>می‌کشد چون اشتر مست این جوال</p></div>
<div class="m2"><p>بی فتور و بی گمان و بی ملال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کفک تصدیقش بگرد پوز او</p></div>
<div class="m2"><p>شد گواه مستی و دلسوز او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اشتر از قوت چو شیر نر شده</p></div>
<div class="m2"><p>زیر ثقل بار اندک‌خور شده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز آرزوی ناقه صد فاقه برو</p></div>
<div class="m2"><p>می‌نماید کوه پیشش تار مو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در الست آنکو چنین خوابی ندید</p></div>
<div class="m2"><p>اندرین دنیا نشد بنده و مرید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ور بشد اندر تردد صد دله</p></div>
<div class="m2"><p>یک زمان شکرستش و سالی گله</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پای پیش و پای پس در راه دین</p></div>
<div class="m2"><p>می‌نهد با صد تردد بی یقین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وام‌دار شرح اینم نک گرو</p></div>
<div class="m2"><p>ور شتابستت ز الم نشرح شنو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون ندارد شرح این معنی کران</p></div>
<div class="m2"><p>خر به سوی مدعی گاو ران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفت کورم خواند زین جرم آن دغا</p></div>
<div class="m2"><p>بس بلیسانه قیاسست ای خدا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من دعا کورانه کی می‌کرده‌ام</p></div>
<div class="m2"><p>جز به خالق کدیه کی آورده‌ام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کور از خلقان طمع دارد ز جهل</p></div>
<div class="m2"><p>من ز تو کز تست هر دشوار سهل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آن یکی کورم ز کوران بشمرید</p></div>
<div class="m2"><p>او نیاز جان و اخلاصم ندید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کوری عشقست این کوری من</p></div>
<div class="m2"><p>حب یعمی و یصمست ای حسن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کورم از غیر خدا بینا بدو</p></div>
<div class="m2"><p>مقتضای عشق این باشد نکو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو که بینایی ز کورانم مدار</p></div>
<div class="m2"><p>دایرم برگرد لطفت ای مدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آنچنانک یوسف صدیق را</p></div>
<div class="m2"><p>خواب بنمودی و گشتش متکا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مر مرا لطف تو هم خوابی نمود</p></div>
<div class="m2"><p>آن دعای بی‌حدم بازی نبود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>می‌نداند خلق اسرار مرا</p></div>
<div class="m2"><p>ژاژ می‌دانند گفتار مرا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حقشان است و کی داند راز غیب</p></div>
<div class="m2"><p>غیر علام سر و ستار عیب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خصم گفتش رو به من کن حق بگو</p></div>
<div class="m2"><p>رو چه سوی آسمان کردی عمو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شید می‌آری غلط می‌افکنی</p></div>
<div class="m2"><p>لاف عشق و لاف قربت می‌زنی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>با کدامین روی چون دل‌مرده‌ای</p></div>
<div class="m2"><p>روی سوی آسمانها کرده‌ای</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>غلغلی در شهر افتاده ازین</p></div>
<div class="m2"><p>آن مسلمان می‌نهد رو بر زمین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کای خدا این بنده را رسوا مکن</p></div>
<div class="m2"><p>گر بدم هم سر من پیدا مکن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تو همی‌دانی و شبهای دراز</p></div>
<div class="m2"><p>که همی‌خواندم ترا با صد نیاز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پیش خلق این را اگر خود قدر نیست</p></div>
<div class="m2"><p>پیش تو همچون چراغ روشنیست</p></div></div>