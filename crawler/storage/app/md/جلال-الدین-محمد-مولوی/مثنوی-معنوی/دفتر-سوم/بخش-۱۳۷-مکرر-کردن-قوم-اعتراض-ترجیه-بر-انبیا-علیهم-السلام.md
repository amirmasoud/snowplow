---
title: >-
    بخش ۱۳۷ - مکرر کردن قوم اعتراض ترجیه بر انبیا علیهم‌السلام
---
# بخش ۱۳۷ - مکرر کردن قوم اعتراض ترجیه بر انبیا علیهم‌السلام

<div class="b" id="bn1"><div class="m1"><p>قوم گفتند از شما سعد خودیت</p></div>
<div class="m2"><p>نحس مایید و ضدیت و مرتدیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ما فارغ بد از اندیشه‌ها</p></div>
<div class="m2"><p>در غم افکندید ما را و عنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق جمعیت که بود و اتفاق</p></div>
<div class="m2"><p>شد ز فال زشتتان صد افتراق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوطی نقل شکر بودیم ما</p></div>
<div class="m2"><p>مرغ مرگ‌اندیش گشتیم از شما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا افسانهٔ غم‌گستریست</p></div>
<div class="m2"><p>هر کجا آوازهٔ مستنکریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا اندر جهان فال بذست</p></div>
<div class="m2"><p>هر کجا مسخی نکالی ماخذست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مثال قصه و فال شماست</p></div>
<div class="m2"><p>در غم‌انگیزی شما را مشتهاست</p></div></div>