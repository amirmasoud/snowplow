---
title: >-
    بخش ۱۱۹ - شرح آن کور دوربین و آن کر تیزشنو و  آن برهنه دراز دامن
---
# بخش ۱۱۹ - شرح آن کور دوربین و آن کر تیزشنو و  آن برهنه دراز دامن

<div class="b" id="bn1"><div class="m1"><p>کر امل را دان که مرگ ما شنید</p></div>
<div class="m2"><p>مرگ خود نشنید و نقل خود ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرص نابیناست بیند مو بمو</p></div>
<div class="m2"><p>عیب خلقان و بگوید کو بکو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیب خود یک ذره چشم کور او</p></div>
<div class="m2"><p>می‌نبیند گرچه هست او عیب‌جو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عور می‌ترسد که دامانش برند</p></div>
<div class="m2"><p>دامن مرد برهنه چون درند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد دنیا مفلس است و ترسناک</p></div>
<div class="m2"><p>هیچ او را نیست از دزدانش باک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او برهنه آمد و عریان رود</p></div>
<div class="m2"><p>وز غم دزدش جگر خون می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت مرگش که بود صد نوحه بیش</p></div>
<div class="m2"><p>خنده آید جانش را زین ترس خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن زمان داند غنی کش نیست زر</p></div>
<div class="m2"><p>هم ذکی داند که او بد بی‌هنر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون کنار کودکی پر از سفال</p></div>
<div class="m2"><p>کو بر آن لرزان بود چون رب مال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ستانی پاره‌ای گریان شود</p></div>
<div class="m2"><p>پاره گر بازش دهی خندان شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نباشد طفل را دانش دثار</p></div>
<div class="m2"><p>گریه و خنده‌ش ندارد اعتبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محتشم چون عاریت را ملک دید</p></div>
<div class="m2"><p>پس بر آن مال دروغین می‌طپید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواب می‌بیند که او را هست مال</p></div>
<div class="m2"><p>ترسد از دزدی که برباید جوال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ز خوابش بر جهاند گوش‌کش</p></div>
<div class="m2"><p>پس ز ترس خویش تسخر آیدش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنان لرزانی این عالمان</p></div>
<div class="m2"><p>که بودشان عقل و علم این جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از پی این عاقلان ذو فنون</p></div>
<div class="m2"><p>گفت ایزد در نبی لا یعلمون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر یکی ترسان ز دزدی کسی</p></div>
<div class="m2"><p>خویشتن را علم پندارد بسی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوید او که روزگارم می‌برند</p></div>
<div class="m2"><p>خود ندارد روزگار سودمند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گوید از کارم بر آوردند خلق</p></div>
<div class="m2"><p>غرق بی‌کاریست جانش تابه حلق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عور ترسان که منم دامن کشان</p></div>
<div class="m2"><p>چون رهانم دامن از چنگالشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صد هزاران فضل داند از علوم</p></div>
<div class="m2"><p>جان خود را می‌نداند آن ظلوم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>داند او خاصیت هر جوهری</p></div>
<div class="m2"><p>در بیان جوهر خود چون خری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که همی‌دانم یجوز و لایجوز</p></div>
<div class="m2"><p>خود ندانی تو یجوزی یا عجوز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این روا و آن ناروا دانی ولیک</p></div>
<div class="m2"><p>تو روا یا ناروایی بین تو نیک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قیمت هر کاله می‌دانی که چیست</p></div>
<div class="m2"><p>قیمت خود را ندانی احمقیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سعدها و نحسها دانسته‌ای</p></div>
<div class="m2"><p>ننگری سعدی تو یا ناشسته‌ای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جان جمله علمها اینست این</p></div>
<div class="m2"><p>که بدانی من کیم در یوم دین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن اصول دین بدانستی ولیک</p></div>
<div class="m2"><p>بنگر اندر اصل خود گر هست نیک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از اصولینت اصول خویش به</p></div>
<div class="m2"><p>که بدانی اصل خود ای مرد مه</p></div></div>