---
title: >-
    بخش ۶۳ - در جامهٔ خواب افتادن استاد و نالیدن او  از وهم رنجوری
---
# بخش ۶۳ - در جامهٔ خواب افتادن استاد و نالیدن او  از وهم رنجوری

<div class="b" id="bn1"><div class="m1"><p>جامه خواب آورد و گسترد آن عجوز</p></div>
<div class="m2"><p>گفت امکان نه و باطن پر ز سوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بگویم متهم دارد مرا</p></div>
<div class="m2"><p>ور نگویم جد شود این ماجرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فال بد رنجور گرداند همی</p></div>
<div class="m2"><p>آدمی را که نبودستش غمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قول پیغامبر قبوله یفرض</p></div>
<div class="m2"><p>ان تمارضتم لدینا تمرضوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بگویم او خیالی بر زند</p></div>
<div class="m2"><p>فعل دارد زن که خلوت می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مر مرا از خانه بیرون می‌کند</p></div>
<div class="m2"><p>بهر فسقی فعل و افسون می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامه خوابش کرد و استاد اوفتاد</p></div>
<div class="m2"><p>آه آه و ناله از وی می‌بزاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کودکان آنجا نشستند و نهان</p></div>
<div class="m2"><p>درس می‌خواندند با صد اندهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کین همه کردیم و ما زندانییم</p></div>
<div class="m2"><p>بد بنایی بود ما بد بانییم</p></div></div>