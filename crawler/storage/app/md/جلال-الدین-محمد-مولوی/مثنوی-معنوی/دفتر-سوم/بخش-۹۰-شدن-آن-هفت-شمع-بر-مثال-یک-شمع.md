---
title: >-
    بخش ۹۰ - شدن آن هفت شمع بر مثال یک شمع
---
# بخش ۹۰ - شدن آن هفت شمع بر مثال یک شمع

<div class="b" id="bn1"><div class="m1"><p>باز می‌دیدم که می‌شد هفت یک</p></div>
<div class="m2"><p>می‌شکافد نور او جیب فلک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز آن یک بار دیگر هفت شد</p></div>
<div class="m2"><p>مستی و حیرانی من زفت شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اتصالاتی میان شمعها</p></div>
<div class="m2"><p>که نیاید بر زبان و گفت ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنک یک دیدن کند ادارک آن</p></div>
<div class="m2"><p>سالها نتوان نمودن از زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنک یک دم بیندش ادراک هوش</p></div>
<div class="m2"><p>سالها نتوان شنودن آن بگوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونک پایانی ندارد رو الیک</p></div>
<div class="m2"><p>زانک لا احصی ثناء ما علیک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیشتر رفتم دوان کان شمعها</p></div>
<div class="m2"><p>تا چه چیزست از نشان کبریا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌شدم بی خویش و مدهوش و خراب</p></div>
<div class="m2"><p>تا بیفتادم ز تعجیل و شتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساعتی بی‌هوش و بی‌عقل اندرین</p></div>
<div class="m2"><p>اوفتادم بر سر خاک زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز با هوش آمدم برخاستم</p></div>
<div class="m2"><p>در روش گویی نه سر نه پاستم</p></div></div>