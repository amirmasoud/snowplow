---
title: >-
    بخش ۱۱۸ - قصهٔ اهل سبا و حماقت ایشان و اثر ناکردن  نصیحت انبیا در احمقان
---
# بخش ۱۱۸ - قصهٔ اهل سبا و حماقت ایشان و اثر ناکردن  نصیحت انبیا در احمقان

<div class="b" id="bn1"><div class="m1"><p>یادم آمد قصهٔ اهل سبا</p></div>
<div class="m2"><p>کز دم احمق صباشان شد وبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سبا ماند به شهر بس کلان</p></div>
<div class="m2"><p>در فسانه بشنوی از کودکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کودکان افسانه‌ها می‌آورند</p></div>
<div class="m2"><p>درج در افسانه‌شان بس سر و پند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزلها گویند در افسانه‌ها</p></div>
<div class="m2"><p>گنج می‌جو در همه ویرانه‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود شهری بس عظیم و مه ولی</p></div>
<div class="m2"><p>قدر او قدر سکره بیش نی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس عظیم و بس فراخ و بس دراز</p></div>
<div class="m2"><p>سخت زفت زفت اندازهٔ پیاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم ده شهر مجموع اندرو</p></div>
<div class="m2"><p>لیک جمله سه تن ناشسته‌رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندرو خلق و خلایق بی‌شمار</p></div>
<div class="m2"><p>لیک آن جمله سه خام پخته‌خوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان ناکرده به جانان تاختن</p></div>
<div class="m2"><p>گر هزارانست باشد نیم تن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن یکی بس دور بین و دیده‌کور</p></div>
<div class="m2"><p>از سلیمان کور و دیده پای مور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و آن دگر بس تیزگوش و سخت کر</p></div>
<div class="m2"><p>گنج و در وی نیست یک جو سنگ زر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وآن دگر عور و برهنه لاشه‌باز</p></div>
<div class="m2"><p>لیک دامنهای جامهٔ او دراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت کور اینک سپاهی می‌رسند</p></div>
<div class="m2"><p>من همی‌بینم که چه قومند و چند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت کر آری شنودم بانگشان</p></div>
<div class="m2"><p>که چه می‌گویند پیدا و نهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن برهنه گفت ترسان زین منم</p></div>
<div class="m2"><p>که ببرند از درازی دامنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کور گفت اینک به نزدیک آمدند</p></div>
<div class="m2"><p>خیز بگریزیم پیش از زخم و بند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کر همی‌گوید که آری مشغله</p></div>
<div class="m2"><p>می‌شود نزدیکتر یاران هله</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن برهنه گفت آوه دامنم</p></div>
<div class="m2"><p>از طمع برند و من ناآمنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهر را هشتند و بیرون آمدند</p></div>
<div class="m2"><p>در هزیمت در دهی اندر شدند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندر آن ده مرغ فربه یافتند</p></div>
<div class="m2"><p>لیک ذرهٔ گوشت بر وی نه نژند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرغ مردهٔ خشک وز زخم کلاغ</p></div>
<div class="m2"><p>استخوانها زار گشته چون پناغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زان همی‌خوردند چون از صید شیر</p></div>
<div class="m2"><p>هر یکی از خوردنش چون پیل سیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر سه زان خوردند و بس فربه شدند</p></div>
<div class="m2"><p>چون سه پیل بس بزرگ و مه شدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنچنان کز فربهی هر یک جوان</p></div>
<div class="m2"><p>در نگنجیدی ز زفتی در جهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با چنین گبزی و هفت اندام زفت</p></div>
<div class="m2"><p>از شکاف در برون جستند و رفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>راه مرگ خلق ناپیدا رهیست</p></div>
<div class="m2"><p>در نظر ناید که آن بی‌جا رهیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نک پیاپی کاروانها مقتفی</p></div>
<div class="m2"><p>زین شکاف در که هست آن مختفی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر در ار جویی نیابی آن شکاف</p></div>
<div class="m2"><p>سخت ناپیدا و زو چندین زفاف</p></div></div>