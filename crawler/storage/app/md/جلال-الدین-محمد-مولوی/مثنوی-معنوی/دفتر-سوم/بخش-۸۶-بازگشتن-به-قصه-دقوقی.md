---
title: >-
    بخش ۸۶ - بازگشتن به قصهٔ دقوقی
---
# بخش ۸۶ - بازگشتن به قصهٔ دقوقی

<div class="b" id="bn1"><div class="m1"><p>مر علی را در مثالی شیر خواند</p></div>
<div class="m2"><p>شیر مثل او نباشد گرچه راند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مثال و مثل و فرق آن بران</p></div>
<div class="m2"><p>جانب قصهٔ دقوقی ای جوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنک در فتوی امام خلق بود</p></div>
<div class="m2"><p>گوی تقوی از فرشته می‌ربود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنک اندر سیر مه را مات کرد</p></div>
<div class="m2"><p>هم ز دین‌داری او دین رشک خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنین تقوی و اوراد و قیام</p></div>
<div class="m2"><p>طالب خاصان حق بودی مدام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سفر معظم مرادش آن بدی</p></div>
<div class="m2"><p>که دمی بر بندهٔ خاصی زدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این همی‌گفتی چو می‌رفتی براه</p></div>
<div class="m2"><p>کن قرین خاصگانم ای اله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا رب آنها را که بشناسد دلم</p></div>
<div class="m2"><p>بنده و بسته‌میان و مجملم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و آنک نشناسم تو ای یزدان جان</p></div>
<div class="m2"><p>بر من محجوبشان کن مهربان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حضرتش گفتی که ای صدر مهین</p></div>
<div class="m2"><p>این چه عشقست و چه استسقاست این</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهر من داری چه می‌جویی دگر</p></div>
<div class="m2"><p>چون خدا با تست چون جویی بشر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او بگفتی یا رب ای دانای راز</p></div>
<div class="m2"><p>تو گشودی در دلم راه نیاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درمیان بحر اگر بنشسته‌ام</p></div>
<div class="m2"><p>طمع در آب سبو هم بسته‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچو داودم نود نعجه مراست</p></div>
<div class="m2"><p>طمع در نعجهٔ حریفم هم بخاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حرص اندر عشق تو فخرست و جاه</p></div>
<div class="m2"><p>حرص اندر غیر تو ننگ و تباه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهوت و حرص نران بیشی بود</p></div>
<div class="m2"><p>و آن حیزان ننگ و بدکیشی بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حرص مردان از ره پیشی بود</p></div>
<div class="m2"><p>در مخنث حرص سوی پس رود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن یکی حرص از کمال مردی است</p></div>
<div class="m2"><p>و آن دگر حرص افتضاح و سردی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آه سری هست اینجا بس نهان</p></div>
<div class="m2"><p>که سوی خضری شود موسی روان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همچو مستسقی کز آبش سیر نیست</p></div>
<div class="m2"><p>بر هر آنچ یافتی بالله مه‌ایست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی نهایت حضرتست این بارگاه</p></div>
<div class="m2"><p>صدر را بگذار صدر تست راه</p></div></div>