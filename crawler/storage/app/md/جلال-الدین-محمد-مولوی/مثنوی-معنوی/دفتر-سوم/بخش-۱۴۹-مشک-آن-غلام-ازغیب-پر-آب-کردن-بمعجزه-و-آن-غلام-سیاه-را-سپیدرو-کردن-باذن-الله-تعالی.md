---
title: >-
    بخش ۱۴۹ - مشک آن غلام ازغیب پر آب کردن بمعجزه و آن غلام سیاه را سپیدرو کردن باذن الله تعالی
---
# بخش ۱۴۹ - مشک آن غلام ازغیب پر آب کردن بمعجزه و آن غلام سیاه را سپیدرو کردن باذن الله تعالی

<div class="b" id="bn1"><div class="m1"><p>ای غلام اکنون تو پر بین مشک خود</p></div>
<div class="m2"><p>تا نگویی درشکایت نیک و بد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سیه حیران شد از برهان او</p></div>
<div class="m2"><p>می‌دمید از لامکان ایمان او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمه‌ای دید از هوا ریزان شده</p></div>
<div class="m2"><p>مشک او روپوش فیض آن شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان نظر روپوشها هم بر درید</p></div>
<div class="m2"><p>تا معین چشمهٔ غیبی بدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمها پر آب کرد آن دم غلام</p></div>
<div class="m2"><p>شد فراموشش ز خواجه وز مقام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست و پایش ماند از رفتن به راه</p></div>
<div class="m2"><p>زلزله افکند در جانش اله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز بهر مصلحت بازش کشید</p></div>
<div class="m2"><p>که به خویش آ باز رو ای مستفید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقت حیرت نیست حیرت پیش تست</p></div>
<div class="m2"><p>این زمان در ره در آ چالاک و چست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستهای مصطفی بر رو نهاد</p></div>
<div class="m2"><p>بوسه‌های عاشقانه بس بداد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مصطفی دست مبارک بر رخش</p></div>
<div class="m2"><p>آن زمان مالید و کرد او فرخش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد سپید آن زنگی و زادهٔ حبش</p></div>
<div class="m2"><p>همچو بدر و روز روشن شد شبش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یوسفی شد در جمال و در دلال</p></div>
<div class="m2"><p>گفتش اکنون رو بده وا گوی حال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او همی‌شد بی سر و بی پای مست</p></div>
<div class="m2"><p>پای می‌نشناخت در رفتن ز دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس بیامد با دو مشک پر روان</p></div>
<div class="m2"><p>سوی خواجه از نواحی کاروان</p></div></div>