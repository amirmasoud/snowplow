---
title: >-
    بخش ۹۴ - یک درخت شدن آن هفت درخت
---
# بخش ۹۴ - یک درخت شدن آن هفت درخت

<div class="b" id="bn1"><div class="m1"><p>گفت راندم پیشتر من نیکبخت</p></div>
<div class="m2"><p>باز شد آن هفت جمله یک درخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هفت می‌شد فرد می‌شد هر دمی</p></div>
<div class="m2"><p>من چه سان می‌گشتم از حیرت همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد از آن دیدم درختان در نماز</p></div>
<div class="m2"><p>صف کشیده چون جماعت کرده ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک درخت از پیش مانند امام</p></div>
<div class="m2"><p>دیگران اندر پس او در قیام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن قیام و آن رکوع و آن سجود</p></div>
<div class="m2"><p>از درختان بس شگفتم می‌نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد کردم قول حق را آن زمان</p></div>
<div class="m2"><p>گفت النجم و شجر را یسجدان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این درختان را نه زانو نه میان</p></div>
<div class="m2"><p>این چه ترتیب نمازست آنچنان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمد الهام خدا کای بافروز</p></div>
<div class="m2"><p>می عجب داری ز کار ما هنوز</p></div></div>