---
title: >-
    بخش ۴۴ - فرستادن فرعون به مداین در طلب ساحران
---
# بخش ۴۴ - فرستادن فرعون به مداین در طلب ساحران

<div class="b" id="bn1"><div class="m1"><p>چونک موسی بازگشت و او بماند</p></div>
<div class="m2"><p>اهل رای و مشورت را پیش خواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچنان دیدند کز اطراف مصر</p></div>
<div class="m2"><p>جمع آردشان شه و صراف مصر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او بسی مردم فرستاد آن زمان</p></div>
<div class="m2"><p>هر نواحی بهر جمع جادوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر طرف که ساحری بد نامدار</p></div>
<div class="m2"><p>کرد پران سوی او ده پیک کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو جوان بودند ساحر مشتهر</p></div>
<div class="m2"><p>سحر ایشان در دل مه مستمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیر دوشیده ز مه فاش آشکار</p></div>
<div class="m2"><p>در سفرها رفته بر خمی سوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکل کرباسی نموده ماهتاب</p></div>
<div class="m2"><p>آن بپیموده فروشیده شتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیم برده مشتری آگه شده</p></div>
<div class="m2"><p>دست از حسرت به رخها بر زده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد هزاران همچنین در جادوی</p></div>
<div class="m2"><p>بوده منشی و نبوده چون روی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بدیشان آمد آن پیغام شاه</p></div>
<div class="m2"><p>کز شما شاهست اکنون چاره‌خواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پی آنک دو درویش آمدند</p></div>
<div class="m2"><p>بر شه و بر قصر او موکب زدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست با ایشان بغیر یک عصا</p></div>
<div class="m2"><p>که همی‌گردد به امرش اژدها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه و لشکر جمله بیچاره شدند</p></div>
<div class="m2"><p>زین دو کس جمله به افغان آمدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چاره‌ای می‌باید اندر ساحری</p></div>
<div class="m2"><p>تا بود که زین دو ساحر جان بری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن دو ساحر را چو این پیغام داد</p></div>
<div class="m2"><p>ترس و مهری در دل هر دو فتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عرق جنسیت چو جنبیدن گرفت</p></div>
<div class="m2"><p>سر به زانو بر نهادند از شگفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون دبیرستان صوفی زانوست</p></div>
<div class="m2"><p>حل مشکل را دو زانو جادوست</p></div></div>