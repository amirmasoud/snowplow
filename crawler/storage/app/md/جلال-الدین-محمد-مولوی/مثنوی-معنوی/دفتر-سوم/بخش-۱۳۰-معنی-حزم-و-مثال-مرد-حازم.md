---
title: >-
    بخش ۱۳۰ - معنی حزم و مثال مرد حازم
---
# بخش ۱۳۰ - معنی حزم و مثال مرد حازم

<div class="b" id="bn1"><div class="m1"><p>یا به حال اولینان بنگرید</p></div>
<div class="m2"><p>یا سوی آخر بحزمی در پرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حزم چه بود در دو تدبیر احتیاط</p></div>
<div class="m2"><p>از دو آن گیری که دورست از خباط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یکی گوید درین ره هفت روز</p></div>
<div class="m2"><p>نیست آب و هست ریگ پای‌سوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دگر گوید دروغست این بران</p></div>
<div class="m2"><p>که بهر شب چشمه‌ای بینی روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حزم آن باشد که بر گیری تو آب</p></div>
<div class="m2"><p>تا رهی از ترس و باشی بر صواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بود در راه آب این را بریز</p></div>
<div class="m2"><p>ور نباشد وای بر مرد ستیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خلیفه‌زادگان دادی کنید</p></div>
<div class="m2"><p>حزم بهر روز میعادی کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن عدوی کز پدرتان کین کشید</p></div>
<div class="m2"><p>سوی زندانش ز علیین کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن شه شطرنج دل را مات کرد</p></div>
<div class="m2"><p>از بهشتش سخرهٔ آفات کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند جا بندش گرفت اندر نبرد</p></div>
<div class="m2"><p>تا بکشتی در فکندش روی‌زرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اینچنین کردست با آن پهلوان</p></div>
<div class="m2"><p>سست سستش منگرید ای دیگران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مادر و بابای ما را آن حسود</p></div>
<div class="m2"><p>تاج و پیرایه بچالاکی ربود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کردشان آنجا برهنه و زار و خوار</p></div>
<div class="m2"><p>سالها بگریست آدم زار زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که ز اشک چشم او رویید نبت</p></div>
<div class="m2"><p>که چرا اندر جریدهٔ لاست ثبت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو قیاسی گیر طراریش را</p></div>
<div class="m2"><p>که چنان سرور کند زو ریش را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الحذر ای گل‌پرستان از شرش</p></div>
<div class="m2"><p>تیغ لا حولی زنید اندر سرش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کو همی‌بیند شما را از کمین</p></div>
<div class="m2"><p>که شما او را نمی‌بینید هین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دایما صیاد ریزد دانه‌ها</p></div>
<div class="m2"><p>دانه پیدا باشد و پنهان دغا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر کجا دانه بدیدی الحذر</p></div>
<div class="m2"><p>تا نبندد دام بر تو بال و پر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زانک مرغی کو بترک دانه کرد</p></div>
<div class="m2"><p>دانه از صحرای بی تزویر خورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هم بدان قانع شد و از دام جست</p></div>
<div class="m2"><p>هیچ دامی پر و بالش را نبست</p></div></div>