---
title: >-
    بخش ۷۳ - متهم کردن آن شیخ را با دزدان وبریدن دستش را
---
# بخش ۷۳ - متهم کردن آن شیخ را با دزدان وبریدن دستش را

<div class="b" id="bn1"><div class="m1"><p>بیست از دزدان بدند آنجا و بیش</p></div>
<div class="m2"><p>بخش می‌کردند مسروقات خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شحنه را غماز آگه کرده بود</p></div>
<div class="m2"><p>مردم شحنه بر افتادند زود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم بدان‌جا پای چپ و دست راست</p></div>
<div class="m2"><p>جمله را ببرید و غوغایی بخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست زاهد هم بریده شد غلط</p></div>
<div class="m2"><p>پاش را می‌خواست هم کردن سقط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زمان آمد سواری بس گزین</p></div>
<div class="m2"><p>بانگ بر زد بر عوان کای سگ ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این فلان شیخست از ابدال خدا</p></div>
<div class="m2"><p>دست او را تو چرا کردی جدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن عوان بدرید جامه تیز رفت</p></div>
<div class="m2"><p>پیش شحنه داد آگاهیش تفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شحنه آمد پا برهنه عذرخواه</p></div>
<div class="m2"><p>که ندانستم خدا بر من گواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هین بحل کن مر مرا زین کار زشت</p></div>
<div class="m2"><p>ای کریم و سرور اهل بهشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت می‌دانم سبب این نیش را</p></div>
<div class="m2"><p>می‌شناسم من گناه خویش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من شکستم حرمت ایمان او</p></div>
<div class="m2"><p>پس یمینم برد دادستان او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من شکستم عهد و دانستم بدست</p></div>
<div class="m2"><p>تا رسید آن شومی جرات بدست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست ما و پای ما و مغز و پوست</p></div>
<div class="m2"><p>باد ای والی فدای حکم دوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قسم من بود این ترا کردم حلال</p></div>
<div class="m2"><p>تو ندانستی ترا نبود وبال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>و آنک او دانست او فرمان‌رواست</p></div>
<div class="m2"><p>با خدا سامان پیچیدن کجاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای بسا مرغی پریده دانه‌جو</p></div>
<div class="m2"><p>که بریده حلق او هم حلق او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بسا مرغی ز معده وز مغص</p></div>
<div class="m2"><p>بر کنار بام محبوس قفس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای بسا ماهی در آب دوردست</p></div>
<div class="m2"><p>گشته از حرص گلو ماخوذ شست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای بسا مستور در پرده بده</p></div>
<div class="m2"><p>شومی فرج و گلو رسوا شده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای بسا قاضی حبر نیک‌خو</p></div>
<div class="m2"><p>از گلو و رشوتی او زردرو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بلک در هاروت و ماروت آن شراب</p></div>
<div class="m2"><p>از عروج چرخشان شد سد باب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با یزید از بهر این کرد احتراز</p></div>
<div class="m2"><p>دید در خود کاهلی اندر نماز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از سبب اندیشه کرد آن ذو لباب</p></div>
<div class="m2"><p>دید علت خوردن بسیار از آب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت تا سالی نخواهم خورد آب</p></div>
<div class="m2"><p>آنچنان کرد و خدایش داد تاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این کمینه جهد او بد بهر دین</p></div>
<div class="m2"><p>گشت او سلطان و قطب العارفین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون بریده شد برای حلق دست</p></div>
<div class="m2"><p>مرد زاهد را در شکوی ببست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شیخ اقطع گشت نامش پیش خلق</p></div>
<div class="m2"><p>کرد معروفش بدین آفات حلق</p></div></div>