---
title: >-
    بخش ۱۹۹ - تمثیل صابر شدن مؤمن چون بر شر و خیر بلا واقف شود
---
# بخش ۱۹۹ - تمثیل صابر شدن مؤمن چون بر شر و خیر بلا واقف شود

<div class="b" id="bn1"><div class="m1"><p>سگ شکاری نیست او را طوق نیست</p></div>
<div class="m2"><p>خام و ناجوشیده جز بی‌ذوق نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت نخود چون چنینست ای ستی</p></div>
<div class="m2"><p>خوش بجوشم یاریم ده راستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو درین جوشش چو معمار منی</p></div>
<div class="m2"><p>کفچلیزم زن که بس خوش می‌زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو پیلم بر سرم زن زخم و داغ</p></div>
<div class="m2"><p>تا نبینم خواب هندستان و باغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که خود را در دهم در جوش من</p></div>
<div class="m2"><p>تا رهی یابم در آن آغوش من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانک انسان در غنا طاغی شود</p></div>
<div class="m2"><p>همچو پیل خواب‌بین یاغی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیل چون در خواب بیند هند را</p></div>
<div class="m2"><p>پیلبان را نشنود آرد دغا</p></div></div>