---
title: >-
    بخش ۱۱۱ - حکم کردن داود بر صاحب گاو کی جمله مال خود را به وی ده
---
# بخش ۱۱۱ - حکم کردن داود بر صاحب گاو کی جمله مال خود را به وی ده

<div class="b" id="bn1"><div class="m1"><p>بعد از آن داود گفتش کای عنود</p></div>
<div class="m2"><p>جمله مال خویش او را بخش زود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورنه کارت سخت گردد گفتمت</p></div>
<div class="m2"><p>تا نگردد ظاهر از وی استمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک بر سر کرد و جامه بر درید</p></div>
<div class="m2"><p>که بهر دم می‌کنی ظلمی مزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک‌دمی دیگر برین تشنیع راند</p></div>
<div class="m2"><p>باز داودش به پیش خویش خواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت چون بختت نبود ای بخت‌کور</p></div>
<div class="m2"><p>ظلمت آمد اندک اندک در ظهور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریده‌ای آنگاه صدر و پیشگاه</p></div>
<div class="m2"><p>ای دریغ از چون تو خر خاشاک و کاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو که فرزندان تو با جفت تو</p></div>
<div class="m2"><p>بندگان او شدند افزون مگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگ بر سینه همی‌زد با دو دست</p></div>
<div class="m2"><p>می‌دوید از جهل خود بالا و پست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق هم اندر ملامت آمدند</p></div>
<div class="m2"><p>کز ضمیر کار او غافل بدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظالم از مظلوم کی داند کسی</p></div>
<div class="m2"><p>کو بود سخرهٔ هوا همچون خسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظالم از مظلوم آنکس پی برد</p></div>
<div class="m2"><p>کو سر نفس ظلوم خود برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ورنه آن ظالم که نفس است از درون</p></div>
<div class="m2"><p>خصم هر مظلوم باشد از جنون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سگ هماره حمله بر مسکین کند</p></div>
<div class="m2"><p>تا تواند زخم بر مسکین زند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شرم شیران راست نه سگ را بدان</p></div>
<div class="m2"><p>که نگیرد صید از همسایگان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عامهٔ مظلوم‌کش ظالم‌پرست</p></div>
<div class="m2"><p>از کمین سگشان سوی داود جست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی در داود کردند آن فریق</p></div>
<div class="m2"><p>کای نبی مجتبی بر ما شفیق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این نشاید از تو کین ظلمیست فاش</p></div>
<div class="m2"><p>قهر کردی بی‌گناهی را بلاش</p></div></div>