---
title: >-
    بخش ۱۲۶ - بیان آنک هر کس را نرسد مثل آوردن  خاصه در کار الهی
---
# بخش ۱۲۶ - بیان آنک هر کس را نرسد مثل آوردن  خاصه در کار الهی

<div class="b" id="bn1"><div class="m1"><p>کی رسدتان این مثلها ساختن</p></div>
<div class="m2"><p>سوی آن درگاه پاک انداختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مثل آوردن آن حضرتست</p></div>
<div class="m2"><p>که بعلم سر و جهر او آیتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چه دانی سر چیزی تا تو کل</p></div>
<div class="m2"><p>یا به زلفی یا به رخ آری مثل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موسیی آن را عصا دید و نبود</p></div>
<div class="m2"><p>اژدها بد سر او لب می‌گشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون چنان شاهی نداند سر چوب</p></div>
<div class="m2"><p>تو چه دانی سر این دام و حبوب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون غلط شد چشم موسی در مثل</p></div>
<div class="m2"><p>چون کند موشی فضولی مدخل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن مثالت را چو اژدرها کند</p></div>
<div class="m2"><p>تا به پاسخ جزو جزوت بر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این مثال آورد ابلیس لعین</p></div>
<div class="m2"><p>تا که شد ملعون حق تا یوم دین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این مثال آورد قارون از لجاج</p></div>
<div class="m2"><p>تا فرو شد در زمین با تخت و تاج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این مثالت را چو زاغ و بوم دان</p></div>
<div class="m2"><p>که ازیشان پست شد صد خاندان</p></div></div>