---
title: >-
    بخش ۳۵ - بوجود آمدن موسی و آمدن عوانان به خانهٔ  عمران و وحی آمدن به مادر موسی کی موسی را در آتش انداز
---
# بخش ۳۵ - بوجود آمدن موسی و آمدن عوانان به خانهٔ  عمران و وحی آمدن به مادر موسی کی موسی را در آتش انداز

<div class="b" id="bn1"><div class="m1"><p>خود زن عمران که موسی برده بود</p></div>
<div class="m2"><p>دامن اندر چید از آن آشوب و دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن زنان قابله در خانه‌ها</p></div>
<div class="m2"><p>بهر جاسوسی فرستاد آن دغا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمز کردندش که اینجا کودکیست</p></div>
<div class="m2"><p>نامد او میدان که در وهم و شکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندرین کوچه یکی زیبا زنیست</p></div>
<div class="m2"><p>کودکی دارد ولیکن پرفنیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس عوانان آمدند او طفل را</p></div>
<div class="m2"><p>در تنور انداخت از امر خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحی آمد سوی زن زان با خبر</p></div>
<div class="m2"><p>که ز اصل آن خلیلست این پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عصمت یا نار کونی باردا</p></div>
<div class="m2"><p>لا تکون النار حرا شاردا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زن بوحی انداخت او را در شرر</p></div>
<div class="m2"><p>بر تن موسی نکرد آتش اثر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس عوانان بی مراد آن سو شدند</p></div>
<div class="m2"><p>باز غمازان کز آن واقف بدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با عوانان ماجرا بر داشتند</p></div>
<div class="m2"><p>پیش فرعون از برای دانگ چند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کای عوانان باز گردید آن طرف</p></div>
<div class="m2"><p>نیک نیکو بنگرید اندر غرف</p></div></div>