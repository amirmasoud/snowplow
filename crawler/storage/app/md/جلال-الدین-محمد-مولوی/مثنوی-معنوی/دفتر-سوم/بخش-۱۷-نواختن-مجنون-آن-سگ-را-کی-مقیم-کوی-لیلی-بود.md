---
title: >-
    بخش ۱۷ - نواختن مجنون آن سگ را کی مقیم  کوی لیلی بود
---
# بخش ۱۷ - نواختن مجنون آن سگ را کی مقیم  کوی لیلی بود

<div class="b" id="bn1"><div class="m1"><p>همچو مجنون کو سگی را می‌نواخت</p></div>
<div class="m2"><p>بوسه‌اش می‌داد و پیشش می‌گداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد او می‌گشت خاضع در طواف</p></div>
<div class="m2"><p>هم جلاب شکرش می‌داد صاف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوالفضولی گفت ای مجنون خام</p></div>
<div class="m2"><p>این چه شیدست این که می‌آری مدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پوز سگ دایم پلیدی می‌خورد</p></div>
<div class="m2"><p>مقعد خود را بلب می‌استرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیبهای سگ بسی او بر شمرد</p></div>
<div class="m2"><p>عیب‌دان از غیب‌دان بویی نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت مجنون تو همه نقشی و تن</p></div>
<div class="m2"><p>اندر آ و بنگرش از چشم من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کین طلسم بستهٔ مولیست این</p></div>
<div class="m2"><p>پاسبان کوچهٔ لیلیست این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همنشین بین و دل و جان و شناخت</p></div>
<div class="m2"><p>کو کجا بگزید و مسکن‌گاه ساخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او سگ فرخ‌رخ کهف منست</p></div>
<div class="m2"><p>بلک او هم‌درد و هم‌لهف منست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن سگی که باشد اندر کوی او</p></div>
<div class="m2"><p>من به شیران کی دهم یک موی او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای که شیران مر سگانش را غلام</p></div>
<div class="m2"><p>گفت امکان نیست خامش والسلام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ز صورت بگذرید ای دوستان</p></div>
<div class="m2"><p>جنتست و گلستان در گلستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صورت خود چون شکستی سوختی</p></div>
<div class="m2"><p>صورت کل را شکست آموختی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بعد از آن هر صورتی را بشکنی</p></div>
<div class="m2"><p>همچو حیدر باب خیبر بر کنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سغبهٔ صورت شد آن خواجهٔ سلیم</p></div>
<div class="m2"><p>که به ده می‌شد بگفتاری سقیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوی دام آن تملق شادمان</p></div>
<div class="m2"><p>همچو مرغی سوی دانهٔ امتحان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از کرم دانست مرغ آن دانه را</p></div>
<div class="m2"><p>غایت حرص است نه جود آن عطا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرغکان در طمع دانه شادمان</p></div>
<div class="m2"><p>سوی آن تزویر پران و دوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ز شادی خواجه آگاهت کنم</p></div>
<div class="m2"><p>ترسم ای ره‌رو که بیگاهت کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مختصر کردم چو آمد ده پدید</p></div>
<div class="m2"><p>خود نبود آن ده ره دیگر گزید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قرب ماهی ده بده می‌تاختند</p></div>
<div class="m2"><p>زانک راه ده نکو نشناختند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که در ره بی قلاوزی رود</p></div>
<div class="m2"><p>هر دو روزه راه صدساله شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که تازد سوی کعبه بی دلیل</p></div>
<div class="m2"><p>همچو این سرگشتگان گردد ذلیل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که گیرد پیشه‌ای بی‌اوستا</p></div>
<div class="m2"><p>ریش‌خندی شد بشهر و روستا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جز که نادر باشد اندر خافقین</p></div>
<div class="m2"><p>آدمی سر بر زند بی والدین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مال او یابد که کسبی می‌کند</p></div>
<div class="m2"><p>نادری باشد که بر گنجی زند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مصطفایی کو که جسمش جان بود</p></div>
<div class="m2"><p>تا که رحمن علم‌القرآن بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اهل تن را جمله علم بالقلم</p></div>
<div class="m2"><p>واسطه افراشت در بذل کرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر حریصی هست محروم ای پسر</p></div>
<div class="m2"><p>چون حریصان تگ مرو آهسته‌تر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اندر آن ره رنجها دیدند و تاب</p></div>
<div class="m2"><p>چون عذاب مرغ خاکی در عذاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سیر گشته از ده و از روستا</p></div>
<div class="m2"><p>وز شکرریز چنان نا اوستا</p></div></div>