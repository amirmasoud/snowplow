---
title: >-
    بخش ۲۱۲ - ملاقات آن عاشق با صدر جهان
---
# بخش ۲۱۲ - ملاقات آن عاشق با صدر جهان

<div class="b" id="bn1"><div class="m1"><p>آن بخاری نیز خود بر شمع زد</p></div>
<div class="m2"><p>گشته بود از عشقش آسان آن کبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه سوزانش سوی گردون شده</p></div>
<div class="m2"><p>در دل صدر جهان مهر آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته با خود در سحرگه کای احد</p></div>
<div class="m2"><p>حال آن آوارهٔ ما چون بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او گناهی کرد و ما دیدیم لیک</p></div>
<div class="m2"><p>رحمت ما را نمی‌دانست نیک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطر مجرم ز ما ترسان شود</p></div>
<div class="m2"><p>لیک صد اومید در ترسش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بترسانم وقیح یاوه را</p></div>
<div class="m2"><p>آنک ترسد من چه ترسانم ورا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر دیگ سرد آذر می‌رود</p></div>
<div class="m2"><p>نه بدان کز جوش از سر می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمنان را من بترسانم به علم</p></div>
<div class="m2"><p>خایفان را ترس بردارم به حلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پاره‌دوزم پاره در موضع نهم</p></div>
<div class="m2"><p>هر کسی را شربت اندر خور دهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست سر مرد چون بیخ درخت</p></div>
<div class="m2"><p>زان بروید برگهاش از چوب سخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درخور آن بیخ رسته برگها</p></div>
<div class="m2"><p>در درخت و در نفوس و در نهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برفلک پرهاست ز اشجار وفا</p></div>
<div class="m2"><p>اصلها ثابت و فرعه فی السما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون برست از عشق پر بر آسمان</p></div>
<div class="m2"><p>چون نروید در دل صدر جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>موج می‌زد در دلش عفو گنه</p></div>
<div class="m2"><p>که ز هر دل تا دل آمد روزنه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که ز دل تا دل یقین روزن بود</p></div>
<div class="m2"><p>نه جدا و دور چون دو تن بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>متصل نبود سفال دو چراغ</p></div>
<div class="m2"><p>نورشان ممزوج باشد در مساغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچ عاشق خود نباشد وصل‌جو</p></div>
<div class="m2"><p>که نه معشوقش بود جویای او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لیک عشق عاشقان تن زه کند</p></div>
<div class="m2"><p>عشق معشوقان خوش و فربه کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون درین دل برق مهر دوست جست</p></div>
<div class="m2"><p>اندر آن دل دوستی می‌دان که هست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در دل تو مهر حق چون شد دوتو</p></div>
<div class="m2"><p>هست حق را بی گمانی مهر تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هیچ بانگ کف زدن ناید بدر</p></div>
<div class="m2"><p>از یکی دست تو بی دستی دگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تشنه می‌نالد که ای آب گوار</p></div>
<div class="m2"><p>آب هم نالد که کو آن آب‌خوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جذب آبست این عطش در جان ما</p></div>
<div class="m2"><p>ما از آن او و او هم آن ما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حکمت حق در قضا و در قدر</p></div>
<div class="m2"><p>کرد ما را عاشقان همدگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جمله اجزای جهان زان حکم پیش</p></div>
<div class="m2"><p>جفت جفت و عاشقان جفت خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هست هر جزوی ز عالم جفت‌خواه</p></div>
<div class="m2"><p>راست همچون کهربا و برگ کاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آسمان گوید زمین را مرحبا</p></div>
<div class="m2"><p>با توم چون آهن و آهن‌ربا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آسمان مرد و زمین زن در خرد</p></div>
<div class="m2"><p>هرچه آن انداخت این می‌پرورد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون نماند گرمیش بفرستد او</p></div>
<div class="m2"><p>چون نماند تری و نم بدهد او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برج خاکی خاک ارضی را مدد</p></div>
<div class="m2"><p>برج آبی تریش اندر دمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برج بادی ابر سوی او برد</p></div>
<div class="m2"><p>تا بخارات وخم را بر کشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برج آتش گرمی خورشید ازو</p></div>
<div class="m2"><p>همچو تابهٔ سرخ ز آتش پشت و رو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هست سرگردان فلک اندر زمن</p></div>
<div class="m2"><p>همچو مردان گرد مکسب بهر زن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وین زمین کدبانویها می‌کند</p></div>
<div class="m2"><p>بر ولادات و رضاعش می‌تند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس زمین و چرخ را دان هوشمند</p></div>
<div class="m2"><p>چونک کار هوشمندان می‌کنند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر نه از هم این دو دلبر می‌مزند</p></div>
<div class="m2"><p>پس چرا چون جفت در هم می‌خزند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بی زمین کی گل بروید و ارغوان</p></div>
<div class="m2"><p>پس چه زاید ز آب و تاب آسمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بهر آن میلست در ماده به نر</p></div>
<div class="m2"><p>تا بود تکمیل کار همدگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>میل اندر مرد و زن حق زان نهاد</p></div>
<div class="m2"><p>تا بقا یابد جهان زین اتحاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>میل هر جزوی به جزوی هم نهد</p></div>
<div class="m2"><p>ز اتحاد هر دو تولیدی زهد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شب چنین با روز اندر اعتناق</p></div>
<div class="m2"><p>مختلف در صورت اما اتفاق</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روز و شب ظاهر دو ضد و دشمنند</p></div>
<div class="m2"><p>لیک هر دو یک حقیقت می‌تنند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر یکی خواهان دگر را همچو خویش</p></div>
<div class="m2"><p>از پی تکمیل فعل و کار خویش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زانک بی شب دخل نبود طبع را</p></div>
<div class="m2"><p>پس چه اندر خرج آرد روزها</p></div></div>