---
title: >-
    بخش ۲۰۸ - مثل زدن در رمیدن کرهٔ اسپ از آب خوردن به سبب شخولیدن سایسان
---
# بخش ۲۰۸ - مثل زدن در رمیدن کرهٔ اسپ از آب خوردن به سبب شخولیدن سایسان

<div class="b" id="bn1"><div class="m1"><p>آنک فرمودست او اندر خطاب</p></div>
<div class="m2"><p>کره و مادر همی‌خوردند آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌شخولیدند هر دم آن نفر</p></div>
<div class="m2"><p>بهر اسپان که هلا هین آب خور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن شخولیدن به کره می‌رسید</p></div>
<div class="m2"><p>سر همی بر داشت و از خور می‌رمید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مادرش پرسید کای کره چرا</p></div>
<div class="m2"><p>می‌رمی هر ساعتی زین استقا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت کره می‌شخولند این گروه</p></div>
<div class="m2"><p>ز اتفاق بانگشان دارم شکوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس دلم می‌لرزد از جا می‌رود</p></div>
<div class="m2"><p>ز اتفاق نعره خوفم می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت مادر تا جهان بودست ازین</p></div>
<div class="m2"><p>کارافزایان بدند اندر زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هین تو کار خویش کن ای ارجمند</p></div>
<div class="m2"><p>زود کایشان ریش خود بر می‌کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وقت تنگ و می‌رود آب فراخ</p></div>
<div class="m2"><p>پیش از آن کز هجر گردی شاخ شاخ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهره کاریزیست پر آب حیات</p></div>
<div class="m2"><p>آب کش تا بر دمد از تو نبات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب خضر از جوی نطق اولیا</p></div>
<div class="m2"><p>می‌خوریم ای تشنهٔ غافل بیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نبینی آب کورانه بفن</p></div>
<div class="m2"><p>سوی جو آور سبو در جوی زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شنیدی کاندرین جو آب هست</p></div>
<div class="m2"><p>کور را تقلید باید کار بست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جو فرو بر مشک آب‌اندیش را</p></div>
<div class="m2"><p>تا گران بینی تو مشک خویش را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون گران دیدی شوی تو مستدل</p></div>
<div class="m2"><p>رست از تقلید خشک آنگاه دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر نبیند کور آب جو عیان</p></div>
<div class="m2"><p>لیک داند چون سبو بیند گران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ز جو اندر سبو آبی برفت</p></div>
<div class="m2"><p>کین سبک بود و گران شد ز آب و زفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زانک هر بادی مرا در می‌ربود</p></div>
<div class="m2"><p>باد می‌نربایدم ثقلم فزود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مر سفیهان را رباید هر هوا</p></div>
<div class="m2"><p>زانک نبودشان گرانی قوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کشتی بی‌لنگر آمد مرد شر</p></div>
<div class="m2"><p>که ز باد کژ نیابد او حذر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لنگر عقلست عاقل را امان</p></div>
<div class="m2"><p>لنگری در یوزه کن از عاقلان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>او مددهای خرد چون در ربود</p></div>
<div class="m2"><p>از خزینه در آن دریای جود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زین چنین امداد دل پر فن شود</p></div>
<div class="m2"><p>بجهد از دل چشم هم روشن شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زانک نور از دل برین دیده نشست</p></div>
<div class="m2"><p>تا چو دل شد دیدهٔ تو عاطلست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل چو بر انوار عقلی نیز زد</p></div>
<div class="m2"><p>زان نصیبی هم بدو دیده دهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس بدان کاب مبارک ز آسمان</p></div>
<div class="m2"><p>وحی دلها باشد و صدق بیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ما چو آن کره هم آب جو خوریم</p></div>
<div class="m2"><p>سوی آن وسواس طاعن ننگریم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پی‌رو پیغمبرانی ره سپر</p></div>
<div class="m2"><p>طعنهٔ خلقان همه بادی شمر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن خداوندان که ره طی کرده‌اند</p></div>
<div class="m2"><p>گوش فا بانگ سگان کی کرده‌اند</p></div></div>