---
title: >-
    بخش ۱۸۸ - رسیدن آن عاشق به معشوق خویش چون دست از جان خود بشست
---
# بخش ۱۸۸ - رسیدن آن عاشق به معشوق خویش چون دست از جان خود بشست

<div class="b" id="bn1"><div class="m1"><p>همچو گویی سجده کن بر رو و سر</p></div>
<div class="m2"><p>جانب آن صدر شد با چشم تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله خلقان منتظر سر در هوا</p></div>
<div class="m2"><p>کش بسوزد یا برآویزد ورا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این زمان این احمق یک لخت را</p></div>
<div class="m2"><p>آن نماید که زمان بدبخت را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو پروانه شرر را نور دید</p></div>
<div class="m2"><p>احمقانه در فتاد از جان برید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک شمع عشق چون آن شمع نیست</p></div>
<div class="m2"><p>روشن اندر روشن اندر روشنیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او به عکس شمعهای آتشیست</p></div>
<div class="m2"><p>می‌نماید آتش و جمله خوشیست</p></div></div>