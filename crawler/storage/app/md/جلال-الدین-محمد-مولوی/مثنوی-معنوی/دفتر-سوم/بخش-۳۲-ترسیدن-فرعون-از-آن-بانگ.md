---
title: >-
    بخش ۳۲ - ترسیدن فرعون از آن بانگ
---
# بخش ۳۲ - ترسیدن فرعون از آن بانگ

<div class="b" id="bn1"><div class="m1"><p>این صدا جان مرا تغییر کرد</p></div>
<div class="m2"><p>از غم و اندوه تلخم پیر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش می‌آمد سپس می‌رفت شه</p></div>
<div class="m2"><p>جمله شب او همچو حامل وقت زه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان می‌گفت ای عمران مرا</p></div>
<div class="m2"><p>سخت از جا برده است این نعره‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهره نه عمران مسکین را که تا</p></div>
<div class="m2"><p>باز گوید اختلاط جفت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که زن عمران به عمران در خزید</p></div>
<div class="m2"><p>تا که شد استارهٔ موسی پدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر پیمبر که در آید در رحم</p></div>
<div class="m2"><p>نجم او بر چرخ گردد منتجم</p></div></div>