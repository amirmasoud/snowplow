---
title: >-
    بخش ۱۵۴ - وجه عبرت گرفتن ازین حکایت و یقین دانستن کی ان مع العسر یسرا
---
# بخش ۱۵۴ - وجه عبرت گرفتن ازین حکایت و یقین دانستن کی ان مع العسر یسرا

<div class="b" id="bn1"><div class="m1"><p>عبرتست آن قصه ای جان مر ترا</p></div>
<div class="m2"><p>تا که راضی باشی در حکم خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که زیرک باشی و نیکوگمان</p></div>
<div class="m2"><p>چون ببینی واقعهٔ بد ناگهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران گردند زرد از بیم آن</p></div>
<div class="m2"><p>تو چو گل خندان گه سود و زیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زانک گل گر برگ برگش می‌کنی</p></div>
<div class="m2"><p>خنده نگذارد نگردد منثنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوید از خاری چرا افتم بغم</p></div>
<div class="m2"><p>خنده را من خود ز خار آورده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه از تو یاوه گردد از قضا</p></div>
<div class="m2"><p>تو یقین دان که خریدت از بلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما التصوف قال وجدان الفرح</p></div>
<div class="m2"><p>فی الفؤاد عند اتیان الترح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن عقابش را عقابی دان که او</p></div>
<div class="m2"><p>در ربود آن موزه را زان نیک‌خو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا رهاند پاش را از زخم مار</p></div>
<div class="m2"><p>ای خنک عقلی که باشد بی غبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت لا تاسوا علی ما فاتکم</p></div>
<div class="m2"><p>ان اتی السرحان واردی شاتکم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کان بلا دفع بلاهای بزرگ</p></div>
<div class="m2"><p>و آن زیان منع زیانهای سترگ</p></div></div>