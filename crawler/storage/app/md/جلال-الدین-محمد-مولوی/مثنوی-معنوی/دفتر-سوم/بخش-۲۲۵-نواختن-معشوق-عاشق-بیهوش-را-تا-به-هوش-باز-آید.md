---
title: >-
    بخش ۲۲۵ - نواختن معشوق عاشق بیهوش را تا به هوش باز آید
---
# بخش ۲۲۵ - نواختن معشوق عاشق بیهوش را تا به هوش باز آید

<div class="b" id="bn1"><div class="m1"><p>می‌کشید از بیهشی‌اش در بیان</p></div>
<div class="m2"><p>اندک اندک از کرم صدر جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانگ زد در گوش او شه کای گدا</p></div>
<div class="m2"><p>زر نثار آوردمت دامن گشا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان تو کاندر فراقم می‌طپید</p></div>
<div class="m2"><p>چونک زنهارش رسیدم چون رمید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بدیده در فراقم گرم و سرد</p></div>
<div class="m2"><p>با خود آ از بی‌خودی و باز گرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ خانه اشتری را بی خرد</p></div>
<div class="m2"><p>رسم مهمانش به خانه می‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به خانه مرغ اشتر پا نهاد</p></div>
<div class="m2"><p>خانه ویران گشت و سقف اندر فتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانهٔ مرغست هوش و عقل ما</p></div>
<div class="m2"><p>هوش صالح طالب ناقهٔ خدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناقه چون سر کرد در آب و گلش</p></div>
<div class="m2"><p>نه گل آنجا ماند نه جان و دلش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد فضل عشق انسان را فضول</p></div>
<div class="m2"><p>زین فزون‌جویی ظلومست و جهول</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جاهلست و اندرین مشکل شکار</p></div>
<div class="m2"><p>می‌کشد خرگوش شیری در کنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی کنار اندر کشیدی شیر را</p></div>
<div class="m2"><p>گر بدانستی و دیدی شیر را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ظالمست او بر خود و بر جان خود</p></div>
<div class="m2"><p>ظلم بین کز عدلها گو می‌برد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهل او مر علمها را اوستاد</p></div>
<div class="m2"><p>ظلم او مر عدلها را شد رشاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست او بگرفت کین رفته دمش</p></div>
<div class="m2"><p>آنگهی آید که من دم بخشمش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون به من زنده شود این مرده‌تن</p></div>
<div class="m2"><p>جان من باشد که رو آرد به من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من کنم او را ازین جان محتشم</p></div>
<div class="m2"><p>جان که من بخشم ببیند بخششم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان نامحرم نبیند روی دوست</p></div>
<div class="m2"><p>جز همان جان کاصل او از کوی اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در دمم قصاب‌وار این دوست را</p></div>
<div class="m2"><p>تا هلد آن مغز نغزش پوست را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت ای جان رمیده از بلا</p></div>
<div class="m2"><p>وصل ما را در گشادیم الصلا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای خود ما بی‌خودی و مستی‌ات</p></div>
<div class="m2"><p>ای ز هست ما هماره هستی‌ات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با تو بی لب این زمان من نو بنو</p></div>
<div class="m2"><p>رازهای کهنه گویم می‌شنو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زانک آن لبها ازین دم می‌رمد</p></div>
<div class="m2"><p>بر لب جوی نهان بر می‌دمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گوش بی‌گوشی درین دم بر گشا</p></div>
<div class="m2"><p>بهر راز یفعل الله ما یشا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون صلای وصل بشنیدن گرفت</p></div>
<div class="m2"><p>اندک اندک مرده جنبیدن گرفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه کم از خاکست کز عشوهٔ صبا</p></div>
<div class="m2"><p>سبز پوشد سر بر آرد از فنا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کم ز آب نطفه نبود کز خطاب</p></div>
<div class="m2"><p>یوسفان زایند رخ چون آفتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کم ز بادی نیست شد از امر کن</p></div>
<div class="m2"><p>در رحم طاوس و مرغ خوش‌سخن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کم ز کوه سنگ نبود کز ولاد</p></div>
<div class="m2"><p>ناقه‌ای کان ناقه ناقه زاد زاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زین همه بگذر نه آن مایهٔ عدم</p></div>
<div class="m2"><p>عالمی زاد و بزاید دم بدم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر جهید و بر طپید و شاد شاد</p></div>
<div class="m2"><p>یک دو چرخی زد سجود اندر فتاد</p></div></div>