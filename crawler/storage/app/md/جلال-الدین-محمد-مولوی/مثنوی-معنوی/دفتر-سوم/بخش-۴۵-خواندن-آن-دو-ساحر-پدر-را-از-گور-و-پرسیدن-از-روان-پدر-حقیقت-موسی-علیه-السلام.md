---
title: >-
    بخش ۴۵ - خواندن آن دو ساحر پدر را از گور و پرسیدن از روان پدر حقیقت موسی علیه السلام
---
# بخش ۴۵ - خواندن آن دو ساحر پدر را از گور و پرسیدن از روان پدر حقیقت موسی علیه السلام

<div class="b" id="bn1"><div class="m1"><p>بعد از آن گفتند ای مادر بیا</p></div>
<div class="m2"><p>گور بابا کو تو ما را ره نما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردشان بر گور او بنمود راه</p></div>
<div class="m2"><p>پس سه‌روزه داشتند از بهر شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد از آن گفتند ای بابا به ما</p></div>
<div class="m2"><p>شاه پیغامی فرستاد از وجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دو مرد او را به تنگ آورده‌اند</p></div>
<div class="m2"><p>آب رویش پیش لشکر برده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست با ایشان سلاح و لشکری</p></div>
<div class="m2"><p>جز عصا و در عصا شور و شری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو جهان راستان در رفته‌ای</p></div>
<div class="m2"><p>گرچه در صورت به خاکی خفته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن اگر سحرست ما را ده خبر</p></div>
<div class="m2"><p>ور خدایی باشد ای جان پدر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم خبر ده تا که ما سجده کنیم</p></div>
<div class="m2"><p>خویشتن بر کیمیایی بر زنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناامیدانیم و اومیدی رسید</p></div>
<div class="m2"><p>راندگانیم و کرم ما را کشید</p></div></div>