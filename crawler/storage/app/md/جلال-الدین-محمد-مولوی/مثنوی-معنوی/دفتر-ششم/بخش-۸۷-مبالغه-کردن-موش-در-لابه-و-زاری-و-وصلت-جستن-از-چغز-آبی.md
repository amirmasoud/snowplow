---
title: >-
    بخش ۸۷ - مبالغه کردن موش در لابه و زاری و وصلت جستن از چغز آبی
---
# بخش ۸۷ - مبالغه کردن موش در لابه و زاری و وصلت جستن از چغز آبی

<div class="b" id="bn1"><div class="m1"><p>گفت کای یار عزیز مهرکار</p></div>
<div class="m2"><p>من ندارم بی‌رخت یک‌دم قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز نور و مکسب و تابم توی</p></div>
<div class="m2"><p>شب قرار و سلوت و خوابم توی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مروت باشد ار شادم کنی</p></div>
<div class="m2"><p>وقت و بی‌وقت از کرم یادم کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شبان‌روزی وظیفهٔ چاشتگاه</p></div>
<div class="m2"><p>راتبه کردی وصال ای نیک‌خواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بدین یک‌بار قانع نیستم</p></div>
<div class="m2"><p>در هوایت طرفه انسانیستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پانصد استسقاستم اندر جگر</p></div>
<div class="m2"><p>با هر استسقا قرین جوع البقر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌نیازی از غم من ای امیر</p></div>
<div class="m2"><p>ده زکات جاه و بنگر در فقیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این فقیر بی‌ادب نا درخورست</p></div>
<div class="m2"><p>لیک لطف عام تو زان برترست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌نجوید لطف عام تو سند</p></div>
<div class="m2"><p>آفتابی بر حدثها می‌زند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور او را زان زیانی نابده</p></div>
<div class="m2"><p>وان حدث از خشکیی هیزم شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا حدث در گلخنی شد نور یافت</p></div>
<div class="m2"><p>در در و دیوار حمامی بتافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود آلایش شد آرایش کنون</p></div>
<div class="m2"><p>چون برو بر خواند خورشید آن فسون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس هم معدهٔ زمین را گرم کرد</p></div>
<div class="m2"><p>تا زمین باقی حدثها را بخورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جزو خاکی گشت و رست از وی نبات</p></div>
<div class="m2"><p>هکذا یمحو الاله السیئات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با حدث که بترینست این کند</p></div>
<div class="m2"><p>کش نبات و نرگس و نسرین کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا به نسرین مناسک در وفا</p></div>
<div class="m2"><p>حق چه بخشد در جزا و در عطا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون خبیثان را چنین خلعت دهد</p></div>
<div class="m2"><p>طیبین را تا چه بخشد در رصد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن دهد حقشان که لا عین رات</p></div>
<div class="m2"><p>که نگنجد در زبان و در لغت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ما کییم این را بیا ای یار من</p></div>
<div class="m2"><p>روز من روشن کن از خلق حسن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>منگر اندر زشتی و مکروهیم</p></div>
<div class="m2"><p>که ز پر زهری چو مار کوهیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای که من زشت و خصالم جمله زشت</p></div>
<div class="m2"><p>چون شوم گل چون مرا او خار کشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نوبهار حسن گل ده خار را</p></div>
<div class="m2"><p>زینت طاووس ده این مار را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در کمال زشتیم من منتهی</p></div>
<div class="m2"><p>لطف تو در فضل و در فن منتهی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حاجت این منتهی زان منتهی</p></div>
<div class="m2"><p>تو بر آر ای حسرت سرو سهی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بمیرم فضل تو خواهد گریست</p></div>
<div class="m2"><p>از کرم گرچه ز حاجت او بریست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر سر گورم بسی خواهد نشست</p></div>
<div class="m2"><p>خواهد از چشم لطیفش اشک جست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نوحه خواهد کرد بر محرومیم</p></div>
<div class="m2"><p>چشم خواهد بست از مظلومیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اندکی زان لطفها اکنون بکن</p></div>
<div class="m2"><p>حلقه‌ای در گوش من کن زان سخن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنک خواهی گفت تو با خاک من</p></div>
<div class="m2"><p>برفشان بر مدرک غمناک من</p></div></div>