---
title: >-
    بخش ۱۰۱ - رجوع کردن به قصهٔ آن پای‌مرد و آن غریب وام‌دار و بازگشتن ایشان از سر گور خواجه و خواب دیدن پای‌مرد خواجه را الی آخره
---
# بخش ۱۰۱ - رجوع کردن به قصهٔ آن پای‌مرد و آن غریب وام‌دار و بازگشتن ایشان از سر گور خواجه و خواب دیدن پای‌مرد خواجه را الی آخره

<div class="b" id="bn1"><div class="m1"><p>بی‌نهایت آمد این خوش سرگذشت</p></div>
<div class="m2"><p>چون غریب از گور خواجه باز گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای مردش سوی خانهٔ خویش برد</p></div>
<div class="m2"><p>مهر صد دینار را فا او سپرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لوتش آورد و حکایت‌هاش گفت</p></div>
<div class="m2"><p>کز امید اندر دلش صد گل شکفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچ بعد العسر یسر او دیده بود</p></div>
<div class="m2"><p>با غریب از قصهٔ آن لب گشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم‌شب بگذشت و افسانه کنان</p></div>
<div class="m2"><p>خوابشان انداخت تا مرعای جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دید پامرد آن همایون خواجه را</p></div>
<div class="m2"><p>اندر آن شب خواب بر صدر سرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجه گفت ای پای‌مرد با نمک</p></div>
<div class="m2"><p>آنچ گفتی من شنیدم یک به یک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لیک پاسخ دادنم فرمان نبود</p></div>
<div class="m2"><p>بی‌اشارت لب نیارستم گشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما چو واقف گشته‌ایم از چون و چند</p></div>
<div class="m2"><p>مهر با لب‌های ما بنهاده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نگردد رازهای غیب فاش</p></div>
<div class="m2"><p>تا نگردد منهدم عیش و معاش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ندرد پردهٔ غفلت تمام</p></div>
<div class="m2"><p>تا نماند دیگ محنت نیم‌خام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما همه گوشیم کر شد نقش گوش</p></div>
<div class="m2"><p>ما همه نطقیم لیکن لب خموش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چه ما دادیم دیدیم این زمان</p></div>
<div class="m2"><p>این جهان پرده‌ست و عینست آن جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روز کشتن روز پنهان کردنست</p></div>
<div class="m2"><p>تخم در خاکی پریشان کردنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وقت بدرودن گه منجل زدن</p></div>
<div class="m2"><p>روز پاداش آمد و پیدا شدن</p></div></div>