---
title: >-
    بخش ۱۳ - حکایت آن شخص کی دزدان قوج او را بدزدیدند  و بر آن قناعت نکرد به حیله جامه‌هاش را هم دزدیدند
---
# بخش ۱۳ - حکایت آن شخص کی دزدان قوج او را بدزدیدند  و بر آن قناعت نکرد به حیله جامه‌هاش را هم دزدیدند

<div class="b" id="bn1"><div class="m1"><p>آن یکی قج داشت از پس می‌کشید</p></div>
<div class="m2"><p>دزد قج را برد حبلش را برید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چونک آگه شد دوان شد چپ و راست</p></div>
<div class="m2"><p>تا بیابد کان قج برده کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر چاهی بدید آن دزد را</p></div>
<div class="m2"><p>که فغان می‌کرد کای واویلتا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت نالان از چئی ای اوستاد</p></div>
<div class="m2"><p>گفت همیان زرم در چه فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر توانی در روی بیرون کشی</p></div>
<div class="m2"><p>خمس بدهم مر ترا با دلخوشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمس صد دینار بستانی به دست</p></div>
<div class="m2"><p>گفت او خود این بهای ده قجست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دری بر بسته شد ده در گشاد</p></div>
<div class="m2"><p>گر قجی شد حق عوض اشتر بداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جامه‌ها بر کند و اندر چاه رفت</p></div>
<div class="m2"><p>جامه‌ها را برد هم آن دزد تفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حازمی باید که ره تا ده برد</p></div>
<div class="m2"><p>حزم نبود طمع طاعون آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او یکی دزدست فتنه‌سیرتی</p></div>
<div class="m2"><p>چون خیال او را بهر دم صورتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس نداند مکر او الا خدا</p></div>
<div class="m2"><p>در خدا بگریز و وا ره زان دغا</p></div></div>