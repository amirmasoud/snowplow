---
title: >-
    بخش ۴۱ - قصهٔ درویشی کی از آن خانه هرچه می‌خواست می‌گفت نیست
---
# بخش ۴۱ - قصهٔ درویشی کی از آن خانه هرچه می‌خواست می‌گفت نیست

<div class="b" id="bn1"><div class="m1"><p>سایلی آمد به سوی خانه‌ای</p></div>
<div class="m2"><p>خشک نانه خواست یا تر نانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت صاحب‌خانه نان اینجا کجاست</p></div>
<div class="m2"><p>خیره‌ای کی این دکان نانباست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت باری اندکی پیهم بیاب</p></div>
<div class="m2"><p>گفت آخر نیست دکان قصاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت پارهٔ آرد ده ای کدخدا</p></div>
<div class="m2"><p>گفت پنداری که هست این آسیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت باری آب ده از مکرعه</p></div>
<div class="m2"><p>گفت آخر نیست جو یا مشرعه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه او درخواست از نان یا سبوس</p></div>
<div class="m2"><p>چربکی می‌گفت و می‌کردش فسوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن گدا در رفت و دامن بر کشید</p></div>
<div class="m2"><p>اندر آن خانه بحسبت خواست رید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت هی هی گفت تن زن ای دژم</p></div>
<div class="m2"><p>تا درین ویرانه خود فارغ کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون درینجا نیست وجه زیستن</p></div>
<div class="m2"><p>بر چنین خانه بباید ریستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نه‌ای بازی که گیری تو شکار</p></div>
<div class="m2"><p>دست آموز شکار شهریار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیستی طاوس با صد نقش بند</p></div>
<div class="m2"><p>که به نقشت چشمها روشن کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم نه‌ای طوطی که چون قندت دهند</p></div>
<div class="m2"><p>گوش سوی گفت شیرینت نهند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم نه‌ای بلبل که عاشق‌وار زار</p></div>
<div class="m2"><p>خوش بنالی در چمن یا لاله‌زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم نه‌ای هدهد که پیکیها کنی</p></div>
<div class="m2"><p>نه چو لک‌لک که وطن بالا کنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در چه کاری تو و بهر چت خرند</p></div>
<div class="m2"><p>تو چه مرغی و ترا با چه خورند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین دکان با مکاسان برتر آ</p></div>
<div class="m2"><p>تا دکان فضل که الله اشتری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاله‌ای که هیچ خلقش ننگرید</p></div>
<div class="m2"><p>از خلاقت آن کریم آن را خرید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هیچ قلبی پیش او مردود نیست</p></div>
<div class="m2"><p>زانک قصدش از خریدن سود نیست</p></div></div>