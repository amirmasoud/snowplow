---
title: >-
    بخش ۱۲۲ - بیان این خبر کی الکذب ریبة والصدق طمانینة
---
# بخش ۱۲۲ - بیان این خبر کی الکذب ریبة والصدق طمانینة

<div class="b" id="bn1"><div class="m1"><p>قصهٔ آن خواب و گنج زر بگفت</p></div>
<div class="m2"><p>پس ز صدق او دل آن کس شکفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی صدقش آمد از سوگند او</p></div>
<div class="m2"><p>سوز او پیدا شد و اسپند او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بیارامد به گفتار صواب</p></div>
<div class="m2"><p>آنچنان که تشنه آرامد به آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز دل محجوب کو را علتیست</p></div>
<div class="m2"><p>از نبیش تا غبی تمییز نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ورنه آن پیغام کز موضع بود</p></div>
<div class="m2"><p>بر زند بر مه شکافیده شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه شکافد وان دل محجوب نی</p></div>
<div class="m2"><p>زانک مردودست او محبوب نی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمه شد چشم عسس ز اشک مبل</p></div>
<div class="m2"><p>نی ز گفت خشک بل از بوی دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک سخن از دوزخ آید سوی لب</p></div>
<div class="m2"><p>یک سخن از شهر جان در کوی لب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحر جان‌افزا و بحر پر حرج</p></div>
<div class="m2"><p>در میان هر دو بحر این لب مرج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون یپنلو در میان شهرها</p></div>
<div class="m2"><p>از نواحی آید آن‌جا بهرها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کالهٔ معیوب قلب کیسه‌بر</p></div>
<div class="m2"><p>کالهٔ پر سود مستشرف چو در</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین یپنلو هر که بازرگان‌ترست</p></div>
<div class="m2"><p>بر سره و بر قلب‌ها دیده‌ورست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد یپنلو مر ورا دار الرباح</p></div>
<div class="m2"><p>وآن گر را از عمی دار الجناح</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر یکی ز اجزای عالم یک به یک</p></div>
<div class="m2"><p>بر غبی بندست و بر استاد فک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر یکی قندست و بر دیگر چو زهر</p></div>
<div class="m2"><p>بر یکی لطفست و بر دیگر چو قهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر جمادی با نبی افسانه‌گو</p></div>
<div class="m2"><p>کعبه با حاجی گواه و نطق‌خو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر مصلی مسجد آمد هم گواه</p></div>
<div class="m2"><p>کو همی‌آمد به من از دور راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با خلیل آتش گل و ریحان و ورد</p></div>
<div class="m2"><p>باز بر نمرودیان مرگست و درد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بارها گفتیم این را ای حسن</p></div>
<div class="m2"><p>می‌نگردم از بیانش سیر من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بارها خوردی تو نان دفع ذبول</p></div>
<div class="m2"><p>این همان نانست چون نبوی ملول</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در تو جوعی می‌رسد تو ز اعتلال</p></div>
<div class="m2"><p>که همی‌سوزد ازو تخمه و ملال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرکه را درد مجاعت نقد شد</p></div>
<div class="m2"><p>نو شدن با جزو جزوش عقد شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لذت از جوعست نه از نقل نو</p></div>
<div class="m2"><p>با مجاعت از شکر به نان جو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس ز بی‌جوعیست وز تخمهٔ تمام</p></div>
<div class="m2"><p>آن ملالت نه ز تکرار کلام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون ز دکان و مکاس و قیل و قال</p></div>
<div class="m2"><p>در فریب مردمت ناید ملال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون ز غیبت و اکل لحم مردمان</p></div>
<div class="m2"><p>شصت سالت سیریی نامد از آن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عشوه‌ها در صید شلهٔ کفته تو</p></div>
<div class="m2"><p>بی ملولی بارها خوش گفته تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بار آخر گوییش سوزان و چست</p></div>
<div class="m2"><p>گرم‌تر صد بار از بار نخست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>درد داروی کهن را نو کند</p></div>
<div class="m2"><p>درد هر شاخ ملولی خو کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کیمیای نو کننده دردهاست</p></div>
<div class="m2"><p>کو ملولی آن طرف که درد خاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هین مزن تو از ملولی آه سرد</p></div>
<div class="m2"><p>درد جو و درد جو و درد درد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خادع دردند درمان‌های ژاژ</p></div>
<div class="m2"><p>ره‌زنند و زرستانان رسم باژ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آب شوری نیست در مان عطش</p></div>
<div class="m2"><p>وقت خوردن گر نماید سرد و خوش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لیک خادع گشته و مانع شد ز جست</p></div>
<div class="m2"><p>ز آب شیرینی کزو صد سبزه رست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هم‌چنین هر زر قلبی مانعست</p></div>
<div class="m2"><p>از شناس زر خوش هرجا که هست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پا و پرت را به تزویری برید</p></div>
<div class="m2"><p>که مراد تو منم گیر ای مرید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفت دردت چینم او خود درد بود</p></div>
<div class="m2"><p>مات بود ار چه به ظاهر برد بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رو ز درمان دروغین می‌گریز</p></div>
<div class="m2"><p>تا شود دردت مصیب و مشک‌بیز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفت نه دزدی تو و نه فاسقی</p></div>
<div class="m2"><p>مرد نیکی لیک گول و احمقی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر خیال و خواب چندین ره کنی</p></div>
<div class="m2"><p>نیست عقلت را تسوی روشنی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بارها من خواب دیدم مستمر</p></div>
<div class="m2"><p>که به بغدادست گنجی مستتر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در فلان سوی و فلان کویی دفین</p></div>
<div class="m2"><p>بود آن خود نام کوی این حزین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هست در خانهٔ فلانی رو بجو</p></div>
<div class="m2"><p>نام خانه و نام او گفت آن عدو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دیده‌ام خود بارها این خواب من</p></div>
<div class="m2"><p>که به بغدادست گنجی در وطن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هیچ من از جا نرفتم زین خیال</p></div>
<div class="m2"><p>تو به یک خوابی بیایی بی‌ملال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خواب احمق لایق عقل ویست</p></div>
<div class="m2"><p>هم‌چو او بی‌قیمتست و لاشیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خواب زن کمتر ز خواب مرد دان</p></div>
<div class="m2"><p>از پی نقصان عقل و ضعف جان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خواب ناقص‌عقل و گول آید کساد</p></div>
<div class="m2"><p>پس ز بی‌عقلی چه باشد خواب باد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفت با خود گنج در خانهٔ منست</p></div>
<div class="m2"><p>پس مرا آن‌جا چه فقر و شیونست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بر سر گنج از گدایی مرده‌ام</p></div>
<div class="m2"><p>زانک اندر غفلت و در پرده‌ام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زین بشارت مست شد دردش نماند</p></div>
<div class="m2"><p>صد هزار الحمد بی لب او بخواند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفت بد موقوف این لت لوت من</p></div>
<div class="m2"><p>آب حیوان بود در حانوت من</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رو که بر لوت شگرفی بر زدم</p></div>
<div class="m2"><p>کوری آن وهم که مفلس بدم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خواه احمق‌دان مرا خواهی فرو</p></div>
<div class="m2"><p>آن من شد هرچه می‌خواهی بگو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>من مراد خویش دیدم بی‌گمان</p></div>
<div class="m2"><p>هرچه خواهی گو مرا ای بددهان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو مرا پر درد گو ای محتشم</p></div>
<div class="m2"><p>پیش تو پر درد و پیش خود خوشم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وای اگر بر عکس بودی این مطار</p></div>
<div class="m2"><p>پیش تو گلزار و پیش خویش راز</p></div></div>