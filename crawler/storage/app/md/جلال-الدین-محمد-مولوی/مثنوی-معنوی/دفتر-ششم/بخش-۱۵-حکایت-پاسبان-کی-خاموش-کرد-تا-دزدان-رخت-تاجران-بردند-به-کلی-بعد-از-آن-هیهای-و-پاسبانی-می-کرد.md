---
title: >-
    بخش ۱۵ - حکایت پاسبان کی خاموش کرد تا دزدان رخت تاجران بردند به کلی بعد از آن هیهای و پاسبانی می‌کرد
---
# بخش ۱۵ - حکایت پاسبان کی خاموش کرد تا دزدان رخت تاجران بردند به کلی بعد از آن هیهای و پاسبانی می‌کرد

<div class="b" id="bn1"><div class="m1"><p>پاسبانی خفت و دزد اسباب برد</p></div>
<div class="m2"><p>رختها را زیر هر خاکی فشرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز شد بیدار شد آن کاروان</p></div>
<div class="m2"><p>دید رفته رخت و سیم و اشتران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس بدو گفتند ای حارس بگو</p></div>
<div class="m2"><p>که چه شد این رخت و این اسباب کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت دزدان آمدند اندر نقاب</p></div>
<div class="m2"><p>رختها بردند از پیشم شتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قوم گفتندش که ای چو تل ریگ</p></div>
<div class="m2"><p>پس چه می‌کردی کیی ای مردریگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت من یک کس بدم ایشان گروه</p></div>
<div class="m2"><p>با سلاح و با شجاعت با شکوه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت اگر در جنگ کم بودت امید</p></div>
<div class="m2"><p>نعره‌ای زن کای کریمان برجهید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت آن دم کارد بنمودند و تیغ</p></div>
<div class="m2"><p>که خمش ورنه کشیمت بی‌دریغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن زمان از ترس بستم من دهان</p></div>
<div class="m2"><p>این زمان هیهای و فریاد و فغان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن زمان بست آن دمم که دم زنم</p></div>
<div class="m2"><p>این زمان چندانک خواهی هی کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونک عمرت برد دیو فاضحه</p></div>
<div class="m2"><p>بی‌نمک باشد اعوذ و فاتحه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه باشد بی‌نمک اکنون حنین</p></div>
<div class="m2"><p>هست غفلت بی‌نمک‌تر زان یقین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم‌چنین هم بی‌نمک می‌نال نیز</p></div>
<div class="m2"><p>که ذلیلان را نظر کن ای عزیز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قادری بی‌گاه باشد یا به گاه</p></div>
<div class="m2"><p>از تو چیزی فوت کی شد ای اله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاه لا تاسوا علی ما فاتکم</p></div>
<div class="m2"><p>کی شود از قدرتش مطلوب گم</p></div></div>