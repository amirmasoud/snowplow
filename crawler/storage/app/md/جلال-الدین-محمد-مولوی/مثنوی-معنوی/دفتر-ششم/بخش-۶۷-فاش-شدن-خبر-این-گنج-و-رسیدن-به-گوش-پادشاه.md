---
title: >-
    بخش ۶۷ - فاش شدن خبر این گنج و رسیدن به گوش پادشاه
---
# بخش ۶۷ - فاش شدن خبر این گنج و رسیدن به گوش پادشاه

<div class="b" id="bn1"><div class="m1"><p>پس خبر کردند سلطان را ازین</p></div>
<div class="m2"><p>آن گروهی که بدند اندر کمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرضه کردند آن سخن را زیردست</p></div>
<div class="m2"><p>که فلانی گنج‌نامه یافتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شنید این شخص کین با شه رسید</p></div>
<div class="m2"><p>جز که تسلیم و رضا چاره ندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش از آنک اشکنجه بیند زان قباد</p></div>
<div class="m2"><p>رقعه را آن شخص پیش او نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت تا این رقعه را یابیده‌ام</p></div>
<div class="m2"><p>گنج نه و رنج بی‌حد دیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود نشد یک حبه از گنج آشکار</p></div>
<div class="m2"><p>لیک پیچیدم بسی من هم‌چو مار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدت ماهی چنینم تلخ‌کام</p></div>
<div class="m2"><p>که زیان و سود این بر من حرام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوک بختت بر کند زین کان غطا</p></div>
<div class="m2"><p>ای شه پیروزجنگ و دزگشا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدت شش ماه و افزون پادشاه</p></div>
<div class="m2"><p>تیر می‌انداخت و برمی‌کند چاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکجا سخته کمانی بود چست</p></div>
<div class="m2"><p>تیر داد انداخت و هر سو گنج جست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیر تشویش و غم و طامات نی</p></div>
<div class="m2"><p>هم‌چو عنقا نام فاش و ذات نی</p></div></div>