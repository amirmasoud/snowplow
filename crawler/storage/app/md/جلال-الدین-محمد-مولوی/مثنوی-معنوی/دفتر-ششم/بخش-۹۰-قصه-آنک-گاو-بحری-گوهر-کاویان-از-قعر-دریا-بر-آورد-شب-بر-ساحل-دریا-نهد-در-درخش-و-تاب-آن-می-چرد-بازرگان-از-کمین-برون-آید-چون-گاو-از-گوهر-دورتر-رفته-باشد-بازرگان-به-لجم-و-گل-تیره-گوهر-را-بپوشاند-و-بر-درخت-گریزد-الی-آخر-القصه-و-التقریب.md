---
title: >-
    بخش ۹۰ - قصهٔ آنک گاو بحری گوهر کاویان از قعر دریا بر آورد شب بر ساحل دریا نهد در درخش و تاب آن می‌چرد بازرگان از کمین برون آید چون گاو از گوهر دورتر رفته باشد بازرگان به لجم و گل تیره گوهر را بپوشاند و بر درخت گریزد الی آخر القصه و التقریب
---
# بخش ۹۰ - قصهٔ آنک گاو بحری گوهر کاویان از قعر دریا بر آورد شب بر ساحل دریا نهد در درخش و تاب آن می‌چرد بازرگان از کمین برون آید چون گاو از گوهر دورتر رفته باشد بازرگان به لجم و گل تیره گوهر را بپوشاند و بر درخت گریزد الی آخر القصه و التقریب

<div class="b" id="bn1"><div class="m1"><p>گاو آبی گوهر از بحر آورد</p></div>
<div class="m2"><p>بنهد اندر مرج و گردش می‌چرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شعاع نور گوهر گاو آب</p></div>
<div class="m2"><p>می‌چرد از سنبل و سوسن شتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان فکندهٔ گاو آبی عنبرست</p></div>
<div class="m2"><p>که غذااش نرگس و نیلوفرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه باشد قوت او نور جلال</p></div>
<div class="m2"><p>چون نزاید از لبش سحر حلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه چون زنبور وحیستش نفل</p></div>
<div class="m2"><p>چون نباشد خانهٔ او پر عسل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌چرد در نور گوهر آن بقر</p></div>
<div class="m2"><p>ناگهان گردد ز گوهر دورتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاجری بر در نهد لجم سیاه</p></div>
<div class="m2"><p>تا شود تاریک مرج و سبزه‌گاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس گریزد مرد تاجر بر درخت</p></div>
<div class="m2"><p>گاوجویان مرد را با شاخ سخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیست بار آن گاو تازد گرد مرج</p></div>
<div class="m2"><p>تا کند آن خصم را در شاخ درج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ازو نومید گردد گاو نر</p></div>
<div class="m2"><p>آید آنجا که نهاده بد گهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لجم بیند فوق در شاه‌وار</p></div>
<div class="m2"><p>پس ز طین بگریزد او ابلیس‌وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کان بلیس از متن طین کور و کرست</p></div>
<div class="m2"><p>گاو کی داند که در گل گوهرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اهبطوا افکند جان را در حضیض</p></div>
<div class="m2"><p>از نمازش کرد محروم این محیض</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای رفیقان زین مقیل و زان مقال</p></div>
<div class="m2"><p>اتقوا ان الهوی حیض الرجال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اهبطوا افکند جان را در بدن</p></div>
<div class="m2"><p>تا به گل پنهان بود در عدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تاجرش داند ولیکن گاو نی</p></div>
<div class="m2"><p>اهل دل دانند و هر گل‌کاو نی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر گلی که اندر دل او گوهریست</p></div>
<div class="m2"><p>گوهرش غماز طین دیگریست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وان گلی کز رش حق نوری نیافت</p></div>
<div class="m2"><p>صحبت گلهای پر در بر نتافت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این سخن پایان ندارد موش ما</p></div>
<div class="m2"><p>هست بر لبهای جو بر گوش ما</p></div></div>