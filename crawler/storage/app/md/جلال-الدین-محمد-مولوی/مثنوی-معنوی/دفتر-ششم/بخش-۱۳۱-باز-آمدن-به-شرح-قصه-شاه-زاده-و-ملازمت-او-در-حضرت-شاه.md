---
title: >-
    بخش ۱۳۱ - باز آمدن به شرح قصهٔ شاه‌زاده و ملازمت او در حضرت شاه
---
# بخش ۱۳۱ - باز آمدن به شرح قصهٔ شاه‌زاده و ملازمت او در حضرت شاه

<div class="b" id="bn1"><div class="m1"><p>شاه‌زاده پیش شه حیران این</p></div>
<div class="m2"><p>هفت گردون دیده در یک مشت طین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ ممکن نه ببحثی لب گشود</p></div>
<div class="m2"><p>لیک جان با جان دمی خامش نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمده در خاطرش کین بس خفیست</p></div>
<div class="m2"><p>این همه معنیست پس صورت ز چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورتی از صورتت بیزار کن</p></div>
<div class="m2"><p>خفته‌ای هر خفته را بیدار کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن کلامت می‌رهاند از کلام</p></div>
<div class="m2"><p>وان سقامت می‌جهاند از سقام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس سقام عشق جان صحتست</p></div>
<div class="m2"><p>رنجهااش حسرت هر راحتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای تن اکنون دست خود زین جان بشو</p></div>
<div class="m2"><p>ور نمی‌شویی جز این جانی بجو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاصل آن شه نیک او را می‌نواخت</p></div>
<div class="m2"><p>او از آن خورشید چون مه می‌گداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن گداز عاشقان باشد نمو</p></div>
<div class="m2"><p>هم‌چو مه اندر گدازش تازه‌رو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمله رنجوران دوا دارند امید</p></div>
<div class="m2"><p>نالد این رنجور کم افزون کنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش‌تر از این سم ندیدم شربتی</p></div>
<div class="m2"><p>زین مرض خوش‌تر نباشد صحتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین گنه بهتر نباشد طاعتی</p></div>
<div class="m2"><p>سالها نسبت بدین دم ساعتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مدتی بد پیش این شه زین نسق</p></div>
<div class="m2"><p>دل کباب و جان نهاده بر طبق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت شه از هر کسی یک سر برید</p></div>
<div class="m2"><p>من ز شه هر لحظه قربانم جدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من فقیرم از زر از سر محتشم</p></div>
<div class="m2"><p>صد هزاران سر خلف دارد سرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با دو پا در عشق نتوان تاختن</p></div>
<div class="m2"><p>با یکی سر عشق نتوان باختن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر کسی را خود دو پا و یک‌سرست</p></div>
<div class="m2"><p>با هزاران پا و سر تن نادرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین سبب هنگامه‌ها شد کل هدر</p></div>
<div class="m2"><p>هست این هنگامه هر دم گرم‌تر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>معدن گرمیست اندر لامکان</p></div>
<div class="m2"><p>هفت دوزخ از شرارش یک دخان</p></div></div>