---
title: >-
    بخش ۵۲ - باز سؤال کردن صوفی از آن قاضی
---
# بخش ۵۲ - باز سؤال کردن صوفی از آن قاضی

<div class="b" id="bn1"><div class="m1"><p>گفت صوفی که چه بودی کین جهان</p></div>
<div class="m2"><p>ابروی رحمت گشادی جاودان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دمی شوری نیاوردی به پیش</p></div>
<div class="m2"><p>بر نیاوردی ز تلوینهاش نیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب ندزدیدی چراغ روز را</p></div>
<div class="m2"><p>دی نبردی باغ عیش آموز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام صحت را نبودی سنگ تب</p></div>
<div class="m2"><p>آمنی با خوف ناوردی کرب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود چه کم گشتی ز جود و رحمتش</p></div>
<div class="m2"><p>گر نبودی خرخشه در نعمتش</p></div></div>