---
title: >-
    بخش ۲۱ - حکایت آن مطرب کی در بزم امیر ترک این غزل آغاز کرد گلی یا سوسنی یا سرو یا ماهی نمی‌دانم ازین آشفتهٔ بی‌دل چه می‌خواهی نمی‌دانم  و بانگ بر زدن ترک کی آن بگو کی می‌دانی و جواب مطرب امیر را
---
# بخش ۲۱ - حکایت آن مطرب کی در بزم امیر ترک این غزل آغاز کرد گلی یا سوسنی یا سرو یا ماهی نمی‌دانم ازین آشفتهٔ بی‌دل چه می‌خواهی نمی‌دانم  و بانگ بر زدن ترک کی آن بگو کی می‌دانی و جواب مطرب امیر را

<div class="b" id="bn1"><div class="m1"><p>مطرب آغازید پیش ترک مست</p></div>
<div class="m2"><p>در حجاب نغمه اسرار الست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ندانم که تو ماهی یا وثن</p></div>
<div class="m2"><p>من ندانم تا چه می‌خواهی ز من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌ندانم که چه خدمت آرمت</p></div>
<div class="m2"><p>تن زنم یا در عبارت آرمت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این عجب که نیستی از من جدا</p></div>
<div class="m2"><p>می‌ندانم من کجاام تو کجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌ندانم که مرا چون می‌کشی</p></div>
<div class="m2"><p>گاه در بر گاه در خون می‌کشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم‌چنین لب در ندانم باز کرد</p></div>
<div class="m2"><p>می‌ندانم می‌ندانم ساز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ز حد شد می‌ندانم از شگفت</p></div>
<div class="m2"><p>ترک ما را زین حراره دل گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برجهید آن ترک و دبوسی کشید</p></div>
<div class="m2"><p>تا علیها بر سر مطرب رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرز را بگرفت سرهنگی بدست</p></div>
<div class="m2"><p>گفت نه مطرب کشی این دم بدست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت این تکرار بی حد و مرش</p></div>
<div class="m2"><p>کوفت طبعم را بکوبم من سرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قلتبانا می‌ندانی گه مخور</p></div>
<div class="m2"><p>ور همی‌دانی بزن مقصود بر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن بگو ای گیج که می‌دانیش</p></div>
<div class="m2"><p>می‌ندانم می‌ندانم در مکش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من بپرسم کز کجایی هی مری</p></div>
<div class="m2"><p>تو بگویی نه ز بلخ و نه از هری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه ز بغداد و نه موصل نه طراز</p></div>
<div class="m2"><p>در کشی در نی و نی راه دراز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود بگو من از کجاام باز ره</p></div>
<div class="m2"><p>هست تنقیح مناط اینجا بله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا بپرسیدم چه خوردی ناشتاب</p></div>
<div class="m2"><p>تو بگویی نه شراب و نه کباب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه قدید و نه ثرید و نه عدس</p></div>
<div class="m2"><p>آنچ خوردی آن بگو تنها و بس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این سخن‌خایی دراز از بهر چیست</p></div>
<div class="m2"><p>گفت مطرب زانک مقصودم خفیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می‌رمد اثبات پیش از نفی تو</p></div>
<div class="m2"><p>نفی کردم تا بری ز اثبات بو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در نوا آرم بنفی این ساز را</p></div>
<div class="m2"><p>چون بمیری مرگ گوید راز را</p></div></div>