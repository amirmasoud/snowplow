---
title: >-
    بخش ۶۵ - قصهٔ آن گنج‌نامه کی پهلوی قبه‌ای روی به قبله کن و تیر در کمان نه بینداز آنجا کی افتد گنجست
---
# بخش ۶۵ - قصهٔ آن گنج‌نامه کی پهلوی قبه‌ای روی به قبله کن و تیر در کمان نه بینداز آنجا کی افتد گنجست

<div class="b" id="bn1"><div class="m1"><p>دید در خواب او شبی و خواب کو</p></div>
<div class="m2"><p>واقعهٔ بی‌خواب صوفی‌راست خو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هاتفی گفتش کای دیده تعب</p></div>
<div class="m2"><p>رقعه‌ای در مشق وراقان طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفیه زان وراق کت همسایه است</p></div>
<div class="m2"><p>سوی کاغذپاره‌هاش آور تو دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقعه‌ای شکلش چنین رنگش چنین</p></div>
<div class="m2"><p>بس بخوان آن را به خلوت ای حزین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بدزدی آن ز وراق ای پسر</p></div>
<div class="m2"><p>پس برون رو ز انبهی و شور و شر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بخوان آن را به خود در خلوتی</p></div>
<div class="m2"><p>هین مجو در خواندن آن شرکتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور شود آن فاش هم غمگین مشو</p></div>
<div class="m2"><p>که نیابد غیر تو زان نیم جو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور کشد آن دیر هان زنهار تو</p></div>
<div class="m2"><p>ورد خود کن دم به دم لاتقنطوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این بگفت و دست خود آن مژده‌ور</p></div>
<div class="m2"><p>بر دل او زد که رو زحمت ببر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون به خویش آمد ز غیبت آن جوان</p></div>
<div class="m2"><p>می‌نگنجید از فرح اندر جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهرهٔ او بر دریدی از قلق</p></div>
<div class="m2"><p>گر نبودی رفق و حفظ و لطف حق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک فرح آن کز پس شصد حجاب</p></div>
<div class="m2"><p>گوش او بشنید از حضرت جواب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از حجب چون حس سمعش در گذشت</p></div>
<div class="m2"><p>شد سرافراز و ز گردون بر گذشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که بود کان حس چشمش ز اعتبار</p></div>
<div class="m2"><p>زان حجاب غیب هم یابد گذار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون گذاره شد حواسش از حجاب</p></div>
<div class="m2"><p>پس پیاپی گرددش دید و خطاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جانب دکان وراق آمد او</p></div>
<div class="m2"><p>دست می‌برد او به مشقش سو به سو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش چشمش آمد آن مکتوب زود</p></div>
<div class="m2"><p>با علاماتی که هاتف گفته بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در بغل زد گفت خواجه خیر باد</p></div>
<div class="m2"><p>این زمان وا می‌رسم ای اوستاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رفت کنج خلوتی و آن را بخواند</p></div>
<div class="m2"><p>وز تحیر واله و حیران بماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که بدین سان گنج‌نامهٔ بی‌بها</p></div>
<div class="m2"><p>چون فتاده ماند اندر مشقها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باز اندر خاطرش این فکر جست</p></div>
<div class="m2"><p>کز پی هر چیز یزدان حافظست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کی گذارد حافظ اندر اکتناف</p></div>
<div class="m2"><p>که کسی چیزی رباید از گزاف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بیابان پر شود زر و نقود</p></div>
<div class="m2"><p>بی رضای حق جوی نتوان ربود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ور بخوانی صد صحف بی سکته‌ای</p></div>
<div class="m2"><p>بی قدر یادت نماند نکته‌ای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور کنی خدمت نخوانی یک کتاب</p></div>
<div class="m2"><p>علمهای نادره یابی ز جیب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد ز جیب آن کف موسی ضو فشان</p></div>
<div class="m2"><p>کان فزون آمد ز ماه آسمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کانک می‌جستی ز چرخ با نهیب</p></div>
<div class="m2"><p>سر بر آوردستت ای موسی ز جیب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا بدانی که آسمانهای سمی</p></div>
<div class="m2"><p>هست عکس مدرکات آدمی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نی که اول دست برد آن مجید</p></div>
<div class="m2"><p>از دو عالم پیشتر عقل آفرید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این سخن پیدا و پنهانست بس</p></div>
<div class="m2"><p>که نباشد محرم عنقا مگس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باز سوی قصه باز آ ای پسر</p></div>
<div class="m2"><p>قصهٔ گنج و فقیر آور به سر</p></div></div>