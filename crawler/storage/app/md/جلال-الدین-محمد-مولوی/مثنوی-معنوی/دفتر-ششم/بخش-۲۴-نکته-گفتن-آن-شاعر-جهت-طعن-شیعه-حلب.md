---
title: >-
    بخش ۲۴ - نکته گفتن آن شاعر جهت طعن شیعه حلب
---
# بخش ۲۴ - نکته گفتن آن شاعر جهت طعن شیعه حلب

<div class="b" id="bn1"><div class="m1"><p>گفت آری لیک کو دور یزید</p></div>
<div class="m2"><p>کی بدست این غم چه دیر اینجا رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم کوران آن خسارت را بدید</p></div>
<div class="m2"><p>گوش کران آن حکایت را شنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفته بودستید تا اکنون شما</p></div>
<div class="m2"><p>که کنون جامه دریدیت از عزا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس عزا بر خود کنید ای خفتگان</p></div>
<div class="m2"><p>زانک بد مرگیست این خواب گران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روح سلطانی ز زندانی بجست</p></div>
<div class="m2"><p>جامه چون دریم ، چون خاییم دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونک ایشان خسرو دین بوده‌اند</p></div>
<div class="m2"><p>وقت شادی شد چو بشکستند بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی شادروان دولت تاختند</p></div>
<div class="m2"><p>کنده و زنجیر را انداختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز ملکست و گش و شاهنشهی</p></div>
<div class="m2"><p>گر تو یک ذره ازیشان آگهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور نه‌ای آگه برو بر خود گری</p></div>
<div class="m2"><p>زانک در انکار نقل و محشری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دل و دین خرابت نوحه کن</p></div>
<div class="m2"><p>که نمی‌بیند جز این خاک کهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور همی‌بیند چرا نبود دلیر</p></div>
<div class="m2"><p>پشتدار و جانسپار و چشم‌سیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در رخت کو از می دین فرخی</p></div>
<div class="m2"><p>گر بدیدی بحر کو کف سخی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنک جو دید آب را نکند دریغ</p></div>
<div class="m2"><p>خاصه آن کو دید آن دریا و میغ</p></div></div>