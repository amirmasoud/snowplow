---
title: >-
    بخش ۷۶ - معجزهٔ هود علیه‌السلام در تخلص مؤمنان امت به وقت نزول باد
---
# بخش ۷۶ - معجزهٔ هود علیه‌السلام در تخلص مؤمنان امت به وقت نزول باد

<div class="b" id="bn1"><div class="m1"><p>مؤمنان از دست باد ضایره</p></div>
<div class="m2"><p>جمله بنشستند اندر دایره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد طوفان بود و کشتی لطف هو</p></div>
<div class="m2"><p>بس چنین کشتی و طوفان دارد او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاهی را خدا کشتی کند</p></div>
<div class="m2"><p>تا به حرص خویش بر صفها زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصد شه آن نه که خلق آمن شوند</p></div>
<div class="m2"><p>قصدش آنک ملک گردد پای‌بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن خراسی می‌دود قصدش خلاص</p></div>
<div class="m2"><p>تا بیابد او ز زخم آن دم مناص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصد او آن نه که آبی بر کشد</p></div>
<div class="m2"><p>یاکه کنجد را بدان روغن کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاو بشتابد ز بیم زخم سخت</p></div>
<div class="m2"><p>نه برای بردن گردون و رخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لیک دادش حق چنین خوف وجع</p></div>
<div class="m2"><p>تا مصالح حاصل آید در تبع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم‌چنان هر کاسبی اندر دکان</p></div>
<div class="m2"><p>بهر خود کوشد نه اصلاح جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر یکی بر درد جوید مرهمی</p></div>
<div class="m2"><p>در تبع قایم شده زین عالمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حق ستون این جهان از ترس ساخت</p></div>
<div class="m2"><p>هر یکی از ترس جان در کار باخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حمد ایزد را که ترسی را چنین</p></div>
<div class="m2"><p>کرد او معمار و اصلاح زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این همه ترسنده‌اند از نیک و بد</p></div>
<div class="m2"><p>هیچ ترسنده نترسد خود ز خود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس حقیقت بر همه حاکم کسیست</p></div>
<div class="m2"><p>که قریبست او اگر محسوس نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست او محسوس اندر مکمنی</p></div>
<div class="m2"><p>لیک محسوس حس این خانه نی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن حسی که حق بر آن حس مظهرست</p></div>
<div class="m2"><p>نیست حس این جهان آن دیگرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حس حیوان گر بدیدی آن صور</p></div>
<div class="m2"><p>بایزید وقت بودی گاو و خر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنک تن را مظهر هر روح کرد</p></div>
<div class="m2"><p>وآنک کشتی را براق نوح کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بخواهد عین کشتی را به خو</p></div>
<div class="m2"><p>او کند طوفان تو ای نورجو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر دمت طوفان و کشتی ای مقل</p></div>
<div class="m2"><p>با غم و شادیت کرد او متصل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر نبینی کشتی و دریا به پیش</p></div>
<div class="m2"><p>لرزها بین در همه اجزای خویش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون نبیند اصل ترسش را عیون</p></div>
<div class="m2"><p>ترس دارد از خیال گونه‌گون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مشت بر اعمی زند یک جلف مست</p></div>
<div class="m2"><p>کور پندارد لگدزن اشترست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زانک آن دم بانگ اشتر می‌شنید</p></div>
<div class="m2"><p>کور را گوشست آیینه نه دید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باز گوید کور نه این سنگ بود</p></div>
<div class="m2"><p>یا مگر از قبهٔ پر طنگ بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این نبود و او نبود و آن نبود</p></div>
<div class="m2"><p>آنک او ترس آفرید اینها نمود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترس و لرزه باشد از غیری یقین</p></div>
<div class="m2"><p>هیچ کس از خود نترسد ای حزین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن حکیمک وهم خواند ترس را</p></div>
<div class="m2"><p>فهم کژ کردست او این درس را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هیچ وهمی بی‌حقیقت کی بود</p></div>
<div class="m2"><p>هیچ قلبی بی‌صحیحی کی رود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کی دروغی قیمت آرد بی ز راست</p></div>
<div class="m2"><p>در دو عالم هر دروغ از راست خاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>راست را دید او رواجی و فروغ</p></div>
<div class="m2"><p>بر امید آن روان کرد او دروغ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای دروغی که ز صدقت این نواست</p></div>
<div class="m2"><p>شکر نعمت گو مکن انکار راست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از مفلسف گویم و سودای او</p></div>
<div class="m2"><p>یا ز کشتیها و دریاهای او</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بل ز کشتیهاش کان پند دلست</p></div>
<div class="m2"><p>گویم از کل جزو در کل داخلست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر ولی را نوح و کشتیبان شناس</p></div>
<div class="m2"><p>صحبت این خلق را طوفان شناس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کم گریز از شیر و اژدرهای نر</p></div>
<div class="m2"><p>ز آشنایان و ز خویشان کن حذر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در تلاقی روزگارت می‌برند</p></div>
<div class="m2"><p>یادهاشان غایبی‌ات می‌چرند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون خر تشنه خیال هر یکی</p></div>
<div class="m2"><p>از قف تن فکر را شربت‌مکی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نشف کرد از تو خیال آن وشات</p></div>
<div class="m2"><p>شبنمی که داری از بحر الحیات</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس نشان نشف آب اندر غصون</p></div>
<div class="m2"><p>آن بود کان می‌نجنبد در رکون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عضو حر شاخ تر و تازه بود</p></div>
<div class="m2"><p>می‌کشی هر سو کشیده می‌شود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر سبد خواهی توانی کردنش</p></div>
<div class="m2"><p>هم توانی کرد چنبر گردنش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون شد آن ناشف ز نشف بیخ خود</p></div>
<div class="m2"><p>ناید آن سویی که امرش می‌کشد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس بخوان قاموا کسالی از نبی</p></div>
<div class="m2"><p>چون نیابد شاخ از بیخش طبی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آتشین است این نشان کوته کنم</p></div>
<div class="m2"><p>بر فقیر و گنج و احوالش زنم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آتشی دیدی که سوزد هر نهال</p></div>
<div class="m2"><p>آتش جان بین کزو سوزد خیال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه خیال و نه حقیقت را امان</p></div>
<div class="m2"><p>زین چنین آتش که شعله زد ز جان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خصم هر شیر آمد و هر روبه او</p></div>
<div class="m2"><p>کل شیء هالک الا وجهه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در وجوه وجه او رو خرج شو</p></div>
<div class="m2"><p>چون الف در بسم در رو درج شو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آن الف در بسم پنهان کرد ایست</p></div>
<div class="m2"><p>هست او در بسم و هم در بسم نیست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هم‌چنین جملهٔ حروف گشته مات</p></div>
<div class="m2"><p>وقت حذف حرف از بهر صلات</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از صله‌ست و بی و سین زو وصل یافت</p></div>
<div class="m2"><p>وصل بی و سین الف را بر نتافت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چونک حرفی برنتابد این وصال</p></div>
<div class="m2"><p>واجب آید که کنم کوته مقال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون یکی حرفی فراق سین و بیست</p></div>
<div class="m2"><p>خامشی اینجا مهمتر واجبیست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون الف از خود فنا شد مکتنف</p></div>
<div class="m2"><p>بی و سین بی او همی‌گویند الف</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ما رمیت اذ رمیت بی ویست</p></div>
<div class="m2"><p>هم‌چنین قال الله از صمتش بجست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا بود دارو ندارد او عمل</p></div>
<div class="m2"><p>چونک شد فانی کند دفع علل</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر شود بیشه قلم دریا مداد</p></div>
<div class="m2"><p>مثنوی را نیست پایانی امید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چارچوب خشت‌زن تا خاک هست</p></div>
<div class="m2"><p>می‌دهد تقطیع شعرش نیز دست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون نماند خاک و بودش جف کند</p></div>
<div class="m2"><p>خاک سازد بحر او چون کف کند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون نماند بیشه و سر در کشد</p></div>
<div class="m2"><p>بیشه‌ها از عین دریا سر کشد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بهر این گفت آن خداوند فرج</p></div>
<div class="m2"><p>حدثوا عن بحرنا اذ لا حرج</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>باز گرد از بحر و رو در خشک نه</p></div>
<div class="m2"><p>هم ز لعبت گو که کودک‌راست به</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا ز لعبت اندک اندک در صبا</p></div>
<div class="m2"><p>جانش گردد با یم عقل آشنا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>عقل از آن بازی همی‌یابد صبی</p></div>
<div class="m2"><p>گرچه با عقلست در ظاهر ابی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کودک دیوانه بازی کی کند</p></div>
<div class="m2"><p>جزو باید تا که کل را فی کند</p></div></div>