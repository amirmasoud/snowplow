---
title: >-
    بخش ۱۱۲ - مقالت برادر بزرگین
---
# بخش ۱۱۲ - مقالت برادر بزرگین

<div class="b" id="bn1"><div class="m1"><p>آن بزرگین گفت ای اخوان خیر</p></div>
<div class="m2"><p>ما نه نر بودیم اندر نصح غیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حشم هر که به ما کردی گله</p></div>
<div class="m2"><p>از بلا و فقر و خوف و زلزله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما همی‌گفتیم کم نال از حرج</p></div>
<div class="m2"><p>صبر کن کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این کلید صبر را اکنون چه شد</p></div>
<div class="m2"><p>ای عجب منسوخ شد قانون چه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما نمی‌گفتیم که اندر کش مکش</p></div>
<div class="m2"><p>اندر آتش هم‌چو زر خندید خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مر سپه را وقت تنگاتنگ جنگ</p></div>
<div class="m2"><p>گفته ما که هین مگردانید رنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن زمان که بود اسپان را وطا</p></div>
<div class="m2"><p>جمله سرهای بریده زیر پا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما سپاه خویش را هی هی کنان</p></div>
<div class="m2"><p>که به پیش آیید قاهر چون سنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله عالم را نشان داده به صبر</p></div>
<div class="m2"><p>زانک صبر آمد چراغ و نور صدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوبت ما شد چه خیره‌سر شدیم</p></div>
<div class="m2"><p>چون زنان زشت در چادر شدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای دلی که جمله را کردی تو گرم</p></div>
<div class="m2"><p>گرم کن خود را و از خود دار شرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای زبان که جمله را ناصح بدی</p></div>
<div class="m2"><p>نوبت تو گشت از چه تن زدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای خرد کو پند شکرخای تو</p></div>
<div class="m2"><p>دور تست این دم چه شد هیهای تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای ز دلها برده صد تشویش را</p></div>
<div class="m2"><p>نوبت تو شد بجنبان ریش را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از غری ریش ار کنون دزدیده‌ای</p></div>
<div class="m2"><p>پیش ازین بر ریش خود خندیده‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وقت پند دیگرانی های های</p></div>
<div class="m2"><p>در غم خود چون زنانی وای وای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون به درد دیگران درمان بدی</p></div>
<div class="m2"><p>درد مهمان تو آمد تن زدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بانگ بر لشکر زدن بد ساز تو</p></div>
<div class="m2"><p>بانگ بر زن چه گرفت آواز تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنچ پنجه سال بافیدی به هوش</p></div>
<div class="m2"><p>زان نسیج خود بغلتانی بپوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از نوایت گوش یاران بود خوش</p></div>
<div class="m2"><p>دست بیرون آر و گوش خود بکش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر بدی پیوسته خود را دم مکن</p></div>
<div class="m2"><p>پا و دست و ریش و سبلت گم مکن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بازی آن تست بر روی بساط</p></div>
<div class="m2"><p>خویش را در طبع آر و در نشاط</p></div></div>