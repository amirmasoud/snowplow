---
title: >-
    بخش ۱۲۶ - مفتون شدن قاضی بر زن جوحی و در صندوق ماندن و نایب قاضی صندوق را خریدن باز سال دوم آمدن زن جوحی بر امید بازی پارینه و گفتن قاضی کی مرا آزاد کن و کسی دیگر را بجوی الی آخر القصه
---
# بخش ۱۲۶ - مفتون شدن قاضی بر زن جوحی و در صندوق ماندن و نایب قاضی صندوق را خریدن باز سال دوم آمدن زن جوحی بر امید بازی پارینه و گفتن قاضی کی مرا آزاد کن و کسی دیگر را بجوی الی آخر القصه

<div class="b" id="bn1"><div class="m1"><p>جوحی هر سالی ز درویشی به فن</p></div>
<div class="m2"><p>رو بزن کردی کای دلخواه زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سلاحت هست رو صیدی بگیر</p></div>
<div class="m2"><p>تا بدوشانیم از صید تو شیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قوس ابرو تیر غمزه دام کید</p></div>
<div class="m2"><p>بهر چه دادت خدا از بهر صید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رو پی مرغی شگرفی دام نه</p></div>
<div class="m2"><p>دانه بنما لیک در خوردش مده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کام بنما و کن او را تلخ‌کام</p></div>
<div class="m2"><p>کی خورد دانه چو شد در حبس دام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد زن او نزد قاضی در گله</p></div>
<div class="m2"><p>که مرا افغان ز شوی ده‌دله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصه کوته کن که قاضی شد شکار</p></div>
<div class="m2"><p>از مقال و از جمال آن نگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت اندر محکمه‌ست این غلغله</p></div>
<div class="m2"><p>من نتوانم فهم کردن این گله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به خلوت آیی ای سرو سهی</p></div>
<div class="m2"><p>از ستم‌کاری شو شرحم دهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت خانهٔ تو ز هر نیک و بدی</p></div>
<div class="m2"><p>باشد از بهر گله آمد شدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خانهٔ سر جمله پر سودا بود</p></div>
<div class="m2"><p>صدر پر وسواس و پر غوغا بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باقی اعضا ز فکر آسوده‌اند</p></div>
<div class="m2"><p>وآن صدور از صادران فرسوده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در خزان و باد خوف حق گریز</p></div>
<div class="m2"><p>آن شقایق‌های پارین را بریز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این شقایق منع نو اشکوفه‌هاست</p></div>
<div class="m2"><p>که درخت دل برای آن نماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خویش را در خواب کن زین افتکار</p></div>
<div class="m2"><p>سر ز زیر خواب در یقظت بر آر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم‌چو آن اصحاب کهف ای خواجه زود</p></div>
<div class="m2"><p>رو به ایقاظا که تحسبهم رقود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت قاضی ای صنم معمول چیست</p></div>
<div class="m2"><p>گفت خانهٔ این کنیزک بس تهیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خصم در ده رفت و حارس نیز نیست</p></div>
<div class="m2"><p>بهر خلوت سخت نیکو مسکنیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>امشب ار امکان بود آنجا بیا</p></div>
<div class="m2"><p>کار شب بی سمعه است و بی‌ریا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جمله جاسوسان ز خمر خواب مست</p></div>
<div class="m2"><p>زنگی شب جمله را گردن زدست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خواند بر قاضی فسون‌های عجب</p></div>
<div class="m2"><p>آن شکرلب وانگهانی از چه لب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چند با آدم بلیس افسانه کرد</p></div>
<div class="m2"><p>چون حوا گفتش بخور آنگاه خورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اولین خون در جهان ظلم و داد</p></div>
<div class="m2"><p>از کف قابیل بهر زن فتاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نوح چون بر تابه بریان ساختی</p></div>
<div class="m2"><p>واهله بر تابه سنگ انداختی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مکر زن بر کار او چیره شدی</p></div>
<div class="m2"><p>آب صاف وعظ او تیره شدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قوم را پیغام کردی از نهان</p></div>
<div class="m2"><p>که نگه دارید دین زین گمرهان</p></div></div>