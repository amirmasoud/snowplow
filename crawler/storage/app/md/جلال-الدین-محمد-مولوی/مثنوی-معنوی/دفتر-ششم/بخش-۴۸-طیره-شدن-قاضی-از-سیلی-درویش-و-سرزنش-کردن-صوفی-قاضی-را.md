---
title: >-
    بخش ۴۸ - طیره شدن قاضی از سیلی درویش و سرزنش کردن صوفی قاضی را
---
# بخش ۴۸ - طیره شدن قاضی از سیلی درویش و سرزنش کردن صوفی قاضی را

<div class="b" id="bn1"><div class="m1"><p>گشت قاضی طیره صوفی گفت هی</p></div>
<div class="m2"><p>حکم تو عدلست لاشپک نیست غی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچ نپسندی به خود ای شیخ دین</p></div>
<div class="m2"><p>چون پسندی بر برادر ای امین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این ندانی که می من چه کنی</p></div>
<div class="m2"><p>هم در آن چه عاقبت خود افکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من حفر بئرا نخواندی از خبر</p></div>
<div class="m2"><p>آنچ خواندی کن عمل جان پدر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این یکی حکمت چنین بد در قضا</p></div>
<div class="m2"><p>که ترا آورد سیلی بر قفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وای بر احکام دیگرهای تو</p></div>
<div class="m2"><p>تا چه آرد بر سر و بر پای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظالمی را رحم آری از کرم</p></div>
<div class="m2"><p>که برای نفقه بادت سه درم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست ظالم را ببر چه جای آن</p></div>
<div class="m2"><p>که بدست او نهی حکم و عنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو بدان بز مانی ای مجهول‌داد</p></div>
<div class="m2"><p>که نژاد گرگ را او شیر داد</p></div></div>