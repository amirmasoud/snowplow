---
title: >-
    بخش ۱۴ - مناظرهٔ مرغ با صیاد در ترهب و در معنی ترهبی کی مصطفی علیه‌السلام نهی کرد از آن امت خود را کی لا رهبانیة فی الاسلام
---
# بخش ۱۴ - مناظرهٔ مرغ با صیاد در ترهب و در معنی ترهبی کی مصطفی علیه‌السلام نهی کرد از آن امت خود را کی لا رهبانیة فی الاسلام

<div class="b" id="bn1"><div class="m1"><p>مرغ گفتش خواجه در خلوت مه‌ایست</p></div>
<div class="m2"><p>دین احمد را ترهب نیک نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ترهب نهی کردست آن رسول</p></div>
<div class="m2"><p>بدعتی چون در گرفتی ای فضول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمعه شرطست و جماعت در نماز</p></div>
<div class="m2"><p>امر معروف و ز منکر احتراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنج بدخویان کشیدن زیر صبر</p></div>
<div class="m2"><p>منفعت دادن به خلقان هم‌چو ابر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیر ناس آن ینفع الناس ای پدر</p></div>
<div class="m2"><p>گر نه سنگی چه حریفی با مدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان امت مرحوم باش</p></div>
<div class="m2"><p>سنت احمد مهل محکوم باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت عقل هر که را نبود رسوخ</p></div>
<div class="m2"><p>پیش عاقل او چو سنگست و کلوخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون حمارست آنک نانش امنیتست</p></div>
<div class="m2"><p>صحبت او عین رهبانیتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانک غیر حق همه گردد رفات</p></div>
<div class="m2"><p>کل آت بعد حین فهو آت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکم او هم حکم قبلهٔ او بود</p></div>
<div class="m2"><p>مرده‌اش خوان چونک مرده‌جو بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که با این قوم باشد راهبست</p></div>
<div class="m2"><p>که کلوخ و سنگ او را صاحبست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خود کلوخ و سنگ کس را ره نزد</p></div>
<div class="m2"><p>زین کلوخان صد هزار آفت رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت مرغش پس جهاد آنگه بود</p></div>
<div class="m2"><p>کین چنین ره‌زن میان ره بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از برای حفظ و یاری و نبرد</p></div>
<div class="m2"><p>بر ره ناآمن آید شیرمرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عرق مردی آنگهی پیدا شود</p></div>
<div class="m2"><p>که مسافر همره اعدا شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون نبی سیف بودست آن رسول</p></div>
<div class="m2"><p>امت او صفدرانند و فحول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مصلحت در دین ما جنگ و شکوه</p></div>
<div class="m2"><p>مصلحت در دین عیسی غار و کوه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت آری گر بود یاری و زور</p></div>
<div class="m2"><p>تا به قوت بر زند بر شر و شور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون نباشد قوتی پرهیز به</p></div>
<div class="m2"><p>در فرار لا یطاق آسان بجه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت صدق دل بباید کار را</p></div>
<div class="m2"><p>ورنه یاران کم نیاید یار را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یار شو تا یار بینی بی‌عدد</p></div>
<div class="m2"><p>زانک بی‌یاران بمانی بی‌مدد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیو گرگست و تو هم‌چون یوسفی</p></div>
<div class="m2"><p>دامن یعقوب مگذار ای صفی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرگ اغلب آنگهی گیرا بود</p></div>
<div class="m2"><p>کز رمه شیشک به خود تنها رود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنک سنت یا جماعت ترک کرد</p></div>
<div class="m2"><p>در چنین مسبع نه خون خویش خورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هست سنت ره جماعت چون رفیق</p></div>
<div class="m2"><p>بی‌ره و بی‌یار افتی در مضیق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همرهی نه کو بود خصم خرد</p></div>
<div class="m2"><p>فرصتی جوید که جامهٔ تو برد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>می‌رود با تو که یابد عقبه‌ای</p></div>
<div class="m2"><p>که تواند کردت آنجا نهبه‌ای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یا بود اشتردلی چون دید ترس</p></div>
<div class="m2"><p>گوید او بهر رجوع از راه درس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یار را ترسان کند ز اشتردلی</p></div>
<div class="m2"><p>این چنین همره عدو دان نه ولی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>راه جان‌بازیست و در هر غیشه‌ای</p></div>
<div class="m2"><p>آفتی در دفع هر جان‌ شیشه‌ای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>راه دین زان رو پر از شور و شرست</p></div>
<div class="m2"><p>که نه راه هر مخنث گوهرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در ره این ترس امتحانهای نفوس</p></div>
<div class="m2"><p>هم‌چو پرویزن به تمییز سبوس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>راه چه بود پر نشان پایها</p></div>
<div class="m2"><p>یار چه بود نردبان رایها</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گیرم آن گرگت نیابد ز احتیاط</p></div>
<div class="m2"><p>بی ز جمعیت نیابی آن نشاط</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنک تنها در رهی او خوش رود</p></div>
<div class="m2"><p>با رفیقان سیر او صدتو شود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>با غلیظی خر ز یاران ای فقیر</p></div>
<div class="m2"><p>در نشاط آید شود قوت‌پذیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر خری کز کاروان تنها رود</p></div>
<div class="m2"><p>بر وی آن راه از تعب صدتو شود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چند سیخ و چند چوب افزون خورد</p></div>
<div class="m2"><p>تا که تنها آن بیابان را برد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مر ترا می‌گوید آن خر خوش شنو</p></div>
<div class="m2"><p>گر نه‌ای خر هم‌چنین تنها مرو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آنک تنها خوش رود اندر رصد</p></div>
<div class="m2"><p>با رفیقان بی‌گمان خوشتر رود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر نبیی اندرین راه درست</p></div>
<div class="m2"><p>معجزه بنمود و همراهان بجست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر نباشد یاری دیوارها</p></div>
<div class="m2"><p>کی برآید خانه و انبارها</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر یکی دیوار اگر باشد جدا</p></div>
<div class="m2"><p>سقف چون باشد معلق در هوا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر نباشد یاری حبر و قلم</p></div>
<div class="m2"><p>کی فتد بر روی کاغذها رقم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این حصیری که کسی می‌گسترد</p></div>
<div class="m2"><p>گر نپیوندد به هم بادش برد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حق ز هر جنسی چو زوجین آفرید</p></div>
<div class="m2"><p>پس نتایج شد ز جمعیت پدید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>او بگفت و او بگفت از اهتزاز</p></div>
<div class="m2"><p>بحثشان شد اندرین معنی دراز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مثنوی را چابک و دلخواه کن</p></div>
<div class="m2"><p>ماجرا را موجز و کوتاه کن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بعد از آن گفتش که گندم آن کیست</p></div>
<div class="m2"><p>گفت امانت از یتیم بی وصیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مال ایتام است امانت پیش من</p></div>
<div class="m2"><p>زانک پندارند ما را مؤتمن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفت من مضطرم و مجروح‌حال</p></div>
<div class="m2"><p>هست مردار این زمان بر من حلال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هین به دستوری ازین گندم خورم</p></div>
<div class="m2"><p>ای امین و پارسا و محترم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفت مفتی ضرورت هم توی</p></div>
<div class="m2"><p>بی‌ضرورت گر خوری مجرم شوی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ور ضرورت هست هم پرهیز به</p></div>
<div class="m2"><p>ور خوری باری ضمان آن بده</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مرغ پس در خود فرو رفت آن زمان</p></div>
<div class="m2"><p>توسنش سر بستد از جذب عنان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون بخورد آن گندم اندر فخ بماند</p></div>
<div class="m2"><p>چند او یاسین و الانعام خواند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بعد در ماندن چه افسوس و چه آه</p></div>
<div class="m2"><p>پیش از آن بایست این دود سیاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن زمان که حرص جنبید و هوس</p></div>
<div class="m2"><p>آن زمان می‌گو کای فریادرس</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کان زمان پیش از خرابی بصره است</p></div>
<div class="m2"><p>بوک بصره وا رهد هم زان شکست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ابک لی یا باکیی یا ثاکلی</p></div>
<div class="m2"><p>قبل هدم البصرة و الموصل</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نح علی قبل موتی واغتفر</p></div>
<div class="m2"><p>لا تنح لی بعد موتی واصطبر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ابک لی قبل ثبوری فی‌النوی</p></div>
<div class="m2"><p>بعد طوفان النوی خل البکا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آن زمان که دیو می‌شد راه‌زن</p></div>
<div class="m2"><p>آن زمان بایست یاسین خواندن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پیش از آنک اشکسته گردد کاروان</p></div>
<div class="m2"><p>آن زمان چوبک بزن ای پاسبان</p></div></div>