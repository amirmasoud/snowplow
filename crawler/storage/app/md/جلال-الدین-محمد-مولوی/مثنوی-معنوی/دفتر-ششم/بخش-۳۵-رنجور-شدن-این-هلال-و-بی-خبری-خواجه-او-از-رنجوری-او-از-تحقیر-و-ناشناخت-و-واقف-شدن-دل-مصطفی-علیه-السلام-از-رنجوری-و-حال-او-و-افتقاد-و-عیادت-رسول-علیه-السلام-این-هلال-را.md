---
title: >-
    بخش ۳۵ - رنجور شدن این هلال و بی‌خبری خواجهٔ او از رنجوری او از تحقیر و ناشناخت و واقف شدن دل مصطفی علیه‌السلام از رنجوری و حال او و افتقاد و عیادت رسول علیه‌السلام این هلال را
---
# بخش ۳۵ - رنجور شدن این هلال و بی‌خبری خواجهٔ او از رنجوری او از تحقیر و ناشناخت و واقف شدن دل مصطفی علیه‌السلام از رنجوری و حال او و افتقاد و عیادت رسول علیه‌السلام این هلال را

<div class="b" id="bn1"><div class="m1"><p>از قضا رنجور و ناخوش شد هلال</p></div>
<div class="m2"><p>مصطفی را وحی شد غماز حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد ز رنجوریش خواجه‌ش بی‌خبر</p></div>
<div class="m2"><p>که بر او بد کساد و بی‌خطر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفته نه روز اندر آخر محسنی</p></div>
<div class="m2"><p>هیچ کس از حال او آگاه نی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنک کس بود و شهنشاه کسان</p></div>
<div class="m2"><p>عقل صد چون قلزمش هر جا رسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحیش آمد رحم حق غم‌خوار شد</p></div>
<div class="m2"><p>که فلان مشتاق تو بیمار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مصطفی بهر هلال با شرف</p></div>
<div class="m2"><p>رفت از بهر عیادت آن طرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پی خورشید وحی آن مه دوان</p></div>
<div class="m2"><p>وآن صحابه در پیش چون اختران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماه می‌گوید که اصحابی نجوم</p></div>
<div class="m2"><p>للسری قدوه و للطاغی رجوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر را گفتند که آن سلطان رسید</p></div>
<div class="m2"><p>او ز شادی بی‌دل و جان برجهید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برگمان آن ز شادی زد دو دست</p></div>
<div class="m2"><p>کان شهنشه بهر او میر آمدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون فرو آمد ز غرفه آن امیر</p></div>
<div class="m2"><p>جان همی‌افشاند پامزد بشیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس زمین‌بوس و سلام آورد او</p></div>
<div class="m2"><p>کرد رخ را از طرب چون ورد او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت بسم‌الله مشرف کن وطن</p></div>
<div class="m2"><p>تا که فردوسی شود این انجمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا فزاید قصر من بر آسمان</p></div>
<div class="m2"><p>که بدیدم قطب دوران زمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتش از بهر عتاب آن محترم</p></div>
<div class="m2"><p>من برای دیدن تو نامدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت روحم آن تو خود روح چیست</p></div>
<div class="m2"><p>هین بفرما کین تجشم بهر کیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا شوم من خاک پای آن کسی</p></div>
<div class="m2"><p>که به باغ لطف تستش مغرسی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس بگفتش کان هلال عرش کو</p></div>
<div class="m2"><p>هم‌چو مهتاب از تواضع فرش کو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن شهی در بندگی پنهان شده</p></div>
<div class="m2"><p>بهر جاسوسی به دنیا آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو مگو کو بنده و آخرجی ماست</p></div>
<div class="m2"><p>این بدان که گنج در ویرانه‌هاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای عجب چونست از سقم آن هلال</p></div>
<div class="m2"><p>که هزاران بدر هستش پای‌مال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت از رنجش مرا آگاه نیست</p></div>
<div class="m2"><p>لیک روزی چند بر درگاه نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صحبت او با ستور و استرست</p></div>
<div class="m2"><p>سایس است و منزلش این آخرست</p></div></div>