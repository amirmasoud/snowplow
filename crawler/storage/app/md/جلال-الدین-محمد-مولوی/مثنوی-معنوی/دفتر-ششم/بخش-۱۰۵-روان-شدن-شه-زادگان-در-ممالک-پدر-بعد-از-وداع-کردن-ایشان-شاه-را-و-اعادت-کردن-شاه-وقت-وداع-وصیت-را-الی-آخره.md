---
title: >-
    بخش ۱۰۵ - روان شدن شه‌زادگان در ممالک پدر بعد از وداع کردن ایشان شاه را و اعادت کردن شاه وقت وداع وصیت را الی آخره
---
# بخش ۱۰۵ - روان شدن شه‌زادگان در ممالک پدر بعد از وداع کردن ایشان شاه را و اعادت کردن شاه وقت وداع وصیت را الی آخره

<div class="b" id="bn1"><div class="m1"><p>عزم ره کردند آن هر سه پسر</p></div>
<div class="m2"><p>سوی املاک پدر رسم سفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طواف شهرها و قلعه‌هاش</p></div>
<div class="m2"><p>از پی تدبیر دیوان و معاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست‌بوس شاه کردند و وداع</p></div>
<div class="m2"><p>پس بدیشان گفت آن شاه مطاع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجاتان دل کشد عازم شوید</p></div>
<div class="m2"><p>فی امان الله دست افشان روید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر آن یک قلعه نامش هش‌ربا</p></div>
<div class="m2"><p>تنگ آرد بر کله‌داران قبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الله الله زان دز ذات الصور</p></div>
<div class="m2"><p>دور باشید و بترسید از خطر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو و پشت برجهاش و سقف و پست</p></div>
<div class="m2"><p>جمله تمثال و نگار و صورتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم‌چو آن حجرهٔ زلیخا پر صور</p></div>
<div class="m2"><p>تا کند یوسف بناکامش نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونک یوسف سوی او می‌ننگرید</p></div>
<div class="m2"><p>خانه را پر نقش خود کرد آن مکید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به هر سو که نگرد آن خوش‌عذار</p></div>
<div class="m2"><p>روی او را بیند او بی‌اختیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر دیده‌روشنان یزدان فرد</p></div>
<div class="m2"><p>شش جهت را مظهر آیات کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بهر حیوان و نامی که نگزند</p></div>
<div class="m2"><p>از ریاض حسن ربانی چرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر این فرمود با آن اسپه او</p></div>
<div class="m2"><p>حیث ولیتم فثم وجهه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از قدح‌گر در عطش آبی خورید</p></div>
<div class="m2"><p>در درون آب حق را ناظرید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنک عاشق نیست او در آب در</p></div>
<div class="m2"><p>صورت صورت خود بیند ای صاحب‌بصر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صورت عاشق چو فانی شد درو</p></div>
<div class="m2"><p>پس در آب اکنون کرا بیند بگو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حسن حق بینند اندر روی حور</p></div>
<div class="m2"><p>هم‌چو مه در آب از صنع غیور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غیرتش بر عاشقی و صادقیست</p></div>
<div class="m2"><p>غیرتش بر دیو و بر استور نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیو اگر عاشق شود هم گوی برد</p></div>
<div class="m2"><p>جبرئیلی گشت و آن دیوی بمرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اسلم الشیطان آنجا شد پدید</p></div>
<div class="m2"><p>که یزیدی شد ز فضلش بایزید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این سخن پایان ندارد ای گروه</p></div>
<div class="m2"><p>هین نگه دارید زان قلعه وجوه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هین مبادا که هوستان ره زند</p></div>
<div class="m2"><p>که فتید اندر شقاوت تا ابد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از خطر پرهیز آمد مفترض</p></div>
<div class="m2"><p>بشنوید از من حدیث بی‌غرض</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در فرج جویی خرد سر تیز به</p></div>
<div class="m2"><p>از کمین‌گاه بلا پرهیز به</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر نمی‌گفت این سخن را آن پدر</p></div>
<div class="m2"><p>ور نمی‌فرمود زان قلعه حذر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خود بدان قلعه نمی‌شد خیلشان</p></div>
<div class="m2"><p>خود نمی‌افتاد آن سو میلشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کان نبد معروف بس مهجور بود</p></div>
<div class="m2"><p>از قلاع و از مناهج دور بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون بکرد آن منع دلشان زان مقال</p></div>
<div class="m2"><p>در هوس افتاد و در کوی خیال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رغبتی زین منع در دلشان برست</p></div>
<div class="m2"><p>که بباید سر آن را باز جست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کیست کز ممنوع گردد ممتنع</p></div>
<div class="m2"><p>چونک الانسان حریص ما منع</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نهی بر اهل تقی تبغیض شد</p></div>
<div class="m2"><p>نهی بر اهل هوا تحریض شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس ازین یغوی به قوما کثیر</p></div>
<div class="m2"><p>هم ازین یهدی به قلبا خبیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کی رمد از نی حمام آشنا</p></div>
<div class="m2"><p>بل رمد زان نی حمامات هوا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پس بگفتندش که خدمتها کنیم</p></div>
<div class="m2"><p>بر سمعنا و اطعناها تنیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رو نگردانیم از فرمان تو</p></div>
<div class="m2"><p>کفر باشد غفلت از احسان تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لیک استثنا و تسبیح خدا</p></div>
<div class="m2"><p>ز اعتماد خود بد از ایشان جدا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ذکر استثنا و حزم ملتوی</p></div>
<div class="m2"><p>گفته شد در ابتدای مثنوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صد کتاب ار هست جز یک باب نیست</p></div>
<div class="m2"><p>صد جهت را قصد جز محراب نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این طرق را مخلصی یک خانه است</p></div>
<div class="m2"><p>این هزاران سنبل از یک دانه است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گونه‌گونه خوردنیها صد هزار</p></div>
<div class="m2"><p>جمله یک چیزست اندر اعتبار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از یکی چون سیر گشتی تو تمام</p></div>
<div class="m2"><p>سرد شد اندر دلت پنجه طعام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در مجاعت پس تو احول دیده‌ای</p></div>
<div class="m2"><p>که یکی را صد هزاران دیده‌ای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفته بودیم از سقام آن کنیز</p></div>
<div class="m2"><p>وز طبیبان و قصور فهم نیز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کان طبیبان هم‌چو اسپ بی‌عذار</p></div>
<div class="m2"><p>غافل و بی‌بهره بودند از سوار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کامشان پر زخم از قرع لگام</p></div>
<div class="m2"><p>سمشان مجروح از تحویل گام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ناشده واقف که نک بر پشت ما</p></div>
<div class="m2"><p>رایض و چستیست استادی‌نما</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نیست سرگردانی ما زین لگام</p></div>
<div class="m2"><p>جز ز تصریف سوار دوست‌کام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ما پی گل سوی بستان‌ها شده</p></div>
<div class="m2"><p>گل نموده آن و آن خاری بده</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هیچ‌شان این نی که گویند از خرد</p></div>
<div class="m2"><p>بر گلوی ما کی می‌کوبد لگد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آن طبیبان آن‌چنان بندهٔ سبب</p></div>
<div class="m2"><p>گشته‌اند از مکر یزدان محتجب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر ببندی در صطبلی گاو نر</p></div>
<div class="m2"><p>باز یابی در مقام گاو خر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از خری باشد تغافل خفته‌وار</p></div>
<div class="m2"><p>که نجویی تا کیست آن خفیه کار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خود نگفته این مبدل تا کیست</p></div>
<div class="m2"><p>نیست پیدا او مگر افلاکیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تیر سوی راست پرانیده‌ای</p></div>
<div class="m2"><p>سوی چپ رفتست تیرت دیده‌ای</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سوی آهویی به صیدی تاختی</p></div>
<div class="m2"><p>خویش را تو صید خوکی ساختی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در پی سودی دویده بهر کبس</p></div>
<div class="m2"><p>نارسیده سود افتاده به حبس</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چاهها کنده برای دیگران</p></div>
<div class="m2"><p>خویش را دیده فتاده اندر آن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در سبب چون بی‌مرادت کرد رب</p></div>
<div class="m2"><p>پس چرا بدظن نگردی در سبب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بس کسی از مکسبی خاقان شده</p></div>
<div class="m2"><p>دیگری زان مکسبه عریان شده</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بس کس از عقد زنان قارون شده</p></div>
<div class="m2"><p>بس کس از عقد زنان مدیون شده</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پس سبب گردان چو دم خر بود</p></div>
<div class="m2"><p>تکیه بر وی کم کنی بهتر بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ور سبب گیری نگیری هم دلیر</p></div>
<div class="m2"><p>که بس آفت‌هاست پنهانش به زیر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سر استثناست این حزم و حذر</p></div>
<div class="m2"><p>زانک خر را بز نماید این قدر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آنک چشمش بست گرچه گربزست</p></div>
<div class="m2"><p>ز احولی اندر دو چشمش خربزست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون مقلب حق بود ابصار را</p></div>
<div class="m2"><p>که بگرداند دل و افکار را</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چاه را تو خانه‌ای بینی لطیف</p></div>
<div class="m2"><p>دام را تو دانه‌ای بینی ظریف</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>این تفسطط نیست تقلیب خداست</p></div>
<div class="m2"><p>می‌نماید که حقیقتها کجاست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>آنک انکار حقایق می‌کند</p></div>
<div class="m2"><p>جملگی او بر خیالی می‌تند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>او نمی‌گوید که حسبان خیال</p></div>
<div class="m2"><p>هم خیالی باشدت چشمی به مال</p></div></div>