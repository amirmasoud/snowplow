---
title: >-
    بخش ۱۳۶ - کرامات شیخ شیبان راعی قدس الله روحه العزیز
---
# بخش ۱۳۶ - کرامات شیخ شیبان راعی قدس الله روحه العزیز

<div class="b" id="bn1"><div class="m1"><p>هم‌چو آن شیبان که از گرگ عنید</p></div>
<div class="m2"><p>وقت جمعه بر رعا خط می‌کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا برون ناید از آن خط گوسفند</p></div>
<div class="m2"><p>نه در آید گرگ و دزد با گزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر مثال دایرهٔ تعویذ هود</p></div>
<div class="m2"><p>که اندر آن صرصر امان آل بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هشت روزی اندرین خط تن زنید</p></div>
<div class="m2"><p>وز برون مثله تماشا می‌کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هوا بردی فکندی بر حجر</p></div>
<div class="m2"><p>تا دریدی لحم و عظم از هم‌دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک گره را بر هوا درهم زدی</p></div>
<div class="m2"><p>تا چو خشخاش استخوان ریزان شدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن سیاست را که لرزید آسمان</p></div>
<div class="m2"><p>مثنوی اندر نگنجد شرح آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به طبع این می‌کنی ای باد سرد</p></div>
<div class="m2"><p>گرد خط و دایرهٔ آن هود گرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای طبیعی فوق طبع این ملک بین</p></div>
<div class="m2"><p>یا بیا و محو کن از مصحف این</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مقریان را منع کن بندی بنه</p></div>
<div class="m2"><p>یا معلم را به مال و سهم ده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاجزی و خیره کن عجز از کجاست</p></div>
<div class="m2"><p>عجز تو تابی از آن روز جزاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجزها داری تو در پیش ای لجوج</p></div>
<div class="m2"><p>وقت شد پنهانیان را نک خروج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خرم آن کین عجز و حیرت قوت اوست</p></div>
<div class="m2"><p>در دو عالم خفته اندر ظل دوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم در آخر عجز خود را او بدید</p></div>
<div class="m2"><p>مرده شد دین عجایز را گزید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون زلیخا یوسفش بر وی بتافت</p></div>
<div class="m2"><p>از عجوزی در جوانی راه یافت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زندگی در مردن و در محنتست</p></div>
<div class="m2"><p>آب حیوان در درون ظلمتست</p></div></div>