---
title: >-
    بخش ۷۵ - پند دادن خرگوش نخچیران را کی بدین شاد مشوید
---
# بخش ۷۵ - پند دادن خرگوش نخچیران را کی بدین شاد مشوید

<div class="b" id="bn1"><div class="m1"><p>هین بمُلکِ نوبتی شادی مکن</p></div>
<div class="m2"><p>ای تو بستهٔ نوبت آزادی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنک ملکش برتر از نوبت تنند</p></div>
<div class="m2"><p>برتر از هفت انجمش نوبت زنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برتر از نوبت ملوک باقیند</p></div>
<div class="m2"><p>دور دایم روحها با ساقیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک این شرب ار بگویی یک دو روز</p></div>
<div class="m2"><p>در کنی اندر شراب خلد پوز</p></div></div>