---
title: >-
    بخش ۷۶ - تفسیر رجعنا من الجهاد الاصغر الی‌الجهاد الاکبر
---
# بخش ۷۶ - تفسیر رجعنا من الجهاد الاصغر الی‌الجهاد الاکبر

<div class="b" id="bn1"><div class="m1"><p>ای شهان کشتیم ما خصم برون</p></div>
<div class="m2"><p>ماند خصمی زو بتر در اندرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتن این، کار عقل و هوش نیست</p></div>
<div class="m2"><p>شیر باطن سخرهٔ خرگوش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوزخست این نفس و دوزخ اژدهاست</p></div>
<div class="m2"><p>کو به دریاها نگردد کم و کاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هفت دریا را در آشامد، هنوز</p></div>
<div class="m2"><p>کم نگردد سوزش آن خلق‌سوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگها و کافران سنگ‌دل</p></div>
<div class="m2"><p>اندر آیند اندرو زار و خجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم نگردد ساکن از چندین غذا</p></div>
<div class="m2"><p>تا ز حق آید مرورا این ندا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیر گشتی سیر، گوید نه هنوز</p></div>
<div class="m2"><p>اینت آتش اینت تابش اینت سوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالمی را لقمه کرد و در کشید</p></div>
<div class="m2"><p>معده‌اش نعره زنان هل من مزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حق قدم بر وی نهد از لامکان</p></div>
<div class="m2"><p>آنگه او ساکن شود از کن فکان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونک جزو دوزخست این نفس ما</p></div>
<div class="m2"><p>طبع کل دارد همیشه جزوها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این قدم حق را بود کو را کُشد</p></div>
<div class="m2"><p>غیر حق خود کی کمان او کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کمان ننهند الا تیر راست</p></div>
<div class="m2"><p>این کمان را بازگون کژ تیرهاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راست شو چون تیر و واره از کمان</p></div>
<div class="m2"><p>کز کمان هر راست بجهد بی‌گمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چونک وا گشتم ز پیگار برون</p></div>
<div class="m2"><p>روی آوردم به پیگار درون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قد رجعنا من جهاد الاصغریم</p></div>
<div class="m2"><p>با نبی اندر جهاد اکبریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قوت از حق خواهم و توفیق و لاف</p></div>
<div class="m2"><p>تا به سوزن بر کَنم این کوه قاف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سهل شیری دان که صفها بشکند</p></div>
<div class="m2"><p>شیر آنست آن که خود را بشکند</p></div></div>