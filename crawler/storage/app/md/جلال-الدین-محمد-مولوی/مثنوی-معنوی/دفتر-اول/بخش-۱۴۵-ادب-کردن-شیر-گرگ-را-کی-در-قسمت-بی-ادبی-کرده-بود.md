---
title: >-
    بخش ۱۴۵ - ادب کردن شیر گرگ را کی در قسمت بی‌ادبی کرده بود
---
# بخش ۱۴۵ - ادب کردن شیر گرگ را کی در قسمت بی‌ادبی کرده بود

<div class="b" id="bn1"><div class="m1"><p>گرگ را بر کند سر آن سرفراز</p></div>
<div class="m2"><p>تا نماند دوسری و امتیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فانتقمنا منهم است ای گرگ پیر</p></div>
<div class="m2"><p>چون نبودی مرده در پیش امیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد از آن رو شیر با روباه کرد</p></div>
<div class="m2"><p>گفت این را بخش کن از بهر خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سجده کرد و گفت کین گاو سمین</p></div>
<div class="m2"><p>چاشت‌خوردت باشد ای شاه گزین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان بز از بهر میان روز را</p></div>
<div class="m2"><p>یخنیی باشد شه پیروز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و آن دگر خرگوش بهر شام هم</p></div>
<div class="m2"><p>شب‌چرهٔ این شاه با لطف و کرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ای روبه تو عدل افروختی</p></div>
<div class="m2"><p>این چنین قسمت ز کی آموختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کجا آموختی این ای بزرگ</p></div>
<div class="m2"><p>گفت ای شاه جهان از حال گرگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت چون در عشق ما گشتی گرو</p></div>
<div class="m2"><p>هر سه را بر گیر و بستان و برو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روبها چون جملگی ما را شدی</p></div>
<div class="m2"><p>چونت آزاریم چون تو ما شدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما ترا و جمله اشکاران ترا</p></div>
<div class="m2"><p>پای بر گردون هفتم نه بر آ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون گرفتی عبرت از گرگ دنی</p></div>
<div class="m2"><p>پس تو روبه نیستی شیر منی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاقل آن باشد که عبرت گیرد از</p></div>
<div class="m2"><p>مرگ یاران در بلای محترز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روبه آن دم بر زبان صد شکر راند</p></div>
<div class="m2"><p>که مرا شیر از پی آن گرگ خواند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر مرا اول بفرمودی که تو</p></div>
<div class="m2"><p>بخش کن این را که بردی جان ازو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس سپاس او را که ما را در جهان</p></div>
<div class="m2"><p>کرد پیدا از پس پیشینیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا شنیدیم آن سیاستهای حق</p></div>
<div class="m2"><p>بر قرون ماضیه اندر سبق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا که ما از حال آن گرگان پیش</p></div>
<div class="m2"><p>همچو روبه پاس خود داریم بیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>امت مرحومه زین رو خواندمان</p></div>
<div class="m2"><p>آن رسول حق و صادق در بیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>استخوان و پشم آن گرگان عیان</p></div>
<div class="m2"><p>بنگرید و پند گیرید ای مهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عاقل از سر بنهد این هستی و باد</p></div>
<div class="m2"><p>چون شنید انجام فرعونان و عاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور بننهد دیگران از حال او</p></div>
<div class="m2"><p>عبرتی گیرند از اضلال او</p></div></div>