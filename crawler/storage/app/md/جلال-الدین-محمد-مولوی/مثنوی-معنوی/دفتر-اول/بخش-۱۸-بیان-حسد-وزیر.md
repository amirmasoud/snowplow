---
title: >-
    بخش ۱۸ - بیان حسد وزیر
---
# بخش ۱۸ - بیان حسد وزیر

<div class="b" id="bn1"><div class="m1"><p>آن وزیرک از حسد بودش نژاد</p></div>
<div class="m2"><p>تا به باطل گوش و بینی باد داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر امید آنک از نیش حسد</p></div>
<div class="m2"><p>زهر او در جان مسکینان رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کسی کو از حسد بینی کند</p></div>
<div class="m2"><p>خویش را بی‌گوش و بی بینی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بینی آن باشد که او بویی برد</p></div>
<div class="m2"><p>بوی او را جانب کویی برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که بویش نیست بی بینی بود</p></div>
<div class="m2"><p>بوی آن بویست کان دینی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونک بویی برد و شکر آن نکرد</p></div>
<div class="m2"><p>کفر نعمت آمد و بینیش خورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر کن مر شاکران را بنده باش</p></div>
<div class="m2"><p>پیش ایشان مرده شو پاینده باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون وزیر از ره‌زنی مایه مساز</p></div>
<div class="m2"><p>خلق را تو بر میاور از نماز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناصح دین گشته آن کافر وزیر</p></div>
<div class="m2"><p>کرده او از مکر در گوزینه سیر</p></div></div>