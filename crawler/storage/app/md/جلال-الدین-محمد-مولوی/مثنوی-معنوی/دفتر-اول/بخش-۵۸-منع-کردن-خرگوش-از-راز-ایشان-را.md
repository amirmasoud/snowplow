---
title: >-
    بخش ۵۸ - منع کردن خرگوش از راز ایشان را
---
# بخش ۵۸ - منع کردن خرگوش از راز ایشان را

<div class="b" id="bn1"><div class="m1"><p>گفت هر رازی نشاید باز گفت</p></div>
<div class="m2"><p>جفت طاق آید گهی گه طاق جفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صفا گر دم زنی با آینه</p></div>
<div class="m2"><p>تیره گردد زود با ما آینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بیان این سه کم جنبان لبت</p></div>
<div class="m2"><p>از ذهاب و از ذهب وز مذهبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کین سه را خصمست بسیار و عدو</p></div>
<div class="m2"><p>در کمینت ایستد چون داند او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور بگویی با یکی دو الوداع</p></div>
<div class="m2"><p>کل سر جاوز الاثنین شاع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دو سه پرنده را بندی بهم</p></div>
<div class="m2"><p>بر زمین مانند محبوس از الم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشورت دارند سرپوشیده خوب</p></div>
<div class="m2"><p>در کنایت با غلط‌افکن مشوب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشورت کردی پیمبر بسته‌سر</p></div>
<div class="m2"><p>گفته ایشانش جواب و بی‌خبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در مثالی بسته گفتی رای را</p></div>
<div class="m2"><p>تا ندانند خصم از سر پای را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او جواب خویش بگرفتی ازو</p></div>
<div class="m2"><p>وز سؤالش می‌نبردی غیر بو</p></div></div>