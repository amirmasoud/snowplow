---
title: >-
    بخش ۱ - سرآغاز
---
# بخش ۱ - سرآغاز

<div class="b" id="bn1"><div class="m1"><p>بشنو این نی چون شکایت می‌کند</p></div>
<div class="m2"><p>از جدایی‌ها حکایت می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز نیستان تا مرا ببریده‌اند</p></div>
<div class="m2"><p>در نفیرم مرد و زن نالیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه خواهم شرحه شرحه از فراق</p></div>
<div class="m2"><p>تا بگویم شرح درد اشتیاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کسی کو دور ماند از اصل خویش</p></div>
<div class="m2"><p>باز جوید روزگار وصل خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به هر جمعیتی نالان شدم</p></div>
<div class="m2"><p>جفت بدحالان و خوش‌حالان شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کسی از ظن خود شد یار من</p></div>
<div class="m2"><p>از درون من نجست اسرار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر من از نالهٔ من دور نیست</p></div>
<div class="m2"><p>لیک چشم و گوش را آن نور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن ز جان و جان ز تن مستور نیست</p></div>
<div class="m2"><p>لیک کس را دید جان دستور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتش است این بانگ نای و نیست باد</p></div>
<div class="m2"><p>هر که این آتش ندارد نیست باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش عشق است کاندر نی فتاد</p></div>
<div class="m2"><p>جوشش عشق است کاندر می فتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی حریف هر که از یاری برید</p></div>
<div class="m2"><p>پرده‌هایش پرده‌های ما درید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو نی زهری و تریاقی که دید</p></div>
<div class="m2"><p>همچو نی دمساز و مشتاقی که دید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نی حدیث راه پر خون می‌کند</p></div>
<div class="m2"><p>قصه‌های عشق مجنون می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محرم این هوش جز بیهوش نیست</p></div>
<div class="m2"><p>مر زبان را مشتری جز گوش نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در غم ما روزها بیگاه شد</p></div>
<div class="m2"><p>روزها با سوزها همراه شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزها گر رفت گو رو باک نیست</p></div>
<div class="m2"><p>تو بمان ای آن که چون تو پاک نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که جز ماهی ز آبش سیر شد</p></div>
<div class="m2"><p>هرکه بی روزیست روزش دیر شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در نیابد حال پخته هیچ خام</p></div>
<div class="m2"><p>پس سخن کوتاه باید و السلام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بند بگسل باش آزاد ای پسر</p></div>
<div class="m2"><p>چند باشی بند سیم و بند زر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر بریزی بحر را در کوزه‌ای</p></div>
<div class="m2"><p>چند گنجد قسمت یک روزه‌ای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کوزهٔ چشم حریصان پر نشد</p></div>
<div class="m2"><p>تا صدف قانع نشد پر در نشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که را جامه ز عشقی چاک شد</p></div>
<div class="m2"><p>او ز حرص و عیب کلی پاک شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شاد باش ای عشق خوش سودای ما</p></div>
<div class="m2"><p>ای طبیب جمله علت‌های ما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای دوای نخوت و ناموس ما</p></div>
<div class="m2"><p>ای تو افلاطون و جالینوس ما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جسم خاک از عشق بر افلاک شد</p></div>
<div class="m2"><p>کوه در رقص آمد و چالاک شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عشق جان طور آمد عاشقا</p></div>
<div class="m2"><p>طور مست و خر موسی صاعقا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با لب دمساز خود گر جفتمی</p></div>
<div class="m2"><p>همچو نی من گفتنی‌ها گفتمی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر که او از هم‌زبانی شد جدا</p></div>
<div class="m2"><p>بی‌زبان شد گرچه دارد صد نوا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون که گل رفت و گلستان درگذشت</p></div>
<div class="m2"><p>نشنوی زآن پس ز بلبل سر گذشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جمله معشوق است و عاشق پرده‌ای</p></div>
<div class="m2"><p>زنده معشوق است و عاشق مرده‌ای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون نباشد عشق را پروای او</p></div>
<div class="m2"><p>او چو مرغی ماند بی‌پر وای او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من چگونه هوش دارم پیش و پس</p></div>
<div class="m2"><p>چون نباشد نور یارم پیش و پس</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عشق خواهد کاین سخن بیرون بود</p></div>
<div class="m2"><p>آینه غماز نبود چون بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آینه‌ت دانی چرا غماز نیست</p></div>
<div class="m2"><p>زآن که زنگار از رخش ممتاز نیست</p></div></div>