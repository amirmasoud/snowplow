---
title: >-
    بخش ۶۶ - قصهٔ هدهد و سلیمان در بیان آنک چون قضا آید چشمهای روشن بسته شود
---
# بخش ۶۶ - قصهٔ هدهد و سلیمان در بیان آنک چون قضا آید چشمهای روشن بسته شود

<div class="b" id="bn1"><div class="m1"><p>چون سلیمان را سراپرده زدند</p></div>
<div class="m2"><p>جمله مرغانش به خدمت آمدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم‌زبان و محرم خود یافتند</p></div>
<div class="m2"><p>پیش او یک یک بجان بشتافتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله مرغان ترک کرده چیک چیک</p></div>
<div class="m2"><p>با سلیمان گشته افصح من اخیک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همزبانی خویشی و پیوندی است</p></div>
<div class="m2"><p>مرد با نامحرمان چون بندی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسا هندو و ترک همزبان</p></div>
<div class="m2"><p>ای بسا دو ترک چون بیگانگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس زبان محرمی خود دیگرست</p></div>
<div class="m2"><p>همدلی از همزبانی بهترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیرنطق و غیر ایما و سجل</p></div>
<div class="m2"><p>صد هزاران ترجمان خیزد ز دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله مرغان هر یکی اسرار خود</p></div>
<div class="m2"><p>از هنر وز دانش و از کار خود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با سلیمان یک بیک وا می‌نمود</p></div>
<div class="m2"><p>از برای عرضه خود را می‌ستود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تکبر نه و از هستی خویش</p></div>
<div class="m2"><p>بهر آن تا ره دهد او را به پیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بباید برده را از خواجه‌ای</p></div>
<div class="m2"><p>عرضه دارد از هنر دیباجه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونک دارد از خریداریش ننگ</p></div>
<div class="m2"><p>خود کند بیمار و کر و شل و لنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نوبت هدهد رسید و پیشه‌اش</p></div>
<div class="m2"><p>و آن بیان صنعت و اندیشه‌اش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت ای شه یک هنر کان کهترست</p></div>
<div class="m2"><p>باز گویم گفت کوته بهترست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت بر گو تا کدامست آن هنر</p></div>
<div class="m2"><p>گفت من آنگه که باشم اوج بر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنگرم از اوج با چشم یقین</p></div>
<div class="m2"><p>من ببینم آب در قعر زمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا کجایست و چه عمقستش چه رنگ</p></div>
<div class="m2"><p>از چه می‌جوشد ز خاکی یا ز سنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای سلیمان بهر لشگرگاه را</p></div>
<div class="m2"><p>در سفر می‌دار این آگاه را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس سلیمان گفت ای نیکو رفیق</p></div>
<div class="m2"><p>در بیابانهای بی آب عمیق</p></div></div>