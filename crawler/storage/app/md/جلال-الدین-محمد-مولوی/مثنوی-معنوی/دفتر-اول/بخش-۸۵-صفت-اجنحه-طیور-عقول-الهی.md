---
title: >-
    بخش ۸۵ - صفت اجنحهٔ طیور عقول الهی
---
# بخش ۸۵ - صفت اجنحهٔ طیور عقول الهی

<div class="b" id="bn1"><div class="m1"><p>قصهٔ طوطی جان زین سان بود</p></div>
<div class="m2"><p>کو کسی کو محرم مرغان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو یکی مرغی ضعیفی بی‌گناه</p></div>
<div class="m2"><p>و اندرون او سلیمان با سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بنالد زار بی‌شکر و گله</p></div>
<div class="m2"><p>افتد اندر هفت گردون غلغله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دمش صد نامه صد پیک از خدا</p></div>
<div class="m2"><p>یا ربی زو شصت لبیک از خدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلت او به ز طاعت نزد حق</p></div>
<div class="m2"><p>پیش کفرش جمله ایمانها خلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دمی او را یکی معراج خاص</p></div>
<div class="m2"><p>بر سر تاجش نهد صد تاج خاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورتش بر خاک و جان بر لامکان</p></div>
<div class="m2"><p>لامکانی فوق وهم سالکان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لامکانی نه که در فهم آیدت</p></div>
<div class="m2"><p>هر دمی در وی خیالی زایدت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بل مکان و لامکان در حکم او</p></div>
<div class="m2"><p>همچو در حکم بهشتی چار جو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرح این کوته کن و رخ زین بتاب</p></div>
<div class="m2"><p>دم مزن والله اعلم بالصواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز می‌گردیم ما ای دوستان</p></div>
<div class="m2"><p>سوی مرغ و تاجر و هندوستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرد بازرگان پذیرفت این پیام</p></div>
<div class="m2"><p>کو رساند سوی جنس از وی سلام</p></div></div>