---
title: >-
    بخش ۹ - فرستادن پادشاه رسولان به سمرقند به آوردن زرگر
---
# بخش ۹ - فرستادن پادشاه رسولان به سمرقند به آوردن زرگر

<div class="b" id="bn1"><div class="m1"><p>شه فرستاد آن طرف یک دو رسول</p></div>
<div class="m2"><p>حاذقان و کافیان بس عَدول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سمرقند آمدند آن دو امیر</p></div>
<div class="m2"><p>پیش آن زرگر ز شاهنشه بَشیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«کای لطیف استاد کامل معرفت</p></div>
<div class="m2"><p>فاش اندر شهرها از تو صفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نک فلان شه از برای زرگری</p></div>
<div class="m2"><p>اختیارت کرد زیرا مهتری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینک این خلعت بگیر و زرّ و سیم</p></div>
<div class="m2"><p>چون بیایی خاص باشی و ندیم»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد مال و خلعت بسیار دید</p></div>
<div class="m2"><p>غرّه شد از شهر و فرزندان برید.</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر آمد شادمان در راه مرد</p></div>
<div class="m2"><p>بی‌خبر کان شاه قصد جانْش کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسپ تازی برنشست و شاد تاخت</p></div>
<div class="m2"><p>خونبهای خویش را خلعت شناخت.</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شده اندر سفر با صد رضا</p></div>
<div class="m2"><p>خود به پای خویش تا سوء القضا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خیالش مُلک و عِزّ و مهتری</p></div>
<div class="m2"><p>گفت عزرائیل رو، آری بری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون رسید از راه آن مرد غریب</p></div>
<div class="m2"><p>اندر آوردش به پیش شه طبیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوی شاهنشاه بردندش بناز</p></div>
<div class="m2"><p>تا بسوزد بر سر شمع طراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه دید او را بسی تعظیم کرد</p></div>
<div class="m2"><p>مخزن زر را بدو تسلیم کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس حکیمش گفت کای سلطان مِه</p></div>
<div class="m2"><p>آن کنیزک را بدین خواجه بدِه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا کنیزک در وصالش خوش شود</p></div>
<div class="m2"><p>آب وصلش دفع آن آتش شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شه بدو بخشید آن مه روی را</p></div>
<div class="m2"><p>جفت کرد آن هر دو صحبت‌جوی را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مدت شش ماه می‌راندند کام</p></div>
<div class="m2"><p>تا به صحتْ آمد آن دختر تمام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بعد از آن از بهر او شربت بساخت</p></div>
<div class="m2"><p>تا بخورد و پیش دختر می‌گداخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون ز رنجوری جمال او نماند</p></div>
<div class="m2"><p>جان دختر در وبال او نماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونک زشت و ناخوش و رخ زرد شد</p></div>
<div class="m2"><p>اندک‌اندک در دل او سرد شد.</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عشقهایی کز پی رنگی بود</p></div>
<div class="m2"><p>عشق نبود عاقبت ننگی بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کاش کان هم ننگ بودی یکسری</p></div>
<div class="m2"><p>تا نرفتی بر وی آن بد داوری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خون دوید از چشم همچون جوی او</p></div>
<div class="m2"><p>دشمن جان وی آمد روی او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دشمن طاووس آمد پرّ او</p></div>
<div class="m2"><p>ای بسی شه را بکشته فرّ او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت: «من آن آهوم کز ناف من</p></div>
<div class="m2"><p>ریخت این صیاد خون صاف من،</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای من آن روباهِ صحرا، کز کمین</p></div>
<div class="m2"><p>سر بریدندش برای پوستین،</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای من آن پیلی که زخم پیلبان</p></div>
<div class="m2"><p>ریخت خونم از برای استخوان،</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنک کشتستم پی مادون من</p></div>
<div class="m2"><p>می‌نداند که نخسپد خون من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر منست امروز و فردا بر وی‌است</p></div>
<div class="m2"><p>خون چون من کسْ، چنین ضایع کی‌است؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر چه دیوار افکند سایهٔ دراز</p></div>
<div class="m2"><p>باز گردد سوی او آن سایه باز؛</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این جهان کوهست و فعل ما ندا</p></div>
<div class="m2"><p>سوی ما آید نداها را صدا»</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این بگفت و رفت در دَم زیر خاک</p></div>
<div class="m2"><p>آن کنیزک شد ز عشق‌و رنج پاک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زانک عشق مردگان پاینده نیست</p></div>
<div class="m2"><p>زانک مرده سوی ما آینده نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عشق زنده در روان و در بصر</p></div>
<div class="m2"><p>هر دمی باشد ز غنچه تازه‌تر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عشق آن زنده گزین کو باقیست</p></div>
<div class="m2"><p>کز شراب جان‌فزایت ساقیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عشق آن بگزین که جمله انبیا</p></div>
<div class="m2"><p>یافتند از عشق او کار و کیا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو مگو ما را بدان شه بار نیست</p></div>
<div class="m2"><p>با کریمان کارها دشوار نیست</p></div></div>