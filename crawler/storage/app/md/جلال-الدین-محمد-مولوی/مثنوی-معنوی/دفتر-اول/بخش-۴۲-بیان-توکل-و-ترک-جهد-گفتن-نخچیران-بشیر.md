---
title: >-
    بخش ۴۲ - بیان توکل و ترک جهد گفتن نخچیران بشیر
---
# بخش ۴۲ - بیان توکل و ترک جهد گفتن نخچیران بشیر

<div class="b" id="bn1"><div class="m1"><p>طایفهٔ نخچیر در وادی خوش</p></div>
<div class="m2"><p>بودشان از شیر دایم کش‌مکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که آن شیر از کمین می در ربود</p></div>
<div class="m2"><p>آن چرا بر جمله ناخوش گشته بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیله کردند آمدند ایشان بشیر</p></div>
<div class="m2"><p>کز وظیفه ما ترا داریم سیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد ازین اندر پی صیدی میا</p></div>
<div class="m2"><p>تا نگردد تلخ بر ما این گیا</p></div></div>