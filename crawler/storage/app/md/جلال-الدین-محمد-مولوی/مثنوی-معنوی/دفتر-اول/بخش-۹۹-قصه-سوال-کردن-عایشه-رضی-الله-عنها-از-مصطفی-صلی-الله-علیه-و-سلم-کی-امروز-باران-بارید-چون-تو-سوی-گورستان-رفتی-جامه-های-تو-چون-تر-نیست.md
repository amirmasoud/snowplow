---
title: >-
    بخش ۹۹ - قصهٔ سوال کردن عایشه رضی الله عنها از مصطفی صلی‌الله علیه و سلم کی امروز باران بارید چون تو سوی گورستان رفتی جامه‌های تو چون تر نیست
---
# بخش ۹۹ - قصهٔ سوال کردن عایشه رضی الله عنها از مصطفی صلی‌الله علیه و سلم کی امروز باران بارید چون تو سوی گورستان رفتی جامه‌های تو چون تر نیست

<div class="b" id="bn1"><div class="m1"><p>مصطفی روزی به گورستان برفت</p></div>
<div class="m2"><p>با جنازهٔ مردی از یاران برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک را در گور او آگنده کرد</p></div>
<div class="m2"><p>زیر خاک آن دانه‌اش را زنده کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این درختانند همچون خاکیان</p></div>
<div class="m2"><p>دستها بر کرده‌اند از خاکدان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی خلقان صد اشارت می‌کنند</p></div>
<div class="m2"><p>وانک گوشستش عبارت می‌کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با زبان سبز و با دست دراز</p></div>
<div class="m2"><p>از ضمیر خاک می‌گویند راز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو بطان سر فرو برده به آب</p></div>
<div class="m2"><p>گشته طاووسان و بوده چون غراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زمستانشان اگر محبوس کرد</p></div>
<div class="m2"><p>آن غرابان را خدا طاووس کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در زمستانشان اگر چه داد مرگ</p></div>
<div class="m2"><p>زنده‌شان کرد از بهار و داد برگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منکران گویند خود هست این قدیم</p></div>
<div class="m2"><p>این چرا بندیم بر رب کریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوری ایشان درون دوستان</p></div>
<div class="m2"><p>حق برویانید باغ و بوستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر گلی کاندر درون بویا بود</p></div>
<div class="m2"><p>آن گل از اسرار کل گویا بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بوی ایشان رغم آنف منکران</p></div>
<div class="m2"><p>گرد عالم می‌رود پرده‌دران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منکران همچون جعل زان بوی گل</p></div>
<div class="m2"><p>یا چو نازک مغز در بانگ دهل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خویشتن مشغول می‌سازند و غرق</p></div>
<div class="m2"><p>چشم می‌دزدند ازین لمعان برق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشم می‌دزدند و آنجا چشم نی</p></div>
<div class="m2"><p>چشم آن باشد که بیند مامنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون ز گورستان پیمبر باز گشت</p></div>
<div class="m2"><p>سوی صدیقه شد و همراز گشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چشم صدیقه چو بر رویش فتاد</p></div>
<div class="m2"><p>پیش آمد دست بر وی می‌نهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر عمامه و روی او و موی او</p></div>
<div class="m2"><p>بر گریبان و بر و بازوی او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت پیغامبر چه می‌جویی شتاب</p></div>
<div class="m2"><p>گفت باران آمد امروز از سحاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جامه‌هاات می‌بجویم در طلب</p></div>
<div class="m2"><p>تر نمی‌یابم ز باران ای عجب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت چه بر سر فکندی از ازار</p></div>
<div class="m2"><p>گفت کردم آن ردای تو خمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت بهر آن نمود ای پاک‌جیب</p></div>
<div class="m2"><p>چشم پاکت را خدا باران غیب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیست آن باران ازین ابر شما</p></div>
<div class="m2"><p>هست ابری دیگر و دیگر سما</p></div></div>