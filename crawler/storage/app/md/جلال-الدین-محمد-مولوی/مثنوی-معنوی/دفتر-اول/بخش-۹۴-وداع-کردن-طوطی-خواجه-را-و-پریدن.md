---
title: >-
    بخش ۹۴ - وداع کردن طوطی خواجه را و پریدن
---
# بخش ۹۴ - وداع کردن طوطی خواجه را و پریدن

<div class="b" id="bn1"><div class="m1"><p>یک دو پندش داد طوطی بی‌نفاق</p></div>
<div class="m2"><p>بعد از آن گفتش سلام الفراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواجه گفتش فی امان الله برو</p></div>
<div class="m2"><p>مر مرا اکنون نمودی راه نو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجه با خود گفت کین پند منست</p></div>
<div class="m2"><p>راه او گیرم که این ره روشنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان من کمتر ز طوطی کی بود</p></div>
<div class="m2"><p>جان چنین باید که نیکوپی بود</p></div></div>