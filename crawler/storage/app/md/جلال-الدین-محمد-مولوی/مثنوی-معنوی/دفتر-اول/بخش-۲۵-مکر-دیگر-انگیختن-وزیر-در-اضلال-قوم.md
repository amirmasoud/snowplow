---
title: >-
    بخش ۲۵ - مکر دیگر انگیختن وزیر در اضلال قوم
---
# بخش ۲۵ - مکر دیگر انگیختن وزیر در اضلال قوم

<div class="b" id="bn1"><div class="m1"><p>مکر دیگر آن وزیر از خود ببست</p></div>
<div class="m2"><p>وعظ را بگذاشت و در خلوت نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مریدان در فکند از شوق سوز</p></div>
<div class="m2"><p>بود در خلوت چهل پنجاه روز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلق دیوانه شدند از شوق او</p></div>
<div class="m2"><p>از فراق حال و قال و ذوق او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لابه و زاری همی کردند و او</p></div>
<div class="m2"><p>از ریاضت گشته در خلوت دوتو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته ایشان نیست ما را بی تو نور</p></div>
<div class="m2"><p>بی عصاکش چون بود احوال کور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر اکرام و از بهر خدا</p></div>
<div class="m2"><p>بیش ازین ما را مدار از خود جدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما چو طفلانیم و ما را دایه تو</p></div>
<div class="m2"><p>بر سر ما گستران آن سایه تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت جانم از محبان دور نیست</p></div>
<div class="m2"><p>لیک بیرون آمدن دستور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن امیران در شفاعت آمدند</p></div>
<div class="m2"><p>وان مریدان در شناعت آمدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کین چه بدبختیست ما را ای کریم</p></div>
<div class="m2"><p>از دل و دین مانده ما بی تو یتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بهانه می‌کنی و ما ز درد</p></div>
<div class="m2"><p>می‌زنیم از سوز دل دمهای سرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما به گفتار خوشت خو کرده‌ایم</p></div>
<div class="m2"><p>ما ز شیر حکمت تو خورده‌ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الله الله این جفا با ما مکن</p></div>
<div class="m2"><p>خیر کن امروز را فردا مکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌دهد دل مر ترا کین بی‌دلان</p></div>
<div class="m2"><p>بی تو گردند آخر از بی‌حاصلان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمله در خشکی چو ماهی می‌طپند</p></div>
<div class="m2"><p>آب را بگشا ز جو بر دار بند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای که چون تو در زمانه نیست کس</p></div>
<div class="m2"><p>الله الله خلق را فریاد رس</p></div></div>