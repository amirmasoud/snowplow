---
title: >-
    بخش ۱۰ - بیان آنک کشتن و زهر دادن مرد زرگر به اشارت الهی بود نه به هوای نفس و تامل فاسد
---
# بخش ۱۰ - بیان آنک کشتن و زهر دادن مرد زرگر به اشارت الهی بود نه به هوای نفس و تامل فاسد

<div class="b" id="bn1"><div class="m1"><p>کشتن آن مرد بر دست حکیم</p></div>
<div class="m2"><p>نه پی اومید بود و نه ز بیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او نکشتش از برای طبع شاه</p></div>
<div class="m2"><p>تا نیامد امر و الهام اله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن پسر را کش خضر ببرید حلق</p></div>
<div class="m2"><p>سر آن را در نیابد عام خلق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنک از حق یابد او وحی و جواب</p></div>
<div class="m2"><p>هرچه فرماید بود عین صواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنک جان بخشد اگر بکشد رواست</p></div>
<div class="m2"><p>نایبست و دست او دست خداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو اسماعیل پیشش سر بنه</p></div>
<div class="m2"><p>شاد و خندان پیش تیغش جان بده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بماند جانت خندان تا ابد</p></div>
<div class="m2"><p>همچو جان پاک احمد با احد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان آنگه شراب جان کشند</p></div>
<div class="m2"><p>که به دست خویش خوبانشان کشند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه آن خون از پی شهوت نکرد</p></div>
<div class="m2"><p>تو رها کن بدگمانی و نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گمان بردی که کرد آلودگی</p></div>
<div class="m2"><p>در صفا غش کی هلد پالودگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر آنست این ریاضت وین جفا</p></div>
<div class="m2"><p>تا بر آرد کوره از نقره جفا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر آنست امتحان نیک و بد</p></div>
<div class="m2"><p>تا بجوشد بر سر آرد زر زبد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نبودی کارش الهام اله</p></div>
<div class="m2"><p>او سگی بودی دراننده نه شاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پاک بود از شهوت و حرص و هوا</p></div>
<div class="m2"><p>نیک کرد او لیک نیک بد نما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر خضر در بحر کشتی را شکست</p></div>
<div class="m2"><p>صد درستی در شکست خضر هست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وهم موسی با همه نور و هنر</p></div>
<div class="m2"><p>شد از آن محجوب تو بی پر مپر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن گل سرخست تو خونش مخوان</p></div>
<div class="m2"><p>مست عقلست او تو مجنونش مخوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بدی خون مسلمان کام او</p></div>
<div class="m2"><p>کافرم گر بردمی من نام او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می‌بلرزد عرش از مدح شقی</p></div>
<div class="m2"><p>بدگمان گردد ز مدحش متقی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه بود و شاه بس آگاه بود</p></div>
<div class="m2"><p>خاص بود و خاصهٔ الله بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن کسی را کش چنین شاهی کُشد</p></div>
<div class="m2"><p>سوی بخت و بهترین جاهی کِشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر ندیدی سود او در قهر او</p></div>
<div class="m2"><p>کی شدی آن لطف مطلق قهرجو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بچه می‌لرزد از آن نیش حجام</p></div>
<div class="m2"><p>مادر مشفق در آن دم شادکام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیم جان بستاند و صد جان دهد</p></div>
<div class="m2"><p>آنچ در وهمت نیاید آن دهد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو قیاس از خویش می‌گیری ولیک</p></div>
<div class="m2"><p>دور دور افتاده‌ای بنگر تو نیک</p></div></div>