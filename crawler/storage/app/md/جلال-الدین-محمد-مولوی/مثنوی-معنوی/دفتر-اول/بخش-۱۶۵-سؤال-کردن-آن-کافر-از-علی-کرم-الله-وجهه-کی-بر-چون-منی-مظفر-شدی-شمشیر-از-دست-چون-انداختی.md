---
title: >-
    بخش ۱۶۵ - سؤال کردن آن کافر از علی کرم الله وجهه کی بر چون منی مظفر شدی شمشیر از دست چون انداختی
---
# بخش ۱۶۵ - سؤال کردن آن کافر از علی کرم الله وجهه کی بر چون منی مظفر شدی شمشیر از دست چون انداختی

<div class="b" id="bn1"><div class="m1"><p>پس بگفت آن نو مسلمان ولی</p></div>
<div class="m2"><p>از سر مستی و لذت با علی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بفرما یا امیر المؤمنین</p></div>
<div class="m2"><p>تا بجنبد جان بتن در چون جنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هفت اختر هر جنین را مدتی</p></div>
<div class="m2"><p>می‌کنند ای جان به نوبت خدمتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونک وقت آید که جان گیرد جنین</p></div>
<div class="m2"><p>آفتابش آن زمان گردد معین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جنین در جنبش آید ز آفتاب</p></div>
<div class="m2"><p>کآفتابش جان همی‌بخشد شتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دگر انجم به جز نقشی نیافت</p></div>
<div class="m2"><p>این جنین تا آفتابش بر نتافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کدامین ره تعلق یافت او</p></div>
<div class="m2"><p>در رحم با آفتاب خوب‌رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ره پنهان که دور از حس ماست</p></div>
<div class="m2"><p>آفتاب چرخ را بس راههاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن رهی که زر بیابد قوت ازو</p></div>
<div class="m2"><p>و آن رهی که سنگ شد یاقوت ازو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن رهی که سرخ سازد لعل را</p></div>
<div class="m2"><p>وان رهی که برق بخشد نعل را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن رهی که پخته سازد میوه را</p></div>
<div class="m2"><p>و آن رهی که دل دهد کالیوه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازگو ای باز پر افروخته</p></div>
<div class="m2"><p>با شه و با ساعدش آموخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز گو ای بار عنقاگیر شاه</p></div>
<div class="m2"><p>ای سپاه‌اشکن بخود نه با سپاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امت وحدی یکی و صد هزار</p></div>
<div class="m2"><p>بازگو ای بنده بازت را شکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در محل قهر این رحمت ز چیست</p></div>
<div class="m2"><p>اژدها را دست دادن راه کیست</p></div></div>