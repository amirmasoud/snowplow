---
title: >-
    بخش ۳۸ - به سخن آمدن طفل درمیان آتش و تحریض کردن خلق را در افتادن بتش
---
# بخش ۳۸ - به سخن آمدن طفل درمیان آتش و تحریض کردن خلق را در افتادن بتش

<div class="b" id="bn1"><div class="m1"><p>یک زنی با طفل آورد آن جهود</p></div>
<div class="m2"><p>پیش آن بت و آتش اندر شعله بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفل ازو بستد در آتش در فکند</p></div>
<div class="m2"><p>زن بترسید و دل از ایمان بکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواست تا او سجده آرد پیش بت</p></div>
<div class="m2"><p>بانگ زد آن طفل انی لم امت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر آ ای مادر اینجا من خوشم</p></div>
<div class="m2"><p>گر چه در صورت میان آتشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم‌بندست آتش از بهر حجیب</p></div>
<div class="m2"><p>رحمتست این سر برآورده ز جیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر آ مادر ببین برهان حق</p></div>
<div class="m2"><p>تا ببینی عشرت خاصان حق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر آ و آب بین آتش‌مثال</p></div>
<div class="m2"><p>از جهانی کآتش است آبش مثال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر آ اسرار ابراهیم بین</p></div>
<div class="m2"><p>کو در آتش یافت سرو و یاسمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرگ می‌دیدم گه زادن ز تو</p></div>
<div class="m2"><p>سخت خوفم بود افتادن ز تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بزادم رستم از زندان تنگ</p></div>
<div class="m2"><p>در جهان خوش‌هوای خوب‌رنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من جهان را چون رحم دیدم کنون</p></div>
<div class="m2"><p>چون درین آتش بدیدم این سکون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندرین آتش بدیدم عالمی</p></div>
<div class="m2"><p>ذره ذره اندرو عیسی‌دمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نک جهان نیست‌شکل هست‌ذات</p></div>
<div class="m2"><p>و آن جهان هست شکل بی‌ثبات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندر آ مادر بحق مادری</p></div>
<div class="m2"><p>بین که این آذر ندارد آذری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اندر آ مادر که اقبال آمدست</p></div>
<div class="m2"><p>اندر آ مادر مده دولت ز دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قدرت آن سگ بدیدی اندر آ</p></div>
<div class="m2"><p>تا ببینی قدرت و لطف خدا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من ز رحمت می‌کشانم پای تو</p></div>
<div class="m2"><p>کز طرب خود نیستم پروای تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندر آ و دیگران را هم بخوان</p></div>
<div class="m2"><p>کاندر آتش شاه بنهادست خوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اندر آیید ای مسلمانان همه</p></div>
<div class="m2"><p>غیر عذب دین عذابست آن همه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندر آیید ای همه پروانه‌وار</p></div>
<div class="m2"><p>اندرین بهره که دارد صد بهار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بانگ می‌زد درمیان آن گروه</p></div>
<div class="m2"><p>پر همی شد جان خلقان از شکوه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خلق خود را بعد از آن بی‌خویشتن</p></div>
<div class="m2"><p>می‌فکندند اندر آتش مرد و زن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بی‌موکل بی‌کشش از عشق دوست</p></div>
<div class="m2"><p>زانک شیرین کردن هر تلخ ازوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا چنان شد کان عوانان خلق را</p></div>
<div class="m2"><p>منع می‌کردند کآتش در میا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن یهودی شد سیه‌رو و خجل</p></div>
<div class="m2"><p>شد پشیمان زین سبب بیماردل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کاندر ایمان خلق عاشق‌تر شدند</p></div>
<div class="m2"><p>در فنای جسم صادق‌تر شدند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مکر شیطان هم درو پیچید شکر</p></div>
<div class="m2"><p>دیو هم خود را سیه‌رو دید شکر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنچ می‌مالید در روی کسان</p></div>
<div class="m2"><p>جمع شد در چهرهٔ آن ناکس آن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنک می‌درید جامهٔ خلق چست</p></div>
<div class="m2"><p>شد دریده آن او ایشان درست</p></div></div>