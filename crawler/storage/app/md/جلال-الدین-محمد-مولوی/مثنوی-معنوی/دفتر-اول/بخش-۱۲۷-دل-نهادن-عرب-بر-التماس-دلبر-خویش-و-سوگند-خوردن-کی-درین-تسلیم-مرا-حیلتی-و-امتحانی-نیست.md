---
title: >-
    بخش ۱۲۷ - دل نهادن عرب بر التماس دلبر خویش و سوگند خوردن کی درین تسلیم مرا حیلتی و امتحانی نیست
---
# بخش ۱۲۷ - دل نهادن عرب بر التماس دلبر خویش و سوگند خوردن کی درین تسلیم مرا حیلتی و امتحانی نیست

<div class="b" id="bn1"><div class="m1"><p>مرد گفت اکنون گذشتم از خلاف</p></div>
<div class="m2"><p>حکم داری تیغ برکش از غلاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه گویی من ترا فرمان برم</p></div>
<div class="m2"><p>در بد و نیک آمد آن ننگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وجود تو شوم من منعدم</p></div>
<div class="m2"><p>چون محبم حب یعمی و یصم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت زن آهنگ برم می‌کنی</p></div>
<div class="m2"><p>یا بحیلت کشف سرم می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت والله عالم السر الخفی</p></div>
<div class="m2"><p>کافرید از خاک آدم را صفی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سه گز قالب که دادش وا نمود</p></div>
<div class="m2"><p>هر چه در الواح و در ارواح بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ابد هرچه بود او پیش پیش</p></div>
<div class="m2"><p>درس کرد از علم الاسماء خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ملک بی‌خود شد از تدریس او</p></div>
<div class="m2"><p>قدس دیگر یافت از تقدیس او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن گشادیشان کز آدم رو نمود</p></div>
<div class="m2"><p>در گشاد آسمانهاشان نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در فراخی عرصهٔ آن پاک جان</p></div>
<div class="m2"><p>تنگ آمد عرصهٔ هفت آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت پیغامبر که حق فرموده است</p></div>
<div class="m2"><p>من نگنجم هیچ در بالا و پست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در زمین و آسمان و عرش نیز</p></div>
<div class="m2"><p>من نگنجم این یقین دان ای عزیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دل مؤمن بگنجم ای عجب</p></div>
<div class="m2"><p>گر مرا جویی در آن دلها طلب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت ادخل فی عبادی تلتقی</p></div>
<div class="m2"><p>جنة من رویتی یا متقی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عرش با آن نور با پهنای خویش</p></div>
<div class="m2"><p>چون بدید آن را برفت از جای خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خود بزرگی عرش باشد بس مدید</p></div>
<div class="m2"><p>لیک صورت کیست چون معنی رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر ملک می‌گفت ما را پیش ازین</p></div>
<div class="m2"><p>الفتی می‌بود بر روی زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تخم خدمت بر زمین می‌کاشتیم</p></div>
<div class="m2"><p>زان تعلق ما عجب می‌داشتیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کین تعلق چیست با این خاکمان</p></div>
<div class="m2"><p>چون سرشت ما بدست از آسمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>الف ما انوار با ظلمات چیست</p></div>
<div class="m2"><p>چون تواند نور با ظلمات زیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آدما آن الف از بوی تو بود</p></div>
<div class="m2"><p>زانک جسمت را زمین بد تار و پود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جسم خاکت را ازینجا بافتند</p></div>
<div class="m2"><p>نور پاکت را درینجا یافتند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این که جان ما ز روحت یافتست</p></div>
<div class="m2"><p>پیش پیش از خاک آن می‌تافتست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در زمین بودیم و غافل از زمین</p></div>
<div class="m2"><p>غافل از گنجی که در وی بد دفین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون سفر فرمود ما را زان مقام</p></div>
<div class="m2"><p>تلخ شد ما را از آن تحویل کام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا که حجتها همی گفتیم ما</p></div>
<div class="m2"><p>که به جای ما کی آید ای خدا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نور این تسبیح و این تهلیل را</p></div>
<div class="m2"><p>می‌فروشی بهر قال و قیل را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حکم حق گسترد بهر ما بساط</p></div>
<div class="m2"><p>که بگویید ازطریق انبساط</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرچه آید بر زبانتان بی‌حذر</p></div>
<div class="m2"><p>همچو طفلان یگانه با پدر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زانک این دمها چه گر نالایقست</p></div>
<div class="m2"><p>رحمت من بر غضب هم سابقست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از پی اظهار این سبق ای ملک</p></div>
<div class="m2"><p>در تو بنهم داعیهٔ اشکال و شک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بگویی و نگیرم بر تو من</p></div>
<div class="m2"><p>منکر حلمم نیارد دم زدن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صد پدر صد مادر اندر حلم ما</p></div>
<div class="m2"><p>هر نفس زاید در افتد در فنا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حلم ایشان کف بحر حلم ماست</p></div>
<div class="m2"><p>کف رود آید ولی دریا بجاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خود چه گویم پیش آن در این صدف</p></div>
<div class="m2"><p>نیست الا کف کف کف کف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حق آن کف حق آن دریای صاف</p></div>
<div class="m2"><p>کامتحانی نیست این گفت و نه لاف</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از سر مهر و صفا است و خضوع</p></div>
<div class="m2"><p>حق آنکس که بدو دارم رجوع</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر بپیشت امتحانست این هوس</p></div>
<div class="m2"><p>امتحان را امتحان کن یک نفس</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر مپوشان تا پدید آید سرم</p></div>
<div class="m2"><p>امر کن تو هر چه بر وی قادرم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دل مپوشان تا پدید آید دلم</p></div>
<div class="m2"><p>تا قبول آرم هر آنچ قابلم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون کنم در دست من چه چاره است</p></div>
<div class="m2"><p>درنگر تا جان من چه کاره است</p></div></div>