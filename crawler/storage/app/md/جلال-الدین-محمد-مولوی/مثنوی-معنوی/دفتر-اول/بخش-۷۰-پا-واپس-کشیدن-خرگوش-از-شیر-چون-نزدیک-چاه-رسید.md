---
title: >-
    بخش ۷۰ - پا واپس کشیدن خرگوش از شیر چون نزدیک چاه رسید
---
# بخش ۷۰ - پا واپس کشیدن خرگوش از شیر چون نزدیک چاه رسید

<div class="b" id="bn1"><div class="m1"><p>چونک نزد چاه آمد شیر دید</p></div>
<div class="m2"><p>کز ره آن خرگوش ماند و پا کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت پا واپس کشیدی تو چرا</p></div>
<div class="m2"><p>پای را واپس مکش پیش اندر آ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت کو پایم که دست و پای رفت</p></div>
<div class="m2"><p>جان من لرزید و دل از جای رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ رویم را نمی‌بینی چو زر</p></div>
<div class="m2"><p>ز اندرون خود می‌دهد رنگم خبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حق چو سیما را معرف خوانده‌ست</p></div>
<div class="m2"><p>چشم عارف سوی سیما مانده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگ و بو غماز آمد چون جرس</p></div>
<div class="m2"><p>از فرس آگه کند بانگ فرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بانگ هر چیزی رساند زو خبر</p></div>
<div class="m2"><p>تا بدانی بانگ خر از بانگ در</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت پیغامبر به تمییز کسان</p></div>
<div class="m2"><p>مرء مخفی لدی طی‌اللسان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگ رو از حال دل دارد نشان</p></div>
<div class="m2"><p>رحمتم کن مهر من در دل نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رنگ روی سرخ دارد بانگ شکر</p></div>
<div class="m2"><p>بانگ روی زرد دارد صبر و نکر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در من آمد آنک دست و پا برد</p></div>
<div class="m2"><p>رنگ رو و قوت و سیما برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنک در هر چه در آید بشکند</p></div>
<div class="m2"><p>هر درخت از بیخ و بن او بر کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در من آمد آنک از وی گشت مات</p></div>
<div class="m2"><p>آدمی و جانور جامد نبات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این خود اجزا اند کلیات ازو</p></div>
<div class="m2"><p>زرد کرده رنگ و فاسد کرده بو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا جهان گه صابرست و گه شکور</p></div>
<div class="m2"><p>بوستان گه حله پوشد گاه عور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آفتابی کو بر آید نارگون</p></div>
<div class="m2"><p>ساعتی دیگر شود او سرنگون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اختران تافته بر چار طاق</p></div>
<div class="m2"><p>لحظه لحظه مبتلای احتراق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ماه کو افزود ز اختر در جمال</p></div>
<div class="m2"><p>شد ز رنج دق او همچون خیال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این زمین با سکون با ادب</p></div>
<div class="m2"><p>اندر آرد زلزله‌ش در لرز تب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای بسا که زین بلای مر دریگ</p></div>
<div class="m2"><p>گشته است اندر جهان او خرد و ریگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این هوا با روح آمد مقترن</p></div>
<div class="m2"><p>چون قضا آید وبا گشت و عفن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آب خوش کو روح را همشیره شد</p></div>
<div class="m2"><p>در غدیری زرد و تلخ و تیره شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آتشی کو باد دارد در بروت</p></div>
<div class="m2"><p>هم یکی بادی برو خواند یموت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حال دریا ز اضطراب و جوش او</p></div>
<div class="m2"><p>فهم کن تبدیلهای هوش او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرخ سرگردان که اندر جست و جوست</p></div>
<div class="m2"><p>حال او چون حال فرزندان اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گه حضیض و گه میانه گاه اوج</p></div>
<div class="m2"><p>اندرو از سعد و نحسی فوج فوج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از خود ای جزوی ز کلها مختلط</p></div>
<div class="m2"><p>فهم می‌کن حالت هر منبسط</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چونک کلیات را رنجست و درد</p></div>
<div class="m2"><p>جزو ایشان چون نباشد روی‌زرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خاصه جزوی کو ز اضدادست جمع</p></div>
<div class="m2"><p>ز آب و خاک و آتش و بادست جمع</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این عجب نبود که میش از گرگ جست</p></div>
<div class="m2"><p>این عجب کین میش دل در گرگ بست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زندگانی آشتی ضدهاست</p></div>
<div class="m2"><p>مرگ آنک اندر میانش جنگ خاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لطف حق این شیر را و گور را</p></div>
<div class="m2"><p>الف دادست این دو ضد دور را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون جهان رنجور و زندانی بود</p></div>
<div class="m2"><p>چه عجب رنجور اگر فانی بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خواند بر شیر او ازین رو پندها</p></div>
<div class="m2"><p>گفت من پس مانده‌ام زین بندها</p></div></div>