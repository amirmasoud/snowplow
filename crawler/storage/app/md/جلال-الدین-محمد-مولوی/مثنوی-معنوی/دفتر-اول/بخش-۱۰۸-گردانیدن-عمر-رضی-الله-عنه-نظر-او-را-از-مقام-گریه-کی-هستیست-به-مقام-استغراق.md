---
title: >-
    بخش ۱۰۸ - گردانیدن عمر رضی الله عنه نظر او را از مقام گریه کی هستیست به مقام استغراق
---
# بخش ۱۰۸ - گردانیدن عمر رضی الله عنه نظر او را از مقام گریه کی هستیست به مقام استغراق

<div class="b" id="bn1"><div class="m1"><p>پس عمر گفتش که این زاری تو</p></div>
<div class="m2"><p>هست هم آثار هشیاری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه فانی گشته راهی دیگرست</p></div>
<div class="m2"><p>زانک هشیاری گناهی دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست هشیاری ز یاد ما مضی</p></div>
<div class="m2"><p>ماضی و مستقبلت پردهٔ خدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش اندر زن بهر دو تا به کی</p></div>
<div class="m2"><p>پر گره باشی ازین هر دو چو نی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا گره با نی بود همراز نیست</p></div>
<div class="m2"><p>همنشین آن لب و آواز نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بطوفی خود بطوفی مرتدی</p></div>
<div class="m2"><p>چون به خانه آمدی هم با خودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خبرهات از خبرده بی‌خبر</p></div>
<div class="m2"><p>توبهٔ تو از گناه تو بتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای تو از حال گذشته توبه‌جو</p></div>
<div class="m2"><p>کی کنی توبه ازین توبه بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه بانگ زیر را قبله کنی</p></div>
<div class="m2"><p>گاه گریهٔ زار را قبله زنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونک فاروق آینهٔ اسرار شد</p></div>
<div class="m2"><p>جان پیر از اندرون بیدار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو جان بی‌گریه و بی‌خنده شد</p></div>
<div class="m2"><p>جانش رفت و جان دیگر زنده شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیرتی آمد درونش آن زمان</p></div>
<div class="m2"><p>که برون شد از زمین و آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جست و جویی از ورای جست و جو</p></div>
<div class="m2"><p>من نمی‌دانم تو می‌دانی بگو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حال و قالی از ورای حال و قال</p></div>
<div class="m2"><p>غرقه گشته در جمال ذوالجلال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غرقه‌ای نه که خلاصی باشدش</p></div>
<div class="m2"><p>یا به جز دریا کسی بشناسدش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عقل جزو از کل گویا نیستی</p></div>
<div class="m2"><p>گر تقاضا بر تقاضا نیستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون تقاضا بر تقاضا می‌رسد</p></div>
<div class="m2"><p>موج آن دریا بدینجا می‌رسد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چونک قصهٔ حال پیر اینجا رسید</p></div>
<div class="m2"><p>پیر و حالش روی در پرده کشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیر دامن را ز گفت و گو فشاند</p></div>
<div class="m2"><p>نیم گفته در دهان ما بماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از پی این عیش و عشرت ساختن</p></div>
<div class="m2"><p>صد هزاران جان بشاید باختن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در شکار بیشهٔ جان باز باش</p></div>
<div class="m2"><p>همچو خورشید جهان جان‌باز باش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان‌فشان افتاد خورشید بلند</p></div>
<div class="m2"><p>هر دمی تی می‌شود پر می‌کنند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جان فشان ای آفتاب معنوی</p></div>
<div class="m2"><p>مر جهان کهنه را بنما نوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در وجود آدمی جان و روان</p></div>
<div class="m2"><p>می‌رسد از غیب چون آب روان</p></div></div>