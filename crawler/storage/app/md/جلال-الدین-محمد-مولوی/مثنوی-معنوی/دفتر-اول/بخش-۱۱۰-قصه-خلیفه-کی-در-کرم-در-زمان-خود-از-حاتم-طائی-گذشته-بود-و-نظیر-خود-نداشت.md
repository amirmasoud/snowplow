---
title: >-
    بخش ۱۱۰ - قصهٔ خلیفه کی در کرم در زمان خود از حاتم طائی گذشته بود و نظیر خود نداشت
---
# بخش ۱۱۰ - قصهٔ خلیفه کی در کرم در زمان خود از حاتم طائی گذشته بود و نظیر خود نداشت

<div class="b" id="bn1"><div class="m1"><p>یک خلیفه بود در ایام پیش</p></div>
<div class="m2"><p>کرده حاتم را غلام جود خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رایت اکرام و داد افراشته</p></div>
<div class="m2"><p>فقر و حاجت از جهان بر داشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحر و در از بخششش صاف آمده</p></div>
<div class="m2"><p>داد او از قاف تا قاف آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان خاک ابر و آب بود</p></div>
<div class="m2"><p>مظهر بخشایش وهاب بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عطااش بحر و کان در زلزله</p></div>
<div class="m2"><p>سوی جودش قافله بر قافله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قبلهٔ حاجت در و دروازه‌اش</p></div>
<div class="m2"><p>رفته در عالم بجود آوازه‌اش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم عجم هم روم هم ترک و عرب</p></div>
<div class="m2"><p>مانده از جود و سخااش در عجب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب حیوان بود و دریای کرم</p></div>
<div class="m2"><p>زنده گشته هم عرب زو هم عجم</p></div></div>