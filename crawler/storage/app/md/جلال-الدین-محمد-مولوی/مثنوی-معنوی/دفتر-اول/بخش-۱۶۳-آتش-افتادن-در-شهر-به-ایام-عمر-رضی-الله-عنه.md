---
title: >-
    بخش ۱۶۳ - آتش افتادن در شهر به ایام عمر رضی الله عنه
---
# بخش ۱۶۳ - آتش افتادن در شهر به ایام عمر رضی الله عنه

<div class="b" id="bn1"><div class="m1"><p>آتشی افتاد در عهد عمر</p></div>
<div class="m2"><p>همچو چوب خشک می‌خورد او حجر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فتاد اندر بنا و خانه‌ها</p></div>
<div class="m2"><p>تا زد اندر پر مرغ و لانه‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم شهر از شعله‌ها آتش گرفت</p></div>
<div class="m2"><p>آب می‌ترسید از آن و می‌شکفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکهای آب و سرکه می‌زدند</p></div>
<div class="m2"><p>بر سر آتش کسان هوشمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش از استیزه افزون می‌شدی</p></div>
<div class="m2"><p>می‌رسید او را مدد از بی حدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق آمد جانب عمر شتاب</p></div>
<div class="m2"><p>کآتش ما می‌نمیرد هیچ از آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آن آتش ز آیات خداست</p></div>
<div class="m2"><p>شعله‌ای از آتش بخل شماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب و سرکه چیست نان قسمت کنید</p></div>
<div class="m2"><p>بخل بگذارید اگر آل منید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق گفتندش که در بگشوده‌ایم</p></div>
<div class="m2"><p>ما سخی و اهل فتوت بوده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت نان در رسم و عادت داده‌اید</p></div>
<div class="m2"><p>دست از بهر خدا نگشاده‌اید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر فخر و بهر بوش و بهر ناز</p></div>
<div class="m2"><p>نه از برای ترس و تقوی و نیاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مال تخمست و بهر شوره منه</p></div>
<div class="m2"><p>تیغ را در دست هر ره‌زن مده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اهل دین را باز دان از اهل کین</p></div>
<div class="m2"><p>همنشین حق بجو با او نشین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کسی بر قوم خود ایثار کرد</p></div>
<div class="m2"><p>کاغه پندارد که او خود کار کرد</p></div></div>