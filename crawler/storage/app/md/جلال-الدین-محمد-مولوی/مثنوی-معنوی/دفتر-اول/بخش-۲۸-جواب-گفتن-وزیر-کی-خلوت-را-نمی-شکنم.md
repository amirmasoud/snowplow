---
title: >-
    بخش ۲۸ - جواب گفتن وزیر کی خلوت را نمی‌شکنم
---
# بخش ۲۸ - جواب گفتن وزیر کی خلوت را نمی‌شکنم

<div class="b" id="bn1"><div class="m1"><p>گفت حجت‌های خود کوته کنید</p></div>
<div class="m2"><p>پند را در جان و در دل ره کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر امینم، متهم نبوَد امین</p></div>
<div class="m2"><p>گر بگویم آسمان را من زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کمالم، با کمال انکار چیست</p></div>
<div class="m2"><p>ور نیَم، این زحمت و آزار چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نخواهم شد ازین خلوت برون</p></div>
<div class="m2"><p>زانک مشغولم باحوال درون</p></div></div>