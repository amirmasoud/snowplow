---
title: >-
    بخش ۴۷ - ترجیح نهادن شیر جهد را بر توکل
---
# بخش ۴۷ - ترجیح نهادن شیر جهد را بر توکل

<div class="b" id="bn1"><div class="m1"><p>گفت شیر آری ولی رب العباد</p></div>
<div class="m2"><p>نردبانی پیش پای ما نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایه پایه رفت باید سوی بام</p></div>
<div class="m2"><p>هست جبری بودن اینجا طمع خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای داری چون کنی خود را تو لنگ</p></div>
<div class="m2"><p>دست داری چون کنی پنهان تو چنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواجه چون بیلی به دست بنده داد</p></div>
<div class="m2"><p>بی زبان معلوم شد او را مراد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست همچون بیل اشارتهای اوست</p></div>
<div class="m2"><p>آخراندیشی عبارتهای اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون اشارتهاش را بر جان نهی</p></div>
<div class="m2"><p>در وفای آن اشارت جان دهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس اشارتهای اسرارت دهد</p></div>
<div class="m2"><p>بار بر دارد ز تو کارت دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاملی محمول گرداند ترا</p></div>
<div class="m2"><p>قابلی مقبول گرداند ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قابل امر ویی قایل شوی</p></div>
<div class="m2"><p>وصل جویی بعد از آن واصل شوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعی شکر نعمتش قدرت بود</p></div>
<div class="m2"><p>جبر تو انکار آن نعمت بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکر قدرت قدرتت افزون کند</p></div>
<div class="m2"><p>جبر نعمت از کفت بیرون کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جبر تو خفتن بود در ره مخسپ</p></div>
<div class="m2"><p>تا نبینی آن در و درگه مخسپ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هان مخسپ ای کاهل بی‌اعتبار</p></div>
<div class="m2"><p>جز به زیر آن درخت میوه‌دار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا که شاخ افشان کند هر لحظه باد</p></div>
<div class="m2"><p>بر سر خفته بریزد نقل و زاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جبر و خفتن درمیان ره‌زنان</p></div>
<div class="m2"><p>مرغ بی‌هنگام کی یابد امان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور اشارتهاش را بینی زنی</p></div>
<div class="m2"><p>مرد پنداری و چون بینی زنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این قدر عقلی که داری گم شود</p></div>
<div class="m2"><p>سر که عقل از وی بپرد دم شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زانک بی‌شکری بود شوم و شنار</p></div>
<div class="m2"><p>می‌برد بی‌شکر را در قعر نار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر توکل می‌کنی در کار کن</p></div>
<div class="m2"><p>کشت کن پس تکیه بر جبار کن</p></div></div>