---
title: >-
    بخش ۴۰ - عتاب کردن آتش را آن پادشاه جهود
---
# بخش ۴۰ - عتاب کردن آتش را آن پادشاه جهود

<div class="b" id="bn1"><div class="m1"><p>رو به آتش کرد شه «کای تندخو</p></div>
<div class="m2"><p>آن جهان سوز طبیعی خوت کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نمی‌سوزی چه شد خاصیتت؟</p></div>
<div class="m2"><p>یا ز بخت ما دگر شد نیتت؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌نبخشایی تو بر آتش‌پرست؛</p></div>
<div class="m2"><p>آنک نپرستد ترا، او چون برست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز ای آتش تو صابر نیستی</p></div>
<div class="m2"><p>چون نسوزی؟ چیست؟ قادر نیستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم‌بندست این عجب، یا هوش‌بند؟</p></div>
<div class="m2"><p>چون نسوزاند چنین شعلهٔ بلند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جادوی کردت کسی یا سیمیاست؟</p></div>
<div class="m2"><p>یا خلاف طبع تو از بخت ماست؟»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آتش من همانم ای شمن</p></div>
<div class="m2"><p>اندر آ تا تو ببینی تاب من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبع من دیگر نگشت و عنصرم</p></div>
<div class="m2"><p>تیغ حقم هم بدستوری برم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر در خرگهٔ سگان ترکمان</p></div>
<div class="m2"><p>چاپلوسی کرده پیش میهمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور بخرگه بگذرد بیگانه‌رو</p></div>
<div class="m2"><p>حمله بیند از سگان شیرانه او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ز سگ کم نیستم در بندگی</p></div>
<div class="m2"><p>کم ز ترکی نیست حق در زندگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آتش طبعت اگر غمگین کند</p></div>
<div class="m2"><p>سوزش از امر ملیک دین کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتش طبعت اگر شادی دهد</p></div>
<div class="m2"><p>اندرو شادی ملیک دین نهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چونک غم‌بینی تو استغفار کن</p></div>
<div class="m2"><p>غم بامر خالق آمد کار کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون بخواهد عین غم شادی شود</p></div>
<div class="m2"><p>عین بند پای آزادی شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باد و خاک و آب و آتش بنده‌اند</p></div>
<div class="m2"><p>با من و تو مرده با حق زنده‌اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش حق آتش همیشه در قیام</p></div>
<div class="m2"><p>همچو عاشق روز و شب پیچان مدام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سنگ بر آهن زنی بیرون جهد</p></div>
<div class="m2"><p>هم به امر حق قدم بیرون نهد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آهن و سنگ هوا بر هم مزن</p></div>
<div class="m2"><p>کین دو می‌زایند همچون مرد و زن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سنگ و آهن خود سبب آمد ولیک</p></div>
<div class="m2"><p>تو به بالاتر نگر ای مرد نیک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کین سبب را آن سبب آورد پیش</p></div>
<div class="m2"><p>بی‌سبب کی شد سبب هرگز ز خویش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و آن سببها کانبیا را رهبرند</p></div>
<div class="m2"><p>آن سببها زین سببها برترند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این سبب را آن سبب عامل کند</p></div>
<div class="m2"><p>باز گاهی بی بر و عاطل کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این سبب را محرم آمد عقلها</p></div>
<div class="m2"><p>و آن سببهاراست محرم انبیا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این سبب چه بود بتازی گو رسن</p></div>
<div class="m2"><p>اندرین چه این رسن آمد بفن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گردش چرخه رسن را علتست</p></div>
<div class="m2"><p>چرخه گردان را ندیدن زلتست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این رسنهای سببها در جهان</p></div>
<div class="m2"><p>هان و هان زین چرخ سرگردان مدان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا نمانی صفر و سرگردان چو چرخ</p></div>
<div class="m2"><p>تا نسوزی تو ز بی‌مغزی چو مرخ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باد آتش می‌شود از امر حق</p></div>
<div class="m2"><p>هر دو سرمست آمدند از خمر حق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آب حلم و آتش خشم ای پسر</p></div>
<div class="m2"><p>هم ز حق بینی چو بگشایی بصر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر نبودی واقف از حق جان باد</p></div>
<div class="m2"><p>فرق کی کردی میان قوم عاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هود گرد مؤمنان خطی کشید</p></div>
<div class="m2"><p>نرم می‌شد باد کانجا می‌رسید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر که بیرون بود زان خط جمله را</p></div>
<div class="m2"><p>پاره پاره می‌گسست اندر هوا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همچنین شیبان راعی می‌کشید</p></div>
<div class="m2"><p>گرد بر گرد رمه خطی پدید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون بجمعه می‌شد او وقت نماز</p></div>
<div class="m2"><p>تا نیارد گرگ آنجا ترک‌تاز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هیچ گرگی در نرفتی اندر آن</p></div>
<div class="m2"><p>گوسفندی هم نگشتی زان نشان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باد حرص گرگ و حرص گوسفند</p></div>
<div class="m2"><p>دایرهٔ مرد خدا را بود بند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همچنین باد اجل با عارفان</p></div>
<div class="m2"><p>نرم و خوش همچون نسیم یوسفان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آتش ابراهیم را دندان نزد</p></div>
<div class="m2"><p>چون گزیدهٔ حق بُوَد چونش گَزَد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز آتش شهوت نسوزد اهل دین</p></div>
<div class="m2"><p>باقیان را برده تا قعر زمین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>موج دریا چون بامر حق بتاخت</p></div>
<div class="m2"><p>اهل موسی را ز قبطی وا شناخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خاک، قارون را چو فرمان در رسید؛</p></div>
<div class="m2"><p>با زر و تختش به قعر خود کشید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آب و گل چون از دم عیسی چرید</p></div>
<div class="m2"><p>بال و پر بگشاد، مرغی شد پّرید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هست تسبیحت بخار آب و گل</p></div>
<div class="m2"><p>مرغ جنت شد ز نفخ صدق دل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کوه طور از نور موسی شد به رقص</p></div>
<div class="m2"><p>صوفی کامل شدُ و رَست او ز نقص</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه عجب گر کوه صوفی شد عزیز</p></div>
<div class="m2"><p>جسم موسی از کلوخی بود نیز</p></div></div>