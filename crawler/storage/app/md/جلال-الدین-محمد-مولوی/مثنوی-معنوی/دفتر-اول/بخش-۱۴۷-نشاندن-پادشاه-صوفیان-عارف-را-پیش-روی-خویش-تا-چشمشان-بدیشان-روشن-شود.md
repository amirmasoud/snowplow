---
title: >-
    بخش ۱۴۷ - نشاندن پادشاه صوفیان عارف را پیش روی خویش تا چشمشان بدیشان روشن شود
---
# بخش ۱۴۷ - نشاندن پادشاه صوفیان عارف را پیش روی خویش تا چشمشان بدیشان روشن شود

<div class="b" id="bn1"><div class="m1"><p>پادشاهان را چنان عادت بود</p></div>
<div class="m2"><p>این شنیده باشی ار یادت بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست چپشان پهلوانان ایستند</p></div>
<div class="m2"><p>زانک دل پهلوی چپ باشد ببند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشرف و اهل قلم بر دست راست</p></div>
<div class="m2"><p>زانک علم خط و ثبت آن دست راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صوفیان را پیش رو موضع دهند</p></div>
<div class="m2"><p>کاینهٔ جانند و ز آیینه بهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه صیقلها زده در ذکر و فکر</p></div>
<div class="m2"><p>تا پذیرد آینهٔ دل نقش بکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که او از صلب فطرت خوب زاد</p></div>
<div class="m2"><p>آینه در پیش او باید نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق آیینه باشد روی خوب</p></div>
<div class="m2"><p>صیقل جان آمد و تقوی القلوب</p></div></div>