---
title: >-
    بخش ۱۴۱ - کبودی زدن قزوینی بر شانه‌گاه صورت شیر و پشیمان شدن او به سبب زخم سوزن
---
# بخش ۱۴۱ - کبودی زدن قزوینی بر شانه‌گاه صورت شیر و پشیمان شدن او به سبب زخم سوزن

<div class="b" id="bn1"><div class="m1"><p>این حکایت بشنو از صاحب بیان</p></div>
<div class="m2"><p>در طریق و عادت قزوینیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تن و دست و کتفها بی‌گزند</p></div>
<div class="m2"><p>از سر سوزن کبودیها زنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی دلاکی بشد قزوینیی</p></div>
<div class="m2"><p>که کبودم زن بکن شیرینیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت چه صورت زنم ای پهلوان</p></div>
<div class="m2"><p>گفت بر زن صورت شیر ژیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طالعم شیرست نقش شیر زن</p></div>
<div class="m2"><p>جهد کن رنگ کبودی سیر زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت بر چه موضعت صورت زنم</p></div>
<div class="m2"><p>گفت بر شانه گهم زن آن رقم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونک او سوزن فرو بردن گرفت</p></div>
<div class="m2"><p>درد آن در شانه‌گه مسکن گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پهلوان در ناله آمد کای سنی</p></div>
<div class="m2"><p>مر مرا کشتی چه صورت می‌زنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت آخر شیر فرمودی مرا</p></div>
<div class="m2"><p>گفت از چه عضو کردی ابتدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت از دمگاه آغازیده‌ام</p></div>
<div class="m2"><p>گفت دم بگذار ای دو دیده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دم و دمگاه شیرم دم گرفت</p></div>
<div class="m2"><p>دمگه او دمگهم محکم گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیر بی‌دم باش گو ای شیرساز</p></div>
<div class="m2"><p>که دلم سستی گرفت از زخم گاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جانب دیگر گرفت آن شخص زخم</p></div>
<div class="m2"><p>بی‌محابا و مواسایی و رحم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بانگ کرد او کین چه اندامست ازو</p></div>
<div class="m2"><p>گفت این گوشست ای مرد نکو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت تا گوشش نباشد ای حکیم</p></div>
<div class="m2"><p>گوش را بگذار و کوته کن گلیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جانب دیگر خلش آغاز کرد</p></div>
<div class="m2"><p>باز قزوینی فغان را ساز کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کین سوم جانب چه اندامست نیز</p></div>
<div class="m2"><p>گفت اینست اشکم شیر ای عزیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت تا اشکم نباشد شیر را</p></div>
<div class="m2"><p>گشت افزون درد کم زن زخمها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خیره شد دلاک و پس حیران بماند</p></div>
<div class="m2"><p>تا بدیر انگشت در دندان بماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر زمین زد سوزن از خشم اوستاد</p></div>
<div class="m2"><p>گفت در عالم کسی را این فتاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شیر بی‌دم و سر و اشکم کی دید</p></div>
<div class="m2"><p>این‌چنین شیری خدا خود نافرید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای برادر صبر کن بر درد نیش</p></div>
<div class="m2"><p>تا رهی از نیش نفس گبر خویش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کان گروهی که رهیدند از وجود</p></div>
<div class="m2"><p>چرخ و مهر و ماهشان آرد سجود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که مرد اندر تن او نفس گبر</p></div>
<div class="m2"><p>مر ورا فرمان برد خورشید و ابر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون دلش آموخت شمع افروختن</p></div>
<div class="m2"><p>آفتاب او را نیارد سوختن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت حق در آفتاب منتجم</p></div>
<div class="m2"><p>ذکر تزاور کذی عن کهفهم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خار جمله لطف چون گل می‌شود</p></div>
<div class="m2"><p>پیش جزوی کو سوی کل می‌رود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چیست تعظیم خدا افراشتن</p></div>
<div class="m2"><p>خویشتن را خوار و خاکی داشتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چیست توحید خدا آموختن</p></div>
<div class="m2"><p>خویشتن را پیش واحد سوختن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر همی‌خواهی که بفروزی چو روز</p></div>
<div class="m2"><p>هستی همچون شب خود را بسوز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هستیت در هست آن هستی‌نواز</p></div>
<div class="m2"><p>همچو مس در کیمیا اندر گداز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در من و ما سخت کردستی دو دست</p></div>
<div class="m2"><p>هست این جمله خرابی از دو هست</p></div></div>