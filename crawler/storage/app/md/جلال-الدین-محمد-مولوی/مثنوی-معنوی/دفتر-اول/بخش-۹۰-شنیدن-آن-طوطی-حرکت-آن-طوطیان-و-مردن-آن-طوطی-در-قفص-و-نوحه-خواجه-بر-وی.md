---
title: >-
    بخش ۹۰ - شنیدن آن طوطی حرکت آن طوطیان و مردن آن طوطی در قفص و نوحهٔ خواجه بر وی
---
# بخش ۹۰ - شنیدن آن طوطی حرکت آن طوطیان و مردن آن طوطی در قفص و نوحهٔ خواجه بر وی

<div class="b" id="bn1"><div class="m1"><p>چون شنید آن مرغ کان طوطی چه کرد</p></div>
<div class="m2"><p>پس بلرزید اوفتاد و گشت سرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواجه چون دیدش فتاده همچنین</p></div>
<div class="m2"><p>بر جهید و زد کله را بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بدین رنگ و بدین حالش بدید</p></div>
<div class="m2"><p>خواجه بر جست و گریبان را درید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ای طوطی خوب خوش‌حنین</p></div>
<div class="m2"><p>این چه بودت این چرا گشتی چنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دریغا مرغ خوش‌آواز من</p></div>
<div class="m2"><p>ای دریغا همدم و همراز من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دریغا مرغ خوش‌الحان من</p></div>
<div class="m2"><p>راح روح و روضه و ریحان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سلیمان را چنین مرغی بدی</p></div>
<div class="m2"><p>کی خود او مشغول آن مرغان شدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دریغا مرغ کارزان یافتم</p></div>
<div class="m2"><p>زود روی از روی او بر تافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای زبان تو بس زیانی بر وری</p></div>
<div class="m2"><p>چون توی گویا چه گویم من ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای زبان هم آتش و هم خرمنی</p></div>
<div class="m2"><p>چند این آتش درین خرمن زنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در نهان جان از تو افغان می‌کند</p></div>
<div class="m2"><p>گرچه هر چه گوییش آن می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای زبان هم گنج بی‌پایان توی</p></div>
<div class="m2"><p>ای زبان هم رنج بی‌درمان توی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم صفیر و خدعهٔ مرغان توی</p></div>
<div class="m2"><p>هم انیس وحشت هجران توی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند امانم می‌دهی ای بی امان</p></div>
<div class="m2"><p>ای تو زه کرده به کین من کمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نک بپرانیده‌ای مرغ مرا</p></div>
<div class="m2"><p>در چراگاه ستم کم کن چرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا جواب من بگو یا داد ده</p></div>
<div class="m2"><p>یا مرا ز اسباب شادی یاد ده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای دریغا نور ظلمت‌سوز من</p></div>
<div class="m2"><p>ای دریغا صبح روز افروز من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای دریغا مرغ خوش‌پرواز من</p></div>
<div class="m2"><p>ز انتها پریده تا آغاز من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عاشق رنجست نادان تا ابد</p></div>
<div class="m2"><p>خیز لا اقسم بخوان تا فی کبد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از کبد فارغ بدم با روی تو</p></div>
<div class="m2"><p>وز زبد صافی بدم در جوی تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این دریغاها خیال دیدنست</p></div>
<div class="m2"><p>وز وجود نقد خود ببریدنست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غیرت حق بود و با حق چاره نیست</p></div>
<div class="m2"><p>کو دلی کز عشق حق صد پاره نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غیرت آن باشد که او غیر همه‌ست</p></div>
<div class="m2"><p>آنک افزون از بیان و دمدمه‌ست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای دریغا اشک من دریا بدی</p></div>
<div class="m2"><p>تا نثار دلبر زیبا بدی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طوطی من مرغ زیرکسار من</p></div>
<div class="m2"><p>ترجمان فکرت و اسرار من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرچه روزی داد و ناداد آیدم</p></div>
<div class="m2"><p>او ز اول گفته تا یاد آیدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طوطیی کآید ز وحی آواز او</p></div>
<div class="m2"><p>پیش از آغاز وجود آغاز او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اندرون تست آن طوطی نهان</p></div>
<div class="m2"><p>عکس او را دیده تو بر این و آن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>می برد شادیت را تو شاد ازو</p></div>
<div class="m2"><p>می‌پذیری ظلم را چون داد ازو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای که جان را بهر تن می‌سوختی</p></div>
<div class="m2"><p>سوختی جان را و تن افروختی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سوختم من سوخته خواهد کسی</p></div>
<div class="m2"><p>تا زمن آتش زند اندر خسی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سوخته چون قابل آتش بود</p></div>
<div class="m2"><p>سوخته بستان که آتش‌کش بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای دریغا ای دریغا ای دریغ</p></div>
<div class="m2"><p>کانچنان ماهی نهان شد زیر میغ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون زنم دم کآتش دل تیز شد</p></div>
<div class="m2"><p>شیر هجر آشفته و خون‌ریز شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنک او هشیار خود تندست و مست</p></div>
<div class="m2"><p>چون بود چون او قدح گیرد به دست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شیر مستی کز صفت بیرون بود</p></div>
<div class="m2"><p>از بسیط مرغزار افزون بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قافیه‌اندیشم و دلدار من</p></div>
<div class="m2"><p>گویدم مندیش جز دیدار من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خوش نشین ای قافیه‌اندیش من</p></div>
<div class="m2"><p>قافیهٔ دولت توی در پیش من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حرف چه بود تا تو اندیشی از آن</p></div>
<div class="m2"><p>حرف چه بود خار دیوار رزان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حرف و صوت و گفت را بر هم زنم</p></div>
<div class="m2"><p>تا که بی این هر سه با تو دم زنم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن دمی کز آدمش کردم نهان</p></div>
<div class="m2"><p>با تو گویم ای تو اسرار جهان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن دمی را که نگفتم با خلیل</p></div>
<div class="m2"><p>و آن غمی را که نداند جبرئیل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آن دمی کز وی مسیحا دم نزد</p></div>
<div class="m2"><p>حق ز غیرت نیز بی ما هم نزد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ما چه باشد در لغت اثبات و نفی</p></div>
<div class="m2"><p>من نه اثباتم منم بی‌ذات و نفی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من کسی در ناکسی در یافتم</p></div>
<div class="m2"><p>پس کسی در ناکسی در بافتم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جمله شاهان بندهٔ بندهٔ خودند</p></div>
<div class="m2"><p>جمله خلقان مردهٔ مردهٔ خودند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جمله شاهان پست پست خویش را</p></div>
<div class="m2"><p>جمله خلقان مست مست خویش را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>می‌شود صیاد مرغان را شکار</p></div>
<div class="m2"><p>تا کند ناگاه ایشان را شکار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بی‌دلان را دلبران جسته بجان</p></div>
<div class="m2"><p>جمله معشوقان شکار عاشقان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر که عاشق دیدیش معشوق دان</p></div>
<div class="m2"><p>کو به نسبت هست هم این و هم آن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تشنگان گر آب جویند از جهان</p></div>
<div class="m2"><p>آب جوید هم به عالم تشنگان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چونک عاشق اوست تو خاموش باش</p></div>
<div class="m2"><p>او چو گوشت می‌کشد تو گوش باش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بند کن چون سیل سیلانی کند</p></div>
<div class="m2"><p>ور نه رسوایی و ویرانی کند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>من چه غم دارم که ویرانی بود</p></div>
<div class="m2"><p>زیر ویران گنج سلطانی بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>غرق حق خواهد که باشد غرق‌تر</p></div>
<div class="m2"><p>همچو موج بحر جان زیر و زبر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زیر دریا خوشتر آید یا زبر</p></div>
<div class="m2"><p>تیر او دلکش‌تر آید یا سپر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پاره کردهٔ وسوسه باشی دلا</p></div>
<div class="m2"><p>گر طرب را باز دانی از بلا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر مرادت را مذاق شکرست</p></div>
<div class="m2"><p>بی‌مرادی نه مراد دلبرست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر ستاره‌ش خونبهای صد هلال</p></div>
<div class="m2"><p>خون عالم ریختن او را حلال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ما بها و خونبها را یافتیم</p></div>
<div class="m2"><p>جانب جان باختن بشتافتیم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ای حیات عاشقان در مردگی</p></div>
<div class="m2"><p>دل نیابی جز که در دل‌بردگی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من دلش جسته به صد ناز و دلال</p></div>
<div class="m2"><p>او بهانه کرده با من از ملال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گفتم آخر غرق تست این عقل و جان</p></div>
<div class="m2"><p>گفت رو رو بر من این افسون مخوان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>من ندانم آنچ اندیشیده‌ای</p></div>
<div class="m2"><p>ای دو دیده دوست را چون دیده‌ای</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای گرانجان خوار دیدستی ورا</p></div>
<div class="m2"><p>زانک بس ارزان خریدستی ورا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هرکه او ارزان خرد ارزان دهد</p></div>
<div class="m2"><p>گوهری طفلی به قرصی نان دهد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>غرق عشقی‌ام که غرقست اندرین</p></div>
<div class="m2"><p>عشقهای اولین و آخرین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مجملش گفتم نکردم زان بیان</p></div>
<div class="m2"><p>ورنه هم افهام سوزد هم زبان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>من چو لب گویم لب دریا بود</p></div>
<div class="m2"><p>من چو لا گویم مراد الا بود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>من ز شیرینی نشستم رو ترش</p></div>
<div class="m2"><p>من ز بسیاری گفتارم خمش</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تا که شیرینی ما از دو جهان</p></div>
<div class="m2"><p>در حجاب رو ترش باشد نهان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تا که در هر گوش ناید این سخن</p></div>
<div class="m2"><p>یک همی گویم ز صد سر لدن</p></div></div>