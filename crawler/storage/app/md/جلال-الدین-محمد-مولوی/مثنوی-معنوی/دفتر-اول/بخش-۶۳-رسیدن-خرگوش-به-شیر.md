---
title: >-
    بخش ۶۳ - رسیدن خرگوش به شیر
---
# بخش ۶۳ - رسیدن خرگوش به شیر

<div class="b" id="bn1"><div class="m1"><p>شیر اندر آتش و در خشم و شور</p></div>
<div class="m2"><p>دید کان خرگوش می‌آید ز دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌دود بی‌دهشت و گستاخ او</p></div>
<div class="m2"><p>خشمگین و تند و تیز و ترش‌رو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز شکسته آمدن تهمت بود</p></div>
<div class="m2"><p>وز دلیری دفع هر ریبت بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون رسید او پیشتر نزدیک صف</p></div>
<div class="m2"><p>بانگ بر زد شیرهای ای ناخلف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که پیلان را ز هم بدریده‌ام</p></div>
<div class="m2"><p>من که گوش شیر نر مالیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیم خرگوشی که باشد که چنین</p></div>
<div class="m2"><p>امر ما را افکند او بر زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترک خواب غفلت خرگوش کن</p></div>
<div class="m2"><p>غرهٔ این شیر ای خرگوش کن</p></div></div>