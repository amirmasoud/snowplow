---
title: >-
    بخش ۲۴ - بیان خسارت وزیر درین مکر
---
# بخش ۲۴ - بیان خسارت وزیر درین مکر

<div class="b" id="bn1"><div class="m1"><p>همچو شه نادان و غافل بد وزیر</p></div>
<div class="m2"><p>پنجه می‌زد با قدیم ناگزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چنان قادر خدایی کز عدم</p></div>
<div class="m2"><p>صد چو عالم هست گرداند بدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد چو عالم در نظر پیدا کند</p></div>
<div class="m2"><p>چونک چشمت را به خود بینا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جهان پیشت بزرگ و بی‌بنیست</p></div>
<div class="m2"><p>پیش قدرت ذره‌ای می‌دان که نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جهان خود حبس جانهای شماست</p></div>
<div class="m2"><p>هین روید آن سو که صحرای شماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جهان محدود و آن خود بی‌حدست</p></div>
<div class="m2"><p>نقش و صورت پیش آن معنی سدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد هزاران نیزهٔ فرعون را</p></div>
<div class="m2"><p>در شکست از موسی با یک عصا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد هزاران طب جالینوس بود</p></div>
<div class="m2"><p>پیش عیسی و دمش افسوس بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد هزاران دفتر اشعار بود</p></div>
<div class="m2"><p>پیش حرف امیی‌اش عار بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با چنین غالب خداوندی کسی</p></div>
<div class="m2"><p>چون نمیرد گر نباشد او خسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس دل چون کوه را انگیخت او</p></div>
<div class="m2"><p>مرغ زیرک با دو پا آویخت او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فهم و خاطر تیز کردن نیست راه</p></div>
<div class="m2"><p>جز شکسته می‌نگیرد فضل شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بسا گنج آگنان کنج‌کاو</p></div>
<div class="m2"><p>کان خیال‌اندیش را شد ریش گاو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاو که بود تا تو ریش او شوی</p></div>
<div class="m2"><p>خاک چه بود تا حشیش او شوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون زنی از کار بد شد روی زرد</p></div>
<div class="m2"><p>مسخ کرد او را خدا و زهره کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عورتی را زهره کردن مسخ بود</p></div>
<div class="m2"><p>خاک و گل گشتن نه مسخست ای عنود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روح می‌بردت سوی چرخ برین</p></div>
<div class="m2"><p>سوی آب و گل شدی در اسفلین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خویشتن را مسخ کردی زین سفول</p></div>
<div class="m2"><p>زان وجودی که بد آن رشک عقول</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس ببین کین مسخ کردن چون بود</p></div>
<div class="m2"><p>پیش آن مسخ این به غایت دون بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اسپ همت سوی اختر تاختی</p></div>
<div class="m2"><p>آدم مسجود را نشناختی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آخر آدم‌زاده‌ای ای ناخلف</p></div>
<div class="m2"><p>چند پنداری تو پستی را شرف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چند گویی من بگیرم عالمی</p></div>
<div class="m2"><p>این جهان را پر کنم از خود همی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر جهان پر برف گردد سربسر</p></div>
<div class="m2"><p>تاب خور بگدازدش با یک نظر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وزر او و صد وزیر و صدهزار</p></div>
<div class="m2"><p>نیست گرداند خدا از یک شرار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عین آن تخییل را حکمت کند</p></div>
<div class="m2"><p>عین آن زهراب را شربت کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن گمان‌انگیز را سازد یقین</p></div>
<div class="m2"><p>مهرها رویاند از اسباب کین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پرورد در آتش ابراهیم را</p></div>
<div class="m2"><p>ایمنی روح سازد بیم را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از سبب سوزیش من سوداییم</p></div>
<div class="m2"><p>در خیالاتش چو سوفسطاییم</p></div></div>