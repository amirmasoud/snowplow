---
title: >-
    بخش ۳۶ - حکایت پادشاه جهود دیگر کی در هلاک دین عیسی سعی نمود
---
# بخش ۳۶ - حکایت پادشاه جهود دیگر کی در هلاک دین عیسی سعی نمود

<div class="b" id="bn1"><div class="m1"><p>یک شه دیگر ز نسل آن جهود</p></div>
<div class="m2"><p>در هلاک قوم عیسی رو نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خبر خواهی ازین دیگر خروج</p></div>
<div class="m2"><p>سوره بر خوان واسما ذات البروج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنت بد کز شه اول بزاد</p></div>
<div class="m2"><p>این شه دیگر قدم بر وی نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که او بنهاد ناخوش سنتی</p></div>
<div class="m2"><p>سوی او نفرین رود هر ساعتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیکوان رفتند و سنتها بماند</p></div>
<div class="m2"><p>وز لئیمان ظلم و لعنتها بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا قیامت هرکه جنس آن بدان</p></div>
<div class="m2"><p>در وجود آید بود رویش بدان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رگ رگست این آب شیرین و آب شور</p></div>
<div class="m2"><p>در خلایق می‌رود تا نفخ صور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیکوان را هست میراث از خوشاب</p></div>
<div class="m2"><p>آن چه میراثست اورثنا الکتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد نیاز طالبان ار بنگری</p></div>
<div class="m2"><p>شعله‌ها از گوهر پیغامبری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعله‌ها با گوهران گردان بود</p></div>
<div class="m2"><p>شعله آن جانب رود هم کان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نور روزن گرد خانه می‌دود</p></div>
<div class="m2"><p>زانک خور برجی به برجی می‌رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که را با اختری پیوستگیست</p></div>
<div class="m2"><p>مر ورا با اختر خود هم‌تگیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طالعش گر زهره باشد در طرب</p></div>
<div class="m2"><p>میل کلی دارد و عشق و طلب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور بود مریخی خون‌ریزخو</p></div>
<div class="m2"><p>جنگ و بهتان و خصومت جوید او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اخترانند از ورای اختران</p></div>
<div class="m2"><p>که احتراق و نحس نبود اندر آن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سایران در آسمانهای دگر</p></div>
<div class="m2"><p>غیر این هفت آسمان معتبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راسخان در تاب انوار خدا</p></div>
<div class="m2"><p>نه به هم پیوسته نه از هم جدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که باشد طالع او زان نجوم</p></div>
<div class="m2"><p>نفس او کفار سوزد در رجوم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خشمِ مریخی نباشد خشم او</p></div>
<div class="m2"><p>منقلب رو غالب و مغلوب خو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نور غالب ایمن از نقص و غسق</p></div>
<div class="m2"><p>درمیان اصبعین نور حق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حق فشاند آن نور را بر جانها</p></div>
<div class="m2"><p>مقبلان بر داشته دامانها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و آن نثار نور را وا یافته</p></div>
<div class="m2"><p>روی از غیر خدا برتافته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که را دامان عشقی نابده</p></div>
<div class="m2"><p>زان نثار نور بی بهره شده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جزوها را رویها سوی کلست</p></div>
<div class="m2"><p>بلبلان را عشق با روی گلست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاو را رنگ از برون و مرد را</p></div>
<div class="m2"><p>از درون جو رنگ سرخ و زرد را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رنگهای نیک از خم صفاست</p></div>
<div class="m2"><p>رنگ زشتان از سیاهابهٔ جفاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صبغة الله نام آن رنگ لطیف</p></div>
<div class="m2"><p>لعنة الله بوی این رنگ کثیف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنچ از دریا به دریا می‌رود</p></div>
<div class="m2"><p>از همانجا کامد آنجا می‌رود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از سرِ کُه سیلهای تیزرو</p></div>
<div class="m2"><p>وز تن ما جان عشق آمیز رو</p></div></div>