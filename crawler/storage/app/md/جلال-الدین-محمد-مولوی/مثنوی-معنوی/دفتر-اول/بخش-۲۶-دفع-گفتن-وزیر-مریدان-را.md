---
title: >-
    بخش ۲۶ - دفع گفتن وزیر مریدان را
---
# بخش ۲۶ - دفع گفتن وزیر مریدان را

<div class="b" id="bn1"><div class="m1"><p>گفت هان ای سخرگان گفت و گو</p></div>
<div class="m2"><p>وعظ و گفتار زبان و گوش جو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنبه اندر گوش حس دون کنید</p></div>
<div class="m2"><p>بند حس از چشم خود بیرون کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنبهٔ آن گوش سر گوش سرست</p></div>
<div class="m2"><p>تا نگردد این کر آن باطن کرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌حس و بی‌گوش و بی‌فکرت شوید</p></div>
<div class="m2"><p>تا خطاب ارجعی را بشنوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به گفت و گوی بیداری دری</p></div>
<div class="m2"><p>تو زگفت خواب بویی کی بری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر بیرونیست قول و فعل ما</p></div>
<div class="m2"><p>سیر باطن هست بالای سما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حس خشکی دید کز خشکی بزاد</p></div>
<div class="m2"><p>عیسی جان پای بر دریا نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیر جسم خشک بر خشکی فتاد</p></div>
<div class="m2"><p>سیر جان پا در دل دریا نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونک عمر اندر ره خشکی گذشت</p></div>
<div class="m2"><p>گاه کوه و گاه دریا گاه دشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب حیوان از کجا خواهی تو یافت</p></div>
<div class="m2"><p>موج دریا را کجا خواهی شکافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موج خاکی وهم و فهم و فکر ماست</p></div>
<div class="m2"><p>موج آبی محو و سکرست و فناست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا درین سکری از آن سکری تو دور</p></div>
<div class="m2"><p>تا ازین مستی از آن جامی نفور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت و گوی ظاهر آمد چون غبار</p></div>
<div class="m2"><p>مدتی خاموش خو کن هوش‌دار</p></div></div>