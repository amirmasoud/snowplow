---
title: >-
    بخش ۱۶۹ - بازگشتن به حکایت علی کرم الله وجهه و مسامحت کردن او با خونی خویش
---
# بخش ۱۶۹ - بازگشتن به حکایت علی کرم الله وجهه و مسامحت کردن او با خونی خویش

<div class="b" id="bn1"><div class="m1"><p>باز رو سوی علی و خونیش</p></div>
<div class="m2"><p>وان کرم با خونی و افزونیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت دشمن را همی‌بینم به چشم</p></div>
<div class="m2"><p>روز و شب بر وی ندارم هیچ خشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانک مرگم همچو من خوش آمدست</p></div>
<div class="m2"><p>مرگ من در بعث چنگ اندر زدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرگ بی مرگی بود ما را حلال</p></div>
<div class="m2"><p>برگ بی برگی بود ما را نوال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظاهرش مرگ و به باطن زندگی</p></div>
<div class="m2"><p>ظاهرش ابتر نهان پایندگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در رحم زادن جنین را رفتنست</p></div>
<div class="m2"><p>در جهان او را ز نو بشکفتنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون مرا سوی اجل عشق و هواست</p></div>
<div class="m2"><p>نهی لا تلقوا بایدیکم مراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زانک نهی از دانهٔ شیرین بود</p></div>
<div class="m2"><p>تلخ را خود نهی حاجت کی شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانه‌ای کش تلخ باشد مغز و پوست</p></div>
<div class="m2"><p>تلخی و مکروهیش خود نهی اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانهٔ مردن مرا شیرین شدست</p></div>
<div class="m2"><p>بل هم احیاء پی من آمدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اقتلونی یا ثقاتی لائما</p></div>
<div class="m2"><p>ان فی قتلی حیاتی دائما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ان فی موتی حیاتی یا فتی</p></div>
<div class="m2"><p>کم افارق موطنی حتی متی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرقتی لو لم تکن فی ذا السکون</p></div>
<div class="m2"><p>لم یقل انا الیه راجعون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راجع آن باشد که باز آید به شهر</p></div>
<div class="m2"><p>سوی وحدت آید از تفریق دهر</p></div></div>