---
title: >-
    بخش ۱۵۰ - مرتد شدن کاتب وحی به سبب آنک پرتو وحی برو زد آن آیت را پیش از پیغامبر صلی الله علیه و سلم بخواند گفت پس من هم محل وحیم
---
# بخش ۱۵۰ - مرتد شدن کاتب وحی به سبب آنک پرتو وحی برو زد آن آیت را پیش از پیغامبر صلی الله علیه و سلم بخواند گفت پس من هم محل وحیم

<div class="b" id="bn1"><div class="m1"><p>پیش از عثمان یکی نساخ بود</p></div>
<div class="m2"><p>کو به نسخ وحی جدی می‌نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نبی از وحی فرمودی سبق</p></div>
<div class="m2"><p>او همان را وا نبشتی بر ورق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرتو آن وحی بر وی تافتی</p></div>
<div class="m2"><p>او درون خویش حکمت یافتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عین آن حکمت بفرمودی رسول</p></div>
<div class="m2"><p>زین قدر گمراه شد آن بوالفضول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کانچ می‌گوید رسول مستنیر</p></div>
<div class="m2"><p>مر مرا هست آن حقیقت در ضمیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرتو اندیشه‌اش زد بر رسول</p></div>
<div class="m2"><p>قهر حق آورد بر جانش نزول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم ز نساخی بر آمد هم ز دین</p></div>
<div class="m2"><p>شد عدو مصطفی و دین بکین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مصطفی فرمود کای گبر عنود</p></div>
<div class="m2"><p>چون سیه گشتی اگر نور از تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر تو ینبوع الهی بودیی</p></div>
<div class="m2"><p>این چنین آب سیه نگشودیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا که ناموسش به پیش این و آن</p></div>
<div class="m2"><p>نشکند بر بست این او را دهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندرون می‌سوختش هم زین سبب</p></div>
<div class="m2"><p>توبه کردن می‌نیارست این عجب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آه می‌کرد و نبودش آه سود</p></div>
<div class="m2"><p>چون در آمد تیغ و سر را در ربود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرده حق ناموس را صد من حدید</p></div>
<div class="m2"><p>ای بسا بسته به بند ناپدید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کبر و کفر آن سان ببست آن راه را</p></div>
<div class="m2"><p>که نیارد کرد ظاهر آه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت اغلالا فهم به مقمحون</p></div>
<div class="m2"><p>نیست آن اغلال بر ما از برون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خلفهم سدا فاغشیناهم</p></div>
<div class="m2"><p>می‌نبیند بند را پیش و پس او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رنگ صحرا دارد آن سدی که خاست</p></div>
<div class="m2"><p>او نمی‌داند که آن سد قضاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاهد تو سد روی شاهدست</p></div>
<div class="m2"><p>مرشد تو سد گفت مرشدست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای بسا کفار را سودای دین</p></div>
<div class="m2"><p>بندشان ناموس و کبر آن و این</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بند پنهان لیک از آهن بتر</p></div>
<div class="m2"><p>بند آهن را کند پاره تبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بند آهن را توان کردن جدا</p></div>
<div class="m2"><p>بند غیبی را نداند کس دوا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرد را زنبور اگر نیشی زند</p></div>
<div class="m2"><p>طبع او آن لحظه بر دفعی تند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زخم نیش اما چو از هستی تست</p></div>
<div class="m2"><p>غم قوی باشد نگردد درد سست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شرح این از سینه بیرون می‌جهد</p></div>
<div class="m2"><p>لیک می‌ترسم که نومیدی دهد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نی مشو نومید و خود را شاد کن</p></div>
<div class="m2"><p>پیش آن فریادرس فریاد کن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کای محب عفو از ما عفو کن</p></div>
<div class="m2"><p>ای طبیب رنج ناسور کهن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عکس حکمت آن شقی را یاوه کرد</p></div>
<div class="m2"><p>خود مبین تا بر نیارد از تو گرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای برادر بر تو حکمت جاریه‌ست</p></div>
<div class="m2"><p>آن ز ابدالست و بر تو عاریه‌ست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گرچه در خود خانه نوری یافتست</p></div>
<div class="m2"><p>آن ز همسایهٔ منور تافتست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شکر کن غره مشو بینی مکن</p></div>
<div class="m2"><p>گوش دار و هیچ خودبینی مکن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صد دریغ و درد کین عاریتی</p></div>
<div class="m2"><p>امتان را دور کرد از امتی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من غلام آن که او در هر رباط</p></div>
<div class="m2"><p>خویش را واصل نداند بر سماط</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بس رباطی که بباید ترک کرد</p></div>
<div class="m2"><p>تا به مسکن در رسد یک روز مرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گرچه آهن سرخ شد او سرخ نیست</p></div>
<div class="m2"><p>پرتو عاریت آتش‌زنیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر شود پر نور روزن یا سرا</p></div>
<div class="m2"><p>تو مدان روشن مگر خورشید را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر در و دیوار گوید روشنم</p></div>
<div class="m2"><p>پرتو غیری ندارم این منم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس بگوید آفتاب ای نارشید</p></div>
<div class="m2"><p>چونک من غارب شوم آید پدید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سبزه‌ها گویند ما سبز از خودیم</p></div>
<div class="m2"><p>شاد و خندانیم و بس زیبا خدیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فصل تابستان بگوید ای امم</p></div>
<div class="m2"><p>خویش را بینید چون من بگذرم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تن همی‌نازد به خوبی و جمال</p></div>
<div class="m2"><p>روح پنهان کرده فر و پر و بال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گویدش ای مزبله تو کیستی</p></div>
<div class="m2"><p>یک دو روز از پرتو من زیستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>غنج و نازت می‌نگنجد در جهان</p></div>
<div class="m2"><p>باش تا که من شوم از تو جهان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرم‌دارانت ترا گوری کنند</p></div>
<div class="m2"><p>طعمهٔ ماران و مورانت کنند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بینی از گند تو گیرد آن کسی</p></div>
<div class="m2"><p>کو به پیش تو همی‌مردی بسی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پرتو روحست نطق و چشم و گوش</p></div>
<div class="m2"><p>پرتو آتش بود در آب جوش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آنچنانک پرتو جان بر تنست</p></div>
<div class="m2"><p>پرتو ابدال بر جان منست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جان جان چو واکشد پا را ز جان</p></div>
<div class="m2"><p>جان چنان گردد که بی‌جان تن بدان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سر از آن رو می‌نهم من بر زمین</p></div>
<div class="m2"><p>تا گواه من بود در روز دین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یوم دین که زلزلت زلزالها</p></div>
<div class="m2"><p>این زمین باشد گواه حالها</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گو تحدث جهرة اخبارها</p></div>
<div class="m2"><p>در سخن آید زمین و خاره‌ها</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فلسفی منکر شود در فکر و ظن</p></div>
<div class="m2"><p>گو برو سر را بر آن دیوار زن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نطق آب و نطق خاک و نطق گل</p></div>
<div class="m2"><p>هست محسوس حواس اهل دل</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فلسفی کو منکر حنانه است</p></div>
<div class="m2"><p>از حواس اولیا بیگانه است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گوید او که پرتو سودای خلق</p></div>
<div class="m2"><p>بس خیالات آورد در رای خلق</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بلک عکس آن فساد و کفر او</p></div>
<div class="m2"><p>این خیال منکری را زد برو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فلسفی مر دیو را منکر شود</p></div>
<div class="m2"><p>در همان دم سخرهٔ دیوی بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر ندیدی دیو را خود را ببین</p></div>
<div class="m2"><p>بی جنون نبود کبودی بر جبین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هر که را در دل شک و پیچانیست</p></div>
<div class="m2"><p>در جهان او فلسفی پنهانیست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>می‌نماید اعتقاد و گاه گاه</p></div>
<div class="m2"><p>آن رگ فلسف کند رویش سیاه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>الحذر ای مؤمنان کان در شماست</p></div>
<div class="m2"><p>در شما بس عالم بی‌منتهاست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جمله هفتاد و دو ملت در توست</p></div>
<div class="m2"><p>وه که روزی آن بر آرد از تو دست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر که او را برگ آن ایمان بود</p></div>
<div class="m2"><p>همچو برگ از بیم این لرزان بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بر بلیس و دیو زان خندیده‌ای</p></div>
<div class="m2"><p>که تو خود را نیک مردم دیده‌ای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چون کند جان بازگونه پوستین</p></div>
<div class="m2"><p>چند وا ویلی بر آید ز اهل دین</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بر دکان هر زرنما خندان شدست</p></div>
<div class="m2"><p>زانک سنگ امتحان پنهان شدست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پرده‌ای ستار از ما بر مگیر</p></div>
<div class="m2"><p>باش اندر امتحان ما را مجیر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>قلب پهلو می‌زند با زر به شب</p></div>
<div class="m2"><p>انتظار روز می‌دارد ذهب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>با زبان حال زر گوید که باش</p></div>
<div class="m2"><p>ای مزور تا بر آید روز فاش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>صد هزاران سال ابلیس لعین</p></div>
<div class="m2"><p>بود ز ابدال و امیر المؤمنین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پنجه زد با آدم از نازی که داشت</p></div>
<div class="m2"><p>گشت رسوا همچو سرگین وقت چاشت</p></div></div>