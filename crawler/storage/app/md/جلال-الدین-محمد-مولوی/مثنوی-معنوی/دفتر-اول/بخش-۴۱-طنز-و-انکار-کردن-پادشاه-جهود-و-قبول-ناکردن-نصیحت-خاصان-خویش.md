---
title: >-
    بخش ۴۱ - طنز و انکار کردن پادشاه جهود و قبول ناکردن نصیحت خاصان خویش
---
# بخش ۴۱ - طنز و انکار کردن پادشاه جهود و قبول ناکردن نصیحت خاصان خویش

<div class="b" id="bn1"><div class="m1"><p>این عجایب دید آن شاه جهود</p></div>
<div class="m2"><p>جز که طنز و جز که انکارش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناصحان گفتند از حد مگذران</p></div>
<div class="m2"><p>مرکب استیزه را چندین مران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناصحان را دست بست و بند کرد</p></div>
<div class="m2"><p>ظلم را پیوند در پیوند کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانگ آمد کار چون اینجا رسید</p></div>
<div class="m2"><p>پای دار ای سگ که قهر ما رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از آن آتش چهل گز بر فروخت</p></div>
<div class="m2"><p>حلقه گشت و آن جهودان را بسوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اصل ایشان بود آتش ز ابتدا</p></div>
<div class="m2"><p>سوی اصل خویش رفتند انتها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم ز آتش زاده بودند آن فریق</p></div>
<div class="m2"><p>جزوها را سوی کل باشد طریق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتشی بودندمؤمن‌سوز و بس</p></div>
<div class="m2"><p>سوخت خود را آتش ایشان چو خس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنک بودست امه الهاویه</p></div>
<div class="m2"><p>هاویه آمد مرورا زاویه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مادر فرزند جویان ویست</p></div>
<div class="m2"><p>اصلها مر فرعها را در پیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آبها در حوض اگر زندانیست</p></div>
<div class="m2"><p>باد نشفش می‌کند کار کانیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌رهاند می‌برد تا معدنش</p></div>
<div class="m2"><p>اندک اندک تا نبینی بردنش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وین نفس جانهای ما را همچنان</p></div>
<div class="m2"><p>اندک اندک دزدد از حبس جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا الیه یصعد اطیاب الکلم</p></div>
<div class="m2"><p>صاعدا منا الی حیث علم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترتقی انفاسنا بالمنتقی</p></div>
<div class="m2"><p>متحفا منا الی دار البقا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ثم تاتینا مکافات المقال</p></div>
<div class="m2"><p>ضعف ذاک رحمة من ذی الجلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ثم یلجینا الی امثالها</p></div>
<div class="m2"><p>کی ینال العبد مما نالها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هکذی تعرج و تنزل دائما</p></div>
<div class="m2"><p>ذا فلا زلت علیه قائما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پارسی گوییم یعنی این کشش</p></div>
<div class="m2"><p>زان طرف آید که آمد آن چشش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشم هر قومی به سویی مانده‌ست</p></div>
<div class="m2"><p>کان طرف یک روز ذوقی رانده‌ست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ذوق جنس از جنس خود باشد یقین</p></div>
<div class="m2"><p>ذوق جزو از کل خود باشد ببین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یا مگر آن قابل جنسی بود</p></div>
<div class="m2"><p>چون بدو پیوست جنس او شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همچو آب و نان که جنس ما نبود</p></div>
<div class="m2"><p>گشت جنس ما و اندر ما فزود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نقش جنسیت ندارد آب و نان</p></div>
<div class="m2"><p>ز اعتبار آخر آن را جنس دان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور ز غیر جنس باشد ذوق ما</p></div>
<div class="m2"><p>آن مگر مانند باشد جنس را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنک مانندست باشد عاریت</p></div>
<div class="m2"><p>عاریت باقی نماند عاقبت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرغ را گر ذوق آید از صفیر</p></div>
<div class="m2"><p>چونک جنس خود نیابد شد نفیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تشنه را گر ذوق آید از سراب</p></div>
<div class="m2"><p>چون رسد در وی گریزد جوید آب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مفلسان هم خوش شوند از زر قلب</p></div>
<div class="m2"><p>لیک آن رسوا شود در دار ضرب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا زر اندودیت از ره نفکند</p></div>
<div class="m2"><p>تا خیال کژ ترا چه نفکند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از کلیله باز جو آن قصه را</p></div>
<div class="m2"><p>واندر آن قصه طلب کن حصه را</p></div></div>