---
title: >-
    بخش ۶ - بردن پادشاه آن طبیب را بر بیمار تا حال او را ببیند
---
# بخش ۶ - بردن پادشاه آن طبیب را بر بیمار تا حال او را ببیند

<div class="b" id="bn1"><div class="m1"><p>قصهٔ رنجور و رنجوری بخواند</p></div>
<div class="m2"><p>بعدازآن در پیش رنجورش نشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ روی و نبض و قاروره بدید</p></div>
<div class="m2"><p>هم علاماتش هم اسبابش شنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت هر دارو که ایشان کرده‌اند</p></div>
<div class="m2"><p>آن عمارت نیست ویران کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌خبر بودند از حال درون</p></div>
<div class="m2"><p>استعیذ الله مما یفترون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید رنج و کشف شد بَر وِی نهفت</p></div>
<div class="m2"><p>لیک پنهان کرد و با سلطان نگفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنجش از صفرا و از سودا نبود</p></div>
<div class="m2"><p>بوی هر هیزم پدید آید ز دود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دید از زاریش کو زار دل است</p></div>
<div class="m2"><p>تن خوش است و او گرفتار دل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقی پیداست از زاری دل</p></div>
<div class="m2"><p>نیست بیماری چو بیماری دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علت عاشق ز علت‌ها جداست</p></div>
<div class="m2"><p>عشق اصطرلاب اسرار خداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقی گر زین سر و گر زان سرست</p></div>
<div class="m2"><p>عاقبت ما را بدان سر رهبرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچه گویم عشق را شرح و بیان</p></div>
<div class="m2"><p>چون به عشق آیم خجل باشم از آن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه تفسیر زبان روشنگرست</p></div>
<div class="m2"><p>لیک عشق بی‌زبان روشنترست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون قلم اندر نوشتن می‌شتافت</p></div>
<div class="m2"><p>چون به عشق آمد قلم بر خود شکافت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل در شرحش چو خر در گل بخفت</p></div>
<div class="m2"><p>شرح عشق و عاشقی هم عشق گفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آفتاب آمد دلیل آفتاب</p></div>
<div class="m2"><p>گر دلیلت باید از وی رو متاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از وی ار سایه نشانی می‌دهد</p></div>
<div class="m2"><p>شمس هر دم نور جانی می‌دهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سایه، خواب آرد تو را همچون سمر</p></div>
<div class="m2"><p>چون برآید شمس انشق القمر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خود غریبی در جهان چون شمس نیست</p></div>
<div class="m2"><p>شمس جان باقیست کاو را امس نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمس در خارج اگر چه هست فرد</p></div>
<div class="m2"><p>می‌توان هم مثل او تصویر کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمس جان کو خارج آمد از اثیر</p></div>
<div class="m2"><p>نبودش در ذهن و در خارج نظیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در تصور ذات او را گُنج کو</p></div>
<div class="m2"><p>تا درآید در تصور مثل او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون حدیث روی شمس‌الدین رسید</p></div>
<div class="m2"><p>شمس چارم‌آسمان سر در کشید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>واجب آید چونکه آمد نام او</p></div>
<div class="m2"><p>شرح کردن رمزی از انعام او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این نفس جان دامنم برتافتست</p></div>
<div class="m2"><p>بوی پیراهان یوسف یافتست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کز برای حقِ صحبت سال‌ها</p></div>
<div class="m2"><p>بازگو حالی از آن خوش حال‌ها</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا زمین و آسمان خندان شود</p></div>
<div class="m2"><p>عقل و روح و دیده صدچندان شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لاتکلفنی فانی فی الفنا</p></div>
<div class="m2"><p>کلت افهامی فلا احصی ثنا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کل شیء قاله غیرالمفیق</p></div>
<div class="m2"><p>ان تکلف او تصلف لا یلیق</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من چه گویم یک رگم هشیار نیست</p></div>
<div class="m2"><p>شرح آن یاری که او را یار نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شرح این هجران و این خون جگر</p></div>
<div class="m2"><p>این زمان بگذار تا وقت دگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قال اطعمنی فانی جائع</p></div>
<div class="m2"><p>واعتجل فالوقت سیف قاطع</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صوفی ابن‌الوقت باشد ای رفیق</p></div>
<div class="m2"><p>نیست فردا گفتن از شرط طریق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو مگر خود مرد صوفی نیستی</p></div>
<div class="m2"><p>هست را از نسیه خیزد نیستی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتمش پوشیده خوش‌تر سِرِّ یار</p></div>
<div class="m2"><p>خود تو در ضمن حکایت گوش‌ دار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خوش‌تر آن باشد که سر دلبران</p></div>
<div class="m2"><p>گفته آید در حدیث دیگران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت مکشوف و برهنه بی‌غلول</p></div>
<div class="m2"><p>بازگو، دفعم مِدِه ای بوالفضول</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پرده بردار و برهنه گو که من</p></div>
<div class="m2"><p>می‌نخسپم با صنم با پیرهن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفتم ار عریان شود او در عیان</p></div>
<div class="m2"><p>نه تو مانی نه کنارت نه میان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آرزو می‌خواه، لیک اندازه خواه</p></div>
<div class="m2"><p>برنتابد کوه را یک برگ کاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آفتابی کز وی این عالم فروخت</p></div>
<div class="m2"><p>اندکی گر پیش آید جمله سوخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فتنه و آشوب و خون‌ریزی مجوی</p></div>
<div class="m2"><p>بیش ازین از شمس تبریزی مگوی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>این ندارد آخر، از آغاز گوی</p></div>
<div class="m2"><p>رو تمام این حکایت بازگوی</p></div></div>