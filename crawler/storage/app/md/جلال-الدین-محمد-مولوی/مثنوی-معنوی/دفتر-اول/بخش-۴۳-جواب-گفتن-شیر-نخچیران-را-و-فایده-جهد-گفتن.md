---
title: >-
    بخش ۴۳ - جواب گفتن شیر نخچیران را و فایدهٔ جهد گفتن
---
# بخش ۴۳ - جواب گفتن شیر نخچیران را و فایدهٔ جهد گفتن

<div class="b" id="bn1"><div class="m1"><p>گفت آری گر وفا بینم نه مکر</p></div>
<div class="m2"><p>مکرها بس دیده‌ام از زید و بکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من هلاک فعل و مکر مردمم</p></div>
<div class="m2"><p>من گزیدهٔ زخم مار و کزدمم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم نفس از درونم در کمین</p></div>
<div class="m2"><p>از همه مردم بتر در مکر و کین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوش من لایلدغ المؤمن شنید</p></div>
<div class="m2"><p>قول پیغامبر بجان و دل گزید</p></div></div>