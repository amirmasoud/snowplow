---
title: >-
    بخش ۱۲۸ - تعیین کردن زن طریق طلب روزی کدخدای خود را و قبول کردن او
---
# بخش ۱۲۸ - تعیین کردن زن طریق طلب روزی کدخدای خود را و قبول کردن او

<div class="b" id="bn1"><div class="m1"><p>گفت زن یک آفتابی تافتست</p></div>
<div class="m2"><p>عالمی زو روشنایی یافتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نایب رحمان خلیفهٔ کردگار</p></div>
<div class="m2"><p>شهر بغدادست از وی چون بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بپیوندی بدان شه شه شوی</p></div>
<div class="m2"><p>سوی هر ادبیر تا کی می‌روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همنشینی با شهان چون کیمیاست</p></div>
<div class="m2"><p>چون نظرشان کیمیایی خود کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم احمد بر ابوبکری زده</p></div>
<div class="m2"><p>او ز یک تصدیق صدیق آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت من شه را پذیرا چون شوم</p></div>
<div class="m2"><p>بی بهانه سوی او من چون روم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسبتی باید مرا یا حیلتی</p></div>
<div class="m2"><p>هیچ پیشه راست شد بی‌آلتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو مجنونی که بشنید از یکی</p></div>
<div class="m2"><p>که مرض آمد به لیلی اندکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت آوه بی بهانه چون روم</p></div>
<div class="m2"><p>ور بمانم از عیادت چون شوم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لیتنی کنت طبیبا حاذقا</p></div>
<div class="m2"><p>کنت امشی نحو لیلی سابقا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قل تعالوا گفت حق ما را بدان</p></div>
<div class="m2"><p>تا بود شرم‌اشکنی ما را نشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب‌پران را گر نظر و آلت بدی</p></div>
<div class="m2"><p>روزشان جولان و خوش حالت بدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت چون شاه کرم میدان رود</p></div>
<div class="m2"><p>عین هر بی‌آلتی آلت شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانک آلت دعوی است و هستی است</p></div>
<div class="m2"><p>کار در بی‌آلتی و پستی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت کی بی‌آلتی سودا کنم</p></div>
<div class="m2"><p>تا نه من بی‌آلتی پیدا کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس گواهی بایدم بر مفلسی</p></div>
<div class="m2"><p>تا مرا رحمی کند شاه غنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو گواهی غیر گفت و گو و رنگ</p></div>
<div class="m2"><p>وا نما تا رحم آرد شاه شنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کین گواهی که ز گفت و رنگ بد</p></div>
<div class="m2"><p>نزد آن قاضی القضاة آن جرح شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صدق می‌خواهد گواه حال او</p></div>
<div class="m2"><p>تا بتابد نور او بی قال او</p></div></div>