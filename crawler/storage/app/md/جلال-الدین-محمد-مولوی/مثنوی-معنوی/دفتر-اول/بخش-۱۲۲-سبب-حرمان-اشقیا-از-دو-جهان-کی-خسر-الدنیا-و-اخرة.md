---
title: >-
    بخش ۱۲۲ - سبب حرمان اشقیا از دو جهان کی خسر الدنیا و اخرة
---
# بخش ۱۲۲ - سبب حرمان اشقیا از دو جهان کی خسر الدنیا و اخرة

<div class="b" id="bn1"><div class="m1"><p>چون حکیمک اعتقادی کرده است</p></div>
<div class="m2"><p>کآسمان بیضه زمین چون زرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت سایل چون بماند این خاکدان</p></div>
<div class="m2"><p>در میان این محیط آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو قندیلی معلق در هوا</p></div>
<div class="m2"><p>نه باسفل می‌رود نه بر علا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن حکیمش گفت کز جذب سما</p></div>
<div class="m2"><p>از جهات شش بماند اندر هوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ز مغناطیس قبهٔ ریخته</p></div>
<div class="m2"><p>درمیان ماند آهنی آویخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دگر گفت آسمان با صفا</p></div>
<div class="m2"><p>کی کشد در خود زمین تیره را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلک دفعش می‌کند از شش جهات</p></div>
<div class="m2"><p>زان بماند اندر میان عاصفات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس ز دفع خاطر اهل کمال</p></div>
<div class="m2"><p>جان فرعونان بماند اندر ضلال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس ز دفع این جهان و آن جهان</p></div>
<div class="m2"><p>مانده‌اند این بی‌رهان بی این و آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر کشی از بندگان ذوالجلال</p></div>
<div class="m2"><p>دان که دارند از وجود تو ملال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کهربا دارند چون پیدا کنند</p></div>
<div class="m2"><p>کاه هستی ترا شیدا کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کهربای خویش چون پنهان کنند</p></div>
<div class="m2"><p>زود تسلیم ترا طغیان کنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنچنان که مرتبهٔ حیوانیست</p></div>
<div class="m2"><p>کو اسیر و سغبهٔ انسانیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرتبهٔ انسان به دست اولیا</p></div>
<div class="m2"><p>سغبه چون حیوان شناسش ای کیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بندهٔ خود خواند احمد در رشاد</p></div>
<div class="m2"><p>جمله عالم را بخوان قل یا عباد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عقل تو همچون شتربان تو شتر</p></div>
<div class="m2"><p>می‌کشاند هر طرف در حکم مر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عقل عقلند اولیا و عقلها</p></div>
<div class="m2"><p>بر مثال اشتران تا انتها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندریشان بنگر آخر ز اعتبار</p></div>
<div class="m2"><p>یک قلاووزست جان صد هزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه قلاووز و چه اشتربان بیاب</p></div>
<div class="m2"><p>دیده‌ای کان دیده بیند آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک جهان در شب بمانده میخ‌دوز</p></div>
<div class="m2"><p>منتظر موقوف خورشیدست و روز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اینت خورشیدی نهان در ذره‌ای</p></div>
<div class="m2"><p>شیر نر در پوستین بره‌ای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اینت دریایی نهان در زیر کاه</p></div>
<div class="m2"><p>پا برین که هین منه با اشتباه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اشتباهی و گمانی را درون</p></div>
<div class="m2"><p>رحمت حقست بهر رهنمون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر پیمبر فرد آمد در جهان</p></div>
<div class="m2"><p>فرد بود آن رهنمایش در نهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عالک کبری بقدرت سحر کرد</p></div>
<div class="m2"><p>کرد خود را در کهین نقشی نورد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ابلهانش فرد دیدند و ضعیف</p></div>
<div class="m2"><p>کی ضعیفست آن که با شه شد حریف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابلهان گفتند مردی بیش نیست</p></div>
<div class="m2"><p>وای آنکو عاقبت‌اندیش نیست</p></div></div>