---
title: >-
    بخش ۳ - ظاهر شدن عجز حکیمان از معالجهٔ کنیزک و روی آوردن پادشاه به درگاه اله و در خواب دیدن او ولیی را
---
# بخش ۳ - ظاهر شدن عجز حکیمان از معالجهٔ کنیزک و روی آوردن پادشاه به درگاه اله و در خواب دیدن او ولیی را

<div class="b" id="bn1"><div class="m1"><p>شه چو عجز آن حکیمان را بدید</p></div>
<div class="m2"><p>پابرهنه جانب مسجد دوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت در مسجد سوی محراب شد</p></div>
<div class="m2"><p>سجده‌گاه از اشک شه پر آب شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به خویش آمد ز غرقاب فنا</p></div>
<div class="m2"><p>خوش زبان بگشاد در مدح و دعا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کای کمینه بخششت مُلک جهان</p></div>
<div class="m2"><p>من چه گویم چون تو می‌دانی نهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای همیشه حاجت ما را پناه</p></div>
<div class="m2"><p>بار دیگر ما غلط کردیم راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک گفتی گرچه می‌دانم سرت</p></div>
<div class="m2"><p>زود هم پیدا کنش بر ظاهرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون برآورد از میان جان خروش</p></div>
<div class="m2"><p>اندر آمد بحر بخشایش به جوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درمیان گریه خوابش در ربود</p></div>
<div class="m2"><p>دید در خواب او که پیری رو نمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت ای شه مژده حاجاتت رواست</p></div>
<div class="m2"><p>گر غریبی آیدت فردا ز ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونکه آید او حکیمی حاذقست</p></div>
<div class="m2"><p>صادقش دان کو امین و صادقست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در علاجش سِحر مطلق را ببین</p></div>
<div class="m2"><p>در مزاجش قدرت حق را ببین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون رسید آن وعده‌گاه و روز شد</p></div>
<div class="m2"><p>آفتاب از شرق اخترسوز شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود اندر منظره شه منتظر</p></div>
<div class="m2"><p>تا ببیند آنچه بنمودند سر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دید شخصی فاضلی پرمایه‌ای</p></div>
<div class="m2"><p>آفتابی درمیان سایه‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می‌رسید از دور مانند هلال</p></div>
<div class="m2"><p>نیست بود و هست بر شکل خیال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست‌وش باشد خیال اندر روان</p></div>
<div class="m2"><p>تو جهانی بر خیالی بین روان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر خیالی صلحشان و جنگشان</p></div>
<div class="m2"><p>وز خیالی فخرشان و ننگشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن خیالاتی که دام اولیاست</p></div>
<div class="m2"><p>عکس مه‌رویان بستان خداست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن خیالی که شه اندر خواب دید</p></div>
<div class="m2"><p>در رخ مهمان همی آمد پدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شه به جای حاجبان فا پیش رفت</p></div>
<div class="m2"><p>پیش آن مهمان غیب خویش رفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر دو بحری آشنا آموخته</p></div>
<div class="m2"><p>هر دو جان بی دوختن بر دوخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت معشوقم تو بودستی نه آن</p></div>
<div class="m2"><p>لیک کار از کار خیزد در جهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای مرا تو مصطفی من چو عمر</p></div>
<div class="m2"><p>از برای خدمتت بندم کمر</p></div></div>