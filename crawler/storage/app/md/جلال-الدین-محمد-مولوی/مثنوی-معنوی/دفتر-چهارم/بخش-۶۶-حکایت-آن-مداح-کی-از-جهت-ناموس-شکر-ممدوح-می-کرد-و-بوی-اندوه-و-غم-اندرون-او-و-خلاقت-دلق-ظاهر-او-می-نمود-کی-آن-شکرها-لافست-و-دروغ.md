---
title: >-
    بخش ۶۶ - حکایت آن مداح کی از جهت ناموس شکر ممدوح می‌کرد و بوی اندوه و غم اندرون او و خلاقت دلق ظاهر او می‌نمود کی آن  شکرها لافست و دروغ
---
# بخش ۶۶ - حکایت آن مداح کی از جهت ناموس شکر ممدوح می‌کرد و بوی اندوه و غم اندرون او و خلاقت دلق ظاهر او می‌نمود کی آن  شکرها لافست و دروغ

<div class="b" id="bn1"><div class="m1"><p>آن یکی با دلق آمد از عراق</p></div>
<div class="m2"><p>باز پرسیدند یاران از فراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت آری بد فراق الا سفر</p></div>
<div class="m2"><p>بود بر من بس مبارک مژده‌ور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که خلیفه داد ده خلعت مرا</p></div>
<div class="m2"><p>که قرینش باد صد مدح و ثنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکرها و حمدها بر می‌شمرد</p></div>
<div class="m2"><p>تا که شکر از حد و اندازه ببرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس بگفتندش که احوال نژند</p></div>
<div class="m2"><p>بر دروغ تو گواهی می‌دهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن برهنه سر برهنه سوخته</p></div>
<div class="m2"><p>شکر را دزدیده یا آموخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو نشان شکر و حمد میر تو</p></div>
<div class="m2"><p>بر سر و بر پای بی توفیر تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر زبانت مدح آن شه می‌تند</p></div>
<div class="m2"><p>هفت اندامت شکایت می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سخای آن شه و سلطان جود</p></div>
<div class="m2"><p>مر ترا کفشی و شلواری نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت من ایثار کردم آنچ داد</p></div>
<div class="m2"><p>میر تقصیری نکرد از افتقاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بستدم جمله عطاها از امیر</p></div>
<div class="m2"><p>بخش کردم بر یتیم و بر فقیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مال دادم بستدم عمر دراز</p></div>
<div class="m2"><p>در جزا زیرا که بودم پاک‌باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس بگفتندش مبارک مال رفت</p></div>
<div class="m2"><p>چیست اندر باطنت این دود نفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صد کراهت در درون تو چو خار</p></div>
<div class="m2"><p>کی بود انده نشان ابتشار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کو نشان عشق و ایثار و رضا</p></div>
<div class="m2"><p>گر درستست آنچ گفتی ما مضی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خود گرفتم مال گم شد میل کو</p></div>
<div class="m2"><p>سیل اگر بگذشت جای سیل کو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چشم تو گر بد سیاه و جان‌فزا</p></div>
<div class="m2"><p>گر نماند او جان‌فزا ازرق چرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کو نشان پاک‌بازی ای ترش</p></div>
<div class="m2"><p>بوی لاف کژ همی‌آید خمش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صد نشان باشد درون ایثار را</p></div>
<div class="m2"><p>صد علامت هست نیکوکار را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مال در ایثار اگر گردد تلف</p></div>
<div class="m2"><p>در درون صد زندگی آید خلف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در زمین حق زراعت کردنی</p></div>
<div class="m2"><p>تخمهای پاک آنگه دخل نی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر نروید خوشه از روضات هو</p></div>
<div class="m2"><p>پس چه واسع باشد ارض الله بگو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چونک این ارض فنا بی‌ریع نیست</p></div>
<div class="m2"><p>چون بود ارض الله آن مستوسعیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این زمین را ریع او خود بی‌حدست</p></div>
<div class="m2"><p>دانه‌ای را کمترین خود هفصدست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حمد گفتی کو نشان حامدون</p></div>
<div class="m2"><p>نه برونت هست اثر نه اندرون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حمد عارف مر خدا را راستست</p></div>
<div class="m2"><p>که گواه حمد او شد پا و دست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از چه تاریک جسمش بر کشید</p></div>
<div class="m2"><p>وز تک زندان دنیااش خرید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اطلس تقوی و نور مؤتلف</p></div>
<div class="m2"><p>آیت حمدست او را بر کتف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وا رهیده از جهان عاریه</p></div>
<div class="m2"><p>ساکن گلزار و عین جاریه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر سریر سر عالی‌همتش</p></div>
<div class="m2"><p>مجلس و جا و مقام و رتبتش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مقعد صدقی که صدیقان درو</p></div>
<div class="m2"><p>جمله سر سبزند و شاد و تازه‌رو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حمدشان چون حمد گلشن از بهار</p></div>
<div class="m2"><p>صد نشانی دارد و صد گیر و دار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر بهارش چشمه و نخل و گیاه</p></div>
<div class="m2"><p>وآن گلستان و نگارستان گواه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شاهد شاهد هزاران هر طرف</p></div>
<div class="m2"><p>در گواهی هم‌چو گوهر بر صدف</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بوی سر بد بیاید از دمت</p></div>
<div class="m2"><p>وز سر و رو تابد ای لافی غمت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بوشناسانند حاذق در مصاف</p></div>
<div class="m2"><p>تو به جلدی های هو کم کن گزاف</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو ملاف از مشک کان بوی پیاز</p></div>
<div class="m2"><p>از دم تو می‌کند مکشوف راز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گل‌شکر خوردم همی‌گویی و بوی</p></div>
<div class="m2"><p>می‌زند از سیر که یافه مگوی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هست دل مانندهٔ خانهٔ کلان</p></div>
<div class="m2"><p>خانهٔ دل را نهان همسایگان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از شکاف روزن و دیوارها</p></div>
<div class="m2"><p>مطلع گردند بر اسرار ما</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از شکافی که ندارد هیچ وهم</p></div>
<div class="m2"><p>صاحب خانه و ندارد هیچ سهم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از نبی بر خوان که دیو و قوم او</p></div>
<div class="m2"><p>می‌برند از حال انسی خفیه بو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از رهی که انس از آن آگاه نیست</p></div>
<div class="m2"><p>زانک زین محسوس و زین اشباه نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در میان ناقدان زرقی متن</p></div>
<div class="m2"><p>با محک ای قلب دون لافی مزن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مر محک را ره بود در نقد و قلب</p></div>
<div class="m2"><p>که خدایش کرد امیر جسم و قلب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون شیاطین با غلیظیهای خویش</p></div>
<div class="m2"><p>واقف‌اند از سر ما و فکر و کیش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مسلکی دارند دزدیده درون</p></div>
<div class="m2"><p>ما ز دزدیهای ایشان سرنگون</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دم به دم خبط و زیانی می‌کنند</p></div>
<div class="m2"><p>صاحب نقب و شکاف روزنند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس چرا جان‌های روشن در جهان</p></div>
<div class="m2"><p>بی‌خبر باشند از حال نهان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در سرایت کمتر از دیوان شدند</p></div>
<div class="m2"><p>روحها که خیمه بر گردون زدند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دیو دزدانه سوی گردون رود</p></div>
<div class="m2"><p>از شهاب محرق او مطعون شود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سرنگون از چرخ زیر افتد چنان</p></div>
<div class="m2"><p>که شقی در جنگ از زخم سنان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آن ز رشک روحهای دل‌پسند</p></div>
<div class="m2"><p>از فلکشان سرنگون می‌افکنند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تو اگر شلی و لنگ و کور و کر</p></div>
<div class="m2"><p>این گمان بر روحهای مه مبر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شرم دار و لاف کم زن جان مکن</p></div>
<div class="m2"><p>که بسی جاسوس هست آن سوی تن</p></div></div>