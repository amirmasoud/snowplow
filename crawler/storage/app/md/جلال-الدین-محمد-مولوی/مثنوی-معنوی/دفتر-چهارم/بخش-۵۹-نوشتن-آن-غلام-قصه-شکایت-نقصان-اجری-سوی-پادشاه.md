---
title: >-
    بخش ۵۹ - نوشتن آن غلام قصهٔ شکایت نقصان اجری سوی پادشاه
---
# بخش ۵۹ - نوشتن آن غلام قصهٔ شکایت نقصان اجری سوی پادشاه

<div class="b" id="bn1"><div class="m1"><p>قصه کوته کن برای آن غلام</p></div>
<div class="m2"><p>که سوی شه بر نوشتست او پیام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصه پر جنگ و پر هستی و کین</p></div>
<div class="m2"><p>می‌فرستد پیش شاه نازنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کالبد نامه‌ست اندر وی نگر</p></div>
<div class="m2"><p>هست لایق شاه را آنگه ببر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه‌ای رو نامه را بگشا بخوان</p></div>
<div class="m2"><p>بین که حرفش هست در خورد شهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نباشد درخور آن را پاره کن</p></div>
<div class="m2"><p>نامهٔ دیگر نویس و چاره کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک فتح نامهٔ تن زپ مدان</p></div>
<div class="m2"><p>ورنه هر کس سر دل دیدی عیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامه بگشادن چه دشوارست و صعب</p></div>
<div class="m2"><p>کار مردانست نه طفلان کعب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله بر فهرست قانع گشته‌ایم</p></div>
<div class="m2"><p>زانک در حرص و هوا آغشته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد آن فهرست دامی عامه را</p></div>
<div class="m2"><p>تا چنان دانند متن نامه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز کن سرنامه را گردن متاب</p></div>
<div class="m2"><p>زین سخن والله اعلم بالصواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست آن عنوان چو اقرار زبان</p></div>
<div class="m2"><p>متن نامهٔ سینه را کن امتحان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که موافق هست با اقرار تو</p></div>
<div class="m2"><p>تا منافق‌وار نبود کار تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون جوالی بس گرانی می‌بری</p></div>
<div class="m2"><p>زان نباید کم که در وی بنگری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که چه داری در جوال از تلخ و خوش</p></div>
<div class="m2"><p>گر همی ارزد کشیدن را بکش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ورنه خالی کن جوالت را ز سنگ</p></div>
<div class="m2"><p>باز خر خود را ازین بیگار و ننگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در جوال آن کن که می‌باید کشید</p></div>
<div class="m2"><p>سوی سلطانان و شاهان رشید</p></div></div>