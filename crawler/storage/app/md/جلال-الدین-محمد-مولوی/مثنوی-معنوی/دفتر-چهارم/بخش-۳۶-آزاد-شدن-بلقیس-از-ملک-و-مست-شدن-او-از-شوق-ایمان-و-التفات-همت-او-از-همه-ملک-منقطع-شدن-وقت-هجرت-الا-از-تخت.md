---
title: >-
    بخش ۳۶ - آزاد شدن بلقیس از ملک و مست  شدن او از شوق ایمان و التفات همت او از همهٔ ملک منقطع شدن وقت هجرت الا از تخت
---
# بخش ۳۶ - آزاد شدن بلقیس از ملک و مست  شدن او از شوق ایمان و التفات همت او از همهٔ ملک منقطع شدن وقت هجرت الا از تخت

<div class="b" id="bn1"><div class="m1"><p>چون سلیمان سوی مرغان سبا</p></div>
<div class="m2"><p>یک صفیری کرد بست آن جمله را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز مگر مرغی که بد بی‌جان و پر</p></div>
<div class="m2"><p>یا چو ماهی گنگ بود از اصل کر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی غلط گفتم که کر گر سر نهد</p></div>
<div class="m2"><p>پیش وحی کبریا سمعش دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونک بلقیس از دل و جان عزم کرد</p></div>
<div class="m2"><p>بر زمان رفته هم افسوس خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک مال و ملک کرد او آن چنان</p></div>
<div class="m2"><p>که بترک نام و ننگ آن عاشقان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن غلامان و کنیزان بناز</p></div>
<div class="m2"><p>پیش چشمش هم‌چو پوسیده پیاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باغها و قصرها و آب رود</p></div>
<div class="m2"><p>پیش چشم از عشق گلحن می‌نمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق در هنگام استیلا و خشم</p></div>
<div class="m2"><p>زشت گرداند لطیفان را به چشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زمرد را نماید گندنا</p></div>
<div class="m2"><p>غیرت عشق این بود معنی لا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لااله الا هو اینست ای پناه</p></div>
<div class="m2"><p>که نماید مه ترا دیگ سیاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ مال و هیچ مخزن هیچ رخت</p></div>
<div class="m2"><p>می دریغش نامد الا جز که تخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس سلیمان از دلش آگاه شد</p></div>
<div class="m2"><p>کز دل او تا دل او راه شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن کسی که بانگ موران بشنود</p></div>
<div class="m2"><p>هم فغان سر دوران بشنود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنک گوید راز قالت نملة</p></div>
<div class="m2"><p>هم بداند راز این طاق کهن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دید از دورش که آن تسلیم کیش</p></div>
<div class="m2"><p>تلخش آمد فرقت آن تخت خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر بگویم آن سبب گردد دراز</p></div>
<div class="m2"><p>که چرا بودش به تخت آن عشق و ساز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرچه این کلک قلم خود بی‌حسیست</p></div>
<div class="m2"><p>نیست جنس کاتب او را مونسیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم‌چنین هر آلت پیشه‌وری</p></div>
<div class="m2"><p>هست بی‌جان مونس جانوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این سبب را من معین گفتمی</p></div>
<div class="m2"><p>گر نبودی چشم فهمت را نمی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بزرگی تخت کز حد می‌فزود</p></div>
<div class="m2"><p>نقل کردن تخت را امکان نبود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خرده کاری بود و تفریقش خطر</p></div>
<div class="m2"><p>هم‌چو اوصال بدن با همدگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس سلیمان گفت گر چه فی‌الاخیر</p></div>
<div class="m2"><p>سرد خواهد شد برو تاج و سریر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون ز وحدت جان برون آرد سری</p></div>
<div class="m2"><p>جسم را با فر او نبود فری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون برآید گوهر از قعر بحار</p></div>
<div class="m2"><p>بنگری اندر کف و خاشاک خوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر بر آرد آفتاب با شرر</p></div>
<div class="m2"><p>دم عقرب را کی سازد مستقر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیک خود با این همه بر نقد حال</p></div>
<div class="m2"><p>جست باید تخت او را انتقال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نگردد خسته هنگام لقا</p></div>
<div class="m2"><p>کودکانه حاجتش گردد روا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هست بر ما سهل و او را بس عزیز</p></div>
<div class="m2"><p>تا بود بر خوان حوران دیو نیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عبرت جانش شود آن تخت ناز</p></div>
<div class="m2"><p>هم‌چو دلق و چارقی پیش ایاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بداند در چه بود آن مبتلا</p></div>
<div class="m2"><p>از کجاها در رسید او تا کجا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خاک را و نطفه را و مضغه را</p></div>
<div class="m2"><p>پیش چشم ما همی‌دارد خدا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کز کجا آوردمت ای بدنیت</p></div>
<div class="m2"><p>که از آن آید همی خفریقیت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو بر آن عاشق بدی در دور آن</p></div>
<div class="m2"><p>منکر این فضل بودی آن زمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این کرم چون دفع آن انکار تست</p></div>
<div class="m2"><p>که میان خاک می‌کردی نخست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حجت انکار شد انشار تو</p></div>
<div class="m2"><p>از دوا بدتر شد این بیمار تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خاک را تصویر این کار از کجا</p></div>
<div class="m2"><p>نطفه را خصمی و انکار از کجا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون در آن دم بی‌دل و بی‌سر بدی</p></div>
<div class="m2"><p>فکرت و انکار را منکر بدی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از جمادی چونک انکارت برست</p></div>
<div class="m2"><p>هم ازین انکار حشرت شد درست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پس مثال تو چو آن حلقه‌زنیست</p></div>
<div class="m2"><p>کز درونش خواجه گوید خواجه نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حلقه‌زن زین نیست دریابد که هست</p></div>
<div class="m2"><p>پس ز حلقه بر ندارد هیچ دست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس هم انکارت مبین می‌کند</p></div>
<div class="m2"><p>کز جماد او حشر صد فن می‌کند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چند صنعت رفت ای انکار تا</p></div>
<div class="m2"><p>آب و گل انکار زاد از هل اتی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آب وگل می‌گفت خود انکار نیست</p></div>
<div class="m2"><p>بانگ می‌زد بی‌خبر که اخبار نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من بگویم شرح این از صد طریق</p></div>
<div class="m2"><p>لیک خاطر لغزد از گفت دقیق</p></div></div>