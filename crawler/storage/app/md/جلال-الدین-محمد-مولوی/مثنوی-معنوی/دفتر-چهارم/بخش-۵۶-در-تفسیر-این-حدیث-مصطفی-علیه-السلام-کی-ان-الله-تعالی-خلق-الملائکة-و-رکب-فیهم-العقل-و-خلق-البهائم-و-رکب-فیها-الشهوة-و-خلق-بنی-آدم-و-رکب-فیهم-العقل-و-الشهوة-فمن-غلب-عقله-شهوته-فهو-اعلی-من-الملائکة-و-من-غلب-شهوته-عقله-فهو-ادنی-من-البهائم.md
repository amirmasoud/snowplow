---
title: >-
    بخش ۵۶ - در تفسیر این حدیث مصطفی علیه‌السلام کی ان الله تعالی خلق الملائکة و رکب  فیهم العقل و خلق البهائم و رکب فیها الشهوة و خلق بنی آدم و رکب فیهم العقل و الشهوة فمن غلب عقله شهوته فهو اعلی من الملائکة و من غلب شهوته عقله فهو ادنی من البهائم
---
# بخش ۵۶ - در تفسیر این حدیث مصطفی علیه‌السلام کی ان الله تعالی خلق الملائکة و رکب  فیهم العقل و خلق البهائم و رکب فیها الشهوة و خلق بنی آدم و رکب فیهم العقل و الشهوة فمن غلب عقله شهوته فهو اعلی من الملائکة و من غلب شهوته عقله فهو ادنی من البهائم

<div class="b" id="bn1"><div class="m1"><p>در حدیث آمد که یزدان مجید</p></div>
<div class="m2"><p>خلق عالم را سه گونه آفرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک گره را جمله عقل و علم و جود</p></div>
<div class="m2"><p>آن فرشته‌ست او نداند جز سجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست اندر عنصرش حرص و هوا</p></div>
<div class="m2"><p>نور مطلق زنده از عشق خدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک گروه دیگر از دانش تهی</p></div>
<div class="m2"><p>هم‌چو حیوان از علف در فربهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او نبیند جز که اصطبل و علف</p></div>
<div class="m2"><p>از شقاوت غافلست و از شرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این سوم هست آدمی‌زاد و بشر</p></div>
<div class="m2"><p>نیم او ز افرشته و نیمیش خر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیم خر خود مایل سفلی بود</p></div>
<div class="m2"><p>نیم دیگر مایل عقلی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن دو قوم آسوده از جنگ و حراب</p></div>
<div class="m2"><p>وین بشر با دو مخالف در عذاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وین بشر هم ز امتحان قسمت شدند</p></div>
<div class="m2"><p>آدمی شکلند و سه امت شدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک گره مستغرق مطلق شدست</p></div>
<div class="m2"><p>هم‌چو عیسی با ملک ملحق شدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقش آدم لیک معنی جبرئیل</p></div>
<div class="m2"><p>رسته از خشم و هوا و قال و قیل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از ریاضت رسته وز زهد و جهاد</p></div>
<div class="m2"><p>گوییا از آدمی او خود نزاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قسم دیگر با خران ملحق شدند</p></div>
<div class="m2"><p>خشم محض و شهوت مطلق شدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وصف جبریلی دریشان بود رفت</p></div>
<div class="m2"><p>تنگ بود آن خانه و آن وصف زفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرده گردد شخص کو بی‌جان شود</p></div>
<div class="m2"><p>خر شود چون جان او بی‌آن شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زانک جانی کان ندارد هست پست</p></div>
<div class="m2"><p>این سخن حقست و صوفی گفته است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او ز حیوانها فزون‌تر جان کند</p></div>
<div class="m2"><p>در جهان باریک کاریها کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مکر و تلبیسی که او داند تنید</p></div>
<div class="m2"><p>آن ز حیوان دیگر ناید پدید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جامه‌های زرکشی را بافتن</p></div>
<div class="m2"><p>درها از قعر دریا یافتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خرده‌کاریهای علم هندسه</p></div>
<div class="m2"><p>یا نجوم و علم طب و فلسفه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که تعلق با همین دنیاستش</p></div>
<div class="m2"><p>ره به هفتم آسمان بر نیستش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این همه علم بنای آخرست</p></div>
<div class="m2"><p>که عماد بود گاو و اشترست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بهر استبقای حیوان چند روز</p></div>
<div class="m2"><p>نام آن کردند این گیجان رموز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>علم راه حق و علم منزلش</p></div>
<div class="m2"><p>صاحب دل داند آن را با دلش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس درین ترکیب حیوان لطیف</p></div>
<div class="m2"><p>آفرید و کرد با دانش الیف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نام کالانعام کرد آن قوم را</p></div>
<div class="m2"><p>زانک نسبت کو بیقظه نوم را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روح حیوانی ندارد غیر نوم</p></div>
<div class="m2"><p>حسهای منعکس دارند قوم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یقظه آمد نوم حیوانی نماند</p></div>
<div class="m2"><p>انعکاس حس خود از لوح خواند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هم‌چو حس آنک خواب او را ربود</p></div>
<div class="m2"><p>چون شد او بیدار عکسیت نمود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لاجرم اسفل بود از سافلین</p></div>
<div class="m2"><p>ترک او کن لا احب الافلین</p></div></div>