---
title: >-
    بخش ۲۹ - تحریض سلیمان علیه‌السلام مر رسولان  را بر تعجیل به هجرت  بلقیس بهر ایمان
---
# بخش ۲۹ - تحریض سلیمان علیه‌السلام مر رسولان  را بر تعجیل به هجرت  بلقیس بهر ایمان

<div class="b" id="bn1"><div class="m1"><p>هم‌چنان که شه سلیمان در نبرد</p></div>
<div class="m2"><p>جذب خیل و لشکر بلقیس کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بیایید ای عزیزان زود زود</p></div>
<div class="m2"><p>که برآمد موجها از بحر جود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی ساحل می‌فشاند بی‌خطر</p></div>
<div class="m2"><p>جوش موجش هر زمانی صد گهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الصلا گفتیم ای اهل رشاد</p></div>
<div class="m2"><p>کین زمان رضوان در جنت گشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس سلیمان گفت ای پیکان روید</p></div>
<div class="m2"><p>سوی بلقیس و بدین دین بگروید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس بگوییدش بیا اینجا تمام</p></div>
<div class="m2"><p>زود که ان الله یدعوا بالسلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هین بیا ای طالب دولت شتاب</p></div>
<div class="m2"><p>که فتوحست این زمان و فتح باب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که تو طالب نه‌ای تو هم بیا</p></div>
<div class="m2"><p>تا طلب یابی ازین یار وفا</p></div></div>