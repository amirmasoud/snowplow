---
title: >-
    بخش ۱۵ - گفتن آن جهود علی را کرم الله وجهه کی اگر اعتماد داری بر حافظی حق از سر این کوشک خود را در انداز و جواب گفتن  امیرالمؤمنین او را
---
# بخش ۱۵ - گفتن آن جهود علی را کرم الله وجهه کی اگر اعتماد داری بر حافظی حق از سر این کوشک خود را در انداز و جواب گفتن  امیرالمؤمنین او را

<div class="b" id="bn1"><div class="m1"><p>مرتضی را گفت روزی یک عنود</p></div>
<div class="m2"><p>کو ز تعظیم خدا آگه نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر بامی و قصری بس بلند</p></div>
<div class="m2"><p>حفظ حق را واقفی ای هوشمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت آری او حفیظست و غنی</p></div>
<div class="m2"><p>هستی ما را ز طفلی و منی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت خود را اندر افکن هین ز بام</p></div>
<div class="m2"><p>اعتمادی کن بحفظ حق تمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا یقین گرددمرا ایقان تو</p></div>
<div class="m2"><p>و اعتقاد خوب با برهان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس امیرش گفت خامش کن برو</p></div>
<div class="m2"><p>تا نگردد جانت زین جرات گرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی رسد مر بنده را که با خدا</p></div>
<div class="m2"><p>آزمایش پیش آرد ز ابتلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده را کی زهره باشد کز فضول</p></div>
<div class="m2"><p>امتحان حق کند ای گیج گول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن خدا را می‌رسد کو امتحان</p></div>
<div class="m2"><p>پیش آرد هر دمی با بندگان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به ما ما را نماید آشکار</p></div>
<div class="m2"><p>که چه داریم از عقیده در سرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ آدم گفت حق را که ترا</p></div>
<div class="m2"><p>امتحان کردم درین جرم و خطا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ببینم غایت حلمت شها</p></div>
<div class="m2"><p>اه کرا باشد مجال این کرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عقل تو از بس که آمد خیره‌سر</p></div>
<div class="m2"><p>هست عذرت از گناه تو بتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنک او افراشت سقف آسمان</p></div>
<div class="m2"><p>تو چه دانی کردن او را امتحان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای ندانسته تو شر و خیر را</p></div>
<div class="m2"><p>امتحان خود را کن آنگه غیر را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>امتحان خود چو کردی ای فلان</p></div>
<div class="m2"><p>فارغ آیی ز امتحان دیگران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون بدانستی که شکردانه‌ای</p></div>
<div class="m2"><p>پس بدانی کاهل شکرخانه‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس بدان بی‌امتحانی که اله</p></div>
<div class="m2"><p>شکری نفرستدت ناجایگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این بدان بی‌امتحان از علم شاه</p></div>
<div class="m2"><p>چون سری نفرستدت در پایگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ عاقل افکند در ثمین</p></div>
<div class="m2"><p>در میان مستراحی پر چمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زانک گندم را حکیم آگهی</p></div>
<div class="m2"><p>هیچ نفرستد به انبار کهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شیخ را که پیشوا و رهبرست</p></div>
<div class="m2"><p>گر مریدی امتحان کرد او خرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امتحانش گر کنی در راه دین</p></div>
<div class="m2"><p>هم تو گردی ممتحن ای بی‌یقین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جرات و جهلت شود عریان و فاش</p></div>
<div class="m2"><p>او برهنه کی شود زان افتتاش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر بیاید ذره سنجد کوه را</p></div>
<div class="m2"><p>بر درد زان که ترازوش ای فتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کز قیاس خود ترازو می‌تند</p></div>
<div class="m2"><p>مرد حق را در ترازو می‌کند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون نگنجد او به میزان خرد</p></div>
<div class="m2"><p>پس ترازوی خرد را بر درد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امتحان هم‌چون تصرف دان درو</p></div>
<div class="m2"><p>تو تصرف بر چنان شاهی مجو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه تصرف کرد خواهد نقشها</p></div>
<div class="m2"><p>بر چنان نقاش بهر ابتلا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>امتحانی گر بدانست و بدید</p></div>
<div class="m2"><p>نی که هم نقاش آن بر وی کشید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه قدر باشد خود این صورت که بست</p></div>
<div class="m2"><p>پیش صورتها که در علم ویست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وسوسهٔ این امتحان چون آمدت</p></div>
<div class="m2"><p>بخت بد دان کآمد و گردن زدت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون چنین وسواس دیدی زود زود</p></div>
<div class="m2"><p>با خدا گرد و در آ اندر سجود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سجده گه را تر کن از اشک روان</p></div>
<div class="m2"><p>کای خدا تو وا رهانم زین گمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن زمان کت امتحان مطلوب شد</p></div>
<div class="m2"><p>مسجد دین تو پر خروب شد</p></div></div>