---
title: >-
    بخش ۱۶ - قصهٔ مسجد اقصی و خروب و عزم کردن داود علیه‌السلام پیش از سلیمان علیه‌السلام بر بنای آن مسجد
---
# بخش ۱۶ - قصهٔ مسجد اقصی و خروب و عزم کردن داود علیه‌السلام پیش از سلیمان علیه‌السلام بر بنای آن مسجد

<div class="b" id="bn1"><div class="m1"><p>چون درآمد عزم داودی به تنگ</p></div>
<div class="m2"><p>که بسازد مسجد اقصی به سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وحی کردش حق که ترک این بخوان</p></div>
<div class="m2"><p>که ز دستت برنیاید این مکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در تقدیر ما آنک تو این</p></div>
<div class="m2"><p>مسجد اقصی بر آری ای گزین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت جرمم چیست ای دانای راز</p></div>
<div class="m2"><p>که مرا گویی که مسجد را مساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت بی‌جرمی تو خونها کرده‌ای</p></div>
<div class="m2"><p>خون مظلومان بگردن برده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ز آواز تو خلقی بی‌شمار</p></div>
<div class="m2"><p>جان بدادند و شدند آن را شکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون بسی رفتست بر آواز تو</p></div>
<div class="m2"><p>بر صدای خوب جان‌پرداز تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت مغلوب تو بودم مست تو</p></div>
<div class="m2"><p>دست من بر بسته بود از دست تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه که هر مغلوب شه مرحوم بود</p></div>
<div class="m2"><p>نه که المغلوب کالمعدوم بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت این مغلوب معدومیست کو</p></div>
<div class="m2"><p>جز به نسبت نیست معدوم ایقنوا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این چنین معدوم کو از خویش رفت</p></div>
<div class="m2"><p>بهترین هستها افتاد و زفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او به نسبت با صفات حق فناست</p></div>
<div class="m2"><p>در حقیقت در فنا او را بقاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جملهٔ ارواح در تدبیر اوست</p></div>
<div class="m2"><p>جملهٔ اشباح هم در تیر اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنک او مغلوب اندر لطف ماست</p></div>
<div class="m2"><p>نیست مضطر بلک مختار ولاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منتهای اختیار آنست خود</p></div>
<div class="m2"><p>که اختیارش گردد اینجا مفتقد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اختیاری را نبودی چاشنی</p></div>
<div class="m2"><p>گر نگشتی آخر او محو از منی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در جهان گر لقمه و گر شربتست</p></div>
<div class="m2"><p>لذت او فرع محو لذتست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه از لذات بی‌تاثیر شد</p></div>
<div class="m2"><p>لذتی بود او و لذت‌گیر شد</p></div></div>