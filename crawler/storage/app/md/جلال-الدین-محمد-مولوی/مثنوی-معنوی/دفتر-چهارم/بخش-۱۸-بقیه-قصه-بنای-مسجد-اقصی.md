---
title: >-
    بخش ۱۸ - بقیهٔ قصهٔ بنای مسجد اقصی
---
# بخش ۱۸ - بقیهٔ قصهٔ بنای مسجد اقصی

<div class="b" id="bn1"><div class="m1"><p>چون سلیمان کرد آغاز بنا</p></div>
<div class="m2"><p>پاک چون کعبه همایون چون منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بنااش دیده می‌شد کر و فر</p></div>
<div class="m2"><p>نی فسرده چون بناهای دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بنا هر سنگ کز که می‌سکست</p></div>
<div class="m2"><p>فاش سیروا بی‌همی گفت از نخست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم‌چو از آب و گل آدم‌کده</p></div>
<div class="m2"><p>نور ز آهک پاره‌ها تابان شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ بی‌حمال آینده شده</p></div>
<div class="m2"><p>وان در و دیوارها زنده شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق همی‌گوید که دیوار بهشت</p></div>
<div class="m2"><p>نیست چون دیوارها بی‌جان و زشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون در و دیوار تن با آگهیست</p></div>
<div class="m2"><p>زنده باشد خانه چون شاهنشهیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم درخت و میوه هم آب زلال</p></div>
<div class="m2"><p>با بهشتی در حدیث و در مقال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانک جنت را نه ز آلت بسته‌اند</p></div>
<div class="m2"><p>بلک از اعمال و نیت بسته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این بنا ز آب و گل مرده بدست</p></div>
<div class="m2"><p>وان بنا از طاعت زنده شدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این به اصل خویش ماند پرخلل</p></div>
<div class="m2"><p>وان به اصل خود که علمست و عمل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم سریر و قصر و هم تاج و ثیاب</p></div>
<div class="m2"><p>با بهشتی در سؤال و در جواب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرش بی‌فراش پیچیده شود</p></div>
<div class="m2"><p>خانه بی‌مکناس روبیده شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خانهٔ دل بین ز غم ژولیده شد</p></div>
<div class="m2"><p>بی‌کناس از توبه‌ای روبیده شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تخت او سیار بی‌حمال شد</p></div>
<div class="m2"><p>حلقه و در مطرب و قوال شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست در دل زندگی دارالخلود</p></div>
<div class="m2"><p>در زبانم چون نمی‌آید چه سود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون سلیمان در شدی هر بامداد</p></div>
<div class="m2"><p>مسجد اندر بهر ارشاد عباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پند دادی گه بگفت و لحن و ساز</p></div>
<div class="m2"><p>گه به فعل اعنی رکوعی یا نماز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پند فعلی خلق را جذاب‌تر</p></div>
<div class="m2"><p>که رسد در جان هر باگوش و کر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندر آن وهم امیری کم بود</p></div>
<div class="m2"><p>در حشم تاثیر آن محکم بود</p></div></div>