---
title: >-
    بخش ۱۲۵ - قصهٔ فرزندان عزیر علیه‌السلام کی از پدر احوال پدر می‌پرسیدند می‌گفت آری دیدمش می‌آید بعضی شناختندش بیهوش شدند بعضی نشناختند می‌گفتند خود مژده‌ای داد این بیهوش شدن  چیست
---
# بخش ۱۲۵ - قصهٔ فرزندان عزیر علیه‌السلام کی از پدر احوال پدر می‌پرسیدند می‌گفت آری دیدمش می‌آید بعضی شناختندش بیهوش شدند بعضی نشناختند می‌گفتند خود مژده‌ای داد این بیهوش شدن  چیست

<div class="b" id="bn1"><div class="m1"><p>همچو پوران عزیر اندر گذر</p></div>
<div class="m2"><p>آمده پرسان ز احوال پدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشته ایشان پیر و باباشان جوان</p></div>
<div class="m2"><p>پس پدرشان پیش آمد ناگهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس بپرسیدند ازو کای ره‌گذر</p></div>
<div class="m2"><p>از عزیر ما عجب داری خبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که کسی‌مان گفت که امروز آن سند</p></div>
<div class="m2"><p>بعد نومیدی ز بیرون می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت آری بعد من خواهد رسید</p></div>
<div class="m2"><p>آن یکی خوش شد چو این مژده شنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بانگ می‌زد کای مبشر باش شاد</p></div>
<div class="m2"><p>وان دگر بشناخت بیهوش اوفتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که چه جای مژده است ای خیره‌سر</p></div>
<div class="m2"><p>که در افتادیم در کان شکر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وهم را مژده‌ست و پیش عقل نقد</p></div>
<div class="m2"><p>ز انک چشم وهم شد محجوب فقد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کافران را درد و مؤمن را بشیر</p></div>
<div class="m2"><p>لیک نقد حال در چشم بصیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانک عاشق در دم نقدست مست</p></div>
<div class="m2"><p>لاجرم از کفر و ایمان برترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کفر و ایمان هر دو خود دربان اوست</p></div>
<div class="m2"><p>کوست مغز و کفر و دین او را دو پوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کفر قشر خشک رو بر تافته</p></div>
<div class="m2"><p>باز ایمان قشر لذت یافته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قشرهای خشک را جا آتش است</p></div>
<div class="m2"><p>قشر پیوسته به مغز جان خوش است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مغز خود از مرتبهٔ خوش برترست</p></div>
<div class="m2"><p>برترست از خوش که لذت گسترست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این سخن پایان ندارد باز گرد</p></div>
<div class="m2"><p>تا برآرد موسیم از بحر گرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درخور عقل عوام این گفته شد</p></div>
<div class="m2"><p>از سخن باقی آن بنهفته شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زر عقلت ریزه است ای متهم</p></div>
<div class="m2"><p>بر قراضه مهر سکه چون نهم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عقل تو قسمت شده بر صد مهم</p></div>
<div class="m2"><p>بر هزاران آرزو و طم و رم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جمع باید کرد اجزا را به عشق</p></div>
<div class="m2"><p>تا شوی خوش چون سمرقند و دمشق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جو جوی چون جمع گردی ز اشتباه</p></div>
<div class="m2"><p>پس توان زد بر تو سکهٔ پادشاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور ز مثقالی شوی افزون تو خام</p></div>
<div class="m2"><p>از تو سازد شه یکی زرینه جام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس برو هم نام و هم القاب شاه</p></div>
<div class="m2"><p>باشد و هم صورتش ای وصل خواه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا که معشوقت بود هم نان هم آب</p></div>
<div class="m2"><p>هم چراغ و شاهد و نقل شراب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جمع کن خود را جماعت رحمتست</p></div>
<div class="m2"><p>تا توانم با تو گفتن آنچ هست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زانک گفتن از برای باوریست</p></div>
<div class="m2"><p>جان شرک از باوری حق بریست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جان قسمت گشته بر حشو فلک</p></div>
<div class="m2"><p>در میان شصت سودا مشترک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس خموشی به دهد او را ثبوت</p></div>
<div class="m2"><p>پس جواب احمقان آمد سکوت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این همی‌دانم ولی مستی تن</p></div>
<div class="m2"><p>می‌گشاید بی‌مراد من دهن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنچنان که از عطسه و از خامیاز</p></div>
<div class="m2"><p>این دهان گردد بناخواه تو باز</p></div></div>