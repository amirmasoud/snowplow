---
title: >-
    بخش ۵۲ - قصهٔ رستن خروب در گوشهٔ مسجد اقصی و غمگین شدن سلیمان علیه‌السلام از آن چون به سخن آمد با او و خاصیت و نام  خود بگفت
---
# بخش ۵۲ - قصهٔ رستن خروب در گوشهٔ مسجد اقصی و غمگین شدن سلیمان علیه‌السلام از آن چون به سخن آمد با او و خاصیت و نام  خود بگفت

<div class="b" id="bn1"><div class="m1"><p>پس سلیمان دید اندر گوشه‌ای</p></div>
<div class="m2"><p>نوگیاهی رسته هم‌چون خوشه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید بس نادر گیاهی سبز و تر</p></div>
<div class="m2"><p>می‌ربود آن سبزیش نور از بصر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس سلامش کرد در حال آن حشیش</p></div>
<div class="m2"><p>او جوابش گفت و بشکفت از خوشیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت نامت چیست برگو بی‌دهان</p></div>
<div class="m2"><p>گفت خروبست ای شاه جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت اندر تو چه خاصیت بود</p></div>
<div class="m2"><p>گفت من رستم مکان ویران شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که خروبم خراب منزلم</p></div>
<div class="m2"><p>هادم بنیاد این آب و گلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس سلیمان آن زمان دانست زود</p></div>
<div class="m2"><p>که اجل آمد سفر خواهد نمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت تا من هستم این مسجد یقین</p></div>
<div class="m2"><p>در خلل ناید ز آفات زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که من باشم وجود من بود</p></div>
<div class="m2"><p>مسجداقصی مخلخل کی شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس که هدم مسجد ما بی‌گمان</p></div>
<div class="m2"><p>نبود الا بعد مرگ ما بدان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مسجدست آن دل که جسمش ساجدست</p></div>
<div class="m2"><p>یار بد خروب هر جا مسجدست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یار بد چون رست در تو مهر او</p></div>
<div class="m2"><p>هین ازو بگریز و کم کن گفت وگو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برکن از بیخش که گر سر بر زند</p></div>
<div class="m2"><p>مر ترا و مسجدت را بر کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشقا خروب تو آمد کژی</p></div>
<div class="m2"><p>هم‌چو طفلان سوی کژ چون می‌غژی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خویش مجرم دان و مجرم گو مترس</p></div>
<div class="m2"><p>تا ندزدد از تو آن استاد درس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بگویی جاهلم تعلیم ده</p></div>
<div class="m2"><p>این چنین انصاف از ناموس به</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از پدر آموز ای روشن‌جبین</p></div>
<div class="m2"><p>ربنا گفت و ظلمنا پیش ازین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه بهانه کرد و نه تزویر ساخت</p></div>
<div class="m2"><p>نه لوای مکر و حیلت بر فراخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باز آن ابلیس بحث آغاز کرد</p></div>
<div class="m2"><p>که بدم من سرخ رو کردیم زرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رنگ رنگ تست صباغم توی</p></div>
<div class="m2"><p>اصل جرم و آفت و داغم توی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هین بخوان رب بما اغویتنی</p></div>
<div class="m2"><p>تا نگردی جبری و کژ کم تنی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر درخت جبر تا کی بر جهی</p></div>
<div class="m2"><p>اختیار خویش را یک‌سو نهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم‌چو آن ابلیس و ذریات او</p></div>
<div class="m2"><p>با خدا در جنگ و اندر گفت و گو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون بود اکراه با چندان خوشی</p></div>
<div class="m2"><p>که تو در عصیان همی دامن کشی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن‌چنان خوش کس رود در مکرهی</p></div>
<div class="m2"><p>کس چنان رقصان دود در گم‌رهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیست مرده جنگ می‌کردی در آن</p></div>
<div class="m2"><p>کت همی‌دادند پند آن دیگران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که صواب اینست و راه اینست و بس</p></div>
<div class="m2"><p>کی زند طعنه مرا جز هیچ‌کس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کی چنین گوید کسی کو مکر هست</p></div>
<div class="m2"><p>چون چنین جنگد کسی کو بی‌رهست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر چه نفست خواست داری اختیار</p></div>
<div class="m2"><p>هر چه عقلت خواست آری اضطرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>داند او کو نیک‌بخت و محرمست</p></div>
<div class="m2"><p>زیرکی ز ابلیس و عشق از آدمست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زیرکی سباحی آمد در بحار</p></div>
<div class="m2"><p>کم رهد غرقست او پایان کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هل سباحت را رها کن کبر و کین</p></div>
<div class="m2"><p>نیست جیحون نیست جو دریاست این</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وانگهان دریای ژرف بی‌پناه</p></div>
<div class="m2"><p>در رباید هفت دریا را چو کاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عشق چون کشتی بود بهر خواص</p></div>
<div class="m2"><p>کم بود آفت بود اغلب خلاص</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زیرکی بفروش و حیرانی بخر</p></div>
<div class="m2"><p>زیرکی ظنست و حیرانی نظر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عقل قربان کن به پیش مصطفی</p></div>
<div class="m2"><p>حسبی الله گو که الله‌ام کفی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم‌چو کنعان سر ز کشتی وا مکش</p></div>
<div class="m2"><p>که غرورش داد نفس زیرکش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که برآیم بر سر کوه مشید</p></div>
<div class="m2"><p>منت نوحم چرا باید کشید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون رمی از منتش بر جان ما</p></div>
<div class="m2"><p>چونک شکر و منتش گوید خدا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو چه دانی ای غرارهٔ پر حسد</p></div>
<div class="m2"><p>منت او را خدا هم می‌کشد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کاشکی او آشنا ناموختی</p></div>
<div class="m2"><p>تا طمع در نوح و کشتی دوختی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کاش چون طفل از حیل جاهل بدی</p></div>
<div class="m2"><p>تا چو طفلان چنگ در مادر زدی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یا به علم نقل کم بودی ملی</p></div>
<div class="m2"><p>علم وحی دل ربودی از ولی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با چنین نوری چو پیش آری کتاب</p></div>
<div class="m2"><p>جان وحی آسای تو آرد عتاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون تیمم با وجود آب دان</p></div>
<div class="m2"><p>علم نقلی با دم قطب زمان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خویش ابله کن تبع می‌رو سپس</p></div>
<div class="m2"><p>رستگی زین ابلهی یابی و بس</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اکثر اهل الجنه البله ای پسر</p></div>
<div class="m2"><p>بهر این گفتست سلطان البشر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زیرکی چون کبر و باد انگیز تست</p></div>
<div class="m2"><p>ابلهی شو تا بماند دل درست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ابلهی نه کو به مسخرگی دوتوست</p></div>
<div class="m2"><p>ابلهی کو واله و حیران هوست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ابلهان‌اند آن زنان دست بر</p></div>
<div class="m2"><p>از کف ابله وز رخ یوسف نذر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عقل را قربان کن اندر عشق دوست</p></div>
<div class="m2"><p>عقلها باری از آن سویست کوست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عقلها آن سو فرستاده عقول</p></div>
<div class="m2"><p>مانده این سو که نه معشوقست گول</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زین سر از حیرت گر این عقلت رود</p></div>
<div class="m2"><p>هر سو مویت سر و عقلی شود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نیست آن سو رنج فکرت بر دماغ</p></div>
<div class="m2"><p>که دماغ و عقل روید دشت و باغ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سوی دشت از دشت نکته بشنوی</p></div>
<div class="m2"><p>سوی باغ آیی شود نخلت روی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اندرین ره ترک کن طاق و طرنب</p></div>
<div class="m2"><p>تا قلاوزت نجنبد تو مجنب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر که او بی سر بجنبد دم بود</p></div>
<div class="m2"><p>جنبشش چون جنبش کزدم بود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کژرو و شب کور و زشت و زهرناک</p></div>
<div class="m2"><p>پیشهٔ او خستن اجسام پاک</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سر بکوب آن را که سرش این بود</p></div>
<div class="m2"><p>خلق و خوی مستمرش این بود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خود صلاح اوست آن سر کوفتن</p></div>
<div class="m2"><p>تا رهد جان‌ریزه‌اش زان شوم‌تن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>واستان آن دست دیوانه سلاح</p></div>
<div class="m2"><p>تا ز تو راضی شود عدل و صلاح</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون سلاحش هست و عقلش نه ببند</p></div>
<div class="m2"><p>دست او را ورنه آرد صد گزند</p></div></div>