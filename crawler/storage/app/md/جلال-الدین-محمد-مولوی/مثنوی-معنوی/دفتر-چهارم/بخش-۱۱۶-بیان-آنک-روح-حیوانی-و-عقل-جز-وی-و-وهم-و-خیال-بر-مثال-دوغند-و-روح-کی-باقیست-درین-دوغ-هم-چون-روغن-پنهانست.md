---
title: >-
    بخش ۱۱۶ - بیان آنک روح حیوانی و عقل جز وی و وهم و خیال بر مثال دوغند و روح کی باقیست درین دوغ هم‌چون روغن پنهانست
---
# بخش ۱۱۶ - بیان آنک روح حیوانی و عقل جز وی و وهم و خیال بر مثال دوغند و روح کی باقیست درین دوغ هم‌چون روغن پنهانست

<div class="b" id="bn1"><div class="m1"><p>جوهر صدقت خفی شد در دروغ</p></div>
<div class="m2"><p>هم‌چو طعم روغن اندر طعم دوغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دروغت این تن فانی بود</p></div>
<div class="m2"><p>راستت آن جان ربانی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها این دوغ تن پیدا و فاش</p></div>
<div class="m2"><p>روغن جان اندرو فانی و لاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا فرستد حق رسولی بنده‌ای</p></div>
<div class="m2"><p>دوغ را در خمره جنباننده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بجنباند به هنجار و به فن</p></div>
<div class="m2"><p>تا بدانم من که پنهان بود من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا کلام بنده‌ای کان جزو اوست</p></div>
<div class="m2"><p>در رود در گوش او کو وحی جوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اذن مؤمن وحی ما را واعیست</p></div>
<div class="m2"><p>آنچنان گوشی قرین داعیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم‌چنانک گوش طفل از گفت مام</p></div>
<div class="m2"><p>پر شود ناطق شود او درکلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور نباشد طفل را گوش رشد</p></div>
<div class="m2"><p>گفت مادر نشنود گنگی شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دایما هر کر اصلی گنگ بود</p></div>
<div class="m2"><p>ناطق آنکس شد که از مادر شنود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دانک گوش کر و گنگ از آفتیست</p></div>
<div class="m2"><p>که پذیرای دم و تعلیم نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنک بی‌تعلیم بد ناطق خداست</p></div>
<div class="m2"><p>که صفات او ز علتها جداست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا چو آدم کرده تلقینش خدا</p></div>
<div class="m2"><p>بی‌حجاب مادر و دایه و ازا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا مسیحی که به تعلیم ودود</p></div>
<div class="m2"><p>در ولادت ناطق آمد در وجود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از برای دفع تهمت در ولاد</p></div>
<div class="m2"><p>که نزادست از زنا و از فساد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جنبشی بایست اندر اجتهاد</p></div>
<div class="m2"><p>تا که دوغ آن روغن از دل باز داد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روغن اندر دوغ باشد چون عدم</p></div>
<div class="m2"><p>دوغ در هستی برآورده علم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنک هستت می‌نماید هست پوست</p></div>
<div class="m2"><p>وآنک فانی می‌نماید اصل اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوغ روغن ناگرفتست و کهن</p></div>
<div class="m2"><p>تا بنگزینی بنه خرجش مکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هین بگردانش به دانش دست دست</p></div>
<div class="m2"><p>تا نماید آنچ پنهان کرده است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زآنک این فانی دلیل باقیست</p></div>
<div class="m2"><p>لابهٔ مستان دلیل ساقیست</p></div></div>