---
title: >-
    بخش ۱۰۰ - بیان این خبر کی کلموا الناس علی قدر عقولهم لا علی قدر عقولکم حتی لا یکذبوا الله و رسوله
---
# بخش ۱۰۰ - بیان این خبر کی کلموا الناس علی قدر عقولهم لا علی قدر عقولکم حتی لا یکذبوا الله و رسوله

<div class="b" id="bn1"><div class="m1"><p>چونک با کودک سر و کارم فتاد</p></div>
<div class="m2"><p>هم زبان کودکان باید گشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که برو کتاب تا مرغت خرم</p></div>
<div class="m2"><p>یا مویز و جوز و فستق آورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز شباب تن نمی‌دانی به کیر</p></div>
<div class="m2"><p>این جوانی را بگیر ای خر شعیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ آژنگی نیفتد بر رخت</p></div>
<div class="m2"><p>تازه ماند آن شباب فرخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه نژند پیریت آید برو</p></div>
<div class="m2"><p>نه قد چون سرو تو گردد دوتو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه شود زور جوانی از تو کم</p></div>
<div class="m2"><p>نه به دندانها خللها یا الم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه کمی در شهوت و طمث و بعال</p></div>
<div class="m2"><p>که زنان را آید از ضعفت ملال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچنان بگشایدت فر شباب</p></div>
<div class="m2"><p>که گشود آن مژدهٔ عکاشه باب</p></div></div>