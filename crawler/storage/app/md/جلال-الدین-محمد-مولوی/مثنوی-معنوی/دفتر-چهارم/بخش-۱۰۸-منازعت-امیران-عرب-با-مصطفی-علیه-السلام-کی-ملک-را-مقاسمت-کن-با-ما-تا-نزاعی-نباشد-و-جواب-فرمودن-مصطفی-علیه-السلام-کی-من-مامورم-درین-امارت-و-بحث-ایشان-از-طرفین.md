---
title: >-
    بخش ۱۰۸ - منازعت امیران عرب با مصطفی علیه‌السلام کی ملک را مقاسمت کن با ما تا نزاعی نباشد و جواب فرمودن مصطفی علیه‌السلام کی من مامورم درین امارت و بحث ایشان از طرفین
---
# بخش ۱۰۸ - منازعت امیران عرب با مصطفی علیه‌السلام کی ملک را مقاسمت کن با ما تا نزاعی نباشد و جواب فرمودن مصطفی علیه‌السلام کی من مامورم درین امارت و بحث ایشان از طرفین

<div class="b" id="bn1"><div class="m1"><p>آن امیران عرب گرد آمدند</p></div>
<div class="m2"><p>نزد پیغامبر منازع می‌شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که تو میری هر یک از ما هم امیر</p></div>
<div class="m2"><p>بخش کن این ملک و بخش خود بگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر یکی در بخش خود انصاف‌جو</p></div>
<div class="m2"><p>تو ز بخش ما دو دست خود بشو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت میری مر مرا حق داده است</p></div>
<div class="m2"><p>سروری و امر مطلق داده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کین قران احمدست و دور او</p></div>
<div class="m2"><p>هین بگیرید امر او را اتقوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قوم گفتندش که ما هم زان قضا</p></div>
<div class="m2"><p>حاکمیم و داد امیریمان خدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت لیکن مر مرا حق ملک داد</p></div>
<div class="m2"><p>مر شما را عاریه از بهر زاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میری من تا قیامت باقیست</p></div>
<div class="m2"><p>میری عاریتی خواهد شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوم گفتند ای امیر افزون مگو</p></div>
<div class="m2"><p>چیست حجت بر فزون‌جویی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در زمان ابری برآمد ز امر مر</p></div>
<div class="m2"><p>سیل آمد گشت آن اطراف پر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رو به شهر آورد سیل بس مهیب</p></div>
<div class="m2"><p>اهل شهر افغان‌کنان جمله رعیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت پیغامبر که وقت امتحان</p></div>
<div class="m2"><p>آمد اکنون تا گمارد گردد عیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر امیری نیزهٔ خود در فکند</p></div>
<div class="m2"><p>تا شود در امتحان آن سیل‌بند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس قضیب انداخت در وی مصطفی</p></div>
<div class="m2"><p>آن قضیب معجز فرمان روا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیزه‌ها را هم‌چو خاشاکی ربود</p></div>
<div class="m2"><p>آب تیز سیل پرجوش عنود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیزه‌ها گم گشت جمله و آن قضیب</p></div>
<div class="m2"><p>بر سر آب ایستاده چون رقیب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز اهتمام آن قضیب آن سیل زفت</p></div>
<div class="m2"><p>روبگردانید و آن سیلاب رفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون بدیدند از وی آن امر عظیم</p></div>
<div class="m2"><p>پس مقر گشتند آن میران ز بیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز سه کس که حقد ایشان چیره بود</p></div>
<div class="m2"><p>ساحرش گفتند و کاهی از جحود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملک بر بسته چنان باشد ضعیف</p></div>
<div class="m2"><p>ملک بر رسته چنین باشد شریف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیزه‌ها را گر ندیدی با قضیب</p></div>
<div class="m2"><p>نامشان بین نام او بین این نجیب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نامشان را سیل تیز مرگ برد</p></div>
<div class="m2"><p>نام او و دولت تیزش نمرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پنج نوبت می‌زنندش بر دوام</p></div>
<div class="m2"><p>هم‌چنین هر روز تا روز قیام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر ترا عقلست کردم لطفها</p></div>
<div class="m2"><p>ور خری آورده‌ام خر را عصا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنچنان زین آخرت بیرون کنم</p></div>
<div class="m2"><p>کز عصا گوش و سرت پر خون کنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندرین آخر خران و مردمان</p></div>
<div class="m2"><p>می‌نیابند از جفای تو امان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نک عصا آورده‌ام بهر ادب</p></div>
<div class="m2"><p>هر خری را کو نباشد مستحب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اژدهایی می‌شود در قهر تو</p></div>
<div class="m2"><p>که اژدهایی گشته‌ای در فعل و خو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اژدهای کوهیی تو بی‌امان</p></div>
<div class="m2"><p>لیک بنگر اژدهای آسمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این عصا از دوزخ آمد چاشنی</p></div>
<div class="m2"><p>که هلا بگریز اندر روشنی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ورنه در مانی تو در دندان من</p></div>
<div class="m2"><p>مخلصت نبود ز در بندان من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این عصایی بود این دم اژدهاست</p></div>
<div class="m2"><p>تا نگویی دوزخ یزدان کجاست</p></div></div>