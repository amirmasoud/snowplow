---
title: >-
    بخش ۷۱ - آشفتن آن غلام از نارسیدن جواب رقعه از قبل پادشاه
---
# بخش ۷۱ - آشفتن آن غلام از نارسیدن جواب رقعه از قبل پادشاه

<div class="b" id="bn1"><div class="m1"><p>این بیابان خود ندارد پا و سر</p></div>
<div class="m2"><p>بی‌جواب نامه خستست آن پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای عجب چونم نداد آن شه جواب</p></div>
<div class="m2"><p>با خیانت کرد رقعه‌بر ز تاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقعه پنهان کرد و ننمود آن به شاه</p></div>
<div class="m2"><p>کو منافق بود و آبی زیر کاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقعهٔ دیگر نویسم ز آزمون</p></div>
<div class="m2"><p>دیگری جویم رسول ذو فنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر امیر و مطبخی و نامه‌بر</p></div>
<div class="m2"><p>عیب بنهاده ز جهل آن بی‌خبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ گرد خود نمی‌گردد که من</p></div>
<div class="m2"><p>کژروی کردم چو اندر دین شمن</p></div></div>