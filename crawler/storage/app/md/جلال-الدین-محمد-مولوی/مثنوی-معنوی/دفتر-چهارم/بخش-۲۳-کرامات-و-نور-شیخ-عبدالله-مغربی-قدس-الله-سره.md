---
title: >-
    بخش ۲۳ - کرامات و نور شیخ عبدالله مغربی قدس الله سره
---
# بخش ۲۳ - کرامات و نور شیخ عبدالله مغربی قدس الله سره

<div class="b" id="bn1"><div class="m1"><p>گفت عبدالله شیخ مغربی</p></div>
<div class="m2"><p>شصت سال از شب ندیدم من شبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ندیدم ظلمتی در شصت سال</p></div>
<div class="m2"><p>نه به روز و نه به شب نه ز اعتلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صوفیان گفتند صدق قال او</p></div>
<div class="m2"><p>شب همی‌رفتیم در دنبال او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بیابانهای پر از خار و گو</p></div>
<div class="m2"><p>او چو ماه بدر ما را پیش‌رو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی پس ناکرده می‌گفتی به شب</p></div>
<div class="m2"><p>هین گو آمد میل کن در سوی چپ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز گفتی بعد یک دم سوی راست</p></div>
<div class="m2"><p>میل کن زیرا که خاری پیش پاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز گشتی پاش را ما پای‌بوس</p></div>
<div class="m2"><p>گشته و پایش چو پاهای عروس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه ز خاک و نه ز گل بر وی اثر</p></div>
<div class="m2"><p>نه از خراش خار و آسیب حجر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغربی را مشرقی کرده خدای</p></div>
<div class="m2"><p>کرده مغرب را چو مشرق نورزای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور این شمس شموسی فارس است</p></div>
<div class="m2"><p>روز خاص و عام را او حارس است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نباشد حارس آن نور مجید</p></div>
<div class="m2"><p>که هزاران آفتاب آرد پدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو به نور او همی رو در امان</p></div>
<div class="m2"><p>در میان اژدها و کزدمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش پیشت می‌رود آن نور پاک</p></div>
<div class="m2"><p>می‌کند هر ره‌زنی را چاک‌چاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یوم لا یخزی النبی راست دان</p></div>
<div class="m2"><p>نور یسعی بین ایدیهم بخوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرچه گردد در قیامت آن فزون</p></div>
<div class="m2"><p>از خدا اینجا بخواهید آزمون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کو ببخشد هم به میغ و هم به ماغ</p></div>
<div class="m2"><p>نور جان والله اعلم بالبلاغ</p></div></div>