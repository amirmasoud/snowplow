---
title: >-
    بخش ۶۴ - زجر مدعی از دعوی و امر کردن او را به متابعت
---
# بخش ۶۴ - زجر مدعی از دعوی و امر کردن او را به متابعت

<div class="b" id="bn1"><div class="m1"><p>بو مسیلم گفت خود من احمدم</p></div>
<div class="m2"><p>دین احمد را به فن برهم زدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بو مسیلم را بگو کم کن بطر</p></div>
<div class="m2"><p>غرهٔ اول مشو آخر نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این قلاوزی مکن از حرص جمع</p></div>
<div class="m2"><p>پس‌روی کن تا رود در پیش شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع مقصد را نماید هم‌چو ماه</p></div>
<div class="m2"><p>کین طرف دانه‌ست یا خود دامگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بخواهی ور نخواهی با چراغ</p></div>
<div class="m2"><p>دیده گردد نقش باز و نقش زاغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ورنه این زاغان دغل افروختند</p></div>
<div class="m2"><p>بانگ بازان سپید آموختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بانگ هدهد گر بیاموزد فتی</p></div>
<div class="m2"><p>راز هدهد کو و پیغام سبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بانگ بر رسته ز بر بسته بدان</p></div>
<div class="m2"><p>تاج شاهان را ز تاج هدهدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرف درویشان و نکتهٔ عارفان</p></div>
<div class="m2"><p>بسته‌اند این بی‌حیایان بر زبان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر هلاک امت پیشین که بود</p></div>
<div class="m2"><p>زانک چندل را گمان بردند عود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بودشان تمییز کان مظهر کند</p></div>
<div class="m2"><p>لیک حرص و آز کور و کر کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوری کوران ز رحمت دور نیست</p></div>
<div class="m2"><p>کوری حرص است که آن معذور نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چارمیخ شه ز رحمت دور نی</p></div>
<div class="m2"><p>چار میخ حاسدی مغفور نی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماهیا آخر نگر بنگر بشست</p></div>
<div class="m2"><p>بدگلویی چشم آخربینت بست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با دو دیده اول و آخر ببین</p></div>
<div class="m2"><p>هین مباش اعور چو ابلیس لعین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اعور آن باشد که حالی دید و بس</p></div>
<div class="m2"><p>چون بهایم بی‌خبر از بازپس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون دو چشم گاو در جرم تلف</p></div>
<div class="m2"><p>هم‌چو یک چشمست کش نبود شرف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نصف قیمت ارزد آن دو چشم او</p></div>
<div class="m2"><p>که دو چشمش راست مسند چشم تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور کنی یک چشم آدم‌زاده‌ای</p></div>
<div class="m2"><p>نصف قیمت لایقست از جاده‌ای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زانک چشم آدمی تنها به خود</p></div>
<div class="m2"><p>بی دو چشم یار کاری می‌کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشم خر چون اولش بی آخرست</p></div>
<div class="m2"><p>گر دو چشمش هست حکمش اعورست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این سخن پایان ندارد وان خفیف</p></div>
<div class="m2"><p>می‌نویسد رقعه در طمع رغیف</p></div></div>