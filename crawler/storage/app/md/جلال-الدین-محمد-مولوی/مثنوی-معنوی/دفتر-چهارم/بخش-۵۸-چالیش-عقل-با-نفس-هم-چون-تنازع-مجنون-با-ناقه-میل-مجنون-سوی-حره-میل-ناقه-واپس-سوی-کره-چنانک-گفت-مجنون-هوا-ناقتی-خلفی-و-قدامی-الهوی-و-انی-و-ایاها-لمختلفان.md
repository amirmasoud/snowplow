---
title: >-
    بخش ۵۸ - چالیش عقل با نفس هم چون تنازع  مجنون با ناقه میل مجنون  سوی حره میل ناقه واپس سوی کره  چنانک گفت مجنون هوا ناقتی خلفی و قدامی الهوی  و انی و ایاها لمختلفان
---
# بخش ۵۸ - چالیش عقل با نفس هم چون تنازع  مجنون با ناقه میل مجنون  سوی حره میل ناقه واپس سوی کره  چنانک گفت مجنون هوا ناقتی خلفی و قدامی الهوی  و انی و ایاها لمختلفان

<div class="b" id="bn1"><div class="m1"><p>هم‌چو مجنون‌اند و چون ناقه‌ش یقین</p></div>
<div class="m2"><p>می‌کشد آن پیش و این واپس به کین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل مجنون پیش آن لیلی روان</p></div>
<div class="m2"><p>میل ناقه پس پی کره دوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک دم ار مجنون ز خود غافل بدی</p></div>
<div class="m2"><p>ناقه گردیدی و واپس آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق و سودا چونک پر بودش بدن</p></div>
<div class="m2"><p>می‌نبودش چاره از بی‌خود شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنک او باشد مراقب عقل بود</p></div>
<div class="m2"><p>عقل را سودای لیلی در ربود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک ناقه بس مراقب بود و چست</p></div>
<div class="m2"><p>چون بدیدی او مهار خویش سست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فهم کردی زو که غافل گشت و دنگ</p></div>
<div class="m2"><p>رو سپس کردی به کره بی‌درنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون به خود باز آمدی دیدی ز جا</p></div>
<div class="m2"><p>کو سپس رفتست بس فرسنگها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سه روزه ره بدین احوالها</p></div>
<div class="m2"><p>ماند مجنون در تردد سالها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت ای ناقه چو هر دو عاشقیم</p></div>
<div class="m2"><p>ما دو ضد پس همره نالایقیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیستت بر وفق من مهر و مهار</p></div>
<div class="m2"><p>کرد باید از تو صحبت اختیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این دو همره یکدگر را راه‌زن</p></div>
<div class="m2"><p>گمره آن جان کو فرو ناید ز تن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان ز هجر عرش اندر فاقه‌ای</p></div>
<div class="m2"><p>تن ز عشق خاربن چون ناقه‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان گشاید سوی بالا بالها</p></div>
<div class="m2"><p>در زده تن در زمین چنگالها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا تو با من باشی ای مردهٔ وطن</p></div>
<div class="m2"><p>پس ز لیلی دور ماند جان من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزگارم رفت زین گون حالها</p></div>
<div class="m2"><p>هم‌چو تیه و قوم موسی سالها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خطوتینی بود این ره تا وصال</p></div>
<div class="m2"><p>مانده‌ام در ره ز شستت شصت سال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راه نزدیک و بماندم سخت دیر</p></div>
<div class="m2"><p>سیر گشتم زین سواری سیرسیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرنگون خود را از اشتر در فکند</p></div>
<div class="m2"><p>گفت سوزیدم ز غم تا چندچند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تنگ شد بر وی بیابان فراخ</p></div>
<div class="m2"><p>خویشتن افکند اندر سنگلاخ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنچنان افکند خود را سخت زیر</p></div>
<div class="m2"><p>که مخلخل گشت جسم آن دلیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون چنان افکند خود را سوی پست</p></div>
<div class="m2"><p>از قضا آن لحظه پایش هم شکست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پای را بر بست و گفتا گو شوم</p></div>
<div class="m2"><p>در خم چوگانش غلطان می‌روم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زین کند نفرین حکیم خوش‌دهن</p></div>
<div class="m2"><p>بر سواری کو فرو ناید ز تن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عشق مولی کی کم از لیلی بود</p></div>
<div class="m2"><p>گوی گشتن بهر او اولی بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گوی شو می‌گرد بر پهلوی صدق</p></div>
<div class="m2"><p>غلط غلطان در خم چوگان عشق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کین سفر زین پس بود جذب خدا</p></div>
<div class="m2"><p>وان سفر بر ناقه باشد سیر ما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این چنین سیریست مستثنی ز جنس</p></div>
<div class="m2"><p>کان فزود از اجتهاد جن و انس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این چنین جذبیست نی هر جذب عام</p></div>
<div class="m2"><p>که نهادش فضل احمد والسلام</p></div></div>