---
title: >-
    بخش ۱۲۰ - اختیار کردن پادشاه دختر درویش زاهدی را از جهت پسر و اعتراض کردن اهل حرم و ننگ داشتن ایشان از پیوندی درویش
---
# بخش ۱۲۰ - اختیار کردن پادشاه دختر درویش زاهدی را از جهت پسر و اعتراض کردن اهل حرم و ننگ داشتن ایشان از پیوندی درویش

<div class="b" id="bn1"><div class="m1"><p>مادر شه‌زاده گفت از نقص عقل</p></div>
<div class="m2"><p>شرط کفویت بود در عقل نقل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ز شح و بخل خواهی وز دها</p></div>
<div class="m2"><p>تا ببندی پور ما را بر گدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت صالح را گدا گفتن خطاست</p></div>
<div class="m2"><p>کو غنی القلب از داد خداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قناعت می‌گریزد از تقی</p></div>
<div class="m2"><p>نه از لیمی و کسل هم‌چون گدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قلتی کان از قناعت وز تقاست</p></div>
<div class="m2"><p>آن ز فقر و قلت دونان جداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حبه‌ای آن گر بیابد سر نهد</p></div>
<div class="m2"><p>وین ز گنج زر به همت می‌جهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شه که او از حرص قصد هر حرام</p></div>
<div class="m2"><p>می‌کند او را گدا گوید همام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت کو شهر و قلاع او را جهاز</p></div>
<div class="m2"><p>یا نثار گوهر و دینار ریز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت رو هر که غم دین برگزید</p></div>
<div class="m2"><p>باقی غمها خدا از وی برید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غالب آمد شاه و دادش دختری</p></div>
<div class="m2"><p>از نژاد صالحی خوش جوهری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ملاحت خود نظیر خود نداشت</p></div>
<div class="m2"><p>چهره‌اش تابان‌تر از خورشید چاشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حسن دختر این خصالش آنچنان</p></div>
<div class="m2"><p>کز نکویی می‌نگنجد در بیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صید دین کن تا رسد اندر تبع</p></div>
<div class="m2"><p>حسن و مال و جاه و بخت منتفع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آخرت قطار اشتر دان به ملک</p></div>
<div class="m2"><p>در تبع دنیاش هم‌چون پشم و پشک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پشم بگزینی شتر نبود ترا</p></div>
<div class="m2"><p>ور بود اشتر چه قیمت پشم را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بر آمد این نکاح آن شاه را</p></div>
<div class="m2"><p>با نژاد صالحان بی مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از قضا کمپیرکی جادو که بود</p></div>
<div class="m2"><p>عاشق شه‌زادهٔ با حسن و جود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جادوی کردش عجوزهٔ کابلی</p></div>
<div class="m2"><p>کی برد زان رشک سحر بابلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شه بچه شد عاشق کمپیر زشت</p></div>
<div class="m2"><p>تا عروس و آن عروسی را بهشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک سیه دیوی و کابولی زنی</p></div>
<div class="m2"><p>گشت به شه‌زاده ناگه ره‌زنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن نودساله عجوزی گنده کس</p></div>
<div class="m2"><p>نه خرد هشت آن ملک را و نه نس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا به سالی بود شه‌زاده اسیر</p></div>
<div class="m2"><p>بوسه‌جایش نعل کفش گنده پیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صحبت کمپیر او را می‌درود</p></div>
<div class="m2"><p>تا ز کاهش نیم‌جانی مانده بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیگران از ضعف وی با درد سر</p></div>
<div class="m2"><p>او ز سکر سحر از خود بی‌خبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این جهان بر شاه چون زندان شده</p></div>
<div class="m2"><p>وین پسر بر گریه‌شان خندان شده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاه بس بیچاره شد در برد و مات</p></div>
<div class="m2"><p>روز و شب می‌کرد قربان و زکات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زانک هر چاره که می‌کرد آن پدر</p></div>
<div class="m2"><p>عشق کمپیرک همی‌شد بیشتر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس یقین گشتش که مطلق آن سریست</p></div>
<div class="m2"><p>چاره او را بعد از این لابه گریست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سجده می‌کرد او که هم فرمان تراست</p></div>
<div class="m2"><p>غیر حق بر ملک حق فرمان کراست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لیک این مسکین همی‌سوزد چو عود</p></div>
<div class="m2"><p>دست گیرش ای رحیم و ای ودود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا ز یا رب یا رب و افغان شاه</p></div>
<div class="m2"><p>ساحری استاد پیش آمد ز راه</p></div></div>