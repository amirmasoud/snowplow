---
title: >-
    بخش ۱ - سر آغاز
---
# بخش ۱ - سر آغاز

<div class="b" id="bn1"><div class="m1"><p>ای ضیاء الحق حسام الدین توی</p></div>
<div class="m2"><p>که گذشت از مه به نورت مثنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت عالی تو ای مرتجا</p></div>
<div class="m2"><p>می‌کشد این را خدا داند کجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردن این مثنوی را بسته‌ای</p></div>
<div class="m2"><p>می‌کشی آن سوی که دانسته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مثنوی پویان کشنده ناپدید</p></div>
<div class="m2"><p>ناپدید از جاهلی کش نیست دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثنوی را چون تو مبدا بوده‌ای</p></div>
<div class="m2"><p>گر فزون گردد توش افزوده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون چنین خواهی خدا خواهد چنین</p></div>
<div class="m2"><p>می‌دهد حق آرزوی متقین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کان لله بوده‌ای در ما مضی</p></div>
<div class="m2"><p>تا که کان الله پیش آمد جزا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مثنوی از تو هزاران شکر داشت</p></div>
<div class="m2"><p>در دعا و شکر کفها بر فراشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در لب و کفش خدا شکر تو دید</p></div>
<div class="m2"><p>فضل کرد و لطف فرمود و مزید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانک شاکر را زیادت وعده است</p></div>
<div class="m2"><p>آنچنانک قرب مزد سجده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت واسجد واقترب یزدان ما</p></div>
<div class="m2"><p>قرب جان شد سجده ابدان ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر زیادت می‌شود زین رو بود</p></div>
<div class="m2"><p>نه از برای بوش و های و هو بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با تو ما چون رز به تابستان خوشیم</p></div>
<div class="m2"><p>حکم داری هین بکش تا می‌کشیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوش بکش این کاروان را تا به حج</p></div>
<div class="m2"><p>ای امیر صبر مفتاح الفرج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حج زیارت کردن خانه بود</p></div>
<div class="m2"><p>حج رب البیت مردانه بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان ضیا گفتم حسام‌الدین ترا</p></div>
<div class="m2"><p>که تو خورشیدی و این دو وصفها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کین حسام و این ضیا یکیست هین</p></div>
<div class="m2"><p>تیغ خورشید از ضیا باشد یقین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نور از آن ماه باشد وین ضیا</p></div>
<div class="m2"><p>آن خورشید این فرو خوان از نبا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمس را قرآن ضیا خواند ای پدر</p></div>
<div class="m2"><p>و آن قمر را نور خواند این را نگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمس چون عالی‌تر آمد خود ز ماه</p></div>
<div class="m2"><p>پس ضیا از نور افزون دان به جاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس کس اندر نور مه منهج ندید</p></div>
<div class="m2"><p>چون برآمد آفتاب آن شد پدید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آفتاب اعواض را کامل نمود</p></div>
<div class="m2"><p>لاجرم بازارها در روز بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا که قلب و نقد نیک آید پدید</p></div>
<div class="m2"><p>تا بود از غبن و از حیله بعید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا که نورش کامل آمد در زمین</p></div>
<div class="m2"><p>تاجران را رحمة للعالمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لیک بر قلاب مبغوضست و سخت</p></div>
<div class="m2"><p>زانک ازو شد کاسد او را نقد و رخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس عدو جان صرافست قلب</p></div>
<div class="m2"><p>دشمن درویش کی بود غیر کلب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>انبیا با دشمنان بر می‌تنند</p></div>
<div class="m2"><p>پس ملایک رب سلم می‌زنند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کین چراغی را که هست او نور کار</p></div>
<div class="m2"><p>از پف و دمهای دزدان دور دار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دزد و قلابست خصم نور بس</p></div>
<div class="m2"><p>زین دو ای فریادرس فریاد رس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روشنی بر دفتر چارم بریز</p></div>
<div class="m2"><p>کآفتاب از چرخ چارم کرد خیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هین ز چارم نور ده خورشیدوار</p></div>
<div class="m2"><p>تا بتابد بر بلاد و بر دیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر کش افسانه بخواند افسانه است</p></div>
<div class="m2"><p>وآنک دیدش نقد خود مردانه است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آب نیلست و به قبطی خون نمود</p></div>
<div class="m2"><p>قوم موسی را نه خون بد آب بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دشمن این حرف این دم در نظر</p></div>
<div class="m2"><p>شد ممثل سرنگون اندر سقر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای ضیاء الحق تو دیدی حال او</p></div>
<div class="m2"><p>حق نمودت پاسخ افعال او</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دیدهٔ غیبت چو غیبست اوستاد</p></div>
<div class="m2"><p>کم مبادا زین جهان این دید و داد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>این حکایت را که نقد وقت ماست</p></div>
<div class="m2"><p>گر تمامش می‌کنی اینجا رواست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ناکسان را ترک کن بهر کسان</p></div>
<div class="m2"><p>قصه را پایان بر و مخلص رسان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این حکایت گر نشد آنجا تمام</p></div>
<div class="m2"><p>چارمین جلدست آرش در نظام</p></div></div>