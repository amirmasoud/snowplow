---
title: >-
    بخش ۱۲۱ - مستجاب شدن دعای پادشاه در خلاص پسرش از جادوی کابلی
---
# بخش ۱۲۱ - مستجاب شدن دعای پادشاه در خلاص پسرش از جادوی کابلی

<div class="b" id="bn1"><div class="m1"><p>او شنیده بود از دور این خبر</p></div>
<div class="m2"><p>که اسیر پیرزن گشت آن پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان عجوزه بود اندر جادوی</p></div>
<div class="m2"><p>بی‌نظیر و آمن از مثل و دوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بر بالای دستست ای فتی</p></div>
<div class="m2"><p>در فن و در زور تا ذات خدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منتهای دستها دست خداست</p></div>
<div class="m2"><p>بحر بی‌شک منتهای سیلهاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم ازو گیرند مایه ابرها</p></div>
<div class="m2"><p>هم بدو باشد نهایت سیل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت شاهش کین پسر از دست رفت</p></div>
<div class="m2"><p>گفت اینک آمدم درمان زفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست همتا زال را زین ساحران</p></div>
<div class="m2"><p>جز من داهی رسیده زان کران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کف موسی به امر کردگار</p></div>
<div class="m2"><p>نک برآرم من ز سحر او دمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که مرا این علم آمد زان طرف</p></div>
<div class="m2"><p>نه ز شاگردی سحر مستخف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمدم تا بر گشایم سحر او</p></div>
<div class="m2"><p>تا نماند شاه‌زاده زردرو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی گورستان برو وقت سحور</p></div>
<div class="m2"><p>پهلوی دیوار هست اسپید گور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوی قبله باز کاو آنجای را</p></div>
<div class="m2"><p>تا ببینی قدرت و صنع خدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس درازست این حکایت تو ملول</p></div>
<div class="m2"><p>زبده را گویم رها کردم فضول</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن گره‌های گران را بر گشاد</p></div>
<div class="m2"><p>پس ز محنت پور شه را راه داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن پسر با خویش آمد شد دوان</p></div>
<div class="m2"><p>سوی تخت شاه با صد امتحان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سجده کرد و بر زمین می‌زد ذقن</p></div>
<div class="m2"><p>در بغل کرده پسر تیغ و کفن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاه آیین بست و اهل شهر شاد</p></div>
<div class="m2"><p>وآن عروس ناامید بی‌مراد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عالم از سر زنده گشت و پر فروز</p></div>
<div class="m2"><p>ای عجب آن روز روز امروز روز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یک عروسی کرد شاه او را چنان</p></div>
<div class="m2"><p>که جلاب قند بد پیش سگان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جادوی کمپیر از غصه بمرد</p></div>
<div class="m2"><p>روی و خوی زشت فا مالک سپرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه‌زاده در تعجب مانده بود</p></div>
<div class="m2"><p>کز من او عقل و نظر چون در ربود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نو عروسی دید هم‌چون ماه حسن</p></div>
<div class="m2"><p>که همی زد بر ملیحان راه حسن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گشت بیهوش و برو اندر فتاد</p></div>
<div class="m2"><p>تا سه روز از جسم وی گم شد فؤاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سه شبان روز او ز خود بیهوش گشت</p></div>
<div class="m2"><p>تا که خلق از غشی او پر جوش گشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از گلاب و از علاج آمد به خود</p></div>
<div class="m2"><p>اندک اندک فهم گشتش نیک و بد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بعد سالی گفت شاهش در سخن</p></div>
<div class="m2"><p>کای پسر یاد آر از آن یار کهن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یاد آور زان ضجیع و زان فراش</p></div>
<div class="m2"><p>تا بدین حد بی‌وفا و مر مباش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت رو من یافتم دار السرور</p></div>
<div class="m2"><p>وا رهیدم از چه دار الغرور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هم‌چنان باشد چو مؤمن راه یافت</p></div>
<div class="m2"><p>سوی نور حق ز ظلمت روی تافت</p></div></div>