---
title: >-
    بخش ۲ - تمامی حکایت آن عاشق که از عسس گریخت در باغی مجهول خود معشوق را در باغ یافت و عسس را از شادی دعای خیر می‌کرد و می‌گفت کی عَسی أَنْ تَکْرَهوا  شَیْئاً وَ هُوَ خَیْرٌ لَکُمْ
---
# بخش ۲ - تمامی حکایت آن عاشق که از عسس گریخت در باغی مجهول خود معشوق را در باغ یافت و عسس را از شادی دعای خیر می‌کرد و می‌گفت کی عَسی أَنْ تَکْرَهوا  شَیْئاً وَ هُوَ خَیْرٌ لَکُمْ

<div class="b" id="bn1"><div class="m1"><p>اندر آن بودیم کان شخص از عسس</p></div>
<div class="m2"><p>راند اندر باغ از خوفی فرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود اندر باغ آن صاحب‌جمال</p></div>
<div class="m2"><p>کز غمش این در عنا بد هشت سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایهٔ او را نبود امکان دید</p></div>
<div class="m2"><p>هم‌چو عنقا وصف او را می‌شنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز یکی لقیه که اول از قضا</p></div>
<div class="m2"><p>بر وی افتاد و شد او را دلربا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از آن چندان که می‌کوشید او</p></div>
<div class="m2"><p>خود مجالش می‌نداد آن تندخو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه به لابه چاره بودش نه به مال</p></div>
<div class="m2"><p>چشم پر و بی‌طمع بود آن نهال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق هر پیشه‌ای و مطلبی</p></div>
<div class="m2"><p>حق بیالود اول کارش لبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بدان آسیب در جست آمدند</p></div>
<div class="m2"><p>پیش پاشان می‌نهد هر روز بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون در افکندش بجست و جوی کار</p></div>
<div class="m2"><p>بعد از آن در بست که کابین بیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم بر آن بو می‌تنند و می‌روند</p></div>
<div class="m2"><p>هر دمی راجی و آیس می‌شوند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کسی را هست اومید بری</p></div>
<div class="m2"><p>که گشادندش در آن روزی دری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز در بستندش و آن درپرست</p></div>
<div class="m2"><p>بر همان اومید آتش پا شدست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون درآمد خوش در آن باغ آن جوان</p></div>
<div class="m2"><p>خود فرو شد پا به گنجش ناگهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مر عسس را ساخته یزدان سبب</p></div>
<div class="m2"><p>تا ز بیم او دود در باغ شب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیند آن معشوقه را او با چراغ</p></div>
<div class="m2"><p>طالب انگشتری در جوی باغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس قرین می‌کرد از ذوق آن نفس</p></div>
<div class="m2"><p>با ثنای حق دعای آن عسس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که زیان کردم عسس را از گریز</p></div>
<div class="m2"><p>بیست چندان سیم و زر بر وی بریز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از عوانی مر ورا آزاد کن</p></div>
<div class="m2"><p>آنچنان که شادم او را شاد کن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سعد دارش این جهان و آن جهان</p></div>
<div class="m2"><p>از عوانی و سگی‌اش وا رهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه خوی آن عوان هست ای خدا</p></div>
<div class="m2"><p>که هماره خلق را خواهد بلا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر خبر آید که شه جرمی نهاد</p></div>
<div class="m2"><p>بر مسلمانان شود او زفت و شاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور خبر آید که شه رحمت نمود</p></div>
<div class="m2"><p>از مسلمانان فکند آن را به جود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ماتمی در جان او افتد از آن</p></div>
<div class="m2"><p>صد چنین ادبارها دارد عوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>او عوان را در دعا در می‌کشید</p></div>
<div class="m2"><p>کز عوان او را چنان راحت رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر همه زهر و برو تریاق بود</p></div>
<div class="m2"><p>آن عوان پیوند آن مشتاق بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس بد مطلق نباشد در جهان</p></div>
<div class="m2"><p>بد به نسبت باشد این را هم بدان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در زمانه هیچ زهر و قند نیست</p></div>
<div class="m2"><p>که یکی را پا دگر را بند نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مر یکی را پا دگر را پای‌بند</p></div>
<div class="m2"><p>مر یکی را زهر و بر دیگر چو قند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهر مار آن مار را باشد حیات</p></div>
<div class="m2"><p>نسبتش با آدمی باشد ممات</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خلق آبی را بود دریا چو باغ</p></div>
<div class="m2"><p>خلق خاکی را بود آن مرگ و داغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همچنین بر می‌شمر ای مرد کار</p></div>
<div class="m2"><p>نسبت این از یکی کس تا هزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زید اندر حق آن شیطان بود</p></div>
<div class="m2"><p>در حق شخصی دگر سلطان بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن بگوید زید صدیق سنیست</p></div>
<div class="m2"><p>وین بگوید زید گبر کشتنیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر تو خواهی کو ترا باشد شکر</p></div>
<div class="m2"><p>پس ورا از چشم عشاقش نگر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>منگر از چشم خودت آن خوب را</p></div>
<div class="m2"><p>بین به چشم طالبان مطلوب را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چشم خود بر بند زان خوش‌چشم تو</p></div>
<div class="m2"><p>عاریت کن چشم از عشاق او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلک ازو کن عاریت چشم و نظر</p></div>
<div class="m2"><p>پس ز چشم او بروی او نگر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا شوی آمن ز سیری و ملال</p></div>
<div class="m2"><p>گفت کان الله له زین ذوالجلال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چشم او من باشم و دست و دلش</p></div>
<div class="m2"><p>تا رهد از مدبریها مقبلش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر چه مکرو هست چون شد او دلیل</p></div>
<div class="m2"><p>سوی محبوبت حبیبست و خلیل</p></div></div>