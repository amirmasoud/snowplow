---
title: >-
    بخش ۴۸ - نشستن دیو بر مقام سلیمان علیه‌السلام و تشبه کردن او به کارهای سلیمان  علیه‌السلام و فرق ظاهر میان هر دو سلیمان و دیو خویشتن را سلیمان بن داود نام کردن
---
# بخش ۴۸ - نشستن دیو بر مقام سلیمان علیه‌السلام و تشبه کردن او به کارهای سلیمان  علیه‌السلام و فرق ظاهر میان هر دو سلیمان و دیو خویشتن را سلیمان بن داود نام کردن

<div class="b" id="bn1"><div class="m1"><p>ورچه عقلت هست با عقل دگر</p></div>
<div class="m2"><p>یار باش و مشورت کن ای پدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دو عقل از بس بلاها وا رهی</p></div>
<div class="m2"><p>پای خود بر اوج گردونها نهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیو گر خود را سلیمان نام کرد</p></div>
<div class="m2"><p>ملک برد و مملکت را رام کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت کار سلیمان دیده بود</p></div>
<div class="m2"><p>صورت اندر سر دیوی می‌نمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق گفتند این سلیمان بی‌صفاست</p></div>
<div class="m2"><p>از سلیمان تا سلیمان فرقهاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او چو بیداریست این هم‌چون وسن</p></div>
<div class="m2"><p>هم‌چنانک آن حسن با این حسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیو می‌گفتی که حق بر شکل من</p></div>
<div class="m2"><p>صورتی کردست خوش بر اهرمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیو را حق صورت من داده است</p></div>
<div class="m2"><p>تا نیندازد شما را او بشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر پدید آید به دعوی زینهار</p></div>
<div class="m2"><p>صورت او را مدارید اعتبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیوشان از مکر این می‌گفت لیک</p></div>
<div class="m2"><p>می‌نمود این عکس در دلهای نیک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست بازی با ممیز خاصه او</p></div>
<div class="m2"><p>که بود تمییز و عقلش غیب‌گو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیچ سحر و هیچ تلبیس و دغل</p></div>
<div class="m2"><p>می‌نبندد پرده بر اهل دول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس همی گفتند با خود در جواب</p></div>
<div class="m2"><p>بازگونه می‌روی ای کژ خطاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بازگونه رفت خواهی همچنین</p></div>
<div class="m2"><p>سوی دوزخ اسفل اندر سافلین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او اگر معزول گشتست و فقیر</p></div>
<div class="m2"><p>هست در پیشانیش بدر منیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو اگر انگشتری را برده‌ای</p></div>
<div class="m2"><p>دوزخی چون زمهریر افسرده‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما ببوش و عارض و طاق و طرنب</p></div>
<div class="m2"><p>سر کجا که خود همی ننهیم سنب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور به غفلت ما نهیم او را جبین</p></div>
<div class="m2"><p>پنجهٔ مانع برآید از زمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که منه آن سر مرین سر زیر را</p></div>
<div class="m2"><p>هین مکن سجده مرین ادبار را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کردمی من شرح این بس جان‌فزا</p></div>
<div class="m2"><p>گر نبودی غیرت و رشک خدا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هم قناعت کن تو بپذیر این قدر</p></div>
<div class="m2"><p>تا بگویم شرح این وقتی دگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نام خود کرده سلیمان نبی</p></div>
<div class="m2"><p>روی‌پوشی می‌کند بر هر صبی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در گذر از صورت و از نام خیز</p></div>
<div class="m2"><p>از لقب وز نام در معنی گریز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس بپرس از حد او وز فعل او</p></div>
<div class="m2"><p>در میان حد و فعل او را بجو</p></div></div>