---
title: >-
    بخش ۸۸ - بیان آنک عهد کردن احمق وقت گرفتاری و ندم هیچ وفایی ندارد کی لو ردوالعادوا لما نهوا عنه و انهم لکاذبون صبح کاذب وفا ندارد
---
# بخش ۸۸ - بیان آنک عهد کردن احمق وقت گرفتاری و ندم هیچ وفایی ندارد کی لو ردوالعادوا لما نهوا عنه و انهم لکاذبون صبح کاذب وفا ندارد

<div class="b" id="bn1"><div class="m1"><p>عقل می‌گفتش حماقت با توست</p></div>
<div class="m2"><p>با حماقت عقل را آید شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل را باشد وفای عهدها</p></div>
<div class="m2"><p>تو نداری عقل رو ای خربها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل را یاد آید از پیمان خود</p></div>
<div class="m2"><p>پردهٔ نسیان بدراند خرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونک عقلت نیست نسیان میر تست</p></div>
<div class="m2"><p>دشمن و باطل کن تدبیر تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کمی عقل پروانهٔ خسیس</p></div>
<div class="m2"><p>یاد نارد ز آتش و سوز و حسیس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونک پرش سوخت توبه می‌کند</p></div>
<div class="m2"><p>آز و نسیانش بر آتش می‌زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضبط و درک و حافظی و یادداشت</p></div>
<div class="m2"><p>عقل را باشد که عقل آن را فراشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونک گوهر نیست تابش چون بود</p></div>
<div class="m2"><p>چون مذکر نیست ایابش چون بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این تمنی هم ز بی‌عقلی اوست</p></div>
<div class="m2"><p>که نبیند کان حماقت را چه خوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن ندامت از نتیجهٔ رنج بود</p></div>
<div class="m2"><p>نه ز عقل روشن چون گنج بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونک شد رنج آن ندامت شد عدم</p></div>
<div class="m2"><p>می‌نیرزد خاک آن توبه و ندم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن ندم از ظلمت غم بست بار</p></div>
<div class="m2"><p>پس کلام اللیل یمحوه النهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون برفت آن ظلمت غم گشت خوش</p></div>
<div class="m2"><p>هم رود از دل نتیجه و زاده‌اش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌کند او توبه و پیر خرد</p></div>
<div class="m2"><p>بانگ لو ردوا لعادوا می‌زند</p></div></div>