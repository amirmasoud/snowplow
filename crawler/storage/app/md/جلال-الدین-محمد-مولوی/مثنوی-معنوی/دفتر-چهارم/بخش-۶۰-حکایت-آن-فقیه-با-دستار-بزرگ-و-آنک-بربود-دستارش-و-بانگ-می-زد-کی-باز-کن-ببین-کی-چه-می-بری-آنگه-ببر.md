---
title: >-
    بخش ۶۰ - حکایت آن فقیه با دستار بزرگ و  آنک بربود دستارش و بانگ می‌زد کی باز کن ببین کی چه می‌بری آنگه ببر
---
# بخش ۶۰ - حکایت آن فقیه با دستار بزرگ و  آنک بربود دستارش و بانگ می‌زد کی باز کن ببین کی چه می‌بری آنگه ببر

<div class="b" id="bn1"><div class="m1"><p>یک فقیهی ژنده‌ها در چیده بود</p></div>
<div class="m2"><p>در عمامهٔ خویش در پیچیده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شود زفت و نماید آن عظیم</p></div>
<div class="m2"><p>چون در آید سوی محفل در حطیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ژنده‌ها از جامه‌ها پیراسته</p></div>
<div class="m2"><p>ظاهرا دستار از آن آراسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظاهر دستار چون حلهٔ بهشت</p></div>
<div class="m2"><p>چون منافق اندرون رسوا و زشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاره پاره دلق و پنبه و پوستین</p></div>
<div class="m2"><p>در درون آن عمامه بد دفین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی سوی مدرسه کرده صبوح</p></div>
<div class="m2"><p>تا بدین ناموس یابد او فتوح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره تاریک مردی جامه کن</p></div>
<div class="m2"><p>منتظر استاده بود از بهر فن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ربود او از سرش دستار را</p></div>
<div class="m2"><p>پس دوان شد تا بسازد کار را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس فقیهش بانگ برزد کای پسر</p></div>
<div class="m2"><p>باز کن دستار را آنگه ببر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این چنین که چار پره می‌پری</p></div>
<div class="m2"><p>باز کن آن هدیه را که می‌بری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز کن آن را به دست خود بمال</p></div>
<div class="m2"><p>آنگهان خواهی ببر کردم حلال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونک بازش کرد آنک می‌گریخت</p></div>
<div class="m2"><p>صد هزاران ژنده اندر ره بریخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان عمامهٔ زفت نابایست او</p></div>
<div class="m2"><p>ماند یک گز کهنه‌ای در دست او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر زمین زد خرقه را کای بی‌عیار</p></div>
<div class="m2"><p>زین دغل ما را بر آوردی ز کار</p></div></div>