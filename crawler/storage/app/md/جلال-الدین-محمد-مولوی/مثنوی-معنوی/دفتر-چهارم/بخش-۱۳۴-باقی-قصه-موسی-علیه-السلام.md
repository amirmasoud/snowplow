---
title: >-
    بخش ۱۳۴ - باقی قصهٔ موسی علیه‌السلام
---
# بخش ۱۳۴ - باقی قصهٔ موسی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>که آمدش پیغام از وحی مهم</p></div>
<div class="m2"><p>که کژی بگذار اکنون فاستقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این درخت تن عصای موسیست</p></div>
<div class="m2"><p>که امرش آمد که بیندازش ز دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ببینی خیر او و شر او</p></div>
<div class="m2"><p>بعد از آن بر گیر او را ز امر هو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش از افکندن نبود او غیر چوب</p></div>
<div class="m2"><p>چون به امرش بر گرفتی گشت خوب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول او بد برگ‌افشان بره را</p></div>
<div class="m2"><p>گشت معجز آن گروه غره را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشت حاکم بر سر فرعونیان</p></div>
<div class="m2"><p>آبشان خون کرد و کف بر سر زنان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مزارعشان برآمد قحط و مرگ</p></div>
<div class="m2"><p>از ملخهایی که می‌خوردند برگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بر آمد بی‌خود از موسی دعا</p></div>
<div class="m2"><p>چون نظر افتادش اندر منتها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کین همه اعجاز و کوشیدن چراست</p></div>
<div class="m2"><p>چون نخواهند این جماعت گشت راست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امر آمد که اتباع نوح کن</p></div>
<div class="m2"><p>ترک پایان‌بینی مشروح کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان تغافل کن چو داعی رهی</p></div>
<div class="m2"><p>امر بلغ هست نبود آن تهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمترین حکمت کزین الحاح تو</p></div>
<div class="m2"><p>جلوه گردد آن لجاج و آن عتو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا که ره بنمودن و اضلال حق</p></div>
<div class="m2"><p>فاش گردد بر همه اهل و فرق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چونک مقصود از وجود اظهار بود</p></div>
<div class="m2"><p>بایدش از پند و اغوا آزمود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیو الحاح غوایت می‌کند</p></div>
<div class="m2"><p>شیخ‌الحاح هدایت می‌کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون پیاپی گشت آن امر شجون</p></div>
<div class="m2"><p>نیل می‌آمد سراسر جمله خون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بنفس خویش فرعون آمدش</p></div>
<div class="m2"><p>لابه می‌کردش دو تا گشته قدش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کانچ ما کردیم ای سلطان مکن</p></div>
<div class="m2"><p>نیست ما را روی ایراد سخن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پاره پاره گردمت فرمان‌پذیر</p></div>
<div class="m2"><p>من بعزت خوگرم سختم مگیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هین بجنبان لب به رحمت ای امین</p></div>
<div class="m2"><p>تا ببندد این دهانهٔ آتشین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت یا رب می‌فریبد او مرا</p></div>
<div class="m2"><p>می‌فریبد او فریبندهٔ ترا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بشنوم یا من دهم هم خدعه‌اش</p></div>
<div class="m2"><p>تا بداند اصل را آن فرع‌کش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که اصل هر مکری و حیلت پیش ماست</p></div>
<div class="m2"><p>هر چه بر خاکست اصلش از سماست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت حق آن سگ نیرزد هم به آن</p></div>
<div class="m2"><p>پیش سگ انداز از دور استخوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هین بجنبان آن عصا تا خاکها</p></div>
<div class="m2"><p>وا دهد هرچه ملخ کردش فنا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وان ملخها در زمان گردد سیاه</p></div>
<div class="m2"><p>تا ببیند خلق تبدیل اله</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که سببها نیست حاجت مر مرا</p></div>
<div class="m2"><p>آن سبب بهر حجابست و غطا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا طبیعی خویش بر دارو زند</p></div>
<div class="m2"><p>تا منجم رو با ستاره کند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا منافق از حریصی بامداد</p></div>
<div class="m2"><p>سوی بازار آید از بیم کساد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بندگی ناکرده و ناشسته روی</p></div>
<div class="m2"><p>لقمهٔ دوزخ بگشته لقمه‌جوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آکل و ماکول آمد جان عام</p></div>
<div class="m2"><p>هم‌چو آن برهٔ چرنده از حطام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>می‌چرد آن بره و قصاب شاد</p></div>
<div class="m2"><p>کو برای ما چرد برگ مراد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کار دوزخ می‌کنی در خوردنی</p></div>
<div class="m2"><p>بهر او خود را تو فربه می‌کنی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کار خود کن روزی حکمت بچر</p></div>
<div class="m2"><p>تا شود فربه دل با کر و فر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خوردن تن مانع این خوردنست</p></div>
<div class="m2"><p>جان چو بازرگان و تن چون ره‌زنست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شمع تاجر آنگهست افروخته</p></div>
<div class="m2"><p>که بود ره‌زن چو هیزم سوخته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که تو آن هوشی و باقی هوش‌پوش</p></div>
<div class="m2"><p>خویشتن را گم مکن یاوه مکوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دانک هر شهوت چو خمرست و چو بنگ</p></div>
<div class="m2"><p>پردهٔ هوشست وعاقل زوست دنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خمر تنها نیست سرمستی هوش</p></div>
<div class="m2"><p>هر چه شهوانیست بندد چشم و گوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آن بلیس از خمر خوردن دور بود</p></div>
<div class="m2"><p>مست بود او از تکبر وز جحود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مست آن باشد که آن بیند که نیست</p></div>
<div class="m2"><p>زر نماید آنچ مس و آهنیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>این سخن پایان ندارد موسیا</p></div>
<div class="m2"><p>لب بجنبان تا برون روژد گیا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هم‌چنان کرد و هم اندر دم زمین</p></div>
<div class="m2"><p>سبز گشت از سنبل و حب ثمین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اندر افتادند در لوت آن نفر</p></div>
<div class="m2"><p>قحط دیده مرده از جوع البقر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چند روزی سیر خوردند از عطا</p></div>
<div class="m2"><p>آن دمی و آدمی و چارپا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون شکم پر گشت و بر نعمت زدند</p></div>
<div class="m2"><p>وآن ضرورت رفت پس طاغی شدند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نفس فرعونیست هان سیرش مکن</p></div>
<div class="m2"><p>تا نیارد یاد از آن کفر کهن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بی تف آتش نگردد نفس خوب</p></div>
<div class="m2"><p>تا نشد آهن چو اخگر هین مکوب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بی‌مجاعت نیست تن جنبش‌کنان</p></div>
<div class="m2"><p>آهن سردیست می‌کوبی بدان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر بگرید ور بنالد زار زار</p></div>
<div class="m2"><p>او نخواهد شد مسلمان هوش دار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>او چو فرعونست در قحط آنچنان</p></div>
<div class="m2"><p>پیش موسی سر نهد لابه‌کنان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چونک مستغنی شد او طاغی شود</p></div>
<div class="m2"><p>خر چو بار انداخت اسکیزه زند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پس فراموشش شود چون رفت پیش</p></div>
<div class="m2"><p>کار او زان آه و زاریهای خویش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سالها مردی که در شهری بود</p></div>
<div class="m2"><p>یک زمان که چشم در خوابی رود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شهر دیگر بیند او پر نیک و بد</p></div>
<div class="m2"><p>هیچ در یادش نیاید شهر خود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که من آنجا بوده‌ام این شهر نو</p></div>
<div class="m2"><p>نیست آن من درینجاام گرو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بل چنان داند که خود پیوسته او</p></div>
<div class="m2"><p>هم درین شهرش به دست ابداع و خو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چه عجب گر روح موطنهای خویش</p></div>
<div class="m2"><p>که بدستش مسکن و میلاد پیش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>می‌نیارد یاد کین دنیا چو خواب</p></div>
<div class="m2"><p>می‌فرو پوشد چو اختر را سحاب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خاصه چندین شهرها را کوفته</p></div>
<div class="m2"><p>گردها از درک او ناروفته</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اجتهاد گرم ناکرده که تا</p></div>
<div class="m2"><p>دل شود صاف و ببیند ماجرا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سر برون آرد دلش از بخش راز</p></div>
<div class="m2"><p>اول و آخر ببیند چشم باز</p></div></div>