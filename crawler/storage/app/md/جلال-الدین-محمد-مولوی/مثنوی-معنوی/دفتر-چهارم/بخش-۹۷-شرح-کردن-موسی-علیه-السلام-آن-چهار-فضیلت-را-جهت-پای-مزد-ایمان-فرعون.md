---
title: >-
    بخش ۹۷ - شرح کردن موسی علیه‌السلام آن  چهار فضیلت را جهت  پای مزد ایمان فرعون
---
# بخش ۹۷ - شرح کردن موسی علیه‌السلام آن  چهار فضیلت را جهت  پای مزد ایمان فرعون

<div class="b" id="bn1"><div class="m1"><p>گفت موسی که اولین آن چهار</p></div>
<div class="m2"><p>صحتی باشد تنت را پایدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این علل‌هایی که در طب گفته‌اند</p></div>
<div class="m2"><p>دور باشد از تنت ای ارجمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثانیا باشد ترا عمر دراز</p></div>
<div class="m2"><p>که اجل دارد ز عمرت احتراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وین نباشد بعد عمر مستوی</p></div>
<div class="m2"><p>که بناکام از جهان بیرون روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلک خواهان اجل چون طفل شیر</p></div>
<div class="m2"><p>نه ز رنجی که ترا دارد اسیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرگ‌جو باشی ولی نه از عجز رنج</p></div>
<div class="m2"><p>بلک بینی در خراب خانه گنج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس به دست خویش گیری تیشه‌ای</p></div>
<div class="m2"><p>می‌زنی بر خانه بی‌اندیشه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که حجاب گنج بینی خانه را</p></div>
<div class="m2"><p>مانع صد خرمن این یک دانه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس در آتش افکنی این دانه را</p></div>
<div class="m2"><p>پیش گیری پیشهٔ مردانه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای به یک برگی ز باغی مانده</p></div>
<div class="m2"><p>هم‌چو کرمی برگش از رز رانده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کرم این کرم را بیدار کرد</p></div>
<div class="m2"><p>اژدهای جهل را این کرم خورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرم کرمی شد پر از میوه و درخت</p></div>
<div class="m2"><p>این چنین تبدیل گردد نیکبخت</p></div></div>