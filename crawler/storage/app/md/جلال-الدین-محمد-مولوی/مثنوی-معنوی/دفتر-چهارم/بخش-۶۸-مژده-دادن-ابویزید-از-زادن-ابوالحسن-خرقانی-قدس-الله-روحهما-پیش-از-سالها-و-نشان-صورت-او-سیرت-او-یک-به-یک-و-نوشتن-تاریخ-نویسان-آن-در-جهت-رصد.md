---
title: >-
    بخش ۶۸ - مژده دادن ابویزید از زادن ابوالحسن خرقانی قدس الله روحهما پیش از سالها  و نشان صورت او سیرت او یک به یک و  نوشتن تاریخ‌نویسان آن در جهت رصد
---
# بخش ۶۸ - مژده دادن ابویزید از زادن ابوالحسن خرقانی قدس الله روحهما پیش از سالها  و نشان صورت او سیرت او یک به یک و  نوشتن تاریخ‌نویسان آن در جهت رصد

<div class="b" id="bn1"><div class="m1"><p>آن شنیدی داستان بایزید</p></div>
<div class="m2"><p>که ز حال بوالحسن پیشین چه دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی آن سلطان تقوی می‌گذشت</p></div>
<div class="m2"><p>با مریدان جانب صحرا و دشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی خوش آمد مر او را ناگهان</p></div>
<div class="m2"><p>در سواد ری ز سوی خارقان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم بدانجا نالهٔ مشتاق کرد</p></div>
<div class="m2"><p>بوی را از باد استنشاق کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی خوش را عاشقانه می‌کشید</p></div>
<div class="m2"><p>جان او از باد باده می‌چشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوزه‌ای کو از یخابه پر بود</p></div>
<div class="m2"><p>چون عرق بر ظاهرش پیدا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن ز سردی هوا آبی شدست</p></div>
<div class="m2"><p>از درون کوزه نم بیرون نجست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد بوی‌آور مر او را آب گشت</p></div>
<div class="m2"><p>آب هم او را شراب ناب گشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون درو آثار مستی شد پدید</p></div>
<div class="m2"><p>یک مرید او را از آن دم بر رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس بپرسیدش که این احوال خوش</p></div>
<div class="m2"><p>که برونست از حجاب پنج و شش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه سرخ و گاه زرد و گه سپید</p></div>
<div class="m2"><p>می‌شود رویت چه حالست و نوید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌کشی بوی و به ظاهر نیست گل</p></div>
<div class="m2"><p>بی‌شک از غیبست و از گلزار کل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای تو کام جان هر خودکامه‌ای</p></div>
<div class="m2"><p>هر دم از غیبت پیام و نامه‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر دمی یعقوب‌وار از یوسفی</p></div>
<div class="m2"><p>می‌رسد اندر مشام تو شفا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قطره‌ای بر ریز بر ما زان سبو</p></div>
<div class="m2"><p>شمه‌ای زان گلستان با ما بگو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خو نداریم ای جمال مهتری</p></div>
<div class="m2"><p>که لب ما خشک و تو تنها خوری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای فلک‌پیمای چست چست‌خیز</p></div>
<div class="m2"><p>زانچ خوردی جرعه‌ای بر ما بریز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میر مجلس نیست در دوران دگر</p></div>
<div class="m2"><p>جز تو ای شه در حریفان در نگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کی توان نوشید این می زیردست</p></div>
<div class="m2"><p>می یقین مر مرد را رسواگرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بوی را پوشیده و مکنون کند</p></div>
<div class="m2"><p>چشم مست خویشتن را چون کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خود نه آن بویست این که اندر جهان</p></div>
<div class="m2"><p>صد هزاران پرده‌اش دارد نهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پر شد از تیزی او صحرا و دشت</p></div>
<div class="m2"><p>دشت چه کز نه فلک هم در گذشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این سر خم را به کهگل در مگیر</p></div>
<div class="m2"><p>کین برهنه نیست خود پوشش‌پذیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لطف کن ای رازدان رازگو</p></div>
<div class="m2"><p>آنچ بازت صید کردش بازگو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت بوی بوالعجب آمد به من</p></div>
<div class="m2"><p>هم‌چنانک مر نبی را از یمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که محمد گفت بر دست صبا</p></div>
<div class="m2"><p>از یمن می‌آیدم بوی خدا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بوی رامین می‌رسد از جان ویس</p></div>
<div class="m2"><p>بوی یزدان می‌رسد هم از اویس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از اویس و از قرن بوی عجب</p></div>
<div class="m2"><p>مر نبی را مست کرد و پر طرب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون اویس از خویش فانی گشته بود</p></div>
<div class="m2"><p>آن زمینی آسمانی گشته بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن هلیلهٔ پروریده در شکر</p></div>
<div class="m2"><p>چاشنی تلخیش نبود دگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن هلیلهٔ رسته از ما و منی</p></div>
<div class="m2"><p>نقش دارد از هلیله طعم نی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این سخن پایان ندارد باز گرد</p></div>
<div class="m2"><p>تا چه گفت از وحی غیب آن شیرمرد</p></div></div>