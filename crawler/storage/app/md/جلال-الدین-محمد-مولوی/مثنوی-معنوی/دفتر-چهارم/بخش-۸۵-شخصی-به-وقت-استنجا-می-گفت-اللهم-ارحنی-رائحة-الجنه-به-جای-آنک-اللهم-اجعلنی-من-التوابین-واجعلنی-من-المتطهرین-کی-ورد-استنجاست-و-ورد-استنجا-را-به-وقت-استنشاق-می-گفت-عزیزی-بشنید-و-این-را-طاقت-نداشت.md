---
title: >-
    بخش ۸۵ - شخصی به وقت استنجا می‌گفت اللهم ارحنی رائحة الجنه به جای آنک اللهم اجعلنی من التوابین واجعلنی من  المتطهرین کی ورد استنجاست و ورد استنجا را به وقت استنشاق می‌گفت عزیزی بشنید و این را طاقت نداشت
---
# بخش ۸۵ - شخصی به وقت استنجا می‌گفت اللهم ارحنی رائحة الجنه به جای آنک اللهم اجعلنی من التوابین واجعلنی من  المتطهرین کی ورد استنجاست و ورد استنجا را به وقت استنشاق می‌گفت عزیزی بشنید و این را طاقت نداشت

<div class="b" id="bn1"><div class="m1"><p>آن یکی در وقت استنجا بگفت</p></div>
<div class="m2"><p>که مرا با بوی جنت دار جفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت شخصی خوب ورد آورده‌ای</p></div>
<div class="m2"><p>لیک سوراخ دعا گم کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این دعا چون ورد بینی بود چون</p></div>
<div class="m2"><p>ورد بینی را تو آوردی به کون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رایحهٔ جنت ز بینی یافت حر</p></div>
<div class="m2"><p>رایحهٔ جنت کم آید از دبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای تواضع برده پیش ابلهان</p></div>
<div class="m2"><p>وی تکبر برده تو پیش شهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن تکبر بر خسان خوبست و چست</p></div>
<div class="m2"><p>هین مرو معکوس عکسش بند تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پی سوراخ بینی رست گل</p></div>
<div class="m2"><p>بو وظیفهٔ بینی آمد ای عتل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی گل بهر مشامست ای دلیر</p></div>
<div class="m2"><p>جای آن بو نیست این سوراخ زیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی ازین جا بوی خلد آید ترا</p></div>
<div class="m2"><p>بو ز موضع جو اگر باید ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم‌چنین حب الوطن باشد درست</p></div>
<div class="m2"><p>تو وطن بشناس ای خواجه نخست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت آن ماهی زیرک ره کنم</p></div>
<div class="m2"><p>دل ز رای و مشورتشان بر کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست وقت مشورت هین راه کن</p></div>
<div class="m2"><p>چون علی تو آه اندر چاه کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محرم آن آه کم‌یابست بس</p></div>
<div class="m2"><p>شب رو و پنهان‌روی کن چون عسس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوی دریا عزم کن زین آب‌گیر</p></div>
<div class="m2"><p>بحر جو و ترک این گرداب گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سینه را پا ساخت می‌رفت آن حذور</p></div>
<div class="m2"><p>از مقام با خطر تا بحر نور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم‌چو آهو کز پی او سگ بود</p></div>
<div class="m2"><p>می‌دود تا در تنش یک رگ بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خواب خرگوش و سگ اندر پی خطاست</p></div>
<div class="m2"><p>خواب خود در چشم ترسنده کجاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفت آن ماهی ره دریا گرفت</p></div>
<div class="m2"><p>راه دور و پهنهٔ پهنا گرفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رنجها بسیار دید و عاقبت</p></div>
<div class="m2"><p>رفت آخر سوی امن و عافیت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خویشتن افکند در دریای ژرف</p></div>
<div class="m2"><p>که نیابد حد آن را هیچ طرف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پس چو صیادان بیاوردند دام</p></div>
<div class="m2"><p>نیم‌عاقل را از آن شد تلخ کام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت اه من فوت کردم فرصه را</p></div>
<div class="m2"><p>چون نگشتم همره آن رهنما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ناگهان رفت او ولیکن چونک رفت</p></div>
<div class="m2"><p>می‌ببایستم شدن در پی بتفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر گذشته حسرت آوردن خطاست</p></div>
<div class="m2"><p>باز ناید رفته یاد آن هباست</p></div></div>