---
title: >-
    بخش ۱۱۵ - مطالبه کردن موسی علیه‌السلام حضرت را کی خَلَقتَ خَلقاً اَهلَکتَهُم و جواب آمدن
---
# بخش ۱۱۵ - مطالبه کردن موسی علیه‌السلام حضرت را کی خَلَقتَ خَلقاً اَهلَکتَهُم و جواب آمدن

<div class="b" id="bn1"><div class="m1"><p>گفت موسی ای خداوند حساب</p></div>
<div class="m2"><p>نقش کردی باز چون کردی خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نر و ماده نقش کردی جان‌فزا</p></div>
<div class="m2"><p>وانگهان ویران کنی این را چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت حق دانم که این پرسش ترا</p></div>
<div class="m2"><p>نیست از انکار و غفلت وز هوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورنه تادیب و عتابت کردمی</p></div>
<div class="m2"><p>بهر این پرسش ترا آزردمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک می‌خواهی که در افعال ما</p></div>
<div class="m2"><p>باز جویی حکمت و سر بقا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا از آن واقف کنی مر عام را</p></div>
<div class="m2"><p>پخته گردانی بدین هر خام را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاصدا سایل شدی در کاشفی</p></div>
<div class="m2"><p>بر عوام ار چه که تو زان واقفی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآنک نیم علم آمد این سؤال</p></div>
<div class="m2"><p>هر برونی را نباشد آن مجال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم سؤال از علم خیزد هم جواب</p></div>
<div class="m2"><p>هم‌چنانک خار و گل از خاک و آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم ضلال از علم خیزد هم هدی</p></div>
<div class="m2"><p>هم‌چنانک تلخ و شیرین از ندا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آشنایی خیزد این بغض و ولا</p></div>
<div class="m2"><p>وز غذای خویش بود سقم و قوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مستفید اعجمی شد آن کلیم</p></div>
<div class="m2"><p>تا عجمیان را کند زین سر علیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما هم از وی اعجمی سازیم خویش</p></div>
<div class="m2"><p>پاسخش آریم چون بیگانه پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرفروشان خصم یکدیگر شدند</p></div>
<div class="m2"><p>تا کلید قفل آن عقد آمدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس بفرمودش خدا ای ذولباب</p></div>
<div class="m2"><p>چون بپرسیدی بیا بشنو جواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>موسیا تخمی بکار اندر زمین</p></div>
<div class="m2"><p>تا تو خود هم وا دهی انصاف این</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چونک موسی کشت و شد کشتش تمام</p></div>
<div class="m2"><p>خوشه‌هااش یافت خوبی و نظام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داس بگرفت و مر آن را می‌برید</p></div>
<div class="m2"><p>پس ندا از غیب در گوشش رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که چرا کشتی کنی و پروری</p></div>
<div class="m2"><p>چون کمالی یافت آن را می‌بری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت یا رب زان کنم ویران و پست</p></div>
<div class="m2"><p>که درینجا دانه هست و کاه هست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دانه لایق نیست درانبار کاه</p></div>
<div class="m2"><p>کاه در انبار گندم هم تباه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیست حکمت این دو را آمیختن</p></div>
<div class="m2"><p>فرق واجب می‌کند در بیختن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت این دانش تو از کی یافتی</p></div>
<div class="m2"><p>که به دانش بیدری بر ساختی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت تمییزم تو دادی ای خدا</p></div>
<div class="m2"><p>گفت پس تمییز چون نبود مرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در خلایق روحهای پاک هست</p></div>
<div class="m2"><p>روحهای تیرهٔ گلناک هست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این صدفها نیست در یک مرتبه</p></div>
<div class="m2"><p>در یکی درست و در دیگر شبه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>واجبست اظهار این نیک و تباه</p></div>
<div class="m2"><p>هم‌چنانک اظهار گندمها ز کاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهر اظهارست این خلق جهان</p></div>
<div class="m2"><p>تا نماند گنج حکمتها نهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنت کنزا کنت مخفیا شنو</p></div>
<div class="m2"><p>جوهر خود گم مکن اظهار شو</p></div></div>