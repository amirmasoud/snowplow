---
title: >-
    بخش ۵ - قصد خیانت کردن عاشق و بانگ  بر زدن معشوق بر وی
---
# بخش ۵ - قصد خیانت کردن عاشق و بانگ  بر زدن معشوق بر وی

<div class="b" id="bn1"><div class="m1"><p>چونک تنهااش بدید آن ساده مرد</p></div>
<div class="m2"><p>زود او قصد کنار و بوسه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانگ بر وی زد به هیبت آن نگار</p></div>
<div class="m2"><p>که مرو گستاخ ادب را هوش دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت آخر خلوتست و خلق نی</p></div>
<div class="m2"><p>آب حاضر تشنهٔ هم‌چون منی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس نمی‌جنبد درینجا جز که باد</p></div>
<div class="m2"><p>کیست حاضر کیست مانع زین گشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت ای شیدا تو ابله بوده‌ای</p></div>
<div class="m2"><p>ابلهی وز عاقلان نشنوده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد را دیدی که می‌جنبد بدان</p></div>
<div class="m2"><p>بادجنبانیست اینجا بادران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جزو بادی که به حکم ما درست</p></div>
<div class="m2"><p>بادبیزن تا نجنبانی نجست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جنبش این جزو باد ای ساده مرد</p></div>
<div class="m2"><p>بی‌تو و بی‌بادبیزن سر نکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنبش باد نفس کاندر لبست</p></div>
<div class="m2"><p>تابع تصریف جان و قالبست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاه دم را مدح و پیغامی کنی</p></div>
<div class="m2"><p>گاه دم را هجو و دشنامی کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس بدان احوال دیگر بادها</p></div>
<div class="m2"><p>که ز جزوی کل می‌بیند نهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد را حق گه بهاری می‌کند</p></div>
<div class="m2"><p>در دیش زین لطف عاری می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر گروه عاد صرصر می‌کند</p></div>
<div class="m2"><p>باز بر هودش معطر می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌کند یک باد را زهر سموم</p></div>
<div class="m2"><p>مر صبا را می‌کند خرم‌قدوم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باد دم را بر تو بنهاد او اساس</p></div>
<div class="m2"><p>تا کنی هر باد را بر وی قیاس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دم نمی‌گردد سخن بی‌لطف و قهر</p></div>
<div class="m2"><p>بر گروهی شهد و بر قومیست زهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مروحه جنبان پی انعام کس</p></div>
<div class="m2"><p>وز برای قهر هر پشه و مگس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مروحهٔ تقدیر ربانی چرا</p></div>
<div class="m2"><p>پر نباشد ز امتحان و ابتلا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چونک جزو باد دم یا مروحه</p></div>
<div class="m2"><p>نیست الا مفسده یا مصلحه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این شمال و این صبا و این دبور</p></div>
<div class="m2"><p>کی بود از لطف و از انعام دور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک کف گندم ز انباری ببین</p></div>
<div class="m2"><p>فهم کن کان جمله باشد همچنین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کل باد از برج باد آسمان</p></div>
<div class="m2"><p>کی جهد بی مروحهٔ آن بادران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر سر خرمن به وقت انتقاد</p></div>
<div class="m2"><p>نه که فلاحان ز حق جویند باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا جدا گردد ز گندم کاهها</p></div>
<div class="m2"><p>تا به انباری رود یا چاهها</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بماند دیر آن باد وزان</p></div>
<div class="m2"><p>جمله را بینی به حق لابه‌کنان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همچنین در طلق آن باد ولاد</p></div>
<div class="m2"><p>گر نیاید بانگ درد آید که داد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر نمی‌دانند کش راننده اوست</p></div>
<div class="m2"><p>باد را پس کردن زاری چه خوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اهل کشتی همچنین جویای باد</p></div>
<div class="m2"><p>جمله خواهانش از آن رب العباد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همچنین در درد دندانها ز باد</p></div>
<div class="m2"><p>دفع می‌خواهی بسوز و اعتقاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از خدا لابه‌کنان آن جندیان</p></div>
<div class="m2"><p>که بده باد ظفر ای کامران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رقعهٔ تعویذ می‌خواهند نیز</p></div>
<div class="m2"><p>در شکنجهٔ طلق زن از هر عزیز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس همه دانسته‌اند آن را یقین</p></div>
<div class="m2"><p>که فرستد باد رب‌العالمین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پس یقین در عقل هر داننده هست</p></div>
<div class="m2"><p>اینک با جنبنده جنباننده هست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر تو او را می‌نبینی در نظر</p></div>
<div class="m2"><p>فهم کن آن را به اظهار اثر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تن به جان جنبد نمی‌بینی تو جان</p></div>
<div class="m2"><p>لیک از جنبیدن تن جان بدان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت او گر ابلهم من در ادب</p></div>
<div class="m2"><p>زیرکم اندر وفا و در طلب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفت ادب این بود خود که دیده شد</p></div>
<div class="m2"><p>آن دگر را خود همی‌دانی تو لد</p></div></div>