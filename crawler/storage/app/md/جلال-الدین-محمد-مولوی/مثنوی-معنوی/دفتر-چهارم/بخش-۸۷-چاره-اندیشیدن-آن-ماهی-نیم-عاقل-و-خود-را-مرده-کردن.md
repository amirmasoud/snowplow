---
title: >-
    بخش ۸۷ - چاره اندیشیدن آن ماهی نیم‌عاقل و خود را مرده کردن
---
# بخش ۸۷ - چاره اندیشیدن آن ماهی نیم‌عاقل و خود را مرده کردن

<div class="b" id="bn1"><div class="m1"><p>گفت ماهی دگر وقت بلا</p></div>
<div class="m2"><p>چونک ماند از سایهٔ عاقل جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو سوی دریا شد و از غم عتیق</p></div>
<div class="m2"><p>فوت شد از من چنان نیکو رفیق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک زان نندیشم و بر خود زنم</p></div>
<div class="m2"><p>خویشتن را این زمان مرده کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس برآرم اشکم خود بر زبر</p></div>
<div class="m2"><p>پشت زیر و می‌روم بر آب بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌روم بر وی چنانک خس رود</p></div>
<div class="m2"><p>نی بسباحی چنانک کس رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرده گردم خویش بسپارم به آب</p></div>
<div class="m2"><p>مرگ پیش از مرگ امنست از عذاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرگ پیش از مرگ امنست ای فتی</p></div>
<div class="m2"><p>این چنین فرمود ما را مصطفی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت موتواکلکم من قبل ان</p></div>
<div class="m2"><p>یاتی الموت تموتوا بالفتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم‌چنان مرد و شکم بالا فکند</p></div>
<div class="m2"><p>آب می‌بردش نشیب و گه بلند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر یکی زان قاصدان بس غصه برد</p></div>
<div class="m2"><p>که دریغا ماهی بهتر بمرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاد می‌شد او کز آن گفت دریغ</p></div>
<div class="m2"><p>پیش رفت این بازیم رستم ز تیغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس گرفتش یک صیاد ارجمند</p></div>
<div class="m2"><p>پس برو تف کرد و بر خاکش فکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غلط غلطان رفت پنهان اندر آب</p></div>
<div class="m2"><p>ماند آن احمق همی‌کرد اضطراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از چپ و از راست می‌جست آن سلیم</p></div>
<div class="m2"><p>تا بجهد خویش برهاند گلیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دام افکندند و اندر دام ماند</p></div>
<div class="m2"><p>احمقی او را در آن آتش نشاند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر سر آتش به پشت تابه‌ای</p></div>
<div class="m2"><p>با حماقت گشت او همخوابه‌ایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او همی جوشید از تف سعیر</p></div>
<div class="m2"><p>عقل می‌گفتش الم یاتک نذیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>او همی‌گفت از شکنجه وز بلا</p></div>
<div class="m2"><p>هم‌چو جان کافران قالوا بلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باز می‌گفت او که گر این بار من</p></div>
<div class="m2"><p>وا رهم زین محنت گردن‌شکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من نسازم جز به دریایی وطن</p></div>
<div class="m2"><p>آبگیری را نسازم من سکن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آب بی‌حد جویم و آمن شوم</p></div>
<div class="m2"><p>تا ابد در امن و صحت می‌روم</p></div></div>