---
title: >-
    بخش ۴ - سؤال کردن از عیسی علیه‌السلام کی در وجود از همهٔ صعبها صعب‌تر چیست
---
# بخش ۴ - سؤال کردن از عیسی علیه‌السلام کی در وجود از همهٔ صعبها صعب‌تر چیست

<div class="b" id="bn1"><div class="m1"><p>گفت عیسی را یکی هشیار سر</p></div>
<div class="m2"><p>چیست در هستی ز جمله صعب‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتش ای جان صعب‌تر خشم خدا</p></div>
<div class="m2"><p>که از آن دوزخ همی لرزد چو ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت ازین خشم خدا چبود امان</p></div>
<div class="m2"><p>گفت ترک خشم خویش اندر زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس عوان که معدن این خشم گشت</p></div>
<div class="m2"><p>خشم زشتش از سبع هم در گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه امیدستش به رحمت جز مگر</p></div>
<div class="m2"><p>باز گردد زان صفت آن بی‌هنر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه عالم را ازیشان چاره نیست</p></div>
<div class="m2"><p>این سخن اندر ضلال افکندنیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاره نبود هم جهان را از چمین</p></div>
<div class="m2"><p>لیک نبود آن چمین ماء معین</p></div></div>