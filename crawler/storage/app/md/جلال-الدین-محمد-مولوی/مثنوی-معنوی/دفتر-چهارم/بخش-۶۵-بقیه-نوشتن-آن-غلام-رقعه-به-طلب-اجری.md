---
title: >-
    بخش ۶۵ - بقیهٔ نوشتن آن غلام رقعه به طلب اجری
---
# بخش ۶۵ - بقیهٔ نوشتن آن غلام رقعه به طلب اجری

<div class="b" id="bn1"><div class="m1"><p>رفت پیش از نامه پیش مطبخی</p></div>
<div class="m2"><p>کای بخیل از مطبخ شاه سخی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور ازو وز همت او کین قدر</p></div>
<div class="m2"><p>از جری‌ام آیدش اندر نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت بهر مصلحت فرموده است</p></div>
<div class="m2"><p>نه برای بخل و نه تنگی دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت دهلیزیست والله این سخن</p></div>
<div class="m2"><p>پیش شه خاکست هم زر کهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطبخی ده گونه حجت بر فراشت</p></div>
<div class="m2"><p>او همه رد کرد از حرصی که داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون جری کم آمدش در وقت چاشت</p></div>
<div class="m2"><p>زد بسی تشنیع او سودی نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت قاصد می‌کنید اینها شما</p></div>
<div class="m2"><p>گفت نه که بنده فرمانیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این مگیر از فرع این از اصل گیر</p></div>
<div class="m2"><p>بر کمان کم زن که از بازوست تیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما رمیت اذ رمیت ابتلاست</p></div>
<div class="m2"><p>بر نبی کم نه گنه کان از خداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب از سر تیره است ای خیره‌خشم</p></div>
<div class="m2"><p>پیشتر بنگر یکی بگشای چشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد ز خشم و غم درون بقعه‌ای</p></div>
<div class="m2"><p>سوی شه بنوشت خشمین رقعه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندر آن رقعه ثنای شاه گفت</p></div>
<div class="m2"><p>گوهر جود و سخای شاه سفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کای ز بحر و ابر افزون کف تو</p></div>
<div class="m2"><p>در قضای حاجت حاجات‌جو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانک ابر آنچ دهد گریان دهد</p></div>
<div class="m2"><p>کف تو خندان پیاپی خوان نهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ظاهر رقعه اگر چه مدح بود</p></div>
<div class="m2"><p>بوی خشم از مدح اثرها می‌نمود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان همه کار تو بی‌نورست و زشت</p></div>
<div class="m2"><p>که تو دوری دور از نور سرشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رونق کار خسان کاسد شود</p></div>
<div class="m2"><p>هم‌چو میوهٔ تازه زو فاسد شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رونق دنیا برآرد زو کساد</p></div>
<div class="m2"><p>زانک هست از عالم کون و فساد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوش نگردد از مدیحی سینه‌ها</p></div>
<div class="m2"><p>چونک در مداح باشد کینه‌ها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای دل از کین و کراهت پاک شو</p></div>
<div class="m2"><p>وانگهان الحمد خوان چالاک شو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر زبان الحمد و اکراه درون</p></div>
<div class="m2"><p>از زبان تلبیس باشد یا فسون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وانگهان گفته خدا که ننگرم</p></div>
<div class="m2"><p>من به ظاهر من به باطن ناظرم</p></div></div>