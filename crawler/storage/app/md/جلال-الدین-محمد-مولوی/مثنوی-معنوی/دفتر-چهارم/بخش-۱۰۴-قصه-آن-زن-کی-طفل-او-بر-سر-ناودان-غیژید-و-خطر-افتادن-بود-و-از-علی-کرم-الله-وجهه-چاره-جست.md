---
title: >-
    بخش ۱۰۴ - قصهٔ آن زن کی طفل او بر سر ناودان غیژید و خطر افتادن بود و از علی کرم‌الله وجهه چاره جست
---
# بخش ۱۰۴ - قصهٔ آن زن کی طفل او بر سر ناودان غیژید و خطر افتادن بود و از علی کرم‌الله وجهه چاره جست

<div class="b" id="bn1"><div class="m1"><p>یک زنی آمد به پیش مرتضی</p></div>
<div class="m2"><p>گفت شد بر ناودان طفلی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرش می‌خوانم نمی‌آید به دست</p></div>
<div class="m2"><p>ور هلم ترسم که افتد او به پست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست عاقل تا که دریابد چون ما</p></div>
<div class="m2"><p>گر بگویم کز خطر سوی من آ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم اشارت را نمی‌داند به دست</p></div>
<div class="m2"><p>ور بداند نشنود این هم بدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس نمودم شیر و پستان را بدو</p></div>
<div class="m2"><p>او همی گرداند از من چشم و رو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای حق شمایید ای مهان</p></div>
<div class="m2"><p>دستگیر این جهان و آن جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زود درمان کن که می‌لرزد دلم</p></div>
<div class="m2"><p>که بدرد از میوهٔ دل بسکلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت طفلی را بر آور هم به بام</p></div>
<div class="m2"><p>تا ببیند جنس خود را آن غلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوی جنس آید سبک زان ناودان</p></div>
<div class="m2"><p>جنس بر جنس است عاشق جاودان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زن چنان کرد و چو دید آن طفل او</p></div>
<div class="m2"><p>جنس خود خوش خوش بدو آورد رو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی بام آمد ز متن ناودان</p></div>
<div class="m2"><p>جاذب هر جنس را هم جنس دان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غژغژان آمد به سوی طفل طفل</p></div>
<div class="m2"><p>وا رهید او از فتادن سوی سفل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان بود جنس بشر پیغامبران</p></div>
<div class="m2"><p>تا بجنسیت رهند از ناودان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس بشر فرمود خود را مثلکم</p></div>
<div class="m2"><p>تا به جنس آیید و کم گردید گم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زانک جنسیت عجایب جاذبیست</p></div>
<div class="m2"><p>جاذبش جنسست هر جا طالبیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عیسی و ادریس بر گردون شدند</p></div>
<div class="m2"><p>با ملایک چونک هم‌جنس آمدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باز آن هاروت و ماروت از بلند</p></div>
<div class="m2"><p>جنس تن بودند زان زیر آمدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کافران هم جنس شیطان آمده</p></div>
<div class="m2"><p>جانشان شاگرد شیطانان شده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صد هزاران خوی بد آموخته</p></div>
<div class="m2"><p>دیده‌های عقل و دل بر دوخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کمترین خوشان به زشتی آن حسد</p></div>
<div class="m2"><p>آن حسد که گردن ابلیس زد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زان سگان آموخته حقد و حسد</p></div>
<div class="m2"><p>که نخواهد خلق را ملک ابد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر کرا دید او کمال از چپ و راست</p></div>
<div class="m2"><p>از حسد قولنجش آمد درد خاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زآنک هر بدبخت خرمن‌سوخته</p></div>
<div class="m2"><p>می‌نخواهد شمع کس افروخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هین کمالی دست آور تا تو هم</p></div>
<div class="m2"><p>از کمال دیگران نفتی به غم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از خدا می‌خواه دفع این حسد</p></div>
<div class="m2"><p>تا خدایت وا رهاند از جسد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مر ترا مشغولیی بخشد درون</p></div>
<div class="m2"><p>که نپردازی از آن سوی برون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جرعهٔ می را خدا آن می‌دهد</p></div>
<div class="m2"><p>که بدو مست از دو عالم می‌دهد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خاصیت بنهاده در کف حشیش</p></div>
<div class="m2"><p>کو زمانی می‌رهاند از خودیش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خواب را یزدان بدان سان می‌کند</p></div>
<div class="m2"><p>کز دو عالم فکر را بر می‌کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کرد مجنون را ز عشق پوستی</p></div>
<div class="m2"><p>کو بنشناسد عدو از دوستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صد هزاران این چنین می‌دارد او</p></div>
<div class="m2"><p>که بر ادراکات تو بگمارد او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هست میهای شقاوت نفس را</p></div>
<div class="m2"><p>که ز ره بیرون برد آن نحس را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هست میهای سعادت عقل را</p></div>
<div class="m2"><p>که بیابد منزل بی‌نقل را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خیمهٔ گردون ز سرمستی خویش</p></div>
<div class="m2"><p>بر کند زان سو بگیرد راه پیش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هین بهر مستی دلا غره مشو</p></div>
<div class="m2"><p>هست عیسی مست حق خر مست جو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>این چنین می را بجو زین خنبها</p></div>
<div class="m2"><p>مستی‌اش نبود ز کوته دنبها</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زانک هر معشوق چون خنبیست پر</p></div>
<div class="m2"><p>آن یکی درد و دگر صافی چو در</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می‌شناسا هین بچش با احتیاط</p></div>
<div class="m2"><p>تا میی یابی منزه ز اختلاط</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر دو مستی می‌دهندت لیک این</p></div>
<div class="m2"><p>مستی‌ات آرد کشان تا رب دین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا رهی از فکر و وسواس و حیل</p></div>
<div class="m2"><p>بی عقال این عقل در رقص‌الجمل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>انبیا چون جنس روحند و ملک</p></div>
<div class="m2"><p>مر ملک را جذب کردند از فلک</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باد جنس آتش است و یار او</p></div>
<div class="m2"><p>که بود آهنگ هر دو بر علو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون ببندی تو سر کوزهٔ تهی</p></div>
<div class="m2"><p>در میان حوض یا جویی نهی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا قیامت آن فرو ناید به پست</p></div>
<div class="m2"><p>که دلش خالیست و در وی باد هست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>میل بادش چون سوی بالا بود</p></div>
<div class="m2"><p>ظرف خود را هم سوی بالا کشد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>باز آن جانها که جنس انبیاست</p></div>
<div class="m2"><p>سوی‌ایشان کش کشان چون سایه‌هاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زانک عقلش غالبست و بی ز شک</p></div>
<div class="m2"><p>عقل جنس آمد به خلقت با ملک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وان هوای نفس غالب بر عدو</p></div>
<div class="m2"><p>نفس جنس اسفل آمد شد بدو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بود قبطی جنس فرعون ذمیم</p></div>
<div class="m2"><p>بود سبطی جنس موسی کلیم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بود هامان جنس‌تر فرعون را</p></div>
<div class="m2"><p>برگزیدش برد بر صدر سرا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لاجرم از صدر تا قعرش کشید</p></div>
<div class="m2"><p>که ز جنس دوزخ‌اند آن دو پلید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر دو سوزنده چو ذوزخ ضد نور</p></div>
<div class="m2"><p>هر دو چون دوزخ ز نور دل نفور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زانک دوزخ گوید ای مؤمن تو زود</p></div>
<div class="m2"><p>برگذر که نورت آتش را ربود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>می‌رمد آن دوزخی از نور هم</p></div>
<div class="m2"><p>زانک طبع دوزخستش ای صنم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دوزخ از مومن گریزد آنچنان</p></div>
<div class="m2"><p>که گریزد مومن از دوزخ به جان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زانک جنس نار نبود نور او</p></div>
<div class="m2"><p>ضد نار آمد حقیقت نورجو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در حدیث آمدی که مومن در دعا</p></div>
<div class="m2"><p>چون امان خواهد ز دوزخ از خدا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دوزخ از وی هم امان خواهد به جان</p></div>
<div class="m2"><p>که خدایا دور دارم از فلان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جاذبهٔ جنسیتست اکنون ببین</p></div>
<div class="m2"><p>که تو جنس کیستی از کفر و دین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گر بهامان مایلی هامانیی</p></div>
<div class="m2"><p>ور به موسی مایلی سبحانیی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ور بهر و مایلی انگیخته</p></div>
<div class="m2"><p>نفس و عقلی هر دوان آمیخته</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر دو در جنگند هان و هان بکوش</p></div>
<div class="m2"><p>تا شود غالب معانی بر نقوش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در جهان جنگ شادی این بسست</p></div>
<div class="m2"><p>که ببینی بر عدو هر دم شکست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آن ستیزه‌رو بسختی عاقبت</p></div>
<div class="m2"><p>گفت با هامان برای مشورت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وعده‌های آن کلیم‌الله را</p></div>
<div class="m2"><p>گفت و محرم ساخت آن گمراه را</p></div></div>