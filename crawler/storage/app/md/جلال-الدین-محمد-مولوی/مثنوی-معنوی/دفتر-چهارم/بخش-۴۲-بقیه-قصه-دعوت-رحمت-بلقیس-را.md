---
title: >-
    بخش ۴۲ - بقیهٔ قصهٔ دعوت رحمت بلقیس را
---
# بخش ۴۲ - بقیهٔ قصهٔ دعوت رحمت بلقیس را

<div class="b" id="bn1"><div class="m1"><p>خیز بلقیسا بیا و ملک بین</p></div>
<div class="m2"><p>بر لب دریای یزدان در بچین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهرانت ساکن چرخ سنی</p></div>
<div class="m2"><p>تو بمرداری چه سلطانی کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهرانت را ز بخششهای راد</p></div>
<div class="m2"><p>هیچ می‌دانی که آن سلطان چه داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ز شادی چون گرفتی طبل‌زن</p></div>
<div class="m2"><p>که منم شاه و رئیس گولحن</p></div></div>