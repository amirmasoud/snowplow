---
title: >-
    بخش ۲۵ - قصهٔ عطاری کی سنگ ترازوی او گل  سرشوی بود و دزدیدن  مشتری گل خوار از آن گل هنگام سنجیدن شکر دزدیده و پنهان
---
# بخش ۲۵ - قصهٔ عطاری کی سنگ ترازوی او گل  سرشوی بود و دزدیدن  مشتری گل خوار از آن گل هنگام سنجیدن شکر دزدیده و پنهان

<div class="b" id="bn1"><div class="m1"><p>پیش عطاری یکی گل‌خوار رفت</p></div>
<div class="m2"><p>تا خرد ابلوج قند خاص زفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس بر عطار طرار دودل</p></div>
<div class="m2"><p>موضع سنگ ترازو بود گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت گل سنگ ترازوی منست</p></div>
<div class="m2"><p>گر ترا میل شکر بخریدنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت هستم در مهمی قندجو</p></div>
<div class="m2"><p>سنگ میزان هر چه خواهی باش گو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت با خود پیش آنک گل‌خورست</p></div>
<div class="m2"><p>سنگ چه بود گل نکوتر از زرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم‌چو آن دلاله که گفت ای پسر</p></div>
<div class="m2"><p>نو عروسی یافتم بس خوب‌فر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخت زیبا لیک هم یک چیز هست</p></div>
<div class="m2"><p>که آن ستیره دختر حلواگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت بهتر این چنین خود گر بود</p></div>
<div class="m2"><p>دختر او چرب و شیرین‌تر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نداری سنگ و سنگت از گلست</p></div>
<div class="m2"><p>این به و به گل مرا میوهٔ دلست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندر آن کفهٔ ترازو ز اعتداد</p></div>
<div class="m2"><p>او به جای سنگ آن گل را نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس برای کفهٔ دیگر به دست</p></div>
<div class="m2"><p>هم به قدر آن شکر را می‌شکست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نبودش تیشه‌ای او دیر ماند</p></div>
<div class="m2"><p>مشتری را منتظر آنجا نشاند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رویش آن سو بود گل‌خور ناشکفت</p></div>
<div class="m2"><p>گل ازو پوشیده دزدیدن گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترس ترسان که نباید ناگهان</p></div>
<div class="m2"><p>چشم او بر من فتد از امتحان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دید عطار آن و خود مشغول کرد</p></div>
<div class="m2"><p>که فزون‌تر دزد هین ای روی‌زرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر بدزدی وز گل من می‌بری</p></div>
<div class="m2"><p>رو که هم از پهلوی خود می‌خوری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو همی ترسی ز من لیک از خری</p></div>
<div class="m2"><p>من همی‌ترسم که تو کمتر خوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه مشغولم چنان احمق نیم</p></div>
<div class="m2"><p>که شکر افزون کشی تو از نیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون ببینی مر شکر را ز آزمود</p></div>
<div class="m2"><p>پس بدانی احمق و غافل کی بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرغ زان دانه نظر خوش می‌کند</p></div>
<div class="m2"><p>دانه هم از دور راهش می‌زند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کز زنای چشم حظی می‌بری</p></div>
<div class="m2"><p>نه کباب از پهلوی خود می‌خوری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این نظر از دور چون تیرست و سم</p></div>
<div class="m2"><p>عشقت افزون می‌شود صبر تو کم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مال دنیا دام مرغان ضعیف</p></div>
<div class="m2"><p>ملک عقبی دام مرغان شریف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بدین ملکی که او دامست ژرف</p></div>
<div class="m2"><p>در شکار آرند مرغان شگرف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من سلیمان می‌نخواهم ملکتان</p></div>
<div class="m2"><p>بلک من برهانم از هر هلکتان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کین زمان هستید خود مملوک ملک</p></div>
<div class="m2"><p>مالک ملک آنک بجهید او ز هلک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بازگونه ای اسیر این جهان</p></div>
<div class="m2"><p>نام خود کردی امیر این جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای تو بندهٔ این جهان محبوس جان</p></div>
<div class="m2"><p>چند گویی خویش را خواجهٔ جهان</p></div></div>