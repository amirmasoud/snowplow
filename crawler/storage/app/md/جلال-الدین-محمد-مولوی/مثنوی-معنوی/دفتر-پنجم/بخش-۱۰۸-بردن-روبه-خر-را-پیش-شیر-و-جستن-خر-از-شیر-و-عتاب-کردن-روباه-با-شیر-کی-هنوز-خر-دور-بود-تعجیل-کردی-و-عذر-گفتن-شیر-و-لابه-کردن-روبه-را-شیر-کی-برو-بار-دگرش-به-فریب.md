---
title: >-
    بخش ۱۰۸ - بردن روبه خر را پیش شیر و جستن خر از شیر و عتاب کردن روباه با شیر کی هنوز خر دور بود تعجیل کردی و عذر گفتن  شیر و لابه کردن روبه را شیر کی برو بار دگرش به فریب
---
# بخش ۱۰۸ - بردن روبه خر را پیش شیر و جستن خر از شیر و عتاب کردن روباه با شیر کی هنوز خر دور بود تعجیل کردی و عذر گفتن  شیر و لابه کردن روبه را شیر کی برو بار دگرش به فریب

<div class="b" id="bn1"><div class="m1"><p>چونک بر کوهش بسوی مرج برد</p></div>
<div class="m2"><p>تا کند شیرش به حمله خرد و مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور بود از شیر و آن شیر از نبرد</p></div>
<div class="m2"><p>تا به نزدیک آمدن صبری نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنبدی کرد از بلندی شیر هول</p></div>
<div class="m2"><p>خود نبودش قوت و امکان حول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خر ز دورش دید و برگشت و گریز</p></div>
<div class="m2"><p>تا به زیر کوه تازان نعل ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت روبه شیر را ای شاه ما</p></div>
<div class="m2"><p>چون نکردی صبر در وقت وغا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به نزدیک تو آید آن غوی</p></div>
<div class="m2"><p>تا باندک حمله‌ای غالب شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکر شیطانست تعجیل و شتاب</p></div>
<div class="m2"><p>لطف رحمانست صبر و احتساب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور بود و حمله را دید و گریخت</p></div>
<div class="m2"><p>ضعف تو ظاهر شد و آب تو ریخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت من پنداشتم بر جاست زور</p></div>
<div class="m2"><p>تا بدین حد می‌ندانستم فتور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیز جوع و حاجتم از حد گذشت</p></div>
<div class="m2"><p>صبر و عقلم از تجوع یاوه گشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر توانی بار دیگر از خرد</p></div>
<div class="m2"><p>باز آوردن مر او را مسترد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منت بسیار دارم از تو من</p></div>
<div class="m2"><p>جهد کن باشد بیاری‌اش به فن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت آری گر خدا یاری دهد</p></div>
<div class="m2"><p>بر دل او از عمی مهری نهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس فراموشش شود هولی که دید</p></div>
<div class="m2"><p>از خری او نباشد این بعید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لیک چون آرم من او را بر متاز</p></div>
<div class="m2"><p>تا ببادش ندهی از تعجیل باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت آری تجربه کردم که من</p></div>
<div class="m2"><p>سخت رنجورم مخلخل گشته تن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا به نزدیکم نیاید خر تمام</p></div>
<div class="m2"><p>من نجنبم خفته باشم در قوام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفت روبه گفت ای شه همتی</p></div>
<div class="m2"><p>تا بپوشد عقل او را غفلتی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>توبه‌ها کردست خر با کردگار</p></div>
<div class="m2"><p>که نگردد غرهٔ هر نابکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>توبه‌هااش را به فن بر هم زنیم</p></div>
<div class="m2"><p>ما عدوی عقل و عهد روشنیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کلهٔ خر گوی فرزندان ماست</p></div>
<div class="m2"><p>فکرتش بازیچهٔ دستان ماست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عقل که آن باشد ز دوران زحل</p></div>
<div class="m2"><p>پیش عقل کل ندارد آن محل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از عطارد وز زحل دانا شد او</p></div>
<div class="m2"><p>ما ز داد کردگار لطف‌خو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>علم الانسان خم طغرای ماست</p></div>
<div class="m2"><p>علم عند الله مقصدهای ماست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تربیهٔ آن آفتاب روشنیم</p></div>
<div class="m2"><p>ربی الاعلی از آن رو می‌زنیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تجربه گر دارد او با این همه</p></div>
<div class="m2"><p>بشکند صد تجربه زین دمدمه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بوک توبه بشکند آن سست‌خو</p></div>
<div class="m2"><p>در رسد شومی اشکستن درو</p></div></div>