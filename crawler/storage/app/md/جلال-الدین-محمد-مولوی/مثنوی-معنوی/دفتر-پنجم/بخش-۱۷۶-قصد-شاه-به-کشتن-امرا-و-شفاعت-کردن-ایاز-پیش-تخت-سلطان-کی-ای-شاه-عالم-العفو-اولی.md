---
title: >-
    بخش ۱۷۶ - قصد شاه به کشتن امرا و شفاعت کردن  ایاز پیش تخت سلطان کی ای شاه عالم العفو اولی
---
# بخش ۱۷۶ - قصد شاه به کشتن امرا و شفاعت کردن  ایاز پیش تخت سلطان کی ای شاه عالم العفو اولی

<div class="b" id="bn1"><div class="m1"><p>پس ایاز مهرافزا بر جهید</p></div>
<div class="m2"><p>پیش تخت آن الغ سلطان دوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سجده‌ای کرد و گلوی خود گرفت</p></div>
<div class="m2"><p>کای قبادی کز تو چرخ آرد شگفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای همایی که همایان فرخی</p></div>
<div class="m2"><p>از تو دارند و سخاوت هر سخی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای کریمی که کرمهای جهان</p></div>
<div class="m2"><p>محو گردد پیش ایثارت نهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای لطیفی که گل سرخت بدید</p></div>
<div class="m2"><p>از خجالت پیرهن را بر درید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غفوری تو غفران چشم‌سیر</p></div>
<div class="m2"><p>روبهان بر شیر از عفو تو چیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز که عفو تو کرا دارد سند</p></div>
<div class="m2"><p>هر که با امر تو بی‌باکی کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غفلت و گستاخی این مجرمان</p></div>
<div class="m2"><p>از وفور عفو تست ای عفولان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دایما غفلت ز گستاخی دمد</p></div>
<div class="m2"><p>که برد تعظیم از دیده رمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غفلت و نسیان بد آموخته</p></div>
<div class="m2"><p>ز آتش تعظیم گردد سوخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیبتش بیداری و فطنت دهد</p></div>
<div class="m2"><p>سهو نسیان از دلش بیرون جهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقت غارت خواب ناید خلق را</p></div>
<div class="m2"><p>تا بنرباید کسی زو دلق را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواب چون در می‌رمد از بیم دلق</p></div>
<div class="m2"><p>خواب نسیان کی بود با بیم حلق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لاتؤاخذ ان نسینا شد گواه</p></div>
<div class="m2"><p>که بود نسیان بوجهی هم گناه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زانک استکمال تعظیم او نکرد</p></div>
<div class="m2"><p>ورنه نسیان در نیاوردی نبرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه نسیان لابد و ناچار بود</p></div>
<div class="m2"><p>در سبب ورزیدن او مختار بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که تهاون کرد در تعظیمها</p></div>
<div class="m2"><p>تا که نسیان زاد یا سهو و خطا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم‌چو مستی کو جنایتها کند</p></div>
<div class="m2"><p>گوید او معذور بودم من ز خود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گویدش لیکن سبب ای زشتکار</p></div>
<div class="m2"><p>از تو بد در رفتن آن اختیار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی‌خودی نامد بخود تش خواندی</p></div>
<div class="m2"><p>اختیارت خود نشد تش راندی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر رسیدی مستی بی‌جهد تو</p></div>
<div class="m2"><p>حفظ کردی ساقی جان عهد تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پشت‌دارت بودی او و عذرخواه</p></div>
<div class="m2"><p>من غلام زلت مست اله</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عفوهای جمله عالم ذره‌ای</p></div>
<div class="m2"><p>عکس عفوت ای ز تو هر بهره‌ای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عفوها گفته ثنای عفو تو</p></div>
<div class="m2"><p>نیست کفوش ایها الناس اتقوا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جانشان بخش و ز خودشان هم مران</p></div>
<div class="m2"><p>کام شیرین تو اند ای کامران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رحم کن بر وی که روی تو بدید</p></div>
<div class="m2"><p>فرقت تلخ تو چون خواهد کشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از فراق و هجر می‌گویی سخن</p></div>
<div class="m2"><p>هر چه خواهی کن ولیکن این مکن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صد هزاران مرگ تلخ شصت تو</p></div>
<div class="m2"><p>نیست مانند فراق روی تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تلخی هجر از ذکور و از اناث</p></div>
<div class="m2"><p>دور دار ای مجرمان را مستغاث</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر امید وصل تو مردن خوشست</p></div>
<div class="m2"><p>تلخی هجر تو فوق آتشست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گبر می‌گوید میان آن سقر</p></div>
<div class="m2"><p>چه غمم بودی گرم کردی نظر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کان نظر شیرین کنندهٔ رنجهاست</p></div>
<div class="m2"><p>ساحران را خونبهای دست و پاست</p></div></div>