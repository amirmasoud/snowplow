---
title: >-
    بخش ۷ - بیان آنک نماز و روزه و همه چیزهای برونی گواهیهاست بر نور اندرونی
---
# بخش ۷ - بیان آنک نماز و روزه و همه چیزهای برونی گواهیهاست بر نور اندرونی

<div class="b" id="bn1"><div class="m1"><p>این نماز و روزه و حج و جهاد</p></div>
<div class="m2"><p>هم گواهی دادنست از اعتقاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این زکات و هدیه و ترک حسد</p></div>
<div class="m2"><p>هم گواهی دادنست از سر خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوان و مهمانی پی اظهار راست</p></div>
<div class="m2"><p>کای مهان ما با شما گشتیم راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هدیه‌ها و ارمغان و پیش‌کش</p></div>
<div class="m2"><p>شد گواه آنک هستم با تو خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی کوشد به مالی یا فسون</p></div>
<div class="m2"><p>چیست دارم گوهری در اندرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهری دارم ز تقوی یا سخا</p></div>
<div class="m2"><p>این زکات و روزه در هر دو گوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزه گوید کرد تقوی از حلال</p></div>
<div class="m2"><p>در حرامش دان که نبود اتصال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وان زکاتش گفت کو از مال خویش</p></div>
<div class="m2"><p>می‌دهد پس چون بدزدد ز اهل کیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بطراری کند پس دو گواه</p></div>
<div class="m2"><p>جرح شد در محکمهٔ عدل اله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست صیاد ار کند دانه نثار</p></div>
<div class="m2"><p>نه ز رحم و جود بل بهر شکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست گربهٔ روزه‌دار اندر صیام</p></div>
<div class="m2"><p>خفته کرده خویش بهر صید خام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرده بدظن زین کژی صد قوم را</p></div>
<div class="m2"><p>کرده بدنام اهل جود و صوم را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فضل حق با این که او کژ می‌تند</p></div>
<div class="m2"><p>عاقبت زین جمله پاکش می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سبق برده رحمتش وان غدر را</p></div>
<div class="m2"><p>داده نوری که نباشد بدر را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کوششش را شسته حق زین اختلاط</p></div>
<div class="m2"><p>غسل داده رحمت او را زین خباط</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا که غفاری او ظاهر شود</p></div>
<div class="m2"><p>مغفری کلیش را غافر شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آب بهر این ببارید از سماک</p></div>
<div class="m2"><p>تا پلیدان را کند از خبث پاک</p></div></div>