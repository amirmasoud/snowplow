---
title: >-
    بخش ۱۲۸ - دعوت کردن مسلمان مغ را
---
# بخش ۱۲۸ - دعوت کردن مسلمان مغ را

<div class="b" id="bn1"><div class="m1"><p>مر مغی را گفت مردی کای فلان</p></div>
<div class="m2"><p>هین مسلمان شو بباش از مؤمنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت اگر خواهد خدا مؤمن شوم</p></div>
<div class="m2"><p>ور فزاید فضل هم موقن شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت می‌خواهد خدا ایمان تو</p></div>
<div class="m2"><p>تا رهد از دست دوزخ جان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک نفس نحس و آن شیطان زشت</p></div>
<div class="m2"><p>می‌کشندت سوی کفران و کنشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت ای منصف چو ایشان غالب‌اند</p></div>
<div class="m2"><p>یار او باشم که باشد زورمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار آن تانم بدن کو غالبست</p></div>
<div class="m2"><p>آن طرف افتم که غالب جاذبست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خدا می‌خواست از من صدق زفت</p></div>
<div class="m2"><p>خواست او چه سود چون پیشش نرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس و شیطان خواست خود را پیش برد</p></div>
<div class="m2"><p>وآن عنایت قهر گشت و خرد و مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو یکی قصر و سرایی ساختی</p></div>
<div class="m2"><p>اندرو صد نقش خوش افراختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواستی مسجد بود آن جای خیر</p></div>
<div class="m2"><p>دیگری آمد مر آن را ساخت دیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا تو بافیدی یکی کرباس تا</p></div>
<div class="m2"><p>خوش بسازی بهر پوشیدن قبا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو قبا می‌خواستی خصم از نبرد</p></div>
<div class="m2"><p>رغم تو کرباس را شلوار کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او زبون شد جرم این کرباس چیست</p></div>
<div class="m2"><p>آنک او مغلوب غالب نیست کیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون کسی بی‌خواست او بر وی براند</p></div>
<div class="m2"><p>خاربن در ملک و خانهٔ او نشاند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صاحب خانه بدین خواری بود</p></div>
<div class="m2"><p>که چنین بر وی خلاقت می‌رود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم خلق گردم من ار تازه و نوم</p></div>
<div class="m2"><p>چونک یار این چنین خواری شوم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چونک خواه نفس آمد مستعان</p></div>
<div class="m2"><p>تسخر آمد ایش شاء الله کان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من اگر ننگ مغان یا کافرم</p></div>
<div class="m2"><p>آن نیم که بر خدا این ظن برم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که کسی ناخواه او و رغم او</p></div>
<div class="m2"><p>گردد اندر ملکت او حکم جو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملکت او را فرو گیرد چنین</p></div>
<div class="m2"><p>که نیارد دم زدن دم آفرین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دفع او می‌خواهد و می‌بایدش</p></div>
<div class="m2"><p>دیو هر دم غصه می‌افزایدش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بندهٔ این دیو می‌باید شدن</p></div>
<div class="m2"><p>چونک غالب اوست در هر انجمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا مبادا کین کشد شیطان ز من</p></div>
<div class="m2"><p>پس چه دستم گیرد آنجا ذوالمنن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنک او خواهد مراد او شود</p></div>
<div class="m2"><p>از کی کار من دگر نیکو شود</p></div></div>