---
title: >-
    بخش ۱۰۳ - مثل آوردن اشتر در بیان آنک در مخبر دولتی فر و اثر آن  چون نبینی جای متهم داشتن باشد کی او مقلدست در آن
---
# بخش ۱۰۳ - مثل آوردن اشتر در بیان آنک در مخبر دولتی فر و اثر آن  چون نبینی جای متهم داشتن باشد کی او مقلدست در آن

<div class="b" id="bn1"><div class="m1"><p>آن یکی پرسید اشتر را که هی</p></div>
<div class="m2"><p>از کجا می‌آیی ای اقبال پی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت از حمام گرم کوی تو</p></div>
<div class="m2"><p>گفت خود پیداست در زانوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مار موسی دید فرعون عنود</p></div>
<div class="m2"><p>مهلتی می‌خواست نرمی می‌نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیرکان گفتند بایستی که این</p></div>
<div class="m2"><p>تندتر گشتی چو هست او رب دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معجزه‌ گر اژدها گر مار بد</p></div>
<div class="m2"><p>نخوت و خشم خدایی‌اش چه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رب اعلی گر ویست اندر جلوس</p></div>
<div class="m2"><p>بهر یک کرمی چیست این چاپلوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس تو تا مست نقلست و نبید</p></div>
<div class="m2"><p>دانک روحت خوشهٔ غیبی ندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که علاماتست زان دیدار نور</p></div>
<div class="m2"><p>التجافی منک عن دار الغرور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ چون بر آب شوری می‌تند</p></div>
<div class="m2"><p>آب شیرین را ندیدست او مدد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلک تقلیدست آن ایمان او</p></div>
<div class="m2"><p>روی ایمان را ندیده جان او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس خطر باشد مقلد را عظیم</p></div>
<div class="m2"><p>از ره و ره‌زن ز شیطان رجیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون ببیند نور حق آمن شود</p></div>
<div class="m2"><p>ز اضطرابات شک او ساکن شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا کف دریا نیاید سوی خاک</p></div>
<div class="m2"><p>که اصل او آمد بود در اصطکاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاکی است آن کف غریبست اندر آب</p></div>
<div class="m2"><p>در غریبی چاره نبود ز اضطراب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چونک چشمش باز شد و آن نقش خواند</p></div>
<div class="m2"><p>دیو را بر وی دگر دستی نماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه با روباه خر اسرار گفت</p></div>
<div class="m2"><p>سرسری گفت و مقلدوار گفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آب را بستود و او تایق نبود</p></div>
<div class="m2"><p>رخ درید و جامه او عاشق نبود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از منافق عذر رد آمد نه خوب</p></div>
<div class="m2"><p>زانک در لب بود آن نه در قلوب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بوی سیبش هست جزو سیب نیست</p></div>
<div class="m2"><p>بو درو جز از پی آسیب نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حملهٔ زن در میان کارزار</p></div>
<div class="m2"><p>نشکند صف بلک گردد کار زار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه می‌بینی چو شیر اندر صفش</p></div>
<div class="m2"><p>تیغ بگرفته همی‌لرزد کفش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وای آنک عقل او ماده بود</p></div>
<div class="m2"><p>نفس زشتش نر و آماده بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لاجرم مغلوب باشد عقل او</p></div>
<div class="m2"><p>جز سوی خسران نباشد نقل او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای خنک آن کس که عقلش نر بود</p></div>
<div class="m2"><p>نفس زشتش ماده و مضطر بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عقل جزوی‌اش نر و غالب بود</p></div>
<div class="m2"><p>نفس انثی را خرد سالب بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حملهٔ ماده به صورت هم جریست</p></div>
<div class="m2"><p>آفت او هم‌چو آن خر از خریست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وصف حیوانی بود بر زن فزون</p></div>
<div class="m2"><p>زانک سوی رنگ و بو دارد رکون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رنگ و بوی سبزه‌زار آن خر شنید</p></div>
<div class="m2"><p>جمله حجتها ز طبع او رمید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تشنه محتاج مطر شد وابر نه</p></div>
<div class="m2"><p>نفس را جوع البقر بد صبر نه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اسپر آهن بود صبر ای پدر</p></div>
<div class="m2"><p>حق نبشته بر سپر جاء الظفر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صد دلیل آرد مقلد در بیان</p></div>
<div class="m2"><p>از قیاسی گوید آن را نه از عیان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مشک‌آلودست الا مشک نیست</p></div>
<div class="m2"><p>بوی مشکستش ولی جز پشک نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا که پشکی مشک گردد ای مرید</p></div>
<div class="m2"><p>سالها باید در آن روضه چرید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که نباید خورد و جو هم‌چون خران</p></div>
<div class="m2"><p>آهوانه در ختن چر ارغوان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جز قرنفل یا سمن یا گل مچر</p></div>
<div class="m2"><p>رو به صحرای ختن با آن نفر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>معده را خو کن بدان ریحان و گل</p></div>
<div class="m2"><p>تا بیابی حکمت و قوت رسل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خوی معده زین که و جو باز کن</p></div>
<div class="m2"><p>خوردن ریحان و گل آغاز کن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>معدهٔ تن سوی کهدان می‌کشد</p></div>
<div class="m2"><p>معدهٔ دل سوی ریحان می‌کشد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر که کاه و جو خورد قربان شود</p></div>
<div class="m2"><p>هر که نور حق خورد قرآن شود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیم تو مشکست و نیمی پشک هین</p></div>
<div class="m2"><p>هین میفزا پشک افزا مشک چین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن مقلد صد دلیل و صد بیان</p></div>
<div class="m2"><p>در زبان آرد ندارد هیچ جان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چونک گوینده ندارد جان و فر</p></div>
<div class="m2"><p>گفت او را کی بود برگ و ثمر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>می‌کند گستاخ مردم را به راه</p></div>
<div class="m2"><p>او بجان لرزان‌ترست از برگ کاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس حدیثش گرچه بس با فر بود</p></div>
<div class="m2"><p>در حدیثش لرزه هم مضمر بود</p></div></div>