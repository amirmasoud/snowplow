---
title: >-
    بخش ۱۵ - مناجات
---
# بخش ۱۵ - مناجات

<div class="b" id="bn1"><div class="m1"><p>ای خدای بی‌نظیر ایثار کن</p></div>
<div class="m2"><p>گوش را چون حلقه دادی زین سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوش ما گیر و بدان مجلس کشان</p></div>
<div class="m2"><p>کز رحیقت می‌خورند آن سرخوشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به ما بویی رسانیدی ازین</p></div>
<div class="m2"><p>سر مبند آن مشک را ای رب دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو نوشند ار ذکورند ار اناث</p></div>
<div class="m2"><p>بی‌دریغی در عطا یا مستغاث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دعا ناگفته از تو مستجاب</p></div>
<div class="m2"><p>داده دل را هر دمی صد فتح باب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند حرفی نقش کردی از رقوم</p></div>
<div class="m2"><p>سنگها از عشق آن شد هم‌چو موم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نون ابرو صاد چشم و جیم گوش</p></div>
<div class="m2"><p>بر نوشتی فتنهٔ صد عقل و هوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان حروفت شد خرد باریک‌ریس</p></div>
<div class="m2"><p>نسخ می‌کن ای ادیب خوش‌نویس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خور هر فکر بسته بر عدم</p></div>
<div class="m2"><p>دم به دم نقش خیالی خوش رقم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حرفهای طرفه بر لوح خیال</p></div>
<div class="m2"><p>بر نوشته چشم و عارض خد و خال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر عدم باشم نه بر موجود مست</p></div>
<div class="m2"><p>زانک معشوق عدم وافی‌ترست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل را خط خوان آن اشکال کرد</p></div>
<div class="m2"><p>تا دهد تدبیرها را زان نورد</p></div></div>