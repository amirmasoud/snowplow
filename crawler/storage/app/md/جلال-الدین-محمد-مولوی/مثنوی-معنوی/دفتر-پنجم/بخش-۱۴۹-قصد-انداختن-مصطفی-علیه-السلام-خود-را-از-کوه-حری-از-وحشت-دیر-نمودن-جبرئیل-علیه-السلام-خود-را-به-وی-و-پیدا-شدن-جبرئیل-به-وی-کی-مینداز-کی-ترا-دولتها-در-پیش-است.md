---
title: >-
    بخش ۱۴۹ - قصد انداختن مصطفی علیه‌السلام خود را از کوه حری از وحشت دیر نمودن جبرئیل علیه‌السلام خود را به وی و پیدا شدن  جبرئیل به وی کی مینداز کی ترا دولتها در پیش است
---
# بخش ۱۴۹ - قصد انداختن مصطفی علیه‌السلام خود را از کوه حری از وحشت دیر نمودن جبرئیل علیه‌السلام خود را به وی و پیدا شدن  جبرئیل به وی کی مینداز کی ترا دولتها در پیش است

<div class="b" id="bn1"><div class="m1"><p>مصطفی را هجر چون بفراختی</p></div>
<div class="m2"><p>خویش را از کوه می‌انداختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بگفتی جبرئیلش هین مکن</p></div>
<div class="m2"><p>که ترا بس دولتست از امر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مصطفی ساکن شدی ز انداختن</p></div>
<div class="m2"><p>باز هجران آوریدی تاختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز خود را سرنگون از کوه او</p></div>
<div class="m2"><p>می‌فکندی از غم و اندوه او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز خود پیدا شدی آن جبرئیل</p></div>
<div class="m2"><p>که مکن این ای تو شاه بی‌بدیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم‌چنین می‌بود تا کشف حجاب</p></div>
<div class="m2"><p>تا بیابید آن گهر را او ز جیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر هر محنت چو خود را می‌کشند</p></div>
<div class="m2"><p>اصل محنتهاست این چونش کشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از فدایی مردمان را حیرتیست</p></div>
<div class="m2"><p>هر یکی از ما فدای سیرتیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خنک آنک فدا کردست تن</p></div>
<div class="m2"><p>بهر آن کارزد فدای آن شدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر یکی چونک فدایی فنیست</p></div>
<div class="m2"><p>کاندر آن ره صرف عمر و کشتنیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشتنی اندر غروبی یا شروق</p></div>
<div class="m2"><p>که نه شایق ماند آنگه نه مشوق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باری این مقبل فدای این فنست</p></div>
<div class="m2"><p>کاندرو صد زندگی در کشتنست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاشق و معشوق و عشقش بر دوام</p></div>
<div class="m2"><p>در دو عالم بهرمند و نیک‌نام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا کرامی ارحموا اهل الهوی</p></div>
<div class="m2"><p>شانهم ورد التوی بعد التوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عفو کن ای میر بر سختی او</p></div>
<div class="m2"><p>در نگر در درد و بدبختی او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا ز جرمت هم خدا عفوی کند</p></div>
<div class="m2"><p>زلتت را مغفرت در آکند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو ز غفلت بس سبو بشکسته‌ای</p></div>
<div class="m2"><p>در امید عفو دل در بسته‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عفو کن تا عفو یابی در جزا</p></div>
<div class="m2"><p>می‌شکافد مو قدر اندر سزا</p></div></div>