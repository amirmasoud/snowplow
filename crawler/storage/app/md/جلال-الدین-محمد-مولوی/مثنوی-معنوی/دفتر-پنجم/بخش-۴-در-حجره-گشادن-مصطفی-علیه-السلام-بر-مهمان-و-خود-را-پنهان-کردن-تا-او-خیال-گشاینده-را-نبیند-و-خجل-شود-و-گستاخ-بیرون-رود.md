---
title: >-
    بخش ۴ - در حجره گشادن مصطفی علیه‌السلام بر مهمان و خود را پنهان  کردن تا او خیال گشاینده را نبیند و خجل شود و گستاخ  بیرون رود
---
# بخش ۴ - در حجره گشادن مصطفی علیه‌السلام بر مهمان و خود را پنهان  کردن تا او خیال گشاینده را نبیند و خجل شود و گستاخ  بیرون رود

<div class="b" id="bn1"><div class="m1"><p>مصطفی صبح آمد و در را گشاد</p></div>
<div class="m2"><p>صبح آن گمراه را او راه داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گشاد و گشت پنهان مصطفی</p></div>
<div class="m2"><p>تا نگردد شرمسار آن مبتلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا برون آید رود گستاخ او</p></div>
<div class="m2"><p>تا نبیند درگشا را پشت و رو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا نهان شد در پس چیزی و یا</p></div>
<div class="m2"><p>از ویش پوشید دامان خدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبغة الله گاه پوشیده کند</p></div>
<div class="m2"><p>پردهٔ بی‌چون بر آن ناظر تند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نبیند خصم را پهلوی خویش</p></div>
<div class="m2"><p>قدرت یزدان از آن بیشست بیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مصطفی می‌دید احوال شبش</p></div>
<div class="m2"><p>لیک مانع بود فرمان ربش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که پیش از خبط بگشاید رهی</p></div>
<div class="m2"><p>تا نیفتد زان فضیحت در چهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیک حکمت بود و امر آسمان</p></div>
<div class="m2"><p>تا ببیند خویشتن را او چنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس عداوتها که آن یاری بود</p></div>
<div class="m2"><p>بس خرابیها که معماری بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جامه خواب پر حدث را یک فضول</p></div>
<div class="m2"><p>قاصدا آورد در پیش رسول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که چنین کردست مهمانت ببین</p></div>
<div class="m2"><p>خنده‌ای زد رحمةللعالمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که بیار آن مطهره اینجا به پیش</p></div>
<div class="m2"><p>تا بشویم جمله را با دست خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کسی می‌جست کز بهر خدا</p></div>
<div class="m2"><p>جان ما و جسم ما قربان ترا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ما بشوییم این حدث را تو بهل</p></div>
<div class="m2"><p>کار دستست این نمط نه کار دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای لعمرک مر ترا حق عمر خواند</p></div>
<div class="m2"><p>پس خلیفه کرد و بر کرسی نشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما برای خدمت تو می‌زییم</p></div>
<div class="m2"><p>چون تو خدمت می‌کنی پس ما چه‌ایم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت آن دانم و لیک این ساعتیست</p></div>
<div class="m2"><p>که درین شستن بخویشم حکمتیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>منتظر بودند کین قول نبیست</p></div>
<div class="m2"><p>تا پدید آید که این اسرار چیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>او به جد می‌شست آن احداث را</p></div>
<div class="m2"><p>خاص ز امر حق نه تقلید و ریا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که دلش می‌گفت کین را تو بشو</p></div>
<div class="m2"><p>که درین جا هست حکمت تو بتو</p></div></div>