---
title: >-
    بخش ۷۵ - بیان آنک آنچ بیان کرده می‌شود صورت قصه است وانگه آن صورتیست کی در خورد این صورت گیرانست و درخورد آینهٔ تصویر ایشان و از قدوسیتی کی حقیقت این قصه راست نطق را ازین تنزیل شرم می‌آید و از خجالت سر و ریش و قلم گم می‌کند و العاقل یکفیه الاشاره
---
# بخش ۷۵ - بیان آنک آنچ بیان کرده می‌شود صورت قصه است وانگه آن صورتیست کی در خورد این صورت گیرانست و درخورد آینهٔ تصویر ایشان و از قدوسیتی کی حقیقت این قصه راست نطق را ازین تنزیل شرم می‌آید و از خجالت سر و ریش و قلم گم می‌کند و العاقل یکفیه الاشاره

<div class="b" id="bn1"><div class="m1"><p>زانک پیلم دید هندستان به خواب</p></div>
<div class="m2"><p>از خراج اومید بر ده شد خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیف یاتی النظم لی والقافیه</p></div>
<div class="m2"><p>بعد ما ضاعت اصول العافیه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما جنون واحد لی فی الشجون</p></div>
<div class="m2"><p>بل جنون فی جنون فی جنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذاب جسمی من اشارات الکنی</p></div>
<div class="m2"><p>منذ عاینت البقاء فی الفنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ایاز از عشق تو گشتم چو موی</p></div>
<div class="m2"><p>ماندم از قصه تو قصهٔ من بگوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس فسانهٔ عشق تو خواندم به جان</p></div>
<div class="m2"><p>تو مرا که افسانه گشتستم بخوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود تو می‌خوانی نه من ای مقتدی</p></div>
<div class="m2"><p>من که طورم تو موسی وین صدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوه بیچاره چه داند گفت چیست</p></div>
<div class="m2"><p>زانک موسی می‌بداند که تهیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوه می‌داند به قدر خویشتن</p></div>
<div class="m2"><p>اندکی دارد ز لطف روح تن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن چو اصطرلاب باشد ز احتساب</p></div>
<div class="m2"><p>آیتی از روح هم‌چون آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن منجم چون نباشد چشم‌تیز</p></div>
<div class="m2"><p>شرط باشد مرد اصطرلاب‌ریز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا صطرلابی کند از بهر او</p></div>
<div class="m2"><p>تا برد از حالت خورشید بو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان کز اصطرلاب جوید او صواب</p></div>
<div class="m2"><p>چه قدر داند ز چرخ و آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو که ز اصطرلاب دیده بنگری</p></div>
<div class="m2"><p>درجهان دیدن یقین بس قاصری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو جهان را قدر دیده دیده‌ای</p></div>
<div class="m2"><p>کو جهان سبلت چرا مالیده‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عارفان را سرمه‌ای هست آن بجوی</p></div>
<div class="m2"><p>تا که دریا گردد این چشم چو جوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ذره‌ای از عقل و هوش ار با منست</p></div>
<div class="m2"><p>این چه سودا و پریشان گفتنست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چونک مغز من ز عقل و هش تهیست</p></div>
<div class="m2"><p>پس گناه من درین تخلیط چیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه گناه اوراست که عقلم ببرد</p></div>
<div class="m2"><p>عقل جملهٔ عاقلان پیشش بمرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا مجیر العقل فتان الحجی</p></div>
<div class="m2"><p>ما سواک للعقول مرتجی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ما اشتهیت العقل مذ جننتنی</p></div>
<div class="m2"><p>ما حسدت الحسن مذ زینتنی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هل جنونی فی هواک مستطاب</p></div>
<div class="m2"><p>قل بلی والله یجزیک الثواب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بتازی گوید او ور پارسی</p></div>
<div class="m2"><p>گوش و هوشی کو که در فهمش رسی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بادهٔ او درخور هر هوش نیست</p></div>
<div class="m2"><p>حلقهٔ او سخرهٔ هر گوش نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بار دیگر آمدم دیوانه‌وار</p></div>
<div class="m2"><p>رو رو ای جان زود زنجیری بیار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غیر آن زنجیر زلف دلبرم</p></div>
<div class="m2"><p>گر دو صد زنجیر آری بردرم</p></div></div>