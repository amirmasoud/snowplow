---
title: >-
    بخش ۱۳۳ - حکایت هم در جواب جبری و اثبات اختیار و صحت امر و نهی و بیان آنک عذر جبری در هیچ ملتی و در هیچ دینی مقبول نیست و موجب خلاص نیست از سزای آن کار  کی کرده است چنانک خلاص نیافت ابلیس جبری بدان کی گفت بما اغویتنی والقلیل یدل علی الکثیر
---
# بخش ۱۳۳ - حکایت هم در جواب جبری و اثبات اختیار و صحت امر و نهی و بیان آنک عذر جبری در هیچ ملتی و در هیچ دینی مقبول نیست و موجب خلاص نیست از سزای آن کار  کی کرده است چنانک خلاص نیافت ابلیس جبری بدان کی گفت بما اغویتنی والقلیل یدل علی الکثیر

<div class="b" id="bn1"><div class="m1"><p>آن یکی می‌رفت بالای درخت</p></div>
<div class="m2"><p>می‌فشاند آن میوه را دزدانه سخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب باغ آمد و گفت ای دنی</p></div>
<div class="m2"><p>از خدا شرمیت کو چه می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت از باغ خدا بندهٔ خدا</p></div>
<div class="m2"><p>گر خورد خرما که حق کردش عطا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عامیانه چه ملامت می‌کنی</p></div>
<div class="m2"><p>بخل بر خوان خداوند غنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت ای ایبک بیاور آن رسن</p></div>
<div class="m2"><p>تا بگویم من جواب بوالحسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس ببستش سخت آن دم بر درخت</p></div>
<div class="m2"><p>می‌زد او بر پشت و ساقش چوب سخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آخر از خدا شرمی بدار</p></div>
<div class="m2"><p>می‌کشی این بی‌گنه را زار زار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت از چوب خدا این بنده‌اش</p></div>
<div class="m2"><p>می‌زند بر پشت دیگر بنده خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چوب حق و پشت و پهلو آن او</p></div>
<div class="m2"><p>من غلام و آلت فرمان او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت توبه کردم از جبر ای عیار</p></div>
<div class="m2"><p>اختیارست اختیارست اختیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اختیارات اختیارش هست کرد</p></div>
<div class="m2"><p>اختیارش چون سواری زیر گرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اختیارش اختیار ما کند</p></div>
<div class="m2"><p>امر شد بر اختیاری مستند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حاکمی بر صورت بی‌اختیار</p></div>
<div class="m2"><p>هست هر مخلوق را در اقتدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا کشد بی‌اختیاری صید را</p></div>
<div class="m2"><p>تا برد بگرفته گوش او زید را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لیک بی هیچ آلتی صنع صمد</p></div>
<div class="m2"><p>اختیارش را کمند او کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اختیارش زید را قدیش کند</p></div>
<div class="m2"><p>بی‌سگ و بی‌دام حق صیدش کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن دروگر حاکم چوبی بود</p></div>
<div class="m2"><p>وآن مصور حاکم خوبی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست آهنگر بر آهن قیمی</p></div>
<div class="m2"><p>هست بنا هم بر آلت حاکمی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نادر این باشد که چندین اختیار</p></div>
<div class="m2"><p>ساجد اندر اختیارش بنده‌وار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قدرت تو بر جمادات از نبرد</p></div>
<div class="m2"><p>کی جمادی را از آنها نفی کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قدرتش بر اختیارات آنچنان</p></div>
<div class="m2"><p>نفی نکند اختیاری را از آن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خواستش می‌گوی بر وجه کمال</p></div>
<div class="m2"><p>که نباشد نسبت جبر و ضلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چونک گفتی کفر من خواست ویست</p></div>
<div class="m2"><p>خواست خود را نیز هم می‌دان که هست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زانک بی‌خواه تو خود کفر تو نیست</p></div>
<div class="m2"><p>کفر بی‌خواهش تناقض گفتنیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>امر عاجز را قبیحست و ذمیم</p></div>
<div class="m2"><p>خشم بتر خاصه از رب رحیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گاو گر یوغی نگیرد می‌زنند</p></div>
<div class="m2"><p>هیچ گاوی که نپرد شد نژند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گاو چون معذور نبود در فضول</p></div>
<div class="m2"><p>صاحب گاو از چه معذورست و دول</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون نه‌ای رنجور سر را بر مبند</p></div>
<div class="m2"><p>اختیارت هست بر سبلت مخند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهد کن کز جام حق یابی نوی</p></div>
<div class="m2"><p>بی‌خود و بی‌اختیار آنگه شوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنگه آن می را بود کل اختیار</p></div>
<div class="m2"><p>تو شوی معذور مطلق مست‌وار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هرچه گویی گفتهٔ می باشد آن</p></div>
<div class="m2"><p>هر چه روبی رفتهٔ می باشد آن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کی کند آن مست جز عدل و صواب</p></div>
<div class="m2"><p>که ز جام حق کشیدست او شراب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جادوان فرعون را گفتند بیست</p></div>
<div class="m2"><p>مست را پروای دست و پای نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دست و پای ما می آن واحدست</p></div>
<div class="m2"><p>دست ظاهر سایه است و کاسدست</p></div></div>