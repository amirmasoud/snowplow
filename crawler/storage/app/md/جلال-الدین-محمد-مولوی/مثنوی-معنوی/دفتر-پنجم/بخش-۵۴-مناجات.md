---
title: >-
    بخش ۵۴ - مناجات
---
# بخش ۵۴ - مناجات

<div class="b" id="bn1"><div class="m1"><p>ای دهندهٔ قوت و تمکین و ثبات</p></div>
<div class="m2"><p>خلق را زین بی‌ثباتی ده نجات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر آن کاری که ثابت بودنیست</p></div>
<div class="m2"><p>قایمی ده نفس را که منثنیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبرشان بخش و کفهٔ میزان گران</p></div>
<div class="m2"><p>وا رهانشان از فن صورتگران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز حسودی بازشان خر ای کریم</p></div>
<div class="m2"><p>تا نباشند از حسد دیو رجیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نعیم فانی مال و جسد</p></div>
<div class="m2"><p>چون همی‌سوزند عامه از حسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پادشاهان بین که لشکر می‌کشند</p></div>
<div class="m2"><p>از حسد خویشان خود را می‌کشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان لعبتان پر قذر</p></div>
<div class="m2"><p>کرده قصد خون و جان همدگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ویس و رامین خسرو و شیرین بخوان</p></div>
<div class="m2"><p>که چه کردند از حسد آن ابلهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که فنا شد عاشق و معشوق نیز</p></div>
<div class="m2"><p>هم نه چیزند و هواشان هم نه چیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاک الهی که عدم بر هم زند</p></div>
<div class="m2"><p>مر عدم را بر عدم عاشق کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دل نه‌دل حسدها سر کند</p></div>
<div class="m2"><p>نیست را هست این چنین مضطر کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این زنانی کز همه مشفق‌تراند</p></div>
<div class="m2"><p>از حسد دو ضره خود را می‌خورند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا که مردانی که خود سنگین‌دلند</p></div>
<div class="m2"><p>از حسد تا در کدامین منزلند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نکردی شرع افسونی لطیف</p></div>
<div class="m2"><p>بر دریدی هر کسی جسم حریف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شرع بهر دفع شر رایی زند</p></div>
<div class="m2"><p>دیو را در شیشهٔ حجت کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از گواه و از یمین و از نکول</p></div>
<div class="m2"><p>تا به شیشه در رود دیو فضول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مثل میزانی که خشنودی دو ضد</p></div>
<div class="m2"><p>جمع می‌آید یقین در هزل و جد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شرع چون کیله و ترازو دان یقین</p></div>
<div class="m2"><p>که بدو خصمان رهند از جنگ و کین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ترازو نبود آن خصم از جدال</p></div>
<div class="m2"><p>کی رهد از وهم حیف و احتیال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس درین مردار زشت بی‌وفا</p></div>
<div class="m2"><p>این همه رشکست و خصمست و جفا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پس در آن اقبال و دولت چون بود</p></div>
<div class="m2"><p>چون شود جنی و انسی در حسد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن شیاطین خود حسود کهنه‌اند</p></div>
<div class="m2"><p>یک زمان از ره‌زنی خالی نه‌اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وآن بنی آدم که عصیان کشته‌اند</p></div>
<div class="m2"><p>از حسودی نیز شیطان گشته‌اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از نبی برخوان که شیطانان انس</p></div>
<div class="m2"><p>گشته‌اند از مسخ حق با دیو جنس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دیو چون عاجز شود در افتتان</p></div>
<div class="m2"><p>استعانت جوید او زین انسیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که شما یارید با ما یاریی</p></div>
<div class="m2"><p>جانب مایید جانب داریی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر کسی را ره زنند اندر جهان</p></div>
<div class="m2"><p>هر دو گون شیطان بر آید شادمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ور کسی جان برد و شد در دین بلند</p></div>
<div class="m2"><p>نوحه می‌دارند آن دو رشک‌مند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر دو می‌خایند دندان حسد</p></div>
<div class="m2"><p>بر کسی که داد ادیب او را خرد</p></div></div>