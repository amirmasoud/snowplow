---
title: >-
    بخش ۱۴۴ - حکایت آن زن کی گفت شوهر را کی گوشت را گربه خورد شوهر گربه را به ترازو بر کشید گربه نیم من برآمد گفت ای زن گوشت نیم من بود و افزون اگر این گوشتست گربه کو و اگر این گربه است گوشت کو
---
# بخش ۱۴۴ - حکایت آن زن کی گفت شوهر را کی گوشت را گربه خورد شوهر گربه را به ترازو بر کشید گربه نیم من برآمد گفت ای زن گوشت نیم من بود و افزون اگر این گوشتست گربه کو و اگر این گربه است گوشت کو

<div class="b" id="bn1"><div class="m1"><p>بود مردی کدخدا او را زنی</p></div>
<div class="m2"><p>سخت طناز و پلید و ره‌زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه آوردی تلف کردیش زن</p></div>
<div class="m2"><p>مرد مضطر بود اندر تن زدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر مهمان گوشت آورد آن معیل</p></div>
<div class="m2"><p>سوی خانه با دو صد جهد طویل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زن بخوردش با کباب و با شراب</p></div>
<div class="m2"><p>مرد آمد گفت دفع ناصواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد گفتش گوشت کو مهمان رسید</p></div>
<div class="m2"><p>پیش مهمان لوت می‌باید کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت زن این گربه خورد آن گوشت را</p></div>
<div class="m2"><p>گوشت دیگر خر اگر باشد هلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ای ایبک ترازو را بیار</p></div>
<div class="m2"><p>گربه را من بر کشم اندر عیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر کشیدش بود گربه نیم من</p></div>
<div class="m2"><p>پس بگفت آن مرد کای محتال زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوشت نیم من بود و افزون یک ستیر</p></div>
<div class="m2"><p>هست گربه نیم‌من هم ای ستیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این اگر گربه‌ست پس آن گوشت کو</p></div>
<div class="m2"><p>ور بود این گوشت گربه کو بجو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بایزید ار این بود آن روح چیست</p></div>
<div class="m2"><p>ور وی آن روحست این تصویر کیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیرت اندر حیرتست ای یار من</p></div>
<div class="m2"><p>این نه کار تست و نه هم کار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر دو او باشد ولیک از ریع زرع</p></div>
<div class="m2"><p>دانه باشد اصل و آن که پره فرع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حکمت این اضداد را با هم ببست</p></div>
<div class="m2"><p>ای قصاب این گردران با گردنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روح بی‌قالب نداند کار کرد</p></div>
<div class="m2"><p>قالبت بی‌جان فسرده بود و سرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قالبت پیدا و آن جانت نهان</p></div>
<div class="m2"><p>راست شد زین هر دو اسباب جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاک را بر سر زنی سر نشکند</p></div>
<div class="m2"><p>آب را بر سر زنی در نشکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر تو می‌خواهی که سر را بشکنی</p></div>
<div class="m2"><p>آب را و خاک را بر هم زنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون شکستی سر رود آبش به اصل</p></div>
<div class="m2"><p>خاک سوی خاک آید روز فصل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حکمتی که بود حق را ز ازدواج</p></div>
<div class="m2"><p>گشت حاصل از نیاز و از لجاج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باشد آنگه ازدواجات دگر</p></div>
<div class="m2"><p>لا سمع اذن و لا عین بصر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر شنیدی اذن کی ماندی اذن</p></div>
<div class="m2"><p>یا کجا کردی دگر ضبط سخن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بدیدی برف و یخ خورشید را</p></div>
<div class="m2"><p>از یخی برداشتی اومید را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آب گشتی بی‌عروق و بی‌گره</p></div>
<div class="m2"><p>ز آب داود هوا کردی زره</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس شدی درمان جان هر درخت</p></div>
<div class="m2"><p>هر درختی از قدومش نیک‌بخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن یخی بفسرده در خود مانده</p></div>
<div class="m2"><p>لا مساسی با درختان خوانده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لیس یالف لیس یؤلف جسمه</p></div>
<div class="m2"><p>لیس الا شح نفس قسمه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیست ضایع زو شود تازه جگر</p></div>
<div class="m2"><p>لیک نبود پیک و سلطان خضر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای ایاز استارهٔ تو بس بلند</p></div>
<div class="m2"><p>نیست هر برجی عبورش را پسند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر وفا را کی پسندد همتت</p></div>
<div class="m2"><p>هر صفا را کی گزیند صفوتت</p></div></div>