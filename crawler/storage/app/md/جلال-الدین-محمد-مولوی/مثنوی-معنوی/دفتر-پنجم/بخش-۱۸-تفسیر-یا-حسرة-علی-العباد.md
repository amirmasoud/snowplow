---
title: >-
    بخش ۱۸ - تفسیر یا حسرة علی العباد
---
# بخش ۱۸ - تفسیر یا حسرة علی العباد

<div class="b" id="bn1"><div class="m1"><p>او همی گوید که از اشکال تو</p></div>
<div class="m2"><p>غره گشتم دیر دیدم حال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع مرده باده رفته دلربا</p></div>
<div class="m2"><p>غوطه خورد از ننگ کژبینی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظلت الارباح خسرا مغرما</p></div>
<div class="m2"><p>نشتکی شکوی الی الله العمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حبذا ارواح اخوان ثقات</p></div>
<div class="m2"><p>مسلمات مؤمنات قانتات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی رویی به سویی برده‌اند</p></div>
<div class="m2"><p>وان عزیزان رو به بی‌سو کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کبوتر می‌پرد در مذهبی</p></div>
<div class="m2"><p>وین کبوتر جانب بی‌جانبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما نه مرغان هوا نه خانگی</p></div>
<div class="m2"><p>دانهٔ ما دانهٔ بی‌دانگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان فراخ آمد چنین روزی ما</p></div>
<div class="m2"><p>که دریدن شد قبادوزی ما</p></div></div>