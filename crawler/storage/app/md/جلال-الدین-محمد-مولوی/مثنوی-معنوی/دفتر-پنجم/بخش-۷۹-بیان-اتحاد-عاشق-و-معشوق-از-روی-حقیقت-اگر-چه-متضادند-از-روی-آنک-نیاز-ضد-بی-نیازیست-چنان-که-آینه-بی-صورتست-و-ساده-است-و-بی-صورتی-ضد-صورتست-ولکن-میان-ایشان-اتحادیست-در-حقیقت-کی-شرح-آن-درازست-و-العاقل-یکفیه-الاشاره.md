---
title: >-
    بخش ۷۹ - بیان اتحاد عاشق و معشوق از روی حقیقت اگر چه متضادند  از روی آنک نیاز ضد بی‌نیازیست چنان که آینه بی‌صورتست و  ساده است و بی‌صورتی ضد صورتست ولکن میان ایشان اتحادیست  در حقیقت کی شرح آن درازست و العاقل یکفیه الاشاره
---
# بخش ۷۹ - بیان اتحاد عاشق و معشوق از روی حقیقت اگر چه متضادند  از روی آنک نیاز ضد بی‌نیازیست چنان که آینه بی‌صورتست و  ساده است و بی‌صورتی ضد صورتست ولکن میان ایشان اتحادیست  در حقیقت کی شرح آن درازست و العاقل یکفیه الاشاره

<div class="b" id="bn1"><div class="m1"><p>جسم مجنون را ز رنج و دوریی</p></div>
<div class="m2"><p>اندر آمد ناگهان رنجوریی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون بجوش آمد ز شعلهٔ اشتیاق</p></div>
<div class="m2"><p>تا پدید آمد بر آن مجنون خناق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس طبیب آمد بدارو کردنش</p></div>
<div class="m2"><p>گفت چاره نیست هیچ از رگ‌زنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رگ زدن باید برای دفع خون</p></div>
<div class="m2"><p>رگ‌زنی آمد بدانجا ذو فنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازوش بست و گرفت آن نیش او</p></div>
<div class="m2"><p>بانک بر زد در زمان آن عشق‌خو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مزد خود بستان و ترک فصد کن</p></div>
<div class="m2"><p>گر بمیرم گو برو جسم کهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آخر از چه می‌ترسی ازین</p></div>
<div class="m2"><p>چون نمی‌ترسی تو از شیر عرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیر و گرگ و خرس و هر گور و دده</p></div>
<div class="m2"><p>گرد بر گرد تو شب گرد آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می نه آیدشان ز تو بوی بشر</p></div>
<div class="m2"><p>ز انبهی عشق و وجد اندر جگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرگ و خرس و شیر داند عشق چیست</p></div>
<div class="m2"><p>کم ز سگ باشد که از عشق او عمیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر رگ عشقی نبودی کلب را</p></div>
<div class="m2"><p>کی بجستی کلب کهفی قلب را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم ز جنس او به صورت چون سگان</p></div>
<div class="m2"><p>گر نشد مشهور هست اندر جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بو نبردی تو دل اندر جنس خویش</p></div>
<div class="m2"><p>کی بری تو بوی دل از گرگ و میش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نبودی عشق هستی کی بدی</p></div>
<div class="m2"><p>کی زدی نان بر تو و کی تو شدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نان تو شد از چه ز عشق و اشتها</p></div>
<div class="m2"><p>ورنه نان را کی بدی تا جان رهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عشق نان مرده را می جان کند</p></div>
<div class="m2"><p>جان که فانی بود جاویدان کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت مجنون من نمی‌ترسم ز نیش</p></div>
<div class="m2"><p>صبر من از کوه سنگین هست بیش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منبلم بی‌زخم ناساید تنم</p></div>
<div class="m2"><p>عاشقم بر زخمها بر می‌تنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لیک از لیلی وجود من پرست</p></div>
<div class="m2"><p>این صدف پر از صفات آن درست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترسم ای فصاد گر فصدم کنی</p></div>
<div class="m2"><p>نیش را ناگاه بر لیلی زنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>داند آن عقلی که او دل‌روشنیست</p></div>
<div class="m2"><p>در میان لیلی و من فرق نیست</p></div></div>