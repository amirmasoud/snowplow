---
title: >-
    بخش ۱۲۰ - سبب دانستن ضمیرهای خلق
---
# بخش ۱۲۰ - سبب دانستن ضمیرهای خلق

<div class="b" id="bn1"><div class="m1"><p>چون دل آن آب زینها خالیست</p></div>
<div class="m2"><p>عکس روها از برون در آب جست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس ترا باطن مصفا ناشده</p></div>
<div class="m2"><p>خانه پر از دیو و نسناس و دده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خری ز استیزه ماند در خری</p></div>
<div class="m2"><p>کی ز ارواح مسیحی بو بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی شناسی گر خیالی سر کند</p></div>
<div class="m2"><p>کز کدامین مکمنی سر بر کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون خیالی می‌شود در زهد تن</p></div>
<div class="m2"><p>تا خیالات از درونه روفتن</p></div></div>