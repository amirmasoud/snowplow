---
title: >-
    بخش ۲۳ - حکایت آن اعرابی کی سگ او از گرسنگی می‌مرد و انبان او پر نان و بر سگ نوحه می‌کرد و شعر می‌گفت و می‌گریست و سر و رو می‌زد و دریغش می‌آمد لقمه‌ای از انبان به سگ دادن
---
# بخش ۲۳ - حکایت آن اعرابی کی سگ او از گرسنگی می‌مرد و انبان او پر نان و بر سگ نوحه می‌کرد و شعر می‌گفت و می‌گریست و سر و رو می‌زد و دریغش می‌آمد لقمه‌ای از انبان به سگ دادن

<div class="b" id="bn1"><div class="m1"><p>آن سگی می‌مرد و گریان آن عرب</p></div>
<div class="m2"><p>اشک می‌بارید و می‌گفت ای کرب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایلی بگذشت و گفت این گریه چیست</p></div>
<div class="m2"><p>نوحه و زاری تو از بهر کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت در ملکم سگی بد نیک‌خو</p></div>
<div class="m2"><p>نک همی‌میرد میان راه او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز صیادم بد و شب پاسبان</p></div>
<div class="m2"><p>تیزچشم و صیدگیر و دزدران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت رنجش چیست زخمی خورده است</p></div>
<div class="m2"><p>گفت جوع الکلب زارش کرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت صبری کن برین رنج و حرض</p></div>
<div class="m2"><p>صابران را فضل حق بخشد عوض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد از آن گفتش کای سالار حر</p></div>
<div class="m2"><p>چیست اندر دستت این انبان پر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت نان و زاد و لوت دوش من</p></div>
<div class="m2"><p>می‌کشانم بهر تقویت بدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت چون ندهی بدان سگ نان و زاد</p></div>
<div class="m2"><p>گفت تا این حد ندارم مهر و داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست ناید بی‌درم در راه نان</p></div>
<div class="m2"><p>لیک هست آب دو دیده رایگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت خاکت بر سر ای پر باد مشک</p></div>
<div class="m2"><p>که لب نان پیش تو بهتر ز اشک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اشک خونست و به غم آبی شده</p></div>
<div class="m2"><p>می‌نیرزد خاک خون بیهده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کل خود را خوار کرد او چون بلیس</p></div>
<div class="m2"><p>پارهٔ این کل نباشد جز خسیس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من غلام آنک نفروشد وجود</p></div>
<div class="m2"><p>جز بدان سلطان با افضال و جود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون بگرید آسمان گریان شود</p></div>
<div class="m2"><p>چون بنالد چرخ یا رب خوان شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من غلام آن مس همت‌پرست</p></div>
<div class="m2"><p>کو به غیر کیمیا نارد شکست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست اشکسته برآور در دعا</p></div>
<div class="m2"><p>سوی اشکسته پرد فضل خدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر رهایی بایدت زین چاه تنگ</p></div>
<div class="m2"><p>ای برادر رو بر آذر بی‌درنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مکر حق را بین و مکر خود بهل</p></div>
<div class="m2"><p>ای ز مکرش مکر مکاران خجل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونک مکرت شد فنای مکر رب</p></div>
<div class="m2"><p>برگشایی یک کمینی بوالعجب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که کمینهٔ آن کمین باشد بقا</p></div>
<div class="m2"><p>تا ابد اندر عروج و ارتقا</p></div></div>