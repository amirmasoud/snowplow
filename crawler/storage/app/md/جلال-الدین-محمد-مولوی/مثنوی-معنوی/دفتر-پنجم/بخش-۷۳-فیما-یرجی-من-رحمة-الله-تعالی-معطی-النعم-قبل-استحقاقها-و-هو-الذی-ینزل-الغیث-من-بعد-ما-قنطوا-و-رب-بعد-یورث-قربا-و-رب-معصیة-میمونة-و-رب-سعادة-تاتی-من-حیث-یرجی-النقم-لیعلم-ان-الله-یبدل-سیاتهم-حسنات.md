---
title: >-
    بخش ۷۳ - فیما یرجی من رحمة الله تعالی معطی النعم قبل استحقاقها و  هو الذی ینزل الغیث من بعد ما قنطوا و رب بعد یورث قربا و رب  معصیة میمونة و رب سعادة تاتی من حیث یرجی النقم لیعلم ان الله یبدل سیاتهم حسنات
---
# بخش ۷۳ - فیما یرجی من رحمة الله تعالی معطی النعم قبل استحقاقها و  هو الذی ینزل الغیث من بعد ما قنطوا و رب بعد یورث قربا و رب  معصیة میمونة و رب سعادة تاتی من حیث یرجی النقم لیعلم ان الله یبدل سیاتهم حسنات

<div class="b" id="bn1"><div class="m1"><p>در حدیث آمد که روز رستخیز</p></div>
<div class="m2"><p>امر آید هر یکی تن را که خیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفخ صور امرست از یزدان پاک</p></div>
<div class="m2"><p>که بر آرید ای ذرایر سر ز خاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز آید جان هر یک در بدن</p></div>
<div class="m2"><p>هم‌چو وقت صبح هوش آید به تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان تن خود را شناسد وقت روز</p></div>
<div class="m2"><p>در خراب خود در آید چون کنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جسم خود بشناسد و در وی رود</p></div>
<div class="m2"><p>جان زرگر سوی درزی کی رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان عالم سوی عالم می‌دود</p></div>
<div class="m2"><p>روح ظالم سوی ظالم می‌دود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که شناسا کردشان علم اله</p></div>
<div class="m2"><p>چونک بره و میش وقت صبحگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای کفش خود شناسد در ظلم</p></div>
<div class="m2"><p>چون نداند جان تن خود ای صنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح حشر کوچکست ای مستجیر</p></div>
<div class="m2"><p>حشر اکبر را قیاس از وی بگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچنان که جان بپرد سوی طین</p></div>
<div class="m2"><p>نامه پرد تا یسار و تا یمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کفش بنهند نامهٔ بخل و جود</p></div>
<div class="m2"><p>فسق و تقوی آنچ دی خو کرده بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون شود بیدار از خواب او سحر</p></div>
<div class="m2"><p>باز آید سوی او آن خیر و شر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر ریاضت داده باشد خوی خویش</p></div>
<div class="m2"><p>وقت بیداری همان آید به پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور بد او دی خام و زشت و در ضلال</p></div>
<div class="m2"><p>چون عزا نامه سیه یابد شمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور بد او دی پاک و با تقوی و دین</p></div>
<div class="m2"><p>وقت بیداری برد در ثمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست ما را خواب و بیداری ما</p></div>
<div class="m2"><p>بر نشان مرگ و محشر دو گوا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حشر اصغر حشر اکبر را نمود</p></div>
<div class="m2"><p>مرگ اصغر مرگ اکبر را زدود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لیک این نامه خیالست و نهان</p></div>
<div class="m2"><p>وآن شود در حشر اکبر بس عیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این خیال اینجا نهان پیدا اثر</p></div>
<div class="m2"><p>زین خیال آنجا برویاند صور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در مهندس بین خیال خانه‌ای</p></div>
<div class="m2"><p>در دلش چون در زمینی دانه‌ای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن خیال از اندرون آید برون</p></div>
<div class="m2"><p>چون زمین که زاید از تخم درون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر خیالی کو کند در دل وطن</p></div>
<div class="m2"><p>روز محشر صورتی خواهد شدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون خیال آن مهندس در ضمیر</p></div>
<div class="m2"><p>چون نبات اندر زمین دانه‌گیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مخلصم زین هر دو محشر قصه‌ایست</p></div>
<div class="m2"><p>مؤمنان را در بیانش حصه‌ایست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بر آید آفتاب رستخیز</p></div>
<div class="m2"><p>بر جهند از خاک زشت و خوب تیز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سوی دیوان قضا پویان شوند</p></div>
<div class="m2"><p>نقد نیک و بد به کوره می‌روند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نقد نیکو شادمان و ناز ناز</p></div>
<div class="m2"><p>نقد قلب اندر زحیر و در گداز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لحظه لحظه امتحانها می‌رسد</p></div>
<div class="m2"><p>سر دلها می‌نماید در جسد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون ز قندیل آب و روغن گشته فاش</p></div>
<div class="m2"><p>یا چو خاکی که بروید سرهاش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از پیاز و گندنا و کوکنار</p></div>
<div class="m2"><p>سر دی پیدا کند دست بهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن یکی سرسبز نحن المتقون</p></div>
<div class="m2"><p>وآن دگر هم‌چون بنفشه سرنگون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چشمها بیرون جهید از خطر</p></div>
<div class="m2"><p>گشته ده چشمه ز بیم مستقر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باز مانده دیده‌ها در انتظار</p></div>
<div class="m2"><p>تا که نامه ناید از سوی یسار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چشم گردان سوی راست و سوی چپ</p></div>
<div class="m2"><p>زانک نبود بخت نامهٔ راست زپ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نامه‌ای آید به دست بنده‌ای</p></div>
<div class="m2"><p>سر سیه از جرم و فسق آگنده‌ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اندرو یک خیر و یک توفیق نه</p></div>
<div class="m2"><p>جز که آزار دل صدیق نه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پر ز سر تا پای زشتی و گناه</p></div>
<div class="m2"><p>تسخر و خنبک زدن بر اهل راه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آن دغل‌کاری و دزدیهای او</p></div>
<div class="m2"><p>و آن چو فرعونان انا و انای او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون بخواند نامهٔ خود آن ثقیل</p></div>
<div class="m2"><p>داند او که سوی زندان شد رحیل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس روان گردد چو دزدان سوی دار</p></div>
<div class="m2"><p>جرم پیدا بسته راه اعتذار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن هزاران حجت و گفتار بد</p></div>
<div class="m2"><p>بر دهانش گشته چون مسمار بد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رخت دزدی بر تن و در خانه‌اش</p></div>
<div class="m2"><p>گشته پیدا گم شده افسانه‌اش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پس روان گردد به زندان سعیر</p></div>
<div class="m2"><p>که نباشد خار را ز آتش گزیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون موکل آن ملایک پیش و پس</p></div>
<div class="m2"><p>بوده پنهان گشته پیدا چون عسس</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>می‌برندش می‌سپوزندش به نیش</p></div>
<div class="m2"><p>که برو ای سگ به کهدانهای خویش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>می‌کشد پا بر سر هر راه او</p></div>
<div class="m2"><p>تا بود که بر جهد زان چاه او</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>منتظر می‌ایستد تن می‌زند</p></div>
<div class="m2"><p>در امیدی روی وا پس می‌کند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اشک می‌بارد چو باران خزان</p></div>
<div class="m2"><p>خشک اومیدی چه دارد او جز آن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هر زمانی روی وا پس می‌کند</p></div>
<div class="m2"><p>رو به درگاه مقدس می‌کند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پس ز حق امر آید از اقلیم نور</p></div>
<div class="m2"><p>که بگوییدش کای بطال عور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>انتظار چیستی ای کان شر</p></div>
<div class="m2"><p>رو چه وا پس می‌کنی ای خیره‌سر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نامه‌ات آنست کت آمد به دست</p></div>
<div class="m2"><p>ای خدا آزار و ای شیطان‌پرست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون بدیدی نامهٔ کردار خویش</p></div>
<div class="m2"><p>چه نگری پس بین جزای کار خویش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بیهده چه مول مولی می‌زنی</p></div>
<div class="m2"><p>در چنین چه کو امید روشنی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه ترا از روی ظاهر طاعتی</p></div>
<div class="m2"><p>نه ترا در سر و باطن نیتی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نه ترا شبها مناجات و قیام</p></div>
<div class="m2"><p>نه ترا در روز پرهیز و صیام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نه ترا حفظ زبان ز آزار کس</p></div>
<div class="m2"><p>نه نظر کردن به عبرت پیش و پس</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پیش چه بود یاد مرگ و نزع خویش</p></div>
<div class="m2"><p>پس چه باشد مردن یاران ز پیش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نه ترا بر ظلم توبهٔ پر خروش</p></div>
<div class="m2"><p>ای دغا گندم‌نمای جوفروش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون ترازوی تو کژ بود و دغا</p></div>
<div class="m2"><p>راست چون جویی ترازوی جزا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چونک پای چپ بدی در غدر و کاست</p></div>
<div class="m2"><p>نامه چون آید ترا در دست راست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون جزا سایه‌ست ای قد تو خم</p></div>
<div class="m2"><p>سایهٔ تو کژ فتد در پیش هم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زین قبل آید خطابات درشت</p></div>
<div class="m2"><p>که شود که را از آن هم کوز پشت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بنده گوید آنچ فرمودی بیان</p></div>
<div class="m2"><p>صد چنانم صد چنانم صد چنان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خود تو پوشیدی بترها را به حلم</p></div>
<div class="m2"><p>ورنه می‌دانی فضیحتها به علم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>لیک بیرون از جهاد و فعل خویش</p></div>
<div class="m2"><p>از ورای خیر و شر و کفر و کیش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وز نیاز عاجزانهٔ خویشتن</p></div>
<div class="m2"><p>وز خیال و وهم من یا صد چو من</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بودم اومیدی به محض لطف تو</p></div>
<div class="m2"><p>از ورای راست باشی یا عتو</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بخشش محضی ز لطف بی‌عوض</p></div>
<div class="m2"><p>بودم اومید ای کریم بی‌عوض</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>رو سپس کردم بدان محض کرم</p></div>
<div class="m2"><p>سوی فعل خویشتن می‌ننگرم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سوی آن اومید کردم روی خویش</p></div>
<div class="m2"><p>که وجودم داده‌ای از پیش بیش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خلعت هستی بدادی رایگان</p></div>
<div class="m2"><p>من همیشه معتمد بودم بر آن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چون شمارد جرم خود را و خطا</p></div>
<div class="m2"><p>محض بخشایش در آید در عطا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>کای ملایک باز آریدش به ما</p></div>
<div class="m2"><p>که بدستش چشم دل سوی رجا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>لاابالی وار آزادش کنیم</p></div>
<div class="m2"><p>وآن خطاها را همه خط بر زنیم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>لا ابالی مر کسی را شد مباح</p></div>
<div class="m2"><p>کش زیان نبود ز غدر و از صلاح</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>آتشی خوش بر فروزیم از کرم</p></div>
<div class="m2"><p>تا نماند جرم و زلت بیش و کم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>آتشی کز شعله‌اش کمتر شرار</p></div>
<div class="m2"><p>می‌بسوزد جرم و جبر و اختیار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>شعله در بنگاه انسانی زنیم</p></div>
<div class="m2"><p>خار را گلزار روحانی کنیم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ما فرستادیم از چرخ نهم</p></div>
<div class="m2"><p>کیمیا یصلح لکم اعمالکم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خود چه باشد پیش نور مستقر</p></div>
<div class="m2"><p>کر و فر اختیار بوالبشر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گوشت‌پاره آلت گویای او</p></div>
<div class="m2"><p>پیه‌پاره منظر بینای او</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مسمع او آن دو پاره استخوان</p></div>
<div class="m2"><p>مدرکش دو قطره خون یعنی جنان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>کرمکی و از قذر آکنده‌ای</p></div>
<div class="m2"><p>طمطراقی در جهان افکنده‌ای</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>از منی بودی منی را واگذار</p></div>
<div class="m2"><p>ای ایاز آن پوستین را یاد دار</p></div></div>