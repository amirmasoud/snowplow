---
title: >-
    بخش ۱۱۴ - آمدن شیخ بعد از چندین سال از بیابان به شهر غزنین و  زنبیل گردانیدن به اشارت غیبی و تفرقه کردن آنچ جمع آید بر فقرا  هر که را جان عز لبیکست  نامه بر نامه پیک بر پیکست  چنانک روزن خانه باز باشد آفتاب و ماهتاب و باران و نامه و غیره  منقطع نباشد
---
# بخش ۱۱۴ - آمدن شیخ بعد از چندین سال از بیابان به شهر غزنین و  زنبیل گردانیدن به اشارت غیبی و تفرقه کردن آنچ جمع آید بر فقرا  هر که را جان عز لبیکست  نامه بر نامه پیک بر پیکست  چنانک روزن خانه باز باشد آفتاب و ماهتاب و باران و نامه و غیره  منقطع نباشد

<div class="b" id="bn1"><div class="m1"><p>رو به شهر آورد آن فرمان‌پذیر</p></div>
<div class="m2"><p>شهر غزنین گشت از رویش منیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فرح خلقی به استقبال رفت</p></div>
<div class="m2"><p>او در آمد از ره دزدیده تفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله اعیان و مهان بر خاستند</p></div>
<div class="m2"><p>قصرها از بهر او آراستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت من از خودنمایی نامدم</p></div>
<div class="m2"><p>جز به خواری و گدایی نامدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستم در عزم قال و قیل من</p></div>
<div class="m2"><p>در به در گردم به کف زنبیل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده فرمانم که امرست از خدا</p></div>
<div class="m2"><p>که گدا باشم گدا باشم گدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گدایی لفظ نادر ناورم</p></div>
<div class="m2"><p>جز طریق خس گدایان نسپرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا شوم غرقهٔ مذلت من تمام</p></div>
<div class="m2"><p>تا سقطها بشنوم از خاص و عام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امر حق جانست و من آن را تبع</p></div>
<div class="m2"><p>او طمع فرمود ذل من طمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون طمع خواهد ز من سلطان دین</p></div>
<div class="m2"><p>خاک بر فرق قناعت بعد ازین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او مذلت خواست کی عزت تنم</p></div>
<div class="m2"><p>او گدایی خواست کی میری کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بعد ازین کد و مذلت جان من</p></div>
<div class="m2"><p>بیست عباس‌اند در انبان من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شیخ بر می‌گشت زنبیلی به دست</p></div>
<div class="m2"><p>شیء لله خواجه توفیقیت هست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برتر از کرسی و عرش اسرار او</p></div>
<div class="m2"><p>شیء لله شیء لله کار او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>انبیا هر یک همین فن می‌زنند</p></div>
<div class="m2"><p>خلق مفلس کدیه ایشان می‌کنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اقرضوا الله اقرضوا الله می‌زنند</p></div>
<div class="m2"><p>بازگون بر انصروا الله می‌تنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در به در این شیخ می‌آرد نیاز</p></div>
<div class="m2"><p>بر فلک صد در برای شیخ باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که آن گدایی که آن به جد می‌کرد او</p></div>
<div class="m2"><p>بهر یزدان بود نه از بهر گلو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور بکردی نیز از بهر گلو</p></div>
<div class="m2"><p>آن گلو از نور حق دارد غلو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در حق او خورد نان و شهد و شیر</p></div>
<div class="m2"><p>به ز چله وز سه روزهٔ صد فقیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نور می‌نوشد مگو نان می‌خورد</p></div>
<div class="m2"><p>لاله می‌کارد به صورت می‌چرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون شراری کو خورد روغن ز شمع</p></div>
<div class="m2"><p>نور افزاید ز خوردش بهر جمع</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نان‌خوری را گفت حق لاتسرفوا</p></div>
<div class="m2"><p>نور خوردن را نگفتست اکتفوا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن گلوی ابتلا بد وین گلو</p></div>
<div class="m2"><p>فارغ از اسراف و آمن از غلو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>امر و فرمان بود نه حرص و طمع</p></div>
<div class="m2"><p>آن چنان جان حرص را نبود تبع</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر بگوید کیمیا مس را بده</p></div>
<div class="m2"><p>تو به من خود را طمع نبود فره</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گنجهای خاک تا هفتم طبق</p></div>
<div class="m2"><p>عرضه کرده بود پیش شیخ حق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شیخ گفتا خالقا من عاشقم</p></div>
<div class="m2"><p>گر بجویم غیر تو من فاسقم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هشت جنت گر در آرم در نظر</p></div>
<div class="m2"><p>ور کنم خدمت من از خوف سقر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ممنی باشم سلامت‌جوی من</p></div>
<div class="m2"><p>زانک این هر دو بود حظ بدن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عاشقی کز عشق یزدان خورد قوت</p></div>
<div class="m2"><p>صد بدن پیشش نیرزد تره‌توت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وین بدن که دارد آن شیخ فطن</p></div>
<div class="m2"><p>چیز دگر گشت کم خوانش بدن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عاشق عشق خدا وانگاه مزد</p></div>
<div class="m2"><p>جبرئیل مؤتمن وانگاه دزد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عاشق آن لیلی کور و کبود</p></div>
<div class="m2"><p>ملک عالم پیش او یک تره بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیش او یکسان شده بد خاک و زر</p></div>
<div class="m2"><p>زر چه باشد که نبد جان را خطر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شیر و گرگ و دد ازو واقف شده</p></div>
<div class="m2"><p>هم‌چو خویشان گرد او گرد آمده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کین شدست از خوی حیوان پاک پاک</p></div>
<div class="m2"><p>پر ز عشق و لحم و شحمش زهرناک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زهر دد باشد شکرریز خرد</p></div>
<div class="m2"><p>زانک نیک نیک باشد ضد بد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>لحم عاشق را نیارد خورد دد</p></div>
<div class="m2"><p>عشق معروفست پیش نیک و بد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ور خورد خود فی‌المثل دام و ددش</p></div>
<div class="m2"><p>گوشت عاشق زهر گردد بکشدش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر چه جز عشقست شد ماکول عشق</p></div>
<div class="m2"><p>دو جهان یک دانه پیش نول عشق</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دانه‌ای مر مرغ را هرگز خورد</p></div>
<div class="m2"><p>کاهدان مر اسپ را هرگز چرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بندگی کن تا شوی عاشق لعل</p></div>
<div class="m2"><p>بندگی کسبیست آید در عمل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بنده آزادی طمع دارد ز جد</p></div>
<div class="m2"><p>عاشق آزادی نخواهد تا ابد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بنده دایم خلعت و ادرارجوست</p></div>
<div class="m2"><p>خلعت عاشق همه دیدار دوست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در نگنجد عشق در گفت و شنید</p></div>
<div class="m2"><p>عشق دریاییست قعرش ناپدید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قطره‌های بحر را نتوان شمرد</p></div>
<div class="m2"><p>هفت دریا پیش آن بحرست خرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این سخن پایان ندارد ای فلان</p></div>
<div class="m2"><p>باز رو در قصهٔ شیخ زمان</p></div></div>