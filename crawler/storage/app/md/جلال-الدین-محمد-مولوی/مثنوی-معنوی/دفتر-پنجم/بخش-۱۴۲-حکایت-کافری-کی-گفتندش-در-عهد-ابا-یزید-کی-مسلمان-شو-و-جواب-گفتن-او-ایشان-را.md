---
title: >-
    بخش ۱۴۲ - حکایت کافری کی گفتندش در عهد ابا یزید کی مسلمان شو و جواب گفتن او ایشان را
---
# بخش ۱۴۲ - حکایت کافری کی گفتندش در عهد ابا یزید کی مسلمان شو و جواب گفتن او ایشان را

<div class="b" id="bn1"><div class="m1"><p>بود گبری در زمان بایزید</p></div>
<div class="m2"><p>گفت او را یک مسلمان سعید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چه باشد گر تو اسلام آوری</p></div>
<div class="m2"><p>تا بیابی صد نجات و سروری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت این ایمان اگر هست ای مرید</p></div>
<div class="m2"><p>آنک دارد شیخ عالم بایزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ندارم طاقت آن تاب آن</p></div>
<div class="m2"><p>که آن فزون آمد ز کوششهای جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه در ایمان و دین ناموقنم</p></div>
<div class="m2"><p>لیک در ایمان او بس مؤمنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم ایمان که آن ز جمله برترست</p></div>
<div class="m2"><p>بس لطیف و با فروغ و با فرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مؤمن ایمان اویم در نهان</p></div>
<div class="m2"><p>گرچه مهرم هست محکم بر دهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز ایمان خود گر ایمان شماست</p></div>
<div class="m2"><p>نه بدان میلستم و نه مشتهاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنک صد میلش سوی ایمان بود</p></div>
<div class="m2"><p>چون شما را دید آن فاتر شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانک نامی بیند و معنیش نی</p></div>
<div class="m2"><p>چون بیابان را مفازه گفتنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق او ز آورد ایمان بفسرد</p></div>
<div class="m2"><p>چون به ایمان شما او بنگرد</p></div></div>