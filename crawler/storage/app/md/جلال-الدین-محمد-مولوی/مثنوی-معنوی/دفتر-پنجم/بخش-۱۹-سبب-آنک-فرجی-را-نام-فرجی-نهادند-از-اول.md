---
title: >-
    بخش ۱۹ - سبب آنک فرجی را نام فرجی نهادند از اول
---
# بخش ۱۹ - سبب آنک فرجی را نام فرجی نهادند از اول

<div class="b" id="bn1"><div class="m1"><p>صوفیی بدرید جبه در حرج</p></div>
<div class="m2"><p>پیشش آمد بعد به دریدن فرج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد نام آن دریده فرجی</p></div>
<div class="m2"><p>این لقب شد فاش زان مرد نجی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این لقب شد فاش و صافش شیخ برد</p></div>
<div class="m2"><p>ماند اندر طبع خلقان حرف درد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم‌چنین هر نام صافی داشتست</p></div>
<div class="m2"><p>اسم را چون دردیی بگذاشتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که گل خوارست دردی را گرفت</p></div>
<div class="m2"><p>رفت صوفی سوی صافی ناشکفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت لابد درد را صافی بود</p></div>
<div class="m2"><p>زین دلالت دل به صفوت می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد عسر افتاد و صافش یسر او</p></div>
<div class="m2"><p>صاف چون خرما و دردی بسر او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یسر با عسرست هین آیس مباش</p></div>
<div class="m2"><p>راه داری زین ممات اندر معاش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روح خواهی جبه بشکاف ای پسر</p></div>
<div class="m2"><p>تا از آن صفوت برآری زود سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست صوفی آنک شد صفوت‌طلب</p></div>
<div class="m2"><p>نه از لباس صوف و خیاطی و دب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صوفیی گشته به پیش این لئام</p></div>
<div class="m2"><p>الخیاطه واللواطه والسلام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر خیال آن صفا و نام نیک</p></div>
<div class="m2"><p>رنگ پوشیدن نکو باشد ولیک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر خیالش گر روی تا اصل او</p></div>
<div class="m2"><p>نی چو عباد خیال تو به تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دور باش غیرتت آمد خیال</p></div>
<div class="m2"><p>گرد بر گرد سراپردهٔ جمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسته هر جوینده را که راه نیست</p></div>
<div class="m2"><p>هر خیالش پیش می‌آید بیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جز مگر آن تیزکوش تیزهوش</p></div>
<div class="m2"><p>کش بود از جیش نصرتهاش جوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نجهد از تخییلها نی شه شود</p></div>
<div class="m2"><p>تیر شه بنماید آنگه ره شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این دل سرگشته را تدبیر بخش</p></div>
<div class="m2"><p>وین کمانهای دوتو را تیر بخش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جرعه‌ای بر ریختی زان خفیه جام</p></div>
<div class="m2"><p>بر زمین خاک من کاس الکرام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست بر زلف و رخ از جرعه‌ش نشان</p></div>
<div class="m2"><p>خاک را شاهان همی‌لیسند از آن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جرعه حسنست اندر خاک گش</p></div>
<div class="m2"><p>که به صد دل روز و شب می‌بوسیش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جرعه خاک آمیز چون مجنون کند</p></div>
<div class="m2"><p>مر ترا تا صاف او خود چون کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر کسی پیش کلوخی جامه‌چاک</p></div>
<div class="m2"><p>که آن کلوخ از حسن آمد جرعه‌ناک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جرعه‌ای بر ماه و خورشید و حمل</p></div>
<div class="m2"><p>جرعه‌ای بر عرش و کرسی و زحل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جرعه گوییش ای عجب یا کیمیا</p></div>
<div class="m2"><p>که ز اسیبش بود چندین بها</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جد طلب آسیب او ای ذوفنون</p></div>
<div class="m2"><p>لا یمس ذاک الا المطهرون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جرعه‌ای بر زر و بر لعل و درر</p></div>
<div class="m2"><p>جرعه‌ای بر خمر و بر نقل و ثمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جرعه‌ای بر روی خوبان لطاف</p></div>
<div class="m2"><p>تا چگونه باشد آن راواق صاف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون همی مالی زبان را اندرین</p></div>
<div class="m2"><p>چون شوی چون بینی آن را بی ز طین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چونک وقت مرگ آن جرعهٔ صفا</p></div>
<div class="m2"><p>زین کلوخ تن به مردن شد جدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنچ می‌ماند کنی دفنش تو زود</p></div>
<div class="m2"><p>این چنین زشتی بدان چون گشته بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جان چو بی این جیفه بنماید جمال</p></div>
<div class="m2"><p>من نتانم گفت لطف آن وصال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مه چو بی‌این ابر بنماید ضیا</p></div>
<div class="m2"><p>شرح نتوان کرد زان کار و کیا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حبذا آن مطبخ پر نوش و قند</p></div>
<div class="m2"><p>کین سلاطین کاسه‌لیسان ویند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حبذا آن خرمن صحرای دین</p></div>
<div class="m2"><p>که بود هر خرمن آن را دانه‌چین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حبذا دریای عمر بی‌غمی</p></div>
<div class="m2"><p>که بود زو هفت دریا شب‌نمی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جرعه‌ای چون ریخت ساقی الست</p></div>
<div class="m2"><p>بر سر این شوره خاک زیردست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جوش کرد آن خاک و ما زان جوششیم</p></div>
<div class="m2"><p>جرعهٔ دیگر که بس بی‌کوششیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر روا بد ناله کردم از عدم</p></div>
<div class="m2"><p>ور نبود این گفتنی نک تن زدم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این بیان بط حرص منثنیست</p></div>
<div class="m2"><p>از خلیل آموز که آن بط کشتنیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هست در بط غیر این بس خیر و شر</p></div>
<div class="m2"><p>ترسم از فوت سخنهای دگر</p></div></div>