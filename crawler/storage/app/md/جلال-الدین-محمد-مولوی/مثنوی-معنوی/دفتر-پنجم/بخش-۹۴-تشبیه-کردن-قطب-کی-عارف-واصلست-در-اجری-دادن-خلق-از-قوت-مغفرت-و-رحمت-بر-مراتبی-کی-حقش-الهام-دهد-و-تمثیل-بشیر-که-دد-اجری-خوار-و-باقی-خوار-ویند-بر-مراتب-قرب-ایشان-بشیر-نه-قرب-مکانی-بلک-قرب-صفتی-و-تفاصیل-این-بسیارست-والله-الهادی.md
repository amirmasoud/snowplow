---
title: >-
    بخش ۹۴ - تشبیه کردن قطب کی عارف واصلست در اجری دادن خلق از قوت مغفرت و رحمت بر مراتبی کی حقش الهام دهد و تمثیل بشیر که دد اجری خوار و باقی خوار ویند بر مراتب قرب ایشان بشیر نه قرب مکانی بلک قرب صفتی و تفاصیل این بسیارست  والله الهادی
---
# بخش ۹۴ - تشبیه کردن قطب کی عارف واصلست در اجری دادن خلق از قوت مغفرت و رحمت بر مراتبی کی حقش الهام دهد و تمثیل بشیر که دد اجری خوار و باقی خوار ویند بر مراتب قرب ایشان بشیر نه قرب مکانی بلک قرب صفتی و تفاصیل این بسیارست  والله الهادی

<div class="b" id="bn1"><div class="m1"><p>قطب شیر و صید کردن کار او</p></div>
<div class="m2"><p>باقیان این خلق باقی‌خوار او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا توانی در رضای قطب کوش</p></div>
<div class="m2"><p>تا قوی گردد کند صید وحوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو برنجد بی‌نوا مانند خلق</p></div>
<div class="m2"><p>کز کف عقلست جمله رزق حلق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زانک وجد حلق باقی خورد اوست</p></div>
<div class="m2"><p>این نگه دار ار دل تو صیدجوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او چو عقل و خلق چون اعضا و تن</p></div>
<div class="m2"><p>بستهٔ عقلست تدبیر بدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضعف قطب از تن بود از روح نی</p></div>
<div class="m2"><p>ضعف در کشتی بود در نوح نی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطب آن باشد که گرد خود تند</p></div>
<div class="m2"><p>گردش افلاک گرد او بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاریی ده در مرمهٔ کشتی‌اش</p></div>
<div class="m2"><p>گر غلام خاص و بنده گشتی‌اش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاریت در تو فزاید نه اندرو</p></div>
<div class="m2"><p>گفت حق ان تنصروا الله تنصروا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم‌چو روبه صید گیر و کن فداش</p></div>
<div class="m2"><p>تا عوض گیری هزاران صید بیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روبهانه باشد آن صید مرید</p></div>
<div class="m2"><p>مرده گیرد صید کفتار مرید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرده پیش او کشی زنده شود</p></div>
<div class="m2"><p>چرک در پالیز روینده شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت روبه شیر را خدمت کنم</p></div>
<div class="m2"><p>حیله‌ها سازم ز عقلش بر کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حیله و افسونگری کار منست</p></div>
<div class="m2"><p>کار من دستان و از ره بردنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از سر که جانب جو می‌شتافت</p></div>
<div class="m2"><p>آن خر مسکین لاغر را بیافت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس سلام گرم کرد و پیش رفت</p></div>
<div class="m2"><p>پیش آن ساده دل درویش رفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت چونی اندرین صحرای خشک</p></div>
<div class="m2"><p>در میان سنگ لاخ و جای خشک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت خر گر در غمم گر در ارم</p></div>
<div class="m2"><p>قسمتم حق کرد من زان شاکرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شکر گویم دوست را در خیر و شر</p></div>
<div class="m2"><p>زانک هست اندر قضا از بد بتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونک قسام اوست کفر آمد گله</p></div>
<div class="m2"><p>صبر باید صبر مفتاح الصله</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غیر حق جمله عدواند اوست دوست</p></div>
<div class="m2"><p>با عدو از دوست شکوت کی نکوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا دهد دوغم نخواهم انگبین</p></div>
<div class="m2"><p>زانک هر نعمت غمی دارد قرین</p></div></div>