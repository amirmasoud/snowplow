---
title: >-
    بخش ۱۳ - بیان آنک نور که غذای جانست غذای جسم اولیا می‌شود تا او هم یار می‌شود روح را کی اسلم شیطانی علی یدی
---
# بخش ۱۳ - بیان آنک نور که غذای جانست غذای جسم اولیا می‌شود تا او هم یار می‌شود روح را کی اسلم شیطانی علی یدی

<div class="b" id="bn1"><div class="m1"><p>گرچه آن مطعوم جانست و نظر</p></div>
<div class="m2"><p>جسم را هم زان نصیبست ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نگشتی دیو جسم آن را اکول</p></div>
<div class="m2"><p>اسلم الشیطان نفرمودی رسول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیو زان لوتی که مرده حی شود</p></div>
<div class="m2"><p>تا نیاشامد مسلمان کی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیو بر دنیاست عاشق کور و کر</p></div>
<div class="m2"><p>عشق را عشقی دگر برد مگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نهان‌خانهٔ یقین چون می‌چشد</p></div>
<div class="m2"><p>اندک‌اندک رخت عشق آنجا کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا حریص االبطن عرج هکذا</p></div>
<div class="m2"><p>انما المنهاج تبدیل الغذا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا مریض القلب عرج للعلاج</p></div>
<div class="m2"><p>جملة التدبیر تبدیل المزاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایها المحبوس فی رهن الطعام</p></div>
<div class="m2"><p>سوف تنجو ان تحملت الفطام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ان فی‌الجوع طعام وافر</p></div>
<div class="m2"><p>افتقدها وارتج یا نافر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اغتذ بالنور کن مثل البصر</p></div>
<div class="m2"><p>وافق الاملاک یا خیر البشر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ملک تسبیح حق را کن غذا</p></div>
<div class="m2"><p>تا رهی هم‌چون ملایک از اذا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جبرئیل ار سوی جیفه کم تند</p></div>
<div class="m2"><p>او به قوت کی ز کرکس کم زند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حبذا خوانی نهاده در جهان</p></div>
<div class="m2"><p>لیک از چشم خسیسان بس نهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر جهان باغی پر از نعمت شود</p></div>
<div class="m2"><p>قسم موش و مار هم خاکی بود</p></div></div>