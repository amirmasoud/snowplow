---
title: >-
    بخش ۱۱۱ - جواب گفتن خر روباه را
---
# بخش ۱۱۱ - جواب گفتن خر روباه را

<div class="b" id="bn1"><div class="m1"><p>گفت رو رو هین ز پیشم ای عدو</p></div>
<div class="m2"><p>تا نبینم روی تو ای زشت‌رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن خدایی که ترا بدبخت کرد</p></div>
<div class="m2"><p>روی زشتت را کریه و سخت کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با کدامین روی می‌آیی به من</p></div>
<div class="m2"><p>این چنین سغری ندارد کرگدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفته‌ای در خون جانم آشکار</p></div>
<div class="m2"><p>که ترا من ره‌برم تا مرغزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدیدم روی عزرائیل را</p></div>
<div class="m2"><p>باز آوردی فن و تسویل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه من ننگ خرانم یا خرم</p></div>
<div class="m2"><p>جانورم جان دارم این را کی خرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچ من دیدم ز هول بی‌امان</p></div>
<div class="m2"><p>طفل دیدی پیر گشتی در زمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌دل و جان از نهیب آن شکوه</p></div>
<div class="m2"><p>سرنگون خود را در افکندم ز کوه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسته شد پایم در آن دم از نهیب</p></div>
<div class="m2"><p>چون بدیدم آن عذاب بی‌حجاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عهد کردم با خدا کای ذوالمنن</p></div>
<div class="m2"><p>برگشا زین بستگی تو پای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ننوشم وسوسهٔ کس بعد ازین</p></div>
<div class="m2"><p>عهد کردم نذر کردم ای معین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حق گشاده کرد آن دم پای من</p></div>
<div class="m2"><p>زان دعا و زاری و ایمای من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ورنه اندر من رسیدی شیر نر</p></div>
<div class="m2"><p>چون بدی در زیر پنجهٔ شیر خر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز بفرستادت آن شیر عرین</p></div>
<div class="m2"><p>سوی من از مکر ای بئس القرین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حق ذات پاک الله الصمد</p></div>
<div class="m2"><p>که بود به مار بد از یار بد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مار بد جانی ستاند از سلیم</p></div>
<div class="m2"><p>یار بد آرد سوی نار مقیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از قرین بی‌قول و گفت و گوی او</p></div>
<div class="m2"><p>خو بدزدد دل نهان از خوی او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چونک او افکند بر تو سایه را</p></div>
<div class="m2"><p>دزدد آن بی‌مایه از تو مایه را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عقل تو گر اژدهایی گشت مست</p></div>
<div class="m2"><p>یار بد او را زمرد دان که هست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیدهٔ عقلت بدو بیرون جهد</p></div>
<div class="m2"><p>طعن اوت اندر کف طاعون نهد</p></div></div>