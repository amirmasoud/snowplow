---
title: >-
    بخش ۸۹ - در بیان آنک دعای عارف واصل و درخواست او از حق هم‌چو درخواست حقست از خویشتن کی کنت له سمعا و بصرا و لسانا و یدا و قوله و ما رمیت اذ رمیت و لکن الله رمی و آیات و اخبار و آثار درین بسیارست و شرح سبب ساختن حق تا مجرم را گوش گرفته بتوبهٔ نصوح آورد
---
# بخش ۸۹ - در بیان آنک دعای عارف واصل و درخواست او از حق هم‌چو درخواست حقست از خویشتن کی کنت له سمعا و بصرا و لسانا و یدا و قوله و ما رمیت اذ رمیت و لکن الله رمی و آیات و اخبار و آثار درین بسیارست و شرح سبب ساختن حق تا مجرم را گوش گرفته بتوبهٔ نصوح آورد

<div class="b" id="bn1"><div class="m1"><p>آن دعا از هفت گردون در گذشت</p></div>
<div class="m2"><p>کار آن مسکین به آخر خوب گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که آن دعای شیخ نه چون هر دعاست</p></div>
<div class="m2"><p>فانی است و گفت او گفت خداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خدا از خود سؤال و کد کند</p></div>
<div class="m2"><p>پس دعای خویش را چون رد کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک سبب انگیخت صنع ذوالجلال</p></div>
<div class="m2"><p>که رهانیدش ز نفرین و وبال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر آن حمام پر می‌کرد طشت</p></div>
<div class="m2"><p>گوهری از دختر شه یاوه گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهری از حلقه‌های گوش او</p></div>
<div class="m2"><p>یاوه گشت و هر زنی در جست و جو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس در حمام را بستند سخت</p></div>
<div class="m2"><p>تا بجویند اولش در پیچ رخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رختها جستند و آن پیدا نشد</p></div>
<div class="m2"><p>دزد گوهر نیز هم رسوا نشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس به جد جستن گرفتند از گزاف</p></div>
<div class="m2"><p>در دهان و گوش و اندر هر شکاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در شکاف تحت و فوق و هر طرف</p></div>
<div class="m2"><p>جست و جو کردند دری خوش صدف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بانگ آمد که همه عریان شوید</p></div>
<div class="m2"><p>هر که هستید ار عجوز و گر نوید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک به یک را حاجبه جستن گرفت</p></div>
<div class="m2"><p>تا پدید آید گهردانهٔ شگفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن نصوح از ترس شد در خلوتی</p></div>
<div class="m2"><p>روی زرد و لب کبود از خشیتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش چشم خویش او می‌دید مرگ</p></div>
<div class="m2"><p>رفت و می‌لرزید او مانند برگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت یارب بارها برگشته‌ام</p></div>
<div class="m2"><p>توبه‌ها و عهدها بشکسته‌ام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرده‌ام آنها که از من می‌سزید</p></div>
<div class="m2"><p>تا چنین سیل سیاهی در رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوبت جستن اگر در من رسد</p></div>
<div class="m2"><p>وه که جان من چه سختیها کشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در جگر افتاده‌استم صد شرر</p></div>
<div class="m2"><p>در مناجاتم ببین بوی جگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این چنین اندوه کافر را مباد</p></div>
<div class="m2"><p>دامن رحمت گرفتم داد داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاشکی مادر نزادی مر مرا</p></div>
<div class="m2"><p>یا مرا شیری بخوردی در چرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای خدا آن کن که از تو می‌سزد</p></div>
<div class="m2"><p>که ز هر سوراخ مارم می‌گزد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان سنگین دارم و دل آهنین</p></div>
<div class="m2"><p>ورنه خون گشتی درین رنج و حنین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وقت تنگ آمد مرا و یک نفس</p></div>
<div class="m2"><p>پادشاهی کن مرا فریاد رس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر مرا این بار ستاری کنی</p></div>
<div class="m2"><p>توبه کردم من ز هر ناکردنی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>توبه‌ام بپذیر این بار دگر</p></div>
<div class="m2"><p>تا ببندم بهر توبه صد کمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من اگر این بار تقصیری کنم</p></div>
<div class="m2"><p>پس دگر مشنو دعا و گفتنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این همی زارید و صد قطره روان</p></div>
<div class="m2"><p>که در افتادم به جلاد و عوان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا نمیرد هیچ افرنگی چنین</p></div>
<div class="m2"><p>هیچ ملحد را مبادا این حنین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نوحه‌ها کرد او بر جان خویش</p></div>
<div class="m2"><p>روی عزرائیل دیده پیش پیش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای خدا و ای خدا چندان بگفت</p></div>
<div class="m2"><p>که آن در و دیوار با او گشت جفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در میان یارب و یارب بد او</p></div>
<div class="m2"><p>بانگ آمد از میان جست و جو</p></div></div>