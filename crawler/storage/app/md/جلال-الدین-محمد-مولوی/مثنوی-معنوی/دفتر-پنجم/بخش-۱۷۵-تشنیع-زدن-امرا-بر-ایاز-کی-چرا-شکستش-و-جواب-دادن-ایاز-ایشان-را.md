---
title: >-
    بخش ۱۷۵ - تشنیع زدن امرا بر ایاز کی چرا شکستش و جواب دادن ایاز ایشان را
---
# بخش ۱۷۵ - تشنیع زدن امرا بر ایاز کی چرا شکستش و جواب دادن ایاز ایشان را

<div class="b" id="bn1"><div class="m1"><p>گفت ایاز ای مهتران نامور</p></div>
<div class="m2"><p>امر شه بهتر به قیمت یا گهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امر سلطان به بود پیش شما</p></div>
<div class="m2"><p>یا که این نیکو گهر بهر خدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نظرتان بر گهر بر شاه نه</p></div>
<div class="m2"><p>قبله‌تان غولست و جادهٔ راه نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ز شه بر می‌نگردانم بصر</p></div>
<div class="m2"><p>من چو مشرک روی نارم با حجر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌گهر جانی که رنگین سنگ را</p></div>
<div class="m2"><p>برگزیند پس نهد شاه مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت سوی لعبت گل‌رنگ کن</p></div>
<div class="m2"><p>عقل در رنگ‌آورنده دنگ کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر آ در جو سبو بر سنگ زن</p></div>
<div class="m2"><p>آتش اندر بو و اندر رنگ زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نه‌ای در راه دین از ره‌زنان</p></div>
<div class="m2"><p>رنگ و بو مپرست مانند زنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر فرود انداختند آن مهتران</p></div>
<div class="m2"><p>عذرجویان گشته زان نسیان به جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دل هر یک دو صد آه آن زمان</p></div>
<div class="m2"><p>هم‌چو دودی می‌شدی تا آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرد اشارت شه به جلاد کهن</p></div>
<div class="m2"><p>که ز صدرم این خسان را دور کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این خسان چه لایق صدر من‌اند</p></div>
<div class="m2"><p>کز پی سنگ امر ما را بشکنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امر ما پیش چنین اهل فساد</p></div>
<div class="m2"><p>بهر رنگین سنگ شد خوار و کساد</p></div></div>