---
title: >-
    بخش ۱۴۸ - حکایت مات کردن دلقک سید شاه ترمد را
---
# بخش ۱۴۸ - حکایت مات کردن دلقک سید شاه ترمد را

<div class="b" id="bn1"><div class="m1"><p>شاه با دلقک همی شطرنج باخت</p></div>
<div class="m2"><p>مات کردش زود خشم شه بتاخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت شه شه و آن شه کبرآورش</p></div>
<div class="m2"><p>یک یک از شطرنج می‌زد بر سرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بگیر اینک شهت ای قلتبان</p></div>
<div class="m2"><p>صبر کرد آن دلقک و گفت الامان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست دیگر باختن فرمود میر</p></div>
<div class="m2"><p>او چنان لرزان که عور از زمهریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باخت دست دیگر و شه مات شد</p></div>
<div class="m2"><p>وقت شه شه گفتن و میقات شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر جهید آن دلقک و در کنج رفت</p></div>
<div class="m2"><p>شش نمد بر خود فکند از بیم تفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر بالشها و زیر شش نمد</p></div>
<div class="m2"><p>خفت پنهان تا ز زخم شه رهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت شه هی هی چه کردی چیست این</p></div>
<div class="m2"><p>گفت شه شه شه شه ای شاه گزین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی توان حق گفت جز زیر لحاف</p></div>
<div class="m2"><p>با تو ای خشم‌آور آتش‌سجاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای تو مات و من ز زخم شاه مات</p></div>
<div class="m2"><p>می‌زنم شه شه به زیر رختهات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون محله پر شد از هیهای میر</p></div>
<div class="m2"><p>وز لگد بر در زدن وز دار و گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خلق بیرون جست زود از چپ و راست</p></div>
<div class="m2"><p>کای مقدم وقت عفوست و رضاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مغز او خشکست و عقلش این زمان</p></div>
<div class="m2"><p>کمترست از عقل و فهم کودکان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهد و پیری ضعف بر ضعف آمده</p></div>
<div class="m2"><p>واندر آن زهدش گشادی ناشده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رنج دیده گنج نادیده ز یار</p></div>
<div class="m2"><p>کارها کرده ندیده مزد کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا نبود آن کار او را خود گهر</p></div>
<div class="m2"><p>یا نیامد وقت پاداش از قدر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا که بود آن سعی چون سعی جهود</p></div>
<div class="m2"><p>یا جزا وابستهٔ میقات بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مر ورا درد و مصیبت این بس است</p></div>
<div class="m2"><p>که درین وادی پر خون بی‌کس است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چشم پر درد و نشسته او به کنج</p></div>
<div class="m2"><p>رو ترش کرده فرو افکنده لنج</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه یکی کحال کو را غم خورد</p></div>
<div class="m2"><p>نیش عقلی که به کحلی پی برد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اجتهادی می‌کند با حزر و ظن</p></div>
<div class="m2"><p>کار در بوکست تا نیکو شدن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زان رهش دورست تا دیدار دوست</p></div>
<div class="m2"><p>کو نجوید سر رئیسیش آرزوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ساعتی او با خدا اندر عتاب</p></div>
<div class="m2"><p>که نصیبم رنج آمد زین حساب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ساعتی با بخت خود اندر جدال</p></div>
<div class="m2"><p>که همه پران و ما ببریده بال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر که محبوس است اندر بو و رنگ</p></div>
<div class="m2"><p>گرچه در زهدست باشد خوش تنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا برون ناید ازین ننگین مناخ</p></div>
<div class="m2"><p>کی شود خویش خوش و صدرش فراخ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زاهدان را در خلا پیش از گشاد</p></div>
<div class="m2"><p>کارد و استره نشاید هیچ داد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کز ضجر خود را بدراند شکم</p></div>
<div class="m2"><p>غصهٔ آن بی‌مرادیها و غم</p></div></div>