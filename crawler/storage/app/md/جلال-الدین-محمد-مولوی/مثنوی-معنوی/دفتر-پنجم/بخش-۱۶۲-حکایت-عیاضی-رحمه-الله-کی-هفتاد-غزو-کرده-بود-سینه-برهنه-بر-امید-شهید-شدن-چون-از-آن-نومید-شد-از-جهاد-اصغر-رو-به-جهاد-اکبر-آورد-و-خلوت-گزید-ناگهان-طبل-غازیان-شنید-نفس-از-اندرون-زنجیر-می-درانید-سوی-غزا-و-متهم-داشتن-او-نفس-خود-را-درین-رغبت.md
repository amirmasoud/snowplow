---
title: >-
    بخش ۱۶۲ - حکایت عیاضی رحمه‌الله کی هفتاد غزو کرده بود سینه برهنه بر امید شهید شدن چون از آن نومید شد از جهاد اصغر رو به جهاد اکبر آورد و خلوت گزید ناگهان طبل غازیان شنید نفس از اندرون زنجیر می‌درانید سوی غزا و متهم داشتن او نفس خود را درین رغبت
---
# بخش ۱۶۲ - حکایت عیاضی رحمه‌الله کی هفتاد غزو کرده بود سینه برهنه بر امید شهید شدن چون از آن نومید شد از جهاد اصغر رو به جهاد اکبر آورد و خلوت گزید ناگهان طبل غازیان شنید نفس از اندرون زنجیر می‌درانید سوی غزا و متهم داشتن او نفس خود را درین رغبت

<div class="b" id="bn1"><div class="m1"><p>گفت عیاضی نود بار آمدم</p></div>
<div class="m2"><p>تن برهنه بوک زخمی آیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن برهنه می‌شدم در پیش تیر</p></div>
<div class="m2"><p>تا یکی تیری خورم من جای‌گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر خوردن بر گلو یا مقتلی</p></div>
<div class="m2"><p>در نیابد جز شهیدی مقبلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر تنم یک جایگه بی‌زخم نیست</p></div>
<div class="m2"><p>این تنم از تیر چون پرویزنیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک بر مقتل نیامد تیرها</p></div>
<div class="m2"><p>کار بخت است این نه جلدی و دها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شهیدی روزی جانم نبود</p></div>
<div class="m2"><p>رفتم اندر خلوت و در چله زود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جهاد اکبر افکندم بدن</p></div>
<div class="m2"><p>در ریاضت کردن و لاغر شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بانگ طبل غازیان آمد به گوش</p></div>
<div class="m2"><p>که خرامیدند جیش غزوکوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس از باطن مرا آواز داد</p></div>
<div class="m2"><p>که به گوش حس شنیدم بامداد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیز هنگام غزا آمد برو</p></div>
<div class="m2"><p>خویش را در غزو کردن کن گرو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم ای نفس خبیث بی‌وفا</p></div>
<div class="m2"><p>از کجا میل غزا تو از کجا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راست گوی ای نفس کین حیلت‌گریست</p></div>
<div class="m2"><p>ورنه نفس شهوت از طاعت بریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نگویی راست حمله آرمت</p></div>
<div class="m2"><p>در ریاضت سخت‌تر افشارمت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نفس بانگ آورد آن دم از درون</p></div>
<div class="m2"><p>با فصاحت بی‌دهان اندر فسون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که مرا هر روز اینجا می‌کشی</p></div>
<div class="m2"><p>جان من چون جان گبران می‌کشی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هیچ کس را نیست از حالم خبر</p></div>
<div class="m2"><p>که مرا تو می‌کشی بی‌خواب و خور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در غزا بجهم به یک زخم از بدن</p></div>
<div class="m2"><p>خلق بیند مردی و ایثار من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم ای نفسک منافق زیستی</p></div>
<div class="m2"><p>هم منافق می‌مری تو چیستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در دو عالم تو مرایی بوده‌ای</p></div>
<div class="m2"><p>در دو عالم تو چنین بیهوده‌ای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نذر کردم که ز خلوت هیچ من</p></div>
<div class="m2"><p>سر برون نارم چو زنده‌ست این بدن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زانک در خلوت هر آنچ تن کند</p></div>
<div class="m2"><p>نه از برای روی مرد و زن کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جنبش و آرامش اندر خلوتش</p></div>
<div class="m2"><p>جز برای حق نباشد نیتش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این جهاد اکبرست آن اصغرست</p></div>
<div class="m2"><p>هر دو کار رستمست و حیدرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کار آن کس نیست کو را عقل و هوش</p></div>
<div class="m2"><p>پرد از تن چون بجنبد دنب موش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن چنان کس را بباید چون زنان</p></div>
<div class="m2"><p>دور بودن از مصاف و از سنان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صوفیی آن صوفیی این اینت حیف</p></div>
<div class="m2"><p>آن ز سوزن کشته این را طعمه سیف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نقش صوفی باشد او را نیست جان</p></div>
<div class="m2"><p>صوفیان بدنام هم زین صوفیان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر در و دیوار جسم گل‌سرشت</p></div>
<div class="m2"><p>حق ز غیرت نقش صد صوفی نبشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا ز سحر آن نقشها جنبان شود</p></div>
<div class="m2"><p>تا عصای موسوی پنهان شود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نقشها را میخورد صدق عصا</p></div>
<div class="m2"><p>چشم فرعونیست پر گرد و حصا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صوفی دیگر میان صف حرب</p></div>
<div class="m2"><p>اندر آمد بیست بار از بهر ضرب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با مسلمانان به کافر وقت کر</p></div>
<div class="m2"><p>وانگشت او با مسلمانان به فر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زخم خورد و بست زخمی را که خورد</p></div>
<div class="m2"><p>بار دیگر حمله آورد و نبرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا نمیرد تن به یک زخم از گزاف</p></div>
<div class="m2"><p>تا خورد او بیست زخم اندر مصاف</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حیفش آمد که به زخمی جان دهد</p></div>
<div class="m2"><p>جان ز دست صدق او آسان رهد</p></div></div>