---
title: >-
    بخش ۴۶ - مثال عالم هست نیست‌نما و عالم نیست هست‌نما
---
# بخش ۴۶ - مثال عالم هست نیست‌نما و عالم نیست هست‌نما

<div class="b" id="bn1"><div class="m1"><p>نیست را بنمود هست و محتشم</p></div>
<div class="m2"><p>هست را بنمود بر شکل عدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر را پوشید و کف کرد آشکار</p></div>
<div class="m2"><p>باد را پوشید و بنمودت غبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون منارهٔ خاک پیچان در هوا</p></div>
<div class="m2"><p>خاک از خود چون برآید بر علا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک را بینی به بالا ای علیل</p></div>
<div class="m2"><p>باد را نی جز به تعریف دلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کف همی‌بینی روانه هر طرف</p></div>
<div class="m2"><p>کف بی‌دریا ندارد منصرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کف به حس بینی و دریا از دلیل</p></div>
<div class="m2"><p>فکر پنهان آشکارا قال و قیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفی را اثبات می‌پنداشتیم</p></div>
<div class="m2"><p>دیدهٔ معدوم‌بینی داشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده‌ای که اندر نعاسی شد پدید</p></div>
<div class="m2"><p>کی تواند جز خیال و نیست دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاجرم سرگشته گشتیم از ضلال</p></div>
<div class="m2"><p>چون حقیقت شد نهان پیدا خیال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این عدم را چون نشاند اندر نظر</p></div>
<div class="m2"><p>چون نهان کرد آن حقیقت از بصر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفرین ای اوستاد سحرباف</p></div>
<div class="m2"><p>که نمودی معرضان را درد صاف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساحران مهتاب پیمایند زود</p></div>
<div class="m2"><p>پیش بازرگان و زر گیرند سود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیم بربایند زین گون پیچ پیچ</p></div>
<div class="m2"><p>سیم از کف رفته و کرباس هیچ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این جهان جادوست ما آن تاجریم</p></div>
<div class="m2"><p>که ازو مهتاب پیموده خریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گز کند کرباس پانصد گز شتاب</p></div>
<div class="m2"><p>ساحرانه او ز نور ماهتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون ستد او سیم عمرت ای رهی</p></div>
<div class="m2"><p>سیم شد کرباس نی کیسه تهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قل اعوذت خواند باید کای احد</p></div>
<div class="m2"><p>هین ز نفاثات افغان وز عقد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می‌دمند اندر گره آن ساحرات</p></div>
<div class="m2"><p>الغیاث المستغاث از برد و مات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لیک بر خوان از زبان فعل نیز</p></div>
<div class="m2"><p>که زبان قول سستست ای عزیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در زمانه مر ترا سه همره‌اند</p></div>
<div class="m2"><p>آن یکی وافی و این دو غدرمند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن یکی یاران و دیگر رخت و مال</p></div>
<div class="m2"><p>وآن سوم وافیست و آن حسن الفعال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مال ناید با تو بیرون از قصور</p></div>
<div class="m2"><p>یار آید لیک آید تا به گور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون ترا روز اجل آید به پیش</p></div>
<div class="m2"><p>یار گوید از زبان حال خویش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بدینجا بیش همره نیستم</p></div>
<div class="m2"><p>بر سر گورت زمانی بیستم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فعل تو وافیست زو کن ملتحد</p></div>
<div class="m2"><p>که در آید با تو در قعر لحد</p></div></div>