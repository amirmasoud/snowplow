---
title: >-
    بخش ۳۶ - صفت کشتن خلیل علیه‌السلام زاغ را کی آن اشارت به قمع کدام صفت بود از صفات مذمومهٔ مهلکه در مرید
---
# بخش ۳۶ - صفت کشتن خلیل علیه‌السلام زاغ را کی آن اشارت به قمع کدام صفت بود از صفات مذمومهٔ مهلکه در مرید

<div class="b" id="bn1"><div class="m1"><p>این سخن را نیست پایان و فراغ</p></div>
<div class="m2"><p>ای خلیل حق چرا کشتی تو زاغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر فرمان حکمت فرمان چه بود</p></div>
<div class="m2"><p>اندکی ز اسرار آن باید نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاغ کاغ و نعرهٔ زاغ سیاه</p></div>
<div class="m2"><p>دایما باشد به دنیا عمرخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم‌چو ابلیس از خدای پاک فرد</p></div>
<div class="m2"><p>تا قیامت عمر تن درخواست کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت انظرنی الی یوم الجزا</p></div>
<div class="m2"><p>کاشکی گفتی که تبنا ربنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر بی توبه همه جان کندنست</p></div>
<div class="m2"><p>مرگ حاضر غایب از حق بودنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر و مرگ این هر دو با حق خوش بود</p></div>
<div class="m2"><p>بی‌خدا آب حیات آتش بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن هم از تاثیر لعنت بود کو</p></div>
<div class="m2"><p>در چنان حضرت همی‌شد عمرجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خدا غیر خدا را خواستن</p></div>
<div class="m2"><p>ظن افزونیست و کلی کاستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاصه عمری غرق در بیگانگی</p></div>
<div class="m2"><p>در حضور شیر روبه‌شانگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمر بیشم ده که تا پس‌تر روم</p></div>
<div class="m2"><p>مهلم افزون کن که تا کمتر شوم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا که لعنت را نشانه او بود</p></div>
<div class="m2"><p>بد کسی باشد که لعنت‌جو بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر خوش در قرب جان پروردنست</p></div>
<div class="m2"><p>عمر زاغ از بهر سرگین خوردنست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمر بیشم ده که تا گه می‌خورم</p></div>
<div class="m2"><p>دایم اینم ده که بس بدگوهرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرنه گه خوارست آن گنده‌دهان</p></div>
<div class="m2"><p>گویدی کز خوی زاغم وا رهان</p></div></div>