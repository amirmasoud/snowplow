---
title: >-
    تکه ۲
---
# تکه ۲

<div class="b" id="bn1"><div class="m1"><p>قصابی سوی گولی گوشت انداخت</p></div>
<div class="m2"><p>چو دیدش زفت گوشت گاو پنداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی ران دگر سوی وی افکند</p></div>
<div class="m2"><p>بگفتا گاو مرده‌ست این زهی گند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا بخشید آنچ اسباب کامست</p></div>
<div class="m2"><p>تو گفتی چیست این؟ خود داد عامست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون شد عام کان با تو بپیوست</p></div>
<div class="m2"><p>نجس شد چونک در کردی درو دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسازد گول را بخل و سخاوت</p></div>
<div class="m2"><p>که گردد هر دوش مایهٔ عداوت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریز از گول اندر سور و ماتم</p></div>
<div class="m2"><p>چو عیسی ای پدر والله اعلم</p></div></div>