---
title: >-
    غزل شمارهٔ ۱۹۲۶
---
# غزل شمارهٔ ۱۹۲۶

<div class="b" id="bn1"><div class="m1"><p>مال است و زر است مکسب تن</p></div>
<div class="m2"><p>کسب دل دوستی فزودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستان بی‌دوست هست زندان</p></div>
<div class="m2"><p>زندان با دوست هست گلشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر لذت دوستی نبودی</p></div>
<div class="m2"><p>نی مرد شدی پدید نی زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاری که به باغ دوست روید</p></div>
<div class="m2"><p>خوشتر ز هزار سرو و سوسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هم دوزید عشق ما را</p></div>
<div class="m2"><p>بی منت ریسمان و سوزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خانه عالم است تاریک</p></div>
<div class="m2"><p>بگشاید عشق شصت روزن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور می ترسی ز تیر و شمشیر</p></div>
<div class="m2"><p>جوشن گر عشق ساخت جوشن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم عشق کمال خود بگوید</p></div>
<div class="m2"><p>دم درکش و باش مرد الکن</p></div></div>