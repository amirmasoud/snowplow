---
title: >-
    غزل شمارهٔ ۱۶۴
---
# غزل شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>چو مرا به سوی زندان بکشید تن ز بالا</p></div>
<div class="m2"><p>ز مقربان حضرت بشدم غریب و تنها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به میان حبس ناگه قمری مرا قرین شد</p></div>
<div class="m2"><p>که فکند در دماغم هوسش هزار سودا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه کس خلاص جوید ز بلا و حبس من نی</p></div>
<div class="m2"><p>چه روم چه روی آرم به برون و یار این جا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که به غیر کنج زندان نرسم به خلوت او</p></div>
<div class="m2"><p>که نشد به غیر آتش دل انگبین مصفا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظری به سوی خویشان نظری برو پریشان</p></div>
<div class="m2"><p>نظری بدان تمنا نظری بدین تماشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بود حریف یوسف نرمد کسی چو دارد</p></div>
<div class="m2"><p>به میان حبس بستان و که خاصه یوسف ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدود به چشم و دیده سوی حبس هر کی او را</p></div>
<div class="m2"><p>ز چنین شکرستانی برسد چنین تقاضا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از اختران شنیدم که کسی اگر بیابد</p></div>
<div class="m2"><p>اثری ز نور آن مه خبری کنید ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بدین گهر رسیدی رسدت که از کرامت</p></div>
<div class="m2"><p>بنهی قدم چو موسی گذری ز هفت دریا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خبرش ز رشک جان‌ها نرسد به ماه و اختر</p></div>
<div class="m2"><p>که چو ماه او برآید بگدازد آسمان‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خجلم ز وصف رویش به خدا دهان ببندم</p></div>
<div class="m2"><p>چه برد ز آب دریا و ز بحر مشک سقا</p></div></div>