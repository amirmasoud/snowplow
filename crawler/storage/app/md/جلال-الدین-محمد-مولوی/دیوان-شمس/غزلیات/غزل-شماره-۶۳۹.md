---
title: >-
    غزل شمارهٔ ۶۳۹
---
# غزل شمارهٔ ۶۳۹

<div class="b" id="bn1"><div class="m1"><p>آن سرخ قبایی که چو مه پار برآمد</p></div>
<div class="m2"><p>امسال در این خرقه زنگار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ترک که آن سال به یغماش بدیدی</p></div>
<div class="m2"><p>آنست که امسال عرب وار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یار همانست اگر جامه دگر شد</p></div>
<div class="m2"><p>آن جامه به در کرد و دگربار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن باده همانست اگر شیشه بدل شد</p></div>
<div class="m2"><p>بنگر که چه خوش بر سر خمار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای قوم گمان برده که آن مشعله‌ها مرد</p></div>
<div class="m2"><p>آن مشعله زین روزن اسرار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این نیست تناسخ سخن وحدت محضست</p></div>
<div class="m2"><p>کز جوشش آن قلزم زخار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک قطره از آن بحر جدا شد که جدا نیست</p></div>
<div class="m2"><p>کآدم ز تک صلصل فخار برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رومی پنهان گشت چو دوران حبش دید</p></div>
<div class="m2"><p>امروز در این لشکر جرار برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر شمس فروشد به غروب او نه فنا شد</p></div>
<div class="m2"><p>از برج دگر آن مه انوار برآمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتار رها کن بنگر آینه عین</p></div>
<div class="m2"><p>کان شبهه و اشکال ز گفتار برآمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس الحق تبریز رسیدست مگویید</p></div>
<div class="m2"><p>کز چرخ صفا آن مه اسرار برآمد</p></div></div>