---
title: >-
    غزل شمارهٔ ۲۴۷۳
---
# غزل شمارهٔ ۲۴۷۳

<div class="b" id="bn1"><div class="m1"><p>آب تو ده گسسته را در دو جهان سقا تویی</p></div>
<div class="m2"><p>بار تو ده شکسته را بارگه وفا تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برج نشاط رخنه شد لشکر دل برهنه شد</p></div>
<div class="m2"><p>میمنه را کله تویی میسره را قبا تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می زده مییم ما کوفته دییم ما</p></div>
<div class="m2"><p>چشم نهاده‌ایم ما در تو که توتیا تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی متاب از وفا خاک مریز بر صفا</p></div>
<div class="m2"><p>آب حیاتی و حیا پشت دل و بقا تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ تو را ندا کند بهر تو جان فدا کند</p></div>
<div class="m2"><p>هر چه ز تو زیان کند آن همه را دوا تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز بیار باده‌ای مرکب هر پیاده‌ای</p></div>
<div class="m2"><p>بهر زکات جان خود ساقی جان ما تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این خبر و مجادلی نیست نشان یک دلی</p></div>
<div class="m2"><p>گردن این خبر بزن شحنه کبریا تویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردن عربده بزن وسوسه را ز بن بکن</p></div>
<div class="m2"><p>باده خاص درفکن خاصبک خدا تویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وقت لقای یوسفان مست بدند کف بران</p></div>
<div class="m2"><p>ما نه کمیم از زنان یوسف خوش لقا تویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از رخ دوست باخبر وز کف خویش بی‌خبر</p></div>
<div class="m2"><p>این خبری است معتبر پیش تو کاوستا تویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پر کن زان می نهان تا بخوریم بی‌دهان</p></div>
<div class="m2"><p>تا که بداند این جهان باز که کیمیا تویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باده کهنه خدا روز الست ره نما</p></div>
<div class="m2"><p>گشته به دست انبیا وارث انبیا تویی</p></div></div>