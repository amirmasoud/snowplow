---
title: >-
    غزل شمارهٔ ۲۳۱۴
---
# غزل شمارهٔ ۲۳۱۴

<div class="b" id="bn1"><div class="m1"><p>ای روی تو رویم را چون روی قمر کرده</p></div>
<div class="m2"><p>اجزای مرا چشمت اصحاب نظر کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد تو درختم را در رقص درآورده</p></div>
<div class="m2"><p>یاد تو دهانم را پرشهد و شکر کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی که درخت من در رقص چرا آید</p></div>
<div class="m2"><p>ای شاخ و درختم را پربرگ و ثمر کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برگ نمی‌نازد وز میوه نمی‌یازد</p></div>
<div class="m2"><p>ای صبر درختم را تو زیر و زبر کرده</p></div></div>