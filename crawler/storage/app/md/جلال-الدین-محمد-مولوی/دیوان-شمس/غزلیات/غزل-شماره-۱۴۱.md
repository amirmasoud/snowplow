---
title: >-
    غزل شمارهٔ ۱۴۱
---
# غزل شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>جمله یاران تو سنگند و توی مرجان چرا</p></div>
<div class="m2"><p>آسمان با جملگان جسمست و با تو جان چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تو آیی جزو جزوم جمله دستک می‌زنند</p></div>
<div class="m2"><p>چون تو رفتی جمله افتادند در افغان چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خیالت جزو جزوم می‌شود خندان لبی</p></div>
<div class="m2"><p>می‌شود با دشمن تو مو به مو دندان چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی خط و بی‌خال تو این عقل امی می‌بود</p></div>
<div class="m2"><p>چون ببیند آن خطت را می‌شود خط خوان چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن همی‌گوید به جان پرهیز کن از عشق او</p></div>
<div class="m2"><p>جانش می‌گوید حذر از چشمه حیوان چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی تو پیغامبر خوبی و حسن ایزدست</p></div>
<div class="m2"><p>جان به تو ایمان نیارد با چنین برهان چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو یکی برهان که آن از روی تو روشنترست</p></div>
<div class="m2"><p>کف نبرد کفرها زین یوسف کنعان چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا تخمی بکاری آن بروید عاقبت</p></div>
<div class="m2"><p>برنروید هیچ از شه دانه احسان چرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا ویران بود آن جا امید گنج هست</p></div>
<div class="m2"><p>گنج حق را می‌نجویی در دل ویران چرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی ترازو هیچ بازاری ندیدم در جهان</p></div>
<div class="m2"><p>جمله موزونند عالم نبودش میزان چرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گیرم این خربندگان خود بار سرگین می‌کشند</p></div>
<div class="m2"><p>این سواران باز می‌مانند از میدان چرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر ترانه اولی دارد دلا و آخری</p></div>
<div class="m2"><p>بس کن آخر این ترانه نیستش پایان چرا</p></div></div>