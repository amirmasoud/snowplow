---
title: >-
    غزل شمارهٔ ۲۶۷۰
---
# غزل شمارهٔ ۲۶۷۰

<div class="b" id="bn1"><div class="m1"><p>خوشی آخر بگو ای یار چونی</p></div>
<div class="m2"><p>از این ایام ناهموار چونی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روز و شب مرا اندیشه توست</p></div>
<div class="m2"><p>کز این روز و شب خون خوار چونی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این آتش که در عالم فتاده‌ست</p></div>
<div class="m2"><p>ز دود لشکر تاتار چونی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این دریا و تاریکی و صد موج</p></div>
<div class="m2"><p>تو اندر کشتی پربار چونی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم بیمار و تو ما را طبیبی</p></div>
<div class="m2"><p>بپرس آخر که ای بیمار چونی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت پرسم اگر تو می‌نپرسی</p></div>
<div class="m2"><p>که ای شیرین شیرین کار چونی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجودی بین که بی‌چون و چگونه‌ست</p></div>
<div class="m2"><p>دلا دیگر مگو بسیار چونی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگو در گوش شمس الدین تبریز</p></div>
<div class="m2"><p>که ای خورشید خوب اسرار چونی</p></div></div>