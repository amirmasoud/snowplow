---
title: >-
    غزل شمارهٔ ۱۳۶۴
---
# غزل شمارهٔ ۱۳۶۴

<div class="b" id="bn1"><div class="m1"><p>ایها النور فی الفؤاد تعال</p></div>
<div class="m2"><p>غایه الجد و المراد تعال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انت تدری حیاتنا بیدیک</p></div>
<div class="m2"><p>لا تضیق علی العباد تعال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایها العشق ایها المعشوق</p></div>
<div class="m2"><p>حل عن الصد و العناد تعال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا سلیمان ذی الهداهد لک</p></div>
<div class="m2"><p>فتفقد بالافتقاد تعال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایها السابق الذی سبقت</p></div>
<div class="m2"><p>منک مصدوقه الوداد تعال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فمن الهجر ضجت الارواح</p></div>
<div class="m2"><p>انجر العود یا معاد تعال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>استر العیب و ابذل المعروف</p></div>
<div class="m2"><p>هکذا عاده الجواد تعال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه بود پارسی تعال بیا</p></div>
<div class="m2"><p>یا بیا یا بده تو داد تعال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بیایی زهی گشاد و مراد</p></div>
<div class="m2"><p>چون نیایی زهی کساد تعال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای گشاد عرب قباد عجم</p></div>
<div class="m2"><p>تو گشایی دلم به یاد تعال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای درونم تعال گویان تو</p></div>
<div class="m2"><p>وی ز بود تو بود و باد تعال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طفت فیک البلاد یا قمرا</p></div>
<div class="m2"><p>بی‌محیطا و بالبلاد تعال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>انت کالشمس اذ دنت و نأت</p></div>
<div class="m2"><p>یا قریبا علی العباد تعال</p></div></div>