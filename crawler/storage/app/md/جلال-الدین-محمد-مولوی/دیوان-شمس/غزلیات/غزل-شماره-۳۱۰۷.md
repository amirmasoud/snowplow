---
title: >-
    غزل شمارهٔ ۳۱۰۷
---
# غزل شمارهٔ ۳۱۰۷

<div class="b" id="bn1"><div class="m1"><p>میان تیرگی خواب و نور بیداری</p></div>
<div class="m2"><p>چنان نمود مرا دوش در شب تاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که خوب طلعتی از ساکنان حضرت قدس</p></div>
<div class="m2"><p>که جمله محض خرد بود و نور هشیاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنش چو روی مقدس بری ز کسوت جسم</p></div>
<div class="m2"><p>چو عقل و جان گهردار، وز غرض عاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ستایش بسیار کرد و گفت:« ای آن</p></div>
<div class="m2"><p>که در جحیم طبیعت چنین گرفتاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکفته گلبن جوزا برای عشرت تست</p></div>
<div class="m2"><p>تو سر به گلخن گیتی چرا فرود آری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سریر هفت فلک تخت تست اگرچه کنون</p></div>
<div class="m2"><p>ز دست طبع، گرفتار چار دیواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال جان چو بهایم ز خواب و خور مطلب</p></div>
<div class="m2"><p>که آفریده تو زین‌سان نه بهر این کاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدی مکن که درین کشت زار زود زوال</p></div>
<div class="m2"><p>به داس دهر همان بدروی که می‌کاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی مراد چه پویی به عالمی که در او</p></div>
<div class="m2"><p>چو دفع رنج کنی جمله راحت انگاری؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حقیقت این شکم از آز پر نخواهد شد</p></div>
<div class="m2"><p>اگر به ملک همه عالمش بیانباری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفتمت که رسیدی بدانچ می‌طلبی</p></div>
<div class="m2"><p>ولی چه سود از آن، چون بجاش بگذاری؟!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب جوانیت ای دوست چون سپیده دمید</p></div>
<div class="m2"><p>تو مست، خفته و آگه نه‌ای ز بیداری</p></div></div>