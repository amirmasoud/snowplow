---
title: >-
    غزل شمارهٔ ۴۴۰
---
# غزل شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>امروز شهر ما را صد رونق‌ست و جانست</p></div>
<div class="m2"><p>زیرا که شاه خوبان امروز در میانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیران چرا نباشد خندان چرا نباشد</p></div>
<div class="m2"><p>شهری که در میانش آن صارم زمانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن آفتاب خوبی چون بر زمین بتابد</p></div>
<div class="m2"><p>آن دم زمین خاکی بهتر ز آسمانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چرخ سبزپوشان پر می‌زنند یعنی</p></div>
<div class="m2"><p>سلطان و خسرو ما آن‌ست و صد چنانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای جان جان جانان از ما سلام برخوان</p></div>
<div class="m2"><p>رحم آر بر ضعیفان عشق تو بی‌امانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سبز و خوش نباشد عالم چو تو بهاری</p></div>
<div class="m2"><p>چون ایمنی نباشد چون شیر پاسبانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کوفت او در دل ناآمده به منزل</p></div>
<div class="m2"><p>دانست جان ز بویش کان یار مهربانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن کو کشید دستت او آفریده‌ستت</p></div>
<div class="m2"><p>وان کو قرین جان شد او صاحب قرانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او ماه بی‌خسوف‌ست خورشید بی‌کسوفست</p></div>
<div class="m2"><p>او خمر بی‌خمارست او سود بی‌زیانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن شهریار اعظم بزمی نهاد خرم</p></div>
<div class="m2"><p>شمع و شراب و شاهد امروز رایگانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون مست گشت مردم شد گوهرش برهنه</p></div>
<div class="m2"><p>پهلو شکست کان را زان کس که پهلوانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلاله چون صبا شد از خار گل جدا شد</p></div>
<div class="m2"><p>باران نبات‌ها را در باغ امتحانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی عز و نازنینی کی کرد ناز و بینی</p></div>
<div class="m2"><p>هر کس که کرد والله خام‌ست و قلتبانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خامش که تا بگوید بی‌حرف و بی‌زبان او</p></div>
<div class="m2"><p>خود چیست این زبان‌ها گر آن زبان زبانست</p></div></div>