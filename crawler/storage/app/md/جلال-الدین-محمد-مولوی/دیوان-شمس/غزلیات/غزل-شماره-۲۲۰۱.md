---
title: >-
    غزل شمارهٔ ۲۲۰۱
---
# غزل شمارهٔ ۲۲۰۱

<div class="b" id="bn1"><div class="m1"><p>در گذر آمد خیالش گفت جان این است او</p></div>
<div class="m2"><p>پادشاه شهرهای لامکان این است او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد هزار انگشت‌ها اندر اشارت دیده شد</p></div>
<div class="m2"><p>سوی او از نور جان‌ها کای فلان این است او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون زمین سرسبز گشت از عکس آن گلزار او</p></div>
<div class="m2"><p>نعره‌ها آمد به گوشم ز آسمان این است او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هین سبکتر دست درزن در عنان مرکبش</p></div>
<div class="m2"><p>پیش از آن کو برکشاند آن عنان این است او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمله نور حق گرفته همچو طور این جان از او</p></div>
<div class="m2"><p>همچو گوهر تافته از عین کان این است او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو به ماه آورد مریخ و بگفتش هوش دار</p></div>
<div class="m2"><p>تا نلافی تو ز خوبی هان و هان این است او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس تبریزی شنیدستی ببین این نور را</p></div>
<div class="m2"><p>کز وی آمد کاسدی‌های بتان این است او</p></div></div>