---
title: >-
    غزل شمارهٔ ۶۹۳
---
# غزل شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>جانی که ز نور مصطفی زاد</p></div>
<div class="m2"><p>با او تو مگو ز داد و بیداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز ماهی سباحت آموخت</p></div>
<div class="m2"><p>آزادی جست سرو آزاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاری که ز گلبن طرب رست</p></div>
<div class="m2"><p>گلزار به روی او شود شاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دورست رواق‌های شادی</p></div>
<div class="m2"><p>از آتش و آب و خاک و از باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین چار بسیط چون چلیپا</p></div>
<div class="m2"><p>ترکیب موحدان برون باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان سو فلکیست نیک روشن</p></div>
<div class="m2"><p>زان سو ملکیست بسته مرصاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمتر بخشش دو چشم بخشد</p></div>
<div class="m2"><p>بینا و حکیم و تیز و استاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دیده جان چو واپس آیی</p></div>
<div class="m2"><p>در عالم آب و گل به ارشاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بینی تو و دیگران نبینند</p></div>
<div class="m2"><p>هر سو نوری به رسم میلاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در هر ابری هزار خورشید</p></div>
<div class="m2"><p>در هر ویران بهشت آباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تختی بنهی به قصر مردان</p></div>
<div class="m2"><p>هم خیمه زنی به بام اوتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بویی ببری ز شمس تبریز</p></div>
<div class="m2"><p>کو را است ملک مطیع و منقاد</p></div></div>