---
title: >-
    غزل شمارهٔ ۵۱
---
# غزل شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>گر تو ملولی ای پدر جانب یار من بیا</p></div>
<div class="m2"><p>تا که بهار جان‌ها تازه کند دل تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی سلام یار من لخلخه بهار من</p></div>
<div class="m2"><p>باغ و گل و ثمار من آرد سوی جان صبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی و طرفه مستیی هستی و طرفه هستیی</p></div>
<div class="m2"><p>ملک و درازدستیی نعره زنان که الصلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای بکوب و دست زن دست در آن دو شست زن</p></div>
<div class="m2"><p>پیش دو نرگس خوشش کشته نگر دل مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنده به عشق سرکشم بینی جان چرا کشم</p></div>
<div class="m2"><p>پهلوی یار خود خوشم یاوه چرا روم چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان چو سوی وطن رود آب به جوی من رود</p></div>
<div class="m2"><p>تا سوی گولخن رود طبع خسیس ژاژخا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدن خسرو زمن شعشعه عقار من</p></div>
<div class="m2"><p>سخت خوش است این وطن می‌نروم از این سرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان طرب پرست ما عقل خراب مست ما</p></div>
<div class="m2"><p>ساغر جان به دست ما سخت خوش است ای خدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوش برفت گو برو جایزه گو بشو گرو</p></div>
<div class="m2"><p>روز شدست گو بشو بی‌شب و روز تو بیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مست رود نگار من در بر و در کنار من</p></div>
<div class="m2"><p>هیچ مگو که یار من باکرمست و باوفا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد جان جان من کوری دشمنان من</p></div>
<div class="m2"><p>رونق گلستان من زینت روضه رضا</p></div></div>