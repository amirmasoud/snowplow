---
title: >-
    غزل شمارهٔ ۲۲۷۰
---
# غزل شمارهٔ ۲۲۷۰

<div class="b" id="bn1"><div class="m1"><p>ابناء ربیعنا تعالوا</p></div>
<div class="m2"><p>فالورد یقول لا تبالوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و العشق یصیحکم جهارا</p></div>
<div class="m2"><p>الخلد لکم فلا تزالوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و الحسن علی البها تجلی</p></div>
<div class="m2"><p>و السکر حواه و الکمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من کان مخرسا جمادا</p></div>
<div class="m2"><p>الیوم تکلموا و قالوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کان مبلسا قنوطا</p></div>
<div class="m2"><p>ذابوا و تضاحکوا و نالوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بعد فان تروا غضوبا</p></div>
<div class="m2"><p>ماذا غضب فذا دلال</p></div></div>