---
title: >-
    غزل شمارهٔ ۷۸
---
# غزل شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>ساقی ز شراب حق پر دار شرابی را</p></div>
<div class="m2"><p>درده می ربانی دل‌های کبابی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم گوی حدیث نان در مجلس مخموران</p></div>
<div class="m2"><p>جز آب نمی‌سازد مر مردم آبی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آب و خطاب تو تن گشت خراب تو</p></div>
<div class="m2"><p>آراسته دار ای جان زین گنج خرابی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلزار کند عشقت آن شوره خاکی را</p></div>
<div class="m2"><p>دربار کند موجت این جسم سحابی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفزای شراب ما بربند تو خواب ما</p></div>
<div class="m2"><p>از شب چه خبر باشد مر مردم خوابی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همکاسه ملک باشد مهمان خدایی را</p></div>
<div class="m2"><p>باده ز فلک آید مردان ثوابی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوشد لب صدیقش ز اکواب و اباریقش</p></div>
<div class="m2"><p>در خم تقی یابی آن باده نابی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هشیار کجا داند بی‌هوشی مستان را</p></div>
<div class="m2"><p>بوجهل کجا داند احوال صحابی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>استاد خدا آمد بی‌واسطه صوفی را</p></div>
<div class="m2"><p>استاد کتاب آمد صابی و کتابی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون محرم حق گشتی وز واسطه بگذشتی</p></div>
<div class="m2"><p>بربای نقاب از رخ خوبان نقابی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منکر که ز نومیدی گوید که نیابی این</p></div>
<div class="m2"><p>بنده ره او سازد آن گفت نیابی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نی باز سپیدست او نی بلبل خوش نغمه</p></div>
<div class="m2"><p>ویرانه دنیا به آن جغد غرابی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاموش و مگو دیگر مفزای تو شور و شر</p></div>
<div class="m2"><p>کز غیب خطاب آید جان‌های خطابی را</p></div></div>