---
title: >-
    غزل شمارهٔ ۲۷۷۷
---
# غزل شمارهٔ ۲۷۷۷

<div class="b" id="bn1"><div class="m1"><p>شاد آن صبحی که جان را چاره آموزی کنی</p></div>
<div class="m2"><p>چاره او یابد که تش بیچارگی روزی کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق جامه می‌دراند عقل بخیه می‌زند</p></div>
<div class="m2"><p>هر دو را زهره بدرد چون تو دلدوزی کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بسوزم همچو عود و نیست گردم همچو دود</p></div>
<div class="m2"><p>خوشتر از سوزش چه باشد چون تو دلسوزی کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه لباس قهر درپوشی و راه دل زنی</p></div>
<div class="m2"><p>گه بگردانی لباس آیی قلاوزی کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش بچر ای گاو عنبربخش نفس مطمئن</p></div>
<div class="m2"><p>در چنین ساحل حلال است ار تو خوش پوزی کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوطیی که طمع اسب و مرکب تازی کنی</p></div>
<div class="m2"><p>ماهیی که میل شعر و جامه توزی کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر مستی و شکارت آهوان شیرمست</p></div>
<div class="m2"><p>با پنیر گنده فانی کجا یوزی کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند گویم قبله کامشب هر یکی را قبله‌ای است</p></div>
<div class="m2"><p>قبله‌ها گردد یکی گر تو شب افروزی کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ز لعل شمس تبریزی بیابی مایه‌ای</p></div>
<div class="m2"><p>کمترین پایه فراز چرخ پیروزی کنی</p></div></div>