---
title: >-
    غزل شمارهٔ ۲۸۹۳
---
# غزل شمارهٔ ۲۸۹۳

<div class="b" id="bn1"><div class="m1"><p>قدر غم گر چشم سر بگریستی</p></div>
<div class="m2"><p>روز و شب‌ها تا سحر بگریستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان گر واقفستی زین فراق</p></div>
<div class="m2"><p>انجم و شمس و قمر بگریستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین چنین عزلی شه ار واقف شدی</p></div>
<div class="m2"><p>بر خود و تاج و کمر بگریستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شب گردک بدیدی این طلاق</p></div>
<div class="m2"><p>بر کنار و بوسه بربگریستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شراب لعل دیدی این خمار</p></div>
<div class="m2"><p>بر قنینه و شیشه گر بگریستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گلستان واقفستی زین خزان</p></div>
<div class="m2"><p>برگ گل بر شاخ تر بگریستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ پران واقفستی زین شکار</p></div>
<div class="m2"><p>سست کردی بال و پر بگریستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر فلاطون را هنر نفریفتی</p></div>
<div class="m2"><p>نوحه کردی بر هنر بگریستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزن ار واقف شدی از دود مرگ</p></div>
<div class="m2"><p>روزن و دیوار و در بگریستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشتی اندر بحر رقصان می‌رود</p></div>
<div class="m2"><p>گر بدیدی این خطر بگریستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آتش این بوته گر ظاهر شدی</p></div>
<div class="m2"><p>محتشم بر سیم و زر بگریستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رستم ار هم واقفستی زین ستم</p></div>
<div class="m2"><p>بر مصاف و کر و فر بگریستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این اجل کر است و ناله نشنود</p></div>
<div class="m2"><p>ور نه با خون جگر بگریستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل ندارد هیچ این جلاد مرگ</p></div>
<div class="m2"><p>ور دلش بودی حجر بگریستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر نمودی ناخنان خویش مرگ</p></div>
<div class="m2"><p>دست و پا بر همدگر بگریستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وقت پیچاپیچ اگر حاضر شدی</p></div>
<div class="m2"><p>ماده بز بر شیر نر بگریستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مادر فرزندخوار آمد زمین</p></div>
<div class="m2"><p>ور نه بر مرگ پسر بگریستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان شیرین دادن از تلخی مرگ</p></div>
<div class="m2"><p>گر شدی پیدا شکر بگریستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داندی مقری که عرعر می‌کند</p></div>
<div class="m2"><p>ترک کردی عر و عر بگریستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر جنازه واقفستی زین کفن</p></div>
<div class="m2"><p>این جنازه بر گذر بگریستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کودک نوزاد می‌گرید ز نقل</p></div>
<div class="m2"><p>عاقلستی بیشتر بگریستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیک بی‌عقلی نگرید طفل نیز</p></div>
<div class="m2"><p>ور نه چشم گاو و خر بگریستی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با همه تلخی همین شیرین ما</p></div>
<div class="m2"><p>چاره دیدی چون مطر بگریستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان که شیرین دید تلخی‌های مرگ</p></div>
<div class="m2"><p>زان چه دید آن دیده ور بگریستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که گذشت آن من و رفت آنچ رفت</p></div>
<div class="m2"><p>کو خبر تا زین خبر بگریستی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تیر زهرآلود کآمد بر جگر</p></div>
<div class="m2"><p>بر سپر جستی سپر بگریستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زیر خاکم آن چنانک این جهان</p></div>
<div class="m2"><p>شاید ار زیر و زبر بگریستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هین خمش کن نیست یک صاحب نظر</p></div>
<div class="m2"><p>ور بدی صاحب نظر بگریستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شمس تبریزی برفت و کو کسی</p></div>
<div class="m2"><p>تا بر آن فخرالبشر بگریستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عالم معنی عروسی یافت زو</p></div>
<div class="m2"><p>لیک بی‌او این صور بگریستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این جهان را غیر آن سمع و بصر</p></div>
<div class="m2"><p>گر بدی سمع و بصر بگریستی</p></div></div>