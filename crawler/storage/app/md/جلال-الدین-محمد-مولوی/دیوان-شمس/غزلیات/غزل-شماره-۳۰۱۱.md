---
title: >-
    غزل شمارهٔ ۳۰۱۱
---
# غزل شمارهٔ ۳۰۱۱

<div class="b" id="bn1"><div class="m1"><p>هر نفسی از درون دلبر روحانیی</p></div>
<div class="m2"><p>عربده آرد مرا از ره پنهانیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه و ویرانیم شور و پریشانیم</p></div>
<div class="m2"><p>برد مسلمانیم وای مسلمانیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت مرا می خوری یا چه گمان می‌بری</p></div>
<div class="m2"><p>کیست برون از گمان جز دل ربانیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر افسانه رو مست سوی خانه رو</p></div>
<div class="m2"><p>جان بفشان کان نگار کرد گل افشانیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دم ای خوش عذار حال مرا گوش دار</p></div>
<div class="m2"><p>مست غمت را بیار رسم نگهبانیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عابد و معبود من شاهد و مشهود من</p></div>
<div class="m2"><p>عشق شناس ای حریف در دل انسانیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه ما کوی او قبله ما روی او</p></div>
<div class="m2"><p>رهبر ما بوی او در ره سلطانیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه صاحب نظر الحذر از ما حذر</p></div>
<div class="m2"><p>تا ننهد خواجه سر در خطر جانیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی غلطم سر بیار تا ببری صد هزار</p></div>
<div class="m2"><p>گل ندمد جز ز خار گنج به ویرانیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمد آن شیر من عاشق جان سیر من</p></div>
<div class="m2"><p>در کف او شیشه‌ای شکل پری خوانیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم ای روح قدس آخر ما را بپرس</p></div>
<div class="m2"><p>گفت چه پرسم دریغ حال مرا دانیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مستم و گم کرده راه تن زن و پرسش مخواه</p></div>
<div class="m2"><p>مست چه‌ام بوی گیر باده جانانیی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کی بود آن ای خدا ما شده از ما جدا</p></div>
<div class="m2"><p>برده قماشات ما غارت سبحانیی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کی ورا کار کیست در کف او خارکیست</p></div>
<div class="m2"><p>هر کی ورا یار کیست هست چو زندانیی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کارک تو هم تویی یارک تو هم تویی</p></div>
<div class="m2"><p>هر کی ز خود دور شد نیست به جز فانیی</p></div></div>