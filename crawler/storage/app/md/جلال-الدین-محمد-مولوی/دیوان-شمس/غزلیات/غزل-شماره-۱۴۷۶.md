---
title: >-
    غزل شمارهٔ ۱۴۷۶
---
# غزل شمارهٔ ۱۴۷۶

<div class="b" id="bn1"><div class="m1"><p>طبیبیم حکیمیم طبیبان قدیمیم</p></div>
<div class="m2"><p>شرابیم و کبابیم و سهیلیم و ادیمیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو رنجور تن آید چو معجون نجاحیم</p></div>
<div class="m2"><p>چو بیمار دل آید نگاریم و ندیمیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیبان بگریزند چو رنجور بمیرد</p></div>
<div class="m2"><p>ولی ما نگریزیم که ما یار کریمیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شتابید شتابید که ما بر سر راهیم</p></div>
<div class="m2"><p>جهان درخور ما نیست که ما ناز و نعیمیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غلط رفت غلط رفت که این نقش نه ماییم</p></div>
<div class="m2"><p>که تن شاخ درختی است و ما باد نسیمیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی جنبش این شاخ هم از فعل نسیم است</p></div>
<div class="m2"><p>خمش باش خمش باش هم آنیم و هم اینیم</p></div></div>