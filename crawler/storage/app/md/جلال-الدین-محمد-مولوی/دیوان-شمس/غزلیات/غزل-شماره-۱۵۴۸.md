---
title: >-
    غزل شمارهٔ ۱۵۴۸
---
# غزل شمارهٔ ۱۵۴۸

<div class="b" id="bn1"><div class="m1"><p>روی تو چو نوبهار دیدم</p></div>
<div class="m2"><p>گل را ز تو شرمسار دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در دل من قرار کردی</p></div>
<div class="m2"><p>دل را ز تو بی‌قرار دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چشم شدم همه چو نرگس</p></div>
<div class="m2"><p>کان نرگس پرخمار دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق روم که عشق را من</p></div>
<div class="m2"><p>از جمله بلا حصار دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ملک جهان و عیش عالم</p></div>
<div class="m2"><p>من عشق تو اختیار دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود ملک تویی و جان عالم</p></div>
<div class="m2"><p>یک بود و منش هزار دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من مردم و از تو زنده گشتم</p></div>
<div class="m2"><p>پس عالم را دو بار دیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مطرب اگر تو یار مایی</p></div>
<div class="m2"><p>این پرده بزن که یار دیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در شهر شما چه یار جویم</p></div>
<div class="m2"><p>چون یاری شهریار دیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون در بر خود خوشش فشردم</p></div>
<div class="m2"><p>آیین شکرفشار دیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بستم من دهان ز گفتن</p></div>
<div class="m2"><p>بس گفتن بی‌شمار دیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون پای نماند اندر این ره</p></div>
<div class="m2"><p>من رفتن راهوار دیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر درنکشم ز ضر که بی‌سر</p></div>
<div class="m2"><p>سرهای کلاه دار دیدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس کن که ملول گشت دلبر</p></div>
<div class="m2"><p>بر خاطر او غبار دیدم</p></div></div>