---
title: >-
    غزل شمارهٔ ۵۰۱
---
# غزل شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>امشب از چشم و مغز خواب گریخت</p></div>
<div class="m2"><p>دید دل را چنین خراب گریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب دل را خراب دید و یباب</p></div>
<div class="m2"><p>بی نمک بود از این کباب گریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب مسکین به زیر پنجه عشق</p></div>
<div class="m2"><p>زخم‌ها خورد وز اضطراب گریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق همچون نهنگ لب بگشاد</p></div>
<div class="m2"><p>خواب چون ماهی اندر آب گریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب چون دید خصم بی‌زنهار</p></div>
<div class="m2"><p>مول مولی بزد شتاب گریخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه ما شب برآمد و این خواب</p></div>
<div class="m2"><p>همچو سایه ز آفتاب گریخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواب چون دید دولت بیدار</p></div>
<div class="m2"><p>همچو گنجشک از عقاب گریخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکرلله همای بازآمد</p></div>
<div class="m2"><p>چونک باز آمد این غراب گریخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق از خواب یک سؤالی کرد</p></div>
<div class="m2"><p>چون فروماند از جواب گریخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواب می‌بست شش جهت را در</p></div>
<div class="m2"><p>چون خدا کرد فتح باب گریخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس تبریز از خیالت خواب</p></div>
<div class="m2"><p>چون خطاییست کز صواب گریخت</p></div></div>