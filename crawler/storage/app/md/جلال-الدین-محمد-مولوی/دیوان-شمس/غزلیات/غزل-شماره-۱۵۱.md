---
title: >-
    غزل شمارهٔ ۱۵۱
---
# غزل شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>سر برون کن از دریچه جان ببین عشاق را</p></div>
<div class="m2"><p>از صبوحی‌های شاه آگاه کن فساق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عنایت‌های آن شاه حیات انگیز ما</p></div>
<div class="m2"><p>جان نو ده مر جهاد و طاعت و انفاق ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون عنایت‌های ابراهیم باشد دستگیر</p></div>
<div class="m2"><p>سر بریدن کی زیان دارد دلا اسحاق را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طاق و ایوانی بدیدم شاه ما در وی چو ماه</p></div>
<div class="m2"><p>نقش‌ها می‌رست و می‌شد در نهان آن طاق را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غلبه جان‌ها در آن جا پشت پا بر پشت پا</p></div>
<div class="m2"><p>رنگ رخ‌ها بی‌زبان می‌گفت آن اذواق را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرد گشتی باز ذوق مستی و نقل و سماع</p></div>
<div class="m2"><p>چون بدیدندی به ناگه ماه خوب اخلاق را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بدید آن شاه ما بر در نشسته بندگان</p></div>
<div class="m2"><p>وان در از شکلی که نومیدی دهد مشتاق را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه ما دستی بزد بشکست آن در را چنانک</p></div>
<div class="m2"><p>چشم کس دیگر نبیند بند یا اغلاق را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پاره‌های آن در بشکسته سبز و تازه شد</p></div>
<div class="m2"><p>کنچ دست شه برآمد نیست مر احراق را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جامه جانی که از آب دهانش شسته شد</p></div>
<div class="m2"><p>تا چه خواهد کرد دست و منت دقاق را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن که در حبسش از او پیغام پنهانی رسید</p></div>
<div class="m2"><p>مست آن باشد نخواهد وعده اطلاق را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بوی جانش چون رسد اندر عقیم سرمدی</p></div>
<div class="m2"><p>زود از لذت شود شایسته مر اعلاق را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه جانست آن خداوند دل و سر شمس دین</p></div>
<div class="m2"><p>کش مکان تبریز شد آن چشمه رواق را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای خداوندا برای جانت در هجرم مکوب</p></div>
<div class="m2"><p>همچو گربه می‌نگر آن گوشت بر معلاق را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور نه از تشنیع و زاری‌ها جهانی پر کنم</p></div>
<div class="m2"><p>از فراق خدمت آن شاه من آفاق را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پرده صبرم فراق پای دارت خرق کرد</p></div>
<div class="m2"><p>خرق عادت بود اندر لطف این مخراق را</p></div></div>