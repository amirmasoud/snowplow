---
title: >-
    غزل شمارهٔ ۱۱۴۴
---
# غزل شمارهٔ ۱۱۴۴

<div class="b" id="bn1"><div class="m1"><p>ندا رسید به جان‌ها ز خسرو منصور</p></div>
<div class="m2"><p>نظر به حلقه مردان چه می‌کنید از دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آفتاب برآمد چه خفته‌اند این خلق</p></div>
<div class="m2"><p>نه روح عاشق روزست و چشم عاشق نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درون چاه ز خورشید روح روشن شد</p></div>
<div class="m2"><p>ز نور خارش پذرفت نیز دیده کور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجنب بر خود آخر که چاشتگاه شدست</p></div>
<div class="m2"><p>از آنک خفته چو جنبید خواب شد مهجور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو که خفته نیم ناظرم به صنع خدا</p></div>
<div class="m2"><p>نظر به صنع حجابست از چنان منظور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روان خفته اگر داندی که در خوابست</p></div>
<div class="m2"><p>از آنچ دیدی نی خوش شدی و نی رنجور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنانک روزی در خواب رفت گلخن تاب</p></div>
<div class="m2"><p>به خواب دید که سلطان شدست و شد مغرور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدید خود را بر تخت ملک وز چپ و راست</p></div>
<div class="m2"><p>هزار صف ز امیر و ز حاجب و دستور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان نشسته بر آن تخت او که پنداری</p></div>
<div class="m2"><p>در امر و نهی خداوند بد سنین و شهور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان غلغله و دار و گیر و بردابرد</p></div>
<div class="m2"><p>میان آن لمن الملک و عزت و شر و شور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درآمد از در گلخن به خشم حمامی</p></div>
<div class="m2"><p>زدش به پای که برجه نه مرده‌ای در گور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بجست و پهلوی خود نی خزینه دید و نه ملک</p></div>
<div class="m2"><p>ولی خزینه حمام سرد دید و نفور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخوان ز آخر یاسین که صیحه فاذا</p></div>
<div class="m2"><p>تو هم به بانگی حاضر شوی ز خواب غرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه خفته‌ایم ولیکن ز خفته تا خفته</p></div>
<div class="m2"><p>هزار مرتبه فرقست ظاهر و مستور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شهی که خفت ز شاهی خود بود غافل</p></div>
<div class="m2"><p>خسی که خفت ز ادبیر خود بود معذور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو هر دو باز از این خواب خویش بازآیند</p></div>
<div class="m2"><p>به تخت آید شاه و به تخته آن مقهور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لباب قصه بماندست و گفت فرمان نیست</p></div>
<div class="m2"><p>نگر به دانش داوود و کوتهی زبور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگر که لطف کند باز شمس تبریزی</p></div>
<div class="m2"><p>وگر نه ماند سخن در دهن چنین مقصور</p></div></div>