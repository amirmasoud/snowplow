---
title: >-
    غزل شمارهٔ ۲۷۲۹
---
# غزل شمارهٔ ۲۷۲۹

<div class="b" id="bn1"><div class="m1"><p>با این همه مهر و مهربانی</p></div>
<div class="m2"><p>دل می‌دهدت که خشم رانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین جمله شیشه خانه‌ها را</p></div>
<div class="m2"><p>درهم شکنی به لن ترانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زلزله است دار دنیا</p></div>
<div class="m2"><p>کز خانه تو رخت می‌کشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نالان تو صد هزار رنجور</p></div>
<div class="m2"><p>بی تو نزیند هین تو دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دنیا چو شب و تو آفتابی</p></div>
<div class="m2"><p>خلقان همه صورت و تو جانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند که غافلند از جان</p></div>
<div class="m2"><p>در مکسبه و غم امانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اما چون جان ز جا بجنبد</p></div>
<div class="m2"><p>آغاز کنند نوحه خوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید چو در کسوف آید</p></div>
<div class="m2"><p>نی عیش بود نه شادمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا هست از او به یاد نارند</p></div>
<div class="m2"><p>ای وای چو او شود نهانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای رونق رزم و جان بازار</p></div>
<div class="m2"><p>شیرینی خانه و دکانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاموش که گفت و گو حجابند</p></div>
<div class="m2"><p>از بحر معلق معانی</p></div></div>