---
title: >-
    غزل شمارهٔ ۱۵۰۲
---
# غزل شمارهٔ ۱۵۰۲

<div class="b" id="bn1"><div class="m1"><p>ز زندان خلق را آزاد کردم</p></div>
<div class="m2"><p>روان عاشقان را شاد کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهان اژدها را بردریدم</p></div>
<div class="m2"><p>طریق عشق را آباد کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آبی من جهانی برتنیدم</p></div>
<div class="m2"><p>پس آنگه آب را پرباد کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببستم نقش‌ها بر آب کان را</p></div>
<div class="m2"><p>نه بر عاج و نه بر شمشاد کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شادی نقش خود جان می دراند</p></div>
<div class="m2"><p>که من نقش خودش میعاد کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چاهی یوسفان را برکشیدم</p></div>
<div class="m2"><p>که از یعقوب ایشان یاد کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خسرو زلف شیرینان گرفتم</p></div>
<div class="m2"><p>اگر قصد یکی فرهاد کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهی باغی که من ترتیب کردم</p></div>
<div class="m2"><p>زهی شهری که من بنیاد کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان داند که تا من شاه اویم</p></div>
<div class="m2"><p>بدادم داد ملک و داد کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان داند که بیرون از جهانم</p></div>
<div class="m2"><p>تصور بهر استشهاد کردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه استادان که من شهمات کردم</p></div>
<div class="m2"><p>چه شاگردان که من استاد کردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسا شیران که غریدند بر ما</p></div>
<div class="m2"><p>چو روبه عاجز و منقاد کردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خمش کن آنک او از صلب عشق است</p></div>
<div class="m2"><p>بسستش اینک من ارشاد کردم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ولیک آن را که طوفان بلا برد</p></div>
<div class="m2"><p>فروشد گر چه من فریاد کردم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر از قعر طوفانش برآرم</p></div>
<div class="m2"><p>چنانک نیست را ایجاد کردم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برآمد شمس تبریزی بزد تیغ</p></div>
<div class="m2"><p>زبان از تیغ او پولاد کردم</p></div></div>