---
title: >-
    غزل شمارهٔ ۱۴۰۵
---
# غزل شمارهٔ ۱۴۰۵

<div class="b" id="bn1"><div class="m1"><p>میل هواش می کنم طال بقاش می زنم</p></div>
<div class="m2"><p>حلقه به گوش و عاشقم طبل وفاش می زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل و جان شکسته‌ام بر سر ره نشسته‌ام</p></div>
<div class="m2"><p>قافله خیال را بهر لقاش می زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر طواشی غمش یا یلواج مرهمش</p></div>
<div class="m2"><p>هر چه سری برون کند بر سر و پاش می زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دل همچو چنگ را مست خراب دنگ را</p></div>
<div class="m2"><p>زخمه به کف گرفته‌ام همچو سه تاش می زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل که خرید جوهری از تک حوض کوثری</p></div>
<div class="m2"><p>خفت و بها نمی‌دهد بهر بهاش می زنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب چو به خواب می رود گوش کشانش می کشم</p></div>
<div class="m2"><p>چون به سحر دعا کند وقت دعاش می زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لذت تازیانه‌ام کی برسد به لاشه‌اش</p></div>
<div class="m2"><p>چون که گمان برد که من بهر فناش می زنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر قمر و فلک بود ور خرد و ملک بود</p></div>
<div class="m2"><p>چونک حجاب دل شود زود قفاش می زنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم شیشه مرا بر سر سنگ می زنی</p></div>
<div class="m2"><p>گفت چو لاف عشق زد تیغ بلاش می زنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر رگ این رباب را ناله نو نوای نو</p></div>
<div class="m2"><p>تا ز نواش پی برد دل که کجاش می زنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دل هر فغان او چاشنی سرشته‌ام</p></div>
<div class="m2"><p>تا نبری گمان که من سهو و خطاش می زنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خشم شهان گه عطا خنجر و گرز می زند</p></div>
<div class="m2"><p>من به سخاش می کشم من به عطاش می زنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخت لطیف می زنم دیده بدان نمی‌رسد</p></div>
<div class="m2"><p>دل که هوای ما کند همچو هواش می زنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خامش باش زین حنین پرده راست نیست این</p></div>
<div class="m2"><p>راه شماست این نوا پیش شماش می زنم</p></div></div>