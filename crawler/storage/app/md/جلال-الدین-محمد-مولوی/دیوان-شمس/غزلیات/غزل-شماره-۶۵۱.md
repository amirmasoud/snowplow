---
title: >-
    غزل شمارهٔ ۶۵۱
---
# غزل شمارهٔ ۶۵۱

<div class="b" id="bn1"><div class="m1"><p>مهتاب برآمد کلک از گور برآمد</p></div>
<div class="m2"><p>وز ریگ سیه چرده سقنقور برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنک از قلمش موسی و عیسیست مصور</p></div>
<div class="m2"><p>از نفخه او دمدمه صور برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هاون اقبال عنایت گهری کوفت</p></div>
<div class="m2"><p>صد دیده حق بین ز دل کور برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تف بهاری چه خبر یافت دل خاک</p></div>
<div class="m2"><p>کز خاک سیه قافله مور برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بحر عسل‌هاش چه دید آن دل زنبور</p></div>
<div class="m2"><p>با مشک عسل گله زنبور برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مخزن او کرم ضعیفی به چه ره یافت</p></div>
<div class="m2"><p>کز وی خز و ابریشم موفور برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی دیده و بی‌گوش صدف رزق کجا یافت</p></div>
<div class="m2"><p>تا حاصل در گشت و چو گنجور برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرم آهن و سنگی سوی انوار چه ره یافت</p></div>
<div class="m2"><p>کز آهن و سنگی علم نور برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنگر که ز گلزار چه گلزار بخندید</p></div>
<div class="m2"><p>وز سرمه چون قیر چه کافور برآمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی غازه و گلگونه گل آن رنگ کجا یافت</p></div>
<div class="m2"><p>کافروخته از پرده مستور برآمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دولت و در عزت آن شاه نکوکار</p></div>
<div class="m2"><p>این لشگر بشکسته چه منصور برآمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک سیب بنی دیدم در باغ جمالش</p></div>
<div class="m2"><p>هر سیب که بشکافت از او حور برآمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون حور برآمد ز دل سیب بخندید</p></div>
<div class="m2"><p>از خنده او حاجت رنجور برآمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این هستی و این مستی و این جنبش مستان</p></div>
<div class="m2"><p>زان باده مدان کز دل انگور برآمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمس الحق تبریز چو این شور برانگیخت</p></div>
<div class="m2"><p>از مشرق جان آن مه مشهور برآمد</p></div></div>