---
title: >-
    غزل شمارهٔ ۲۹۱۸
---
# غزل شمارهٔ ۲۹۱۸

<div class="b" id="bn1"><div class="m1"><p>می‌زنم حلقهٔ درِ هر خانه‌ای</p></div>
<div class="m2"><p>هست در کوی شما دیوانه‌ای؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ جان دیوانهٔ آن دام شد</p></div>
<div class="m2"><p>دام عشق دلبری دُردانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل‌ها نعره‌زنان کآخر کجاست</p></div>
<div class="m2"><p>در جنونْ دریادلی مردانه‌ای؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خدا مجنون آن لیلی کجاست؟</p></div>
<div class="m2"><p>تا به گوشش دردمیم افسانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه گوش عقل نامحرم بُوَد</p></div>
<div class="m2"><p>از فسون عاشقان بیگانه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلسله‌زلفی که جان مجنون او است</p></div>
<div class="m2"><p>میل دارد با شکسته شانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهر ما پرفتنه و پرشور شد</p></div>
<div class="m2"><p>الغیاث از فتنهٔ فَتّانه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زوتر ای قَفّال مِفتاحی بساز</p></div>
<div class="m2"><p>کز فرج باشد ورا دندانه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هین! خِمَش کن کژ مرو فرزین نه‌ای</p></div>
<div class="m2"><p>کی چو فرزین کژ رود فرزانه‌ای</p></div></div>