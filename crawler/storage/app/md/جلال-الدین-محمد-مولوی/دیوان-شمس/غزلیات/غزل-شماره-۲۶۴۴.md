---
title: >-
    غزل شمارهٔ ۲۶۴۴
---
# غزل شمارهٔ ۲۶۴۴

<div class="b" id="bn1"><div class="m1"><p>یک روز مرا بر لب خود میر نکردی</p></div>
<div class="m2"><p>وز لعل لبت جامگی تقریر نکردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان شب که سر زلف تو در خواب بدیدم</p></div>
<div class="m2"><p>حیران و پریشانم و تعبیر نکردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک عالم و عاقل به جهان نیست که او را</p></div>
<div class="m2"><p>دیوانه آن زلف چو زنجیر نکردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگریست بسی از غم تو طفل دو چشمم</p></div>
<div class="m2"><p>وز سنگ دلی در دهنش شیر نکردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کعبه خوبی تو احرام ببستیم</p></div>
<div class="m2"><p>بس تلبیه گفتیم و تو تکبیر نکردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگرفت دلم در غمت ای سرو جوان بخت</p></div>
<div class="m2"><p>شد پیر دلم پیروی پیر نکردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با قوس دو ابروی تو یک دل به جهان نیست</p></div>
<div class="m2"><p>تا خسته بدان غمزه چون تیر نکردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس عقل که در آیت حسن تو فروماند</p></div>
<div class="m2"><p>وز وی به کرم روزی تفسیر نکردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بردن جان‌ها و در آزردن جان‌ها</p></div>
<div class="m2"><p>الحق صنما هیچ تو تقصیر نکردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کشتنم ای دلبر خون خوار بکردم</p></div>
<div class="m2"><p>صد لابه و یک ساعت تأخیر نکردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آتش عشق تو دلم سوخت به یک بار</p></div>
<div class="m2"><p>وز بهر دوا قرص تباشیر نکردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیمار شدم از غم هجر تو و روزی</p></div>
<div class="m2"><p>از بهر من خسته تو تدبیر نکردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خورشید رخت با زحل زلف سیاهت</p></div>
<div class="m2"><p>صد بار قران کرد و تو تأثیر نکردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر خاک درت روی نهادم ز سر عجز</p></div>
<div class="m2"><p>وز قصه هجرانم تحریر نکردی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خامش شوم و هیچ نگویم پس از این من</p></div>
<div class="m2"><p>هر چاکر دیرینه چو توفیر نکردی</p></div></div>