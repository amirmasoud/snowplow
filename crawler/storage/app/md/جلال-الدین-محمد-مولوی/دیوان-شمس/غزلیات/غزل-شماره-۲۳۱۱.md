---
title: >-
    غزل شمارهٔ ۲۳۱۱
---
# غزل شمارهٔ ۲۳۱۱

<div class="b" id="bn1"><div class="m1"><p>از انبهی ماهی دریا به نهان گشته</p></div>
<div class="m2"><p>انبه شده قالب‌ها تا پرده جان گشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فرقت آن دریا چون زهر شده شکر</p></div>
<div class="m2"><p>زهر از هوس دریا آب حیوان گشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشرت آن دریا نی این و نه آن بوده</p></div>
<div class="m2"><p>بر ساحل این خشکی این گشته و آن گشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر هوس دریا ای جان چو مرغابی</p></div>
<div class="m2"><p>چندان تو چنین گفته کز عشق چنان گشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش از شکم دریا برخاست یکی صورت</p></div>
<div class="m2"><p>و آن غمزه‌اش از دریا بس سخته کمان گشته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل گفت به زیر لب من جان نبرم از وی</p></div>
<div class="m2"><p>سوگند به جان دل کان کار چنان گشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از غمزه غمازی وز طرفه بغدادی</p></div>
<div class="m2"><p>دل گشته چنان شادی جانم همدان گشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بیشه درافتاده در نیم شبی آتش</p></div>
<div class="m2"><p>در پختن این شیران تا مغز پزان گشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شعله آن بیشه تابان شده اندیشه</p></div>
<div class="m2"><p>تا قالب جان پیشه بی‌جا و مکان گشته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرمابه روحانی آوخ چه پری خوان است</p></div>
<div class="m2"><p>وین عالم گورستان چون جامه کنان گشته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بهر چنین سری در سوسن‌ها بنگر</p></div>
<div class="m2"><p>دستوری گفتن نی سر جمله زبان گشته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شمس الحق تبریزی درتافته از روزن</p></div>
<div class="m2"><p>تا آنچ نیارم گفت چون ماه عیان گشته</p></div></div>