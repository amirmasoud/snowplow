---
title: >-
    غزل شمارهٔ ۱۹۷۹
---
# غزل شمارهٔ ۱۹۷۹

<div class="b" id="bn1"><div class="m1"><p>عاشقان را مژده‌ای از سرفراز راستین</p></div>
<div class="m2"><p>مژده مر دل را هزار از دلنواز راستین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژده مر کان‌های زر را از برای خالصیش</p></div>
<div class="m2"><p>هست نقاد بصیر و هست گاز راستین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مژده مر کسوه بقا را کز پی عمر ابد</p></div>
<div class="m2"><p>هستش از اقبال و دولت‌ها طراز راستین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرخا زاغی که در زاغی نماند بعد از این</p></div>
<div class="m2"><p>پیش شمس الدین درآید گشت باز راستین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حبذا دستی که او بستم درازی کم کند</p></div>
<div class="m2"><p>دست در فتراک او زد شد دراز راستین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد دراز آن دست او تا بگذرید او را ختن</p></div>
<div class="m2"><p>تا گرفت از جیب معشوقی طراز راستین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد از آن خوب طرازی چون شود همدست او</p></div>
<div class="m2"><p>دو به دو چون مست گشته گفته راز راستین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم بگشاید ببیند از ورای وهم و روح</p></div>
<div class="m2"><p>آنک بر ترک طرازی کرد ناز راستین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه تبریزی کریمی روح بخشی کاملی</p></div>
<div class="m2"><p>در فرازی در وصال و ملک باز راستین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک جانی‌ها نه ملک فانیی جسمانیی</p></div>
<div class="m2"><p>تا شود جان‌ها ز ملکش چشم باز راستین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرحبا ای شاه جان‌ها مرحبا ای فر و حسن</p></div>
<div class="m2"><p>ملک بخش بندگان و کارساز راستین</p></div></div>