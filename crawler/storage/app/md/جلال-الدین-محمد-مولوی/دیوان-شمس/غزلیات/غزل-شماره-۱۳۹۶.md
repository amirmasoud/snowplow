---
title: >-
    غزل شمارهٔ ۱۳۹۶
---
# غزل شمارهٔ ۱۳۹۶

<div class="b" id="bn1"><div class="m1"><p>باز در اسرار روم جانب آن یار روم</p></div>
<div class="m2"><p>نعره بلبل شنوم در گل و گلزار روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی از این شرم و حیا شرم بسوزان و بیا</p></div>
<div class="m2"><p>همره دل گردم خوش جانب دلدار روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر نمانده‌ست که من گوش سوی نسیه برم</p></div>
<div class="m2"><p>عقل نمانده‌ست که من راه به هنجار روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنگ زن ای زهره من تا که بر این تنتن تن</p></div>
<div class="m2"><p>گوش بر این بانگ نهم دیده به دیدار روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسته دام است دلم بر در و بام است دلم</p></div>
<div class="m2"><p>شاهد دل را بکشم سوی خریدار روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت مرا در چه فنی کار چرا می نکنی</p></div>
<div class="m2"><p>راه دکانم بنما تا که پس کار روم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که ز خود بد خبرش رفت دلم بر اثرش</p></div>
<div class="m2"><p>کو اثری از دل من تا که بر آثار روم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ز حریفان حسد چشم بدی درنرسد</p></div>
<div class="m2"><p>کف به کف یار دهم در کنف غار روم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درس رئیسان خوشی بی‌هشی است و خمشی</p></div>
<div class="m2"><p>درس چو خام است مرا بر سر تکرار روم</p></div></div>