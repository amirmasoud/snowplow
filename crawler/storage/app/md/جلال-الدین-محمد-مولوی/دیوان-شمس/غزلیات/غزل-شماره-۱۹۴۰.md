---
title: >-
    غزل شمارهٔ ۱۹۴۰
---
# غزل شمارهٔ ۱۹۴۰

<div class="b" id="bn1"><div class="m1"><p>ای ز تو مه پای کوبان وز تو زهره دف زنان</p></div>
<div class="m2"><p>می زنند ای جان مردان عشق ما بر دف زنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقل هر مجلس شده‌ست این عشق ما و حسن تو</p></div>
<div class="m2"><p>شهره شهری شده ما کو چنین بد شد چنان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای به هر هنگامه دام عشق تو هنگامه گیر</p></div>
<div class="m2"><p>وی چکیده خون ما بر راه ره رو را نشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد هزاران زخم بر سینه ز زخم تیر عشق</p></div>
<div class="m2"><p>صد شکار خسته و نی تیر پیدا نی کمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی در دیوار کرده در غم تو مرد و زن</p></div>
<div class="m2"><p>ز آب و نان عشق رفته اشتهای آب و نان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون عاشق اشک شد وز اشک او سبزه برست</p></div>
<div class="m2"><p>سبزه‌ها از عکس روی چون گل تو گلستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذوق عشقت چون ز حد شد خلق آتشخوار شد</p></div>
<div class="m2"><p>همچو اشترمرغ آتش می خورد در عشق جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هجر سرد چون زمستان راه‌ها را بسته بود</p></div>
<div class="m2"><p>در زمین محبوس بود اشکوفه‌های بوستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونک راه ایمن شد از داد بهاران آمدند</p></div>
<div class="m2"><p>سبزه را تیغ برهنه غنچه را در کف سنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیز بیرون آ به بستان کز ره دور آمدند</p></div>
<div class="m2"><p>خیز کالقادم یزار و رنجه شو مرکب بران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از عدم بستند رخت و جانب بحر آمدند</p></div>
<div class="m2"><p>آنگه از بحر آمدند اندر هوا تا آسمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برج برج آسمان را گشته و پذرفته اند</p></div>
<div class="m2"><p>از هر استاره بضاعت و آمده تا خاکدان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آب و آتش ز آسمانش می رسد هر دم مدد</p></div>
<div class="m2"><p>چند روزی کاندر این خاکند ایشان میهمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوان‌ها بر سر نسیم و کاس‌ها بر کف صبا</p></div>
<div class="m2"><p>با طبق پوشی که پوشیده‌ست جز از اهل خوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می رسند و هر کسی پرسان که چیست اندر طبق</p></div>
<div class="m2"><p>با زبان حال می گویند با پرسندگان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کسی گر محرمستی پس طبق پوشیده چیست</p></div>
<div class="m2"><p>قوت جان چون جان نهان و قوت تن پیدا چو نان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ذوق نان هم گرسنه بیند نبیند هیچ سیر</p></div>
<div class="m2"><p>بر دکان نانبا از نان چه می داند دکان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نانوا گر گرسنه ستی هیچ نان نفروختی</p></div>
<div class="m2"><p>گر بدانستی صبا گل را نکردی گلفشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر کش از معشوق ذوقی نیست الا در فروخت</p></div>
<div class="m2"><p>او نباشد عاشق او باشد به معنی قلتبان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عذر عاشق گر فروشد دانک میل دلبر است</p></div>
<div class="m2"><p>از ضرورت تا نبندد در به رویش دلستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چونک می بیند که میل دلبر اندر شهرگی است</p></div>
<div class="m2"><p>اشک می بارد ز رشک آن صنم از دیدگان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اشک او مر رشک او را ضد و دشمن آمده‌ست</p></div>
<div class="m2"><p>رشک پنهان دارد و اشکش روان و قصه خوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تخم پنهان کرده خود را نگر باغ و چمن</p></div>
<div class="m2"><p>شهوت پنهان خود را بین یکی شخصی دوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عین پنهان داشتن شد علت پیدا شدن</p></div>
<div class="m2"><p>بی لسانی می شود بر رغم ما عین لسان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چند فرزندان به هر اندیشه بعد مرگ خویش</p></div>
<div class="m2"><p>گرد جان خویش بینی در لحد باباکنان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زاده از اندیشه‌های خوب تو ولدان و حور</p></div>
<div class="m2"><p>زاده از اندیشه‌های زشت تو دیو کلان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر اندیشه مهندس بین شده قصر و سرا</p></div>
<div class="m2"><p>سر تقدیر ازل را بین شده چندین جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>واقفی از سر خود از سر سر واقف نه‌ای</p></div>
<div class="m2"><p>سر سر همچون دل آمد سر تو همچون زبان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر سر تو هست خوب از سر سر ایمن مباش</p></div>
<div class="m2"><p>باش ناایمن که ناایمن همی‌یابد امان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سربلندی سرو و خنده گل نوای عندلیب</p></div>
<div class="m2"><p>میوه‌های گرم رو سر دم سرد خزان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برگ‌ها لرزان چه می لرزید وقت شادی است</p></div>
<div class="m2"><p>دام‌ها در دانه‌های خوش بود ای باغبان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ما ز سرسبزی به روی زرد چند افتاده‌ایم</p></div>
<div class="m2"><p>در کمین غیب بس تیر است پران از کمان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لاله رخ افروخته وز خشم شد دل سوخته</p></div>
<div class="m2"><p>سنبله پرسود و کژگردن ز اندیشه گران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن گل سوری ستیزه گل دکانی باز کرد</p></div>
<div class="m2"><p>رنگ‌ها آمیخت اما نیستش بویی از آن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خوشه‌ها از سست پایی رو نهاده بر زمین</p></div>
<div class="m2"><p>غوره‌اش شیرین شد آخر از خطاب یسجدان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نرگس خیره نگر آخر چه می بینی به باغ</p></div>
<div class="m2"><p>گفت غمازی کنم پس من نگنجم در میان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سوسنا افسوس می داری زبان کردی برون</p></div>
<div class="m2"><p>یا زبان درکش چو ما و یا بکن حالی بیان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفت بی‌گفتن زبان ما بیان حال ماست</p></div>
<div class="m2"><p>گر نه پایان راسخستی سبز کی بودی سران</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفتم ای بید پیاده چون پیاده رسته‌ای</p></div>
<div class="m2"><p>گفت تا لطف تواضع گیرم از آب روان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رنگ معشوق است سیب لعل را طعم ترش</p></div>
<div class="m2"><p>زانک خوبان را ترش بودن بزیبد این بدان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس درخت و شاخ شفتالو چرا پستی نمود</p></div>
<div class="m2"><p>بهر شفتالو فشاندن پیش شفتالوستان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت آری لیک وقتی می دهد شفتالویی</p></div>
<div class="m2"><p>که رسد جان از تن عاشق ز ناخن تا دهان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای سپیدار این بلندی جستنت رسوایی است</p></div>
<div class="m2"><p>چون نه گل داری نه میوه گفت خامش هان و هان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر گلم بودی و میوه همچو تو خودبینمی</p></div>
<div class="m2"><p>فارغم از دید خود بر خودپرستان دیدبان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نار آبی را همی‌گفت این رخ زردت ز چیست</p></div>
<div class="m2"><p>گفت زان دردانه‌ها کاندر درون داری نهان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفت چون دانسته‌ای از سر من گفتا بدانک</p></div>
<div class="m2"><p>می نگنجی در خود و خندان نمایی ناردان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نی تو خندانی همیشه خواه خند و خواه نی</p></div>
<div class="m2"><p>وز تو خندان است عالم چون جنان اندر جنان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>لیک آن خنده چون برق او راست کو گرید چو ابر</p></div>
<div class="m2"><p>ابر اگر گریان نباشد برق از او نبود جهان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خاک را دیدم سیاه و تیره و روشن ضمیر</p></div>
<div class="m2"><p>آب روشن آمد از گردون و کردش امتحان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آب روشن را پذیرا شد ضمیر روشنش</p></div>
<div class="m2"><p>زاد چون فردوس و جنت شاخ و کاخ بی‌کران</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>این خیار و خربزه در راه دور و پای سست</p></div>
<div class="m2"><p>چون پیاده حاج می آیند اندر کاروان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بادیه خون خوار بینی از عدم سوی وجود</p></div>
<div class="m2"><p>بر خطاب کن همه لبیک گو بهر امان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چه پیاده بلک خفته رفته چون اصحاب کهف</p></div>
<div class="m2"><p>خفته پهلو بر زمین و رفته تک تا آسمان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در چنین مجمع کدو آمد رسن بازی گرفت</p></div>
<div class="m2"><p>از کی دید آن زو که دادش آن رسن‌های رسان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>این چمن‌ها وین سمن وین میوه‌ها خود رزق ماست</p></div>
<div class="m2"><p>آن گیا و خار و گل کاندر بیابان است آن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آن نصیب و میوه و روزی قومی دیگر است</p></div>
<div class="m2"><p>نفرت و بی‌میلی ما هست آن را پاسبان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>صد هزاران مور و مار و صد هزاران رزق خوار</p></div>
<div class="m2"><p>هر یکی جوید نصیبه هر یکی دارد فغان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هر دوا درمان رنجی هر یکی را طالبی</p></div>
<div class="m2"><p>چون عقاقیری که نشناسد به غیر طب دان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بس گیا کان پیش ما زهر و بر ایشان پای زهر</p></div>
<div class="m2"><p>پیش ما خار است و پیش اشتران خرمابنان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جوز و بادام از درون مغز است و بیرون پوست و قشر</p></div>
<div class="m2"><p>اندرون پوست پرورده چو بیضه ماکیان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>باز خرما عکس آن بیرون خوش و باطن قشور</p></div>
<div class="m2"><p>باطن و ظاهر تو چون انجیر باش ای مهربان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جذبه شاخ آب را از بیخ تا بالا کشد</p></div>
<div class="m2"><p>همچنانک جذبه جان را برکشد بی‌نردبان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>غوصه گشت این باد و آبستن شد آن خاک و درخت</p></div>
<div class="m2"><p>بادها چون گشن تازی شاخه‌ها چون مادیان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>می رسد هر جنس مرغی در بهار از گرمسیر</p></div>
<div class="m2"><p>همچو مهمان سرسری می سازد این جا آشیان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>صد هزاران غیب می گویند مرغان در ضمیر</p></div>
<div class="m2"><p>کان فلان خواهد گذشتن جای او گیرد فلان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از سلیمان نامه‌ها آورده‌اند این هدهدان</p></div>
<div class="m2"><p>کو زبان مرغ دانی تا شود او ترجمان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>عارف مرغان است لک لک لک لکش دانی که چیست</p></div>
<div class="m2"><p>ملک لک و الامر لک و الحمد لک یا مستعان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>وقت پیله روح آمد قشلق تن را بهل</p></div>
<div class="m2"><p>آخر از مرغان بیاموزید رسم ترکمان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همچو مرغان پاسبانی خویش کن تسبیح گو</p></div>
<div class="m2"><p>چند گاهی خود شود تسبیح تو تسبیح خوان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بس کنم زین باد پیمودن ولیکن چاره نیست</p></div>
<div class="m2"><p>زانک کشتی مجاهد کی رود بی‌بادبان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بادپیمایی بهار آمد حیات عالمی</p></div>
<div class="m2"><p>بادپیمایی خزان آمد عذاب انس و جان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>این بهار و باغ بیرون عکس باغ باطن است</p></div>
<div class="m2"><p>یک قراضه‌ست این همه عالم و باطن هست کان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>لاجرم ما هر چه می گوییم اندر نظم هست</p></div>
<div class="m2"><p>نزد عاشق نقد وقت و نزد عاقل داستان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>عقل دانایی است و نقلش نقل آمد یا قیاس</p></div>
<div class="m2"><p>عشق کان بینش آمد ز آفتاب کن فکان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>آفتابی کو مجرد آمد از برج حمل</p></div>
<div class="m2"><p>آفتابی بی‌نظیر بی‌قرین خوش قران</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>آنک لاشرقیه بوده‌ست و لاغربیه</p></div>
<div class="m2"><p>زانک شرق و غرب باشد در زمین و در زمان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>آفتابی کو نسوزد جز دل عشاق را</p></div>
<div class="m2"><p>مهر جان ره یابد آن جا نی ربیع و مهر جان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چونک ما را از زمین و از زمان بیرون برد</p></div>
<div class="m2"><p>از فنا ایمن شویم از جود او ما جاودان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>این زمین و این زمان بیضه‌ست و مرغی کاندر او است</p></div>
<div class="m2"><p>مظلم و اشکسته پر باشد حقیر و مستهان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کفر و ایمان دان در این بیضه سپید و زرده را</p></div>
<div class="m2"><p>واصل و فارق میانشان برزخ لایبغیان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بیضه را چون زیر پر خویش پرورد از کرم</p></div>
<div class="m2"><p>کفر و دین فانی شد و شد مرغ وحدت پرفشان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>شمس تبریزی دو عالم بود بی‌رویت عقیم</p></div>
<div class="m2"><p>هر یکی ذره کنون از آفتابت توامان</p></div></div>