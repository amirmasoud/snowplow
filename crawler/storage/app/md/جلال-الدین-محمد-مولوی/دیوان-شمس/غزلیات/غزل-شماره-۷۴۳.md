---
title: >-
    غزل شمارهٔ ۷۴۳
---
# غزل شمارهٔ ۷۴۳

<div class="b" id="bn1"><div class="m1"><p>آن زمانی را که چشم از چشم او مخمور بود</p></div>
<div class="m2"><p>چون رسیدش چشم بد کز چشم‌ها مستور بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی شب‌های ما کز مشک و عنبر پرده داشت</p></div>
<div class="m2"><p>شادی آن صبح‌ها کز یار پرکافور بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از فراز عرش و کرسی بانگ تحسین می‌رسید</p></div>
<div class="m2"><p>تا به پشت گاو و ماهی از رخش پرنور بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر طرف از حسن او بدلیلیی کاسد شده</p></div>
<div class="m2"><p>ذره ذره همچو مجنون عاشق مشهور بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل به پیش روی او چون بایزید اندر مزید</p></div>
<div class="m2"><p>جان در آویزان ز زلفش شیوه منصور بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع عشق افروز را یک بار دیگر اندرآر</p></div>
<div class="m2"><p>کوری آن کس که او از عشرت ما دور بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقیی با رطل آمد مر مرا از کار برد</p></div>
<div class="m2"><p>تا ز مستی من ندانستم که رشک حور بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقش شمس الدین تبریزیست جان جان عشق</p></div>
<div class="m2"><p>کاین به دفترهای عشق اندر ازل مسطور بود</p></div></div>