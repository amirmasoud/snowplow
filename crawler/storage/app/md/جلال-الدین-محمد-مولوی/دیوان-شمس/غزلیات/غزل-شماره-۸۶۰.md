---
title: >-
    غزل شمارهٔ ۸۶۰
---
# غزل شمارهٔ ۸۶۰

<div class="b" id="bn1"><div class="m1"><p>ای دل اگر کم آیی کارت کمال گیرد</p></div>
<div class="m2"><p>مرغت شکار گردد صید حلال گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه می‌دود چو آیی در ظل آفتابی</p></div>
<div class="m2"><p>بدری شود اگر چه شکل هلال گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل مقام سازد همچون خیال آن کس</p></div>
<div class="m2"><p>کاندر ره حقیقت ترک خیال گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو آن خلیل گویا وجهت وجه حقا</p></div>
<div class="m2"><p>وان جان گوشمالی کو پای مال گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این گنده پیر دنیا چشمک زند ولیکن</p></div>
<div class="m2"><p>مر چشم روشنان را از وی ملال گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر در برم کشد او از ساحری و شیوه</p></div>
<div class="m2"><p>اندر برش دل من کی پر و بال گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلگونه کرده است او تا روی چون گلم را</p></div>
<div class="m2"><p>بویش تباه گردد رنگش زوال گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخ بر رخش منه تو تا رویت از شهنشه</p></div>
<div class="m2"><p>مانند آفتابی نور جلال گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه جای آفتابی کز پرتو جمالش</p></div>
<div class="m2"><p>صد آفتاب و مه را بر چرخ حال گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شویان اولینش بنگر که در چه حالند</p></div>
<div class="m2"><p>آن کاین دلیل داند نی آن دلال گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای صد هزار عاقل او در جوال کرده</p></div>
<div class="m2"><p>کو عقل کاملی تا ترک جوال گیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خطی نوشت یزدان بر خد خوش عذاران</p></div>
<div class="m2"><p>کز خط سیه‌تر است او کاین خط و خال گیرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ابر خط برون آ وز خال و عم جدا شو</p></div>
<div class="m2"><p>تا مه ز طلعت تو هر شام فال گیرد</p></div></div>