---
title: >-
    غزل شمارهٔ ۲۸۸۶
---
# غزل شمارهٔ ۲۸۸۶

<div class="b" id="bn1"><div class="m1"><p>اگر امشب بر من باشی و خانه نروی</p></div>
<div class="m2"><p>یا علی شیر خدا باشی یا خود علوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندک اندک به جنون راه بری از دم من</p></div>
<div class="m2"><p>برهی از خرد و ناگه دیوانه شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کهنه و پیر شدی زین خرد پیر گریز</p></div>
<div class="m2"><p>تا بهار تو نماید گل و گلزار نوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خیالی به من آیی به خیالی بروی</p></div>
<div class="m2"><p>این چه رسوایی و ننگ است زهی بند قوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ترازوی زر ار راه دهندت غلط است</p></div>
<div class="m2"><p>بجوی زر بنه ارزی چو همان حب جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیک لابد بدود کیک چو او هم بدود</p></div>
<div class="m2"><p>پس کمال تو در آن نیست که یاوه بدوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر بردن بدو از هیبت مردن بمدو</p></div>
<div class="m2"><p>بهر کعبه بدو ای جان نه ز خوف بدوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باش شب‌ها بر من تا به سحر تا که شبی</p></div>
<div class="m2"><p>مه برآید برهی از ره و همراه غوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه کس بیند رخساره مه را از دور</p></div>
<div class="m2"><p>خنک آن کس که برد از بغل مه گروی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مه ز آغاز چو خورشید بسی تیغ کشد</p></div>
<div class="m2"><p>که ببرم سر تو گر تو از این جا نروی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ببیند که سر خویش نمی‌گیرد او</p></div>
<div class="m2"><p>گوید او را که حریفی و ظریفی و روی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من توام ور تو نیم یار شب و روز توام</p></div>
<div class="m2"><p>پدر و مادر و خویش تو به منهاج سوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه شود گر من و تو بی‌من و تو جمع شویم</p></div>
<div class="m2"><p>فرد باشیم و یکی کوری چشم ثنوی</p></div></div>