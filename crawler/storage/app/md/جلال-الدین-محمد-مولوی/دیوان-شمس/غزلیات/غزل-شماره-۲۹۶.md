---
title: >-
    غزل شمارهٔ ۲۹۶
---
# غزل شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>مخسب ای یار مهمان دار امشب</p></div>
<div class="m2"><p>که تو روحی و ما بیمار امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون کن خواب را از چشم اسرار</p></div>
<div class="m2"><p>که تا پیدا شود اسرار امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو مشترییی گرد مه گرد</p></div>
<div class="m2"><p>بگرد گنبد دوار امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکار نسر طایر را به گردون</p></div>
<div class="m2"><p>چو جان جعفر طیار امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را حق داد صیقل تا زدایی</p></div>
<div class="m2"><p>ز هجر ازرق زنگار امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحمدالله که خلقان جمله خفتند</p></div>
<div class="m2"><p>و من بر خالقم بر کار امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهی کر و فر و اقبال بیدار</p></div>
<div class="m2"><p>که حق بیدار و ما بیدار امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چشمم بخسبد تا سحرگه</p></div>
<div class="m2"><p>ز چشم خود شوم بیزار امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر بازار خالی شد تو بنگر</p></div>
<div class="m2"><p>به راه کهکشان بازار امشب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب ما روز آن استارگان‌ست</p></div>
<div class="m2"><p>که درتابید در دیدار امشب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اسد بر ثور برتازد به جمله</p></div>
<div class="m2"><p>عطارد برنهد دستار امشب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زحل پنهان بکارد تخم فتنه</p></div>
<div class="m2"><p>بریزد مشتری دینار امشب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خمش کردم زبان بستم ولیکن</p></div>
<div class="m2"><p>منم گویای بی‌گفتار امشب</p></div></div>