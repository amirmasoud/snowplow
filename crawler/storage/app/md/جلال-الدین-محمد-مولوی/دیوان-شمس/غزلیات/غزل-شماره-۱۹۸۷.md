---
title: >-
    غزل شمارهٔ ۱۹۸۷
---
# غزل شمارهٔ ۱۹۸۷

<div class="b" id="bn1"><div class="m1"><p>هله نیم مست گشتم قدحی دگر مدد کن</p></div>
<div class="m2"><p>چو حریف نیک داری تو به ترک نیک و بد کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منگر که کیست گریان ز جفا و کیست عریان</p></div>
<div class="m2"><p>نه وصی آدمی تو بنشین و کار خود کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظری به سوی می کن به نوای چنگ و نی کن</p></div>
<div class="m2"><p>نظری دگر به سوی رخ یار سروقد کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکرت چو آرزو شد ز لب شکرفروشش</p></div>
<div class="m2"><p>چو عباس دبس زودتر ز شکرفروش کدکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه که کودکم که میلم به مویز و جوز باشد</p></div>
<div class="m2"><p>تو مویز و جوز خود را بستان در آن سبد کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر خوش تبرزد که هزار جان به ارزد</p></div>
<div class="m2"><p>حسد ار کنی تو باری پی آن شکر حسد کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بت شکرفشان شو ز لبش شکرستان شو</p></div>
<div class="m2"><p>جهت قران ماهش چو منجمان رصد کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو رسید ماه روزه نه ز کاسه گو نه کوزه</p></div>
<div class="m2"><p>پس از این نشاط و مستی ز صراحی ابد کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سماع و طوی بنشین به میان کوی بنشین</p></div>
<div class="m2"><p>که کسی خورت نبیند طرب از می احد کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو عروس جان ز مستی برسد به کوی هستی</p></div>
<div class="m2"><p>خورشش از این طبق ده تتقش هم از خرد کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سخن ملول گشتی که کسیت نیست محرم</p></div>
<div class="m2"><p>سبک آینه بیان را تو بگیر و در نمد کن</p></div></div>