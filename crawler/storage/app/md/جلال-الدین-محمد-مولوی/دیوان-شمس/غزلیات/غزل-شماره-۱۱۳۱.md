---
title: >-
    غزل شمارهٔ ۱۱۳۱
---
# غزل شمارهٔ ۱۱۳۱

<div class="b" id="bn1"><div class="m1"><p>گفت لبم چون شکر ارزد گنج گهر</p></div>
<div class="m2"><p>آه ندارم گهر گفت نداری بخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گهرم دام کن ور نبود وام کن</p></div>
<div class="m2"><p>خانه غلط کرده‌ای عاشق بی‌سیم و زر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمده‌ای در قمار کیسه پرزر بیار</p></div>
<div class="m2"><p>ور نه برو از کنار غصه و زحمت ببر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه زنانیم ما جامه کنانیم ما</p></div>
<div class="m2"><p>گر تو ز مایی درآ کاسه بزن کوزه خور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام همه ما دریم مال همه ما خوریم</p></div>
<div class="m2"><p>از همه ما خوشتریم کوری هر کور و کر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامه خران دیگرند جامه دران دیگرند</p></div>
<div class="m2"><p>جامه دران برکنند سبلت هر جامه خر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبلت فرعون تن موسی جان برکند</p></div>
<div class="m2"><p>تا همه تن جان شود هر سر مو جانور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ره عشاق او روی معصفر شناس</p></div>
<div class="m2"><p>گوهر عشق اشک دان اطلس خون جگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قیمت روی چو زر چیست بگو لعل یار</p></div>
<div class="m2"><p>قیمت اشک چو در چیست بگو آن نظر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنده آن ساقیم تا به ابد باقیم</p></div>
<div class="m2"><p>عالم ما برقرار عالمیان برگذر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کی بزاد او بمرد جان به موکل سپرد</p></div>
<div class="m2"><p>عاشق از کس نزاد عشق ندارد پدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو از این رو نه‌ای همچو قفا پس نشین</p></div>
<div class="m2"><p>ور تو قفا نیستی پیش درآ چون سپر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون سپر بی‌خبر پیش درآ و ببین</p></div>
<div class="m2"><p>از نظر زخم دوست باخبران بی‌خبر</p></div></div>