---
title: >-
    غزل شمارهٔ ۲۵۷۳
---
# غزل شمارهٔ ۲۵۷۳

<div class="b" id="bn1"><div class="m1"><p>در پرده خاک ای جان عیشی است به پنهانی</p></div>
<div class="m2"><p>و اندر تتق غیبی صد یوسف کنعانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این صورت تن رفته و آن صورت جا مانده</p></div>
<div class="m2"><p>ای صورت جان باقی وی صورت تن فانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چاشنیی خواهی هر شب بنگر خود را</p></div>
<div class="m2"><p>تن مرده و جان پران در روضه رضوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عشق که آن داری یا رب چه جهان داری</p></div>
<div class="m2"><p>چندان صفتت کردم والله که دو چندانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>المؤمن حلوی و العاش علوی</p></div>
<div class="m2"><p>با تو چه زبان گویم ای جان که نمی‌دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندان بدوان لنگان کاین پای فروماند</p></div>
<div class="m2"><p>وآنگه رسد از سلطان صد مرکب میدانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می مرد یکی عاشق می‌گفت یکی او را</p></div>
<div class="m2"><p>در حالت جان کندن چون است که خندانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتا چو بپردازم من جمله دهان گردم</p></div>
<div class="m2"><p>صدمرده همی‌خندم بی‌خنده دندانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیرا که یکی نیمم نی بود شکر گشتم</p></div>
<div class="m2"><p>نیم دگرم دارد عزم شکرافشانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کو نمرد خندان تو شمع مخوان او را</p></div>
<div class="m2"><p>بو بیش دهد عنبر در وقت پریشانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای شهره نوای تو جان است سزای تو</p></div>
<div class="m2"><p>تو مطرب جانانی چون در طمع نانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کس کیسه میفشان گو کس خرقه میفکن گو</p></div>
<div class="m2"><p>اومید کی ضایع شد از کیسه ربانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از کیسه حق گردون صد نور و ضیا ریزد</p></div>
<div class="m2"><p>دریا ز عطای حق دارد گهرافشانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نان ریزه سفره‌ست این کز چرخ همی‌ریزد</p></div>
<div class="m2"><p>بگذر ز فلک بررو گر درخور آن خوانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر خسته شود کفت کفی دگرت بخشد</p></div>
<div class="m2"><p>ور خسته شود حلقت در حلقه سلطانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برگو غزلی برگو پامزد خود از حق جو</p></div>
<div class="m2"><p>بر سوخته زن آبی چون چشمه حیوانی</p></div></div>