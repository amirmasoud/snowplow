---
title: >-
    غزل شمارهٔ ۱۷۴۴
---
# غزل شمارهٔ ۱۷۴۴

<div class="b" id="bn1"><div class="m1"><p>بیار مطرب بر ما کریم باش کریم</p></div>
<div class="m2"><p>به کوی خسته دلانی رحیم باش رحیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم چو آتش چون در دمی شود زنده</p></div>
<div class="m2"><p>چو دل مباش مسافر مقیم باش مقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیامد آتش و بر راه عاشقان بنشست</p></div>
<div class="m2"><p>که ای مسافر این ره یتیم باش یتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندا رسید به آتش که بر همه عشاق</p></div>
<div class="m2"><p>چو شعله‌های خلیلی نعیم باش نعیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلیم از آب چو خواهی که تا برون آری</p></div>
<div class="m2"><p>به زیر پای عزیزان گلیم باش گلیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بایدت که تو را بحر دایه وار بود</p></div>
<div class="m2"><p>مثال دانه در رو یتیم باش یتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درست و راست شد ای دل که در هوا دل را</p></div>
<div class="m2"><p>درست راست نیاید دو نیم باش دو نیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الف مباش ز ابجد که سرکشی دارد</p></div>
<div class="m2"><p>مباش بی دو سر تو چو جیم باش چو جیم</p></div></div>