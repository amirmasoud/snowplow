---
title: >-
    غزل شمارهٔ ۲۷۷۸
---
# غزل شمارهٔ ۲۷۷۸

<div class="b" id="bn1"><div class="m1"><p>ای خدایی که مفرح بخش رنجوران تویی</p></div>
<div class="m2"><p>در میان لطف و رحمت همچو جان پنهان تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسته کردی بندگان را تا تو را زاری کنند</p></div>
<div class="m2"><p>چون خریدار نفیر و لابه و افغان تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله درمان خواه و آن درمانشان خواهان توست</p></div>
<div class="m2"><p>آنک درد و دارو از وی خاست بی‌شک آن تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردهایی کآدمی را بر در خلقان برد</p></div>
<div class="m2"><p>آن حجاب از اول است و آخر و پایان تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا کاری فروبندد تو باشی چشم بند</p></div>
<div class="m2"><p>هر کجا روشن شود آن شعله تابان تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله بخشی خستگان را تا بدان ساکن شوند</p></div>
<div class="m2"><p>چون حقیقت بنگرم در درد ما نالان تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم تویی آن کس که می‌گوید تویی والله تویی</p></div>
<div class="m2"><p>گوی و چوگان و نظاره گر در این میدان تویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و آنک منکر می‌شود این را و علت می‌نهد</p></div>
<div class="m2"><p>در میان وسوسه او نفس علت خوان تویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و آنک می‌گوید تویی زین گفت ترسان می‌شود</p></div>
<div class="m2"><p>در میان جان او در پرده ترسان تویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنج زندان را به یک اندیشه بستان می‌کنی</p></div>
<div class="m2"><p>رنج هر زندان ز توست و ذوق هر بستان تویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در یکی کار آن یکی راغب و آن دیگر نفور</p></div>
<div class="m2"><p>تو مخالف کرده‌ای شان فتنه ایشان تویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن یکی محبوب این و باز او مکروه آن</p></div>
<div class="m2"><p>چشم بندی چشم و دل را قبله و سامان تویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صد هزاران نقش را تو بنده نقشی کنی</p></div>
<div class="m2"><p>گویی سلطان است آن دام است خود سلطان تویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بندگی و خواجگی و سلطنت خط‌های توست</p></div>
<div class="m2"><p>خط کژ و خط راست این دبیرستان تویی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صورت ما خانه‌ها و روح ما مهمان در آن</p></div>
<div class="m2"><p>نقش و جان‌ها سایه تو جان آن مهمان تویی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دست در طاعت زنیم و چشم در ایمان نهیم</p></div>
<div class="m2"><p>بر امید آنک بنمایی که خود ایمان تویی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست احسان بر سر ما نه ز احسانی که ما</p></div>
<div class="m2"><p>چشم روشن در تو آویزیم کان احسان تویی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غفلت و بیداری ما در، توی بر کار و بس</p></div>
<div class="m2"><p>غفلت ما بی‌فضولی بر، چو خود یقظان تویی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>توبه با تو خود فضول است و شکستن خود بتر</p></div>
<div class="m2"><p>نقش پیمان گر شکست ارواح آن پیمان تویی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روح‌ها می‌پروری همچون زر و مس و عقیق</p></div>
<div class="m2"><p>چون مخالف شد جواهر ای عجب چون کان تویی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روز درپیچد صفت در ما و تابد تا به شب</p></div>
<div class="m2"><p>شب صفات از ما به تو آید صفاتستان تویی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روز تا شب ما چنین بر همدگر رحمت کنیم</p></div>
<div class="m2"><p>شب همه رحمت رود سوی تو چون رحمان تویی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کو سلاطین جهان گر شاه ایوان بوده‌اند</p></div>
<div class="m2"><p>پس بدانستیم بی‌شک کاندر این ایوان تویی</p></div></div>