---
title: >-
    غزل شمارهٔ ۶۱۸
---
# غزل شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>چون جغد بود اصلش کی صورت باز آید</p></div>
<div class="m2"><p>چون سیر خورد مردم کی بوی پیاز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون افتد شیر نر از حمله حیز و غر</p></div>
<div class="m2"><p>وز زخمه کون خر کی بانگ نماز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای تو شده کوچک از تنگی پاپوچک</p></div>
<div class="m2"><p>پا برکش ای کوچک تا پهن و دراز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگشای به امیدی تو دیده جاویدی</p></div>
<div class="m2"><p>تا تابش خورشیدش از عرش فرازآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنگا تو سری برکن در حلقه سر اندر کن</p></div>
<div class="m2"><p>تو خویش تهیتر کن تا چنگ به ساز آید</p></div></div>