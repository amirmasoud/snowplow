---
title: >-
    غزل شمارهٔ ۲۲۸۸
---
# غزل شمارهٔ ۲۲۸۸

<div class="b" id="bn1"><div class="m1"><p>شحنه عشق می‌کشد از دو جهان مصادره</p></div>
<div class="m2"><p>دیده و دل گرو کنم بهر چنان مصادره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سبب مصادره شحنه عشق رهزند</p></div>
<div class="m2"><p>پس بر عاشقان شود راحت جان مصادره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد جگر مصادره از خود لعل پاره‌ها</p></div>
<div class="m2"><p>جانب دیده پاره‌ای رفت از آن مصادره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق شهی است چون قمر کیسه گشا و سیم بر</p></div>
<div class="m2"><p>سیم بده به سیم بر نیست زیان مصادره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه برد مصادره از تن عاشقان گرو</p></div>
<div class="m2"><p>بازرسد به کوی دل نورفشان مصادره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فصل بهار را ببین جمله به باغ وادهد</p></div>
<div class="m2"><p>آنچ ز باغ برده بد ظلم خزان مصادره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخشش آفتاب بین بازدهد قماش مه</p></div>
<div class="m2"><p>هر چه ز ماه می‌ستد دور زمان مصادره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده و عقل و هوش را شب به مصادره برد</p></div>
<div class="m2"><p>صبحدمی ندا کند بازستان مصادره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نور سحر بریخته زنگیکان گریخته</p></div>
<div class="m2"><p>گر چه شب آفتاب را کرد نهان مصادره</p></div></div>