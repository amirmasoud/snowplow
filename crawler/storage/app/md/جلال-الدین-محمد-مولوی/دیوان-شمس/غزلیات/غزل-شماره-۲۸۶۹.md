---
title: >-
    غزل شمارهٔ ۲۸۶۹
---
# غزل شمارهٔ ۲۸۶۹

<div class="b" id="bn1"><div class="m1"><p>هست اندر غم تو دلشده دانشمندی</p></div>
<div class="m2"><p>همچو نقره‌ست در آتشکده دانشمندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر امید کرم و رحمت بخشایش تو</p></div>
<div class="m2"><p>از ره دور به سر آمده دانشمندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست ز اوباش خیالات تو اندر ره عشق</p></div>
<div class="m2"><p>خسته و شیفته و ره زده دانشمندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه زیان دارد خوبی تو را دوست اگر</p></div>
<div class="m2"><p>قوت یابد ز چنین مایده دانشمندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنین جام جنونی که تو گردان کردی</p></div>
<div class="m2"><p>کی بماند به سر قاعده دانشمندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی روا دارد انصاف و جوانمردی تو</p></div>
<div class="m2"><p>که به غم کشته شود بیهده دانشمندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی روا دارد خورشید حق گرمی بخش</p></div>
<div class="m2"><p>که فسرده شود از مجمده دانشمندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانب مدرسه عشق کشیدش لطفت</p></div>
<div class="m2"><p>تا ز درس تو برد فایده دانشمندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نحس تربیع عناصر بگرفتش رحمی</p></div>
<div class="m2"><p>تا منور شود از منقده دانشمندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس سخن دارد وز بیم ملال دل تو</p></div>
<div class="m2"><p>لب ببسته‌ست در این معبده دانشمندی</p></div></div>