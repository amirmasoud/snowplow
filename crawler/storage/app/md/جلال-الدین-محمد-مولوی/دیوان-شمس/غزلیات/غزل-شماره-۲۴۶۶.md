---
title: >-
    غزل شمارهٔ ۲۴۶۶
---
# غزل شمارهٔ ۲۴۶۶

<div class="b" id="bn1"><div class="m1"><p>ای که به لطف و دلبری از دو جهان زیاده‌ای</p></div>
<div class="m2"><p>ای که چو آفتاب و مه دست کرم گشاده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح که آفتاب خود سر نزده‌ست از زمین</p></div>
<div class="m2"><p>جام جهان نمای را بر کف جان نهاده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهدی و مهتدی تویی رحمت ایزدی تویی</p></div>
<div class="m2"><p>روی زمین گرفته‌ای داد زمانه داده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مایه صد ملامتی شورش صد قیامتی</p></div>
<div class="m2"><p>چشمه مشک دیده‌ای جوشش خنب باده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر نبرد هر آنک او سر کشد از هوای تو</p></div>
<div class="m2"><p>ز آنک به گردن همه بسته‌تر از قلاده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز دلا و خلق را سوی صبوح بانگ زن</p></div>
<div class="m2"><p>گر چه ز دوش بیخودی بی‌سر و پا فتاده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سحری خیال تو دارد میل سردهی</p></div>
<div class="m2"><p>دشمن عقل و دانشی فتنه مرد ساده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو بهار ساقیی همچو بهشت باقیی</p></div>
<div class="m2"><p>همچو کباب قوتی همچو شراب شاده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیز دلا کشان کشان رو سوی بزم بی‌نشان</p></div>
<div class="m2"><p>عشق سواره‌ات کند گر چه چنین پیاده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذره به ذره ای جهان جانب تو نظرکنان</p></div>
<div class="m2"><p>گوهر آب و آتشی مونس نر و ماده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این تن همچو غرقه را تا نکنی ز سر برون</p></div>
<div class="m2"><p>بند ردا و خرقه‌ای مرد سر سجاده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باده خامشانه خور تا برهی ز گفت و گو</p></div>
<div class="m2"><p>یا حیوان ناطقی جمله ز نطق زاده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لطف نمای ساقیا دست بگیر مست را</p></div>
<div class="m2"><p>جانب بزم خویش کش شاه طریق جاده‌ای</p></div></div>