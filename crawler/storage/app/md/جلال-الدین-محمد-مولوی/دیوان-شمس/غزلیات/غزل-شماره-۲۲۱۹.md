---
title: >-
    غزل شمارهٔ ۲۲۱۹
---
# غزل شمارهٔ ۲۲۱۹

<div class="b" id="bn1"><div class="m1"><p>من غلام قمرم غیر قمر هیچ مگو</p></div>
<div class="m2"><p>پیش من جز سخن شمع و شکر هیچ مگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن رنج مگو جز سخن گنج مگو</p></div>
<div class="m2"><p>ور از این بی‌خبری رنج مبر هیچ مگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش دیوانه شدم عشق مرا دید و بگفت</p></div>
<div class="m2"><p>آمدم نعره مزن جامه مدر هیچ مگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ای عشق من از چیز دگر می‌ترسم</p></div>
<div class="m2"><p>گفت آن چیز دگر نیست دگر هیچ مگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به گوش تو سخن‌های نهان خواهم گفت</p></div>
<div class="m2"><p>سر بجنبان که بلی جز که به سر هیچ مگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قمری جان صفتی در ره دل پیدا شد</p></div>
<div class="m2"><p>در ره دل چه لطیف است سفر هیچ مگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم ای دل چه مه‌ست این دل اشارت می‌کرد</p></div>
<div class="m2"><p>که نه اندازه توست این بگذر هیچ مگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم این روی فرشته‌ست عجب یا بشر است</p></div>
<div class="m2"><p>گفت این غیر فرشته‌ست و بشر هیچ مگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم این چیست بگو زیر و زبر خواهم شد</p></div>
<div class="m2"><p>گفت می‌باش چنین زیر و زبر هیچ مگو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای نشسته تو در این خانه پرنقش و خیال</p></div>
<div class="m2"><p>خیز از این خانه برو رخت ببر هیچ مگو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم ای دل پدری کن نه که این وصف خداست</p></div>
<div class="m2"><p>گفت این هست ولی جان پدر هیچ مگو</p></div></div>