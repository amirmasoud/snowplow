---
title: >-
    غزل شمارهٔ ۲۱۱۵
---
# غزل شمارهٔ ۲۱۱۵

<div class="b" id="bn1"><div class="m1"><p>بازرسید آن بت زیبای من</p></div>
<div class="m2"><p>خرمی این دم و فردای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظرش روشنی چشم من</p></div>
<div class="m2"><p>در رخ او باغ و تماشای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقبت امر به گوشش رسید</p></div>
<div class="m2"><p>بانگ من و نعره و هیهای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در من کیست که در می‌زند</p></div>
<div class="m2"><p>جان و جهان است و تمنای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نزند او در من درد من</p></div>
<div class="m2"><p>ور نکند یاد من او وای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور مکن سایه خود از سرم</p></div>
<div class="m2"><p>باز مکن سلسله از پای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چه خیالی هله ای روترش</p></div>
<div class="m2"><p>رو بر حلوایی و حلوای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم بخور و هم کف حلوا بیار</p></div>
<div class="m2"><p>تا که بیفزاید صفرای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریش تو را سخت گرفته‌ست غم</p></div>
<div class="m2"><p>چیست زبونی تو بابای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در زنخش کوب دو سه مشت سخت</p></div>
<div class="m2"><p>ای نر و نرزاده و مولای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشک بدرید و بینداخت دلو</p></div>
<div class="m2"><p>غرقه آب آمد سقای من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بانگ زدم کای کر سقا بیا</p></div>
<div class="m2"><p>رفت و بنشنید علالای من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن من است او و به هر جا رود</p></div>
<div class="m2"><p>عاقبت آید سوی صحرای من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جوشش دریای معلق مگر</p></div>
<div class="m2"><p>از لمع گوهر گویای من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گوید دریا که ز کشتی بجه</p></div>
<div class="m2"><p>دررو در آب مصفای من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قطره به دریا چو رود در شود</p></div>
<div class="m2"><p>قطره شود بحر به دریای من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترک غزل گیر و نگر در ازل</p></div>
<div class="m2"><p>کز ازل آمد غم و سودای من</p></div></div>