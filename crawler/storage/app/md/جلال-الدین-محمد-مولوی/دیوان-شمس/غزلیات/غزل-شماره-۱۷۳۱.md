---
title: >-
    غزل شمارهٔ ۱۷۳۱
---
# غزل شمارهٔ ۱۷۳۱

<div class="b" id="bn1"><div class="m1"><p>اگر زمین و فلک را پر از سلام کنیم</p></div>
<div class="m2"><p>وگر سگان تو را فرش سیم خام کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر همای تو را هر سحر که می آید</p></div>
<div class="m2"><p>ز جان و دیده و دل حلقه‌های دام کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر هزار دل پاک را به هر سر راه</p></div>
<div class="m2"><p>به دست نامه پرخون به تو پیام کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر چو نقره و زر پاک و خالص از پی تو</p></div>
<div class="m2"><p>میان آتش تو منزل و مقام کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ذات پاک منزه که بعد این همه کار</p></div>
<div class="m2"><p>به هر طرف نگرانیم تا کدام کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قرار عاقبت کار هم بر این افتاد</p></div>
<div class="m2"><p>که خویش را همه حیران و خیره نام کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و آنگهی که رسد باده‌های حیرانان</p></div>
<div class="m2"><p>ز شیشه خانه دل صد هزار جام کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو سیمبر به صفا تنگمان به بر گیرد</p></div>
<div class="m2"><p>فلک که کره تند است ماش رام کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مغز روح از آن باده‌ها به جوش آید</p></div>
<div class="m2"><p>چهار حد جهان را به تک دو گام کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شمس تبریز انگشتری چو بستانیم</p></div>
<div class="m2"><p>هزار خسرو تمغاج را غلام کنیم</p></div></div>