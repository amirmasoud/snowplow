---
title: >-
    غزل شمارهٔ ۲۲۱۳
---
# غزل شمارهٔ ۲۲۱۳

<div class="b" id="bn1"><div class="m1"><p>خنک آن جان که رود مست و خرامان بر او</p></div>
<div class="m2"><p>برهد از خر تن در سفر مصدر او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلع نعلین کند وز خود و دنیا بجهد</p></div>
<div class="m2"><p>همچو موسی قدم صدق زند بر در او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو جرجیس شود کشته عشقش صد بار</p></div>
<div class="m2"><p>یا چو اسحاق شود بسمل از آن خنجر او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر دیگر رسدش جز سر پردرد و صداع</p></div>
<div class="m2"><p>مغفرت بنهد بر فرق سرش مغفر او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیله رزقش اگر درشکند میکائیل</p></div>
<div class="m2"><p>عوضش گاه بود خلد و گهی کوثر او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدر و مادر و خویشان چو به خاکش بنهند</p></div>
<div class="m2"><p>شود او ماهی و دریا پدر و مادر او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق دریای حیات است که او را تک نیست</p></div>
<div class="m2"><p>عمر جاوید بود موهبت کمتر او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌رود شمس و قمر هر شب در گور غروب</p></div>
<div class="m2"><p>می‌دهدشان فر نو شعشعه گوهر او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک الموت به صد ناز ستاند جانی</p></div>
<div class="m2"><p>که بود باخبر و دیده ور از محشر او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن ما خفته در آن خاک به چشم عامه</p></div>
<div class="m2"><p>روح چون سرو روان در چمن اخضر او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه به ظاهر تن ما معدن خون و خلط است</p></div>
<div class="m2"><p>هیچ جان را سقمی هست از این مقذر او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در چنین مزبله جان را دو هزاران باغ است</p></div>
<div class="m2"><p>پس چرا ترسد جان از لحد و مقبر او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنک خون را چو می ناب غذای جان کرد</p></div>
<div class="m2"><p>بنگر در تن پرنور و رخ احمر او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هله دلدار بخوان باقی این بر منکر</p></div>
<div class="m2"><p>تا دو صد چشمه روان گردد از مرمر او</p></div></div>