---
title: >-
    غزل شمارهٔ ۱۸۶۸
---
# غزل شمارهٔ ۱۸۶۸

<div class="b" id="bn1"><div class="m1"><p>دروازه هستی را جز ذوق مدان ای جان</p></div>
<div class="m2"><p>این نکته شیرین را در جان بنشان ای جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا عرض و جوهر از ذوق برآرد سر</p></div>
<div class="m2"><p>ذوق پدر و مادر کردت مهمان ای جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که بود ذوقی ز آسیب دو جفت آید</p></div>
<div class="m2"><p>زان یک شدن دو تن ذوق است نشان ای جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر حس به محسوسی جفت است یکی گشته</p></div>
<div class="m2"><p>هر عقلی به معقولی جفت و نگران ای جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر جفت شوی ای حس با آنک حست کرد او</p></div>
<div class="m2"><p>وز غیر بپرهیزی باشی سلطان ای جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوقی که ز خلق آید زو هستی تن زاید</p></div>
<div class="m2"><p>ذوقی که ز حق آید زاید دل و جان ای جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو چشم که تا بیند هر گوشه تتق بسته</p></div>
<div class="m2"><p>هر ذره بپیوسته با جفت نهان ای جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمیخته با شاهد هم عاشق و هم زاهد</p></div>
<div class="m2"><p>وز ذوق نمی‌گنجد در کون و مکان ای جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پنهان ز همه عالم گرمابه زده هر دم</p></div>
<div class="m2"><p>هم پیر خردپیشه هم جان جوان ای جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پنهان مکن ای رستم پنهان تو را جستم</p></div>
<div class="m2"><p>احوال تو دانستم تو عشوه مخوان ای جان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر روی ترش داری دانیم که طراری</p></div>
<div class="m2"><p>ز احداث همی‌ترسی وز مکر عوان ای جان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کنج عزبخانه حوری چو دردانه</p></div>
<div class="m2"><p>دور از لب بیگانه خفته‌ست ستان ای جان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صد عشق همی‌بازد صد شیوه همی‌سازد</p></div>
<div class="m2"><p>آن لحظه که می یازد بوسه بستان ای جان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر ظاهر دریا کی بینی خورش ماهی</p></div>
<div class="m2"><p>کان آب تتق آمد بر عیش کنان ای جان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چندان حیوان آن سو می خاید و می زاید</p></div>
<div class="m2"><p>چون گرگ گرو برده پنهان ز شبان ای جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خنبک زده هر ذره بر معجب بی‌بهره</p></div>
<div class="m2"><p>کآب حیوان را کی داند حیوان ای جان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندر دل هر ذره تابان شده خورشیدی</p></div>
<div class="m2"><p>در باطن هر قطره صد جوی روان ای جان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاموش که آن لقمه هر بسته دهان خاید</p></div>
<div class="m2"><p>تا لقمه نیندازی بربند دهان ای جان</p></div></div>