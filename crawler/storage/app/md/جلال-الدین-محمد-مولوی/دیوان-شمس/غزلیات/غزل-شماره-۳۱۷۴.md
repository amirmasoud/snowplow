---
title: >-
    غزل شمارهٔ ۳۱۷۴
---
# غزل شمارهٔ ۳۱۷۴

<div class="b" id="bn1"><div class="m1"><p>صد دل و صد جان بدمی دادمی</p></div>
<div class="m2"><p>وز جهت دادن جان شادمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور تن من خاک بدی این نفس</p></div>
<div class="m2"><p>جمله گل و عشق و هوش زادمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جهت کشت غمش آبمی</p></div>
<div class="m2"><p>وز جهت خرمن او بادمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ندمیدی غم او در دلم</p></div>
<div class="m2"><p>چون دگران بی‌دم و فریادمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نبدی غیرت شیرین من</p></div>
<div class="m2"><p>فخر دو صد خسرو و فرهادمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نشکستی دل دربان راز</p></div>
<div class="m2"><p>قفل جهان همه بگشادمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور همدانم نشدی پای گیر</p></div>
<div class="m2"><p>همره آن طرفهٔ بغدادمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که همه سهو و فراموشیم</p></div>
<div class="m2"><p>گر نبدی یاد تو من یادمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس! که برد سر و پی این زبان</p></div>
<div class="m2"><p>حسره که من سوسن آزادمی</p></div></div>