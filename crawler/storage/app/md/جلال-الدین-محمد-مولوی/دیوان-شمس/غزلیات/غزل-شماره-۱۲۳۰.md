---
title: >-
    غزل شمارهٔ ۱۲۳۰
---
# غزل شمارهٔ ۱۲۳۰

<div class="b" id="bn1"><div class="m1"><p>جانم به چه آرامد ای یار به آمیزش</p></div>
<div class="m2"><p>صحت به چه دریابد بیمار به آمیزش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند به بر گیری او را نبود سیری</p></div>
<div class="m2"><p>دانی به چه بنشیند این بار به آمیزش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن تشنه ده روزه کی به شود از کوزه</p></div>
<div class="m2"><p>الا که کند آبش خوش خوار به آمیزش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وصل تو می‌جوید وز شرم نمی‌گوید</p></div>
<div class="m2"><p>کامسال طرب خواهد چون پار به آمیزش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاری که کند بنده تقدیر زند خنده</p></div>
<div class="m2"><p>کای خفته بجو آخر این کار به آمیزش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیرا که به آمیزش یک خشت شود قصری</p></div>
<div class="m2"><p>زیرا که شود جامه یک تار به آمیزش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر چمن عشقت شمس الحق تبریزی</p></div>
<div class="m2"><p>صد گلشن و گل گردد یک خار به آمیزش</p></div></div>