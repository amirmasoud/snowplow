---
title: >-
    غزل شمارهٔ ۳۱۷۰
---
# غزل شمارهٔ ۳۱۷۰

<div class="b" id="bn1"><div class="m1"><p>ای که تو از عالم ما می‌روی</p></div>
<div class="m2"><p>خوش ز زمین سوی سما می‌روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای قفس اشکسته و جسته ز بند</p></div>
<div class="m2"><p>پر بگشادی به کجا می‌روی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر ز کفن بر زن و ما را بگو</p></div>
<div class="m2"><p>که: « ز وطن خویش چرا می‌روی؟ »</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی غلطم، عاریه بود این وطن</p></div>
<div class="m2"><p>سوی وطنگاه بقا می‌روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ز قضا دعوت و فرمان رسید</p></div>
<div class="m2"><p>در پی سرهنگ قضا می‌روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا که ز جنات نسیمی رسید</p></div>
<div class="m2"><p>در پی رضوان رضا می‌روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا ز تجلی جلال قدیم</p></div>
<div class="m2"><p>مضطرب و بی‌سر و پا می‌روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا ز شعاعات جمال خدا</p></div>
<div class="m2"><p>مست ملاقات لقا می‌روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا ز بن خم جهان همچو درد</p></div>
<div class="m2"><p>صاف شدی سوی علا می‌روی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا به صفاتی که خموشان کنند</p></div>
<div class="m2"><p>خامش و مخفی و خفا می‌روی</p></div></div>