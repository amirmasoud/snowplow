---
title: >-
    غزل شمارهٔ ۱۹۸۴
---
# غزل شمارهٔ ۱۹۸۴

<div class="b" id="bn1"><div class="m1"><p>بده آن مرد ترش را قدحی ای شه شیرین</p></div>
<div class="m2"><p>صدقات تو روان است به هر بیوه و مسکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدقات تو لطیف است توان خورد دو صد من</p></div>
<div class="m2"><p>که نداند لب بالا و نجنبد لب زیرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هله ای باغ نگویی به چه لب باده کشیدی</p></div>
<div class="m2"><p>مگر اشکوفه بگوید پنهان با گل و نسرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه شراب است کز آن بو گل‌تر آهوی ناف است</p></div>
<div class="m2"><p>به زمستان نه که دیدی همه را چون سگ گرگین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هله تا جمع رسیدن بده آن می به کف من</p></div>
<div class="m2"><p>پس من زهره بنوشد قدح از ساعد پروین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر آن مست نهد سر که رباید ز تو ساغر</p></div>
<div class="m2"><p>مده او را تو مرا ده که منم بر در تحسین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کند باده حق را جگر باطل فانی</p></div>
<div class="m2"><p>چه شناسد مه جان را نظر و غمزه عنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنر و زر چو فزون شد خطر و خوف کنون شد</p></div>
<div class="m2"><p>ملکان را تب لرز است و حریر است نهالین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مه توبه درآمد مه توبه شکن آمد</p></div>
<div class="m2"><p>شکنش باد همیشه تو بگو نیز که آمین</p></div></div>