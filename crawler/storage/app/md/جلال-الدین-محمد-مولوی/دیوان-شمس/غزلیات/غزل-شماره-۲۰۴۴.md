---
title: >-
    غزل شمارهٔ ۲۰۴۴
---
# غزل شمارهٔ ۲۰۴۴

<div class="b" id="bn1"><div class="m1"><p>جانا بیار باده و بختم بلند کن</p></div>
<div class="m2"><p>زان حلقه‌های زلف دلم را کمند کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس خوش است و ما و حریفان همه خوشیم</p></div>
<div class="m2"><p>آتش بیار و چاره مشتی سپند کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان جام بی‌دریغ در اندیشه‌ها بریز</p></div>
<div class="m2"><p>در بیخودی سزای دل خودپسند کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای غم برو برو بر مستانت کار نیست</p></div>
<div class="m2"><p>آن را که هوشیار بیابی گزند کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستان مسلمند ز اندیشه‌ها و غم</p></div>
<div class="m2"><p>آن کو نشد مسلم او را نژند کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای جان مست مجلس ابرار یشربون</p></div>
<div class="m2"><p>بر گربه اسیر هوا ریش خند کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریش همه به دست اجل بین و رحم کن</p></div>
<div class="m2"><p>از مرگ وارهان همه را سودمند کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عزم سفر کن ای مه و بر گاو نه تو رخت</p></div>
<div class="m2"><p>با شیرگیر مست مگو ترک پند کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چشم ما نگر اثر بیخودی ببین</p></div>
<div class="m2"><p>ما را سوار اشقر و پشت سمند کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک رگ اگر در این تن ما هوشیار هست</p></div>
<div class="m2"><p>با او حساب دفتر هفتاد و اند کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای طبع روسیاه سوی هند بازرو</p></div>
<div class="m2"><p>وی عشق ترک تاز سفر سوی جند کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن جا که مست گشتی بنشین مقیم شو</p></div>
<div class="m2"><p>و آن جا که باده خوردی آن جا فکند کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در مطبخ خدا اگرت قوت روح نیست</p></div>
<div class="m2"><p>آن گاه سر در آخر این گوسفند کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواهی که شاهدان فلک جلوه گر شوند</p></div>
<div class="m2"><p>دل را حریف صیقل آیینه رند کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای دل خموش کن همه بی‌حرف گو سخن</p></div>
<div class="m2"><p>بی‌لب حدیث عالم بی‌چون و چند کن</p></div></div>