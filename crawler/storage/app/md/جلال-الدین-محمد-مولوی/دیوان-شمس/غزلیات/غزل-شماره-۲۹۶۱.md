---
title: >-
    غزل شمارهٔ ۲۹۶۱
---
# غزل شمارهٔ ۲۹۶۱

<div class="b" id="bn1"><div class="m1"><p>در رنگ یار بنگر تا رنگ زندگانی</p></div>
<div class="m2"><p>بر روی تو نشیند ای ننگ زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ذره‌ای دوان است تا زندگی بیابد</p></div>
<div class="m2"><p>تو ذره‌ای نداری آهنگ زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز آنک زندگانی بودی مثال سنگی</p></div>
<div class="m2"><p>خوش چشمه‌ها دویدی از سنگ زندگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آینه بدیدم نقش خیال فانی</p></div>
<div class="m2"><p>گفتم چیی تو گفتا من زنگ زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر حیات باقی یابی تو زندگان را</p></div>
<div class="m2"><p>وین باقیان کیانند دلتنگ زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن‌ها که اهل صلحند بردند زندگی را</p></div>
<div class="m2"><p>وین ناکسان بمانند در جنگ زندگانی</p></div></div>