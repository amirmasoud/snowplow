---
title: >-
    غزل شمارهٔ ۴۸۳
---
# غزل شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>هر آنک از سبب وحشت غمی تنهاست</p></div>
<div class="m2"><p>بدانک خصم دلست و مراقب تن‌هاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چنگ و تنتن این تن نهاده‌ای گوشی</p></div>
<div class="m2"><p>تن تو توده خاکست و دمدمه ش چو هواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای نفس تو همچون هوای گردانگیز</p></div>
<div class="m2"><p>عدو دیده و بیناییست و خصم ضیاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی مگر مگس این مطاعم عسلین</p></div>
<div class="m2"><p>که زامقلو تو را درد و زانقلوه عناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن زمان که در این دوغ می‌فتی چو مگس</p></div>
<div class="m2"><p>عجب که توبه و عقل و رأیت تو کجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عهد و توبه چرا چون فتیله می‌پیچی</p></div>
<div class="m2"><p>که عهد تو چو چراغی رهین هر نکباست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو به یوسف یعقوب هجر را دریاب</p></div>
<div class="m2"><p>که بی ز پیرهن نصرت تو حبس عماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گوشت پاره ضریریست مانده بر جایی</p></div>
<div class="m2"><p>چو مرده‌ای‌ست ضریر و عقیله احیاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جای دارو او خاک می‌زند در چشم</p></div>
<div class="m2"><p>بدان گمان که مگر سرمه است و خاک و دواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو لا تعاف من الکافرین دیارا</p></div>
<div class="m2"><p>دعای نوح نبیست و او مجاب دعاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همیشه کشتی احمق غریق طوفان‌ست</p></div>
<div class="m2"><p>که زشت صنعت و مبغوض گوهر و رسواست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چه بحر کرم موج می‌زند هر سو</p></div>
<div class="m2"><p>به حکم عدل خبیثات مر خبیثین راست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قفا همی‌خور و اندرمکش کلا گردن</p></div>
<div class="m2"><p>چنان گلو که تو داری سزای صفع و قفاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گلو گشاده چو فرج فراخ ماده خران</p></div>
<div class="m2"><p>که کیر خر نرهد زو چو پیش او برخاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخور تو ای سگ گرگین شکنبه و سرگین</p></div>
<div class="m2"><p>شکمبه و دهن سگ بلی سزا به سزاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیا بخور خر مرده سگ شکار نه‌ای</p></div>
<div class="m2"><p>ز پوز و ز شکم و طلعت تو خود پیداست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سگ محله و بازار صید کی گیرد</p></div>
<div class="m2"><p>مقام صید سر کوه و بیشه و صحراست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رها کن این همه را نام یار و دلبر گو</p></div>
<div class="m2"><p>که زشت‌ها که بدو دررسد همه زیباست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که کیمیاست پناه وی و تعلق او</p></div>
<div class="m2"><p>مصرف همه ذرات اسفل و اعلاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهان کند دو جهان را درون یک ذره</p></div>
<div class="m2"><p>که از تصرف او عقل گول و نابیناست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدانک زیرکی عقل جمله دهلیزیست</p></div>
<div class="m2"><p>اگر به علم فلاطون بود برون سراست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جنون عشق به از صد هزار گردون عقل</p></div>
<div class="m2"><p>که عقل دعوی سر کرد و عشق بی‌سر و پاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر آنک سر بودش بیم سر همش باشد</p></div>
<div class="m2"><p>حریف بیم نباشد هر آنک شیر وغاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رود درونه سم الخیاط رشته عشق</p></div>
<div class="m2"><p>که سر ندارد و بی‌سر مجرد و یکتاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قلاوزی کندش سوزن و روان کندش</p></div>
<div class="m2"><p>که تا وصال ببخشد به پاره‌ها که جداست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حدیث سوزن و رشته بهل که باریکست</p></div>
<div class="m2"><p>حدیث موسی جان کن که با ید بیضاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حدیث قصه آن بحر خوشدلی‌ها گو</p></div>
<div class="m2"><p>که قطره قطره او مایه دو صد دریاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو کاسه بر سر بحری و بی‌خبر از بحر</p></div>
<div class="m2"><p>ببین ز موج تو را هر نفس چه گردشهاست</p></div></div>