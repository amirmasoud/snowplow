---
title: >-
    غزل شمارهٔ ۲۶۷۹
---
# غزل شمارهٔ ۲۶۷۹

<div class="b" id="bn1"><div class="m1"><p>ز ما برگشتی و با گل فتادی</p></div>
<div class="m2"><p>دو چشم خویش سوی گل گشادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شرم روی ما گل از تو بگریخت</p></div>
<div class="m2"><p>ز گل واگشتی این جا سر نهادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهادی سر که پای من ببوسی</p></div>
<div class="m2"><p>نیابی بوسه گل را بوسه دادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان لب‌ها که بوی گل گرفته‌ست</p></div>
<div class="m2"><p>نیابی بوسه گر چه اوستادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای رفع بویش این دو لب را</p></div>
<div class="m2"><p>همی‌مالم به خاکت من ز شادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا بردارم این لب از تو ای خاک</p></div>
<div class="m2"><p>ولی فتنه تویی گل را تو زادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو آن خاکی که از حق لطف دزدی</p></div>
<div class="m2"><p>تو دزدی و مریدی و مرادی</p></div></div>