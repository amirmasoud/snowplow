---
title: >-
    غزل شمارهٔ ۲۳۵۱
---
# غزل شمارهٔ ۲۳۵۱

<div class="b" id="bn1"><div class="m1"><p>دیدی که چه کرد آن یگانه</p></div>
<div class="m2"><p>برساخت پریر یک بهانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را و تو را کجا فرستاد</p></div>
<div class="m2"><p>او ماند و دو سه پری خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را بفریفت ما چه باشیم</p></div>
<div class="m2"><p>با آن حرکات ساحرانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن سلسله کو به دست دارد</p></div>
<div class="m2"><p>بربندد گردن زمانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سنگ برون کشید مکری</p></div>
<div class="m2"><p>شاباش زهی شکر فسانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بست او گرهی میان ابرو</p></div>
<div class="m2"><p>گم گشت خرد از این میانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر درگه او است دل چو مسمار</p></div>
<div class="m2"><p>بردوخته خویش بر ستانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر مرکب مملکت سوار او است</p></div>
<div class="m2"><p>در دست وی است تازیانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر او کمر کهی بگیرد</p></div>
<div class="m2"><p>که را چو کهی کند کشانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود آن که قاف همچو سیمرغ</p></div>
<div class="m2"><p>کرده‌ست به کویش آشیانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شرم عقیق درفشانش</p></div>
<div class="m2"><p>درها بگداخت دانه دانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بادی که ز عشق او است در تن</p></div>
<div class="m2"><p>ساکن نشود به رازیانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشاق مذکرند وین خلق</p></div>
<div class="m2"><p>درمانده‌اند در مثانه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ساقی درده قدح که ماییم</p></div>
<div class="m2"><p>مخمور ز باده شبانه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آبی برزن که آتش دل</p></div>
<div class="m2"><p>بر چرخ همی‌زند زبانه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در دست همیشه مصحفم بود</p></div>
<div class="m2"><p>وز عشق گرفته‌ام چغانه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندر دهنی که بود تسبیح</p></div>
<div class="m2"><p>شعر است و دوبیتی و ترانه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بس صومعه‌ها که سیل بربود</p></div>
<div class="m2"><p>چه سیل که بحر بی‌کرانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هشیار ز من فسانه ناید</p></div>
<div class="m2"><p>مانند رباب بی‌کمانه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مستم کن و برپران چو تیرم</p></div>
<div class="m2"><p>بشنو قصص بنی کنانه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون مست بود ز باده حق</p></div>
<div class="m2"><p>شهباز شود کمین سمانه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی‌خویش گذر کند ز دیوار</p></div>
<div class="m2"><p>بر روی هوا شود روانه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باخویش ز حق شوند و بی‌خویش</p></div>
<div class="m2"><p>می‌ها بکشند عاشقانه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیدم که لبش شراب نوشد</p></div>
<div class="m2"><p>کی دید ز لب می مغانه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>و آن گاه چی می می خدایی</p></div>
<div class="m2"><p>نه از خنب فلان و یا فلانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ماهی ز کنار چرخ درتافت</p></div>
<div class="m2"><p>گم گشت دلم از این میانه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این طرفه که شخص بی‌دل و جان</p></div>
<div class="m2"><p>چون چنگ همی‌کند فغانه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مشنو غم عشق را ز هشیار</p></div>
<div class="m2"><p>کو سردلب است و سردچانه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرگز دیدی تو یا کسی دید</p></div>
<div class="m2"><p>یخدان ز آتش دهد نشانه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دم درکش و فضل و فن رها کن</p></div>
<div class="m2"><p>با باز چه فن زند سمانه</p></div></div>