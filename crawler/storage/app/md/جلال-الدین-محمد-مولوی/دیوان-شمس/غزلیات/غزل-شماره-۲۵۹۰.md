---
title: >-
    غزل شمارهٔ ۲۵۹۰
---
# غزل شمارهٔ ۲۵۹۰

<div class="b" id="bn1"><div class="m1"><p>ای پرده در پرده بنگر که چه‌ها کردی</p></div>
<div class="m2"><p>دل بردی و جان بردی این جا چه رها کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای برده هوس‌ها را بشکسته قفس‌ها را</p></div>
<div class="m2"><p>مرغ دل ما خستی پس قصد هوا کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر قصد هوا کردی ور عزم جفا کردی</p></div>
<div class="m2"><p>کو زهره که تا گویم ای دوست چرا کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن شمع که می‌سوزد گویم ز چه می‌گرید</p></div>
<div class="m2"><p>زیرا که ز شیرینش در قهر جدا کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن چنگ که می‌زارد گویم ز چه می‌زارد</p></div>
<div class="m2"><p>کز هجر تو پشت او چون بنده دوتا کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جمله جفا کردی اما چو نمودی رو</p></div>
<div class="m2"><p>زهرم چو شکر کردی وز درد دوا کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر برگ ز بی‌برگی کف‌ها به دعا برداشت</p></div>
<div class="m2"><p>از بس که کرم کردی حاجات روا کردی</p></div></div>