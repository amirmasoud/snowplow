---
title: >-
    غزل شمارهٔ ۱۳۵۴
---
# غزل شمارهٔ ۱۳۵۴

<div class="b" id="bn1"><div class="m1"><p>تو را سعادت بادا در آن جمال و جلال</p></div>
<div class="m2"><p>هزار عاشق اگر مرد خون مات حلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یک دمم بفروزی به یک دمم بکشی</p></div>
<div class="m2"><p>چو آتشیم به پیش تو ای لطیف خصال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل آب و قالب کوزه‌ست و خوف بر کوزه</p></div>
<div class="m2"><p>چو آب رفت به اصلش شکسته گیر سفال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را چگونه فریبم چه در جوال کنم</p></div>
<div class="m2"><p>که اصل مکر تویی و چراغ هر محتال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو در جوال نگنجی و دام را بدری</p></div>
<div class="m2"><p>که دیده است که شیری رود درون جوال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه گربه‌ای که روی در جوال و بسته شوی</p></div>
<div class="m2"><p>که شیر پیش تو بر ریگ می‌زند دنبال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار صورت زیبا بروید از دل و جان</p></div>
<div class="m2"><p>چو ابر عشق تو بارید در بی‌امثال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مثال آنک ببارد ز آسمان باران</p></div>
<div class="m2"><p>چو قبه قبه شود جوی و حوض و آب زلال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه قبه قبه کز آن قبه‌ها برون آیند</p></div>
<div class="m2"><p>گل و بنفشه و نسرین و سنبل چو هلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگویمت که از این‌ها کیان برون آیند</p></div>
<div class="m2"><p>شنودم از تکشان بانگ ژغرغ خلخال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ردای احمد مرسل بگیر ای عاشق</p></div>
<div class="m2"><p>صلای عشق شنو هر دم از روان بلال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهل مرا که بگوییم عجایبت ای عشق</p></div>
<div class="m2"><p>دری گشایم در غیب خلق را ز مقال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه چو کوس و چو طبلیم دل تهی پیشت</p></div>
<div class="m2"><p>برآوریم فغان چون زنی تو زخم دوال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چگونه طبل نپرد بپر کرمنا</p></div>
<div class="m2"><p>که باشدش چو تو سلطان زننده و طبال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود آفتاب جهانی تو شمس تبریزی</p></div>
<div class="m2"><p>ولی مدام نه آن شمس کو رسد به زوال</p></div></div>