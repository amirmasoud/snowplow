---
title: >-
    غزل شمارهٔ ۱۱۸۸
---
# غزل شمارهٔ ۱۱۸۸

<div class="b" id="bn1"><div class="m1"><p>الا ای شمع گریان گرم می‌سوز</p></div>
<div class="m2"><p>خلاص شمع نزدیکست شد روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلاص شمع‌ها شمعی برآمد</p></div>
<div class="m2"><p>که بر زنگی ظلمت‌هاست پیروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهان شد ظلم و ظلمت‌ها ز خورشید</p></div>
<div class="m2"><p>نهان گردد الف چون گشت مهموز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنو از شمس تأویلات و تعبیر</p></div>
<div class="m2"><p>چو اندر خواب بشنیدی تو مرموز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین باشد بیان نور ناطق</p></div>
<div class="m2"><p>نه لب باشد نه آواز و نه پدفوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مه از ابر تن بیرون رو ای دوست</p></div>
<div class="m2"><p>هزار اکسیر از خورشید آموز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی خورشید بهر این دوانست</p></div>
<div class="m2"><p>هلال و بدر صبح و شام چون یوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دیدی پرده سوزی‌های خورشید</p></div>
<div class="m2"><p>دهان از پرده دریدن فرودوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خمش آن شیر شیران نور معنیست</p></div>
<div class="m2"><p>پنیری شد به حرف از حاجت یوز</p></div></div>