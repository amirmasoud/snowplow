---
title: >-
    غزل شمارهٔ ۳۱۱
---
# غزل شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>زشت کسی کو نشد مسخره یار خوب</p></div>
<div class="m2"><p>دست نگر پا نگر دست بزن پا بکوب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسخره باد گشت هر چه درختست و کشت</p></div>
<div class="m2"><p>و آنچ کشد سر ز باد خار بود خشک و چوب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه ز اجزای تو رو ننهد سر کشد</p></div>
<div class="m2"><p>پای بزن بر سرش هین سر و پایش بکوب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونک نخواهی رهید از دم هر گول گیر</p></div>
<div class="m2"><p>خاک کسی شو کز او چاره ندارد قلوب</p></div></div>