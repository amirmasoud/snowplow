---
title: >-
    غزل شمارهٔ ۱۱۱۷
---
# غزل شمارهٔ ۱۱۱۷

<div class="b" id="bn1"><div class="m1"><p>دل ناظر جمال تو آن گاه انتظار</p></div>
<div class="m2"><p>جان مست گلستان تو آن گاه خار خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم ز پرتو نظر او به سوی دل</p></div>
<div class="m2"><p>حوریست بر یمین و نگاریست بر یسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر صبحدم که دام شب و روز بردریم</p></div>
<div class="m2"><p>از دوست بوسه‌ای و ز ما سجده صد هزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امسال حلقه ایست ز سودای عاشقان</p></div>
<div class="m2"><p>گر نیست بازگشت در این عشق عمر پار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنواز چنگ عشق تو به نغمات لم یزل</p></div>
<div class="m2"><p>کز چنگ‌های عشق تو جانست تار تار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر هوای عشق تو از تابش حیات</p></div>
<div class="m2"><p>بگرفته بیخ‌های درخت و دهد ثمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غوطی بخورد جان به تک بحر و شد گهر</p></div>
<div class="m2"><p>این بحر و این گهر ز پی لعل توست زار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نغمه‌های طوطی شکرستان توست</p></div>
<div class="m2"><p>در رقص شاخ بید و دو دستک زنان چنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بعد ماجرای صفا صوفیان عشق</p></div>
<div class="m2"><p>گیرند یک دگر را چون مستیان کنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مستانه جان برون جهد از وحدت الست</p></div>
<div class="m2"><p>چون سیل سوی بحر نه آرام و نه قرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جزوی چو تیر جسته ز قبضه کمان کل</p></div>
<div class="m2"><p>او را نشانه نیست به جز کل و نی گذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جانیست خوش برون شده از صد هزار پوست</p></div>
<div class="m2"><p>در چاربالش ابد او راست کار و بار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان‌های صادقان همه در وی زنند چنگ</p></div>
<div class="m2"><p>تا بانوا شوند از آن جان نامدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان‌ها گرفته دامنش از عشق و او چو قطب</p></div>
<div class="m2"><p>بگرفته دامن ازل محض مردوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تبریز رو دلا و ز شمس حق این بپرس</p></div>
<div class="m2"><p>تا بر براق سر معانی شوی سوار</p></div></div>