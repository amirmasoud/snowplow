---
title: >-
    غزل شمارهٔ ۱۸۵۸
---
# غزل شمارهٔ ۱۸۵۸

<div class="b" id="bn1"><div class="m1"><p>چو افتم من ز عشق دل به پای دلربای من</p></div>
<div class="m2"><p>از آن شادی بیاید جان نهان افتد به پای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر روزی در آن خدمت کنم تقصیر چون خامان</p></div>
<div class="m2"><p>شود دل خصم جان من کند هجران سزای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحرگاهان دعا کردم که این جان باد خاک او</p></div>
<div class="m2"><p>شنیدم نعره آمین ز جان اندر دعای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه راه برد این دل به سوی دلبر پنهان</p></div>
<div class="m2"><p>چگونه بوی برد این جان که هست او جان فزای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی جامی به پیش آورد من از ناز گفتم نی</p></div>
<div class="m2"><p>بگفتا نی مگو بستان برای اقتضای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو از صافش چشیدم من مرا درداد یک دردی</p></div>
<div class="m2"><p>یکی دردی گران خواری که کامل شد صفای من</p></div></div>