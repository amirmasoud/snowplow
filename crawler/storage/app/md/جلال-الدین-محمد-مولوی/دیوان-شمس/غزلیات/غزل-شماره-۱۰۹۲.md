---
title: >-
    غزل شمارهٔ ۱۰۹۲
---
# غزل شمارهٔ ۱۰۹۲

<div class="b" id="bn1"><div class="m1"><p>اختران را شب وصلست و نثارست و نثار</p></div>
<div class="m2"><p>چون سوی چرخ عروسیست ز ماه ده و چار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهره در خویش نگنجد ز نواهای لطیف</p></div>
<div class="m2"><p>همچو بلبل که شود مست ز گل فصل بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جدی را بین به کرشمه به اسد می‌نگرد</p></div>
<div class="m2"><p>حوت را بین که ز دریا چه برآورد غبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشتری اسب دوانید سوی پیر زحل</p></div>
<div class="m2"><p>که جوانی تو ز سر گیر و بر او مژده بیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کف مریخ که پرخون بود از قبضه تیغ</p></div>
<div class="m2"><p>گشت جان بخش چو خورشید مشرف آثار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلو گردون چو از آن آب حیات آمد پر</p></div>
<div class="m2"><p>شود آن سنبله خشک از او گوهربار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوز پرمغز ز میزان و شکستن نرمد</p></div>
<div class="m2"><p>حمل از مادر خود کی بگریزد به نفار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیر غمزه چو رسید از سوی مه بر دل قوس</p></div>
<div class="m2"><p>شب روی پیشه گرفت از هوسش عقرب وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر این عید برو گاو فلک قربان کن</p></div>
<div class="m2"><p>گر نه‌ای چون سرطان در وحلی کژرفتار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این فلک هست سطرلاب و حقیقت عشقست</p></div>
<div class="m2"><p>هر چه گوییم از این گوش سوی معنی دار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس تبریز در آن صبح که تو درتابی</p></div>
<div class="m2"><p>روز روشن شود از روی چو ماهت شب تار</p></div></div>