---
title: >-
    غزل شمارهٔ ۱۰۸۱
---
# غزل شمارهٔ ۱۰۸۱

<div class="b" id="bn1"><div class="m1"><p>ای صبا حالی ز خد و خال شمس الدین بیار</p></div>
<div class="m2"><p>عنبر و مشک ختن از چین به قسطنطین بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سلامی از لب شیرین او داری بگو</p></div>
<div class="m2"><p>ور پیامی از دل سنگین او داری بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر چه باشد تا فدای پای شمس الدین کنم</p></div>
<div class="m2"><p>نام شمس الدین بگو تا جان کنم بر او نثار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلعت خیر و لباس از عشق او دارد دلم</p></div>
<div class="m2"><p>حسن شمس الدین دثار و عشق شمس الدین شعار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به بوی شمس دین سرخوش شدیم و می‌رویم</p></div>
<div class="m2"><p>ما ز جام شمس دین مستیم ساقی می میار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما دماغ از بوی شمس الدین معطر کرده‌ایم</p></div>
<div class="m2"><p>فارغیم از بوی عود و عنبر و مشک تتار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس دین بر دل مقیم و شمس دین بر جان کریم</p></div>
<div class="m2"><p>شمس دین در یتیم و شمس دین نقد عیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من نه تنها می‌سرایم شمس دین و شمس دین</p></div>
<div class="m2"><p>می‌سراید عندلیب از باغ و کبک از کوهسار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن حوران شمس دین و باغ رضوان شمس دین</p></div>
<div class="m2"><p>عین انسان شمس دین و شمس دین فخر کبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز روشن شمس دین و چرخ گردان شمس دین</p></div>
<div class="m2"><p>گوهر کان شمس دین و شمس دین لیل و نهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس دین جام جمست و شمس دین بحر عظیم</p></div>
<div class="m2"><p>شمس دین عیسی دم است و شمس دین یوسف عذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خدا خواهم ز جان خوش دولتی با او نهان</p></div>
<div class="m2"><p>جان ما اندر میان و شمس دین اندر کنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس دین خوشتر ز جان و شمس دین شکرستان</p></div>
<div class="m2"><p>شمس دین سرو روان و شمس دین باغ و بهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمس دین نقل و شراب و شمس دین چنگ و رباب</p></div>
<div class="m2"><p>شمس دین خمر و خمار و شمس دین هم نور و نار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نی خماری کز وی آید انده و حزن و ندم</p></div>
<div class="m2"><p>آن خمار شمس دین کز وی فزاید افتخار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای دلیل بی‌دلان و ای رسول عاشقان</p></div>
<div class="m2"><p>شمس تبریزی بیا زنهار دست از ما مدار</p></div></div>