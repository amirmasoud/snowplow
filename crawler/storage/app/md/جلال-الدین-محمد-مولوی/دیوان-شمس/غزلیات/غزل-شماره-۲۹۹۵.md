---
title: >-
    غزل شمارهٔ ۲۹۹۵
---
# غزل شمارهٔ ۲۹۹۵

<div class="b" id="bn1"><div class="m1"><p>اندر میان جمع چه جان است آن یکی</p></div>
<div class="m2"><p>یک جان نخوانمش که جهان است آن یکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوگند می‌خورم به جمال و کمال او</p></div>
<div class="m2"><p>کز چشم خویش هم پنهان است آن یکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر فرق خاک آب روان کرد عشق او</p></div>
<div class="m2"><p>در باغ عشق سرو روان است آن یکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله شکوفه‌اند اگر میوه است او</p></div>
<div class="m2"><p>جمله قراضه‌اند چو کان است آن یکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل موج می‌زند ز صفاتش ولی خموش</p></div>
<div class="m2"><p>زیرا فزون ز شرح و بیان است آن یکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی که او بزاد زمین و زمان نبود</p></div>
<div class="m2"><p>بالاتر از زمین و زمان است آن یکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قفلی است بر دهان من از رشک عاشقان</p></div>
<div class="m2"><p>تا من نگویم این که فلان است آن یکی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دم که کنج چشمم بر روی او فتد</p></div>
<div class="m2"><p>گویم که ای خدای چه سان است آن یکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چشم درد نیست تو را چشم باز کن</p></div>
<div class="m2"><p>زیرا چو آفتاب عیان است آن یکی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیشش تو سجده می‌کن تا پادشا شوی</p></div>
<div class="m2"><p>زیرا که پادشاه نشان است آن یکی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر صد هزار خلق تو را رهزند که نیست</p></div>
<div class="m2"><p>اندر گمان مباش که آن است آن یکی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم به شمس مفخر تبریز بنگرش</p></div>
<div class="m2"><p>گفتا عجب مدار چنان است آن یکی</p></div></div>