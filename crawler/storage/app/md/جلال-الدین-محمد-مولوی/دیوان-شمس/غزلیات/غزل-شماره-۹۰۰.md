---
title: >-
    غزل شمارهٔ ۹۰۰
---
# غزل شمارهٔ ۹۰۰

<div class="b" id="bn1"><div class="m1"><p>بگیر دامن لطفش که ناگهان بگریزد</p></div>
<div class="m2"><p>ولی مکش تو چو تیرش که از کمان بگریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه نقش‌ها که ببازد چه حیله‌ها که بسازد</p></div>
<div class="m2"><p>به نقش حاضر باشد ز راه جان بگریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آسمانش بجویی چو مه ز آب بتابد</p></div>
<div class="m2"><p>در آب چونک درآیی بر آسمان بگریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز لامکانش بخوانی نشان دهد به مکانت</p></div>
<div class="m2"><p>چو در مکانش بجویی به لامکان بگریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه پیک تیزرو اندر وجود مرغ گمانست</p></div>
<div class="m2"><p>یقین بدان که یقین وار از گمان بگریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از این و آن بگریزم ز ترس نی ز ملولی</p></div>
<div class="m2"><p>که آن نگار لطیفم از این و آن بگریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریزپای چو بادم ز عشق گل نه گلی که</p></div>
<div class="m2"><p>ز بیم باد خزانی ز بوستان بگریزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان گریزد نامش چو قصد گفتن بیند</p></div>
<div class="m2"><p>که گفت نیز نتانی که آن فلان بگریزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان گریزد از تو که گر نویسی نقشش</p></div>
<div class="m2"><p>ز لوح نقش بپرد ز دل نشان بگریزد</p></div></div>