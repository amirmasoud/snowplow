---
title: >-
    غزل شمارهٔ ۴۷۳
---
# غزل شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>هر نفس آواز عشق می‌رسد از چپ و راست</p></div>
<div class="m2"><p>ما به چمن می‌رویم عزم تماشا که راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوبت خانه گذشت نوبت بستان رسید</p></div>
<div class="m2"><p>صبح سعادت دمید وقت وصال و لقاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شه صاحب قران خیز ز خواب گران</p></div>
<div class="m2"><p>مرکب دولت بران نوبت وصل آن ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبل وفا کوفتند راه سما روفتند</p></div>
<div class="m2"><p>عیش شما نقد شد نسیه فردا کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روم برآورد دست زنگی شب را شکست</p></div>
<div class="m2"><p>عالم بالا و پست پرلمعان و صفاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خنک آن را که او رست از این رنگ و بو</p></div>
<div class="m2"><p>زانک جز این رنگ و بو در دل و جان رنگ‌هاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خنک آن جان و دل کو رهد از آب و گل</p></div>
<div class="m2"><p>گر چه در این آب و گل دستگه کیمیاست</p></div></div>