---
title: >-
    غزل شمارهٔ ۱۳۰۵
---
# غزل شمارهٔ ۱۳۰۵

<div class="b" id="bn1"><div class="m1"><p>کعبه جان‌ها تویی گرد تو آرم طواف</p></div>
<div class="m2"><p>جغد نیم بر خراب هیچ ندارم طواف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشه ندارم جز این کار ندارم جز این</p></div>
<div class="m2"><p>چون فلکم روز و شب پیشه و کارم طواف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهتر از این یار کیست خوشتر از این کار چیست</p></div>
<div class="m2"><p>پیش بت من سجود گرد نگارم طواف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخت کشیدم به حج تا کنم آن جا قرار</p></div>
<div class="m2"><p>برد عرب رخت من برد قرارم طواف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنه چه بیند به خواب چشمه و حوض و سبو</p></div>
<div class="m2"><p>تشنه وصل توام کی بگذارم طواف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونک برآرم سجود بازرهم از وجود</p></div>
<div class="m2"><p>کعبه شفیعم شود چونک گزارم طواف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجی عاقل طواف چند کند هفت هفت</p></div>
<div class="m2"><p>حاجی دیوانه‌ام من نشمارم طواف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم گل را که خار کیست ز پیشش بران</p></div>
<div class="m2"><p>گفت بسی کرد او گرد عذارم طواف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت به آتش هوا دود نه درخورد توست</p></div>
<div class="m2"><p>گفت بهل تا کند گرد شرارم طواف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق مرا می‌ستود کو همه شب همچو ماه</p></div>
<div class="m2"><p>بر سر و رو می‌کند گرد غبارم طواف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو فلک می‌کند بر سر خاکم سجود</p></div>
<div class="m2"><p>همچو قدح می‌کند گرد خمارم طواف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواجه عجب نیست اینک من بدوم پیش صید</p></div>
<div class="m2"><p>طرفه که بر گرد من کرد شکارم طواف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چار طبیعت چو چار گردن حمال دان</p></div>
<div class="m2"><p>همچو جنازه مبا بر سر چارم طواف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هست اثرهای یار در دمن این دیار</p></div>
<div class="m2"><p>ور نه نبودی بر این تیره دیارم طواف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاشق مات ویم تا ببرد رخت من</p></div>
<div class="m2"><p>ور نه نبودی چنین گرد قمارم طواف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرو بلندم که من سبز و خوشم در خزان</p></div>
<div class="m2"><p>نی چو حشیشم بود گرد بهارم طواف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از سپه رشک ما تیر قضا می‌رسد</p></div>
<div class="m2"><p>تا نکنی بی‌سپر گرد حصارم طواف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خشت وجود مرا خرد کن ای غم چو گرد</p></div>
<div class="m2"><p>تا که کنم همچو گرد گرد سوارم طواف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بس کن و چون ماهیان باش خموش اندر آب</p></div>
<div class="m2"><p>تا نه چو تابه شود بر سر نارم طواف</p></div></div>