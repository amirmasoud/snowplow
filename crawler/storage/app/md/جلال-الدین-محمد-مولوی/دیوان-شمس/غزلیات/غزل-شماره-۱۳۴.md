---
title: >-
    غزل شمارهٔ ۱۳۴
---
# غزل شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>ساقیا در نوش آور شیره عنقود را</p></div>
<div class="m2"><p>در صبوح آور سبک مستان خواب آلود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک به یک در آب افکن جمله تر و خشک را</p></div>
<div class="m2"><p>اندر آتش امتحان کن چوب را و عود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی شورستان روان کن شاخی از آب حیات</p></div>
<div class="m2"><p>چون گل نسرین بخندان خار غم فرسود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبلان را مست گردان مطربان را شیرگیر</p></div>
<div class="m2"><p>تا که درسازند با هم نغمه داوود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بادپیما بادپیمایان خود را آب ده</p></div>
<div class="m2"><p>کوری آن حرص افزون جوی کم پیمود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم بزن بر صافیان آن درد دردانگیز را</p></div>
<div class="m2"><p>هم بخور با صوفیان پالوده بی‌دود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می میاور زان بیاور که می از وی جوش کرد</p></div>
<div class="m2"><p>آنک جوشش در وجود آورد هر موجود را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان میی کاندر جبل انداخت صد رقص الجمل</p></div>
<div class="m2"><p>زان میی کو روشنی بخشد دل مردود را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر صباحی عید داریم از تو خاصه این صبوح</p></div>
<div class="m2"><p>کز کرم بر می فشانی باده موعود را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برفشان چندانک ما افشانده گردیم از وجود</p></div>
<div class="m2"><p>تا که هر قاصد بیابد در فنا مقصود را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو آبی دیده در خود آفتاب و ماه را</p></div>
<div class="m2"><p>چون ایازی دیده در خود هستی محمود را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شمس تبریزی برآر از چاه مغرب مشرقی</p></div>
<div class="m2"><p>همچو صبحی کو برآرد خنجر مغمود را</p></div></div>