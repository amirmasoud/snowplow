---
title: >-
    غزل شمارهٔ ۱۱۴۰
---
# غزل شمارهٔ ۱۱۴۰

<div class="b" id="bn1"><div class="m1"><p>بیامدیم دگربار چون نسیم بهار</p></div>
<div class="m2"><p>برآمدیم چو خورشید با صد استظهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آفتاب تموزیم رغم فصل عجوز</p></div>
<div class="m2"><p>فکنده غلغل و شادی میانه گلزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار فاخته جویان ما که کو کوکو</p></div>
<div class="m2"><p>هزار بلبل و طوطی به سوی ما طیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ماهیان خبر ما رسید در دریا</p></div>
<div class="m2"><p>هزار موج برآورد جوش دریابار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ذات پاک خدایی که گوش و هوش دهد</p></div>
<div class="m2"><p>که در جهان نگذاریم یک خرد هشیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مصطفی و به هر چار یار فاضل او</p></div>
<div class="m2"><p>که پنج نوبت ما می‌زنند در اسرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیامدیم ز مصر و دو صد قطار شکر</p></div>
<div class="m2"><p>تو هیچ کار مکن جز که نیشکر مفشار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبات مصر چه حاجت که شمس تبریزی</p></div>
<div class="m2"><p>دو صد نبات بریزد ز لفظ شکربار</p></div></div>