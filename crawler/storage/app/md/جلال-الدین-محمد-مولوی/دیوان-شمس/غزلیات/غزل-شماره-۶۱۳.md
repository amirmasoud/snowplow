---
title: >-
    غزل شمارهٔ ۶۱۳
---
# غزل شمارهٔ ۶۱۳

<div class="b" id="bn1"><div class="m1"><p>ای خواجه بازرگان از مصر شکر آمد</p></div>
<div class="m2"><p>وان یوسف چون شکر ناگه ز سفر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح آمد و راح آمد معجون نجاح آمد</p></div>
<div class="m2"><p>ور چیز دگر خواهی آن چیز دگر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن میوه یعقوبی وان چشمه ایوبی</p></div>
<div class="m2"><p>از منظره پیدا شد هنگام نظر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر از کرم ایزد بر آب حیاتی زد</p></div>
<div class="m2"><p>نک زهره غزل گویان در برج قمر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد شه معراجی شب رست ز محتاجی</p></div>
<div class="m2"><p>گردون به نثار او با دامن زر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسی نهان آمد صد چشمه روان آمد</p></div>
<div class="m2"><p>جان همچو عصا آمد تن همچو حجر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین مردم کارافزا زین خانه پرغوغا</p></div>
<div class="m2"><p>عیسی نخورد حلوا کاین آخر خر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بسته نبود آن دم در شش جهت عالم</p></div>
<div class="m2"><p>در جستن او گردون بس زیر و زبر آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن کو مثل هدهد بی‌تاج نبد هرگز</p></div>
<div class="m2"><p>چون مور ز مادر او بربسته کمر آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عشق بود بالغ از تاج و کمر فارغ</p></div>
<div class="m2"><p>کز کرسی و از عرشش منشور ظفر آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باقیش ز سلطان جو سلطان سخاوت خو</p></div>
<div class="m2"><p>زو پرس خبرها را کو کان خبر آمد</p></div></div>