---
title: >-
    غزل شمارهٔ ۳۰۰۳
---
# غزل شمارهٔ ۳۰۰۳

<div class="b" id="bn1"><div class="m1"><p>ای کاشکی تو خویش زمانی بدانیی</p></div>
<div class="m2"><p>وز روی خوب خویشت بودی نشانیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آب و گل تو همچو ستوران نخفتیی</p></div>
<div class="m2"><p>خود را به عیش خانه خوبان کشانیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گرد خویش گشتی کاظهار خود کنی</p></div>
<div class="m2"><p>پنهان بماند زیر تو گنج نهانیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از روح بی‌خبر بدیی گر تو جسمیی</p></div>
<div class="m2"><p>در جان قرار داشتیی گر تو جانیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با نیک و بد بساختیی همچو دیگران</p></div>
<div class="m2"><p>با این و آنیی تو اگر این و آنیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ذوق بودیی تو اگر یک اباییی</p></div>
<div class="m2"><p>یک نوع جوشییی چو یکی قازغانیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین جوش در دوار اگر صاف گشتیی</p></div>
<div class="m2"><p>چون صاف گشتگان تو بر این آسمانیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی به هر خیال که جان و جهان من</p></div>
<div class="m2"><p>گر گم شدی خیال تو جان و جهانیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس کن که بند عقل شدست این زبان تو</p></div>
<div class="m2"><p>ور نی چو عقل کلی جمله زبانیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس کن که دانش‌ست که محجوب دانشست</p></div>
<div class="m2"><p>دانستیی که شاهی کی ترجمانیی</p></div></div>