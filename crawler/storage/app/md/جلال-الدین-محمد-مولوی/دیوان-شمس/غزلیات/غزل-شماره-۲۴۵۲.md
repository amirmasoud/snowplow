---
title: >-
    غزل شمارهٔ ۲۴۵۲
---
# غزل شمارهٔ ۲۴۵۲

<div class="b" id="bn1"><div class="m1"><p>ای دل نگویی چون شدی ور عشق روزافزون شدی</p></div>
<div class="m2"><p>گاهی ز غم مجنون شدی گاهی ز محنت خون شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو چون دم زدم صد فتنه شد اندر عدم</p></div>
<div class="m2"><p>ای مطرب شیرین قدم می‌زن نوا تا صبحدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که شد هنگام می ما غرقه اندر وام می</p></div>
<div class="m2"><p>نی نی رها کن نام می مستان نگر بی‌جام می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو همچو آتش سرکشی من همچون خاکم مفرشی</p></div>
<div class="m2"><p>در من زدی تو آتشی خوشی خوشی خوشی خوشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نیست بر هستی بزن بر عیش سرمستی بزن</p></div>
<div class="m2"><p>دل بر دل مستی بزن دستی بزن دستی بزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم مها در ما نگر در چشم چون دریا نگر</p></div>
<div class="m2"><p>آن جا مرو این جا نگر گفتا که خه سودا نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بلبل از گلشن بگو زان سرو و زان سوسن بگو</p></div>
<div class="m2"><p>زان شاخ آبستن بگو پنهان مکن روشن بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر همه صورت مبین بنگر به جان نازنین</p></div>
<div class="m2"><p>کز تابش روح الامین چون چرخ شد روی زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر نقش چون اسپر بود در دست صورتگر بود</p></div>
<div class="m2"><p>صورت یکی چادر بود در پرده آزر بود</p></div></div>