---
title: >-
    غزل شمارهٔ ۲۷۰۵
---
# غزل شمارهٔ ۲۷۰۵

<div class="b" id="bn1"><div class="m1"><p>مرا هر لحظه منزل آسمانی</p></div>
<div class="m2"><p>تو را هر دم خیالی و گمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گویی کو طمع کرده‌ست در من</p></div>
<div class="m2"><p>جهانی زین خیال اندر زیانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آن چشم دروغت طمع کردم</p></div>
<div class="m2"><p>که چون دوزخ نمودستت جنانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آن عقل خسیست طمع کردم</p></div>
<div class="m2"><p>که جان دادی برای خاکدانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه نور افزاید از برق آفتابی</p></div>
<div class="m2"><p>چه بربندد ز ویرانی جهانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز یک قطره چه خواهد خورد بحری</p></div>
<div class="m2"><p>ز یک حبه چه دزدد گنج و کانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه رونق یا چه آرایش فزاید</p></div>
<div class="m2"><p>ز پژمرده گیایی گلستانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حق نور چشم دلبر من</p></div>
<div class="m2"><p>که روشنتر از این نبود نشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حق آن دو لعل قندبارش</p></div>
<div class="m2"><p>که شرح آن نگنجد در دهانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که مقصودم گشاد سینه‌ای بود</p></div>
<div class="m2"><p>نه طمع آنک بگشایم دکانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غرض تا نانی آن جا پخته گردد</p></div>
<div class="m2"><p>نه آنک درربایم از تو نانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بهمان و فلان تو فارغ آیند</p></div>
<div class="m2"><p>طمع آن نی که گویندم فلانی</p></div></div>