---
title: >-
    غزل شمارهٔ ۸۴۶
---
# غزل شمارهٔ ۸۴۶

<div class="b" id="bn1"><div class="m1"><p>بیمار رنج صفرا ذوق شکر نداند</p></div>
<div class="m2"><p>هر سنگ دل در این ره قلب از گهر نداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر عنکبوت جوله در تار و پود آن چه</p></div>
<div class="m2"><p>از ذوق صنعت خود ذوق دگر نداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وان کو ز چه برافتد در جام و ساغر افتد</p></div>
<div class="m2"><p>مستیش در سر افتد پا را ز سر نداند</p></div></div>