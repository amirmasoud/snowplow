---
title: >-
    غزل شمارهٔ ۱۱۶۴
---
# غزل شمارهٔ ۱۱۶۴

<div class="b" id="bn1"><div class="m1"><p>خلق را زیر گنبد دوار</p></div>
<div class="m2"><p>چشم‌ها کور و دیدنی بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جور او کش از آنک شورش دل</p></div>
<div class="m2"><p>نور چشمست یا اولوالابصار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دو دیده نهم غمت کاین درد</p></div>
<div class="m2"><p>داروی خاص خسرویست به بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغ جان خوش ز سنگ بارانست</p></div>
<div class="m2"><p>ما نخواهیم قطره سنگ ببار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمس تبریز گوهر عشقست</p></div>
<div class="m2"><p>گوهر عشق را تو خوار مدار</p></div></div>