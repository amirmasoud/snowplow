---
title: >-
    غزل شمارهٔ ۱۸۴۲
---
# غزل شمارهٔ ۱۸۴۲

<div class="b" id="bn1"><div class="m1"><p>چهره شرمگین تو بستد شرمگان من</p></div>
<div class="m2"><p>شور تو کرد عاقبت فتنه و شر مکان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه که نشانده تو است لابه کنان به پیش تو</p></div>
<div class="m2"><p>پیش خودم نشان دمی ای شه خوش نشان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره تو کمین خسم از ره دور می رسم</p></div>
<div class="m2"><p>ای دل من به دست تو بشنو داستان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد فلک همی‌دوم پر و تهی همی‌شوم</p></div>
<div class="m2"><p>زانک قرار برده‌ای ای دل و جان ز جان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد تو گشتمی ولی گرد کجاست مر تو را</p></div>
<div class="m2"><p>گرد در تو می دوم ای در تو امان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق برید ناف من بر تو بود طواف من</p></div>
<div class="m2"><p>لاف من و گزاف من پیش تو ترجمان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه همه لعل می شوم گاه چو نعل می شوم</p></div>
<div class="m2"><p>تا کرمت بگویدم باز درآ به کان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت مرا که چند چند سیر نگشتی از سخن</p></div>
<div class="m2"><p>زانک سوی تو می رود این سخن روان من</p></div></div>