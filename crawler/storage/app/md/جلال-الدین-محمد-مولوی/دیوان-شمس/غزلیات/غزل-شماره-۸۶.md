---
title: >-
    غزل شمارهٔ ۸۶
---
# غزل شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای گشته ز تو خندان بستان و گل رعنا</p></div>
<div class="m2"><p>پیوسته چنین بادا چون شیر و شکر با ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چرخ تو را بنده وی خلق ز تو زنده</p></div>
<div class="m2"><p>احسنت زهی خوابی شاباش زهی زیبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریای جمال تو چون موج زند ناگه</p></div>
<div class="m2"><p>پرگنج شود پستی فردوس شود بالا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سوی که روی آری در پیش تو گل روید</p></div>
<div class="m2"><p>هر جا که روی آیی فرشت همه زر بادا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان دم که ز بدخویی دشنام و جفا گویی</p></div>
<div class="m2"><p>می‌گو که جفای تو حلواست همه حلوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه دل سنگستش بنگر که چه رنگستش</p></div>
<div class="m2"><p>کز مشعله ننگستش وز رنگ گل حمرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رب دل بازش ده صد عمر درازش ده</p></div>
<div class="m2"><p>فخرش ده و نازش ده تا فخر بود ما را</p></div></div>