---
title: >-
    غزل شمارهٔ ۲۴۱۸
---
# غزل شمارهٔ ۲۴۱۸

<div class="b" id="bn1"><div class="m1"><p>ای جان ای جان فی ستر الله</p></div>
<div class="m2"><p>اشتر می‌ران فی ستر الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام آتش درکش درکش</p></div>
<div class="m2"><p>پیش سلطان فی ستر الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساغر تا لب می‌خور تا شب</p></div>
<div class="m2"><p>اندر میدان فی ستر الله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمش را بین خشمش را بین</p></div>
<div class="m2"><p>پنهان پنهان فی ستر الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاری شنگی پروین رنگی</p></div>
<div class="m2"><p>آمد مهمان فی ستر الله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدم مستش خستم دستش</p></div>
<div class="m2"><p>آسان آسان فی ستر الله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی برجه باده درده</p></div>
<div class="m2"><p>پنگان پنگان فی ستر الله</p></div></div>