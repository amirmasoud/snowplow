---
title: >-
    غزل شمارهٔ ۱۷۲۵
---
# غزل شمارهٔ ۱۷۲۵

<div class="b" id="bn1"><div class="m1"><p>نگفتمت مرو آن جا که آشنات منم</p></div>
<div class="m2"><p>در این سراب فنا چشمه حیات منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر به خشم روی صد هزار سال ز من</p></div>
<div class="m2"><p>به عاقبت به من آیی که منتهات منم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگفتمت که به نقش جهان مشو راضی</p></div>
<div class="m2"><p>که نقش بند سراپرده رضات منم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگفتمت که منم بحر و تو یکی ماهی</p></div>
<div class="m2"><p>مرو به خشک که دریای باصفات منم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگفتمت که چو مرغان به سوی دام مرو</p></div>
<div class="m2"><p>بیا که قدرت پرواز و پرّ و پات منم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگفتمت که تو را ره زنند و سرد کنند</p></div>
<div class="m2"><p>که آتش و تبش و گرمی هوات منم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگفتمت که صفت‌های زشت در تو نهند</p></div>
<div class="m2"><p>که گم کنی که سر چشمه صفات منم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگفتمت که مگو کار بنده از چه جهت</p></div>
<div class="m2"><p>نظام گیرد خلاق بی‌جهات منم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چراغ دلی دان که راه خانه کجاست</p></div>
<div class="m2"><p>وگر خداصفتی دان که کدخدات منم</p></div></div>