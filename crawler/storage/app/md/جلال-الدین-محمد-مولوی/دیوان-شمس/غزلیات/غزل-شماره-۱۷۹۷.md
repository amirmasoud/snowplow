---
title: >-
    غزل شمارهٔ ۱۷۹۷
---
# غزل شمارهٔ ۱۷۹۷

<div class="b" id="bn1"><div class="m1"><p>ای دل شکایت‌ها مکن تا نشنود دلدار من</p></div>
<div class="m2"><p>ای دل نمی‌ترسی مگر از یار بی‌زنهار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل مرو در خون من در اشک چون جیحون من</p></div>
<div class="m2"><p>نشنیده‌ای شب تا سحر آن ناله‌های زار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یادت نمی‌آید که او می کرد روزی گفت گو</p></div>
<div class="m2"><p>می گفت بس دیگر مکن اندیشه گلزار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندازه خود را بدان نامی مبر زین گلستان</p></div>
<div class="m2"><p>این بس نباشد خود تو را کآگه شوی از خار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم امانم ده به جان خواهم که باشی این زمان</p></div>
<div class="m2"><p>تو سرده و من سرگران ای ساقی خمار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خندید و می گفت ای پسر آری ولیک از حد مبر</p></div>
<div class="m2"><p>وانگه چنین می کرد سر کای مست و ای هشیار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون لطف دیدم رای او افتادم اندر پای او</p></div>
<div class="m2"><p>گفتم نباشم در جهان گر تو نباشی یار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتا مباش اندر جهان تا روی من بینی عیان</p></div>
<div class="m2"><p>خواهی چنین گم شو چنان در نفی خود دان کار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم منم در دام تو چون گم شوم بی‌جام تو</p></div>
<div class="m2"><p>بفروش یک جامم به جان وانگه ببین بازار من</p></div></div>