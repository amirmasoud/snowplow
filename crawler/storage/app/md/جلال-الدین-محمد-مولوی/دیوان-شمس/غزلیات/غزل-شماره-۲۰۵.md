---
title: >-
    غزل شمارهٔ ۲۰۵
---
# غزل شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>چند گریزی ز ما چند روی جا به جا</p></div>
<div class="m2"><p>جان تو در دست ماست همچو گلوی عصا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند بکردی طواف گرد جهان از گزاف</p></div>
<div class="m2"><p>زین رمه پر ز لاف هیچ تو دیدی وفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز دو سه‌ای زحیر گرد جهان گشته گیر</p></div>
<div class="m2"><p>همچو سگان مرده گیر گرسنه و بی‌نوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرده دل و مرده جو چون پسر مرده شو</p></div>
<div class="m2"><p>از کفن مرده ایست در تن تو آن قبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنده ندیدی که تا مرده نماید تو را</p></div>
<div class="m2"><p>چند کشی در کنار صورت گرمابه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن تو پرسفال پیش تو آن زر و مال</p></div>
<div class="m2"><p>باورم آنگه کنی که اجل آرد فنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی که زر کهن من چه کنم بخش کن</p></div>
<div class="m2"><p>من به سما می‌روم نیست زر آن جا روا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جغد نه‌ای بلبلی از چه در این منزلی</p></div>
<div class="m2"><p>باغ و چمن را چه شد سبزه و سرو و صبا</p></div></div>