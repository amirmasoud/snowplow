---
title: >-
    غزل شمارهٔ ۳۱۳۶
---
# غزل شمارهٔ ۳۱۳۶

<div class="b" id="bn1"><div class="m1"><p>خواهی ز جنون بویی ببری</p></div>
<div class="m2"><p>ز اندیشه و غم می‌باش بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تنگ دلی از بهر قبا</p></div>
<div class="m2"><p>جانت نکند زرین کمری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی عشق تو را محرم شمرد</p></div>
<div class="m2"><p>تا همچو خسان زر می‌شمری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فوق همه‌ای چون نور شوی</p></div>
<div class="m2"><p>تا نور نه‌ای در زیر دری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیزم بود آن چوبی که نسوخت</p></div>
<div class="m2"><p>چون سوخته شد باشد شرری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وانگه شررش وا اصل رود</p></div>
<div class="m2"><p>همچون شرر جان بشری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمه بود آن کز چشم جداست</p></div>
<div class="m2"><p>در چشم رود گردد نظری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک قطره بود در ابر گران</p></div>
<div class="m2"><p>در بحر فتد یابد گهری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار سیهی بد سوختنی</p></div>
<div class="m2"><p>گردش گل تر باد سحری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک لقمه نان چون کوفته شد</p></div>
<div class="m2"><p>جان گشت و کند نان جانوری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خون گشت غذا در پیشه وری</p></div>
<div class="m2"><p>آن لقمه کند هم پیشه وری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر زانک بلا کوبد دل تو</p></div>
<div class="m2"><p>از عین بلانوشی بچری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور زانک اجل کوبد سر تو</p></div>
<div class="m2"><p>دانی پس از آن که جمله سری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بیضه تن مرغ عجبی</p></div>
<div class="m2"><p>در بیضه دری ز آن می‌نپری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر بیضه تن سوراخ شود</p></div>
<div class="m2"><p>هم پر بزنی هم جان ببری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سودای سفر از ذکر بود</p></div>
<div class="m2"><p>از ذکر شود مردم سفری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو در حضری وین وهم سفر</p></div>
<div class="m2"><p>پنداشت توست از بی‌هنری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا رب برهان زین وهم کژش</p></div>
<div class="m2"><p>تو وهم نهی در دیو و پری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون در حضری بربند دهان</p></div>
<div class="m2"><p>در ذکر مرو چون در حضری</p></div></div>