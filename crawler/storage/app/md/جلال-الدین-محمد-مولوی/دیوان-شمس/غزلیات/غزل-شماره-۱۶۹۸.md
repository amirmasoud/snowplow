---
title: >-
    غزل شمارهٔ ۱۶۹۸
---
# غزل شمارهٔ ۱۶۹۸

<div class="b" id="bn1"><div class="m1"><p>ای توبه‌ام شکسته از تو کجا گریزم</p></div>
<div class="m2"><p>ای در دلم نشسته از تو کجا گریزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نور هر دو دیده بی‌تو چگونه بینم</p></div>
<div class="m2"><p>وی گردنم ببسته از تو کجا گریزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شش جهت ز نورت چون آینه‌ست شش رو</p></div>
<div class="m2"><p>وی روی تو خجسته از تو کجا گریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بود از تو خسته جان بود از تو رسته</p></div>
<div class="m2"><p>جان نیز گشت خسته از تو کجا گریزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بندم این بصر را ور بسکلم نظر را</p></div>
<div class="m2"><p>از دل نه‌ای گسسته از تو کجا گریزم</p></div></div>