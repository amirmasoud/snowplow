---
title: >-
    غزل شمارهٔ ۱۰۲۴
---
# غزل شمارهٔ ۱۰۲۴

<div class="b" id="bn1"><div class="m1"><p>مرا همچون پدر بنگر نه همچون شوهر مادر</p></div>
<div class="m2"><p>پدر را نیک واقف دان از آن کژبازی مضمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گردی راست اولیتر از آنک کژ نهی او را</p></div>
<div class="m2"><p>وگر تو کژ نهی او را به استیزت کند کژتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بابا بشنو و برجه که سلطانیت می‌خواند</p></div>
<div class="m2"><p>که خاک اوت کیخسرو بمیرد پیش او سنجر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ان الله یدعو را شنیدی کژ مکن رو را</p></div>
<div class="m2"><p>زهی راعی زهی داعی زهی راه و زهی رهبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پراکنده شدی ای جان به هر درد و به هر درمان</p></div>
<div class="m2"><p>ز عشقش جوی جمعیت در آن جامع بنه منبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو کر و فر او دیدی تویی کرار و شیر حق</p></div>
<div class="m2"><p>چو بال و پر او دیدی تویی طیار چون جعفر</p></div></div>