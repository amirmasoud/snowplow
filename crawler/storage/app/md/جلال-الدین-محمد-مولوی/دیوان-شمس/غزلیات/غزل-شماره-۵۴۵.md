---
title: >-
    غزل شمارهٔ ۵۴۵
---
# غزل شمارهٔ ۵۴۵

<div class="b" id="bn1"><div class="m1"><p>بی تو به سر می نشود با دگری می‌نشود</p></div>
<div class="m2"><p>هر چه کنم عشق بیان بی‌جگری می‌نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک دوان هر سحری از دلم آرد خبری</p></div>
<div class="m2"><p>هیچ کسی را ز دلم خود خبری می‌نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک سر مو از غم تو نیست که اندر تن من</p></div>
<div class="m2"><p>آب حیاتی ندهد یا گهری می‌نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای غم تو راحت جان چیستت این جمله فغان</p></div>
<div class="m2"><p>تا بزنم بانگ و فغان خود حشری می‌نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میل تو سوی حشرست پیشه تو شور و شرست</p></div>
<div class="m2"><p>بی ره و رای تو شها رهگذری می‌نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چیست حشر از خود خود رفتن جان‌ها به سفر</p></div>
<div class="m2"><p>مرغ چو در بیضه خود بال و پری می‌نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیست چو خورشید اگر تابد اندر شب من</p></div>
<div class="m2"><p>تا تو قدم درننهی خود سحری می‌نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانه دل کاشته‌ای زیر چنین آب و گلی</p></div>
<div class="m2"><p>تا به بهارت نرسد او شجری می‌نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غزلم جبر و قدر هست از این دو بگذر</p></div>
<div class="m2"><p>زانک از این بحث به جز شور و شری می‌نشود</p></div></div>