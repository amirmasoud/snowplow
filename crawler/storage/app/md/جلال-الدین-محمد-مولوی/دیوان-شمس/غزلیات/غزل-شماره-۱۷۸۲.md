---
title: >-
    غزل شمارهٔ ۱۷۸۲
---
# غزل شمارهٔ ۱۷۸۲

<div class="b" id="bn1"><div class="m1"><p>ظننتم ایا عذال ان قد عدلتم</p></div>
<div class="m2"><p>تظنون ان الحق فیما عذلتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و ما ضاء ذاک البدر الا لاهله</p></div>
<div class="m2"><p>و غادرکم انواره فضللتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فما مل من ذاق الصبابه و الهوی</p></div>
<div class="m2"><p>و انکم ما ذقتم فمللتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و ان ذقتموا ما ذقتموه بحقها</p></div>
<div class="m2"><p>و لا مشرب العشاق یوما وصلتم</p></div></div>