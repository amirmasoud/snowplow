---
title: >-
    غزل شمارهٔ ۶۱۶
---
# غزل شمارهٔ ۶۱۶

<div class="b" id="bn1"><div class="m1"><p>چونی و چه باشد چون تا قدر تو را داند</p></div>
<div class="m2"><p>جز پادشه بی‌چون قدر تو کجا داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم ز تو پرنورست ای دلبر دور از تو</p></div>
<div class="m2"><p>حق تو زمین داند یا چرخ سما داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این پرده نیلی را بادیست که جنباند</p></div>
<div class="m2"><p>این باد هوایی نی بادی که خدا داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرقه غم و شادی را دانی که که می‌دوزد</p></div>
<div class="m2"><p>وین خرقه ز دوزنده خود را چه جدا داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر دل آیینه دانی که چه می‌تابد</p></div>
<div class="m2"><p>داند چه خیالست آن آن کس که صفا داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شقه علم عالم هر چند که می‌رقصد</p></div>
<div class="m2"><p>چشم تو علم بیند جان تو هوا داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وان کس که هوا را هم داند که چه بیچارست</p></div>
<div class="m2"><p>جز حضرت الاالله باقی همه لا داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس الحق تبریزی این مکر که حق دارد</p></div>
<div class="m2"><p>بی مهره تو جانم کی نرد دغا داند</p></div></div>