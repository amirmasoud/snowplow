---
title: >-
    غزل شمارهٔ ۱۲۹۶
---
# غزل شمارهٔ ۱۲۹۶

<div class="b" id="bn1"><div class="m1"><p>بیا بیا که تویی جان جان جان سماع</p></div>
<div class="m2"><p>هزار شمع منور به خاندان سماع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو صد هزار ستاره ز تست روشن دل</p></div>
<div class="m2"><p>بیا که ماه تمامی در آسمان سماع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا که جان و جهان در رخ تو حیرانست</p></div>
<div class="m2"><p>بیا که بوالعجبی نیک در جهان سماع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا که بی تو به بازار عشق نقدی نیست</p></div>
<div class="m2"><p>بیا که چون تو زری را ندید کان سماع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا که بر در تو شسته‌اند مشتاقان</p></div>
<div class="m2"><p>ز بام خویش فروکن تو نردبان سماع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا که رونق بازار عشق از لب تست</p></div>
<div class="m2"><p>که شاهدیست نهانی در این دکان سماع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیار قند معانی ز شمس تبریزی</p></div>
<div class="m2"><p>که باز ماند ز عشق لبش دهان سماع</p></div></div>