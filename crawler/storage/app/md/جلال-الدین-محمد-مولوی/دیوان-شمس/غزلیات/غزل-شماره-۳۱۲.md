---
title: >-
    غزل شمارهٔ ۳۱۲
---
# غزل شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>به جان تو که مرو از میان کار مخسب</p></div>
<div class="m2"><p>ز عمر یک شب کم گیر و زنده دار مخسب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار شب تو برای هوای خود خفتی</p></div>
<div class="m2"><p>یکی شبی چه شود از برای یار مخسب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای یار لطیفی که شب نمی‌خسبد</p></div>
<div class="m2"><p>موافقت کن و دل را بدو سپار مخسب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بترس از آن شب رنجوریی که تو تا روز</p></div>
<div class="m2"><p>فغان و یارب و یارب کنی به زار مخسب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی که مرگ بیاید قنق کرک گوید</p></div>
<div class="m2"><p>به حق تلخی آن شب که ره سپار مخسب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن زلازل هیبت که سنگ آب شود</p></div>
<div class="m2"><p>اگر تو سنگ نه‌ای آن به یاد آر مخسب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چه زنگی شب سخت ساقی چستست</p></div>
<div class="m2"><p>مگیر جام وی و ترس از آن خمار مخسب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدای گفت که شب دوستان نمی‌خسبند</p></div>
<div class="m2"><p>اگر خجل شده‌ای زین و شرمسار مخسب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بترس از آن شب سخت عظیم بی‌زنهار</p></div>
<div class="m2"><p>ذخیره ساز شبی را و زینهار مخسب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شنیده‌ای که مهان کام‌ها به شب یابند</p></div>
<div class="m2"><p>برای عشق شهنشاه کامیار مخسب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مغز خشک شود تازه مغزیت بخشد</p></div>
<div class="m2"><p>که جمله مغز شوی ای امیدوار مخسب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هزار بارت گفتم خموش و سودت نیست</p></div>
<div class="m2"><p>یکی بیار و عوض گیر صد هزار مخسب</p></div></div>