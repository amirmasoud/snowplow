---
title: >-
    غزل شمارهٔ ۱۴۹۶
---
# غزل شمارهٔ ۱۴۹۶

<div class="b" id="bn1"><div class="m1"><p>چه دیدم خواب شب کامروز مستم</p></div>
<div class="m2"><p>چو مجنونان ز بند عقل جستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بیداری مگر من خواب بینم</p></div>
<div class="m2"><p>که خوابم نیست تا این درد هستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر من صورت عشق حقیقی</p></div>
<div class="m2"><p>بدیدم خواب کو را می پرستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا ای عشق کاندر تن چو جانی</p></div>
<div class="m2"><p>به اقبالت ز حبس تن برستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گفتی بدر پرده دریدم</p></div>
<div class="m2"><p>مرا گفتی قدح بشکن شکستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا گفتی ببر از جمله یاران</p></div>
<div class="m2"><p>بکندم از همه دل در تو بستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا دل خسته کردی جرمم این بود</p></div>
<div class="m2"><p>که از مژگان خیالت را بجستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببر جان مرا تا در پناهت</p></div>
<div class="m2"><p>دو دستک می زنم کز جان بسستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه عالم‌هاست در هر تار مویت</p></div>
<div class="m2"><p>بیفشان زلف کز عالم گسستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که در هفتم زمین با تو بلندم</p></div>
<div class="m2"><p>که در هفتم فلک بی‌روت پستم</p></div></div>