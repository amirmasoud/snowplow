---
title: >-
    غزل شمارهٔ ۱۱۵۲
---
# غزل شمارهٔ ۱۱۵۲

<div class="b" id="bn1"><div class="m1"><p>ببین دلی که نگردد ز جان سپاری سیر</p></div>
<div class="m2"><p>اسیر عشق نگردد ز رنج و خواری سیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز زخم‌های نهانی که عاشقان دانند</p></div>
<div class="m2"><p>به خون درست و نگردد ز زخم کاری سیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقیم شد به خرابات و جمله رندان را</p></div>
<div class="m2"><p>خراب کرد و نشد از شراب باری سیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار جان مقدس سپرد هر نفسی</p></div>
<div class="m2"><p>در آن شکار و نشد جان از آن شکاری سیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثال نی ز لب یار کام پرشکرست</p></div>
<div class="m2"><p>ولیک نیست چو نی از فغان و زاری سیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفت تو ز چه سیری بگفتم از جز تو</p></div>
<div class="m2"><p>ولیک هیچ نگردم از آنچ داری سیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه شهر و یار شناسیم ای مسلمانان</p></div>
<div class="m2"><p>از آنک نیست دل از جام شهریاری سیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوای تو چو بهارست و دل ز توست چو باغ</p></div>
<div class="m2"><p>که باغ می‌نشود از دم بهاری سیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شرمسارم از احسان شمس تبریزی</p></div>
<div class="m2"><p>که جان مباد از این شرم و شرمساری سیر</p></div></div>