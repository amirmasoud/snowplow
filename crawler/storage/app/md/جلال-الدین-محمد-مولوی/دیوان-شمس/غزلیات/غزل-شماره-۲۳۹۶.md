---
title: >-
    غزل شمارهٔ ۲۳۹۶
---
# غزل شمارهٔ ۲۳۹۶

<div class="b" id="bn1"><div class="m1"><p>ای پاک از آب و از گل پایی در این گلم نه</p></div>
<div class="m2"><p>بی‌دست و دل شدستم دستی بر این دلم نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آب تیره گشته در راه خیره گشته</p></div>
<div class="m2"><p>از ره مرا برون بر در صدر منزلم نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارم ز پیچ زلفت شوریده گشت و مشکل</p></div>
<div class="m2"><p>شوریده زلف خود را بر کار مشکلم نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر حاصلی که دارم بی‌حاصلی است بی‌تو</p></div>
<div class="m2"><p>سیلاب عشق خود را بر کار و حاصلم نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که گرد شمعم پروانه روح باشد</p></div>
<div class="m2"><p>زان آتشی که داری بر شمع قابلم نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون رشته تبم من با صد گره ز زلفت</p></div>
<div class="m2"><p>همچون گره زمانی بر زلف سلسلم نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چشم توست جانا پرسحر چاه بابل</p></div>
<div class="m2"><p>سحری بکن حلالی در چاه بابلم نه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی الست زان دم حاصل شده‌ست جانم</p></div>
<div class="m2"><p>تعویذ کن بلی را بر جان حاملم نه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی باشد آن زمانی کان ابر را برانی</p></div>
<div class="m2"><p>گویی بیا و رخ را بر ماه کاملم نه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای شمس حق تبریز ار مقبل است جانم</p></div>
<div class="m2"><p>اقبال وصل خود را بر جان مقبلم نه</p></div></div>