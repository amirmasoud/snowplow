---
title: >-
    غزل شمارهٔ ۲۶۵۵
---
# غزل شمارهٔ ۲۶۵۵

<div class="b" id="bn1"><div class="m1"><p>تو نقشی نقش بندان را چه دانی</p></div>
<div class="m2"><p>تو شکلی پیکری جان را چه دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خود می‌نشنوی بانگ دهل را</p></div>
<div class="m2"><p>رموز سر پنهان را چه دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنوز از کات کفرت خود خبر نیست</p></div>
<div class="m2"><p>حقایق‌های ایمان را چه دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوزت خار در پای است بنشین</p></div>
<div class="m2"><p>تو سرسبزی بستان را چه دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو نامی کرده‌ای این را و آن را</p></div>
<div class="m2"><p>از این نگذشته‌ای آن را چه دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه صورت‌هاست مر بی‌صورتان را</p></div>
<div class="m2"><p>تو صورت‌های ایشان را چه دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنخ کم زن که اندر چاه نفسی</p></div>
<div class="m2"><p>تو آن چاه زنخدان را چه دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درخت سبز داند قدر باران</p></div>
<div class="m2"><p>تو خشکی قدر باران را چه دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیه کاری مکن با باز چون زاغ</p></div>
<div class="m2"><p>تو باز چتر سلطان را چه دانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلیمانی نکردی در ره عشق</p></div>
<div class="m2"><p>زبان جمله مرغان را چه دانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگهبانی است حاضر بر تو سبحان</p></div>
<div class="m2"><p>تو حیوانی نگهبان را چه دانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو را در چرخ آورده‌ست ماهی</p></div>
<div class="m2"><p>تو ماه چرخ گردان را چه دانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تجلی کرد این دم شمس تبریز</p></div>
<div class="m2"><p>تو دیوی نور رحمان را چه دانی</p></div></div>