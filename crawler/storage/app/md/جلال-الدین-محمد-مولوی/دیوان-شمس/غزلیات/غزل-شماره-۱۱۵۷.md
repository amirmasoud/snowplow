---
title: >-
    غزل شمارهٔ ۱۱۵۷
---
# غزل شمارهٔ ۱۱۵۷

<div class="b" id="bn1"><div class="m1"><p>گر تو خواهی وطن پر از دلدار</p></div>
<div class="m2"><p>خانه را رو تهی کن از اغیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور تو خواهی سماع را گیرا</p></div>
<div class="m2"><p>دور دارش ز دیده انکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او را سماع مست نکرد</p></div>
<div class="m2"><p>منکرش دان اگر چه کرد اقرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که اقرار کرد و باده شناخت</p></div>
<div class="m2"><p>عاقلش نام نه مگو خمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بهانه به ره کن آن‌ها را</p></div>
<div class="m2"><p>تا شوی از سماع برخوردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز میان خویش را برون کن تیز</p></div>
<div class="m2"><p>تا بگیری تو خویش را به کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایه یار به که ذکر خدای</p></div>
<div class="m2"><p>این چنین گفتست صدر کبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نگویی که گل هم از خارست</p></div>
<div class="m2"><p>زانک هر خار گل نیارد بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار بیگانه را ز دل برکن</p></div>
<div class="m2"><p>خار گل را به جان و دل می‌دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موسی اندر درخت آتش دید</p></div>
<div class="m2"><p>سبزتر می‌شد آن درخت از نار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهوت و حرص مرد صاحب دل</p></div>
<div class="m2"><p>همچنین دان و همچنین پندار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صورت شهوتست لیکن هست</p></div>
<div class="m2"><p>همچو نار خلیل پرانوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس تبریز را بشر بینند</p></div>
<div class="m2"><p>چون گشایند دیده‌ها کفار</p></div></div>