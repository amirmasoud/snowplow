---
title: >-
    غزل شمارهٔ ۱۴۵
---
# غزل شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>ای وصالت یک زمان بوده فراقت سال‌ها</p></div>
<div class="m2"><p>ای به زودی بار کرده بر شتر احمال‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب شد و درچین ز هجران رخ چون آفتاب</p></div>
<div class="m2"><p>درفتاده در شب تاریک بس زلزال‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون همی‌رفتی به سکته حیرتی حیران بدم</p></div>
<div class="m2"><p>چشم باز و من خموش و می‌شد آن اقبال‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور نه سکته بخت بودی مر مرا خود آن زمان</p></div>
<div class="m2"><p>چهره خون آلود کردی بردریدی شال‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر ره جان و صد جان در شفاعت پیش تو</p></div>
<div class="m2"><p>در زمان قربان بکردی خود چه باشد مال‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بگشتی در شب تاریک ز آتش نال‌ها</p></div>
<div class="m2"><p>تا چو احوال قیامت دیده شد اهوال‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بدیدی دل عذابی گونه گونه در فراق</p></div>
<div class="m2"><p>سنگ خون گرید اگر زان بشنود احوال‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدها چون تیر بوده گشته در هجران کمان</p></div>
<div class="m2"><p>اشک خون آلود گشت و جمله دل‌ها دال‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون درستی و تمامی شاه تبریزی بدید</p></div>
<div class="m2"><p>در صف نقصان نشست است از حیا مثقال‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از برای جان پاک نورپاش مه وشت</p></div>
<div class="m2"><p>ای خداوند شمس دین تا نشکنی آمال‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از مقال گوهرین بحر بی‌پایان تو</p></div>
<div class="m2"><p>لعل گشته سنگ‌ها و ملک گشته حال‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حال‌های کاملانی کان ورای قال‌هاست</p></div>
<div class="m2"><p>شرمسار از فر و تاب آن نوادر قال‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ذره‌های خاک‌هامون گر بیابد بوی او</p></div>
<div class="m2"><p>هر یکی عنقا شود تا برگشاید بال‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بال‌ها چون برگشاید در دو عالم ننگرد</p></div>
<div class="m2"><p>گرد خرگاه تو گردد واله اجمال‌ها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیده نقصان ما را خاک تبریز صفا</p></div>
<div class="m2"><p>کحل بادا تا بیابد زان بسی اکمال‌ها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چونک نورافشان کنی درگاه بخشش روح را</p></div>
<div class="m2"><p>خود چه پا دارد در آن دم رونق اعمال‌ها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خود همان بخشش که کردی بی‌خبر اندر نهان</p></div>
<div class="m2"><p>می‌کند پنهان پنهان جمله افعال‌ها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ناگهان بیضه شکافد مرغ معنی برپرد</p></div>
<div class="m2"><p>تا هما از سایه آن مرغ گیرد فال‌ها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم تو بنویس ای حسام الدین و می‌خوان مدح او</p></div>
<div class="m2"><p>تا به رغم غم ببینی بر سعادت خال‌ها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر چه دست افزار کارت شد ز دستت باک نیست</p></div>
<div class="m2"><p>دست شمس الدین دهد مر پات را خلخال‌ها</p></div></div>