---
title: >-
    غزل شمارهٔ ۲۱۶۱
---
# غزل شمارهٔ ۲۱۶۱

<div class="b" id="bn1"><div class="m1"><p>اگر نه عاشق اویم چه می‌پویم به کوی او</p></div>
<div class="m2"><p>وگر نه تشنه اویم چه می‌جویم به جوی او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر این مجنون چه می‌بندم مگر بر خویش می‌خندم</p></div>
<div class="m2"><p>که او زنجیر نپذیرد مگر زنجیر موی او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببر عقلم ببر هوشم که چون پنبه‌ست در گوشم</p></div>
<div class="m2"><p>چو گوشم رست از این پنبه درآید های هوی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی‌گوید دل زارم که با خود عهدها دارم</p></div>
<div class="m2"><p>نیاشامم شراب خوش مگر خون عدوی او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم را می‌کند پرخون سرم را پرمی و افیون</p></div>
<div class="m2"><p>دل من شد تغار او سر من شد کدوی او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه باشد ماه یا زهره چو او بگشود آن چهره</p></div>
<div class="m2"><p>چه دارد قند یا حلوا ز شیرینی خوی او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا گوید چرا زاری ز ذوق آن شکرباری</p></div>
<div class="m2"><p>مرا گوید چرا زردی ز لاله ستان روی او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا هر دم برانگیزی به سوی شمس تبریزی</p></div>
<div class="m2"><p>بگو در گوش من ای دل چه می‌تازی به سوی او</p></div></div>