---
title: >-
    غزل شمارهٔ ۲۰۱۸
---
# غزل شمارهٔ ۲۰۱۸

<div class="b" id="bn1"><div class="m1"><p>مرغ خانه با هما پر وا مکن</p></div>
<div class="m2"><p>پر نداری نیت صحرا مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سمندر در دل آتش مرو</p></div>
<div class="m2"><p>وز مری تو خویش را رسوا مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درزیا آهنگری کار تو نیست</p></div>
<div class="m2"><p>تو ندانی فعل آتش‌ها مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول از آهنگران تعلیم گیر</p></div>
<div class="m2"><p>ور نه بی‌تعلیم تو آن را مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نه‌ای بحری تو بحر اندرمشو</p></div>
<div class="m2"><p>قصد موج و غره دریا مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور کنی پس گوشه کشتی بگیر</p></div>
<div class="m2"><p>دست خود را تو ز کشتی وا مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بیفتی هم در آتش کشتی بیفت</p></div>
<div class="m2"><p>تکیه تو بر پنجه و بر پا مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ خواهی صحبت عیسی گزین</p></div>
<div class="m2"><p>ور نه قصد گنبد خضرا مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میوه خامی مقیم شاخ باش</p></div>
<div class="m2"><p>بی‌معانی ترک این اسما مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمس تبریزی مقیم حضرت است</p></div>
<div class="m2"><p>تو مقام خویش جز آن جا مکن</p></div></div>