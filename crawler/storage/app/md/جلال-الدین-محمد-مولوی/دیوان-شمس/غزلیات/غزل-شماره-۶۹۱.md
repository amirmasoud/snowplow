---
title: >-
    غزل شمارهٔ ۶۹۱
---
# غزل شمارهٔ ۶۹۱

<div class="b" id="bn1"><div class="m1"><p>هر چند که بلبلان گزینند</p></div>
<div class="m2"><p>مرغان دگر خمش نشینند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود گیر که خرمنی ندارند</p></div>
<div class="m2"><p>نه از خرمن فقر دانه چینند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حلقه برون نه‌ایم ما نیز</p></div>
<div class="m2"><p>هر چند که آن شهان نگینند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ولوله مرا نخواهند</p></div>
<div class="m2"><p>از بهر چه کارم آفرینند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیرین و ترش مراد شاهست</p></div>
<div class="m2"><p>دو دیگ نهاده بهر اینند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بایست بود ترش به مطبخ</p></div>
<div class="m2"><p>چون مخموران بدان رهینند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر حالت ما غذای قومیست</p></div>
<div class="m2"><p>زین اغذیه غیبیان سمینند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغان ضمیر از آسمانند</p></div>
<div class="m2"><p>روزی دو سه بسته زمینند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانشان ز فلک گسیل کردند</p></div>
<div class="m2"><p>هر چند ستارگان دینند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا قدر وصال حق بدانند</p></div>
<div class="m2"><p>تا درد فراق حق بینند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر خاک قراضه گر بریزند</p></div>
<div class="m2"><p>آن را نهلند و برگزینند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شمس تبریز کم سخن بود</p></div>
<div class="m2"><p>شاهان همه صابر و امینند</p></div></div>