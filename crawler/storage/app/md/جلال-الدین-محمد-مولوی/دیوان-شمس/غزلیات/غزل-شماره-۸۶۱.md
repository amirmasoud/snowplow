---
title: >-
    غزل شمارهٔ ۸۶۱
---
# غزل شمارهٔ ۸۶۱

<div class="b" id="bn1"><div class="m1"><p>لطفی نماند کان صنم خوش لقا نکرد</p></div>
<div class="m2"><p>ما را چه جرم اگر کرمش با شما نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنیع می‌زنی که جفا کرد آن نگار</p></div>
<div class="m2"><p>خوبی که دید در دو جهان کو جفا نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقش شکر بس است اگر او شکر نداد</p></div>
<div class="m2"><p>حسنش همه وفاست اگر او وفا نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنمای خانه‌ای که از او نیست پرچراغ</p></div>
<div class="m2"><p>بنمای صفه‌ای که رخش پرصفا نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چشم و آن چراغ دو نورند هر یکی</p></div>
<div class="m2"><p>چون آن به هم رسید کسیشان جدا نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون روح در نظاره فنا گشت این بگفت</p></div>
<div class="m2"><p>نظاره جمال خدا جز خدا نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر یک از این مثال بیانست و مغلطه است</p></div>
<div class="m2"><p>حق جز ز رشک نام رخش والضحی نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشیدروی مفخر تبریز شمس دین</p></div>
<div class="m2"><p>بر فانیی نتافت که آن را بقا نکرد</p></div></div>