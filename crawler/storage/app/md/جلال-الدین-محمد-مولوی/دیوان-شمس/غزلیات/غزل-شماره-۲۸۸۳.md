---
title: >-
    غزل شمارهٔ ۲۸۸۳
---
# غزل شمارهٔ ۲۸۸۳

<div class="b" id="bn1"><div class="m1"><p>چه حریصی که مرا بی‌خور و بی‌خواب کنی</p></div>
<div class="m2"><p>درکشی روی و مرا روی به محراب کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب را در دهنم تلختر از زهر کنی</p></div>
<div class="m2"><p>زهره‌ام را ببری در غم خود آب کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی حج رانی و در بادیه‌ام قطع کنی</p></div>
<div class="m2"><p>اشتر و رخت مرا قسمت اعراب کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه ببخشی ثمر و زرع مرا خشک کنی</p></div>
<div class="m2"><p>گه به بارانش همی سخره سیلاب کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ز دام تو گریزم تو به تیرم دوزی</p></div>
<div class="m2"><p>چون سوی دام روم دست به مضراب کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باادب باشم گویی که برو مست نه‌ای</p></div>
<div class="m2"><p>بی ادب گردم تو قصه آداب کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بباری تو چو باران کرم بر بامم</p></div>
<div class="m2"><p>هر دو چشمم ز نم و قطره چو میزاب کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه عزلت تو بگویی که چو رهبان گشتی</p></div>
<div class="m2"><p>گه صحبت تو مرا دشمن اصحاب کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر قصب وار نپیچم دل خود در غم تو</p></div>
<div class="m2"><p>چون قصب پیچ مرا هالک مهتاب کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در توکل تو بگویی که سبب سنت ماست</p></div>
<div class="m2"><p>در تسبب تو نکوهیدن اسباب کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز جان صید کنی چنگل او درشکنی</p></div>
<div class="m2"><p>تن شود کلب معلم تش بی‌ناب کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زرگر رنگ رخ ما چو دکانی گیرد</p></div>
<div class="m2"><p>لقب زرگر ما را همه قلاب کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من که باشم که به درگاه تو صبح صادق</p></div>
<div class="m2"><p>هست لرزان که مباداش که کذاب کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه را نفی کنی بازدهی صد چندان</p></div>
<div class="m2"><p>دی دهی و به بهارش همه ایجاب کنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزنی گردن انجم تو به تیغ خورشید</p></div>
<div class="m2"><p>بازشان هم تو فروز رخ عناب کنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو خمش کرد بگویی که بگو و چو بگفت</p></div>
<div class="m2"><p>گوییش پس تو چرا فتح چنین باب کنی</p></div></div>