---
title: >-
    غزل شمارهٔ ۳۱۲۳
---
# غزل شمارهٔ ۳۱۲۳

<div class="b" id="bn1"><div class="m1"><p>به حیلت تو خواهی که در را ببندی</p></div>
<div class="m2"><p>بنالی چو رنجور و سر را ببندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو رنجور والله که آن زور داری</p></div>
<div class="m2"><p>که بر چرخ آیی قمر را ببندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آن روی چون مه به گردون نمایی</p></div>
<div class="m2"><p>به صبح جمالت سحر را ببندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلام صبوحم ولی خصم صبحم</p></div>
<div class="m2"><p>که از بهر رفتن کمر را ببندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر گاو آرند پیشت سفیهان</p></div>
<div class="m2"><p>به یک نکته صد گاو و خر را ببندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک غمزه آهوان دو چشمت</p></div>
<div class="m2"><p>چو روبه کنی شیر نر را ببندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمستان هجر آمد و ترسم آنست</p></div>
<div class="m2"><p>که سیلاب این چشم تر را ببندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر همچو خورشید ناگه بتابی</p></div>
<div class="m2"><p>بدین آب هر رهگذر را ببندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خموشم ولیکن روا نیست جانا</p></div>
<div class="m2"><p>که از حال زارم نظر را ببندی</p></div></div>