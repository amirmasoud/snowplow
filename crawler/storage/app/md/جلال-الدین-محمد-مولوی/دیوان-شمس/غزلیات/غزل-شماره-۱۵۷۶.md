---
title: >-
    غزل شمارهٔ ۱۵۷۶
---
# غزل شمارهٔ ۱۵۷۶

<div class="b" id="bn1"><div class="m1"><p>ما زنده به نور کبریاییم</p></div>
<div class="m2"><p>بیگانه و سخت آشناییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس است چو گرگ لیک در سر</p></div>
<div class="m2"><p>بر یوسف مصر برفزاییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه توبه کند ز خویش بینی</p></div>
<div class="m2"><p>گر ما رخ خود به مه نماییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درسوزد پر و بال خورشید</p></div>
<div class="m2"><p>چون ما پر و بال برگشاییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این هیکل آدم است روپوش</p></div>
<div class="m2"><p>ما قبله جمله سجده‌هاییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دم بنگر مبین تو آدم</p></div>
<div class="m2"><p>تا جانت به لطف دررباییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابلیس نظر جدا جدا داشت</p></div>
<div class="m2"><p>پنداشت که ما ز حق جداییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس تبریز خود بهانه‌ست</p></div>
<div class="m2"><p>ماییم به حسن لطف ماییم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با خلق بگو برای روپوش</p></div>
<div class="m2"><p>کو شاه کریم و ما گداییم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را چه ز شاهی و گدایی</p></div>
<div class="m2"><p>شادیم که شاه را سزاییم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محویم به حسن شمس تبریز</p></div>
<div class="m2"><p>در محو نه او بود نه ماییم</p></div></div>