---
title: >-
    غزل شمارهٔ ۱۳۹
---
# غزل شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>رنج تن دور از تو ای تو راحت جان‌های ما</p></div>
<div class="m2"><p>چشم بد دور از تو ای تو دیده بینای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحت تو صحت جان و جهانست ای قمر</p></div>
<div class="m2"><p>صحت جسم تو بادا ای قمرسیمای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عافیت بادا تنت را ای تن تو جان صفت</p></div>
<div class="m2"><p>کم مبادا سایه لطف تو از بالای ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلشن رخسار تو سرسبز بادا تا ابد</p></div>
<div class="m2"><p>کان چراگاه دلست و سبزه و صحرای ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنج تو بر جان ما بادا مبادا بر تنت</p></div>
<div class="m2"><p>تا بود آن رنج همچون عقل جان آرای ما</p></div></div>