---
title: >-
    غزل شمارهٔ ۱۵۴۶
---
# غزل شمارهٔ ۱۵۴۶

<div class="b" id="bn1"><div class="m1"><p>رفتم تصدیع از جهان بردم</p></div>
<div class="m2"><p>بیرون شدم از زحیر و جان بردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردم بدرود همنشینان را</p></div>
<div class="m2"><p>جان را به جهان بی‌نشان بردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین خانه شش دری برون رفتم</p></div>
<div class="m2"><p>خوش رخت به سوی لامکان بردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون میر شکار غیب را دیدم</p></div>
<div class="m2"><p>چون تیر پریدم و کمان بردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چوگان اجل چو سوی من آمد</p></div>
<div class="m2"><p>من گوی سعادت از میان بردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از روزن من مهی عجب درتافت</p></div>
<div class="m2"><p>رفتم سوی بام و نردبان بردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بام فلک که مجمع جان‌هاست</p></div>
<div class="m2"><p>ز آن خوشتر بد که من گمان بردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاخ گل من چو گشت پژمرده</p></div>
<div class="m2"><p>بازش سوی باغ و گلستان بردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مشتریی نبود نقدم را</p></div>
<div class="m2"><p>زودش سوی اصل اصل کان بردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین قلب زنان قراضه جان را</p></div>
<div class="m2"><p>هم جانب زرگر ارمغان بردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در غیب جهان بی‌کران دیدم</p></div>
<div class="m2"><p>آلاجق خود بدان کران بردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر من مگری که زین سفر شادم</p></div>
<div class="m2"><p>چون راه به خطه جنان بردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این نکته نویس بر سر گورم</p></div>
<div class="m2"><p>که سر ز بلا و امتحان بردم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوش خسپ تنا در این زمین که من</p></div>
<div class="m2"><p>پیغام تو سوی آسمان بردم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بربند زنخ که من فغان‌ها را</p></div>
<div class="m2"><p>سرجمله به خالق فغان بردم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین بیش مگو غم دل ایرا من</p></div>
<div class="m2"><p>دل را به جناب غیب دان بردم</p></div></div>