---
title: >-
    غزل شمارهٔ ۲۸۱۱
---
# غزل شمارهٔ ۲۸۱۱

<div class="b" id="bn1"><div class="m1"><p>از هوای شمس دین بنگر تو این دیوانگی</p></div>
<div class="m2"><p>با همه خویشان گرفته شیوه بیگانگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وحش صحرا گشته و رسوای بازاری شده</p></div>
<div class="m2"><p>از هوای خانه او صد هزاران خانگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاعقه هجرش زده برسوخته یک بارگی</p></div>
<div class="m2"><p>عقل و شرم و فهم و تقوا دانش و فرزانگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ز شمع عشق او نان پاره‌ای می‌خواستم</p></div>
<div class="m2"><p>گفت بنویسید توقیعش پی پروانگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای گشاده قلعه‌های جان به چشم آتشین</p></div>
<div class="m2"><p>ای هزاران صف دریده عشقت از مردانگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خداوند شمس دین صد گنج خاک است پیش تو</p></div>
<div class="m2"><p>تا چه باشد عاشق بیچاره‌ای یک دانگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد غریو و بانگ اندر سقف گردون افکنیم</p></div>
<div class="m2"><p>من نیم در عشق پابرجای تو یک بانگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل را گفتم میان جان و جانان فرق کن</p></div>
<div class="m2"><p>شانه عقلم ز فرقش یاوه کرده شانگی</p></div></div>