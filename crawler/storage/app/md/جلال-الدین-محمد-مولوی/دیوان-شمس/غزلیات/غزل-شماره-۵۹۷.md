---
title: >-
    غزل شمارهٔ ۵۹۷
---
# غزل شمارهٔ ۵۹۷

<div class="b" id="bn1"><div class="m1"><p>امروز جمال تو بر دیده مبارک باد</p></div>
<div class="m2"><p>بر ما هوس تازه پیچیده مبارک باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل‌ها چون میان بندد بر جمله جهان خندد</p></div>
<div class="m2"><p>ای پرگل و صد چون گل خندیده مبارک باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوبان چو رخت دیده افتاده و لغزیده</p></div>
<div class="m2"><p>دل بر در این خانه لغزیده مبارک باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوروز رخت دیدم خوش اشک بباریدم</p></div>
<div class="m2"><p>نوروز و چنین باران باریده مبارک باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی گفت زبان تو بی‌حرف و بیان تو</p></div>
<div class="m2"><p>از باطن تو گوشت بشنیده مبارک باد</p></div></div>