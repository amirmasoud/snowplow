---
title: >-
    غزل شمارهٔ ۹۰۵
---
# غزل شمارهٔ ۹۰۵

<div class="b" id="bn1"><div class="m1"><p>شدم ز عشق به جایی که عشق نیز نداند</p></div>
<div class="m2"><p>رسید کار به جایی که عقل خیره بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار ظلم رسیده ز عقل گشت رهیده</p></div>
<div class="m2"><p>چو عقل بسته شد این جا بگو کیش برهاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا مگر که تو مستی که دل به عقل ببستی</p></div>
<div class="m2"><p>که او نشست نیابد تو را کجا بنشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>متاع عقل نشانست و عشق روح فشانست</p></div>
<div class="m2"><p>که عشق وقت نظاره نثار جان بفشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار جان و دل و عقل گر به هم تو ببندی</p></div>
<div class="m2"><p>چو عشق با تو نباشد به روزنش نرساند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روی بت نرسی تو مگر به دام دو زلفش</p></div>
<div class="m2"><p>ولیک کوشش می‌کن که کوششت بپزاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو باز چشم تو را بست دست اوست گشایش</p></div>
<div class="m2"><p>ولی به هر سر کویی تو را چو کبک دواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آنک بالش دارد ز آستان عنایت</p></div>
<div class="m2"><p>غلام خفتن اویم که هیچ خفته نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میانه گیرد آهو میانه دل شیری</p></div>
<div class="m2"><p>هزار آهوی دیگر ز شیر او برهاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو در درونه صیاد مرغ یافت قبولی</p></div>
<div class="m2"><p>هزار مرغ گرفته ز دام او بپراند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آن دلی که به تبریز و شمس دین شده باشد</p></div>
<div class="m2"><p>چو شاه ماه به میدان چرخ اسب دواند</p></div></div>