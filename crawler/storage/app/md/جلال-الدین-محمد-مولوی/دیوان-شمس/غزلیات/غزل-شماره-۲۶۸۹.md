---
title: >-
    غزل شمارهٔ ۲۶۸۹
---
# غزل شمارهٔ ۲۶۸۹

<div class="b" id="bn1"><div class="m1"><p>منم غرقه درون جوی باری</p></div>
<div class="m2"><p>نهانم می‌خلد در آب خاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه خار را من می‌نبینم</p></div>
<div class="m2"><p>نیم خالی ز زخم خار باری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم تا چه خار است اندر این جوی</p></div>
<div class="m2"><p>که خالی نیست جان از خارخاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنم را بین که صورتگر ز سوزن</p></div>
<div class="m2"><p>بر او بنگاشت هر سویی نگاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو پیراهن برون افکندم از سر</p></div>
<div class="m2"><p>به دریا درشدم مرغاب واری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که غسل آرم برون آیم به پاکی</p></div>
<div class="m2"><p>به خنده گفت موج بحر کاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مثال کاسه چوبین بگشتم</p></div>
<div class="m2"><p>بر آن آبی که دارد سهم ناری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی‌دانم که آن ساحل کجا شد</p></div>
<div class="m2"><p>که پیدا نیست دریا را کناری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو شمس الدین تبریز ار ملولی</p></div>
<div class="m2"><p>به هر لحظه چه افروزی شراری</p></div></div>