---
title: >-
    غزل شمارهٔ ۱۹۵۶
---
# غزل شمارهٔ ۱۹۵۶

<div class="b" id="bn1"><div class="m1"><p>سر فروکرد از فلک آن ماه روی سیمتن</p></div>
<div class="m2"><p>آستین را می فشاند در اشارت سوی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو چشم کشتگان چشمان من حیران او</p></div>
<div class="m2"><p>وز شراب عشق او این جان من بی‌خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر جعد زلف مشکش صد قیامت را مقام</p></div>
<div class="m2"><p>در صفای صحن رویش آفت هر مرد و زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ جان اندر قفس می کند پر و بال خویش</p></div>
<div class="m2"><p>تا قفس را بشکند اندر هوای آن شکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فلک آمد همایی بر سر من سایه کرد</p></div>
<div class="m2"><p>من فغان کردم که دور از پیش آن خوب ختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سخن آمد همای و گفت بی‌روزی کسی</p></div>
<div class="m2"><p>کز سعادت می گریزی ای شقی ممتحن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش آخر حجابی در میان ما و دوست</p></div>
<div class="m2"><p>من جمال دوست خواهم کو است مر جان را سکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن همای از بس تعجب سوی آن مه بنگرید</p></div>
<div class="m2"><p>از من او دیوانه تر شد در جمالش مفتتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر مست و خواجه مست و روح مست و جسم مست</p></div>
<div class="m2"><p>از خداوند شمس دین آن شاه تبریز و زمن</p></div></div>