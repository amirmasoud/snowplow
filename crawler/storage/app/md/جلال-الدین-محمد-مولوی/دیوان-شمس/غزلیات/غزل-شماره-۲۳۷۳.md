---
title: >-
    غزل شمارهٔ ۲۳۷۳
---
# غزل شمارهٔ ۲۳۷۳

<div class="b" id="bn1"><div class="m1"><p>مشنو حیلت خواجه هله ای دزد شبانه</p></div>
<div class="m2"><p>بشلولم بشلولم مجه از روزن خانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمشو غره پرستش بمده ریش به دستش</p></div>
<div class="m2"><p>وگرت شاه کند او که تویی یار یگانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی صحرای عدم رو به سوی باغ ارم رو</p></div>
<div class="m2"><p>می بی‌درد نیابی تو در این دور زمانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شه بنده نوازی تو بپر باز چو بازی</p></div>
<div class="m2"><p>به خدا لقمه بازان نخورد هیچ سمانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخورم گر نخورم من بنهد در دهن من</p></div>
<div class="m2"><p>بروم گر نروم من کندم گوش کشانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه میرند ولیکن همه میرند به پیشت</p></div>
<div class="m2"><p>همه تیر ای مه مه رو نپرد سوی نشانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چه افروخت خیالش رخ خورشیدصفت را</p></div>
<div class="m2"><p>ز کی آموخت خدایا عجب این فعل و بهانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو تو را حسن فزون شد خردم صید جنون شد</p></div>
<div class="m2"><p>چو مرا درد فزون شد بده آن درد مغانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو تو جمعیت جمعی تو در این جمع چو شمعی</p></div>
<div class="m2"><p>چو در این حلقه نگینی مجه ای جان زمانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو اگر نوش حدیثی ز حدیثان خوش او</p></div>
<div class="m2"><p>تو مگو تا که بگوید لب آن قندفسانه</p></div></div>