---
title: >-
    غزل شمارهٔ ۴۵۴
---
# غزل شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>جان سوی جسم آمد و تن سوی جان نرفت</p></div>
<div class="m2"><p>وان سو که تیر رفت حقیقت کمان نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان چست شد که تا بپرد وین تن گران</p></div>
<div class="m2"><p>هم در زمین فروشد و بر آسمان نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان میزبان تن شد در خانه گلین</p></div>
<div class="m2"><p>تن خانه دوست بود که با میزبان نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وحشتی بماند که تن را گمان نبود</p></div>
<div class="m2"><p>جان رفت جانبی که بدان جا گمان نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پایان فراق بین که جهان آمد این جهان</p></div>
<div class="m2"><p>اندر جهان کی دید کسی کز جهان نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرگت گلو بگیرد تو خیره سر شوی</p></div>
<div class="m2"><p>گویی رسول نامد وین را بیان نرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هر دهان که آب از آزادیم گشاد</p></div>
<div class="m2"><p>در گور هیچ مور ورا در دهان نرفت</p></div></div>