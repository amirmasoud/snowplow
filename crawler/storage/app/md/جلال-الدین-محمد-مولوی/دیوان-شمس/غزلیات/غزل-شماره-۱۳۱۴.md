---
title: >-
    غزل شمارهٔ ۱۳۱۴
---
# غزل شمارهٔ ۱۳۱۴

<div class="b" id="bn1"><div class="m1"><p>به دلجویی و دلداری درآمد یار پنهانک</p></div>
<div class="m2"><p>شب آمد چون مه تابان شه خون خوار پنهانک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهان بر می‌نهاد او دست یعنی دم مزن خامش</p></div>
<div class="m2"><p>و می‌فرمود چشم او درآ در کار پنهانک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کرد آن لطف او مستم در گلزار بشکستم</p></div>
<div class="m2"><p>همی‌دزدیدم آن گل‌ها از آن گلزار پنهانک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفتم که ای دلبر چه مکرانگیز و عیاری</p></div>
<div class="m2"><p>برانگیزان یکی مکری خوش ای عیار پنهانک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنه بر گوش من آن لب اگر چه خلوتست و شب</p></div>
<div class="m2"><p>مهل تا برزند بادی بر آن اسرار پنهانک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن اسرار عاشق کش مشو امشب مها خامش</p></div>
<div class="m2"><p>نوای چنگ عشرت را بجنبان تار پنهانک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بده ای دلبر خندان به رسم صدقه پنهان</p></div>
<div class="m2"><p>از آن دو لعل جان افزای شکربار پنهانک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که غمازان همه مستند اندر خواب گفت آری</p></div>
<div class="m2"><p>ولیکن هست از این مستان یکی هشیار پنهانک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن ای شمس تبریزی چنین تندی چنین تیزی</p></div>
<div class="m2"><p>کجا یابم تو را ای شاه دیگربار پنهانک</p></div></div>