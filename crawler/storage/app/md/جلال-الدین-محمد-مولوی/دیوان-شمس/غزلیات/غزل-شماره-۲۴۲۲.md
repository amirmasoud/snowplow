---
title: >-
    غزل شمارهٔ ۲۴۲۲
---
# غزل شمارهٔ ۲۴۲۲

<div class="b" id="bn1"><div class="m1"><p>رخ نفسی بر رخ این مست نه</p></div>
<div class="m2"><p>جنگ و جفا را نفسی پست نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیم اگر نیست به دست آورم</p></div>
<div class="m2"><p>باده چون زر تو بر این دست نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای تو گشاده در هفت آسمان</p></div>
<div class="m2"><p>دست کرم بر دل پابست نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیشکشم نیست به جز نیستی</p></div>
<div class="m2"><p>نیستیم را تو لقب هست نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم شکننده تو هم اشکسته بند</p></div>
<div class="m2"><p>مرهم جان بر سر اشکست نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر بر آن شکر و پسته منه</p></div>
<div class="m2"><p>مهر بر این چاکر پیوست نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته امت ای دل پنجاه بار</p></div>
<div class="m2"><p>صید مکن پای در این شست نه</p></div></div>