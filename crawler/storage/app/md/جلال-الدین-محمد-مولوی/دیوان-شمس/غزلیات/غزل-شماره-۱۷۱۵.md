---
title: >-
    غزل شمارهٔ ۱۷۱۵
---
# غزل شمارهٔ ۱۷۱۵

<div class="b" id="bn1"><div class="m1"><p>هر کی بمیرد شود دشمن او دوستکام</p></div>
<div class="m2"><p>دشمنم از مرگ من کور شود والسلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شکرستان مرا می کشد اندر شکر</p></div>
<div class="m2"><p>ای که چنین مرگ را جان و دل من غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غلط افکنده‌ست نام و نشان خلق را</p></div>
<div class="m2"><p>عمر شکربسته را مرگ نهادند نام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جهت این رسول گفت که الفقر کنز</p></div>
<div class="m2"><p>فقر کند نام گنج تا غلط افتند عام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحی در ایشان بود گنج به ویران بود</p></div>
<div class="m2"><p>تا که زر پخته را ره نبرد هیچ خام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم ای جان ببین زین دلم سست تنگ</p></div>
<div class="m2"><p>گفت که زین پس ز جهل وامکش از پس لگام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که سرانجام تو گردد بر کام تو</p></div>
<div class="m2"><p>توسن خنگ فلک باشد زیر تو رام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو بدانی که مرگ دارد صد باغ و برگ</p></div>
<div class="m2"><p>هست حیات ابد جوییش از جان مدام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خامش کن لب ببند بی‌دهنی خای قند</p></div>
<div class="m2"><p>نیست شو از خود که تا هست شوی زو تمام</p></div></div>