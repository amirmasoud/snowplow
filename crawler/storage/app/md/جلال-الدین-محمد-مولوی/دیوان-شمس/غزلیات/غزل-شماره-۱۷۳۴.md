---
title: >-
    غزل شمارهٔ ۱۷۳۴
---
# غزل شمارهٔ ۱۷۳۴

<div class="b" id="bn1"><div class="m1"><p>سماع چیست ز پنهانیان دل پیغام</p></div>
<div class="m2"><p>دل غریب بیابد ز نامه شان آرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکفته گردد از این باد شاخه‌های خرد</p></div>
<div class="m2"><p>گشاده گردد از این زخمه در وجود مسام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحر رسد ز ندای خروس روحانی</p></div>
<div class="m2"><p>ظفر رسد ز صدای نقاره بهرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عصیر جان به خم جسم تیر می انداخت</p></div>
<div class="m2"><p>چو دف شنید برآرد کفی نشان قوام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلاوتی عجبی در بدن پدید آید</p></div>
<div class="m2"><p>که از نی و لب مطرب شکر رسید به کام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار کزدم غم را کنون ببین کشته</p></div>
<div class="m2"><p>هزار دور فرح بین میان ما بی‌جام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فسون رقیه کزدم نویس عید رسید</p></div>
<div class="m2"><p>که هست رقیه کزدم به کوی عشق مدام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هر طرف بجهد بی‌قرار یعقوبی</p></div>
<div class="m2"><p>که بوی پیرهن یوسفی بیافت مشام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو جان ما ز نفخت است فیه من روحی</p></div>
<div class="m2"><p>روا بود که نفختش بود شراب و طعام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو حشر جمله خلایق به نفخ خواهد بود</p></div>
<div class="m2"><p>ز ذوق زمزمه بجهند مردگان ز منام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که خاک بر سر جان کسی که افسرده‌ست</p></div>
<div class="m2"><p>اثر نگیرد از آن نفخ و کم بود ز اعدام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن و دلی که بنوشید از این رحیق حلال</p></div>
<div class="m2"><p>بر آتش غم هجران حرام گشت حرام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جمال صورت غیبی ز وصف بیرون است</p></div>
<div class="m2"><p>هزار دیده روشن به وام خواه به وام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درون توست یکی مه کز آسمان خورشید</p></div>
<div class="m2"><p>ندا همی‌کندش کای منت غلام غلام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز جیب خویش بجو مه چو موسی عمران</p></div>
<div class="m2"><p>نگر به روزن خویش و بگو سلام سلام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سماع گرم کن و خاطر خران کم جو</p></div>
<div class="m2"><p>که جان جان سماعی و رونق ایام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبان خود بفروشم هزار گوش خرم</p></div>
<div class="m2"><p>که رفت بر سر منبر خطیب شهدکلام</p></div></div>