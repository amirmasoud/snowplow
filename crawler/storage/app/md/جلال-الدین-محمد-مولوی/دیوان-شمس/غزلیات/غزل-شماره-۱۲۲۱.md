---
title: >-
    غزل شمارهٔ ۱۲۲۱
---
# غزل شمارهٔ ۱۲۲۱

<div class="b" id="bn1"><div class="m1"><p>اگر گم گردد این بی‌دل از آن دلدار جوییدش</p></div>
<div class="m2"><p>وگر اندررمد عاشق به کوی یار جوییدش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر این بلبل جانم بپرد ناگهان از تن</p></div>
<div class="m2"><p>زهر خاری مپرسیدش در آن گلزار جوییدش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بیمار عشق او شود یاوه از این مجلس</p></div>
<div class="m2"><p>به پیش نرگس بیمار آن عیار جوییدش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر سرمست دل روزی زند بر سنگ آن شیشه</p></div>
<div class="m2"><p>به میخانه روید آن دم از آن خمار جوییدش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن عاشق که گم گردد هلا زنهار می‌گویم</p></div>
<div class="m2"><p>بر خورشید برق انداز بی‌زنهار جوییدش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر دزدی زند نقبی بدزدد رخت عاشق را</p></div>
<div class="m2"><p>میان طره مشکین آن طرار جوییدش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بت بیدار پرفن را که بیداری ز بخت اوست</p></div>
<div class="m2"><p>چنین خفته نیابیدش مگر بیدار جوییدش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپرسیدم به کوی دل ز پیری من از آن دلبر</p></div>
<div class="m2"><p>اشارت کرد آن پیرم که در اسرار جوییدش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفتم پیر را بالله تویی اسرار گفت آری</p></div>
<div class="m2"><p>منم دریای پرگوهر به دریابار جوییدش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی گوهر که دریا را به نور خویش پر دارد</p></div>
<div class="m2"><p>مسلمانان مسلمانان در آن انوار جوییدش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو یوسف شمس تبریزی به بازار صفا آمد</p></div>
<div class="m2"><p>مر اخوان صفا را گو در آن بازار جوییدش</p></div></div>