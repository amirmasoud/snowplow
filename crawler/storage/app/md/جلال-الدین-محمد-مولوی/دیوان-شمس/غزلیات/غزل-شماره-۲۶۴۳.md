---
title: >-
    غزل شمارهٔ ۲۶۴۳
---
# غزل شمارهٔ ۲۶۴۳

<div class="b" id="bn1"><div class="m1"><p>ای شاه تو ترکی عجمی وار چرایی</p></div>
<div class="m2"><p>تو جان و جهانی تو و بیمار چرایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلزار چو رنگ از صدقات تو ببردند</p></div>
<div class="m2"><p>گلزار بده زان رخ و پرخار چرایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الحق تو نگفتی و دم باده او گفت</p></div>
<div class="m2"><p>ای خواجه منصور تو بر دار چرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غار فتم چون دل و دلدار حریفند</p></div>
<div class="m2"><p>دلدار چو شد ای دل در غار چرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شاه نشد لیک پی چشم بد این گو</p></div>
<div class="m2"><p>گر شاه بشد مخزن اسرار چرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بیخ دلت نیست در آن آب حیاتش</p></div>
<div class="m2"><p>ای باغ چنین تازه و پربار چرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر راه نبرده‌ست دلت جانب گلزار</p></div>
<div class="m2"><p>خوش بو و شکرخنده و دلدار چرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دیو زند طعنه که خود نیست سلیمان</p></div>
<div class="m2"><p>ای دیو اگر نیست تو در کار چرایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر چشمه دل گر نه پری خانه حسن است</p></div>
<div class="m2"><p>ای جان سراسیمه پری دار چرایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای مریم جان گر تو نه‌ای حامل عیسی</p></div>
<div class="m2"><p>زان زلف چلیپا پی زنار چرایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر از می شمس الحق تبریز نه مستی</p></div>
<div class="m2"><p>پس معتکف خانه خمار چرایی</p></div></div>