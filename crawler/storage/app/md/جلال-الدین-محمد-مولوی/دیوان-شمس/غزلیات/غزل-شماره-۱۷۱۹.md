---
title: >-
    غزل شمارهٔ ۱۷۱۹
---
# غزل شمارهٔ ۱۷۱۹

<div class="b" id="bn1"><div class="m1"><p>پیشتر آ می لبا تا همه شیدا شویم</p></div>
<div class="m2"><p>بیشتر آ گوهرا تا همه دریا رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست به هم وادهیم حلقه صفت جوق جوق</p></div>
<div class="m2"><p>جمع معلق زنان مست به دریا دویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر لب دریای عشق تازه بروییم باز</p></div>
<div class="m2"><p>های که چون گلستان تا به ابد ما نویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز جگر گلستان شعله دیگر زنیم</p></div>
<div class="m2"><p>چون ز رخ آتشین مایه صد پرتویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوهر ما رو نمود لیک از آن سوی بحر</p></div>
<div class="m2"><p>آه که تو زین سوی آه که ما زان سویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه سوارا به سر تاج بجنبان چنین</p></div>
<div class="m2"><p>تاج تو را گوهریم اسپ تو را ما جویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر دارش کنیم هر کی بگوید یکیم</p></div>
<div class="m2"><p>آتش اندرزنیم هر کی بگوید دویم</p></div></div>