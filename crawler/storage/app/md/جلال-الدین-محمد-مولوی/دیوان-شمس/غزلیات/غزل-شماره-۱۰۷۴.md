---
title: >-
    غزل شمارهٔ ۱۰۷۴
---
# غزل شمارهٔ ۱۰۷۴

<div class="b" id="bn1"><div class="m1"><p>گرم در گفتار آمد آن صنم این الفرار</p></div>
<div class="m2"><p>بانگ خیزاخیز آمد در عدم این الفرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد هزاران شعله بر در صد هزاران مشعله</p></div>
<div class="m2"><p>کیست بر در کیست بر در هم منم این الفرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از درون نی آن منم گویان که بر در کیست آن</p></div>
<div class="m2"><p>هم منم بر در که حلقه می‌زنم این الفرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که پندارد دو نیمم پس دو نیمش کرد قهر</p></div>
<div class="m2"><p>ور یکی ام پس هم آب و روغنم این الفرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون یکی باشم که زلفم صد هزاران ظلمتست</p></div>
<div class="m2"><p>چون دو باشم چونک ماه روشنم این الفرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد خانه چند جویی تو مرا چون کاله دزد</p></div>
<div class="m2"><p>بنگر این دزدی که شد بر روزنم این الفرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین قفس سر را ز هر سوراخ بیرون می‌کنم</p></div>
<div class="m2"><p>سوی وصلت پر خود را می‌کنم این الفرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در درون این قفس تن در سر سودا گداخت</p></div>
<div class="m2"><p>وز قفس بیرون به هر دم گردنم این الفرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌می از شمس الحق تبریز مست گفتنم</p></div>
<div class="m2"><p>طوطیم یا بلبلم یا سوسنم این الفرار</p></div></div>