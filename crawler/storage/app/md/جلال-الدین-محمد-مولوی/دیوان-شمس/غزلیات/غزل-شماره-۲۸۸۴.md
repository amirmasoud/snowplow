---
title: >-
    غزل شمارهٔ ۲۸۸۴
---
# غزل شمارهٔ ۲۸۸۴

<div class="b" id="bn1"><div class="m1"><p>به شکرخنده بتا نرخ شکر می‌شکنی</p></div>
<div class="m2"><p>چه زند پیش عقیق تو عقیق یمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلرخا سوی گلستان دو سه هفته بمرو</p></div>
<div class="m2"><p>تا ز شرم تو نریزد گل سرخ چمنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل چه باشد که اگر جانب گردون نگری</p></div>
<div class="m2"><p>سرنگون زهره و مه را ز فلک درفکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حق تو را از جهت فتنه و شور آورده‌ست</p></div>
<div class="m2"><p>فتنه و شور و قیامت نکنی پس چه کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی چون آتش از آن داد که دل‌ها سوزی</p></div>
<div class="m2"><p>شکن زلف بدان داد که دل‌ها شکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ما بتکده‌ها نقش تو در وی شمنی</p></div>
<div class="m2"><p>هر بتی رو به شمن کرده که تو آن منی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برمکن تو دل خود از من ازیرا به جفا</p></div>
<div class="m2"><p>گر که قاف شود دل تو ز بیخش بکنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تک چاه زنخدان تو نادر آبی است</p></div>
<div class="m2"><p>که به هر چه که درافتم بنماید رسنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غمت بوالحسنان مذهب و دین گم کردند</p></div>
<div class="m2"><p>زان سبب که حسن اندر حسن اندر حسنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیرکان را رخ تو مست از آن می‌دارد</p></div>
<div class="m2"><p>تا در این بزم ندانند که تو در چه فنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کافری ای دل اگر در جز او دل بندی</p></div>
<div class="m2"><p>کافری ای تن اگر بر جز این عشق تنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی وی ار بر فلکی تو به خدا در گوری</p></div>
<div class="m2"><p>هر چه پوشی به جز از خلعت او در کفنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس تبریز که در روح وطن ساخته‌ای</p></div>
<div class="m2"><p>جان جان‌هاست وطن چونک تو جان را وطنی</p></div></div>