---
title: >-
    غزل شمارهٔ ۲۰۱۳
---
# غزل شمارهٔ ۲۰۱۳

<div class="b" id="bn1"><div class="m1"><p>ساقیا برخیز و می در جام کن</p></div>
<div class="m2"><p>وز شراب عشق دل را دام کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام رندی را بکن بر خود درست</p></div>
<div class="m2"><p>خویشتن را لاابالی نام کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ گردنده تو را چون رام شد</p></div>
<div class="m2"><p>مرکب بی‌مرکبی را رام کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش بی‌باکی اندر چرخ زن</p></div>
<div class="m2"><p>خاک تیره بر سر ایام کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مذهب زناربندان پیشه گیر</p></div>
<div class="m2"><p>خدمت کاووس و آذرنام کن</p></div></div>