---
title: >-
    غزل شمارهٔ ۲۲۵۶
---
# غزل شمارهٔ ۲۲۵۶

<div class="b" id="bn1"><div class="m1"><p>به قرار تو او رسد که بود بی‌قرار تو</p></div>
<div class="m2"><p>که به گلزار تو رسد دل خسته به خار تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل و سوسن از آن تو همه گلشن از آن تو</p></div>
<div class="m2"><p>تلفش از خزان تو طربش از بهار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زمین تا به آسمان همه گویان و خامشان</p></div>
<div class="m2"><p>چو دل و جان عاشقان به درون بی‌قرار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه سوداپرست تو همه عالم به دست تو</p></div>
<div class="m2"><p>نفسی پست و مست تو نفسی در خمار تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه زیر و زبر ز تو همگان بی‌خبر ز تو</p></div>
<div class="m2"><p>چه غریب است نظر به تو چه خوش است انتظار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کند سرو و باغ را چو نظر نیست زاغ را</p></div>
<div class="m2"><p>تو ز بلبل فغان شنو که وی است اختیار تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم از کار مانده‌ای ز خریدار مانده‌ای</p></div>
<div class="m2"><p>به فراغت نظرکنان به سوی کار و بار تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذارم ز بحر و پل بگریزم ز جزو و کل</p></div>
<div class="m2"><p>چه کنم من عذار گل که ندارد عذار تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه کنم عمر مرده را تن و جان فسرده را</p></div>
<div class="m2"><p>دو سه روز شمرده را چو منم در شمار تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دل و چشم و گوش‌ها ز تو نوشند نوش‌ها</p></div>
<div class="m2"><p>همه هر دم شکوفه‌ها شکفد در نثار تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس از این جان که دارمش به خموشی سپارمش</p></div>
<div class="m2"><p>ز کجا خامشم هلد هوس جان سپار تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خموشی نهان شدن چو شکارم نتان شدن</p></div>
<div class="m2"><p>که شکار و شکاریان نجهند از شکار تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه فربه ز بوی تو همه لاغر ز هجر تو</p></div>
<div class="m2"><p>همه شادی و گریه شان اثر و یادگار تو</p></div></div>