---
title: >-
    غزل شمارهٔ ۲۰۵۱
---
# غزل شمارهٔ ۲۰۵۱

<div class="b" id="bn1"><div class="m1"><p>می‌بینمت که عزم جفا می‌کنی مکن</p></div>
<div class="m2"><p>عزم عتاب و فرقت ما می‌کنی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مرغزار غیرت چون شیر خشمگین</p></div>
<div class="m2"><p>در خونم ای دو دیده چرا می‌کنی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخت مرا چو کلک نگون می‌کنی مکن</p></div>
<div class="m2"><p>پشت مرا چو دال دوتا می‌کنی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای تو تمام لطف خدا و عطای او</p></div>
<div class="m2"><p>خود را نکال و قهر خدا می‌کنی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیوند کرده‌ای کرم و لطف با دلم</p></div>
<div class="m2"><p>پیوند کرده را چه جدا می‌کنی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن بیذقی که شاه شده‌ست از رخ خوشت</p></div>
<div class="m2"><p>بازش به مات غم چه گدا می‌کنی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن بنده‌ای که بدر شد از پرتو رخت</p></div>
<div class="m2"><p>چون ماه نو ز غصه دوتا می‌کنی مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر گبر و مؤمن است چو کشته هوای توست</p></div>
<div class="m2"><p>بر گبر کشته تو چه غزا می‌کنی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌هوش شو چو موسی و همچون عصا خموش</p></div>
<div class="m2"><p>مانند طور تو چه صدا می‌کنی مکن</p></div></div>