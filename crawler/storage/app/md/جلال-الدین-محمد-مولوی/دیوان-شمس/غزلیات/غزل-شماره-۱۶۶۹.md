---
title: >-
    غزل شمارهٔ ۱۶۶۹
---
# غزل شمارهٔ ۱۶۶۹

<div class="b" id="bn1"><div class="m1"><p>آتشی نو در وجود اندرزدیم</p></div>
<div class="m2"><p>در میان محو نو اندرشدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک و بد اندر جهان هستی است</p></div>
<div class="m2"><p>ما نه نیکیم ای برادر نی بدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه چرخ دزد از ما برده بود</p></div>
<div class="m2"><p>شب عسس رفتیم و از وی بستدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما یکی بودیم با صد ما و من</p></div>
<div class="m2"><p>یک جوی زان یک نماند و ما صدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خودی نارفته نتوان آمدن</p></div>
<div class="m2"><p>از خودی رفتیم وانگه آمدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قد ما شد پست اندر قد عشق</p></div>
<div class="m2"><p>قد ما چون پست شد عالی قدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیشه مردی ز حق آموختیم</p></div>
<div class="m2"><p>پهلوان عشق و یار احمدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیست و نه حرف است بر لوح وجود</p></div>
<div class="m2"><p>حرف‌ها شستیم و اندر ابجدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعد شمس الدین تبریزی بتافت</p></div>
<div class="m2"><p>وز قران سعد او ما اسعدیم</p></div></div>