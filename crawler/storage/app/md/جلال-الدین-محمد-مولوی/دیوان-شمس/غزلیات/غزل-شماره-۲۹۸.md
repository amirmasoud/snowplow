---
title: >-
    غزل شمارهٔ ۲۹۸
---
# غزل شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>آه از این زشتان که مه رو می‌نمایند از نقاب</p></div>
<div class="m2"><p>از درون سو کاه تاب و از برون سو ماهتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنگ دجال از درون و رنگ ابدال از برون</p></div>
<div class="m2"><p>دام دزدان در ضمیر و رمز شاهان در خطاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق چادر مباش و خر مران در آب و گل</p></div>
<div class="m2"><p>تا نمانی ز آب و گل مانند خر اندر خلاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به سگ نان افکنی سگ بو کند آنگه خورد</p></div>
<div class="m2"><p>سگ نه‌ای شیری چه باشد بهر نان چندین شتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر آن مردار بینی رنگکی گویی که جان</p></div>
<div class="m2"><p>جان کجا رنگ از کجا جان را بجو جان را بیاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو سؤال و حاجتی دلبر جواب هر سؤال</p></div>
<div class="m2"><p>چون جواب آید فنا گردد سؤال اندر جواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خطابش هست گشتی چون شراب از سعی آب</p></div>
<div class="m2"><p>وز شرابش نیست گشتی همچو آب اندر شراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او ز نازش سر کشیده همچو آتش در فروغ</p></div>
<div class="m2"><p>تو ز خجلت سر فکنده چون خطا پیش صواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خزان غارتی مر باغ را بی‌برگ کرد</p></div>
<div class="m2"><p>عدل سلطان بهار آمد برای فتح باب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برگ‌ها چون نامه‌ها بر وی نبشته خط سبز</p></div>
<div class="m2"><p>شرح آن خط‌ها بجو از عنده‌ام الکتاب</p></div></div>