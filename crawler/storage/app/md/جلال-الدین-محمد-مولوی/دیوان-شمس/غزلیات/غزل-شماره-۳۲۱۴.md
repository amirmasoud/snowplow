---
title: >-
    غزل شمارهٔ ۳۲۱۴
---
# غزل شمارهٔ ۳۲۱۴

<div class="b" id="bn1"><div class="m1"><p>یا مالک دمة الزمان</p></div>
<div class="m2"><p>یا فاتح جنة الامعانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لا هوتک موضح المصادر</p></div>
<div class="m2"><p>ناسوتک سلم الامانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من رام لقاک فی جهات</p></div>
<div class="m2"><p>ردوه بفول لن ترانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کم اتلفنی بلن حبیبی</p></div>
<div class="m2"><p>لما اتلفنی بلن اتانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کم رد علی بات وصل</p></div>
<div class="m2"><p>کم عنه رجعت قد دعانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کم عانق روحه و روحی</p></div>
<div class="m2"><p>کم جالسنی بلا مکان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کم البسنی ببرد تیه</p></div>
<div class="m2"><p>کم اطعمنی و کم سقانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کم اسکرنی بکاس حب</p></div>
<div class="m2"><p>بین الحرفاء و المغانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا قلب کفاک لا تطول</p></div>
<div class="m2"><p>بالله علیک یا لسانی</p></div></div>