---
title: >-
    غزل شمارهٔ ۷
---
# غزل شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>بنشسته‌ام من بر درت تا بوک برجوشد وفا</p></div>
<div class="m2"><p>باشد که بگشایی دری گویی که برخیز اندرآ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرقست جانم بر درت در بوی مشک و عنبرت</p></div>
<div class="m2"><p>ای صد هزاران مرحمت بر روی خوبت دایما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماییم مست و سرگران فارغ ز کار دیگران</p></div>
<div class="m2"><p>عالم اگر برهم رود عشق تو را بادا بقا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق تو کف برهم زند صد عالم دیگر کند</p></div>
<div class="m2"><p>صد قرن نو پیدا شود بیرون ز افلاک و خلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عشق خندان همچو گل وی خوش نظر چون عقل کل</p></div>
<div class="m2"><p>خورشید را درکش به جل ای شهسوار هل اتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز ما مهمان تو مست رخ خندان تو</p></div>
<div class="m2"><p>چون نام رویت می‌برم دل می‌رود والله ز جا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو بام غیر بام تو کو نام غیر نام تو</p></div>
<div class="m2"><p>کو جام غیر جام تو ای ساقی شیرین ادا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر زنده جانی یابمی من دامنش برتابمی</p></div>
<div class="m2"><p>ای کاشکی درخوابمی در خواب بنمودی لقا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بر درت خیل و حشم بیرون خرام ای محتشم</p></div>
<div class="m2"><p>زیرا که سرمست و خوشم زان چشم مست دلربا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>افغان و خون دیده بین صد پیرهن بدریده بین</p></div>
<div class="m2"><p>خون جگر پیچیده بین بر گردن و روی و قفا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن کس که بیند روی تو مجنون نگردد کو بگو</p></div>
<div class="m2"><p>سنگ و کلوخی باشد او او را چرا خواهم بلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رنج و بلایی زین بتر کز تو بود جان بی‌خبر</p></div>
<div class="m2"><p>ای شاه و سلطان بشر لا تبل نفسا بالعمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان‌ها چو سیلابی روان تا ساحل دریای جان</p></div>
<div class="m2"><p>از آشنایان منقطع با بحر گشته آشنا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیلی روان اندر وله سیلی دگر گم کرده ره</p></div>
<div class="m2"><p>الحمدلله گوید آن وین آه و لا حول و لا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای آفتابی آمده بر مفلسان ساقی شده</p></div>
<div class="m2"><p>بر بندگان خود را زده باری کرم باری عطا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گل دیده ناگه مر تو را بدریده جان و جامه را</p></div>
<div class="m2"><p>وان چنگ زار از چنگ تو افکنده سر پیش از حیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مقبلترین و نیک پی در برج زهره کیست نی</p></div>
<div class="m2"><p>زیرا نهد لب بر لبت تا از تو آموزد نوا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نی‌ها و خاصه نیشکر بر طمع این بسته کمر</p></div>
<div class="m2"><p>رقصان شده در نیستان یعنی تعز من تشا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بد بی‌تو چنگ و نی حزین برد آن کنار و بوسه این</p></div>
<div class="m2"><p>دف گفت می‌زن بر رخم تا روی من یابد بها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این جان پاره پاره را خوش پاره پاره مست کن</p></div>
<div class="m2"><p>تا آن چه دوشش فوت شد آن را کند این دم قضا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حیفست ای شاه مهین هشیار کردن این چنین</p></div>
<div class="m2"><p>والله نگویم بعد از این هشیار شرحت ای خدا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یا باده ده حجت مجو یا خود تو برخیز و برو</p></div>
<div class="m2"><p>یا بنده را با لطف تو شد صوفیانه ماجرا</p></div></div>