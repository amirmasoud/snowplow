---
title: >-
    غزل شمارهٔ ۵۵۹
---
# غزل شمارهٔ ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>زهره عشق هر سحر بر در ما چه می‌کند</p></div>
<div class="m2"><p>دشمن جان صد قمر بر در ما چه می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که بدید از او نظر باخبرست و بی‌خبر</p></div>
<div class="m2"><p>او ملکست یا بشر بر در ما چه می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر جهان زبر شده آب مرا ز سر شده</p></div>
<div class="m2"><p>سنگ از او گهر شده بر در ما چه می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بت شنگ پرده‌ای گر تو نه فتنه کرده‌ای</p></div>
<div class="m2"><p>هر نفسی چنین حشر بر در ما چه می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نه که روز روشنی پیشه گرفته رهزنی</p></div>
<div class="m2"><p>روز به روز و ره گذر بر در ما چه می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور نه که دوش مست او آمد و درشکست او</p></div>
<div class="m2"><p>پس به نشانه این کمر بر در ما چه می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نه جمال حسن او گرد برآرد از عدم</p></div>
<div class="m2"><p>این همه گرد شور و شر بر در ما چه می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تبریز شمس دین سوی که رای می‌کند</p></div>
<div class="m2"><p>بحر چه موج زد گهر بر در ما چه می‌کند</p></div></div>