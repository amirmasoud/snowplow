---
title: >-
    غزل شمارهٔ ۱۸۹۵
---
# غزل شمارهٔ ۱۸۹۵

<div class="b" id="bn1"><div class="m1"><p>بفریفتیم دوش و پرندوش به دستان</p></div>
<div class="m2"><p>خوردم دغل گرم تو چون عشوه پرستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی عهد نکردی بروم بازبیایم</p></div>
<div class="m2"><p>سوگند نخوردی که بجویم دل مستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که به بستان بر من چاشت بیایید</p></div>
<div class="m2"><p>رفتی تو سحرگاه و ببستی در بستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عشوه تو گرمتر از باد تموزی</p></div>
<div class="m2"><p>وی چهره تو خوبتر از روی گلستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی که دغل از چو تو یاری به چه ماند</p></div>
<div class="m2"><p>در عین تموزی بجهد برق زمستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زانک تو را عشوه دهد کس گله کم کن</p></div>
<div class="m2"><p>صد شعبده کردی تو یکی شعبده بستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر وعده مکن صبر که گر صبر نبودی</p></div>
<div class="m2"><p>هرگز نرسیدی مدد از نیست بهستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور نه بکنم غمز و بگویم که سبب چیست</p></div>
<div class="m2"><p>زان سان که تو اقرار کنی که سبب است آن</p></div></div>