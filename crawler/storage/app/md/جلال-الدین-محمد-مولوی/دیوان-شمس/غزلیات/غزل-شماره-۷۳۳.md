---
title: >-
    غزل شمارهٔ ۷۳۳
---
# غزل شمارهٔ ۷۳۳

<div class="b" id="bn1"><div class="m1"><p>ذره ذره آفتاب عشق دردی خوار باد</p></div>
<div class="m2"><p>مو به موی ما بدان سر جعفر طیار باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذره‌ها بر آفتابت هر زمان بر می‌زنند</p></div>
<div class="m2"><p>هر که این بر خورد از تو از تو برخوردار باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا یک تار مویت بر هوس سر می‌نهد</p></div>
<div class="m2"><p>تار ما را پود باد و پود ما را تار باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بیابان غم از دوری دارالملک وصل</p></div>
<div class="m2"><p>چند غم بردار بودستم که غم بر دار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار مسکینی که هر دم طعنه گل می‌کشد</p></div>
<div class="m2"><p>خواجه گلزار باد و از حسد گل زار باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل پرستان چمن را دشمن مخفیست مار</p></div>
<div class="m2"><p>این چمن بی‌مار باد و دشمنش بیمار باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونک غمخواری نباشد سخت دشوارست غم</p></div>
<div class="m2"><p>همنشین غمخوار باد و بعد از این غم خوار باد</p></div></div>