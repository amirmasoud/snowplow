---
title: >-
    غزل شمارهٔ ۲۴۶۵
---
# غزل شمارهٔ ۲۴۶۵

<div class="b" id="bn1"><div class="m1"><p>آمده‌ای که راز من بر همگان بیان کنی</p></div>
<div class="m2"><p>و آن شه بی‌نشانه را جلوه دهی نشان کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش خیال مست تو آمد و جام بر کفش</p></div>
<div class="m2"><p>گفتم می نمی‌خورم گفت مکن زیان کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ترسم ار خورم شرم بپرد از سرم</p></div>
<div class="m2"><p>دست برم به جعد تو باز ز من کران کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دید که ناز می‌کنم گفت بیا عجب کسی</p></div>
<div class="m2"><p>جان به تو روی آورد روی بدو گران کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با همگان پلاس و کم با چو منی پلاس هم</p></div>
<div class="m2"><p>خاصبک نهان منم راز ز من نهان کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنج دل زمین منم سر چه نهی تو بر زمین</p></div>
<div class="m2"><p>قبله آسمان منم رو چه به آسمان کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی شهی نگر که او نور نظر دهد تو را</p></div>
<div class="m2"><p>ور به ستیزه سر کشی روز اجل چنان کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگ رخت که داد رو زرد شو از برای او</p></div>
<div class="m2"><p>چون ز پی سیاهه‌ای روی چو زعفران کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو خروس باش نر وقت شناس و پیش رو</p></div>
<div class="m2"><p>حیف بود خروس را ماده چو ماکیان کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کژ بنشین و راست گو راست بود سزا بود</p></div>
<div class="m2"><p>جان و روان تو منم سوی دگر روان کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به مثال اقرضوا قرض دهی قراضه‌ای</p></div>
<div class="m2"><p>نیم قراضه قلب را گنج کنی و کان کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور دو سه روز چشم را بند کنی باتقوا</p></div>
<div class="m2"><p>چشمه چشم حس را بحر در عیان کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور به نشان ما روی راست چو تیر ساعتی</p></div>
<div class="m2"><p>قامت تیر چرخ را بر زه خود کمان کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهتر از این کرم بود جرم تو را گنه تو را</p></div>
<div class="m2"><p>شرح کنم که پیش من بر چه نمط فغان کنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس که نگنجد آن سخن کو بنبشت در دهان</p></div>
<div class="m2"><p>گر همه ذره ذره را بازکشی دهان کنی</p></div></div>