---
title: >-
    غزل شمارهٔ ۲۶۴۷
---
# غزل شمارهٔ ۲۶۴۷

<div class="b" id="bn1"><div class="m1"><p>دلا چون واقف اسرار گشتی</p></div>
<div class="m2"><p>ز جمله کارها بی‌کار گشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان سودایی و دیوانه می‌باش</p></div>
<div class="m2"><p>چرا عاقل شدی هشیار گشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تفکر از برای برد باشد</p></div>
<div class="m2"><p>تو سرتاسر همه ایثار گشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان ترتیب مجنون را نگه دار</p></div>
<div class="m2"><p>که از ترتیب‌ها بیزار گشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تو مستور و عاقل خواستی شد</p></div>
<div class="m2"><p>چرا سرمست در بازار گشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشستن گوشه ای سودت ندارد</p></div>
<div class="m2"><p>چو با رندان این ره یار گشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صحرا رو بدان صحرا که بودی</p></div>
<div class="m2"><p>در این ویرانه‌ها بسیار گشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خراباتی است در همسایه تو</p></div>
<div class="m2"><p>که از بوهای می خمار گشتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگیر این بو و می‌رو تا خرابات</p></div>
<div class="m2"><p>که همچون بو سبک رفتار گشتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کوه قاف رو مانند سیمرغ</p></div>
<div class="m2"><p>چه یار جغد و بوتیمار گشتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برو در بیشه معنی چو شیران</p></div>
<div class="m2"><p>چه یار روبه و کفتار گشتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرو بر بوی پیراهان یوسف</p></div>
<div class="m2"><p>که چون یعقوب ماتم دار گشتی</p></div></div>