---
title: >-
    غزل شمارهٔ ۷۷۱
---
# غزل شمارهٔ ۷۷۱

<div class="b" id="bn1"><div class="m1"><p>هله عاشقان بکوشید که چو جسم و جان نماند</p></div>
<div class="m2"><p>دلتان به چرخ پرد چو بدن گران نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و جان به آب حکمت ز غبارها بشویید</p></div>
<div class="m2"><p>هله تا دو چشم حسرت سوی خاکدان نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه که هر چه در جهانست نه که عشق جان آنست</p></div>
<div class="m2"><p>جز عشق هر چه بینی همه جاودان نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدم تو همچو مشرق اجل تو همچو مغرب</p></div>
<div class="m2"><p>سوی آسمان دیگر که به آسمان نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ره آسمان درونست پر عشق را بجنبان</p></div>
<div class="m2"><p>پر عشق چون قوی شد غم نردبان نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مبین جهان ز بیرون که جهان درون دیده‌ست</p></div>
<div class="m2"><p>چو دو دیده را ببستی ز جهان جهان نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل تو مثال بامست و حواس ناودان‌ها</p></div>
<div class="m2"><p>تو ز بام آب می‌خور که چو ناودان نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو ز لوح دل فروخوان به تمامی این غزل را</p></div>
<div class="m2"><p>منگر تو در زبانم که لب و زبان نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن آدمی کمان و نفس و سخن چو تیرش</p></div>
<div class="m2"><p>چو برفت تیر و ترکش عمل کمان نماند</p></div></div>