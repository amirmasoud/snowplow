---
title: >-
    غزل شمارهٔ ۶۵۹
---
# غزل شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>دلم امروز خوی یار دارد</p></div>
<div class="m2"><p>هوای روی چون گلنار دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که طاووس آن طرف پر می‌فشاند</p></div>
<div class="m2"><p>که بلبل آن طرف تکرار دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدای نای آن جا نکته گوید</p></div>
<div class="m2"><p>نوای چنگ بس اسرار دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگه برخیز فردا سوی او رو</p></div>
<div class="m2"><p>که او عاشق چو من بسیار دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بگشاید رخان تو دل نگهدار</p></div>
<div class="m2"><p>که بس آتش در آن رخسار دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولیکن عقل کو آن لحظه دل را</p></div>
<div class="m2"><p>که دل‌ها را لبش خمار دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ما کاری مجو چون داده‌ای می</p></div>
<div class="m2"><p>که می مر مرد را بی‌کار دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم افتان و خیزان دوش آمد</p></div>
<div class="m2"><p>که می مستی او اظهار دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دویدم پیش و گفتم باده خوردی</p></div>
<div class="m2"><p>نمی‌ترسی که عقل انکار دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بو کردم دهانش را بدیدم</p></div>
<div class="m2"><p>که بوی آن پری دیدار دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خداوندی شمس الدین تبریز</p></div>
<div class="m2"><p>که بوی خالق جبار دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بو تا بوی فرقی بس عظیمست</p></div>
<div class="m2"><p>و او بی‌حد و بی‌مقدار دارد</p></div></div>