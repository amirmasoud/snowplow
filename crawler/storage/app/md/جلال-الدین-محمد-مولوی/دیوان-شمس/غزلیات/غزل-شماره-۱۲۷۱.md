---
title: >-
    غزل شمارهٔ ۱۲۷۱
---
# غزل شمارهٔ ۱۲۷۱

<div class="b" id="bn1"><div class="m1"><p>باز درآمد طبیب از در رنجور خویش</p></div>
<div class="m2"><p>دست عنایت نهاد بر سر مهجور خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار دگر آن حبیب رفت بر آن غریب</p></div>
<div class="m2"><p>تا جگر او کشید شربت موفور خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شربت او چون ربود گشت فنا از وجود</p></div>
<div class="m2"><p>ساقی وحدت بماند ناظر و منظور خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوش ورا نیش نیست ور بودش راضیم</p></div>
<div class="m2"><p>نیست عسل خواره را چاره ز زنبور خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این شب هجران دراز با تو بگویم چراست</p></div>
<div class="m2"><p>فتنه شد آن آفتاب بر رخ مستور خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غفلت هر دلبری از رخ خود رحمتست</p></div>
<div class="m2"><p>ور نه ببستی نقاب بر رخ مشهور خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق حسن خودی لیک تو پنهان ز خود</p></div>
<div class="m2"><p>خلعت وصلت بپوش بر تن این عور خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکر که خورشید عشق رفت به برج حمل</p></div>
<div class="m2"><p>در دل و جان‌ها فکند پرورش نور خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر که موسی برست از همه فرعونیان</p></div>
<div class="m2"><p>باز به میقات وصل آمد بر طور خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عیسی جان دررسید بر سر عازر دمید</p></div>
<div class="m2"><p>عازر از افسون او حشر شد از گور خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز سلیمان رسید دیو و پری جمع شد</p></div>
<div class="m2"><p>بر همه شان عرضه کرد خاتم و منشور خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساقی اگر بایدت تا کنم این را تمام</p></div>
<div class="m2"><p>باده گویا بنه بر لب مخمور خویش</p></div></div>