---
title: >-
    غزل شمارهٔ ۱۶۷۲
---
# غزل شمارهٔ ۱۶۷۲

<div class="b" id="bn1"><div class="m1"><p>روز باران است و ما جو می کنیم</p></div>
<div class="m2"><p>بر امید وصل دستی می زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابرها آبستن از دریای عشق</p></div>
<div class="m2"><p>ما ز ابر عشق هم آبستنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو مگو مطرب نیم دستی بزن</p></div>
<div class="m2"><p>تو بیا ما خود تو را مطرب کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشن است آن خانه گویی آن کیست</p></div>
<div class="m2"><p>ما غلام خانه‌های روشنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما حجاب آب حیوان خودیم</p></div>
<div class="m2"><p>بر سر آن آب ما چون روغنیم</p></div></div>