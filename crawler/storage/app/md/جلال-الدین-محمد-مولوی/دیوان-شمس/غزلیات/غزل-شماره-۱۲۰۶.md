---
title: >-
    غزل شمارهٔ ۱۲۰۶
---
# غزل شمارهٔ ۱۲۰۶

<div class="b" id="bn1"><div class="m1"><p>سوی لبش هر آنک شد زخم خورد ز پیش و پس</p></div>
<div class="m2"><p>زانک حوالی عسل نیش زنان بود مگس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی ویست گلستان مار بود در او نهان</p></div>
<div class="m2"><p>جعد ویست همچو شب مجمع دزد و هر عسس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کان زمردی مها دیده مار برکنی</p></div>
<div class="m2"><p>ماه دوهفته‌ای شها غم نخوریم از غلس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌تو جهان چه فن زند بی‌تو چگونه تن زند</p></div>
<div class="m2"><p>جان و جهان غلام تو جان و جهان تویی و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نصرت رستمان تویی فتح و ظفررسان تویی</p></div>
<div class="m2"><p>هست اثر حمایتت گر زره‌ست وگر فرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمس تو معنوی بود آن نه که منطوی بود</p></div>
<div class="m2"><p>صد مه و آفتاب را نور توست مقتبس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ میان آب تو بر دوران همی‌زند</p></div>
<div class="m2"><p>عقل بر طبیبیت عرضه همی‌کند مجس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذره به ذره طمع‌ها صف زده پیش خوان تو</p></div>
<div class="m2"><p>سجده کنان و دم زنان بهر امید هر نفس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست چنین چنین کند لطف که من چنان دهم</p></div>
<div class="m2"><p>آنچ بهار می‌دهد از دم خود به خار و خس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاک که نور می‌خورد نقره و زر نبات او</p></div>
<div class="m2"><p>خاک که آب می‌خورد ماش شدست یا عدس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رنگ جهان چو سحرها عشق عصای موسوی</p></div>
<div class="m2"><p>باز کند دهان خود درکشدش به یک نفس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند بترسی ای دل از نقش خود و خیال خود</p></div>
<div class="m2"><p>چند گریز می‌کنی بازنگر که نیست کس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس کن و بس که کمتر از اسب سقای نیستی</p></div>
<div class="m2"><p>چونک بیافت مشتری باز کند از او جرس</p></div></div>