---
title: >-
    غزل شمارهٔ ۲۹۸۴
---
# غزل شمارهٔ ۲۹۸۴

<div class="b" id="bn1"><div class="m1"><p>ای مرغ گیر دام نهانی نهاده‌ای</p></div>
<div class="m2"><p>بر روی دام شعر دخانی نهاده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین هزار مرغ بدین فن بکشته‌ای</p></div>
<div class="m2"><p>پرهای کشته بهر نشانی نهاده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغان پاسبان تو هیهای می‌زنند</p></div>
<div class="m2"><p>درهای هویشان چه معانی نهاده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغان تشنه را به خرابات قرب خویش</p></div>
<div class="m2"><p>خم‌ها و باده‌های معانی نهاده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن خنب را که ساقی و مستیش بود نبرد</p></div>
<div class="m2"><p>از بهر شب روی که تو دانی نهاده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صبر و توبه عصمت اسپر سرشته‌ای</p></div>
<div class="m2"><p>و اندر جفا و خشم سنانی نهاده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی زحمت سنان و سپر بهر مخلصان</p></div>
<div class="m2"><p>ملکی درون سبع مثانی نهاده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر سواد چشم روان کرده موج نور</p></div>
<div class="m2"><p>و اندر جهان پیر جوانی نهاده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سینه کز مخیله تصویر می‌رود</p></div>
<div class="m2"><p>بی کلک و بی‌بنان تو بنانی نهاده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چندین حجاب لحم و عصب بر فراز دل</p></div>
<div class="m2"><p>دل را نفوذ و سیر عیانی نهاده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غمزه عجبتر است که چون تیر می‌پرد</p></div>
<div class="m2"><p>یا ابروی که بهر کمانی نهاده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اخلاق مختلف چو شرابات تلخ و نوش</p></div>
<div class="m2"><p>در جسم‌های همچو اوانی نهاده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وین شربت نهان مترشح شد از زبان</p></div>
<div class="m2"><p>سرجوش نطق را به لسانی نهاده‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر عین و هر عرض چو دهان بسته غنچه‌ای است</p></div>
<div class="m2"><p>کان را حجاب مهد غوانی نهاده‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روزی که بشکفانی و آن پرده برکشی</p></div>
<div class="m2"><p>ای جان جان جان که تو جانی نهاده‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل‌های بی‌قرار ببیند که در فراق</p></div>
<div class="m2"><p>از بهر چه نیاز و کشانی نهاده‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاموش تا بگوید آن جان گفته‌ها</p></div>
<div class="m2"><p>این چه دراز شعبده خوانی نهاده‌ای</p></div></div>