---
title: >-
    غزل شمارهٔ ۲۵۰۸
---
# غزل شمارهٔ ۲۵۰۸

<div class="b" id="bn1"><div class="m1"><p>مرا آن دلبر پنهان همی‌گوید به پنهانی</p></div>
<div class="m2"><p>به من ده جان به من ده جان چه باشد این گران جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی لحظه قلندر شو قلندر را مسخر شو</p></div>
<div class="m2"><p>سمندر شو سمندر شو در آتش رو به آسانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آتش رو در آتش رو در آتشدان ما خوش رو</p></div>
<div class="m2"><p>که آتش با خلیل ما کند رسم گلستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌دانی که خار ما بود شاهنشه گل‌ها</p></div>
<div class="m2"><p>نمی‌دانی که کفر ما بود جان مسلمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سراندازان سراندازان سراندازی سراندازی</p></div>
<div class="m2"><p>مسلمانان مسلمانان مسلمانی مسلمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خداوندا تو می‌دانی که صحرا از قفس خوشتر</p></div>
<div class="m2"><p>ولیکن جغد نشکیبد ز گورستان ویرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون دوران جان آمد که دریا را درآشامد</p></div>
<div class="m2"><p>زهی دوران زهی حلقه زهی دوران سلطانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمش چون نیست پوشیده فقیر باده نوشیده</p></div>
<div class="m2"><p>که هست اندر رخش پیدا فر و انوار سبحانی</p></div></div>