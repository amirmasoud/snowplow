---
title: >-
    غزل شمارهٔ ۱۶۹۰
---
# غزل شمارهٔ ۱۶۹۰

<div class="b" id="bn1"><div class="m1"><p>اندر دو کون جانا بی‌تو طرب ندیدم</p></div>
<div class="m2"><p>دیدم بسی عجایب چون تو عجب ندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتند سوز آتش باشد نصیب کافر</p></div>
<div class="m2"><p>محروم ز آتش تو جز بولهب ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بر دریچه دل بس گوش جان نهادم</p></div>
<div class="m2"><p>چندان سخن شنیدم اما دو لب ندیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر بنده ناگهانی کردی نثار رحمت</p></div>
<div class="m2"><p>جز لطف بی‌حد تو آن را سبب ندیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ساقی گزیده مانندت ای دو دیده</p></div>
<div class="m2"><p>اندر عجم نیامد و اندر عرب ندیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان باده که عصیرش اندر چرش نیامد</p></div>
<div class="m2"><p>وان شیشه که نظیرش اندر حلب ندیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندان بریز باده کز خود شوم پیاده</p></div>
<div class="m2"><p>کاندر خودی و هستی غیر تعب ندیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شمس و ای قمر تو ای شهد و ای شکر تو</p></div>
<div class="m2"><p>ای مادر و پدر تو جز تو نسب ندیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای عشق بی‌تناهی وی مظهر الهی</p></div>
<div class="m2"><p>هم پشت و هم پناهی کفوت لقب ندیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پولادپاره‌هاییم آهن رباست عشقت</p></div>
<div class="m2"><p>اصل همه طلب تو در تو طلب ندیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خامش کن ای برادر فضل و ادب رها کن</p></div>
<div class="m2"><p>تا تو ادب بخواندی در تو ادب ندیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای شمس حق تبریز ای اصل اصل جان‌ها</p></div>
<div class="m2"><p>بی‌بصره وجودت من یک رطب ندیدم</p></div></div>