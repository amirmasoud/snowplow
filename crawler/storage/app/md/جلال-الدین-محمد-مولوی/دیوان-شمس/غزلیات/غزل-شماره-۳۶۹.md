---
title: >-
    غزل شمارهٔ ۳۶۹
---
# غزل شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>در شهر شما یکی نگاریست</p></div>
<div class="m2"><p>کز وی دل و عقل بی‌قراریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نفسی را از او نصیبیست</p></div>
<div class="m2"><p>هر باغی را از او بهاریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر کویی از او فغانیست</p></div>
<div class="m2"><p>در هر راهی از او غباریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر گوشی از او سماعیست</p></div>
<div class="m2"><p>هر چشم از او در اعتباریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کار شوید ای حریفان</p></div>
<div class="m2"><p>کاین جا ما را عظیم کاریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنهان یاری به گوش من گفت</p></div>
<div class="m2"><p>کاین جا پنهان لطیف یاریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او بد که به این طریق می‌گفت</p></div>
<div class="m2"><p>کز تعبیه‌هاش دل نزاریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او بود رسول خویش و مرسل</p></div>
<div class="m2"><p>کان لهجه از آن شهریاریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوحست و امان غرقگانست</p></div>
<div class="m2"><p>روحست و نهان و آشکاریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرد ترشان مگرد زین پس</p></div>
<div class="m2"><p>چون پهلوی تو شکرنثاریست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد شکران طبع کم گرد</p></div>
<div class="m2"><p>کان شهوت نیز برگذاریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این جا شکریست بی‌نهایت</p></div>
<div class="m2"><p>این جا سر وقت پایداریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاموش کن ای دل و مپندار</p></div>
<div class="m2"><p>کو را حدیست یا کناریست</p></div></div>