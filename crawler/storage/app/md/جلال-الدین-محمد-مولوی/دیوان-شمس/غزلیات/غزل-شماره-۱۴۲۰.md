---
title: >-
    غزل شمارهٔ ۱۴۲۰
---
# غزل شمارهٔ ۱۴۲۰

<div class="b" id="bn1"><div class="m1"><p>اگر شد سود و سرمایه چه غمگینی چو من هستم</p></div>
<div class="m2"><p>برآور سر ز جود من که لاتأسوا نمودستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر فانی شود عالم ز دریایی بود شبنم</p></div>
<div class="m2"><p>گر افتاده‌ست او از خود نیفتاده‌ست از دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان ماهی عدم دریا درون ماهی این غوغا</p></div>
<div class="m2"><p>کنم صیدش اگر گم شد که من صیاد بی‌شستم</p></div></div>