---
title: >-
    غزل شمارهٔ ۶۷۰
---
# غزل شمارهٔ ۶۷۰

<div class="b" id="bn1"><div class="m1"><p>پریر آن چهره یارم چه خوش بود</p></div>
<div class="m2"><p>عتاب و ناز دلدارم چه خوش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یادم نیست هیچ آن ماجراها</p></div>
<div class="m2"><p>ولیکن زین خبر دارم چه خوش بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن بزم و در آن جمع و در آن عیش</p></div>
<div class="m2"><p>میان باغ و گلزارم چه خوش بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه مست جام عشق بودم</p></div>
<div class="m2"><p>رخ معشوق هشیارم چه خوش بود</p></div></div>