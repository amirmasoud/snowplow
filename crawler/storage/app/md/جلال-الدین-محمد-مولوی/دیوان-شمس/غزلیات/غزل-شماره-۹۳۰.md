---
title: >-
    غزل شمارهٔ ۹۳۰
---
# غزل شمارهٔ ۹۳۰

<div class="b" id="bn1"><div class="m1"><p>سپاس و شکر خدا را که بندها بگشاد</p></div>
<div class="m2"><p>میان به شکر چو بستیم بند ما بگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان رسید فلک از دعا و ناله من</p></div>
<div class="m2"><p>فلک دهان خود اندر ره دعا بگشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس که سینه ما سوخت در وفا جستن</p></div>
<div class="m2"><p>ز شرم ما عرق از صورت وفا بگشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ادیم روی سهیلیم هر کجا بنمود</p></div>
<div class="m2"><p>غلام چشمه عشقیم هر کجا بگشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس دریچه دل صد در نهانی بود</p></div>
<div class="m2"><p>که بسته بود خدا بنده خدا بگشاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این سرا که دو قندیل ماه و خورشیدست</p></div>
<div class="m2"><p>خدا ز جانب دل روزن سرا بگشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الست گفت حق و جان‌ها بلی گفتند</p></div>
<div class="m2"><p>برای صدق بلی حق ره بلا بگشاد</p></div></div>