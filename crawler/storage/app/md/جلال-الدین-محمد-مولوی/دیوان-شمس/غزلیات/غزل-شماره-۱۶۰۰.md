---
title: >-
    غزل شمارهٔ ۱۶۰۰
---
# غزل شمارهٔ ۱۶۰۰

<div class="b" id="bn1"><div class="m1"><p>از شهنشه شمس دین من ساغری را یافتم</p></div>
<div class="m2"><p>در درون ساغرش چشمه خوری را یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تابش سینه و برت را خود ندارد چشم تاب</p></div>
<div class="m2"><p>شکر ایزد را که من زین دلبری را یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میرداد قهر چون ماری فروکوبد سرش</p></div>
<div class="m2"><p>آنک گوید در دو کونش هم سری را یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون درون طره‌اش دریافتم دل را عجب</p></div>
<div class="m2"><p>در درون مشک رفتم عنبری را یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ببینی طوطی جان مرا گرد لبش</p></div>
<div class="m2"><p>می پرد پرک زنان که شکری را یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بپرسندت حکایت کن که من بر جام لعل</p></div>
<div class="m2"><p>عاشقی مستی جوانی می خوری را یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کسی منکر شود تو گردن او را ببند</p></div>
<div class="m2"><p>می کشانش روسیه که منکری را یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در میان طره‌اش رخسار چون آتش ببین</p></div>
<div class="m2"><p>گو میان مشک و عنبر مجمری را یافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون گشاید لعل را او تا نثار در کند</p></div>
<div class="m2"><p>گو که در خورشید از رحمت دری را یافتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون دکان سرپزان سرها و دل‌ها پیش او</p></div>
<div class="m2"><p>هست بی‌پایان در آن سرها سری را یافتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نگه کردم سر من بود پر از عشق او</p></div>
<div class="m2"><p>من برون از هر دو عالم منظری را یافتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من به برج ثور دیدم منکر آن آفتاب</p></div>
<div class="m2"><p>گاو جستم من ز ثور و خود خری را یافتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من صف رستم دلان جستم بدیدم شاه را</p></div>
<div class="m2"><p>ترک آن کردم چو بی‌صف صفدری را یافتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من همی‌کشتی سوی تبریز راندم می نرفت</p></div>
<div class="m2"><p>پس ز جان بر کشتی خود لنگری را یافتم</p></div></div>