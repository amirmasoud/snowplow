---
title: >-
    غزل شمارهٔ ۲۸۰۰
---
# غزل شمارهٔ ۲۸۰۰

<div class="b" id="bn1"><div class="m1"><p>در میان جان نشین کامروز جان دیگری</p></div>
<div class="m2"><p>کاین جهان خیره است در تو کز جهان دیگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش خرام ای سرو جان کامروز جان دیگری</p></div>
<div class="m2"><p>خوش بخند ای گلستان کز گلستان دیگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب خلقان رفت جمله در هوای آب و نان</p></div>
<div class="m2"><p>یوسفا در قحط عالم آب و نان دیگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو جهان زندگی و این جهان بندگی</p></div>
<div class="m2"><p>تو ز شاه شه نشان والله نشان دیگری</p></div></div>