---
title: >-
    غزل شمارهٔ ۱۷۲۹
---
# غزل شمارهٔ ۱۷۲۹

<div class="b" id="bn1"><div class="m1"><p>اگر چه شرط نهادیم و امتحان کردیم</p></div>
<div class="m2"><p>ز شرط‌ها بگذشتیم و رایگان کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه یک طرف از آسمان زمینی شد</p></div>
<div class="m2"><p>نه پاره پاره زمین را هم آسمان کردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه بام بلندست آسمان مگریز</p></div>
<div class="m2"><p>چه غم خوری ز بلندی چو نردبان کردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرت دهیم که چون تیر بر فلک بپری</p></div>
<div class="m2"><p>اگر ز غم تن بیچاره را کمان کردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه جان مدد جسم شد کثیفی یافت</p></div>
<div class="m2"><p>لطافتش بنمودیم و باز جان کردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تو دیوی ما دیو را فرشته کنیم</p></div>
<div class="m2"><p>وگر تو گرگی ما گرگ را شبان کردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ماهیی که به بحر عسل بخواهی تاخت</p></div>
<div class="m2"><p>هزار بارت از آن شهد در دهان کردیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه مرغ ضعیفی بجوی شاخ بلند</p></div>
<div class="m2"><p>بر این درخت سعادت که آشیان کردیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگیر ملک دو عالم که مالک الملکیم</p></div>
<div class="m2"><p>بیا به بزم که شمشیر در میان کردیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار ذره از این قطب آفتابی یافت</p></div>
<div class="m2"><p>بسا قراضه قلبی که ماش کان کردیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسا یخی بفسرده کز آفتاب کرم</p></div>
<div class="m2"><p>فسردگیش ببردیم و خوش روان کردیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر آب روح مکدر شد اندر این گرداب</p></div>
<div class="m2"><p>ز سیل‌ها و مددهاش خوش عنان کردیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چرا شکفته نباشی چو برگ می لرزی</p></div>
<div class="m2"><p>چه ناامیدی از ما که را زیان کردیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسا دلی که چو برگ درخت می لرزید</p></div>
<div class="m2"><p>به آخرش بگزیدیم و باغبان کردیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الست گفتیم از غیب و تو بلی گفتی</p></div>
<div class="m2"><p>چه شد بلی تو چون غیب را عیان کردیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پنیر صدق بگیر و به باغ روح بیا</p></div>
<div class="m2"><p>که ما بلی تو را باغ و بوستان کردیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خموش باش که تا سر به سر زبان گردی</p></div>
<div class="m2"><p>زبان نبود زبان تو ما زبان کردیم</p></div></div>