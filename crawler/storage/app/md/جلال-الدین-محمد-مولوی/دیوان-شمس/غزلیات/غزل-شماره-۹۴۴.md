---
title: >-
    غزل شمارهٔ ۹۴۴
---
# غزل شمارهٔ ۹۴۴

<div class="b" id="bn1"><div class="m1"><p>به باغ بلبل از این پس نوای ما گوید</p></div>
<div class="m2"><p>حدیث عشق شکرریز جان فزا گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ز رنگ رخ یار ما خبر دارد</p></div>
<div class="m2"><p>ز لاله زار و ز نسرین و گل چرا گوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز راه غیرت گوید که تا بپوشاند</p></div>
<div class="m2"><p>رها کند سر چشمه حدیث پا گوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که پاره پاره به تدریج ذره که گردد</p></div>
<div class="m2"><p>فنا شود که اگر تند و بر ولا گوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کهی که ذره بود پیش او دو صد که قاف</p></div>
<div class="m2"><p>دوان دوان شود آن دم که او بیا گوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گوش کوه شنید آن بیای فرخ او</p></div>
<div class="m2"><p>به سر بیاید و لبیک را دو تا گوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حق گلشن اقبال کاندر او مستی</p></div>
<div class="m2"><p>چو گل خموش که تا بلبلت ثنا گوید</p></div></div>