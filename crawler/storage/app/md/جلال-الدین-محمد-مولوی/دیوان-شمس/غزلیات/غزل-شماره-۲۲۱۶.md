---
title: >-
    غزل شمارهٔ ۲۲۱۶
---
# غزل شمارهٔ ۲۲۱۶

<div class="b" id="bn1"><div class="m1"><p>تن مزن ای پسر خوش دم خوش کام بگو</p></div>
<div class="m2"><p>بهر آرام دلم نام دلارام بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده من مدران و در احسان بگشا</p></div>
<div class="m2"><p>شیشه دل مشکن قصه آن جام بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور در لطف ببستی در اومید مبند</p></div>
<div class="m2"><p>بر سر بام برآ و ز سر بام بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور حدیث و صفت او شر و شوری دارد</p></div>
<div class="m2"><p>صفت این دل تنگ شررآشام بگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونک رضوان بهشتی تو صلایی درده</p></div>
<div class="m2"><p>چونک پیغامبر عشقی هله پیغام بگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه زندانی این دام بسی بشنودیم</p></div>
<div class="m2"><p>حال مرغی که برسته‌ست از این دام بگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن بند مگو و صفت قند بگو</p></div>
<div class="m2"><p>صفت راه مگو و ز سرانجام بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرح آن بحر که واگشت همه جان‌ها او است</p></div>
<div class="m2"><p>که فزون است ز ایام و ز اعوام بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور تنور تو بود گرم و دعای تو قبول</p></div>
<div class="m2"><p>غم هر ممتحن سوخته خام بگو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکر آن بهره که ما یافته‌ایم از در فضل</p></div>
<div class="m2"><p>فرصت ار دست دهد هم بر بهرام بگو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر از عام بترسی که سخن فاش کنی</p></div>
<div class="m2"><p>سخن خاص نهان در سخن عام بگو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور از آن نیز بترسی هله چون مرغ چمن</p></div>
<div class="m2"><p>دم به دم زمزمه بی‌الف و لام بگو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو اندیشه که دانی تو و دانای ضمیر</p></div>
<div class="m2"><p>سخنی بی‌نقط و بی‌مد و ادغام بگو</p></div></div>