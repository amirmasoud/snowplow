---
title: >-
    غزل شمارهٔ ۲۸۶۵
---
# غزل شمارهٔ ۲۸۶۵

<div class="b" id="bn1"><div class="m1"><p>در رخ عشق نگر تا به صفت مرد شوی</p></div>
<div class="m2"><p>نزد سردان منشین کز دمشان سرد شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رخ عشق بجو چیز دگر جز صورت</p></div>
<div class="m2"><p>کار آن است که با عشق تو هم درد شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کلوخی به صفت تو به هوا برنپری</p></div>
<div class="m2"><p>به هوا برشوی ار بشکنی و گرد شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو اگر نشکنی آن کت به سرشت او شکند</p></div>
<div class="m2"><p>چونک مرگت شکند کی گهر فرد شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگ چون زرد شود بیخ ترش سبز کند</p></div>
<div class="m2"><p>تو چرا قانعی از عشق کز او زرد شوی</p></div></div>