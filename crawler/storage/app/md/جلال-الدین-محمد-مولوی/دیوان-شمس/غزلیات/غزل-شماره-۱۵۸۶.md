---
title: >-
    غزل شمارهٔ ۱۵۸۶
---
# غزل شمارهٔ ۱۵۸۶

<div class="b" id="bn1"><div class="m1"><p>خویش را چون خار دیدم سوی گل بگریختم</p></div>
<div class="m2"><p>خویش را چون سرکه دیدم در شکر آمیختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاسه پرزهر بودم سوی تریاق آمدم</p></div>
<div class="m2"><p>ساغری دردی بدم در آب حیوان ریختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده پردرد بودم دست در عیسی زدم</p></div>
<div class="m2"><p>خام دیدم خویش را در پخته‌ای آویختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک کوی عشق را من سرمه جان یافتم</p></div>
<div class="m2"><p>شعر گشتم در لطافت سرمه را می بیختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق گوید راست می گویی ولی از خود مبین</p></div>
<div class="m2"><p>من چو بادم تو چو آتش من تو را انگیختم</p></div></div>