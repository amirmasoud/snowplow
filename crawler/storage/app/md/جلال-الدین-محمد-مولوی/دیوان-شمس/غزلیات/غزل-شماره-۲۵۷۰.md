---
title: >-
    غزل شمارهٔ ۲۵۷۰
---
# غزل شمارهٔ ۲۵۷۰

<div class="b" id="bn1"><div class="m1"><p>پنهان به میان ما می‌گردد سلطانی</p></div>
<div class="m2"><p>و اندر حشر موران افتاده سلیمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌بیند و می‌داند یک یک سر یاران را</p></div>
<div class="m2"><p>امروز در این مجمع شاهنشه سردانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسرار بر او ظاهر همچون طبق حلوا</p></div>
<div class="m2"><p>گر مکر کند دزدی ور راست رود جانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک و بد هر کس را از تخته پیشانی</p></div>
<div class="m2"><p>می‌بیند و می‌خواند با تجربه خط خوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مطبخ ما آمد یک بی‌من و بی‌مایی</p></div>
<div class="m2"><p>تا شور دراندازد بر ما ز نمکدانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز سماع ما چون دل سبکی دارد</p></div>
<div class="m2"><p>یا رب تو نگهدارش ز آسیب گران جانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن شیشه دلی کو دی بگریخت چو نامردان</p></div>
<div class="m2"><p>امروز همی‌آید پرشرم و پشیمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد سال اگر جایی بگریزد و بستیزد</p></div>
<div class="m2"><p>پرگریه و غم باشد بی‌دولت خندانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید چه غم دارد ار خشم کند گازر</p></div>
<div class="m2"><p>خاموش که بازآید بلبل به گلستانی</p></div></div>