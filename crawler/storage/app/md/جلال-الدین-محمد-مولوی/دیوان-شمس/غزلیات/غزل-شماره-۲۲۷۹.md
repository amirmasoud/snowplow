---
title: >-
    غزل شمارهٔ ۲۲۷۹
---
# غزل شمارهٔ ۲۲۷۹

<div class="b" id="bn1"><div class="m1"><p>این کیست این این کیست این در حلقه ناگاه آمده</p></div>
<div class="m2"><p>این نور اللهی است این از پیش الله آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این لطف و رحمت را نگر وین بخت و دولت را نگر</p></div>
<div class="m2"><p>در چاره بداختران با روی چون ماه آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیلی زیبا را نگر خوش طالب مجنون شده</p></div>
<div class="m2"><p>و آن کهربای روح بین در جذب هر کاه آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لذت بوهای او وز حسن و از خوهای او</p></div>
<div class="m2"><p>وز قل تعالوهای او جان‌ها به درگاه آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد نقش سازد بر عدم از چاکر و صاحب علم</p></div>
<div class="m2"><p>در دل خیالات خوشش زیبا و دلخواه آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تخییل‌ها را آن صمد روزی حقیقت‌ها کند</p></div>
<div class="m2"><p>تا دررسد در زندگی اشکال گمراه آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چاه شور این جهان در دلو قرآن رو برآ</p></div>
<div class="m2"><p>ای یوسف آخر بهر توست این دلو در چاه آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی باشد ای گفت زبان من از تو مستغنی شده</p></div>
<div class="m2"><p>با آفتاب معرفت در سایه شاه آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا رب مرا پیش از اجل فارغ کن از علم و عمل</p></div>
<div class="m2"><p>خاصه ز علم منطقی در جمله افواه آمده</p></div></div>