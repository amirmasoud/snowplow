---
title: >-
    غزل شمارهٔ ۲۰۲۸
---
# غزل شمارهٔ ۲۰۲۸

<div class="b" id="bn1"><div class="m1"><p>گر چه بسی نشستم در نار تا به گردن</p></div>
<div class="m2"><p>اکنون در آب وصلم با یار تا به گردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که تا به گردن در لطف‌هات غرقم</p></div>
<div class="m2"><p>قانع نگشت از من دلدار تا به گردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتا که سر قدم کن تا قعر عشق می‌رو</p></div>
<div class="m2"><p>زیرا که راست ناید این کار تا به گردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم سر من ای جان نعلین توست لیکن</p></div>
<div class="m2"><p>قانع شو ای دو دیده این بار تا به گردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتا تو کم ز خاری کز انتظار گل‌ها</p></div>
<div class="m2"><p>در خاک بود نه مه آن خار تا به گردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که خار چه بود کز بهر گلستانت</p></div>
<div class="m2"><p>در خون چو گل نشستم بسیار تا به گردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتا به عشق رستی از عالم کشاکش</p></div>
<div class="m2"><p>کان جا همی‌کشیدی بیگار تا به گردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رستی ز عالم اما از خویشتن نرستی</p></div>
<div class="m2"><p>عار است هستی تو وین عار تا به گردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عیاروار کم نه تو دام و حیله کم کن</p></div>
<div class="m2"><p>در دام خویش ماند عیار تا به گردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامی است دام دنیا کز وی شهان و شیران</p></div>
<div class="m2"><p>ماندند چون سگ اندر مردار تا به گردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دامی است طرفه‌تر زین کز وی فتاده بینی</p></div>
<div class="m2"><p>بی‌عقل تا به کعب و هشیار تا به گردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس کن ز گفتن آخر کان دم بود بریده</p></div>
<div class="m2"><p>کز تاسه نبود آخر گفتار تا به گردن</p></div></div>