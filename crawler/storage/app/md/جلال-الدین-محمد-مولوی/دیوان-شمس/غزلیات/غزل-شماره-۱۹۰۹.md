---
title: >-
    غزل شمارهٔ ۱۹۰۹
---
# غزل شمارهٔ ۱۹۰۹

<div class="b" id="bn1"><div class="m1"><p>دل معشوق سوزیده است بر من</p></div>
<div class="m2"><p>وزان سوزش جهان را سوخت خرمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزد آتش به جان بنده شمعی</p></div>
<div class="m2"><p>کز او شد موم جان سنگ و آهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پدید آمد از آن آتش به ناگه</p></div>
<div class="m2"><p>میان شب هزاران صبح روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کوی عشق آوازه درافتاد</p></div>
<div class="m2"><p>که شد در خانه دل شکل روزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه روزن کآفتاب نو برآمد</p></div>
<div class="m2"><p>که سایه نیست آن جا قدر سوزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن نوری که از لطفش برسته‌ست</p></div>
<div class="m2"><p>ز آتش گلبن و نسرین و سوسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن سو بازگرد ای یار بدخو</p></div>
<div class="m2"><p>بدین سو آ که این سوی است مؤمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سوی بی‌سوی جمله بهار است</p></div>
<div class="m2"><p>به هر سو غیر این سرمای بهمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شمس الدین جان آمد ز تبریز</p></div>
<div class="m2"><p>تو جان کندن همی‌خواهی همی‌کن</p></div></div>