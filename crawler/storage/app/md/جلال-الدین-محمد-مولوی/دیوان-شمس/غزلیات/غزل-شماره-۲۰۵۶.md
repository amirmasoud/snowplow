---
title: >-
    غزل شمارهٔ ۲۰۵۶
---
# غزل شمارهٔ ۲۰۵۶

<div class="b" id="bn1"><div class="m1"><p>خواجه غلط کرده‌ای در روش یار من</p></div>
<div class="m2"><p>صد چو تو هم گم شود در من و در کار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود هر گردنی لایق شمشیر عشق</p></div>
<div class="m2"><p>خون سگان کی خورد ضیغم خون خوار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلزم من کی کشد تخته هر کشتیی</p></div>
<div class="m2"><p>شوره تو کی چرد ز ابر گهربار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بمگردان چنین پوز مجنبان چنان</p></div>
<div class="m2"><p>چون تو خری کی رسد در جو انبار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه به خویش آ یکی چشم گشا اندکی</p></div>
<div class="m2"><p>گر چه نه بر پای توست اندک و بسیار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت که عاشق چرا مست شد و بی‌حیا</p></div>
<div class="m2"><p>باده حیا کی هلد خاصه ز خمار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنه گرگی شده هم دغل و مکر او</p></div>
<div class="m2"><p>دام وی از وی کند قانص عیار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر بازار او گرگ کهن کی خرند</p></div>
<div class="m2"><p>هر طرفی یوسفی زنده به بازار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو تو جغدی کجا باغ ارم را سزد</p></div>
<div class="m2"><p>بلبل جان هم نیافت راه به گلزار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مفخر تبریزیان شمس حق و دین بگو</p></div>
<div class="m2"><p>بلک صدای تو است این همه گفتار من</p></div></div>