---
title: >-
    غزل شمارهٔ ۱۲۶۴
---
# غزل شمارهٔ ۱۲۶۴

<div class="b" id="bn1"><div class="m1"><p>می‌گفت چشم شوخش با طره سیاهش</p></div>
<div class="m2"><p>من دم دهم فلان را تو درربا کلاهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعقوب را بگویم یوسف به قعر چاهست</p></div>
<div class="m2"><p>چون بر سر چه آید تو درفکن به چاهش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما شکل حاجیانیم جاسوس و رهزنانیم</p></div>
<div class="m2"><p>حاجی چو در ره آید ما خود زنیم راهش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما شاخ ارغوانیم در آب و می‌نماییم</p></div>
<div class="m2"><p>با نعل بازگونه چون ماه و چون سپاهش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روباه دید دنبه در سبزه زار و می‌گفت</p></div>
<div class="m2"><p>هرگز کی دید دنبه بی‌دام در گیاهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان گرگ از حریصی در دنبه چون نمک شد</p></div>
<div class="m2"><p>از دام بی‌خبر بد آن خاطر تباهش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابله چو اندرافتد گوید که بی‌گناهم</p></div>
<div class="m2"><p>بس نیست ای برادر آن ابلهی گناهش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابله کننده عشقست عشقی گزین تو باری</p></div>
<div class="m2"><p>کابله شدن بیرزد حسن و جمال و جاهش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای تو درد گیرد افسون جان بر او خوان</p></div>
<div class="m2"><p>آن پای گاو باشد کافسون اوست کاهش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلق تو درد گیرد همراه دم پذیرد</p></div>
<div class="m2"><p>خود حلق کی گشاید بی‌آه غصه کاهش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا پیشگاه عشقش چون باشد و چه باشد</p></div>
<div class="m2"><p>چون ما ز دست رفتیم از پای گاه جاهش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا چه جمال دارد آن نادره مطرز</p></div>
<div class="m2"><p>که سوخت جان ما را آن نقش کارگاهش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز اندیشه می‌گذارم تا خود چه حیله سازم</p></div>
<div class="m2"><p>با او که مکر و حیله تلقین کند الهش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن کس که گم کند ره با عقل بازگردد</p></div>
<div class="m2"><p>وان را که عقل گم شد از کی بود پناهش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نی ما از آن شاهیم ما عقل و جان نخواهیم</p></div>
<div class="m2"><p>چه عقل و بند و پندش چه جان و آه آهش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مستی فزود خامش تا نکته‌ای نرانی</p></div>
<div class="m2"><p>ای رفته لاابالی در خون نیکخواهش</p></div></div>