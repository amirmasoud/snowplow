---
title: >-
    غزل شمارهٔ ۲۳۱۳
---
# غزل شمارهٔ ۲۳۱۳

<div class="b" id="bn1"><div class="m1"><p>ای جان تو جانم را از خویش خبر کرده</p></div>
<div class="m2"><p>اندیشه تو هر دم در بنده اثر کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای هر چه بیندیشی در خاطر تو آید</p></div>
<div class="m2"><p>بر بنده همان لحظه آن چیز گذر کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شیوه و ناز تو مشغول شده جانم</p></div>
<div class="m2"><p>مکر تو به پنهانی خود کار دگر کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر یاد لب تو نی هر صبح بنالیده</p></div>
<div class="m2"><p>عشقت دهن نی را پرقند و شکر کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چهره چون ماهت وز قد و کمرگاهت</p></div>
<div class="m2"><p>چون ماه نو این جانم خود را چو قمر کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود را چو کمر کردم باشد به میان آیی</p></div>
<div class="m2"><p>ای چشم تو سوی من از خشم نظر کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خشم نظر کردی دل زیر و زبر کردی</p></div>
<div class="m2"><p>تا این دل آواره از خویش سفر کرده</p></div></div>