---
title: >-
    غزل شمارهٔ ۳۱۹۷
---
# غزل شمارهٔ ۳۱۹۷

<div class="b" id="bn1"><div class="m1"><p>سیدی ایم هو کی، خذیدی ایم هو کی</p></div>
<div class="m2"><p>ارنی وجهک ساعة، نقتدی ایم هو کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ردا اکرامکم، نرتدی ایم هو کی</p></div>
<div class="m2"><p>فی سناسیمائکم نهتدی، ایم هو کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بود از جام تو، بیخودی ایم هو کی</p></div>
<div class="m2"><p>در صبوح از نقل تو، نغتدی ایم هو کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو مه در شهرها، شاهدی ایم هو کی</p></div>
<div class="m2"><p>از همه بیندت، مقتدی ایم هو کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاضر و آواره را، مسندی ایم هو کی</p></div>
<div class="m2"><p>کعبه‌وار آفاق را، مسجدی ایم هو کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برد عشقت از دلم، زاهدی ایم هو کی</p></div>
<div class="m2"><p>اسکتوا ذاک‌الخیال، قایدی ایم هو کی</p></div></div>