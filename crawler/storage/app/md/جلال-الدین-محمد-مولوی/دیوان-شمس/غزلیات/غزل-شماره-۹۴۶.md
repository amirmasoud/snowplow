---
title: >-
    غزل شمارهٔ ۹۴۶
---
# غزل شمارهٔ ۹۴۶

<div class="b" id="bn1"><div class="m1"><p>میان باغ گل سرخ‌های و هو دارد</p></div>
<div class="m2"><p>که بو کنید دهان مرا چه بو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیاله‌ای به من آورد لاله که بخوری</p></div>
<div class="m2"><p>خورم چرا نخورم بنده هم گلو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلو چه حاجت می‌نوش بی‌گلو و دهان</p></div>
<div class="m2"><p>رحیق غیب که طعم سقا همو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو سال سال نشاطست و روز روز طرب</p></div>
<div class="m2"><p>خنک مرا و کسی را که عیش خو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا مقیم نباشد چو ما به مجلس گل</p></div>
<div class="m2"><p>کسی که ساقی باقی ماه رو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آفتاب جلالت که ذره ذره عشق</p></div>
<div class="m2"><p>نهان به زیر قبا ساغر و کدو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سؤال کردم از گل که بر که می‌خندی</p></div>
<div class="m2"><p>جواب داد بدان زشت کو دو شو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلام کور که او را دو خواجه می‌باید</p></div>
<div class="m2"><p>چو سگ همیشه مقام او میان کو دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سؤال کردم از خار کاین سلاح تو چیست</p></div>
<div class="m2"><p>جواب داد که گلزار صد عدو دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار بار چمن را بسوخت و بازآراست</p></div>
<div class="m2"><p>چه عشق دارد با ما چه جست و جو دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شمس مفخر تبریز پرس کاین از چیست</p></div>
<div class="m2"><p>وگر چه دفع دهد دم مخور که او دارد</p></div></div>