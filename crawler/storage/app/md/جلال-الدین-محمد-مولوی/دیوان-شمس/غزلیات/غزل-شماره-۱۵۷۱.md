---
title: >-
    غزل شمارهٔ ۱۵۷۱
---
# غزل شمارهٔ ۱۵۷۱

<div class="b" id="bn1"><div class="m1"><p>در عشق قدیم سال خوردیم</p></div>
<div class="m2"><p>وز گفت حسود برنگردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین دمدمه‌ها زنان بترسند</p></div>
<div class="m2"><p>بر ما تو مخوان که مرد مردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردانه کنیم کار مردان</p></div>
<div class="m2"><p>پنهان نکنیم آنچ کردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را تو به زرد و سرخ مفریب</p></div>
<div class="m2"><p>کز خنجر عشق روی زردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر درد هزار آفرین باد</p></div>
<div class="m2"><p>باقی بر ما که یار دردیم</p></div></div>