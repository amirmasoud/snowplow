---
title: >-
    غزل شمارهٔ ۲۵۶۶
---
# غزل شمارهٔ ۲۵۶۶

<div class="b" id="bn1"><div class="m1"><p>آورد طبیب جان یک طبله ره آوردی</p></div>
<div class="m2"><p>گر پیر خرف باشی تو خوب و جوان گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن را بدهد هستی جان را بدهد مستی</p></div>
<div class="m2"><p>از دل ببرد سستی وز رخ ببرد زردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن طبله عیسی بد میراث طبیبان شد</p></div>
<div class="m2"><p>تریاق در او یابی گر زهر اجل خوردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طالب آن طبله روی آر بدین قبله</p></div>
<div class="m2"><p>چون روی بدو آری مه روی جهان گردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حبیب است در او پنهان کان ناید در دندان</p></div>
<div class="m2"><p>نی تری و نی خشکی نی گرمی و نی سردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان حب کم از حبه آیی بر آن قبه</p></div>
<div class="m2"><p>کان مسکن عیسی شد و آن حبه بدان خردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد محرز و شد محرز از داد تو هر عاجز</p></div>
<div class="m2"><p>لاغر نشود هرگز آن را که تو پروردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم به طبیب جان امروز هزاران سان</p></div>
<div class="m2"><p>صدق قدمی باشد چون تو قدم افشردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جا نبرد چیزی آن را که تو جا دادی</p></div>
<div class="m2"><p>غم نسترد آن دل را کو را ز غم استردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خامش کن و دم درکش چون تجربه افتادت</p></div>
<div class="m2"><p>ترک گروان برگو تو زان گروان فردی</p></div></div>