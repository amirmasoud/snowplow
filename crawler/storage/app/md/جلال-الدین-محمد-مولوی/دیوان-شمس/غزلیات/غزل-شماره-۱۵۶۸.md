---
title: >-
    غزل شمارهٔ ۱۵۶۸
---
# غزل شمارهٔ ۱۵۶۸

<div class="b" id="bn1"><div class="m1"><p>ای جان لطیف و ای جهانم</p></div>
<div class="m2"><p>از خواب گرانت برجهانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌شرم و حیا کنم تقاضا</p></div>
<div class="m2"><p>دانی که غریم بی‌امانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بر دل تو غبار بینم</p></div>
<div class="m2"><p>از اشک خودش فرونشانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای گلبن جان برای مجلس</p></div>
<div class="m2"><p>بگرفته امت که گل فشانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک بوسه بده که اندر این راه</p></div>
<div class="m2"><p>من باج عقیق می ستانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسیار شب است کاندر این دشت</p></div>
<div class="m2"><p>من از پی باج راهبانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب نعره زنم چو پاسبانان</p></div>
<div class="m2"><p>چون طالب باج کاروانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همخانه گریخت از نفیرم</p></div>
<div class="m2"><p>همسایه گریست از فغانم</p></div></div>