---
title: >-
    غزل شمارهٔ ۶۸۴
---
# غزل شمارهٔ ۶۸۴

<div class="b" id="bn1"><div class="m1"><p>ز رویت دسته گل می‌توان کرد</p></div>
<div class="m2"><p>ز زلفت شاخ سنبل می‌توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قد پرخم من در ره عشق</p></div>
<div class="m2"><p>بر آب چشم من پل می‌توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اشک خون همچون اطلس من</p></div>
<div class="m2"><p>براق عشق را جل می‌توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر حلقه از آن زلفین پربند</p></div>
<div class="m2"><p>پر گردن کشان غل می‌توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو دریایی و من یک قطره ای جان</p></div>
<div class="m2"><p>ولیکن جزو را کل می‌توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم صدپاره شد هر پاره نالان</p></div>
<div class="m2"><p>که از هر پاره بلبل می‌توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو قاف قندی و من لام لب تلخ</p></div>
<div class="m2"><p>ز قاف و لام ما قل می‌توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا همشیره است اندیشه تو</p></div>
<div class="m2"><p>از این شیره بسی مل می‌توان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رهی دورست و جان من پیاده</p></div>
<div class="m2"><p>ولی دل را چو دلدل می‌توان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خمش کن زان که بی‌گفت زبانی</p></div>
<div class="m2"><p>جهان پربانگ و غلغل می‌توان کرد</p></div></div>