---
title: >-
    غزل شمارهٔ ۳۰۰۰
---
# غزل شمارهٔ ۳۰۰۰

<div class="b" id="bn1"><div class="m1"><p>ساقی بیار باده سغراق ده منی</p></div>
<div class="m2"><p>اندیشه را رها کن کاری است کردنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نقد جان مگوی که ایام بیننا</p></div>
<div class="m2"><p>گردن مخار خواجه که وامی است گردنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای آب زندگانی در تشنگان نگر</p></div>
<div class="m2"><p>بر دوست رحم آر به کوری دشمنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوشی است بند ما و به پیش تو هوش چیست</p></div>
<div class="m2"><p>گر برج خیبر است بخواهیش برکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر مقام هوش همه خوف و زلزله‌ست</p></div>
<div class="m2"><p>در بی‌هشی است عیش و مقامات ایمنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بزم بی‌هشی همه جان‌ها مجردند</p></div>
<div class="m2"><p>رقصان چو ذره‌ها خورشان نور و روشنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای آفتاب جان در و دیوار تن بسوز</p></div>
<div class="m2"><p>قانع نمی‌شویم بدین نور روزنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این قصه را رها کن ما سخت تشنه‌ایم</p></div>
<div class="m2"><p>تو ساقی کریمی و بی‌صرفه و غنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیهای عاشقان همه از بوی گلشنی است</p></div>
<div class="m2"><p>آگاه نیست کس که چه باغ و چه گلشنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خشک آر و می‌نگر ز چپ و راست اشک خون</p></div>
<div class="m2"><p>ای سنگ دل بگوی که تا چند تن زنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیهوده چند گویی خاموش کن بس است</p></div>
<div class="m2"><p>فرمان گفت نیست همان گیر که الکنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا شمس حق تبریز آرد گشایشی</p></div>
<div class="m2"><p>کاین ناطقه نماند در حرف معتنی</p></div></div>