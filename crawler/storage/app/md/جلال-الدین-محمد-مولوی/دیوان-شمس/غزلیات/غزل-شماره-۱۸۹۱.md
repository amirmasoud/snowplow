---
title: >-
    غزل شمارهٔ ۱۸۹۱
---
# غزل شمارهٔ ۱۸۹۱

<div class="b" id="bn1"><div class="m1"><p>ما دست تو را خواجه بخواهیم کشیدن</p></div>
<div class="m2"><p>وز نیک و بدت پاک بخواهیم بریدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند شب غفلت و مستیت دراز است</p></div>
<div class="m2"><p>ما بر همه چون صبح بخواهیم دمیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پرده ناموس و دغل چند گریزی</p></div>
<div class="m2"><p>نزدیک رسیده‌ست تو را پرده دریدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر میوه که در باغ جهان بود همه پخت</p></div>
<div class="m2"><p>ای غوره چون سنگ نخواهی تو پزیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحم آر بر این جان که طپان است در این دام</p></div>
<div class="m2"><p>نشنود مگر گوش تو آواز طپیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمی است تو را در دل و آن چشم به درد است</p></div>
<div class="m2"><p>پس چیست غم تو به جز آن چشم خلیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون می خلد آن چشم بجو دارو و درمان</p></div>
<div class="m2"><p>تا بازرهی از خلش و آب دویدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داروی دل و دیده نبوده‌ست و نباشد</p></div>
<div class="m2"><p>ای یوسف خوبان به جز از روی تو دیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هین مخلص این را تو بفرما به تمامی</p></div>
<div class="m2"><p>که گفت تو و قول تو مزد است شنیدن</p></div></div>