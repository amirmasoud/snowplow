---
title: >-
    غزل شمارهٔ ۱۶۶۰
---
# غزل شمارهٔ ۱۶۶۰

<div class="b" id="bn1"><div class="m1"><p>ای گزیده یار چونت یافتم</p></div>
<div class="m2"><p>ای دل و دلدار چونت یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می گریزی هر زمان از کار ما</p></div>
<div class="m2"><p>در میان کار چونت یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند بارم وعده کردی و نشد</p></div>
<div class="m2"><p>ای صنم این بار چونت یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زحمت اغیار آخر چند چند</p></div>
<div class="m2"><p>هین که بی‌اغیار چونت یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دریده پرده‌های عاشقان</p></div>
<div class="m2"><p>پرده را بردار چونت یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ز رویت گلستان‌ها شرمسار</p></div>
<div class="m2"><p>در گل و گلزار چونت یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل اندک نیست زخم چشم بد</p></div>
<div class="m2"><p>پس مگو بسیار چونت یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که در خوابت ندیده خسروان</p></div>
<div class="m2"><p>این عجب بیدار چونت یافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریزی که انوار از تو تافت</p></div>
<div class="m2"><p>اندر آن انوار چونت یافتم</p></div></div>