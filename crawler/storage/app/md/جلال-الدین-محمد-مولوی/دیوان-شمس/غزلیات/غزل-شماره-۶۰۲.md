---
title: >-
    غزل شمارهٔ ۶۰۲
---
# غزل شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>آن کس که تو را دارد از عیش چه کم دارد</p></div>
<div class="m2"><p>وان کس که تو را بیند ای ماه چه غم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رنگ بلور تو شیرین شده جور تو</p></div>
<div class="m2"><p>هر چند که جور تو بس تند قدم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نازش حور از تو وی تابش نور از تو</p></div>
<div class="m2"><p>ای آنک دو صد چون مه شاگرد و حشم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور خود حشمش نبود خورشید بود تنها</p></div>
<div class="m2"><p>آخر حشم حسنش صد طبل و علم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس عاشق آشفته آسوده و خوش خفته</p></div>
<div class="m2"><p>در سایه آن زلفی کو حلقه و خم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم به نگار من کز جور مرا مشکن</p></div>
<div class="m2"><p>گفتا به صدف مانی کو در به شکم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نشکنی ای شیدا آن در نشود پیدا</p></div>
<div class="m2"><p>آن در بت من باشد یا شکل بتم دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس الحق تبریزی بر لوح چو پیدا شد</p></div>
<div class="m2"><p>والله که بسی منت بر لوح و قلم دارد</p></div></div>