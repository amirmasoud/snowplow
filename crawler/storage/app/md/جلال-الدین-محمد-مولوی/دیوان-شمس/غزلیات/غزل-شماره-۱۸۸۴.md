---
title: >-
    غزل شمارهٔ ۱۸۸۴
---
# غزل شمارهٔ ۱۸۸۴

<div class="b" id="bn1"><div class="m1"><p>آن ساعد سیمین را در گردن ما افکن</p></div>
<div class="m2"><p>بر سینه ما بنشین ای جان منت مسکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمست شدم ای جان وز دست شدم ای جان</p></div>
<div class="m2"><p>ای دوست خمارم را از لعل لبت بشکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ساقی هر نادر این می ز چه خم داری</p></div>
<div class="m2"><p>من بنده ظلم تو از بیخ و بنم برکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم پرده من می در هم خون دلم می خور</p></div>
<div class="m2"><p>آخر نه تویی با من شاباش زهی ای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دوست ستم نبود بر مست قلم نبود</p></div>
<div class="m2"><p>جز عفو و کرم نبود بر مست چنین مسکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از معدن خویش ای جان بخرام در این میدان</p></div>
<div class="m2"><p>رونق نبود زر را تا باشد در معدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با لعل چو تو کانی غمگین نشود جانی</p></div>
<div class="m2"><p>در گور و کفن ناید تا باشد جان در تن</p></div></div>