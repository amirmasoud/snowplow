---
title: >-
    غزل شمارهٔ ۱۴۸۷
---
# غزل شمارهٔ ۱۴۸۷

<div class="b" id="bn1"><div class="m1"><p>امروز چنانم که خر از بار ندانم</p></div>
<div class="m2"><p>امروز چنانم که گل از خار ندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز مرا یار بدان حال ز سر برد</p></div>
<div class="m2"><p>با یار چنانم که خود از یار ندانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی باده مرا برد ز مستی به در یار</p></div>
<div class="m2"><p>امروز چه چاره که در از دار ندانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خوف و رجا پار دو پر داشت دل من</p></div>
<div class="m2"><p>امروز چنان شد که پر از پار ندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چهره زار چو زرم بود شکایت</p></div>
<div class="m2"><p>رستم ز شکایت چو زر از زار ندانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کار جهان کور بود مردم عاشق</p></div>
<div class="m2"><p>اما نه چو من خود که کر از کار ندانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جولاهه تردامن ما تار بدرید</p></div>
<div class="m2"><p>می گفت ز مستی که تر از تار ندانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون چنگم از زمزمه خود خبرم نیست</p></div>
<div class="m2"><p>اسرار همی‌گویم و اسرار ندانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مانند ترازو و گزم من که به بازار</p></div>
<div class="m2"><p>بازار همی‌سازم و بازار ندانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در اصبع عشقم چو قلم بیخود و مضطر</p></div>
<div class="m2"><p>طومار نویسم من و طومار ندانم</p></div></div>