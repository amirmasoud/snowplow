---
title: >-
    غزل شمارهٔ ۱۴۴
---
# غزل شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>عقل دریابد تو را یا عشق یا جان صفا</p></div>
<div class="m2"><p>لوح محفوظت شناسد یا ملایک بر سما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جبرئیلت خواب بیند یا مسیحا یا کلیم</p></div>
<div class="m2"><p>چرخ شاید جای تو یا سدره‌ها یا منتها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طور موسی بارها خون گشت در سودای عشق</p></div>
<div class="m2"><p>کز خداوند شمس دین افتد به طور اندر صدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر در پر بافته رشک احد گرد رخش</p></div>
<div class="m2"><p>جان احمد نعره زن از شوق او واشوقنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیرت و رشک خدا آتش زند اندر دو کون</p></div>
<div class="m2"><p>گر سر مویی ز حسنش بی‌حجاب آید به ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ورای صد هزاران پرده حسنش تافته</p></div>
<div class="m2"><p>نعره‌ها در جان فتاده مرحبا شه مرحبا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجده تبریز را خم درشده سرو سهی</p></div>
<div class="m2"><p>غاشیه تبریز را برداشته جان سها</p></div></div>