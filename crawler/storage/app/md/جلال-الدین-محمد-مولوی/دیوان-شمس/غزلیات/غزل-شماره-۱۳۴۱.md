---
title: >-
    غزل شمارهٔ ۱۳۴۱
---
# غزل شمارهٔ ۱۳۴۱

<div class="b" id="bn1"><div class="m1"><p>امروز بحمدالله از دی بترست این دل</p></div>
<div class="m2"><p>امروز در این سودا رنگی دگرست این دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زیر درخت گل دی باده همی‌خورد او</p></div>
<div class="m2"><p>از خوردن آن باده زیر و زبرست این دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس که نی عشقت نالید در این پرده</p></div>
<div class="m2"><p>از ذوق نی عشقت همچون شکرست این دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بند کمرت گشتم ای شهره قبای من</p></div>
<div class="m2"><p>تا بسته بگرد تو همچون کمرست این دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پرورش آبت ای بحر حلاوت‌ها</p></div>
<div class="m2"><p>همچون صدفست این تن همچون گهرست این دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خانه هر مؤمن از عشق تو ویران شد</p></div>
<div class="m2"><p>هر لحظه در این شورش بر بام و درست این دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس الحق تبریزی تابنده چو خورشیدست</p></div>
<div class="m2"><p>وز تابش خورشیدش همچون سحرست این دل</p></div></div>