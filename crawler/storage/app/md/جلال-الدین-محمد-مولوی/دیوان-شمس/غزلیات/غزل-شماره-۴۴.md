---
title: >-
    غزل شمارهٔ ۴۴
---
# غزل شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>در دو جهان لطیف و خوش همچو امیر ما کجا</p></div>
<div class="m2"><p>ابروی او گره نشد گر چه که دید صد خطا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم گشا و رو نگر جرم بیار و خو نگر</p></div>
<div class="m2"><p>خوی چو آب جو نگر جمله طراوت و صفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز سلام گرم او آب شدم ز شرم او</p></div>
<div class="m2"><p>وز سخنان نرم او آب شوند سنگ‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر به پیش او ببر تا کندش به از شکر</p></div>
<div class="m2"><p>قهر به پیش او بنه تا کندش همه رضا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب حیات او ببین هیچ مترس از اجل</p></div>
<div class="m2"><p>در دو در رضای او هیچ ملرز از قضا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سجده کنی به پیش او عزت مسجدت دهد</p></div>
<div class="m2"><p>ای که تو خوار گشته‌ای زیر قدم چو بوریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواندم امیر عشق را فهم بدین شود تو را</p></div>
<div class="m2"><p>چونک تو رهن صورتی صورت توست ره نما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تو دل ار سفر کند با تپش جگر کند</p></div>
<div class="m2"><p>بر سر پاست منتظر تا تو بگوییش بیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل چو کبوتری اگر می‌بپرد ز بام تو</p></div>
<div class="m2"><p>هست خیال بام تو قبله جانش در هوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بام و هوا تویی و بس نیست روی به جز هوس</p></div>
<div class="m2"><p>آب حیات جان تویی صورت‌ها همه سقا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دور مرو سفر مجو پیش تو است ماه تو</p></div>
<div class="m2"><p>نعره مزن که زیر لب می‌شنود ز تو دعا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌شنود دعای تو می‌دهدت جواب او</p></div>
<div class="m2"><p>کای کر من کری بهل گوش تمام برگشا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نه حدیث او بدی جان تو آه کی زدی</p></div>
<div class="m2"><p>آه بزن که آه تو راه کند سوی خدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ زنان بدان خوشم کآب به بوستان کشم</p></div>
<div class="m2"><p>میوه رسد ز آب جان شوره و سنگ و ریگ را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باغ چو زرد و خشک شد تا بخورد ز آب جان</p></div>
<div class="m2"><p>شاخ شکسته را بگو آب خور و بیازما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شب برود بیا به گه تا شنوی حدیث شه</p></div>
<div class="m2"><p>شب همه شب مثال مه تا به سحر مشین ز پا</p></div></div>