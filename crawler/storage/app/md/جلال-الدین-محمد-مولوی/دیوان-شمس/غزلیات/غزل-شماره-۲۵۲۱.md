---
title: >-
    غزل شمارهٔ ۲۵۲۱
---
# غزل شمارهٔ ۲۵۲۱

<div class="b" id="bn1"><div class="m1"><p>اگر یار مرا از من غم و سودا نبایستی</p></div>
<div class="m2"><p>مرا صد در دکان بودی مرا صد عقل و رایستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر کشتی رخت من نگشتی غرقه دریا</p></div>
<div class="m2"><p>فلک با جمله گوهرهاش پیش من گدایستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر از راه اندیشه بدین مستان رهی بودی</p></div>
<div class="m2"><p>خرد در کار عشق ما چرا بی‌دست و پایستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر خسرو از این شیرین یکی انگشت لیسیدی</p></div>
<div class="m2"><p>چرا قید کله بودی چرا قید قبایستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیب عشق اگر دادی به جالینوس یک معجون</p></div>
<div class="m2"><p>چرا بهر حشایش او بدین حد ژاژخایستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مستی تجلی گر سر هر کوه را بودی</p></div>
<div class="m2"><p>مثال ابر هر کوهی معلق بر هوایستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر غولان اندیشه همه یک گوشه رفتندی</p></div>
<div class="m2"><p>بیابان‌های بی‌مایه پر از نوش و نوایستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر در عهدهٔ عهدی وفایی آمدی از ما</p></div>
<div class="m2"><p>دلارام جهان پرور بر آن عهد و وفایستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر این گندم هستی سبکتر آرد می‌گشتی</p></div>
<div class="m2"><p>متاع هستی خلقان برون زین آسیایستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر خضری دراشکستی به ناگه کشتی تن را</p></div>
<div class="m2"><p>در این دریا همه جان‌ها چو ماهی آشنایستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ستایش می‌کند شاعر ملک را و اگر او را</p></div>
<div class="m2"><p>ز خویش خود خبر بودی ملک شاعر ستایستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر جبار بربستی شکسته ساق و دستش را</p></div>
<div class="m2"><p>نه در جبر و قدر بودی نه در خوف و رجایستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آن اشکستگی او گر بدیدی ذوق اشکستن</p></div>
<div class="m2"><p>نه از مرهم بپرسیدی نه جویای دوایستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشان از جان تو این داری که می‌باید نمی‌باید</p></div>
<div class="m2"><p>نمی‌باید شدی باید اگر او را ببایستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر از خرمن خدمت تو ده سالار منبل را</p></div>
<div class="m2"><p>یکی برگ کهی بودی گنه بر کهربایستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فراز آسمان صوفی همی‌رقصید و می‌گفت این</p></div>
<div class="m2"><p>زمین کل آسمان گشتی گرش چون من صفایستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خمش کن شعر می‌ماند و می‌پرند معنی‌ها</p></div>
<div class="m2"><p>پر از معنی بدی عالم اگر معنی بپایستی</p></div></div>