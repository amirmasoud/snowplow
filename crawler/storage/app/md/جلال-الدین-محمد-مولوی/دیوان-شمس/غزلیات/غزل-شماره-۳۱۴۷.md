---
title: >-
    غزل شمارهٔ ۳۱۴۷
---
# غزل شمارهٔ ۳۱۴۷

<div class="b" id="bn1"><div class="m1"><p>ساقیا ساقیا روا داری</p></div>
<div class="m2"><p>که رود روز ما به هشیاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بریزی تو نقل‌ها در پیش</p></div>
<div class="m2"><p>عقل‌ها را ز پیش برداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عوض باده نکته می‌گویی</p></div>
<div class="m2"><p>تا بری وقت ما به طراری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد دل را اگر نمی‌بینی</p></div>
<div class="m2"><p>بشنو از چنگ ناله و زاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله نای و چنگ حال دلست</p></div>
<div class="m2"><p>حال دل را تو بین که دلداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست بر حرف بی‌دلی چه نهی</p></div>
<div class="m2"><p>حرف را در میان چه می‌آری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوق گردن تویی و حلقه گوش</p></div>
<div class="m2"><p>گردن و گوش را چه می‌خاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفته را دانه‌های دام مساز</p></div>
<div class="m2"><p>که ز گفتست این گرفتاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه کلیدست گفت و گه قفلست</p></div>
<div class="m2"><p>گاه از او روشنیم و گه تاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت بادست گر در او بوییست</p></div>
<div class="m2"><p>هدیه تو بود که گلزاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت جامست گر بر او نوریست</p></div>
<div class="m2"><p>از رخ تو بود که انواری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشک بربند کوزه‌ها پر شد</p></div>
<div class="m2"><p>مشک هم می‌درد ز بسیاری</p></div></div>