---
title: >-
    غزل شمارهٔ ۶۷۵
---
# غزل شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>هر آن دل‌ها که بی‌تو شاد باشد</p></div>
<div class="m2"><p>چو خاشاکی میان باد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مرغ خانگی کز اوج پرد</p></div>
<div class="m2"><p>چو شاگردی که بی‌استاد باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه ماند صورتی کز خود تراشی</p></div>
<div class="m2"><p>بدان شاهی که حوری زاد باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه ماند هیبت شمشیر چوبین</p></div>
<div class="m2"><p>به شمشیری که از پولاد باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو عهدی کرده چون روح بودی</p></div>
<div class="m2"><p>ولیکن کی تو را آن یاد باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر منکر شوی من صبر دارم</p></div>
<div class="m2"><p>بدان روزی که روز داد باشد</p></div></div>