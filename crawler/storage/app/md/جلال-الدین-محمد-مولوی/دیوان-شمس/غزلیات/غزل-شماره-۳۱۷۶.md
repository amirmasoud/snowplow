---
title: >-
    غزل شمارهٔ ۳۱۷۶
---
# غزل شمارهٔ ۳۱۷۶

<div class="b" id="bn1"><div class="m1"><p>کردم با کان گهر آشتی</p></div>
<div class="m2"><p>کردم با قرص قمر آشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمرهٔ سرکه ز شکر صلح خواست</p></div>
<div class="m2"><p>شکر که پذرفت شکر آشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشتی و جنگ ز جذبهٔ حق است</p></div>
<div class="m2"><p>نیست زدم، هست ز سر آشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت مسیحا به فلک ناگهان</p></div>
<div class="m2"><p>با ملکان کرد بشر آشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای فلک لطف، مسیح توم</p></div>
<div class="m2"><p>گر بکنی بار دگر آشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جذبهٔ او داد عدم را وجود</p></div>
<div class="m2"><p>کرده بدان پیه نظر آشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه مرا میل چو در آشتیست</p></div>
<div class="m2"><p>کرد در افلاک اثر آشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشت فلک دایهٔ این خاکدان</p></div>
<div class="m2"><p>ثور و اسد آمد در آشتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صلح درآ، این قدر آخر بدانک</p></div>
<div class="m2"><p>کرد کنون جبر و قدر آشتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس کن کین صبح مرا، دایمست</p></div>
<div class="m2"><p>نیست مرا بهر سپر آشتی</p></div></div>