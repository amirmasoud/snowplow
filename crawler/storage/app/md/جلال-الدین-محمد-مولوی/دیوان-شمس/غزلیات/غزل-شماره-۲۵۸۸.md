---
title: >-
    غزل شمارهٔ ۲۵۸۸
---
# غزل شمارهٔ ۲۵۸۸

<div class="b" id="bn1"><div class="m1"><p>ای یار غلط کردی با یار دگر رفتی</p></div>
<div class="m2"><p>از کار خود افتادی در کار دگر رفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بار ببخشودم بر تو به تو بنمودم</p></div>
<div class="m2"><p>ای خویش پسندیده هین بار دگر رفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد بار فسون کردم خار از تو برون کردم</p></div>
<div class="m2"><p>گلزار ندانستی در خار دگر رفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که تویی ماهی با مار چه همراهی</p></div>
<div class="m2"><p>ای حال غلط کرده با مار دگر رفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانند مکوک کژ اندر کف جولاهه</p></div>
<div class="m2"><p>صد تار بریدی تو در تار دگر رفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که تو را یارا در غار نمی‌بینم</p></div>
<div class="m2"><p>آن یار در آن غار است تو غار دگر رفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کم نشود سنگت چون بد نشود رنگت</p></div>
<div class="m2"><p>بازار مرا دیده بازار دگر رفتی</p></div></div>