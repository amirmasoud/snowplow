---
title: >-
    غزل شمارهٔ ۲۹۹۷
---
# غزل شمارهٔ ۲۹۹۷

<div class="b" id="bn1"><div class="m1"><p>ای آسمان که بر سر ما چرخ می‌زنی</p></div>
<div class="m2"><p>در عشق آفتاب تو همخرقه منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله که عاشقی و بگویم نشان عشق</p></div>
<div class="m2"><p>بیرون و اندرون همه سرسبز و روشنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بحر تر نگردی و ز خاک فارغی</p></div>
<div class="m2"><p>از آتشش نسوزی و ز باد ایمنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای چرخ آسیا ز چه آب است گردشت</p></div>
<div class="m2"><p>آخر یکی بگو که چه دولاب آهنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گردشی کنار زمین چون ارم کنی</p></div>
<div class="m2"><p>وز گردشی دگر چه درختان که برکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمعی است آفتاب و تو پروانه‌ای به فعل</p></div>
<div class="m2"><p>پروانه وار گرد چنین شمع می‌تنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پوشیده‌ای چو حاج تو احرام نیلگون</p></div>
<div class="m2"><p>چون حاج گرد کعبه طوافی همی‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حق گفت ایمن است هر آن کو به حج رسید</p></div>
<div class="m2"><p>ای چرخ حق گزار ز آفات ایمنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله بهانه‌هاست که عشق است هر چه هست</p></div>
<div class="m2"><p>خانه خداست عشق و تو در خانه ساکنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین بیش می‌نگویم و امکان گفت نیست</p></div>
<div class="m2"><p>والله چه نکته‌هاست در این سینه گفتنی</p></div></div>