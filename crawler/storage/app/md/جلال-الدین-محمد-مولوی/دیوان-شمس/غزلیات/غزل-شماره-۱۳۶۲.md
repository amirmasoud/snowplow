---
title: >-
    غزل شمارهٔ ۱۳۶۲
---
# غزل شمارهٔ ۱۳۶۲

<div class="b" id="bn1"><div class="m1"><p>لجکنن اغلن هی بزه کلکل</p></div>
<div class="m2"><p>دغدن دغدا هی کزه کلکل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آی بکی سنسن کن بکی سنسن</p></div>
<div class="m2"><p>بی‌مزه کلمه بامزه کلکل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذ لحبی من حرکاتی</p></div>
<div class="m2"><p>ارسل کنزا للصدقات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلص روحی من هفواتی</p></div>
<div class="m2"><p>اعتق قلبی من شبکاتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتم آن جا لنگان لنگان</p></div>
<div class="m2"><p>شربت خوردم پنگان پنگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدم آن جا قومی شنگان</p></div>
<div class="m2"><p>گشته ز ساغر خیره و دنگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت عشقی صاحب مخزن</p></div>
<div class="m2"><p>شوخ جهانی رندی و رهزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش جان را سنگی و آهن</p></div>
<div class="m2"><p>هر که نه عاشق ریشش برکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا رحمونا منه صبونا</p></div>
<div class="m2"><p>یا رهبونا عز علینا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صدر صدور جاء الینا</p></div>
<div class="m2"><p>بدر بدور بات لدینا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دنب خری تو ای خر ملعون</p></div>
<div class="m2"><p>نی کم گردی نی شوی افزون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای دل و جانم از کژی تو</p></div>
<div class="m2"><p>وز فن و مکرت خسته و پرخون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لاح صباحی طیب حالی</p></div>
<div class="m2"><p>جاء ربیعی هب شمالی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خصب غصنی ماء زلالی</p></div>
<div class="m2"><p>اسکر قلبی خمر وصال</p></div></div>