---
title: >-
    غزل شمارهٔ ۲۳۰۷
---
# غزل شمارهٔ ۲۳۰۷

<div class="b" id="bn1"><div class="m1"><p>بربند دهان از نان کآمد شکر روزه</p></div>
<div class="m2"><p>دیدی هنر خوردن بنگر هنر روزه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شاه دو صد کشور تاجیت نهد بر سر</p></div>
<div class="m2"><p>بربند میان زوتر کآمد کمر روزه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین عالم چون سجین برپر سوی علیین</p></div>
<div class="m2"><p>بستان نظر حق بین زود از نظر روزه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای نقره باحرمت در کوره این مدت</p></div>
<div class="m2"><p>آتش کندت خدمت اندر شرر روزه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزه نم زمزم شد در عیسی مریم شد</p></div>
<div class="m2"><p>بر طارم چارم شد او در سفر روزه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو پر زدن مرغان کو پر ملک ای جان</p></div>
<div class="m2"><p>این هست پر چینه و آن هست پر روزه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر روزه ضرر دارد صد گونه هنر دارد</p></div>
<div class="m2"><p>سودای دگر دارد سودای سر روزه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این روزه در این چادر پنهان شده چون دلبر</p></div>
<div class="m2"><p>از چادر او بگذر واجو خبر روزه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باریک کند گردن ایمن کند از مردن</p></div>
<div class="m2"><p>تخمه اثر خوردن مستی اثر روزه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سی روز در این دریا پا سر کنی و سر پا</p></div>
<div class="m2"><p>تا دررسی ای مولا اندر گهر روزه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیطان همه تدبیرش و آن حیله و تزویرش</p></div>
<div class="m2"><p>بشکست همه تیرش پیش سپر روزه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روزه کر و فر خود خوشتر ز تو برگوید</p></div>
<div class="m2"><p>دربند در گفتن بگشای در روزه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس الحق تبریزی هم صبری و پرهیزی</p></div>
<div class="m2"><p>هم عید شکرریزی هم کر و فر روزه</p></div></div>