---
title: >-
    غزل شمارهٔ ۱۵۳۱
---
# غزل شمارهٔ ۱۵۳۱

<div class="b" id="bn1"><div class="m1"><p>بیا کامروز شه را ما شکاریم</p></div>
<div class="m2"><p>سر خویش و سر عالم نداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا کامروز چون موسی عمران</p></div>
<div class="m2"><p>به مردی گرد از دریا برآریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب چون عصا افتاده بودیم</p></div>
<div class="m2"><p>چو روز آمد چو ثعبان بی‌قراریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گرد سینه خود طوف کردیم</p></div>
<div class="m2"><p>ید بیضا ز جیب جان برآریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان قدرت که ماری شد عصایی</p></div>
<div class="m2"><p>به هر شب چون عصا و روز ماریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پی فرعون سرکش اژدهاییم</p></div>
<div class="m2"><p>پی موسی عصا و بردباریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به همت خون نمرودان بریزیم</p></div>
<div class="m2"><p>تو این منگر که چون پشه نزاریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برافزاییم بر شیران و پیلان</p></div>
<div class="m2"><p>اگر چه در کف آن شیر زاریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه همچو اشتر کژنهادیم</p></div>
<div class="m2"><p>چو اشتر سوی کعبه راهواریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به اقبال دوروزه دل نبندیم</p></div>
<div class="m2"><p>که در اقبال باقی کامکاریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خورشید و قمر نزدیک و دوریم</p></div>
<div class="m2"><p>چو عشق و دل نهان و آشکاریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برای عشق خون آشام خون خوار</p></div>
<div class="m2"><p>سگانش را چو خون اندر تغاریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو ماهی وقت خاموشی خموشیم</p></div>
<div class="m2"><p>به وقت گفت ماه بی‌غباریم</p></div></div>