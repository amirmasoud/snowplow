---
title: >-
    غزل شمارهٔ ۲۳۱
---
# غزل شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>سبکتری تو از آن دم که می‌رسد ز صبا</p></div>
<div class="m2"><p>ز دم زدن نشود سیر و مانده کس جانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دم زدن کی شود مانده یا کی سیر شود</p></div>
<div class="m2"><p>تو آن دمی که خدا گفت یحیی الموتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهان گور شود باز و لقمه ایش کند</p></div>
<div class="m2"><p>چو بسته گشت دهان تن از دم احیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمم فزون ده تا خیک من شود پرباد</p></div>
<div class="m2"><p>که تا شوم ز دم تو سوار بر دریا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباد روزی کاندر جهان تو درندمی</p></div>
<div class="m2"><p>که یک گیاه نروید ز جمله صحرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروکش این دم زیرا تو را دمی دگر است</p></div>
<div class="m2"><p>چو بسکلد ز لب این باد آن بود برجا</p></div></div>