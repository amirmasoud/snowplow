---
title: >-
    غزل شمارهٔ ۲۵۲۸
---
# غزل شمارهٔ ۲۵۲۸

<div class="b" id="bn1"><div class="m1"><p>مروت نیست در سرها که اندازند دستاری</p></div>
<div class="m2"><p>کجا گیرد نظام ای جان به صرفه خشک بازاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رها کن گرگ خونی را که رو نارد بدان صیدی</p></div>
<div class="m2"><p>رها کن صرفه جویی را که برناید بدین کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه باشد زر چه باشد جان چه باشد گوهر و مرجان</p></div>
<div class="m2"><p>چو نبود خرج سودایی فدای خوبی یاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بخل ار طوق زر دارم مرا غلی بود غلی</p></div>
<div class="m2"><p>وگر خلخال زر دارم مرا خاری بود خاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو ای شاخ بی‌میوه تهی می‌گرد چون چرخی</p></div>
<div class="m2"><p>شدستی پاسبان زر هلا می‌پیچ چون ماری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو زر سرخ می‌گویش که او زرد است و رنجوری</p></div>
<div class="m2"><p>تو خواجه شهر می‌خوانش که او را نیست شلواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا از بهر همدردان نبازم سیم چون مردان</p></div>
<div class="m2"><p>چرا چون شربت شافی نباشم نوش بیماری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتانم بد کم از چنگی حریف هر دل تنگی</p></div>
<div class="m2"><p>غذای گوش‌ها گشته به هر زخمی و هر تاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نتانم بد کم از باده ز ینبوع طرب زاده</p></div>
<div class="m2"><p>صلای عیش می‌گوید به هر مخمور و خماری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرم آموز تو یارا ز سنگ مرمر و خارا</p></div>
<div class="m2"><p>که می‌جوشد ز هر عرقش عطابخشی و ایثاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چگونه میر و سرهنگی که ننگ صخره و سنگی</p></div>
<div class="m2"><p>چگونه شیر حق باشد اسیر نفس سگساری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمش کردم که رب دین نهان‌ها را کند تعیین</p></div>
<div class="m2"><p>نماید شاخ زشتش را وگر چه هست ستاری</p></div></div>