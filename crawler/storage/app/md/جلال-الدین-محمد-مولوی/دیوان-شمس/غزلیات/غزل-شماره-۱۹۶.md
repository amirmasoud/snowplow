---
title: >-
    غزل شمارهٔ ۱۹۶
---
# غزل شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>در جنبش اندرآور زلف عبرفشان را</p></div>
<div class="m2"><p>در رقص اندرآور جان‌های صوفیان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید و ماه و اختر رقصان بگرد چنبر</p></div>
<div class="m2"><p>ما در میان رقصیم رقصان کن آن میان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطف تو مطربانه از کمترین ترانه</p></div>
<div class="m2"><p>در چرخ اندرآرد صوفی آسمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد بهار پویان آید ترانه گویان</p></div>
<div class="m2"><p>خندان کند جهان را خیزان کند خزان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس مار یار گردد گل جفت خار گردد</p></div>
<div class="m2"><p>وقت نثار گردد مر شاه بوستان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دم ز باغ بویی آید چو پیک سویی</p></div>
<div class="m2"><p>یعنی که الصلا زن امروز دوستان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سر خود روان شد بستان و با تو گوید</p></div>
<div class="m2"><p>در سر خود روان شو تا جان رسد روان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا غنچه برگشاید با سرو سر سوسن</p></div>
<div class="m2"><p>لاله بشارت آرد مر بید و ارغوان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا سر هر نهالی از قعر بر سر آید</p></div>
<div class="m2"><p>معراجیان نهاده در باغ نردبان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرغان و عندلیبان بر شاخه‌ها نشسته</p></div>
<div class="m2"><p>چون بر خزینه باشد ادرار پاسبان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این برگ چون زبان‌ها وین میوه‌ها چو دل‌ها</p></div>
<div class="m2"><p>دل‌ها چو رو نماید قیمت دهد زبان را</p></div></div>