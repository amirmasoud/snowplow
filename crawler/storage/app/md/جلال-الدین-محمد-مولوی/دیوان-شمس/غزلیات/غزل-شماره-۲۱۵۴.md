---
title: >-
    غزل شمارهٔ ۲۱۵۴
---
# غزل شمارهٔ ۲۱۵۴

<div class="b" id="bn1"><div class="m1"><p>هین کژ و راست می‌روی باز چه خورده‌ای بگو</p></div>
<div class="m2"><p>مست و خراب می‌روی خانه به خانه کو به کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کی حریف بوده‌ای بوسه ز کی ربوده‌ای</p></div>
<div class="m2"><p>زلف که را گشوده‌ای حلقه به حلقه مو به مو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی تو حریف کی کنی ای همه چشم و روشنی</p></div>
<div class="m2"><p>خفیه روی چو ماهیان حوض به حوض جو به جو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست بگو به جان تو ای دل و جانم آن تو</p></div>
<div class="m2"><p>ای دل همچو شیشه‌ام خورده میت کدو کدو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست بگو نهان مکن پشت به عاشقان مکن</p></div>
<div class="m2"><p>چشمه کجاست تا که من آب کشم سبو سبو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در طلبم خیال تو دوش میان انجمن</p></div>
<div class="m2"><p>می‌نشناخت بنده را می‌نگریست رو به رو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بشناخت بنده را بنده کژرونده را</p></div>
<div class="m2"><p>گفت بیا به خانه هی چند روی تو سو به سو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر تو رفت در سفر با بد و نیک و خیر و شر</p></div>
<div class="m2"><p>همچو زنان خیره سر حجره به حجره شو به شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش ای رسول جان ای سبب نزول جان</p></div>
<div class="m2"><p>ز آنک تو خورده‌ای بده چند عتاب و گفت و گو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت شراره‌ای از آن گر ببری سوی دهان</p></div>
<div class="m2"><p>حلق و دهان بسوزدت بانگ زنی گلو گلو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لقمه هر خورنده را درخور او دهد خدا</p></div>
<div class="m2"><p>آنچ گلو بگیردت حرص مکن مجو مجو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم کو شراب جان ای دل و جان فدای آن</p></div>
<div class="m2"><p>من نه‌ام از شتردلان تا برمم به های و هو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حلق و گلوبریده با کو برمد از این ابا</p></div>
<div class="m2"><p>هر کی بلنگد او از این هست مرا عدو عدو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست کز آن تهی بود گر چه شهنشهی بود</p></div>
<div class="m2"><p>دست بریده‌ای بود مانده به دیر بر سمو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خامش باش و معتمد محرم راز نیک و بد</p></div>
<div class="m2"><p>آنک نیازمودیش راز مگو به پیش او</p></div></div>