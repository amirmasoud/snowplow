---
title: >-
    غزل شمارهٔ ۲۲۸
---
# غزل شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>بیار آن که قرین را سوی قرین کشدا</p></div>
<div class="m2"><p>فرشته را ز فلک جانب زمین کشدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر شبی چو محمد به جانب معراج</p></div>
<div class="m2"><p>براق عشق ابد را به زیر زین کشدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیش روح نشین زان که هر نشست تو را</p></div>
<div class="m2"><p>به خلق و خوی و صفت‌های همنشین کشدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب عشق ابد را که ساقیش روح است</p></div>
<div class="m2"><p>نگیرد و نکشد ور کشد چنین کشدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو بدزد ز پروانه خوی جانبازی</p></div>
<div class="m2"><p>که آن تو را به سوی نور شمع دین کشدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسید وحی خدایی که گوش تیز کنید</p></div>
<div class="m2"><p>که گوش تیز به چشم خدای بین کشدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال دوست تو را مژده وصال دهد</p></div>
<div class="m2"><p>که آن خیال و گمان جانب یقین کشدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این چهی تو چو یوسف خیال دوست رسن</p></div>
<div class="m2"><p>رسن تو را به فلک‌های برترین کشدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روز وصل اگر عقل ماندت گوید</p></div>
<div class="m2"><p>نگفتمت که چنان کن که آن به این کشدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجه بجه ز جهان همچو آهوان از شیر</p></div>
<div class="m2"><p>گرفتمش همه کان است کان به کین کشدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به راستی برسد جان بر آستان وصال</p></div>
<div class="m2"><p>اگر کژی به حریر و قز کژین کشدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکش تو خار جفاها از آن که خارکشی</p></div>
<div class="m2"><p>به سبزه و گل و ریحان و یاسمین کشدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنوش لعنت و دشنام دشمنان پی دوست</p></div>
<div class="m2"><p>که آن به لطف و ثناها و آفرین کشدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دهان ببند و امین باش در سخن داری</p></div>
<div class="m2"><p>که شه کلید خزینه بر امین کشدا</p></div></div>