---
title: >-
    غزل شمارهٔ ۱۲۹۹
---
# غزل شمارهٔ ۱۲۹۹

<div class="b" id="bn1"><div class="m1"><p>گویند شاه عشق ندارد وفا دروغ</p></div>
<div class="m2"><p>گویند صبح نبود شام تو را دروغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند بهر عشق تو خود را چه می‌کشی</p></div>
<div class="m2"><p>بعد از فنای جسم نباشد بقا دروغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویند اشک چشم تو در عشق بیهده‌ست</p></div>
<div class="m2"><p>چون چشم بسته گشت نباشد لقا دروغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند چون ز دور زمانه برون شدیم</p></div>
<div class="m2"><p>زان سو روان نباشد این جان ما دروغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند آن کسان که نرستند از خیال</p></div>
<div class="m2"><p>جمله خیال بد قصص انبیا دروغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند آن کسان که نرفتند راه راست</p></div>
<div class="m2"><p>ره نیست بنده را به جناب خدا دروغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند رازدان دل اسرار و راز غیب</p></div>
<div class="m2"><p>بی‌واسطه نگوید مر بنده را دروغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند بنده را نگشایند راز دل</p></div>
<div class="m2"><p>وز لطف بنده را نبرد بر سما دروغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویند آن کسی که بود در سرشت خاک</p></div>
<div class="m2"><p>با اهل آسمان نشود آشنا دروغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویند جان پاک از این آشیان خاک</p></div>
<div class="m2"><p>با پر عشق برنپرد بر هوا دروغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویند ذره ذره بد و نیک خلق را</p></div>
<div class="m2"><p>آن آفتاب حق نرساند جزا دروغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاموش کن ز گفت وگر گویدت کسی</p></div>
<div class="m2"><p>جز حرف و صوت نیست سخن را ادا دروغ</p></div></div>