---
title: >-
    غزل شمارهٔ ۱۵۶۰
---
# غزل شمارهٔ ۱۵۶۰

<div class="b" id="bn1"><div class="m1"><p>تا عشق تو سوخت همچو عودم</p></div>
<div class="m2"><p>یک عقده نماند از وجودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه باروی چرخ رخنه کردم</p></div>
<div class="m2"><p>گه سکه آفتاب سودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مه پی آفتاب رفتم</p></div>
<div class="m2"><p>گه کاهیدم گهی فزودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو دل من نمی‌شکیبد</p></div>
<div class="m2"><p>صد بار منش بیازمودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این بخشش توست زور من نیست</p></div>
<div class="m2"><p>گر حلقه سیم درربودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دشمن چاشتم خفاشم</p></div>
<div class="m2"><p>ور منکر احمدم جهودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تفهیم تو تیز کرد گوشم</p></div>
<div class="m2"><p>کان راز شریف را شنودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیل آمد و برد خفتگان را</p></div>
<div class="m2"><p>من تشنه بدم نمی‌غنودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صیقل گر سینه امر کن بود</p></div>
<div class="m2"><p>گر من ز کسل نمی‌زدودم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توفیر شد از مکارم تو</p></div>
<div class="m2"><p>هر تقصیری که من نمودم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من جود چرا کنم به جلدی</p></div>
<div class="m2"><p>کز جود تو مو به موی جودم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عشق تو بر فراز عرشم</p></div>
<div class="m2"><p>گر بالایم وگر فرودم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از فضل تو است اگر ضحوکم</p></div>
<div class="m2"><p>از رشک تو است اگر حسودم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس کردم ذکر شمس تبریز</p></div>
<div class="m2"><p>ای عالم سر تار و پودم</p></div></div>