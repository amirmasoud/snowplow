---
title: >-
    غزل شمارهٔ ۱۲۴۰
---
# غزل شمارهٔ ۱۲۴۰

<div class="b" id="bn1"><div class="m1"><p>آن مطرب ما خوشست و چنگش</p></div>
<div class="m2"><p>دیوانه شود دل از ترنگش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چنگ زند یکی تو بنگر</p></div>
<div class="m2"><p>کز لطف چگونه گشت رنگش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تنگ آیی ز زندگانی</p></div>
<div class="m2"><p>برجه به کنار گیر تنگش</p></div></div>