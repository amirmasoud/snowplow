---
title: >-
    غزل شمارهٔ ۷۹۷
---
# غزل شمارهٔ ۷۹۷

<div class="b" id="bn1"><div class="m1"><p>ز اول روز که مخموری مستان باشد</p></div>
<div class="m2"><p>شیخ را ساغر جان در کف دستان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش او ذره صفت هر سحری رقص کنیم</p></div>
<div class="m2"><p>این چنین عادت خورشیدپرستان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ابد این رخ خورشید سحر در سحرست</p></div>
<div class="m2"><p>تا دل سنگ از او لعل بدخشان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای صلاح دل و دین تو ز برون جهتی</p></div>
<div class="m2"><p>تا چنین شش جهت از نور تو رخشان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده عشق تو در عشق کجا سرد شود</p></div>
<div class="m2"><p>چون صلاح دل و دین آتش سوزان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو رضای دل او جو اگرت دل باید</p></div>
<div class="m2"><p>دل او چون طلبد آنک گران جان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بس ایمان که شود کفر چو با او نبود</p></div>
<div class="m2"><p>ای بسی کفر که از دولتش ایمان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلخنی را چو ببینی به دل و روی سیاه</p></div>
<div class="m2"><p>هر چه از کان گهر گوید بهتان باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریز تو سلطان همه خوبانی</p></div>
<div class="m2"><p>هم جمال تو مگر یوسف کنعان باشد</p></div></div>