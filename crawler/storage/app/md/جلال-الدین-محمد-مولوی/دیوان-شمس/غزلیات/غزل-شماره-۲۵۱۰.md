---
title: >-
    غزل شمارهٔ ۲۵۱۰
---
# غزل شمارهٔ ۲۵۱۰

<div class="b" id="bn1"><div class="m1"><p>مرا پرسید آن سلطان به نرمی و سخن خایی</p></div>
<div class="m2"><p>عجب امسال ای عاشق بدان اقبالگه آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای آنک واگوید نمودم گوش کرانه</p></div>
<div class="m2"><p>که یعنی من گران گوشم سخن را بازفرمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر کوری بود کان دم نسازد خویشتن را کر</p></div>
<div class="m2"><p>که تا باشد که واگوید سخن آن کان زیبایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهم دریافت بازی را بخندید و بگفت این را</p></div>
<div class="m2"><p>بدان کس گو که او باشد چو تو بی‌عقل و هیهایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی حمله دگر چون کر ببردم گوش و سر پیشش</p></div>
<div class="m2"><p>بگفتا شید آوردی تو جز استیزه نفزایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دعوی کری کردم جواب و عذر چون گویم</p></div>
<div class="m2"><p>همه در هام شد بسته بدان فرهنگ و بدرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دربانش نظر کردم که یک نکته درافکن تو</p></div>
<div class="m2"><p>بپرسیدش ز نام من بگفتا گیج و سودایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر کردم دگربارش که اندرکش به گفتارش</p></div>
<div class="m2"><p>که شاگرد در اویی چو او عیارسیمایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا چشمک زد آن دربان که تو او را نمی‌دانی</p></div>
<div class="m2"><p>که حیلت گر به پیش او نبیند غیر رسوایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن حیلت که آن حلوا گهی در حلق تو آید</p></div>
<div class="m2"><p>که جوشی بر سر آتش مثال دیگ حلوایی</p></div></div>