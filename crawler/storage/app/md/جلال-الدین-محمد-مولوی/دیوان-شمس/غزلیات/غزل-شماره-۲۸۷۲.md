---
title: >-
    غزل شمارهٔ ۲۸۷۲
---
# غزل شمارهٔ ۲۸۷۲

<div class="b" id="bn1"><div class="m1"><p>مرغ اندیشه که اندر همه دل‌ها بپری</p></div>
<div class="m2"><p>به خدا کز دل و از دلبر ما بی‌اثری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی که به هر روزنه‌ای درتابی</p></div>
<div class="m2"><p>از سر روزن آن اصل بصر بی‌بصری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد شبگیر که چون پیک خبرها آری</p></div>
<div class="m2"><p>ز آنچ دریای خبرهاست چرا بی‌خبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدبانا که تو را عقل و خرد می‌گویند</p></div>
<div class="m2"><p>ساکن سقف دماغی و چراغ نظری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر بام شدستی مه نو می‌جویی</p></div>
<div class="m2"><p>مه نو کو و تو مسکین به کجا می‌نگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ترسنده که از عشق گریزان شده‌ای</p></div>
<div class="m2"><p>ز کف عشق اگر جان ببری جان نبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهزنانند به هر گام یکی عشوه دهی</p></div>
<div class="m2"><p>وای بر تو گر از این عشوه دهان عشوه خری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مه ار تو عسسی الحذر از جامه کنان</p></div>
<div class="m2"><p>که کلاهت ببرند ار چه که سیمین کمری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حشر غره مشو این نگر ای مه کز بیم</p></div>
<div class="m2"><p>می‌گریزی همه شب گر چه شه باحشری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌گریزی تو ولی جان نبری از کف عشق</p></div>
<div class="m2"><p>تیرت آید سه پری گر چه همه تن سپری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر همه تن سپری ور ره پنهان سپری</p></div>
<div class="m2"><p>ور دو پر ور سه پری در فخ آن دام وری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مردم چشم که مردم به تو مردم بیند</p></div>
<div class="m2"><p>نظرت نیست به دل گر چه که صاحب نظری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در درون ظلمات سیهی چشمان</p></div>
<div class="m2"><p>همچو آب حیوان ساکنی و مستتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خانه در دیده گرفتی و تو را یار نشد</p></div>
<div class="m2"><p>آنک از چشمه او جوش کند دیده وری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر شکر را خبری بودی از لذت عشق</p></div>
<div class="m2"><p>آب گشتی ز خجالت ننمودی شکری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چشم غیرت ز حسد گوش شکر را کر کرد</p></div>
<div class="m2"><p>ترس از آن چشم که در گوش شکر ریخت کری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شیر گردون که همه شیردلان از تو برند</p></div>
<div class="m2"><p>جگر و صف شکنی حمیت و استیزه گری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جگر باجگران آب ظفر از تو خورند</p></div>
<div class="m2"><p>به کمینگاه دل اهل دلان بی‌جگری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شیر ز آتش برمد سخت و دل آتشکده‌ای است</p></div>
<div class="m2"><p>جان پروانه بود بر شرر شمع جری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پر پروانه بسوزد جز پروانه دل</p></div>
<div class="m2"><p>که پرش ده پره گردد ز فروغ شرری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه حلمی ز خلاء زیر پر دل می‌رو</p></div>
<div class="m2"><p>تا تو را علم دهد واهب انسان و پری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رو به مریخ بگو که بنگر وصلت دل</p></div>
<div class="m2"><p>تا که خنجر بنهی هیچ سری را نبری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر توانی عوض سر سر دیگر دادن</p></div>
<div class="m2"><p>سزد ار سر ببری حاکم و وهاب سری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر ز تو یافت سری پر ز تو دزدید پری</p></div>
<div class="m2"><p>ز تو آموخت تری و ز تو آورد زری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شیشه گر کو به دمی صد قدح و جام کند</p></div>
<div class="m2"><p>قدحی گر شکند زو نتوان گشت بری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مشتری را نرسد لاف که من سیمبرم</p></div>
<div class="m2"><p>که نبود و نبود سیمبری سیم بری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مشتری بود زلیخا مه کنعانی را</p></div>
<div class="m2"><p>سیم بر بود بر سیم بر از زرشمری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زهره زخمه زن آخر بشنو زخمه دل</p></div>
<div class="m2"><p>بتری غره مشو چنگ کنندت بتری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنگ دل چند از این چنگ و دف و نای شکست</p></div>
<div class="m2"><p>وای بر مادر تو گر نکند دل پدری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای عطارد بس از این کاغذ و از حبر و قلم</p></div>
<div class="m2"><p>زفتی و لاف و تکبر حیل و پرهنری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر پلنگی به یکی باد چو موشی گردی</p></div>
<div class="m2"><p>ور تو شیری به یکی برق ز روبه بتری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سر قدم کن چو قلم بر اثر دل می‌رو</p></div>
<div class="m2"><p>که اثرهاست نهان در عدم و بی‌صوری</p></div></div>