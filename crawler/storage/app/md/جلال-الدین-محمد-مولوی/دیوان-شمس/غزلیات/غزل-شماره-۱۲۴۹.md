---
title: >-
    غزل شمارهٔ ۱۲۴۹
---
# غزل شمارهٔ ۱۲۴۹

<div class="b" id="bn1"><div class="m1"><p>شده‌ام سپند حسنت وطنم میان آتش</p></div>
<div class="m2"><p>چو ز تیر تست بنده بکشد کمان آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بسوخت جان عاشق ز حبیب سر برآرد</p></div>
<div class="m2"><p>چه بسوخت اندر آتش که نگشت جان آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمسوز جز دلم را که ز آتشت به داغم</p></div>
<div class="m2"><p>بنگر به سینه من اثر سنان آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ستاره‌های آتش سوی سوخته گراید</p></div>
<div class="m2"><p>که ز سوخته بیابد شررش نشان آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم عشق آتشینت چو درخت کرد خشکم</p></div>
<div class="m2"><p>چو درخت خشک گردد نبود جز آن آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنک آنک ز آتش تو سمن و گلشن بروید</p></div>
<div class="m2"><p>که خلیل عشق داند به صفا زبان آتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که خلیل او بر آتش چو دخان بود سواره</p></div>
<div class="m2"><p>که خلیل مالک آمد به کفش عنان آتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سحری صلای عشقت بشنید گوش جانم</p></div>
<div class="m2"><p>که درآ در آتش ما بجه از جهان آتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل چون تنور پر شد که ز سوز چند گوید</p></div>
<div class="m2"><p>دهن پرآتش من سخن از دهان آتش</p></div></div>