---
title: >-
    غزل شمارهٔ ۲۹۹۱
---
# غزل شمارهٔ ۲۹۹۱

<div class="b" id="bn1"><div class="m1"><p>ای عشق پرده در که تو در زیر چادری</p></div>
<div class="m2"><p>در حسن حوریی تو و در مهر مادری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلقه اندرآ و ببین جمله جان‌ها</p></div>
<div class="m2"><p>در گوش حلقه کرده به قانون چاکری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آینه نظر کن و در چشم خود نگر</p></div>
<div class="m2"><p>صد جان گره گره شده از وی به ساحری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر گره نگه کن وضع خدای بین</p></div>
<div class="m2"><p>در هم ببسته موسی و فرعون و سامری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زیر دامنت تو برون آر شمع را</p></div>
<div class="m2"><p>تا نقش حق بخندد بر نقش آزری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دست و پا نهاد دو زلف تو کفر را</p></div>
<div class="m2"><p>هر دم بمیرد ایمان در پای کافری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون مر تو را نیابد در جان و جا دلم</p></div>
<div class="m2"><p>گشتم هزار بار من از جان و جا بری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خشک و تر دو چشم و لب من روان شده</p></div>
<div class="m2"><p>در قلزمی که خشک نیابند و نی تری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دی لطف‌ها بکرد خیال تو گفتمش</p></div>
<div class="m2"><p>کای باوفا و عهد ز من باوفاتری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانم ز شمس دین است تو را این همه وفا</p></div>
<div class="m2"><p>تبریز این سلام بر جان ما بری</p></div></div>