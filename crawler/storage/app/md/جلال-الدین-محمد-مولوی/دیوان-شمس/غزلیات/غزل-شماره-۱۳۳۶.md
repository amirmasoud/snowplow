---
title: >-
    غزل شمارهٔ ۱۳۳۶
---
# غزل شمارهٔ ۱۳۳۶

<div class="b" id="bn1"><div class="m1"><p>حلقه دل زدم شبی در هوس سلام دل</p></div>
<div class="m2"><p>بانگ رسید کیست آن گفتم من غلام دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله نور آن قمر می‌زد از شکاف در</p></div>
<div class="m2"><p>بر دل و چشم رهگذر از بر نیک نام دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج ز نور روی دل پر شده بود کوی دل</p></div>
<div class="m2"><p>کوزه آفتاب و مه گشته کمینه جام دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل کل ار سری کند با دل چاکری کند</p></div>
<div class="m2"><p>گردن عقل و صد چو او بسته به بند دام دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفته به چرخ ولوله کون گرفته مشغله</p></div>
<div class="m2"><p>خلق گسسته سلسله از طرف پیام دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نور گرفته از برش کرسی و عرش اکبرش</p></div>
<div class="m2"><p>روح نشسته بر درش می‌نگرد به بام دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست قلندر از بشر نک به تو گفت مختصر</p></div>
<div class="m2"><p>جمله نظر بود نظر در خمشی کلام دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله کون مست دل گشته زبون به دست دل</p></div>
<div class="m2"><p>مرحله‌های نه فلک هست یقین دو گام دل</p></div></div>