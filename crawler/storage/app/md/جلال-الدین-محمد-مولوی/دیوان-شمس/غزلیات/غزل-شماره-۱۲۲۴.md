---
title: >-
    غزل شمارهٔ ۱۲۲۴
---
# غزل شمارهٔ ۱۲۲۴

<div class="b" id="bn1"><div class="m1"><p>پریشان باد پیوسته دل از زلف پریشانش</p></div>
<div class="m2"><p>وگر برناورم فردا سر خویش از گریبانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا ای شحنه خوبی ز لعل تو بسی گوهر</p></div>
<div class="m2"><p>بدزدیدست جان من برنجانش برنجانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ایمان آورد جانی به غیر کافر زلفت</p></div>
<div class="m2"><p>بزن از آتش شوقت تو اندر کفر و ایمانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پریشان باد زلف او که تا پنهان شود رویش</p></div>
<div class="m2"><p>که تا تنها مرا باشد پریشانی ز پنهانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم در عشق بی‌برگی که اندر باغ عشق او</p></div>
<div class="m2"><p>چو گل پاره کنم جامه ز سودای گلستانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن گل‌های رخسارش همی‌غلطید روزی دل</p></div>
<div class="m2"><p>بگفتم چیست این گفتا همی‌غلطم در احسانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی خطی نویسم من ز حال خود بر آن عارض</p></div>
<div class="m2"><p>که تا برخواند آن عارض که استادست خط خوانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولیکن سخت می‌ترسم از آن زلف سیه کاوش</p></div>
<div class="m2"><p>که بس دل در رسن بستست آن هندو ز بهتانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چاه آن ذقن بنگر مترس ای دل ز افتادن</p></div>
<div class="m2"><p>که هر دل کان رسن بیند چنان چاهست زندانش</p></div></div>