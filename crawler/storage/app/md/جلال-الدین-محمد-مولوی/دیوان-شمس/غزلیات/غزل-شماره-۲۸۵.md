---
title: >-
    غزل شمارهٔ ۲۸۵
---
# غزل شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>اتاک عید وصال فلا تذق حزنا</p></div>
<div class="m2"><p>و نلت خیر ریاض فنعم ما سکنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و زال عنک فراق امر من صبر</p></div>
<div class="m2"><p>و محنه فتنتنا و خاب من فتنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فهز غصن سعود و کل جنا شجر</p></div>
<div class="m2"><p>فقر عینک منه و نعم ذاک جنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فطب تجوت من اصحاب قریه ظلمت</p></div>
<div class="m2"><p>و نال قلبک منهم شقاوه و عنا</p></div></div>