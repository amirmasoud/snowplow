---
title: >-
    غزل شمارهٔ ۱۲۲۸
---
# غزل شمارهٔ ۱۲۲۸

<div class="b" id="bn1"><div class="m1"><p>ای یوسف مه رویان ای جاه و جمالت خوش</p></div>
<div class="m2"><p>ای خسرو و ای شیرین ای نقش و خیالت خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چهره تو مه وش آبست و در او آتش</p></div>
<div class="m2"><p>هم آتش تو نادر هم آب زلالت خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای صورت لطف حق نقش تو خوشست الحق</p></div>
<div class="m2"><p>ای نقش تو روحانی وی نور جلالت خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مستی هوش آخر در مهر بجوش آخر</p></div>
<div class="m2"><p>در وصل بکوش آخر ای صبح وصالت خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای روز ز روی تو شب سایه موی تو</p></div>
<div class="m2"><p>چون ماه برآ امشب ای طالع و فالت خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر لطف و وصال آری ور جور و محال آری</p></div>
<div class="m2"><p>آمیخته‌ای با جان ای جور و محالت خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گفت مرا روزی سالی گذرد زان مه</p></div>
<div class="m2"><p>جان گفت به گوش دل کای دل مه و سالت خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تبریز بگو آخر با غمزه شمس الدین</p></div>
<div class="m2"><p>کای فتنه جادویان ای سحر حلالت خوش</p></div></div>