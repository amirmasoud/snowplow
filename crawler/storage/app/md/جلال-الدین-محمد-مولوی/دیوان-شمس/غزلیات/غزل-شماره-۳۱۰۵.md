---
title: >-
    غزل شمارهٔ ۳۱۰۵
---
# غزل شمارهٔ ۳۱۰۵

<div class="b" id="bn1"><div class="m1"><p>ز صبحگاه فتادم به دست سرمستی</p></div>
<div class="m2"><p>نهاده جام چو خورشید بر کف دستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نوبهار رخش این جهان گلستانی</p></div>
<div class="m2"><p>به پیش قامت زیباش آسمان پستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروگرفت مرا مست وار و می‌گفتم</p></div>
<div class="m2"><p>بجستمی من از او گر بهانه‌ای هستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفت حیله مکن هین گمان مبر که اگر</p></div>
<div class="m2"><p>تن تو حیله شدی سر به سر ز ما رستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بریخت بر من از آن می که چرخ پست شدی</p></div>
<div class="m2"><p>اگر ز جرعه آن می دمی بخوردستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بتاب مفخر ایام شمس تبریزی</p></div>
<div class="m2"><p>ایا فکنده در این بحر نور شستستی</p></div></div>