---
title: >-
    غزل شمارهٔ ۲۰۶۳
---
# غزل شمارهٔ ۲۰۶۳

<div class="b" id="bn1"><div class="m1"><p>ای رخ خندان تو مایه صد گلستان</p></div>
<div class="m2"><p>باغ خدایی درآ خار بده گل ستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامه تن را بکن جان برهنه ببین</p></div>
<div class="m2"><p>جان برهنه خوش است تا چه کنی جامه دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هین که نه‌ای بی‌زبان پیش چنین جان‌ها</p></div>
<div class="m2"><p>قصه نی بی‌زبان نعره جان بی‌دهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد امروز یار گفت سلام علیک</p></div>
<div class="m2"><p>چرخ و زمین را مجو از نفسش آن زمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو خوبان بخواست از صنمان سرخراج</p></div>
<div class="m2"><p>خاست غریو از فلک وز سوی مه کالامان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لعل لب او که دور از لب و دندان تو</p></div>
<div class="m2"><p>خواند فسون‌های عشق خواجه ببین این نشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد غماز عشق گفت در این گوش من</p></div>
<div class="m2"><p>یار میان شماست خوب و لطیف و نهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن دل را کشید یار به یک گوشه‌ای</p></div>
<div class="m2"><p>گوشه بس بوالعجب زان سوی هفت آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت ترایم ولیک هر که بگوید ز من</p></div>
<div class="m2"><p>شرح دهد از لبم ده بزنش بر دهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و آنک بگوید ز تو برد مرا و تو را</p></div>
<div class="m2"><p>و آنک بگوید ز من دور شد از هر دوان</p></div></div>