---
title: >-
    غزل شمارهٔ ۲۴۹
---
# غزل شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>دل بر ما شدست دلبر ما</p></div>
<div class="m2"><p>گل ما بی‌حدست و شکر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما همیشه میان گلشکریم</p></div>
<div class="m2"><p>زان دل ما قویست در بر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهره دارد حوادث طبعی</p></div>
<div class="m2"><p>که بگردد بگرد لشکر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما به پر می‌پریم سوی فلک</p></div>
<div class="m2"><p>زانک عرشیست اصل جوهر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساکنان فلک بخور کنند</p></div>
<div class="m2"><p>از صفات خوش معنبر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه نسرین و ارغوان و گلست</p></div>
<div class="m2"><p>بر زمین شاهراه کشور ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بخندد نه بشکفد عالم</p></div>
<div class="m2"><p>بی نسیم دم منور ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذره‌های هوا پذیرد روح</p></div>
<div class="m2"><p>از دم عشق روح پرور ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش‌ها گشته‌اند محرم غیب</p></div>
<div class="m2"><p>از زبان و دل سخنور ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمس تبریز ابرسوز شدست</p></div>
<div class="m2"><p>سایه‌اش کم مباد از سر ما</p></div></div>