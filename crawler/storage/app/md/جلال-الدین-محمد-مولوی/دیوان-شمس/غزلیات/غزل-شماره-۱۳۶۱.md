---
title: >-
    غزل شمارهٔ ۱۳۶۱
---
# غزل شمارهٔ ۱۳۶۱

<div class="b" id="bn1"><div class="m1"><p>عمرک یا واحدا فی درجات الکمال</p></div>
<div class="m2"><p>قد نزل الهم بی یا سندی قم تعال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند از این قیل و قال عشق پرست و ببال</p></div>
<div class="m2"><p>تا تو بمانی چو عشق در دو جهان بی‌زوال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا فرجی مونسی یا قمر المجلس</p></div>
<div class="m2"><p>وجهک بدر تمام ریقک خمر حلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند کشی بار هجر غصه و تیمار هجر</p></div>
<div class="m2"><p>خاصه که منقار هجر کند تو را پر و بال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روحک بحر الوفا لونک لمع الصفا</p></div>
<div class="m2"><p>عمرک لو لا التقی قلت ایا ذا الجلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه ز نفس فضول آه ز ضعف عقول</p></div>
<div class="m2"><p>آه ز یار ملول چند نماید ملال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تطرب قلب الوری تسکرهم بالهوی</p></div>
<div class="m2"><p>تدرک ما لا یری انت لطیف الخیال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنک همی‌خوانمش عجز نمی‌دانمش</p></div>
<div class="m2"><p>تا که بترسانمش از ستم و از وبال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تدخل ارواحهم تسکر اشباحهم</p></div>
<div class="m2"><p>تجلسهم مجلسا فیه کؤوس ثقال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمله سؤال و جواب زوست و منم چون رباب</p></div>
<div class="m2"><p>می زندم او شتاب زخمه که یعنی بنال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تصلح میزاننا تحسن الحاننا</p></div>
<div class="m2"><p>تذهب احزاننا انت شدید المحال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک دم آواز مات یک دم بانگ نجات</p></div>
<div class="m2"><p>می زند آن خوش صفات بر من و بر وصف حال</p></div></div>