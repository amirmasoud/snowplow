---
title: >-
    غزل شمارهٔ ۳۹۰
---
# غزل شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>ساربانا اشتران بین سر به سر قطار مست</p></div>
<div class="m2"><p>میر مست و خواجه مست و یار مست اغیار مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغبانا رعد مطرب ابر ساقی گشت و شد</p></div>
<div class="m2"><p>باغ مست و راغ مست و غنچه مست و خار مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمانا چند گردی گردش عنصر ببین</p></div>
<div class="m2"><p>آب مست و باد مست و خاک مست و نار مست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال صورت این چنین و حال معنی خود مپرس</p></div>
<div class="m2"><p>روح مست و عقل مست و خاک مست اسرار مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو تو جباری رها کن خاک شو تا بنگری</p></div>
<div class="m2"><p>ذره ذره خاک را از خالق جبار مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نگویی در زمستان باغ را مستی نماند</p></div>
<div class="m2"><p>مدتی پنهان شدست از دیده مکار مست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیخ‌های آن درختان می نهانی می‌خورند</p></div>
<div class="m2"><p>روزکی دو صبر می‌کن تا شود بیدار مست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو را کوبی رسد از رفتن مستان مرنج</p></div>
<div class="m2"><p>با چنان ساقی و مطرب کی رود هموار مست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساقیا باده یکی کن چند باشد عربده</p></div>
<div class="m2"><p>دوستان ز اقرار مست و دشمنان ز انکار مست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد را افزون بده تا برگشاید این گره</p></div>
<div class="m2"><p>باده تا در سر نیفتد کی دهد دستار مست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخل ساقی باشد آن جا یا فساد باده‌ها</p></div>
<div class="m2"><p>هر دو ناهموار باشد چون رود رهوار مست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی‌های زرد بین و باده گلگون بده</p></div>
<div class="m2"><p>زانک از این گلگون ندارد بر رخ و رخسار مست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باده‌ای داری خدایی بس سبک خوار و لطیف</p></div>
<div class="m2"><p>زان اگر خواهد بنوشد روز صد خروار مست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمس تبریزی به دورت هیچ کس هشیار نیست</p></div>
<div class="m2"><p>کافر و مؤمن خراب و زاهد و خمار مست</p></div></div>