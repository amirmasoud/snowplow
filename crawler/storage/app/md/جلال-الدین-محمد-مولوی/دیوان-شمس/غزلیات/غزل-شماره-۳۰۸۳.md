---
title: >-
    غزل شمارهٔ ۳۰۸۳
---
# غزل شمارهٔ ۳۰۸۳

<div class="b" id="bn1"><div class="m1"><p>بیا بیا که چو آب حیات درخوردی</p></div>
<div class="m2"><p>بیا بیا که شفا و دوای هر دردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا بیا که گلستان ثنات می‌گوید</p></div>
<div class="m2"><p>بیا بیا بنما کز کجاش پروردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا بیا که به بیمارخانه بی‌قدمت</p></div>
<div class="m2"><p>نمی‌رود ز رخ هیچ خسته‌ای زردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآ برآ هله ای آفتاب چون بی‌تو</p></div>
<div class="m2"><p>نمی‌رود ز هوا هیچ تلخی و سردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآ برآ هله ای مه که حیف بسیارست</p></div>
<div class="m2"><p>که دیده‌ها همه گریان و تو در این گردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا بیا که ولی نعمت همه کونی</p></div>
<div class="m2"><p>که مخلص دل حیران و مهره نردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا بیا و بیاموز بنده خود را</p></div>
<div class="m2"><p>که در امامت و تعلیم و آگهی فردی</p></div></div>