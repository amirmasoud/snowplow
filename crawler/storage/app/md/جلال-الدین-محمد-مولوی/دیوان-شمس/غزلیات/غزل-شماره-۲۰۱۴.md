---
title: >-
    غزل شمارهٔ ۲۰۱۴
---
# غزل شمارهٔ ۲۰۱۴

<div class="b" id="bn1"><div class="m1"><p>راز چون با من نگوید یار من</p></div>
<div class="m2"><p>بند گردد پیش او گفتار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عذر می‌گوید که یعنی خامشم</p></div>
<div class="m2"><p>با تو می‌گوید دل هشیار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با کسی دیگر زبان گردد همه</p></div>
<div class="m2"><p>سر خود می‌گوید و اسرار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گمان افتد دلم زین واقعه</p></div>
<div class="m2"><p>این دل ترسان بدپندار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بگوید ور نگوید راز من</p></div>
<div class="m2"><p>دل ندارد صبر از دلدار من</p></div></div>