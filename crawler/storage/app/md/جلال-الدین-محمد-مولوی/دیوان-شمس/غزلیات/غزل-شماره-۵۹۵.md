---
title: >-
    غزل شمارهٔ ۵۹۵
---
# غزل شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>آن را که درون دل عشق و طلبی باشد</p></div>
<div class="m2"><p>چون دل نگشاید در آن را سببی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو بر در دل بنشین کان دلبر پنهانی</p></div>
<div class="m2"><p>وقت سحری آید یا نیم شبی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانی که جدا گردد جویای خدا گردد</p></div>
<div class="m2"><p>او نادره‌ای باشد او بوالعجبی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دیده کز این ایوان ایوان دگر بیند</p></div>
<div class="m2"><p>صاحب نظری باشد شیرین لقبی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن کس که چنین باشد با روح قرین باشد</p></div>
<div class="m2"><p>در ساعت جان دادن او را طربی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایش چو به سنگ آید دریش به چنگ آید</p></div>
<div class="m2"><p>جانش چو به لب آید با قندلبی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تاج ملوکاتش در چشم نمی‌آید</p></div>
<div class="m2"><p>او بی‌پدر و مادر عالی نسبی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاموش کن و هر جا اسرار مکن پیدا</p></div>
<div class="m2"><p>در جمع سبک روحان هم بولهبی باشد</p></div></div>