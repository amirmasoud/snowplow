---
title: >-
    غزل شمارهٔ ۲۴۲۰
---
# غزل شمارهٔ ۲۴۲۰

<div class="b" id="bn1"><div class="m1"><p>آمد آمد نگار پوشیده</p></div>
<div class="m2"><p>صنم خوش عذار پوشیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد از گلستان حسن و جمال</p></div>
<div class="m2"><p>باغ را نوبهار پوشیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زمین دل همه عشاق</p></div>
<div class="m2"><p>رسته شد سبزه زار پوشیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دم پرده سوز گرمش را</p></div>
<div class="m2"><p>هر طرف گرمدار پوشیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همگنان اشک و خون روان کرده</p></div>
<div class="m2"><p>خونشان در تغار پوشیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی آن خون همی‌رسد به دماغ</p></div>
<div class="m2"><p>همچو مشک تتار پوشیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا از آن بو برند مشتاقان</p></div>
<div class="m2"><p>سوی آن یار غار پوشیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس تبریز صدقه جانت</p></div>
<div class="m2"><p>بوسه‌ای یا کنار پوشیده</p></div></div>