---
title: >-
    غزل شمارهٔ ۹۷
---
# غزل شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>رفتم به سوی مصر و خریدم شکری را</p></div>
<div class="m2"><p>خود فاش بگو یوسف زرین کمری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شهر کی دیدست چنین شهره بتی را</p></div>
<div class="m2"><p>در بر کی کشیدست سهیل و قمری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنشاند به ملکت ملکی بنده بد را</p></div>
<div class="m2"><p>بخرید به گوهر کرمش بی‌گهری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر خضرانست و از هیچ عجب نیست</p></div>
<div class="m2"><p>کز چشمه جان تازه کند او جگری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بهر زبردستی و دولت دهی آمد</p></div>
<div class="m2"><p>نی زیر و زبر کردن زیر و زبری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاید که نخسپیم به شب چونک نهانی</p></div>
<div class="m2"><p>مه بوسه دهد هر شب انجم شمری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آثار رساند دل و جان را به مؤثر</p></div>
<div class="m2"><p>حمال دل و جان کند آن شه اثری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اکسیر خداییست بدان آمد کاین جا</p></div>
<div class="m2"><p>هر لحظه زر سرخ کند او حجری را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان‌های چو عیسی به سوی چرخ برانند</p></div>
<div class="m2"><p>غم نیست اگر ره نبود لاشه خری را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چیز گمان بردم در عالم و این نی</p></div>
<div class="m2"><p>کاین جاه و جلالست خدایی نظری را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوز دل شاهانه خورشید بباید</p></div>
<div class="m2"><p>تا سرمه کشد چشم عروس سحری را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما عقل نداریم یکی ذره وگر نی</p></div>
<div class="m2"><p>کی آهوی عاقل طلبد شیر نری را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی عقل چو سایه پیت ای دوست دوانیم</p></div>
<div class="m2"><p>کان روی چو خورشید تو نبود دگری را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورشید همه روز بدان تیغ گزارد</p></div>
<div class="m2"><p>تا زخم زند هر طرفی بی‌سپری را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر سینه نهد عقل چنان دل شکنی را</p></div>
<div class="m2"><p>در خانه کشد روح چنان رهگذی را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در هدیه دهد چشم چنان لعل لبی را</p></div>
<div class="m2"><p>رخ زر زند از بهر چنین سیمبری را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رو صاحب آن چشم شو ای خواجه چو ابرو</p></div>
<div class="m2"><p>کو راست کند چشم کژ کژنگری را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای پاک دلان با جز او عشق مبازید</p></div>
<div class="m2"><p>نتوان دل و جان دادن هر مختصری را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاموش که او خود بکشد عاشق خود را</p></div>
<div class="m2"><p>تا چند کشی دامن هر بی‌هنری را</p></div></div>