---
title: >-
    غزل شمارهٔ ۷۸۵
---
# غزل شمارهٔ ۷۸۵

<div class="b" id="bn1"><div class="m1"><p>ما نه زان محتشمانیم که ساغر گیرند</p></div>
<div class="m2"><p>و نه زان مفلسکان که بز لاغر گیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما از آن سوختگانیم که از لذت سوز</p></div>
<div class="m2"><p>آب حیوان بهلند و پی آذر گیرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مه از روزن هر خانه که اندرتابیم</p></div>
<div class="m2"><p>از ضیا شب صفتان جمله ره در گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناامیدان که فلک ساغر ایشان بشکست</p></div>
<div class="m2"><p>چو ببینند رخ ما طرب از سر گیرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنک زین جرعه کشد جمله جهانش نکشد</p></div>
<div class="m2"><p>مگر او را به گلیم از بر ما برگیرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کی او گرم شد این جا نشود غره کس</p></div>
<div class="m2"><p>اگرش سردمزاجان همه در زر گیرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در فروبند و بده باده که آن وقت رسید</p></div>
<div class="m2"><p>زردرویان تو را که می احمر گیرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یکی دست می خالص ایمان نوشند</p></div>
<div class="m2"><p>به یکی دست دگر پرچم کافر گیرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب ماییم به هر جا که بگردد چرخی</p></div>
<div class="m2"><p>عود ماییم به هر سور که مجمر گیرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس این پرده ازرق صنمی مه روییست</p></div>
<div class="m2"><p>که ز نور رخش انجم همه زیور گیرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز احتراقات و ز تربیع و نحوست برهند</p></div>
<div class="m2"><p>اگر او را سحری گوشه چادر گیرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو دورای و دودلی و دل صاف آن‌ها راست</p></div>
<div class="m2"><p>که دل خود بهلند و دل دلبر گیرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خمش ای عقل عطارد که در این مجلس عشق</p></div>
<div class="m2"><p>حلقه زهره بیانت همه تسخر گیرند</p></div></div>