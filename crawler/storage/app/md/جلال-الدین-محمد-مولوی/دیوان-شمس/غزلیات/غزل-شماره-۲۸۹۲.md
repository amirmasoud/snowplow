---
title: >-
    غزل شمارهٔ ۲۸۹۲
---
# غزل شمارهٔ ۲۸۹۲

<div class="b" id="bn1"><div class="m1"><p>ای شه جاودانی وی مه آسمانی</p></div>
<div class="m2"><p>چشمه زندگانی گلشن لامکانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زلال تو دیدم قصه جان شنیدم</p></div>
<div class="m2"><p>همچو جان ناپدیدم در تک بی‌نشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق مشک خوش بو می‌کند صید آهو</p></div>
<div class="m2"><p>می‌رود مست هر سو یا تواش می‌دوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شکر بنده تو زان شکرخنده تو</p></div>
<div class="m2"><p>ای جهان زنده از تو غرقه زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز شد های مستان بشنوید از گلستان</p></div>
<div class="m2"><p>می‌کند مرغ دستان شیوه دلستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیوه یاسمین کن سر بجنبان چنین کن</p></div>
<div class="m2"><p>خانه پرانگبین کن چون شکر می‌فشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرگست مست گشته جنیی یا فرشته</p></div>
<div class="m2"><p>با شکر درسرشته غنچه گلستانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با چنین ساقی حق با خودی کفر مطلق</p></div>
<div class="m2"><p>می‌زند جان معلق با می رایگانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز و شب ای برادر مست و بی‌خویش خوشتر</p></div>
<div class="m2"><p>مست الله اکبر کش نبوده است ثانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نام او جان جان‌ها یاد او لعل کان‌ها</p></div>
<div class="m2"><p>عشق او در روان‌ها هم امان هم امانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون برم نام او را دررسد بخت خضرا</p></div>
<div class="m2"><p>اسم شد پس مسما بی‌دوی بی‌توانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند مستند پنهان اندر این سبز میدان</p></div>
<div class="m2"><p>می‌روم سوی ایشان با تو گفتم تو دانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان ویسند و رامین سخت شیرین شیرین</p></div>
<div class="m2"><p>مفخر آل یاسین وز خدا ارمغانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو اگر می‌شتابی سوی مرغان آبی</p></div>
<div class="m2"><p>آب حیوان بیابی قلزم شادمانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرب و شیرین بخوردی عیش و عشرت بکردی</p></div>
<div class="m2"><p>سوی عشق آی یک شب هم ببین میزبانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما هم از بامدادان بیخود و مست و شادان</p></div>
<div class="m2"><p>ای شه بامرادان مستمان می‌کشانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با ظریفان و خوبان تا به شب پای کوبان</p></div>
<div class="m2"><p>وز می پیر رهبان هر دمی دوستگانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این قدح می شتابد تا شما را بیابد</p></div>
<div class="m2"><p>در دل و جان بتابد از ره بی‌دهانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای که داری تو فهمی قبض کن قبض اعمی</p></div>
<div class="m2"><p>غیر این نیست چیزی تو مباش امتحانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غیر این نیست راهی غیر این نیست شاهی</p></div>
<div class="m2"><p>غیر این نیست ماهی غیر این جمله فانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نی خمش کن خمش کن رو به قاصد ترش کن</p></div>
<div class="m2"><p>ترک اصحاب هش کن باده خور در نهانی</p></div></div>