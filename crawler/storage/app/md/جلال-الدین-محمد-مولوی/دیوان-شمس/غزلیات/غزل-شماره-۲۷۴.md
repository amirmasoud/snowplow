---
title: >-
    غزل شمارهٔ ۲۷۴
---
# غزل شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>ایه یا اهل الفرادیس اقرؤا منشورنا</p></div>
<div class="m2"><p>و ادهشوا من خمرنا و استسمعوا ناقورنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حورکم تصفر عشقا تنحنی من ناره</p></div>
<div class="m2"><p>لو رات فی جنح لیل او نهار حورنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جاء بدر کامل قد کدر الشمس الضحی</p></div>
<div class="m2"><p>فی قیان خادمات و استقروا دورنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الف بدر حول بدری سجدا خروا له</p></div>
<div class="m2"><p>طیبوا ما حولنا و استشرقوا دیجورنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد سکرنا من حواشی بدرهم اکرم بهم</p></div>
<div class="m2"><p>استجابوا بغینا و استکثروا میسورنا</p></div></div>