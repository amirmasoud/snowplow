---
title: >-
    غزل شمارهٔ ۱۹۸۰
---
# غزل شمارهٔ ۱۹۸۰

<div class="b" id="bn1"><div class="m1"><p>یارکان رقصی کنید اندر غمم خوشتر از این</p></div>
<div class="m2"><p>کره عشقم رمید و نی لگامستم نی زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش روی ماه ما مستانه یک رقصی کنید</p></div>
<div class="m2"><p>مطربا بهر خدا بر دف بزن ضرب حزین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقص کن در عشق جانم ای حریف مهربان</p></div>
<div class="m2"><p>مطربا دف را بکوب و نیست بختت غیر از این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دف خوب تو این جا هست مقبول و صواب</p></div>
<div class="m2"><p>مطربا دف را بزن بس مر تو را طاعت همین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطربا این دف برای عشق شاه دلبر است</p></div>
<div class="m2"><p>مفخر تبریز جان جان جان‌ها شمس دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطربا گفتی تو نام شمس دین و شمس دین</p></div>
<div class="m2"><p>درربودی از سرم یک بارگی تو عقل و دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونک گفتی شمس دین زنهار تو فارغ مشو</p></div>
<div class="m2"><p>کفر باشد در طلب گر زانک گویی غیر این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطربا گشتی ملول از گفت من از گفت من</p></div>
<div class="m2"><p>همچنان خواهی مکن تو همچنین و همچنین</p></div></div>