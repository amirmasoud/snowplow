---
title: >-
    غزل شمارهٔ ۱۷۹۸
---
# غزل شمارهٔ ۱۷۹۸

<div class="b" id="bn1"><div class="m1"><p>ای یار من ای یار من ای یار بی‌زنهار من</p></div>
<div class="m2"><p>ای دلبر و دلدار من ای محرم و غمخوار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای در زمین ما را قمر ای نیم شب ما را سحر</p></div>
<div class="m2"><p>ای در خطر ما را سپر ای ابر شکربار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش می روی در جان من خوش می کنی درمان من</p></div>
<div class="m2"><p>ای دین و ای ایمان من ای بحر گوهردار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شب روان را مشعله ای بی‌دلان را سلسله</p></div>
<div class="m2"><p>ای قبله هر قافله ای قافله سالار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم رهزنی هم ره بری هم ماهی و هم مشتری</p></div>
<div class="m2"><p>هم این سری هم آن سری هم گنج و استظهار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون یوسف پیغامبری آیی که خواهم مشتری</p></div>
<div class="m2"><p>تا آتشی اندرزنی در مصر و در بازار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم موسیی بر طور من عیسی هر رنجور من</p></div>
<div class="m2"><p>هم نور نور نور من هم احمد مختار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم مونس زندان من هم دولت خندان من</p></div>
<div class="m2"><p>والله که صد چندان من بگذشته از بسیار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی مرا برجه بگو گویم چه گویم پیش تو</p></div>
<div class="m2"><p>گویی بیا حجت مجو ای بنده طرار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویم که گنجی شایگان گوید بلی نی رایگان</p></div>
<div class="m2"><p>جان خواهم وانگه چه جان گویم سبک کن بار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر گنج خواهی سر بنه ور عشق خواهی جان بده</p></div>
<div class="m2"><p>در صف درآ واپس مجه ای حیدر کرار من</p></div></div>