---
title: >-
    غزل شمارهٔ ۹۹۸
---
# غزل شمارهٔ ۹۹۸

<div class="b" id="bn1"><div class="m1"><p>آتش عشق تو قلاووز شد</p></div>
<div class="m2"><p>دوش دلم سوی دل افروز شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به سخن داشت مرا دوش یار</p></div>
<div class="m2"><p>چون به دم گرم جگرسوز شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چه زنم با دم و با مکر او</p></div>
<div class="m2"><p>کو به دغل بر همه پیروز شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دل من ساده و بی‌مکر بود</p></div>
<div class="m2"><p>دید دغل‌هاش بدآموز شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه به عالم خوشی شهوتست</p></div>
<div class="m2"><p>همچو پنیر آفت هر یوز شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه که شب جمله در این وعده رفت</p></div>
<div class="m2"><p>بوسه دهم بوسه دهم روز شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار برهنه به قبا میل کرد</p></div>
<div class="m2"><p>عقل دگربار کمردوز شد</p></div></div>