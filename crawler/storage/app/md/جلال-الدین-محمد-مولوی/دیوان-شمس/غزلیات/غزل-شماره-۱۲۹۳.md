---
title: >-
    غزل شمارهٔ ۱۲۹۳
---
# غزل شمارهٔ ۱۲۹۳

<div class="b" id="bn1"><div class="m1"><p>علی الله ای مسلمانان از آن هجران پرآتش</p></div>
<div class="m2"><p>ظلام فی ظلام من فراق الحب قد اغطش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دور افتاد ماهی جان ز بحر افتاد در حیله</p></div>
<div class="m2"><p>کما حوت الشقی الیوم فی ارض الفلاینبش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب نبود اگر عاشق شود بی‌جان در این هجران</p></div>
<div class="m2"><p>اذا ما الحوت زال الماء لا تعجب بان تعطش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر منکر شود مردی ز سوز عاشق سوزان</p></div>
<div class="m2"><p>متی یمتاز عین الشمس من عین له اعمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو فرش وصل بردارد شفا از منزل عاشق</p></div>
<div class="m2"><p>فراش من لهیب النار من تحت الفتی یفرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که تا پیغام آن یوسف بدین یعقوب عشق آید</p></div>
<div class="m2"><p>یبرد ذاک و البستان و الفردوس یستنعش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم در گوش من گوید ز حرص وصل شمس الدین</p></div>
<div class="m2"><p>الی تبریز یستسعی و فی تبریز یستفتش</p></div></div>