---
title: >-
    غزل شمارهٔ ۱۳۴۴
---
# غزل شمارهٔ ۱۳۴۴

<div class="b" id="bn1"><div class="m1"><p>شتران مست شدستند ببین رقص جمل</p></div>
<div class="m2"><p>ز اشتر مست که جوید ادب و علم و عمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم ما داده او و ره ما جاده او</p></div>
<div class="m2"><p>گرمی ما دم گرمش نه ز خورشید حمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم او جان دهدت روز نفخت بپذیر</p></div>
<div class="m2"><p>کار او کن فیکون‌ست نه موقوف علل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما در این ره همه نسرین و قرنفل کوبیم</p></div>
<div class="m2"><p>ما نه زان اشتر عامیم که کوبیم وحل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شتران وحلی بسته این آب و گلند</p></div>
<div class="m2"><p>پیش جان و دل ما آب و گلی را چه محل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناقه الله بزاده به دعای صالح</p></div>
<div class="m2"><p>جهت معجزه دین ز کمرگاه جبل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هان و هان ناقه حقیم تعرض مکنید</p></div>
<div class="m2"><p>تا نبرد سرتان را سر شمشیر اجل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی مشرق نرویم و سوی مغرب نرویم</p></div>
<div class="m2"><p>تا ابد گام زنان جانب خورشید ازل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هله بنشین تو بجنبان سر و می‌گوی بلی</p></div>
<div class="m2"><p>شمس تبریز نماید به تو اسرار غزل</p></div></div>