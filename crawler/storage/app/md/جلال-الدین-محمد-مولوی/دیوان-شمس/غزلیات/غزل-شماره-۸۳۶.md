---
title: >-
    غزل شمارهٔ ۸۳۶
---
# غزل شمارهٔ ۸۳۶

<div class="b" id="bn1"><div class="m1"><p>گر نخسپی شبکی جان چه شود</p></div>
<div class="m2"><p>ور نکوبی در هجران چه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور بیاری شبکی روز آری</p></div>
<div class="m2"><p>از برای دل یاران چه شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور دو دیده ز تو روشن گردد</p></div>
<div class="m2"><p>کوری دیده شیطان چه شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بگیرد ز گل افشانی تو</p></div>
<div class="m2"><p>همه عالم گل و ریحان چه شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب حیوان که در آن تاریکیست</p></div>
<div class="m2"><p>پر شود شهر و بیابان چه شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور خضروار قلاووز شوی</p></div>
<div class="m2"><p>تا لب چشمه حیوان چه شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور ز خوان کرم و نعمت تو</p></div>
<div class="m2"><p>زنده گردد دو سه مهمان چه شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور ز دلداری و جان بخشی تو</p></div>
<div class="m2"><p>جان بیابد دو سه بی‌جان چه شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور سواره سوی میدان آیی</p></div>
<div class="m2"><p>تا شود سینه چو میدان چه شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی چون ماهت اگر بنمایی</p></div>
<div class="m2"><p>تا رود زهره به میزان چه شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور بریزی قدحی مالامال</p></div>
<div class="m2"><p>بر سر وقت خماران چه شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور بپوشیم یکی خلعت نو</p></div>
<div class="m2"><p>ما غلامان ز تو سلطان چه شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور چو موسی تو بگیری چوبی</p></div>
<div class="m2"><p>تا شود چوب چو ثعبان چه شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور برآری ز تک دریا گرد</p></div>
<div class="m2"><p>چو کف موسی عمران چه شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور سلیمان بر موران آید</p></div>
<div class="m2"><p>تا شود مور سلیمان چه شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بس کن و جمع کن و خامش باش</p></div>
<div class="m2"><p>گر نگویی تو پریشان چه شود</p></div></div>