---
title: >-
    غزل شمارهٔ ۱۹۸۸
---
# غزل شمارهٔ ۱۹۸۸

<div class="b" id="bn1"><div class="m1"><p>چه شکر داد عجب یوسف خوبی به لبان</p></div>
<div class="m2"><p>که شد ادریسش قیماز و سلیمان به لبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شکرخانه او رفته به سر لب شکران</p></div>
<div class="m2"><p>مانده اندر عجبش خیره همه بوالعجبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر افتاد که گرگی طمع یوسف کرد</p></div>
<div class="m2"><p>همه گرگان شده از خجلت این گرگ شبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خوشی‌های نهان است در آن درد و غمش</p></div>
<div class="m2"><p>که رمیدند ز دارو همه درمان طلبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس بود هستی او مایه هر نیست شده</p></div>
<div class="m2"><p>بس بود مستی او عذر همه بی‌ادبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارف از ورزش اسباب بدان کاهل شد</p></div>
<div class="m2"><p>که همان بی‌سببی شد سبب بی‌سببان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیز کامروز ز اقبال و سعادت باری</p></div>
<div class="m2"><p>طرب اندر طرب است از مدد بوطربان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بر آن بودم کز جان و دل تفسیده</p></div>
<div class="m2"><p>بازگویی صفت عشق به روزان و شبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریزی مرا دوش همی‌گفت خموش</p></div>
<div class="m2"><p>چون تو را عشق لب ماست نگهدار زبان</p></div></div>