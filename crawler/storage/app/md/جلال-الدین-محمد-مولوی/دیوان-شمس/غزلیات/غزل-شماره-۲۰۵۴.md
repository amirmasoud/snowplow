---
title: >-
    غزل شمارهٔ ۲۰۵۴
---
# غزل شمارهٔ ۲۰۵۴

<div class="b" id="bn1"><div class="m1"><p>بشنیده‌ام که عزم سفر می‌کنی مکن</p></div>
<div class="m2"><p>مهر حریف و یار دگر می‌کنی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو در جهان غریبی غربت چه می‌کنی</p></div>
<div class="m2"><p>قصد کدام خسته جگر می‌کنی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ما مدزد خویش به بیگانگان مرو</p></div>
<div class="m2"><p>دزدیده سوی غیر نظر می‌کنی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مه که چرخ زیر و زبر از برای توست</p></div>
<div class="m2"><p>ما را خراب و زیر و زبر می‌کنی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه وعده می‌دهی و چه سوگند می‌خوری</p></div>
<div class="m2"><p>سوگند و عشوه را تو سپر می‌کنی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو عهد و کو وثیقه که با بنده کرده‌ای</p></div>
<div class="m2"><p>از عهد و قول خویش عبر می‌کنی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای برتر از وجود و عدم بارگاه تو</p></div>
<div class="m2"><p>از خطه وجود گذر می‌کنی مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دوزخ و بهشت غلامان امر تو</p></div>
<div class="m2"><p>بر ما بهشت را چو سقر می‌کنی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر شکرستان تو از زهر ایمنیم</p></div>
<div class="m2"><p>آن زهر را حریف شکر می‌کنی مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جانم چو کوره‌ای است پرآتش بست نکرد</p></div>
<div class="m2"><p>روی من از فراق چو زر می‌کنی مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون روی درکشی تو شود مه سیه ز غم</p></div>
<div class="m2"><p>قصد خسوف قرص قمر می‌کنی مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما خشک لب شویم چو تو خشک آوری</p></div>
<div class="m2"><p>چشم مرا به اشک چه تر می‌کنی مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون طاقت عقیله عشاق نیستت</p></div>
<div class="m2"><p>پس عقل را چه خیره نگر می‌کنی مکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حلوا نمی‌دهی تو به رنجور ز احتما</p></div>
<div class="m2"><p>رنجور خویش را تو بتر می‌کنی مکن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشم حرام خواره من دزد حسن توست</p></div>
<div class="m2"><p>ای جان سزای دزد بصر می‌کنی مکن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر درکش ای رفیق که هنگام گفت نیست</p></div>
<div class="m2"><p>در بی‌سری عشق چه سر می‌کنی مکن</p></div></div>