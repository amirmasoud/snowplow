---
title: >-
    غزل شمارهٔ ۲۴۰۲
---
# غزل شمارهٔ ۲۴۰۲

<div class="b" id="bn1"><div class="m1"><p>باده بده ساقیا عشوه و بادم مده</p></div>
<div class="m2"><p>وز غم فردا و دی هیچ به یادم مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده از آن خم مِه پر کن و پیشم بنه</p></div>
<div class="m2"><p>گر نگشایم گره هیچ گشادم مده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گذرد می ز سر گویم ای خوش پسر</p></div>
<div class="m2"><p>باده نخواهم دگر مست فتادم مده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاکر خنده توام کشته زنده توام</p></div>
<div class="m2"><p>گر نه که بنده توام باده شادم مده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتنه به شهر توام کشته قهر توام</p></div>
<div class="m2"><p>گر نه که بهر توام هیچ مرادم مده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدقه از آن لعل کان بخش بر این پرزیان</p></div>
<div class="m2"><p>ور ز برای تو جان صدقه ندادم مده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر کین درگذر بوسه ده ای لب شکر</p></div>
<div class="m2"><p>بر سر هر خاک سر گر ننهادم مده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که دوم بار زاد عشق بدو داد داد</p></div>
<div class="m2"><p>صد ره از صدق و داد گر بنزادم مده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس حق نیک نام شد تبریزت مقام</p></div>
<div class="m2"><p>گر نشکستم تمام هیچ تو دادم مده</p></div></div>