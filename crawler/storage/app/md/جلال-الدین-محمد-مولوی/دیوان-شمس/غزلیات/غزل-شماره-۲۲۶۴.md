---
title: >-
    غزل شمارهٔ ۲۲۶۴
---
# غزل شمارهٔ ۲۲۶۴

<div class="b" id="bn1"><div class="m1"><p>بوسیسی افندیمو هم محسن و هم مه رو</p></div>
<div class="m2"><p>نیپو سر کینیکا چونم من و چونی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا نعم صباح ای جان مستند همه رندان</p></div>
<div class="m2"><p>تا شب همگان عریان با یار در آب جو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا قوم اتیناکم فی الحب فدیناکم</p></div>
<div class="m2"><p>مذ نحن رایناکم امنیتنا تصفوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جام دهی شادم دشنام دهی شادم</p></div>
<div class="m2"><p>افندی اوتی تیلس ثیلو که براکالو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مست شد این بنده بشنو تو پراکنده</p></div>
<div class="m2"><p>قویثز می کناکیمو سیمیر ابرالالو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا سیدتی هاتی من قهوه کاساتی</p></div>
<div class="m2"><p>من زارک من صحو ایاک و ایاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای فارس این میدان می‌گرد تو سرگردان</p></div>
<div class="m2"><p>آخر نه کم از چرخی در خدمت آن مه رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پویسی چلبی پویسی ای پوسه اغا پوسی</p></div>
<div class="m2"><p>بی‌نخوت و ناموسی این دم دل ما را جو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل چو بیاسودی در خواب کجا بودی</p></div>
<div class="m2"><p>اسکرت کما تدری من سکرک لا تصحو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واها سندی واها لما فتحت فاها</p></div>
<div class="m2"><p>ما اطیب سقیاها تحلوا ابدا تحلو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای چون نمکستانی اندر دل هر جانی</p></div>
<div class="m2"><p>هر صورت را ملحی از حسن تو ای مرجو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چیزی به تو می‌ماند هر صورت خوب ار نی</p></div>
<div class="m2"><p>از دیدن مرد و زن خالی کنمی پهلو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر خلق بخندندم ور دست ببندندم</p></div>
<div class="m2"><p>ور زجر پسندندم من می‌نروم زین کو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از مردم پژمرده دل می‌شود افسرده</p></div>
<div class="m2"><p>دارد سیهی در جان گر زرد بود مازو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بانگ تو کبوتر را در برج وصال آرد</p></div>
<div class="m2"><p>گر هست حجاب او صد برج و دو صد بارو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قوم خلقو بورا قالو شططا زورا</p></div>
<div class="m2"><p>فی وصفک یا مولی لا نسمع ما قالوا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این نفس ستیزه رو چون بزبچه بالاجو</p></div>
<div class="m2"><p>جز ریش ندارد او نامش چه کنم ریشو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خامش کن خامش کن از گفته فرامش کن</p></div>
<div class="m2"><p>هین بازمیا این سو آن سو پر چون تیهو</p></div></div>