---
title: >-
    غزل شمارهٔ ۱۶۴۷
---
# غزل شمارهٔ ۱۶۴۷

<div class="b" id="bn1"><div class="m1"><p>روز شادی است بیا تا همگان یار شویم</p></div>
<div class="m2"><p>دست با هم بدهیم و بر دلدار شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در او دنگ شویم و همه یک رنگ شویم</p></div>
<div class="m2"><p>همچنین رقص کنان جانب بازار شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز آن است که خوبان همه در رقص آیند</p></div>
<div class="m2"><p>ما ببندیم دکان‌ها همه بی‌کار شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز آن است که تشریف بپوشد جان‌ها</p></div>
<div class="m2"><p>ما به مهمان خدا بر سر اسرار شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز آن است که در باغ بتان خیمه زنند</p></div>
<div class="m2"><p>ما به نظاره ایشان سوی گلزار شویم</p></div></div>