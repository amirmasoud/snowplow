---
title: >-
    غزل شمارهٔ ۷۴۰
---
# غزل شمارهٔ ۷۴۰

<div class="b" id="bn1"><div class="m1"><p>مشک و عنبر گر ز مشک زلف یارم بو کند</p></div>
<div class="m2"><p>بوی خود را واهلد در حال و زلفش بو کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کافر و مؤمن گر از خوی خوشش واقف شوند</p></div>
<div class="m2"><p>خوی را خود واکند در حین و خو با او کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی ناگهان از روی او تابان شود</p></div>
<div class="m2"><p>پردها را بردرد وین کار را یک سو کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنگ تن‌ها را به دست روح‌ها زان داد حق</p></div>
<div class="m2"><p>تا بیان سر حق لایزالی او کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تارهای خشم و عشق و حقد و حاجت می‌زند</p></div>
<div class="m2"><p>تا ز هر یک بانگ دیگر در حوادث رو کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاد با چنگ تنی کز دست جان حق بستدش</p></div>
<div class="m2"><p>بر کنار خود نهاد و ساز آن را هو کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوستاد چنگ‌ها آن چنگ باشد در جهان</p></div>
<div class="m2"><p>وای آن چنگی که با آن چنگ حق پهلو کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز هم در چنگ حق تاریست بس پنهان و خوش</p></div>
<div class="m2"><p>کو به ناگه وصف آن دو نرگس جادو کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرگسان مست شمس الدین تبریزی که هست</p></div>
<div class="m2"><p>چشم آهو تا شکار شیر آن آهو کند</p></div></div>