---
title: >-
    غزل شمارهٔ ۴۷۴
---
# غزل شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>ز عشق روی تو روشن دل بنین و بنات</p></div>
<div class="m2"><p>بیا که از تو شود سیئاتهم حسنات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال تو چو درآید به سینه عاشق</p></div>
<div class="m2"><p>درون خانه تن پر شود چراغ حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دود به پیش خیالت خیال‌های دگر</p></div>
<div class="m2"><p>چنانک خاطر زندانیان به بانگ نجات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گرد سنبل تو جان‌ها چو مور و ملخ</p></div>
<div class="m2"><p>که تا ز خرمن لطفت برند جمله زکات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مرده‌ای نگری صد هزار زنده شود</p></div>
<div class="m2"><p>خنک کسی که از آن یک نظر بیافت برات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی شهی که شهان بر بساط شطرنجت</p></div>
<div class="m2"><p>به خانه خانه دوند از گریزخانه مات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام صبح که عشقت پیاله‌ای آرد</p></div>
<div class="m2"><p>ز خواب برجهد این بخت خفته گویدهات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرودود ز فلک مه به بوی این باده</p></div>
<div class="m2"><p>بگویدم که مرا نیز گویمش هیهات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طرب که از تو نباشد بیات می‌گردد</p></div>
<div class="m2"><p>بیار جام که جان آمدم ز عشق بیات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیش دیده من باش تا تو را بینم</p></div>
<div class="m2"><p>که سیر می‌نشود دیده من از آیات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندانم از سرمستیست شمس تبریزی</p></div>
<div class="m2"><p>که بر لبت زده‌ام بوسه‌ها و یا بر پات</p></div></div>