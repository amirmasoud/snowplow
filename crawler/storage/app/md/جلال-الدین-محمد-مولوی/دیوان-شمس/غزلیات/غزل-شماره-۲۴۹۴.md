---
title: >-
    غزل شمارهٔ ۲۴۹۴
---
# غزل شمارهٔ ۲۴۹۴

<div class="b" id="bn1"><div class="m1"><p>زرگر آفتاب را بسته گاز می‌کنی</p></div>
<div class="m2"><p>کرته شام را ز مه نقش و طراز می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب و نتایج این حبشی و روم را</p></div>
<div class="m2"><p>بر مثل اصولشان گرد و دراز می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه مجاز بنده را حق و حقیقتی دهی</p></div>
<div class="m2"><p>و آنک حقیقتی بود هزل و مجاز می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چه کرامت است ای نقش خیال روی او</p></div>
<div class="m2"><p>با درهای بسته در خانه جواز می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطر همچو باد را نقش جحود می‌دهی</p></div>
<div class="m2"><p>خاطر بی‌نیاز را پر ز نیاز می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شب ابرگین غم مشعله‌ها درآوری</p></div>
<div class="m2"><p>در دل تنگ پرگره پنجره باز می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما به دمشق عشق تو مست و مقیم بهر تو</p></div>
<div class="m2"><p>تو ز دلال و عز خود عزم عزاز می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه ز نیم زلتی برهمشان همی‌زنی</p></div>
<div class="m2"><p>گاه خود از کبیرها چشم فراز می‌کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه گدای راه را همت شاه می‌دهی</p></div>
<div class="m2"><p>گاه قباد و شاه را بنده آز می‌کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌شکنی به زیر پا نای طرب نوای را</p></div>
<div class="m2"><p>چنگ شکسته بسته را لایق ساز می‌کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بربط عشرت مرا گاه سه تا همی‌کنی</p></div>
<div class="m2"><p>پرده بوسلیک را گاه حجاز می‌کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان ز وجود جود تو آمد و مغز نغز شد</p></div>
<div class="m2"><p>باز ز پوست‌هاش چون همچو پیاز می‌کنی</p></div></div>