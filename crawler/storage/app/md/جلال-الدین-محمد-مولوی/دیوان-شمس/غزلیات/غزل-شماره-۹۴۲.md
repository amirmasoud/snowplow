---
title: >-
    غزل شمارهٔ ۹۴۲
---
# غزل شمارهٔ ۹۴۲

<div class="b" id="bn1"><div class="m1"><p>اگر مرا تو نخواهی دلم تو را خواهد</p></div>
<div class="m2"><p>تو هم به صلح گرایی اگر خدا خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار عاشق داری تو را به جان جویان</p></div>
<div class="m2"><p>که تا سعادت و دولت ز ما که را خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشق عاشق درویش خلق در عجبند</p></div>
<div class="m2"><p>که آنچ رشک شهانست او چرا خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب نباشد اگر مرده‌ای بجوید جان</p></div>
<div class="m2"><p>و یا گیاه بپژمرده‌ای صبا خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و یا دو دیده کور از خدا بصر جوید</p></div>
<div class="m2"><p>و یا گرسنه ده ساله‌ای نوا خواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه دعا شده‌ام من ز بس دعا کردن</p></div>
<div class="m2"><p>که هر که بیند رویم ز من دعا خواهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولی به چشم تو من رنگ کافران دارم</p></div>
<div class="m2"><p>که چشم خیره کشت بیندم غزا خواهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر مرا نکشد هجر تو ز من بحلست</p></div>
<div class="m2"><p>اسیر کشته ز غازی چه خونبها خواهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلام و خدمت کردم بگفتیم چونی</p></div>
<div class="m2"><p>چنان بود مس مسکین که کیمیا خواهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان برآید صورت که بست صورتگر</p></div>
<div class="m2"><p>چنان بود تن خسته کیش دوا خواهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آفتاب مزن گفت و گوی چون سایه</p></div>
<div class="m2"><p>ز سایه ذره گریزد همه ضیا خواهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی سخاوت و ایثار شمس تبریزی</p></div>
<div class="m2"><p>که شمس گنبد خضرا از او عطا خواهد</p></div></div>