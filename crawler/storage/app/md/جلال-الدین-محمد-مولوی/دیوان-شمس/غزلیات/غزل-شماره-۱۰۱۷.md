---
title: >-
    غزل شمارهٔ ۱۰۱۷
---
# غزل شمارهٔ ۱۰۱۷

<div class="b" id="bn1"><div class="m1"><p>آمد ترش رویی دگر یا زمهریر است او مگر</p></div>
<div class="m2"><p>برریز جامی بر سرش ای ساقی همچون شکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا می دهش از بلبله یا خود به راهش کن هله</p></div>
<div class="m2"><p>زیرا میان گلرخان خوش نیست عفریت ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درده می پیغامبری تا خر نماند در خری</p></div>
<div class="m2"><p>خر را بروید در زمان از باده عیسی دو پر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مجلس مستان دل هشیار اگر آید مهل</p></div>
<div class="m2"><p>دانی که مستان را بود در حال مستی خیر و شر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای پاسبان بر در نشین در مجلس ما ره مده</p></div>
<div class="m2"><p>جز عاشقی آتش دلی کآید از او بوی جگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دست خواهی پا دهد ور پای خواهی سر نهد</p></div>
<div class="m2"><p>ور بیل خواهی عاریت بر جای بیل آرد تبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا در شراب آغشته‌ام بی‌شرم و بی‌دل گشته‌ام</p></div>
<div class="m2"><p>اسپر سلامت نیستم در پیش تیغم چون سپر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهم یکی گوینده‌ای آب حیاتی زنده‌ای</p></div>
<div class="m2"><p>کآتش به خواب اندرزند وین پرده گوید تا سحر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر تن من گر رگی هشیار یابی بردرش</p></div>
<div class="m2"><p>چون شیرگیر حق نشد او را در این ره سگ شمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قومی خراب و مست و خوش قومی غلام پنج و شش</p></div>
<div class="m2"><p>آن‌ها جدا وین‌ها جدا آن‌ها دگر وین‌ها دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز اندازه بیرون خورده‌ام کاندازه را گم کرده‌ام</p></div>
<div class="m2"><p>شدوا یدی شدوا فمی هذا حفاظ ذی السکر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هین نیش ما را نوش کن افغان ما را گوش کن</p></div>
<div class="m2"><p>ما را چو خود بی‌هوش کن بی‌هوش سوی ما نگر</p></div></div>