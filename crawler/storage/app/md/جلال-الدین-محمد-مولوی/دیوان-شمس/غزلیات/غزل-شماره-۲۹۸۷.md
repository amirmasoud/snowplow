---
title: >-
    غزل شمارهٔ ۲۹۸۷
---
# غزل شمارهٔ ۲۹۸۷

<div class="b" id="bn1"><div class="m1"><p>ای جان و ای دو دیده بینا چگونه‌ای</p></div>
<div class="m2"><p>وی رشک ماه و گنبد مینا چگونه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ما و صد چو ما ز پی تو خراب و مست</p></div>
<div class="m2"><p>ما بی‌تو خسته‌ایم تو بی‌ما چگونه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن جا که با تو نیست چو سوراخ کژدم است</p></div>
<div class="m2"><p>و آن جا که جز تو نیست تو آن جا چگونه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان تو در گزینش جان‌ها چه می‌کنی</p></div>
<div class="m2"><p>وی گوهری فزوده ز دریا چگونه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مرغ عرش آمده در دام آب و گل</p></div>
<div class="m2"><p>در خون و خلط و بلغم و صفرا چگونه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان گلشن لطیف به گلخن فتاده‌ای</p></div>
<div class="m2"><p>با اهل گولخن به مواسا چگونه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای کوه قاف صبر و سکینه چه صابری</p></div>
<div class="m2"><p>وی عزلتی گرفته چو عنقا چگونه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم به توست قایم تو در چه عالمی</p></div>
<div class="m2"><p>تن‌ها به توست زنده تو تنها چگونه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای آفتاب از تو خجل در چه مشرقی</p></div>
<div class="m2"><p>وی زهر ناب با تو چو حلوا چگونه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیر و زبر شدیمت بی‌زیر و بی‌زبر</p></div>
<div class="m2"><p>ای درفکنده فتنه و غوغا چگونه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر غایبی ز دل تو در این دل چه می‌کنی</p></div>
<div class="m2"><p>ور در دلی ز دوده سودا چگونه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای شاه شمس مفخر تبریز بی‌نظیر</p></div>
<div class="m2"><p>در قاب قوس قرب و در ادنی چگونه‌ای</p></div></div>