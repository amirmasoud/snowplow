---
title: >-
    غزل شمارهٔ ۲۰۶۷
---
# غزل شمارهٔ ۲۰۶۷

<div class="b" id="bn1"><div class="m1"><p>ای هوس عشق تو کرده جهان را زبون</p></div>
<div class="m2"><p>خیره عشقت چو من این فلک سرنگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌در و می‌دوز تو می‌بر و می‌سوز تو</p></div>
<div class="m2"><p>خون کن و می‌شوی تو خون دلم را به خون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونک ز تو خاسته‌ست هر کژ تو راست است</p></div>
<div class="m2"><p>لیک بتا راست گو نیست مقام جنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش خیال نگار بعد بسی انتظار</p></div>
<div class="m2"><p>آمد و من در خمار یا رب چون بود چون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواست که پر وا کند روی به صحرا کند</p></div>
<div class="m2"><p>باز مرا می‌فریفت از سخن پرفسون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم والله که نی هیچ مساز این بنا</p></div>
<div class="m2"><p>گر عجمی رفت نیست ور عربی لایکون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل شب آمدی نیک عجب آمدی</p></div>
<div class="m2"><p>چون بر ما آمدی نیست رهایی کنون</p></div></div>