---
title: >-
    غزل شمارهٔ ۲۶۳۹
---
# غزل شمارهٔ ۲۶۳۹

<div class="b" id="bn1"><div class="m1"><p>برخیز که صبح است و صبوح است و سکاری</p></div>
<div class="m2"><p>بگشای کنار آمد آن یار کناری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز بیا دبدبه عمر ابد بین</p></div>
<div class="m2"><p>رستند و گذشتند ز دم‌های شماری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن رفت که اقبال بخارید سر ما</p></div>
<div class="m2"><p>ای دل سر اقبال از این بار تو خاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنجی تو عجب نیست که در توده خاکی</p></div>
<div class="m2"><p>ماهی تو عجب نیست که در گرد و غباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر حرم کعبه اقبال خرامید</p></div>
<div class="m2"><p>از بادیه ایمن شده وز ناز مکاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردان شده بین چرخ که صد ماه در او هست</p></div>
<div class="m2"><p>جز تابش یک روزه تو ای چرخ چه داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن ساغر جان که ملک الموت اجل شد</p></div>
<div class="m2"><p>نی شورش دل آرد و نی رنج خماری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس کن که اگر جان بخورد صورت ما را</p></div>
<div class="m2"><p>صد عذر بخواهد لبش از خوب عذاری</p></div></div>