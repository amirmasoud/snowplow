---
title: >-
    غزل شمارهٔ ۳۲۰۴
---
# غزل شمارهٔ ۳۲۰۴

<div class="b" id="bn1"><div class="m1"><p>یا ملک المبعث والمحشر</p></div>
<div class="m2"><p>لیس سوی صدرک من مصدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نبری ای سر، اگر سر بری</p></div>
<div class="m2"><p>آن ز خری دان که تو سر واخری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقلة عینی لک یا ناظری</p></div>
<div class="m2"><p>نظرة قلبی لک یا منظری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو پری، باش ز خلقان نهان</p></div>
<div class="m2"><p>بر نپری تا نشنوی چون پری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غاب فؤادی لم غیبته</p></div>
<div class="m2"><p>بعد حضوری لک، یا محضری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر خشکی چو ثقیلان مران</p></div>
<div class="m2"><p>برتر از آنی که روی برتری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منزلناالعرش و ما فوقه</p></div>
<div class="m2"><p>عمرک یا نفس قمی، سافری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله چو دردند به پایان خم</p></div>
<div class="m2"><p>سرور از آنی تو، که تو سروری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلت الا بدلنا سلما</p></div>
<div class="m2"><p>اسلمک الصبر قفی واصبری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند پس پرده و از در برون</p></div>
<div class="m2"><p>بر در این پرده، اگر بر دری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قالت هل صبری الا به</p></div>
<div class="m2"><p>هل عقدالبیع بلا مشتری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می مفروش از جهت حرص زر</p></div>
<div class="m2"><p>جوهر می خود بنماید زری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اذ حضرالراح فما فاتنا</p></div>
<div class="m2"><p>افتح عینیک به وابصری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می بفروشی، چه خری؟! جز که غم</p></div>
<div class="m2"><p>دین بفروشی چه بری؟! کافری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قر به‌العین کلی واشربی</p></div>
<div class="m2"><p>قد قرب‌امنزل فاستبشری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وصلت فانی ننماید بقا</p></div>
<div class="m2"><p>زن نشود حامله از سعتری</p></div></div>