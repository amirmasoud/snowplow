---
title: >-
    غزل شمارهٔ ۱۳۶۸
---
# غزل شمارهٔ ۱۳۶۸

<div class="b" id="bn1"><div class="m1"><p>عمرک یا واحدا فی درجات الکمال</p></div>
<div class="m2"><p>قد نزل الهم بی یا سندی قم تعال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا فرحی مونسی یا قمر المجلس</p></div>
<div class="m2"><p>وجهک بدر تمام ریقک خمر حلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روحک بحر الوفا لونک لمع الصفا</p></div>
<div class="m2"><p>عمرک لو لا التقی قلت ایا ذا الجلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تسکن قلب الوری تسکرهم بالهوی</p></div>
<div class="m2"><p>تدرک ما لا یری انت لطیف الخیال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تسکن ارواحهم تسکر اشباحهم</p></div>
<div class="m2"><p>تجلسهم مجلسا فیه کؤوس ثقال</p></div></div>