---
title: >-
    غزل شمارهٔ ۹۳۴
---
# غزل شمارهٔ ۹۳۴

<div class="b" id="bn1"><div class="m1"><p>میان باغ گل سرخ‌های و هو دارد</p></div>
<div class="m2"><p>که بو کنید دهان مرا چه بو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به باغ خود همه مستند لیک نی چون گل</p></div>
<div class="m2"><p>که هر یکی به قدح خورد و او سبو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سال سال نشاطست و روز روز طرب</p></div>
<div class="m2"><p>خنک مرا و کسی را که عیش خو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا مقیم نباشد چو ما به مجلس گل</p></div>
<div class="m2"><p>کسی که ساقی باقی ماه رو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار جان مقدس فدای آن جانی</p></div>
<div class="m2"><p>که او به مجلس ما امر اشربوا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سؤال کردم گل را که بر کی می‌خندی</p></div>
<div class="m2"><p>جواب داد بر آن زشت کو دو شو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار بار خزان کرد نوبهار تو را</p></div>
<div class="m2"><p>چه عشق دارد با ما چه جست و جو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیاله‌ای به من آورد گل که باده خوری</p></div>
<div class="m2"><p>خورم چرا نخورم بنده هم گلو دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه حاجتیست گلو باده خدایی را</p></div>
<div class="m2"><p>که ذره ذره همه نقل و می از او دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب که خار چه بدمست و تیز و روترشست</p></div>
<div class="m2"><p>ز رشک آنک گل و لاله صد عدو دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به طور موسی بنگر که از شراب گزاف</p></div>
<div class="m2"><p>دهان ندارد و اشکم چهارسو دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مستیان درختان نگر به فصل بهار</p></div>
<div class="m2"><p>شکوفه کرده که در شرب می غلو دارد</p></div></div>