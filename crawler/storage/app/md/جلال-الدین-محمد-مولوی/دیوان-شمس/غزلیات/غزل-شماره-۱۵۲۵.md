---
title: >-
    غزل شمارهٔ ۱۵۲۵
---
# غزل شمارهٔ ۱۵۲۵

<div class="b" id="bn1"><div class="m1"><p>ز قند یار تا شاخی نخایم</p></div>
<div class="m2"><p>نماز شام روزه کی گشایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی‌دانم کجا می روید آن قند</p></div>
<div class="m2"><p>کز او خوردم نمی‌دانم کجایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجایب آنک نقلش عقل من برد</p></div>
<div class="m2"><p>چو عقل نیست چونش می ستایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی دارد روزه همچون روزه من</p></div>
<div class="m2"><p>کز او هر لحظه عیدی می ربایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز صبح روی او دارم صبوحی</p></div>
<div class="m2"><p>نماز شام را هرگز نپایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گل در باغ حسنش خوش بخندم</p></div>
<div class="m2"><p>چو صبح از آفتابش خوش برآیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبانم از شراب او شکسته‌ست</p></div>
<div class="m2"><p>ز دستانش شکسته دست و پایم</p></div></div>