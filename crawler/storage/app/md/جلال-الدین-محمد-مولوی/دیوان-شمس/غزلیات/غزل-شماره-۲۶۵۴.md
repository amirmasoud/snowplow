---
title: >-
    غزل شمارهٔ ۲۶۵۴
---
# غزل شمارهٔ ۲۶۵۴

<div class="b" id="bn1"><div class="m1"><p>ببین این فتح ز استفتاح تا کی</p></div>
<div class="m2"><p>ز ساقی مست شو زین راح تا کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این اقداح صورت راح جانی است</p></div>
<div class="m2"><p>نظاره صورت اقداح تا کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مرغابی ز خود برساز کشتی</p></div>
<div class="m2"><p>صداع کشتی و ملاح تا کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو سباحی و از سباح زادی</p></div>
<div class="m2"><p>فسانه و باد هر سباح تا کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفخت فیه جان بخشی است هر صبح</p></div>
<div class="m2"><p>فراق فالق الاصباح تا کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو جان بالغان لوحی است محفوظ</p></div>
<div class="m2"><p>مثال کودکان ز الواح تا کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو فرموده‌ست رزقت ز آسمان است</p></div>
<div class="m2"><p>زمین شوریدن ای فلاح تا کی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن باغ است این سیب زنخدان</p></div>
<div class="m2"><p>قناعت بر یکی تفاح تا کی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جراحت راست دارو حسن یوسف</p></div>
<div class="m2"><p>دوا جستن ز هر جراح تا کی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هر جزوت چو مطرب می‌توان ساخت</p></div>
<div class="m2"><p>ز چشمت ساختن نواح تا کی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نفس واحدیم از خلق و از بعث</p></div>
<div class="m2"><p>جدا باشیدن ارواح تا کی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دهان بربند در دریا صدف وار</p></div>
<div class="m2"><p>دهان بگشاده چون تمساح تا کی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهان بربند و قفلی بر دهان نه</p></div>
<div class="m2"><p>ز ضایع کردن مفتاح تا کی</p></div></div>