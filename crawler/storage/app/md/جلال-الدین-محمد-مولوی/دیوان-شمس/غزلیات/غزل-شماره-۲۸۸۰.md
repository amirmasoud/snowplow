---
title: >-
    غزل شمارهٔ ۲۸۸۰
---
# غزل شمارهٔ ۲۸۸۰

<div class="b" id="bn1"><div class="m1"><p>به حق و حرمت آنک همگان را جانی</p></div>
<div class="m2"><p>قدحی پر کن از آنک صفتش می‌دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه را زیر و زبر کن نه زبر مان و نه زیر</p></div>
<div class="m2"><p>تا بدانند که امروز در این میدانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش باده بزن در بنه شرم و حیا</p></div>
<div class="m2"><p>دل مستان بگرفت از طرب پنهانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت آن شد که دل رفته به ما بازآری</p></div>
<div class="m2"><p>عقل‌ها را چو کبوتربچگان پرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکته می‌گویی در حلقه مستان خراب</p></div>
<div class="m2"><p>خوش بود گنج که درتابد در ویرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می جوشیده بر این سوختگان گردان کن</p></div>
<div class="m2"><p>پیش خامان بنه آن قلیه و آن بورانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه شدم من تو بگو هم که چه دانم شده‌ای</p></div>
<div class="m2"><p>کی بگوید لب تو حرف بدین آسانی</p></div></div>