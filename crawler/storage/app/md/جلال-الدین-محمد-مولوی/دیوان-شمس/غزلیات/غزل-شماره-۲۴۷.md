---
title: >-
    غزل شمارهٔ ۲۴۷
---
# غزل شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>بانگ تسبیح بشنو از بالا</p></div>
<div class="m2"><p>پس تو هم سبح اسمه الاعلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل و سنبل چرد دلت چون یافت</p></div>
<div class="m2"><p>مرغزاری که اخرج المرعی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یعلم الجهر نقش این آهوست</p></div>
<div class="m2"><p>ناف مشکین او و مایخفی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس آهوان او چو رسید</p></div>
<div class="m2"><p>روح را سوی مرغزار هدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنه را کی بود فراموشی</p></div>
<div class="m2"><p>چون سنقرئک فلا تنسی</p></div></div>