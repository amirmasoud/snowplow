---
title: >-
    غزل شمارهٔ ۹۴۱
---
# غزل شمارهٔ ۹۴۱

<div class="b" id="bn1"><div class="m1"><p>ز بعد خاک شدن یا زیان بود یا سود</p></div>
<div class="m2"><p>به نقد خاک شوم بنگرم چه خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نقد خاک شدن کار عاشقان باشد</p></div>
<div class="m2"><p>که راه بند شکستن خدایشان بنمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به امر موتوا من قبل ان تموتوا ما</p></div>
<div class="m2"><p>کنیم همچو محمد غزای نفس جهود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهود و مشرک و ترسا نتیجه نفس است</p></div>
<div class="m2"><p>ز پشک باشد دود خبیث نی از عود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود دمی همه خاک و شود دمی همه آب</p></div>
<div class="m2"><p>شود دمی همه آتش شود دمی همه دود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود دمی همه یار و شود دمی همه غار</p></div>
<div class="m2"><p>شود دمی همه تار و شود دمی همه پود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیش خلق نشسته هزار نقش شود</p></div>
<div class="m2"><p>ولیک در نظر تو نه کم شود نه فزود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش چشم محمد بهشت و دوزخ عین</p></div>
<div class="m2"><p>به پیش چشم دگر کس مستر و مغمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مذللست قطوف بهشت بر احمد</p></div>
<div class="m2"><p>که کرد دست دراز و از آن بخواست ربود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که تا دهد به صحابه ولیک آن بگداخت</p></div>
<div class="m2"><p>شد آب در کفش ایرا نبود وقت نمود</p></div></div>