---
title: >-
    غزل شمارهٔ ۱۹۹۱
---
# غزل شمارهٔ ۱۹۹۱

<div class="b" id="bn1"><div class="m1"><p>همه خوردند و بخفتند و تهی گشت وطن</p></div>
<div class="m2"><p>وقت آن شد که درآییم خرامان به چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه خوردند و برفتند بقای ما باد</p></div>
<div class="m2"><p>که دل و جان زمانیم و سپهدار زمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تویی آب حیاتی کی نماند باقی</p></div>
<div class="m2"><p>چو تو باشی بت زیبا همه گردند شمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کتب العشق علینا غمرات و محن</p></div>
<div class="m2"><p>و قضی الحجب علینا فتنا بعد فتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرج آمد برهیدیم ز تشویش جهان</p></div>
<div class="m2"><p>بپرد جان مجرد به گلستان منن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناقتی نخ هنا فهو مناخ حسن</p></div>
<div class="m2"><p>فیه ماء و سخاء و رخاء و عطن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یرزقون فرحین بخوریم آن می و نقل</p></div>
<div class="m2"><p>مقعد صدق چو شد منزل عشاق سکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن سیب کشانیم سوی شفتالو</p></div>
<div class="m2"><p>ببریم از گل تر چند سخن سوی سمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مرا می بدهی هیچ مجو شرط ادب</p></div>
<div class="m2"><p>مست را حد نزند شرع مرا نیز مزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ادب و بی‌ادبی نیست به دستم چه کنم</p></div>
<div class="m2"><p>چو شتر می کشدم مست شتربان به رسن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلبل از عشق ز گل بوسه طمع کرد و بگفت</p></div>
<div class="m2"><p>بشکن شاخ نبات و دل ما را مشکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت گل راز من اندرخور طفلان نبود</p></div>
<div class="m2"><p>بچه را ابجد و هوز به و حطی کلمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت گر می ندهی بوسه بده باده عشق</p></div>
<div class="m2"><p>گفت این هم ندهم باش حزین جفت حزن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت من نیز تو را بر دف و بربط بزنم</p></div>
<div class="m2"><p>تنن تن تننن تن تننن تن تننن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت شب طشت مزن که همه بیدار شوند</p></div>
<div class="m2"><p>که مگر ماه گرفته‌ست مجو شور و فتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طشت اگر من نزنم فتنه چو نه ماهه شده‌ست</p></div>
<div class="m2"><p>فتنه‌ها زاید ناچار شب آبستن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برگ می لرزد بر شاخ و دلم می لرزد</p></div>
<div class="m2"><p>لرزه برگ ز باد و دلم از خوب ختن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تاب رخسار گل و لاله خبر می دهدم</p></div>
<div class="m2"><p>که چراغی است نهان گشته در این زیر لگن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهد کن تا لگن جهل ز دل برداری</p></div>
<div class="m2"><p>تا که از مشرق جان صبح برآید روشن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمس تبریز طلوعی کن از مشرق روح</p></div>
<div class="m2"><p>که چو خورشید تو جانی و جهان جمله بدن</p></div></div>