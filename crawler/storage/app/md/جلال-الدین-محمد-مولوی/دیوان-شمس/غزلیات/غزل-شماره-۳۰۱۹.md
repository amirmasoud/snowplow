---
title: >-
    غزل شمارهٔ ۳۰۱۹
---
# غزل شمارهٔ ۳۰۱۹

<div class="b" id="bn1"><div class="m1"><p>ای که تو عشاق را همچو شکر می‌کشی</p></div>
<div class="m2"><p>جان مرا خوش بکش این نفس ار می‌کشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتن شیرین و خوش خاصیت دست توست</p></div>
<div class="m2"><p>زانک نظرخواه را تو به نظر می‌کشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سحری مستمر منتظرم منتظر</p></div>
<div class="m2"><p>زانک مرا بیشتر وقت سحر می‌کشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جور تو ما را چو قند راه مدد درمبند</p></div>
<div class="m2"><p>نی که مرا عاقبت بر سر در می‌کشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دم تو بی‌شکم ای غم تو دفع غم</p></div>
<div class="m2"><p>ای که تو ما را به دام همچو شرر می‌کشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دم دفعی دگر پیش کنی چون سپر</p></div>
<div class="m2"><p>تیغ رها کرده‌ای تو به سپر می‌کشی</p></div></div>