---
title: >-
    غزل شمارهٔ ۳۱۰
---
# غزل شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>بازآمد آن مهی که ندیدش فلک به خواب</p></div>
<div class="m2"><p>آورد آتشی که نمیرد به هیچ آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر به خانه تن و بنگر به جان من</p></div>
<div class="m2"><p>از جام عشق او شده این مست و آن خراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میر شرابخانه چو شد با دلم حریف</p></div>
<div class="m2"><p>خونم شراب گشت ز عشق و دلم کباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دیده پر شود ز خیالش ندا رسد</p></div>
<div class="m2"><p>احسنت ای پیاله و شاباش ای شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریای عشق را دل من دید ناگهان</p></div>
<div class="m2"><p>از من بجست در وی و گفتا مرا بیاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشیدروی مفخر تبریز شمس دین</p></div>
<div class="m2"><p>اندر پیش دوان شده دل‌های چون سحاب</p></div></div>