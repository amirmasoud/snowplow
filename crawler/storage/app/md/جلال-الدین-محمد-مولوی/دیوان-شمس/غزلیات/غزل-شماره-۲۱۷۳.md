---
title: >-
    غزل شمارهٔ ۲۱۷۳
---
# غزل شمارهٔ ۲۱۷۳

<div class="b" id="bn1"><div class="m1"><p>چنگ خردم بگسل تاری من و تاری تو</p></div>
<div class="m2"><p>هین نوبت دل می‌زن باری من و باری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وحدت مشتاقی ما جمله یکی باشیم</p></div>
<div class="m2"><p>اما چو به گفت آییم یاری من و یاری تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون احمد و بوبکریم در کنج یکی غاری</p></div>
<div class="m2"><p>زیرا که دوی باشد غاری من و غاری تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عالم خارستان بسیار سفر کردم</p></div>
<div class="m2"><p>اکنون بکش از پایم خاری من و خاری تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرمست بخسپ ای دل در ظل مسیح خود</p></div>
<div class="m2"><p>آن رفت که می‌بودیم زاری من و زاری تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من غرقه شدم در زر تو سجده کنان ای سر</p></div>
<div class="m2"><p>بی‌کار نمی‌شاید کاری من و کاری تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس که مرا جوید در کوی تو باید جست</p></div>
<div class="m2"><p>گر لیلی و مجنون است باری من و باری تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دزدی که رهی می‌زد هنگام سیاست شد</p></div>
<div class="m2"><p>اکنون بزنیم او را داری من و داری تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاموش که خاموشی فخری من و فخری تو</p></div>
<div class="m2"><p>در گفتن و بی‌صبری عاری من و عاری تو</p></div></div>