---
title: >-
    غزل شمارهٔ ۹۰
---
# غزل شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای شاد که ما هستیم اندر غم تو جانا</p></div>
<div class="m2"><p>هم محرم عشق تو هم محرم تو جانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم ناظر روی تو هم مست سبوی تو</p></div>
<div class="m2"><p>هم شسته به نظاره بر طارم تو جانا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو جان سلیمانی آرامگه جانی</p></div>
<div class="m2"><p>ای دیو و پری شیدا از خاتم تو جانا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بیخودی جان‌ها در طلعت خوب تو</p></div>
<div class="m2"><p>ای روشنی دل‌ها اندر دم تو جانا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق تو خمارم در سر ز تو می دارم</p></div>
<div class="m2"><p>از حسن جمالات پرخرم تو جانا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو کعبه عشاقی شمس الحق تبریزی</p></div>
<div class="m2"><p>زمزم شکر آمیزد از زمزم تو جانا</p></div></div>