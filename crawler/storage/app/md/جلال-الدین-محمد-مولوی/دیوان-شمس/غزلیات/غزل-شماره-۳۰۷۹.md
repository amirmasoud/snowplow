---
title: >-
    غزل شمارهٔ ۳۰۷۹
---
# غزل شمارهٔ ۳۰۷۹

<div class="b" id="bn1"><div class="m1"><p>بیامدیم دگربار سوی مولایی</p></div>
<div class="m2"><p>که تا به زانوی او نیست هیچ دریایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار عقل ببندی به هم بدو نرسد</p></div>
<div class="m2"><p>کجا رسد به مه چرخ دست یا پایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک به طمع گلو را دراز کرد بدو</p></div>
<div class="m2"><p>نیافت بوسه ولیکن چشید حلوایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار حلق و گلو شد دراز سوی لبش</p></div>
<div class="m2"><p>که ریز بر سر ما نیز من و سلوایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیامدیم دگربار سوی معشوقی</p></div>
<div class="m2"><p>که می‌رسید به گوش از هواش هیهایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیامدیم دگربار سوی آن حرمی</p></div>
<div class="m2"><p>که فرق سجده کنش هست آسمان سایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیامدیم دگربار سوی آن چمنی</p></div>
<div class="m2"><p>که هست بلبل او را غلام عنقایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیامدیم بدو کو جدا نبود از ما</p></div>
<div class="m2"><p>که مشک پر نشود بی‌وجود سقایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همیشه مشک بچفسیده بر تن سقا</p></div>
<div class="m2"><p>که نیست بی‌تو مرا دست و دانش و رایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیامدیم دگربار سوی آن بزمی</p></div>
<div class="m2"><p>که شد ز نقل خوشش کام نیشکرخایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیامدیم دگربار سوی آن چرخی</p></div>
<div class="m2"><p>که جان چو رعد زند در خمش علالایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیامدیم دگربار سوی آن عشقی</p></div>
<div class="m2"><p>که دیو گشت ز آسیب او پری زایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خموش زیر زبان ختم کن تو باقی را</p></div>
<div class="m2"><p>که هست بر تو موکل غیور لالایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حدیث مفخر تبریز شمس دین کم گو</p></div>
<div class="m2"><p>که نیست درخور آن گفت عقل گویایی</p></div></div>