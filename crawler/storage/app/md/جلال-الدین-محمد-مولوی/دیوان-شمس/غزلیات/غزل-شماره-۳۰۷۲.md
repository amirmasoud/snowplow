---
title: >-
    غزل شمارهٔ ۳۰۷۲
---
# غزل شمارهٔ ۳۰۷۲

<div class="b" id="bn1"><div class="m1"><p>به من نگر که به جز من به هر کی درنگری</p></div>
<div class="m2"><p>یقین شود که ز عشق خدای بی‌خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان رخی بنگر که کو نمک ز حق دارد</p></div>
<div class="m2"><p>بود که ناگه از آن رخ تو دولتی ببری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را چو عقل پدر بوده‌ست و تن مادر</p></div>
<div class="m2"><p>جمال روی پدر درنگر اگر پسری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدانک پیر سراسر صفات حق باشد</p></div>
<div class="m2"><p>وگر چه پیر نماید به صورت بشری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پیش تو چو کفست و به وصف خود دریا</p></div>
<div class="m2"><p>به چشم خلق مقیمست و هر دم او سفری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز مشکل مانده‌ست حال پیر تو را</p></div>
<div class="m2"><p>هزار آیت کبری در او چه بی‌هنری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسید صورت روحانیی به مریم دل</p></div>
<div class="m2"><p>ز بارگاه منزه ز خشکی و ز تری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن نفس که در او سر روح پنهان شد</p></div>
<div class="m2"><p>بکرد حامله دل را رسول رهگذری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایا دلی که تو حامل شدی از آن خسرو</p></div>
<div class="m2"><p>به وقت جنبش آن حمل تا در او نگری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو حمل صورت گیرد ز شمس تبریزی</p></div>
<div class="m2"><p>چو دل شوی تو و چون دل به سوی غیب پری</p></div></div>