---
title: >-
    غزل شمارهٔ ۲۳۰۵
---
# غزل شمارهٔ ۲۳۰۵

<div class="b" id="bn1"><div class="m1"><p>کی باشد من با تو باده به گرو خورده</p></div>
<div class="m2"><p>تو برده و من مانده من خرقه گرو کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در می شده من غرقه چون ساغر و چون کوزه</p></div>
<div class="m2"><p>با یار درافتاده بی‌حاجب و بی‌پرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد نوش تو نوشیده تشریف تو پوشیده</p></div>
<div class="m2"><p>صد جوش بجوشیده این عالم افسرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نور تو روشن دل چون ماه ز نور خور</p></div>
<div class="m2"><p>وز بوی گلت خوشدل چون روغن پرورده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خود چه فسون گفتی با گل که شد او خندان</p></div>
<div class="m2"><p>تا خود چه جفا گفتی با خارک پژمرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک لحظه بخندانی یک لحظه بگریانی</p></div>
<div class="m2"><p>ای نادره صنعت‌ها در صنع درآورده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقل ز تو نازارد زان روی که زشت آید</p></div>
<div class="m2"><p>ظلمت ز مه آشفته خاری ز گل آزرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس غصه رسول آمد از منعم و می‌گوید</p></div>
<div class="m2"><p>ده مرده شکر خوردی بگذار یکی مرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس فکر چو بحر آمد حکمت مثل ماهی</p></div>
<div class="m2"><p>در فکر سخن زنده در گفت سخن مرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نی فکر چو دام آمد دریا پس این دام است</p></div>
<div class="m2"><p>در دام کجا گنجد جز ماهی بشمرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس دل چو بهشتی دان گفتار زبان دوزخ</p></div>
<div class="m2"><p>وین فکر چو اعرافی جای گنه و خرده</p></div></div>