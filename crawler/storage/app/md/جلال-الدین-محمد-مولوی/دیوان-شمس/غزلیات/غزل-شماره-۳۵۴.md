---
title: >-
    غزل شمارهٔ ۳۵۴
---
# غزل شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>ز میخانه دگربار این چه بویست</p></div>
<div class="m2"><p>دگربار این چه شور و گفت و گویست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان بگرفت ارواح مجرد</p></div>
<div class="m2"><p>زمین و آسمان پرهای و هوی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا ای عشق این می از چه خمست</p></div>
<div class="m2"><p>اشارت کن خرابات از چه سوی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه می‌گویم اشارت چیست کاین جا</p></div>
<div class="m2"><p>نگنجد فکرتی کان همچو مویست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاید در نظر آن سر یک تو</p></div>
<div class="m2"><p>که در فکر آنچ آید چارتویست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ز اندیشه به گفت آید چه گویم</p></div>
<div class="m2"><p>که خانه کنده و رسوای کویست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز رسوایی به بحر دل رود باز</p></div>
<div class="m2"><p>که دل بحرست و گفتن‌ها چو جویست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خزینه دار گوهر بحر بدخوست</p></div>
<div class="m2"><p>که آب جو و چه تن جامه شویست</p></div></div>