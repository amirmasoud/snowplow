---
title: >-
    غزل شمارهٔ ۱۱۰۰
---
# غزل شمارهٔ ۱۱۰۰

<div class="b" id="bn1"><div class="m1"><p>بس که می‌انگیخت آن مه شور و شر</p></div>
<div class="m2"><p>بس که می‌کرد او جهان زیر و زبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر زبان را طاقت شرحش نماند</p></div>
<div class="m2"><p>خیره گشته همچنین می‌کرد سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بسا سر همچنین جنبان شده</p></div>
<div class="m2"><p>با دهان خشک و با چشمان تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دو چشمش بین خیال یار ما</p></div>
<div class="m2"><p>رقص رقصان در سواد آن بصر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به سر گویم حدیثش بعد از این</p></div>
<div class="m2"><p>من زبان بستم ز گفتن ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش او رو ای نسیم نرم رو</p></div>
<div class="m2"><p>پیش او بنشین به رویش درنگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیز تیزش بنگر ای باد صبا</p></div>
<div class="m2"><p>چشم و دل را پر کن از خوبی و فر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور ببینی یار ما را روترش</p></div>
<div class="m2"><p>پرده‌ای باشد ز غیرت در نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مو نباشد عکس مو باشد در آب</p></div>
<div class="m2"><p>صورتی باشد ترش اندر شکر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توبه کردم از سخن این باز چیست</p></div>
<div class="m2"><p>توبه نبود عاشقانش را مگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توبه شیشه عشق او چون گازرست</p></div>
<div class="m2"><p>پیش گازر چیست کار شیشه گر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بشکنم شیشه بریزم زیر پای</p></div>
<div class="m2"><p>تا خلد در پای مرد بی‌خبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شحنه یار ماست هر کو خسته شد</p></div>
<div class="m2"><p>گو مرا بسته به پیش شحنه بر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شحنه را چاه زنخ زندان ماست</p></div>
<div class="m2"><p>تا نهم زنجیر زلفش پای بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بند و زندان خوش ای زنده دلان</p></div>
<div class="m2"><p>خوش مرا عیشیست آن جا معتبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چه می‌کاهم چو ماه از عشق او</p></div>
<div class="m2"><p>گر چه می‌گردم چه گردون بر قمر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بعد من صد سال دیگر این غزل</p></div>
<div class="m2"><p>چون جمال یوسفی باشد سمر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زانک دل هرگز نپوسد زیر خاک</p></div>
<div class="m2"><p>این ز دل گفتم نگفتم از جگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من چو داوودم شما مرغان پاک</p></div>
<div class="m2"><p>وین غزل‌ها چون زبور مستطر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای خدایا پر این مرغان مریز</p></div>
<div class="m2"><p>چون به داوودند از جان یارگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای خدایا دست بر لب می‌نهم</p></div>
<div class="m2"><p>تا نگویم زان چه گشتم مستتر</p></div></div>