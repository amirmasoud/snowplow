---
title: >-
    غزل شمارهٔ ۱۲۱۲
---
# غزل شمارهٔ ۱۲۱۲

<div class="b" id="bn1"><div class="m1"><p>دست بنه بر دلم از غم دلبر مپرس</p></div>
<div class="m2"><p>چشم من اندرنگر از می و ساغر مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوشش خون را ببین از جگر مؤمنان</p></div>
<div class="m2"><p>وز ستم و ظلم آن طره کافر مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سکه شاهی ببین در رخ همچون زرم</p></div>
<div class="m2"><p>نقش تمامی بخوان پس تو ز زرگر مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق چو لشکر کشید عالم جان را گرفت</p></div>
<div class="m2"><p>حال من از عشق پرس از من مضطر مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست دل عاشقان همچو دل مرغ از او</p></div>
<div class="m2"><p>جز سخن عاشقی نکته دیگر مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاصیت مرغ چیست آنک ز روزن پرد</p></div>
<div class="m2"><p>گر تو چو مرغی بیا برپر و از در مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون پدر و مادر عاشق هم عشق اوست</p></div>
<div class="m2"><p>بیش مگو از پدر بیش ز مادر مپرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست دل عاشقان همچو تنوری به تاب</p></div>
<div class="m2"><p>چون به تنور آمدی جز که ز آذر مپرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ دل تو اگر عاشق این آتشست</p></div>
<div class="m2"><p>سوخته پر خوشتری هیچ تو از پر مپرس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر تو و دلدار سر هر دو یکی کرده ایت</p></div>
<div class="m2"><p>پای دگر کژ منه خواجه از این سر مپرس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده و گوش بشر دان که همه پرگلست</p></div>
<div class="m2"><p>از بصر پروحل گوهر منظر مپرس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونک بشستی بصر از مدد خون دل</p></div>
<div class="m2"><p>مجلس شاهی تو راست جز می احمر مپرس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رو تو به تبریز زود از پی این شکر را</p></div>
<div class="m2"><p>با لطف شمس حق از می و شکر مپرس</p></div></div>