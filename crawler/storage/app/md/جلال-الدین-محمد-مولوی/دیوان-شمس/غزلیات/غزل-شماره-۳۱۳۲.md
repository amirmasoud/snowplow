---
title: >-
    غزل شمارهٔ ۳۱۳۲
---
# غزل شمارهٔ ۳۱۳۲

<div class="b" id="bn1"><div class="m1"><p>خواهیم یارا کامشب نخسپی</p></div>
<div class="m2"><p>حق خدا را کامشب نخسپی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سرو و سوسن تا روز روشن</p></div>
<div class="m2"><p>خوبیم و زیبا کامشب نخسپی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار موافق تا صبح صادق</p></div>
<div class="m2"><p>شاهی و مولا کامشب نخسپی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ماه پاره همچون ستاره</p></div>
<div class="m2"><p>باشی به بالا کامشب نخسپی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حسن رویت و از لطف مویت</p></div>
<div class="m2"><p>خواهد ثریا کامشب نخسپی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دید ما را مست تو یارا</p></div>
<div class="m2"><p>نالید سرنا کامشب نخسپی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون روز لالا دارد علالا</p></div>
<div class="m2"><p>کوری لالا کامشب نخسپی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جمع مستان با زیردستان</p></div>
<div class="m2"><p>بگریست صهبا کامشب نخسپی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قومی ز خویشان گشته پریشان</p></div>
<div class="m2"><p>بهر تو تنها کامشب نخسپی</p></div></div>