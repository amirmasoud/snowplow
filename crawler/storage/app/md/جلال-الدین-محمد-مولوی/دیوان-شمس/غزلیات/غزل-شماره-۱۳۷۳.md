---
title: >-
    غزل شمارهٔ ۱۳۷۳
---
# غزل شمارهٔ ۱۳۷۳

<div class="b" id="bn1"><div class="m1"><p>هان ای طبیب عاشقان دستی فروکش بر برم</p></div>
<div class="m2"><p>تا بخت و رخت و تخت خود بر عرش و کرسی بر برم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر گردن و بر دست من بربند آن زنجیر را</p></div>
<div class="m2"><p>افسون مخوان ز افسون تو هر روز دیوانه ترم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهم که بدهم گنج زر تا آن گواه دل بود</p></div>
<div class="m2"><p>گر چه گواهی می‌دهد رخسارهٔ همچون زرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور تو گواهان مرا رد می کنی ای پرجفا</p></div>
<div class="m2"><p>ای قاضی شیرین قضا باری فروخوان محضرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌لطف و دلداری تو یا رب چه می لرزد دلم</p></div>
<div class="m2"><p>در شوق خاک پای تو یا رب چه می گردد سرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیشم نشین پیشم نشان ای جان جان جان جان</p></div>
<div class="m2"><p>پر کن دلم گر کشتیم بیخم ببر گر لنگرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه در طواف آتشم گه در شکاف آتشم</p></div>
<div class="m2"><p>باد آهن دل سرخ رو از دمگه آهنگرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر روز نو جامی دهد تسکین و آرامی دهد</p></div>
<div class="m2"><p>هر روز پیغامی دهد این عشق چون پیغامبرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سایه‌ات تا آمدم چون آفتابم بر فلک</p></div>
<div class="m2"><p>تا عشق را بنده شدم خاقان و سلطان سنجرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای عشق آخر چند من وصف تو گویم بی‌دهن</p></div>
<div class="m2"><p>گه بلبلم گه گلبنم گه خضرم و گه اخضرم</p></div></div>