---
title: >-
    غزل شمارهٔ ۲۵۳۲
---
# غزل شمارهٔ ۲۵۳۲

<div class="b" id="bn1"><div class="m1"><p>کی افسون خواند در گوشت که ابرو پرگره داری</p></div>
<div class="m2"><p>نگفتم با کسی منشین که باشد از طرب عاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی پرزهر افسونی فروخواند به گوش تو</p></div>
<div class="m2"><p>ز صحن سینه پرغم دهد پیغام بیماری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دیدی آن ترش رو را مخلل کرده ابرو را</p></div>
<div class="m2"><p>از او بگریز و بشناسش چرا موقوف گفتاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه حاجت آب دریا را چشش چون رنگ او دیدی</p></div>
<div class="m2"><p>که پرزهرت کند آبش اگر چه نوش منقاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطیفان و ظریفانی که بودستند در عالم</p></div>
<div class="m2"><p>رمیده و بدگمان بودند همچون کبک کهساری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر استفراغ می‌خواهی از آن طزغوی گندیده</p></div>
<div class="m2"><p>مفرح بدهمت لیکن مکن دیگر وحل خواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الا یا صاحب الدار ادر کأسا من النار</p></div>
<div class="m2"><p>فدفینی و صفینی و صفو عینک الجاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فطفینا و عزینا فان عدنا فجازینا</p></div>
<div class="m2"><p>فانا مسنا ضر فلا ترضی باضراری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ادر کأسا عهدناه فانا ما جحدناه</p></div>
<div class="m2"><p>فعندی منه آثار و انی مدرک ثاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ادر کأسا باجفانی فدا روحی و ریحانی</p></div>
<div class="m2"><p>و انت المحشر الثانی فاحیینا بمدرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فاوقد لی مصابیحی و ناولنی مفاتیحی</p></div>
<div class="m2"><p>و غیرنی و سیرنی بجود کفک الساری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو نامت پارسی گویم کند تازی مرا لابه</p></div>
<div class="m2"><p>چو تازی وصف تو گویم برآرد پارسی زاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگه امروز زنجیری دگر در گردنم کردی</p></div>
<div class="m2"><p>زهی طوق و زهی منصب که هست آن سلسله داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو زنجیری نهی بر سگ شود شاه همه شیران</p></div>
<div class="m2"><p>چو زنگی را دهی رنگی شود رومی و روم آری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الا یا صاحب الکاس و یا من قلبه قاسی</p></div>
<div class="m2"><p>اتبلینی بافلاسی و تعلینی باکثاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لسان العرب و الترک هما فی کاسک المر</p></div>
<div class="m2"><p>فناول قهوه تغنی من اعساری و ایساری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر شاه عرب را من بدیدم دوش خواب اندر</p></div>
<div class="m2"><p>چه جای خواب می‌بینم جمالش را به بیداری</p></div></div>