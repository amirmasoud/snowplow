---
title: >-
    غزل شمارهٔ ۱۶۴۸
---
# غزل شمارهٔ ۱۶۴۸

<div class="b" id="bn1"><div class="m1"><p>ساقیا عربده کردیم که در جنگ شویم</p></div>
<div class="m2"><p>می گلرنگ بده تا همه یک رنگ شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت لطف سقی الله تویی در دو جهان</p></div>
<div class="m2"><p>رخ می رنگ نما تا همگان دنگ شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده منسوخ شود چون به صفت باده شویم</p></div>
<div class="m2"><p>بنگ منسوخ شود چون همگی بنگ شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هین که اندیشه و غم پهلوی ما خانه گرفت</p></div>
<div class="m2"><p>باده ده تا که از او ما به دو فرسنگ شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطربا بهر خدا زخمه مستانه بزن</p></div>
<div class="m2"><p>تا ز زخمه خوش تو ساخته چون چنگ شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجلس قیصر روم است بده صیقل دل</p></div>
<div class="m2"><p>تا که چون آینه جان همه بی‌رنگ شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک جهان تنگ دل و ما ز فراخی نشاط</p></div>
<div class="m2"><p>یک نفس عاشق آنیم که دلتنگ شویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشمن عقل کی دیده‌ست کز آمیزش او</p></div>
<div class="m2"><p>همه عقل و همه علم و همه فرهنگ شویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریز چو در باغ صفا رو بنمود</p></div>
<div class="m2"><p>زود در گردن عشقش همه آونگ شویم</p></div></div>