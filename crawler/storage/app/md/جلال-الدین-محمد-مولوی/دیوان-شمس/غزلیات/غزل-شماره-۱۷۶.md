---
title: >-
    غزل شمارهٔ ۱۷۶
---
# غزل شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>روح زیتونیست عاشق نار را</p></div>
<div class="m2"><p>نار می‌جوید چو عاشق یار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح زیتونی بیفزا ای چراغ</p></div>
<div class="m2"><p>ای معطل کرده دست افزار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان شهوانی که از شهوت زهد</p></div>
<div class="m2"><p>دل ندارد دیدن دلدار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس به علت دوست دارد دوست را</p></div>
<div class="m2"><p>بر امید خلد و خوف نار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شکستی جان ناری را ببین</p></div>
<div class="m2"><p>در پی او جان پرانوار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نبودی جان اخوان پس جهود</p></div>
<div class="m2"><p>کی جدا کردی دو نیکوکار را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان شهوت جان اخوان دان از آنک</p></div>
<div class="m2"><p>نار بیند نور موسی وار را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان شهوانی‌ست از بی‌حکمتی</p></div>
<div class="m2"><p>یاوه کرده نطق طوطی وار را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت بیمار و زبان تو گرفت</p></div>
<div class="m2"><p>روی سوی قبله کن بیمار را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قبله شمس الدین تبریزی بود</p></div>
<div class="m2"><p>نور دیده مر دل و دیدار را</p></div></div>