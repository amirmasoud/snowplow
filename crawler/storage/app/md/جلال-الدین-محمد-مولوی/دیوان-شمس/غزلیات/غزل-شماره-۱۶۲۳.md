---
title: >-
    غزل شمارهٔ ۱۶۲۳
---
# غزل شمارهٔ ۱۶۲۳

<div class="b" id="bn1"><div class="m1"><p>هذیان که گفت دشمن به درون دل شنیدم</p></div>
<div class="m2"><p>پی من تصوری را که بکرد هم بدیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سگ او گزید پایم بنمود بس جفایم</p></div>
<div class="m2"><p>نگزم چو سگ من او را لب خویش را گزیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو به رازهای فردان برسیده‌ام چو مردان</p></div>
<div class="m2"><p>چه بدین تفاخر آرم که به راز او رسیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه عیب از من آمد که ز من چنین فن آمد</p></div>
<div class="m2"><p>که به قصد کزدمی را سوی پای خود کشیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بلیس کو ز آدم بندید جز که نقشی</p></div>
<div class="m2"><p>من از این بلیس ناکس به خدا که نابدیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برسان به همدمانم که من از چه روگرانم</p></div>
<div class="m2"><p>چو گزید مار رانم ز سیه رسن رمیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خمشان بس خجسته لب و چشم برببسته</p></div>
<div class="m2"><p>ز رهی که کس نداند به ضمیرشان دویدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ز دل به جانب دل ره خفیه است و کامل</p></div>
<div class="m2"><p>ز خزینه‌های دل‌ها زر و نقره برگزیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ضمیر همچو گلخن سگ مرده درفکندم</p></div>
<div class="m2"><p>ز ضمیر همچو گلشن گل و یاسمن بچیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بد و نیک دوستان را به کنایت ار بگفتم</p></div>
<div class="m2"><p>به بهینه پرده آن را چو نساج برتنیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دلم رسید ناگه به دلی عظیم و آگه</p></div>
<div class="m2"><p>ز مهابت دل او به مثال دل طپیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو به حال خویش شادی تو به من کجا فتادی</p></div>
<div class="m2"><p>پس کار خویشتن رو که نه شیخ و نه مریدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سوی تو ای برادر نه مسم نه زر سرخم</p></div>
<div class="m2"><p>ز در خودم برون ران که نه قفل و نه کلیدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو بگیر آن چنانک بنگفتم این سخن هم</p></div>
<div class="m2"><p>اگرم به یاد بودی به خدا نمی‌چخیدم</p></div></div>