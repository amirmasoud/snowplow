---
title: >-
    غزل شمارهٔ ۴۰۳
---
# غزل شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>گر تو پنداری به حسن تو نگاری هست نیست</p></div>
<div class="m2"><p>ور تو پنداری مرا بی‌تو قراری هست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور تو گویی چرخ می‌گردد به کار نیک و بد</p></div>
<div class="m2"><p>چرخ را جز خدمت خاک تو کاری هست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سال‌ها شد که بیرون درت چون حلقه‌ایم</p></div>
<div class="m2"><p>بر در تو حلقه بودن هیچ عاری هست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در اندیشه ترسان گشته‌ایم از هر خیال</p></div>
<div class="m2"><p>خواجه را این جا خیالی هست آری هست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل جاسوس من در پیش کیکاووس من</p></div>
<div class="m2"><p>جز صلاح الدین ز دل‌ها هوشیاری هست نیست</p></div></div>