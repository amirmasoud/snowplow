---
title: >-
    غزل شمارهٔ ۲۶۵۳
---
# غزل شمارهٔ ۲۶۵۳

<div class="b" id="bn1"><div class="m1"><p>گر این سلطان ما را بنده باشی</p></div>
<div class="m2"><p>همه گریند و تو در خنده باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر غم پر شود اطراف عالم</p></div>
<div class="m2"><p>تو شاد و خرم و فرخنده باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر چرخ و زمین از هم بدرد</p></div>
<div class="m2"><p>ورای هر دو جانی زنده باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هفتم چرخ نوبت پنج داری</p></div>
<div class="m2"><p>چو خیمه شش جهت برکنده باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه مشتاق دیدار تو باشند</p></div>
<div class="m2"><p>تو صد پرده فروافکنده باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو اندیشه به جاسوسی اسرار</p></div>
<div class="m2"><p>درون سینه‌ها گردنده باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا بر چشم خوبان چهره بگشا</p></div>
<div class="m2"><p>که اندیشد که تو شرمنده باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدیشان صدقه می‌ده چون هلالند</p></div>
<div class="m2"><p>تو بدری از کجا گیرنده باشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر خالی شوی از خویش چون نی</p></div>
<div class="m2"><p>چو نی پر از شکر آکنده باشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برو خرقه گرو کن در خرابات</p></div>
<div class="m2"><p>چو سالوسان چرا در ژنده باشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به عشق شمس تبریزی بده جان</p></div>
<div class="m2"><p>که تا چون عشق او پاینده باشی</p></div></div>