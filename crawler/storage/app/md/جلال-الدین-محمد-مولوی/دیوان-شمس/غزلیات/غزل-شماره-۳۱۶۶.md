---
title: >-
    غزل شمارهٔ ۳۱۶۶
---
# غزل شمارهٔ ۳۱۶۶

<div class="b" id="bn1"><div class="m1"><p>ای دل سرمست، کجا می‌پری؟</p></div>
<div class="m2"><p>بزم تو کو؟ باده کجا می‌خوری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایهٔ هر نقش و ترا نقش نی</p></div>
<div class="m2"><p>دایهٔ هر جان و تو از جان بری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد مثل و نام و لقب گفتمت</p></div>
<div class="m2"><p>برتری از نام ولقب، برتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونک ترا در دو جهان خانه نیست</p></div>
<div class="m2"><p>هر نفسی رخت کجا می‌بری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد ترا بردم من پیش عقل</p></div>
<div class="m2"><p>گفتم: « قیمت کنش ای جوهری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صیر فی نقد معانی توی</p></div>
<div class="m2"><p>سرمه کش دیدهٔ هر ناظری »</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت: « چه دانم ببرش پیش عشق</p></div>
<div class="m2"><p>عشق بود نقد ترا مشتری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون به سر کوچهٔ عشق آمدیم</p></div>
<div class="m2"><p>دل بشد و من بشدم بر سری</p></div></div>