---
title: >-
    غزل شمارهٔ ۲۱۰۷
---
# غزل شمارهٔ ۲۱۰۷

<div class="b" id="bn1"><div class="m1"><p>جان منی جان منی جان من</p></div>
<div class="m2"><p>آن منی آن منی آن من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه منی لایق سودای من</p></div>
<div class="m2"><p>قند منی لایق دندان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور منی باش در این چشم من</p></div>
<div class="m2"><p>چشم من و چشمه حیوان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل چو تو را دید به سوسن بگفت</p></div>
<div class="m2"><p>سرو من آمد به گلستان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دو پراکنده تو چونی بگو</p></div>
<div class="m2"><p>زلف تو حال پریشان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای رسن زلف تو پابند من</p></div>
<div class="m2"><p>چاه زنخدان تو زندان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست فشان مست کجا می‌روی</p></div>
<div class="m2"><p>پیش من آ ای گل خندان من</p></div></div>