---
title: >-
    غزل شمارهٔ ۵۵۳
---
# غزل شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>بی همگان به سر شود بی‌تو به سر نمی‌شود</p></div>
<div class="m2"><p>داغ تو دارد این دلم جای دگر نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده عقل مست تو چرخه چرخ پست تو</p></div>
<div class="m2"><p>گوش طرب به دست تو بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان ز تو جوش می‌کند دل ز تو نوش می‌کند</p></div>
<div class="m2"><p>عقل خروش می‌کند بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمر من و خمار من باغ من و بهار من</p></div>
<div class="m2"><p>خواب من و قرار من بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جاه و جلال من تویی ملکت و مال من تویی</p></div>
<div class="m2"><p>آب زلال من تویی بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه سوی وفا روی گاه سوی جفا روی</p></div>
<div class="m2"><p>آن منی کجا روی بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بنهند برکنی توبه کنند بشکنی</p></div>
<div class="m2"><p>این همه خود تو می‌کنی بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی تو اگر به سر شدی زیر جهان زبر شدی</p></div>
<div class="m2"><p>باغ ارم سقر شدی بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر تو سری قدم شوم ور تو کفی علم شوم</p></div>
<div class="m2"><p>ور بروی عدم شوم بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواب مرا ببسته‌ای نقش مرا بشسته‌ای</p></div>
<div class="m2"><p>وز همه‌ام گسسته‌ای بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر تو نباشی یار من گشت خراب کار من</p></div>
<div class="m2"><p>مونس و غمگسار من بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی تو نه زندگی خوشم بی‌تو نه مردگی خوشم</p></div>
<div class="m2"><p>سر ز غم تو چون کشم بی‌تو به سر نمی‌شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چه بگویم ای سند نیست جدا ز نیک و بد</p></div>
<div class="m2"><p>هم تو بگو به لطف خود بی‌تو به سر نمی‌شود</p></div></div>