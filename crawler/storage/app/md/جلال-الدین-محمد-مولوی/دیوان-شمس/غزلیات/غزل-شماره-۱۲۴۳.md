---
title: >-
    غزل شمارهٔ ۱۲۴۳
---
# غزل شمارهٔ ۱۲۴۳

<div class="b" id="bn1"><div class="m1"><p>اندرآ ای اصل اصل شادمانی شاد باش</p></div>
<div class="m2"><p>اندرآ ای آب آب زندگانی شاد باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرت بیند زندگانی تا ابد باقی شود</p></div>
<div class="m2"><p>ورت بیند مرده هم داند که جانی شاد باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچنین تو دم به دم آن جام باقی می‌رسان</p></div>
<div class="m2"><p>تا شویم از دست و آن باقی تو دانی شاد باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر نشانه خاک ما اینک نشان زخم تو</p></div>
<div class="m2"><p>ای نشانه شاد زی و ای نشانی شاد باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای هما کز سایه‌ات پر یافت کوه قاف نیز</p></div>
<div class="m2"><p>ای همای خوش لقای آن جهانی شاد باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم ظریفی هم حریفی هم چراغی هم شراب</p></div>
<div class="m2"><p>هم جهانی هم نهانی هم عیانی شاد باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تحفه‌های آن جهانی می‌رسانی دم به دم</p></div>
<div class="m2"><p>می‌رسان و می‌رسان خوش می‌رسانی شاد باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخت‌ها را می‌کشاند جان مستان سوی تو</p></div>
<div class="m2"><p>می‌چشان و می‌کشان خوش می‌کشانی شاد باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جهان را شاد کرده وی زمین را جمله گنج</p></div>
<div class="m2"><p>تا زمین گوید تو را کای آسمانی شاد باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر سر خوبی بخارد دلبری در عهد تو</p></div>
<div class="m2"><p>پرچمش آرند پیشت ارمغانی شاد باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوهر آدم به عالم شمس تبریزی تویی</p></div>
<div class="m2"><p>ای ز تو حیران شده بحر معانی شاد باش</p></div></div>