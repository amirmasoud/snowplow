---
title: >-
    غزل شمارهٔ ۳۱۲۴
---
# غزل شمارهٔ ۳۱۲۴

<div class="b" id="bn1"><div class="m1"><p>چو عشقش برآرد سر از بی‌قراری</p></div>
<div class="m2"><p>تو را کی گذارد که سر را بخاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا کار ماند تو را در دو عالم</p></div>
<div class="m2"><p>چو از عشق خوردی یکی جام کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از زخم عشقش چو چنگی شدستم</p></div>
<div class="m2"><p>تهی نیست در من به جز بانگ و زاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چنگی تو ای چنگ تا چند نالی</p></div>
<div class="m2"><p>نه کت می‌نوازد نه اندر کناری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خواهی که پوشی بدین ناله خود را</p></div>
<div class="m2"><p>تو حیلت رها کن تو داری تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر آن گل نچیدی چه بویست این بو</p></div>
<div class="m2"><p>گر آن می نخوردی چرا در خماری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلستان جان‌ها به روی تو خندد</p></div>
<div class="m2"><p>که مر باغ جان را دو صد نوبهاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیالت چو جامست و عشق تو چون می</p></div>
<div class="m2"><p>زهی می‌زهی می‌زهی خوشگواری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو ای شمس تبریز در شرح نایی</p></div>
<div class="m2"><p>بجز آن که یا رب چه یاری چه یاری</p></div></div>