---
title: >-
    غزل شمارهٔ ۲۵۳۴
---
# غزل شمارهٔ ۲۵۳۴

<div class="b" id="bn1"><div class="m1"><p>مها یک دم رعیت شو مرا شه دان و سالاری</p></div>
<div class="m2"><p>اگر مه را جفا گویم بجنبان سر بگو آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا بر تخت خود بنشان دوزانو پیش من بنشین</p></div>
<div class="m2"><p>مرا سلطان کن و می‌دو به پیشم چون سلحداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شها شیری تو من روبه تو من شو یک زمان من تو</p></div>
<div class="m2"><p>چو روبه شیرگیر آید جهان گوید خوش اشکاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان نادر خداوندی ز نادر خسروی آید</p></div>
<div class="m2"><p>که بخشد تاج و تخت خود مگر چون تو کلهداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس احسان که فرمودی چنانم آرزو آمد</p></div>
<div class="m2"><p>که موسی چون سخن بشنود در می‌خواست دیداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی کف خاک بستان شد یکی کف خاک بستانبان</p></div>
<div class="m2"><p>که زنده می‌شود زین لطف هر خاکی و مرداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو خود بی‌تخت سلطانی و بی‌خاتم سلیمانی</p></div>
<div class="m2"><p>تو ماهی وین فلک پیشت یکی طشت نگوساری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی باشد عقل کل پیشت یکی طفلی نوآموزی</p></div>
<div class="m2"><p>چه دارد با کمال تو به جز ریشی و دستاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلیم موسی و هارون به از مال و زر قارون</p></div>
<div class="m2"><p>چرا شاید که بفروشی تو دیداری به دیناری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا باری بحمدالله چه قرص مه چه برگ که</p></div>
<div class="m2"><p>ز مستی خود نمی‌دانم یکی جو را ز قنطاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر عالم نمی‌دارم بیار آن جام خمارم</p></div>
<div class="m2"><p>ز هست خویش بیزارم چه باشد هست من باری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سگ کهفی که مجنون شد ز شیر شرزه افزون شد</p></div>
<div class="m2"><p>خمش کردم که سرمستم نباید بسکلد تاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهل ای دل چو بینایی سخن گویی و رعنایی</p></div>
<div class="m2"><p>هلا بگذار تا یابی از این اطلس کلهواری</p></div></div>