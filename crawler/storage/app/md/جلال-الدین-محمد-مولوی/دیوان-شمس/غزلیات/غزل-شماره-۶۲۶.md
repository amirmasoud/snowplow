---
title: >-
    غزل شمارهٔ ۶۲۶
---
# غزل شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>هر کآتش من دارد او خرقه ز من دارد</p></div>
<div class="m2"><p>زخمی چو حسینستش جامی چو حسن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس ار چه که زاهد شد او راست نخواهد شد</p></div>
<div class="m2"><p>ور راستیی خواهی آن سرو چمن دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانیست تو را ساده نقش تو از آن زاده</p></div>
<div class="m2"><p>در ساده جان بنگر کان ساده چه تن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه جان را بین هم ساده و هم نقشین</p></div>
<div class="m2"><p>هر دم بت نو سازد گویی که شمن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه جانب دل باشد گه در غم گل باشد</p></div>
<div class="m2"><p>ماننده آن مردی کز حرص دو زن دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی شاد شود آن شه کز جان نبود آگه</p></div>
<div class="m2"><p>کی ناز کند مرده کز شعر کفن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌خاید چون اشتر یعنی که دهانم پر</p></div>
<div class="m2"><p>خاییدن بی‌لقمه تصدیق ذقن دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردانه تو مجنون شو و اندر لگن خون شو</p></div>
<div class="m2"><p>گه ماده و گه نر نی کان شیوه زغن دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون موسی رخ زردش توبه مکن از دردش</p></div>
<div class="m2"><p>تا یار نعم گوید کر گفتن لن دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون مست نعم گشتی بی‌غصه و غم گشتی</p></div>
<div class="m2"><p>پس مست کجا داند کاین چرخ سخن دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چشمه بود دلکش دارد دهنت را خوش</p></div>
<div class="m2"><p>لیکن همه گوهرها دریای عدن دارد</p></div></div>