---
title: >-
    غزل شمارهٔ ۱۵۱۳
---
# غزل شمارهٔ ۱۵۱۳

<div class="b" id="bn1"><div class="m1"><p>اگر سرمست اگر مخمور باشم</p></div>
<div class="m2"><p>مهل کز مجلس تو دور باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخم از قبله جان نور گیرد</p></div>
<div class="m2"><p>چو با یاد تو اندر گور باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرارم کی بود خود در تک گور</p></div>
<div class="m2"><p>چو بر دمگاه نفخ صور باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد افسنتین و داروهای نافع</p></div>
<div class="m2"><p>تویی جان را چو من رنجور باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوم شیرین ز لطف گوهر تو</p></div>
<div class="m2"><p>اگر چون بحر تلخ و شور باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر غم همچو شب عالم بگیرد</p></div>
<div class="m2"><p>برآ ای صبح تا منصور باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تویی روز و منم استاره روز</p></div>
<div class="m2"><p>عجب نبود اگر مشهور باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به من شادند جمله روزجویان</p></div>
<div class="m2"><p>چو پیش آهنگ چون تو نور باشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا مخمور می داری نه از بخل</p></div>
<div class="m2"><p>ولی تا ساکن و مستور باشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدان مستور می داری چو حوتم</p></div>
<div class="m2"><p>که تا از عقربت مهجور باشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه غم دارم ز نیش عقرب ای ماه</p></div>
<div class="m2"><p>چو غرق شهد چون زنبور باشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمش کردم ولیکن عشق خواهد</p></div>
<div class="m2"><p>که پیش زخمه‌اش طنبور باشم</p></div></div>