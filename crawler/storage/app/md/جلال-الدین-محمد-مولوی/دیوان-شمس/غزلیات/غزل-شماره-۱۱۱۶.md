---
title: >-
    غزل شمارهٔ ۱۱۱۶
---
# غزل شمارهٔ ۱۱۱۶

<div class="b" id="bn1"><div class="m1"><p>هر کس به جنس خویش درآمیخت ای نگار</p></div>
<div class="m2"><p>هر کس به لایق گهر خود گرفت یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او را که داغ توست نیارد کسی خرید</p></div>
<div class="m2"><p>آن کو شکار توست کسی چون کند شکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را چو لطف روی تو بی‌خویشتن کند</p></div>
<div class="m2"><p>ما را ز روی لطف تو بی‌خویشتن مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون جنس همدگر بگرفتند جنس جنس</p></div>
<div class="m2"><p>هر جنس جنس گوهر خود کرد اختیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با غیر جنس اگر بنشیند بود نفاق</p></div>
<div class="m2"><p>مانند آب و روغن و مانند قیر و قار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چون به جنس خویش رود از خلاف جنس</p></div>
<div class="m2"><p>زین سوی تشنه‌تر شده باشد بدان کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه از تو می‌گریزد با دیگری خوشست</p></div>
<div class="m2"><p>و آنک از تو می‌رمد به کسی دارد او قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و آن کو ترش نشست به پیش تو همچو ابر</p></div>
<div class="m2"><p>خندان دلست پیش دگر کس چو نوبهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی که نیست از مه غیبم به جز دریغ</p></div>
<div class="m2"><p>وز جام و خمر روح مرا نیست جز خمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن نای و نوش یاد نمی‌آیدت که تو</p></div>
<div class="m2"><p>خوش می‌خوری ز دست یکی دیو سنگسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد جام درکشی ز کف دیو آنگهی</p></div>
<div class="m2"><p>بینی ترش کنی بخور ای خام پخته خوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این جا سرک فکنده و رویک ترش ولیک</p></div>
<div class="m2"><p>آن جا چو اژدهای سیه فام کوهسار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با جنس همچو سوسن و با غیر جنس گنگ</p></div>
<div class="m2"><p>با جنس خویش چون گل و با غیر جنس خار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رو رو به جمله خلق نتانی تو جنس بود</p></div>
<div class="m2"><p>شاخی ز صد درخت نشد حامل ثمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون شاخ یک درخت شدی زان دگر ببر</p></div>
<div class="m2"><p>جویای وصل این شده‌ای دست از آن بدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر زانک جنس مفخر تبریز گشت جان</p></div>
<div class="m2"><p>احسنت ای ولایت و شاباش کار و بار</p></div></div>