---
title: >-
    غزل شمارهٔ ۳۰۷۰
---
# غزل شمارهٔ ۳۰۷۰

<div class="b" id="bn1"><div class="m1"><p>اگر به خشم شود چرخ هفتم از تو بری</p></div>
<div class="m2"><p>به جان من که نترسی و هیچ غم نخوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر دلت به بلا و غمش مشرح نیست</p></div>
<div class="m2"><p>یقین بدانک تو در عشق شاه مختصری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنج گنج بترس و ز رنج هر کس نی</p></div>
<div class="m2"><p>که خشم حق نبود همچو کینه بشری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو غیر گوهر معشوق گوهری دانی</p></div>
<div class="m2"><p>تو را گهر نپذیرد ازانک بدگهری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر چو حامله لرزان شوی به هر بویی</p></div>
<div class="m2"><p>ز حاملان امانت بدانک بو نبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسند خویش رها کن پسند دوست طلب</p></div>
<div class="m2"><p>که ماند از شکر آن کس که او کند شکری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ذوق خویش مگو با کسی که همدل نیست</p></div>
<div class="m2"><p>ازانک او دگرست و تو خود کسی دگری</p></div></div>