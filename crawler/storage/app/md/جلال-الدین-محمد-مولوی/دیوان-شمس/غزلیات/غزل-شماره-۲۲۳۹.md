---
title: >-
    غزل شمارهٔ ۲۲۳۹
---
# غزل شمارهٔ ۲۲۳۹

<div class="b" id="bn1"><div class="m1"><p>رفتم به کوی خواجه و گفتم که خواجه کو</p></div>
<div class="m2"><p>گفتند خواجه عاشق و مست است و کو به کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم فریضه دارم آخر نشان دهید</p></div>
<div class="m2"><p>من دوستدار خواجه‌ام آخر نیم عدو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتند خواجه عاشق آن باغبان شده‌ست</p></div>
<div class="m2"><p>او را به باغ‌ها جو یا بر کنار جو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستان و عاشقان بر دلدار خود روند</p></div>
<div class="m2"><p>هر کس که گشت عاشق رو دست از او بشو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماهی که آب دید نپاید به خاکدان</p></div>
<div class="m2"><p>عاشق کجا بماند در دور رنگ و بو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برف فسرده کو رخ آن آفتاب دید</p></div>
<div class="m2"><p>خورشید پاک خوردش اگر هست تو به تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاصه کسی که عاشق سلطان ما بود</p></div>
<div class="m2"><p>سلطان بی‌نظیر وفادار قندخو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن کیمیای بی‌حد و بی‌عد و بی‌قیاس</p></div>
<div class="m2"><p>بر هر مسی که برزد زر شد به ارجعوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خواب شو ز عالم وز شش جهت گریز</p></div>
<div class="m2"><p>تا چند گول گردی و آواره سو به سو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناچار می برندت باری به اختیار</p></div>
<div class="m2"><p>تا پیش شاه باشدت اعزاز و آبرو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر ز آنک در میانه نبودی سرخری</p></div>
<div class="m2"><p>اسرار کشف کردی عیسیت مو به مو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بستم ره دهان و گشادم ره نهان</p></div>
<div class="m2"><p>رستم به یک قنینه ز سودای گفت و گو</p></div></div>