---
title: >-
    غزل شمارهٔ ۳۱۹۵
---
# غزل شمارهٔ ۳۱۹۵

<div class="b" id="bn1"><div class="m1"><p>درهم شکن چو شیشه خود را، چو مست جامی</p></div>
<div class="m2"><p>بد نام عشق جان شو، اینست نیکنامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرذوق، چون صراحی بنشین، اگر نشینی</p></div>
<div class="m2"><p>کن کالقدح مذیقا للقوم فی‌القیام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل تو پای‌بندی، عشق تو سربلندی</p></div>
<div class="m2"><p>العقل فی‌الملام والعشق فی‌المدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الدیک فی صیاح، واللیل فی انهزام</p></div>
<div class="m2"><p>والصبح قد تبدی فی مهجةالضلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معشوق غیر ما، نی، جز که خون ما، نی</p></div>
<div class="m2"><p>هم جان کند رئیسی، هم جان کند غلامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را کباب کردی، خون را شراب کردی</p></div>
<div class="m2"><p>یا من فداک روحی یا سیدالانام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اندیشه شو پیاده، تا بر خوری ز باده</p></div>
<div class="m2"><p>من راوق قدیم، مستکمل‌القوام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستفعلن فعولن، آتش مکن مجوشان</p></div>
<div class="m2"><p>زیرا کمال آمد، دیگر نماند خامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌گو تو هرچه خواهی، فرمان‌روا و شاهی</p></div>
<div class="m2"><p>سلمت یا عزیزی، یا صاحب‌السلام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باده چو با خیزان، چون پشه غم‌گریزان</p></div>
<div class="m2"><p>لا تعذلوا السکارا افدیکم کرامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تبریز شاد بادا، ز اشرق شمس دینم</p></div>
<div class="m2"><p>فالشمس حیث تجری للمشرقین حامی</p></div></div>