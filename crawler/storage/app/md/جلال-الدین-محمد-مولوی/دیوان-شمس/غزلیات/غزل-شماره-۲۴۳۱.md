---
title: >-
    غزل شمارهٔ ۲۴۳۱
---
# غزل شمارهٔ ۲۴۳۱

<div class="b" id="bn1"><div class="m1"><p>این عشق گردان کو به کو بر سر نهاده طبله‌ای</p></div>
<div class="m2"><p>که هر کجا مرده بود زنده کنم بی‌حیله‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوان روانم از کرم زنده کنم مرده بدم</p></div>
<div class="m2"><p>کو نرگدایی تا برد از خوان لطفم زله‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاهی تو را در بر کنم گاهی ز زهرت پر کنم</p></div>
<div class="m2"><p>آگاه شو آخر ز من ای در کفم چون کیله‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر حبه‌ای آید به من صد کان پرزرش کنم</p></div>
<div class="m2"><p>دریای شیرینش کنم هر چند باشد قله‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو عدم وز من کرم وز تو رضا وز من قسم</p></div>
<div class="m2"><p>صد اطلس و اکسون نهم در پیش کرم پیله‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر لحظه نومید را خرمن دهم بی‌کشتنی</p></div>
<div class="m2"><p>هر لحظه درویش را قربت دهم بی‌چله‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمه شکر جوشان کنم اندر دل تنگ نیی</p></div>
<div class="m2"><p>اندیشه‌های خوش نهم اندر دماغ و کله‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌ران فرس در دین فقط ور اسب تو گردد سقط</p></div>
<div class="m2"><p>بر جای اسب لاغری هر سو بیابی گله‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاموش باش و لا مگو جز آن که حق بخشد مجو</p></div>
<div class="m2"><p>جوشان ز حلوای رضا بر جمره چون پاتیله‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تبریز شد خلد برین از عکس روی شمس دین</p></div>
<div class="m2"><p>هر نقش در وی حور عین هر جامه از وی حله‌ای</p></div></div>