---
title: >-
    غزل شمارهٔ ۲۶۰
---
# غزل شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>چرخ فلک با همه کار و کیا</p></div>
<div class="m2"><p>گرد خدا گردد چون آسیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد چنین کعبه کن ای جان طواف</p></div>
<div class="m2"><p>گرد چنین مایده گرد ای گدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر مثل گوی به میدانش گرد</p></div>
<div class="m2"><p>چونک شدی سرخوش بی‌دست و پا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسب و رخت راست بر این شه طواف</p></div>
<div class="m2"><p>گر چه بر این نطع روی جا به جا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاتم شاهیت در انگشت کرد</p></div>
<div class="m2"><p>تا که شوی حاکم و فرمانروا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که به گرد دل آرد طواف</p></div>
<div class="m2"><p>جان جهانی شود و دلربا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همره پروانه شود دلشده</p></div>
<div class="m2"><p>گردد بر گرد سر شمع‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زانک تنش خاکی و دل آتشی‌ست</p></div>
<div class="m2"><p>میل سوی جنس بود جنس را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد فلک گردد هر اختری</p></div>
<div class="m2"><p>زانک بود جنس صفا با صفا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرد فنا گردد جان فقیر</p></div>
<div class="m2"><p>بر مثل آهن و آهن ربا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زانک وجودست فنا پیش او</p></div>
<div class="m2"><p>شسته نظر از حول و از خطا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مست همی‌کرد وضو از کمیز</p></div>
<div class="m2"><p>کز حدثم بازرهان ربنا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت نخستین تو حدث را بدان</p></div>
<div class="m2"><p>کژمژ و مقلوب نباید دعا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانک کلیدست و چو کژ شد کلید</p></div>
<div class="m2"><p>وا شدن قفل نیابی عطا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خامش کردم همگان برجهید</p></div>
<div class="m2"><p>قامت چون سرو بتم زد صلا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خسرو تبریز شهم شمس دین</p></div>
<div class="m2"><p>بستم لب را تو بیا برگشا</p></div></div>