---
title: >-
    غزل شمارهٔ ۲۵۶۸
---
# غزل شمارهٔ ۲۵۶۸

<div class="b" id="bn1"><div class="m1"><p>یک حمله و یک حمله کآمد شب و تاریکی</p></div>
<div class="m2"><p>چستی کن و ترکی کن نی نرمی و تاجیکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داریم سری کان سر بی‌تن بزید چون مه</p></div>
<div class="m2"><p>گر گردن ما دارد در عشق تو باریکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهیم نه سه روزه لعلیم نه پیروزه</p></div>
<div class="m2"><p>عشقیم نه سردستی مستیم نه از سیکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بنده خوبانم هر چند بدم گویند</p></div>
<div class="m2"><p>با زشت نیامیزم هر چند کند نیکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشاق بسی دارد من از حسد ایشان</p></div>
<div class="m2"><p>بیگانه همی‌باشم از غایت نزدیکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روپوش کند او هم با محرم و نامحرم</p></div>
<div class="m2"><p>گویند فلان بنده گوید که عجب کی کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طفلی است سخن گفتن مردی است خمش کردن</p></div>
<div class="m2"><p>تو رستم چالاکی نی کودک چالیکی</p></div></div>