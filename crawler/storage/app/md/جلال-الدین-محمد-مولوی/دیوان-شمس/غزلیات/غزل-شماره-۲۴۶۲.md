---
title: >-
    غزل شمارهٔ ۲۴۶۲
---
# غزل شمارهٔ ۲۴۶۲

<div class="b" id="bn1"><div class="m1"><p>طوطی و طوطی بچه‌ای قند به صد ناز خوری</p></div>
<div class="m2"><p>از شکرستان ازل آمده‌ای بازپری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قند تو فرخنده بود خاصه که در خنده بود</p></div>
<div class="m2"><p>بزم ز آغاز نهم چون تو به آغاز دری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای طربستان ابد ای شکرستان احد</p></div>
<div class="m2"><p>هم طرب اندر طربی هم شکر اندر شکری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسف اندر تتقی یا اسدی بر افقی</p></div>
<div class="m2"><p>یا قمر اندر قمر اندر قمر اندر قمری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی این میکده‌ای نوبت عشرت زده‌ای</p></div>
<div class="m2"><p>تا همه را مست کنی خرقه مستان ببری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست شدم مست ولی اندککی باخبرم</p></div>
<div class="m2"><p>زین خبرم بازرهان ای که ز من باخبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیشتر آ پیش که آن شعشعه چهره تو</p></div>
<div class="m2"><p>می‌نهلد تا نگرم که ملکی یا بشری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقص کنان هر قدحی نعره زنان وافرحی</p></div>
<div class="m2"><p>شیشه گران شیشه شکن مانده از شیشه گری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جام طرب عام شده عقل و سرانجام شده</p></div>
<div class="m2"><p>از کف حق جام بری به که سرانجام بری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر ز خرد تافته‌ام عقل دگر یافته‌ام</p></div>
<div class="m2"><p>عقل جهان یک سری و عقل نهانی دوسری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راهب آفاق شدم با همگان عاق شدم</p></div>
<div class="m2"><p>از همگان می‌ببرم تا که تو از من نبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با غمت آموخته‌ام چشم ز خود دوخته‌ام</p></div>
<div class="m2"><p>در جز تو چون نگرد آنک تو در وی نگری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داد ده ای عشق مرا وز در انصاف درآ</p></div>
<div class="m2"><p>چون ابدا آن توام نی قنقم رهگذری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من به تو مانم فلکا ساکنم و زیر و زبر</p></div>
<div class="m2"><p>ز آنک مقیمی به نظر روز و شب اندر سفری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناظر آنی که تو را دارد منظور جهان</p></div>
<div class="m2"><p>حاضر آنی که از او در سفر و در حضری</p></div></div>