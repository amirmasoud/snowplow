---
title: >-
    غزل شمارهٔ ۲۶۳۴
---
# غزل شمارهٔ ۲۶۳۴

<div class="b" id="bn1"><div class="m1"><p>امروز در این شهر نفیر است و فغانی</p></div>
<div class="m2"><p>از جادوی چشم یکی شعبده خوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شهر به هر گوشه یکی حلقه به گوشی است</p></div>
<div class="m2"><p>از عشق چنین حلقه ربا چرب زبانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌زخم نیابی تو در این شهر یکی دل</p></div>
<div class="m2"><p>از تیر نظرهای چنین سخته کمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شهر چه شهری تو که هر روز تو عید است</p></div>
<div class="m2"><p>ای شهر مکان تو شد از لطف زمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه جای مکان است و چه سودای زمان است</p></div>
<div class="m2"><p>ای هر دو شده از دم تو نادره لانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهری است که او تختگه عشق خدایی است</p></div>
<div class="m2"><p>بغداد نهان است وز او دل همدانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز در این مصر از این یوسف خوبی</p></div>
<div class="m2"><p>بی‌زجر و سیاست شده هر گرگ شبانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد پیر دو صدساله از این یوسف خوش دم</p></div>
<div class="m2"><p>مانند زلیخا شده در عشق جوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او حاکم دل‌ها و روان‌هاست در این شهر</p></div>
<div class="m2"><p>ماننده تقدیر خدا حکم روانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد نور یقین سجده کن روی چو ماهش</p></div>
<div class="m2"><p>کی سوی مهش راه بزد ابر گمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد چون من و تو محو چنان بی‌من و مایی</p></div>
<div class="m2"><p>چون ظلمت شب محو رخ ماه جهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز حضرت او نیست فقیرانه حضوری</p></div>
<div class="m2"><p>جز سایه خورشید رخش نیست امانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از حیله او یک دو سخن دارم بشنو</p></div>
<div class="m2"><p>چون زهره ندارم که بگویم که فلانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نام نگوییم و نشان نیز نگوییم</p></div>
<div class="m2"><p>زین باده شکافیده شود شیشه جانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هین دست ملرزان و فروکش قدح عشق</p></div>
<div class="m2"><p>پازهر چو داری نکند زهر زیانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چیز که خواهی تو ز عطار بیابی</p></div>
<div class="m2"><p>دکان محیط است و جز این نیست دکانی</p></div></div>