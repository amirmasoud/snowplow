---
title: >-
    غزل شمارهٔ ۱۱۶
---
# غزل شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>ای سخت گرفته جادوی را</p></div>
<div class="m2"><p>شیری بنموده آهوی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سحر تو احولست دیده</p></div>
<div class="m2"><p>در دیده نهاده‌ای دوی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنموده‌ای از ترنج آلو</p></div>
<div class="m2"><p>کی یافت ترنج آلوی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سحر تو نمود بره را گرگ</p></div>
<div class="m2"><p>بنموده ز گندمی جوی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منشور بقا نموده سحرت</p></div>
<div class="m2"><p>طومار خیال منطوی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر باد هدایتست ریشش</p></div>
<div class="m2"><p>از سحر تو جاهل غوی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوفسطاییم کرد سحرت</p></div>
<div class="m2"><p>ای ترک نموده هندوی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون پشه نموده وقت پیکار</p></div>
<div class="m2"><p>پیلان تهمتن قوی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا جنگ کنند و راست آرند</p></div>
<div class="m2"><p>تقدیر و قضای مستوی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوفسطایی مشو خمش کن</p></div>
<div class="m2"><p>بگشای زبان معنوی را</p></div></div>