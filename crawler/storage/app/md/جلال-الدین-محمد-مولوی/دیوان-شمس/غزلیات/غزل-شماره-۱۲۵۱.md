---
title: >-
    غزل شمارهٔ ۱۲۵۱
---
# غزل شمارهٔ ۱۲۵۱

<div class="b" id="bn1"><div class="m1"><p>گر لب او شکند نرخ شکر می‌رسدش</p></div>
<div class="m2"><p>ور رخش طعنه زند بر گل تر می‌رسدش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر فلک سجده برد بر در او می‌سزدش</p></div>
<div class="m2"><p>ور ستاند گرو از قرص قمر می‌رسدش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور شه عقل که عالم همگی چاکر اوست</p></div>
<div class="m2"><p>جهت خدمت او بست کمر می‌رسدش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه خورشید که بر زنگی شب تیغ کشید</p></div>
<div class="m2"><p>گر پی هیبتش افکند سپر می‌رسدش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عطارد ز پی دایره و نقطه او</p></div>
<div class="m2"><p>همچو پرگار دوانست به سر می‌رسدش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن جمالی که فرشته نبود محرم او</p></div>
<div class="m2"><p>گر ندارد سر دیدار بشر می‌رسدش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار و بار ملکانی که زبردست شدند</p></div>
<div class="m2"><p>نکند ور بکند زیر و زبر می‌رسدش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌شمردم من از این نوع شنودم ز فلک</p></div>
<div class="m2"><p>که از این‌ها بگذر چیز دگر می‌رسدش</p></div></div>