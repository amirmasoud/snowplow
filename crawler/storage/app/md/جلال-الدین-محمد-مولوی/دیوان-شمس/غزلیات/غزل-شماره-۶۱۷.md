---
title: >-
    غزل شمارهٔ ۶۱۷
---
# غزل شمارهٔ ۶۱۷

<div class="b" id="bn1"><div class="m1"><p>چشم از پی آن باید تا چیز عجب بیند</p></div>
<div class="m2"><p>جان از پی آن باید تا عیش و طرب بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر از پی آن باید تا مست بتی باشد</p></div>
<div class="m2"><p>پا از پی آن باید کز یار تعب بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق از پی آن باید تا سوی فلک پرد</p></div>
<div class="m2"><p>عقل از پی آن باید تا علم و ادب بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیرون سبب باشد اسرار و عجایب‌ها</p></div>
<div class="m2"><p>محجوب بود چشمی کو جمله سبب بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق که به صد تهمت بدنام شود این سو</p></div>
<div class="m2"><p>چون نوبت وصل آید صد نام و لقب بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ارزد که برای حج در ریگ و بیابان‌ها</p></div>
<div class="m2"><p>با شیر شتر سازد یغمای عرب بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سنگ سیه حاجی زان بوسه زند از دل</p></div>
<div class="m2"><p>کز لعل لب یاری او لذت لب بیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر نقد سخن جانا هین سکه مزن دیگر</p></div>
<div class="m2"><p>کان کس که طلب دارد او کان ذهب بیند</p></div></div>