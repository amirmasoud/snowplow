---
title: >-
    غزل شمارهٔ ۱۶۴۴
---
# غزل شمارهٔ ۱۶۴۴

<div class="b" id="bn1"><div class="m1"><p>جز ز فتان دو چشمت ز کی مفتون باشیم</p></div>
<div class="m2"><p>جز ز زنجیر دو زلفت ز کی مجنون باشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز از آن روی چو ماهت که مهش جویان است</p></div>
<div class="m2"><p>دگر از بهر که سرگشته چو گردون باشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نار خندان تو ما را صنما گریان کرد</p></div>
<div class="m2"><p>تا چو نار از غم تو با دل پرخون باشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم مست تو قدح بر سر ما می ریزد</p></div>
<div class="m2"><p>ما چه موقوف شراب و می و افیون باشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلفشان رخ تو خرمن گل می بخشد</p></div>
<div class="m2"><p>ما چه موقوف بهار و گل گلگون باشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو موسی ز درخت تو حریف نوریم</p></div>
<div class="m2"><p>ما چرا عاشق برگ و زر قارون باشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر زمان عشق درآید که حریفان چونید</p></div>
<div class="m2"><p>ما ز چون گفتن او واله و بی‌چون باشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما چو زاییده و پرورده آن دریاییم</p></div>
<div class="m2"><p>صاف و تابنده و خوش چون در مکنون باشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما ز نور رخ خورشید چو اجرا داریم</p></div>
<div class="m2"><p>همچو مه تیزرو و چابک و موزون باشیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دعا نوح خیالت یم و جیحون خواهد</p></div>
<div class="m2"><p>بهر این سابح و با چشم چو جیحون باشیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو عشقیم درون دل هر سودایی</p></div>
<div class="m2"><p>لیک چون عشق ز وهم همه بیرون باشیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونک در مطبخ دل لوت طبق بر طبق است</p></div>
<div class="m2"><p>ما چرا کاسه کش مطبخ هر دون باشیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وقف کردیم بر این باده جان کاسه سر</p></div>
<div class="m2"><p>تا حریف سری و شبلی و ذاالنون باشیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمس تبریز پی نور تو زان ذره شدیم</p></div>
<div class="m2"><p>تا ز ذرات جهان در عدد افزون باشیم</p></div></div>