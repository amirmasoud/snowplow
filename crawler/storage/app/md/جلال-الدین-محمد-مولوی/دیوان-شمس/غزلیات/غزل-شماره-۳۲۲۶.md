---
title: >-
    غزل شمارهٔ ۳۲۲۶
---
# غزل شمارهٔ ۳۲۲۶

<div class="b" id="bn1"><div class="m1"><p>قلت له مصیحا یا ملک‌المشرق</p></div>
<div class="m2"><p>اقسم بالخالق مثلک لم یخلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدرک لایعرف وعدک لا یخلف</p></div>
<div class="m2"><p>نائلک الاشرف بالک لم یغلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسمی کالخردله احرقه ذاالوله</p></div>
<div class="m2"><p>خلد فی الزلزله من یک لم یخفق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صرت انا لا انا غیرک عندی فنا</p></div>
<div class="m2"><p>ضدک یا ذاالغنا مختدع احمق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیج کس ای جان من، جان سخن دان من</p></div>
<div class="m2"><p>نور رخ شد ندید، تا نکند بیدقی</p></div></div>