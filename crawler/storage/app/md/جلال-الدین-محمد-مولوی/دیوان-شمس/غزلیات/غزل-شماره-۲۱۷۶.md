---
title: >-
    غزل شمارهٔ ۲۱۷۶
---
# غزل شمارهٔ ۲۱۷۶

<div class="b" id="bn1"><div class="m1"><p>آن دلبر عیار جگرخواره ما کو</p></div>
<div class="m2"><p>آن خسرو شیرین شکرپاره ما کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌صورت او مجلس ما را نمکی نیست</p></div>
<div class="m2"><p>آن پرنمک و پرفن و عیاره ما کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باریک شده‌ست از غم او ماه فلک نیز</p></div>
<div class="m2"><p>آن زهره با بهره سیاره ما کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پربسته چو هاروتم و لب تشنه چو ماروت</p></div>
<div class="m2"><p>آن رشک چه بابل سحاره ما کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موسی که در این خشک بیابان به عصایی</p></div>
<div class="m2"><p>صد چشمه روان کرد از این خاره ما کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین پنج حسن ظاهر و زین پنج حسن سر</p></div>
<div class="m2"><p>ده چشمه گشاینده در این قاره ما کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فرقت آن دلبر دردی است در این دل</p></div>
<div class="m2"><p>آن داروی درد دل و آن چاره ما کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>استاره روز او است چو بر می‌ندمد صبح</p></div>
<div class="m2"><p>گویم که بدم گوید کاستاره ما کو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر ظلمات است خضر در طلب آب</p></div>
<div class="m2"><p>کان عین حیات خوش فواره ما کو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان همچو مسیحی است به گهواره قالب</p></div>
<div class="m2"><p>آن مریم بندنده گهواره ما کو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن عشق پر از صورت بی‌صورت عالم</p></div>
<div class="m2"><p>هم دوز ز ما هم زه قواره ما کو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کنج یکی پرغم مخمور نشسته‌ست</p></div>
<div class="m2"><p>کان ساقی دریادل خماره ما کو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن زنده کن این در و دیوار بدن کو</p></div>
<div class="m2"><p>و آن رونق سقف و در و درساره ما کو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لوامه و اماره بجنگند شب و روز</p></div>
<div class="m2"><p>جنگ افکن لوامه و اماره ما کو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ما مشت گلی در کف قدرت متقلب</p></div>
<div class="m2"><p>از غفلت خود گفته که گل کاره ما کو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شمس الحق تبریز کجا رفت و کجا نیست</p></div>
<div class="m2"><p>و اندر پی او آن دل آواره ما کو</p></div></div>