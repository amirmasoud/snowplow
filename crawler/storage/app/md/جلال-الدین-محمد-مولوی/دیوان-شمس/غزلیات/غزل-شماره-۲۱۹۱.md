---
title: >-
    غزل شمارهٔ ۲۱۹۱
---
# غزل شمارهٔ ۲۱۹۱

<div class="b" id="bn1"><div class="m1"><p>ای رونق نوبهار برگو</p></div>
<div class="m2"><p>وی شادی لاله زار برگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌غصه می فروش می‌نوش</p></div>
<div class="m2"><p>بی‌زحمت شاخ خار برگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بلبل و ای هزاردستان</p></div>
<div class="m2"><p>برگو صفت بهار برگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای حلقه به گوش و عاشق گل</p></div>
<div class="m2"><p>گوش و پس سر مخار برگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرح قد سرو و چهره گل</p></div>
<div class="m2"><p>بر عرعر و بر چنار برگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون رفت خزان و رو نهان کرد</p></div>
<div class="m2"><p>بر سرو رو آشکار برگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر پرسندت که جان رز چیست</p></div>
<div class="m2"><p>بر برگ نظر مدار برگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد شیر و هزار گونه خرگوش</p></div>
<div class="m2"><p>خواهی که کنی شکار برگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی که شود قبول عذرت</p></div>
<div class="m2"><p>ز اشکوفه خوش عذار برگو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهی که بری قرار مستان</p></div>
<div class="m2"><p>زان نرگس پرخمار برگو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امروز سر شراب داریم</p></div>
<div class="m2"><p>ساقی شو و بر نهار برگو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مستی آمد ملولیت رفت</p></div>
<div class="m2"><p>صد بار و هزار بار برگو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای جام شرابدار برگرد</p></div>
<div class="m2"><p>وی چنگ لطیف تار برگو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از بهر ثواب و رحمت حق</p></div>
<div class="m2"><p>ای عارف حق گزار برگو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ما منتظر توایم بشتاب</p></div>
<div class="m2"><p>بی‌زحمت انتظار برگو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تشنیع مزن که صله‌ای نیست</p></div>
<div class="m2"><p>نک آوردم نثار برگو</p></div></div>