---
title: >-
    غزل شمارهٔ ۱۲۴۲
---
# غزل شمارهٔ ۱۲۴۲

<div class="b" id="bn1"><div class="m1"><p>گر لاش نمود راه قلاش</p></div>
<div class="m2"><p>ای هر دو جهان غلام آن لاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده جهان و جان ندیده</p></div>
<div class="m2"><p>جانست جهان تو یک نفس باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردیست جهان و اندر این گرد</p></div>
<div class="m2"><p>جاروب نهان شدست و فراش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این مشعله از کجاست بینی</p></div>
<div class="m2"><p>آن روز که بشکنی چو خشخاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشقی که نهان و آشکارست</p></div>
<div class="m2"><p>خون ریز و ستمگرست و اوباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کشته شوی در او بمانی</p></div>
<div class="m2"><p>من مات من الهوی فقد عاش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشقست نه زر نهان نماند</p></div>
<div class="m2"><p>العاشق کل سره فاش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لا حسن یلد حیث لا عشق</p></div>
<div class="m2"><p>شاباش زهی جمال شاباش</p></div></div>