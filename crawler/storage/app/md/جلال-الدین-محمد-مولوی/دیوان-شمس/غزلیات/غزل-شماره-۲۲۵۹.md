---
title: >-
    غزل شمارهٔ ۲۲۵۹
---
# غزل شمارهٔ ۲۲۵۹

<div class="b" id="bn1"><div class="m1"><p>هله طبل وفا بزن که بیامد اوان تو</p></div>
<div class="m2"><p>می چون ارغوان بده که شکفت ارغوان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفشاریم شیره از شکرانگور باغ تو</p></div>
<div class="m2"><p>بفشانیم میوه‌ها ز درخت جوان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمران جان و عقل را ز سر خوان فضل خود</p></div>
<div class="m2"><p>چه خورد یا چه کم کند مگسی دو ز خوان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طمع جمله طامعان بود از خرمنت جوی</p></div>
<div class="m2"><p>دو ده مختصر بود دو جهان در جهان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه روز آفتاب اگر ز ضیا تیغ می‌زند</p></div>
<div class="m2"><p>به کم از ذره می‌شود ز نهیب سنان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زمین بوس می‌کند پی تو جان آسمان</p></div>
<div class="m2"><p>به چه پر برپرد زمین به سوی آسمان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنشیند شکسته پر سوی تو می‌کند نظر</p></div>
<div class="m2"><p>که همین جاش می‌رسد مدد ارمغان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه گذشته‌ست در جهان نه شب و نی سحرگهان</p></div>
<div class="m2"><p>که دمم آتشین نشد ز دم پاسبان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه مرا وعده کرده‌ای نه که سوگند خورده‌ای</p></div>
<div class="m2"><p>که به هنگام برشدن برسد نردبان تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بدان چشم عبهری به سوی بنده بنگری</p></div>
<div class="m2"><p>بپرد جانش از مکان به سوی لامکان تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنوازیش کای حزین مخور اندوه بعد از این</p></div>
<div class="m2"><p>که خروشید آسمان ز خروش و فغان تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منم از مادر و پدر به نوازش رحیمتر</p></div>
<div class="m2"><p>جهت پختگی تو برسید امتحان تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بکنم باغ و جنتی و دوایی ز درد تو</p></div>
<div class="m2"><p>بکنم آسمان تو به از این از دخان تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه گفتیم و اصل را بنگفتیم دلبرا</p></div>
<div class="m2"><p>که همان به که راز تو شنوند از دهان تو</p></div></div>