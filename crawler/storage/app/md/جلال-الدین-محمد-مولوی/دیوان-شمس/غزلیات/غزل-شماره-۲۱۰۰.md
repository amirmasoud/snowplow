---
title: >-
    غزل شمارهٔ ۲۱۰۰
---
# غزل شمارهٔ ۲۱۰۰

<div class="b" id="bn1"><div class="m1"><p>چند بوسه وظیفه تعیین کن</p></div>
<div class="m2"><p>به شکرخنده‌ایم شیرین کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دلت را خدای نرم کناد</p></div>
<div class="m2"><p>این دعای خوش است آمین کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر این را به خواب خواهم دید</p></div>
<div class="m2"><p>من بخسبم کنار بالین کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای فسون اجل فراق لبت</p></div>
<div class="m2"><p>رو فسون مسیح آیین کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرصه چرخ بی‌تو تنگ آمد</p></div>
<div class="m2"><p>هین براق وصال را زین کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن داری وفاست لایق حسن</p></div>
<div class="m2"><p>حسن را با وفا تو کابین کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بمیرند رحم خواهی کرد</p></div>
<div class="m2"><p>آنچ آخر کنی تو پیشین کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاجیان مانده‌اند از ره حج</p></div>
<div class="m2"><p>داروی اشتران گرگین کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به کعبه وصال تو برسند</p></div>
<div class="m2"><p>چاره آب و زاد و خرجین کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دو چشم جهان به تو روشن</p></div>
<div class="m2"><p>این جهان را تو آن جهان بین کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تجلی آفتاب رخت</p></div>
<div class="m2"><p>چشم و دل را چو طور سینین کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس کنم شد ز حد گستاخی</p></div>
<div class="m2"><p>من کی باشم که گویمت این کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نبود این سخن ز من لایق</p></div>
<div class="m2"><p>آنچ آن لایق است تلقین کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمس تبریز بر افق بخرام</p></div>
<div class="m2"><p>گو شمال هلال و پروین کن</p></div></div>