---
title: >-
    غزل شمارهٔ ۱۵۴۷
---
# غزل شمارهٔ ۱۵۴۷

<div class="b" id="bn1"><div class="m1"><p>من با تو حدیث بی‌زبان گویم</p></div>
<div class="m2"><p>وز جمله حاضران نهان گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز گوش تو نشنود حدیث من</p></div>
<div class="m2"><p>هر چند میان مردمان گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خواب سخن نه بی‌زبان گویند</p></div>
<div class="m2"><p>در بیداری من آن چنان گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز در بن چاه می ننالم من</p></div>
<div class="m2"><p>اسرار غم تو بی‌مکان گویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر روی زمین نشسته باشم خوش</p></div>
<div class="m2"><p>احوال زمین بر آسمان گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معشوق همی‌شود نهان از من</p></div>
<div class="m2"><p>هر چند علامت نشان گویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان‌های لطیف در فغان آیند</p></div>
<div class="m2"><p>آن دم که من از غمت فغان گویم</p></div></div>