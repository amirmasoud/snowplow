---
title: >-
    غزل شمارهٔ ۲۴۷۲
---
# غزل شمارهٔ ۲۴۷۲

<div class="b" id="bn1"><div class="m1"><p>چشم تو خواب می‌رود یا که تو ناز می‌کنی</p></div>
<div class="m2"><p>نی به خدا که از دغل چشم فراز می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ببسته‌ای که تا خواب کنی حریف را</p></div>
<div class="m2"><p>چونک بخفت بر زرش دست دراز می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلسله‌ای گشاده‌ای دام ابد نهاده‌ای</p></div>
<div class="m2"><p>بند کی سخت می‌کنی بند کی باز می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق بی‌گناه را بهر ثواب می‌کشی</p></div>
<div class="m2"><p>بر سر گور کشتگان بانگ نماز می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه به مثال ساقیان عقل ز مغز می‌بری</p></div>
<div class="m2"><p>گه به مثال مطربان نغنغه ساز می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبل فراق می‌زنی نای عراق می‌زنی</p></div>
<div class="m2"><p>پرده بوسلیک را جفت حجاز می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان و دل فقیر را خسته دل اسیر را</p></div>
<div class="m2"><p>از صدقات حسن خود گنج نیاز می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده چرخ می‌دری جلوه ملک می‌کنی</p></div>
<div class="m2"><p>تاج شهان همی‌بری ملک ایاز می‌کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق منی و عشق را صورت شکل کی بود</p></div>
<div class="m2"><p>اینک به صورتی شدی این به مجاز می‌کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنج بلا نهایتی سکه کجاست گنج را</p></div>
<div class="m2"><p>صورت سکه گر کنی آن پی گاز می‌کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غرق غنا شو و خمش شرم بدار چند چند</p></div>
<div class="m2"><p>در کنف غنای او ناله آز می‌کنی</p></div></div>