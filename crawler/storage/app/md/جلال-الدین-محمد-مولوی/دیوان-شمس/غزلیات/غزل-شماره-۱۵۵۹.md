---
title: >-
    غزل شمارهٔ ۱۵۵۹
---
# غزل شمارهٔ ۱۵۵۹

<div class="b" id="bn1"><div class="m1"><p>من دوش به تازه عهد کردم</p></div>
<div class="m2"><p>سوگند به جان تو بخوردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز روی تو چشم برندارم</p></div>
<div class="m2"><p>گر تیغ زنی ز تو نگردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درمان ز کسی دگر نجویم</p></div>
<div class="m2"><p>زیرا ز فراق توست دردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آتشم ار فروبری تو</p></div>
<div class="m2"><p>گر آه برآورم نه مردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخاستم از رهت چو گردی</p></div>
<div class="m2"><p>بر خاک ره تو بازگردم</p></div></div>