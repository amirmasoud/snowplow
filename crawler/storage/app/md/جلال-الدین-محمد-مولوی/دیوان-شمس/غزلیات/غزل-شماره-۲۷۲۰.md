---
title: >-
    غزل شمارهٔ ۲۷۲۰
---
# غزل شمارهٔ ۲۷۲۰

<div class="b" id="bn1"><div class="m1"><p>تو تا بنشسته‌ای بر دار فانی</p></div>
<div class="m2"><p>نشسته می‌روی و می نبینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشسته می‌روی این نیز نیکو است</p></div>
<div class="m2"><p>اگر رویت در این گفتن سوی او است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی گشتی در این گرداب گردان</p></div>
<div class="m2"><p>به سوی جوی رحمت رو بگردان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزن پایی بر این پابند عالم</p></div>
<div class="m2"><p>که تا دست از تبرک بر تو مالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را زلفی است به از مشک و عنبر</p></div>
<div class="m2"><p>تو ده کل را کلاهی ای برادر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کله کم جو چو داری جعد فاخر</p></div>
<div class="m2"><p>کله بر آسمان انداز آخر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا دنیا به نکته مستحیله</p></div>
<div class="m2"><p>فریبد چون تو زیرک را به حیله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سردی نکته گوید سرد سیلی</p></div>
<div class="m2"><p>نداری پای آن خر را شکالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر دوران دلیل آرد در آن قال</p></div>
<div class="m2"><p>تخلف دیده‌ای در روی او مال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را عمری کشید این غول در تیه</p></div>
<div class="m2"><p>بکن با غول خود بحثی به توجیه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا الزام اویی چیست سکته</p></div>
<div class="m2"><p>جوابش گو که مقلوب است نکته</p></div></div>