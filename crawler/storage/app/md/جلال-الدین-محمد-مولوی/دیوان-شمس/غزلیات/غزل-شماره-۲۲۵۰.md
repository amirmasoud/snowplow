---
title: >-
    غزل شمارهٔ ۲۲۵۰
---
# غزل شمارهٔ ۲۲۵۰

<div class="b" id="bn1"><div class="m1"><p>هزار بار کشیده‌ست عشق کافرخو</p></div>
<div class="m2"><p>شبم ز بام به حجره ز حجره تا سر کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب آن چنان به گاه آمده که هی برخیز</p></div>
<div class="m2"><p>گرفته گوش مرا سخت همچو گوش سبو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر چه پر کندم من سبوی تسلیمم</p></div>
<div class="m2"><p>سبو اسیر سقایست چون گریزد از او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار بار سبو را به سنگ بشکست او</p></div>
<div class="m2"><p>شکست او خوشم آید ز شوق و ذوق رفو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبو سپرده بدو گوش با هزاران دل</p></div>
<div class="m2"><p>بدان هوس که خورد غوطه در میانه جو</p></div></div>