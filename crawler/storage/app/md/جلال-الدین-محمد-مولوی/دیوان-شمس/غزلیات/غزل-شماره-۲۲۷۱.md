---
title: >-
    غزل شمارهٔ ۲۲۷۱
---
# غزل شمارهٔ ۲۲۷۱

<div class="b" id="bn1"><div class="m1"><p>جود الشموس علی الوری اشراق</p></div>
<div class="m2"><p>و وراء ها نور الهوی براق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و وراء انوار الهوی لی سید</p></div>
<div class="m2"><p>ضائت لنا بضیائه الافاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما اطیب العشاق فی اشواقهم</p></div>
<div class="m2"><p>العشق ایضا نحوهم مشتاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هموا لرؤیته فلاحت شمسه</p></div>
<div class="m2"><p>حارت و کلت نحوه الاحداق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نادی منادی عاشقیه بدعوه</p></div>
<div class="m2"><p>طفقوا الی صوت النداء و ساقوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سکروا برؤیته و راح لقائه</p></div>
<div class="m2"><p>لا تحسبوهم بعد ذاک افاقوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ان شئت من یحکیک برق خدوده</p></div>
<div class="m2"><p>ضعفی و صفره و جنتی مصداق</p></div></div>