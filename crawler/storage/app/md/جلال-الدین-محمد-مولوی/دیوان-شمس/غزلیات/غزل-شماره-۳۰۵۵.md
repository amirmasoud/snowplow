---
title: >-
    غزل شمارهٔ ۳۰۵۵
---
# غزل شمارهٔ ۳۰۵۵

<div class="b" id="bn1"><div class="m1"><p>بیا بیا که نیابی چو ما دگر یاری</p></div>
<div class="m2"><p>چو ما به هر دو جهان خود کجاست دلداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا بیا و به هر سوی روزگار مبر</p></div>
<div class="m2"><p>که نیست نقد تو را پیش غیر بازاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو همچو وادی خشکی و ما چو بارانی</p></div>
<div class="m2"><p>تو همچو شهر خرابی و ما چو معماری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر خدمت ما که مشارق شادیست</p></div>
<div class="m2"><p>ندید خلق و نبیند ز شادی آثاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار صورت جنبان به خواب می‌بینی</p></div>
<div class="m2"><p>چو خواب رفت نبینی ز خلق دیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببند چشم خر و برگشای چشم خرد</p></div>
<div class="m2"><p>که نفس همچو خر افتاد و حرص افساری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز باغ عشق طلب کن عقیده شیرین</p></div>
<div class="m2"><p>که طبع سرکه فروشست و غوره افشاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا به جانب دارالشفای خالق خویش</p></div>
<div class="m2"><p>کز آن طبیب ندارد گریز بیماری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان مثال تن بی‌سرست بی‌آن شاه</p></div>
<div class="m2"><p>بپیچ گرد چنان سر مثال دستاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر سیاه نه‌ای آینه مده از دست</p></div>
<div class="m2"><p>که روح آینه توست و جسم زنگاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجاست تاجر مسعود مشتری طالع</p></div>
<div class="m2"><p>که گرمدار منش باشم و خریداری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا و فکرت من کن که فکرتت دادم</p></div>
<div class="m2"><p>چو لعل می‌خری از کان من بخر باری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به پای جانب آن کس برو که پایت داد</p></div>
<div class="m2"><p>بدو نگر به دو دیده که داد دیداری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو کف به شادی او زن که کف ز بحر ویست</p></div>
<div class="m2"><p>که نیست شادی او را غمی و تیماری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو بی‌ز گوش شنو بی‌زبان بگو با او</p></div>
<div class="m2"><p>که نیست گفت زبان بی‌خلاف و آزاری</p></div></div>