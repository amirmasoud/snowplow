---
title: >-
    غزل شمارهٔ ۵۱۹
---
# غزل شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>ای دل فرورو در غمش کالصبر مفتاح الفرج</p></div>
<div class="m2"><p>تا رو نماید مرهمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان فروخور آن دهان تا پیشت آید ناگهان</p></div>
<div class="m2"><p>کرسی و عرش اعظمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خندان شو از نور جهان تا تو شوی سور جهان</p></div>
<div class="m2"><p>ایمن شوی از ماتمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باری دلم از مرد و زن برکند مهر خویشتن</p></div>
<div class="m2"><p>تا عشق شد خال و عمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سینه آیینه کنی بی‌کبر و بی‌کینه کنی</p></div>
<div class="m2"><p>در وی ببینی هر دمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آسمان گر خم دهی در امر و فرمان وارهی</p></div>
<div class="m2"><p>زین آسمان و از خمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم بجهی از ما و منی هم دیو را گردن زنی</p></div>
<div class="m2"><p>در دست پیچی پرچمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اقبال خویش آید تو را دولت به پیش آید تو را</p></div>
<div class="m2"><p>فرخ شوی از مقدمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیویست در اسرار تو کز وی نگون شد کار تو</p></div>
<div class="m2"><p>بربند این دم محکمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دارد خدا خوش عالمی منگر در این عالم دمی</p></div>
<div class="m2"><p>جز حق نباشد محرمش کالصبر مفتاح الفرج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خامش بیان سر مکن خامش که سر من لدن</p></div>
<div class="m2"><p>چون می‌زند اندرهمش کالصبر مفتاح الفرج</p></div></div>