---
title: >-
    غزل شمارهٔ ۲۶۶۲
---
# غزل شمارهٔ ۲۶۶۲

<div class="b" id="bn1"><div class="m1"><p>دلا رو رو همان خون شو که بودی</p></div>
<div class="m2"><p>بدان صحرا و هامون شو که بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این خاکستر هستی چو غلطی</p></div>
<div class="m2"><p>در آتشدان و کانون شو که بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این چون شد چگونه چند مانی</p></div>
<div class="m2"><p>بدان تصریف بی‌چون شو که بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه گاوی که کشی بیگار گردون</p></div>
<div class="m2"><p>بر آن بالای گردون شو که بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این کاهش چو بیماران دقی</p></div>
<div class="m2"><p>به عمر روزافزون شو که بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبون طب افلاطون چه باشی</p></div>
<div class="m2"><p>فلاطون فلاطون شو که بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایم هو کی اسیرانه چه باشی</p></div>
<div class="m2"><p>همان سلطان و بارون شو که بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر رویین تنی جسم آفت توست</p></div>
<div class="m2"><p>همان جان فریدون شو که بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان اقبال و دولت بین که دیدی</p></div>
<div class="m2"><p>همان بخت همایون شو که بودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رها کن نظم کردن درها را</p></div>
<div class="m2"><p>به دریا در مکنون شو که بودی</p></div></div>