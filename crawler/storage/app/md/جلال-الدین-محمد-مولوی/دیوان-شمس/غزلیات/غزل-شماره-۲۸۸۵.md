---
title: >-
    غزل شمارهٔ ۲۸۸۵
---
# غزل شمارهٔ ۲۸۸۵

<div class="b" id="bn1"><div class="m1"><p>هله آن به که خوری این می و از دست روی</p></div>
<div class="m2"><p>تا به هر جا که روی خوشدل و سرمست روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ گردان به تو گردد که تو آب اویی</p></div>
<div class="m2"><p>ماه چرخی چه زیان دارد اگر پست روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهیی لیک چنان مست توست آن دریا</p></div>
<div class="m2"><p>همه دریا ز پی آید چو تو در شست روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدقات همه شاهان که سوی نیست رود</p></div>
<div class="m2"><p>رو سوی هست نهد چون تو سوی هست روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سابق تیزروانی تو در این راه دراز</p></div>
<div class="m2"><p>وز ره رفق تو با این دو سه پابست روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسب عیش ابد آموز ز شمس تبریز</p></div>
<div class="m2"><p>تا در آن مجلس عیشی که جنان است روی</p></div></div>