---
title: >-
    غزل شمارهٔ ۲۵۲۰
---
# غزل شمارهٔ ۲۵۲۰

<div class="b" id="bn1"><div class="m1"><p>گر آبت بر جگر بودی دل تو پس چه کاره ستی</p></div>
<div class="m2"><p>تنت گر آن چنان بودی که گفتی دل نگاره ستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر بر کار بودی دل درون کارگاه عشق</p></div>
<div class="m2"><p>ملالت بر برون تو نمی‌گویی چه کاره ستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنیمت دار رمضان را چو عیدت روی ننموده‌ست</p></div>
<div class="m2"><p>و عیدت گر کنارستی ز غم جان برکناره ستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو روشن گشتی از طاعت شدی تاریک از عصیان</p></div>
<div class="m2"><p>دل بیچاره را می‌دان که او محتاج چاره ستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر محتاج این طاعت نماندستی دل مسکین</p></div>
<div class="m2"><p>ورای کفر و ایمان دل همیشه در نظاره ستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گویی جان من لعل است مگر نبود بدین لعلی</p></div>
<div class="m2"><p>ز تابش‌های خورشیدش مبر گو سنگ خاره ستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گرد قلعه ظلمت نماندی سنگ یک پاره</p></div>
<div class="m2"><p>اگر خود منجنیق صوم دایم سوی باره ستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزن این منجنیق صوم قلعه کفر و ظلمت بر</p></div>
<div class="m2"><p>اگر بودی مسلمانی مؤذن بر مناره ستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر از عید قربان سرافرازان بدانندی</p></div>
<div class="m2"><p>نه هر پاره ز گاو نفس آویز قناره ستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر سوز دل مسکین بدیدییی از این لقمه</p></div>
<div class="m2"><p>ز بهر ساکنی سوزش شکم سوزی هماره ستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در اول منزلت این عشق با این لوت ضدانند</p></div>
<div class="m2"><p>اگر این عشق باره ستی چرا او لوت باره ستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه عالم خر و گاوان به عیش اندرخزیدندی</p></div>
<div class="m2"><p>اگر عاشق بدی آن کس که دایم لوت خواره ستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر دیدی تو ظلمت‌ها ز قوت‌های این لقمه</p></div>
<div class="m2"><p>ز جور نفس تردامن گریبان‌هات پاره ستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به تدریج ار کنی تو پی خر دجال از روزه</p></div>
<div class="m2"><p>ببینی عیسی مریم که در میدان سواره ستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر امر تصوموا را نگهداری به امر رب</p></div>
<div class="m2"><p>به هر یا رب که می‌گویی تو لبیکت دوباره ستی</p></div></div>