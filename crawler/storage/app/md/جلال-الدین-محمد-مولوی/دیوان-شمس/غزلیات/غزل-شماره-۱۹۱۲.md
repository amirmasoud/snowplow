---
title: >-
    غزل شمارهٔ ۱۹۱۲
---
# غزل شمارهٔ ۱۹۱۲

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی می ما را بگردان</p></div>
<div class="m2"><p>بدان می این قضاها را بگردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضا خواهی که از بالا بگردد</p></div>
<div class="m2"><p>شراب پاک بالا را بگردان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمینی خود که باشد با غبارش</p></div>
<div class="m2"><p>زمین و چرخ و دریا را بگردان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیندیشم دگر زین خورده سودا</p></div>
<div class="m2"><p>بیا دریای سودا را بگردان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر من محرم ساغر نباشم</p></div>
<div class="m2"><p>مرا لا گیر و الا را بگردان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر کژ رفت این دل‌ها ز مستی</p></div>
<div class="m2"><p>دل بی‌دست و بی‌پا را بگردان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرابی ده که اندر جا نگنجم</p></div>
<div class="m2"><p>چو فرمودی مرا جا را بگردان</p></div></div>