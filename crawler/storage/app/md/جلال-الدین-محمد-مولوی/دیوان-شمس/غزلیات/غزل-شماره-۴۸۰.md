---
title: >-
    غزل شمارهٔ ۴۸۰
---
# غزل شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>به حق آن که در این دل به جز ولای تو نیست</p></div>
<div class="m2"><p>ولی او نشوم کو ز اولیای تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مباد جانم بی‌غم اگر فدای تو نیست</p></div>
<div class="m2"><p>مباد چشمم روشن اگر سقای تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وفا مباد امیدم اگر به غیر تو است</p></div>
<div class="m2"><p>خراب باد وجودم اگر برای تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام حسن و جمالی که آن نه عکس تو است</p></div>
<div class="m2"><p>کدام شاه و امیری که او گدای تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رضا مده که دلم کام دشمنان گردد</p></div>
<div class="m2"><p>ببین که کام دل من به جز رضای تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا نتانم کردن دمی که بی‌تو گذشت</p></div>
<div class="m2"><p>ولی چه چاره که مقدور جز قضای تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا بباز تو جان را بر او چه می‌لرزی</p></div>
<div class="m2"><p>بر او ملرز فدا کن چه شد خدای تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملرز بر خود تا بر تو دیگران لرزند</p></div>
<div class="m2"><p>به جان تو که تو را دشمنی ورای تو نیست</p></div></div>