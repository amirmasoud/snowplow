---
title: >-
    غزل شمارهٔ ۲۴۳۹
---
# غزل شمارهٔ ۲۴۳۹

<div class="b" id="bn1"><div class="m1"><p>دامن کشانم می‌کشد در بتکده عیاره‌ای</p></div>
<div class="m2"><p>من همچو دامن می‌دوم اندر پی خون خواره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک لحظه هستم می‌کند یک لحظه پستم می‌کند</p></div>
<div class="m2"><p>یک لحظه مستم می‌کند خودکامه‌ای خماره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مهره‌ام در دست او چون ماهیم در شست او</p></div>
<div class="m2"><p>بر چاه بابل می‌تنم از غمزه سحاره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاهوت و ناسوت من او هاروت و ماروت من او</p></div>
<div class="m2"><p>مرجان و یاقوت من او بر رغم هر بدکاره‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در صورت آب خوشی ماهی چو برج آتشی</p></div>
<div class="m2"><p>در سینه دلبر دلی چون مرمری چون خاره‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسرار آن گنج جهان با تو بگویم در نهان</p></div>
<div class="m2"><p>تو مهلتم ده تا که من با خویش آیم پاره‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی ز عکس روی او بردم سبوی تا جوی او</p></div>
<div class="m2"><p>دیدم ز عکس نور او در آب جو استاره‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که آنچ از آسمان جستم بدیدم در زمین</p></div>
<div class="m2"><p>ناگاه فضل ایزدی شد چاره بیچاره‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر است در اول صفم شمشیر هندی در کفم</p></div>
<div class="m2"><p>در باغ نصرت بشکفم از فر گل رخساره‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن رفت کز رنج و غمان خم داده بودم چون کمان</p></div>
<div class="m2"><p>بود این تنم چون استخوان در دست هر سگساره‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خورشید دیدم نیم شب زهره درآمد در طرب</p></div>
<div class="m2"><p>در شهر خویش آمد عجب سرگشته‌ای آواره‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندر خم طغرای کن نو گشت این چرخ کهن</p></div>
<div class="m2"><p>عیسی درآمد در سخن بربسته در گهواره‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دل نیفتد آتشی در پیش ناید ناخوشی</p></div>
<div class="m2"><p>سر برنیارد سرکشی نفسی نماند اماره‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوش شد جهان عاشقان آمد قران عاشقان</p></div>
<div class="m2"><p>وارست جان عاشقان از مکر هر مکاره‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان لطیف بانمک بر عرش گردد چون ملک</p></div>
<div class="m2"><p>نبود دگر زیر فلک مانند هر سیاره‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مانند موران عقل و جان گشتند در طاس جهان</p></div>
<div class="m2"><p>آن رخنه جویان را نهان وا شد در و درساره‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی‌خار گردد شاخ گل زیرا که ایمن شد ز ذل</p></div>
<div class="m2"><p>زیرا نماندش دشمنی گل چین و گل افشاره‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاموش خاموش ای زبان همچون زبان سوسنان</p></div>
<div class="m2"><p>مانند نرگس چشم شو در باغ کن نظاره‌ای</p></div></div>