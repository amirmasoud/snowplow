---
title: >-
    غزل شمارهٔ ۱۱۷۴
---
# غزل شمارهٔ ۱۱۷۴

<div class="b" id="bn1"><div class="m1"><p>مرا می‌گفت دوش آن یار عیار</p></div>
<div class="m2"><p>سگ عاشق به از شیران هشیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان پر شد مگر گوشت گرفتست</p></div>
<div class="m2"><p>سگ اصحاب کهف و صاحب غار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرین شاه باشد آن سگی کو</p></div>
<div class="m2"><p>برای شاه جوید کبک و کفتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خصوصا آن سگی کو را به همت</p></div>
<div class="m2"><p>نباشد صید او جز شاه مختار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببوسد خاک پایش شیر گردون</p></div>
<div class="m2"><p>بدان لب که نیالاید به مردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی می‌خور دمی می‌گو به نوبت</p></div>
<div class="m2"><p>مده خود را به گفت و گو به یک بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه آن مطرب که در مجلس نشیند</p></div>
<div class="m2"><p>گهی نوشد گهی کوشد به مزمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملولان باز جنبیدن گرفتند</p></div>
<div class="m2"><p>همی‌جنگند و می‌لنگند ناچار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجنبان گوشه زنجیر خود را</p></div>
<div class="m2"><p>رگ دیوانگیشان را بیفشار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملول جمله عالم تازه گردد</p></div>
<div class="m2"><p>چو خندان اندرآید یار بی‌یار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>الفت السکر ادرکنی باسکار</p></div>
<div class="m2"><p>ایا جاری ایا جاری ایا جار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و لا تسق بکاسات صغار</p></div>
<div class="m2"><p>فهذا یوم احسان و ایثار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و قاتل فی سبیل الجود بخلا</p></div>
<div class="m2"><p>لیبقی منک منهاج و آثار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فقل انا صببنا الماء صبا</p></div>
<div class="m2"><p>و نحن الماء لا ماء و لا نار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>و سیمائی شهید لی بانی</p></div>
<div class="m2"><p>قضیت عندهم فی العشق اوطار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و طیبوا و اسکروا قومی فانی</p></div>
<div class="m2"><p>کریم فی کروم العصر عصار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جنون فی جنون فی جنون</p></div>
<div class="m2"><p>تخفف عنک اثقالا و اوزار</p></div></div>