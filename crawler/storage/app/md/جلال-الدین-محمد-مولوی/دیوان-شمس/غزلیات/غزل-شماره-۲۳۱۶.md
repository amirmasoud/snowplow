---
title: >-
    غزل شمارهٔ ۲۳۱۶
---
# غزل شمارهٔ ۲۳۱۶

<div class="b" id="bn1"><div class="m1"><p>امروز بت خندان می‌بخش کند خنده</p></div>
<div class="m2"><p>عالم همه خندان شد بگذشت ز حد خنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته حسد بودی پرغصه ولیک این دم</p></div>
<div class="m2"><p>می‌جوشد و می‌روید از عین حسد خنده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در من بنگر ای جان تا هر دو سلف خندیم</p></div>
<div class="m2"><p>کان خنده بی‌پایان آورد مدد خنده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بربسته و بررسته غرقند در این رسته</p></div>
<div class="m2"><p>تا با همگان باشد از عین ابد خنده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند نهان خندم پنهان نکنم زین پس</p></div>
<div class="m2"><p>هر چند نهان دارم از من بجهد خنده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور تو پنهان داری ناموس تو من دانم</p></div>
<div class="m2"><p>کاندر سر هر مویت درجست دو صد خنده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ذره که می‌پوید بی‌خنده نمی‌روید</p></div>
<div class="m2"><p>از نیست سوی هستی ما را کی کشد خنده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خنده پدر و مادر در چرخ درآوردت</p></div>
<div class="m2"><p>بنمود به هر طورت الطاف احد خنده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن دم که دهان خندد در خنده جان بنگر</p></div>
<div class="m2"><p>کان خنده بی‌دندان در لب بنهد خنده</p></div></div>