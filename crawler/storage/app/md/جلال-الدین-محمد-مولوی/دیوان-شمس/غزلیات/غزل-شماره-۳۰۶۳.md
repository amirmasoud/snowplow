---
title: >-
    غزل شمارهٔ ۳۰۶۳
---
# غزل شمارهٔ ۳۰۶۳

<div class="b" id="bn1"><div class="m1"><p>به هر دلی که درآیی چو عشق بنشینی</p></div>
<div class="m2"><p>بجوشد از تک دل چشمه چشمه شیرینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلید حاجت خلقان بدان شده‌ست دعا</p></div>
<div class="m2"><p>که جان جان دعایی و نور آمینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا به کوی خرابات ناز تو نخرند</p></div>
<div class="m2"><p>مکن تو بینی و ناموس تا جهان بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن الست و بلی جان بی‌بدن بودی</p></div>
<div class="m2"><p>تو را نمود که آنی چه در غم اینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را یکی پر و بالیست آسمان پیما</p></div>
<div class="m2"><p>چه در پی خر و اسپی چه در غم زینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو بگو تو چه جستی که آنت پیش نرفت</p></div>
<div class="m2"><p>بیا بیا که تو سلطان این سلاطینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو تاج شاه جهان را عزیزتر گهری</p></div>
<div class="m2"><p>عروس جان نهان را هزار کابینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه چنگ درزده‌ای در جهان و قانونش</p></div>
<div class="m2"><p>که از ورای فلک زهره قوانینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روز جلوه ملایک تو را سجود کنند</p></div>
<div class="m2"><p>بنشنوند ز ابلیسیان که تو طینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان ببستی و کردی به صدق خدمت دین</p></div>
<div class="m2"><p>کنند خدمت تو بعد از این که تو دینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ستاره وار به انگشت‌ها نمودندت</p></div>
<div class="m2"><p>چو آفتاب کنون نامشار تعیینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چه درخور نازی نیاز را مگذار</p></div>
<div class="m2"><p>برای رشک ز ویسه خوشست رامینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خمش به سوره کنون اقرا بسی عمل کردی</p></div>
<div class="m2"><p>ز قشر حرف گذر کن کنون که والتینی</p></div></div>