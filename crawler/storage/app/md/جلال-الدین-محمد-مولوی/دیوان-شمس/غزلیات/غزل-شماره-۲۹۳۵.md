---
title: >-
    غزل شمارهٔ ۲۹۳۵
---
# غزل شمارهٔ ۲۹۳۵

<div class="b" id="bn1"><div class="m1"><p>گر چه به زیر دلقی شاهی و کیقبادی</p></div>
<div class="m2"><p>ور چه ز چشم دوری در جان و سینه یادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه به نقش پستی بر آسمان شدستی</p></div>
<div class="m2"><p>قندیل آسمانی نه چرخ را عمادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستی تو هست ما را بر نیستی مطلق</p></div>
<div class="m2"><p>بستی مراد ما را بر شرط بی‌مرادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا هیچ سست پایی در کوی تو نیاید</p></div>
<div class="m2"><p>پیش تو شیر آید شیری و شیرزادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر را نهد به بیرون بی‌سر بر تو آید</p></div>
<div class="m2"><p>تا بشنود ز گردون بی‌گوش یا عبادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ماهه راه را تو بگذر برو به روزی</p></div>
<div class="m2"><p>زیرا که چون سلیمان بر بارگیر بادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دینار و زر چه باشد انبار جان بیاور</p></div>
<div class="m2"><p>جان ده درم رها کن گر عاشق جوادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاجت نیاید ای جان در راه تو قلاوز</p></div>
<div class="m2"><p>چون نور و ماهتاب است این مهتدی و هادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مه نور و تاب خود را از جا به جا کشاند</p></div>
<div class="m2"><p>چون اشتر عرب را از جا به جای حادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از صد هزار توبه بشناخت جان مجنون</p></div>
<div class="m2"><p>چون بوی گور لیلی برداشت در منادی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون مه پی فزایش غمگین مشو ز کاهش</p></div>
<div class="m2"><p>زیرا ز بعد کاهش چون مه در ازدیادی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر لحظه دسته دسته ریحان به پیشت آید</p></div>
<div class="m2"><p>رسته ز دست رنجت وز خوب اعتقادی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تشنیع بر سلیمان آری که گم شدم من</p></div>
<div class="m2"><p>گم شو چو هدهد ار تو دربند افتقادی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا صاحبی هذا دیباجه الرشاد</p></div>
<div class="m2"><p>الصبح قد تجلی حولوا عن الرقاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الشمس قد تلالا من غیر احتجاب</p></div>
<div class="m2"><p>و النصر قد توالی من غیر اجتهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الروح فی المطار و الکأس فی الدوار</p></div>
<div class="m2"><p>و الهم فی الفرار و السکر فی امتداد</p></div></div>