---
title: >-
    غزل شمارهٔ ۱۵۶۲
---
# غزل شمارهٔ ۱۵۶۲

<div class="b" id="bn1"><div class="m1"><p>گر ناز تو را به گفت نارم</p></div>
<div class="m2"><p>مهر تو درون سینه دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌مهر تو گر گلی ببویم</p></div>
<div class="m2"><p>در حال بسوز همچو خارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماننده ماهی ار خموشم</p></div>
<div class="m2"><p>چون موج و چو بحر بی‌قرارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بر لب من نهاده مهری</p></div>
<div class="m2"><p>می کش تو به سوی خود مهارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقصود تو چیست من چه دانم</p></div>
<div class="m2"><p>دانم که من اندر این قطارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشخوار غمت زنم چو اشتر</p></div>
<div class="m2"><p>چون اشتر مست کف برآرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چند نهان کنم نگویم</p></div>
<div class="m2"><p>در حضرت عشق آشکارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماننده دانه زیر خاکم</p></div>
<div class="m2"><p>موقوف اشارت بهارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بی‌دم خود زنم دمی خوش</p></div>
<div class="m2"><p>تا بی‌سر خود سری بخارم</p></div></div>