---
title: >-
    غزل شمارهٔ ۱۵۷۵
---
# غزل شمارهٔ ۱۵۷۵

<div class="b" id="bn1"><div class="m1"><p>ما شاخ گلیم نی گیاهیم</p></div>
<div class="m2"><p>ما شیوه تر و تازه خواهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکوفه باغ آسمانیم</p></div>
<div class="m2"><p>نقل و می مجلس الهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما جوی نه‌ایم بلک آبیم</p></div>
<div class="m2"><p>ما ابر نه‌ایم بلک ماهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لوح و قلمیم نی حروفیم</p></div>
<div class="m2"><p>تیغ و علمیم نی سپاهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم خسته غمزه چو تیریم</p></div>
<div class="m2"><p>هم بسته طره سیاهیم</p></div></div>