---
title: >-
    غزل شمارهٔ ۲۰۲۶
---
# غزل شمارهٔ ۲۰۲۶

<div class="b" id="bn1"><div class="m1"><p>به شکرخنده ببردی دل من</p></div>
<div class="m2"><p>بشکن شکر دل را مشکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ما را که ز جا برکندی</p></div>
<div class="m2"><p>به تو آمد پر و بالش بمکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنگر تا به چه لطفش بردی</p></div>
<div class="m2"><p>رحم کن هر نفسش زخم مزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم اندر پی دل می‌آید</p></div>
<div class="m2"><p>چه کند بی‌تو در این قالب تن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌تو دل را نبود برگ جهان</p></div>
<div class="m2"><p>بی‌تو گل را نبود برگ چمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هین چرا بند شکستی خاموش</p></div>
<div class="m2"><p>یا مگر نیست تو را بند دهن</p></div></div>