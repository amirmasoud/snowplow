---
title: >-
    غزل شمارهٔ ۳۷۶
---
# غزل شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>امروز جنون نو رسیده‌ست</p></div>
<div class="m2"><p>زنجیر هزار دل کشیده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز ز کندهای ابلوج</p></div>
<div class="m2"><p>پهلوی جوال‌ها دریده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز آن بدوی به هجده‌ای قلب</p></div>
<div class="m2"><p>آن یوسف حسن را خریده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان‌ها همه شب به عز و اقبال</p></div>
<div class="m2"><p>در نرگس و یاسمن چریده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا لاجرم از بگاه هر جان</p></div>
<div class="m2"><p>چالاک و لطیف و برجهیده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز بنفشه زار و لاله</p></div>
<div class="m2"><p>از سنگ و کلوخ بردمیده‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشکفت درخت در زمستان</p></div>
<div class="m2"><p>در بهمن میوه‌ها پزیده‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی که خدای عالمی نو</p></div>
<div class="m2"><p>در عالم کهنه آفریده‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای عارف عاشق این غزل گو</p></div>
<div class="m2"><p>کت عشق ز عاشقان گزیده‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر چهره چون زر تو گازیست</p></div>
<div class="m2"><p>آن سیمبرت مگر گزیده‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاید که نوازد آن دلی را</p></div>
<div class="m2"><p>کاندر غم او بسی طپیده‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاموش و تفرج چمن کن</p></div>
<div class="m2"><p>کامروز نیابت دو دیده‌ست</p></div></div>