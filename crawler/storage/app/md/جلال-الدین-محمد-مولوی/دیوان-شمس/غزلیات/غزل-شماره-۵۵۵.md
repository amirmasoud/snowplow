---
title: >-
    غزل شمارهٔ ۵۵۵
---
# غزل شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>چونک جمال حسن تو اسب شکار زین کند</p></div>
<div class="m2"><p>نیست عجب که از جنون صد چو مرا چنین کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بال برآرد این دلم چونک غمت پرک زند</p></div>
<div class="m2"><p>بارخدا تو حکم کن تا به ابد همین کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونک ستاره دلم با مه تو قران کند</p></div>
<div class="m2"><p>اه که فلک چه لطف‌ها از تو بر این زمین کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده به دست ساقیت گرد جهان همی‌رود</p></div>
<div class="m2"><p>آخر کار عاقبت جان مرا گزین کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه بسی بیاورد در دل بنده سر کند</p></div>
<div class="m2"><p>غیرت تو بسوزدش گر نفسی جز این کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل همچو آهنم دیو و پری حذر کند</p></div>
<div class="m2"><p>چون دل همچو آب را عشق تو آهنین کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان چو تیر راست من در کف تست چون کمان</p></div>
<div class="m2"><p>چرخ از این ز کین من هر طرفی کمین کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده چرخ و چرخیان نقش کند نشان من</p></div>
<div class="m2"><p>زانک مرا به هر نفس لطف تو همنشین کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سجده کنم به هر نفس از پی شکر آنک حق</p></div>
<div class="m2"><p>در تبریز مر مرا بنده شمس دین کند</p></div></div>