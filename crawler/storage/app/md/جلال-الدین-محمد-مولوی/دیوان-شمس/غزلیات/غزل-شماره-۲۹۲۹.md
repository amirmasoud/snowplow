---
title: >-
    غزل شمارهٔ ۲۹۲۹
---
# غزل شمارهٔ ۲۹۲۹

<div class="b" id="bn1"><div class="m1"><p>ای خیالی که به دل می‌گذری</p></div>
<div class="m2"><p>نی خیالی نی پری نی بشری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اثر پای تو را می‌جویم</p></div>
<div class="m2"><p>نه زمین و نه فلک می‌سپری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز تو باخبران بی‌خبرند</p></div>
<div class="m2"><p>نه تو از بی‌خبران باخبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مونس و یار دلی یا تو دلی</p></div>
<div class="m2"><p>تو مقیم نظری یا نظری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایها الخاطر فی مکرمه</p></div>
<div class="m2"><p>قف زمانا بخداء البصر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لا تعجل به مرور و نوی</p></div>
<div class="m2"><p>بدل اللیل بضؤ السحر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن تدبیرک قد صاغ لنا</p></div>
<div class="m2"><p>الهیولی به حسان الصور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر صور جان و هیولی خرد است</p></div>
<div class="m2"><p>عشق تو دیگر و تو خود دگری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این هیولی پدر صورت‌هاست</p></div>
<div class="m2"><p>ای تو کرده پدران را پدری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نی هیولای همه آبی بود</p></div>
<div class="m2"><p>چه کند آب چو آبش ببری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر هیولا و صور جان افزاست</p></div>
<div class="m2"><p>دگرم عشوه مده تو دگری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از هیولا است صور ریگ روان</p></div>
<div class="m2"><p>ریگ را هرزه چرا می‌شمری</p></div></div>