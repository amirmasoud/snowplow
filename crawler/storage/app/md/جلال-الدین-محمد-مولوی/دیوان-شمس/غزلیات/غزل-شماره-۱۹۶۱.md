---
title: >-
    غزل شمارهٔ ۱۹۶۱
---
# غزل شمارهٔ ۱۹۶۱

<div class="b" id="bn1"><div class="m1"><p>نوبهارا جان مایی جان‌ها را تازه کن</p></div>
<div class="m2"><p>باغ‌ها را بشکفان و کشت‌ها را تازه کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل جمال افروخته‌ست و مرغ قول آموخته‌ست</p></div>
<div class="m2"><p>بی صبا جنبش ندارند هین صبا را تازه کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو سوسن را همی‌گوید زبان را برگشا</p></div>
<div class="m2"><p>سنبله با لاله می گوید وفا را تازه کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد چناران دف زنان و شد صنوبر کف زنان</p></div>
<div class="m2"><p>فاخته نعره زنان کوکو عطا را تازه کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گل سوری قیام و از بنفشه بین رکوع</p></div>
<div class="m2"><p>برگ رز اندر سجود آمد صلا را تازه کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله گل‌ها صلح جو و خار بدخو جنگ جو</p></div>
<div class="m2"><p>خیز ای وامق تو باری عهد عذرا تازه کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رعد گوید ابر آمد مشک‌ها بر خاک ریخت</p></div>
<div class="m2"><p>ای گلستان رو بشو و دست و پا را تازه کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس آمد سوی بلبل خفته چشمک می زند</p></div>
<div class="m2"><p>کاندرآ اندر نوا عشق و هوا را تازه کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل این بشنید از او و با گل صدبرگ گفت</p></div>
<div class="m2"><p>گر سماعت میل شد این بی‌نوا را تازه کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبزپوشان خضرکسوه همی‌گویند رو</p></div>
<div class="m2"><p>چون شکوفه سر سر اولیا را تازه کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وان سه برگ و آن سمن وان یاسمین گویند نی</p></div>
<div class="m2"><p>در خموشی کیمیا بین کیمیا را تازه کن</p></div></div>