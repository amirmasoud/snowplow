---
title: >-
    غزل شمارهٔ ۲۶۹۵
---
# غزل شمارهٔ ۲۶۹۵

<div class="b" id="bn1"><div class="m1"><p>متاز ای دل سوی دریای ناری</p></div>
<div class="m2"><p>که می‌ترسم که تاب نار ناری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجودت از نی و دارد نوایی</p></div>
<div class="m2"><p>ز نی هر دم نوایی نو برآری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیستانت ندارد تاب آتش</p></div>
<div class="m2"><p>وگر چه تو ز نی شهری برآری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان شهر نی منشین بر آذر</p></div>
<div class="m2"><p>که هر سو شعله اندر شعله داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نی سوی آتش میل دارد</p></div>
<div class="m2"><p>چو میل رزق سوی رزق خواری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیاز آتش است آن میل تنها</p></div>
<div class="m2"><p>که آتش رزق می‌خواهد به زاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر چت نی بفرماید تو نی کن</p></div>
<div class="m2"><p>خلاف نی بکن از شهریاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلافش کردی و نی در کمین است</p></div>
<div class="m2"><p>چو نی کم شد سر دیگر نخاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پدید آید تو را ناگه وجودی</p></div>
<div class="m2"><p>نه نی دارد نه شکر آنچ داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی نوری لطیفی جان فزایی</p></div>
<div class="m2"><p>در او می‌های گوناگون کاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشایی پر و بالی کز حلاوت</p></div>
<div class="m2"><p>نمایی لطف‌های لاله زاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان این چنین نوری نماید</p></div>
<div class="m2"><p>دگر خورشید و جان‌ها چون ذراری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به نور او بسوزی پر خود را</p></div>
<div class="m2"><p>ز شیرینی نورش گردی عاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز ناله واشکافد قرص خورشید</p></div>
<div class="m2"><p>که گل گل وادهد هم خار خاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زبان واماند زین پس از بیانش</p></div>
<div class="m2"><p>زبان را کار نقش است و نگاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگار و نقش چون گلبرگ باشد</p></div>
<div class="m2"><p>گدازیده شود چون آب واری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر آن ساحل که‌ای‌ن گل‌ها گدازید</p></div>
<div class="m2"><p>اگر خواهی تو مستی و خماری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی‌گو نام شمس الدین تبریز</p></div>
<div class="m2"><p>کز او این کارها را برگزاری</p></div></div>