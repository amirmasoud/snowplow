---
title: >-
    غزل شمارهٔ ۴۵۸
---
# غزل شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>امروز چرخ را ز مه ما تحیریست</p></div>
<div class="m2"><p>خورشید را ز غیرت رویش تغیریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح وجود را به جز این آفتاب نیست</p></div>
<div class="m2"><p>بر ذره ذره وحدت حسنش مقرریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اما بدان سبب که به هر شام و هر صبوح</p></div>
<div class="m2"><p>اشکال نو نماید گویی که دیگریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشکال نو به نو چو مناقض نمایدت</p></div>
<div class="m2"><p>اندر مناقضات خلافی مستریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تو چو جنگ باشد گویی دو لشکر است</p></div>
<div class="m2"><p>در تو چو جنگ نبود دانی که لشکریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر خلیل لطف بد آتش نمود آب</p></div>
<div class="m2"><p>نمرود قهر بود بر او آب آذریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرگی نمود یوسف در چشم حاسدان</p></div>
<div class="m2"><p>پنهان شد آنک خوب و شکرلب برادریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این دست خود همی‌برد از عشق روی او</p></div>
<div class="m2"><p>وان قصد جانش کرده که بس زشت و منکریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن پرده از نمد نبود از حسد بود</p></div>
<div class="m2"><p>زان پرده دوست را منگر زشت منظریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیویست نفس تو که حسد جزو وصف اوست</p></div>
<div class="m2"><p>تا کل او چگونه قبیحی و مقذریست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن مار زشت را تو کنون شیر می‌دهی</p></div>
<div class="m2"><p>نک اژدها شود که به طبع آدمی خوریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای برق اژدهاکش از آسمان فضل</p></div>
<div class="m2"><p>برتاب و برکشش که از او روح مضطریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی حرف شو چو دل اگرت صدر آرزوست</p></div>
<div class="m2"><p>کز گفت این زبانت چو خواهنده بر دریست</p></div></div>