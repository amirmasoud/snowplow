---
title: >-
    غزل شمارهٔ ۲۹۵۴
---
# غزل شمارهٔ ۲۹۵۴

<div class="b" id="bn1"><div class="m1"><p>گر روشنی تو یارا یا خود سیه ضمیری</p></div>
<div class="m2"><p>در هر دو حال خود را از یار وانگیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پا واگرفتن تو هر دو ز حال کفر است</p></div>
<div class="m2"><p>صد کفر بیش باشد در عاشقان نفیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاکت شود پلیدی چون از صنم بریدی</p></div>
<div class="m2"><p>گردد پلید پاکی چون غرقه در غدیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دنبال شیر گیری کی بی‌کباب مانی</p></div>
<div class="m2"><p>کی بی‌نوا نشینی چون صاحب امیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذار سر بد را پنهان مکن تو خود را</p></div>
<div class="m2"><p>در زیرکی چو مویی پیدا میان شیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوردی تو زهر و گفتی حق را از این چه نقصان</p></div>
<div class="m2"><p>حق بی‌نیاز باشد وز زهر تو بمیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر درخت خرما انداز همچو مریم</p></div>
<div class="m2"><p>گر کاهلی به غایت ور نیز سست پیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سایه‌های خرما شیرین شوی چو خرما</p></div>
<div class="m2"><p>وز پختگی خرما تو پختگی پذیری</p></div></div>