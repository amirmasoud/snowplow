---
title: >-
    غزل شمارهٔ ۱۷۲۱
---
# غزل شمارهٔ ۱۷۲۱

<div class="b" id="bn1"><div class="m1"><p>خوش سوی ما آ دمی ز آنچ که ما هم خوشیم</p></div>
<div class="m2"><p>آب حیات توایم گر چه به شکل آتشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو جو کبوتربچه زاده این لانه‌ای</p></div>
<div class="m2"><p>گر تو نیایی به خود مات از این سو کشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاضر ما شو که ما حاضر آن شاهدیم</p></div>
<div class="m2"><p>مست می اش می شویم باده از او می چشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیزروان همچو سیل گر چه چو که ساکنیم</p></div>
<div class="m2"><p>نعره زنان همچو رعد گر چه چنین خامشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان چو دریا تو راست بر کف خود نه بیا</p></div>
<div class="m2"><p>گر چه که ما همچو چرخ بی‌گنهی می کشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان سوی این پنج حس نوبت ما پنج کن</p></div>
<div class="m2"><p>کان سوی این شش جهت خسرو این هر ششیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پی سرنای عشق تیزدم و دلنواز</p></div>
<div class="m2"><p>کز رگ جان همچو چنگ بهر تو در نالشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحت دعوی عشق مسند و بالش مجو</p></div>
<div class="m2"><p>ما نه چو رنجورکان عاشق آن بالشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نور فلک شمس دین مفخر تبریز ما</p></div>
<div class="m2"><p>از رخ آن آفتاب چرخ درون مه وشیم</p></div></div>