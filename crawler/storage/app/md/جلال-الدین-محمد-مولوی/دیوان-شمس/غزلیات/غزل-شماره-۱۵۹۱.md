---
title: >-
    غزل شمارهٔ ۱۵۹۱
---
# غزل شمارهٔ ۱۵۹۱

<div class="b" id="bn1"><div class="m1"><p>وقت آن آمد که من سوگندها را بشکنم</p></div>
<div class="m2"><p>بندها را بردرانم پندها را بشکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ بدپیوند را من برگشایم بند بند</p></div>
<div class="m2"><p>همچو شمشیر اجل پیوندها را بشکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنبه‌ای از لاابالی در دو گوش دل نهم</p></div>
<div class="m2"><p>پند نپذیرم ز صبر و بندها را بشکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر برگیرم ز قفل و در شکرخانه روم</p></div>
<div class="m2"><p>تا ز شاخی زان شکر این قندها را بشکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به کی از چند و چون آخر ز عشقم شرم باد</p></div>
<div class="m2"><p>کی ز چونی برتر آیم چندها را بشکنم</p></div></div>