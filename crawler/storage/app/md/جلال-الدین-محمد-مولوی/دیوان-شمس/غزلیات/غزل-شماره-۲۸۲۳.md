---
title: >-
    غزل شمارهٔ ۲۸۲۳
---
# غزل شمارهٔ ۲۸۲۳

<div class="b" id="bn1"><div class="m1"><p>ز کجایی ز کجایی هله ای مجلس سامی</p></div>
<div class="m2"><p>نفسی در دل تنگی نفسی بر سر بامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هله ای جان و جهانم مدد نور نهانم</p></div>
<div class="m2"><p>ستن چرخ و زمینی هوس خاصی و عامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب از خلوتیانی عجب از مجلس جانی</p></div>
<div class="m2"><p>عجب از ارمن و رومی عجب از خطه شامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب آن چیست مشعشع رخت از نور مبرقع</p></div>
<div class="m2"><p>که مه و مهر به پیشش کند از عشق غلامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گلستان جمالت چو رسد دیده عاشق</p></div>
<div class="m2"><p>به سوی باغ چه آید مگر از غفلت و خامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیدی انت من این صاد حسناک ندامی</p></div>
<div class="m2"><p>نظر الحق تعالی لک فی البهجه حامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قمر سار الینا حبه فرض علینا</p></div>
<div class="m2"><p>سطع العشق لدینا طرد العشق منامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شجر طاب جناه شجر الخلد فداه</p></div>
<div class="m2"><p>وجد القلب مناه و کلوا منه کرامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر خنبی که ببستی به کرم بازگشایی</p></div>
<div class="m2"><p>خرد هر دو جهان را بربایی به تمامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بشنیدیم که دیکی ز پی خلق بپختی</p></div>
<div class="m2"><p>که از او یابد اباها همگی ذوق طعامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عدم هر چه برآید چو مصفا نظر آید</p></div>
<div class="m2"><p>به دو صد دام درآید چو تواش دانه دامی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رخ یوسف خوبان همه زندان چو گلستان</p></div>
<div class="m2"><p>چو چنین باشد زندان تو چرا در غم وامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هله خاموش مپرسش که کسی قرص قمر را</p></div>
<div class="m2"><p>بنپرسد که چه نامی و کیی وز چه مقامی</p></div></div>