---
title: >-
    غزل شمارهٔ ۳۴۳
---
# غزل شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>ز همراهان جدایی مصلحت نیست</p></div>
<div class="m2"><p>سفر بی‌روشنایی مصلحت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ملک و پادشاهی دیده باشی</p></div>
<div class="m2"><p>پس شاهی گدایی مصلحت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شما را بی‌شما می‌خواند آن یار</p></div>
<div class="m2"><p>شما را این شمایی مصلحت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خوان آسمان آمد به دنیا</p></div>
<div class="m2"><p>از این پس بی‌نوایی مصلحت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این مطبخ که قربانست جان‌ها</p></div>
<div class="m2"><p>چو دونان نان ربایی مصلحت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو آن حرص و آز راه زن را</p></div>
<div class="m2"><p>که مکر و بدنمایی مصلحت نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پا داری برو دستی بجنبان</p></div>
<div class="m2"><p>تو را بی‌دست و پایی مصلحت نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پای تو نماند پر دهندت</p></div>
<div class="m2"><p>که بی‌پر در هوایی مصلحت نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو پر یابی به سوی دام حق پر</p></div>
<div class="m2"><p>که از دامش رهایی مصلحت نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همای قاف قربی ای برادر</p></div>
<div class="m2"><p>هما را جز همایی مصلحت نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان جوی و صفا بحر و تو ماهی</p></div>
<div class="m2"><p>در این جو آشنایی مصلحت نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمش باش و فنای بحر حق شو</p></div>
<div class="m2"><p>به هنبازی خدایی مصلحت نیست</p></div></div>