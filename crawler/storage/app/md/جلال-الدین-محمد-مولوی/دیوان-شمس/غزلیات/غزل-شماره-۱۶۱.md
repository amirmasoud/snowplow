---
title: >-
    غزل شمارهٔ ۱۶۱
---
# غزل شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>چو فرستاد عنایت به زمین مشعله‌ها را</p></div>
<div class="m2"><p>که بدر پرده تن را و ببین مشعله‌ها را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چرا منکر نوری مگر از اصل تو کوری</p></div>
<div class="m2"><p>وگر از اصل تو دوری چه از این مشعله‌ها را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خردا چند به هوشی خردا چند بپوشی</p></div>
<div class="m2"><p>تو عزبخانه مه را تو چنین مشعله‌ها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگر رزم جهان را بنگر لشکر جان را</p></div>
<div class="m2"><p>که به مردی بگشادند کمین مشعله‌ها را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو اگر خواب درآیی ور از این باب درآیی</p></div>
<div class="m2"><p>تو بدانی و ببینی به یقین مشعله‌ها را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو صلاح دل و دین را چو بدان چشم ببینی</p></div>
<div class="m2"><p>به خدا روح امینی و امین مشعله‌ها را</p></div></div>