---
title: >-
    غزل شمارهٔ ۱۷۹۶
---
# غزل شمارهٔ ۱۷۹۶

<div class="b" id="bn1"><div class="m1"><p>دلدار من در باغ دی می گشت و می گفت ای چمن</p></div>
<div class="m2"><p>صد حور کش داری ولی بنگر یکی داری چو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدر لبم نشناختی با من دغاها باختی</p></div>
<div class="m2"><p>اینک چنین بگداختی حیران فی هذا الزمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای فتنه‌ها انگیخته بر خلق آتش ریخته</p></div>
<div class="m2"><p>وز آسمان آویخته بر هر دلی پنهان رسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بحر صاف پاک تو جمله جهان خاشاک تو</p></div>
<div class="m2"><p>در بحر تو رقصان شده خاشاک نقش مرد و زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاشاک اگر گردان بود از موج جان از جا مرو</p></div>
<div class="m2"><p>سرنای خود را گفته تو من دم زنم تو دم مزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس شمع‌ها افروختی بیرون ز سقف آسمان</p></div>
<div class="m2"><p>بس نقش‌ها بنگاشتی بیرون ز شهر جان و تن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بی‌خیال روی تو جمله حقیقت‌ها خیال</p></div>
<div class="m2"><p>ای بی‌تو جان اندر تنم چون مرده‌ای اندر کفن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌نور نورافروز او ای چشم من چیزی مبین</p></div>
<div class="m2"><p>بی‌جان جان انگیز او ای جان من رو جان مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم صلای ماجرا ما را نمی‌پرسی چرا</p></div>
<div class="m2"><p>گفتا که پرسش‌های ما بیرون ز گوش است و دهن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای سایه معشوق را معشوق خود پنداشته</p></div>
<div class="m2"><p>ای سال‌ها نشناخته تو خویش را از پیرهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا جان بااندازه‌ات بر جان بی‌اندازه زد</p></div>
<div class="m2"><p>جانت نگنجد در بدن شمعت نگنجد در لگن</p></div></div>