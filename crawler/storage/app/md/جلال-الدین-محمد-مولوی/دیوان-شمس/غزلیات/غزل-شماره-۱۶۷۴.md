---
title: >-
    غزل شمارهٔ ۱۶۷۴
---
# غزل شمارهٔ ۱۶۷۴

<div class="b" id="bn1"><div class="m1"><p>ما ز بالاییم و بالا می رویم</p></div>
<div class="m2"><p>ما ز دریاییم و دریا می رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما از آن جا و از این جا نیستیم</p></div>
<div class="m2"><p>ما ز بی‌جاییم و بی‌جا می رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لااله اندر پی الالله است</p></div>
<div class="m2"><p>همچو لا ما هم به الا می رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قل تعالوا آیتیست از جذب حق</p></div>
<div class="m2"><p>ما به جذبه حق تعالی می رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتی نوحیم در طوفان روح</p></div>
<div class="m2"><p>لاجرم بی‌دست و بی‌پا می رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو موج از خود برآوردیم سر</p></div>
<div class="m2"><p>باز هم در خود تماشا می رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه حق تنگ است چون سم الخیاط</p></div>
<div class="m2"><p>ما مثال رشته یکتا می رویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هین ز همراهان و منزل یاد کن</p></div>
<div class="m2"><p>پس بدانک هر دمی ما می رویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوانده‌ای انا الیه راجعون</p></div>
<div class="m2"><p>تا بدانی که کجاها می رویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اختر ما نیست در دور قمر</p></div>
<div class="m2"><p>لاجرم فوق ثریا می رویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همت عالی است در سرهای ما</p></div>
<div class="m2"><p>از علی تا رب اعلا می رویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رو ز خرمنگاه ما ای کورموش</p></div>
<div class="m2"><p>گر نه کوری بین که بینا می رویم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای سخن خاموش کن با ما میا</p></div>
<div class="m2"><p>بین که ما از رشک بی‌ما می رویم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای که هستی ما ره را مبند</p></div>
<div class="m2"><p>ما به کوه قاف و عنقا می رویم</p></div></div>