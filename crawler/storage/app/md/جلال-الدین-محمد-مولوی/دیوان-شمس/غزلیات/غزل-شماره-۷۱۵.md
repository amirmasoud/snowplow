---
title: >-
    غزل شمارهٔ ۷۱۵
---
# غزل شمارهٔ ۷۱۵

<div class="b" id="bn1"><div class="m1"><p>اول نظر ار چه سرسری بود</p></div>
<div class="m2"><p>سرمایه و اصل دلبری بود</p></div></div>
<div class="b2" id="bn2"><p>گر عشق وبال و کافری بود</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn3"><div class="m1"><p>زان رنگ تو گشته‌ایم بی‌رنگ</p></div>
<div class="m2"><p>زان سوی خرد هزار فرسنگ</p></div></div>
<div class="b2" id="bn4"><p>گر روم گزید جان اگر زنگ</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn5"><div class="m1"><p>رو کرده به چتر پادشاهی</p></div>
<div class="m2"><p>وز نور مشارقش سپاهی</p></div></div>
<div class="b2" id="bn6"><p>گر یاوه شد او ز شاهراهی</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn7"><div class="m1"><p>همچون مه بی‌پری پریدن</p></div>
<div class="m2"><p>چون سایه به رو و سر دویدن</p></div></div>
<div class="b2" id="bn8"><p>چون سرو ز بادها خمیدن</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn9"><div class="m1"><p>زان مه که نواخت مشتری را</p></div>
<div class="m2"><p>جان داد بتان آزری را</p></div></div>
<div class="b2" id="bn10"><p>گر سهو فتاد سامری را</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn11"><div class="m1"><p>گر هجده هزار عالم ای جان</p></div>
<div class="m2"><p>پر گشت ز قال و قالم ای جان</p></div></div>
<div class="b2" id="bn12"><p>گر حالم وگر محالم ای جان</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn13"><div class="m1"><p>چون ماه نزارگشته شادیم</p></div>
<div class="m2"><p>کاندر پی آفتاب رادیم</p></div></div>
<div class="b2" id="bn14"><p>ور هم به خسوف درفتادیم</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn15"><div class="m1"><p>ناموس شکسته‌ایم و مستیم</p></div>
<div class="m2"><p>صد توبه و عهد را شکستیم</p></div></div>
<div class="b2" id="bn16"><p>ور دست و ترنج را بخستیم</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn17"><div class="m1"><p>زان جام شراب ارغوانی</p></div>
<div class="m2"><p>زان چشمه آب زندگانی</p></div></div>
<div class="b2" id="bn18"><p>گر داد فضولیی نشانی</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn19"><div class="m1"><p>فصلی به جز این چهار فصلش</p></div>
<div class="m2"><p>نی فصل ربیع و اصل اصلش</p></div></div>
<div class="b2" id="bn20"><p>گر لاف زدیم ما ز وصلش</p>
<p>آخر نه به روی آن پری بود</p></div>
<div class="b" id="bn21"><div class="m1"><p>خاموش که گفتنی نتان گفت</p></div>
<div class="m2"><p>رازش باید ز راه جان گفت</p></div></div>
<div class="b2" id="bn22"><p>ور مست شد این دل و نشان گفت</p>
<p>آخر نه به روی آن پری بود</p></div>