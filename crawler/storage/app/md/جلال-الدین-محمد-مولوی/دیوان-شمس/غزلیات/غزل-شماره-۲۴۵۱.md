---
title: >-
    غزل شمارهٔ ۲۴۵۱
---
# غزل شمارهٔ ۲۴۵۱

<div class="b" id="bn1"><div class="m1"><p>دریوزه‌ای دارم ز تو در اقتضای آشتی</p></div>
<div class="m2"><p>دی نکته‌ای فرموده‌ای جان را برای آشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان را نشاط و دمدمه جمله مهماتش همه</p></div>
<div class="m2"><p>کاری نمی‌بینم دگر الا نوای آشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان خشم گیرد با کسی گردد جهانش محبسی</p></div>
<div class="m2"><p>جان را فتد یا رب عجب با جسم رای آشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با غیر اگر خشمین شوی گیری سر خویش و روی</p></div>
<div class="m2"><p>سر با تو چون خشمین شود آن گاه وای آشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دستبوس وصل تو یابد دلم در جست و جو</p></div>
<div class="m2"><p>بس بوسه‌ها که دل دهد بر خاک پای آشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نیکوی که تن کند از لطف داد جان بود</p></div>
<div class="m2"><p>من هر سخا که کرده‌ام بود آن سخای آشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ابر دی گریان شدم وز برگ و بر عریان شدم</p></div>
<div class="m2"><p>خواهم که ناگه درغژم خوش در قبای آشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلطان و شاهنشه شوم اجری فرست مه شوم</p></div>
<div class="m2"><p>نیکولقا آنگه شود کید لقای آشتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جان صد باغ و چمن تشریف ده سوی وطن</p></div>
<div class="m2"><p>هر چند بدرایی من نگذاشت جای آشتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نوبهار لم یکن این باد را تلطیف کن</p></div>
<div class="m2"><p>تا بی‌بخار غم شود از تو فضای آشتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آلایش ما چیست خود با بحر جان و جر و مد</p></div>
<div class="m2"><p>یا کبر و شیطانی ما با کبریای آشتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاموش کن ای بی‌ادب چیزی مگو در زیر لب</p></div>
<div class="m2"><p>تا بی‌ریا باشد طلب اندر دعای آشتی</p></div></div>