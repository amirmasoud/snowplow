---
title: >-
    غزل شمارهٔ ۲۸۲
---
# غزل شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>یا من لواء عشقک لا زال عالیا</p></div>
<div class="m2"><p>قد خاب من یکون من العشق خالیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نادی نسیم عشقک فی انفس الوری</p></div>
<div class="m2"><p>احیاکم جلالی جل جلالیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الحب و الغرام اصول حیاتکم</p></div>
<div class="m2"><p>قد خاب من یظلل من الحب سالیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فی وجنه المحب سطور رقیمه</p></div>
<div class="m2"><p>طوبی لمن یصیر لمعناه تالیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا عابسا تفرق فی الهم حاله</p></div>
<div class="m2"><p>بالله تستمع لمقالی و حالیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا من اذل عقلک نفس الهوی تعی</p></div>
<div class="m2"><p>من ذله النفوس سریعا معالیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا مهملا معیشته فی محبه</p></div>
<div class="m2"><p>اسکت کفی الا له معینا وکالیا</p></div></div>