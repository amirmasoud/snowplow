---
title: >-
    غزل شمارهٔ ۱۱۳۵
---
# غزل شمارهٔ ۱۱۳۵

<div class="b" id="bn1"><div class="m1"><p>بیار ساقی بادت فدا سر و دستار</p></div>
<div class="m2"><p>ز هر کجا که دهد دست جام جان دست آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآی مست و خرامان و ساغر اندر دست</p></div>
<div class="m2"><p>روا مبین چو تو ساقی و ما چنین هشیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیار جام که جانم ز آرزومندی</p></div>
<div class="m2"><p>ز خویش نیز برآمد چه جای صبر و قرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیار جام حیاتی که هم مزاج توست</p></div>
<div class="m2"><p>که مونس دل خسته‌ست و محرم اسرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن شراب که گر جرعه‌ای از او بچکد</p></div>
<div class="m2"><p>ز خاک شوره بروید همان زمان گلزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب لعل که گر نیم شب برآرد جوش</p></div>
<div class="m2"><p>میان چرخ و زمین پر شود از او انوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهی شراب و زهی ساغر و زهی ساقی</p></div>
<div class="m2"><p>که جان‌ها و روان‌ها نثار باد نثار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا که در دل من رازهای پنهانست</p></div>
<div class="m2"><p>شراب لعل بگردان و پرده‌ای مگذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا چو مست کنی آنگهی تماشا کن</p></div>
<div class="m2"><p>که شیرگیر چگونست در میان شکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تبارک الله آن دم که پر شود مجلس</p></div>
<div class="m2"><p>ز بوی جام و ز نور رخ چنان دلدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزار مست چو پروانه جانب آن شمع</p></div>
<div class="m2"><p>نهاده جان به طبق بر که این بگیر و بیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز مطربان خوش آواز و نعره مستان</p></div>
<div class="m2"><p>شراب در رگ خمار گم کند رفتار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببین به حال جوانان کهف کان خوردند</p></div>
<div class="m2"><p>خراب سیصد و نه سال مست اندر غار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه باده بود که موسی به ساحران درریخت</p></div>
<div class="m2"><p>که دست و پای بدادند مست و بیخودوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنان مصر چه دیدند بر رخ یوسف</p></div>
<div class="m2"><p>که شرحه شرحه بریدند ساعد چو نگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه ریخت ساقی تقدیس بر سر جرجیس</p></div>
<div class="m2"><p>که غم نخورد و نترسید ز آتش کفار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزار بارش کشتند و پیشتر می‌رفت</p></div>
<div class="m2"><p>که مستم و خبرم نیست از یکی و هزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صحابیان که برهنه به پیش تیغ شدند</p></div>
<div class="m2"><p>خراب و مست بدند از محمد مختار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غلط محمد ساقی نبود جامی بود</p></div>
<div class="m2"><p>پر از شراب و خدا بود ساقی ابرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کدام شربت نوشید پوره ادهم</p></div>
<div class="m2"><p>که مست وار شد از ملک و مملکت بیزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه سکر بود که آواز داد سبحانی</p></div>
<div class="m2"><p>که گفت رمز اناالحق و رفت بر سر دار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به بوی آن می‌شد آب روشن و صافی</p></div>
<div class="m2"><p>چو مست سجده کنان می‌رود به سوی بحار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز عشق این می خاکست گشته رنگ آمیز</p></div>
<div class="m2"><p>ز تف این می آتش فروخت خوش رخسار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وگر نه باد چرا گشت همدم و غماز</p></div>
<div class="m2"><p>حیات سبزه و بستان و دفتر گفتار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه ذوق دارند این چار اصل ز آمیزش</p></div>
<div class="m2"><p>نبات و مردم و حیوان نتیجه این چار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه بی‌هشانه میی دارد این شب زنگی</p></div>
<div class="m2"><p>که خلق را به یکی جام می‌برد از کار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز لطف و صنعت صانع کدام را گویم</p></div>
<div class="m2"><p>که بحر قدرت او را پدید نیست کنار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شراب عشق بنوشیم و بار عشق کشیم</p></div>
<div class="m2"><p>چنانک اشتر سرمست در میان قطار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه مستیی که تو را آرزوی عقل آید</p></div>
<div class="m2"><p>ز مستی که کند روح و عقل را بیدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز هر چه دارد غیر خدا شکوفه کند</p></div>
<div class="m2"><p>از آنک غیر خدا نیست جز صداع و خمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کجا شراب طهور و کجا می انگور</p></div>
<div class="m2"><p>طهور آب حیاتست و آن دگر مردار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دمی چو خوک و زمانی چو بوزنه کندت</p></div>
<div class="m2"><p>به آب سرخ سیه روی گردی آخر کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دلست خنب شراب خدا سرش بگشا</p></div>
<div class="m2"><p>سرش به گل بگرفتست طبع بدکردار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو اندکی سر خم را ز گل کنی خالی</p></div>
<div class="m2"><p>برآید از سر خم بو و صد هزار آثار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر درآیم کآثار آن فروشمرم</p></div>
<div class="m2"><p>شمار آن نتوان کرد تا به روز شمار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو عاجزیم بلا احصیی فرود آریم</p></div>
<div class="m2"><p>چو گشت وقت فروداشت جام جان بردار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درآ به مجلس عشاق شمس تبریزی</p></div>
<div class="m2"><p>که آفتاب از آن شمس می‌برد انوار</p></div></div>