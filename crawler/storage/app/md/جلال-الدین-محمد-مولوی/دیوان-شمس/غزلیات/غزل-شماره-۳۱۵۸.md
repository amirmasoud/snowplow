---
title: >-
    غزل شمارهٔ ۳۱۵۸
---
# غزل شمارهٔ ۳۱۵۸

<div class="b" id="bn1"><div class="m1"><p>رو، مسلم تراست بی‌کاری</p></div>
<div class="m2"><p>چونک اندر عنایت یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش را کار نیست پیش قلم</p></div>
<div class="m2"><p>آن قلم را چه حاجت از یاری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو بت باش پیش آن بتگر</p></div>
<div class="m2"><p>که همه نقش و رنگ ازو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بپرسد، چه صورتت باید؟</p></div>
<div class="m2"><p>گو: « همان صورتی که بنگاری »</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مرا تن کنی، تو جان منی</p></div>
<div class="m2"><p>ور مرا دل کنی، تو دلداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطف گل، خار را تو می‌بخشی</p></div>
<div class="m2"><p>چه کند شاخ خار، جز خاری؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده ده، باده خواهمان کردی</p></div>
<div class="m2"><p>که حرامست با تو هشیاری</p></div></div>