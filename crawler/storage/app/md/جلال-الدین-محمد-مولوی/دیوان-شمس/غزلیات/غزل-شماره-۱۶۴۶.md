---
title: >-
    غزل شمارهٔ ۱۶۴۶
---
# غزل شمارهٔ ۱۶۴۶

<div class="b" id="bn1"><div class="m1"><p>روز آن است که ما خویش بر آن یار زنیم</p></div>
<div class="m2"><p>نظری سیر بر آن روی چو گلنار زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشتری وار سر زلف مه خود گیریم</p></div>
<div class="m2"><p>فتنه و غلغله اندر همه بازار زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندرافتیم در آن گلشن چون باد صبا</p></div>
<div class="m2"><p>همه بر جیب گل و جعد سمن زار زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسی کوزه زنیم و نفسی کاسه خوریم</p></div>
<div class="m2"><p>تا سبووار همه بر خم خمار زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به کی نامه بخوانیم گه جام رسید</p></div>
<div class="m2"><p>نامه را یک نفسی در سر دستار زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنگ اقبال ز فر رخ تو ساخته شد</p></div>
<div class="m2"><p>واجب آید که دو سه زخمه بر آن تار زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت شور آمد و هنگام نگه داشت نماند</p></div>
<div class="m2"><p>ما که مستیم چه دانیم چه مقدار زنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک زر می شود اندر کف اخوان صفا</p></div>
<div class="m2"><p>خاک در دیده این عالم غدار زنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کشانند سوی میمنه ما را به طناب</p></div>
<div class="m2"><p>خیمه عشرت از این بار در اسرار زنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد جهان روشن و خوش از رخ آتشرویی</p></div>
<div class="m2"><p>خیز تا آتش در مکسبه و کار زنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پاره پاره شود و زنده شود چون که طور</p></div>
<div class="m2"><p>گر ز برق دل خود بر که و کهسار زنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هله باقیش تو گو که به وجود چو توی</p></div>
<div class="m2"><p>سرد و حیف است که ما حلقه گفتار زنیم</p></div></div>