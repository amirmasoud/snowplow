---
title: >-
    غزل شمارهٔ ۳۰۲
---
# غزل شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>در هوایت بی‌قرارم روز و شب</p></div>
<div class="m2"><p>سر ز پایت برندارم روز و شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب را همچو خود مجنون کنم</p></div>
<div class="m2"><p>روز و شب را کی گذارم روز و شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان و دل از عاشقان می‌خواستند</p></div>
<div class="m2"><p>جان و دل را می‌سپارم روز و شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نیابم آن چه در مغز منست</p></div>
<div class="m2"><p>یک زمانی سر نخارم روز و شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که عشقت مطربی آغاز کرد</p></div>
<div class="m2"><p>گاه چنگم گاه تارم روز و شب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌زنی تو زخمه و بر می‌رود</p></div>
<div class="m2"><p>تا به گردون زیر و زارم روز و شب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقیی کردی بشر را چل صبوح</p></div>
<div class="m2"><p>زان خمیر اندر خمارم روز و شب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مهار عاشقان در دست تو</p></div>
<div class="m2"><p>در میان این قطارم روز و شب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌کشم مستانه بارت بی‌خبر</p></div>
<div class="m2"><p>همچو اشتر زیر بارم روز و شب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بنگشایی به قندت روزه‌ام</p></div>
<div class="m2"><p>تا قیامت روزه دارم روز و شب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ز خوان فضل روزه بشکنم</p></div>
<div class="m2"><p>عید باشد روزگارم روز و شب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان روز و جان شب ای جان تو</p></div>
<div class="m2"><p>انتظارم انتظارم روز و شب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا به سالی نیستم موقوف عید</p></div>
<div class="m2"><p>با مه تو عیدوارم روز و شب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان شبی که وعده کردی روز بعد</p></div>
<div class="m2"><p>روز و شب را می‌شمارم روز و شب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس که کشت مهر جانم تشنه است</p></div>
<div class="m2"><p>ز ابر دیده اشکبارم روز و شب</p></div></div>