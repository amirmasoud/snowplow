---
title: >-
    غزل شمارهٔ ۲۸۰۱
---
# غزل شمارهٔ ۲۸۰۱

<div class="b" id="bn1"><div class="m1"><p>عاشقان را آتشی وآنگه چه پنهان آتشی</p></div>
<div class="m2"><p>وز برای امتحان بر نقد مردان آتشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ سلطان می‌نهند اندر دل مردان عشق</p></div>
<div class="m2"><p>تخت سلطان در میان و گرد سلطان آتشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابش تافته در روزن هر عاشقی</p></div>
<div class="m2"><p>ما پریشان ذره وار اندر پریشان آتشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الصلا ای عاشقان کاین عشق خوانی گسترید</p></div>
<div class="m2"><p>بهر آتشخوارگانش بر سر خوان آتشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس این آتش بزد بر آینه گردون و شد</p></div>
<div class="m2"><p>هر طرف از اختران بر چرخ گردان آتشی</p></div></div>