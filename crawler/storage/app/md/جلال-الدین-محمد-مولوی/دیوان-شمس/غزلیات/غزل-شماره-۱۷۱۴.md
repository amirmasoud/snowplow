---
title: >-
    غزل شمارهٔ ۱۷۱۴
---
# غزل شمارهٔ ۱۷۱۴

<div class="b" id="bn1"><div class="m1"><p>چند روی بی‌خبر آخر بنگر به بام</p></div>
<div class="m2"><p>بام چه باشد بگو بر فلک سبزفام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا قمری همچو جان جلوه شود ناگهان</p></div>
<div class="m2"><p>صد مه و صد آفتاب چهره او را غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هوس عشق او چرخ زند نه فلک</p></div>
<div class="m2"><p>وز می او جان و دل نوش کند جام جام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به تجلی بتافت جانب جان‌ها شتافت</p></div>
<div class="m2"><p>باده جان شد مباح خوردن و خفتن حرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت جهان سلیم چیست خبر ای نسیم</p></div>
<div class="m2"><p>گفت ندارم ز بیم جز نفسی والسلام</p></div></div>