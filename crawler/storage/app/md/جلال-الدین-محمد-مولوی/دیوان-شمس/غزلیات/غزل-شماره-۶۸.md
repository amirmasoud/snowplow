---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>چو شست عشق در جانم شناسا گشت شستش را</p></div>
<div class="m2"><p>به شست عشق دست آورد جان بت پرستش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گوش دل بگفت اقبال رست آن جان به عشق ما</p></div>
<div class="m2"><p>بکرد این دل هزاران جان نثار آن گفت رستش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غیرت چونک جان افتاد گفت اقبال هم نجهد</p></div>
<div class="m2"><p>نشستست این دل و جانم همی‌پاید نجستش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو اندر نیستی هستست و در هستی نباشد هست</p></div>
<div class="m2"><p>بیامد آتشی در جان بسوزانید هستش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برات عمر جان اقبال چون برخواند پنجه شصت</p></div>
<div class="m2"><p>تراشید و ابد بنوشت بر طومار شصتش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدیو روح شمس الدین که از بسیاری رفعت</p></div>
<div class="m2"><p>نداند جبرئیل وحی خود جای نشستش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو جامش دید این عقلم چو قرابه شد اشکسته</p></div>
<div class="m2"><p>درستی‌های بی‌پایان ببخشید آن شکستش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عشقش دید جانم را به بالای‌یست از این هستی</p></div>
<div class="m2"><p>بلندی داد از اقبال او بالا و پستش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه شیرگیری تو دلا می‌ترس از آن آهو</p></div>
<div class="m2"><p>که شیرانند بیچاره مر آن آهوی مستش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو از تیغ حیات انگیز زد مر مرگ را گردن</p></div>
<div class="m2"><p>فروآمد ز اسپ اقبال و می‌بوسید دستش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آن روزی که در عالم الست آمد ندا از حق</p></div>
<div class="m2"><p>بده تبریز از اول بلی گویان الستش را</p></div></div>