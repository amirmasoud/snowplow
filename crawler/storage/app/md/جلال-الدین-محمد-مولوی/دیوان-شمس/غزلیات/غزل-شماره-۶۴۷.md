---
title: >-
    غزل شمارهٔ ۶۴۷
---
# غزل شمارهٔ ۶۴۷

<div class="b" id="bn1"><div class="m1"><p>تدبیر کند بنده و تقدیر نداند</p></div>
<div class="m2"><p>تدبیر به تقدیر خداوند چه ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده چو بیندیشد پیداست چه بیند</p></div>
<div class="m2"><p>حیلت بکند لیک خدایی بنداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گامی دو چنان آید کو راست نهادست</p></div>
<div class="m2"><p>وان گاه که داند که کجاهاش کشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>استیزه مکن مملکت عشق طلب کن</p></div>
<div class="m2"><p>کاین مملکتت از ملک الموت رهاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شه را تو شکاری شو کم گیر شکاری</p></div>
<div class="m2"><p>کاشکار تو را باز اجل بازستاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خامش کن و بگزین تو یکی جای قراری</p></div>
<div class="m2"><p>کان جا که گزینی ملک آن جات نشاند</p></div></div>