---
title: >-
    غزل شمارهٔ ۲۸۲۵
---
# غزل شمارهٔ ۲۸۲۵

<div class="b" id="bn1"><div class="m1"><p>مثل ذره روزن همگان گشته هوایی</p></div>
<div class="m2"><p>که تو خورشیدشمایل به سر بام برآیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه ذرات پریشان ز تو کالیوه و شادان</p></div>
<div class="m2"><p>همه دستک زن و گویان که تو در خانه مایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه در نور نهفته همه در لطف تو خفته</p></div>
<div class="m2"><p>غلط انداز بگفته که خدایا تو کجایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه همخوابه رحمت همه پرورده نعمت</p></div>
<div class="m2"><p>همه شه زاده دولت شده در لبس گدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من این وصل بدیدم همه آفاق دویدم</p></div>
<div class="m2"><p>طلبیدم نشنیدم که چه بد نام جدایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر این نام نقیبی بود از رشک رقیبی</p></div>
<div class="m2"><p>چه رقیبی چه نقیبی همه مکر است و دغایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز از روح بقایی به جز از خوب لقایی</p></div>
<div class="m2"><p>مده از جهل گوایی هله تا ژاژ نخایی</p></div></div>