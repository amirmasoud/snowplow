---
title: >-
    غزل شمارهٔ ۶۴۳
---
# غزل شمارهٔ ۶۴۳

<div class="b" id="bn1"><div class="m1"><p>در کوی خرابات مرا عشق کشان کرد</p></div>
<div class="m2"><p>آن دلبر عیار مرا دید نشان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در پی آن دلبر عیار برفتم</p></div>
<div class="m2"><p>او روی خود آن لحظه ز من باز نهان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من در عجب افتادم از آن قطب یگانه</p></div>
<div class="m2"><p>کز یک نظرش جمله وجودم همه جان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگاه یک آهو به دو صد رنگ عیان شد</p></div>
<div class="m2"><p>کز تابش حسنش مه و خورشید فغان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن آهوی خوش ناف به تبریز روان گشت</p></div>
<div class="m2"><p>بغداد جهان را به بصیرت همدان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کس که ورا کرد به تقلید سجودی</p></div>
<div class="m2"><p>فرخنده و بگزیده و محبوب زمان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن‌ها که بگفتند که ما کامل و فردیم</p></div>
<div class="m2"><p>سرگشته و سودایی و رسوای جهان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلطان عرفناک بدش محرم اسرار</p></div>
<div class="m2"><p>تا سر تجلی ازل جمله بیان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس الحق تبریز چو بگشاد پر عشق</p></div>
<div class="m2"><p>جبریل امین را ز پی خویش دوان کرد</p></div></div>