---
title: >-
    غزل شمارهٔ ۱۴۲۷
---
# غزل شمارهٔ ۱۴۲۷

<div class="b" id="bn1"><div class="m1"><p>من از اقلیم بالایم سر عالم نمی‌دارم</p></div>
<div class="m2"><p>نه از آبم نه از خاکم سر عالم نمی‌دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بالاست پراختر وگر دریاست پرگوهر</p></div>
<div class="m2"><p>وگر صحراست پرعبهر سر آن هم نمی‌دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا گویی ظریفی کن دمی با ما حریفی کن</p></div>
<div class="m2"><p>مرا گفته‌ست لاتسکن تو را همدم نمی‌دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چون دایه فضلش به شیر لطف پرورده‌ست</p></div>
<div class="m2"><p>چو من مخمور آن شیرم سر زمزم نمی‌دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن شربت که جان سازد دل مشتاق جان بازد</p></div>
<div class="m2"><p>خرد خواهد که دریازد منش محرم نمی‌دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شادی‌ها چو بیزارم سر غم از کجا دارم</p></div>
<div class="m2"><p>به غیر یار دلدارم خوش و خرم نمی‌دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی آن خمر چون عندم شکم بر روزه می بندم</p></div>
<div class="m2"><p>که من آن سرو آزادم که برگ غم نمی‌دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درافتادم در آب جو شدم شسته ز رنگ و بو</p></div>
<div class="m2"><p>ز عشق ذوق زخم او سر مرهم نمی‌دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو روز و شب دو مرکب دان یکی اشهب یکی ادهم</p></div>
<div class="m2"><p>بر اشهب بر نمی‌شینم سر ادهم نمی‌دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز این منهاج روز و شب بود عشاق را مذهب</p></div>
<div class="m2"><p>که بر مسلک به زیر این کهن طارم نمی‌دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به باغ عشق مرغانند سوی بی‌سویی پران</p></div>
<div class="m2"><p>من ایشان را سلیمانم ولی خاتم نمی‌دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منم عیسی خوش خنده که شد عالم به من زنده</p></div>
<div class="m2"><p>ولی نسبت ز حق دارم من از مریم نمی‌دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز عشق این حرف بشنیدم خموشی راه خود دیدم</p></div>
<div class="m2"><p>بگو عشقا که من با دوست لا و لم نمی‌دارم</p></div></div>