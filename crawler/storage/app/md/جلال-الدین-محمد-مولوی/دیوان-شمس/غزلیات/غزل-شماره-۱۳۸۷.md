---
title: >-
    غزل شمارهٔ ۱۳۸۷
---
# غزل شمارهٔ ۱۳۸۷

<div class="b" id="bn1"><div class="m1"><p>هین خیره خیره می نگر اندر رخ صفراییم</p></div>
<div class="m2"><p>هر کس که او مکی بود داند که من بطحاییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان لاله روی دلستان روید ز رویم زعفران</p></div>
<div class="m2"><p>هر لحظه زان شادی فزا بیش است کارافزاییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانند برف آمد دلم هر لحظه می کاهد دلم</p></div>
<div class="m2"><p>آن جا همی‌خواهد دلم زیرا که من آن جاییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا حیاتی بیشتر مردم در او بی‌خویشتر</p></div>
<div class="m2"><p>خواهی بیا در من نگر کز شید جان شیداییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن برف گوید دم به دم بگذارم و سیلی شوم</p></div>
<div class="m2"><p>غلطان سوی دریا روم من بحری و دریاییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنها شدم راکد شدم بفسردم و جامد شدم</p></div>
<div class="m2"><p>تا زیر دندان بلا چون برف و یخ می خاییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون آب باش و بی‌گره از زخم دندان‌ها بجه</p></div>
<div class="m2"><p>من تا گره دارم یقین می کوبی و می ساییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برف آب را بگذار هین فقاع‌های خاص بین</p></div>
<div class="m2"><p>می جوشد و بر می جهد که تیزم و غوغاییم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر لحظه بخروشانترم برجسته و جوشانترم</p></div>
<div class="m2"><p>چون عقل بی‌پر می پرم زیرا چو جان بالاییم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسیار گفتم ای پدر دانم که دانی این قدر</p></div>
<div class="m2"><p>که چون نیم بی‌پا و سر در پنجه آن ناییم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر تو ملولستی ز من بنگر در آن شاه زمن</p></div>
<div class="m2"><p>تا گرم و شیرینت کند آن دلبر حلواییم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای بی‌نوایان را نوا جان ملولان را دوا</p></div>
<div class="m2"><p>پران کننده جان که من از قافم و عنقاییم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من بس کنم بس از حنین او بس نخواهد کرد از این</p></div>
<div class="m2"><p>من طوطیم عشقش شکر هست از شکر گویاییم</p></div></div>