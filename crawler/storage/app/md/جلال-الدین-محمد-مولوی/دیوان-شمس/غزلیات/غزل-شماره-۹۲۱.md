---
title: >-
    غزل شمارهٔ ۹۲۱
---
# غزل شمارهٔ ۹۲۱

<div class="b" id="bn1"><div class="m1"><p>سخن که خیزد از جان ز جان حجاب کند</p></div>
<div class="m2"><p>ز گوهر و لب دریا زبان حجاب کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیان حکمت اگر چه شگرف مشعله ایست</p></div>
<div class="m2"><p>ز آفتاب حقایق بیان حجاب کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان کفست و صفات خداست چون دریا</p></div>
<div class="m2"><p>ز صاف بحر کف این جهان حجاب کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی‌شکاف تو کف را که تا به آب رسی</p></div>
<div class="m2"><p>به کف بحر بمنگر که آن حجاب کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نقش‌های زمین و ز آسمان مندیش</p></div>
<div class="m2"><p>که نقش‌های زمین و زمان حجاب کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برای مغز سخن قشر حرف را بشکاف</p></div>
<div class="m2"><p>که زلف‌ها ز جمال بتان حجاب کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو هر خیال که کشف حجاب پنداری</p></div>
<div class="m2"><p>بیفکنش که تو را خود همان حجاب کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشان آیت حقست این جهان فنا</p></div>
<div class="m2"><p>ولی ز خوبی حق این نشان حجاب کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شمس تبریز ار چه قراضه‌ایست وجود</p></div>
<div class="m2"><p>قراضه‌ایست که جان را ز کان حجاب کند</p></div></div>