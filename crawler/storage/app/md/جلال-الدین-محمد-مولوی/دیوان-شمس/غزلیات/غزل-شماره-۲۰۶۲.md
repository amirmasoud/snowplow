---
title: >-
    غزل شمارهٔ ۲۰۶۲
---
# غزل شمارهٔ ۲۰۶۲

<div class="b" id="bn1"><div class="m1"><p>سیر نشد چشم و دل از نظر شاه من</p></div>
<div class="m2"><p>سیر مشو هم تو نیز زین دل آگاه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشک و سقا سیر شد از جگر گرم من</p></div>
<div class="m2"><p>هیچ به جز آب نیست لذت و دلخواه من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درشکنم کوزه را پاره کنم مشک را</p></div>
<div class="m2"><p>روی به دریا نهم نیست جز این راه من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند شود تر زمین از مدد اشک من</p></div>
<div class="m2"><p>چند بسوزد فلک از تبش و آه من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند بگوید دلم وای دلم وای دل</p></div>
<div class="m2"><p>چند بگوید لبم راز شهنشاه من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو سوی بحری کز او هر نفسی موج موج</p></div>
<div class="m2"><p>آمد و اندرربود خیمه و خرگاه من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب خوشی جوش کرد نیم شب از خانه‌ام</p></div>
<div class="m2"><p>یوسف حسن اوفتاد ناگه در چاه من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آب رخ یوسفی خرمن من سیل برد</p></div>
<div class="m2"><p>دود برآمد ز دل سوخته شد کاه من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرمن من گر بسوخت باک ندارم خوشم</p></div>
<div class="m2"><p>صد چو مرا بس بود خرمن آن ماه من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل نخواهم بس است دانش و علمش مرا</p></div>
<div class="m2"><p>شمع رخ او بس است در شب بی‌گاه من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت کسی کاین سماع جاه و ادب کم کند</p></div>
<div class="m2"><p>جاه نخواهم که عشق در دو جهان جاه من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در پی هر بیت من گویم پایان رسید</p></div>
<div class="m2"><p>چون ز سرم می‌برد آن شه آگاه من</p></div></div>