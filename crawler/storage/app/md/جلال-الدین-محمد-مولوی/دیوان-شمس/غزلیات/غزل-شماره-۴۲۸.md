---
title: >-
    غزل شمارهٔ ۴۲۸
---
# غزل شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>این چنین پابند جان میدان کیست</p></div>
<div class="m2"><p>ما شدیم از دست این دستان کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق گردان کرد ساغرهای خاص</p></div>
<div class="m2"><p>عشق می‌داند که او گردان کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان حیاتی داد کوه و دشت را</p></div>
<div class="m2"><p>ای خدایا ای خدایا جان کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چه باغست این که جنت مست اوست</p></div>
<div class="m2"><p>وین بنفشه و سوسن و ریحان کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخ گل از بلبلان گویاترست</p></div>
<div class="m2"><p>سرو رقصان گشته کاین بستان کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاسمن گفتا نگویی با سمن</p></div>
<div class="m2"><p>کاین چنین نرگس ز نرگسدان کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بگفتم یاسمن خندید و گفت</p></div>
<div class="m2"><p>بیخودم من می‌ندانم کان کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌دود چون گوی زرین آفتاب</p></div>
<div class="m2"><p>ای عجب اندر خم چوگان کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماه همچون عاشقان اندر پیش</p></div>
<div class="m2"><p>فربه و لاغر شده حیران کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابر غمگین در غم و اندیشه است</p></div>
<div class="m2"><p>سر پرآتش عجب گریان کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرخ ازرق پوش روشن دل عجب</p></div>
<div class="m2"><p>روز و شب سرمست و سرگردان کیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درد هم از درد او پرسان شده</p></div>
<div class="m2"><p>کای عجب این درد بی‌درمان کیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس تبریزی گشاده‌ست این گره</p></div>
<div class="m2"><p>ای عجب این قدرت و امکان کیست</p></div></div>