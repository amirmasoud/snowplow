---
title: >-
    غزل شمارهٔ ۳۰۶۵
---
# غزل شمارهٔ ۳۰۶۵

<div class="b" id="bn1"><div class="m1"><p>شدم به سوی چه آب همچو سقایی</p></div>
<div class="m2"><p>برآمد از تک چه یوسفی معلایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبک به دامن پیراهنش زدم من دست</p></div>
<div class="m2"><p>ز بوی پیرهنش دیده گشت بینایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چاه در نظری کردم از تعجب من</p></div>
<div class="m2"><p>چه از ملاحت او گشته بود صحرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلیم روح به هر جا رسید میقاتش</p></div>
<div class="m2"><p>اگر چه کور بود گشت طور سینایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنخ ز دست رقیبی که گفت از چه دور</p></div>
<div class="m2"><p>از این سپس منم و چاه و چون تو زیبایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که زنده شود صد هزار مرده از او</p></div>
<div class="m2"><p>عجب نباشد اگر پیر گشت برنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار گنج گدای چنین عجب کانی</p></div>
<div class="m2"><p>هزار سیم نثار لطیف سیمایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان چو آینه پرنقش توست اما کو</p></div>
<div class="m2"><p>به روی خوب تو بی‌آینه تماشایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن تو گو که مرا از حلاوت لب تو</p></div>
<div class="m2"><p>نه عقل ماند و نه اندیشه‌ای و نی رایی</p></div></div>