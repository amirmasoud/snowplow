---
title: >-
    غزل شمارهٔ ۴۹۹
---
# غزل شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>عشق جز دولت و عنایت نیست</p></div>
<div class="m2"><p>جز گشاد دل و هدایت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق را بوحنیفه درس نکرد</p></div>
<div class="m2"><p>شافعی را در او روایت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لایجوز و یجوز تا اجل‌ست</p></div>
<div class="m2"><p>علم عشاق را نهایت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقان غرقه‌اند در شکراب</p></div>
<div class="m2"><p>از شکر مصر را شکایت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان مخمور چون نگوید شکر</p></div>
<div class="m2"><p>باده‌ای را که حد و غایت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که را پرغم و ترش دیدی</p></div>
<div class="m2"><p>نیست عاشق و زان ولایت نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نه هر غنچه پرده باغی‌ست</p></div>
<div class="m2"><p>غیرت و رشک را سرایت نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبتدی باشد اندر این ره عشق</p></div>
<div class="m2"><p>آنک او واقف از بدایت نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست شو نیست از خودی زیرا</p></div>
<div class="m2"><p>بتر از هستیت جنایت نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ راعی مشو رعیت شو</p></div>
<div class="m2"><p>راعیی جز سد رعایت نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس بدی بنده را کفی بالله</p></div>
<div class="m2"><p>لیکش این دانش و کفایت نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوید این مشکل و کنایاتست</p></div>
<div class="m2"><p>این صریح است این کنایت نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پای کوری به کوزه‌ای برزد</p></div>
<div class="m2"><p>گفت فراش را وقایت نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کوزه و کاسه چیست بر سر ره</p></div>
<div class="m2"><p>راه را زین خزف نقایت نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کوزه‌ها را ز راه برگیرید</p></div>
<div class="m2"><p>یا که فراش در سعایت نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت ای کور کوزه بر ره نیست</p></div>
<div class="m2"><p>لیک بر ره تو را درایت نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ره رها کرده‌ای سوی کوزه</p></div>
<div class="m2"><p>می‌روی آن به جز غوایت نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواجه جز مستی تو در ره دین</p></div>
<div class="m2"><p>آیتی ز ابتدا و غایت نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آیتی تو و طالب آیت</p></div>
<div class="m2"><p>به ز آیت طلب خود آیت نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی رهی ور نه در ره کوشش</p></div>
<div class="m2"><p>هیچ کوشنده بی‌جرایت نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چونک مثقال ذره یره است</p></div>
<div class="m2"><p>ذره زله بی‌نکایت نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ذره خیر بی‌گشادی نیست</p></div>
<div class="m2"><p>چشم بگشا اگر عمایت نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر نباتی نشانی آب است</p></div>
<div class="m2"><p>چیست کان را از او جبایت نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس کن این آب را نشانی‌هاست</p></div>
<div class="m2"><p>تشنه را حاجت وصایت نیست</p></div></div>