---
title: >-
    غزل شمارهٔ ۳۴۸
---
# غزل شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>صدایی کز کمان آید نذیریست</p></div>
<div class="m2"><p>که اغلب با صدایش زخم تیریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مؤثر را نگر در آب آثار</p></div>
<div class="m2"><p>کاثر جستن عصای هر ضریریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس لا تبصرونت تبصرونی‌ست</p></div>
<div class="m2"><p>بصر جستن ز الهام بصیریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو هر چه داری نه جویانش بودی</p></div>
<div class="m2"><p>طلب‌ها گوش گیری و بشیریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کن که طلب‌ها بیش گردد</p></div>
<div class="m2"><p>کثیرالزرع را طمع وفیریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشو نومید از ظلمی که کردی</p></div>
<div class="m2"><p>که دریای کرم توبه پذیریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گناهت را کند تسبیح و طاعات</p></div>
<div class="m2"><p>که در توبه پذیری بی‌نظیریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکسته باش و خاکی باش این جا</p></div>
<div class="m2"><p>که می‌جوید کرم هر جا فقیریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرم دامن پر از زر کرد و آورد</p></div>
<div class="m2"><p>که تا وا می‌خرد هر جا اسیریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عزیزی بخشد آن کس را که خواری‌ست</p></div>
<div class="m2"><p>بزرگی بخشد آن را که حقیریست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که هستی نیستی جوید همیشه</p></div>
<div class="m2"><p>زکات آن جا نیاید که امیریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازیرا مظهر چیزیست ضدش</p></div>
<div class="m2"><p>از این دو ضد را ضد خود ظهیریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو بر تخته سیاهی گر نویسی</p></div>
<div class="m2"><p>نهان گردد که هر دو همچو قیریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود فرقی ز تری تا ترست خط</p></div>
<div class="m2"><p>چو گردد خشک پنهان چون ضمیریست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خمش کن گر چه شرحش بی‌شمارست</p></div>
<div class="m2"><p>طبیعت‌ها عدو هر کثیریست</p></div></div>