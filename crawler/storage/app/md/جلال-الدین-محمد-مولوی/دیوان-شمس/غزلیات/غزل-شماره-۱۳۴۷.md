---
title: >-
    غزل شمارهٔ ۱۳۴۷
---
# غزل شمارهٔ ۱۳۴۷

<div class="b" id="bn1"><div class="m1"><p>سوی آن سلطان خوبان الرحیل</p></div>
<div class="m2"><p>سوی آن خورشید جانان الرحیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاروان بس گران آهنگ کرد</p></div>
<div class="m2"><p>هین سبکتر ای گرانان الرحیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی آن دریای مردی و بقا</p></div>
<div class="m2"><p>مردوار ای مردمان هان الرحیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب روی شه عالم گرفت</p></div>
<div class="m2"><p>صبح شد ای پاسبانان الرحیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو مرغان خلیلی سوی سر</p></div>
<div class="m2"><p>زانک بی‌سر نیست سامان الرحیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوی اصل خویش یعنی بحر جان</p></div>
<div class="m2"><p>جمع یاران همچو باران الرحیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شده بگلربگان ملک غیب</p></div>
<div class="m2"><p>کمترینه عاشق قان الرحیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خانه و فرزند و بستر ترک کن</p></div>
<div class="m2"><p>اسپ و استر زین و پالان الرحیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش شمس الدین تبریزی شاه</p></div>
<div class="m2"><p>خاک بی‌جان گشته با جان الرحیل</p></div></div>