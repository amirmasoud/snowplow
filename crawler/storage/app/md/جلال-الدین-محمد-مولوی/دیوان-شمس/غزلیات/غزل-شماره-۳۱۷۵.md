---
title: >-
    غزل شمارهٔ ۳۱۷۵
---
# غزل شمارهٔ ۳۱۷۵

<div class="b" id="bn1"><div class="m1"><p>کار به پیری و جوانیستی</p></div>
<div class="m2"><p>پیر بمردی و جوان زیستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانگ خر نفست اگر کم شدی</p></div>
<div class="m2"><p>دعوت عقل تو مسیحیستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نبدی خندهٔ صبح کذوب</p></div>
<div class="m2"><p>هیچ دلی زار بنگریستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بت جان روی نمودی به ما</p></div>
<div class="m2"><p>جملهٔ ذرات چو ما نیستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر توی تو نفسی کاستی</p></div>
<div class="m2"><p>همچو تو اندر دو جهان کیستی؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نبدی غیرت آن آفتاب</p></div>
<div class="m2"><p>ذره به ذره همه ساقیستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانه من از کاه جدا کردمی</p></div>
<div class="m2"><p>گر کفه را هیچ تناهیستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مار اگر آب وفا یافتی</p></div>
<div class="m2"><p>در دل آن بحر چو ماهیستی</p></div></div>