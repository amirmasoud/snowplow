---
title: >-
    غزل شمارهٔ ۲۹۲۴
---
# غزل شمارهٔ ۲۹۲۴

<div class="b" id="bn1"><div class="m1"><p>آه از عشق جمال حوره‌ای</p></div>
<div class="m2"><p>کو گرفت از عاشقانش دوره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زندگیِ نوبه‌نو از کشتنش</p></div>
<div class="m2"><p>صحتی تازه شد از رنجوره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر گهر داری ببین حال مرا</p></div>
<div class="m2"><p>در تک دریا ز دریا دوره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم: ای عقلم کجایی؟ عقل گفت:</p></div>
<div class="m2"><p>«چون شدم مِی، چون کنم انگوره‌ای؟!»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان بسوز و سرمه کن خاکسترش</p></div>
<div class="m2"><p>تا نمانَد در دو عالم کوره‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کند جان‌های بی‌جان در سماع</p></div>
<div class="m2"><p>گرد آن شهد ازل زنبوره‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کند آن شمس تبریزی به حق</p></div>
<div class="m2"><p>جمله ویران‌هات را معموره‌ای</p></div></div>