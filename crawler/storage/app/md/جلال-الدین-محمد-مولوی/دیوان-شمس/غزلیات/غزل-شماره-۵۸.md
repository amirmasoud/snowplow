---
title: >-
    غزل شمارهٔ ۵۸
---
# غزل شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>رسید آن شه رسید آن شه بیارایید ایوان را</p></div>
<div class="m2"><p>فروبرید ساعدها برای خوب کنعان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آمد جان جان جان نشاید برد نام جان</p></div>
<div class="m2"><p>به پیشش جان چه کار آید مگر از بهر قربان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدم بی‌عشق گمراهی درآمد عشق ناگاهی</p></div>
<div class="m2"><p>بدم کوهی شدم کاهی برای اسب سلطان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ترکست و تاجیکست بدو این بنده نزدیکست</p></div>
<div class="m2"><p>چو جان با تن ولیکن تن نبیند هیچ مر جان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلا یاران که بخت آمد گه ایثار رخت آمد</p></div>
<div class="m2"><p>سلیمانی به تخت آمد برای عزل شیطان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجه از جا چه می‌پایی چرا بی‌دست و بی‌پایی</p></div>
<div class="m2"><p>نمی‌دانی ز هدهد جو ره قصر سلیمان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکن آن جا مناجاتت بگو اسرار و حاجاتت</p></div>
<div class="m2"><p>سلیمان خود همی‌داند زبان جمله مرغان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن بادست ای بنده کند دل را پراکنده</p></div>
<div class="m2"><p>ولیکن اوش فرماید که گرد آور پریشان را</p></div></div>