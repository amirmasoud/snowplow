---
title: >-
    غزل شمارهٔ ۵۱۶
---
# غزل شمارهٔ ۵۱۶

<div class="b" id="bn1"><div class="m1"><p>بازرسیدیم ز میخانه مست</p></div>
<div class="m2"><p>بازرهیدیم ز بالا و پست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله مستان خوش و رقصان شدند</p></div>
<div class="m2"><p>دست زنید ای صنمان دست دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهی و دریا همه مستی کنند</p></div>
<div class="m2"><p>چونک سر زلف تو افتاده شست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر و زبر گشت خرابات ما</p></div>
<div class="m2"><p>خنب نگون گشت و قرابه شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیر خرابات چو آن شور دید</p></div>
<div class="m2"><p>بر سر بام آمد و از بام جست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوش برآورد یکی می کز او</p></div>
<div class="m2"><p>هست شود نیست شود نیست هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیشه چو بشکست و به هر سوی ریخت</p></div>
<div class="m2"><p>چند کف پای حریفان که خست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن که سر از پای نداند کجاست</p></div>
<div class="m2"><p>مست فتادست به کوی الست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باده پرستان همه در عشرتند</p></div>
<div class="m2"><p>تنتن تنتن شنو ای تن پرست</p></div></div>