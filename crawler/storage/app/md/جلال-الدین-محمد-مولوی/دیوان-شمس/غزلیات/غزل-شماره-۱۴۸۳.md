---
title: >-
    غزل شمارهٔ ۱۴۸۳
---
# غزل شمارهٔ ۱۴۸۳

<div class="b" id="bn1"><div class="m1"><p>امروز مها خویش ز بیگانه ندانیم</p></div>
<div class="m2"><p>مستیم بدان حد که ره خانه ندانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو از عاقله عقل برستیم</p></div>
<div class="m2"><p>جز حالت شوریده دیوانه ندانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باغ به جز عکس رخ دوست نبینیم</p></div>
<div class="m2"><p>وز شاخ به جز حالت مستانه ندانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتند در این دام یکی دانه نهاده‌ست</p></div>
<div class="m2"><p>در دام چنانیم که ما دانه ندانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز از این نکته و افسانه مخوانید</p></div>
<div class="m2"><p>کافسون نپذیرد دل و افسانه ندانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شانه در آن زلف چنان رفت دل ما</p></div>
<div class="m2"><p>کز بیخودی از زلف تو تا شانه ندانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده ده و کم پرس که چندم قدح است این</p></div>
<div class="m2"><p>کز یاد تو ما باده ز پیمانه ندانیم</p></div></div>