---
title: >-
    غزل شمارهٔ ۱۵۴۱
---
# غزل شمارهٔ ۱۵۴۱

<div class="b" id="bn1"><div class="m1"><p>مرا خواندی ز در تو خستی از بام</p></div>
<div class="m2"><p>زهی بازی زهی بازی زهی دام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن بازی که من می دانم و تو</p></div>
<div class="m2"><p>چه بازی‌ها تو پختستی و من خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی کز مکر و از افسوس و وعده</p></div>
<div class="m2"><p>چو خواهی سنگ و آهن را کنی رام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مها با این همه خوشی تو چونی</p></div>
<div class="m2"><p>ز زحمت‌های ما وز جور ایام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه می پرسم تو خود چون خوش نباشی</p></div>
<div class="m2"><p>که در مجلس تو داری جام بر جام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا در راه دی دشنام دادی</p></div>
<div class="m2"><p>چنین مستم ز شیرینی دشنام</p></div></div>