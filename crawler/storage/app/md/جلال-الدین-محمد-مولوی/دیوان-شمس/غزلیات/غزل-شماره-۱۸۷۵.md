---
title: >-
    غزل شمارهٔ ۱۸۷۵
---
# غزل شمارهٔ ۱۸۷۵

<div class="b" id="bn1"><div class="m1"><p>ای یار مقامردل پیش آ و دمی کم زن</p></div>
<div class="m2"><p>زخمی که زنی بر ما مردانه و محکم زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تخت نهی ما را بر سینه دریا نه</p></div>
<div class="m2"><p>ور دار زنی ما را بر گنبد اعظم زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازواج موافق را شربت ده و دم دم ده</p></div>
<div class="m2"><p>امشاج منافق را درهم زن و برهم زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکسیر لدنی را بر خاطر جامد نه</p></div>
<div class="m2"><p>مخمور یتیمی را بر جام محرم زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دیده عالم نه عدلی نو و عقلی نو</p></div>
<div class="m2"><p>وان آهوی یاهو را بر کلب معلم زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر گل بسرشته یک نفخ دگر دردم</p></div>
<div class="m2"><p>وان سنبل ناکشته بر طینت آدم زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صادق صدیقی در غار سعادت رو</p></div>
<div class="m2"><p>چون مرد مسلمانی بر ملک مسلم زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان خواسته‌ای ای جان اینک من و اینک جان</p></div>
<div class="m2"><p>جانی که تو را نبود بر قعر جهنم زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی که به هر ساعت عیسی نوی زاید</p></div>
<div class="m2"><p>زان گلشن خود بادی بر چادر مریم زن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر دار فنا خواهی تا دار بقا گردد</p></div>
<div class="m2"><p>آن آتش عمرانی در خرمن ماتم زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهی تو دو عالم را همکاسه و هم یاسه</p></div>
<div class="m2"><p>آن کحل اناالله را در عین دو عالم زن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من بس کنم اما تو ای مطرب روشن دل</p></div>
<div class="m2"><p>از زیر چو سیر آیی بر زمزمه بم زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو دشمن غم‌هایی خاموش نمی‌شایی</p></div>
<div class="m2"><p>هر لحظه یکی سنگی بر مغز سر غم زن</p></div></div>