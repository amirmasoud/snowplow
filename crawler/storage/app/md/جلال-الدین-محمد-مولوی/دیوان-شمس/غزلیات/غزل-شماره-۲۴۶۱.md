---
title: >-
    غزل شمارهٔ ۲۴۶۱
---
# غزل شمارهٔ ۲۴۶۱

<div class="b" id="bn1"><div class="m1"><p>چون دل من جست ز تن بازنگشتی چه شدی</p></div>
<div class="m2"><p>بی‌دل من بی‌دل من راست شدی هر چه بدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کژ و گر راست شدی ور کم ور کاست شدی</p></div>
<div class="m2"><p>فارغ و آزاد بدی خواجه ز هر نیک و بدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ فضولی نبدی هیچ ملولی نبدی</p></div>
<div class="m2"><p>دانش و گولی نبدی طبل تحیات زدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواجه چه گیری گروم تو نروی من بروم</p></div>
<div class="m2"><p>کهنه نه‌ام خواجه نوم در مدد اندر مددی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش و نفتم نخورد ور بخورد بازدهد</p></div>
<div class="m2"><p>چون عددی را بخورد بازدهد بی‌عددی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر خرپشته من بانگ زن ای کشته من</p></div>
<div class="m2"><p>دانک من اندر چمنم صورت من در لحدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه بود در لحدی خوش بودش با احدی</p></div>
<div class="m2"><p>آنک در آن دام بود کی خوردش دام و ددی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و آنک از او دور بود گر چه که منصور بود</p></div>
<div class="m2"><p>زارتر از مور بود ز آنک ندارد سندی</p></div></div>