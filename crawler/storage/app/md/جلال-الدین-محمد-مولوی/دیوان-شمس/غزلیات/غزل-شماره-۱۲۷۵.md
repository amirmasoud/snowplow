---
title: >-
    غزل شمارهٔ ۱۲۷۵
---
# غزل شمارهٔ ۱۲۷۵

<div class="b" id="bn1"><div class="m1"><p>چون بزند گردنم سجده کند گردنش</p></div>
<div class="m2"><p>شیر خورد خون من ذوق من از خوردنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هین هله شیر شکار پنجه ز من برمدار</p></div>
<div class="m2"><p>هین که هزاران هزار منت آن بر منش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پخته خورد پخته خوار خام خورد عشق یار</p></div>
<div class="m2"><p>خام منم ای نگار که نتوان پختنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای تو دهلزن به قل بنده تو را چون دهل</p></div>
<div class="m2"><p>در تو درآویخته همچو دهل می‌زنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش همه سرخوشان عشق کشد کش کشان</p></div>
<div class="m2"><p>عشق تو داوود توست موم شده آهنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل همه مال و عقار خرج کند در قمار</p></div>
<div class="m2"><p>چونک برهنه شود چرخ دهد مخزنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ز سخن مال مال خواست زدن پر و بال</p></div>
<div class="m2"><p>پرتو نور کمال کرد چنین الکنش</p></div></div>