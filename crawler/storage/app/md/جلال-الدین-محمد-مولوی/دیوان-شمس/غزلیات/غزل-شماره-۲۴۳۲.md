---
title: >-
    غزل شمارهٔ ۲۴۳۲
---
# غزل شمارهٔ ۲۴۳۲

<div class="b" id="bn1"><div class="m1"><p>ای رونق هر گلشنی وی روزن هر خانه‌ای</p></div>
<div class="m2"><p>هر ذره از خورشید تو تابنده چون دردانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای غوث هر بیچاره‌ای واگشت هر آواره‌ای</p></div>
<div class="m2"><p>اصلاح هر مکاره‌ای مقصود هر افسانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای حسرت سرو سهی ای رونق شاهنشهی</p></div>
<div class="m2"><p>خواهم که یاران را دهی یک یاریی یارانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر سری سودای تو در هر لبی هیهای تو</p></div>
<div class="m2"><p>بی‌فیض شربت‌های تو عالم تهی پیمانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر خسروی مسکین تو صید کمین شاهین تو</p></div>
<div class="m2"><p>وی سلسله تقلیب تو زنجیر هر دیوانه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نور را ناری بود با هر گلی خاری بود</p></div>
<div class="m2"><p>بهر حرس ماری بود بر گنج هر ویرانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای گلشنت را خار نی با نور پاکت نار نی</p></div>
<div class="m2"><p>بر گرد گنجت مار نی نی زخم و نی دندانه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک عشرتی افراشتی صد تخم فتنه کاشتی</p></div>
<div class="m2"><p>در شهر ما نگذاشتی یک عاقلی فرزانه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندیشه و فرهنگ‌ها دارد ز عشقت رنگ‌ها</p></div>
<div class="m2"><p>شب تا سحرگه چنگ‌ها ماه تو را حنانه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل و جنون آمیخته صد نعل در ره ریخته</p></div>
<div class="m2"><p>در جعد تو آویخته اندیشه همچون شانه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای چشم تو چون نرگسی شد خواب در چشمم خسی</p></div>
<div class="m2"><p>بیدار می‌بینم بسی لیک از پی دانگانه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بقال با دوغ ترش جانش مراقب لب خمش</p></div>
<div class="m2"><p>تا روز بیدار و به هش بر گوشه دکانه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون روز گردد می‌دود از بهر کسب و بهر کد</p></div>
<div class="m2"><p>تا خشک نانه او شود مشتری ترنانه‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای مزرعه بگذاشته در شوره گندم کاشته</p></div>
<div class="m2"><p>ای شعله را پنداشته روزن تو چون پروانه‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امروز تشریفت دهد تفهیم و تشریفت دهد</p></div>
<div class="m2"><p>ترکیب و تألیفت دهد با عقل کل جانانه‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خامش که تو زین رسته‌ای زین دام‌ها برجسته‌ای</p></div>
<div class="m2"><p>جان و دل اندربسته‌ای در دلبری فتانه‌ای</p></div></div>