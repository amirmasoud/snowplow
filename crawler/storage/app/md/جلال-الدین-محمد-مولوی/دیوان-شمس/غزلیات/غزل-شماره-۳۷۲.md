---
title: >-
    غزل شمارهٔ ۳۷۲
---
# غزل شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>من سر نخورم که سر گران‌ست</p></div>
<div class="m2"><p>پاچه نخورم که استخوان‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بریان نخورم که هم زیان‌ست</p></div>
<div class="m2"><p>من نور خورم که قوت جان‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من سر نخوهم که باکلاهند</p></div>
<div class="m2"><p>من زر نخوهم که بازخواهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من خر نخوهم که بند کاهند</p></div>
<div class="m2"><p>من کبک خورم که صید شاهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بالا نپرم نه لک لکم من</p></div>
<div class="m2"><p>کس را نگزم که نی سگم من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لنگی نکنم نه بدتکم من</p></div>
<div class="m2"><p>که عاشق روی ایبکم من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترشی نکنم نه سرکه‌ام من</p></div>
<div class="m2"><p>پرنم نشوم نه برکه‌ام من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرکش نشوم نه عکه‌ام من</p></div>
<div class="m2"><p>قانع بزیم که مکه‌ام من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستار مرا گرو نهادی</p></div>
<div class="m2"><p>یک کوزه مثلثم ندادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انصاف بده عوان نژادی</p></div>
<div class="m2"><p>ما را کم نیست هیچ شادی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سالار دهی و خواجه ده</p></div>
<div class="m2"><p>آن باده که گفته‌ای به من ده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور دفع دهی تو و برون جه</p></div>
<div class="m2"><p>در کس زنان خویشتن نه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من عشق خورم که خوشگوارست</p></div>
<div class="m2"><p>ذوق دهنست و نشو جان‌ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوردم ز ثرید و پاچه یک چند</p></div>
<div class="m2"><p>از پاچه سر مرا زیانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زین پس سر پاچه نیست ما را</p></div>
<div class="m2"><p>ما را و کسی که اهل خوانست</p></div></div>