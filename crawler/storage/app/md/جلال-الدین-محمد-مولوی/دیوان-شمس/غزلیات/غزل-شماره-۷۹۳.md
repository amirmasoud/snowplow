---
title: >-
    غزل شمارهٔ ۷۹۳
---
# غزل شمارهٔ ۷۹۳

<div class="b" id="bn1"><div class="m1"><p>هست مستی که مرا جانب میخانه برد</p></div>
<div class="m2"><p>جانب ساقی گلچهره دردانه برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست مستی که کشد گوش مرا یارانه</p></div>
<div class="m2"><p>از چنین صف نعالم سوی پیشانه برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعل آنست که بوسه گه او خاک بود</p></div>
<div class="m2"><p>لعل آنست که سوی می و پیمانه برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان سپاریم بدان باده جان دست نهیم</p></div>
<div class="m2"><p>پیشتر زانک خردمان سوی افسانه برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخ شاخست دل از رنگ سر زلف خوشش</p></div>
<div class="m2"><p>تا چرا بند چنان موسی سر شانه برد</p></div></div>