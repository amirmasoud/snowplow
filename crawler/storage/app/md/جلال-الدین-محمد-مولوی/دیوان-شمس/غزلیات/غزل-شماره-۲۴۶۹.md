---
title: >-
    غزل شمارهٔ ۲۴۶۹
---
# غزل شمارهٔ ۲۴۶۹

<div class="b" id="bn1"><div class="m1"><p>آه خجسته ساعتی که صنما به من رسی</p></div>
<div class="m2"><p>پاک و لطیف همچو جان صبحدمی به تن رسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سر زلف سرکشت گفته مرا که شب خوشت</p></div>
<div class="m2"><p>زین سفر چو آتشت کی تو بدین وطن رسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی بود آفتاب تو در دل چون حمل رسد</p></div>
<div class="m2"><p>تا تو چو آب زندگی بر گل و بر سمن رسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو حسن ز دست غم جرعه زهر می‌کشم</p></div>
<div class="m2"><p>ای تریاق احمدی کی تو به بوالحسن رسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه غمت به خون من چابک و تیز می‌رود</p></div>
<div class="m2"><p>هست امید جان که تو در غم دل شکن رسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله تو باشی آن زمان دل شده باشد از میان</p></div>
<div class="m2"><p>پاک شود بدن چو جان چون تو بدین بدن رسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ فروسکل تو خوش ننگ فلک دگر مکش</p></div>
<div class="m2"><p>بوک به بوی طره‌اش بر سر آن رسن رسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زن ز زنی برون شود مرد میان خون شود</p></div>
<div class="m2"><p>چون تو به حسن لم یزل بر سر مرد و زن رسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن تو پای درنهد یوسف مصر سر نهد</p></div>
<div class="m2"><p>مرده ز گور برجهد چون به سر کفن رسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لطف خیال شمس دین از تبریز در کمین</p></div>
<div class="m2"><p>طالب جان شوی چو دین تا به چه شکل و فن رسی</p></div></div>