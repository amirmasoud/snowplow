---
title: >-
    غزل شمارهٔ ۱۵۶۴
---
# غزل شمارهٔ ۱۵۶۴

<div class="b" id="bn1"><div class="m1"><p>روزی که گذر کنی به گورم</p></div>
<div class="m2"><p>یاد آور از این نفیر و شورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرنور کن آن تک لحد را</p></div>
<div class="m2"><p>ای دیده و ای چراغ نورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا از تو سجود شکر آرد</p></div>
<div class="m2"><p>اندر لحد این تن صبورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خرمن گل شتاب مگذار</p></div>
<div class="m2"><p>خوش کن نفسی بدان بخورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان گاه که بگذری مینگار</p></div>
<div class="m2"><p>کز روزن و درگه تو دورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سنگ لحد ببست راهم</p></div>
<div class="m2"><p>از راه خیال بی‌فتورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صد کفنم بود ز اطلس</p></div>
<div class="m2"><p>بی‌خلعت صورت تو عورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صحن سرای تو برآیم</p></div>
<div class="m2"><p>در نقب زنی مگر که مورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من مور توام تویی سلیمان</p></div>
<div class="m2"><p>یک دم مگذار بی‌حضورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خامش کردم بگو تو باقی</p></div>
<div class="m2"><p>کز گفت و شنود خود نفورم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس تبریز دعوتم کن</p></div>
<div class="m2"><p>چون دعوت توست نفخ صورم</p></div></div>