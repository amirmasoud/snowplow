---
title: >-
    غزل شمارهٔ ۱۵۲۴
---
# غزل شمارهٔ ۱۵۲۴

<div class="b" id="bn1"><div class="m1"><p>چو آب آهسته زیر که درآیم</p></div>
<div class="m2"><p>به ناگه خرمن که درربایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چکم از ناودان من قطره قطره</p></div>
<div class="m2"><p>چو طوفان من خراب صد سرایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرا چه بود فلک را برشکافم</p></div>
<div class="m2"><p>ز بی‌صبری قیامت را نپایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلا را من علف بودم ز اول</p></div>
<div class="m2"><p>ولیک اکنون بلاها را بلایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حبس جا میابا دل رهایی</p></div>
<div class="m2"><p>اگر من واقفم که من کجایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر نخلم ندانی کز چه سوی است</p></div>
<div class="m2"><p>در این آب ار نگونت می نمایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه قلماشی است لیکن ماند آن را</p></div>
<div class="m2"><p>نه هجوی می کنم نی می ستایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم عشق است و عشق از لطف پنهان</p></div>
<div class="m2"><p>ولی من از غلیظی‌های هایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو که را اگر آرد صدایی</p></div>
<div class="m2"><p>که‌ای که نامدی گفتی که آیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو او را گو که بانگ که از او بود</p></div>
<div class="m2"><p>زهی گوینده بی‌منتهایم</p></div></div>