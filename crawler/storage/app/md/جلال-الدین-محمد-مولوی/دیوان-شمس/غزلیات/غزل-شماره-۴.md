---
title: >-
    غزل شمارهٔ ۴
---
# غزل شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای یوسف خوش نام ما خوش می‌روی بر بام ما</p></div>
<div class="m2"><p>ای درشکسته جام ما ای بردریده دام ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نور ما ای سور ما ای دولت منصور ما</p></div>
<div class="m2"><p>جوشی بنه در شور ما تا می شود انگور ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دلبر و مقصود ما ای قبله و معبود ما</p></div>
<div class="m2"><p>آتش زدی در عود ما نظاره کن در دود ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای یار ما عیار ما دام دل خمار ما</p></div>
<div class="m2"><p>پا وامکش از کار ما بستان گرو دستار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گل بمانده پای دل جان می‌دهم چه جای دل</p></div>
<div class="m2"><p>وز آتش سودای دل ای وای دل ای وای ما</p></div></div>