---
title: >-
    غزل شمارهٔ ۱۵۰۷
---
# غزل شمارهٔ ۱۵۰۷

<div class="b" id="bn1"><div class="m1"><p>ایا یاری که در تو ناپدیدم</p></div>
<div class="m2"><p>تو را شکل عجب در خواب دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خاتونان مصر از عشق یوسف</p></div>
<div class="m2"><p>ترنج و دست بیخود می بریدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا آن مه کجا آن چشم دوشین</p></div>
<div class="m2"><p>کجا آن گوش کان‌ها می شنیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تو پیدا نه من پیدا نه آن دم</p></div>
<div class="m2"><p>نه آن دندان که لب را می گزیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم انبار آکنده ز سودا</p></div>
<div class="m2"><p>کز آن خرمن همه سودا کشیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو آرام دل سوداییانی</p></div>
<div class="m2"><p>تو ذاالنون و جنید و بایزیدم</p></div></div>