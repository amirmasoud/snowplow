---
title: >-
    غزل شمارهٔ ۱۵۵
---
# غزل شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>از فراق شمس دین افتاده‌ام در تنگنا</p></div>
<div class="m2"><p>او مسیح روزگار و درد چشمم بی‌دوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه درد عشق او خود راحت جان منست</p></div>
<div class="m2"><p>خون جانم گر بریزد او بود صد خونبها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل آواره شده دوش آمد و حلقه بزد</p></div>
<div class="m2"><p>من بگفتم کیست بر در باز کن در اندرآ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت آخر چون درآید خانه تا سر آتشست</p></div>
<div class="m2"><p>می‌بسوزد هر دو عالم را ز آتش‌های لا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش تو غم مخور پا اندرون نه مردوار</p></div>
<div class="m2"><p>تا کند پاکت ز هستی هست گردی ز اجتبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت بینی مکن تا عاقبت بینی شوی</p></div>
<div class="m2"><p>تا چو شیر حق باشی در شجاعت لافتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ببینی هستیت چون از عدم سر برزند</p></div>
<div class="m2"><p>روح مطلق کامکار و شهسوار هل اتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله عشق و جمله لطف و جمله قدرت جمله دید</p></div>
<div class="m2"><p>گشته در هستی شهید و در عدم او مرتضی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن عدم نامی که هستی موج‌ها دارد از او</p></div>
<div class="m2"><p>کز نهیب و موج او گردان شد صد آسیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندر آن موج اندرآیی چون بپرسندت از این</p></div>
<div class="m2"><p>تو بگویی صوفیم صوفی بخواند مامضی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از میان شمع بینی برفروزد شمع تو</p></div>
<div class="m2"><p>نور شمعت اندرآمیزد به نور اولیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مر تو را جایی برد آن موج دریا در فنا</p></div>
<div class="m2"><p>دررباید جانت را او از سزا و ناسزا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک از آسیب جانت وز صفای سینه‌ات</p></div>
<div class="m2"><p>بی تو داده باغ هستی را بسی نشو و نما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در جهان محو باشی هست مطلق کامران</p></div>
<div class="m2"><p>در حریم محو باشی پیشوا و مقتدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیده‌های کون در رویت نیارد بنگرید</p></div>
<div class="m2"><p>تا که نجهد دیده‌اش از شعشعه آن کبریا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ناگهان گردی بخیزد زان سوی محو فنا</p></div>
<div class="m2"><p>که تو را وهمی نبوده زان طریق ماورا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شعله‌های نور بینی از میان گردها</p></div>
<div class="m2"><p>محو گردد نور تو از پرتو آن شعله‌ها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زو فروآ تو ز تخت و سجده‌ای کن زانک هست</p></div>
<div class="m2"><p>آن شعاع شمس دین شهریار اصفیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور کسی منکر شود اندر جبین او نگر</p></div>
<div class="m2"><p>تا ببینی داغ فرعونی بر آن جا قد طغی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا نیارد سجده‌ای بر خاک تبریز صفا</p></div>
<div class="m2"><p>کم نگردد از جبینش داغ نفرین خدا</p></div></div>