---
title: >-
    غزل شمارهٔ ۲۳۶۱
---
# غزل شمارهٔ ۲۳۶۱

<div class="b" id="bn1"><div class="m1"><p>ای روز مبارک و خجسته</p></div>
<div class="m2"><p>ما جمع و تو در میان نشسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای همنفس همیشه پیش آ</p></div>
<div class="m2"><p>تا زنده شود دمی شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیغام دل است این دو سه حرف</p></div>
<div class="m2"><p>بشنو سخن شکسته بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک بار بگو که بنده من</p></div>
<div class="m2"><p>کزاد شوم ز رنج و رسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دست ز روی خویش برگیر</p></div>
<div class="m2"><p>تا گل چینیم دسته دسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک بار دگر شکرفشان کن</p></div>
<div class="m2"><p>طوطی نگر از قفس برسته</p></div></div>