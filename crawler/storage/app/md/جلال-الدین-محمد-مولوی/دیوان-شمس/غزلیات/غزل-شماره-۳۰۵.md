---
title: >-
    غزل شمارهٔ ۳۰۵
---
# غزل شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>آواز داد اختر بس روشنست امشب</p></div>
<div class="m2"><p>گفتم ستارگان را مه با منست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بررو به بام بالا از بهر الصلا را</p></div>
<div class="m2"><p>گل چیدنست امشب می خوردنست امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا روز دلبر ما اندر برست چون دل</p></div>
<div class="m2"><p>دستش به مهر ما را در گردنست امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا روز زنگیان را با روم دار و گیرست</p></div>
<div class="m2"><p>تا روز چنگیان را تنتن تنست امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا روز ساغر می در گردش است و بخشش</p></div>
<div class="m2"><p>تا روز گل به خلوت با سوسنست امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امشب شراب وصلت بر خاص و عام ریزم</p></div>
<div class="m2"><p>شادی آنک ماهت بر روزنست امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داوودوار ما را آهن چو موم گردد</p></div>
<div class="m2"><p>کهن رباست دلبر دل آهنست امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگشای دست دل را تا پای عشق کوبد</p></div>
<div class="m2"><p>کان زار ترس دیده در مأمنست امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر روی چون زر من ای بخت بوسه می‌ده</p></div>
<div class="m2"><p>کاین زر گازدیده در معدنست امشب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کو به مکر و دانش می‌بست راه ما را</p></div>
<div class="m2"><p>پالان خر بر او نه کو کودنست امشب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمشیر آبدارش پوسیده است و چوبین</p></div>
<div class="m2"><p>وان نیزه درازش چون سوزنست امشب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرگاه عنکبوتست آن قلعه حصینش</p></div>
<div class="m2"><p>برگستوان و خودش چون روغنست امشب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاموش کن که طامع الکن بود همیشه</p></div>
<div class="m2"><p>با او چه بحث داری کو الکنست امشب</p></div></div>