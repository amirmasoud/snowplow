---
title: >-
    غزل شمارهٔ ۱۰۰۰
---
# غزل شمارهٔ ۱۰۰۰

<div class="b" id="bn1"><div class="m1"><p>آنچ گل سرخ قبا می‌کند</p></div>
<div class="m2"><p>دانم من کان ز کجا می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بید پیاده که کشیدست صف</p></div>
<div class="m2"><p>آنچ گذشتست قضا می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوسن با تیغ و سمن با سپر</p></div>
<div class="m2"><p>هر یک تکبیر غزا می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل مسکین که چه‌ها می‌کشد</p></div>
<div class="m2"><p>آه از آن گل که چه‌ها می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوید هر یک ز عروسان باغ</p></div>
<div class="m2"><p>کان گل اشارت سوی ما می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوید بلبل که گل آن شیوه‌ها</p></div>
<div class="m2"><p>بهر من بی‌سر و پا می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست برآورده به زاری چنار</p></div>
<div class="m2"><p>با تو بگویم چه دعا می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر غنچه کی کله می‌نهد</p></div>
<div class="m2"><p>پشت بنفشه کی دوتا می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه خزان کرد جفاها بسی</p></div>
<div class="m2"><p>بین که بهاران چه وفا می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فصل خزان آنچ به تاراج برد</p></div>
<div class="m2"><p>فصل بهار آمد ادا می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ذکر گل و بلبل و خوبان باغ</p></div>
<div class="m2"><p>جمله بهانه‌ست چرا می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غیرت عشق است وگر نه زبان</p></div>
<div class="m2"><p>شرح عنایات خدا می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مفخر تبریز و جهان شمس دین</p></div>
<div class="m2"><p>باز مراعات شما می‌کند</p></div></div>