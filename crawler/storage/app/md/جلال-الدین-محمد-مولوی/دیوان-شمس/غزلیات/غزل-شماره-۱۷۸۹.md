---
title: >-
    غزل شمارهٔ ۱۷۸۹
---
# غزل شمارهٔ ۱۷۸۹

<div class="b" id="bn1"><div class="m1"><p>ای عاشقان ای عاشقان هنگام کوچ است از جهان</p></div>
<div class="m2"><p>در گوش جانم می رسد طبل رحیل از آسمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نک ساربان برخاسته قطارها آراسته</p></div>
<div class="m2"><p>از ما حلالی خواسته چه خفته‌اید ای کاروان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این بانگ‌ها از پیش و پس بانگ رحیل است و جرس</p></div>
<div class="m2"><p>هر لحظه‌ای نفس و نفس سر می کشد در لامکان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین شمع‌های سرنگون زین پرده‌های نیلگون</p></div>
<div class="m2"><p>خلقی عجب آید برون تا غیب‌ها گردد عیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین چرخ دولابی تو را آمد گران خوابی تو را</p></div>
<div class="m2"><p>فریاد از این عمر سبک زنهار از این خواب گران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل سوی دلدار شو ای یار سوی یار شو</p></div>
<div class="m2"><p>ای پاسبان بیدار شو خفته نشاید پاسبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سوی شمع و مشعله هر سوی بانگ و مشغله</p></div>
<div class="m2"><p>کامشب جهان حامله زاید جهان جاودان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو گل بدی و دل شدی جاهل بدی عاقل شدی</p></div>
<div class="m2"><p>آن کو کشیدت این چنین آن سو کشاند کش کشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر کشاکش‌های او نوش است ناخوش‌های او</p></div>
<div class="m2"><p>آب است آتش‌های او بر وی مکن رو را گران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جان نشستن کار او توبه شکستن کار او</p></div>
<div class="m2"><p>از حیله بسیار او این ذره‌ها لرزان دلان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای ریش خند رخنه جه یعنی منم سالار ده</p></div>
<div class="m2"><p>تا کی جهی گردن بنه ور نی کشندت چون کمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تخم دغل می کاشتی افسوس‌ها می داشتی</p></div>
<div class="m2"><p>حق را عدم پنداشتی اکنون ببین ای قلتبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای خر به کاه اولیتری دیگی سیاه اولیتری</p></div>
<div class="m2"><p>در قعر چاه اولیتری ای ننگ خانه و خاندان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در من کسی دیگر بود کاین خشم‌ها از وی جهد</p></div>
<div class="m2"><p>گر آب سوزانی کند ز آتش بود این را بدان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در کف ندارم سنگ من با کس ندارم جنگ من</p></div>
<div class="m2"><p>با کس نگیرم تنگ من زیرا خوشم چون گلستان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس خشم من زان سر بود وز عالم دیگر بود</p></div>
<div class="m2"><p>این سو جهان آن سو جهان بنشسته من بر آستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر آستان آن کس بود کو ناطق اخرس بود</p></div>
<div class="m2"><p>این رمز گفتی بس بود دیگر مگو درکش زبان</p></div></div>