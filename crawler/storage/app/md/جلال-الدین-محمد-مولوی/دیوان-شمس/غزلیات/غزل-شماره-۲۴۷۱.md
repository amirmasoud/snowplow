---
title: >-
    غزل شمارهٔ ۲۴۷۱
---
# غزل شمارهٔ ۲۴۷۱

<div class="b" id="bn1"><div class="m1"><p>سوخت یکی جهان به غم آتش غم پدید نی</p></div>
<div class="m2"><p>صورت این طلسم را هیچ کسی بدید نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کشدم به هر طرف قوت کهربای او</p></div>
<div class="m2"><p>ای عجبا بدید کس آنک مرا کشید نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست سماع چنگ نی هست شراب رنگ نی</p></div>
<div class="m2"><p>صد قدح است بر قدح آنک قدح چشید نی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق قرابه باز و من در کف او چو شیشه‌ای</p></div>
<div class="m2"><p>شیشه شکست زیر پا پای کسی خلید نی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در قدم روندگان شیخ و مرید بی‌عدد</p></div>
<div class="m2"><p>در نفس یگانگی شیخ نه و مرید نی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنک میان مردمان شهره شد و حدیث شد</p></div>
<div class="m2"><p>سایه بایزید بد مایه بایزید نی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مژده دهید عاشقان عید وصال می رسد</p></div>
<div class="m2"><p>ز آنک ندید هیچ کس خود رمضان و عید نی</p></div></div>