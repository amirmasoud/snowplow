---
title: >-
    غزل شمارهٔ ۲۷۱۲
---
# غزل شمارهٔ ۲۷۱۲

<div class="b" id="bn1"><div class="m1"><p>بیا جانا که امروز آن مایی</p></div>
<div class="m2"><p>کجایی تو کجایی تو کجایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فر سایه‌ات چون آفتابیم</p></div>
<div class="m2"><p>همایی تو همایی تو همایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان فانی نماند ز آنک او را</p></div>
<div class="m2"><p>بقایی تو بقایی تو بقایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه چنگ اندر تو زد عالم که او را</p></div>
<div class="m2"><p>نوایی تو نوایی تو نوایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو عاشق بی‌کله گردد تو او را</p></div>
<div class="m2"><p>قبایی تو قبایی تو قبایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمش کردم ولی بهر خدا را</p></div>
<div class="m2"><p>خدایی کن خدایی کن خدایی</p></div></div>