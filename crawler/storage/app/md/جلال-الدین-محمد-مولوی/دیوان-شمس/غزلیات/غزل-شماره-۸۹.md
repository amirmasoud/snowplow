---
title: >-
    غزل شمارهٔ ۸۹
---
# غزل شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>یک پند ز من بشنو خواهی نشوی رسوا</p></div>
<div class="m2"><p>من خمره افیونم زنهار سرم مگشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش به من اندرزن آتش چه زند با من</p></div>
<div class="m2"><p>کاندر فلک افکندم صد آتش و صد غوغا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چرخ همه سر شد ور خاک همه پا شد</p></div>
<div class="m2"><p>نی سر بهلم آن را نی پا بهلم این را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا صافیه الخمر فی آنیه المولی</p></div>
<div class="m2"><p>اسکر نفرا لدا و السکر بنا اولی</p></div></div>