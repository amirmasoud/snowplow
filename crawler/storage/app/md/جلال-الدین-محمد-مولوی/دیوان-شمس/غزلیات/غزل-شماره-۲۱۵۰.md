---
title: >-
    غزل شمارهٔ ۲۱۵۰
---
# غزل شمارهٔ ۲۱۵۰

<div class="b" id="bn1"><div class="m1"><p>عید نمی‌دهد فرح بی‌نظر هلال تو</p></div>
<div class="m2"><p>کوس و دهل نمی‌چخد بی‌شرف دوال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من به تو مایل و تویی هر نفسی ملولتر</p></div>
<div class="m2"><p>وه که خجل نمی‌شود میل من از ملال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناز کن ای حیات جان کبر کن و بکش عنان</p></div>
<div class="m2"><p>شمس و قمر دلیل تو شهد و شکر دلال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیت هر ملاحتی ماه تو خواند بر جهان</p></div>
<div class="m2"><p>مایه هر خجستگی ماه تو است و سال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب زلال ملک تو باغ و نهال ملک تو</p></div>
<div class="m2"><p>جز ز زلال صافیت می‌نخورد نهال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک تو است تخت‌ها باغ و سرا و رخت‌ها</p></div>
<div class="m2"><p>رقص کند درخت‌ها چونک رسد شمال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطبخ توست آسمان مطبخیانت اختران</p></div>
<div class="m2"><p>آتش و آب ملک تو خلق همه عیال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق کمینه نام تو چرخ کمینه بام تو</p></div>
<div class="m2"><p>رونق آفتاب‌ها از مه بی‌زوال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خشک لبند عالمی از لمع سراب تو</p></div>
<div class="m2"><p>لطف سراب این بود تا چه بود زلال تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ز خیال‌های تو گشته خیال عاشقان</p></div>
<div class="m2"><p>خیل خیال این بود تا چه بود جمال تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وصل کنی درخت را حالت او بدل شود</p></div>
<div class="m2"><p>چون نشود مها بدل جان و دل از وصال تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهر بود شکر شود سنگ بود گهر شود</p></div>
<div class="m2"><p>شام بود سحر شود از کرم خصال تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس سخن است در دلم بسته‌ام و نمی‌هلم</p></div>
<div class="m2"><p>گوش گشاده‌ام که تا نوش کنم مقال تو</p></div></div>