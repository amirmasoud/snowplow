---
title: >-
    غزل شمارهٔ ۳۱۰۰
---
# غزل شمارهٔ ۳۱۰۰

<div class="b" id="bn1"><div class="m1"><p>ببست خواب مرا جاودانه دلداری</p></div>
<div class="m2"><p>به زیر سنگ نهان کرد و در بن غاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خواب هم نتوان دید خواب چشم مرا</p></div>
<div class="m2"><p>چو مرده‌ای که درافتاد در نمکساری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجاست خواب و کجا چشم و کو قرار دلی</p></div>
<div class="m2"><p>کجا گذارد این فتنه صبر صباری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه کوه بود عقل همچو که بپرد</p></div>
<div class="m2"><p>ببین چه صرصر باهیبتست این باری</p></div></div>