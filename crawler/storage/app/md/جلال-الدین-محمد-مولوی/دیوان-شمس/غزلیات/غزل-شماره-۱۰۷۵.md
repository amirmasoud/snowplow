---
title: >-
    غزل شمارهٔ ۱۰۷۵
---
# غزل شمارهٔ ۱۰۷۵

<div class="b" id="bn1"><div class="m1"><p>آینه چینی تو را با زنگی اعشی چه کار</p></div>
<div class="m2"><p>کر مادرزاد را با ناله سرنا چه کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر مخنث از کجا و ناز معشوق از کجا</p></div>
<div class="m2"><p>طفلک نوزاد را با باده حمرا چه کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست زهره در حنی او کی سلحشوری کند</p></div>
<div class="m2"><p>مرغ خاکی را به موج و غره دریا چه کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر چرخی که عیسی از بلندی بو نبرد</p></div>
<div class="m2"><p>مر خرش را ای مسلمانان بر آن بالا چه کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قوم رندانیم در کنج خرابات فنا</p></div>
<div class="m2"><p>خواجه ما را با جهاز و مخزن و کالا چه کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد هزاران ساله از دیوانگی بگذشته‌ایم</p></div>
<div class="m2"><p>چون تو افلاطون عقلی رو تو را با ما چه کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چنین عقل و دل آیی سوی قطاعان راه</p></div>
<div class="m2"><p>تاجر ترسنده را اندر چنین غوغا چه کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخم شمشیرست این جا زخم زوبین هر طرف</p></div>
<div class="m2"><p>جمع خاتونان نازک ساق رعنا را چه کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رستمان امروز اندر خون خود غلطان شدند</p></div>
<div class="m2"><p>زالکان پیر را با قامت دوتا چه کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقان را منبلان دان زخم خوار و زخم دوست</p></div>
<div class="m2"><p>عاشقان عافیت را با چنین سودا چه کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشقان بوالعجب تا کشته‌تر خود زنده تر</p></div>
<div class="m2"><p>در جهان عشق باقی مرگ را حاشا چه کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وانگهی این مست عشق اندر هوای شمس دین</p></div>
<div class="m2"><p>رفته تبریز و شنیده رو تو را آن جا چه کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ورای هر دو عالم بانگ آید روح را</p></div>
<div class="m2"><p>پس تو را با شمس دین باقی اعلا چه کار</p></div></div>