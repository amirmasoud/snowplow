---
title: >-
    غزل شمارهٔ ۶۲۱
---
# غزل شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>در تابش خورشیدش رقصم به چه می‌باید</p></div>
<div class="m2"><p>تا ذره چو رقص آید از منش به یاد آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد حامله هر ذره از تابش روی او</p></div>
<div class="m2"><p>هر ذره از آن لذت صد ذره همی‌زاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هاون تن بنگر کز عشق سبک روحی</p></div>
<div class="m2"><p>تا ذره شود خود را می‌کوبد و می‌ساید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر گوهر و مرجانی جز خرد مشو این جا</p></div>
<div class="m2"><p>زیرا که در این حضرت جز ذره نمی‌شاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گوهر جان بنگر اندر صدف این تن</p></div>
<div class="m2"><p>کز دست گران جانی انگشت همی‌خاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون جان بپرد از تو این گوهر زندانی</p></div>
<div class="m2"><p>چون ذره به اصلش شد خوانیش ولی ناید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور سخت شود بندش در خون بزند نقبی</p></div>
<div class="m2"><p>عمری برود در خون موییش نیالاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز تا به چه بابل او را نبود منزل</p></div>
<div class="m2"><p>تا جان نشود جادو جایی بنیاساید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تبریز ز برج تو گر تابد شمس الدین</p></div>
<div class="m2"><p>هم ابر شود چون مه هم ماه درافزاید</p></div></div>