---
title: >-
    غزل شمارهٔ ۲۶۱۷
---
# غزل شمارهٔ ۲۶۱۷

<div class="b" id="bn1"><div class="m1"><p>عیسی چو تویی جانا ای دولت ترسایی</p></div>
<div class="m2"><p>لاهوت ازل را از ناسوت تو بنمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمان ز سر زلفت زنار عجب بندد</p></div>
<div class="m2"><p>کز کافر زلف خود یک پیچ تو بگشایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای از پس صد پرده درتافته رخسارت</p></div>
<div class="m2"><p>تا عالم خاکی را از عشق برآرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان دوش ز سرمستی با عشق تو عهدی کرد</p></div>
<div class="m2"><p>جان بود در آن بیعت با عشق به تنهایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر عشق به گوشش برد سر گفت به گوش جان</p></div>
<div class="m2"><p>کس عهد کند با خود نی تو همگی مایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندانک تو می‌کوشی جز چشم نمی‌پوشی</p></div>
<div class="m2"><p>تا چند گریزی تو از خویش و نیاسایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان گفت که ای فردم سوگند بدین دردم</p></div>
<div class="m2"><p>سوگند بدان زلفی عاشق کش سودایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کان عهد که من کردم بی‌جان و بدن کردم</p></div>
<div class="m2"><p>نی ما و نه من کردم ای مفرد یکتایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مست آنچ کند در می از می‌بود آن به روی</p></div>
<div class="m2"><p>در آب نماید او لیک او است ز بالایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تبریز ز شمس الدین آخر قدحی زو هین</p></div>
<div class="m2"><p>آن ساقی ترسا را یک نکته نفرمایی</p></div></div>