---
title: >-
    غزل شمارهٔ ۸۹۹
---
# غزل شمارهٔ ۸۹۹

<div class="b" id="bn1"><div class="m1"><p>یار مرا عارض و عذار نه این بود</p></div>
<div class="m2"><p>باغ مرا نخل و برگ و بار نه این بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهدشکن گشته‌اند خاصه و عامه</p></div>
<div class="m2"><p>قاعده اهل این دیار نه این بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روح در این غار غوره وار ترش چیست</p></div>
<div class="m2"><p>پرورش و عهد یار غار نه این بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل غم بی‌شمار بار و خرم برد</p></div>
<div class="m2"><p>طمع من از یار بردبار نه این بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جهت من چه دیگ می‌پزد آن یار</p></div>
<div class="m2"><p>راتبه میر پخته کار نه این بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دام نهان کرد و دانه ریخت به پیشم</p></div>
<div class="m2"><p>کینه نهان داشت و آشکار نه این بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناصح من کژ نهاد و برد ز راهم</p></div>
<div class="m2"><p>شرط امینی و مستشار نه این بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در چمن عیش خار از چه شکفته‌ست</p></div>
<div class="m2"><p>منبت آن شهره نوبهار نه این بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شحنه شد آن دزد من ببست دو دستم</p></div>
<div class="m2"><p>سایسی و عدل شهریار نه این بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهل ندادی که عذر خویش بگویم</p></div>
<div class="m2"><p>خوی چو تو کوه باوقار نه این بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌رسدم بوی خون ز گفت درشتش</p></div>
<div class="m2"><p>رایحه ناف مشکبار نه این بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوش تو را ذوق و طعم و لطف نه این بود</p></div>
<div class="m2"><p>وان شتر مست خوش عیار نه این بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش شه افغان کنم ز خدعه قلاب</p></div>
<div class="m2"><p>زر من آن نقد خوش عیار نه این بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه چو دریا خزینه‌اش همه گوهر</p></div>
<div class="m2"><p>لیک شهم را خزینه دار نه این بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس که گله‌ست این نثار و جمله شکایت</p></div>
<div class="m2"><p>شاه شکور مرا نثار نه این بود</p></div></div>