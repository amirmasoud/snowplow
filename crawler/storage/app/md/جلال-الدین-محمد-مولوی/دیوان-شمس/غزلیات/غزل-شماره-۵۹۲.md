---
title: >-
    غزل شمارهٔ ۵۹۲
---
# غزل شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>اگر چرخ وجود من از این گردش فروماند</p></div>
<div class="m2"><p>بگرداند مرا آن کس که گردون را بگرداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر این لشکر ما را ز چشم بد شکست افتد</p></div>
<div class="m2"><p>به امر شاه لشکرها از آن بالا فروآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر باد زمستانی کند باغ مرا ویران</p></div>
<div class="m2"><p>بهار شهریار من ز دی انصاف بستاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمار برگ اگر باشد یکی فرعون جباری</p></div>
<div class="m2"><p>کف موسی یکایک را به جای خویش بنشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مترسان دل مترسان دل ز سختی‌های این منزل</p></div>
<div class="m2"><p>که آب چشمه حیوان بتا هرگز نمیراند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رایناکم رایناکم و اخرجنا خفایاکم</p></div>
<div class="m2"><p>فان لم تنتهوا عنها فایانا و ایاکم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و ان طفتم حوالینا و انتم نور عینانا</p></div>
<div class="m2"><p>فلا تستیاسوا منان فان العیش احیاکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکسته بسته تازی‌ها برای عشقبازی‌ها</p></div>
<div class="m2"><p>بگویم هر چه من گویم شهی دارم که بستاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو من خود را نمی‌یابم سخن را از کجا یابم</p></div>
<div class="m2"><p>همان شمعی که داد این را همو شمعم بگیراند</p></div></div>