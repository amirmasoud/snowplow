---
title: >-
    غزل شمارهٔ ۲۹۶۴
---
# غزل شمارهٔ ۲۹۶۴

<div class="b" id="bn1"><div class="m1"><p>دی دامنش گرفتم کای گوهر عطایی</p></div>
<div class="m2"><p>شب خوش مگو مرنجان کامشب از آن مایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افروخت روی دلکش شد سرخ همچو اخگر</p></div>
<div class="m2"><p>گفتا بس است درکش تا چند از این گدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم رسول حق گفت حاجت ز روی نیکو</p></div>
<div class="m2"><p>درخواه اگر بخواهی تا تو مظفر آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتا که روی نیکو خودکامه است و بدخو</p></div>
<div class="m2"><p>زیرا که ناز و جورش دارد بسی روایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم اگر چنان است جورش حیات جان است</p></div>
<div class="m2"><p>زیرا طلسم کان است هر گه بیازمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت این حدیث خام است روی نکو کدام است</p></div>
<div class="m2"><p>این رنگ و نقش دام است مکر است و بی‌وفایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون جان جان ندارد می‌دانک آن ندارد</p></div>
<div class="m2"><p>بس کس که جان سپارد در صورت فنایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که خوش عذارا تو هست کن فنا را</p></div>
<div class="m2"><p>زر ساز مس ما را تو جان کیمیایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تسلیم مس بباید تا کیمیا بیابد</p></div>
<div class="m2"><p>تو گندمی ولیکن بیرون آسیایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتا تو ناسپاسی تو مس ناشناسی</p></div>
<div class="m2"><p>در شک و در قیاسی زین‌ها که می‌نمایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گریان شدم به زاری گفتم که حکم داری</p></div>
<div class="m2"><p>فریاد رس به یاری ای اصل روشنایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون دید اشک بنده آغاز کرد خنده</p></div>
<div class="m2"><p>شد شرق و غرب زنده زان لطف آشنایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای همرهان و یاران گریید همچو باران</p></div>
<div class="m2"><p>تا در چمن نگاران آرند خوش لقایی</p></div></div>