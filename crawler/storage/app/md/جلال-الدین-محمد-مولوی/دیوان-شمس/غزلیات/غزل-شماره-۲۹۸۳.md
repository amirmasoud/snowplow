---
title: >-
    غزل شمارهٔ ۲۹۸۳
---
# غزل شمارهٔ ۲۹۸۳

<div class="b" id="bn1"><div class="m1"><p>ای ساقیی که آن می احمر گرفته‌ای</p></div>
<div class="m2"><p>وی مطربی که آن غزل تر گرفته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای زهره‌ای که آتش در آسمان زدی</p></div>
<div class="m2"><p>مریخ را بگو که چه خنجر گرفته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جان و از جهان دل عاشق ربوده‌ای</p></div>
<div class="m2"><p>الحق شکار نازک و لاغر گرفته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای هجر تو ز روز قیامت درازتر</p></div>
<div class="m2"><p>این چه قیامتی است که از سر گرفته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آسمان چو دور ندیمانش دیده‌ای</p></div>
<div class="m2"><p>در دور خویش شکل مدور گرفته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیلان شیردل چو کفت را مسخرند</p></div>
<div class="m2"><p>این چند پشه را چه مسخر گرفته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هان ای فقیر روز فقیری گله مکن</p></div>
<div class="m2"><p>زیرا که صد چو ملکت سنجر گرفته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای روی خویش دیده تو در روی خوب یار</p></div>
<div class="m2"><p>آیینه‌ای عظیم منور گرفته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل طپان چرایی چون برگ هر دمی</p></div>
<div class="m2"><p>چون دامن بهار معنبر گرفته‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای چشم گریه چیست به هر ساعتی تو را</p></div>
<div class="m2"><p>چون کحل از مسیح پیمبر گرفته‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هجده هزار عالم اگر ملک تو شود</p></div>
<div class="m2"><p>بی روی دوست چیز محقر گرفته‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داری تکی که بگذری از خنگ آسمان</p></div>
<div class="m2"><p>کاهل چرا شدی صفت خر گرفته‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خامش کن و زبان دگر گو و رسم نو</p></div>
<div class="m2"><p>این رسم کهنه را چه مکرر گرفته‌ای</p></div></div>