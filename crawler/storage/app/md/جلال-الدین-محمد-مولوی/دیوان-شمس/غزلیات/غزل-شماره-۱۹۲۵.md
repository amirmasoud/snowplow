---
title: >-
    غزل شمارهٔ ۱۹۲۵
---
# غزل شمارهٔ ۱۹۲۵

<div class="b" id="bn1"><div class="m1"><p>بازآمد آستین فشانان</p></div>
<div class="m2"><p>آن دشمن جان و عقل و ایمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غارتگر صد هزار خانه</p></div>
<div class="m2"><p>ویران کن صد هزار دکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شورنده صد هزار فتنه</p></div>
<div class="m2"><p>حیرتگه صد هزار حیران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دایه عقل و آفت عقل</p></div>
<div class="m2"><p>آن مونس جان و دشمن جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او عقل سبک کجا رباید</p></div>
<div class="m2"><p>عقلی خواهد چو عقل لقمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او جان خسیس کی پذیرد</p></div>
<div class="m2"><p>جانی خواهد چو بحر عمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد که خراج ده بیاور</p></div>
<div class="m2"><p>گفتم که چه ده دهی است ویران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طوفان تو شهرها شکست است</p></div>
<div class="m2"><p>یک ده چه زند میان طوفان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتا ویران مقام گنج است</p></div>
<div class="m2"><p>ویرانه ماست ای مسلمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ویرانه به ما ده و برون رو</p></div>
<div class="m2"><p>تشنیع مزن مگو پریشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ویرانه ز توست چون تو رفتی</p></div>
<div class="m2"><p>معمور شود به عدل سلطان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیلت مکن و مگو که رفتم</p></div>
<div class="m2"><p>اندر پس در مباش پنهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون مرده بساز خویشتن را</p></div>
<div class="m2"><p>تا زنده شوی به روح انسان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتی که تو در میان نباشی</p></div>
<div class="m2"><p>آن گفت تو هست عین قرآن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کاری که کنی تو در میان نی</p></div>
<div class="m2"><p>آن کرده حق بود یقین دان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باقی غزل به سر بگوییم</p></div>
<div class="m2"><p>نتوان گفتن به پیش خامان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاموش که صد هزار فرق است</p></div>
<div class="m2"><p>از گفت زبان و نور فرقان</p></div></div>