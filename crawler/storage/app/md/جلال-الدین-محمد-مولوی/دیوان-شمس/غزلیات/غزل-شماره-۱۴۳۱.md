---
title: >-
    غزل شمارهٔ ۱۴۳۱
---
# غزل شمارهٔ ۱۴۳۱

<div class="b" id="bn1"><div class="m1"><p>مرا چون کم فرستی غم حزین و تنگ دل باشم</p></div>
<div class="m2"><p>چو غم بر من فروریزی ز لطف غم خجل باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمان تو مرا نگذاشت تا غمگین شوم یک دم</p></div>
<div class="m2"><p>هوای تو مرا نگذاشت تا من آب و گل باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه اجزای عالم را غم تو زنده می دارد</p></div>
<div class="m2"><p>منم کز تو غمی خواهم که در وی مستقل باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب دردی برانگیزی که دردم را دوا گردد</p></div>
<div class="m2"><p>عجب گردی برانگیزی که از وی مکتحل باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فدایی را کفیلی کو که ارزد جان فدا کردن</p></div>
<div class="m2"><p>کسایی را کسایی کو که آن را مشتمل باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا رنج تو نگذارد که رنجوری به من آید</p></div>
<div class="m2"><p>مرا گنج تو نگذارد که درویش و مقل باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صباح تو مرا نگذاشت تا شمعی برافروزم</p></div>
<div class="m2"><p>عیان تو مرا نگذاشت تا من مستدل باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیالی کان به پیش آید خیالت را بپوشاند</p></div>
<div class="m2"><p>اگر خونش بریزم من ز خون او بحل باشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوزانم ز عشق تو خیال هر دو عالم را</p></div>
<div class="m2"><p>بسوزند این دو پروانه چو من شمع چگل باشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خمش کن نقل کمتر کن ز حال خود به قال خود</p></div>
<div class="m2"><p>چنان نقلی که من دارم چرا من منتقل باشم</p></div></div>