---
title: >-
    غزل شمارهٔ ۲۱۵۱
---
# غزل شمارهٔ ۲۱۵۱

<div class="b" id="bn1"><div class="m1"><p>در سفر هوای تو بی‌خبرم به جان تو</p></div>
<div class="m2"><p>نیک مبارک آمده‌ست این سفرم به جان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل قبا سمر شدی چونک در آن کمر شدی</p></div>
<div class="m2"><p>کشته زار در میان زان کمرم به جان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو قمر برآمدی بر قمران سر آمدی</p></div>
<div class="m2"><p>همچو هلال زار من زان قمرم به جان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشک و ترم خیال تو آینه جمال تو</p></div>
<div class="m2"><p>خشک لبم ز سوز دل چشم ترم به جان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو ز لعل بسته‌ات تنگ شکر گشاده‌ای</p></div>
<div class="m2"><p>چون مگس شکسته پر بر شکرم به جان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دام همیشه تا بود آفت بال و پر بود</p></div>
<div class="m2"><p>رسته شود ز دام تو بال و پرم به جان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تبریز شمس دین هست چراغ هر سحر</p></div>
<div class="m2"><p>طالب آفتاب من چون سحرم به جان تو</p></div></div>