---
title: >-
    غزل شمارهٔ ۲۸۵۰
---
# غزل شمارهٔ ۲۸۵۰

<div class="b" id="bn1"><div class="m1"><p>ز غم تو زار زارم هله تا تو شاد باشی</p></div>
<div class="m2"><p>صنما در انتظارم هله تا تو شاد باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو مرا چو خسته بینی نظر خجسته بینی</p></div>
<div class="m2"><p>دل و جان به غم سپارم هله تا تو شاد باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غم دلم چه شادی به جفا چه اوستادی</p></div>
<div class="m2"><p>دم شاد برنیارم هله تا تو شاد باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صنما چو تیغ دشنه تو به خون بنده تشنه</p></div>
<div class="m2"><p>ز دو دیده خون ببارم هله تا تو شاد باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو مرا چو شاد بینی سر و سینه پر ز کینی</p></div>
<div class="m2"><p>سر خویش را نخارم هله تا تو شاد باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تو بخت و جاه دارم دل تو نگاه دارم</p></div>
<div class="m2"><p>صنما بر این قرارم هله تا تو شاد باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تویی جان این زمانه تو نشسته پربهانه</p></div>
<div class="m2"><p>ز زمانه برکنارم هله تا تو شاد باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن و نفس تا نمیرد دل و جان صفا نگیرد</p></div>
<div class="m2"><p>همه این شده‌ست کارم هله تا تو شاد باشی</p></div></div>