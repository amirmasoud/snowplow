---
title: >-
    غزل شمارهٔ ۵۳۱
---
# غزل شمارهٔ ۵۳۱

<div class="b" id="bn1"><div class="m1"><p>صوفی چرا هُشیار شد ساقی چرا بی‌کار شد</p></div>
<div class="m2"><p>مستی اگر در خواب شد مستی دگر بیدار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید اگر در گور شد عالم ز تو پرنور شد</p></div>
<div class="m2"><p>چشم خوشت مخمور شد چشم دگر خمار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر عیش اول پیر شد صد عیش نو توفیر شد</p></div>
<div class="m2"><p>چون زلف تو زنجیر شد دیوانگی ناچار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مطرب شیرین نفس عشرت نگر از پیش و پس</p></div>
<div class="m2"><p>کس نشنود افسون کس چون واقف اسرار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما موسییم و تو مها گاهی عصا گه اژدها</p></div>
<div class="m2"><p>ای شاهدان ارزان بها چون غارت بلغار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لعلت شکرها کوفته چشمت ز رشک آموخته</p></div>
<div class="m2"><p>جان خانه دل روفته هین نوبت دیدار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر بار عذری می‌نهی وز دست مستی می‌جهی</p></div>
<div class="m2"><p>ای جان چه دفعم می‌دهی این دفع تو بسیار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای کرده دل چون خاره‌ای امشب نداری چاره‌ای</p></div>
<div class="m2"><p>تو ماه و ما استاره‌ای استاره با مه یار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ماه بیرون از افق ای ما تو را امشب قنق</p></div>
<div class="m2"><p>چون شب جهان را شد تتق پنهان روان را کار شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر زحمت از تو برده‌ام پنداشتی من مرده‌ام</p></div>
<div class="m2"><p>تو صافی و من درده‌ام بی‌صاف دردی خوار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از وصل همچون روز تو در هجر عالم سوز تو</p></div>
<div class="m2"><p>در عشق مکرآموز تو بس ساده دل عیار شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نی تب بدم نی درد سر سر می‌زدم دیوار بر</p></div>
<div class="m2"><p>کز طمع آن خوش گلشکر قاصد دلم بیمار شد</p></div></div>