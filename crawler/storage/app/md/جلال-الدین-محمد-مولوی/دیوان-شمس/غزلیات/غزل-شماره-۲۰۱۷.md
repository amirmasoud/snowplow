---
title: >-
    غزل شمارهٔ ۲۰۱۷
---
# غزل شمارهٔ ۲۰۱۷

<div class="b" id="bn1"><div class="m1"><p>آمد آمد در میان خوب ختن</p></div>
<div class="m2"><p>هر دو دستت را بشو از جان و تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد شمشیری به دست عشق و گفت</p></div>
<div class="m2"><p>هرچ بینی غیر من گردن بزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر آب انداز الا نوح را</p></div>
<div class="m2"><p>هر که باشد خوب و زشت و مرد و زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که او اندر دل نوح است رست</p></div>
<div class="m2"><p>هر که در پستی است در دریا فکن</p></div></div>