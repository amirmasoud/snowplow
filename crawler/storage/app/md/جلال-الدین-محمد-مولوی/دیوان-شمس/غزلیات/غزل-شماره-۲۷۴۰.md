---
title: >-
    غزل شمارهٔ ۲۷۴۰
---
# غزل شمارهٔ ۲۷۴۰

<div class="b" id="bn1"><div class="m1"><p>بازم صنما چه می‌فریبی</p></div>
<div class="m2"><p>بازم به دغا چه می‌فریبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه بخوانیم که ای دوست</p></div>
<div class="m2"><p>ای دوست مرا چه می‌فریبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری تو و عمر را وفا نیست</p></div>
<div class="m2"><p>بازم به وفا چه می‌فریبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل سیر نمی‌شود به جیحون</p></div>
<div class="m2"><p>او را به سقا چه می‌فریبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاریک شده‌ست چشم بی‌تو</p></div>
<div class="m2"><p>ما را به عصا چه می‌فریبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دوست دعا وظیفه ماست</p></div>
<div class="m2"><p>ما را به دعا چه می‌فریبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن را که مثال امن دادی</p></div>
<div class="m2"><p>با خوف و رجا چه می‌فریبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی به قضای حق رضا ده</p></div>
<div class="m2"><p>ما را به قضا چه می‌فریبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نیست دواپذیر این درد</p></div>
<div class="m2"><p>ما را به دوا چه می‌فریبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنها خوردن چو پیشه کردی</p></div>
<div class="m2"><p>ما را به صلا چه می‌فریبی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون چنگ نشاط ما شکستی</p></div>
<div class="m2"><p>ما را به سه تا چه می‌فریبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما را بی‌ما چو می‌نوازی</p></div>
<div class="m2"><p>ما را با ما چه می‌فریبی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بسته کمر به پیش تو جان</p></div>
<div class="m2"><p>ما را به قبا چه می‌فریبی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاموش که غیر تو نخواهیم</p></div>
<div class="m2"><p>ما را به عطا چه می‌فریبی</p></div></div>