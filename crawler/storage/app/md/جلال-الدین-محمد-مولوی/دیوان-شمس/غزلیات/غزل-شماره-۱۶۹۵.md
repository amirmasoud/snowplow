---
title: >-
    غزل شمارهٔ ۱۶۹۵
---
# غزل شمارهٔ ۱۶۹۵

<div class="b" id="bn1"><div class="m1"><p>پیش چنین جمال جان بخش چون نمیرم</p></div>
<div class="m2"><p>دیوانه چون نگردم زنجیر چون نگیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون باده تو خوردم من محو چون نگردم</p></div>
<div class="m2"><p>تو چون میی من آبم تو شهد و من چو شیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگشا دهان خود را آن قند بی‌عدد را</p></div>
<div class="m2"><p>عذر ار نمی‌پذیری من عشوه می پذیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی که از چه خندم از همت بلندم</p></div>
<div class="m2"><p>زیرا به شهر عشقت بر عاشقان امیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عشق لایزالی از یک شکم بزادم</p></div>
<div class="m2"><p>نوعشق می نمایم والله که سخت پیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چشم اگر گشایی جز خویش را نشایی</p></div>
<div class="m2"><p>ور این نظر گشایی دانی که بی‌نظیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر تنور سردان آتش زنم چو مردان</p></div>
<div class="m2"><p>و اندر تنور گرمان من پخته‌تر خمیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در لطف همچو شیرم اندر گلو نگیرم</p></div>
<div class="m2"><p>تا در غلط نیفتی گر شور چون پنیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عشق شمس تبریز سلطان تاجدارم</p></div>
<div class="m2"><p>چون او به تخت آید من پیش او وزیرم</p></div></div>