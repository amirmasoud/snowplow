---
title: >-
    غزل شمارهٔ ۷۴۸
---
# غزل شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>فخر جمله ساقیانی ساغرت در کار باد</p></div>
<div class="m2"><p>چشم تو مخمور باد و جان ما خمار باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ز نوشانوش بزمت هوش‌ها بی‌هوش باد</p></div>
<div class="m2"><p>وی ز جوشاجوش عشقت عقل بی‌دستار باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون زنان مصر جان را دست و دل مجروح باد</p></div>
<div class="m2"><p>یوسف مصری همیشه شورش بازار باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا از دست تو بس دست‌ها از دست شد</p></div>
<div class="m2"><p>مست تو از دست تو پیوسته برخوردار باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مغز ما پرباد باد و مشک ما پرآب باد</p></div>
<div class="m2"><p>باد ما را و آب ما را عشق پذرفتار باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه خوبان میر ما و عشق گیراگیر ما</p></div>
<div class="m2"><p>جان دولت یار ما و بخت و دولت یار باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرکشیم و سرخوشیم و یک دگر را می‌کشیم</p></div>
<div class="m2"><p>این وجود ما همیشه جاذب اسرار باد</p></div></div>