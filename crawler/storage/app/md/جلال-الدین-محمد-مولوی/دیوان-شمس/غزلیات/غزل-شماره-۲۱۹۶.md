---
title: >-
    غزل شمارهٔ ۲۱۹۶
---
# غزل شمارهٔ ۲۱۹۶

<div class="b" id="bn1"><div class="m1"><p>از حلاوت‌ها که هست از خشم و از دشنام او</p></div>
<div class="m2"><p>می‌ستیزم هر شبی با چشم خون آشام او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دام‌های عشق او گر پر و بالم بسکلد</p></div>
<div class="m2"><p>طوطی جان نسکلد از شکر و بادام او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند پرسی مر مرا از وحشت و شب‌های هجر</p></div>
<div class="m2"><p>شب کجا ماند بگو در دولت ایام او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون ما را رنگ خون و فعل می‌آمد از آنک</p></div>
<div class="m2"><p>خون‌ها می می‌شود چون می‌رود در جام او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وعده‌های خام او در مغز جان جوشان شده</p></div>
<div class="m2"><p>عاشقان پخته بین از وعده‌های خام او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسروان بر تخت دولت بین که حسرت می‌خورند</p></div>
<div class="m2"><p>در لقای عاشقان کشته بدنام او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن سگان کوی او شاهان شیران گشته‌اند</p></div>
<div class="m2"><p>کان چنان آهوی فتنه دیده شد بر بام او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الله الله تو مپرس از باخودان اوصاف می</p></div>
<div class="m2"><p>تو ببین در چشم مستان لطف‌های عام او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست بر رگ‌های مستان نه دلا تا پی بری</p></div>
<div class="m2"><p>از دهان آلودگان زان باده خودکام او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمس تبریزی که گامش بر سر ارواح بود</p></div>
<div class="m2"><p>پا منه تو سر بنه بر جایگاه گام او</p></div></div>