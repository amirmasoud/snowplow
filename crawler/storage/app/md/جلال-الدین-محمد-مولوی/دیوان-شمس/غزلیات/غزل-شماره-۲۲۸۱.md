---
title: >-
    غزل شمارهٔ ۲۲۸۱
---
# غزل شمارهٔ ۲۲۸۱

<div class="b" id="bn1"><div class="m1"><p>ای از تو خاکی تن شده تن فکرت و گفتن شده</p></div>
<div class="m2"><p>وز گفت و فکرت بس صور در غیب آبستن شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر صورتی پرورده‌ای معنی است لیک افسرده‌ای</p></div>
<div class="m2"><p>صورت چو معنی شد کنون آغاز را روشن شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یخ را اگر بیند کسی و آن کس نداند اصل یخ</p></div>
<div class="m2"><p>چون دید کآخر آب شد در اصل یخ بی‌ظن شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندیشه جز زیبا مکن کو تار و پود صورت است</p></div>
<div class="m2"><p>ز اندیشه‌ای احسن تند هر صورتی احسن شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان سوی کاندازی نظر آن جنس می‌آید صور</p></div>
<div class="m2"><p>پس از نظر آید صور اشکال مرد و زن شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آن نشین کو روشن است کز دل سوی دل روزن است</p></div>
<div class="m2"><p>خاک از چه ورد و سوسن است کش آب هم مسکن شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور همنشین حق شوی جان خوش مطلق شوی</p></div>
<div class="m2"><p>یا رب چه بارونق شوی ای جان جان من شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جا به بی‌جا آمده اه رفته هیهای آمده</p></div>
<div class="m2"><p>بی‌دست و بی‌پای آمده چون ماه خوش خرمن شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا رب که چون می‌بینمش ای بنده جان و دینمش</p></div>
<div class="m2"><p>خود چیست این تمکینمش ای عقل از این امکن شده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر ذره‌ای را محرم او هر خوش دمی را همدم او</p></div>
<div class="m2"><p>نادیده زو زاهد شده زو دیده تردامن شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای عشق حق سودای او آن او است او جویای او</p></div>
<div class="m2"><p>وی می‌دمد در وای او ای طالب معدن شده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم طالب و مطلوب او هم عاشق و معشوق او</p></div>
<div class="m2"><p>هم یوسف و یعقوب او هم طوق و هم گردن شده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اوصافت ای کس کم چو تو پایان ندارد همچو تو</p></div>
<div class="m2"><p>چند آب و روغن می‌کنم ای آب من روغن شده</p></div></div>