---
title: >-
    غزل شمارهٔ ۲۱۶۹
---
# غزل شمارهٔ ۲۱۶۹

<div class="b" id="bn1"><div class="m1"><p>ز مکر حق مباش ایمن اگر صد بخت بینی تو</p></div>
<div class="m2"><p>بمال این چشم‌ها را گر به پندار یقینی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مکر حق چنان تند است کز وی دیده جانت</p></div>
<div class="m2"><p>تو را عرشی نماید او و گر باشی زمینی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان خاینی می بر تو بر جان امین شکلت</p></div>
<div class="m2"><p>که گر تو ساده دل باشی ندارد سود امینی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خریدی هندوی زشتی قبیحی را تو در چادر</p></div>
<div class="m2"><p>تو ساده پوستین بر بوی زهره روی چینی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شب در خانه آوردی بدیدی روش بی‌چادر</p></div>
<div class="m2"><p>ز رویش دیده بگرفتی ز بویش بستی بینی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این بازار طراران زاهدشکل بسیارند</p></div>
<div class="m2"><p>فریبندت اگر چه اهل و باعقل متینی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر فضل خداوند خداوندان شمس الدین</p></div>
<div class="m2"><p>کند تنبیه جانت را کند هر دم معینی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین آن آفتابی را کش اول نیست و نی پایان</p></div>
<div class="m2"><p>که اندر دین همی‌تابد اگر از اهل دینی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سوی باغ وحدت رو کز او شادی همی‌روید</p></div>
<div class="m2"><p>که هر جزوت شود خندان اگر در خود حزینی تو</p></div></div>