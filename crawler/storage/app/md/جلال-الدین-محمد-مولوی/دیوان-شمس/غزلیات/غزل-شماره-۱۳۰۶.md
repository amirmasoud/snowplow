---
title: >-
    غزل شمارهٔ ۱۳۰۶
---
# غزل شمارهٔ ۱۳۰۶

<div class="b" id="bn1"><div class="m1"><p>بیا بیا که تویی شیر شیر شیر مصاف</p></div>
<div class="m2"><p>ز مرغزار برون آ و صف‌ها بشکاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مدحت آنچ بگویند نیست هیچ دروغ</p></div>
<div class="m2"><p>ز هر چه از تو بلافند صادقست نه لاف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب که کرت دیگر ببیند این چشمم</p></div>
<div class="m2"><p>به سلطنت تو نشسته ملوک بر اطراف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بر مقامه خویشی وز آنچ گفتم بیش</p></div>
<div class="m2"><p>ولیک دیده ز هجرت نه روشنست نه صاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعاع چهره او خود نهان نمی‌گردد</p></div>
<div class="m2"><p>برو تو غیرت بافنده پرده‌ها می‌باف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو دلفریب صفت‌های دلفریب آری</p></div>
<div class="m2"><p>ولیک آتش من کی رها کند اوصاف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو عاشقان به جهان جان‌ها فدا کردند</p></div>
<div class="m2"><p>فدا بکردم جانی و جان جان به مصاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه کعبه اقبال جان من باشد</p></div>
<div class="m2"><p>هزار کعبه جان را بگرد تست طواف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهان ببسته‌ام از راز چون جنین غمم</p></div>
<div class="m2"><p>که کودکان به شکم در غذا خورند از ناف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو عقل عقلی و من مست پرخطای توام</p></div>
<div class="m2"><p>خطای مست بود پیش عقل عقل معاف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمار بی‌حد من بحرهای می‌خواهد</p></div>
<div class="m2"><p>که نیست مست تو را رطل‌ها و جره کفاف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بجز به عشق تو جایی دگر نمی‌گنجم</p></div>
<div class="m2"><p>که نیست موضع سیمرغ عشق جز که قاف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه عاشق دم خویشم ولیک بوی تست</p></div>
<div class="m2"><p>چو دم زنم ز غمت از مت و از آلاف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه الف گیرد اجزای من به غیر تو دوست</p></div>
<div class="m2"><p>اگر هزار بخوانند سوره ایلاف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به نور دیده سلف بسته‌ام به عشق رخت</p></div>
<div class="m2"><p>که گوش من نگشاید به قصه اسلاف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منم کمانچه نداف شمس تبریزی</p></div>
<div class="m2"><p>فتاده آتش او در دکان این نداف</p></div></div>