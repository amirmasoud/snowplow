---
title: >-
    غزل شمارهٔ ۲۸۳۱
---
# غزل شمارهٔ ۲۸۳۱

<div class="b" id="bn1"><div class="m1"><p>چو نماز شام هر کس بنهد چراغ و خوانی</p></div>
<div class="m2"><p>منم و خیال یاری غم و نوحه و فغانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو وضو ز اشک سازم بود آتشین نمازم</p></div>
<div class="m2"><p>در مسجدم بسوزد چو بدو رسد اذانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ قبله‌ام کجا شد که نماز من قضا شد</p></div>
<div class="m2"><p>ز قضا رسد هماره به من و تو امتحانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجبا نماز مستان تو بگو درست هست آن</p></div>
<div class="m2"><p>که نداند او زمانی نشناسد او مکانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجبا دو رکعت است این عجبا که هشتمین است</p></div>
<div class="m2"><p>عجبا چه سوره خواندم چو نداشتم زبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حق چگونه کوبم که نه دست ماند و نه دل</p></div>
<div class="m2"><p>دل و دست چون تو بردی بده ای خدا امانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خدا خبر ندارم چو نماز می‌گزارم</p></div>
<div class="m2"><p>که تمام شد رکوعی که امام شد فلانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس از این چو سایه باشم پس و پیش هر امامی</p></div>
<div class="m2"><p>که بکاهم و فزایم ز حراک سایه بانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به رکوع سایه منگر به قیام سایه منگر</p></div>
<div class="m2"><p>مطلب ز سایه قصدی مطلب ز سایه جانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز حساب رست سایه که به جان غیر جنبد</p></div>
<div class="m2"><p>که همی‌زند دو دستک که کجاست سایه دانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شه است سایه بانم چو روان شود روانم</p></div>
<div class="m2"><p>چو نشیند او نشستم به کرانه دکانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو مرا نماند مایه منم و حدیث سایه</p></div>
<div class="m2"><p>چه کند دهان سایه تبعیت دهانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نکنی خمش برادر چو پری ز آب و آذر</p></div>
<div class="m2"><p>ز سبو همان تلابد که در او کنند یا نی</p></div></div>