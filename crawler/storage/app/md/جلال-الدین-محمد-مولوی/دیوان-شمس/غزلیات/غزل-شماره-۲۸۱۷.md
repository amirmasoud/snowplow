---
title: >-
    غزل شمارهٔ ۲۸۱۷
---
# غزل شمارهٔ ۲۸۱۷

<div class="b" id="bn1"><div class="m1"><p>مکن ای دوست نشاید که بخوانند و نیایی</p></div>
<div class="m2"><p>و اگر نیز بیایی بروی زود نپایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هله ای دیده و نورم گه آن شد که بشورم</p></div>
<div class="m2"><p>پی موسی تو طورم شدی از طور کجایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرم خصم بخندد و گرم شحنه ببندد</p></div>
<div class="m2"><p>تو اگر نیز به قاصد به غضب دست بخایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تو سوگند بخوردم که از این شیوه نگردم</p></div>
<div class="m2"><p>بکنم شور و بگردم به خدا و به خدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکن ای دوست چراغی که به از اختر و چرخی</p></div>
<div class="m2"><p>بکن ای دوست طبیبی که به هر درد دوایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ویران من اندر غلط ار جغد درآید</p></div>
<div class="m2"><p>بزند عکس تو بر وی کند آن جغد همایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هله یک قوم بگریند و یکی قوم بخندند</p></div>
<div class="m2"><p>ره عشق تو ببندند به استیزه نمایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر از خشم بجنگی وگر از خصم بلنگی</p></div>
<div class="m2"><p>و اگر شیر و پلنگی تو هم از حلقه مایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بد و نیک زمانه نجهد عشق ز خانه</p></div>
<div class="m2"><p>نبود عشق فسانه که سمایی است سمایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو مرا درد دوا شد چو مرا جور وفا شد</p></div>
<div class="m2"><p>چو مرا ارض سما شد چه کنم طال بقایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سحرالعین چه باشد که جهان خشک نماید</p></div>
<div class="m2"><p>بر عام و بر عارف چو گلستان رضایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هله این ناز رها کن نفسی روی به ما کن</p></div>
<div class="m2"><p>نفسی ترک دغا کن چه بود مکر و دغایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هله خاموش که تا او لب شیرین بگشاید</p></div>
<div class="m2"><p>بکند هر دو جهان را خضر وقت سقایی</p></div></div>