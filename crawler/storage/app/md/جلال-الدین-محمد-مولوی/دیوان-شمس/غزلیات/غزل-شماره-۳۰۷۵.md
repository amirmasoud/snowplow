---
title: >-
    غزل شمارهٔ ۳۰۷۵
---
# غزل شمارهٔ ۳۰۷۵

<div class="b" id="bn1"><div class="m1"><p>بیا بیا که تو از نادرات ایامی</p></div>
<div class="m2"><p>برادری پدری مادری دلارامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نام خوب تو مرده ز گور برخیزد</p></div>
<div class="m2"><p>گزاف نیست برادر چنین نکونامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو فضل و رحمت حقی که هر که در تو گریخت</p></div>
<div class="m2"><p>قبول می کنیش با کژی و با خامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی‌زیم به ستیزه و این هم از گولیست</p></div>
<div class="m2"><p>که تا مرا نکشی ای هوس نیارامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هیچ نقش نگنجی ولیک تقدیرا</p></div>
<div class="m2"><p>اگر به نقش درآیی عجب گل اندامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی فراق نمایی و چاره آموزی</p></div>
<div class="m2"><p>گهی رسول فرستی و جان پیغامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درون روزن دل چون فتاد شعله شمع</p></div>
<div class="m2"><p>بداند این دل شب رو که بر سر بامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرادم آنک شود سایه و آفتاب یکی</p></div>
<div class="m2"><p>که تا ز عشق نمایم تمام خوش کامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محال جوی و محالم بدین گناه مرا</p></div>
<div class="m2"><p>قبول می‌نکند هیچ عالم و عامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو هم محال ننوشی و معتقد نشوی</p></div>
<div class="m2"><p>برو برو که مرید عقول و احلامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر ز خسرو جان‌ها حلاوتی یابی</p></div>
<div class="m2"><p>محال هر دو جهان را چو من درآشامی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور از طبیب طبیبان گوارشی یابی</p></div>
<div class="m2"><p>مکاشفی تو بخوان خدا نه اوهامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برآ ز مشرق تبریز شمس دین بخرام</p></div>
<div class="m2"><p>که بر ممالک هر دو جهان چو بهرامی</p></div></div>