---
title: >-
    غزل شمارهٔ ۱۳۰۴
---
# غزل شمارهٔ ۱۳۰۴

<div class="b" id="bn1"><div class="m1"><p>باده نمی‌بایدم فارغم از درد و صاف</p></div>
<div class="m2"><p>تشنه خون خودم آمد وقت مصاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برکش شمشیر تیز خون حسودان بریز</p></div>
<div class="m2"><p>تا سر بی‌تن کند گرد تن خود طواف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه کن از کله‌ها بحر کن از خون ما</p></div>
<div class="m2"><p>تا بخورد خاک و ریگ جرعه خون از گزاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ز دل من خبیر رو دهنم را مگیر</p></div>
<div class="m2"><p>ور نه شکافد دلم خون بجهد از شکاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش به غوغا مکن هیچ محابا مکن</p></div>
<div class="m2"><p>سلطنت و قهرمان نیست چنین دست باف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل آتش روم لقمه آتش شوم</p></div>
<div class="m2"><p>جان چو کبریت را بر چه بریدند ناف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش فرزند ماست تشنه و دربند ماست</p></div>
<div class="m2"><p>هر دو یکی می‌شویم تا نبود اختلاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چک چک و دودش چراست زانک دورنگی به جاست</p></div>
<div class="m2"><p>چونک شود هیزم او چک چک نبود ز لاف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور بجهد نیم سوز فحم بود او هنوز</p></div>
<div class="m2"><p>تشنه دل و رو سیه طالب وصل و زفاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش گوید برو تو سیهی من سپید</p></div>
<div class="m2"><p>هیزم گوید که تو سوخته‌ای من معاف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این طرفش روی نی وان طرفش روی نی</p></div>
<div class="m2"><p>کرده میان دو یار در سیهی اعتکاف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو مسلمان غریب نی سوی خلقش رهی</p></div>
<div class="m2"><p>نی سوی شاهنشهی بر طرفی چون سجاف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلک چو عنقا که او از همه مرغان فزود</p></div>
<div class="m2"><p>بر فلکش ره نبود ماند بر آن کوه قاف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با تو چه گویم که تو در غم نان مانده‌ای</p></div>
<div class="m2"><p>پشت خمی همچو لام تنگ دلی همچو کاف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هین بزن ای فتنه جو بر سر سنگ آن سبو</p></div>
<div class="m2"><p>تا نکشم آب جو تا نکنم اغتراف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترک سقایی کنم غرقه دریا شوم</p></div>
<div class="m2"><p>دور ز جنگ و خلاف بی‌خبر از اعتراف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همچو روان‌های پاک خامش در زیر خاک</p></div>
<div class="m2"><p>قالبشان چون عروس خاک بر او چون لحاف</p></div></div>