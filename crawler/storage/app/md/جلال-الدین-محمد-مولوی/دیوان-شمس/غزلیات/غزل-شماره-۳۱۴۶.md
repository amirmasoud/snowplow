---
title: >-
    غزل شمارهٔ ۳۱۴۶
---
# غزل شمارهٔ ۳۱۴۶

<div class="b" id="bn1"><div class="m1"><p>ای دلزار محنت و بلا داری</p></div>
<div class="m2"><p>بر خدا اعتمادها داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینچنین حضرتی و تو نومید؟</p></div>
<div class="m2"><p>مکن ای دل، اگر خدا داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت اندیشه می‌کشی هرجا</p></div>
<div class="m2"><p>بنگر آخر، جز او کرا داری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطفهایی که کرد چندین گاه</p></div>
<div class="m2"><p>یاد آور اگر وفاداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم سر داد و چشم سر ایزد</p></div>
<div class="m2"><p>چشم جای دگر چرا داری؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر ضایع مکن، که عمر گذشت</p></div>
<div class="m2"><p>زرگری کن، که کیمیا داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سحر مر ترا ندا آید</p></div>
<div class="m2"><p>سو ما آ، که داغ ما داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش ازین تن تو جان پاک بدی</p></div>
<div class="m2"><p>چند خود را ازان جدا داری؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان پاکی، میان خاک سیاه</p></div>
<div class="m2"><p>من نگویم، تو خود روا داری؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خویشتن را تو از قبا بشناس</p></div>
<div class="m2"><p>که ازین آب و گل قبا داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌روی هر شب از قبا بیرون</p></div>
<div class="m2"><p>که جز این دست، دست و پا داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس بود، این قدر بدان گفتم</p></div>
<div class="m2"><p>که درین کوچه آشنا داری</p></div></div>