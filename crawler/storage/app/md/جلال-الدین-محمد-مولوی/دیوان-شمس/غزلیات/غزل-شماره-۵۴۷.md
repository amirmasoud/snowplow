---
title: >-
    غزل شمارهٔ ۵۴۷
---
# غزل شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>سجده کنم پیشکش آن قد و بالا چه شود</p></div>
<div class="m2"><p>دیده کنم پیشکش آن دل بینا چه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده او را نخورم ور نخورم پس کی خورد</p></div>
<div class="m2"><p>گر بخورم نقد و نیندیشم فردا چه شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده او همدل من بام فلک منزل من</p></div>
<div class="m2"><p>گر بگشایم پر خود برپرم آن جا چه شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نشناسم چه بود جان و بدن تا برود</p></div>
<div class="m2"><p>غم نخورم غم نخورم غم نخورم تا چه شود</p></div></div>