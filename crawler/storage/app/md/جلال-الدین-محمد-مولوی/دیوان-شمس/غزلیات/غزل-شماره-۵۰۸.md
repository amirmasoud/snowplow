---
title: >-
    غزل شمارهٔ ۵۰۸
---
# غزل شمارهٔ ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>شیر خدا بند گسستن گرفت</p></div>
<div class="m2"><p>ساقی جان شیشه شکستن گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دزد دلم گشت گرفتار یار</p></div>
<div class="m2"><p>دزد مرا دست ببستن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش چه شب بود که در نیم شب</p></div>
<div class="m2"><p>برق ز رخسار تو جستن گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق تو آورد شراب و کباب</p></div>
<div class="m2"><p>عقل به یک گوشه نشستن گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساغر می قهقهه آغاز کرد</p></div>
<div class="m2"><p>خابیه خونابه گرستن گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل خم باده چو انداخت تیر</p></div>
<div class="m2"><p>بال و پر غصه گسستن گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیر خرد دید که سرده توی</p></div>
<div class="m2"><p>دست ز مستان تو شستن گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طفل دلم را به کرم شیر ده</p></div>
<div class="m2"><p>چون سر پستان تو جستن گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان من از شیر تو شد شیرگیر</p></div>
<div class="m2"><p>وز سگی نفس برستن گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساقی باقی چو به جان باده داد</p></div>
<div class="m2"><p>عمر ابد یافت و بزستن گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیش مگو راز که دلبر به خشم</p></div>
<div class="m2"><p>جانب من کژ نگرستن گرفت</p></div></div>