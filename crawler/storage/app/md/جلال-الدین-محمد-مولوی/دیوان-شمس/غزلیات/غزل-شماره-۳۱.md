---
title: >-
    غزل شمارهٔ ۳۱
---
# غزل شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>بادا مبارک در جهان سور و عروسی‌های ما</p></div>
<div class="m2"><p>سور و عروسی را خدا ببرید بر بالای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهره قرین شد با قمر طوطی قرین شد با شکر</p></div>
<div class="m2"><p>هر شب عروسیی دگر از شاه خوش سیمای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ان القلوب فرجت ان النفوس زوجت</p></div>
<div class="m2"><p>ان الهموم اخرجت در دولت مولای ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسم الله امشب بر نوی سوی عروسی می‌روی</p></div>
<div class="m2"><p>داماد خوبان می‌شوی ای خوب شهرآرای ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش می‌روی در کوی ما خوش می‌خرامی سوی ما</p></div>
<div class="m2"><p>خوش می‌جهی در جوی ما ای جوی و ای جویای ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش می‌روی بر رای ما خوش می‌گشایی پای ما</p></div>
<div class="m2"><p>خوش می‌بُری کف‌های ما ای یوسف زیبای ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تو جفا کردن روا وز ما وفا جستن خطا</p></div>
<div class="m2"><p>پای تصرف را بنه بر جان خون پالای ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جان جان جان را بکش تا حضرت جانان ما</p></div>
<div class="m2"><p>وین استخوان را هم بکش هدیه بر عنقای ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رقصی کنید ای عارفان چرخی زنید ای منصفان</p></div>
<div class="m2"><p>در دولت شاه جهان آن شاه جان افزای ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گردن افکنده دهل در گردک نسرین و گل</p></div>
<div class="m2"><p>کامشب بود دف و دهل نیکوترین کالای ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاموش کامشب زهره شد ساقی به پیمانه و به مد</p></div>
<div class="m2"><p>بگرفته ساغر می‌کشد حمرای ما حمرای ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>والله که این دم صوفیان بستند از شادی میان</p></div>
<div class="m2"><p>در غیب پیش غیبدان از شوق استسقای ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قومی چو دریا کف زنان چون موج‌ها سجده کنان</p></div>
<div class="m2"><p>قومی مبارز چون سنان خون خوار چون اجزای ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاموش کامشب مطبخی شاهست از فرخ رخی</p></div>
<div class="m2"><p>این نادره که می‌پزد حلوای ما حلوای ما</p></div></div>