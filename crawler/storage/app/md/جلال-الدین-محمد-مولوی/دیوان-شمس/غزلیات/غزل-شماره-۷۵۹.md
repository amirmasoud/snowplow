---
title: >-
    غزل شمارهٔ ۷۵۹
---
# غزل شمارهٔ ۷۵۹

<div class="b" id="bn1"><div class="m1"><p>دل من رای تو دارد سر سودای تو دارد</p></div>
<div class="m2"><p>رخ فرسوده زردم غم صفرای تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر من مست جمالت دل من دام خیالت</p></div>
<div class="m2"><p>گهر دیده نثار کف دریای تو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تو هر هدیه که بردم به خیال تو سپردم</p></div>
<div class="m2"><p>که خیال شکرینت فر و سیمای تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلطم گر چه خیالت به خیالات نماند</p></div>
<div class="m2"><p>همه خوبی و ملاحت ز عطاهای تو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل صدبرگ به پیش تو فروریخت ز خجلت</p></div>
<div class="m2"><p>که گمان برد که او هم رخ رعنای تو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر خود پیش فکنده چو گنه کار تو عرعر</p></div>
<div class="m2"><p>که خطا کرد و گمان برد که بالای تو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جگر و جان عزیزان چو رخ زهره فروزان</p></div>
<div class="m2"><p>همه چون ماه گدازان که تمنای تو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل من تابه حلوا ز بر آتش سودا</p></div>
<div class="m2"><p>اگر از شعله بسوزد نه که حلوای تو دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هله چون دوست به دستی همه جا جای نشستی</p></div>
<div class="m2"><p>خنک آن بی‌خبری کو خبر از جای تو دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرم در نگشایی ز ره بام درآیم</p></div>
<div class="m2"><p>که زهی جان لطیفی که تماشای تو دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دو صد بام برآیم به دو صد دام درآیم</p></div>
<div class="m2"><p>چه کنم آهوی جانم سر صحرای تو دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمش ای عاشق مجنون بمگو شعر و بخور خون</p></div>
<div class="m2"><p>که جهان ذره به ذره غم غوغای تو دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوی تبریز شو ای دل بر شمس الحق مفضل</p></div>
<div class="m2"><p>چو خیالش به تو آید که تقاضای تو دارد</p></div></div>