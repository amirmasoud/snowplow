---
title: >-
    غزل شمارهٔ ۲۴۴۹
---
# غزل شمارهٔ ۲۴۴۹

<div class="b" id="bn1"><div class="m1"><p>من پیش از این می‌خواستم گفتار خود را مشتری</p></div>
<div class="m2"><p>و اکنون همی‌خواهم ز تو کز گفت خویشم واخری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت‌ها تراشیدم بسی بهر فریب هر کسی</p></div>
<div class="m2"><p>مست خلیلم من کنون سیر آمدم از آزری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد بتی بی‌رنگ و بو دستم معطل شد بدو</p></div>
<div class="m2"><p>استاد دیگر را بجو بهر دکان بتگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دکان ز خود پرداختم انگازها انداختم</p></div>
<div class="m2"><p>قدر جنون بشناختم ز اندیشه‌ها گشتم بری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر صورتی آید به دل گویم برون رو ای مضل</p></div>
<div class="m2"><p>ترکیب او ویران کنم گر او نماید لمتری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی درخور لیلی بود آن کس کز او مجنون شود</p></div>
<div class="m2"><p>پای علم آن کس بود کو راست جانی آن سری</p></div></div>