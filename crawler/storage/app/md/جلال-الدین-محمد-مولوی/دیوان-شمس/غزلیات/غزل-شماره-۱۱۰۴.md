---
title: >-
    غزل شمارهٔ ۱۱۰۴
---
# غزل شمارهٔ ۱۱۰۴

<div class="b" id="bn1"><div class="m1"><p>باز شد در عاشقی بابی دگر</p></div>
<div class="m2"><p>بر جمال یوسفی تابی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژده بیداران راه عشق را</p></div>
<div class="m2"><p>آنک دیدم دوش من خوابی دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساخته شد از برای طالبان</p></div>
<div class="m2"><p>غیر این اسباب اسبابی دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابرها گر می‌نبارد نقد شد</p></div>
<div class="m2"><p>از برای زندگی آبی دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یارکان سرکش شدند و حق بداد</p></div>
<div class="m2"><p>غیر این اصحاب اصحابی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزه زار عشق را معمور کرد</p></div>
<div class="m2"><p>عاشقان را دشت و دولابی دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وین جگرهایی که بد پرزخم عشق</p></div>
<div class="m2"><p>شد درآویزان به قلابی دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق اگر بدنام گردد غم مخور</p></div>
<div class="m2"><p>عشق دارد نام و القابی دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کفشگر گر خشم گیرد چاره شد</p></div>
<div class="m2"><p>صوفیان را نعل و قبقابی دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نداند حرف صوفی دان که هست</p></div>
<div class="m2"><p>دردهای عشق را بابی دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از هوای شمس دین آموختم</p></div>
<div class="m2"><p>جانب تبریز آدابی دگر</p></div></div>