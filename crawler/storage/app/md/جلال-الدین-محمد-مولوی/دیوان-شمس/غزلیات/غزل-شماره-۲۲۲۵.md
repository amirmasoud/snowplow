---
title: >-
    غزل شمارهٔ ۲۲۲۵
---
# غزل شمارهٔ ۲۲۲۵

<div class="b" id="bn1"><div class="m1"><p>شکر ایزد را که دیدم روی تو</p></div>
<div class="m2"><p>یافتم ناگه رهی من سوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم گریانم ز گریه کند بود</p></div>
<div class="m2"><p>یافت نور از نرگس جادوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس بگفتم کو وصال و کو نجاح</p></div>
<div class="m2"><p>برد این کو کو مرا در کوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لب اقبال و دولت بوسه یافت</p></div>
<div class="m2"><p>این لبان خشک مدحت گوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر غم را اسپری مانع نبود</p></div>
<div class="m2"><p>جز زره‌هایی که دارد موی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسمان جاهی که او شد فرش تو</p></div>
<div class="m2"><p>شیرمردی کو شود آهوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاد بختی که غم تو قوت او است</p></div>
<div class="m2"><p>پهلوانی کو فتد پهلوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جست و جویی در دلم انداختی</p></div>
<div class="m2"><p>تا ز جست و جو روم در جوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک را هایی و هویی کی بدی</p></div>
<div class="m2"><p>گر نبودی جذب‌های و هوی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب دریا تا به کعب آید ورا</p></div>
<div class="m2"><p>کو بیابد بوسه بر زانوی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس که تا هر کس رود بر طبع خویش</p></div>
<div class="m2"><p>جمله خلقان را نباشد خوی تو</p></div></div>