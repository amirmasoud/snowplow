---
title: >-
    غزل شمارهٔ ۲۰۶۵
---
# غزل شمارهٔ ۲۰۶۵

<div class="b" id="bn1"><div class="m1"><p>باز درآمد ز راه فتنه برانگیز من</p></div>
<div class="m2"><p>باز کمر بست سخت یار به استیز من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطبخ دل را نگار باز قباله گرفت</p></div>
<div class="m2"><p>می‌شکند دیگ من کاسه و کفلیز من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه خرابی گرفت ز آنک قنق زفت بود</p></div>
<div class="m2"><p>هیچ نگنجد فلک در در و دهلیز من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه قنق را گرفت غیرت و گفتش مرو</p></div>
<div class="m2"><p>جمله افق را گرفت ابر شکرریز من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر کن ای بوالفضول ای ز کشاکش ملول</p></div>
<div class="m2"><p>جاذبه خیزان او منگر در خیز من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت او را که او منت و شکر آفرید</p></div>
<div class="m2"><p>کز کف کفران گذشت مرکب شبدیز من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رست رخم از عبس کاسه ز ننگ عدس</p></div>
<div class="m2"><p>آخر کاری بکرد اشک غم آمیز من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اصل همه باغ‌ها جان همه لاغ‌ها</p></div>
<div class="m2"><p>چیست اگر زیرکی لاغ دلاویز من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خضر راستین گوهر دریاست این</p></div>
<div class="m2"><p>از تو در این آستین همچو فراویز من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونک مرا یار خواند دست سوی من فشاند</p></div>
<div class="m2"><p>تیز فرس پیش راند خاطر سرتیز من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند نهان می‌کنم شمس حق مغتنم</p></div>
<div class="m2"><p>خواجگیی می‌کند خواجه تبریز من</p></div></div>