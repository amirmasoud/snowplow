---
title: >-
    غزل شمارهٔ ۲۰۷۵
---
# غزل شمارهٔ ۲۰۷۵

<div class="b" id="bn1"><div class="m1"><p>توی که بدرقه باشی گهی گهی رهزن</p></div>
<div class="m2"><p>توی که خرمن مایی و آفت خرمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار جامه بدوزی ز عشق و پاره کنی</p></div>
<div class="m2"><p>و آنگهان بنویسی تو جرم آن بر من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو قلزمی و دو عالم ز توست یک قطره</p></div>
<div class="m2"><p>قراضه‌ای است دو عالم تویی دو صد معدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو راست حکم که گویی به کور چشم گشا</p></div>
<div class="m2"><p>سخن تو بخشی و گویی که گفت آن الکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بساختی ز هوس صد هزار مغناطیس</p></div>
<div class="m2"><p>که نیست لایق آن سنگ خاص هر آهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا چو مست کشانی به سنگ و آهن خویش</p></div>
<div class="m2"><p>مرا چه کار که من جان روشنم یا تن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو باده‌ای تو خماری تو دشمنی و تو دوست</p></div>
<div class="m2"><p>هزار جان مقدس فدای این دشمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو شمس دین به حقی و مفخر تبریز</p></div>
<div class="m2"><p>بهار جان که بدادی سزای صد بهمن</p></div></div>