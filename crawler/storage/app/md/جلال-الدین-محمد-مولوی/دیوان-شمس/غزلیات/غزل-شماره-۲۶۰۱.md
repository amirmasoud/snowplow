---
title: >-
    غزل شمارهٔ ۲۶۰۱
---
# غزل شمارهٔ ۲۶۰۱

<div class="b" id="bn1"><div class="m1"><p>ای بر سر هر سنگی از لعل لبت نوری</p></div>
<div class="m2"><p>وز شورش زلف تو در هر طرفی سوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسن بهشت تو در زیر درختانت</p></div>
<div class="m2"><p>هر سوی یکی ساقی هر سوی یکی حوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشق شراب تو هر سوی یکی جانی</p></div>
<div class="m2"><p>محبوس یکی خنبی چون شیره انگوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر صبح ز عشق تو این عقل شود شیدا</p></div>
<div class="m2"><p>بر بام دماغ آید بنوازد طنبوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شادی آن شهری کش عشق بود سلطان</p></div>
<div class="m2"><p>هر کوی بود بزمی هر خانه بود سوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذشتم بر دیری پیش آمد قسیسی</p></div>
<div class="m2"><p>می‌زد به در وحدت از عشق تو ناقوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ادریس شد از درسش هر جا که بد ابلیسی</p></div>
<div class="m2"><p>در صحبت آن کافر شب گشته چون کافوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم ز کی داری این گفتا ز یکی شاهی</p></div>
<div class="m2"><p>هم عاشق و معشوقی هم ناصر و منصوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک شاه شکرریزی شمس الحق تبریزی</p></div>
<div class="m2"><p>جان پرور هر خویشی شور و شر هر دوری</p></div></div>