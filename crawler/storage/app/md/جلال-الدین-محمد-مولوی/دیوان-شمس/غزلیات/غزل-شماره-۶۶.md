---
title: >-
    غزل شمارهٔ ۶۶
---
# غزل شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>تو را ساقی جان گوید برای ننگ و نامی را</p></div>
<div class="m2"><p>فرومگذار در مجلس چنین اشگرف جامی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خون ما قصاصت را بجو این دم خلاصت را</p></div>
<div class="m2"><p>مهل ساقی خاصت را برای خاص و عامی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکش جام جلالی را فدا کن نفس و مالی را</p></div>
<div class="m2"><p>مشو سخره حلالی را مخوان باده حرامی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلط کردار نادانی همه نامیست یا نانی</p></div>
<div class="m2"><p>تو را چون پخته شد جانی مگیر ای پخته خامی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کز نام می‌لافد بهل کز غصه بشکافد</p></div>
<div class="m2"><p>چو آن مرغی که می‌بافد به گرد خویش دامی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این دام و در این دانه مجو جز عشق جانانه</p></div>
<div class="m2"><p>مگو از چرخ وز خانه تو دیده گیر بامی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو شین و کاف و ری را خود مگو شکر که هست از نی</p></div>
<div class="m2"><p>مگو القاب جان حی یکی نقش و کلامی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بی‌صورت تو جان باشی چه نقصان گر نهان باشی</p></div>
<div class="m2"><p>چرا دربند آن باشی که واگویی پیامی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا ای هم دل محرم بگیر این باده خرم</p></div>
<div class="m2"><p>چنان سرمست شو این دم که نشناسی مقامی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برو ای راه ره پیما بدان خورشید جان افزا</p></div>
<div class="m2"><p>از این مجنون پرسودا ببر آن جا سلامی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگو ای شمس تبریزی از آن می‌های پاییزی</p></div>
<div class="m2"><p>به خود در ساغرم ریزی نفرمایی غلامی را</p></div></div>