---
title: >-
    غزل شمارهٔ ۲۹۴۷
---
# غزل شمارهٔ ۲۹۴۷

<div class="b" id="bn1"><div class="m1"><p>یا من عجب فتادم یا تو عجب فتادی</p></div>
<div class="m2"><p>چندین قدح بخوردی جامی به من ندادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو از شراب مستی من هم ز بوی مستم</p></div>
<div class="m2"><p>بو نیز نیست اندک در بزم کیقبادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسیار عاشقان را کشتی تو بی‌گناهی</p></div>
<div class="m2"><p>در رنج و غم نکشتی کشتی ز ذوق و شادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای تو گشاد عالم ای تو مراد آدم</p></div>
<div class="m2"><p>خانه چرا گرفتی در کوی بی‌مرادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیرا چراغ روشن در ظلمت شب آید</p></div>
<div class="m2"><p>درمان به درد آید این است اوستادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بستی زبان و گوشم تا جز غمت ننوشم</p></div>
<div class="m2"><p>نی نکته عمیدی نی گفته عمادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تبریز شمس دین را خدمت رسان ز مستان</p></div>
<div class="m2"><p>سجده کن و بگویش اوحشت یا فؤادی</p></div></div>