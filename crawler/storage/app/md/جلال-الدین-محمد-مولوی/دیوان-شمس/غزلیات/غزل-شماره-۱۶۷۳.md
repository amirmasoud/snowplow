---
title: >-
    غزل شمارهٔ ۱۶۷۳
---
# غزل شمارهٔ ۱۶۷۳

<div class="b" id="bn1"><div class="m1"><p>امشب ای دلدار مهمان توییم</p></div>
<div class="m2"><p>شب چه باشد روز و شب آن توییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا باشیم و هر جا که رویم</p></div>
<div class="m2"><p>حاضران کاسه و خوان توییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش‌های صنعت دست توییم</p></div>
<div class="m2"><p>پروریده نعمت و نان توییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون کبوترزاده برج توییم</p></div>
<div class="m2"><p>در سفر طواف ایوان توییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیث ما کنتم فولوا شطره</p></div>
<div class="m2"><p>با زجاجه دل پری خوان توییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان نقشی کنی در مغز ما</p></div>
<div class="m2"><p>ما صحیفه خط و عنوان توییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو موسی کم خوریم از دایه شیر</p></div>
<div class="m2"><p>زانک مست شیر و پستان توییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایمنیم از دزد و مکر راه زن</p></div>
<div class="m2"><p>زانک چون زر در حرمدان توییم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان چنین مست است و دلخوش جان ما</p></div>
<div class="m2"><p>که سبکسار و گران جان توییم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوی زرین فلک رقصان ماست</p></div>
<div class="m2"><p>چون نباشد چون که چوگان توییم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواه چوگان ساز ما را خواه گوی</p></div>
<div class="m2"><p>دولت این بس که به میدان توییم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواه ما را مار کن خواهی عصا</p></div>
<div class="m2"><p>معجز موسی و برهان توییم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر عصا سازیم بیفشانیم برگ</p></div>
<div class="m2"><p>وقت خشم و جنگ ثعبان توییم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق ما را پشت داری می کند</p></div>
<div class="m2"><p>زانک خندان روی بستان توییم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سایه ساز ماست نور سایه سوز</p></div>
<div class="m2"><p>زانک همچون مه به میزان توییم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم تو بگشا این دهان را هم تو بند</p></div>
<div class="m2"><p>بند آن توست و انبان توییم</p></div></div>