---
title: >-
    غزل شمارهٔ ۱۹۸۲
---
# غزل شمارهٔ ۱۹۸۲

<div class="b" id="bn1"><div class="m1"><p>گلسن بنده ستایک غرضم یق اشد رسن</p></div>
<div class="m2"><p>قلسن انده یوز در یلنز قنده قلرسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چلبی درقیمو درلک چلبا گل نه گز رسن</p></div>
<div class="m2"><p>چلبی قللرن استر چلبی نه سز سن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه اغر در نه اغر در چلب اغرندن قغرمق</p></div>
<div class="m2"><p>قولغن اج قولغن اج بله کم انده دگرسن</p></div></div>