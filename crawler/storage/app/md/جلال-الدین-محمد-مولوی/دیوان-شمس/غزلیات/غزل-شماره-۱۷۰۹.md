---
title: >-
    غزل شمارهٔ ۱۷۰۹
---
# غزل شمارهٔ ۱۷۰۹

<div class="b" id="bn1"><div class="m1"><p>ما قحطیان تشنه و بسیارخواره‌ایم</p></div>
<div class="m2"><p>بیچاره نیستیم که درمان و چاره‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم چون عقار و گه رزم ذوالفقار</p></div>
<div class="m2"><p>در شکر همچو چشمه و در صبر خاره‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما پادشاه رشوت باره نبوده‌ایم</p></div>
<div class="m2"><p>بل پاره دوز خرقه دل‌های پاره‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ما مپوش راز که در سینه توایم</p></div>
<div class="m2"><p>وز ما مدزد دل که نه ما دل فشاره‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما آب قلزمیم نهان گشته زیر کاه</p></div>
<div class="m2"><p>یا آفتاب تن زده اندر ستاره‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را ببین تو مست چنین بر کنار بام</p></div>
<div class="m2"><p>داند کنار بام که ما بی‌کناره‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهتاب را چه ترس بود از کنار بام</p></div>
<div class="m2"><p>پس ما چه غم خوریم که بر مه سواره‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تیردوز گشت جگرهای ما ز عشق</p></div>
<div class="m2"><p>بی‌زحمت جگر تو ببین خون چه کاره‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصاب ده اگر چه که ما را بکشت زار</p></div>
<div class="m2"><p>هم می چریم در ده و هم بر قناره‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما مهره‌ایم و هم جهت مهره حقه‌ایم</p></div>
<div class="m2"><p>هنگامه گیر دل شده و هم نظاره‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاموش باش اگر چه به بشرای احمدی</p></div>
<div class="m2"><p>همچون مسیح ناطق طفل گواره‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عشق شمس مفخر تبریز روز و شب</p></div>
<div class="m2"><p>بر چرخ دیوکش چو شهاب و شراره‌ایم</p></div></div>