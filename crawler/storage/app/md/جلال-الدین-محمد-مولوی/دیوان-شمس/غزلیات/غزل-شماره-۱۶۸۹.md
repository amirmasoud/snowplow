---
title: >-
    غزل شمارهٔ ۱۶۸۹
---
# غزل شمارهٔ ۱۶۸۹

<div class="b" id="bn1"><div class="m1"><p>صد بار مردم ای جان وین را بیازمودم</p></div>
<div class="m2"><p>چون بوی تو بیامد دیدم که زنده بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بار جان بدادم وز پای درفتادم</p></div>
<div class="m2"><p>بار دگر بزادم چون بانگ تو شنودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا روی تو بدیدم از خویش نابدیدم</p></div>
<div class="m2"><p>ای ساخته چو عیدم وی سوخته چو عودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامی است در ضمیرم تا باز عشق گیرم</p></div>
<div class="m2"><p>آن باز بازگونه چون مرغ درربودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شعله‌های گردان در سینه‌های مردان</p></div>
<div class="m2"><p>گردان به گرد ماهت چون گنبد کبودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ساعت خجسته تو عهدها ببسته</p></div>
<div class="m2"><p>من توبه‌ها شکسته بودم چنانک بودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقلم ببرد از ره کز من رسی تو در شه</p></div>
<div class="m2"><p>چون سوی عقل رفتم عقلم نداشت سودم</p></div></div>