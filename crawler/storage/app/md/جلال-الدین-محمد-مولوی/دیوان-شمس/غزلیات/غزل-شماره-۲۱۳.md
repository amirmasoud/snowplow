---
title: >-
    غزل شمارهٔ ۲۱۳
---
# غزل شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>اگر تو عاشق عشقی و عشق را جویا</p></div>
<div class="m2"><p>بگیر خنجر تیز و ببر گلوی حیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدانک سد عظیم است در روش ناموس</p></div>
<div class="m2"><p>حدیث بی‌غرض است این قبول کن به صفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار گونه جنون از چه کرد آن مجنون</p></div>
<div class="m2"><p>هزار شید برآورد آن گزین شیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی قباش درید و گهی به کوه دوید</p></div>
<div class="m2"><p>گهی ز زهر چشید و گهی گزید فنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو عنکبوت چنان صیدهای زفت گرفت</p></div>
<div class="m2"><p>ببین چه صید کند دام ربی الاعلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو عشق چهره لیلی بدان همه ارزید</p></div>
<div class="m2"><p>چگونه باشد اسری بعبده لیلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندیده‌ای تو دواوین ویسه و رامین</p></div>
<div class="m2"><p>نخوانده‌ای تو حکایات وامق و عذرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو جامه گرد کنی تا ز آب تر نشود</p></div>
<div class="m2"><p>هزار غوطه تو را خوردنی‌ست در دریا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طریق عشق همه مستی آمد و پستی</p></div>
<div class="m2"><p>که سیل پست رود کی رود سوی بالا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان حلقه عشاق چون نگین باشی</p></div>
<div class="m2"><p>اگر تو حلقه به گوش تکینی ای مولا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنانک حلقه به گوش است چرخ را این خاک</p></div>
<div class="m2"><p>چنانک حلقه به گوش است روح را اعضا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا بگو چه زیان کرد خاک از این پیوند</p></div>
<div class="m2"><p>چه لطف‌ها که نکرده‌ست عقل با اجزا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهل به زیر گلیم ای پسر نشاید زد</p></div>
<div class="m2"><p>علم بزن چو دلیران میانه صحرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گوش جان بشنو از غریو مشتاقان</p></div>
<div class="m2"><p>هزار غلغله در جو گنبد خضرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو برگشاید بند قبا ز مستی عشق</p></div>
<div class="m2"><p>توهای و هوی ملک بین و حیرت حورا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه اضطراب که بالا و زیر عالم راست</p></div>
<div class="m2"><p>ز عشق کوست منزه ز زیر و از بالا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آفتاب برآمد کجا بماند شب</p></div>
<div class="m2"><p>رسید جیش عنایت کجا بماند عنا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خموش کردم ای جان جان جان تو بگو</p></div>
<div class="m2"><p>که ذره ذره ز عشق رخ تو شد گویا</p></div></div>