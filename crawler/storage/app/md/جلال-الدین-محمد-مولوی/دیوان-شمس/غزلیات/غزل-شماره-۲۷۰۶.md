---
title: >-
    غزل شمارهٔ ۲۷۰۶
---
# غزل شمارهٔ ۲۷۰۶

<div class="b" id="bn1"><div class="m1"><p>چه دلشادم به دلدار خدایی</p></div>
<div class="m2"><p>خدایا تو نگهدار از جدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا ای خواجه بنگر یار ما را</p></div>
<div class="m2"><p>چو از اصحاب و از یاران مایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان شرطی که با ما کژ نبازی</p></div>
<div class="m2"><p>وگر بازی تو با ما برنیایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دغایانی که با جسم چو پیلند</p></div>
<div class="m2"><p>سوار اسب فرهنگ و کیانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیاده گشته و رخ زرد ماندند</p></div>
<div class="m2"><p>ز فرزین بند شاهان بقایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه بودی گر بدانستی مهی را</p></div>
<div class="m2"><p>شکسته اختری در بی‌وفایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر مه را نداند ماه ماه است</p></div>
<div class="m2"><p>چگونه مه نه ارضی نی سمایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که ارضی و سمایی را غروب است</p></div>
<div class="m2"><p>فتد بی‌اختیارش اختفایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ظهور و اختفای ماه جانی</p></div>
<div class="m2"><p>به دست او است در قدرت نمایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسوز ای تن که جان را چون سپندی</p></div>
<div class="m2"><p>به دفع چشم بد چون کیمیایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که چشم بد به جز بر جسم ناید</p></div>
<div class="m2"><p>به معنی کی رسد چشم هوایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کناری گیرمش در جامه تن</p></div>
<div class="m2"><p>که جان را زو است هر دم جان فزایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خیالت هر دمی این جاست با ما</p></div>
<div class="m2"><p>الا ای شمس تبریزی کجایی</p></div></div>