---
title: >-
    غزل شمارهٔ ۱۹۸۱
---
# غزل شمارهٔ ۱۹۸۱

<div class="b" id="bn1"><div class="m1"><p>مطربا نرمک بزن تا روح بازآید به تن</p></div>
<div class="m2"><p>چون زنی بر نام شمس الدین تبریزی بزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام شمس الدین به گوشت بهتر است از جسم و جان</p></div>
<div class="m2"><p>نام شمس الدین چو شمع و جان بنده چون لگن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطربا بهر خدا تو غیر شمس الدین مگو</p></div>
<div class="m2"><p>بر تن چون جان او بنواز تن تن تن تنن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شود این نقش تو رقصان به سوی آسمان</p></div>
<div class="m2"><p>تا شود این جان پاکت پرده سوز و گام زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمس دین و شمس دین و شمس دین می گوی و بس</p></div>
<div class="m2"><p>تا ببینی مردگان رقصان شده اندر کفن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطربا گر چه نیی عاشق مشو از ما ملول</p></div>
<div class="m2"><p>عشق شمس الدین کند مر جانت را چون یاسمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاله‌ها دستک زنان و یاسمین رقصان شده</p></div>
<div class="m2"><p>سوسنک مستک شده گوید که باشد خود سمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خارها خندان شده بر گل بجسته برتری</p></div>
<div class="m2"><p>سنگ‌ها باجان شده با لعل گوید ما و من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایها الساقی ادر کأس الحمیا نصفه</p></div>
<div class="m2"><p>ان عشقی مثل خمر ان جسمی مثل دن</p></div></div>