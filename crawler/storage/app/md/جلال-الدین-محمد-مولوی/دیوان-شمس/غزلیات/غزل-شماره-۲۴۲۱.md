---
title: >-
    غزل شمارهٔ ۲۴۲۱
---
# غزل شمارهٔ ۲۴۲۱

<div class="b" id="bn1"><div class="m1"><p>مطرب جان‌های دل برده</p></div>
<div class="m2"><p>تا به شب تا به شب همین پرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان‌هایی که مست و مخمورند</p></div>
<div class="m2"><p>بر سر باده باده‌ای خورده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات مفردان رفته</p></div>
<div class="m2"><p>خرقه آب و گل گرو کرده</p></div></div>