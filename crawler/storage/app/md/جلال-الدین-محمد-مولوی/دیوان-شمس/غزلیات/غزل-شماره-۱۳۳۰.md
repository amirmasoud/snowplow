---
title: >-
    غزل شمارهٔ ۱۳۳۰
---
# غزل شمارهٔ ۱۳۳۰

<div class="b" id="bn1"><div class="m1"><p>بگردان شراب ای صنم بی‌درنگ</p></div>
<div class="m2"><p>که بزمست و چنگ و ترنگاترنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی بزم روحست و ساقی غیب</p></div>
<div class="m2"><p>ببویید بوی و نبینید رنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو صحرای دل بین در آن قطره خون</p></div>
<div class="m2"><p>زهی دشت بی‌حد در آن کنج تنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن بزم قدسند ابدال مست</p></div>
<div class="m2"><p>نه قدسی که افتد به دست فرنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه افرنگ عقلی که بود اصل دین</p></div>
<div class="m2"><p>چو حلقه‌ست بر در در آن کوی و دنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خشکیست این عقل و دریاست آن</p></div>
<div class="m2"><p>بمانده است بیرون ز بیم نهنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بده می گزافه به مستان حق</p></div>
<div class="m2"><p>که نی عربده بینی آن جا نه جنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی جام بنمودشان در الست</p></div>
<div class="m2"><p>که از جام خورشید دارند ننگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو گویی که بی‌دست و شیشه که دید</p></div>
<div class="m2"><p>شراب دلارام و بکنی و بنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببین نیم شب خلق را جمله مست</p></div>
<div class="m2"><p>ز سغراق خواب و ز ساقی زنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطار شتر بین که گشتند مست</p></div>
<div class="m2"><p>ندانند افسار از پالهنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمش کن که اغلب همه باخودند</p></div>
<div class="m2"><p>همه شهر لنگند تو هم بلنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ره سیرت شمس تبریز گیر</p></div>
<div class="m2"><p>به جرات چو شیر و به حمله پلنگ</p></div></div>