---
title: >-
    غزل شمارهٔ ۲۹
---
# غزل شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ای از ورای پرده‌ها تاب تو تابستان ما</p></div>
<div class="m2"><p>ما را چو تابستان ببر دل گرم تا بستان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چشم جان را توتیا آخر کجا رفتی بیا</p></div>
<div class="m2"><p>تا آب رحمت برزند از صحن آتشدان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سبزه گردد شوره‌ها تا روضه گردد گورها</p></div>
<div class="m2"><p>انگور گردد غوره‌ها تا پخته گردد نان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آفتاب جان و دل ای آفتاب از تو خجل</p></div>
<div class="m2"><p>آخر ببین کاین آب و گل چون بست گرد جان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد خارها گلزارها از عشق رویت بارها</p></div>
<div class="m2"><p>تا صد هزار اقرارها افکند در ایمان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای صورت عشق ابد خوش رو نمودی در جسد</p></div>
<div class="m2"><p>تا ره بری سوی احد جان را از این زندان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دود غم بگشا طرب روزی نما از عین شب</p></div>
<div class="m2"><p>روزی غریب و بوالعجب ای صبح نورافشان ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهر کنی خرمهره را زهره بدری زهره را</p></div>
<div class="m2"><p>سلطان کنی بی‌بهره را شاباش ای سلطان ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کو دیده‌ها درخورد تو تا دررسد در گرد تو</p></div>
<div class="m2"><p>کو گوش هوش آورد تو تا بشنود برهان ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون دل شود احسان شمر در شکر آن شاخ شکر</p></div>
<div class="m2"><p>نعره برآرد چاشنی از بیخ هر دندان ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد ز جان بانگ دهل تا جزوها آید به کل</p></div>
<div class="m2"><p>ریحان به ریحان گل به گل از حبس خارستان ما</p></div></div>