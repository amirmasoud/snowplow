---
title: >-
    غزل شمارهٔ ۶۰۸
---
# غزل شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>ایمان بر کفر تو ای شاه چه کس باشد</p></div>
<div class="m2"><p>سیمرغ فلک پیما پیش تو مگس باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب حیوان ایمان خاک سیهی کفران</p></div>
<div class="m2"><p>بر آتش تو هر دو ماننده خس باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان را صفت ایمان شد وین جان به نفس جان شد</p></div>
<div class="m2"><p>دل غرقه عمان شد چه جای نفس باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب کفر و چراغ ایمان خورشید چو شد رخشان</p></div>
<div class="m2"><p>با کفر بگفت ایمان رفتیم که بس باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمان فرسی دین را مر نفس چو فرزین را</p></div>
<div class="m2"><p>وان شاه نوآیین را چه جای فرس باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایمان گودت پیش آ وان کفر گود پس رو</p></div>
<div class="m2"><p>چون شمع تنت جان شد نی پیش و نی پس باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس الحق تبریزی رانی تو چنان بالا</p></div>
<div class="m2"><p>تا جز من پابرجا خود دست مرس باشد</p></div></div>