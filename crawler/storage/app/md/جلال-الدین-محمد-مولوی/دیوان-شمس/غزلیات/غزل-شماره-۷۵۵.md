---
title: >-
    غزل شمارهٔ ۷۵۵
---
# غزل شمارهٔ ۷۵۵

<div class="b" id="bn1"><div class="m1"><p>شاه ما از جمله شاهان پیش بود و بیش بود</p></div>
<div class="m2"><p>زانک شاهنشاه ما هم شاه و هم درویش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه ما از پرده برجان چو خود را جلوه کرد</p></div>
<div class="m2"><p>جان ما بی‌خویش شد زیرا که شه بی‌خویش بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه ما از جان ما هم دور و هم نزدیک بود</p></div>
<div class="m2"><p>جان ما با شاه ما نزدیک و دوراندیش بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاف او بی‌درد بود و راحتش بی‌درد بود</p></div>
<div class="m2"><p>گلشن بی‌خار بود و نوش او بی‌نیش بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک صفت از لطف شه آن جا که پرده برگرفت</p></div>
<div class="m2"><p>آب و آتش صلح کرد و گرگ دایه میش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان مطلق شد ز نورش صورتی کو جان نداشت</p></div>
<div class="m2"><p>گشت قربان رهش آن کس که او بدکیش بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست می‌گفتیم اندر هست گفت آری بیا</p></div>
<div class="m2"><p>هست شد عالم از او موقوف یک آریش بود</p></div></div>