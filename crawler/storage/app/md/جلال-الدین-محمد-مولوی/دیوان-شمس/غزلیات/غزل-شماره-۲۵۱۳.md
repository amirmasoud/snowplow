---
title: >-
    غزل شمارهٔ ۲۵۱۳
---
# غزل شمارهٔ ۲۵۱۳

<div class="b" id="bn1"><div class="m1"><p>بیا ای عارف مطرب چه باشد گر ز خوش خویی</p></div>
<div class="m2"><p>چو شعری نور افشانی و زان اشعار برگویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان جمله مردان به درد جمله بادردان</p></div>
<div class="m2"><p>که برگو تا چه می‌خواهی و زین حیران چه می‌جویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن روی چو ماه او ز عشق حسن خواه او</p></div>
<div class="m2"><p>بیاموزید ای خوبان رخ افروزی و مه رویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن چشم سیاه او وزان زلف سه تاه او</p></div>
<div class="m2"><p>الا ای اهل هندستان بیاموزید هندویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز غمزه تیراندازش کرشمه ساحری سازش</p></div>
<div class="m2"><p>هلا هاروت و ماروتم بیاموزید جادویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایا اصحاب و خلوتیان شده دل را چنان جویان</p></div>
<div class="m2"><p>ز لعل جان فزای او بیاموزید دلجویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خرمنگاه شش گوشه نخواهی یافتن خوشه</p></div>
<div class="m2"><p>روان شو سوی بی‌سویان رها کن رسم شش سویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه عالم ز تو نالان تو باری از چه می‌نالی</p></div>
<div class="m2"><p>چو از تو کم نشد یک مو نمی‌دانم چه می‌مویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فدایم آن کبوتر را که بر بام تو می‌پرد</p></div>
<div class="m2"><p>کجایی ای سگ مقبل که اهل آن چنان کویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آن عمر عزیز آمد چرا عشرت نمی‌سازی</p></div>
<div class="m2"><p>چو آن استاد جان آمد چرا تخته نمی‌شویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در این دام است آن آهو تو در صحرا چه می‌گردی</p></div>
<div class="m2"><p>گهر در خانه گم کردی به هر ویران چه می‌پویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هر روزی در این خانه یکی حجره نوی یابی</p></div>
<div class="m2"><p>تو یک تو نیستی ای جان تفحص کن که صدتویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر کفری و گر دینی اگر مهری و گر کینی</p></div>
<div class="m2"><p>همو را بین همو را دان یقین می‌دان که با اویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بماند آن نادره دستان ولیکن ساقی مستان</p></div>
<div class="m2"><p>گرفت این دم گلوی من که بفشارم گر افزویی</p></div></div>