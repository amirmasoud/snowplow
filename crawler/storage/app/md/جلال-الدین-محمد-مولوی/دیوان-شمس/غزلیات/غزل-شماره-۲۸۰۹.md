---
title: >-
    غزل شمارهٔ ۲۸۰۹
---
# غزل شمارهٔ ۲۸۰۹

<div class="b" id="bn1"><div class="m1"><p>ساخت بغراقان به رسم عید بغراقانیی</p></div>
<div class="m2"><p>زهره آمد ز آسمان و می‌زند سرخوانیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جبرئیل آمد به مهمان بار دیگر تا خلیل</p></div>
<div class="m2"><p>می‌کند عجل سمین را از کرم بریانیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز مهمانی است امروز الصلا جان‌های پاک</p></div>
<div class="m2"><p>هین ز سرها کاسه زیبا در چنین مهمانیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانگ جوشاجوش آمد بامدادان مر مرا</p></div>
<div class="m2"><p>بوی خوش می‌آیدم از قلیه و بورانیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌کشید آن بو مرا تا جانب مطبخ شدم</p></div>
<div class="m2"><p>مطبخی پرنور دیدم مطبخی نورانیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش زان کفچه‌ای تا نفس من ساکن شود</p></div>
<div class="m2"><p>گفت رو کاین نیست ای جان بهره انسانیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون منش الحاح کردم کفچه را زد بر سرم</p></div>
<div class="m2"><p>در سر و عقلم درآمد مستی و ویرانیی</p></div></div>