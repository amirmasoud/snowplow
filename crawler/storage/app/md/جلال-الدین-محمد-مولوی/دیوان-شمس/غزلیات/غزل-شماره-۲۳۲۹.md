---
title: >-
    غزل شمارهٔ ۲۳۲۹
---
# غزل شمارهٔ ۲۳۲۹

<div class="b" id="bn1"><div class="m1"><p>هر موی من از عشقت بیت و غزلی گشته</p></div>
<div class="m2"><p>هر عضو من از ذوقت خم عسلی گشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید حمل رویت دریای عسل خویت</p></div>
<div class="m2"><p>هر ذره ز خورشیدت صاحب عملی گشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این دل ز هوای تو دل را به هوا داده</p></div>
<div class="m2"><p>وین جان ز لقای تو برج حملی گشته</p></div></div>