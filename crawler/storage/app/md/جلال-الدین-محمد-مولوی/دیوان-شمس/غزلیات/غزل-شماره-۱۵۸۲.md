---
title: >-
    غزل شمارهٔ ۱۵۸۲
---
# غزل شمارهٔ ۱۵۸۲

<div class="b" id="bn1"><div class="m1"><p>هرچ گویی از بهانه لا نسلم لا نسلم</p></div>
<div class="m2"><p>کار دارم من به خانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته‌ای فردا بیایم لطف و نیکویی نمایم</p></div>
<div class="m2"><p>وعده‌ست این بی‌نشانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته‌ای رنجور دارم دل ز غم پرشور دارم</p></div>
<div class="m2"><p>این فریب است و بهانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت مادر مادرانه چون ببینی دام و دانه</p></div>
<div class="m2"><p>این چنین گو ره روانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوییم امروز زارم نیت حمام دارم</p></div>
<div class="m2"><p>می نمایی سنگ و شانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا خوانند ما را تا فریبانند ما را</p></div>
<div class="m2"><p>غیر این عالی ستانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر مستان بیایی هر دمی زحمت نمایی</p></div>
<div class="m2"><p>کاین فلان است آن فلانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوییم من خواجه تاشم عاقبت اندیش باشم</p></div>
<div class="m2"><p>تا درافتی در میانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو ترش کرد آن مپرسم تا ز شکل او بترسم</p></div>
<div class="m2"><p>ای عجوزه بامثانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست از خشمم گزیدی گویی از عشقت گزیدم</p></div>
<div class="m2"><p>مغلطه است این ای یگانه لا نسلم لا نسلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمله را نتوان شمردن شرح یک یک حیله کردن</p></div>
<div class="m2"><p>نیست مکرت را کرانه لا نسلم لا نسلم</p></div></div>