---
title: >-
    غزل شمارهٔ ۱۸۷۲
---
# غزل شمارهٔ ۱۸۷۲

<div class="b" id="bn1"><div class="m1"><p>ای در غم بیهوده رو کم ترکوا برخوان</p></div>
<div class="m2"><p>وی حرص تو افزوده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از اسپک و از زینک پربادک و پرکینک</p></div>
<div class="m2"><p>وز غصه بیالوده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در روده و سرگینی باد هوس و کینی</p></div>
<div class="m2"><p>ای غافل آلوده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شیخ پر از دعوی وی صورت بی‌معنی</p></div>
<div class="m2"><p>نابوده و بنموده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منگر که شه و میری بنگر که همی‌میری</p></div>
<div class="m2"><p>در زیر یکی توده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن نازک و آن مشتک آن ما و من زشتک</p></div>
<div class="m2"><p>پوسیده و فرسوده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخ بر رخ زیبایان کم نه بنگر پایان</p></div>
<div class="m2"><p>رخسار تو فرسوده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر باغ و سرا داری با مرگ چه پا داری</p></div>
<div class="m2"><p>در گور گل اندوده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفتند جهان داران خون خواره و عیاران</p></div>
<div class="m2"><p>بر خلق نبخشوده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تابوت کسان دیده وز دور بخندیده</p></div>
<div class="m2"><p>وان چشم تو نگشوده رو کم ترکوا برخوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس کن ز سخن گویی از گفت چه می جویی</p></div>
<div class="m2"><p>ای بادبپیموده رو کم ترکوا برخوان</p></div></div>