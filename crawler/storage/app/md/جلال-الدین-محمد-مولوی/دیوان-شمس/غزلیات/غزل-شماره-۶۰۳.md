---
title: >-
    غزل شمارهٔ ۶۰۳
---
# غزل شمارهٔ ۶۰۳

<div class="b" id="bn1"><div class="m1"><p>گویند به بلا ساقون ترکی دو کمان دارد</p></div>
<div class="m2"><p>ور زان دو یکی کم شد ما را چه زیان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای در غم بیهوده از بوده و نابوده</p></div>
<div class="m2"><p>کاین کیسه زر دارد وان کاسه و خوان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شام اگر میری زینی به کسی بخشد</p></div>
<div class="m2"><p>جانت ز حسد این جا رنج خفقان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز غمزه چشم شه جز غصه خشم شه</p></div>
<div class="m2"><p>والله که نیندیشد هر زنده که جان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیوانه کنم خود را تا هرزه نیندیشم</p></div>
<div class="m2"><p>دیوانه من از اصلم ای آنک عیان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون عقل ندارم من پیش آ که تویی عقلم</p></div>
<div class="m2"><p>تو عقل بسی آن را کو چون تو شبان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر طاعت کم دارم تو طاعت و خیر من</p></div>
<div class="m2"><p>آن را که تویی طاعت از خوف امان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای کوزه گر صورت مفروش مرا کوزه</p></div>
<div class="m2"><p>کوزه چه کند آن کس کو جوی روان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو وقف کنی خود را بر وقف یکی مرده</p></div>
<div class="m2"><p>من وقف کسی باشم کو جان و جهان دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو نیز بیا یارا تا یار شوی ما را</p></div>
<div class="m2"><p>زیرا که ز جان ما جان تو نشان دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس الحق تبریزی خورشید وجود آمد</p></div>
<div class="m2"><p>کان چرخ چه چرخست آن کان جا سیران دارد</p></div></div>