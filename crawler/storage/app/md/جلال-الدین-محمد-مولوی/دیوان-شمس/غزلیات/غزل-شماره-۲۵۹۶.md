---
title: >-
    غزل شمارهٔ ۲۵۹۶
---
# غزل شمارهٔ ۲۵۹۶

<div class="b" id="bn1"><div class="m1"><p>نظاره چه می‌آیی در حلقه بیداری</p></div>
<div class="m2"><p>گر سینه نپوشانی تیری بخوری کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلقه سر اندرکن دل را تو قویتر کن</p></div>
<div class="m2"><p>شاهی است تو باور کن بر کرسی جباری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بازرهی زان دم تا مست شوی هر دم</p></div>
<div class="m2"><p>گاهی ز لب لعلش گاهی ز می ناری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگشای دهانت را خاشاک مجو در می</p></div>
<div class="m2"><p>خاشاک کجا باشد در ساغر هشیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خواجه چرا جویی دلداری از آن جانان</p></div>
<div class="m2"><p>بس نیست رخ خوبش دلجویی و دلداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی نامه او خواندم در قصه بی‌خویشی</p></div>
<div class="m2"><p>بنوشتم از عالم صد نامه بیزاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش تو چو نقش من رخ بر رخ خود کرده‌ست</p></div>
<div class="m2"><p>با ما غم دل گویی یا قصه جان آری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من با صنم معنی تن جامه برون کردم</p></div>
<div class="m2"><p>چون عشق بزد آتش در پرده ستاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در رنگ رخم عشقش چون عکس جمالش دید</p></div>
<div class="m2"><p>افتاد به پایم عشق در عذر گنه کاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمس الحق تبریزی آیی و نبینندت</p></div>
<div class="m2"><p>زیرا که چو جان آیی بی‌رنگ صباواری</p></div></div>