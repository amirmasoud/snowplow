---
title: >-
    غزل شمارهٔ ۱۶۹۳
---
# غزل شمارهٔ ۱۶۹۳

<div class="b" id="bn1"><div class="m1"><p>من پاکباز عشقم تخم غرض نکارم</p></div>
<div class="m2"><p>پشت و پناه فقرم پشت طمع نخارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی بند خلق باشم نی از کسی تراشم</p></div>
<div class="m2"><p>مرغ گشاده پایم برگ قفس ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ابر آب دارم چرخ گهرنثارم</p></div>
<div class="m2"><p>بر تشنگان خاکی آب حیات بارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موسی بدید آتش آن نور بود دلخوش</p></div>
<div class="m2"><p>من نیز نورم ای جان گر چه ز دور نارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخ درخت گردان اصل درخت ساکن</p></div>
<div class="m2"><p>گر چه که بی‌قرارم در روح برقرارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بوالعجب جهانم در مشت گل نهانم</p></div>
<div class="m2"><p>در هر شبی چو روزم در هر خزان بهارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با مرغ شب شبم من با مرغ روز روزم</p></div>
<div class="m2"><p>اما چو باخود آیم زین هر دو برکنارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن لحظه باخود آیم کز محو بیخود آیم</p></div>
<div class="m2"><p>شش دانگ آن گهم که بیرون ز پنج و چارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان بشر به ناحق دعویش اختیار است</p></div>
<div class="m2"><p>بی‌اختیار گردد در فر اختیارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن عقل پرهنر را بادی است در سر او</p></div>
<div class="m2"><p>آن باد او نماند چون باده‌ای درآرم</p></div></div>