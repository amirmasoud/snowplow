---
title: >-
    غزل شمارهٔ ۲۶۶۹
---
# غزل شمارهٔ ۲۶۶۹

<div class="b" id="bn1"><div class="m1"><p>برفتیم ای عقیق لامکانی</p></div>
<div class="m2"><p>ز شهر تو تو باید که بمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفر کردیم چون استارگان ما</p></div>
<div class="m2"><p>ز تو هم سوی تو که آسمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی صورت رود دیگر بیاید</p></div>
<div class="m2"><p>به مهمانخانه‌ات زیرا که جانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که مهمانان مثال چار فصلند</p></div>
<div class="m2"><p>تو اصل فصل‌هایی که جهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال خوب تو در سینه بردیم</p></div>
<div class="m2"><p>شفق از آفتاب آمد نشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیشت ماند دل با ما نیامد</p></div>
<div class="m2"><p>دل از تو کی رود چون دلستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر دل‌ها به زیر سایه‌ات باد</p></div>
<div class="m2"><p>که دل‌ها را در این مرعا شبانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فروریزید دندان‌های گرگان</p></div>
<div class="m2"><p>از آنگه که نمودی مهربانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهل تا بحر گوید قصه خویش</p></div>
<div class="m2"><p>که تا باری ببینی قصه خوانی</p></div></div>