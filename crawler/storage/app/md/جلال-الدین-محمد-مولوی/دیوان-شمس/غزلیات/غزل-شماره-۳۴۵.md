---
title: >-
    غزل شمارهٔ ۳۴۵
---
# غزل شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>بگو ای یار همراز این چه شیوه‌ست</p></div>
<div class="m2"><p>دگرگون گشته‌ای باز این چه شیوه‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب ترک خوش رنگ این چه رنگست</p></div>
<div class="m2"><p>عجب ای چشم غماز این چه شیوه‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگربار این چه دامست و چه دانه‌ست</p></div>
<div class="m2"><p>که ما را کشتی از ناز این چه شیوه‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریدی پرده ما این چه پرده‌ست</p></div>
<div class="m2"><p>یکی پرده برانداز این چه شیوه‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم آن کهنه عشقی که دگربار</p></div>
<div class="m2"><p>گرفتم عشق از آغاز این چه شیوه‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان آواز جان دادن حلالست</p></div>
<div class="m2"><p>زهی آواز دمساز این چه شیوه‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسلمانان شما این شور بینید</p></div>
<div class="m2"><p>که مثلش نیست هنباز این چه شیوه‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراب و عشق و رنگم هر سه غماز</p></div>
<div class="m2"><p>یکی پنهان سه غماز این چه شیوه‌ست</p></div></div>