---
title: >-
    غزل شمارهٔ ۴۲۵
---
# غزل شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>عاشقان را جست و جو از خویش نیست</p></div>
<div class="m2"><p>در جهان جوینده جز او بیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جهان و آن جهان یک گوهر است</p></div>
<div class="m2"><p>در حقیقت کفر و دین و کیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دمت عیسی دم از دوری مزن</p></div>
<div class="m2"><p>من غلام آن که دوراندیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بگویی پس روم نی پس مرو</p></div>
<div class="m2"><p>ور بگویی پیش نی ره پیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست بگشا دامن خود را بگیر</p></div>
<div class="m2"><p>مرهم این ریش جز این ریش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جزو درویشند جمله نیک و بد</p></div>
<div class="m2"><p>هر که نبود او چنین درویش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که از جا رفت جای او دل است</p></div>
<div class="m2"><p>همچو دل اندر جهان جاییش نیست</p></div></div>