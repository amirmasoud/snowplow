---
title: >-
    غزل شمارهٔ ۲۶۵۱
---
# غزل شمارهٔ ۲۶۵۱

<div class="b" id="bn1"><div class="m1"><p>کریما تو گلی یا جمله قندی</p></div>
<div class="m2"><p>که چون بینی مرا چون گل بخندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزیزا تو به بستان آن درختی</p></div>
<div class="m2"><p>که چون دیدم تو را بیخم بکندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کم گردد ز جاهت گر بپرسی</p></div>
<div class="m2"><p>که چونی در فراقم دردمندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من آنم کز فراقت مستمندم</p></div>
<div class="m2"><p>تو آنی که خلاص مستمندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این مطبخ هزاران جان به خرج است</p></div>
<div class="m2"><p>ببین تو ای دل پرخون که چندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو حلقه بر درت گر چه مقیمم</p></div>
<div class="m2"><p>چه چاره چون تو بر بام بلندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا ای زلف چوگان حکم داری</p></div>
<div class="m2"><p>که چون گویم در این میدان فکندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپند از بهر آن باشد که سوزد</p></div>
<div class="m2"><p>دلا می‌سوز دلبر را سپندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا ای جام عشق شمس تبریز</p></div>
<div class="m2"><p>که درد کهنه را تو سودمندی</p></div></div>