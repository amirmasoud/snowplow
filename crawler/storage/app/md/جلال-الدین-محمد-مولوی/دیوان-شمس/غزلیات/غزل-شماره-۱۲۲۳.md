---
title: >-
    غزل شمارهٔ ۱۲۲۳
---
# غزل شمارهٔ ۱۲۲۳

<div class="b" id="bn1"><div class="m1"><p>قرین مه دو مریخند و آن دو چشمت ای دلکش</p></div>
<div class="m2"><p>بدان هاروت و ماروتت لجوجان را به بابل کش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلیمانا بدان خاتم که ختم جمله خوبانی</p></div>
<div class="m2"><p>همه دیوان و پریان را به قهر اندر سلاسل کش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای جن و انسان را گشادی گنج احسان را</p></div>
<div class="m2"><p>مثال نحن اعطیناک بر محروم سائل کش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جسد را کن به جان روشن حسد را بیخ و بن برکن</p></div>
<div class="m2"><p>نظر را بر مشارق زن خرد را در مسائل کش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو لب الحمد برخواند دهش نقل و می بی‌حد</p></div>
<div class="m2"><p>چو برخواند و لا الضالین تو او را در دلایل کش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوی تو جان چو بشتابد دهش شمعی که ره یابد</p></div>
<div class="m2"><p>چو خورشید تو را جوید چو ماهش در منازل کش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شراب کاس کیکاووس ده مخمور عاشق را</p></div>
<div class="m2"><p>دقیقه دانی و فن را به پیش فکر عاقل کش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به اقبال عنایاتت بکش جان را و قابل کن</p></div>
<div class="m2"><p>قبول و خلعت خود را به سوی نفس قابل کش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسیر درد و حسرت را بده پیغام لاتأسوا</p></div>
<div class="m2"><p>قتول عشق حسنت را از این مقتل به قاتل کش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر کافردلست این تن شهادت عرضه کن بر وی</p></div>
<div class="m2"><p>وگر بی‌حاصلست این جان چه باشد توش به حاصل کش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنش زنده وگر نکنی مسیحا را تو نایب کن</p></div>
<div class="m2"><p>تو وصلش ده وگر ندهی به فضلش سوی فاضل کش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمین لرزید ای خاکی چو دید آن قدس و آن پاکی</p></div>
<div class="m2"><p>اذا ما زلزلت برخوان نظر را در زلازل کش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تمامش کن هلا حالی که شاه حالی و قالی</p></div>
<div class="m2"><p>کسی که قول پیش آرد خطی بر قول و قایل کش</p></div></div>