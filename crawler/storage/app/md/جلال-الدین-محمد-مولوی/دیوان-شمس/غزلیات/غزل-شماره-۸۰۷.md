---
title: >-
    غزل شمارهٔ ۸۰۷
---
# غزل شمارهٔ ۸۰۷

<div class="b" id="bn1"><div class="m1"><p>لحظه‌ای قصه کنان قصه تبریز کنید</p></div>
<div class="m2"><p>لحظه‌ای قصه آن غمزه خون ریز کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فراق لب چون شکر او تلخ شدیم</p></div>
<div class="m2"><p>زان شکرهای خدایانه شکرریز کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هندوی شب سر زلفین ببرد ز طمع</p></div>
<div class="m2"><p>زلف او گر بفشانید عبربیز کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس زبان کز صفت آن لب او کند شود</p></div>
<div class="m2"><p>چون سنان نظر از دولت او تیز کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسا شب که ز نور مه او روز شود</p></div>
<div class="m2"><p>گر چه مه در طلبش شیوه شبخیز کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت شمشیر بود واسطه‌ها برگیرید</p></div>
<div class="m2"><p>صرف آرید نخواهیم که آمیز کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس تبریز که خورشید یکی ذره اوست</p></div>
<div class="m2"><p>ذره را شمس مگوییدش و پرهیز کنید</p></div></div>