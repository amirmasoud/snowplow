---
title: >-
    غزل شمارهٔ ۹۱۰
---
# غزل شمارهٔ ۹۱۰

<div class="b" id="bn1"><div class="m1"><p>بر آستانه اسرار آسمان نرسد</p></div>
<div class="m2"><p>به بام فقر و یقین هیچ نردبان نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گمان عارف در معرفت چو سیر کند</p></div>
<div class="m2"><p>هزار اختر و مه اندر آن گمان نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که جغدصفت شد در این جهان خراب</p></div>
<div class="m2"><p>ز بلبلان ببرید و به گلستان نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن دلی که به یک دانگ جو جوست ز حرص</p></div>
<div class="m2"><p>به دانک بسته شود جان او به کان نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علف مده حس خود را در این مکان ز بتان</p></div>
<div class="m2"><p>که حس چو گشت مکانی به لامکان نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که آهوی متأنس بماند از یاران</p></div>
<div class="m2"><p>به لاله زار و به مرعای ارغوان نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سوی عکه روی تا به مکه پیوندی</p></div>
<div class="m2"><p>برو محال مجو کت همین همان نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیاز و سیر به بینی بری و می‌بویی</p></div>
<div class="m2"><p>از آن پیاز دم ناف آهوان نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خموش اگر سر گنجینه ضمیرستت</p></div>
<div class="m2"><p>که در ضمیر هدی دل رسد زبان نرسد</p></div></div>