---
title: >-
    غزل شمارهٔ ۲۳۹۷
---
# غزل شمارهٔ ۲۳۹۷

<div class="b" id="bn1"><div class="m1"><p>ای گرد عاشقانت از رشک تخته بسته</p></div>
<div class="m2"><p>وی جمله عاشقانت از تخت و تخته رسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد مطرقه کشیده در یک قدح بکرده</p></div>
<div class="m2"><p>صد زین قدح کشیده چون عاقلان نشسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک ریسمان فکندی بردیم بر بلندی</p></div>
<div class="m2"><p>من در هوا معلق و آن ریسمان گسسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آهوان چشمت ای بس که شیر عشقت</p></div>
<div class="m2"><p>هم پوست بردریده هم استخوان شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدن به خواب در شب ماه تو را مبارک</p></div>
<div class="m2"><p>وز بامداد رویت دیدن زهی خجسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بنده کمینت گشته چو آبگینه</p></div>
<div class="m2"><p>بشکسته آبگینه صد دست و پا بخسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حسن شمس تبریز دزدیده بنگریدم</p></div>
<div class="m2"><p>زه گفتم و ز غیرت تیر از کمان بجسته</p></div></div>