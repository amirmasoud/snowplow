---
title: >-
    غزل شمارهٔ ۲۰۱۵
---
# غزل شمارهٔ ۲۰۱۵

<div class="b" id="bn1"><div class="m1"><p>فقر را در خواب دیدم دوش من</p></div>
<div class="m2"><p>گشتم از خوبی او بی‌هوش من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جمال و از کمال لطف فقر</p></div>
<div class="m2"><p>تا سحرگه بوده‌ام مدهوش من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فقر را دیدم مثال کان لعل</p></div>
<div class="m2"><p>تا ز رنگش گشتم اطلس پوش من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس شنیدم های و هوی عاشقان</p></div>
<div class="m2"><p>بس شنیدم بانگ نوشانوش من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقه‌ای دیدم همه سرمست فقر</p></div>
<div class="m2"><p>حلقه او دیدم اندر گوش من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس بدیدم نقش‌ها در نور فقر</p></div>
<div class="m2"><p>بس بدیدم نقش جان در روش من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از میان جان ما صد جوش خاست</p></div>
<div class="m2"><p>چون بدیدم بحر را در جوش من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد هزاران نعره می‌زد آسمان</p></div>
<div class="m2"><p>ای غلام همچنان چاووش من</p></div></div>