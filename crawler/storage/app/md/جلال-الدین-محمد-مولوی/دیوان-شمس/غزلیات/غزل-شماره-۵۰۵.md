---
title: >-
    غزل شمارهٔ ۵۰۵
---
# غزل شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>پیشتر آ روی تو جز نور نیست</p></div>
<div class="m2"><p>کیست که از عشق تو مخمور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی غلطم در طلب جان جان</p></div>
<div class="m2"><p>پیش میا پس به مرو دور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلعت خورشید کجا برنتافت</p></div>
<div class="m2"><p>ماه بر کیست که مشهور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده اندیشه جز اندیشه نیست</p></div>
<div class="m2"><p>ترک کن اندیشه که مستور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شکری دور ز وهم مگس</p></div>
<div class="m2"><p>وی عسلی کز تن زنبور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که خورد غصه و غم بعد از این</p></div>
<div class="m2"><p>با رخ چون ماه تو معذور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دل بی‌عشق اگر پادشاست</p></div>
<div class="m2"><p>جز کفن اطلس و جز گور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تابش اندیشه هر منکری</p></div>
<div class="m2"><p>مقت خدا بیند اگر کور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر و جوان کو خورد آب حیات</p></div>
<div class="m2"><p>مرگ بر او نافذ و میسور نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرده حق خواست شدن ماه و خور</p></div>
<div class="m2"><p>عشق شناسید که او حور نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مفخر تبریز تویی شمس دین</p></div>
<div class="m2"><p>گفتن اسرار تو دستور نیست</p></div></div>