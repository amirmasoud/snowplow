---
title: >-
    غزل شمارهٔ ۲۷۴۷
---
# غزل شمارهٔ ۲۷۴۷

<div class="b" id="bn1"><div class="m1"><p>خضری به میان سینه داری</p></div>
<div class="m2"><p>در آب حیات و سبزه زاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خضر آب حیات را نپاید</p></div>
<div class="m2"><p>گر بوی برد که تو چه داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کشتی نوح همچو روحی</p></div>
<div class="m2"><p>در گلشن روح نوبهاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر طبل وجودها بدرد</p></div>
<div class="m2"><p>از کتم عدم علم برآری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چار طبیعت ار بسوزد</p></div>
<div class="m2"><p>غم نیست تو جان هر چهاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صیاد بدایت وجودی</p></div>
<div class="m2"><p>اجزای جهان همه شکاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه بند کند گهی گشاید</p></div>
<div class="m2"><p>ای کارافزا تو بر چه کاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او سرو بلند و تو چو سایه</p></div>
<div class="m2"><p>او باد شمال و تو غباری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چشم تو ریخت کحل پندار</p></div>
<div class="m2"><p>می‌پنداری به اختیاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این چرخ به اختیار خود نیست</p></div>
<div class="m2"><p>آخر تو کیی بدین نزاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نیست تو خویش هست کردی</p></div>
<div class="m2"><p>وین گردن خود تو می‌فشاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین ترس تو حجت است بر تو</p></div>
<div class="m2"><p>کز غیر تو است ترسگاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خویش دل کسی نترسد</p></div>
<div class="m2"><p>از خویش کسی نجست یاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس خوف و رجای تو گواهند</p></div>
<div class="m2"><p>بر ملکت شاه و کامکاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز خوف و رجا چو برتر آیی</p></div>
<div class="m2"><p>ایمن چو صفات کردگاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشتی ترسد ز بحر نی بحر</p></div>
<div class="m2"><p>تو کشتی بحر بی‌کناری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کشتی توی تو چو بشکست</p></div>
<div class="m2"><p>خاموش کن از سخن گزاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کشتی شکسته را کی راند</p></div>
<div class="m2"><p>جز آب به موج بی‌قراری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کشتیبان شکستگان است</p></div>
<div class="m2"><p>آن بحر کرم به بردباری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خامش که زبان عقل مهر است</p></div>
<div class="m2"><p>بنشین بر جا که گشت تاری</p></div></div>