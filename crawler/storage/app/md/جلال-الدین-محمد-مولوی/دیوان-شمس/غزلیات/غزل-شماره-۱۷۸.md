---
title: >-
    غزل شمارهٔ ۱۷۸
---
# غزل شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>می‌شدی غافل ز اسرار قضا</p></div>
<div class="m2"><p>زخم خوردی از سلحدار قضا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه کار افتاد آخر ناگهان</p></div>
<div class="m2"><p>این چنین باشد چنین کار قضا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ گل دیدی که خندد در جهان</p></div>
<div class="m2"><p>کو نشد گرینده از خار قضا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ بختی در جهان رونق گرفت</p></div>
<div class="m2"><p>کو نشد محبوس و بیمار قضا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ کس دزدیده روی عیش دید</p></div>
<div class="m2"><p>کو نشد آونگ بر دار قضا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کس را مکر و فن سودی نکرد</p></div>
<div class="m2"><p>پیش بازی‌های مکار قضا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این قضا را دوستان خدمت کنند</p></div>
<div class="m2"><p>جان کنند از صدق ایثار قضا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه صورت مرد جان باقی بماند</p></div>
<div class="m2"><p>در عنایت‌های بسیار قضا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوز بشکست و بمانده مغز روح</p></div>
<div class="m2"><p>رفت در حلوا ز انبار قضا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنک سوی نار شد بی‌مغز بود</p></div>
<div class="m2"><p>مغز او پوسید از انکار قضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنک سوی یار شد مسعود بود</p></div>
<div class="m2"><p>مغز جان بگزید و شد یار قضا</p></div></div>