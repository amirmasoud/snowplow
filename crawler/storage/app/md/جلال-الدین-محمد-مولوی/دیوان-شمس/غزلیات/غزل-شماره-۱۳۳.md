---
title: >-
    غزل شمارهٔ ۱۳۳
---
# غزل شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>غمزه عشقت بدان آرد یکی محتاج را</p></div>
<div class="m2"><p>کو به یک جو برنسنجد هیچ صاحب تاج را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اطلس و دیباج بافد عاشق از خون جگر</p></div>
<div class="m2"><p>تا کشد در پای معشوق اطلس و دیباج را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل عاشق کجا یابی غم هر دو جهان</p></div>
<div class="m2"><p>پیش مکی قدر کی باشد امیر حاج را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق معراجیست سوی بام سلطان جمال</p></div>
<div class="m2"><p>از رخ عاشق فروخوان قصه معراج را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی ز آویختن دارد چو میوه از درخت</p></div>
<div class="m2"><p>زان همی‌بینی درآویزان دو صد حلاج را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نه علم حال فوق قال بودی کی بدی</p></div>
<div class="m2"><p>بنده احبار بخارا خواجه نساج را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلمه ای‌هان تا نگیری ریش کوسه در نبرد</p></div>
<div class="m2"><p>هندوی ترکی میاموز آن ملک تمغاج را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو فرزین کژروست و رخ سیه بر نطع شاه</p></div>
<div class="m2"><p>آنک تلقین می‌کند شطرنج مر لجلاج را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که میرخوان به غراقان روحانی شدی</p></div>
<div class="m2"><p>بر چنین خوانی چه چینی خرده تتماج را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشق آشفته از آن گوید که اندر شهر دل</p></div>
<div class="m2"><p>عشق دایم می‌کند این غارت و تاراج را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس کن ایرا بلبل عشقش نواها می‌زند</p></div>
<div class="m2"><p>پیش بلبل چه محل باشد دم دراج را</p></div></div>