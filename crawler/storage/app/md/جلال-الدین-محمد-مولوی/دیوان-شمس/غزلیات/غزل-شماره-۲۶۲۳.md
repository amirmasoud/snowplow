---
title: >-
    غزل شمارهٔ ۲۶۲۳
---
# غزل شمارهٔ ۲۶۲۳

<div class="b" id="bn1"><div class="m1"><p>ما گوش شماییم شما تن زده تا کی</p></div>
<div class="m2"><p>ما مست و خراباتی و بیخود شده تا کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما سوخته حالان و شما سیر و ملولان</p></div>
<div class="m2"><p>آخر بنگویید که این قاعده تا کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل زیر و زبر گشت مها چند زنی طشت</p></div>
<div class="m2"><p>مجلس همه شوریده بتا عربده تا کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی عقل درافتاد و به کف کرده عصایی</p></div>
<div class="m2"><p>در حلقه رندان شده کاین مفسده تا کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ساقی ما ریخت بر او جام شرابی</p></div>
<div class="m2"><p>بشکست در صومعه کاین معبده تا کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تسبیح بینداخت و ز سالوس بپرداخت</p></div>
<div class="m2"><p>کاین نوبت شادی است غم بیهده تا کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن‌ها که خموشند به مستی مزه نوشند</p></div>
<div class="m2"><p>ای در سخن بی‌مزه گرم آمده تا کی</p></div></div>