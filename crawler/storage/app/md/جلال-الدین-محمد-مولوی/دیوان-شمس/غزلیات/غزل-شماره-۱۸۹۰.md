---
title: >-
    غزل شمارهٔ ۱۸۹۰
---
# غزل شمارهٔ ۱۸۹۰

<div class="b" id="bn1"><div class="m1"><p>با روی تو کفر است به معنی نگریدن</p></div>
<div class="m2"><p>یا باغ صفا را به یکی تره خریدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پر تو مرغان ضمیر دل ما را</p></div>
<div class="m2"><p>در جنت فردوس حرام است پریدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر فلک عشق هر آن مه که بتابد</p></div>
<div class="m2"><p>آن ابر تو است ای مه و فرض است دریدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشتی که چراگاه شکاران تو باشد</p></div>
<div class="m2"><p>شیران بنیارند در آن دست چریدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر عشق که از آتش حسن تو نخیزد</p></div>
<div class="m2"><p>آن عشق حرام است و صلای فسریدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باطن من جان من از غیر تو ببرید</p></div>
<div class="m2"><p>محسوس شنیدم من آواز بریدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خواب شود غافل از این دولت بیدار</p></div>
<div class="m2"><p>از پوست چه شیره بودت در فشریدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنجور شقاوت چو بیفتاد به یاسین</p></div>
<div class="m2"><p>لاحول بود چاره و انگشت گزیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز عشق خداوندی شمس الحق تبریز</p></div>
<div class="m2"><p>آن موی بصر باشد باید ستریدن</p></div></div>