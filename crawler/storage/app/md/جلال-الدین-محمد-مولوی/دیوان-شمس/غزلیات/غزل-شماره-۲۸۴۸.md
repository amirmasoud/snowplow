---
title: >-
    غزل شمارهٔ ۲۸۴۸
---
# غزل شمارهٔ ۲۸۴۸

<div class="b" id="bn1"><div class="m1"><p>سحر است خیز ساقی بکن آنچ خوی داری</p></div>
<div class="m2"><p>سر خنب برگشای و برسان شراب ناری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شود اگر ز عیسی دو سه مرده زنده گردد</p></div>
<div class="m2"><p>خوش و شیرگیر گردد ز کفت دو سه خماری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدح چو آفتابت چو به دور اندرآید</p></div>
<div class="m2"><p>برهد جهان تیره ز شب و ز شب شماری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شراب چون عقیقت شکفد گل حقیقت</p></div>
<div class="m2"><p>که حیات مرغ زاری و بهار مرغزاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدهیم جان شیرین به شراب خسروانی</p></div>
<div class="m2"><p>چو سر خمار ما را به کف کرم بخاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ز فکرت دقیقه خللی است در شقیقه</p></div>
<div class="m2"><p>تو روان کن آب درمان بگشا ره مجاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه آتشی تو مطلق بر ما شد این محقق</p></div>
<div class="m2"><p>که هزار دیگ سر را به تفی به جوش آری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه مطربان خروشان همه از تو گشته جوشان</p></div>
<div class="m2"><p>همه رخت خود فروشان خوششان همی‌فشاری</p></div></div>