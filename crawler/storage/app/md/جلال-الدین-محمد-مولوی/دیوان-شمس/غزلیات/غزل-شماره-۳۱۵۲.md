---
title: >-
    غزل شمارهٔ ۳۱۵۲
---
# غزل شمارهٔ ۳۱۵۲

<div class="b" id="bn1"><div class="m1"><p>گر چه تو نیم شب رسیدستی</p></div>
<div class="m2"><p>صبح عشاق را کلیدستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناپدیدی چو جان در این عالم</p></div>
<div class="m2"><p>در جهان دلم پدیدستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب جان تو را شود قربان</p></div>
<div class="m2"><p>ز آن که تو بامداد عیدستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آدمی چون پری رمیدم من</p></div>
<div class="m2"><p>تا ز من ای پری رمیدستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مزیدم چو دولت منصور</p></div>
<div class="m2"><p>چون مرا تو ابایزیدستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بسا نازکان و خامان را</p></div>
<div class="m2"><p>چون من سوخته پزیدستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس تبریز سرمه دیگر</p></div>
<div class="m2"><p>در دو دیده خرد کشیدستی</p></div></div>