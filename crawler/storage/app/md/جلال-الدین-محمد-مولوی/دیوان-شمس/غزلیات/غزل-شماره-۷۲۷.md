---
title: >-
    غزل شمارهٔ ۷۲۷
---
# غزل شمارهٔ ۷۲۷

<div class="b" id="bn1"><div class="m1"><p>از دلبر ما نشان کی دارد</p></div>
<div class="m2"><p>در خانه مهی نهان کی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی دیده جمال او کی بیند</p></div>
<div class="m2"><p>بیرون ز جهان جهان کی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن تیر که جان شکار آنست</p></div>
<div class="m2"><p>بنمای که آن کمان کی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر طرفی یکی نگاریست</p></div>
<div class="m2"><p>صوفی تو نگر که آن کی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این صورت خلق جمله نقش اند</p></div>
<div class="m2"><p>هم جان داند که جان کی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جمله گدا و خوشه چین اند</p></div>
<div class="m2"><p>آن دست گهرفشان کی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قلاب شدند جمله عالم</p></div>
<div class="m2"><p>آخر خبری ز کان کی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شادست زمان به شمس تبریز</p></div>
<div class="m2"><p>آخر بنگر زمان کی دارد</p></div></div>