---
title: >-
    غزل شمارهٔ ۳۲۲۵
---
# غزل شمارهٔ ۳۲۲۵

<div class="b" id="bn1"><div class="m1"><p>یا ملک‌المحشر، ترحم لا ترتشی</p></div>
<div class="m2"><p>کل سقیط ردی ترحمه تنعش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تحبس ارواحنا فی صورت صورت</p></div>
<div class="m2"><p>فی ورق مدرک جل عن المنقش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نورک شعشاعه یخرق حجب الدجی</p></div>
<div class="m2"><p>تمنعها غیرة عن بصر الاعمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ضء فضاء الفلا عن درک ادراکه</p></div>
<div class="m2"><p>تدرجه راقة فی نظر الا خفش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قارب معراجنا، فارق الی‌المرتقی</p></div>
<div class="m2"><p>حان رحیل السری فانا عن المفرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وارکب خیل السخا، فهو حسان النهی</p></div>
<div class="m2"><p>وادرس لوح الوفا وافهم ما یرقش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فاسرق درا اذا کنت اخی سارقا</p></div>
<div class="m2"><p>واشرب من کاسنا معتجلا تنتشی</p></div></div>