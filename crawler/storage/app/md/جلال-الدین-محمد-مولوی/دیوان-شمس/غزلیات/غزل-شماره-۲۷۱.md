---
title: >-
    غزل شمارهٔ ۲۷۱
---
# غزل شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>تعالوا کلنا ذا الیوم سکری</p></div>
<div class="m2"><p>باقداح تخامرنا و تتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سقانا ربنا کاسا دهاقا</p></div>
<div class="m2"><p>فشکرا ثم شکرا ثم شکرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تعالوا ان هذا یوم عید</p></div>
<div class="m2"><p>تجلی فیه ما ترجون جهرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوارق زرننا و اللیل ساجی</p></div>
<div class="m2"><p>فما ابقین فی التضییق صدرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کف هر یکی دریای بخشش</p></div>
<div class="m2"><p>نثرن جواهرا جما و وفرا</p></div></div>