---
title: >-
    غزل شمارهٔ ۲۵۶۹
---
# غزل شمارهٔ ۲۵۶۹

<div class="b" id="bn1"><div class="m1"><p>آن زلف مسلسل را گر دام کنی حالی</p></div>
<div class="m2"><p>در عشق جهانی را بدنام کنی حالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌جوش ز سر گیرد خمخانه به رقص آید</p></div>
<div class="m2"><p>گر از شکرقندت در جام کنی حالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشم چو بادامت در مجلس یک رنگی</p></div>
<div class="m2"><p>هر نقل که پیش آید بادام کنی حالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاشا ز عطای تو کان نسیه بود ای جان</p></div>
<div class="m2"><p>گر تشنه بود صادق انعام کنی حالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ماه فلک پیما از منزل ما تا تو</p></div>
<div class="m2"><p>صدساله ره ار باشد یک گام کنی حالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لطف تو از عقرب صد شیر بجوشیده</p></div>
<div class="m2"><p>و آن کره گردون را هم رام کنی حالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر بام فلک صد در بگشاید و بنماید</p></div>
<div class="m2"><p>گر حارس بامت را بر بام کنی حالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر خام شود پخته هم خوانده شود تخته</p></div>
<div class="m2"><p>گر صبح رخت جلوه در شام کنی حالی</p></div></div>