---
title: >-
    غزل شمارهٔ ۳۰۲۸
---
# غزل شمارهٔ ۳۰۲۸

<div class="b" id="bn1"><div class="m1"><p>ای صنم گلزاری چند مرا آزاری</p></div>
<div class="m2"><p>من چو کمین فلاحم تو دهیم سالاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند مرا بفریبی هر چه کنی می‌زیبی</p></div>
<div class="m2"><p>چند به دل آموزی مغلطه و طراری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که از آن طراری باز بر او برشکنی</p></div>
<div class="m2"><p>افتد و سودش نکند در دغلی هشیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساده دلی ساز مرا سوی عدم تاز مرا</p></div>
<div class="m2"><p>تار هم از لطف فنا زین فرح و زین زاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کی بگرید به یقین دیده بود گنج دفین</p></div>
<div class="m2"><p>هر کی بخندد بود او در حجب ستاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که ز دور آمده‌ام با شر و شور آمده‌ام</p></div>
<div class="m2"><p>بازبنگشاده‌ام این دان خبر سرباری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار که بگشاده شود از پی سرمایه بود</p></div>
<div class="m2"><p>مایه نداری تو ولی خایه خود می‌خاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس کن و بسیار مگو روی بدو آر بدو</p></div>
<div class="m2"><p>مشتری گفت تو او سیر نه از بسیاری</p></div></div>