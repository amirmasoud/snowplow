---
title: >-
    غزل شمارهٔ ۱۴۶۸
---
# غزل شمارهٔ ۱۴۶۸

<div class="b" id="bn1"><div class="m1"><p>امروز خوشم با تو جان تو و فردا هم</p></div>
<div class="m2"><p>از تو شکرافشانم این جا هم و آن جا هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل باده تو خورده وز خانه سفر کرده</p></div>
<div class="m2"><p>ما بی‌دل و دل با تو با ما هم و بی‌ما هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل که روانی تو آن سوی که دانی تو</p></div>
<div class="m2"><p>خدمت برسان از ما آن جا و موصی هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما منتظر وقت و دل ناظر تو دایم</p></div>
<div class="m2"><p>در حالت آرامش در شورش و غوغا هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از باده و باد تو چون موج شده این دل</p></div>
<div class="m2"><p>در مستی و پستی خوش در رفعت و بالا هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابر خوش لطف تو با جان و روان ما</p></div>
<div class="m2"><p>در خاک اثر کرده در صخره و خارا هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تو پس از این عالم بی‌نقش بنی آدم</p></div>
<div class="m2"><p>خوش خلوت جان باشد آمیزش جان‌ها هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان غمزه مست تو زان جادو و جادوخو</p></div>
<div class="m2"><p>خیره شده هر دیده نادان هم و دانا هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من ننگ نمی‌دارم مجنونم و می دانی</p></div>
<div class="m2"><p>هم عرق جنون دارم از مایه و سودا هم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آتش و آب او ای جسته نشان بنگر</p></div>
<div class="m2"><p>در آب دو چشم ما در زردی سیما هم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عالم آب و گل در پرده جان و دل</p></div>
<div class="m2"><p>هم ایمنی از عشقت وین فتنه و غوغا هم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان طره روحانی زان سلسله جانی</p></div>
<div class="m2"><p>زنار تو بربسته هم مؤمن و ترسا هم</p></div></div>