---
title: >-
    غزل شمارهٔ ۲۴۰۶
---
# غزل شمارهٔ ۲۴۰۶

<div class="b" id="bn1"><div class="m1"><p>ایا دلی چو صبا ذوق صبح‌ها دیده</p></div>
<div class="m2"><p>ز دیده مست شدی یا ز ذوق نادیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی به بحر تحیر گهی به دامن کوه</p></div>
<div class="m2"><p>کمر ببسته و در کوه کهربا دیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورای دیده و دل صد دریچه بگشاده</p></div>
<div class="m2"><p>برون ز چرخ و زمین رفته صد سما دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو جوششی و بخاری فتاد در دریا</p></div>
<div class="m2"><p>ز لذت نظرش رست در قفا دیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو موج موج درآمیخت چشم با دریا</p></div>
<div class="m2"><p>عجب عجب که همه بحر گشت یا دیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیش دیده دو عالم چو دانه پیش خروس</p></div>
<div class="m2"><p>چنین بود نظر پاک کبریادیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه طالب است و نه مطلوب آن که در توحید</p></div>
<div class="m2"><p>صفات طالب و مطلوب را جدا دیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اله را کی شناسد کسی که رست ز لا</p></div>
<div class="m2"><p>ز لا کی رست بگو عاشق بلادیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رموز لیس و فی جبتی بدانسته</p></div>
<div class="m2"><p>هزار بار من این جبه را قبا دیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهان گشاد ضمیر و صلاح دین را گفت</p></div>
<div class="m2"><p>تویی حیات من ای دیده خدادیده</p></div></div>