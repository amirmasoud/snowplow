---
title: >-
    غزل شمارهٔ ۹۱۹
---
# غزل شمارهٔ ۹۱۹

<div class="b" id="bn1"><div class="m1"><p>ببرد خواب مرا عشق و عشق خواب برد</p></div>
<div class="m2"><p>که عشق جان و خرد را به نیم جو نخرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که عشق شیر سیاه‌ست تشنه و خون خوار</p></div>
<div class="m2"><p>به غیر خون دل عاشقان همی‌نچرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مهر بر تو بچفسد به سوی دام آرد</p></div>
<div class="m2"><p>چو درفتادی از آن پس ز دور می‌نگرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امیر دست درازست و شحنه بی‌باک</p></div>
<div class="m2"><p>شکنجه می‌کند و بی‌گناه می‌فشرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آنک در کفش آید چو ابر می‌گرید</p></div>
<div class="m2"><p>هر آنک دور شد از وی چو برف می‌فسرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار جام به هر لحظه خرد درشکند</p></div>
<div class="m2"><p>هزار جامه به یک دم بدوزد و بدرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار چشم بگریاند و فروخندد</p></div>
<div class="m2"><p>هزار کس بکشد زار زار و یک شمرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کوه قاف اگر چه که خوش پرد سیمرغ</p></div>
<div class="m2"><p>چو دام عشق ببیند فتد دگر نپرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بند او نرهد کس به شید یا به جنون</p></div>
<div class="m2"><p>ز دام او نرهد هیچ عاقلی به خرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مخبط‌ست سخن‌های من از او گر نی</p></div>
<div class="m2"><p>نمودمی به تو آن راه‌ها که می‌سپرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمودمی به تو کو شیر را چه سان گیرد</p></div>
<div class="m2"><p>نمودمی که چگونه شکار را شکرد</p></div></div>