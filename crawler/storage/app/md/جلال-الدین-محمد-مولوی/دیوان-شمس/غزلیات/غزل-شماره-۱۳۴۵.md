---
title: >-
    غزل شمارهٔ ۱۳۴۵
---
# غزل شمارهٔ ۱۳۴۵

<div class="b" id="bn1"><div class="m1"><p>تو مرا می بده و مست بخوابان و بهل</p></div>
<div class="m2"><p>چون رسد نوبت خدمت نشوم هیچ خجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گه خدمت شه آید من می‌دانم</p></div>
<div class="m2"><p>گر ز آب و گلم ای دوست نیم پای به گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نمازش چو خروسم سبک و وقت شناس</p></div>
<div class="m2"><p>نه چو زاغم که بود نعره او وصل گسل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ز راز خوش او یک دو سخن خواهم گفت</p></div>
<div class="m2"><p>دل من دار دمی ای دل تو بی‌غش و غل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لذت عشق بتان را ز زحیران مطلب</p></div>
<div class="m2"><p>صبح کاذب بود این قافله را سخت مضل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بحل کردم ای جان که بریزی خونم</p></div>
<div class="m2"><p>ور نریزی تو مرا مظلمه داری نه بحل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس خمش کردم و با چشم و به ابرو گفتم</p></div>
<div class="m2"><p>سخنانی که نیاید به زبان و به سجل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه آن فهم نکردی تو ولی گرم شدی</p></div>
<div class="m2"><p>هله گرمی تو بیفزا چه کنی جهد مقل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سردی از سایه بود شمس بود روشن و گرم</p></div>
<div class="m2"><p>فانی طلعت آن شمس شو ای سرد چو ظل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا درآمد بت خوبم ز در صومعه مست</p></div>
<div class="m2"><p>چند قندیل شکستم پی آن شمع چگل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس تبریز مگر ماه ندانست حقت</p></div>
<div class="m2"><p>که گرفتار شدست او به چنین علت سل</p></div></div>