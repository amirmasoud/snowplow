---
title: >-
    غزل شمارهٔ ۸۳۳
---
# غزل شمارهٔ ۸۳۳

<div class="b" id="bn1"><div class="m1"><p>مرگ ما هست عروسی ابد</p></div>
<div class="m2"><p>سر آن چیست هو الله احد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمس تفریق شد از روزنه‌ها</p></div>
<div class="m2"><p>بسته شد روزنه‌ها رفت عدد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن عددها که در انگور بود</p></div>
<div class="m2"><p>نیست در شیره کز انگور چکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کی زنده‌ست به نورالله</p></div>
<div class="m2"><p>مرگ این روح مر او راست مدد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بد مگو نیک مگو ایشان را</p></div>
<div class="m2"><p>که گذشتند ز نیکو و ز بد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده در حق نه و نادیده مگو</p></div>
<div class="m2"><p>تا که در دیده دگر دیده نهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده دیده بود آن دیده</p></div>
<div class="m2"><p>هیچ غیبی و سری زو نجهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظرش چونک به نورالله است</p></div>
<div class="m2"><p>بر چنان نور چه پوشیده شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نورها گر چه همه نور حقند</p></div>
<div class="m2"><p>تو مخوان آن همه را نور صمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور باقیست که آن نور خدا است</p></div>
<div class="m2"><p>نور فانی صفت جسم و جسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نور ناریست در این دیده خلق</p></div>
<div class="m2"><p>مگر آن را که حقش سرمه کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نار او نور شد از بهر خلیل</p></div>
<div class="m2"><p>چشم خر شد به صفت چشم خرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای خدایی که عطایت دیدست</p></div>
<div class="m2"><p>مرغ دیده به هوای تو پرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قطب این که فلک افلاکست</p></div>
<div class="m2"><p>در پی جستن تو بست رصد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا ز دیدار تو دید آر او را</p></div>
<div class="m2"><p>یا بدین عیب مکن او را رد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دیده تر دار تو جان را هر دم</p></div>
<div class="m2"><p>نگهش دار ز دام قد و خد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیده در خواب ز تو بیداری</p></div>
<div class="m2"><p>این چنین خواب کمالست و رشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لیک در خواب نیابد تعبیر</p></div>
<div class="m2"><p>تو ز خوابش به جهان رغم حسد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور نه می‌کوشد و بر می‌جوشد</p></div>
<div class="m2"><p>ز آتش عشق احد تا به لحد</p></div></div>