---
title: >-
    غزل شمارهٔ ۲۶۵۸
---
# غزل شمارهٔ ۲۶۵۸

<div class="b" id="bn1"><div class="m1"><p>اگر درد مرا درمان فرستی</p></div>
<div class="m2"><p>وگر کشت مرا باران فرستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر آن میر خوبان را به حیلت</p></div>
<div class="m2"><p>ز خانه جانب میدان فرستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر ساقی جان عاشقان را</p></div>
<div class="m2"><p>میان حلقه مستان فرستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه ذرات عالم زنده گردد</p></div>
<div class="m2"><p>چو جانم را بر جانان فرستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر لب را به رحمت برگشایی</p></div>
<div class="m2"><p>مفرح سوی بیماران فرستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دربان گفته‌ای مگذار ما را</p></div>
<div class="m2"><p>مرا هر دم بر دربان فرستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم کشتی در این بحر و نشاید</p></div>
<div class="m2"><p>که بر من باد سرگردان فرستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی‌خواهم که کشتیبان تو باشی</p></div>
<div class="m2"><p>اگر بر عاشقان طوفان فرستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا تا کی مها چون ارمغانی</p></div>
<div class="m2"><p>به پیش این و پیش آن فرستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل بریان عاشق باده خواهد</p></div>
<div class="m2"><p>تو او را غصه و گریان فرستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی رطلی گران برریز بر وی</p></div>
<div class="m2"><p>از آن رطلی که بر مردان فرستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل و جان هر دو را در نامه پیچم</p></div>
<div class="m2"><p>اگر تو نامه پنهان فرستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو چون خورشید از مشرق برآیی</p></div>
<div class="m2"><p>جهان بی‌خبر را جان فرستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه باشد ای صبا گر این غزل را</p></div>
<div class="m2"><p>به خلوتخانه سلطان فرستی</p></div></div>