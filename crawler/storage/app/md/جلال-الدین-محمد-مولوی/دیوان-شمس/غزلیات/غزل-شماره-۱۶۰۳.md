---
title: >-
    غزل شمارهٔ ۱۶۰۳
---
# غزل شمارهٔ ۱۶۰۳

<div class="b" id="bn1"><div class="m1"><p>چونک در باغت به زیر سایه طوبیستم</p></div>
<div class="m2"><p>گرم در کار آمدم موقوف مطرب نیستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو سایه بر طوافم گرد نور آفتاب</p></div>
<div class="m2"><p>گه سجودش می کنم گاهی به سر می ایستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه درازم گاه کوته همچو سایه پیش نور</p></div>
<div class="m2"><p>جمله فرعونم چو هستم چون نیم موسیستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من میان اصبعین حکم حقم چون قلم</p></div>
<div class="m2"><p>در کف موسی عصا گاهی و گه افعیستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق را اندیشه نبود زانک اندیشه عصاست</p></div>
<div class="m2"><p>عقل را باشد عصا یعنی که من اعمیستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روح موقوف اشارت می بنالد هر دمی</p></div>
<div class="m2"><p>بر سر ره منتظر موقوف یک آریستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون از این جا نیستم این جا غریبم من غریب</p></div>
<div class="m2"><p>چون در این جا بی‌قرارم آخر از جاییستم</p></div></div>