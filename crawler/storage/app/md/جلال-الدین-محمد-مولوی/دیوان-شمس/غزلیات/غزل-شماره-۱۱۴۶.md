---
title: >-
    غزل شمارهٔ ۱۱۴۶
---
# غزل شمارهٔ ۱۱۴۶

<div class="b" id="bn1"><div class="m1"><p>مرا بگاه ده ای ساقی کریم عقار</p></div>
<div class="m2"><p>که دوش هیچ نخفتم ز تشنگی و خمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبم که نام تو گوید به باده‌اش خوش کن</p></div>
<div class="m2"><p>سرم خمار تو دارد به مستیش تو بخار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بریز باده بر اجسامم و بر اعراضم</p></div>
<div class="m2"><p>چنانک هیچ نماند ز من رگی هشیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر خراب شوم من بود رگی باقی</p></div>
<div class="m2"><p>چو جغد هل که بگردد در این خراب دیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو لاله زار کن این دشت را به باده لعل</p></div>
<div class="m2"><p>روا مدار که موقوف داریم به بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز توست این شجره و خرقه‌اش تو دادستی</p></div>
<div class="m2"><p>که از شراب تو اشکوفه کرده‌اند اشجار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا چو مست کنی زین شجر برآرم سر</p></div>
<div class="m2"><p>به خنده دل بنمایم به خلق همچو انار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چو وقف خرابات خویش کردستی</p></div>
<div class="m2"><p>توام خراب کنی هم تو باشیم معمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیار رطل گران تا خمش کنم پی آن</p></div>
<div class="m2"><p>نه لایقست که باشد غلام تو مکثار</p></div></div>