---
title: >-
    غزل شمارهٔ ۹۵۱
---
# غزل شمارهٔ ۹۵۱

<div class="b" id="bn1"><div class="m1"><p>هر آن نوی که رسد سوی تو قدید شود</p></div>
<div class="m2"><p>چو آب پاک که در تن رود پلید شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شیر دیو مزیدی مزید تو هم از اوست</p></div>
<div class="m2"><p>که بایزید از این شیردان یزید شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرید خواند خداوند دیو وسوسه را</p></div>
<div class="m2"><p>که هر که خورد دم او چو او مرید شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مشرقست و چو مغرب مثال این دو جهان</p></div>
<div class="m2"><p>بدین قریب شود مرد زان بعید شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن دلی که بشورید و قی شدش آن شیر</p></div>
<div class="m2"><p>ز شورش و قی آن شیر بوسعید شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آنک صدر رها کرد و خاک این در شد</p></div>
<div class="m2"><p>هزار قفل گران را دلش کلید شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترش ترش تو به خسرو مگو که شیرین کو</p></div>
<div class="m2"><p>پدید آید چون خواجه ناپدید شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو غوره رست ز خامی خویش شد شیرین</p></div>
<div class="m2"><p>چو ماه روزه به پایان رسید عید شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خموش آینه منمای در ولایت زنگ</p></div>
<div class="m2"><p>نما به قیصر رومش که تا مرید شود</p></div></div>