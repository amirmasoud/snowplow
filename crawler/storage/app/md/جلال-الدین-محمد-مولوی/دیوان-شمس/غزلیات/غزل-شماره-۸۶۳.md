---
title: >-
    غزل شمارهٔ ۸۶۳
---
# غزل شمارهٔ ۸۶۳

<div class="b" id="bn1"><div class="m1"><p>آتش پریر گفت نهانی به گوش دود</p></div>
<div class="m2"><p>کز من نمی‌شکیبد و با من خوش است عود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدر من او شناسد و شکر من او کند</p></div>
<div class="m2"><p>کاندر فنای خویش بدیدست عود سود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر تا به پای عود گره بود بند بند</p></div>
<div class="m2"><p>اندر گشایش عدم آن عقدها گشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای یار شعله خوار من اهلا و مرحبا</p></div>
<div class="m2"><p>ای فانی و شهید من و مفخر شهود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنگر که آسمان و زمین رهن هستی اند</p></div>
<div class="m2"><p>اندر عدم گریز از این کور و زان کبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جان که می‌گریزد از فقر و نیستی</p></div>
<div class="m2"><p>نحسی بود گریزان از دولت و سعود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی محو کس ز لوح عدم مستفید نیست</p></div>
<div class="m2"><p>صلحی فکن میان من و محو ای ودود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن خاک تیره تا نشد از خویشتن فنا</p></div>
<div class="m2"><p>نی در فزایش آمد و نی رست از رکود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نطفه نطفه بود و نشد محو از منی</p></div>
<div class="m2"><p>نی قد سرو یافت نه زیبایی خدود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در معده چون بسوزد آن نان و نان خورش</p></div>
<div class="m2"><p>آن گاه عقل و جان شود و حسرت حسود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سنگ سیاه تا نشد از خویشتن فنا</p></div>
<div class="m2"><p>نی زر و نقره گشت و نی ره یافت در نقود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواریست و بندگیست پس آنگه شهنشهیست</p></div>
<div class="m2"><p>اندر نماز قامه بود آنگهی قعود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمری بیازمودی هستی خویش را</p></div>
<div class="m2"><p>یک بار نیستی را هم باید آزمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طاق و طرنب فقر و فنا هم گزاف نیست</p></div>
<div class="m2"><p>هر جا که دود آمد بی‌آتشی نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر نیست عشق را سر ما و هوای ما</p></div>
<div class="m2"><p>چون از گزافه او دل و دستار ما ربود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عشق آمدست و گوش کشانمان همی‌کشد</p></div>
<div class="m2"><p>هر صبح سوی مکتب یوفون بالعهود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از چشم مؤمن آب ندم می‌کند روان</p></div>
<div class="m2"><p>تا سینه را بشوید از کینه و جحود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو خفته‌ای و آب خضر بر تو می‌زند</p></div>
<div class="m2"><p>کز خواب برجه و بستان ساغر خلود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باقیش عشق گوید با تو نهان ز من</p></div>
<div class="m2"><p>ز اصحاب کهف باش هم ایقاظ و رقود</p></div></div>