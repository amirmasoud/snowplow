---
title: >-
    غزل شمارهٔ ۲۶۷۷
---
# غزل شمارهٔ ۲۶۷۷

<div class="b" id="bn1"><div class="m1"><p>سلام علیک ای مقصود هستی</p></div>
<div class="m2"><p>هم از آغاز روز امروز مستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی می واجب آید باده خوردن</p></div>
<div class="m2"><p>تویی بت واجب آید بت پرستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دوران تو منسوخ است شیشه</p></div>
<div class="m2"><p>بگردان آن سبوهای دودستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا بشنو حدیث پوست کنده</p></div>
<div class="m2"><p>همه مغزم چو در مغزم نشستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلا ای یوسف خوبان به مصر آ</p></div>
<div class="m2"><p>ز قعر چه به حبل الله رستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگیر ای چرخ پیر چنبری پشت</p></div>
<div class="m2"><p>رسن را سخت کز چنبر بجستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم لولی و سرنا خوش نوازم</p></div>
<div class="m2"><p>بده شکر نیم را چون شکستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دو بوسه مخا از خشم لب را</p></div>
<div class="m2"><p>تو ده نان چون دکان‌ها را ببستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلی گو نی مگو ای صورت عشق</p></div>
<div class="m2"><p>که سلطان بلی شاه الستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلی تو برآردمان به بالا</p></div>
<div class="m2"><p>بلی ما فرود آرد به پستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمش کن عشق خود مجنون خویش است</p></div>
<div class="m2"><p>نه لیلی گنجد و نی فاطمستی</p></div></div>