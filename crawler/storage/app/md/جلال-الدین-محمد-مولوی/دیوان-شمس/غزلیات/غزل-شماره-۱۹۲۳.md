---
title: >-
    غزل شمارهٔ ۱۹۲۳
---
# غزل شمارهٔ ۱۹۲۳

<div class="b" id="bn1"><div class="m1"><p>ای روی مه تو شاد خندان</p></div>
<div class="m2"><p>آن روی همیشه باد خندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ماه ز هیچ کس نزاده‌ست</p></div>
<div class="m2"><p>ور زانک بزاد زاد خندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای یوسف یوسفان نشستی</p></div>
<div class="m2"><p>در مسند عدل و داد خندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن در که همیشه بسته بودی</p></div>
<div class="m2"><p>وا شد ز تو با گشاد خندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آب حیات چون رسیدی</p></div>
<div class="m2"><p>شد آتش و خاک و باد خندان</p></div></div>