---
title: >-
    غزل شمارهٔ ۳۰۹۲
---
# غزل شمارهٔ ۳۰۹۲

<div class="b" id="bn1"><div class="m1"><p>هزار جان مقدس فدای سلطانی</p></div>
<div class="m2"><p>که دست کفر برو برنبست پالانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببرد او به سلامت میان چندین باد</p></div>
<div class="m2"><p>به ظلمت لحد خود چراغ ایمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگین عشق کاسیر ویند دیو و پری</p></div>
<div class="m2"><p>ز دیو تن کی ستاند مگر سلیمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی برشکافت زره بر تن چنین کافر</p></div>
<div class="m2"><p>به غیر شیر حق و ذوالفقار برانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای قاعده نی غم به پیش تابوتش</p></div>
<div class="m2"><p>دریده صورت خیرات او گریبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنک کس که دود پیش و پیشکش ببرد</p></div>
<div class="m2"><p>چو بوهریره در انبان عقیق و مرجانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خانه جانب گور و ز گور جانب دوست</p></div>
<div class="m2"><p>لفافه را طربی و جنازه را جانی</p></div></div>