---
title: >-
    غزل شمارهٔ ۱۴۵۰
---
# غزل شمارهٔ ۱۴۵۰

<div class="b" id="bn1"><div class="m1"><p>بستان قدح از دستم ای مست که من مستم</p></div>
<div class="m2"><p>کز حلقه هشیاران این ساعت وارستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هشیار بر رندی ضدی بود و ضدی</p></div>
<div class="m2"><p>همرنگ شو ای خواجه گر فوقم اگر پستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چیز که اندیشی از جنگ از آن دورم</p></div>
<div class="m2"><p>هر چیز که اندیشی از مهر من آنستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا عشق تو بگرفتم سودای تو پذرفتم</p></div>
<div class="m2"><p>با جنگ تو یکتاام با صلح تو همدستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسپانخ خویشم دان با ترش پز و شیرین</p></div>
<div class="m2"><p>با هر چه شدم پخته تا با تو بپیوستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌کار بود سازش سازش نبود نازش</p></div>
<div class="m2"><p>گر جست غلط از من من مست برون جستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی تو و مستی من بربسته به هم دامن</p></div>
<div class="m2"><p>چون دسته و چون هاون دو هست و یکی هستم</p></div></div>