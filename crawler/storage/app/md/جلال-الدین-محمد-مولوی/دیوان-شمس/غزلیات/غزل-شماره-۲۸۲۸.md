---
title: >-
    غزل شمارهٔ ۲۸۲۸
---
# غزل شمارهٔ ۲۸۲۸

<div class="b" id="bn1"><div class="m1"><p>خبری است نورسیده تو مگر خبر نداری</p></div>
<div class="m2"><p>جگر حسود خون شد تو مگر جگر نداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قمری است رونموده پر نور برگشوده</p></div>
<div class="m2"><p>دل و چشم وام بستان ز کسی اگر نداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب از کمان پنهان شب و روز تیر پران</p></div>
<div class="m2"><p>بسپار جان به تیرش چه کنی سپر نداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مس هستیت چو موسی نه ز کیمیاش زر شد</p></div>
<div class="m2"><p>چه غم است اگر چو قارون به جوال زر نداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به درون توست مصری که تویی شکرستانش</p></div>
<div class="m2"><p>چه غم است اگر ز بیرون مدد شکر نداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شده ای غلام صورت به مثال بت پرستان</p></div>
<div class="m2"><p>تو چو یوسفی ولیکن به درون نظر نداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خدا جمال خود را چو در آینه ببینی</p></div>
<div class="m2"><p>بت خویش هم تو باشی به کسی گذر نداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خردانه ظالمی تو که ورا چو ماه گویی</p></div>
<div class="m2"><p>ز چه روش ماه گویی تو مگر بصر نداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر توست چون چراغی بگرفته شش فتیله</p></div>
<div class="m2"><p>همه شش ز چیست روشن اگر آن شرر نداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن توست همچو اشتر که برد به کعبه دل</p></div>
<div class="m2"><p>ز خری به حج نرفتی نه از آنک خر نداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو به کعبه گر نرفتی بکشاندت سعادت</p></div>
<div class="m2"><p>مگریز ای فضولی که ز حق عبر نداری</p></div></div>