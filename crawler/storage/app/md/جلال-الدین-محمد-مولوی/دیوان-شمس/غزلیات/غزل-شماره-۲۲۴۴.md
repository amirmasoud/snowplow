---
title: >-
    غزل شمارهٔ ۲۲۴۴
---
# غزل شمارهٔ ۲۲۴۴

<div class="b" id="bn1"><div class="m1"><p>سیر نیم سیر نی از لب خندان تو</p></div>
<div class="m2"><p>ای که هزار آفرین بر لب و دندان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ کسی سیر شد ای پسر از جان خویش</p></div>
<div class="m2"><p>جان منی چون یکی است جان من و جان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشنه و مستسقیم مرگ و حیاتم ز آب</p></div>
<div class="m2"><p>دور بگردان که من بنده دوران تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش کشی می‌کنی پیش خودم کش تمام</p></div>
<div class="m2"><p>تا که برآرد سرم سر ز گریبان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه دو دستم بخست دست من آن تو است</p></div>
<div class="m2"><p>دست چه کار آیدم بی‌دم و دستان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق تو گفت ای کیا در حرم ما بیا</p></div>
<div class="m2"><p>تا نکند هیچ دزد قصد حرمدان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم ای ذوالقدم حلقه این در شدم</p></div>
<div class="m2"><p>تا که نرنجد ز من خاطر دربان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت که هم بر دری واقف و هم در بری</p></div>
<div class="m2"><p>خارج و داخل توی هر دو وطن آن تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خامش و دیگر مخوان بس بود این نزل و خوان</p></div>
<div class="m2"><p>تا به ابد روم و ترک برخورد از خوان تو</p></div></div>