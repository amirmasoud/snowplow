---
title: >-
    غزل شمارهٔ ۲۰۵۸
---
# غزل شمارهٔ ۲۰۵۸

<div class="b" id="bn1"><div class="m1"><p>با رخ چون مشعله بر در ما کیست آن</p></div>
<div class="m2"><p>هر طرفی موج خون نیم شبان چیست آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کفن خویشتن رقص کنان مردگان</p></div>
<div class="m2"><p>نفخه صور است یا عیسی ثانی است آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه خود باز کن روزن دل درنگر</p></div>
<div class="m2"><p>کآتش تو شعله زد نی خبر دی است آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش نو را ببین زود درآ چون خلیل</p></div>
<div class="m2"><p>گر چه به شکل آتش است باده صافی است آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یونس قدسی تویی در تن چون ماهیی</p></div>
<div class="m2"><p>بازشکاف و ببین کاین تن ماهی است آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلق تن خویش را بر گرو می‌بنه</p></div>
<div class="m2"><p>پاک شوی پاکباز نوبت پاکی است آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده کشیدی ولیک در قدحت باقی است</p></div>
<div class="m2"><p>حمله دیگر که اصل جرعه باقی است آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشنه تیز ار خلیل بنهد بر گردنت</p></div>
<div class="m2"><p>رو بمگردان که آن شیوه شاهی است آن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حکم به هم درشکست هست قضا در خطر</p></div>
<div class="m2"><p>فتنه حکم است این آفت قاضی است آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس تو امروز اگر وعده فردا دهد</p></div>
<div class="m2"><p>بر دهنش زن از آنک مردک لافی است آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باده فروشد ولیک باده دهد جمله باد</p></div>
<div class="m2"><p>خم نماید ولیک حق نمک نیست آن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما ز زمستان نفس برف تن آورده‌ایم</p></div>
<div class="m2"><p>بهر تقاضای لطف نکته کاجی است آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مفخر تبریزیان شمس حق ای پیش تو</p></div>
<div class="m2"><p>طاق و طرنب دو کون طفلی و بازی است آن</p></div></div>