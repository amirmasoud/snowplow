---
title: >-
    غزل شمارهٔ ۹۴۳
---
# غزل شمارهٔ ۹۴۳

<div class="b" id="bn1"><div class="m1"><p>نماز شام چو خورشید در غروب آید</p></div>
<div class="m2"><p>ببندد این ره حس راه غیب بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش درکند ارواح را فرشته خواب</p></div>
<div class="m2"><p>به شیوه گله بانی که گله را پاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به لامکان به سوی مرغزار روحانی</p></div>
<div class="m2"><p>چه شهرها و چه روضاتشان که بنماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار صورت و شخص عجب ببیند روح</p></div>
<div class="m2"><p>چو خواب نقش جهان را از او فروساید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هماره گویی جان خود مقیم آن جا بود</p></div>
<div class="m2"><p>نه یاد این کند و نی ملالش افزاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بار و رخت که این جا بر آن همی‌لرزید</p></div>
<div class="m2"><p>دلش چنان برهد که غمیش نگزاید</p></div></div>