---
title: >-
    غزل شمارهٔ ۱۸۱۸
---
# غزل شمارهٔ ۱۸۱۸

<div class="b" id="bn1"><div class="m1"><p>قصد جفاها نکنی ور بکنی با دل من</p></div>
<div class="m2"><p>وا دل من وا دل من وا دل من وا دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصد کنی بر تن من شاد شود دشمن من</p></div>
<div class="m2"><p>وانگه از این خسته شود یا دل تو یا دل من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واله و مجنون دل من خانه پرخون دل من</p></div>
<div class="m2"><p>بهر تماشا چه شود رنجه شوی تا دل من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورده شکرها دل من بسته کمرها دل من</p></div>
<div class="m2"><p>وقت سحرها دل من رفته به هر جا دل من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرده و زنده دل من گریه و خنده دل من</p></div>
<div class="m2"><p>خواجه و بنده دل من از تو چو دریا دل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای شده استاد امین جز که در آتش منشین</p></div>
<div class="m2"><p>گر چه چنین است و چنین هیچ میاسا دل من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی صلاح دل و دین آمده جبریل امین</p></div>
<div class="m2"><p>در طلب نعمت جان بهر تقاضا دل من</p></div></div>