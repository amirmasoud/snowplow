---
title: >-
    غزل شمارهٔ ۲۹۷۱
---
# غزل شمارهٔ ۲۹۷۱

<div class="b" id="bn1"><div class="m1"><p>اندر قمارخانه چون آمدی به بازی</p></div>
<div class="m2"><p>کارت شود حقیقت هر چند تو مجازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با جمله سازواری ای جان به نیک خویی</p></div>
<div class="m2"><p>این جا که اصل کار است جانا چرا نسازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویی که من شب و روز مرد نمازکارم</p></div>
<div class="m2"><p>چون نیست ای برادر گفتار تو نمازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با ناکسان تو صحبت زنهار تا نداری</p></div>
<div class="m2"><p>شو همنشین شاهان گر مرد سرفرازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر چرا تو خود را کردی چو پای تابه</p></div>
<div class="m2"><p>چون بر لباس آدم تو بهترین طرازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر خر چرا نشینی ای همنشین شاهان</p></div>
<div class="m2"><p>چون هست در رکابت چندین هزار تازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیشه دلی که داری بربا ز سنگ جانان</p></div>
<div class="m2"><p>باری به بزم شاه آ بنگر تو دلنوازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جانت دردمد شه از شادیی که جانت</p></div>
<div class="m2"><p>هم وارهد ز مطرب وز پرده حجازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرمست و پای کوبان با جمع ماه رویان</p></div>
<div class="m2"><p>در نور روی آن شه شاهانه می‌گرازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاهت همی‌نوازد کای پیشوای خاصان</p></div>
<div class="m2"><p>پیوسته پیش ما باش چون تو امین رازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه از جمال پستی گاه از شراب مستی</p></div>
<div class="m2"><p>گه با قدم قرینی گه با کرشم و نازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مقصود شمس دین است هم صدر و هم خداوند</p></div>
<div class="m2"><p>وصلم به خدمت او است چون مرغزی و رازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کس که در دل او باشد هوای تبریز</p></div>
<div class="m2"><p>گردد اگر چه هندو است او گلرخ طرازی</p></div></div>