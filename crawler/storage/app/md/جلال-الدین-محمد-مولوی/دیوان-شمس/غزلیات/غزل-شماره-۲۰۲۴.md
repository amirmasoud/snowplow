---
title: >-
    غزل شمارهٔ ۲۰۲۴
---
# غزل شمارهٔ ۲۰۲۴

<div class="b" id="bn1"><div class="m1"><p>مات خود را صنما مات مکن</p></div>
<div class="m2"><p>بجز از لطف و مراعات مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرده و بی‌ادبی‌ها که برفت</p></div>
<div class="m2"><p>عفو کن هیچ مکافات مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت رحم است بکن کینه مکش</p></div>
<div class="m2"><p>بنده را طعمه آفات مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سر تو که جدایی مندیش</p></div>
<div class="m2"><p>جز که پیوند و ملاقات مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک خود را به زمین برمگذار</p></div>
<div class="m2"><p>منزلش جز به سماوات مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اولش جز به سوی خویش مکش</p></div>
<div class="m2"><p>آخرش جز که سعادات مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچ خو کرد ز لطفت برسان</p></div>
<div class="m2"><p>ترک تیمار و جرایات مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده اهل خرابات توایم</p></div>
<div class="m2"><p>پشت ما را به خرابات مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما که باشیم که گوییم مکن</p></div>
<div class="m2"><p>چونک گفتیم ممارات مکن</p></div></div>