---
title: >-
    غزل شمارهٔ ۳۲۲۸
---
# غزل شمارهٔ ۳۲۲۸

<div class="b" id="bn1"><div class="m1"><p>ایا ملتقی العیش کم تبعدی</p></div>
<div class="m2"><p>و یا فرقة الحسب کم تعتدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیالی الفراق! فکم ذاالجوی؟!</p></div>
<div class="m2"><p>ربی الوصل! ما حان ان تهتدی؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و نشرب من عذب لقیاکم</p></div>
<div class="m2"><p>و من حلو رؤیاکم نعتدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فذاک الوصال، بما نشتری</p></div>
<div class="m2"><p>و قلب‌المعنی بما نفتدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لباسا من‌الطیف کی نکتسی</p></div>
<div class="m2"><p>رداء من‌القرب کی نرتدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فحب الذی نرتجی دیننا</p></div>
<div class="m2"><p>به اختتام به نبتدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایا بعد مولای ، ما تقرب؟</p></div>
<div class="m2"><p>ایا جمرةالقلب، ما تبردی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایا خفق قلبی اما تسکن؟</p></div>
<div class="m2"><p>و یا دمعة العین ما ترکدی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایا حزن قلبی اما تنجلی؟</p></div>
<div class="m2"><p>ایا جفنتی قط ترقدی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعم نور خدیه شمس‌الضحی</p></div>
<div class="m2"><p>نعم مثل حسناه ما یوجد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نعم نار شوقی یکفی الوری</p></div>
<div class="m2"><p>ایا واقد النار لا توقد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فکم تبکی یا عین من صدهم؟</p></div>
<div class="m2"><p>اما تخش یا عین ان ترمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فان ترمدی کیف یوم اللقا</p></div>
<div class="m2"><p>تری سیدا مفخرالسودد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یقول دع ارمد فیوم اللقا</p></div>
<div class="m2"><p>اکحل من حسنه الاثمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لاقسمت حقا لمن لم یلد</p></div>
<div class="m2"><p>تفرد باالمجد لم یولد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابحت الفؤاد لبلواکم</p></div>
<div class="m2"><p>و ان کان حردا علی اردد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا سیدا شمس دین‌العلا</p></div>
<div class="m2"><p>فدیت لتبریزی المسعد</p></div></div>