---
title: >-
    غزل شمارهٔ ۳۳
---
# غزل شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>می ده گزافه ساقیا تا کم شود خوف و رجا</p></div>
<div class="m2"><p>گردن بزن اندیشه را ما از کجا او از کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش آر نوشانوش را از بیخ برکن هوش را</p></div>
<div class="m2"><p>آن عیش بی‌روپوش را از بند هستی برگشا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مجلس ما سرخوش آ برقع ز چهره برگشا</p></div>
<div class="m2"><p>زان سان که اول آمدی ای یفعل الله ما یشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانگان جسته بین از بند هستی رسته بین</p></div>
<div class="m2"><p>در بی‌دلی دل بسته بین کاین دل بود دام بلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زوتر بیا هین دیر شد دل زین ولایت سیر شد</p></div>
<div class="m2"><p>مستش کن و بازش رهان زین گفتن زوتر بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگشا ز دستم این رسن بربند پای بوالحسن</p></div>
<div class="m2"><p>پر ده قدح را تا که من سر را بنشناسم ز پا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی ذوق آن جانی که او در ماجرا و گفت و گو</p></div>
<div class="m2"><p>هر لحظه گرمی می‌کند با بوالعلی و بوالعلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نانم مده آبم مده آسایش و خوابم مده</p></div>
<div class="m2"><p>ای تشنگی عشق تو صد همچو ما را خونبها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز مهمان توام مست و پریشان توام</p></div>
<div class="m2"><p>پر شد همه شهر این خبر کامروز عیش است الصلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کو به جز حق مشتری جوید نباشد جز خری</p></div>
<div class="m2"><p>در سبزه این گولخن همچون خران جوید چرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌دان که سبزه گولخن گنده کند ریش و دهن</p></div>
<div class="m2"><p>زیرا ز خضرای دمن فرمود دوری مصطفی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دورم ز خضرای دمن دورم ز حورای چمن</p></div>
<div class="m2"><p>دورم ز کبر و ما و من مست شراب کبریا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از دل خیال دلبری برکرد ناگاهان سری</p></div>
<div class="m2"><p>ماننده ماه از افق ماننده گل از گیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جمله خیالات جهان پیش خیال او دوان</p></div>
<div class="m2"><p>مانند آهن پاره‌ها در جذبه آهن ربا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بد لعل‌ها پیشش حجر شیران به پیشش گورخر</p></div>
<div class="m2"><p>شمشیرها پیشش سپر خورشید پیشش ذره‌ها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عالم چو کوه طور شد هر ذره‌اش پرنور شد</p></div>
<div class="m2"><p>مانند موسی روح هم افتاد بی‌هوش از لقا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر هستییی در وصل خود در وصل اصل اصل خود</p></div>
<div class="m2"><p>خنبک زنان بر نیستی دستک زنان اندر نما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرسبز و خوش هر تره‌ای نعره زنان هر ذره‌ای</p></div>
<div class="m2"><p>کالصبر مفتاح الفرج و الشکر مفتاح الرضا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گل کرد بلبل را ندا کای صد چو من پیشت فدا</p></div>
<div class="m2"><p>حارس بدی سلطان شدی تا کی زنی طال بقا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ذرات محتاجان شده اندر دعا نالان شده</p></div>
<div class="m2"><p>برقی بر ایشان برزده مانده ز حیرت از دعا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>السلم منهاج الطلب الحلم معراج الطرب</p></div>
<div class="m2"><p>و النار صراف الذهب و النور صراف الولا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>العشق مصباح العشا و الهجر طباخ الحشا</p></div>
<div class="m2"><p>و الوصل تریاق الغشا یا من علی قلبی مشا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>الشمس من افراسنا و البدر من حراسنا</p></div>
<div class="m2"><p>و العشق من جلاسنا من یدر ما فی راسنا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یا سایلی عن حبه اکرم به انعم به</p></div>
<div class="m2"><p>کل المنی فی جنبه عند التجلی کالهبا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یا سایلی عن قصتی العشق قسمی حصتی</p></div>
<div class="m2"><p>و السکر افنی غصتی یا حبذا لی حبذا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>الفتح من تفاحکم و الحشر من اصباحکم</p></div>
<div class="m2"><p>القلب من ارواحکم فی الدور تمثال الرحا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اریاحکم تجلی البصر یعقوبکم یلقی النظر</p></div>
<div class="m2"><p>یا یوسفینا فی البشر جودوا بما الله اشتری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>الشمس خرت و القمر نسکا مع الاحدی عشر</p></div>
<div class="m2"><p>قدامکم فی یقظه قدام یوسف فی الکری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اصل العطایا دخلنا ذخر البرایا نخلنا</p></div>
<div class="m2"><p>یا من لحب او نوی یشکوا مخالیب النوی</p></div></div>