---
title: >-
    غزل شمارهٔ ۴۱۱
---
# غزل شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>عجب ای ساقی جان مطرب ما را چه شدست</p></div>
<div class="m2"><p>هله چون می‌نزند ره ره او را کی زدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او ز هر نیک و بد خلق چرا می‌لنگد</p></div>
<div class="m2"><p>بد و نیک همه را نعره مطرب مددست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دف دریدست طرب را به خدا بی‌دف او</p></div>
<div class="m2"><p>مجلس یارکده بی‌دم او بارکدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهر غلبیرگهی دان که شود زیر و زبر</p></div>
<div class="m2"><p>دست غلبیرزنش سخره صاحب بلدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیره کم گوی خمش مطرب مسکین چه کند</p></div>
<div class="m2"><p>این همه فتنه آن فتنه گر خوب خدست</p></div></div>