---
title: >-
    غزل شمارهٔ ۱۴۴۹
---
# غزل شمارهٔ ۱۴۴۹

<div class="b" id="bn1"><div class="m1"><p>زان می که ز بوی او شوریده و سرمستم</p></div>
<div class="m2"><p>دریاب مرا ساقی والله که چنینستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساقی مست من بنگر به شکست من</p></div>
<div class="m2"><p>ای جسته ز دست من دریاب کز آن دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشکست مرا دامت بشکستم من جامت</p></div>
<div class="m2"><p>مستی تو و مستی من بشکستی و بشکستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان و دل مستان بستان سخنم بستان</p></div>
<div class="m2"><p>گویی که نه ای محرم هستم به خدا هستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر کن ز می پیشین بنشین بر من بنشین</p></div>
<div class="m2"><p>بنشین که چنین وقتی در خواب همی‌جستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان و سر تو یارا بر نقد بزن ما را</p></div>
<div class="m2"><p>مفریب و مگو فردا بردارم و بفرستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>والله که بنگذارم دست از تو چرا دارم</p></div>
<div class="m2"><p>تا لاف زنی گویی کز عربده وارستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهم که ز باد می آتش بفروزانی</p></div>
<div class="m2"><p>خواهم که ز آب خود چون خاک کنی پستم</p></div></div>