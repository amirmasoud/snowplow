---
title: >-
    غزل شمارهٔ ۱۷۴۷
---
# غزل شمارهٔ ۱۷۴۷

<div class="b" id="bn1"><div class="m1"><p>اگر به عقل و کفایت پی جنون باشم</p></div>
<div class="m2"><p>میان حلقه عشاق ذوفنون باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم به عشق سلیمان زبان من آصف</p></div>
<div class="m2"><p>چرا ببسته هر داروی فسون باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلیل وار نپیچم سر خود از کعبه</p></div>
<div class="m2"><p>مقیم کعبه شوم کعبه را ستون باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار رستم دستان به گرد ما نرسد</p></div>
<div class="m2"><p>به دست نفس مخنث چرا زبون باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست گیرم آن ذوالفقار پرخون را</p></div>
<div class="m2"><p>شهید عشقم و اندر میان خون باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این بساط منم عندلیب الرحمان</p></div>
<div class="m2"><p>مجوی حد و کنارم ز حد برون باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا به عشق بپرورد شمس تبریزی</p></div>
<div class="m2"><p>ز روح قدس ز کروبیان فزون باشم</p></div></div>