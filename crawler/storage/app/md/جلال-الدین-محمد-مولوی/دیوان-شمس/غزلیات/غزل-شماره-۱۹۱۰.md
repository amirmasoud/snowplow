---
title: >-
    غزل شمارهٔ ۱۹۱۰
---
# غزل شمارهٔ ۱۹۱۰

<div class="b" id="bn1"><div class="m1"><p>تو هر جزو جهان را بر گذر بین</p></div>
<div class="m2"><p>تو هر یک را رسیده از سفر بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو هر یک را به طمع روزی خود</p></div>
<div class="m2"><p>به پیش شاه خود بنهاده سر بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مثال اختران از بهر تابش</p></div>
<div class="m2"><p>فتاده عاجز اندر پای خور بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مثال سیل‌ها در جستن آب</p></div>
<div class="m2"><p>به سوی بحرشان زیر و زبر بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای هر یکی از مطبخ شاه</p></div>
<div class="m2"><p>به قدر او تو خوان معتبر بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیش جام بحرآشام ایشان</p></div>
<div class="m2"><p>تو دریای جهان را مختصر بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وان‌ها را که روزی روی شاه است</p></div>
<div class="m2"><p>ز حسن شه دهانش پرشکر بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چشم شمس تبریزی تو بنگر</p></div>
<div class="m2"><p>یکی دریای دیگر پرگهر بین</p></div></div>