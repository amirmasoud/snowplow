---
title: >-
    غزل شمارهٔ ۱۲۷۲
---
# غزل شمارهٔ ۱۲۷۲

<div class="b" id="bn1"><div class="m1"><p>باز فرود آمدیم بر در سلطان خویش</p></div>
<div class="m2"><p>بازگشادیم خوش بال و پر جان خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز سعادت رسید دامن ما را کشید</p></div>
<div class="m2"><p>بر سر گردون زدیم خیمه و ایوان خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده دیو و پری دید ز ما سروری</p></div>
<div class="m2"><p>هدهد جان بازگشت سوی سلیمان خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی مستان ما شد شکرستان ما</p></div>
<div class="m2"><p>یوسف جان برگشاد جعد پریشان خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش مرا گفت یار چونی از این روزگار</p></div>
<div class="m2"><p>چون بود آن کس که دید دولت خندان خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن شکری را که هیچ مصر ندیدش به خواب</p></div>
<div class="m2"><p>شکر که من یافتم در بن دندان خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌زر و سر سروریم بی‌حشمی مهتریم</p></div>
<div class="m2"><p>قند و شکر می‌خوریم در شکرستان خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو زر بس نادری نیست کست مشتری</p></div>
<div class="m2"><p>صنعت آن زرگری رو به سوی کان خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور قمر عمرها ناقص و کوته بود</p></div>
<div class="m2"><p>عمر درازی نهاد یار به دوران خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل سوی تبریز رفت در هوس شمس دین</p></div>
<div class="m2"><p>رو رو ای دل بجو زر به حرمدان خویش</p></div></div>