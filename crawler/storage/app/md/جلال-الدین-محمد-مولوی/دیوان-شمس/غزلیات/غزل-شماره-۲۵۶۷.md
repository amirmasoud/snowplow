---
title: >-
    غزل شمارهٔ ۲۵۶۷
---
# غزل شمارهٔ ۲۵۶۷

<div class="b" id="bn1"><div class="m1"><p>افتاد دل و جانم در فتنه طراری</p></div>
<div class="m2"><p>سنگینک جنگینک سر بسته چو بیماری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آید سوی بی‌خوابی خواهد ز درش آبی</p></div>
<div class="m2"><p>آب چه که می‌خواهد تا درفکند ناری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوید که به اجرت ده این خانه مرا چندی</p></div>
<div class="m2"><p>هین تا چه کنی سازم از آتشش انباری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه گوید این عرصه کاین خانه برآوردی</p></div>
<div class="m2"><p>بوده‌ست از آن من تو دانی و دیواری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیوار ببر زین جا این عرصه به ما واده</p></div>
<div class="m2"><p>در عرصه جان باشد دیوار تو مرداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دلبر سروین قد در قصد کسی باشد</p></div>
<div class="m2"><p>در کوی همی‌گردد چون مشتغل کاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناگه بکند چاهی ناگه بزند راهی</p></div>
<div class="m2"><p>ناگه شنوی آهی از کوچه و بازاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان نقش همی‌خواند می‌داند و می‌راند</p></div>
<div class="m2"><p>چون رخت نمی‌ماند در غارت او باری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شاه شکرخنده‌ای شادی هر زنده</p></div>
<div class="m2"><p>دل کیست تو را بنده جان کیست گرفتاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ذوق دل از نوشت وی شوق دل از جوشت</p></div>
<div class="m2"><p>پیش آر به من گوشت تا نشنود اغیاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از باغ تو جان و تن پر کرده ز گل دامن</p></div>
<div class="m2"><p>آموخت خرامیدن با تو به سمن زاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان گوش همی‌خارد کاومید چنین دارد</p></div>
<div class="m2"><p>و آن گاه یقین دارد این از کرمت آری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا از تو شدم دانا چون چنگ شدم جانا</p></div>
<div class="m2"><p>بشنو هله مولانا زاری چنین زاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا عشق حمیاخد این مهر همی‌کارد</p></div>
<div class="m2"><p>خامش که دلم دارد بی‌مشغله گفتاری</p></div></div>