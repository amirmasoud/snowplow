---
title: >-
    غزل شمارهٔ ۲۲۶۵
---
# غزل شمارهٔ ۲۲۶۵

<div class="b" id="bn1"><div class="m1"><p>الیوم من الوصل نسیم و سعود</p></div>
<div class="m2"><p>الیوم اری الحب علی العهد فعودوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفته‌ست رقیب و بر آن یار نبود او</p></div>
<div class="m2"><p>بی‌زحمت دشمن دم عشاق شنود او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا قلب ابشرک به وصل و رحیق</p></div>
<div class="m2"><p>ما فاتک من دهرک الیوم یعود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر است عدو رفته و ما همدم جامیم</p></div>
<div class="m2"><p>ما سرخ و سپید از طرب و کور و کبود او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا حب حنا نیک تجلیت بوصل</p></div>
<div class="m2"><p>الروح فدا روحک بالروح تجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را که برای دل حساد جفا گفت</p></div>
<div class="m2"><p>امروز چو خلوت شد ما را بستود او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هذا قمر قد غلب الشمس بنور</p></div>
<div class="m2"><p>من طالعه الیوم علی الشمس یسود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز نقاب از رخ خود ماه برانداخت</p></div>
<div class="m2"><p>بر طلعت خورشید و مه و زهره فزود او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما اکثر ما قد خفض العیش به هجر</p></div>
<div class="m2"><p>للعیش من الیوم نهوض و صعود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیوسته ز خورشید ستاند مه نو نور</p></div>
<div class="m2"><p>این مه که به خورشید دهد نور چه بود او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا قلب تمتع و طب ان شکورا</p></div>
<div class="m2"><p>الحب شفیق لک و الله ودود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این دم سپه عشق چه خوش دست گشادند</p></div>
<div class="m2"><p>چون یک گره از طره پربند گشود او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الحب الی المجلس والله سقانا</p></div>
<div class="m2"><p>و السکر من القهوه کالدهر ولود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن غم که ز عشاق بسی گرد برآورد</p></div>
<div class="m2"><p>بیرون ز در است این دم و از بام فرود او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الیوم من العیش لقاء و شفا</p></div>
<div class="m2"><p>الیوم من السکر رکوع و سجود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن ساغر لاغرشده را داروی دل ده</p></div>
<div class="m2"><p>دیر است که محروم شد از ذوق وجود او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا قوم الی العشق انیبوا و اجیبوا</p></div>
<div class="m2"><p>لما کتب الله علی العشق خلود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>امروز صلا می‌زند این خفته دلان را</p></div>
<div class="m2"><p>آن عشق سماوی که نخفت و نغنود او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>العشق من الکون حیات و لباب</p></div>
<div class="m2"><p>و العیش سوی العشق قشور و جلود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر دوست که از عشق به دنیات کشاند</p></div>
<div class="m2"><p>خود دشمن تو او است یقین دان و حسود او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لا تنطق فی العشق و یکفیک انین</p></div>
<div class="m2"><p>فالمخلص للعاشق صبر و جحود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بس کن تو مگو هیچ که تا اشک بگوید</p></div>
<div class="m2"><p>دل خود چو بسوزد بدهد بوی چو عود او</p></div></div>