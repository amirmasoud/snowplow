---
title: >-
    غزل شمارهٔ ۷۱۸
---
# غزل شمارهٔ ۷۱۸

<div class="b" id="bn1"><div class="m1"><p>آخر گهر وفا ببارید</p></div>
<div class="m2"><p>آخر سر عاشقان بخارید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خاک شما شدیم در خاک</p></div>
<div class="m2"><p>تخم ستم و جفا مکارید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر مظلومان راه هجران</p></div>
<div class="m2"><p>این ظلم دگر روا مدارید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای زهره ییان به بام این مه</p></div>
<div class="m2"><p>بر پرده زیر و بم بزارید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا نیز شما ز درد دوری</p></div>
<div class="m2"><p>همچون من خسته دلفکارید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محروم نماند کس از این در</p></div>
<div class="m2"><p>ما را به کسی نمی‌شمارید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن درد که کوه از او چو ذرست</p></div>
<div class="m2"><p>بر ذرگکی چه می‌گمارید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای قوم که شیرگیر بودیت</p></div>
<div class="m2"><p>آن آهو را کنون شکارید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان نرگس مست شیرگیرش</p></div>
<div class="m2"><p>بی خمر وصال در خمارید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان دلبر گلعذار اکنون</p></div>
<div class="m2"><p>بس بی‌دل و زعفران عذارید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با این همه گنج نیست بی‌رنج</p></div>
<div class="m2"><p>بر صبر و وفا قدم فشارید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مردانه و مردرنگ باشید</p></div>
<div class="m2"><p>گر در ره عشق مرد کارید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون عاشق را هزار جانست</p></div>
<div class="m2"><p>بی صرفه و ترس جان سپارید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان کم ناید ز جان مترسید</p></div>
<div class="m2"><p>کاندر پی جان کامکارید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عشقست حریف حیله آموز</p></div>
<div class="m2"><p>گرد از دغل و حیل برآرید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در عشق حلال گشت حیله</p></div>
<div class="m2"><p>در عشق رهین صد قمارید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حقست اگر ز عشق آن سرو</p></div>
<div class="m2"><p>با جمله گلرخان چو خارید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حقست اگر ز عشق موسی</p></div>
<div class="m2"><p>بر فرعونان نفس مارید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جان را سپر بلاش سازید</p></div>
<div class="m2"><p>کاندر کف عشق ذوالفقارید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در صبر و ثبات کوه قافید</p></div>
<div class="m2"><p>چون کوه حلیم و باوقارید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون بحر نهان به مظهر آید</p></div>
<div class="m2"><p>ماننده موج بی‌قرارید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هنگام نثار و درفشانی</p></div>
<div class="m2"><p>چون ابر به وقت نوبهارید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در تیر شهیت اگر شهیدیت</p></div>
<div class="m2"><p>در پیش مهیت اگر غبارید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پاینده و تازه همچو سروید</p></div>
<div class="m2"><p>چون شاخ بلند میوه دارید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز آسیب درخت او چو سیبید</p></div>
<div class="m2"><p>چون سیب درخت سنگسارید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر سنگ دلان زنندتان سنگ</p></div>
<div class="m2"><p>با گوهر خویش یار غارید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون دامن در پیش دوانید</p></div>
<div class="m2"><p>گر همچو سجاف بر کنارید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون همسفرید با مه خویش</p></div>
<div class="m2"><p>پیوسته چو چرخ در دوارید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هم عشق شما و هم شما عشق</p></div>
<div class="m2"><p>با اشتر عشق هم مهارید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر نقب زنست نفس و دزدست</p></div>
<div class="m2"><p>آخر نه در این حصین حصارید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از عشق خورید باده و نقل</p></div>
<div class="m2"><p>گر مقبل وگر حلال خوارید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دیدیت که تان همی‌نگارد</p></div>
<div class="m2"><p>دیگر چه خیال می‌نگارید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اوتان به خود اختیار کردست</p></div>
<div class="m2"><p>چه در پی جبر و اختیارید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>محکوم یک اختیار باشید</p></div>
<div class="m2"><p>گر عاشق و اهل اعتبارید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خاموش کنم اگر چه با من</p></div>
<div class="m2"><p>در نطق و سکوت سازوارید</p></div></div>