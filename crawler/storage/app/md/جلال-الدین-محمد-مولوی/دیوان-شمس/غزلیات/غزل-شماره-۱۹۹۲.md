---
title: >-
    غزل شمارهٔ ۱۹۹۲
---
# غزل شمارهٔ ۱۹۹۲

<div class="b" id="bn1"><div class="m1"><p>خوی با ما کن و با بی‌خبران خوی مکن</p></div>
<div class="m2"><p>دم هر ماده خری را چو خران بوی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول و آخر تو عشق ازل خواهد بود</p></div>
<div class="m2"><p>چون زن فاحشه هر شب تو دگر شوی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بنه بر هوسی که دل از آن برنکنی</p></div>
<div class="m2"><p>شیرمردا دل خود را سگ هر کوی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم بدان سو که گه درد دوا می خواهی</p></div>
<div class="m2"><p>وقف کن دیده و دل روی به هر سوی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو اشتر بمدو جانب هر خاربنی</p></div>
<div class="m2"><p>ترک این باغ و بهار و چمن و جوی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان که خاقان بنهاده است شهانه بزمی</p></div>
<div class="m2"><p>اندر این مزبله از بهر خدا طوی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میر چوگانی ما جانب میدان آمد</p></div>
<div class="m2"><p>پی اسپش دل و جان را هله جز گوی مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی را پاک بشو عیب بر آیینه منه</p></div>
<div class="m2"><p>نقد خود را سره کن عیب ترازوی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز بر آن که لبت داد لب خود مگشا</p></div>
<div class="m2"><p>جز سوی آنک تکت داد تکاپوی مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی و مویی که بتان راست دروغین می دان</p></div>
<div class="m2"><p>نامشان را تو قمرروی زره موی مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر کلوخی است رخ و چشم و لب عاریتی</p></div>
<div class="m2"><p>پیش بی‌چشم به جد شیوه ابروی مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قامت عشق صلا زد که سماع ابدی است</p></div>
<div class="m2"><p>جز پی قامت او رقص و هیاهوی مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دم مزن ور بزنی زیر لب آهسته بزن</p></div>
<div class="m2"><p>دم حجاب است یکی تو کن و صدتوی مکن</p></div></div>