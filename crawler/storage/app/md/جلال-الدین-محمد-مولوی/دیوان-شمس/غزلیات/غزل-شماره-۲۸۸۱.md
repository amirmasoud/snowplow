---
title: >-
    غزل شمارهٔ ۲۸۸۱
---
# غزل شمارهٔ ۲۸۸۱

<div class="b" id="bn1"><div class="m1"><p>گر تو ما را به جفای صنمان ترسانی</p></div>
<div class="m2"><p>شکم گرسنگان را تو به نان ترسانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و به دشنام بتم آیی و تهدید دهی</p></div>
<div class="m2"><p>مردگان را بنشانی و به جان ترسانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور به مجنون سقطی از لب لیلی آری</p></div>
<div class="m2"><p>همچو مخمورکش از رطل گران ترسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که چون دیگ بر آتش ز تبش خشک لبم</p></div>
<div class="m2"><p>گوش آنم کم از آن چرب زبان ترسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرگ هجران پی من کرد و مرا ننگ آورد</p></div>
<div class="m2"><p>گرگ ترسد نه من ار تو به شبان ترسانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده‌ای گر تو ز تلخی ویم بیم دهی</p></div>
<div class="m2"><p>ساده‌ای گر مگسان را تو بخوان ترسانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاکبازند و مقامر که در این جا جمعند</p></div>
<div class="m2"><p>نیست تاجر که تو او را به زیان ترسانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خیالات لطیفند نه خونند و نه گوشت</p></div>
<div class="m2"><p>که تو تیری بزنی یا به کمان ترسانی</p></div></div>