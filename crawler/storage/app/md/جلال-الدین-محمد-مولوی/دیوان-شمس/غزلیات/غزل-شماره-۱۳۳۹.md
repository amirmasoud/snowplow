---
title: >-
    غزل شمارهٔ ۱۳۳۹
---
# غزل شمارهٔ ۱۳۳۹

<div class="b" id="bn1"><div class="m1"><p>مهم را لطف در لطفست از آنم بی‌قرار ای دل</p></div>
<div class="m2"><p>دلم پرچشمه حیوان تنم در لاله زار ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زیر هر درختی بین نشسته بهر روی شه</p></div>
<div class="m2"><p>ملیحی یوسفی مه رو لطیفی گلعذار ای دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فکنده در دل خوبان روحانی و جسمانی</p></div>
<div class="m2"><p>ز عشق روح و جسم خود ز سوداها شرار ای دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآکنده ز شادی‌ها درون چاکران خود</p></div>
<div class="m2"><p>مثال دانه‌های در که باشد در انار ای دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بزم او چو مستان را کنار و لطف‌ها باشد</p></div>
<div class="m2"><p>بگیرد آب با آتش ز عشقش هم کنار ای دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن خلوت که خوبان را به جام خاص بنوازد</p></div>
<div class="m2"><p>بود روح الامین حارس و خضرش پرده دار ای دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از بزمش برون آید کمینه چاکرش سکران</p></div>
<div class="m2"><p>ز ملک و ملک و تخت و بخت دارد ننگ و عار ای دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان بستان او را دان و این عالم چو غاری دان</p></div>
<div class="m2"><p>برون آرد تو را لطفش از این تاریک غار ای دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلستان‌ها و ریحان‌ها شقایق‌های گوناگون</p></div>
<div class="m2"><p>بنفشه زارها بر خاک و باد و آب و نار ای دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که این گل‌های خاکی هم ز عکس آن همی‌روید</p></div>
<div class="m2"><p>تو خاکی می‌خوری این جا تو را آن جا چه کار ای دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزن دستی و رقصی کن ز عشق آن خداوندان</p></div>
<div class="m2"><p>که چون بوسی از او یابی کند آفت کنار ای دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به جان پاک شمس الدین خداوند خداوندان</p></div>
<div class="m2"><p>که پرها هم از او یابی اگر خواهی فرار ای دل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خاک پای تبریزی که اکسیرست خاک او</p></div>
<div class="m2"><p>که جان‌ها یابی ار بر وی کنی جانی نثار ای دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون از هجر بر پایم چنین بندیست از آتش</p></div>
<div class="m2"><p>ز یادش مست و مخمورم اگر چندم نزار ای دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مثال چنگ می‌باشم هزاران نغمه‌ها دارد</p></div>
<div class="m2"><p>به لحن عشق انگیزش وگر نالید زار ای دل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سودای چنان بختی که معشوق از سر دستی</p></div>
<div class="m2"><p>به دستم داده بود از لطف دنبال مهار ای دل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگرد مرکبم بودی به زیر سایه آن شاه</p></div>
<div class="m2"><p>هزاران شاه در خدمت به صف‌ها در قطار ای دل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از این سو نه از آن سوی جهان روح تا دانی</p></div>
<div class="m2"><p>که آن جا که نه امسالست و آن سالست پار ای دل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو دیدم من عنایت‌ها ز صدر غیب شمس الدین</p></div>
<div class="m2"><p>شدم مغرور خاصه مست و مجنون خمار ای دل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان حلمی و تمکینی چنان صبر خداوندی</p></div>
<div class="m2"><p>که اندر صبر ایوبش نتاند بود یار ای دل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عنان از من چنان برتافت جایی شد که وهم آن جا</p></div>
<div class="m2"><p>به جسم او نیابد راه و نی چشمش غبار ای دل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به درگاه خدا نالم که سایه آفتابی را</p></div>
<div class="m2"><p>به ما آرد که دل را نیست بی او پود و تار ای دل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امیدست ای دل غمگین که ناگاهان درآید او</p></div>
<div class="m2"><p>تو این جان را به صد حیله همی‌کن داردار ای دل</p></div></div>