---
title: >-
    غزل شمارهٔ ۱۷۰۸
---
# غزل شمارهٔ ۱۷۰۸

<div class="b" id="bn1"><div class="m1"><p>ای گوش من گرفته تویی چشم روشنم</p></div>
<div class="m2"><p>باغم چه می بری چو تویی باغ و گلشنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری است کز عطای تو من طبل می خورم</p></div>
<div class="m2"><p>در سایه لوای کرم طبل می زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می مالم این دو چشم که خواب است یا خیال</p></div>
<div class="m2"><p>باور نمی‌کنم عجب ای دوست کاین منم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آری منم ولیک برون رفته از منی</p></div>
<div class="m2"><p>چون ماه نو ز بدر تو باریک می تنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تاج خسروان به حقارت نظر کنم</p></div>
<div class="m2"><p>تا شوق روی توست مها طوق گردنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با ماهیان ز بحر تو من نزل می خورم</p></div>
<div class="m2"><p>با خاکیان ز رشک تو چون آب و روغنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه ز بحر صنعت من آب خوردنی است</p></div>
<div class="m2"><p>چون ماهیم نبیند کس آب خوردنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ناخن جفا بخراشد رگ مرا</p></div>
<div class="m2"><p>من خوش صدا چو چنگ ز آسیب ناخنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود پی ببرده‌ای تو که رگ دار نیستم</p></div>
<div class="m2"><p>گر می جهد رگی بنما تاش برکنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتی چه کار داری بر نیست کار نیست</p></div>
<div class="m2"><p>گر نیست نیستم ز چه شد نیست مسکنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفخ قیامتی تو و من شخص مرده‌ام</p></div>
<div class="m2"><p>تو جان نوبهاری و من سرو و سوسنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من نیم کاره گفتم باقیش تو بگو</p></div>
<div class="m2"><p>تو عقل عقل عقلی و من سخت کودنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من صورتی کشیدم جان بخشی آن توست</p></div>
<div class="m2"><p>تو جان جان جانی و من قالب تنم</p></div></div>