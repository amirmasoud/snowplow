---
title: >-
    غزل شمارهٔ ۸۸۱
---
# غزل شمارهٔ ۸۸۱

<div class="b" id="bn1"><div class="m1"><p>آه که بار دگر آتش در من فتاد</p></div>
<div class="m2"><p>وین دل دیوانه باز روی به صحرا نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه که دریای عشق بار دگر موج زد</p></div>
<div class="m2"><p>وز دل من هر طرف چشمه خون برگشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه که جست آتشی خانه دل درگرفت</p></div>
<div class="m2"><p>دود گرفت آسمان آتش من یافت باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش دل سهل نیست هیچ ملامت مکن</p></div>
<div class="m2"><p>یا رب فریاد رس ز آتش دل داد داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لشکر اندیشه‌ها می‌رسد از بیشه‌ها</p></div>
<div class="m2"><p>سوی دلم طلب طلب وز غم من شاد شاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل روشن ضمیر بر همه دل‌ها امیر</p></div>
<div class="m2"><p>صبر گزیدی و یافت جان تو جمله مراد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم همه خشک و تر مانده در همدگر</p></div>
<div class="m2"><p>چشم تو سوی خداست چشم همه بر تو باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست تو دست خدا چشم تو مست خدا</p></div>
<div class="m2"><p>بر همه پاینده باد سایه رب العباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله خلق از شماست آن شما از کجاست</p></div>
<div class="m2"><p>این همه از عشق زاد عشق عجب از چه زاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمس حق دین تویی مالک ملک وجود</p></div>
<div class="m2"><p>ای که ندیده چو تو عشق دگر کیقباد</p></div></div>