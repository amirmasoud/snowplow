---
title: >-
    غزل شمارهٔ ۱۸۴۶
---
# غزل شمارهٔ ۱۸۴۶

<div class="b" id="bn1"><div class="m1"><p>حرام است ای مسلمانان از این خانه برون رفتن</p></div>
<div class="m2"><p>می چون ارغوان هشتن ز بانگ ارغنون رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون زرق است یا استم هزاران بار دیدستم</p></div>
<div class="m2"><p>از این پس ابلهی باشد برای آزمون رفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرو زین خانه ای مجنون که خون گریی ز هجران خون</p></div>
<div class="m2"><p>چو دستی را فروبری عجایب نیست خون رفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شمع آموز ای خواجه میان گریه خندیدن</p></div>
<div class="m2"><p>ز چشم آموز ای زیرک به هنگام سکون رفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر باشد تو را روزی ز استادان بیاموزی</p></div>
<div class="m2"><p>چو مرغ جان معصومان به چرخ نیلگون رفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا ای جان که وقتت خوش چو استن بار ما می کش</p></div>
<div class="m2"><p>که تا صبرت بیاموزد به سقف بی‌ستون رفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فسون عیسی مریم نکرد از درد عاشق کم</p></div>
<div class="m2"><p>وظیفه درد دل نبود به دارو و فسون رفتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو طاسی سرنگون گردد رود آنچ در او باشد</p></div>
<div class="m2"><p>ولی سودا نمی‌تاند ز کاسه سر نگون رفتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر پاکی و ناپاکی مرو زین خانه‌ای زاکی</p></div>
<div class="m2"><p>گناهی نیست در عالم تو را ای بنده چون رفتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تویی شیر اندر این درگه عدو راه تو روبه</p></div>
<div class="m2"><p>بود بر شیر بدنامی از این چالش زبون رفتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نازی می کشی باری بیا ناز چنین شه کش</p></div>
<div class="m2"><p>که بس بداختری باشد به زیر چرخ دون رفتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز دانش‌ها بشویم دل ز خود خود را کنم غافل</p></div>
<div class="m2"><p>که سوی دلبر مقبل نشاید ذوفنون رفتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شناسد جان مجنونان که این جان است قشر جان</p></div>
<div class="m2"><p>بباید بهر این دانش ز دانش در جنون رفتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی کو دم زند بی‌دم مباح او راست غواصی</p></div>
<div class="m2"><p>کسی کو کم زند در کم رسد او را فزون رفتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رها کن تا بگوید او خموشی گیر و توبه جو</p></div>
<div class="m2"><p>که آن دلدار خو دارد به سوی تایبون رفتن</p></div></div>