---
title: >-
    غزل شمارهٔ ۵۶۷
---
# غزل شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>نباشد عیب پرسیدن تو را خانه کجا باشد</p></div>
<div class="m2"><p>نشانی ده اگر یابیم وان اقبال ما باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خورشید جهان باشی ز چشم ما نهان باشی</p></div>
<div class="m2"><p>تو خود این را روا داری وانگه این روا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگفتی من وفادارم وفا را من خریدارم</p></div>
<div class="m2"><p>ببین در رنگ رخسارم بیندیش این وفا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا ای یار لعلین لب دلم گم گشت در قالب</p></div>
<div class="m2"><p>دلم داغ شما دارد یقین پیش شما باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این آتش کبابم من خراب اندر خرابم من</p></div>
<div class="m2"><p>چه باشد ای سر خوبان تنی کز سر جدا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من در فراق جان چو ماری سرزده پیچان</p></div>
<div class="m2"><p>بگرد نقش تو گردان مثال آسیا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتم ای دل مسکین بیا بر جای خود بنشین</p></div>
<div class="m2"><p>حذر کن ز آتش پرکین دل من گفت تا باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فروبستست تدبیرم بیا ای یار شبگیرم</p></div>
<div class="m2"><p>بپرس از شاه کشمیرم کسی را کشنا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود او پیدا و پنهانست جهان نقش است و او جانست</p></div>
<div class="m2"><p>بیندیش این چه سلطانست مگر نور خدا باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خروش و جوش هر مستی ز جوش خم می باشد</p></div>
<div class="m2"><p>سبکساری هر آهن ز تو آهن ربا باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خریدی خانه دل را دل آن توست می‌دانی</p></div>
<div class="m2"><p>هر آنچ هست در خانه از آن کدخدا باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قماشی کان تو نبود برون انداز از خانه</p></div>
<div class="m2"><p>درون مسجد اقصی سگ مرده چرا باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مسلم گشت دلداری تو را ای تو دل عالم</p></div>
<div class="m2"><p>مسلم گشت جان بخشی تو را وان دم تو را باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که دریا را شکافیدن بود چالاکی موسی</p></div>
<div class="m2"><p>قبای مه شکافیدن ز نور مصطفی باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برآرد عشق یک فتنه که مردم راه که گیرد</p></div>
<div class="m2"><p>به شهر اندر کسی ماند که جویای فنا باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زند آتش در این بیشه که بگریزند نخجیران</p></div>
<div class="m2"><p>ز آتش هر که نگریزد چو ابراهیم ما باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خمش کوته کن ای خاطر که علم اول و آخر</p></div>
<div class="m2"><p>بیان کرده بود عاشق چو پیش شاه لا باشد</p></div></div>