---
title: >-
    غزل شمارهٔ ۷۰۷
---
# غزل شمارهٔ ۷۰۷

<div class="b" id="bn1"><div class="m1"><p>آن یوسف خوش عذار آمد</p></div>
<div class="m2"><p>وان عیسی روزگار آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان سنجق صد هزار نصرت</p></div>
<div class="m2"><p>بر موکب نوبهار آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای کار تو مرده زنده کردن</p></div>
<div class="m2"><p>برخیز که روز کار آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیری که به صید شیر گیرد</p></div>
<div class="m2"><p>سرمست به مرغزار آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دی رفت و پریر نقد بستان</p></div>
<div class="m2"><p>کان نقد خوش عیار آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این شهر امروز چون بهشتست</p></div>
<div class="m2"><p>می‌گوید شهریار آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌زن دهلی که روز عیدست</p></div>
<div class="m2"><p>می‌کن طربی که یار آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماهی از غیب سر برون کرد</p></div>
<div class="m2"><p>کاین مه بر او غبار آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خوبی آن قرار جان‌ها</p></div>
<div class="m2"><p>عالم همه بی‌قرار آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هین دامن عشق برگشایید</p></div>
<div class="m2"><p>کز چرخ نهم نثار آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای مرغ غریب پربریده</p></div>
<div class="m2"><p>بر جای دو پر چهار آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هان ای دل بسته سینه بگشا</p></div>
<div class="m2"><p>کان گمشده در کنار آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای پای بیا و پای می‌کوب</p></div>
<div class="m2"><p>کان سرده نامدار آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پیر مگو که او جوان شد</p></div>
<div class="m2"><p>وز پار مگو که پار آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتی با شه چه عذر گویم</p></div>
<div class="m2"><p>خود شاه به اعتذار آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتی که کجا رهم ز دستش</p></div>
<div class="m2"><p>دستش همه دستیار آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ناری دیدی و نور آمد</p></div>
<div class="m2"><p>خونی دیدی عقار آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن کس که ز بخت خود گریزد</p></div>
<div class="m2"><p>بگریخته شرمسار آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خامش کن و لطف‌هاش مشمر</p></div>
<div class="m2"><p>لطفیست که بی‌شمار آمد</p></div></div>