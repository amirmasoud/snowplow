---
title: >-
    غزل شمارهٔ ۲۵۱۲
---
# غزل شمارهٔ ۲۵۱۲

<div class="b" id="bn1"><div class="m1"><p>رها کن ماجرا ای جان فروکن سر ز بالایی</p></div>
<div class="m2"><p>که آمد نوبت عشرت زمان مجلس آرایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باشد جرم و سهو ما به پیش یرلغ لطفت</p></div>
<div class="m2"><p>کجا تردامنی ماند چو تو خورشید ما رایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآ ای تاج و تخت ما برون انداز رخت ما</p></div>
<div class="m2"><p>بسوزان هر چه می‌سوزی بفرما هر چه فرمایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر آتش زنی سوزی تو باغ عقل کلی را</p></div>
<div class="m2"><p>هزاران باغ برسازی ز بی‌عقلی و شیدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر رسوا شود عاشق به صد مکروه و صد تهمت</p></div>
<div class="m2"><p>از این سویش بیالایی وزان سویش بیارایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه تو اجزای آبی را بدادی تابش جوهر</p></div>
<div class="m2"><p>نه تو اجزای خاکی را بدادی حله خضرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه از اجزای یک آدم جهان پرآدمی کردی</p></div>
<div class="m2"><p>نه آنی که مگس را تو بدادی فر عنقایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبیبی دید کوری را نمودش داروی دیده</p></div>
<div class="m2"><p>بگفتش سرمه ساز این را برای نور بینایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفتش کور اگر آن را که من دیدم تو می‌دیدی</p></div>
<div class="m2"><p>دو چشم خویش می‌کندی و می‌گشتی تماشایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی لطفی که بر بستان و گورستان همی‌ریزی</p></div>
<div class="m2"><p>زهی نوری که اندر چشم و در بی‌چشم می‌آیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بر زندگان ریزی برون پرند از گردون</p></div>
<div class="m2"><p>وگر بر مردگان ریزی شود مرده مسیحایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غذای زاغ سازیدی ز سرگینی و مرداری</p></div>
<div class="m2"><p>چه داند زاغ کان طوطی چه دارد در شکرخایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه گفت آن زاغ بیهوده که سرگینش خورانیدی</p></div>
<div class="m2"><p>نگهدار ای خدا ما را از آن گفتار و بدرایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه گفت آن طوطی اخضر که شکر دادیش درخور</p></div>
<div class="m2"><p>به فضل خود زبان ما بدان گفتار بگشایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کیست آن زاغ سرگین چش کسی کو مبتلا گردد</p></div>
<div class="m2"><p>به علمی غیر علم دین برای جاه دنیایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کیست آن طوطی و شکرضمیر منبع حکمت</p></div>
<div class="m2"><p>که حق باشد زبان او چو احمد وقت گویایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا در دل یکی دلبر همی‌گوید خمش بهتر</p></div>
<div class="m2"><p>که بس جان‌های نازک را کند این گفت سودایی</p></div></div>