---
title: >-
    غزل شمارهٔ ۲۹۹
---
# غزل شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>یا وصال یار باید یا حریفان را شراب</p></div>
<div class="m2"><p>چونک دریا دست ندهد پای نه در جوی آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن حریفان چو جان و باقیان جاودان</p></div>
<div class="m2"><p>در لطافت همچو آب و در سخاوت چون سحاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همرهان آب حیوان خضریان آسمان</p></div>
<div class="m2"><p>زندگی هر عمارت گنج‌های هر خراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب یار نور آمد این لطیف و آن ظریف</p></div>
<div class="m2"><p>هر دو غمازند لیکن نی ز کین بل ز احتساب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب اندر طشت و یا جو چون ز کف جنبان شود</p></div>
<div class="m2"><p>نور بر دیوار هم آغاز گیرد اضطراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرق جنسیت برادر جون قیامت می‌کند</p></div>
<div class="m2"><p>خود تو بنگر من خموشم و هوا علم بالصواب</p></div></div>