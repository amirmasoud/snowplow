---
title: >-
    غزل شمارهٔ ۱۵۲۹
---
# غزل شمارهٔ ۱۵۲۹

<div class="b" id="bn1"><div class="m1"><p>شب دوشینه ما بیدار بودیم</p></div>
<div class="m2"><p>همه خفتند و ما بر کار بودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریف غمزه غماز گشتیم</p></div>
<div class="m2"><p>ندیم طره طرار بودیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گرد نقطه خوبی و مستی</p></div>
<div class="m2"><p>به سر گردنده چون پرگار بودیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو چون دی زاده‌ای با تو چه گویم</p></div>
<div class="m2"><p>که با یار قدیمی یار بودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثال کاسه‌های لب شکسته</p></div>
<div class="m2"><p>به دکان شه جبار بودیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا چون جام شه زرین نباشیم</p></div>
<div class="m2"><p>چو اندر مخزن اسرار بودیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا خود کف ما دریا نباشد</p></div>
<div class="m2"><p>چو اندر قعر دریابار بودیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمش باش و دو عالم را به گفت آر</p></div>
<div class="m2"><p>کز اول گفت بی‌گفتار بودیم</p></div></div>