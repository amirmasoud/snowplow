---
title: >-
    غزل شمارهٔ ۲۳۲۳
---
# غزل شمارهٔ ۲۳۲۳

<div class="b" id="bn1"><div class="m1"><p>ای دل تو بگو هستم چون ماهی بر تابه</p></div>
<div class="m2"><p>کاستیزه همی‌گیرد او را مگر از لابه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی تو بنال ای دل زیرا که من مسکین</p></div>
<div class="m2"><p>بی‌صورت او هستم چون صورت گرمابه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد خانه چو زندانم شب خواب نمی‌دانم</p></div>
<div class="m2"><p>تا او نشود با من همخانه و همخوابه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن تو و عشق من در شهر شده شهره</p></div>
<div class="m2"><p>برداشته هر مطرب آن بر دف و شبابه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای در هوست غرقه هم صوفی و هم خرقه</p></div>
<div class="m2"><p>هم بنده بیچاره هم خواجه نسابه</p></div></div>