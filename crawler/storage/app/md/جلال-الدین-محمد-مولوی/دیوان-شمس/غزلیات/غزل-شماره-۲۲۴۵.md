---
title: >-
    غزل شمارهٔ ۲۲۴۵
---
# غزل شمارهٔ ۲۲۴۵

<div class="b" id="bn1"><div class="m1"><p>مطرب مهتاب رو آنچ شنیدی بگو</p></div>
<div class="m2"><p>ما همگان محرمیم آنچ بدیدی بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شه و سلطان ما ای طربستان ما</p></div>
<div class="m2"><p>در حرم جان ما بر چه رسیدی بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگس خمار او ای که خدا یار او</p></div>
<div class="m2"><p>دوش ز گلزار او هر چه بچیدی بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شده از دست من چون دل سرمست من</p></div>
<div class="m2"><p>ای همه را دیده تو آنچ گزیدی بگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عید بیاید رود عید تو ماند ابد</p></div>
<div class="m2"><p>کز فلک بی‌مدد چون برهیدی بگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شکرستان جان غرقه شدم ای شکر</p></div>
<div class="m2"><p>زین شکرستان اگر هیچ چشیدی بگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کشدم می به چپ می‌کشدم دل به راست</p></div>
<div class="m2"><p>رو که کشاکش خوش است تو چه کشیدی بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می به قدح ریختی فتنه برانگیختی</p></div>
<div class="m2"><p>کوی خرابات را تو چه کلیدی بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شور خرابات ما نور مناجات ما</p></div>
<div class="m2"><p>پرده حاجات ما هم تو دریدی بگو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماه به ابر اندرون تیره شده‌ست و زبون</p></div>
<div class="m2"><p>ای مه کز ابرها پاک و بعیدی بگو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظل تو پاینده باد ماه تو تابنده باد</p></div>
<div class="m2"><p>چرخ تو را بنده باد از چه رمیدی بگو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق مرا گفت دی عاشق من چون شدی</p></div>
<div class="m2"><p>گفتم بر چون متن ز آنچ تنیدی بگو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرد مجاهد بدم عاقل و زاهد بدم</p></div>
<div class="m2"><p>عافیتا همچو مرغ از چه پریدی بگو</p></div></div>