---
title: >-
    غزل شمارهٔ ۱۰۳۷
---
# غزل شمارهٔ ۱۰۳۷

<div class="b" id="bn1"><div class="m1"><p>گیرم که بود میر تو را زر به خروار</p></div>
<div class="m2"><p>رخساره چون زر ز کجا یابد زردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دلشده زار چو زاری بشنیدند</p></div>
<div class="m2"><p>از خاک برآمد به تماشا گل و گلزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هین جامه بکن زود در این حوض فرورو</p></div>
<div class="m2"><p>تا بازرهی از سر و از غصه دستار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما نیز چو تو منکر این غلغله بودیم</p></div>
<div class="m2"><p>گشتیم به یک غمزه چنین سغبه دلدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی شکنی عاشق خود را تو ز غیرت</p></div>
<div class="m2"><p>هل تا دو سه ناله بکند این دل بیمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی نی مهلش زانک از آن ناله زارش</p></div>
<div class="m2"><p>نی خلق زمین ماند و نی چرخه دوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز عجب نیست اگر فاش نگردد</p></div>
<div class="m2"><p>آن عالم مستور به دستوری ستار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز این دل دیوانه ز زنجیر برون جست</p></div>
<div class="m2"><p>بدرید گریبان خود از عشق دگربار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خامش که اشارت ز شه عشق چنین است</p></div>
<div class="m2"><p>کز صبر گلوی دل و جان گیر و بیفشار</p></div></div>