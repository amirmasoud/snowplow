---
title: >-
    غزل شمارهٔ ۱۳۸۱
---
# غزل شمارهٔ ۱۳۸۱

<div class="b" id="bn1"><div class="m1"><p>هرگز ندانم راندن مستی که افتد بر درم</p></div>
<div class="m2"><p>در خانه گر می باشدم پیشش نهم با وی خورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی که شد مهمان من جان منست و آن من</p></div>
<div class="m2"><p>تاج من و سلطان من تا برنشیند بر سرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای یار من وی خویش من مستی بیاور پیش من</p></div>
<div class="m2"><p>روزی که مستی کم کنم از عمر خویشش نشمرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون وقف کردستم پدر بر باده‌های همچو زر</p></div>
<div class="m2"><p>در غیر ساقی ننگرم وز امر ساقی نگذرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند آزمایم خویش را وین جان عقل اندیش را</p></div>
<div class="m2"><p>روزی که مستم کشتیم روزی که عاقل لنگرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو خمر تن کو خمر جان کو آسمان کو ریسمان</p></div>
<div class="m2"><p>تو مست جام ابتری من مست حوض کوثرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی بیاید قی کند مستی زمین را طی کند</p></div>
<div class="m2"><p>این خوار و زار اندر زمین وان آسمان بر محترم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مستی و روشن روان امشب مخسب ای ساربان</p></div>
<div class="m2"><p>خاموش کن خاموش کن زین باده نوش ای بوالکرم</p></div></div>