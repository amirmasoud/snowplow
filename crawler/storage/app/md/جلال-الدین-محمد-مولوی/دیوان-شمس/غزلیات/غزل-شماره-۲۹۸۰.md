---
title: >-
    غزل شمارهٔ ۲۹۸۰
---
# غزل شمارهٔ ۲۹۸۰

<div class="b" id="bn1"><div class="m1"><p>آن لحظه کآفتاب و چراغ جهان شوی</p></div>
<div class="m2"><p>اندر جهان مرده درآیی و جان شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر دو چشم کور درآیی نظر دهی</p></div>
<div class="m2"><p>و اندر دهان گنگ درآیی زبان شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیو زشت درروی و یوسفش کنی</p></div>
<div class="m2"><p>و اندر نهاد گرگ درآیی شبان شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز سر برآری از چارطاق نو</p></div>
<div class="m2"><p>چون رو بدان کنند از آن جا نهان شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی چو بوی گل مدد مغزها شوی</p></div>
<div class="m2"><p>گاهی انیس دیده شوی گلستان شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرزین کژروی و رخ راست رو شها</p></div>
<div class="m2"><p>در لعب کس نداند تا خود چه سان شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو رو ورق بگردان ای عشق بی‌نشان</p></div>
<div class="m2"><p>بر یک ورق قرار نمایی نشان شوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عدل دوست محو شو ای دل به وقت غم</p></div>
<div class="m2"><p>هم محو لطف او شو چون شادمان شوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آبی که محو کل شد او نیز کل شود</p></div>
<div class="m2"><p>هم تو صفات پاک شوی گر چنان شوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن بانگ چنگ را چو هوا هر طرف بری</p></div>
<div class="m2"><p>و آن سوز قهر را تو گوا چون دخان شوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای عشق این همه بشوی و تو پاک از این</p></div>
<div class="m2"><p>بی صورتی چو خشم اگر چه سنان شوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این دم خموش کرده‌ای و من خمش کنم</p></div>
<div class="m2"><p>آنگه بیان کنم که تو نطق و بیان شوی</p></div></div>