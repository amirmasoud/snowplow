---
title: >-
    غزل شمارهٔ ۱۷۶۲
---
# غزل شمارهٔ ۱۷۶۲

<div class="b" id="bn1"><div class="m1"><p>آمدستیم تا چنان گردیم</p></div>
<div class="m2"><p>که چو خورشید جمله جان گردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مونس و یار غمگنان باشیم</p></div>
<div class="m2"><p>گل و گلزار خاکیان گردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند کس را نییم خاص چو زر</p></div>
<div class="m2"><p>بر همه همچو بحر و کان گردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان نماییم جسم عالم را</p></div>
<div class="m2"><p>قره العین دیدگان گردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زمین نیستیم یغماگاه</p></div>
<div class="m2"><p>ایمن و خوش چو آسمان گردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کی ترسان بود چو ترسایان</p></div>
<div class="m2"><p>همچو ایمان بر او امان گردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هین خمش کن از آن هم افزونیم</p></div>
<div class="m2"><p>که بر الفاظ و بر زبان گردیم</p></div></div>