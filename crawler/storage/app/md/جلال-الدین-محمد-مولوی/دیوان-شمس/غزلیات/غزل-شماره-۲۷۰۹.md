---
title: >-
    غزل شمارهٔ ۲۷۰۹
---
# غزل شمارهٔ ۲۷۰۹

<div class="b" id="bn1"><div class="m1"><p>دلاراما چنین زیبا چرایی</p></div>
<div class="m2"><p>چنین چست و چنین رعنا چرایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتم من که جانی و جهانی</p></div>
<div class="m2"><p>چنین جان و جهان آرا چرایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتم من که الیاسی و خضری</p></div>
<div class="m2"><p>چو آب خضر عمرافزا چرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفتم من که دنیایی و دینی</p></div>
<div class="m2"><p>چو دنیا مایه سودا چرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفتم گنج قارونی به خوبی</p></div>
<div class="m2"><p>چو موسی با ید بیضا چرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز رشکت دوست خون دوست ریزد</p></div>
<div class="m2"><p>بدین حد شنگ و سرغوغا چرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نور تو گرفت از قاف تا قاف</p></div>
<div class="m2"><p>نهان از دیده چون عنقا چرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد هیچ حلوا طبع صهبا</p></div>
<div class="m2"><p>تو هم حلوا و هم صهبا چرایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز عشق گفت تو با خود بجنگم</p></div>
<div class="m2"><p>که پیش چون ویی گویا چرایی</p></div></div>