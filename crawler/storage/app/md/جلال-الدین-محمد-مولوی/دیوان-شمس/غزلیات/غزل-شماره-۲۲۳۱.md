---
title: >-
    غزل شمارهٔ ۲۲۳۱
---
# غزل شمارهٔ ۲۲۳۱

<div class="b" id="bn1"><div class="m1"><p>می‌دوید از هر طرف در جست و جو</p></div>
<div class="m2"><p>چشم پرخون تیغ در کف عشق او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش خفته خلق اندر خواب خوش</p></div>
<div class="m2"><p>او به قصد جان عاشق سو به سو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه چون مه تافته بر بام‌ها</p></div>
<div class="m2"><p>گاه چون باد صبا او کو به کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگهان افکند طشت ما ز بام</p></div>
<div class="m2"><p>پاسبانان درشده در گفت و گو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میان کوی بانگ دزد خاست</p></div>
<div class="m2"><p>او بزد زخمی و پنهان کرد رو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد او را پاسبانی درنیافت</p></div>
<div class="m2"><p>کش زبون گشته‌ست چرخ تندخو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر زخم آمد افلاطون عقل</p></div>
<div class="m2"><p>کو نشان‌ها را بداند مو به مو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت دانستم که زخم دست کیست</p></div>
<div class="m2"><p>کو است اصل فتنه‌های تو به تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونک زخم او است نبود چاره‌ای</p></div>
<div class="m2"><p>آنچ او بشکافت نپذیرد رفو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پی این زخم جان نو رسید</p></div>
<div class="m2"><p>جان کهنه دست‌ها از خود بشو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق شمس الدین تبریزی است این</p></div>
<div class="m2"><p>کو برون است از جهان رنگ و بو</p></div></div>