---
title: >-
    غزل شمارهٔ ۲۶۴
---
# غزل شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>لی حبیب حبه یشوی الحشا</p></div>
<div class="m2"><p>لو یشا یمشی علی عینی مشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز آن باشد که روزیم او بود</p></div>
<div class="m2"><p>ای خوشا آن روز و روزی ای خوشا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چه باشد کو کند کان نیست خوش</p></div>
<div class="m2"><p>قد رضینا یفعل الله ما یشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار او سرمایه گل‌ها بود</p></div>
<div class="m2"><p>انه المنان فی کشف الغشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه گفتی یا شنیدی پوست بود</p></div>
<div class="m2"><p>لیس لب العشق سرا قد فشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی به قشر پوست‌ها قانع شود</p></div>
<div class="m2"><p>ذو لباب فی التجلی قد نشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خمش کردم غمش خامش نکرد</p></div>
<div class="m2"><p>عافنا من شر واش قد وشا</p></div></div>