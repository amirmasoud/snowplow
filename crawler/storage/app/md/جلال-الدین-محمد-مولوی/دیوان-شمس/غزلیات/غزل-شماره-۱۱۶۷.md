---
title: >-
    غزل شمارهٔ ۱۱۶۷
---
# غزل شمارهٔ ۱۱۶۷

<div class="b" id="bn1"><div class="m1"><p>مست توام نه از می و نه از کوکنار</p></div>
<div class="m2"><p>وقت کنارست بیا گو کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برجه مستانه کناری بگیر</p></div>
<div class="m2"><p>چون شجر و باد به وقت بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاخ تر از باد کناری چو یافت</p></div>
<div class="m2"><p>رقص درآمد چو من بی‌قرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این خبر افتاد به خوبان غیب</p></div>
<div class="m2"><p>تا برسیدند هزاران نگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاله رخ افروخته از که رسید</p></div>
<div class="m2"><p>سنبله پا به گل از مرغزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوسن با تیغ و سمن با سپر</p></div>
<div class="m2"><p>سبزه پیادست و گل تر سوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فندق و خشخاش به دست آمده</p></div>
<div class="m2"><p>نعنع و حلبو به لب جویبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جدول هر گونه حویجی جدا</p></div>
<div class="m2"><p>تا مددی یابد از یار یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرده دکان‌ها همه حلواییان</p></div>
<div class="m2"><p>پرشکر و فستق از بهر کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میوه فروشان همه با طبل‌ها</p></div>
<div class="m2"><p>بر سر هر پشته فشانده ثمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک ز گل گوی که همرنگ اوست</p></div>
<div class="m2"><p>جمله ز بو گو که پریست یار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلبل و قمری و دو صد نوع مرغ</p></div>
<div class="m2"><p>جانب باغ آمده قادم یزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌زندم نرگس چشمک خموش</p></div>
<div class="m2"><p>خطبه مرغان چمن گوش دار</p></div></div>