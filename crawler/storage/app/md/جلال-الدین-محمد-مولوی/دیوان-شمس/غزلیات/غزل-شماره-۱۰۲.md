---
title: >-
    غزل شمارهٔ ۱۰۲
---
# غزل شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>سلیمانا بیار انگشتری را</p></div>
<div class="m2"><p>مطیع و بنده کن دیو و پری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآر آواز ردوها علی</p></div>
<div class="m2"><p>منور کن سرای شش دری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآوردن ز مغرب آفتابی</p></div>
<div class="m2"><p>مسلم شد ضمیر آن سری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین سان مهتری یابد هر آن کس</p></div>
<div class="m2"><p>که بهر حق گذارد مهتری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنه بر خوان جفان کالجوابی</p></div>
<div class="m2"><p>مکرم کن نیاز مشتری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کاسی کاسه سر را طرب ده</p></div>
<div class="m2"><p>تو کن مخمور چشم عبهری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز صورت‌های غیبی پرده بردار</p></div>
<div class="m2"><p>کسادی ده نقوش آزری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چاه و آب چه رنجور گشتیم</p></div>
<div class="m2"><p>روان کن چشمه‌های کوثری را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلا در بزم شاهنشاه دررو</p></div>
<div class="m2"><p>پذیرا شو شراب احمری را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زر و زن را به جان مپرست زیرا</p></div>
<div class="m2"><p>بر این دو دوخت یزدان کافری را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهاد نفس کن زیرا که اجری</p></div>
<div class="m2"><p>برای این دهد شه لشکری را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل سیمین بری کز عشق رویش</p></div>
<div class="m2"><p>ز حیرت گم کند زر هم زری را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان دریادلی کز جوش و نوشش</p></div>
<div class="m2"><p>به دست آورد گوهر گوهری را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که باقی غزل را تو بگویی</p></div>
<div class="m2"><p>به رشک آری تو سحر سامری را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خمش کردم که پایم گل فرورفت</p></div>
<div class="m2"><p>تو بگشا پر نطق جعفری را</p></div></div>