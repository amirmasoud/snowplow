---
title: >-
    غزل شمارهٔ ۲۵۶۰
---
# غزل شمارهٔ ۲۵۶۰

<div class="b" id="bn1"><div class="m1"><p>الا ای جان قدس آخر به سوی من نمی‌آیی</p></div>
<div class="m2"><p>هماره جان به تن آید تو سوی تن نمی‌آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدم دامن کشان تا تو ز من دامن کشیدستی</p></div>
<div class="m2"><p>ز اشک خون همی‌ریزم در این دامن نمی‌آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی بی‌آبی جانم چو نیسانت نمی‌بارد</p></div>
<div class="m2"><p>زهی خرمن که سوی این سیه خرمن نمی‌آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دورم زان نظر کردن نظاره عالمی گشتم</p></div>
<div class="m2"><p>نظاره من بیا گر تو نظر کردن نمی‌آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الا ای دل پری خوانی نگویی آن پری را تو</p></div>
<div class="m2"><p>چرا خوابم ببردی گر به سحر و فن نمی‌آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الا ای طوق وصل او که در گردن همی‌زیبی</p></div>
<div class="m2"><p>چو قمری ناله می‌دارم که در گردن نمی‌آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل تو همچو سنگ و من چو آهن ثابت اندر عشق</p></div>
<div class="m2"><p>ایا آهن ربا آخر سوی آهن نمی‌آیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ما و من برست آن کس که تو رویی بدو آری</p></div>
<div class="m2"><p>چرا تو سوی این هجران صد چون من نمی‌آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فزایش از کجا باشد بهارا چون نمی‌باری</p></div>
<div class="m2"><p>سکونت از کجا آخر سوی مسکن نمی‌آیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الا ای نور غایب بین در این دیده نمی‌تابی</p></div>
<div class="m2"><p>الا ای ناطقه کلی بدین الکن نمی‌آیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ارزن خرد گشتستم ز بهر مرغ مژده آور</p></div>
<div class="m2"><p>الا ای مرغ مژده آور بدین ارزن نمی‌آیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه جان‌ها شده لرزان در این مکمن گه هجران</p></div>
<div class="m2"><p>برای امن این جان‌ها در این مکمن نمی‌آیی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبان چون سوسن تازه به مدحت ای خوش آوازه</p></div>
<div class="m2"><p>الا گلزار ربانی بدین سوسن نمی‌آیی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>الا ای باده شادان به عشق اندر چو استادان</p></div>
<div class="m2"><p>درونت خنب سرمستی چرا از دن نمی‌آیی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>معاش خانه جانم اگر نه از قرص خورشید است</p></div>
<div class="m2"><p>چرا ای خانه بی‌خورشید تو روشن نمی‌آیی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر نه طالب اویی به خانه خانه خورشید</p></div>
<div class="m2"><p>چرا چون شکل شب دزدان به هر روزن نمی‌آیی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو صحرای جمال او برای جان بود مؤمن</p></div>
<div class="m2"><p>چرا در خوف می‌باشی چرامؤمننمی‌آیی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو بشکن جوز این تن را بکوب این مغز را درهم</p></div>
<div class="m2"><p>چرا اندر چراغ عشق چون روغن نمی‌آیی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو آب و روغنی کردی به نورت ره کجا باشد</p></div>
<div class="m2"><p>مبر تو آب بی‌روغن که بی‌دشمن نمی‌آیی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه نقد پاک می‌دانی تو خود را وین نمی‌بینی</p></div>
<div class="m2"><p>که اندر دست خود ماندی و در مخزن نمی‌آیی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز عشق شمس تبریزی چو موسی گفته‌ام ارنی</p></div>
<div class="m2"><p>ز سوی طور تبریزی چرا چون لن نمی‌آیی</p></div></div>