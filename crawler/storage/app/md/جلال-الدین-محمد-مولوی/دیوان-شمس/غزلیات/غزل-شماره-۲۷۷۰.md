---
title: >-
    غزل شمارهٔ ۲۷۷۰
---
# غزل شمارهٔ ۲۷۷۰

<div class="b" id="bn1"><div class="m1"><p>رخ‌ها بنگر تو زعفرانی</p></div>
<div class="m2"><p>کز درد همی‌دهد نشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهری بنگر ز درد رنجور</p></div>
<div class="m2"><p>چون باغ به موسم خزانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این درد ز غصه فراق است</p></div>
<div class="m2"><p>از هیبت حکم آسمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیم است فلک سیاه گردد</p></div>
<div class="m2"><p>از آتش و ناله نهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوزخ بنگر که سر برآورد</p></div>
<div class="m2"><p>ناگه ز میان شادمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخاست غریو جان ز هر سو</p></div>
<div class="m2"><p>هان ای کس بی‌کسان تو دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرمود که این فراق فانی است</p></div>
<div class="m2"><p>افغان ز فراق جاودانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا رب چه شود اگر تو ما را</p></div>
<div class="m2"><p>از هر دو فراق وارهانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این گفته و بسته شد دهانم</p></div>
<div class="m2"><p>باقی تو بگو اگر توانی</p></div></div>