---
title: >-
    غزل شمارهٔ ۲۸۳۸
---
# غزل شمارهٔ ۲۸۳۸

<div class="b" id="bn1"><div class="m1"><p>صفت خدای داری چو به سینه‌ای درآیی</p></div>
<div class="m2"><p>لمعان طور سینا تو ز سینه وانمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفت چراغ داری چو به خانه شب درآیی</p></div>
<div class="m2"><p>همه خانه نور گیرد ز فروغ روشنایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفت شراب داری تو به مجلسی که باشی</p></div>
<div class="m2"><p>دو هزار شور و فتنه فکنی ز خوش لقایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو طرب رمیده باشد چو هوس پریده باشد</p></div>
<div class="m2"><p>چه گیاه و گل بروید چو تو خوش کنی سقایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جهان فسرده باشد چو نشاط مرده باشد</p></div>
<div class="m2"><p>چه جهان‌های دیگر که ز غیب برگشایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تو است این تقاضا به درون بی‌قراران</p></div>
<div class="m2"><p>و اگر نه تیره گل را به صفا چه آشنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلکی به گرد خاکی شب و روز گشته گردان</p></div>
<div class="m2"><p>فلکا ز ما چه خواهی نه تو معدن ضیایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفسی سرشک ریزی نفسی تو خاک بیزی</p></div>
<div class="m2"><p>نه قراضه جویی آخر همه کان و کیمیایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مثل قراضه جویان شب و روز خاک بیزی</p></div>
<div class="m2"><p>ز چه خاک می‌پرستی نه تو قبله دعایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه عجب اگر گدایی ز شهی عطا بجوید</p></div>
<div class="m2"><p>عجب این که پادشاهی ز گدا کند گدایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و عجبتر اینک آن شه به نیاز رفت چندان</p></div>
<div class="m2"><p>که گدا غلط درافتد که مراست پادشاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فلکا نه پادشاهی نه که خاک بنده توست</p></div>
<div class="m2"><p>تو چرا به خدمت او شب و روز در هوایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلکم جواب گوید که کسی تهی نپوید</p></div>
<div class="m2"><p>که اگر کهی بپرد بود آن ز کهربایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخنم خور فرشته‌ست من اگر سخن نگویم</p></div>
<div class="m2"><p>ملک گرسنه گوید که بگو خمش چرایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو نه از فرشتگانی خورش ملک چه دانی</p></div>
<div class="m2"><p>چه کنی ترنگبین را تو حریف گندنایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو چه دانی این ابا را که ز مطبخ دماغ است</p></div>
<div class="m2"><p>که خدا کند در آن جا شب و روز کدخدایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تبریز شمس دین را تو بگو که رو به ما کن</p></div>
<div class="m2"><p>غلطم بگو که شمسا همه روی بی‌قفایی</p></div></div>