---
title: >-
    غزل شمارهٔ ۲۱۷
---
# غزل شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>چه نیکبخت کسی که خدای خواند تو را</p></div>
<div class="m2"><p>درآ درآ به سعادت درت گشاد خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که برگشاید درها مفتح الابواب</p></div>
<div class="m2"><p>که نزل و منزل بخشید نحن نزلنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که دانه را بشکافد ندا کند به درخت</p></div>
<div class="m2"><p>که سر برآر به بالا و می فشان خرما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دردمید در آن نی که بود زیر زمین</p></div>
<div class="m2"><p>که گشت مادر شیرین و خسرو حلوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی کرد در کف کان خاک را زر و نقره</p></div>
<div class="m2"><p>کی کرد در صدفی آب را جواهرها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جان و تن برهیدی به جذبه جانان</p></div>
<div class="m2"><p>ز قاب و قوس گذشتی به جذب او ادنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم آفتاب شده مطربت که خیز سجود</p></div>
<div class="m2"><p>به سوی قامت سروی ز دست لاله صلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین بلند چرا می‌پرد همای ضمیر</p></div>
<div class="m2"><p>شنید بانگ صفیری ز ربی الاعلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل شکفته بگویم که از چه می‌خندد</p></div>
<div class="m2"><p>که مستجاب شد او را از آن بهار دعا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بوی یوسف معنی گل از گریبان یافت</p></div>
<div class="m2"><p>دهان گشاد به خنده که‌های یا بشرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دی بگوید گلشن که هر چه خواهی کن</p></div>
<div class="m2"><p>به فر عدل شهنشه نترسم از یغما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آسمان و زمین در کفش کم از سیبی‌ست</p></div>
<div class="m2"><p>تو برگ من بربایی کجا بری و کجا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو اوست معنی عالم به اتفاق همه</p></div>
<div class="m2"><p>بجز به خدمت معنی کجا روند اسما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد اسم مظهر معنی کاردت ان اعرف</p></div>
<div class="m2"><p>وز اسم یافت فراغت بصیرت عرفا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کلیم را بشناسد به معرفت‌هارون</p></div>
<div class="m2"><p>اگر عصاش نباشد وگر ید بیضا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چگونه چرخ نگردد بگرد بام و درش</p></div>
<div class="m2"><p>که آفتاب و مه از نور او کنند سخا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو نور گفت خداوند خویشتن را نام</p></div>
<div class="m2"><p>غلام چشم شو ایرا ز نور کرد چرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از این همه بگذشتم نگاه دار تو دست</p></div>
<div class="m2"><p>که می‌خرامد از آن پرده مست یوسف ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه جای دست بود عقل و هوش شد از دست</p></div>
<div class="m2"><p>که ساقی‌ست دلارام و باده اش گیرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خموش باش که تا شرح این همو گوید</p></div>
<div class="m2"><p>که آب و تاب همان به که آید از بالا</p></div></div>