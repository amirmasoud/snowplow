---
title: >-
    غزل شمارهٔ ۷۸۱
---
# غزل شمارهٔ ۷۸۱

<div class="b" id="bn1"><div class="m1"><p>در دلم چون غمت ای سرو روان برخیزد</p></div>
<div class="m2"><p>همچو سرو این تن من بی‌دل و جان برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من گمانم تو عیان پیش تو من محو به هم</p></div>
<div class="m2"><p>چون عیان جلوه کند چهره گمان برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رسد سنجق تو در ستمستان جهان</p></div>
<div class="m2"><p>ظلم کوته شود و کوچ و قلان برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر حصار فلک ار خوبی تو جمله برد</p></div>
<div class="m2"><p>از مقیمان فلک بانگ امان برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذر از باغ جهان یک سحر ای رشک بهار</p></div>
<div class="m2"><p>تا ز گلزار چمن رسم خزان برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت افلاک خمیدست از این بار گران</p></div>
<div class="m2"><p>ز سبک روحی تو بار گران برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من چو از تیر توم بال و پرم ده بپران</p></div>
<div class="m2"><p>خوش پرد تیر زمانی که کمان برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رمه خفتست و همی‌گردد گرگ از چپ و راست</p></div>
<div class="m2"><p>سگ ما بانگ زند تا که شبان برخیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هین خمش دل پنهانست چو رگ زیر زبان</p></div>
<div class="m2"><p>آشکارا شود آن رگ چو زبان برخیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این مجابات مجیرست در آن قطعه که گفت</p></div>
<div class="m2"><p>بر سر کوی تو عقل از سر جان برخیزد</p></div></div>