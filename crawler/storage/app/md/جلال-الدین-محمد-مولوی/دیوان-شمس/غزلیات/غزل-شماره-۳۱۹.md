---
title: >-
    غزل شمارهٔ ۳۱۹
---
# غزل شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>امسی و اصبح بالجوی اتعذب</p></div>
<div class="m2"><p>قلبی علی نار الهوی یتقلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ان کنت تهجرنی تهذبنی به</p></div>
<div class="m2"><p>انت النهی و بلاک لا اتهذب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما بال قلبک قد قسی فالی متی</p></div>
<div class="m2"><p>ابکی و مما قد جری اتعتب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مما احب بان اقول فدیتکم</p></div>
<div class="m2"><p>احیی بکم و قتیلکم اتلقب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و اشرتم بالصبر لی متسلیا</p></div>
<div class="m2"><p>ما هکذی عشقوا به لا تحسبوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما عشت فی هذا الفراق سویعه</p></div>
<div class="m2"><p>لو لا لقاؤک کل یوم ارقب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انی اتوب مناجیا و منادیا</p></div>
<div class="m2"><p>فانا المسی بسیدی و المذنب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تبریز جل به شمس دین سیدی</p></div>
<div class="m2"><p>ابکی دما مما جنیت و اشرب</p></div></div>