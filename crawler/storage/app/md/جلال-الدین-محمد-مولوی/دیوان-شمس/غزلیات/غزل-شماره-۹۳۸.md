---
title: >-
    غزل شمارهٔ ۹۳۸
---
# غزل شمارهٔ ۹۳۸

<div class="b" id="bn1"><div class="m1"><p>سخن به نزد سخندان بزرگوار بود</p></div>
<div class="m2"><p>ز آسمان سخن آمد سخن نه خوار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن چو نیک نگویی هزار نیست یکی</p></div>
<div class="m2"><p>سخن چو نیکو گویی یکی هزار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن ز پرده برون آید آن گهش بینی</p></div>
<div class="m2"><p>که او صفات خداوند کردگار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن چو روی نماید خدای رشک برد</p></div>
<div class="m2"><p>خنک کسی که به گفتار رازدار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عرش تا به ثری ذره ذره گویااند</p></div>
<div class="m2"><p>که داند آنک به ادراک عرش وار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن ز علم خدا و عمل خدای کند</p></div>
<div class="m2"><p>وگر ز ما طلبی کار کار کار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مرغکان ابابیل لشکری شکنند</p></div>
<div class="m2"><p>به پیش لشکر پنهان چه کارزار بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پشه سر شاهی برد که نمرودست</p></div>
<div class="m2"><p>یقین شود که نهان در سلاحدار بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو یک سواره مه را سپر دو نیم شود</p></div>
<div class="m2"><p>سنان دیده احمد چه دلگذار بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو صورتی طلبی زین سخن که دست نهی</p></div>
<div class="m2"><p>دهم به دست تو گر دست دستیار بود</p></div></div>