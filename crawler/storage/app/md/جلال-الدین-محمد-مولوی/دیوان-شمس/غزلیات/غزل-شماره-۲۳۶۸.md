---
title: >-
    غزل شمارهٔ ۲۳۶۸
---
# غزل شمارهٔ ۲۳۶۸

<div class="b" id="bn1"><div class="m1"><p>ای به میدان‌های وحدت گوی شاهی باخته</p></div>
<div class="m2"><p>جمله را عریان بدیده کس تو را نشناخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل کل کژچشم گشته از کمال غیرتت</p></div>
<div class="m2"><p>وز کژی پنداشته کو مر تو را انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای چراغ و چشم عالم در جهان فرد آمدی</p></div>
<div class="m2"><p>تا در اسرار جهان تو صد جهان پرداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که طاووس بهار از عشق رویت جلوه گر</p></div>
<div class="m2"><p>بر درخت جسم جان نالان شده چون فاخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از برای ما تو آتش را چو گلشن داشته</p></div>
<div class="m2"><p>وز برای ما تو دریا را چو کشتی ساخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمس تبریزی جهان را چون تو پر کردی ز حسن</p></div>
<div class="m2"><p>من جهان روح را از غیر عشقت آخته</p></div></div>