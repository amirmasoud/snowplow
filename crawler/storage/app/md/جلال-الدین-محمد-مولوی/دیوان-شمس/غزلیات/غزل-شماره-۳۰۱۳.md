---
title: >-
    غزل شمارهٔ ۳۰۱۳
---
# غزل شمارهٔ ۳۰۱۳

<div class="b" id="bn1"><div class="m1"><p>یار در آخرزمان کرد طرب سازیی</p></div>
<div class="m2"><p>باطن او جد جد ظاهر او بازیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله عشاق را یار بدین علم کشت</p></div>
<div class="m2"><p>تا نکند هان و هان جهل تو طنازیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حرکت باش ازانک آب روان نفسرد</p></div>
<div class="m2"><p>کز حرکت یافت عشق سر سراندازیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنبش جان کی کند صورت گرمابه‌ای</p></div>
<div class="m2"><p>صف شکنی کی کند اسب گدا غازیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبل غزا کوفتند این دم پیدا شود</p></div>
<div class="m2"><p>جنبش پالانیی از فرس تازیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌زن و می‌خور چو شیر تا به شهادت رسی</p></div>
<div class="m2"><p>تا بزنی گردن کافر ابخازیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازی شیران مصاف بازی روبه گریز</p></div>
<div class="m2"><p>روبه با شیر حق کی کند انبازیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرم روان از کجا تیره دلان از کجا</p></div>
<div class="m2"><p>مروزیی اوفتاد در ره با رازیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق عجب غازییست زنده شود زو شهید</p></div>
<div class="m2"><p>سر بنه ای جان پاک پیش چنین غازیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرخ تن دل سیاه پر شود از نور ماه</p></div>
<div class="m2"><p>گر بکند قلب تو قالب پردازیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مطرب و سرنا و دف باده برآورده کف</p></div>
<div class="m2"><p>هر نفسی زان لطف آرد غمازیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خنک آن جان پاک کز سر میدان خاک</p></div>
<div class="m2"><p>گیرد زین قلبگاه قالب پردازیی</p></div></div>