---
title: >-
    غزل شمارهٔ ۴۷۹
---
# غزل شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>ستیزه کن که ز خوبان ستیزه شیرینست</p></div>
<div class="m2"><p>بهانه کن که بتان را بهانه آیینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن لب شکرینت بهانه‌های دروغ</p></div>
<div class="m2"><p>به جای فاتحه و کاف‌ها و یاسینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وفا طمع نکنم زانک جور خوبان را</p></div>
<div class="m2"><p>طبیعت است و سرشت است و عادت و دینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ترش کنی و رو ز ما بگردانی</p></div>
<div class="m2"><p>به قاصد است و به مکر است و آن دروغینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست غیر تو اندر دهان من حلوا</p></div>
<div class="m2"><p>به جان پاک عزیزان که گرز رویینست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار وعده ده آنگه خلاف کن همه را</p></div>
<div class="m2"><p>که آن سراب که ارزد صد آب خوش اینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر او دهد که رخش از فراق همچو زر است</p></div>
<div class="m2"><p>چرا دهد زر و سیم آن پری که سیمینست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواب همچو شکر او دهد که محتاج است</p></div>
<div class="m2"><p>جواب تلخ تو را صد هزار تمکینست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمال و حسن تو گنج است و خوی بد چون مار</p></div>
<div class="m2"><p>بقای گنج تو بادا که آن برونینست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قماش هستی ما را به ناز خویش بسوز</p></div>
<div class="m2"><p>که آن زکات لطیفت نصیب مسکینست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برون در همه را چون سگان کو بنشان</p></div>
<div class="m2"><p>که در شرف سر کوی تو طور سینینست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورند چوب خلیفه شهان چو شاه شوند</p></div>
<div class="m2"><p>جفای عشق کشیدن فن سلاطین است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امام فاتحه خواند ملک کند آمین</p></div>
<div class="m2"><p>مرا چو فاتحه خواندم امید آمینست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آن فریب کز اندیشه تو می‌زاید</p></div>
<div class="m2"><p>هزار گوهر و لعلش بها و کابینست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنانک مدرسه فقه را برون شوها است</p></div>
<div class="m2"><p>بدانک مدرسه عشق را قوانینست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خمش کنیم که تا شرح آن بگوید شاه</p></div>
<div class="m2"><p>که زنده شخص جهان زان گزیده تلقینست</p></div></div>