---
title: >-
    غزل شمارهٔ ۲۰۸
---
# غزل شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>از جهت ره زدن راه درآرد مرا</p></div>
<div class="m2"><p>تا به کف رهزنان بازسپارد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنک زند هر دمی راه دو صد قافله</p></div>
<div class="m2"><p>من چه زنم پیش او او به چه آرد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من سر و پا گم کنم دل ز جهان برکنم</p></div>
<div class="m2"><p>گر نفسی او به لطف سر بنخارد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او ره خوش می‌زند رقص بر آن می‌کنم</p></div>
<div class="m2"><p>هر دم بازی نو عشق برآرد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه به فسوس او مرا گوید کنجی نشین</p></div>
<div class="m2"><p>چونک نشینم به کنج خود به درآرد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اول امروزم او می‌بپراند چو باز</p></div>
<div class="m2"><p>تا که چه گیرد به من بر کی گمارد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت من همچو رعد نکته من همچو ابر</p></div>
<div class="m2"><p>قطره چکد ز ابر من چون بفشارد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابر من از بامداد دارد از آن بحر داد</p></div>
<div class="m2"><p>تا که ز رعد و ز باد بر کی ببارد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونک ببارد مرا یاوه ندارد مرا</p></div>
<div class="m2"><p>در کف صد گون نبات بازگذارد مرا</p></div></div>