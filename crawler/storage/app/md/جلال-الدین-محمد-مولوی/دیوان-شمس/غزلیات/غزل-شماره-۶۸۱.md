---
title: >-
    غزل شمارهٔ ۶۸۱
---
# غزل شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>کسی که غیر این سوداش نبود</p></div>
<div class="m2"><p>ز ذوق ماش یاد ماش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مثال گوی در میدان حیرت</p></div>
<div class="m2"><p>دوان باشد اگر چه پاش نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجودی که نرست از سایه خوش</p></div>
<div class="m2"><p>پناه سایه عنقاش نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماید آینه سیمای هر کس</p></div>
<div class="m2"><p>ازیرا صورت و سیماش نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روزی صد هزاران عیب و خوبی</p></div>
<div class="m2"><p>بگوید آینه غوغاش نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد آینه با زشت بغضی</p></div>
<div class="m2"><p>هوای چهره زیباش نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهانی زین شکر مجروح گردد</p></div>
<div class="m2"><p>که دندان‌های شکرخاش نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پرهای عجب دل برپریدی</p></div>
<div class="m2"><p>ولیک از دام او پرواش نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو چون مه پی خورشید می‌کاه</p></div>
<div class="m2"><p>که بی‌کاهش جمال افزاش نبود</p></div></div>