---
title: >-
    غزل شمارهٔ ۴۷۱
---
# غزل شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>پیش چنین ماه رو گیج شدن واجبست</p></div>
<div class="m2"><p>عشرت پروانه را شمع و لگن واجبست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست ز چنگ غمش گوش مرا کش مکش</p></div>
<div class="m2"><p>هر دمم از چنگ او تن تننن واجبست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلو دو چشم مرا گر چه که کم نیست آب</p></div>
<div class="m2"><p>مردمک دیده را چاه ذقن واجبست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلبر چون ماه را هر چه کند می‌رسد</p></div>
<div class="m2"><p>عاشق درگاه را خلق حسن واجبست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طره خویش ای نگار خوش به کف من سپار</p></div>
<div class="m2"><p>هر که در این چه فتاد داد رسن واجبست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق که شهر خوشیست این همه اغیار چیست</p></div>
<div class="m2"><p>حفظ چنین شهر را برج و بدن واجبست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمزه دزدیده را شحنه غم در پیست</p></div>
<div class="m2"><p>روشنی دیده را خوب ختن واجبست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق عیسی نه‌ای بی‌خور و خر کی زیی</p></div>
<div class="m2"><p>کالبد مرده را گور و کفن واجبست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مریم جان را مخاض برد به نخل و ریاض</p></div>
<div class="m2"><p>منقطع درد را نزل وطن واجبست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزل دل بارکش هست ملاقات خوش</p></div>
<div class="m2"><p>ناقه پرفاقه را شرب و عطن واجبست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لطف کن ای کان قند راه دهانم ببند</p></div>
<div class="m2"><p>اشتر سرمست را بند دهن واجبست</p></div></div>