---
title: >-
    غزل شمارهٔ ۴۷۶
---
# غزل شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>بخند بر همه عالم که جای خنده تو راست</p></div>
<div class="m2"><p>که بنده قد و ابروی تست هر کژ و راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتد به پای تو دولت نهد به پیش تو سر</p></div>
<div class="m2"><p>که آدمی و پری در ره تو بی‌سر و پاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پریر جان من از عشق سوی گلشن رفت</p></div>
<div class="m2"><p>تو را ندید به گلشن دمی نشست و نخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برون دوید ز گلشن چو آب سجده کنان</p></div>
<div class="m2"><p>که جویبار سعادت که اصل جاست کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو اهل دل ز دلم قصه تو بشنیدند</p></div>
<div class="m2"><p>ز جمله نعره برآمد که مست دلبر ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس آدمی و پری جمع گشت بر من و گفت</p></div>
<div class="m2"><p>بده ز شرق نشان‌ها که این دمت چو صباست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جفات نیز شکروار چاشنی دارد</p></div>
<div class="m2"><p>زهی جفا که در او صد هزار گنج وفاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قفا بداد و سفر کرد شمس تبریزی</p></div>
<div class="m2"><p>بگو مرا تو که خورشید را چه رو و قفاست</p></div></div>