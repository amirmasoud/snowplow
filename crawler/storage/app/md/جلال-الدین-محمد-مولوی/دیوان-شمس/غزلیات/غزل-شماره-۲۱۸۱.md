---
title: >-
    غزل شمارهٔ ۲۱۸۱
---
# غزل شمارهٔ ۲۱۸۱

<div class="b" id="bn1"><div class="m1"><p>دل و جان را طربگاه و مقام او</p></div>
<div class="m2"><p>شراب خم بی‌چون را قوام او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه عالم دهان خشکند و تشنه</p></div>
<div class="m2"><p>غذای جمله را داده تمام او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غذاها هم غذا جویند از وی</p></div>
<div class="m2"><p>که گندم را دهد آب از غمام او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدم چون اژدهای فتنه جویان</p></div>
<div class="m2"><p>ببسته فتنه را حلق و مسام او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سزای صد عتاب و صد عذابیم</p></div>
<div class="m2"><p>کشیده از سزای ما لگام او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حلم او جهان گستاخ گشته</p></div>
<div class="m2"><p>که گویی ما شهانیم و غلام او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای مغز مخموران عشقش</p></div>
<div class="m2"><p>بجوشیده به دست خود مدام او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشیده گوش هشیاران به مستی</p></div>
<div class="m2"><p>زهی اقبال و بخت مستدام او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیمبر را چو پرده کرده در پیش</p></div>
<div class="m2"><p>پس آن پرده می‌گوید پیام او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نکرده بندگان او را سلامی</p></div>
<div class="m2"><p>بر ایشان کرده از اول سلام او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه باشد گر شبی را زنده داری</p></div>
<div class="m2"><p>به عشق او که آرد صبح و شام او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر خامی‌کنی غافل بخسپی</p></div>
<div class="m2"><p>بنگذارد تو را ای دوست خام او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خردی تا کنون بس جا بخفتی</p></div>
<div class="m2"><p>کشانیدت ز پستی تا به بام او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خاکی تا به چالاکی کشیدت</p></div>
<div class="m2"><p>بدادت دانش و ناموس و نام او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مقامات نوت خواهد نمودن</p></div>
<div class="m2"><p>که تا خاصت کند ز انعام عام او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خردی هم ز مکتب می‌جهیدی</p></div>
<div class="m2"><p>چه نرمت کرد و پابرجا و رام او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خاکی و نباتی و به نطفه</p></div>
<div class="m2"><p>ستیزیدی درآوردت به دام او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز چندین ره به مهمانیت آورد</p></div>
<div class="m2"><p>نیاوردت برای انتقام او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به وقت درد می‌دانی که او او است</p></div>
<div class="m2"><p>به خاکی می‌دهد اویی به وام او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه اویان چو خاشاکی نمایند</p></div>
<div class="m2"><p>چو بوی خود فرستد در مشام او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سخن‌ها بانگ زنبوران نماید</p></div>
<div class="m2"><p>چو اندر گوش ما گوید کلام او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نماید چرخ بیت العنکبوتی</p></div>
<div class="m2"><p>چو بنماید مقام بی‌مقام او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه عالم گرفته‌ست آفتابی</p></div>
<div class="m2"><p>زهی کوری که می‌گوید کدام او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو درماند نگوید او جز او را</p></div>
<div class="m2"><p>چو بجهد هر خسی را کرده نام او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شکنجه بایدش زیرا که دزد است</p></div>
<div class="m2"><p>مقر ناید به نرمی‌و به کام او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو باری دزد خود را سیخ می‌زن</p></div>
<div class="m2"><p>چو می‌دانی که دزدیده‌ست جام او</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به یاری‌های شمس الدین تبریز</p></div>
<div class="m2"><p>شود بس مستخف و مستهام او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خمش از پارسی تازی بگویم</p></div>
<div class="m2"><p>فؤاد ما تسلیه المدام</p></div></div>