---
title: >-
    غزل شمارهٔ ۲۰۰۹
---
# غزل شمارهٔ ۲۰۰۹

<div class="b" id="bn1"><div class="m1"><p>نک بهاران شد صلا ای لولیان</p></div>
<div class="m2"><p>بانگ نای و سبزه و آب روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لولیان از شهر تن بیرون شوید</p></div>
<div class="m2"><p>لولیان را کی پذیرد خان و مان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران بردند حسرت زین جهان</p></div>
<div class="m2"><p>حسرتی بنهیم در جان جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با جهان بی‌وفا ما آن کنیم</p></div>
<div class="m2"><p>هرچ او کرده‌ست با آن دیگران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا حریف خود ببیند او یکی</p></div>
<div class="m2"><p>امتحان او بیابد امتحان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی غلط گفتم جهان چون عاشق است</p></div>
<div class="m2"><p>او به جان جوید جفای نیکوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان عاشق زنده از جور و جفاست</p></div>
<div class="m2"><p>ای مسلمان جان که را دارد زیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راه صحرا را فروبست این سخن</p></div>
<div class="m2"><p>کس نجوید راه صحرا را دهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو بگو دارد دهان تنگ یار</p></div>
<div class="m2"><p>با لب بسته گشاد بی‌کران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که بر وی آن لبان صحرا نشد</p></div>
<div class="m2"><p>او نه صحرا داند و نی آشیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که بر وی زان قمر نوری نتافت</p></div>
<div class="m2"><p>او چه بیند از زمین و آسمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کسی را کاین غزل صحرا شود</p></div>
<div class="m2"><p>عیش بیند زان سوی کون و مکان</p></div></div>