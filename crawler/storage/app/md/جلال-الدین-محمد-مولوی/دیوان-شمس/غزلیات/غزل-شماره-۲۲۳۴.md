---
title: >-
    غزل شمارهٔ ۲۲۳۴
---
# غزل شمارهٔ ۲۲۳۴

<div class="b" id="bn1"><div class="m1"><p>ای دیده من جمال خود اندر جمال تو</p></div>
<div class="m2"><p>آیینه گشته‌ام همه بهر خیال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و این طرفه‌تر که چشم نخسپد ز شوق تو</p></div>
<div class="m2"><p>گرمابه رفته هر سحری از وصال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاتون خاطرم که بزاید به هر دمی</p></div>
<div class="m2"><p>آبستن است لیک ز نور جلال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آبستن است نه مهه کی باشدش قرار</p></div>
<div class="m2"><p>او را خبر کجاست ز رنج و ملال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عشق اگر بجوشد خونم به غیر تو</p></div>
<div class="m2"><p>بادا به بی‌مرادی خونم حلال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر تا قدم ز عشق مرا شد زبان حال</p></div>
<div class="m2"><p>افغان به عرش برده و پرسان ز حال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر از عدم هزار جهان نو شود دگر</p></div>
<div class="m2"><p>بر صفحه جمال تو باشد چو خال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بس که غرقه‌ام چو مگس در حلاوتت</p></div>
<div class="m2"><p>پروا نباشدم به نظر در خصال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پیش شمس خسرو تبریز ای فلک</p></div>
<div class="m2"><p>می‌باش در سجود که این شد کمال تو</p></div></div>