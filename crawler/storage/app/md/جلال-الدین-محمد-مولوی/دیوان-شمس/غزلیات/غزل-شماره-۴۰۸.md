---
title: >-
    غزل شمارهٔ ۴۰۸
---
# غزل شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>آن شنیدی که خضر تخته کشتی بشکست</p></div>
<div class="m2"><p>تا که کشتی ز کف ظالم جبار برست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خضر وقت تو عشق است که صوفی ز شکست</p></div>
<div class="m2"><p>صافیست و مثل درد به پستی بنشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذت فقر چو باده‌ست که پستی جوید</p></div>
<div class="m2"><p>که همه عاشق سجده‌ست و تواضع سرمست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بدانی که تکبر همه از بی‌مزگیست</p></div>
<div class="m2"><p>پس سزای متکبر سر بی‌ذوق بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گریه شمع همه شب نه که از درد سرست</p></div>
<div class="m2"><p>چون ز سر رست همه نور شد از گریه برست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کف هستی ز سر خم مدمغ برود</p></div>
<div class="m2"><p>چون بگیرد قدح باده جان بر کف دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهیا هر چه تو را کام دل از بحر بجو</p></div>
<div class="m2"><p>طمع خام مکن تا نخلد کام ز شست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بحر می‌غرد و می‌گوید کای امت آب</p></div>
<div class="m2"><p>راست گویید بر این مایده کس را گله هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دم به دم بحر دل و امت او در خوش و نوش</p></div>
<div class="m2"><p>در خطابات و مجابات بلی‌اند و الست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نی در آن بزم کس از درد دلی سر بگرفت</p></div>
<div class="m2"><p>نی در آن باغ و چمن پای کس از خار بخست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هله خامش به خموشیت اسیران برهند</p></div>
<div class="m2"><p>ز خموشانه تو ناطق و خاموش بجست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لب فروبند چو دیدی که لب بسته یار</p></div>
<div class="m2"><p>دست شمشیرزنان را به چه تدبیر ببست</p></div></div>