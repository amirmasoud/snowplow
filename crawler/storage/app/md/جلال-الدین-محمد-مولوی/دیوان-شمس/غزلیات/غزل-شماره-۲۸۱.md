---
title: >-
    غزل شمارهٔ ۲۸۱
---
# غزل شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>یا ساقی المدامه حی علی الصلا</p></div>
<div class="m2"><p>املا زجاجنا بحمیا فقد خلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جسمی زجاجتی و محیاک قهوتی</p></div>
<div class="m2"><p>یا کامل الملاحه و اللطف و العلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما فاز عاشق بمحیاک ساعه</p></div>
<div class="m2"><p>الا و فی الصدود تلاشی من البلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الموت فی لقائک یا بدر طیب</p></div>
<div class="m2"><p>حاشاک بل لقاؤک امن من البلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لما تلا هواک صفاتا لمهجتی</p></div>
<div class="m2"><p>فیها حمائم یتلقین ما تلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسقیتنی المدامه من طرفک البهی</p></div>
<div class="m2"><p>حتی جلا فؤادی من احسن الجلا</p></div></div>