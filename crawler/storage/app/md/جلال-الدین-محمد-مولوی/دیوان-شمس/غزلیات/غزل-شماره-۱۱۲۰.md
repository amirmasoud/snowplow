---
title: >-
    غزل شمارهٔ ۱۱۲۰
---
# غزل شمارهٔ ۱۱۲۰

<div class="b" id="bn1"><div class="m1"><p>مستیم و بیخودیم و جمال تو پرده در</p></div>
<div class="m2"><p>زین پس مباش ماها در ابر و پرده در</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما جمع عاشقان تو خوش قد و قامتیم</p></div>
<div class="m2"><p>ما را صلای فتنه و شور و هزار شر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید تافتست ز روی تو چاشتگاه</p></div>
<div class="m2"><p>در عشق قرص روی تو رفتیم بام بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستیست در سر از می و این تاب آفتاب</p></div>
<div class="m2"><p>در سر بتافتست پس از دست رفت سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مطرب هوای دل عاشقان روح</p></div>
<div class="m2"><p>بنواز لحن جان که تننتن لطیفتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا جان‌ها ز خرقه تن‌ها برون شود</p></div>
<div class="m2"><p>تا بر سرین خرقه رود جان باخبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جام صاف باده تو خاشاک جسم را</p></div>
<div class="m2"><p>بردار تا نهیم به اقبال بر به بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دیده‌ها گذاره شود از حجاب‌ها</p></div>
<div class="m2"><p>تا وارهد ز خانه و مان و ز بام و در</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیمرغ جان و مفخر تبریز شمس دین</p></div>
<div class="m2"><p>بیند هزار روضه و یابد هزار پر</p></div></div>