---
title: >-
    غزل شمارهٔ ۱۹۵۳
---
# غزل شمارهٔ ۱۹۵۳

<div class="b" id="bn1"><div class="m1"><p>هر صبوحی ارغنون‌ها را برنجان همچنین</p></div>
<div class="m2"><p>آفرین‌ها بر جمالت همچنین جان همچنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش رویت روز مست و پیش زلفت شب خراب</p></div>
<div class="m2"><p>ای که کفرت همچنان و ای که ایمان همچنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کنار زهره نه تو چنگ عشرت همچنان</p></div>
<div class="m2"><p>پای کوبان اندرآ ای ماه تابان همچنین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشتهای مشک و عنبر چون بخیزد جمع را</p></div>
<div class="m2"><p>حلقه‌های زلف خود را زو برافشان همچنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخه چرخ ار بگردد بی‌مرادت یک نفس</p></div>
<div class="m2"><p>آتشی درزن به جان چرخ گردان همچنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز روز مجلس است ای عشق دست ما بگیر</p></div>
<div class="m2"><p>می کشان تا بزم خاص و تخت سلطان همچنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاره پاره پیشتر رو گر چه مستی ای رفیق</p></div>
<div class="m2"><p>پاره‌ای راه است از ما تا به میدان همچنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هوای شمس تبریزی ز ظلمت می گذر</p></div>
<div class="m2"><p>ناگهان سر برزنی از باغ و ایوان همچنین</p></div></div>