---
title: >-
    غزل شمارهٔ ۲۰۹۲
---
# غزل شمارهٔ ۲۰۹۲

<div class="b" id="bn1"><div class="m1"><p>آن دلبر من آمد بر من</p></div>
<div class="m2"><p>زنده شد از او بام و در من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم قنقی امشب تو مرا</p></div>
<div class="m2"><p>ای فتنه من شور و شر من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتا بروم کاری است مهم</p></div>
<div class="m2"><p>در شهر مرا جان و سر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم به خدا گر تو بروی</p></div>
<div class="m2"><p>امشب نزید این پیکر من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر تو شبی رحمی نکنی</p></div>
<div class="m2"><p>بر رنگ و رخ همچون زر من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رحمی نکند چشم خوش تو</p></div>
<div class="m2"><p>بر نوحه و این چشم تر من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بفشاند گل گلزار رخت</p></div>
<div class="m2"><p>بر اشک خوش چون کوثر من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتا چه کنم چون ریخت قضا</p></div>
<div class="m2"><p>خون همه را در ساغر من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مریخیم و جز خون نبود</p></div>
<div class="m2"><p>در طالع من در اختر من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عودی نشود مقبول خدا</p></div>
<div class="m2"><p>تا درنرود در مجمر من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم چو تو را قصد است به جان</p></div>
<div class="m2"><p>جز خون نبود نقل و خور من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو سرو و گلی من سایه تو</p></div>
<div class="m2"><p>من کشته تو تو حیدر من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتا نشود قربانی من</p></div>
<div class="m2"><p>جز نادره‌ای ای چاکر من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جرجیس رسد کو هر نفسی</p></div>
<div class="m2"><p>نو کشته شود در کشور من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اسحاق نبی باید که بود</p></div>
<div class="m2"><p>قربان شده بر خاک در من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من عشقم و چون ریزم ز تو خون</p></div>
<div class="m2"><p>زنده کنمت در محشر من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هان تا نطپی در پنجه من</p></div>
<div class="m2"><p>هان تا نرمی از خنجر من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با مرگ مکن تو روی ترش</p></div>
<div class="m2"><p>تا شکر کند از تو بر من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می‌خند چو گل چون برکندت</p></div>
<div class="m2"><p>تا به سر شدت در شکر من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اسحاق تویی من والد تو</p></div>
<div class="m2"><p>کی بشکنمت ای گوهر من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عشق است پدر عاشق رمه را</p></div>
<div class="m2"><p>زاینده از او کر و فر من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این گفت و بشد چون باد صبا</p></div>
<div class="m2"><p>شد اشک روان از منظر من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتم چه شود گر لطف کنی</p></div>
<div class="m2"><p>آهسته روی ای سرور من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اشتاب مکن آهسته ترک</p></div>
<div class="m2"><p>ای جان و جهان ای صدپر من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کس هیچ ندید اشتاب مرا</p></div>
<div class="m2"><p>این است تک کاهلتر من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این چرخ فلک گر جهد کند</p></div>
<div class="m2"><p>هرگز نرسد در معبر من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفتا که خمش کاین خنگ فلک</p></div>
<div class="m2"><p>لنگانه رود در محضر من</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خامش که اگر خامش نکنی</p></div>
<div class="m2"><p>در بیشه فتد این آذر من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باقیش مگو تا روز دگر</p></div>
<div class="m2"><p>تا دل نپرد از مصدر من</p></div></div>