---
title: >-
    غزل شمارهٔ ۲۲۸۲
---
# غزل شمارهٔ ۲۲۸۲

<div class="b" id="bn1"><div class="m1"><p>ای جان و دل از عشق تو در بزم تو پا کوفته</p></div>
<div class="m2"><p>سرها بریده بی‌عدد در رزم تو پا کوفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عزم میدان زمین کردی تو ای روح امین</p></div>
<div class="m2"><p>ذرات خاک این زمین از عزم تو پا کوفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرمان خرمشاهیت در خون دل توقیع شد</p></div>
<div class="m2"><p>کف کرد خون بر روی خون از جزم تو پا کوفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای حزم جمله خسروان از عهد آدم تا کنون</p></div>
<div class="m2"><p>بستان گرو از من به جان کز حزم تو پا کوفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوارزمیان منکر شده دیدار بی‌چون را ولی</p></div>
<div class="m2"><p>از بینش بی‌چون تو خوارزم تو پا کوفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آفتاب روی تو کرده هزیمت ماه را</p></div>
<div class="m2"><p>و آن ماه در راه آمده از هزم تو پا کوفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شمس تبریزی کند در مصحف دل یک نظر</p></div>
<div class="m2"><p>اعراب او رقصان شده هم جزم تو پا کوفته</p></div></div>