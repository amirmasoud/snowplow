---
title: >-
    غزل شمارهٔ ۱۲۷۴
---
# غزل شمارهٔ ۱۲۷۴

<div class="b" id="bn1"><div class="m1"><p>خواجه چرا کرده‌ای روی تو بر ما ترش</p></div>
<div class="m2"><p>زین شکرستان برو هست کس این جا ترش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شکرستان دل قند بود هم خجل</p></div>
<div class="m2"><p>تو ز کجا آمدی ابرو و سیما ترش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر فلک آن طوطیان جمله شکر می‌خورند</p></div>
<div class="m2"><p>گر نپری بر فلک منگر بالا ترش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رستم میدان فکر پیش عروسان بکر</p></div>
<div class="m2"><p>هیچ بود در وصال وقت تماشا ترش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کی خورد می صبوح روز بود شیرگیر</p></div>
<div class="m2"><p>هر کی خورد دوغ هست امشب و فردا ترش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مؤمن و ایمان و دین ذوق و حلاوت بود</p></div>
<div class="m2"><p>تو به کجا دیده‌ای طبله حلوا ترش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این ترشی‌ها همه پیش تو زان جمع شد</p></div>
<div class="m2"><p>جنس رود سوی جنس ترش رود با ترش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>والله هر میوه‌ای کو نپزد ز آفتاب</p></div>
<div class="m2"><p>گر چه بود نیشکر نبود الا ترش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوزش خورشید عشق صبر بود صبر کن</p></div>
<div class="m2"><p>روز دو سه صبر به مذهب تو با ترش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کی ترش بینیش دانک ز آتش گریخت</p></div>
<div class="m2"><p>غوره که در سایه ماند هست سر و پا ترش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دعوه دل کرده‌ای وعده وفا کن مباش</p></div>
<div class="m2"><p>در صف دعوی چو شیر وقت تقاضا ترش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنگر در مصطفی چونک ترش شد دمی</p></div>
<div class="m2"><p>کرده عتابش عبس خواند مر او را ترش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خامش و تهمت منه خواجه ترش نیست لیک</p></div>
<div class="m2"><p>گه گه قاصد کند مردم دانا ترش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او چو شکر بوده است دل ز شکر پر ولیک</p></div>
<div class="m2"><p>در ادب کودکان باشد لالا ترش</p></div></div>