---
title: >-
    غزل شمارهٔ ۳۰۳۰
---
# غزل شمارهٔ ۳۰۳۰

<div class="b" id="bn1"><div class="m1"><p>سلمک الله نیست مثل تو یاری</p></div>
<div class="m2"><p>نیست نکوتر ز بندگی تو کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل گفتی که یار غار منست او</p></div>
<div class="m2"><p>هیچ نگنجد چنین محیط به غاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق او خرد نیست زانک نخسبد</p></div>
<div class="m2"><p>بر سر آن گنج غیب هر نره ماری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذره به ذره کنار شوق گشادست</p></div>
<div class="m2"><p>گر چه نگنجد نگار ما به کناری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شکرستان رسید تا نگذارد</p></div>
<div class="m2"><p>سرکه فروشنده‌ای و غوره فشاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوی فراتی روان شدست از این سو</p></div>
<div class="m2"><p>کاین همه جان‌ها ز آب اوست بخاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر مستی پریر گفتم او را</p></div>
<div class="m2"><p>کار مرا این زمان بده تو قراری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خنده شیرین زد و ز شرم برافروخت</p></div>
<div class="m2"><p>ماه غریب از چو من غریب شماری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت مخور غم که زرد و خشک نماند</p></div>
<div class="m2"><p>باغ تو با این چنین لطیف بهاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هفت فلک ز آتش منست چو دودی</p></div>
<div class="m2"><p>هفت زمین در ره منست غباری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دام جهان را هزار قرن گذشتست</p></div>
<div class="m2"><p>درخور صیدم نیامدست شکاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم به کنار آمد این زمانه و دورش</p></div>
<div class="m2"><p>عاشق مستی ز ما نیافت کناری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این مه و خورشید چون دو گاو خراسند</p></div>
<div class="m2"><p>روز چرایی و شب اسیر شیاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جمع خرانی نگر که گاوپرستند</p></div>
<div class="m2"><p>یاوه شدستند بی‌شکال و فساری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رو به خران گو که ریش گاو بریزاد</p></div>
<div class="m2"><p>توبه کنید و روید سوی مطاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا که شود هر خری ندیم مسیحی</p></div>
<div class="m2"><p>وحی پذیرنده‌ای و روح سپاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از شش و از پنج بگذرید و ببینید</p></div>
<div class="m2"><p>شهره حریفان و مقبلانه قماری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون به خلاصه رسید تا که بگویم</p></div>
<div class="m2"><p>سوخت لبم را ز شوق دوست شراری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ماند سخن در دهان و رفت دل من</p></div>
<div class="m2"><p>جانب یاران به سوی دور دیاری</p></div></div>