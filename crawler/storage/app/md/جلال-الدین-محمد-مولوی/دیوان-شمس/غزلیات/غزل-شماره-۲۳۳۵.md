---
title: >-
    غزل شمارهٔ ۲۳۳۵
---
# غزل شمارهٔ ۲۳۳۵

<div class="b" id="bn1"><div class="m1"><p>رندان همه جمعند در این دیر مغانه</p></div>
<div class="m2"><p>درده تو یکی رطل بدان پیر یگانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون ریزبک عشق در و بام گرفته‌ست</p></div>
<div class="m2"><p>و آن عقل گریزان شده از خانه به خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک پرده برانداخته آن شاهد اعظم</p></div>
<div class="m2"><p>از پرده برون رفته همه اهل زمانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن جنس که عشاق در این بحر فتادند</p></div>
<div class="m2"><p>چه جای امان باشد و چه جای امانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی سرد شود عشق ز آواز ملامت</p></div>
<div class="m2"><p>هرگز نرمد شیر ز فریاد زنانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر کن تو یکی رطل ز می‌های خدایی</p></div>
<div class="m2"><p>مگذار خدایان طبیعت به میانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اول بده آن رطل بدان نفس محدث</p></div>
<div class="m2"><p>تا ناطقه‌اش هیچ نگوید ز فسانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بند شود نطق یکی سیل درآید</p></div>
<div class="m2"><p>کز کون و مکان هیچ نبینی تو نشانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس الحق تبریز چه آتش که برافروخت</p></div>
<div class="m2"><p>احسنت زهی آتش و شاباش زبانه</p></div></div>