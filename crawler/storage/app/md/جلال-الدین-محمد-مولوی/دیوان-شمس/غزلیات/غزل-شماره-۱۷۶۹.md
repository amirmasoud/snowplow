---
title: >-
    غزل شمارهٔ ۱۷۶۹
---
# غزل شمارهٔ ۱۷۶۹

<div class="b" id="bn1"><div class="m1"><p>ای دل صافی دم ثابت قدم</p></div>
<div class="m2"><p>جئت لکی تنذر خیر الامم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر ننهی جز به اشارات دل</p></div>
<div class="m2"><p>بر ورق عشق ازل چون قلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از طرب باد تو و داد تو</p></div>
<div class="m2"><p>رقص کنانیم چو شقه علم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقص کنان خواجه کجا می روی</p></div>
<div class="m2"><p>سوی گشایشگه عرصه عدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه کدامین عدم است این بگو</p></div>
<div class="m2"><p>گوش قدم داند حرف قدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق غریب است و زبانش غریب</p></div>
<div class="m2"><p>همچو غریب عربی در عجم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیز که آورده امت قصه‌ای</p></div>
<div class="m2"><p>بشنو از بنده نه بیش و نه کم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشنو این حرف غریبانه را</p></div>
<div class="m2"><p>قصه غریب آمد و گوینده هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از رخ آن یوسف شد قعر چاه</p></div>
<div class="m2"><p>روشن و فرخنده چو باغ ارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قصر شد آن حبس و در او باغ و راغ</p></div>
<div class="m2"><p>جنت و ایوان شد و صفه حرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو کلوخی که در آب افکنی</p></div>
<div class="m2"><p>باز شود آب در آن دم ز هم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو شب ابر که خورشید صبح</p></div>
<div class="m2"><p>ناگه سر برزند از چاه غم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو شرابی که عرب خورد و گفت</p></div>
<div class="m2"><p>صل علی دنتها و ارتسم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از طرب این حبس به خواری و نقص</p></div>
<div class="m2"><p>می نگرد بر فلک محتشم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای خرد از رشک دهانم مگیر</p></div>
<div class="m2"><p>قد شهد الله و عد النعم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چه درخت آب نهان می خورد</p></div>
<div class="m2"><p>بان علی شعبته ما کتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چه بدزدید زمین ز آسمان</p></div>
<div class="m2"><p>فصل بهاران بدهد دم به دم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر شبه دزدیده‌ای وگر گهر</p></div>
<div class="m2"><p>ور علم افراشتی وگر قلم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رفت شب و روز تو اینک رسید</p></div>
<div class="m2"><p>سوف یری النائم ماذا احتلم</p></div></div>