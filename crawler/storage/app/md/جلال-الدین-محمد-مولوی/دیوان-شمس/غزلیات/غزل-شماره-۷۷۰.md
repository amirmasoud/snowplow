---
title: >-
    غزل شمارهٔ ۷۷۰
---
# غزل شمارهٔ ۷۷۰

<div class="b" id="bn1"><div class="m1"><p>همه را بیازمودم ز تو خوشترم نیامد</p></div>
<div class="m2"><p>چو فروشدم به دریا چو تو گوهرم نیامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر خنب‌ها گشادم ز هزار خم چشیدم</p></div>
<div class="m2"><p>چو شراب سرکش تو به لب و سرم نیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه عجب که در دل من گل و یاسمن بخندد</p></div>
<div class="m2"><p>که سمن بری لطیفی چو تو در برم نیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پیت مراد خود را دو سه روز ترک کردم</p></div>
<div class="m2"><p>چه مراد ماند زان پس که میسرم نیامد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو سه روز شاهیت را چو شدم غلام و چاکر</p></div>
<div class="m2"><p>به جهان نماند شاهی که چو چاکرم نیامد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خردم بگفت برپر ز مسافران گردون</p></div>
<div class="m2"><p>چه شکسته پا نشستی که مسافرم نیامد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پرید سوی بامت ز تنم کبوتر دل</p></div>
<div class="m2"><p>به فغان شدم چو بلبل که کبوترم نیامد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پی کبوتر دل به هوا شدم چو بازان</p></div>
<div class="m2"><p>چه همای ماند و عنقا که برابرم نیامد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو ای تن پریشان تو و آن دل پشیمان</p></div>
<div class="m2"><p>که ز هر دو تا نرستم دل دیگرم نیامد</p></div></div>