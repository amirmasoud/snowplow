---
title: >-
    غزل شمارهٔ ۵۲
---
# غزل شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>چون همه عشق روی تست جمله رضای نفس ما</p></div>
<div class="m2"><p>کفر شدست لاجرم ترک هوای نفس ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چونک به عشق زنده شد قصد غزاش چون کنم</p></div>
<div class="m2"><p>غمزه خونی تو شد حج و غزای نفس ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست ز نفس ما مگر نقش و نشان سایه‌ای</p></div>
<div class="m2"><p>چون به خم دو زلف تست مسکن و جای نفس ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق فروخت آتشی کآب حیات از او خجل</p></div>
<div class="m2"><p>پرس که از برای که آن ز برای نفس ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هژده هزار عالم عیش و مراد عرضه شد</p></div>
<div class="m2"><p>جز به جمال تو نبود جوشش و رای نفس ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوزخ جای کافران جنت جای مؤمنان</p></div>
<div class="m2"><p>عشق برای عاشقان محو سزای نفس ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اصل حقیقت وفا سر خلاصه رضا</p></div>
<div class="m2"><p>خواجه روح شمس دین بود صفای نفس ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عوض عبیر جان در بدن هزار سنگ</p></div>
<div class="m2"><p>از تبریز خاک را کحل ضیای نفس ما</p></div></div>