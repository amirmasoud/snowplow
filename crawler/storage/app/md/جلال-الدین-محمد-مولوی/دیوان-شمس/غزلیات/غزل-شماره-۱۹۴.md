---
title: >-
    غزل شمارهٔ ۱۹۴
---
# غزل شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>خواهم گرفتن اکنون آن مایه صور را</p></div>
<div class="m2"><p>دامی نهاده‌ام خوش آن قبله نظر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوار گوش دارد آهسته‌تر سخن گو</p></div>
<div class="m2"><p>ای عقل بام بررو ای دل بگیر در را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اعدا که در کمینند در غصه همینند</p></div>
<div class="m2"><p>چون بشنوند چیزی گویند همدگر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ذره‌ها نهانند خصمان و دشمنانند</p></div>
<div class="m2"><p>در قعر چه سخن گو خلوت گزین سحر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای جان چه جای دشمن روزی خیال دشمن</p></div>
<div class="m2"><p>در خانه دلم شد از بهر رهگذر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رمزی شنید زین سر زو پیش دشمنان شد</p></div>
<div class="m2"><p>می‌خواند یک به یک را می‌گفت خشک و تر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان روز ما و یاران در راه عهد کردیم</p></div>
<div class="m2"><p>پنهان کنیم سر را پیش افکنیم سر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما نیز مردمانیم نی کم ز سنگ کانیم</p></div>
<div class="m2"><p>بی زخم‌های میتین پیدا نکرد زر را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریای کیسه بسته تلخ و ترش نشسته</p></div>
<div class="m2"><p>یعنی خبر ندارم کی دیده‌ام گهر را</p></div></div>