---
title: >-
    غزل شمارهٔ ۲۲۷۴
---
# غزل شمارهٔ ۲۲۷۴

<div class="b" id="bn1"><div class="m1"><p>مررت بدر فی هواه بحار</p></div>
<div class="m2"><p>راوه بدر و فی الدلال و حاروا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و شاهدت ماء شابه الروح فی الصفا</p></div>
<div class="m2"><p>و یعشق ذاک الماء ما هو نار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و للعشق نور لیس للشمس مثله</p></div>
<div class="m2"><p>فظل دلیل العاشقین و ساروا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عروس الهوی بدر تلالا فی الدجی</p></div>
<div class="m2"><p>علیها دماء العاشقین خمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظللت من الدنیا علی طلب الهوی</p></div>
<div class="m2"><p>اضاء لنا غیر الدیار دیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فشاهدت رکبانا قریحا مطیهم</p></div>
<div class="m2"><p>و کان لهم عند المسیر بدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فقلت لهم فی ذاک قالوا لفی الهوی</p></div>
<div class="m2"><p>لمن فر من هذا الدیار دمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و ان شئت برهانا فسافر ببلده</p></div>
<div class="m2"><p>یقال لها تبریز و هی مزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیشتم اهل العشق من ترباته</p></div>
<div class="m2"><p>و للروح منها زخرف و سوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تروح کلیل مظلم فی هوائه</p></div>
<div class="m2"><p>و ترجع مسرورا و انت نهار</p></div></div>