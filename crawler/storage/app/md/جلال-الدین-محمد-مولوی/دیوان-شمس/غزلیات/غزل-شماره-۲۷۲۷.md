---
title: >-
    غزل شمارهٔ ۲۷۲۷
---
# غزل شمارهٔ ۲۷۲۷

<div class="b" id="bn1"><div class="m1"><p>گر وسوسه ره دهی به گوشی</p></div>
<div class="m2"><p>افسرده شوی بدان ز جوشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن گرمی چشم را که داری</p></div>
<div class="m2"><p>نیش زهر است و شکل نوشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انبار نعیم را زیان چیست</p></div>
<div class="m2"><p>گر خشم گرفت کورموشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر چه زیان اگر بیفتد</p></div>
<div class="m2"><p>یک دو مگس از شکرفروشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مر ناقه شیر را چه نقصان</p></div>
<div class="m2"><p>گر دیگ شکست شیردوشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب بود و زمانه خفته بودند</p></div>
<div class="m2"><p>در هیچ سری نبود هوشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن شاه ز روی لطف برداشت</p></div>
<div class="m2"><p>سرنای و در او بزد خروشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خون خودی اگر بمانی</p></div>
<div class="m2"><p>زین پس زان رو به روی پوشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماییم ز عشق شمس تبریز</p></div>
<div class="m2"><p>هم ناطق عشق هم خموشی</p></div></div>