---
title: >-
    غزل شمارهٔ ۸۳۴
---
# غزل شمارهٔ ۸۳۴

<div class="b" id="bn1"><div class="m1"><p>از دل رفته نشان می‌آید</p></div>
<div class="m2"><p>بوی آن جان و جهان می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعره و غلغله آن مستان</p></div>
<div class="m2"><p>آشکارا و نهان می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر از هر طرفی می‌تابد</p></div>
<div class="m2"><p>پای کوبان سوی جان می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از در مشعله داران فلک</p></div>
<div class="m2"><p>آتش دل به دهان می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان پروانه میان می‌بندد</p></div>
<div class="m2"><p>شمع روشن به میان می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابی که ز ما پنهان بود</p></div>
<div class="m2"><p>سوی ما نورفشان می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر از غیب اگر پران نیست</p></div>
<div class="m2"><p>پس چرا بانگ کمان می‌آید</p></div></div>