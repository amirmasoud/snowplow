---
title: >-
    غزل شمارهٔ ۹۶۴
---
# غزل شمارهٔ ۹۶۴

<div class="b" id="bn1"><div class="m1"><p>گفتم که ای جان خود جان چه باشد</p></div>
<div class="m2"><p>ای درد و درمان درمان چه باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که سازم صد جان و دل را</p></div>
<div class="m2"><p>پیش تو قربان قربان چه باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نور رویت ای بوی کویت</p></div>
<div class="m2"><p>اسرار ایمان ایمان چه باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی گزیدی بر ما دکانی</p></div>
<div class="m2"><p>بر بی‌گناهی بهتان چه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اقبال پیشت سجده کنانست</p></div>
<div class="m2"><p>ای بخت خندان خندان چه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگشای ای جان در بر ضعیفان</p></div>
<div class="m2"><p>بر رغم دربان دربان چه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرمود صوفی که آن نداری</p></div>
<div class="m2"><p>باری بپرسش که آن چه باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با حسن رویت احسان کی جوید</p></div>
<div class="m2"><p>خود پیش حسنت احسان چه باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو شیری و ما انبان حیله</p></div>
<div class="m2"><p>در پیش شیران انبان چه باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بردار پرده از پیش دیده</p></div>
<div class="m2"><p>کوری شیطان شیطان چه باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس خلق هستند کز دوست مستند</p></div>
<div class="m2"><p>هرگز ندانند که نان چه باشد</p></div></div>