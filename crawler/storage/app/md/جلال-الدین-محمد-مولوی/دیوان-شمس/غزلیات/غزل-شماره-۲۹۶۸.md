---
title: >-
    غزل شمارهٔ ۲۹۶۸
---
# غزل شمارهٔ ۲۹۶۸

<div class="b" id="bn1"><div class="m1"><p>ای چنگیان غیبی از راه خوش نوایی</p></div>
<div class="m2"><p>تشنه دلان خود را کردید بس سقایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان تشنه ابد شد وین تشنگی ز حد شد</p></div>
<div class="m2"><p>یا ضربت جدایی یا شربت عطایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای زهره مزین زین هر دو یک نوا زن</p></div>
<div class="m2"><p>یا پرده رهاوی یا پرده رهایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چنگ کژ نوازی در چنگ غم گدازی</p></div>
<div class="m2"><p>خوش زن نوا اگر نی مردی ز بی‌نوایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی زخمه هیچ چنگی آب و نوا ندارد</p></div>
<div class="m2"><p>می‌کش تو زخمه زخمه گر چنگ بوالوفایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بگسلند تارت گیرند بر کنارت</p></div>
<div class="m2"><p>پیوند نو دهندت چندین دژم چرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو خود عزیز یاری پیوسته در کناری</p></div>
<div class="m2"><p>در بزم شهریاری بیرون ز جان و جایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامش که سخت مستم بربند هر دو دستم</p></div>
<div class="m2"><p>ور نه قدح شکستم گر لحظه‌ای بپایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من پیر منبلانم بر خویش زخم رانم</p></div>
<div class="m2"><p>من مصلحت ندانم با ما تو برنیایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم پاره پاره باشم هم خصم چاره باشم</p></div>
<div class="m2"><p>هم سنگ خاره باشم در صبر و بی‌نوایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بس که تند و عاقم در دوزخ فراقم</p></div>
<div class="m2"><p>دوزخ ز احتراقم گیرد گریزپایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون دید شور ما را عطار آشکارا</p></div>
<div class="m2"><p>بشکست طبل‌ها را در بزم کبریایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تبریز چون برفتم با شمس دین بگفتم</p></div>
<div class="m2"><p>بی حرف صد مقالت در وحدت خدایی</p></div></div>