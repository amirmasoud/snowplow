---
title: >-
    غزل شمارهٔ ۳۶
---
# غزل شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>خواجه بیا خواجه بیا خواجه دگربار بیا</p></div>
<div class="m2"><p>دفع مده دفع مده ای مه عیار بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق مهجور نگر عالم پرشور نگر</p></div>
<div class="m2"><p>تشنه مخمور نگر ای شه خمار بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای تویی دست تویی هستی هر هست تویی</p></div>
<div class="m2"><p>بلبل سرمست تویی جانب گلزار بیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوش تویی دیده تویی وز همه بگزیده تویی</p></div>
<div class="m2"><p>یوسف دزدیده تویی بر سر بازار بیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نظر گشته نهان ای همه را جان و جهان</p></div>
<div class="m2"><p>بار دگر رقص کنان بی‌دل و دستار بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشنی روز تویی شادی غم سوز تویی</p></div>
<div class="m2"><p>ماه شب افروز تویی ابر شکربار بیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای علم عالم نو پیش تو هر عقل گرو</p></div>
<div class="m2"><p>گاه میا گاه مرو خیز به یک بار بیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل آغشته به خون چند بود شور و جنون</p></div>
<div class="m2"><p>پخته شد انگور کنون غوره میفشار بیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شب آشفته برو وی غم ناگفته برو</p></div>
<div class="m2"><p>ای خرد خفته برو دولت بیدار بیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دل آواره بیا وی جگر پاره بیا</p></div>
<div class="m2"><p>ور ره در بسته بود از ره دیوار بیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای نفس نوح بیا وی هوس روح بیا</p></div>
<div class="m2"><p>مرهم مجروح بیا صحت بیمار بیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای مه افروخته رو آب روان در دل جو</p></div>
<div class="m2"><p>شادی عشاق بجو کوری اغیار بیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس بود ای ناطق جان چند از این گفت زبان</p></div>
<div class="m2"><p>چند زنی طبل بیان بی‌دم و گفتار بیا</p></div></div>