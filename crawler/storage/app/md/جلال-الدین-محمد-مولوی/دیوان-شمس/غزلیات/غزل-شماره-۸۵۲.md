---
title: >-
    غزل شمارهٔ ۸۵۲
---
# غزل شمارهٔ ۸۵۲

<div class="b" id="bn1"><div class="m1"><p>جز لطف و جز حلاوت خود از شکر چه آید</p></div>
<div class="m2"><p>جز نور بخش کردن خود از قمر چه آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز رنگ‌های دلکش از گلستان چه خیزد</p></div>
<div class="m2"><p>جز برگ و جز شکوفه از شاخ تر چه آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز طالع مبارک از مشتری چه یابی</p></div>
<div class="m2"><p>جز نقدهای روشن از کان زر چه آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن آفتاب تابان مر لعل را چه بخشد</p></div>
<div class="m2"><p>وز آب زندگانی اندر جگر چه آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دیدن جمالی کو حسن آفریند</p></div>
<div class="m2"><p>بالله یکی نظر کن کاندر نظر چه آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماییم و شور مستی مستی و بت پرستی</p></div>
<div class="m2"><p>زین سان که ما شدستیم از ما دگر چه آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی و مستتر شو بی‌زیر و بی‌زبر شو</p></div>
<div class="m2"><p>بی خویش و بی‌خبر شو خود از خبر چه آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چیزی ز ماست باقی مردانه باش ساقی</p></div>
<div class="m2"><p>درده می رواقی زین مختصر چه آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون گل رویم بیرون با جامه‌های گلگون</p></div>
<div class="m2"><p>مجنون شویم مجنون از خواب و خور چه آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای شه صلاح دین تو بیرون مشو ز صورت</p></div>
<div class="m2"><p>بنما فرشتگان را تو کز بشر چه آید</p></div></div>