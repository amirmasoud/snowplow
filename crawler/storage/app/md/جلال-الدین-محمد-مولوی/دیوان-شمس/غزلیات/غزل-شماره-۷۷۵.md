---
title: >-
    غزل شمارهٔ ۷۷۵
---
# غزل شمارهٔ ۷۷۵

<div class="b" id="bn1"><div class="m1"><p>هله هش دار که در شهر دو سه طرارند</p></div>
<div class="m2"><p>که به تدبیر کلاه از سر مه بردارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو سه رندند که هشیاردل و سرمستند</p></div>
<div class="m2"><p>که فلک را به یکی عربده در چرخ آرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سردهانند که تا سر ندهی سر ندهند</p></div>
<div class="m2"><p>ساقیانند که انگور نمی‌افشارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار آن صورت غیبند که جان طالب اوست</p></div>
<div class="m2"><p>همچو چشم خوش او خیره کش و بیمارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورتی‌اند ولی دشمن صورت‌هااند</p></div>
<div class="m2"><p>در جهانند ولی از دو جهان بیزارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو شیران بدرانند و به لب می‌خندند</p></div>
<div class="m2"><p>دشمن همدگرند و به حقیقت یارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرفروشانه یکی با دگری در جنگند</p></div>
<div class="m2"><p>لیک چون وانگری متفق یک کارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو خورشید همه روز نظر می‌بخشند</p></div>
<div class="m2"><p>مثل ماه و ستاره همه شب سیارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به کف خاک بگیرند زر سرخ شود</p></div>
<div class="m2"><p>روز گندم دروند ار چه به شب جو کارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلبرانند که دل بر ندهد بی‌برشان</p></div>
<div class="m2"><p>سرورانند که بیرون ز سر و دستارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکرانند که در معده نگردند ترش</p></div>
<div class="m2"><p>شاکرانند و از آن یار چه برخوردارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مردمی کن برو از خدمتشان مردم شو</p></div>
<div class="m2"><p>زانک این مردم دیگر همه مردم خوارند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس کن و بیش مگو گر چه دهان پرسخنست</p></div>
<div class="m2"><p>زانک این حرف و دم و قافیه هم اغیارند</p></div></div>