---
title: >-
    غزل شمارهٔ ۶۳۲
---
# غزل شمارهٔ ۶۳۲

<div class="b" id="bn1"><div class="m1"><p>عید آمد و عید آمد وان بخت سعید آمد</p></div>
<div class="m2"><p>برگیر و دهل می‌زن کان ماه پدید آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید آمد ای مجنون غلغل شنو از گردون</p></div>
<div class="m2"><p>کان معتمد سدره از عرش مجید آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عید آمد ره جویان رقصان و غزل گویان</p></div>
<div class="m2"><p>کان قیصر مه رویان زان قصر مشید آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد معدن دانایی مجنون شد و سودایی</p></div>
<div class="m2"><p>کان خوبی و زیبایی بی‌مثل و ندید آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان قدرت پیوستش داوود نبی مستش</p></div>
<div class="m2"><p>تا موم کند دستش گر سنگ و حدید آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عید آمد و ما بی‌او عیدیم بیا تا ما</p></div>
<div class="m2"><p>بر عید زنیم این دم کان خوان و ثرید آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زو زهر شکر گردد زو ابر قمر گردد</p></div>
<div class="m2"><p>زو تازه و تر گردد هر جا که قدید آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برخیز به میدان رو در حلقه رندان رو</p></div>
<div class="m2"><p>رو جانب مهمان رو کز راه بعید آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم‌هاش همه شادی بندش همه آزادی</p></div>
<div class="m2"><p>یک دانه بدو دادی صد باغ مزید آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من بنده آن شرقم در نعمت آن غرقم</p></div>
<div class="m2"><p>جز نعمت پاک او منحوس و پلید آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بربند لب و تن زن چون غنچه و چون سوسن</p></div>
<div class="m2"><p>رو صبر کن از گفتن چون صبر کلید آمد</p></div></div>