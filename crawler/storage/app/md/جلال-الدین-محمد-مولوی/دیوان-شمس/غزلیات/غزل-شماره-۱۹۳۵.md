---
title: >-
    غزل شمارهٔ ۱۹۳۵
---
# غزل شمارهٔ ۱۹۳۵

<div class="b" id="bn1"><div class="m1"><p>دلبر بیگانه صورت مهر دارد در نهان</p></div>
<div class="m2"><p>گر زبانش تلخ گوید قند دارد در دهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درون سو آشنا و از برون بیگانه رو</p></div>
<div class="m2"><p>این چنین پرمهر دشمن من ندیدم در جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونک دلبر خشم گیرد عشق او می گویدم</p></div>
<div class="m2"><p>عاشق ناشی مباش و رو مگردان هان و هان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست ماند تلخی دلبر به تلخی شراب</p></div>
<div class="m2"><p>سازوار اندر مزاج و تلخ تلخ اندر زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش او مردن به هر دم از شکر شیرینتر است</p></div>
<div class="m2"><p>مرده داند این سخن را تو مپرس از زندگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاد روزی کاین غزل را من بخوانم پیش عشق</p></div>
<div class="m2"><p>سجده‌ای آرم بر زمین و جان سپارم در زمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ جان را عشق گوید میل داری در قفس</p></div>
<div class="m2"><p>مرغ گوید من تو را خواهم قفس را بردران</p></div></div>