---
title: >-
    غزل شمارهٔ ۳۷۷
---
# غزل شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>آن را که در آخرش خری هست</p></div>
<div class="m2"><p>او را به طواف رهبری هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازار جهان به کسب برپاست</p></div>
<div class="m2"><p>زین در همه خارش وگری هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خارششان همی‌کشاند</p></div>
<div class="m2"><p>هر جای که شور یا شری هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در یم صدفی قرار گیرد</p></div>
<div class="m2"><p>کو را به درونه گوهری هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اما صدفی که در ندارد</p></div>
<div class="m2"><p>در جستن درش معبری هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه در یم و گاه سوی ساحل</p></div>
<div class="m2"><p>در جستن قطره‌اش سری هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاموش و طمع مکن سکینه</p></div>
<div class="m2"><p>آن راست سکون که مخبری هست</p></div></div>