---
title: >-
    غزل شمارهٔ ۱۰۳۹
---
# غزل شمارهٔ ۱۰۳۹

<div class="b" id="bn1"><div class="m1"><p>بگرد فتنه می‌گردی دگربار</p></div>
<div class="m2"><p>لب بامست و مستی هوش می‌دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا گردم دگر کو جای دیگر</p></div>
<div class="m2"><p>که ما فی الدار غیر الله دیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگردد نقش جز بر کلک نقاش</p></div>
<div class="m2"><p>بگرد نقطه گردد پای پرگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو تو باشی دل و جان کم نیاید</p></div>
<div class="m2"><p>چو سر باشد بیاید نیز دستار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفتارست دل در قبضه حق</p></div>
<div class="m2"><p>گرفته صعوه را بازی به منقار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز منقارش فلک سوراخ سوراخ</p></div>
<div class="m2"><p>ز چنگالش گران جانان سبکبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رها کن این سخن‌ها را ندا کن</p></div>
<div class="m2"><p>به مخموران که آمد شاه خمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم و اندیشه را گردن بریدند</p></div>
<div class="m2"><p>که آمد دور وصل و لطف و ایثار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هلا ای ساربان اشتر بخوابان</p></div>
<div class="m2"><p>از این خوشتر کجا باشد علف زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو مهمانان بدین دولت رسیدند</p></div>
<div class="m2"><p>بیا ای خازن و بگشای انبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب مشتاق را روزی نیاید</p></div>
<div class="m2"><p>چنین پنداشتی دیگر مپندار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمش کن تا خموش ما بگوید</p></div>
<div class="m2"><p>ویست اصل سخن سلطان گفتار</p></div></div>