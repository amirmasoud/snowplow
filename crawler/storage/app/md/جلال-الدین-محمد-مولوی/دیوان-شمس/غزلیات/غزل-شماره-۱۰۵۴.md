---
title: >-
    غزل شمارهٔ ۱۰۵۴
---
# غزل شمارهٔ ۱۰۵۴

<div class="b" id="bn1"><div class="m1"><p>ای یار شگرف در همه کار</p></div>
<div class="m2"><p>عیاره و عاشق تو عیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو روز قیامتی که از تو</p></div>
<div class="m2"><p>زیر و زبرست شهر و بازار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من زاری عاشقان چه گویم</p></div>
<div class="m2"><p>ای معشوقان ز عشق تو زار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در روز اجل چو من بمیرم</p></div>
<div class="m2"><p>در گور مکن مرا نگهدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور می‌خواهی که زنده گردیم</p></div>
<div class="m2"><p>ما را به نسیم وصل بسپار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر تو کجا و ما کجاییم</p></div>
<div class="m2"><p>ای بی‌تو حیات و عیش بی‌کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از من رگ جان بریده بادا</p></div>
<div class="m2"><p>گر بی‌تو رگیم هست هشیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر ره تو دو صد کمین بود</p></div>
<div class="m2"><p>نزدیک نمود راه و هموار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گلشن روی تو شدم مست</p></div>
<div class="m2"><p>بنهادم مست پای بر خار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفتم سوی دانه تو چون مرغ</p></div>
<div class="m2"><p>پرخون دیدم جناح و منقار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این طرفه که خوشترست زخمت</p></div>
<div class="m2"><p>از هر دانه که دارد انبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای بی‌تو حرام زندگانی</p></div>
<div class="m2"><p>ای بی‌تو نگشته بخت بیدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود بخت تویی و زندگی تو</p></div>
<div class="m2"><p>باقی نامی و لاف و آزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای کرده ز دل مرا فراموش</p></div>
<div class="m2"><p>آخر چه شود مرا به یاد آر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک بار چو رفت آب در جوی</p></div>
<div class="m2"><p>کی گردد چرخ طمع یک بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خامش که ستیزه می‌فزاید</p></div>
<div class="m2"><p>آن خواجه عشق را ز گفتار</p></div></div>