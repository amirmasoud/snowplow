---
title: >-
    غزل شمارهٔ ۱۵۸۹
---
# غزل شمارهٔ ۱۵۸۹

<div class="b" id="bn1"><div class="m1"><p>چشم بگشا جان نگر کش سوی جانان می برم</p></div>
<div class="m2"><p>پیش آن عید ازل جان بهر قربان می برم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کبوترخانه جان‌ها از او معمور گشت</p></div>
<div class="m2"><p>پس چرا این زیره را من سوی کرمان می برم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانک هر چیزی به اصلش شاد و خندان می رود</p></div>
<div class="m2"><p>سوی اصل خویش جان را شاد و خندان می برم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر دندان تا نیاید قند شیرین کی بود</p></div>
<div class="m2"><p>جان همچون قند را من زیر دندان می برم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که زر در کان بود او را نباشد رونقی</p></div>
<div class="m2"><p>سوی زرگر اندک اندک زودش از کان می برم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دود آتش کفر باشد نور او ایمان بود</p></div>
<div class="m2"><p>شمع جان را من ورای کفر و ایمان می برم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی هر ابری که او منکر شود خورشید را</p></div>
<div class="m2"><p>آفتابی زیر دامن بهر برهان می برم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس تبریز ارمغانم گوهر بحر دل است</p></div>
<div class="m2"><p>من ز شرم جان پاکت همچو عمان می برم</p></div></div>