---
title: >-
    غزل شمارهٔ ۱۱۷۳
---
# غزل شمارهٔ ۱۱۷۳

<div class="b" id="bn1"><div class="m1"><p>بشنو خبر صادق از گفته پیغامبر</p></div>
<div class="m2"><p>اندر صفت مؤمن المؤمن کالمزهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جاء الملک الاکبر ما احسن ذا المنظر</p></div>
<div class="m2"><p>حتی ملاء الدنیا بالعبهر و العنبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بربط شد مؤمن در ناله و در زاری</p></div>
<div class="m2"><p>بربط ز کجا نالد بی‌زخمه زخم آور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جاء الفرج الاعظم جاء الفرج الاکبر</p></div>
<div class="m2"><p>جاء الکرم الادوم جاء القمر الاقمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خو کرد دل بربط نشکیبد از آن زخمه</p></div>
<div class="m2"><p>اندر قدم مطرب می‌مالد رو و سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الدوله عیشیه و القهوه عرشیه</p></div>
<div class="m2"><p>و المجلس منثور باللوز مع السکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینک غزلی دیگر الخمس مع الخمسین</p></div>
<div class="m2"><p>زان پیش که برخوانم که شانیک الابتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الرب هو الساقی و العیش به باقی</p></div>
<div class="m2"><p>و السعد هو الراقی یا خایف لا تحذر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الروح غداً سکری من قهوتنا الکبری</p></div>
<div class="m2"><p>و ازینت الدنیا بالاخضر و الاحمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاموش شو و محرم می‌خور می جان هر دم</p></div>
<div class="m2"><p>در مجلس ربانی بی‌حلق و لب و ساغر</p></div></div>