---
title: >-
    غزل شمارهٔ ۱۶۲۵
---
# غزل شمارهٔ ۱۶۲۵

<div class="b" id="bn1"><div class="m1"><p>دو هزار عهد کردم که سر جنون نخارم</p></div>
<div class="m2"><p>ز تو درشکست عهدم ز تو باد شد قرارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ره زیاده جویی به طریق خیره رویی</p></div>
<div class="m2"><p>بروم که کدخدایم غله بدروم بکارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه حل و عقد عالم چو به دست غیب آمد</p></div>
<div class="m2"><p>من بوالفضول معجب تو بگو که بر چه کارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو قضا به سخره خواهد که ز سبلتی بخندد</p></div>
<div class="m2"><p>سگ لنگ را بگوید که برس بدان شکارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بر اوش رحم آید خبرش کند که بنشین</p></div>
<div class="m2"><p>بهل اختیار خود را تو به پیش اختیارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرت شکار باید ز منت شکار خوشتر</p></div>
<div class="m2"><p>همه صیدهای جان را به نثار بر تو بارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه ز دام من ملالی نه ز جام من وبالی</p></div>
<div class="m2"><p>نه نظیر من جمالی چه غریب و ندره یارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمش ار دگر بگویم ز مقالت خوش او</p></div>
<div class="m2"><p>بپرد کبوتر دل سوی اولین مطارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تبریز و شمس دین شد سبب فروغ اختر</p></div>
<div class="m2"><p>رخ شمس از او منور به فراز سبز طارم</p></div></div>