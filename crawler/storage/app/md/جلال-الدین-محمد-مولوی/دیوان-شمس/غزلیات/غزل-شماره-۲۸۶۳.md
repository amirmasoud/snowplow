---
title: >-
    غزل شمارهٔ ۲۸۶۳
---
# غزل شمارهٔ ۲۸۶۳

<div class="b" id="bn1"><div class="m1"><p>وقت آن شد که بدان روح فزا آمیزی</p></div>
<div class="m2"><p>مرغ زیرک شوی و خوش به دو پا آویزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه بگشا چو درختان به سوی باد بهار</p></div>
<div class="m2"><p>ز آنک زهر است تو را باد روی پاییزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شکرخنده معنی تو شکر شو همگی</p></div>
<div class="m2"><p>در صفات ترشی خواجه چرا بستیزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر دیوار وجود تو تویی گنج گهر</p></div>
<div class="m2"><p>گنج ظاهر شود ار تو ز میان برخیزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن قراضه ازلی ریخته در خاک تن است</p></div>
<div class="m2"><p>کو قراضه تک غلبیر تو گر می‌بیزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ جانی تو برآور ز نیام بدنت</p></div>
<div class="m2"><p>که دو نیمه کند او قرص قمر از تیزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ در دست درآ در سر میدان ابد</p></div>
<div class="m2"><p>از شب و روز برون تاز چو بر شبدیزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب حیوان بکش از چشمه به سوی دل خود</p></div>
<div class="m2"><p>ز آنک در خلقت جان بر مثل کاریزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور نتانی بگریز آ بر شه شمس الدین</p></div>
<div class="m2"><p>کو به جان هست ز عرش و به بدن تبریزی</p></div></div>