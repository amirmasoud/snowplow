---
title: >-
    غزل شمارهٔ ۱۷۵۶
---
# غزل شمارهٔ ۱۷۵۶

<div class="b" id="bn1"><div class="m1"><p>تا به جان مست عشق آن یارم</p></div>
<div class="m2"><p>سرده باده‌های انوارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دمی گر نه جان نو دهدم</p></div>
<div class="m2"><p>ای دل از جان خویش بیزارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد آن مه چو چرخ می گردم</p></div>
<div class="m2"><p>پس دگر چیست در زمین کارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر کارگاه خوبی بود</p></div>
<div class="m2"><p>سوزنش کرده‌ست چون تارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوزنم چنگ شد از او در تار</p></div>
<div class="m2"><p>تا به آواز زیر می زارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا من این کارگاه عالم را</p></div>
<div class="m2"><p>کو حجاب حق است بردارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بسوزم حجاب غفلت و خواب</p></div>
<div class="m2"><p>ز آتش چشم‌های بیدارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بیابم ز شمس تبریزی</p></div>
<div class="m2"><p>صحت این ضمیر بیمارم</p></div></div>