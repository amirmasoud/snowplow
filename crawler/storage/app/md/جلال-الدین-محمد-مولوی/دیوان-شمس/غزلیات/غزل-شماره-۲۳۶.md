---
title: >-
    غزل شمارهٔ ۲۳۶
---
# غزل شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>مبارکی که بود در همه عروسی‌ها</p></div>
<div class="m2"><p>در این عروسی ما باد ای خدا تنها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبارکی شب قدر و ماه روزه و عید</p></div>
<div class="m2"><p>مبارکی ملاقات آدم و حوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبارکی ملاقات یوسف و یعقوب</p></div>
<div class="m2"><p>مبارکی تماشای جنه المأوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبارکی دگر کان به گفت درناید</p></div>
<div class="m2"><p>نثار شادی اولاد شیخ و مهتر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به همدمی و خوشی همچو شیر باد و عسل</p></div>
<div class="m2"><p>به اختلاط و وفا همچو شکر و حلوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبارکی تبارک ندیم و ساقی باد</p></div>
<div class="m2"><p>بر آنک گوید آمین بر آنک کرد دعا</p></div></div>