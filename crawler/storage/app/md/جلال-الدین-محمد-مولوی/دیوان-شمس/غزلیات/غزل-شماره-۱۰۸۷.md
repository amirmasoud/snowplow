---
title: >-
    غزل شمارهٔ ۱۰۸۷
---
# غزل شمارهٔ ۱۰۸۷

<div class="b" id="bn1"><div class="m1"><p>بده آن باده به ما باده به ما اولیتر</p></div>
<div class="m2"><p>هر چه خواهی بکنی لیک وفا اولیتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر مردان چه کند خوبتر از سجده تو</p></div>
<div class="m2"><p>مسجد عیسی ز جان سقف سما اولیتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک فسون خوان صنما در دل مجنون بردم</p></div>
<div class="m2"><p>غنج‌های چو صبی را نه صبا اولیتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل را قبله کند آنک جمال تو ندید</p></div>
<div class="m2"><p>در کف کور ز قندیل عطا اولیتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو عطا می ده و از چرخ ندا می‌آید</p></div>
<div class="m2"><p>که ز دریا و ز خورشید عطا اولیتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطف‌ها کرده‌ای امروز دو تا کن آن را</p></div>
<div class="m2"><p>چونک در چنگ نیایی تو دوتا اولیتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونک خورشید برآید بگریزد سرما</p></div>
<div class="m2"><p>هر کی سردست از او پشت و قفا اولیتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بدیدم چمنت ز آب و گیا ببریدم</p></div>
<div class="m2"><p>آن ستورست که در آب و گیا اولیتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سادگی را ببرد گر چه سخن نقش خوشست</p></div>
<div class="m2"><p>بر رخ آینه از نقش صفا اولیتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صورت کون تویی آینه کون تویی</p></div>
<div class="m2"><p>داد آینه به تصویر بقا اولیتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمش این طبل مزن تیغ بزن وقت غزاست</p></div>
<div class="m2"><p>طبل اگر پشت سپاهست غزا اولیتر</p></div></div>