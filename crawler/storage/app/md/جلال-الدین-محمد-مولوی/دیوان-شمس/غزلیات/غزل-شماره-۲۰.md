---
title: >-
    غزل شمارهٔ ۲۰
---
# غزل شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>چندانک خواهی جنگ کن یا گرم کن تهدید را</p></div>
<div class="m2"><p>می‌دان که دود گولخن هرگز نیاید بر سما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور خود برآید بر سما کی تیره گردد آسمان</p></div>
<div class="m2"><p>کز دود آورد آسمان چندان لطیفی و ضیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود را مرنجان ای پدر سر را مکوب اندر حجر</p></div>
<div class="m2"><p>با نقش گرمابه مکن این جمله چالیش و غزا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو کنی بر مه تفو بر روی تو بازآید آن</p></div>
<div class="m2"><p>ور دامن او را کشی هم بر تو تنگ آید قبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از تو خامان دگر در جوش این دیگ جهان</p></div>
<div class="m2"><p>بس برطپیدند و نشد درمان نبود الا رضا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگرفت دم مار را یک خارپشت اندر دهن</p></div>
<div class="m2"><p>سر درکشید و گرد شد مانند گویی آن دغا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن مار ابله خویش را بر خار می‌زد دم به دم</p></div>
<div class="m2"><p>سوراخ سوراخ آمد او از خود زدن بر خارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی صبر بود و بی‌حیل خود را بکشت او از عجل</p></div>
<div class="m2"><p>گر صبر کردی یک زمان رستی از او آن بدلقا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خارپشت هر بلا خود را مزن تو هم هلا</p></div>
<div class="m2"><p>ساکن نشین وین ورد خوان جاء القضا ضاق الفضا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرمود رب العالمین با صابرانم همنشین</p></div>
<div class="m2"><p>ای همنشین صابران افرغ علینا صبرنا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رفتم به وادی دگر باقی تو فرما ای پدر</p></div>
<div class="m2"><p>مر صابران را می‌رسان هر دم سلامی نو ز ما</p></div></div>