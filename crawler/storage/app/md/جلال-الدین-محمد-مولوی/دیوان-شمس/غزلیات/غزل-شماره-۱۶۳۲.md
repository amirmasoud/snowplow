---
title: >-
    غزل شمارهٔ ۱۶۳۲
---
# غزل شمارهٔ ۱۶۳۲

<div class="b" id="bn1"><div class="m1"><p>هله رفتیم و گرانی ز جمالت بردیم</p></div>
<div class="m2"><p>جهت توشه ره ذکر وصالت بردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که ما را و تو را تذکره‌ای باشد یاد</p></div>
<div class="m2"><p>دل خسته به تو دادیم و خیالت بردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خیال رخ خوبت که قمر بنده اوست</p></div>
<div class="m2"><p>وان خم ابروی مانند هلالت بردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان شکرخنده خوبت که شکر تشنه اوست</p></div>
<div class="m2"><p>ز شکرخانه مجموع خصالت بردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کبوتر چو بپریم به تو بازآییم</p></div>
<div class="m2"><p>زانک ما این پر و بال از پر و بالت بردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا پرد فرعی به سوی اصل آید</p></div>
<div class="m2"><p>هر چه داریم همه از عز و جلالت بردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس تبریز شنو خدمت ما را ز صبا</p></div>
<div class="m2"><p>گر شمال است و صبا هم ز شمالت بردیم</p></div></div>