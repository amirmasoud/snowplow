---
title: >-
    غزل شمارهٔ ۱۶۶۱
---
# غزل شمارهٔ ۱۶۶۱

<div class="b" id="bn1"><div class="m1"><p>سالکان راه را محرم شدم</p></div>
<div class="m2"><p>ساکنان قدس را همدم شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طارمی دیدم برون از شش جهت</p></div>
<div class="m2"><p>خاک گشتم فرش آن طارم شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون شدم جوشیده در رگ‌های عشق</p></div>
<div class="m2"><p>در دو چشم عاشقانش نم شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه چو عیسی جملگی گشتم زبان</p></div>
<div class="m2"><p>گه دل خاموش چون مریم شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچ از عیسی و مریم یاوه شد</p></div>
<div class="m2"><p>گر مرا باور کنی آن هم شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش نشترهای عشق لم یزل</p></div>
<div class="m2"><p>زخم گشتم صد ره و مرهم شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر قدم همراه عزرائیل بود</p></div>
<div class="m2"><p>جان مبادم گر از او درهم شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رو به رو با مرگ کردم حرب‌ها</p></div>
<div class="m2"><p>تا ز عین مرگ من خرم شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سست کردم تنگ هستی را تمام</p></div>
<div class="m2"><p>تا که بر زین بقا محکم شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بانگ نای لم یزل بشنو ز من</p></div>
<div class="m2"><p>گر چو پشت چنگ اندر خم شدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رو نمود الله اعلم مر مرا</p></div>
<div class="m2"><p>کشته الله و پس اعلم شدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عید اکبر شمس تبریزی بود</p></div>
<div class="m2"><p>عید را قربانی اعظم شدم</p></div></div>