---
title: >-
    غزل شمارهٔ ۷۶۸
---
# غزل شمارهٔ ۷۶۸

<div class="b" id="bn1"><div class="m1"><p>چمنی که جمله گل‌ها به پناه او گریزد</p></div>
<div class="m2"><p>که در او خزان نباشد که در او گلی نریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شجری خوش و خرامان به میانه بیابان</p></div>
<div class="m2"><p>که کسی به سایه او چو بخفت مست خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلکی چو آسمان‌ها که بدوست قصد جان‌ها</p></div>
<div class="m2"><p>که زحل نیارد آن جا که به زهره برستیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهری لطیف کانی به مکان لامکانی</p></div>
<div class="m2"><p>بویست اشارت دل چو دو دیده اشک بیزد</p></div></div>