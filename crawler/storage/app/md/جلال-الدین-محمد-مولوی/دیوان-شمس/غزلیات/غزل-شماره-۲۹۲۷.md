---
title: >-
    غزل شمارهٔ ۲۹۲۷
---
# غزل شمارهٔ ۲۹۲۷

<div class="b" id="bn1"><div class="m1"><p>ز کجا آمده‌ای می‌دانی</p></div>
<div class="m2"><p>ز میان حرم سبحانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد کن هیچ به یادت آید</p></div>
<div class="m2"><p>آن مقامات خوش روحانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس فراموش شدستت آن‌ها</p></div>
<div class="m2"><p>لاجرم خیره و سرگردانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان فروشی به یکی مشتی خاک</p></div>
<div class="m2"><p>این چه بیع است بدین ارزانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازده خاک و بدان قیمت خود</p></div>
<div class="m2"><p>نی غلامی ملکی سلطانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهت تو ز فلک آمده‌اند</p></div>
<div class="m2"><p>خوبرویان خوش پنهانی</p></div></div>