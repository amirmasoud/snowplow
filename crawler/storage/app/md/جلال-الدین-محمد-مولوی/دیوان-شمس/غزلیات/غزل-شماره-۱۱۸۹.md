---
title: >-
    غزل شمارهٔ ۱۱۸۹
---
# غزل شمارهٔ ۱۱۸۹

<div class="b" id="bn1"><div class="m1"><p>در این سرما سر ما داری امروز</p></div>
<div class="m2"><p>سر عیش و تماشا داری امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی خورشید و ما پیشت چو ذره</p></div>
<div class="m2"><p>که ما را بی‌سر و پا داری امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چارم آسمان پهلوی خورشید</p></div>
<div class="m2"><p>تو ما را چون مسیحا داری امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا از سنگ صد چشمه روان کن</p></div>
<div class="m2"><p>که احسان موفا داری امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تراشیدی ز رحمت نردبانی</p></div>
<div class="m2"><p>که عزم کوچ بالا داری امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی دعوت زهی مهمانی زفت</p></div>
<div class="m2"><p>که بر چرخ معلا داری امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیش هر کسی ماهی بریان</p></div>
<div class="m2"><p>در آن ماهی تو دریا داری امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون ماهی دریا کی دیدست</p></div>
<div class="m2"><p>عجایب‌های زیبا داری امروز</p></div></div>