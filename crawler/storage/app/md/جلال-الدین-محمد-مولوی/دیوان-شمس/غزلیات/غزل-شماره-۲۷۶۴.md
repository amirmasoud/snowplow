---
title: >-
    غزل شمارهٔ ۲۷۶۴
---
# غزل شمارهٔ ۲۷۶۴

<div class="b" id="bn1"><div class="m1"><p>ماها چو به چرخ دل برآیی</p></div>
<div class="m2"><p>چون جان به تن جهان درآیی</p></div></div>
<div class="b2" id="bn2"><p>ماها چه لطیف و خوش لقایی</p>
<p>ای ماه بگو که از کجایی</p></div>
<div class="b" id="bn3"><div class="m1"><p>داریم ز عشق تو براتی</p></div>
<div class="m2"><p>وز قند لطیف تو نباتی</p></div></div>
<div class="b2" id="bn4"><p>از لعل لبت بده زکاتی</p>
<p>ای ماه بگو که از کجایی</p></div>
<div class="b" id="bn5"><div class="m1"><p>ای یوسف جان که در نخاسی</p></div>
<div class="m2"><p>در حسن و جمال بی‌قیاسی</p></div></div>
<div class="b2" id="bn6"><p>در ما بنگر چو می‌شناسی</p>
<p>ای ماه بگو که از کجایی</p></div>
<div class="b" id="bn7"><div class="m1"><p>زان سان ز شراب تو خرابیم</p></div>
<div class="m2"><p>کز خود اثری همی‌نیابیم</p></div></div>
<div class="b2" id="bn8"><p>بفزای اگر چه می‌نتابیم</p>
<p>ای ماه بگو که از کجایی</p></div>
<div class="b" id="bn9"><div class="m1"><p>در زیر درخت تو نشینیم</p></div>
<div class="m2"><p>وز میوه دلکش تو چینیم</p></div></div>
<div class="b2" id="bn10"><p>جز گلشن روی تو نبینیم</p>
<p>ای ماه بگو که از کجایی</p></div>
<div class="b" id="bn11"><div class="m1"><p>هر دم که ز باده تو نوشیم</p></div>
<div class="m2"><p>بس روشن جان و تیزگوشیم</p></div></div>
<div class="b2" id="bn12"><p>بی هوش شدیم و بس به هوشیم</p>
<p>ای ماه بگو که از کجایی</p></div>
<div class="b" id="bn13"><div class="m1"><p>از آتش‌هات در فروغند</p></div>
<div class="m2"><p>فارغ از صدق وز دروغند</p></div></div>
<div class="b2" id="bn14"><p>با قبله آتشین چو موغند</p>
<p>ای ماه بگو که از کجایی</p></div>
<div class="b" id="bn15"><div class="m1"><p>ای رشک بتان و بت پرستان</p></div>
<div class="m2"><p>آرام دل خراب مستان</p></div></div>
<div class="b2" id="bn16"><p>پا را بمکش ز زیردستان</p>
<p>ای ماه بگو که از کجایی</p></div>
<div class="b" id="bn17"><div class="m1"><p>شمس تبریز پادشاهی</p></div>
<div class="m2"><p>در خطه بی‌حد الهی</p></div></div>
<div class="b2" id="bn18"><p>از ماه تو راست تا به ماهی</p>
<p>ای ماه بگو که از کجایی</p></div>