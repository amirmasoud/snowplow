---
title: >-
    غزل شمارهٔ ۱۲۸۸
---
# غزل شمارهٔ ۱۲۸۸

<div class="b" id="bn1"><div class="m1"><p>چو رو نمود به منصور وصل دلدارش</p></div>
<div class="m2"><p>روا بود که رساند به اصل دل دارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از قباش ربودم یکی کلهواری</p></div>
<div class="m2"><p>بسوخت عقل و سر و پایم از کلهوارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکستم از سر دیوار باغ او خاری</p></div>
<div class="m2"><p>چه خارخار و طلب در دلست از آن خارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شیرگیر شد این دل یکی سحر ز میش</p></div>
<div class="m2"><p>سزد که زخم کشد از فراق سگسارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه کره گردون حرون و تند نمود</p></div>
<div class="m2"><p>به دست عشق وی آمد شکال و افسارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه صاحب صدرست عقل و بس دانا</p></div>
<div class="m2"><p>به جام عشق گرو شد ردا و دستارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسا دلا که به زنهار آمد از عشقش</p></div>
<div class="m2"><p>کشان کشان بکشیدش نداد زنهارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به روز سرد یکی پوستین بد اندر جو</p></div>
<div class="m2"><p>به عور گفتم درجه به جو برون آرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه پوستین بود آن خرس بود اندر جو</p></div>
<div class="m2"><p>فتاده بود همی‌برد آب جوبارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درآمد او به طمع تا به پوست خرس رسید</p></div>
<div class="m2"><p>به دست خرس بکرد آن طمع گرفتارش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتمش که رها کن تو پوستین بازآ</p></div>
<div class="m2"><p>چه دور و دیر بماندی به رنج و پیکارش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفت رو که مرا پوستین چنان بگرفت</p></div>
<div class="m2"><p>که نیست امید رهایی ز چنگ جبارش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار غوطه مرا می‌دهد به هر ساعت</p></div>
<div class="m2"><p>خلاص نیست از آن چنگ عاشق افشارش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خمش بس است حکایت اشارتی بس کن</p></div>
<div class="m2"><p>چه حاجتست بر عقل طول طومارش</p></div></div>