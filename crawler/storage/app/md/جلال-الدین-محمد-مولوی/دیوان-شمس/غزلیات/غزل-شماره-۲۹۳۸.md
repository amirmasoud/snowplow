---
title: >-
    غزل شمارهٔ ۲۹۳۸
---
# غزل شمارهٔ ۲۹۳۸

<div class="b" id="bn1"><div class="m1"><p>آن مه چو در دل آید او را عجب شناسی</p></div>
<div class="m2"><p>در دل چگونه آید از راه بی‌قیاسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گویی می‌شناسم لاف بزرگ و دعوی</p></div>
<div class="m2"><p>ور گویی من چه دانم کفر است و ناسپاسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردانم و ندانم گردان شده‌ست خلقی</p></div>
<div class="m2"><p>گردان و چشم بسته چون استر خراسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌گرد چون خراسی خواهی و گر نخواهی</p></div>
<div class="m2"><p>گردن مپیچ زیرا دربند احتباسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف خرید کوری با هیجده قلب آری</p></div>
<div class="m2"><p>از کوری خرنده وز حاسدی نخاسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو هم ز یوسفانی در چاه تن فتاده</p></div>
<div class="m2"><p>اینک رسن برون آ تا در زمین نتاسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای نفس مطمئنه اندر صفات حق رو</p></div>
<div class="m2"><p>اینک قبای اطلس تا کی در این پلاسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر من غزل نخوانم بشکافد او دهانم</p></div>
<div class="m2"><p>گوید طرب بیفزا آخر حریف کاسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بانگ طاس ماه بگرفته می‌گشاید</p></div>
<div class="m2"><p>ماهت منم گرفته بانگی زن ار تو طاسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آدم ز سنبلی خورد کان عاقبت بریزد</p></div>
<div class="m2"><p>تو سنبل وصالی ایمن ز زخم داسی</p></div></div>