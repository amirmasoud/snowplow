---
title: >-
    غزل شمارهٔ ۲۴۷۶
---
# غزل شمارهٔ ۲۴۷۶

<div class="b" id="bn1"><div class="m1"><p>هین که خروس بانگ زد وقت صبوح یافتی</p></div>
<div class="m2"><p>شرح نمی‌کنم که بس عاقل را اشارتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فهم کنی تو خود که تو زیرک و پاک خاطری</p></div>
<div class="m2"><p>باده بیار و دل ببر زود بکن تجارتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نای بنه دهان همی‌آرد صبح ناله‌ای</p></div>
<div class="m2"><p>چنگ ز چنگ هجر تو کرد حزین شکایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درده بی‌دریغ از آن شیره و شیر رایگان</p></div>
<div class="m2"><p>شیر و نبید خلد را نیست حدی و غایتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درده باده‌ای چو زر پاک ز خویشمان ببر</p></div>
<div class="m2"><p>نیست بتر ز باخودی مذهب ما جنایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده شاد جان فزا تحفه بیار از سما</p></div>
<div class="m2"><p>تا غم و غصه را کند اشقر می سیاستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل ز نقل تو شود منتقل از عقیله‌ها</p></div>
<div class="m2"><p>دانش غیب یابد و تبصره و فراستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جام تو را چو دل بود در سر و سینه شعله‌ای</p></div>
<div class="m2"><p>مست تو را چه کم بود تجربه یا کفایتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست که یافت مشربی ماند ز حرص و مکسبی</p></div>
<div class="m2"><p>سر که بیافت آن طرب کی طلبد ریاستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شست تو ماهی مرا چله نشاند مدتی</p></div>
<div class="m2"><p>دام تو کرکس مرا داد به غم ریاضتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطره ز بحر فضل تو یافت عجب تبدلی</p></div>
<div class="m2"><p>پاکدلی و صفوتی توسعه و احاطتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفس خسیس حرص خو عاشق مال و گفت و گو</p></div>
<div class="m2"><p>یافت به گنج رحمتت از دو جهان فراغتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترک زیارتت شها دان ز خری نه بی‌خری</p></div>
<div class="m2"><p>ز آنک به جان است متصل حج تو بی‌مسافتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیچ مگو دلا هلا طاقت رنج نیستم</p></div>
<div class="m2"><p>طاق شو از فضول خود حاجت نیست طاقتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طاقت رنج هر کسی داری و می‌کشی بسی</p></div>
<div class="m2"><p>طاقت گنج نیستت این چه بود خساستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر دل تو جز ولا تا نبود که بی‌گمان</p></div>
<div class="m2"><p>بر سر بینیت کند سر دلت علامتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حشر شود ضمیر تو در سخن و صفیر تو</p></div>
<div class="m2"><p>نقد شود در این جهان عرض تو را قیامتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از بد و نیک مجرمان کند نشد وفای تو</p></div>
<div class="m2"><p>ز آنک تو راست در کرم ثابتی و مهارتی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جان و دل مرید را از شهوات ما و من</p></div>
<div class="m2"><p>جز ز زلال بحر تو نیست یقین طهارتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>متقیان به بادیه رفته عشا و غادیه</p></div>
<div class="m2"><p>کعبه روان شده به تو تا که کند زیارتی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روح سجود می‌کند شکر وجود می‌کند</p></div>
<div class="m2"><p>یافت ز بندگی تو سروری و سیادتی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر کرم و کرامت خنده آفتاب تو</p></div>
<div class="m2"><p>ذره به ذره را بود نوع دگر شهادتی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جمله به جست و جوی تو معتکفان کوی تو</p></div>
<div class="m2"><p>روی به کعبه کرم مشتغل عبادتی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پنج حس از مصاحف نور و حیات جامعت</p></div>
<div class="m2"><p>یاد گرفته ز اوستا ظاهر پنج آیتی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاه چو چنگ می‌کند پیش درت رکوع خوش</p></div>
<div class="m2"><p>گاه چو نای می‌کند بهر دم تو قامتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بس کن ای خرد از این ناله و قصه حزین</p></div>
<div class="m2"><p>بوی برد به خامشی هر دل باشهامتی</p></div></div>