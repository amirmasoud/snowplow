---
title: >-
    غزل شمارهٔ ۲۷۳۱
---
# غزل شمارهٔ ۲۷۳۱

<div class="b" id="bn1"><div class="m1"><p>بشنیده بدم که جان جانی</p></div>
<div class="m2"><p>آنی و هزار همچنانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خلق نشان تو شنیدم</p></div>
<div class="m2"><p>کفو تو نبود آن نشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الحمد شدم ز حمد گفتن</p></div>
<div class="m2"><p>تا بوک بدان لبم بخوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان دید کسی بدین لطیفی</p></div>
<div class="m2"><p>کس دید روان بدین روانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای قوت قلوب همچو معنی</p></div>
<div class="m2"><p>وی صورت تو به از معانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای گشته ز لامکان حقایق</p></div>
<div class="m2"><p>از لذت کان تو مکانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شاه و وزیر را سعادت</p></div>
<div class="m2"><p>وی عالم پیر را جوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن جان که از این جهان جهان بود</p></div>
<div class="m2"><p>کردیش تو باز این جهانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جانی چو تو باشد این جهان را</p></div>
<div class="m2"><p>باقی بود این جهان فانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان چرب زبان توست اما</p></div>
<div class="m2"><p>نبود به لسان تو لسانی</p></div></div>