---
title: >-
    غزل شمارهٔ ۲۱۹۲
---
# غزل شمارهٔ ۲۱۹۲

<div class="b" id="bn1"><div class="m1"><p>ای عارف خوش کلام برگو</p></div>
<div class="m2"><p>ای فخر همه کرام برگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ممتحنی ز دست رفته</p></div>
<div class="m2"><p>بر دست گرفت جام برگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قایم شو و مات کن خرد را</p></div>
<div class="m2"><p>وز باده باقوام برگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا روح شویم جمله می ده</p></div>
<div class="m2"><p>تا خواجه شود غلام برگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قانع نشوم به نور روزن</p></div>
<div class="m2"><p>بشکاف حجاب بام برگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپذیر مدام خوش ز ساقی</p></div>
<div class="m2"><p>چون مست شدی مدام برگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن جام چو زر پخته بستان</p></div>
<div class="m2"><p>زان سوختگان خام برگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبدل شد و خوش حطام دنیا</p></div>
<div class="m2"><p>چون رستی از این حطام برگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب بستم ای بت شکرلب</p></div>
<div class="m2"><p>بی‌واسطه و پیام برگو</p></div></div>