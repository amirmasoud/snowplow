---
title: >-
    غزل شمارهٔ ۲۶۸۷
---
# غزل شمارهٔ ۲۶۸۷

<div class="b" id="bn1"><div class="m1"><p>صلا ای صوفیان کامروز باری</p></div>
<div class="m2"><p>سماع است و وصال و عیش آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکن ای موسی جان خلع نعلین</p></div>
<div class="m2"><p>که اندر گلشن جان نیست خاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کبوترها سراسر باز گردند</p></div>
<div class="m2"><p>که افتاد این شکاران را شکاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود سرهای مستان فارغ از درد</p></div>
<div class="m2"><p>چو سر درکرد خمر بی‌خماری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخور که ساعتی دیگر نبینی</p></div>
<div class="m2"><p>ز مشرق تا به مغرب هوشیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآور بینی و بوی دگر جوی</p></div>
<div class="m2"><p>که این بینی است آن بو را مهاری</p></div></div>