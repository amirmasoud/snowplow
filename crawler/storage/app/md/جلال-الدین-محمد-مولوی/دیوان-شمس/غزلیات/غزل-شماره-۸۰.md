---
title: >-
    غزل شمارهٔ ۸۰
---
# غزل شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>امروز گزافی ده آن باده نابی را</p></div>
<div class="m2"><p>برهم زن و درهم زن این چرخ شتابی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم قدح غیبی از دیده نهان آمد</p></div>
<div class="m2"><p>پنهان نتوان کردن مستی و خرابی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عشق طرب پیشه خوش گفت خوش اندیشه</p></div>
<div class="m2"><p>بربای نقاب از رخ آن شاه نقابی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خیزد ای فرخ زین سو اخ و زان سو اخ</p></div>
<div class="m2"><p>برکن هله ای گلرخ سغراق و شرابی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زان که نمی‌خواهی تا جلوه شود گلشن</p></div>
<div class="m2"><p>از بهر چه بگشادی دکان گلابی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را چو ز سر بردی وین جوی روان کردی</p></div>
<div class="m2"><p>در آب فکن زوتر بط زاده آبی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماییم چو کشت ای جان بررسته در این میدان</p></div>
<div class="m2"><p>لب خشک و به جان جویان باران سحابی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر سوی رسولی نو گوید که نیابی رو</p></div>
<div class="m2"><p>لاحول بزن بر سر آن زاغ غرابی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای فتنه هر روحی کیسه بر هر جوحی</p></div>
<div class="m2"><p>دزدیده رباب از کف بوبکر ربابی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امروز چنان خواهم تا مست و خرف سازی</p></div>
<div class="m2"><p>این جان محدث را وان عقل خطابی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای آب حیات ما شو فاش چو حشر ار چه</p></div>
<div class="m2"><p>شیر شتر گرگین جانست عرابی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای جاه و جمالت خوش خامش کن و دم درکش</p></div>
<div class="m2"><p>آگاه مکن از ما هر غافل خوابی را</p></div></div>