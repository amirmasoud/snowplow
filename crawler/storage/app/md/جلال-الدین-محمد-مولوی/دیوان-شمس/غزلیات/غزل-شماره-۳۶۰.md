---
title: >-
    غزل شمارهٔ ۳۶۰
---
# غزل شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>ایا ساقی تویی قاضی حاجات</p></div>
<div class="m2"><p>شرابی ده که آرد در مراعات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان گشتم ز مستی و خرابی</p></div>
<div class="m2"><p>که نشناسم اشارات از عبارات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پدر بر خم خمرم وقف کردست</p></div>
<div class="m2"><p>سبیلم کرد مادر بر خرابات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو گوشم بست یزدان تا رهیدم</p></div>
<div class="m2"><p>ز حال دی و فردا و خرافات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگرگون است کوی اهل تمییز</p></div>
<div class="m2"><p>که آن جا رسم طاعاتست و زلات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این کو کدخدا شاهی است باقی</p></div>
<div class="m2"><p>فرو روبیده این کو را ز آفات</p></div></div>