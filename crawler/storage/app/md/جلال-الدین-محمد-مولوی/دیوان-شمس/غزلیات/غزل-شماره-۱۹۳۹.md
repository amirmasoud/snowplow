---
title: >-
    غزل شمارهٔ ۱۹۳۹
---
# غزل شمارهٔ ۱۹۳۹

<div class="b" id="bn1"><div class="m1"><p>می پرد این مرغ دیگر در جنان عاشقان</p></div>
<div class="m2"><p>سوی عنقا می کشاند استخوان عاشقان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دریغا چشم بودی تا بدیدی در هوا</p></div>
<div class="m2"><p>تا روان دیدی روان گشته روان عاشقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشتران سربریده پای بالا می نهند</p></div>
<div class="m2"><p>اشتر باسر مجو در کاروان عاشقان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن جنازه برپریدی گر نگفتی غیرتش</p></div>
<div class="m2"><p>بی نشان رو بی‌نشان رو بی‌نشان عاشقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به گورستان درآید استخوان عاشقی</p></div>
<div class="m2"><p>صد نواله پیچد از وی میرخوان عاشقان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذره ذره دف زدی و کف زدی در عرس او</p></div>
<div class="m2"><p>گر روا بودی شدن پیدا نهان عاشقان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تن عاشق درآید همچو گنجی در زمین</p></div>
<div class="m2"><p>صد دریچه برگشاید آسمان عاشقان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کفن پیچید بینید ای عزیزان کوه قاف</p></div>
<div class="m2"><p>چشم بند است این عجب یا امتحان عاشقان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرمن گل بود و شد از مرگ شاخ زعفران</p></div>
<div class="m2"><p>صد گلستان بیش ارزد زعفران عاشقان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای رسول غیرت مردان دهانم را مگیر</p></div>
<div class="m2"><p>تا دو سه نکته بگویم از زبان عاشقان</p></div></div>