---
title: >-
    غزل شمارهٔ ۲۲۸۶
---
# غزل شمارهٔ ۲۲۸۶

<div class="b" id="bn1"><div class="m1"><p>ای تو برای آبرو آب حیات ریخته</p></div>
<div class="m2"><p>زهر گرفته در دهان قند و نبات ریخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست و خراب این چنین چرخ ندانی از زمین</p></div>
<div class="m2"><p>از پی آب پارگین آب فرات ریخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو خران به کاه و جو نیست روا چنین مرو</p></div>
<div class="m2"><p>بر فقرا تو درنگر زر صدقات ریخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روح شو و جهت مجو ذات شو و صفت مگو</p></div>
<div class="m2"><p>زان شه بی‌جهت نگر جمله جهات ریخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه دریغ مغز تو در ره پوست باخته</p></div>
<div class="m2"><p>آه دریغ شاه تو در غم مات ریخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غم مات شاه دل خانه به خانه می‌دود</p></div>
<div class="m2"><p>رنگ رخ و پیاده‌ها بهر نجات ریخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جسته برات جان از او باز چو دیده روی او</p></div>
<div class="m2"><p>کیسه دریده پیش او جمله برات ریخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صفتش صفات ما خارشناس گل شده</p></div>
<div class="m2"><p>باز صفات ما چو گل در ره ذات ریخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بال و پری که او تو را برد و اسیر دام کرد</p></div>
<div class="m2"><p>بال و پری است عاریت روز وفات ریخته</p></div></div>