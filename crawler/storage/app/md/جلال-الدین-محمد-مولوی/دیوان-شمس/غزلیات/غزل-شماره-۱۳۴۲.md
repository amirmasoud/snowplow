---
title: >-
    غزل شمارهٔ ۱۳۴۲
---
# غزل شمارهٔ ۱۳۴۲

<div class="b" id="bn1"><div class="m1"><p>چه کارستان که داری اندر این دل</p></div>
<div class="m2"><p>چه بت‌ها می‌نگاری اندر این دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار آمد زمان کشت آمد</p></div>
<div class="m2"><p>کی داند تا چه کاری اندر این دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حجاب عزت ار بستی ز بیرون</p></div>
<div class="m2"><p>به غایت آشکاری اندر این دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آب و گل فروشد پای طالب</p></div>
<div class="m2"><p>سرش را می‌بخاری اندر این دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از افلاک اگر افزون نبودی</p></div>
<div class="m2"><p>نکردی مه سواری اندر این دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر دل نیستی شهر معظم</p></div>
<div class="m2"><p>نکردی شهریاری اندر این دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجایب بیشه‌ای آمد دل ای جان</p></div>
<div class="m2"><p>که تو میر شکاری اندر این دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بحر دل هزاران موج خیزد</p></div>
<div class="m2"><p>چو جوهرها بیاری اندر این دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خمش کردم که در فکرت نگنجد</p></div>
<div class="m2"><p>چو وصف دل شماری اندر این دل</p></div></div>