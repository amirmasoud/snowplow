---
title: >-
    غزل شمارهٔ ۲۷۱۷
---
# غزل شمارهٔ ۲۷۱۷

<div class="b" id="bn1"><div class="m1"><p>تو جانا بی‌وصالش در چه کاری</p></div>
<div class="m2"><p>به دست خویش بی‌وصلش چه داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه لافت که زاری‌ها کنم من</p></div>
<div class="m2"><p>به نزد او نیرزد خاک زاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر سنگت ببیند بر تو گرید</p></div>
<div class="m2"><p>که از وصل چه کس گشتی تو عاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وصلش مر سما را فخر بودی</p></div>
<div class="m2"><p>به هجرش خاک را اکنون تو عاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان مغرور و سرکش گشته بودی</p></div>
<div class="m2"><p>زمان وصل یعنی یار غاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن می‌ها ز وصلش مست بودی</p></div>
<div class="m2"><p>نک آمد مر تو را دور خماری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیکن مرغ دولت مژده آورد</p></div>
<div class="m2"><p>کز آن اقبال می‌آید بهاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز لطف و حلم او بوده‌ست آن وصل</p></div>
<div class="m2"><p>نبود از عقل و فرهنگ و عیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پیر هندوی بگذشت لطفش</p></div>
<div class="m2"><p>چو ماهی گشت پیر از خوش عذاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین‌ها دیده‌ای از لطف و حسنش</p></div>
<div class="m2"><p>تو جانا کز پی او بی‌قراری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه سودم دارد ار صد ملک دارم</p></div>
<div class="m2"><p>که تو که جان آنی در فراری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خداوندی ز تو دور است ای دل</p></div>
<div class="m2"><p>که بی‌او یاوه گشته و بی‌مهاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزاران زخم دارد از تو ای هجر</p></div>
<div class="m2"><p>که این دم بر سر گنجش تو ماری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایا روز فراقم همچو قیری</p></div>
<div class="m2"><p>ایا روز وصالم همچو قاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو بودی در وصالش در قماری</p></div>
<div class="m2"><p>کنون تو با خیالش در قماری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هجر فخر ما شمس الحق و دین</p></div>
<div class="m2"><p>ایا صبرا نکردی هیچ یاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر صبری که رست از خاک تبریز</p></div>
<div class="m2"><p>خورم یابم دمی زو بردباری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببینا این فراق من فراقی</p></div>
<div class="m2"><p>ببینا بخت لنگم راهواری</p></div></div>