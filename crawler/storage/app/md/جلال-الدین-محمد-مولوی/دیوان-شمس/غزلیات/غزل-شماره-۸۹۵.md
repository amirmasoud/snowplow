---
title: >-
    غزل شمارهٔ ۸۹۵
---
# غزل شمارهٔ ۸۹۵

<div class="b" id="bn1"><div class="m1"><p>وسوسه تن گذشت غلغله جان رسید</p></div>
<div class="m2"><p>مور فروشد به گور چتر سلیمان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این فلک آتشی چند کند سرکشی</p></div>
<div class="m2"><p>نوح به کشتی نشست جوشش طوفان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند مخنث نژاد دعوی مردی کند</p></div>
<div class="m2"><p>رستم خنجر کشید سام و نریمان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جادوکانی ز فن چند عصا و رسن</p></div>
<div class="m2"><p>مار کنند از فریب موسی و ثعبان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد به پستی نشست صاف ز دردی برست</p></div>
<div class="m2"><p>گردن گرگان شکست یوسف کنعان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح دروغین گذشت صبح سعادت رسید</p></div>
<div class="m2"><p>جان شد و جان بقا از بر جانان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محنت ایوب را فاقه یعقوب را</p></div>
<div class="m2"><p>چاره دیگر نبود رحمت رحمان رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دزد کی باشد چو رفت شحنه ایمان به شهر</p></div>
<div class="m2"><p>شحنه کی باشد بگو چون شه و سلطان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صدق نگر بی‌نفاق وصل نگر بی‌فراق</p></div>
<div class="m2"><p>طاق طرنبین و طاق طاق شوم کان رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مفتعلن فاعلات جان مرا کرد مات</p></div>
<div class="m2"><p>جان خداخوان بمرد جان خدادان رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میوه دل می‌پزید روح از او می‌مزید</p></div>
<div class="m2"><p>باد کرم بروزید حرف پریشان رسید</p></div></div>