---
title: >-
    غزل شمارهٔ ۱۶۹
---
# غزل شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>رو ترش کن که همه روترشانند این جا</p></div>
<div class="m2"><p>کور شو تا نخوری از کف هر کور عصا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لنگ رو چونک در این کوی همه لنگانند</p></div>
<div class="m2"><p>لته بر پای بپیچ و کژ و مژ کن سر و پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زعفران بر رخ خود مال اگر مه رویی</p></div>
<div class="m2"><p>روی خوب ار بنمایی بخوری زخم قفا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آینه زیر بغل زن چو ببینی زشتی</p></div>
<div class="m2"><p>ور نه بدنام کنی آینه را ای مولا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که هشیاری و با خویش مدارا می‌کن</p></div>
<div class="m2"><p>چونک سرمست شدی هر چه که بادا بادا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساغری چند بخور از کف ساقی وصال</p></div>
<div class="m2"><p>چونک بر کار شدی برجه و در رقص درآ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد آن نقطه چو پرگار همی‌زن چرخی</p></div>
<div class="m2"><p>این چنین چرخ فریضه‌ست چنین دایره را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازگو آنچ بگفتی که فراموشم شد</p></div>
<div class="m2"><p>سلم الله علیک ای مه و مه پاره ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلم الله علیک ای همه ایام تو خوش</p></div>
<div class="m2"><p>سلم الله علیک ای دم یحیی الموتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم بد دور از آن رو که چو بربود دلی</p></div>
<div class="m2"><p>هیچ سودش نکند چاره و لا حول و لا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما به دریوزه حسن تو ز دور آمده‌ایم</p></div>
<div class="m2"><p>ماه را از رخ پرنور بود جود و سخا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماه بشنود دعای من و کف‌ها برداشت</p></div>
<div class="m2"><p>پیش ماه تو و می‌گفت مرا نیز مها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مه و خورشید و فلک‌ها و معانی و عقول</p></div>
<div class="m2"><p>سوی ما محتشمانند و به سوی تو گدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غیرتت لب بگزید و به دلم گفت خموش</p></div>
<div class="m2"><p>دل من تن زد و بنشست و بیفکند لوا</p></div></div>