---
title: >-
    غزل شمارهٔ ۲۴۰۰
---
# غزل شمارهٔ ۲۴۰۰

<div class="b" id="bn1"><div class="m1"><p>گل را نگر ز لطف سوی خار آمده</p></div>
<div class="m2"><p>دل ناز و باز کرده و دلدار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه را نگر برآمده مهمان شب شده</p></div>
<div class="m2"><p>دامن کشان ز عالم انوار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید را نگر که شهنشاه اختر است</p></div>
<div class="m2"><p>از بهر عذر گازر غمخوار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منگر به نقطه خوار تو آن را نگر که دوست</p></div>
<div class="m2"><p>اندر طواف نقطه چو پرگار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دلبری که دل ز همه دلبران ربود</p></div>
<div class="m2"><p>اندر وثاق این دل بیمار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این عشق همچو روح در این خاکدان غریب</p></div>
<div class="m2"><p>مانند مصطفاست به کفار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچون بهار سوی درختان خشک ما</p></div>
<div class="m2"><p>آن نوبهار حسن به ایثار آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنهان بود بهار ولی در اثر نگر</p></div>
<div class="m2"><p>زو باغ زنده گشته و در کار آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان را اگر نبینی در دلبران نگر</p></div>
<div class="m2"><p>با قد سرو و روی چو گلنار آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر عشق را نبینی در عاشقان نگر</p></div>
<div class="m2"><p>منصوروار شاد سوی دار آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عین مرگ چشمه آب حیات دید</p></div>
<div class="m2"><p>آن چشمه ای که مایه دیدار آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آمد بهار عشق به بستان جان درآ</p></div>
<div class="m2"><p>بنگر به شاخ و برگ به اقرار آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اقرار می‌کنند که حشر و قیامت است</p></div>
<div class="m2"><p>آن مردگان باغ دگربار آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای دل ز خود چو باخبری رو خموش کن</p></div>
<div class="m2"><p>چون بی‌خبر مباش به اخبار آمده</p></div></div>