---
title: >-
    غزل شمارهٔ ۱۵۸۰
---
# غزل شمارهٔ ۱۵۸۰

<div class="b" id="bn1"><div class="m1"><p>تا دلبر خویش را نبینیم</p></div>
<div class="m2"><p>جز در تک خون دل نشینیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به نشویم از نصیحت</p></div>
<div class="m2"><p>چون گمره عشق آن بهینیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر دل درد خانه داریم</p></div>
<div class="m2"><p>درمان نبود چو همچنینیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حلقه عاشقان قدسی</p></div>
<div class="m2"><p>سرحلقه چو گوهر نگینیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاشا که ز عقل و روح لافیم</p></div>
<div class="m2"><p>آتش در ما اگر همینیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از عقبات روح جستی</p></div>
<div class="m2"><p>مستانه مرو که در کمینیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون فتنه نشان آسمانیم</p></div>
<div class="m2"><p>چون است که فتنه زمینیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ساده‌تر از روان پاکیم</p></div>
<div class="m2"><p>پرنقش چرا مثال چینیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پژمرده شود هزار دولت</p></div>
<div class="m2"><p>ما تازه و تر چو یاسمینیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر متهمیم پیش هستی</p></div>
<div class="m2"><p>اندر تتق فنا امینیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما پشت بدین وجود داریم</p></div>
<div class="m2"><p>کاندر شکم فنا جنینیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تبریز ببین چه تاجداریم</p></div>
<div class="m2"><p>زان سر که غلام شمس دینیم</p></div></div>