---
title: >-
    غزل شمارهٔ ۲۷۴۸
---
# غزل شمارهٔ ۲۷۴۸

<div class="b" id="bn1"><div class="m1"><p>می‌آید سنجق بهاری</p></div>
<div class="m2"><p>لشکرکش شور و بی‌قراری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلزار نقاب می‌گشاید</p></div>
<div class="m2"><p>بلبل بگرفت باز زاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر کف بنهاده لاله جامی</p></div>
<div class="m2"><p>کای نرگس مست بر چه کاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز بنفشه در رکوع است</p></div>
<div class="m2"><p>می‌جوید از خدای یاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرها ز مغاره کرده بیرون</p></div>
<div class="m2"><p>آن لاله رخان کوهساری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا رب که که را همی‌فریبند</p></div>
<div class="m2"><p>خوش می‌نگرند در شکاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منگر به سمن به چشم خردی</p></div>
<div class="m2"><p>منگر به چمن به چشم خواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیرا به مسافران عزت</p></div>
<div class="m2"><p>گر خوار نظر کنی نیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشنو ز زبان سبز هر برگ</p></div>
<div class="m2"><p>کز عیب بروید آنچ کاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشته‌ست زبان گاو ناطق</p></div>
<div class="m2"><p>در حمد و ثنا و شکر آری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عذرت نبود ز یأس از آن کو</p></div>
<div class="m2"><p>بخشد به کلوخ خوش عذاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بابرگ شد آن کلوخ جان یافت</p></div>
<div class="m2"><p>در شکر نمود جان سپاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صد میوه چو شیشه‌های شربت</p></div>
<div class="m2"><p>هر یک مزه‌ای به خوشگواری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بعضی چو شکر اگر شکوری</p></div>
<div class="m2"><p>بعضی ترشند اگر خماری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاموش نشین و مستمع باش</p></div>
<div class="m2"><p>نی واعظ خلق شو نه قاری</p></div></div>