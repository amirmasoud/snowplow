---
title: >-
    غزل شمارهٔ ۳۱۹۱
---
# غزل شمارهٔ ۳۱۹۱

<div class="b" id="bn1"><div class="m1"><p>کالی تیشی آینوسؤای افندی چلبی</p></div>
<div class="m2"><p>نیمشب بر بام مایی، تا کرا می‌طلبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه سیه‌پوش و عصایی، که منم کالویروس</p></div>
<div class="m2"><p>گه عمامه و نیزه در کف که غریبم عربی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون عرب گردی، بگویی «فاعلاتن فلاعات</p></div>
<div class="m2"><p>ابصرالدنیا جمیعا فی قمیصی تختبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علت اولی نمودی خویش را با فلسفی</p></div>
<div class="m2"><p>چه زیان دارد ترا؟! تو یاربی و یاربی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چنینی، گر چنانی، جان مایی جان جان</p></div>
<div class="m2"><p>هر زبان خواهی بفرما، خسروا، شیرین لبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ارتمی اغاپسودی کایکا پراترا</p></div>
<div class="m2"><p>نور حقی یا تو حقی، یا فرشته یا نبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با نه اینی و نه آنی، صورت عشقی و بس</p></div>
<div class="m2"><p>با کدامین لشکری و در کدامین موکبی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون غم دل می‌خورم، یا رحم بر دل می‌برم</p></div>
<div class="m2"><p>کای دل مسکین، چرا اندر چنین تاب و تبی؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل همی گوید « برو من از کجا، تو از کجا!</p></div>
<div class="m2"><p>من دلم تو قالبی رو، رو، همی کن قالبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پوست‌ها را رنگ‌ها و مغزها را ذوق‌ها</p></div>
<div class="m2"><p>پوست‌ها با مغزها خود کی کند هم مذهبی؟! »</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کالی میراسس نزیتن بوستن کالاستن</p></div>
<div class="m2"><p>شب شما را روز گشت و نیست شب‌ها را شبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من خمش کردم، فسونم، بی‌زبان تعلیم ده</p></div>
<div class="m2"><p>ای ز تو لرزان و ترسان مشرقی و مغربی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس تبریزی، برآ چون آفتاب از شرق جان</p></div>
<div class="m2"><p>تا گشایند از میان زنار کفر و معجبی</p></div></div>