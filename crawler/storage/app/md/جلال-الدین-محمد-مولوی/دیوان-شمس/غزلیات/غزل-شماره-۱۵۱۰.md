---
title: >-
    غزل شمارهٔ ۱۵۱۰
---
# غزل شمارهٔ ۱۵۱۰

<div class="b" id="bn1"><div class="m1"><p>اگر عشقت به جای جان ندارم</p></div>
<div class="m2"><p>به زلف کافرت ایمان ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گفتی ننگ می داری ز عشقم</p></div>
<div class="m2"><p>غم عشق تو را پنهان ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو می گفتی مکن در من نگاهی</p></div>
<div class="m2"><p>که من خون‌ها کنم تاوان ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من سرگشته چون فرمان نبردم</p></div>
<div class="m2"><p>از آن بر نیک و بد فرمان ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو هر کس لطف می یابند از تو</p></div>
<div class="m2"><p>من بیچاره آخر جان ندارم</p></div></div>