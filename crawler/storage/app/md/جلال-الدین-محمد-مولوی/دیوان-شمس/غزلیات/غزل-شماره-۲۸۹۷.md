---
title: >-
    غزل شمارهٔ ۲۸۹۷
---
# غزل شمارهٔ ۲۸۹۷

<div class="b" id="bn1"><div class="m1"><p>بوی باغ و گلستان آید همی</p></div>
<div class="m2"><p>بوی یار مهربان آید همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نثار جوهر یارم مرا</p></div>
<div class="m2"><p>آب دریا تا میان آید همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خیال گلستانش خارزار</p></div>
<div class="m2"><p>نرمتر از پرنیان آید همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چنین نجار یعنی عشق او</p></div>
<div class="m2"><p>نردبان آسمان آید همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوع کلبم را ز مطبخ‌های جان</p></div>
<div class="m2"><p>لحظه لحظه بوی نان آید همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان در و دیوارهای کوی دوست</p></div>
<div class="m2"><p>عاشقان را بوی جان آید همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک وفا می‌آر و می‌بر صد هزار</p></div>
<div class="m2"><p>این چنین را آن چنان آید همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که میرد پیش حسن روی دوست</p></div>
<div class="m2"><p>نابمرده در جنان آید همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاروان غیب می‌آید به عین</p></div>
<div class="m2"><p>لیک از این زشتان نهان آید همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نغزرویان سوی زشتان کی روند</p></div>
<div class="m2"><p>بلبل اندر گلبنان آید همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پهلوی نرگس بروید یاسمین</p></div>
<div class="m2"><p>گل به غنچه خوش دهان آید همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این همه رمز است و مقصود این بود</p></div>
<div class="m2"><p>کان جهان اندر جهان آید همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو روغن در میان جان شیر</p></div>
<div class="m2"><p>لامکان اندر مکان آید همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچو عقل اندر میان خون و پوست</p></div>
<div class="m2"><p>بی نشان اندر نشان آید همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز ورای عقل عشق خوبرو</p></div>
<div class="m2"><p>می‌به کف دامن کشان آید همی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز ورای عشق آن کش شرح نیست</p></div>
<div class="m2"><p>جز همین گفتن که آن آید همی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیش از این شرحش توان کردن ولیک</p></div>
<div class="m2"><p>از سوی غیرت سنان آید همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تن زنم زیرا ز حرف مشکلش</p></div>
<div class="m2"><p>هر کسی را صد گمان آید همی</p></div></div>