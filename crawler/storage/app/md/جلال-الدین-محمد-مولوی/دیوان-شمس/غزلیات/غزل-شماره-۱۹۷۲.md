---
title: >-
    غزل شمارهٔ ۱۹۷۲
---
# غزل شمارهٔ ۱۹۷۲

<div class="b" id="bn1"><div class="m1"><p>عاشقا دو چشم بگشا چارجو در خود ببین</p></div>
<div class="m2"><p>جوی آب و جوی خمر و جوی شیر و انگبین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقا در خویش بنگر سخره مردم مشو</p></div>
<div class="m2"><p>تا فلان گوید چنان و آن فلان گوید چنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من غلام آن گل بینا که فارغ باشد او</p></div>
<div class="m2"><p>کان فلانم خار خواند وان فلانم یاسمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده بگشا زین سپس با دیده مردم مرو</p></div>
<div class="m2"><p>کان فلانت گبر گوید وان فلانت مرد دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خدا داده تو را چشم بصیرت از کرم</p></div>
<div class="m2"><p>کز خمارش سجده آرد شهپر روح الامین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم نرگس را مبند و چشم کرکس را مگیر</p></div>
<div class="m2"><p>چشم اول را مبند و چشم احول را مبین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان صورتی در صورتی افتاده‌اند</p></div>
<div class="m2"><p>چون مگس کز شهد افتد در طغار دوغگین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاد باش ای عشقباز ذوالجلال سرمدی</p></div>
<div class="m2"><p>با چنان پرها چه غم باشد تو را از آب و طین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر همی‌خواهی که جبریلت شود بنده برو</p></div>
<div class="m2"><p>سجده‌ای کن پیش آدم زود ای دیو لعین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بادیه خون خوار اگر واقف شدی از کعبه‌ام</p></div>
<div class="m2"><p>هر طرف گلشن نمودی هر طرف ماء معین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای به نظاره بد و نیک کسان درمانده</p></div>
<div class="m2"><p>چون بدین راضی شدی یارب تو را بادا معین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون امانت‌های حق را آسمان طاقت نداشت</p></div>
<div class="m2"><p>شمس تبریزی چگونه گستریدش در زمین</p></div></div>