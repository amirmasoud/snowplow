---
title: >-
    غزل شمارهٔ ۲۱۰۸
---
# غزل شمارهٔ ۲۱۰۸

<div class="b" id="bn1"><div class="m1"><p>می‌نروم هیچ از این خانه من</p></div>
<div class="m2"><p>در تک این خانه گرفتم وطن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه یار من و دارالقرار</p></div>
<div class="m2"><p>کفر بود نیت بیرون شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر نهم آن جا که سرم مست شد</p></div>
<div class="m2"><p>گوش نهم سوی تنن تنتنن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکته مگو هیچ به راهم مکن</p></div>
<div class="m2"><p>راه من این است تو راهم مزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه لیلی است و مجنون منم</p></div>
<div class="m2"><p>جان من این جاست برو جان مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کی در این خانه درآید ورا</p></div>
<div class="m2"><p>همچو منش باز بماند دهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیز ببند آن در اما چه سود</p></div>
<div class="m2"><p>قارع در گشت دو صد درشکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خنک آن را که سرش گرم شد</p></div>
<div class="m2"><p>ز آتش روی چو تو شیرین ذقن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن رخ چون ماه به برقع مپوش</p></div>
<div class="m2"><p>ای رخ تو حسرت هر مرد و زن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این در رحمت که گشادی مبند</p></div>
<div class="m2"><p>ای در تو قبله هر ممتحن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمع تویی شاهد تو باده تو</p></div>
<div class="m2"><p>هم تو سهیلی و عقیق یمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باقی عمر از تو نخواهم برید</p></div>
<div class="m2"><p>حلقه به گوش توام و مرتهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌نرمد شیر من از آتشت</p></div>
<div class="m2"><p>می‌نرمد پیل من از کرگدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو گل و من خار که پیوسته‌ایم</p></div>
<div class="m2"><p>بی‌گل و بی‌خار نباشد چمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من شب و تو ماه به تو روشنم</p></div>
<div class="m2"><p>جان شبی دل ز شبم برمکن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شمع تو پروانه جانم بسوخت</p></div>
<div class="m2"><p>سر پی شکرانه نهم بر لگن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان من و جان تو هر دو یکی است</p></div>
<div class="m2"><p>گشته یکی جان پنهان در دو تن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان من و تو چو یکی آفتاب</p></div>
<div class="m2"><p>روشن از او گشته هزار انجمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وقت حضور تو دو تا گشت جان</p></div>
<div class="m2"><p>رسته شد از تفرقه خویشتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تن زدم از غیرت و خامش شدم</p></div>
<div class="m2"><p>مطرب عشاق بگو تن مزن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خطه تبریز و رخ شمس دین</p></div>
<div class="m2"><p>ماهی جان راست چو بحر عدن</p></div></div>