---
title: >-
    غزل شمارهٔ ۳۰۸۹
---
# غزل شمارهٔ ۳۰۸۹

<div class="b" id="bn1"><div class="m1"><p>به اهل پرده اسرارها ببر خبری</p></div>
<div class="m2"><p>که پرده‌های شما بردرید از قمری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشسته بودند یک شب نجوم و سیارات</p></div>
<div class="m2"><p>برای طلعت آن آفتاب در سمری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برید غیرت شمشیر برکشید و برفت</p></div>
<div class="m2"><p>که در چه‌اید بگفتند نیستمان خبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برید غیرت واگشت و هر یکی می‌گفت</p></div>
<div class="m2"><p>به ناله‌های پرآتش که آه واحذری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبانگهانی عقرب چو کزدمک می‌رفت</p></div>
<div class="m2"><p>به گوش‌های سراپرده‌هاش بر خطری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که پاسبان سراپرده جلالت او</p></div>
<div class="m2"><p>به نفط قهر بزد تا بسوخت از شرری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریغ دیده بختم به کحل خاک درش</p></div>
<div class="m2"><p>ز بهر روشنی چشم یافتی نظری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که تا به قوت آن یک نظر بدو کردی</p></div>
<div class="m2"><p>که مهر و ماه نیابند اندر او اثری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که نسر طایر بگذشت از هوس آن سو</p></div>
<div class="m2"><p>به اعتماد که او راست بسته بال و پری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی مگس ز شکرهای بی‌کرانه او</p></div>
<div class="m2"><p>پرید در پی آن نسر و برسکست سری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بوی خمر رحیقش برون زند ز جهان</p></div>
<div class="m2"><p>خراب و مست ببینی به هر طرف عمری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به بر و بحر فتادست ولوله شادی</p></div>
<div class="m2"><p>که بحر رحمت پوشید قالب بشری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فکند ایمن و ساکن حذرکنان بلا</p></div>
<div class="m2"><p>سلاح‌ها بفراغت ز تیغ یا سپری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که ذره‌های هواها و قطره‌های بحار</p></div>
<div class="m2"><p>به گوش حلقه او کرد و بر میان کمری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو حق خدمت او ماجرا کند آغاز</p></div>
<div class="m2"><p>یقین شود همه را زانک نیستشان هنری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگارگر بگه نقش شهرها می‌کرد</p></div>
<div class="m2"><p>گشاد هندسه را پس مهندسانه دری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دررسید به تبریز و نقش او ناگاه</p></div>
<div class="m2"><p>برو فتاد شعاعات روح سیمبری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قلم شکست و بیفتاد بی‌خبر بر جای</p></div>
<div class="m2"><p>چو مستیان شبانه ز خوردن سکری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تمام چون کنم این را که خاطر از آتش</p></div>
<div class="m2"><p>همی‌گدازد در آب شکر چون شکری</p></div></div>