---
title: >-
    غزل شمارهٔ ۳۲۱۶
---
# غزل شمارهٔ ۳۲۱۶

<div class="b" id="bn1"><div class="m1"><p>طارت حیلی و زال حیلی</p></div>
<div class="m2"><p>اصبحت مکابدا لویلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد اظلم بالجوی نهاری</p></div>
<div class="m2"><p>کیف اخبرکم انا بلیلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما املاء عصتی و وجدی</p></div>
<div class="m2"><p>ما افرع من رضاک کیلی</p></div></div>