---
title: >-
    غزل شمارهٔ ۲۱۴۹
---
# غزل شمارهٔ ۲۱۴۹

<div class="b" id="bn1"><div class="m1"><p>ای تو خموش پرسخن چیست خبر بیا بگو</p></div>
<div class="m2"><p>سوره هل اتی بخوان نکته لافتی بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیمه جان بر اوج زن در دل بحر موج زن</p></div>
<div class="m2"><p>مشک وجود بردران ترک دو سه سقا بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونک ز خود سفر کنی وز دو جهان گذر کنی</p></div>
<div class="m2"><p>کیست کز او حذر کنی هیچ سخن مخا بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از می لعل پرگهر بی‌خبری و باخبر</p></div>
<div class="m2"><p>در دل ما بزن شرر بر سر ما برآ بگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی چرخ در طرب مجلس خاک خشک لب</p></div>
<div class="m2"><p>زین دو بزاده روز و شب چیست سبب مرا بگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل چرخ در زمین باغ و گل است و یاسمین</p></div>
<div class="m2"><p>باد خزانش در کمین چیست چنین چرا بگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخل و سخا و خیر و شر نیست جدا ز یک دگر</p></div>
<div class="m2"><p>نیست یکی و نیست دو چیست یکی دو تا بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل مست تا به کی ناله کنی ز ماه دی</p></div>
<div class="m2"><p>ذکر جفا بس است هی شکر کن از وفا بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ در این دو مرحله شکر تو نیست بی‌گله</p></div>
<div class="m2"><p>نقش فنا بشو هله ز آینه صفا بگو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جزو بهل ز کل بگو خار بهل ز گل بگو</p></div>
<div class="m2"><p>درگذر از صفات او ذات نگر خدا بگو</p></div></div>