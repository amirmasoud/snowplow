---
title: >-
    غزل شمارهٔ ۱۲۸۵
---
# غزل شمارهٔ ۱۲۸۵

<div class="b" id="bn1"><div class="m1"><p>شکست نرخ شکر را بتم به روی ترش</p></div>
<div class="m2"><p>چه باده‌هاست بتم را در آن کدوی ترش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قاصد او ترشست و به جان شیرینش</p></div>
<div class="m2"><p>که نیست در همه اجزاش تای موی ترش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار خمره سرکه عسل شدست از او</p></div>
<div class="m2"><p>که هست دلبر شیرین دوای خوی ترش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهای و هوی ترش‌های ماش خنده گرفت</p></div>
<div class="m2"><p>حلاوت عجبی یافت‌های و هوی ترش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترش چگونه نخندد به زیر لب چو شنید</p></div>
<div class="m2"><p>که جوی شیر و شکر شد روان به سوی ترش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ربود سیل ویم دوش و خلق نعره زنان</p></div>
<div class="m2"><p>میان جوی عسل چیست آن سبوی ترش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پریر یار مرا جست کان ترش رو کو</p></div>
<div class="m2"><p>خمار نیست چرا بودش آرزوی ترش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شتاب و تیز همی‌رفت کو به کو پی من</p></div>
<div class="m2"><p>چرا کند شکرقند جست و جوی ترش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفته طبله حلوا و بنده را جویان</p></div>
<div class="m2"><p>که تا ز جایزه شیرین کند گلوی ترش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب نباشد اگر قصد او فنای منست</p></div>
<div class="m2"><p>همیشه شیرین باشد یقین عدوی ترش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غلط مکن ترشی نی برای دفع توست</p></div>
<div class="m2"><p>ز رشک چون تو شکاریست رنگ و بوی ترش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رشک جاه امیرست روترش دربان</p></div>
<div class="m2"><p>ز رشک روی عروس است روی شوی ترش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار خانه چو زنبور پرعسل داری</p></div>
<div class="m2"><p>به جان تو که گذر کن ز گفت و گوی ترش</p></div></div>