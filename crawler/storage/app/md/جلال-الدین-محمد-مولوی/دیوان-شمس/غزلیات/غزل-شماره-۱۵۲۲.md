---
title: >-
    غزل شمارهٔ ۱۵۲۲
---
# غزل شمارهٔ ۱۵۲۲

<div class="b" id="bn1"><div class="m1"><p>ورا خواهم دگر یاری نخواهم</p></div>
<div class="m2"><p>چو گل را یافتم خاری نخواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را گر غیر او یار دگر هست</p></div>
<div class="m2"><p>برو آن جا که من باری نخواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز دیدار او بختی نجویم</p></div>
<div class="m2"><p>به غیر کار او کاری نخواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بازان ساعد سلطان گزیدم</p></div>
<div class="m2"><p>چو کرکس بوی مرداری نخواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان اهل دل جز دل نگنجد</p></div>
<div class="m2"><p>جز این دلدار دلداری نخواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من جزوی ستاند کل ببخشد</p></div>
<div class="m2"><p>از این به روز بازاری نخواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه آن جزوم که غیر کل بود آن</p></div>
<div class="m2"><p>نخواهم غیر را آری نخواهم</p></div></div>