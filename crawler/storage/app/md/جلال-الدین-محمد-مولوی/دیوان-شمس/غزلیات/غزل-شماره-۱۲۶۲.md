---
title: >-
    غزل شمارهٔ ۱۲۶۲
---
# غزل شمارهٔ ۱۲۶۲

<div class="b" id="bn1"><div class="m1"><p>گر جان به جز تو خواهد از خویش برکنیمش</p></div>
<div class="m2"><p>ور چرخ سرکش آید بر همدگر زنیمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رخت خویش خواهد ما رخت او دهیمش</p></div>
<div class="m2"><p>ور قلعه‌ها درآید ویرانه‌ها کنیمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر این جهان چو جانست ما جان جان جانیم</p></div>
<div class="m2"><p>ور این فلک سر آمد ما چشم روشنیمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیخ درخت خاکست وین چرخ شاخ و برگش</p></div>
<div class="m2"><p>عالم درخت زیتون ما همچو روغنیمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عشق شمس تبریز آهن ربای باشد</p></div>
<div class="m2"><p>ما بر طریق خدمت مانند آهنیمش</p></div></div>