---
title: >-
    غزل شمارهٔ ۱۸۸۳
---
# غزل شمارهٔ ۱۸۸۳

<div class="b" id="bn1"><div class="m1"><p>بی او نتوان رفتن بی‌او نتوان گفتن</p></div>
<div class="m2"><p>بی او نتوان شستن بی‌او نتوان خفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای حلقه زن این در در باز نتان کردن</p></div>
<div class="m2"><p>زیرا که تو هشیاری هر لحظه کشی گردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردن ز طمع خیزد زر خواهد و خون ریزد</p></div>
<div class="m2"><p>او عاشق گل خوردن همچون زن آبستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو عاشق شیرین خد زر بدهد و جان بدهد</p></div>
<div class="m2"><p>چون مرغ دل او پرد زین گنبد بی‌روزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این باید و آن باید از شرک خفی زاید</p></div>
<div class="m2"><p>آزاد بود بنده زین وسوسه چون سوسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن باید کو آرد او جمله گهر بارد</p></div>
<div class="m2"><p>یا رب که چه‌ها دارد آن ساقی شیرین فن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو خواجه به یک خانه شد خانه چو ویرانه</p></div>
<div class="m2"><p>او خواجه و من بنده پستی بود و روغن</p></div></div>