---
title: >-
    غزل شمارهٔ ۸۱۵
---
# غزل شمارهٔ ۸۱۵

<div class="b" id="bn1"><div class="m1"><p>شهر پر شد لولیان عقل دزد</p></div>
<div class="m2"><p>هم بدزدد هم بخواهد دستمزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که بتواند نگه دارد خرد</p></div>
<div class="m2"><p>من نتانستم مرا باری ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد من می‌گشت یک لولی پریر</p></div>
<div class="m2"><p>همچنینم برد کلی کرد و مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد لولی دست خود در خون من</p></div>
<div class="m2"><p>خون من در دست آن لولی فسرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که می‌شد خون من انگوروار</p></div>
<div class="m2"><p>سال‌ها انگور دل را می‌فشرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد دیدم کو کند دزدی ولیک</p></div>
<div class="m2"><p>کرد ما را بین که او دزدید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی گمان دارد که او دزدی کند</p></div>
<div class="m2"><p>خاصه شه صوفی شد آمد مو سترد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دزد خونی بین که هر کس را که کشت</p></div>
<div class="m2"><p>خضر و الیاسی شد و هرگز نمرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخت برد و بخت داد آنگه چه بخت</p></div>
<div class="m2"><p>سیم برد و دامن پرزر شمرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دردها و دردها را صاف کرد</p></div>
<div class="m2"><p>پیش او آرید هر جا هست درد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این جهان چشمست و او چون مردمک</p></div>
<div class="m2"><p>تنگ می‌آید جهان زین مرد خرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز رشک حق دهانم قفل کرد</p></div>
<div class="m2"><p>شد کلید و قفل را جایی سپرد</p></div></div>