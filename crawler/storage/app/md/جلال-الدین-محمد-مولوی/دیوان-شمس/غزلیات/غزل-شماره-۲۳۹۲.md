---
title: >-
    غزل شمارهٔ ۲۳۹۲
---
# غزل شمارهٔ ۲۳۹۲

<div class="b" id="bn1"><div class="m1"><p>ای کهربای عشقت دل را به خود کشیده</p></div>
<div class="m2"><p>دل رفته ما پی دل چون بی‌دلان دویده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دزدیده دل ز حسنت از عشق جامه واری</p></div>
<div class="m2"><p>تا شحنه فراقت دستان دل بریده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس شکر که جانم از مصر عشق خورده</p></div>
<div class="m2"><p>نی را ز ناله من در جان شکر دمیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سایه‌های عشقت ای خوش همای عرشی</p></div>
<div class="m2"><p>هر لحظه باز جان‌ها تا عرش برپریده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شاد مرغزاری کان جاست ورد و نسرین</p></div>
<div class="m2"><p>از آب عشق رسته وین آهوان چریده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده ندیده خود را و اکنون ز آینه تو</p></div>
<div class="m2"><p>هر دیده خویشتن را در آینه بدیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرنای دولت تو ای شمس حق تبریز</p></div>
<div class="m2"><p>گوش رباب جانی برتافته شنیده</p></div></div>