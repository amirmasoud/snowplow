---
title: >-
    غزل شمارهٔ ۴۷۷
---
# غزل شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>ز آفتاب سعادت مرا شراباتست</p></div>
<div class="m2"><p>که ذره‌های تنم حلقه خراباتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صلای چهره خورشید ما که فردوسست</p></div>
<div class="m2"><p>صلای سایه زلفین او که جناتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آسمان و زمین لطف ایتیا فرمود</p></div>
<div class="m2"><p>که آسمان و زمین مست آن مراعاتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هست و نیست برون‌ست تختگاه ملک</p></div>
<div class="m2"><p>هزار ساله از آن سوی نفی و اثباتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار در ز صفا اندرون دل بازست</p></div>
<div class="m2"><p>شتاب کن که ز تأخیرها بس آفاتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیات‌های حیات آفرین بود آن جا</p></div>
<div class="m2"><p>از آنک شاه حقایق نه شاه شهماتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نردبان درون هر نفس به معراجند</p></div>
<div class="m2"><p>پیاله‌های پر از خون نگر که آیاتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن هوا که خداوند شمس تبریزیست</p></div>
<div class="m2"><p>نه لاف چرخه چرخ‌ست و نی سماواتست</p></div></div>