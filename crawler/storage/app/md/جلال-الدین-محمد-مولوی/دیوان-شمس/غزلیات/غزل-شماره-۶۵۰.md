---
title: >-
    غزل شمارهٔ ۶۵۰
---
# غزل شمارهٔ ۶۵۰

<div class="b" id="bn1"><div class="m1"><p>آن سرخ قبایی که چو مه پار برآمد</p></div>
<div class="m2"><p>امسال در این خرقه زنگار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ترک که آن سال به یغماش بدیدی</p></div>
<div class="m2"><p>آنست که امسال عرب وار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یار همانست اگر جامه دگر شد</p></div>
<div class="m2"><p>آن جامه بدل کرد و دگربار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن باده همانست اگر شیشه بدل شد</p></div>
<div class="m2"><p>بنگر که چه خوش بر سر خمار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب رفت حریفان صبوحی به کجایید</p></div>
<div class="m2"><p>کان مشعله از روزن اسرار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رومی پنهان گشت چو دوران حبش دید</p></div>
<div class="m2"><p>امروز در این لشکر جرار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس الحق تبریز رسیدست بگویید</p></div>
<div class="m2"><p>کز چرخ صفا آن مه انوار برآمد</p></div></div>