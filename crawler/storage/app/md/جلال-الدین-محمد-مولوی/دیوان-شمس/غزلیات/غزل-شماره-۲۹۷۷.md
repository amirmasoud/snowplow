---
title: >-
    غزل شمارهٔ ۲۹۷۷
---
# غزل شمارهٔ ۲۹۷۷

<div class="b" id="bn1"><div class="m1"><p>هر روز بامداد درآید یکی پری</p></div>
<div class="m2"><p>بیرون کشد مرا که ز من جان کجا بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عاشقی نیابی مانند من بتی</p></div>
<div class="m2"><p>ور تاجری کجاست چو من گرم مشتری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور عارفی حقیقت معروف جان منم</p></div>
<div class="m2"><p>ور کاهلی چنان شوی از من که برپری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور حس فاسدی دهمت نور مصطفی</p></div>
<div class="m2"><p>ور مس کاسدی کنمت زر جعفری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محتاج روی مایی گر پشت عالمی</p></div>
<div class="m2"><p>محتاج آفتابی گر صبح انوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بر و بحر بگذر و بر کوه قاف رو</p></div>
<div class="m2"><p>بر خشک و بر تری منشین زین دو برتری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل اگر دلی دل از آن یار درمدزد</p></div>
<div class="m2"><p>وی سر اگر سری مکن این سجده سرسری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون اسب می‌گریزی و من بر توام سوار</p></div>
<div class="m2"><p>مگریز از او که بر تو بود کان بود خری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد حیله گر تراشی و صد شهر اگر روی</p></div>
<div class="m2"><p>قربان عید خنجر الله اکبری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاموش اگر چه بحر دهد در بی‌دریغ</p></div>
<div class="m2"><p>لیکن مباح نیست که من رام یشتری</p></div></div>