---
title: >-
    غزل شمارهٔ ۲۷۳۹
---
# غزل شمارهٔ ۲۷۳۹

<div class="b" id="bn1"><div class="m1"><p>ای یار یگانه چند خسبی</p></div>
<div class="m2"><p>وی شاه زمانه چند خسبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر روزن توست بنده از کی</p></div>
<div class="m2"><p>ای رونق خانه چند خسبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای کرده به زه کمان ابرو</p></div>
<div class="m2"><p>برزن به نشانه چند خسبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افسانه ما شنو که در عشق</p></div>
<div class="m2"><p>گشتیم فسانه چند خسبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماییم چو میخ سر نهاده</p></div>
<div class="m2"><p>بر روی ستانه چند خسبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خنب ببسته است پیش آر</p></div>
<div class="m2"><p>باقی شبانه چند خسبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درده قدح شراب و چون شمع</p></div>
<div class="m2"><p>بنشین به میانه چند خسبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشتاب مها که این شب قدر</p></div>
<div class="m2"><p>آمد به کرانه چند خسبی</p></div></div>