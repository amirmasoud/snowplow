---
title: >-
    غزل شمارهٔ ۳۰۲۴
---
# غزل شمارهٔ ۳۰۲۴

<div class="b" id="bn1"><div class="m1"><p>بستگی این سماع هست ز بیگانه‌ای</p></div>
<div class="m2"><p>ز ارچلی جغد گشت حلقه چو ویرانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنک بود همچو برف سرد کند وقت را</p></div>
<div class="m2"><p>چون بگدازد چو سیل پست کند خانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر برونی بدست غیر درونی بتر</p></div>
<div class="m2"><p>از سبب غیریست کندن دندانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد خزانست غیر زرد کند باغ را</p></div>
<div class="m2"><p>حبس کند در زمین خوبی هر دانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش تو خندد چو گل پای درآید چو خار</p></div>
<div class="m2"><p>ریش نگه دار از آن دوسر چون شانه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سبب آنک بد در صف ترسنده‌ای</p></div>
<div class="m2"><p>گشت شکسته بسی لشکر مردانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو تبریزیی شمس حق و دین که او</p></div>
<div class="m2"><p>شمع همه جمع‌هاست من شده پروانه‌ای</p></div></div>