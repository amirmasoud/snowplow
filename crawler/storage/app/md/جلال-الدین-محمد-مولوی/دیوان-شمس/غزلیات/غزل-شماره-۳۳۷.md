---
title: >-
    غزل شمارهٔ ۳۳۷
---
# غزل شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>ببستی چشم یعنی وقت خواب است</p></div>
<div class="m2"><p>نه خوابست آن حریفان را جواب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو می‌دانی که ما چندان نپاییم</p></div>
<div class="m2"><p>ولیکن چشم مستت را شتاب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جفا می‌کن جفاات جمله لطف است</p></div>
<div class="m2"><p>خطا می‌کن خطای تو صواب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو چشم آتشین در خواب می‌کن</p></div>
<div class="m2"><p>که ما را چشم و دل باری کباب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی سرها ربوده چشم ساقی</p></div>
<div class="m2"><p>به شمشیری که آن یک قطره آب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی گوید که این از عشق ساقیست</p></div>
<div class="m2"><p>یکی گوید که این فعل شراب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می و ساقی چه باشد نیست جز حق</p></div>
<div class="m2"><p>خدا داند که این عشق از چه باب است</p></div></div>