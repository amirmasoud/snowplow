---
title: >-
    غزل شمارهٔ ۱۴۹۰
---
# غزل شمارهٔ ۱۴۹۰

<div class="b" id="bn1"><div class="m1"><p>از شهر تو رفتیم تو را سیر ندیدیم</p></div>
<div class="m2"><p>از شاخ درخت تو چنین خام فتیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سایه سرو تو مها سیر نخفتیم</p></div>
<div class="m2"><p>وز باغ تو از بیم نگهبان نچریدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر تابه سودای تو گشتیم چو ماهی</p></div>
<div class="m2"><p>تا سوخته گشتیم ولیکن نپزیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشتیم به ویرانه به سودای چو تو گنج</p></div>
<div class="m2"><p>چون مار به آخر به تک خاک خزیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سایه گذشتیم به هر پاکی و ناپاک</p></div>
<div class="m2"><p>اکنون به تو محویم نه پاک و نه پلیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را چو بجویید بر دوست بجویید</p></div>
<div class="m2"><p>کز پوست فناییم و بر دوست پدیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بر نمک و نان تو انگشت زدستیم</p></div>
<div class="m2"><p>در فرقت و در شور بس انگشت گزیدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون طبل رحیل آمد و آواز جرس‌ها</p></div>
<div class="m2"><p>ما رخت و قماشات بر افلاک کشیدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر است که تریاق تو با ماست اگر چه</p></div>
<div class="m2"><p>زهری که همه خلق چشیدند چشیدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دم که بریده شد از این جوی جهان آب</p></div>
<div class="m2"><p>چون ماهی بی‌آب بر این خاک طپیدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون جوی شد این چشم ز بی‌آبی آن جوی</p></div>
<div class="m2"><p>تا عاقبت امر به سرچشمه رسیدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون صبر فرج آمد و بی‌صبر حرج بود</p></div>
<div class="m2"><p>خاموش مکن ناله که ما صبر گزیدیم</p></div></div>