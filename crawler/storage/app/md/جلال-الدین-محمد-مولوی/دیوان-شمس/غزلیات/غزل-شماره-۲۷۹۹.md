---
title: >-
    غزل شمارهٔ ۲۷۹۹
---
# غزل شمارهٔ ۲۷۹۹

<div class="b" id="bn1"><div class="m1"><p>بی گهان شد هر رفتن سوی روزن ننگری</p></div>
<div class="m2"><p>آتشی اندرزنی از سوی مه در مشتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منگر آخر سوی روزن سوی روی من نگر</p></div>
<div class="m2"><p>تا ز روی من به روزن‌های غیبی بنگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی زرینم به هر سو شش جهت را لعل کرد</p></div>
<div class="m2"><p>تا ز لعل تو بیاموزید رویم زرگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شش جهت گوساله‌ای زرین و بانگش بانگ زر</p></div>
<div class="m2"><p>گاوکان بر بانگ زر مستان سحر سامری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیرگیرا گاو و گوساله به بانگ زر سپار</p></div>
<div class="m2"><p>چونک شیر و شیرگیر جام صرف احمری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمن اسلام زلف کافرت ما را بگفت</p></div>
<div class="m2"><p>دور شو گر مؤمنی و پیشم آ گر کافری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش این لاف‌ها از شمس تبریزیستت</p></div>
<div class="m2"><p>گفت آری و برون آورد مهر دلبری</p></div></div>