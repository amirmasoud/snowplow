---
title: >-
    غزل شمارهٔ ۱۸۹۶
---
# غزل شمارهٔ ۱۸۹۶

<div class="b" id="bn1"><div class="m1"><p>نشاید از تو چندین جور کردن</p></div>
<div class="m2"><p>نشاید خون مظلومان به گردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا بهر تو باید زندگانی</p></div>
<div class="m2"><p>وگر نی سهل دارم جان سپردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن روزی که نام تو شنیدم</p></div>
<div class="m2"><p>شدم عاجز من از شب‌ها شمردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روا باشد که از چون تو کریمی</p></div>
<div class="m2"><p>نصیب من بود افسوس خوردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خداوندا از آن خوشتر چه باشد</p></div>
<div class="m2"><p>بدیدن روی تو پیش تو مردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثال شمع شد خونم در آتش</p></div>
<div class="m2"><p>ز دل جوشیدن و بر رخ فسردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در این زندان مرا کند است دندان</p></div>
<div class="m2"><p>از این صبر و از این دندان فشردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از این خانه شدم من سیر وقت است</p></div>
<div class="m2"><p>به بام آسمان‌ها رخت بردن</p></div></div>