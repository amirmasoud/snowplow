---
title: >-
    غزل شمارهٔ ۱۰۹۷
---
# غزل شمارهٔ ۱۰۹۷

<div class="b" id="bn1"><div class="m1"><p>عقل بند رهروانست ای پسر</p></div>
<div class="m2"><p>بند بشکن ره عیانست ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل بند و دل فریب و جان حجاب</p></div>
<div class="m2"><p>راه از این هر سه نهانست ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ز عقل و جان و دل برخاستی</p></div>
<div class="m2"><p>این یقین هم در گمانست ای پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد کو از خود نرفت او مرد نیست</p></div>
<div class="m2"><p>عشق بی‌درد آفسانست ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه خود را هدف کن پیش دوست</p></div>
<div class="m2"><p>هین که تیرش در کمانست ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینه‌ای کز زخم تیرش خسته شد</p></div>
<div class="m2"><p>در جبینش صد نشانست ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق کار نازکان نرم نیست</p></div>
<div class="m2"><p>عشق کار پهلوانست ای پسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کی او مر عاشقان را بنده شد</p></div>
<div class="m2"><p>خسرو و صاحب قرانست ای پسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق را از کس مپرس از عشق پرس</p></div>
<div class="m2"><p>عشق ابر درفشانست ای پسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترجمانی منش محتاج نیست</p></div>
<div class="m2"><p>عشق خود را ترجمانست ای پسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر روی بر آسمان هفتمین</p></div>
<div class="m2"><p>عشق نیکونردبانست ای پسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کجا که کاروانی می‌رود</p></div>
<div class="m2"><p>عشق قبله کاروانست ای پسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این جهان از عشق تا نفریبدت</p></div>
<div class="m2"><p>کاین جهان از تو جهانست ای پسر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هین دهان بربند و خامش چون صدف</p></div>
<div class="m2"><p>کاین زبانت خصم جانست ای پسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمس تبریز آمد و جان شادمان</p></div>
<div class="m2"><p>چونک با شمسش قرانست ای پسر</p></div></div>