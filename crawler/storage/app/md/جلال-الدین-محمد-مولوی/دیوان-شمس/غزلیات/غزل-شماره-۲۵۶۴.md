---
title: >-
    غزل شمارهٔ ۲۵۶۴
---
# غزل شمارهٔ ۲۵۶۴

<div class="b" id="bn1"><div class="m1"><p>گر عشق بزد راهم ور عقل شد از مستی</p></div>
<div class="m2"><p>ای دولت و اقبالم آخر نه توام هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رستن ز جهان شک هرگز نبود اندک</p></div>
<div class="m2"><p>خاک کف پای شه کی باشد سردستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای طوطی جان پر زن بر خرمن شکر زن</p></div>
<div class="m2"><p>بر عمر موفر زن کز بند قفس رستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان سوی جانان رو در حلقه مردان رو</p></div>
<div class="m2"><p>در روضه و بستان رو کز هستی خود جستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیرت تو ماندم از گریه و از خنده</p></div>
<div class="m2"><p>با رفعت تو رستم از رفعت و از پستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل بزن انگشتک بی‌زحمت لی و لک</p></div>
<div class="m2"><p>در دولت پیوسته رفتی و بپیوستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن باده فروش تو بس گفت به گوش تو</p></div>
<div class="m2"><p>جان‌ها بپرستندت گر جسم بنپرستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خواجه شنگولی ای فتنه صد لولی</p></div>
<div class="m2"><p>بشتاب چه می مولی آخر دل ما خستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خیر و شرت باشد ور کر و فرت باشد</p></div>
<div class="m2"><p>ور صد هنرت باشد آخر نه در آن شستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چالاک کسی یارا با آن دل چون خارا</p></div>
<div class="m2"><p>تا ره نزدی ما را از پای بننشستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درجست در این گفتن بنمودن و بنهفتن</p></div>
<div class="m2"><p>یک پرده برافکندی صد پرده نو بستی</p></div></div>