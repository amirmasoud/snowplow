---
title: >-
    غزل شمارهٔ ۵۴۹
---
# غزل شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>آب زنید راه را هین که نگار می‌رسد</p></div>
<div class="m2"><p>مژده دهید باغ را بوی بهار می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه دهید یار را آن مه ده چهار را</p></div>
<div class="m2"><p>کز رخ نوربخش او نور نثار می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاک شدست آسمان غلغله ایست در جهان</p></div>
<div class="m2"><p>عنبر و مشک می‌دمد سنجق یار می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رونق باغ می‌رسد چشم و چراغ می‌رسد</p></div>
<div class="m2"><p>غم به کناره می‌رود مه به کنار می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر روانه می‌رود سوی نشانه می‌رود</p></div>
<div class="m2"><p>ما چه نشسته‌ایم پس شه ز شکار می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغ سلام می‌کند سرو قیام می‌کند</p></div>
<div class="m2"><p>سبزه پیاده می‌رود غنچه سوار می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوتیان آسمان تا چه شراب می‌خورند</p></div>
<div class="m2"><p>روح خراب و مست شد عقل خمار می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون برسی به کوی ما خامشی است خوی ما</p></div>
<div class="m2"><p>زان که ز گفت و گوی ما گرد و غبار می‌رسد</p></div></div>