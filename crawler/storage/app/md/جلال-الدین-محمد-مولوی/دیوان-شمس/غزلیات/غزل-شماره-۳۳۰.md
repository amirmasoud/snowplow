---
title: >-
    غزل شمارهٔ ۳۳۰
---
# غزل شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>بار دگر آن دلبر عیار مرا یافت</p></div>
<div class="m2"><p>سرمست همی‌گشت به بازار مرا یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنهان شدم از نرگس مخمور مرا دید</p></div>
<div class="m2"><p>بگریختم از خانه خمار مرا یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگریختنم چیست کز او جان نبرد کس</p></div>
<div class="m2"><p>پنهان شدنم چیست چو صد بار مرا یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که در انبوهی شهرم که بیابد</p></div>
<div class="m2"><p>آن کس که در انبوهی اسرار مرا یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مژده که آن غمزه غماز مرا جست</p></div>
<div class="m2"><p>وی بخت که آن طره طرار مرا یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستار ربود از سر مستان به گروگان</p></div>
<div class="m2"><p>دستار برو گوشه دستار مرا یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از کف پا خار همی‌کردم بیرون</p></div>
<div class="m2"><p>آن سرو دو صد گلشن و گلزار مرا یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گلشن خود بر سر من یار گل افشاند</p></div>
<div class="m2"><p>وان بلبل وان نادره تکرار مرا یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من گم شدم از خرمن آن ماه چو کیله</p></div>
<div class="m2"><p>امروز مه اندر بن انبار مرا یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خون من آثار به هر راه چکیدست</p></div>
<div class="m2"><p>اندر پی من بود به آثار مرا یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون آهو از آن شیر رمیدم به بیابان</p></div>
<div class="m2"><p>آن شیر گه صید به کهسار مرا یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن کس که به گردون رود و گیرد آهو</p></div>
<div class="m2"><p>با صبر و تأنی و به هنجار مرا یافت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در کام من این شست و من اندر تک دریا</p></div>
<div class="m2"><p>صاید به سررشته جرار مرا یافت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جامی که برد از دلم آزار به من داد</p></div>
<div class="m2"><p>آن لحظه که آن یار کم آزار مرا یافت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این جان گران جان سبکی یافت و بپرید</p></div>
<div class="m2"><p>کان رطل گران سنگ سبکسار مرا یافت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>امروز نه هوش است و نه گوش است و نه گفتار</p></div>
<div class="m2"><p>کان اصل هر اندیشه و گفتار مرا یافت</p></div></div>