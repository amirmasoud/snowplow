---
title: >-
    غزل شمارهٔ ۲۳۹۵
---
# غزل شمارهٔ ۲۳۹۵

<div class="b" id="bn1"><div class="m1"><p>دیدم نگار خود را می‌گشت گرد خانه</p></div>
<div class="m2"><p>برداشته ربابی می‌زد یکی ترانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زخمه چو آتش می‌زد ترانه خوش</p></div>
<div class="m2"><p>مست و خراب و دلکش از باده مغانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پرده عراقی می‌زد به نام ساقی</p></div>
<div class="m2"><p>مقصود باده بودش ساقی بدش بهانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی ماه رویی در دست او سبویی</p></div>
<div class="m2"><p>از گوشه‌ای درآمد بنهاد در میانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر کرد جام اول زان باده مشعل</p></div>
<div class="m2"><p>در آب هیچ دیدی کآتش زند زبانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کف نهاده آن را از بهر دلستان را</p></div>
<div class="m2"><p>آنگه بکرد سجده بوسید آستانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بستد نگار از وی اندرکشید آن می</p></div>
<div class="m2"><p>شد شعله‌ها از آن می بر روی او دوانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌دید حسن خود را می‌گفت چشم بد را</p></div>
<div class="m2"><p>نی بود و نی بیاید چون من در این زمانه</p></div></div>