---
title: >-
    غزل شمارهٔ ۹۲۶
---
# غزل شمارهٔ ۹۲۶

<div class="b" id="bn1"><div class="m1"><p>حبیب کعبه جانست اگر نمی‌دانید</p></div>
<div class="m2"><p>به هر طرف که بگردید رو بگردانید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که جان ویست به عالم اگر شما جسمید</p></div>
<div class="m2"><p>که جان جمله جان‌هاست اگر شما جانید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندا برآمد امشب که جان کیست فدا</p></div>
<div class="m2"><p>بجست جان من از جا که نقد بستانید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار نکته نبشتست عشق بر رویم</p></div>
<div class="m2"><p>ز حال دل چو شما عاشقید برخوانید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه ساغرست که هر دم به عاشقان آید</p></div>
<div class="m2"><p>شما کشید چنین ساغری که مردانید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که عشق باغ و تماشاست اگر ملول شوید</p></div>
<div class="m2"><p>هواش مرکب تازیست اگر فرومانید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آب و نان همه ماهیان ز بحر بود</p></div>
<div class="m2"><p>چو ماهیید چرا عاشق لب نانید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قرابه ایست پر از رنج و نام او جسمست</p></div>
<div class="m2"><p>به سنگ بربزنید و تمام برهانید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مرغ در قفسم بهر شمس تبریزی</p></div>
<div class="m2"><p>ز دشمنی قفسم بشکنید و بدرانید</p></div></div>