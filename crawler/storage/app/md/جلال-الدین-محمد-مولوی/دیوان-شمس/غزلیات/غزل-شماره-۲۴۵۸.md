---
title: >-
    غزل شمارهٔ ۲۴۵۸
---
# غزل شمارهٔ ۲۴۵۸

<div class="b" id="bn1"><div class="m1"><p>سنگ مزن بر طرف کارگه شیشه گری</p></div>
<div class="m2"><p>زخم مزن بر جگر خسته خسته جگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دل من زن همه را ز آنک دریغ است و غبین</p></div>
<div class="m2"><p>زخم تو و سنگ تو بر سینه و جان دگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازرهان جمله اسیران جفا را جز من</p></div>
<div class="m2"><p>تا به جفا هم نکنی در جز بنده نظری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم به وفا با تو خوشم هم به جفا با تو خوشم</p></div>
<div class="m2"><p>نی به وفا نی به جفا بی‌تو مبادم سفری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونک خیالت نبود آمده در چشم کسی</p></div>
<div class="m2"><p>چشم بز کشته بود تیره و خیره نگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ز زندان جهان با تو بدم من همگی</p></div>
<div class="m2"><p>کاش بر این دامگهم هیچ نبودی گذری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند بگفتم که خوشم هیچ سفر می‌نروم</p></div>
<div class="m2"><p>این سفر صعب نگر ره ز علی تا به ثری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لطف تو بفریفت مرا گفت برو هیچ مرم</p></div>
<div class="m2"><p>بدرقه باشد کرمم بر تو نباشد خطری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به غریبی بروی فرجه کنی پخته شوی</p></div>
<div class="m2"><p>بازبیایی به وطن باخبری پرهنری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم ای جان خبر بی‌تو خبر را چه کنم</p></div>
<div class="m2"><p>بهر خبر خود که رود از تو مگر بی‌خبری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ز کفت باده کشم بی‌خبر و مست و خوشم</p></div>
<div class="m2"><p>بی‌خطر و خوف کسی بی‌شر و شور بشری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت به گوشم سخنان چون سخن راه زنان</p></div>
<div class="m2"><p>برد مرا شاه ز سر کرد مرا خیره سری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قصه دراز است بلی آه ز مکر و دغلی</p></div>
<div class="m2"><p>گر ننماید کرمش این شب ما را سحری</p></div></div>