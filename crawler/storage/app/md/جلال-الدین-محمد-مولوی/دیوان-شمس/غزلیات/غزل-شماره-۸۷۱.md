---
title: >-
    غزل شمارهٔ ۸۷۱
---
# غزل شمارهٔ ۸۷۱

<div class="b" id="bn1"><div class="m1"><p>آمد بهار خرم و رحمت نثار شد</p></div>
<div class="m2"><p>سوسن چو ذوالفقار علی آبدار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجزای خاک حامله بودند از آسمان</p></div>
<div class="m2"><p>نه ماه گشت حامله زان بی‌قرار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلنار پرگره شد و جوبار پرزره</p></div>
<div class="m2"><p>صحرا پر از بنفشه و که لاله زار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشکوفه لب گشاد که هنگام بوسه گشت</p></div>
<div class="m2"><p>بگشاد سر و دست که وقت کنار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلزار چرخ چونک گلستان دل بدید</p></div>
<div class="m2"><p>در رو کشید ابر و ز دل شرمسار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن خار می‌گریست که ای عیب پوش خلق</p></div>
<div class="m2"><p>شد مستجاب دعوت او گلعذار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه بهار بست کمر را به معذرت</p></div>
<div class="m2"><p>هر شاخ و هر درخت از او تاجدار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چوب در تجمل چون بزم میر گشت</p></div>
<div class="m2"><p>گر در دو دست موسی یک چوب مار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنده شدند بار دگر کشتگان دی</p></div>
<div class="m2"><p>تا منکر قیامت بی‌اعتبار شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اصحاب کهف باغ ز خواب اندرآمدند</p></div>
<div class="m2"><p>چون لطف روح بخش خدا یار غار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای زنده گشتگان به زمستان کجا بدیت</p></div>
<div class="m2"><p>آن سو که وقت خواب روان را مطار شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن سو که هر شبی بپرد این حواس و روح</p></div>
<div class="m2"><p>آن سو که هر شبی نظر و انتظار شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مه چون هلال بود سفر کرد آن طرف</p></div>
<div class="m2"><p>بدری منور آمد و شمع دیار شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این پنج حس ظاهر و پنج دگر نهان</p></div>
<div class="m2"><p>لنگ و ملول رفت و سحر راهوار شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بربند این دهان و مپیمای باد بیش</p></div>
<div class="m2"><p>کز باد گفت راه نظر پرغبار شد</p></div></div>