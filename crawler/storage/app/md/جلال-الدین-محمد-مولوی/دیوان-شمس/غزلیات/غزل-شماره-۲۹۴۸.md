---
title: >-
    غزل شمارهٔ ۲۹۴۸
---
# غزل شمارهٔ ۲۹۴۸

<div class="b" id="bn1"><div class="m1"><p>ای کرده رو چو سرکه چه گردد ار بخندی</p></div>
<div class="m2"><p>والله ز سرکه رویی تو هیچ برنبندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخی ستان شکر ده سیلی بنوش و سر ده</p></div>
<div class="m2"><p>خندان بمیر چون گل گر ز آنک ارجمندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مو شده‌ست آن مه در خنده است و قهقه</p></div>
<div class="m2"><p>چت کم شود که گه گه از خوی ماه رندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشکفته است شوره تو غوره‌ای و غوره</p></div>
<div class="m2"><p>آخر تو جان نداری تا چند مستمندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با کان غم نشینی شادی چگونه بینی</p></div>
<div class="m2"><p>از موش و موش خانه کی یافت کس بلندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالای چرخ نیلی یابند جبرئیلی</p></div>
<div class="m2"><p>وز خاک پای پاکان یابند بی‌گزندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان رنگ روی و سیما اسرار توست پیدا</p></div>
<div class="m2"><p>کاندر کدام کویی چه یار می‌پسندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون چشم می‌گشاید در چشم می‌نماید</p></div>
<div class="m2"><p>گر ز آنک ریش گاوی ور شیر هوشمندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قارون مثال دلوی در قعر چه فروشد</p></div>
<div class="m2"><p>عیسی به بام گردون بنمود خوش کمندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر دلو سر برآرد جز آب چه ندارد</p></div>
<div class="m2"><p>پاره شود بپوسد در ظلمت و نژندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای لولیان لالا بالا پریده بالا</p></div>
<div class="m2"><p>وارسته زین هیولا فارغ ز چون و چندی</p></div></div>