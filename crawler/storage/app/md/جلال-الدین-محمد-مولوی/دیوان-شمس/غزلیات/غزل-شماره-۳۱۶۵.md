---
title: >-
    غزل شمارهٔ ۳۱۶۵
---
# غزل شمارهٔ ۳۱۶۵

<div class="b" id="bn1"><div class="m1"><p>جان و جهان! دوش کجا بوده‌ای</p></div>
<div class="m2"><p>نی غلطم، در دل ما بوده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش ز هجر تو جفا دیده‌ام</p></div>
<div class="m2"><p>ای که تو سلطان وفا بوده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه که من دوش چه سان بوده‌ام!</p></div>
<div class="m2"><p>آه که تو دوش کرا بوده‌ای!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشک برم کاش قبا بودمی</p></div>
<div class="m2"><p>چونک در آغوش قبا بوده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهره ندارم که بگویم ترا</p></div>
<div class="m2"><p>« بی من بیچاره چرا بوده‌ای؟! »</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار سبک روح! به وقت گریز</p></div>
<div class="m2"><p>تیزتر از باد صبا بوده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌تو مرا رنج و بلا بند کرد</p></div>
<div class="m2"><p>باش که تو بنده بلا بوده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگ رخ خوب تو آخر گواست</p></div>
<div class="m2"><p>در حرم لطف خدا بوده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگ تو داری، که زرنگ جهان</p></div>
<div class="m2"><p>پاکی، و همرنگ بقا بوده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آینهٔ رنگ تو عکس کسیست</p></div>
<div class="m2"><p>تو ز همه رنگ جدا بوده‌ای</p></div></div>