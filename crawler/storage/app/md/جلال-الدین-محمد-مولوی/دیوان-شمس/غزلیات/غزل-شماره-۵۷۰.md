---
title: >-
    غزل شمارهٔ ۵۷۰
---
# غزل شمارهٔ ۵۷۰

<div class="b" id="bn1"><div class="m1"><p>بهار آمد بهار آمد بهار خوش عذار آمد</p></div>
<div class="m2"><p>خوش و سرسبز شد عالم اوان لاله زار آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سوسن بشنو ای ریحان که سوسن صد زبان دارد</p></div>
<div class="m2"><p>به دشت آب و گل بنگر که پرنقش و نگار آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل از نسرین همی‌پرسد که چون بودی در این غربت</p></div>
<div class="m2"><p>همی‌گوید خوشم زیرا خوشی‌ها زان دیار آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سمن با سرو می‌گوید که مستانه همی‌رقصی</p></div>
<div class="m2"><p>به گوشش سرو می‌گوید که یار بردبار آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنفشه پیش نیلوفر درآمد که مبارک باد</p></div>
<div class="m2"><p>که زردی رفت و خشکی رفت و عمر پایدار آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی‌زد چشمک آن نرگس به سوی گل که خندانی</p></div>
<div class="m2"><p>بدو گفتا که خندانم که یار اندر کنار آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صنوبر گفت راه سخت آسان شد به فضل حق</p></div>
<div class="m2"><p>که هر برگی به ره بری چو تیغ آبدار آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ترکستان آن دنیا بنه ترکان زیبارو</p></div>
<div class="m2"><p>به هندستان آب و گل به امر شهریار آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببین کان لکلک گویا برآمد بر سر منبر</p></div>
<div class="m2"><p>که ای یاران آن کاره صلا که وقت کار آمد</p></div></div>