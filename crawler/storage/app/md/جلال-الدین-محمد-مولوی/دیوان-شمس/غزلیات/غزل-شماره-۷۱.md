---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>اگر نه عشق شمس الدین بدی در روز و شب ما را</p></div>
<div class="m2"><p>فراغت‌ها کجا بودی ز دام و از سبب ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت شهوت برآوردی دمار از ما ز تاب خود</p></div>
<div class="m2"><p>اگر از تابش عشقش نبودی تاب و تب ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوازش‌های عشق او لطافت‌های مهر او</p></div>
<div class="m2"><p>رهانید و فراغت داد از رنج و نصب ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی این کیمیای حق که هست از مهر جان او</p></div>
<div class="m2"><p>که عین ذوق و راحت شد همه رنج و تعب ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنایت‌های ربانی ز بهر خدمت آن شه</p></div>
<div class="m2"><p>برویانید و هستی داد از عین ادب ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار حسن آن مهتر به ما بنمود ناگاهان</p></div>
<div class="m2"><p>شقایق‌ها و ریحان‌ها و گل‌های عجب ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهی دولت زهی رفعت زهی بخت و زهی اختر</p></div>
<div class="m2"><p>که مطلوب همه جان‌ها کند از جان طلب ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گزید او لب گه مستی که رو پیدا مکن مستی</p></div>
<div class="m2"><p>چو جام جان لبالب شد از آن می‌های لب ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجب بختی که رو بنمود ناگاهان هزاران شکر</p></div>
<div class="m2"><p>ز معشوق لطیف اوصاف خوب بوالعجب ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن مجلس که گردان کرد از لطف او صراحی‌ها</p></div>
<div class="m2"><p>گران قدر و سبک دل شد دل و جان از طرب ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سوی خطه تبریز چه چشمه آب حیوانست</p></div>
<div class="m2"><p>کشاند دل بدان جانب به عشق چون کنب ما را</p></div></div>