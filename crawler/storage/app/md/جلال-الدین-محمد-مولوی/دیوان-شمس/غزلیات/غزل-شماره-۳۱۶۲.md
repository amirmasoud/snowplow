---
title: >-
    غزل شمارهٔ ۳۱۶۲
---
# غزل شمارهٔ ۳۱۶۲

<div class="b" id="bn1"><div class="m1"><p>ای که مستک شدی و می‌گویی</p></div>
<div class="m2"><p>تو غریبی و یا از این کویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست و بی‌خویش می‌روی چپ و راست</p></div>
<div class="m2"><p>بی چپ و راست را همی‌جویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی چپست و نه راست در جانست</p></div>
<div class="m2"><p>آن که جان خسته از پی اویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آن شکر روی اگر بگردانی</p></div>
<div class="m2"><p>اگر نباتی بدانک بدخویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور تو دیوی و رو بدو آری</p></div>
<div class="m2"><p>الله الله چه خوب مه رویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم از جا رود چو گویم او</p></div>
<div class="m2"><p>می‌برد جان و دل زهی اویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هین ز خوهای او یکی بشنو</p></div>
<div class="m2"><p>گاه شیری کند گه آهویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ره او نماند پای مرا</p></div>
<div class="m2"><p>زانوم را نماند زانویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز به چوگان او مغلطان سر</p></div>
<div class="m2"><p>گر به میدان او یکی گویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هین خمش کن در این حدیث بازمپیچ</p></div>
<div class="m2"><p>آسمان وار اگر یکی تویی</p></div></div>