---
title: >-
    غزل شمارهٔ ۱۹۶۹
---
# غزل شمارهٔ ۱۹۶۹

<div class="b" id="bn1"><div class="m1"><p>از بدی‌ها آن چه گویم هست قصدم خویشتن</p></div>
<div class="m2"><p>زانک زهری من ندیدم در جهان چون خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر اشارت با کسی دیدی ندارم قصد او</p></div>
<div class="m2"><p>نی به حق ذوالجلال و ذوالکمال و ذوالمنن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ز خود فارغ نیایم با دگر کس چون رسم</p></div>
<div class="m2"><p>ور بگویم فارغم از خود بود سودا و ظن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بگفتم نکته‌ای هستش بسی تأویل‌ها</p></div>
<div class="m2"><p>گر غرض نقصان کس دارم نه مردم من نه زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو دارم التماسی ای حریف رازدار</p></div>
<div class="m2"><p>حسن ظنی در هوی و مهر من با خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمن جانم منم افغان من هم از خود است</p></div>
<div class="m2"><p>کز خودی خود من بخواهم همچو هیزم سوختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونک یاری را هزاران بار با نام و نشان</p></div>
<div class="m2"><p>مدح‌های بی‌نفاقش کرده باشم در علن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فخر کرده من بر او صد بار پیدا و نهان</p></div>
<div class="m2"><p>بوده ما را از عزیزی با دو دیده مقترن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر یکی عیبی بگویم قصد من عیب من است</p></div>
<div class="m2"><p>زانک ماهم را بپوشد ابر من اندر بدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو بدان یک وصف کردم کز ملامت مر ورا</p></div>
<div class="m2"><p>بهر حق دوستی حملش مکن بر مکر و فن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من خودی خویش را گویم که در پنداشتی</p></div>
<div class="m2"><p>رو اگر نور خدایی نیست شو شو ممتحن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خود من گر همه سر خدایی محو شو</p></div>
<div class="m2"><p>کان همه خود دیده‌ای پس دیده خودبین بکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون خداوند شمس دین را می ستایم تو بدان</p></div>
<div class="m2"><p>کاین همه اوصاف خوبی را ستودم در قرن</p></div></div>