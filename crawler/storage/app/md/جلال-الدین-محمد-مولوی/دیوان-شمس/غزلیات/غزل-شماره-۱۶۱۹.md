---
title: >-
    غزل شمارهٔ ۱۶۱۹
---
# غزل شمارهٔ ۱۶۱۹

<div class="b" id="bn1"><div class="m1"><p>تو گواه باش خواجه که ز توبه توبه کردم</p></div>
<div class="m2"><p>بشکست جام توبه چو شراب عشق خوردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جمال بی‌نظیرت به شراب شیرگیرت</p></div>
<div class="m2"><p>که به گرد عهد و توبه نروم دگر نگردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به لب شکرفشانت به ضمیر غیب دانت</p></div>
<div class="m2"><p>که نه سخره جهانم نه زبون سرخ و زردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رخ چو آفتابت به حلاوت خطابت</p></div>
<div class="m2"><p>که هزارساله ره من ز ورای گرم و سردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هوای همچو رخشت به لوای روح بخشت</p></div>
<div class="m2"><p>که به جز تو کس نداند که کیم چگونه مردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سعادت صباحت به قیامت صبوحت</p></div>
<div class="m2"><p>که سجل آسمان را به فر تو درنوردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هله ای شه مخلد تو بگو به ساقی خود</p></div>
<div class="m2"><p>چو کسی ترش درآید دهدش ز درد در دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هله تا دوی نباشد کهن و نوی نباشد</p></div>
<div class="m2"><p>که در این مقام عشرت من از آن جمع فردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدهش از آن رحیقی که شود خوشی عشیقی</p></div>
<div class="m2"><p>که ز مستی و خرابی برهد ز عکس و طردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه در او حسد بماند نه غم جسد بماند</p></div>
<div class="m2"><p>خوش و پاک بازآید به سوی بساط نردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به صفا مثال زهره به رضا به سان مهره</p></div>
<div class="m2"><p>نه نصیبه جو نه بهره که ببردم و نبردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپریده از زمانه ز هوای دام و دانه</p></div>
<div class="m2"><p>که در این قمارخانه چو گواه بی‌نبردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس از این خموش باشم همه گوش و هوش باشم</p></div>
<div class="m2"><p>که نه بلبلم نه طوطی همه قند و شاخ وردم</p></div></div>