---
title: >-
    غزل شمارهٔ ۲۲۷۵
---
# غزل شمارهٔ ۲۲۷۵

<div class="b" id="bn1"><div class="m1"><p>امروز مستان را نگر در مست ما آویخته</p></div>
<div class="m2"><p>افکنده عقل و عافیت و اندر بلا آویخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که ای مستان جان می‌خورده از دستان جان</p></div>
<div class="m2"><p>ای صد هزاران جان و دل اندر شما آویخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتند شکر الله را کو جلوه کرد این ماه را</p></div>
<div class="m2"><p>افتاده بودیم از بقا در قعر لا آویخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگریختیم از جور او یک مدتی وز دور او</p></div>
<div class="m2"><p>چون دشمنان بودیم ما اندر جفا آویخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام وفا برداشته کار و دکان بگذاشته</p></div>
<div class="m2"><p>و افسردگان بی‌مزه در کارها آویخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنشسته عقل سرمه کش با هر کی با چشمی است خوش</p></div>
<div class="m2"><p>بنشسته زاغ دیده کش بر هر کجا آویخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین خنب‌های تلخ و خوش گر چاشنی داری بچش</p></div>
<div class="m2"><p>ترک هوا خوشتر بود یا در هوا آویخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمری دل من در غمش آواره شد می‌جستمش</p></div>
<div class="m2"><p>دیدم دل بیچاره را خوش در خدا آویخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر دار دنیا ای فتی گر ایمنی برخیز تا</p></div>
<div class="m2"><p>بنمایم آزادانت را و هم تو را آویخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دار ملک جاودان بین کشتگان زنده جان</p></div>
<div class="m2"><p>مانند منصور جوان در ارتضا آویخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشقا تویی سلطان من از بهر من داری بزن</p></div>
<div class="m2"><p>روشن ندارد خانه را قندیل ناآویخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من خاک پای آن کسم کو دست در مردان زند</p></div>
<div class="m2"><p>جانم غلام آن مسی در کیمیا آویخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برجه طرب را ساز کن عیش و سماع آغاز کن</p></div>
<div class="m2"><p>خوش نیست آن دف سرنگون نی بی‌نوا آویخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دف دل گشاید بسته را نی جان فزاید خسته را</p></div>
<div class="m2"><p>این دلگشا چون بسته شد و آن جان فزا آویخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امروز دستی برگشا ایثار کن جان در سخا</p></div>
<div class="m2"><p>با کفر حاتم رست چون بد در سخا آویخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست آن سخا چون دام نان اما صفا چون دام جان</p></div>
<div class="m2"><p>کو در سخا آویخته کو در صفا آویخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باشد سخی چون خایفی در غار ایثاری شده</p></div>
<div class="m2"><p>صوفی چو بوبکری بود در مصطفی آویخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این دل دهد در دلبری جان هم سپارد بر سری</p></div>
<div class="m2"><p>و آن صرفه جو چون مشتری اندر بها آویخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن چون نهنگ آیان شده دریا در او حیران شده</p></div>
<div class="m2"><p>وین بحری نوآشنا در آشنا آویخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گویی که این کار و کیا یا صدق باشد یا ریا</p></div>
<div class="m2"><p>آن جا که عشاقند و ما صدق و ریا آویخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شب گشت ای شاه جهان چشم و چراغ شب روان</p></div>
<div class="m2"><p>ای پیش روی چون مهت ماه سما آویخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من شادمان چون ماه نو تو جان فزا چون جاه نو</p></div>
<div class="m2"><p>وی در غم تو ماه نو چون من دوتا آویخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کوه است جان در معرفت تن برگ کاهی در صفت</p></div>
<div class="m2"><p>بر برگ کی دیده است کس یک کوه را آویخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از ره روان گردی روان صحبت ببر از دیگران</p></div>
<div class="m2"><p>ور نی بمانی مبتلا در مبتلا آویخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان عزیزان گشته خون تا عاقبت چون است چون</p></div>
<div class="m2"><p>از بدگمانی سرنگون در انتها آویخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون دید جان پاکشان آن تخم کاول کاشت جان</p></div>
<div class="m2"><p>واگشت فکر از انتها در ابتدا آویخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اصل ندا از دل بود در کوه تن افتد صدا</p></div>
<div class="m2"><p>خاموش رو در اصل کن ای در صدا آویخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت زبان کبر آورد کبرت نیازت را خورد</p></div>
<div class="m2"><p>شو تو ز کبر خود جدا در کبریا آویخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای شمس تبریزی برآ از سوی شرق کبریا</p></div>
<div class="m2"><p>جان‌ها ز تو چون ذره‌ها اندر ضیا آویخته</p></div></div>