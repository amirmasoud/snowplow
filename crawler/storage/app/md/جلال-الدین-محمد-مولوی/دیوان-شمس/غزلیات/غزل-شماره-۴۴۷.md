---
title: >-
    غزل شمارهٔ ۴۴۷
---
# غزل شمارهٔ ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>ای گل تو را اگر چه که رخسار نازکست</p></div>
<div class="m2"><p>رخ بر رخش مدار که آن یار نازکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل مدار نیز که رخ بر رخش نهی</p></div>
<div class="m2"><p>کو سر دل بداند و دلدار نازکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آرزو ز حد شد دزدیده سجده کن</p></div>
<div class="m2"><p>بسیار هم مکوش که بسیار نازکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بیخودی ز خویش همه وقت وقت تو است</p></div>
<div class="m2"><p>گر نی به وقت آی که اسرار نازکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را ز غم بروب که خانه خیال او است</p></div>
<div class="m2"><p>زیرا خیال آن بت عیار نازک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی بتافت سایه گل بر خیال دوست</p></div>
<div class="m2"><p>بر دوست کار کرد که این کار نازکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر خیال مفخر تبریز شمس دین</p></div>
<div class="m2"><p>منگر تو خوار کان شه خون خوار نازکست</p></div></div>