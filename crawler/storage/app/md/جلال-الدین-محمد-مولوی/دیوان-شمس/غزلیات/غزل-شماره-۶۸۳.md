---
title: >-
    غزل شمارهٔ ۶۸۳
---
# غزل شمارهٔ ۶۸۳

<div class="b" id="bn1"><div class="m1"><p>ز خاک من اگر گندم برآید</p></div>
<div class="m2"><p>از آن گر نان پزی مستی فزاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمیر و نانبا دیوانه گردد</p></div>
<div class="m2"><p>تنورش بیت مستانه سراید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بر گور من آیی زیارت</p></div>
<div class="m2"><p>تو را خرپشته‌ام رقصان نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میا بی‌دف به گور من برادر</p></div>
<div class="m2"><p>که در بزم خدا غمگین نشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنخ بربسته و در گور خفته</p></div>
<div class="m2"><p>دهان افیون و نقل یار خاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدری زان کفن بر سینه بندی</p></div>
<div class="m2"><p>خراباتی ز جانت درگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هر سو بانگ جنگ و چنگ مستان</p></div>
<div class="m2"><p>ز هر کاری به لابد کار زاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا حق از می عشق آفریده‌ست</p></div>
<div class="m2"><p>همان عشقم اگر مرگم بساید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم مستی و اصل من می عشق</p></div>
<div class="m2"><p>بگو از می به جز مستی چه آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به برج روح شمس الدین تبریز</p></div>
<div class="m2"><p>بپرد روح من یک دم نپاید</p></div></div>