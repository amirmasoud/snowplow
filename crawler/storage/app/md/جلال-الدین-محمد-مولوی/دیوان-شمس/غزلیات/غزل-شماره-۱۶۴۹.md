---
title: >-
    غزل شمارهٔ ۱۶۴۹
---
# غزل شمارهٔ ۱۶۴۹

<div class="b" id="bn1"><div class="m1"><p>وقت آن شد که به زنجیر تو دیوانه شویم</p></div>
<div class="m2"><p>بند را برگسلیم از همه بیگانه شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان سپاریم دگر ننگ چنین جان نکشیم</p></div>
<div class="m2"><p>خانه سوزیم و چو آتش سوی میخانه شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نجوشیم از این خنب جهان برناییم</p></div>
<div class="m2"><p>کی حریف لب آن ساغر و پیمانه شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن راست تو از مردم دیوانه شنو</p></div>
<div class="m2"><p>تا نمیریم مپندار که مردانه شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سر زلف سعادت که شکن در شکن است</p></div>
<div class="m2"><p>واجب آید که نگونتر ز سر شانه شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بال و پر باز گشاییم به بستان چو درخت</p></div>
<div class="m2"><p>گر در این راه فنا ریخته چون دانه شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه سنگیم پی مهر تو چون موم شویم</p></div>
<div class="m2"><p>گر چه شمعیم پی نور تو پروانه شویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه شاهیم برای تو چو رخ راست رویم</p></div>
<div class="m2"><p>تا بر این نطع ز فرزین تو فرزانه شویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در رخ آینه عشق ز خود دم نزنیم</p></div>
<div class="m2"><p>محرم گنج تو گردیم چو پروانه شویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما چو افسانه دل بی‌سر و بی‌پایانیم</p></div>
<div class="m2"><p>تا مقیم دل عشاق چو افسانه شویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر مریدی کند او ما به مرادی برسیم</p></div>
<div class="m2"><p>ور کلیدی کند او ما همه دندانه شویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مصطفی در دل ما گر ره و مسند نکند</p></div>
<div class="m2"><p>شاید ار ناله کنیم استن حنانه شویم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نی خمش کن که خموشانه بباید دادن</p></div>
<div class="m2"><p>پاسبان را چو به شب ما سوی کاشانه شویم</p></div></div>