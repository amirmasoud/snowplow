---
title: >-
    غزل شمارهٔ ۱۷۳۲
---
# غزل شمارهٔ ۱۷۳۲

<div class="b" id="bn1"><div class="m1"><p>به حق آنک بخواندی مرا ز گوشه بام</p></div>
<div class="m2"><p>اشارتی که بکردی به سر به جای سلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حق آنک گشادی کمر که می نروم</p></div>
<div class="m2"><p>که شد قمر کمرت را چو من کمینه غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حق آنک نداند دل خیال اندیش</p></div>
<div class="m2"><p>مثال‌های خیال مرا به وقت پیام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حق آنک به فراش گفته‌ای که بروب</p></div>
<div class="m2"><p>ز چند گنده بغل خانه را برای کرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حق آنک گزیدی دو لب که جام بگیر</p></div>
<div class="m2"><p>بنوش جام رها کن حدیث پخته و خام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به حق آنک تو را دیدم و قلم افتاد</p></div>
<div class="m2"><p>ز دست عشق نویسم به پیش تو ناکام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حق آنک گمان‌های بد فرستی تو</p></div>
<div class="m2"><p>به هدهدی که بخواهی که جان ببر زین دام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حق حلقه رندان که باده می نوشند</p></div>
<div class="m2"><p>به پیش خلق هویدا میان روز صیام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار شیشه شکستند و روزه شان نشکست</p></div>
<div class="m2"><p>از آنک شیشه گر عشق ساخته‌ست آن جام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ماه روزه جهودانه می مخور تو به شب</p></div>
<div class="m2"><p>بیا به بزم محمد مدام نوش مدام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میان گفت بدم من که سست خندیدی</p></div>
<div class="m2"><p>که ای سلیم دل آخر کشیده دار لگام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتمش چو دهان مرا نمی‌دوزی</p></div>
<div class="m2"><p>بدوز گوش کسی را که نیست یار تمام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به حق آنک حلال است خون من بر تو</p></div>
<div class="m2"><p>که بر عدو سخنم را حرام دار حرام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیال من ز ملاقات شمس تبریزی</p></div>
<div class="m2"><p>هزار صورت بیند عجب پی اعلام</p></div></div>