---
title: >-
    غزل شمارهٔ ۲۵۵۱
---
# غزل شمارهٔ ۲۵۵۱

<div class="b" id="bn1"><div class="m1"><p>دلی یا دیده عقلی تو یا نور خدابینی</p></div>
<div class="m2"><p>چراغ افروز عشاقی تو یا خورشیدآیینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نامت بشنود دل‌ها نگنجد در منازل‌ها</p></div>
<div class="m2"><p>شود حل جمله مشکل‌ها به نور لم یزل بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتم آفتابا تو مرا همراه کن با تو</p></div>
<div class="m2"><p>که جمله دردها را تو شفا گشتی و تسکینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفتا جان ربایم من قدم بر عرش سایم من</p></div>
<div class="m2"><p>به آب و گل کم آیم من مگر در وقت و هر حینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تو از خویش آگاهی ندانی کرد همراهی</p></div>
<div class="m2"><p>که آن معراج اللهی نیابد جز که مسکینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مسکینی در این ظاهر درونت نفس بس قاهر</p></div>
<div class="m2"><p>یکی سالوسک کافر که رهزن گشت و ره شینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن پوشیده از پیری چنین مو در چنین شیری</p></div>
<div class="m2"><p>یکی پیری که علم غیب زیر او است بالینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبیب عاشقان است او جهان را همچو جان است او</p></div>
<div class="m2"><p>گداز آهنان است او به آهن داده تلبینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کند در حال گل را زر دهد در حال تن را سر</p></div>
<div class="m2"><p>از او انوار دین یابد روان و جان بی‌دینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن دهلیز و ایوانش بیا بنگر تو برهانش</p></div>
<div class="m2"><p>شده هر مرده از جانش یکی ویسی و رامینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شمس الدین تبریزی دلا این حرف می‌بیزی</p></div>
<div class="m2"><p>به امیدی که بازآید از آن خوش شاه شاهینی</p></div></div>