---
title: >-
    غزل شمارهٔ ۱۲۵۷
---
# غزل شمارهٔ ۱۲۵۷

<div class="b" id="bn1"><div class="m1"><p>چون تو شادی بنده گو غمخوار باش</p></div>
<div class="m2"><p>تو عزیزی صد چو ما گو خوار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار تو باید که باشد بر مراد</p></div>
<div class="m2"><p>کارهای عاشقان گو زار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه منصوری و ملکت آن توست</p></div>
<div class="m2"><p>بنده چون منصور گو بر دار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشتر مستم نجویم نسترن</p></div>
<div class="m2"><p>نوشخوارم در رهت گو خار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشنوم من هیچ جز پیغام او</p></div>
<div class="m2"><p>هر چه خواهی گفت گو اسرار باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل آن جایی تو باری که ویست</p></div>
<div class="m2"><p>از جمال یار برخوردار باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او طبیبست و به بیماران رود</p></div>
<div class="m2"><p>ای تن وامانده تو بیمار باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر امید یار غار خلوتی</p></div>
<div class="m2"><p>ثانی اثنین برو در غار باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر امید داد و ایثار بهار</p></div>
<div class="m2"><p>مهرها می‌کار و در ایثار باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرمنا بر طمع ماه بانمک</p></div>
<div class="m2"><p>گم شو از دزد و در آن انبار باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر نطق یار خوش گفتار خویش</p></div>
<div class="m2"><p>لب ببند از گفت و کم گفتار باش</p></div></div>