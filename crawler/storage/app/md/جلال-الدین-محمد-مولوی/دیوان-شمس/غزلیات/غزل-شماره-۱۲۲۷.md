---
title: >-
    غزل شمارهٔ ۱۲۲۷
---
# غزل شمارهٔ ۱۲۲۷

<div class="b" id="bn1"><div class="m1"><p>رویش خوش و مویش خوش وان طره جعدینش</p></div>
<div class="m2"><p>صد رحمت هر ساعت بر جانش و بر دینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه و هر ساعت یک شیوه نو آرد</p></div>
<div class="m2"><p>شیرینتر و نادرتر زان شیوه پیشینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن طره پرچین را چون باد بشوراند</p></div>
<div class="m2"><p>صد چین و دو صد ماچین گم گردد در چینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر روی و قفای مه سیلی زده حسن او</p></div>
<div class="m2"><p>بر دبدبه قارون تسخر زده مسکینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن ماه که می‌خندد در شرح نمی‌گنجد</p></div>
<div class="m2"><p>ای چشم و چراغ من دم درکش و می‌بینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد چرخ همی‌گردد بر آب حیات او</p></div>
<div class="m2"><p>صد کوه کمر بندد در خدمت تمکینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گولی مگر ای لولی این جا به چه می‌لولی</p></div>
<div class="m2"><p>رو صید و تماشا کن در شاهی شاهینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر اسب ندارد جان پیشش برود لنگان</p></div>
<div class="m2"><p>بنشاند آن فارس جان را سپس زینش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور پای ندارد هم سر بندد و سر بنهد</p></div>
<div class="m2"><p>مانند طبیب آید آن شاه به بالینش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشقست یکی جانی دررفته به صد صورت</p></div>
<div class="m2"><p>دیوانه شدم باری من در فن و آیینش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن و نمک نادر در صورت عشق آمد</p></div>
<div class="m2"><p>تا حسن و سکون یابد جان از پی تسکینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر طالع ماه خود تقویم عجب بست او</p></div>
<div class="m2"><p>تقویم طلب می‌کن در سوره والتینش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خورشید به تیغ خود آن را که کشد ای جان</p></div>
<div class="m2"><p>از تابش خود سازد تجهیزش و تکفینش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرهاد هوای او رفتست به که کندن</p></div>
<div class="m2"><p>تا لعل شود مرمر از ضربت میتینش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من بس کنم ای مطرب بر پرده بگو این را</p></div>
<div class="m2"><p>بشنو ز پس پرده کر و فر تحسینش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خامش که به پیش آمد جوزینه و لوزینه</p></div>
<div class="m2"><p>لوزینه دعا گوید حلوا کند آمینش</p></div></div>