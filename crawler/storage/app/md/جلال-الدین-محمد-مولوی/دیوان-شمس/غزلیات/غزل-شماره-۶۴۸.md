---
title: >-
    غزل شمارهٔ ۶۴۸
---
# غزل شمارهٔ ۶۴۸

<div class="b" id="bn1"><div class="m1"><p>ای قوم به حج رفته کجایید کجایید</p></div>
<div class="m2"><p>معشوق همین جاست بیایید بیایید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوق تو همسایه و دیوار به دیوار</p></div>
<div class="m2"><p>در بادیه سرگشته شما در چه هوایید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر صورت بی‌صورت معشوق ببینید</p></div>
<div class="m2"><p>هم خواجه و هم خانه و هم کعبه شمایید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ده بار از آن راه بدان خانه برفتید</p></div>
<div class="m2"><p>یک بار از این خانه بر این بام برآیید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن خانه لطیفست نشان‌هاش بگفتید</p></div>
<div class="m2"><p>از خواجه آن خانه نشانی بنمایید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دسته گل کو اگر آن باغ بدیدید</p></div>
<div class="m2"><p>یک گوهر جان کو اگر از بحر خدایید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با این همه آن رنج شما گنج شما باد</p></div>
<div class="m2"><p>افسوس که بر گنج شما پرده شمایید</p></div></div>