---
title: >-
    غزل شمارهٔ ۱۵۳
---
# غزل شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>شمع دیدم گرد او پروانه‌ها چون جمع‌ها</p></div>
<div class="m2"><p>شمع کی دیدم که گردد گرد نورش شمع‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع را چون برفروزی اشک ریزد بر رخان</p></div>
<div class="m2"><p>او چو بفروزد رخ عاشق بریزد دمع‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شکر گفتار آغازد ببینی ذره‌ها</p></div>
<div class="m2"><p>از برای استماعش واگشاده سمع‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناامیدانی که از ایام‌ها بفسرده‌اند</p></div>
<div class="m2"><p>گرمی جانش برانگیزد ز جانشان طمع‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نه لطف او بدی بودی ز جان‌های غیور</p></div>
<div class="m2"><p>مر مرا از ذکر نام شکرینش منع‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمس دین صدر خداوند خداوندان به حق</p></div>
<div class="m2"><p>کز جمال جان او بازیب و فر شد صنع‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بر آن آمد که مر جسمانیان را رو دهد</p></div>
<div class="m2"><p>جان صدیقان گریبان را درید از شنع‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تخم امیدی که کشتم از پی آن آفتاب</p></div>
<div class="m2"><p>یک نظر بادا از او بر ما برای ینع‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه جسم لطیفش جان ما را جان‌هاست</p></div>
<div class="m2"><p>یا رب آن سایه به ما واده برای طبع‌ها</p></div></div>