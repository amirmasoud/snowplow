---
title: >-
    غزل شمارهٔ ۲۲۶۱
---
# غزل شمارهٔ ۲۲۶۱

<div class="b" id="bn1"><div class="m1"><p>بوقلمون چند از انکار تو</p></div>
<div class="m2"><p>در کف ما چند خلد خار تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار تو از سر فلک واقف است</p></div>
<div class="m2"><p>پس چه بود پیش وی اسرار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند بگویی که همین بار و بس</p></div>
<div class="m2"><p>چند از این چند از این بار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ز تو بیمار حبیب و طبیب</p></div>
<div class="m2"><p>بسته ز ناسور تو تیمار تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورده می غفلت و منکر شده</p></div>
<div class="m2"><p>بوی دهانت شده اقرار تو</p></div></div>