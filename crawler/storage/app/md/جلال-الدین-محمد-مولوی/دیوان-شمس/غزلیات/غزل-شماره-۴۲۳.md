---
title: >-
    غزل شمارهٔ ۴۲۳
---
# غزل شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>مگر این دم سر آن زلف پریشان شده است</p></div>
<div class="m2"><p>که چنین مشک تتاری عبرافشان شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر از چهره او باد صبا پرده ربود</p></div>
<div class="m2"><p>که هزاران قمر غیب درخشان شده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست جانی که ز بوی خوش او شادان نیست</p></div>
<div class="m2"><p>گر چه جان بو نبرد کو ز چه شادان شده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا شاد گلی کز دم حق خندان است</p></div>
<div class="m2"><p>لیک هر جان بنداند ز چه خندان شده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب رخش امروز زهی خوش که بتافت</p></div>
<div class="m2"><p>که هزاران دل از او لعل بدخشان شده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق آخر ز چه رو تا به ابد دل ننهد</p></div>
<div class="m2"><p>بر کسی کز لطفش تن همگی جان شده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگرش دل سحری دید بدان سان که وی است</p></div>
<div class="m2"><p>که از آن دیدنش امروز بدین سان شده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بدیده است دل آن حسن پری زاد مرا</p></div>
<div class="m2"><p>شیشه بر دست گرفته است و پری خوان شده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر درخت تن اگر باد خوشش می‌نوزد</p></div>
<div class="m2"><p>پس دو صد برگ دو صد شاخ چه لرزان شده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهر هر کشته او جان ابد گر نبود</p></div>
<div class="m2"><p>جان سپردن بر عاشق ز چه آسان شده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حیات و خبرش باخبران بی‌خبرند</p></div>
<div class="m2"><p>که حیات و خبرش پرده ایشان شده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نه در نای دلی مطرب عشقش بدمید</p></div>
<div class="m2"><p>هر سر موی چو سرنای چه نالان شده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس تبریز ز بام ار نه کلوخ اندازد</p></div>
<div class="m2"><p>سوی دل پس ز چه جان‌هاش چو دربان شده است</p></div></div>