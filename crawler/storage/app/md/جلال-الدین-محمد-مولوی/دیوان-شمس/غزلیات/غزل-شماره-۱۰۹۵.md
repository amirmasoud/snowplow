---
title: >-
    غزل شمارهٔ ۱۰۹۵
---
# غزل شمارهٔ ۱۰۹۵

<div class="b" id="bn1"><div class="m1"><p>داد جاروبی به دستم آن نگار</p></div>
<div class="m2"><p>گفت کز دریا برانگیزان غبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز آن جاروب را ز آتش بسوخت</p></div>
<div class="m2"><p>گفت کز آتش تو جاروبی برآر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردم از حیرت سجودی پیش او</p></div>
<div class="m2"><p>گفت بی‌ساجد سجودی خوش بیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه بی‌ساجد سجودی چون بود</p></div>
<div class="m2"><p>گفت بی‌چون باشد و بی‌خارخار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردنک را پیش کردم گفتمش</p></div>
<div class="m2"><p>ساجدی را سر ببر از ذوالفقار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ تا او بیش زد سر بیش شد</p></div>
<div class="m2"><p>تا برست از گردنم سر صد هزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من چراغ و هر سرم همچون فتیل</p></div>
<div class="m2"><p>هر طرف اندر گرفته از شرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع‌ها می‌ورشد از سرهای من</p></div>
<div class="m2"><p>شرق تا مغرب گرفته از قطار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرق و مغرب چیست اندر لامکان</p></div>
<div class="m2"><p>گلخنی تاریک و حمامی به کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای مزاجت سرد کو تاسه دلت</p></div>
<div class="m2"><p>اندر این گرمابه تا کی این قرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برشو از گرمابه و گلخن مرو</p></div>
<div class="m2"><p>جامه کن دربنگر آن نقش و نگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ببینی نقش‌های دلربا</p></div>
<div class="m2"><p>تا ببینی رنگ‌های لاله زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون بدیدی سوی روزن درنگر</p></div>
<div class="m2"><p>کان نگار از عکس روزن شد نگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شش جهت حمام و روزن لامکان</p></div>
<div class="m2"><p>بر سر روزن جمال شهریار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک و آب از عکس او رنگین شده</p></div>
<div class="m2"><p>جان بباریده به ترک و زنگبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز رفت و قصه‌ام کوته نشد</p></div>
<div class="m2"><p>ای شب و روز از حدیثش شرمسار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاه شمس الدین تبریزی مرا</p></div>
<div class="m2"><p>مست می‌دارد خمار اندر خمار</p></div></div>