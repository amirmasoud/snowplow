---
title: >-
    غزل شمارهٔ ۵۸۴
---
# غزل شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>یکی گولی همی‌خواهم که در دلبر نظر دارد</p></div>
<div class="m2"><p>نمی‌خواهم هنرمندی که دیده در هنر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی همچون صدف خواهم که در جان گیرد آن گوهر</p></div>
<div class="m2"><p>دل سنگین نمی‌خواهم که پندار گهر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خودبینی جدا گشته پر از عشق خدا گشته</p></div>
<div class="m2"><p>ز مالش‌های غم غافل به مالنده عبر دارد</p></div></div>