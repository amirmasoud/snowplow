---
title: >-
    غزل شمارهٔ ۳۲۰۱
---
# غزل شمارهٔ ۳۲۰۱

<div class="b" id="bn1"><div class="m1"><p>یا ساقی الحی اسمع سؤالی</p></div>
<div class="m2"><p>انشد فادی، واخبر بحال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قالو تسلی، حاشا و کلا</p></div>
<div class="m2"><p>عشق تجلی من ذی‌الجلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>العشق فنی، والشوق دنی</p></div>
<div class="m2"><p>والخمر منی، والسکر حالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق وجیهی، بحر یلیه</p></div>
<div class="m2"><p>والحوت فیه روح‌الرجال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انتم شفایی، انتم دوایی</p></div>
<div class="m2"><p>انتم رجایی، انتم کمالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الفخ کامن، والعشق آمن</p></div>
<div class="m2"><p>والرب ضامن، کی لاتبالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق موبد، فتلی تعمد</p></div>
<div class="m2"><p>و انا معود، بأس‌النزال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که: « ما را هنگامه بنما »</p></div>
<div class="m2"><p>گفت: « اینک اما تو در جوالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدران جوال و سر را برون کن</p></div>
<div class="m2"><p>تا خود ببینی کندر وصالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندر ره جان پا را مرنجان</p></div>
<div class="m2"><p>زیرا همایی با پر و بالی »</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم که: « عاشق بیند مرافق »</p></div>
<div class="m2"><p>گفتا که: « لالا ان کان سالی »</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم که: « بکشی تو بی‌گنه را »</p></div>
<div class="m2"><p>گفتا: « کذا هوالوصل غالی »</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم « چه نوشم زان شهد؟ » گفتا</p></div>
<div class="m2"><p>« مومت نباشد هان، تا نمالی »</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>انعم صباحا، واطلب رباحا</p></div>
<div class="m2"><p>وابسط جناحا فالقصر عالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می‌نال چون نا، خوش همنشینا!</p></div>
<div class="m2"><p>حقست بینا، هر چون که نالی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>انا وجدنا درا، فقدنا</p></div>
<div class="m2"><p>لما ولجنا، موج‌اللیالی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می گرد شبها، گرد طلبها</p></div>
<div class="m2"><p>تا پیشت آید نیکو سگالی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می گرد شب در، مانند اختر</p></div>
<div class="m2"><p>ان‌اللیالی بحراللالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دارم رسولی، اما ملولی</p></div>
<div class="m2"><p>یارب خلص، عن ذی‌الملال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عندی شراب لوذقت منه</p></div>
<div class="m2"><p>بس شیرگیری، گرچه شغالی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درکش چو افیون، واره تو اکنون</p></div>
<div class="m2"><p>گه در جوابی، گه در سوالی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من سخت مستم، به خود خوشستم</p></div>
<div class="m2"><p>یا من تلمنی، لم تدر حالی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جانا فرود آ، از بام بالا</p></div>
<div class="m2"><p>وانعم بوصل، فالبیت خالی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتم که: « بشنو، رمزی ز بنده »</p></div>
<div class="m2"><p>گفتا که: « اسکت یا ذاالمقال »</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم: « خموشی صعبست » گفتا:</p></div>
<div class="m2"><p>یا ذاالمقال، صرذاالمعالی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کس نیست محرم، کوتاه کن دم</p></div>
<div class="m2"><p>والله اعلم، والله تالی</p></div></div>