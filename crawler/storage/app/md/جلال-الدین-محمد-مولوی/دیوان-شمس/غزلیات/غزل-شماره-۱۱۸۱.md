---
title: >-
    غزل شمارهٔ ۱۱۸۱
---
# غزل شمارهٔ ۱۱۸۱

<div class="b" id="bn1"><div class="m1"><p>به سوی ما نگر چشمی برانداز</p></div>
<div class="m2"><p>وگر فرصت بود بوسی درانداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو کردی نیت نیکو مگردان</p></div>
<div class="m2"><p>از آن گلشن گلی بر چاکر انداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر خواهی که روزافزون بود کار</p></div>
<div class="m2"><p>نظر بر کار ما افزونتر انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر تو فتنه انگیزی و خودکام</p></div>
<div class="m2"><p>رها کن داد و رسمی دیگر انداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگون کن سرو را همچون بنفشه</p></div>
<div class="m2"><p>گناه غنچه بر نیلوفر انداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز باد و بوی توست امروز در باغ</p></div>
<div class="m2"><p>درختان جمله رقاص و سرانداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شاخ لاغری افزون کند رقص</p></div>
<div class="m2"><p>تو میوه سوی شاخ لاغر انداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آمد خار گل را اسپری بخش</p></div>
<div class="m2"><p>چو خصم آمد به سوسن خنجر انداز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر عاشق بری چون سیم بگشا</p></div>
<div class="m2"><p>سوی مفلس یکی مشتی زر انداز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برآ ای شاه شمس الدین تبریز</p></div>
<div class="m2"><p>یکی نوری عجب بر اختر انداز</p></div></div>