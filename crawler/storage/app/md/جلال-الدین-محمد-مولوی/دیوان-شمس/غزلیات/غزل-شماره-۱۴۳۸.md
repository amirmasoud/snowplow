---
title: >-
    غزل شمارهٔ ۱۴۳۸
---
# غزل شمارهٔ ۱۴۳۸

<div class="b" id="bn1"><div class="m1"><p>ندارد پای عشق او دل بی‌دست و بی‌پایم</p></div>
<div class="m2"><p>که روز و شب چو مجنونم سر زنجیر می خایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان خونم و ترسم که گر آید خیال او</p></div>
<div class="m2"><p>به خون دل خیالش را ز بی‌خویشی بیالایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیالات همه عالم اگر چه آشنا داند</p></div>
<div class="m2"><p>به خون غرقه شود والله اگر این راه بگشایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم افتاده در سیلی اگر مجنون آن لیلی</p></div>
<div class="m2"><p>ز من گر یک نشان خواهد نشانی‌هاش بنمایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه گردد دل پاره همه شب همچو استاره</p></div>
<div class="m2"><p>شده خواب من آواره ز سحر یار خودرایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شب‌های من گریان بپرس از لشکر پریان</p></div>
<div class="m2"><p>که در ظلمت ز آمدشد پری را پای می سایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر یک دم بیاسایم روان من نیاساید</p></div>
<div class="m2"><p>من آن لحظه بیاسایم که یک لحظه نیاسایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رها کن تا چو خورشیدی قبایی پوشم از آتش</p></div>
<div class="m2"><p>در آن آتش چو خورشیدی جهانی را بیارایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که آن خورشید بر گردون ز عشق او همی‌سوزد</p></div>
<div class="m2"><p>و هر دم شکر می گوید که سوزش را همی‌شایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رها کن تا که چون ماهی گدازان غمش باشم</p></div>
<div class="m2"><p>که تا چون مه نکاهم من چو مه زان پس نیفزایم</p></div></div>