---
title: >-
    غزل شمارهٔ ۱۸۰۸
---
# غزل شمارهٔ ۱۸۰۸

<div class="b" id="bn1"><div class="m1"><p>با آنک از پیوستگی من عشق گشتم عشق من</p></div>
<div class="m2"><p>بیگانه می باشم چنین با عشق از دست فتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غایت پیوستگی بیگانه باشد کس بلی</p></div>
<div class="m2"><p>این مشکلات ار حل شود دشمن نماند در زمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحری است از ما دور نی ظاهر نه و مستور نی</p></div>
<div class="m2"><p>هم دم زدن دستور نی هم کفر از او خامش شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتن از او تشبیه شد خاموشیت تعطیل شد</p></div>
<div class="m2"><p>این درد بی‌درمان بود فرج لنا یا ذا المنن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش جهان رنگ و بو هر دم مدد خواهد از او</p></div>
<div class="m2"><p>هم بی‌خبر هم لقمه جو چون طفل بگشاده دهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خفته‌ست و برجسته‌ست دل در جوش پیوسته‌ست دل</p></div>
<div class="m2"><p>چون دیگ سربسته‌ست دل در آتشش کرده وطن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای داده خاموشانه‌ای ما را تو از پیمانه‌ای</p></div>
<div class="m2"><p>هر لحظه نوافسانه‌ای در خامشی شد نعره زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قهر او صد مرحمت در بخل او صد مکرمت</p></div>
<div class="m2"><p>در جهل او صد معرفت در خامشی گویا چو ظن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الفاظ خاموشان تو بشنوده بی‌هوشان تو</p></div>
<div class="m2"><p>خاموشم و جوشان تو مانند دریای عدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لطفت خدایی می کند حاجت روایی می کند</p></div>
<div class="m2"><p>وان کو جدایی می کند یا رب تو از بیخش بکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای خوشدلی و ناز ما ای اصل و ای آغاز ما</p></div>
<div class="m2"><p>آخر چه داند راز ما عقل حسن یا بوالحسن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای عشق تو بخریده ما وز غیر تو ببریده ما</p></div>
<div class="m2"><p>ای جامه‌ها بدریده ما بر چاک ما بخیه مزن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای خون عقلم ریخته صبر از دلم بگریخته</p></div>
<div class="m2"><p>ای جان من آمیخته با جان هر صورت شکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن جا که شد عاشق تلف مرغی نپرد آن طرف</p></div>
<div class="m2"><p>ور مرده یابد زان علف بیخود بدراند کفن</p></div></div>