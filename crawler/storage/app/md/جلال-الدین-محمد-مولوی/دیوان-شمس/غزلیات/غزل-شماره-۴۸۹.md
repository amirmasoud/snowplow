---
title: >-
    غزل شمارهٔ ۴۸۹
---
# غزل شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>اگر تو مست وصالی رخ تو ترش چراست</p></div>
<div class="m2"><p>برون شیشه ز حال درون شیشه گواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدید باشد مستی میان صد هشیار</p></div>
<div class="m2"><p>ز بوی رنگ و ز چشم و فتادن از چپ و راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علی الخصوص شرابی که اولیا نوشند</p></div>
<div class="m2"><p>که جوش و نوش و قوامش ز خم لطف خداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خم شراب میان هزار خم دگر</p></div>
<div class="m2"><p>به کف و تف و به جوش و به غلغله پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جوش دیدی می‌دان که آتش‌ست ز جان</p></div>
<div class="m2"><p>خروش دیدی می‌دانک شعله سوداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدانک سرکه فروشی شراب کی دهدت</p></div>
<div class="m2"><p>که جرعه‌اش را صد من شکر به نقد بهاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهای باده من المؤمنین انفسهم</p></div>
<div class="m2"><p>هوای نفس بمان گر هوات بیع و شراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوای نفس رها کردی و عوض نرسید</p></div>
<div class="m2"><p>مگو چنین که بر آن مکرم این دروغ خطاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی که شب به خرابات قاب قوسینست</p></div>
<div class="m2"><p>درون دیده پرنور او خمار لقاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طهارتی‌ست ز غم باده شراب طهور</p></div>
<div class="m2"><p>در آن دماغ که باده‌ست باد غم ز کجاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابیت عند ربی نام آن خراباتست</p></div>
<div class="m2"><p>نشان یطعم و یسقن هم از پیمبر ماست</p></div></div>