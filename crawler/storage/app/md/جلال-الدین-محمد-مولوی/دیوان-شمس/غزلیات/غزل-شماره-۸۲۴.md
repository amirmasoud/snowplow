---
title: >-
    غزل شمارهٔ ۸۲۴
---
# غزل شمارهٔ ۸۲۴

<div class="b" id="bn1"><div class="m1"><p>عاشقان پیدا و دلبر ناپدید</p></div>
<div class="m2"><p>در همه عالم چنین عشقی که دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نارسیده یک لبی بر نقش جان</p></div>
<div class="m2"><p>صد هزاران جان‌ها تا لب رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاب قوسین از علی تیری فکند</p></div>
<div class="m2"><p>تا سپرهای فلک‌ها را درید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناکشیده دامن معشوق غیب</p></div>
<div class="m2"><p>دل هزاران محنت و ضربت کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگزیده او لب شیرین لبی</p></div>
<div class="m2"><p>چند پشت دست در هجران گزید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناچریده از لبش شاخ شکر</p></div>
<div class="m2"><p>دل هزاران عشوه او را چرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناشکفته از گلستانش گلی</p></div>
<div class="m2"><p>صد هزاران خار در سینه خلید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه جان از وی ندید الا جفا</p></div>
<div class="m2"><p>از وفاها بر امید او رمید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن الم را بر کرم‌ها فضل داد</p></div>
<div class="m2"><p>وان جفا را از وفاها برگزید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خار او از جمله گل‌ها دست برد</p></div>
<div class="m2"><p>قفل او دلکشترست از صد کلید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جور او از دور دولت گوی برد</p></div>
<div class="m2"><p>قندها از زهر قهرش بردمید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رد او به از قبول دیگران</p></div>
<div class="m2"><p>لعل و مروارید سنگش را مرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این سعادت‌های دنیا هیچ نیست</p></div>
<div class="m2"><p>آن سعادت جو که دارد بوسعید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این زیادت‌های این عالم کمیست</p></div>
<div class="m2"><p>آن زیادت جو که دارد بایزید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن زیادت دست شش انگشت تست</p></div>
<div class="m2"><p>قیمت او کم به ظاهر مستزید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن سناجو کش سنایی شرح کرد</p></div>
<div class="m2"><p>یافت فردیت ز عطار آن فرید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرب و شیرین می‌نماید پاک و خوش</p></div>
<div class="m2"><p>یک شبی بگذشت با تو شد پلید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرب و شیرین از غذای عشق خور</p></div>
<div class="m2"><p>تا پرت برروید و دانی پرید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آخر اندر غار در طفلی خلیل</p></div>
<div class="m2"><p>از سر انگشت شیری می‌مکید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن رها کن آن جنین اندر شکم</p></div>
<div class="m2"><p>آب حیوانی ز خونی می‌مزید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قد و بالایی که چرخش کرد راست</p></div>
<div class="m2"><p>عاقبت چون چرخ کژقامت خمید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قد و بالایی که عشقش برفراشت</p></div>
<div class="m2"><p>برگذشت آن قدش از عرش مجید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نی خمش کن عالم السر حاضرست</p></div>
<div class="m2"><p>نحن اقرب گفت من حبل الورید</p></div></div>