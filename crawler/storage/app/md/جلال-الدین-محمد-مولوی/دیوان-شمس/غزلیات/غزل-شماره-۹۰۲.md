---
title: >-
    غزل شمارهٔ ۹۰۲
---
# غزل شمارهٔ ۹۰۲

<div class="b" id="bn1"><div class="m1"><p>ز سر بگیرم عیشی چو پا به گنج فروشد</p></div>
<div class="m2"><p>ز روی پشت و پناهی که پشت‌ها همه رو شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر نشینم هرگز برای دل که برآید</p></div>
<div class="m2"><p>کجا برآید آن دل که کوی عشق فروشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موکلان چو آتش ز عشق سوی من آیند</p></div>
<div class="m2"><p>به سوی عشق گریزم که جمله فتنه از او شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که در سرم ز شرابش نه چشم ماند نه خوابش</p></div>
<div class="m2"><p>به دست ساقی نابش مگر سرم چو کدو شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خوان عشق نشستم چشیدم از نمک او</p></div>
<div class="m2"><p>چو لقمه کردم خود را مرا چو عشق گلو شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبو به دست دویدم به جویبار معانی</p></div>
<div class="m2"><p>که آب گشت سبویم چو آب جان به سبو شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نماز شام برفتم به سوی طرفه رومی</p></div>
<div class="m2"><p>چو دید بر در خویشم ز بام زود فروشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر از دریچه برون کرد چو شعله‌های منور</p></div>
<div class="m2"><p>که بام و خانه و بنده به جملگی همه او شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهیم دست دهان بر که نازکست معانی</p></div>
<div class="m2"><p>ز شمس مفخر تبریز سوخت جان و همو شد</p></div></div>