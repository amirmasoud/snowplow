---
title: >-
    غزل شمارهٔ ۲۸۵۳
---
# غزل شمارهٔ ۲۸۵۳

<div class="b" id="bn1"><div class="m1"><p>تو ز عشق خود نپرسی که چه خوب و دلربایی</p></div>
<div class="m2"><p>دو جهان به هم برآید چو جمال خود نمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شراب و ما سبویی تو چو آب و ما چو جویی</p></div>
<div class="m2"><p>نه مکان تو را نه سویی و همه به سوی مایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تو دل چگونه پوید نظرم چگونه جوید</p></div>
<div class="m2"><p>که سخن چگونه پرسد ز دهان که تو کجایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو به گوش دل چه گفتی که به خنده‌اش شکفتی</p></div>
<div class="m2"><p>به دهان نی چه دادی که گرفت قندخایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو به می چه جوش دادی به عسل چه نوش دادی</p></div>
<div class="m2"><p>به خرد چه هوش دادی که کند بلندرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تو خاک‌ها منقش دل خاکیان مشوش</p></div>
<div class="m2"><p>ز تو ناخوشی شده خوش که خوشی و خوش فزایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طرب از تو باطرب شد عجب از تو بوالعجب شد</p></div>
<div class="m2"><p>کرم از تو نوش لب شد که کریم و پرعطایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خسته را تو جویی ز حوادثش تو شویی</p></div>
<div class="m2"><p>سخنی به درد گویی که همو کند دوایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تو است ابر گریان ز تو است برق خندان</p></div>
<div class="m2"><p>ز تو خود هزار چندان که تو معدن وفایی</p></div></div>