---
title: >-
    غزل شمارهٔ ۲۸۳۶
---
# غزل شمارهٔ ۲۸۳۶

<div class="b" id="bn1"><div class="m1"><p>به چه روی پشت آرم به کسی که از گزینی</p></div>
<div class="m2"><p>سوی او کند خدا رو به حدیث و همنشینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه که روی و پشت عالم همه رو به قبله دارد</p></div>
<div class="m2"><p>که ز کیمیاست مس را برهیدن از مسینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همگان ز خود گریزان سوی حق و نعل ریزان</p></div>
<div class="m2"><p>که ز کاسدی رسانمان به لطافت و ثمینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه زمین ستان بخفته ز رخ فلک شکفته</p></div>
<div class="m2"><p>ز فلک نبات یابد برهد از این زمینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهد آن حبوب علوی به زمین خوشی و حلوی</p></div>
<div class="m2"><p>به بهار امانتی‌ها بنماید از امینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هله ای حیات حسی بگریز هم ز مسی</p></div>
<div class="m2"><p>سوی آسمان قدسی که تو عاشق مهینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز برای دعوت جان برسیده‌اند خوبان</p></div>
<div class="m2"><p>که بیا به معدن و کان بهل این قراضه چینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خدا که ماه رویی به خدا فرشته خویی</p></div>
<div class="m2"><p>به خدا که مشک بویی به خدا که این چنینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو که یوسف زمانی چه میان هندوانی</p></div>
<div class="m2"><p>برو آینه طلب کن بنگر که روی بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به صفا چو آسمانی به ملاطفت چو جانی</p></div>
<div class="m2"><p>به شکفتگی چنانی به نهفتگی چنینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خزینه خوب رختی ز قدیم نیکبختی</p></div>
<div class="m2"><p>به نبات چون درختی به ثبات چون یقینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شده‌ام چو موم ای جان به هوای مهر سلطان</p></div>
<div class="m2"><p>برسان به موم مهرش که گزیده‌تر نگینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هله بس که کاسه‌ها را به طعام او است قیمت</p></div>
<div class="m2"><p>و اگر نه خاک نه ارزد همه کاسه‌های چینی</p></div></div>