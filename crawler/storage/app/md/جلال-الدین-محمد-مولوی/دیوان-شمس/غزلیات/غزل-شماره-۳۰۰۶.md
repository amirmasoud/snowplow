---
title: >-
    غزل شمارهٔ ۳۰۰۶
---
# غزل شمارهٔ ۳۰۰۶

<div class="b" id="bn1"><div class="m1"><p>سیمرغ و کیمیا و مقام قلندری</p></div>
<div class="m2"><p>وصف قلندرست و قلندر از او بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی قلندرم من و این دل پذیر نیست</p></div>
<div class="m2"><p>زیرا که آفریده نباشد قلندری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دام و دم قلندر بی‌چون بود مقیم</p></div>
<div class="m2"><p>خالیست از کفایت و معنی داوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خود به خود چه جویی چون سر به سر تویی</p></div>
<div class="m2"><p>چون آب در سبویی کلی ز کل پری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خود به خود سفر کن در راه عاشقی</p></div>
<div class="m2"><p>وین قصه مختصر کن ای دوست یک سری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی بیم و نی امید نه طاعت نه معصیت</p></div>
<div class="m2"><p>نی بنده نی خدای نه وصف مجاوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجزست و قدرتست و خدایی و بندگی</p></div>
<div class="m2"><p>بیرون ز جمله آمد این ره چو بنگری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راه قلندری ز خدایی برون بود</p></div>
<div class="m2"><p>در بندگی نیاید و نه در پیمبری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زینهار تا نلافد هر عاشق از گزاف</p></div>
<div class="m2"><p>کس را نشد مسلم این راه و ره بری</p></div></div>