---
title: >-
    غزل شمارهٔ ۳۱۸۱
---
# غزل شمارهٔ ۳۱۸۱

<div class="b" id="bn1"><div class="m1"><p>ما انصف ندمانی، لو انکر ادمانی</p></div>
<div class="m2"><p>فالقهوة من شرطی، لاالتوبة من شانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریحان به سفال اندر بسیار بود دانی</p></div>
<div class="m2"><p>آن جام سفالین کو؟ وان راوق ریحانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لو تمزجها بالدم، من ادمع اجفانی</p></div>
<div class="m2"><p>یزداد لها صبغ فی احمر القانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفهای پری رویان، در بزم سلیمانی</p></div>
<div class="m2"><p>با نغمهٔ داودی، مرغ خوش الحانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا یوسف عللنی، لو لامک اخوانی</p></div>
<div class="m2"><p>کم من علل یشفی، من علة احزانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شو گوش خرد برکش، چون طفل دبستانی</p></div>
<div class="m2"><p>تا پیر مغان بینی در بلبله گردانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اقبلت علی وصلی، راحلت لهجرانی</p></div>
<div class="m2"><p>این القدم الاول؟ این‌النظر الثانی</p></div></div>