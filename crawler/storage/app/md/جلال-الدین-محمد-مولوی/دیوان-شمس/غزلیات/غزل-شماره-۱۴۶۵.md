---
title: >-
    غزل شمارهٔ ۱۴۶۵
---
# غزل شمارهٔ ۱۴۶۵

<div class="b" id="bn1"><div class="m1"><p>ای کرده تو مهمانم در پیش درآ جانم</p></div>
<div class="m2"><p>زان روی که حیرانم من خانه نمی‌دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گشته ز تو واله هم شهر و هم اهل ده</p></div>
<div class="m2"><p>کو خانه نشانم ده من خانه نمی‌دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان کس که شدی جایش زان کس مطلب دانَش</p></div>
<div class="m2"><p>پیش آ و مرنجانش من خانه نمی‌دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان کز تو بود شورش می دار تو معذورش</p></div>
<div class="m2"><p>وز خانه مکن دورش من خانه نمی‌دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من عاشق و مشتاقم من شهره آفاقم</p></div>
<div class="m2"><p>رحم آر و مکن طاقم من خانه نمی‌دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مطرب صاحب صف می زن تو به زخم کف</p></div>
<div class="m2"><p>بر راه دلم این دف من خانه نمی‌دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس الحق تبریزم جز با تو نیامیزم</p></div>
<div class="m2"><p>می افتم و می خیزم من خانه نمی‌دانم</p></div></div>