---
title: >-
    غزل شمارهٔ ۱۲۱۸
---
# غزل شمارهٔ ۱۲۱۸

<div class="b" id="bn1"><div class="m1"><p>ای شب خوش رو که تویی مهتر و سالار حبش</p></div>
<div class="m2"><p>ما ز تو شادیم همه وقت تو خوش وقت تو خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو اندرخور ما شوق تو اندر بر ما</p></div>
<div class="m2"><p>دست بنه بر سر ما دست مکش دست مکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شب خوبی و بهی جان بجهد گر بجهی</p></div>
<div class="m2"><p>گر سه عدد بر سه نهی گردد شش گردد شش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شش جهتم از رخ تو وز نظر فرخ تو</p></div>
<div class="m2"><p>هفت فلک را بدهد خوبی و کش خوبی و کش</p></div></div>