---
title: >-
    غزل شمارهٔ ۹۵۶
---
# غزل شمارهٔ ۹۵۶

<div class="b" id="bn1"><div class="m1"><p>ز جان سوخته‌ام خلق را حذار کنید</p></div>
<div class="m2"><p>که الله الله ز آتش رخان فرار کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که آتش رخشان خاصیت چنین دارد</p></div>
<div class="m2"><p>که هر قرار که دارید بی‌قرار کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی که کاهل گردد نداش می‌آید</p></div>
<div class="m2"><p>که زنده است سلیمان عشق کار کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مباش کاهل کاین قافله روانه شدست</p></div>
<div class="m2"><p>ز قافله بممانید و زود بار کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهارپای طبایع نکوبد این ره را</p></div>
<div class="m2"><p>به ترک خاک و هواها و آب و نار کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنیست چشم من از سرمه سپاهانی</p></div>
<div class="m2"><p>ز خاک تبریز او را مگر نثار کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزرگی از شه ارواح شمس تبریزست</p></div>
<div class="m2"><p>وجودها پی این کبریا صغار کنید</p></div></div>