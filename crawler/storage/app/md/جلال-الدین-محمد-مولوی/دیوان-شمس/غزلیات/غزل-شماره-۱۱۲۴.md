---
title: >-
    غزل شمارهٔ ۱۱۲۴
---
# غزل شمارهٔ ۱۱۲۴

<div class="b" id="bn1"><div class="m1"><p>تاخت رخ آفتاب گشت جهان مست وار</p></div>
<div class="m2"><p>بر مثل ذره‌ها رقص کنان پیش یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه نشسته به تخت عشق گرو کرده رخت</p></div>
<div class="m2"><p>رقص کنان هر درخت دست زنان هر چنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از قدح جام وی مست شده کو و کی</p></div>
<div class="m2"><p>گرم شده جام دی سرد شده جان نار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روح بشارت شنید پرده جان بردرید</p></div>
<div class="m2"><p>رایت احمد رسید کفر بشد زار زار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بانگ زده آن هما هر کی که هست از شما</p></div>
<div class="m2"><p>دور شو از عشق ما تا نشوی دلفکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته دل من بدو کای صنم تندخو</p></div>
<div class="m2"><p>چون برهد آن که او گشت به زخمت شکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق چو ابر گران ریخت بر این و بر آن</p></div>
<div class="m2"><p>شد طرفی زعفران شد طرفی لاله زار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب منی همچو شیر بعد زمانی یسیر</p></div>
<div class="m2"><p>زاد یکی همچو قیر وان دگری همچو قار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منکر شه کور زاد بی‌خبر و کور باد</p></div>
<div class="m2"><p>از شه ما شمس دین در تبریز افتخار</p></div></div>