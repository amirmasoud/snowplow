---
title: >-
    غزل شمارهٔ ۲۸۵۷
---
# غزل شمارهٔ ۲۸۵۷

<div class="b" id="bn1"><div class="m1"><p>چه جمال جان فزایی که میان جان مایی</p></div>
<div class="m2"><p>تو به جان چه می‌نمایی تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بدان تو راه یابی چو هزار مه بتابی</p></div>
<div class="m2"><p>تو چه آتش و چه آبی تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم عشق تو پیاده شده قلعه‌ها گشاده</p></div>
<div class="m2"><p>به سپاه نور ساده تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه زنگ را شکسته شده دست جمله بسته</p></div>
<div class="m2"><p>شه چین بس خجسته تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو چراغ طور سینا تو هزار بحر و مینا</p></div>
<div class="m2"><p>بجز از تو جان مبینا تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو برسته از فزونی ز قیاس‌ها برونی</p></div>
<div class="m2"><p>به دو چشم مست خونی تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دلم چه آذر آمد چو خیال تو درآمد</p></div>
<div class="m2"><p>دو جهان به هم برآمد تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو در آن دو رخ چه داری که فکندی از عیاری</p></div>
<div class="m2"><p>دو هزار بی‌قراری تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بدان لطیف خنده همه را بکرده بنده</p></div>
<div class="m2"><p>ز دم تو مرده زنده تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو صفات حسن ایزد عرقت به بحر ریزد</p></div>
<div class="m2"><p>دو هزار موج خیزد تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دو زلف توست طوقم ز شراب توست شوقم</p></div>
<div class="m2"><p>بنگر که در چه ذوقم تو چنین شکر چرایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز گلت سمن فنا شد همه مکر و فن فنا شد</p></div>
<div class="m2"><p>من و صد چو من فنا شد تو چنین شکر چرایی</p></div></div>