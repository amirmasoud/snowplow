---
title: >-
    غزل شمارهٔ ۷۰۰
---
# غزل شمارهٔ ۷۰۰

<div class="b" id="bn1"><div class="m1"><p>آن خواجه خوش لقا چه دارد</p></div>
<div class="m2"><p>آیینه‌اش از صفا چه دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان تا نروی تو در جوالش</p></div>
<div class="m2"><p>رختش بطلب که تا چه دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر سخنش کشان و بو گیر</p></div>
<div class="m2"><p>کز بوی می بقا چه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلشن ذوق او فرورو</p></div>
<div class="m2"><p>کز نرگس و لاله‌ها چه دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند کز انبیا بلافید</p></div>
<div class="m2"><p>از گوهر انبیا چه دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه صلوات می‌فرستند</p></div>
<div class="m2"><p>از صفوت مصطفی چه دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا سایه خود بر او مینداز</p></div>
<div class="m2"><p>کو خود چه کس است یا چه دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ساقی خویش چنگ درزن</p></div>
<div class="m2"><p>مندیش که آن سه تا چه دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمری پی زید و عمرو بردی</p></div>
<div class="m2"><p>زین پس بنگر خدا چه دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سرمجموع اصل مگذر</p></div>
<div class="m2"><p>کاین اصل جدا جدا چه دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این کاه سخن دگر مپیما</p></div>
<div class="m2"><p>بندیش که کهربا چه دارد</p></div></div>