---
title: >-
    غزل شمارهٔ ۲۵۳
---
# غزل شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>چند نهان داری آن خنده را</p></div>
<div class="m2"><p>آن مه تابنده فرخنده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده کند روی تو صد شاه را</p></div>
<div class="m2"><p>شاه کند خنده تو بنده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده بیاموز گل سرخ را</p></div>
<div class="m2"><p>جلوه کن آن دولت پاینده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسته بدانست در آسمان</p></div>
<div class="m2"><p>تا بکشد چون تو گشاینده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده قطار شترهای مست</p></div>
<div class="m2"><p>منتظرانند کشاننده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف برافشان و در آن حلقه کش</p></div>
<div class="m2"><p>حلق دو صد حلقه رباینده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز وصالست و صنم حاضرست</p></div>
<div class="m2"><p>هیچ مپا مدت آینده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق زخمست دف سخت رو</p></div>
<div class="m2"><p>میل لبست آن نی نالنده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر رخ دف چند طپانچه بزن</p></div>
<div class="m2"><p>دم ده آن نای سگالنده را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور به طمع ناله برآرد رباب</p></div>
<div class="m2"><p>خوش بگشا آن کف بخشنده را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عیب مکن گر غزل ابتر بماند</p></div>
<div class="m2"><p>نیست وفا خاطر پرنده را</p></div></div>