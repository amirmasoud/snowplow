---
title: >-
    غزل شمارهٔ ۲۳۸۲
---
# غزل شمارهٔ ۲۳۸۲

<div class="b" id="bn1"><div class="m1"><p>ای بخاری را تو جان پنداشته</p></div>
<div class="m2"><p>حبه زر را تو کان پنداشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای فرورفته چو قارون در زمین</p></div>
<div class="m2"><p>وی زمین را آسمان پنداشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بدیده لعبتان دیو را</p></div>
<div class="m2"><p>لعبتان را مردمان پنداشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای کرانه رفته عشق از ننگ تو</p></div>
<div class="m2"><p>ای تو خود را در میان پنداشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای گرفته چشمت آب از دود کفر</p></div>
<div class="m2"><p>دود را نور عیان پنداشته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ز شهوت در پلیدی همچو کرم</p></div>
<div class="m2"><p>عاشقان را همچنان پنداشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی شهوت نشان لعنت است</p></div>
<div class="m2"><p>ای نشان را بی‌نشان پنداشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای تو گندیده میان حرف و صوت</p></div>
<div class="m2"><p>وی خدا را بی‌زبان پنداشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماهتابش می‌زند بر کوریت</p></div>
<div class="m2"><p>ای تو مه را هم نهان پنداشته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه گفتم خویشتن را گفته‌ام</p></div>
<div class="m2"><p>ای تو هجو دیگران پنداشته</p></div></div>