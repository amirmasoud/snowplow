---
title: >-
    غزل شمارهٔ ۲۴۲۵
---
# غزل شمارهٔ ۲۴۲۵

<div class="b" id="bn1"><div class="m1"><p>طوبی لمن آواه سر فؤاده</p></div>
<div class="m2"><p>سکن الفؤاد بعشقه و وداده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس الکریم کمریم و فؤاده</p></div>
<div class="m2"><p>شبه المسیح و صدره کمهاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اذن الفؤاد لکی یبوح بسره</p></div>
<div class="m2"><p>شرح الصدور کرامه لعباده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحم القلوب بفتح‌ها و فتوح‌ها</p></div>
<div class="m2"><p>قهر النفوس سیاسه لجهاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشف الغطاء و لا انتظار و لا نسا</p></div>
<div class="m2"><p>فرح السعید تانسا بعتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشقوا لرأیه ربهم و تعلقوا</p></div>
<div class="m2"><p>و العرش یخضع حالهم بعماده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و صلوا الی نظر الحبیب بفضله</p></div>
<div class="m2"><p>و الحق ارشدهم بحسن رشاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>القوم معشوقون فی اوصافهم</p></div>
<div class="m2"><p>و الحق عاشقهم علی افراده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حار العقول به عاشقیه تحیرا</p></div>
<div class="m2"><p>کیف العقول به معشقیه فناده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لا تنکرن و لا تکن متصرفا</p></div>
<div class="m2"><p>بالعقل فی هذا و خف لکیاده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فالامر اعظم من تصرف حکمنا</p></div>
<div class="m2"><p>و الود بالجبار من اعقاده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملک البصیره من ممالک شیخنا</p></div>
<div class="m2"><p>یعطی و یمنع ما یشا بمراده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما غاب من قلبی شعاشع خده</p></div>
<div class="m2"><p>لا تشمتوا بصدوده و بعاده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمس المصیف اذا نی بغروبه</p></div>
<div class="m2"><p>ما غاب حر الشمس من عباده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تبریز جل به شمس دین سیدی</p></div>
<div class="m2"><p>ما اکرم المولی بکثر رماده</p></div></div>