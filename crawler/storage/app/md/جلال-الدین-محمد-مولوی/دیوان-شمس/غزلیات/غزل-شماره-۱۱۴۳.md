---
title: >-
    غزل شمارهٔ ۱۱۴۳
---
# غزل شمارهٔ ۱۱۴۳

<div class="b" id="bn1"><div class="m1"><p>تو شاخ خشک چرایی به روی یار نگر</p></div>
<div class="m2"><p>تو برگ زرد چرایی به نوبهار نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآ به حلقه رندان که مصلحت اینست</p></div>
<div class="m2"><p>شراب و شاهد و ساقی بی‌شمار نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدانک عشق جهانی است بی‌قرار در او</p></div>
<div class="m2"><p>هزار عاشق بی‌جان و بی‌قرار نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دررسی تو بدان شه که نام او نبرم</p></div>
<div class="m2"><p>به حق شاهی آن شه که شاهوار نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دیده سرمه کشی باز رو از این سو کن</p></div>
<div class="m2"><p>بدین جهان پر از دود و پرغبار نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار دود مرکب که چیست این فلکست</p></div>
<div class="m2"><p>غبار رنگ برآرد که سبزه زار نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگه مکن تو به خورشید چونک درتابد</p></div>
<div class="m2"><p>به گاه شام ورا زرد و شرمسار نگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ماه نیز به دریوزه پر کند زنبیل</p></div>
<div class="m2"><p>ز بعد پانزده روزش تو خوار و زار نگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا به بحر ملاحت به سوی کان وصال</p></div>
<div class="m2"><p>بدان دو غمزه مخمور یار غار نگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو روح قدس ببوسید نعل مرکب او</p></div>
<div class="m2"><p>ز نعل نعره برآمد که حال و کار نگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر نه عفو کند حلم شمس تبریزی</p></div>
<div class="m2"><p>تو روح را ز چنین یار شرمسار نگر</p></div></div>