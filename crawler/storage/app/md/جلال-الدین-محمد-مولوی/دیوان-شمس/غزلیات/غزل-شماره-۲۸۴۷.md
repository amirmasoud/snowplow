---
title: >-
    غزل شمارهٔ ۲۸۴۷
---
# غزل شمارهٔ ۲۸۴۷

<div class="b" id="bn1"><div class="m1"><p>دل بی‌قرار را گو که چو مستقر نداری</p></div>
<div class="m2"><p>سوی مستقر اصلی ز چه رو سفر نداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دم خوش سحرگه همه خلق زنده گردد</p></div>
<div class="m2"><p>تو چگونه دلستانی که دم سحر نداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چگونه گلستانی که گلی ز تو نروید</p></div>
<div class="m2"><p>تو چگونه باغ و راغی که یکی شجر نداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو دلا چنان شدستی ز خرابی و ز مستی</p></div>
<div class="m2"><p>سخن پدر نگویی هوس پسر نداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مثال آفتابی نروی مگر که تنها</p></div>
<div class="m2"><p>به مثال ماه شب رو حشم و حشر نداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو در این سرا چو مرغی چو هوات آرزو شد</p></div>
<div class="m2"><p>بپری ز راه روزن هله گیر در نداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و اگر گرفته جانی که نه روزن است و نی در</p></div>
<div class="m2"><p>چو عرق ز تن برون رو که جز این گذر نداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو چو جعد موی داری چه غم ار کله بیفتد</p></div>
<div class="m2"><p>تو چو کوه پای داری چه غم ار کمر نداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو فرشتگان گردون به تو تشنه‌اند و عاشق</p></div>
<div class="m2"><p>رسدت ز نازنینی که سر بشر نداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظرت ز چیست روشن اگر آن نظر ندیدی</p></div>
<div class="m2"><p>رخ تو ز چیست تابان اگر آن گهر نداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بگو مر آن ترش را ترشی ببر از این جا</p></div>
<div class="m2"><p>ور از آن شراب خوردی ز چه رو بطر نداری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر از درونه مستی و به قاصدی ترش رو</p></div>
<div class="m2"><p>بدر اندر آب و آتش که دگر خطر نداری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدهد خدا به دریا خبری که رام او شو</p></div>
<div class="m2"><p>بنهد خبر در آتش که در او اثر نداری</p></div></div>