---
title: >-
    غزل شمارهٔ ۱۱۳۳
---
# غزل شمارهٔ ۱۱۳۳

<div class="b" id="bn1"><div class="m1"><p>نه در وفات گذارد نه در جفا دلدار</p></div>
<div class="m2"><p>نه منکرت بگذارد نه بر سر اقرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر کجا که نهی دل به قهر برکندت</p></div>
<div class="m2"><p>به هیچ جای منه دل دلا و پا مفشار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شب قرار نهی روز آن بگرداند</p></div>
<div class="m2"><p>بگیر عبرت از این اختلاف لیل و نهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جهل توبه و سوگند می‌تند غافل</p></div>
<div class="m2"><p>چه حیله دارد مقهور در کف قهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برادرا سر و کار تو با کی افتادست</p></div>
<div class="m2"><p>کز اوست بی‌سر و پا گشته گنبد دوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برادرا تو کجا خفته‌ای نمی‌دانی</p></div>
<div class="m2"><p>که بر سر تو نشستست افعی بیدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه خواب‌هاست که می‌بینی ای دل مغرور</p></div>
<div class="m2"><p>چه دیگ بهر تو پختست پیر خوان سالار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار تاجر بر بوی سود شد به سفر</p></div>
<div class="m2"><p>ببرد دمدمه حکم حق ز جانش قرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنانش کرد که در شهرها نمی‌گنجید</p></div>
<div class="m2"><p>ملول شد ز بیابان و رفت سوی بحار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رود که گیرد مرجان ولیک بدهد جان</p></div>
<div class="m2"><p>که در کمین بنشستست بر رهش جرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوید در پی آب و نیافت غیر سراب</p></div>
<div class="m2"><p>دوید در پی نور و نیافت الا نار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قضا گرفته دو گوشش کشان کشان که بیا</p></div>
<div class="m2"><p>چنین کشند به سوی جوال گوش حمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بتر ز گاوی کاین چرخ را نمی‌بینی</p></div>
<div class="m2"><p>که گردن تو ببستست از برای دوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در این دوار طبیبان همه گرفتارند</p></div>
<div class="m2"><p>کز این دوار بود مست کله بیمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به بر و بحر و به دشت و به کوه می‌کشدش</p></div>
<div class="m2"><p>که تا کجاش دراند به پنجه شیر شکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولیک عاشق حق را چو بردراند شیر</p></div>
<div class="m2"><p>هلا دریدن او را چو دیگران مشمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل و جگر چو نیابد درونه تن او</p></div>
<div class="m2"><p>همان کسی که دریدش همو شود معمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو در حیات خود او کشته گشت در کف عشق</p></div>
<div class="m2"><p>به امر موتوا من قبل ان تموتوا زار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که بی‌دلست و جگرخون عاشقست یقین</p></div>
<div class="m2"><p>شکار را ندرانید هیچ شیر دو بار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وگر درید به سهوش بدوزدش در حال</p></div>
<div class="m2"><p>در او دمد دم جان و بگیردش به کنار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حرام کرد خدا شحم و لحم عاشق را</p></div>
<div class="m2"><p>که تا طمع نکند در فناش مردم خوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو عشق نوش که تریاق خاک فاروقیست</p></div>
<div class="m2"><p>که زهر زهره ندارد که دم زند ز ضرار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخن رسید به عشق و همی‌جهد دل من</p></div>
<div class="m2"><p>کجا جهد ز چنین زخم بی‌محابا تار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو قطب می‌نجهد از میان دور فلک</p></div>
<div class="m2"><p>کجا جهد تو بگو نقطه از چنین پرگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خموش باش که این هم کشاکش قدرست</p></div>
<div class="m2"><p>تو را به شعر و به اطلس مرا سوی اشعار</p></div></div>