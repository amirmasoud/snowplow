---
title: >-
    غزل شمارهٔ ۱۷۷۴
---
# غزل شمارهٔ ۱۷۷۴

<div class="b" id="bn1"><div class="m1"><p>ما به تماشای تو بازآمدیم</p></div>
<div class="m2"><p>جانب دریای تو بازآمدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیل غمت خانه دل را ببرد</p></div>
<div class="m2"><p>زود به صحرای تو بازآمدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سر ما مطبخ سودای توست</p></div>
<div class="m2"><p>بر سر سودای تو بازآمدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر چه صد رسن انداختی</p></div>
<div class="m2"><p>تا سوی بالای تو بازآمدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله سرنای تو در جان رسید</p></div>
<div class="m2"><p>در پی سرنای تو بازآمدیم</p></div></div>