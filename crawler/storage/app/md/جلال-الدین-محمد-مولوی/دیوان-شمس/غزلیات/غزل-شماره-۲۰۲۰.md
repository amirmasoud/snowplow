---
title: >-
    غزل شمارهٔ ۲۰۲۰
---
# غزل شمارهٔ ۲۰۲۰

<div class="b" id="bn1"><div class="m1"><p>ای خدا این وصل را هجران مکن</p></div>
<div class="m2"><p>سرخوشان عشق را نالان مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ جان را تازه و سرسبز دار</p></div>
<div class="m2"><p>قصد این مستان و این بستان مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خزان بر شاخ و برگ دل مزن</p></div>
<div class="m2"><p>خلق را مسکین و سرگردان مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر درختی کآشیان مرغ توست</p></div>
<div class="m2"><p>شاخ مشکن مرغ را پران مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمع و شمع خویش را برهم مزن</p></div>
<div class="m2"><p>دشمنان را کور کن شادان مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه دزدان خصم روز روشنند</p></div>
<div class="m2"><p>آنچ می‌خواهد دل ایشان مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه اقبال این حلقه است و بس</p></div>
<div class="m2"><p>کعبه اومید را ویران مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این طناب خیمه را برهم مزن</p></div>
<div class="m2"><p>خیمه توست آخر ای سلطان مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در عالم ز هجران تلخ تر</p></div>
<div class="m2"><p>هرچه خواهی کن ولیکن آن مکن</p></div></div>