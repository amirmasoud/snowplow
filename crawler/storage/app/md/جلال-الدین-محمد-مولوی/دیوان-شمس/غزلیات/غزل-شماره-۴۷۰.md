---
title: >-
    غزل شمارهٔ ۴۷۰
---
# غزل شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>ای غم اگر مو شوی پیش منت بار نیست</p></div>
<div class="m2"><p>در شکرینه یقین سرکه انکار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه تو خون خواره‌ای رهزن و عیاره‌ای</p></div>
<div class="m2"><p>قبله ما غیر آن دلبر عیار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کان شکرهاست او مستی سرهاست او</p></div>
<div class="m2"><p>ره نبرد با وی آنک مرغ شکرخوار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که دلی داشتست بنده دلبر شدست</p></div>
<div class="m2"><p>هر که ندارد دلی طالب دلدار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کل چه کند شانه را چونک ورا موی نیست</p></div>
<div class="m2"><p>پود چه کار آیدش آنک ورا تار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با سر میدان چه کار آن که بود خرسوار</p></div>
<div class="m2"><p>تا چه کند صیرفی هر کش دینار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان کلیم و خلیل جانب آتش دوان</p></div>
<div class="m2"><p>نار نماید در او جز گل و گلزار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای غم از این جا برو ور نه سرت شد گرو</p></div>
<div class="m2"><p>رنگ شب تیره را تاب مه یار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای غم پرخار رو در دل غمخوار رو</p></div>
<div class="m2"><p>نقل بخیلانه‌ات طعمه خمار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلقهٔ غین تو تنگ میمت از آن تنگتر</p></div>
<div class="m2"><p>تنگ متاع تو را عشق خریدار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای غم شادی شکن پر شکرست این دهن</p></div>
<div class="m2"><p>کز شکرآکندگی ممکن گفتار نیست</p></div></div>