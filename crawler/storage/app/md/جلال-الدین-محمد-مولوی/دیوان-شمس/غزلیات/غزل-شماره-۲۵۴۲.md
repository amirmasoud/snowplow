---
title: >-
    غزل شمارهٔ ۲۵۴۲
---
# غزل شمارهٔ ۲۵۴۲

<div class="b" id="bn1"><div class="m1"><p>بتاب ای ماه بر یارم بگو یارا اغا پوسی</p></div>
<div class="m2"><p>بزن ای باد بر زلفش که ای زیبا اغا پوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر این جایی گر آن جایی وگر آیی وگر نایی</p></div>
<div class="m2"><p>همه قندی و حلوایی زهی حلوا اغا پوسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملامت نشنوم هرگز نگردم در طلب عاجز</p></div>
<div class="m2"><p>نباشد عشق بازیچه بیا حقا اغا پوسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر در خاک بنهندم تویی دلدار و دلبندم</p></div>
<div class="m2"><p>وگر بر چرخ آرندم از آن بالا اغا پوسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بالای که باشم چو رهبان عشق تو جویم</p></div>
<div class="m2"><p>وگر در قعر دریاام در آن دریا اغا پوسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تاب روی تو ماها ز احسان‌های تو شاها</p></div>
<div class="m2"><p>شده زندان مرا صحرا در آن صحرا اغا پوسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مست دیدن اویم دو دست از شرم واشویم</p></div>
<div class="m2"><p>بگیرم در رهش گویم که ای مولا اغا پوسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلارام خوش روشن ستیزه می‌کند با من</p></div>
<div class="m2"><p>بیار ای اشک و بر وی زن بگو ایلا اغا پوسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را هر جان همی‌جوید که تا پای تو را بوسد</p></div>
<div class="m2"><p>ندارد زهره تا گوید بیا این جا اغا پوسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر از بنده سیرابی بگیری خشم و دیر آیی</p></div>
<div class="m2"><p>بماند بی‌کس و تنها تو را تنها اغا پوسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا ای باغ و ای گلشن بیا ای سرو و ای سوسن</p></div>
<div class="m2"><p>برای کوری دشمن بگو ما را اغا پوسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا پهلوی من بنشین به رسم و عادت پیشین</p></div>
<div class="m2"><p>بجنبان آن لب شیرین که مولانا اغا پوسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منم نادان تویی دانا تو باقی را بگو جانا</p></div>
<div class="m2"><p>به گویایی افیغومی به ناگویا اغا پوسی</p></div></div>