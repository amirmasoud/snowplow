---
title: >-
    غزل شمارهٔ ۱۹۴۲
---
# غزل شمارهٔ ۱۹۴۲

<div class="b" id="bn1"><div class="m1"><p>من ز گوش او بدزدم حلقه دیگر نهان</p></div>
<div class="m2"><p>تا نداند چشم دشمن ور بداند گو بدان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رخم خطی نبشت و من نهان می داشتم</p></div>
<div class="m2"><p>زین سپس پنهان ندارم هر کی خواند گو بخوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوق زر عشق او هم لایق این گردن است</p></div>
<div class="m2"><p>بشکند از طوق عشقش گردن گردن کشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوس محمودی همه بر اشتر محمود باد</p></div>
<div class="m2"><p>بار دل هم دل کشد محرم کجا باشد زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آینه آهن دلی باید که تا زخمش کشد</p></div>
<div class="m2"><p>زخم آیینه نباشد درخور آیینه دان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک روی دوست بینی بی‌خبر باشی ز زخم</p></div>
<div class="m2"><p>چون زنان مصر بیخود در جمال یوسفان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد هزاران حسن یوسف در جمال روی کیست</p></div>
<div class="m2"><p>شمس تبریزی ما آن خوش نشین خوش نشان</p></div></div>