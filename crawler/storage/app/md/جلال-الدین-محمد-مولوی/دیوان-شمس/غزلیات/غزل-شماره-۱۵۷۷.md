---
title: >-
    غزل شمارهٔ ۱۵۷۷
---
# غزل شمارهٔ ۱۵۷۷

<div class="b" id="bn1"><div class="m1"><p>امروز نیم ملول شادم</p></div>
<div class="m2"><p>غم را همه طاق برنهادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سبلت هر کجا ملولی است</p></div>
<div class="m2"><p>گر میر من است و اوستادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز میان به عیش بستم</p></div>
<div class="m2"><p>روبند ز روی مه گشادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز ظریفم و لطیفم</p></div>
<div class="m2"><p>گویی که مگر ز لطف زادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاری که نداد بوسه از ناز</p></div>
<div class="m2"><p>او بوسه بجست و من ندادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دوش عجب چه خواب دیدم</p></div>
<div class="m2"><p>کامروز عظیم بامرادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی تو که رو که پادشاهی</p></div>
<div class="m2"><p>آری که خوش و خجسته بادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌ساقی و بی‌شراب مستم</p></div>
<div class="m2"><p>بی‌تخت و کلاه کیقبادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در من ز کجا رسد گمان‌ها</p></div>
<div class="m2"><p>سبحان الله کجا فتادم</p></div></div>