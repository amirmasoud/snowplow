---
title: >-
    غزل شمارهٔ ۳۴۶
---
# غزل شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>شنیدم مر مرا لطفت دعا گفت</p></div>
<div class="m2"><p>برای بنده خود لطف‌ها گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه گویم من مکافات تو ای جان</p></div>
<div class="m2"><p>که نیکی تو را جانا خدا گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیکن جان این کمتر دعاگو</p></div>
<div class="m2"><p>همه شب روی ماهت را دعا گفت</p></div></div>