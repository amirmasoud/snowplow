---
title: >-
    غزل شمارهٔ ۸۳۲
---
# غزل شمارهٔ ۸۳۲

<div class="b" id="bn1"><div class="m1"><p>شب شد و هنگام خلوتگاه شد</p></div>
<div class="m2"><p>قبله عشاق روی ماه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه پرستان ماه خندیدن گرفت</p></div>
<div class="m2"><p>شب روان خیزید وقت راه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب آمد ما و من‌ها لا شدند</p></div>
<div class="m2"><p>وقت آن بی‌خواب الاالله شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مغزها آمیخته با کاه تن</p></div>
<div class="m2"><p>تن بخفت و دانه‌ها بی‌کاه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هندوان خرگاه تن را روفتند</p></div>
<div class="m2"><p>ترک خلوت دید و در خرگاه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت و گوهای جهان را آب برد</p></div>
<div class="m2"><p>وقت گفتن های شاهنشاه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس تبریزی چو آمد در میان</p></div>
<div class="m2"><p>اهل معنی را سخن کوتاه شد</p></div></div>