---
title: >-
    غزل شمارهٔ ۲۱۵۲
---
# غزل شمارهٔ ۲۱۵۲

<div class="b" id="bn1"><div class="m1"><p>سخت خوش است چشم تو و آن رخ گلفشان تو</p></div>
<div class="m2"><p>دوش چه خورده‌ای دلا راست بگو به جان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه گر است نام تو پرشکر است دام تو</p></div>
<div class="m2"><p>باطرب است جام تو بانمک است نان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرده اگر ببیندت فهم کند که سرخوشی</p></div>
<div class="m2"><p>چند نهان کنی که می فاش کند نهان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی کباب می‌زند از دل پرفغان من</p></div>
<div class="m2"><p>بوی شراب می‌زند از دم و از فغان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر خدا بیا بگو ور نه بهل مرا که تا</p></div>
<div class="m2"><p>یک دو سخن به نایبی بردهم از زبان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوبی جمله شاهدان مات شد و کساد شد</p></div>
<div class="m2"><p>چون بنمود ذره‌ای خوبی بی‌کران تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازبدید چشم ما آنچ ندید چشم کس</p></div>
<div class="m2"><p>بازرسید پیر ما بیخود و سرگران تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر نفسی بگوییم عقل تو کو چه شد تو را</p></div>
<div class="m2"><p>عقل نماند بنده را در غم و امتحان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر سحری چو ابر دی بارم اشک بر درت</p></div>
<div class="m2"><p>پاک کنم به آستین اشک ز آستان تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشرق و مغرب ار روم ور سوی آسمان شوم</p></div>
<div class="m2"><p>نیست نشان زندگی تا نرسد نشان تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زاهد کشوری بدم صاحب منبری بدم</p></div>
<div class="m2"><p>کرد قضا دل مرا عاشق و کف زنان تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از می این جهانیان حق خدا نخورده‌ام</p></div>
<div class="m2"><p>سخت خراب می‌شوم خائفم از گمان تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صبر پرید از دلم عقل گریخت از سرم</p></div>
<div class="m2"><p>تا به کجا کشد مرا مستی بی‌امان تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیر سیاه عشق تو می‌کند استخوان من</p></div>
<div class="m2"><p>نی تو ضمان من بدی پس چه شد این ضمان تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای تبریز بازگو بهر خدا به شمس دین</p></div>
<div class="m2"><p>کاین دو جهان حسد برد بر شرف جهان تو</p></div></div>