---
title: >-
    غزل شمارهٔ ۶۴۰
---
# غزل شمارهٔ ۶۴۰

<div class="b" id="bn1"><div class="m1"><p>تا باد سعادت ز محمد خبر افکند</p></div>
<div class="m2"><p>زان مردی و زان حمله شقاوت سپر افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حال گدا نیست عجب گر شود او پست</p></div>
<div class="m2"><p>تیغ غم تو از سر صد شاه سر افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی پسر ادهم اندر پی آهو</p></div>
<div class="m2"><p>مانند فلک مرکب شبدیز برافکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دادیش یکی شربت کز لذت و بویش</p></div>
<div class="m2"><p>مستیش به سر برشد و از اسب درافکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتند همه کس به سر کوی تحیر</p></div>
<div class="m2"><p>مسکین پسر ادهم تاج و کمر افکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نام تو بود آنک سلیمان به یکی مرغ</p></div>
<div class="m2"><p>در ملکت بلقیس شکوه و ظفر افکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از یاد تو بود آنک محمد به اشارت</p></div>
<div class="m2"><p>غوغای دو نیمه شدن اندر قمر افکند</p></div></div>