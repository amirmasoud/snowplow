---
title: >-
    غزل شمارهٔ ۲۴۶۳
---
# غزل شمارهٔ ۲۴۶۳

<div class="b" id="bn1"><div class="m1"><p>آه چه دیوانه شدم در طلب سلسله‌ای</p></div>
<div class="m2"><p>در خم گردون فکنم هر نفسی غلغله‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر قدم می‌سپرم هر سحری بادیه‌ای</p></div>
<div class="m2"><p>خون جگر می‌سپرم در طلب قافله‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از آن کس که زند بر دل من داغ عجب</p></div>
<div class="m2"><p>بر کف پای دل من از ره او آبله‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم به فلک درفکند زهره ز بامش شرری</p></div>
<div class="m2"><p>هم به زمین درفکند هیبت او زلزله‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ تقاضا نکنم ور بکنم دفع دهد</p></div>
<div class="m2"><p>صد چو مرا دفع کند او به یکی هین هله‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونک از او دفع شوم گوشگکی سر بنهم</p></div>
<div class="m2"><p>آید عشق چله گر بر سر من با چله‌ای</p></div></div>