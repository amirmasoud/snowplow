---
title: >-
    غزل شمارهٔ ۲۵۱
---
# غزل شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>پیشتر آ پیشتر ای بوالوفا</p></div>
<div class="m2"><p>از من و ما بگذر و زوتر بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشتر آ درگذر از ما و من</p></div>
<div class="m2"><p>پیشتر آ تا نه تو باشی نه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کبر و تکبر بگذار و بگیر</p></div>
<div class="m2"><p>در عوض کبر چنین کبریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت الست و تو بگفتی بلی</p></div>
<div class="m2"><p>شکر بلی چیست کشیدن بلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر بلی چیست که یعنی منم</p></div>
<div class="m2"><p>حلقه زن درگه فقر و فنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم برو از جا و هم از جا مرو</p></div>
<div class="m2"><p>جا ز کجا حضرت بی‌جا کجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاک شو از خویش و همه خاک شو</p></div>
<div class="m2"><p>تا که ز خاک تو بروید گیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور چو گیا خشک شوی خوش بسوز</p></div>
<div class="m2"><p>تا که ز سوز تو فروزد ضیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور شوی از سوز چو خاکستری</p></div>
<div class="m2"><p>باشد خاکستر تو کیمیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنگر در غیب چه سان کیمیاست</p></div>
<div class="m2"><p>کو ز کف خاک بسازد تو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از کف دریا بنگارد زمین</p></div>
<div class="m2"><p>دود سیه را بنگارد سما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لقمه نان را مدد جان کند</p></div>
<div class="m2"><p>باد نفس را دهد این علم‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش چنین کار و کیا جان بده</p></div>
<div class="m2"><p>فقر به جان داند جود و سخا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان پر از علت او را دهی</p></div>
<div class="m2"><p>جان بستانی خوش و بی‌منتها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس کنم این گفتن و خامش کنم</p></div>
<div class="m2"><p>در خمشی به سخن جان فزا</p></div></div>