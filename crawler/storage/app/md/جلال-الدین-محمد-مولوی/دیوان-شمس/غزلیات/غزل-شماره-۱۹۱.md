---
title: >-
    غزل شمارهٔ ۱۹۱
---
# غزل شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>بیدار کن طرب را بر من بزن تو خود را</p></div>
<div class="m2"><p>چشمی چنین بگردان کوری چشم بد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را بزن تو بر من اینست زنده کردن</p></div>
<div class="m2"><p>بر مرده زن چو عیسی افسون معتمد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای رویت از قمر به آن رو به روی من نه</p></div>
<div class="m2"><p>تا بنده دیده باشد صد دولت ابد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در واقعه بدیدم کز قند تو چشیدم</p></div>
<div class="m2"><p>با آن نشان که گفتی این بوسه نام زد را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان فرشته بودی یا رب چه گشته بودی</p></div>
<div class="m2"><p>کز چهره می‌نمودی لم یتخذ ولد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دست تو کشیدم صورت دگر ندیدم</p></div>
<div class="m2"><p>بی هوشیی بدیدم گم کرده مر خرد را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام چو نار درده بی‌رحم وار درده</p></div>
<div class="m2"><p>تا گم شوم ندانم خود را و نیک و بد را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این بار جام پر کن لیکن تمام پر کن</p></div>
<div class="m2"><p>تا چشم سیر گردد یک سو نهد حسد را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درده میی ز بالا در لا اله الا</p></div>
<div class="m2"><p>تا روح اله بیند ویران کند جسد را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از قالب نمدوش رفت آینه خرد خوش</p></div>
<div class="m2"><p>چندانک خواهی اکنون می‌زن تو این نمد را</p></div></div>