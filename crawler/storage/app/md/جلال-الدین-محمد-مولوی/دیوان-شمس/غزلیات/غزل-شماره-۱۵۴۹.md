---
title: >-
    غزل شمارهٔ ۱۵۴۹
---
# غزل شمارهٔ ۱۵۴۹

<div class="b" id="bn1"><div class="m1"><p>زنهار مرا مگو که پیرم</p></div>
<div class="m2"><p>پیری و فنا کجا پذیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ماهی چشمه حیاتم</p></div>
<div class="m2"><p>من غرقه بحر شهد و شیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز از لب لعل جان ننوشم</p></div>
<div class="m2"><p>غیر سر زلف او نگیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر کژ نهدم کمان ابرو</p></div>
<div class="m2"><p>در حکم کمان او چو تیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انداخته‌ای چو تیر دورم</p></div>
<div class="m2"><p>برگیر که از تو ناگزیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرم تو دهی چرا نپرم</p></div>
<div class="m2"><p>میرم چو تویی چرا بمیرم</p></div></div>