---
title: >-
    غزل شمارهٔ ۶۱۱
---
# غزل شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>ای خفته شب تیره هنگام دعا آمد</p></div>
<div class="m2"><p>وی نفس جفاپیشه هنگام وفا آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر به سوی روزن بگشای در توبه</p></div>
<div class="m2"><p>پرداخته کن خانه هین نوبت ما آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جرم و جفاجویی چون دست نمی‌شویی</p></div>
<div class="m2"><p>بر روی بزن آبی میقات صلا آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین قبله به یاد آری چون رو به لحد آری</p></div>
<div class="m2"><p>سودت نکند حسرت آنگه که قضا آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین قبله بجو نوری تا شمع لحد باشد</p></div>
<div class="m2"><p>آن نور شود گلشن چون نور خدا آمد</p></div></div>