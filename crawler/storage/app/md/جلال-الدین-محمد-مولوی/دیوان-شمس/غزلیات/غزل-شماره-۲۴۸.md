---
title: >-
    غزل شمارهٔ ۲۴۸
---
# غزل شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>گوش من منتظر پیام تو را</p></div>
<div class="m2"><p>جان به جان جسته یک سلام تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلم خون شوق می‌جوشد</p></div>
<div class="m2"><p>منتظر بوی جوش جام تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ز شیرینی و دلاویزی</p></div>
<div class="m2"><p>دانه حاجت نبوده دام تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده شاهان نثار تاج و کمر</p></div>
<div class="m2"><p>مر قبای کمین غلام تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اول عشق من گمان بردم</p></div>
<div class="m2"><p>که تصور کنم ختام تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلسله‌ام کن به پای اشتر بند</p></div>
<div class="m2"><p>من طمع کی کنم سنام تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنک شیری ز لطف تو خوردست</p></div>
<div class="m2"><p>مرگ بیند یقین فطام تو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حق آن زبان کاشف غیب</p></div>
<div class="m2"><p>که به گوشم رسان پیام تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حق آن سرای دولت بخش</p></div>
<div class="m2"><p>بنمایم ز دور بام تو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر سر از سجده تو سود کند</p></div>
<div class="m2"><p>چه زیانست لطف عام تو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس تبریز این دل آشفته</p></div>
<div class="m2"><p>بر جگر بسته است نام تو را</p></div></div>