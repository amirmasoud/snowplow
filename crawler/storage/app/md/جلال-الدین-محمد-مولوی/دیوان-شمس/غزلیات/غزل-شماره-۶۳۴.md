---
title: >-
    غزل شمارهٔ ۶۳۴
---
# غزل شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>نک ماه رجب آمد تا ماه عجب بیند</p></div>
<div class="m2"><p>وز سوختگان ره گرمی و طلب بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سجده کنان آید در امن و امان آید</p></div>
<div class="m2"><p>ور بی‌ادبی آرد سیلی و ادب بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حکمی که کند یزدان راضی بود و شادان</p></div>
<div class="m2"><p>ور سر کشد از سلطان در حلق کنب بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر درخور عشق آید خرم چو دمشق آید</p></div>
<div class="m2"><p>ور دل ندهد دل را ویران چو حلب بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوید چه سبب باشد آن خرم و این ویران</p></div>
<div class="m2"><p>جان خضری باید تا جان سبب بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد شعبان عمدا از بهر برات ما</p></div>
<div class="m2"><p>تا روزی و بی‌روزی از بخشش رب بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه رمضان آمد آن بند دهان آمد</p></div>
<div class="m2"><p>زد بر دهن بسته تا لذت لب بیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمد قدح روزه بشکست قدح‌ها را</p></div>
<div class="m2"><p>تا منکر این عشرت بی‌باده طرب بیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سغراق معانی را بر معده خالی زن</p></div>
<div class="m2"><p>معشوقه خلوت را هم چشم عزب بیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با غره دولت گو هم بگذرد این نوبت</p></div>
<div class="m2"><p>چون بگذرد این نوبت هم نوبت تب بیند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نوبت بگذار و رو نوبت زن احمد شو</p></div>
<div class="m2"><p>تا برف وجود تو خورشید عرب بیند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خامش کن و کمتر گو بسیار کسی گوید</p></div>
<div class="m2"><p>کو جاه و هوا جوید تا نام و لقب بیند</p></div></div>