---
title: >-
    غزل شمارهٔ ۲۷۵۹
---
# غزل شمارهٔ ۲۷۵۹

<div class="b" id="bn1"><div class="m1"><p>گویم سخن لب تو یا نی</p></div>
<div class="m2"><p>ای لعل لب تو را بها نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گفته ما غلام آن دم</p></div>
<div class="m2"><p>کان جا همگی تویی و ما نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این جا که منم به جز خطا نی</p></div>
<div class="m2"><p>و آن جا که تویی به جز عطا نی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این جا گفتن ز روی جسم است</p></div>
<div class="m2"><p>و آن جا همه هستی است جا نی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیاره همی‌روند پا نی</p></div>
<div class="m2"><p>صد مشک روانه و سقا نی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنجورانند همچو ایوب</p></div>
<div class="m2"><p>دریافته صحت و دوا نی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی چشمانند همچو یعقوب</p></div>
<div class="m2"><p>بینا شده چشم و توتیا نی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ره پویانند همچو ماهی</p></div>
<div class="m2"><p>بینند طریق‌ها ضیا نی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از رشک تو من دهان ببستم</p></div>
<div class="m2"><p>شرح تو رسد به منتها نی</p></div></div>