---
title: >-
    غزل شمارهٔ ۳۲۰۰
---
# غزل شمارهٔ ۳۲۰۰

<div class="b" id="bn1"><div class="m1"><p>هذا طبیبی، عند الدوآء</p></div>
<div class="m2"><p>هذا حبیبی، عند الوء</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هذا لباسی، هذا کناسی</p></div>
<div class="m2"><p>هذا شرابی، هذا غذایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هذا انیسی، عندالفراق</p></div>
<div class="m2"><p>هذا خلاصی، عند البء</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قالوا تسلی، حاشا و کلا</p></div>
<div class="m2"><p>قلبی مقیم، وسط الوفء</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این کان احمد، قلبی تعمد</p></div>
<div class="m2"><p>روحی فداه، عند الفنء</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ان کان شاکی، یبغی هلاکی</p></div>
<div class="m2"><p>سمعا و طاعه ذا مشتهایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هذا سلحدار، لایدخل الدار</p></div>
<div class="m2"><p>الا بدینار، عند الابء</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مونی حیاتی، حصدی نباتی</p></div>
<div class="m2"><p>حبسی نجاتی، مقتی بقایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا من یلمنی، مالک و مالی</p></div>
<div class="m2"><p>صبری محال فی الاتقء</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روحی مصیب، قلبی مصاف</p></div>
<div class="m2"><p>صبری مذاب، فی حرنایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انا نسینا، ما قد لقینا</p></div>
<div class="m2"><p>لما راینا، بدر الضیء</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا ذوفنونی، ابصر جنونی</p></div>
<div class="m2"><p>فوق‌الظنون، خرق الحیاء</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امروز دلبر یکبار دیگر</p></div>
<div class="m2"><p>آمد که گیرد مرغ هوایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر او پذیرد، ده ده بگیرد</p></div>
<div class="m2"><p>لیکن بخیلست، در رخ نمایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر گرد دلبر، پانصد کبوتر</p></div>
<div class="m2"><p>پر می‌فشانند، بهر گوایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای نیم مرده، پران شو اینجا</p></div>
<div class="m2"><p>کاینجا نماند، بی‌اشتهایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مستان کم زن، رستند از تن</p></div>
<div class="m2"><p>دزدم گلیمی، من از کسایی</p></div></div>