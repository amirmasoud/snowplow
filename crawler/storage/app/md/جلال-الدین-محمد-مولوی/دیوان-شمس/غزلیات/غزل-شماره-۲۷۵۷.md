---
title: >-
    غزل شمارهٔ ۲۷۵۷
---
# غزل شمارهٔ ۲۷۵۷

<div class="b" id="bn1"><div class="m1"><p>کژزخمه مباش تا توانی</p></div>
<div class="m2"><p>هر زخمه که کژ زنی بمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیر است عروس عیش دنیا</p></div>
<div class="m2"><p>مرگش طلبی اگر ستانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا رخ ننمود جمله نور است</p></div>
<div class="m2"><p>چون رخ بنمود شد دخانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سیل بلا چو کاه مگریز</p></div>
<div class="m2"><p>در عشق و ولا چو پهلوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون آب روان به هر نباتی</p></div>
<div class="m2"><p>باید که حیات را رسانی</p></div></div>