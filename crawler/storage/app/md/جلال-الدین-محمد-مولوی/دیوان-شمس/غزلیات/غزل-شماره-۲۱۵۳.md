---
title: >-
    غزل شمارهٔ ۲۱۵۳
---
# غزل شمارهٔ ۲۱۵۳

<div class="b" id="bn1"><div class="m1"><p>ای تو امان هر بلا ما همه در امان تو</p></div>
<div class="m2"><p>جان همه خوش است در سایه لطف جان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه همه جهان تویی اصل همه کسان تویی</p></div>
<div class="m2"><p>چونک تو هستی آن ما نیست غم از کسان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر غم تو ای قمر آمد دوش بر جگر</p></div>
<div class="m2"><p>گفت مرا ز بام و در صد سقط از زبان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جست دلم ز قال او رفت بر خیال او</p></div>
<div class="m2"><p>شاید ای نبات خو این همه در زمان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان مرا در این جهان آتش توست در دهان</p></div>
<div class="m2"><p>از هوس وصال تو وز طلب جهان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست مرا ز جسم و جان در ره عشق تو نشان</p></div>
<div class="m2"><p>ز آنک نغول می‌روم در طلب نشان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده بدید جوهرت لنگ شده‌ست بر درت</p></div>
<div class="m2"><p>مانده‌ام ای جواهری بر طرف دکان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاد شود دل و جگر چون بگشایی آن کمر</p></div>
<div class="m2"><p>بازگشا تو خوش قبا آن کمر از میان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نظری به جان کنی جان مرا چو کان کنی</p></div>
<div class="m2"><p>در تبریز شمس دین نقد رسم به کان تو</p></div></div>