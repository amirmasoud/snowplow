---
title: >-
    غزل شمارهٔ ۲۰۵۲
---
# غزل شمارهٔ ۲۰۵۲

<div class="b" id="bn1"><div class="m1"><p>ای آنک از میانه کران می‌کنی مکن</p></div>
<div class="m2"><p>با ما ز خشم روی گران می‌کنی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دربند سود خویشی و اندر زیان ما</p></div>
<div class="m2"><p>کس زین نکرد سود زیان می‌کنی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راضی شدی که بیش نجویی زیان ما</p></div>
<div class="m2"><p>این از پی رضای کیان می‌کنی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جای باده سرکه غم می‌دهی مده</p></div>
<div class="m2"><p>در جوی آب خون چه روان می‌کنی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چهره‌ام نشاط طرب می‌بری مبر</p></div>
<div class="m2"><p>بر چهره‌ام ز دیده نشان می‌کنی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مظلوم می‌کشی و تظلم همی‌کنی</p></div>
<div class="m2"><p>خود راه می‌زنی و فغان می‌کنی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پایم به کار نیست که سرمست دلبرم</p></div>
<div class="m2"><p>مر مست را بهل چه کشان می‌کنی مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی بیا که بر تو کنم صبر را شبان</p></div>
<div class="m2"><p>بر بره گرگ را چه شبان می‌کنی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در روز زاهدی و به شب زاهدان کشی</p></div>
<div class="m2"><p>امشب که آشتی است همان می‌کنی مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دوستان ز رشک تو خصمان همدگر</p></div>
<div class="m2"><p>این دوست را چه دشمن آن می‌کنی مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویی که می مخور پس اگر می همی‌دهی</p></div>
<div class="m2"><p>مخمور را چه خشک دهان می‌کنی مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گویی چو تیر راست رو اندر هوای ما</p></div>
<div class="m2"><p>پس تیر راست را چه کمان می‌کنی مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گویی خموش کن تو خموشم نمی‌هلی</p></div>
<div class="m2"><p>هر موی را ز عشق زبان می‌کنی مکن</p></div></div>