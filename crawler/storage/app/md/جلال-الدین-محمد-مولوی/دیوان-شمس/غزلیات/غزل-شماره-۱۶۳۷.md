---
title: >-
    غزل شمارهٔ ۱۶۳۷
---
# غزل شمارهٔ ۱۶۳۷

<div class="b" id="bn1"><div class="m1"><p>منم آن دزد که شب نقب زدم ببریدم</p></div>
<div class="m2"><p>سر صندوق گشادم گهری دزدیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز زلیخای حرم چادر سر بربودم</p></div>
<div class="m2"><p>چو بدیدم رخ یوسف کف خود ببریدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر سودای کسی قصد سر من دارد</p></div>
<div class="m2"><p>کی برد سر ز کف آنک از آن سر دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بگفتم نبرم سر سر من گفت آمین</p></div>
<div class="m2"><p>چون غمش کند ز بیخم پس از آن روییدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چه ماه است که اندر دل و جان‌ها گردد</p></div>
<div class="m2"><p>که من از گردش او بس چو فلک گردیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان اخوان صفا اوست که اندر هوسش</p></div>
<div class="m2"><p>همه دردی جهان در سر خود مالیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر این چاه جهان یوسف حسنی است نهان</p></div>
<div class="m2"><p>من بر این چرخ از او همچو رسن پیچیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هله ای عشق بیا یار منی در دو جهان</p></div>
<div class="m2"><p>از همه خلق بریدم به تو برچفسیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان چنین در فرحم کز قدحت سرمستم</p></div>
<div class="m2"><p>زان گزیده‌ست مرا حق که تو را بگزیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنهان از همه خلقان چه خوش آیین باغی است</p></div>
<div class="m2"><p>که چو گل در چمنش جامه جان بدریدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر آن باغ یکی دلبر بالاشجری است</p></div>
<div class="m2"><p>که چو برگ از شجر اندر قدمش ریزیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس کنم آنچ بگفت او که بگو من گفتم</p></div>
<div class="m2"><p>و آنچ فرمود بپوشان و مگو پوشیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس تبریز که آفاق از او شد پرنور</p></div>
<div class="m2"><p>من به هر سوی چو سایه ز پیش گردیدم</p></div></div>