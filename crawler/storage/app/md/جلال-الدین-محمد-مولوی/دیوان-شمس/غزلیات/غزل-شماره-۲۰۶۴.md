---
title: >-
    غزل شمارهٔ ۲۰۶۴
---
# غزل شمارهٔ ۲۰۶۴

<div class="b" id="bn1"><div class="m1"><p>باز فروریخت عشق از در و دیوار من</p></div>
<div class="m2"><p>باز ببرید بند اشتر کین دار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار دگر شیر عشق پنجه خونین گشاد</p></div>
<div class="m2"><p>تشنه خون گشت باز این دل سگسار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز سر ماه شد نوبت دیوانگی است</p></div>
<div class="m2"><p>آه که سودی نکرد دانش بسیار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار دگر فتنه زاد جمره دیگر فتاد</p></div>
<div class="m2"><p>خواب مرا بست باز دلبر بیدار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر مرا خواب برد عقل مرا آب برد</p></div>
<div class="m2"><p>کار مرا یار برد تا چه شود کار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلسله عاشقان با تو بگویم که چیست</p></div>
<div class="m2"><p>آنک مسلسل شود طره دلدار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیز دگربار خیز خیز که شد رستخیز</p></div>
<div class="m2"><p>مایه صد رستخیز شور دگربار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ز خزان گلستان چون دل عاشق بسوخت</p></div>
<div class="m2"><p>نک رخ آن گلستان گلشن و گلزار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باغ جهان سوخته باغ دل افروخته</p></div>
<div class="m2"><p>سوخته اسرار باغ ساخته اسرار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوبت عشرت رسید ای تن محبوس من</p></div>
<div class="m2"><p>خلعت صحت رسید ای دل بیمار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیر خرابات هین از جهت شکر این</p></div>
<div class="m2"><p>رو گرو می‌بنه خرقه و دستار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرقه و دستار چیست این نه ز دون همتی است</p></div>
<div class="m2"><p>جان و جهان جرعه‌ای است از شه خمار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داد سخن دادمی سوسن آزادمی</p></div>
<div class="m2"><p>لیک ز غیرت گرفت دل ره گفتار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکر که آن ماه را هر طرفی مشتری است</p></div>
<div class="m2"><p>نیست ز دلال گفت رونق بازار من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عربده قال نیست حاجت دلال نیست</p></div>
<div class="m2"><p>جعفر طرار نیست جعفر طیار من</p></div></div>