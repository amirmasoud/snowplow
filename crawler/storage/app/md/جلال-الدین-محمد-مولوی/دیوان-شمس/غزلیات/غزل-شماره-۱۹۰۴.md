---
title: >-
    غزل شمارهٔ ۱۹۰۴
---
# غزل شمارهٔ ۱۹۰۴

<div class="b" id="bn1"><div class="m1"><p>کجا خواهی ز چنگ ما پریدن</p></div>
<div class="m2"><p>کی داند دام قدرت را دریدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو پایت نیست تا از ما گریزی</p></div>
<div class="m2"><p>بنه گردن رها کن سر کشیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوان شو سوی شیرینی چو غوره</p></div>
<div class="m2"><p>به باطن گر نمی‌دانی دویدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسن را می گزی ای صید بسته</p></div>
<div class="m2"><p>نبرد این رسن هیچ از گزیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌بینی سرت اندر زه ماست</p></div>
<div class="m2"><p>کمانی بایدت از زه خمیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه جفته می زنی کز بار رستم</p></div>
<div class="m2"><p>یکی دم هشتمت بهر چریدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل دریا ز بیم و هیبت ما</p></div>
<div class="m2"><p>همی‌جوشد ز موج و از طپیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که سنگین اگر آن زخم یابد</p></div>
<div class="m2"><p>ز بند ما نیارد برجهیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک را تا نگوید امر ما بس</p></div>
<div class="m2"><p>به گرد خاک ما باید تنیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوا شیری است از پستان شیطان</p></div>
<div class="m2"><p>بود عقل تو شیر خر مکیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دهان خاک خشک از حسرت ماست</p></div>
<div class="m2"><p>نیارد جرعه‌ای بی‌ما چشیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی یارد صید ما را قصد کردن</p></div>
<div class="m2"><p>کی یارد بنده ما را خریدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسی را که ربودیم و گزیدیم</p></div>
<div class="m2"><p>که را خواهد به غیر ما گزیدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امانی نیست جان را در جز عشق</p></div>
<div class="m2"><p>میان عاشقان باید خزیدن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امان هر دو عالم عاشقان راست</p></div>
<div class="m2"><p>چنین بودند وقت آفریدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نشاید بره را از جور چوپان</p></div>
<div class="m2"><p>ز چوپان جانب گرگان رمیدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که این چوپان نریزد خون بره</p></div>
<div class="m2"><p>که او جاوید داند پروریدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدان کاصحاب تن اصحاب فیلند</p></div>
<div class="m2"><p>به کعبه کی تواند بررسیدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که کعبه ناف عالم پیل بینی است</p></div>
<div class="m2"><p>نتان بینی بر نافی کشیدن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ابابیلی شو و از پیل مگریز</p></div>
<div class="m2"><p>ابابیل است دل در دانه چیدن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بچینند دشمنان را همچو دانه</p></div>
<div class="m2"><p>پیام کعبه را داند شنیدن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز دل خواهی شدن بر آسمان‌ها</p></div>
<div class="m2"><p>ز دل خواهد گل دولت دمیدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دل خواهی به دلبر راه بردن</p></div>
<div class="m2"><p>ز دل خواهی ز ننگ تن رهیدن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دل از بهر تو یک دیکی بپخته‌ست</p></div>
<div class="m2"><p>زمانی صبر می کن تا پزیدن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل دل‌هاست شمس الدین تبریز</p></div>
<div class="m2"><p>نتاند شمس را خفاش دیدن</p></div></div>