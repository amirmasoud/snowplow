---
title: >-
    غزل شمارهٔ ۲۲۲۸
---
# غزل شمارهٔ ۲۲۲۸

<div class="b" id="bn1"><div class="m1"><p>جان ما را هر نفس بستان نو</p></div>
<div class="m2"><p>گوش ما را هر نفس دستان نو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهیانیم اندر آن دریا که هست</p></div>
<div class="m2"><p>روز روزش گوهر و مرجان نو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا فسون هیچ کس را نشنوی</p></div>
<div class="m2"><p>این جهان کهنه را برهان نو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیش ما نقد است وآنگه نقد نو</p></div>
<div class="m2"><p>ذات ما کان است وآنگه کان نو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این شکر خور این شکر کز ذوق او</p></div>
<div class="m2"><p>می‌دهد اندر دهان دندان نو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله جان شو ار کسی پرسد تو را</p></div>
<div class="m2"><p>تو کیی گو هر زمانی جان نو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من زمین را لقمه‌ام لیکن زمین</p></div>
<div class="m2"><p>رویدش زین لقمه صد لقمان نو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زرد گشتی از خزان غمگین مشو</p></div>
<div class="m2"><p>در خزان بین تاب تابستان نو</p></div></div>