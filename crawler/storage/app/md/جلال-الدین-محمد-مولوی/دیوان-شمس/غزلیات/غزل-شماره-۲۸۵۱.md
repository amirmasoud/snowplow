---
title: >-
    غزل شمارهٔ ۲۸۵۱
---
# غزل شمارهٔ ۲۸۵۱

<div class="b" id="bn1"><div class="m1"><p>شب و روز آن نکوتر که به پیش یار باشی</p></div>
<div class="m2"><p>به میان سرو و سوسن گل خوش عذار باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به طرب هزار چندان که بوند عیش مندان</p></div>
<div class="m2"><p>به میان باغ خندان مثل انار باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشوی چو خارهایی که خلند دست و پا را</p></div>
<div class="m2"><p>به مثال نیشکرها که شکرنثار باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مثال آفتابی که شهیر شد به بخشش</p></div>
<div class="m2"><p>به میان پاکبازان به عطا مشار باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هله بس که تا شهنشه بگشاید و بگوید</p></div>
<div class="m2"><p>چو خمش کنی نگویی و در انتظار باشی</p></div></div>