---
title: >-
    غزل شمارهٔ ۲۵۵۹
---
# غزل شمارهٔ ۲۵۵۹

<div class="b" id="bn1"><div class="m1"><p>الا ای یوسف مصری از این دریای ظلمانی</p></div>
<div class="m2"><p>روان کن کشتی وصلت برای پیر کنعانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی کشتی که این دریا ز نور او بگیرد چشم</p></div>
<div class="m2"><p>که از شعشاع آن کشتی بگردد بحر نورانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه زان نوری که آن باشد به جان چاکران لایق</p></div>
<div class="m2"><p>از آن نوری که آن باشد جمال و فر سلطانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن بحر جلالت‌ها که آن کشتی همی‌گردد</p></div>
<div class="m2"><p>چو باشد عاشق او حق که باشد روح روحانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آن کشتی نماید رخ برآید گرد آن دریا</p></div>
<div class="m2"><p>نماند صعبیی دیگر بگردد جمله آسانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه آسانی که از شادی ز عاشق هر سر مویی</p></div>
<div class="m2"><p>در آن دریا به رقص اندرشده غلطان و خندانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبیند خنده جان را مگر که دیده جان‌ها</p></div>
<div class="m2"><p>نماید خدها در جسم آب و خاک ارکانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عریانی نشانی‌هاست بر درز لباس او</p></div>
<div class="m2"><p>ز چشم و گوش و فهم و وهم اگر خواهی تو برهانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو برهان را چه خواهی کرد که غرق عالم حسی</p></div>
<div class="m2"><p>برو می‌چر چو استوران در این مرعای شهوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر الطاف مخدومی خداوندی شمس دین</p></div>
<div class="m2"><p>رباید مر تو را چون باد از وسواس شیطانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کز این جمله اشارت‌ها هم از کشتی هم از دریا</p></div>
<div class="m2"><p>مکن فهمی مگر در حق آن دریای ربانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو این را فهم کردی تو سجودی بر سوی تبریز</p></div>
<div class="m2"><p>که تا او را بیابد جان ز رحمت‌های یزدانی</p></div></div>