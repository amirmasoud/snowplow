---
title: >-
    غزل شمارهٔ ۳۰۷۷
---
# غزل شمارهٔ ۳۰۷۷

<div class="b" id="bn1"><div class="m1"><p>ایا مربی جان از صداع جان چونی</p></div>
<div class="m2"><p>ایا ببرده دل از جمله دلبران چونی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز زحمت شب ما و ز ناله‌های صبوح</p></div>
<div class="m2"><p>که می‌رسد به تو ای ماه مهربان چونی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایا کسی که نخفت و نخفت چشم خوشت</p></div>
<div class="m2"><p>ز لکلک جرس و بانگ پاسبان چونی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایا غریب فلک تو بر این زمین حیفی</p></div>
<div class="m2"><p>ایا جهان ملاحت در این جهان چونی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آفتاب کی پرسد که چون همی‌گردی</p></div>
<div class="m2"><p>به گلستان که بگوید که گلستان چونی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روی زرد بپرسند درد دل چونست</p></div>
<div class="m2"><p>ولی کسی بنپرسد که ارغوان چونی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو روی زشت به آیینه گفت چونی تو</p></div>
<div class="m2"><p>بگفت من چو چراغم تو قلتبان چونی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواب گفت که من بازگونه می‌پرسم</p></div>
<div class="m2"><p>مثال کشت که گوید به آسمان چونی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهان گشادم یعنی ببین که لب خشکم</p></div>
<div class="m2"><p>که تا شراب تو گوید که ای دهان چونی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گفت چون تو جویی روان شود در حال</p></div>
<div class="m2"><p>میان جان و روانم که ای روان چونی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگو تو باقی این را که از خمار لبت</p></div>
<div class="m2"><p>سرم گران شد پرسش که سرگران چونی</p></div></div>