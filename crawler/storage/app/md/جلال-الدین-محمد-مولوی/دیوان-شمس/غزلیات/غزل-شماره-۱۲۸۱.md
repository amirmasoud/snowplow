---
title: >-
    غزل شمارهٔ ۱۲۸۱
---
# غزل شمارهٔ ۱۲۸۱

<div class="b" id="bn1"><div class="m1"><p>ز هدهدان تفکر چو دررسید نشانش</p></div>
<div class="m2"><p>مراست ملک سلیمان چو نقد گشت عیانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پری و دیو نداند ز تختگاه بلندش</p></div>
<div class="m2"><p>که تخت او نظرست و بصیرتست جهانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان جمله مرغان بداند او به بصیرت</p></div>
<div class="m2"><p>که هیچ مرغ نداند به وهم خویش زبانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشان سکه او بین به هر درست که نقدست</p></div>
<div class="m2"><p>ولیک نقد نیابی که بو بری سوی کانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر که حلقه رندان بی‌نشان تو ببینی</p></div>
<div class="m2"><p>که عشق پیش درآید درآورد به میانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تیر او بود آن دل که برپرید از آن سو</p></div>
<div class="m2"><p>وگر نه کیست ز مردان که او کشید کمانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که خورد شرابش ز دست ساقی عشقش</p></div>
<div class="m2"><p>همان شراب مقدم تو پر کن و برسانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آنک هیچ شرابی خمار او ننشاند</p></div>
<div class="m2"><p>دغل میار تو ساقی مده از این و از آنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شمس مفخر تبریز باده گشت وظیفه</p></div>
<div class="m2"><p>چگونه بنده نباشد به هر دمی دل و جانش</p></div></div>