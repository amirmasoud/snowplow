---
title: >-
    غزل شمارهٔ ۲۲
---
# غزل شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>چندان بنالم ناله‌ها چندان برآرم رنگ‌ها</p></div>
<div class="m2"><p>تا برکنم از آینه هر منکری من زنگ‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مرکب عشق تو دل می‌راند و این مرکبش</p></div>
<div class="m2"><p>در هر قدم می‌بگذرد زان سوی جان فرسنگ‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنما تو لعل روشنت بر کوری هر ظلمتی</p></div>
<div class="m2"><p>تا بر سر سنگین دلان از عرش بارد سنگ‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با این چنین تابانیت دانی چرا منکر شدند</p></div>
<div class="m2"><p>کاین دولت و اقبال را باشد از ایشان ننگ‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نی که کورندی چنین آخر بدیدندی چنان</p></div>
<div class="m2"><p>آن سو هزاران جان ز مه چون اختران آونگ‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون از نشاط نور تو کوران همی بینا شوند</p></div>
<div class="m2"><p>تا از خوشی راه تو رهوار گردد لنگ‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اما چو اندر راه تو ناگاه بیخود می‌شود</p></div>
<div class="m2"><p>هر عقل زیرا رسته شد در سبزه زارت بنگ‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین رو همی‌بینم کسان نالان چو نی وز دل تهی</p></div>
<div class="m2"><p>زین رو دو صد سرو روان خم شد ز غم چون چنگ‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین رو هزاران کاروان بشکسته شد از ره روان</p></div>
<div class="m2"><p>زین ره بسی کشتی پر بشکسته شد بر گنگ‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشکستگان را جان‌ها بستست بر اومید تو</p></div>
<div class="m2"><p>تا دانش بی‌حد تو پیدا کند فرهنگ‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا قهر را برهم زند آن لطف اندر لطف تو</p></div>
<div class="m2"><p>تا صلح گیرد هر طرف تا محو گردد جنگ‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا جستنی نوعی دگر ره رفتنی طرزی دگر</p></div>
<div class="m2"><p>پیدا شود در هر جگر در سلسله آهنگ‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وز دعوت جذب خوشی آن شمس تبریزی شود</p></div>
<div class="m2"><p>هر ذره انگیزنده‌ای هر موی چون سرهنگ‌ها</p></div></div>