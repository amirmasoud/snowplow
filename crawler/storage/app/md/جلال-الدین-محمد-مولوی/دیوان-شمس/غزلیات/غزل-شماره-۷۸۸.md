---
title: >-
    غزل شمارهٔ ۷۸۸
---
# غزل شمارهٔ ۷۸۸

<div class="b" id="bn1"><div class="m1"><p>آه کان طوطی دل بی‌شکرستان چه کند</p></div>
<div class="m2"><p>آه کان بلبل جان بی‌گل و بستان چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنک از نقد وصال تو به یک جو نرسید</p></div>
<div class="m2"><p>چو گه عرض بود بر سر میزان چه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنک بحر تو چو خاشاک به یک سوش افکند</p></div>
<div class="m2"><p>چو بجویند از او گوهر ایمان چه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش گرمابه ز گرمابه چه لذت یابد</p></div>
<div class="m2"><p>در تماشاگه جان صورت بی‌جان چه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با بد و نیک بد و نیک مرا کاری نیست</p></div>
<div class="m2"><p>دل تشنه لب من در شب هجران چه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست و پا و پر و بال دل من منتظرند</p></div>
<div class="m2"><p>تا که عشقش چه کند عشق جز احسان چه کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنک او دست ندارد چه برد روز نثار</p></div>
<div class="m2"><p>و آنک او پای ندارد گه خیزان چه کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنک بر پرده عشاق دلش زنگله نیست</p></div>
<div class="m2"><p>پرده زیر و عراقی و سپاهان چه کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنک از باده جان گوش و سرش گرم نشد</p></div>
<div class="m2"><p>سرد و افسرده میان صف مستان چه کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنک چون شیر نجست از صفت گرگی خویش</p></div>
<div class="m2"><p>چشم آهوفکن یوسف کنعان چه کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه فرعون به در ریش مرصع دارد</p></div>
<div class="m2"><p>او حدیث چو در موسی عمران چه کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنک او لقمه حرص است به طمع خامی</p></div>
<div class="m2"><p>او دم عیسی و یا حکمت لقمان چه کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس کن و جمع شو و بیش پراکنده مگو</p></div>
<div class="m2"><p>بی دل جمع دو سه حرف پریشان چه کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمس تبریز تویی صبح شکرریز تویی</p></div>
<div class="m2"><p>عاشق روز به شب قبله پنهان چه کند</p></div></div>