---
title: >-
    غزل شمارهٔ ۲۳۴۳
---
# غزل شمارهٔ ۲۳۴۳

<div class="b" id="bn1"><div class="m1"><p>ایا خورشید بر گردون سواره</p></div>
<div class="m2"><p>به حیله کرده خود را چون ستاره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی باشی چو دل اندر میانه</p></div>
<div class="m2"><p>گهی آیی نشینی بر کناره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی از دور دور استاده باشی</p></div>
<div class="m2"><p>که من مرد غریبم در نظاره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی چون چاره غم‌ها را بسوزی</p></div>
<div class="m2"><p>گهی گویی که این غم را چه چاره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو پاره می‌کنی و هم بدوزی</p></div>
<div class="m2"><p>که دل آن به که باشد پاره پاره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی دل را بگریانم چو طفلان</p></div>
<div class="m2"><p>مرا گویی بجنبان گاهواره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی بر گیریم چون دایگان تو</p></div>
<div class="m2"><p>گهی بر من نشینی چون سواره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی پیری نمایی گاه دومو</p></div>
<div class="m2"><p>زمانی کودک و گه شیرخواره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبونم یا زبونم تو گرفتی</p></div>
<div class="m2"><p>زهی عیار و چست و حیله باره</p></div></div>