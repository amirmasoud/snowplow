---
title: >-
    غزل شمارهٔ ۹۲۷
---
# غزل شمارهٔ ۹۲۷

<div class="b" id="bn1"><div class="m1"><p>به باغ بلبل از این پس حدیث ما گوید</p></div>
<div class="m2"><p>حدیث خوبی آن یار دلربا گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو باد در سر بید افتد و شود رقصان</p></div>
<div class="m2"><p>خدای داند کو با هوا چه‌ها گوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنار فهم کند اندکی ز سوز چمن</p></div>
<div class="m2"><p>دو دست پهن برآرد خوش و دعا گوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپرسم از گل کان حسن از که دزدیدی</p></div>
<div class="m2"><p>ز شرم سست بخندد ولی کجا گوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه مست بود گل خراب نیست چو من</p></div>
<div class="m2"><p>که راز نرگس مخمور با شما گوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو رازها طلبی در میان مستان رو</p></div>
<div class="m2"><p>که راز را سر سرمست بی‌حیا گوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که باده دختر کرمست و خاندان کرم</p></div>
<div class="m2"><p>دهان کیسه گشادست و از سخا گوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خصوص باده عرشی ز ذوالجلال کریم</p></div>
<div class="m2"><p>سخاوت و کرم آن مگر خدا گوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شیردانه عارف بجوشد آن شیره</p></div>
<div class="m2"><p>ز قعر خم تن او تو را صلا گوید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سینه شیر دهد شیره هم تواند داد</p></div>
<div class="m2"><p>ز سینه چشمه جاریش ماجرا گوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مستتر شود آن روح خرقه باز شود</p></div>
<div class="m2"><p>کلاه و سر بنهد ترک این قبا گوید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو خون عقل خورد باده لاابالی وار</p></div>
<div class="m2"><p>دهان گشاید و اسرار کبریا گوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خموش باش که کس باورت نخواهد کرد</p></div>
<div class="m2"><p>که مس بد نخورد آنچ کیمیا گوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خبر ببر سوی تبریز مفخر آفاق</p></div>
<div class="m2"><p>مگر که مدح تو را شمس دین ما گوید</p></div></div>