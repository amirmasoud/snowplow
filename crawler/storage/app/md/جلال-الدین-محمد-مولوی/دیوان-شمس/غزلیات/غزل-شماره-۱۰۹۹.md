---
title: >-
    غزل شمارهٔ ۱۰۹۹
---
# غزل شمارهٔ ۱۰۹۹

<div class="b" id="bn1"><div class="m1"><p>ای نهاده بر سر زانو تو سر</p></div>
<div class="m2"><p>وز درون جان جمله باخبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش چشمت سرکش روپوش نیست</p></div>
<div class="m2"><p>آفرین‌ها بر صفای آن بصر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحر خونست ای صنم آن چشم نیست</p></div>
<div class="m2"><p>الحذر ای دل ز زخم آن نظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مژه او گر چه دل را مژده‌هاست</p></div>
<div class="m2"><p>الحذر ای عاشقان از وی حذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او به زیر کاه آب خفته‌ست</p></div>
<div class="m2"><p>پا منه گستاخ ور نی رفت سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خفته شکلی اصل هر بیدادیی</p></div>
<div class="m2"><p>تا ز خوابش تو نخسپی ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاره خواهم کرد من جامه ز تو</p></div>
<div class="m2"><p>ای برادر پاره‌ای زین گرمتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرکه آشامی و گویی شهد کو</p></div>
<div class="m2"><p>دست تو در زهر و گویی کو شکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روح را عمریست صابون می‌زنی</p></div>
<div class="m2"><p>یا تو را خود جان نبودست ای مگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به کی صیقل زنی آیینه را</p></div>
<div class="m2"><p>شرم بادت آخر از آیینه گر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی بحر شمس تبریزی گریز</p></div>
<div class="m2"><p>تا برآرد ز آینه جانت گهر</p></div></div>