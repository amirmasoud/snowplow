---
title: >-
    غزل شمارهٔ ۳۹۶
---
# غزل شمارهٔ ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>در ره معشوق ما ترسندگان را کار نیست</p></div>
<div class="m2"><p>جمله شاهانند آن جا بندگان را بار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو نازی می‌کنی یعنی که من فرخنده‌ام</p></div>
<div class="m2"><p>نزد این اقبال ما فرخندگی جز عار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به فقرت ناز باشد ژنده برگیر و برو</p></div>
<div class="m2"><p>نزد این سلطان ما آن جمله جز زنار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو نور حق شدی از شرق تا مغرب برو</p></div>
<div class="m2"><p>زانک ما را زین صفت پروای آن انوار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو سر حق بدانستی برو با سر باش</p></div>
<div class="m2"><p>زانک این اسرار ما را خوی آن اسرار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست شو در راه ما وین مکر را یک سوی نه</p></div>
<div class="m2"><p>زان که این میدان ما جولانگه مکار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس دین و شمس دین آن جان ما اینک بدان</p></div>
<div class="m2"><p>جز به سوی راه تبریز اسب ما رهوار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مست بودم فاش کردم سر خود با یارکان</p></div>
<div class="m2"><p>زانک هشیاری مرا خود مذهب آزار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نهی پرگار بر تن تا بدانی حد ما</p></div>
<div class="m2"><p>حد ما خود ای برادر لایق پرگار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاک پاشی می‌کنی تو ای صنم در راه ما</p></div>
<div class="m2"><p>خاک پاشی دو عالم پیش ما در کار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صوفیان عشق را خود خانقاهی دیگر است</p></div>
<div class="m2"><p>جان ما را اندر آن جا کاسه و ادرار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در تک دوزخ نشستم ترک کردم بخت را</p></div>
<div class="m2"><p>زانک ما را اشتهای جنت و ابرار نیست</p></div></div>