---
title: >-
    غزل شمارهٔ ۱۳۵۹
---
# غزل شمارهٔ ۱۳۵۹

<div class="b" id="bn1"><div class="m1"><p>ز خود شدم ز جمال پر از صفا ای دل</p></div>
<div class="m2"><p>بگفتمش که زهی خوبی خدا ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام تست هزار آفتاب و چشم و چراغ</p></div>
<div class="m2"><p>ز پرتو تو ظلالست جان‌ها ای دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهایتیست که خوبی از آن گذر نکند</p></div>
<div class="m2"><p>گذشت حسن تو از حد و منتها ای دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پری و دیو به پیش تو بسته‌اند کمر</p></div>
<div class="m2"><p>ملک سجود کند و اختر و سما ای دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کدام دل که بر او داغ بندگی تو نیست</p></div>
<div class="m2"><p>کدام داغ غمی کش نه‌ای دوا ای دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به حکم تست همه گنج‌های لم یزلی</p></div>
<div class="m2"><p>چه گنج‌ها که نداری تو در فنا ای دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر ز سوختگان وامگیر کز نظرت</p></div>
<div class="m2"><p>چه کوثرست و دوا دفع سوز را ای دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتم این مه ماند به شمس تبریزی</p></div>
<div class="m2"><p>بگفت دل که کجایست تا کجا ای دل</p></div></div>