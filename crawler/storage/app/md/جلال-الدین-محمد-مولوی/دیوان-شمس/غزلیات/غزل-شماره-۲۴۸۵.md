---
title: >-
    غزل شمارهٔ ۲۴۸۵
---
# غزل شمارهٔ ۲۴۸۵

<div class="b" id="bn1"><div class="m1"><p>یاور من تویی بکن بهر خدای یاریی</p></div>
<div class="m2"><p>نیست تو را ضعیفتر از دل من شکاریی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نای برای من کند در شب و روز ناله‌ای</p></div>
<div class="m2"><p>چنگ برای من کند با غم و سوز زاریی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی بفشاردی مرا دست غمی و غصه‌ای</p></div>
<div class="m2"><p>گر تو مرا به عاطفت در بر خود فشاریی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده همچو ابر من اشک روان نباردی</p></div>
<div class="m2"><p>گر تو ز ابر مرحمت بر سر من بباریی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست دراز کردمی‌گوش فلک گرفتمی</p></div>
<div class="m2"><p>گر سر زلف خویش را تو به کفم سپاریی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر ماه من کله بستدمی ربودمی</p></div>
<div class="m2"><p>گر تو شبی به لطف خود خوش سر من بخاریی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حق حقوق سابقت حق نیاز عاشقت</p></div>
<div class="m2"><p>حق زروع جان من کش تو کنی بهاریی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حق نسیم بوی تو کان رسدم ز کوی تو</p></div>
<div class="m2"><p>حق شعاع روی تو کو کندم نهاریی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که نثار کرده‌ای از گل وصل بر سرم</p></div>
<div class="m2"><p>بر کف پای کوششم خار نکرد خاریی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دارد از تو جزو و کل خرمیی و شادیی</p></div>
<div class="m2"><p>وز رخ تو درخت گل خجلت و شرمساریی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای لب من خموش کن سوی اصول گوش کن</p></div>
<div class="m2"><p>تا کند او به نطق خود نادره غمگساریی</p></div></div>