---
title: >-
    غزل شمارهٔ ۲۵۸۰
---
# غزل شمارهٔ ۲۵۸۰

<div class="b" id="bn1"><div class="m1"><p>ای خیره نظر در جو پیش آ و بخور آبی</p></div>
<div class="m2"><p>بیهوده چه می‌گردی بر آب چو دولابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحراست پر از شکر دریاست پر از گوهر</p></div>
<div class="m2"><p>یک جو نبری زین دو بی‌کوشش و اسبابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مرد تماشایی چون دیده بنگشایی</p></div>
<div class="m2"><p>بگشادن چشم ارزد تا بانی مهتابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محراب بسی دیدی در وی بنگنجیدی</p></div>
<div class="m2"><p>اندر نظر حربی بشکافد محرابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما تشنه و هر جانب یک چشمه حیوانی</p></div>
<div class="m2"><p>ما طامع و پیش و پس دریا کف وهابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ره چیست میان ما جز نقص عیان ما</p></div>
<div class="m2"><p>کو پرده میان ما جز چشم گران خوابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شش نور همی‌بارد زان ابر که حق آرد</p></div>
<div class="m2"><p>جسمت مثل بامی هر حس تو میزانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شش چشمه پیوسته می‌گردد شب بسته</p></div>
<div class="m2"><p>زان سوش روان کرده آن فاتح ابوابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید و قمر گاهی شب افتد در چاهی</p></div>
<div class="m2"><p>بیرون کشدش زان چه بی‌آلت و قلابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد صنعت سلطانی دارد ز تو پنهانی</p></div>
<div class="m2"><p>زیرا که ضعیفی تو بی‌طاقت و بی‌تابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این مفرش و آن کیوان افلاک ورای آن</p></div>
<div class="m2"><p>بر کف خدا لرزان ماننده سیمابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دریا چو چنان باشد کف درخور آن باشد</p></div>
<div class="m2"><p>اندر صفتش خاطر هست احول و کذابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگریزد عقل و جان از هیبت آن سلطان</p></div>
<div class="m2"><p>چون دیو که بگریزد از عمر خطابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بکری برمد از شو معشوق جهانش او</p></div>
<div class="m2"><p>از جان عزیز خود بیگانه و صخابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ره داده به دام خود صد زاغ پی بازی</p></div>
<div class="m2"><p>چون باز به دام آمد برداشته مضرابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خاموش که آن اسعد این را به از این گوید</p></div>
<div class="m2"><p>بی‌صفقه صفاقی بی‌شرفه دبابی</p></div></div>