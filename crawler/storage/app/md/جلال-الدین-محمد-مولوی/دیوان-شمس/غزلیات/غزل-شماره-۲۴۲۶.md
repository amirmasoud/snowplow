---
title: >-
    غزل شمارهٔ ۲۴۲۶
---
# غزل شمارهٔ ۲۴۲۶

<div class="b" id="bn1"><div class="m1"><p>فدیتتک یا ستی الناسیه</p></div>
<div class="m2"><p>الی کم تشد فم الخابیه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا فاملئی منه لی کاسه</p></div>
<div class="m2"><p>تذکرنی صفوه ناسیه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فما کاسه منه الا نجی</p></div>
<div class="m2"><p>و تأتی باخت لها آبیه</p></div></div>