---
title: >-
    غزل شمارهٔ ۲۴۴۲
---
# غزل شمارهٔ ۲۴۴۲

<div class="b" id="bn1"><div class="m1"><p>بانکی عجب از آسمان در می‌رسد هر ساعتی</p></div>
<div class="m2"><p>می‌نشنود آن بانگ را الا که صاحب حالتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سر فروبرده چو خر زین آب و سبزه بس مچر</p></div>
<div class="m2"><p>یک لحظه‌ای بالا نگر تا بوک بینی آیتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی در این آخرزمان بگشاد خم آسمان</p></div>
<div class="m2"><p>از روح او را لشکری وز راح او را رایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو شیرمردی در جهان تا شیرگیر او شود</p></div>
<div class="m2"><p>شاه و فتی باید شدن تا باده نوشی یا فتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیچاره گوش مشترک کو نشنود بانگ فلک</p></div>
<div class="m2"><p>بیچاره جان بی‌مزه کز حق ندارد راحتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر چه باشد گر شبی از جان برآری یاربی</p></div>
<div class="m2"><p>بیرون جهی از گور تن و اندرروی در ساحتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پا گشایی ریسمان تا برپری بر آسمان</p></div>
<div class="m2"><p>چون آسمان ایمن شوی از هر شکست و آفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جان برآری یک سری ایمن ز شمشیر اجل</p></div>
<div class="m2"><p>باغی درآیی کاندر او نبود خزان را غارتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خامش کنم خامش کنم تا عشق گوید شرح خود</p></div>
<div class="m2"><p>شرحی خوشی جان پروری کان را نباشد غایتی</p></div></div>