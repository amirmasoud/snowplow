---
title: >-
    غزل شمارهٔ ۲۰۷۸
---
# غزل شمارهٔ ۲۰۷۸

<div class="b" id="bn1"><div class="m1"><p>چهار روز ببودم به پیش تو مهمان</p></div>
<div class="m2"><p>سه روز دیگر خواهم بدن یقین می‌دان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حق این سه و آن چار رو ترش نکنی</p></div>
<div class="m2"><p>که تا نیفتد این دل به صد هزار گمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر طعام خوشم من جز این یکی ترشی</p></div>
<div class="m2"><p>که سخت این ترشی کند می‌کند دندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که جمله ترشی‌ها بدان گوار شود</p></div>
<div class="m2"><p>که تو ترش نکنی روی ای گل خندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشای آن لب خندان که آن گوارش ماست</p></div>
<div class="m2"><p>که تعبیه‌ست دو صد گلشکر در آن احسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترش مکن که نخواهد ترش شدن آن رو</p></div>
<div class="m2"><p>که می‌دهد مدد قند هر دمش رحمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه جای این که اگر صد هزار تلخ و ترش</p></div>
<div class="m2"><p>به نزد روی تو افتد شود خوش و شادان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر به روز قیامت نهان شود رویت</p></div>
<div class="m2"><p>وگر نه دوزخ خوشتر شود ز صدر جنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر میان زمستان بهار نو خواهی</p></div>
<div class="m2"><p>درآ به باغ جمالت درخت‌ها بفشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به روز جمعه چو خواهی که عیدها بینند</p></div>
<div class="m2"><p>برآی بر سر منبر صفات خود برخوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غلط شدم که تو گر برروی به منبر بر</p></div>
<div class="m2"><p>پری برآرد منبر چو دل شود پران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا به قند و شکرهای خویش مهمان کن</p></div>
<div class="m2"><p>علف میاور پیشم منه نیم حیوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرشته از چه خورد از جمال حضرت حق</p></div>
<div class="m2"><p>غذای ماه و ستاره ز آفتاب جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غذای خلق در آن قحط حسن یوسف بود</p></div>
<div class="m2"><p>که اهل مصر رهیده بدند از غم نان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خمش کنم که دگربار یار می‌خواهد</p></div>
<div class="m2"><p>که درروم به سخن او برون جهد ز میان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غلط که او چو بخواهد که از خرم فکند</p></div>
<div class="m2"><p>حذر چه سود کند یا گرفتن پالان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر همو بنماید ره حذر کردن</p></div>
<div class="m2"><p>همو بدوزد انبان همو درد انبان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا سخن همه با او است گر چه در ظاهر</p></div>
<div class="m2"><p>عتاب و صلح کنم گرم با فلان و فلان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خمش که تا نزند بر چنین حدیث هوا</p></div>
<div class="m2"><p>از آنک باد هوا نیست محرم ایشان</p></div></div>