---
title: >-
    غزل شمارهٔ ۹۹۱
---
# غزل شمارهٔ ۹۹۱

<div class="b" id="bn1"><div class="m1"><p>عشق جانان مرا ز جان ببرید</p></div>
<div class="m2"><p>جان به عشق اندرون ز خود برهید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانک جان محدثست و عشق قدیم</p></div>
<div class="m2"><p>هرگز این در وجود آن نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق جانان چو سنگ مغناطیس</p></div>
<div class="m2"><p>جان ما را به قرب خویش کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز جان را ز خویشتن گم کرد</p></div>
<div class="m2"><p>جان چو گم شد وجود خویش بدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از آن باز با خود آمد جان</p></div>
<div class="m2"><p>دام عشق آمد و در او پیچید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شربتی دادش از حقیقت عشق</p></div>
<div class="m2"><p>جمله اخلاص‌ها از او برمید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این نشان بدایت عشق است</p></div>
<div class="m2"><p>هیچ کس در نهایتش نرسید</p></div></div>