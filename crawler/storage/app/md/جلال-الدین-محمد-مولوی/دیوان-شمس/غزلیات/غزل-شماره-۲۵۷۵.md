---
title: >-
    غزل شمارهٔ ۲۵۷۵
---
# غزل شمارهٔ ۲۵۷۵

<div class="b" id="bn1"><div class="m1"><p>هر لحظه یکی صورت می‌بینی و زادن نی</p></div>
<div class="m2"><p>جز دیده فزودن نی جز چشم گشودن نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نعمت روحانی در مجلس پنهانی</p></div>
<div class="m2"><p>چندانک خوری می خور دستوری دادن نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن میوه که از لطفش می آب شود در کف</p></div>
<div class="m2"><p>و آن میوه نورش را بر کف به نهان نی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این بوی که از زلف آن ترک خطا آمد</p></div>
<div class="m2"><p>در مشک تتاری نی در عنبر و لادن نی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌کوبد تقدیرش در هاون تن جان را</p></div>
<div class="m2"><p>وین سرمه عشق او اندرخور هاون نی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدی تو چنین سرمه کو هاون‌ها ساید</p></div>
<div class="m2"><p>تا بازرود آن جا آن جا که تو و من نی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن جا روش و دین نی جز باغ نوآیین نی</p></div>
<div class="m2"><p>جز گلبن و نسرین نی جز لاله و سوسن نی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذار تنی‌ها را بشنو ارنی‌ها را</p></div>
<div class="m2"><p>چون سوخت منی‌ها را پس طعنه گه لن نی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن را تو مبر سوی شمس الحق تبریزی</p></div>
<div class="m2"><p>کز غلبه جان آن جا جای سر سوزن نی</p></div></div>