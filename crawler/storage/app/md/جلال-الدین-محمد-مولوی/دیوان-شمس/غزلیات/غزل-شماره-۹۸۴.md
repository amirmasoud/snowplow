---
title: >-
    غزل شمارهٔ ۹۸۴
---
# غزل شمارهٔ ۹۸۴

<div class="b" id="bn1"><div class="m1"><p>هین که هنگام صابران آمد</p></div>
<div class="m2"><p>وقت سختی و امتحان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چنین وقت عهدها شکنند</p></div>
<div class="m2"><p>کارد چون سوی استخوان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهد و سوگند سخت سست شود</p></div>
<div class="m2"><p>مرد را کار چون به جان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هله ای دل تو خویش سست مکن</p></div>
<div class="m2"><p>دل قوی کن که وقت آن آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زر سرخ اندر آتش خند</p></div>
<div class="m2"><p>تا بگویند زر کان آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم خوش رو به پیش تیغ اجل</p></div>
<div class="m2"><p>بانگ برزن که پهلوان آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خدا باش و نصرت از وی خواه</p></div>
<div class="m2"><p>که مددها ز آسمان آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خدا آستین فضل فشان</p></div>
<div class="m2"><p>چونک بنده بر آستان آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون صدف ما دهان گشادستیم</p></div>
<div class="m2"><p>کابر فضل تو درفشان آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای بسا خار خشک کز دل او</p></div>
<div class="m2"><p>در پناه تو گلستان آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نشان کرده‌ام تو را که ز تو</p></div>
<div class="m2"><p>دلخوشی‌های بی‌نشان آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقت رحمست و وقت عاطفت است</p></div>
<div class="m2"><p>که مرا زخم بس گران آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ابابیل هین که بر کعبه</p></div>
<div class="m2"><p>لشکر و پیل بی‌کران آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل گوید مرا خمش کن بس</p></div>
<div class="m2"><p>که خداوند غیب دان آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من خمش کردم ای خدا لیکن</p></div>
<div class="m2"><p>بی من از خان من فغان آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما رمیت اذ رمیت هم ز خداست</p></div>
<div class="m2"><p>تیر ناگه کز این کمان آمد</p></div></div>