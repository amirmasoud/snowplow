---
title: >-
    غزل شمارهٔ ۲۱۰۳
---
# غزل شمارهٔ ۲۱۰۳

<div class="b" id="bn1"><div class="m1"><p>گر چه اندر فغان و نالیدن</p></div>
<div class="m2"><p>اندکی هست خویشتن دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نباشد مرا چو در عشقت</p></div>
<div class="m2"><p>خوگرم من به خویش دزدیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خدا و به پاکی ذاتش</p></div>
<div class="m2"><p>پاکم از خویشتن پسندیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده کی از رخ تو برگردد</p></div>
<div class="m2"><p>به که آید به وقت گردیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چنین دولت و چنین میدان</p></div>
<div class="m2"><p>ننگ باشد ز مرگ لنگیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان تو را مسلم شد</p></div>
<div class="m2"><p>بر همه مرگ‌ها بخندیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرع‌های درخت لرزانند</p></div>
<div class="m2"><p>اصل را نیست خوف لرزیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغبانان عشق را باشد</p></div>
<div class="m2"><p>از دل خویش میوه برچیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان عاشق نواله‌ها می‌پیچ</p></div>
<div class="m2"><p>در مکافات رنج پیچیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهد و دانش بورز ای خواجه</p></div>
<div class="m2"><p>نتوان عشق را بورزیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش از این گفت شمس تبریزی</p></div>
<div class="m2"><p>لیک کو گوش بهر بشنیدن</p></div></div>