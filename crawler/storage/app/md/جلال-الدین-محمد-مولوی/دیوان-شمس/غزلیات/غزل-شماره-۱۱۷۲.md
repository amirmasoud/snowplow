---
title: >-
    غزل شمارهٔ ۱۱۷۲
---
# غزل شمارهٔ ۱۱۷۲

<div class="b" id="bn1"><div class="m1"><p>جاء الربیع و البطر زال الشتاء و الخطر</p></div>
<div class="m2"><p>من فضل رب عنده کل الخطایا تغتفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد ترش رویی دگر یا زمهریرست او مگر</p></div>
<div class="m2"><p>برریز جامی بر سرش ای ساقی همچون شکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوحی الیکم ربکم انا غفرنا ذنبکم</p></div>
<div class="m2"><p>و ارضوا بما یقضی لکم ان الرضا خیر السیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا می دهش از بلبله یا خود به راهش کن هله</p></div>
<div class="m2"><p>زیرا میان گلرخان خوش نیست عفریت ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و قایل یقول لی انا علمنا بره</p></div>
<div class="m2"><p>فاحک لدینا سره لا تشتغل فیما اشتهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درده می بیغامبری تا خر نماند در خری</p></div>
<div class="m2"><p>خر را بروید در زمان از باده عیسی دو پر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>السر فیک یا فتی لا تلتمس فیما اتی</p></div>
<div class="m2"><p>من لیس سر عنده لم ینتفع مما ظهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مجلس مستان دل هشیار اگر آید مهل</p></div>
<div class="m2"><p>دانی که مستان را بود در حال مستی خیر و شر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انظر الی اهل الردی کم عاینوا نور الهدی</p></div>
<div class="m2"><p>لم ترتفع استارهم من بعد ما انشق القمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای پاسبان بر در نشین در مجلس ما ره مده</p></div>
<div class="m2"><p>جز عاشقی آتش دلی کید از او بوی جگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا ربنا رب المنن ان انت لم ترحم فمن</p></div>
<div class="m2"><p>منک الهدی منک الردی ما غیر ذا الا غرر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز عاشقی عاشق کنی مستی لطیفی روشنی</p></div>
<div class="m2"><p>نشناسد از مستی خود او سرکله را از کمر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا شوق این العافیه کی اضطفر بالقافیه</p></div>
<div class="m2"><p>عندی صفات صافیه فی جنبها نطقی کدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر دست خواهی پا نهد ور پای خواهی سر نهد</p></div>
<div class="m2"><p>ور بیل خواهی عاریت بر جای بیل آرد تبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ان کان نطقی مدرسی قد ظل عشقی مخرسی</p></div>
<div class="m2"><p>و العشق قرن غالب فینا و سلطان الظفر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای خواجه من آغشته‌ام بی‌شرم و بی‌دل گشته‌ام</p></div>
<div class="m2"><p>اسپر سلامت نیستم در پیش تیغم چون سپر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر کتیم لفظه سیف حسیم لحظه</p></div>
<div class="m2"><p>شمس الضحی لا تختفی الا بسحار سحر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواهم یکی گوینده‌ای مستی خرابی زنده‌ای</p></div>
<div class="m2"><p>کآتش به خواب اندرزند وین پرده گوید تا سحر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا ساحراء ابصارنا بالغت فی اسحارنا</p></div>
<div class="m2"><p>فارفق بنا اودارنا انا حبسنا فی السفر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندر تن من گر رگی هشیار یابی بردرش</p></div>
<div class="m2"><p>چون شیرگیر او نشد او را در این ره سگ شمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یا قوم موسی اننا فی التیه تهنا مثلکم</p></div>
<div class="m2"><p>کیف اهتدیتم فاخبروا لا تکتموا عنا الخبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن‌ها خراب و مست و خوش وین‌ها غلام پنج و شش</p></div>
<div class="m2"><p>آن‌ها جدا وین‌ها جدا آن‌ها دگر وین‌ها دگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ان عوقوا ترحالنا فالمن و السلوی لنا</p></div>
<div class="m2"><p>اصلحت ربی بالنا طاب السفر طاب الحضر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتن همه جنگ آورد در بوی و در رنگ آورد</p></div>
<div class="m2"><p>چون رافضی جنگ افکند هر دم علی را با عمر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اسکت و لا تکثر اخی ان طلت تکثر ترتخی</p></div>
<div class="m2"><p>الحیل فی ریح الهوی فاحفظه کلا لا وزر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خامش کن و کوتاه کن نظاره آن ماه کن</p></div>
<div class="m2"><p>آن مه که چون بر ماه زد از نورش انشق القمر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ان الهوی قد غرنا من بعد ما قد سرنا</p></div>
<div class="m2"><p>فاکشف به لطف ضرنا قال النبی لا ضرر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای میر مه روپوش کن ای جان عاشق جوش کن</p></div>
<div class="m2"><p>ما را چو خود بی‌هوش کن بی‌هوش خوش در ما نگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قالوا ندبر شأنکم نفتح لکم آذانکم</p></div>
<div class="m2"><p>نرفع لکم ارکانکم انتم مصابیح البشر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز اندازه بیرون خورده‌ام کاندازه را گم کرده‌ام</p></div>
<div class="m2"><p>شدوا یدی شدوا فمی هذا دواء من سکر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هاکم معاریج اللقا فیها تداریج البقا</p></div>
<div class="m2"><p>انعم به من مستقی اکرم به من مستقر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هین نیش ما را نوش کن افغان ما را گوش کن</p></div>
<div class="m2"><p>ما را چو خود بی‌هوش کن بی‌هوش سوی ما نگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>العیش حقا عیشکم و الموت حقا موتکم</p></div>
<div class="m2"><p>و الدین و الدنیا لکم هذا جزاء من شکر</p></div></div>