---
title: >-
    غزل شمارهٔ ۱۳۸۰
---
# غزل شمارهٔ ۱۳۸۰

<div class="b" id="bn1"><div class="m1"><p>دی بر سرم تاج زری بنهاده است آن دلبرم</p></div>
<div class="m2"><p>چندانک سیلی می زنی آن می نیفتد از سرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه کله دوز ابد بر فرق من از فرق خود</p></div>
<div class="m2"><p>شب پوش عشق خود نهد پاینده باشد لاجرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور سر نماند با کله من سر شوم جمله چو مه</p></div>
<div class="m2"><p>زیرا که بی‌حقه و صدف رخشانتر آید گوهرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینک سر و گرز گران می زن برای امتحان</p></div>
<div class="m2"><p>ور بشکند این استخوان از عقل و جان مغزینترم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن جوز بی‌مغزی بود کو پوست بگزیده بود</p></div>
<div class="m2"><p>او ذوق کی دیده بود از لوزی پیغامبرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لوزینه پرجوز او پرشکر و پرلوز او</p></div>
<div class="m2"><p>شیرین کند حلق و لبم نوری نهد در منظرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون مغز یابی ای پسر از پوست برداری نظر</p></div>
<div class="m2"><p>در کوی عیسی آمدی دیگر نگویی کو خرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جان من تا کی گله یک خر تو کم گیر از گله</p></div>
<div class="m2"><p>در زفتی فارس نگر نی بارگیر لاغرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زفتی عاشق را بدان از زفتی معشوق او</p></div>
<div class="m2"><p>زیرا که کبر عاشقان خیزد ز الله اکبرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دردهای آه گو اه اه مگو الله گو</p></div>
<div class="m2"><p>از چه مگو از جان گو ای یوسف جان پرورم</p></div></div>