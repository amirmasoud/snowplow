---
title: >-
    غزل شمارهٔ ۳۷۱
---
# غزل شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>گر جام سپهر زهرپیماست</p></div>
<div class="m2"><p>آن در لب عاشقان چو حلواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین واقعه گر ز جای رفتی</p></div>
<div class="m2"><p>از جای برو که جای این جاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگریز ز سوز عشق زیرا</p></div>
<div class="m2"><p>جز آتش عشق دود و سوداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دودت نپزد کند سیاهت</p></div>
<div class="m2"><p>در پختنت آتشست کاستاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانه که گرد دود گردد</p></div>
<div class="m2"><p>دودآلودست و خام و رسواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خانه و مان به یاد ناید</p></div>
<div class="m2"><p>آن را که چنین سفر مهیاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شهر مگو که در بیابان</p></div>
<div class="m2"><p>موسیست رفیق من و سلواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبت چه کنی که در سقیمی</p></div>
<div class="m2"><p>هر لحظه طبیب تو مسیحاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلتنگ خوشم که در فراخی</p></div>
<div class="m2"><p>هر مسخره را رهست و گنجاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خانه دل ز غم شود تنگ</p></div>
<div class="m2"><p>در وی شه دلنواز تنهاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل تنگ بود جز او نگنجد</p></div>
<div class="m2"><p>تنگی دلم امان و غوغاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دندان عدو ز ترس کندست</p></div>
<div class="m2"><p>پس روترشی رهایی ماست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاموش که بحر اگر ترش روست</p></div>
<div class="m2"><p>هم معدن گوهرست و دریاست</p></div></div>