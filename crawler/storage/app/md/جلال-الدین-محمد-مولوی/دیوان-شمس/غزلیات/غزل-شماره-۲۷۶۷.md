---
title: >-
    غزل شمارهٔ ۲۷۶۷
---
# غزل شمارهٔ ۲۷۶۷

<div class="b" id="bn1"><div class="m1"><p>گر یار لطیف و باوفایی</p></div>
<div class="m2"><p>ور از دل و جان از آن مایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که در این میان درآیی</p></div>
<div class="m2"><p>ای ماه بگو که کی برآیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صورت جان لطیف کاری</p></div>
<div class="m2"><p>از حلقه چرا تو برکناری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز یارک خود دریغ داری</p></div>
<div class="m2"><p>ای ماه بگو که کی برآیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخیز که ما و تو چو جانیم</p></div>
<div class="m2"><p>وز رازک همدگر بدانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر نه من و تو یارکانیم</p></div>
<div class="m2"><p>ای ماه بگو که کی برآیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریاب که بر در خداییم</p></div>
<div class="m2"><p>آخر بنگر که ما کجاییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا رقص کنان ز در درآییم</p></div>
<div class="m2"><p>ای ماه بگو که کی برآیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جان و جهان چرا چنینی</p></div>
<div class="m2"><p>چون یارک خویش را نبینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گوشه روی ترش نشینی</p></div>
<div class="m2"><p>ای ماه بگو که کی برآیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونی تو و آن دل لطیفت</p></div>
<div class="m2"><p>و آن صورت و قامت ظریفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواهم که شوم شبی حریفت</p></div>
<div class="m2"><p>ای ماه بگو که کی برآیی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در جمله عالم الهی</p></div>
<div class="m2"><p>وز دامن ماه تا به ماهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن شد که تو گویی و بخواهی</p></div>
<div class="m2"><p>ای ماه بگو که کی برآیی</p></div></div>