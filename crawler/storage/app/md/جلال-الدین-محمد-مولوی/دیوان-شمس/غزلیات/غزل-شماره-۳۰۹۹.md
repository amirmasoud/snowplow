---
title: >-
    غزل شمارهٔ ۳۰۹۹
---
# غزل شمارهٔ ۳۰۹۹

<div class="b" id="bn1"><div class="m1"><p>بداد پندم استاد عشق از استادی</p></div>
<div class="m2"><p>که هین بترس ز هر کس که دل بدو دادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آن کسی که تو از نوش او بنوشیدی</p></div>
<div class="m2"><p>ز بعد نوش کند نیش اوت فصادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو چشم مست کسی کرد حلقه در گوشت</p></div>
<div class="m2"><p>ز گوش پنبه برون کن مجوی آزادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر این بنه دل خود را چو دخل خنده رسید</p></div>
<div class="m2"><p>که غم نجوید عشرت ز خرمن شادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر زمین مسلم دهد تو را سلطان</p></div>
<div class="m2"><p>چنانک داد به بشر و جنید بغدادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو طوق موهبت آمد شکست گردن غم</p></div>
<div class="m2"><p>رسید داد خدا و بمرد بیدادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر کجا که روی ماه بر تو می‌تابد</p></div>
<div class="m2"><p>مهست نورفشان بر خراب و آبادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلام ماه شدی شب تو را به از روزست</p></div>
<div class="m2"><p>که پشتدار تو باشد میان هر وادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خنک تو را و خنک جمله همرهان تو را</p></div>
<div class="m2"><p>که سعد اکبری و نیکبخت افتادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به وعده‌های خوشش اعتماد کن ای جان</p></div>
<div class="m2"><p>که شاه مثل ندارد به راست میعادی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گوش تو همه تفسیر این بگوید شاه</p></div>
<div class="m2"><p>چنانک اشتر خود را نوا زند حادی</p></div></div>