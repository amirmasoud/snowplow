---
title: >-
    غزل شمارهٔ ۲۶۹۹
---
# غزل شمارهٔ ۲۶۹۹

<div class="b" id="bn1"><div class="m1"><p>ندارد مجلس ما بی‌تو نوری</p></div>
<div class="m2"><p>که مجلس بی‌تو باشد همچو گوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیایی یا بدان سومان بخوانی</p></div>
<div class="m2"><p>ز فضلت این کرامت نیست دوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلایق همچو کشت و تو بهاری</p></div>
<div class="m2"><p>به تو یابد شقایقشان ظهوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تجلی کن که تا سرمست گردند</p></div>
<div class="m2"><p>کنند اجزای عالم مست شوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دریای عتاب تو بجوشد</p></div>
<div class="m2"><p>برآید موج طوفان از تنوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گردون قبول تو بگردد</p></div>
<div class="m2"><p>شود جمله مصیبت‌ها سروری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خمش بگذار این شیشه گری را</p></div>
<div class="m2"><p>مبادا که زند بر شیشه کوری</p></div></div>