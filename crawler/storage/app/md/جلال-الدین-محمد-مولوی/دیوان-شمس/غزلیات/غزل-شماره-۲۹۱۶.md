---
title: >-
    غزل شمارهٔ ۲۹۱۶
---
# غزل شمارهٔ ۲۹۱۶

<div class="b" id="bn1"><div class="m1"><p>مرحبا ای پرده تو آن پرده‌ای</p></div>
<div class="m2"><p>کز جهان جان نشان آورده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگذر از گوش و بر جان‌ها بزن</p></div>
<div class="m2"><p>ز آنک جان این جهان مرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درربا جان را و بر بالا برو</p></div>
<div class="m2"><p>اندر آن عالم که دل را برده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه خندانت گواهی می‌دهد</p></div>
<div class="m2"><p>کان شراب آسمانی خورده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان شیرینت نشانی می‌دهد</p></div>
<div class="m2"><p>کز الست اندر عسل پرورده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزه‌ها از خاک بررستن گرفت</p></div>
<div class="m2"><p>تا نماید کشت‌ها که کرده‌ای</p></div></div>