---
title: >-
    غزل شمارهٔ ۲۷۸۳
---
# غزل شمارهٔ ۲۷۸۳

<div class="b" id="bn1"><div class="m1"><p>ای نرفته از دل من اندرآ شاد آمدی</p></div>
<div class="m2"><p>ای تو شمع شب فروزی مرحبا شاد آمدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانقاه روحیان را از تو حلو و حمزه‌ها</p></div>
<div class="m2"><p>جان جان صوفیانی الصلا شاد آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب چو چتر و مه چو سلطان می‌دود در زیر چتر</p></div>
<div class="m2"><p>وز تو تخت و تاج ما و چتر ما شاد آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی گهان در پیش کردی روح‌های پاک را</p></div>
<div class="m2"><p>ای صحابه عشق را چون مصطفی شاد آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که آن رحمت نمودی از پی چندین فراق</p></div>
<div class="m2"><p>می‌نگنجم زین طرب در هیچ جا شاد آمدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من گمان‌ها داشتم اندر وفای لطف تو</p></div>
<div class="m2"><p>لیک در وهمم نیامد این وفا شاد آمدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده داری کن تو ای شب کان مه اندر خلوت است</p></div>
<div class="m2"><p>مطربا پیوند کن تو پرده‌ها شاد آمدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون به نزد پرده دار شمس تبریزی رسی</p></div>
<div class="m2"><p>بشنوی از شش جهت کای خوش لقا شاد آمدی</p></div></div>