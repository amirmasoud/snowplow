---
title: >-
    غزل شمارهٔ ۴۱۴
---
# غزل شمارهٔ ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>روز و شب خدمت تو بی‌سر و بی‌پا چه خوشست</p></div>
<div class="m2"><p>در شکرخانه تو مرغ شکرخا چه خوشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر غنچه بسته که نهان می‌خندد</p></div>
<div class="m2"><p>سایه سرو خوش نادره بالا چه خوشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاغ اگر عاشق سرگین خر آمد گو باش</p></div>
<div class="m2"><p>بلبلان را به چمن با گل رعنا چه خوشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانک سرنای چه گر مونس غمگینان‌ست</p></div>
<div class="m2"><p>از دم روح نفخنا دل سرنا چه خوشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه شب بازرهد خلق ز اندیشه به خواب</p></div>
<div class="m2"><p>در رخ شمس ضحی دیده بینا چه خوشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت پرستانه تو را پای فرورفت به گل</p></div>
<div class="m2"><p>تو چه دانی که بر این گنبد مینا چه خوشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تجلی بود از رحمت حق موسی را</p></div>
<div class="m2"><p>زان شکرریز لقا سینه سینا چه خوشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که صدا دارد و در کان زر صامت هم هست</p></div>
<div class="m2"><p>گه خمش بودن و گه گفت مواسا چه خوشست</p></div></div>