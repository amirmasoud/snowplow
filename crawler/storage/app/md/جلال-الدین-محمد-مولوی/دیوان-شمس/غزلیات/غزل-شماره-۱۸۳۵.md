---
title: >-
    غزل شمارهٔ ۱۸۳۵
---
# غزل شمارهٔ ۱۸۳۵

<div class="b" id="bn1"><div class="m1"><p>گرم درآ و دم مده ساقی بردبار من</p></div>
<div class="m2"><p>ای دم تو ندیم من ای رخ تو بهار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هین که خروس بانگ زد بوی صبوح می دهد</p></div>
<div class="m2"><p>بر کف همچو بحر نه بلبله عقار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه به باده خنده کن مرده به باده زنده کن</p></div>
<div class="m2"><p>چونک چنین کنی بتا بس به نواست کار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بند من است مشتبه باز گشا گره گره</p></div>
<div class="m2"><p>تا که برهنه‌تر شود خفیه و آشکار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک حیا و شرم کن پشت مراد گرم کن</p></div>
<div class="m2"><p>پشت من و پناه من خویش من و تبار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست قبول مست تو باده ز غیر دست تو</p></div>
<div class="m2"><p>آن رخ من چو گل کند وان شکند خمار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داد هزار جان بده باده آسمان بده</p></div>
<div class="m2"><p>تا که پرد همای جان مست سوی مطار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان برهد ز کنده‌ها زین همه تخته بندها</p></div>
<div class="m2"><p>مقعد صدق بررود صادق حق گزار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باده ده و نهان بده از ره عقل و جان بده</p></div>
<div class="m2"><p>تا نرسد به هر کسی عشرت و کار و بار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم عوام بسته به روح ز شهر رسته به</p></div>
<div class="m2"><p>فتنه و شر نشسته به ای شه باوقار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باده همی‌زند لمع جان هزار با طمع</p></div>
<div class="m2"><p>مست و پیاده می تپد گرد می سوار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست بدار از این قدح گیر عوض از آن فرج</p></div>
<div class="m2"><p>تا بزند بر اندهت تابش ابتشار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ نیرزد این میش نی غلیان و نی قیش</p></div>
<div class="m2"><p>این بفروش و باده بین باده بی‌کنار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست نلرزدت از این بی‌خرد خوش رزین</p></div>
<div class="m2"><p>جام گزین و می ببین از کف شهریار من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پر ز حیات جام او مشک و عبر ختام او</p></div>
<div class="m2"><p>دیو و پری غلام او چستی و انتشار من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برجه ساقیا تو گو چون تو صفت کننده کو</p></div>
<div class="m2"><p>ای که ز لطف نسج او سخت درید تار من</p></div></div>