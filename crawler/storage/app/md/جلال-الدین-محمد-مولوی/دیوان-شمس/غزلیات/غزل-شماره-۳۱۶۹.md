---
title: >-
    غزل شمارهٔ ۳۱۶۹
---
# غزل شمارهٔ ۳۱۶۹

<div class="b" id="bn1"><div class="m1"><p>گر نه شکار غم دلدارمی</p></div>
<div class="m2"><p>گردن شیر فلک افشارمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست مرا بست، وگر نی کنون</p></div>
<div class="m2"><p>من سر تو بهتر ازین خارمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نبدی رشک رخ چون گلشن</p></div>
<div class="m2"><p>بلبل هر گلشن و گلزارمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر گل او در نگشادی، چرا</p></div>
<div class="m2"><p>خار صفت بر سر دیوارمی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست یکی کار که او آن نکرد</p></div>
<div class="m2"><p>ورنه چرا کاهل و بی‌کارمی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق طبیبست که رنجور جوست</p></div>
<div class="m2"><p>ورنه چرا خسته و بیمارمی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشت خلیل از پی او چار مرغ</p></div>
<div class="m2"><p>کاش به قربانیش آن چارمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا پی خوردن به شکر خوردنش</p></div>
<div class="m2"><p>طوطی با صد سر و منقارمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز جهت قوت دگر طوطیان</p></div>
<div class="m2"><p>چون لب او جمله شکر کارمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نه دلی داد چو دریا مرا</p></div>
<div class="m2"><p>چون دگران تند و جگر خوارمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در سر من عشق بپیچید سخت</p></div>
<div class="m2"><p>ورنه چرا بی‌دل و دستارمی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر لب من دوش ببوسید یار</p></div>
<div class="m2"><p>ورنه چرا با مزه گفتارمی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر خط من نقطهٔ دولت نهاد</p></div>
<div class="m2"><p>ورنه چه گردنده چو پرگارمی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نه‌امی پست، که دیدی مرا؟!</p></div>
<div class="m2"><p>ورنه امی مست بهنجارمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چونک ز مستی کژ و مژ می‌روم</p></div>
<div class="m2"><p>کاش که من بر ره هموارمی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا مثل لاله رخان خوشش</p></div>
<div class="m2"><p>معتزلی بر سر کهسارمی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بس! که گرین بانگ دهل نیستی</p></div>
<div class="m2"><p>همچو خیالات در اسرارمی</p></div></div>