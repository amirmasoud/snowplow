---
title: >-
    غزل شمارهٔ ۱۳۲۱
---
# غزل شمارهٔ ۱۳۲۱

<div class="b" id="bn1"><div class="m1"><p>ایا هوای تو در جان‌ها سلام علیک</p></div>
<div class="m2"><p>غلام می‌خری ارزان بها سلام علیک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایا کسی که هزاران هزار جان و روان</p></div>
<div class="m2"><p>همی‌کشند ز هر سو تو را سلام علیک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به وقت خواندن آن نامه‌های خون آلود</p></div>
<div class="m2"><p>بخوان ز جانب این آشنا سلام علیک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو می‌خرامی و خورشید و ماه در پی تو</p></div>
<div class="m2"><p>همی‌دوند که‌ای خوش لقا سلام علیک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خاک پای تو هر دم همی‌کنند پیغام</p></div>
<div class="m2"><p>هزار چشم که ای توتیا سلام علیک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو تیزگوش تری از همه که هر نفست</p></div>
<div class="m2"><p>ز غیب می‌رسد از انبیا سلام علیک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلام خشک نباشد خصوص از شاهان</p></div>
<div class="m2"><p>هزار خلعت و هدیه‌ست با سلام علیک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنانک کرد خداوند در شب معراج</p></div>
<div class="m2"><p>به نور مطلق بر مصطفی سلام علیک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهی سلام که دارد ز نور دنب دراز</p></div>
<div class="m2"><p>چنین بود چو کند کبریا سلام علیک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گذشت این همه ای دوست ماجرا بشنو</p></div>
<div class="m2"><p>ولیک پیشتر از ماجرا سلام علیک</p></div></div>