---
title: >-
    غزل شمارهٔ ۲۹۵۵
---
# غزل شمارهٔ ۲۹۵۵

<div class="b" id="bn1"><div class="m1"><p>چون روی آتشین را یک دم تو می‌نپوشی</p></div>
<div class="m2"><p>ای دوست چند جوشم گویی که چند جوشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان و عقل مسکین کی یابد از تو تسکین</p></div>
<div class="m2"><p>زین سان که تو نهادی قانون می فروشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرنای جان‌ها را در می دمی تو دم دم</p></div>
<div class="m2"><p>نی را چه جرم باشد چون تو همی‌خروشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روپوش برنتابد گر تاب روی این است</p></div>
<div class="m2"><p>پنهان نگردد این رو گر صد هزار پوشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر گرد شید گردی ای جان عشق ساده</p></div>
<div class="m2"><p>یا نیک سرخ چشمی یا خود سیاه گوشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز آنک عقل داری دیوانه چون نگشتی</p></div>
<div class="m2"><p>ور نه از اصل عشقی با عشق چند کوشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اجزای خویش دیدم اندر حضور خامش</p></div>
<div class="m2"><p>بس نعره‌ها شنیدم در زیر هر خموشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم به شمس تبریز کاین خامشان کیانند</p></div>
<div class="m2"><p>گفتا چو وقت آید تو نیز هم نپوشی</p></div></div>