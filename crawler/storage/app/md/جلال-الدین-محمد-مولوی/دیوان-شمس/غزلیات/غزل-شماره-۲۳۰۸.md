---
title: >-
    غزل شمارهٔ ۲۳۰۸
---
# غزل شمارهٔ ۲۳۰۸

<div class="b" id="bn1"><div class="m1"><p>یا رب چه کس است آن مه یا رب چه کس است آن مه</p></div>
<div class="m2"><p>کز چهره بزد آتش در خیمه و در خرگه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر ذقن یوسف چاهی چه عجب چاهی</p></div>
<div class="m2"><p>صد یوسف کنعانی اندر تک آن خوش چه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر چه کند یوسف کز چاه بپرهیزد</p></div>
<div class="m2"><p>کو دیده ربودستش و آن چاه میان ره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن کس که ربود از رخ مر کاه ربایان را</p></div>
<div class="m2"><p>انصاف بده آخر با او چه کند یک که</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنهار نگهدارید زان غمزه زبان‌ها را</p></div>
<div class="m2"><p>کو مست بود خفته از حال همه آگه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شطرنج همی‌بازد با بنده و این طرفه</p></div>
<div class="m2"><p>کاندر دو جهان شه او وز بنده بخواهد شه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان بخشد و جان بخشد چندانک فناها را</p></div>
<div class="m2"><p>در خانه و مان افتد هم ماتم و هم آوه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او جان بهاران است جان‌هاست درختانش</p></div>
<div class="m2"><p>جان‌ها شود آبستن هم نسل دهد هم زه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آینه کو بیند شمس الحق تبریزی</p></div>
<div class="m2"><p>هم آینه برسوزد هم آینه گوید خه</p></div></div>