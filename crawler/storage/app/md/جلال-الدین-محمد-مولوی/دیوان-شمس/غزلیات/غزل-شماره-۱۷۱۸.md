---
title: >-
    غزل شمارهٔ ۱۷۱۸
---
# غزل شمارهٔ ۱۷۱۸

<div class="b" id="bn1"><div class="m1"><p>ای تو ترش کرده رو تا که بترسانیم</p></div>
<div class="m2"><p>بسته شکرخنده را تا که بگریانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترش نگردم از آنک از تو همه شکرم</p></div>
<div class="m2"><p>گریه نصیب تن است من گهر جانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل آتش روم تازه و خندان شوم</p></div>
<div class="m2"><p>همچو زر سرخ از آنک جمله زر کانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل آتش اگر غیر تو را بنگرم</p></div>
<div class="m2"><p>دار مرا سنگسار ز آنچ من ارزانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ نشینم به عیش هیچ نخیزم به پا</p></div>
<div class="m2"><p>جز تو که برداریم جز تو که بنشانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این دل من صورتی گشت و به من بنگرید</p></div>
<div class="m2"><p>بوسه همی‌داد دل بر سر و پیشانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم ای دل بگو خیر بود حال چیست</p></div>
<div class="m2"><p>تو نه که نوری همه من نه که ظلمانیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور تو منی من توام خیرگی از خود ز چیست</p></div>
<div class="m2"><p>مست بخندید و گفت دل که نمی‌دانیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو مطلب تو محال نیست زبان را مجال</p></div>
<div class="m2"><p>سوره کهفم که تو خفته فروخوانیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زود بر او درفتاد صورت من پیش دل</p></div>
<div class="m2"><p>گفت بگو راست ای صادق ربانیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت که این حیرت از منظر شمس حق است</p></div>
<div class="m2"><p>مفخر تبریزیان آنک در او فانیم</p></div></div>