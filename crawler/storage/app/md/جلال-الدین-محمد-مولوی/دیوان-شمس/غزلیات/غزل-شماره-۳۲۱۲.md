---
title: >-
    غزل شمارهٔ ۳۲۱۲
---
# غزل شمارهٔ ۳۲۱۲

<div class="b" id="bn1"><div class="m1"><p>الا یا مالکا رق‌الزمان</p></div>
<div class="m2"><p>الا یا ناسخا، حسن الغوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا من لطفه ماء زلال</p></div>
<div class="m2"><p>و مافی‌الکون ظرف کالاوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سجود کل اوج او حضیض</p></div>
<div class="m2"><p>بشمس‌الدین سلطان المعانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الا تبریز بشراک دواما</p></div>
<div class="m2"><p>و صار ساجدیک‌المشرقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظل‌الله تبریزا بظل</p></div>
<div class="m2"><p>تضعضع من تصوره جنانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تعالی عن مدیحی، قد تعالی</p></div>
<div class="m2"><p>ولکن لیس صبر فی لسانی</p></div></div>