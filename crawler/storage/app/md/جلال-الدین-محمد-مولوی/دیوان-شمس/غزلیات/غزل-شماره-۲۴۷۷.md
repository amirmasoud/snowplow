---
title: >-
    غزل شمارهٔ ۲۴۷۷
---
# غزل شمارهٔ ۲۴۷۷

<div class="b" id="bn1"><div class="m1"><p>سرکه هفت ساله را از لب او حلاوتی</p></div>
<div class="m2"><p>خاربنان خشک را از گل او طراوتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل فسرده را از نظرش گشایشی</p></div>
<div class="m2"><p>سنگ سیاه مرده را از گذرش سعادتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گذری که او کند گردد سرد دوزخی</p></div>
<div class="m2"><p>وز نظری که افکند زنده شود ولایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرده ز گور برجهد آید و مستمع شود</p></div>
<div class="m2"><p>گر بت من ز مرده‌ای یاد کند حکایتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنک ز چشم شوخ او هر نفسی است فتنه‌ای</p></div>
<div class="m2"><p>آنک ز لطف قامتش هر طرفی قیامتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه که در فراق او هر قدمی است آتشی</p></div>
<div class="m2"><p>آه که از هوای او می‌رسدم ملامتی</p></div></div>