---
title: >-
    غزل شمارهٔ ۳۱۴۲
---
# غزل شمارهٔ ۳۱۴۲

<div class="b" id="bn1"><div class="m1"><p>حکم نو کن که شاه دورانی</p></div>
<div class="m2"><p>سکه تازه زن که سلطانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکم مطلق تو راست در عالم</p></div>
<div class="m2"><p>حاکمان قالب‌اند و تو جانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چه شاهان به خواب می‌جستند</p></div>
<div class="m2"><p>چون مسلم شدت به آسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه مرغان چو دانه چین تواند</p></div>
<div class="m2"><p>تو همایی میان مرغانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر آمد رواق دولت تو</p></div>
<div class="m2"><p>ز آن که تو صاف صاف انسانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برتر آید ز جان ملک و ملک</p></div>
<div class="m2"><p>گر دهی دل به روح حیوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرط‌ها را ز عاشقان برگیر</p></div>
<div class="m2"><p>که تو احوال شان همی‌دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دام‌ها را ز راه شان بردار</p></div>
<div class="m2"><p>خواه تقدیر و خواه شیطانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا شوم سرخ رو در این دعوی</p></div>
<div class="m2"><p>که تو چون حق لطیف فرمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمس تبریز رحمت صرفی</p></div>
<div class="m2"><p>ز آن که سر صفات رحمانی</p></div></div>