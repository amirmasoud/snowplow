---
title: >-
    غزل شمارهٔ ۲۶۱۳
---
# غزل شمارهٔ ۲۶۱۳

<div class="b" id="bn1"><div class="m1"><p>ای شادی آن روزی کز راه تو بازآیی</p></div>
<div class="m2"><p>در روزن جان تابی چون ماه ز بالایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان ماه پرافزایش آن فارغ از آرایش</p></div>
<div class="m2"><p>این فرش زمینی را چون عرش بیارایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس عاقل پابسته کز خویش شود رسته</p></div>
<div class="m2"><p>بس جان که ز سر گیرد قانون شکرخایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین منزل شش گوشه بی‌مرکب و بی‌توشه</p></div>
<div class="m2"><p>بس قافله ره یابد در عالم بی‌جایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشن کن جان من تا گوید جان با تن</p></div>
<div class="m2"><p>کامروز مرا بنگر ای خواجه فردایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو آبی و من جویم جز وصل تو کی جویم</p></div>
<div class="m2"><p>رونق نبود جو را چون آب بنگشایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شاد تو از پیشی یعنی ز همه بیشی</p></div>
<div class="m2"><p>والله که چو با خویشی از خویش نیاسایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جستن دل بودم بر راه خودش دیدم</p></div>
<div class="m2"><p>افتاده در این سودا چون مردم صفرایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس الحق تبریزی پالود مرا هجرت</p></div>
<div class="m2"><p>جز عشق نبینی گر صد بار بپالایی</p></div></div>