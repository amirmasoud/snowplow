---
title: >-
    غزل شمارهٔ ۲۶۶۷
---
# غزل شمارهٔ ۲۶۶۷

<div class="b" id="bn1"><div class="m1"><p>مبارک باد بر ما این عروسی</p></div>
<div class="m2"><p>خجسته باد ما را این عروسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شیر و چون شکر بادا همیشه</p></div>
<div class="m2"><p>چو صهبا و چو حلوا این عروسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم از برگ و هم از میوه ممتع</p></div>
<div class="m2"><p>مثال نخل خرما این عروسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو حوران بهشتی باد خندان</p></div>
<div class="m2"><p>ابد امروز فردا این عروسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان رحمت و توقیع دولت</p></div>
<div class="m2"><p>هم این جا و هم آن جا این عروسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکونام و نکوروی و نکوفال</p></div>
<div class="m2"><p>چو ماه و چرخ خضرا این عروسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خمش کردم که در گفتن نگنجد</p></div>
<div class="m2"><p>که بسرشت است جان با این عروسی</p></div></div>