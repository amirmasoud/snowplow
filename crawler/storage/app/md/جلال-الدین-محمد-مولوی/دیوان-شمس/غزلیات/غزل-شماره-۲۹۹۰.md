---
title: >-
    غزل شمارهٔ ۲۹۹۰
---
# غزل شمارهٔ ۲۹۹۰

<div class="b" id="bn1"><div class="m1"><p>جان خاک آن مهی که خداش است مشتری</p></div>
<div class="m2"><p>آن کس ملک ندید و نه انسان و نی پری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از خودی برون شد او آدمی نماند</p></div>
<div class="m2"><p>او راست چشم روشن و گوش پیمبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا آدمی است آدمی و تا ملک ملک</p></div>
<div class="m2"><p>بسته‌ست چشم هر دو از آن جان و دلبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم به حکم او است مر او را چه فخر از این</p></div>
<div class="m2"><p>چون آن او است خالق عالم به یک سوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحری که کمترین شبه را گوهری کند</p></div>
<div class="m2"><p>حاشا از او که لاف برآرد ز گوهری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ذره است لایق رقص چنان شعاع</p></div>
<div class="m2"><p>کو گشت از هزار چو خورشید و مه بری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن ذره‌ای که گر قدمش بوسد آفتاب</p></div>
<div class="m2"><p>خود ننگرد به تابش او جز که سرسری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنما مها به کوری خورشید تابشی</p></div>
<div class="m2"><p>تا زین سپس زنخ نزند از منوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درتاب شاه و مفخر تبریز شمس دین</p></div>
<div class="m2"><p>تا هر دو کون پر شود از نور داوری</p></div></div>