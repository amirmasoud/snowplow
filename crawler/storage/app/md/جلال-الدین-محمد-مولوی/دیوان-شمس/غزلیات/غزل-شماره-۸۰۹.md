---
title: >-
    غزل شمارهٔ ۸۰۹
---
# غزل شمارهٔ ۸۰۹

<div class="b" id="bn1"><div class="m1"><p>طرفه گرمابه بانی کو ز خلوت برآید</p></div>
<div class="m2"><p>نقش گرمابه یک یک در سجود اندرآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش‌های فسرده بی‌خبروار مرده</p></div>
<div class="m2"><p>ز انعکاسات چشمش چشمشان عبهر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوش‌هاشان ز گوشش اهل افسانه گردد</p></div>
<div class="m2"><p>چشم‌هاشان ز چشمش قابل منظر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش گرمابه بینی هر یکی مست و رقصان</p></div>
<div class="m2"><p>چون معاشر که گه گه در می احمر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر شده بانگ و نعره صحن گرمابه ز ایشان</p></div>
<div class="m2"><p>کز هیاهوی و غلغل غره محشر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش‌ها یک دگر را جانب خویش خوانند</p></div>
<div class="m2"><p>نقش از آن گوشه خندان سوی این دیگر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیک گرمابه بان را صورتی درنیابد</p></div>
<div class="m2"><p>گر چه صورت ز جستن در کر و در فر آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله گشته پریشان او پس و پیش ایشان</p></div>
<div class="m2"><p>ناشناسا شه جان بر سر لشکر آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلشن هر ضمیری از رخش پرگل آید</p></div>
<div class="m2"><p>دامن هر فقیری از کفش پرزر آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دار زنبیل پیشش تا کند پر ز خویشش</p></div>
<div class="m2"><p>تا که زنبیل فقرت حسرت سنجر آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برهد از بیش وز کم قاضی و مدعی هم</p></div>
<div class="m2"><p>چونک آن ماه یک دم مست در محضر آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باده خمخانه گردد مرده مستانه گردد</p></div>
<div class="m2"><p>چوب حنانه گردد چونک بر منبر آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کم کند از لقاشان بفسرد نقش‌هاشان</p></div>
<div class="m2"><p>گم شود چشم‌هاشان گوش‌هاشان کر آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز چون رو نماید چشم‌ها برگشاید</p></div>
<div class="m2"><p>باغ پرمرغ گردد بوستان اخضر آید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رو به گلزار و بستان دوستان بین و دستان</p></div>
<div class="m2"><p>در پی این عبارت جان بدان معبر آید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنچ شد آشکارا کی توان گفت یارا</p></div>
<div class="m2"><p>کلک آن کی نویسد گر چه در محبر آید</p></div></div>