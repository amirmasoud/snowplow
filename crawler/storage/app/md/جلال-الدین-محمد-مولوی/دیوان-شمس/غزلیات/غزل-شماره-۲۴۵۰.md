---
title: >-
    غزل شمارهٔ ۲۴۵۰
---
# غزل شمارهٔ ۲۴۵۰

<div class="b" id="bn1"><div class="m1"><p>در دل خیالش زان بود تا تو به هر سو ننگری</p></div>
<div class="m2"><p>و آن لطف بی‌حد زان کند تا هیچ از حد نگذری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با صوفیان صاف دین در وجد گردی همنشین</p></div>
<div class="m2"><p>گر پای در بیرون نهی زین خانقاه شش دری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داری دری پنهان صفت شش در مجو و شش جهت</p></div>
<div class="m2"><p>پنهان دری که هر شبی زان در همی‌بیرون پری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون می‌پری بر پای تو رشته خیالی بسته‌اند</p></div>
<div class="m2"><p>تا واکشندت صبحدم تا برنپری یک سری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازآ به زندان رحم تا خلقتت کامل شدن</p></div>
<div class="m2"><p>هست این جهان همچون رحم این جمله خون زان می‌خوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان را چو بررویید پر شد بیضه تن را شکست</p></div>
<div class="m2"><p>جان جعفر طیار شد تا می‌نماید جعفری</p></div></div>