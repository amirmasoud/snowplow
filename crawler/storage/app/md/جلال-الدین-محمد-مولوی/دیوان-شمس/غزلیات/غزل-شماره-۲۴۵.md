---
title: >-
    غزل شمارهٔ ۲۴۵
---
# غزل شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>از برای صلاح مجنون را</p></div>
<div class="m2"><p>بازخوان ای حکیم افسون را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای علاج بی‌خبری</p></div>
<div class="m2"><p>درج کن در نبیذ افیون را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نداری خلاص بی‌چون شو</p></div>
<div class="m2"><p>تا ببینی جمال بی‌چون را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل پرخون ببین تو ای ساقی</p></div>
<div class="m2"><p>درده آن جام لعل چون خون را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانک عقل از برای مادونی</p></div>
<div class="m2"><p>سجده آرد ز حرص هر دون را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده خواران به نیم جو نخرند</p></div>
<div class="m2"><p>این دو قرص درست گردون را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخوت عشق را ز مجنون پرس</p></div>
<div class="m2"><p>تا که در سر چهاست مجنون را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گمرهی‌های عشق بردرد</p></div>
<div class="m2"><p>صد هزاران طریق و قانون را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای صبا تو برو بگو از من</p></div>
<div class="m2"><p>از کرم بحر در مکنون را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه از خشم گفته‌ای نکنم</p></div>
<div class="m2"><p>روح بخش این حماء مسنون را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس تبریز موسی عهدی</p></div>
<div class="m2"><p>در فراقت مدارهارون را</p></div></div>