---
title: >-
    غزل شمارهٔ ۱۰۲۲
---
# غزل شمارهٔ ۱۰۲۲

<div class="b" id="bn1"><div class="m1"><p>دی سحری بر گذری گفت مرا یار</p></div>
<div class="m2"><p>شیفته و بی‌خبری چند از این کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهره من رشک گل و دیده خود را</p></div>
<div class="m2"><p>کرده پر از خون جگر در طلب خار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم کی پیش قدت سرو نهالی</p></div>
<div class="m2"><p>گفتم کی پیش رخت شمع فلک تار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم کی زیر و زبر چرخ و زمینت</p></div>
<div class="m2"><p>نیست عجب گر بر تو نیست مرا بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت منم جان و دلت خیره چه باشی</p></div>
<div class="m2"><p>دم مزن و باش بر سیمبرم زار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم کی از دل و جان برده قراری</p></div>
<div class="m2"><p>نیست مرا تاب سکون گفت به یک بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطره دریای منی دم چه زنی بیش</p></div>
<div class="m2"><p>غرقه شو و جان صدف پر ز گهر دار</p></div></div>