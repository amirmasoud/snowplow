---
title: >-
    غزل شمارهٔ ۷۶۳
---
# غزل شمارهٔ ۷۶۳

<div class="b" id="bn1"><div class="m1"><p>خنک آن کس که چو ما شد همگی لطف و رضا شد</p></div>
<div class="m2"><p>ز جفا رست و ز غصه همه شادی و وفا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز طرب چون طربون شد خرد از باده زبون شد</p></div>
<div class="m2"><p>گرو عشق و جنون شد گهر بحر صفا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه و خورشید نظر شد که از او خاک چو زر شد</p></div>
<div class="m2"><p>به کرم بحر گهر شد به روش باد صبا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شه عشق کشیدش ز همه خلق بریدش</p></div>
<div class="m2"><p>نظر عشق گزیدش همه حاجات روا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سفر چون مه گردون به شب چارده پر شد</p></div>
<div class="m2"><p>به نظرهای الهی به یکی لحظه کجا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زمین بود فلک شد همگی حسن و نمک شد</p></div>
<div class="m2"><p>بشری بود ملک شد مگسی بود هما شد</p></div></div>