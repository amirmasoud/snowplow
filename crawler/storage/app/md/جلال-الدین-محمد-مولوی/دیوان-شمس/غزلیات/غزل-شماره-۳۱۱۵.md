---
title: >-
    غزل شمارهٔ ۳۱۱۵
---
# غزل شمارهٔ ۳۱۱۵

<div class="b" id="bn1"><div class="m1"><p>در لطف اگر بروی شاه همه چمنی</p></div>
<div class="m2"><p>در قهر اگر بروی که را ز بن بکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که بر گل تو بلبل چه ناله کند</p></div>
<div class="m2"><p>املی الهوی اسقا یوم النوی بدنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل از تو تازه بود جان از تو زنده بود</p></div>
<div class="m2"><p>تو عقل عقل منی تو جان جان منی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من مست نعمت تو دانم ز رحمت تو</p></div>
<div class="m2"><p>کز من به هر گنهی دل را تو برنکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاج تو بر سر ما نور تو در بر ما</p></div>
<div class="m2"><p>بوی تو رهبر ما گر راه ما نزنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حارس تویی رمه را ایمن کنی همه را</p></div>
<div class="m2"><p>اهوی الهوا امنو فی ظل ذو المننی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن دم که دم بزنم با تو ز خود بروم</p></div>
<div class="m2"><p>لو لا مخاطبتی ایاک لم ترنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جان اسیر تنی وی تن حجاب منی</p></div>
<div class="m2"><p>وی سر تو در رسنی وی دل تو در وطنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل چو در وطنی یاد آر صحبت ما</p></div>
<div class="m2"><p>آخر رفیق بدی در راه ممتحنی</p></div></div>