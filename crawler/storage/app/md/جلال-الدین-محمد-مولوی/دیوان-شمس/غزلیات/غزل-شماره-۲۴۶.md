---
title: >-
    غزل شمارهٔ ۲۴۶
---
# غزل شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>صد دهل می‌زنند در دل ما</p></div>
<div class="m2"><p>بانگ آن بشنویم ما فردا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنبه در گوش و موی در چشمست</p></div>
<div class="m2"><p>غم فردا و وسوسه سودا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش عشق زن در این پنبه</p></div>
<div class="m2"><p>همچو حلاج و همچو اهل صفا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش و پنبه را چه می‌داری</p></div>
<div class="m2"><p>این دو ضدند و ضد نکرد بقا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ملاقات عشق نزدیکست</p></div>
<div class="m2"><p>خوش لقا شو برای روز لقا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرگ ما شادی و ملاقاتست</p></div>
<div class="m2"><p>گر تو را ماتمست رو زین جا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونک زندان ماست این دنیا</p></div>
<div class="m2"><p>عیش باشد خراب زندان‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنک زندان او چنین خوش بود</p></div>
<div class="m2"><p>چون بود مجلس جهان آرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو وفا را مجو در این زندان</p></div>
<div class="m2"><p>که در این جا وفا نکرد وفا</p></div></div>