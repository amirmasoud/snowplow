---
title: >-
    غزل شمارهٔ ۱۴۸۰
---
# غزل شمارهٔ ۱۴۸۰

<div class="b" id="bn1"><div class="m1"><p>خیزید مخسپید که نزدیک رسیدیم</p></div>
<div class="m2"><p>آواز خروس و سگ آن کوی شنیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله که نشان‌های قروی ده یارست</p></div>
<div class="m2"><p>آن نرگس و نسرین و قرنفل که چریدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ذوق چراگاه و ز اشتاب چریدن</p></div>
<div class="m2"><p>وز حرص زبان و لب و پدفوز گزیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تیر پریدیم و بسی صید گرفتیم</p></div>
<div class="m2"><p>گر چه چو کمان از زه احکام خمیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما عاشق مستیم به صد تیغ نگردیم</p></div>
<div class="m2"><p>شیریم که خون دل فغفور چشیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستان الستیم به جز باده ننوشیم</p></div>
<div class="m2"><p>بر خوان جهان نی ز پی آش و ثریدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حق داند و حق دید که در وقت کشاکش</p></div>
<div class="m2"><p>از ما چه کشیدید وز ایشان چه کشیدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیزید مخسپید که هنگام صبوح است</p></div>
<div class="m2"><p>استاره روز آمد و آثار بدیدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب بود و همه قافله محبوس رباطی</p></div>
<div class="m2"><p>خیزید کز آن ظلمت و آن حبس رهیدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورشید رسولان بفرستاد در آفاق</p></div>
<div class="m2"><p>کاینک یزک مشرق و ما جیش عتیدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هین رو به شفق آر اگر طایر روزی</p></div>
<div class="m2"><p>کز سوی شفق چون نفس صبح دمیدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کس که رسولی شفق را بشناسد</p></div>
<div class="m2"><p>ما نیز در اظهار بر او فاش و پدیدیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان کس که رسولی شفق را نپذیرد</p></div>
<div class="m2"><p>هم محرم ما نیست بر او پرده تنیدیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خفاش نپذرفت فرودوخت از او چشم</p></div>
<div class="m2"><p>ما پرده آن دوخته را هم بدریدیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تریاق جهان دید و گمان برد که زهر است</p></div>
<div class="m2"><p>ای مژده دلی را که ز پندار خریدیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خامش کن تا واعظ خورشید بگوید</p></div>
<div class="m2"><p>کو بر سر منبر شد و ما جمله مریدیم</p></div></div>