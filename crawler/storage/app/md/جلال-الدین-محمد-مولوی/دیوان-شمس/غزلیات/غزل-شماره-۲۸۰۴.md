---
title: >-
    غزل شمارهٔ ۲۸۰۴
---
# غزل شمارهٔ ۲۸۰۴

<div class="b" id="bn1"><div class="m1"><p>چون تو آن روبند را از روی چون مه برکنی</p></div>
<div class="m2"><p>چون قضای آسمانی توبه‌ها را بشکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منگر اندر شور و بدمستی من ای نیک عهد</p></div>
<div class="m2"><p>بنگر آخر در میی کاندر سرم می‌افکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول از دست فراقت عاشقان را تی کنی</p></div>
<div class="m2"><p>وآنگه اندر پوستشان تا سر همه در زر کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه رخا سیمرغ جانی منزل تو کوه قاف</p></div>
<div class="m2"><p>از تو پرسیدن چه حاجت کز کدامین مسکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کلام تو شنید از بخت نفس ناطقه</p></div>
<div class="m2"><p>کرد صد اقرار بر خود بهر جهل و الکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز غیر شمس تبریزی بریدی ای بدن</p></div>
<div class="m2"><p>در حریر و در زر و در دیبه و در ادکنی</p></div></div>