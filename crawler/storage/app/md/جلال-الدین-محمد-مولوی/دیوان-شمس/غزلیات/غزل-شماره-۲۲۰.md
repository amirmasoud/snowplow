---
title: >-
    غزل شمارهٔ ۲۲۰
---
# غزل شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>ز بامداد سعادت سه بوسه داد مرا</p></div>
<div class="m2"><p>که بامداد عنایت خجسته باد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد آر دلا تا چه خواب دیدی دوش</p></div>
<div class="m2"><p>که بامداد سعادت دری گشاد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر به خواب بدیدم که مه مرا برداشت</p></div>
<div class="m2"><p>ببرد بر فلک و بر فلک نهاد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتاده دیدم دل را خراب در راهش</p></div>
<div class="m2"><p>ترانه گویان کاین دم چنین فتاد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان عشق و دلم پیش کارها بوده‌ست</p></div>
<div class="m2"><p>که اندک اندک آیدهمی به یاد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نمود به ظاهر که عشق زاد ز من</p></div>
<div class="m2"><p>همی‌بدان به حقیقت که عشق زاد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایا پدید صفاتت نهان چو جان ذاتت</p></div>
<div class="m2"><p>به ذات تو که تویی جملگی مراد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی‌رسد ز توام بوسه و نمی‌بینم</p></div>
<div class="m2"><p>ز پرده‌های طبیعت که این کی داد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مبر وظیفه رحمت که در فنا افتم</p></div>
<div class="m2"><p>فغان برآورم آن جا که داد داد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جای بوسه اگر خود مرا رسد دشنام</p></div>
<div class="m2"><p>خوشم که حادثه کردست اوستاد مرا</p></div></div>