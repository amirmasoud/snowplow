---
title: >-
    غزل شمارهٔ ۱۳۴۶
---
# غزل شمارهٔ ۱۳۴۶

<div class="b" id="bn1"><div class="m1"><p>رفت عمرم در سر سودای دل</p></div>
<div class="m2"><p>وز غم دل نیستم پروای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به قصد جان من برخاسته</p></div>
<div class="m2"><p>من نشسته تا چه باشد رای دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ز حلقه دین گریزد زانک هست</p></div>
<div class="m2"><p>حلقه زلفین خوبان جای دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد او گردم که دل را گرد کرد</p></div>
<div class="m2"><p>کو رسد فریادم از غوغای دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب شب بر چشم خود کردم حرام</p></div>
<div class="m2"><p>تا ببینم صبحدم سیمای دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قد من همچون کمان شد از رکوع</p></div>
<div class="m2"><p>تا ببینم قامت و بالای دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن جهان یک تابش از خورشید دل</p></div>
<div class="m2"><p>وین جهان یک قطره از دریای دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب ببند ایرا به گردون می‌رسد</p></div>
<div class="m2"><p>بی‌زبان هیهای دل هیهای دل</p></div></div>