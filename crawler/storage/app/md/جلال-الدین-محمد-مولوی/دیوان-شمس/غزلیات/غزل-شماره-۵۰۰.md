---
title: >-
    غزل شمارهٔ ۵۰۰
---
# غزل شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>قبله امروز جز شهنشه نیست</p></div>
<div class="m2"><p>هر که آید به در بگو ره نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عذر گو وز بهانه آگه باش</p></div>
<div class="m2"><p>همه خفتند و یک کس آگه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگذارد نه کوته و نه دراز</p></div>
<div class="m2"><p>آتشی کو دراز و کوته نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چه طبع تو خیالاتست</p></div>
<div class="m2"><p>یوسفی بی‌خیال در چه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون که گندم رسید مغز آکند</p></div>
<div class="m2"><p>همره ماست و همره که نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاره پاره کند یکایک را</p></div>
<div class="m2"><p>عشق آن یک که پاره ده نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه گهی می‌کشند گوش تو را</p></div>
<div class="m2"><p>سوی آن عالمی که گه گه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس تبریز شاه ترکانست</p></div>
<div class="m2"><p>رو به صحرا که شه به خرگه نیست</p></div></div>