---
title: >-
    غزل شمارهٔ ۱۹۹
---
# غزل شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>ای خان و مان بمانده و از شهر خود جدا</p></div>
<div class="m2"><p>شاد آمدیت از سفر خانه خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز از سفر به فاقه و شب‌ها قرار نی</p></div>
<div class="m2"><p>در عشق حج کعبه و دیدار مصطفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مالیده رو و سینه در آن قبله گاه حق</p></div>
<div class="m2"><p>در خانه خدا شده قد کان آمنسا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونید و چون بدیت در این راه باخطر</p></div>
<div class="m2"><p>ایمن کند خدای در این راه جمله را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آسمان ز غلغل لبیک حاجیان</p></div>
<div class="m2"><p>تا عرش نعره‌ها و غریوست از صدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان چشم تو ببوسد و بر پات سر نهد</p></div>
<div class="m2"><p>ای مروه را بدیده و بررفته بر صفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهمان حق شدیت و خدا وعده کرده است</p></div>
<div class="m2"><p>مهمان عزیز باشد خاصه به پیش ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان خاک اشتری که کشد بار حاجیان</p></div>
<div class="m2"><p>تا مشعرالحرام و تا منزل منا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بازآمده ز حج و دل آن جا شده مقیم</p></div>
<div class="m2"><p>جان حلقه را گرفته و تن گشته مبتلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شام ذات جحفه و از بصره ذات عرق</p></div>
<div class="m2"><p>باتیغ و باکفن شده این جا که ربنا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوه صفا برآ به سر کوه رخ به بیت</p></div>
<div class="m2"><p>تکبیر کن برادر و تهلیل و هم دعا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اکنون که هفت بار طوافت قبول شد</p></div>
<div class="m2"><p>اندر مقام دو رکعت کن قدوم را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وانگه برآ به مروه و مانند این بکن</p></div>
<div class="m2"><p>تا هفت بار و باز به خانه طواف‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا روز ترویه بشنو خطبه بلیغ</p></div>
<div class="m2"><p>وانگه به جانب عرفات آی در صلا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وانگه به موقف آی و به قرب جبل بایست</p></div>
<div class="m2"><p>پس بامداد بار دگر بیست هم به جا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وان گاه روی سوی منی آر و بعد از آن</p></div>
<div class="m2"><p>تا هفت بار می‌زن و می‌گیر سنگ‌ها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از ما سلام بادا بر رکن و بر حطیم</p></div>
<div class="m2"><p>ای شوق ما به زمزم و آن منزل وفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صبحی بود ز خواب بخیزیم گرد ما</p></div>
<div class="m2"><p>از اذخر و خلیل به ما بو دهد صبا</p></div></div>