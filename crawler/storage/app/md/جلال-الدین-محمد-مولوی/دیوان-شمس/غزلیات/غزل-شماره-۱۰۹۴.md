---
title: >-
    غزل شمارهٔ ۱۰۹۴
---
# غزل شمارهٔ ۱۰۹۴

<div class="b" id="bn1"><div class="m1"><p>پر ده آن جام می را ساقیا بار دیگر</p></div>
<div class="m2"><p>نیست در دین و دنیا همچو تو یار دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر دان در طریقت جهل دان در حقیقت</p></div>
<div class="m2"><p>جز تماشای رویت پیشه و کار دیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو آن رخ نمودی عقل و ایمان ربودی</p></div>
<div class="m2"><p>هست منصور جان را هر طرف دار دیگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان ز تو گشت شیدا دل ز تو گشت دریا</p></div>
<div class="m2"><p>کی کند التفاتی دل به دلدار دیگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز به بغداد کویت یا خوش آباد رویت</p></div>
<div class="m2"><p>نیست هر دم فلک را جز که پیکار دیگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات مردان جام جانست گردان</p></div>
<div class="m2"><p>نیست مانند ایشان هیچ خمار دیگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همتی دار عالی کان شه لاابالی</p></div>
<div class="m2"><p>غیر انبار دنیا دارد انبار دیگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پاره‌ای چون برانی اندر این ره بدانی</p></div>
<div class="m2"><p>غیر این گلستان‌ها باغ و گلزار دیگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پا به مردی فشردی سر سلامت ببردی</p></div>
<div class="m2"><p>رفت دستار بستان شصت دستار دیگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل مرا برد ناگه سوی آن شهره خرگه</p></div>
<div class="m2"><p>من گرفتار گشتم دل گرفتار دیگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز چون عذر آری شب سر خواب خاری</p></div>
<div class="m2"><p>پای ما تا چه گردد هر دم از خار دیگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز که در عشق صانع عمر هرزه‌ست و ضایع</p></div>
<div class="m2"><p>ژاژ دان در طریقت فعل و گفتار دیگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخت اینست و دولت عیش اینست و عشرت</p></div>
<div class="m2"><p>کو جز این عشق و سودا سود و بازار دیگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتمش دل ببردی تا کجاها سپردی</p></div>
<div class="m2"><p>گفت نی من نبردم برد عیار دیگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتمش من نترسم من هم از دل بپرسم</p></div>
<div class="m2"><p>دل بگوید نماند شک و انکار دیگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>راستی گوی ای جان عاشقان را مرنجان</p></div>
<div class="m2"><p>جز تو در دلربایان کو دل افشار دیگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون کمالات فانی هستشان این امانی</p></div>
<div class="m2"><p>که به هر دم نمایند لطف و ایثار دیگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس کمالات آن را کو نگارد جهان را</p></div>
<div class="m2"><p>چون تقاضا نباشد عشق و هنجار دیگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بحر از این روی جوشد مرغ از این رو خروشد</p></div>
<div class="m2"><p>تا در این دام افتد هر دم آشکار دیگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون خدا این جهان را کرد چون گنج پیدا</p></div>
<div class="m2"><p>هر سری پر ز سودا دارد اظهار دیگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کجا خوش نگاری روز و شب بی‌قراری</p></div>
<div class="m2"><p>جوید او حسن خود را نوخریدار دیگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر کجا ماه رویی هر کجا مشک بویی</p></div>
<div class="m2"><p>مشتری وار جوید عاشقی زار دیگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این نفس مست اویم روز دیگر بگویم</p></div>
<div class="m2"><p>هم بر این پرده تر با تو اسرار دیگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس کن و طبل کم زن کاندر این باغ و گلشن</p></div>
<div class="m2"><p>هست پهلوی طبلت بیست نعار دیگر</p></div></div>