---
title: >-
    غزل شمارهٔ ۲۲۴
---
# غزل شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>چه خیره می‌نگری در رخ من ای برنا</p></div>
<div class="m2"><p>مگر که در رخمست آیتی از آن سودا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر که بر رخ من داغ عشق می‌بینی</p></div>
<div class="m2"><p>میان داغ نبشته که نحن نزلنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار مشک همی‌خواهم و هزار شکم</p></div>
<div class="m2"><p>که آب خضر لذیذست و من در استسقا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وفا چه می‌طلبی از کسی که بی‌دل شد</p></div>
<div class="m2"><p>چو دل برفت برفت از پیش وفا و جفا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حق این دل ویران و حسن معمورت</p></div>
<div class="m2"><p>خوش است گنج خیالت در این خرابه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غریو و ناله جان‌ها ز سوی بی‌سویی</p></div>
<div class="m2"><p>مرا ز خواب جهانید دوش وقت دعا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ناله گویم یا از جمال ناله کنان</p></div>
<div class="m2"><p>ز ناله گوش پرست از جمالش آن عینا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قرار نیست زمانی تو را برادر من</p></div>
<div class="m2"><p>ببین که می‌کشدت هر طرف تقاضاها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مثال گویی اندر میان صد چوگان</p></div>
<div class="m2"><p>دوانه تا سر میدان و گه ز سر تا پا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجاست نیت شاه و کجاست نیت گوی</p></div>
<div class="m2"><p>کجاست قامت یار و کجاست بانگ صلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز جوش شوق تو من همچو بحر غریدم</p></div>
<div class="m2"><p>بگو تو ای شه دانا و گوهر دریا گویا</p></div></div>