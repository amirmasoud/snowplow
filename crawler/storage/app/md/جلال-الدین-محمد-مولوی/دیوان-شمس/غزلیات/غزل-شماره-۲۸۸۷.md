---
title: >-
    غزل شمارهٔ ۲۸۸۷
---
# غزل شمارهٔ ۲۸۸۷

<div class="b" id="bn1"><div class="m1"><p>بده ای کف تو را قاعده لطف افزایی</p></div>
<div class="m2"><p>کف دریا چه کند خواجه به جز دریایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تو خواهی که شکرخایی غلط اندازی</p></div>
<div class="m2"><p>ز پی خشم رهی ساعد و کف می‌خایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صنما مغلطه بگذار و مگو تا فردا</p></div>
<div class="m2"><p>چون تویی پای علم نقد که را می‌پایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترشم گفتی و پیش شکر بی‌حد تو</p></div>
<div class="m2"><p>عسل و قند چه دارند به جز سرکایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه من روترشم لیک خم سرکه نیم</p></div>
<div class="m2"><p>ور چه هر جا بروم لیک نیم هرجایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو خوبی و منم آینه روی خوشت</p></div>
<div class="m2"><p>پیش رو دار مرا چونک جهان آرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی غلط گفتم سرمست بدم زفت زدم</p></div>
<div class="m2"><p>کی بود آینه را با رخ تو گنجایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نو فسونی است مرا سخت عجب پیشتر آ</p></div>
<div class="m2"><p>تا به گوش تو فروخوانم ای بینایی</p></div></div>