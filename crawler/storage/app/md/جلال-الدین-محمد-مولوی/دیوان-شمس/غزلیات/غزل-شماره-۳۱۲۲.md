---
title: >-
    غزل شمارهٔ ۳۱۲۲
---
# غزل شمارهٔ ۳۱۲۲

<div class="b" id="bn1"><div class="m1"><p>الا میر خوبان هلا تا نرنجی</p></div>
<div class="m2"><p>بهانه نگیری و از ما نرنجی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی یار غارم امید تو دارم</p></div>
<div class="m2"><p>که سر را نخارم نگارا نرنجی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو جانان مایی تو خاصان مایی</p></div>
<div class="m2"><p>ز هر جا برنجی از این جا نرنجی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی شب فروزم تویی بخت و روزم</p></div>
<div class="m2"><p>که امشب بخندی و فردا نرنجی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی مشت خاکیم ای جان چه باشد</p></div>
<div class="m2"><p>که از ما و زین‌ها و زان‌ها نرنجی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دانا و نادان شدند از تو شادان</p></div>
<div class="m2"><p>ز نادان نگیری ز دانا نرنجی</p></div></div>