---
title: >-
    غزل شمارهٔ ۱۹۴۳
---
# غزل شمارهٔ ۱۹۴۳

<div class="b" id="bn1"><div class="m1"><p>می گزید او آستین را شرمگین در آمدن</p></div>
<div class="m2"><p>بر سر کویی که پوشد جان‌ها حله بدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن طرف رندان همه شب جامه‌ها را می کنند</p></div>
<div class="m2"><p>تا ببینی روز روشن ما و من بی‌ما و من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رومیانش جامه دزد و زنگیانش جامه دوز</p></div>
<div class="m2"><p>شاد باش ای جامه دزد و آفرین ای جامه کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرفرازی کار شمع و سرسپاری کار او</p></div>
<div class="m2"><p>شرط باشد هر دو کارش هر کی شد شمع لگن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سپردن هر کی زودتر در فروزش بیشتر</p></div>
<div class="m2"><p>سر بنه در زیر پای و دستکی بر هم بزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون درآرد ماه رویی دست خود در گردنت</p></div>
<div class="m2"><p>ترک کن سالوس را تو خویش را بر وی فکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بریزی و برویی آن زمان در باغ او</p></div>
<div class="m2"><p>روی گل بر روی گل هم یاسمن بر یاسمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان اندرربوده از بتان روبندها</p></div>
<div class="m2"><p>زانک در وحدت نباشد نقش‌های مرد و زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر گور بدن بین روح‌ها رقصان شده</p></div>
<div class="m2"><p>تا بدیده صد هزاران خویشتن بی‌خویشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زلف عنبرسای او گوید به جان لولیان</p></div>
<div class="m2"><p>خیز لولی تا رسن بازی کنیم اینک رسن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرتضای عشق شمس الدین تبریزی ببین</p></div>
<div class="m2"><p>چون حسینم خون خود در زهر کش همچون حسن</p></div></div>