---
title: >-
    غزل شمارهٔ ۱۲۵۶
---
# غزل شمارهٔ ۱۲۵۶

<div class="b" id="bn1"><div class="m1"><p>آنک جانش داده‌ای آن را مکش</p></div>
<div class="m2"><p>ور ندادی نقش بی‌جان را مکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دو زلف کافر خود را بگو</p></div>
<div class="m2"><p>کای یگانه اهل ایمان را مکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابا روی خود جلوه مکن</p></div>
<div class="m2"><p>چند روزی ماه تابان را مکش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو سیمرغی به قاف ذوالجلال</p></div>
<div class="m2"><p>بازگرد و جمله مرغان را مکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میان خون هر مسکین مرو</p></div>
<div class="m2"><p>جز قباد و شاه خاقان را مکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر مرا دربان عشقت بار داد</p></div>
<div class="m2"><p>از سر غیرت تو دربان را مکش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر فضولم من که مهمان توام</p></div>
<div class="m2"><p>شرط نبود هیچ مهمان را مکش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مست میدانم ز مِی‌ دانم خراب</p></div>
<div class="m2"><p>شیشه مشکن مست میدان را مکش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریزی تویی سلطان من</p></div>
<div class="m2"><p>بازگشتم باز سلطان را مکش</p></div></div>