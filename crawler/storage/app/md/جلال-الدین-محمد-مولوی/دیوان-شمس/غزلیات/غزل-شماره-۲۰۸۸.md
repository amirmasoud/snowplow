---
title: >-
    غزل شمارهٔ ۲۰۸۸
---
# غزل شمارهٔ ۲۰۸۸

<div class="b" id="bn1"><div class="m1"><p>ببردی دلم را بدادی به زاغان</p></div>
<div class="m2"><p>گرفتم گروگان خیالت به تاوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآیی درآیم بگیری بگیرم</p></div>
<div class="m2"><p>بگویی بگویم علامات مستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشاید نشاید ستم کرد با من</p></div>
<div class="m2"><p>برای گریبان دریدن ز دامان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیاور بیاور شرابی که گفتی</p></div>
<div class="m2"><p>مگو که نگفتم مرنجان مرنجان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرابی شرابی که دل جمع گردد</p></div>
<div class="m2"><p>چو دل جمع گردد شود تن پریشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهم نخواهم شرابی بهایی</p></div>
<div class="m2"><p>از آن بحر بگشا شراب فراوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تو باده دادن ز من سجده کردن</p></div>
<div class="m2"><p>ز من شکر کردن ز تو گوهرافشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنانم کن ای جان که شکرم نماند</p></div>
<div class="m2"><p>وظیفه بیفزا دو چندان سه چندان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجوشان بجوشان شرابی ز سینه</p></div>
<div class="m2"><p>بهاری برآور از این برگ ریزان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرابم کن ای جان که از شهر ویران</p></div>
<div class="m2"><p>خراجی نجوید نه دیوان نه سلطان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمش باش ای تن که تا جان بگوید</p></div>
<div class="m2"><p>علی میر گردد چو بگذشت عثمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمش کردم ای جان بگو نوبت خود</p></div>
<div class="m2"><p>تویی یوسف ما تویی خوب کنعان</p></div></div>