---
title: >-
    غزل شمارهٔ ۸۲۳
---
# غزل شمارهٔ ۸۲۳

<div class="b" id="bn1"><div class="m1"><p>عمر بر اومید فردا می‌رود</p></div>
<div class="m2"><p>غافلانه سوی غوغا می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزگار خویش را امروز دان</p></div>
<div class="m2"><p>بنگرش تا در چه سودا می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه به کیسه گه به کاسه عمر رفت</p></div>
<div class="m2"><p>هر نفس از کیسه ما می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرگ یک یک می‌برد وز هیبتش</p></div>
<div class="m2"><p>عاقلان را رنگ و سیما می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ در ره ایستاده منتظر</p></div>
<div class="m2"><p>خواجه بر عزم تماشا می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرگ از خاطر به ما نزدیکتر</p></div>
<div class="m2"><p>خاطر غافل کجاها می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن مپرور زانک قربانیست تن</p></div>
<div class="m2"><p>دل بپرور دل به بالا می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرب و شیرین کم ده این مردار را</p></div>
<div class="m2"><p>زانک تن پرورد رسوا می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرب و شیرین ده ز حکمت روح را</p></div>
<div class="m2"><p>تا قوی گردد که آن جا می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکمتت از شه صلاح الدین رسد</p></div>
<div class="m2"><p>آنک چون خورشید یکتا می‌رود</p></div></div>