---
title: >-
    غزل شمارهٔ ۱۹۲۱
---
# غزل شمارهٔ ۱۹۲۱

<div class="b" id="bn1"><div class="m1"><p>ای ساقی و دستگیر مستان</p></div>
<div class="m2"><p>دل را ز وفای مست مستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساقی تشنگان مخمور</p></div>
<div class="m2"><p>بس تشنه شدند می پرستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست به دست می روان کن</p></div>
<div class="m2"><p>بر دست مگیر مکر و دستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سررشته نیستی به ما ده</p></div>
<div class="m2"><p>در حسرت نیستند هستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قیصر ما به قیصریه‌ست</p></div>
<div class="m2"><p>ما را منشان به آبلستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا که می است بزم آن جاست</p></div>
<div class="m2"><p>هر جا که وی است نک گلستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک جام برآر همچو خورشید</p></div>
<div class="m2"><p>عالی کن از آن نهال پستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدار حق است مؤمنان را</p></div>
<div class="m2"><p>خوارزم نبیند و دهستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منکر ز برای چشم زخمت</p></div>
<div class="m2"><p>همچو سر خر میان بستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر در دل او نمی‌نشیند</p></div>
<div class="m2"><p>خوش در دل ما نشسته است آن</p></div></div>