---
title: >-
    غزل شمارهٔ ۲۱۰۶
---
# غزل شمارهٔ ۲۱۰۶

<div class="b" id="bn1"><div class="m1"><p>مست رسید آن بت بی‌باک من</p></div>
<div class="m2"><p>دردکش و دلخوش و چالاک من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت به من بنگر و دلشاد شو</p></div>
<div class="m2"><p>هیچ به خود منگر غمناک من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب و گل این دیده تو پرگل است</p></div>
<div class="m2"><p>پاک کنش در نظر پاک من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست بزد خرقه من چاک کرد</p></div>
<div class="m2"><p>گفت مزن بخیه بر این چاک من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی چو بر خاک نهادم بگفت</p></div>
<div class="m2"><p>پاک مکن روی خود از خاک من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای منت آورده منت می‌برم</p></div>
<div class="m2"><p>ز آنک منم شیر و تو شیشاک من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفت زدم در تو و می‌سوز خوش</p></div>
<div class="m2"><p>لیک سیه می‌نکند زاک من</p></div></div>