---
title: >-
    غزل شمارهٔ ۱۴۲
---
# غزل شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>دولتی همسایه شد همسایگان را الصلا</p></div>
<div class="m2"><p>زین سپس باخود نماند بوالعلی و بوالعلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقبت از مشرق جان تیغ زد چون آفتاب</p></div>
<div class="m2"><p>آن که جان می‌جست او را در خلا و در ملا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن ز دور آتش نماید چون روی نوری بود</p></div>
<div class="m2"><p>همچنان که آتش موسی برای ابتلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الصلا پروانه جانان قصد آن آتش کنید</p></div>
<div class="m2"><p>چون بلی گفتید اول درروید اندر بلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سمندر در میان آتشش باشد مقام</p></div>
<div class="m2"><p>هر که دارد در دل و جان این چنین شوق و ولا</p></div></div>