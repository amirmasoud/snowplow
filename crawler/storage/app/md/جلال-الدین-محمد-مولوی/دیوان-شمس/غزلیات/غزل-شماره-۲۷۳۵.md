---
title: >-
    غزل شمارهٔ ۲۷۳۵
---
# غزل شمارهٔ ۲۷۳۵

<div class="b" id="bn1"><div class="m1"><p>برجه که بهار زد صلایی</p></div>
<div class="m2"><p>در باغ خرام چون صبایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شاخ درخت گیر رقصی</p></div>
<div class="m2"><p>وز لاله و که شنو صدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریحان گوید به سبزه رازی</p></div>
<div class="m2"><p>بلبل طلبد ز گل نوایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از باد زند گیاه موجی</p></div>
<div class="m2"><p>در بحر هوای آشنایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز ابر که حامله‌ست از بحر</p></div>
<div class="m2"><p>چون چشم عروس بین بکایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز گریه ابر و خنده برق</p></div>
<div class="m2"><p>در سنبل و سرو ارتقایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فخ شسته به پیش گوش قمری</p></div>
<div class="m2"><p>کموزدش او بهانه‌هایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس گوید به سوسن آخر</p></div>
<div class="m2"><p>برگوی تو هجو یا ثنایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای سوسن صدزبان فروخوان</p></div>
<div class="m2"><p>بر مرغ حکایت همایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوسن گوید خمش که مستم</p></div>
<div class="m2"><p>از جام میی گران بهایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرمستم و بیخودم مبادا</p></div>
<div class="m2"><p>بجهد ز دهان من خطایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رو کن به شهی کز او بپوشید</p></div>
<div class="m2"><p>اشکوفه بریشمین قبایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌گوید بید سرفشانان</p></div>
<div class="m2"><p>رستیم ز دست اژدهایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای سرو برای شکر این را</p></div>
<div class="m2"><p>تو نیز چنین بکوب پایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای جان و جهان به تو رهیدیم</p></div>
<div class="m2"><p>ز اشکنجه جان جان نمایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از وسوسه چنین حریفی</p></div>
<div class="m2"><p>وز دغدغه چنین دغایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان دی که بسی قفا بخوردیم</p></div>
<div class="m2"><p>رفت و بنمودمان قفایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ظاهر مشواد او که آمد</p></div>
<div class="m2"><p>از شوم ظهور او خفایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاموش کن و نظاره می‌کن</p></div>
<div class="m2"><p>بی زحمت خوف در رجایی</p></div></div>