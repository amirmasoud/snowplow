---
title: >-
    غزل شمارهٔ ۲۶۷۱
---
# غزل شمارهٔ ۲۶۷۱

<div class="b" id="bn1"><div class="m1"><p>بر من نیستی یارا کجایی</p></div>
<div class="m2"><p>به هر جایی که هستی جان فزایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خشم من به هر ناکس بسازی</p></div>
<div class="m2"><p>به رغم من به هر آتش درآیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بینی مر مرا نادیده آری</p></div>
<div class="m2"><p>چنین باشد وفا و آشنایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزیزی بودم خوارم ز عشقت</p></div>
<div class="m2"><p>در این خواری نگر کبر خدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای تو جدا گردم ز عالم</p></div>
<div class="m2"><p>که تا ناید مرا بوی جدایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبک روحا گران کردی تو رو را</p></div>
<div class="m2"><p>که یعنی قصد دارم بی‌وفایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو در دل جورها داری همی‌کن</p></div>
<div class="m2"><p>که تا روز قیامت جان مایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الا ای چرخ زاینده چنین ماه</p></div>
<div class="m2"><p>نزایی و نزایی و نزایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کوه قاف شمس الدین تبریز</p></div>
<div class="m2"><p>همایی و همایی و همایی</p></div></div>