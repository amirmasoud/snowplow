---
title: >-
    غزل شمارهٔ ۲۸۳۹
---
# غزل شمارهٔ ۲۸۳۹

<div class="b" id="bn1"><div class="m1"><p>بکشید یار گوشم که تو امشب آن مایی</p></div>
<div class="m2"><p>صنما بلی ولیکن تو نشان بده کجایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو رها کنی بهانه بدهی نشان خانه</p></div>
<div class="m2"><p>به سر و دو دیده آیم که تو کان کیمیایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و اگر به حیله کوشی دغل و دغا فروشی</p></div>
<div class="m2"><p>ز فلک ستاره دزدی ز خرد کله ربایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب من نشان مویت سحرم نشان رویت</p></div>
<div class="m2"><p>قمر از فلک درافتد چو نقاب برگشایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صنما تو همچو شیری من اسیر تو چو آهو</p></div>
<div class="m2"><p>به جهان کی دید صیدی که بترسد از رهایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صنما هوای ما کن طلب رضای ما کن</p></div>
<div class="m2"><p>که ز بحر و کان شنیدم که تو معدن عطایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همگی وبالم از تو به خدا بنالم از تو</p></div>
<div class="m2"><p>بنشان تکبرش را تو خدا به کبریایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ره خواب من چو بستی بمبند راه مستی</p></div>
<div class="m2"><p>ز همه جدام کردی مده از خودم جدایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مه و مهر یار ما شد به امید تو خدا شد</p></div>
<div class="m2"><p>که زهی امید زفتی که زند در خدایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه مال و دل بداده سر کیسه برگشاده</p></div>
<div class="m2"><p>به امید کیسه تو که خلاصه وفایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه را دکان شکسته ره خواب و خور ببسته</p></div>
<div class="m2"><p>به امید آن نشسته که ز گوشه‌ای درآیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به امید کس چه باشی که تویی امید عالم</p></div>
<div class="m2"><p>تو به گوش می چه باشی که تویی می عطایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به درون توست یوسف چه روی به مصر هرزه</p></div>
<div class="m2"><p>تو درآ درون پرده بنگر چه خوش لقایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به درون توست مطرب چه دهی کمر به مطرب</p></div>
<div class="m2"><p>نه کم است تن ز نایی نه کم است جان ز نایی</p></div></div>