---
title: >-
    غزل شمارهٔ ۱۰۶۳
---
# غزل شمارهٔ ۱۰۶۳

<div class="b" id="bn1"><div class="m1"><p>عزم رفتن کرده‌ای چون عمر شیرین یاد دار</p></div>
<div class="m2"><p>کرده‌ای اسب جدایی رغم ما زین یاد دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر زمین و چرخ روید مر تو را یاران صاف</p></div>
<div class="m2"><p>لیک عهدی کرده‌ای با یار پیشین یاد دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده‌ام تقصیرها کان مر تو را کین آورد</p></div>
<div class="m2"><p>لیک شب‌های مرا ای یار بی‌کین یاد دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قرص مه را هر شبی چون بر سر بالین نهی</p></div>
<div class="m2"><p>آنک کردی زانوی ما را تو بالین یاد دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو فرهاد از هوایت کوه هجران می‌کنم</p></div>
<div class="m2"><p>ای تو را خسرو غلام و صد چو شیرین یاد دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر لب دریای چشمم دیده‌ای صحرای عشق</p></div>
<div class="m2"><p>پر ز شاخ زعفران و پر ز نسرین یاد دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>التماس آتشینم سوی گردون می‌رود</p></div>
<div class="m2"><p>جبرئیل از عرش گوید یا رب آمین یاد دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس تبریزی از آن روزی که دیدم روی تو</p></div>
<div class="m2"><p>دین من شد عشق رویت مفخر دین یاد دار</p></div></div>