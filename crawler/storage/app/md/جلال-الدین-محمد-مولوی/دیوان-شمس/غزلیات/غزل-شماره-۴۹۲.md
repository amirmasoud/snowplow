---
title: >-
    غزل شمارهٔ ۴۹۲
---
# غزل شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>ز دام چند بپرسی و دانه را چه شدست</p></div>
<div class="m2"><p>به بام چند برآیی و خانه را چه شدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فسرده چند نشینی میان هستی خویش</p></div>
<div class="m2"><p>تنور آتش عشق و زبانه را چه شدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگرد آتش عشقش ز دور می‌گردی</p></div>
<div class="m2"><p>اگر تو نقره صافی میانه را چه شدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دردی غم و اندیشه سیر چون نشوی</p></div>
<div class="m2"><p>جمال یار و شراب مغانه را چه شدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه سرد وجودیت گرم درپیچید</p></div>
<div class="m2"><p>به ره کنش به بهانه بهانه را چه شدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکایت ار ز زمانه کند بگو تو برو</p></div>
<div class="m2"><p>زمانه بی‌تو خوشست و زمانه را چه شدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درخت وار چرا شاخ شاخ وسوسه‌ای</p></div>
<div class="m2"><p>یگانه باش چو بیخ و یگانه را چه شدست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن ختن که در او شخص هست و صورت نیست</p></div>
<div class="m2"><p>مگو فلان چه کس است و فلانه را چه شدست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشان عشق شد این دل ز شمس تبریزی</p></div>
<div class="m2"><p>ببین ز دولت عشقش نشانه را چه شدست</p></div></div>