---
title: >-
    غزل شمارهٔ ۱۳۹۷
---
# غزل شمارهٔ ۱۳۹۷

<div class="b" id="bn1"><div class="m1"><p>زین دو هزاران من و ما ای عجبا من چه منم</p></div>
<div class="m2"><p>گوش بنه عربده را دست منه بر دهنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چونک من از دست شدم در ره من شیشه منه</p></div>
<div class="m2"><p>ور بنهی پا بنهم هر چه بیابم شکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانک دلم هر نفسی دنگ خیال تو بود</p></div>
<div class="m2"><p>گر طربی در طربم گر حزنی در حزنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلخ کنی تلخ شوم لطف کنی لطف شوم</p></div>
<div class="m2"><p>با تو خوش است ای صنم لب شکر خوش ذقنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اصل تویی من چه کسم آینه‌ای در کف تو</p></div>
<div class="m2"><p>هر چه نمایی بشوم آینه ممتحنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو به صفت سرو چمن من به صفت سایه تو</p></div>
<div class="m2"><p>چونک شدم سایه گل پهلوی گل خیمه زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌تو اگر گل شکنم خار شود در کف من</p></div>
<div class="m2"><p>ور همه خارم ز تو من جمله گل و یاسمنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم به دم از خون جگر ساغر خونابه کشم</p></div>
<div class="m2"><p>هر نفسی کوزه خود بر در ساقی شکنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست برم هر نفسی سوی گریبان بتی</p></div>
<div class="m2"><p>تا بخراشد رخ من تا بدرد پیرهنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لطف صلاح دل و دین تافت میان دل من</p></div>
<div class="m2"><p>شمع دل است او به جهان من کیم او را لگنم</p></div></div>