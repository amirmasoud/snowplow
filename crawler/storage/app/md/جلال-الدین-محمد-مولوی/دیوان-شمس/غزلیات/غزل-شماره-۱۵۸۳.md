---
title: >-
    غزل شمارهٔ ۱۵۸۳
---
# غزل شمارهٔ ۱۵۸۳

<div class="b" id="bn1"><div class="m1"><p>می خرامد جان مجلس سوی مجلس گام گام</p></div>
<div class="m2"><p>در جبینش آفتاب و در یمینش جام جام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خرامد بخت ما کو هست نقد وقت ما</p></div>
<div class="m2"><p>مشنو ای پخته از این پس وعده‌های خام خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جاء نصر الله حقا مستجیبا داعیا</p></div>
<div class="m2"><p>ان تعالوا یا کرامی و ادخلوا بین الکرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قال ان الله یدعوا اخرجوا من ضیقکم</p></div>
<div class="m2"><p>ان عقبا ملتقانا مشعر البیت الحرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترجمانش این بود کز خود برون آیید زود</p></div>
<div class="m2"><p>ور نه هر دم بند باشد هر دو گامی دام دام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خودی بیرون رویم آخر کجا در بیخودی</p></div>
<div class="m2"><p>بیخودی معنی است معنی باخودی‌ها نام نام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ان تکن اسما فاسم بالمسمی مازج</p></div>
<div class="m2"><p>لا کاسم شبه غمد و المسمی کالحسام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجلس خاص اندرآ و عام را وادان ز خاص</p></div>
<div class="m2"><p>ای درونت خاص خاص و ای برونت عام عام</p></div></div>