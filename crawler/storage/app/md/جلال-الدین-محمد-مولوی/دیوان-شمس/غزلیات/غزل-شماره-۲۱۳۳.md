---
title: >-
    غزل شمارهٔ ۲۱۳۳
---
# غزل شمارهٔ ۲۱۳۳

<div class="b" id="bn1"><div class="m1"><p>بیدار شو بیدار شو هین رفت شب بیدار شو</p></div>
<div class="m2"><p>بیزار شو بیزار شو وز خویش هم بیزار شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مصر ما یک احمقی نک می‌فروشد یوسفی</p></div>
<div class="m2"><p>باور نمی‌داری مرا اینک سوی بازار شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌چون تو را بی‌چون کند روی تو را گلگون کند</p></div>
<div class="m2"><p>خار از کفت بیرون کند وآنگه سوی گلزار شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشنو تو هر مکر و فسون خون را چرا شویی به خون</p></div>
<div class="m2"><p>همچون قدح شو سرنگون و آن گاه دردی خوار شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گردش چوگان او چون گوی شو چون گوی شو</p></div>
<div class="m2"><p>وز بهر نقل کرکسش مردار شو مردار شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد ندای آسمان آمد طبیب عاشقان</p></div>
<div class="m2"><p>خواهی که آید پیش تو بیمار شو بیمار شو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این سینه را چون غار دان خلوتگه آن یار دان</p></div>
<div class="m2"><p>گر یار غاری هین بیا در غار شو در غار شو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو مرد نیک ساده‌ای زر را به دزدان داده‌ای</p></div>
<div class="m2"><p>خواهی بدانی دزد را طرار شو طرار شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاموش وصف بحر و در کم گوی در دریای او</p></div>
<div class="m2"><p>خواهی که غواصی کنی دم دار شو دم دار شو</p></div></div>