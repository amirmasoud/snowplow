---
title: >-
    غزل شمارهٔ ۱۸۲۶
---
# غزل شمارهٔ ۱۸۲۶

<div class="b" id="bn1"><div class="m1"><p>هر کی ز حور پرسدت رخ بنما که همچنین</p></div>
<div class="m2"><p>هر کی ز ماه گویدت بام برآ که همچنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کی پری طلب کند چهره خود بدو نما</p></div>
<div class="m2"><p>هر کی ز مشک دم زند زلف گشا که همچنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کی بگویدت ز مه ابر چگونه وا شود</p></div>
<div class="m2"><p>باز گشا گره گره بند قبا که همچنین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز مسیح پرسدت مرده چگونه زنده کرد</p></div>
<div class="m2"><p>بوسه بده به پیش او جان مرا که همچنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کی بگویدت بگو کشته عشق چون بود</p></div>
<div class="m2"><p>عرضه بده به پیش او جان مرا که همچنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کی ز روی مرحمت از قد من بپرسدت</p></div>
<div class="m2"><p>ابروی خویش عرضه ده گشته دوتا که همچنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان ز بدن جدا شود باز درآید اندرون</p></div>
<div class="m2"><p>هین بنما به منکران خانه درآ که همچنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر طرفی که بشنوی ناله عاشقانه‌ای</p></div>
<div class="m2"><p>قصه ماست آن همه حق خدا که همچنین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه هر فرشته‌ام سینه کبود گشته‌ام</p></div>
<div class="m2"><p>چشم برآر و خوش نگر سوی سما که همچنین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر وصال دوست را جز به صبا نگفته‌ام</p></div>
<div class="m2"><p>تا به صفای سر خود گفت صبا که همچنین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوری آنک گوید او بنده به حق کجا رسد</p></div>
<div class="m2"><p>در کف هر یکی بنه شمع صفا که همچنین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم بوی یوسفی شهر به شهر کی رود</p></div>
<div class="m2"><p>بوی حق از جهان هو داد هوا که همچنین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم بوی یوسفی چشم چگونه وادهد</p></div>
<div class="m2"><p>چشم مرا نسیم تو داد ضیا که همچنین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از تبریز شمس دین بوک مگر کرم کند</p></div>
<div class="m2"><p>وز سر لطف برزند سر ز وفا که همچنین</p></div></div>