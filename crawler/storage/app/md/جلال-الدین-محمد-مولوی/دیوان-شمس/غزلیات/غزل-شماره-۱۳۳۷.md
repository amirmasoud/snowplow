---
title: >-
    غزل شمارهٔ ۱۳۳۷
---
# غزل شمارهٔ ۱۳۳۷

<div class="b" id="bn1"><div class="m1"><p>الا ای رو ترش کرده که تا نبود مرا مدخل</p></div>
<div class="m2"><p>نبشته گرد روی خود صلا نعم الادام الخل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو سه گام ار ز حرص و کین به حلم آیی عسل جوشی</p></div>
<div class="m2"><p>که عالم‌ها کنی شیرین نمی‌آیی زهی کاهل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلط دیدم غلط گفتم همیشه با غلط جفتم</p></div>
<div class="m2"><p>که گر من دیدمی رویت نماندی چشم من احول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا خود را در آیینه چو کژ بینی هرآیینه</p></div>
<div class="m2"><p>تو کژ باشی نه آیینه تو خود را راست کن اول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی می‌رفت در چاهی چو در چه دید او ماهی</p></div>
<div class="m2"><p>مه از گردون ندا کردش من این سویم تو لاتعجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجو مه را در این پستی که نبود در عدم هستی</p></div>
<div class="m2"><p>نروید نیشکر هرگز چو کارد آدمی حنظل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشی در نفی تست ای جان تو در اثبات می‌جویی</p></div>
<div class="m2"><p>از آن جا جو که می‌آید نگردد مشکل این جا حل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو آن بطی کز اشتابی ستاره جست در آبی</p></div>
<div class="m2"><p>تو آنی کز برای پا همی‌زد او رگ اکحل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در این پایان در این ساران چو گم گشتند هشیاران</p></div>
<div class="m2"><p>چه سازم من که من در ره چنان مستم که لاتسأل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خدایا دست مست خود بگیر ار نی در این مقصد</p></div>
<div class="m2"><p>ز مستی آن کند با خود که در مستی کند منبل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرم زیر و زبر کردی به خود نزدیکتر کردی</p></div>
<div class="m2"><p>که صحت آید از دردی چو افشرده شود دنبل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بعد این می و مستی چو کار من تو کردستی</p></div>
<div class="m2"><p>توکل کرده‌ام بر تو صلا ای کاهلان تنبل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تویی ای شمس تبریزی نه زین مشرق نه زین مغرب</p></div>
<div class="m2"><p>نه آن شمسی که هر باری کسوف آید شود مختل</p></div></div>