---
title: >-
    غزل شمارهٔ ۱۷۱۷
---
# غزل شمارهٔ ۱۷۱۷

<div class="b" id="bn1"><div class="m1"><p>لولیکان توییم در بگشا ای صنم</p></div>
<div class="m2"><p>لولیکان را دمی بار ده ای محتشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای تو امان جهان ای تو جهان را چو جان</p></div>
<div class="m2"><p>ای شده خندان دهان از کرمت دم به دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امن دو عالم تویی گوهر آدم تویی</p></div>
<div class="m2"><p>هین که رسید از حبش بر سر کوی حشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون برسد کوس تو کمتر جاسوس تو</p></div>
<div class="m2"><p>گردد هر لولیی صاحب طبل و علم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رایت نصرت فرست لشکر عشرت فرست</p></div>
<div class="m2"><p>تا که ز شادی ما جان نبرد هیچ غم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ عرب برکنیم بر سر ترکان زنیم</p></div>
<div class="m2"><p>چون لطفت برکشد بر خط لولی رقم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوف مهل در میان بانگ بزن کالامان</p></div>
<div class="m2"><p>عشرت با خوف جان راست نیاید به هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر برآور به جوش وز دل چنگ آن خروش</p></div>
<div class="m2"><p>پر کن از عیش گوش پر کن از می شکم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا سوی تبریز جان جانب شمس الزمان</p></div>
<div class="m2"><p>آید صافی روان گوید ای من منم</p></div></div>