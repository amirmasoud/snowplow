---
title: >-
    غزل شمارهٔ ۱۹۸۵
---
# غزل شمارهٔ ۱۹۸۵

<div class="b" id="bn1"><div class="m1"><p>صنما بیار باده بنشان خمار مستان</p></div>
<div class="m2"><p>که ببرد عشق رویت همگی قرار مستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کهنه را کشان کن به صبوح گلستان کن</p></div>
<div class="m2"><p>که به جوش اندرآمد فلک از عقار مستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بده آن قرار جان را گل و لاله زار جان را</p></div>
<div class="m2"><p>ز نبات و قند پر کن دهن و کنار مستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدحی به دست برنه به کف شکرلبان ده</p></div>
<div class="m2"><p>بنشان به آب رحمت به کرم غبار مستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صنما به چشم مستت دل و جان غلام دستت</p></div>
<div class="m2"><p>به می خوشی که هستت ببر اختیار مستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شراب لاله رنگت به دماغ‌ها برآید</p></div>
<div class="m2"><p>گل سرخ شرم دارد ز رخ و عذار مستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو جناح و قلب مجلس ز شراب یافت مونس</p></div>
<div class="m2"><p>ببرد گلوی غم را سر ذوالفقار مستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صنما تو روز مایی غم و غصه سوز مایی</p></div>
<div class="m2"><p>ز تو است ای معلا همه کار و بار مستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکشان تو گوش شیران چو شتر قطارشان کن</p></div>
<div class="m2"><p>که تو شیرگیر حقی به کفت مهار مستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز عقیق جام داری نمکی تمام داری</p></div>
<div class="m2"><p>چه غریب دام داری جهت شکار مستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخنی بماند جانی که تو بی‌بیان بدانی</p></div>
<div class="m2"><p>که تو رشک ساقیانی سر و افتخار مستان</p></div></div>