---
title: >-
    غزل شمارهٔ ۳۰۰۵
---
# غزل شمارهٔ ۳۰۰۵

<div class="b" id="bn1"><div class="m1"><p>آن دل که گم شده‌ست هم از جان خویش جوی</p></div>
<div class="m2"><p>آرام جان خویش ز جانان خویش جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر شکر نیابی ذوق نبات غیب</p></div>
<div class="m2"><p>آن ذوق را هم از لب و دندان خویش جوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو چشم را تو ناظر هر بی‌نظر مکن</p></div>
<div class="m2"><p>در ناظری گریز و ازو آن خویش جوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقلست از رسول که مردم معادنند</p></div>
<div class="m2"><p>پس نقد خویش را برو از کان خویش جوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تخت تن برون رو و بر تخت جان نشین</p></div>
<div class="m2"><p>از آسمان گذر کن و کیوان خویش جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برقی که بر دلت زد و دل بی‌قرار شد</p></div>
<div class="m2"><p>آن برق را در اشک چو باران خویش جوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انبان بوهریره وجود توست و بس</p></div>
<div class="m2"><p>هر چه مراد توست در انبان خویش جوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بی‌نشان محض نشان از کی جویمت</p></div>
<div class="m2"><p>هم تو بجو مرا و به احسان خویش جوی</p></div></div>