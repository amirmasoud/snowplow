---
title: >-
    غزل شمارهٔ ۱۰۵۰
---
# غزل شمارهٔ ۱۰۵۰

<div class="b" id="bn1"><div class="m1"><p>کی باشد اختری در اقطار</p></div>
<div class="m2"><p>در برج چنین مهی گرفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آواره شده ز کفر و ایمان</p></div>
<div class="m2"><p>اقرار به پیش او چو انکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس دید دلی که دل ندارد</p></div>
<div class="m2"><p>با جان فنا به تیغ جان دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من دیدم اگر کسی ندیدست</p></div>
<div class="m2"><p>زیرا که مرا نمود دیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم و عملم قبول او بس</p></div>
<div class="m2"><p>ای من ز جز این قبول بیزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خواب شبم ببست آن شه</p></div>
<div class="m2"><p>بخشید وصال و بخت بیدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این وصل به از هزار خوابست</p></div>
<div class="m2"><p>از خواب مکن تو یاد زنهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گریه خود چه داند آن طفل</p></div>
<div class="m2"><p>کاندر دل‌ها چه دارد آثار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌گرید بی‌خبر ولیکن</p></div>
<div class="m2"><p>صد چشمه شیر از او در اسرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگری تو اگر اثر ندانی</p></div>
<div class="m2"><p>کز گریه تست خلد و انهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امشب کر و فر شهریاریش</p></div>
<div class="m2"><p>اندر ده ماست شاه و سالار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نی خواب رها کند نه آرام</p></div>
<div class="m2"><p>آن صبح صفا و شیر کرار</p></div></div>