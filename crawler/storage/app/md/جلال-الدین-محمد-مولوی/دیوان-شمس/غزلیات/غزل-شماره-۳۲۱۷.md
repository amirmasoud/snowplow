---
title: >-
    غزل شمارهٔ ۳۲۱۷
---
# غزل شمارهٔ ۳۲۱۷

<div class="b" id="bn1"><div class="m1"><p>قالت الکأس ارفعونی کم تحبسونی</p></div>
<div class="m2"><p>ان جسمی فی زجاج بالنوی لا تکسرونی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجعلوا الساقی خبیرا عارفا عنه سلونی</p></div>
<div class="m2"><p>اننی لست احب المفتری لا تظلمونی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فاذا انتم سکرتم فوق السکر سکرا</p></div>
<div class="m2"><p>فاقرعوا باب‌التقاضی واسألوا لا تقنطونی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنت فی سیر خفی صورتی فی‌ذالسکون</p></div>
<div class="m2"><p>خلتمنی کالجماد ذاک من نکس العیون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ان اردتم انتعاشا فاتقوا مکرالظنون</p></div>
<div class="m2"><p>ان نکستم فاستقیموا واحذروا ریب‌المنون</p></div></div>