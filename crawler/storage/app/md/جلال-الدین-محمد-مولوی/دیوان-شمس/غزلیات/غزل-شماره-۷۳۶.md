---
title: >-
    غزل شمارهٔ ۷۳۶
---
# غزل شمارهٔ ۷۳۶

<div class="b" id="bn1"><div class="m1"><p>گر یکی شاخی شکستم من ز گلزاری چه شد</p></div>
<div class="m2"><p>ور ز سرمستی کشیدم زلف دلداری چه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بزد ناداشت زخمی از سر مستی چه باک</p></div>
<div class="m2"><p>ور ز طراری ربودم رخت طراری چه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور یکی زنبیل کم شد از همه بغداد چیست</p></div>
<div class="m2"><p>ور یکی دانه برون آمد ز انباری چه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای فلک تا چند از این دستان و مکاری تو</p></div>
<div class="m2"><p>گر یکی دم خوش نشیند یار با یاری چه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوییم از سر او ناگفتنی‌ها گفته‌ای</p></div>
<div class="m2"><p>چند گویی چند گویی گفته‌ام آری چه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر میان عاشق و معشوق کاری رفت رفت</p></div>
<div class="m2"><p>تو نه معشوقی نه عاشق مر تو را باری چه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از لب لعلش چه کم شد گر لبش لطفی نمود</p></div>
<div class="m2"><p>ور ز عیسی عافیت یابید بیماری چه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر براتست امشب و هر کس براتی یافتند</p></div>
<div class="m2"><p>بی خطی گر پیشم آید ماه رخساری چه شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریزی اگر من از جنون عشق تو</p></div>
<div class="m2"><p>برشکستم عاشقان را کار و بازاری چه شد</p></div></div>