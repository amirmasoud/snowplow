---
title: >-
    غزل شمارهٔ ۱۵۹۲
---
# غزل شمارهٔ ۱۵۹۲

<div class="b" id="bn1"><div class="m1"><p>نی تو گفتی از جفای آن جفاگر نشکنم</p></div>
<div class="m2"><p>نی تو گفتی عالمی در عشق او برهم زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی تو دست او گرفتی عهد کردی دو به دو</p></div>
<div class="m2"><p>کز پی آن جان و دل این جان و دل را برکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور چشمت چون منم دورم مبین ای نور چشم</p></div>
<div class="m2"><p>سوی بالا بنگر آخر زانک من بر روزنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سررشته طرب‌ها عیسی دوران تویی</p></div>
<div class="m2"><p>سر از این روزن فروکن گر چه من چون سوزنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق را روز قیامت آتش و دودی بود</p></div>
<div class="m2"><p>نور آن آتش تو باشی دود آن آتش منم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نبینم روی چون گلزار آن صد نوبهار</p></div>
<div class="m2"><p>همچو لاله من سیه دل صدزبان چون سوسنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه شمس الدین تبریزی منت عاشق بسم</p></div>
<div class="m2"><p>روز بزمت همچو مومم روز رزمت آهنم</p></div></div>