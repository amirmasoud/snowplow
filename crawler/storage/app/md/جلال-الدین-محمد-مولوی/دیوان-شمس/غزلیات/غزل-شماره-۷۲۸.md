---
title: >-
    غزل شمارهٔ ۷۲۸
---
# غزل شمارهٔ ۷۲۸

<div class="b" id="bn1"><div class="m1"><p>دشمن خویشیم و یار آنک ما را می‌کشد</p></div>
<div class="m2"><p>غرق دریاییم و ما را موج دریا می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان چنین خندان و خوش ما جان شیرین می‌دهیم</p></div>
<div class="m2"><p>کان ملک ما را به شهد و قند و حلوا می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویش فربه می‌نماییم از پی قربان عید</p></div>
<div class="m2"><p>کان قصاب عاشقان بس خوب و زیبا می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بلیس بی‌تبش مهلت همی‌خواهد از او</p></div>
<div class="m2"><p>مهلتی دادش که او را بعد فردا می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو اسماعیل گردن پیش خنجر خوش بنه</p></div>
<div class="m2"><p>درمدزد از وی گلو گر می‌کشد تا می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست عزرائیل را دست و رهی بر عاشقان</p></div>
<div class="m2"><p>عاشقان عشق را هم عشق و سودا می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتگان نعره زنان یا لیت قومی یعلمون</p></div>
<div class="m2"><p>خفیه صد جان می‌دهد دلدار و پیدا می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زمین کالبد برزن سری وانگه ببین</p></div>
<div class="m2"><p>کو تو را بر آسمان بر می‌کشد یا می‌کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روح ریحی می‌ستاند راح روحی می‌دهد</p></div>
<div class="m2"><p>باز جان را می‌رهاند جغد غم را می‌کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن گمان ترسا برد مؤمن ندارد آن گمان</p></div>
<div class="m2"><p>کو مسیح خویشتن را بر چلیپا می‌کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر یکی عاشق چو منصورند خود را می‌کشند</p></div>
<div class="m2"><p>غیر عاشق وانما که خویش عمدا می‌کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صد تقاضا می‌کند هر روز مردم را اجل</p></div>
<div class="m2"><p>عاشق حق خویشتن را بی‌تقاضا می‌کشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس کنم یا خود بگویم سر مرگ عاشقان</p></div>
<div class="m2"><p>گر چه منکر خویش را از خشم و صفرا می‌کشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمس تبریزی برآمد بر افق چون آفتاب</p></div>
<div class="m2"><p>شمع‌های اختران را بی‌محابا می‌کشد</p></div></div>