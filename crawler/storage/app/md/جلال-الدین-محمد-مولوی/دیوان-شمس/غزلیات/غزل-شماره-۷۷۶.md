---
title: >-
    غزل شمارهٔ ۷۷۶
---
# غزل شمارهٔ ۷۷۶

<div class="b" id="bn1"><div class="m1"><p>عاشقان بر درت از اشک چو باران کارند</p></div>
<div class="m2"><p>خوش به هر قطره دو صد گوهر جان بردارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه از کار از آن روی معطل شده‌اند</p></div>
<div class="m2"><p>چو از آن سر نگری موی به مو در کارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه بی‌دست و دهانند درختان چمن</p></div>
<div class="m2"><p>لیک سرسبز و فزاینده و دردی خوارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد هزارند ولیکن همه یک نور شوند</p></div>
<div class="m2"><p>شمع‌ها یک صفتند ار به عدد بسیارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نورهاشان به هم اندرشده بی‌حد و قیاس</p></div>
<div class="m2"><p>چون برآید مه تو جمله به تو بسپارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم‌هاشان همه وامانده در بحر محیط</p></div>
<div class="m2"><p>لب فروبسته از آن موج که در سر دارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بسا جان سلیمان نهان همچو پری</p></div>
<div class="m2"><p>که به لشکرگهشان مور نمی‌آزارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست اندر پس دل واقف از این جاسوسی</p></div>
<div class="m2"><p>کو بگوید همه اسرار گرش بفشارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی کلیدیست که چون حلقه ز در بیرونند</p></div>
<div class="m2"><p>ور نه هر جزو از آن نقده کل انبارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این بدن تخت شه و چار طبایع پایش</p></div>
<div class="m2"><p>تاجداران فلک تخت به تو نگذارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمس تبریز اگر تاج بقا می‌بخشد</p></div>
<div class="m2"><p>دل و جان را تو بشارت ده اگر بیدارند</p></div></div>