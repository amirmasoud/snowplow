---
title: >-
    غزل شمارهٔ ۱۹۷۵
---
# غزل شمارهٔ ۱۹۷۵

<div class="b" id="bn1"><div class="m1"><p>عشق شمس الدین است یا نور کف موسی است آن</p></div>
<div class="m2"><p>این خیال شمس دین یا خود دو صد عیسی است آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر همه معنی است پس این چهره چون ماه چیست</p></div>
<div class="m2"><p>صورتش چون گویم آخر چون همه معنی است آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواه این و خواه آن باری از آن فتنه لبش</p></div>
<div class="m2"><p>جان ما رقصان و خوش سرمست و سودایی است آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک بنگر در رخ من در فراق جان جان</p></div>
<div class="m2"><p>بی دل و جان می نویسد گر چه در انشی است آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چه گویم خود عطارد با همه جان‌های پاک</p></div>
<div class="m2"><p>از برای پاکی او عاشق املی است آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان من همچون عصا چون دستبوس او بیافت</p></div>
<div class="m2"><p>پس چو موسی درفکندش جان کنون افعی است آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده من در فراق دولت احیای او</p></div>
<div class="m2"><p>در میان خندان شده در قدرت مولی است آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرک او اندر رکاب شاه شمس الدین دوید</p></div>
<div class="m2"><p>فارغ از دنیا و عقبی آخر و اولی است آن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و آنک او بوسید دستش خود چه گویم بهر او</p></div>
<div class="m2"><p>عاقلان دانند کان خود در شرف اولی است آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جسم او چون دید جانم زود ایمان تازه کرد</p></div>
<div class="m2"><p>گفتمش چه گفت بنگر معجزه کبری است آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فر تبریز است از فر و جمال آن رخی</p></div>
<div class="m2"><p>کان غبین و حسرت صد آزر و مانی است آن</p></div></div>