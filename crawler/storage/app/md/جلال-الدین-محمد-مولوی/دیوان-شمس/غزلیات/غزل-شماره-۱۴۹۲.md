---
title: >-
    غزل شمارهٔ ۱۴۹۲
---
# غزل شمارهٔ ۱۴۹۲

<div class="b" id="bn1"><div class="m1"><p>بار دگر از راه سوی چاه رسیدیم</p></div>
<div class="m2"><p>وز غربت اجسام بالله رسیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اسب بدان شاه کسی چون نرسیده‌ست</p></div>
<div class="m2"><p>ما اسب بدادیم و بدان شاه رسیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ابر بسی اشک در این خاک فشاندیم</p></div>
<div class="m2"><p>وز ابر گذشتیم و بدان ماه رسیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طبل زنان نوبت ما گشت بکوبید</p></div>
<div class="m2"><p>وی ترک برون آ که به خرگاه رسیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک چند چو یوسف به بن چاه نشستیم</p></div>
<div class="m2"><p>زان سر رسن آمد به سر چاه رسیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما چند صنم پیش محمد بشکستیم</p></div>
<div class="m2"><p>تا در صنم دلبر دلخواه رسیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزدیکتر آیید که از دور رسیدیم</p></div>
<div class="m2"><p>و احوال بپرسید که از راه رسیدیم</p></div></div>