---
title: >-
    غزل شمارهٔ ۲۹۰۳
---
# غزل شمارهٔ ۲۹۰۳

<div class="b" id="bn1"><div class="m1"><p>باوفا یارا جفا آموختی</p></div>
<div class="m2"><p>این جفا را از کجا آموختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو وفاهای لطیفت کز نخست</p></div>
<div class="m2"><p>در شکار جان ما آموختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا زشتی جفاکاری رسید</p></div>
<div class="m2"><p>خوبیش دادی وفا آموختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل از عالم چنین بیگانگی</p></div>
<div class="m2"><p>هم ز یار آشنا آموختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانت گر خواهد صنم گویی بلی</p></div>
<div class="m2"><p>این بلی را زان بلا آموختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق را گفتم فروخوردی مرا</p></div>
<div class="m2"><p>این مگر از اژدها آموختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن عصای موسی اژدرها بخورد</p></div>
<div class="m2"><p>تو مگر هم زان عصا آموختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل ار از غمزه‌اش خسته شدی</p></div>
<div class="m2"><p>از لبش آخر دوا آموختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر هشتی و شکایت می‌کنی</p></div>
<div class="m2"><p>از یکی باری خطا آموختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان شکرخانه مگو الا که شکر</p></div>
<div class="m2"><p>آن چنان کز انبیا آموختی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این صفا را از گله تیره مکن</p></div>
<div class="m2"><p>کاین صفا از مصطفی آموختی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چه خلق آموختت زان لب ببند</p></div>
<div class="m2"><p>جمله آن شو کز خدا آموختی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاشقا از شمس تبریزی چو ابر</p></div>
<div class="m2"><p>سوختی لیکن ضیا آموختی</p></div></div>