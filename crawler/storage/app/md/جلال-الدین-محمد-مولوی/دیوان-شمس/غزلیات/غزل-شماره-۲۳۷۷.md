---
title: >-
    غزل شمارهٔ ۲۳۷۷
---
# غزل شمارهٔ ۲۳۷۷

<div class="b" id="bn1"><div class="m1"><p>ای خداوند یکی یار جفاکارش ده</p></div>
<div class="m2"><p>دلبری عشوه ده سرکش خون خوارش ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بداند که شب ما به چه سان می‌گذرد</p></div>
<div class="m2"><p>غم عشقش ده و عشقش ده و بسیارش ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند روزی جهت تجربه بیمارش کن</p></div>
<div class="m2"><p>با طبیبی دغلی پیشه سر و کارش ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببرش سوی بیابان و کن او را تشنه</p></div>
<div class="m2"><p>یک سقایی حجری سینه سبکسارش ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گمرهش کن که ره راست نداند سوی شهر</p></div>
<div class="m2"><p>پس قلاوز کژ بیهده رفتارش ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم از سرکشی آن مه سرگشته شدند</p></div>
<div class="m2"><p>مدتی گردش این گنبد دوارش ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو صیادی که همی‌کرد دل ما را پار</p></div>
<div class="m2"><p>زو ببر سنگ دلی و دل پیرارش ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منکر پار شده‌ست او که مرا یاد نماند</p></div>
<div class="m2"><p>ببر انکار از او و دم اقرارش ده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم آخر به نشانی که به دربان گفتی</p></div>
<div class="m2"><p>که فلانی چو بیاید بر ما بارش ده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت آمد که مرا خواجه ز بالا گیرد</p></div>
<div class="m2"><p>رو بجو همچو خودی ابله و آچارش ده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس کن ای ساقی و کس را چو رهی مست مکن</p></div>
<div class="m2"><p>ور کنی مست بدین حد ره هموارش ده</p></div></div>