---
title: >-
    غزل شمارهٔ ۱۴۰۰
---
# غزل شمارهٔ ۱۴۰۰

<div class="b" id="bn1"><div class="m1"><p>تیز دوم تیز دوم تا به سواران برسم</p></div>
<div class="m2"><p>نیست شوم نیست شوم تا بر جانان برسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش شده‌ام خوش شده‌ام پاره آتش شده‌ام</p></div>
<div class="m2"><p>خانه بسوزم بروم تا به بیابان برسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک شوم خاک شوم تا ز تو سرسبز شوم</p></div>
<div class="m2"><p>آب شوم سجده کنان تا به گلستان برسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونک فتادم ز فلک ذره صفت لرزانم</p></div>
<div class="m2"><p>ایمن و بی‌لرز شوم چونک به پایان برسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ بود جای شرف خاک بود جای تلف</p></div>
<div class="m2"><p>بازرهم زین دو خطر چون بر سلطان برسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم این خاک و هوا گوهر کفر است و فنا</p></div>
<div class="m2"><p>در دل کفر آمده‌ام تا که به ایمان برسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن شه موزون جهان عاشق موزون طلبد</p></div>
<div class="m2"><p>شد رخ من سکه زر تا که به میزان برسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحمت حق آب بود جز که به پستی نرود</p></div>
<div class="m2"><p>خاکی و مرحوم شوم تا بر رحمان برسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ طبیبی ندهد بی‌مرضی حب و دوا</p></div>
<div class="m2"><p>من همگی درد شوم تا که به درمان برسم</p></div></div>