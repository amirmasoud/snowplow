---
title: >-
    غزل شمارهٔ ۶۷۱
---
# غزل شمارهٔ ۶۷۱

<div class="b" id="bn1"><div class="m1"><p>دلم را ناله سرنای باید</p></div>
<div class="m2"><p>که از سرنای بوی یار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان خواهم نوای عاشقانه</p></div>
<div class="m2"><p>کز آن ناله جمال جان نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی‌نالم که از غم بار دارم</p></div>
<div class="m2"><p>عجب این جان نالان تا چه زاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگو ای نای حال عاشقان را</p></div>
<div class="m2"><p>که آواز تو جان می‌آزماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین ای جان من کز بانگ طاسی</p></div>
<div class="m2"><p>مه بگرفته چون وا می‌گشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخوان بر سینه دل این عزیمت</p></div>
<div class="m2"><p>که تا فریاد از پریان برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ناله مونس رنجور گردد</p></div>
<div class="m2"><p>گرش گویی خمش کن هم نشاید</p></div></div>