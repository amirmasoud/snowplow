---
title: >-
    غزل شمارهٔ ۲۴۰۷
---
# غزل شمارهٔ ۲۴۰۷

<div class="b" id="bn1"><div class="m1"><p>زهی لواء و علم لا اله الا الله</p></div>
<div class="m2"><p>که زد بر اوج قدم لا اله الا الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه گرد برآورد شاه موسی وار</p></div>
<div class="m2"><p>ز بحر هست و عدم لا اله الا الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستاده‌اند صفات صفا ز خجلت او</p></div>
<div class="m2"><p>به پیش او به قدم لا اله الا الله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی ستم ز وی از صد هزار عدل به است</p></div>
<div class="m2"><p>زهی خوشی ستم لا اله الا الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هر طرف که نظر کرد می برویاند</p></div>
<div class="m2"><p>هزار باغ ارم لا اله الا الله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بحر غم به کناری رسم عجب روزی</p></div>
<div class="m2"><p>ز موج لطف و کرم لا اله الا الله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد از شه من هیچ بوی جان آن کس</p></div>
<div class="m2"><p>که ببینیش تو به غم لا اله الا الله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دیده کحل نپذرفت از شه تبریز</p></div>
<div class="m2"><p>زهی دریغ و ندم لا اله الا الله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآید از دل و از جان الست شه شنود</p></div>
<div class="m2"><p>هزار بانگ نعم لا اله الا الله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهشت لطف و بلندی خدیو شمس الدین</p></div>
<div class="m2"><p>زهی شفای سقم لا اله الا الله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلم طواف به تبریز می‌کند محرم</p></div>
<div class="m2"><p>در آن حریم حرم لا اله الا الله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی خوشی که بگویم که کیست هان بر در</p></div>
<div class="m2"><p>بگوید او که منم لا اله الا الله</p></div></div>