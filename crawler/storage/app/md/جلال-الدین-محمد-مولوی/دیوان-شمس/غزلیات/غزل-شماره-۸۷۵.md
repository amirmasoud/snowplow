---
title: >-
    غزل شمارهٔ ۸۷۵
---
# غزل شمارهٔ ۸۷۵

<div class="b" id="bn1"><div class="m1"><p>گر عید وصل تست منم خود غلام عید</p></div>
<div class="m2"><p>بهر تست خدمت و سجده و سلام عید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نام تو شنیدم شد سرد بر دلم</p></div>
<div class="m2"><p>از غایت حلاوت نام تو نام عید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شاد آن زمان که درآید وصال تو</p></div>
<div class="m2"><p>تا ما ز گنج وصل تو بدهیم وام عید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا آفتاب چهره زیبات دررسید</p></div>
<div class="m2"><p>صبحی شود ز صبح جمال تو شام عید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در یمن و در سعادت و در بخت و در صفا</p></div>
<div class="m2"><p>ای پرتو خیال تو بوده امام عید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای سجده‌ها به پیش درت واجبات عید</p></div>
<div class="m2"><p>وی دیده خویشتن ز تو قایم خرام عید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام شراب وصل تو پر کن ز فضل خود</p></div>
<div class="m2"><p>تا کام جان روا شود از جام و کام عید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر رکاب تو چو روان‌ها روا شوند</p></div>
<div class="m2"><p>در وی کجا رسد به دو صد سال گام عید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آمد ز گرد راه تو این عید و مژده داد</p></div>
<div class="m2"><p>جانم دوید پیش و گرفته لگام عید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانست کز خدیو اجل شمس دین بود</p></div>
<div class="m2"><p>این فرو این جلالت و این لطف عام عید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیکن کجاست فر و جمال تو بی‌نظیر</p></div>
<div class="m2"><p>خود کی شوند دلشدگان تو رام عید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تبریز با شراب چنان صدر نامدار</p></div>
<div class="m2"><p>بر تو حرام باشد بی‌شبهه تو جام عید</p></div></div>