---
title: >-
    غزل شمارهٔ ۲۰۶
---
# غزل شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>ای همه خوبی تو را پس تو که رایی که را</p></div>
<div class="m2"><p>ای گل در باغ ما پس تو کجایی کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوسن با صد زبان از تو نشانم نداد</p></div>
<div class="m2"><p>گفت رو از من مجو غیر دعا و ثنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کف تو ای قمر باغ دهان پرشکر</p></div>
<div class="m2"><p>وز کف تو بی‌خبر با همه برگ و نوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو اگر سر کشید در قد تو کی رسید</p></div>
<div class="m2"><p>نرگس اگر چشم داشت هیچ ندید او تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ اگر خطبه خواند شاخ اگر گل فشاند</p></div>
<div class="m2"><p>سبزه اگر تیز راند هیچ ندارد دوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرب گل از ابر بود شرب دل از صبر بود</p></div>
<div class="m2"><p>ابر حریف گیاه صبر حریف صبا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر طرفی صف زده مردم و دیو و دده</p></div>
<div class="m2"><p>لیک در این میکده پای ندارند پا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر طرفی‌ام بجو هر چه بخواهی بگو</p></div>
<div class="m2"><p>ره نبری تار مو تا ننمایم هدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرم شود روی آب از تپش آفتاب</p></div>
<div class="m2"><p>باز همش آفتاب برکشد اندر علا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بربردش خرد خرد تا که ندانی چه برد</p></div>
<div class="m2"><p>صاف بدزدد ز درد شعشعه دلربا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین سخن بوالعجب بستم من هر دو لب</p></div>
<div class="m2"><p>لیک فلک جمله شب می‌زندت الصلا</p></div></div>