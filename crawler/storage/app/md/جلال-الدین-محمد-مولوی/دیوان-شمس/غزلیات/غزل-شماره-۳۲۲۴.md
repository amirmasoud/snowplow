---
title: >-
    غزل شمارهٔ ۳۲۲۴
---
# غزل شمارهٔ ۳۲۲۴

<div class="b" id="bn1"><div class="m1"><p>یا ویح نفسنا بفوات الفضائل</p></div>
<div class="m2"><p>یا ویل روحنا بفسادالوسائل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد حن واشتکی فلذا الصخر بکیا</p></div>
<div class="m2"><p>علی علی هجران فخرالقبایل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لو ان فراقی حمل‌الطور والصفا</p></div>
<div class="m2"><p>زمانا یسیرا هدمت بالزلازل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لو ان شرارا من هوانا تبلجت</p></div>
<div class="m2"><p>علی ظاهری احرقت کل العواذل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لو ان قلیلا من جمالک اثرت</p></div>
<div class="m2"><p>علی‌البر لم توحش فلا بالقوافل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحق وصال نورالقلب فضله</p></div>
<div class="m2"><p>بنور نای عن درکه کل فاضل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و حرمهٔ اسرار جرت و لطایف</p></div>
<div class="m2"><p>کنیت بها سرا و لست بقایل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و جودک و النعماء ما لم تسمه</p></div>
<div class="m2"><p>لسانی و قلبی عنه لیس بزائل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تجود بوصل مشرق باهر نری</p></div>
<div class="m2"><p>به جملة حاجاتنا و المسائل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فانی لا اسطاع زورة زایر</p></div>
<div class="m2"><p>بجفنین مقروحین در الهوامل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ارید ترابا من تراب فنائه</p></div>
<div class="m2"><p>مدبر نورالعین منی و کاحل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اکل ثری تبریز مثل ترابه</p></div>
<div class="m2"><p>فلا کان جسم قال روحی ممائلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلا زال شمس الدین مولا و سیدا</p></div>
<div class="m2"><p>و ذو منة فی ذمتی و هو کافلی</p></div></div>