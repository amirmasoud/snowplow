---
title: >-
    غزل شمارهٔ ۲۸۹۹
---
# غزل شمارهٔ ۲۸۹۹

<div class="b" id="bn1"><div class="m1"><p>بار دیگر عزم رفتن کرده‌ای</p></div>
<div class="m2"><p>بار دیگر دل چو آهن کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی چراغ عشرت ما را مکش</p></div>
<div class="m2"><p>در چراغ ما تو روغن کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الله الله کاین جهان از روی خود</p></div>
<div class="m2"><p>پرگل و نسرین و سوسن کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الله الله تا نگوید دشمنی</p></div>
<div class="m2"><p>دوستی و کار دشمن کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الله الله بندگان را جمع دار</p></div>
<div class="m2"><p>ای که عالم را تو روشن کرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار دیگر تو به یک سو می‌نهی</p></div>
<div class="m2"><p>عشقبازی‌ها که با من کرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الله الله کز نثار آستین</p></div>
<div class="m2"><p>نفس بد را پاکدامن کرده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کان زرکوبان صلاح الدین که تو</p></div>
<div class="m2"><p>همچو مه از سیم خرمن کرده‌ای</p></div></div>