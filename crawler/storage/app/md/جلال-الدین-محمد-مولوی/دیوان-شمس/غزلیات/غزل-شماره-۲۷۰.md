---
title: >-
    غزل شمارهٔ ۲۷۰
---
# غزل شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>افدی قمرا لاح علینا و تلالا</p></div>
<div class="m2"><p>ما احسنه رب تبارک و تعالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد حل بروحی فتضاعفت حیاه</p></div>
<div class="m2"><p>و الیوم نای عنی عزا و جلالا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ادعوه سرارا و انادیه جهارا</p></div>
<div class="m2"><p>ان ابدلنی الصبوه طیفا و خیالا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لو قطعنی دهری لا زلت انادی</p></div>
<div class="m2"><p>کی تخترق الجب و یروین وصالا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لا مل من العشق و لو مر قرون</p></div>
<div class="m2"><p>حاشاه ملالا بی‌حاشای ملالا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>العاشق حوت و هوی العشق کنجر</p></div>
<div class="m2"><p>هل مل اذا ما سکن الحوت زلالا</p></div></div>