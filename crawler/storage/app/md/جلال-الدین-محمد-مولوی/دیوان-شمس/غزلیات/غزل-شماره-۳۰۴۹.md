---
title: >-
    غزل شمارهٔ ۳۰۴۹
---
# غزل شمارهٔ ۳۰۴۹

<div class="b" id="bn1"><div class="m1"><p>ربود عقل و دلم را جمال آن عربی</p></div>
<div class="m2"><p>درون غمزه مستش هزار بوالعجبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار عقل و ادب داشتم من ای خواجه</p></div>
<div class="m2"><p>کنون چو مست و خرابم صلای بی‌ادبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسبب سبب این جا در سبب بربست</p></div>
<div class="m2"><p>تو آن ببین که سبب می‌کشد ز بی‌سببی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پریر رفتم سرمست بر سر کویش</p></div>
<div class="m2"><p>به خشم گفت چه گم کرده‌ای چه می‌طلبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکسته بسته بگفتم یکی دو لفظ عرب</p></div>
<div class="m2"><p>اتیت اطلب فی حیکم مقام ابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جواب داد کجا خفته‌ای چه می‌جویی</p></div>
<div class="m2"><p>به پیش عقل محمد پلاس بولهبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عجز خوردم سوگندها و گرم شدم</p></div>
<div class="m2"><p>به ذات پاک خدا و به جان پاک نبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه جای گرمی و سوگند پیش آن بینا</p></div>
<div class="m2"><p>و کیف یصرع صقر بصوله الخرب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روان شد اشک ز چشم من و گواهی داد</p></div>
<div class="m2"><p>کما یسیل میاه السقا من القرب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه چاره دارم غماز من هم از خانه‌ست</p></div>
<div class="m2"><p>رخم چو سکه زر آب دیده‌ام سحبی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دریغ دلبر جان را به مال میل بدی</p></div>
<div class="m2"><p>و یا فریفته گشتی به سیدی چلبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و یا به حیله و مکری ز ره درافتادی</p></div>
<div class="m2"><p>و یا که مست شدی او ز باده عنبی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهان به گوش من آرد به گاه نومیدی</p></div>
<div class="m2"><p>چه می‌کند سر و گوش مرا به شهد لبی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غلام ساعت نومیدیم که آن ساعت</p></div>
<div class="m2"><p>شراب وصل بتابد ز شیشه‌ای حلبی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن شراب پرستم که یار می بخشست</p></div>
<div class="m2"><p>رخم چو شیشه می کرد و بود رخ ذهبی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برادرم پدرم اصل و فصل من عشقست</p></div>
<div class="m2"><p>که خویش عشق بماند نه خویشی نسبی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خمش که مفخر آفاق شمس تبریزی</p></div>
<div class="m2"><p>بشست نام و نشان مرا به خوش لقبی</p></div></div>