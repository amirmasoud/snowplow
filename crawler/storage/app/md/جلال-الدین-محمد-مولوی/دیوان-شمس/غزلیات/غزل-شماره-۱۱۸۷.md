---
title: >-
    غزل شمارهٔ ۱۱۸۷
---
# غزل شمارهٔ ۱۱۸۷

<div class="b" id="bn1"><div class="m1"><p>در این سرما سر ما داری امروز</p></div>
<div class="m2"><p>دل عیش و تماشا داری امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میفکن نوبت عشرت به فردا</p></div>
<div class="m2"><p>چو آسایش مهیا داری امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگستر بر سر ما سایه خود</p></div>
<div class="m2"><p>که خورشیدانه سیما داری امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این خمخانه ما را میهمان کن</p></div>
<div class="m2"><p>بدان همسایه کان جا داری امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقاب از روی سرخ او فروکش</p></div>
<div class="m2"><p>که در پرده حمیرا داری امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دراشکن کشتی اندیشه‌ها را</p></div>
<div class="m2"><p>که کفی همچو دریا داری امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سری از عین و شین و قاف برزن</p></div>
<div class="m2"><p>که صد اسم و مسما داری امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمش باش و مدم در نای منطق</p></div>
<div class="m2"><p>که مصر و نیشکرها داری امروز</p></div></div>