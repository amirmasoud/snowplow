---
title: >-
    غزل شمارهٔ ۹۵۹
---
# غزل شمارهٔ ۹۵۹

<div class="b" id="bn1"><div class="m1"><p>اگر دل از غم دنیا جدا توانی کرد</p></div>
<div class="m2"><p>نشاط و عیش به باغ بقا توانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به آب ریاضت برآوری غسلی</p></div>
<div class="m2"><p>همه کدورت دل را صفا توانی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز منزل هوسات ار دو گام پیش نهی</p></div>
<div class="m2"><p>نزول در حرم کبریا توانی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درون بحر معانی لا نه آن گهری</p></div>
<div class="m2"><p>که قدر و قیمت خود را بها توانی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به همت ار نشوی در مقام خاک مقیم</p></div>
<div class="m2"><p>مقام خویش بر اوج علا توانی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به جیب تفکر فروبری سر خویش</p></div>
<div class="m2"><p>گذشته‌های قضا را ادا توانی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیکن این صفت ره روان چالاکست</p></div>
<div class="m2"><p>تو نازنین جهانی کجا توانی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه دست و پای اجل را فرو توانی بست</p></div>
<div class="m2"><p>نه رنگ و بوی جهان را رها توانی کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو رستم دل و جانی و سرور مردان</p></div>
<div class="m2"><p>اگر به نفس لئیمت غزا توانی کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر که درد غم عشق سر زند در تو</p></div>
<div class="m2"><p>به درد او غم دل را روا توانی کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خار چون و چرا این زمان چو درگذری</p></div>
<div class="m2"><p>به باغ جنت وصلش چرا توانی کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر تو جنس همایی و جنس زاغ نه‌ای</p></div>
<div class="m2"><p>ز جان تو میل به سوی هما توانی کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همای سایه دولت چو شمس تبریزیست</p></div>
<div class="m2"><p>نگر که در دل آن شاه جا توانی کرد</p></div></div>