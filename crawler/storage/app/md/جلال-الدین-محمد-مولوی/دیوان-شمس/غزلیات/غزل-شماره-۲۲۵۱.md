---
title: >-
    غزل شمارهٔ ۲۲۵۱
---
# غزل شمارهٔ ۲۲۵۱

<div class="b" id="bn1"><div class="m1"><p>چو از سر بگیرم بود سرور او</p></div>
<div class="m2"><p>چو من دل بجویم بود دلبر او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو من صلح جویم شفیع او بود</p></div>
<div class="m2"><p>چو در جنگ آیم بود خنجر او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو در مجلس آیم شراب است و نقل</p></div>
<div class="m2"><p>چو در گلشن آیم بود عبهر او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو در کان روم او عقیق است و لعل</p></div>
<div class="m2"><p>چو در بحر آیم بود گوهر او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو در دشت آیم بود روضه او</p></div>
<div class="m2"><p>چو وا چرخ آیم بود اختر او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در صبر آیم بود صدر او</p></div>
<div class="m2"><p>چو از غم بسوزم بود مجمر او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو در رزم آیم به وقت قتال</p></div>
<div class="m2"><p>بود صف نگهدار و سرلشکر او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو در بزم آیم به وقت نشاط</p></div>
<div class="m2"><p>بود ساقی و مطرب و ساغر او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نامه نویسم سوی دوستان</p></div>
<div class="m2"><p>بود کاغذ و خامه و محبر او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بیدار گردم بود هوش نو</p></div>
<div class="m2"><p>چو بخوابم بیاید به خواب اندر او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو جویم برای غزل قافیه</p></div>
<div class="m2"><p>به خاطر بود قافیه گستر او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو هر صورتی که مصور کنی</p></div>
<div class="m2"><p>چو نقاش و خامه بود بر سر او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو چندانک برتر نظر می‌کنی</p></div>
<div class="m2"><p>از آن برتر تو بود برتر او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برو ترک گفتار و دفتر بگو</p></div>
<div class="m2"><p>که آن به که باشد تو را دفتر او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خمش کن که هر شش جهت نور او است</p></div>
<div class="m2"><p>وزین شش جهت بگذری داور او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رضاک رضای الذی اوثر</p></div>
<div class="m2"><p>و سرک سری فما اظهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زهی شمس تبریز خورشیدوش</p></div>
<div class="m2"><p>که خود را بود سخت اندرخور او</p></div></div>