---
title: >-
    غزل شمارهٔ ۱۱۰۸
---
# غزل شمارهٔ ۱۱۰۸

<div class="b" id="bn1"><div class="m1"><p>ساقیا باده چون نار بیار</p></div>
<div class="m2"><p>دفع غم را تو ز اسرار بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده‌ای را که ز دل می‌جوشد</p></div>
<div class="m2"><p>زود ای ساقی دلدار بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کافر عشق بیا باده ببین</p></div>
<div class="m2"><p>نیست شو در می و اقرار بیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا دست همه مستان گیر</p></div>
<div class="m2"><p>همچنان جانب گلزار بیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش این شاهد ما خوبان را</p></div>
<div class="m2"><p>گردن بسته ز بلغار بیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مؤمنان را همه عریان کردی</p></div>
<div class="m2"><p>گروی نیز ز کفار بیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمس تبریز بگو دولت را</p></div>
<div class="m2"><p>بپذیر اندک و بسیار بیار</p></div></div>