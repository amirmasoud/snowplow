---
title: >-
    غزل شمارهٔ ۱۹۵۷
---
# غزل شمارهٔ ۱۹۵۷

<div class="b" id="bn1"><div class="m1"><p>هست عاقل هر زمانی در غم پیدا شدن</p></div>
<div class="m2"><p>هست عاشق هر زمانی بیخود و شیدا شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقلان از غرقه گشتن بر گریز و بر حذر</p></div>
<div class="m2"><p>عاشقان را کار و پیشه غرقه دریا شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقلان را راحت از راحت رسانیدن بود</p></div>
<div class="m2"><p>عاشقان را ننگ باشد بند راحت‌ها شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق اندر حلقه باشد از همه تن‌ها چنانک</p></div>
<div class="m2"><p>زیت را و آب را در یک محل تنها شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و آنک باشد در نصیحت دادن عشاق عشق</p></div>
<div class="m2"><p>نیست او را حاصلی جز سخره سودا شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق بوی مشک دارد زان سبب رسوا بود</p></div>
<div class="m2"><p>مشک را کی چاره باشد از چنین رسوا شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق باشد چون درخت و عاشقان سایه درخت</p></div>
<div class="m2"><p>سایه گر چه دور افتد بایدش آن جا شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر مقام عقل باید پیر گشتن طفل را</p></div>
<div class="m2"><p>در مقام عشق بینی پیر را برنا شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریزی به عشقت هر کی او پستی گزید</p></div>
<div class="m2"><p>همچو عشق تو بود در رفعت و بالا شدن</p></div></div>