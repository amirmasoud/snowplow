---
title: >-
    غزل شمارهٔ ۲۴۱۰
---
# غزل شمارهٔ ۲۴۱۰

<div class="b" id="bn1"><div class="m1"><p>مقام خلوت و یار و سماع و تو خفته</p></div>
<div class="m2"><p>که شرم بادت از آن زلف‌های آشفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از این سپس منم و شب روی و حلقه یار</p></div>
<div class="m2"><p>شب دراز و تب و رازهای ناگفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون پرده درند آن بتان و سوزانند</p></div>
<div class="m2"><p>که لطف‌های بتان در شب است بنهفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خواب کن همه را طاق شو از این جفتان</p></div>
<div class="m2"><p>به سوی طاق و رواقش مرو به شب جفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدانک خلوت شب بر مثال دریایی است</p></div>
<div class="m2"><p>به قعر بحر بود درهای ناسفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ چو کعبه نما شاه شمس تبریزی</p></div>
<div class="m2"><p>که باشدت عوض حج‌های پذرفته</p></div></div>