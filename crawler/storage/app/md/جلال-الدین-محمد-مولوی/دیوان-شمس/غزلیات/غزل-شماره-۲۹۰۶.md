---
title: >-
    غزل شمارهٔ ۲۹۰۶
---
# غزل شمارهٔ ۲۹۰۶

<div class="b" id="bn1"><div class="m1"><p>گوید آن دلبر که چون همدل شدی</p></div>
<div class="m2"><p>با هوس همراه و هم منزل شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از میان نقش‌ها پنهان شدی</p></div>
<div class="m2"><p>در جهان جان‌ها حاصل شدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم برآوردی سر از لطف خدا</p></div>
<div class="m2"><p>هم به شمشیر خدا بسمل شدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش آتش رو تو از نقصان مترس</p></div>
<div class="m2"><p>چونک از آتش چنین کامل شدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشرت دیوانگان را دیده‌ای</p></div>
<div class="m2"><p>ننگ بادت باز چون عاقل شدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نه‌ای حیوان چه مست سبزه‌ای</p></div>
<div class="m2"><p>چون نمردی چون در آب و گل شدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آستین شه صلاح الدین بگیر</p></div>
<div class="m2"><p>ور نگیری باطل باطل شدی</p></div></div>