---
title: >-
    غزل شمارهٔ ۱۵۸
---
# غزل شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>امتزاج روح‌ها در وقت صلح و جنگ‌ها</p></div>
<div class="m2"><p>با کسی باید که روحش هست صافی صفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تغییر هست در جان وقت جنگ و آشتی</p></div>
<div class="m2"><p>آن نه یک روحست تنها بلک گشتستند جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بخواهد دل سلام آن یکی همچون عروس</p></div>
<div class="m2"><p>مر زفاف صحبت داماد دشمن روی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز چون میلی بود سویی بدان ماند که او</p></div>
<div class="m2"><p>میل دارد سوی داماد لطیف دلربا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نظرها امتزاج و از سخن‌ها امتزاج</p></div>
<div class="m2"><p>وز حکایت امتزاج و از فکر آمیزها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچنانک امتزاج ظاهرست اندر رکوع</p></div>
<div class="m2"><p>وز تصافح وز عناق و قبله و مدح و دعا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر تفاوت این تمازج‌ها ز میل و نیم میل</p></div>
<div class="m2"><p>وز سر کره و کراهت وز پی ترس و حیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن رکوع باتأنی وان ثنای نرم نرم</p></div>
<div class="m2"><p>هم مراتب در معانی در صورها مجتبا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همه بازیچه گردد چون رسیدی در کسی</p></div>
<div class="m2"><p>کش سما سجده‌اش برد وان عرش گوید مرحبا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن خداوند لطیف بنده پرور شمس دین</p></div>
<div class="m2"><p>کو رهاند مر شما را زین خیال بی‌وفا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با عدم تا چند باشی خایف و امیدوار</p></div>
<div class="m2"><p>این همه تأثیر خشم اوست تا وقت رضا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هستی جان اوست حقا چونک هستی زو بتافت</p></div>
<div class="m2"><p>لاجرم در نیستی می‌ساز با قید هوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه به تسبیع هوا و گه به تسبیع خیال</p></div>
<div class="m2"><p>گه به تسبیع کلام و گه به تسبیع لقا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه خیال خوش بود در طنز همچون احتلام</p></div>
<div class="m2"><p>گه خیال بد بود همچون که خواب ناسزا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وانگهی تخییل‌ها خوشتر از این قوم رذیل</p></div>
<div class="m2"><p>اینت هستی کو بود کمتر ز تخییل عما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس از آن سوی عدم بدتر از این از صد عدم</p></div>
<div class="m2"><p>این عدم‌ها بر مراتب بود همچون که بقا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا نیاید ظل میمون خداوندی او</p></div>
<div class="m2"><p>هیچ بندی از تو نگشاید یقین می‌دان دلا</p></div></div>