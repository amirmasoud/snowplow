---
title: >-
    غزل شمارهٔ ۱۳۱۶
---
# غزل شمارهٔ ۱۳۱۶

<div class="b" id="bn1"><div class="m1"><p>رو رو که نه‌ای عاشق ای زلفک و ای خالک</p></div>
<div class="m2"><p>ای نازک و ای خشمک پابسته به خلخالک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مرگ کجا پیچد آن زلفک و آن پیچک</p></div>
<div class="m2"><p>بر چرخ کجا پرد آن پرک و آن بالک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نازک نازک‌دل دل جو که دلت ماند</p></div>
<div class="m2"><p>روزی که جدا مانی از زرک و از مالک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشکسته چرا باشی دلتنگ چرا گردی</p></div>
<div class="m2"><p>دل همچو دل میمک قد همچو قد دالک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو رستم دستانی از زال چه می‌ترسی</p></div>
<div class="m2"><p>یا رب برهان او را از ننگ چنین زالک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دوش تو را دیدم در خواب و چنان باشد</p></div>
<div class="m2"><p>بر چرخ همی‌گشتی سرمستک و خوش حالک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌گشتی و می‌گفتی ای زهره به من بنگر</p></div>
<div class="m2"><p>سرمستم و آزادم ز ادبارک و اقبالک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درویشی وانگه غم از مست نبیذی کم</p></div>
<div class="m2"><p>رو خدمت آن مه کن مردانه یکی سالک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر هفت فلک بگذر افسون زحل مشنو</p></div>
<div class="m2"><p>بگذار منجم را در اختر و در فالک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من خرقه ز خور دارم چون لعل و گهر دارم</p></div>
<div class="m2"><p>من خرقه کجا پوشم از صوفک و از شالک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با یار عرب گفتم در چشم ترم بنگر</p></div>
<div class="m2"><p>می‌گفت به زیر لب لا تخدعنی والک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌گفتم و می‌پختم در سینه دو صد حیلت</p></div>
<div class="m2"><p>می‌گفت مرا خندان کم تکتم احوالک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خامش کن و شه را بین چون باز سپیدی تو</p></div>
<div class="m2"><p>نی بلبل قوالی درمانده در این قالک</p></div></div>