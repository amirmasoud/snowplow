---
title: >-
    غزل شمارهٔ ۲۴۰۵
---
# غزل شمارهٔ ۲۴۰۵

<div class="b" id="bn1"><div class="m1"><p>ای همه منزل شده از تو ره بی‌رهه</p></div>
<div class="m2"><p>بی‌قدمی رقص بین بی‌دهنی قهقهه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر پستان عشق چونک دمی شیر یافت</p></div>
<div class="m2"><p>قامت سروی گرفت کودکک یک مهه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی ببینید روی بهر خدا عاشقان</p></div>
<div class="m2"><p>گر چه زنخ زد بسی کوردلی ابلهه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>والله کو یوسف است بشنو از من از آنک</p></div>
<div class="m2"><p>بودم با یوسفی هم نمک و هم چهه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونک نماید جمال گوش سوی غیب دار</p></div>
<div class="m2"><p>عرش پر از نعره‌هاست فرش پر از وه وهه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق باشد کمان خاص بتی همچو تیر</p></div>
<div class="m2"><p>هیچ نپرد کمان گر بشود ده زهه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنک ز تبریز دید یک نظر شمس دین</p></div>
<div class="m2"><p>طعنه زند بر چله سخره کند بر دهه</p></div></div>