---
title: >-
    غزل شمارهٔ ۲۱۲۲
---
# غزل شمارهٔ ۲۱۲۲

<div class="b" id="bn1"><div class="m1"><p>کیف اتوب یا اخی من سکر کارجوان</p></div>
<div class="m2"><p>لیس من التراب بل معصره بلا مکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط علی کوسها کتابه شارحه</p></div>
<div class="m2"><p>یا من من یشربها من الممات و الهوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من تبریز نبعه منبته و ینعه</p></div>
<div class="m2"><p>فها الیها جانب و جانب الی الجنان</p></div></div>