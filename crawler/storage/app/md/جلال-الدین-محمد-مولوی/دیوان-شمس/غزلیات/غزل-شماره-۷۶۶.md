---
title: >-
    غزل شمارهٔ ۷۶۶
---
# غزل شمارهٔ ۷۶۶

<div class="b" id="bn1"><div class="m1"><p>خضری که عمر ز آبت بکشد دراز گردد</p></div>
<div class="m2"><p>در مرگ برخورنده ابدا فرازگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نظر کنی به بالا سوی آسمان اعلا</p></div>
<div class="m2"><p>دو هزار در ز رحمت ز بهشت باز گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو فتاد سایه تو سوی مفسدان مجرم</p></div>
<div class="m2"><p>همه جرم‌های ایشان چله و نماز گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو رکاب مصطفایی سوی عفو روی آرد</p></div>
<div class="m2"><p>دو هزار بولهب هم خوش و پرنیاز گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دو دست همچو بحرت به کرم گهرفشان شد</p></div>
<div class="m2"><p>رخ چون زرم زر آرد که به گرد گاز گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کف تست کیمیایی لب بحر کبریایی</p></div>
<div class="m2"><p>چه عجب که نیم حبه ز کفت رکاز گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو هزار جان و دیده ز فزع عنان کشیده</p></div>
<div class="m2"><p>چو صلای وصل آید گه ترک تاز گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه زهر دین و دنیا ز تو شهد و نوش آمد</p></div>
<div class="m2"><p>غم و درد سینه سوزان ز تو دلنواز گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه دامن تو گیرد دل و این قدر نداند</p></div>
<div class="m2"><p>که به گرد شیر آهو به صد احتراز گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در وصل چون ببستی و به لامکان نشستی</p></div>
<div class="m2"><p>ز کجا رسد گشایش چو دری فراز گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمش و سخن رها کن جز اله را تو لا کن</p></div>
<div class="m2"><p>به فنا چو ساز گیری همه کارساز گردد</p></div></div>