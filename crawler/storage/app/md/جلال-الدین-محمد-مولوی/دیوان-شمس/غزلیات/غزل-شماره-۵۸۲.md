---
title: >-
    غزل شمارهٔ ۵۸۲
---
# غزل شمارهٔ ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>اگر خواب آیدم امشب سزای ریش خود بیند</p></div>
<div class="m2"><p>به جای مفرش و بالی همه مشت و لگد بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازیرا خواب کژ بیند که آیینه خیالست او</p></div>
<div class="m2"><p>که معلوم‌ست تعبیرش اگر او نیک و بد بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خصوصا اندر این مجلس که امشب در نمی‌گنجد</p></div>
<div class="m2"><p>دو چشم عقل پایان بین که صدساله رصد بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب قدرست وصل او شب قبرست هجر او</p></div>
<div class="m2"><p>شب قبر از شب قدرش کرامات و مدد بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنک جانی که بر بامش همی چوبک زند امشب</p></div>
<div class="m2"><p>شود همچون سحر خندان عطای بی‌عدد بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو ای خواب خاری زن تو اندر چشم نامحرم</p></div>
<div class="m2"><p>که حیفست آن که بیگانه در این شب قد و خد بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرابش ده بخوابانش برون بر از گلستانش</p></div>
<div class="m2"><p>که تا در گردن او فردا ز غم حبل مسد بیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببردی روز در گفتن چو آمد شب خمش باری</p></div>
<div class="m2"><p>که هرک از گفت خامش شد عوض گفت ابد بیند</p></div></div>