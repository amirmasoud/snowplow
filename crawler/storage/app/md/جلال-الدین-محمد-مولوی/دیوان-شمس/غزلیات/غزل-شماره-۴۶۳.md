---
title: >-
    غزل شمارهٔ ۴۶۳
---
# غزل شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>هر نفس آواز عشق می‌رسد از چپ و راست</p></div>
<div class="m2"><p>ما به فلک می‌رویم عزم تماشا که راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به فلک بوده‌ایم یار ملک بوده‌ایم</p></div>
<div class="m2"><p>باز همان جا رویم جمله که آن شهر ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود ز فلک برتریم وز ملک افزونتریم</p></div>
<div class="m2"><p>زین دو چرا نگذریم منزل ما کبریاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهر پاک از کجا عالم خاک از کجا</p></div>
<div class="m2"><p>بر چه فرود آمدیت بار کنید این چه جاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت جوان یار ما دادن جان کار ما</p></div>
<div class="m2"><p>قافله سالار ما فخر جهان مصطفاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مه او مه شکافت دیدن او برنتافت</p></div>
<div class="m2"><p>ماه چنان بخت یافت او که کمینه گداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی خوش این نسیم از شکن زلف اوست</p></div>
<div class="m2"><p>شعشعه این خیال زان رخ چون والضحاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل ما درنگر هر دم شق قمر</p></div>
<div class="m2"><p>کز نظر آن نظر چشم تو آن سو چراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق چو مرغابیان زاده ز دریای جان</p></div>
<div class="m2"><p>کی کند این جا مقام مرغ کز آن بحر خاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلک به دریا دریم جمله در او حاضریم</p></div>
<div class="m2"><p>ور نه ز دریای دل موج پیاپی چراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد موج الست کشتی قالب ببست</p></div>
<div class="m2"><p>باز چو کشتی شکست نوبت وصل و لقاست</p></div></div>