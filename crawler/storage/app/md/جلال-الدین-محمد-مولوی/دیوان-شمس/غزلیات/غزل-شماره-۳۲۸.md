---
title: >-
    غزل شمارهٔ ۳۲۸
---
# غزل شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>بادست مرا زان سر اندر سر و در سبلت</p></div>
<div class="m2"><p>پرباد چرا نبود سرمست چنین دولت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه و هر ساعت بر کوری هشیاری</p></div>
<div class="m2"><p>صد رطل درآشامم بی‌ساغر و بی‌آلت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغان هوایی را بازان خدایی را</p></div>
<div class="m2"><p>از غیب به دست آرم بی‌صنعت و بی‌حیلت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود از کف دست من مرغان عجب رویند</p></div>
<div class="m2"><p>می از لب من جوشد در مستی آن حالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دانه آدم را کز سنبل او باشد</p></div>
<div class="m2"><p>بفروشم جنت را بر جان نهم جنت</p></div></div>