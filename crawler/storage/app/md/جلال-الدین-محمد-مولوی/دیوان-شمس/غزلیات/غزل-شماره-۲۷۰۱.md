---
title: >-
    غزل شمارهٔ ۲۷۰۱
---
# غزل شمارهٔ ۲۷۰۱

<div class="b" id="bn1"><div class="m1"><p>مرا هر لحظه قربان است جانی</p></div>
<div class="m2"><p>تو را هر لحظه در بنده گمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چشم تو بیان حال من بس</p></div>
<div class="m2"><p>که روشنتر از این نبود بیانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان چون نی هزاران ناله دارد</p></div>
<div class="m2"><p>که یک نی دید از شکرستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن شکرستان دیدم نشان‌ها</p></div>
<div class="m2"><p>ندیدم از تو شیرینتر نشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثال عشق پیدایی و پنهان</p></div>
<div class="m2"><p>ندیدم همچو تو پیدا نهانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان جویای توست و جای آن هست</p></div>
<div class="m2"><p>مثل بشنو که جان به از جهانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه‌ای بر آسمان ای ماه لیکن</p></div>
<div class="m2"><p>شود هر جا که تابی آسمانی</p></div></div>