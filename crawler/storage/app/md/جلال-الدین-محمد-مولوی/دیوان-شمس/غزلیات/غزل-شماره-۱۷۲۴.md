---
title: >-
    غزل شمارهٔ ۱۷۲۴
---
# غزل شمارهٔ ۱۷۲۴

<div class="b" id="bn1"><div class="m1"><p>همه جمال تو بینم چو چشم باز کنم</p></div>
<div class="m2"><p>همه شراب تو نوشم چو لب فراز کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرام دارم با مردمان سخن گفتن</p></div>
<div class="m2"><p>و چون حدیث تو آید سخن دراز کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار گونه بلنگم به هر رهم که برند</p></div>
<div class="m2"><p>رهی که آن به سوی تو است ترک تاز کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به دست من آید چو خضر آب حیات</p></div>
<div class="m2"><p>ز خاک کوی تو آن آب را طراز کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خارخار غم تو چو خارچین گردم</p></div>
<div class="m2"><p>ز نرگس و گل صدبرگ احتراز کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آفتاب و ز مهتاب بگذرد نورم</p></div>
<div class="m2"><p>چو روی خود به شهنشاه دلنواز کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پر و بال برآرم ز شوق چون بهرام</p></div>
<div class="m2"><p>به مسجد فلک هفتمین نماز کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه سعادت بینم چو سوی نحس روم</p></div>
<div class="m2"><p>همه حقیقت گردد اگر مجاز کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا و قوم مرا عاقبت شود محمود</p></div>
<div class="m2"><p>چو خویش را پی محمود خود ایاز کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آفتاب شوم آتش و ز گرمی دل</p></div>
<div class="m2"><p>چو ذره‌ها همه را مست و عشقباز کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پریر عشق مرا گفت من همه نازم</p></div>
<div class="m2"><p>همه نیاز شو آن لحظه‌ای که ناز کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو ناز را بگذاری همه نیاز شوی</p></div>
<div class="m2"><p>من از برای تو خود را همه نیاز کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خموش باش زمانی بساز با خمشی</p></div>
<div class="m2"><p>که تا برای سماع تو چنگ ساز کنم</p></div></div>