---
title: >-
    غزل شمارهٔ ۱۷۳۶
---
# غزل شمارهٔ ۱۷۳۶

<div class="b" id="bn1"><div class="m1"><p>به گرد تو چو نگردم به گرد خود گردم</p></div>
<div class="m2"><p>به گرد غصه و اندوه و بخت بد گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نیم مست من از خواب برجهم به صبوح</p></div>
<div class="m2"><p>به گرد ساقی خود طالب مدد گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گرد لقمه معدود خلق گردانند</p></div>
<div class="m2"><p>به گرد خالق و بر نقد بی‌عدد گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوام عالم محدود چون ز بی‌حدی است</p></div>
<div class="m2"><p>مگیر عیب اگر من برون ز حد گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که او لحد سینه را چو باغی کرد</p></div>
<div class="m2"><p>روا نداشت که من بسته لحد گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لحد چه باشد در آسمان نگنجد جان</p></div>
<div class="m2"><p>ز پنج و شش گذرم زود بر احد گردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چه آینه روشنم ز بیم غبار</p></div>
<div class="m2"><p>روا بود که دو سه روز بر نمد گردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر گلی بده‌ام زین بهار باغ شوم</p></div>
<div class="m2"><p>وگر یکی بده‌ام زین وصال صد گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان صورت‌ها این حسد بود ناچار</p></div>
<div class="m2"><p>ولی چو آینه گشتم بر حسد گردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من از طویله این حرف می روم به چرا</p></div>
<div class="m2"><p>ستور بسته نیم از چه بر وتد گردم</p></div></div>