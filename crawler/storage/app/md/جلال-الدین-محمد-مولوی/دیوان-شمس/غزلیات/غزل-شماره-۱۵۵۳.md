---
title: >-
    غزل شمارهٔ ۱۵۵۳
---
# غزل شمارهٔ ۱۵۵۳

<div class="b" id="bn1"><div class="m1"><p>ما صحبت همدگر گزینیم</p></div>
<div class="m2"><p>بر دامن همدگر نشینیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران همه پیشتر نشینید</p></div>
<div class="m2"><p>تا چهره همدگر ببینیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را ز درون موافقت‌هاست</p></div>
<div class="m2"><p>تا ظن نبری که ما همینیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دم که نشسته‌ایم با هم</p></div>
<div class="m2"><p>می بر کف و گل در آستینیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عین به غیب راه داریم</p></div>
<div class="m2"><p>زیرا همراه پیک دینیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خانه به باغ راه داریم</p></div>
<div class="m2"><p>همسایه سرو و یاسمینیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر روز به باغ اندرآییم</p></div>
<div class="m2"><p>گل‌های شکفته صد ببینیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز بهر نثار عاشقان را</p></div>
<div class="m2"><p>دامن دامن ز گل بچینیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از باغ هر آنچ جمع کردیم</p></div>
<div class="m2"><p>در پیش نهیم و برگزینیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ما دل خویش درمدزدید</p></div>
<div class="m2"><p>ما دزد نه‌ایم ما امینیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اینک دم ما نسیم آن گل</p></div>
<div class="m2"><p>ما گلبن گلشن یقینیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالم پر شد نسیم آن گل</p></div>
<div class="m2"><p>یعنی که بیا که ما چنینیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بومان ببرد چو بوی بردیم</p></div>
<div class="m2"><p>مه مان کند ار چه ما کهینیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر چند کمین غلام عشقیم</p></div>
<div class="m2"><p>چون عشق نشسته در کمینیم</p></div></div>