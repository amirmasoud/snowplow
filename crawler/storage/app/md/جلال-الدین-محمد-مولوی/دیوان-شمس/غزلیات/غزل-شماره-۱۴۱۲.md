---
title: >-
    غزل شمارهٔ ۱۴۱۲
---
# غزل شمارهٔ ۱۴۱۲

<div class="b" id="bn1"><div class="m1"><p>بیا هر کس که می خواهد که تا با وی گرو بندم</p></div>
<div class="m2"><p>که سنگ خاره جان گیرد بپیوند خداوندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی‌گفتم به گل روزی زهی خندان قلاوزی</p></div>
<div class="m2"><p>مرا گل گفت می دانی تو باری کز چه می خندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال شاه خوش خویم تبسم کرد در رویم</p></div>
<div class="m2"><p>چنین شد نسل بر نسلم چنین فرزند فرزندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه من گفت هر مسکین که عمرش نیست من عمرم</p></div>
<div class="m2"><p>بدین وعده من مسکین امید از عمر برکندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من بانگ بر من زد چه باشد قدر عمری خود</p></div>
<div class="m2"><p>چه منت می نهی بر من تو خود چندی و من چندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهی کز لطف می آید اگر منت نهد شاید</p></div>
<div class="m2"><p>که چاهی پرحدث بودی منت از زر درآگندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمر نابسته در خدمت مرا تاج خرد داد او</p></div>
<div class="m2"><p>تو خود اندیشه کن با خود چه بخشد گر بپیوندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یقول العشق لی سرا تنافس و اغتنم برا</p></div>
<div class="m2"><p>و لا تفجر و لا تهجر و الا تبتئس تندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه شاهان غلامان را به خرسندی ثنا گفته</p></div>
<div class="m2"><p>همه خشم خداوندی بر من این که خرسندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مضی فی صحوتی یومی و فاض السکر فی قومی</p></div>
<div class="m2"><p>فاسرع و اسقنی خمرا حمیرا تشبه العندم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا درده یکی جامی پر از شادی و آرامی</p></div>
<div class="m2"><p>که بنمایم سرانجامی چو مخموران بپرسندم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میازارید از خویم که من بسیار می گویم</p></div>
<div class="m2"><p>جهانی طوطیان دارم اگر بسیار شد قندم</p></div></div>