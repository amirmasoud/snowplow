---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای دل چه اندیشیده‌ای در عذر آن تقصیرها</p></div>
<div class="m2"><p>زان سوی او چندان وفا زین سوی تو چندین جفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان سوی او چندان کرم زین سو خلاف و بیش و کم</p></div>
<div class="m2"><p>زان سوی او چندان نعم زین سوی تو چندین خطا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین سوی تو چندین حسد چندین خیال و ظن بد</p></div>
<div class="m2"><p>زان سوی او چندان کشش چندان چشش چندان عطا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندین چشش از بهر چه تا جان تلخت خوش شود</p></div>
<div class="m2"><p>چندین کشش از بهر چه تا دررسی در اولیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بد پشیمان می‌شوی الله گویان می‌شوی</p></div>
<div class="m2"><p>آن دم تو را او می‌کشد تا وارهاند مر تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جرم ترسان می‌شوی وز چاره پرسان می‌شوی</p></div>
<div class="m2"><p>آن لحظه ترساننده را با خود نمی‌بینی چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چشم تو بربست او چون مهره‌ای در دست او</p></div>
<div class="m2"><p>گاهی بغلطاند چنین گاهی ببازد در هوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاهی نهد در طبع تو سودای سیم و زر و زن</p></div>
<div class="m2"><p>گاهی نهد در جان تو نور خیال مصطفی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این سو کشان سوی خوشان وان سو کشان با ناخوشان</p></div>
<div class="m2"><p>یا بگذرد یا بشکند کشتی در این گرداب‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چندان دعا کن در نهان چندان بنال اندر شبان</p></div>
<div class="m2"><p>کز گنبد هفت آسمان در گوش تو آید صدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بانک شعیب و ناله‌اش وان اشک همچون ژاله‌اش</p></div>
<div class="m2"><p>چون شد ز حد از آسمان آمد سحرگاهش ندا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر مجرمی بخشیدمت وز جرم آمرزیدمت</p></div>
<div class="m2"><p>فردوس خواهی دادمت خامش رها کن این دعا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتا نه این خواهم نه آن دیدار حق خواهم عیان</p></div>
<div class="m2"><p>گر هفت بحر آتش شود من درروم بهر لقا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر رانده آن منظرم بستست از او چشم ترم</p></div>
<div class="m2"><p>من در جحیم اولیترم جنت نشاید مر مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جنت مرا بی‌روی او هم دوزخست و هم عدو</p></div>
<div class="m2"><p>من سوختم زین رنگ و بو کو فر انوار بقا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتند باری کم گری تا کم نگردد مبصری</p></div>
<div class="m2"><p>که چشم نابینا شود چون بگذرد از حد بکا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت ار دو چشمم عاقبت خواهند دیدن آن صفت</p></div>
<div class="m2"><p>هر جزو من چشمی شود کی غم خورم من از عمی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور عاقبت این چشم من محروم خواهد ماندن</p></div>
<div class="m2"><p>تا کور گردد آن بصر کو نیست لایق دوست را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اندر جهان هر آدمی باشد فدای یار خود</p></div>
<div class="m2"><p>یار یکی انبان خون یار یکی شمس ضیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون هر کسی درخورد خود یاری گزید از نیک و بد</p></div>
<div class="m2"><p>ما را دریغ آید که خود فانی کنیم از بهر لا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روزی یکی همراه شد با بایزید اندر رهی</p></div>
<div class="m2"><p>پس بایزیدش گفت چه پیشه گزیدی ای دغا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفتا که من خربنده‌ام پس بایزیدش گفت رو</p></div>
<div class="m2"><p>یا رب خرش را مرگ ده تا او شود بنده خدا</p></div></div>