---
title: >-
    غزل شمارهٔ ۸۲۲
---
# غزل شمارهٔ ۸۲۲

<div class="b" id="bn1"><div class="m1"><p>عشق اکنون مهربانی می‌کند</p></div>
<div class="m2"><p>جان جان امروز جانی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شعاع آفتاب معرفت</p></div>
<div class="m2"><p>ذره ذره غیب دانی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیمیای کیمیاسازست عشق</p></div>
<div class="m2"><p>خاک را گنج معانی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه درها می‌گشاید بر فلک</p></div>
<div class="m2"><p>گه خرد را نردبانی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه چو صهبا بزم شادی می‌نهد</p></div>
<div class="m2"><p>گه چو دریا درفشانی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه چو روح الله طبیبی می‌شود</p></div>
<div class="m2"><p>گه خلیلش میزبانی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اعتمادی دارد او بر عشق دوست</p></div>
<div class="m2"><p>گر سماع لن ترانی می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر این طوفان که خونست آب او</p></div>
<div class="m2"><p>لطف خود را نوح ثانی می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بانگ انانستعین ما شنید</p></div>
<div class="m2"><p>لطف و داد و مستعانی می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون قرین شد عشق او با جان‌ها</p></div>
<div class="m2"><p>مو به مو صاحب قرانی می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ارمغان‌های غریب آورده است</p></div>
<div class="m2"><p>قسمت آن ارمغانی می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که می‌بندد ره عشاق را</p></div>
<div class="m2"><p>جاهلی و قلتبانی می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرنگون اندررود در آب شور</p></div>
<div class="m2"><p>هر که چون لنگر گرانی می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا چه خوردست این دهان کز ذوق آن</p></div>
<div class="m2"><p>اقتضای بی‌زبانی می‌کند</p></div></div>