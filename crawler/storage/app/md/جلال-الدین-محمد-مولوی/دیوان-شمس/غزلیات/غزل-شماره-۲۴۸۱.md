---
title: >-
    غزل شمارهٔ ۲۴۸۱
---
# غزل شمارهٔ ۲۴۸۱

<div class="b" id="bn1"><div class="m1"><p>با همگان فضولکی چون که به ما ملولکی</p></div>
<div class="m2"><p>رو که بدین عاشقی سخت عظیم گولکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای تو فضول در هوا ای تو ملول در خدا</p></div>
<div class="m2"><p>چون تو از آن قان نه‌ای رو که یکی مغولکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستک خویش گشته‌ای گه ترشک گهی خوشک</p></div>
<div class="m2"><p>نازک و کبرکت که چه در هنرک نغولکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو کتاب خانه‌ای طالب باغ جان نه‌ای</p></div>
<div class="m2"><p>گر چه اصیلکی ولی خواجه تو بی‌اصولکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو تو به کیمیای جان مس وجود خرج کن</p></div>
<div class="m2"><p>تا نشوی از او چو زر در غم نیم پولکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم با ضمیر خود چند خیال جسمیان</p></div>
<div class="m2"><p>یا تو ز هر فسرده‌ای سوی دلم رسولکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور خدایگان جان در تبریز شمس دین</p></div>
<div class="m2"><p>کرد طریق سالکان ایمن اگر تو غولکی</p></div></div>