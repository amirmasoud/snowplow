---
title: >-
    غزل شمارهٔ ۳۴۹
---
# غزل شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>مبر رنج ای برادر خواجه سختست</p></div>
<div class="m2"><p>به وقت داد و بخشش شوربختست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه باغ را نیمی گرفته‌ست</p></div>
<div class="m2"><p>ولیکن سخت بی‌میوه درختست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشاده ابروست و بسته کیسه</p></div>
<div class="m2"><p>مشو غره که او را سیم و رختست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو دستش را به تخته دوختستند</p></div>
<div class="m2"><p>چه سود ار خواجه بر بالای تختست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وجودش گر چه یک پاره‌ست چون کوه</p></div>
<div class="m2"><p>سخااش مرده است و لخت لختست</p></div></div>