---
title: >-
    غزل شمارهٔ ۲۰۷
---
# غزل شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>ای که
به هنگام ِ دَرد، راحت ِ جانی مرا؛</p></div>
<div class="m2"><p>و ای
که به تلخی‌یِ فقر گنج ِ روانی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن‌چه
نَبُرده ست وَهم، عقل ندیده ست و فهم،</p></div>
<div class="m2"><p>از تو
به جان‌ام رسید؛ قبله از آنی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کَرَم‌ات،
من به ناز می‌نگرم در بقا</p></div>
<div class="m2"><p>کی بفریبَد
شَها دولت ِ فانی مرا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نَغمَت
ِ آن‌کس که او مژده‌یِ تو آوَرَد،</p></div>
<div class="m2"><p>گر چه
به خوابی بوَد، بِه زِ اَغانی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در رکَعات
ِ نماز، هست خیال ِ تو، شه!</p></div>
<div class="m2"><p>واجب و
لازم چُنان‌ک سَبع ِ مَثانی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گُنَه
ِ کافران رحم و شفاعت تو را ست</p></div>
<div class="m2"><p>مهتری
و سروری! سنگ‌دلانی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کَرَم
ِ لایزال عَرضه کُنَد مُلک‌ها،</p></div>
<div class="m2"><p>پیش نهَد
جمله‌ئی کَنز ِ نهانی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سُجده
کُنَم من ز جان؛ روی نهَم من به خاک</p></div>
<div class="m2"><p>گویم:
از این‌ها همه، عشق ِ فلانی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عُمر ِ
ابَد، پیش ِ من هست زمان ِ وصال</p></div>
<div class="m2"><p>زآن‌ک
نگنجد در او هیچ زمانی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر، اَوانی‌ست
و وصل، شربت ِ صافی در آن</p></div>
<div class="m2"><p>بی تو
چه کار آید م رنج ِ اَوانی مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیست
هزار آرزو بود مرا پیش از این</p></div>
<div class="m2"><p>در هوس‌اش
خود نماند هیچ اَمانی مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از مدد
ِ لطف ِ او ایمِن گشتم؛ از آن‌ک</p></div>
<div class="m2"><p>گوید
سلطان ِ غیب: «لَستَ تَرانی» مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوهر ِ
معنی ِ او ست پر شده جان و دل‌ام</p></div>
<div class="m2"><p>او ست
اگر گفت نیست ثالث و ثانی مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رفت
وصال‌اش به روح؛ جسم نکرد التفات</p></div>
<div class="m2"><p>گر چه
مجرّد زِ تَن گشت عیانی مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیر
شدم از غم‌اش؛ لیک چو تبریز را</p></div>
<div class="m2"><p>نام بَری،
بازگشت جمله جوانی مرا</p></div></div>