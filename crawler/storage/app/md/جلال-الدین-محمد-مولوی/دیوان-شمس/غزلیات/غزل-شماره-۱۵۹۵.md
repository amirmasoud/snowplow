---
title: >-
    غزل شمارهٔ ۱۵۹۵
---
# غزل شمارهٔ ۱۵۹۵

<div class="b" id="bn1"><div class="m1"><p>سر قدم کردیم و آخر سوی جیحون تاختیم</p></div>
<div class="m2"><p>عالمی برهم زدیم و چست و بیرون تاختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون براق عشق عرشی بود زیر ران ما</p></div>
<div class="m2"><p>گنبدی کردیم و سوی چرخ گردون تاختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم چون را مثال ذره‌ها برهم زدیم</p></div>
<div class="m2"><p>تا به پیش تخت آن سلطان بی‌چون تاختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فهم و وهم و عقل انسان جملگی در ره بریخت</p></div>
<div class="m2"><p>چونک از شش حد انسان سخت افزون تاختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونک در سینور مجنونان آن لیلی شدیم</p></div>
<div class="m2"><p>سرکش آمد مرکب و از حد مجنون تاختیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس چون قارون ز سعی ما درون خاک شد</p></div>
<div class="m2"><p>بعد از آن مردانه سوی گنج قارون تاختیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشت و هامون روح گیرد گر بیابد ذره‌ای</p></div>
<div class="m2"><p>ز آنچ ما از نور او در دشت و هامون تاختیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس صدف‌های چو گوهر زیر سنگی کوفتیم</p></div>
<div class="m2"><p>تا به سوی گنج‌های در مکنون تاختیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوی شمع شمس تبریزی به بیشه شیر جان</p></div>
<div class="m2"><p>بوده پروانه نپنداری که اکنون تاختیم</p></div></div>