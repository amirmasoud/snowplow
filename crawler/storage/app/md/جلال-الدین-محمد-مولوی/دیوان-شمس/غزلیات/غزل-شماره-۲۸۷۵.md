---
title: >-
    غزل شمارهٔ ۲۸۷۵
---
# غزل شمارهٔ ۲۸۷۵

<div class="b" id="bn1"><div class="m1"><p>نی تو شکلی دگری سنگ نباشی تو زری</p></div>
<div class="m2"><p>سنگ هم بوی برد نیز که زیباگهری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل نهادم که به همسایگیت خانه کنم</p></div>
<div class="m2"><p>که بسی نادر و سبز و تر و عالی شجری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبزه‌ها جمله در این سبزی تو محو شوند</p></div>
<div class="m2"><p>من چه گویم که تری تو نماند به تری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه چون شیر و شکر با همه آمیخته‌ای</p></div>
<div class="m2"><p>هیچ عقلی نپذیرد ز تو که زین نفری</p></div></div>