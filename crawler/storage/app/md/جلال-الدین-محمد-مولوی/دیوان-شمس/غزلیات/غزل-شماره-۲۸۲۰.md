---
title: >-
    غزل شمارهٔ ۲۸۲۰
---
# غزل شمارهٔ ۲۸۲۰

<div class="b" id="bn1"><div class="m1"><p>چو به شهر تو رسیدم تو ز من گوشه گزیدی</p></div>
<div class="m2"><p>چو ز شهر تو برفتم به وداعیم ندیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو اگر لطف گزینی و اگر بر سر کینی</p></div>
<div class="m2"><p>همه آسایش جانی همه آرایش عیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبب غیرت توست آنک نهانی و اگر نی</p></div>
<div class="m2"><p>همه خورشید عیانی که ز هر ذره پدیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو اگر گوشه بگیری تو جگرگوشه و میری</p></div>
<div class="m2"><p>و اگر پرده دری تو همه را پرده دریدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل کفر از تو مشوش سر ایمان به میت خوش</p></div>
<div class="m2"><p>همه را هوش ربودی همه را گوش کشیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه گل‌ها گرو دی همه سرها گرو می</p></div>
<div class="m2"><p>تو هم این را و هم آن را ز کف مرگ خریدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو وفا نبود در گل چو رهی نیست سوی کل</p></div>
<div class="m2"><p>همه بر توست توکل که عمادی و عمیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر از چهره یوسف نفری کف ببریدند</p></div>
<div class="m2"><p>تو دو صد یوسف جان را ز دل و عقل بریدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز پلیدی و ز خونی تو کنی صورت شخصی</p></div>
<div class="m2"><p>که گریزد به دو فرسنگ وی از بوی پلیدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنیش طعمه خاکی که شود سبزه پاکی</p></div>
<div class="m2"><p>برهد او ز نجاست چو در او روح دمیدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هله ای دل به سما رو به چراگاه خدا رو</p></div>
<div class="m2"><p>به چراگاه ستوران چو یکی چند چریدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو همه طمع بر آن نه که در او نیست امیدت</p></div>
<div class="m2"><p>که ز نومیدی اول تو بدین سوی رسیدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو خمش کن که خداوند سخن بخش بگوید</p></div>
<div class="m2"><p>که همو ساخت در قفل و همو کرد کلیدی</p></div></div>