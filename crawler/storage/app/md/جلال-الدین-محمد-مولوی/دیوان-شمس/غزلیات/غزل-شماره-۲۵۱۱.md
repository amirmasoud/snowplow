---
title: >-
    غزل شمارهٔ ۲۵۱۱
---
# غزل شمارهٔ ۲۵۱۱

<div class="b" id="bn1"><div class="m1"><p>به باغ و چشمه حیوان چرا این چشم نگشایی</p></div>
<div class="m2"><p>چرا بیگانه‌ای از ما چو تو در اصل از مایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو طوطی زاده‌ای جانم مکن ناز و مرنجانم</p></div>
<div class="m2"><p>ز اصل آورده‌ای دانم تو قانون شکرخایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا در خانه خویش آ مترس از عکس خود پیش آ</p></div>
<div class="m2"><p>بهل طبع کژاندیشی که او یاوه‌ست و هرجایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا ای شاه یغمایی مرو هر جا که ما رایی</p></div>
<div class="m2"><p>اگر بر دیگران تلخی به نزد ما چو حلوایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نباشد عیب در نوری کز او غافل بود کوری</p></div>
<div class="m2"><p>نباشد عیب حلوا را به طعن شخص صفرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآر از خاک جانی را ببین جان آسمانی را</p></div>
<div class="m2"><p>کز آن گردان شده‌ست ای جان مه و این چرخ خضرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدم بر نردبانی نه دو چشم اندر عیانی نه</p></div>
<div class="m2"><p>بدن را در زیانی نه که تا جان را بیفزایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درختی بین بسی بابر نه خشکش بینی و نی تر</p></div>
<div class="m2"><p>به سایه آن درخت اندر بخسپی و بیاسایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی چشمه عجب بینی که نزدیکش چو بنشینی</p></div>
<div class="m2"><p>شوی همرنگ او در حین به لطف و ذوق و زیبایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانی خویش را از وی شوی هم شیء و هم لاشی</p></div>
<div class="m2"><p>نماند کو نماند کی نماند رنگ و سیمایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو با چشمه درآمیزی نماید شمس تبریزی</p></div>
<div class="m2"><p>درون آب همچون مه ز بهر عالم آرایی</p></div></div>