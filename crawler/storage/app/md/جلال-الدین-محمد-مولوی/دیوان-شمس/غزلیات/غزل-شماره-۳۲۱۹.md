---
title: >-
    غزل شمارهٔ ۳۲۱۹
---
# غزل شمارهٔ ۳۲۱۹

<div class="b" id="bn1"><div class="m1"><p>اسفا لقلبی یوما هجرالحبیب داری</p></div>
<div class="m2"><p>و تحرقت ضلوعی و جوانحی بناری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و سعادة لیوم نظرالسعود فینا</p></div>
<div class="m2"><p>نزل السهیل سهلا و اقام فی جواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فدخلت لج بحر بطرا بما اتانی</p></div>
<div class="m2"><p>فغرقت فیه لکن نظرالحبیب جاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتحت عیون قلبی فرأیت الف بحر</p></div>
<div class="m2"><p>و مراکبا علیها بهوی الهوا سواری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبریز حض فضلا و ترابه کمالا</p></div>
<div class="m2"><p>بشعاع نور صدر هو افضل الکبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تبریز اشفعی لی بشفاعة الی من</p></div>
<div class="m2"><p>زعقات وجد قلبی لحقتة بالتواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و لاجل سؤ حالی بتواضعی لدیه</p></div>
<div class="m2"><p>و تعرضی هوانی بهواه والصغار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و تقول لا تقطع کبدا رهین شوق</p></div>
<div class="m2"><p>برجاک ما یرجی و یذوب بالبواری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و تتوب من ذنوبی و تجاسری علیه</p></div>
<div class="m2"><p>و لیه عود قلبی و نهایة الفرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لمعات شمس دین هو سیدی حقیقا</p></div>
<div class="m2"><p>هی اصل اصل روحی و وراء هاعواری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمع الاله شملا قطعته شقوة لی</p></div>
<div class="m2"><p>فهو الکبیر یعفو لجنایة العصاری</p></div></div>