---
title: >-
    غزل شمارهٔ ۹۶
---
# غزل شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>لب را تو به هر بوسه و هر لوت میالا</p></div>
<div class="m2"><p>تا از لب دلدار شود مست و شکرخا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا از لب تو بوی لب غیر نیاید</p></div>
<div class="m2"><p>تا عشق مجرد شود و صافی و یکتا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن لب که بود کون خری بوسه گه او</p></div>
<div class="m2"><p>کی یابد آن لب شکربوس مسیحا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌دانک حدث باشد جز نور قدیمی</p></div>
<div class="m2"><p>بر مزبله پرحدث آن گاه تماشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنگه که فنا شد حدث اندر دل پالیز</p></div>
<div class="m2"><p>رست از حدثی و شود او چاشنی افزا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تو حدثی لذت تقدیس چه دانی</p></div>
<div class="m2"><p>رو از حدثی سوی تبارک و تعالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان دست مسیح آمد داروی جهانی</p></div>
<div class="m2"><p>کو دست نگه داشت ز هر کاسه سکبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نعمت فرعون چو موسی کف و لب شست</p></div>
<div class="m2"><p>دریای کرم داد مر او را ید بیضا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی که ز معده و لب هر خام گریزی</p></div>
<div class="m2"><p>پرگوهر و روتلخ همی‌باش چو دریا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هین چشم فروبند که آن چشم غیورست</p></div>
<div class="m2"><p>هین معده تهی دار که لوتیست مهیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سگ سیر شود هیچ شکاری بنگیرد</p></div>
<div class="m2"><p>کز آتش جوعست تک و گام تقاضا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کو دست و لب پاک که گیرد قدح پاک</p></div>
<div class="m2"><p>کو صوفی چالاک که آید سوی حلوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنمای از این حرف تصاویر حقایق</p></div>
<div class="m2"><p>یا من قسم القهوه و الکاس علینا</p></div></div>