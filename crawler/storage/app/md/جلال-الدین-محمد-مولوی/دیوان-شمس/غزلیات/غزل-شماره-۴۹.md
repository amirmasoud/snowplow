---
title: >-
    غزل شمارهٔ ۴۹
---
# غزل شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>با تو حیات و زندگی بی‌تو فنا و مردنا</p></div>
<div class="m2"><p>زانک تو آفتابی و بی‌تو بود فسردنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق بر این بساط‌ها بر کف تو چو مهره‌ای</p></div>
<div class="m2"><p>هم ز تو ماه گشتنا هم ز تو مهره بردنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت دمم چه می‌دهی دم به تو من سپرده‌ام</p></div>
<div class="m2"><p>من ز تو بی‌خبر نیم در دم دم سپردنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش به سجده می‌شدم پست خمیده چون شتر</p></div>
<div class="m2"><p>خنده زنان گشاد لب گفت درازگردنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بین که چه خواهی کردنا بین که چه خواهی کردنا</p></div>
<div class="m2"><p>گردن دراز کرده‌ای پنبه بخواهی خوردنا</p></div></div>