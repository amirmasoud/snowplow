---
title: >-
    غزل شمارهٔ ۱۰۱۴
---
# غزل شمارهٔ ۱۰۱۴

<div class="b" id="bn1"><div class="m1"><p>حکم البین بموتی و عمد</p></div>
<div class="m2"><p>رضی الصد بحینی و قصد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتح الدهر عیون حسد</p></div>
<div class="m2"><p>فر آنی بفناکم و حسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یهرق العشق دماء حقنت</p></div>
<div class="m2"><p>لیس للعشق قریب و ولد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لکن الموت حیاه لکم</p></div>
<div class="m2"><p>لکن الفقر غناء و رغد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سافروا فی سبل العشق معی</p></div>
<div class="m2"><p>لا تخافن ضلالا و رصد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لا یهولنکم بعدکم</p></div>
<div class="m2"><p>دونکم وفد وصال و مدد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فنسیم طرب اولهم</p></div>
<div class="m2"><p>یهب السالک حولا و جلد</p></div></div>