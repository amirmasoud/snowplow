---
title: >-
    غزل شمارهٔ ۲۶۷۶
---
# غزل شمارهٔ ۲۶۷۶

<div class="b" id="bn1"><div class="m1"><p>سبک بنواز ای مطرب ربایی</p></div>
<div class="m2"><p>بگردان زوتر ای ساقی شرابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که آورد آن پری رو رنگ دیگر</p></div>
<div class="m2"><p>ز چشمه زندگی جوشید آبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه آتش زد نهان دلبر به دل‌ها</p></div>
<div class="m2"><p>که مجلس پر شد از بوی کبابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا ای پیر مجلس چنگ پرفن</p></div>
<div class="m2"><p>نگویی ناله نی را جوابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی نه چشم زان چشمان چه گوید</p></div>
<div class="m2"><p>چنین بیدار باشد مست خوابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل سنگین چو یابد تاب آن چشم</p></div>
<div class="m2"><p>شود در حال او در خوشابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گدازد هر دو عالم بحر گیرد</p></div>
<div class="m2"><p>چون آن مه رو براندازد نقابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایا ساقی به اصحاب سعادت</p></div>
<div class="m2"><p>بده حالی تو باری خمر نابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قدم تا فرق پر دارید از این می</p></div>
<div class="m2"><p>که بوی شمس تبریزی بیابی</p></div></div>