---
title: >-
    غزل شمارهٔ ۲۱۲۹
---
# غزل شمارهٔ ۲۱۲۹

<div class="b" id="bn1"><div class="m1"><p>نحن الی سیدنا راجعون</p></div>
<div class="m2"><p>طیبه النفس به طایعون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیدنا یصبح یبتا عنا</p></div>
<div class="m2"><p>انفسنا نحن له بایعون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یفسد ان جاع الی مؤکل</p></div>
<div class="m2"><p>نحن الی نظرته جایعون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوف تلاقیه به میعاده</p></div>
<div class="m2"><p>تحسب انا ابدا ضایعون</p></div></div>