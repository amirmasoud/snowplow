---
title: >-
    غزل شمارهٔ ۱۱۶۵
---
# غزل شمارهٔ ۱۱۶۵

<div class="b" id="bn1"><div class="m1"><p>میر خرابات تویی ای نگار</p></div>
<div class="m2"><p>وز تو خرابات چنین بی‌قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله خرابات خراب تواند</p></div>
<div class="m2"><p>جمله اسرار ز توست آشکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان خراباتی و عمر عزیز</p></div>
<div class="m2"><p>هین که بشد عمر چنین هوشیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان و جهان جان مرا دست گیر</p></div>
<div class="m2"><p>چشم جهان حرف مرا گوش دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک کفت چشم مرا توتیاست</p></div>
<div class="m2"><p>وعده تو گوش مرا گوشوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمر کهن بر سر عشاق ریز</p></div>
<div class="m2"><p>صورت نو در دل مستان نگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساغر بازیچه فانی ببر</p></div>
<div class="m2"><p>ساغر مردانه ما را بیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش می بر سر پرهیز ریز</p></div>
<div class="m2"><p>وای بر آن زاهد پرهیزکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حق چو شراب ازلی دردهد</p></div>
<div class="m2"><p>مرد خورد باده حق مردوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرورش جان به سقاهم بود</p></div>
<div class="m2"><p>از می و از ساغر پروردگار</p></div></div>