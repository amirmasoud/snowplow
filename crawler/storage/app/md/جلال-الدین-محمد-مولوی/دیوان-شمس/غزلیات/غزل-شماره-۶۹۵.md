---
title: >-
    غزل شمارهٔ ۶۹۵
---
# غزل شمارهٔ ۶۹۵

<div class="b" id="bn1"><div class="m1"><p>این قافله بار ما ندارد</p></div>
<div class="m2"><p>از آتش یار ما ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند درخت‌های سبزند</p></div>
<div class="m2"><p>بویی ز بهار ما ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان تو چو گلشنست لیکن</p></div>
<div class="m2"><p>دلخسته به خار ما ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحریست دل تو در حقایق</p></div>
<div class="m2"><p>کو جوش کنار ما ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند که کوه برقرارست</p></div>
<div class="m2"><p>والله که قرار ما ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانی که به هر صبوح مستست</p></div>
<div class="m2"><p>بویی ز خمار ما ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن مطرب آسمان که زهره‌ست</p></div>
<div class="m2"><p>هم طاقت کار ما ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شیر خدای پرس ما را</p></div>
<div class="m2"><p>هر شیر قفار ما ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منمای تو نقد شمس تبریز</p></div>
<div class="m2"><p>آن را که عیار ما ندارد</p></div></div>