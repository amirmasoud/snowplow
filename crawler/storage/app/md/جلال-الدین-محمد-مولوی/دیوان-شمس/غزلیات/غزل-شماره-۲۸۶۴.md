---
title: >-
    غزل شمارهٔ ۲۸۶۴
---
# غزل شمارهٔ ۲۸۶۴

<div class="b" id="bn1"><div class="m1"><p>به شکرخنده اگر می‌ببرد دل ز کسی</p></div>
<div class="m2"><p>می‌دهد در عوضش جان خوشی بوالهوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه سحر حمله برد بر دو جهان خورشیدش</p></div>
<div class="m2"><p>گه به شب گشت کند بر دل و جان چون عسسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه بگوید که حذر کن شه شطرنج منم</p></div>
<div class="m2"><p>بیدقی گر ببری من برم از تو فرسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوطیانند که خود را بکشند از غیرت</p></div>
<div class="m2"><p>گر به سوی شکرش راه برد خرمگسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاره پاره کند آن طوطی مسکین خود را</p></div>
<div class="m2"><p>گر یکی پاره شکر زو ببرد مرتبسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در رخ دشمن من دوست بخندید چو برق</p></div>
<div class="m2"><p>همچو ابر این دل من پر شد و بگریست بسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل عارف تو هر دو جهان یاوه شود</p></div>
<div class="m2"><p>کی درآید به دو چشمی که تو را دید خسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جیب مریم ز دمش حامل معنی گردد</p></div>
<div class="m2"><p>که منم کز نفسی سازم عیسی نفسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجمع روح تویی جان به تو خواهد آمد</p></div>
<div class="m2"><p>تو چو بحری همه سیل‌اند و فرات و ارسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای که صالح تو و این هر دو جهان یک اشتر</p></div>
<div class="m2"><p>ما همه نعره زنان زنگله همچون جرسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نعره زنگله از جنبش اشتر باشد</p></div>
<div class="m2"><p>که شتر نقل کند از کنسی تا کنسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چراغی که بسوزد مطلب زو نوری</p></div>
<div class="m2"><p>نور موسی طلبی رو به چنان مقتبسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس کن این گفت خیال است مشو وقف خیال</p></div>
<div class="m2"><p>چونک هستت به حقیقت نظر و دسترسی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای ضیاء الحق ذوالفضل حسام الدین تو</p></div>
<div class="m2"><p>عارف طب دلی بی‌رگ و نبض و مجسی</p></div></div>