---
title: >-
    غزل شمارهٔ ۲۹۲۶
---
# غزل شمارهٔ ۲۹۲۶

<div class="b" id="bn1"><div class="m1"><p>گر در آب و گر در آتش می‌روی</p></div>
<div class="m2"><p>آن نمی‌دانم برو خوش می‌روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رخت پیداست والله رنگ او</p></div>
<div class="m2"><p>رو که سوی یار مه وش می‌روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش‌ها را پشت و پایی می‌زنی</p></div>
<div class="m2"><p>سوی نقش نامنقش می‌روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق جان‌ها می‌زند بر جان تو</p></div>
<div class="m2"><p>مست و دست انداز و سرکش می‌روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پی تو می‌دود اقبال رو</p></div>
<div class="m2"><p>گر به عرش و گر به مفرش می‌روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنک در سر داری از سودای یار</p></div>
<div class="m2"><p>چه عجب گر تو مشوش می‌روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شه صلاح الدین برآ زین شش جهت</p></div>
<div class="m2"><p>گر چه ظاهر اندر این شش می‌روی</p></div></div>