---
title: >-
    غزل شمارهٔ ۱۸۸۹
---
# غزل شمارهٔ ۱۸۸۹

<div class="b" id="bn1"><div class="m1"><p>دل دل دل تو دل مرا مرنجان</p></div>
<div class="m2"><p>چرا چرا چه معنی مرا کنی پریشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا بیا و بازآ به صلح سوی خانه</p></div>
<div class="m2"><p>مرو مرو ز پیشم کتف چنین مجنبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو صد شکرستانی ترش چه کردی ابرو</p></div>
<div class="m2"><p>سبکتر از صبایی چرا شوی گران جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم کنون ز عشق رخ چو گلشن تو</p></div>
<div class="m2"><p>فراز سرو و گلشن چو صد هزاردستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا بیا دمم ده که دمدمه لطیفت</p></div>
<div class="m2"><p>حیات دل فزاید مرا چو آب حیوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیار عشوه اینک بهای عشوه صد جان</p></div>
<div class="m2"><p>هزار جان به ارزد زهی متاع ارزان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو عقل عقل مایی چرا ز ما جدایی</p></div>
<div class="m2"><p>سری که عقل از او شد نه گیج ماند و حیران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستون این سرایی ز در برون چرایی</p></div>
<div class="m2"><p>سرا که بی‌ستون شد نه پست گشت ویران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو ماه آسمانی و ما شبیم تاری</p></div>
<div class="m2"><p>شبی که مه نباشد غلس بود فراوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو پادشاه شهری و ما کنار شهری</p></div>
<div class="m2"><p>چو شهر ماند بی‌شه چه سر بود چه سامان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مها تویی سلیمان فراق و غم چو دیوان</p></div>
<div class="m2"><p>چو دور شد سلیمان نه دست یافت شیطان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تویی به جای موسی و ما تو را عصایی</p></div>
<div class="m2"><p>بجز به کف موسی عصا نیافت برهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مسیح خوش دمی تو و ما ز گل چو مرغی</p></div>
<div class="m2"><p>دمی بدم تو بر ما بر اوج بین تو جولان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو نوح روزگاری و ما چو اهل کشتی</p></div>
<div class="m2"><p>چو نوح رفت کشتی کجا رهد ز طوفان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تویی خلیل ای جان همه جهان پرآتش</p></div>
<div class="m2"><p>که بی‌خلیل آتش نمی‌شود گلستان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو نور مصطفایی و کعبه پربتان شد</p></div>
<div class="m2"><p>هلا بیا برون کن بتان ز بیت رحمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو یوسف جمالی و چشم خلق بسته</p></div>
<div class="m2"><p>نظر ز تو گشاید چو چشم پیر کنعان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو گوهر صفایی و ما صدف به گردت</p></div>
<div class="m2"><p>صدف چه قیمت آرد چو رفت گوهر کان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو جان آفتابی که او است جان عالم</p></div>
<div class="m2"><p>سزد گرت بگویم که جان جان کیهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به غیب باشد ایمان تو غیب را عیانی</p></div>
<div class="m2"><p>که عین عین عینی و اصل اصل ایمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خمش که تا قیامت اگر دهی علامت</p></div>
<div class="m2"><p>جوی نموده باشی به ما ز گنج پنهان</p></div></div>