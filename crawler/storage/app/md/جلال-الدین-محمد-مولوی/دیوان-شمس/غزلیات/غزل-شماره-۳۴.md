---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای عاشقان ای عاشقان آمد گه وصل و لقا</p></div>
<div class="m2"><p>از آسمان آمد ندا کای ماه رویان الصلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سرخوشان ای سرخوشان آمد طرب دامن کشان</p></div>
<div class="m2"><p>بگرفته ما زنجیر او بگرفته او دامان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد شراب آتشین ای دیو غم کنجی نشین</p></div>
<div class="m2"><p>ای جان مرگ اندیش رو ای ساقی باقی درآ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای هفت گردون مست تو ما مهره‌ای در دست تو</p></div>
<div class="m2"><p>ای هست ما از هست تو در صد هزاران مرحبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مطرب شیرین نفس هر لحظه می‌جنبان جرس</p></div>
<div class="m2"><p>ای عیش زین نه بر فرس بر جان ما زن ای صبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بانگ نای خوش سمر در بانگ تو طعم شکر</p></div>
<div class="m2"><p>آید مرا شام و سحر از بانگ تو بوی وفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بار دگر آغاز کن آن پرده‌ها را ساز کن</p></div>
<div class="m2"><p>بر جمله خوبان ناز کن ای آفتاب خوش لقا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاموش کن پرده مدر سغراق خاموشان بخور</p></div>
<div class="m2"><p>ستار شو ستار شو خو گیر از حلم خدا</p></div></div>