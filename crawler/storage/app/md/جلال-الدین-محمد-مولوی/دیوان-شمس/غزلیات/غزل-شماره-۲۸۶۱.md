---
title: >-
    غزل شمارهٔ ۲۸۶۱
---
# غزل شمارهٔ ۲۸۶۱

<div class="b" id="bn1"><div class="m1"><p>چند روز است که شطرنج عجب می‌بازی</p></div>
<div class="m2"><p>دانه بوالعجب و دام عجب می‌سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی برد جان ز تو گر ز آنک تو دل سخت کنی</p></div>
<div class="m2"><p>کی برد سر ز تو گر ز آنک بدین پردازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفت حکم تو در خون شهیدان رقصد</p></div>
<div class="m2"><p>مرگ موش است ولیکن بر گربه بازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدگمان باشد عاشق تو از این‌ها دوری</p></div>
<div class="m2"><p>همه لطفی و ز سر لطف دگر آغازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو نایم ز لبت می‌چشم و می‌نالم</p></div>
<div class="m2"><p>کم زنم تا نکند کس طمع انبازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نای اگر ناله کند لیک از او بوی لبت</p></div>
<div class="m2"><p>برسد سوی دماغ و بکند غمازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو که می ناله کنی گر نه پی طراری است</p></div>
<div class="m2"><p>از گزافه تو چنین خوش دم و خوش آوازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه هر آواز گواه است خبر می‌آرد</p></div>
<div class="m2"><p>این خبر فهم کن ار همنفس آن رازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل از خویش و از اندیشه تهی شو زیرا</p></div>
<div class="m2"><p>نی تهی گشت از آن یافت ز وی دمسازی</p></div></div>