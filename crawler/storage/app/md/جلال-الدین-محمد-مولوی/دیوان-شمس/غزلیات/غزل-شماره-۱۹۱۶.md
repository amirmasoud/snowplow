---
title: >-
    غزل شمارهٔ ۱۹۱۶
---
# غزل شمارهٔ ۱۹۱۶

<div class="b" id="bn1"><div class="m1"><p>برآ بر بام و اکنون ماه نو بین</p></div>
<div class="m2"><p>درآ در باغ و اکنون سیب می چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن سیبی که بشکافد در روم</p></div>
<div class="m2"><p>رود بوی خوشش تا چین و ماچین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآ بر خرمن سیب و بکش پا</p></div>
<div class="m2"><p>ز سیب لعل کن فرش و نهالین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر سیبش لقب گویم وگر می</p></div>
<div class="m2"><p>وگر نرگس وگر گلزار و نسرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی چیز است در وی چیست کان نیست</p></div>
<div class="m2"><p>خدا پاینده دارش یا رب آمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا اکنون اگر افسانه خواهی</p></div>
<div class="m2"><p>درآ در پیش من چون شمع بنشین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی‌ترسم که بگریزی ز گوشه</p></div>
<div class="m2"><p>برآ بالا برون انداز نعلین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پهلویم نشین برچفس بر من</p></div>
<div class="m2"><p>رها کن ناز و آن خوهای پیشین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیامیز اندکی ای کان رحمت</p></div>
<div class="m2"><p>که تا گردد رخ زرد تو رنگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روا باشد وگر خود من نگویم</p></div>
<div class="m2"><p>همیشه عشوه و وعده دروغین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از این پاکی تو لیکن عاشقان را</p></div>
<div class="m2"><p>پراکنده سخن‌ها هست آیین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی اوصاف شمس الدین تبریز</p></div>
<div class="m2"><p>زهی کر و فر و امکان و تمکین</p></div></div>