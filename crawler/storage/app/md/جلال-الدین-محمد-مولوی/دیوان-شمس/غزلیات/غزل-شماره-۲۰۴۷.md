---
title: >-
    غزل شمارهٔ ۲۰۴۷
---
# غزل شمارهٔ ۲۰۴۷

<div class="b" id="bn1"><div class="m1"><p>می‌آیدم ز رنگ تو ای یار بوی آن</p></div>
<div class="m2"><p>برکنده‌ای به خشم دل از یار مهربان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آفتاب روی تو چون شکل خشم تافت</p></div>
<div class="m2"><p>پشتم خم است و سینه کبودم چو آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان تیرهای غمزه خشمین که می‌زنی</p></div>
<div class="m2"><p>صد قامت چو تیر خمیده‌ست چون کمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پرسشم ز خشم لب لعل بسته‌ای</p></div>
<div class="m2"><p>جان ماندم ز غصه این یا دل و زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف تو نردبان بده بر بام دولتی</p></div>
<div class="m2"><p>ای لطف واگرفته و بشکسته نردبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این لابه ام به ذات خدا نیست بهر جان</p></div>
<div class="m2"><p>ای هر دمی خیال تو صد جان جان جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد آر دلبرا که ز من خواستی شبی</p></div>
<div class="m2"><p>نقشی ز جان خون شده من دادمت نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانا به حق آن شب کان زلف جعد را</p></div>
<div class="m2"><p>در گردنم درافکن و سرمست می‌کشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا جان باسعادت غلطان همی‌رود</p></div>
<div class="m2"><p>چوگان دو زلف و گوی دل و دشت لامکان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرسی عدل نه تو به تبریز شمس دین</p></div>
<div class="m2"><p>تا عرش نور گیرد و حیران شود جهان</p></div></div>