---
title: >-
    غزل شمارهٔ ۲۹۴۰
---
# غزل شمارهٔ ۲۹۴۰

<div class="b" id="bn1"><div class="m1"><p>چون زخمه رجا را بر تار می‌کشانی</p></div>
<div class="m2"><p>کاهل روان ره را در کار می‌کشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عشق چون درآیی در لطف و دلربایی</p></div>
<div class="m2"><p>دامان جان بگیری تا یار می‌کشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایمن کنی تو جان را کوری رهزنان را</p></div>
<div class="m2"><p>دزدان نقد دل را بر دار می‌کشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوداییان جان را از خود دهی مفرح</p></div>
<div class="m2"><p>صفراییان زر را بس زار می‌کشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهجور خارکش را گلزار می‌نمایی</p></div>
<div class="m2"><p>گلروی خارخو را در خار می‌کشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسی خاک رو را بر بحر می‌نشانی</p></div>
<div class="m2"><p>فرعون بوش جو را در عار می‌کشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موسی عصا بگیرد تا یار خویش سازد</p></div>
<div class="m2"><p>ماری کنی عصا را چون مار می‌کشانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مار را بگیرد یابد عصای خود را</p></div>
<div class="m2"><p>این نعل بازگونه هموار می‌کشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن کو در آتش افتد راهش دهی به آبی</p></div>
<div class="m2"><p>و آن کو در آب آید در نار می‌کشانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دل چه خوش ز پرده سرمست و باده خورده</p></div>
<div class="m2"><p>سر را برهنه کرده دستار می‌کشانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را مده به غیری تا سوی خود کشاند</p></div>
<div class="m2"><p>ما را تو کش ازیرا شهوار می‌کشانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا یار زنده باشد کوهی کنی تو سدش</p></div>
<div class="m2"><p>چون در غمش بکشتی در غار می‌کشانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاموش و درکش این سر خوش خامشانه می‌خور</p></div>
<div class="m2"><p>زیرا که چون خموشی اسرار می‌کشانی</p></div></div>