---
title: >-
    غزل شمارهٔ ۱۴۸۵
---
# غزل شمارهٔ ۱۴۸۵

<div class="b" id="bn1"><div class="m1"><p>صبح است و صبوح است بر این بام برآییم</p></div>
<div class="m2"><p>از ثور گریزیم و به برج قمر آییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیکار نجوییم و ز اغیار نگوییم</p></div>
<div class="m2"><p>هنگام وصال است بدان خوش صور آییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو گلستان و لب تو شکرستان</p></div>
<div class="m2"><p>در سایه این هر دو همه گلشکر آییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید رخ خوب تو چون تیغ کشیده‌ست</p></div>
<div class="m2"><p>شاید که به پیش تو چو مه شب‌سپر آییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف تو شب قدر و رخ تو همه نوروز</p></div>
<div class="m2"><p>ما واسطه روز و شبش چون سحر آییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این شکل ندانیم که آن شکل نمودی</p></div>
<div class="m2"><p>ور زانک دگرگونه نمایی دگر آییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید جهانی تو و ما ذره پنهان</p></div>
<div class="m2"><p>درتاب در این روزن تا در نظر آییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید چو از روی تو سرگشته و خیره‌ست</p></div>
<div class="m2"><p>ما ذره عجب نیست که خیره نگر آییم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم چو بیایید دو صد در بگشایید</p></div>
<div class="m2"><p>گفتند که این هست ولیکن اگر آییم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم که چو دریا به سوی جوی نیاید</p></div>
<div class="m2"><p>چون آب روان جانب او در سفر آییم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای ناطقه غیب تو برگوی که تا ما</p></div>
<div class="m2"><p>از مخبر و اخبار خوشت خوش خبر آییم</p></div></div>