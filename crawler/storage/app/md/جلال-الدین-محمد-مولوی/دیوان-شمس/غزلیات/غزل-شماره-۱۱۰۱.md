---
title: >-
    غزل شمارهٔ ۱۱۰۱
---
# غزل شمارهٔ ۱۱۰۱

<div class="b" id="bn1"><div class="m1"><p>نرم نرمک سوی رخسارش نگر</p></div>
<div class="m2"><p>چشم بگشا چشم خمارش نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بخندد آن عقیق قیمتی</p></div>
<div class="m2"><p>صد هزاران دل گرفتارش نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر برآر از مستی و بیدار شو</p></div>
<div class="m2"><p>کار و بار و بخت بیدارش نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندرآ در باغ بی‌پایان دل</p></div>
<div class="m2"><p>میوه شیرین بسیارش نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخه‌های سبز رقصانش ببین</p></div>
<div class="m2"><p>لطف آن گل‌های بی‌خارش نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند بینی صورت نقش جهان</p></div>
<div class="m2"><p>بازگرد و سوی اسرارش نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرص بین در طبع حیوان و نبات</p></div>
<div class="m2"><p>بعد از آن سیری و ایثارش نگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرص و سیری صنعت عشقست و بس</p></div>
<div class="m2"><p>گر ندیدی عشق را کارش نگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ندیدی عشق رنگ آمیز را</p></div>
<div class="m2"><p>رنگ روی عاشق زارش نگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با چنین دشوار بازاری که اوست</p></div>
<div class="m2"><p>با زر و بی‌زر خریدارش نگر</p></div></div>