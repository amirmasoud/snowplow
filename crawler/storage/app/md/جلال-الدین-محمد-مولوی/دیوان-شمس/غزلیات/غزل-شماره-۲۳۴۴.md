---
title: >-
    غزل شمارهٔ ۲۳۴۴
---
# غزل شمارهٔ ۲۳۴۴

<div class="b" id="bn1"><div class="m1"><p>مبارک باد آمد ماه روزه</p></div>
<div class="m2"><p>رهت خوش باد ای همراه روزه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم بر بام تا مه را ببینم</p></div>
<div class="m2"><p>که بودم من به جان دلخواه روزه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر کردم کلاه از سر بیفتاد</p></div>
<div class="m2"><p>سرم را مست کرد آن شاه روزه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسلمانان سرم مست است از آن روز</p></div>
<div class="m2"><p>زهی اقبال و بخت و جاه روزه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجز این ماه ماهی هست پنهان</p></div>
<div class="m2"><p>نهان چون ترک در خرگاه روزه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان مه ره برد آن کس که آید</p></div>
<div class="m2"><p>در این مه خوش به خرمنگاه روزه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخ چون اطلسش گر زرد گردد</p></div>
<div class="m2"><p>بپوشد خلعت از دیباه روزه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دعاها اندر این مه مستجاب است</p></div>
<div class="m2"><p>فلک‌ها را بدرد آه روزه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو یوسف ملک مصر عشق گیرد</p></div>
<div class="m2"><p>کسی کو صبر کرد در چاه روزه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سحوری کم زن ای نطق و خمش کن</p></div>
<div class="m2"><p>ز روزه خود شوند آگاه روزه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا ای شمس دین و فخر تبریز</p></div>
<div class="m2"><p>تویی سرلشکر اسپاه روزه</p></div></div>