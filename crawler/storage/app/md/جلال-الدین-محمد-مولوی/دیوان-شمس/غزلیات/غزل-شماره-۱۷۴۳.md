---
title: >-
    غزل شمارهٔ ۱۷۴۳
---
# غزل شمارهٔ ۱۷۴۳

<div class="b" id="bn1"><div class="m1"><p>اگر چه ما نه خروس و نه ماکیان داریم</p></div>
<div class="m2"><p>ز بیضه سر کن و بنگر که ما کیان داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آفتاب حقایق به هر سحر گوییم</p></div>
<div class="m2"><p>تو جمله جانی و ما از تو نیم جان داریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از صفات تو نتوان نشان نمود ولی</p></div>
<div class="m2"><p>ز بی‌نشانی اوصاف او نشان داریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل چو شبنم ما را به بحر بازرسان</p></div>
<div class="m2"><p>که دم به دم ز غریبی دو صد زیان داریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو یوسف از کف گرگان دریده پیرهنم</p></div>
<div class="m2"><p>ولی ز همت یعقوب پاسبان داریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دام تو که همه دام‌ها زبون ویند</p></div>
<div class="m2"><p>که هر قدم ز قدم دام امتحان داریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیک بندگشا هر دم آن کند با ما</p></div>
<div class="m2"><p>که مادر و پدر و عم مگر که آن داریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنوش کردن زهر این چه جرات است مگر</p></div>
<div class="m2"><p>ز کان فضل تو تریاق بی‌کران داریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خرج کردن این نقد عمر مبتشریم</p></div>
<div class="m2"><p>ز عمربخش مگر عمر جاودان داریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگیرد آینه زنگار هیچ اگر گیرد</p></div>
<div class="m2"><p>ز عین زنگ بدان روی دیدمان داریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یقین بنشکند آن نردبان وگر شکند</p></div>
<div class="m2"><p>ز عین رخنه اشکست نردبان داریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رهین روز چرایی چو شب کند روزی</p></div>
<div class="m2"><p>مکان بهل که مکانی ز لامکان داریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهار حله دریدی ز رشک و زرد شدی</p></div>
<div class="m2"><p>اگر بدیش خبر کاین چنین خزان داریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دهان پر است و خموشم که تا بگویی تو</p></div>
<div class="m2"><p>کز آن لب شکرینت شکرفشان داریم</p></div></div>