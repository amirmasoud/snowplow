---
title: >-
    غزل شمارهٔ ۸۸۴
---
# غزل شمارهٔ ۸۸۴

<div class="b" id="bn1"><div class="m1"><p>پرده دل می‌زند زهره هم از بامداد</p></div>
<div class="m2"><p>مژده که آن بوطرب داد طرب‌ها بداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر کرم کرد جوش پنبه برون کن ز گوش</p></div>
<div class="m2"><p>آنچ کفش داد دوش ما و تو را نوش باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق همایون پیست خطبه به نام ویست</p></div>
<div class="m2"><p>از سر ما کم مباد سایه این کیقباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی خوشش چون شرار خوی خوشش نوبهار</p></div>
<div class="m2"><p>وان دگرش زینهار او هو رب العباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اول روز این خمار کرد مرا بی‌قرار</p></div>
<div class="m2"><p>می‌کشدم ابروار عشق تو چون تندباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست دل از رنج رست گر چه دلارام مست</p></div>
<div class="m2"><p>بست سر زلف بست خواجه ببین این گشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کشدم موکشان من ترش و سرگران</p></div>
<div class="m2"><p>رو که مراد جهان می‌کشدم بی‌مراد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل بر آن عقل ساز ناز همی‌کرد ناز</p></div>
<div class="m2"><p>شکر کز آن گشت باز تا به مقام اوفتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای به گل بوده‌ام زانک دودل بوده‌ام</p></div>
<div class="m2"><p>شکر که دودل نماند یک دله شد دل نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاف دل از آسمان لاف تن از ریسمان</p></div>
<div class="m2"><p>بگسلم این ریسمان بازروم در معاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلبر روز الست چیز دگر گفت پست</p></div>
<div class="m2"><p>هیچ کسی هست کو آرد آن را به یاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت به تو تاختم بهر خودت ساختم</p></div>
<div class="m2"><p>ساخته خویش را من ندهم در مزاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم تو کیستی گفت مراد همه</p></div>
<div class="m2"><p>گفتم من کیستم گفت مراد مراد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مفتعلن فاعلات رفته بدم از صفات</p></div>
<div class="m2"><p>محو شده پیش ذات دل به سخن چون فتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>داد دل و عقل و جان مفخر تبریزیان</p></div>
<div class="m2"><p>از مدد این سه داد یافت زمانه سداد</p></div></div>