---
title: >-
    غزل شمارهٔ ۱۱۸۴
---
# غزل شمارهٔ ۱۱۸۴

<div class="b" id="bn1"><div class="m1"><p>بیا با تو مرا کارست امروز</p></div>
<div class="m2"><p>مرا سودای گلزارست امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا دلدار من دلداریی کن</p></div>
<div class="m2"><p>که روز لطف و ایثارست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من جامه‌ها را می‌دراند</p></div>
<div class="m2"><p>که روز وصل دلدارست امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخندان جان ما را از جمالی</p></div>
<div class="m2"><p>که بر گلبرگ و گلنارست امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا جان‌ها بر آن لب مست گشتند</p></div>
<div class="m2"><p>که آن جا نقل بسیارست امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای طوطیان آفاق پر شد</p></div>
<div class="m2"><p>که شکرها به خروارست امروز</p></div></div>