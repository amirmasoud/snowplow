---
title: >-
    غزل شمارهٔ ۷۰۴
---
# غزل شمارهٔ ۷۰۴

<div class="b" id="bn1"><div class="m1"><p>آن جا که چو تو نگار باشد</p></div>
<div class="m2"><p>سالوس و حفاظ عار باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالوس و حیل کنار گیرد</p></div>
<div class="m2"><p>چون رحمت بی‌کنار باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوسی به دغا ربودم از تو</p></div>
<div class="m2"><p>ای دوست دغا سه بار باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز وفا کن آن سوم را</p></div>
<div class="m2"><p>امروز یکی هزار باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من جوی و تو آب و بوسه آب</p></div>
<div class="m2"><p>هم بر لب جویبار باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بوسه آب بر لب جوی</p></div>
<div class="m2"><p>اشکوفه و سبزه زار باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سبزه چه کم شود که سبزه</p></div>
<div class="m2"><p>در دیده خیره خار باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موسی ز عصا چرا گریزد</p></div>
<div class="m2"><p>گر بر فرعون مار باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر فرعونان که نیل خون گشت</p></div>
<div class="m2"><p>بر مؤمن خوشگوار باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرگز نرمد خلیل ز آتش</p></div>
<div class="m2"><p>گر بر نمرود نار باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یعقوب کجا رمد ز یوسف</p></div>
<div class="m2"><p>گر بر پسرانش بار باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن باد بهار جان باغست</p></div>
<div class="m2"><p>بر شوره اگر غبار باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان باغ درخت برگ یابد</p></div>
<div class="m2"><p>اشکوفه بر او سوار باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>احمد چو تو راست پس ز بوجهل</p></div>
<div class="m2"><p>عشقا سزدت که عار باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این را بر دست و آن بدین مات</p></div>
<div class="m2"><p>کار دنیا قمار باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن کس که ز بخت خود گریزد</p></div>
<div class="m2"><p>بگریخته شرمسار باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هین دام منه به صید خرگوش</p></div>
<div class="m2"><p>تا شیر تو را شکار باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای دل ز عبیر عشق کم گوی</p></div>
<div class="m2"><p>خود بو برد آن که یار باشد</p></div></div>