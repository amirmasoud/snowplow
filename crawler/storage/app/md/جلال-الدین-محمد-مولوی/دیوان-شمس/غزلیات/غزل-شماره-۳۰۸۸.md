---
title: >-
    غزل شمارهٔ ۳۰۸۸
---
# غزل شمارهٔ ۳۰۸۸

<div class="b" id="bn1"><div class="m1"><p>حرام گشت از این پس فغان و غمخواری</p></div>
<div class="m2"><p>بهشت گشت جهان زانک تو جهان داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مثال ده که نروید ز سینه خار غمی</p></div>
<div class="m2"><p>مثال ده که کند ابر غم گهرباری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مثال ده که نیاید ز صبح غمازی</p></div>
<div class="m2"><p>مثال ده که نگردد جهان به شب تاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مثال ده که نریزد گلی ز شاخ درخت</p></div>
<div class="m2"><p>مثال ده که کند توبه خار از خاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مثال ده که رهد حرص از گداچشمی</p></div>
<div class="m2"><p>مثال ده که طمع وارهد ز طراری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثال گر ندهی حسن بی‌مثال تو بس</p></div>
<div class="m2"><p>که مستی دل و جانست و خصم هشیاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شب به خلوت معراج تو مشرف شد</p></div>
<div class="m2"><p>به آفتاب نظر می‌کند به صد خواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز رشک نیشکرت نی هزار ناله کند</p></div>
<div class="m2"><p>ز چنگ هجر تو گیرند چنگ‌ها زاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تف عشق تو سوزی است در دل آتش</p></div>
<div class="m2"><p>هم از هوای تو دارد هوا سبکساری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برای خدمت تو آب در سجود رود</p></div>
<div class="m2"><p>ز درد توست بر این خاک رنگ بیماری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عشق تابش خورشید تو به وقت طلوع</p></div>
<div class="m2"><p>بلند کرد سر آن کوه نی ز جباری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که تا نخست برو تابد آن تف خورشید</p></div>
<div class="m2"><p>نخست او کند آن نور را خریداری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تنا ز کوه بیاموز سر به بالا دار</p></div>
<div class="m2"><p>که کان عشق خدایی نه کم ز کهساری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مکن به زیر و به بالا به لامکان کن سر</p></div>
<div class="m2"><p>که هست شش جهت آن جا تو را نگوساری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به دل نگر که دل تو برون شش جهت است</p></div>
<div class="m2"><p>که دل تو را برهاند از این جگرخواری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روانه باش به اسرار و می تماشا کن</p></div>
<div class="m2"><p>ز آسمان بپذیر این لطیف رفتاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو غوره از ترشی رو به سوی انگوری</p></div>
<div class="m2"><p>چو نی برو ز نیی جانب شکرباری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حلاوت شکر او گلوی من بگرفت</p></div>
<div class="m2"><p>بماندم از رخ خوبش ز خوب گفتاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگو به عشق که ای عشق خوش گلوگیری</p></div>
<div class="m2"><p>گه جفا و وفا خوب و خوب کرداری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گلو چو سخت بگیری سبک برآید جان</p></div>
<div class="m2"><p>درآیدم ز تو جان چون گلوم افشاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گلوی خود به رسن زان سپرد خوش منصور</p></div>
<div class="m2"><p>دلا چو بوی بری صد گلو تو بسپاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز کودکی تو به پیری روانه‌ای و دوان</p></div>
<div class="m2"><p>ولیکن آن حرکت نیست فاش و اظهاری</p></div></div>