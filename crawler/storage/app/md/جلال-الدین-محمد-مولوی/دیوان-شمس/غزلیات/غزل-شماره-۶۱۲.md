---
title: >-
    غزل شمارهٔ ۶۱۲
---
# غزل شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>بگذشت مه روزه عید آمد و عید آمد</p></div>
<div class="m2"><p>بگذشت شب هجران معشوق پدید آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن صبح چو صادق شد عذرای تو وامق شد</p></div>
<div class="m2"><p>معشوق تو عاشق شد شیخ تو مرید آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد جنگ و نظر آمد شد زهر و شکر آمد</p></div>
<div class="m2"><p>شد سنگ و گهر آمد شد قفل و کلید آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان از تن آلوده هم پاک به پاکی رفت</p></div>
<div class="m2"><p>هر چند چو خورشیدی بر پاک و پلید آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لذت جام تو دل ماند به دام تو</p></div>
<div class="m2"><p>جان نیز چو واقف شد او نیز دوید آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس توبه شایسته بر سنگ تو بشکسته</p></div>
<div class="m2"><p>بس زاهد و بس عابد کو خرقه درید آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باغ از دی نامحرم سه ماه نمی‌زد دم</p></div>
<div class="m2"><p>بر بوی بهار تو از غیب دمید آمد</p></div></div>