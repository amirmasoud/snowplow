---
title: >-
    غزل شمارهٔ ۱۷۶۷
---
# غزل شمارهٔ ۱۷۶۷

<div class="b" id="bn1"><div class="m1"><p>تو چه دانی که ما چه مرغانیم</p></div>
<div class="m2"><p>هر نفس زیر لب چه می خوانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به دست آورد کسی ما را</p></div>
<div class="m2"><p>ما گهی گنج گاه ویرانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ از بهر ماست در گردش</p></div>
<div class="m2"><p>زان سبب همچو چرخ گردانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی بمانیم اندر این خانه</p></div>
<div class="m2"><p>چون در این خانه جمله مهمانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به صورت گدای این کوییم</p></div>
<div class="m2"><p>به صفت بین که ما چه سلطانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونک فردا شهیم در همه مصر</p></div>
<div class="m2"><p>چه غم امروز اگر به زندانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا در این صورتیم از کس ما</p></div>
<div class="m2"><p>هم نرنجیم و هم نرنجانیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمس تبریز چونک شد مهمان</p></div>
<div class="m2"><p>صد هزاران هزار چندانیم</p></div></div>