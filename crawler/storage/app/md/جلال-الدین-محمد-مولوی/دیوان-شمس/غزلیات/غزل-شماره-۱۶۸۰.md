---
title: >-
    غزل شمارهٔ ۱۶۸۰
---
# غزل شمارهٔ ۱۶۸۰

<div class="b" id="bn1"><div class="m1"><p>من اگر پرغم اگر شادانم</p></div>
<div class="m2"><p>عاشق دولت آن سلطانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که خاک قدمش تاج من است</p></div>
<div class="m2"><p>اگرم تاج دهی نستانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا لب قند خوشش پندم داد</p></div>
<div class="m2"><p>قند روید بن هر دندانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلم ار چند که خارم در پاست</p></div>
<div class="m2"><p>یوسفم گر چه در این زندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کی یعقوب من است او را من</p></div>
<div class="m2"><p>مونس زاویه احزانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وصال شب او همچو نیم</p></div>
<div class="m2"><p>قند می نوشم و در افغانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای من گر چه در این گل مانده‌ست</p></div>
<div class="m2"><p>نه که من سرو چنین بستانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز جهان گر پنهانم چه عجب</p></div>
<div class="m2"><p>که نهان باشد جان من جانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه پرخارم سر تا به قدم</p></div>
<div class="m2"><p>کوری خار چو گل خندانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوده‌ام مؤمن توحید کنون</p></div>
<div class="m2"><p>مؤمنان را پس از این ایمانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سایه شخصم و اندازه او</p></div>
<div class="m2"><p>قامتش چند بود چندانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کی او سایه ندارد چو فلک</p></div>
<div class="m2"><p>او بداند که ز خورشیدانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قیمتم نبود هر چند زرم</p></div>
<div class="m2"><p>که به بازار نیم در کانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من درون دل این سنگ دلان</p></div>
<div class="m2"><p>چون زر و خاک به کان یک سانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چونک از کان جهان بازرهم</p></div>
<div class="m2"><p>زان سوی کون و مکان من دانم</p></div></div>