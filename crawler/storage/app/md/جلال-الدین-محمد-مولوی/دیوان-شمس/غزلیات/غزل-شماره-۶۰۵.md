---
title: >-
    غزل شمارهٔ ۶۰۵
---
# غزل شمارهٔ ۶۰۵

<div class="b" id="bn1"><div class="m1"><p>ای دوست شکر خوشتر یا آنک شکر سازد</p></div>
<div class="m2"><p>ای دوست قمر خوشتر یا آنک قمر سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار شکرها را بگذار قمرها را</p></div>
<div class="m2"><p>او چیز دگر داند او چیز دگر سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بحر عجایب‌ها باشد به جز از گوهر</p></div>
<div class="m2"><p>اما نه چو سلطانی کو بحر و درر سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز آب دگر آبی از نادره دولابی</p></div>
<div class="m2"><p>بی شبهه و بی‌خوابی او قوت جگر سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی عقل نتان کردن یک صورت گرمابه</p></div>
<div class="m2"><p>چون باشد آن علمی کو عقل و خبر سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی علم نمی‌تانی کز پیه کشی روغن</p></div>
<div class="m2"><p>بنگر تو در آن علمی کز پیه نظر سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان‌ها است برآشفته ناخورده و ناخفته</p></div>
<div class="m2"><p>از بهر عجب بزمی کو وقت سحر سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شاد سحرگاهی کان حسرت هر ماهی</p></div>
<div class="m2"><p>بر گرد میان من دو دست کمر سازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌خندد این گردون بر سبلت آن مفتون</p></div>
<div class="m2"><p>خود را پی دو سه خر آن مسخره خر سازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن خر به مثال جو در زر فکند خود را</p></div>
<div class="m2"><p>غافل بود از شاهی کز سنگ گهر سازد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس کردم و بس کردم من ترک نفس کردم</p></div>
<div class="m2"><p>خود گوید جانانی کز گوش بصر سازد</p></div></div>