---
title: >-
    غزل شمارهٔ ۳۱۶۱
---
# غزل شمارهٔ ۳۱۶۱

<div class="b" id="bn1"><div class="m1"><p>خامشی ناطقی مگر جانی</p></div>
<div class="m2"><p>می‌زنی نعره‌های پنهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چو باغی و صورتت برگی</p></div>
<div class="m2"><p>باغ چه صد هزار چندانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تو باغ حیات زندانیست</p></div>
<div class="m2"><p>هست مردن خلاص زندانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو بحری و صورتت ابرست</p></div>
<div class="m2"><p>فیض دل قطره‌های مرجانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای یکی گو شده یکی گویان</p></div>
<div class="m2"><p>پیش حکمت که شاه چوگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا یکی گو نشد اگر چه زرست</p></div>
<div class="m2"><p>گر چه نیکوست نیست میدانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پهلوی اعتراض را بتراش</p></div>
<div class="m2"><p>گر تو چون گوی چست و گردانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پهلوی اعتراض در ابلیس</p></div>
<div class="m2"><p>گشت مردود رد ربانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس به خراط خویش را بسپار</p></div>
<div class="m2"><p>تا یکی گو شوی اگر آنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مانعست اعتراض ابلیسی</p></div>
<div class="m2"><p>از یکی گویی و یکی دانی</p></div></div>