---
title: >-
    غزل شمارهٔ ۲۸۸۲
---
# غزل شمارهٔ ۲۸۸۲

<div class="b" id="bn1"><div class="m1"><p>تیغ را گر تو چو خورشید دمی رنده زنی</p></div>
<div class="m2"><p>بر سر و سبلت این خنده زنان خنده زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ژنده پوشیدی و جامه ملکی برکندی</p></div>
<div class="m2"><p>پاره پاره دل ما را تو بر آن ژنده زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کی بندی است از این آب و از این گل برهد</p></div>
<div class="m2"><p>گر تو یک بند از آن طره بر این بنده زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا عقل کجا ماند یا شرم و ادب</p></div>
<div class="m2"><p>زان می لعل چو بر مردم شرمنده زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه فربه شود آن سان که نگنجد در چرخ</p></div>
<div class="m2"><p>گر تو تابی ز رخت بر مه تابنده زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه می‌گوید با زهره که گر مست شوی</p></div>
<div class="m2"><p>ز آنچ من مست شدم ضرب پراکنده زنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه تا ماهی از این ساقی جان سرمستند</p></div>
<div class="m2"><p>نقد بستان تو چرا لاف ز آینده زنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیز کامروز همایون و خوش و فرخنده‌ست</p></div>
<div class="m2"><p>خاصه که چشم بر آن چهره فرخنده زنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر باز از کله و پاش از این کنده غمی است</p></div>
<div class="m2"><p>برهد پاش اگر تیشه بر این کنده زنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هله ای باز کله بازده و پر بگشا</p></div>
<div class="m2"><p>وقت آن شد که بر آن دولت پاینده زنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو منصور تو بر دار کن این ناطقه را</p></div>
<div class="m2"><p>چو زنان چند بر این پنبه و پاغنده زنی</p></div></div>