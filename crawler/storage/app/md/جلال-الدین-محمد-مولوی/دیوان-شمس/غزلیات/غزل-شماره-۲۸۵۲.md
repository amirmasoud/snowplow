---
title: >-
    غزل شمارهٔ ۲۸۵۲
---
# غزل شمارهٔ ۲۸۵۲

<div class="b" id="bn1"><div class="m1"><p>چو یقین شده‌ست دل را که تو جان جان جانی</p></div>
<div class="m2"><p>بگشا در عنایت که ستون صد جهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو فراق گشت سرکش بزنی تو گردنش خوش</p></div>
<div class="m2"><p>به قصاص عاشقانت که تو صارم زمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو وصال گشت لاغر تو بپرورش به ساغر</p></div>
<div class="m2"><p>همه چیز را به پیشت خورشی است رایگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حمل رسید آخر به سعادت آفتابت</p></div>
<div class="m2"><p>که جهان پیر یابد ز تو تابش جوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سماع‌هاست در جان چه قرابه‌های ریزان</p></div>
<div class="m2"><p>که به گوش می‌رسد زان دف و بربط و اغانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه پر است این گلستان ز دم هزاردستان</p></div>
<div class="m2"><p>که ز های و هوی مستان تو می از قدح ندانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه شاخه‌ها شکفته ملکان قدح گرفته</p></div>
<div class="m2"><p>همگان ز خویش رفته به شراب آسمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برسان سلام جانم تو بدان شهان ولیکن</p></div>
<div class="m2"><p>تو کسی به هش نیابی که سلامشان رسانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پشه نیز باده خورده سر و ریش یاوه کرده</p></div>
<div class="m2"><p>نمرود را به دشنه ز وجود کرده فانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو به پشه این رساند تو بگو به پیل چه دهد</p></div>
<div class="m2"><p>چه کنم به شرح ناید می جام لامکانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شراب جان پذیرش سگ کهف شیرگیرش</p></div>
<div class="m2"><p>که به گرد غار مستان نکند به جز شبانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو سگی چنین ز خود شد تو ببین که شیر شرزه</p></div>
<div class="m2"><p>چو وفا کند چه یابد ز رحیق آن اوانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تبریز مشرقی شد به طلوع شمس دینی</p></div>
<div class="m2"><p>که از او رسد شرارت به کواکب معانی</p></div></div>