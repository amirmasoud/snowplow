---
title: >-
    غزل شمارهٔ ۵۲۵
---
# غزل شمارهٔ ۵۲۵

<div class="b" id="bn1"><div class="m1"><p>بی گاه شد بی‌گاه شد خورشید اندر چاه شد</p></div>
<div class="m2"><p>خیزید ای خوش طالعان وقت طلوع ماه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی به سوی جام رو ای پاسبان بر بام رو</p></div>
<div class="m2"><p>ای جان بی‌آرام رو کان یار خلوت خواه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشکی که چشم افروختی صبری که خرمن سوختی</p></div>
<div class="m2"><p>عقلی که راه آموختی در نیم شب گمراه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان‌های باطن روشنان شب را به دل روشن کنان</p></div>
<div class="m2"><p>هندوی شب نعره زنان کان ترک در خرگاه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد ز بازی‌های خوش بی‌ذوق رود فرزین شود</p></div>
<div class="m2"><p>در سایه فرخ رخی بیدق برفت و شاه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب روح‌ها واصل شود مقصودها حاصل شود</p></div>
<div class="m2"><p>چون روز روشن دل شود هر کو ز شب آگاه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای روز چون حشری مگر وی شب شب قدری مگر</p></div>
<div class="m2"><p>یا چون درخت موسیی کو مظهر الله شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب ماه خرمن می‌کند ای روز زین بر گاو نه</p></div>
<div class="m2"><p>بنگر که راه کهکشان از سنبله پرکاه شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چاه شب غافل مشو در دلو گردون دست زن</p></div>
<div class="m2"><p>یوسف گرفت آن دلو را از چاه سوی جاه شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تیره شب چون مصطفی می‌رو طلب می‌کن صفا</p></div>
<div class="m2"><p>کان شه ز معراج شبی بی‌مثل و بی‌اشباه شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاموش شد عالم به شب تا چست باشی در طلب</p></div>
<div class="m2"><p>زیرا که بانگ و عربده تشویش خلوتگاه شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای شمس تبریزی که تو از پرده شب فارغی</p></div>
<div class="m2"><p>لاشرقی و لاغربیی اکنون سخن کوتاه شد</p></div></div>