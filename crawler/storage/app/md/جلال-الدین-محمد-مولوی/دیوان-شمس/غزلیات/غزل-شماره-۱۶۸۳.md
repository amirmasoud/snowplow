---
title: >-
    غزل شمارهٔ ۱۶۸۳
---
# غزل شمارهٔ ۱۶۸۳

<div class="b" id="bn1"><div class="m1"><p>من که حیران ز ملاقات توام</p></div>
<div class="m2"><p>چون خیالی ز خیالات توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مراعات کنی دلجویی</p></div>
<div class="m2"><p>اه که بی‌دل ز مراعات توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات من نقش صفات خوش توست</p></div>
<div class="m2"><p>من مگر خود صفت ذات توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر کرامات ببخشد کرمت</p></div>
<div class="m2"><p>مو به مو لطف و کرامات توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش و اندیشه من از دم توست</p></div>
<div class="m2"><p>گویی الفاظ و عبارات توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه شه بودم و گاهت بنده</p></div>
<div class="m2"><p>این زمان هر دو نیم مات توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل زجاج آمد و نورت مصباح</p></div>
<div class="m2"><p>من بی‌دل شده مشکات توام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مهندس که تو را لوحم و خاک</p></div>
<div class="m2"><p>چون رقم محو تو و اثبات توام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه کنم ذکر که من ذکر توام</p></div>
<div class="m2"><p>چه کنم رای که رایات توام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنریهم شد و فی انفسهم</p></div>
<div class="m2"><p>هم توام خوان که ز آیات توام</p></div></div>