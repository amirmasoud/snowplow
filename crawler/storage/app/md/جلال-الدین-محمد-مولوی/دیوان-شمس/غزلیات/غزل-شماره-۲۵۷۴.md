---
title: >-
    غزل شمارهٔ ۲۵۷۴
---
# غزل شمارهٔ ۲۵۷۴

<div class="b" id="bn1"><div class="m1"><p>از آتش ناپیدا دارم دل بریانی</p></div>
<div class="m2"><p>فریاد مسلمانان از دست مسلمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهد و شکرش گویم کان گهرش گویم</p></div>
<div class="m2"><p>شمع و سحرش خوانم یا نادره سلطانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین فتنه و غوغایی آتش زده هر جایی</p></div>
<div class="m2"><p>وز آتش و دود ما برخاسته ایوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با این همه سلطانی آن خصم مسلمانی</p></div>
<div class="m2"><p>بربود به قهر از من در راه حرمدانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشاد حرمدانم بربود دل و جانم</p></div>
<div class="m2"><p>آن کس که به پیش او جانی به یکی نانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دوش ز بوی او رفتم سر کوی او</p></div>
<div class="m2"><p>ناگاه پدید آمد باغی و گلستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن جا دل و دلداری هم عالم اسراری</p></div>
<div class="m2"><p>هم واقف و بیداری هم شهره و پنهانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خدمت خاک او عیشی و تماشایی</p></div>
<div class="m2"><p>در آتش عشق او هر چشمه حیوانی</p></div></div>