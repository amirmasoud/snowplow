---
title: >-
    غزل شمارهٔ ۲۷۳۸
---
# غزل شمارهٔ ۲۷۳۸

<div class="b" id="bn1"><div class="m1"><p>من پار بخورده‌ام شرابی</p></div>
<div class="m2"><p>امسال چه مستم و خرابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من پار ز آتشی گذشتم</p></div>
<div class="m2"><p>امسال چرا شدم کبابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من تشنه به آب جوی رفتم</p></div>
<div class="m2"><p>ماهی دیدم میان آبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیران همه ماهتاب جویند</p></div>
<div class="m2"><p>من شیرم و یار ماهتابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از درد مپرس رنگ رخ بین</p></div>
<div class="m2"><p>تا رنگ بگویدت جوابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانم مست است و تن خراب است</p></div>
<div class="m2"><p>مستی است نشسته در خرابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این هر دو چنین و دل چنینتر</p></div>
<div class="m2"><p>کز غم چو خری است در خلابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک لحظه مشو ملول بشنو</p></div>
<div class="m2"><p>تا باشدت از خدا ثوابی</p></div></div>