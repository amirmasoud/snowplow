---
title: >-
    غزل شمارهٔ ۲۴۰۸
---
# غزل شمارهٔ ۲۴۰۸

<div class="b" id="bn1"><div class="m1"><p>چو آفتاب برآمد ز قعر آب سیاه</p></div>
<div class="m2"><p>ز ذره ذره شنو لا اله الا الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه جای ذره که چون آفتاب جان آمد</p></div>
<div class="m2"><p>ز آفتاب ربودند خود قبا و کلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب و گل چو برآمد مه دل آدم وار</p></div>
<div class="m2"><p>صد آفتاب چو یوسف فروشود در چاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری ز خاک برآور که کم ز مور نه‌ای</p></div>
<div class="m2"><p>خبر ببر بر موران ز دشت و خرمنگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن به دانه پوسیده مور قانع شد</p></div>
<div class="m2"><p>که او ز سنبل سرسبز ما نبود آگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو به مور بهار است و دست و پا داری</p></div>
<div class="m2"><p>چرا ز گور نسازی به سوی صحرا راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه جای مور سلیمان درید جامه شوق</p></div>
<div class="m2"><p>مرا مگیر خدا زین مثال‌های تباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی به قد خریدار می‌برند قبا</p></div>
<div class="m2"><p>اگر چه جامه دراز است هست قد کوتاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیار قد درازی که تا فروبریم</p></div>
<div class="m2"><p>قبا که پیش درازیش بسکلد زه ماه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خموش کردم از این پس که از خموشی من</p></div>
<div class="m2"><p>جدا شود حق و باطل چنانک دانه ز کاه</p></div></div>