---
title: >-
    غزل شمارهٔ ۷۲۹
---
# غزل شمارهٔ ۷۲۹

<div class="b" id="bn1"><div class="m1"><p>اینک آن جویی که چرخ سبز را گردان کند</p></div>
<div class="m2"><p>اینک آن رویی که ماه و زهره را حیران کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینک آن چوگان سلطانی که در میدان روح</p></div>
<div class="m2"><p>هر یکی گو را به وحدت سالک میدان کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینک آن نوحی که لوح معرفت کشتی اوست</p></div>
<div class="m2"><p>هر که در کشتیش ناید غرقه طوفان کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که از وی خرقه پوشد برکشد خرقه فلک</p></div>
<div class="m2"><p>هر که از وی لقمه یابد حکمتش لقمان کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ترتیب زمستان و بهارت با شهی</p></div>
<div class="m2"><p>بر من این دم را کند دی بر تو تابستان کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خار و گل پیشش یکی آمد که او از نوک خار</p></div>
<div class="m2"><p>بر یکی کس خار و بر دیگر کسی بستان کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که در آبی گریزد ز امر او آتش شود</p></div>
<div class="m2"><p>هر که در آتش شود از بهر او ریحان کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بر این برهان بگویم زانک آن برهان من</p></div>
<div class="m2"><p>گر همه شبهه‌ست او آن شبهه را برهان کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه نگری در دیو مردم این نگر کو دم به دم</p></div>
<div class="m2"><p>آدمی را دیو سازد دیو را انسان کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اینک آن خضری که میرآب حیوان گشته بود</p></div>
<div class="m2"><p>زنده را بخشد بقا و مرده را حیوان کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه نامش فلسفی خود علت اولی نهد</p></div>
<div class="m2"><p>علت آن فلسفی را از کرم درمان کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوهر آیینه کلست با او دم مزن</p></div>
<div class="m2"><p>کو از این دم بشکند چون بشکند تاوان کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دم مزن با آینه تا با تو او همدم بود</p></div>
<div class="m2"><p>گر تو با او دم زنی او روی خود پنهان کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کفر و ایمان تو و غیر تو در فرمان اوست</p></div>
<div class="m2"><p>سر مکش از وی که چشمش غارت ایمان کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که نادان ساخت خود را پیش او دانا شود</p></div>
<div class="m2"><p>ور بر او دانش فروشد غیرتش نادان کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دام نان آمد تو را این دانش تقلید و ظن</p></div>
<div class="m2"><p>صورت عین الیقین را علم القرآن کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس ز نومیدی بود کان کور بر درها رود</p></div>
<div class="m2"><p>داروی دیده نجوید جمله ذکر نان کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این سخن آبیست از دریای بی‌پایان عشق</p></div>
<div class="m2"><p>تا جهان را آب بخشد جسم‌ها را جان کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر که چون ماهی نباشد جوید او پایان آب</p></div>
<div class="m2"><p>هر که او ماهی بود کی فکرت پایان کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر به فقر و صدق پیش آیی به راه عاشقان</p></div>
<div class="m2"><p>شمس تبریزی تو را همصحبت مردان کند</p></div></div>