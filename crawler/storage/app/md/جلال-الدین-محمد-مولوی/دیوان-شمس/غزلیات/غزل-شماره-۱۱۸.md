---
title: >-
    غزل شمارهٔ ۱۱۸
---
# غزل شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>بنمود وفا از این جا</p></div>
<div class="m2"><p>هرگز نرویم ما از این جا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جا مدد حیات جانست</p></div>
<div class="m2"><p>ذوقست دو چشم را از این جا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این جاست که پا به گل فرورفت</p></div>
<div class="m2"><p>چون برگیریم پا از این جا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این جا به خدا که دل نهادیم</p></div>
<div class="m2"><p>کس را مبر ای خدا از این جا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جاست که مرگ ره ندارد</p></div>
<div class="m2"><p>مرگست بدن جدا از این جا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین جای برآمدی چو خورشید</p></div>
<div class="m2"><p>روشن کردی مرا از این جا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان خرم و شاد و تازه گردد</p></div>
<div class="m2"><p>زین جا یابد بقا از این جا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک بار دگر حجاب بردار</p></div>
<div class="m2"><p>یک بار دگر برآ از این جا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این جاست شراب لایزالی</p></div>
<div class="m2"><p>درریز تو ساقیا از این جا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این چشمه آب زندگانیست</p></div>
<div class="m2"><p>مشکی پر کن سقا از این جا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این جا پر و بال یافت دل‌ها</p></div>
<div class="m2"><p>بگرفت خرد هوا از این جا</p></div></div>