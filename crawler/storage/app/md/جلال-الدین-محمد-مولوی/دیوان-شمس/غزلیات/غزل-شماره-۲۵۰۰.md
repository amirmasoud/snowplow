---
title: >-
    غزل شمارهٔ ۲۵۰۰
---
# غزل شمارهٔ ۲۵۰۰

<div class="b" id="bn1"><div class="m1"><p>چه افسردی در آن گوشه چرا تو هم نمی‌گردی</p></div>
<div class="m2"><p>مگر تو فکر منحوسی که جز بر غم نمی‌گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آمد موسی عمران چرا از آل فرعونی</p></div>
<div class="m2"><p>چو آمد عیسی خوش دم چرا همدم نمی‌گردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو با حق عهدها بستی ز سستی عهد بشکستی</p></div>
<div class="m2"><p>چو قول عهد جانبازان چرا محکم نمی‌گردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان خاک چون موشان به هر مطبخ رهی سازی</p></div>
<div class="m2"><p>چرا مانند سلطانان بر این طارم نمی‌گردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا چون حلقه بر درها برای بانگ و آوازی</p></div>
<div class="m2"><p>چرا در حلقه مردان دمی محرم نمی‌گردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه بسته بگشاید چو دشمن دار مفتاحی</p></div>
<div class="m2"><p>چگونه خسته به گردد چو بر مرهم نمی‌گردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر آنگه سر بود ای جان که خاک راه او باشد</p></div>
<div class="m2"><p>ز عشق رایتش ای سر چرا پرچم نمی‌گردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا چون ابر بی‌باران به پیش مه ترنجیدی</p></div>
<div class="m2"><p>چرا همچون مه تابان بر این عالم نمی‌گردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلم آن جا نهد دستش که کم بیند در او حرفی</p></div>
<div class="m2"><p>چرا از عشق تصحیحش تو حرفی کم نمی‌گردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گلستان و گل و ریحان نروید جز ز دست تو</p></div>
<div class="m2"><p>دو چشمه داری ای چهره چرا پرنم نمی‌گردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو طوافان گردونی همی‌گردند بر آدم</p></div>
<div class="m2"><p>مگر ابلیس ملعونی که بر آدم نمی‌گردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر خلوت نمی‌گیری چرا خامش نمی‌باشی</p></div>
<div class="m2"><p>اگر کعبه نه ای باری چرا زمزم نمی‌گردی</p></div></div>