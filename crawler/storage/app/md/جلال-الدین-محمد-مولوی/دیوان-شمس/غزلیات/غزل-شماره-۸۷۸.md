---
title: >-
    غزل شمارهٔ ۸۷۸
---
# غزل شمارهٔ ۸۷۸

<div class="b" id="bn1"><div class="m1"><p>صحرا خوشست لیک چو خورشید فر دهد</p></div>
<div class="m2"><p>بستان خوشست لیک چو گلزار بر دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید دیگریست که فرمان و حکم او</p></div>
<div class="m2"><p>خورشید را برای مصالح سفر دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوسه به او رسد که رخش همچو زر بود</p></div>
<div class="m2"><p>او را نمی‌رسد که رود مال و زر دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگر به طوطیان که پر و بال می‌زنند</p></div>
<div class="m2"><p>سوی شکرلبی که به ایشان شکر دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کس شکرلبی بگزیده‌ست در جهان</p></div>
<div class="m2"><p>ما را شکرلبیست که چیزی دگر دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را شکرلبیست شکرها گدای اوست</p></div>
<div class="m2"><p>ما را شهنشهیست که ملک و ظفر دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت بلند دار اگر شاه زاده‌ای</p></div>
<div class="m2"><p>قانع مشو ز شاه که تاج و کمر دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برکن تو جامه‌ها و در آب حیات رو</p></div>
<div class="m2"><p>تا پاره‌های خاک تو لعل و گهر دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگریز سوی عشق و بپرهیز از آن بتی</p></div>
<div class="m2"><p>کو دلبری نماید و خون جگر دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در چشم من نیاید خوبی هیچ خوب</p></div>
<div class="m2"><p>نقاش جسم جان را غیبی صور دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی آب شور نوشد با مرغ‌های کور</p></div>
<div class="m2"><p>آن مرغ را که عقل ز کوثر خبر دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خود پر کند دو دیده ما را به حسن خویش</p></div>
<div class="m2"><p>گر ماه آن ببیند در حال سر دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دیده گدای تو آید نگار خاک</p></div>
<div class="m2"><p>حاشا ز دیده‌ای که خدایش نظر دهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خامش ز حرف گفتن تا بوک عقل کل</p></div>
<div class="m2"><p>ما را ز عقل جزوی راه و عبر دهد</p></div></div>