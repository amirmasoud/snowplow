---
title: >-
    غزل شمارهٔ ۲۰۱۹
---
# غزل شمارهٔ ۲۰۱۹

<div class="b" id="bn1"><div class="m1"><p>ای ببرده دل تو قصد جان مکن</p></div>
<div class="m2"><p>و آنچ من کردم تو جانا آن مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر اندر درد من گر صاف نیست</p></div>
<div class="m2"><p>درد خود مفرستم و درمان مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد ایمان داد زلف کافرت</p></div>
<div class="m2"><p>یک سر مویی ز کفر ایمان مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عادت خوبان جفا باشد جفا</p></div>
<div class="m2"><p>هم بر آن عادت بر او احسان مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه دل بر مرگ خود بنهاده‌ایم</p></div>
<div class="m2"><p>در جفا آهسته‌تر چندان مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیش ما را مرگ باشد پرده دار</p></div>
<div class="m2"><p>پرده پوش و مرگ را خندان مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای زلیخا فتنه عشق از تو است</p></div>
<div class="m2"><p>یوسفی را هرزه در زندان مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سر رندان نداری وقت عیش</p></div>
<div class="m2"><p>وعده‌ها اندر سر رندان مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نور چشم عاشقان آخر تویی</p></div>
<div class="m2"><p>عیش‌ها بر کوری ایشان مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقدکی را از یکی مفلس مبر</p></div>
<div class="m2"><p>از حریصی نقد او در کان مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب روان را همچو استاره مسوز</p></div>
<div class="m2"><p>راه خود را پر ز رهبانان مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شمس تبریزی یکی رویی نمای</p></div>
<div class="m2"><p>تا ابد تو روی با جانان مکن</p></div></div>