---
title: >-
    غزل شمارهٔ ۱۵۹
---
# غزل شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>ای ز مقدارت هزاران فخر بی‌مقدار را</p></div>
<div class="m2"><p>داد گلزار جمالت جان شیرین خار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ملوکان جهان روح بر درگاه تو</p></div>
<div class="m2"><p>در سجودافتادگان و منتظر مر بار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل از عقلی رود هم روح روحی گم کند</p></div>
<div class="m2"><p>چونک طنبوری ز عشقت برنوازد تار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز آب لطف تو نم یافتی گلزارها</p></div>
<div class="m2"><p>کس ندیدی خالی از گل سال‌ها گلزار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محو می‌گردد دلم در پرتو دلدار من</p></div>
<div class="m2"><p>می‌نتانم فرق کردن از دلم دلدار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایما فخرست جان را از هوای او چنان</p></div>
<div class="m2"><p>کو ز مستی می‌نداند فخر را و عار را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست غاری جان رهبانان عشقت معتکف</p></div>
<div class="m2"><p>کرده رهبان مبارک پر ز نور این غار را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر شود عالم چو قیر از غصه هجران تو</p></div>
<div class="m2"><p>نخوتی دارد که اندرننگرد مر قار را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون عصای موسی بود آن وصل اکنون مار شد</p></div>
<div class="m2"><p>ای وصال موسی وش اندرربا این مار را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خداوند شمس دین از آتش هجران تو</p></div>
<div class="m2"><p>رشک نور باقی‌ست صد آفرین این نار را</p></div></div>