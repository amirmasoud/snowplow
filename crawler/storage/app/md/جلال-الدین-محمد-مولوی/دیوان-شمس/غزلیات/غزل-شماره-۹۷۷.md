---
title: >-
    غزل شمارهٔ ۹۷۷
---
# غزل شمارهٔ ۹۷۷

<div class="b" id="bn1"><div class="m1"><p>عید بر عاشقان مبارک باد</p></div>
<div class="m2"><p>عاشقان عیدتان مبارک باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید ار بوی جان ما دارد</p></div>
<div class="m2"><p>در جهان همچو جان مبارک باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر تو ای ماه آسمان و زمین</p></div>
<div class="m2"><p>تا به هفت آسمان مبارک باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عید آمد به کف نشان وصال</p></div>
<div class="m2"><p>عاشقان این نشان مبارک باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزه مگشای جز به قند لبش</p></div>
<div class="m2"><p>قند او در دهان مبارک باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عید بنوشت بر کنار لبش</p></div>
<div class="m2"><p>کاین می بی‌کران مبارک باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عید آمد که ای سبک روحان</p></div>
<div class="m2"><p>رطل‌های گران مبارک باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند پنهان خوری صلاح الدین</p></div>
<div class="m2"><p>بوسه‌های نهان مبارک باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نصیبی به من دهی گویم</p></div>
<div class="m2"><p>بر من و بر فلان مبارک باد</p></div></div>