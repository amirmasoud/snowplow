---
title: >-
    غزل شمارهٔ ۱۴۶۲
---
# غزل شمارهٔ ۱۴۶۲

<div class="b" id="bn1"><div class="m1"><p>صورتگر نقاشم هر لحظه بتی سازم</p></div>
<div class="m2"><p>وانگه همه بت‌ها را در پیش تو بگدازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد نقش برانگیزم با روح درآمیزم</p></div>
<div class="m2"><p>چون نقش تو را بینم در آتشش اندازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ساقی خماری یا دشمن هشیاری</p></div>
<div class="m2"><p>یا آنک کنی ویران هر خانه که می سازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان ریخته شد بر تو آمیخته شد با تو</p></div>
<div class="m2"><p>چون بوی تو دارد جان جان را هله بنوازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر خون که ز من روید با خاک تو می گوید</p></div>
<div class="m2"><p>با مهر تو همرنگم با عشق تو هنبازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خانه آب و گل بی‌توست خراب این دل</p></div>
<div class="m2"><p>یا خانه درآ جانا یا خانه بپردازم</p></div></div>