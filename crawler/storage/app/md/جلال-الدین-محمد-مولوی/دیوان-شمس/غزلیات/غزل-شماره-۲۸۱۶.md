---
title: >-
    غزل شمارهٔ ۲۸۱۶
---
# غزل شمارهٔ ۲۸۱۶

<div class="b" id="bn1"><div class="m1"><p>که شکیبد ز تو ای جان که جگرگوشه جانی</p></div>
<div class="m2"><p>چه تفکر کند از مکر و ز دستان که ندانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه درونی نه برونی که از این هر دو فزونی</p></div>
<div class="m2"><p>نه ز شیری نه ز خونی نه از اینی نه از آنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برود فکرت جادو نهدت دام به هر سو</p></div>
<div class="m2"><p>تو همه دام و فنش را به یکی فن بدرانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بود باطن کبکی که دل باز نداند</p></div>
<div class="m2"><p>چه حبوب است زمین در که ز چرخ است نهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلهش بنهی وآنگه فکنی باز به سیلی</p></div>
<div class="m2"><p>چه کند بره مسکین چو کند شیر شبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کله و تاج سرم را پی سیلی تو باید</p></div>
<div class="m2"><p>که مرا تاج تویی و جز تو جمله گرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کجا اسب دواند به کجا رخت کشاند</p></div>
<div class="m2"><p>ز تو چون جان بجهاند که تو صد جان جهانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چه نقصان نگرندت به چه عیبی شکنندت</p></div>
<div class="m2"><p>به کی مانند کنندت که به مخلوق نمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ملاقات نشان ده ز خیالات امان ده</p></div>
<div class="m2"><p>مکشش زود زمان ده که تو قسام زمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هله ای جان گشاده قدم صدق نهاده</p></div>
<div class="m2"><p>همه از پای فتاده تو خوش و دست زنانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شه و شاهین جلالی که چنین باپر و بالی</p></div>
<div class="m2"><p>نه گمانی نه خیالی همه عینی و عیانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه بود طبع و رموزش به یکی شعله بسوزش</p></div>
<div class="m2"><p>به یکی تیر بدوزش که بسی سخته کمانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هله بر قوس بنه زه ز کمینگاه برون جه</p></div>
<div class="m2"><p>برهان خویش از این ده که تو زان شهر کلانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو همه خانه دل را بگرفت آتش بالا</p></div>
<div class="m2"><p>بود اظهار زبانه به از اظهار زبانی</p></div></div>