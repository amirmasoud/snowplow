---
title: >-
    غزل شمارهٔ ۱۹۳۱
---
# غزل شمارهٔ ۱۹۳۱

<div class="b" id="bn1"><div class="m1"><p>عقل از کف عشق خورد افیون</p></div>
<div class="m2"><p>هش دار جنون عقل اکنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق مجنون و عقل عاقل</p></div>
<div class="m2"><p>امروز شدند هر دو مجنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جیحون که به عشق بحر می رفت</p></div>
<div class="m2"><p>دریا شد و محو گشت جیحون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق رسید بحر خون دید</p></div>
<div class="m2"><p>بنشست خرد میانه خون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فرق گرفت موج خونش</p></div>
<div class="m2"><p>می برد ز هر سوی به بی‌سون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا گم کردش تمام از خود</p></div>
<div class="m2"><p>تا گشت به عشق چست و موزون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گم شدگی رسید جایی</p></div>
<div class="m2"><p>کان جا نه زمین بود نه گردون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر پیش رود قدم ندارد</p></div>
<div class="m2"><p>ور بنشیند پس او است مغبون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناگاه بدید زان سوی محو</p></div>
<div class="m2"><p>زان سوی جهان نور بی‌چون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک سنجق و صد هزار نیزه</p></div>
<div class="m2"><p>از نور لطیف گشت مفتون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن پای گرفته‌اش روان شد</p></div>
<div class="m2"><p>می رفت در آن عجیب‌هامون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بو که رسد قدم بدان جا</p></div>
<div class="m2"><p>تا رسته شود ز خویش و مادون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش آمد در رهش دو وادی</p></div>
<div class="m2"><p>یک آتش بد یکیش گلگون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آواز آمد که رو در آتش</p></div>
<div class="m2"><p>تا یافت شوی به گلستان هون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور زانک به گلستان درآیی</p></div>
<div class="m2"><p>خود را بینی در آتش و تون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر پشت فلک پری چو عیسی</p></div>
<div class="m2"><p>و اندر بالا فرو چو قارون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگریز و امان شاه جان جو</p></div>
<div class="m2"><p>از جمله عقیله‌ها تو بیرون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن شمس الدین و فخر تبریز</p></div>
<div class="m2"><p>کز هر چه صفت کنیش افزون</p></div></div>