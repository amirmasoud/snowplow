---
title: >-
    غزل شمارهٔ ۴۲۶
---
# غزل شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>غیر عشقت راه بین جستیم نیست</p></div>
<div class="m2"><p>جز نشانت همنشین جستیم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چنان جستن که می‌خواهی بگو</p></div>
<div class="m2"><p>کان چنان را این چنین جستیم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد از این بر آسمان جوییم یار</p></div>
<div class="m2"><p>زانک یاری در زمین جستیم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خیال ماه تو ای بی‌خیال</p></div>
<div class="m2"><p>تا به چرخ هفتمین جستیم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهتر آن باشد که محو این شویم</p></div>
<div class="m2"><p>کز دو عالم به از این جستیم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صاف‌های جمله عالم خورده گیر</p></div>
<div class="m2"><p>همچو درد درد دین جستیم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاتم ملک سلیمان جستنیست</p></div>
<div class="m2"><p>حلقه‌ها هست و نگین جستیم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورتی کاندر نگین او بدست</p></div>
<div class="m2"><p>در بتان روم و چین جستیم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن چنان صورت که شرحش می‌کنم</p></div>
<div class="m2"><p>جز که صورت آفرین جستیم نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اندر آن صورت یقین حاصل شود</p></div>
<div class="m2"><p>کز ورای آن یقین جستیم نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جای آن هست ار گمان بد بریم</p></div>
<div class="m2"><p>ز آنک بی‌مکری امین جستیم نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پشت ما از ظن بد شد چون کمان</p></div>
<div class="m2"><p>زانک راهی بی‌کمین جستیم نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین بیان نوری که پیدا می‌شود</p></div>
<div class="m2"><p>در بیان و در مبین جستیم نیست</p></div></div>