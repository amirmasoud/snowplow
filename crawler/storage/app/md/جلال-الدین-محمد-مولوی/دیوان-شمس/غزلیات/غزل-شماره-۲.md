---
title: >-
    غزل شمارهٔ ۲
---
# غزل شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای طایران قدس را عشقت فزوده بال‌ها</p></div>
<div class="m2"><p>در حلقه سودای تو روحانیان را حال‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در لا احب الآفلین پاکی ز صورت‌ها یقین</p></div>
<div class="m2"><p>در دیده‌های غیب بین هر دم ز تو تمثال‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افلاک از تو سرنگون خاک از تو چون دریای خون</p></div>
<div class="m2"><p>ماهت نخوانم ای فزون از ماه‌ها و سال‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه از غمت بشکافته وان غم به دل درتافته</p></div>
<div class="m2"><p>یک قطره خونی یافته از فضلت این افضال‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای سروران را تو سند بشمار ما را زان عدد</p></div>
<div class="m2"><p>دانی سران را هم بود اندر تبع دنبال‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سازی ز خاکی سیدی بر وی فرشته حاسدی</p></div>
<div class="m2"><p>با نقد تو جان کاسدی پامال گشته مال‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کو تو باشی بال او ای رفعت و اجلال او</p></div>
<div class="m2"><p>آن کو چنین شد حال او بر روی دارد خال‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیرم که خارم خار بد خار از پی گل می‌زهد</p></div>
<div class="m2"><p>صراف زر هم می‌نهد جو بر سر مثقال‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکری بدست افعال‌ها خاکی بدست این مال‌ها</p></div>
<div class="m2"><p>قالی بدست این حال‌ها حالی بدست این قال‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آغاز عالم غلغله پایان عالم زلزله</p></div>
<div class="m2"><p>عشقی و شکری با گله آرام با زلزال‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توقیع شمس آمد شفق طغرای دولت عشق حق</p></div>
<div class="m2"><p>فال وصال آرد سبق کان عشق زد این فال‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از رحمة للعالمین اقبال درویشان ببین</p></div>
<div class="m2"><p>چون مه منور خرقه‌ها چون گل معطر شال‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق امر کل ما رقعه‌ای او قلزم و ما جرعه‌ای</p></div>
<div class="m2"><p>او صد دلیل آورده و ما کرده استدلال‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از عشق گردون مؤتلف بی‌عشق اختر منخسف</p></div>
<div class="m2"><p>از عشق گشته دال الف بی‌عشق الف چون دال‌ها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آب حیات آمد سخن کاید ز علم من لدن</p></div>
<div class="m2"><p>جان را از او خالی مکن تا بردهد اعمال‌ها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر اهل معنی شد سخن اجمال‌ها تفصیل‌ها</p></div>
<div class="m2"><p>بر اهل صورت شد سخن تفصیل‌ها اجمال‌ها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر شعرها گفتند پر پر به بود دریا ز در</p></div>
<div class="m2"><p>کز ذوق شعر آخر شتر خوش می‌کشد ترحال‌ها</p></div></div>