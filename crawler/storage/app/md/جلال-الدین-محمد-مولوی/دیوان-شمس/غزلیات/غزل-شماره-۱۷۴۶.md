---
title: >-
    غزل شمارهٔ ۱۷۴۶
---
# غزل شمارهٔ ۱۷۴۶

<div class="b" id="bn1"><div class="m1"><p>بر آن شده‌ست دلم کآتشی بگیرانم</p></div>
<div class="m2"><p>که هر کی او نمرد پیش تو بمیرانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان عشق بدرم که تا بداند عقل</p></div>
<div class="m2"><p>که بی‌نظیرم و سلطان بی‌نظیرانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که رفت در نظر تو که بی‌نظیر نشد</p></div>
<div class="m2"><p>مقام گنج شده‌ست این نهاد ویرانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از کجا و مباهات سلطنت ز کجا</p></div>
<div class="m2"><p>فقیر فقرم و افتاده فقیرانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من آن کسم که تو نامم نهی نمی‌دانم</p></div>
<div class="m2"><p>چو من اسیر توام پس امیر میرانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز از اسیری و میری مقام دیگر هست</p></div>
<div class="m2"><p>چو من فنا شوم از هر دو کس نفیرانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شب بیاید میر و اسیر محو شوند</p></div>
<div class="m2"><p>اسیر هیچ نداند که از اسیرانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خواب شب گرو آمد امیری میران</p></div>
<div class="m2"><p>چو عشق هیچ نخسبد ز عشق گیرانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آفتاب نگر پادشاه یک روزه‌ست</p></div>
<div class="m2"><p>همی‌گدازد مه منیر کز وزیرانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم که پخته عشقم نه خام و خام طمع</p></div>
<div class="m2"><p>خدای کرد خمیری از آن خمیرانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمیرکرده یزدان کجا بماند خام</p></div>
<div class="m2"><p>خمیرمایه پذیرم نه از فطیرانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فطیر چون کند او فاطرالسموات است</p></div>
<div class="m2"><p>چو اختران سماوات از منیرانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو چند نام نهی خویش را خمش می باش</p></div>
<div class="m2"><p>که کودکی است که گویی که من ز پیرانم</p></div></div>