---
title: >-
    غزل شمارهٔ ۱۹۷۰
---
# غزل شمارهٔ ۱۹۷۰

<div class="b" id="bn1"><div class="m1"><p>مطربا بردار چنگ و لحن موسیقار زن</p></div>
<div class="m2"><p>آتش از جرمم بیار و اندر استغفار زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کلیم عشق بر فرعون هستی حمله بر</p></div>
<div class="m2"><p>بر سر او تو عصای محو موسی وار زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل از بهر هوس‌ها دارداری می کند</p></div>
<div class="m2"><p>زود چشمش را ببند و بهر او تو دار زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بگوید من به دانش نظم کاری می کنم</p></div>
<div class="m2"><p>آتشی دست آور و در نظم و اندر کار زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غریبستان جان تا کی شوی مهمان خاک</p></div>
<div class="m2"><p>خاک اندر چشم این مهمان و مهمان دار زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطربا حسنت ز پرگار خرد بیرونتر است</p></div>
<div class="m2"><p>خیمه عشرت برون از عقل و از پرگار زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تار چنگت را ز پود صرف می جانی بده</p></div>
<div class="m2"><p>زان حراره کهنه نوبخت بر اوتار زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در مخدوم شمس الدین ز دیده آب زن</p></div>
<div class="m2"><p>در همه هستی ز نار چهره او نار زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از یکی دستان او خورشید و مه را خفته کن</p></div>
<div class="m2"><p>پس نهان زو چنگ اندر دولت بیدار زن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل هشیارت قبایی دوخت بهر شمس دین</p></div>
<div class="m2"><p>تو ز عشق او به چشم منکران مسمار زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر براق عشق بنشین جانب تبریز رو</p></div>
<div class="m2"><p>و آنگهی زانو ز بهر غمزه خون خوار زن</p></div></div>