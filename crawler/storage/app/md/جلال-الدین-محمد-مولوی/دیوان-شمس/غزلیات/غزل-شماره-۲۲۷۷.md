---
title: >-
    غزل شمارهٔ ۲۲۷۷
---
# غزل شمارهٔ ۲۲۷۷

<div class="b" id="bn1"><div class="m1"><p>یک چند رندند این طرف در ظل دل پنهان شده</p></div>
<div class="m2"><p>و آن آفتاب از سقف دل بر جانشان تابان شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نجم ناهیدی شده هر ذره خورشیدی شده</p></div>
<div class="m2"><p>خورشید و اختر پیششان چون ذره سرگردان شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن عقل و دل گم کردگان جان سوی کیوان بردگان</p></div>
<div class="m2"><p>بی‌چتر و سنجق هر یکی کیخسرو و سلطان شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیار مرکب کشته‌ای گرد جهان برگشته‌ای</p></div>
<div class="m2"><p>در جان سفر کن درنگر قومی سراسر جان شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با این عطای ایزدی با این جمال و شاهدی</p></div>
<div class="m2"><p>فرمان پرستان را نگر مستغرق فرمان شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آینه آن سینه شان آن سینه بی‌کینه شان</p></div>
<div class="m2"><p>دلشان چو میدان فلک سلطان سوی میدان شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هیهی و هیهایشان وز لعل شکرخایشان</p></div>
<div class="m2"><p>نقل و شراب و آن دگر در شهر ما ارزان شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دوش اگر بی‌خویشمی از فتنه من نندیشمی</p></div>
<div class="m2"><p>باقی این را بودمی بی‌خویشتن گویان شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دم فروبندم دهن زیرا به خویشم مرتهن</p></div>
<div class="m2"><p>تا آن زمانی که دلم باشد از او سکران شده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلطان سلطانان جان شمس الحق تبریزیان</p></div>
<div class="m2"><p>هر جان از او دریا شده هر جسم از او مرجان شده</p></div></div>