---
title: >-
    غزل شمارهٔ ۱۳۵۱
---
# غزل شمارهٔ ۱۳۵۱

<div class="b" id="bn1"><div class="m1"><p>شد پی این لولیان در حرم ذوالجلال</p></div>
<div class="m2"><p>چشمه و سبزه مقام شوخی و دزدی حلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهزنی آن کس کند کو نشناسد رهی</p></div>
<div class="m2"><p>خانه دغل او بود کو نشناسد جمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل جهان عنکبوت صید همه خرمگس</p></div>
<div class="m2"><p>هیچ از ایشان مگو تام نگیرد ملال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دزد نهان خانه را شاهد و غماز کیست</p></div>
<div class="m2"><p>چهره چون زعفران اشک چو آب زلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک چرا می‌دود تا بکشد آتشی</p></div>
<div class="m2"><p>زرد چرا می‌شود تا بکند وصف حال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک و رخ عاشقان می‌کشدت که بیا</p></div>
<div class="m2"><p>پیشگه عشق رو خیز ز صف نعال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زردی رخ آینه‌ست سرخی معشوق را</p></div>
<div class="m2"><p>اشک رقم می‌کشد بر صحف خط و خال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه خوبی و کش بر رخ خاک حبش</p></div>
<div class="m2"><p>تافته از ماه غیب پرتو نور کمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبر کن این یک دو روز با همه فر و فروز</p></div>
<div class="m2"><p>بازرود سوی اصل بازکند اتصال</p></div></div>