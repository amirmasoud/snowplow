---
title: >-
    غزل شمارهٔ ۲۰۰۱
---
# غزل شمارهٔ ۲۰۰۱

<div class="b" id="bn1"><div class="m1"><p>دم ده و عشوه ده ای دلبر سیمین بر من</p></div>
<div class="m2"><p>که دمم بی‌دم تو چون اجل آمد بر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل چو دریا شودم چون گهرت درتابد</p></div>
<div class="m2"><p>سر به گردون رسدم چونک بخاری سر من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنک آن دم که بیاری سوی من باده لعل</p></div>
<div class="m2"><p>بدرخشد ز شرارش رخ همچون زر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان خرابم که ز اوقاف خرابات توام</p></div>
<div class="m2"><p>در خرابی است عمارت شدن مخبر من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهد جان چو شهادت ز درون عرضه کند</p></div>
<div class="m2"><p>زود انگشت برآرد خرد کافر من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش از آنک به حریفان دهی ای ساقی جمع</p></div>
<div class="m2"><p>از همه تشنه ترم من بده آن ساغر من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده امر توام خاصه در آن امر که تو</p></div>
<div class="m2"><p>گوییم خیز نظر کن به سوی منظر من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هین برافروز دلم را تو به نار موسی</p></div>
<div class="m2"><p>تا که افروخته ماند ابدا اخگر من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من خمش کردم و در جوی تو افکندم خویش</p></div>
<div class="m2"><p>که ز جوی تو بود رونق شعر تر من</p></div></div>