---
title: >-
    غزل شمارهٔ ۱۱۹۴
---
# غزل شمارهٔ ۱۱۹۴

<div class="b" id="bn1"><div class="m1"><p>گر نه‌ای دیوانه رو مر خویش را دیوانه ساز</p></div>
<div class="m2"><p>گر چه صد ره مات گشتی مهره دیگر بباز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه چون تاری ز زخمش زخمه دیگر بزن</p></div>
<div class="m2"><p>بازگرد ای مرغ گر چه خسته‌ای از چنگ باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند خانه گم کنی و یاوه گردی گرد شهر</p></div>
<div class="m2"><p>ور ز شهری نیز یاوه با قلاوزی بساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسب چوبین برتراشیدی که این اسب منست</p></div>
<div class="m2"><p>گر نه چوبینست اسبت خواجه یک منزل بتاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دعوت حق نشنوی آنگه دعاها می‌کنی</p></div>
<div class="m2"><p>شرم بادت ای برادر زین دعای بی‌نماز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر به سر راضی نه‌ای که سر بری از تیغ حق</p></div>
<div class="m2"><p>کی دهد بو همچو عنبر چونک سیری و پیاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نیازت را پذیرد شمس تبریزی ز لطف</p></div>
<div class="m2"><p>بعد از آن بر عرش نه تو چاربالش بهر ناز</p></div></div>