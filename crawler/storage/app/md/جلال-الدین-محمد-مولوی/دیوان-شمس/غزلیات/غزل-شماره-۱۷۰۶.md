---
title: >-
    غزل شمارهٔ ۱۷۰۶
---
# غزل شمارهٔ ۱۷۰۶

<div class="b" id="bn1"><div class="m1"><p>برخیز تا شراب به رطل و سبو خوریم</p></div>
<div class="m2"><p>بزم شهنشه‌ست نه ما باده می خریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحری است شهریار و شرابی است خوشگوار</p></div>
<div class="m2"><p>درده شراب لعل ببین ما چه گوهریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید جام نور چو برریخت بر زمین</p></div>
<div class="m2"><p>ما ذره وار مست بر این اوج برپریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید لایزال چو ما را شراب داد</p></div>
<div class="m2"><p>از کبر در پیاله خورشید ننگریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش آر آن شراب خردسوز دلفروز</p></div>
<div class="m2"><p>تا همچو دل ز آب و گل خویش بگذریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرخواره‌ایم کز کرم شاه واقفیم</p></div>
<div class="m2"><p>در شرب سابقیم و به خدمت مقصریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیرا که سکر مانع خدمت بود یقین</p></div>
<div class="m2"><p>زین سو چو فربهیم بدان سوی لاغریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوری که در زجاجه و مشکات تافته‌ست</p></div>
<div class="m2"><p>بر ما بزن که ما ز شعاعش منوریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس گرم و سرد شد دل از این باده چون تنور</p></div>
<div class="m2"><p>درسوزمان چو هیزم تا هیچ نفسریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شیشه فلک پر از آتش شده‌ست جان</p></div>
<div class="m2"><p>چون کوره بهر ما که مس و قلب یا زریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای گلعذار جام چو لاله به مجلس آر</p></div>
<div class="m2"><p>کز ساغر چو لاله چو گل یاسمین بریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوش خوش بیا و اصل خوشی را به بزم آر</p></div>
<div class="m2"><p>با جمله ما خوشیم ولی با تو خوشتریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای مطرب آن ترانه تر بازگو ببین</p></div>
<div class="m2"><p>تو تری و لطیفی و ما از تو ترتریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندرفکن ز بانگ و خروش خوشت صدا</p></div>
<div class="m2"><p>در ما که در وفای تو چون کوه مرمریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن دم که از مسیح تو میراث برده‌ای</p></div>
<div class="m2"><p>در گوش ما بدم که چو سرنای مضطریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چه دهان پر است ز گفتار لب ببند</p></div>
<div class="m2"><p>خاموش کن که پیش حسودان منکریم</p></div></div>