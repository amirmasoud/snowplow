---
title: >-
    غزل شمارهٔ ۱۷۰
---
# غزل شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>تا به شب ای عارف شیرین نوا</p></div>
<div class="m2"><p>آن مایی آن مایی آن ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به شب امروز ما را عشرتست</p></div>
<div class="m2"><p>الصلا ای پاکبازان الصلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درخرام ای جان جان هر سماع</p></div>
<div class="m2"><p>مه لقایی مه لقایی مه لقا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان شکران گل ریز کن</p></div>
<div class="m2"><p>مرحبا ای کان شکر مرحبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر را نبود وفا الا تو عمر</p></div>
<div class="m2"><p>باوفایی باوفایی باوفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس غریبی بس غریبی بس غریب</p></div>
<div class="m2"><p>از کجایی از کجایی از کجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با که می‌باشی و همراز تو کیست</p></div>
<div class="m2"><p>با خدایی با خدایی با خدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای گزیده نقش از نقاش خود</p></div>
<div class="m2"><p>کی جدایی کی جدایی کی جدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با همه بیگانه‌ای و با غمش</p></div>
<div class="m2"><p>آشنایی آشنایی آشنایی آشنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جزو جزو تو فکنده در فلک</p></div>
<div class="m2"><p>ربنا و ربنا و ربنا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل شکسته هین چرایی برشکن</p></div>
<div class="m2"><p>قلب‌ها و قلب‌ها و قلب‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آخر ای جان اول هر چیز را</p></div>
<div class="m2"><p>منتهایی منتهایی منتها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یوسفا در چاه شاهی تو ولیک</p></div>
<div class="m2"><p>بی لوایی بی‌لوایی بی‌لوا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چاه را چون قصر قیصر کرده‌ای</p></div>
<div class="m2"><p>کیمیایی کیمیایی کیمیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک ولی کی خوانمت که صد هزار</p></div>
<div class="m2"><p>اولیایی اولیایی اولیا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حشرگاه هر حسینی گر کنون</p></div>
<div class="m2"><p>کربلایی کربلایی کربلا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مشک را بربند ای جان گر چه تو</p></div>
<div class="m2"><p>خوش سقایی خوش سقایی خوش سقا</p></div></div>