---
title: >-
    غزل شمارهٔ ۱۲۶۷
---
# غزل شمارهٔ ۱۲۶۷

<div class="b" id="bn1"><div class="m1"><p>در عشق آتشینش آتش نخورده آتش</p></div>
<div class="m2"><p>بی‌چهره خوش او در خوش هزار ناخوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از تو شرحه شرحه بنشین کباب می‌خور</p></div>
<div class="m2"><p>خون چون میست جوشان بنشین شراب می‌چش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشی کشد مرا می گوشی دگر کشد وی</p></div>
<div class="m2"><p>ای دل در این کشاکش بنشین و باده می‌کش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هفت اخترند عامل در شش جهت ولیکن</p></div>
<div class="m2"><p>ای عشق بردریدی این هفت را از آن شش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی چو آفتابم سرمایه بخش صد مه</p></div>
<div class="m2"><p>گه چون مهم گذاران در عشق یار مه وش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر منکری گریزد از عشق نیست نادر</p></div>
<div class="m2"><p>کز آفتاب دارد پرهیز چشم اعمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدغ الوفاء حقاء من فقدکم مشوش</p></div>
<div class="m2"><p>وجه الولاء حقاء من عبرتی منقش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>القلب لیس یلقی نادیک کیف یصبر</p></div>
<div class="m2"><p>الاذن لیس یلقن حادیک کیف ینعش</p></div></div>