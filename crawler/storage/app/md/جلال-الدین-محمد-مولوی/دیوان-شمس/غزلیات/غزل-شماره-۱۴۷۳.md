---
title: >-
    غزل شمارهٔ ۱۴۷۳
---
# غزل شمارهٔ ۱۴۷۳

<div class="b" id="bn1"><div class="m1"><p>بیایید بیایید به گلزار بگردیم</p></div>
<div class="m2"><p>بر این نقطه اقبال چو پرگار بگردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیایید که امروز به اقبال و به پیروز</p></div>
<div class="m2"><p>چو عشاق نوآموز بر آن یار بگردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی تخم بکشتیم بر این شوره بگشتیم</p></div>
<div class="m2"><p>بر آن حب که نگنجید در انبار بگردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن روی که پشت است به آخر همه زشت است</p></div>
<div class="m2"><p>بر آن یار نکوروی وفادار بگردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از خویش برنجیم زبون شش و پنجیم</p></div>
<div class="m2"><p>یکی جانب خمخانه خمار بگردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این غم چو نزاریم در آن دام شکاریم</p></div>
<div class="m2"><p>دگر کار نداریم در این کار بگردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ما بی‌سر و پاییم چو ذرات هواییم</p></div>
<div class="m2"><p>بر آن نادره خورشید قمروار بگردیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دولاب چه گردیم پر از ناله و افغان</p></div>
<div class="m2"><p>چو اندیشه بی‌شکوت و گفتار بگردیم</p></div></div>