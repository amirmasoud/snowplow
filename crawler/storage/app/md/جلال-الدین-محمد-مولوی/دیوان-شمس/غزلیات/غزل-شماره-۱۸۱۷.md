---
title: >-
    غزل شمارهٔ ۱۸۱۷
---
# غزل شمارهٔ ۱۸۱۷

<div class="b" id="bn1"><div class="m1"><p>قصد جفاها نکنی ور بکنی با دل من</p></div>
<div class="m2"><p>وا دل من وا دل من وا دل من وا دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصد کنی بر تن من شاد شود دشمن من</p></div>
<div class="m2"><p>وانگه از این خسته شود یا دل تو یا دل من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واله و شیدا دل من بی‌سر و بی‌پا دل من</p></div>
<div class="m2"><p>وقت سحرها دل من رفته به هر جا دل من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیخود و مجنون دل من خانه پرخون دل من</p></div>
<div class="m2"><p>ساکن و گردان دل من فوق ثریا دل من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوخته و لاغر تو در طلب گوهر تو</p></div>
<div class="m2"><p>آمده و خیمه زده بر لب دریا دل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه چو کباب این دل من پر شده بویش به جهان</p></div>
<div class="m2"><p>گه چو رباب این دل من کرده علالا دل من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زار و معاف است کنون غرق مصاف است کنون</p></div>
<div class="m2"><p>بر که قاف است کنون در پی عنقا دل من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طفل دلم می نخورد شیر از این دایه شب</p></div>
<div class="m2"><p>سینه سیه یافت مگر دایه شب را دل من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صخره موسی گر از او چشمه روان گشت چو جو</p></div>
<div class="m2"><p>جوی روان حکمت حق صخره و خارا دل من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عیسی مریم به فلک رفت و فروماند خرش</p></div>
<div class="m2"><p>من به زمین ماندم و شد جانب بالا دل من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس کن کاین گفت زبان هست حجاب دل و جان</p></div>
<div class="m2"><p>کاش نبودی ز زبان واقف و دانا دل من</p></div></div>