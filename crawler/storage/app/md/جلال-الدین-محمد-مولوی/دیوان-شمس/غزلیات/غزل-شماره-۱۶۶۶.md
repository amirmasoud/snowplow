---
title: >-
    غزل شمارهٔ ۱۶۶۶
---
# غزل شمارهٔ ۱۶۶۶

<div class="b" id="bn1"><div class="m1"><p>گفته‌ای من یار دیگر می کنم</p></div>
<div class="m2"><p>بر تو دل چون سنگ مرمر می کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس تو خود این گو که از تیغ جفا</p></div>
<div class="m2"><p>عاشقی را قصد و بی‌سر می کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهری را زیر مرمر می کشم</p></div>
<div class="m2"><p>مرمری را لعل و گوهر می کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد هزاران مؤمن توحید را</p></div>
<div class="m2"><p>بسته آن زلف کافر می کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان را در کشاکش همچو ماه</p></div>
<div class="m2"><p>گاه فربه گاه لاغر می کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کله‌های عشق را از خنب جان</p></div>
<div class="m2"><p>کیل باده همچو ساغر می کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باغ دل سرسبز و تر باشد ولیک</p></div>
<div class="m2"><p>از فراقش خشک و بی‌بر می کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلبنان را جمله گردن می زنم</p></div>
<div class="m2"><p>قصد شاخ تازه و تر می کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونک بی‌من باغ حال خود بدید</p></div>
<div class="m2"><p>جور هشتم داد و داور می کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بهار وصل بر بیمار دی</p></div>
<div class="m2"><p>مغفرت را روح پرور می کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بار دیگر از بر سیمین خود</p></div>
<div class="m2"><p>دست بی‌سیمان پر از زر می کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بندگان خویش را بر هر دو کون</p></div>
<div class="m2"><p>خسرو و خاقان و سنجر می کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس تبریزی همی‌گوید به روح</p></div>
<div class="m2"><p>من ز عین روح سرور می کنم</p></div></div>