---
title: >-
    غزل شمارهٔ ۸۴۸
---
# غزل شمارهٔ ۸۴۸

<div class="b" id="bn1"><div class="m1"><p>از چشم پرخمارت دل را قرار ماند</p></div>
<div class="m2"><p>وز روی همچو ماهت در مه شمار ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مطرب هوایت چنگ طرب نوازد</p></div>
<div class="m2"><p>مر زهره فلک را کی کسب و کار ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یغمابک جمالت هر سو که لشکر آرد</p></div>
<div class="m2"><p>آن سوی شهر ماند آن سو دیار ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلزار جان فزایت بر باغ جان بخندد</p></div>
<div class="m2"><p>گل‌ها به عقل باشد یا خار خار ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جاسوس شاه عشقت چون در دلی درآید</p></div>
<div class="m2"><p>جز عشق هیچ کس را در سینه یار ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای شاد آن زمانی کز بخت ناگهانی</p></div>
<div class="m2"><p>جانت کنار گیرد تن برکنار ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زان چنان نگاری در سر فتد خماری</p></div>
<div class="m2"><p>دل تخت و بخت جوید یا ننگ و عار ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌خواهم از خدا من تا شمس حق تبریز</p></div>
<div class="m2"><p>در غار دل بتابد با یار غار ماند</p></div></div>