---
title: >-
    غزل شمارهٔ ۲۰۰۲
---
# غزل شمارهٔ ۲۰۰۲

<div class="b" id="bn1"><div class="m1"><p>تو سبب سازی و دانایی آن سلطان بین</p></div>
<div class="m2"><p>آنچ ممکن نبود در کف او امکان بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهن اندر کف او نرمتر از مومی بین</p></div>
<div class="m2"><p>پیش نور رخ او اختر را پنهان بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نم اندیشه بیا قلزم اندیشه نگر</p></div>
<div class="m2"><p>صورت چرخ بدیدی هله اکنون جان بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان بنفروختی ای خر به چنین مشتریی</p></div>
<div class="m2"><p>رو به بازار غمش جان چو علف ارزان بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کی بفسرد بر او سخت نماید حرکت</p></div>
<div class="m2"><p>اندکی گرم شو و جنبش را آسان بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خشک کردی تو دماغ از طلب بحث و دلیل</p></div>
<div class="m2"><p>بفشان خویش ز فکر و لمع برهان بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست میزان معینت و بدان می‌سنجی</p></div>
<div class="m2"><p>هله میزان بگذار و زر بی‌میزان بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفسی موضع تنگ و نفسی جای فراخ</p></div>
<div class="m2"><p>می جان نوش و از آن پس همه را میدان بین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحر کرده‌ست تو را دیو همی‌خوان قل اعوذ</p></div>
<div class="m2"><p>چونک سرسبز شدی جمله گل و ریحان بین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تو سرسبز شدی سبز شود جمله جهان</p></div>
<div class="m2"><p>اتحادی عجبی در عرض و ابدان بین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون دمی چرخ زنی و سر تو برگردد</p></div>
<div class="m2"><p>چرخ را بنگر و همچون سر خود گردان بین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آنک تو جزو جهانی مثل کل باشی</p></div>
<div class="m2"><p>چونک نو شد صفتت آن صفت از ارکان بین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه ارکان چو لباس آمد و صنعش چو بدن</p></div>
<div class="m2"><p>چند مغرور لباسی بدن انسان بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی ایمان تو در آیینه اعمال ببین</p></div>
<div class="m2"><p>پرده بردار و درآ شعشعه ایمان بین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر تو عاشق شده‌ای حسن بجو احسان نی</p></div>
<div class="m2"><p>ور تو عباس زمانی بنشین احسان بین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لابه کردم شه خود را پس از این او گوید</p></div>
<div class="m2"><p>چونک دریاش بجوشد در بی‌پایان بین</p></div></div>