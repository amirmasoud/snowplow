---
title: >-
    غزل شمارهٔ ۵۷۳
---
# غزل شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>برآمد بر شجر طوطی که تا خطبه شکر گوید</p></div>
<div class="m2"><p>به بلبل کرد اشارت گل که تا اشعار برگوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سرو سبز وحی آمد که تا جانش بود در تن</p></div>
<div class="m2"><p>میان بندد به خدمت روز و شب‌ها این سمر گوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه تسبیح گویانند اگر ماهست اگر ماهی</p></div>
<div class="m2"><p>ولیکن عقل استادست او مشروحتر گوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآید سنگ در گریه درآید چرخ در کدیه</p></div>
<div class="m2"><p>ز عرش آید دو صد هدیه چو او درس نظر گوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزاران سیمبر بینی گشاییده بر او سینه</p></div>
<div class="m2"><p>چو آن عنبرفشان قصه نسیم آن سحر گوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که را ماند دل آن لحظه که آن جان شرح دل گوید</p></div>
<div class="m2"><p>که را ماند خبر از خود در آن دم کو خبر گوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث عشق جان گوید حدیث ره روان گوید</p></div>
<div class="m2"><p>حدیث سکر سر گوید حدیث خون جگر گوید</p></div></div>