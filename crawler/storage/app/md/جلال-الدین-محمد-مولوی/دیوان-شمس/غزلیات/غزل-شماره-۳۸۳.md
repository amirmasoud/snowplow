---
title: >-
    غزل شمارهٔ ۳۸۳
---
# غزل شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>هین که گردن سست کردی کو کبابت کو شرابت</p></div>
<div class="m2"><p>هین که بس تاریک رویی ای گرفته آفتابت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد داری که ز مستی با خرد استیزه بستی</p></div>
<div class="m2"><p>چون کلیدش را شکستی از کی باشد فتح بابت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غم شیرین نجوشی لاجرم سرکه فروشی</p></div>
<div class="m2"><p>آب حیوان را ببستی لاجرم رفتست آبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوالمعالی گشته بودی فضل و حجت می‌نمودی</p></div>
<div class="m2"><p>نک محک عشق آمد کو سؤالت کو جوابت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهتر تجار بودی خویش قارون می‌نمودی</p></div>
<div class="m2"><p>خواب بود و آن فنا شد چونک از سر رفت خوابت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس زدی تو لاف زفتی عاقبت در دوغ رفتی</p></div>
<div class="m2"><p>می‌خور اکنون آنچ داری دوغ آمد خمر نابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخلص و معنی این‌ها گر چه دانی هم نهان کن</p></div>
<div class="m2"><p>اندر الواح ضمیری تا نیاید در کتابت</p></div></div>