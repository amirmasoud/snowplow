---
title: >-
    غزل شمارهٔ ۲۰۴۳
---
# غزل شمارهٔ ۲۰۴۳

<div class="b" id="bn1"><div class="m1"><p>دیدی چه گفت بهمن هیزم بنه چو خرمن</p></div>
<div class="m2"><p>گر دی نکرد سرما سرمای هر دو بر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرما چو گشت سرکش هیزم بنه در آتش</p></div>
<div class="m2"><p>هیزم دریغت آید هیزم به است یا تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش فناست هیزم عشق خداست آتش</p></div>
<div class="m2"><p>درسوز نقش‌ها را ای جان پاکدامن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نقش را نسوزی جانت فسرده باشد</p></div>
<div class="m2"><p>مانند بت پرستان دور از بهار و مؤمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق همچو آتش چون نقره باش دلخوش</p></div>
<div class="m2"><p>چون زاده خلیلی آتش تو راست مسکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش به امر یزدان گردد به پیش مردان</p></div>
<div class="m2"><p>لاله و گل و شکوفه ریحان و بید و سوسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مؤمن فسون بداند بر آتشش بخواند</p></div>
<div class="m2"><p>سوزش در او نماند ماند چو ماه روشن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاباش ای فسونی کافتد از او سکونی</p></div>
<div class="m2"><p>در آتشی که آهن گردد از او چو سوزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پروانه زان زند خود بر آتش موقد</p></div>
<div class="m2"><p>کو را همی‌نماید آتش به شکل روزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیر و سنان به حمزه چون گلفشان نماید</p></div>
<div class="m2"><p>در گلفشان نپوشد کس خویش را به جوشن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرعون همچو دوغی در آب غرقه گشته</p></div>
<div class="m2"><p>بر فرق آب موسی بنشسته همچو روغن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اسپان اختیاری حمال شهریاری</p></div>
<div class="m2"><p>پالان کشند و سرگین اسبان کند و کودن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو لک لک است منطق بر آسیای معنی</p></div>
<div class="m2"><p>طاحون ز آب گردد نه از لکلک مقنن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان لکلک ای برادر گندم ز دلو بجهد</p></div>
<div class="m2"><p>در آسیا درافتد گردد خوش و مطحن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز لکلک بیان تو از دلو حرص و غفلت</p></div>
<div class="m2"><p>در آسیا درافتی یعنی رهی مبین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من گرم می‌شوم جان اما ز گفت و گو نی</p></div>
<div class="m2"><p>از شمس دین زرین تبریز همچو معدن</p></div></div>