---
title: >-
    دهم
---
# دهم

<div class="b" id="bn1"><div class="m1"><p>هست کسی کو چو من اشکار نیست</p></div>
<div class="m2"><p>هست کسی کو تلف یار نیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست سری کو چو سرم مست نیست؟</p></div>
<div class="m2"><p>هست دلی کو چو دلم زار نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مختلف آمد همه کار جهان</p></div>
<div class="m2"><p>لیک همه جز که یکی کار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غرقهٔ دل دان و طلب کار دل</p></div>
<div class="m2"><p>آنک گله کرد که دلدار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد جهان جستم اغیار من</p></div>
<div class="m2"><p>گشت یقینم که کس اغیار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشتریان جمله یکی مشتریست</p></div>
<div class="m2"><p>جز که یکی رستهٔ بازار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهیت گلشن آنکس که دید</p></div>
<div class="m2"><p>کشف شد او را که یکی خار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خنب ز یخ بود و درو کردم آب</p></div>
<div class="m2"><p>شد همه آب و زخم آثار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله جهان لایتجزی بدست</p></div>
<div class="m2"><p>چنگ جهان را جز یک تار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وسوسهٔ این عدد و این خلاف</p></div>
<div class="m2"><p>جز که فریبنده و غرار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست درین گفت تناقض ولیک</p></div>
<div class="m2"><p>از طرف دیده و دیدار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نقطه دل بی‌عدد و گردش است</p></div>
<div class="m2"><p>گفت زبان جز تک پرگار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طاقت و بی‌طاقتی آمد یکی</p></div>
<div class="m2"><p>پیش مرا طاقت گفتار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مست شدی سر بنه اینجا، مرو</p></div>
<div class="m2"><p>زانکه گلست و ره هموار نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مست دگر از تو بدزدد کمر</p></div>
<div class="m2"><p>جز تو مپندار که طرار نیست</p></div></div>
<div class="b2" id="bn16"><p>چونک ز مطلوب رسیدست برات</p>
<p>گشت نهان از نظر تو صفات</p></div>
<div class="b" id="bn17"><div class="m1"><p>بار دگر یوسف خوبان رسید</p></div>
<div class="m2"><p>سلسلهٔ صد چو زلیخا کشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جامه درد ماه ازین دستگاه</p></div>
<div class="m2"><p>نعره زند چرخ که هل من مزید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جملهٔ دنیا نمکستان شدست</p></div>
<div class="m2"><p>تا که یکی گردد پاک و پلید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بار دگر عقل قلمها شکست</p></div>
<div class="m2"><p>بار دگر عشق گریبان درید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرد زلیخا که نکردت کس</p></div>
<div class="m2"><p>بنده خداوندهٔ خود را خرید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مست شدی بوسه همی بایدت</p></div>
<div class="m2"><p>بوسه بران لب ده، کان می‌چشید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخت خوشی، چشم بدت دور باد</p></div>
<div class="m2"><p>ای خنک آن چشم که روی تو دید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیدن روی تو بسی نادرست</p></div>
<div class="m2"><p>ای خنک آن، گوش که نامت شنید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شعشعهٔ جام تو عالم گرفت</p></div>
<div class="m2"><p>ولولهٔ صبح قیامت دمید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عقل نیابند به دارو، دگر</p></div>
<div class="m2"><p>عقل ازین حیرت شد ناپدید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باز نیاید، بدود تا هدف</p></div>
<div class="m2"><p>تیر چو از قوس مجاهد جهید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هدهد جان چون بجهد از قفس</p></div>
<div class="m2"><p>می‌پرد از عشق به عرش مجید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تیغ و کفن می‌برد و می‌رود</p></div>
<div class="m2"><p>روح سوی قیصر و قصرمشید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رسته ز اندیشه که دل می‌فشرد</p></div>
<div class="m2"><p>جسته ز هر خار که پا می‌خلید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چرخ ازو چرخ زد و گفت ماه</p></div>
<div class="m2"><p>منک لنا کل غد الف عید</p></div></div>
<div class="b2" id="bn32"><p>شد گه ترجیع و دلم می‌جهد</p>
<p>دلبر من داد سخن می‌دهد</p></div>
<div class="b" id="bn33"><div class="m1"><p>این بخورد جام دگر آرمش</p></div>
<div class="m2"><p>بارد و هشیار بنگذارمش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از عدمش من بخریدم به زر</p></div>
<div class="m2"><p>بی می و بی‌مایده کی دارمش؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شیره و شیرین بدهم رایگان</p></div>
<div class="m2"><p>لیک چو انگور نیفشارمش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همچو سر خویش همی پوشمش</p></div>
<div class="m2"><p>همچو سر خویش همی خارمش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>روح منست و فرج روح من</p></div>
<div class="m2"><p>دشمن و بیگانه نینگارمش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون زنم او را؟! که ز مهر و ز عشق</p></div>
<div class="m2"><p>گفتن گستاخ نمی‌یارمش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر برمد کبکبهٔ چار طبع</p></div>
<div class="m2"><p>من عوض و نایب هر چارمش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من به سفر یار و قلاووزمش</p></div>
<div class="m2"><p>من به سحر ساقی و خمارمش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا چه کند لکلکهٔ زر و سیم</p></div>
<div class="m2"><p>که تو بگویی که: « گرفتارمش »</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>او چو ز گفتار ببندد دهن</p></div>
<div class="m2"><p>از جهت ترجمه گفتارمش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ور دل او گرم شود از ملال</p></div>
<div class="m2"><p>مروحه و باد سبکسارمش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ور بسوی غیب نظر خواهد او</p></div>
<div class="m2"><p>آینهٔ دیدهٔ دیدارمش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ور به زمین آید چون بوتراب</p></div>
<div class="m2"><p>جمله زمین لاله و گل کارمش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ور بسوی روضهٔ جانها رود</p></div>
<div class="m2"><p>یاسمن و سبزه و گلزارمش</p></div></div>
<div class="b2" id="bn47"><p>نوبت ترجیع شد ای جان من</p>
<p>موج زن ای بحر درافشان من</p></div>
<div class="b" id="bn48"><div class="m1"><p>شد سحر ای ساقی ما نوش، نوش</p></div>
<div class="m2"><p>ای ز رخت در دل ما جوش، جوش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بادهٔ حمرای تو همچون پلنگ</p></div>
<div class="m2"><p>گرگ غم اندر کف او موش، موش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چونک برآید به قصور دماغ</p></div>
<div class="m2"><p>افتد از بام نگون هوش، هوش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چونک کشد گوش خرد سوی خود</p></div>
<div class="m2"><p>گوید از درد خرد: « گوش، گوش »</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گوش او: خیز، به جان سجده کن</p></div>
<div class="m2"><p>در قدم این قمر می فروش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفت: کی آمد که ندیدم منش</p></div>
<div class="m2"><p>گفت که: تو خفته بودی دوش دوش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عاشق آید بر معشوقه مست</p></div>
<div class="m2"><p>که نبرد بوی از آن شوش شوش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عشق سوی غیب زند نعرها</p></div>
<div class="m2"><p>بر حس حیوان نزند آن، خروش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شهر پر از بانگ خر و گاو شد</p></div>
<div class="m2"><p>بر سر که باشد بانگ وحوش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ترک سوارست برین یک قدح</p></div>
<div class="m2"><p>ساغر دیگر جهة قوش، قوش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چونک شدی پر ز می لایزال</p></div>
<div class="m2"><p>هیچ نبینی قدحی بوش، بوش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جمله جمادات سلامت کنند</p></div>
<div class="m2"><p>راز بگویند چو خویش، و چو توش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>روح چو ز مهر کنارت گرفت</p></div>
<div class="m2"><p>روح شود پیش تو جمله نقوش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نوبت آن شد که زنم چرخ من</p></div>
<div class="m2"><p>عشق عزل گوید بی روی پوش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همچو گل سرخ سواری کند</p></div>
<div class="m2"><p>جمله ریاحین پی او چون جیوش</p></div></div>
<div class="b2" id="bn63"><p>نقل بیار و می و پیشم نشین</p>
<p>ای رخ تو شمع و میت آتشین</p></div>