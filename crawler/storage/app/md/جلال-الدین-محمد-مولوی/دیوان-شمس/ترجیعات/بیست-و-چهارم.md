---
title: >-
    بیست و چهارم
---
# بیست و چهارم

<div class="b" id="bn1"><div class="m1"><p>امروز به قونیه، می‌خندد صد مه رو</p></div>
<div class="m2"><p>یعنی که ز لارنده، می‌آید شفتالو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پیش چنین خنده، جانست و جهان، بنده</p></div>
<div class="m2"><p>صد جان و جهان نو ، در می‌رسد از هر سو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کهنه بگذار و رو در بر کش یار نو</p></div>
<div class="m2"><p>نو بیش دهد لذت، ای جان و جهان، نوجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم پر ازین خوبان، ما را چه شدست ای جان؟!</p></div>
<div class="m2"><p>هر سوی یکی خسرو، خندان لب و شیرین خو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر چهرهٔ هر یک بت بنوشته که لاتکبت</p></div>
<div class="m2"><p>بر سیب زنخ مرقم من یمشق لایصحو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخیز که تا خیزیم، با دوست درآمیزیم</p></div>
<div class="m2"><p>لالا چه خبر دارد، از ما و ازان لولو؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر گل رخسارش، کز باغ بقا روید</p></div>
<div class="m2"><p>چون فاخته می‌گوید هر بلبل جان: « کوکو »</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر این شکرست ای جان، پس چه بود آن شکر</p></div>
<div class="m2"><p>ای جان مرا مستی، وی درد مرا دارو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بازآمد و بازآمد، آن دلبر زیبا خد</p></div>
<div class="m2"><p>تا فتنه براندازد، زن را ببرد از شو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با خوبی یار من زن چه بود؟! طبلک زن</p></div>
<div class="m2"><p>در مطبخ عشق او، شو چه بود؟ کاسه شو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر درنگری خوش خوش، اندر سرانگشتش</p></div>
<div class="m2"><p>نی جیب نسب گیری، نی چادر اغلاغو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب خفته بدی ای جان، من بودم سرگردان</p></div>
<div class="m2"><p>تا روز دهل می‌زد آن شاه برین بارو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم ز فضولی من: « ای شاه خوش روشن</p></div>
<div class="m2"><p>این کار چه کار تست ؟! کو سنجر و کو قتلو »</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتا: « بنگر آخر از عشق من فاخر</p></div>
<div class="m2"><p>هم خواجه و هم بنده، افتاده میان کو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر طبل کسی دیگر برنارد عاشق سر</p></div>
<div class="m2"><p>پیراهن یوسف را مخصوص و شدست این بو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مستست دماغ من، خواهم سخنی گفتن</p></div>
<div class="m2"><p>تا باشم من مجرم تا باشم یا زقلو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گیرم که بگویم من، چه سود ازین گفتن؟</p></div>
<div class="m2"><p>گوش همه عالم را بر دوزد آن جادو</p></div></div>
<div class="b2" id="bn18"><p>ترجیع کنم ای جان گر زانک بخندی تو</p>
<p>تا از خوشی و مستی بر شیر جهد آهو</p></div>
<div class="b" id="bn19"><div class="m1"><p>ای عید غلام تو، وای جان شده قربانت</p></div>
<div class="m2"><p>تا زنده شود قربان، پیش لبت خندانت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون قند و شکر آید پیش تو؟! که می‌باید</p></div>
<div class="m2"><p>بر قند و شکر خندد آن لعل سخن‌دانت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکس که ذلیل آمد، در عشق عزیز آمد</p></div>
<div class="m2"><p>جز تشنه نیاشامد از چشمهٔ حیوانت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای شادی سرمستان، ای رونق صد بستان</p></div>
<div class="m2"><p>بنگر به تهی‌دستان، هریک شده مهمانت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پرکن قدحی باده، تا دل شود آزاده</p></div>
<div class="m2"><p>جان سیر خورد جانا، از مایدهٔ خوانت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس راز نیوشیدم، بس باده بنوشیدم</p></div>
<div class="m2"><p>رازم همه پیدا کرد، آن بادهٔ پنهانت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای رحمت بی‌پایان وقتست که در احسان</p></div>
<div class="m2"><p>موجی بزند ناگه بحر گهرافشانت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا دامن هر جانی، پر در وگهر گردد</p></div>
<div class="m2"><p>تا غوطه خورد ماهی در قلزم احسانت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وقتیست که سرمستان گیرند ره خانه</p></div>
<div class="m2"><p>شب گشت چه غم از شب با ماه درخشانت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای عید، بیفکن خوان، داد از رمضان بستان</p></div>
<div class="m2"><p>جمعیت نومان ده، زان جعد پریشانت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در پوش لباس نو، خوش بر سر منبر رو</p></div>
<div class="m2"><p>تا سجدهٔ شکر آرد، صد ماه خراسانت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای جان بداندیشش، گستاخ درآ پیشش</p></div>
<div class="m2"><p>من مجرم تو باشم، گر گیرد دربانت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در باز شود والله، دربان بزند قهقه</p></div>
<div class="m2"><p>بوسد کف پای تو، چو نبیند حیرانت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خنده بر یار من، پنهان نتوان کردن</p></div>
<div class="m2"><p>هردم رطلی خنده می‌ریزد در جانت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای جان، ز شراب مر، فربه شدی و لمتر</p></div>
<div class="m2"><p>کز فربهی گردن، بدرید گریبانت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با چهرهٔ چون اطلس، زین اطلس ما را بس</p></div>
<div class="m2"><p>تو نیز شوی چون ما گر روی دهد آنت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زینها بگذشتم من گیر این قدح روشن</p></div>
<div class="m2"><p>مستی کن و باقی را درده به عزیزانت</p></div></div>
<div class="b2" id="bn36"><p>چون خانه روند ایشان شب مانم من تنها</p>
<p>با زنگیکان شب تا روز بکوبم پا</p></div>
<div class="b" id="bn37"><div class="m1"><p>امروز گرو بندم با آن بت شکرخا</p></div>
<div class="m2"><p>من خوشتر می‌خندم، یا آن لب چون حلوا؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من نیم دهان دارم، آخر چه قدر خندم؟!</p></div>
<div class="m2"><p>او همچو درخت گل، خندست ز سر تا پا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هستم کن جانا خوش تا جان بدهد شرحش</p></div>
<div class="m2"><p>تا شهر برآشوبد زین فتنه و زین غوغا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شهری چه محل دارد کز عشق تو شور آرد؟</p></div>
<div class="m2"><p>دیوانه شود ماهی از عشق تو در دریا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر روی زمین ای جان، این سایهٔ عشق آمد</p></div>
<div class="m2"><p>تا چیست خدا داند از عشق، برین بالا!</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کو عالم جسمانی؟! کو عالم روحانی؟!</p></div>
<div class="m2"><p>کو پا و سر گلها؟ کو کر و فر دلها؟!</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با مشعلهٔ جانان، در پیش شعاع جان</p></div>
<div class="m2"><p>تاریک بود انجم، بی‌مغز بود جوزا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون نار نماید آن، خود نور بود آخر</p></div>
<div class="m2"><p>سودای کلیم الله شد جمله ید بیضا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مگریز ز غم ای جان، در درد بجو درمان</p></div>
<div class="m2"><p>کز خار بروید گل، لعل و گهر از خارا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زین جمله گذر کردم ساقی! می جان درده</p></div>
<div class="m2"><p>ای گوشهٔ هر زندان با روی خوشت صحرا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای ساقی روحانی، پیش‌آر می جانی</p></div>
<div class="m2"><p>تو چشمهٔ حیوانی، ما جمله در استسقا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>لب بسته و سرگردان ما را مگذار ای جان</p></div>
<div class="m2"><p>ساغر هله گردان کن، پر بادهٔ جان‌افزا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن بادهٔ جان‌افزا، از دل ببرد غم را</p></div>
<div class="m2"><p>چون سور و طرب سازد هر غصه و ماتم را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون باشد جام جان، خوبی و نظام جان</p></div>
<div class="m2"><p>کز گفتن نام جان، دل می‌برود از جا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفتم به دل: « از محنت، باز آی یکی ساعت »</p></div>
<div class="m2"><p>گفتا که: « نمی‌آیم، کاین خار به از خرما »</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ماهی که هم از اول با حر بیارامد</p></div>
<div class="m2"><p>در جوی نیاساید حوضش نشود مأوا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر آبم در پستی، من بفسرم از هستی</p></div>
<div class="m2"><p>خورشید پرستم من خو کرده در آن گرما</p></div></div>
<div class="b2" id="bn54"><p>در محنت عشق او، درجست دوصد راحت</p>
<p>زین محنت خوش ترسان کی باشد جز ترسا؟!</p></div>