---
title: >-
    سی‌پنجم
---
# سی‌پنجم

<div class="b" id="bn1"><div class="m1"><p>زهی دریا زهی بحر حیاتی</p></div>
<div class="m2"><p>زهی حسن و جمال و فر ذاتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تو جانم براتی خواست از رنج</p></div>
<div class="m2"><p>یکی شمعی فرستادش، براتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تندی عشق او آهن چو مومست</p></div>
<div class="m2"><p>زهی عشق حرون تند عاتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولیکن سر عشقش شکرستان</p></div>
<div class="m2"><p>ز نخلستان ز جوهای فراتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر لب، مه رخان جام بر کف</p></div>
<div class="m2"><p>تو می‌گو هر کرا خواهی که: « هاتی »</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هر لعل لبی بوست رسیده</p></div>
<div class="m2"><p>تو درویشی و آن لعلش زکاتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن شطرنج اگر بردی تو، شاهی</p></div>
<div class="m2"><p>ولی کو بخت پنهان؟! چونک ماتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خداوند شمس دین دریای جان‌بخش</p></div>
<div class="m2"><p>تو شورستان درین دولت، مواتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهی شاهی، لطیفی، بی‌نظیری</p></div>
<div class="m2"><p>که مجموعست ازو جان شتاتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر تبریز دارد حبهٔ زو</p></div>
<div class="m2"><p>چه نقصان گر شود از گنجها، تی</p></div></div>
<div class="b2" id="bn11"><p>هزاران زاهد زهد صلاحی</p>
<p>ز تو خونش مباح و او مباحی</p></div>
<div class="b" id="bn12"><div class="m1"><p>زهی کعبه که تو جان‌بخش حاجی</p></div>
<div class="m2"><p>زهی اقبال هر محتاج راجی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آن سر کو فرو ناید به کیوان</p></div>
<div class="m2"><p>ز روی فخر، بر فرقش تو تاجی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نهاده سر به تسلیم و به طاعت</p></div>
<div class="m2"><p>به پیشت از دل و جان هر لجاجی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهی نور جهان جان، که نورت</p></div>
<div class="m2"><p>نه از خورشید و ماهست و سراجی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه جانها باقطاع مثالت</p></div>
<div class="m2"><p>که بعضی عشری، و بعضی خراجی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خداوند! شمس دینا! این مدیحت</p></div>
<div class="m2"><p>بجای جاه و فرت هست هاجی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایا تبریز، بستان باج جانها</p></div>
<div class="m2"><p>که فرمان ده توی بر جان و باجی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مزاج دل اگر چون برف گردد</p></div>
<div class="m2"><p>ز آتشهای تو گردد نتاجی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرآن جان و دلی کان زنده باشد</p></div>
<div class="m2"><p>ز مهر تستشان دایم تناجی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آن بازار کز تو هست بویی</p></div>
<div class="m2"><p>زهی مر یوسفان را بی‌رواجی</p></div></div>
<div class="b2" id="bn22"><p>به چرخ چارمت عیسیست داعی</p>
<p>به پیش دولتت چاوش ساعی</p></div>
<div class="b" id="bn23"><div class="m1"><p>ز شاه ماست ملک با مرادی</p></div>
<div class="m2"><p>که او ختمست احسان را، و بادی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر احسان را زبان باشد بگردد</p></div>
<div class="m2"><p>به مدح و شکر او سیصد عبادی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان سوی جهان گر گوش داری</p></div>
<div class="m2"><p>چه چاوشان جانندش منادی!</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دهان آفرینش باز مانده</p></div>
<div class="m2"><p>ازان روزی که دیدستش ز شادی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی گوید به عالم او به سوگند</p></div>
<div class="m2"><p>که: « تا زادی، چنین روزی نزادی »</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی چندی نهان شو تا نگردد</p></div>
<div class="m2"><p>همه بازار مه‌رویان کسادی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدیدم عشق خوانی را فتاده</p></div>
<div class="m2"><p>به خاک و خون بگفتم: « چون فتادی؟ »</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که تو خون‌ریز جمله عاشقانی</p></div>
<div class="m2"><p>تو نیزک دل چنین بر باد دادی؟! »</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفتا: « دیده‌ام چیزی که صد ماه</p></div>
<div class="m2"><p>ازو سوزند در نار ودادی »</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خداوند شمس دین! آخر چه نوری؟</p></div>
<div class="m2"><p>فرشته یا پری، یا تش نژادی</p></div></div>
<div class="b2" id="bn33"><p>به تبریز آ دلا، از لحر عشقش</p>
<p>چو بندهٔ عیب ناک اندر مزادی</p></div>