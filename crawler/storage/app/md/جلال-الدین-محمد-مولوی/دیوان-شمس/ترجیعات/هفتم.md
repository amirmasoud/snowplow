---
title: >-
    هفتم
---
# هفتم

<div class="b" id="bn1"><div class="m1"><p>مستی و عاشقی و جوانی و یار ما</p></div>
<div class="m2"><p>نوروز و نوبهار و حمل می‌زند صلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز ندیده چشم جهان این چنین بهار</p></div>
<div class="m2"><p>می‌روید از زمین و ز کهسار کیمیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پهلوی هر درخت یکی حور نیکبخت</p></div>
<div class="m2"><p>دزدیده می‌نماید اگر محرمی لقا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشکوفه می‌خورد ز می روح طاس طاس</p></div>
<div class="m2"><p>بنگر بسوی او که صلا می‌زند ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خوردنش ندیدی اشکوفه‌اش ببین</p></div>
<div class="m2"><p>شاباش ای شکوفه و ای باده مرحبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوسن به غنچه گوید: «برجه چه خفتهٔ</p></div>
<div class="m2"><p>شمعست و شاهدست و شرابست و فتنها »</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریحان و لالها بگرفته پیالها</p></div>
<div class="m2"><p>از کیست این عطا ز کی باشد جز از خدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز حق همه گدا و حزینند و رو ترش</p></div>
<div class="m2"><p>عباس دبس در سر و بیرون چو اغنیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کد کردن از گدا نبود شرط عاقلی</p></div>
<div class="m2"><p>یک جرعه می‌بدیش بدی مست همچو ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنبل به گوش گل پنهان شکر کرد و گفت:</p></div>
<div class="m2"><p>« هرگز مباد سایهٔ یزدان ز ما جدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما خرقها همه بفکندیم پارسال</p></div>
<div class="m2"><p>جانها دریغ نیست چه جای دو سه قبا »</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای آنک کهنه دادی نک تازه باز گیر</p></div>
<div class="m2"><p>کوری هر بخیل بداندیش ژاژخا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر شه عمامه بخشد وین شاه عقل و سر</p></div>
<div class="m2"><p>جانهاست بی‌شمار مر این شاه را عطا</p></div></div>
<div class="b2" id="bn14"><p>ای گلستان خندان رو شکر ابر کن</p>
<p>ترجیع باز گوید باقیش، صبر کن</p></div>
<div class="b" id="bn15"><div class="m1"><p>ای صد هزار رحمت نو ز آسمان داد</p></div>
<div class="m2"><p>هر لحظه بی‌دریغ بران روی خوب باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن رو که روی خوبان پرده و نقاب اوست</p></div>
<div class="m2"><p>جمله فنا شوند چو آن رو کند گشاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زهره چه رو نماید در فر آفتاب</p></div>
<div class="m2"><p>پشه چه حمله آرد در پیش تندباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای شاد آن بهار که در وی نسیم تست</p></div>
<div class="m2"><p>وی شاد آن مرید که باشی توش مراد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از عشق پیش دوست ببستم دمی کمر</p></div>
<div class="m2"><p>آورد تاج زرین بر فرق من نهاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکو برهنه گشت و به بحر تو غوطه خورد</p></div>
<div class="m2"><p>چون پاک دل نباشد و پاکیزه اعتقاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن کز عنایت تو سلاح صلاح یافت</p></div>
<div class="m2"><p>با این چنین صلاح چه غم دارد از فساد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرکس که اعتماد کند بر وفای تو</p></div>
<div class="m2"><p>پا برنهد به فضل برین بام بی‌عماد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مغفور ما تقدم و هم ما تأخرست</p></div>
<div class="m2"><p>ایمن ز انقطاع و ز اعراض و ارتداد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرسبز گشت عالم زیرا که میرآب</p></div>
<div class="m2"><p>آخر زمانیان را آب حیات داد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بختی که قرن پیشین در خواب جسته‌اند</p></div>
<div class="m2"><p>آخر زمانیان را کردست افتقاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حلوا نه او خورد که بد انگشت او دراز</p></div>
<div class="m2"><p>آنکس خورد که باشد مقبول کپقباد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دریای رحمتش ز پری موج می‌زند</p></div>
<div class="m2"><p>هر لحظهٔ بغرد و گوید که: « یا عباد »</p></div></div>
<div class="b2" id="bn28"><p>هم اصل نوبهاری و هم فصل نوبهار</p>
<p>ترجیع سیومست هلا قصه گوش‌دار</p></div>
<div class="b" id="bn29"><div class="m1"><p>شب گشته بود و هرکس در خانه می‌دوید</p></div>
<div class="m2"><p>ناگه نماز شام یکی صبح بردمید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جانی که جانها همگی سایهای اوست</p></div>
<div class="m2"><p>آن جان بران پرورش جانها رسید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا خلق را رهاند زین حبس و تنگنا</p></div>
<div class="m2"><p>بر رخش زین نهاد و سبک تنگ برکشید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از بند و دام غم که گرفتست راه خلق</p></div>
<div class="m2"><p>هردم گشایشیست و گشاینده ناپدید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بگشای سینه را که صبایی همی رسد</p></div>
<div class="m2"><p>مرده حیات یابد و زنده شود قدید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باور نمی‌کنی بسوی باغ رو ببین</p></div>
<div class="m2"><p>کان خاک جرعهٔ ز شراب صبا چشید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر زانکه بر دل تو جفا قفل کرده‌ست</p></div>
<div class="m2"><p>نک طبل می‌زنند که آمد ترا کلید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ور طعنه می‌زنند بر اومید عاشقان</p></div>
<div class="m2"><p>دریا کجا شود به لب این سگان پلید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عیدیست صوفیان را وین طبلها گواه</p></div>
<div class="m2"><p>ور طبل هم نباشد چه کم شود ز عید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بازار آخر آمد هین چه خریدهٔ</p></div>
<div class="m2"><p>شاد آنک داد او شبهٔ گوهری خرید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بشناخت عیبهای متاع غرور را</p></div>
<div class="m2"><p>بگزید عشق یار و عجایب دری گزید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نادر مثلثی که تو داری بخور حلال</p></div>
<div class="m2"><p>خمخانهٔ ابد خنک آ، کاندرو خزید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر لحظهٔ بهار نوست و عقار نو</p></div>
<div class="m2"><p>جانش هزار بار چو گل جامها درید</p></div></div>
<div class="b2" id="bn42"><p>من عشق را بدیدم بر کف نهاده جام</p>
<p>می‌گفت : « عاشقان را از بزم ما سلام »</p></div>