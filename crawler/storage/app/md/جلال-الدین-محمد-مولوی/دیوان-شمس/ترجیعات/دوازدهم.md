---
title: >-
    دوازدهم
---
# دوازدهم

<div class="b" id="bn1"><div class="m1"><p>زان بادهٔ صوفی بود از جام، مجرد</p></div>
<div class="m2"><p>کز غایت مستی ز کفش جام بیفتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حالت مستی چو دل و هوش نگنجید</p></div>
<div class="m2"><p>پس نیست عجیب گر قدح و جام نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول سبقت بود « الف هیچ ندارد »</p></div>
<div class="m2"><p>زان پیش رو افتاد و سپهدار و مؤید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>« حی » نیز اگر هیچ ندارد، چو الف نیز</p></div>
<div class="m2"><p>در صورت جیم آمد، و جیمست مقید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میم از الف و هاست مرکب بنبشتن</p></div>
<div class="m2"><p>ترکیب بود علت بر هستی مفرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس بزم رسول آمد بی‌ساغر و بی‌جام</p></div>
<div class="m2"><p>تا جمع به خود باشد هستی محمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بام فلک از استن و دیوار چو تنهاست</p></div>
<div class="m2"><p>هر بام درافتاده و آن بام مشبد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بالاتر ازین چرخ کهن عالم لطفیست</p></div>
<div class="m2"><p>کارواح در آ، ناحیه مانند، مجدد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عریان شدهٔ بر لب این جوی، پی غسل</p></div>
<div class="m2"><p>نی جوی نماید به نظر صرح ممرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دیو و پری ساخته از پی تغلیط</p></div>
<div class="m2"><p>تا شیشه نماید به نظر آب مسرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از مکر گریزان شو و در وکر رضا رو</p></div>
<div class="m2"><p>تا زنده شوی فارغ از انفاس معدد</p></div></div>
<div class="b2" id="bn12"><p>ترجیع کنم خواجه، که این قافیه تنگست</p>
<p>نی، خود نزنم دم، که دم ما همه ننگست</p></div>
<div class="b" id="bn13"><div class="m1"><p>من دم نزنم، لیک دم نحن نفحنا</p></div>
<div class="m2"><p>در من بدمد، ناله رسد تا به ثریا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این نای تنم را چو ببرید و تراشید</p></div>
<div class="m2"><p>از سوی نیستان عدم عز تعالا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل یکسر نی بود و دهان یکسر دیگر</p></div>
<div class="m2"><p>آن سر ز لب عشق همی بود شکرخا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون از دم او پر شد و از دو لب او مست</p></div>
<div class="m2"><p>تنگ آمد و مستانه، برآورد علالا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>والله ز می آن دو لب ار کوه بنوشد</p></div>
<div class="m2"><p>چون ریگ شود کوه، ز آسیب تجلا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نی پردهٔ لب بود که گر لب بگشاید</p></div>
<div class="m2"><p>نی چرخ فلک ماند و نی زیر ونه بالا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آواز ده اندر عدم ای نای و نظر کن</p></div>
<div class="m2"><p>صد لیلی و مجنون و دو صد وامق و عذرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگشاید هر ذره دهان گوید: « شاباش »</p></div>
<div class="m2"><p>وندر دل هر ذره حقیر آید صحرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زود از حبش تن بسوی روم جنان رو</p></div>
<div class="m2"><p>تا برکشدت قیصر، بر قصر معلا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اینجای نه آنجاست که اینجا بتوان بود</p></div>
<div class="m2"><p>هی، جای خوشی جوی و درآ در صف هیجا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هین، وقت جهادست و گه حملهٔ مردان</p></div>
<div class="m2"><p>صفرا مکن و درشکن از حمله تو، صف را</p></div></div>
<div class="b2" id="bn24"><p>ترجیع سوم آمد و گفتی تو خدایا</p>
<p>« برگم شده مگری که مرا هست عوضها »</p></div>
<div class="b" id="bn25"><div class="m1"><p>آن مطرب خوش نغمهٔ شیرین دهن آمد</p></div>
<div class="m2"><p>جانها همه مستند که آن، جان به من آمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خندان شده اشکوفه و گل جامه دریده</p></div>
<div class="m2"><p>کز سوی عدم سنبله و یاسمن آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جانهای گلستان بدم دی بپریدند</p></div>
<div class="m2"><p>هنگام بهاران شد، و هر جان به تن آمد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خوبان برسیدند ز بتخانهٔ غیبی</p></div>
<div class="m2"><p>کوری خزانی که بخو، بت‌شکن آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون صبر گزیدند بدی جمله درختان</p></div>
<div class="m2"><p>آن هجر چو چاهست و صبوری رسن آمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون صبر گزید آیس، آمد فرجش زود</p></div>
<div class="m2"><p>چون خلق حسن کرد، نگار حسن آمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در عید بهار، ابر برافشاند گلابی</p></div>
<div class="m2"><p>وان رعد بران اوج هوا، طبل زن آمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یک باغ پر از شاهد، نی ترک و نه رومی</p></div>
<div class="m2"><p>کندر حجب غیب، هزاران ختن آمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بس جان که چو یوسف به چه مهلکه افتاد</p></div>
<div class="m2"><p>پنداشت که گم گشت خود او در وطن آمد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زیرا که ره آب خضر مظلم و تاریست</p></div>
<div class="m2"><p>آخر ز ره خار، گل اندر چمن آمد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خامش کن، اگرچه که غزل اغلب باقیست</p></div>
<div class="m2"><p>تا شاه بگوید، چو درین انجمن آمد</p></div></div>
<div class="b2" id="bn36"><p>ای ماه عذار من و ای خوش قد و قامت</p>
<p>برخیز که برخاست ز عشق تو قیامت</p></div>