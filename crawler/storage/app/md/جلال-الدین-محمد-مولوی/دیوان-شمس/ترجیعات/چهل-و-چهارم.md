---
title: >-
    چهل و چهارم
---
# چهل و چهارم

<div class="b" id="bn1"><div class="m1"><p>گر مه و گز زهره و گر فرقدی</p></div>
<div class="m2"><p>از همه سعدان فلک اسعدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستی از چرخ و ازین آسمان</p></div>
<div class="m2"><p>سخت لطیفی، ز کجا آمدی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونک به صورت تو ممثل شوی</p></div>
<div class="m2"><p>ماه رخ و دلبر و زیبا قدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو پدید آمده سودای عشق</p></div>
<div class="m2"><p>وز تو بود خوبی و زیبا خدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گم شدهٔ هر دل و اندیشهٔ</p></div>
<div class="m2"><p>هرچه شود یاوه توش واجدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاتم هر ملک و ممالک توی</p></div>
<div class="m2"><p>تاج سر هر شه و هر سیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوبت خود بر سر گردون زدند</p></div>
<div class="m2"><p>چونک دمی خویش بر ایشان زدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر بدیی کو به تو آورد رو</p></div>
<div class="m2"><p>خوب شود، رسته شود از بدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نظرت معدن هر کیمیا</p></div>
<div class="m2"><p>ای خود تو مشعلهٔ هر خودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خور عامست چنین شرحها</p></div>
<div class="m2"><p>کو صفت و معرفت ایزدی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر برسد برق ازان آسمان</p></div>
<div class="m2"><p>گیرد خورشید و فلک کاسدی</p></div></div>
<div class="b2" id="bn12"><p>گرد نیایند وجود و عدم</p>
<p>عاشقی و شرم، دو ضدند هم</p></div>
<div class="b" id="bn13"><div class="m1"><p>چون تلف عشق موبد شدی</p></div>
<div class="m2"><p>گر تو یکی روح بدی صد شدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مست و خراب و خوش و بیخود شود</p></div>
<div class="m2"><p>خلق، چو تو جلوه‌گر خود شدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای دل من باده بخور فاش فاش</p></div>
<div class="m2"><p>حد نزنندت، چو تو بی‌حد شدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حد اگر باشد هم بگذرد</p></div>
<div class="m2"><p>شاد بمان تو که مخلد شدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای دل پرکینه مصفا شدی</p></div>
<div class="m2"><p>وی تن دیرینه، مجدد شدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مست همی باش و میا سوی خود</p></div>
<div class="m2"><p>چون به خود آیی، تو مقید شدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روح چو بست و بدن همچو خاک</p></div>
<div class="m2"><p>آبی و از خاک مجرد شدی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیره بدی در بن خنب جهان</p></div>
<div class="m2"><p>راوقی اکنون و مصعد شدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خواست چراغت که بمیرد ولیک</p></div>
<div class="m2"><p>رو که به خورشید موید شدی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان تو خفاش بد و باز شد</p></div>
<div class="m2"><p>چونک درین نور معود شدی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم نفسی آمد، لب را ببند</p></div>
<div class="m2"><p>تا بکی ام دم تو درآمد شدی</p></div></div>
<div class="b2" id="bn24"><p>ساقی جان آمد با جام جم</p>
<p>نوبت عشرت شد خامش کنیم</p></div>