---
title: >-
    بیست و پنجم
---
# بیست و پنجم

<div class="b" id="bn1"><div class="m1"><p>شب مست یار بودم و در های های او</p></div>
<div class="m2"><p>حیران آن جمال خوش و شیوهای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه دست می‌زدم که زهی وقت روزگار</p></div>
<div class="m2"><p>گه مست می‌فتادم بر خاک پای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هفت آسمان ز عشق معلق زنان او</p></div>
<div class="m2"><p>فربه شده ز جام خوش جانفزای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوشها فتاده نهایات بیهشی</p></div>
<div class="m2"><p>در گوشها فتاده صریر صلای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بره گوش شیر گرفته ز عدل او</p></div>
<div class="m2"><p>هر ذره گشاده دهان در ثنای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرجا وفاست حاصل، و هرجا که بوالوفاست</p></div>
<div class="m2"><p>بگداخته زخجلت و شرم وفای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمت ضعیف می‌شود از فرص آفتاب</p></div>
<div class="m2"><p>صد همچو آفتاب ضعیف از لقای او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چندان بود ضعیف که یک روز چشم را</p></div>
<div class="m2"><p>سرمه کشد به لطف و کرم توتیای او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن نقدهای قلب که بنهادهٔ به پیش</p></div>
<div class="m2"><p>چون ژیوه می‌طپند پی کیمیای او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر سوت می‌کشند خیالات آن و این</p></div>
<div class="m2"><p>والله کشنده نیست به جز اقتضای او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هریک چو کشتییم که برهم همی زنیم</p></div>
<div class="m2"><p>بحر کرم وی آمد و ما آشنای او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جانم دهی ولی نکشی، ور کشی بگو</p></div>
<div class="m2"><p>من بارها گزارده‌ام خونبهای او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرع عنایت تو بود کوشش مرید</p></div>
<div class="m2"><p>فرع دعای تست حنین و دعای او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر بوی آب تست ورا در سراب میل</p></div>
<div class="m2"><p>بر بوی نقد تست سوی قلب رای او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون تاج عشق بر سر تست ای مرید صدق</p></div>
<div class="m2"><p>سرمست می‌خرام به زیر لوای او</p></div></div>
<div class="b2" id="bn16"><p>ترجیع هم بگویم زیرا که یار خواست</p>
<p>هر کژ که من بگویم، گردد ز یار راست</p></div>
<div class="b" id="bn17"><div class="m1"><p>امسال سال عشرت و ولت در استوا</p></div>
<div class="m2"><p>ای شاد آنکسی که بود طالعش چو ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دف می‌خرید زهره و برهم همی نهاد</p></div>
<div class="m2"><p>می‌ساخت چنگ را سر و پهلو و گردنا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در طبع می‌نهاد هزاران خروش جوش</p></div>
<div class="m2"><p>در نای نی نهاد ز انفاس خود نوا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بنیاد عشرتی که جهان آن ندیده است</p></div>
<div class="m2"><p>خورشید را چه کار به جز گرمی و ضیا؟!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>امسال سال تست، اگر زهره طالعی</p></div>
<div class="m2"><p>زهره جنی ببست ازین مژده دست و پا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خوان ابد، نهاد خدا و اساس نو</p></div>
<div class="m2"><p>من سال و ماه گفتم، از غیرت خدا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای شاه، کژنهادهٔ از مستی آن کلاه</p></div>
<div class="m2"><p>چندان گرو شود به خرابات ما قبا؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جانها فنا شوند ز جام خدای خویش</p></div>
<div class="m2"><p>ز اندیشه باز رسته و از جنگ و ماجرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گوید که: « چون بدیت دران غربت دراز »</p></div>
<div class="m2"><p>گویند : « آنچنان که بود درد بی‌دوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون ماهیان طپان شده بر ریگهای گرم</p></div>
<div class="m2"><p>مهجور از لقای تو ای ماه کبریا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در بحر زاده‌ایم و به خشکی فتاده‌ایم »</p></div>
<div class="m2"><p>ای زادهٔ وفاش تو چونی درین جفا؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منت خدای راست که بازآمدی به بحر</p></div>
<div class="m2"><p>چون صوفیان ببند لب از ذکر مامضی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زیرا که ذکر وحشت هم وحشتیست نو</p></div>
<div class="m2"><p>گفتن ز بعد صلح: « چنین گفتهٔ مرا »</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در بزم اولیا نه شکوفه نه عربده‌ست</p></div>
<div class="m2"><p>در خرمن خدای، نه رخصست و نی غلا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنجا سعادتیست که آن را قیاس نیست</p></div>
<div class="m2"><p>هر لحظه نو به نو متراقیست اجتبا</p></div></div>
<div class="b2" id="bn32"><p>ترجیع سیومست، اگر حق نخواستی</p>
<p>جان را به نظم کردن پروا کجاستی</p></div>
<div class="b" id="bn33"><div class="m1"><p>در روضهٔ ریاحین می‌گرد چپ و راست</p></div>
<div class="m2"><p>گل دسته بستن تو ندانم پی کراست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گل دسته در هوای عفن پایدار نیست</p></div>
<div class="m2"><p>آن را کشیدن این سو، هم حیف و هم خطاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زنجیر بسکلد، بسوی اصل خود رود</p></div>
<div class="m2"><p>زیرا که پروریدهٔ آن معتدل هواست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اینجا قباش ماند، یعنی عبارتی</p></div>
<div class="m2"><p>اما قبای یوسف، دلرا چو توتیاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هین جهد کن تو نیز، که بیرون کنی قبا</p></div>
<div class="m2"><p>در بحر، بی‌قبا شدنت شرط آشناست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای مرد یک قبا، تو قبا بر قبا مپوش</p></div>
<div class="m2"><p>گر بحریی، تجمل و پوشش ترا عراست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>الفقر فخر گفت رسول خدای ازین</p></div>
<div class="m2"><p>سباح فحل و شاه سباحات مصطفاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشتی که داشت، هم ز برای عوام داشت</p></div>
<div class="m2"><p>بهر پیادهٔ چو پیاده شوی، سخاست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اما دغل بسیست، تو کشتی شناس باش</p></div>
<div class="m2"><p>زیرا که کار دنیا سحرست و سیمیاست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دنیا چو کهرباست و همه که رباید او</p></div>
<div class="m2"><p>گندم که مغز دارد، فارغ ز کهرباست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هرکو سفر به بحر کند در سفینه‌اش</p></div>
<div class="m2"><p>او ساکن و رونده و همراه انبیاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در نان بسی برفتی، در آب هم برو</p></div>
<div class="m2"><p>از بعد سیر آب یقین مفرشت سماست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زین‌سان طبق طبق، متعالی همی شوی</p></div>
<div class="m2"><p>اما علای مرتبه جز صورت علاست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این ره چنین دراز به یکدم میسرست</p></div>
<div class="m2"><p>این روضه دور نیست، چو رهبر ترا رضاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آری، دراز و کوته در عالم تنست</p></div>
<div class="m2"><p>اما بر خدا، نه صباحست و نی مساست</p></div></div>
<div class="b2" id="bn48"><p>گر در جفا رود ره وگر در وفا رود</p>
<p>جان توست، جان تو از تو کجا رود؟!</p></div>