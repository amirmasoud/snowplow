---
title: >-
    چهاردهم
---
# چهاردهم

<div class="b" id="bn1"><div class="m1"><p>ای قد و بالای تو حسرت سرو بلند</p></div>
<div class="m2"><p>خنده نمی‌آیدت، بهر دل من بخند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ز تو عالم بجوش، لطف کن، ارزان فروش</p></div>
<div class="m2"><p>خندهٔ‌شیرین نوش راست بفرما، بچند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده زند آفتاب، گیرد عالم خضاب</p></div>
<div class="m2"><p>صدمه وصد آفتاب خنده ز تو می‌برند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله و گلبرگها، عکس تو آمد، مها</p></div>
<div class="m2"><p>نیشکر از قند تو، پر شده بین بند بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طلعتت ای آفتاب، تیغ طرب برکشید</p></div>
<div class="m2"><p>گردن تلخی بزد، بیخ غم و غصه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور قمر درگذشت، زهرء زهرا رسید</p></div>
<div class="m2"><p>گشت جهان گلستان، خار ندارد گزند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزم ابد می‌نهد، شه جهت عاشقان</p></div>
<div class="m2"><p>نعل زرین می‌زند، بهر سم هر سمند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه بگذشت نیز، پیشتر آ ای عزیز</p></div>
<div class="m2"><p>پیش لب نوش تو حلقه بگوش است قند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیشتر آ پیشتر، تا بدهم جان وسر</p></div>
<div class="m2"><p>تا شکفد همچو گل، روی زمین نژند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما و حریفان خوشیم، ساغر حق می‌کشیم</p></div>
<div class="m2"><p>از جهت چشم بد، آتش و مشتی سپند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بوی وصالت رسید، روضهٔ رضوان دمید</p></div>
<div class="m2"><p>صلح کن « الصلح خیر » کوری دیو لوند</p></div></div>
<div class="b2" id="bn12"><p>تازه شو و چست شو، از پی ترجیع را</p>
<p>گوش نوی وام کن تا شنوی ماجرا</p></div>
<div class="b" id="bn13"><div class="m1"><p>شاه هم از بامداد، سرخوش و سرمست خاست</p></div>
<div class="m2"><p>طبل به خود می‌زند، در دل او تا چهاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منتظرست آسمان، تا چه کند قهرمان</p></div>
<div class="m2"><p>هرچه کند گو بکن، هرچه کند جان ماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر نفسی روضهٔ، از تو به پیش دلست</p></div>
<div class="m2"><p>حاتم طی با سخاش، طی شد اگر این سخاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای چو درخت بلند، قبلهٔ هر دردمند</p></div>
<div class="m2"><p>برگ و برش خیره کن، شاخ ترش باوفاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک نفری بخت ور از تو خوش و میوه خور</p></div>
<div class="m2"><p>یک نفری خیره‌سر گشته که آخر کجاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشم بمالید تا خواب جهد از شما</p></div>
<div class="m2"><p>کشف شود کان درخت پهلوی فکر شماست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فکرتها چشمهاست گشته روان زان درخت</p></div>
<div class="m2"><p>پاک کن از جو وحل، کاب ازو بی‌صفاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آب اگر منکر چشمهٔ خود می‌شود</p></div>
<div class="m2"><p>خاک سیه بر سرش باد، کهبس ژاژخاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای طمع ژاژخا، گنده‌تر از گندنا</p></div>
<div class="m2"><p>تات نگیرد بلا، هیچ نگویی خداست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خر ز زدن گشت فرد، کژروی آغاز کرد</p></div>
<div class="m2"><p>راه رها کرد و رفت آن طرفی که گیاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن طرفی که گیاست امن و امان از کجاست؟</p></div>
<div class="m2"><p>غره به سبزی مشو، گرگ سیه در قفاست</p></div></div>
<div class="b2" id="bn24"><p>گوش به ترجیع نه، جانب ره کن رجوع</p>
<p>زانک ملاقات گرگ تلختر آمد ز جوع</p></div>
<div class="b" id="bn25"><div class="m1"><p>ای ز در رحمتت هر نفسی نعمتی</p></div>
<div class="m2"><p>زان همه رحمت، فرست جانب ما رحمتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای به خرابات تو، جام مراعات تو</p></div>
<div class="m2"><p>داده بهر ذرهٔ نوع دگر عشرتی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر نفسی روح نو، بنهد در مردهٔ</p></div>
<div class="m2"><p>هر نفسی راح نو، بخشد بی‌مهلتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خنب تو آمد بجوش، جوش کند نای و نوش</p></div>
<div class="m2"><p>جان سر و پا گم کند چون بخورد شربتی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عفو کن از جام مست خنب و سبو گر شکست</p></div>
<div class="m2"><p>مست شد، و مست را چون نفتد زلتی؟!</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قاعدهٔ خوش نهاد، در طرب و در گشاد</p></div>
<div class="m2"><p>چشم بدش دور باد والله خوش سنتی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بوی تو ای رشک باغ، چون بزند بر دماغ</p></div>
<div class="m2"><p>پر شود از راح روح، بی‌گره و علتی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روح و ملک مست شد از می پوشیدهٔ</p></div>
<div class="m2"><p>چرخ فلک پست شد از پنهان صورتی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بلبلهٔ پر زمی می‌رسدم هر دمی</p></div>
<div class="m2"><p>عربده می‌آورم عشق تو هر ساعتی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنک ره دین بود، پر ز ریاحین بود</p></div>
<div class="m2"><p>هر قدمی گلشنی، هر طرفی جنتی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خط سقبنا بکش بر رخ هر مست خوش</p></div>
<div class="m2"><p>تا که بدانند کو غرقه شد از لذتی</p></div></div>
<div class="b2" id="bn36"><p>ساغر بر ساغرم می‌دهد او هر نفس</p>
<p>نعره‌زنان من که های، پر شدم از باده، بس</p></div>