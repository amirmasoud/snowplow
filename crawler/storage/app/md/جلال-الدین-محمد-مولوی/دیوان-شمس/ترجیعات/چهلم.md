---
title: >-
    چهلم
---
# چهلم

<div class="b" id="bn1"><div class="m1"><p>هله نوش کن شرابی، شده آتشی به تیزی</p></div>
<div class="m2"><p>سوی من بیا و بستان بدو دست، تا نریزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدح و می گزیده، ز کف خدا رسیده</p></div>
<div class="m2"><p>چو خوری، چنان بیفتی که به حشر بر نخیزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و اگر کشی تو گردن، ز می و شراب خوردن</p></div>
<div class="m2"><p>دهمت به قهر خوردن، تو ز من کجا گریزی؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بربود جام مهرش، چو تو صد هزار سرکش</p></div>
<div class="m2"><p>بستان قدح، نظر کن، که تو با کی می‌ستیزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شه خوش‌عذار را بین، که گرفت باده بخشی</p></div>
<div class="m2"><p>سر زلف یار را بین، که گرفت مشک بیزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ز خود برفت ساقی، بدهد قدح گزافی</p></div>
<div class="m2"><p>چو ز خود برفت مطرب، بزند ره حجازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز می خدای یابی تف و آتش جوانی</p></div>
<div class="m2"><p>هنر و وفا نیابی ز حرارت غریزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بستان قدح، نظر کن به صفا و گوهر او</p></div>
<div class="m2"><p>نه ز شیره است این می به خدا، و نی مویزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدرون صبر آمد فرج، و ره گشایش</p></div>
<div class="m2"><p>بدرون خواری آمد شرف و کش و عزیزی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهلم سخن‌فزایی، بهلم حدیث‌خایی</p></div>
<div class="m2"><p>تو بگو که خوش ادایی، عجبی، غریب چیزی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترجیع کن بسازش، چو عروس نو، جهازی</p></div>
<div class="m2"><p>که عروس می‌بنالد بر تو ز بی‌جهیزی</p></div></div>
<div class="b2" id="bn12"><p>عدم و وجود را حق به عطا همی‌نوازد</p>
<p>پدرت اگر ندارد ملکت جهاز سازد</p></div>
<div class="b" id="bn13"><div class="m1"><p>هله ای غریب نادر، تو درین دیار چونی؟</p></div>
<div class="m2"><p>هله ای ندیم دولت، تو درین خمار چونی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز فراق، شهریاری، تو چگونه می‌گذاری</p></div>
<div class="m2"><p>هله ای گل سعادت، به میان خار چونی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به تو آفتاب گوید که: « درآتشیم بی‌تو »</p></div>
<div class="m2"><p>به تو باغ و راغ گوید که: « تو ای بهار چونی؟ »</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو توی حیات جانها، ز چه بند صورتستی؟</p></div>
<div class="m2"><p>چو توی قرار دلها، هله، بی‌قرار چونی؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توی جان هر عروسی، توی سور هردو عالم</p></div>
<div class="m2"><p>خردم بماند خیره، که تو سوگوار چونی؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه تو یوسفی به عالم؟ بشنو یکی سالم</p></div>
<div class="m2"><p>که میان چاه و زندان، تو باختیار چونی؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هله آسمان عزت، تو چرا کبود پوشی؟</p></div>
<div class="m2"><p>هله آفتاب رفعت، تو درین دوار چونی؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پدرت ز جنت آمد، ز بلای گندمی دو</p></div>
<div class="m2"><p>چو هوای جنتستت، تو هریسه خوار چونی؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به میان کاسه‌لیسان، تو چو دیک چند جوشی؟</p></div>
<div class="m2"><p>به میان این حریفان، تو درین قمار چونی؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو بسی سخن بگفتی، خلل سخن نهفتی</p></div>
<div class="m2"><p>محک خدای دیدی، تو در اضطرار چونی؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز چه رو خموش کردی، تو اگر ز اهل دردی</p></div>
<div class="m2"><p>بنظر چو ره‌نوردی، تو در انتظار چونی؟</p></div></div>
<div class="b2" id="bn24"><p>رخت از ضمیر و فکرت به یقین اثر بیابد</p>
<p>چو درون کوزه چیزی بود از برون تلابد</p></div>
<div class="b" id="bn25"><div class="m1"><p>به جناب غیب یاری، به سفر دوید باری</p></div>
<div class="m2"><p>ز فخ زمانه مرغی سره، برپرید، باری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هله ای نکو نهادا، که روانت شاد بادا</p></div>
<div class="m2"><p>که به ظاهر آن شکوفه ز چمن برید، باری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هله، چشم پرنم، تو، زخدای باد روشن</p></div>
<div class="m2"><p>که ز چشم ما سرشک غم تو چکید، باری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرد آهوی ضمیرت ز ریاض قدس بالا</p></div>
<div class="m2"><p>که ز گرگ مرگ صیدت بشد و رمید باری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سوی آسمان غیبی، تو چگونهٔ و چونی؟</p></div>
<div class="m2"><p>که بر آسمان ز یاران اسفا رسید، باری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برهانش ای سعادت، ز فراق و رنج وحشت</p></div>
<div class="m2"><p>که ز دام تنگ صورت، بشد و رهید، باری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز جهان برفت باید، چه جوانی، و چه پیری</p></div>
<div class="m2"><p>خوش و عاشق و مکرم، سبک و شهید، باری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به صلای تو دویدم، ز دیار خود بریدم</p></div>
<div class="m2"><p>به وثاق تو رسیدم، بده آن کلید، باری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر آفتاب عمرم، بمغاربی فروشد</p></div>
<div class="m2"><p>بجز آن سحر ز فضلت، سحری دمید، باری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وگر آن ستاره ناگه، بفسرد از نحوست</p></div>
<div class="m2"><p>من از آفتاب غیبی شده‌ام سعید، باری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>و اگر سزای دنیا نبدم، به عمر کوته</p></div>
<div class="m2"><p>کرم و کرامتت را دل من سزید، باری</p></div></div>
<div class="b2" id="bn36"><p>هله ساقی از فراقت شب و روز در خمارم</p>
<p>تو بیا که من ز مستی سر جام خود ندارم</p></div>