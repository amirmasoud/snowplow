---
title: >-
    بیست و هفتم
---
# بیست و هفتم

<div class="b" id="bn1"><div class="m1"><p>ای درد دهنده‌ام دوا ده</p></div>
<div class="m2"><p>تاریک مکن جهان، ضیا ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد تو دواست و دل ضریرست</p></div>
<div class="m2"><p>آن چشم ضریر را صفا ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نومید همی شود بهر غم</p></div>
<div class="m2"><p>نومید شونده را رجا ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دیده که بهر تو بگرید</p></div>
<div class="m2"><p>کحلش کش و نور مصطفی ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکرش ده، وانگهیش نعمت</p></div>
<div class="m2"><p>صبرش ده، وانگهش بلا ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر جان ز جهان وفا ندارد</p></div>
<div class="m2"><p>از رحمت خویششان وفا ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوی تو خوش است، هم خوشی بخش</p></div>
<div class="m2"><p>کار تو عطاست، هم عطا ده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن نی که دم تو خورد روزی</p></div>
<div class="m2"><p>بازش ز دم خوشت نواده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این قفل تو کردهٔ برین دل</p></div>
<div class="m2"><p>بفرست کلید و دلگشا ده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس طاقت خشم تو ندارد</p></div>
<div class="m2"><p>این خشم ببر عوض رضا ده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غم منکر بس نکیر آمد</p></div>
<div class="m2"><p>زومان بستان به آشنا ده</p></div></div>
<div class="b2" id="bn12"><p>رحم آر برین فغان و تشنیع</p>
<p>ورنه کنمش قرین ترجیع</p></div>
<div class="b" id="bn13"><div class="m1"><p>چون باخبری ز هر فغانی</p></div>
<div class="m2"><p>زین حالت آتشین، امانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مهمان من آمدست اندوه</p></div>
<div class="m2"><p>خون ریز و درشت میهمانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک لقمه کند هزار جان را</p></div>
<div class="m2"><p>کی داو، دهد به نیم جانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر سیلی او چو ذوالفقاری</p></div>
<div class="m2"><p>هر نکتهٔ او یکی سنانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زو تلخ شده دهان دریا</p></div>
<div class="m2"><p>چون تلخ شد آنچنان دهانی؟!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دریاچه بود؟! که از نهیبش</p></div>
<div class="m2"><p>پوشید کبود، آسمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ماییم سرشتهٔ نوازش</p></div>
<div class="m2"><p>پروردهٔ نازنین جهانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خو کرده به سلسبیل و تسنیم</p></div>
<div class="m2"><p>با ساقی چون شکرستانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با جمع شکر لبان رقاص</p></div>
<div class="m2"><p>هر لحظه عروسیی و خوانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این عیش و طرب دریغ باشد</p></div>
<div class="m2"><p>کاشفته شود به امتحانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حیفست که مجلس لطیفان</p></div>
<div class="m2"><p>ناخوش شود از چنین گرانی</p></div></div>
<div class="b2" id="bn24"><p>ترجیع سوم رسید یارا</p>
<p>هم بر سر عیش آر ما را</p></div>
<div class="b" id="bn25"><div class="m1"><p>در چاه فتاد دل، برآرش</p></div>
<div class="m2"><p>بیچاره و منتظر مدارش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ور وعده دهیش تا به فردا</p></div>
<div class="m2"><p>امروز بسوزد این شرارش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخشای برین اسیر هجران</p></div>
<div class="m2"><p>بر جان ضعیف بی‌قرارش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرچند که ظالمست و مجرم</p></div>
<div class="m2"><p>مظلوم و شکسته دل شمارش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گشتست چو لاله غرقهٔ خون</p></div>
<div class="m2"><p>گشتست چو زعفران عذارش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خواهد که به پیش تو بمیرد</p></div>
<div class="m2"><p>اینست همیشه کسب و کارش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یاری دگری کجا پسندد</p></div>
<div class="m2"><p>آن را که خدا به دست یارش؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن را که بخواندهٔ تو روزی</p></div>
<div class="m2"><p>مسپار بدست روزگارش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرچند به زیر کوه غم ماند</p></div>
<div class="m2"><p>اندیشهٔ تست یار غارش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>امسال چو ماه می‌گدازد</p></div>
<div class="m2"><p>می‌آید یاد وصل پارش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>راهی بگشا درین بیابان</p></div>
<div class="m2"><p>ماهی بنما درین غبارش</p></div></div>
<div class="b2" id="bn36"><p>گر شرح کنم تمام پیغام</p>
<p>می‌مانم از شراب و از جام</p></div>