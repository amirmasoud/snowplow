---
title: >-
    رباعی شمارهٔ ۱۹۸
---
# رباعی شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>ای عقل برو که عاقل اینجا نیست</p></div>
<div class="m2"><p>گر موی شوی موی ترا گنجانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز آمد و روز هر چراغی که فروخت</p></div>
<div class="m2"><p>در شعلهٔ آفتاب جز رسوا نیست</p></div></div>