---
title: >-
    رباعی شمارهٔ ۳۷۱
---
# رباعی شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>گر باد بر آن زلف پریشان زندت</p></div>
<div class="m2"><p>مه طال بقا از بن دندان زندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ناصح من ز خود برآئی و ز نصح</p></div>
<div class="m2"><p>گر زانچه دلم چشیده بر جان زندت</p></div></div>