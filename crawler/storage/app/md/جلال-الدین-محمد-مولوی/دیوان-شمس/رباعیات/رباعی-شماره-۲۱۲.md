---
title: >-
    رباعی شمارهٔ ۲۱۲
---
# رباعی شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>این فصل بهار نیست فصلی دگر است</p></div>
<div class="m2"><p>مخموری هر چشم ز وصلی دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که جمله شاخ‌ها رقصانند</p></div>
<div class="m2"><p>جنبیدن هر شاخ ز اصلی دگر است</p></div></div>