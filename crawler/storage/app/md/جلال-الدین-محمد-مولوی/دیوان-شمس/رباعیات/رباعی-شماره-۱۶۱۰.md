---
title: >-
    رباعی شمارهٔ ۱۶۱۰
---
# رباعی شمارهٔ ۱۶۱۰

<div class="b" id="bn1"><div class="m1"><p>ای سرو ز قامت تو قد دزدیده</p></div>
<div class="m2"><p>گل پیش رخ تو پیرهن بدریده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردار یکی آینه از بهر خدای</p></div>
<div class="m2"><p>تا همچو خودی شنیده‌ای یا دیده</p></div></div>