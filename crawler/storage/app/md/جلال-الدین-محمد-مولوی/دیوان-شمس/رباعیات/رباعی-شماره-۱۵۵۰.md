---
title: >-
    رباعی شمارهٔ ۱۵۵۰
---
# رباعی شمارهٔ ۱۵۵۰

<div class="b" id="bn1"><div class="m1"><p>ای ساقی جان برین خوش آواز برو</p></div>
<div class="m2"><p>ساز ازلیست هم بر این ساز برو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باز چو طبل باز او بشنیدی</p></div>
<div class="m2"><p>شه منتظر تست سبک باز برو</p></div></div>