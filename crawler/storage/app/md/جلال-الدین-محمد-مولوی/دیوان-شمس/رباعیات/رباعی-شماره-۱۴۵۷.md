---
title: >-
    رباعی شمارهٔ ۱۴۵۷
---
# رباعی شمارهٔ ۱۴۵۷

<div class="b" id="bn1"><div class="m1"><p>حرص و حسد و کینه ز دل بیرون کن</p></div>
<div class="m2"><p>خوی بدو اندیشه تو دیگرگون کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انکار زیان تست زو کمتر گیر</p></div>
<div class="m2"><p>اقرار ترا سود دهد افزون کن</p></div></div>