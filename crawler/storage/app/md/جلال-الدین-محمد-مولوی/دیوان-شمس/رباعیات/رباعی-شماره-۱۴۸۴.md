---
title: >-
    رباعی شمارهٔ ۱۴۸۴
---
# رباعی شمارهٔ ۱۴۸۴

<div class="b" id="bn1"><div class="m1"><p>سرمست شدم در هوس سرمستان</p></div>
<div class="m2"><p>از دست شدم در ظفر آن دستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیزار شدم ز عقل و دیوانه شدم</p></div>
<div class="m2"><p>تا درکشدم عشق به بیمارستان</p></div></div>