---
title: >-
    رباعی شمارهٔ ۱۹۲۳
---
# رباعی شمارهٔ ۱۹۲۳

<div class="b" id="bn1"><div class="m1"><p>گر عقل به کوی دوست رهبر نبدی</p></div>
<div class="m2"><p>روی عاشق چنین مزعفر نبدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آنکه صدف را غم گوهر نبدی</p></div>
<div class="m2"><p>بگشاده لب و عاشق و مضطر نبدی</p></div></div>