---
title: >-
    رباعی شمارهٔ ۱۶۳۱
---
# رباعی شمارهٔ ۱۶۳۱

<div class="b" id="bn1"><div class="m1"><p>صحت که کشد به سقم و رنجوری به</p></div>
<div class="m2"><p>زان جامه که سازی بستم عوری به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی که نبیند ره حق کوری به</p></div>
<div class="m2"><p>صحبت که تقرب نبود دوری به</p></div></div>