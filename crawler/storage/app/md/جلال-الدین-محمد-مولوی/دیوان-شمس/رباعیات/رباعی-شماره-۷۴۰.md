---
title: >-
    رباعی شمارهٔ ۷۴۰
---
# رباعی شمارهٔ ۷۴۰

<div class="b" id="bn1"><div class="m1"><p>شادی زمانه با غمم برنامد</p></div>
<div class="m2"><p>جز از غم دوست مرهمم برنامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که به بینمش چه دمها دهمش</p></div>
<div class="m2"><p>چون راست بدیدمش دمم برنامد</p></div></div>