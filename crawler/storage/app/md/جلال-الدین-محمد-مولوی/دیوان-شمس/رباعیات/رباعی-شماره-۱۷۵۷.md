---
title: >-
    رباعی شمارهٔ ۱۷۵۷
---
# رباعی شمارهٔ ۱۷۵۷

<div class="b" id="bn1"><div class="m1"><p>این شاخ شکوفه بارگیرد روزی</p></div>
<div class="m2"><p>وین باز طلب شکار گیرد روزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌آید و میرود خیالش بر تو</p></div>
<div class="m2"><p>تا چند رود قرار گیرد روزی</p></div></div>