---
title: >-
    رباعی شمارهٔ ۱۵۸۲
---
# رباعی شمارهٔ ۱۵۸۲

<div class="b" id="bn1"><div class="m1"><p>گفتم که کجا بود مها خانهٔ تو</p></div>
<div class="m2"><p>گفتا که دل خراب مستانهٔ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خورشیدم درون ویرانه روم</p></div>
<div class="m2"><p>ای مست، خراب باد کاشانهٔ تو</p></div></div>