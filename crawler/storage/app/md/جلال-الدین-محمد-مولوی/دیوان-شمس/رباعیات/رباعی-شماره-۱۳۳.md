---
title: >-
    رباعی شمارهٔ ۱۳۳
---
# رباعی شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>آن روح که بسته بود در نقش صفات</p></div>
<div class="m2"><p>از پرتو مصطفی درآمد بر ذات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واندم که روان گشت ز شادی میگفت</p></div>
<div class="m2"><p>شادی روان مصطفی را صلوات</p></div></div>