---
title: >-
    رباعی شمارهٔ ۱۲۶۵
---
# رباعی شمارهٔ ۱۲۶۵

<div class="b" id="bn1"><div class="m1"><p>عشق تو گرفته آستین می‌کشدم</p></div>
<div class="m2"><p>واندر پی یار راستین می‌کشدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگه گوئی دراز تا چند کشی</p></div>
<div class="m2"><p>با عشق بگو که همچنین می‌کشدم</p></div></div>