---
title: >-
    رباعی شمارهٔ ۱۴۴۷
---
# رباعی شمارهٔ ۱۴۴۷

<div class="b" id="bn1"><div class="m1"><p>تو شاه دل منی و شاهی می‌کن</p></div>
<div class="m2"><p>نوشت بادا ظلم سپاهی می‌کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کف داری شراب و جامی که مپرس</p></div>
<div class="m2"><p>آن را بده و تو هرچه خواهی می‌کن</p></div></div>