---
title: >-
    رباعی شمارهٔ ۱۹۸۷
---
# رباعی شمارهٔ ۱۹۸۷

<div class="b" id="bn1"><div class="m1"><p>هر لحظه مها پیش خودم می‌خوانی</p></div>
<div class="m2"><p>احوال همی پرسی و خود می‌دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو سرو روانی و سخن پیش تو باد</p></div>
<div class="m2"><p>می‌گویم و سر به خیره می‌جنبانی</p></div></div>