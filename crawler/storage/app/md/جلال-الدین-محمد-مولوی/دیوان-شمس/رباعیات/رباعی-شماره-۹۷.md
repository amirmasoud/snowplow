---
title: >-
    رباعی شمارهٔ ۹۷
---
# رباعی شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>خواب آمد و در چشم نبد موضع خواب</p></div>
<div class="m2"><p>زیرا ز تو چشم بود پرآتش و آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد جانب دل دیدددلی چون سیماب</p></div>
<div class="m2"><p>شد جانب تن دید خراب و چه خراب</p></div></div>