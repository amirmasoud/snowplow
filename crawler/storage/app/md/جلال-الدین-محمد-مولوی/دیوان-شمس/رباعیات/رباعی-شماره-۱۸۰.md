---
title: >-
    رباعی شمارهٔ ۱۸۰
---
# رباعی شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>ای بی‌خبر از مغز شده غره بپوست</p></div>
<div class="m2"><p>هشدار که در میان جانداری دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حس مغز تنست و مغز حست جانست</p></div>
<div class="m2"><p>چون از تن و حس و جان گذشتی همه اوست</p></div></div>