---
title: >-
    رباعی شمارهٔ ۱۱۹۴
---
# رباعی شمارهٔ ۱۱۹۴

<div class="b" id="bn1"><div class="m1"><p>تا روی تو دیدم از جهان سیر شدم</p></div>
<div class="m2"><p>روباه بدم ز فر تو شیر شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای پای نهاده بر سر خلق ز کبر</p></div>
<div class="m2"><p>این نیز بیندیش که سر زیر شدم</p></div></div>