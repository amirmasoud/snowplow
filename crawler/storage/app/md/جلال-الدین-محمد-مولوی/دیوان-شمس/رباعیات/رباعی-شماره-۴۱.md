---
title: >-
    رباعی شمارهٔ ۴۱
---
# رباعی شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>جز عشق نبود هیچ دمساز مرا</p></div>
<div class="m2"><p>نی اول و نی آخر و آغاز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان می‌دهد از درونه آواز مرا</p></div>
<div class="m2"><p>کی کاهل راه عشق درباز مرا</p></div></div>