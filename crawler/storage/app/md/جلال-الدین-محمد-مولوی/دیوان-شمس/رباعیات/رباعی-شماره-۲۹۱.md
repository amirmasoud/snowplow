---
title: >-
    رباعی شمارهٔ ۲۹۱
---
# رباعی شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>حاشا که به عالم از تو خوشتر یاریست</p></div>
<div class="m2"><p>یا خوبتر از دیدن رویت کاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر دو جهان دلبر و یارم تو بسی</p></div>
<div class="m2"><p>هم پرتو تست هر کجا دلداریست</p></div></div>