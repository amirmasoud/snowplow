---
title: >-
    رباعی شمارهٔ ۱۵۴۳
---
# رباعی شمارهٔ ۱۵۴۳

<div class="b" id="bn1"><div class="m1"><p>ای جان جهان جز تو کسی کیست بگو</p></div>
<div class="m2"><p>بی‌جان و جهان هیچ کسی زیست بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بد کنم و تو بد مکافات دهی</p></div>
<div class="m2"><p>پس فرق میان من و تو چیست بگو</p></div></div>