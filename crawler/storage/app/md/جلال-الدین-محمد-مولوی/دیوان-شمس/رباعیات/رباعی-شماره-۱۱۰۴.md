---
title: >-
    رباعی شمارهٔ ۱۱۰۴
---
# رباعی شمارهٔ ۱۱۰۴

<div class="b" id="bn1"><div class="m1"><p>نومید مشو امید می‌دار ای دل</p></div>
<div class="m2"><p>در غیب عجایب است بسیار ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جمله جهان قصد به جان تو کنند</p></div>
<div class="m2"><p>تو دامن دوست را نه بگذار ای دل</p></div></div>