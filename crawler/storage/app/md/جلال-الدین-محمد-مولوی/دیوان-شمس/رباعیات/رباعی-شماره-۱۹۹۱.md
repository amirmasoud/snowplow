---
title: >-
    رباعی شمارهٔ ۱۹۹۱
---
# رباعی شمارهٔ ۱۹۹۱

<div class="b" id="bn1"><div class="m1"><p>یاد تو کنم میان یادم باشی</p></div>
<div class="m2"><p>لب بگشایم در این گشادم باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شاد شوم ضمیر شادم باشی</p></div>
<div class="m2"><p>حیله طلبم تو اوستادم باشی</p></div></div>