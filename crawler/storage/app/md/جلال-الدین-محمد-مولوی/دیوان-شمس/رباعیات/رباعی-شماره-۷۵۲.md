---
title: >-
    رباعی شمارهٔ ۷۵۲
---
# رباعی شمارهٔ ۷۵۲

<div class="b" id="bn1"><div class="m1"><p>صد سال بقای آن بت مهوش باد</p></div>
<div class="m2"><p>تیر غم او دل من ترکش باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک درش بمرد خوش خوش دل من</p></div>
<div class="m2"><p>یارب که دعا کرد که خاکش خوش باد</p></div></div>