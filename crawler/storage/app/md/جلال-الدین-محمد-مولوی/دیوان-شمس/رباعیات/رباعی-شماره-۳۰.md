---
title: >-
    رباعی شمارهٔ ۳۰
---
# رباعی شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>با عشق روان شد از عدم مرکب ما</p></div>
<div class="m2"><p>روشن ز شراب وصل دائم شب ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان می که حرام نیست در مذهب ما</p></div>
<div class="m2"><p>تا صبح عدم خشک نیابی لب ما</p></div></div>