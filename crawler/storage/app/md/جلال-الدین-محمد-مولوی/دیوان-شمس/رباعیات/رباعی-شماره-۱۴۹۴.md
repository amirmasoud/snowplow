---
title: >-
    رباعی شمارهٔ ۱۴۹۴
---
# رباعی شمارهٔ ۱۴۹۴

<div class="b" id="bn1"><div class="m1"><p>عید آمد و عیدانه جمال سلطان</p></div>
<div class="m2"><p>عیدانه که دیده است چنین در دو جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید این بود و هزار عید ای دل و جان</p></div>
<div class="m2"><p>کان گنج جهان برآمد از کنج نهان</p></div></div>