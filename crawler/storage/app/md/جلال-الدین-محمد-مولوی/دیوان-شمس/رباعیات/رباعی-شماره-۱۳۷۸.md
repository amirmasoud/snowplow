---
title: >-
    رباعی شمارهٔ ۱۳۷۸
---
# رباعی شمارهٔ ۱۳۷۸

<div class="b" id="bn1"><div class="m1"><p>آمد دل من بهر نشانم گفتن</p></div>
<div class="m2"><p>گفتا ز برای او چه دانم گفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که از آن دو چشم یک حرف بگوی</p></div>
<div class="m2"><p>گفتا که دو چشم را چه تانم گفتن</p></div></div>