---
title: >-
    رباعی شمارهٔ ۱۵۲۷
---
# رباعی شمارهٔ ۱۵۲۷

<div class="b" id="bn1"><div class="m1"><p>یارب چه دلست این و چه خو دارد این</p></div>
<div class="m2"><p>در جستن او چه جستجو دارد این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک درش هر نفسی سر بنهد</p></div>
<div class="m2"><p>خاکش گوید هزار رو دارد این</p></div></div>