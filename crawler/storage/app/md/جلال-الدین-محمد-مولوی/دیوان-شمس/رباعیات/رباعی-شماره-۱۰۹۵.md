---
title: >-
    رباعی شمارهٔ ۱۰۹۵
---
# رباعی شمارهٔ ۱۰۹۵

<div class="b" id="bn1"><div class="m1"><p>الخمر و من‌الزق ینادیک تعال</p></div>
<div class="m2"><p>واقطع لوصالنا جمیع‌الاشغال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فربا و صفاء و سبقنا الحوال</p></div>
<div class="m2"><p>کی نعتق بالنجدة روح‌العمال</p></div></div>