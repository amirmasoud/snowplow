---
title: >-
    رباعی شمارهٔ ۹۷۰
---
# رباعی شمارهٔ ۹۷۰

<div class="b" id="bn1"><div class="m1"><p>نی چارهٔ آنکه با تو باشم همراز</p></div>
<div class="m2"><p>نی زهرهٔ آنکه بی‌تو پردازم راز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارم ز تو البته نمیگردد ساز</p></div>
<div class="m2"><p>کار من بیچاره حدیثی است دراز</p></div></div>