---
title: >-
    رباعی شمارهٔ ۲۷۸
---
# رباعی شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>جانی که به راه عشق تو در خطر است</p></div>
<div class="m2"><p>بس دیده ز جاهلی بر او نوحه‌گر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاصل چشمی که بیندش نشناسد</p></div>
<div class="m2"><p>کو را بر رخ هزار صاحب خبر است</p></div></div>