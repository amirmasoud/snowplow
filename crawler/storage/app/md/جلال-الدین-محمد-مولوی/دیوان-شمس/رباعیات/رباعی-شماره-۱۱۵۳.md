---
title: >-
    رباعی شمارهٔ ۱۱۵۳
---
# رباعی شمارهٔ ۱۱۵۳

<div class="b" id="bn1"><div class="m1"><p>اندر طلب دوست همی بشتابم</p></div>
<div class="m2"><p>عمرم به کران رسید و من در خوابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که وصال دوست در خواهم یافت</p></div>
<div class="m2"><p>این عمر گذشته را کجا دریابم</p></div></div>