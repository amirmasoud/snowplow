---
title: >-
    رباعی شمارهٔ ۱۴۹۳
---
# رباعی شمارهٔ ۱۴۹۳

<div class="b" id="bn1"><div class="m1"><p>عقلی که خلاف تو گزیدن نتوان</p></div>
<div class="m2"><p>دینی که ز عهد تو بریدن نتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علمی که به کنه تو رسیدن نتوان</p></div>
<div class="m2"><p>زهدی که در دام تو رهیدن نتوان</p></div></div>