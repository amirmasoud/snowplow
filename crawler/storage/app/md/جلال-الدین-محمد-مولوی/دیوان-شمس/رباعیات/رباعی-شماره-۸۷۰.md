---
title: >-
    رباعی شمارهٔ ۸۷۰
---
# رباعی شمارهٔ ۸۷۰

<div class="b" id="bn1"><div class="m1"><p>امروز من از تشنه دهانی و خمار</p></div>
<div class="m2"><p>نی دل دارم نه عقل و نه صبر و قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌آیم و می‌روم چو انگور افشار</p></div>
<div class="m2"><p>آخر قدح شیره به عصار بسیار</p></div></div>