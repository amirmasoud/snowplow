---
title: >-
    رباعی شمارهٔ ۹۷۲
---
# رباعی شمارهٔ ۹۷۲

<div class="b" id="bn1"><div class="m1"><p>یاری خواهی ز یار با یار بساز</p></div>
<div class="m2"><p>سودت سوداست با خریدار بساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر وصال ماه از شب مگریز</p></div>
<div class="m2"><p>وز بهر گل و گلاب با خار بساز</p></div></div>