---
title: >-
    رباعی شمارهٔ ۱۸۴۶
---
# رباعی شمارهٔ ۱۸۴۶

<div class="b" id="bn1"><div class="m1"><p>خواهی که در این زمانه فردی گردی</p></div>
<div class="m2"><p>یا در ره دین صاحب دردی گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این را به جز از صحبت مردان مطلب</p></div>
<div class="m2"><p>مردی گردی چو گرد مردی گردی</p></div></div>