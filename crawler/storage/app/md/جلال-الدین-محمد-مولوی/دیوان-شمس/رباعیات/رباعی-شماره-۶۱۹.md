---
title: >-
    رباعی شمارهٔ ۶۱۹
---
# رباعی شمارهٔ ۶۱۹

<div class="b" id="bn1"><div class="m1"><p>جان باز که وصل او به دستان ندهند</p></div>
<div class="m2"><p>شیر از قدح شرع به مستان ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که مجردان بهم می‌نوشند</p></div>
<div class="m2"><p>یک جرعه به خویشتن‌پرستان ندهند</p></div></div>