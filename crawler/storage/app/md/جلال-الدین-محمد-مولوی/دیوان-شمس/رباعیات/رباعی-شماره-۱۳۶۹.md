---
title: >-
    رباعی شمارهٔ ۱۳۶۹
---
# رباعی شمارهٔ ۱۳۶۹

<div class="b" id="bn1"><div class="m1"><p>هوش عاشق کجا بود سوی نسیم</p></div>
<div class="m2"><p>هوش عاقل کجا بود با زر و سیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جای گلها کجا بود باغ و نعیم</p></div>
<div class="m2"><p>جای هیزم کجا بود قعر جحیم</p></div></div>