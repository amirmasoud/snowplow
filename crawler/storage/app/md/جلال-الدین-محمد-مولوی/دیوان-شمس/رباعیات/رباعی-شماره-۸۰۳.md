---
title: >-
    رباعی شمارهٔ ۸۰۳
---
# رباعی شمارهٔ ۸۰۳

<div class="b" id="bn1"><div class="m1"><p>کی غم خورد آنکه با تو خرم باشد</p></div>
<div class="m2"><p>ور نور تو آفتاب عالم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسرار جهان چگونه پوشیده شود</p></div>
<div class="m2"><p>بر خاطره آنکه با تو محرم باشد</p></div></div>