---
title: >-
    رباعی شمارهٔ ۳۶۰
---
# رباعی شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>عشق آمد و شد چو خونم اندر رگ و پوست</p></div>
<div class="m2"><p>تا کرد مرا تهی و پر کرد ز دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجزای وجود من همه دوست گرفت</p></div>
<div class="m2"><p>نامیست ز من بر من و باقی همه اوست</p></div></div>