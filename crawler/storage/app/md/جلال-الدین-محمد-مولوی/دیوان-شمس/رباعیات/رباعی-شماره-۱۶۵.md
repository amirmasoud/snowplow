---
title: >-
    رباعی شمارهٔ ۱۶۵
---
# رباعی شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>امروز مهم دست زنان آمده است</p></div>
<div class="m2"><p>پیدا و نهان چو نقش جان آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست و خوش و شنگ و بی‌امان آمده است</p></div>
<div class="m2"><p>زانروی چنینم که چنان آمده است</p></div></div>