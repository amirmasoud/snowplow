---
title: >-
    رباعی شمارهٔ ۸۹۷
---
# رباعی شمارهٔ ۸۹۷

<div class="b" id="bn1"><div class="m1"><p>در نوبت عشق چشم باشد در بار</p></div>
<div class="m2"><p>چون او بگذشت دل بروید چو بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دم چو بهار است ز روی دلدار</p></div>
<div class="m2"><p>چون کار به نوبت است دم را هشدار</p></div></div>