---
title: >-
    رباعی شمارهٔ ۱۲۳۱
---
# رباعی شمارهٔ ۱۲۳۱

<div class="b" id="bn1"><div class="m1"><p>دل میگوید که نقد این باغ دریم</p></div>
<div class="m2"><p>امروز چریدیم و به شب هم بچریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب میگزدش عقل که گستاخ مرو</p></div>
<div class="m2"><p>گرچه در رحمت است زحمت ببریم</p></div></div>