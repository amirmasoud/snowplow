---
title: >-
    رباعی شمارهٔ ۱۶۸۱
---
# رباعی شمارهٔ ۱۶۸۱

<div class="b" id="bn1"><div class="m1"><p>امروز مرا سخت پریشان کردی</p></div>
<div class="m2"><p>پوشیدهٔ خویش را تو عریان کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دوش حریف تو نگشتم از خواب</p></div>
<div class="m2"><p>خوردی و نصیب بنده پنهان کردی</p></div></div>