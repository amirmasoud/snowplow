---
title: >-
    رباعی شمارهٔ ۶۴۳
---
# رباعی شمارهٔ ۶۴۳

<div class="b" id="bn1"><div class="m1"><p>چون صورت تو در دل ما بازآید</p></div>
<div class="m2"><p>مسکین دل گمگشته بجا بازآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عمر گذشت و یک نفس بیش نماند</p></div>
<div class="m2"><p>چون او برسد گذشته‌ها بازآید</p></div></div>