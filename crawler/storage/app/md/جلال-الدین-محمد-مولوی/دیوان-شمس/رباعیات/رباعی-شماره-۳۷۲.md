---
title: >-
    رباعی شمارهٔ ۳۷۲
---
# رباعی شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>گر بر سر شهوت و هوا خواهی رفت</p></div>
<div class="m2"><p>از من خبرت که بینوا خواهی رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور درگذری از این ببینی بعیان</p></div>
<div class="m2"><p>کز بهر چه آمدی کجا خواهی رفت</p></div></div>