---
title: >-
    رباعی شمارهٔ ۲۶۳
---
# رباعی شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>تا تن نبری دور زمانم کشته است</p></div>
<div class="m2"><p>آن چشمهٔ آب حیوانم کشته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او نیست عجب که دشمن جانش کشت</p></div>
<div class="m2"><p>من بوالعجبم که جان جانم کشته است</p></div></div>