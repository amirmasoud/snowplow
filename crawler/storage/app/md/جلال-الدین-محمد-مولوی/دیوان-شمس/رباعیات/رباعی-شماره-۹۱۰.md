---
title: >-
    رباعی شمارهٔ ۹۱۰
---
# رباعی شمارهٔ ۹۱۰

<div class="b" id="bn1"><div class="m1"><p>گفتم بنما که چون کنم بمیر</p></div>
<div class="m2"><p>گفتم که: شد آب روغنم گفت بمیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که شوم شمع من پروانه</p></div>
<div class="m2"><p>ای رو تو شمع روشنم گفت بمیر</p></div></div>