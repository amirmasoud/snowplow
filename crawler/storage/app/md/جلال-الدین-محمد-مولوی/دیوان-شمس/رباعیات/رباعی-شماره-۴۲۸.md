---
title: >-
    رباعی شمارهٔ ۴۲۸
---
# رباعی شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>نه چرخ غلام طبع خود رایهٔ ماست</p></div>
<div class="m2"><p>هستی ز برای نیستی مایهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر پس پرده‌ها یکی دایهٔ ماست</p></div>
<div class="m2"><p>ما آمده نیستیم این سایهٔ ماست</p></div></div>