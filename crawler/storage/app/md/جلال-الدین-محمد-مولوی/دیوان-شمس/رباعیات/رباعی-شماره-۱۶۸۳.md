---
title: >-
    رباعی شمارهٔ ۱۶۸۳
---
# رباعی شمارهٔ ۱۶۸۳

<div class="b" id="bn1"><div class="m1"><p>امشب که فتاده‌ای به چنگال رهی</p></div>
<div class="m2"><p>بسیار طپی ولیک دشوار رهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله نرهی ز بنده‌ای سرو سهی</p></div>
<div class="m2"><p>تا سینه به این دل خرابم ننهی</p></div></div>