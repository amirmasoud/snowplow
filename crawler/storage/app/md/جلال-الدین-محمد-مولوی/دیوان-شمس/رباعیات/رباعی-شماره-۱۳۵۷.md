---
title: >-
    رباعی شمارهٔ ۱۳۵۷
---
# رباعی شمارهٔ ۱۳۵۷

<div class="b" id="bn1"><div class="m1"><p>می‌پنداری که از غمانت رستم</p></div>
<div class="m2"><p>یا بی‌تو صبور گشتم و بنشستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب مرسان به هیچ شادی دستم</p></div>
<div class="m2"><p>گر یک نفس از غم تو خالی هستم</p></div></div>