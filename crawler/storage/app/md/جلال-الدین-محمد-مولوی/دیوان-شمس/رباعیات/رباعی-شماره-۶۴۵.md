---
title: >-
    رباعی شمارهٔ ۶۴۵
---
# رباعی شمارهٔ ۶۴۵

<div class="b" id="bn1"><div class="m1"><p>حاشا که دل از عشق جهانرا نگرد</p></div>
<div class="m2"><p>خود چیست به جز عشق که آنرا نگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیزار شوم ز چشم در روز اجل</p></div>
<div class="m2"><p>گر عشق رها کند که جانرا نگرد</p></div></div>