---
title: >-
    رباعی شمارهٔ ۷۹۸
---
# رباعی شمارهٔ ۷۹۸

<div class="b" id="bn1"><div class="m1"><p>گفتی که بگو زبان چه محرم باشد</p></div>
<div class="m2"><p>محرم نبود هرچه به عالم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله نتوان حدیث آن دم گفتن</p></div>
<div class="m2"><p>با او که سرشت خاک آدم باشد</p></div></div>