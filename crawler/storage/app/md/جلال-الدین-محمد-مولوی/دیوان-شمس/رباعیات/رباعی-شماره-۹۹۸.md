---
title: >-
    رباعی شمارهٔ ۹۹۸
---
# رباعی شمارهٔ ۹۹۸

<div class="b" id="bn1"><div class="m1"><p>امروز حریف عشق بانگی زد فاش</p></div>
<div class="m2"><p>گر اوباشی جز بر اوباش مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی نیست شده است بین میندیش ز لاش</p></div>
<div class="m2"><p>فردا که نیامده است از وی متراش</p></div></div>