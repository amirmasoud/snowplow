---
title: >-
    رباعی شمارهٔ ۱۷۱۷
---
# رباعی شمارهٔ ۱۷۱۷

<div class="b" id="bn1"><div class="m1"><p>ای چون علم سپید در صحرائی</p></div>
<div class="m2"><p>ای رحمت در رسیده از بالائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در هوس تو میپزم حلوائی</p></div>
<div class="m2"><p>حلوا بنگر به صورت سودائی</p></div></div>