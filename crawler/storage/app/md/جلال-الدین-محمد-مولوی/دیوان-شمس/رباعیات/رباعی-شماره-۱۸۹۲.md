---
title: >-
    رباعی شمارهٔ ۱۸۹۲
---
# رباعی شمارهٔ ۱۸۹۲

<div class="b" id="bn1"><div class="m1"><p>سرسبزتر از تو من ندیدم شجری</p></div>
<div class="m2"><p>پرنورتر از تو من ندیدم قمری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبخیزتر از تو من ندیدم سحری</p></div>
<div class="m2"><p>پرذوق‌تر از تو من ندیدم شکری</p></div></div>