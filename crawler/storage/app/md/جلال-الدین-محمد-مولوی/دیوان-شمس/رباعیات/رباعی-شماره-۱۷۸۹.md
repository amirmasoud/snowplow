---
title: >-
    رباعی شمارهٔ ۱۷۸۹
---
# رباعی شمارهٔ ۱۷۸۹

<div class="b" id="bn1"><div class="m1"><p>در باغ درآب با گل اگر خار نه‌ای</p></div>
<div class="m2"><p>پیش آر موافقت گر اغیار نه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زهر مدار روی اگر مار نه‌ای</p></div>
<div class="m2"><p>این نقش بخوان چو نقش دیوار نه‌ای</p></div></div>