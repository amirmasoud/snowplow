---
title: >-
    رباعی شمارهٔ ۱۹۳۲
---
# رباعی شمارهٔ ۱۹۳۲

<div class="b" id="bn1"><div class="m1"><p>گر یک نفسی واقف اسرار شوی</p></div>
<div class="m2"><p>جانبازی را به جان خریدار شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا منست خود تو تا ابد تیره‌ستی</p></div>
<div class="m2"><p>چون مست از او شوی تو هشیار شوی</p></div></div>