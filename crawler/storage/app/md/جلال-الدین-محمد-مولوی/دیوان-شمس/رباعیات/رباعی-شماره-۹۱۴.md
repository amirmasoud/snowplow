---
title: >-
    رباعی شمارهٔ ۹۱۴
---
# رباعی شمارهٔ ۹۱۴

<div class="b" id="bn1"><div class="m1"><p>گوش ما را بی‌دم اسرار مدار</p></div>
<div class="m2"><p>چشم ما را بی‌رخ دلدار مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزم ما را بی‌می خمار مدار</p></div>
<div class="m2"><p>ما را نفسی بیخودت ای یار مدار</p></div></div>