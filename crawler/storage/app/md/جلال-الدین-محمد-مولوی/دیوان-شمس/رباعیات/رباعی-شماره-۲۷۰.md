---
title: >-
    رباعی شمارهٔ ۲۷۰
---
# رباعی شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>توبه کردم که تا جانم برجاست</p></div>
<div class="m2"><p>من کج نروم نگردم از سیرت راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندانکه نظر همی کنم از چپ و راست</p></div>
<div class="m2"><p>جمله چپ و راست و راست و چپ دلبر ماست</p></div></div>