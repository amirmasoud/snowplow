---
title: >-
    رباعی شمارهٔ ۷۶۷
---
# رباعی شمارهٔ ۷۶۷

<div class="b" id="bn1"><div class="m1"><p>عشقی آمد که عشقها سودا شد</p></div>
<div class="m2"><p>سوزیدم و خاکستر من هم لا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز از هوس سوز خاکستر من</p></div>
<div class="m2"><p>واگشت و هزار بار صورتها شد</p></div></div>