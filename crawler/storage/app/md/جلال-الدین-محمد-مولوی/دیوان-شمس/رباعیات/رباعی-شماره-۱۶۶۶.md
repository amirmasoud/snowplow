---
title: >-
    رباعی شمارهٔ ۱۶۶۶
---
# رباعی شمارهٔ ۱۶۶۶

<div class="b" id="bn1"><div class="m1"><p>احوال من زار حزین می‌پرسی</p></div>
<div class="m2"><p>زین پیش مپرس اگر چنین می‌پرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در غم تو دامن دل چاک زدم</p></div>
<div class="m2"><p>وانگاه مرا بستین می‌پرسی</p></div></div>