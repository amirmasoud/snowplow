---
title: >-
    رباعی شمارهٔ ۹۷۹
---
# رباعی شمارهٔ ۹۷۹

<div class="b" id="bn1"><div class="m1"><p>جانا صفت قدم ز ابروت بپرس</p></div>
<div class="m2"><p>آشفتگیم ز زلف هندوت بپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال دلم از دهان تنگت بطلب</p></div>
<div class="m2"><p>بیماری من ز چشم جادوت بپرس</p></div></div>