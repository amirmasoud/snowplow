---
title: >-
    رباعی شمارهٔ ۱۰۵۳
---
# رباعی شمارهٔ ۱۰۵۳

<div class="b" id="bn1"><div class="m1"><p>ای بندهٔ سردی به زمستان چون زاغ</p></div>
<div class="m2"><p>محروم ز بلبل و گلستان ز باغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب که این دم اگرت فوت شود</p></div>
<div class="m2"><p>بسیار طلب کنی به صد چشم و چراغ</p></div></div>