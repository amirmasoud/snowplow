---
title: >-
    رباعی شمارهٔ ۲۵۰
---
# رباعی شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>بگرفت دلت زانکه ترا دل نگرفت</p></div>
<div class="m2"><p>وآنرا که گرفت دل غم گل نگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری دل من جز صفت گل نگرفت</p></div>
<div class="m2"><p>بی‌حاصلیم جز ره حاصل نگرفت</p></div></div>