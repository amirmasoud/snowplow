---
title: >-
    رباعی شمارهٔ ۸۲۲
---
# رباعی شمارهٔ ۸۲۲

<div class="b" id="bn1"><div class="m1"><p>معشوقه چو آفتاب تابان گردد</p></div>
<div class="m2"><p>عاشق به مثال ذره گردان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون باد بهار عشق جنبان گردد</p></div>
<div class="m2"><p>هر شاخ که خشک نیست رقصان گردد</p></div></div>