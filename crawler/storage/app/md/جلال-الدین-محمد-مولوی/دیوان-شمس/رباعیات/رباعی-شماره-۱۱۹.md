---
title: >-
    رباعی شمارهٔ ۱۱۹
---
# رباعی شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>آن پیش روی که جان او پیش صف است</p></div>
<div class="m2"><p>داند که تو بحری و جهان همچو کفست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌دف و نیی، رقص کند عاشق تو</p></div>
<div class="m2"><p>امشب چه کند که هر طرف نای و دفست</p></div></div>