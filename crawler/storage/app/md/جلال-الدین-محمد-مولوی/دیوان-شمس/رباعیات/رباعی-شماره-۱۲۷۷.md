---
title: >-
    رباعی شمارهٔ ۱۲۷۷
---
# رباعی شمارهٔ ۱۲۷۷

<div class="b" id="bn1"><div class="m1"><p>گر جنگ کند به جای چنگش گیرم</p></div>
<div class="m2"><p>ور خوار کنم بنام و ننگش گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی بر من تنگ چرا می‌گیرد</p></div>
<div class="m2"><p>تا چون ببرم آید تنگش گیرم</p></div></div>