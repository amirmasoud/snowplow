---
title: >-
    رباعی شمارهٔ ۷۱۳
---
# رباعی شمارهٔ ۷۱۳

<div class="b" id="bn1"><div class="m1"><p>روز شادیست غم چرا باید خورد</p></div>
<div class="m2"><p>امروز می از جام وفا باید خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند از کف خباز و سفا رزق خوریم</p></div>
<div class="m2"><p>یکچند هم از کف خدا باید خورد</p></div></div>