---
title: >-
    رباعی شمارهٔ ۱۷۳۷
---
# رباعی شمارهٔ ۱۷۳۷

<div class="b" id="bn1"><div class="m1"><p>ای دوست ز من طمع مکن غمخواری</p></div>
<div class="m2"><p>جز مستی و جز شنگی و جز خماری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را چو خدا برای این آوردست</p></div>
<div class="m2"><p>خصم خردیم و دشمن هشیاری</p></div></div>