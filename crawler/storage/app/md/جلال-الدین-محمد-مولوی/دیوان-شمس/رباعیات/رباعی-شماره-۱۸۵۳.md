---
title: >-
    رباعی شمارهٔ ۱۸۵۳
---
# رباعی شمارهٔ ۱۸۵۳

<div class="b" id="bn1"><div class="m1"><p>در بی‌خبری خبر نبودی چه بدی</p></div>
<div class="m2"><p>و اندیشهٔ خیر و شر نبودی چه بدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای هوش تو و گوش من و حلقهٔ در</p></div>
<div class="m2"><p>گر حلقهٔ سیم و زر نبودی چه بدی</p></div></div>