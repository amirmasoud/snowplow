---
title: >-
    رباعی شمارهٔ ۱۰۵۴
---
# رباعی شمارهٔ ۱۰۵۴

<div class="b" id="bn1"><div class="m1"><p>بلبل آمد به باغ و رستیم ز زاغ</p></div>
<div class="m2"><p>آئیم به باغ با تو ای چشم و چراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سوسن و گل ز خویش بیرون آئیم</p></div>
<div class="m2"><p>چون آب روان رویم از باغ به باغ</p></div></div>