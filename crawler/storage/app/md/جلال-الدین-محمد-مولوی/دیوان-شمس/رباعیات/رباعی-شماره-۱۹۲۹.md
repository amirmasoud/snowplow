---
title: >-
    رباعی شمارهٔ ۱۹۲۹
---
# رباعی شمارهٔ ۱۹۲۹

<div class="b" id="bn1"><div class="m1"><p>گرنه حذر از غیرت مردان کنمی</p></div>
<div class="m2"><p>آن کار که دوش گفته‌ام آن کنمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور رشک نبودی همه هشیاران را</p></div>
<div class="m2"><p>بی‌خویش و خراب و مست و حیران کنمی</p></div></div>