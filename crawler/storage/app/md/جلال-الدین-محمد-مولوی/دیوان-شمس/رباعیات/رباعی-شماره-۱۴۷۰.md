---
title: >-
    رباعی شمارهٔ ۱۴۷۰
---
# رباعی شمارهٔ ۱۴۷۰

<div class="b" id="bn1"><div class="m1"><p>دل برد ز من دوش به صد عشق و فسون</p></div>
<div class="m2"><p>بشکافت و بدید پر زخون بود درون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرمود در آتشش نهادن حالی</p></div>
<div class="m2"><p>یعنی که نپخته است از آنست پر خون</p></div></div>