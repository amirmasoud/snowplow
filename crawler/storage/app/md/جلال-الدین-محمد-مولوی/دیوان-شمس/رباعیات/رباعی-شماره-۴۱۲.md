---
title: >-
    رباعی شمارهٔ ۴۱۲
---
# رباعی شمارهٔ ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>مست است دو چشم از دو چشم مستت</p></div>
<div class="m2"><p>دریاب که از دست شدم در دستت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو هم به موافقت سری در جنبان</p></div>
<div class="m2"><p>گر زانکه سر عاشق هستی هستت</p></div></div>