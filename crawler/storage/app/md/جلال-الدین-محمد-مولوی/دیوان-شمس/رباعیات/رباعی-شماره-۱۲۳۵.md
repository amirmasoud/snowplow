---
title: >-
    رباعی شمارهٔ ۱۲۳۵
---
# رباعی شمارهٔ ۱۲۳۵

<div class="b" id="bn1"><div class="m1"><p>دوش ارچه هزار نام بر ننگ زدم</p></div>
<div class="m2"><p>بر دامن آن عهد شکن چنگ زدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر دل او نهادم از شوق وصال</p></div>
<div class="m2"><p>هم عاقبت آبگینه بر سنگ زدم</p></div></div>