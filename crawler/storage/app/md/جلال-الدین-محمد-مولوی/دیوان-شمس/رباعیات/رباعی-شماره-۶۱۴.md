---
title: >-
    رباعی شمارهٔ ۶۱۴
---
# رباعی شمارهٔ ۶۱۴

<div class="b" id="bn1"><div class="m1"><p>تو جانی و هر زنده غم جان بکشد</p></div>
<div class="m2"><p>هر کان دارد منت آن بکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجان که چو کارد با تو در بند زر است</p></div>
<div class="m2"><p>گر تیغ زنی از بن دندان بکشد</p></div></div>