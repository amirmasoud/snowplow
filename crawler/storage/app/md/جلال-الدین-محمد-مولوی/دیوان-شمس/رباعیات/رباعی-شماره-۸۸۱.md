---
title: >-
    رباعی شمارهٔ ۸۸۱
---
# رباعی شمارهٔ ۸۸۱

<div class="b" id="bn1"><div class="m1"><p>ای مرد سماع معده را خالی دار</p></div>
<div class="m2"><p>زیرا چو تهیست نی کند نالهٔ زار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پر کردی شکم ز لوت بسیار</p></div>
<div class="m2"><p>خالی مانی ز دلبر و بوس و کنار</p></div></div>