---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>ای دوست به دوستی قرینیم ترا</p></div>
<div class="m2"><p>هرجا که قدم نهی زمینیم ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مذهب عاشقی روا کی باشد</p></div>
<div class="m2"><p>عالم تو ببینیم و نه بینیم ترا</p></div></div>