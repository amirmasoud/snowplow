---
title: >-
    رباعی شمارهٔ ۱۸۰۱
---
# رباعی شمارهٔ ۱۸۰۱

<div class="b" id="bn1"><div class="m1"><p>بر گلشن یارم گذرت بایستی</p></div>
<div class="m2"><p>بر چهرهٔ او یک نظرت بایستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بی‌خبری گوی ز میدان بردی</p></div>
<div class="m2"><p>از بی‌خبریها خبرت بایستی</p></div></div>