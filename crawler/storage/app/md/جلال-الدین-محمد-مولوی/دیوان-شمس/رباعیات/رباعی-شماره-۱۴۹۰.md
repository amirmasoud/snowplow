---
title: >-
    رباعی شمارهٔ ۱۴۹۰
---
# رباعی شمارهٔ ۱۴۹۰

<div class="b" id="bn1"><div class="m1"><p>صورت همه مقبول هیولا میدان</p></div>
<div class="m2"><p>تصویر گرش علت اولی میدان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاهوت به ناسوت فرو ناید لیک</p></div>
<div class="m2"><p>ناوست ز لاهوت هویدا میدان</p></div></div>