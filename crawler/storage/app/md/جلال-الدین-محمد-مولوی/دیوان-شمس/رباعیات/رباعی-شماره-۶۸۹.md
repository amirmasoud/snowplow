---
title: >-
    رباعی شمارهٔ ۶۸۹
---
# رباعی شمارهٔ ۶۸۹

<div class="b" id="bn1"><div class="m1"><p>دست تو به جود طعنه بر میغ زند</p></div>
<div class="m2"><p>در معرکه تیغ گوهر آمیغ زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کار تو آفتاب را شرمی باد</p></div>
<div class="m2"><p>کو تیغ تو دیده صبحدم تیغ زند</p></div></div>