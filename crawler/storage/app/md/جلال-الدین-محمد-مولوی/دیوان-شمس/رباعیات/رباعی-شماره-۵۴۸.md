---
title: >-
    رباعی شمارهٔ ۵۴۸
---
# رباعی شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>ای اهل صفا که در جهان گردانید</p></div>
<div class="m2"><p>از بهر بتی چرا چنین حیرانید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنرا که شما در این جهان جویانید</p></div>
<div class="m2"><p>در خود چو جوئید شما خود آنید</p></div></div>