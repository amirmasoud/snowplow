---
title: >-
    رباعی شمارهٔ ۱۹۶۱
---
# رباعی شمارهٔ ۱۹۶۱

<div class="b" id="bn1"><div class="m1"><p>من جمله خطا کنم صوابم تو بسی</p></div>
<div class="m2"><p>مقصود از این عمر خرابم تو بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من میدانم که چون بخواهم رفتن</p></div>
<div class="m2"><p>پرسند چه کرده‌ای جوابم تو بسی</p></div></div>