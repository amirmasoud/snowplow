---
title: >-
    رباعی شمارهٔ ۳۸۸
---
# رباعی شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>گفتم عشقت قرابت و خویش منست</p></div>
<div class="m2"><p>غم نیست غم از دل بداندیش منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا بکمان و تیر خود می‌نازی</p></div>
<div class="m2"><p>گستاخ مینداز گرو پیش منست</p></div></div>