---
title: >-
    رباعی شمارهٔ ۱۲۳۶
---
# رباعی شمارهٔ ۱۲۳۶

<div class="b" id="bn1"><div class="m1"><p>دل داد مرا که دلستان را بزدم</p></div>
<div class="m2"><p>آن را که نواختم همان را بزدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانی که بدو زنده‌ام و خندانم</p></div>
<div class="m2"><p>دیوانه شدم چنانکه جان را بزدم</p></div></div>