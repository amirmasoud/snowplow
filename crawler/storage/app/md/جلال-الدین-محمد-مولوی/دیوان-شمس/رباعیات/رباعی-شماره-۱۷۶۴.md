---
title: >-
    رباعی شمارهٔ ۱۷۶۴
---
# رباعی شمارهٔ ۱۷۶۴

<div class="b" id="bn1"><div class="m1"><p>ای یار گرفتهٔ شراب آمیزی</p></div>
<div class="m2"><p>برخیزد رستخیز چون برخیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌ریز شراب را که خوش می‌ریزی</p></div>
<div class="m2"><p>چون خویش چنین شدی چرا بگریزی</p></div></div>