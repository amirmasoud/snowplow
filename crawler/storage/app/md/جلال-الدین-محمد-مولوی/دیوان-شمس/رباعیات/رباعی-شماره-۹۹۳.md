---
title: >-
    رباعی شمارهٔ ۹۹۳
---
# رباعی شمارهٔ ۹۹۳

<div class="b" id="bn1"><div class="m1"><p>آندیده که هست عاشق گلزارش</p></div>
<div class="m2"><p>مشغول کجا کند سر هر خارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر راست بود یار دهد پرگارش</p></div>
<div class="m2"><p>ور کژ نگردد راست نیاید کارش</p></div></div>