---
title: >-
    رباعی شمارهٔ ۱۵۵۷
---
# رباعی شمارهٔ ۱۵۵۷

<div class="b" id="bn1"><div class="m1"><p>با نامحرم حدیث اسرار مگو</p></div>
<div class="m2"><p>با مردودان حکایت از یار مگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مردم اغیار جز اغیار مگو</p></div>
<div class="m2"><p>با اشتر خار خوار جز خار مگو</p></div></div>