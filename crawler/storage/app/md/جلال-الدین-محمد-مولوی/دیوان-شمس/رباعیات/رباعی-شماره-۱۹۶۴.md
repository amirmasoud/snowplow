---
title: >-
    رباعی شمارهٔ ۱۹۶۴
---
# رباعی شمارهٔ ۱۹۶۴

<div class="b" id="bn1"><div class="m1"><p>من دوش به کاسهٔ رباب سحری</p></div>
<div class="m2"><p>می‌نالیدم ترانهٔ کاسه‌گری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کاسهٔ می درآمد آن رشک پری</p></div>
<div class="m2"><p>گفتا که اگر کاسه زنی کوزه خوری</p></div></div>