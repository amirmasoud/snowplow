---
title: >-
    رباعی شمارهٔ ۱۳۶۷
---
# رباعی شمارهٔ ۱۳۶۷

<div class="b" id="bn1"><div class="m1"><p>هم مستم و هم بادهٔ مستان توام</p></div>
<div class="m2"><p>هم آفت جان زیر دستان توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نیست شدم کنون ز هستان توام</p></div>
<div class="m2"><p>گفتی که الست از الست آن توام</p></div></div>