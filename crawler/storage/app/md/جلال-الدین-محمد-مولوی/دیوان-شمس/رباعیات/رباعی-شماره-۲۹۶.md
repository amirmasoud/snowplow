---
title: >-
    رباعی شمارهٔ ۲۹۶
---
# رباعی شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>خورشید رخت ز آسمان بیرونست</p></div>
<div class="m2"><p>چون حسن تو کز شرح و بیان بیرونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو در درون جان من جا دارد</p></div>
<div class="m2"><p>وین طرفه که از جان و جهان بیرونست</p></div></div>