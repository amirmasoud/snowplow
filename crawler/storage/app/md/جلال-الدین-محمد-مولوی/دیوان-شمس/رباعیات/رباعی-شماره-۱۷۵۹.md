---
title: >-
    رباعی شمارهٔ ۱۷۵۹
---
# رباعی شمارهٔ ۱۷۵۹

<div class="b" id="bn1"><div class="m1"><p>ای نسخهٔ نامهٔ الهی که توئی</p></div>
<div class="m2"><p>وی آینهٔ جمال شاهی که توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون ز تو نیست هرچه در عالم هست</p></div>
<div class="m2"><p>در خود بطلب هر آنچه خواهی که توئی</p></div></div>