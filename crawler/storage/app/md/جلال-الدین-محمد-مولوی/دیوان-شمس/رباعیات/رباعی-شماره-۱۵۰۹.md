---
title: >-
    رباعی شمارهٔ ۱۵۰۹
---
# رباعی شمارهٔ ۱۵۰۹

<div class="b" id="bn1"><div class="m1"><p>معشوق من از همه نهانست بدان</p></div>
<div class="m2"><p>بیرون ز کمان هر گمانست بدان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینهٔ من چو مه عیانست بدان</p></div>
<div class="m2"><p>آمیخته با تنم چو جانست بدان</p></div></div>