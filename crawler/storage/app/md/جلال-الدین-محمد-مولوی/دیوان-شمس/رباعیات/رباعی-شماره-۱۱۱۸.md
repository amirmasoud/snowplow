---
title: >-
    رباعی شمارهٔ ۱۱۱۸
---
# رباعی شمارهٔ ۱۱۱۸

<div class="b" id="bn1"><div class="m1"><p>آواز سرافیل طرب میرسدم</p></div>
<div class="m2"><p>از خاک فنا بر آسمان می‌بردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس را خبری نیست که بر من چه رسید</p></div>
<div class="m2"><p>زان با خبری که بی‌خبر می‌رسدم</p></div></div>