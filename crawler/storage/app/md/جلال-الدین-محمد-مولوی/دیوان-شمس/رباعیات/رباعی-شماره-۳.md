---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>آن کس که ترا نقش کند او تنها</p></div>
<div class="m2"><p>تنها نگذاردت میان سودا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خانه تصویر تو یعنی دل تو</p></div>
<div class="m2"><p>بر رویاند دو صد حریف زیبا</p></div></div>