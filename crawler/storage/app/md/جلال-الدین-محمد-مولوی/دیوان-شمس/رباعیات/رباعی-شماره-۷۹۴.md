---
title: >-
    رباعی شمارهٔ ۷۹۴
---
# رباعی شمارهٔ ۷۹۴

<div class="b" id="bn1"><div class="m1"><p>گفتم بیتی نگار از من رنجید</p></div>
<div class="m2"><p>یعنی که بوزن بیت ما را سنجید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چه ویران کنی این بیت مرا</p></div>
<div class="m2"><p>گفتا به کدام بیت خواهم گنجید</p></div></div>