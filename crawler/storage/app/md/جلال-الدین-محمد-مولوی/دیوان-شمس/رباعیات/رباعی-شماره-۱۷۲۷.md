---
title: >-
    رباعی شمارهٔ ۱۷۲۷
---
# رباعی شمارهٔ ۱۷۲۷

<div class="b" id="bn1"><div class="m1"><p>ای دل تو اگر هزار دلبر داری</p></div>
<div class="m2"><p>شرط آن نبود که دل ز ما برداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دل داری که دل ز ما برداری</p></div>
<div class="m2"><p>از یار نوت مباد برخورداری</p></div></div>