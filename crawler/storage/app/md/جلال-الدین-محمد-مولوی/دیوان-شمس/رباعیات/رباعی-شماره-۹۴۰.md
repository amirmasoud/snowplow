---
title: >-
    رباعی شمارهٔ ۹۴۰
---
# رباعی شمارهٔ ۹۴۰

<div class="b" id="bn1"><div class="m1"><p>ای کرده ز نقش آدمی چنگی ساز</p></div>
<div class="m2"><p>جانها همه اقوال تو از روی نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای لعل لبت توانگری عمر دراز</p></div>
<div class="m2"><p>یک هدیه از آن لعل به قوال انداز</p></div></div>