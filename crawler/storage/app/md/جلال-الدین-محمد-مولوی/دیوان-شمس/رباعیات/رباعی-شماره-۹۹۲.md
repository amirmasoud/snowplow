---
title: >-
    رباعی شمارهٔ ۹۹۲
---
# رباعی شمارهٔ ۹۹۲

<div class="b" id="bn1"><div class="m1"><p>آن دم که حق بنده‌گزاری همه خوش</p></div>
<div class="m2"><p>وز مهر سر بنده بخاری همه خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خانه برانیم بزاری همه خوش</p></div>
<div class="m2"><p>چون عزم کنم هم بگذاری همه خوش</p></div></div>