---
title: >-
    رباعی شمارهٔ ۵۱۰
---
# رباعی شمارهٔ ۵۱۰

<div class="b" id="bn1"><div class="m1"><p>از آب حیات دوست بیمار نماند</p></div>
<div class="m2"><p>در گلبن وصل دوست یک خار نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند درچه‌ایست از دل سوی دل</p></div>
<div class="m2"><p>چه جای دریچه‌ای که دیوار نماند</p></div></div>