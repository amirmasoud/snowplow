---
title: >-
    رباعی شمارهٔ ۵۹۳
---
# رباعی شمارهٔ ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>بیدار شو ای دل که جهان می‌گذرد</p></div>
<div class="m2"><p>وین مایهٔ عمر رایگان می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در منزل تن مخسب و غافل منشین</p></div>
<div class="m2"><p>کز منزل عمر کاروان می‌گذرد</p></div></div>