---
title: >-
    رباعی شمارهٔ ۱۷۱۱
---
# رباعی شمارهٔ ۱۷۱۱

<div class="b" id="bn1"><div class="m1"><p>ای پر ز جفا چند از این طراری</p></div>
<div class="m2"><p>پنهان چه کنی آنچه به باطن داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر ز خط وفای من برداری</p></div>
<div class="m2"><p>واقف نیم از ضمیر دل پنداری</p></div></div>