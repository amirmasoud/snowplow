---
title: >-
    رباعی شمارهٔ ۹۶۲
---
# رباعی شمارهٔ ۹۶۲

<div class="b" id="bn1"><div class="m1"><p>مائیم و دمی کوته و سودای دراز</p></div>
<div class="m2"><p>در سایهٔ دل فکنده دو پای دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظاره‌کنان بسوی صحرای دراز</p></div>
<div class="m2"><p>صد روز قیامت است چه جای دراز</p></div></div>