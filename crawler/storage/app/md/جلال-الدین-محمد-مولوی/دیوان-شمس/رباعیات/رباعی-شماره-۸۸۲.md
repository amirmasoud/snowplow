---
title: >-
    رباعی شمارهٔ ۸۸۲
---
# رباعی شمارهٔ ۸۸۲

<div class="b" id="bn1"><div class="m1"><p>این صورت باغست و در او نیست ثمر</p></div>
<div class="m2"><p>تو رنجه مشو بیهده سوگند مخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا کار معلق و فریبست و غرر</p></div>
<div class="m2"><p>خود از تو نجست کس از این جنس خبر</p></div></div>