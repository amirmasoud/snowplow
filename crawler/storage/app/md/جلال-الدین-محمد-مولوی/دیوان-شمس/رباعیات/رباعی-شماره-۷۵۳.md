---
title: >-
    رباعی شمارهٔ ۷۵۳
---
# رباعی شمارهٔ ۷۵۳

<div class="b" id="bn1"><div class="m1"><p>صد مرحله زانسوی خرد خواهم شد</p></div>
<div class="m2"><p>فارغ ز وجود نیک و بد خواهم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس خوبی که در پس پرده منم</p></div>
<div class="m2"><p>ای بیخبران عاشق خود خواهم شد</p></div></div>