---
title: >-
    رباعی شمارهٔ ۱۲۱۰
---
# رباعی شمارهٔ ۱۲۱۰

<div class="b" id="bn1"><div class="m1"><p>چون می‌دانی که از نکوئی دورم</p></div>
<div class="m2"><p>گر بگریزم ز نیکوان معذورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او همچو عصا کش است و من نابینا</p></div>
<div class="m2"><p>من گام به خود نمیزنم مأمورم</p></div></div>