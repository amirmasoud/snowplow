---
title: >-
    رباعی شمارهٔ ۱۶۹
---
# رباعی شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>امشب شب من بسی ضعیف و زار است</p></div>
<div class="m2"><p>امشب شب پرداختن اسرار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسرار دلم جمله خیال یار است</p></div>
<div class="m2"><p>ای شب بگذر زود که ما را کار است</p></div></div>