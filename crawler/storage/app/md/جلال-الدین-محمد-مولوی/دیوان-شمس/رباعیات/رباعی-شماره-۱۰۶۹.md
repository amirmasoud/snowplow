---
title: >-
    رباعی شمارهٔ ۱۰۶۹
---
# رباعی شمارهٔ ۱۰۶۹

<div class="b" id="bn1"><div class="m1"><p>لو کان اقل هذه الاشواق</p></div>
<div class="m2"><p>للشمس لا ذهلت عن الاشراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لو قسم ذوالهوی علی‌العشاق</p></div>
<div class="m2"><p>العشر لهم ولی جمیع‌الباقی</p></div></div>