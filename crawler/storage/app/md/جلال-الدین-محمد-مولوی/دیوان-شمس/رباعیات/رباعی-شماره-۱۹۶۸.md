---
title: >-
    رباعی شمارهٔ ۱۹۶۸
---
# رباعی شمارهٔ ۱۹۶۸

<div class="b" id="bn1"><div class="m1"><p>مهمان دو دیده شد خیالت گذری</p></div>
<div class="m2"><p>در دیده وطن ساخت ز نیکو گهری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی خیال شد دو دیده میگفت</p></div>
<div class="m2"><p>مهمان منی به آب چندان که خوری</p></div></div>