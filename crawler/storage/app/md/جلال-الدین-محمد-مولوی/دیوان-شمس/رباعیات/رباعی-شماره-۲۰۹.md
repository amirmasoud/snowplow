---
title: >-
    رباعی شمارهٔ ۲۰۹
---
# رباعی شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>این عشق شهست و رایتش پیدا نیست</p></div>
<div class="m2"><p>قرآن حقست و آیتش پیدا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر عاشق از این صیاد تیری خورده است</p></div>
<div class="m2"><p>خون میرود و جراحتش پیدا نیست</p></div></div>