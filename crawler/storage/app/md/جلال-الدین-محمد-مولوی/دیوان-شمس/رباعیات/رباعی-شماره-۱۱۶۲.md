---
title: >-
    رباعی شمارهٔ ۱۱۶۲
---
# رباعی شمارهٔ ۱۱۶۲

<div class="b" id="bn1"><div class="m1"><p>ای راحت و آرامگه پیوستم</p></div>
<div class="m2"><p>تا روی تو دیدم ز حوادث رستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مجلس تو گر قدحی بشکستم</p></div>
<div class="m2"><p>صد ساغر زرین بخرم بفرستم</p></div></div>