---
title: >-
    رباعی شمارهٔ ۷۲۶
---
# رباعی شمارهٔ ۷۲۶

<div class="b" id="bn1"><div class="m1"><p>زلف تو به حسن ذوفنون‌ها برزد</p></div>
<div class="m2"><p>در مالش عنبر آستین‌ها برزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکش گفتم از این سخن تاب آورد</p></div>
<div class="m2"><p>در هم شد و خویشتن زمین‌ها برزد</p></div></div>