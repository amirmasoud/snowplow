---
title: >-
    رباعی شمارهٔ ۳۱۹
---
# رباعی شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>درویشی و عاشقی به هم سلطانیست</p></div>
<div class="m2"><p>گنجست غم عشق ولی پنهانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ویران کردم بدست خود خانهٔ دل</p></div>
<div class="m2"><p>چون دانستم که گنج در ویرانیست</p></div></div>