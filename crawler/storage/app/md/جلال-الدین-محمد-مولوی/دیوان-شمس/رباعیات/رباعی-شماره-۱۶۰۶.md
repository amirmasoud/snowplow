---
title: >-
    رباعی شمارهٔ ۱۶۰۶
---
# رباعی شمارهٔ ۱۶۰۶

<div class="b" id="bn1"><div class="m1"><p>ای خواب مرا بسته و مدفون کرده</p></div>
<div class="m2"><p>شب را و مرا بی‌خود و مجنون کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان را به فسون گرم از تن برده</p></div>
<div class="m2"><p>دل را بسته ز خانه بیرون کرده</p></div></div>