---
title: >-
    رباعی شمارهٔ ۱۴۸
---
# رباعی شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>آن نور مبین که در جبین ما هست</p></div>
<div class="m2"><p>وان ض یقین که در دل آگاهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جملهٔ نور بلکه نور همه نور</p></div>
<div class="m2"><p>از نور محمد رسول‌الله است</p></div></div>