---
title: >-
    رباعی شمارهٔ ۴۲۹
---
# رباعی شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>نی با تو دمی نشستنم سامانست</p></div>
<div class="m2"><p>نی بیتو دمی زیستنم امکانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشه در این واقعه سرگردانست</p></div>
<div class="m2"><p>این واقعه نیست درد بیدرمانست</p></div></div>