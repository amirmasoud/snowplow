---
title: >-
    رباعی شمارهٔ ۱۴۴۳
---
# رباعی شمارهٔ ۱۴۴۳

<div class="b" id="bn1"><div class="m1"><p>پیموده شدم ز راه تو پیمودن</p></div>
<div class="m2"><p>فرسوده شدم ز عشق تو فرسودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی روز بخوردن و نه شب بغنودن</p></div>
<div class="m2"><p>ای دوستی تو دشمن خود بودن</p></div></div>