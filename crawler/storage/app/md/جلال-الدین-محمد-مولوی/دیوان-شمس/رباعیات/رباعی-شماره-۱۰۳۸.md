---
title: >-
    رباعی شمارهٔ ۱۰۳۸
---
# رباعی شمارهٔ ۱۰۳۸

<div class="b" id="bn1"><div class="m1"><p>دل یاد تو آرد برود هوش ز هوش</p></div>
<div class="m2"><p>می بی‌لب نوشین تو کی گردد نوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدار ترا چشم همی دارد چشم</p></div>
<div class="m2"><p>آواز ترا گوش همی دارد گوش</p></div></div>