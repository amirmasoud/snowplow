---
title: >-
    رباعی شمارهٔ ۹۹۰
---
# رباعی شمارهٔ ۹۹۰

<div class="b" id="bn1"><div class="m1"><p>آتش در زن بگیر پا در کویش</p></div>
<div class="m2"><p>تازه نبرد هیچ فضول سویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن‌روی چو ماه را بپوش از مویش</p></div>
<div class="m2"><p>تا دیدهٔ هر خسی نبیند رویش</p></div></div>