---
title: >-
    رباعی شمارهٔ ۱۰۶۳
---
# رباعی شمارهٔ ۱۰۶۳

<div class="b" id="bn1"><div class="m1"><p>گویند مرا چند بخندی ز گزاف</p></div>
<div class="m2"><p>کارت همه عشرتست و گفتت همه لاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خصم چو عنکبوت صفرا میباف</p></div>
<div class="m2"><p>سیمرغ طربناک شناسد سر قاف</p></div></div>