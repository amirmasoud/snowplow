---
title: >-
    رباعی شمارهٔ ۷۷۱
---
# رباعی شمارهٔ ۷۷۱

<div class="b" id="bn1"><div class="m1"><p>غم را بر او گزیده میباید کرد</p></div>
<div class="m2"><p>وز چاه طمع بریده میباید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون دل من ریخته میخواهد یار</p></div>
<div class="m2"><p>این کار مرا به دیده میباید کرد</p></div></div>