---
title: >-
    رباعی شمارهٔ ۱۲۳۷
---
# رباعی شمارهٔ ۱۲۳۷

<div class="b" id="bn1"><div class="m1"><p>دیوانه‌ام نیم ولیک همی خوانندم</p></div>
<div class="m2"><p>بیگانه‌ام ولیک میرانندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون عسسان بجهد در نیمهٔ شب</p></div>
<div class="m2"><p>مستند ولی چو روز میدانندم</p></div></div>