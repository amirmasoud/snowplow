---
title: >-
    رباعی شمارهٔ ۹
---
# رباعی شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>از حال ندیده تیره ایامان را</p></div>
<div class="m2"><p>از دور ندیده دوزخ آشامان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی چکنی عشق دلارامان را</p></div>
<div class="m2"><p>با عشق چکار است نکونامان را</p></div></div>