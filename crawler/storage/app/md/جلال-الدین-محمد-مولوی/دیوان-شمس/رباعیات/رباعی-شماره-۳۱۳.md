---
title: >-
    رباعی شمارهٔ ۳۱۳
---
# رباعی شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>در مجلس عشاق قراری دگر است</p></div>
<div class="m2"><p>وین بادهٔ عشق را خماری دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن علم که در مدرسه حاصل کردند</p></div>
<div class="m2"><p>کار دگر است و عشق کاری دگر است</p></div></div>