---
title: >-
    رباعی شمارهٔ ۱۵۷۶
---
# رباعی شمارهٔ ۱۵۷۶

<div class="b" id="bn1"><div class="m1"><p>فرزانهٔ عشق را تو دیوانه مگو</p></div>
<div class="m2"><p>همخرقهٔ روح را بیگانه مگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریای محیط را تو پیمانه مگو</p></div>
<div class="m2"><p>او داند نام خود تو افسانه مگو</p></div></div>