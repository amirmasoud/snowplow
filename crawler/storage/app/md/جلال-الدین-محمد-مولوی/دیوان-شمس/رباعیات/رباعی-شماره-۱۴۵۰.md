---
title: >-
    رباعی شمارهٔ ۱۴۵۰
---
# رباعی شمارهٔ ۱۴۵۰

<div class="b" id="bn1"><div class="m1"><p>جز بادهٔ لعل لامکان یاد مکن</p></div>
<div class="m2"><p>آنرا بنگر از این و آن یاد مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جان داری از این جهان یاد مکن</p></div>
<div class="m2"><p>مستی خواهی ز عاقلان یاد مکن</p></div></div>