---
title: >-
    رباعی شمارهٔ ۷۷
---
# رباعی شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>یک طرفه عصاست موسی این رمه را</p></div>
<div class="m2"><p>یک لقمه کند چو بفکند این همه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی سور گذارد او و نی ملحمه را</p></div>
<div class="m2"><p>هر عقل نکرد فهم این زمزمه را</p></div></div>