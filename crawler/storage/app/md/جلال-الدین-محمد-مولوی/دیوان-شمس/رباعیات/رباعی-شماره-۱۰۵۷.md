---
title: >-
    رباعی شمارهٔ ۱۰۵۷
---
# رباعی شمارهٔ ۱۰۵۷

<div class="b" id="bn1"><div class="m1"><p>گویند که عشق بانگ و نامست دروغ</p></div>
<div class="m2"><p>گویند امید عشق خامست دروغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیوان سعادت بر ما در جانست</p></div>
<div class="m2"><p>گویند فراز هفت بامست دروغ</p></div></div>