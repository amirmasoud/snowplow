---
title: >-
    رباعی شمارهٔ ۱۳۰۷
---
# رباعی شمارهٔ ۱۳۰۷

<div class="b" id="bn1"><div class="m1"><p>ما برزگران این کهن دشت نویم</p></div>
<div class="m2"><p>در کشتهٔ شادی همه غم میدرویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون لالهٔ کم عمر در این دشت فنا</p></div>
<div class="m2"><p>تا سر زده از خاک ببادی گرویم</p></div></div>