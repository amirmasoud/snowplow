---
title: >-
    رباعی شمارهٔ ۳۹
---
# رباعی شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>تا نقش خیال دوست با ماست دلا</p></div>
<div class="m2"><p>ما را هم عمر خود تماشاست دلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانجا که مراد دل برآرید ای دل</p></div>
<div class="m2"><p>یک خار به از هزار خرماست دلا</p></div></div>