---
title: >-
    رباعی شمارهٔ ۱۴۲۳
---
# رباعی شمارهٔ ۱۴۲۳

<div class="b" id="bn1"><div class="m1"><p>ای ماه لطیف جانفزا خرمن من</p></div>
<div class="m2"><p>وی ماه فرو کرده سر از روزن من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گلشن جان و دیدهٔ روشن من</p></div>
<div class="m2"><p>کی بینمت آویخته بر گردن من</p></div></div>