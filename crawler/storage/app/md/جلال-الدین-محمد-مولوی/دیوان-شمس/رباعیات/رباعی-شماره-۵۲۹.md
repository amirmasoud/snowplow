---
title: >-
    رباعی شمارهٔ ۵۲۹
---
# رباعی شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>افسوس که طبع دلفروزیت نبود</p></div>
<div class="m2"><p>جز دلشکنی و سینه سوزیت نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادم به تو من همه دل و دیده و جان</p></div>
<div class="m2"><p>بردی تو همه ولیک روزیت نبود</p></div></div>