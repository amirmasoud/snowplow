---
title: >-
    رباعی شمارهٔ ۸۴۴
---
# رباعی شمارهٔ ۸۴۴

<div class="b" id="bn1"><div class="m1"><p>هر شب که دل سپهر گلشن گردد</p></div>
<div class="m2"><p>عالم همه ساکن چو دل من گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد آه برآورم ز آیینهٔ دل</p></div>
<div class="m2"><p>آیینهٔ دل ز آه روشن گردد</p></div></div>