---
title: >-
    رباعی شمارهٔ ۸۵۹
---
# رباعی شمارهٔ ۸۵۹

<div class="b" id="bn1"><div class="m1"><p>یاد تو کنم دلم تپیدن گیرد</p></div>
<div class="m2"><p>خونابه ز دیده‌ام چکیدن گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا خبر دوست رسیدن گیرد</p></div>
<div class="m2"><p>بیچاره دلم ز خود رمیدن گیرد</p></div></div>