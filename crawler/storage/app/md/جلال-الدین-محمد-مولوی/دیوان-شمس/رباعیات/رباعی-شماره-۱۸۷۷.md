---
title: >-
    رباعی شمارهٔ ۱۸۷۷
---
# رباعی شمارهٔ ۱۸۷۷

<div class="b" id="bn1"><div class="m1"><p>دوش از سر عاشقی و از مشتاقی</p></div>
<div class="m2"><p>می‌کردم التماس می از ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جاه و جمال خویش بنمود به من</p></div>
<div class="m2"><p>من نیست شدم بماند ساقی ساقی</p></div></div>