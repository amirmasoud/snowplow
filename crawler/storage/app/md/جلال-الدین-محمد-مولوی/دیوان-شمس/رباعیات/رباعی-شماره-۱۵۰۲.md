---
title: >-
    رباعی شمارهٔ ۱۵۰۲
---
# رباعی شمارهٔ ۱۵۰۲

<div class="b" id="bn1"><div class="m1"><p>گفتم که بر حریف غمگین منشین</p></div>
<div class="m2"><p>جز پهلوی خوشدلان شیرین منشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ چو آمدی سوی خار مرو</p></div>
<div class="m2"><p>جز با گل و یاسمین و نسرین منشین</p></div></div>