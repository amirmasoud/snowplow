---
title: >-
    رباعی شمارهٔ ۱۹۳۵
---
# رباعی شمارهٔ ۱۹۳۵

<div class="b" id="bn1"><div class="m1"><p>گفتم صنما مگر که جانان منی</p></div>
<div class="m2"><p>اکنون که همی نظر کنم جان منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرتد گردم گر ز تو من برگردی</p></div>
<div class="m2"><p>ای جان جهان تو کفر و ایمان منی</p></div></div>