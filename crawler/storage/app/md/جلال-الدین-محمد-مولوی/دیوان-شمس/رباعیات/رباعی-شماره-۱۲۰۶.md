---
title: >-
    رباعی شمارهٔ ۱۲۰۶
---
# رباعی شمارهٔ ۱۲۰۶

<div class="b" id="bn1"><div class="m1"><p>جانی که در او دو صد جهان میدانم</p></div>
<div class="m2"><p>گوئیکه فلانست و فلان میدانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او شاهد حضرتست و حق نیک غیور</p></div>
<div class="m2"><p>هر چشم که بسته گشت از آن میدانم</p></div></div>