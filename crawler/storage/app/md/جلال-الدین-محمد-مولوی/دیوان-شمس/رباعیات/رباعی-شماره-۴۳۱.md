---
title: >-
    رباعی شمارهٔ ۴۳۱
---
# رباعی شمارهٔ ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>هان ای دل خسته روز مردانگیست</p></div>
<div class="m2"><p>در عشق توم چه جای بیگانگیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز که در تصرف عقل آید</p></div>
<div class="m2"><p>بگذار کنون که وقت دیوانگیست</p></div></div>