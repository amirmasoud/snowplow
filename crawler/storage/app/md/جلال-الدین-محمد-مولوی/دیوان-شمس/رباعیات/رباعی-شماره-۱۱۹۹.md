---
title: >-
    رباعی شمارهٔ ۱۱۹۹
---
# رباعی شمارهٔ ۱۱۹۹

<div class="b" id="bn1"><div class="m1"><p>تا ظن نبری که من دوئی می‌بینم</p></div>
<div class="m2"><p>هر لحظه فتوحی بنوی می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل من جمله توئی می‌دانم</p></div>
<div class="m2"><p>چشم و سر من جمله توئی می‌بینم</p></div></div>