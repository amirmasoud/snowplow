---
title: >-
    رباعی شمارهٔ ۱۱۲۹
---
# رباعی شمارهٔ ۱۱۲۹

<div class="b" id="bn1"><div class="m1"><p>از خویش خوشم نی نباشد خوشیم</p></div>
<div class="m2"><p>از خود گرمم نه آب و نی آتشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان سبکم به عشق کاندر میزان</p></div>
<div class="m2"><p>از هیچ کم آیم دو من ار برکشیم</p></div></div>