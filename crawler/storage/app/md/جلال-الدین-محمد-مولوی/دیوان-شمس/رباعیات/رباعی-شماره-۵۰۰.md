---
title: >-
    رباعی شمارهٔ ۵۰۰
---
# رباعی شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>آن وسوسه‌ای که شرمها را ببرد</p></div>
<div class="m2"><p>آن داهیه‌ای که بندها را بدرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سیر برهنه گردد از رسم جهان</p></div>
<div class="m2"><p>در عشق جهان را به پیازی نخرد</p></div></div>