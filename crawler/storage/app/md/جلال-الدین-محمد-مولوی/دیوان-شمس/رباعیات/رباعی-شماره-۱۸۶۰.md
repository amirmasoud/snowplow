---
title: >-
    رباعی شمارهٔ ۱۸۶۰
---
# رباعی شمارهٔ ۱۸۶۰

<div class="b" id="bn1"><div class="m1"><p>در روزه چو از طبع دمی پاک شوی</p></div>
<div class="m2"><p>اندر پی پاکان تو بر افلاک شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سوزش روزه نور گردی چون شمع</p></div>
<div class="m2"><p>وز ظلمت لقمه لقمهٔ خاک شوی</p></div></div>