---
title: >-
    رباعی شمارهٔ ۴۹۲
---
# رباعی شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>آن کس که از آب و گل نگاری دارد</p></div>
<div class="m2"><p>روزی به وصال او قراری دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نادره آنکه زاب و گل بیرون شد</p></div>
<div class="m2"><p>کو چون تو غریب شهریاری دارد</p></div></div>