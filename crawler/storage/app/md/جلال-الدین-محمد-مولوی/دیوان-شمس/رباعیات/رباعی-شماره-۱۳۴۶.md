---
title: >-
    رباعی شمارهٔ ۱۳۴۶
---
# رباعی شمارهٔ ۱۳۴۶

<div class="b" id="bn1"><div class="m1"><p>من قاعدهٔ درد و دوا می‌شکنم</p></div>
<div class="m2"><p>من قاعدهٔ مهر و جفا میشکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدی که به صدق توبه‌ها میکردم</p></div>
<div class="m2"><p>بنگر که چگونه توبه‌ها میشکنم</p></div></div>