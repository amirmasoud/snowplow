---
title: >-
    رباعی شمارهٔ ۱۳۰۸
---
# رباعی شمارهٔ ۱۳۰۸

<div class="b" id="bn1"><div class="m1"><p>ما جان لطیفیم و نظر در نائیم</p></div>
<div class="m2"><p>در جای نمائیم ولی بیجائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چهره اگر نقاب را بگشائیم</p></div>
<div class="m2"><p>عقل و دل و هوش جمله را بربائیم</p></div></div>