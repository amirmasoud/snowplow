---
title: >-
    رباعی شمارهٔ ۱۹۰۱
---
# رباعی شمارهٔ ۱۹۰۱

<div class="b" id="bn1"><div class="m1"><p>عاشق شوی ای دل و ز جان اندیشی</p></div>
<div class="m2"><p>دزدی کنی و ز پاسبان اندیشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی محبت کنی ای بی‌معنی</p></div>
<div class="m2"><p>وانگه ز زبان این و آن اندیشی</p></div></div>