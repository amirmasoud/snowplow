---
title: >-
    رباعی شمارهٔ ۱۳۷۴
---
# رباعی شمارهٔ ۱۳۷۴

<div class="b" id="bn1"><div class="m1"><p>یک جرعه ز جام تو تمامست تمام</p></div>
<div class="m2"><p>جز عشق تو در دلم کدامست کدام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو خون دل حلالست حلال</p></div>
<div class="m2"><p>آسودگی و عشق حرامست حرام</p></div></div>