---
title: >-
    رباعی شمارهٔ ۱۶۶۸
---
# رباعی شمارهٔ ۱۶۶۸

<div class="b" id="bn1"><div class="m1"><p>از جان بگریزم ار ز جان بگریزی</p></div>
<div class="m2"><p>از دل بگریزم ار از آن بگریزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو تیری و ما همچو کمانیم هنوز</p></div>
<div class="m2"><p>تیری چه عجب گر ز کمان بگریزی</p></div></div>