---
title: >-
    رباعی شمارهٔ ۱۴۱
---
# رباعی شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>آنکس که امید یاری غم داده است</p></div>
<div class="m2"><p>هان تا نخوری که او ترا دم داده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در روز خوشی همه جهان یار تواند</p></div>
<div class="m2"><p>یار شب غم نشان کسی کم داده است</p></div></div>