---
title: >-
    رباعی شمارهٔ ۲۹۸
---
# رباعی شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>خیزید که آن یار سعادت برخاست</p></div>
<div class="m2"><p>خیزید که از عشق غرامت برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیزید که آن لطیف قامت برخاست</p></div>
<div class="m2"><p>خیزید که امروز قیامت برخاست</p></div></div>