---
title: >-
    رباعی شمارهٔ ۸۲۶
---
# رباعی شمارهٔ ۸۲۶

<div class="b" id="bn1"><div class="m1"><p>من بندهٔ آن قوم که خود را دانند</p></div>
<div class="m2"><p>هردم دل خود را ز علط برهانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ذات و صفات خویش خالی گردند</p></div>
<div class="m2"><p>وز لوح وجود اناالحق خوانند</p></div></div>