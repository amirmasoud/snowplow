---
title: >-
    رباعی شمارهٔ ۳۷۸
---
# رباعی شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>گرمای تموز از دل پردرد شماست</p></div>
<div class="m2"><p>سرمای زمستان تبش سرد شماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این گرمی و سردی نرسد با صدپر</p></div>
<div class="m2"><p>بر گرد جهانیکه در او گرد شماست</p></div></div>