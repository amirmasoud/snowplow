---
title: >-
    رباعی شمارهٔ ۴۸۱
---
# رباعی شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>آن روز که عشق با دلم بستیزد</p></div>
<div class="m2"><p>جان پای برهنه از میان بگریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه کسی که عاقلم پندارد</p></div>
<div class="m2"><p>عاقل مردی که او ز من پرهیزد</p></div></div>