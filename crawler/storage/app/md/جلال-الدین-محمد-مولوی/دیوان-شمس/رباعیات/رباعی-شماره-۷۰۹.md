---
title: >-
    رباعی شمارهٔ ۷۰۹
---
# رباعی شمارهٔ ۷۰۹

<div class="b" id="bn1"><div class="m1"><p>دیوانه میان خلق پیدا باشد</p></div>
<div class="m2"><p>زیرا که سوار اسب سودا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه کسی بود که او را نشناخت</p></div>
<div class="m2"><p>دیوانه به نزد ما شناسا باشد</p></div></div>