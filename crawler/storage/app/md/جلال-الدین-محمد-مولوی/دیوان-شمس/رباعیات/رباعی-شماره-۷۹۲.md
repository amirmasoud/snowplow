---
title: >-
    رباعی شمارهٔ ۷۹۲
---
# رباعی شمارهٔ ۷۹۲

<div class="b" id="bn1"><div class="m1"><p>کس واقف آن حضرت شاهانه نشد</p></div>
<div class="m2"><p>تا بی‌دل و بی‌عقل سوی خانه نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه کسی بود که آن روی تو دید</p></div>
<div class="m2"><p>وانگه ز تو دور ماند و دیوانه نشد</p></div></div>