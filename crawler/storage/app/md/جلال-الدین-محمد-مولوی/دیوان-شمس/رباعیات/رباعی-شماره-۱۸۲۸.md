---
title: >-
    رباعی شمارهٔ ۱۸۲۸
---
# رباعی شمارهٔ ۱۸۲۸

<div class="b" id="bn1"><div class="m1"><p>جانم دارد ز عشق جان‌افزائی</p></div>
<div class="m2"><p>از سوداها لطیفتر سودائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز شهر تنم چو لولیان آواره است</p></div>
<div class="m2"><p>هر روز به منزلی و هر شب جائی</p></div></div>