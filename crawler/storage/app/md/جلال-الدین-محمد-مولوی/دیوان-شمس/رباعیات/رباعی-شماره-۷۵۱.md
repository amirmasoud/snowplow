---
title: >-
    رباعی شمارهٔ ۷۵۱
---
# رباعی شمارهٔ ۷۵۱

<div class="b" id="bn1"><div class="m1"><p>صد بار ز سر برفت عقلم و آمد</p></div>
<div class="m2"><p>تا کی ز می شیفتگان آشامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کار بماندم وز بیکاری نیز</p></div>
<div class="m2"><p>تا عاقبت کار کجا انجامد</p></div></div>