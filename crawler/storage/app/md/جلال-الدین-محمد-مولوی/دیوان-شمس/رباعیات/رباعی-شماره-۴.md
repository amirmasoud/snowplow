---
title: >-
    رباعی شمارهٔ ۴
---
# رباعی شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>آن لعل سخن که جان دهد مرجان را</p></div>
<div class="m2"><p>بی‌رنگ چه رنگ بخشد او مرجان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایه بخشد مشعلهٔ ایمان را</p></div>
<div class="m2"><p>بسیار بگفتیم و نگفتیم آن را</p></div></div>