---
title: >-
    رباعی شمارهٔ ۱۶۸۵
---
# رباعی شمارهٔ ۱۶۸۵

<div class="b" id="bn1"><div class="m1"><p>اندر دل من مها دل‌افروز توئی</p></div>
<div class="m2"><p>یاران هستند لیک دلسوز توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادند جهانیان به نوروز و به عید</p></div>
<div class="m2"><p>عید من و نوروز من امروز توئی</p></div></div>