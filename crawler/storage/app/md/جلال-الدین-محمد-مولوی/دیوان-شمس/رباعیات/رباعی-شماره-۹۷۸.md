---
title: >-
    رباعی شمارهٔ ۹۷۸
---
# رباعی شمارهٔ ۹۷۸

<div class="b" id="bn1"><div class="m1"><p>ای یوسف جان ز حال یعقوب بپرس</p></div>
<div class="m2"><p>وی جان کرم ز رنج ایوب بپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی جمله خوبان بر تو لعبتگان</p></div>
<div class="m2"><p>حال ما را ز هجرنا خوب بپرس</p></div></div>