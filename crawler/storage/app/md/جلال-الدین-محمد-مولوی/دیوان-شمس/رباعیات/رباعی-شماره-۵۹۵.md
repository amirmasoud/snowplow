---
title: >-
    رباعی شمارهٔ ۵۹۵
---
# رباعی شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>بی‌زارم از آن آب که آتش نشود</p></div>
<div class="m2"><p>در زلف مشوشی مشوش نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوقهٔ ما خوش است بیخوش نشود</p></div>
<div class="m2"><p>آن سر دارد که هیچ سرکش نشود</p></div></div>