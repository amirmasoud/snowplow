---
title: >-
    رباعی شمارهٔ ۴۴۹
---
# رباعی شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>هرگز ز دماغ بنده بوی تو نرفت</p></div>
<div class="m2"><p>وز دیدهٔ من خیال روی تو نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی تو عمر بردم شب و روز</p></div>
<div class="m2"><p>عمرم همه رفت و آرزوی تو نرفت</p></div></div>