---
title: >-
    رباعی شمارهٔ ۲۴۰
---
# رباعی شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>برجه که سماع روح برپای شده است</p></div>
<div class="m2"><p>وان دف چو شکر حریف آن نای شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای قدیم آتش افزای شده است</p></div>
<div class="m2"><p>آن های تو کو که وقت هیهات شده است</p></div></div>