---
title: >-
    رباعی شمارهٔ ۱۲۲۸
---
# رباعی شمارهٔ ۱۲۲۸

<div class="b" id="bn1"><div class="m1"><p>دشنامم ده که مست دشنام توام</p></div>
<div class="m2"><p>مست سقط خوش خوش آشام توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهرابه بیار تا بنوشم چو شکر</p></div>
<div class="m2"><p>من رام توام رام توام رام توام</p></div></div>