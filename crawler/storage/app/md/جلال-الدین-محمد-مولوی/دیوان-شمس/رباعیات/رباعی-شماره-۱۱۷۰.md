---
title: >-
    رباعی شمارهٔ ۱۱۷۰
---
# رباعی شمارهٔ ۱۱۷۰

<div class="b" id="bn1"><div class="m1"><p>با سرکشی عشق اگر سرد آرم</p></div>
<div class="m2"><p>بالله به سوگند که بس سر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزیکه چو منصور کنی بردارم</p></div>
<div class="m2"><p>هردم خبری آرد از آن سردارم</p></div></div>