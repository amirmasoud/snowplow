---
title: >-
    رباعی شمارهٔ ۴۳۷
---
# رباعی شمارهٔ ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>هرچند فراق پشت امید شکست</p></div>
<div class="m2"><p>هرچند جفا دو دست آمال ببست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نومید نمی‌شود دل عاشق مست</p></div>
<div class="m2"><p>مردم برسد به هر چه همت دربست</p></div></div>