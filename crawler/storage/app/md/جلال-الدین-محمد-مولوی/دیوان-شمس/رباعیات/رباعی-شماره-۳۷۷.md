---
title: >-
    رباعی شمارهٔ ۳۷۷
---
# رباعی شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>گر شرم همی از آن و این باید داشت</p></div>
<div class="m2"><p>پس عیب کسان زیر زمین باید داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور آینه‌وار نیک و بد بنمائی</p></div>
<div class="m2"><p>چون آینه روی آهنین باید داشت</p></div></div>