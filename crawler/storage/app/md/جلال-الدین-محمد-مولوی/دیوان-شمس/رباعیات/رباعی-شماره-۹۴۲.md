---
title: >-
    رباعی شمارهٔ ۹۴۲
---
# رباعی شمارهٔ ۹۴۲

<div class="b" id="bn1"><div class="m1"><p>امروز خوشم به جان تو فردا نیز</p></div>
<div class="m2"><p>هم آبم و هم گوهرم و دریا نیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم کار و گیای دوست کارافزا نیز</p></div>
<div class="m2"><p>هر لاف که دل زند بگویم ما نیز</p></div></div>