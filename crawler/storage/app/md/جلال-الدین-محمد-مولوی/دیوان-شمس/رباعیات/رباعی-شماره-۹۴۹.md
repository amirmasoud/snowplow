---
title: >-
    رباعی شمارهٔ ۹۴۹
---
# رباعی شمارهٔ ۹۴۹

<div class="b" id="bn1"><div class="m1"><p>زنها مشو غره به بیباکی باز</p></div>
<div class="m2"><p>زیرا که پری دارد از دولت باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغی تو ولیک مرغ مسکین و مجاز</p></div>
<div class="m2"><p>با باز شهنشاه تو شطرنج مباز</p></div></div>