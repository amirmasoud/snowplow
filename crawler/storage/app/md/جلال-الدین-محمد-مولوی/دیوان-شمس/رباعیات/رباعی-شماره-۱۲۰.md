---
title: >-
    رباعی شمارهٔ ۱۲۰
---
# رباعی شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>آن تلخ سخنها که چنان دل شکن است</p></div>
<div class="m2"><p>انصاف بده چه لایق آن دهن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین لب او تلخ نگفتی هرگز</p></div>
<div class="m2"><p>این بی‌نمکی ز شور بختی منست</p></div></div>