---
title: >-
    رباعی شمارهٔ ۸۰۱
---
# رباعی شمارهٔ ۸۰۱

<div class="b" id="bn1"><div class="m1"><p>گویند که فردوس برین خواهد بود</p></div>
<div class="m2"><p>آنجا می ناب و حور عین خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس ما می و معشوق به کف میداریم</p></div>
<div class="m2"><p>چون عاقبت کار همین خواهد بود</p></div></div>