---
title: >-
    رباعی شمارهٔ ۴۳۹
---
# رباعی شمارهٔ ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>هر درویشی که در شکست خویش است</p></div>
<div class="m2"><p>تا ظن نبری که او خیال اندیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که سراپردهٔ آنخوش کیش است</p></div>
<div class="m2"><p>از کون و مکان و کل عالم پیش است</p></div></div>