---
title: >-
    رباعی شمارهٔ ۱۱۴۴
---
# رباعی شمارهٔ ۱۱۴۴

<div class="b" id="bn1"><div class="m1"><p>امروز همه روز به پیش نظرم</p></div>
<div class="m2"><p>او بود از آن خراب و زیر و زبرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غایت حاضری چنین مهجورم</p></div>
<div class="m2"><p>وز قوت آن بیخبری بیخبرم</p></div></div>