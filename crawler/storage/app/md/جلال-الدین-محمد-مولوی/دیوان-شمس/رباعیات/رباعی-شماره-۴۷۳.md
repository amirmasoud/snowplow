---
title: >-
    رباعی شمارهٔ ۴۷۳
---
# رباعی شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>آن را که خدای ناف بر عشق برید</p></div>
<div class="m2"><p>او داند ناله‌های عشاق شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جای که دانه دید زانجا برمید</p></div>
<div class="m2"><p>پرید بدان سوی که مرغی نپرید</p></div></div>