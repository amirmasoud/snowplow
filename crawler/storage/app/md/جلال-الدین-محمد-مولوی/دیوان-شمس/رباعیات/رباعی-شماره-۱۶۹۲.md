---
title: >-
    رباعی شمارهٔ ۱۶۹۲
---
# رباعی شمارهٔ ۱۶۹۲

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تو خون عاشقان آشامی</p></div>
<div class="m2"><p>فریاد ز عاشقی و بی‌آرامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست منم اسیر دشمن کامی</p></div>
<div class="m2"><p>آخر به تو باز گردد این بدنامی</p></div></div>