---
title: >-
    رباعی شمارهٔ ۱۶۳۳
---
# رباعی شمارهٔ ۱۶۳۳

<div class="b" id="bn1"><div class="m1"><p>عشق غلب القلب و قد صار به</p></div>
<div class="m2"><p>حتی فنی القلب بما جاربه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القلب کطیی خفض الریش به</p></div>
<div class="m2"><p>عشق نتف الریش و قد طار به</p></div></div>