---
title: >-
    رباعی شمارهٔ ۱۲۹۲
---
# رباعی شمارهٔ ۱۲۹۲

<div class="b" id="bn1"><div class="m1"><p>گویی تو که من ز هر هنر باخبرم</p></div>
<div class="m2"><p>این بی‌خبری بس که ز خود بیخبرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا از من و مای خود مسلم نشوی</p></div>
<div class="m2"><p>با این ملکان محرم و همدم نشوم</p></div></div>