---
title: >-
    رباعی شمارهٔ ۱۰۶۴
---
# رباعی شمارهٔ ۱۰۶۴

<div class="b" id="bn1"><div class="m1"><p>مهمان تو نیست دو سه روز و گزاف</p></div>
<div class="m2"><p>خوان تو گرفته است از قاف به قاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر فتنه شود کسی معافست معاف</p></div>
<div class="m2"><p>بر شمع کند همیشه پروانه طواف</p></div></div>