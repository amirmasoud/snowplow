---
title: >-
    رباعی شمارهٔ ۳۵۳
---
# رباعی شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>شاهی که شفیع هر گنه بود برفت</p></div>
<div class="m2"><p>وانشب که به از هزار مه بود برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باز آید مرا نبیند تو بگوی</p></div>
<div class="m2"><p>کو همچو شما بر سر ره بود برفت</p></div></div>