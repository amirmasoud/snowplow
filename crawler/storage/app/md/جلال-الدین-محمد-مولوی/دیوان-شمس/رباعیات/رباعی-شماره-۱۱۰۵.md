---
title: >-
    رباعی شمارهٔ ۱۱۰۵
---
# رباعی شمارهٔ ۱۱۰۵

<div class="b" id="bn1"><div class="m1"><p>هم شاهد دیده‌ای و هم شاهد دل</p></div>
<div class="m2"><p>ای دیده و دل ز نور روی تو خجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند از آن هر دو چه حاصل کردی</p></div>
<div class="m2"><p>جز عشق ز عاشقان چه آید حاصل</p></div></div>