---
title: >-
    رباعی شمارهٔ ۲۲۴
---
# رباعی شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>با دشمن تو چو یار بسیار نشست</p></div>
<div class="m2"><p>با یار نشایدت دگربار نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرهیز از آن گلی که با خار نشست</p></div>
<div class="m2"><p>بگریز از آن مگس که بر مار نشست</p></div></div>