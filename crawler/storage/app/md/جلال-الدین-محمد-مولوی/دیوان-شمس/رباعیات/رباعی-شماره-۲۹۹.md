---
title: >-
    رباعی شمارهٔ ۲۹۹
---
# رباعی شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>دایم ز ولایت علی خواهم گفت</p></div>
<div class="m2"><p>چون روح قدس نادعلی خواهم گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روح شود غمی که بر جان منست</p></div>
<div class="m2"><p>کل هم و غم سینجلی خواهم گفت</p></div></div>