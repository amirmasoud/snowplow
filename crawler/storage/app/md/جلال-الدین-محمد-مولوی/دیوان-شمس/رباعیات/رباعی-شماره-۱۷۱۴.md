---
title: >-
    رباعی شمارهٔ ۱۷۱۴
---
# رباعی شمارهٔ ۱۷۱۴

<div class="b" id="bn1"><div class="m1"><p>ای پیر اگر تو روی با حق داری</p></div>
<div class="m2"><p>یا همچو صلاح دست مطلق داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینک رسن دراز و اینک سر دار</p></div>
<div class="m2"><p>بسم الله اگر سر انا الحق داری</p></div></div>