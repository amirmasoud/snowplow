---
title: >-
    رباعی شمارهٔ ۱۱۸۳
---
# رباعی شمارهٔ ۱۱۸۳

<div class="b" id="bn1"><div class="m1"><p>بیدف بر ما میا که ما در سوریم</p></div>
<div class="m2"><p>برخیز و دهل بزن که ما منصوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستیم نه مست بادهٔ انگوریم</p></div>
<div class="m2"><p>از هرچه خیال کرده‌ای ما دوریم</p></div></div>