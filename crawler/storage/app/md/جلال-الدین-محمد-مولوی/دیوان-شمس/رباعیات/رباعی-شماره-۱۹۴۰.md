---
title: >-
    رباعی شمارهٔ ۱۹۴۰
---
# رباعی شمارهٔ ۱۹۴۰

<div class="b" id="bn1"><div class="m1"><p>گفتند که هست یار را شور وشری</p></div>
<div class="m2"><p>گفتم که دوم بار بگو خوش خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا ترش است روی خوبش قدری</p></div>
<div class="m2"><p>گفتم که زهی تهمت کژ بر شکری</p></div></div>