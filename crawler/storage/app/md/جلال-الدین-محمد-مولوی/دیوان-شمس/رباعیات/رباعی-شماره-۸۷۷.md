---
title: >-
    رباعی شمارهٔ ۸۷۷
---
# رباعی شمارهٔ ۸۷۷

<div class="b" id="bn1"><div class="m1"><p>ای دل بگذر ز عشق و معشوق و دیار</p></div>
<div class="m2"><p>گر دیده وری ز هر سه بندی زنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در توبهٔ نیستی شو و باک مدار</p></div>
<div class="m2"><p>کاین فقر منزه است ز یار و اغیار</p></div></div>