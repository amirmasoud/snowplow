---
title: >-
    رباعی شمارهٔ ۶۷۱
---
# رباعی شمارهٔ ۶۷۱

<div class="b" id="bn1"><div class="m1"><p>در عشق تو عقل ذوفنون میخسبد</p></div>
<div class="m2"><p>مشتاق در آتش درون میخسبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌دیده و دل اگر نخسبم چه عجب</p></div>
<div class="m2"><p>خون گشته مرا دو دیده چون میخسبد</p></div></div>