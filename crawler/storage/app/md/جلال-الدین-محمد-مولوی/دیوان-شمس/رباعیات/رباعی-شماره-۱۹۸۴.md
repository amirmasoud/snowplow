---
title: >-
    رباعی شمارهٔ ۱۹۸۴
---
# رباعی شمارهٔ ۱۹۸۴

<div class="b" id="bn1"><div class="m1"><p>هرگز نبود میل تو کافراشت کنی</p></div>
<div class="m2"><p>تا عاشق آنی که فرو داشت کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسم الله ناگفته تو گوئی الحمد</p></div>
<div class="m2"><p>ناآمده صبح از طمع چاشت کنی</p></div></div>