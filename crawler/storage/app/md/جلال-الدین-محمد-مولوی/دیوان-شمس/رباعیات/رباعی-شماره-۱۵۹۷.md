---
title: >-
    رباعی شمارهٔ ۱۵۹۷
---
# رباعی شمارهٔ ۱۵۹۷

<div class="b" id="bn1"><div class="m1"><p>السکر صار کاسدا من شفتیه</p></div>
<div class="m2"><p>والبدر تراه ساجدا بین یدیه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالحسن علیه کل شیی وافر</p></div>
<div class="m2"><p>الا فمه فانه ضاق علیه</p></div></div>