---
title: >-
    رباعی شمارهٔ ۱۸۷۲
---
# رباعی شمارهٔ ۱۸۷۲

<div class="b" id="bn1"><div class="m1"><p>دلدار به زیر لب بخواند چیزی</p></div>
<div class="m2"><p>دیوانه شوی عقل نماند چیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب چه فسونست که او می‌خواند</p></div>
<div class="m2"><p>کاندر دل سنگ می‌نشاند چیزی</p></div></div>