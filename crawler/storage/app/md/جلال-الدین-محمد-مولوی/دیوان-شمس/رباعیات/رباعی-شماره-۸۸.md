---
title: >-
    رباعی شمارهٔ ۸۸
---
# رباعی شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>ای روی ترا غلام گلنار مخسپ</p></div>
<div class="m2"><p>وی رونق نوبهار و گلزار مخسب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نرگس پرخمار خونخوار مخسب</p></div>
<div class="m2"><p>امشب شب عشرت است زنهار مخسب</p></div></div>