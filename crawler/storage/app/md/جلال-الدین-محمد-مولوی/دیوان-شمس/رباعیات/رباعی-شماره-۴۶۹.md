---
title: >-
    رباعی شمارهٔ ۴۶۹
---
# رباعی شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>آن ذره که جز همدم خورشید نشد</p></div>
<div class="m2"><p>بر نقد زد و سخرهٔ امید نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقت به کدام سر درافتاد که زود</p></div>
<div class="m2"><p>از باد تو رقصان چو سر بید نشد</p></div></div>