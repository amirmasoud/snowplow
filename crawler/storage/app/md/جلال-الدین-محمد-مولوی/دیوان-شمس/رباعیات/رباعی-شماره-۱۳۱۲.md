---
title: >-
    رباعی شمارهٔ ۱۳۱۲
---
# رباعی شمارهٔ ۱۳۱۲

<div class="b" id="bn1"><div class="m1"><p>ما رخت وجود بر عدم بربندیم</p></div>
<div class="m2"><p>بر هستی نیست مزور خندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازی بازی طنابها بگسستیم</p></div>
<div class="m2"><p>تا خیمهٔ صبر از فلک برکندیم</p></div></div>