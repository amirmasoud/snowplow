---
title: >-
    رباعی شمارهٔ ۳۳۷
---
# رباعی شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>روزی ترش است و دیدهٔ ابرتر است</p></div>
<div class="m2"><p>این گریه برای خندهٔ برگ و بر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بازی کودکان و خندید نشان</p></div>
<div class="m2"><p>از گریهٔ مادر است و قبض پدر است</p></div></div>