---
title: >-
    رباعی شمارهٔ ۱۹۹
---
# رباعی شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>ای فکر تو بر بسته نه پایت باز است</p></div>
<div class="m2"><p>آخر حرکت نیز که دیدی راز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر حرکت قبض یقین بسط شود</p></div>
<div class="m2"><p>آب چه و آب جو بدین ممتاز است</p></div></div>