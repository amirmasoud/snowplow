---
title: >-
    رباعی شمارهٔ ۱۷۲۸
---
# رباعی شمارهٔ ۱۷۲۸

<div class="b" id="bn1"><div class="m1"><p>ای دل تو بدین مفلسی و رسوائی</p></div>
<div class="m2"><p>انصاف بده که عشق را چون سائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق آتش تیز است و ترا آبی نیست</p></div>
<div class="m2"><p>خاکت بر سر چه باد می‌پیمائی</p></div></div>