---
title: >-
    رباعی شمارهٔ ۶۳۷
---
# رباعی شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>چون دیده بر آن عارض چون سیم افتاد</p></div>
<div class="m2"><p>جان در لب تو چو دیدهٔ میم افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمرود صفت ز دیدگان رفت دلم</p></div>
<div class="m2"><p>در آتش سودای براهیم افتاد</p></div></div>