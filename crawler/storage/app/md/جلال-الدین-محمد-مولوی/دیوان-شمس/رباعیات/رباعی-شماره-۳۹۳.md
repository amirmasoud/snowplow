---
title: >-
    رباعی شمارهٔ ۳۹۳
---
# رباعی شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>گفتی چونی بنده چنانست که هست</p></div>
<div class="m2"><p>سودای تو بر سر است و سر بر سر دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میگردد آن چیز بگرد سر من</p></div>
<div class="m2"><p>نامش نتوان گفت ولیکن چه خوش است</p></div></div>