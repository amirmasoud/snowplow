---
title: >-
    رباعی شمارهٔ ۱۷۳
---
# رباعی شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>اندر سر ما همت کاری دگر است</p></div>
<div class="m2"><p>معشوقه خوب ما نگاری دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله که بعشق نیز قانع نشویم</p></div>
<div class="m2"><p>ما را پس از این خزان بهاری دگر است</p></div></div>