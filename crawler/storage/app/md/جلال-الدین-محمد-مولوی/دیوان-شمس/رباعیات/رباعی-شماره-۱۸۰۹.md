---
title: >-
    رباعی شمارهٔ ۱۸۰۹
---
# رباعی شمارهٔ ۱۸۰۹

<div class="b" id="bn1"><div class="m1"><p>پیش آی خیال او که شوری داری</p></div>
<div class="m2"><p>بر دیدهٔ من نشین که نوری داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طالع خود ز زهره سوری داری</p></div>
<div class="m2"><p>در سینه چو داود زبوری داری</p></div></div>