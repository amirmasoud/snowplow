---
title: >-
    رباعی شمارهٔ ۱۲۵
---
# رباعی شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>آن چشم که خون گشت غم او را جفت است</p></div>
<div class="m2"><p>زو خواب طمع مدار کوکی خفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پندارد کاین نیز نهایت دارد</p></div>
<div class="m2"><p>ای بیخبر از عشق که این را گفته است</p></div></div>