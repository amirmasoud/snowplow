---
title: >-
    رباعی شمارهٔ ۵۴۷
---
# رباعی شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>ایام وصال یار گوئی که نبود</p></div>
<div class="m2"><p>وان دولت بیشمار گوئی که نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یار به جز فراق بر جای نماند</p></div>
<div class="m2"><p>رفت آن همه روزگار گوئی که نبود</p></div></div>