---
title: >-
    رباعی شمارهٔ ۱۱۶۹
---
# رباعی شمارهٔ ۱۱۶۹

<div class="b" id="bn1"><div class="m1"><p>بازآمد و بازآمد ره بگشائیم</p></div>
<div class="m2"><p>جویان دلست دل بدو بنمائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما نعره‌زنان که آن شکارت مائیم</p></div>
<div class="m2"><p>او خنده کنان که ما ترا میپائیم</p></div></div>