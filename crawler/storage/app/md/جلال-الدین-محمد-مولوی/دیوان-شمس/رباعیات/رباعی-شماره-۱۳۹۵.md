---
title: >-
    رباعی شمارهٔ ۱۳۹۵
---
# رباعی شمارهٔ ۱۳۹۵

<div class="b" id="bn1"><div class="m1"><p>ای بی‌تو حرام زندگانی ای جان</p></div>
<div class="m2"><p>خود بی‌تو کدام زندگانی ای جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوگند خورم که زندگانی بی‌تو</p></div>
<div class="m2"><p>مرگست به نام زندگانی ای جان</p></div></div>