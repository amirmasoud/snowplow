---
title: >-
    رباعی شمارهٔ ۹۳۸
---
# رباعی شمارهٔ ۹۳۸

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو داده باز جان را پرواز</p></div>
<div class="m2"><p>لطف تو کشیده چنگ جان را در ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذره عنایت تو ای بنده‌نواز</p></div>
<div class="m2"><p>بهتر ز هزار ساله تسبیح و نماز</p></div></div>