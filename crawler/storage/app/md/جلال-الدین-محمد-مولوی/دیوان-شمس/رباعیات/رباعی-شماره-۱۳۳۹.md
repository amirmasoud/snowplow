---
title: >-
    رباعی شمارهٔ ۱۳۳۹
---
# رباعی شمارهٔ ۱۳۳۹

<div class="b" id="bn1"><div class="m1"><p>من سیر نیم ولی ز سیران سیرم</p></div>
<div class="m2"><p>بر خاک درت ز آب حیوان سیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمان به تو دادم وز جان برگشتم</p></div>
<div class="m2"><p>سیرم از این چو ملحد از آن سیرم</p></div></div>