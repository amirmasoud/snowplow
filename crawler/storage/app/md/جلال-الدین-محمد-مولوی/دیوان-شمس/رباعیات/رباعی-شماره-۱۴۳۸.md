---
title: >-
    رباعی شمارهٔ ۱۴۳۸
---
# رباعی شمارهٔ ۱۴۳۸

<div class="b" id="bn1"><div class="m1"><p>بر خسته دلان راه ملامت می زن</p></div>
<div class="m2"><p>هردم زخمی فزون ز طاقت می زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش می زن به هر نفس در جانی</p></div>
<div class="m2"><p>واندر همه دم دم فراغت می زن</p></div></div>