---
title: >-
    رباعی شمارهٔ ۱۴۳۱
---
# رباعی شمارهٔ ۱۴۳۱

<div class="b" id="bn1"><div class="m1"><p>ای یار بیا و بر دلم بر میزان</p></div>
<div class="m2"><p>وی زهره بیا و از رخم زر میزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنان که میان ما جدائی جستند</p></div>
<div class="m2"><p>دیوار بد و نمای و گو سر میزن</p></div></div>