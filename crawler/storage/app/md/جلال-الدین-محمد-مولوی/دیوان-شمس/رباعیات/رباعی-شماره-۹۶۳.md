---
title: >-
    رباعی شمارهٔ ۹۶۳
---
# رباعی شمارهٔ ۹۶۳

<div class="b" id="bn1"><div class="m1"><p>مائیم و هوای یار مه رو شب و روز</p></div>
<div class="m2"><p>چون ماهی تشنه اندر این جو شب و روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین روز شبان کجا برد بو شب و روز</p></div>
<div class="m2"><p>خود در شب وصل عاشقان کو شب و روز</p></div></div>