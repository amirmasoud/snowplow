---
title: >-
    رباعی شمارهٔ ۸۷۴
---
# رباعی شمارهٔ ۸۷۴

<div class="b" id="bn1"><div class="m1"><p>ای بوده سماع آسمانرا ره و در</p></div>
<div class="m2"><p>وی بوده سماع مرغ جانرا سر و پر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اما به حضور تست آن چیز دگر</p></div>
<div class="m2"><p>مانند نماز از پس پیغمبر</p></div></div>