---
title: >-
    رباعی شمارهٔ ۱۰۸۹
---
# رباعی شمارهٔ ۱۰۸۹

<div class="b" id="bn1"><div class="m1"><p>این نکته شنو ز بنده ای نقش چگل</p></div>
<div class="m2"><p>هرچند که راهیست ز دل جانب دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم تو نیستم تو در چشم منی</p></div>
<div class="m2"><p>تو مردم دیدای و من مردم گل</p></div></div>