---
title: >-
    رباعی شمارهٔ ۲۱۷
---
# رباعی شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>این همدم اندرون که دم میدهدت</p></div>
<div class="m2"><p>امید رسیدن به حرم می‌دهدت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو تا دم آخرین دم او میخور</p></div>
<div class="m2"><p>کان عشوه نباشد ز کرم میدهدت</p></div></div>