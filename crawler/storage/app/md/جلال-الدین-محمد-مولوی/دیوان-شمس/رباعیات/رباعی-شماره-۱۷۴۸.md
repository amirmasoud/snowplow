---
title: >-
    رباعی شمارهٔ ۱۷۴۸
---
# رباعی شمارهٔ ۱۷۴۸

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو عین عالم حیرانی</p></div>
<div class="m2"><p>سرمایهٔ سودای تو سرگردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال من دلسوخته تا کی پرسی</p></div>
<div class="m2"><p>چون می‌دانم که به ز من میدانی</p></div></div>