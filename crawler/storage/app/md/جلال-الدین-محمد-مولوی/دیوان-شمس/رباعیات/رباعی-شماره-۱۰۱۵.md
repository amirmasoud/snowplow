---
title: >-
    رباعی شمارهٔ ۱۰۱۵
---
# رباعی شمارهٔ ۱۰۱۵

<div class="b" id="bn1"><div class="m1"><p>ای سودائی برو پی سودا باش</p></div>
<div class="m2"><p>در صورت شیدای دلت شیدا باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سایهٔ خود ز خوی خود در جنگی</p></div>
<div class="m2"><p>خود سایهٔ تست خصم تو، تنها باش</p></div></div>