---
title: >-
    رباعی شمارهٔ ۱۲۱۷
---
# رباعی شمارهٔ ۱۲۱۷

<div class="b" id="bn1"><div class="m1"><p>در بحر خیال غرقهٔ گردابم</p></div>
<div class="m2"><p>نی بلکه به بحر می‌کشد سیلابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده نمی‌خواب من بندهٔ آنک</p></div>
<div class="m2"><p>در خواب بدانست که من در خوابم</p></div></div>