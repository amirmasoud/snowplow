---
title: >-
    رباعی شمارهٔ ۹۳
---
# رباعی شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>بی‌جام در این دور شرابست شراب</p></div>
<div class="m2"><p>بی‌دود در این سینه کبابست کباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد رباب عشق از زحمهٔ او است</p></div>
<div class="m2"><p>زنهار مگو همین ربابست رباب</p></div></div>