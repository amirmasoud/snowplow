---
title: >-
    رباعی شمارهٔ ۶۵۴
---
# رباعی شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>خوش عادت خوش خو که محمد دارد</p></div>
<div class="m2"><p>ما را شب تیره بینوا نگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنوازد آن رباب را تا به سحر</p></div>
<div class="m2"><p>ور خواب آید گلوش را بفشارد</p></div></div>