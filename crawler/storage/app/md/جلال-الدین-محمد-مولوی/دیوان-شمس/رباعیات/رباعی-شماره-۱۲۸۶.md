---
title: >-
    رباعی شمارهٔ ۱۲۸۶
---
# رباعی شمارهٔ ۱۲۸۶

<div class="b" id="bn1"><div class="m1"><p>گر صبر کنی پردهٔ صبرت بدریم</p></div>
<div class="m2"><p>ور خواب روی خواب ز چشمت ببریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کوه شوی در آتشت بگدازیم</p></div>
<div class="m2"><p>ور بحر شوی تمام آبت بخوریم</p></div></div>