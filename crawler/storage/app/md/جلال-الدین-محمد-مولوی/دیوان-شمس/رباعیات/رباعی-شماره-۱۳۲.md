---
title: >-
    رباعی شمارهٔ ۱۳۲
---
# رباعی شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>آن را که غمی باشد و بتواند گفت</p></div>
<div class="m2"><p>گر از دل خود بگفت بتواند رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این طرفه گلی نگر که ما را بشکفت</p></div>
<div class="m2"><p>نه رنگ توان نمود و نه بوی نهفت</p></div></div>