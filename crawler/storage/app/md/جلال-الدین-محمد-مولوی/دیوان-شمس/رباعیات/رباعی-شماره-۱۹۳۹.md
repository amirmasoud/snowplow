---
title: >-
    رباعی شمارهٔ ۱۹۳۹
---
# رباعی شمارهٔ ۱۹۳۹

<div class="b" id="bn1"><div class="m1"><p>گفتم که کدامست طریق هستی</p></div>
<div class="m2"><p>دل گفت طریق هستی اندر پستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس گفتم دل چرا ز پستی برمد</p></div>
<div class="m2"><p>گفتا زانرو که در درین دربستی</p></div></div>