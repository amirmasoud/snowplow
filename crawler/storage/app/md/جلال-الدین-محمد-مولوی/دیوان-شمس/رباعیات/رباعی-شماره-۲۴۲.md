---
title: >-
    رباعی شمارهٔ ۲۴۲
---
# رباعی شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>برکان شکر چند مگس را غوغاست</p></div>
<div class="m2"><p>کی کان شکر را به مگسها پرواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغی که بر آن کوه نشست و برخاست</p></div>
<div class="m2"><p>بنگر که بر آن کوه چه افزود و چه کاست</p></div></div>