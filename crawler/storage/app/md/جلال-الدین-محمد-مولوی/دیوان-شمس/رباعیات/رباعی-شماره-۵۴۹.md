---
title: >-
    رباعی شمارهٔ ۵۴۹
---
# رباعی شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>ای اهل مناجات که در محرابید</p></div>
<div class="m2"><p>منزل دور است یک زمان بشتابید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی اهل خرابات که در غرقابید</p></div>
<div class="m2"><p>صد قافله بگذشت و شما در خوابید</p></div></div>