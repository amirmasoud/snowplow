---
title: >-
    رباعی شمارهٔ ۵۴۶
---
# رباعی شمارهٔ ۵۴۶

<div class="b" id="bn1"><div class="m1"><p>ای اطلس دعوی ترا معنی برد</p></div>
<div class="m2"><p>فردا به قیامت این عمل خواهی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمت بادا اگر چنین خواهی زیست</p></div>
<div class="m2"><p>ننگت بادا اگر چنان خواهی مرد</p></div></div>