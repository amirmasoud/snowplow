---
title: >-
    رباعی شمارهٔ ۱۲۹۵
---
# رباعی شمارهٔ ۱۲۹۵

<div class="b" id="bn1"><div class="m1"><p>گفتم که دل از تو برکنم نتوانم</p></div>
<div class="m2"><p>یا بی‌غم تو دمی زنم نتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که ز سر برون کنم سودایت</p></div>
<div class="m2"><p>ای خواجه اگر مرد منم نتوانم</p></div></div>