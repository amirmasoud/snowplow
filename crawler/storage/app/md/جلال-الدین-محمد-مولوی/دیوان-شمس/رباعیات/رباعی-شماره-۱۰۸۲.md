---
title: >-
    رباعی شمارهٔ ۱۰۸۲
---
# رباعی شمارهٔ ۱۰۸۲

<div class="b" id="bn1"><div class="m1"><p>آنکس که ترا دید و نخندید چو گل</p></div>
<div class="m2"><p>از جان و خرد تهیست مانند دهل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گبر ابدی باشد کو شاد نشد</p></div>
<div class="m2"><p>از دعوت ذوالجلال و دیدار رسل</p></div></div>