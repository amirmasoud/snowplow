---
title: >-
    رباعی شمارهٔ ۸۸۳
---
# رباعی شمارهٔ ۸۸۳

<div class="b" id="bn1"><div class="m1"><p>بالا بنگر دو چشم را بالا دار</p></div>
<div class="m2"><p>صاحب‌نظری کن و نظر با ما دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردانه و مرد روی دل اینجا دار</p></div>
<div class="m2"><p>آوردم و آمدم تو دانی یاد آر</p></div></div>