---
title: >-
    رباعی شمارهٔ ۱۸۶۶
---
# رباعی شمارهٔ ۱۸۶۶

<div class="b" id="bn1"><div class="m1"><p>در عشق موافقت بود چون جانی</p></div>
<div class="m2"><p>در مذهب هر ظریف معنی دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سی و دو دندان چو یکی گشت دراز</p></div>
<div class="m2"><p>بی‌دندان شد از چنان دندانی</p></div></div>