---
title: >-
    رباعی شمارهٔ ۵۶۰
---
# رباعی شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>ای قوم که برتر از مه و مهتابید</p></div>
<div class="m2"><p>از هستی آب و گل چرا میتابید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای اهل خرابات که در غرقابید</p></div>
<div class="m2"><p>خیزید که روز و شب چرا در خوابید</p></div></div>