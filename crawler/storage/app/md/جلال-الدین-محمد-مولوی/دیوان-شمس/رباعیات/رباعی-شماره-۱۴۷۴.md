---
title: >-
    رباعی شمارهٔ ۱۴۷۴
---
# رباعی شمارهٔ ۱۴۷۴

<div class="b" id="bn1"><div class="m1"><p>دوشست دیدم یار جدائی جویان</p></div>
<div class="m2"><p>با من به جفا و کین جدا شو گریان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز چنانم که جدا گشته ز جان</p></div>
<div class="m2"><p>رخسارهٔ خود به خون فرقت شویان</p></div></div>