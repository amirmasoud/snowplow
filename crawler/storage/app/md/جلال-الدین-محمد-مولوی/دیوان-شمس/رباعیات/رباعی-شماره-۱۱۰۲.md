---
title: >-
    رباعی شمارهٔ ۱۱۰۲
---
# رباعی شمارهٔ ۱۱۰۲

<div class="b" id="bn1"><div class="m1"><p>مردا منشین جز که به پهلوی رجال</p></div>
<div class="m2"><p>خوش باشد آینه به پهلوی صقال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب چه طرب دارد جان پهلوی جان</p></div>
<div class="m2"><p>آن سنگ بود فتاده پهلوی سفال</p></div></div>