---
title: >-
    رباعی شمارهٔ ۱۹۷۶
---
# رباعی شمارهٔ ۱۹۷۶

<div class="b" id="bn1"><div class="m1"><p>واپس مانی ز یار واپس باشی</p></div>
<div class="m2"><p>از شاخ درخت بگسلی خس باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم کسی تو خویش را جای کنی</p></div>
<div class="m2"><p>تو مردمک دیدهٔ آن کس باشی</p></div></div>