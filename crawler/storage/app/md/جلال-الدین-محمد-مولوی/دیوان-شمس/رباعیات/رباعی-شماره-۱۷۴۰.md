---
title: >-
    رباعی شمارهٔ ۱۷۴۰
---
# رباعی شمارهٔ ۱۷۴۰

<div class="b" id="bn1"><div class="m1"><p>ای ساقی از آن باده که اول دادی</p></div>
<div class="m2"><p>رطلی دو درانداز و بیفزا شادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا چاشنیی از آن نبایست نمود</p></div>
<div class="m2"><p>یا مست و خراب کن چو سر بگشادی</p></div></div>