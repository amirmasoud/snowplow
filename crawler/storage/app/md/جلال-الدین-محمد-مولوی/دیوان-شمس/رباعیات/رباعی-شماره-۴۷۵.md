---
title: >-
    رباعی شمارهٔ ۴۷۵
---
# رباعی شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>آن را منگر که ذوفنون آید مرد</p></div>
<div class="m2"><p>در عهد و وفا نگر که چون آید مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عهدهٔ عهد اگر برون آید مرد</p></div>
<div class="m2"><p>از هرچه صفت کنی فزون آید مرد</p></div></div>