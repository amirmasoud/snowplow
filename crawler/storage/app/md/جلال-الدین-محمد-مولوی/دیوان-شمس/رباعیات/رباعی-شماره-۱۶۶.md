---
title: >-
    رباعی شمارهٔ ۱۶۶
---
# رباعی شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>امشب آمد خیال آن دلبر چست</p></div>
<div class="m2"><p>در خانهٔ تن مقام دل را میجست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را چو بیافت زود خنجر بکشید</p></div>
<div class="m2"><p>زد بر دل من که دست و بازوش درست</p></div></div>