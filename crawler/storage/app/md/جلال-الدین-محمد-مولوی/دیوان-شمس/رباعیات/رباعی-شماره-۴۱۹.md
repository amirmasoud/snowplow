---
title: >-
    رباعی شمارهٔ ۴۱۹
---
# رباعی شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>منصور حلاجی که اناالحق میگفت</p></div>
<div class="m2"><p>خاک همه ره به نوک مژگان می‌رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درقلزم نیستی خود غوطه بخورد</p></div>
<div class="m2"><p>آنکه پس از آن در اناالحق می‌سفت</p></div></div>