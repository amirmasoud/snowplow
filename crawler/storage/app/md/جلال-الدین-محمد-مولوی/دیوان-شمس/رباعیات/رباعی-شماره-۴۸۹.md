---
title: >-
    رباعی شمارهٔ ۴۸۹
---
# رباعی شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>آن کس که بر آتش جهانم بنهاد</p></div>
<div class="m2"><p>صد گونه زبانه بر زبانم بنهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شش جهتم شعلهٔ آتش بگرفت</p></div>
<div class="m2"><p>آه کردم و دست بر دهانم بنهاد</p></div></div>