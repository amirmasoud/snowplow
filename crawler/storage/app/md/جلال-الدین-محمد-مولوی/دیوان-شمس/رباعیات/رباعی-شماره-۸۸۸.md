---
title: >-
    رباعی شمارهٔ ۸۸۸
---
# رباعی شمارهٔ ۸۸۸

<div class="b" id="bn1"><div class="m1"><p>تا چند کشی سخرهٔ نفس بیکار</p></div>
<div class="m2"><p>تا چند خوری چو اشتران خوشهٔ خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند دوی از پی نان و دینار</p></div>
<div class="m2"><p>ای کافر و کافر بچه آخر دین‌دار</p></div></div>