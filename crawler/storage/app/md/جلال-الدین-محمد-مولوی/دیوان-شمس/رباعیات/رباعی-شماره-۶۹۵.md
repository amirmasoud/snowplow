---
title: >-
    رباعی شمارهٔ ۶۹۵
---
# رباعی شمارهٔ ۶۹۵

<div class="b" id="bn1"><div class="m1"><p>دلدار ابد گرد دلم میگردد</p></div>
<div class="m2"><p>گرد دل و جان خجلم میگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گل چو درخت سر برآرم خندان</p></div>
<div class="m2"><p>کاب حیوان گرد گلم میگردد</p></div></div>