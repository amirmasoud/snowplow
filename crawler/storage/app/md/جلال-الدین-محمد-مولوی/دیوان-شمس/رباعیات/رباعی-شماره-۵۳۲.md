---
title: >-
    رباعی شمارهٔ ۵۳۲
---
# رباعی شمارهٔ ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>امروز ما یار جنون میخواهد</p></div>
<div class="m2"><p>ما مجنون و او افزون می‌خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نیست چنین پرده چرا میدرد</p></div>
<div class="m2"><p>رسوا شده او پرده برون میخواهد</p></div></div>