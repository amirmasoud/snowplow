---
title: >-
    رباعی شمارهٔ ۱۷۹۹
---
# رباعی شمارهٔ ۱۷۹۹

<div class="b" id="bn1"><div class="m1"><p>بر ظلمت شب خیمهٔ مهتاب زدی</p></div>
<div class="m2"><p>می‌خفت خرد بر رخ او آب زدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادی همه را به وعده خواب خرگوشی</p></div>
<div class="m2"><p>وز تیغ فراق گردن خواب زدی</p></div></div>