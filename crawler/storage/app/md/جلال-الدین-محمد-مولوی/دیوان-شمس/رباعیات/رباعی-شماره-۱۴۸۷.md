---
title: >-
    رباعی شمارهٔ ۱۴۸۷
---
# رباعی شمارهٔ ۱۴۸۷

<div class="b" id="bn1"><div class="m1"><p>شد کودکی و رفت جوانی ز جوان</p></div>
<div class="m2"><p>روز پیری رسید بر پر ز جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر مهمانرا سه روز باشد پیمان</p></div>
<div class="m2"><p>ای خواجه سه روز شد تو بر خیز و بران</p></div></div>