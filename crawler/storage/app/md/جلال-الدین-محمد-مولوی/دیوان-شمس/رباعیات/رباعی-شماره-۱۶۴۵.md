---
title: >-
    رباعی شمارهٔ ۱۶۴۵
---
# رباعی شمارهٔ ۱۶۴۵

<div class="b" id="bn1"><div class="m1"><p>میخوردم باده بابت آشفته</p></div>
<div class="m2"><p>خوابم بربود حال دل ناگفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدار شدم ز خواب مستی دیدم</p></div>
<div class="m2"><p>دلبر شده شمع مرده ساقی خفته</p></div></div>