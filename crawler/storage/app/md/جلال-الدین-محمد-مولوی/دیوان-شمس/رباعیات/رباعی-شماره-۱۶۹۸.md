---
title: >-
    رباعی شمارهٔ ۱۶۹۸
---
# رباعی شمارهٔ ۱۶۹۸

<div class="b" id="bn1"><div class="m1"><p>ای آنکه طبیب دردهای مائی</p></div>
<div class="m2"><p>این درد ز حد رفت چه میفرمائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله اگر هزار معجون داری</p></div>
<div class="m2"><p>من جان نبرم تا تو رخی ننمائی</p></div></div>