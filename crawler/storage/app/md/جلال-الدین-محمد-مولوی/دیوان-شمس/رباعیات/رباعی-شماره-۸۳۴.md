---
title: >-
    رباعی شمارهٔ ۸۳۴
---
# رباعی شمارهٔ ۸۳۴

<div class="b" id="bn1"><div class="m1"><p>می‌گوید عشق هرکه جان پیش کشد</p></div>
<div class="m2"><p>صد جان و هزار جان عوض بیش کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوش تو بین عشق چها میگوید</p></div>
<div class="m2"><p>تا گوش کشانت بسوی خویش کشد</p></div></div>