---
title: >-
    رباعی شمارهٔ ۸۹۴
---
# رباعی شمارهٔ ۸۹۴

<div class="b" id="bn1"><div class="m1"><p>در باغ در نیامدم گرد آور</p></div>
<div class="m2"><p>درویش و تهی روم من راهگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که برون روم مرا بگشا در</p></div>
<div class="m2"><p>ور نگشائی گمان بد نیز مبر</p></div></div>