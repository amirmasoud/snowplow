---
title: >-
    رباعی شمارهٔ ۹۷۶
---
# رباعی شمارهٔ ۹۷۶

<div class="b" id="bn1"><div class="m1"><p>از حادثهٔ جهان زاینده مترس</p></div>
<div class="m2"><p>وز هرچه رسد چو نیست پاینده مترس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این یکدم عمر را غنیمت میدان</p></div>
<div class="m2"><p>از رفته میندیش وز آینده مترس</p></div></div>