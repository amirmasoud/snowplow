---
title: >-
    رباعی شمارهٔ ۱۲۱۹
---
# رباعی شمارهٔ ۱۲۱۹

<div class="b" id="bn1"><div class="m1"><p>در دور سپهر و مهر ساقی مائیم</p></div>
<div class="m2"><p>سرمست مدام اشتیاقی مائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آینه وجود کردیم نگاه</p></div>
<div class="m2"><p>مائیم و نمائیم که باقی مائیم</p></div></div>