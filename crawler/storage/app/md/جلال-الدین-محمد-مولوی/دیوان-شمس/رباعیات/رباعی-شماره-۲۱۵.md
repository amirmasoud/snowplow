---
title: >-
    رباعی شمارهٔ ۲۱۵
---
# رباعی شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>این من نه منم آنکه منم گوئی کیست</p></div>
<div class="m2"><p>گویا نه منم در دهنم گوئی کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من پیرهنی بیش نیم سر تا پای</p></div>
<div class="m2"><p>آن کس که منش پیرهنم گوئی کیست</p></div></div>