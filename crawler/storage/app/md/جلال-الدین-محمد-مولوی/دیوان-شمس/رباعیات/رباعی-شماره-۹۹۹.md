---
title: >-
    رباعی شمارهٔ ۹۹۹
---
# رباعی شمارهٔ ۹۹۹

<div class="b" id="bn1"><div class="m1"><p>اندر بر خویشم بفشاری همه خوش</p></div>
<div class="m2"><p>بر راه زنان مرگ گماری همه خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مرگ دهی از پس آن برگ دهی</p></div>
<div class="m2"><p>از مرگ حیاتها برآری همه خوش</p></div></div>