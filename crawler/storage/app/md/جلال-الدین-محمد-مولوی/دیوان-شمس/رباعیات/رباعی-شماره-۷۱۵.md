---
title: >-
    رباعی شمارهٔ ۷۱۵
---
# رباعی شمارهٔ ۷۱۵

<div class="b" id="bn1"><div class="m1"><p>روزیکه بود دلت ز جان پر از درد</p></div>
<div class="m2"><p>شکرانه هزاران جان فدا باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاندر ره عشق و عاشقی ای سره مرد</p></div>
<div class="m2"><p>بیشکر قفای نیکوان نتوان کرد</p></div></div>