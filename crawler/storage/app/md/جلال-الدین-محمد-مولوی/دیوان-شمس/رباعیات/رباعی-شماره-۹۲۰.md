---
title: >-
    رباعی شمارهٔ ۹۲۰
---
# رباعی شمارهٔ ۹۲۰

<div class="b" id="bn1"><div class="m1"><p>من رنگ خزان دارم و تو رنگ بهار</p></div>
<div class="m2"><p>تا این دو یکی نشد نیامد گل و خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این خار و گل ارچه شد مخالف دیدار</p></div>
<div class="m2"><p>بر چشم خلاف بین بخند ای گلزار</p></div></div>