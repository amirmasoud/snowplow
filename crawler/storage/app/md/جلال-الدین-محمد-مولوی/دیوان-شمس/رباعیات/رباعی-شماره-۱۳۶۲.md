---
title: >-
    رباعی شمارهٔ ۱۳۶۲
---
# رباعی شمارهٔ ۱۳۶۲

<div class="b" id="bn1"><div class="m1"><p>نی دست که در مصاف خونریز کنم</p></div>
<div class="m2"><p>نی پای که در صبر قدم تیز کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی رحم ترا که با رهی در سازی</p></div>
<div class="m2"><p>نی عقل مرا که از تو پرهیز کنم</p></div></div>