---
title: >-
    رباعی شمارهٔ ۳۷
---
# رباعی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>تا عشق ترا است این شکرخایی‌ها</p></div>
<div class="m2"><p>هر روز تو گوش دار صفرایی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارت همه شب شراب‌پیمایی‌ها</p></div>
<div class="m2"><p>مکر و دغل و خصومت‌افزایی‌ها</p></div></div>