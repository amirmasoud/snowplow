---
title: >-
    رباعی شمارهٔ ۱۳۹۳
---
# رباعی شمارهٔ ۱۳۹۳

<div class="b" id="bn1"><div class="m1"><p>امشب منم و هزار صوفی پنهان</p></div>
<div class="m2"><p>مانندهٔ جان جمله نهانند و عیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عارف مطرب هله تقصیر مکن</p></div>
<div class="m2"><p>تا دریابی بدین صفت رقص‌کنان</p></div></div>