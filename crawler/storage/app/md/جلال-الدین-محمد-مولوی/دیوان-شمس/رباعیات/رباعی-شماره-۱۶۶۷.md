---
title: >-
    رباعی شمارهٔ ۱۶۶۷
---
# رباعی شمارهٔ ۱۶۶۷

<div class="b" id="bn1"><div class="m1"><p>از آب و گلی نیست بنای چو توئی</p></div>
<div class="m2"><p>یارب که چه هاست از برای چو توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نعره زنانی تو برای چو ویی</p></div>
<div class="m2"><p>لبیک کنانست برای چو توئی</p></div></div>