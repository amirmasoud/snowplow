---
title: >-
    رباعی شمارهٔ ۹۶۴
---
# رباعی شمارهٔ ۹۶۴

<div class="b" id="bn1"><div class="m1"><p>مردانه بیا که نیست کار تو مجاز</p></div>
<div class="m2"><p>آغاز بنه ترانهٔ بی‌آغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبلت میمال خواجهٔ شهر توئی</p></div>
<div class="m2"><p>آخر به گزاف نیست این ریش دراز</p></div></div>