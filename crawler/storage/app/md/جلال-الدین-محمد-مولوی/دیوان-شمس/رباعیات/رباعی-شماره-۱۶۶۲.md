---
title: >-
    رباعی شمارهٔ ۱۶۶۲
---
# رباعی شمارهٔ ۱۶۶۲

<div class="b" id="bn1"><div class="m1"><p>آن میوه توئی که نادر ایامی</p></div>
<div class="m2"><p>بتوان خوردن هزار من در خامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ما مپسند هجر و دشمن کامی</p></div>
<div class="m2"><p>کاخر به تو باز گردد این بدنامی</p></div></div>