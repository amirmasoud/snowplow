---
title: >-
    رباعی شمارهٔ ۴۱۶
---
# رباعی شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>من آن توام کام منت باید جست</p></div>
<div class="m2"><p>زیرا که در این شهر حدیث من و تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سخت کنی دل خود ار نرم کنی</p></div>
<div class="m2"><p>من از دل سخت تو نمیگردم سست</p></div></div>