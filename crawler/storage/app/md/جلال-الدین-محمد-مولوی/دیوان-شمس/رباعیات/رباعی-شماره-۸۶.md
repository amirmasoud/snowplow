---
title: >-
    رباعی شمارهٔ ۸۶
---
# رباعی شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تو یوسف منی من یعقوب</p></div>
<div class="m2"><p>ای آنکه تو صحت تنی من ایوب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود چه کسم ای همه را تو محبوب</p></div>
<div class="m2"><p>من دست همی‌زنم تو پائی میکوب</p></div></div>