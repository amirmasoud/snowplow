---
title: >-
    رباعی شمارهٔ ۱۰۵
---
# رباعی شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>علمی که ترا گره گشاید به طلب</p></div>
<div class="m2"><p>زان پیش که از تو جان برآید به طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نیست که هست مینماید بگذار</p></div>
<div class="m2"><p>آن هست که نیست مینماید به طلب</p></div></div>