---
title: >-
    رباعی شمارهٔ ۱۴۱۵
---
# رباعی شمارهٔ ۱۴۱۵

<div class="b" id="bn1"><div class="m1"><p>ای عادت تو خشم و جفا ورزیدن</p></div>
<div class="m2"><p>وز چشم تو شاید این سخن پرسیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینگونه که ابروی تو با چشم خوش است</p></div>
<div class="m2"><p>او را ز چه رو نمیتواند دیدن</p></div></div>