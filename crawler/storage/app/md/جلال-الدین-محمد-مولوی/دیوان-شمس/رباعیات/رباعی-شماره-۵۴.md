---
title: >-
    رباعی شمارهٔ ۵۴
---
# رباعی شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>عاشق همه سال مست و رسوا بادا</p></div>
<div class="m2"><p>دیوانه و شوریده و شیدا بادا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هشیاری غصهٔ هرچیز خوریم</p></div>
<div class="m2"><p>چون مست شویم هرچه بادا بادا</p></div></div>