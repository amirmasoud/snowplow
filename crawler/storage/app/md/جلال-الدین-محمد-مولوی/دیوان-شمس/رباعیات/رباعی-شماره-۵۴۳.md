---
title: >-
    رباعی شمارهٔ ۵۴۳
---
# رباعی شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>ای آنکه ز تو مشکلم آسان گردد</p></div>
<div class="m2"><p>سرو و گل و باغ مست احسان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل سرمست و خار بد مست و خمار</p></div>
<div class="m2"><p>جامی در ده که جمله یکسان گردد</p></div></div>