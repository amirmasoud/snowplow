---
title: >-
    رباعی شمارهٔ ۱۹۰۲
---
# رباعی شمارهٔ ۱۹۰۲

<div class="b" id="bn1"><div class="m1"><p>عالم سبز است و هر طرف بستانی</p></div>
<div class="m2"><p>از عکس جمال گل‌رخی خندانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سو گهریست مشتعل از کانی</p></div>
<div class="m2"><p>هر سو جانیست متصل با جانی</p></div></div>