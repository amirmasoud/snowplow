---
title: >-
    رباعی شمارهٔ ۶۸۲
---
# رباعی شمارهٔ ۶۸۲

<div class="b" id="bn1"><div class="m1"><p>در مغز فلک چو عشق تو جا گیرد</p></div>
<div class="m2"><p>تا عرش همه فتنه و غوغا گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روح شود جهان نه بالا و نه زیر</p></div>
<div class="m2"><p>چون عشق تو روح را ز بالا گیرد</p></div></div>