---
title: >-
    رباعی شمارهٔ ۱۸۱۲
---
# رباعی شمارهٔ ۱۸۱۲

<div class="b" id="bn1"><div class="m1"><p>تا چند ز جان مستمند اندیشی</p></div>
<div class="m2"><p>تا کی ز جهان پرگزند اندیشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه از تو ستد همین کالبد است</p></div>
<div class="m2"><p>یک مزبله گو مباش چند اندیشی</p></div></div>