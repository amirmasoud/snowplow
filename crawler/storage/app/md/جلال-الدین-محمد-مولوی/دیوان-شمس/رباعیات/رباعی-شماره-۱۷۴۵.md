---
title: >-
    رباعی شمارهٔ ۱۷۴۵
---
# رباعی شمارهٔ ۱۷۴۵

<div class="b" id="bn1"><div class="m1"><p>ای شمع تو صوفی صفتی پنداری</p></div>
<div class="m2"><p>کاین شش صفت از اهل صفا می‌داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبخیزی و نور چهره و زردی روی</p></div>
<div class="m2"><p>سوز دل و اشک دیده و بیداری</p></div></div>