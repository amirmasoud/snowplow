---
title: >-
    رباعی شمارهٔ ۱۴۰۳
---
# رباعی شمارهٔ ۱۴۰۳

<div class="b" id="bn1"><div class="m1"><p>ای در دو جهان یگانه تعجیل مکن</p></div>
<div class="m2"><p>در رفتن چون زمانه تعجیل مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگریز سوی کرانه تعجیل مکن</p></div>
<div class="m2"><p>از خانهٔ ما به خانه تعجیل مکن</p></div></div>