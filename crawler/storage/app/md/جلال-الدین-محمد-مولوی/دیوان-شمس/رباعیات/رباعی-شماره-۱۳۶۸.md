---
title: >-
    رباعی شمارهٔ ۱۳۶۸
---
# رباعی شمارهٔ ۱۳۶۸

<div class="b" id="bn1"><div class="m1"><p>هم منزل عشق و هم رهت می‌بینم</p></div>
<div class="m2"><p>در بنده و در مرو شهت می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اختر و خورشید و مهت می‌بینم</p></div>
<div class="m2"><p>در برگ و گیاه و درگهت می‌بینم</p></div></div>