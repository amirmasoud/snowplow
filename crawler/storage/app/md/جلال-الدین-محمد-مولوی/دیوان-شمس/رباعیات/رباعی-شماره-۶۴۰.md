---
title: >-
    رباعی شمارهٔ ۶۴۰
---
# رباعی شمارهٔ ۶۴۰

<div class="b" id="bn1"><div class="m1"><p>چون زیر افکند در عراق آمیزد</p></div>
<div class="m2"><p>دل عقل کند رها ز تن بگریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آتشم و چو درد می برخیزم</p></div>
<div class="m2"><p>هر آتش را که درد می‌برخیزد</p></div></div>