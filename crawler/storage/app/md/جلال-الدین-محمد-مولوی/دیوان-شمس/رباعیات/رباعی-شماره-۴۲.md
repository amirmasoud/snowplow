---
title: >-
    رباعی شمارهٔ ۴۲
---
# رباعی شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>چو نزود نبشته بود حق فرقت ما</p></div>
<div class="m2"><p>از بهر چه بود جنگ و آن وحشت ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بد بودیم رستی از زحمت ما</p></div>
<div class="m2"><p>ور نیک بدیم یاد کن صحبت ما</p></div></div>