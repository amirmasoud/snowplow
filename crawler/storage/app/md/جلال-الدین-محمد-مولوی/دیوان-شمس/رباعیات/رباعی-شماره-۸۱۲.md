---
title: >-
    رباعی شمارهٔ ۸۱۲
---
# رباعی شمارهٔ ۸۱۲

<div class="b" id="bn1"><div class="m1"><p>مائیم ز عشق یافته مرهم خود</p></div>
<div class="m2"><p>بر عشق نثار کرده هر دم دم خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هر دم ما حوصلهٔ عشق رود</p></div>
<div class="m2"><p>در هر دم ما عشق بیابد دم خود</p></div></div>