---
title: >-
    رباعی شمارهٔ ۱۳۳۳
---
# رباعی شمارهٔ ۱۳۳۳

<div class="b" id="bn1"><div class="m1"><p>من خاک ترا به چرخ اعظم ندهم</p></div>
<div class="m2"><p>یک ذره غمت بهر دو عالم ندهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش خود را نثار عالم کردم</p></div>
<div class="m2"><p>وز نقش تو من آب به آدم ندهم</p></div></div>