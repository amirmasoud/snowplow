---
title: >-
    رباعی شمارهٔ ۹۵۸
---
# رباعی شمارهٔ ۹۵۸

<div class="b" id="bn1"><div class="m1"><p>گر بکشندم نگردم از عشق توباز</p></div>
<div class="m2"><p>زیرا که ز چنگ ما برون شد آواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند مرا سرت ببریم به گاز</p></div>
<div class="m2"><p>پیراهن عمر خود چه کوته چه دراز</p></div></div>