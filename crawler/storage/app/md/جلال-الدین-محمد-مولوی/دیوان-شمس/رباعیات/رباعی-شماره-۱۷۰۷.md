---
title: >-
    رباعی شمارهٔ ۱۷۰۷
---
# رباعی شمارهٔ ۱۷۰۷

<div class="b" id="bn1"><div class="m1"><p>ای باده تو باشی که همه داد کنی</p></div>
<div class="m2"><p>صد بنده به یک صبوح آزاد کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم به تو روشنست همچون خورشید</p></div>
<div class="m2"><p>هم در تو گریزم که توام شاد کنی</p></div></div>