---
title: >-
    رباعی شمارهٔ ۴۰۳
---
# رباعی شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>ما را بدم پیر نگه نتوان داشت</p></div>
<div class="m2"><p>در خانهٔ دلگبر نگه نتوان داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنرا که سر زلف چو زنجیر بود</p></div>
<div class="m2"><p>در خانه به زنجیر نگه نتوان داشت</p></div></div>