---
title: >-
    رباعی شمارهٔ ۵۵۴
---
# رباعی شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>ای دوست مگو تو بنده‌ای یا آزاد</p></div>
<div class="m2"><p>بنده که خرد برای زشتی و فساد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دست برآورده ترا دست که داد</p></div>
<div class="m2"><p>بگزار مراد خویش کاوراست مراد</p></div></div>