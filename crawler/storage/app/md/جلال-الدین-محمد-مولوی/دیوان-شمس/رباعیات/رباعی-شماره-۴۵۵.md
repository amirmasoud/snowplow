---
title: >-
    رباعی شمارهٔ ۴۵۵
---
# رباعی شمارهٔ ۴۵۵

<div class="b" id="bn1"><div class="m1"><p>یکبار به مردم و مرا کس نگریست</p></div>
<div class="m2"><p>گر بار دگر زنده شوم دانم زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کرده تو قصد من ترا با من چیست</p></div>
<div class="m2"><p>یا صحبت ابلهان همه دیگ تهیست</p></div></div>