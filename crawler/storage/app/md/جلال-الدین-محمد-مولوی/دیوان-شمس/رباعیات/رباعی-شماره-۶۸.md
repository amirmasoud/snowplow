---
title: >-
    رباعی شمارهٔ ۶۸
---
# رباعی شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>ما اطیب ما الذما احلانا</p></div>
<div class="m2"><p>کنا مهجا ولم نکن ابدانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این شأبنا کرامة مولانا</p></div>
<div class="m2"><p>یعفو و یعیدنا کما ابدنا</p></div></div>