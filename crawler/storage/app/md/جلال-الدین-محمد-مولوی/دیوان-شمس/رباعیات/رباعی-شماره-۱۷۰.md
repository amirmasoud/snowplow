---
title: >-
    رباعی شمارهٔ ۱۷۰
---
# رباعی شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>امشب منم و طواف کاشانهٔ دوست</p></div>
<div class="m2"><p>میگردم تا بصبح در خانهٔ دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که بهر صبوح موسوم شده است</p></div>
<div class="m2"><p>کاین کاسهٔ سر بدست پیمانهٔ اوست</p></div></div>