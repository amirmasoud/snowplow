---
title: >-
    رباعی شمارهٔ ۹۴۴
---
# رباعی شمارهٔ ۹۴۴

<div class="b" id="bn1"><div class="m1"><p>امشب که گشاده است صنم با ما راز</p></div>
<div class="m2"><p>ای شب چه شبی که عمر تو باد دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاغان سیاه امشب اندر طربند</p></div>
<div class="m2"><p>با باز سپید جان شده در پرواز</p></div></div>