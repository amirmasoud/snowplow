---
title: >-
    رباعی شمارهٔ ۱۷۲۰
---
# رباعی شمارهٔ ۱۷۲۰

<div class="b" id="bn1"><div class="m1"><p>ای خواجه گنه مکن که بدنام شوی</p></div>
<div class="m2"><p>گر خاص توئی گنه کنی عام شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رهگذرت دام نهاده است ابلیس</p></div>
<div class="m2"><p>بدکار مباش زانکه در دام شوی</p></div></div>