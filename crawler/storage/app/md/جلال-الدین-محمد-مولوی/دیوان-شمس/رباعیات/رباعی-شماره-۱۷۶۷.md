---
title: >-
    رباعی شمارهٔ ۱۷۶۷
---
# رباعی شمارهٔ ۱۷۶۷

<div class="b" id="bn1"><div class="m1"><p>ای آنکه به جز شادی و جز نور نه‌ای</p></div>
<div class="m2"><p>چون نعره زنم که از برم دور نه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند نمک‌های جهان از لب تست</p></div>
<div class="m2"><p>لیکن چکنم چو اندر این شور نه‌ای</p></div></div>