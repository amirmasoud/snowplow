---
title: >-
    رباعی شمارهٔ ۷۵۵
---
# رباعی شمارهٔ ۷۵۵

<div class="b" id="bn1"><div class="m1"><p>عارف چو گل و جز گل خندان نبود</p></div>
<div class="m2"><p>تلخی نکند عادت قند آن نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصباح زجاجه است جان عارف</p></div>
<div class="m2"><p>پس شیشه بود زجاجه سندان نبود</p></div></div>