---
title: >-
    رباعی شمارهٔ ۱۲۲۶
---
# رباعی شمارهٔ ۱۲۲۶

<div class="b" id="bn1"><div class="m1"><p>در هر فلکی مردمکی می‌بینم</p></div>
<div class="m2"><p>هر مردمکش را فلکی می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای احول اگر یکی دو می‌بینی تو</p></div>
<div class="m2"><p>بر عکس تو من دو را یکی می‌بینم</p></div></div>