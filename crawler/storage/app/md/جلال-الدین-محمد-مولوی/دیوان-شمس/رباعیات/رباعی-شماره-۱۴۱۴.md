---
title: >-
    رباعی شمارهٔ ۱۴۱۴
---
# رباعی شمارهٔ ۱۴۱۴

<div class="b" id="bn1"><div class="m1"><p>ای شاه تو مات گشته را مات مکن</p></div>
<div class="m2"><p>افتادهٔ توست جز مراعات مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر غرقهٔ جرم است مجازات مکن</p></div>
<div class="m2"><p>از بهر خدا قصد مکافات مکن</p></div></div>