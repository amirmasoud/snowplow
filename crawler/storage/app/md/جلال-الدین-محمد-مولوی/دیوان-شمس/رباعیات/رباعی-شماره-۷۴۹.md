---
title: >-
    رباعی شمارهٔ ۷۴۹
---
# رباعی شمارهٔ ۷۴۹

<div class="b" id="bn1"><div class="m1"><p>صبح آمد و وقت روشنائی آمد</p></div>
<div class="m2"><p>شبخیزان را دم جدائی آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چشم چو پاسبان فروبست بخواب</p></div>
<div class="m2"><p>وقت هوس شکر ربائی آمد</p></div></div>