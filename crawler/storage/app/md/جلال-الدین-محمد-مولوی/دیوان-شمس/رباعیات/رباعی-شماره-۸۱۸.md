---
title: >-
    رباعی شمارهٔ ۸۱۸
---
# رباعی شمارهٔ ۸۱۸

<div class="b" id="bn1"><div class="m1"><p>مرغی ملکی زانسوی گردون بپرد</p></div>
<div class="m2"><p>آن سوی که سوی نیست بیچون بپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مرغ که از بیضهٔ سیمرغ بزاد</p></div>
<div class="m2"><p>جز جانب سیمرغ بگو چون بپرد</p></div></div>