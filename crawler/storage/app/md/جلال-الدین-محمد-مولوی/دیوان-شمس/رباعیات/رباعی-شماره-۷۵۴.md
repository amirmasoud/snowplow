---
title: >-
    رباعی شمارهٔ ۷۵۴
---
# رباعی شمارهٔ ۷۵۴

<div class="b" id="bn1"><div class="m1"><p>طاوس نه‌ای که بر جمالت نگرند</p></div>
<div class="m2"><p>سیمرغ نه‌ای که بیتو نام تو برند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهباز نه‌ای که از شکار تو چرند</p></div>
<div class="m2"><p>آخر تو چه مرغی و ترا با چه خرند</p></div></div>