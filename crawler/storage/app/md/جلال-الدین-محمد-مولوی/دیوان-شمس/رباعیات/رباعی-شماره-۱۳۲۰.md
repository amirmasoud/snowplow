---
title: >-
    رباعی شمارهٔ ۱۳۲۰
---
# رباعی شمارهٔ ۱۳۲۰

<div class="b" id="bn1"><div class="m1"><p>مائیم که بی‌قماش و بی‌سیم خوشیم</p></div>
<div class="m2"><p>در رنج مرفهیم و در بیم خوشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دور ابد از می تسلیم خوشیم</p></div>
<div class="m2"><p>تا ظن نبری که ما چو تونیم خوشیم</p></div></div>