---
title: >-
    رباعی شمارهٔ ۴۳
---
# رباعی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>خود را به خیل درافکنم مست آنجا</p></div>
<div class="m2"><p>تا بنگرم آن جان جهان هست آنجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا پای رساندم به مقصود و مراد</p></div>
<div class="m2"><p>یا سر بدهم همچو دل از دست آنجا</p></div></div>