---
title: >-
    رباعی شمارهٔ ۱۷۶۹
---
# رباعی شمارهٔ ۱۷۶۹

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تو بر فلک وطن داشته‌ای</p></div>
<div class="m2"><p>خود را ز جهان پاک پنداشته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک تو نقش خویش بنگاشته‌ای</p></div>
<div class="m2"><p>وان چیز که اصل تست بگذاشته‌ای</p></div></div>