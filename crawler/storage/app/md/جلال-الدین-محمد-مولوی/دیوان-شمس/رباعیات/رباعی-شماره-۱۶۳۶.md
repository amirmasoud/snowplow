---
title: >-
    رباعی شمارهٔ ۱۶۳۶
---
# رباعی شمارهٔ ۱۶۳۶

<div class="b" id="bn1"><div class="m1"><p>گفتم که توئی می و منم پیمانه</p></div>
<div class="m2"><p>من مرده‌ام و تو جانی و جانانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون بگشا در وفا گفت خموش</p></div>
<div class="m2"><p>دیوانه کسی رها کند در خانه</p></div></div>