---
title: >-
    رباعی شمارهٔ ۵۰۲
---
# رباعی شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>آنها که به کوی عارفان افتادند</p></div>
<div class="m2"><p>با نفخهٔ صور چابک و دلشادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قومی به فدای نفس تن در دادند</p></div>
<div class="m2"><p>قومی ز خود و جهان و جان آزادند</p></div></div>