---
title: >-
    رباعی شمارهٔ ۷۰۶
---
# رباعی شمارهٔ ۷۰۶

<div class="b" id="bn1"><div class="m1"><p>دی چشم تو رای سحر مطلق میزد</p></div>
<div class="m2"><p>روی تو ره گنبد از رق میزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا داشتی آفتاب در سایهٔ زلف</p></div>
<div class="m2"><p>جان بر صفت ذره معلق میزد</p></div></div>