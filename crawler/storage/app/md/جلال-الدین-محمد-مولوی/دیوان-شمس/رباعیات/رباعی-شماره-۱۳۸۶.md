---
title: >-
    رباعی شمارهٔ ۱۳۸۶
---
# رباعی شمارهٔ ۱۳۸۶

<div class="b" id="bn1"><div class="m1"><p>از بسکه برآورد غمت آه از من</p></div>
<div class="m2"><p>ترسم که شود به کام بدخواه از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردا که ز هجران تو ای جان جهان</p></div>
<div class="m2"><p>خون شد دلم و دلت نه آگاه از من</p></div></div>