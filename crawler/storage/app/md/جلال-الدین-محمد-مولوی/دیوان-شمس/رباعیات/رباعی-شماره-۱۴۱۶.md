---
title: >-
    رباعی شمارهٔ ۱۴۱۶
---
# رباعی شمارهٔ ۱۴۱۶

<div class="b" id="bn1"><div class="m1"><p>ای عادت عشق عین ایمان خوردن</p></div>
<div class="m2"><p>نی غصهٔ نان و غصهٔ جان خوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مائده چون زر و زو شب بیرونست</p></div>
<div class="m2"><p>روزه چه بود صلای پنهان خوردن</p></div></div>