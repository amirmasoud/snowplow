---
title: >-
    رباعی شمارهٔ ۳۵۵
---
# رباعی شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>شمشیر ازل بدست مردان خداست</p></div>
<div class="m2"><p>گوی ابدی در خم چوگان خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن تن که چو کوه طور روشن آید</p></div>
<div class="m2"><p>نور خود از او طلب که او کان خداست</p></div></div>