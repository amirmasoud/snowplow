---
title: >-
    رباعی شمارهٔ ۱۸۹۵
---
# رباعی شمارهٔ ۱۸۹۵

<div class="b" id="bn1"><div class="m1"><p>سوگند همی خورد پریر آن ساقی</p></div>
<div class="m2"><p>می‌گفت به حق صحبت مشتاقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باده دهم به شهری و آفاقی</p></div>
<div class="m2"><p>عقلی نگذارم به جهان من باقی</p></div></div>