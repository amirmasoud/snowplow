---
title: >-
    رباعی شمارهٔ ۱۱۲
---
# رباعی شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>یاری کن و یار باش ای یار مخسب</p></div>
<div class="m2"><p>ای بلبل سرمست به گلزار مخسب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران غریب را نگهدار مخسب</p></div>
<div class="m2"><p>امشب شب بخشش است زنهار مخسب</p></div></div>