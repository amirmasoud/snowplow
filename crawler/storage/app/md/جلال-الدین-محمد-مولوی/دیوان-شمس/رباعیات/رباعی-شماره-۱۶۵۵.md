---
title: >-
    رباعی شمارهٔ ۱۶۵۵
---
# رباعی شمارهٔ ۱۶۵۵

<div class="b" id="bn1"><div class="m1"><p>آن خوش باشد که صاحب تمییزی</p></div>
<div class="m2"><p>بی‌آنکه بگویند و بگوید چیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌گفت و تقاضا برسد مهمانرا</p></div>
<div class="m2"><p>تروندهٔ خوش ز صاحب پالیزی</p></div></div>