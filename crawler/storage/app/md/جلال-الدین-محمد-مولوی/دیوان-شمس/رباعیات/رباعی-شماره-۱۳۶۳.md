---
title: >-
    رباعی شمارهٔ ۱۳۶۳
---
# رباعی شمارهٔ ۱۳۶۳

<div class="b" id="bn1"><div class="m1"><p>نی سخرهٔ آسمان پیروزه شوم</p></div>
<div class="m2"><p>نی شیفتهٔ شاهد ده روزه شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در روزه چو روزی ده بیواسطه‌ای</p></div>
<div class="m2"><p>پس حلقه بگوش و بندهٔ روزه شوم</p></div></div>