---
title: >-
    رباعی شمارهٔ ۱۵۸۳
---
# رباعی شمارهٔ ۱۵۸۳

<div class="b" id="bn1"><div class="m1"><p>گه در دل ما نشین چو اسرار و مرو</p></div>
<div class="m2"><p>گه بر سر ما نشین چو دستار و مرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که چو دل زود روم زود آیم</p></div>
<div class="m2"><p>عشوه مده ای دلبر عیار و مرو</p></div></div>