---
title: >-
    رباعی شمارهٔ ۵۰۱
---
# رباعی شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>آنها که بتش خزان سوخته‌اند</p></div>
<div class="m2"><p>وز لطف بهار چشمشان دوخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون همه را خلعت تو دوخته‌اند</p></div>
<div class="m2"><p>شیوه‌گری و غنج درآموخته‌اند</p></div></div>