---
title: >-
    رباعی شمارهٔ ۱۵۹۲
---
# رباعی شمارهٔ ۱۵۹۲

<div class="b" id="bn1"><div class="m1"><p>آمد بر من خیال جانان ز پگه</p></div>
<div class="m2"><p>در کف قدح باده که بستان ز پگه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درکش این جام تا به پایان ز پگه</p></div>
<div class="m2"><p>سرمست درآ میان مستان ز پگه</p></div></div>