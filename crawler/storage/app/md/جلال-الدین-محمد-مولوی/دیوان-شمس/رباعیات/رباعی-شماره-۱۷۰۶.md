---
title: >-
    رباعی شمارهٔ ۱۷۰۶
---
# رباعی شمارهٔ ۱۷۰۶

<div class="b" id="bn1"><div class="m1"><p>ای باد سحر تو از سر نیکوئی</p></div>
<div class="m2"><p>شاید که حکایتم به آن مه گوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی غلطم گرت بدوره بودی</p></div>
<div class="m2"><p>پس گرد جهان دگر کرا میجوئی</p></div></div>