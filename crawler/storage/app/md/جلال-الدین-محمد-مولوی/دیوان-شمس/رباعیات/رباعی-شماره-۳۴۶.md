---
title: >-
    رباعی شمارهٔ ۳۴۶
---
# رباعی شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>سر سخن دوست نمیارم گفت</p></div>
<div class="m2"><p>دریست گرانبها نمیارم سفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که بخواب دربگویم سخنی</p></div>
<div class="m2"><p>شبهاست که از بیم نمیارم خفت</p></div></div>