---
title: >-
    رباعی شمارهٔ ۱۹۵۱
---
# رباعی شمارهٔ ۱۹۵۱

<div class="b" id="bn1"><div class="m1"><p>مائیم در این زمان زمین پیمائی</p></div>
<div class="m2"><p>بگذاشته هر شهر به شهر آرائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کشتی یاوه گشته در دریائی</p></div>
<div class="m2"><p>هر روز به منزلی و هرشب جائی</p></div></div>