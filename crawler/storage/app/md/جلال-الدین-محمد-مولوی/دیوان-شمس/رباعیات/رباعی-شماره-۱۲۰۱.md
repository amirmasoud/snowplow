---
title: >-
    رباعی شمارهٔ ۱۲۰۱
---
# رباعی شمارهٔ ۱۲۰۱

<div class="b" id="bn1"><div class="m1"><p>تا کاسهٔ دوغ خویش باشد پیشم</p></div>
<div class="m2"><p>والله که به انگبین کس نندیشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور بی‌برگی به مرگ مالد گوشم</p></div>
<div class="m2"><p>آزادی را به بندگی نفروشم</p></div></div>