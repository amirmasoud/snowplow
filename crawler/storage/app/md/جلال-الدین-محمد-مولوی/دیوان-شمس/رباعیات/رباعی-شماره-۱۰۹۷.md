---
title: >-
    رباعی شمارهٔ ۱۰۹۷
---
# رباعی شمارهٔ ۱۰۹۷

<div class="b" id="bn1"><div class="m1"><p>در عشق نوا جزو زند آنگه کل</p></div>
<div class="m2"><p>در باغ نخست غوره است آنگه مل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینست دلا قاعده در فصل بهار</p></div>
<div class="m2"><p>در بانگ شود گربه و آنگه بلبل</p></div></div>