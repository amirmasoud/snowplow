---
title: >-
    رباعی شمارهٔ ۳۰۵
---
# رباعی شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>در راه طلب عاقل و دیوانه یکیست</p></div>
<div class="m2"><p>در شیوهٔ عشق خویش و بیگانه یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که شراب وصل جانان دادند</p></div>
<div class="m2"><p>در مذهب او کعبه و بتخانه یکیست</p></div></div>