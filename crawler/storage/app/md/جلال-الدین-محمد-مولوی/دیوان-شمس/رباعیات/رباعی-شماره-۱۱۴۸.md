---
title: >-
    رباعی شمارهٔ ۱۱۴۸
---
# رباعی شمارهٔ ۱۱۴۸

<div class="b" id="bn1"><div class="m1"><p>امشب که شراب جان مدامست مدام</p></div>
<div class="m2"><p>ساقی شه و باده با قوامست قوام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسباب طرب جمله تمامست تمام</p></div>
<div class="m2"><p>ای زنده‌دلان خواب حرامست حرام</p></div></div>