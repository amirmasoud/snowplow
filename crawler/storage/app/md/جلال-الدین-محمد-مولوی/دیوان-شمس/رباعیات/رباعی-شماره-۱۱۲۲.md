---
title: >-
    رباعی شمارهٔ ۱۱۲۲
---
# رباعی شمارهٔ ۱۱۲۲

<div class="b" id="bn1"><div class="m1"><p>از بهر تو صد بار ملامت بکشم</p></div>
<div class="m2"><p>گر بشکنم این عهد غرامت بکشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عمر وفا کند جفاهای ترا</p></div>
<div class="m2"><p>در دل دارم که تا قیامت بکشم</p></div></div>