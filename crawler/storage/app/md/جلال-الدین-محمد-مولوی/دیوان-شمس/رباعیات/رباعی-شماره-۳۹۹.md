---
title: >-
    رباعی شمارهٔ ۳۹۹
---
# رباعی شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>گویند که عشق عاقبت تسکین است</p></div>
<div class="m2"><p>اول شور است و عاقبت تمکین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانست ز آسیاش سنگ زیرین</p></div>
<div class="m2"><p>این صورت بی‌قرار بالایین است</p></div></div>