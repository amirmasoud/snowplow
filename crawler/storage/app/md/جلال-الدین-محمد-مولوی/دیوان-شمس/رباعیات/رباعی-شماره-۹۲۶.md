---
title: >-
    رباعی شمارهٔ ۹۲۶
---
# رباعی شمارهٔ ۹۲۶

<div class="b" id="bn1"><div class="m1"><p>آمد آمد آنکه نرفت او هرگز</p></div>
<div class="m2"><p>بیرون نبد آن آب از این جو هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او نافهٔ مشک و ما همه بوی وئیم</p></div>
<div class="m2"><p>از نافه شنیده‌ای جدا بو هرگز</p></div></div>