---
title: >-
    رباعی شمارهٔ ۶۵
---
# رباعی شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>گویم که کیست روح‌افزا مرا</p></div>
<div class="m2"><p>آنکس که بداد جان ز آغاز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه چشم مرا چو باز بر می‌بندد</p></div>
<div class="m2"><p>گه بگشاید به صید چون باز مرا</p></div></div>