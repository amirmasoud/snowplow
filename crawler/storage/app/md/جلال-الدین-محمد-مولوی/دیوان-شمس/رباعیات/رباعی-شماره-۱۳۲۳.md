---
title: >-
    رباعی شمارهٔ ۱۳۲۳
---
# رباعی شمارهٔ ۱۳۲۳

<div class="b" id="bn1"><div class="m1"><p>مائیم که دوست خویش دشمن داریم</p></div>
<div class="m2"><p>اما دشمن هر عاشق و هر بیداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قاصد دشمنان خود یاریم</p></div>
<div class="m2"><p>ما دامن خود همیشه در خون داریم</p></div></div>