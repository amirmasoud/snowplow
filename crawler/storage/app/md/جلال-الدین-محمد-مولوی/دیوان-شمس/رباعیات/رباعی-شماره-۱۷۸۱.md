---
title: >-
    رباعی شمارهٔ ۱۷۸۱
---
# رباعی شمارهٔ ۱۷۸۱

<div class="b" id="bn1"><div class="m1"><p>بازآی که تا به خود نیازم بینی</p></div>
<div class="m2"><p>بیداری شبهای درازم بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی غلطم که خود فراق تو مرا</p></div>
<div class="m2"><p>کی زنده رها کند که بازم بینی</p></div></div>