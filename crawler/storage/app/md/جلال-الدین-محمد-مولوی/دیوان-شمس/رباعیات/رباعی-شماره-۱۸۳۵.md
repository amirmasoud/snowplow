---
title: >-
    رباعی شمارهٔ ۱۸۳۵
---
# رباعی شمارهٔ ۱۸۳۵

<div class="b" id="bn1"><div class="m1"><p>چون ساز کند عدم حیات افزائی</p></div>
<div class="m2"><p>گیری ز عدم لقمه و خوش می‌خائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در می‌رسدت طبق طبق حلواها</p></div>
<div class="m2"><p>آنجا نه دکان پدید و نه حلوائی</p></div></div>