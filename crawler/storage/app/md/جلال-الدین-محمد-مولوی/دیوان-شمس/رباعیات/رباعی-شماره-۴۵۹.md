---
title: >-
    رباعی شمارهٔ ۴۵۹
---
# رباعی شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>اندر سر من نبود جز رای صلاح</p></div>
<div class="m2"><p>اندر شب و روز پاک جویای صلاح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امسال چنانم که نیارم گفتن</p></div>
<div class="m2"><p>یک سال دگر وای مرا وای صلاح</p></div></div>