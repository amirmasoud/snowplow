---
title: >-
    رباعی شمارهٔ ۴۷۹
---
# رباعی شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>آن روز که چشم تو ز من برگردد</p></div>
<div class="m2"><p>وز بهر تو کشتنم میسر گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غصهٔ آنم که چه خواهم عذرت</p></div>
<div class="m2"><p>گر چشم تو در ماتم من تر گردد</p></div></div>