---
title: >-
    رباعی شمارهٔ ۱۸۲۱
---
# رباعی شمارهٔ ۱۸۲۱

<div class="b" id="bn1"><div class="m1"><p>تو دوش چه خواب دیده‌ای می‌دانی</p></div>
<div class="m2"><p>نی دانش آن نیست بدین آسانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دست و تن تو کاله پنهان کرده است</p></div>
<div class="m2"><p>ای شحنه چراش زو نمی‌رنجانی</p></div></div>