---
title: >-
    رباعی شمارهٔ ۱۸۴۷
---
# رباعی شمارهٔ ۱۸۴۷

<div class="b" id="bn1"><div class="m1"><p>خود را چو دمی ز یار محرم یابی</p></div>
<div class="m2"><p>در عمر نصیب خویش آن دم یابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار که ضایع نکنی آن دم را</p></div>
<div class="m2"><p>زیرا که دگر چنان دمی کم‌یابی</p></div></div>