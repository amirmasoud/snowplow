---
title: >-
    رباعی شمارهٔ ۹۰
---
# رباعی شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>این باد سحر محرم رازست مخسب</p></div>
<div class="m2"><p>هنگام تفرع و نیاز است مخسب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خلق دو کون از ازل تا به ابد</p></div>
<div class="m2"><p>این در که نبسته است باز است مخسب</p></div></div>