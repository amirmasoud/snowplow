---
title: >-
    رباعی شمارهٔ ۱۷۳۵
---
# رباعی شمارهٔ ۱۷۳۵

<div class="b" id="bn1"><div class="m1"><p>ای دوست بهر سخن در جنگ زنی</p></div>
<div class="m2"><p>صد تیر جفا بر من دلتنگ زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم تو من مسم دگر کس زر سرخ</p></div>
<div class="m2"><p>فردا بنمایمت چو بر سنگ زنی</p></div></div>