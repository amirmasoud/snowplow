---
title: >-
    رباعی شمارهٔ ۱۳۰۳
---
# رباعی شمارهٔ ۱۳۰۳

<div class="b" id="bn1"><div class="m1"><p>لیلم که نهاری نکند من چکنم</p></div>
<div class="m2"><p>بختم که سواری نکند من چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که به دولتی جهانرا بخورم</p></div>
<div class="m2"><p>اقبال چو یاری نکند من چکنم</p></div></div>