---
title: >-
    رباعی شمارهٔ ۸۷۹
---
# رباعی شمارهٔ ۸۷۹

<div class="b" id="bn1"><div class="m1"><p>ای ظل تو از سایهٔ طوبی خوشتر</p></div>
<div class="m2"><p>ای رنج تو از راحت عقبی خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از رخ بندهٔ معنی بودم</p></div>
<div class="m2"><p>ای نقش تو از هزار معنی خوشتر</p></div></div>