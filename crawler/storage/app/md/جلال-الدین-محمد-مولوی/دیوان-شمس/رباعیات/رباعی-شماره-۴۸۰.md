---
title: >-
    رباعی شمارهٔ ۴۸۰
---
# رباعی شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>آن روز که روز ابر و باران باشد</p></div>
<div class="m2"><p>شرط است که جمعیت یاران باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانروی که روییار را تازه کند</p></div>
<div class="m2"><p>چون مجمع گل که در بهاران باشد</p></div></div>