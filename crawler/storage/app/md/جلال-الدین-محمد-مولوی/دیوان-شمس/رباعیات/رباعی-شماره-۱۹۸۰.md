---
title: >-
    رباعی شمارهٔ ۱۹۸۰
---
# رباعی شمارهٔ ۱۹۸۰

<div class="b" id="bn1"><div class="m1"><p>هر روز ز عاشقی و شیرین رائی</p></div>
<div class="m2"><p>مر عاشق را پیرهنی فرمائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یوسف روزگار ما یعقوبیم</p></div>
<div class="m2"><p>پیراهن تست چشم را بینائی</p></div></div>