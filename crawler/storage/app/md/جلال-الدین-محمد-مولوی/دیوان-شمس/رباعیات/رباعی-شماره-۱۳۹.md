---
title: >-
    رباعی شمارهٔ ۱۳۹
---
# رباعی شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>آن عشق مجرد سوی صحرا می‌تاخت</p></div>
<div class="m2"><p>دیدش دل من ز کر و فرش بشناخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خود می‌گفت چون ز صورت برهم</p></div>
<div class="m2"><p>با صورت عشق عشقها خواهم باخت</p></div></div>