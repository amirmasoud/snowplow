---
title: >-
    رباعی شمارهٔ ۹۷۵
---
# رباعی شمارهٔ ۹۷۵

<div class="b" id="bn1"><div class="m1"><p>احوال دلم هر سحر از باد بپرس</p></div>
<div class="m2"><p>تا شاد شوی از من ناشاد بپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور کشتن بیگناه سودات شود</p></div>
<div class="m2"><p>از چشم خود آن جادوی استاد بپرس</p></div></div>