---
title: >-
    رباعی شمارهٔ ۹۹۱
---
# رباعی شمارهٔ ۹۹۱

<div class="b" id="bn1"><div class="m1"><p>آن دل که من آن خویش پنداشتمش</p></div>
<div class="m2"><p>بالله بر هیچ دوست نگذاشتمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذاشت بتا مرا و آمد بر تو</p></div>
<div class="m2"><p>نیکو دارش که من نکو داشتمش</p></div></div>