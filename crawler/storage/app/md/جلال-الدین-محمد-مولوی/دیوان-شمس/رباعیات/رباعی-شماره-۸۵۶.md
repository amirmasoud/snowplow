---
title: >-
    رباعی شمارهٔ ۸۵۶
---
# رباعی شمارهٔ ۸۵۶

<div class="b" id="bn1"><div class="m1"><p>هل تا برود سرش به دیوار آید</p></div>
<div class="m2"><p>سر بشکند و جامه به خون آلاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آید بر من سوزن و انگشت گزان</p></div>
<div class="m2"><p>کان گفته سخنهای منش یاد آید</p></div></div>