---
title: >-
    رباعی شمارهٔ ۷۷۵
---
# رباعی شمارهٔ ۷۷۵

<div class="b" id="bn1"><div class="m1"><p>قد الفم ز مشق چون جیم افتاد</p></div>
<div class="m2"><p>آن سو که تویی حسن دو میم افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن خوبی باقی تو ای جان جهان</p></div>
<div class="m2"><p>دل بستد و اندر پی باقیم افتاد</p></div></div>