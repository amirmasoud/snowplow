---
title: >-
    رباعی شمارهٔ ۱۵۲۱
---
# رباعی شمارهٔ ۱۵۲۱

<div class="b" id="bn1"><div class="m1"><p>هر مطرب کو نیست ز دل دفتر خوان</p></div>
<div class="m2"><p>آن مطرب را تو مطرب دفتر خوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چهرهٔ نهان کرد ز تو بیت و غزل</p></div>
<div class="m2"><p>گر خط خوانی ز چهرهٔ ما برخوان</p></div></div>