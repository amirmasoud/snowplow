---
title: >-
    رباعی شمارهٔ ۵۷۹
---
# رباعی شمارهٔ ۵۷۹

<div class="b" id="bn1"><div class="m1"><p>پرسیدم از آن کسی که برهان داند</p></div>
<div class="m2"><p>کان کیست که او حقیقت جان داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش خوش به جواب گفت کای سودائی</p></div>
<div class="m2"><p>این منطق طیر است سلیمان داند</p></div></div>