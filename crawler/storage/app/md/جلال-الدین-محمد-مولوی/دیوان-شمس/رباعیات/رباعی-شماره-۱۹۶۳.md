---
title: >-
    رباعی شمارهٔ ۱۹۶۳
---
# رباعی شمارهٔ ۱۹۶۳

<div class="b" id="bn1"><div class="m1"><p>من دوش به خواب در بدیدم قمری</p></div>
<div class="m2"><p>دریا صفتی عجایبی سیم‌بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز بگرد هر دری میگردم</p></div>
<div class="m2"><p>کز یارک دوشینه چه دارد خبری</p></div></div>