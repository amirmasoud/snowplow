---
title: >-
    رباعی شمارهٔ ۱۵۳۲
---
# رباعی شمارهٔ ۱۵۳۲

<div class="b" id="bn1"><div class="m1"><p>آن کس که همیشه دل پر از دردم از او</p></div>
<div class="m2"><p>با سینهٔ ریش و با رخ زردم از او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز بناز او بری بر من زد</p></div>
<div class="m2"><p>المنة لله که بری خوردم از او</p></div></div>