---
title: >-
    رباعی شمارهٔ ۱۰۰۲
---
# رباعی شمارهٔ ۱۰۰۲

<div class="b" id="bn1"><div class="m1"><p>ای چشم بیا دامن خود در خون کش</p></div>
<div class="m2"><p>وی روح برو قماش بر گردون کش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لعل لبت هر آنکه انگشت نهاد</p></div>
<div class="m2"><p>مندبس و زبانش از قفا بیرون کش</p></div></div>