---
title: >-
    رباعی شمارهٔ ۱۶۵۱
---
# رباعی شمارهٔ ۱۶۵۱

<div class="b" id="bn1"><div class="m1"><p>یارب تو مرا به نفس طناز مده</p></div>
<div class="m2"><p>با هر چه به جز تست مرا ساز مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در تو گریزان شدم از فتنهٔ خویش</p></div>
<div class="m2"><p>من آن توام مرا به من باز مده</p></div></div>