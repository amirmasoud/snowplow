---
title: >-
    رباعی شمارهٔ ۱۷۰۵
---
# رباعی شمارهٔ ۱۷۰۵

<div class="b" id="bn1"><div class="m1"><p>ای باد سحر به کوی آن سلسله موی</p></div>
<div class="m2"><p>احوال دلم بگوی اگر یابی روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور زانکه ترا ز دل نباشد دلجوی</p></div>
<div class="m2"><p>زنهار مرا ندیده‌ای هیچ مگوی</p></div></div>