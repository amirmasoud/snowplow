---
title: >-
    رباعی شمارهٔ ۱۶۱۷
---
# رباعی شمارهٔ ۱۶۱۷

<div class="b" id="bn1"><div class="m1"><p>بیگاه شد و دل نرهید از ناله</p></div>
<div class="m2"><p>روزی نتوان گفت غم صد ساله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان جهان غصهٔ بیگاه شدن</p></div>
<div class="m2"><p>آنکس داند که گم شدش گوساله</p></div></div>