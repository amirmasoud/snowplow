---
title: >-
    رباعی شمارهٔ ۸۶۸
---
# رباعی شمارهٔ ۸۶۸

<div class="b" id="bn1"><div class="m1"><p>آن کس که ترا دیده بود ای دلبر</p></div>
<div class="m2"><p>او چون نگرد بسوی معشوق دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده هر آنکه کرد سوی تو نظر</p></div>
<div class="m2"><p>تاریک نماید به خدا شمس و قمر</p></div></div>