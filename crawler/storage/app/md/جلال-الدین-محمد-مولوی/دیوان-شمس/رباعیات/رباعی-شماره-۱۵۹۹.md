---
title: >-
    رباعی شمارهٔ ۱۵۹۹
---
# رباعی شمارهٔ ۱۵۹۹

<div class="b" id="bn1"><div class="m1"><p>آهوی قمرا سهامه عیناه</p></div>
<div class="m2"><p>ما شوش عزم خاطری الا هو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روحی تلفت و مهجتی تهواه</p></div>
<div class="m2"><p>قلبی ابدا یقون یا هویا هو</p></div></div>