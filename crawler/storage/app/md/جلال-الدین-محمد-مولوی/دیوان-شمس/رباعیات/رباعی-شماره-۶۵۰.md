---
title: >-
    رباعی شمارهٔ ۶۵۰
---
# رباعی شمارهٔ ۶۵۰

<div class="b" id="bn1"><div class="m1"><p>خواهم که دلم با غم هم‌خو باشد</p></div>
<div class="m2"><p>گر دست دهد غمش چه نیکو باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان ای دل بی‌دل غم او دربر گیر</p></div>
<div class="m2"><p>تا چشم زنی خود غم او او باشد</p></div></div>