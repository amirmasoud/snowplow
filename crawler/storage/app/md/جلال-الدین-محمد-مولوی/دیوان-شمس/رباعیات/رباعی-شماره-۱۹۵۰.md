---
title: >-
    رباعی شمارهٔ ۱۹۵۰
---
# رباعی شمارهٔ ۱۹۵۰

<div class="b" id="bn1"><div class="m1"><p>ماه آمد پیش او که تو جان منی</p></div>
<div class="m2"><p>گفتش که تو کمترین غلامان منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند بدان جمع تکبر می‌کرد</p></div>
<div class="m2"><p>می‌داشت طمع که گویمش آن منی</p></div></div>