---
title: >-
    رباعی شمارهٔ ۱۵۹۶
---
# رباعی شمارهٔ ۱۵۹۶

<div class="b" id="bn1"><div class="m1"><p>از دیدهٔ کژ دلبر رعنا را چه</p></div>
<div class="m2"><p>وز بدنامی عاشق شیدا را چه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما در ره عشق چست و چالاک شویم</p></div>
<div class="m2"><p>ور زانکه خری لنگ شود ما را چه</p></div></div>