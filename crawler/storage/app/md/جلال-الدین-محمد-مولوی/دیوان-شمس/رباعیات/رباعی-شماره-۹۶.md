---
title: >-
    رباعی شمارهٔ ۹۶
---
# رباعی شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>حاجت نبود مستی ما را به شراب</p></div>
<div class="m2"><p>یا مجلس ما را طرب از چنگ و رباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌ساقی و بی‌شاهد و بی‌مطرب و نی</p></div>
<div class="m2"><p>شوریده و مستیم چو مستان خراب</p></div></div>