---
title: >-
    رباعی شمارهٔ ۱۸۹۸
---
# رباعی شمارهٔ ۱۸۹۸

<div class="b" id="bn1"><div class="m1"><p>شمشیر اگر گردن جان ببریدی</p></div>
<div class="m2"><p>بل احیاء بربهم که شنیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح یحیی اگر نه باقی بودی</p></div>
<div class="m2"><p>در خون سر او سه ماه کی گردیدی</p></div></div>