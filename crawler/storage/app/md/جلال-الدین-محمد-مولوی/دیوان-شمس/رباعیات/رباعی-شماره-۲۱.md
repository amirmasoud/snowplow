---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ای در سر زلف تو پریشانی‌ها</p></div>
<div class="m2"><p>واندر لب لعلت شکرافشانی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی ز فراق ما پشیمان گشتی</p></div>
<div class="m2"><p>ای جان چه پشیمان که پشیمانی‌ها</p></div></div>