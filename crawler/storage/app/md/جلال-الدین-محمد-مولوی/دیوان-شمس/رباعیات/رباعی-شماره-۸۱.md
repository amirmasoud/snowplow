---
title: >-
    رباعی شمارهٔ ۸۱
---
# رباعی شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>امروز چو هر روز خرابیم خراب</p></div>
<div class="m2"><p>مگشا در اندیشه و برگیر رباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدگونه نماز است و رکوعست و سجود</p></div>
<div class="m2"><p>آنرا که جمال دوست باشد محراب</p></div></div>