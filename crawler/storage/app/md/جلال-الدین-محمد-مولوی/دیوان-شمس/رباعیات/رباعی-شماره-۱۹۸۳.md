---
title: >-
    رباعی شمارهٔ ۱۹۸۳
---
# رباعی شمارهٔ ۱۹۸۳

<div class="b" id="bn1"><div class="m1"><p>هرگز به مزاج خود یکی دم نزنی</p></div>
<div class="m2"><p>تا از دم خویش گردن غم نزنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند ملولی تو یقین است که تو</p></div>
<div class="m2"><p>با اینکه ملولی ز کسی کم نزنی</p></div></div>