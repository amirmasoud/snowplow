---
title: >-
    رباعی شمارهٔ ۱۸۶۴
---
# رباعی شمارهٔ ۱۸۶۴

<div class="b" id="bn1"><div class="m1"><p>در عشق تو خون دیده بارید بسی</p></div>
<div class="m2"><p>جان در تن من ز غم بنالید بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگاه نی ز حالم ای جان جهان</p></div>
<div class="m2"><p>چرخم به بهانه تو مالید بسی</p></div></div>