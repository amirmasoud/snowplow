---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>عاشق شب خلوت از پی پی گم را</p></div>
<div class="m2"><p>بسیار بود که کژ نهد انجم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که شب وصال زحمت باشد</p></div>
<div class="m2"><p>از مردم دیده دیدهٔ مردم را</p></div></div>