---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای آنکه نیافت ماه شب گرد ترا</p></div>
<div class="m2"><p>از ماه تو تحفه‌ها است شبگرد ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که سرخ روست اطراف شفق</p></div>
<div class="m2"><p>شهمات همی شوند رخ زرد ترا</p></div></div>