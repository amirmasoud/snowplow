---
title: >-
    رباعی شمارهٔ ۵۲۳
---
# رباعی شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>از عشق خدا نه بر زیان خواهی شد</p></div>
<div class="m2"><p>بی‌جان ز کجا شوی که جان خواهی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول به زمین از آسمان آمده‌ای</p></div>
<div class="m2"><p>آخر ز زمین بر آسمان خواهی شد</p></div></div>