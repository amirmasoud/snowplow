---
title: >-
    رباعی شمارهٔ ۱۰۶۷
---
# رباعی شمارهٔ ۱۰۶۷

<div class="b" id="bn1"><div class="m1"><p>ای داروی فربهی و جان عاشق</p></div>
<div class="m2"><p>فربه ز خیال تو روان عاشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین ز دهان تو دهان عاشق</p></div>
<div class="m2"><p>جان بنده‌ات ای جان و جهان عاشق</p></div></div>