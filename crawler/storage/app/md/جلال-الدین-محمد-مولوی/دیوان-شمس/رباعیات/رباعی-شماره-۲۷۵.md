---
title: >-
    رباعی شمارهٔ ۲۷۵
---
# رباعی شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>جانا غم تو ز هرچه گویی بتر است</p></div>
<div class="m2"><p>رنج دل و تاب تن و سوز جگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هرچه خورند کم شود جز غم تو</p></div>
<div class="m2"><p>تا بیشترش همی خورم بیشتر است</p></div></div>