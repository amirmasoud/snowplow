---
title: >-
    رباعی شمارهٔ ۱۳۸۷
---
# رباعی شمارهٔ ۱۳۸۷

<div class="b" id="bn1"><div class="m1"><p>از بسکه فساد و ابلهی زاد از من</p></div>
<div class="m2"><p>در عمر کسی نگشت دلشاد از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من طالب داد و جمله بیداد از من</p></div>
<div class="m2"><p>فریاد من از جمله و فریاد از من</p></div></div>