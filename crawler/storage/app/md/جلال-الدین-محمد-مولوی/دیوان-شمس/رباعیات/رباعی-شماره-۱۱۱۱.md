---
title: >-
    رباعی شمارهٔ ۱۱۱۱
---
# رباعی شمارهٔ ۱۱۱۱

<div class="b" id="bn1"><div class="m1"><p>آن خوش سخنان که ما بگفتیم به هم</p></div>
<div class="m2"><p>در دل دارد نهفته این چرخ به خم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکروز چو باران کند او غمازی</p></div>
<div class="m2"><p>بر روید سر ماز صحن عالم</p></div></div>