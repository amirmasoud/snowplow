---
title: >-
    رباعی شمارهٔ ۴۱۵
---
# رباعی شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>معشوق شراب‌خوار و بیسامانست</p></div>
<div class="m2"><p>خونخواره و شوخ و شنگ و نافرمانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر سر جعد آن صنم ایمانست</p></div>
<div class="m2"><p>دیریست که درد عشق بیدرمانست</p></div></div>