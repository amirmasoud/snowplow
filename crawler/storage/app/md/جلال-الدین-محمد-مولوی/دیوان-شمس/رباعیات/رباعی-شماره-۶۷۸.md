---
title: >-
    رباعی شمارهٔ ۶۷۸
---
# رباعی شمارهٔ ۶۷۸

<div class="b" id="bn1"><div class="m1"><p>در لشکر عشق چونکه خونریز کنند</p></div>
<div class="m2"><p>شمشیر ز پاره‌های ما تیز کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من غرقهٔ آن سینهٔ دریا صفتم</p></div>
<div class="m2"><p>یاران مرا بگو که پرهیز کنند</p></div></div>