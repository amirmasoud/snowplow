---
title: >-
    رباعی شمارهٔ ۴۴۴
---
# رباعی شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>هر روز به نو برآید آن دلبر مست</p></div>
<div class="m2"><p>با ساغر پرفتنهٔ پرشور بدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بستانم قرابهٔ عقل شکست</p></div>
<div class="m2"><p>ور نستانم ندانم از دستش رست</p></div></div>