---
title: >-
    رباعی شمارهٔ ۱۷۲۹
---
# رباعی شمارهٔ ۱۷۲۹

<div class="b" id="bn1"><div class="m1"><p>ای دل تو دمی مطیع سبحان نشدی</p></div>
<div class="m2"><p>وز کار بدت هیچ پشیمان نشدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفی و فقیه و زاهد و دانشمند</p></div>
<div class="m2"><p>این جمله شدی ولی مسلمان نشدی</p></div></div>