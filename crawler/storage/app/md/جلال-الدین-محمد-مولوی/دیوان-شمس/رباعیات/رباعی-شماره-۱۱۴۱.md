---
title: >-
    رباعی شمارهٔ ۱۱۴۱
---
# رباعی شمارهٔ ۱۱۴۱

<div class="b" id="bn1"><div class="m1"><p>افتاده مرا عجب شکاری چکنم</p></div>
<div class="m2"><p>واندر سرم افکنده خماری چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالوسم و زاهدم ولیکن در راه</p></div>
<div class="m2"><p>گر بوسه دهد مرا نگاری چکنم</p></div></div>