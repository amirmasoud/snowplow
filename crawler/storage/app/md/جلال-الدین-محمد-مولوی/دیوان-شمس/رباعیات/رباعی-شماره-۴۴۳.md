---
title: >-
    رباعی شمارهٔ ۴۴۳
---
# رباعی شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>هر ذره و هر خیال چون بیداریست</p></div>
<div class="m2"><p>از شادی و اندهان ما هشیاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه چرا نشد میان خویشان</p></div>
<div class="m2"><p>کز باخبران بی‌خبری بدکاریست</p></div></div>