---
title: >-
    رباعی شمارهٔ ۱۹۷۹
---
# رباعی شمارهٔ ۱۹۷۹

<div class="b" id="bn1"><div class="m1"><p>هر روز پگاه خیمه بر جوی زنی</p></div>
<div class="m2"><p>صد نقش تو بر گلشن خوشبوی زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دف دل ما سماع آنگاه کند</p></div>
<div class="m2"><p>کش هر نفسی هزار بر روی زنی</p></div></div>