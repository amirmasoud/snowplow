---
title: >-
    رباعی شمارهٔ ۷۷۷
---
# رباعی شمارهٔ ۷۷۷

<div class="b" id="bn1"><div class="m1"><p>کاری ز درون جان میباید</p></div>
<div class="m2"><p>وز قصه شنیدن این گره نگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک چشمهٔ آب در درون خانه</p></div>
<div class="m2"><p>به زان رودی که از برون می‌آید</p></div></div>