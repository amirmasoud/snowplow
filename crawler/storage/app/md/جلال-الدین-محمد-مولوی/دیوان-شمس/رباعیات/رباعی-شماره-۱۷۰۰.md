---
title: >-
    رباعی شمارهٔ ۱۷۰۰
---
# رباعی شمارهٔ ۱۷۰۰

<div class="b" id="bn1"><div class="m1"><p>ای آنکه مرا بستهٔ صد دام کنی</p></div>
<div class="m2"><p>گوئی که برو در شب و پیغام کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر من بروم تو با که آرام کنی</p></div>
<div class="m2"><p>همنام من ای دوست کرا نام کنی</p></div></div>