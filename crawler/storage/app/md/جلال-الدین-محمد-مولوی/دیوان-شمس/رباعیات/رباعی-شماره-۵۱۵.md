---
title: >-
    رباعی شمارهٔ ۵۱۵
---
# رباعی شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>از آدمیی دمی بجائی ارزد</p></div>
<div class="m2"><p>یک موی کز اوفتد بکانی ارزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم آدمیی بود که از صحبت او</p></div>
<div class="m2"><p>نادیدن او ملک جهانی ارزد</p></div></div>