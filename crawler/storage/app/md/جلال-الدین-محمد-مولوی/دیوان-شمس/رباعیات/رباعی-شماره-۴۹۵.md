---
title: >-
    رباعی شمارهٔ ۴۹۵
---
# رباعی شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>آن کس که مرا به صدق اقرار کند</p></div>
<div class="m2"><p>چون لعبتگان مرا به بازار کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیزارم از آن کار و نیم بازاری</p></div>
<div class="m2"><p>من بندهٔ آن کسم که انکار کند</p></div></div>