---
title: >-
    رباعی شمارهٔ ۸۰۷
---
# رباعی شمارهٔ ۸۰۷

<div class="b" id="bn1"><div class="m1"><p>لعلیست که او شکر فروشی داند</p></div>
<div class="m2"><p>وز عالم غیب باده نوشی داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامش گویم و لیک دستوری نیست</p></div>
<div class="m2"><p>من بندهٔ آنم که خموشی داند</p></div></div>