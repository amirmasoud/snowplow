---
title: >-
    رباعی شمارهٔ ۵۴۰
---
# رباعی شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>اندیشهٔ هشیار تو هشیار کشد</p></div>
<div class="m2"><p>زارش کشد و بزاری زار کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهان همه خصم خویش بر دار کشند</p></div>
<div class="m2"><p>زان دولت بیدار تو بیدار کشد</p></div></div>