---
title: >-
    رباعی شمارهٔ ۱۶۹۱
---
# رباعی شمارهٔ ۱۶۹۱

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تو از دوش بیادم دادی</p></div>
<div class="m2"><p>زان حالت پرجوش بیادم دادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن رحمت را کجا فراموش کنم</p></div>
<div class="m2"><p>کز گنج فراموش بیادم دادی</p></div></div>