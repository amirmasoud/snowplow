---
title: >-
    رباعی شمارهٔ ۱۵۴۵
---
# رباعی شمارهٔ ۱۵۴۵

<div class="b" id="bn1"><div class="m1"><p>ای در دل من میل و تمنا همه تو</p></div>
<div class="m2"><p>واندر سر من مایهٔ سودا همه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند بروی کار در مینگرم</p></div>
<div class="m2"><p>امروز همه توئی و فردا همه تو</p></div></div>