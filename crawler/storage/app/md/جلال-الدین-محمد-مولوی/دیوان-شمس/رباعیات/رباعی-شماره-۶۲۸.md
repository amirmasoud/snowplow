---
title: >-
    رباعی شمارهٔ ۶۲۸
---
# رباعی شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>جز دمدمهٔ عشق تو در گوش نماند</p></div>
<div class="m2"><p>جان را ز حلاوت ازل هوش نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌رنگی عشق رنگها را آمیخت</p></div>
<div class="m2"><p>وز قالب بی‌رنگ فراموش نماند</p></div></div>