---
title: >-
    رباعی شمارهٔ ۱۵۸۱
---
# رباعی شمارهٔ ۱۵۸۱

<div class="b" id="bn1"><div class="m1"><p>گفتم روزی که من به جانم با تو</p></div>
<div class="m2"><p>دیگر نشدم بتا همانم با تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن دانم که هرچه بازم ببری</p></div>
<div class="m2"><p>زان میبازم که تا بمانم با تو</p></div></div>