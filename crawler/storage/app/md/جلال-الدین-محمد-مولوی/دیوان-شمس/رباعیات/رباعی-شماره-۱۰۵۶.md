---
title: >-
    رباعی شمارهٔ ۱۰۵۶
---
# رباعی شمارهٔ ۱۰۵۶

<div class="b" id="bn1"><div class="m1"><p>گفتی مگری چو ابر در فرقت باغ</p></div>
<div class="m2"><p>من آن توام بخسب ایمن به فراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که چراغ زیر طشتی بنهی</p></div>
<div class="m2"><p>وانگاه بجویمش به صد چشم و چراغ</p></div></div>