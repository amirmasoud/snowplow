---
title: >-
    رباعی شمارهٔ ۱۳۱۰
---
# رباعی شمارهٔ ۱۳۱۰

<div class="b" id="bn1"><div class="m1"><p>ما خواجهٔ ده نه‌ایم ما قلاشیم</p></div>
<div class="m2"><p>ما صدر سرانه‌ایم ما اوباشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی چو قلم به دست آن نقاشیم</p></div>
<div class="m2"><p>خود نیز ندانیم کجا میباشیم</p></div></div>