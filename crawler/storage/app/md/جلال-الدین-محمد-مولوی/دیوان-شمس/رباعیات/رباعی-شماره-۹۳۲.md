---
title: >-
    رباعی شمارهٔ ۹۳۲
---
# رباعی شمارهٔ ۹۳۲

<div class="b" id="bn1"><div class="m1"><p>ای جان سماع و روزه و حج و نماز</p></div>
<div class="m2"><p>وی از تو حقیقت شده بازی و مجاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز منم مطربت ای شمع طراز</p></div>
<div class="m2"><p>وز چرخ بود نثار و قوال انداز</p></div></div>