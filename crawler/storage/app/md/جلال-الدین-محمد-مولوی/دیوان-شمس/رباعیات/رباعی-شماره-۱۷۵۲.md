---
title: >-
    رباعی شمارهٔ ۱۷۵۲
---
# رباعی شمارهٔ ۱۷۵۲

<div class="b" id="bn1"><div class="m1"><p>ای کمتر مهمانیت آب گرمی</p></div>
<div class="m2"><p>کز لذت آن مست شود بی‌شرمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خالق گردون به خودم مهمان کن</p></div>
<div class="m2"><p>گردون به کجا برد به آب گرمی</p></div></div>