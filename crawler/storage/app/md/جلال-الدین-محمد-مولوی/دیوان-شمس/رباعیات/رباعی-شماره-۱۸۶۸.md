---
title: >-
    رباعی شمارهٔ ۱۸۶۸
---
# رباعی شمارهٔ ۱۸۶۸

<div class="b" id="bn1"><div class="m1"><p>درویشان را عار بود محتشمی</p></div>
<div class="m2"><p>واندر دلشان بار بود محتشمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر ره دوست فقر مطلق خوشتر</p></div>
<div class="m2"><p>کاندر ره او خوار بود محتشمی</p></div></div>