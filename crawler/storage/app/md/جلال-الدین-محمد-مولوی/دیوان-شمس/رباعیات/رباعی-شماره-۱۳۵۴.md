---
title: >-
    رباعی شمارهٔ ۱۳۵۴
---
# رباعی شمارهٔ ۱۳۵۴

<div class="b" id="bn1"><div class="m1"><p>من همچو کسی نشسته بر اسب خام</p></div>
<div class="m2"><p>در وادی هولناک بگسسته لگام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تازد چون مرغ تا که بجهد از دام</p></div>
<div class="m2"><p>تا منزل این اسب کدام است کدام</p></div></div>