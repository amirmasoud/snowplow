---
title: >-
    رباعی شمارهٔ ۱۵۹۰
---
# رباعی شمارهٔ ۱۵۹۰

<div class="b" id="bn1"><div class="m1"><p>هرچند در این هوس بسی باشی تو</p></div>
<div class="m2"><p>بیقدر تو همچون مگسی باشی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار مباش هیچکس تا برهی</p></div>
<div class="m2"><p>آخر که تو باشی که کسی باشی تو</p></div></div>