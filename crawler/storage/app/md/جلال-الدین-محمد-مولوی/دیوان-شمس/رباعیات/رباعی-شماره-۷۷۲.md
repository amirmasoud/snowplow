---
title: >-
    رباعی شمارهٔ ۷۷۲
---
# رباعی شمارهٔ ۷۷۲

<div class="b" id="bn1"><div class="m1"><p>غم کیست که گرد دل مردان گردد</p></div>
<div class="m2"><p>غم گرد فسردگان و سردان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر دل مردان خدا دریائیست</p></div>
<div class="m2"><p>کز موج خوشش گنبد گردان گردد</p></div></div>