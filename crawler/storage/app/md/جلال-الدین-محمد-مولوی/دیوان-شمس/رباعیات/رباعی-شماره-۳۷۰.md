---
title: >-
    رباعی شمارهٔ ۳۷۰
---
# رباعی شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>گر آه کنم آه بدین قانع نیست</p></div>
<div class="m2"><p>ور خاک شوم شاه بدین قانع نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور سجده کنم چو سایه هرسو که مه است</p></div>
<div class="m2"><p>پنهان چه کنم ماه بدین قانع نیست</p></div></div>