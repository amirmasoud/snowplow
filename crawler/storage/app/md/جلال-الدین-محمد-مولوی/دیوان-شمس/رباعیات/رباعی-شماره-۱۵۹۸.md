---
title: >-
    رباعی شمارهٔ ۱۵۹۸
---
# رباعی شمارهٔ ۱۵۹۸

<div class="b" id="bn1"><div class="m1"><p>ای کان العباد ما اهواه</p></div>
<div class="m2"><p>ما یذکرنا فکیف ما ینساه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدر ان به القلوب والافواه</p></div>
<div class="m2"><p>قد احسن لا اله الا الله</p></div></div>