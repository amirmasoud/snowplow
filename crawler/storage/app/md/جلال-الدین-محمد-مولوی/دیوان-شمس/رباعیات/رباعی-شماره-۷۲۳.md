---
title: >-
    رباعی شمارهٔ ۷۲۳
---
# رباعی شمارهٔ ۷۲۳

<div class="b" id="bn1"><div class="m1"><p>زان مقصد صنع تو یکی نی ببرید</p></div>
<div class="m2"><p>از بهر لب چون شکر خود بگزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان نی ز تو از بسکه می لب نوشید</p></div>
<div class="m2"><p>هم بر لب تو مست شد و بخروشید</p></div></div>