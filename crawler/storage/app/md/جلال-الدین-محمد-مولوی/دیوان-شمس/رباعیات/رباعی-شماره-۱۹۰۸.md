---
title: >-
    رباعی شمارهٔ ۱۹۰۸
---
# رباعی شمارهٔ ۱۹۰۸

<div class="b" id="bn1"><div class="m1"><p>غم را دیدم گرفته جام دردی</p></div>
<div class="m2"><p>گفتم که غما خبر بود رخ زردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا چکنم که شادیی آوردی</p></div>
<div class="m2"><p>بازار مرا خراب و کاسد کردی</p></div></div>