---
title: >-
    رباعی شمارهٔ ۵۰۳
---
# رباعی شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>آنها که چو آب صافی و ساده روند</p></div>
<div class="m2"><p>اندر رگ و مغز خلق چون باده روند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من پای کشیدم و دراز افتادم</p></div>
<div class="m2"><p>اندر کشتی دراز افتاده روند</p></div></div>