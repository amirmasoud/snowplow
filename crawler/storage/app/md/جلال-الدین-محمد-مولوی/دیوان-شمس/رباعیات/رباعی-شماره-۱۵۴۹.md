---
title: >-
    رباعی شمارهٔ ۱۵۴۹
---
# رباعی شمارهٔ ۱۵۴۹

<div class="b" id="bn1"><div class="m1"><p>ای زندگی تن و توانم همه تو</p></div>
<div class="m2"><p>جانی و دلی ای دل و جانم همه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو هستی من شدی از آنی همه من</p></div>
<div class="m2"><p>من نیست شدم در تو از آنم همه تو</p></div></div>