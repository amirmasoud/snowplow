---
title: >-
    رباعی شمارهٔ ۱۷۵
---
# رباعی شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>او پاک شده است و خام ار در حرم است</p></div>
<div class="m2"><p>در کیسه بدان رود که نقد درم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قلاب نشاید که شود با او یار</p></div>
<div class="m2"><p>از ضد بجهد یکی اگر محترم است</p></div></div>