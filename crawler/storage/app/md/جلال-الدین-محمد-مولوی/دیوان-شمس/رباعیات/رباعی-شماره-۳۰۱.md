---
title: >-
    رباعی شمارهٔ ۳۰۱
---
# رباعی شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>در بتکده تا خیال معشوهٔ ما است</p></div>
<div class="m2"><p>رفتن به طواف کعبه در عین خطا است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کعبه از او بوی ندارد کنش است</p></div>
<div class="m2"><p>با بوی وصال او کنش کعبهٔ ما است</p></div></div>