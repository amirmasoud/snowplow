---
title: >-
    رباعی شمارهٔ ۱۰۷۴
---
# رباعی شمارهٔ ۱۰۷۴

<div class="b" id="bn1"><div class="m1"><p>خندید فرح تا بزنی انگشتک</p></div>
<div class="m2"><p>گردید قدح تا بزنی انگشتک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمودت ابروی خود از زیر نقاب</p></div>
<div class="m2"><p>چون قوس قزح تا بزنی انگشتک</p></div></div>