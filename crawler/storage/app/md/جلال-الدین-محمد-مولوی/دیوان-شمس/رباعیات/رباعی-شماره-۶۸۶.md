---
title: >-
    رباعی شمارهٔ ۶۸۶
---
# رباعی شمارهٔ ۶۸۶

<div class="b" id="bn1"><div class="m1"><p>در عشق توم وفا قرین می‌باید</p></div>
<div class="m2"><p>وصل تو گمانست، یقین می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار من دل خواسته در خدمت تو</p></div>
<div class="m2"><p>بد نیست ولیکن به ازین می‌باید</p></div></div>