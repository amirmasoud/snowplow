---
title: >-
    رباعی شمارهٔ ۱۱۵۸
---
# رباعی شمارهٔ ۱۱۵۸

<div class="b" id="bn1"><div class="m1"><p>ای جان و جهان، جان و جهان گم کردم</p></div>
<div class="m2"><p>ای ماه زمین و آسمان گم کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می بر کف من منه بنه بر دهنم</p></div>
<div class="m2"><p>کز مستی تو راه دهان گم کردم</p></div></div>