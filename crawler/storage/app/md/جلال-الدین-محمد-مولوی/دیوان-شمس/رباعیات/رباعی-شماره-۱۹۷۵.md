---
title: >-
    رباعی شمارهٔ ۱۹۷۵
---
# رباعی شمارهٔ ۱۹۷۵

<div class="b" id="bn1"><div class="m1"><p>نی من منم و نی تو توئی نی تو منی</p></div>
<div class="m2"><p>هم من منم و هم تو توئی و هم تو منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با تو چنانم ای نگار ختنی</p></div>
<div class="m2"><p>کاندر غلطم که من توام یا تو منی</p></div></div>