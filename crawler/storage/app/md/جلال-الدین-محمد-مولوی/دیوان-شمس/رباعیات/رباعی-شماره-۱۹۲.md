---
title: >-
    رباعی شمارهٔ ۱۹۲
---
# رباعی شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>ای ذکر تو مانع تماشای تو دوست</p></div>
<div class="m2"><p>برق رخ تو نقاب سیمای تو دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با یاد لبت از لب تو محرومم</p></div>
<div class="m2"><p>ای یاد لبت حجاب لبهای تو دوست</p></div></div>