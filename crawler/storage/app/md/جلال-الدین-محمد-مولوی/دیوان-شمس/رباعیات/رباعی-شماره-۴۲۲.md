---
title: >-
    رباعی شمارهٔ ۴۲۲
---
# رباعی شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>میدان که در درون تو مثال غاریست</p></div>
<div class="m2"><p>واندر پس آنغار عجب بازاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکس یاری گرفت و کاری بگزید</p></div>
<div class="m2"><p>این یار نهانیست عجب یاریست</p></div></div>