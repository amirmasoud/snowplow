---
title: >-
    رباعی شمارهٔ ۸۶۵
---
# رباعی شمارهٔ ۸۶۵

<div class="b" id="bn1"><div class="m1"><p>آن جمع کن جان پراکنده بیار</p></div>
<div class="m2"><p>وان مستی هر خواجه و هر بنده بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آواز بکش رضای پاینده بیار</p></div>
<div class="m2"><p>ز آواز سرافیل شوم زنده بیار</p></div></div>