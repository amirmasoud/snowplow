---
title: >-
    رباعی شمارهٔ ۲۶۶
---
# رباعی شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>تا من بزیم پیشه و کارم اینست</p></div>
<div class="m2"><p>صیاد نیم صید و شکارم اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزم اینست و روزگارم اینست</p></div>
<div class="m2"><p>آرام و قرار و غمگسارم اینست</p></div></div>