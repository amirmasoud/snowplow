---
title: >-
    رباعی شمارهٔ ۸۴۳
---
# رباعی شمارهٔ ۸۴۳

<div class="b" id="bn1"><div class="m1"><p>هر روز دلم نو شکری نوش کند</p></div>
<div class="m2"><p>کز ذوق گذشته‌ها فراموش کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول باده ز عاشقی نوش کند</p></div>
<div class="m2"><p>آنگاه دهد به ما و مدهوش کند</p></div></div>