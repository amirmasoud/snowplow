---
title: >-
    رباعی شمارهٔ ۱۸۳۰
---
# رباعی شمارهٔ ۱۸۳۰

<div class="b" id="bn1"><div class="m1"><p>چشم تو بهر غمزه بسوزد مستی</p></div>
<div class="m2"><p>گر دلبندی هزار خون کردستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پای درآمد دل و دل پای نداشت</p></div>
<div class="m2"><p>از دست کسی که او ندارد رستی</p></div></div>