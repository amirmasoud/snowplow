---
title: >-
    رباعی شمارهٔ ۴۹۴
---
# رباعی شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>آن کس که ز دل دم اناالحق میزد</p></div>
<div class="m2"><p>امروز بر این رسن معلق میزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکس که ز چشم سحر مطلق میزد</p></div>
<div class="m2"><p>بر خود ز غمت هزار گون دق میزد</p></div></div>