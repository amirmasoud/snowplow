---
title: >-
    رباعی شمارهٔ ۱۳۰۶
---
# رباعی شمارهٔ ۱۳۰۶

<div class="b" id="bn1"><div class="m1"><p>ما باده ز یار دلفروز آوردیم</p></div>
<div class="m2"><p>ما آتش عشق سینه‌سوز آوردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دور ابد جهان نبیند در خواب</p></div>
<div class="m2"><p>آن شبها را که ما به روز آوردیم</p></div></div>