---
title: >-
    رباعی شمارهٔ ۱۹۵۸
---
# رباعی شمارهٔ ۱۹۵۸

<div class="b" id="bn1"><div class="m1"><p>من بی‌دلم ای نگار و تو دلداری</p></div>
<div class="m2"><p>شاید که بهر سخن ز من نازاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا آن دل من که برده‌ای بازدهی</p></div>
<div class="m2"><p>یا هر چه کنم ز بیدلی برداری</p></div></div>