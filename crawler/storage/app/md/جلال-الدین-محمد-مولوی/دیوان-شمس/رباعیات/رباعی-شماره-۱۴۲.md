---
title: >-
    رباعی شمارهٔ ۱۴۲
---
# رباعی شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>آنکس که بروی خواب او رشک پریست</p></div>
<div class="m2"><p>آمد سحری و بر دل من نگریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او گریه و من گریه که تا آمد صبح</p></div>
<div class="m2"><p>پرسید کز این هر دو عجب عاشق کیست</p></div></div>