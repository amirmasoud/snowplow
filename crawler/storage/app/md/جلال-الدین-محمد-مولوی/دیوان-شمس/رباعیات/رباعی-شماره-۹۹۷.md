---
title: >-
    رباعی شمارهٔ ۹۹۷
---
# رباعی شمارهٔ ۹۹۷

<div class="b" id="bn1"><div class="m1"><p>از آتش تو فتاده جانم در جوش</p></div>
<div class="m2"><p>وز باده تو شده است جانم مدهوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حسرت آنکه گیرمت در آغوش</p></div>
<div class="m2"><p>هرجای کنم فغان و هر سوی خروش</p></div></div>