---
title: >-
    رباعی شمارهٔ ۸۴۲
---
# رباعی شمارهٔ ۸۴۲

<div class="b" id="bn1"><div class="m1"><p>هر دل که بسوی دلربائی نرود</p></div>
<div class="m2"><p>والله که به جز سوی فنائی نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شاد کبوتری که صید عشق است</p></div>
<div class="m2"><p>چندانکه برانیش بجائی نرود</p></div></div>