---
title: >-
    رباعی شمارهٔ ۱۱۸۰
---
# رباعی شمارهٔ ۱۱۸۰

<div class="b" id="bn1"><div class="m1"><p>بر یاد لبت لعل نگین می‌بوسم</p></div>
<div class="m2"><p>آنم چو بدست نیست این می‌بوسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستم چو بر آسمان تو می‌نرسد</p></div>
<div class="m2"><p>می‌آرم سجده و زمین می‌بوسم</p></div></div>