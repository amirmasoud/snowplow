---
title: >-
    رباعی شمارهٔ ۱۱۲۷
---
# رباعی شمارهٔ ۱۱۲۷

<div class="b" id="bn1"><div class="m1"><p>از خاک در تو چون جدا می‌باشم</p></div>
<div class="m2"><p>با گریه و ناله آشنا میباشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع ز گریه آبرو میدارم</p></div>
<div class="m2"><p>چون چنگ ز ناله با نوا میباشم</p></div></div>