---
title: >-
    رباعی شمارهٔ ۴۳۵
---
# رباعی شمارهٔ ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>هر چند به حلم یار ما جورکش است</p></div>
<div class="m2"><p>لیکن زاری عاشقان نیز خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان عاشق چون گلستان میخندد</p></div>
<div class="m2"><p>تن میلفرزد چو برگ گوئی تبش است</p></div></div>