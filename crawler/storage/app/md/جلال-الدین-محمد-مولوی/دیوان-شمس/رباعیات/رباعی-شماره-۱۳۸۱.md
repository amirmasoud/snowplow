---
title: >-
    رباعی شمارهٔ ۱۳۸۱
---
# رباعی شمارهٔ ۱۳۸۱

<div class="b" id="bn1"><div class="m1"><p>آن صورت غیبی که شندیش دشمن</p></div>
<div class="m2"><p>با خود به قیاس می‌بریدش دشمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانندهٔ خورشید برآمد پیشین</p></div>
<div class="m2"><p>هر سو که نظر کرد ندیدش دشمن</p></div></div>