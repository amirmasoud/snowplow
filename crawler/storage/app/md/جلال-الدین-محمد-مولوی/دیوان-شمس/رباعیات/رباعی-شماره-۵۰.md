---
title: >-
    رباعی شمارهٔ ۵۰
---
# رباعی شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>دیدم در خواب ساقی زیبا را</p></div>
<div class="m2"><p>بر دست گرفته ساغر صهبا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم به خیالش که غلام اوئی</p></div>
<div class="m2"><p>شاید که به جای خواجه باشی ما را</p></div></div>