---
title: >-
    رباعی شمارهٔ ۷۲۱
---
# رباعی شمارهٔ ۷۲۱

<div class="b" id="bn1"><div class="m1"><p>رو نیکی کن که دهر نیکی داند</p></div>
<div class="m2"><p>او نیکی را از نیکوان نستاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مال از همه ماند و از تو هم خواهد ماند</p></div>
<div class="m2"><p>آن به که بجای مال نیکی ماند</p></div></div>