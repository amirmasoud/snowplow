---
title: >-
    رباعی شمارهٔ ۶۶۰
---
# رباعی شمارهٔ ۶۶۰

<div class="b" id="bn1"><div class="m1"><p>در بندم از آن دو زلف بند اندر بند</p></div>
<div class="m2"><p>در ناله‌ام از لبان قند اندر قند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر وعدهٔ دیدار تو هیچ اندر هیچ</p></div>
<div class="m2"><p>آخر غم هجران تو چند اندر چند</p></div></div>