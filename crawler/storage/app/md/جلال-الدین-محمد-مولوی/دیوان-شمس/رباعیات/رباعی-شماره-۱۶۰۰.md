---
title: >-
    رباعی شمارهٔ ۱۶۰۰
---
# رباعی شمارهٔ ۱۶۰۰

<div class="b" id="bn1"><div class="m1"><p>ای آنکه به جان این جهانی زنده</p></div>
<div class="m2"><p>شرمت بادا چرا چنانی زنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌عشق مباش تا نباشی مرده</p></div>
<div class="m2"><p>در عشق بمیر تا بمانی زنده</p></div></div>