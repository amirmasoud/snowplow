---
title: >-
    رباعی شمارهٔ ۱۲۴۶
---
# رباعی شمارهٔ ۱۲۴۶

<div class="b" id="bn1"><div class="m1"><p>زاهد بودی ترانه گویت کردم</p></div>
<div class="m2"><p>خاموش بدی فسانه گویت کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر عالم نه نام بودت نه نشان</p></div>
<div class="m2"><p>ننشاندمت و نشانه گویت کردم</p></div></div>