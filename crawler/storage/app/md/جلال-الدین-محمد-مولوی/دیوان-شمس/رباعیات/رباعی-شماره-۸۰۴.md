---
title: >-
    رباعی شمارهٔ ۸۰۴
---
# رباعی شمارهٔ ۸۰۴

<div class="b" id="bn1"><div class="m1"><p>کی غم خورد آنکه شاد مطلق باشد</p></div>
<div class="m2"><p>واندل که برون ز چرخ ازرق باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخم غم را کجا پذیرد چو زمین</p></div>
<div class="m2"><p>آن کز هوسش فلک معلق باشد</p></div></div>