---
title: >-
    رباعی شمارهٔ ۹۲۷
---
# رباعی شمارهٔ ۹۲۷

<div class="b" id="bn1"><div class="m1"><p>آمد بر من دوش نگاری سر تیز</p></div>
<div class="m2"><p>شیرین سخنی شکر لبی شورانگیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با روی چو آفتاب بیدارم کرد</p></div>
<div class="m2"><p>یعنی که چو آفتاب دیدی برخیز</p></div></div>