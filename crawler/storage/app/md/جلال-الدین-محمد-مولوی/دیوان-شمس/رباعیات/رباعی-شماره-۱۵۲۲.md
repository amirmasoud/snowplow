---
title: >-
    رباعی شمارهٔ ۱۵۲۲
---
# رباعی شمارهٔ ۱۵۲۲

<div class="b" id="bn1"><div class="m1"><p>هشدار که می‌روند هر سو غولان</p></div>
<div class="m2"><p>با دانه و دام در شکار گوران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شاد تنی که دامن دل گیرد</p></div>
<div class="m2"><p>عبرت گیرد ز حالت معزولان</p></div></div>