---
title: >-
    رباعی شمارهٔ ۷۶۳
---
# رباعی شمارهٔ ۷۶۳

<div class="b" id="bn1"><div class="m1"><p>عشق از ازلست و تا ابد خواهد بود</p></div>
<div class="m2"><p>جویندهٔ عشق بیعدد خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فردا که قیامت آشکارا گردد</p></div>
<div class="m2"><p>هر دل که نه عاشق است رد خواهد بود</p></div></div>