---
title: >-
    رباعی شمارهٔ ۱۱۵۵
---
# رباعی شمارهٔ ۱۱۵۵

<div class="b" id="bn1"><div class="m1"><p>از دوستیت خون جگر را بخورم</p></div>
<div class="m2"><p>این مظلمه را تا به قیامت ببرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فردا که قیامت آشکار گردد</p></div>
<div class="m2"><p>تو خون طلبی و من برویت نگرم</p></div></div>