---
title: >-
    رباعی شمارهٔ ۱۷۱۶
---
# رباعی شمارهٔ ۱۷۱۶

<div class="b" id="bn1"><div class="m1"><p>ای چون علم بلند در صحرائی</p></div>
<div class="m2"><p>وی چون شکر شگرف در حلوائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان میترسم که بدرگ و بدرائی</p></div>
<div class="m2"><p>در مغز تو افکند دگر سودائی</p></div></div>