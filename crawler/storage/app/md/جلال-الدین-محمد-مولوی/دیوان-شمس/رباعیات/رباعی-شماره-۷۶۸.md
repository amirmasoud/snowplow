---
title: >-
    رباعی شمارهٔ ۷۶۸
---
# رباعی شمارهٔ ۷۶۸

<div class="b" id="bn1"><div class="m1"><p>عقل و دل من چه عیشها میداند</p></div>
<div class="m2"><p>گر یار دمی پیش خودم بنشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد جای نشیب آسیا میدانم</p></div>
<div class="m2"><p>کز بی‌آبی کار فرو میماند</p></div></div>