---
title: >-
    رباعی شمارهٔ ۶۸۴
---
# رباعی شمارهٔ ۶۸۴

<div class="b" id="bn1"><div class="m1"><p>در نفی تو عقل را امان نتوان دید</p></div>
<div class="m2"><p>جز در ره اثبات تو جان نتوان داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اینکه ز تو هیچ مکان خالی نیست</p></div>
<div class="m2"><p>در هیچ مکان ترا نشان نتوان داد</p></div></div>