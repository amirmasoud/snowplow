---
title: >-
    رباعی شمارهٔ ۱۳۰۱
---
# رباعی شمارهٔ ۱۳۰۱

<div class="b" id="bn1"><div class="m1"><p>لا الفجر بقینة و لا شرب مدام</p></div>
<div class="m2"><p>الفخر لمن یطعن فی یوم زحام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من یبدل روحه به سیف و سهام</p></div>
<div class="m2"><p>یستأهل آن یقعدو الناس قیام</p></div></div>