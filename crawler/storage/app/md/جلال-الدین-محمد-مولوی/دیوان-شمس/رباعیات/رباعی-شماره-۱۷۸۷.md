---
title: >-
    رباعی شمارهٔ ۱۷۸۷
---
# رباعی شمارهٔ ۱۷۸۷

<div class="b" id="bn1"><div class="m1"><p>جانم ز طرب چون شکر انباشته‌ای</p></div>
<div class="m2"><p>چون برگ گل اندر شکرم داشته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز مرا خنده فرو می‌گیرد</p></div>
<div class="m2"><p>تا در دهنم چه خنده‌ها کاشته‌ای</p></div></div>