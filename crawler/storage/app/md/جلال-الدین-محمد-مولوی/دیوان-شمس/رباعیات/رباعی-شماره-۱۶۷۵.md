---
title: >-
    رباعی شمارهٔ ۱۶۷۵
---
# رباعی شمارهٔ ۱۶۷۵

<div class="b" id="bn1"><div class="m1"><p>از عشق تو هر طرف یکی شبخیزی</p></div>
<div class="m2"><p>شب کشته ز زلفین تو عنبر بیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقاش ازل نقش کند هر طرفی</p></div>
<div class="m2"><p>از بهر قرار دل من تبریزی</p></div></div>