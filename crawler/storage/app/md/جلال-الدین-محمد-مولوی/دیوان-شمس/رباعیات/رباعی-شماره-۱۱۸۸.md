---
title: >-
    رباعی شمارهٔ ۱۱۸۸
---
# رباعی شمارهٔ ۱۱۸۸

<div class="b" id="bn1"><div class="m1"><p>تا آتش و آب عشق بشناخته‌ام</p></div>
<div class="m2"><p>در آتش دل چو آب بگداخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند رباب دل بپرداخته‌ام</p></div>
<div class="m2"><p>تا زخمهٔ زخم عشق خوش ساخته‌ام</p></div></div>