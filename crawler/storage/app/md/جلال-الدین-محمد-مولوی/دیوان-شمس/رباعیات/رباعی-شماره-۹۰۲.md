---
title: >-
    رباعی شمارهٔ ۹۰۲
---
# رباعی شمارهٔ ۹۰۲

<div class="b" id="bn1"><div class="m1"><p>زان ابروی چون کمانت ای بدر منیر</p></div>
<div class="m2"><p>دل شیشهٔ پرخون شود از ضربت تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویم ز دل و شیشه و خون چیست نظیر</p></div>
<div class="m2"><p>بردارم جام باده و گوید گیر</p></div></div>