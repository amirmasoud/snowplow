---
title: >-
    رباعی شمارهٔ ۱۷۲۲
---
# رباعی شمارهٔ ۱۷۲۲

<div class="b" id="bn1"><div class="m1"><p>ای داده مرا چو عشق خود بیداری</p></div>
<div class="m2"><p>وین شمع میان این جهان تاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چنگم و تو زخمه فرو نگذاری</p></div>
<div class="m2"><p>وانگه گوئی بس است تا کی زاری</p></div></div>