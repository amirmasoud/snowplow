---
title: >-
    رباعی شمارهٔ ۶۱۵
---
# رباعی شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>تو هیچ نه‌ای و هیچ توبه ز وجود</p></div>
<div class="m2"><p>تو غرق زیانی و زیانت همه سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئیکه مرا نیست به جز خاک بدست</p></div>
<div class="m2"><p>ای بر سر خاک جمله افلاک چه سود</p></div></div>