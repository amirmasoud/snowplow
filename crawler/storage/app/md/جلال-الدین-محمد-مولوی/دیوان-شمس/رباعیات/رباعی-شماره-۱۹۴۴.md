---
title: >-
    رباعی شمارهٔ ۱۹۴۴
---
# رباعی شمارهٔ ۱۹۴۴

<div class="b" id="bn1"><div class="m1"><p>کی پست شود آنکه بلندش تو کنی</p></div>
<div class="m2"><p>شادان بود آنجا که نژندش تو کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون سرافراشته صد بوسه زند</p></div>
<div class="m2"><p>هر روز بر آن پای که بندش تو کنی</p></div></div>