---
title: >-
    رباعی شمارهٔ ۱۰۴۲
---
# رباعی شمارهٔ ۱۰۴۲

<div class="b" id="bn1"><div class="m1"><p>شب چیست برای ما زمان نالش</p></div>
<div class="m2"><p>وان را که نه عاشق است او را مالش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان عاشق ناقصی که نوکار بود</p></div>
<div class="m2"><p>گوشش نشود گرم به شب بی‌بالش</p></div></div>