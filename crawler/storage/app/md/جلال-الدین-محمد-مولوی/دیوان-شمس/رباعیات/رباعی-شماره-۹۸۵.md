---
title: >-
    رباعی شمارهٔ ۹۸۵
---
# رباعی شمارهٔ ۹۸۵

<div class="b" id="bn1"><div class="m1"><p>رویم چو زر زمانه می‌بین و مپرس</p></div>
<div class="m2"><p>این اشک چو ناردانه می‌بین و مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احوال درون خانه از من مطلب</p></div>
<div class="m2"><p>خون بر در آستانه می‌بین و مپرس</p></div></div>