---
title: >-
    رباعی شمارهٔ ۱۰۴۰
---
# رباعی شمارهٔ ۱۰۴۰

<div class="b" id="bn1"><div class="m1"><p>سودای توام در جنون می‌زد دوش</p></div>
<div class="m2"><p>دریای دو چشم موج خون می‌زد دوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نیم شبی خیل خیالت برسید</p></div>
<div class="m2"><p>ورنی جانم خیمه برون می‌زد دوش</p></div></div>