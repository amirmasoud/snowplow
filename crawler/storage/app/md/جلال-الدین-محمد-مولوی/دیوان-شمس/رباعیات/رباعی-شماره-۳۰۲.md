---
title: >-
    رباعی شمارهٔ ۳۰۲
---
# رباعی شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>در خواب مهی دوش روانم دیده است</p></div>
<div class="m2"><p>با روی و لبی که روشنی دیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا بر گل ترکان شکر جوشیده است</p></div>
<div class="m2"><p>یا بر شکرستان گل تر روئیده است</p></div></div>