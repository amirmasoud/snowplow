---
title: >-
    رباعی شمارهٔ ۱۴۵
---
# رباعی شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>آنکس که ز سر عاشقی باخبر است</p></div>
<div class="m2"><p>فاش است میان عاشقان مشتهر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکس که ز ناموس نهان میدارد</p></div>
<div class="m2"><p>پیداست که در فراق زیر و زبر است</p></div></div>