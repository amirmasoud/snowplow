---
title: >-
    رباعی شمارهٔ ۳۸۶
---
# رباعی شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>گفتم چشمم که هست خاک کویت</p></div>
<div class="m2"><p>پرآب مدار بی‌رخ نیکویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که نه کس بود که در دولت من</p></div>
<div class="m2"><p>از من همه عمر باشد آب رویت</p></div></div>