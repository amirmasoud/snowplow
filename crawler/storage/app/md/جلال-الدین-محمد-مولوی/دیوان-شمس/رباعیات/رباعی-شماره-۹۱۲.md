---
title: >-
    رباعی شمارهٔ ۹۱۲
---
# رباعی شمارهٔ ۹۱۲

<div class="b" id="bn1"><div class="m1"><p>گر رنگ خزان دارم و گر رنگ بهار</p></div>
<div class="m2"><p>تا هردو یکی نشد نیامد گل و خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ظاهر خار و گل، مخالف دیدار</p></div>
<div class="m2"><p>بر چشم خلاف دید، خندد گلزار</p></div></div>