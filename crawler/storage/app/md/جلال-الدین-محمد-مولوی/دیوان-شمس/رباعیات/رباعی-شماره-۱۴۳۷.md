---
title: >-
    رباعی شمارهٔ ۱۴۳۷
---
# رباعی شمارهٔ ۱۴۳۷

<div class="b" id="bn1"><div class="m1"><p>با هر دو جهان چو رنگ باید بودن</p></div>
<div class="m2"><p>بیزار ز لعل و سنگ باید بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردانه و مرد رنگ باید بودن</p></div>
<div class="m2"><p>ور نی به هزار ننگ باید بودن</p></div></div>