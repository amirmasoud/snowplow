---
title: >-
    رباعی شمارهٔ ۷۳۶
---
# رباعی شمارهٔ ۷۳۶

<div class="b" id="bn1"><div class="m1"><p>شاد آنکه جمال ماهتابش ببرد</p></div>
<div class="m2"><p>ساقی کرم مست و خرابش ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌آید آب دیده می‌ناید خواب</p></div>
<div class="m2"><p>ترسد که اگر بیاید آبش ببرد</p></div></div>