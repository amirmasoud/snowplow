---
title: >-
    رباعی شمارهٔ ۱۵۳۹
---
# رباعی شمارهٔ ۱۵۳۹

<div class="b" id="bn1"><div class="m1"><p>ای بسته تو خواب من به چشم جادو</p></div>
<div class="m2"><p>آن آب حیات و نقل بیخوابان کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی بینم آب چون منم غرقهٔ جو</p></div>
<div class="m2"><p>خود آب گرفته است مرا هر شش سو</p></div></div>