---
title: >-
    رباعی شمارهٔ ۹۰۱
---
# رباعی شمارهٔ ۹۰۱

<div class="b" id="bn1"><div class="m1"><p>روی چو مهت پیش چراغ اولی‌تر</p></div>
<div class="m2"><p>روی حبشی کرده به داغ اولی‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این حلقه چو باغست تو بلبل ما را</p></div>
<div class="m2"><p>رقص بلبل میان باغ اولی‌تر</p></div></div>