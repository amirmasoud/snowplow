---
title: >-
    رباعی شمارهٔ ۱۳۴۵
---
# رباعی شمارهٔ ۱۳۴۵

<div class="b" id="bn1"><div class="m1"><p>من غیر ترا گزین ندارم چکنم</p></div>
<div class="m2"><p>درمان دل حزین ندارم چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئیکه ز چرخ تا بکی چرخ زنیم</p></div>
<div class="m2"><p>من کار دگر جزین ندارم چکنم</p></div></div>