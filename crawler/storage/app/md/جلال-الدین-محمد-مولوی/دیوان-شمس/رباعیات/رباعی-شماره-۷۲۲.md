---
title: >-
    رباعی شمارهٔ ۷۲۲
---
# رباعی شمارهٔ ۷۲۲

<div class="b" id="bn1"><div class="m1"><p>زان آب که چرخ از آن بسر می‌گردد</p></div>
<div class="m2"><p>استارهٔ جانم چو قمر می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحریست محیط و در وی این خلق مقیم</p></div>
<div class="m2"><p>تا کیست کز این بحر گهر میگردد</p></div></div>