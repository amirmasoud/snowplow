---
title: >-
    رباعی شمارهٔ ۱۷۶۶
---
# رباعی شمارهٔ ۱۷۶۶

<div class="b" id="bn1"><div class="m1"><p>امروز ندانم بچه دست آمده‌ای</p></div>
<div class="m2"><p>کز اول بامداد مست آمده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خون دلم خوری ز دستت ندهم</p></div>
<div class="m2"><p>زیرا که به خون دل به دست آمده‌ای</p></div></div>