---
title: >-
    رباعی شمارهٔ ۱۲۵۵
---
# رباعی شمارهٔ ۱۲۵۵

<div class="b" id="bn1"><div class="m1"><p>شاعر نیم و ز شاعری نان نخورم</p></div>
<div class="m2"><p>وز فضل نلافم و غم آن نخورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فضل و هنرم یکی قدح میباشد</p></div>
<div class="m2"><p>وان نیز مگر ز دست جانان نخورم</p></div></div>