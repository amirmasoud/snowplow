---
title: >-
    رباعی شمارهٔ ۱۵۰
---
# رباعی شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>از بسکه دل تو دام حیلت افراخت</p></div>
<div class="m2"><p>خود را و ترا ز چشم رحمت انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانندهٔ فرعون خدا را نشناخت</p></div>
<div class="m2"><p>چون برق گرفت عالمی را بگداخت</p></div></div>