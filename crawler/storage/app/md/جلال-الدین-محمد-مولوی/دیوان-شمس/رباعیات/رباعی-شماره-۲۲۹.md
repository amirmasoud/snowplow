---
title: >-
    رباعی شمارهٔ ۲۲۹
---
# رباعی شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>با شاه هر آنکسی که در خرگاهست</p></div>
<div class="m2"><p>آن از کرم و لطف و عطای شاهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با شاه کجا رسی بهر بیخویشی</p></div>
<div class="m2"><p>زانجانب بیخودی هزاران راهست</p></div></div>