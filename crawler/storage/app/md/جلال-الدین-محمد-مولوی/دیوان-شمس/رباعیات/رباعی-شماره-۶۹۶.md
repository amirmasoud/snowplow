---
title: >-
    رباعی شمارهٔ ۶۹۶
---
# رباعی شمارهٔ ۶۹۶

<div class="b" id="bn1"><div class="m1"><p>دل در پی دلدار بسی تاخت و نشد</p></div>
<div class="m2"><p>هر خشک و تری که داشت درباخت و نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره به کنج سینه بنشست بمکر</p></div>
<div class="m2"><p>هر حیله و فن که داشت پرداخت و نشد</p></div></div>