---
title: >-
    رباعی شمارهٔ ۱۱۷۹
---
# رباعی شمارهٔ ۱۱۷۹

<div class="b" id="bn1"><div class="m1"><p>بر میکده وقف است دلم سرمستم</p></div>
<div class="m2"><p>جان نیز سبیل جام می‌کردستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جان و دلم همی نمی‌پیوستند</p></div>
<div class="m2"><p>آن هر دو بوی دادم از غم رستم</p></div></div>