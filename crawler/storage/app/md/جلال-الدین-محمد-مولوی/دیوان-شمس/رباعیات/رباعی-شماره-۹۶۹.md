---
title: >-
    رباعی شمارهٔ ۹۶۹
---
# رباعی شمارهٔ ۹۶۹

<div class="b" id="bn1"><div class="m1"><p>میگوید مرمرا نگار دلسوز</p></div>
<div class="m2"><p>میباید رفت چون به پایان شد روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شب تو برون میای از کتم عدم</p></div>
<div class="m2"><p>خورشید تو خویش را بدین چرخ بدوز</p></div></div>