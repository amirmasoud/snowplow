---
title: >-
    رباعی شمارهٔ ۴۷۱
---
# رباعی شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>آنرا که به ضاعت قناعت باشد</p></div>
<div class="m2"><p>هرگونه که خورد و خفت و طاعت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار تولا مکن الا به خدای</p></div>
<div class="m2"><p>کاین رغبت خلق نیم ساعت باشد</p></div></div>