---
title: >-
    رباعی شمارهٔ ۱۲۱۸
---
# رباعی شمارهٔ ۱۲۱۸

<div class="b" id="bn1"><div class="m1"><p>در چنگ توام بتا در آن چنگ خوشم</p></div>
<div class="m2"><p>گر جنگ کنی بکن در آن جنگ خوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ننگست ملامت بره عشق ترا</p></div>
<div class="m2"><p>من نام گرو کردم و با ننگ خوشم</p></div></div>