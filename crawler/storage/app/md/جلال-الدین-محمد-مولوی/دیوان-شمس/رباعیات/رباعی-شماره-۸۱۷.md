---
title: >-
    رباعی شمارهٔ ۸۱۷
---
# رباعی شمارهٔ ۸۱۷

<div class="b" id="bn1"><div class="m1"><p>مرغی که ز باغ پاکبازان باشد</p></div>
<div class="m2"><p>هم سرکش و هم سرخوش و شادان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر بکشد ز سرکشان میرسدش</p></div>
<div class="m2"><p>کاندر سر او غرور بازان باشد</p></div></div>