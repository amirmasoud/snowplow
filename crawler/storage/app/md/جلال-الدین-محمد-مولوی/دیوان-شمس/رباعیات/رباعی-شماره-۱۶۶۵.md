---
title: >-
    رباعی شمارهٔ ۱۶۶۵
---
# رباعی شمارهٔ ۱۶۶۵

<div class="b" id="bn1"><div class="m1"><p>آنی که به صد شفاعت و صد زاری</p></div>
<div class="m2"><p>بر پات یکی بوسه دهم نگذاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آب دهی مرا اگر آتش باری</p></div>
<div class="m2"><p>سلطان ولایتی و فرمانداری</p></div></div>