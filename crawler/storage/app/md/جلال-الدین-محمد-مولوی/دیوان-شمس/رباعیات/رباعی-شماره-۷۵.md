---
title: >-
    رباعی شمارهٔ ۷۵
---
# رباعی شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>هان ای سفری عزم کجایست کجا</p></div>
<div class="m2"><p>هرجا که روی نشسته‌ای در دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان غم دریاست ترا چون ماهی</p></div>
<div class="m2"><p>کافشاند لب خشک تو را در دریا</p></div></div>