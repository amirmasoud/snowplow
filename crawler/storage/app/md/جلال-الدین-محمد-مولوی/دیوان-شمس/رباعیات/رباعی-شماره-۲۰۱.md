---
title: >-
    رباعی شمارهٔ ۲۰۱
---
# رباعی شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>ای لعل و عقیق و در و دریا و درست</p></div>
<div class="m2"><p>فارغ از جای و پای بر جا و درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خواجهٔ روح و روح‌افزا و درست</p></div>
<div class="m2"><p>دیر آمدنت رواست دیرآ و درست</p></div></div>