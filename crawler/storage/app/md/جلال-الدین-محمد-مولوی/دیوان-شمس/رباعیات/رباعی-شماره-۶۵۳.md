---
title: >-
    رباعی شمارهٔ ۶۵۳
---
# رباعی شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>خورشید مگر بسته به پیشت میرد</p></div>
<div class="m2"><p>وان ماه جگر خسته به پیشت میرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان سرو و گل رسته به پیشت میرد</p></div>
<div class="m2"><p>وین دلشده پیوسته به پیشت میرد</p></div></div>