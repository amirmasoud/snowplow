---
title: >-
    رباعی شمارهٔ ۱۴۸۶
---
# رباعی شمارهٔ ۱۴۸۶

<div class="b" id="bn1"><div class="m1"><p>شب رفت و نرفت ای بت سیمین برمن</p></div>
<div class="m2"><p>سودای مناجات غمت از سر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب شب من توئی و نور روزم</p></div>
<div class="m2"><p>نه روز و نه شب چون تو نباشی بر من</p></div></div>