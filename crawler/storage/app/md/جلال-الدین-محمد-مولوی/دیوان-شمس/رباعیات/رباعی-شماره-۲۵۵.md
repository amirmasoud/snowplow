---
title: >-
    رباعی شمارهٔ ۲۵۵
---
# رباعی شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>بیرون ز جهان کفر و ایمان جائیست</p></div>
<div class="m2"><p>کانجا نه مقام هر تر و رعنائیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان باید داد و دل بشکرانهٔ جان</p></div>
<div class="m2"><p>آنرا که تمنای چنین مأوائیست</p></div></div>