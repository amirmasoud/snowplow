---
title: >-
    رباعی شمارهٔ ۱۰۳۵
---
# رباعی شمارهٔ ۱۰۳۵

<div class="b" id="bn1"><div class="m1"><p>در حلقهٔ مستان تو ای دلبر دوش</p></div>
<div class="m2"><p>میخانه درون کشیدم از خم سر جوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر یاد تو کاس و طاس تا وقت سحر</p></div>
<div class="m2"><p>میخوردم و میزدم همی دوش خروش</p></div></div>