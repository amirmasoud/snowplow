---
title: >-
    رباعی شمارهٔ ۹۰۵
---
# رباعی شمارهٔ ۹۰۵

<div class="b" id="bn1"><div class="m1"><p>طبعم چو حیات یافت از جلوهٔ فکر</p></div>
<div class="m2"><p>آورد عروس نظم در حجرهٔ ذکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر بیتی هزار دختر بنمود</p></div>
<div class="m2"><p>هر یک به مثال مریم آبستن و بکر</p></div></div>