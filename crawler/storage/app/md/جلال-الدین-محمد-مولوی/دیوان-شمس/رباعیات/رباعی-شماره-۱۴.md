---
title: >-
    رباعی شمارهٔ ۱۴
---
# رباعی شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ای آنکه چو آفتاب فرداست بیا</p></div>
<div class="m2"><p>بیرون تو برگ و باغ زرد است بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم بی‌تو غبار و گرد است بیا</p></div>
<div class="m2"><p>این مجلس عیش بی‌تو سرد است بیا</p></div></div>