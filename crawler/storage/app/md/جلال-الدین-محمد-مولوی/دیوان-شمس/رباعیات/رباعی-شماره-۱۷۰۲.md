---
title: >-
    رباعی شمارهٔ ۱۷۰۲
---
# رباعی شمارهٔ ۱۷۰۲

<div class="b" id="bn1"><div class="m1"><p>ای آنکه نظر به طعنه می‌اندازی</p></div>
<div class="m2"><p>بشناس دمی تو بازی از جان بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان غریب در جهان می‌سازی</p></div>
<div class="m2"><p>روزی دو فتاد مرغزی بارازی</p></div></div>