---
title: >-
    رباعی شمارهٔ ۱۷۹۷
---
# رباعی شمارهٔ ۱۷۹۷

<div class="b" id="bn1"><div class="m1"><p>پران باشی چو در صف یارانی</p></div>
<div class="m2"><p>پری باشی سقط چو بی ایشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پرانی تو حاکمی بر سر آن</p></div>
<div class="m2"><p>چون پر گشتی ز باد سرگردانی</p></div></div>