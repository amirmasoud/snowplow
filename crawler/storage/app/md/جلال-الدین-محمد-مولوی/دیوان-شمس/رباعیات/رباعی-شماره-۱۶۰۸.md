---
title: >-
    رباعی شمارهٔ ۱۶۰۸
---
# رباعی شمارهٔ ۱۶۰۸

<div class="b" id="bn1"><div class="m1"><p>ای دوست مرا دمدمه بسیار مده</p></div>
<div class="m2"><p>کاین دمدمه می‌خورد ز من هر که و مه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و سر تو که دم کنم پیش تو زه</p></div>
<div class="m2"><p>کز دمدمهٔ گرم کنم آب کرده</p></div></div>