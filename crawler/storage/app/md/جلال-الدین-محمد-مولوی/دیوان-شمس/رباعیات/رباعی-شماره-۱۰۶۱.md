---
title: >-
    رباعی شمارهٔ ۱۰۶۱
---
# رباعی شمارهٔ ۱۰۶۱

<div class="b" id="bn1"><div class="m1"><p>با زنگی امشب چو شدستی به مصاف</p></div>
<div class="m2"><p>از سینهٔ خود سینهٔ شب را بشکاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کعبهٔ عشاق طوافی چو کنی</p></div>
<div class="m2"><p>دریاب که کعبه می‌کند با تو طواف</p></div></div>