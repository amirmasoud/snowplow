---
title: >-
    رباعی شمارهٔ ۶۷۹
---
# رباعی شمارهٔ ۶۷۹

<div class="b" id="bn1"><div class="m1"><p>در مدرسهٔ عشق اگر قال بود</p></div>
<div class="m2"><p>کی فرق میان قال با حال بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق نداد هیچ مفتی فتوی</p></div>
<div class="m2"><p>در عشق زبان مفتیان لال بود</p></div></div>