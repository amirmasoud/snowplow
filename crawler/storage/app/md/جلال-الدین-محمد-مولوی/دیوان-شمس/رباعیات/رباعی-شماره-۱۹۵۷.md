---
title: >-
    رباعی شمارهٔ ۱۹۵۷
---
# رباعی شمارهٔ ۱۹۵۷

<div class="b" id="bn1"><div class="m1"><p>من بادم و تو برگ نلرزی چکنی</p></div>
<div class="m2"><p>کاری که منت دهم نورزی چکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سنگ زدم سبوی تو بشکستم</p></div>
<div class="m2"><p>صد گوهر و صد بحر نیرزی چکنی</p></div></div>