---
title: >-
    رباعی شمارهٔ ۱۹۹۰
---
# رباعی شمارهٔ ۱۹۹۰

<div class="b" id="bn1"><div class="m1"><p>همسایگی مست فزاید مستی</p></div>
<div class="m2"><p>چون مست شوی بازرهی از هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رستهٔ مردان چو نشستی رستی</p></div>
<div class="m2"><p>بر باده زنی ز آب و آتش دستی</p></div></div>