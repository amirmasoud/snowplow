---
title: >-
    رباعی شمارهٔ ۱۲۷۳
---
# رباعی شمارهٔ ۱۲۷۳

<div class="b" id="bn1"><div class="m1"><p>گاهی ز هوس دست زنان میباشم</p></div>
<div class="m2"><p>گاه از دوری دست گزان میباشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آب کنم دست که مه را گیرم</p></div>
<div class="m2"><p>مه گوید من بر آسمان میباشم</p></div></div>