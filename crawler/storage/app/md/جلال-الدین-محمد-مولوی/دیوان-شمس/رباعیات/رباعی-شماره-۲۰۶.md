---
title: >-
    رباعی شمارهٔ ۲۰۶
---
# رباعی شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>این جو که تراست هر کسی جویان نیست</p></div>
<div class="m2"><p>هر چرخ ز آب جوی تو گردان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکس نکشد کمان کمان ارزان نیست</p></div>
<div class="m2"><p>رستم باید که کار نامردان نیست</p></div></div>