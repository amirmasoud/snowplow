---
title: >-
    رباعی شمارهٔ ۱۱۴۵
---
# رباعی شمارهٔ ۱۱۴۵

<div class="b" id="bn1"><div class="m1"><p>امروز یکی گردش مستانه کنم</p></div>
<div class="m2"><p>وز کاسهٔ سر ساغر و پیمانه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز در این شهر همی گردم مست</p></div>
<div class="m2"><p>می‌جویم عاقلی که دیوانه کنم</p></div></div>