---
title: >-
    رباعی شمارهٔ ۱۲۸۸
---
# رباعی شمارهٔ ۱۲۸۸

<div class="b" id="bn1"><div class="m1"><p>گر ماه شوی بر آسمان کم نگرم</p></div>
<div class="m2"><p>ور بخت شوی رخت بسویت نبرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بیش اگر بر سر کویت گذرم</p></div>
<div class="m2"><p>فرمای که چون مار بکوبند سرم</p></div></div>