---
title: >-
    رباعی شمارهٔ ۱۴۷۵
---
# رباعی شمارهٔ ۱۴۷۵

<div class="b" id="bn1"><div class="m1"><p>دی از تو چنان بدم که گل در بستان</p></div>
<div class="m2"><p>امروز چنانم و چنان‌تر ز چنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چون نزنم دست که پابند منی</p></div>
<div class="m2"><p>چون پای نکوبم که توئی دست زنان</p></div></div>