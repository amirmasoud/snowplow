---
title: >-
    رباعی شمارهٔ ۱۵۸۰
---
# رباعی شمارهٔ ۱۵۸۰

<div class="b" id="bn1"><div class="m1"><p>گر هیچ ترا میل سوی ماست بگو</p></div>
<div class="m2"><p>ورنه که رهی عاشق و تنهاست بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هیچ مرا در دل تو جاست بگو</p></div>
<div class="m2"><p>گر هست بگو نیست بگو راست بگو</p></div></div>