---
title: >-
    رباعی شمارهٔ ۶۱۸
---
# رباعی شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>جانا تبش عشق به غایت برسید</p></div>
<div class="m2"><p>از شوق تو کارم به شکایت برسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارزانکه نخواهی که بنالم سحری</p></div>
<div class="m2"><p>دریاب که هنگام عنایت برسید</p></div></div>