---
title: >-
    رباعی شمارهٔ ۱۹۴۲
---
# رباعی شمارهٔ ۱۹۴۲

<div class="b" id="bn1"><div class="m1"><p>گوهر چه بود به بحر او جز سنگی</p></div>
<div class="m2"><p>گردون چه بود بر در او سرهنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دولت دوست هیچ چیزم کم نیست</p></div>
<div class="m2"><p>جز صبر که از صبر ندارم رنگی</p></div></div>