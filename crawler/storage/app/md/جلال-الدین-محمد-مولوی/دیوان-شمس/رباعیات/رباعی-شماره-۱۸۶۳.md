---
title: >-
    رباعی شمارهٔ ۱۸۶۳
---
# رباعی شمارهٔ ۱۸۶۳

<div class="b" id="bn1"><div class="m1"><p>در عالم حسن اینت سلطان که توئی</p></div>
<div class="m2"><p>در خطهٔ لطف شهره برهان که توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قالب عاشقان بی‌جان گشته</p></div>
<div class="m2"><p>انصاف بدادیم زهی جان که توئی</p></div></div>