---
title: >-
    رباعی شمارهٔ ۱۱۶۰
---
# رباعی شمارهٔ ۱۱۶۰

<div class="b" id="bn1"><div class="m1"><p>ای دل چو بهر خسی نشینی چکنم</p></div>
<div class="m2"><p>وز باغ مدام گل نچینی چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم همه از جمال او روشن شد</p></div>
<div class="m2"><p>تو دیده نداری که ببینی چکنم</p></div></div>