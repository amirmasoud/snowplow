---
title: >-
    رباعی شمارهٔ ۸۵۱
---
# رباعی شمارهٔ ۸۵۱

<div class="b" id="bn1"><div class="m1"><p>هر لحظه همی خوانمش از راه بعید</p></div>
<div class="m2"><p>کو سورهٔ یوسف است و قرآن مجید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که دلم خون شد و از دیده دوید</p></div>
<div class="m2"><p>گفت آنکه ترا دید کس را ندوید</p></div></div>