---
title: >-
    رباعی شمارهٔ ۴۸۶
---
# رباعی شمارهٔ ۴۸۶

<div class="b" id="bn1"><div class="m1"><p>آن عشق که برق و بوش تا فرق رسید</p></div>
<div class="m2"><p>مالم همه خورد و کار با دلق رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبی که از آن دامن خود میچیدم</p></div>
<div class="m2"><p>اکنون جوشیده است و تا حلق رسید</p></div></div>