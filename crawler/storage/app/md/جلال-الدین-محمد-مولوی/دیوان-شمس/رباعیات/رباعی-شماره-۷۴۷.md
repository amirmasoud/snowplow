---
title: >-
    رباعی شمارهٔ ۷۴۷
---
# رباعی شمارهٔ ۷۴۷

<div class="b" id="bn1"><div class="m1"><p>شیرین سخنی در دل ما میخندد</p></div>
<div class="m2"><p>بر خسرو شیرین سخنی می‌بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه تند کند مرا و او رام شود</p></div>
<div class="m2"><p>گه رام کند مرا و او می‌تندد</p></div></div>