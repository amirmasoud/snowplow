---
title: >-
    رباعی شمارهٔ ۱۴۵۵
---
# رباعی شمارهٔ ۱۴۵۵

<div class="b" id="bn1"><div class="m1"><p>چون بنده نه‌ای ندای شاهی میزن</p></div>
<div class="m2"><p>تیر نظر آنچنانکه خواهی میزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از خود و غیر خود مسلم گشتی</p></div>
<div class="m2"><p>بی‌خود بنشین کوس الهی میزن</p></div></div>