---
title: >-
    رباعی شمارهٔ ۵۷
---
# رباعی شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>عمریست ندیده‌ایم گلزار ترا</p></div>
<div class="m2"><p>وان نرگس پرخمار خمار ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنهان‌شده‌ای ز خلق مانند وفا</p></div>
<div class="m2"><p>دیریست ندیده‌ایم رخسار ترا</p></div></div>