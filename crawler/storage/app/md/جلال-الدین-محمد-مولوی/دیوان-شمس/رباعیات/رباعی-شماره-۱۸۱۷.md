---
title: >-
    رباعی شمارهٔ ۱۸۱۷
---
# رباعی شمارهٔ ۱۸۱۷

<div class="b" id="bn1"><div class="m1"><p>تا هشیاری به طعم مستی نرسی</p></div>
<div class="m2"><p>تا تن ندهی به جان پرستی نرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در غم عشق دوست چون آتش و آب</p></div>
<div class="m2"><p>از خود نشوی نیست به هستی نرسی</p></div></div>