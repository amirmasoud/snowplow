---
title: >-
    رباعی شمارهٔ ۱۵۱۹
---
# رباعی شمارهٔ ۱۵۱۹

<div class="b" id="bn1"><div class="m1"><p>هر روز خوش است منزلی بسپردن</p></div>
<div class="m2"><p>چون آب روان و فارغ از افسردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی رفت و حدیث دی چو دی هم بگذشت</p></div>
<div class="m2"><p>امروز حدیث تازه باید کردن</p></div></div>