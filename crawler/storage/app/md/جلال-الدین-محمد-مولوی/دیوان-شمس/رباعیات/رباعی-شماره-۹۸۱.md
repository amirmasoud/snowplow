---
title: >-
    رباعی شمارهٔ ۹۸۱
---
# رباعی شمارهٔ ۹۸۱

<div class="b" id="bn1"><div class="m1"><p>دارد به قدح می حرامی که مپرس</p></div>
<div class="m2"><p>یک دشمن جان شگرف حامی که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشم دارد شراب خامی که مپرس</p></div>
<div class="m2"><p>می‌خواند مرمرا به نامی که مپرس</p></div></div>