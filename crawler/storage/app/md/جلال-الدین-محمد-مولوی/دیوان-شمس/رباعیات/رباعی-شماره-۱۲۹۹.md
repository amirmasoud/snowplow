---
title: >-
    رباعی شمارهٔ ۱۲۹۹
---
# رباعی شمارهٔ ۱۲۹۹

<div class="b" id="bn1"><div class="m1"><p>گوئیکه به تن دور و به دل با یارم</p></div>
<div class="m2"><p>زنهار مپندار که من دل دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نقش خیال خود ببینی روزی</p></div>
<div class="m2"><p>فریاد کنی که من ز خود بیزارم</p></div></div>