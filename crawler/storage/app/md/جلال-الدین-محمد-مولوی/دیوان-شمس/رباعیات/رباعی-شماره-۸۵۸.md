---
title: >-
    رباعی شمارهٔ ۸۵۸
---
# رباعی شمارهٔ ۸۵۸

<div class="b" id="bn1"><div class="m1"><p>همواره خوشی و دلکشی نامیزد</p></div>
<div class="m2"><p>هشدار مکن کژ که قدح میریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم باد خاک بر سر کردن</p></div>
<div class="m2"><p>شک نیست که هر لحظه غباری خیزد</p></div></div>