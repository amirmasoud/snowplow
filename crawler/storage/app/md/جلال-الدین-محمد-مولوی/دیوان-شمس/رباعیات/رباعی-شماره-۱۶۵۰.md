---
title: >-
    رباعی شمارهٔ ۱۶۵۰
---
# رباعی شمارهٔ ۱۶۵۰

<div class="b" id="bn1"><div class="m1"><p>هم آینه‌ایم و هم لقائیم همه</p></div>
<div class="m2"><p>سرمست پیالدهٔ بقائیم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم دافع رنج و هم شفائیم همه</p></div>
<div class="m2"><p>هم آب حیات و هم سقائیم همه</p></div></div>