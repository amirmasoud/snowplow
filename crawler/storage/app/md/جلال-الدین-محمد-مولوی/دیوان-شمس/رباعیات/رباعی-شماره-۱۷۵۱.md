---
title: >-
    رباعی شمارهٔ ۱۷۵۱
---
# رباعی شمارهٔ ۱۷۵۱

<div class="b" id="bn1"><div class="m1"><p>ای گل تو ز لطف گلستان می‌خندی</p></div>
<div class="m2"><p>یا از دم عشق بلبلان می‌خندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا در رخ معشوق نهان می‌خندی</p></div>
<div class="m2"><p>چیزیت بدو ماند از آن می‌خندی</p></div></div>