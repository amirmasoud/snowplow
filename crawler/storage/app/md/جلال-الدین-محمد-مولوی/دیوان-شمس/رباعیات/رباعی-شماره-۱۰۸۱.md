---
title: >-
    رباعی شمارهٔ ۱۰۸۱
---
# رباعی شمارهٔ ۱۰۸۱

<div class="b" id="bn1"><div class="m1"><p>یک چند میان خلق کردیم درنگ</p></div>
<div class="m2"><p>ز ایشان بوفا نه بوی دیدیم نه رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن به که نهان شویم از دیدهٔ خلق</p></div>
<div class="m2"><p>چون آب در آهن و چو آتش در سنگ</p></div></div>