---
title: >-
    رباعی شمارهٔ ۳۴۳
---
# رباعی شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>زان می خوردم که روح پیمانه اوست</p></div>
<div class="m2"><p>زان مست شدم که عقل دیوانهٔ اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعی به من آمد آتشی در من زد</p></div>
<div class="m2"><p>آن شمع که آفتاب پروانهٔ اوست</p></div></div>