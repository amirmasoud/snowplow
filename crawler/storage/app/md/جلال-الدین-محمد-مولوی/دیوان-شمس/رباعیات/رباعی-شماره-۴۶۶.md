---
title: >-
    رباعی شمارهٔ ۴۶۶
---
# رباعی شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>آن دشمن دوست روی دیدی که چه کرد</p></div>
<div class="m2"><p>یا هیچ به غور آن رسیدی که چه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا همه آن کنم که رایت خواهد</p></div>
<div class="m2"><p>دیدی که چه گفت و هم شنیدی که چه کرد</p></div></div>