---
title: >-
    رباعی شمارهٔ ۶۰۱
---
# رباعی شمارهٔ ۶۰۱

<div class="b" id="bn1"><div class="m1"><p>بی‌یاری تو دل بسوی یار نشد</p></div>
<div class="m2"><p>تا لطف غمت ندیده غمخوار نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچیز که بسیار شود خار شود</p></div>
<div class="m2"><p>غمهای تو بسیار شد و خوار شد</p></div></div>