---
title: >-
    رباعی شمارهٔ ۷۴۱
---
# رباعی شمارهٔ ۷۴۱

<div class="b" id="bn1"><div class="m1"><p>شاهیست که تو هرچه بپوشی داند</p></div>
<div class="m2"><p>بی‌کام و زبان گر بخروشی داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس هوس سخن فروشی داند</p></div>
<div class="m2"><p>من بندهٔ آنم که خموشی داند</p></div></div>