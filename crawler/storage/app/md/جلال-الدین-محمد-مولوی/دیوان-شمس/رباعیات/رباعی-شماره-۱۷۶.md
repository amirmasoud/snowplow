---
title: >-
    رباعی شمارهٔ ۱۷۶
---
# رباعی شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>ای آب حیات قطره از آب رخت</p></div>
<div class="m2"><p>وی ماه فلک یک اثر از تاب رخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که شب دراز خواهم مهتاب</p></div>
<div class="m2"><p>آن شب شب زلف تست و مهتاب رخت</p></div></div>