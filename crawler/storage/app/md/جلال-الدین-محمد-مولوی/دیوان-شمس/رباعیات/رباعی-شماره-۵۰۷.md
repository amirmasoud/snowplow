---
title: >-
    رباعی شمارهٔ ۵۰۷
---
# رباعی شمارهٔ ۵۰۷

<div class="b" id="bn1"><div class="m1"><p>آن یار که عقلها شکارش میشد</p></div>
<div class="m2"><p>وان یار که کوه بیقرارش میشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که سر زلف بریدی گفتا</p></div>
<div class="m2"><p>بسیار سر اندر سر کارش میشد</p></div></div>