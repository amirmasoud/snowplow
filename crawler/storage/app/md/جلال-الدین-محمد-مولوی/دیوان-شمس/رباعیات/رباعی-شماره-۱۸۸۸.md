---
title: >-
    رباعی شمارهٔ ۱۸۸۸
---
# رباعی شمارهٔ ۱۸۸۸

<div class="b" id="bn1"><div class="m1"><p>روزی به خرابات گذر می‌کردی</p></div>
<div class="m2"><p>کژ کژ به کرشمه‌ای نظر می‌کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که جهان زیر و زبر می‌کردند</p></div>
<div class="m2"><p>چون کار جهان زیر و زبر می‌کردی</p></div></div>