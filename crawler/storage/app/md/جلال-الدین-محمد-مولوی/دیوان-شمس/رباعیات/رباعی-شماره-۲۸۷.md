---
title: >-
    رباعی شمارهٔ ۲۸۷
---
# رباعی شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>خون دلبر من میان دلداران نیست</p></div>
<div class="m2"><p>او را چون جهان هلاکت و پایان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خیره‌سری زنخ زند گو میزن</p></div>
<div class="m2"><p>معشوق ازین لطیفتر امکان نیست</p></div></div>