---
title: >-
    غزل شمارهٔ ۵۰۳
---
# غزل شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>بر شکرت جمع مگس‌ها چراست</p></div>
<div class="m2"><p>نکته لاحول مگسران کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نظری بر رخ او راست نیست</p></div>
<div class="m2"><p>جز نظری کو ز ازل بود راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسب خسان را به رخی پی بزن</p></div>
<div class="m2"><p>عشوه ده ای شاه که این روی ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشوه و عیاری و جور و دغل</p></div>
<div class="m2"><p>تو نکنی ور کنی از تو رواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو اگر سنگ رسد گوهرست</p></div>
<div class="m2"><p>گر تو کنی جور به از صد وفاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیره نظر چونک ببیند دو نقش</p></div>
<div class="m2"><p>جامه درد نعره زند کاین صفاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونک هر اندیشه خیالی گزید</p></div>
<div class="m2"><p>مجلس عشاق خیالش جداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کعبه چو از سنگ پرستان پرست</p></div>
<div class="m2"><p>روی به ما آر که قبله خداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنک از این قبله گدایی کند</p></div>
<div class="m2"><p>در نظرش سنجر و سلطان گداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز که به تبریز بر شمس دین</p></div>
<div class="m2"><p>روح نیاسود و نخفت و نخاست</p></div></div>