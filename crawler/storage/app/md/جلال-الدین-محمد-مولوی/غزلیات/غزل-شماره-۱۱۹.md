---
title: >-
    غزل شمارهٔ ۱۱۹
---
# غزل شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>برخیز و صبوح را بیارا</p></div>
<div class="m2"><p>پرلخلخه کن کنار ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش آر شراب رنگ آمیز</p></div>
<div class="m2"><p>ای ساقی خوب خوب سیما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از من پرسید کو چه ساقیست</p></div>
<div class="m2"><p>قندست و هزار رطل حلوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن ساغر پرعقار برریز</p></div>
<div class="m2"><p>بر وسوسه محال پیما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن می که چو صعوه زو بنوشد</p></div>
<div class="m2"><p>آهنگ کند به صید عنقا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان پیش که دررسد گرانی</p></div>
<div class="m2"><p>برجه سبک و میان ما آ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌گرد و چو ماه نور می‌ده</p></div>
<div class="m2"><p>حمرا می ده بدان حمیرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را همه مست و کف زنان کن</p></div>
<div class="m2"><p>وان گاه نظاره کن تماشا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گردش و شیوه‌های مستان</p></div>
<div class="m2"><p>در عربده‌های در علالا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گردن این فکنده آن دست</p></div>
<div class="m2"><p>کان شاه من و حبیب و مولا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او نیز ببرده روی چون گل</p></div>
<div class="m2"><p>می‌بوسد یار را کف پا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این کیسه گشاده از سخاوت</p></div>
<div class="m2"><p>که خرج کنید بی‌محابا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دستار و قبا فکنده آن نیز</p></div>
<div class="m2"><p>کاین را به گرو نهید فردا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صد مادر و صد پدر ندارد</p></div>
<div class="m2"><p>آن مهر که می‌بجوشد آن جا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این می آمد اصول خویشی</p></div>
<div class="m2"><p>کز سکر چنین شدند اعدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن عربده در شراب دنیاست</p></div>
<div class="m2"><p>در بزم خدا نباشد آن‌ها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نی شورش و نی قیست و نی جنگ</p></div>
<div class="m2"><p>ساقیست و شراب مجلس آرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاموش که ز سکر نفس کافر</p></div>
<div class="m2"><p>می‌گوید لا اله الا</p></div></div>