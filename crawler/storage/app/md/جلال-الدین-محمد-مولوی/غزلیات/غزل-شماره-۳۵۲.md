---
title: >-
    غزل شمارهٔ ۳۵۲
---
# غزل شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>چو با ما یار ما امروز جفتست</p></div>
<div class="m2"><p>بگویم آنچ هرگز کس نگفته‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه مستند این جا محرمانند</p></div>
<div class="m2"><p>میندیش از کسی غماز خفته‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خزان خفت و بهاران گشت بیدار</p></div>
<div class="m2"><p>نمی‌بینی درخت و گل شکفته‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر یک روز باقی باشد از دی</p></div>
<div class="m2"><p>زمین لب بسته است و گل نهفته‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلا در خواب کن اوباش تن را</p></div>
<div class="m2"><p>که گوهرهای جانی جمله سفته‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمش کن زردهی زان در نیابی</p></div>
<div class="m2"><p>وگر محرم شوی بستان که مفتست</p></div></div>