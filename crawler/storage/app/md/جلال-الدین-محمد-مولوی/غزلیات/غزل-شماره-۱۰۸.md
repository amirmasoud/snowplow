---
title: >-
    غزل شمارهٔ ۱۰۸
---
# غزل شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>به برج دل رسیدی بیست این جا</p></div>
<div class="m2"><p>چو آن مه را بدیدی بیست این جا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی این رخت خود را هر نواحی</p></div>
<div class="m2"><p>ز نادانی کشیدی بیست این جا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشد عمری و از خوبی آن مه</p></div>
<div class="m2"><p>به هر نوعی شنیدی بیست این جا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببین آن حسن را کز دیدن او</p></div>
<div class="m2"><p>بدید و نابدیدی بیست این جا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سینه تو که آن پستان شیرست</p></div>
<div class="m2"><p>که از شیرش چشیدی بیست این جا</p></div></div>