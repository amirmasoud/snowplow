---
title: >-
    غزل شمارهٔ ۴۶۶
---
# غزل شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>باز درآمد به بزم مجلسیان دوست دوست</p></div>
<div class="m2"><p>گر چه غلط می‌دهد نیست غلط اوست اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه خوش خوش شود گه همه آتش شود</p></div>
<div class="m2"><p>تعبیه‌های عجب یار مرا خوست خوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش وفا وی کند پشت به ما کی کند</p></div>
<div class="m2"><p>پشت ندارد چو شمع او همگی روست روست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پوست رها کن چو مار سر تو برآور ز یار</p></div>
<div class="m2"><p>مغز نداری مگر تا کی از این پوست پوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کی به جد تمام در هوس ماست ماست</p></div>
<div class="m2"><p>هر کی چو سیل روان در طلب جوست جوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هوس عشق او باغ پر از بلبل‌ست</p></div>
<div class="m2"><p>وز گل رخسار او مغز پر از بوست بوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفخر تبریزیان شمس حق آگه بود</p></div>
<div class="m2"><p>کز غم عشق این تنم بر مثل موست موست</p></div></div>