---
title: >-
    غزل شمارهٔ ۲۸۴
---
# غزل شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>اخی رایت جمالا سبا القلوب سبا</p></div>
<div class="m2"><p>و هل اتیک حدیث جلا العقول جلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الست من یتمنی الخلود فی طرب</p></div>
<div class="m2"><p>الا انتبه و تیقظ فقد اتاک اتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یقر عینک بدر و فی جبینته</p></div>
<div class="m2"><p>سعاده و مرام و عزه و سنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و سکره لفؤادی من شمائله</p></div>
<div class="m2"><p>کانها ملات کاسنا و اسقانا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجائب ظهرت بین صفو غرته</p></div>
<div class="m2"><p>تلالات لسناه بمهجتی و صفا</p></div></div>