---
title: >-
    غزل شمارهٔ ۴۹۸
---
# غزل شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>فعل نیکان محرض نیکیست</p></div>
<div class="m2"><p>همچو مطرب که باعث سیکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر تحریض بندگان یزدان</p></div>
<div class="m2"><p>از بد و نیک شاکر و شاکیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکر فرعون و شکر موسی کرد</p></div>
<div class="m2"><p>به بهانه ز حال ما حاکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنس فرعون هر کی در منیست</p></div>
<div class="m2"><p>جنس موسی هر آنک در پاکیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی غم یقین همه شادیست</p></div>
<div class="m2"><p>و از پی شادی تو غمناکیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک باشی گزید احمد از آن</p></div>
<div class="m2"><p>شاه معراج و پیک افلاکیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک باشی بروید از تو نبات</p></div>
<div class="m2"><p>گنج دل یافت آنک او خاکیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما همه چون یکیم بی‌من و تو</p></div>
<div class="m2"><p>پس خمش باش این سخن با کیست</p></div></div>