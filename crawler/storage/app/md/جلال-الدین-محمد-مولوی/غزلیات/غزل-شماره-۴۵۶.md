---
title: >-
    غزل شمارهٔ ۴۵۶
---
# غزل شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>ما را کنار گیر تو را خود کنار نیست</p></div>
<div class="m2"><p>عاشق نواختن به خدا هیچ عار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی حد و بی‌کناری نایی تو در کنار</p></div>
<div class="m2"><p>ای بحر بی‌امان که تو را زینهار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان شب که ماه خویش نمودی به عاشقان</p></div>
<div class="m2"><p>چون چرخ بی‌قرار کسی را قرار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز فیض بحر فضل تو ما را امید نیست</p></div>
<div class="m2"><p>جز گوهر ثنای تو ما را نثار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کار و بار عشق هوای تو دیده‌ام</p></div>
<div class="m2"><p>ما را تحیریست که با کار کار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک میر وانما که تو را او اسیر نیست</p></div>
<div class="m2"><p>یک شیر وانما که تو را او شکار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغان جسته‌ایم ز صد دام مردوار</p></div>
<div class="m2"><p>دامیست دام تو که از این سو مطار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمد رسول عشق تو چون ساقی صبوح</p></div>
<div class="m2"><p>با جام باده‌ای که مر آن را خمار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم که ناتوانم و رنجورم از فراق</p></div>
<div class="m2"><p>گفتا بگیر هین که گه اعتذار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم بهانه نیست تو خود حال من ببین</p></div>
<div class="m2"><p>مپذیر عذر بنده اگر زار زار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کارم به یک دم آمد از دمدمه جفا</p></div>
<div class="m2"><p>هنگام مردنست زمان عقار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتا که حال خویش فراموش کن بگیر</p></div>
<div class="m2"><p>زیرا که عاشقان را هیچ اختیار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا نگذری ز راحت و رنج و ز یاد خویش</p></div>
<div class="m2"><p>سوی مقربان وصالت گذار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آبی بزن از این می و بنشان غبار هوش</p></div>
<div class="m2"><p>جز ماه عشق هر چه بود جز غبار نیست</p></div></div>