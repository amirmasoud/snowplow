---
title: >-
    غزل شمارهٔ ۴۶۵
---
# غزل شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>کار ندارم جز این کارگه و کارم اوست</p></div>
<div class="m2"><p>لاف زنم لاف لاف چونک خریدارم اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوطی گویا شدم چون شکرستانم اوست</p></div>
<div class="m2"><p>بلبل بویا شدم چون گل و گلزارم اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر به ملک برزنم چون پر و بالم از اوست</p></div>
<div class="m2"><p>سر به فلک برزنم چون سر و دستارم اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان و دلم ساکنست زانک دل و جانم اوست</p></div>
<div class="m2"><p>قافله‌ام ایمنست قافله سالارم اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر مثل گلستان رنگرزم خم اوست</p></div>
<div class="m2"><p>بر مثل آفتاب تیغ گهردارم اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه جسمم چرا سجده گه خلق شد</p></div>
<div class="m2"><p>زانک به روز و به شب بر در و دیوارم اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست به دست جز او می‌نسپارد دلم</p></div>
<div class="m2"><p>زانک طبیب غم این دل بیمارم اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر رخ هر کس که نیست داغ غلامی او</p></div>
<div class="m2"><p>گر پدر من بود دشمن و اغیارم اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که تو مفلس شدی سنگ به دل برزدی</p></div>
<div class="m2"><p>صله ز من خواه زانک مخزن و انبارم اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاه مرا خوانده است چون نروم پیش شاه</p></div>
<div class="m2"><p>منکر او چون شوم چون همه اقرارم اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت خمش چند چند لاف تو و گفت تو</p></div>
<div class="m2"><p>من چه کنم ای عزیز گفتن بسیارم اوست</p></div></div>