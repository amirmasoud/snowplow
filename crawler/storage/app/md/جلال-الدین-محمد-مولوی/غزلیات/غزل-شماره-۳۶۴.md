---
title: >-
    غزل شمارهٔ ۳۶۴
---
# غزل شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>تا نقش خیال دوست با ماست</p></div>
<div class="m2"><p>ما را همه عمر خود تماشاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جا که وصال دوستانست</p></div>
<div class="m2"><p>والله که میان خانه صحراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وان جا که مراد دل برآید</p></div>
<div class="m2"><p>یک خار به از هزار خرماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بر سر کوی یار خسبیم</p></div>
<div class="m2"><p>بالین و لحاف ما ثریاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون در سر زلف یار پیچیم</p></div>
<div class="m2"><p>اندر شب قدر قدر ما راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون عکس جمال او بتابد</p></div>
<div class="m2"><p>کهسار و زمین حریر و دیباست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از باد چو بوی او بپرسیم</p></div>
<div class="m2"><p>در باد صدای چنگ و سرناست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خاک چو نام او نویسیم</p></div>
<div class="m2"><p>هر پاره خاک حور و حوراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آتش از او فسون بخوانیم</p></div>
<div class="m2"><p>زو آتش تیزاب سیماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قصه چه کنم که بر عدم نیز</p></div>
<div class="m2"><p>نامش چو بریم هستی افزاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن نکته که عشق او در آن جاست</p></div>
<div class="m2"><p>پرمغزتر از هزار جوزاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وان لحظه که عشق روی بنمود</p></div>
<div class="m2"><p>این‌ها همه از میانه برخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خامش که تمام ختم گشته‌ست</p></div>
<div class="m2"><p>کلی مراد حق تعالاست</p></div></div>