---
title: >-
    غزل شمارهٔ ۱۹۷
---
# غزل شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>ای بنده بازگرد به درگاه ما بیا</p></div>
<div class="m2"><p>بشنو ز آسمان‌ها حی علی الصلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درهای گلستان ز پی تو گشاده‌ایم</p></div>
<div class="m2"><p>در خارزار چند دوی ای برهنه پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان را من آفریدم و دردیش داده‌ام</p></div>
<div class="m2"><p>آن کس که درد داده همو سازدش دوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدی چو سرو خواهی در باغ عشق رو</p></div>
<div class="m2"><p>کاین چرخ کوژپشت کند قد تو دوتا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغی که برگ و شاخش گویا و زنده‌اند</p></div>
<div class="m2"><p>باغی که جان ندارد آن نیست جان فزا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای زنده زاده چونی از گند مردگان</p></div>
<div class="m2"><p>خود تاسه می نگیرد از این مردگان تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دو جهان پر است ز حی حیات بخش</p></div>
<div class="m2"><p>با جان پنج روزه قناعت مکن ز ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان‌ها شمار ذره معلق همی‌زنند</p></div>
<div class="m2"><p>هر یک چو آفتاب در افلاک کبریا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایشان چو ما ز اول خفاش بوده‌اند</p></div>
<div class="m2"><p>خفاش شمس گشت از آن بخشش و عطا</p></div></div>