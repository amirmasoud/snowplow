---
title: >-
    غزل شمارهٔ ۱۲۸
---
# غزل شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>ما را سفری فتاد بی‌ما</p></div>
<div class="m2"><p>آن جا دل ما گشاد بی‌ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مه که ز ما نهان همی‌شد</p></div>
<div class="m2"><p>رخ بر رخ ما نهاد بی‌ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در غم دوست جان بدادیم</p></div>
<div class="m2"><p>ما را غم او بزاد بی‌ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماییم همیشه مست بی‌می</p></div>
<div class="m2"><p>ماییم همیشه شاد بی‌ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را مکنید یاد هرگز</p></div>
<div class="m2"><p>ما خود هستیم یاد بی‌ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی ما شده‌ایم شاد گوییم</p></div>
<div class="m2"><p>ای ما که همیشه باد بی‌ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درها همه بسته بود بر ما</p></div>
<div class="m2"><p>بگشود چو راه داد بی‌ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با ما دل کیقباد بنده‌ست</p></div>
<div class="m2"><p>بنده‌ست چو کیقباد بی‌ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماییم ز نیک و بد رهیده</p></div>
<div class="m2"><p>از طاعت و از فساد بی‌ما</p></div></div>