---
title: >-
    غزل شمارهٔ ۴۰۷
---
# غزل شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>چشم پرنور که مست نظر جانانست</p></div>
<div class="m2"><p>ماه از او چشم گرفتست و فلک لرزانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصه آن لحظه که از حضرت حق نور کشد</p></div>
<div class="m2"><p>سجده گاه ملک و قبله هر انسانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او سر ننهد بر کف پایش آن دم</p></div>
<div class="m2"><p>بهر ناموس منی آن نفس او شیطانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و آنک آن لحظه نبیند اثر نور برو</p></div>
<div class="m2"><p>او کم از دیو بود زانک تن بی‌جانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل به جا دار در آن طلعت باهیبت او</p></div>
<div class="m2"><p>گر تو مردی که رخش قبله گه مردانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست بردار ز سینه چه نگه می‌داری</p></div>
<div class="m2"><p>جان در آن لحظه بده شاد که مقصود آنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمله را آب درانداز و در آن آتش شو</p></div>
<div class="m2"><p>کآتش چهره او چشمه گه حیوانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر برآور ز میان دل شمس تبریز</p></div>
<div class="m2"><p>کو خدیو ابد و خسرو هر فرمانست</p></div></div>