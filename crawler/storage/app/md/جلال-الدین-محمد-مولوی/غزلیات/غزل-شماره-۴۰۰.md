---
title: >-
    غزل شمارهٔ ۴۰۰
---
# غزل شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>چون نظر کردن همه اوصاف خوب اندر دلست</p></div>
<div class="m2"><p>وین همه اوصاف رسوا معدنش آب و گلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هوا و شهوت ای جان آب و گل می صد شود</p></div>
<div class="m2"><p>مشکل این ترک هوا و کاشف هر مشکلست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وین تعلل بهر ترکش دافع صد علتست</p></div>
<div class="m2"><p>چون بشد علت ز تو پس نقل منزل منزلست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک شرطی کن تو با خود تا که شرطی نشکنی</p></div>
<div class="m2"><p>ور نه علت باقی و درمانت محو و زایلست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چونک طبعت خو کند با شرط تندش بعد از آن</p></div>
<div class="m2"><p>صد هزاران حاصل جان از درونت حاصلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس تو را آیینه گردد این دل آهن چنانک</p></div>
<div class="m2"><p>هر دمی رویی نماید روی آن کو کاهلست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس تو را مطرب شود در عیش و هم ساقی شود</p></div>
<div class="m2"><p>آن امانت چونک شد محمول جان را حاملست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فارغ آیی بعد از آن از شغل و هم از فارغی</p></div>
<div class="m2"><p>شهره گردد از تو آن گنجی که آن بس خاملست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه حلواها خوری شیرین نگردد جان تو</p></div>
<div class="m2"><p>ذوق آن برقی بود تا در دهان آکلست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این طبیعت کور و کر گر نیست پس چون آزمود</p></div>
<div class="m2"><p>کاین حجاب و حائل‌ست آن سوی آن چون مایلست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک طبع از اصل رنج و غصه‌ها بررسته‌ست</p></div>
<div class="m2"><p>در پی رنج و بلاها عاشق بی‌طایلست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در تواضع‌های طبعت سر نخوت را نگر</p></div>
<div class="m2"><p>و اندر آن کبرش تواضع‌های بی‌حد شاکلست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر حدیث طبع را تو پرورش‌هایی بدش</p></div>
<div class="m2"><p>شرح و تأویلی بکن وادانک این بی‌حائلست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر یکی بیتی جمال بیت دیگر دانک هست</p></div>
<div class="m2"><p>با مؤید این طریقت ره روان را شاغلست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور تو را خوف مطالب باشد از اشهادها</p></div>
<div class="m2"><p>از خدا می‌خواه شیرینی اجل کان آجلست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر طرف رنجی دگرگون فرض کن آن گاه برو</p></div>
<div class="m2"><p>جز به سوی بی‌سوی‌ها کان دگر بی‌حاصلست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو وثاق مار آیی از پی ماری دگر</p></div>
<div class="m2"><p>غصه ماران ببینی زانک این چون سلسله‌ست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا نگویی مار را از خویش عذری زهرناک</p></div>
<div class="m2"><p>وان گهت او متهم دارد که این هم باطلست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از حدیث شمس دین آن فخر تبریز صفا</p></div>
<div class="m2"><p>آن مزاجش گرم باید کاین نه کار پلپلست</p></div></div>