---
title: >-
    غزل شمارهٔ ۵۲۰
---
# غزل شمارهٔ ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>ای مبارک ز تو صبوح و صباح</p></div>
<div class="m2"><p>ای مظفر فر از تو قلب و جناح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شراب طهور از کف حور</p></div>
<div class="m2"><p>بر حریفان مجلس تو مباح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای گشاده هزار در بر ما</p></div>
<div class="m2"><p>وی بداده به دست ما مفتاح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانمودی هر آنچ می‌گویند</p></div>
<div class="m2"><p>مؤذنان صبح فالق الاصباح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچ دادی عوض نمی‌خواهی</p></div>
<div class="m2"><p>گر چه گفتند السماح رباح</p></div></div>