---
title: >-
    غزل شمارهٔ ۱۰۳
---
# غزل شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>دل و جان را در این حضرت بپالا</p></div>
<div class="m2"><p>چو صافی شد رود صافی به بالا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر خواهی که ز آب صاف نوشی</p></div>
<div class="m2"><p>لب خود را به هر دردی میالا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این سیلاب درد او پاک ماند</p></div>
<div class="m2"><p>که جانبازست و چست و بی‌مبالا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نپرد عقل جزوی زین عقیله</p></div>
<div class="m2"><p>چو نبود عقل کل بر جزو لالا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نلرزد دست وقت زر شمردن</p></div>
<div class="m2"><p>چو بازرگان بداند قدر کالا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گرگینست وگر خارست این حرص</p></div>
<div class="m2"><p>کسی خود را بر این گرگین ممالا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شد ناسور بر گرگین چنین گر</p></div>
<div class="m2"><p>طلی سازش به ذکر حق تعالا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر خواهی که این در باز گردد</p></div>
<div class="m2"><p>سوی این در روان و بی‌ملال آ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رها کن صدر و ناموس و تکبر</p></div>
<div class="m2"><p>میان جان بجو صدر معلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلاه رفعت و تاج سلیمان</p></div>
<div class="m2"><p>به هر کل کی رسد حاشا و کلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمش کردم سخن کوتاه خوشتر</p></div>
<div class="m2"><p>که این ساعت نمی‌گنجد علالا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جواب آن غزل که گفت شاعر</p></div>
<div class="m2"><p>بقایی شاء لیس هم ارتحالا</p></div></div>