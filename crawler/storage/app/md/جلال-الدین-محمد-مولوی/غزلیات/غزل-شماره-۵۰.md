---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>ای بگرفته از وفا گوشه کران چرا چرا</p></div>
<div class="m2"><p>بر من خسته کرده‌ای روی گران چرا چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دل من که جای تست کارگه وفای تست</p></div>
<div class="m2"><p>هر نفسی همی‌زنی زخم سنان چرا چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر نو به گوهری برد سبق ز مشتری</p></div>
<div class="m2"><p>جان و جهان همی‌بری جان و جهان چرا چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمه خضر و کوثری ز آب حیات خوشتری</p></div>
<div class="m2"><p>ز آتش هجر تو منم خشک دهان چرا چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر تو جان نهان بود مهر تو بی‌نشان بود</p></div>
<div class="m2"><p>در دل من ز بهر تو نقش و نشان چرا چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت که جان جان منم دیدن جان طمع مکن</p></div>
<div class="m2"><p>ای بنموده روی تو صورت جان چرا چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای تو به نور مستقل وی ز تو اختران خجل</p></div>
<div class="m2"><p>بس دودلی میان دل ز ابر گمان چرا چرا</p></div></div>