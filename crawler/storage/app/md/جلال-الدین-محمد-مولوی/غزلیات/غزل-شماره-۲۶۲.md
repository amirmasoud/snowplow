---
title: >-
    غزل شمارهٔ ۲۶۲
---
# غزل شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>فیما تری فیما تری یا من یری و لا یری</p></div>
<div class="m2"><p>العیش فی اکنافنا و الموت فی ارکاننا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ان تدننا طوبی لنا ان تحفنا یا ویلنا</p></div>
<div class="m2"><p>یا نور ضؤ ناظرا یا خاطرا مخاطرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندعوک ربا حاضرا من قلبنا تفاخرا</p></div>
<div class="m2"><p>فکن لنا فی ذلنا برا کریما غافرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من می‌روم توکلی در این ره و در این سرا</p></div>
<div class="m2"><p>اگر نواله‌ای رسد نیمی مرا نیمی تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود کی رود کشتی در او که او تهی بیرون رود</p></div>
<div class="m2"><p>کیل گهر همی‌رسد بر مشتری و مشترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیل گهر همی‌رسد قرص قمر همی‌رسد</p></div>
<div class="m2"><p>نور بصر همی‌رسد اندکترین چیزها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش اندرآ در انجمن جز بر شکر لگد مزن</p></div>
<div class="m2"><p>جز بر قرابی‌ها مزن جر بر بتان جان فزا</p></div></div>