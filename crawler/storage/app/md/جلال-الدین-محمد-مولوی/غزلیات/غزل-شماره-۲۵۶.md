---
title: >-
    غزل شمارهٔ ۲۵۶
---
# غزل شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>داد دهی ساغر و پیمانه را</p></div>
<div class="m2"><p>مایه دهی مجلس و میخانه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست کنی نرگس مخمور را</p></div>
<div class="m2"><p>پیش کشی آن بت دردانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز ز خداوندی تو کی رسد</p></div>
<div class="m2"><p>صبر و قرار این دل دیوانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ برآور هله ای آفتاب</p></div>
<div class="m2"><p>نور ده این گوشه ویرانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قاف تویی مسکن سیمرغ را</p></div>
<div class="m2"><p>شمع تویی جان چو پروانه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمه حیوان بگشا هر طرف</p></div>
<div class="m2"><p>نقل کن آن قصه و افسانه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست کن ای ساقی و در کار کش</p></div>
<div class="m2"><p>این بدن کافر بیگانه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نکند رام چنین دیو را</p></div>
<div class="m2"><p>پس چه شد آن ساغر مردانه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیم دلی را به چه آرد که او</p></div>
<div class="m2"><p>پست کند صد دل فرزانه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پگه امروز چه خوش مجلسیست</p></div>
<div class="m2"><p>آن صنم و فتنه فتانه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بشکند آن چشم تو صد عهد را</p></div>
<div class="m2"><p>مست کند زلف تو صد شانه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک نفسی بام برآ ای صنم</p></div>
<div class="m2"><p>رقص درآر استن حنانه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شرح فتحنا و اشارات آن</p></div>
<div class="m2"><p>قفل بگوید سر دندانه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه بگوید شنود پیش من</p></div>
<div class="m2"><p>ترک کنم گفت غلامانه را</p></div></div>