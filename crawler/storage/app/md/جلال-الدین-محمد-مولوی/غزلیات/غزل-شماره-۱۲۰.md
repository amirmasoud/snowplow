---
title: >-
    غزل شمارهٔ ۱۲۰
---
# غزل شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>تا چند تو پس روی به پیش آ</p></div>
<div class="m2"><p>در کفر مرو به سوی کیش آ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نیش تو نوش بین به نیش آ</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند به صورت از زمینی</p></div>
<div class="m2"><p>پس رشته گوهر یقینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر مخزن نور حق امینی</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود را چو به بیخودی ببستی</p></div>
<div class="m2"><p>می‌دانک تو از خودی برستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز بند هزار دام جستی</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پشت خلیفه‌ای بزادی</p></div>
<div class="m2"><p>چشمی به جهان دون گشادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آوه که بدین قدر تو شادی</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چند طلسم این جهانی</p></div>
<div class="m2"><p>در باطن خویشتن تو کانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگشای دو دیده نهانی</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون زاده پرتو جلالی</p></div>
<div class="m2"><p>وز طالع سعد نیک فالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از هر عدمی تو چند نالی</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لعلی به میان سنگ خارا</p></div>
<div class="m2"><p>تا چند غلط دهی تو ما را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در چشم تو ظاهرست یارا</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون از بر یار سرکش آیی</p></div>
<div class="m2"><p>سرمست و لطیف و دلکش آیی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با چشم خوش و پرآتش آیی</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در پیش تو داشت جام باقی</p></div>
<div class="m2"><p>شمس تبریز شاه و ساقی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سبحان الله زهی رواقی</p></div>
<div class="m2"><p>آخر تو به اصل اصل خویش آ</p></div></div>