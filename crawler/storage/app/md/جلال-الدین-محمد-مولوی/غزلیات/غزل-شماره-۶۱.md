---
title: >-
    غزل شمارهٔ ۶۱
---
# غزل شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>هلا ای زهره زهرا بکش آن گوش زهرا را</p></div>
<div class="m2"><p>تقاضایی نهادستی در این جذبه دل ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم ناکام کام تو برای صید و دام تو</p></div>
<div class="m2"><p>گهی بر رکن بام تو گهی بگرفته صحرا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه داند دام بیچاره فریب مرغ آواره</p></div>
<div class="m2"><p>چه داند یوسف مصری نتیجه شور و غوغا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریبان گیر و این جا کش کسی را که تو خواهی خوش</p></div>
<div class="m2"><p>که من دامم تو صیادی چه پنهان صنعتی یارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شهر لوط ویرانم چو چشم لوط حیرانم</p></div>
<div class="m2"><p>سبب خواهم که واپرسم ندارم زهره و یارا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر عطار عاشق بد سنایی شاه و فایق بد</p></div>
<div class="m2"><p>نه اینم من نه آنم من که گم کردم سر و پا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی آهم کز این آهم بسوزد دشت و خرگاهم</p></div>
<div class="m2"><p>یکی گوشم که من وقفم شهنشاه شکرخا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمش کن در خموشی جان کشد چون کهربا آن را</p></div>
<div class="m2"><p>که جانش مستعد باشد کشاکش‌های بالا را</p></div></div>