---
title: >-
    غزل شمارهٔ ۳۹
---
# غزل شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>آه که آن صدر سرا می‌ندهد بار مرا</p></div>
<div class="m2"><p>می‌نکند محرم جان محرم اسرار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نغزی و خوبی و فرش آتش تیز نظرش</p></div>
<div class="m2"><p>پرسش همچون شکرش کرد گرفتار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت مرا مهر تو کو رنگ تو کو فر تو کو</p></div>
<div class="m2"><p>رنگ کجا ماند و بو ساعت دیدار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غرقه جوی کرمم بنده آن صبحدمم</p></div>
<div class="m2"><p>کان گل خوش بوی کشد جانب گلزار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که به جوبار بود جامه بر او بار بود</p></div>
<div class="m2"><p>چند زیانست و گران خرقه و دستار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملکت و اسباب کز این ماه رخان شکرین</p></div>
<div class="m2"><p>هست به معنی چو بود یار وفادار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستگه و پیشه تو را دانش و اندیشه تو را</p></div>
<div class="m2"><p>شیر تو را بیشه تو را آهوی تاتار مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست کند هست کند بی‌دل و بی‌دست کند</p></div>
<div class="m2"><p>باده دهد مست کند ساقی خمار مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل قلاش مکن فتنه و پرخاش مکن</p></div>
<div class="m2"><p>شهره مکن فاش مکن بر سر بازار مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر شکند پند مرا زفت کند بند مرا</p></div>
<div class="m2"><p>بر طمع ساختن یار خریدار مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیش مزن دم ز دوی دو دو مگو چون ثنوی</p></div>
<div class="m2"><p>اصل سبب را بطلب بس شد از آثار مرا</p></div></div>