---
title: >-
    غزل شمارهٔ ۳۳۵
---
# غزل شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>همه خوف آدمی را از درونست</p></div>
<div class="m2"><p>ولیکن هوش او دایم برونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون را می‌نوازد همچو یوسف</p></div>
<div class="m2"><p>درون گرگی‌ست کو در قصد خونست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدرد زهره او گر نبیند</p></div>
<div class="m2"><p>درون را کو به زشتی شکل چونست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان زشتی به یک حمله بمیرد</p></div>
<div class="m2"><p>ولیکن آدمی او را زبونست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الف گشت‌ست نون می‌بایدش ساخت</p></div>
<div class="m2"><p>که تا گردد الف چیزی که نونست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه خود عنایات خداوند</p></div>
<div class="m2"><p>بدیدستی چه امکان سکون‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه عالم بد نه آدم بد نه روحی</p></div>
<div class="m2"><p>که صافی و لطیف و آبگون‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که او را بود حکم و پادشاهی</p></div>
<div class="m2"><p>نپنداری که این کار از کنونست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌گویم که در تقدیر شه بود</p></div>
<div class="m2"><p>حقیقت بود و صد چندین فزونست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خداوندی شمس الدین تبریز</p></div>
<div class="m2"><p>ورای هفت چرخ نیلگونست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زیر ران او تقدیر رامست</p></div>
<div class="m2"><p>اگر چه نیک تندست و حرونست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو عقل کل بویی برد از وی</p></div>
<div class="m2"><p>شب و روز از هوس اندر جنونست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که پیش همت او عقل دیده‌ست</p></div>
<div class="m2"><p>که همت‌های عالی جمله دونست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کدامین سوی جویم خدمتش را</p></div>
<div class="m2"><p>که منزلگاه او بالای سونست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر آن مشکل که شیران حل نکردند</p></div>
<div class="m2"><p>بر او جمله بازی و فسونست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگفتم هیچ رمزی تا بدانی</p></div>
<div class="m2"><p>ز عین حال او این‌ها شجونست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا تبریز خاک توست کحلم</p></div>
<div class="m2"><p>که در خاکت عجایب‌ها فنونست</p></div></div>