---
title: >-
    غزل شمارهٔ ۱۷۹
---
# غزل شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>گر تو عودی سوی این مجمر بیا</p></div>
<div class="m2"><p>ور برانندت ز بام از در بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسفی از چاه و زندان چاره نیست</p></div>
<div class="m2"><p>سوی زهر قهر چون شکر بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتنت الله اکبر رسمی است</p></div>
<div class="m2"><p>گر تو آن اکبری اکبر بیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون می احمر سگان هم می‌خورند</p></div>
<div class="m2"><p>گر تو شیری چون می احمر بیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زر چه جویی مس خود را زر بساز</p></div>
<div class="m2"><p>گر نباشد زر تو سیمین بر بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اغنیا خشک و فقیران چشم تر</p></div>
<div class="m2"><p>عاشقا بی‌شکل خشک و تر بیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صفت‌های ملک را محرمی</p></div>
<div class="m2"><p>چون ملک بی‌ماده و بی‌نر بیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور صفات دل گرفتی در سفر</p></div>
<div class="m2"><p>همچو دل بی‌پا بیا بی‌سر بیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون لب لعلش صلایی می‌دهد</p></div>
<div class="m2"><p>گر نه‌ای چون خاره و مرمر بیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ز شمس الدین جهان پرنور شد</p></div>
<div class="m2"><p>سوی تبریز آ دلا بر سر بیا</p></div></div>