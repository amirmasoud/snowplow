---
title: >-
    غزل شمارهٔ ۲۵
---
# غزل شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>من دی نگفتم مر تو را کای بی‌نظیر خوش لقا</p></div>
<div class="m2"><p>ای قد مه از رشک تو چون آسمان گشته دوتا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز صد چندان شدی حاجب بدی سلطان شدی</p></div>
<div class="m2"><p>هم یوسف کنعان شدی هم فر نور مصطفی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امشب ستایمت ای پری فردا ز گفتن بگذری</p></div>
<div class="m2"><p>فردا زمین و آسمان در شرح تو باشد فنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امشب غنیمت دارمت باشم غلام و چاکرت</p></div>
<div class="m2"><p>فردا ملک بی‌هش شود هم عرش بشکافد قبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگه برآید صرصری نی بام ماند نه دری</p></div>
<div class="m2"><p>زین پشگان پر کی زند چونک ندارد پیل پا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز از میان صرصرش درتابد آن حسن و فرش</p></div>
<div class="m2"><p>هر ذره‌ای خندان شود در فر آن شمس الضحی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تعلیم گیرد ذره‌ها زان آفتاب خوش لقا</p></div>
<div class="m2"><p>صد ذرگی دلربا کان‌ها نبودش ز ابتدا</p></div></div>