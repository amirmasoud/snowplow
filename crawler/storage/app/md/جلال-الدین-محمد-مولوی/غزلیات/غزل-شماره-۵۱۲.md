---
title: >-
    غزل شمارهٔ ۵۱۲
---
# غزل شمارهٔ ۵۱۲

<div class="b" id="bn1"><div class="m1"><p>صبر مرا آینه بیماریست</p></div>
<div class="m2"><p>آینه عاشق غمخواریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد نباشد ننماید صبور</p></div>
<div class="m2"><p>که دل او روشن یا تاریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینه جویی‌ست نشان جمال</p></div>
<div class="m2"><p>که رخم از عیب و کلف عاریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور کلفی باشد عاریتیست</p></div>
<div class="m2"><p>قابل داروست و تب افشاریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آینه رنج ز فرعون دور</p></div>
<div class="m2"><p>کان رخ او رنگی و زنگاریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند هزاران سر طفلان برید</p></div>
<div class="m2"><p>کم ز قضا دردسری ساریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من در آن خوف ببندم تمام</p></div>
<div class="m2"><p>چون که مرا حکم و شهی جاریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت قضا بر سر و سبلت مخند</p></div>
<div class="m2"><p>کاین قلمی رفته ز جباریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کور شو امروز که موسی رسید</p></div>
<div class="m2"><p>در کف او خنجر قهاریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلق بکش پیش وی و سر مپیچ</p></div>
<div class="m2"><p>کاین نه زمان فن و مکاریست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سبط که سرشان بشکستی به ظلم</p></div>
<div class="m2"><p>بعد توشان دولت و پاداریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خار زدی در دل و در دیدشان</p></div>
<div class="m2"><p>این دمشان نوبت گلزاریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خلق مرا زهر خورانیده‌ای</p></div>
<div class="m2"><p>از منشان داد شکرباریست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از تو کشیدند خمار دراز</p></div>
<div class="m2"><p>تا به ابدشان می و خماریست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیزم دیک فقرا ظالمست</p></div>
<div class="m2"><p>پخته بدو گردد کو ناریست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دم نزدم زان که دم من سکست</p></div>
<div class="m2"><p>نوبت خاموشی و ستاریست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خامش کن که تا بگوید حبیب</p></div>
<div class="m2"><p>آن سخنان کز همه متواریست</p></div></div>