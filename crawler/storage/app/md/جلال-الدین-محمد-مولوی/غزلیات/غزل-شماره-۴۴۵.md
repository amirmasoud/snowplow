---
title: >-
    غزل شمارهٔ ۴۴۵
---
# غزل شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>این طرفه آتشی که دمی برقرار نیست</p></div>
<div class="m2"><p>گر نزد یار باشد وگر نزد یار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت چه پای دارد کو را ثبات نیست</p></div>
<div class="m2"><p>معنی چه دست گیرد چون آشکار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم شکارگاه و خلایق همه شکار</p></div>
<div class="m2"><p>غیر نشانه‌ای ز امیر شکار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سوی کار و بار که ما میر و مهتریم</p></div>
<div class="m2"><p>وان سو که بارگاه امیرست بار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای روح دست برکن و بنمای رنگ خوش</p></div>
<div class="m2"><p>کاین‌ها همه به جز کف و نقش و نگار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا غبار خیزد آن جای لشکرست</p></div>
<div class="m2"><p>کآتش همیشه بی‌تف و دود و بخار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو مرد را ز گرد ندانی چه مردیست</p></div>
<div class="m2"><p>در گرد مرد جوی که با گرد کار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای نیکبخت اگر تو نجویی بجویدت</p></div>
<div class="m2"><p>جوینده‌ای که رحمت وی را شمار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیلت چو دررباید دانی که در رهش</p></div>
<div class="m2"><p>هست اختیار خلق ولیک اختیار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در فقر عهد کردم تا حرف کم کنم</p></div>
<div class="m2"><p>اما گلی که دید که پهلویش خار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما خار این گلیم برادر گواه باش</p></div>
<div class="m2"><p>این جنس خار بودن فخرست عار نیست</p></div></div>