---
title: >-
    غزل شمارهٔ ۳۴۴
---
# غزل شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>به جان تو که سوگند عظیمست</p></div>
<div class="m2"><p>که جانم بی‌تو دربند عظیمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه خضر سیرآب حیاتست</p></div>
<div class="m2"><p>به لعلت آرزومند عظیمست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن‌ها دارم از تو با تو بسیار</p></div>
<div class="m2"><p>ولی خاموشیم پند عظیمست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن کز بیم تو خاموش باشد</p></div>
<div class="m2"><p>اگر چه خر خردمند عظیمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن کس کو هنر را ترک گوید</p></div>
<div class="m2"><p>ز بهر تو هنرمند عظیمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکندم خویش را چون سایه پیشت</p></div>
<div class="m2"><p>فکندن پیشت افکند عظیمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که بغداد تو را داد بزرگست</p></div>
<div class="m2"><p>سمرقند تو را قند عظیمست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حریصم کرد طمع داد قندت</p></div>
<div class="m2"><p>اگر چه بنده خرسند عظیمست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بریدستی مرا از خویش و پیوند</p></div>
<div class="m2"><p>که دل را با تو پیوند عظیمست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خمش کن همچو عشق ای زاده عشق</p></div>
<div class="m2"><p>اگر چه گفت فرزند عظیمست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رکاب شمس تبریزی گرفتم</p></div>
<div class="m2"><p>که زین شمس زرکند عظیمست</p></div></div>