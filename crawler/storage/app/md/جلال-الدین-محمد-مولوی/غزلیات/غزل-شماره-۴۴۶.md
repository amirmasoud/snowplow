---
title: >-
    غزل شمارهٔ ۴۴۶
---
# غزل شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>گر چپ و راست طعنه و تشنیع بیهده‌ست</p></div>
<div class="m2"><p>از عشق برنگردد آن کس که دلشده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه نور می‌فشاند و سگ بانگ می‌کند</p></div>
<div class="m2"><p>مه را چه جرم خاصیت سگ چنین بده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوهست نیست که که به بادی ز جا رود</p></div>
<div class="m2"><p>آن گله پشه‌ست که بادیش ره زده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر قاعده است این که ملامت بود ز عشق</p></div>
<div class="m2"><p>کری گوش عشق از آن نیز قاعده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ویرانی دو کون در این ره عمارتست</p></div>
<div class="m2"><p>ترک همه فواید در عشق فایده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیسی ز چرخ چارم می‌گوید الصلا</p></div>
<div class="m2"><p>دست و دهان بشوی که هنگام مایده‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو محو یار شو به خرابات نیستی</p></div>
<div class="m2"><p>هر جا دو مست باشد ناچار عربده‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بارگاه دیو درآیی که داد داد</p></div>
<div class="m2"><p>داد از خدای خواه که این جا همه دده‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتست مصطفی که ز زن مشورت مگیر</p></div>
<div class="m2"><p>این نفس ما زن‌ست اگر چه که زاهده‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چندان بنوش می که بمانی ز گفت و گو</p></div>
<div class="m2"><p>آخر نه عاشقی و نه این عشق میکده‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نظم و نثر گویی چون زر جعفری</p></div>
<div class="m2"><p>آن سو که جعفرست خرافات فاسده‌ست</p></div></div>