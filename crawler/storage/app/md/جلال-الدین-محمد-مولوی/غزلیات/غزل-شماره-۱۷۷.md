---
title: >-
    غزل شمارهٔ ۱۷۷
---
# غزل شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>ای بگفته در دلم اسرارها</p></div>
<div class="m2"><p>وی برای بنده پخته کارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خیالت غمگسار سینه‌ها</p></div>
<div class="m2"><p>ای جمالت رونق گلزارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عطای دست شادی بخش تو</p></div>
<div class="m2"><p>دست این مسکین گرفته بارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای کف چون بحر گوهرداد تو</p></div>
<div class="m2"><p>از کف پایم بکنده خارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ببخشیده بسی سرها عوض</p></div>
<div class="m2"><p>چون دهند از بهر تو دستارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود چه باشد هر دو عالم پیش تو</p></div>
<div class="m2"><p>دانه افتاده از انبارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتاب فضل عالم پرورت</p></div>
<div class="m2"><p>کرده بر هر ذره‌ای ایثارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاره‌ای نبود جز از بیچارگی</p></div>
<div class="m2"><p>گر چه حیله می‌کنیم و چاره‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نورهای شمس تبریزی چو تافت</p></div>
<div class="m2"><p>ایمنیم از دوزخ و از نارها</p></div></div>