---
title: >-
    غزل شمارهٔ ۵۰۶
---
# غزل شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>کار من اینست که کاریم نیست</p></div>
<div class="m2"><p>عاشقم از عشق تو عاریم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که مرا شیر غمت صید کرد</p></div>
<div class="m2"><p>جز که همین شیر شکاریم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تک این بحر چه خوش گوهری</p></div>
<div class="m2"><p>که مثل موج قراریم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر لب بحر تو مقیمم مقیم</p></div>
<div class="m2"><p>مست لبم گر چه کناریم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقف کنم اشکم خود بر میت</p></div>
<div class="m2"><p>کز می تو هیچ خماریم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌رسدم باده تو ز آسمان</p></div>
<div class="m2"><p>منت هر شیره فشاریم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده‌ات از کوه سکونت برد</p></div>
<div class="m2"><p>عیب مکن زان که وقاریم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک جهان گیرم چون آفتاب</p></div>
<div class="m2"><p>گر چه سپاهی و سواریم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌کشم از مصر شکر سوی روم</p></div>
<div class="m2"><p>گر چه شتربان و قطاریم نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه ندارم به جهان سروری</p></div>
<div class="m2"><p>دردسر بیهده باریم نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سر کوی تو مرا خانه گیر</p></div>
<div class="m2"><p>کز سر کوی تو گذاریم نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو شکر با گلت آمیختم</p></div>
<div class="m2"><p>نیست عجب گر سر خاریم نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قطب جهانی همه را رو به توست</p></div>
<div class="m2"><p>جز که به گرد تو دواریم نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خویش من آنست که از عشق زاد</p></div>
<div class="m2"><p>خوشتر از این خویش و تباریم نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چیست فزون از دو جهان شهر عشق</p></div>
<div class="m2"><p>بهتر از این شهر و دیاریم نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر ننگارم سخنی بعد از این</p></div>
<div class="m2"><p>نیست از آن رو که نگاریم نیست</p></div></div>