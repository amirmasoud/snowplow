---
title: >-
    غزل شمارهٔ ۵۷
---
# غزل شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>مسلمانان مسلمانان چه باید گفت یاری را</p></div>
<div class="m2"><p>که صد فردوس می‌سازد جمالش نیم خاری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکان‌ها بی‌مکان گردد زمین‌ها جمله کان گردد</p></div>
<div class="m2"><p>چو عشق او دهد تشریف یک لحظه دیاری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوندا زهی نوری لطافت بخش هر حوری</p></div>
<div class="m2"><p>که آب زندگی سازد ز روی لطف ناری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو لطفش را بیفشارد هزاران نوبهار آرد</p></div>
<div class="m2"><p>چه نقصان گر ز غیرت او زند برهم بهاری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمالش آفتاب آمد جهان او را نقاب آمد</p></div>
<div class="m2"><p>ولیکن نقش کی بیند به جز نقش و نگاری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمال گل گواه آمد که بخشش‌ها ز شاه آمد</p></div>
<div class="m2"><p>اگر چه گل بنشناسد هوای سازواری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر گل را خبر بودی همیشه سرخ و تر بودی</p></div>
<div class="m2"><p>ازیرا آفتی ناید حیات هوشیاری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دست آور نگاری تو کز این دستست کار تو</p></div>
<div class="m2"><p>چرا باید سپردن جان نگاری جان سپاری را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شمس الدین تبریزی منم قاصد به خون ریزی</p></div>
<div class="m2"><p>که عشقی هست در دستم که ماند ذوالفقاری را</p></div></div>