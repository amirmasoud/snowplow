---
title: >-
    غزل شمارهٔ ۳۴۷
---
# غزل شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>قرار زندگانی آن نگارست</p></div>
<div class="m2"><p>کز او آن بی‌قراری برقرارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا سودای تو دامن گرفته‌ست</p></div>
<div class="m2"><p>که این سودا نه آن سودای پارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم سوزان در آتش‌های نو نو</p></div>
<div class="m2"><p>مرا با یارکان اکنون چه کارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی‌نالد درون از بی‌قراری</p></div>
<div class="m2"><p>بدان ماند که آن جان نگارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از یاری تو را جان خسته گردد</p></div>
<div class="m2"><p>نمی‌داند که اندر جانش خارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو در جویی و خارت می‌خراشد</p></div>
<div class="m2"><p>نمی‌دانی که خاری در سرا رست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریزان شو از آن خار و به گل رو</p></div>
<div class="m2"><p>که شمس الدین تبریزی بهارست</p></div></div>