---
title: >-
    غزل شمارهٔ ۹۱
---
# غزل شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>در آب فکن ساقی بط زاده آبی را</p></div>
<div class="m2"><p>بشتاب و شتاب اولی مستان شبابی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان بهار و دی وی حاتم نقل و می</p></div>
<div class="m2"><p>پر کن ز شکر چون نی بوبکر ربابی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ساقی شور و شر هین عیش بگیر از سر</p></div>
<div class="m2"><p>پر کن ز می احمر سغراق و شرابی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنما ز می فرخ این سو اخ و آن سو اخ</p></div>
<div class="m2"><p>بربای نقاب از رخ معشوق نقابی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>احسنت زهی یار او شاخ گل بی‌خار او</p></div>
<div class="m2"><p>شاباش زهی دارو دل‌های کبابی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد حلقه نگر شیدا زان باده ناپیدا</p></div>
<div class="m2"><p>کاسد کند این صهبا صد خمر لعابی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستان چمن پنهان اشکوفه ز شاخ افشان</p></div>
<div class="m2"><p>صد کوه چو که غلطان سیلاب حبابی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر آن قدح روشن جانست نهان از تن</p></div>
<div class="m2"><p>پنهان نتوان کردن مستی و خرابی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماییم چو کشت ای جان سرسبز در این میدان</p></div>
<div class="m2"><p>تشنه شده و جویان باران سحابی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون رعد نه‌ای خامش چون پرده تست این هش</p></div>
<div class="m2"><p>وز صبر و فنا می‌کش طوطی خطابی را</p></div></div>