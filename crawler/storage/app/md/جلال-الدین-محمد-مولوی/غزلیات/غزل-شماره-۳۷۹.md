---
title: >-
    غزل شمارهٔ ۳۷۹
---
# غزل شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>ای کرده میان سینه غارت</p></div>
<div class="m2"><p>ای جان و هزار جان شکارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز کشتن عاشقان چه شغلت</p></div>
<div class="m2"><p>جز کشتن خلق چیست کارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌کش که درست باد دستت</p></div>
<div class="m2"><p>ای جان جهانیان نثارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس کشته زنده را که دیدم</p></div>
<div class="m2"><p>از غمزه چشم پرخمارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس ساکن بی‌قرار دیدم</p></div>
<div class="m2"><p>در آتش عشق بی‌قرارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک مرده به خاک درنماند</p></div>
<div class="m2"><p>گر رنجه شوی کنی زیارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان بوسد خاک تو به هر دم</p></div>
<div class="m2"><p>بر بوی کنار بی‌کنارت</p></div></div>