---
title: >-
    غزل شمارهٔ ۲۴۰
---
# غزل شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>تو جان و جهانی کریما مرا</p></div>
<div class="m2"><p>چه جان و جهان از کجا تا کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که جان خود چه باشد بر عاشقان</p></div>
<div class="m2"><p>جهان خود چه باشد بر اولیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه بر پشت گاویست جمله زمین</p></div>
<div class="m2"><p>که در مرغزار تو دارد چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن کاروانی که کل زمین</p></div>
<div class="m2"><p>یکی گاوبارست و تو ره نما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در انبار فضل تو بس دانه‌هاست</p></div>
<div class="m2"><p>که آن نشکند زیر هفت آسیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو در چشم نقاش و پنهان ز چشم</p></div>
<div class="m2"><p>زهی چشم بند و زهی سیمیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را عالمی غیر هجده هزار</p></div>
<div class="m2"><p>زهی کیمیا و زهی کبریا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی بیت دیگر بر این قافیه</p></div>
<div class="m2"><p>بگویم بلی وام دارم تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که نگزارد این وام را جز فقیر</p></div>
<div class="m2"><p>که فقرست دریای در وفا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غنی از بخیلی غنی مانده‌ست</p></div>
<div class="m2"><p>فقیر از سخاوت فقیر از سخا</p></div></div>