---
title: >-
    غزل شمارهٔ ۲۵۸
---
# غزل شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>گر بنخسبی شبی ای مه لقا</p></div>
<div class="m2"><p>رو به تو بنماید گنج بقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم شوی شب تو به خورشید غیب</p></div>
<div class="m2"><p>چشم تو را باز کند توتیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امشب استیزه کن و سر منه</p></div>
<div class="m2"><p>تا که ببینی ز سعادت عطا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه گه جمله بتان در شبست</p></div>
<div class="m2"><p>نشنود آن کس که بخفت الصلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موسی عمران نه به شب دید نور</p></div>
<div class="m2"><p>سوی درختی که بگفتش بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت به شب بیش ز ده ساله راه</p></div>
<div class="m2"><p>دید درختی همه غرق ضیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی که به شب احمد معراج رفت</p></div>
<div class="m2"><p>برد براقیش به سوی سما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز پی کسب و شب از بهر عشق</p></div>
<div class="m2"><p>چشم بدی تا که نبیند تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق بخفتند ولی عاشقان</p></div>
<div class="m2"><p>جمله شب قصه کنان با خدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت به داوود خدای کریم</p></div>
<div class="m2"><p>هر کی کند دعوی سودای ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون همه شب خفت بود آن دروغ</p></div>
<div class="m2"><p>خواب کجا آید مر عشق را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان که بود عاشق خلوت طلب</p></div>
<div class="m2"><p>تا غم دل گوید با دلربا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تشنه نخسپید مگر اندکی</p></div>
<div class="m2"><p>تشنه کجا خواب گران از کجا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چونک بخسپید به خواب آب دید</p></div>
<div class="m2"><p>یا لب جو یا که سبو یا سقا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمله شب می رسد از حق خطاب</p></div>
<div class="m2"><p>خیز غنیمت شمر ای بی‌نوا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور نه پس مرگ تو حسرت خوری</p></div>
<div class="m2"><p>چونک شود جان تو از تن جدا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جفت ببردند و زمین ماند خام</p></div>
<div class="m2"><p>هیچ ندارد جز خار و گیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من شدم از دست تو باقی بخوان</p></div>
<div class="m2"><p>مست شدم سر نشناسم ز پا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمس حق مفخر تبریزیان</p></div>
<div class="m2"><p>بستم لب را تو بیا برگشا</p></div></div>