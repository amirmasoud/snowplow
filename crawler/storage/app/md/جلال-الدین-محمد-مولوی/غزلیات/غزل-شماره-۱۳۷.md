---
title: >-
    غزل شمارهٔ ۱۳۷
---
# غزل شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>با چنین شمشیر دولت تو زبون مانی چرا</p></div>
<div class="m2"><p>گوهری باشی و از سنگی فرومانی چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کشد هر کرکسی اجزات را هر جانبی</p></div>
<div class="m2"><p>چون نه مرداری تو بلک باز جانانی چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده‌ات را چون نظر از دیده باقی رسید</p></div>
<div class="m2"><p>دیده‌ات شرمین شود از دیده فانی چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که او را کس به نسیه و نقد نستاند به خاک</p></div>
<div class="m2"><p>این چنین بیشی کند بر نقده کانی چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن سیه جانی که کفر از جان تلخش ننگ داشت</p></div>
<div class="m2"><p>زهر ریزد بر تو و تو شهد ایمانی چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو چنین لرزان او باشی و او سایه توست</p></div>
<div class="m2"><p>آخر او نقشیست جسمانی و تو جانی چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او همه عیب تو گیرد تا بپوشد عیب خود</p></div>
<div class="m2"><p>تو بر او از غیب جان ریزی و می‌دانی چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون در او هستی به بینی گویی آن من نیستم</p></div>
<div class="m2"><p>دعوی او چون نبینی گوییش آنی چرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خشم یاران فرع باشد اصلشان عشق نوست</p></div>
<div class="m2"><p>از برای خشم فرعی اصل را رانی چرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شه به حق چون شمس تبریزیست ثانی نیستش</p></div>
<div class="m2"><p>ناحقی را اصل گویی شاه را ثانی چرا</p></div></div>