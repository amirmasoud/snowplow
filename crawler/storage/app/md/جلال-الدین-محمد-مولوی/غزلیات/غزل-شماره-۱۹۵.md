---
title: >-
    غزل شمارهٔ ۱۹۵
---
# غزل شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>شهوت که با تو رانند صدتو کنند جان را</p></div>
<div class="m2"><p>چون با زنی برانی سستی دهد میان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا جماع مرده تن را کند فسرده</p></div>
<div class="m2"><p>بنگر به اهل دنیا دریاب این نشان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میران و خواجگانشان پژمرده است جانشان</p></div>
<div class="m2"><p>خاک سیاه بر سر این نوع شاهدان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دررو به عشق دینی تا شاهدان ببینی</p></div>
<div class="m2"><p>پرنور کرده از رخ آفاق آسمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخشد بت نهانی هر پیر را جوانی</p></div>
<div class="m2"><p>زان آشیان جانی اینست ارغوان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خامش کنی وگر نی بیرون شوم از این جا</p></div>
<div class="m2"><p>کز شومی زبانت می‌پوشد او دهان را</p></div></div>