---
title: >-
    غزل شمارهٔ ۱۸۵
---
# غزل شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>از بس که ریخت جرعه بر خاک ما ز بالا</p></div>
<div class="m2"><p>هر ذره خاک ما را آورد در علالا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه شکاف گشته دل عشق باف گشته</p></div>
<div class="m2"><p>چون شیشه صاف گشته از جام حق تعالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشکوفه‌ها شکفته وز چشم بد نهفته</p></div>
<div class="m2"><p>غیرت مرا بگفته می خور دهان میالا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان چو رو نمودی جان و دلم ربودی</p></div>
<div class="m2"><p>چون مشتری تو بودی قیمت گرفت کالا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابرت نبات بارد جورت حیات آرد</p></div>
<div class="m2"><p>درد تو خوش گوارد تو درد را مپالا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عشق با توستم وز باده تو مستم</p></div>
<div class="m2"><p>وز تو بلند و پستم وقت دنا تدلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهت چگونه خوانم مه رنج دق دارد</p></div>
<div class="m2"><p>سروت اگر بخوانم آن راستست الا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرو احتراق دارد مه هم محاق دارد</p></div>
<div class="m2"><p>جز اصل اصل جان‌ها اصلی ندارد اصلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید را کسوفی مه را بود خسوفی</p></div>
<div class="m2"><p>گر تو خلیل وقتی این هر دو را بگو لا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویند جمله یاران باطل شدند و مردند</p></div>
<div class="m2"><p>باطل نگردد آن کو بر حق کند تولا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این خنده‌های خلقان برقیست دم بریده</p></div>
<div class="m2"><p>جز خنده‌ای که باشد در جان ز رب اعلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آب حیات حقست وان کو گریخت در حق</p></div>
<div class="m2"><p>هم روح شد غلامش هم روح قدس لالا</p></div></div>