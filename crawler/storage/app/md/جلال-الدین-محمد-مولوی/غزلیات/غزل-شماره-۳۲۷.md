---
title: >-
    غزل شمارهٔ ۳۲۷
---
# غزل شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>از دفتر عمر ما یکتا ورقی مانده‌ست</p></div>
<div class="m2"><p>کز غیرت لطف آن جان در قلقی مانده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنوشته بر آن دفتر حرفی ز شکر خوشتر</p></div>
<div class="m2"><p>از خجلت آن حرفش مه در عرقی مانده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر ابدی تابان اندر ورق بستان</p></div>
<div class="m2"><p>نی خوف ز تحویلی نی جای دقی مانده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نامش ورقی بوده ملک ابد اندر وی</p></div>
<div class="m2"><p>اسرار همه پاکان آن جا شفقی مانده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیچیده ورق بر وی نوری ز خداوندی</p></div>
<div class="m2"><p>شمس الحق تبریزی روشن حدقی مانده‌ست</p></div></div>