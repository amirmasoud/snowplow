---
title: >-
    غزل شمارهٔ ۹۸
---
# غزل شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>ای از نظرت مست شده اسم و مسما</p></div>
<div class="m2"><p>ای یوسف جان گشته ز لب‌های شکرخا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را چه از آن قصه که گاو آمد و خر رفت</p></div>
<div class="m2"><p>هین وقت لطیفست از آن عربده بازآ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شاه تو شاهی کن و آراسته کن بزم</p></div>
<div class="m2"><p>ای جان ولی نعمت هر وامق و عذرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم دایه جان‌هایی و هم جوی می و شیر</p></div>
<div class="m2"><p>هم جنت فردوسی و هم سدره خضرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز این بنگوییم وگر نیز بگوییم</p></div>
<div class="m2"><p>گویید خسیسان که محالست و علالا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی که بگویم بده آن جام صبوحی</p></div>
<div class="m2"><p>تا چرخ به رقص آید و صد زهره زهرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر جا ترشی باشد اندر غم دنیی</p></div>
<div class="m2"><p>می‌غرد و می‌برد از آن جای دل ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برخیز بخیلانه در خانه فروبند</p></div>
<div class="m2"><p>کان جا که تویی خانه شود گلشن و صحرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این مه ز کجا آمد وین روی چه رویست</p></div>
<div class="m2"><p>این نور خداییست تبارک و تعالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم قادر و هم قاهر و هم اول و آخر</p></div>
<div class="m2"><p>اول غم و سودا و به آخر ید بیضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر دل که نلرزیدت و هر چشم که نگریست</p></div>
<div class="m2"><p>یا رب خبرش ده تو از این عیش و تماشا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا شید برآرد وی و آید به سر کوی</p></div>
<div class="m2"><p>فریاد برآرد که تمنیت تمنا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگذاردش آن عشق که سر نیز بخارد</p></div>
<div class="m2"><p>شاباش زهی سلسله و جذب و تقاضا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در شهر چو من گول مگر عشق ندیدست</p></div>
<div class="m2"><p>هر لحظه مرا گیرد این عشق ز بالا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر داد و گرفتی که ز بالاست لطیفست</p></div>
<div class="m2"><p>گر حاذق جدست وگر عشوه تیبا</p></div></div>