---
title: >-
    غزل شمارهٔ ۳۸۴
---
# غزل شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>عاشقان را گر چه در باطن جهانی دیگرست</p></div>
<div class="m2"><p>عشق آن دلدار ما را ذوق و جانی دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه‌های روشنان بس غیب‌ها دانند لیک</p></div>
<div class="m2"><p>سینه عشاق او را غیب دانی دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس زبان حکمت اندر شوق سرش گوش شد</p></div>
<div class="m2"><p>زانک مر اسرار او را ترجمانی دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک زمین نقره بین از لطف او در عین جان</p></div>
<div class="m2"><p>تا بدانی کان مهم را آسمانی دیگرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل و عشق و معرفت شد نردبان بام حق</p></div>
<div class="m2"><p>لیک حق را در حقیقت نردبانی دیگرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب روان از شاه عقل و پاسبان آن سو شوند</p></div>
<div class="m2"><p>لیک آن جان را از آن سو پاسبانی دیگرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلبران راه معنی با دلی عاجز بدند</p></div>
<div class="m2"><p>وحیشان آمد که دل را دلستانی دیگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای زبان‌ها برگشاده بر دل بربوده‌ای</p></div>
<div class="m2"><p>لب فروبندید کو را همزبانی دیگرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریزی چو جمع و شمع‌ها پروانه‌اش</p></div>
<div class="m2"><p>زانک اندر عین دل او را عیانی دیگرست</p></div></div>