---
title: >-
    غزل شمارهٔ ۴۸
---
# غزل شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ماه درست را ببین کو بشکست خواب ما</p></div>
<div class="m2"><p>تافت ز چرخ هفتمین در وطن خراب ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب ببر ز چشم ما چون ز تو روز گشت شب</p></div>
<div class="m2"><p>آب مده به تشنگان عشق بس است آب ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله ره چکیده خون از سر تیغ عشق او</p></div>
<div class="m2"><p>جمله کو گرفته بو از جگر کباب ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر باکرانه را شکر بی‌کرانه گفت</p></div>
<div class="m2"><p>غره شدی به ذوق خود بشنو این جواب ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روترشی چرا مگر صاف نبد شراب تو</p></div>
<div class="m2"><p>از پی امتحان بخور یک قدح از شراب ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چه شوند عاشقان روز وصال ای خدا</p></div>
<div class="m2"><p>چونک ز هم بشد جهان از بت بانقاب ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تبریز شمس دین روی نمود عاشقان</p></div>
<div class="m2"><p>ای که هزار آفرین بر مه و آفتاب ما</p></div></div>