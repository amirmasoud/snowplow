---
title: >-
    غزل شمارهٔ ۴۳۶
---
# غزل شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>گفتا که کیست بر در گفتم کمین غلامت</p></div>
<div class="m2"><p>گفتا چه کار داری گفتم مها سلامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که چند رانی گفتم که تا بخوانی</p></div>
<div class="m2"><p>گفتا که چند جوشی گفتم که تا قیامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعوی عشق کردم سوگندها بخوردم</p></div>
<div class="m2"><p>کز عشق یاوه کردم من ملکت و شهامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتا برای دعوی قاضی گواه خواهد</p></div>
<div class="m2"><p>گفتم گواه اشکم زردی رخ علامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتا گواه جرحست تردامنست چشمت</p></div>
<div class="m2"><p>گفتم به فر عدلت عدلند و بی‌غرامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتا که بود همره گفتم خیالت ای شه</p></div>
<div class="m2"><p>گفتا که خواندت این جا گفتم که بوی جامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتا چه عزم داری گفتم وفا و یاری</p></div>
<div class="m2"><p>گفتا ز من چه خواهی گفتم که لطف عامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتا کجاست خوشتر گفتم که قصر قیصر</p></div>
<div class="m2"><p>گفتا چه دیدی آن جا گفتم که صد کرامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتا چراست خالی گفتم ز بیم رهزن</p></div>
<div class="m2"><p>گفتا که کیست رهزن گفتم که این ملامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتا کجاست ایمن گفتم که زهد و تقوا</p></div>
<div class="m2"><p>گفتا که زهد چه بود گفتم ره سلامت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتا کجاست آفت گفتم به کوی عشقت</p></div>
<div class="m2"><p>گفتا که چونی آن جا گفتم در استقامت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خامش که گر بگویم من نکته‌های او را</p></div>
<div class="m2"><p>از خویشتن برآیی نی در بود نه بامت</p></div></div>