---
title: >-
    غزل شمارهٔ ۴۰۵
---
# غزل شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>به خدا کت نگذارم که روی راه سلامت</p></div>
<div class="m2"><p>که سر و پا و سلامت نبود روز قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حشم عشق درآمد ربض شهر برآمد</p></div>
<div class="m2"><p>هله ای یار قلندر بشنو طبل ملامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل و جان فانی لا کن تن خود همچو قبا کن</p></div>
<div class="m2"><p>نه اثر گو نه خبر گو نه نشانی نه علامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو من از خویش برستم ره اندیشه ببستم</p></div>
<div class="m2"><p>هله ای سرده مستم برهانم به تمامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هله برجه هله برجه قدمی بر سر خود نه</p></div>
<div class="m2"><p>هله برپر هله برپر چو من از شکر و غرامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببر ای عشق چو موسی سر فرعون تکبر</p></div>
<div class="m2"><p>هله فرعون به پیش آ که گرفتم در و بامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو من از غیب رسیدم سپه غیب کشیدم</p></div>
<div class="m2"><p>برو ای ظالم سرکش که فتادی ز زعامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هله پالیز تو باقی سر خر عالم فانی</p></div>
<div class="m2"><p>همه دیدار کریمست در این عشق کرامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکند رحمت مطلق به بلا جان تو ویران</p></div>
<div class="m2"><p>نکند والده ما را ز پی کینه حجامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبود جان و دلم را ز تو سیری و ملولی</p></div>
<div class="m2"><p>نبود هیچ کسی را ز دل و دیده سمت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بجز از عشق مجرد به هر آن نقش که رفتم</p></div>
<div class="m2"><p>بنه ارزید خوشی‌هاش به تلخی ندامت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هله تا یاوه نگردی چو در این حوض رسیدی</p></div>
<div class="m2"><p>که تکش آب حیاتست و لبش جای اقامت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو در این حوض درافتی همه خویش بدو ده</p></div>
<div class="m2"><p>به مزن دستک و پایک تو به چستی و شهامت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه تسلیم و خمش کن نه امامی تو ز جمعی</p></div>
<div class="m2"><p>نرسد هیچ کسی را به جز این عشق امامت</p></div></div>