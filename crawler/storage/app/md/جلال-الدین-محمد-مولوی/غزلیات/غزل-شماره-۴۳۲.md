---
title: >-
    غزل شمارهٔ ۴۳۲
---
# غزل شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>این چنین پابند جان میدان کیست</p></div>
<div class="m2"><p>ما شدیم از دست این دستان کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌دود چون گوی زرین آفتاب</p></div>
<div class="m2"><p>ای عجب اندر خم چوگان کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابا راه زن راهت نزد</p></div>
<div class="m2"><p>چون زند داند که این ره آن کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیب را بو کرد موسی جان بداد</p></div>
<div class="m2"><p>بازجو آن بو ز سیبستان کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم یعقوبی از این بو باز شد</p></div>
<div class="m2"><p>ای خدا این بوی از کنعان کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک بودیم این چنین موزون شدیم</p></div>
<div class="m2"><p>خاک ما زر گشت در میزان کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر زر ما هر زمان مهر نوست</p></div>
<div class="m2"><p>تا بداند زر که او از کان کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله حیرانند و سرگردان عشق</p></div>
<div class="m2"><p>ای عجب این عشق سرگردان کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله مهمانند در عالم ولیک</p></div>
<div class="m2"><p>کم کسی داند که او مهمان کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نرگس چشم بتان ره می‌زند</p></div>
<div class="m2"><p>آب این نرگس ز نرگسدان کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جسم‌ها شب خالی از ما روز پر</p></div>
<div class="m2"><p>ما و من چون گربه در انبان کیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کسی دستک زنان کای جان من</p></div>
<div class="m2"><p>و آنک دستک زن کند او جان کیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمس تبریزی که نور اولیاست</p></div>
<div class="m2"><p>با چنان عز و شرف سلطان کیست</p></div></div>