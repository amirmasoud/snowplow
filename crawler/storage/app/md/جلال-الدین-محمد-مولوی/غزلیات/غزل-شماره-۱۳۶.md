---
title: >-
    غزل شمارهٔ ۱۳۶
---
# غزل شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>پرده دیگر مزن جز پرده دلدار ما</p></div>
<div class="m2"><p>آن هزاران یوسف شیرین شیرین کار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسفان را مست کرد و پرده‌هاشان بردرید</p></div>
<div class="m2"><p>غمزه خونی مست آن شه خمار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان ما همچون سگان کوی او خون خوار شد</p></div>
<div class="m2"><p>آفرین‌ها صد هزاران بر سگ خون خوار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نوای عشق آن صد نوبهار سرمدی</p></div>
<div class="m2"><p>صد هزاران بلبلان اندر گل و گلزار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چو زناری ز عشق آن مسیح عهد بست</p></div>
<div class="m2"><p>لاجرم غیرت برد ایمان بر این زنار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابی نی ز شرق و نی ز غرب از جان بتافت</p></div>
<div class="m2"><p>ذره وار آمد به رقص از وی در و دیوار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون مثال ذره‌ایم اندر پی آن آفتاب</p></div>
<div class="m2"><p>رقص باشد همچو ذره روز و شب کردار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان عشق را بسیار یاری‌ها دهیم</p></div>
<div class="m2"><p>چونک شمس الدین تبریزی کنون شد یار ما</p></div></div>