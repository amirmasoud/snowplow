---
title: >-
    غزل شمارهٔ ۲۸۷
---
# غزل شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>ورد البشیر مبشرا ببشاره</p></div>
<div class="m2"><p>احیی الفؤاد عشیه بورودها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکان ارضا نورت بربیعها</p></div>
<div class="m2"><p>فکان شمسا اشرقت بخدودها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا طاعنی فی صبوتی و تهتکی</p></div>
<div class="m2"><p>انظر الی نار الهوی و وقودها</p></div></div>