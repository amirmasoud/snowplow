---
title: >-
    غزل شمارهٔ ۴۹۰
---
# غزل شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>مرا چو زندگی از یاد روی چون مه توست</p></div>
<div class="m2"><p>همیشه سجده گهم آستان خرگه توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر شبی کشدم تا به روز زنده کند</p></div>
<div class="m2"><p>نوای آن سگ کو پاسبان درگه توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پیش آب و گل من بدید روح تو را</p></div>
<div class="m2"><p>خرد بگفت که سجده کنش که او شه توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سجود کرد و در آن سجده ماند تا به ابد</p></div>
<div class="m2"><p>نهاده روی بر آن خاک خوش که او ره توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه باشدت اگر این شوره خاک را که منم</p></div>
<div class="m2"><p>به نعل بازنوازی که آن گذرگه توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایا دو دیده تبریز شمس دین به حق</p></div>
<div class="m2"><p>تو کهربای دلی دل به عاشقی که توست</p></div></div>