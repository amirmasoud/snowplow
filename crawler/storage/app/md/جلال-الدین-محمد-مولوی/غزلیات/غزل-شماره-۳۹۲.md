---
title: >-
    غزل شمارهٔ ۳۹۲
---
# غزل شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>گر ندید آن شادجان این گلستان را شاد چیست</p></div>
<div class="m2"><p>گر نه لطف او بود پس عیش را بنیاد چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خرابات ازل از تاب رویش پر نگشت</p></div>
<div class="m2"><p>پس هزاران صومعه در محو جان آباد چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان ما با عشق او گر نی ز یک جا رسته‌اند</p></div>
<div class="m2"><p>جان بااقبال ما با عشق او همزاد چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه پرتوهای آن رخسار داد حسن داد</p></div>
<div class="m2"><p>پس به دیوان سرای عاشقان بیداد چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساکنان آب و گل گر عشق ما را محرمند</p></div>
<div class="m2"><p>پس درون گنبد دل غلغله و فریاد چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نه آتش می‌زند آتش رخی در جان نهان</p></div>
<div class="m2"><p>پس دماغ عاشقان پرآتش و پرباد چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نه آتش رنگ گشتی جان‌ها در لامکان</p></div>
<div class="m2"><p>صد هزاران مشعله همچون شب میلاد چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نه تقصیر است از جان در فدا گشتن در او</p></div>
<div class="m2"><p>لطف نقد اولین و وعده و میعاد چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نه شمس الدین تبریزی قباد جان‌ها است</p></div>
<div class="m2"><p>صد هزاران جان قدسی هر دمش منقاد چیست</p></div></div>