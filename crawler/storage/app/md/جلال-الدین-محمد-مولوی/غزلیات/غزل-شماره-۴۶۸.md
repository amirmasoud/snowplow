---
title: >-
    غزل شمارهٔ ۴۶۸
---
# غزل شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>با وی از ایمان و کفر باخبری کافریست</p></div>
<div class="m2"><p>آنک از او آگهست از همه عالم بریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه که چه بی‌بهره‌اند باخبران زانک هست</p></div>
<div class="m2"><p>چهره او آفتاب طره او عنبریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از آن موسیی کانک بدیدش دمی</p></div>
<div class="m2"><p>گشته رمیده ز خلق بر مثل سامریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر عدد ریگ هست در هوسش کوه طور</p></div>
<div class="m2"><p>بر عدد اختران ماه ورا مشتریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خلایق از او بسته شد از چشم بند</p></div>
<div class="m2"><p>زانک مسلم شده چشم ورا ساحریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوست یکی کیمیا کز تبش فعل او</p></div>
<div class="m2"><p>زرگر عشق ورا بر رخ من زرگریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای در آتش بنه همچو خلیل ای پسر</p></div>
<div class="m2"><p>کآتش از لطف او روضه نیلوفریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون رخ گلزار او هست چراگاه روح</p></div>
<div class="m2"><p>روح از آن لاله زار آه که چون پروریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفخر جان شمس دین عقل به تبریز یافت</p></div>
<div class="m2"><p>آن گهری را که بحر در نظرش سرسریست</p></div></div>