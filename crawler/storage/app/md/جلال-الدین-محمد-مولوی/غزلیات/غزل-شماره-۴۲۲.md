---
title: >-
    غزل شمارهٔ ۴۲۲
---
# غزل شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>ای که رویت چو گل و زلف تو چون شمشادست</p></div>
<div class="m2"><p>جانم آن لحظه که غمگین تو باشم شادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقدهایی که نه نقد غم توست آن خاکست</p></div>
<div class="m2"><p>غیر پیمودن باد هوس تو بادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار او دارد کموخته کار توست</p></div>
<div class="m2"><p>زانک کار تو یقین کارگه ایجادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان را و زمین را خبرست و معلوم</p></div>
<div class="m2"><p>کآسمان همچو زمین امر تو را منقادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی بنمای و خمار دو جهان را بشکن</p></div>
<div class="m2"><p>نه که امروز خماران تو را میعادست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب ار چه در این دور فریدست و وحید</p></div>
<div class="m2"><p>شرقیانند که او در صفشان آحادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسروان خاک کفش را به خدا تاج کنند</p></div>
<div class="m2"><p>هر که شیرین تو را دلشده چون فرهادست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌نهد بر لب خود دست دل من که خموش</p></div>
<div class="m2"><p>این چه وقت سخن‌ست و چه گه فریادست</p></div></div>