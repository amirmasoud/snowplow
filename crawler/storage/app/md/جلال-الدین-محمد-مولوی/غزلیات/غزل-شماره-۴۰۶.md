---
title: >-
    غزل شمارهٔ ۴۰۶
---
# غزل شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>چند گویی که چه چاره‌ست و مرا درمان چیست</p></div>
<div class="m2"><p>چاره جوینده که کرده‌ست تو را خود آن چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند باشد غم آنت که ز غم جان ببرم</p></div>
<div class="m2"><p>خود نباشد هوس آنک بدانی جان چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی نانی که رسیده‌ست بر آن بوی برو</p></div>
<div class="m2"><p>تا همان بوی دهد شرح تو را کاین نان چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو عاشق شده‌ای عشق تو برهان تو بس</p></div>
<div class="m2"><p>ور تو عاشق نشدی پس طلب برهان چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این قدر عقل نداری که ببینی آخر</p></div>
<div class="m2"><p>گر نه شاهیست پس این بارگه سلطان چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نه اندر تتق ازرق زیباروییست</p></div>
<div class="m2"><p>در کف روح چنین مشعله تابان چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونک از دور دلت همچو زنان می‌لرزد</p></div>
<div class="m2"><p>تو چه دانی که در آن جنگ دل مردان چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش دیده مردان حجب غیب بسوخت</p></div>
<div class="m2"><p>تو پس پرده نشسته که به غیب ایمان چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس تبریز اگر نیست مقیم اندر چشم</p></div>
<div class="m2"><p>چشمه شهد از او در بن هر دندان چیست</p></div></div>