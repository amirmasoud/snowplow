---
title: >-
    غزل شمارهٔ ۲۲۲
---
# غزل شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>رویم و خانه بگیریم پهلوی دریا</p></div>
<div class="m2"><p>که داد اوست جواهر که خوی اوست سخا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان که صحبت جان را همی‌کند همرنگ</p></div>
<div class="m2"><p>ز صحبت فلک آمد ستاره خوش سیما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه تن به صحبت جان خوبروی و خوش فعل‌ست</p></div>
<div class="m2"><p>چه می‌شود تن مسکین چو شد ز جان عذرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دست متصل توست بس هنر دارد</p></div>
<div class="m2"><p>چو شد ز جسم جدا اوفتاد اندر پا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجاست آن هنر تو نه که همان دستی</p></div>
<div class="m2"><p>نه این زمان فراق‌ست و آن زمان لقا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس الله الله زنهار ناز یار بکش</p></div>
<div class="m2"><p>که ناز یار بود صد هزار من حلوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراق را بندیدی خدات منما یاد</p></div>
<div class="m2"><p>که این دعاگو به زین نداشت هیچ دعا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نفس کلی چون نفس جزو ما ببرید</p></div>
<div class="m2"><p>به اهبطوا و فرود آمد از چنان بالا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مثال دست بریده ز کار خویش بماند</p></div>
<div class="m2"><p>که گشت طعمه گربه زهی ذلیل و بلا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دست او همه شیران شکسته پنجه بدند</p></div>
<div class="m2"><p>که گربه می‌کشدش سو به سو ز دست قضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امید وصل بود تا رگیش می‌جنبد</p></div>
<div class="m2"><p>که یافت دولت وصلت هزار دست جدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدار این عجب از شهریار خوش پیوند</p></div>
<div class="m2"><p>که پاره پاره دود از کفش شدست سما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شه جهانی و هم پاره دوز استادی</p></div>
<div class="m2"><p>بکن نظر سوی اجزای پاره پاره ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو چنگ ما بشکستی بساز و کش سوی خود</p></div>
<div class="m2"><p>ز الست زخمه همی‌زن همی‌پذیر بلا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلا کنیم ولیکن بلی اول کو</p></div>
<div class="m2"><p>که آن چو نعره روحست وین ز کوه صدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو نای ما بشکستی شکسته را بربند</p></div>
<div class="m2"><p>نیاز این نی ما را ببین بدان دم‌ها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که نای پاره ما پاره می‌دهد صد جان</p></div>
<div class="m2"><p>که کی دمم دهد او تا شوم لطیف ادا</p></div></div>