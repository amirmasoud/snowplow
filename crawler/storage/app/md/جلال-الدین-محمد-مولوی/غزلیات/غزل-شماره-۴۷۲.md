---
title: >-
    غزل شمارهٔ ۴۷۲
---
# غزل شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>کالبد ما ز خواب کاهل و مشغول خاست</p></div>
<div class="m2"><p>آنک به رقص آورد کاهل ما را کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنک به رقص آورد پرده دل بردرد</p></div>
<div class="m2"><p>این همه بویش کند دیدن او خود جداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جنبش خلقان ز عشق جنبش عشق از ازل</p></div>
<div class="m2"><p>رقص هوا از فلک رقص درخت از هواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل چو شد از عشق گرم رفت ز دل ترس و شرم</p></div>
<div class="m2"><p>شد نفسش آتشین عشق یکی اژدهاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی جان در قدح دوش اگر درد ریخت</p></div>
<div class="m2"><p>دردی ساقی ما جمله صفا در صفاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده عشق ای غلام نیست حلال و حرام</p></div>
<div class="m2"><p>پر کن و پیش آر جام بنگر نوبت که راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل پاک تمام بر تو هزاران سلام</p></div>
<div class="m2"><p>جمله خوبان غلام جمله خوبی تو راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سجده کنم پیش یار گوید دل هوش دار</p></div>
<div class="m2"><p>دادن جان در سجود جان همه سجده‌هاست</p></div></div>