---
title: >-
    غزل شمارهٔ ۱۱۳
---
# غزل شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>ای مطرب دل برای یاری را</p></div>
<div class="m2"><p>در پرده زیر گوی زاری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو در چمن و به روی گل بنگر</p></div>
<div class="m2"><p>همدم شو بلبل بهاری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی چه حیات‌ها و مستی‌هاست</p></div>
<div class="m2"><p>در مجلس عشق جان سپاری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دولت بی‌شمار را دیدی</p></div>
<div class="m2"><p>بسپار بدو دم شماری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای روح شکار دلبری گشتی</p></div>
<div class="m2"><p>کو زنده کند ابد شکاری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ساقی دل ز کار واماندم</p></div>
<div class="m2"><p>وقتست بده شراب کاری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آراسته کن مرا و مجلس را</p></div>
<div class="m2"><p>کراسته‌ای شرابداری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزمیست نهان چنین حریفان را</p></div>
<div class="m2"><p>جا نیست دگر شرابخواری را</p></div></div>