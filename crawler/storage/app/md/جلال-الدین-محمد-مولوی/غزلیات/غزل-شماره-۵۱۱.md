---
title: >-
    غزل شمارهٔ ۵۱۱
---
# غزل شمارهٔ ۵۱۱

<div class="b" id="bn1"><div class="m1"><p>همچو گل سرخ برو دست دست</p></div>
<div class="m2"><p>همچو میی خلق ز تو مست مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازوی تو قوس خدا یافت یافت</p></div>
<div class="m2"><p>تیر تو از چرخ برون جست جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیرت تو گفت برو راه نیست</p></div>
<div class="m2"><p>رحمت تو گفت بیا هست هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف تو دریاست و منم ماهیش</p></div>
<div class="m2"><p>غیرت تو ساخت مرا شست شست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرهم تو طالب مجروح‌هاست</p></div>
<div class="m2"><p>نیست غم ار شست توام خست خست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که تو نزدیکتر از دم به من</p></div>
<div class="m2"><p>دم نزنم پیش تو جز پست پست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه یکی یوسف و صد گرگ بود</p></div>
<div class="m2"><p>از دم یعقوب کرم رست رست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مست همه گرد در این شهر ما</p></div>
<div class="m2"><p>دزد و عسس را شه ما بست بست</p></div></div>