---
title: >-
    غزل شمارهٔ ۴۲۹
---
# غزل شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>عاشقی و بی‌وفایی کار ماست</p></div>
<div class="m2"><p>کار کار ماست چون او یار ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصد جان جمله خویشان کنیم</p></div>
<div class="m2"><p>هر چه خویش ما کنون اغیار ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل اگر سلطان این اقلیم شد</p></div>
<div class="m2"><p>همچو دزد آویخته بر دار ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خویش و بی‌خویشی به یک جا کی بود</p></div>
<div class="m2"><p>هر گلی کز ما بروید خار ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خودپرستی نامبارک حالتیست</p></div>
<div class="m2"><p>کاندر او ایمان ما انکار ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنک افلاطون و جالینوس توست</p></div>
<div class="m2"><p>از منی پرعلت و بیمار ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوبهاری کو نوی خود بدید</p></div>
<div class="m2"><p>جان گلزارست اما زار ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این منی خاکست زر در وی بجو</p></div>
<div class="m2"><p>کاندر او گنجور یار غار ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک بی‌آتش بننماید گهر</p></div>
<div class="m2"><p>عشق و هجران ابر آتشبار ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طالبا بشنو که بانگ آتشست</p></div>
<div class="m2"><p>تا نپنداری که این گفتار ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طالبا بگذر از این اسرار خود</p></div>
<div class="m2"><p>سر طالب پرده اسرار ماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نور و نار توست ذوق و رنج تو</p></div>
<div class="m2"><p>رو بدان جایی که نور و نار ماست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاه گویی شیرم و گه شیرگیر</p></div>
<div class="m2"><p>شیرگیر و شیر تو کفتار ماست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طالب ره طالب شه کی بود</p></div>
<div class="m2"><p>گر چه دل دارد مگو دلدار ماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شهر از عاقل تهی خواهد شدن</p></div>
<div class="m2"><p>این چنین ساقی که این خمار ماست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عاشق و مفلس کند این شهر را</p></div>
<div class="m2"><p>این چنین چابک که این طرار ماست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مدرسه عشق و مدرس ذوالجلال</p></div>
<div class="m2"><p>ما چو طالب علم و این تکرار ماست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شمس تبریزی که شاه دلبری‌ست</p></div>
<div class="m2"><p>با همه شاهنشهی جاندار ماست</p></div></div>