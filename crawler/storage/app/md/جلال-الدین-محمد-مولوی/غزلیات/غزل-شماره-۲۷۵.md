---
title: >-
    غزل شمارهٔ ۲۷۵
---
# غزل شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>ابصرت روحی ملیحا زلزلت زلزالها</p></div>
<div class="m2"><p>انعطش روحی فقلت ویح روحی مالها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذاق من شعشاع خمر العشق روحی جرعه</p></div>
<div class="m2"><p>طار فی جو الهوی و استقلعت اثقالها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صار روحی فی هواه غارقا حتی دری</p></div>
<div class="m2"><p>لو تلقاه ضریر تائه احوالها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فی الهوی من لیس فی الکونین بدر مثله</p></div>
<div class="m2"><p>ان روحی فی الهوی من لا تری امثالها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لم تمل روحی الی مال الی ان اعشقت</p></div>
<div class="m2"><p>رامت الاموال کی تنثر له اموالها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لم تزل سفن الهوی تجری بها مذ اصبحت</p></div>
<div class="m2"><p>فی بحار العز و الاقبال یوما یالها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عین روحی قد اصابتها فاردتها بها</p></div>
<div class="m2"><p>حین عدت فضلها و استکثرت اعمالها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افلحت من بعد هلک ان اعوان الهوی</p></div>
<div class="m2"><p>اعتنوا فی امرها ان خففوا حمالها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آه روحی من هوی صدر کبیر فائق</p></div>
<div class="m2"><p>کل مدح قالها فیه ازدرت اقوالها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ییاس النفس اللقاء من وصال فائت</p></div>
<div class="m2"><p>حین تتلو فی کتاب الغیب من افعالها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حبذا احسان مولی عاد روحا اذ نفث</p></div>
<div class="m2"><p>ناولتها شربه صفی لها احوالها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ان روحی تقشع اللقیات فی الماضی مدا</p></div>
<div class="m2"><p>ثم لا تبصر مضی اذ تفکر استقبالها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اختفی العشق الثقیل فی ضمیری دره</p></div>
<div class="m2"><p>ان روحی اثقلت من دره قد شالها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مثله ان اثقل الیوم المخاض حره</p></div>
<div class="m2"><p>اوقعتها فی ردی لم تغنها احجالها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غیر ان سیدا جادت لها الطافه</p></div>
<div class="m2"><p>ان روحی ربوه و استنزلت اطلالها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سیدا مولی عزیزا کاملا فی امره</p></div>
<div class="m2"><p>شمس دین مالک اوفت لها آمالها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صادف المولی بروحی و هی فی ذاک الردی</p></div>
<div class="m2"><p>من زمان اکرمته ما رات اذلالها</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جاء من تبریز سربال نسیج بالهوی</p></div>
<div class="m2"><p>اکتست روحی صباحا انزعت سربالها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قالت الروح افتخارا اصطفانا فضله</p></div>
<div class="m2"><p>ثم غارت بعد حین من مقال نالها</p></div></div>