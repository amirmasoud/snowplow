---
title: >-
    غزل شمارهٔ ۱۵۴
---
# غزل شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>دیده حاصل کن دلا آنگه ببین تبریز را</p></div>
<div class="m2"><p>بی بصیرت کی توان دیدن چنین تبریز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه بر افلاک روحانیست از بهر شرف</p></div>
<div class="m2"><p>می‌نهد بر خاک پنهانی جبین تبریز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پا نهادی بر فلک از کبر و نخوت بی‌درنگ</p></div>
<div class="m2"><p>گر به چشم سر بدیدستی زمین تبریز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روح حیوانی تو را و عقل شب کوری دگر</p></div>
<div class="m2"><p>با همین دیده دلا بینی همین تبریز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو اگر اوصاف خواهی هست فردوس برین</p></div>
<div class="m2"><p>از صفا و نور سر بنده کمین تبریز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس تو عجل سمین و تو مثال سامری</p></div>
<div class="m2"><p>چون شناسد دیده عجل سمین تبریز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو دریاییست تبریز از جواهر و ز درر</p></div>
<div class="m2"><p>چشم درناید دو صد در ثمین تبریز را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بدان افلاک کاین افلاک گردانست از آن</p></div>
<div class="m2"><p>وافروشی هست بر جانت غبین تبریز را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نه جسمستی تو را من گفتمی بهر مثال</p></div>
<div class="m2"><p>جوهرین یا از زمرد یا زرین تبریز را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون همه روحانیون روح قدسی عاجزند</p></div>
<div class="m2"><p>چون بدانی تو بدین رای رزین تبریز را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون درختی را نبینی مرغ کی بینی برو</p></div>
<div class="m2"><p>پس چه گویم با تو جان جان این تبریز را</p></div></div>