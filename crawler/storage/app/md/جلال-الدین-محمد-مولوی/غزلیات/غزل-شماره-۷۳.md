---
title: >-
    غزل شمارهٔ ۷۳
---
# غزل شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>آمد بت میخانه تا خانه برد ما را</p></div>
<div class="m2"><p>بنمود بهار نو تا تازه کند ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشاد نشان خود بربست میان خود</p></div>
<div class="m2"><p>پر کرد کمان خود تا راه زند ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد نکته دراندازد صد دام و دغل سازد</p></div>
<div class="m2"><p>صد نرد عجب بازد تا خوش بخورد ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رو سایه سروش شو پیش و پس او می‌دو</p></div>
<div class="m2"><p>گر چه چو درخت نو از بن بکند ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر هست دلش خارا مگریز و مرو یارا</p></div>
<div class="m2"><p>کاول بکشد ما را و آخر بکشد ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ناز کند جانان اندر دل ما پنهان</p></div>
<div class="m2"><p>بر جمله سلطانان صد ناز رسد ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازآمد و بازآمد آن عمر دراز آمد</p></div>
<div class="m2"><p>آن خوبی و ناز آمد تا داغ نهد ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن جان و جهان آمد وان گنج نهان آمد</p></div>
<div class="m2"><p>وان فخر شهان آمد تا پرده درد ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌آید و می‌آید آن کس که همی‌باید</p></div>
<div class="m2"><p>وز آمدنش شاید گر دل بجهد ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمس الحق تبریزی در برج حمل آمد</p></div>
<div class="m2"><p>تا بر شجر فطرت خوش خوش بپزد ما را</p></div></div>