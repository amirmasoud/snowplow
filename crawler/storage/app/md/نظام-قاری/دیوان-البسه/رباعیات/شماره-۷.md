---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>در البسه ام مگو جواب ای سره مرد</p></div>
<div class="m2"><p>نتوان چو دو سر زیک گریبان بر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند کنی پوش زپوشی کسان</p></div>
<div class="m2"><p>(از جامه عاریت نشاید برخورد)</p></div></div>