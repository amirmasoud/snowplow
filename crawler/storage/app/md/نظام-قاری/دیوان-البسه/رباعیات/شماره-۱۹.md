---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>ای داده بجیب جامه از مدفون زه</p></div>
<div class="m2"><p>تخفیفه و دستار بامرت که و مه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصک تو ستانی بقد ارمک تو دهی</p></div>
<div class="m2"><p>(یارب تو بلطف خویش بستان و بده)</p></div></div>