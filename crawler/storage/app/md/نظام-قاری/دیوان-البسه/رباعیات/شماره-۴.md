---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>از بندقی انکه سرفرازی دارد</p></div>
<div class="m2"><p>روز طربش رو بدرازی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایصوف مشو غره بخندیدن شرب</p></div>
<div class="m2"><p>(گو با تو سر دو البازی دارد)</p></div></div>