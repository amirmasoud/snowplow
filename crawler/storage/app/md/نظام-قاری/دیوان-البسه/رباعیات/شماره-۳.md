---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گفتم که عمامه جز مجازی نبود</p></div>
<div class="m2"><p>و او را چو کلاه سرفرازی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشفته برک گفت بر وقصه مخوان</p></div>
<div class="m2"><p>(بیهوده سخن بدین درازی نبود)</p></div></div>