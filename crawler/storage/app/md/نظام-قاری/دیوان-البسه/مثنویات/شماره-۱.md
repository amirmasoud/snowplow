---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>بسر تخفیفه روزی بدستار</p></div>
<div class="m2"><p>سری میجست و بالائی زپندار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنا که طیلسان بروی بر آشفت</p></div>
<div class="m2"><p>لسان حال را بگشوده میگفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>(هر آن مهتر که با کهتر ستیزد)</p></div>
<div class="m2"><p>(چنان افتد که هرگز برنخیزد)</p></div></div>