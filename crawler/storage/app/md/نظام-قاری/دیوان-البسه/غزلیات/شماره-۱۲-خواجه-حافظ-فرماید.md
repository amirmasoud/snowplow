---
title: >-
    شمارهٔ  ۱۲ - خواجه حافظ فرماید
---
# شمارهٔ  ۱۲ - خواجه حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>خمی که ابروی شوخ تو در کمان انداخت</p></div>
<div class="m2"><p>به قصد جان من زار ناتوان انداخت</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>مرا اگر چه ببسترلت کتان انداخت</p></div>
<div class="m2"><p>ز روی صوف نظر بر نمیتوان انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخرمی که در آمد بسایه فرجی</p></div>
<div class="m2"><p>قباکله نه عجب گربر آسمان انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزیر تیغ چو سنجابرا بدید اطلس</p></div>
<div class="m2"><p>نمود یاری و خود را بروی آن انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود شرب مجرح که بود زیر افکن</p></div>
<div class="m2"><p>زمانه طرح نهالی نه این زمان انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحلقه زکمر بود در میان رمزی</p></div>
<div class="m2"><p>قبا حدیث فسن چست در میان انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببر گرفته ام این جامه کهن چه کنم</p></div>
<div class="m2"><p>نصیبه ازل از خود نمیتوان انداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه غلغلست که قاری بچرخ ابریشم</p></div>
<div class="m2"><p>بمدح تافته و شرب در جهان انداخت</p></div></div>