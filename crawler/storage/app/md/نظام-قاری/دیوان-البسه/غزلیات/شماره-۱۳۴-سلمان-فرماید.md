---
title: >-
    شمارهٔ  ۱۳۴ - سلمان فرماید
---
# شمارهٔ  ۱۳۴ - سلمان فرماید

<div class="b" id="bn1"><div class="m1"><p>هر مختصر چه داند آئین عشقباری</p></div>
<div class="m2"><p>کی در هوا مگس را باشد مجال بازی</p></div></div>
<div class="c"><p>در جواب آن</p></div>
<div class="b" id="bn2"><div class="m1"><p>ارمک بپوش و از حق میخواه جان درازی</p></div>
<div class="m2"><p>دستار بندقی بند از بهر سرفرازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن تارها بچنگست از تار و پود والا</p></div>
<div class="m2"><p>زان روی اینهمه نقش دارد بپرده سازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>والای پرمگس کی باشد چو سینه باز</p></div>
<div class="m2"><p>کی درهوا مگس را باشد مجال بازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی باشدت صفائی ایخواجه در مصلا</p></div>
<div class="m2"><p>در سعدی از نگردد رخت دلت نمازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر صاحب تمیزی بردار دامن از خاک</p></div>
<div class="m2"><p>ضایع مکن لباست چون کودکان ببازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر منست دستار میخواهمش همیشه</p></div>
<div class="m2"><p>آن کیست کو نخواهد عمری بدین درازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاری حقیقتی دان کردن ببر سقرلاط</p></div>
<div class="m2"><p>تفتیک راو ماشا هر دو شمر مجازی</p></div></div>