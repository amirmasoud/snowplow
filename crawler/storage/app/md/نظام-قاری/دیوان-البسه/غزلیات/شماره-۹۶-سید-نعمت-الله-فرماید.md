---
title: >-
    شمارهٔ  ۹۶ - سید نعمت الله فرماید
---
# شمارهٔ  ۹۶ - سید نعمت الله فرماید

<div class="b" id="bn1"><div class="m1"><p>غرقه بحر بیکران مائیم</p></div>
<div class="m2"><p>گاه موجیم و گاه دریائیم</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>خرقه صوف موجزن مائیم</p></div>
<div class="m2"><p>طالب در جیب زیبائیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما نهادیم زان دکان قماش</p></div>
<div class="m2"><p>که گزی را بنرمه بنمائیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو قطنی بنرمدست حریر</p></div>
<div class="m2"><p>چون مختم ندیم کمخائیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدیدیم چشمه مدفون</p></div>
<div class="m2"><p>در بصارت بعین بنمائیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بهای قماش هندستان</p></div>
<div class="m2"><p>کرده دهلی ذل چو دریائیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سقرلاط وصوف در چکمه</p></div>
<div class="m2"><p>گاه شیبیم و گاه بالائیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بارمک شدیم محرم خاص</p></div>
<div class="m2"><p>همچو اطلس ببخت والائیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو والا درین صفت قاری</p></div>
<div class="m2"><p>بر سر حکم شعر طغرائیم</p></div></div>