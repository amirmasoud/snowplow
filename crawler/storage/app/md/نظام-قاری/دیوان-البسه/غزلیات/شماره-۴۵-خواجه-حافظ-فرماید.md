---
title: >-
    شمارهٔ  ۴۵ - خواجه حافظ فرماید
---
# شمارهٔ  ۴۵ - خواجه حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>تا زمیخانه و می نام و نشان خواهد بود</p></div>
<div class="m2"><p>سرما خاک ره پیر مغان خواهد بود</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>تازقطنی و قدک نام و نشان خواهد بود</p></div>
<div class="m2"><p>تنم از شوق شمط جامه دران خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برزمینی که در وصندلی رخت نهند</p></div>
<div class="m2"><p>سالها سجده گه بقچه کشان خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقه انکله جیب بگوش از ازلست</p></div>
<div class="m2"><p>برهمانیم که بودیم و همان خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم مدفون چو نهد سربکنار جامه</p></div>
<div class="m2"><p>برخ شاهد کمخا نگران خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعدما و توبسی صوف سفید و سبزی</p></div>
<div class="m2"><p>که لباس تن هر پیرو جوان خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بروای دامک شلوار که بردیده تو</p></div>
<div class="m2"><p>راز لنگوته نهانست و نهان خواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخت قاری اگر از بقچه یاران باشد</p></div>
<div class="m2"><p>خلعت صوف به دوش دگران خواهد بود</p></div></div>