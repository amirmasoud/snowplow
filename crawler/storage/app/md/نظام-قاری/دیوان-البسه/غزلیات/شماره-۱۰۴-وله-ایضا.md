---
title: >-
    شمارهٔ  ۱۰۴ - وله ایضا
---
# شمارهٔ  ۱۰۴ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>باد گلبوی سحر خوش میوزد خیر ای ندیم</p></div>
<div class="m2"><p>بس که خواهد رفت بر بالای خاک مانسیم</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>رخت بین بر حمل وخیز از جامه خواب ای سلیم</p></div>
<div class="m2"><p>بس که پوشد خلق بیما سالها دلق سلیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انکه بر دنیا براه از رخت پا انداز رفت</p></div>
<div class="m2"><p>بر صراطش از گذشتن جای تشویشست و بیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه محرومم درین دار از سقرلاط و سمور</p></div>
<div class="m2"><p>دارم امیدی بخضرو سندس خلد نعیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قماش مصرنی گور است مروارید گوی</p></div>
<div class="m2"><p>نرم میگو چون غریبست او و در او یتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عطسه چون میآیدت دستار بر از سر منه</p></div>
<div class="m2"><p>تو عرقچین دوست داری فوطه فرماید حکیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انکه تن پوشید و ارمک داد و در بر صوف کرد</p></div>
<div class="m2"><p>هم ببخشد چون بکر باسین کفن باشم رمیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخت ابیاری نگر از دگمها بنموده دال</p></div>
<div class="m2"><p>انگله در جیب او چون حلقه اندر دورجیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخت سیمک دوزرا نبود رواجی در مزاد</p></div>
<div class="m2"><p>زر مگر در چار قب زآتش برون آید سلیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به کی گویی سخن قاری به وصف البسه</p></div>
<div class="m2"><p>هست این‌ها شستنی استغفرالله العظیم</p></div></div>