---
title: >-
    شمارهٔ  ۱۱۲ - لاادری قائله
---
# شمارهٔ  ۱۱۲ - لاادری قائله

<div class="b" id="bn1"><div class="m1"><p>در بدخشان لعل اگر از سنگ می‌آید برون</p></div>
<div class="m2"><p>آب رکنی چون شکر از تنگ می‌آید برون</p></div></div>
<div class="c"><p>در جواب آن</p></div>
<div class="b" id="bn2"><div class="m1"><p>پیش درزی جامه کز تنگ می‌آید برون</p></div>
<div class="m2"><p>چند تنقیصم دهد از سنگ می‌آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یادم آرد ار برآن نرمدست چون حریر</p></div>
<div class="m2"><p>ناله ابریشمی کز چنگ می‌آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستگاه صبغه الله از خم نیلی نگر</p></div>
<div class="m2"><p>هر سحر کاین اطلس گلرنگ می‌آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب رکنی از دل خارا چو حبرماویست</p></div>
<div class="m2"><p>یا خشیشی جامه کز تنگ می‌آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه بودش صوف و اطلس از همه نوعی به جهد</p></div>
<div class="m2"><p>این زمان از عهده خود رنگ می‌آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فوطه شیر و شکر از تنگه بازارگان</p></div>
<div class="m2"><p>در لطفات چون شکر از تنگ می‌آید برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌رسد از تنگنا کتان پرپهنا به خلق</p></div>
<div class="m2"><p>چون به قاری می‌رسد پرتنگ می‌آید برون</p></div></div>