---
title: >-
    شمارهٔ  ۱۳۱ - وله ایضا
---
# شمارهٔ  ۱۳۱ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>ای مقنعه و شده مرا صبحی و شامی</p></div>
<div class="m2"><p>مو بندو سرانداز چو نوری و ظلامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن زینت و ترتیب در آرایش آن گوشک</p></div>
<div class="m2"><p>خوش بود دریغا که نکردند دوامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگاه که با پیر نمد نیست جرز دان</p></div>
<div class="m2"><p>حقا که عصارا نبود رسم قیامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشن نکنی دیده بالباس چهله</p></div>
<div class="m2"><p>از رخت سیه تا ننشینی بظلامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرگار صفت انکه بزیلو چه قدم زد</p></div>
<div class="m2"><p>بیرون ننهد هرگز ازین دایره کامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جقه و دربندی و تشریف سقرلاط</p></div>
<div class="m2"><p>خاصی بجهان فرق توان کرد زعامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خواجه دهد مژده تشریف بقاری</p></div>
<div class="m2"><p>آن لحظه بدل میرسد از دوست پیامی</p></div></div>