---
title: >-
    شمارهٔ  ۱۷ - وله فی هذا الوزن قدس الله روحه
---
# شمارهٔ  ۱۷ - وله فی هذا الوزن قدس الله روحه

<div class="b" id="bn1"><div class="m1"><p>خوشتر از حمام و رخت پاک نیست</p></div>
<div class="m2"><p>کهنه گر باشد لباست باک نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که در بر جامه خود میدرد</p></div>
<div class="m2"><p>در حقیقت صاحب ادراک نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از همه رختی ببرمیکن مله</p></div>
<div class="m2"><p>هیچ رنگی به زرنگ خاک نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقلانرا ناگزیرست از لباس</p></div>
<div class="m2"><p>گر بود مجنون برهنه باک نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر وصل بر چه داند پیرهن</p></div>
<div class="m2"><p>دامن او چون زهجران چاک نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو دلق پیر خالی از عصاست</p></div>
<div class="m2"><p>بر سر سجاده چون مسواک نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی میان بسته در میدان رخت</p></div>
<div class="m2"><p>کس چوقاری در جهان چالاک نیست</p></div></div>