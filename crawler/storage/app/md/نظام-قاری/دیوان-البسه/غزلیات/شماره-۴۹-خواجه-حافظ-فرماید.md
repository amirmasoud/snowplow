---
title: >-
    شمارهٔ  ۴۹ - خواجه حافظ فرماید
---
# شمارهٔ  ۴۹ - خواجه حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>صوفی نهاد دام و سرحقه باز کرد</p></div>
<div class="m2"><p>بنیاد مگر با فلک حقه باز کرد</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>خرم تنی که گوی شب از جامه باز کرد</p></div>
<div class="m2"><p>پارا بنرمدست نهالی دراز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با انکه یکدرم نتوان بست اندرو</p></div>
<div class="m2"><p>دستار کهنه بین که مرا سرفراز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منت پذیر کرد ززیلو که با نمد</p></div>
<div class="m2"><p>از بوریاو پوستکت بی نیاز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حقا که از حقیقت مسواک غافلست</p></div>
<div class="m2"><p>حمل عمامه انکه بروی مجاز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن فشاند بر قدک آندم تنم که دست</p></div>
<div class="m2"><p>بر آستین صوف مربع دراز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کسنه قیام بطاعت توان نمود</p></div>
<div class="m2"><p>بیش و پس برهنه نشاید نماز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاری بکرد بالشک نازروی کت</p></div>
<div class="m2"><p>آنکو نداد تکیه چه عشرت چه ناز کرد</p></div></div>