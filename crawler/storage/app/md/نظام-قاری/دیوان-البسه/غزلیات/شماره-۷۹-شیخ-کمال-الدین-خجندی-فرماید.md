---
title: >-
    شمارهٔ  ۷۹ - شیخ کمال الدین خجندی فرماید
---
# شمارهٔ  ۷۹ - شیخ کمال الدین خجندی فرماید

<div class="b" id="bn1"><div class="m1"><p>چهره ام دیده چه حاصل که بخون کرد نکار</p></div>
<div class="m2"><p>که برون نقش ونگارست و درون ناله زار</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>گور ظلم نگر از رخت پر از نقش و نکار</p></div>
<div class="m2"><p>که برون نقش و نگارست و درون ناله زار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صف زخت که عنبر چه بود صدرنشین</p></div>
<div class="m2"><p>گوی بر بسته که باشد که در آید بشمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که مهلک جهه جامه نخواهی که قویست</p></div>
<div class="m2"><p>کاش میبود بدرزیت ازینجامه هزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برکسوندار نباید که بود صاحب ریش</p></div>
<div class="m2"><p>در کتاب نمدی یافته انداین اخبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق را باد چو از گرمی موئینه زدست</p></div>
<div class="m2"><p>بیداکر نیز زد او را تو مدان دور از کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که در البسه پک بیت چوقاری گوید</p></div>
<div class="m2"><p>مینهم جامه ببالایش و بر سر دستار</p></div></div>