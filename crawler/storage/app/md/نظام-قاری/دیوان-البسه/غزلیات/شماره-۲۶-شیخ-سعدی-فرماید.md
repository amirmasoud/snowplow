---
title: >-
    شمارهٔ  ۲۶ - شیخ سعدی فرماید
---
# شمارهٔ  ۲۶ - شیخ سعدی فرماید

<div class="b" id="bn1"><div class="m1"><p>این باد روح پرور ازان کوی دلبرست</p></div>
<div class="m2"><p>وین آب زندکانی ازان حوض کوثرست</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم زروی بند در ایدل منور است</p></div>
<div class="m2"><p>وزبوی عنبرینه دماغمم معطرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمخاچه حاجتست برو پیچک طلا</p></div>
<div class="m2"><p>معشوق خوبروی چه محتاج زیورست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درزی چو جامه دگمه نهادی بخانه آر</p></div>
<div class="m2"><p>کاصحاب را دودیده چو مسمار بر درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن خوش شود زعلت سرما بپوستین</p></div>
<div class="m2"><p>تشخیص کرده ایم و مداوا مقررست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در انتظار خلعت عیدی دو چشم من</p></div>
<div class="m2"><p>چون گوش روزه دار بالله اکبرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اکثر ازان بشعر کنم وصف نرمدست</p></div>
<div class="m2"><p>کزهر چه میرود سخن دوست خوشترست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>(قاری) نواست شعر تو همچون سجیف صوف</p></div>
<div class="m2"><p>و اشعار خلق جمله چو مدفون مکررست</p></div></div>