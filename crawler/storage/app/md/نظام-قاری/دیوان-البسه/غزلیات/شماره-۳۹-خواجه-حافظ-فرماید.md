---
title: >-
    شمارهٔ  ۳۹ - خواجه حافظ فرماید
---
# شمارهٔ  ۳۹ - خواجه حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>واعظان کین جلوه بر محراب و منبر می‌کنند</p></div>
<div class="m2"><p>چون به خلوت می‌روند آن کار دیگر می‌کنند</p></div></div>
<div class="c"><p>در تتبع او</p></div>
<div class="b" id="bn2"><div class="m1"><p>نازکان کین موزهٔ برجسته بر پا می‌کنند</p></div>
<div class="m2"><p>چکمه را بهر تنعم زیر و بالا می‌کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب این نوخلعتان با میلک و میخک رسان</p></div>
<div class="m2"><p>کین تکبر از قبای صوف و دیبا می‌کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکلی دارم بپرس از جامه‌پوشان زمان</p></div>
<div class="m2"><p>نیم گز این یقه‌ها را از چه بهنا می‌کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دوال احتساب شرب گویی غافلند</p></div>
<div class="m2"><p>کین همه قلب و دغل در لای کمخا می‌کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست باریکی و نرمی موجب مدح قماش</p></div>
<div class="m2"><p>تا جرانش وصف پهنا و درازا می‌کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کله با کوی صوف موج زن در اتصال</p></div>
<div class="m2"><p>حلقه گویی به گوش موج دریا می‌کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه بر جامه والا غداد مشک و زر</p></div>
<div class="m2"><p>شاهدان خوش از برای عرض کالا می‌کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیر هر تویی ز کمسان باف تویی دیگر است</p></div>
<div class="m2"><p>زآن که تعلیم خیال آن ز والا می‌کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خازنان خلد قاری در معانی این دُرَر</p></div>
<div class="m2"><p>بهر جیب حله‌ها گویی مهیا می‌کنند</p></div></div>