---
title: >-
    شمارهٔ  ۱۰۱ - خواجه حافظ فرماید
---
# شمارهٔ  ۱۰۱ - خواجه حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>من نه آن رندم که ترک شاهد و ساغر کنم</p></div>
<div class="m2"><p>محتسب داند که من کاری چنین کمتر کنم</p></div></div>
<div class="c"><p>در جواب آن</p></div>
<div class="b" id="bn2"><div class="m1"><p>ایخوش آنساعت که صوفی موجزن در بر کنم</p></div>
<div class="m2"><p>فخر بر جمله قدک پوشان بحرو برکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند ازین رو جامه گردانم بدان روی دگر</p></div>
<div class="m2"><p>تا یکی دستار را از کهنگی بر سر کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرقه از سوراخ پرجیبش بتن پوشیده شد</p></div>
<div class="m2"><p>سر فرو بردم بدامان تا کجا سربر کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن خاتون کمخا گربدست افتد مرا</p></div>
<div class="m2"><p>زیبدارگوی کریبانش درو گوهر کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریشه معجر به از پوشی خوش خط گفته</p></div>
<div class="m2"><p>این سخنهای پس چرخت کجا باور کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که در دیوان شعرم هست و صف چارقب</p></div>
<div class="m2"><p>کی نظر در چارلوح و جدول دفتر کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلنواز نرمدست ار تن در آغوشم دهد</p></div>
<div class="m2"><p>در دم ای قاری دهان و جیب او پرزر کنم</p></div></div>