---
title: >-
    شمارهٔ  ۷ - کمال خجندی فرماید
---
# شمارهٔ  ۷ - کمال خجندی فرماید

<div class="b" id="bn1"><div class="m1"><p>این چه مجلس چه بهشت این چه مقامست اینجا</p></div>
<div class="m2"><p>عمر باقی رخ ساقی لب جامست اینجا</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>این چه خرگه چه تتق این چه خیامست اینجا</p></div>
<div class="m2"><p>چترمه رایت خور ظل غمامست اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلمی گرچه بود خواجه ابیاریها</p></div>
<div class="m2"><p>همچو لالائی بیقدر غلامست اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر و بالا نبود مجلس الباس مرا</p></div>
<div class="m2"><p>کفش و دستار ندانند کدامست اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جامها سر بسر از داغ اتو سوخته دل</p></div>
<div class="m2"><p>جز نپرداخته کرباس که خامست اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صف رخت بدستار دمشقی بنگر</p></div>
<div class="m2"><p>گرز دین باف ابی تاج؟ بنامست اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ارمک و صوف درین دارنپوشم کوئی</p></div>
<div class="m2"><p>که بمن چون نخ زربفت حرامست اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاری این خرگه والا که تو در شعر زدی</p></div>
<div class="m2"><p>چشمه ماه نگویند تمامست اینجا</p></div></div>