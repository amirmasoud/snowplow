---
title: >-
    شمارهٔ  ۵۸ - مولانا جمال الدین فرماید
---
# شمارهٔ  ۵۸ - مولانا جمال الدین فرماید

<div class="b" id="bn1"><div class="m1"><p>مژده ای آرام دل کآرام جانها میرسد</p></div>
<div class="m2"><p>دل که از ما رفته بود اکنون بما وامیرسد</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>در برش برقد همه رختی ببالا میرسد</p></div>
<div class="m2"><p>جز سقرلاط بهمت کان بپهنا میرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اطلس والا جناب نازک گلروی را</p></div>
<div class="m2"><p>هر زمان خاری زسوزن بیمحابا میرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوتهی را هجو کردم کز چنین آرایشی</p></div>
<div class="m2"><p>گر بیفتد جامه او را ببالا میرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلبر رعنا چو گیرد شاهد کمخا ببر</p></div>
<div class="m2"><p>میبرد از راستی این را و آنرا میرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عید آمد وزکلاه و کفش نوایعاریان</p></div>
<div class="m2"><p>مژده پوشش بجمعی بی سر و پا میرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کوک باید چپر وزپوستین بره سپر</p></div>
<div class="m2"><p>ناوک سرمای قومی کآن بتنها میرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاه کز کردن قماش از هر دو سر در البسه</p></div>
<div class="m2"><p>صیت شعر قاری از اقصا باقصا میرسد</p></div></div>