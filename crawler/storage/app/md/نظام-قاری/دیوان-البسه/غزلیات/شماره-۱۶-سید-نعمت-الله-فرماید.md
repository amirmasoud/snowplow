---
title: >-
    شمارهٔ  ۱۶ - سید نعمت الله فرماید
---
# شمارهٔ  ۱۶ - سید نعمت الله فرماید

<div class="b" id="bn1"><div class="m1"><p>دل ندارد هر که او را درد نیست</p></div>
<div class="m2"><p>وانکه این دردش نباشد مرد نیست</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>جامه بیچاک صاحبدرد نیست</p></div>
<div class="m2"><p>غیر یکتائی بپوشش فرد نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گل بستان چو نازی پیش ما</p></div>
<div class="m2"><p>غیر کمخا در گلستان ورد نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سقر لاطش غبار از پر زهست</p></div>
<div class="m2"><p>در میان صوف باری گرد نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که هر روزی نبخشد خلعتی</p></div>
<div class="m2"><p>در میان جامه پوشان مرد نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیزه قندس سمور تیغ دار</p></div>
<div class="m2"><p>زین دو به بهره نبرد برد نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهوت انگیزی ببازار قماش</p></div>
<div class="m2"><p>شوخ چون والای سرخ وزرد نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاری اشعار تو در اوصاف رخت</p></div>
<div class="m2"><p>عید بطنان را یقین در خورد نیست</p></div></div>