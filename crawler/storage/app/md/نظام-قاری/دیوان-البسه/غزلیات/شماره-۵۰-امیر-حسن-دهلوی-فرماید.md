---
title: >-
    شمارهٔ  ۵۰ - امیر حسن دهلوی فرماید
---
# شمارهٔ  ۵۰ - امیر حسن دهلوی فرماید

<div class="b" id="bn1"><div class="m1"><p>فلک با کس دل یکتا ندارد</p></div>
<div class="m2"><p>ز صد دیده یکی بینا ندارد</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>گلستان رونق کمخا ندارد</p></div>
<div class="m2"><p>چمن آرایش دیبا ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنم تا یافت در بر صوف طاقین</p></div>
<div class="m2"><p>سر حبر و دل خارا ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترحم کن بر آنکس ای ملبس</p></div>
<div class="m2"><p>که او شلوار خود در پا ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببر آنرا که دستی رخت نو نیست</p></div>
<div class="m2"><p>دل عیش و سر صحرا ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین نه تو نپوشم پک دو توئی</p></div>
<div class="m2"><p>فلک با کس دل یکتا ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برقد شمط این اطلس چرخ</p></div>
<div class="m2"><p>گرش پهنا بود بالاندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وصف جامه‌ها قاری چو پرداخت</p></div>
<div class="m2"><p>درین طرز سخن همتا ندارد</p></div></div>