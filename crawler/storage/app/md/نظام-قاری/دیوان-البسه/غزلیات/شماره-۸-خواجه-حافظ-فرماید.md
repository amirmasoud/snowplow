---
title: >-
    شمارهٔ  ۸ - خواجه حافظ فرماید
---
# شمارهٔ  ۸ - خواجه حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>بیا که قصرامل سخت سست بنیادست</p></div>
<div class="m2"><p>بیار باده که بنیاد عمر بر بادست</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>بنای جبه کرباس سست بنیادست</p></div>
<div class="m2"><p>بیار صوف که بنیاد پنبه بر بادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آرزو نرساند برخت دست آنکس</p></div>
<div class="m2"><p>که قفل دکه ز صندوق سینه نگشادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب مدار که والا بزیرکتان رفت</p></div>
<div class="m2"><p>که این عجوزه عروس هزار دامادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بصوف از چه برد رشک خاکسار مله</p></div>
<div class="m2"><p>سمور یقه و گوی طلا خدا دادست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمامه بایقه در قفا فتاده چه گفت</p></div>
<div class="m2"><p>مراست طره فتاده ترا چه افتادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چکمه و فرجی خرمیست قاری را</p></div>
<div class="m2"><p>خنک تنی کدوی از همبران خودشادست</p></div></div>