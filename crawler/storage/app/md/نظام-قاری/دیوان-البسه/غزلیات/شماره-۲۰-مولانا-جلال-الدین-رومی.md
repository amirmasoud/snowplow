---
title: >-
    شمارهٔ  ۲۰ - مولانا جلال الدین رومی
---
# شمارهٔ  ۲۰ - مولانا جلال الدین رومی

<div class="b" id="bn1"><div class="m1"><p>از بامداد روی تو دیدن حیات ماست</p></div>
<div class="m2"><p>امروز باز روی تو دیدن چه دلرباست</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>از بامداد پیرهن نوحیوه ماست</p></div>
<div class="m2"><p>امروز باز خشخش مخفی چه دلرباست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز روز خرمی و عید پوششست</p></div>
<div class="m2"><p>امروز هر لباس که در برکنی رواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش کسی که کرد مرا عیب پوستین</p></div>
<div class="m2"><p>سرمای صبح دید و زمن عذرها بخواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرینهای گفته سر دستیم بشعر</p></div>
<div class="m2"><p>چون نیک بنگری همه انگشترین ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن روی باشدم که بود رویش آستر</p></div>
<div class="m2"><p>آن روی از که جویم و این آستر کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاری بمهر رخت چو ذرات بخیها</p></div>
<div class="m2"><p>یا چون نجوم ثابت و سیاره سماست</p></div></div>