---
title: >-
    شمارهٔ  ۱۳۶ - سید جلال الدین عضد فرماید
---
# شمارهٔ  ۱۳۶ - سید جلال الدین عضد فرماید

<div class="b" id="bn1"><div class="m1"><p>ای برگ گل سوری از خار مکن دوری</p></div>
<div class="m2"><p>از خار مکن دوری ای برگ گل سوری</p></div></div>
<div class="c"><p>در جواب آن</p></div>
<div class="b" id="bn2"><div class="m1"><p>ای مخفی کافوری از پنبه مکن دوری</p></div>
<div class="m2"><p>از پنبه مکن دوری ای مخفی کافوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پرده مستوری والا نتواند بود</p></div>
<div class="m2"><p>والا نتواند بود در پرده مستوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جبه بدستوری من مینهمت پنبه</p></div>
<div class="m2"><p>من مینهمت پنبه ایجبه بدستوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درویش تو معذوری در پاچو ازارت نیست</p></div>
<div class="m2"><p>درپا چوانارت نیست درویش تومعذوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایشرب تو منظوری مدفون بودت ناظر</p></div>
<div class="m2"><p>مدفون بودت ناظر ایشرب تو منظوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حبری خوش و صابوری خواهم ببر آوردن</p></div>
<div class="m2"><p>خواهم ببر آوردن حبری خوش و صابوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از رخت نو سوری قاری فرجی بادت</p></div>
<div class="m2"><p>قاری فرجی بادت از رخت نو سوری</p></div></div>