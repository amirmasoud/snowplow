---
title: >-
    شمارهٔ  ۸۲ - مولانا محمد حافظ فرماید
---
# شمارهٔ  ۸۲ - مولانا محمد حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>عیدست و اول گل و یاران در انتظار</p></div>
<div class="m2"><p>ساقی بروی یار ببین ماه و می بیار</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>خازن بعید ابلق سنجاب من بیار</p></div>
<div class="m2"><p>بنگر هلال را چودم قاقم آشکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این مه فزود خرقه نان در لباس عید</p></div>
<div class="m2"><p>کاری بکرد همت پاکان روزکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که دامنت ندرد زود و آستین</p></div>
<div class="m2"><p>از رخت قلب شو چو فراویز در کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلال رخت بر تن عریان من ببخش</p></div>
<div class="m2"><p>ور نو بدست نیست برو کهنه بیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پیش شاخ آمدم از دگمها بیاد</p></div>
<div class="m2"><p>چون غنچه جلوه داده بر اطراف جویبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آویختند چمته که در بند سیم ماند</p></div>
<div class="m2"><p>تا حست ازان عزیز که ترکش شد اختیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش خلعتیست فاخر و خوش جامه سلیم</p></div>
<div class="m2"><p>یارب زچشم زخم و گزندش نگاهدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دامن مکش ز گفته قاری که جیب تو</p></div>
<div class="m2"><p>گویش سزد که باشد ازین در شاهوار</p></div></div>