---
title: >-
    شمارهٔ  ۱۰ - شیخ سعدی فرماید
---
# شمارهٔ  ۱۰ - شیخ سعدی فرماید

<div class="b" id="bn1"><div class="m1"><p>کس ندانم که درین شهر گرفتار تو نیست</p></div>
<div class="m2"><p>هیچ بازار چنین گرم چو بازار تو نیست</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>کیست ای مویند درزی که هوا دار تو نیست</p></div>
<div class="m2"><p>هیچ بازار چنین گرم چه بازار تونیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یلمه صوف مشو بسته بند والا</p></div>
<div class="m2"><p>زانکه والاست شعار زن و این کار تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای فلک هست کفایت قدک رنگینم</p></div>
<div class="m2"><p>احتیاجیم بدین اطلس زرکارتونیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای سلق اهل درم از تو ندارند گزیر</p></div>
<div class="m2"><p>مگرش هیچ نباشد که خریدار تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامه با صندلی و گت بگذار ای صندوق</p></div>
<div class="m2"><p>سر خود گیر که این بقچه کشی کار تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشته ام گردگلستان و ریاض کمخا</p></div>
<div class="m2"><p>الحق ای جامه لاوسمه چو گلزار تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفت کلفتنت کرد سرآمد قاری</p></div>
<div class="m2"><p>شیوه نیست که در پیچش دستار تو نیست</p></div></div>