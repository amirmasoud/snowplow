---
title: >-
    شمارهٔ  ۹ - شیخ سعدی فرماید
---
# شمارهٔ  ۹ - شیخ سعدی فرماید

<div class="b" id="bn1"><div class="m1"><p>صبحی مبارکست نظر بر جمال دوست</p></div>
<div class="m2"><p>بر خوردن از درخت امید وصال دوست</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>افزون زرخت نو شده حسن و جمال دوست</p></div>
<div class="m2"><p>از زیور وزرست زیادت کمال دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت به گزیده و والای سیبکی</p></div>
<div class="m2"><p>پوشیده تا که خورد بری از نهال دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردم صباح عید ببر جامه عقل گفت</p></div>
<div class="m2"><p>صبحی مبارکست نظر بر جمال دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرمی بکار عشق سزد نی فسردگی</p></div>
<div class="m2"><p>سر ما برد زکله عریان خیال دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستت بود بگردن مقصود همچو جیب</p></div>
<div class="m2"><p>مانند یقه گربکشی گوشمال دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درشده ریشه دید بوالا غداد مشک</p></div>
<div class="m2"><p>ازسر گرفت دل هوس زلف و خال دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن قباچه قلمی دوخته نگر</p></div>
<div class="m2"><p>با جامه شکافته غنج و دلال دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاری به بیت البسه مدح بتان مکن</p></div>
<div class="m2"><p>در خانه جای رخت بود یا مجال دوست</p></div></div>