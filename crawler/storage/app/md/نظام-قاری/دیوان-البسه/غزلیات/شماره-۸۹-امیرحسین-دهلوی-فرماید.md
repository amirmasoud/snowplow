---
title: >-
    شمارهٔ  ۸۹ - امیرحسین دهلوی فرماید
---
# شمارهٔ  ۸۹ - امیرحسین دهلوی فرماید

<div class="b" id="bn1"><div class="m1"><p>به بزمگاه صبوحی کنان مجلس خاص</p></div>
<div class="m2"><p>حیوه نحش بود جام می به حکم خواص</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>امید هست که بنوازیم بخلعت خاص</p></div>
<div class="m2"><p>بارمک ار نرسد دست کم زجامه خاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیافت سوزن ازان بخیه چو مروارید</p></div>
<div class="m2"><p>که او ببحر پر از موج حبر شد غواص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درید پرده بکار برهنگان کرباس</p></div>
<div class="m2"><p>بخورد زخم زگازر که (والجروح قصاص)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخرج رخت زمستان گریز میجستم</p></div>
<div class="m2"><p>گرفت برد ره من که (لات حین مناص)</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه شیوه دستار و زیب جامه بود</p></div>
<div class="m2"><p>کیش برقص برازندگی بود رقاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار نفع درین جامها که میپوشی</p></div>
<div class="m2"><p>نوشته اند حکیمان بتن زروی خواص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا جهیز عروسی زن بقید آورد</p></div>
<div class="m2"><p>مگر برخت عزایش شوی ز قید خلاص</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشعر البسه بردی تو گوی ای قاری</p></div>
<div class="m2"><p>کجا بود قلمی این همه معانی خاص</p></div></div>