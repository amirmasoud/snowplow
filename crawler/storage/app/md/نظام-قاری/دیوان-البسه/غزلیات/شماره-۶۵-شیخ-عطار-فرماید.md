---
title: >-
    شمارهٔ  ۶۵ - شیخ عطار فرماید
---
# شمارهٔ  ۶۵ - شیخ عطار فرماید

<div class="b" id="bn1"><div class="m1"><p>نسبت روی تو با روی پری نتوان کرد</p></div>
<div class="m2"><p>از کجا تا بکجا بی بصری نتوان کرد</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>نسبت شرب زرافشان بپری نتوان کرد</p></div>
<div class="m2"><p>از کجا تا بکجا بی بصری نتوانکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالوو ساغر اگر زانکه بعقدت نرسد</p></div>
<div class="m2"><p>کله از گردش دور قمری نتوانکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از برای لت کتان سپری زر باید</p></div>
<div class="m2"><p>بهر آن لت کم ازین جان سپری نتوانکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسبت گونه والای بمی و برمی</p></div>
<div class="m2"><p>برخ لاله و گلبرگ طری نتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز بدستار طلا دوز و کلاه قمه</p></div>
<div class="m2"><p>ما برآنیم که دعوی سری نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاری این جلوه خوبان همه از رخت خوشست</p></div>
<div class="m2"><p>بی سر و پای نکو جلوه گری نتوان کرد</p></div></div>