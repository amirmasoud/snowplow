---
title: >-
    شمارهٔ  ۱۳۲ - شیخ کمال الدین خجند فرماید
---
# شمارهٔ  ۱۳۲ - شیخ کمال الدین خجند فرماید

<div class="b" id="bn1"><div class="m1"><p>هر لحظه بغمزه دل ریشم چه خراشی</p></div>
<div class="m2"><p>روی از نظرم پوشی و خون از مژه پاشی</p></div></div>
<div class="c"><p>در جواب آن</p></div>
<div class="b" id="bn2"><div class="m1"><p>تا جنس خطائی بود ای اطلس کاشی</p></div>
<div class="m2"><p>در بارمنه لاف تو باری چه قماشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر اطلس یزدی ندهد دست زنان را</p></div>
<div class="m2"><p>میسازد اگر زانکه بسازند بکاشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون موزه و دستار و قبا و فرجی هست</p></div>
<div class="m2"><p>آنگاه توان کآدمی از چوب تراشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر عطر شود آستی و دامن آفاق</p></div>
<div class="m2"><p>زان رخت که پوشی و از آن مشک که پاشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گلفتنت عقد نیاید بشماری</p></div>
<div class="m2"><p>تا بسته پیچ و شکن شیله و شاشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاری ببرت رخت معانی همه جمعست</p></div>
<div class="m2"><p>میبر بقد فکر معطل زچه باشی</p></div></div>