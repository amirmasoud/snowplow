---
title: >-
    شمارهٔ  ۸۸ - خواجه حافظ فرماید
---
# شمارهٔ  ۸۸ - خواجه حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>فکر بلبل همه آنست که گل شد یارش</p></div>
<div class="m2"><p>گل در اندیشه که چون عشوه کند در کارش</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>انکه خیاط برد پارچه از رووارش</p></div>
<div class="m2"><p>پنبه حلاج چرا کم نکند از کارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت را زود مدر دیر مپوسان در چرک</p></div>
<div class="m2"><p>خواجه آنست که باشد غم خدمتکارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایکه دستار سمرقندیت افتاده پسند</p></div>
<div class="m2"><p>جانب طره عزیز است فرو مگذارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سرو پای کسی هست تهی تن عریان</p></div>
<div class="m2"><p>به از آنست که در پا نبود شلوارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای آنست که اطلس رود از رنگ برنگ</p></div>
<div class="m2"><p>زین تغابن که قدک میکشند بازارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد دیدم که بیاراست برخت والا</p></div>
<div class="m2"><p>تن خود را زجوانی و نیامد عارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآنهمه رخت زنانرا بکه آرایش</p></div>
<div class="m2"><p>پهلوان پنبه خوش آمد بنظر و افزارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاری از موی شکافان و سخن پردازان</p></div>
<div class="m2"><p>کیست کو مدحت موئینه بود اشعارش</p></div></div>