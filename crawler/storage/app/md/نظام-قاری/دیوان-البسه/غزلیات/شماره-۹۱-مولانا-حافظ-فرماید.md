---
title: >-
    شمارهٔ  ۹۱ - مولانا حافظ فرماید
---
# شمارهٔ  ۹۱ - مولانا حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>مقام امن و می بیغش و رفیق شفیق</p></div>
<div class="m2"><p>گرت مدام میسر شود زهی توفیق</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>قبای ارمک و پیراهن کتان دقیق</p></div>
<div class="m2"><p>اگر بود فرجی در برش زهی توفیق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغیر صوف و سقرلاط اینهمه هیچست</p></div>
<div class="m2"><p>هزار بار من این نکته کرده ام تحقیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زرخت کهنه امید ثبات نو کردن</p></div>
<div class="m2"><p>تصوریست که عقلش نمیکند تصدیق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگاه جامه بریدن نشین بر خیاط</p></div>
<div class="m2"><p>که وصله را بکمینند قاطعان طریق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان بحبر پر از موج سر فرو بردم</p></div>
<div class="m2"><p>که عقل یافت تحیر در آنمقام عمیق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه شیوه میکند از درج پر جواهر جیب</p></div>
<div class="m2"><p>زعنبرینه لولوو دگمهای عمیق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه جامه روئی ندارم ایقاری</p></div>
<div class="m2"><p>خوشست خاطرم از فکر اینخیال دقیق</p></div></div>