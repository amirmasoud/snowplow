---
title: >-
    شمارهٔ  ۶ - وله شیرازیه ولکن یلزمها التصحیح
---
# شمارهٔ  ۶ - وله شیرازیه ولکن یلزمها التصحیح

<div class="b" id="bn1"><div class="m1"><p>مهل که گیوه بنوتن غرت چو نیست کلا</p></div>
<div class="m2"><p>که دوست نیست اثر دایما و دشمن ابا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمع نه رخت مهن بوکه نت و گوبا لوت</p></div>
<div class="m2"><p>بنی مغاره سنغرایز جمش میوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمیذنم که که بوتن چو شرم کی حدنی</p></div>
<div class="m2"><p>که ات امعرد دارائی گوشرمت با</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزیرکش چه نیکک واکتان روسی گفت</p></div>
<div class="m2"><p>جهن کتان نمیوت ازمو میزر و مقنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مختمش پش کمخا مرا و لوشی بو</p></div>
<div class="m2"><p>الوادست و بدا عروخش نه انکه ولا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی ترا زادست ثخن پهلودار</p></div>
<div class="m2"><p>نه از گریبن نه از قبن آیت فتحا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه شعر البسه گفتن مثیلها قاری</p></div>
<div class="m2"><p>یکی نه ای چه بگو تن که هیچ و نه دعا</p></div></div>