---
title: >-
    شمارهٔ  ۱۲۷ - مولانا عبیدزاکانی فرماید
---
# شمارهٔ  ۱۲۷ - مولانا عبیدزاکانی فرماید

<div class="b" id="bn1"><div class="m1"><p>افتاده بازم در سر هوائی</p></div>
<div class="m2"><p>دل باز دارد میلی بجائی</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>دل بازکردست فکر قبائی</p></div>
<div class="m2"><p>باصوف دارد روی صفائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارمک امیری صوفک نقیری</p></div>
<div class="m2"><p>اطلس چوشاهی کاسر گدائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارست جبه اغیار تشریف</p></div>
<div class="m2"><p>کین هست مخفی او خود نمائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همتای کتان گو دلفریبی</p></div>
<div class="m2"><p>مانند روسی گو جانفزائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دور گشتست دستارم از سر</p></div>
<div class="m2"><p>افتاده بازم در سر هوائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایمن زانبوه شد وزعمارت</p></div>
<div class="m2"><p>هرکو زخیمه دارد سرائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنرخت قاری گو کز کم وذیل</p></div>
<div class="m2"><p>دروی توانیم زد دست و پائی</p></div></div>