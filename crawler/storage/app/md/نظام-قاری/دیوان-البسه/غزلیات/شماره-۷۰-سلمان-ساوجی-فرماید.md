---
title: >-
    شمارهٔ  ۷۰ - سلمان ساوجی فرماید
---
# شمارهٔ  ۷۰ - سلمان ساوجی فرماید

<div class="b" id="bn1"><div class="m1"><p>اسیر بند گیسویت کجا در بند جان باشد</p></div>
<div class="m2"><p>زهی دیوانه عاقل که دربندی چنان باشد</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بر مفرش رختم نگاهت یکزمان باشد</p></div>
<div class="m2"><p>کلاهت بخشم و خلعت کمرهم در میان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برخت سبز قیغاجی خشیشی دیدم و گفتم</p></div>
<div class="m2"><p>خنک آبی که در پای سهی سروی روان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکمخا اطلس چرخی چه نسبت میکنی آخر</p></div>
<div class="m2"><p>که از این تابان فرق از زمین تا آسمان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ از زیلو نگردانم بخار بوریا از فرش</p></div>
<div class="m2"><p>خسک در راه مشتاقان بساط پرنیان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زگرد آن ره مفتون خطی خواندم که تفسیرش</p></div>
<div class="m2"><p>یکی داند که همچون دگمه ذهنش خرده دان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر وی یکدیگر پوشیدن رخت آنچنان باید</p></div>
<div class="m2"><p>که روسی زیر و بالا صوف و اطلس در میان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوای سرمه دان عاج در صندوق من یابی</p></div>
<div class="m2"><p>در آنساعت که خاک تیره ام در استخوان باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زدنیا میرود قاری چو کرباس کفن ساده</p></div>
<div class="m2"><p>ولیکن شعر رنگینش بماند تا جهان باشد</p></div></div>