---
title: >-
    شمارهٔ  ۴۸ - شیخ سعدی فرماید
---
# شمارهٔ  ۴۸ - شیخ سعدی فرماید

<div class="b" id="bn1"><div class="m1"><p>پیش رویت دگران صورت بر دیوارند</p></div>
<div class="m2"><p>ته چنین صورت معنی که تو داری دارند</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>گلها پیش گل شرب سراسر خارند</p></div>
<div class="m2"><p>جامهائی که ببارند جز اطلس بارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر دستار که پیچش و مندیله او</p></div>
<div class="m2"><p>نیست چیزی که بگیرند و دگر بگذارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحقارت منگر کاسترو خضری و شال</p></div>
<div class="m2"><p>که ببازار قماش این همه اندر کارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکسان را که تو بینی بسه وردار لباس</p></div>
<div class="m2"><p>جامه شان بنده و خود خواجه خدمتکارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت اطلس چرخی چو بدیدم گفتم</p></div>
<div class="m2"><p>پیش رویت دگران صورت بر دیوارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامها دیده ام ای طرفه عذار والا</p></div>
<div class="m2"><p>نه چنین صورت معنی که تو داری دارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاری این اطلس کمخای نفیسست که خود</p></div>
<div class="m2"><p>همه پشمینه خرانند که دربازارند</p></div></div>