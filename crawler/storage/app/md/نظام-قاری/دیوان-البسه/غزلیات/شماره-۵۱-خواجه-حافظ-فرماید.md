---
title: >-
    شمارهٔ  ۵۱ - خواجه حافظ فرماید
---
# شمارهٔ  ۵۱ - خواجه حافظ فرماید

<div class="b" id="bn1"><div class="m1"><p>مطرب عشق عجب ساز و نوائی دارد</p></div>
<div class="m2"><p>زخم هر زخمه که زد راه بجائی دارد</p></div></div>
<div class="c"><p>در جواب او</p></div>
<div class="b" id="bn2"><div class="m1"><p>گل بر اطلس اگر چند قبائی دارد</p></div>
<div class="m2"><p>نه قبائیست که گویند بهائی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشنوا یخواجه تو در مذهب ارباب لباس</p></div>
<div class="m2"><p>که قبای مله بیصوف صفایی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طیلسان صوفی ارمک بود از بند قیش</p></div>
<div class="m2"><p>وزگلیم عسلی نیز ردائی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش گرفتند بسنجاب زمستان خرگاه</p></div>
<div class="m2"><p>دولتی انکه چنین آب و هوائی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بر شاهد ما اطلس والا نگرید</p></div>
<div class="m2"><p>چاک در دامن او راه بجائی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر ششماه کتان تاب نیارد در بر</p></div>
<div class="m2"><p>بنده ارمک خویشم که وفائی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرقه پوش ارچه شد از مفرش و مرکب عاری</p></div>
<div class="m2"><p>خوب و مرغوب جرزدان و عصائی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست جز اطلس و الباغ و میان تو کاسر</p></div>
<div class="m2"><p>پادشاهی که بهمسایه گدائی دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پر بدستار طلا دوز نگه کن قاری</p></div>
<div class="m2"><p>کانکه بنهاده بسر فر همائی دارد</p></div></div>