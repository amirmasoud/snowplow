---
title: >-
    شمارهٔ ۱۰ - مولانا خواجو فرماید
---
# شمارهٔ ۱۰ - مولانا خواجو فرماید

<div class="b" id="bn1"><div class="m1"><p>وجه برات شام بر اختر نوشته اند</p></div>
<div class="m2"><p>و اموال زنگ برشه خاور نوشته اند</p></div></div>
<div class="b2" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>اوصاف شمله بر علم زر نوشته اند</p></div>
<div class="m2"><p>القاب بندقی بسراسر نوشته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صوف رقعه بمختم رسانده اند</p></div>
<div class="m2"><p>وزحبر کاغذی بمحبر نوشته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدح قماش رومی و حسن ثبات آن</p></div>
<div class="m2"><p>برطاق جامه خانه قیصر نوشته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرح قماش مصری و جنس سکندری</p></div>
<div class="m2"><p>برشامیانهای سکندر نوشته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در وصف عنبرینه جیب آنچه گفته ام</p></div>
<div class="m2"><p>بر قرص کشتهای معنبر نوشته اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عصمت و طهارت خاتون نرمدست</p></div>
<div class="m2"><p>یاران بقچه کش همه محضر نوشته اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تعویذ چشم زخم نگر کز عذاد مشک</p></div>
<div class="m2"><p>بر جامهای احمر و اصفر نوشته اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رازی که در میان سر آغوش و پیچک است</p></div>
<div class="m2"><p>آن راز سر بمهر بمعجر نوشته اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوی سنجیف صوف زمدفون شکایتی</p></div>
<div class="m2"><p>پیچیده در لباس مکرر نوشته اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مستوفیان مخفی وا بیاری و بمی</p></div>
<div class="m2"><p>وجه برات فوطه بمیزر نوشته اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در جمع رختها چو کلانتر عمامه است</p></div>
<div class="m2"><p>وجه برات ازان بکلانتر نوشته اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منشور خرگه وتتق و چتر و سایبان</p></div>
<div class="m2"><p>بر کندلان چرخ مدور نوشته اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جز دیده صدف زالرجاق ننگرد</p></div>
<div class="m2"><p>خطی که برعبائی استر نوشته اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مدح سلیم ژنده و دلق الف نمد</p></div>
<div class="m2"><p>بر دلق سلجقی همه یکسر نوشته اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گوئی برات جامه من خازنان بخت</p></div>
<div class="m2"><p>برتن برهنگان قلندر نوشته اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مردم زکهنگی سرو دستار در قدم</p></div>
<div class="m2"><p>آشفته را نگرکه چه در سر نوشته اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیجامه نکو نتوان شد بدعوتی</p></div>
<div class="m2"><p>این رمز را بپرده هر در نوشته اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در جامه خواب گوش بزیر افکنی نکو</p></div>
<div class="m2"><p>بر بالش این لطیفه و بستر نوشته اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنگر خط غبار خشیشی که صفحه</p></div>
<div class="m2"><p>زان خط بهیچ کاغذ و دفتر نوشته اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قاری مصنفات توبر پوشی و برک</p></div>
<div class="m2"><p>هرجا رفوگران هنرور نوشته اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر شاه بیت من که درین طرز گفته ام</p></div>
<div class="m2"><p>شاهان بگرد چار قب زر نوشته اند</p></div></div>