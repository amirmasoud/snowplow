---
title: >-
    شمارهٔ ۱۴ - سید حسن ترمدی گوید
---
# شمارهٔ ۱۴ - سید حسن ترمدی گوید

<div class="b" id="bn1"><div class="m1"><p>سلام علی دار ام الکواعب</p></div>
<div class="m2"><p>بتان سیه چشم مشکین ذوائب</p></div></div>
<div class="b2" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>(لبسنا لباسا لطیف الجبائب)</p></div>
<div class="m2"><p>شی صوف مشکین صفت در غیاهب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزیر منور عروس منصه</p></div>
<div class="m2"><p>تتقها بگردش مشلشل جوانب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدیبای چینی حلل را محلی</p></div>
<div class="m2"><p>باعلام پیشک صدور مناکب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریبان و اطلس بدرها ودگمه</p></div>
<div class="m2"><p>منور بسان سپهر از کواکب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جیوب لباسات همچون مشارق</p></div>
<div class="m2"><p>چو اذیال کآمد بپوشش مغارب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امیران ارمک سلاطین اطلس</p></div>
<div class="m2"><p>گزیده زسنجاب و ابلق مراکب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراسر سر آغوش و والا و موبند</p></div>
<div class="m2"><p>چو خوبان گلروی مشکین ذوائب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان بندهای قصب هر یکی را</p></div>
<div class="m2"><p>بدیدم برابریشمش پنبه غالب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلاه و عرقچین و مسحی و موزه</p></div>
<div class="m2"><p>چو ارواح بگزیده دوری زقالب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لباسات رومی و چینی نفایس</p></div>
<div class="m2"><p>قماشات هندوستانی غرایب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درآنان که ایزار در پاندارند</p></div>
<div class="m2"><p>نظرکن چو خواهی که بینی عجایب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مبر جامه نارسا و رببری</p></div>
<div class="m2"><p>بیندیش پایان کار و عواقب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگر موجها در خشیشی که بینی</p></div>
<div class="m2"><p>نشان سپهر ونجوم ثواقب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بروی قبای کهن جامه نو</p></div>
<div class="m2"><p>بهر تن که پوشند باشد معایب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توان آدمی ساخت از رخت رنگین</p></div>
<div class="m2"><p>چو آن لعبتکها که سازد ملاعب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میان بند و الباغ ودستار و موزه</p></div>
<div class="m2"><p>سزد با هم ارزانکه باشد مناسب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بیکار سرما که تنها بلرزد</p></div>
<div class="m2"><p>مگر پهلوان پنبه باشد محارب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن حرب قندس چو آید زخشمش</p></div>
<div class="m2"><p>شود موی بر تن چو نیش عقارب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود چکمه از دگمه پا درازش</p></div>
<div class="m2"><p>بکف گرز و همچون گرازان مضارب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خریدم یکی کفش نو جامه بدرید</p></div>
<div class="m2"><p>ندیدم ازین جنس کعاب کاعب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دهد بندقی هر زماتم فریبی</p></div>
<div class="m2"><p>شکیبم از و نیست (طال المعاتب)</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدیدم ذهها بر اعلام دستار</p></div>
<div class="m2"><p>دلم میل آن کرد(والصبر ذاهب)</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگر اطلس وصوف دارد مفاصل</p></div>
<div class="m2"><p>که داغ از اتو کردنش بود واجب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوالای پر مگس بین و دامک</p></div>
<div class="m2"><p>ذباب ار ندیدی و دام عناکب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خوشا آن شمطها و آن صاحبیها</p></div>
<div class="m2"><p>که آرند سوغات مارا صواحب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بمقدار تشریف و خلعت بیابی</p></div>
<div class="m2"><p>بمحفل جواب سلام و مراحب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنانست دستار پیچیدنم صعب</p></div>
<div class="m2"><p>که گوئی که باید بریدن سیاسب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بمحراب و سجاده رونه زمانی</p></div>
<div class="m2"><p>رها کن بتان محلل حواجب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گذشتم زناگاه بر محفلی خاص</p></div>
<div class="m2"><p>همه جامه نحشان واهل مناصب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در اندیشه کین رختها بر که پوشم</p></div>
<div class="m2"><p>که باشد برازنده این مراتب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خرد گفت ممدوح اهل العمائم</p></div>
<div class="m2"><p>(معین البرایا)(کفیل المارب)</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پناه امم زین اعیان (علی) آن</p></div>
<div class="m2"><p>که چرخش بسجاده داریست راغب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بمسند مه و آفتابش ازائک</p></div>
<div class="m2"><p>عطارد بدیوان جاهش محاسب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بخطهای ابیاری و برد و مخفی</p></div>
<div class="m2"><p>نوشتند القاب ومدح و مناقب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان جامه بخشی که رختی که پوشد</p></div>
<div class="m2"><p>بجز یک زمان نبود او را مصاحب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهان گفت با چرخ کمحلی که برکن</p></div>
<div class="m2"><p>بعهدش ز سر این لباس مصائب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو رایت جناب(وی اعلی المواقف)</p></div>
<div class="m2"><p>چو خرگاه ذات وی اقصی المطالب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زبهر عرقچین واعظ ازین پیش</p></div>
<div class="m2"><p>شدندی برهنه سران جمله تائب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بهر گوشه دستار بندان نبودی</p></div>
<div class="m2"><p>گذرشان شبانگاه از ترس سالب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از و خلعت تربیت تا نبودش</p></div>
<div class="m2"><p>نشد طیلسان دار برجیس خاطب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حسودت چه سودش بود شرب زرکش</p></div>
<div class="m2"><p>که چون شمع جان داده (والجسم ذائب)</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بجزقیف و کمخاکه دل میر بایند</p></div>
<div class="m2"><p>ندیدم بعهدت دگر قلب و غاصب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو سرما که او را دوا پوستین است</p></div>
<div class="m2"><p>علل را کنی دفع از فکر صائب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدین نظم پیچیده وین طرز مخصوص</p></div>
<div class="m2"><p>مرا هست انعام و الباس واجب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو رختم متاعی که آورد کاسد</p></div>
<div class="m2"><p>که دیدست بیمزد چون بنده کاسب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>الاتا نخواهند موئینه گرما</p></div>
<div class="m2"><p>کتانرا بسرما نباشند طالب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فلک رخت جاه تراقیچیجی باد</p></div>
<div class="m2"><p>ز تشریف الطاف ستار واهب</p></div></div>