---
title: >-
    شمارهٔ ۵ - در عرض دادن موئینه لشکر خود را
---
# شمارهٔ ۵ - در عرض دادن موئینه لشکر خود را

<div class="b" id="bn1"><div class="m1"><p>شه سمور بعرض سپه علامت را</p></div>
<div class="m2"><p>علم نمود ز پر همای بر افسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمود پوشن و جوشن زپشت شیر وپلنگ</p></div>
<div class="m2"><p>شده بتوسن ابلق سوار هر صفدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهر دوروی کشیدند صف و آرایش</p></div>
<div class="m2"><p>که هست قیتل رخت و نفایس زیور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبارزان کتان چون بقلب کیخاتو</p></div>
<div class="m2"><p>عیان شدند زعول قصبچه در لشکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرختهای قصاره خروش برغوخاست</p></div>
<div class="m2"><p>چنانکه گوش کلاه فلک ازان شدکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زتیغ آتش والای سرخ هیجاشد</p></div>
<div class="m2"><p>مثال اطلس چرخی بتاب خسقی خور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زدامن و یقه و آستین وبند قبا</p></div>
<div class="m2"><p>همه ندای ببند و بکش بگیر و ببر</p></div></div>