---
title: >-
    شمارهٔ ۱۷ - در تتبع ظهیر فاریابی
---
# شمارهٔ ۱۷ - در تتبع ظهیر فاریابی

<div class="b" id="bn1"><div class="m1"><p>سپیده دم که شدم حله پوش حجله و سور</p></div>
<div class="m2"><p>(ویلبسون) ثیابا شنیدم از لب حور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگوش شه کلهی این ندا زخازن خلد</p></div>
<div class="m2"><p>رسید کای شرف تاج قیصر و فغفور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراب چون که شد از روغن چراغ لباس</p></div>
<div class="m2"><p>گمان مبر که بیکمشت گل شود معمور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بباب محفل تشریف دل منه که ترا</p></div>
<div class="m2"><p>زتیمچه وزکله برکشیده اند قصور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رزگوش پنبه برون آرای گتو که به پیش</p></div>
<div class="m2"><p>مسافتی است ترا ریسمان صفت بس دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی نشیب و فرازت بره چوکفش و کلاه</p></div>
<div class="m2"><p>زتنگنای قبا تا بجا مه کاه قبور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر حریر تنت عنبری و کافوری</p></div>
<div class="m2"><p>دو خادمند یکی عنبر و یکی کافور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنیش با عسلی خرقه زد بسی سوزن</p></div>
<div class="m2"><p>که دوخت برتن خود شرب زرفشان زنبور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشاده بر رخ کمخاست دیده الجه</p></div>
<div class="m2"><p>بدان دلیل که این ناظرست وان منظور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگر که بالش زربفت و نطع زیلوچه</p></div>
<div class="m2"><p>زکتم غیب که میآورد بصدر صدور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که داد این قلمی را فراز بوقلمون</p></div>
<div class="m2"><p>که نقشش آمده هر دم زمخفی بظهور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به بند هیکل مصحف که کرد ابریشم</p></div>
<div class="m2"><p>روا که داشت دگر ره بتاره طنبور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو کفش راست یقین پایه فتادن خویش</p></div>
<div class="m2"><p>برآمدن بسر منبرش بود زغرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مقرر است برختی که چند دست رود</p></div>
<div class="m2"><p>بهیچ روی تغیر نمیشود مقدور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو در محاصره پشه خانه بتموز</p></div>
<div class="m2"><p>زکندلان بچه نمرودسان شوی مغرور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سیه کلیمی شده سفید روئی بیت</p></div>
<div class="m2"><p>دو آیتند بهر دو خطی بمی مسطور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر چه شاهد والا بپرده میدارند</p></div>
<div class="m2"><p>زمردمش نتوانند داشتن مستور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چراغ اطلس گلگون بجامه دان شمعی است</p></div>
<div class="m2"><p>که آفتاب بپروانه خواهد از وی نور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بملک رخت سقرلاط پادشا آمد</p></div>
<div class="m2"><p>امیر ارمک و صوف مربعش دستور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قطیفه از شرفست آفتاب رخت ولی</p></div>
<div class="m2"><p>بیمن رخت شهان گشت در جهان مشهور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو گز بچوب در آید بمعرض کرباس</p></div>
<div class="m2"><p>قیاس کار زاستاد کیریا مزدور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برای لشگر سرماست قلعه جبه</p></div>
<div class="m2"><p>که دارد از یقه و جیب کرد خندق وسور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مثال تاج بدستار و بر سر آن مسواک</p></div>
<div class="m2"><p>چو موسی است و عصا کو برآمدست بطور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر چه تالب گورست خوردنی همراه</p></div>
<div class="m2"><p>لباس نیست زتو دور تابیوم نشور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگوش وصف در کوی جامه ای قاری</p></div>
<div class="m2"><p>برهنه راست بسی به زلولو منشور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حسود کوز شکم دائما سخن کفتی</p></div>
<div class="m2"><p>بداد پشت که دارم بدست تیغ سمور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بروزه نیست مرا غیر غصه جامه</p></div>
<div class="m2"><p>مگر بعید کنم دل زخرمی مسرور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود که دامن رختی زنو بدست آرم</p></div>
<div class="m2"><p>بعهد باذل تشریف محفل جمهور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قضا دثار شریعت شعار علم اثات</p></div>
<div class="m2"><p>خزینه حکم و نقد علم را گنجور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بریده برقد او رخت سروری و حسب</p></div>
<div class="m2"><p>چنانچه نیست باندامترازان مقدور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>طراز آستی شرع رکن دین(مسعود)</p></div>
<div class="m2"><p>که هست دامن جاهش بری زکرد فتور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بمسندش بنهادست متکا خورشید</p></div>
<div class="m2"><p>بهرکجا که مشرف ازوست صدر صدور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>معاندش چو فراویز رانده اند از آن</p></div>
<div class="m2"><p>چو یقه باز پس افتاده بهر جمع امور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بطیلسان چه کند فخر مشتری کاورا</p></div>
<div class="m2"><p>سپهر کرده بسجاده داریش مامور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>توئی که دست تو چون شرب زرفشان آمد</p></div>
<div class="m2"><p>دلت چو صوف پر از موج بروی آب بحور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زکوی جیب کمالت کهی که شرح دهم</p></div>
<div class="m2"><p>بود بگوش در استاده لولوی منشور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>میان اهل عمایم سرآمدست چوتاج</p></div>
<div class="m2"><p>چو موزه هرکه درین آستانه کرد عبور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ترا علم چو بقاضی القضاه میکردند</p></div>
<div class="m2"><p>نبود رایت آفاق این سرادق نور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گهی که اطلس رای تو روی بنماید</p></div>
<div class="m2"><p>چو کرد پنبه بود مهر بر مثال ذرور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فکنده ببردهی جامه از خیر</p></div>
<div class="m2"><p>برون کشیده دگر از تنش لباس شرور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نگشت مخفی و پوشیده این که بی حجت</p></div>
<div class="m2"><p>جفای ماه زکتان بعدل کردی دور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زحکم تست که والابسان دستاری</p></div>
<div class="m2"><p>ز احترام ببندند بر سر منشور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همیشه تا که ببر صوف وارمکست و کتان</p></div>
<div class="m2"><p>لباس عیدی و رخت بهار و جامه سور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تریز جامه عمرت سجیف سر مد باد</p></div>
<div class="m2"><p>بدر ز آن عدد بخیها سنین و شهور</p></div></div>