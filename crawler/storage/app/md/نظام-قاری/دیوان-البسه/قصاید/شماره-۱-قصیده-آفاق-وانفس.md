---
title: >-
    شمارهٔ ۱ - (قصیده آفاق وانفس)
---
# شمارهٔ ۱ - (قصیده آفاق وانفس)

<div class="b" id="bn1"><div class="m1"><p>نیست پوشیده بر اهل خرد و استبصار</p></div>
<div class="m2"><p>زانکه(الناس لباس) است کلام اخیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که از اطعمه سیری زپی البسه رو</p></div>
<div class="m2"><p>که تن از رخت عزیز است و شکم پرور خوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خور شست و کنش و پوشش و ارباب تمیز</p></div>
<div class="m2"><p>نیستشان هیچ ازنیگونه گزیری ناچار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلعتی دوخته ام برقد اشعار چنان</p></div>
<div class="m2"><p>که نه پوشیده و نه کهنه شود لیل و نهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درزیش درزی معنی و خرداستاد است</p></div>
<div class="m2"><p>رنگرز دست خیالست و تفکر قصار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شستن رخت مرا چرخ حصین چون صابون</p></div>
<div class="m2"><p>ابر لیفست و بپرداخت کدنیه اشجار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوش کن تا که بدوشت کنم اینجامه نو</p></div>
<div class="m2"><p>برکن از خویشتن آنجامه پار و پیرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست در البسه هر چیز که در آفاقست</p></div>
<div class="m2"><p>برضمیر تو کنم چند نظیرش اظهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسمان خرگه وزیلوست زمین خارا کوه</p></div>
<div class="m2"><p>اطلس و تافته دان مهرومه پر انوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابرکرباس و شفق خسقی و شامست سمور</p></div>
<div class="m2"><p>صبح قاقم شمرو حبر پر از موج بحار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لوح سجاده و مسواک قلم میزر عرش</p></div>
<div class="m2"><p>صندلی کرسی و فرشست فراش از آثار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صوف گرما بودو جنس حصیری سرما</p></div>
<div class="m2"><p>رخت زردست خزان جامه سبزست بهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شش جهت چاک پس و پیشت و جیب و دامن</p></div>
<div class="m2"><p>و آستین هردو که آنست ترا دست افزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ترا پنج حواسست کزان داری خط</p></div>
<div class="m2"><p>پنج وصله است ز تو جامه چنان برخوردار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هفت کویست گریبان ترازان هفت است</p></div>
<div class="m2"><p>عدد ارض و سماوات و نجوم سیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چار عنصر زمن ارزانکه بپرسی هریک</p></div>
<div class="m2"><p>با تو گویم که بمانی عجبم در گفتار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوع والا که وراباد صبا میخوانند</p></div>
<div class="m2"><p>بادت آن آتش والای برنک گلنار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اطلس ماویت آبست روان و ین دریاب</p></div>
<div class="m2"><p>مله خاک که آنست لباس ابرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برش جامه قضا و قدرش کز گردون</p></div>
<div class="m2"><p>اجل وحادثه ببریدن و زخم ای هشیار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پوشش ماتم و سورست دو کون ای سرور</p></div>
<div class="m2"><p>ورسوالت ز سه روحست بدان این اسرار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روحی ابریشم و روحیست دگر پنبه زوصف</p></div>
<div class="m2"><p>سیومین روح بود پشم بگفتم یکبار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مبدات پنبه بتحقیق و معادست کفن</p></div>
<div class="m2"><p>تن و جان تو درین کارکه این پود آن تار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جسم رختست جواهر عرض آن الوان</p></div>
<div class="m2"><p>ستر آن جمله محیطست و سجافست مدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صفت روز و شبت نیز شب اندر روزست</p></div>
<div class="m2"><p>نقش دوزیت در اثواب کواکب انگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زیر و بالانه دوتا کارگهش نساج است؟</p></div>
<div class="m2"><p>عالم سفلی و علویت بدان زاستحضار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وصف تشریح زسرتا قدمت بنمودم</p></div>
<div class="m2"><p>هم در آن خواب اگر زانکه بعقلی بیدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جنتت جامه پاکست و عذابت دوزخ</p></div>
<div class="m2"><p>هست پیراهن چرکین چو ضمیر اشرار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیست معلوم صراطت بجز از پای انداز</p></div>
<div class="m2"><p>چون قیامت که بود برهنگی برتن زار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باز جلپاره مرقع صفت طفلی تست</p></div>
<div class="m2"><p>نخ دیبای ثمینت چوشبابت پندار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کهلی آنروز که ریشت شمرند ابیاری</p></div>
<div class="m2"><p>پیریت صوف سفیدست که استغفار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صورت دیو پلاسست و پری کمسان دوز</p></div>
<div class="m2"><p>نیک و بد شال و حریرست نبرد احرار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مغربت چیست دواج شب تار و مشرق</p></div>
<div class="m2"><p>جیب خرقه است سر از جیب خرافات برآر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خشم و قهر و غضبت جوشن و جیه است و زره</p></div>
<div class="m2"><p>شهوتت جامه خوابست و لباست شب تار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیشوازست زن و مرد قبا وانچه درو</p></div>
<div class="m2"><p>چاک پس هست مخنث بود و بی هنجار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اطلس است امردو ابیاری سبزست بخط</p></div>
<div class="m2"><p>پوسیتن صاحب ریشست و در آن هم اطوار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در خور ریش سفیدست چو شیخان کامو</p></div>
<div class="m2"><p>وان سیه بره سیه ریش بخاطر میدار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قندس آنست که او ریش کندرنک مدام</p></div>
<div class="m2"><p>چند نیرنک چو روباه کنی ایطرار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>داری اخلاق پسندیده قماشات نفیس</p></div>
<div class="m2"><p>گر بدانی چه قماشی نکنی استکبار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خانه را که دروهست مقامت شب و روز</p></div>
<div class="m2"><p>هم درین جامه بگویم صفت او هموار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سربا مست گریبان یقه با مقلب</p></div>
<div class="m2"><p>آن کنیسه که زدند از پی دفع امطار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حد آن و ربدن و تیرز آن لنگیها</p></div>
<div class="m2"><p>جیب پهلو بود و چاک درو روزن دار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آستین شاه نشینها که برون میدارند</p></div>
<div class="m2"><p>چارسو خشتک و ایزاره فراویز انگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جفت زلفین بدر آن انگله و گوی بود</p></div>
<div class="m2"><p>بخیها جمله در آن باب مثال مسمار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کس ازین جنس نفیسی ننمودست انفس</p></div>
<div class="m2"><p>گرچه گفتند در آفاق و در انفس بسیار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر که او وصله معنی برد از جامه من</p></div>
<div class="m2"><p>علم دزدی او باد عیان روز شمار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بلباس دگر این طرز حدیثم بشنو</p></div>
<div class="m2"><p>دستبردی چو نمودم بجهان زین اشعار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سرور جمله اثواب ز روی معنی</p></div>
<div class="m2"><p>هست برد یمنی لبس رسول مختار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جبه برد که او جنه برد آمده است</p></div>
<div class="m2"><p>پشت گرمی وی از پینه زروی پندار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بابرک گفت که دوزم عسلی تو بدوش</p></div>
<div class="m2"><p>که بسرما نکنم حرب بگاه پیکار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از پی حرب عدوی تو زره بافدابر</p></div>
<div class="m2"><p>آسمان جبه وانجم همه بر وی مسمار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مه سپر مهر کلاخود و کمان قوس قزح</p></div>
<div class="m2"><p>ناوکت تیر و سماکست و سها نیزه گذار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ابرمانند عروسیست سپیدش چادر</p></div>
<div class="m2"><p>انکه از برق پدید آمده سرخی ازار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شسته کرباس که پرداخته در می پیچند</p></div>
<div class="m2"><p>کاغذی دان که زقر طاس به پیچد طومار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>موج در صوف مربع نگرای اهل تمیز</p></div>
<div class="m2"><p>دل بدریا فکن وزر ببهایش بشمار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گرچه ماشاه و سقرلاط بهم مشتبهند</p></div>
<div class="m2"><p>هریکی رابحد خویش شناسد ابصار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ایکه بامیرزی و چکمه برک حاجت نیست</p></div>
<div class="m2"><p>پیشتر پازگلیم خودت آخر مگذار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پوستین بخیه چو از جیب نماید بندند</p></div>
<div class="m2"><p>تسمه از گوز گره بر بن ریشش ناچار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نخوت شرب بوالا که زپر مکس است</p></div>
<div class="m2"><p>چیست در باغ چو طاوس مگس هست بکار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خصم میخک نکند فرق زکمخاورنه</p></div>
<div class="m2"><p>کارگاهیست مرا از همه جنسی دربار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نیش شاخی که بقیقاج بود دانی چیست</p></div>
<div class="m2"><p>گلستانی که به بندند بگردش انهار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>صاحبی را که زکتان هوس کیسه است</p></div>
<div class="m2"><p>کیسه از سیم بپرداز بگو در بازار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زوده نرم ستان از جهت پیراهن</p></div>
<div class="m2"><p>کانچه در زیر بود نرم به از استظهار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>متکا درکله با صندلی اینمعنی گفت</p></div>
<div class="m2"><p>که توئی بغچه کش وتکیه بمن دارد یار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>صندلی داد جوابش که توئی آلت طیش</p></div>
<div class="m2"><p>صندلی و قتلی چند نهی شرمی دار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جامه حبر و دروگوی زمرواریدست</p></div>
<div class="m2"><p>راست چون بحر کز و خاسته در شهوار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تانهم بالش زین گرد قطیفه چو صدف</p></div>
<div class="m2"><p>بهر آن راحت جانست دو چشم من چار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر غرض معنی دستار بکسمه است ترا</p></div>
<div class="m2"><p>نو خطان پیش که بندند چو کسمه دستار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نرمدستی که بهجرانش شب اندر روزم</p></div>
<div class="m2"><p>تافته روزمن و مانده بعشقش افکار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چادر آن صنم ابرست و قصاره رعدش</p></div>
<div class="m2"><p>آتش برق نمودست زگلگون شلوار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خط الوانست بدستارچه یزدی لیک</p></div>
<div class="m2"><p>یزد یانرا بخط سبز کشد دل بسیار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ایکه پهلو بشکم داری و سنجاب و سمور</p></div>
<div class="m2"><p>انکه بر پوستکی خفته ز حالش یاد آر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نقش والای لطیف قلغی گربیند</p></div>
<div class="m2"><p>قالبک زن سز نقش نخواند در کار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر سقرلاط ترا هست و نمد میپوشی</p></div>
<div class="m2"><p>سردیست این بنمدمال چه عیبست و عوار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در بر آن کسوت سنجاب نه دور از کارست</p></div>
<div class="m2"><p>آبگرمی بزمستان چه کند رغبت یار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رخت ابیاری و مثقالی و تابستانی</p></div>
<div class="m2"><p>ساده در زیر و خط آورده ببالا پندار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>فکرکتان چه کنی چون بزمستان برسی</p></div>
<div class="m2"><p>پوستین را چه کنی غم چو رسد فصل بهار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مریم ای یا رنه رشتست یکی شیرین باف</p></div>
<div class="m2"><p>بسر خود بخر ارهست گزی صد دینار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>قفسه هر که بمدفون علادینی دید</p></div>
<div class="m2"><p>مرغ مدفون بقفس یافته ای خوب شعار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>التفات ار بمجرح نکند دارائی</p></div>
<div class="m2"><p>پادشاهیست چودارا زگدا دارد عار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چشمهای الجه باز بروی مله ایست</p></div>
<div class="m2"><p>همچو عاشق که کند دیده بروی دلدار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نازکت چار شب اولیست که بالا افکن</p></div>
<div class="m2"><p>چون درشتست و قوی میرسدت زان آزار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>در نماز آر بسجاده شطرنجی رخ</p></div>
<div class="m2"><p>تابری دست بطاعت زصغار و زکبار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از سر مردم شهری هوس پوشی رفت</p></div>
<div class="m2"><p>تا که این عقد سپیچ آمده اکنون بشمار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گرد آن پرده گلگون چو مشلشل دیدم</p></div>
<div class="m2"><p>آمدم یاداران زلف و زان رنگ و عذار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ایکه یکتائیت از زیر دو توئی بمی است</p></div>
<div class="m2"><p>اینچنین زیر و بمی برد زما صبر و قرار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>حبذا بخت نهالی که نهالی چون تو</p></div>
<div class="m2"><p>خیزدش هر سحری تازه و خرم زکنار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گلهایی که بر آن بالش زردوز افتاد</p></div>
<div class="m2"><p>همچنانست که بر تخته دیبا دینار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گر سربسته والا بگشاید خاتون</p></div>
<div class="m2"><p>بوی نسرین و قرنفل برود در اقطار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>جبه سان گر ببر آن سرو قبا پوش آرم</p></div>
<div class="m2"><p>فرجی یابم و از بخت شوم برخوردار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اطلس قرمزی ارآل بود طغرایش</p></div>
<div class="m2"><p>شرب بادال نگر مهر برو با خوددار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اطلس یزدی و کاشی و ختائی دیدم</p></div>
<div class="m2"><p>مثل شاه وامیرست و سپاهی دربار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>جامه سرخ نگر بر قد آن سرو ملیح</p></div>
<div class="m2"><p>ای که باور نکنی (فی الشجر الاخضرنار)</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کافرار دامک شلوارزر افشان بیند</p></div>
<div class="m2"><p>جای آنست که دردم بگشاید زنار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>این همه نقش بدیدار در آرایشها</p></div>
<div class="m2"><p>نظر آنکو نکند نقش بود بر دیوار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نه بخود در حرکت آلت آغا پنبه است</p></div>
<div class="m2"><p>در پس پرده یکی هست چو بینی درکار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>رختهایی که تو بینی همه با دوست نکوست</p></div>
<div class="m2"><p>جامها را چو محل گر نبود در بریار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تا جهانست کم از مفرش اصحاب مباد</p></div>
<div class="m2"><p>سی و یک چیز ز افضال خدالیل و نهار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>صوفک و خاصک و تن جامه و بیت و برتنک</p></div>
<div class="m2"><p>گلی و گلفتن و سالوو روسی انصار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ارمک وقطنی و عین البقر و رومی باف</p></div>
<div class="m2"><p>مله میلک ولالائی بی حد و شمار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>صوف سته عشری قبرسی و تفصیله</p></div>
<div class="m2"><p>کستمانی حلبی حبر و غزی بسیار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>قلمی فوطه و کرباس و ندافی و قدک</p></div>
<div class="m2"><p>یقلق و طاقیه و موزه و کفش و دستار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در لباس این سخنان گفت نظام قاری</p></div>
<div class="m2"><p>که او زکرم هم تو بپوش ای ستار</p></div></div>