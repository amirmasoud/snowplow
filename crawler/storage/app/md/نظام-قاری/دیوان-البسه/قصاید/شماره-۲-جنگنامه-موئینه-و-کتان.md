---
title: >-
    شمارهٔ ۲ - جنگنامه موئینه و کتان
---
# شمارهٔ ۲ - جنگنامه موئینه و کتان

<div class="b" id="bn1"><div class="m1"><p>ز پرتو علم خلعت مغرق خور</p></div>
<div class="m2"><p>سحر شد آستی و دامن جهان پرزر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخی کز آبله مانند نقش کمخا بود</p></div>
<div class="m2"><p>نمود اطلس خانبالغی زشوکت و فر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتخت کت چوبر آمد نهالی زر بفت</p></div>
<div class="m2"><p>کلاه وار قبا پیش او ببست کمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فش عمامه در آمد باحتساب رخوت</p></div>
<div class="m2"><p>براند دره بنهی محرمات دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو بصوفی صاحب سماع زردک پوش</p></div>
<div class="m2"><p>که نوکسیت نخواهد خرید کهنه مدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملاف باقلمی ای لباس آژیده</p></div>
<div class="m2"><p>بر وی کار چو افتاد بخیه ات یکسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکازر ار بودت پیرهن ضرورت دان</p></div>
<div class="m2"><p>یکی دگر که بود لازمت زخشک وزتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که عجب سقرلاط سبز و سنجابش</p></div>
<div class="m2"><p>بود بآب و علف گشته مفتخر چون خر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپرد راه دوئی موزه زان بپا افتاد</p></div>
<div class="m2"><p>کلاه زد دم وحدت ازان بود بر سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قوی عجب بود از گندکان اسپاهان</p></div>
<div class="m2"><p>حریر وار چنین نرم زوده در بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو باد بیزن و مسواک داشت حکم علم</p></div>
<div class="m2"><p>بشد سجاده زردک بمرشدی اشهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کشان بپای بت دلرباست دامن شرب</p></div>
<div class="m2"><p>بدانطریق که طاوس میکشد شهپر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون که وقت حصیرست و بوریا بزمین</p></div>
<div class="m2"><p>چه شد که سبزه بزیلو فکندنست سمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گلست و لاله چو والای سرخ و اطلس آل</p></div>
<div class="m2"><p>لباس شاهد باغ و شکوفه اش چادر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشید سرو سهی پادرازتر زگلیم</p></div>
<div class="m2"><p>عبای سبز حنینی ازان شدش در بر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خرده گیری گل دان قبای تنگ شکفت</p></div>
<div class="m2"><p>که بر زمین کشد از حیف دامن پر زر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دال شرب سفیدست و نرمدست بنفش</p></div>
<div class="m2"><p>بیا بنفشه و نرگس بگلستان بنگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نگر بگونه والای زرفشان کبود</p></div>
<div class="m2"><p>چو آسمان که بتابد از و بشب اختر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بجان خشیشی سنجاب ما طلب دارد</p></div>
<div class="m2"><p>یکی که باشدش از گرم و سرد دهر خبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چوشه کلاه دمی گوش باش و ین سخنان</p></div>
<div class="m2"><p>که در حکایت رختست یادگیر از بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مثال جامه بکاغذ سفید نامه شوی</p></div>
<div class="m2"><p>ازین حدیث میان بندشان زشیر و شکر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شنیده توبسی قصه سلحشوران</p></div>
<div class="m2"><p>بحرب دیده دلیران بجبه و مغفر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازین نمط که بود پوستین و رخت بهار</p></div>
<div class="m2"><p>خصومتی بمیانشان که داده است خبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ربود قاقم که باد و بیدمشک صفت</p></div>
<div class="m2"><p>بچوب گیرمت ار پوستین کنی در بر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان میان کتان و حریر گل یاریست</p></div>
<div class="m2"><p>که هیچ موی نکنجد میانشان دیگر</p></div></div>