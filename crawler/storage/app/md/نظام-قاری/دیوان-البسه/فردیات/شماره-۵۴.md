---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>قاری برای جامه تو صوف روز حشر</p></div>
<div class="m2"><p>مانند پشم شده شود کوه با شکوه</p></div></div>