---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>منم که از جهت رنک و بوی البسه ام</p></div>
<div class="m2"><p>چمن برنگرزی شد صبا بعطاری</p></div></div>