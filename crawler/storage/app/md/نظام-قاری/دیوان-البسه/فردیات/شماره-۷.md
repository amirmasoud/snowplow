---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>از قدک تا باطلس چرخی</p></div>
<div class="m2"><p>زآسمان تا بریسمان فرقست</p></div></div>