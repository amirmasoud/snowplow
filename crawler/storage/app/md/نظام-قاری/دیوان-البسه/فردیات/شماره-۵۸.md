---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>برای جبه ما ابر میزند پنبه</p></div>
<div class="m2"><p>برو زقوس قزح بین کمان حلاجی</p></div></div>