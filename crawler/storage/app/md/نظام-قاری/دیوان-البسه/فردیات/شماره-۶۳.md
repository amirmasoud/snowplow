---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>زصندلی تو اگر پابه رابجنبانی</p></div>
<div class="m2"><p>دو صد عمامه سالو بسر بگردانی</p></div></div>