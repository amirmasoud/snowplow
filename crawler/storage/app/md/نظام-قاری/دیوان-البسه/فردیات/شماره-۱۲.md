---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>گر چو کرباس پاره ام بکنی</p></div>
<div class="m2"><p>روی کاسر بچشم من نه خوشست</p></div></div>