---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>پوستین بر روی اطلس ساده این بر موی آن</p></div>
<div class="m2"><p>گوئیا با ترک تاجیکی هم آغوش آمده</p></div></div>