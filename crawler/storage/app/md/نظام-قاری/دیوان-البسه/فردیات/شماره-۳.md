---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>بر در چاک پس چو سر بنهی</p></div>
<div class="m2"><p>(ان هذا اقل ما فی الباب)</p></div></div>