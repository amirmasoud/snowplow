---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>المنه لله که کشیدیم ببر باز</p></div>
<div class="m2"><p>رخت نو و از جامه چرکن برهیدیم</p></div></div>