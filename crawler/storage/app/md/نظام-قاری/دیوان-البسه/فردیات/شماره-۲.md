---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>یقه پهن پوستین سمور</p></div>
<div class="m2"><p>هست ریشی دگر ولی زقفا</p></div></div>