---
title: >-
    بخش ۱۶ - خواب دیدن جامه خواب و تعبیر آن
---
# بخش ۱۶ - خواب دیدن جامه خواب و تعبیر آن

<div class="b" id="bn1"><div class="m1"><p>شبی دید ناگه لحافی به خواب</p></div>
<div class="m2"><p>که از میخ در جامه شد خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خرقه‌ای شد که تعبیر کن</p></div>
<div class="m2"><p>مر این آیه را شرح و تفسیر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتا به این حال ناگفته است</p></div>
<div class="m2"><p>که چون عقد دستار آشفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب گر به هم برنیاید لباس</p></div>
<div class="m2"><p>معارض شود با حریری پلاس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر میخ دیده نباشد چه باک</p></div>
<div class="m2"><p>بری باد از فتنه دامان پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سلطان کمخا تباهی رسد</p></div>
<div class="m2"><p>گزندی به والای شاهی رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجیفی خشیشی بباید کنون</p></div>
<div class="m2"><p>ز بازو چو تعویذ کردن نگون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که تا ایمن از چشم عین البقر</p></div>
<div class="m2"><p>بماند به هر حال دور از خطر</p></div></div>