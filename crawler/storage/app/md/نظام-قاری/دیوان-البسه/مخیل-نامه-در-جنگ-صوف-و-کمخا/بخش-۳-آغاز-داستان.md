---
title: >-
    بخش ۳ - آغاز داستان
---
# بخش ۳ - آغاز داستان

<div class="b" id="bn1"><div class="m1"><p>چنین خواندم از خط ابیارئی</p></div>
<div class="m2"><p>که میخواندی نوبتی عارئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که کمخاهمی کرد تعریف خویش</p></div>
<div class="m2"><p>که بیشم بجاه از قماشات بیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که از چین و ماچین فرازم علم</p></div>
<div class="m2"><p>گهی از خطا و ختن دم زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ابریشم چنگم اسرار بین</p></div>
<div class="m2"><p>در اوتار او از من آثار بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپشتی شاهان منم چارقب</p></div>
<div class="m2"><p>که دارد چنین اعتبار و نسب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه و مهر روی کلاه منست</p></div>
<div class="m2"><p>شفق شقه قدرو جاه منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنقشم خجل گشته ارژنک چین</p></div>
<div class="m2"><p>گلستانم از رنک پر زیب بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواهر بجیبم رسانند باج</p></div>
<div class="m2"><p>زر از کان فرستد بقیقم خراج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در اسرار چنگم شنیدی صدا</p></div>
<div class="m2"><p>که اول کجا بودم اکنون کجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخا نبالغم گاه رایت زنند</p></div>
<div class="m2"><p>سمرقندیم گاه نسبت کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخوتی که بودند ابریشمیین</p></div>
<div class="m2"><p>چه از پنبه و از کتان و کژین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لباسی که از جنس موئینه بود</p></div>
<div class="m2"><p>قماشی که از نوع پشمینه بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زپیچیدنی و ز پوشیدنی</p></div>
<div class="m2"><p>زافکندنی و زگستردنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدادند با یکدیگر این قرار</p></div>
<div class="m2"><p>که نبود سریری چوبی تا جدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زافتادگی و زره قدرو جاه</p></div>
<div class="m2"><p>همه کفش باشیم و او شه کلاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه رختها چون سپاه آمدند</p></div>
<div class="m2"><p>کواکب صفت گرد ماه آمدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی صندلی عاج و زآبنوس</p></div>
<div class="m2"><p>بدش تخت وزرتاج وزردوزکوس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زخرمی و پوشی برش زیج بود</p></div>
<div class="m2"><p>سطرلاب نیز ازنمکدان نمود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که سلطان کمخا نشاندن بتخت</p></div>
<div class="m2"><p>چه روزی نکو باشد از فر بخت</p></div></div>