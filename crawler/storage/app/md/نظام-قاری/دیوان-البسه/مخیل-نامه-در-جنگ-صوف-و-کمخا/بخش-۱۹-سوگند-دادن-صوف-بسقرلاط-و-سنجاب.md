---
title: >-
    بخش ۱۹ - سوگند دادن صوف بسقرلاط و سنجاب
---
# بخش ۱۹ - سوگند دادن صوف بسقرلاط و سنجاب

<div class="b" id="bn1"><div class="m1"><p>سقرلاط و سنجاب را خواندند</p></div>
<div class="m2"><p>چو تسمه بر ایشان سخن راندند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که باید شما را کنون عهد کرد</p></div>
<div class="m2"><p>از اندازه بیرون قسم نیز خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه این رو بگرداند از هیچ رو</p></div>
<div class="m2"><p>نه او هم دهد پشت از هیچ سو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبانرا بدندان درهای گوی</p></div>
<div class="m2"><p>گزیدند که بی روئی از ما مجوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتشریف منبر ببرد یمن</p></div>
<div class="m2"><p>بآن خرقه کآمد بویس قرن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخرگاه والا و فرهنگ بخت</p></div>
<div class="m2"><p>زاسباب بروی زهرگونه رخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتعظیم خیمه که از احترام</p></div>
<div class="m2"><p>عمودش بخدمت نموده قیام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بقدر سراپرده و کندلان</p></div>
<div class="m2"><p>چه از شامیانه چه از سایبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برخت مغرق خجل کرده ورد</p></div>
<div class="m2"><p>زمهر و سپهرش زرو لاجورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جواهر زهر نوع و زرینها</p></div>
<div class="m2"><p>لآلی زهر جنس سیمینها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزین مرصع که خورشید را</p></div>
<div class="m2"><p>بودرشک بروی ززیب و بها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ببال پرو گوشهای صدف</p></div>
<div class="m2"><p>برین مستمع گشته از هر طرف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببستان سجاده پرنیاز</p></div>
<div class="m2"><p>که مسواک دروی بود سروناز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که هرگز نگردیم از رای تو</p></div>
<div class="m2"><p>نه پیچیم از حکم والای تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخود گر بگیریم ازین حرب تن</p></div>
<div class="m2"><p>میان توی بادا بتنمان کفن</p></div></div>