---
title: >-
    بخش ۲۰ - آهنگ نمودن صوف به پیکار کمخا
---
# بخش ۲۰ - آهنگ نمودن صوف به پیکار کمخا

<div class="b" id="bn1"><div class="m1"><p>پس انگه مقرر شد ازداوری</p></div>
<div class="m2"><p>بر افراد این جامه لشکری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از جنس موئینه و آستر</p></div>
<div class="m2"><p>بود زیرشان اسبها سر بسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازین رختهائی که ما را بزیر</p></div>
<div class="m2"><p>بدندی شوند این زمان بارگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگیرند ازینجمله با خویشتن</p></div>
<div class="m2"><p>دو توئی و یکتائی و پیرهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکلتو چنین گفت باجل براه</p></div>
<div class="m2"><p>که آمد کنون نوبت پایگاه</p></div></div>