---
title: >-
    بخش ۱۷ - در لشکر آراستن صوف
---
# بخش ۱۷ - در لشکر آراستن صوف

<div class="b" id="bn1"><div class="m1"><p>وزان روی صوف از پی کارزار</p></div>
<div class="m2"><p>شدش جمع پشمینه بیشمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسان فراویز بر دامنش</p></div>
<div class="m2"><p>برآمد زهر سوی پیرامنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو طاقین که از جامها اوست طاق</p></div>
<div class="m2"><p>چو سته عشر نامدار عراق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زانکوره کردند یاور طلب</p></div>
<div class="m2"><p>بیامد مدد نیزشان از حلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زد میزرینی و هم زاغکی</p></div>
<div class="m2"><p>دگر بید بازاری و شالکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سقرلاط و بزمات و آن بنات</p></div>
<div class="m2"><p>چو ماشاک و تفتیک و عین ثبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمدهای باران چه جای چه بور</p></div>
<div class="m2"><p>که مالش بسی آزمودند و زور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زجرجانیان انجمن تیره گشت</p></div>
<div class="m2"><p>زتر بینیان عالمی خیره گشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زره گشت ناگاه گردی بدید</p></div>
<div class="m2"><p>بگفتند زیلو بلشکر رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهر جنس و هر جای با جهرمی</p></div>
<div class="m2"><p>تو گوئی گرفتند روی زمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بپشتی بیامد زهر سوکول</p></div>
<div class="m2"><p>به پیکار سرما نموده جدل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلشکر گهش پوستینها همه</p></div>
<div class="m2"><p>بیامد چو پیش شبانان رمه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو سنجاب و قاقم سموروفنک</p></div>
<div class="m2"><p>دله صدرو روباه و ابلق ادک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تعلق بدین داشت هر چیر گرم</p></div>
<div class="m2"><p>باو بود وابسته هر جنس نرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بارانی و پیش بند و جقه</p></div>
<div class="m2"><p>دگر چکمه سرفراز از یقه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جبه چه قبا پوستین و سلیم</p></div>
<div class="m2"><p>دگر ینمچه باحنین و سلیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باین جمله تشریف گفت ای گروه</p></div>
<div class="m2"><p>شما میشوید از معارض ستوه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که در زیر هر جبه پنهان شوید</p></div>
<div class="m2"><p>ببالای پوشی گریزان شوید</p></div></div>