---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>بنام خطا پوش آمرزگار</p></div>
<div class="m2"><p>که ستار عیبست بر جرم کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکنده قبا کحلی آسمان</p></div>
<div class="m2"><p>زفضلش ببر خلعت زرفشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکوه از کرم رخت خارا دهد</p></div>
<div class="m2"><p>پر از موج خبری بدریا دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی را کند صوف و اطلس لباس</p></div>
<div class="m2"><p>یکی را دهد پوستک باپلاس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرآنست تشریف احسان اوست</p></div>
<div class="m2"><p>وراینست بدرخت و عریان اوست</p></div></div>