---
title: >-
    بخش ۲۲ - رزم صوف و کمخا
---
# بخش ۲۲ - رزم صوف و کمخا

<div class="b" id="bn1"><div class="m1"><p>یکی دیده بان از علم بر منار</p></div>
<div class="m2"><p>سپاهی آن لشکر بیشمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدید و بدین سر خبر باز داد</p></div>
<div class="m2"><p>که آن رختها آمد اینک چو باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلایه زرخت طلادوز بود</p></div>
<div class="m2"><p>چو مهر فلک عالم افروز بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندیدند القصه آسایشی</p></div>
<div class="m2"><p>رسیدند با هم در آرایشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارخته چو برداشت رخت و بنه</p></div>
<div class="m2"><p>بدش ز آستین میسره میمنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلا دوز کرد آن سیاهی نگاه</p></div>
<div class="m2"><p>بگفت این زر سرخ و روی سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دستار بافش فرو هل نمود</p></div>
<div class="m2"><p>زدرزو زگو جامه گودرز بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگر کیسه میخ حمل لباس</p></div>
<div class="m2"><p>بتحقیق روئین تن او را شناس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان بندها را علم ساختند</p></div>
<div class="m2"><p>بحرب ملابس بر افراختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زسرهای دستارچه بد درفش</p></div>
<div class="m2"><p>همه سرخ و زرد و کبود و بنفش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی بوددستار بر صندلی</p></div>
<div class="m2"><p>ابا تاج بر قلبگاه ملی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که صف را چو آئین بیاراستند</p></div>
<div class="m2"><p>سلحها سراسر به پیراستند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبس گرد پنبه که ازجبه خاست</p></div>
<div class="m2"><p>یکی روی را آستر شد دور است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرو رفت و بر رفت در آن نبرد</p></div>
<div class="m2"><p>بهر جبه سوزن زهر خرقه گرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چپرهابد از خرقه پوستین</p></div>
<div class="m2"><p>سپرهایشان از الرجاق زین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برآورد دستار گرزی گران</p></div>
<div class="m2"><p>فرو کوفت برترک توبی روان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برآهیخت گرزی کدینه برخت</p></div>
<div class="m2"><p>بزد برقدک تا که شد لخت لخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زحرب و زضرب آن ملاکم نشد</p></div>
<div class="m2"><p>نمد زینشان خشک یکدم نشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زنیهای جولاهگان نیزه بود</p></div>
<div class="m2"><p>کتکهای قصار همچون عمود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خیاط آنچنان ناوکی در سپوخت</p></div>
<div class="m2"><p>که ده روی از جامه درهم بدوخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو دو لشکر برد درهم زدند</p></div>
<div class="m2"><p>برو آستر را بیکدم زدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کشیده بت و شال و خفری رده</p></div>
<div class="m2"><p>ملای مله جمله برهم زده</p></div></div>