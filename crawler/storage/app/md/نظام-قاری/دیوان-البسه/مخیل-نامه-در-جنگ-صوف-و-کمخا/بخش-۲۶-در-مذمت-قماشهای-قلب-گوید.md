---
title: >-
    بخش ۲۶ - در مذمت قماشهای قلب گوید
---
# بخش ۲۶ - در مذمت قماشهای قلب گوید

<div class="b" id="bn1"><div class="m1"><p>قماشی که از تل بود روی آن</p></div>
<div class="m2"><p>گرش روی دیگر کنی پرنیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خشیشی وصوف ارسجیفش کنی</p></div>
<div class="m2"><p>و یا دگمه در بجیبش زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزودی بدرد همه روی وار</p></div>
<div class="m2"><p>بماند از و آستر یادگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگی بعریان طمع داشتن</p></div>
<div class="m2"><p>بود شال را زوده پنداشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو قاقم بکامو مدارید امید</p></div>
<div class="m2"><p>که چرکن چوشد می نگردد سفید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سربند شلوار افراشتن</p></div>
<div class="m2"><p>وزو چشم بند سلق داشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سررشته خویش گم کردنست</p></div>
<div class="m2"><p>بجیب اندرون مار پروردنست</p></div></div>