---
title: >-
    بخش ۸ - در ایلچی فرستادن کمخا و باج از صوف و سقرلاط طلب کردن
---
# بخش ۸ - در ایلچی فرستادن کمخا و باج از صوف و سقرلاط طلب کردن

<div class="b" id="bn1"><div class="m1"><p>چنین گفت ابیاری خسروی</p></div>
<div class="m2"><p>که بشنو سخن چون تو شاه نوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسرپوش گفتند چیزی براز</p></div>
<div class="m2"><p>نباید کشیدن چو میرز دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بباید فرستادن ایلچی برو</p></div>
<div class="m2"><p>نباید نهاد این حدیثش برو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلب کردن از صوف و ارمک خراج</p></div>
<div class="m2"><p>گرفتن بضرب از سقرلاط باج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری گر برآید رجیب خلاف</p></div>
<div class="m2"><p>توهم دزد و دشمن بکین برشکاف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسند آمدش اینسخن زود و گفت</p></div>
<div class="m2"><p>که با جبه اش خرمی باد جفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قبائی بایلچیگری خواستند</p></div>
<div class="m2"><p>بنوروزی و چمته آراستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرستاده شد بهر تحصیل مال</p></div>
<div class="m2"><p>بنزدیک صوف از برای منال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی میشد آهسته ایلچی براه</p></div>
<div class="m2"><p>بدو کرد مدفون یزدی نگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا گفت نی چست تر میدوی</p></div>
<div class="m2"><p>ببالای حجله مگر میروی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زقبرس بسوی خطاروی کرد</p></div>
<div class="m2"><p>بدش ابلقی پوستین ره بورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمان ایلچی رخت اینجا براه</p></div>
<div class="m2"><p>شنو قصه صوف و آن بارگاه</p></div></div>