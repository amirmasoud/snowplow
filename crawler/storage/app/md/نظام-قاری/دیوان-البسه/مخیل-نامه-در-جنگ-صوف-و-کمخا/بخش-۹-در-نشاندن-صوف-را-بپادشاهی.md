---
title: >-
    بخش ۹ - در نشاندن صوف را بپادشاهی
---
# بخش ۹ - در نشاندن صوف را بپادشاهی

<div class="b" id="bn1"><div class="m1"><p>بشاهی بشد صوف بر صندلی</p></div>
<div class="m2"><p>نشاندند بر تختگاه ملی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآمد بگردش همه جامها</p></div>
<div class="m2"><p>بهر جا نوشت از بمی نامها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ماننده دگمه در کرد جیب</p></div>
<div class="m2"><p>برآیند و باشند عاری زعیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخوت زمستان فراوان قماش</p></div>
<div class="m2"><p>که بددر بر مردمان جمله فاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شنیدند احکام والای او</p></div>
<div class="m2"><p>ندیدند جز رای اعلای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکر جنسهایی که بود از قصب</p></div>
<div class="m2"><p>کزا بر یشمین داشتندی نسب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتابیده رو را زفرمان او</p></div>
<div class="m2"><p>نبودند قطعا به پیمان او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتند دیگر برسم سجیف</p></div>
<div class="m2"><p>بقیقاج نیزش بگرد لحیف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخواهیم و نکنیم ازین پس رها</p></div>
<div class="m2"><p>که پشمان به بیند بیکره بما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان بند گفتا دو سرمان مگر</p></div>
<div class="m2"><p>بود تاز کمخا به پیچیم سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیاویزم آنگه بدامان صوف</p></div>
<div class="m2"><p>عقود سپیچم نخوانند یوف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آن بارگه گفت پک پیش شاخ</p></div>
<div class="m2"><p>میانهای دندانش از گو فراخ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نمانند الباغ کز آنمیان</p></div>
<div class="m2"><p>بهر حالیش هست بند زبان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که تیزی بازار این فتنه جو</p></div>
<div class="m2"><p>نه بستست چون بقچه بندی برو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که اینان بدینسان دو شلواریند</p></div>
<div class="m2"><p>گرفتار قلبی و طراریند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگرانکه تارخت اطلس زرخت</p></div>
<div class="m2"><p>بسر برکرا مینهد تاج و تخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه زینت تا جداریش راست</p></div>
<div class="m2"><p>بشدانچه ازرخت و اسباب خواست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زپشمینه شلوار میخواست یام</p></div>
<div class="m2"><p>رساندن بکمخا پیام و سلام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که در منبر جمله ام خطبه خوان</p></div>
<div class="m2"><p>زوالا بزن زربنامم روان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که ایلچی کمخا درآمد بدم</p></div>
<div class="m2"><p>همه خرمیها بدل شدم بغم</p></div></div>