---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>از رخوتم عاریت کردی طلب</p></div>
<div class="m2"><p>(چون برم از پیش یاری آمدی)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فراش خانه هیچم کم نبود</p></div>
<div class="m2"><p>(گر بمن خرم نکاری آمدی)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامه بودی مرا از صوف نیز</p></div>
<div class="m2"><p>(چونکه عیدی یا بهاری آمدی)</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانده ز آنها جامه خواب یک بر آب؟</p></div>
<div class="m2"><p>(هم نماندی گر بکاری آمدی)</p></div></div>