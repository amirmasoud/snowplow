---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>شنیده ام که بدستار گیوه میگفت</p></div>
<div class="m2"><p>(تو آفتاب بلندی و من چنین پستم)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجامه متکلف برهنه هم گفت</p></div>
<div class="m2"><p>(بدامنت زفقیری نمیرسد دستم)</p></div></div>