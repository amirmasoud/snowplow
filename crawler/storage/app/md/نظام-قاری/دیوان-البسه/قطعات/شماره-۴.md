---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>در مدحت بخیه سقرلاط</p></div>
<div class="m2"><p>(لاف از سخنی چو در توان زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن بنمد چو وصله دوزی</p></div>
<div class="m2"><p>(آن خشت بود که پر توان زد)</p></div></div>