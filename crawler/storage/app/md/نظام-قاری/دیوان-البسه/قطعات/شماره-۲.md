---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>میان شده و معجر خصومتی افتاد</p></div>
<div class="m2"><p>چنانکه پوشی و دستار را مقالانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیم شده برک بر علم نوشت این بیت</p></div>
<div class="m2"><p>که بر دقایق معنیش بس دلالاتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>(گلیم بخت کسی را که بافتند سیاه)</p></div>
<div class="m2"><p>(سفید کردن نوعی از محالاتست)</p></div></div>