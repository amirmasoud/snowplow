---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دو قماشند صوف و موئینه</p></div>
<div class="m2"><p>(یکی آرام جان یکی دلبند)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این یکی برزبر عدیم المثل</p></div>
<div class="m2"><p>وان یکی بهر زیر بی مانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فی المثل در میان این دو قماش</p></div>
<div class="m2"><p>(نیست فرقی مگر بموئی چند)</p></div></div>