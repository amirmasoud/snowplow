---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>زیاری طمع داشتم ارمکی</p></div>
<div class="m2"><p>بسوغات خاصی رسید از سفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان دامن همت افشاندم</p></div>
<div class="m2"><p>که تشریف اونامدم در نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از چند که جامه هدیه ام</p></div>
<div class="m2"><p>فرستاد یک حق گذار دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدیدم دروتا خود آن جنس چیست</p></div>
<div class="m2"><p>قدک بود رو و آستر کاستر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(بهر حال مربنده را شکر به)</p></div>
<div class="m2"><p>( که بسیار بد باشد از بد بتر)</p></div></div>