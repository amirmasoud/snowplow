---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چنین که دختر فکرم جهیز معنی یافت</p></div>
<div class="m2"><p>سزد که حجله رخت از برای او باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه ز جامه رنگین وعظ میگویم</p></div>
<div class="m2"><p>( که هر کجا که عروسیست رنک و بو باشد)</p></div></div>