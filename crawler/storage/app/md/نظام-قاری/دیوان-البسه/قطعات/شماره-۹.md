---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>گذشت موسم سرما و پوستین و نمد</p></div>
<div class="m2"><p>فکندم از خود و در بر دگر کتان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دید وصل کتان عضو گفت مشتاقم</p></div>
<div class="m2"><p>(عجب عجب که ترا یاد دوستان آمد)</p></div></div>