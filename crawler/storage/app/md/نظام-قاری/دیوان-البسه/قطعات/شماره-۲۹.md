---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>غرض زین طرز تشریف قبولیست</p></div>
<div class="m2"><p>که پوشاند بما اهل صفائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر الباغ بخشی چون بخواند</p></div>
<div class="m2"><p>بعریانی دهد جامه بهائی</p></div></div>