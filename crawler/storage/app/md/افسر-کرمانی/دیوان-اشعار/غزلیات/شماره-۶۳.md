---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>بی زلف تو، چون قرار ما نیست،</p></div>
<div class="m2"><p>جز شام به روزگار ما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زلف تو، در رخت، فروغی،</p></div>
<div class="m2"><p>با بخت سیاه، کار ما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن پروری و خودی ستودن،</p></div>
<div class="m2"><p>در عشق بتان، شعار ما نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب نیست، که آفتاب تابان،</p></div>
<div class="m2"><p>همخوابه زلف یار ما نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل پیش تو و عنان شوقش،</p></div>
<div class="m2"><p>دانی که به اختیار ما نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بردم دل و جان، نثار رویش</p></div>
<div class="m2"><p>گفتا: دل و جان نثار ما نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک صید ضعیف تر ز افسر،</p></div>
<div class="m2"><p>در حلقه شهسوار ما نیست</p></div></div>