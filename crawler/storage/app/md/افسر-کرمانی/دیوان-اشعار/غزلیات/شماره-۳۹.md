---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>ما را به سراپرده گل رفت اشارت</p></div>
<div class="m2"><p>برقع ز رخ افکندش، ای دیده بشارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام دهن از بهر چه شیرین نکنم من؟</p></div>
<div class="m2"><p>شکر دهن ار می دهدم زهر مرارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من خاک شوم در طلب و او ننهد پای</p></div>
<div class="m2"><p>اوج عظمت بنگر و پستی حقارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم تو برد عقل مرا و این نه شگفت است</p></div>
<div class="m2"><p>ترک است و برد خانه تاجیک به غارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کشتن من بهر چه آن شوخ برآشفت،</p></div>
<div class="m2"><p>جان دادن و نالیدن اگر نیست جسارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در منظره دیده اگر جای نسازد</p></div>
<div class="m2"><p>عاقل نکند در ره سیلاب عمارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمایه جان در طلب وصل تو دادن،</p></div>
<div class="m2"><p>سودی است که در وی نبود هیچ خسارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنهار بهر کس منما آیینه رخ</p></div>
<div class="m2"><p>کاه دل ما سوی تو آید به سفارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افسر، دلی از غبغب تو سوخته دارد</p></div>
<div class="m2"><p>افزاید عجب دیدن کافور حرارت</p></div></div>