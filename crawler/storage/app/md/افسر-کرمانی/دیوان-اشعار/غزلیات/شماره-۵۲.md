---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>ما را طمع ز ساقی مجلس شراب توست</p></div>
<div class="m2"><p>و از مطرب آرزوی سرود رباب توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم فتاده همچو سکندر ز تشنگی،</p></div>
<div class="m2"><p>در ظلمتی که چشمه حیوانش آب توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معمور مسکنی است، که ویران پذیر نیست</p></div>
<div class="m2"><p>آن خانه دلی که به طغیان خراب توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من، مرغ آشیان کمالم، ولی چه سود</p></div>
<div class="m2"><p>بال و پرم شکسته سنگ عتاب توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ شب است، چشم جهان بین آفتاب</p></div>
<div class="m2"><p>آنجا که عکس پرتوی از آفتاب توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلدار را ز افسر بی دل خبر دهید،</p></div>
<div class="m2"><p>کان رند مست عاشق بی‌خورد و خواب توست</p></div></div>