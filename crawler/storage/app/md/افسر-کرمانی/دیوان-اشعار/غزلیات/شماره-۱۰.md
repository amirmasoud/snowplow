---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>بر فرق کوه اگر بزنم برق آه را</p></div>
<div class="m2"><p>سوزد چنانکه آتش سوزنده کاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر اشک و آه من به غلط طعنه ها مزن</p></div>
<div class="m2"><p>بنگر چها اثر بود این اشک و آه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این اشک دجله ای است که ویران کند جهان</p></div>
<div class="m2"><p>و این آه شعله است که سوزد گیاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با سنگدل بتی سر و کارم فتاده است</p></div>
<div class="m2"><p>کو از ثواب فرق نیارد گناه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای زنخ بداده به پیرامن عذار</p></div>
<div class="m2"><p>ز افسونگری به ماه رسانیده جاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای کاش همچو دامنش افتادمی به پای</p></div>
<div class="m2"><p>تا بوسه اش همی زدمی خاک راه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افسر بنال خوش که کسی منع عاشقان</p></div>
<div class="m2"><p>نتوان نمود، ناله بیگاه و گاه را</p></div></div>