---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>جان است اگر گرامی و عمر است اگر عزیز،</p></div>
<div class="m2"><p>بادا نثار روی خوش و سیرت تو نیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح است و باغ پرگل و بلبل ترانه سنج</p></div>
<div class="m2"><p>تا بوستان بهشت کنی ای نگار خیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستان نه بلکه ساحت کیهان معطر است</p></div>
<div class="m2"><p>تا داده ای به باد تو آن زلف عطر بیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا من برقصم از غم شادی فزای دوست،</p></div>
<div class="m2"><p>مطرب تو نغمه سرکن و ساقی تو باده ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر آتشم زنی چو نی اندر به بند بند،</p></div>
<div class="m2"><p>عاشق نباشد آن که ز نار آورد گریز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جان هزار بانگ برآید که مرحبا</p></div>
<div class="m2"><p>گر بند بند من بشکافی به تیغ تیز</p></div></div>