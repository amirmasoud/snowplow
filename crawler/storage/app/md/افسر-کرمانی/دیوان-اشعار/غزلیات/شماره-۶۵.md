---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>تا شدم شیفته زلف تو آرامم نیست</p></div>
<div class="m2"><p>همچو آغاز غمت شادی انجامم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم آن گونه به دام تو هوس کرد که هیچ،</p></div>
<div class="m2"><p>حلقه ای خوبتر از حلقه اندامم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که در آتش سودای غمت پخته شدم،</p></div>
<div class="m2"><p>هیچ در بوسه آن لب طمع خامم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوس خلوت خاصت، به چه امید و چه حدّ</p></div>
<div class="m2"><p>من که ره در گذر بارگه عامم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دو زلفت، که شبی روز نکردم هرگز</p></div>
<div class="m2"><p>که نه امروز از آن زلف دوتا شامم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله ام زار و دلم کاسه خون است، دگر</p></div>
<div class="m2"><p>هوس مطرب و ساقی، طلب جامم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر مژگان تو، کام دل هر ناکام است</p></div>
<div class="m2"><p>حیف، کان تیر نصیب دل ناکامم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با صبا، شرح پریشانی خود گویم از آنک،</p></div>
<div class="m2"><p>سوی آن نافه گشا، زهره پیغامم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست توفیق چو بگرفت عنانم، افسر</p></div>
<div class="m2"><p>تو مپندار که یکران سخن رامم نیست</p></div></div>