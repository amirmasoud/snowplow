---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>بنازم خاک اقلیم سلیمان را که هر مورش،</p></div>
<div class="m2"><p>بیارد تخت بلقیسی اگر سازند مأمورش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیثی را که عقل از نقل آن دیوانه شد یارب،</p></div>
<div class="m2"><p>بدارم تا کی اندر تنگنای سینه مستورش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم شاهد ما را چه شهد آمد به لب پنهان،</p></div>
<div class="m2"><p>که می جویند خلقی نوش جان از نیش زنبورش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بهرام گوری عاقبت گورت کشد در بر</p></div>
<div class="m2"><p>زمن گر باورت نبود، چه شد بهرام و کو گورش؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فزاید تیرگی درچشم عاشق بی رخ جانان</p></div>
<div class="m2"><p>برافروزند اگر در محفل جان مشعل طورش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکورویی که نقد جان شهانش دربها داده،</p></div>
<div class="m2"><p>چسان بی زور و زر در خانه آرد عاشق عورش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبیند آفتاب روی جانان چشم کوته‌بین</p></div>
<div class="m2"><p>که عاجز باشد از دیدار چشم مرغ شب کورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شمع عارض او هرکه نزدیک است می‌سوزد</p></div>
<div class="m2"><p>همان بهتر که افسر گاه‌گاهی بیند از دورش</p></div></div>