---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>شود آیا که ز ما حرف وفا گوش کنی</p></div>
<div class="m2"><p>مهربان باشی و بیداد فراموش کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتو روی تو بگرفت جهان، پرده بهل</p></div>
<div class="m2"><p>مگر این آتش پر مشعله خاموش کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی خود از باده دوش استی و ما هشیاران</p></div>
<div class="m2"><p>مست سهل است، که دیوانه و مدهوش کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه دل بود، ربودی و کشیدی در زلف</p></div>
<div class="m2"><p>این گران سلسله را بهر چه بر دوش کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوسم زندگی دیگر، و عمر دگر است</p></div>
<div class="m2"><p>شود این هر دو گرم دست در آغوش کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده زآن شیشه که با غیر خوری شرمت باد</p></div>
<div class="m2"><p>خون حسرت زدگان است چرا نوش کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده طلعت او، خواهی اگر بود، افسر</p></div>
<div class="m2"><p>شرطش آن است که از خویش فراموش کنی</p></div></div>