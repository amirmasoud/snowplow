---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>زنده آن را نتوان گفت که جانی دارد</p></div>
<div class="m2"><p>ای خوش آن دل، که چو ما جان جهانی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج ها می طلبد دوست ز ویرانه دل</p></div>
<div class="m2"><p>به من راه نشین طرفه گمانی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سود ما مردن در عشق و زبان، هستی ماست</p></div>
<div class="m2"><p>عاشقی بین، که چه خوش سود و زیانی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چو پروانه دل ماست، غزل خوان از چیست؟</p></div>
<div class="m2"><p>ور بود شمع چرا سوز نهانی دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکته ها هست بهر زمزمه در هر شاخی</p></div>
<div class="m2"><p>نبود بلبل مست آن که فغانی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت جانان و برفت از تن ما تاب و توان</p></div>
<div class="m2"><p>جان ما بین که عجب تاب و توانی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخ می گوید و شیرینیم از حد ببرد</p></div>
<div class="m2"><p>طرفه شکر لب ما، نوش دهانی دارد</p></div></div>