---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>آن کو به تو بخشید چنین لطف و صباحت</p></div>
<div class="m2"><p>آموخت مرا شاعری و رسم فصاحت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مردم چشمم صدف در تو بیند</p></div>
<div class="m2"><p>چون‌ آدم آبی شده در غوص و سباحت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم چشمه خضری تو و هم جام سکندر</p></div>
<div class="m2"><p>هم معدن حسنی تو هم کان ملاحت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم مایه جادویی و هم سایه اعجاز</p></div>
<div class="m2"><p>هم آفت آرامی و هم فتنه راحت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم داروی دردی تو و هم محنت جان ها</p></div>
<div class="m2"><p>هم مرهم زخمی تو و هم داغ جراحت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسیار دل ما هوس بوس تو را کرد</p></div>
<div class="m2"><p>نشنید جواب از لب لعلت به صراحت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با روی تو، کان جلوه ده صبح مصفاست</p></div>
<div class="m2"><p>خورشید برون آمد و نشناخت قباحت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسر، مکش از رنج طلب پای به دامن</p></div>
<div class="m2"><p>کاین خسرو ما، صاحب جود است و سماحت</p></div></div>