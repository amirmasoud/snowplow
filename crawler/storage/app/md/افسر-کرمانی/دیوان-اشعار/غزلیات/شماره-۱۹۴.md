---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>هرجا که تو بنشینی، صد فتنه برانگیزی</p></div>
<div class="m2"><p>هر فتنه شود هفتاد، هر لحظه که برخیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام دل هر عاقل، از لعل شکر خایی</p></div>
<div class="m2"><p>دام ره هر دانا، با زلف دلاویزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جلوه رخ بیضا، در هاله تو می پوشی</p></div>
<div class="m2"><p>وز سبزه خط عنبر، بر لاله بیاویزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر فرق سر خورشید، خاک سیه از غیرت</p></div>
<div class="m2"><p>زآن کاکل مشک افشان، ای ما تو می بیزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زلف عبیر آسا، در هاله قمر پوشی</p></div>
<div class="m2"><p>وز طلعت چون بیضا، در لاله گهرریزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لعل روان پرور، با معجز عیسایی</p></div>
<div class="m2"><p>وز چشم ستم گستر، با فتنه چنگیزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کوه غم عشقت، خوبان همه فرهادند</p></div>
<div class="m2"><p>صد حیف که چون شیرین، هم صحبت پرویزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خانه خود ای دوست، من رفتم و او آمد</p></div>
<div class="m2"><p>وقت است فریدون وار، با حادثه بستیزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای آن که دل ما شد، دیوانه زنجیرت</p></div>
<div class="m2"><p>گیرم که پری زادی، از ما ز چه بگریزی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای آن که تو را باشد، رخساره چو آیینه</p></div>
<div class="m2"><p>از آه دل افسر، تا چند نپرهیزی؟</p></div></div>