---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>هیچ می‌دانی مرا در بوته دل تاب نیست؟</p></div>
<div class="m2"><p>کمتر آتش زن که جانم، کمتر از سیماب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرشب از هجر تو می میریم و در بالین خاک،</p></div>
<div class="m2"><p>مردگان را خود نشاید کس بگوید خواب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم چشمم شود منزلگه عکس رخت،</p></div>
<div class="m2"><p>تا نگویی خانه کس در ره سیلاب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نباشد زلف و رویت، کفر و ایمان، گو، چرا</p></div>
<div class="m2"><p>فارغ از این ماجرا یک لحظه شیخ و شاب نیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون اگر گریم، مکن عیبم، که بی لعل لبت،</p></div>
<div class="m2"><p>بسکه کردم گریه در دریای چشمم آب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا معنبر زلف را آشفته کردی از نسیم،</p></div>
<div class="m2"><p>نیست، کان آشفتگی،‌ در خاطر احباب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو آن رخ در گلستان، هیچ نبود ارغوان،</p></div>
<div class="m2"><p>همچو آن لب، در بدخشان، هیچ لعل ناب نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو نظم افسر و آن گوهرین دندان دوست</p></div>
<div class="m2"><p>خوب سنجیدیم، آری لؤلؤ خوشاب نیست</p></div></div>