---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>گشتیم همه عالم و جای دگری نیست</p></div>
<div class="m2"><p>دیدیم و ز سودای غمت خوبتری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شیفته سامان گذرد خاطرم ای دوست،</p></div>
<div class="m2"><p>از زلف تو در خاطرم آشفته تری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرتاسر نخجیرگه عشق مپندار،</p></div>
<div class="m2"><p>کز ناوک مژگان تو خونین جگری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانانه وبل جان منی زآن رخ و زآن لب</p></div>
<div class="m2"><p>پنهان ز چه دارم، که جز اینم هنری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارند به گلزار تو مرغان خوش آواز</p></div>
<div class="m2"><p>پرواز، و دریغا که مرا بال و پری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدار رخت نیست مگر کار سکندر</p></div>
<div class="m2"><p>کایینه گری شیوه هر بی بصری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در راه وفاداری آن سخت دل، افسر</p></div>
<div class="m2"><p>جز خضر محبت دگرم راهبری نیست</p></div></div>