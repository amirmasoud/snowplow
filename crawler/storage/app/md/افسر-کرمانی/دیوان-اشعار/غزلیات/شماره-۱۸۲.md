---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>بهار آمد و در باغ، لاله شد چو پیاله</p></div>
<div class="m2"><p>خوش است در کف ساقی، پیاله غیرت لاله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو نیز می به قدح ریز و خوی به لاله عارض</p></div>
<div class="m2"><p>کنون که ابر به رخسار لاله، ریخته ژاله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروخت گل، رخ و افروخت، سرو قامت موزون</p></div>
<div class="m2"><p>هزار زمزمه سنج آمد و تذرو به ناله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو نیز سرو برافراز و باغ چهره برافروز</p></div>
<div class="m2"><p>که صد هزار تذروت شوند عاشق و واله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنم ز اشک پیاپی دُرو عقیق تصدق</p></div>
<div class="m2"><p>مبادا آن که برآید به گرد ماه تو هاله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحیفه رخ دلدار را ببین و چو افسر،</p></div>
<div class="m2"><p>به رغم واعظ ناصح بسوز جمله رساله</p></div></div>