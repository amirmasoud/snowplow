---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>ای که بر ما گذری با همه کبر و غرور</p></div>
<div class="m2"><p>غم ما شیفتگان آوردت عیش و سرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این همه گام که از کبر گذاری به زمین</p></div>
<div class="m2"><p>هیچ دانی که بود دیده ما فرش عبور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه غم ار ز آن که کند، بر تو نظر کوته بین</p></div>
<div class="m2"><p>دیده مرغ شب از دیدن مهر آمده کور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست وقتی که بخواهم غم عشقت دم مرگ،</p></div>
<div class="m2"><p>چیست روزی که ببینم مه رویت شب گور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم و شادی همه یکسان بنماید در عشق</p></div>
<div class="m2"><p>خود تفاوت نبود با غم دل ماتم و سور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسرا، گر ننهد پا بسرت دوست، مرنج</p></div>
<div class="m2"><p>که سلیمان ننهد تاج شهی بر سر مور</p></div></div>