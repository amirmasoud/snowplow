---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>ای که در فکر مهندس، دهنت سرّ محال</p></div>
<div class="m2"><p>کمرت نیز در اندیشه ما دیگر حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو با ماه رخ و زلف دراز آمده ای</p></div>
<div class="m2"><p>عاشقان راست شب و روز نظر بر مه و سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شده از حسرت صهبای عقیق لب تو،</p></div>
<div class="m2"><p>ساغر دیده ام از خون جگر مالامال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر پریشان نبود زلف تو چون خاطر من</p></div>
<div class="m2"><p>از چه آشفته شود هر نفس از باد شمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوخت گر بال و پر مرغ دلم برق غمت،</p></div>
<div class="m2"><p>در گلستان تو پرواز کند بی پر و بال</p></div></div>