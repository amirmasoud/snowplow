---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>ای، دل ما غرق خون از نوک تیرت</p></div>
<div class="m2"><p>غرق خون مپسند، صید دستگیرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کمان ابرو، بنازم شست و دستت</p></div>
<div class="m2"><p>صید بتوان کرد، هر جا رفت تیرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صفت نخجیر، از پیکان مژگان</p></div>
<div class="m2"><p>همچو آهو، عاجز آید شرزه شیرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در صفت ما عاشقان، جز دل نجوید</p></div>
<div class="m2"><p>تیر مژگان، در کمان دلپذیرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ بر ما آختی، صد حیف کامد،</p></div>
<div class="m2"><p>طعمه شیر، جان های حقیرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شب غم جان سپارم، زود زودت</p></div>
<div class="m2"><p>روز عیش، ای آنکه بینم دیر دیرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افسرا، در ورطه غم در نمانی</p></div>
<div class="m2"><p>الفت دلدار اگر شد دستگیرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای که خون عاشقان چون می به جامت</p></div>
<div class="m2"><p>از چه آمد خون حلال و می حرامت؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خرامان سرو باغ دلربایی</p></div>
<div class="m2"><p>با قیامت، بوده آشوب قیامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مسیحا مردگان را مژده جان،</p></div>
<div class="m2"><p>باد شبگیری که می آرد پیامت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز عیش همدمت را شام نبود</p></div>
<div class="m2"><p>آفتابی در جبین، ای مه غلامت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با نقاب زلف، رخ پوشی و باشد،</p></div>
<div class="m2"><p>شام یلدا، روزگار خاص و عامت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو نخجیر کمند خوبرویان</p></div>
<div class="m2"><p>افسرا، بر دست و پا سخت است دامت</p></div></div>