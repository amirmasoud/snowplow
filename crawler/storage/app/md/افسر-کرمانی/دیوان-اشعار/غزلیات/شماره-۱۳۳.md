---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>خوشا سودای عشق و روزگارش</p></div>
<div class="m2"><p>وز آن خوش تر به جان ما شرارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم در زلف او عمری اسیر است</p></div>
<div class="m2"><p>من آشفته سامان یادگارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توانم برد چشم ناتوانش</p></div>
<div class="m2"><p>قرارم برد زلف بی قرارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسلمانان ز عشق روی خوبان</p></div>
<div class="m2"><p>دلی دارم، ندارم اختیارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنام ایزد مرا باشد نگاری،</p></div>
<div class="m2"><p>که مانی شرمسار است از نگارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی دانم چه باغست این محبت</p></div>
<div class="m2"><p>که برق آرد به جان گل شرارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن وادی که عشق آید به جولان</p></div>
<div class="m2"><p>جهان تسخیر یک چابک‌سوارش</p></div></div>