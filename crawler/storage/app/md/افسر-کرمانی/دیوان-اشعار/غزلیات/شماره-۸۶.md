---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>زلف سیه منه که حجاب قمر شود</p></div>
<div class="m2"><p>مپسند روزگارم از این تیره تر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه روی من، مشو ایمن که سوی توست</p></div>
<div class="m2"><p>آهم، که هم عنان نسیم سحر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غافل مشو ز آتش پنهانی دلم،</p></div>
<div class="m2"><p>مپسند پای تا سرم از گریه تر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیلی است سیل اشک که از هجر لعل یار،</p></div>
<div class="m2"><p>هرچند خون شود جگر، این بیشتر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیداست کز غم لب یاقوت فام توست</p></div>
<div class="m2"><p>لخت دلم که رنگ به خون جگر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ویرانه دلم که بود جای گنج مهر،</p></div>
<div class="m2"><p>ز این بیشتر مخواه که زیر و زبر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوته نظر، نظر ز تو بر دیگری کند</p></div>
<div class="m2"><p>حاشا که دیده جز به رخت دیده‌ور شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای نور دیده دیده به غیرت درآورم</p></div>
<div class="m2"><p>گر دیده باز بر رخ یاری دگر شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افسر حدیث عشق تو و محنت فراق</p></div>
<div class="m2"><p>مشکل فسانه ای است مبادا سمر شود</p></div></div>