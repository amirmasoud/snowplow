---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>تا چند جفا باما‌، یک ره ز وفا دم زن</p></div>
<div class="m2"><p>با خیل وفاکیشان، دستان جفا کم زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمعیت دلها را، آشفته اگر خواهی،</p></div>
<div class="m2"><p>دستی ز سر شوخی، بر طرّه پر خم زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل در خم گیسویت، کارش به جنون پیوست</p></div>
<div class="m2"><p>بر پای گران بندیش زین سلسله محکم زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دایره ای کانجا، خوبان جهان جمعند</p></div>
<div class="m2"><p>بنمای یکی جلوه، وآن دایره بر هم زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر گردن دلها بند، از زلف مسلسل نه</p></div>
<div class="m2"><p>بر تارک جانها، تیغ،‌ زابروی موسّم زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زآن رایت فیروزی،‌ کت قامت موزون است</p></div>
<div class="m2"><p>بروی ز دو چین زلف، همواره دو پرچم زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سنبل مشکینت، تاری به چمن بفرست</p></div>
<div class="m2"><p>طغرای سیه رویی، بر خط سپرغم زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ملک سخن افسر، در زیر نگین خواهی</p></div>
<div class="m2"><p>روزی چو سلیمان دم، زآن لعل چو خاتم زن</p></div></div>