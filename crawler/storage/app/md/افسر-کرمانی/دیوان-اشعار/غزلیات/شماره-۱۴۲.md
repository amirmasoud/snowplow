---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>من که سودا زده روی توام</p></div>
<div class="m2"><p>بسته سلسله موی توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که خود بلبل هر گل نشوم</p></div>
<div class="m2"><p>قمری قامت دلجوی توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شب از دست غمت جان سپرم</p></div>
<div class="m2"><p>هر سحر زنده کند بوی توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو برقی که به خرمن گذرد</p></div>
<div class="m2"><p>سوخت سر تا به قدم خوی توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز و شب صحبت هر انجمن است</p></div>
<div class="m2"><p>قصه روی تو و موی توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ بر کشتن من آخته ای</p></div>
<div class="m2"><p>بنده ساعد و بازوی توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کسی سوی کسی دیده برد</p></div>
<div class="m2"><p>من همه دیده و دل سوی توام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بود کوه احد پیکر من</p></div>
<div class="m2"><p>همچو کاهی به ترازوی توام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که از یک نگهم زنده کنی</p></div>
<div class="m2"><p>کشته تیغ دو ابروی توام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای که با جنبش آن زلف دراز</p></div>
<div class="m2"><p>صولجان داری و من گوی توام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افسرا، فاش تر از این می گوی</p></div>
<div class="m2"><p>که بود گلشن جان کوی توام</p></div></div>