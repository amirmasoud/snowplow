---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>عاشق نتوان گفت که در بند نباشد</p></div>
<div class="m2"><p>در بند گرفتاری، خرسند نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق به مایی و منی یار نگردد</p></div>
<div class="m2"><p>در بند خودی در غم دلبند نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند فریبم دهی ای دوست که شکر،</p></div>
<div class="m2"><p>شیرین بود آنجا که شکرخند نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زلف پریشان تو دل ها همه جمعند</p></div>
<div class="m2"><p>ما راست دلی شیفته، مپسند نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی نکشد هیچ تنی پیکر الوند</p></div>
<div class="m2"><p>ما را غم عشقت، کم از الوند نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آن لب شیرین دو سه دشنام بیامیز</p></div>
<div class="m2"><p>هرچند بود تلخ، کم از قند نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز نبرم شکوه دلدار به اغیار</p></div>
<div class="m2"><p>افسر، گله از یار خوشایند نباشد</p></div></div>