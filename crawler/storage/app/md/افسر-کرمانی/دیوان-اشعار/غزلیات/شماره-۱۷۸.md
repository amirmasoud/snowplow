---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>بیا، تصرف در صنعت سکندر کن</p></div>
<div class="m2"><p>ز آفتاب رخ، آیینه اش منور کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگو به ساقی مجلس، برغم زاهد خشک</p></div>
<div class="m2"><p>ز آب روشن ساغر دماغ ما تر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عکس آن لب یاقوت، در پیاله ما</p></div>
<div class="m2"><p>به جای لعل مروّق، شراب کوثر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر علاج جنون مرا طلبکاری</p></div>
<div class="m2"><p>به گردن دلم آن طرّه معنبر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه فصل بهار است و بوستان فردوس،</p></div>
<div class="m2"><p>بیا و کویم از آن رخ، بهار دیگر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وجود خاک رقیبان ز پختگی دور است</p></div>
<div class="m2"><p>وجود پخته ما را بسوز و اخگر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به قاف غم چه نشینم هماره چون سیمرغ</p></div>
<div class="m2"><p>دمی بر آتش آن رخ مرا سمندر کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگو به افسر از این پس که دُر نظمت را</p></div>
<div class="m2"><p>نثار مقدم دلدار مهرپرور کن</p></div></div>