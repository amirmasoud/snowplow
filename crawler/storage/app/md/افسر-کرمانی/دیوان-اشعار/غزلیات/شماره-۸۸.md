---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>حاجتم از روی خوبان، جز تماشایی نباشد</p></div>
<div class="m2"><p>ور میسر گرددم، دیگر تمنایی نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلم جز مهر رخسار بتان، چیزی نگنجد</p></div>
<div class="m2"><p>در سرم جز عشق خوبان، شور و سودایی نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده رنگین ننوشم، کام از ساغر نگیرم</p></div>
<div class="m2"><p>تا به پهلویم، نگار باده پیمایی نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر پری رویی نباشد، همرهم در باغ و صحرا</p></div>
<div class="m2"><p>خوش به چشمم بی رخ او، باغ و صحرایی نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل به چشمم خار آید، سبزه همچون نوک نشتر</p></div>
<div class="m2"><p>گر به گرد سبزه و گل، ماه سیمایی نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرف بستان را نشاید بی نکو رویان تفرج</p></div>
<div class="m2"><p>سایه سروی مجو، تا سرو بالایی نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افسرا، گر خود دلی داری و دلداری نداری</p></div>
<div class="m2"><p>نام دل بر وی منه، کمتر ز خارایی نباشد</p></div></div>