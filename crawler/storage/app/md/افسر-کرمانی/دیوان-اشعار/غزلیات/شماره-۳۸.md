---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>زمان سرکشی عشق و گاه شرب مدام است</p></div>
<div class="m2"><p>بیا به دور تو گردم که وقت گردش جام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زاهدم سخنی هست اگرچه پند نگیرد</p></div>
<div class="m2"><p>بریز خون صراحی که خون خلق حرام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سوز عشق مشو غافل ای دل هوس‌ آیین</p></div>
<div class="m2"><p>بسوز در دل آتش که خود نسوخته خام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب مدار اگر نیست عشق شیوه عاقل</p></div>
<div class="m2"><p>که آن به خلوت خاص است و این به شارع عام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که صورت و معنی ندیده است نداند</p></div>
<div class="m2"><p>میان کعبه و بتخانه راه صدق کدام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دیدم آن بت عیار خویشتن نپرستم</p></div>
<div class="m2"><p>که کار عاشق مسکین به یک نگاه تمام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کار عشق زبان هیچگه نبود و نباشد</p></div>
<div class="m2"><p>که دور عشق به عاشق علی الدوام به کام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بغیر هجر و وصالت مرا نه صبحی و شامی</p></div>
<div class="m2"><p>برآر پرده که صبح و بپوش چهره که شام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندیده جلوه قامت شنیده ای تو قیامت</p></div>
<div class="m2"><p>قیامت آن قد موزون یار خوب خرام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخوان حکایت محمود و سرّ عشق ایازش</p></div>
<div class="m2"><p>ببین چگونه در این آستانه شاه غلام است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگو به طایر آزاد شاخسار چو افسر</p></div>
<div class="m2"><p>به شوق دانه بغفلت مرو که حلقه دام است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما را به لب از نقطه دهانی است حکایت</p></div>
<div class="m2"><p>گفتیم و به عمری نشد این نکته روایت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن سخت کمان تیرم اگر زد، بهلش باد</p></div>
<div class="m2"><p>کی عاشق صادق کند از دوست شکایت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز ز آن که تو برما دگری را بگزینی،</p></div>
<div class="m2"><p>از ما نکند مهر تو بر غیر سرایت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی پرتو رخسار تو ای ماه شب افروز،</p></div>
<div class="m2"><p>روشن نشود محفلم از شمع هدایت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صورت ز تو نادیده مگر چشم بصیرت</p></div>
<div class="m2"><p>سیرت ز تو نشنیده مگر گوش روایت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جور تو به جایی است که جانم به تظلم</p></div>
<div class="m2"><p>حسن تو به حدّی است که جرمم به نهایت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>افسوس که هرگز ننوازی به نگاهی</p></div>
<div class="m2"><p>ما را که کند گوشه چشم تو کفایت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ما را سخن از زلف و رخت بوده و عمری،</p></div>
<div class="m2"><p>گفتیم و به پایان نرسید این دو حکایت</p></div></div>