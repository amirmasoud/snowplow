---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>جز ماه من که هشته به تارک کلاه را</p></div>
<div class="m2"><p>باور مکن که بوده کله فرق ماه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد از عذار خویشتن ای ماه من بگیر</p></div>
<div class="m2"><p>بزدای، ز آینه، اثر دود آه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خاطر حزین مکن ای دل خیال دوست</p></div>
<div class="m2"><p>اندر وثاق تنگ مبر پادشاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچند نیست غیر نگاهی،‌ گناه من</p></div>
<div class="m2"><p>شویم به آب دیده حروف گناه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد آورد به عمری اگر دل گیاه چند</p></div>
<div class="m2"><p>سوزد به یک نفس تف عشق آن گیاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمری است فرش راه طلب، دیده کرده ام</p></div>
<div class="m2"><p>شاید قدم نهی دگر این فرش راه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب ها ز بس که اشک ز چشم ترم چکد،</p></div>
<div class="m2"><p>سیلی شود چنان که برد خوابگاه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسر، کمند زلف تو نازد که هر خمش،</p></div>
<div class="m2"><p>هم شاه را اسیر کند هم سپاه را</p></div></div>