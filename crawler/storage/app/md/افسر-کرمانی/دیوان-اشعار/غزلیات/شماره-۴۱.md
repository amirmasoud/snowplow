---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>عید است و مرا در طلب باده شتاب است</p></div>
<div class="m2"><p>خورشید مرا ساغر و یاقوت شراب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بوسه لعلش نخورم باده گلگون،</p></div>
<div class="m2"><p>کانجا که بود بوسه، بلی باده چو آب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چشمه حیوان لبش کوثر و تسنیم</p></div>
<div class="m2"><p>ما تشنه لبان را همه رخشنده سراب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی تو گلابم چه زنی این همه بر رخ؟</p></div>
<div class="m2"><p>ما را عرق آن گل رخسار، گلاب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن خطه حسن است که تعمیر شد از یار</p></div>
<div class="m2"><p>واین کشور عشق است که از جور خراب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای پادشه حسن، که در کشور نازی،</p></div>
<div class="m2"><p>دریاب، که دلجویی درویش ثواب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلدار به نالیدن افسر ندهد گوش</p></div>
<div class="m2"><p>این ناله عشق است نه طنبور و رباب است</p></div></div>