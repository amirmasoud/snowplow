---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>خوبان که در آیینه رخ خود نگرانند</p></div>
<div class="m2"><p>دانند که عشاق چه صاحبنظرانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوی محبّت چه عجب مرحله سنجند</p></div>
<div class="m2"><p>در بحر حقیقت چه گرامی گهرانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش آن مه خورشید نشان چهره نمودی</p></div>
<div class="m2"><p>کاین خاک نشینان به رهش منتظرانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها نه منم از پی او بی خبر از خویش</p></div>
<div class="m2"><p>برّی است که تن ها ز پیش بی خبرانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی جامه که بر تن به عوض دست بدرند</p></div>
<div class="m2"><p>آنجا که مجانین غمت جامه درانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غوغای خلایق همه دانی سبب از چیست</p></div>
<div class="m2"><p>بر عشق من و حُسن تو افسوس خورانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی افسرش از گفته سعدی همه دم گفت</p></div>
<div class="m2"><p>«شوخی مکن ای دوست که صاحبنظرانند»</p></div></div>