---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>حالت چشم تو و قصه بیماری دل</p></div>
<div class="m2"><p>می توان یافت طبیب از اثر زاری دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت بیدارتو داند که من و مردم چشم</p></div>
<div class="m2"><p>چشم خوابی نکنیم از غم بیماری دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و چشم تو و دل هر سه علیل و بیمار</p></div>
<div class="m2"><p>هم مگر لعل تو آید به پرستاری دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون شد از بس که غم لعل تو را خورد دلم</p></div>
<div class="m2"><p>نکند لعل تو از بهر چه غمخواری دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک گرفتار در این شهر نباشد چه کنم؟</p></div>
<div class="m2"><p>با چنین بی خبران شرح گرفتاری دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ دانی که چرا از وطن آواره شدم</p></div>
<div class="m2"><p>جای ما نیست درآن زلف ز بسیاری دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب نبوده است در آن زلف سیاه پرخم،</p></div>
<div class="m2"><p>که مرا گم نشود جان به طلبکاری دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه از دوست جفا دید وفا کرد دلم</p></div>
<div class="m2"><p>افسر، آیین جفا بین و وفاداری دل</p></div></div>