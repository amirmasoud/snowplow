---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>در کشورم کشید سپه پادشاه باز</p></div>
<div class="m2"><p>در مزرعم نماند اثر از گیاه باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور از نظر شدی تو و عمری بود که ما،</p></div>
<div class="m2"><p>داریم دیده در طلبت فرش راه باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نفس شوم شرمی از این کرده های زشت</p></div>
<div class="m2"><p>گیرم که کردگار ببخشد گناه باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچند صیقل‌ آوری افزون شود غبار</p></div>
<div class="m2"><p>آیینه را که تیره کند دود آه باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بنده گریخته از درگه توام</p></div>
<div class="m2"><p>هم بر در تو آورم اکنون گناه باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جهل اگرچه توبه شکستم هزار ره</p></div>
<div class="m2"><p>نتوان طمع برید ز لطف اله باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کوی دوست دل نگران گر نمی رود</p></div>
<div class="m2"><p>اندر قفا کند ز چه افسر نگاه باز؟</p></div></div>