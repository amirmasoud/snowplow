---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>جز ملک محبت به جهان مملکتی نیست</p></div>
<div class="m2"><p>جز بندگی دوست، در آن سلطنتی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل می سپرم در دهن افعی زلفت،</p></div>
<div class="m2"><p>در فکرت دیوانه، مرا مشورتی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مرحمت آزاد غمت را بنوازی</p></div>
<div class="m2"><p>برمن که اسیر تو شدم مرحمتی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این منزلتی نیست که بر چرخ برآیم</p></div>
<div class="m2"><p>جز خاک شدن در قدمت منزلتی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریاب که وصل تو بود بر من درویش،</p></div>
<div class="m2"><p>آن دولت دایم که در آن مسکنتی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز در قدمش خاک شود پیکر افسر</p></div>
<div class="m2"><p>بر درگه آن ماه مرا مسألتی نیست</p></div></div>