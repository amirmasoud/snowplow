---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>تا چند سخن از عقل، از عشق دلا دم زن</p></div>
<div class="m2"><p>زنهار، دم از دانش اندر بر ما،‌ کم زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسانه دور دهر، بر ما چه فروخوانی</p></div>
<div class="m2"><p>شیدای رخ یاریم،‌ با ما ز جنون دم زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اسرار نهان عشق، یک نکته بگو با ما</p></div>
<div class="m2"><p>دم از نفس قدسی، بر قالب آدم زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودای سرو سامان، با عشق نمی سازد</p></div>
<div class="m2"><p>رو،‌ خانه هستی را بنیان کن و برهم زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درویشی کوی دوست بر محتشمی بگزین</p></div>
<div class="m2"><p>در عین گدایی پای بر دستگه جم زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر قصد عدوی نفس، کش نخوت فرعونی است</p></div>
<div class="m2"><p>روزی چو کلیم ای دل، بی واهمه بر یم زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این کارگه خاکی، بر همت ما تنگ است</p></div>
<div class="m2"><p>خرگاه عزیمت را، بیرون ز دو عالم زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرگشته در آن وادی، خشکیده لبی چند است</p></div>
<div class="m2"><p>باری تو قدم آنجا، با دیده پرنم زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عرصه جانبازی، منصور صفت پانه</p></div>
<div class="m2"><p>بردار بلا خود را، با خاطر خرّم زن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در شش جهت این کاخ، تا چند دو دل بودن</p></div>
<div class="m2"><p>تکبیر چهارم را، چون زاده ادهم زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آیین کرم افسر، منسوخ شد از گیتی</p></div>
<div class="m2"><p>باری تو دم همت ز ارواح مکرم زن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر سری است با گلت، به عارض نگار بین</p></div>
<div class="m2"><p>وگر هواست سنبلت، شکنج زلف یار بین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مخواه نوبهار را، مجو بنفشه زار را</p></div>
<div class="m2"><p>ز طره آن نگار را، بنفشه در کنار بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه طرف باشد از گلت، چه آرزو به سنبلت</p></div>
<div class="m2"><p>ز روی یار محفلت، فریب نوبهار بین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به عارضش عرق نگر، ستاره بر شفق نگر</p></div>
<div class="m2"><p>گهر به گل ورق نگر، به مه در استوار بین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الا که می برد دلت، به سوی سبزه و گلت</p></div>
<div class="m2"><p>ز بوستان چه حاصلت، به بوستان عذار بین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به کوه لاله زارها، به دشت سبزه کارها</p></div>
<div class="m2"><p>بهر چمن بهارها، از او به یادگار بین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گلی نگر بهشت رو، بهارکی بنفشه مو</p></div>
<div class="m2"><p>فراز سرو قد او، گل و بنفشه یار بین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز طلعتش بهار جو، ز طرّه اش تتار جو</p></div>
<div class="m2"><p>ز قامتش عرار جو،‌به نرگسش خمار بین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مگو که نوبهار شد، زمین بنفشه زار شد</p></div>
<div class="m2"><p>اگر جهان نگار شد، تو طلعت نگار بین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فسون و ناز و غمزه اش، دلال و غنج و عشوه اش</p></div>
<div class="m2"><p>تعلل و کرشمه اش فزون و بیشمار بین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز جعد تابدار او، ز خط مشکبار او،</p></div>
<div class="m2"><p>فراز لاله زار او، دمیده سبزه زار بین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بجوی طرفه یارکی، بتی سمن عذارکی</p></div>
<div class="m2"><p>بهشت وش نگارکی، وز آن به دل قرار بین</p></div></div>