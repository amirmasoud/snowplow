---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>روز هنگامه شه ما، صف مژگان سپهش</p></div>
<div class="m2"><p>وآن خم زلف پر از حلقه، کمند و زرهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک آن چشم سیاه مژه اش حاجت نیست</p></div>
<div class="m2"><p>کار صد لشکر خونخوار کند یک نگهش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که از ناز نهد پای تکبر بر خاک</p></div>
<div class="m2"><p>غافل است آن که بوّد فرق سران خاک رهش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که از دیده او دیده ما گشت سفید،</p></div>
<div class="m2"><p>نور هر دیده بود مردم چشم سیهش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قیامت نکند میل بهشت و رخ حور</p></div>
<div class="m2"><p>هر که آغوش نگاری بود آرامگهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر ترسم بر افسر هنر خویش کند</p></div>
<div class="m2"><p>فرصت صحبت وی بار خدایا مدهش</p></div></div>