---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>ای ز شرم عارضت در کاستن جرم قمر</p></div>
<div class="m2"><p>وز عقیق لعل تو، لعل بدخشان خون جگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشک تاتاری که این سان منتشر شد در جهان</p></div>
<div class="m2"><p>از عبیر زلف تو باد صبا دادش خبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم حکایت می کند روی تو را بستان و گل</p></div>
<div class="m2"><p>هم روایت می کند بانگ مرا مرغ سحر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چه خواهد بود حال سینه های چون حریر</p></div>
<div class="m2"><p>تیر مژگانت که کرد از گنبد گردون گذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نیازی آورم روزی بگو دشنام تلخ</p></div>
<div class="m2"><p>طوطیان را باید از منطق چکد شهد و شکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاکیم رنجور خواهی، گر کشی زودم بکش</p></div>
<div class="m2"><p>خون عاشق باشد اندر مذهب خوبان هدر</p></div></div>