---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>در دام گرفتند و شکستند پرم را</p></div>
<div class="m2"><p>وانگاه به بازیچه بریدند سرم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش سرم را که به بازیچه بریدند،</p></div>
<div class="m2"><p>بریان ننمودند بر آتش جگرم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم همه طوفان شود، ای وای به مردم</p></div>
<div class="m2"><p>خشک ار نکند آتش دل چشم ترم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه ز بیداد دگر زیر و زبر کرد،</p></div>
<div class="m2"><p>دست غمت این خانه زیر و زبرم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانم به لب و سوی توام راه نباشد</p></div>
<div class="m2"><p>ای وای، صبا گر نرساند خبرم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی تو آسوده توانم که بیایم</p></div>
<div class="m2"><p>گر اشک روانم نکند گل گذرم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افسر نبود در همه کشور خوبی،</p></div>
<div class="m2"><p>دادی که بود دلبر بیدادگرم را</p></div></div>