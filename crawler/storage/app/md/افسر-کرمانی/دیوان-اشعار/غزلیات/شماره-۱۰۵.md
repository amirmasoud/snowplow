---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>بسته بر قتل من آن ترک جفا پیشه کمر</p></div>
<div class="m2"><p>ابروان کرده حسام و مژّگان را خنجر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ما در صف مژگانش، شبی یک تنه تاخت</p></div>
<div class="m2"><p>چون نیارست ستیز آورد انداخت سپر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نثار دُر دندان و عقیق لب او،</p></div>
<div class="m2"><p>از دلم دیده فرو ریخت عقیقین گوهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبرد شیفتگی راه به غم خانه دل</p></div>
<div class="m2"><p>نشود شیفته، گر زلف تو از باد سحر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خم زلف دلم آرزوی لعل تو کرد</p></div>
<div class="m2"><p>طلبد آب حیات از ظلمات اسکندر</p></div></div>