---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>مرا ز دست تو دانی،‌ چها بسر گذرد</p></div>
<div class="m2"><p>چها به من، ز تو ای شوخ سیمبر گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت روز وصال تو و همی شادم</p></div>
<div class="m2"><p>که شام هجر تواش، نیز بر اثر گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز دو سنبل مشکین فراز نخل قدت</p></div>
<div class="m2"><p>که دیده مشک تر، از سرو کاشمر گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شام هجر تو، بس شعله ها که زآه دلم</p></div>
<div class="m2"><p>شهاب وار، ز خاور به باختر گذرد</p></div></div>