---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>صبا، به جانان اگر توانی،</p></div>
<div class="m2"><p>پیامی از من ببر نهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگو، ز نازت، چه می‌شود کم</p></div>
<div class="m2"><p>شبی به کویت گرم بخوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جام وصلت، زلال مهری</p></div>
<div class="m2"><p>به کام خشکم، اگر چکانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرشکم از چشم، همی کنی پاک</p></div>
<div class="m2"><p>غبارم از رخ، همی فشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه عهد کردی که از محبان،</p></div>
<div class="m2"><p>علاقه مهر، نه بگسلانی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شد که اینک نمی کنی یاد</p></div>
<div class="m2"><p>ز دوستداران به هیچ آنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از فراغت همیشه ناکام</p></div>
<div class="m2"><p>تو با رقیبان، به کامرانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو کرده ای خو به نعمت و ناز</p></div>
<div class="m2"><p>بلای هجران بلی ندانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حالت من دلت بسوزد</p></div>
<div class="m2"><p>غم دلم را، اگر بدانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه نونهالی است قد تو یارب</p></div>
<div class="m2"><p>که سرو نبود، بدین روانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهاده ابروت، به قصد جانم</p></div>
<div class="m2"><p>ز مژّه صد تیر به یک کمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن چه گویی، چو نیستت لب</p></div>
<div class="m2"><p>کمر چه بندی، که بی میانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نوید وصلی بده به افسر،</p></div>
<div class="m2"><p>که جان سپارد، به مژدگانی</p></div></div>