---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>با صبا نفحه جان از بر جانان آید</p></div>
<div class="m2"><p>هر نفس ز آمدنش در تن ما جان آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که ننشست و به اکراه برفت، اینک باز،</p></div>
<div class="m2"><p>فرش راه طلبش، دیده یاران آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدعی قالب بی جان شده، کان جان جهان،</p></div>
<div class="m2"><p>سوی ما مست و غزل خوان و خرامان آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهره نظاره کنان است که در مجلس انس،</p></div>
<div class="m2"><p>دف زنان، رقص کنان، یار غزل خوان آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر آن روح روان رفته به بستان کامروز،</p></div>
<div class="m2"><p>بوی جان هر نفس از جانب بستان آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار پیمان شکنم نیست مگر مایه عمر،</p></div>
<div class="m2"><p>عمر باز آید اگر بر سر پیمان آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغ هجر تو نه داغی، که پذیرد مرهم،</p></div>
<div class="m2"><p>درد عشق تو، نه دردی که به درمان آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسرا، خاطر مجموع از این حلقه مجوی،</p></div>
<div class="m2"><p>خاصه این لحظه، که با زلف پریشان آید</p></div></div>