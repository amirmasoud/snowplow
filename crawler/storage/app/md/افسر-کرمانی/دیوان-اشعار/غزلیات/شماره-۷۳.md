---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>آن گروهی که ز جان و دل ما خوب‌ترند</p></div>
<div class="m2"><p>آن گروهند که پرورده خون جگرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخ چشمند و سیه طرّه و سیمین اندام</p></div>
<div class="m2"><p>بلکه با طلعت خورشید نظیر قمرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه سنگین دل و پیمان شکن و عهد گسل</p></div>
<div class="m2"><p>سرو رفتار و صنوبر قد و طوبی ثمرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حور کردار و پری پیکر و شکر گفتار</p></div>
<div class="m2"><p>حیف و صد حیف، که عاشق کش و بیدادگرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهوشان شه منش و با گهر و نازک طبع</p></div>
<div class="m2"><p>عاشقان مفلس و دیوانه و بی پا و سرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانه هایی که من از دیده به دامان دارم</p></div>
<div class="m2"><p>در غم سیمبران حسرت لعل و گهرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محنت عشق و غم فرقت یاران عزیز،</p></div>
<div class="m2"><p>همچو روز و شب ما غمزدگان در گذرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خوش آنان که ز شوق قد سرو و رخ گل،</p></div>
<div class="m2"><p>در مقامات غزل همدم مرغ سحرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افسرا، در غم دلدار، ز ما صبر مجوی</p></div>
<div class="m2"><p>عاشقانی که صبورند گروهی دگرند</p></div></div>