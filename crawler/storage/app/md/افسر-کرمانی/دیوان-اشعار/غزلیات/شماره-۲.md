---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای لب لعل تو روح بخش مسیحا</p></div>
<div class="m2"><p>وی به روان بخشی از مسیح معلاّ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر به جام ار کنی، تو با همه تلخی</p></div>
<div class="m2"><p>خوب تر آید مرا ز شهد مصفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک تو بر فرق، به که تاج به تارک</p></div>
<div class="m2"><p>سر به سرای تو، به که پا به ثریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر به من بنگرد به دیده حسرت</p></div>
<div class="m2"><p>گر به تو باشد مرا نگاه چو حربا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنج تو بر جان ما کم است و محقر</p></div>
<div class="m2"><p>درد تو بر جان ما خوش است و مهنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح وصال تو، بامداد همایون</p></div>
<div class="m2"><p>روز فراق تو، شام تیره یلدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تو منور چراغ معنی هستی</p></div>
<div class="m2"><p>وز تو مصوّر وجود صورت اشیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن جاهت ز شرح و وصف منزه</p></div>
<div class="m2"><p>پایه ذاتت ز چون و چند مبرّا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افسر و مدحت، زهی بزرگ جسارت</p></div>
<div class="m2"><p>پشه و آنگاه، لاف عرصه عنقا</p></div></div>