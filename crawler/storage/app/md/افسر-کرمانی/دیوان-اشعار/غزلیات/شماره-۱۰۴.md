---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>ای ز ماه طلعتت خورشید تابان شرمسار</p></div>
<div class="m2"><p>وز عبیر طرّه ات مشک تتاری یادگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست شهد جان مشتاق، آن دو لعل شکرین</p></div>
<div class="m2"><p>چیست تار عمر عاشق، آن دو زلف تابدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح و شام از ساحت گیتی گریزد در عدم</p></div>
<div class="m2"><p>پرده برداری اگر روزی از آن زلف و عذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست پنهان در کمانت، ای کمان ابرو که صید</p></div>
<div class="m2"><p>می خورد تیر تو را چون سبزه اندر مرغزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون گره در ناف آهوی ختایی شد زرشک</p></div>
<div class="m2"><p>تا به صحرای ختا، بردت صبا بویی ز تار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهمت روزی چو جان اندر کنار خویشتن،</p></div>
<div class="m2"><p>همچو بادام دو مغز اندر یکی جلد استوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نار در مار آوری پنهان ز هر آئین و کیش</p></div>
<div class="m2"><p>مار بر نار افکنی پیدا ز هر رسم و شعار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بهار ار نغمه سنج آمد هزار اندر چمن</p></div>
<div class="m2"><p>افسر، اندر گلشن تو نغمه‌ها زد چون هزار</p></div></div>