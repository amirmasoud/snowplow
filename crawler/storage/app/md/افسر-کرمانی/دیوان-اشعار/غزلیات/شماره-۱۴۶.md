---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>چو ماه روی تو را در خیال می نگرم</p></div>
<div class="m2"><p>دگر خیال بود هر چه هست در نظرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر تو را لب لعل است لؤلؤ شهوار</p></div>
<div class="m2"><p>من از خیال لبت چون عقیق خون جگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو سرو بنی، من تذرو نغمه تراز</p></div>
<div class="m2"><p>اگر تو شاخ گلی من چو بلبل سحرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم تو اوج دهی بر سریر نه فلکم</p></div>
<div class="m2"><p>گرم تو پست کنی کم ز خاک رهگذرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر التفات توام پایمرد لطف شود،</p></div>
<div class="m2"><p>به ملک نظم یکی شهسوار تاجورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه ملک خرابم ولی به همت دوست،</p></div>
<div class="m2"><p>در اوج شاعری آزرم ابر پرگهرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان درخت کهن عمر دیرسالم من</p></div>
<div class="m2"><p>که سنگ جور تو ریزد شکوفه و ثمرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمک به منطق شیرین من شود شکر</p></div>
<div class="m2"><p>چو بر زبان گذرد نام شاهد شکرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز یمن دولت دلدار افسرا، هر شب</p></div>
<div class="m2"><p>در آسمان همه شعرا چو شعر می شمرم</p></div></div>