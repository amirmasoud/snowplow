---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>بهر پا بستن دل زلف گره‌گیر دوتا</p></div>
<div class="m2"><p>هست دیوانه یکی حلقه زنجیر دوتا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک چشمش چو نظر جانب ابرو افکند</p></div>
<div class="m2"><p>واجب‌القتل یکی، دست به شمشیر دوتا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز دو لعل لب و آن حلقه موهوم دهان</p></div>
<div class="m2"><p>جمع نادیده کس عنقا یک و اکسیر دوتا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان و دل برد بها، گندم خال تو عجب</p></div>
<div class="m2"><p>جنس یک جنس در این کشور و تسعیر دوتا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من یکی خواستم او بوسه رو بخشید به من</p></div>
<div class="m2"><p>ای عجب خواب یکی آمد و تعبیر دوتا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخم افسر، ز چه دور افکند از درگه دوست</p></div>
<div class="m2"><p>گر نه تقدیر یکی آمد و تدبیر دوتا</p></div></div>