---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>هر بنده که بر در تو دربانی کرد</p></div>
<div class="m2"><p>حاتم صفت آغاز زرافشانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گسترده فلک چو خوان احسان تو دید</p></div>
<div class="m2"><p>صد ثور بجای بره قربانی کرد</p></div></div>