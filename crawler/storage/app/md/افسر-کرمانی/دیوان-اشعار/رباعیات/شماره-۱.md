---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>درملک تو راستی نیامد از ما</p></div>
<div class="m2"><p>جز کجی و کاستی، نیامد از ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز که خواستیم ما، آن کردیم</p></div>
<div class="m2"><p>چیزی که تو خواستی نیامد از ما</p></div></div>