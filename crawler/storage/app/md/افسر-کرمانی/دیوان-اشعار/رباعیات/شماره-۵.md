---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>کس چون من، اسیر محنت و سوز مباد</p></div>
<div class="m2"><p>در دام بتی چون تو، نوآموز مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شامی که تو را نبینم آن شب نبود</p></div>
<div class="m2"><p>روزی که تو را نخواهم آن روز مباد</p></div></div>