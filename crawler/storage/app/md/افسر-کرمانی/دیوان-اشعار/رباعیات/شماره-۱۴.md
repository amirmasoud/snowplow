---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>سرو آمده یا به گل ز رفتار خوشت</p></div>
<div class="m2"><p>گلبن شده منفعل ز طرز روشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو تازه نهال از کدامین چمنی</p></div>
<div class="m2"><p>وز خون کدام دل بود پرورشت</p></div></div>