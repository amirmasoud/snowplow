---
title: >-
    شمارهٔ ۵۷ - درسوگ همسر خود
---
# شمارهٔ ۵۷ - درسوگ همسر خود

<div class="b" id="bn1"><div class="m1"><p>فلک جمعیتم بر هم زند، خواهد پریشانم</p></div>
<div class="m2"><p>نمی داند من از زلف بتی آشفته سامانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پری زادی که با خود رام کردم با هزار افسون</p></div>
<div class="m2"><p>هنوزش سیر نادیدم که شد از دیده پنهانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب شمع فروزانی اجل خاموش کرد از من</p></div>
<div class="m2"><p>که تاریک است بی نور جمال او شبستانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرامان گلبنی از من ز پا افکند دست دی</p></div>
<div class="m2"><p>که با شمشاد قدش رشک بستان بود ایوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روبه به بازی این گردون غزالم را ربود آخر</p></div>
<div class="m2"><p>بخوابم کرد چون خرگوش اگر چه شیر غژمانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تبه بادا، دل گردون، که سامانم بزد بر هم</p></div>
<div class="m2"><p>سیه بادا،‌ رخ انجم، که ویران کرد بنیانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گمانم بود کاین گردون به من دارد سر یاری</p></div>
<div class="m2"><p>ندانستم که او آخر کند با خاک یکسانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یغما رفت آن گوهر، که می پوشیدم از مردم</p></div>
<div class="m2"><p>دریغا زآن همه کوشش که افزون کرد حرمانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهاران روید از گلشن هزاران سنبل و سوسن</p></div>
<div class="m2"><p>نهان در خاک دارد تن، چرا آن شاخ ریحانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگو پاداش هر دادن ستادن نیست در گیتی،</p></div>
<div class="m2"><p>خزان یک گل گرفت از من، گلستان کرد دامانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلی، کو را بپروردم به آب چشم و خون دل</p></div>
<div class="m2"><p>بنفشه وار از هجرش کنون سر در گریبانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا با صحبت آن مه دلی خوش بود و کامی خوش</p></div>
<div class="m2"><p>دریغا کز بساط او، به دور افکند دورانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو یاد لعل او آرم که از تب کهربایی شد</p></div>
<div class="m2"><p>به یاد آن عقیق لب، چکد از دیده مرجانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به هر شاخی که در گلشن پرافشان طایری بینم</p></div>
<div class="m2"><p>به یاد آرم از‌ آن مرغی که بسمل شد به بستانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر بر تربتش گریم مکن منعم که حق دارم</p></div>
<div class="m2"><p>گلستانی است بی آب و من آنجا ابر نیسانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا در فرقت آن مه، مکن تشنیع ای ناصح</p></div>
<div class="m2"><p>تو در ساحل مکان داری، من اندر موج طوفانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو بر سنجاب می غلطی چه دانی حال مسکینان</p></div>
<div class="m2"><p>مرا پهلو بفرساید که عریان در مغیلانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تماشایی چه غم دارد که گلشن را رسد آفت</p></div>
<div class="m2"><p>غم گل من خورم زیرا که یک عمریش دهقانم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برآن عهدم که بعد از وی نگیرم یار در عالم</p></div>
<div class="m2"><p>چو گل برخاست از گلشن به جایش خار ننشانم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>الا ای باد شبگیری به آن محمل نشین برگو</p></div>
<div class="m2"><p>تو رفتی و منت از پی همی افتان و خیزانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گذارت گر فتد آنجا پیام از من ببر او را</p></div>
<div class="m2"><p>که ای مه حجله را آرا، که بر وصل تو مهمانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز هجرت ای سهی قامت رود از دیده جوی خون</p></div>
<div class="m2"><p>به یادت ای کمان ابرو، خلد در سینه پیکانم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>الا ای همدم دیرین که از خشت بود بالین</p></div>
<div class="m2"><p>نظر بگشای و بر من بین که خون پالاست مژگانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رخ از من زود بنهفتی میان خاک چون خفتی</p></div>
<div class="m2"><p>نه آخر بارها گفتی، که من صبر از تو نتوانم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نمودی جای در محمل، نهادی بار غم بر دل</p></div>
<div class="m2"><p>جرس آسا به هر منزل، منت از پی در افغانم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز کویم رخت بربستی، مگر از یاریم خستی</p></div>
<div class="m2"><p>چه دیدی کز برم جستی، شکستی عهد و پیمانم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو خوش رفتی و آسودی مرا از غم بفرسودی</p></div>
<div class="m2"><p>ز رفتن گر تو خشنودی، من از ماندن پشیمانم</p></div></div>