---
title: >-
    شمارهٔ ۳۱ - امام عوالم
---
# شمارهٔ ۳۱ - امام عوالم

<div class="b" id="bn1"><div class="m1"><p>تعجب از آن روی چون مهر انور</p></div>
<div class="m2"><p>شگفتی از آن موی چون مشک اذفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن موی صبحم چو شام است تیره</p></div>
<div class="m2"><p>وز آن روی شامم چو صبح است انور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد شب ما مگر صبح از پی</p></div>
<div class="m2"><p>و یا صبح هجر است شامی مکرر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس بر وجودم رسد رنجی از تو</p></div>
<div class="m2"><p>ز بس بر عقودم فتد عقدی از سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنالند بر من ضعیف و توانا</p></div>
<div class="m2"><p>بمویند بر من فقیر و توانگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان گشتم از فتنه دهر عاجز</p></div>
<div class="m2"><p>چنان ماندم از باری چرخ مضطر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که درمان دردم بود سم قاتل</p></div>
<div class="m2"><p>که مرهم به زخمم بود نیش خنجر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه چشمم به ساقی، نه گوشم به مطرب</p></div>
<div class="m2"><p>نه ذوقم به صهبا، نه شوقم به ساغر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که شد ساقی و مطربم محنت جان</p></div>
<div class="m2"><p>که شد ساغر و باده ام زحمت سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من و رنج، با هم چو جانیم و قالب</p></div>
<div class="m2"><p>من و درد با هم چو روحیم و پیکر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی یاد دارم که در بوستانی</p></div>
<div class="m2"><p>نشاندیم شمشاد و سرو و صنوبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به نیروی بیداد و منشار کینه</p></div>
<div class="m2"><p>فتادند از پا نهالان دلبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوشا عهد دیرین که در بزم و باهم</p></div>
<div class="m2"><p>ستادیم خرم، نشستیم خوش تر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به حسرت بما چشم بیدار گردون</p></div>
<div class="m2"><p>بغیرت بما دیده باز اختر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برافروخت رخ یار، چون ماه نخشب</p></div>
<div class="m2"><p>برافراخت قد دوست چون سرو کشمر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلی دارم امروز بختی و وقتی</p></div>
<div class="m2"><p>چو شام غریبان و چون صبح محشر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرم را ز حسرت نه جز خشت بالین</p></div>
<div class="m2"><p>تنم را ز محنت نه جز خاک بستر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به نیران رنجم چو عاصی مخلد</p></div>
<div class="m2"><p>به عمّان دردم چو ماهی شناور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به گلشن روم گرنه با یار گلرخ</p></div>
<div class="m2"><p>به صحرا روم گرنه با شوخ دلبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بود لاله در سینه ام نار سوزان</p></div>
<div class="m2"><p>بود سبزه در دیده ام نوک نشتر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر نالم از زخم دیرینه دل</p></div>
<div class="m2"><p>زند دست دوران به دل زخم دیگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گریزم به دربار دارای دوران</p></div>
<div class="m2"><p>کشد دهر از من گر این گونه کیفر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خداوند گیرنده عدل پیشه</p></div>
<div class="m2"><p>شهنشاه بخشنده دادگستر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>امام عوالم ملقب به کاظم</p></div>
<div class="m2"><p>که آمد مسمی به موسی بن جعفر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زهی ای که آید ز حکمت مبدل</p></div>
<div class="m2"><p>ز یک لمحه کمتر قضای مقدر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تعالی الله از آسمان جلالت</p></div>
<div class="m2"><p>که مهر آفرین بی حدش باشد اختر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نمی خواند اگر نام پاک تو موسی</p></div>
<div class="m2"><p>نمی دید اگر نور تو پور آذر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیک لمحه مغروق می گشت در یم</p></div>
<div class="m2"><p>بیک لحظه محروق می شد در آذر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خیال است خس، وصف جاه تو قلزم</p></div>
<div class="m2"><p>عقولند اعراض و ذات تو جوهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خطا قلزمت گفتمی ز آنکه جوهر</p></div>
<div class="m2"><p>ز ابرت یک از قطره های مقطر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غلط جوهرت سفتمی ز آنکه جوهر</p></div>
<div class="m2"><p>ز جود تو هر دم ستد فیض دیگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخا راست دست عطای تو موجد</p></div>
<div class="m2"><p>لقا راست شخص ولای تو مصدر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به آب ولای تو ای موجد کل</p></div>
<div class="m2"><p>گل آدم آمد عجین و مخمر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو آنی که با صد هزاران تنزل</p></div>
<div class="m2"><p>عقول آمد از نام پاکت معبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بجز صورتت سجده بر هرچه آرم</p></div>
<div class="m2"><p>شود بی گمان لات و عزّی مصور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مشام عقول از شمیمت مروّح</p></div>
<div class="m2"><p>دماغ وجود از سجودت معفّر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگفتن سخن مفرد آری، تو آری</p></div>
<div class="m2"><p>شدت کثرت خصم جمع مکسر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به عدلت سزاوار باشد که آهو</p></div>
<div class="m2"><p>گزیند مکان در کنام غضنفر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گدای تو را تکیه زن چار بالش</p></div>
<div class="m2"><p>غلام تو خال رخ هفت کشور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی پلّه از بارگاه جلالت</p></div>
<div class="m2"><p>بسی برتر از کاخ نه طاق اخضر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز خاک درت کسب نور ار نکردی</p></div>
<div class="m2"><p>چو جرم قمر قرص خور شد مکدر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مزن دم تو افسر ز مدح شه دین</p></div>
<div class="m2"><p>که او را خداوند شد مدح گستر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بود تا تن خاکیم خانه جان</p></div>
<div class="m2"><p>من و مهر احمد من و حُبّ حیدر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>الا تا ز آهوی تاتار آید</p></div>
<div class="m2"><p>معنبر چو زلف بتان نافه تر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>محب ترا بخت چون مهر تابان</p></div>
<div class="m2"><p>به دنیا معزّز به عقبی مظفر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عدوی تو را روز و شب شام ادهم</p></div>
<div class="m2"><p>عنای موّفا، بلای موّفر</p></div></div>