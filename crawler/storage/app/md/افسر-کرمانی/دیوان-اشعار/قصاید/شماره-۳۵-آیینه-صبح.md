---
title: >-
    شمارهٔ ۳۵ - آیینه صبح
---
# شمارهٔ ۳۵ - آیینه صبح

<div class="b" id="bn1"><div class="m1"><p>صبح هنوز این عروس حجله خاور</p></div>
<div class="m2"><p>پرده نیفکنده بود از رخ انور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لشکر چین تاختن نکرده به هندو</p></div>
<div class="m2"><p>لشکر هندو تهی نساخته سنگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیرگی شب نرفته از رخ گیتی</p></div>
<div class="m2"><p>روشنی صبح نادمیده ز خاور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آینه آسمان نه روشن و نه تار</p></div>
<div class="m2"><p>ساحت غبرا نه صافی و نه مکدّر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک من، آن سنگدل حریف ستم خو،</p></div>
<div class="m2"><p>ماه من، آن سرو قد نگار سمن بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرّه و رخ آب و تاب داده، درآمد</p></div>
<div class="m2"><p>تا ببرد آب و تاب من همه یکسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاکر خورشیدش، آنچه ماه به خلخ</p></div>
<div class="m2"><p>بنده شمشادش آنچه سرو به کشمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریخته مرجان دو لعلش بر سر مرجان</p></div>
<div class="m2"><p>بیخته عنبر دو زلفش بر سر عنبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر رخ بیضا گسسته رشته پروین</p></div>
<div class="m2"><p>بر زبر مه شکسته حقه گوهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>افعی غژمان او، معلق شمشاد</p></div>
<div class="m2"><p>ارقم پیچان او حمایل عرعر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمده زاغش مقیم سایه گلبن</p></div>
<div class="m2"><p>جسته غرابش مکان به شاخ صنوبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر مه نورانیش دو هاله مشکین</p></div>
<div class="m2"><p>در شب ظلمانیش دو زهره ازهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رقص کنانش به ماه عقرب جرّار</p></div>
<div class="m2"><p>حلقه زنانش به گنج افعی حمیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نار خلیلش عیان و معجز داوود</p></div>
<div class="m2"><p>مار کلیمش عیان و صنع سکندر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوده الماسش گرد جزع یمانی</p></div>
<div class="m2"><p>حلّه حمراش زیر دیبه اخضر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسته به یک گلبنی دو گوی بلورین</p></div>
<div class="m2"><p>هشته به یک عرعری دو پشته مرمر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرده تباشیر را به غالیه پنهان</p></div>
<div class="m2"><p>ساخته شنگرف را به مشک مستر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر سمن از لاجورد هشته دو جلباب</p></div>
<div class="m2"><p>بر قمر از آبنوس بسته دو چنبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لاله حمرا نهفته زیر دو ریحان</p></div>
<div class="m2"><p>سبزه بویا نهاده گرد دو عبهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گشته سمن زار او چراگه آهو</p></div>
<div class="m2"><p>و آمده آهوی او دچار به اژدر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیضه کافور او به طبله زنگار</p></div>
<div class="m2"><p>قرص تباشیر او به نافه ازفر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرد دو گلنار او دو دسته سنبل</p></div>
<div class="m2"><p>زیر دو نسرین او دو شاخه سعتر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سعتر او را ز ارغوان همه بالین</p></div>
<div class="m2"><p>سنبل او را ز اقحوان همه بستر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لؤلؤی شهوار او به حقه سیمین</p></div>
<div class="m2"><p>گوهر شب تاب او به مشک مقطر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چشمه زیبق ز مشکپایش جاری</p></div>
<div class="m2"><p>پاره آهن به سیم خامش مضمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عاج و طبرخون نهان نموده به سنجاب</p></div>
<div class="m2"><p>نقل و طبرزد پراکنیده به شکر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرد گلش نیش خار هیچ ولیکن،</p></div>
<div class="m2"><p>گرد سیه نرگسش هزاران نشتر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غیر زنخدان و چهر او نشنیدم</p></div>
<div class="m2"><p>چاه مقعر به اوج ماه منوّر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پسته درآمیخته به شهد مهنّا</p></div>
<div class="m2"><p>فندقی انگیخته ز قند مکرر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گلشنی آراسته، که اینم عارض</p></div>
<div class="m2"><p>گلبنی افراخته که اینم پیکر</p></div></div>