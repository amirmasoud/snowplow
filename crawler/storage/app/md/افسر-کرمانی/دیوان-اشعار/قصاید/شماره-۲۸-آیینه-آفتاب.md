---
title: >-
    شمارهٔ ۲۸ - آیینه آفتاب
---
# شمارهٔ ۲۸ - آیینه آفتاب

<div class="b" id="bn1"><div class="m1"><p>ای دل گمشده باز آی که آمد به شهود</p></div>
<div class="m2"><p>آنچه مقصود تو از دایره هستی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل از پرده برون آی، که از پرده غیب</p></div>
<div class="m2"><p>شاهد بزم ازل آمده، در ملک وجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرکز دایره ملک وجود، آن که بود</p></div>
<div class="m2"><p>موجد نیر برج فلک عالم جود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناظم نظم جهان احمد مرسل که فلک</p></div>
<div class="m2"><p>از ازل آمده بر کاخ جلالش به سجود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب رخ تو جلوه گر از خود بیند</p></div>
<div class="m2"><p>هر که او، رنگ ظلم ز آینه دیده زدود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحر امکان هم از آن موج زن آمد ز نخست</p></div>
<div class="m2"><p>کآمد از ابر عطای تو یکی قطره فرود</p></div></div>