---
title: >-
    شمارهٔ ۸۱ - با سالکان کوی دوست
---
# شمارهٔ ۸۱ - با سالکان کوی دوست

<div class="b" id="bn1"><div class="m1"><p>مرا به خانه ی درون، دلی بود از آن تو</p></div>
<div class="m2"><p>ولی چه سود کان سرا نمی سزد مکان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بر لب آوری سخن شود فضای انجمن</p></div>
<div class="m2"><p>لبالب از دُر عدن ز لعل دُر فشان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو راست جسم نازنین لطیف تر ز روح و جان</p></div>
<div class="m2"><p>عجب که آمده زمین، مکان جسم و جان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار راهت افسرم، نثار مقدمت سرم</p></div>
<div class="m2"><p>بهشت را نمی خرم به خاک آستان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جور سیم بربتی، که مهر گشتش آیتی</p></div>
<div class="m2"><p>نهم رخ مذلتی، به پای سالکان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی منیر مهر و مه، تو آن رفیع بارگه</p></div>
<div class="m2"><p>که سوده فرقدان کله به پای پاسبان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الا علی مرتضی، چو آینه است ماسوا</p></div>
<div class="m2"><p>که کرده است برملا، صفایشان نشان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه موجدی تو بر جهان، چه علتی تو بر زمان</p></div>
<div class="m2"><p>نه این جهان تو را جهان، نه این زمان، زمان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه این زمین و آسمان، تو را مکان سزاست هان</p></div>
<div class="m2"><p>نه این مکان و لامکان، مکان و لامکان تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو برگزید ایزدت، ز قبل دهر و سرمدت</p></div>
<div class="m2"><p>بجز وجود واحدت کسی نه در جهان تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان و هرچه اندر او، ز وصف و ذات و رنگ و بو</p></div>
<div class="m2"><p>بریده هاست جوی جو، ز بحر بیکران تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به اوجه خلوت بشر، زدت عقاب جود پر</p></div>
<div class="m2"><p>اگرچه زاین سراست بر، بلند آشیان تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو مقصد و همه دگر، سوی تواند رهسپر</p></div>
<div class="m2"><p>شد این خرابه دو در، سرای کاروان تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه آسمان حقیرتر ز بیضه ایش در نظر</p></div>
<div class="m2"><p>چو بنگرد به زیر پر حقیر ماکیان تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی به غیر رازدان، ز انبیای انس و جان</p></div>
<div class="m2"><p>چه اهل سرّ، چه غیب دان نگشته رازدان تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به محفل وجود جان، به عرش و لوح اختران</p></div>
<div class="m2"><p>رسد جلای نورهان ز روی مهرسان تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو شاه لشکر قدم، تو ماه کشور کرم</p></div>
<div class="m2"><p>شهنشهان محترم، گدای بندگان تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شد آشکار در زمن، هزار عیسوی سخن</p></div>
<div class="m2"><p>چو شد به خلق مقترن، روان فزا بیان تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو ای امیر ملک دین، چو جاکنی به پشت زین</p></div>
<div class="m2"><p>لوای چرخ هفتمین، به دوش چاکران تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز مرتع سپهر دون، حمل چو سر کند برون</p></div>
<div class="m2"><p>به آه گوید از درون، من و غم شبان تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آرزو بود ملک، به حسرتند نه فلک</p></div>
<div class="m2"><p>که کاش می شدیم تک، بجای خاکیان تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عطای توست بی مثل، سخای توست بی بدل</p></div>
<div class="m2"><p>که شخص هستی از ازل،‌ کمین گدای خوان تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عدو بمیرد از حسد، که بوده ماسوا احد</p></div>
<div class="m2"><p>هم از ازل، هم از ابد، همیشه میهمان تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به وهم و عقل کی سزد، کسی تو را ثنا کند</p></div>
<div class="m2"><p>که فوق عقل کل پرد، عقاب سان کمان تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همیشه تا بود فلک، پر از عبادت ملک</p></div>
<div class="m2"><p>مباد مقترن به شک دل ملازمان تو</p></div></div>