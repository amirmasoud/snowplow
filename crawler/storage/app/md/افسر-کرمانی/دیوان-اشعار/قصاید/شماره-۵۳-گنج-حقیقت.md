---
title: >-
    شمارهٔ ۵۳ - گنج حقیقت
---
# شمارهٔ ۵۳ - گنج حقیقت

<div class="b" id="bn1"><div class="m1"><p>ای ایزد یکتا را، تو مظهر اعظم</p></div>
<div class="m2"><p>وی شیر خدا، قصد حق از آدم و عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جلوه گر از پرده رخت گشت به گیتی</p></div>
<div class="m2"><p>شد گلشن ایجاد ز انوار تو خرّم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ذات تو در کون و مکان اول و آخر</p></div>
<div class="m2"><p>و ای شخص تو در عالم ایجاد مقدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشتاق لقایت همه، چه پیر و چه برنا</p></div>
<div class="m2"><p>محتاج عطایت همه مضطر و منعّم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون طعمه به خرطوم کشد پیل فلک را</p></div>
<div class="m2"><p>بگشاید اگر پشه کوی تو پر از هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دفتر ایجاد بود نام تو عنوان</p></div>
<div class="m2"><p>آری شود آن ختم بدین نام مفخّم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن گنج حقیقت که نهان بود عیان شد</p></div>
<div class="m2"><p>چون نور تو تابید ز رخساره آدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آب بسی رود برون آید و آتش</p></div>
<div class="m2"><p>روزی اگر افتد نظر قهر تو در یم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بوسه زند بر در ایوان جلالت</p></div>
<div class="m2"><p>مانند کمان است قد چرخ برین خم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خامه صنع تو یکی نقطه فرو ریخت</p></div>
<div class="m2"><p>بر صفحه ایجاد و بشد چرخ معظّم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عرصه ایجاد شب و روز و مه و سال</p></div>
<div class="m2"><p>بر قتل عدوی تو اجل گشته مصمم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردید لوای سیه کفر نگونسار</p></div>
<div class="m2"><p>تا دست تو افراشت در این مرحله پرچم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرتوفکن ار می نشدی نور جمالت</p></div>
<div class="m2"><p>آفاق بدی چون دل اعدای تو مظلم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کی بد طرف افزا و فرحبخش، نبودی</p></div>
<div class="m2"><p>بر کوثر و تسنیم اگر خاک درت ضم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گردند اگر منکر تو خلق دو گیتی</p></div>
<div class="m2"><p>یک قطره ای از بحر جلالت نشود کم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>موجود نگشتی ز عدم گر نفتادی</p></div>
<div class="m2"><p>عکسی ز جمال تو به مرآت دو عالم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبود عجب از فرش کنی عرش برین را</p></div>
<div class="m2"><p>باشد به سر انگشت تو چون گردش خاتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با اهل ضلالت به جهان رأی منیرت</p></div>
<div class="m2"><p>آن کرده که کردی رخ خورشید به شبنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز تو ز تو نبود به جهان هیچکس آگاه</p></div>
<div class="m2"><p>کس نیست بر اسرار تو چون شخص تو محرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر اوج مدیحت نرسد طایر اوهام</p></div>
<div class="m2"><p>آری نتوان رفت بر افلاک به سُلّم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس سجده به درگاه تو بنمود و ز خورشید</p></div>
<div class="m2"><p>پیشانی چرخ است بدین داغ مسوّم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای جان جهان خرم و خوشدل من از آنم</p></div>
<div class="m2"><p>کز سرو قدت گلشن بختم شده خرّم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا هست به آفاق نشان از غم و شادی</p></div>
<div class="m2"><p>تا هست به گیتی اثر از خرّمی و غم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با شادی و عشرت دل احباب تو مقرون</p></div>
<div class="m2"><p>با محنت و غم، خاطر اعدای تو توأم</p></div></div>