---
title: >-
    شمارهٔ ۲۹ - بهار و یار
---
# شمارهٔ ۲۹ - بهار و یار

<div class="b" id="bn1"><div class="m1"><p>رسید مژده که اینک صباح عید رسید</p></div>
<div class="m2"><p>طرب گزین شده رند شقی و شیخ سعید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاط را شده آماده عارف و عامی</p></div>
<div class="m2"><p>ز بس به باغ صنوبر ستاد و سرو چمید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به طفل غنچه نگر شد مراهق از بس شیر</p></div>
<div class="m2"><p>به مهد برگ ز پستان مام شاخ مکید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین میت را بین که زنده آمد باز</p></div>
<div class="m2"><p>مگر مسیح نسیمش به جیب نفخه دمید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و یا ز فرط لطافت دوباره بر تن آن</p></div>
<div class="m2"><p>شمیم دوست بسان نسیم صبح وزید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چمن سپهر و درخت است ثور و بر شاخش</p></div>
<div class="m2"><p>نگر تو منزل پروین و خانه ناهید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوا چنان طرب انگیز شد که زاهد شهر</p></div>
<div class="m2"><p>فروخت خرقه به پیر مغان و باده خرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخاست از سر سجاده و بطرف چمن</p></div>
<div class="m2"><p>به طاق ابروی ساقی نشست و جام کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من از جدائی جانان به کلبه احزان</p></div>
<div class="m2"><p>سری به جیب تفکر ز ماسوی نومید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که ناگهان بخیال از درم درآمد یار</p></div>
<div class="m2"><p>بسوی کلبه نظر کرد و جانب من دید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه دید، دید یکی مرده از بلای فراق</p></div>
<div class="m2"><p>چه دید، دید یکی مانده در غم جاوید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به ناز و کبر نشست آن گه از تفقد و داد</p></div>
<div class="m2"><p>بدین بلاکش بی دل ز باغ و راغ نوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه گفت؟ گفت مگر چشم و گوشت از هجرم</p></div>
<div class="m2"><p>ندید روی گل و بانگ مرغ را نشنید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که مسکنت شده، ای رند مست کلبه تار</p></div>
<div class="m2"><p>که همدمت شده از هرچه هست فکر وعید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین به پاسخش آوردم این لطیفه نغز</p></div>
<div class="m2"><p>که شد خیال توام گلبن و صنوبر و بید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگو مرا ز بساتین که بی توام جنات</p></div>
<div class="m2"><p>چو دوزخ آمد و آنگه گناهکار عبید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا بدیده نشیند چو نیشتر سبزه</p></div>
<div class="m2"><p>ز بس بپای دلم بی تو خار هجر خلید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس از مکالمه دیدم که این نگار بود</p></div>
<div class="m2"><p>به غمزه الحق جلاد صد هزار شهید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر آفتاب رخش توده توده عنبرتر</p></div>
<div class="m2"><p>ز انقلاب مهش نافه نافه مشک پدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز جور او، بر او داوری همی بردم</p></div>
<div class="m2"><p>که داورش رخ و زلف است بر سیاه و سپید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زهی شهی که بعدلت پلنگ حافظ گور</p></div>
<div class="m2"><p>به دشت حسن توام از چه شیر عشق درید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جیوش پادشهان از چه ملک چند گرفت</p></div>
<div class="m2"><p>سیوف شیردلان از چه فرق خصم کنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>توراست لشکر هندو بگرد کشور روم</p></div>
<div class="m2"><p>توراست صارم ابرو به تارک خورشید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر آفتاب رخت تا هلال ابرو یافت</p></div>
<div class="m2"><p>بسان تیغ ز غیرت قد سپهر خمید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خوشم که چشمه خضرش نهان به جوهر بود</p></div>
<div class="m2"><p>ز تیغ ابرویت ار صید دل بخون غلطید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رخ تو خلد و دهان سلسبیل و لب غنچه</p></div>
<div class="m2"><p>نهان به غنچه کسی سلسبیل را نشنید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به درگه تو سلاطین ملک حسن، غلام</p></div>
<div class="m2"><p>به مکتب تو فلاطون درس عشق، ولید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به اوج وصف خرامت نبرد راه و نجست</p></div>
<div class="m2"><p>اگرچه طایر عقلم هزار سال پرید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به چشمه لب نوشینت آن چنان افسر</p></div>
<div class="m2"><p>در اشتیاق مداوم که روزه دار به عید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر به روی تو لغزید پای دل از خشم</p></div>
<div class="m2"><p>بناز ناز مسوزش که درد هجر کشید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تنم به بستر رنج است مبتلا، که چرا</p></div>
<div class="m2"><p>جدا ز کوی توام کرد روزگار عنید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مدام تا بود از قرب و بعد نام و نشان</p></div>
<div class="m2"><p>همیشه تا بود از هجر و وصل گفت و شنید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا ز هجر و ز وصل تو باد جان و دلی</p></div>
<div class="m2"><p>گهی به درد قریب و گهی ز رنج بعید</p></div></div>