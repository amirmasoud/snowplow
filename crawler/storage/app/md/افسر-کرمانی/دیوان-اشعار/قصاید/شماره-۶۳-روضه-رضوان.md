---
title: >-
    شمارهٔ ۶۳ - روضه رضوان
---
# شمارهٔ ۶۳ - روضه رضوان

<div class="b" id="bn1"><div class="m1"><p>بنال ای خطه یزد و ببال ای ساحت کرمان</p></div>
<div class="m2"><p>که جانت از بدن شد دور، و وارد بر تنت شد جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فیض مقدم شه گشتی ای کرمان بی رونق</p></div>
<div class="m2"><p>به گیتی تا ابد پیرایه بخش روضه رضوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز درد دوری شه آمدی ای یزد با زیور</p></div>
<div class="m2"><p>به دوران دم بدم بر ساکنانت کلبه ویران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شه آمد به کرمان، اهل کرمان جفا دیده</p></div>
<div class="m2"><p>چو بیرون آمد از یزد، اهل یزد از وفا خندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنند از این فرح جان را نثار یکدگر خرم</p></div>
<div class="m2"><p>دهند از این الم خون درون از دیده ها جریان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشا کرمان که غوث اعظم آمد مسکنش در وی</p></div>
<div class="m2"><p>زهی اهلش که دیدند از شرافت چهره یزدان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانبان فلک معبر، خداوند ملک چاکر</p></div>
<div class="m2"><p>شهنشاه جهان پرور خدیو کشور ایمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شها گر نیستی بر مهدی دجال کش نایب،</p></div>
<div class="m2"><p>چه سان پس آمدی سفیانیان در خاک غم پنهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نداری ور به لب بر دوستان گر عیسوی معجز</p></div>
<div class="m2"><p>نداری ور به کف بر دشمنان گر موسوی ثعبان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چسان پس ساختی احیا جهانی را ز فیض دم</p></div>
<div class="m2"><p>چسان پس سوختی جان عدو از حرقت نیران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نامت بر زبان آید به بزم فرقه کافر</p></div>
<div class="m2"><p>چو شخصت در سخط آید به رزم قوم با عصیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آن لحظه نظیر است آن به برق خاطف و خرمن</p></div>
<div class="m2"><p>در آن لمحه شبیه است این به نجم ثاقب و شیطان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نباشد گر وجودت ماه بزم ملت و مذهب</p></div>
<div class="m2"><p>نباشد گر ظهورت آفتاب کشور ایمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخوشد دشمن دین از ظهورت از چه چون شبنم</p></div>
<div class="m2"><p>بپاشد منکر حق از وجودت از چه چون کتّان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر سو بنگرم از دشمنان آید به ملک دل</p></div>
<div class="m2"><p>بهر جا بگذرم از دوستان آید به گوش جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ای کاش آمدی ما را به گیتی وصل او حاصل</p></div>
<div class="m2"><p>که ای کاش آمدی ما را به عالم دردها درمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همانا جنت است امروز بر یاران جان پرور</p></div>
<div class="m2"><p>همانا دوزخ است امروز بر اعدای دل بریان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که بینم یاورت را هر زمان با لعل پر خنده</p></div>
<div class="m2"><p>که بینم منکرت را هر نفس با دیده گریان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهنشاها اگر قهرت نباشد ظلمت دوزخ</p></div>
<div class="m2"><p>خداوندا اگر مهرت نباشد چشمه حیوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز قهرت از چه رو قلب عدو شد تار چون شبنم</p></div>
<div class="m2"><p>ز مهرت از چه ره دیدند یاران عمر جاویدان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وجودت بر عدو ماننده آب است بر ناخوش</p></div>
<div class="m2"><p>ظهورت بر محب باشنده آب است بر عطشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان از صوت منحو سان پر از غوغای واشمرا</p></div>
<div class="m2"><p>زمان از بانگ منکوسان پر از آواز یا عثمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی گویند فریاد و امان از قهر هفت اختر</p></div>
<div class="m2"><p>همی گویند افسوس و فغان از جور چار ارکان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مداوا کی شود درد دل این قوم کین پرور</p></div>
<div class="m2"><p>که می جویند از شیطان دوا، بر درد بی درمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی سازند اجل را هر زمان بر خویشتن حاضر</p></div>
<div class="m2"><p>که از خوفت درآیند از لباس زندگی عریان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی گویند در بال دجاج نیستی هر دم</p></div>
<div class="m2"><p>نهان می آمدیم ای کاش در این فتنه چون فرخان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود هر شام تا روی جهان تار از ظلام شب</p></div>
<div class="m2"><p>شود هر صبح تا خورشید رخشان از افق تابان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو را شام محبان باد چون صبح فرح روشن</p></div>
<div class="m2"><p>تو را صبح عدویان باد چون شام الم قطران</p></div></div>