---
title: >-
    شمارهٔ ۶۱ - شاه اورنگ امامت
---
# شمارهٔ ۶۱ - شاه اورنگ امامت

<div class="b" id="bn1"><div class="m1"><p>دی به تأدیبم ادیبی نکته سنج و نکته دان</p></div>
<div class="m2"><p>هی همی گفتا: زهی از عقل و دانش بی نشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند با نیرنگ و دستان، روز و شب باشی قرین</p></div>
<div class="m2"><p>چند از فرهنگ و دانش، گاه و گه جویی کران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لختی از دانش نظر بگشا به کار این سپهر</p></div>
<div class="m2"><p>لمحی از غیرت نگه بنما به وضع این جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاین جهان را از چه بنهادند بنیاد اینچنین</p></div>
<div class="m2"><p>کاین فلک را از چه بنمودند بنیان آنچنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مختلف اضداد را بنگر که باهم مقترن</p></div>
<div class="m2"><p>منفصل اشیاء را بنگر که با هم توأمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه جمادی جانب ملک نباتی رهسپر</p></div>
<div class="m2"><p>گه نباتی جانب اقلیم حیوانی روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه این بر تختگاه حشمت آن تکیه زن</p></div>
<div class="m2"><p>گاه آن در شهر بند ملکت این حکمران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جنس ها را بین که هریک جسته از دیگر فرار</p></div>
<div class="m2"><p>بین تو ضدها‌ را، که هریک کرده با دیگر قران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر این آثار قدرت از که در عالم پدید؟</p></div>
<div class="m2"><p>آخر این انوار رحمت از که در گیتی عیان؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این تماثیل شگفت از کیست بی سعی قلم؟</p></div>
<div class="m2"><p>این تصاویر شگرف از کیست بی رنج بنان؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این همه نقش نوادر را، که باشد مخترع؟</p></div>
<div class="m2"><p>این همه شکل بدایع را که باشد ترجمان؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آخر این آثار هستی،‌ خود که را باشد دلیل؟</p></div>
<div class="m2"><p>آخر این اسرار معنی، خود که را باشد نشان؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کیست آن صانع، که صنعش آدمی را از نخست،</p></div>
<div class="m2"><p>تعبیت کرده است در تاریک تن روشن روان؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کیست آن دانا، که عزمش هرکجا رازی نهفت</p></div>
<div class="m2"><p>بی تکلف داند اندر سینه هر رازدان؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کیست این بنا، که سعیش بر فراز این زمین،</p></div>
<div class="m2"><p>کرده بر پا این مقرنس طارم زنگار سان؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کیست آن قادر، که از نهماری قدرت کند،</p></div>
<div class="m2"><p>خار را گل، خاره را گوهر، گیا را پرنیان؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کیست آن منعم، که از انعام بی پایان خویش</p></div>
<div class="m2"><p>مور را در صخره صمّا بود روزی رسان؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با چنین قادر، الا تا چند رنج از عمر و زید؟</p></div>
<div class="m2"><p>با چنین منعم، هلا تا چند جور از این و آن؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چند خدمت ها کنی بر هر کجا ژاژی دنی؟</p></div>
<div class="m2"><p>چند منت ها بری از هرچه شومی قلتبان؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چند سختی ها کشی از بهر جمع سیم و زر؟</p></div>
<div class="m2"><p>چند تلخی ها چشی از بهر پاس آب و نان؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چند هر ناچیز را باشی الا، مدحت سرای</p></div>
<div class="m2"><p>چند هر بی اصل را باشی هلا، توصیف خوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مردمی را چون تو الحق کس ندیدم بی نصیب</p></div>
<div class="m2"><p>بخردی را چون تو بالله کس نجستم بی نشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خالی از تدبیر و دانش، عاری از تشریف عقل</p></div>
<div class="m2"><p>فارغ از انوار هستی غافل از اسرار جان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از خردمندی جدا و با تبهکاری قرین</p></div>
<div class="m2"><p>از ذکاوت برکنار و با سفاهت توأمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جلوه روی بتانت روز و شب اندر نظر</p></div>
<div class="m2"><p>وصف جعد دلبرانت گاه و بیگه بر زبان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چهره این را مثال آری گهی از یاسمین</p></div>
<div class="m2"><p>طرّه آن را صفت خوانی گهی از ضیمران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گاه خوانی غمزه آن را خدنگی دلنشین</p></div>
<div class="m2"><p>گاه خوانی مژّه این را سنانی جان ستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گه به چهره اشک ریزی بهر جعدی مشک ریز</p></div>
<div class="m2"><p>گه ز دیده خون فشانی بهر چهری خوی فشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه بنالی سخت سخت از عشق یاری مهرکیش</p></div>
<div class="m2"><p>گه بگریی زار زار از هجر ماهی مهربان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ناله ای چون ناله حبلی بگاه وضع حمل</p></div>
<div class="m2"><p>گریه ای چون گریه مینا به بزم میکشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لحظه ای از عشق آن رانی به گردون صد نفیر</p></div>
<div class="m2"><p>لمحه ای از هجر آن رانی به انجم صد فغان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گه کنی مدح فلان میر و گه از بهمان وزیر</p></div>
<div class="m2"><p>گه سرایی مدح این و گه سگالی وصف آن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گاه گاه از ناصر خسرو کنی هر سو سخن</p></div>
<div class="m2"><p>گه گه از مسعود سعد آری به هرجا داستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لختی از معروف کرخی، قصه ها سازی حدیث</p></div>
<div class="m2"><p>گاهی از ذوالنون مصری فضل ها سازی بیان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هی همی رانی حدیث از فضل ابدال و رجال</p></div>
<div class="m2"><p>هی همی گویی سخن در وصف بهمان و فلان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>معرفت ها خام بتراشی برای صید خلق</p></div>
<div class="m2"><p>نردهای باژگون بازی به اغوای کسان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ننگ ها را فخرها پنداری از خوی دژم</p></div>
<div class="m2"><p>لعل ها را سنگ ها بشماری از طبع هوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طایر جان را که باشد ذروه گردون مطار</p></div>
<div class="m2"><p>مرغ هستی را که اوج سدره باشد آشیان،</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هشته ای بر پایش از شهوات بندی بس قوی</p></div>
<div class="m2"><p>بسته ای بر بالش از عادات سنگی بس گران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بند آز از پای بگسل مرغ جان را تا همی</p></div>
<div class="m2"><p>بنگری طیران آن را بر فراز لامکان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>لذت روح ار تو را باید رها کن خوی نفس</p></div>
<div class="m2"><p>دل ز اول بگسل از این تا بپیوندی به آن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تخت بنهادن اگر خواهی به ملک عافیت</p></div>
<div class="m2"><p>رخت بیرون کش از این ویران سرای خاکدان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>امنیت را خود چه جویی در دیار آب و گل</p></div>
<div class="m2"><p>شو به ملک جان و دل، کانجا بود حصن امان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مردمی را مایلی گر در دو گیتی ز این سپس</p></div>
<div class="m2"><p>جز ثنای شاه بر لب هیچگه حرفی مران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شاه اورنگ امامت آن که کمتر چاکرش</p></div>
<div class="m2"><p>ملک هستی را گرفت از باختر تا خاوران</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خسرو عالم مهینه دادخواه راستین</p></div>
<div class="m2"><p>مفخر آدم بهینه پادشاه راستان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حامی شرع پیمبر وارث نوح و خلیل</p></div>
<div class="m2"><p>پشت دار دین احمد، مهدی صاحب زمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آن که فیض عام او در داده هرکس را صلا</p></div>
<div class="m2"><p>بر بساط آفرینش تا کفش گسترده خوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>قلزم توحید را عزمش مهین زورق سپار</p></div>
<div class="m2"><p>زورق تسدید را حزمش بهینه بادبان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نزد رای او بود یکسان چه سر و چه علن</p></div>
<div class="m2"><p>در ضمیر او بود روشن چه پیدا چه نهان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گوی سان زآن رو به گرد خود همی گردد سپهر</p></div>
<div class="m2"><p>کامد او را بر بتارک لطمه ای از صولجان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قهر او سوزان شرار و دوزخ او را التهاب</p></div>
<div class="m2"><p>مهر او خرّم بهار و جنّت او را بوستان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>روز کین کز نعره شیر اوژنان کارزار</p></div>
<div class="m2"><p>پهنه هیجا به دشت ارژن آید سخره خوان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نای رومی از دو سو بنیاد سازد زیر و بم</p></div>
<div class="m2"><p>کوس حربی از دو جانب سر کند بانگ فغان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خاک اندر اهتزاز آید ز غوغای نبرد</p></div>
<div class="m2"><p>چرخ، اندر اعتراض آید ز هیهای یلان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پر ز آشوب دلیران باختر تا باختر</p></div>
<div class="m2"><p>پر ز غوغای هژبران قیروان تا قیروان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یک طرف غلطان سری بینی به خون اندر خضاب</p></div>
<div class="m2"><p>یک طرف پرخون تنی بینی به خاک اندر تپان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در کشاکش یک طرف قومی به قومی در ستیز</p></div>
<div class="m2"><p>در تکاپو یک کنف برخی به برخی توأمان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نوک پیکان یلان خونریز چون مژگان یار</p></div>
<div class="m2"><p>خام پر خمّ گوان پرتاب چون زلف بتان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پردلان را خانه زین غیرت تخت قباد</p></div>
<div class="m2"><p>سرکشان را خود زرین خجلت تاج کیان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کاخ گردون پر شود از های و هوی اهل رزم</p></div>
<div class="m2"><p>گوش کیوان کر شود از گیر و دار سرکشان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>عرصه هیجا پر از شیر و پلنگ آید به چشم</p></div>
<div class="m2"><p>از پلنگین صولتان رزم و شیراوژن یلان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پهنه کین در نظر آید پر از افعی و مار</p></div>
<div class="m2"><p>از افاعی تیرها و از زه ماران کمان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز آتش تیغش شراری گر فتد آنگه به خصم</p></div>
<div class="m2"><p>خصم را یکباره خیزد دود مرگ از دودمان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شعله ای از تیغ او بر هرکه تابد تا ابد</p></div>
<div class="m2"><p>بانگ ویلک ویلکش خیزد همی از استخوان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اوست گویی خود سرافیل قیامتگاه رزم</p></div>
<div class="m2"><p>کز ظهورش قالب اعدا کند بدرود جان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بسکه تیغ او فشاند خون خصمان در دغا</p></div>
<div class="m2"><p>بر زمین گویی همی شنگرف بارد زآسمان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای پناه خلق ای دست خدا، ای پشت دین</p></div>
<div class="m2"><p>ای یدالله فوق ایدیهم تو را در خورد و شأن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دردها دارم به دل ناگفته از جور سپهر</p></div>
<div class="m2"><p>رنج ها دارم به جان بنهفته از دور زمان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هم مگر لطف عمیم تو دهد بهبود این</p></div>
<div class="m2"><p>هم مگر خوی کریم تو شود داروی آن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مدح ها گفتیم و کس از ما نپذیرفتی به هیچ</p></div>
<div class="m2"><p>وصفها کردیم و کس از ما نبودی شایگان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هم در این عید از تو امیدم که بخشی مرمرا</p></div>
<div class="m2"><p>نغز تشریفی کز آن گردم به گیتی شادمان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>وآرزویم آنکه زین پس گر زیم در روزگار</p></div>
<div class="m2"><p>هم ز عون تو بود بی منّت اهل زمان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تا بود از کفر و دین در عرصه گیتی اثر</p></div>
<div class="m2"><p>تا بود از روز و شب در حیز عالم نشان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>باد احبابت همه با عیش و با عشرت قرین</p></div>
<div class="m2"><p>باد اعدایت، همه با رنج و محنت توأمان</p></div></div>