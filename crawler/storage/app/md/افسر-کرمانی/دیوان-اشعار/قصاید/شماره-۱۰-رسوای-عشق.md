---
title: >-
    شمارهٔ ۱۰ - رسوای عشق
---
# شمارهٔ ۱۰ - رسوای عشق

<div class="b" id="bn1"><div class="m1"><p>بامدادان قاصدی از کوی یار آمد مرا</p></div>
<div class="m2"><p>قاصدی فرخنده ز آن فرخ دیار آمد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان همی کردم فدا در راه آن فرخنده پیک</p></div>
<div class="m2"><p>کز ورودش جانی از نو مستعار آمد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبرویم رفت گر در عاشقی از کف چه غم</p></div>
<div class="m2"><p>زاشک چشم، آبی ز نو بر روی کار آمد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور گردون شد ز راز سر به مهرم پرده در</p></div>
<div class="m2"><p>تا که در دل مهر ماهی پرده دار آمد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاش وصلش هم شدی در طی هجران پایمرد</p></div>
<div class="m2"><p>آن که در هر ورطه عشقش، دستیار آمد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدعی کش لاف مردی بود و کذب عاشقی</p></div>
<div class="m2"><p>در نبرد عشق او در زینهار آمد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشه زلفش که دارم دانه های اشک از آن</p></div>
<div class="m2"><p>ز آن همی بر خرمن هستی شرار آمد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رشته مهرش دهد پیوند کالای روان</p></div>
<div class="m2"><p>خود جدا از یکدگر، گر پود و تار آمد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر شدم رسوای عشق آخر شدم مقبول دوست</p></div>
<div class="m2"><p>حبذا رسوائیی کو اعتبار آمد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانی افسر ناهنرمندان چرا عیبم کنند،</p></div>
<div class="m2"><p>کاندر این دوران، هنرمندی شعار آمد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردمی آموختم تا پا بیفشردم به عشق</p></div>
<div class="m2"><p>پخته گشتم تا که در آتش قرار آمد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوشه زلفش که دامانم از او پر دانه هاست</p></div>
<div class="m2"><p>وه که ز او هر دم به خرمن صد شرار آمد مرا</p></div></div>