---
title: >-
    شمارهٔ ۴۲ - عروس گلزار
---
# شمارهٔ ۴۲ - عروس گلزار

<div class="b" id="bn1"><div class="m1"><p>باز پیرانه سر از باد بهار</p></div>
<div class="m2"><p>یافت پیرایه عروس گلزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد جوان، نخل کهن را عادت</p></div>
<div class="m2"><p>گشت نو، سرو چمن را رفتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد از ابر همان شیوه پیش</p></div>
<div class="m2"><p>سر زد از باد همان پیشه پار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر را همسفری با خورشید</p></div>
<div class="m2"><p>باد را همنفسی با اسحار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناف گل، نافه آهوی ختن</p></div>
<div class="m2"><p>برگ تر، برقع بانوی تتار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبلان را همه در گل مسکن</p></div>
<div class="m2"><p>قمریان را همه بر سرو قرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برده زنگار به گلشن سبزه</p></div>
<div class="m2"><p>سوده شنجرف به گیتی گلزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عود را عایده از ساعد سرو</p></div>
<div class="m2"><p>تاک را کبکبه از دوش چنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب حیوان همه در چشمه کوه</p></div>
<div class="m2"><p>آتش گل همه در خرمن خار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاله از رنگ چو روی دلبر</p></div>
<div class="m2"><p>غنچه دلتنگ چو لعل دلدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابر را گریه پارینه عمل</p></div>
<div class="m2"><p>باد را جنبش دیرینه شعار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قلم صنع به دیوار چمن</p></div>
<div class="m2"><p>کلک تقدیر، در ایوان بهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن لب لاله کشید از شنجرف</p></div>
<div class="m2"><p>و آن خط سبزه نوشت از زنگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برهمن وار، براهیم چمن</p></div>
<div class="m2"><p>آزر آثار، خلیل آزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بت تراشیش به گلشن پیشه</p></div>
<div class="m2"><p>بت فروشیش به بستان هنجار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نای بلبل، چو نی موسیقی</p></div>
<div class="m2"><p>جان قمری، چو لب موسیقار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن به فریاد، ز بد عهدی گل</p></div>
<div class="m2"><p>این در افغان ز دل آزاری خار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دوش شد رهبر من، سوی چمن</p></div>
<div class="m2"><p>ناله قمری و فریاد هزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باغ را دیدم از آن سان که ندید،</p></div>
<div class="m2"><p>هیچ دلداده بروی دلدار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چمنی دیدم از انبوهی گل،</p></div>
<div class="m2"><p>گلشنی یافتم از دوری خار</p></div></div>