---
title: >-
    شمارهٔ ۶۹ - شبی شبه سان
---
# شمارهٔ ۶۹ - شبی شبه سان

<div class="b" id="bn1"><div class="m1"><p>شبی چو زلف دلارام مشک فام و شبه سان</p></div>
<div class="m2"><p>بسان خط بتان تار و جعد خوبان تاران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشیده بودم از آسیب دهر پای به دامن</p></div>
<div class="m2"><p>فکنده بودم از اندوه فکر سر به گریبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دیده اشک روانم، روان به دامن صحرا</p></div>
<div class="m2"><p>ز سینه آه درونم دوان به گنبد کیوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هجر عارض جانان نه سینه، غیر گلخن</p></div>
<div class="m2"><p>ز دوری رخ دلبر نه دیده،‌ خجلت عمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار شعله آهم شدی ز سینه برانجم</p></div>
<div class="m2"><p>هزار قطره اشکم، بدی ز دیده به دامان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ناگهم ز در آمد بتی به طرّه سمن سای</p></div>
<div class="m2"><p>که ناگهم به سر آمد مهی، به چهره خوی افشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمونه ایش ز قامت، طراز قامت طوبی</p></div>
<div class="m2"><p>نشانه ایش ز طلعت، فضای روضه رضوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فزوده حیرت خلقی ز چشمکان فتن زا</p></div>
<div class="m2"><p>ربوده طاقت جمعی ز زلفکان پریشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلای خاطر قومی به یک کرشمه دلکش</p></div>
<div class="m2"><p>هلاک جان گروهی به نیم غمزه فتان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم از فسون، دو صدش دل، نگون در آتش عارض</p></div>
<div class="m2"><p>هم از حیل دو صدش جان اسیر چاه زنخدان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ختن ختن همه مشک اندرش به دسته سنبل</p></div>
<div class="m2"><p>یمن یمن همه لعل اندرش به حقه مرجان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زلف بسته همی طبله طبله لادن و عنبر</p></div>
<div class="m2"><p>به خال سوده همی توده، توده غالیه و بان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به تار طرّه مشکین دو صد تتارش مضمر</p></div>
<div class="m2"><p>به چین زلف پریشان هزار چینش پنهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کمر ببسته که در باغ نارون فتد از پای</p></div>
<div class="m2"><p>دهان گشاده که در شهر انگبین شود ارزان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشسته لشکر خطش به گرد صفحه عارض</p></div>
<div class="m2"><p>که دیده مور همی حکمران ملک سلیمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدیدم آن رخ و چیدم هزار خرمن لاله</p></div>
<div class="m2"><p>بدیدم آن خط و بردم هزار دامن ریحان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نمود چهره به سویم، مگر که تا بردم دل</p></div>
<div class="m2"><p>گشود دیده به رویم، مگر که تا ستدم جان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه دید، دید ضعیفی فتاده عاجز و مضطر</p></div>
<div class="m2"><p>چه دید، دید حزینی نشسته واله و حیران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه گفت، گفت که ای در بلا، سکندر آفاق</p></div>
<div class="m2"><p>چه گفت، گفت که ای در ستم، تهمتن دوران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تویی که این همه بار جفا کشیده ز دلبر</p></div>
<div class="m2"><p>تویی که این همه زهر عنا چشیده ز جانان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهاده ای ز چه سر بر فراز بالش فرقت</p></div>
<div class="m2"><p>فکنده ای ز چه تن در نشیب بستر حرمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز چیست نالی چون بلبلان واله و شیدا</p></div>
<div class="m2"><p>ز چیست گریی چون عاشقان کلبه احزان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر به عشوه ای از کف، بتی ربوده تو را دل</p></div>
<div class="m2"><p>مگر به غمزه ای از تن، مهی گرفته تو را جان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به ناله گفتمش ای شرم لاله ات رخ نسرین</p></div>
<div class="m2"><p>به گریه گفتمش، ای رغم غنچه ات لب خندان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو زخم، زخم تو باشد مرا چه حاجت مرهم</p></div>
<div class="m2"><p>چو درد، درد تو باشد مرا چه منت درمان</p></div></div>