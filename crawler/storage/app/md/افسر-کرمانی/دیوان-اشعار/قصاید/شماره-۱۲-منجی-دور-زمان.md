---
title: >-
    شمارهٔ ۱۲ - مُنجی دور زمان
---
# شمارهٔ ۱۲ - مُنجی دور زمان

<div class="b" id="bn1"><div class="m1"><p>ماه من، ای کز رخت دایم بتاب است آفتاب</p></div>
<div class="m2"><p>نور را از رویت اندر اکتساب است آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خرامی بر زمین از ناز و خود گویا ز رشک</p></div>
<div class="m2"><p>هر نفس یا لیتنی کنت تراب است آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ظهور نور حق و ای منجی دور زمان</p></div>
<div class="m2"><p>از فروغ بارگاهت نوریاب است آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لوحش الله کز فروغ شمع ایوانت نهان</p></div>
<div class="m2"><p>هر شب از خجلت در این نیلی ثیات است آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشحه ای از خامه صنعت به چارم آسمان</p></div>
<div class="m2"><p>نقطه ای بر صفحه نیلی کتاب است آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم ز موج بحر اجلالت حباب است آسمان</p></div>
<div class="m2"><p>هم ز تاب تابش کاخت بتاب است آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر دیوار گردون رخت،‌ بگشاده چشم</p></div>
<div class="m2"><p>همچو حربا، کو به سیر آفتاب است آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفعت کاخ جلالت را چه گویم کاندر آن</p></div>
<div class="m2"><p>بیضه ای در سایه پرّ غراب است آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با رخت مه را چه نسبت، ای که با خاک درت</p></div>
<div class="m2"><p>بر بساط چرخ چون نقشی بر آب است آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرنه آسیبش رسید از تیغ تو،‌ پس از چه روی</p></div>
<div class="m2"><p>پیکرش دایم به خون اندر خضاب است آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کند برگرد کویت پاسبانی روز و شب</p></div>
<div class="m2"><p>لرز لرزان دایم اندر اضطراب است آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ز خامی پخته سازد منکرانت را به دهر</p></div>
<div class="m2"><p>از افق هر صبحدم در التهاب است آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا کند سوی تو باز ای مبدأ کل بازگشت</p></div>
<div class="m2"><p>دایم اندر گرد کویت در شتاب است آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهر امید ظهورت ای شه آخر زمان</p></div>
<div class="m2"><p>بر کمیت آسمان پا در رکاب است آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خویش را بنهاده تا در بوته عشقت به مهر</p></div>
<div class="m2"><p>در کف صراف گردون زرّ ناب است آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درجهان قدرتت باشد کنامی نه سپهر</p></div>
<div class="m2"><p>جاگزین در بیشه اش چون شیر غاب است آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زآن میی کز جام تو نوشید در بزم ازل</p></div>
<div class="m2"><p>تا ابد سرگشته و مست و خراب است آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آسمان بر خوان یغمای تو سیمین کاسه ای است</p></div>
<div class="m2"><p>وز یم جودت در آن یک قطره آب است آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرنه از طبع منیرت کرد افسر کسب نور</p></div>
<div class="m2"><p>چون ز شعر روشنش در اکتساب است آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا بگرد مرکز غبرا به سیر است آسمان</p></div>
<div class="m2"><p>تا به بیداری سپهر اندر ذهاب است آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد خصمت زآتش قهر خدا در تاب و تب</p></div>
<div class="m2"><p>بر فراز چرخ تا دایم بتاب است آفتاب</p></div></div>