---
title: >-
    شمارهٔ ۲۷ - اسکندر گل
---
# شمارهٔ ۲۷ - اسکندر گل

<div class="b" id="bn1"><div class="m1"><p>وقت شد کاسکندر گل لشکر آرایی کند</p></div>
<div class="m2"><p>لشکر گل را خدیو باغ دارایی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ را دست صبا، عنبر نهد در آستین</p></div>
<div class="m2"><p>راغ را جیب هوا، مشکین ز بویایی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفتگان بوستان را باد بیداری دهد</p></div>
<div class="m2"><p>تشنگان گلستان را، ابر سقایی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل بیدل ز فکر غنچه سر در پر برد</p></div>
<div class="m2"><p>قمری مسکین به یاد سرو شیدایی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه از میزان یاقوتی زر لعلی کشد</p></div>
<div class="m2"><p>لاله از مکیال لعلی، سیم پیمایی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نسیم کاکل گل، صبح مشک افشان شود</p></div>
<div class="m2"><p>زلف سنبل از شمیم شب سمن سایی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست لاله موسی آسا، آتش طور آورد</p></div>
<div class="m2"><p>طفل سوسن عیسی آیین میل گویایی کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس اندر مسند رز بزم شاهی گسترد</p></div>
<div class="m2"><p>غنچه بر فرش زمرّد باده پیمایی کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابر گوهر بار گردد، باد عنبر بو شود</p></div>
<div class="m2"><p>خاک نسرین خیز آید، گل سمن سایی کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفتاب غنچه سر از مشرق گلبن زند</p></div>
<div class="m2"><p>نرگس آن خورشید را چون ذره حربایی کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل کند در دامن گلچین به جان عندلیب،</p></div>
<div class="m2"><p>آنچه با دلدادگان دلدار هر جایی کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نشاط انگیزی گل، منزل تنگ دلم</p></div>
<div class="m2"><p>پیش مهمانان خیال یار صحرایی کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرنه باغ آمد سپهری تازه، اندر وی چرا،</p></div>
<div class="m2"><p>لاله خورشیدی نماید، گل ثریایی کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا که در پیشانی هندوی بستان زعفران</p></div>
<div class="m2"><p>نرگس اندر هاون زر زغفران سایی کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسکه از گلبن بلند و پست گردد دشت و کوه</p></div>
<div class="m2"><p>دشت کهساری نماید، کوه صحرایی کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نپیچد سر ز فرمان طبیعت نامیه</p></div>
<div class="m2"><p>در مزاج گل طبیعت کارفرمایی کند</p></div></div>