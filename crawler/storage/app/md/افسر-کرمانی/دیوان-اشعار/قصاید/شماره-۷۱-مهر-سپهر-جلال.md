---
title: >-
    شمارهٔ ۷۱ - مهر سپهر جلال
---
# شمارهٔ ۷۱ - مهر سپهر جلال

<div class="b" id="bn1"><div class="m1"><p>تاخت بکاخ حمل، خسرو خور تا عنان</p></div>
<div class="m2"><p>غیر باغ ارم، گشت فضای جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرصه گیتی بهشت، گشت ز اردیبهشت</p></div>
<div class="m2"><p>افسر نخوت بهشت، از سر خود مهرگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از اثر باد دی، شخص جهان بود پیر</p></div>
<div class="m2"><p>نک ز هوای ربیع، گشت دگر ره جوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر مطیر از مطر، ریخته لؤلوی تر</p></div>
<div class="m2"><p>طرف چمن را نگر، کامده عنبر فشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست نسیم صبا، پرده گل بر درید</p></div>
<div class="m2"><p>بلبل بیچاره را، راز نهان شد عیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله شکفت از دمن، چون رخ دلدار من</p></div>
<div class="m2"><p>سبزه دمید از چمن، چون خط سبز بتان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دعوی رضوانیش، هست کشاورز باغ</p></div>
<div class="m2"><p>تا که شد از نوبهار، باغ چو کوی جنان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابر بهاری فشاند بس که گهر بر زمین</p></div>
<div class="m2"><p>همچو صدف غنچه راست، لؤلوی تر در دهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفت چو سلطان گل، بر بزمرّد سریر</p></div>
<div class="m2"><p>سرو،‌ ستادش به پای، بر صفت بندگان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل شده بس دلفریب، برده نک از عندلیب</p></div>
<div class="m2"><p>صبر و قرار و شکیب، طاقت و تاب و توان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سر هر سروبن قمریی اندر نوا</p></div>
<div class="m2"><p>بر رخ هر سرخ گل، بلبلی اندر فغان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای دل نادیده عیش،‌ چند بری بار طیش</p></div>
<div class="m2"><p>فصل بهار است هین، نوبت غم نیست، هان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازچه نشینی خموش، خیز و به عشرت بکوش</p></div>
<div class="m2"><p>باده صافی بنوش، مدح شهنشه بخوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه جهان مرتضی، صفدر خیبرگشا</p></div>
<div class="m2"><p>شافع روز جزا، قاسم نار و جنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهر سپهر جلال، اختر برج کمال</p></div>
<div class="m2"><p>آن که نه او را زوال، هست بکون و مکان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای که به دربار تو، حضرت روح القدس</p></div>
<div class="m2"><p>سبحه به کف هر صباح، آمده تسبیح خوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مختصری بی بها هست به گاه عطا،</p></div>
<div class="m2"><p>خادم کوی تو را، مایه دریا و کان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مطبخی از کوی توست عرصه این روزگار</p></div>
<div class="m2"><p>مهر سپهر اندر او،‌ آمده نار و دخان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روز و شبان در خطر، بود ز گرگ اجل</p></div>
<div class="m2"><p>گله ایجاد را، گر تو نبودی شبان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرنه به صبح ازل مهر رخت برفروخت</p></div>
<div class="m2"><p>تار بدی تا ابد، عرصه کون و مکان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مطبخ کوی تو را، هست تنور این فلک</p></div>
<div class="m2"><p>کش بود از مهر و ماه، پخته و ناپخته نان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پهنه جود تو را، پیک خرد پی سپر</p></div>
<div class="m2"><p>گشته و نابرده ره، عاقبتش بر کران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پرده برافکن ز رخ تا به یقین پی بریم</p></div>
<div class="m2"><p>چند بپوییم ما، بیهده راه گمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بل بفشاند ز نار بر دو جهان آستین</p></div>
<div class="m2"><p>آن که بساید تو را ناصیه برآستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مدح تو فیصل پذیر نبود اگر آورد</p></div>
<div class="m2"><p>منشی دیوان وحی، خامه کروبیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از چه تصور کنم مدح تو را در ضمیر</p></div>
<div class="m2"><p>وز چه تمنا کنم وصف تو را در بیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وصف تو آری کجا، در خور اوهام ماست</p></div>
<div class="m2"><p>وصفت تو نتوان کند همچو منی ناتوان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>طلعت خورشید را چون نگرد مرغ شب</p></div>
<div class="m2"><p>عرصه جبریل را چون گذرد ماکیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا ز سموم خزان هست به عالم اثر</p></div>
<div class="m2"><p>تا ز نسیم بهار هست به گیتی نشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گلشن احباب تو، باد همیشه بهار</p></div>
<div class="m2"><p>کشت امید عدوت، باد سراسر خزان</p></div></div>