---
title: >-
    شمارهٔ ۴۱ - زنده جاوید
---
# شمارهٔ ۴۱ - زنده جاوید

<div class="b" id="bn1"><div class="m1"><p>برده ز جان ها قرار زلف دلاویز یار</p></div>
<div class="m2"><p>زلف دلاویز یار برده ز جان ها قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل بدخشان نگر، نزد لبش چون خزف</p></div>
<div class="m2"><p>مهر درخشان ببین پیش رخش ذره وار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلوه گر آمد رخش تا به گلستان جان</p></div>
<div class="m2"><p>مرغ دلم شد ز شوق، نغمه سرا چون هزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل چه بود تا دهم بهر نگاهی و لیک</p></div>
<div class="m2"><p>جان دهم اندر رهش گر بودم صد هزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیهده گویند خلق، کز پی دلبر مرو</p></div>
<div class="m2"><p>بسته گیسوی دوست، هیچ نخواهد فرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشته شمشیر دوست زنده جاوید شد</p></div>
<div class="m2"><p>بر سرم از دست او تیغ و سنان گو ببار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهره نه بگشود و خلق در طلبش جان دهند</p></div>
<div class="m2"><p>آه اگر بی حجاب چهره کند آشکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز و شب عاشقان روی تو و موی تست</p></div>
<div class="m2"><p>زلف تو مقصد ز لیل، چهر تو آمد نهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راحت جان ها توئی، روح روان ها توئی</p></div>
<div class="m2"><p>ورد زبان ها توئی، مختفی و آشکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده حق بین اگر باز شود در جهان</p></div>
<div class="m2"><p>جز تو نبیند کسی نه بمیان، نی کنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساقی دوران توئی، دارم امید از تو من</p></div>
<div class="m2"><p>خمر خم نیستی چاره رنج خمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا رهم از خویشتن وز تو بگویم سخن</p></div>
<div class="m2"><p>فاش بهر انجمن واله و بی اختیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رشته بیم و امید از تو نخواهم برید</p></div>
<div class="m2"><p>گر بکشی تیغ تیز ور بکشی خوار و زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز تو نپویم دمی کوی نگاری که هست</p></div>
<div class="m2"><p>هجرک بئس القرین وصلک نعم القرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر ثنا گفتنش از چه دلا خائفی</p></div>
<div class="m2"><p>گو بزنندت به سنگ، گو بکشندت به دار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا کند از وصل و هجر چشم و لب عاشقان</p></div>
<div class="m2"><p>خنده چو گل در چمن گریه چو ابر بهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باد لب یاورت خنده زنان همچو گل</p></div>
<div class="m2"><p>باد دل دشمنت خون ز غم روزگار</p></div></div>