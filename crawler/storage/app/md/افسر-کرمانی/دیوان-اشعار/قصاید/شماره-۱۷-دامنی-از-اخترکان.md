---
title: >-
    شمارهٔ ۱۷ - دامنی از اخترکان
---
# شمارهٔ ۱۷ - دامنی از اخترکان

<div class="b" id="bn1"><div class="m1"><p>ای سهی سرو، که هر سوی بنازت گذراست</p></div>
<div class="m2"><p>در گذرگاه توام دیده و دل منتظر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دام دلهاست به رخسار تو، یا حلقه زلف</p></div>
<div class="m2"><p>یا که مار سیه، اندر بر گنج گهر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیم و زر گر پی ایثار توام نیست به کف</p></div>
<div class="m2"><p>اینک اشک و رخم آمیخته چون سیم و زر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده زار ببین، سوز درون را بنگر</p></div>
<div class="m2"><p>که بر اسرار نهان، این دو مرا پرده در است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازدحام سر کوی تو نه امری است شگفت</p></div>
<div class="m2"><p>انگبین هر چه بود بیش، مگس بیشتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده بگشا ز رخ، ای شاهد دیرین که مرا،</p></div>
<div class="m2"><p>دیده و دل به تماشای رخت منتظر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرمرا ز آرزوی روز وصالت همه شب</p></div>
<div class="m2"><p>جاری از دیده و دل قطره خون تا سحر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیستت داعیه امر نبوت ز چه روی،</p></div>
<div class="m2"><p>به رخ از خنده تو را معجز شق القمر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پاس ما هیچ نداری و ندانی که همی،</p></div>
<div class="m2"><p>خرمن حسن تو را، آه دل ما شرر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه من شیفته دل واله رخسار توام</p></div>
<div class="m2"><p>که ز رخسار تو بر من، دگری جلوه گر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن که هر صبح و مسا، حضرت روح القدسش</p></div>
<div class="m2"><p>بر در بارگه از روی شرف سجده بر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داور کشور جان، مهدی قائم، که جز او</p></div>
<div class="m2"><p>خلق را از دو جهان یکسره قطع نظر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای که در دایره بارگه رفعت او</p></div>
<div class="m2"><p>چو یکی نقطه موهوم، فلک مستتر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وه که گر طایری از کوی تو پرواز کند</p></div>
<div class="m2"><p>عرصه کون و مکانش همه در زیر پر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود شگفت آیدم از خلق که بی پرده تو را</p></div>
<div class="m2"><p>می ببینند و بگویند که در پرده در است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست مستور جمال تو، ولی نتواند،</p></div>
<div class="m2"><p>بیندت آن که نه اندر دو جهان با بصر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن که شد با خبر از تو خبر از هیچش نیست</p></div>
<div class="m2"><p>وآنکه دارد خبر از خویش ز تو بی خبر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عجب این است که دورم ز تو هرچند به من</p></div>
<div class="m2"><p>از من شیفته دل، فیض تو نزدیکتر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نزد خورشید رخت بر سر دیوار سپهر</p></div>
<div class="m2"><p>همچو حربا متحیر به شب و روز خور است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهر پیدایی ذات تو چه جوییم دلیل</p></div>
<div class="m2"><p>که نه جز جلوه رخسار تو در بحر و بر است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تویی آن پادشه ملک که از فرّ و جلال،</p></div>
<div class="m2"><p>هستی از خوان عطای تو یکی ریزه خور است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از چه هر سو نگرم روی تو آید به نظر،</p></div>
<div class="m2"><p>گرنه رخسار تو از شش جهتم جلوه گر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور گذشتی به نخستین قدم از وادی لا،</p></div>
<div class="m2"><p>حالیت مصطبه کشور اِلاّ مقر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیست چیزی که بود در دو جهان از تو نهان</p></div>
<div class="m2"><p>ذره اندر بر خورشید چسان مستتر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جوهر ذات تو بیرون بود از چون و چرا</p></div>
<div class="m2"><p>وآنچه آید به تصور ز تو نقش صور است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در بر بحر کف جود تو دریای وجود</p></div>
<div class="m2"><p>غوص کردیم بسی قصه بحر و شمر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مهر روی تو شود یکدم اگر ملک فروز</p></div>
<div class="m2"><p>آفتابش چو یکی ذره نهان از نظر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چهره بنمای که از بهر نثارت همه شب</p></div>
<div class="m2"><p>چرخ را دامنی از اخترکان پرگهر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من و اندیشه ذات تو کجا، زآن که بری</p></div>
<div class="m2"><p>ذات پاکت ز چه و چونی و بوک و مگر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرمرا گشته ز مدح تو زبان قاصر از آنک</p></div>
<div class="m2"><p>که ثنایت هم از اندیشه اوهام بر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حلقه بندگیت را نکشد هر که به گوش،</p></div>
<div class="m2"><p>دایم از دشنه جلاد قضا در خطر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر که را در دل و جان نائره عشق نیست،</p></div>
<div class="m2"><p>هست پیدا که در این دایره چون گاو و خر است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن که در چنبر عشقت ننهد گردن طوع</p></div>
<div class="m2"><p>خونش در مذهب هفتاد و دو ملت هدر است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>داد دل را ز چه اندر برت اظهار کنم</p></div>
<div class="m2"><p>ای که از حال دل سوختگانت خبر است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرچه ما غرقه دریای گناهیم ولی،</p></div>
<div class="m2"><p>لطف عام تو برون از حد و احصا و مر است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگرم زنده کنی ور بکشی سلطانی</p></div>
<div class="m2"><p>عاشق آن نیست که اندر پی نفع و ضرر است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جز به درگاه توام نی بدری روی نیاز</p></div>
<div class="m2"><p>خاک اگر بستر و ور خاره مرا زیر سر است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سر نهادیم به پای تو و پا بر سر چرخ</p></div>
<div class="m2"><p>تا نگویند که هر عاشق، بی پا و سر است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا در این بادیه از هجر تو نام است و نشان</p></div>
<div class="m2"><p>تا در این دایره از مهر جمالت اثر است</p></div></div>