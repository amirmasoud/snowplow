---
title: >-
    شمارهٔ ۶۵ - با کاروان نور
---
# شمارهٔ ۶۵ - با کاروان نور

<div class="b" id="bn1"><div class="m1"><p>فرو ریزم از دوری روی جانان</p></div>
<div class="m2"><p>ز سرچشمه چشم، سیل فراوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرو ریزیش، لیک نی قطره قطره</p></div>
<div class="m2"><p>چو از ابر ریزنده، باران نیسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرو ریزیش کامد از نیم رشحش</p></div>
<div class="m2"><p>یمین و یسارم، دو موّج عمّان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب را که با اینچنین موج دارم</p></div>
<div class="m2"><p>تنی پای تا سر، در احراق نیران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی یاد دارم که در کنج خلوت</p></div>
<div class="m2"><p>سری برده بودم فرو در گریبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه دستگیرم خیالات دلبر</p></div>
<div class="m2"><p>همه پایمردم سخن های جانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بحر تفکر رسیدم به برّی</p></div>
<div class="m2"><p>که بد عالمش حلقه ای در بیابان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه خاک او غیرت مشک اذفر</p></div>
<div class="m2"><p>همه ریگ او حسرت درّ و مرجان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرازش همه توده های زبرجد</p></div>
<div class="m2"><p>نشیبش همه لعل های بدخشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن دشت دیدم روان کاروانی</p></div>
<div class="m2"><p>که چون رهروانش نپرورده دوران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر توسنی دلبری روی انور</p></div>
<div class="m2"><p>بهر هودجی مهوشی موی قطران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه بر ستوران سوار و پیاده</p></div>
<div class="m2"><p>همه بر لب جوی تسنیم، عطشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه دهر پیما، ولی پا برهنه</p></div>
<div class="m2"><p>همه ستر بخشا ولی جمله عریان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تنی چند لیکن به جان ها معانق</p></div>
<div class="m2"><p>مهی چند لیکن ز یک خور درخشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه پادشاهان در ملک، ملت</p></div>
<div class="m2"><p>همه شهریاران در شهر ایمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن قوم آتش دل و باد جنبش</p></div>
<div class="m2"><p>بگرداب حیرت من خاک بنیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که اینان چه جمعند، این گونه عاجل</p></div>
<div class="m2"><p>که اینان چه قومند، این سان شتابان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بتأیید عقل و به تسدید دانش</p></div>
<div class="m2"><p>به درک آمدم عاقبت کاین وشاقان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیابان نوردند منزل به منزل</p></div>
<div class="m2"><p>که شاید ببوسند دربار سلطان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>الا ای منیری که از عکس نورت</p></div>
<div class="m2"><p>هویدا به کیهانیان روی یزدان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگرچه بر ابطال قومی مشعبد</p></div>
<div class="m2"><p>عصا اژدر آورده موسی بن عمران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو را کلک معجز نگار است بر کف</p></div>
<div class="m2"><p>براین قوم چون دست موسی و ثعبان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو را نجم رخسار و چشم مخالف</p></div>
<div class="m2"><p>به تمثیل مانا، شهاب است و شیطان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو را نام در نامه آفرینش</p></div>
<div class="m2"><p>چو نام خدا بر سر نامه عنوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو را لفظ معجز بیان در حقیقت</p></div>
<div class="m2"><p>کلید در گنج اسرار قرآن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو در دشت لفظ از پی صید معنی</p></div>
<div class="m2"><p>در آری سمند سخن را به جولان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرو ماندش عقل در گام اوّل</p></div>
<div class="m2"><p>چو از سرعت عقل اجساد بیجان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو را صولجان عزم و گو هفت گردون</p></div>
<div class="m2"><p>قضا پنجه و لامکان سطح میدان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شد از مزرعت حبّه ای رزق موری</p></div>
<div class="m2"><p>خلایق بر آن مور تا حشر مهمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چرا تا نبوسید خور، خاک پایت</p></div>
<div class="m2"><p>به جان آتش افتادش از داغ حرمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>و از این اضطراب است هر صبح تا شب</p></div>
<div class="m2"><p>ز مشرق به مغرب روان زد و لرزان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>الا ای خدیوی که نه کاخ علیا</p></div>
<div class="m2"><p>یکت پله از آستان نگهبان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شنیدم که از کبر قومی سیه دل</p></div>
<div class="m2"><p>ز یک جنس در ذات با جان بن جان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گروهی که ابلیسشان در شقاوت</p></div>
<div class="m2"><p>به مکتب سرا کم ز طفل دبستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گروهی که بوجهلشان درجهالت</p></div>
<div class="m2"><p>در عجز گویان به تسلیم و اذعان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گروهی که فرعونشان در تکبر</p></div>
<div class="m2"><p>به دربار نخوت یکی عبد فرمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی محفل آراستند از شیاطین</p></div>
<div class="m2"><p>نه شیطان جان، بلکه اشباه انسان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نموده همه با عصا زهر توام</p></div>
<div class="m2"><p>نموده همه درعبا تیغ پنهان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو برقصد شبل علی، کور موصل</p></div>
<div class="m2"><p>چو بر قتل صهر نبی، شرک شیطان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس آن قوم بی ننگ و بی عار و بی دین</p></div>
<div class="m2"><p>به محفل تو را خوانده پرخاش جویان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شدی وارد ای شه بر آن قوم پرکین</p></div>
<div class="m2"><p>چو بر اهرمن از کرامت سلیمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بلی مهر از فیض عامی که دارد</p></div>
<div class="m2"><p>بتابد به حربا و خفاش یکسان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نگویم چه گفتند آن قوم ابکم</p></div>
<div class="m2"><p>که لعنت ابر قول و بر ذات ایشان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به پاداش گفتار آن فرقه دون</p></div>
<div class="m2"><p>که نشناخته بر که بستند بهتان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر ایشان گروهی عجب شد مسلط</p></div>
<div class="m2"><p>نه رحمی بر اموالشان کرد و نه جان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گروهی ز کُفّار از رحم عاری</p></div>
<div class="m2"><p>نهاده به فجار شمشیر برّان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به هامون روان گشت هر لحظه آمون</p></div>
<div class="m2"><p>ز بس جوی خون گشت جاری ز شریان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نبردند از آن بحر زورق به ساحل</p></div>
<div class="m2"><p>نگشتند از آن لجه ایمن ز طوفان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مگر هر که بگریخت اندر پناهت</p></div>
<div class="m2"><p>شد ایمن که کعبه است ایمن ز حدثان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جهانبان، خدیوا، الا ای که آمد</p></div>
<div class="m2"><p>پناه تو ملجأ گبر و مسلمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گریزد به حصن ولای تو افسر</p></div>
<div class="m2"><p>هم از کید نفس و هم از شر شیطان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>الا تا شررخیز شد نار دوزخ</p></div>
<div class="m2"><p>الا تا فرحبخش شد باغ رضوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مُحبّ تو را باد نعمت مؤبد</p></div>
<div class="m2"><p>عدوی تو را باد نقمت فراوان</p></div></div>