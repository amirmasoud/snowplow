---
title: >-
    شمارهٔ ۶۸ - ای زلف بتم
---
# شمارهٔ ۶۸ - ای زلف بتم

<div class="b" id="bn1"><div class="m1"><p>زلف بتم، ای جادوی حیلت گر فتان</p></div>
<div class="m2"><p>ای ابن عم غالیه، ای نوبوه بان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای هر شکنت دامی، خم در خم و پرچین</p></div>
<div class="m2"><p>و ای هرگرهت خامی، پرحلقه و پیچان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظاهر همه دود استی و باطن همه شعله</p></div>
<div class="m2"><p>صورت همه کفر استی و سیرت همه ایمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خم گشته چو قربانی و پرحلقه چو فتراک</p></div>
<div class="m2"><p>ببریده چو پیوندی و بشکسته چو پیمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر خمّی و درهمّی و مرغولی و مفتول</p></div>
<div class="m2"><p>تاریکی و باریکی و مجموع و پریشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم دامی و هم بندی و هم چینی و چنبر</p></div>
<div class="m2"><p>هم عودی و هم مشکی و هم غالیه هم بان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیاری و طرّاری و سحّاری و مکّار</p></div>
<div class="m2"><p>صیادی و شیادی و محتالی و فتان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یغما چی ملکی تو و برهم زن اقلیم</p></div>
<div class="m2"><p>آشوب جهانی تو و اعجوبه دوران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای زلف چه جنسی، که نه از دوده انسی</p></div>
<div class="m2"><p>هر چیز که خوانیم نه این استی و نه آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عودی تو و جایت همه بر تارک مجمر</p></div>
<div class="m2"><p>دودی تو و کارت همه با شعله نیران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زاغی و همی پرّی در ساحت گلشن</p></div>
<div class="m2"><p>ماری و همی باشی بر گنج نگهبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کویت چو سمندر همه بر توده آذر</p></div>
<div class="m2"><p>کارت چو سکندر همه با چشمه حیوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آهویی و در مرتع خطت همه پویه</p></div>
<div class="m2"><p>طاووسی و در گلشن رویت همه طیران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هندویی و خوی تو بود گرده آتش</p></div>
<div class="m2"><p>جادویی و کار تو بود حیله و دستان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر دوش بتم پا نهی ای بی ادب آخر</p></div>
<div class="m2"><p>هشدار، که آیین ادب نبود ایشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلها تو ربایی بت من،‌ متهم خلق</p></div>
<div class="m2"><p>جانها تو فریبی مه من، شهره دوران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر لمعه نورش بکشی پرده ظلمت</p></div>
<div class="m2"><p>بر گوی بلورش بسپاری خم چوگان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گاهی ز گریبانش کنی عزم سر دوش</p></div>
<div class="m2"><p>گاهی ز سر دوشش، آهنگ گریبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنبر بشوی گاه و بپیچیش به گردن</p></div>
<div class="m2"><p>عنبر بشوی گاه و بریزیش به دامان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه از لبکانش بخوری قند مکرر</p></div>
<div class="m2"><p>گه از رخکانش بچنی لاله نعمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همواره در آغوش بتم خُسبی و نبود</p></div>
<div class="m2"><p>بیمت ز شه عادل دریا دل ایمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اکلیل همم فُلک عطا، مجمع اجلال</p></div>
<div class="m2"><p>منجوق کرم، ابر سخا، لجه احسان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>احیای روان را، دمش اعجاز مسیحا</p></div>
<div class="m2"><p>انگشتر جان را کفش انگشت سلیمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حرفی ز رضا و غضبش دوزخ و جنت</p></div>
<div class="m2"><p>شرحی ز سخا و سخطش کوثر و نیران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یک پرتوی از شمع رخش نور مه و مهر</p></div>
<div class="m2"><p>یک شمه ای از بذل کفش، نقد یم و کان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دستش نه ببخشد زر، جز خرمن خرمن</p></div>
<div class="m2"><p>کلکش نه بپاشد دُر، جز عمان عمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هان دادگرا، هیچ ندانی که در این شهر</p></div>
<div class="m2"><p>بر من چه جفا می رود از گردش دوران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سالی دو فزون رفت که در ساحت این ملک</p></div>
<div class="m2"><p>بی قیمت و بی قدر چو کُحلم به صفاهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر چیز مرا بود ز اسباب تجمل</p></div>
<div class="m2"><p>شد صرف معیشت چه کتاب و چه قلمدان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چیزی که مرا ماند دلی زار و مشوش</p></div>
<div class="m2"><p>آن هم به سر زلف بتی گشت گروگان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با پایه جاهت چه کند کلک سخنور</p></div>
<div class="m2"><p>با رفعت شأنت چه کند نطق سخندان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر چرخ کجا پای توان هشت به سُلّم</p></div>
<div class="m2"><p>بر کوه کجا راه توان کرد به سوهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تابان شده تا مهر در این ساحت گیتی</p></div>
<div class="m2"><p>پویان شده تا چرخ بر این گرده کیهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گیتی همه بر دیده اعدایت تیره</p></div>
<div class="m2"><p>گردون همه بر کام مُحبانت گردان</p></div></div>