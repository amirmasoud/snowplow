---
title: >-
    شمارهٔ ۴۵ - خیمه سیمگون
---
# شمارهٔ ۴۵ - خیمه سیمگون

<div class="b" id="bn1"><div class="m1"><p>وقت آن شد که دگر در گلزار</p></div>
<div class="m2"><p>سیمگون خیمه زند ابر بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزری ابر شود بت پرور</p></div>
<div class="m2"><p>مانوی باغ شود غنچه نگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آورد عاریه لعل از دل سنگ</p></div>
<div class="m2"><p>پرورد نامیه، گل در بن خار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه اندیشه زند قامت سرو</p></div>
<div class="m2"><p>پای اندازه برد دست چنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنبر از خاک برآرد بستان</p></div>
<div class="m2"><p>سنبل از سنگ نماید کهسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزه فیروزه برد در گلشن</p></div>
<div class="m2"><p>غنچه بیجاده نهد در گلزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ مرغوله زند بر سر سرو</p></div>
<div class="m2"><p>یار بیغاله کشد از کف یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه بر گل فکند سنبل تر</p></div>
<div class="m2"><p>خمیه بر سبزه زند ابر بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می خورد مفتی شهر از صوفی</p></div>
<div class="m2"><p>گل برد مفلس ده از بازار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبزه از یاوری باد سحر</p></div>
<div class="m2"><p>لاله از ساحری ابر بهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوه را حلّه کند از شنگرف</p></div>
<div class="m2"><p>دشت را جامه دهد از زنگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابر مانند مغی باده فروش</p></div>
<div class="m2"><p>لاله، چون مغ بچه باده گسار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تر شود آن، تنش از رشحه می</p></div>
<div class="m2"><p>خون خورد این دلش از بیم خمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قدرت ما سکه از خاک ختن</p></div>
<div class="m2"><p>قوت جاذبه، از مشک تتار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باغ را غالیه بندد به میان</p></div>
<div class="m2"><p>راغ را لخلخه آرد به کنار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غنچه در بزم چمن، خسرو وش</p></div>
<div class="m2"><p>لاله بر طرف دمن، لیلی وار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گردد این دور ز مجنون خزان</p></div>
<div class="m2"><p>شود آن یار به شیرین بهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باغ را، لاله کند لعل انگیز</p></div>
<div class="m2"><p>راغ را، ژاله کند گوهربار</p></div></div>