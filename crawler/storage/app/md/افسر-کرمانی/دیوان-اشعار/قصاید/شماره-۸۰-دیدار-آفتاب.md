---
title: >-
    شمارهٔ ۸۰ - دیدار آفتاب
---
# شمارهٔ ۸۰ - دیدار آفتاب

<div class="b" id="bn1"><div class="m1"><p>ای که اندر گلشن خوبی گلی نشکفته است</p></div>
<div class="m2"><p>خرّم و سیراب الحق چون گل رخسار تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نونهالی چون قدت نارسته در بستان دل</p></div>
<div class="m2"><p>ای دل و جانم فدای قامت و رفتار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوستان دلبری را سر بسر دیدم نبود</p></div>
<div class="m2"><p>نرگسی در خواب همچون دیده خمّار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلستان جهان گشتم، بسی نایافتم</p></div>
<div class="m2"><p>سنبلی سیراب تر از جعد عنبر بار تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و دل ایثارت آوردم ولی گشتم خجل</p></div>
<div class="m2"><p>کیستم من تا نمایم جان و دل ایثار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب آسا تجلی کرده ای در چشم خلق</p></div>
<div class="m2"><p>خلق را طاقت نباشد دیدن دیدار تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شامگاهانم چه حاجت پرتو قندیل و شمع</p></div>
<div class="m2"><p>من که هر شب محفلم روشن بود ز انوار تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دل از سرّ دهانت هیچ ناگویم سخن</p></div>
<div class="m2"><p>ز آن که هرکس را ندانم قابل اسرار تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من که دل هرگز نمی دادم به دست عمر وزید</p></div>
<div class="m2"><p>برد اینک دل ز دستم جادوی طرّار تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فتنه دوران ضحاکم همی آمد به یاد</p></div>
<div class="m2"><p>بر سر دوش تو دیدم تا دو مشکین مار تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رشته های زلف پیرامون خالت بهر چیست؟</p></div>
<div class="m2"><p>حبل سحری چند دارد هندوی سحار تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیگران بر فرق من گلها بیفشانند و من</p></div>
<div class="m2"><p>دوست می دارم که بر چشمم نشیند خار تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکسی از خوان احسان تو خورده است ای عجب</p></div>
<div class="m2"><p>چون منی یارب نباشم از چه برخوردار تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر زمان کاندر سخن آیی، همی بینم به چشم</p></div>
<div class="m2"><p>شکرین شهدی چکد از دلنشین گفتار تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>موسوی حسن تو بر الزام فرعونی خطت</p></div>
<div class="m2"><p>معجز انگیزی کند از زلف ثعبان ثار تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خود شنیداستم ز من رنجیده ای خاکم به سر</p></div>
<div class="m2"><p>من که باشم تا نماید رنج من آزار تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خصم اگر گفتا حدیثی در حق من غم مدار</p></div>
<div class="m2"><p>کشکشان آرم ورا یک روز بر دربار تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صدق و کذبش بی کم و بی کاستی سازم عیان</p></div>
<div class="m2"><p>تا شود خرّم هم از من خاطر افکار تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیست قول خصم را درباره خصم اعتبار</p></div>
<div class="m2"><p>و این معانی، نیک داند خاطر هشیار تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خود وظیفه خصم من اندر حق من چیست، آنک</p></div>
<div class="m2"><p>تهمتی گوید که آن باشد همی دشوار تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من به رغم خصم، اگر نیکم اگر بد، لاجرم</p></div>
<div class="m2"><p>خار یا گل، هرچه خوانی هستم از گلزار تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باری ار بر من به میل خصم می خواهی جفا</p></div>
<div class="m2"><p>این من و این تیغ و این میدان و این پیکار تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قادری بر من، الا خود آنچه خواهی کن کنون</p></div>
<div class="m2"><p>گر کشی ور بخشیم، من بنده کردار تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>استخوان سوزد مرا، هرگه به خاطر بگذرد</p></div>
<div class="m2"><p>زآن جفاهایی که بر من رفته از اغیار تو</p></div></div>