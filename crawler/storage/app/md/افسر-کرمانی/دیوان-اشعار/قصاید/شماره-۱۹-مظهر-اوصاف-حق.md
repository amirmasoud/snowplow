---
title: >-
    شمارهٔ ۱۹ - مظهر اوصاف حقّ
---
# شمارهٔ ۱۹ - مظهر اوصاف حقّ

<div class="b" id="bn1"><div class="m1"><p>دیری است که جان در قفس جسم اسیر است</p></div>
<div class="m2"><p>دور از وطن افتاده در این ملک حقیر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دست که گیرد دل افتاده ما را</p></div>
<div class="m2"><p>کامروز در این منزل ویرانه فقیر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادیم دل از دست و دریغا که ندیدیم</p></div>
<div class="m2"><p>میری که همی نام ویم نقش ضمیر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارای جهان داور دین احمد مرسل</p></div>
<div class="m2"><p>آن کو ملک ملک خداوند قدیر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او جلوه گه قدرت دادار اگر نیست</p></div>
<div class="m2"><p>ذرات جهان را به ذوات از چه اثیر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آن که کمین چاکر دربار ثنایت</p></div>
<div class="m2"><p>در محکمه حکمت دادار مدیر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد قافله جودت و اجرام کواکب</p></div>
<div class="m2"><p>بر سطح فلک نقش کف پای بعیر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مظهر اوصاف و کمالات خدائی</p></div>
<div class="m2"><p>در کون و مکان ذات تو بی مثل و نظیر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز جلوه ی رخسار تو در کون و مکان نیست</p></div>
<div class="m2"><p>و آن راست مبرهن که در او چشم بصیر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر روی منیرت به جهان جلوه گر آمد</p></div>
<div class="m2"><p>اندر بر او چهر مه و مهر چو قیر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شأنت بر از آن است که جبریل امین را</p></div>
<div class="m2"><p>گویم به دبستان تو یک طفل صغیر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با فیض سحاب کف جودت یم امکان</p></div>
<div class="m2"><p>چون نیک بدیدم مثل بحر و غدیر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سبز از چه بود مزرع آمال خلایق</p></div>
<div class="m2"><p>سر پنجه جودت نه اگر ابر مطیر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سطری است سماوات ز دیوان ثنایت</p></div>
<div class="m2"><p>کش نقطه آن سطر یکی مهر منیر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با مزرع احسان تو نه کشته گردون</p></div>
<div class="m2"><p>ما نیک بدیدیم و کم از قشر شعیر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از رایحه خلق تو یک نافه جنان است</p></div>
<div class="m2"><p>وز نایره قهر تو یک شعله سعیر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در جنب مکین جدولی از لجه جودت</p></div>
<div class="m2"><p>دریای ازل همچو یکی جوی حقیر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر قطب زند دور از آن گنبد دوّار</p></div>
<div class="m2"><p>کش دست تو همواره شب و روز مدیر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درک تو به افهام خلایق چه در آید؟</p></div>
<div class="m2"><p>کی سبزه برآید ز زمینی که کویر است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر خاک سرایت سر تسلیم نهادن</p></div>
<div class="m2"><p>صد باره به از زینت دیهیم و سریر است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای آنکه مطاف امل اهل جهان را</p></div>
<div class="m2"><p>درگاه عطایت ز صغیر و ز کبیر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ما چند تهیدست در این شهر بمانیم</p></div>
<div class="m2"><p>با جود تو کاندر همه ملک شهیر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لختی ز کرم بین به من دلشده کامروز</p></div>
<div class="m2"><p>جز آن که گریزم به پناهت نه گزیر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مدحت نتواند که به انجام رساند</p></div>
<div class="m2"><p>افسر که زبانش ز ثنای تو قصیر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از کون و مکان بربودش دوحه مدحت</p></div>
<div class="m2"><p>اغصان و مرا طایر اندیشه حقیر است</p></div></div>