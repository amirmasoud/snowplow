---
title: >-
    شمارهٔ ۶۲ - جام آفتاب
---
# شمارهٔ ۶۲ - جام آفتاب

<div class="b" id="bn1"><div class="m1"><p>آمد زمان آنکه دلارام عاشقان</p></div>
<div class="m2"><p>بی پرده همچو مهر زند سر ز شرق جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلهای معنوی دمد از شاخه مراد</p></div>
<div class="m2"><p>سازند نغمه ساز هزاران نکته دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد مگر ادیب سخن سنج هوشمند</p></div>
<div class="m2"><p>کاورده خوش بکف ورق اطفال بوستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکجا ستاده سرو ولی پای در به گل</p></div>
<div class="m2"><p>آمد مگر به باغ سهی سرو دل چمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی به جام کرد دگر راح روح بخش</p></div>
<div class="m2"><p>گوئی دمید باز ز نوجان به می کشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد ز سبحه طرف نبدد که می فروش</p></div>
<div class="m2"><p>یک جرعه گر دهد طلبد صد هزار جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افتاده در بساط چمن مست و بیقرار</p></div>
<div class="m2"><p>زهاد خشک مشرب و رندان تر زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طالع مگر ز شرق خم آمد چو مهر، جام</p></div>
<div class="m2"><p>کامد روان تیره دلان جای نوریان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر در رواح راح به ساغر کنم چه باک</p></div>
<div class="m2"><p>صبح است دست ساقی و جام آفتاب دان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وقت است تا ز پرده برآید رخ نگار</p></div>
<div class="m2"><p>بر عاشقان فراق شود وصل جاودان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظلمت فرو برد سر، در مغرب عدم</p></div>
<div class="m2"><p>مهر ازل ز شرق ابد، رخ کند عیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردد ز عکس پرتو خورشید لم یزل</p></div>
<div class="m2"><p>خفاش وار ظلم، به کتم عدم نهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اهریمنان به قید طلسم اوفتند باز</p></div>
<div class="m2"><p>بر تخت اقتدار سلیمان کند مکان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>موسی شود نهان و عیان گاو سامری</p></div>
<div class="m2"><p>قبطی شود نهان و عیان موسی زمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گردد عیان به کون و مکان ذات ذوالجلال</p></div>
<div class="m2"><p>آید پدید روی خدای جهانیان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یعنی لوای فتح شهنشاه، شرق و غرب</p></div>
<div class="m2"><p>یعنی جلای چهر خداوند انس و جان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن یکه تاز عرصه میدان عدل و داد</p></div>
<div class="m2"><p>آن شاهباز ذروه گردون لا مکان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای ظاهر از جمال تو انوار ذوالجلال</p></div>
<div class="m2"><p>و ای کاشف از زبان تو اسرار رازدان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاید کنند حمد تو را ما عدا ادا</p></div>
<div class="m2"><p>باید کنند مدح تو کروبیان بیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر خس رسد به سعی به پایان بحر ژرف</p></div>
<div class="m2"><p>کرکس پرد به جهد به ایوان آسمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کی بزم انس حضرت یزدان سرودمش</p></div>
<div class="m2"><p>عرش برین نبود گرت فرش آستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در راه حق چگونه قوافل زنند گام</p></div>
<div class="m2"><p>مهر رخ تو نبود اگر میر کاروان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>انسی کجا و خلوت انس تو زینهار</p></div>
<div class="m2"><p>کی پر کاه جای گزیند به کهکشان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عقل محیط را چه به درک کمال تو</p></div>
<div class="m2"><p>عصفور را چگونه شود عرش، آشیان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا جای بر بسیط زمین کردی از کرم</p></div>
<div class="m2"><p>گشتند رشک جوهر افلاک خاکیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه آسمان چو قطره که افشاندش فرود</p></div>
<div class="m2"><p>سقای بارگاه تو از بهر تشنگان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مهرت اگر به شعله آذر شود قرین</p></div>
<div class="m2"><p>مهرت اگر به چشمه حیوان کند قران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آذر شود به چشمه حیوان حیات بخش</p></div>
<div class="m2"><p>حیوان شود ز شعله آذر مددستان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر پرده از جمال ز سطوت برافکنی</p></div>
<div class="m2"><p>لرزند و اوفتند چو خورشید اختران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باید هزار مرتبه از عقل برتری</p></div>
<div class="m2"><p>تا طی شود ز کاخ تو یک پله نردبان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سیارگان به مرتع چرخند چون غنم</p></div>
<div class="m2"><p>چوب شهاب بر کف گردون تو را شبان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر هر که آفتاب ولایت کند طلوع</p></div>
<div class="m2"><p>عار است سایه گرچه بود عرش سایه بان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یک لحظه بگذری اگر از مهر برزمین</p></div>
<div class="m2"><p>یک لمحه بنگری اگر از قهر بر زمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گردد ز سطوت نظرت آن بسان این</p></div>
<div class="m2"><p>گردد ز رأفت گذرت این بسان آن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پروین به چنگ باز فلک ز امر محکمت</p></div>
<div class="m2"><p>مانند عقرب است به منقار ماکیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گردون زند به خاک درت لاف همسری</p></div>
<div class="m2"><p>بر پای حاجبت رسد، ار فرق فرقدان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا در دهور امر تو آرد بجای هور</p></div>
<div class="m2"><p>هر صبحدم ز شرق به غرب است از آن دوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در مکتبت ولید، فلاطون خم نشین</p></div>
<div class="m2"><p>بر درگهت عبید، سلاطین جم نشان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>میکال چاکری است تو را گوش بر سخن</p></div>
<div class="m2"><p>جبریل خادمی است تو را سر بر آستان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>طبعت ادای قرض سوائل زمین زمین</p></div>
<div class="m2"><p>دستت کفاف عرض ایادی زمان زمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حرفی کجا ز دفتر مدحت بیان شود</p></div>
<div class="m2"><p>گر ماسوا شوند همه مو به مو زبان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا هست در جهان دل عشاق و زلف یار</p></div>
<div class="m2"><p>در نزد عاشقان مثلش گوی و صولجان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بادا، سر عدوی تو در معرض جدال</p></div>
<div class="m2"><p>مانند گوی در خم چوگانِ دوستان</p></div></div>