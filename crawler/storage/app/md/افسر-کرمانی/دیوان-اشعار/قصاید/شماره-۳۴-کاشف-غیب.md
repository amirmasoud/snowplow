---
title: >-
    شمارهٔ ۳۴ - کاشف غیب
---
# شمارهٔ ۳۴ - کاشف غیب

<div class="b" id="bn1"><div class="m1"><p>وزید بر تن خوابیدگان نسیم سحر</p></div>
<div class="m2"><p>وز آن شمیم بیفتاد خوابشان از سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت بر بدن مردگان مسیح نسیم</p></div>
<div class="m2"><p>وز آن نسیم روان یافت بازشان پیکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمین مرده دگر زنده شد به فیض نسیم</p></div>
<div class="m2"><p>دلی که زنده نشد از زمین بود کمتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خفتگان به غفلت منم در این وادی</p></div>
<div class="m2"><p>هوس مکان و طمع بالش و غضب بستر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از این مقام مگر اشتیاق یار عزیز</p></div>
<div class="m2"><p>کشاندم به مکانی که پا نهم بر سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسید آنکه مه عاشقان به شام فراق</p></div>
<div class="m2"><p>کند چو مهر جهانتاب سر ز شرق بدر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذشت نوبت اشرار، ایهاالاحباب</p></div>
<div class="m2"><p>رسید دولت اخیار، صاحبان بصر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درید پنجه شیر قضا به نیم نفس</p></div>
<div class="m2"><p>گلو ز روبه شرک و شکم ز استر شر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکست ساعد گم گشتگان به پنجه عدل</p></div>
<div class="m2"><p>نشست خسرو ایمانیان به تخت ظفر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قول پیر مغان و به اذن مفتی شهر</p></div>
<div class="m2"><p>گرفت ساقی خورشید رخ به کف ساغر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر ز ساقی روحانیان نگیرم می</p></div>
<div class="m2"><p>بدان بود که ندارم ز فیض عقل خبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گذشت جلوه خفاش در شب دیجور</p></div>
<div class="m2"><p>دمید صبح مبارک طلوع از خاور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فتاد اهرمن خون به قید فوج ملک</p></div>
<div class="m2"><p>چو بر سریر، سلیمان عصر کرد مقر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کفی نخورده که خون گشت نیل بر قبطی</p></div>
<div class="m2"><p>بلی ز موسی هر عصر معجزی است دگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشید شب پره شرک سر به وکن عدم</p></div>
<div class="m2"><p>چو آفتاب احد رخ نمود از خاور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حسن محاسن و باقر علوم و کاظم خلق</p></div>
<div class="m2"><p>نبی خصال و علی قدرت و خدا منظر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یگانه مهدی موعود حجة بن حسن</p></div>
<div class="m2"><p>بزرگ مقصد یزدان ز اول و آخر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی زبان تو اسرار غیب را کاشف</p></div>
<div class="m2"><p>خهی بیان تو اظهار علم را مصدر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدا بخوانمت ار، شرک می شود لیکن</p></div>
<div class="m2"><p>ظهور روی تو آمد خدای را مظهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به مجلس تو مه و مهر شمع بزم افروز</p></div>
<div class="m2"><p>ز مطبخ تو نه افلاک مشت خاکستر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به لوح فکر تو نقش است هستی امکان</p></div>
<div class="m2"><p>که شد خیال تو مر کاینات را مصدر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حذر ز قهر تو باید نه ز آتش دوزخ</p></div>
<div class="m2"><p>که شد ز شعله قهر تو یک شراره سقر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طمع به مهر تو باید نه بر بهشت برین</p></div>
<div class="m2"><p>که از ولای تو عکسی است جنت و کوثر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عرض ز فیض تو آن صاف جوهر است که شد</p></div>
<div class="m2"><p>صفای جوهر صافی بنزد آن چو حجر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به محضر تو قدر چاکری چنانکه قضا</p></div>
<div class="m2"><p>بدرگه تو قضا بنده ای بسان قدر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خرد کجا و گمانت بلی نشاید زد</p></div>
<div class="m2"><p>ابر مطار ملک پر، کلاغ حیلت گر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ذوات را چه به درک صفات بیچونت</p></div>
<div class="m2"><p>عرض چگونه به جوهر شود ثناگستر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ستایش تو تمناست فکر افسر را</p></div>
<div class="m2"><p>که نظم داده بدین گونه طبع عقد درر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگرچه عمری از این آستان فتادم دور</p></div>
<div class="m2"><p>هزار شکر که دادم خدای عمر دگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هزار حمد که ز الطاف قادر یکتا</p></div>
<div class="m2"><p>به کام خویش بدین آستان نهادم سر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جدا مباد سر از گرد ساحت کویت</p></div>
<div class="m2"><p>مگر ز دور فلک خاک گرددم پیکر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جهانپناه، خدیوا، ز جور دور زمان</p></div>
<div class="m2"><p>فتادم از تو جدا همچو کور کز رهبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گهی به راه و گهی قعر چاه افتادم</p></div>
<div class="m2"><p>که کور مسکین بی راهبر بود مضطر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بزرگوار خدایا، به عزّ احمد و آل</p></div>
<div class="m2"><p>به فضل خاص از این عاصی دغا بگذر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه تا بود از قرب و بعد نام و نشان</p></div>
<div class="m2"><p>هماره تا بود از مهر و قهر رسم و اثر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به نار قهر تو بادا، تن عدو سوزان</p></div>
<div class="m2"><p>به نور مهر تو بادا، دل مُحبّ انور</p></div></div>