---
title: >-
    شمارهٔ ۴۹ - کلام الله ناطق
---
# شمارهٔ ۴۹ - کلام الله ناطق

<div class="b" id="bn1"><div class="m1"><p>جهان شوخی است دستان ساز و دلها گرم دستانش</p></div>
<div class="m2"><p>یکی زی خویشتن باز آی و بستان دل ز دستانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشو مفتون دلداری که آفت زاست دیدارش</p></div>
<div class="m2"><p>مجو پیوند معشوقی که رنج افزاست پیمانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر آسایشت باید، مبر اندر پیش زحمت</p></div>
<div class="m2"><p>وگر جمعیتت باید، مکن خاطر پریشانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منه بر خط و خالش دل، که مجنون خواندت عاقل</p></div>
<div class="m2"><p>مشو بر وصل او مایل، که در وصل است هجرانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی مهمان کش است این شوخ بد عهد سیه کاسه،‌</p></div>
<div class="m2"><p>که غیر از سم قاتل نیست چیزی شربت خوانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجو برگ تن آسایی از این معشوق هرجایی</p></div>
<div class="m2"><p>که هر شب گوی چوگانی است سیمین گوی پستانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن چون نار، دل پرخون و بر یاری مشو مفتون</p></div>
<div class="m2"><p>که هر ساعت یکی بوید همی سیب زنخدانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی رنگین دکان دارد مر این دنیای بوقلمون</p></div>
<div class="m2"><p>که نبود جز زیان دین و ایمان سود دکانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الا گر مرد دانایی بهل قانون خود رآیی</p></div>
<div class="m2"><p>بکش خار غمش از پا، بنه از دست دامانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منه رخت اندر این دریا که طوفان زاست گردابش</p></div>
<div class="m2"><p>مزن گام اندر این بیدا، که ناپیداست پایانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کند آهنگ خونریزی چو این معشوق عاشق کش</p></div>
<div class="m2"><p>ندارد در نظر یک جو، گدا فرقی ز سلطانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسا خوبان جان پرور، که در خشتند مکنونش</p></div>
<div class="m2"><p>بسا ترکان سیمین بر، که در خاکند پنهانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی بر کاخ نوشروان به عبرت بگذر و بنگر</p></div>
<div class="m2"><p>که از کسری پیامی گویدت هر خشت ایوانش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به فرمان سلیمان بود، گر دیو و دد و مردم</p></div>
<div class="m2"><p>چو شد فرّ سلیمان و چه شد فرخنده فرمانش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر از خاوران تا باختر شد رام اسکندر،</p></div>
<div class="m2"><p>به نعل آهنین سم، طی ظلمت کرد یکرانش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک نگذاشت جز نامی از او برجای در گیتی</p></div>
<div class="m2"><p>چشاندش زهر مرگ آخر، نخست ار کرد مهمانش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الا، گر مرد عقبایی ره مردان عقبی جو</p></div>
<div class="m2"><p>رها کن دامن دنیا و بگذر زآب و از نانش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو طفلان تا کی ای جاهل، شوی مشغول آب و گل</p></div>
<div class="m2"><p>کنی رنجور دردی دل، که پیدا نیست درمانش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان شوخی که دل بستی و صد ره از غمش خستی</p></div>
<div class="m2"><p>اگر از تیر او رستی، مکن آهنگ میدانش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مبین آن چهرگان روشن و آن قد دلجویش</p></div>
<div class="m2"><p>مبین آن زلفکان تیره و آن چشم فتانش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که ماری جانگزایت گردد، آن گیسوی پرتابش</p></div>
<div class="m2"><p>که شامی تیره فامت گردد، آن رخسار رخشانش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه سود ار نکهت عنبر وزد زآن مو، که در محشر</p></div>
<div class="m2"><p>دماغ مرد و زن گندد همی از بوی عصیانش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جای آب، خون دل دهی تا کی بدان گلبن،</p></div>
<div class="m2"><p>که هر دم دیگری گردد همی مرغ خوش الحانش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنی تا کی سپر از جان به پیش ناوک جانان</p></div>
<div class="m2"><p>بهل، کافتد به خاک تیره آخر تیر مژگانش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خلاف مردمی باشد به اینان دادن آن دل را،</p></div>
<div class="m2"><p>که درج گوهر مهر علی خوانده است یزدانش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ولی حضرت داور، امیرالمؤمنین حیدر</p></div>
<div class="m2"><p>که بستوده است در قرآن جهاندار جهانبانش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شهنشاهی که آورده است سر در ربقه حکمش</p></div>
<div class="m2"><p>ز آغاز وجود این باژگون گردون گردانش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کلام الله ناطق او، و آیات کتاب الله</p></div>
<div class="m2"><p>همه در مدحت قدرش، همه در رفعت شانش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قضا بر دیده امضا نهد هر لحظه یرلیغش</p></div>
<div class="m2"><p>قدر بر گوشه افسر نهد هر لمحه فرمانش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دو نور افکن، چراغ بزمگه، ناهید و برجیسش</p></div>
<div class="m2"><p>دو روشن رخ، غلام بارگه، بهرام و کیوانش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه اسرار سبحانی نهان در سینه پاکش</p></div>
<div class="m2"><p>همه انوار یزدانی عیان از روی تابانش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر روی ارادت چرخ از کویش بگرداند،</p></div>
<div class="m2"><p>تزلزل اندر افتد تا ابد بر چار ارکانش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهان را جسم بی جان دان و در وی جسم او را جان</p></div>
<div class="m2"><p>نجنبد عضوی از اعضا، نبخشد گر بدو جانش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>الا، گر مرد حق جویی، همی بیخود چه می پویی،</p></div>
<div class="m2"><p>ببین رخسار یزدان را، ز رخسار فروزانش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان رنگ خودی بزدوده از آیینه هستی،</p></div>
<div class="m2"><p>که یزدان است سر تا پا و پا تا سر ز یزدانش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به ظاهر گر چه فرزندی گران مایه است آدم را،</p></div>
<div class="m2"><p>ولی پیش از پدر در ملک هستی بوده جولانش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نخستین جلوه ای در جسم آدم کرد و آدم شد</p></div>
<div class="m2"><p>و زآن پس شد پدید از صلب و از خود کرد پنهانش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دگر ره جلوه گر در حضرت عیسی بن مریم شد</p></div>
<div class="m2"><p>چو در موسی درآمد نام شد موسی بن عمرانش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بوصفش تا به کی گویی که میکال است مملوکش</p></div>
<div class="m2"><p>به مدحش تا به کی خوانی که جبریل است دربانش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کسی کایجاد جبرائیل و میکائیل کرداستی</p></div>
<div class="m2"><p>چه طرفش زآن که هستند این دو تن مملوک احسانش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو در میدان درآید از پی خونریزی اعدا</p></div>
<div class="m2"><p>فلک ماننده گویی است اندر خم چوگانش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به روز رزم کز گرد سم اسبان گردون بر</p></div>
<div class="m2"><p>برآید قیرگونه ابر و گردد تیر، بارانش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شود از سیل خون دریایی آنسان پهنه هامون</p></div>
<div class="m2"><p>که اندازد خلل در فلک گردون موج طوفانش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در آن نوبت چو شاه دین برآید بر فراز زین</p></div>
<div class="m2"><p>ملک بر وی کند تحسین، فلک درّد ز پیکانش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قضا از بیم جان گیرد مکان در ظلّ زنهارش</p></div>
<div class="m2"><p>قدر از خوف سر جوید امان در زیر فرمانش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بخوشد خون به جسم پردلان از تیغ خونریزش</p></div>
<div class="m2"><p>بپرد مرغ جان سرکشان از تیر پرّانش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بسختی خصمش ارثَهْلان شود در پهنه هیجا</p></div>
<div class="m2"><p>کی از یک خردل است افزون به پیش تیغ برّانش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جهان گر پیل گردد یکسره با پشه همسنگش</p></div>
<div class="m2"><p>زمین گر شیر گردد یکسره با مور یکسانش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شود از آستین بیرون یدی چون دست یزدانی</p></div>
<div class="m2"><p>نماید کمتر از موران همه شیران غژمانش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جهاندارا، شها، از من چسان آید ثنای تو</p></div>
<div class="m2"><p>که نتواند بیان کردن یک از بسیار حسانش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بویژه اندر این نوبت که جان در جسم پرمحنت</p></div>
<div class="m2"><p>چنان پژمرده از زحمت که نتوان کرد ریانش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بحسرت آمده توأم، نشسته با غمان همدم</p></div>
<div class="m2"><p>درون پرخون، مژه پرنم، ز جور چرخ و کیوانش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو را ای شاه والا فرّ، تو را ای شافع محشر</p></div>
<div class="m2"><p>به نور پاک پیغمبر دهم سوگند و یارانش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کز این فقر و غم و محنت وز این اندوه و این ذلت</p></div>
<div class="m2"><p>رها کن افسر و برهان ز دست کید کیهانش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>الا رنجور تا نالد همی از درد رنجوری</p></div>
<div class="m2"><p>الا بیمار تا هذیان همی گوید به بحرانش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مُحِبَّت را بود عیشی که نتوان یافت انجامش</p></div>
<div class="m2"><p>عدویت را بود دردی که نتوان یافت درمانش</p></div></div>