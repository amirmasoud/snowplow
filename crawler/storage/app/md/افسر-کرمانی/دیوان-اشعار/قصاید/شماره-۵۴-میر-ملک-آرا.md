---
title: >-
    شمارهٔ ۵۴ - میرِ مُلک آرا
---
# شمارهٔ ۵۴ - میرِ مُلک آرا

<div class="b" id="bn1"><div class="m1"><p>بیا این چشم صورت بین بنه ای دل دمی برهم</p></div>
<div class="m2"><p>به خلوتخانه معنی درآ، با خاطری خرّم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خودبینی درآ، یک دم اگر خواهی خدابینی</p></div>
<div class="m2"><p>چو ماهی آب می جوئی و هستی غوطه ور در یم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود در باطنت پنهان، سراسر عالم امکان</p></div>
<div class="m2"><p>مپندار اینچنین خود را، که هستی نقطه مبهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظام ملک هستی را، نگر با دیده دانش</p></div>
<div class="m2"><p>ببین هر نیش خاری را در او نوشی بود مدغم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی بنما نظر اندر بساط ساحت گیتی</p></div>
<div class="m2"><p>ببین هر قطره بحری هست و هر ذره کهی اعظم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زیر پا بود گنجت ولی آگه نئی از آن</p></div>
<div class="m2"><p>در این گنج را بگشا، مخواه از این و آن درهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را اینک به ساغر ریخت ساقی شهد جان افزا</p></div>
<div class="m2"><p>کنون خود از چه می داری مبدل شهد را با سم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عشرتخانه وصل اندرآ، از محنت هجران</p></div>
<div class="m2"><p>حلی بربند از شادی و بفکن جامه ماتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندانم از چه دل بستی، دراین ویرانه هستی</p></div>
<div class="m2"><p>کنون وقت است اگر دستی، زنی بر دامنی محکم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه دامن؟ دامن پاک ولی حضرت قائم</p></div>
<div class="m2"><p>چه دامن؟ دامن حُبّ سلیل سید خاتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهی کز ظل کمتر پایه اورنگ جاه او</p></div>
<div class="m2"><p>فروغ عرش اعظم تافته بر هستی عالم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شه ملک مکان و لامکان آن کز نخست آمد</p></div>
<div class="m2"><p>خیام احتشامش را فضای لامکان مخیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهی ای داوری کز عکس زرین نعل شبرنگت</p></div>
<div class="m2"><p>به چرخ چارمین آمد فروزان نیر اعظم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تمام اهل بینش را، توئی صورت، توئی معنی</p></div>
<div class="m2"><p>کتاب آفرینش را، توئی عنوان، توئی خاتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه از رشحه کلک تو شد ای مظهر صانع</p></div>
<div class="m2"><p>سراسر عالم امکان، محقر نقطه ای مبهم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر از مشرق دل سر نمی زد مهر رخسارت</p></div>
<div class="m2"><p>کجا بیدار می گردید از خواب عدم آدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز برق آتش قهرت بود دوزخ یکی پرتو</p></div>
<div class="m2"><p>ز ابر رحمت لطفت، بود کوثر یکی شبنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه مقصود مولود تو بود ای میر ملک آرا</p></div>
<div class="m2"><p>که شد این چار مام و هفت آبا مقترن باهم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نمی شد محیی اموات، احیا، گر نمی گشتی</p></div>
<div class="m2"><p>ز خفاش شبستان جلالت عیسی مریم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یم جود تو باشد آن گران دریای بی پایان</p></div>
<div class="m2"><p>که این بحر وجود آمد محقر نقطه ای ز آن یم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ملک بر بوالبشر هرگز نبردی سجده حشمت</p></div>
<div class="m2"><p>نبودی خاک پای تو اگر با طینتش منضم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تواند پیک فکرت پی برد بر کنه جاه تو</p></div>
<div class="m2"><p>به بام عرش اگر بتوان شدن با پله سُلّم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همان بهتر که بربندم زبان را از ثنای تو</p></div>
<div class="m2"><p>ز شرح عزّ تو باشد زبان ما کنون ابکم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به پرواز آید از کویت، اگر کوچک ترین مرغی</p></div>
<div class="m2"><p>بر او باشد قفس مانا، فضای گلشن عالم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مقصر گرچه از مدح تو باشم، دار معذورم</p></div>
<div class="m2"><p>نهاد انگشت عشقت مرمرا مهر مگو بر فم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز نیش خنجر هجران، دلم صد چاک شد آوخ</p></div>
<div class="m2"><p>اگر ننهی به دست مرحمت بر زخم دل مرهم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا خو کرده مرغ دل به دام جعد مشکینت</p></div>
<div class="m2"><p>برون مشکل توانم برد دل زآن دام خم در خم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلم از مطرب عشقت مدام اندر سماع استی</p></div>
<div class="m2"><p>گهی از ناله زیر و زمانی از نوای بم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به دام حلقه زلفت بود مسکین دلم مایل</p></div>
<div class="m2"><p>چگونه راست می آید حدیث صعوه با ارقم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بجز من کز درت دورم، ز دیدار تو مهجورم</p></div>
<div class="m2"><p>خدا را هر که می بینم بود در کوی تو محرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا جز نقد جانی کز تو دارم وام ای مولا</p></div>
<div class="m2"><p>نباشد در کفم چیزی نه از دینار و نه درهم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صف عشاق می گردد پریشان تر ز زلف تو</p></div>
<div class="m2"><p>به میدان رایت نازت برافرازد اگر پرچم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نگر از مرحمت شاها گدائی را که روز و شب</p></div>
<div class="m2"><p>همی در آتش هجرت بسوزد برنیارد دم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بود اندر بسیط دهر تا آثاری از شادی</p></div>
<div class="m2"><p>بود اندر بساط خاک تا نام و نشان از غم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>محبت باد روز و شب قرین با عشرت و شادی</p></div>
<div class="m2"><p>عدویت باد سال و مه اسیر محنت و ماتم</p></div></div>