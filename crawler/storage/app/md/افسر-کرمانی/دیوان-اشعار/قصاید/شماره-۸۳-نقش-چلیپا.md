---
title: >-
    شمارهٔ ۸۳ - نقش چلیپا
---
# شمارهٔ ۸۳ - نقش چلیپا

<div class="b" id="bn1"><div class="m1"><p>ای زلف تا به دوش بتم پا نهاده ای</p></div>
<div class="m2"><p>پای ادب ز قاعده بالا نهاده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برتر نهاده ای قدم از جای خویشتن</p></div>
<div class="m2"><p>شرمت ز خویش باد که بیجا نهاده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوخی بدین مثابه ندیدم ز هیچ تن</p></div>
<div class="m2"><p>این رسم در زمانه تو تنها نهاده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعری فزون نباشی و می بینمت همی</p></div>
<div class="m2"><p>از رتبه پا به تارک شعری نهاده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانا توراست دعوی اعجاز موسوی</p></div>
<div class="m2"><p>کاین سان به آستین ید بیضا نهاده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونان کلیم عصری و در طور عاشقی</p></div>
<div class="m2"><p>روی امید جانب سینا نهاده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا چون خلیل عهدی و بر تار عشق یار</p></div>
<div class="m2"><p>تن را به منجنیق تولّی نهاده ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا عیسی زمانی و از فرط منزلت</p></div>
<div class="m2"><p>دل بر عروج طارم اعلی نهاده ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاهی به دوش و گه به گریبان، گهی به رخ</p></div>
<div class="m2"><p>هر لحظه در مقامی مأوی نهاده ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طاووس جنتی تو و همواره بال و پر</p></div>
<div class="m2"><p>بر شاخسار دوحه طوبی نهاده ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راهب صفت همی به کلیسای چهر دوست</p></div>
<div class="m2"><p>ترک جهان به عادت ترسا نهاده ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قسیس وار بر تن از آن حلقه حلقه ها</p></div>
<div class="m2"><p>شکل صلیب و نقش چلیپا نهاده ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر نافه کز ختا سوی آفاق می رود</p></div>
<div class="m2"><p>در چین خویش جمله مهیا نهاده ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای کافر سیه دل هندوی، خیره سر</p></div>
<div class="m2"><p>اندر سرم هزاران سودا نهاده ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای دزد اهرمن خو، طرار فتنه جو</p></div>
<div class="m2"><p>دامم براه دین پی یغما نهاده ای</p></div></div>