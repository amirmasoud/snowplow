---
title: >-
    شمارهٔ ۴۳ - در سوگ همسر خود
---
# شمارهٔ ۴۳ - در سوگ همسر خود

<div class="b" id="bn1"><div class="m1"><p>کنون که عهد ربیع است و روزگار بهار</p></div>
<div class="m2"><p>مرا دلی است به رنج اندر از تغابن یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار پار نبردیم لذت ای همدم</p></div>
<div class="m2"><p>مگر بریم هم امسال لذتی ز بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار پار مرا بود سیمبر ترکی</p></div>
<div class="m2"><p>فرشته خوی و ملک سیرت و پری رفتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سحر غمزه به تاراج داده صد بابل</p></div>
<div class="m2"><p>ز چین طرّه به یغما سپرد صد تاتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گسسته بر زبر ماه رشته پروین</p></div>
<div class="m2"><p>شکسته بر سر خورشید طبله زنگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سفر نمود بناگاه و رفت از بر من</p></div>
<div class="m2"><p>مرا ز رفتن او مانده دل حزین و فکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شنیده بود که مه را سفر فزاید قدر</p></div>
<div class="m2"><p>سفر نمود مهم تا فزایدش مقدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهر بین که به آیین همسران حریف</p></div>
<div class="m2"><p>چگونه با من و دل باژگونه باخت قمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفته بود دلم با وصال او الفت</p></div>
<div class="m2"><p>بدل به فرقت او کرد الفتم ناچار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنار من تهی از غم نگشت زآن ساعت</p></div>
<div class="m2"><p>که آن غزال غزلخوان ز من گرفت کنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهار طلعت من چون کناره جست ز من</p></div>
<div class="m2"><p>بهار عیش مرا شد خزان رنج دچار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلی چو شاخ شود منقطع ز ریشه خویش</p></div>
<div class="m2"><p>ورق بخوشد و پژمرده اش شود اثمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الا چه شرح دهم از غم جدایی او،</p></div>
<div class="m2"><p>که نیست ناطقه را بیش تاب این تکرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون که گاه بهار است و بوستان از گل</p></div>
<div class="m2"><p>چو شاهدی بود از فرق تا قدم به نگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به جای سبزه و گل در چمن همی گویی،</p></div>
<div class="m2"><p>نهاده طره و رخ لعبتان چین و تتار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صبا نموده عنبر، همی به جیب و بغل</p></div>
<div class="m2"><p>هوا نهفته لادن، همی به گوش و کنار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صفیر مرغ زند روح را صلای بهشت</p></div>
<div class="m2"><p>نسیم صبح دهد مغز را شمیم بهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چمن ز باد صبا عطر سائیش آیین</p></div>
<div class="m2"><p>صبا ز خاک چمن مشکبیزیش هنجار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پای سر و بن آواز برکشیده تذرو</p></div>
<div class="m2"><p>به شاخ سرخ گل، آهنگ ساز کرده هزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تذروگان همه بنهاده چنگ بر حلقوم</p></div>
<div class="m2"><p>چکاوکان همه بر بسته زنگ بر منقار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به چهر سوری افکنده باد سرخ حریر</p></div>
<div class="m2"><p>به جام نرگس شبنم نموده زرد عقار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ارغوان و سمن، بس فشانده باد ورق</p></div>
<div class="m2"><p>نهفته جیب چمن را به درهم و دینار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز برگ لاله عیان، شقه شقه حلّه چین</p></div>
<div class="m2"><p>به جیب غنچه نهان، نافه نافه مشک تتار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمن چو افسر کاووس پر ز گوهر و لعل</p></div>
<div class="m2"><p>چمن چو پیکر طاووس پر ز نقش و نگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نهفته تن به نقوش خورنقی بستان</p></div>
<div class="m2"><p>نموده جا به فراش ستبرقی کهسار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز لاله بستان چونان که پشته پشته عقیق</p></div>
<div class="m2"><p>ز سبزه هامون‌، ز آنسان که کوه که زنگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سحاب‌، گوهر ریز است و خاک لعل انگیز</p></div>
<div class="m2"><p>چمن زمرّد خیز است و دشت مرجان زار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هوا، معنبر بوی است و باد مشک آیین</p></div>
<div class="m2"><p>زمین منقش چهر است و آب آیینه دار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بنفشه مشک فروش و شکوفه قاقم پوش</p></div>
<div class="m2"><p>نسیم لخلخه سای و سحاب لؤلؤ بار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بس به باغ ز خوبان کشمری قامت،</p></div>
<div class="m2"><p>ز بس به راغ ز ترکان خلخی رخسار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه باغ، بل فلکی و اندر آن ملک انبوه</p></div>
<div class="m2"><p>نه راغ بل ارمی و اندر آن پری بسیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کجا رواست خدا را، که در چنین فصلی،</p></div>
<div class="m2"><p>جهانیان همه در عیش و من به اندوه یار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خصوص اندوه دلبر که سخت تر رنجی است</p></div>
<div class="m2"><p>که ز آن رهاییم الحق بسی بود دشوار</p></div></div>