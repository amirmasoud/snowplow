---
title: >-
    شمارهٔ ۴۴ - صبح صادق
---
# شمارهٔ ۴۴ - صبح صادق

<div class="b" id="bn1"><div class="m1"><p>داد دیگر بار زیور باغ را ابر بهار</p></div>
<div class="m2"><p>گشت چون خلد برین از سبزه صحن مرغزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد نوروزی وزان شد باز در اطراف باغ</p></div>
<div class="m2"><p>ابر آزاری دگر شد درّ فشان در کوهسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بار دیگر غنچه از بهر تبسم لب گشود</p></div>
<div class="m2"><p>باز از بهر ترنم بیقرار آمد هزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو چشم نوغزالان ختن شد گوئیا</p></div>
<div class="m2"><p>دیده عابد فریب مست نرگس در خمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارض نسرین گرفته شبنم ابر مطیر</p></div>
<div class="m2"><p>یا خوی افشان از حیا آمد رخ گلرنگ یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد خدّ نوشخندان رسته خطّ مشکفام</p></div>
<div class="m2"><p>یا ریاحین بردمیدستی بطرف جویبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باغ گوئی از ریاحین سبز چون خطّ بتان</p></div>
<div class="m2"><p>راغ گوئی از شقایق سرخ چون روی نگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب گوئی چون زلال چشمه حیوان روان</p></div>
<div class="m2"><p>باد گوئی چون روان عاشقان شد بیقرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک گوئی از دم باد صبا شد زنده دل</p></div>
<div class="m2"><p>نار گوئی همچو خوی دلبر من پر شرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قمریان گوئی نواخوان گشته بی آشوب زاغ</p></div>
<div class="m2"><p>بلبلان گوئی غزلخوان گشته بی آسیب خار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشقان با گلرخان در باغ مست از بیخودی</p></div>
<div class="m2"><p>من به خلوت مانده تنها تن نزار و جانفکار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناگهم از در درآمد نوغزالی شیرمست</p></div>
<div class="m2"><p>حمله ور بر شیرمردان چشم مستش شیروار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بیاض روی او صبح مصفی بس خجل</p></div>
<div class="m2"><p>وز سواد زلف او مشک تتاری شرمسار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صبح صادق کرده از چاک گریبانش طلوع</p></div>
<div class="m2"><p>روز من از هجر او چونان شب یلدای تار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمه ای از گلستان روی او باغ ارم</p></div>
<div class="m2"><p>قطعه ای از بوستان حسن او دارالقرار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون مرا بر روی او افتاد چشم حق شناس</p></div>
<div class="m2"><p>دادمی یکبارگی از کف زمام اختیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتم ای سیمین بدن بر گوی با من کیستی</p></div>
<div class="m2"><p>گفت من آثار مهر ماه گردون اقتدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن کریم ذوالکرم کز جود عامش می برند</p></div>
<div class="m2"><p>فیض هستی انس و جن و وحش و طیر و مور و مار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بتابد رخ ز امرش مهر گردد ذره سان</p></div>
<div class="m2"><p>ور نپیچد سر ز حکمش ذره گردد مهروار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از شعاع او نمی بودی اگر در کسب نور</p></div>
<div class="m2"><p>وز ز ظل او نه پیدا کرده این ظلمت مدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از چه چون روی مهان گردیده خور رخشان به صبح</p></div>
<div class="m2"><p>وز چه چون موی بتان هر شامگه گردیده تار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای وجودت مرکز پرگار کان و مایکون</p></div>
<div class="m2"><p>و ای ظهورت علت اظهار امر کردگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای نظام آفرینش، یا امیرالمؤمنین</p></div>
<div class="m2"><p>همسر صدیقه کبری و باب هفت و چار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای لقای مهر از خاک سرایت مستنیر</p></div>
<div class="m2"><p>وی بقای خضر از آب ولایت پایدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آسمان گر سر کشد از طوع طوق بندگانت</p></div>
<div class="m2"><p>می شود نقش قدم مانند خام رهگذار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هم توئی آیات یزدان را صراطی مستقیم</p></div>
<div class="m2"><p>هم توئی مرآت سر تا پا نمای کردگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روز و شب دارم خیال موی و رویت در نظر</p></div>
<div class="m2"><p>نگذرد بر عاشقان زین خوب تر لیل و نهار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روی ما و خاک درگاه تو تا یوم النشور</p></div>
<div class="m2"><p>دست ما و دامن مهر تو تا روز شمار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وصف مویت را کند افسر که اینسان دمبدم</p></div>
<div class="m2"><p>ریزدش از خامه در دفتر همی مشک تتار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بود از آتش دوزخ کلام اندر جهان</p></div>
<div class="m2"><p>تا بود از جنت المأوی سخن در روزگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باد بر هر عضو اعدای تو صد دوزخ عقاب</p></div>
<div class="m2"><p>باد بر هر موی احباب تو صد جنّت نثار</p></div></div>