---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>فشاند ژاله به گلشن سحاب نیسانی</p></div>
<div class="m2"><p>بفرق لاله و گل کرد گوهر افشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکست رونق دی را بهار بستانی</p></div>
<div class="m2"><p>ایا بهار من، ای بوستان روحانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدح ز باده گران کن، بهل گرانجانی</p></div>
<div class="m2"><p>که عید نوروز آمد ز فیض یزدانی</p></div></div>