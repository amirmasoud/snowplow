---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ای مظهر ذات کبریائی</p></div>
<div class="m2"><p>زیبد بتو گر کنی خدائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شاهی عالمست بهتر</p></div>
<div class="m2"><p>بردرگه تو مرا گدائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید برخ نفاب بندد</p></div>
<div class="m2"><p>گر پرده ز روی برگشائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش نیست بتاز کف رها کن</p></div>
<div class="m2"><p>آیین جفا و بی وفائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین بیش منه چو لاله داغم</p></div>
<div class="m2"><p>برسینه زآتش جدائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای نور فزای چشم مردم</p></div>
<div class="m2"><p>از دیده من نهان چرائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکبار ببزمم ار خرامی</p></div>
<div class="m2"><p>از راه وفا و آشنائی</p></div></div>
<div class="b2" id="bn8"><p>برخیزم و سرنهم بپایت</p>
<p>بنشینم و جان کنم فدایت</p></div>
<div class="b" id="bn9"><div class="m1"><p>ای کوی تو طرف لاله زارم</p></div>
<div class="m2"><p>وی روی تو نوگل بهارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باری نگذاریش چو مرهم</p></div>
<div class="m2"><p>مجروح مکن دل فکارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز ابروی کمان و تیر مژگان</p></div>
<div class="m2"><p>هر لحظه بنو کنی شکارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل بر نکنم ز خاک کویت</p></div>
<div class="m2"><p>برباد اگر رود غبارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس لاله ز داغ حسرت تو</p></div>
<div class="m2"><p>روید پس مرگ از مزارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیروی تو چند همچو باران</p></div>
<div class="m2"><p>خونابه دل ز دیده بارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بربسته میان و رو گشاده</p></div>
<div class="m2"><p>باری گذر از تو برندارم</p></div></div>
<div class="b2" id="bn16"><p>برخیزم و سرنهم بپایت</p>
<p>بنشینم و جان کنم فدایت</p></div>
<div class="b" id="bn17"><div class="m1"><p>بردار ز رخ نقاب یارا</p></div>
<div class="m2"><p>مگذار بدل حجاب ما را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باری چه شود بشکر شاهی</p></div>
<div class="m2"><p>بنوازی اگر دمی گدا را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اسرار نهان ز مهرت ای جان</p></div>
<div class="m2"><p>در مخزن دل شد آشکارا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مهری چو تو ای مه دلفروز</p></div>
<div class="m2"><p>کی در نظر آرد این ها را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کی ثبت کنم بلوح سینه</p></div>
<div class="m2"><p>جز نقش خیال تو نگارا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در کوی تو راه چون بیابم</p></div>
<div class="m2"><p>کانجا نبود رهی صبا را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روزی قدم ار تو رنجه سازی</p></div>
<div class="m2"><p>درکلبه محنتم خدا را</p></div></div>
<div class="b2" id="bn24"><p>برخیزم و سر نهم بپایت</p>
<p>بنشینم و جان کنم فدایت</p></div>
<div class="b" id="bn25"><div class="m1"><p>از دیده چه اشگم ار فکندی</p></div>
<div class="m2"><p>دست آر دلم بنوشخندی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>درهر شکنی فکنده زلفت</p></div>
<div class="m2"><p>برگردن جان من کمندی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این خال به منظرت نشسته</p></div>
<div class="m2"><p>یابر مجمر بود سپندی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کی دل بکنم ز نخل قدت</p></div>
<div class="m2"><p>گر ریشه هستیم بکندی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بس چشم بدیدم و ندیدم</p></div>
<div class="m2"><p>چون چشم تو هیچ چشم بندی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سوزد دلم از همین که دایم</p></div>
<div class="m2"><p>پیش رخت ای نگار چندی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خواهم که نهان ز چشم اغیار</p></div>
<div class="m2"><p>پیش رخت ای نگار چندی</p></div></div>
<div class="b2" id="bn32"><p>برخیزم و سرنهم بپایت</p>
<p>بنشینم و جان کنم فدایت</p></div>
<div class="b" id="bn33"><div class="m1"><p>ای قد خوش تو سرو نازم</p></div>
<div class="m2"><p>دامن مکش از کف نیازم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باری چه شود ز لطف سازی</p></div>
<div class="m2"><p>در سایه خویش سرفرازم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بی روی تو از سرشک غماز</p></div>
<div class="m2"><p>گردیده عیان ز پرده رازم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>درکعبه نماز کی گذارم</p></div>
<div class="m2"><p>تا قبله ز ابرویت نسازم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از وصل تو کی بیابم اکسیر</p></div>
<div class="m2"><p>دربوته هجر می گدازم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کی کم شودم عیار چون زر</p></div>
<div class="m2"><p>گر خود بری از دهان گازم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای آنکه ز نور رخ نهفتی</p></div>
<div class="m2"><p>بنمائی رخ اگر تو بازم</p></div></div>
<div class="b2" id="bn40"><p>برخیزم و سرنهم بپایت</p>
<p>بنشینم و جان کنم فدایت</p></div>
<div class="b" id="bn41"><div class="m1"><p>جانا چه شود که گاهگاهی</p></div>
<div class="m2"><p>برسوی من افکنی نگاهی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در فقر کمال بی نظیری</p></div>
<div class="m2"><p>درمصر جمال پادشاهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آوازه حسن وصیت خویت</p></div>
<div class="m2"><p>بگرفته زماه تا بماهی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هردم چو کشی بقصد جانم</p></div>
<div class="m2"><p>از خال خطت صف سپاهی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شاها چه شود ز خاک پایت</p></div>
<div class="m2"><p>برسر بنهم اگر کلاهی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از حادثه ام چه غم که دارم</p></div>
<div class="m2"><p>از ظل حمایتت پناهی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>باری بگذار ای جفا جوی</p></div>
<div class="m2"><p>تا من برهت بعذر خواهی</p></div></div>
<div class="b2" id="bn48"><p>برخیزم و سرنهم به پایت</p>
<p>بنشینم و جان کنم فدایت</p></div>
<div class="b" id="bn49"><div class="m1"><p>درمصر رخت مرا نباتی</p></div>
<div class="m2"><p>فرمای بآن لبان براتی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چشمت کشدم اگر بغمزه</p></div>
<div class="m2"><p>وز بوسه دهد لبت حیاتی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هم زینت کعبه به بطحا</p></div>
<div class="m2"><p>هم زیور بت به سومناتی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اشیا بوجود تو دمادم</p></div>
<div class="m2"><p>یابند حیاتی و مماتی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از قهر بقوم و لطف قومی</p></div>
<div class="m2"><p>طوفان بلائی و نجاتی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گردیده جهات از تو پیدا</p></div>
<div class="m2"><p>در ذات اگر چه بیجهاتی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرچند که حد من نباشد</p></div>
<div class="m2"><p>خواهم که مدام چون حیاتی</p></div></div>
<div class="b2" id="bn56"><p>برخیزم و سر نهم به پایت</p>
<p>بنشینم و جان کنم فدایت</p></div>