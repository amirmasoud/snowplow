---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>رو وصال خدا طلب ای یار</p></div>
<div class="m2"><p>بگذر از خویش و بگسل از اغیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم جان برگشا ببین در دل</p></div>
<div class="m2"><p>متجلی است جلوه دلدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان حجابست در ره جانان</p></div>
<div class="m2"><p>خویشتن را از آن حجاب برآر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روبه پای حریف سرمستان</p></div>
<div class="m2"><p>خوش بینداز این سرو دستار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور بر دور نقطه توحید</p></div>
<div class="m2"><p>خط کشان می درآی چون پرگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج و بحر و حباب هر سه یکیست</p></div>
<div class="m2"><p>جز یکی نیست اندک و بسیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحده لاشریک له خواهی</p></div>
<div class="m2"><p>خوش بشو گوش و بشنو اینگفتار</p></div></div>
<div class="b2" id="bn8"><p>که همه صورتند و معنی او</p>
<p>وحده لااله الاهو</p></div>
<div class="b" id="bn9"><div class="m1"><p>زاهدا چند باشی اندر خواب</p></div>
<div class="m2"><p>رو وصالش بجان و دل دریاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش بگو بر در سرای مغان</p></div>
<div class="m2"><p>افتتح یامفتح الابواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم دل باز کن ببین در دل</p></div>
<div class="m2"><p>آفتاب منیر در مهتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکزمان نزد ما درآ و نشین</p></div>
<div class="m2"><p>در خرابات عشق مست و خراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با لب لعل ساقی باقی</p></div>
<div class="m2"><p>یکدو ساغر بنوش باده ناب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوش درآ در کنار بحر و ببین</p></div>
<div class="m2"><p>عین یکدیگرند موج و حباب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل ز ظاهر چو رو بباطن کرد</p></div>
<div class="m2"><p>آمد آندم بگوش جانش خطاب</p></div></div>
<div class="b2" id="bn16"><p>که همه صورتند و معنی او</p>
<p>وحده لا اله الاهو</p></div>
<div class="b" id="bn17"><div class="m1"><p>هرکه از خویشتن شود یکتا</p></div>
<div class="m2"><p>ره برد در حریم او ادنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر کسی نور حق عیان بیند</p></div>
<div class="m2"><p>دیده از دیدنش شود بینا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جمله او گشت و از خودی برخاست</p></div>
<div class="m2"><p>هرکه بنشست یکزمان با ما</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غرقه بحر بیکران گردید</p></div>
<div class="m2"><p>هر حبابی که شد از آن دریا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا بکی بند دی و فردائی</p></div>
<div class="m2"><p>دی گذشت و نیامده فردا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ظاهر و باطن اول و آخر</p></div>
<div class="m2"><p>یک مسماست این همه اسما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزبان فصیح و لفظ ملیح</p></div>
<div class="m2"><p>سر توحید میکنم انشا</p></div></div>
<div class="b2" id="bn24"><p>که همه صورتند و معنی او</p>
<p>وحده لااله الاهو</p></div>
<div class="b" id="bn25"><div class="m1"><p>در دلم عکس یار پیدا شد</p></div>
<div class="m2"><p>سرپنهان همه هویدا شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر حبابی که بود از این دریا</p></div>
<div class="m2"><p>چون بدریا رسید دریا شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سر وحدت چو در دلم بنمود</p></div>
<div class="m2"><p>دل حریم خدای یکتا شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بی نشانش همه نشان گردید</p></div>
<div class="m2"><p>دل ز صورت چو سوی معنا شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>غیر نور خدا نخواهد بود</p></div>
<div class="m2"><p>دیده کو بنور بینا شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لذت درد ما اگر جوئی</p></div>
<div class="m2"><p>از دل دردمند شیدا شد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون بذکر خدا شدم مشغول</p></div>
<div class="m2"><p>در زبان این مقال گویا شد</p></div></div>
<div class="b2" id="bn32"><p>که همه صورتند و معنی او</p>
<p>وحده لا اله الا هو</p></div>
<div class="b" id="bn33"><div class="m1"><p>چون نهان تو در عیان دیدم</p></div>
<div class="m2"><p>بی نشان تو در نشان دیدم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حق مطلق بدل هویدا شد</p></div>
<div class="m2"><p>این منزه ز جسم و جان دیدم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از حجاب خودی شدم بکنار</p></div>
<div class="m2"><p>بارها پرده درمیان دیدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نور معنی واحد مطلق</p></div>
<div class="m2"><p>درهمه صورتی عیان دیدم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>میر سرمست لاابالی را</p></div>
<div class="m2"><p>سرور جمله عاشقان دیدم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون بذکر خدا شدم بینا</p></div>
<div class="m2"><p>سرتوحید در زبان دیدم</p></div></div>
<div class="b2" id="bn39"><p>که همه صورتند و معنی او</p>
<p>وحده لا اله الاهو</p></div>
<div class="b" id="bn40"><div class="m1"><p>شاه دلدل سوار می بینم</p></div>
<div class="m2"><p>صاحب ذوالفقار می بینم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دمبدم در تجلیات ظهور</p></div>
<div class="m2"><p>جلوه روی یار می بینم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عکس صانع بجان و دل دیدم</p></div>
<div class="m2"><p>صنعت کردگار می بینم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جز خدا نیست در نظر ما را</p></div>
<div class="m2"><p>گر یکی در هزار می بینم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مذهب عاشقان قرار گرفت</p></div>
<div class="m2"><p>دین خود برقرار می بینم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دوستان غرقه درمیان محیط</p></div>
<div class="m2"><p>دشمنان در کنار می بینم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون بدریای جان شدم پنهان</p></div>
<div class="m2"><p>هر نفس آشکار می بینم</p></div></div>
<div class="b2" id="bn47"><p>که همه صورتند و معنی او</p>
<p>وحده لا اله الا هو</p></div>
<div class="b" id="bn48"><div class="m1"><p>ما مرایای عین اشیائیم</p></div>
<div class="m2"><p>مظهر سر جمله اسمائیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گاه فانی شویم و گه باقی</p></div>
<div class="m2"><p>گاه پنهان و گاه پیدائیم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ما حریفان سید سرمست</p></div>
<div class="m2"><p>بردر دیر باده پیمائیم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گاه عاشق شویم و گه معشوق</p></div>
<div class="m2"><p>گاه مطلوب و گاه جویائیم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در خرابات عشق مست و خراب</p></div>
<div class="m2"><p>فارغ از عیش دی و فردائیم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گه نشیب و گهی فراز شویم</p></div>
<div class="m2"><p>گاه پستیم و گاه بالائیم</p></div></div>
<div class="b2" id="bn54"><p>که همه صورتند معنی او</p>
<p>وحده لااله الا هو</p></div>