---
title: >-
    شمارهٔ ۶۹۱
---
# شمارهٔ ۶۹۱

<div class="b" id="bn1"><div class="m1"><p>همچو عنقاست مرا گوشه ای از دنیا بس</p></div>
<div class="m2"><p>لب نانی بود امروز بس و فردا بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکسی چاشنی فقر و قناعت دانست</p></div>
<div class="m2"><p>همچو گوهر بودش قطره ای از دریا بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در قافله ی ریگ روان راهبری</p></div>
<div class="m2"><p>خضر ما راهروان، باد درین صحرا بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب که در انجمنم بیخودی از حد بگذشت</p></div>
<div class="m2"><p>بود معنی نگاه تو به من گویا بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تغافل نتوان گشت حریف خوبان</p></div>
<div class="m2"><p>همچو خورشید، جهان را بود او تنها بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست آوارگی آن کار که کس پیشه کند</p></div>
<div class="m2"><p>در سفر چند به سر می بری ای عنقا، بس!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب به ساقی نگهی داشتی از عجز سلیم</p></div>
<div class="m2"><p>مدعای تو: بده بود ندانم، یا بس؟</p></div></div>