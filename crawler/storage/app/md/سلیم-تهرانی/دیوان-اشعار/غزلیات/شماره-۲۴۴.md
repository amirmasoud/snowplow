---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>غنچه دلتنگ و لاله در خون است</p></div>
<div class="m2"><p>زین چمن برگ عیش بیرون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آشنایان ما درین گلشن</p></div>
<div class="m2"><p>سرو موزون و بید مجنون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوحه بر حال خویش دارد سرو</p></div>
<div class="m2"><p>این چنین است هر که موزون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمانش به خاک زنده کند</p></div>
<div class="m2"><p>هر که مغرور زر چو قارون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه از پای ما به خون خفته ست</p></div>
<div class="m2"><p>نیست شبگیر، این شبیخون است!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر به فکر جهان سلیم مپیچ</p></div>
<div class="m2"><p>کس چه داند که این جهان چون است</p></div></div>