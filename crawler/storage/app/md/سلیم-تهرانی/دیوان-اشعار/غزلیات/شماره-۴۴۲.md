---
title: >-
    شمارهٔ ۴۴۲
---
# شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>چشم خود بگشا حدیث خوش نگاهی می‌رود</p></div>
<div class="m2"><p>گردنی برکش که حرف کج‌کلاهی می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلش از من غباری هست، پنداری که باز</p></div>
<div class="m2"><p>آب چشمم از برای عذرخواهی می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهروان رفتند و چون از کاروان واماندگان</p></div>
<div class="m2"><p>برق ایشان را ز دنبال سیاهی می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه در راه کسی باشد، ازان نتوان گریخت</p></div>
<div class="m2"><p>گر روم بر آب، در پا خار ماهی می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وادی عشق است اینجا، نیست نومیدی سلیم</p></div>
<div class="m2"><p>کاروان در منزل از گم کرده راهی می‌رود</p></div></div>