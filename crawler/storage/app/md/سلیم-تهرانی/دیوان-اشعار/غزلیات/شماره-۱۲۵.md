---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>درین بساط که نقشی به مدعا ننشست</p></div>
<div class="m2"><p>کسی به پیش نیامد که بر قفا ننشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام تخت نشین یک سبق ز عشق تو خواند</p></div>
<div class="m2"><p>که همچو طفل دبستان به بوریا ننشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان چو شعله درین ره به خاروخس غلتم</p></div>
<div class="m2"><p>که خارخار دل من ز خار پا ننشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ و بوی مقید مشو چو لاله و گل</p></div>
<div class="m2"><p>به راه سیل، کسی پای در حنا ننشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گوشه ای بنشین و ز نفس ایمن شو</p></div>
<div class="m2"><p>ز سگ خلاص نگردید تا گدا ننشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به راه شوق چو برخاستی، دگر منشین</p></div>
<div class="m2"><p>نشد به خاک برابر، غبار تا ننشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل هزار تمنا ز وصل او داریم</p></div>
<div class="m2"><p>کسی به راه بتان در ره خدا ننشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کدام روز مرا سایه ای به سر انداخت</p></div>
<div class="m2"><p>که همچو تیغ به فرقم پر هما ننشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکسته پایی ازو در طریق عشق آموز</p></div>
<div class="m2"><p>به راه شوق کسی همچو نقش پا ننشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فضای هند ز بس تنگ عرصه بود سلیم</p></div>
<div class="m2"><p>نشست نقش من، اما به مدعا ننشست</p></div></div>