---
title: >-
    شمارهٔ ۵۸۰
---
# شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>از قفای زلف مشکین تو عنبر می‌دود</p></div>
<div class="m2"><p>در رکاب حلقهٔ گوش تو گوهر می‌دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زلیخا در رهت ای یوسف گل پیرهن</p></div>
<div class="m2"><p>گه به دیوار آفتاب و گاه بر در می‌دود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهروان را نیست آرامی، که همچون گردباد</p></div>
<div class="m2"><p>پا به دامن هرکه پیچیده‌ست بهتر می‌دود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطره‌قطره اشکم از لب‌تشنگی در راه شوق</p></div>
<div class="m2"><p>در سراغ آب، چون خیل سکندر می‌دود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌فروشد نکهت پیراهن او را صبا</p></div>
<div class="m2"><p>برگ گل در باغ هرسو از پی زر می‌دود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک می‌جوشد ز چشمم در قفای او سلیم</p></div>
<div class="m2"><p>پادشاهی رفته، وز دنبال، لشکر می‌دود</p></div></div>