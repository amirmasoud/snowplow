---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>چو شعله گرم درآمد، چو گل به تاب نشست</p></div>
<div class="m2"><p>چراغ باده بیارید کآفتاب نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ناامید ازو گشت، دل قرار گرفت</p></div>
<div class="m2"><p>سپند سوخته چون شد ز اضطراب نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شمع انجمن ما نسیم محرم نیست</p></div>
<div class="m2"><p>ازان ز پرده ی فانوس در نقاب نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بزم باده مرو بی صحیفه ی غزلی</p></div>
<div class="m2"><p>سفینه ای بطلب تا توان به آب نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشست یار چو پیشت، نماز چیست سلیم</p></div>
<div class="m2"><p>نماز خویش قضا کن که آفتاب نشست</p></div></div>