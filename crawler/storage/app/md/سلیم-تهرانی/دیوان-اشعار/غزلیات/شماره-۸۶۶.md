---
title: >-
    شمارهٔ ۸۶۶
---
# شمارهٔ ۸۶۶

<div class="b" id="bn1"><div class="m1"><p>ساقی ز کار من گره توبه باز کن</p></div>
<div class="m2"><p>دست مرا به گردن مینا دراز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفی ترا چه کار به جام شراب ناب</p></div>
<div class="m2"><p>کاری که کرده ای همه ی عمر، باز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد صرف باده سیم و زر ما همه بیا</p></div>
<div class="m2"><p>مطرب ز لطف دست تو بر سیم ساز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن روی از کجا و تو ای آینه، بیا</p></div>
<div class="m2"><p>ما دم نمی زنیم، تو خود امتیاز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند عندلیب ز نازت در آتش است</p></div>
<div class="m2"><p>نازت رواست، ناز کن ای غنچه، ناز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرف کله شکسته ای، آشوب خلق شو</p></div>
<div class="m2"><p>دامان فتنه برزده ای، ترکتاز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کار مختصر مگذر در طریق فقر</p></div>
<div class="m2"><p>در نان سیاه دانه ز تخم پیاز کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راحت طلب، سلیم به مقصد نمی رسد</p></div>
<div class="m2"><p>چون ناقه پای خویش به رفتن دراز کن</p></div></div>