---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>دامان طرب بهار افشاند</p></div>
<div class="m2"><p>گل بر سر روزگار افشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ از دل من نسیم برچید</p></div>
<div class="m2"><p>بر دامن لاله زار افشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردید عبیر جامه ی حور</p></div>
<div class="m2"><p>گر باد ز گل غبار افشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جامه ی شاهدان بستان</p></div>
<div class="m2"><p>شبنم، عرق بهار افشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون باز سفید، ابر خفته</p></div>
<div class="m2"><p>پر بر سر کوهسار افشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برداشت سلیم باز ساغر</p></div>
<div class="m2"><p>دست از همه کار و بار افشاند</p></div></div>