---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>برای طعنه ی ما این همه چه در جوشند</p></div>
<div class="m2"><p>که عاقلان همه دیوانه ی قبا پوشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاط مستی ما را به شب تماشا کن</p></div>
<div class="m2"><p>که روز، باده کشان چون چراغ خاموشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن ز بلبل و قمری به کام صیاد است</p></div>
<div class="m2"><p>زبس که شاخ گل و سرو، دوش بر دوشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وصال زن ای دل که همچو خمیازه</p></div>
<div class="m2"><p>بتان هند همه خانه زاد آغوشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که رفته است ازین خاکدان، نمی دانم</p></div>
<div class="m2"><p>کز آسمان، همه عالم جنازه بر دوشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر غلط نکنم، راحت از جهان رفته ست</p></div>
<div class="m2"><p>وگرنه موی بر اعضا چرا سیه پوشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلیم شکوه ازان تندخو خطر دارد</p></div>
<div class="m2"><p>خموش باش که دیوار و در همه گوشند</p></div></div>