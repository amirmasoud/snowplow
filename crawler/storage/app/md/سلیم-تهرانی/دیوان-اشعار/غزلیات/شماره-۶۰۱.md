---
title: >-
    شمارهٔ ۶۰۱
---
# شمارهٔ ۶۰۱

<div class="b" id="bn1"><div class="m1"><p>پرتوی هردم به دل فیض الهی افکند</p></div>
<div class="m2"><p>وقت آن آمد که داغ ما سیاهی افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرفرازی از سر عریان بود خورشید را</p></div>
<div class="m2"><p>شمع سر در پیش از صاحب کلاهی افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود لب تشنه را معلوم، راز تشنگی</p></div>
<div class="m2"><p>بر لب دریا اگر گوشی چو ماهی افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله در باغ از نوید مقدم او همچو شمع</p></div>
<div class="m2"><p>تاج خود را پیش باد صبحگاهی افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوششی دارد پی سرمایه ی خود هرکه هست</p></div>
<div class="m2"><p>بخت من بردارد، ار داغی سیاهی افکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مستت ریخت خونم را، بلی اینش سزاست</p></div>
<div class="m2"><p>هرکه طرح آشنایی با سپاهی افکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چتر سبز تاک را نازم که مستان را سلیم</p></div>
<div class="m2"><p>سایه اش در سر هوای پادشاهی افکند</p></div></div>