---
title: >-
    شمارهٔ ۹۱۴
---
# شمارهٔ ۹۱۴

<div class="b" id="bn1"><div class="m1"><p>درین ره ای خضر از خار پا نمی‌میری</p></div>
<div class="m2"><p>ترا گمان که ز آب بقا نمی‌میری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه راستی‌ام یاد داد و گفت چو خضر</p></div>
<div class="m2"><p>به دست تا بودت این عصا نمی‌میری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو در محیط تعلق نمردی ای درویش</p></div>
<div class="m2"><p>ز موج بی‌خطر بوریا نمی‌میری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جام عشق اگر آب زندگی نوشی</p></div>
<div class="m2"><p>چو شعله در دهن اژدها نمی‌میری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین که زهر به کار تو استخوانم کرد</p></div>
<div class="m2"><p>به حیرتم که چرا ای هما نمی‌میری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بحر عشق چه کار است ای نهنگ ترا</p></div>
<div class="m2"><p>به مرگ خویش چو ماهی چرا نمی‌میری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ستمکشان همه از جورت ای فلک مردند</p></div>
<div class="m2"><p>تو از برای چه ای بی‌وفا نمی‌میری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به درد عشق چو نبضم مسیح دید سلیم</p></div>
<div class="m2"><p>به خنده گفت مرا مرحبا نمی‌میری</p></div></div>