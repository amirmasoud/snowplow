---
title: >-
    شمارهٔ ۶۹۰
---
# شمارهٔ ۶۹۰

<div class="b" id="bn1"><div class="m1"><p>راه شوقی نسپردم افسوس</p></div>
<div class="m2"><p>خجل از پای خودم چون طاووس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت عشق نخواهد افسر</p></div>
<div class="m2"><p>چه کند مرغ چمن تاج خروس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند باشیم ز تنگی جهان</p></div>
<div class="m2"><p>زنده در گور چو شمع فانوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست گلزار به سامان دلم</p></div>
<div class="m2"><p>بس که چیدم ز لبش غنچه ی بوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کنم در شب وصلش افغان</p></div>
<div class="m2"><p>ناله ام کفر بود چون ناقوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فلک رفت سلیم از دل ما</p></div>
<div class="m2"><p>مسند عشق چو تخت کاوس</p></div></div>