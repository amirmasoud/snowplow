---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>زاهد همه عمرم به می ناب گذشته ست</p></div>
<div class="m2"><p>چون سبزه ی جو از سر من آب گذشته ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دامن دل رفته مرا چاک گریبان</p></div>
<div class="m2"><p>کارم ز رفوکاری احباب گذشته ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جرعه ی آبم به گلو تیغ گذارد</p></div>
<div class="m2"><p>تا باز چه در خاطر قصاب گذشته ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردید تر آن چشم چو افتاد به اشکم</p></div>
<div class="m2"><p>چون آهوی صحرا که ز سیلاب گذشته ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر غیر، غم عشق تو آسان ز وصال است</p></div>
<div class="m2"><p>این بادیه را در شب مهتاب گذشته ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داند که به کوشش نرود کار کسی پیش</p></div>
<div class="m2"><p>هر کس که به سروقت رسن تاب گذشته ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی ناله و بی اشک سلیم از غم دل نیست</p></div>
<div class="m2"><p>عمرش به همین طور چو دولاب گذشته ست</p></div></div>