---
title: >-
    شمارهٔ ۹۰۱
---
# شمارهٔ ۹۰۱

<div class="b" id="bn1"><div class="m1"><p>سحر زان شمع باشد تاب خورده</p></div>
<div class="m2"><p>که او شب باده در مهتاب خورده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم بی نغمه ذوق از می ندارد</p></div>
<div class="m2"><p>گل ما آب از دولاب خورده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود پرورده ی سرگشتگی دل</p></div>
<div class="m2"><p>چو گوهر آب از گرداب خورده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازان زاهد ندارد ذوق گلشن</p></div>
<div class="m2"><p>که دایم باده در محراب خورده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بی تابی گل این باغ گویی</p></div>
<div class="m2"><p>که آب از شبنم سیماب خورده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مخورمی چون تراغم در کمین است</p></div>
<div class="m2"><p>گران خیز است صید آب خورده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلیم از می دلم خرم نگردد</p></div>
<div class="m2"><p>چه داند باده را، خوناب خورده</p></div></div>