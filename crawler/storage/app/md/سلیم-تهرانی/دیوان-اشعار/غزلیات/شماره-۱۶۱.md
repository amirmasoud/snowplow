---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>زهی ز شوق لبت زاهدان شراب پرست</p></div>
<div class="m2"><p>چو گل به دور رخت شبنم آفتاب پرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه قاتلی تو ندانم که خضر بر لب جوی</p></div>
<div class="m2"><p>به یاد تیغ تو شد همچو سبزه آب پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طواف چشم بتان واجب است بر دل ما</p></div>
<div class="m2"><p>شرابخانه بود کعبه ی شراب پرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان ز مقدم قاصد خوشم به مژده ی وصل</p></div>
<div class="m2"><p>که از ستاره ی صبح است آفتاب پرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدم نمی نهد از ننگ شمع در محفل</p></div>
<div class="m2"><p>بود به کوی تو پروانه ماهتاب پرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیچ و تاب چو افتد سلیم می میرد</p></div>
<div class="m2"><p>کسی مباد چو زلف تو پیچ و تاب پرست</p></div></div>