---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>پیری نشد خزان گل شاخسار ما</p></div>
<div class="m2"><p>موی سفید ماست چو عنبر بهار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهد به کار آمد اگر خاک هم شویم</p></div>
<div class="m2"><p>افلاک را چو شیشه ی ساعت غبار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردیم و گفتگوی بزرگانه کم نشد</p></div>
<div class="m2"><p>آید صدای کوه ز سنگ مزار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرمی ندیده ایم به عمر خود از کسی</p></div>
<div class="m2"><p>جام شراب ما بود آب خمار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویشی ست در میانه ی ما و گل دورنگ</p></div>
<div class="m2"><p>یک پشت می رسد به خزان نوبهار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افلاک و انجمند به کاری، ولی سلیم</p></div>
<div class="m2"><p>کاری نمی کنند که آید به کار ما</p></div></div>