---
title: >-
    شمارهٔ ۶۵۸
---
# شمارهٔ ۶۵۸

<div class="b" id="bn1"><div class="m1"><p>در کوی تو دیوانه مرا نام نهادند</p></div>
<div class="m2"><p>طفلان چه بزرگانه مرا نام نهادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پای خمم ناف به طفلی چو بریدند</p></div>
<div class="m2"><p>دردی کش میخانه مرا نام نهادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من طالع چوب گل و زنجیر ندارم</p></div>
<div class="m2"><p>احباب، چه دیوانه مرا نام نهادند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه نام چراغی به جهان بود، نه شمعی</p></div>
<div class="m2"><p>آن روز که پروانه مرا نام نهادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامم کسی از هیچ زبانی نشنیده ست</p></div>
<div class="m2"><p>افلاک چه بیگانه مرا نام نهادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایام، فلاطون جهان داد خطابم</p></div>
<div class="m2"><p>فریاد که طفلانه مرا نام نهادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دادند گهی برگ گل و لاله خطابم</p></div>
<div class="m2"><p>گاهی پر پروانه مرا نام نهادند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خال لب او شدم آواره ی عالم</p></div>
<div class="m2"><p>عنقا، پی این دانه مرا نام نهادند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در زلف تو- آن کوچه ی زنجیرفروشان-</p></div>
<div class="m2"><p>عمری ست که دیوانه مرا نام نهادند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخت از پی معموری من بود سلیما</p></div>
<div class="m2"><p>آن روز که ویرانه مرا نام نهادند</p></div></div>