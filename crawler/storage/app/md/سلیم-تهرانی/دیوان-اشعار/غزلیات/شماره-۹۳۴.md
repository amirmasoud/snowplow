---
title: >-
    شمارهٔ ۹۳۴
---
# شمارهٔ ۹۳۴

<div class="b" id="bn1"><div class="m1"><p>ای کاش زخم سینه ی ما واکند کسی</p></div>
<div class="m2"><p>شاید ترحمی به دل ما کند کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما چو برق می گذرد آفتاب ما</p></div>
<div class="m2"><p>کو فرصتی که عرض تمنا کند کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تکلیف جلوه قامت او را ز عقل نیست</p></div>
<div class="m2"><p>آن فتنه را برای چه برپا کند کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس را چو تاب دیدن او نیست در جهان</p></div>
<div class="m2"><p>گردد چه آفتاب که پیدا کنی کسی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسرو به طعنه گفت که پنداشت کوهکن</p></div>
<div class="m2"><p>کاری ست کار عشق که تنها کند کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید هرکجا که حدیث تو بشنود</p></div>
<div class="m2"><p>باید که اضطراب تماشا کند کسی!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نام وطن، ملال غریبی فزون کند</p></div>
<div class="m2"><p>باید سفر به شیوه ی عنقا کند کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سهل است زر به خاک چو خورشید ریختن</p></div>
<div class="m2"><p>از خاک، زر خوش است که پیدا کند کسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل چه پیش می روی، آن به که در جهان</p></div>
<div class="m2"><p>از دور چون ستاره تماشا کند کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیوانگی سلیم به جایی نمی رسد</p></div>
<div class="m2"><p>خود را به کوی عشق چه رسوا کند کسی</p></div></div>