---
title: >-
    شمارهٔ ۸۰۴
---
# شمارهٔ ۸۰۴

<div class="b" id="bn1"><div class="m1"><p>به افسون محبت دست آتش را به مو بندم</p></div>
<div class="m2"><p>چو غنچه بر گل کاغذ، طلسم رنگ و بو بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتم سهل کار عشق را، بر من جهان خندد</p></div>
<div class="m2"><p>که می خواهم ره سیلاب را چون آب جو بندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن ها از زبان من به آن بی باک می گویند</p></div>
<div class="m2"><p>شوم خاموش و بر احباب راه گفتگو بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمر در خدمت بت خود مرا از کار افتاده ست</p></div>
<div class="m2"><p>مگر زنار خود را همچو قمری بر گلو بندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم امشب ز مستی دل اناالحق می زند دیگر</p></div>
<div class="m2"><p>نشد ممکن که بتوانم دهان این سبو بندم</p></div></div>