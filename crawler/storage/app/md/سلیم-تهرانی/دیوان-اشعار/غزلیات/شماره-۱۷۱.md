---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>ای گل شکفته شو که به جا می‌فرستمت</p></div>
<div class="m2"><p>یعنی به یار سست‌وفا می‌فرستمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاصد بگیر نامه و دست مرا ببوس</p></div>
<div class="m2"><p>آگاه نیستی که کجا می‌فرستمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار تا تمام شود نامه، ای صبا</p></div>
<div class="m2"><p>بی‌طاقتی مکن، به خدا می‌فرستمت!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاصد ز رشک، سایهٔ خود را نمی‌برد</p></div>
<div class="m2"><p>ای گریه صبر کن ز قفا می‌فرستمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌پیشه خوب نیست کسی در جهان سلیم</p></div>
<div class="m2"><p>سوی چمن به کسب هوا می‌فرستمت</p></div></div>