---
title: >-
    شمارهٔ ۷۶۹
---
# شمارهٔ ۷۶۹

<div class="b" id="bn1"><div class="m1"><p>ای خوش آن ساعت که روی خنجری رنگین کنم</p></div>
<div class="m2"><p>در میان خود خود، بال و پری رنگین کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شب ای شاخ گل رعنا در آغوشم درآ</p></div>
<div class="m2"><p>کز فروغ عارضت دوش و بری رنگین کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کف خود لاله ی دل را ندیدم یک نفس</p></div>
<div class="m2"><p>زین حنا تا چند دست دیگری رنگین کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوبهار عمر رفت و بخت بد یاری نکرد</p></div>
<div class="m2"><p>کز گل وصل تو روی بستری رنگین کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک معنی را گرفتم، وقت آن آمد سلیم</p></div>
<div class="m2"><p>همچو شمع از سرفرازی افسری رنگین کنم</p></div></div>