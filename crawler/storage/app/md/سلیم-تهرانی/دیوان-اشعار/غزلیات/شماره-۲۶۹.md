---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>بیا که فصل خوش روزگار نزدیک است</p></div>
<div class="m2"><p>زمان سیر گل و لاله زار نزدیک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتاده ام به طلسم قفس، چه چاره کنم</p></div>
<div class="m2"><p>ز باغ دورم و فصل بهار نزدیک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حرف عشق، همین کوهکن بود امروز</p></div>
<div class="m2"><p>که پاره ای سخن او به کار نزدیک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوید دولت دنیا چنان بود کز مهر</p></div>
<div class="m2"><p>دهند مژده به مجرم که دار نزدیک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نیست قوت یک گام رفتنم از ضعف</p></div>
<div class="m2"><p>چه سود ازین که ره کوی یار نزدیک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث قرب وطن پیش من مگوی، چه سود</p></div>
<div class="m2"><p>که سیل بادیه را کوهسار نزدیک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خانه می نتوان خورد در بهار سلیم</p></div>
<div class="m2"><p>کنار کشت و لب جویبار نزدیک است</p></div></div>