---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>قصه ی منصور را سر کرده است</p></div>
<div class="m2"><p>باز صوفی از کجا بر کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار در دعوی زبان را تیز کرد</p></div>
<div class="m2"><p>گوش خود را گل ازان کر کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد دگر ارزان متاع بیخودی</p></div>
<div class="m2"><p>کاروان بوی گل سر کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوخ چشمی های خوبان هم بلاست</p></div>
<div class="m2"><p>خنده ی گل ابر را تر کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش زاهد توبه کردم از شراب</p></div>
<div class="m2"><p>ساده لوحی بین که باور کرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبت پاکان نباشد بی اثر</p></div>
<div class="m2"><p>رشته را هموار، گوهر کرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکوه ی من نیست از رهزن سلیم</p></div>
<div class="m2"><p>آنچه با من کرده، رهبر کرده است</p></div></div>