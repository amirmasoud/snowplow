---
title: >-
    شمارهٔ ۷۷۴
---
# شمارهٔ ۷۷۴

<div class="b" id="bn1"><div class="m1"><p>ز لاله و گل این باغ، کی خبر داریم</p></div>
<div class="m2"><p>گل همیشه بهار جنون به سر داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به راه شوق همین کار با کف پا نیست</p></div>
<div class="m2"><p>تنی پرآبله چون رشتهٔ گهر داریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فضای سینه چنان زین چمن به ما تنگ است</p></div>
<div class="m2"><p>که همچو بیضه دل خود به زیر پر داریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوای لعل تو دارد سبک عنان ما را</p></div>
<div class="m2"><p>چو کودکان همه مرکب ز نیشکر داریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بعد کشته شدن دست برمدار از ما</p></div>
<div class="m2"><p>که مطلب تو اگر جان بود، دگر داریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب عشق ترا این قدر خمار از چیست</p></div>
<div class="m2"><p>که سر به کوی تو شد خاک و دردسر داریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هند چند توان بود، کاشکی چون مور</p></div>
<div class="m2"><p>برون رویم ازین ملک تا کمر داریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تیغ غمزه ی خوبان سلیم سر نکشیم</p></div>
<div class="m2"><p>چه شد، اگرچه نداریم دل، جگر داریم</p></div></div>