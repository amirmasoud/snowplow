---
title: >-
    شمارهٔ ۴۸۶
---
# شمارهٔ ۴۸۶

<div class="b" id="bn1"><div class="m1"><p>رفت اشکم که سری بر گذر یار کشد</p></div>
<div class="m2"><p>صورت حال مرا بر در و دیوار کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناخنی بایدش از برگ گل آورد به چنگ</p></div>
<div class="m2"><p>هرکه خواهد ز دل مرغ چمن خار کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاقت و صبر ازین بیش ندارم، وقت است</p></div>
<div class="m2"><p>که مرا شور جنون بر سر بازار کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره شوق تو افتد چو گذارم به چمن</p></div>
<div class="m2"><p>بلبلم از کف پا خار به منقار کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ خوشتر بود از رحمت احباب، سلیم</p></div>
<div class="m2"><p>مرهم از زخم دلم تا به کی آزار کشد</p></div></div>