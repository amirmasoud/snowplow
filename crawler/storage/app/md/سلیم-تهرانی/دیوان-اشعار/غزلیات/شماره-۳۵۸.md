---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>طالع من شد ضعیف از بس غم افلاک خورد</p></div>
<div class="m2"><p>رنگ و روی اخترم زرد است از بس خاک خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تمام عمر، زاهد روزه نتوان داشتن</p></div>
<div class="m2"><p>روزی خود را چرا باید بدین امساک خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرعه ای گفتم مگر صوفی ازان خواهد کشید</p></div>
<div class="m2"><p>از کفم پیمانه را کافر گرفت و پاک خورد!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان گر قرص خورشیدم دهد، دور افکنم</p></div>
<div class="m2"><p>گر همه چون شعله ام باید خس و خاشاک خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه را جام شرابی دسترس باشد سلیم</p></div>
<div class="m2"><p>چون شقایق از چه می باید دگر تریاک خورد؟</p></div></div>