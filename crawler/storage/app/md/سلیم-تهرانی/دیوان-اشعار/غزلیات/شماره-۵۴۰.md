---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>از بزم می چو آن قد رعنا بلند شد</p></div>
<div class="m2"><p>آتش چو شمع از سر مینا بلند شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس به سینه ی آه شکستم ز بیم او</p></div>
<div class="m2"><p>دودم چو مجمر از همه اعضا بلند شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پهلو به بستری که نهادم، ز سوز دل</p></div>
<div class="m2"><p>آه و فغان ز صورت دیبا بلند شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانگان او چو خس و خار می دوند</p></div>
<div class="m2"><p>تا دست گردباد ز صحرا بلند شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرجا حدیث ما رود، او نیز داخل است</p></div>
<div class="m2"><p>نام فلک ز دشمنی ما بلند شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه و فغان من به فلک شعله زد سلیم</p></div>
<div class="m2"><p>بگریز ای حریف که غوغا بلند شد</p></div></div>