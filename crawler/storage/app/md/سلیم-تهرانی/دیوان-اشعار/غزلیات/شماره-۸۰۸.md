---
title: >-
    شمارهٔ ۸۰۸
---
# شمارهٔ ۸۰۸

<div class="b" id="bn1"><div class="m1"><p>از زمین آزرده ام، وز آسمان رنجیده ام</p></div>
<div class="m2"><p>دوستان! رنجیده ام زین دشمنان، رنجیده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو بادامم اگر بر چشم صد سوزن زنند</p></div>
<div class="m2"><p>چشم نگشایم ز هم، بس کز جهان رنجیده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد درای محملم ناقوس بر عزم فرنگ</p></div>
<div class="m2"><p>کز عراق آزرده وز هندوستان رنجیده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه بادا باد، تنها می روم این راه را</p></div>
<div class="m2"><p>نکهت پیراهنم، از کاروان رنجیده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده ی صافی نمی سازد دل ناصاف را</p></div>
<div class="m2"><p>می نمی خواهم، که از پیرمغان رنجیده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اتفاق دست و دل باید، به هرکاری که هست</p></div>
<div class="m2"><p>گل نمی گیرم به کف، کز باغبان رنجیده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایه ی خار بیابانم به از صد گلشن است</p></div>
<div class="m2"><p>بلبلم، اما ز باغ و بوستان رنجیده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه بر گل می زنم خود را، گهی بر خار و خس</p></div>
<div class="m2"><p>طایر رم کرده ام، از آشیان رنجیده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنجشم با دوستان افزون ز شوق افتاده است</p></div>
<div class="m2"><p>مژده باد ای دشمنان کز دوستان رنجیده ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برق من کی خویش را غافل به خرمن می زند</p></div>
<div class="m2"><p>با خبر باشید، گفتم دوستان، رنجیده ام!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیغی از هر پر حمایل کرده ام همچون همای</p></div>
<div class="m2"><p>بعد ازین خون می خورم کز استخوان رنجیده ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>انتقام چرخ را کلکم ازیشان می کشد</p></div>
<div class="m2"><p>وای بر اهل جهان کز آسمان رنجیده ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک کس از آسیب من مشکل که جان بیرون برد</p></div>
<div class="m2"><p>در رمه افتاده گرگ از شبان رنجیده ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این قصیده شد غزل، آخر ز روی اختصار</p></div>
<div class="m2"><p>تا به کی گویم سلیم از این و آن رنجیده ام</p></div></div>