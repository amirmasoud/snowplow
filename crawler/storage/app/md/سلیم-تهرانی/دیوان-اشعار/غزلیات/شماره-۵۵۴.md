---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>به هر چمن که دلم با فغان درون آید</p></div>
<div class="m2"><p>ز داغ لاله ی او تا به حشر خون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شوق دیدن من سر به کوه و دشت نهد</p></div>
<div class="m2"><p>ز هر دیار که دیوانه ای برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی شود به فسون رام با کسی این مار</p></div>
<div class="m2"><p>مرا به دست، سر زلف یار چون آید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر به جانب گل بی رخ تو نگشایم</p></div>
<div class="m2"><p>به دیده ام چو گل چشم اگر درون آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فیض عشق بنازم که آفتاب سلیم</p></div>
<div class="m2"><p>به دیدنم همه صبح از پی شگون آید</p></div></div>