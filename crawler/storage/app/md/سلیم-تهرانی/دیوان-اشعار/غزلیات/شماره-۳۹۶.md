---
title: >-
    شمارهٔ ۳۹۶
---
# شمارهٔ ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>به کوی عشق اسیران به بوی یار روند</p></div>
<div class="m2"><p>که بلبلان به چمن از پی بهار روند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پای خم چو نشینند می کشان به نشاط</p></div>
<div class="m2"><p>به فکر کار نیفتند تا ز کار روند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هلاک شیوه ی تمکین بی قرارانم</p></div>
<div class="m2"><p>که همچو کوه نشینند و چون غبار روند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو در برون چمن بگذری، همه گل ها</p></div>
<div class="m2"><p>پی نظاره به بالای شاخسار روند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مومیایی لطفی امید نیست سلیم</p></div>
<div class="m2"><p>شکستگان به سر کوی او چه کار روند</p></div></div>