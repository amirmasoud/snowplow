---
title: >-
    شمارهٔ ۷۰۳
---
# شمارهٔ ۷۰۳

<div class="b" id="bn1"><div class="m1"><p>هرکه را ساز بود زمزمه ی زنجیرش</p></div>
<div class="m2"><p>می کند ناله ی مرغان چمن دلگیرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خضر آید به ره قاتل ما تا بیند</p></div>
<div class="m2"><p>صورت حال خود از آینه ی شمشیرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذری بر دل سودازده ی ما نکند</p></div>
<div class="m2"><p>در کمانخانه مگر چله نشین شد تیرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزگاری ست که بر هرکه نظر اندازی</p></div>
<div class="m2"><p>همچو گوهر تهی از آب نباشد شیرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دعای سحری رام دلم گشت سلیم</p></div>
<div class="m2"><p>آفتابی که نکرده ست کسی تسخیرش</p></div></div>