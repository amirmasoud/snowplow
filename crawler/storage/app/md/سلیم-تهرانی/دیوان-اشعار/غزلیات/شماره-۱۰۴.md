---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>شورش مغز جنون نالهٔ مستانهٔ ماست</p></div>
<div class="m2"><p>شانهٔ طرهٔ آشفتگی افسانهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بال و پر نیست که خود را برساند جایی</p></div>
<div class="m2"><p>گریهٔ شمع به نومیدی پروانهٔ ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختیار سر ما تیغ محبت دارد</p></div>
<div class="m2"><p>موج سیلاب، کلید در ویرانهٔ ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده جز در دل شب فیض نبخشد ما را</p></div>
<div class="m2"><p>گل شب‌بو که شنیدی، گل پیمانهٔ ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط خوبان همه دلجوی بود، آه سلیم</p></div>
<div class="m2"><p>چشم صد مور گرسنه پی یک دانهٔ ماست</p></div></div>