---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>سحر شد و دم تأثیر ناله می‌گذرد</p></div>
<div class="m2"><p>تو خفته ای به کمین و غزاله می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چیست این همه تعجیل عمر، حیرانم</p></div>
<div class="m2"><p>که تند و تلخ چو دور پیاله می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رخصت تو که خواهم به آب می شستن</p></div>
<div class="m2"><p>نه صفحه را، که سخن در رساله می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز باد صبح سبک‌خیزتر بود، آری</p></div>
<div class="m2"><p>نگه به روی تو بر برگ لاله می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبا ز حلقهٔ زلف تو بوی مشک گرفت</p></div>
<div class="m2"><p>گمان بری که ز ناف غزاله می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش آنکه دختر رز در نقاب عصمت بود</p></div>
<div class="m2"><p>بیا سلیم که حرف دوساله می‌گذرد</p></div></div>