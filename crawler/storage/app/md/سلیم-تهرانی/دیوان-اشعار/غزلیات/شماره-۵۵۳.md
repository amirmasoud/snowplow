---
title: >-
    شمارهٔ ۵۵۳
---
# شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>می کشان در انجمن چون حرف لعل او زنند</p></div>
<div class="m2"><p>شیشه و پیمانه از مستی به هم پهلو زنند!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاه خوبرویان است، چندان دور نیست</p></div>
<div class="m2"><p>سرو [و] شمشاد چمن گر پیش او زانو زنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوک مژگانی درون چشمشان نشکسته است</p></div>
<div class="m2"><p>گلرخان از بی غمی زان تیر بر آهو زنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جور خود را بر ضعیفان آزماید روزگار</p></div>
<div class="m2"><p>تیغ را دایم برای امتحان بر مو زنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم بر اصلاح غمخواران سلیم از ابلهی ست</p></div>
<div class="m2"><p>زخم نتوان زد به خود، گر بخیه را نیکو زنند</p></div></div>