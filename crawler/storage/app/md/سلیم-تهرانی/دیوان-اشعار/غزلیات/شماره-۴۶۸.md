---
title: >-
    شمارهٔ ۴۶۸
---
# شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>ز گریه گشت مرا دیده ی خراب سفید</p></div>
<div class="m2"><p>به رنگ گل که شود در میان آب سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برابری به مه من نمی کند هرگز</p></div>
<div class="m2"><p>چنین خوش است ادب، روی آفتاب سفید!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفای سوختگی بین و فیض آتش عشق</p></div>
<div class="m2"><p>که چون نمک شده خاکستر کباب سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تاب آتش می بس که چهره اش افروخت</p></div>
<div class="m2"><p>به پیش او نتواند شدن نقاب سفید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس که بی تو سیه دید روزگار مرا</p></div>
<div class="m2"><p>ز گریه شد مژه در چشم آفتاب سفید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنند اهل محبت ز فیض عشق سلیم</p></div>
<div class="m2"><p>کتان خویش به صابون ماهتاب سفید</p></div></div>