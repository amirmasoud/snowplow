---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>بهار آمد و سر تا به سر جهان سبز است</p></div>
<div class="m2"><p>ز فیض ابر، زمین تا به آسمان سبز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خانه بهر چه بیرون رود، که بلبل را</p></div>
<div class="m2"><p>ز یاد رفته چمن، بس که آشیان سبز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار کو که نشیند به دامن صحرا؟</p></div>
<div class="m2"><p>ز بس چو آب چمن، راه کاروان سبز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو چگونه توان طعن زردرویی زد؟</p></div>
<div class="m2"><p>که همچو ریشه ی گل، رنگ زعفران سبز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چوب تیر همین سبز شد، که پنداری</p></div>
<div class="m2"><p>نهاده وسمه بر ابرو ز بس کمان سبز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز لطف جوهر آتش به حشر هندو را</p></div>
<div class="m2"><p>هنوز چون قلم سوسن استخوان سبز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز خرمی نوبهار رنگی نیست</p></div>
<div class="m2"><p>چه سود دسته ی گل را که ریسمان سبز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز لطف ابر بهار است هرچه هست، ولی</p></div>
<div class="m2"><p>چمن ز آبله ی دست باغبان سبز است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خنک تر است ز خس خانه آشیانه ی ما</p></div>
<div class="m2"><p>درین چمن که به دلگرمی خزان سبز است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خزان رسید و حریفان نشسته اند به خاک</p></div>
<div class="m2"><p>بجز شراب که جایش به بوستان سبز است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمانه زهرفروش است ازان شکر مطلب</p></div>
<div class="m2"><p>که زهر خورده، از ان رنگ طوطیان سبز است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به نوبهار خط سبز نازم و اثرش</p></div>
<div class="m2"><p>که تا رسیده سخن بر سر زبان، سبز است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سلیم جام می از کف منه چو لاله که باز</p></div>
<div class="m2"><p>شکفته شد گل و اطراف بوستان سبز است</p></div></div>