---
title: >-
    شمارهٔ ۶۶۵
---
# شمارهٔ ۶۶۵

<div class="b" id="bn1"><div class="m1"><p>حسن، شیرین را ازان می پرورد دایم به شیر</p></div>
<div class="m2"><p>کز برای کشتن فرهاد گردد شیرگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ازو زخمی نگیرم دلپسند خویشتن</p></div>
<div class="m2"><p>برنمی دارم سر از دنبال پیکانش چو تیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می دهم دل را و می گیرم پریشانی ازو</p></div>
<div class="m2"><p>گر درین سودا نباشد طره ی او شانه گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چمن هر گه به او همراه می بیند مرا</p></div>
<div class="m2"><p>از پی سر چون رقیبان می کشد بلبل صفیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما پریشان خاطران در بند سامان نیستیم</p></div>
<div class="m2"><p>خط آزادی ست بر اندام ما نقش حصیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد این موی سفید از خویش ما را ناامید</p></div>
<div class="m2"><p>شکر از خود دست شوید در کنار جوی شیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دو روزی دیگری را پیش می آرد سلیم</p></div>
<div class="m2"><p>می کند دوران چو طفلان بازی میر و وزیر</p></div></div>