---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>دل رمیده ام از خنده ی تو بیزار است</p></div>
<div class="m2"><p>به دیده موج قدح، می گزیده را مار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فزود زردی رخسارم از می گلگون</p></div>
<div class="m2"><p>که باده رنگ مرا آب زعفران زار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسیح را نگذارد برون ز خانه ی خویش</p></div>
<div class="m2"><p>که آفتاب ز عشقت همیشه بیمار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سر نمانده ز پیری مرا هوای لباس</p></div>
<div class="m2"><p>به فرق، موی سفیدم چو شمع دستار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز گفتگوی لب او مپرس حال مرا</p></div>
<div class="m2"><p>که پا پر آبله و راه در نمکزار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل شکسته ام از جور پاجیان خون شد</p></div>
<div class="m2"><p>چو هند، هر نفر هند هم جگرخوار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلیم از بد و نیک جهان همین دانم</p></div>
<div class="m2"><p>که هر چه هست درین کارخانه در کار است</p></div></div>