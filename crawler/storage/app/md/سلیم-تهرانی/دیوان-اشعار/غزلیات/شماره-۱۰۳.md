---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>بخت بد با اخترم هر شب به جنگ افتاده است</p></div>
<div class="m2"><p>این سیاهی گویی از داغ پلنگ افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد این صحرا ز بازیگاه طفلان می دهد</p></div>
<div class="m2"><p>هر کجا پا می نهد دیوانه، سنگ افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاک های سینه ام هر یک در بتخانه ای ست</p></div>
<div class="m2"><p>از دل من کار بر اسلام تنگ افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمه ای از مجلس مستان نمی گردد بلند</p></div>
<div class="m2"><p>ناخن مطرب مگر امشب ز چنگ افتاده است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شکست شیشه ی دل ها به او فرصت نداد</p></div>
<div class="m2"><p>آتش از رشک دلش در جان سنگ افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تحفه ی دل کی به چشم آید که در بزم بتان</p></div>
<div class="m2"><p>گل ز بی قدری چو مصحف در فرنگ افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تهمت وسعت چه می بندی برین عالم سلیم</p></div>
<div class="m2"><p>نیست او را در وسعتی، چشم تو تنگ افتاده است</p></div></div>