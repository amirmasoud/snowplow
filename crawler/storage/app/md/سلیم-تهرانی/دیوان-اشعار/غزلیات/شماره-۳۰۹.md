---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>با وجود صد هنر، لافم ز شعر دلکش است</p></div>
<div class="m2"><p>خامه در دست هنرور، تیر روی ترکش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی کس کی خورد هرگز کسی، زان چوب را</p></div>
<div class="m2"><p>آب نتواند فرو بردن که رزق آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چمن ترسم رود آخر به تاراج خزان</p></div>
<div class="m2"><p>آنچه اسباب تعلق غنچه را در مفرش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس را تحریک نتوان کرد در لهو و لعب</p></div>
<div class="m2"><p>تازیانه آفتی باشد چو توسن سرکش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قسمت ما نیست آبی در جهان، ورنه سلیم</p></div>
<div class="m2"><p>آنچه بسیار است در غمخانه ی ما، آتش است</p></div></div>