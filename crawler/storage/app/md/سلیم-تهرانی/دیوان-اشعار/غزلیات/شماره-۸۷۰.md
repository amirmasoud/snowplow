---
title: >-
    شمارهٔ ۸۷۰
---
# شمارهٔ ۸۷۰

<div class="b" id="bn1"><div class="m1"><p>خاطر من نشکفد از وصل یار خویشتن</p></div>
<div class="m2"><p>این چمن رنگی ندارد از بهار خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق از بس غافلم کرده ست از خود، می کنم</p></div>
<div class="m2"><p>همچو طفلان خاکبازی با غبار خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر کار محبت، جان به حسرت دادن است</p></div>
<div class="m2"><p>می تراشد کوهکن سنگ مزار خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تمام عمر می سوزم برای دیگران</p></div>
<div class="m2"><p>آتشم، آتش نمی آید به کار خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهل باشد گر سوار توسن گردون شوی</p></div>
<div class="m2"><p>جهد کن تا چون صبا گردی سوار خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکسی سر در کنار یار دارد، من سلیم</p></div>
<div class="m2"><p>می نهم چون غنچه شب سر در کنار خویشتن</p></div></div>