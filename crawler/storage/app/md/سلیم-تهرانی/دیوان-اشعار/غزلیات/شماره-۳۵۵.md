---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>شد باغ از بهار، سفید و سیاه و سرخ</p></div>
<div class="m2"><p>مرغان شاخسار، سفید و سیاه و سرخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم ز گریه در ره شوق تو دیده ای</p></div>
<div class="m2"><p>چون ابر نوبهار، سفید و سیاه و سرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد رنگ موج جلوه درین بحر می کند</p></div>
<div class="m2"><p>چون نقش پشت مار، سفید و سیاه و سرخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردیده داغ کهنه و نو جمع در دلم</p></div>
<div class="m2"><p>همچون زر قمار، سفید و سیاه و سرخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گه سلیم جام ز دشمن گرفت یار</p></div>
<div class="m2"><p>گشتم هزار بار سفید و سیاه و سرخ</p></div></div>