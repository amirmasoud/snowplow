---
title: >-
    شمارهٔ ۸۶۵
---
# شمارهٔ ۸۶۵

<div class="b" id="bn1"><div class="m1"><p>عجب مدار ز زاهد شراب ما خوردن</p></div>
<div class="m2"><p>که نیست ننگ گدا، روزی گدا خوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو هست باده، غم نان مخور که مستان را</p></div>
<div class="m2"><p>چو گل زیان نهد آب ناشتا خوردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان قناعت فقر است سازگار مرا</p></div>
<div class="m2"><p>که چون حباب شوم فربه از هوا خوردن!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به راه شوق مرا نیست توشه ای همراه</p></div>
<div class="m2"><p>بود چو شعله مدارم به خار پا خوردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صلای باده ی عیشم زمانه داد سلیم</p></div>
<div class="m2"><p>روم به خانه ی دشمن به خونبها خوردن</p></div></div>