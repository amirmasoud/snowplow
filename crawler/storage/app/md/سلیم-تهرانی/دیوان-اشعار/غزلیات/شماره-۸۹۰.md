---
title: >-
    شمارهٔ ۸۹۰
---
# شمارهٔ ۸۹۰

<div class="b" id="bn1"><div class="m1"><p>دل چو می رفت سوی زلف تو، شد جان همراه</p></div>
<div class="m2"><p>به ره هند شدند این دو پریشان همراه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولین گام به ره ماند چو میل فرسنگ</p></div>
<div class="m2"><p>گردبادی که مرا شد به بیابان همراه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل دیوانهٔ ما فصل گل از عریانی</p></div>
<div class="m2"><p>نه گریبان به چمن برد و نه دامان همراه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رخ ساقی و چشم تر من مستان را</p></div>
<div class="m2"><p>همه جا ابر رفیق است و گلستان همراه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خوشی باشدم از سیر گل و لاله سلیم؟</p></div>
<div class="m2"><p>گر نباشد به من آن سرو خرامان همراه</p></div></div>