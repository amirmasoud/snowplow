---
title: >-
    شمارهٔ ۷۴۴
---
# شمارهٔ ۷۴۴

<div class="b" id="bn1"><div class="m1"><p>رفت آن شمع و ز حسرت شد لب پیمانه خشک</p></div>
<div class="m2"><p>برگ گل شد در چمن همچون پر پروانه خشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از وصال او مرا آبی به روی کار بود</p></div>
<div class="m2"><p>پنجه ام بی زلف او شد همچو دست شانه خشک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد شکایت در دل، اما لب ندارد زان خبر</p></div>
<div class="m2"><p>در درون خانه سیل و آستان خانه خشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه از جوش و خروش آسیا آید مرا</p></div>
<div class="m2"><p>حیرتی دارم که چون گردیده چشم دانه خشک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تغافل های ابر نوبهاری در چمن</p></div>
<div class="m2"><p>غنچه شد همچون دماغ بلبل دیوانه خشک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی توانم برگرفتن یک قدم از جای خویش؟</p></div>
<div class="m2"><p>چون خم می پای من گردیده در میخانه خشک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک دم از آوارگی ایام نگذارد سلیم</p></div>
<div class="m2"><p>تا چو آیینه کنم آب و عرق در خانه خشک</p></div></div>