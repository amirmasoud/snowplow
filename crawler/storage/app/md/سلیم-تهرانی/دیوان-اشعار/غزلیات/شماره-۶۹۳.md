---
title: >-
    شمارهٔ ۶۹۳
---
# شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>زاهد از رشک این قدر گرم عتاب ما مباش</p></div>
<div class="m2"><p>گر توان فکر شرابی کن، کباب ما مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطلبی در گفتگوی مردم دیوانه نیست</p></div>
<div class="m2"><p>همچو مخمل در پی تعبیر خواب ما مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسبت ما می برد از چهره رنگ اعتبار</p></div>
<div class="m2"><p>گر حسابی داری از خود، در حساب ما مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاکساری پیش مغروران ندارد اعتبار</p></div>
<div class="m2"><p>ذره باش، اما اسیر آفتاب ما مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطره ی ما کار صد دریای رحمت می کند</p></div>
<div class="m2"><p>ای گیاه تشنه، نومید از سحاب ما مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عنان او نظر کردم به سوی ماه نو</p></div>
<div class="m2"><p>گفت ای دیوانه دیگر در رکاب ما مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جنون باشد اگر ما گفتگویی می کنیم</p></div>
<div class="m2"><p>نیستی دیوانه، در بند جواب ما مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای خنجر عشقبازان ترا در سینه نیست</p></div>
<div class="m2"><p>ما نهنگانیم، گو ماهی در آب ما مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما چو بیهوشیم از کیفیت آن لب سلیم</p></div>
<div class="m2"><p>خنده گو بیهوشداروی شراب ما مباش</p></div></div>