---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>رهنمایی چون کنم در دیدن او دیده را؟</p></div>
<div class="m2"><p>حاجت تعلیم نبود مردم فهمیده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کسی بیرون نمی آرد سری از زلف او</p></div>
<div class="m2"><p>شانه داند معنی این مصرع پیچیده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم از اشکم مگر گردون بپرهیزد، ولی</p></div>
<div class="m2"><p>نیست بیم از گریه ام این گرگ باران دیده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زمان طالع ما تیره روزان بس نشد</p></div>
<div class="m2"><p>فتنه زاییدن شب گیسو به خون غلتیده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>التفات و مهربانی را عبث ضایع مکن</p></div>
<div class="m2"><p>مشکل است اصلاح کردن خاطر رنجیده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده را از دیدن رویت تسلی ساختم</p></div>
<div class="m2"><p>چون دهم تسکین نمی دانم دل نادیده را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیب شاکر کی شود ظاهر سلیم از شعر فهم؟</p></div>
<div class="m2"><p>با محک نشناخت هرگز کس زر دزدیده را</p></div></div>