---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>ساغر گلی ز گلشن بیهوشی من است</p></div>
<div class="m2"><p>سرمه غبار کوچه ی خاموشی من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من شعله ام، نهان نتوان داشت شعله را</p></div>
<div class="m2"><p>ایام بی سبب پی خس پوشی من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فصل بهار و مستی مرغان تمام شد</p></div>
<div class="m2"><p>آمد خزان و وقت قدح نوشی من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازم به رازداری خود چون صدف که بحر</p></div>
<div class="m2"><p>از موج، جمله لب پی سرگوشی من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیراهنم ز یاد لبش بوی می گرفت</p></div>
<div class="m2"><p>خمیازه سینه چاک هم آغوشی من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هر لباس، جوهر زیبندگی خوش است</p></div>
<div class="m2"><p>آیینه داغ طرز نمدپوشی من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساغر سلیم من به حریفان نمی دهم</p></div>
<div class="m2"><p>موقوف مستی تو به بیهوشی من است</p></div></div>