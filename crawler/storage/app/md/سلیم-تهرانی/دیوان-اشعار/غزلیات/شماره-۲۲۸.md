---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>هوای تخت ندارد کسی که گوشه‌نشین است</p></div>
<div class="m2"><p>سر و دلی که به ما داده‌اند تاج و نگین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غیر عیب جهان بر زبانشان سخنی نیست</p></div>
<div class="m2"><p>مدار صحبت آزادگان عشق برین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سر رویم همه شب به کوی یار و عبث نیست</p></div>
<div class="m2"><p>کلاه ما که چو پاپوش شبروان نمدین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گمان زر چو ندارد دل حریص ملول است</p></div>
<div class="m2"><p>که خنده رویی هندو ز زعفران جبین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اعتماد سمند تو داغم ای شه خوبان</p></div>
<div class="m2"><p>که هرچه هست ترا در رکاب خانه ی زین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل سلیم کند هر کجا اراده ی طاعت</p></div>
<div class="m2"><p>اشاره می کند ابروی او که قبله چنین است</p></div></div>