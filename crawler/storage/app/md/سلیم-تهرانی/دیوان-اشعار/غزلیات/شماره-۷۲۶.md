---
title: >-
    شمارهٔ ۷۲۶
---
# شمارهٔ ۷۲۶

<div class="b" id="bn1"><div class="m1"><p>زهی نهال خزان دیده ای ز باغ تو شمع</p></div>
<div class="m2"><p>فکنده تیر به تاریکی از سراغ تو شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وسیله ای ست مرا در جنون عشق تو گل</p></div>
<div class="m2"><p>فتیله ای ست مرا از برای داغ تو شمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس فتیله ی عنبر به خویش می دزدد</p></div>
<div class="m2"><p>بیان کند چو حدیث گل چراغ تو شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب و شاهد و گل عرض می کند بر تو</p></div>
<div class="m2"><p>دماغ چند بسوزد پی دماغ تو شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشد ز گریه ی مستانه یک نفس خاموش</p></div>
<div class="m2"><p>سلیم تا شده سرگرم از ایاغ تو شمع</p></div></div>