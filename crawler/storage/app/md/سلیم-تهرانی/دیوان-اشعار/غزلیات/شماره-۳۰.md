---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>در قفس رفته چو قمری چمن از یاد مرا</p></div>
<div class="m2"><p>بهتر از سرو بود سایهٔ صیاد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همنشین، ضعف من افزون شود از سیرچمن</p></div>
<div class="m2"><p>باخبر باش که ناگه نبرد باد مرا!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عیادت نرود بر سر بیمار، اجل</p></div>
<div class="m2"><p>دوستی نیست اگر یار کند یاد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرنوشتم چه به کار است چو می دانم کار</p></div>
<div class="m2"><p>نیستم طفل که سرخط دهد استاد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست افسوس چراغی که نماید روشن</p></div>
<div class="m2"><p>که درین خانه ی تاریک چه افتاد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم آبی که جهان قسمت من کرد سلیم</p></div>
<div class="m2"><p>گه به بنگاله برد، گاه به بغداد مرا</p></div></div>