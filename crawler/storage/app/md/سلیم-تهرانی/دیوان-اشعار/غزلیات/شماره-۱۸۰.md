---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>منم که مایه ی جمعیتم پریشانی ست</p></div>
<div class="m2"><p>چو شعله زینت من در لباس عریانی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آینه همه عمرم به یک نگاه گذشت</p></div>
<div class="m2"><p>چه حالت است، نمی دانم این چه حیرانی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ما نمانده به غیر از بنای عشق اثری</p></div>
<div class="m2"><p>عمارتی که بماند ز سیل، ویرانی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دل شکسته ی تقدیر، ازو چه نفع رسد</p></div>
<div class="m2"><p>که مومیایی تدبیر عقل، انسانی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بخت حادثه انگیز خود مرا دایم</p></div>
<div class="m2"><p>چو گردباد به خشکی سفینه طوفانی ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهر ز شرم گدا بس که آب شد، گویی</p></div>
<div class="m2"><p>که تاج بر سر شاهان کلاه بارانی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلیم منت خلعت ز آفتاب مکش</p></div>
<div class="m2"><p>که جامه ای که به اندام ماست، عریانی ست</p></div></div>