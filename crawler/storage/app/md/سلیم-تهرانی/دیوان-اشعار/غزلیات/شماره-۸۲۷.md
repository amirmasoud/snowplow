---
title: >-
    شمارهٔ ۸۲۷
---
# شمارهٔ ۸۲۷

<div class="b" id="bn1"><div class="m1"><p>هردم غمی آید ز حریفان لئیمم</p></div>
<div class="m2"><p>گویی به دبستان جهان، طفل یتیمم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن گلبن فیضم که ز شادابی فطرت</p></div>
<div class="m2"><p>در جوش بود مغز جهانی ز شمیمم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فکر همه فیض، چو گفتار رسولم</p></div>
<div class="m2"><p>در نطق همه معجزه، چون دست کلیمم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوقم چو برد جانب گلزار، سحابم</p></div>
<div class="m2"><p>راهم چو فتد در حرم غنچه، نسیمم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمتر نیم از سنجر و فغفور، که من هم</p></div>
<div class="m2"><p>در هند سیه بختی خود، شاه سلیمم</p></div></div>