---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>گریه طوفان می کند از نکهت محبوب ما</p></div>
<div class="m2"><p>همچو دریا باد باشد باعث آشوب ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو جنونی تا همه عالم ز ما چینند گل</p></div>
<div class="m2"><p>باغبان تا کی گل خود را کند سرکوب ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو دل ویرانه ای داریم پر گرد و غبار</p></div>
<div class="m2"><p>وز برای خاک رفتن، دست ما جاروب ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفته ایم از یاد یاران، گرچه دارد در وطن</p></div>
<div class="m2"><p>کاغذ بادی به کف هر طفل از مکتوب ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک هند از آه ما تاریک شد، زان رو سلیم</p></div>
<div class="m2"><p>درنمی‌آید به چشم خلق، زشت و خوب ما</p></div></div>