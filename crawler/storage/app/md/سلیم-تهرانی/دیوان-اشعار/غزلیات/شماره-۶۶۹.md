---
title: >-
    شمارهٔ ۶۶۹
---
# شمارهٔ ۶۶۹

<div class="b" id="bn1"><div class="m1"><p>مست گر نیستی از شیوهٔ مستان مگذر</p></div>
<div class="m2"><p>قطره ی آب گهر باش و ز طوفان مگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پا به اندازه کشیدن روش اهل دل است</p></div>
<div class="m2"><p>همه تن اشک شو و از سر مژگان مگذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدن قاصد بی مژده، غم آرد ای باد</p></div>
<div class="m2"><p>نکهت پیرهنت نیست، به کنعان مگذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر از سایه درو دام جنون افکنده ست</p></div>
<div class="m2"><p>گر ز من می شنوی، سوی گلستان مگذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما درین بادیه چون ریگ روانیم سلیم</p></div>
<div class="m2"><p>تو که مرغ چمنی، سوی بیابان مگذر</p></div></div>