---
title: >-
    شمارهٔ ۹۱۶
---
# شمارهٔ ۹۱۶

<div class="b" id="bn1"><div class="m1"><p>چنان از رهروان فیض شد روی زمین خالی</p></div>
<div class="m2"><p>که جای موج در دریاست چون نقش نگین خالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس در خویش دزدیدند، از همت کریمان را</p></div>
<div class="m2"><p>ز دست جود چون بند قبا شد آستین خالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس پهلوی موج او در آیین کرم خشک است</p></div>
<div class="m2"><p>ز دریا می‌کند از ننگ پهلو را زمین خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود هر قطرهٔ خون، تخم آهی چون سپند اینجا</p></div>
<div class="m2"><p>ازین ریحان مبادا باغ دل های حزین خالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غیر از حلقهٔ فتراک یارم خوابگاهی نیست</p></div>
<div class="m2"><p>مبادا از سر من هرگز آن دامان زین خالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم از اضطراب مور عاجز، برق می خندد</p></div>
<div class="m2"><p>ز خرمن دامن خود را برد چون خوشه‌چین خالی</p></div></div>