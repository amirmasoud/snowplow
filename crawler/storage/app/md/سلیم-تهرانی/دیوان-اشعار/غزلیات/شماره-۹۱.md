---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>بهار است و چمن چون روی محبوب</p></div>
<div class="m2"><p>چو قد یار، هر سروی دل آشوب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از موج و دم ماهی گشاید</p></div>
<div class="m2"><p>صفای خانه از آب است و جاروب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کبوتر را فرستادم به سویش</p></div>
<div class="m2"><p>خط آزادی اش دادم ز مکتوب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گروهی نیستند ابنای عالم</p></div>
<div class="m2"><p>که بگذارند یوسف را به یعقوب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خونخواری جهانی اوفتاده</p></div>
<div class="m2"><p>مرا در پوست، همچون کرم ایوب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قفس از شعله ی آواز ما سوخت</p></div>
<div class="m2"><p>چنین می باشد آتشخانه ی چوب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن کردن نمی آید ز هر کس</p></div>
<div class="m2"><p>تو می دانی سلیم این شیوه را خوب</p></div></div>