---
title: >-
    شمارهٔ ۴۷۹
---
# شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>نکته سنجان! صفحه را از وصف می گلگون کنید</p></div>
<div class="m2"><p>مصرعی در پای هر سرو چمن موزون کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شورش ایشان ز مستی نیست، از دیوانگی‌ست</p></div>
<div class="m2"><p>بلبلان را از چمن با چوب گل بیرون کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مزاج هرکسی باشد شرابی سازگار</p></div>
<div class="m2"><p>نوبت ما چون رسد، پیمانه را پرخون کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش بساط سبزه ای افکنده در صحرا بهار</p></div>
<div class="m2"><p>آهوان خوش باشد، اما کفش را بیرون کنید!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع را کی می‌گذارد باد در صحرا سلیم</p></div>
<div class="m2"><p>نقش لیلی را چراغ تربت مجنون کنید</p></div></div>