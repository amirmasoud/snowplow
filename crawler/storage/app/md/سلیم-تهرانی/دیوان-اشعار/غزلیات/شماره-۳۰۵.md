---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>واقف کسی ز شیوه ی آن کج کلاه نیست</p></div>
<div class="m2"><p>چون صورت فرنگ، نگاهش نگاه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ها ز راز یکدگر آگاه نیستند</p></div>
<div class="m2"><p>آیینه را به خانه ی آیینه راه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دایم چو آفتاب سفید است روی من</p></div>
<div class="m2"><p>چشمی مرا به سرمه ی مردم سیاه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی برگی جهان همه معلوم من شده ست</p></div>
<div class="m2"><p>گل چون طلب کنم که به باغش گیاه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل مشو که مردم درویش را سلیم</p></div>
<div class="m2"><p>در خرقه هست پشمی، اگر در کلاه نیست</p></div></div>