---
title: >-
    شمارهٔ ۶۵۴
---
# شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>آنچه در پرده ی گل بود نهان، روی تو بود</p></div>
<div class="m2"><p>گره غنچه گشودیم، در آن بوی تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلخنم بود به از باغ، که در چشم مرا</p></div>
<div class="m2"><p>آتش و دود در آن، روی تو و موی تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا زمزمه ی مرغ سحرخیزی خاست</p></div>
<div class="m2"><p>نیک چون گوش نهادیم، دعاگوی تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد آوارگی از قرب تو مانع ما را</p></div>
<div class="m2"><p>هر کجا پای نهادیم سر کوی تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حشمت چشم تو از چشم مرا دور نشد</p></div>
<div class="m2"><p>روی صحرا همه بر گله ی آهوی تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن روزی که ترا جامه ی رعنایی داد</p></div>
<div class="m2"><p>همچو سرو چمن آن تا سر زانوی تو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتی و شورش مرغان چمن آخر شد</p></div>
<div class="m2"><p>می توان یافت که آنها ز گل روی تو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنجه ام را به گریبان سروکار افتاده ست</p></div>
<div class="m2"><p>ای خوش آن روز که آن شانه ی گیسوی تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود در راه ترا قطع بیابانی چند</p></div>
<div class="m2"><p>ورنه در خانه ی خود کعبه به پهلوی تو بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست در کار محبت چو تو فرهاد، سلیم</p></div>
<div class="m2"><p>تیشه ی کوهکنی در خور بازوی تو بود</p></div></div>