---
title: >-
    شمارهٔ ۶۸۷
---
# شمارهٔ ۶۸۷

<div class="b" id="bn1"><div class="m1"><p>ای خوش آن سروری که از ناموس</p></div>
<div class="m2"><p>چتردار خود است چون طاووس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عار آید ز لاف، مردان را</p></div>
<div class="m2"><p>پر ندیده کسی به تاج خروس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاسبان خودند آگاهان</p></div>
<div class="m2"><p>هم چراغ است لاله، هم فانوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز ذوق هوای ابر آورد</p></div>
<div class="m2"><p>بط می را به جلوه چون طاووس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رغبت طبع را برانگیزد</p></div>
<div class="m2"><p>گردن شیشه، همچو ساق عروس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق باشد سلیم در دل من</p></div>
<div class="m2"><p>باده در شیشه، شمع در فانوس</p></div></div>