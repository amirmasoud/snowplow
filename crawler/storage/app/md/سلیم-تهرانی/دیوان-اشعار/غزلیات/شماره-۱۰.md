---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای ز مژگانت مرا چون خوشه در دل تیرها</p></div>
<div class="m2"><p>بر سرم چون برگ بید از غمزه ات شمشیرها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خفته در راه تو از عجز ای غزال شیرگیر</p></div>
<div class="m2"><p>دست بر بالای یکدیگر نهاده شیرها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه در راه تو عمرم صرف شد چون گردباد</p></div>
<div class="m2"><p>دارم از ریگ بیابان بیشتر تقصیرها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صیدگاه کیست این صحرا که از زخم خدنگ</p></div>
<div class="m2"><p>سایه پنداری پلنگ است از پی نخجیرها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر اسباب تعلق کردن از دیوانگی ست</p></div>
<div class="m2"><p>موج دریای جنون ماست این زنجیرها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایه ی تیغ است عاشق را مقام عافیت</p></div>
<div class="m2"><p>برگ نی باشد حصاری از برای شیرها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفر و دین را عشق صلحی داد کز اندام خود</p></div>
<div class="m2"><p>چون زره کردند جوهر را برون شمشیرها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست آزادی سلیم از حلقهٔ فتراک عشق</p></div>
<div class="m2"><p>صیدگاه اوست عالم، ما همه نخجیرها</p></div></div>