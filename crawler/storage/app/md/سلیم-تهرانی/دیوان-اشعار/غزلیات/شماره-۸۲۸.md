---
title: >-
    شمارهٔ ۸۲۸
---
# شمارهٔ ۸۲۸

<div class="b" id="bn1"><div class="m1"><p>کرشمه سنج نگاه ستیزه جویانیم</p></div>
<div class="m2"><p>سواد خوان الف قامتان مژگانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من حکایت مجنون و کوهکن بشنو</p></div>
<div class="m2"><p>که ما فلک زدگان، طفل یک دبستانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بخت ماست که ما را زمانه نشناسد</p></div>
<div class="m2"><p>اگرچه سودهٔ قندیم، در نمکدانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز قید چرخ به مستی کنیم فکر گریز</p></div>
<div class="m2"><p>که باده پرتو مهتاب و ما غلامانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طریق کار جهان را ز ما چه می پرسی</p></div>
<div class="m2"><p>نه ایم خضر، که مجنون این بیابانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیده گلشن روحانیان چه می دانی</p></div>
<div class="m2"><p>که در سفال فلک، خوش نشین چو ریحانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود به سایه ی ما هرکجا اسیری هست</p></div>
<div class="m2"><p>به تنگنای جهان ما چراغ زندانیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلیم، ظاهر ما را مبین، به باطن بین</p></div>
<div class="m2"><p>که دوست آینه و ما چو آینه دانیم</p></div></div>