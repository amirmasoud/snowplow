---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>امشب گل شکفتگی ما دو رنگ بود</p></div>
<div class="m2"><p>نقل شراب، پسته ی خندان تنگ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی ز چهره آینه بر روی بزم داشت</p></div>
<div class="m2"><p>مطرب ز پنجه، شانه کش زلف چنگ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل از حجاب لاله رخان حال غنچه داشت</p></div>
<div class="m2"><p>شکر ز خنده ی لب خوبان به تنگ بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کرد مدعی به من اظهار دوستی</p></div>
<div class="m2"><p>امشب ستاره پنبه ی داغ پلنگ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر موج کز محیط پرآشوب روزگار</p></div>
<div class="m2"><p>برخاست، چون ملاحظه کردم نهنگ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ننمود مرگ هیچ کس از بیم جان مرا</p></div>
<div class="m2"><p>اوقات عمر من همه چون روز جنگ بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد دلم شکسته ز تندی بوی گل</p></div>
<div class="m2"><p>آن روزگار رفت که این شیشه سنگ بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون صورت فرنگ، نگاه تو عام شد</p></div>
<div class="m2"><p>رفت آن زمان که عرصه بر احباب تنگ بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موج شراب، صیقل آن تا نشد سلیم</p></div>
<div class="m2"><p>چون برگ تاک، آینه ام زیر زنگ بود</p></div></div>