---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>شد خزان و در چمن رنگ دگر افلاک ریخت</p></div>
<div class="m2"><p>برگ جمعیت فراهم کن که برگ تاک ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مشامم بوی او هرگاه آمد از نسیم</p></div>
<div class="m2"><p>همچو غنچه از گریبانم به دامن چاک ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بساط این چمن تا همتم دامن فشاند</p></div>
<div class="m2"><p>هر گلی کز خون به دامن داشتم هم پاک ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشته همچون موج لرزد بر سر آب گهر</p></div>
<div class="m2"><p>بس که آب روی پاکان را جهان بر خاک ریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیسه ی صد پاره ی گل، زر نمی دارد نگاه</p></div>
<div class="m2"><p>هر کجا داغی نهادم، بر دل صد چاک ریخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانده از آشفتگی دستم ز هر کاری سلیم</p></div>
<div class="m2"><p>خاک بر فرقم غبار این دل غمناک ریخت</p></div></div>