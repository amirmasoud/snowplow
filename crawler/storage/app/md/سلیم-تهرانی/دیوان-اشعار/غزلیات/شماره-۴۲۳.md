---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>از نفس آنچه دلم دید، ز زنجیر ندید</p></div>
<div class="m2"><p>گریه را چون سخن عشق گلوگیر ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صبا پای سبک نه، که درین ره مجنون</p></div>
<div class="m2"><p>آنچه از نقش قدم دید ز زنجیر ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش ما رسم تواضع ز تواضع خیزد</p></div>
<div class="m2"><p>خم نشد گردن ما تا خم شمشیر ندید!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خیال تو به جاسوسی دل ها برخاست</p></div>
<div class="m2"><p>هیچ کس را بجز از من ز تو دلگیر ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده معشوق خود آن را، عجبی نیست اگر</p></div>
<div class="m2"><p>روی نان را به همه عمر، گدا سیر ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافل از جلوه ی سبزان نتوان بود سلیم</p></div>
<div class="m2"><p>آنچه در هند دلم دید، به کشمیر ندید</p></div></div>