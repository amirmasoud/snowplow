---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>خوش آن زمان که سر مهر بود خوبان را</p></div>
<div class="m2"><p>کرشمه منع نمی کرد آه و افغان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین نبود در وصل بسته بر دل ها</p></div>
<div class="m2"><p>نداشت قفل حرم، پره ی بیابان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ضعف، بند قبا آستین من شده است</p></div>
<div class="m2"><p>نشانه ای به ازین نیست عشق پنهان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش باده فروش آن قدر گرو جمع است</p></div>
<div class="m2"><p>که نام نیست در آن خاتم سلیمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز قید، کیست که آزادی آرزو نکند؟</p></div>
<div class="m2"><p>ز کاهلی ست ثبات قدم غلامان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نوبهار جوانی ز کف پیاله منه</p></div>
<div class="m2"><p>که می ز موج کند ریشخند، پیران را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فوت گشتن دندان چه غم، سلامت باد</p></div>
<div class="m2"><p>زبان ما که ولی نعمت است دندان را!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازو هزار کرامات دیده ایم سلیم</p></div>
<div class="m2"><p>شراب کهنه بود پیر جام، مستان را</p></div></div>