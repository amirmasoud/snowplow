---
title: >-
    شمارهٔ ۵۴۳
---
# شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>زلفت ز من حزین گریزد</p></div>
<div class="m2"><p>این خوشه ز خوشه چین گریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دست تو گل ز شرم رویت</p></div>
<div class="m2"><p>وقت است در آستین گریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر تو ز صحبت دل ما</p></div>
<div class="m2"><p>با موزه ی آهنین گریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آفت برق، دانه ی ما</p></div>
<div class="m2"><p>چون مور سوی زمین گریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی ز جهان چو برق جستن؟</p></div>
<div class="m2"><p>بیدرد! کسی چنین گریزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از باده سلیم چون کشم دست؟</p></div>
<div class="m2"><p>کی مور ز انگبین گریزد؟</p></div></div>