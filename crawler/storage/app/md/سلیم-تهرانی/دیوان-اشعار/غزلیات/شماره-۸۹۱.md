---
title: >-
    شمارهٔ ۸۹۱
---
# شمارهٔ ۸۹۱

<div class="b" id="bn1"><div class="m1"><p>روی ننمایی، نباشد تا رخت پرداخته</p></div>
<div class="m2"><p>چند چون آیینه می نازی به حسن ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعده اش با دیگران، وز انتظار او مرا</p></div>
<div class="m2"><p>خشک شد همچون صراحی، گردن افراخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذت زخم کهن را مرهم ای دل از تو برد</p></div>
<div class="m2"><p>فکر تیر تازه ای کن چون حریف باخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر کوی مغان، طفلی که آید در وجود</p></div>
<div class="m2"><p>خاتم جم آورد با خود چو طوق فاخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی شکستی نیست یک مو در سراپایم سلیم</p></div>
<div class="m2"><p>عشق، پنداری مرا از آسمان انداخته</p></div></div>