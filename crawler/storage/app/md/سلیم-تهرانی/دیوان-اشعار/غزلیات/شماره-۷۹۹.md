---
title: >-
    شمارهٔ ۷۹۹
---
# شمارهٔ ۷۹۹

<div class="b" id="bn1"><div class="m1"><p>ز شوقت گاه در دنبال گل همچون صبا افتم</p></div>
<div class="m2"><p>گهی بر دست و پای گلرخان همچون حنا افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر راهی نهاده شوق در پیشم که از شادی</p></div>
<div class="m2"><p>به پای خویشتن در هر قدم چون نقش پا افتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین ضعفم مددکاری به راه شوق می‌باید</p></div>
<div class="m2"><p>به خضرم دسترس چون نیست، در پای عصا افتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا کاری به جز افتادگی چون نیست در عالم</p></div>
<div class="m2"><p>گر از خاک در میخانه برخیزم، کجا افتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه نقصانی مرا بر پایهٔ قدر و شرف دارد</p></div>
<div class="m2"><p>اگر بر خاک ره چون سایهٔ بال هما افتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا طالع به سوی مقصدی هرگز نشد رهبر</p></div>
<div class="m2"><p>به خاک ناامیدی چند چون تیر خطا افتم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قسمت زان نمی‌نالم، که همچون دانه می‌دانم</p></div>
<div class="m2"><p>ز چنگ مور اگر گردم رها، در آسیا افتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیم غمگین اگر بخت سیه آواره‌ام دارد</p></div>
<div class="m2"><p>چو خال روی خوبان خوش‌نمایم، هرکجا افتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به درویشی سلیم از بس که خو کردم، پس از مردن</p></div>
<div class="m2"><p>چو آتش زنده می‌گردم، اگر بر بوریا افتم</p></div></div>