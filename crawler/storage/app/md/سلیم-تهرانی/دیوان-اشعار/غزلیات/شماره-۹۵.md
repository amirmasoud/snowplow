---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>از برای رونق وحدت به کثرت جای ماست</p></div>
<div class="m2"><p>عالم صورت به معنی جامهٔ دیبای ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حریم قرب، عمری شد که دور افتاده‌ایم</p></div>
<div class="m2"><p>هر کجا در بزم او خالی ست، آنجا جای ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تلاش مدعا، گر برنخیزد دور نیست</p></div>
<div class="m2"><p>خواب همچون دست مخمل باف، کار پای ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این که می خندند [و] می گویند مستان در چمن</p></div>
<div class="m2"><p>جای می خالی ست، مطلب ساغر و مینای ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گرانباری حسرت بس که سنگین می رود</p></div>
<div class="m2"><p>در ره او کوه پنداری به پشت پای ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق را بی چشم گریان، رتبه ای نبود سلیم</p></div>
<div class="m2"><p>آب چون گوهر فروشان رونق کالای ماست</p></div></div>