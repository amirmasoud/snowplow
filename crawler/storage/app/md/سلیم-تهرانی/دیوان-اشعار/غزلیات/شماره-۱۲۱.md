---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>دارد حذر ز فتنه ی او هر که عاقل است</p></div>
<div class="m2"><p>همچون شراب کهنه، جهان پیر جاهل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز شد حجاب تماشای او مرا</p></div>
<div class="m2"><p>آتش زنم بر آن، همه گر پرده ی دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس که با غبار دل آلوده می رود</p></div>
<div class="m2"><p>دایم ز آب دیده ی خود راه من گل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ورطه ای که قسمتم افکنده، موج را</p></div>
<div class="m2"><p>بر سر سفینه نیست، که تابوت ساحل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سنگلاخ کام و هوس تاختن دلیر</p></div>
<div class="m2"><p>بر توسن برهنه ی تجرید مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خاطرم سلیم برد باده زنگ غم</p></div>
<div class="m2"><p>موج شراب، صیقل آیینه ی دل است</p></div></div>