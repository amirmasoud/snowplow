---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>باغبان را چشم لطف از مصلحت بر سوی ماست</p></div>
<div class="m2"><p>احتیاج زعفران زارش به آب روی ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قوت رفتار می خواهیم و توفیق طلب</p></div>
<div class="m2"><p>کاسهٔ دریوزهٔ ما کاسهٔ زانوی ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارهای کوهکن را فرصت شهرت نداد</p></div>
<div class="m2"><p>باطل السحری که از عشق تو بر بازوی ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که بر صاحبدلان بگذشت، فیضی می برد</p></div>
<div class="m2"><p>ما سبوی باده ایم و جام در پهلوی ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر جاویدی ز هر مصرع شود حاصل سلیم</p></div>
<div class="m2"><p>آبروی چشمهٔ حیوان روان در جوی ماست</p></div></div>