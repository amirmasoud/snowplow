---
title: >-
    شمارهٔ ۶۳۶
---
# شمارهٔ ۶۳۶

<div class="b" id="bn1"><div class="m1"><p>شراب صحبت اهل جهان صداع آرد</p></div>
<div class="m2"><p>جنون کجاست که سنگ از پی نزاع آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین خرابه دم رفتن است، عشق کجاست</p></div>
<div class="m2"><p>که هوش را به سرم از پی وداع آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام ذوق و چه مستی، به جای خود بنشین</p></div>
<div class="m2"><p>ترا که خرقه چو پروانه در سماع آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ لاله به پیش رخ تو بی نور است</p></div>
<div class="m2"><p>که دزد همره خود شمع کم شعاع آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کارهای موافق مخور فریب جهان</p></div>
<div class="m2"><p>چو آن اصول که زن در دم جماع آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم لخت دلی چند، سوی ایران برد</p></div>
<div class="m2"><p>چو آن کسی که ز هندوستان متاع آرد</p></div></div>