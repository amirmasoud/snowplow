---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>ای نهاده حسن را برطاق از ابروی بلند</p></div>
<div class="m2"><p>بسته دست عالمی را با دو گیسوی بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جذبه ی کوی خراباتم اگر گشتی رفیق</p></div>
<div class="m2"><p>چون کبوتر می زدم از کعبه یاهوی بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتگوی زلف او ای دل چو خواهی سر کنی</p></div>
<div class="m2"><p>نام بردن احتیاجی نیست: هندوی بلند!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نصیبم کرده همت از مراد روزگار</p></div>
<div class="m2"><p>در کمندم کوتهی آمد ز بازوی بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل دنیا به همت درنیامیزد سلیم</p></div>
<div class="m2"><p>می رود آب روان دشوار در جوی بلند</p></div></div>