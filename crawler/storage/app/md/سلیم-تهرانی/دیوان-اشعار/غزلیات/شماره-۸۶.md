---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>نیست ممکن جرعه ی آبی نباشد با نصیب</p></div>
<div class="m2"><p>تشنگان یاخضر می گویند و عارف یانصیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر یک خوان، جهانی روزی خود می خورد</p></div>
<div class="m2"><p>می برند از شربت آبی همه اعضا نصیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کسی را روزی از جایی مقرر کرده اند</p></div>
<div class="m2"><p>قطره ی آبی صدف را نیست از دریا نصیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار و خس را اختیاری بر سر سیلاب نیست</p></div>
<div class="m2"><p>می روم سرگشته و حیران، برد هر جا نصیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردن مینا و ساق ساقی و ران شکار</p></div>
<div class="m2"><p>نعمتی باشد، شود این هر سه چون یک جا نصیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکوه ی آوارگی ما را سلیم از عشق نیست</p></div>
<div class="m2"><p>از ازل ما را غریبی بود چون عنقا نصیب</p></div></div>