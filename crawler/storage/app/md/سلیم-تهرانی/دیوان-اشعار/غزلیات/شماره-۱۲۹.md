---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>جهان بر خود مرا واله گرفته ست</p></div>
<div class="m2"><p>مرا خود دل ز شهر و ده گرفته ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خاک این چمن، تهمت چه بندم</p></div>
<div class="m2"><p>غبار از خود دلم چون به گرفته ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمندم با وجود لاغری ها</p></div>
<div class="m2"><p>همیشه آهوی فربه گرفته ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگردد گریه ی مستانه ام کم</p></div>
<div class="m2"><p>که این باران شب شنبه گرفته ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم از اعتقاد حسن طبع است</p></div>
<div class="m2"><p>که عالم را به خود واله گرفته ست</p></div></div>