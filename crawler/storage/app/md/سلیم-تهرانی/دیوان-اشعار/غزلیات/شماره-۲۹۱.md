---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>نوای مرغ شباهنگ، ناله ی نی ماست</p></div>
<div class="m2"><p>ستاره ی سحر ما پیاله ی می ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمانده قاعده ی تازه ای، پیاله بگیر</p></div>
<div class="m2"><p>که آنچه کهنه به عالم نمی شود می ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شد اگر به غریبی کنیم یاد وطن</p></div>
<div class="m2"><p>که در قبیله سماع از نوای یاحی ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستاره نیست همین خصم ما، که همچون صبح</p></div>
<div class="m2"><p>همیشه تیغ به کف آفتاب در پی ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم، تی تی مستان هند آخر شد</p></div>
<div class="m2"><p>پیاله گیر که اکنون زمان هی هی ماست</p></div></div>