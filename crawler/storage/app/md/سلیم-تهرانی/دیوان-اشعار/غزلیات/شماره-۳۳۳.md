---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>گر عاشقی، از گنه چه باک است</p></div>
<div class="m2"><p>خورشید به هرچه تافت پاک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ دلم از غبار خاطر</p></div>
<div class="m2"><p>چون حلقه ی دام، زیر خاک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرجا برخاست صرصر عشق</p></div>
<div class="m2"><p>آنجا دو جهان، دو مشت خاک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسبت ازلی اگر نباشد</p></div>
<div class="m2"><p>چون نخل کدو شبیه تاک است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند گره زنیم چون دام</p></div>
<div class="m2"><p>بر پرده ی دل که چاک چاک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیلی هر چند آن قدر نیست</p></div>
<div class="m2"><p>مجنون ز برای او هلاک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد جامه اگر چو گل بپوشم</p></div>
<div class="m2"><p>در نیم نفس، تمام چاک است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناخن همه کس سلیم دارد</p></div>
<div class="m2"><p>گر سینه نمی کند چه باک است</p></div></div>