---
title: >-
    شمارهٔ ۲۵۲
---
# شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>سوختن از تب سوزان محبت تابی ست</p></div>
<div class="m2"><p>تشنگی از چمن عشق، گل سیرابی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انتظار غمی از هر طرفی دارد دل</p></div>
<div class="m2"><p>چشم ویرانه ز هر سو به ره سیلابی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کعبه هرچند که محراب ندارد، اما</p></div>
<div class="m2"><p>بر سر کوی تو هر نقش قدم محرابی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جهان دل به غم عشق تو الفت دارد</p></div>
<div class="m2"><p>همچو دیوانه که همصحبت گلخن تابی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد امشب سر پیمانه کشیدن داریم</p></div>
<div class="m2"><p>قیمت شمع به می ده، که عجب مهتابی ست!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در غم عشق ز مردن مکن اندیشه سلیم</p></div>
<div class="m2"><p>مرگ با زندگی تلخ تو شکرخوابی ست</p></div></div>