---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>در چمن ای گلرخان تا چند منزل خوش کنید</p></div>
<div class="m2"><p>یا به چشم من گذارید و مرا دل خوش کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلنشین باشد حدیث عشق در وارستگی</p></div>
<div class="m2"><p>موج دریا چند، ای یاران ز ساحل خوش کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر ادایی در حقیقت ره به جایی می برد</p></div>
<div class="m2"><p>هرچه می بینید از مجنون و عاقل خوش کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه ی خوبان، قیامت در جهان انگیخته ست</p></div>
<div class="m2"><p>کشتگان از خاک برخیزید و قاتل خوش کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتگویی کز غرض باشد، ندارد اعتبار</p></div>
<div class="m2"><p>عیش را دیوانه دارد، عاقلان دل خوش کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه ی صحرا و دامان بیابان هم خوش است</p></div>
<div class="m2"><p>چون سلیم ای همنشینان چند محفل خوش کنید؟</p></div></div>