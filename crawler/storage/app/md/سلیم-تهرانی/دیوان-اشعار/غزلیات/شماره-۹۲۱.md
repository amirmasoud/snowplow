---
title: >-
    شمارهٔ ۹۲۱
---
# شمارهٔ ۹۲۱

<div class="b" id="bn1"><div class="m1"><p>با کسی کی می‌کنم در عشقبازی همرهی</p></div>
<div class="m2"><p>من که دایم از دل خود می‌کنم پهلو تهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ضعیفی برنمی‌آید ز لب فریاد من</p></div>
<div class="m2"><p>ناله هم آخر به عشقت کرد با من کوتهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شغل عشقم کرد از سامان عالم بی‌نیاز</p></div>
<div class="m2"><p>بیستون فرهاد را بس مسند شاهنشهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پا به دامن کش چو کوه و رسم تمکین پیشه کن</p></div>
<div class="m2"><p>همچو دریا چند بتوان جوش زد از بی‌تَهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌گریزم، رهبری هرجا که می‌بینم سلیم</p></div>
<div class="m2"><p>خضر این وادی ز بس کرده‌ست با من بی‌رهی</p></div></div>