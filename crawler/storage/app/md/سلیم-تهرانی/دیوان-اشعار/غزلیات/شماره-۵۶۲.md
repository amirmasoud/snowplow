---
title: >-
    شمارهٔ ۵۶۲
---
# شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>نماند باده و آن تندخو نمی‌آید</p></div>
<div class="m2"><p>بهار آمد و گل رفت و او نمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمار همچو منی را شکستن آسان نیست</p></div>
<div class="m2"><p>کجاست خم که ز دست سبو نمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه سود جلوهٔ خوبان، که از حجاب مرا</p></div>
<div class="m2"><p>نظر بر آینه کردن ز رو نمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو فاخته نکنم یاد ناله‌ای هرگز</p></div>
<div class="m2"><p>که موج سرمه ز دل تا گلو نمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوخ‌چشمی گل‌های این چمن، بلبل</p></div>
<div class="m2"><p>ز بس که تر شده، پرواز ازو نمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم مشکل اگر افتدم گذر به وطن</p></div>
<div class="m2"><p>به سوی چشمه دگر آب جو نمی‌آید</p></div></div>