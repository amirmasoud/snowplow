---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>دل از هوای صحبت جانانه پر شده ست</p></div>
<div class="m2"><p>یک کس درون نیامده و خانه پر شده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس برای خود سر زلفی گرفته است</p></div>
<div class="m2"><p>زنجیر ازان کم است که دیوانه پر شده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست از کجا و مرگ، وگرنه همین مرا</p></div>
<div class="m2"><p>روزی هزار مرتبه پیمانه پر شده ست!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستی به کعبه ما و تو تنها نکرده ایم</p></div>
<div class="m2"><p>هنگامه ای ست این که درین خانه پر شده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خواب صبح، بالش نرم است شمع را</p></div>
<div class="m2"><p>فانوس بس که از پر پروانه پر شده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مجلسی که چهره برافروخت او سلیم</p></div>
<div class="m2"><p>صحبت میان بلبل و پروانه پر شده ست</p></div></div>