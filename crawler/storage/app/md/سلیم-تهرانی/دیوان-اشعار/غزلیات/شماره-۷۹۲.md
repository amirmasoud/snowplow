---
title: >-
    شمارهٔ ۷۹۲
---
# شمارهٔ ۷۹۲

<div class="b" id="bn1"><div class="m1"><p>در سخن سنجی هرکس بتر از غمازیم</p></div>
<div class="m2"><p>وای بر ما که درین بزم سخن پردازیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفر اول شوق است به کویت ما را</p></div>
<div class="m2"><p>صید ما زود توان کرد که نوپروازیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست کز مطرب این بزم در آتش ننشست</p></div>
<div class="m2"><p>ما همه سوخته ی شعله ی یک آوازیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیف باشد که ز بی مهری او شکوه کنیم</p></div>
<div class="m2"><p>ما که معشوق پران همچو کبوتر بازیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خونابه فشان بی خبری نیست سلیم</p></div>
<div class="m2"><p>راز پنهان مکن از ما که ز اهل رازیم</p></div></div>