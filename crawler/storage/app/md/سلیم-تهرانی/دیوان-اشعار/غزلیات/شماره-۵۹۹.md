---
title: >-
    شمارهٔ ۵۹۹
---
# شمارهٔ ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>بی تو معموره ی دل رو به خرابی دارد</p></div>
<div class="m2"><p>مو به مویم ز جنون سلسله تابی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برنیاید ز فلک در طلبت کام جهان</p></div>
<div class="m2"><p>همچو آن تشنه که پیراهن آبی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر کار دل از عشق تو پیداست که چیست</p></div>
<div class="m2"><p>که سر رشته ی این مرغ، کبابی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیخودم از لب مستی که چو آب زمزم</p></div>
<div class="m2"><p>غنچه ته جرعه ی او را به گلابی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رغبتم هیچ نمانده ست به شیرینی جان</p></div>
<div class="m2"><p>دل خونابه فشان، حال شرابی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماتم تشنه لبان گر نگرفته ست حباب</p></div>
<div class="m2"><p>از برای چه به تن جامه ی آبی دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غم است از فلک آن را که به غم خوی گرفت</p></div>
<div class="m2"><p>چون سمندر که فراغت ز کبابی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوه هرچند که دل را ز غم دوست، سلیم</p></div>
<div class="m2"><p>بی حساب است، ولی حرف حسابی دارد</p></div></div>