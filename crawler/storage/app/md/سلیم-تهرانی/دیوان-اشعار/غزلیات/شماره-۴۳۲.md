---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>در گدایی همت ما پادشاهی می کند</p></div>
<div class="m2"><p>هرچه می خواهد به توفیق الهی می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پابرهنه می دوم بر روی نیش دوستان</p></div>
<div class="m2"><p>آب کی اندیشه ای از خار ماهی می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان از بس به زیر منتم خواهد، به من</p></div>
<div class="m2"><p>می شمارد سرمه، گر چشمم سیاهی می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو من دیوانه ای در کوچه ی زنجیر نیست</p></div>
<div class="m2"><p>از محبت سیل با من خانه خواهی می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عافیت خواهی سلیم از فقر روگردان مباش</p></div>
<div class="m2"><p>مرد درویش از قناعت پادشاهی می کند</p></div></div>