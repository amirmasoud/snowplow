---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>در سر کوی تو شب خاک بود بستر ما</p></div>
<div class="m2"><p>چون شهیدان سر ما بالش زیر سر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تماشاگه دیدار تو ما سوخته ایم</p></div>
<div class="m2"><p>سرمه ی دیده کند آینه خاکستر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما ترقی بجز از راه تنزل نکنیم</p></div>
<div class="m2"><p>خاک چون دانه کند تربیت اختر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه گویند درین قصه، مرصع خوانی ست</p></div>
<div class="m2"><p>جام جمشید نبوده ست به از ساغر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هوای می گلرنگ ندارد آرام</p></div>
<div class="m2"><p>چون گدایان نفسی کشتی بی لنگر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو آیینه ی دیوار، درین دیر خراب</p></div>
<div class="m2"><p>ریشه ی سبزه ی زنگار بود جوهر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی آگاهی عیب و هنر خویش سلیم</p></div>
<div class="m2"><p>همچو طاووس شد آیینه ی ما هر پر ما</p></div></div>