---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>آبروی تیغ را خونگرمی بسمل برد</p></div>
<div class="m2"><p>کی تواند صرفه ای از قتل ما قاتل برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم همراهی مدار از کی که موج از جوش بحر</p></div>
<div class="m2"><p>کی تواند کشتی خود را سوی ساحل برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خضر را هم آگهی از کعبه ی توفیق نیست</p></div>
<div class="m2"><p>چون کسی از جستجو راهی به این منزل برد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژده بادا مرغ دل ها را که می بینم دگر</p></div>
<div class="m2"><p>بر سر آن دست شهبازی که زنگ از دل برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر دایم در سر کوی مغان چون روزگار</p></div>
<div class="m2"><p>جاهلان را آورد در مجلس و کامل برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شرافت هرکه با ما دم زند، تر می شود</p></div>
<div class="m2"><p>خاک هرکس در ره سیلاب آرد، گل برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لذت دشنام او دل می برد از کف سلیم</p></div>
<div class="m2"><p>همچو شیرینی ندیدم من که تلخی دل برد</p></div></div>