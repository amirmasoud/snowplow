---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>ای خوش آن آزاده ای کو در به روی کام بست</p></div>
<div class="m2"><p>دیده از نظارهٔ این باغ، چون بادام بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ضعیفی، قوت بی طاقتی با من نماند</p></div>
<div class="m2"><p>ناتوانی بر من آخر تهمت آرام بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجود آنکه لبریز شرابم، از خمار</p></div>
<div class="m2"><p>یک دم از خمیازه نتوانم دهن چون جام بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صید صیادی نگردیدیم، تا کی می توان</p></div>
<div class="m2"><p>خویش را همچون گره بر حلقه ی هر دام بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما کجا و خوشدلی، هرگز نیاساید سلیم</p></div>
<div class="m2"><p>آنکه بر ما تهمت این آرزوی خام بست</p></div></div>