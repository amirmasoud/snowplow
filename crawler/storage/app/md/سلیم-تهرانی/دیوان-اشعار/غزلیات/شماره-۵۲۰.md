---
title: >-
    شمارهٔ ۵۲۰
---
# شمارهٔ ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>دل به تدبیر رهایی چو به سویم بیند</p></div>
<div class="m2"><p>چون گره، بسته ی صد سلسله مویم بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاف سرچشمه ی حیوان، تهی از دردی نیست</p></div>
<div class="m2"><p>خضر کو تا می یکدست سبویم بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه منعم کند از باده ی گلگون دایم</p></div>
<div class="m2"><p>نتواند ز حسد رنگ به رویم بیند!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون روم از سر کوی تو، به من هرکه رسد</p></div>
<div class="m2"><p>گره گریه ی پنهان ز گلویم بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر آن گه شود از همتم آگه که سلیم</p></div>
<div class="m2"><p>مرده از تشنه لبی بر لب جویم بیند</p></div></div>