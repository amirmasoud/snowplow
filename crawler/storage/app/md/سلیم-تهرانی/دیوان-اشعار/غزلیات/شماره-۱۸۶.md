---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>با تو گل را سر و سامان خودآرایی نیست</p></div>
<div class="m2"><p>سرو را پیش تو سرمایه ی رعنایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرو ای شمع و مرا بر سر فریاد میار</p></div>
<div class="m2"><p>همچو اطفال، مرا طاقت تنهایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه زنجیر به پا از رگ خارا دارد</p></div>
<div class="m2"><p>نفس شیرین نتوان گفت که هر جایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما و این خرقه ی پشمینه که درویشان را</p></div>
<div class="m2"><p>چون سکندر هوس جامه ی دارایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز وحدت چو به کثرت رود از عرفان است</p></div>
<div class="m2"><p>کعبه شهری ست، سیه خانه ی صحرایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من گرفتم که شدی شبلی ایام سلیم</p></div>
<div class="m2"><p>حاصل این همه هیچ است چو دنیایی نیست</p></div></div>