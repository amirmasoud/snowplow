---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>بی سبب کی دامنم از گریه چون دریا پر است</p></div>
<div class="m2"><p>دل مرا چون کاسه ی دریوزه از صد جا پر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلم خوناب حسرت هست اگر در دیده نیست</p></div>
<div class="m2"><p>گر تهی گردیده از می ساغرم، مینا پر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون حریفان نیستم گلچین گلزار کسی</p></div>
<div class="m2"><p>از گل خود دامنم چون جامه ی دیبا پر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشک دارد بر بساط مستی ما آسمان</p></div>
<div class="m2"><p>شیشه های او سراسر خالی و از ما پر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخیه نتوان زد، گر از دشمن به من زخمی رسد</p></div>
<div class="m2"><p>جای سوزن نیست، از مهرم ز بس اعضا پر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ره افتادگی کی می روم از جا سلیم</p></div>
<div class="m2"><p>ورنه از بانگ درای کاروان صحرا پر است</p></div></div>