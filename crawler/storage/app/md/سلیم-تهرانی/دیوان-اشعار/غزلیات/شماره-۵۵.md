---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>فلک نبود به مستی حریف نالهٔ ما</p></div>
<div class="m2"><p>چو لاله ریخت از آن سرمه در پیالهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جز چراغ نداریم مجلس‌افروزی</p></div>
<div class="m2"><p>به غیر شیشه کسی نیست هم پیالهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بخت ما مددی غیر ازین نمی‌آید</p></div>
<div class="m2"><p>که از سیاهی او رم کند غزالهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌توان به دل گرم ما به سر بردن</p></div>
<div class="m2"><p>چو سایه داغ گریزان بود ز لالهٔ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود سلیم همه صرف شاهد و مطرب</p></div>
<div class="m2"><p>اگر جهان زر گل را کند حوالهٔ ما</p></div></div>