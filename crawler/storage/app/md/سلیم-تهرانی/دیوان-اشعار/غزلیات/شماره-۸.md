---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>تا چند دیر و کعبه، مخوان این فسانه را</p></div>
<div class="m2"><p>همچون کمان حلقه، یکی کن دو خانه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوق، پاسبانی ما عاشقان کند</p></div>
<div class="m2"><p>بلبل ز غنچه قفل زند آشیانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سینه هرچه بود، سپردم به دست عشق</p></div>
<div class="m2"><p>آری همین علاج بود دزد خانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرسبزی از جهان چه تمنا کنی که هست</p></div>
<div class="m2"><p>دندان مور ریشه درین خاک دانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست تهی دلالت دیوانگی کند</p></div>
<div class="m2"><p>بهتر ز ما ندیده کسی فال شانه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را چه جای شکوه، که شب پاسبان سلیم</p></div>
<div class="m2"><p>زنجیر می کند در زنجیرخانه را</p></div></div>