---
title: >-
    شمارهٔ ۶۱۲
---
# شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>نه همین تنها مرا چون شمع، آتش قوت کرد</p></div>
<div class="m2"><p>داد تا آبی جهان، خون در دل یاقوت کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر از بس تلخ شد بر اهل عالم، شوق مرگ</p></div>
<div class="m2"><p>خضر را چون اهل کشتی، زنده در تابوت کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیک و بد را فرق را فرق نتواند کند از یکدگر</p></div>
<div class="m2"><p>پیری از بس این جهان کهنه را فرتوت کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دل صد پاره ی مرغی به نوک خار دید</p></div>
<div class="m2"><p>باغبان این چمن آن را خیال توت کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هند شد همدوش عرش از کبریای من سلیم</p></div>
<div class="m2"><p>جلوه ی من عرصه ی لاهور را لاهوت کرد</p></div></div>