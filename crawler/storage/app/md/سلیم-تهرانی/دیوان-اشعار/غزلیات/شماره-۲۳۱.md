---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>گر به ظاهر کسی از قید جهان آزاد است</p></div>
<div class="m2"><p>نیست بی مصلحتی، این روش صیاد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه توان کرد، هنر قسمت ما شد ز جهان</p></div>
<div class="m2"><p>برطرف کی شود آن عیب که مادرزاد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نمیرم، همه شب شمع صفت می میرم</p></div>
<div class="m2"><p>مرگ چون خواب، من سوخته را معتاد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سست بنیاد بود ظلم، ز هم خواهد ریخت</p></div>
<div class="m2"><p>چون زره، خانه ی زنبور گر از فولاد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کسانی که به باغ آمد و رفتی دارند</p></div>
<div class="m2"><p>خانه خواهی که مرا هست، همین صیاد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک شغل هوس از عشق دلم کرد سلیم</p></div>
<div class="m2"><p>نکند بی ادبی طفل چو با استاد است</p></div></div>