---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>مهر و کین تو هر دو مطلوب است</p></div>
<div class="m2"><p>خوب هر کار می کند خوب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرف عشق است نقش جبهه ی ما</p></div>
<div class="m2"><p>این چه مضمون و این چه مکتوب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و بی طاقتی، که شیوه ی صبر</p></div>
<div class="m2"><p>کار ما نیست، کار ایوب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عافیت خواهی، از جنون مگذر</p></div>
<div class="m2"><p>گل این باغ بر سر چوب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنکبوت جهان اسبابیم</p></div>
<div class="m2"><p>خانه ی ما به کام جاروب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داغ سودا مرا بس است سلیم</p></div>
<div class="m2"><p>بر سرم گل مزن که سرکوب است</p></div></div>