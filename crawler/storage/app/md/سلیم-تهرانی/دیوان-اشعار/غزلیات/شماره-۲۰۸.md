---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>همچو بلبل دلم غمین گل است</p></div>
<div class="m2"><p>غنچه گردیده در کمین گل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلم درد، نایب عیش است</p></div>
<div class="m2"><p>بر سرم داغ، جانشین گل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نسیمی شکفته می گردد</p></div>
<div class="m2"><p>خنده گویا در آستین گل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادی دهر را شگونی نیست</p></div>
<div class="m2"><p>خندهٔ گل، دم پسین گل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد بستان ز جام باده سلیم</p></div>
<div class="m2"><p>که خزان سخت در کمین گل است</p></div></div>