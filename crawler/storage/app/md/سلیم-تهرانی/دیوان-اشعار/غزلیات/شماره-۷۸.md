---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>در دوزخ و بهشت نیاسوده ایم ما</p></div>
<div class="m2"><p>هر جا که بوده ایم چنین بوده ایم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را به مدعای غمت آفریده اند</p></div>
<div class="m2"><p>عشق ترا چو جامه ی فرموده ایم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خصم انتقام به نرمی توان گرفت</p></div>
<div class="m2"><p>بر داغ مدعی نمک سوده ایم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گفتگوی ناصح بیگانه سود نیست</p></div>
<div class="m2"><p>پند پدر به عشق چو نشنوده ایم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را همین ز قاتل ما بس بود سلیم</p></div>
<div class="m2"><p>کو اضطراب دارد و آسوده ایم ما</p></div></div>