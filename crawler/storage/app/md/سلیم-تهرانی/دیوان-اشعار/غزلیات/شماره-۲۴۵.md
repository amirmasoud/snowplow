---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>ز دوری تو مرا خوشدلی میسر نیست</p></div>
<div class="m2"><p>به غیر خون جگر بی توام به ساغر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم به سوی تو پرواز می کند از شوق</p></div>
<div class="m2"><p>که گفته است که مرغ کباب را پر نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان که از پی مکتوب خود به دام امید</p></div>
<div class="m2"><p>هزار مرغ گرفتم، یکی کبوتر نیست!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دید حال مرا، گفت پیر کنعانی</p></div>
<div class="m2"><p>که داغ دوری فرزند چون برادر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه می، که آب حرام است بی برادر خویش</p></div>
<div class="m2"><p>برین حدیث گواهی چو شیر مادر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین محیط، تفاوت به چشم معنی بین</p></div>
<div class="m2"><p>ز دست و پا زدن بسته و شناور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلند و پست جهان هرچه هست در کار است</p></div>
<div class="m2"><p>ز حکمت است که انگشت ها برابر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز کینه نیست به هنگام گریه آه مرا</p></div>
<div class="m2"><p>که گرد قافله است این، غبار لشکر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جور دوست شکایت سلیم نتوان کرد</p></div>
<div class="m2"><p>چه شد که خون مرا ریخت، آب کوثر نیست</p></div></div>