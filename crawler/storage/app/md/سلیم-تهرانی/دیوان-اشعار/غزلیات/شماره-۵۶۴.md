---
title: >-
    شمارهٔ ۵۶۴
---
# شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>گل ز رخسار تو رنگ و بو به دامن می کشد</p></div>
<div class="m2"><p>لاله از شوق تو همچون شمع گردن می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه با دل کرده بودم، یافتم از عشق تو</p></div>
<div class="m2"><p>انتقام سنگ را آتش ز آهن می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شکست خویش با این عاجزی دستم قوی ست</p></div>
<div class="m2"><p>شیشه ی من سنگ از مشت فلاخن می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خامه ی نقاش را ماند چراغ کلبه ام</p></div>
<div class="m2"><p>کز نشان دود بر دیوار، سوسن می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با حوادث بس که عادت کرده ام در خانه ام</p></div>
<div class="m2"><p>انتظار سیل دایم چشم روزن می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل حکمت، چاره ی فاسد به افسد می کنند</p></div>
<div class="m2"><p>از کف پا خار بیرون نوک سوزن می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتنم را غیر گر مانع شود از مهر نیست</p></div>
<div class="m2"><p>از محبت کی کسی را خار دامن می کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهدان را می دهد جامی که هوش از سر برد</p></div>
<div class="m2"><p>از کدوی خشک، پیر دیر روغن می کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پی آزار پاکان است از بس روزگار</p></div>
<div class="m2"><p>جوهری چون رشته گوهر را به سوزن می کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه جامی خورد، من دارم خمارش را سلیم</p></div>
<div class="m2"><p>انتقام دیگران را چرخ از من می کشد</p></div></div>