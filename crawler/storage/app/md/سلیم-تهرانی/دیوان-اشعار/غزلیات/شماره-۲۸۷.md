---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>مشرق خورشید را فیض گریبان تو نیست</p></div>
<div class="m2"><p>دامن گل پاک اگر باشد، چو دامان تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کسی را در طریق دلنشینی پایه ای ست</p></div>
<div class="m2"><p>غنچه ی گل دلکش است اما چو پیکان تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان دانست پیش خودپسندان چمن</p></div>
<div class="m2"><p>چهچه بلبل بجز وصف زنخدان تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتگان عشق را اعضا نمی ریزد ز هم</p></div>
<div class="m2"><p>لاله زین داغ است کز خیل شهیدان تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تماشای تو داغ حیرت آیینه ام</p></div>
<div class="m2"><p>خاک جای سرمه در چشمی که حیران تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو گل بر خود بناز ای دل، که از اهل جهان</p></div>
<div class="m2"><p>منت یک بخیه بر چاک گریبان تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این همه معنی رنگین نیست در جایی سلیم</p></div>
<div class="m2"><p>بلبلان را دفتر گل همچو دیوان تو نیست</p></div></div>