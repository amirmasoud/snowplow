---
title: >-
    شمارهٔ ۷۸۱
---
# شمارهٔ ۷۸۱

<div class="b" id="bn1"><div class="m1"><p>قطرهٔ خونابه‌ای تا سوی مژگان می‌کشم</p></div>
<div class="m2"><p>از دل مجروح، پنداری که پیکان می‌کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن صحرا ز موج گریه‌ام شد لاله‌زار</p></div>
<div class="m2"><p>رنگ می‌ریزم به هرسو، طرح طوفان می‌کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو مرغ بیضه شد بر من قفس پیراهنم</p></div>
<div class="m2"><p>از جنون خود را به روی تیغ عریان می‌کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌دهد از زلف خوبان یاد بر روی هوا</p></div>
<div class="m2"><p>همچو مجمر بس که آه از دل پریشان می‌کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهر بر دیوانه زندان است، همچون گردباد</p></div>
<div class="m2"><p>بر مراد دل نفس من در بیابان می‌کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌کنم چون غنچه هرگه یاد اوضاع چمن</p></div>
<div class="m2"><p>پای در دامان، سر خود در گریبان می‌کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که دارم ذوق جستن از فضای روزگار</p></div>
<div class="m2"><p>در میان خانه همچون تیر میدان می‌کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پا به مقدار گلیم خود کند هرکس دراز</p></div>
<div class="m2"><p>زان سبب من پا به قدر طرف دامان می‌کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیره شد چشم از غبار کشور هندم سلیم</p></div>
<div class="m2"><p>سرمه‌ای در چشم از خاک صفاهان می‌کشم</p></div></div>