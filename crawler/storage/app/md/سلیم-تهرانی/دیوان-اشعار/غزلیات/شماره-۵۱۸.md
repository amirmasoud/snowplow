---
title: >-
    شمارهٔ ۵۱۸
---
# شمارهٔ ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>ساقی دلگشای ما آمد</p></div>
<div class="m2"><p>رفت و بر مدعای ما آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه گر گشت ختر رز باز</p></div>
<div class="m2"><p>کهنه ی باصفای ما آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیشه ی باده دید ابر بهار</p></div>
<div class="m2"><p>تا چمن در قفای ما آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیخودی آورد نسیم چمن</p></div>
<div class="m2"><p>فصل گل هم برای ما آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیشه ای هرکجا شکست، به گوش</p></div>
<div class="m2"><p>می کشان را صدای ما آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نسیمی به مجلس مستان</p></div>
<div class="m2"><p>رفت، آواز پای ما آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ما در بغل شکست سلیم</p></div>
<div class="m2"><p>سنگ عشقی سزای ما آمد</p></div></div>