---
title: >-
    شمارهٔ ۷۴۰
---
# شمارهٔ ۷۴۰

<div class="b" id="bn1"><div class="m1"><p>کدام سر که نشد خاک آستانهٔ عشق؟</p></div>
<div class="m2"><p>علاج باد غرور است رازیانهٔ عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متاع صبر و خرد را به جای دیگر بر</p></div>
<div class="m2"><p>که نیست غیر زر قلب در خزانهٔ عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کاغذی که درآید ز مد مشق به موج</p></div>
<div class="m2"><p>تنم سیاه شد از نقش تازیانهٔ عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به موج فتنه چو سیلاب، خانهٔ ما را</p></div>
<div class="m2"><p>خراب کرد، که بادا خراب خانهٔ عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو فاسقی که بپوشد لباس اهل صلاح</p></div>
<div class="m2"><p>به روزگار تو دارد هوس نشانهٔ عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث درد دل ما به گوش کس مرساد</p></div>
<div class="m2"><p>که خواب می‌برد از دیده‌ها فسانهٔ عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس از وفات، دلم را سلیم آفت نیست</p></div>
<div class="m2"><p>به خاک، مور بود پاسبان دانهٔ عشق</p></div></div>