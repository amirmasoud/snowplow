---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>تا سحر امشب شراب ناب می‌باید گرفت</p></div>
<div class="m2"><p>خون‌بهای شمع از مهتاب می‌باید گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر صرف باده کردی، روی در میخانه کن</p></div>
<div class="m2"><p>هرچه آبش برده، در گرداب می‌باید گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دم کشتن مکن در عشق ترک اضطراب</p></div>
<div class="m2"><p>این روش را یاد از سیماب می‌باید گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوحه بر خود می‌کند همچون صنوبر نخل موم</p></div>
<div class="m2"><p>در گلستانی کز آتش آب می‌باید گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعی در تحصیل می‌کن، زان که از دوران سلیم</p></div>
<div class="m2"><p>نان گرفتن سهل باشد، آب می‌باید گرفت</p></div></div>