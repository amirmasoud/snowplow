---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>ز من شکایت آن جورپیشه برعکس است</p></div>
<div class="m2"><p>فغان سنگ ز بیداد شیشه برعکس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدای سنگ کند رخنه در دل فرهاد</p></div>
<div class="m2"><p>به بیستون وفا، کار تیشه برعکس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهال خشک وجود مرا ز موی سفید</p></div>
<div class="m2"><p>چو نخل شمع، درین باغ ریشه برعکس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دیدن رخ او آتشم فتد در دل</p></div>
<div class="m2"><p>چو آب و آینه، کارم همیشه برعکس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ره گریز به خاک است از فلک ما را</p></div>
<div class="m2"><p>جز این چه چاره پری را، که شیشه برعکس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم، ناله ز راحت کند دلم، آری</p></div>
<div class="m2"><p>به هند زلف بتان، کار و پیشه برعکس است</p></div></div>