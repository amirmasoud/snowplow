---
title: >-
    شمارهٔ ۹۲۳
---
# شمارهٔ ۹۲۳

<div class="b" id="bn1"><div class="m1"><p>به بزم می کشان لاابالی</p></div>
<div class="m2"><p>تهی شد شیشه، جای باده خالی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترم از ابرهای خشک ایران</p></div>
<div class="m2"><p>خوشا هند و هوای برشکالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل امید من تا کی به بزمش</p></div>
<div class="m2"><p>شود پامال چون گل های قالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا شب تا سحر دارد در آغوش</p></div>
<div class="m2"><p>خوشا احوال تصویر نهالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به اشکش تر کنم دل را بپیچم</p></div>
<div class="m2"><p>که مکتوبم نباشد خشک و خالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم از ماه من شرمنده گردد</p></div>
<div class="m2"><p>کند خورشید اگر صاحب کمالی</p></div></div>