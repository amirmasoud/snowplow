---
title: >-
    شمارهٔ ۷۲۱
---
# شمارهٔ ۷۲۱

<div class="b" id="bn1"><div class="m1"><p>منم که ساخته ام سور با عزا مربوط</p></div>
<div class="m2"><p>شده ست در کف من نیل با حنا مربوط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم ز فیض محبت هوس نمی داند</p></div>
<div class="m2"><p>که نیست غنچه ی این باغ با صبا مربوط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جدا کنند چو گل از تو پوست را به نفاق</p></div>
<div class="m2"><p>به هم شدند چو پیراهن و قبا مربوط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری به صحبت اهل زمانه نیست مرا</p></div>
<div class="m2"><p>بود چو دایره دایم سرم به پا مربوط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط می کند اظهار آشنایی من</p></div>
<div class="m2"><p>به حیرتم که به من بوده از کجا مربوط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اعتدال شود سعد و نحس را تأثیر</p></div>
<div class="m2"><p>در آن دیار که با جغد شد هما مربوط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دیگران هنر و عیب او چه می پرسی</p></div>
<div class="m2"><p>که هیچ کس به سخن نیست همچو ما مربوط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان کند ز عناصر همیشه فخر، سلیم</p></div>
<div class="m2"><p>به یک رباعی و آن نیز سست و نامربوط!</p></div></div>