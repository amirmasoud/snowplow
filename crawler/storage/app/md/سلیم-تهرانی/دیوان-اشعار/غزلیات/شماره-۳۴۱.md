---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>لطف این دلشکنان [در حق] ما معلوم است</p></div>
<div class="m2"><p>هر که دارد کرمی، آن به گدا معلوم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می برد حسرت کوی تو ز دنیا خورشید</p></div>
<div class="m2"><p>دارد از بهر همین رو به قفا، معلوم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد بی برگ و نوا را ز جهان رنگی نیست</p></div>
<div class="m2"><p>حال دست تهی از رنگ حنا معلوم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهد پاکی طینت، سخن ما کافی ست</p></div>
<div class="m2"><p>جوهر ذاتی چینی ز صدا معلوم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می دهد ظاهر هر کس خبر از باطن او</p></div>
<div class="m2"><p>رتبه ی پیرهن آری ز قبا معلوم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین گلستان به سرت هست تمنای گلی</p></div>
<div class="m2"><p>می توان یافت، ز خار کف پا معلوم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راستی شاهد ناسازی طالع باشد</p></div>
<div class="m2"><p>سرگرانی ضعیفان به عصا معلوم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله ی سوخته حال دل ما پرسی</p></div>
<div class="m2"><p>حال این قافله از بانگ درا معلوم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موج می، خط نجات است قدح نوشان را</p></div>
<div class="m2"><p>نیست زاهد به تو معلوم، به ما معلوم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رقم جبهه خط بندگی ماست سلیم</p></div>
<div class="m2"><p>گر ندانند بتان این، به خدا معلوم است</p></div></div>