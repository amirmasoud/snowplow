---
title: >-
    شمارهٔ ۷۷۵
---
# شمارهٔ ۷۷۵

<div class="b" id="bn1"><div class="m1"><p>بیا که چتر سعادت ز برگ تاک کنیم</p></div>
<div class="m2"><p>چو صبح، جیب فلک را ز غصه چاک کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیار باده که در دل اگر غباری هست</p></div>
<div class="m2"><p>چو آب و آینه با روزگار پاک کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر را ز لباس عزا برون آریم</p></div>
<div class="m2"><p>سر بریده ی خورشید را به خاک کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی خوریم غم روزگار تا می هست</p></div>
<div class="m2"><p>چه لازم است که خود را ز غم هلاک کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه می کند آن فتنه ای که می خواهد</p></div>
<div class="m2"><p>چه سود ازین که چو گل جیب خویش چاک کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم، فرصتی از روزگار تا داریم</p></div>
<div class="m2"><p>حساب خویش به ایام به که پاک کنیم</p></div></div>