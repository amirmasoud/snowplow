---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>ریزد ز بس غبار دل از هر فغان ما</p></div>
<div class="m2"><p>پر خاک شد چو حلقه ی دام آشیان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسان ز هجر یار ز بس جان سپرده ایم</p></div>
<div class="m2"><p>منقار زاغ زرد شد از استخوان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با قامت خمیده ره راست می رویم</p></div>
<div class="m2"><p>هرگز نجسته تیر خطا از کمان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ارباب هوش پی به معانی نمی برند</p></div>
<div class="m2"><p>دیوانه ذوق می کند از داستان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ننگ خضر بس که نهفتیم راز خویش</p></div>
<div class="m2"><p>شد خاک، حرف تشنه لبی در دهان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز کسی سلیم ندیده ست آفتی</p></div>
<div class="m2"><p>چون تیغ آفتاب ز تیغ زبان ما</p></div></div>