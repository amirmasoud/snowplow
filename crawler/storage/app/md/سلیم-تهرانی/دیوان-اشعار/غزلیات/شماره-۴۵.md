---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>عشق دیگر در فغان آورده ناقوس مرا</p></div>
<div class="m2"><p>غنچهٔ گلبرگ آتش کرده فانوس مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاشکی اندیشه ای از باطن عصمت کند</p></div>
<div class="m2"><p>عشق بر گردن نگیرد خون ناموس مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حریم آستانش، چند منع پاسبان</p></div>
<div class="m2"><p>همچو تبخاله گره سازد به لب بوس مرا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خامه ام را کار با معنی خود باشد، که هست</p></div>
<div class="m2"><p>در گلستان پر خود جلوه طاووس مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد نظاره روشناس گلرخان، ترسم سلیم</p></div>
<div class="m2"><p>در دیار حسن بشناسند جاسوس مرا</p></div></div>