---
title: >-
    شمارهٔ ۵۷۳
---
# شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>خطش به تازه باعث ناز و نیاز شد</p></div>
<div class="m2"><p>کوتاه کرد زلف و حکایت دراز شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محمود از کجا، سفر هند از کجا</p></div>
<div class="m2"><p>این شور و فتنه بر سر حسن ایاز شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیر مردم و زشرف مشت خاک من</p></div>
<div class="m2"><p>در سجده گاه صومعه مهر نماز شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سامان روزگار، پریشانی آورد</p></div>
<div class="m2"><p>افتد گره به کار چو ناخن دراز شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افتد ز بس که طشت کسی هر نفس ز بام</p></div>
<div class="m2"><p>روی زمین چو معرکه ی طاس باز شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تاب عشق تا سر من گرم شد سلیم</p></div>
<div class="m2"><p>چون شمع، کار من همه سوز و گداز شد</p></div></div>