---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>چو ماه شعشعه ی ماست برق خرمن ما</p></div>
<div class="m2"><p>چو خوشه هیکل عمر است داس گردن ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دست و پنجه ی خورشید بر نمی آید</p></div>
<div class="m2"><p>که همچو داغ، سیاهی برد ز روزن ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای تاج که دارد به سر، که همچون باز</p></div>
<div class="m2"><p>به زور بسته کلاهی جهان به گردن ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست شوق ز بس چاک گشته، پنداری</p></div>
<div class="m2"><p>که همچو غنچه گریبان ماست دامن ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو موج آب که دارد حباب در آغوش</p></div>
<div class="m2"><p>به جای سنگ بود شیشه در فلاخن ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه مشکل است که هر گام از سیاهی بخت</p></div>
<div class="m2"><p>چراغپا نشود همچو برق، توسن ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلیم، کینه ی دشمن ز ما محبت شد</p></div>
<div class="m2"><p>خزان بهار شود چون رسد به گلشن ما</p></div></div>