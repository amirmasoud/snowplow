---
title: >-
    شمارهٔ ۸۲۶
---
# شمارهٔ ۸۲۶

<div class="b" id="bn1"><div class="m1"><p>چون گره جا در خم آن زلف دلکش کرده‌ایم</p></div>
<div class="m2"><p>پای خود پیچیده در دامن، فروکش کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کند از دلگشایی گریهٔ ما کار می</p></div>
<div class="m2"><p>ما به آتش آب را چون شیشه روکش کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار ارباب صفا برعکس چون آیینه است</p></div>
<div class="m2"><p>خانه را از ساده‌کاری ما منقش کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نان یاران را که بوی قرص افعی می‌دهد</p></div>
<div class="m2"><p>زهر قاتل باد اگر هرگز نمک چش کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طریق عشق، دل را پختگی حاصل نشد</p></div>
<div class="m2"><p>بیضهٔ فولاد پنداری در آتش کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر شانه کس ندارد دست بر وصلش سلیم</p></div>
<div class="m2"><p>خاطر خود جمع ازان زلف مشوش کرده‌ایم</p></div></div>