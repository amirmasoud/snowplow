---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>ایام بهار است و گلی در چمنم نیست</p></div>
<div class="m2"><p>عشرت همه جا هست، در آنجا که منم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش به بساط افکندم گرمی آهی</p></div>
<div class="m2"><p>غیر از پر پروانه گل انجمنم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کشور ما حادثه را دست دراز است</p></div>
<div class="m2"><p>شادم که به غربت خبری از وطنم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر روی کسی در مگشا خانه ی خود را</p></div>
<div class="m2"><p>صدبار اگر دوست بگوید که منم، نیست!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اعضای من از داغ تو با مهر و نشان است</p></div>
<div class="m2"><p>هر عضو که بی داغ تو باشد ز تنم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشفته بیان همچو سلیمم، اگر احباب</p></div>
<div class="m2"><p>دارند سخن بر سخن من، سخنم نیست</p></div></div>