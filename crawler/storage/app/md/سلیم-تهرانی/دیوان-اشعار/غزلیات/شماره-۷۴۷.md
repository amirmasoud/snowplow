---
title: >-
    شمارهٔ ۷۴۷
---
# شمارهٔ ۷۴۷

<div class="b" id="bn1"><div class="m1"><p>همچو آتش داردم ایام در زندان سنگ</p></div>
<div class="m2"><p>می توان گفتن ز جان سختی تنم را کان سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون لب جو سخت گیرد کار هرکس را جهان</p></div>
<div class="m2"><p>از برای آب خوردن بایدش دندان سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نصیحت چند اصلاح دل سختش کنم</p></div>
<div class="m2"><p>همچو موج آب باشم تا به کی سوهان سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد قزوین در دلم بگذشت و از هندوستان</p></div>
<div class="m2"><p>شیشه ام میدان کشید و جست تا میدان سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افکند تا بر بساط شیشهٔ دل‌ها سلیم</p></div>
<div class="m2"><p>آسمان همچون فلاخن می شود قربان سنگ</p></div></div>