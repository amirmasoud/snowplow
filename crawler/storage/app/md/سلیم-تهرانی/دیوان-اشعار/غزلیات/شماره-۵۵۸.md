---
title: >-
    شمارهٔ ۵۵۸
---
# شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>لبت چون غنچه دلتنگی ندارد</p></div>
<div class="m2"><p>چو گل روی تو یکرنگی ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اهل کفر و ایمان سینه صافم</p></div>
<div class="m2"><p>چو آب، آیینه ام زنگی ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای شیشه ی من هیچ کس نیست</p></div>
<div class="m2"><p>که از دل، در بغل سنگی ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرود نوحه گر را جای خالی ست</p></div>
<div class="m2"><p>نوای مطرب آهنگی ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خون ما نگردد تیغ رنگین</p></div>
<div class="m2"><p>سلیم از ما کسی رنگی ندارد</p></div></div>