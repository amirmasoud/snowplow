---
title: >-
    شمارهٔ ۶۴۸
---
# شمارهٔ ۶۴۸

<div class="b" id="bn1"><div class="m1"><p>در قید محبت دل ناشاد نباشد</p></div>
<div class="m2"><p>یک صید ندیدیم که آزاد نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل محکم اگر نیست، چه از دست گشاید</p></div>
<div class="m2"><p>تیغ از چه توان ساخت چو فولاد نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آه اسیران بود این گردش افلاک</p></div>
<div class="m2"><p>کشتی نرود راه، اگر باد نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازم به دبستان تو ای عشق که خود را</p></div>
<div class="m2"><p>تعلیم دهد طفل، گر استاد نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر گفته ی احباب بسی گوش نهادیم</p></div>
<div class="m2"><p>حرفی نشنیدیم که در یاد نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انکار بت و عشق برهمن نتوان کرد</p></div>
<div class="m2"><p>حسنی نبرد دل که خداداد نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی عشق چه از ناخن تدبیر گشاید</p></div>
<div class="m2"><p>کاری نکند تیشه چو فرهاد نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل را که سلیم از غم عشق است خرابی</p></div>
<div class="m2"><p>بهتر همه آن است که آباد نباشد</p></div></div>