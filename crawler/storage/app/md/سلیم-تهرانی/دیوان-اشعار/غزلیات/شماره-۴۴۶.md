---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>مژگان من وظیفهٔ خوناب می‌خورد</p></div>
<div class="m2"><p>غواض نان ز سفرهٔ گرداب می‌خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغم ز دست لاله که در موسم بهار</p></div>
<div class="m2"><p>دارد شراب در قدح و آب می‌خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌نغمه‌ای شکفته نگردد دل از شراب</p></div>
<div class="m2"><p>خرم دلی که آب ز دولاب می‌خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد به خانه جام بلورم به طاق دید</p></div>
<div class="m2"><p>گفت این گل کدو ز کجا آب می‌خورد؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منعش مکن که در نظرش طاق ابرویی‌ست</p></div>
<div class="m2"><p>آن کس که می به گوشهٔ محراب می‌خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیران اضطراب گل و لاله‌ام، مگر</p></div>
<div class="m2"><p>آب این چمن ز شبنم سیماب می‌خورد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسایشی نمانده ز عقل و جنون مرا</p></div>
<div class="m2"><p>در عشق رشته‌ام ز دو سر تاب می‌خورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغی نیم که دانه‌خور بوستان شوم</p></div>
<div class="m2"><p>از چشمه‌سار دام، دلم آب می‌خورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنجیر را کشاکش دیوانه بگسلد</p></div>
<div class="m2"><p>زلف تو تاب از دل بی‌تاب می‌خورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مستی سلیم باد حلال کسی که می</p></div>
<div class="m2"><p>در روز ابر و در شب مهتاب می‌خورد</p></div></div>