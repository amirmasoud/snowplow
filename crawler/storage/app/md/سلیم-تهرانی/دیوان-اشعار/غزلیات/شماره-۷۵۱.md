---
title: >-
    شمارهٔ ۷۵۱
---
# شمارهٔ ۷۵۱

<div class="b" id="bn1"><div class="m1"><p>هر قطره ی شبنم رخ گل</p></div>
<div class="m2"><p>سیلی ست به خانمان بلبل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرم همه در خیال او رفت</p></div>
<div class="m2"><p>چون آب روان به سایه ی گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مغرور به حسن خویشتن بود</p></div>
<div class="m2"><p>زلف تو شکست شاخ سنبل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شرم رخ تو گل گریزد</p></div>
<div class="m2"><p>هر لحظه به زیر بال بلبل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بهر دلم سلیم، دایم</p></div>
<div class="m2"><p>خم در خم زلف کرده کاکل</p></div></div>