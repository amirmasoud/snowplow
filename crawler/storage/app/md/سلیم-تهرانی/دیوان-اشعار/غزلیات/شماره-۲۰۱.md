---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>دلا ز دام صفیری به گلستان بفرست</p></div>
<div class="m2"><p>به دست ناله دعایی به بلبلان بفرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی قبول ندارد که در قفس هستی</p></div>
<div class="m2"><p>پری برای نشانی به آشیان بفرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تهی مدار چمن را ز گلشن آرایی</p></div>
<div class="m2"><p>اگر بهار نیاید، پی خزان بفرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی به خلوت خود آفتاب را بطلب</p></div>
<div class="m2"><p>پی سفارش من پیش آسمان بفرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر کجا که بود دلخوشی، بهشت آنجاست</p></div>
<div class="m2"><p>گلی سلیم ز گلخن به باغبان بفرست</p></div></div>