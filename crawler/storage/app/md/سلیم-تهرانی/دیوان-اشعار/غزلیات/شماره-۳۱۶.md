---
title: >-
    شمارهٔ ۳۱۶
---
# شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>آفت باد خزان را می توان معذور داشت</p></div>
<div class="m2"><p>در گلستانی که هر بادام، چشم شور داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفحه ی رنگین خوان خود سلیمان جلوه داد</p></div>
<div class="m2"><p>از سرشک عاجزان، افشان چشم مور داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش مستی پرده از راز نهان افکنده بود</p></div>
<div class="m2"><p>هر سر مویم به کف پیمانه ی منصور داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از توانایی می رطل گران را در شباب</p></div>
<div class="m2"><p>می کشیدم، گر کمان موج صد من زور داشت(؟)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون حباب اکنون به پیش قطره اندازم سپر</p></div>
<div class="m2"><p>موسم پیری ست ساقی، می توان معذور داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پر پروانه زن دامان همت بر میان</p></div>
<div class="m2"><p>دست بر آتش توان تا کی سلیم از دور داشت</p></div></div>