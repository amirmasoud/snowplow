---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>دلم چو شمع همه عمر میهمان خود است</p></div>
<div class="m2"><p>چو قرعه چشم همایم بر استخوان خود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نسبت دگری نیست سربلندی ما</p></div>
<div class="m2"><p>سر شهید تو چون لاله بر سنان خود است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرینه نیست در آوارگی مرا که مدام</p></div>
<div class="m2"><p>مسافرم من و عنقا در آشیان خود است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قبول نیست فلک، برگرفته ی او را</p></div>
<div class="m2"><p>غبار چون ز زمین خاست، آسمان خود است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دیگری چه کنی شکوه بی سبب منصور!</p></div>
<div class="m2"><p>طناب دار تو از پنبه ی دکان خود است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عشق دم ز علایق مزن، چه نادانی</p></div>
<div class="m2"><p>که با اجل همه سوگند او به جان خود است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غم ز فتنه ی محشر شهید عشق ترا</p></div>
<div class="m2"><p>چو شیر مست که در خواب، پاسبان خود است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلیم را که فلک بود در عنان، اکنون</p></div>
<div class="m2"><p>دوان به راه تو چون برق در عنان خود است</p></div></div>