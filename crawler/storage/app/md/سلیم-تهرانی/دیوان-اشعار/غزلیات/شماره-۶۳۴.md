---
title: >-
    شمارهٔ ۶۳۴
---
# شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>لطف ساقی، خار از پهلوی آتش می کشد</p></div>
<div class="m2"><p>شیشه ی می آب را بر روی آتش می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل ما آشیان در گلشنی دارد که خار</p></div>
<div class="m2"><p>خویش را پهلوی گل بر بوی آتش می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست از پیغام وصلم حاصلی جز سوختن</p></div>
<div class="m2"><p>خار را یاد گلستان سوی آتش می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه از پهلونشینی های او من می کشم</p></div>
<div class="m2"><p>کافرم گر خار از پهلوی آتش می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو دوری چاره ی ما تیره بختان بود و بس</p></div>
<div class="m2"><p>دود این سرگشتگی از خوی آتش می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز قسمت سر توان پیچید، کز دریا نصیب</p></div>
<div class="m2"><p>گوش ماهی را گرفته سوی آتش می کشد!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می برد نام تو هم هرکس که یاد ما کند</p></div>
<div class="m2"><p>حرف پروانه به گفت و گوی آتش می کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق پرورده ست از روز ازل ما را سلیم</p></div>
<div class="m2"><p>ریشه ی خاشاک، نم از جوی آتش می کشد</p></div></div>