---
title: >-
    شمارهٔ ۸۷۵
---
# شمارهٔ ۸۷۵

<div class="b" id="bn1"><div class="m1"><p>ای جرس، سوی سفر هر لحظه آوازم مکن</p></div>
<div class="m2"><p>بی پر و بالم، عبث تکلیف پروازم مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گره، سررشته ی عمرم به دست بستگی ست</p></div>
<div class="m2"><p>دشمن من گر نه ای، از دوستی بازم مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد رسوا این می مردآزما منصور را</p></div>
<div class="m2"><p>راز خود با من مگو ای عشق، غمازم مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قفای رهروان فریاد کردن شرط نیست</p></div>
<div class="m2"><p>د بیابان جنون ای خضر آوازم مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صیقل دل، صحبت آزادگان باشد سلیم</p></div>
<div class="m2"><p>جز به چوب بید چون آیینه پردازم مکن</p></div></div>