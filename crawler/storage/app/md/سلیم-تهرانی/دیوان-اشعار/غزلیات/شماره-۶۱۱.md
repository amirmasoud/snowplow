---
title: >-
    شمارهٔ ۶۱۱
---
# شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>تیغ بردار که چون حوصله بی تاب شود</p></div>
<div class="m2"><p>کشتن ما به تو دشوار چو سیماب شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می پرستان همه از آتش ما می سوزند</p></div>
<div class="m2"><p>روزن خانه ببندیم که مهتاب شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می به یادم مکن ای دوست به ساغر که مباد</p></div>
<div class="m2"><p>باده در جام تو چون آبله خوناب شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بی مصلحتی ناله ی ما، می خواهیم</p></div>
<div class="m2"><p>بخت خود را نگذاریم که در خواب شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز می گرم چو شد، جلوه ی معشوق کند</p></div>
<div class="m2"><p>ماهی موم به آتش چو رسد آب شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم از پرتو دیدار به جوش است سلیم</p></div>
<div class="m2"><p>شور دیوانه شود بیش چو مهتاب شود</p></div></div>