---
title: >-
    شمارهٔ ۹۰۷
---
# شمارهٔ ۹۰۷

<div class="b" id="bn1"><div class="m1"><p>به چهره خنده به گل‌های باصفا زده‌ای</p></div>
<div class="m2"><p>به نغمه طعنه به مرغانِ خوش‌نوا زده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو لاله چشم سیاه از خمار داری سرخ</p></div>
<div class="m2"><p>پیاله تا به سحر، دوش در کجا زده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی ندیده به اقبال، چون تو صیادی</p></div>
<div class="m2"><p>به هر خدنگ که افکنده‌ای، هما زده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کوی عشق تو از خون گرفتگان دیگر</p></div>
<div class="m2"><p>کسی نمانده، کنون دست بر حنا زده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روی آینه آن پشت پاست روشن‌تر</p></div>
<div class="m2"><p>گمان بری که به خورشید پشت پا زده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریف دشمن و اقبال او سلیم نه‌ای</p></div>
<div class="m2"><p>ازین چه سود که بر سر پر هما زده‌ای</p></div></div>