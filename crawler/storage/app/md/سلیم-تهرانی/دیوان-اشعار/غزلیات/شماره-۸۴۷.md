---
title: >-
    شمارهٔ ۸۴۷
---
# شمارهٔ ۸۴۷

<div class="b" id="bn1"><div class="m1"><p>گه مستم و گاه در خمارم</p></div>
<div class="m2"><p>این است تمام عمر کارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پاره ی دل، سرشک چون گل</p></div>
<div class="m2"><p>آیینه شکسته در کنارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندم ببرد به سیر گلشن؟</p></div>
<div class="m2"><p>از روی نسیم، شرمسارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عالم خاک، پای عنقا</p></div>
<div class="m2"><p>ببریده ز تیغ کوهسارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیچیده ام آنچنان که افتند</p></div>
<div class="m2"><p>مرغان به قفس ز شاخسارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی آن گل رو، سلیم بگذشت</p></div>
<div class="m2"><p>افسرده تر از خزان، بهارم</p></div></div>