---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>دلم بی طره ای آشفته حال است</p></div>
<div class="m2"><p>جدا از ماه خویشم، چند سال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خامه راز ما نتوان شنیدن</p></div>
<div class="m2"><p>زبان ترجمان شوق، لال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند چون دختر رز جلوه، زاهد</p></div>
<div class="m2"><p>تماشا کن که یک دیدن حلال است!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببین آن چشم و ابروی گرهگیر</p></div>
<div class="m2"><p>که پنداری مگر شاخ غزال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر انگشتم برای دلخراشی</p></div>
<div class="m2"><p>همه ناخن چو انگشت هلال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیری عشق کیفیت پذیرد</p></div>
<div class="m2"><p>که صافی باده را در درد سال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی دانم فلک را مدعا چیست</p></div>
<div class="m2"><p>که سرگردان چو فانوس خیال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهرافشانی لعل تو تا دید</p></div>
<div class="m2"><p>صدف غرق عرق از انفعال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گزیری پادشاهان را ازو نیست</p></div>
<div class="m2"><p>سخن درویش را آب سفال است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه با گل سازگار و نی به خاشاک</p></div>
<div class="m2"><p>هوای این چمن بی اعتدال است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکفته رویی ظاهر چه بینی؟</p></div>
<div class="m2"><p>که گل را گوش سرخ از گوشمال است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلیم اشکم نوید وصل او داد</p></div>
<div class="m2"><p>که طفلان هرچه می گویند فال است</p></div></div>