---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>چون گل ز پاره ی دلم اسباب داده اند</p></div>
<div class="m2"><p>چون لاله ز آتش جگرم آب داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهد بهانه از پی خون ریختن، مگر</p></div>
<div class="m2"><p>تیغ ترا ز دیده ی من آب داده اند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زحمت مکش که کس نتواند به جور کشت</p></div>
<div class="m2"><p>ما را که سخت جانی سیماب داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطع نظر ز طاعت حق، سجده کردنی ست</p></div>
<div class="m2"><p>این طاق ابرویی که به محراب داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خال تو همچو حلقه ی زلف تو دلرباست</p></div>
<div class="m2"><p>این دانه را ز چشمه ی دام آب داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیهوده نیست روی به صحرا اگر نهند</p></div>
<div class="m2"><p>دیوانگان که خانه به سیلاب داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساحل غبار بود ز خاطر سلیم رفت</p></div>
<div class="m2"><p>تا راه ما به حلقه ی گرداب داده اند</p></div></div>