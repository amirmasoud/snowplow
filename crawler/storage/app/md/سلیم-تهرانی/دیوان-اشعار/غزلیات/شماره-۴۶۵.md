---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>دلم آسوده شد تا در خم زلفش مکان دارد</p></div>
<div class="m2"><p>چو آن مرغی که بر شاخ بلندی آشیان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هجر و وصل می سوزد دلم یارب چه بخت است این</p></div>
<div class="m2"><p>که یاقوت مرا، هم آب و هم آتش زیان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب وصلم ز رشک غیر همچون روز هجران است</p></div>
<div class="m2"><p>بهار گلشن ما چون حنا رنگ خزان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کوی عشق او چون می توانم گم کنم خود را؟</p></div>
<div class="m2"><p>که همچون لاله هر عضو من از داغی نشان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملاحت هرکه می خواهد، به هندش رهنمایی کن</p></div>
<div class="m2"><p>که حسن شورش انگیزش نمک در سرمه دان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین گلشن مرا رحمی به حال لاله می آید</p></div>
<div class="m2"><p>که داغ بی بقایی چون زر هندوستان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلبکار دیانت چون کلیدیم اندرین بازار</p></div>
<div class="m2"><p>متاعی را که می جوییم ما، قفل دکان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکست پیکرم از اشک خونین می شود ظاهر</p></div>
<div class="m2"><p>کزو هر قطره‌ای چون دانهٔ نار استخوان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گفتارم سلیم آزرده‌ای جز خود نمی‌بینم</p></div>
<div class="m2"><p>اگر خاری درین باغ است، دست باغبان دارد</p></div></div>