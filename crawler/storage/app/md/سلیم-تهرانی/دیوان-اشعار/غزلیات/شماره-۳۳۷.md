---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>چون شحنه مرا پنجه ی تقدیر گرفته ست</p></div>
<div class="m2"><p>کو آنکه بپرسد به چه تقصیر گرفته ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا که به جوش آمده دیوانه ای، از رشک</p></div>
<div class="m2"><p>اعضای مرا رعشه چو زنجیر گرفته ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای مرغ چمن، از اثر باد خزان است</p></div>
<div class="m2"><p>کآواز تو چون بلبل تصویر گرفته ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خضر تواند کند اصلاح دل ما؟</p></div>
<div class="m2"><p>ویرانه ی ما را ره تعمیر گرفته ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشاطه پی منع سلیم از دل روشن</p></div>
<div class="m2"><p>آیینه ی خود داده و شمشیر گرفته ست</p></div></div>