---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>میخانه ی ما چون طرب آباد بهار است</p></div>
<div class="m2"><p>از برگ گلش خشت چو بنیاد بهار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل کرد جنونم ز رگ و ریشه به صد رنگ</p></div>
<div class="m2"><p>اینها همه از لطف تو ای باد بهار است!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن مرغ که از صحبت گل خوی گرفته ست</p></div>
<div class="m2"><p>کارش همه در فصل خزان، یاد بهار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که درین باغ کنی کسب شکفتن</p></div>
<div class="m2"><p>شاگرد خزان باش که استاد بهار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیلی چمن: بید، که مجنون لقب اوست</p></div>
<div class="m2"><p>گویی که مگر بال پریزاد بهار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارند خروشی به چمن سرو و صنوبر</p></div>
<div class="m2"><p>از شوق تو اینها همه فریاد بهار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس باخبر از کار خود و سستی آن نیست</p></div>
<div class="m2"><p>هر برگ گل آیینه ی فولاد بهار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیروزه ند اطفال گل و لاله ی نوخیز</p></div>
<div class="m2"><p>در باغ، خزان است که همزاد بهار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فریاد سلیم از جگرم دود برآورد</p></div>
<div class="m2"><p>چون مرغ گرفتار که در یاد بهار است</p></div></div>