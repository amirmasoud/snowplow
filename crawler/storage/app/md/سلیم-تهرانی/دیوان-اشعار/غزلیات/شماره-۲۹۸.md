---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>راحتی ما را اگر می باشد از آزار ماست</p></div>
<div class="m2"><p>بوی گل در خانه از خار سر دیوار ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال مرغان قفس را پیش گل خواهیم گفت</p></div>
<div class="m2"><p>رشته ی انگشت ما انگشتر زنهار ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر مژگانم سحاب دجله افشان کس ندید</p></div>
<div class="m2"><p>این روش در دودمان ابر دریابار ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ کس حال سر ما را نمی داند که چیست</p></div>
<div class="m2"><p>عالمی را چشم همچون صبح بر دستار ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیب دیگر گر گمان داری سلیم آن را بگوی</p></div>
<div class="m2"><p>طعنهٔ مستی مزن بر ما که آن خود کار ماست</p></div></div>