---
title: >-
    شمارهٔ ۹۳۱
---
# شمارهٔ ۹۳۱

<div class="b" id="bn1"><div class="m1"><p>در ره آوارگی بختم فکند از دشمنی</p></div>
<div class="m2"><p>در بیابانی که کار خضر باشد رهزنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لباس مطربی کز بزم ما بیرون رود</p></div>
<div class="m2"><p>سرمه می ریزد چو خاکستر ز رخت گلخنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عافیت خواهی، مشو آلوده ی مال جهان</p></div>
<div class="m2"><p>شمع را باشد بلای جان، لباس روغنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلوت حمام را ماند حریم روزگار</p></div>
<div class="m2"><p>در میان خلق از بس عام شد تردامنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کنایت نکته ای کافی ست از روشندلان</p></div>
<div class="m2"><p>مسند عیسی کند خورشید تابان سوزنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم تا پوشیدم از دنیا، صفای دل فزود</p></div>
<div class="m2"><p>خانهٔ آیینه از روزن ندارد روشنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که هرسو بلبلان از آتش گل سوختند</p></div>
<div class="m2"><p>می کند قمری درین گلشن تخلص گلخنی!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر سبوی می ز خاک رستم یک دست نیست</p></div>
<div class="m2"><p>از کجا آورده است این زور و این مردافکنی؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش مرغان گر به آن قد سرو را نسبت کند</p></div>
<div class="m2"><p>طوق قمری بشکند، از بس زنندش گردنی!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتگوی عشق او دارند با هم در میان</p></div>
<div class="m2"><p>گلخنی با گلخنی و گلشنی با گلشنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بقای این چمن گر باخبر بودی، سلیم</p></div>
<div class="m2"><p>گل به مرگ خویش پوشیدی قبای سوسنی</p></div></div>