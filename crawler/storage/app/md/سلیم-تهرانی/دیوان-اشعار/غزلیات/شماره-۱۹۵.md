---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>گل نشاط به بزم شراب پامال است</p></div>
<div class="m2"><p>پیاله در کف مستان چراغ اقبال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اشک چشم اسیران کجا نگاه کند</p></div>
<div class="m2"><p>چو موج در ره او آب خضر پامال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیان بود به تو پروانه آخر کارت</p></div>
<div class="m2"><p>که سرنوشت تو بر صفحه ی پر و بال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به چشم حقیقت نظر کنی، دانی</p></div>
<div class="m2"><p>که طوق فاخته بر پای سرو خلخال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهوش باش فریب سخن دلت نبرد</p></div>
<div class="m2"><p>سواد نامه ی ما از سیاهی خال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکرد فتح مرادی سلیم در همه عمر</p></div>
<div class="m2"><p>که گفته است که همت بلنداقبال است؟</p></div></div>