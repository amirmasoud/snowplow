---
title: >-
    شمارهٔ ۹۳۵
---
# شمارهٔ ۹۳۵

<div class="b" id="bn1"><div class="m1"><p>خوش بود گر ز سر هر هوسی برخیزی</p></div>
<div class="m2"><p>زین گلستان به سراغ قفسی برخیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کعبه در بادیه هرگاه به خوابت آید</p></div>
<div class="m2"><p>حیف باشد که به بانگ جرسی برخیزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خدا باک نداری، ولی از مجلس می</p></div>
<div class="m2"><p>نتوانی که ز بیم عسسی برخیزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باخبر باش که خواب عدم آن خوابی نیست</p></div>
<div class="m2"><p>که ز جنبیدن بال مگسی برخیزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیز ای طفل و ز افتادن خود گریه مکن</p></div>
<div class="m2"><p>که بسی افتی ازین سان و بسی برخیزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند ای شاخ گل از باده پرستی هر صبح</p></div>
<div class="m2"><p>همچو خمیازه ز آغوش کسی برخیزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیر از زندگی خود شده ام همچو سلیم</p></div>
<div class="m2"><p>کاش ای دوست ز پیشم نفسی برخیزی</p></div></div>