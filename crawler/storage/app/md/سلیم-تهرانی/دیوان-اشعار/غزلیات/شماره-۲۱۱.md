---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>بر سر عشق تو هرکس هست با من دشمن است</p></div>
<div class="m2"><p>آنکه اشکی پاک می‌سازد ز چشمم دامن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کف او می به رنگ شبنم روی گل است</p></div>
<div class="m2"><p>بر میانش تیغ چون آب میان گلشن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنمی‌خیزیم از جای خود و آواره‌ایم</p></div>
<div class="m2"><p>دامن صحرای مجنون تو طرف دامن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو من منصور را سامان رسوایی کجاست</p></div>
<div class="m2"><p>مایهٔ حلاجی او پنبهٔ داغ من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوچهٔ زنجیر را ماند به عهدش روزگار</p></div>
<div class="m2"><p>بس که از بیداد او هر خانه‌ای پر شیون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمن جان است دل اهل محبت را سلیم</p></div>
<div class="m2"><p>یوسف ما را همیشه گرگ در پیراهن است</p></div></div>