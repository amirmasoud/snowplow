---
title: >-
    شمارهٔ ۷۵۴
---
# شمارهٔ ۷۵۴

<div class="b" id="bn1"><div class="m1"><p>مگر افتد گذارش سوی آن گل</p></div>
<div class="m2"><p>ببندم نامه ای بر بال بلبل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا هرگاه بیند در گلستان</p></div>
<div class="m2"><p>پریشانی رود از یاد سنبل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه: پیکان تیر کینه جویی</p></div>
<div class="m2"><p>تبسم: جوهر تیغ تغافل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آید در چمن آن سرو، خیزد</p></div>
<div class="m2"><p>پی تعظیم او، رنگ از رخ گل!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو زلف ای دل کناری گیر، تا کی</p></div>
<div class="m2"><p>نهی سر در پی خوبان چو کاکل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم او را چو استغنا بلند است</p></div>
<div class="m2"><p>توهم او را چو می بینی، تغافل</p></div></div>