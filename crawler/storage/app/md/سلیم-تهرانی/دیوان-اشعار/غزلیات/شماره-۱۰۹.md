---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>لاله را با روی او تاب قدح نوشی کجاست</p></div>
<div class="m2"><p>سرو را با قد او سامان همدوشی کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منع ناصح پرده ی رسوایی ما کی شود</p></div>
<div class="m2"><p>شعله ی عریان ما را تاب خس پوشی کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعله ی شوقش چو مومم در گداز آورده است</p></div>
<div class="m2"><p>سازگارم نیست یاد او، فراموشی کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خر طنبور نتوان زیر بار نغمه رفت</p></div>
<div class="m2"><p>مطربی ما بی دماغان [را] چو خاموشی کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خس و خارم سلیم، او شعله ی هستی گداز</p></div>
<div class="m2"><p>گر درآید در برم، تاب هم آغوشی کجاست</p></div></div>