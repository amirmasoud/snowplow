---
title: >-
    شمارهٔ ۷۷۹
---
# شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>اثر در عالم از دارا و اسکندر نمی‌بینم</p></div>
<div class="m2"><p>سری همچون کلاه لاله در افسر نمی‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین مجلس چراغی از که خواهد خواست پروانه</p></div>
<div class="m2"><p>که غیر از شمع، یک سرزندهٔ دیگر نمی‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ذوقی ز دانش نیست، لوح ساده در کار است</p></div>
<div class="m2"><p>که بر آیینه دارم چشم، من جوهر نمی‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز کویش می‌توان رفتن، قرار و صبر اگر باشد</p></div>
<div class="m2"><p>درین پرواز، همراهی ز بال و پر نمی‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان یوسف و معشوق ما نسبت نمی‌گنجد</p></div>
<div class="m2"><p>من اندر راستگویی روی پیغمبر نمی‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب جمعیتی از بوی زلف او به دست آمد</p></div>
<div class="m2"><p>پریشانی دگر زین گنج بادآور نمی‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان را تیره کرد آه و نشان گریه ظاهر نیست</p></div>
<div class="m2"><p>غبار فتنه پیدا شد، ولی لشکر نمی‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین دریا چنان بیم نهنگ آشفته‌ام دارد</p></div>
<div class="m2"><p>که چون ماهی به زیر پای خود گوهر نمی‌بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهان در پرده هر برگ گلی مشاطه‌ای دارد</p></div>
<div class="m2"><p>همین آیینه‌ای پیداست، روشنگر نمی‌بینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشد ترک کلاهم باعث آسایشی ای دل</p></div>
<div class="m2"><p>صلاح کار خود را جز به ترک سر نمی‌بینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش آن محتاج کز روی کریمان چشم بردارد</p></div>
<div class="m2"><p>درین دریا حبابی به ز نیلوفر نمی‌بینم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جنون از انقلاب روزگار آسوده‌ام دارد</p></div>
<div class="m2"><p>شهید کربلای عشقم و محشر نمی‌بینم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مکن منعم اگر چشم از جمالت برنمی‌دارم</p></div>
<div class="m2"><p>نمی‌بینم ترا عمری‌ست ای کافر، نمی‌بینم!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرسنه نیستم ای اهل همت، تشنه‌ام تشنه</p></div>
<div class="m2"><p>من آن کز آب رز دیدم در آب زر نمی‌بینم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سکون و جنبش اشیا، سلیم از خویش کی باشد</p></div>
<div class="m2"><p>همین کشتی به دریا دیده‌ام، لنگر نمی‌بینم</p></div></div>