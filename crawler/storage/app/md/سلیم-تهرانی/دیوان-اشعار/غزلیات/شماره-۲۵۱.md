---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>پایه ی نعش مرا یار من از جا برداشت</p></div>
<div class="m2"><p>آخر از خاک مرا آن گل رعنا برداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رهش هیچ کس از خاک مرا برنگرفت</p></div>
<div class="m2"><p>نقش پا را نتواند کسی از جا برداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذر از هستی و خود را به مقامی برسان</p></div>
<div class="m2"><p>تا درین ره ننهی سر، نتوان پا برداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه و صحرا همه گلزار شد از گریه ی من</p></div>
<div class="m2"><p>مژه ام منت ابر از سر دنیا برداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی از بزم برون رفت و دلم با خود برد</p></div>
<div class="m2"><p>از پی باده مگر رفت که مینا برداشت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت آن سرو روان چون سوی گلزار سلیم</p></div>
<div class="m2"><p>بلبل از دامن گل دست تمنا برداشت</p></div></div>