---
title: >-
    شمارهٔ ۷۷۸
---
# شمارهٔ ۷۷۸

<div class="b" id="bn1"><div class="m1"><p>ما در چمن بساط تمنا فکنده ایم</p></div>
<div class="m2"><p>بر برگ لاله طرح تماشا فکنده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آینه ز دیدن او چشم ما پر است</p></div>
<div class="m2"><p>یوسف به طرح، پیش زلیخا فکنده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهر صید کردن مرغابیان اشک</p></div>
<div class="m2"><p>مانند موج، دام به دریا فکنده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق، لاله را سبب اعتبار شد</p></div>
<div class="m2"><p>داغی که ما ز سینه به صحرا فکنده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود سلیم توبه شکستن گناه ما</p></div>
<div class="m2"><p>این عذر را به گردن مینا فکنده ایم</p></div></div>