---
title: >-
    شمارهٔ ۴۵۸
---
# شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>چو شعله آن گل رویم به روی خار کشید</p></div>
<div class="m2"><p>به جای سرمه به چشمم خط غبار کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سادگی ست که خال لب تو آخر کار</p></div>
<div class="m2"><p>به گرد خویش چو هندو ز خط حصار کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رهروان جهان ترک آشنایی کرد</p></div>
<div class="m2"><p>ز بس که خضر به راه من انتظار کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا ز حرف خزان خوش لطیفه ای انگیخت</p></div>
<div class="m2"><p>که گفت با گل و بر گوش شاخسار کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شغل عشق، خلاصی ندارم ای منصور</p></div>
<div class="m2"><p>مجال کو که توانم سری به دار کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم از خط او شورش من افزون شد</p></div>
<div class="m2"><p>جنون زیاده شود چون به نوبهار کشید</p></div></div>