---
title: >-
    شمارهٔ ۶۵۷
---
# شمارهٔ ۶۵۷

<div class="b" id="bn1"><div class="m1"><p>شد موی خضر در طلبت جابه جا سفید</p></div>
<div class="m2"><p>ما را ز موی سر شده تا خار پا سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر رفته است و از سر کویش نمی روم</p></div>
<div class="m2"><p>رحمت برین ثبات قدم، روی ما سفید!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شاخ یاسمن ز خرام تو سرو را</p></div>
<div class="m2"><p>بر تن ز شست و شوی عرق شد قبا سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مشت استخوان چه به جا مانده ای، که شد</p></div>
<div class="m2"><p>در راه انتظار تو چشم هما سفید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیریم و طفل خنده به تدبیر ما کند</p></div>
<div class="m2"><p>چون صبح، موی ما شده در آسیا سفید!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمی سیاه ساخته بودم برو سلیم</p></div>
<div class="m2"><p>از گریه کرد عاقبت آن بی وفا سفید</p></div></div>