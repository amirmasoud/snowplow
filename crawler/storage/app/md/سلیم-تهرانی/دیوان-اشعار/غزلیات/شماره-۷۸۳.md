---
title: >-
    شمارهٔ ۷۸۳
---
# شمارهٔ ۷۸۳

<div class="b" id="bn1"><div class="m1"><p>آنم که می به نغمه ی زنجیر می خورم</p></div>
<div class="m2"><p>ساغر به طاق ابروی شمشیر می خورم!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فیض ماهتاب، شرابم حلال شد</p></div>
<div class="m2"><p>می در پیاله می کنم و شیر می خورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیهوشی ام ز جلوه ی سبزان گیلک است</p></div>
<div class="m2"><p>در لاهجانم و می کشمیر می خورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مورم، ولی ز خرمن عشق است دانه ام</p></div>
<div class="m2"><p>چون موریانه جوهر شمشیر می خورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی سلیم، جور جهان می توان کشید؟</p></div>
<div class="m2"><p>چندین شکنجه من به چه تقصیر می خورم؟</p></div></div>