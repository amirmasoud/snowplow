---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>خدایا رهنما شو بر دل ما رهنمایی را</p></div>
<div class="m2"><p>که بنماید به ما خوشتر ازین گلزار، جایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر از بیم هرگز چشم نگذارد به هم گندم</p></div>
<div class="m2"><p>اگر در خواب بیند همچو گردون آسیایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان آمیزشی با ذات حق دارد ولی فانی ست</p></div>
<div class="m2"><p>بقا تا کی بود پیچیده بر کوهی صدایی را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خود ای دل چه خواهی کرد کز فرمان چو سرپیچد</p></div>
<div class="m2"><p>برون کردند از جنت چو آدم کدخدایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم از رشک همچون نقش پا از پای می‌افتم</p></div>
<div class="m2"><p>به خاک آستان او چو بینم نقش پایی را</p></div></div>