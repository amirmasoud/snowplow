---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>هما! نصیب تو از من چنان که خواهی نیست</p></div>
<div class="m2"><p>که استخوان مرا مغز همچو ماهی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به خاک شهیدان گذار خواهی کرد</p></div>
<div class="m2"><p>کسی که کشته نمی گردد او سپاهی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هلاک قاعده ی جنگ و صلح طفلانم</p></div>
<div class="m2"><p>که در میانه ره و رسم عذرخواهی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نام درد دل از خون خود رقم سازیم</p></div>
<div class="m2"><p>چو برق، خامه ی ما در پی سیاهی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که کسب هوایی نموده، می داند</p></div>
<div class="m2"><p>که سربرهنگی ما ز بی کلاهی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو لاله ساخته با صاف و درد خویش سلیم</p></div>
<div class="m2"><p>گدای میکده را ذوق پادشاهی نیست</p></div></div>