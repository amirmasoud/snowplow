---
title: >-
    شمارهٔ ۶۳۵
---
# شمارهٔ ۶۳۵

<div class="b" id="bn1"><div class="m1"><p>مستان تواند خسته ای چند</p></div>
<div class="m2"><p>چون توبه ی خود، شکسته ای چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوی تو همچو مرغ بسمل</p></div>
<div class="m2"><p>برخاسته و نشسته ای چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید به عدم قرار گیرند</p></div>
<div class="m2"><p>چون برق ز خویش جسته ای چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم به بساط همچو طاووس</p></div>
<div class="m2"><p>آیینه ی زنگ بسته ای چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ذوق سخن سلیم داری</p></div>
<div class="m2"><p>داریم شکسته بسته ای چند</p></div></div>