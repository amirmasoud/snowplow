---
title: >-
    شمارهٔ ۷۰۱
---
# شمارهٔ ۷۰۱

<div class="b" id="bn1"><div class="m1"><p>چو گل کسی که هوای تو برده آرامش</p></div>
<div class="m2"><p>ز موج بیخودی باده بشکند جامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند ز زلف تو صیاد، خاک از آن بر سر</p></div>
<div class="m2"><p>که غیر خاک چو غربال نیست در دامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جواب نامه ی ما را، ز بس تغافل کرد</p></div>
<div class="m2"><p>هزار بیضه کبوتر نهاد در بامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام دل که نشد صید این سیه چشمان</p></div>
<div class="m2"><p>فغان ز هند و غزالان شیراندامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سرمه ی امل آن کس که چشم ساخت سیاه</p></div>
<div class="m2"><p>سفید کرد جهان همچو مغز بادامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریب انجمن عشق را سلیم مپرس</p></div>
<div class="m2"><p>کباب کیست که آتش نمی کند خامش؟</p></div></div>