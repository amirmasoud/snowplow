---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>صاف می از دگران، لای ته شیشه ز ماست</p></div>
<div class="m2"><p>اول کاسه و دردی که شنیدی اینجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست از قید غم امید خلاصی ما را</p></div>
<div class="m2"><p>خط آزادی ما بر ورق صبح فناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زور بازو به چه کار کسی آید اینجا</p></div>
<div class="m2"><p>قوت مرد ره عشق تو در پنجه ی پاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان دفع غمم کرد کز آیینه ی من</p></div>
<div class="m2"><p>ریشه ی سبزه ی زنگار ز جوهر پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهد بی شوق به جایی نرسد در ره عشق</p></div>
<div class="m2"><p>بال چون نیست چه حاصل که کبوتر پر پاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرقه گر در گرو باده کنم، منع مکن</p></div>
<div class="m2"><p>نیست چیزی دگرم، عالم درویشی هاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهدان به که نباشند دعاگوی کسی</p></div>
<div class="m2"><p>از لب اهل ریا، فاتحه تکبیر فناست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست در شهر به ما حاجت احباب، سلیم</p></div>
<div class="m2"><p>گذری کن به سوی دشت که مجنون تنهاست</p></div></div>