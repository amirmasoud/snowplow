---
title: >-
    شمارهٔ ۶۹۷
---
# شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>در وادی محبت، چون خضر راهبر باش</p></div>
<div class="m2"><p>با رهروان دریا، چون موج همسفر باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگشت دایه در کام زهر غمم کشیده ست</p></div>
<div class="m2"><p>در دست من هر انگشت، گو شاخ نیشکر باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشوب موج و طوفان سامان اهل دریاست</p></div>
<div class="m2"><p>همچون سفینه ی عشق، مجموعه ی خطر باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندان که بال باز است، پرواز در خیال است</p></div>
<div class="m2"><p>ای دل به کنج عزلت، عنقای بسته پر باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکس به کینه ی ما خیزد، کمر نبندد</p></div>
<div class="m2"><p>چون کوه خصم ما را هر عضو گو کمر باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آسمان عدویی داری سلیم بر سر</p></div>
<div class="m2"><p>غافل نمی توان بود، از خویش باخبر باش</p></div></div>