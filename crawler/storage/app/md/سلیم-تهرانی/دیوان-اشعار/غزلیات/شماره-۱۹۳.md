---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>بیا که بی لب لعل تو بس که پیر شده ست</p></div>
<div class="m2"><p>شراب کهنه به ساغر به رنگ شیر شده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجود من به جراحت سرشته همچون گل</p></div>
<div class="m2"><p>به آب تیغ مگر خاک من خمیر شده ست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببین به شانه که دعوی شبروی می کرد</p></div>
<div class="m2"><p>که چون به کوچه ی آن زلف دستگیر شده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شوق غنچه ی پیکان او خدا داند</p></div>
<div class="m2"><p>کدام شاخ گل است این که چوب تیر شده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چمن ز مجلس شیرین خبر دهد امشب</p></div>
<div class="m2"><p>ز ماهتاب، خیابان چو جوی شیر شده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم صبح دمید و هنوز مخمورم</p></div>
<div class="m2"><p>پیاله زود بنوش و بده، که دیر شده ست</p></div></div>