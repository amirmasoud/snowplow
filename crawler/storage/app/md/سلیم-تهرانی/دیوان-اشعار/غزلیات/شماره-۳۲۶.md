---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>حسن با مهر و وفا بیگانه است</p></div>
<div class="m2"><p>هر که عاشق می شود، دیوانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست بی یاران هوای گلشنم</p></div>
<div class="m2"><p>باغبان فصل خزان در خانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی لب میگون او در انجمن</p></div>
<div class="m2"><p>شیشه سرگردان تر از پیمانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن بهر عشقبازان قحط نیست</p></div>
<div class="m2"><p>هر که شمعی دارد از پروانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راز عالم را به پایان کس نبرد</p></div>
<div class="m2"><p>گوش ما بر آخر افسانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در محبت مهر خاموشی سلیم</p></div>
<div class="m2"><p>بر لب من قفل آتشخانه است</p></div></div>