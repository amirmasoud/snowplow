---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>ای گل دگر ز دست کجا می گذارمت</p></div>
<div class="m2"><p>نتوان فریب داد مرا، خوب دارمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو ندیده ام به جهان بی وفاتری</p></div>
<div class="m2"><p>باور مکن گر اهل وفا می‌شمارمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در روزگار نیست مرا چون تو دشمنی</p></div>
<div class="m2"><p>در حیرتم که این همه چون دوست دارمت؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاری نکرده ای که نصیبم مباد، اگر</p></div>
<div class="m2"><p>از پیش دیده دور شوی یاد دارمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوقی چنان به صحبت وقت وداع نیست</p></div>
<div class="m2"><p>جان عزیز من، به خدا می سپارمت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم سرایت از تو مرا ای سرشک نیست</p></div>
<div class="m2"><p>تخمی نه ای که از پی حاصل بکارمت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش آن زمان سلیم که پرسد چو نام من</p></div>
<div class="m2"><p>گویم فلان غلام وفادار خوارمت</p></div></div>