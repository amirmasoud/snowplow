---
title: >-
    شمارهٔ ۸۰۳
---
# شمارهٔ ۸۰۳

<div class="b" id="bn1"><div class="m1"><p>صبح چون میل تماشای گلستان می‌کنیم</p></div>
<div class="m2"><p>سر برون چون غنچه از چاک گریبان می‌کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال ما آشفتگان در کار عالم عبرت است</p></div>
<div class="m2"><p>همچو گل تعبیر هر خواب پریشان می‌کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم‌نشین گرید به وقت مرگ بر بالین ما</p></div>
<div class="m2"><p>ما وداع او چو شمع صبح، خندان می‌کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که بی‌دردی ز اهل این گلستان دیده‌ایم</p></div>
<div class="m2"><p>چاک‌های سینه را چون غنچه پنهان می‌کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه عاجز دیده‌ای ما را، ز ما غافل مباش</p></div>
<div class="m2"><p>قطرهٔ اشک اسیرانیم، طوفان می‌کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه دم از کینهٔ ما زد، مهیای فناست</p></div>
<div class="m2"><p>خصم می‌پوشد کفن تا تیغ عریان می‌کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل آزاری ز یکدیگر حریفان بدترند</p></div>
<div class="m2"><p>کار چون با غنچه افتد، یاد پیکان می‌کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که در عشق از پی سامان خود افتاده‌ایم</p></div>
<div class="m2"><p>داغ اگر در دست ما باشد، نمکدان می‌کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذوق گلگشت خراسان رفته است از یاد ما</p></div>
<div class="m2"><p>در سواد هند، سیر باغ زاغان می‌کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل به دامن می‌کنند احباب در گلشن سلیم</p></div>
<div class="m2"><p>جای گل ما پاره‌های دل به دامان می‌کنیم</p></div></div>