---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>ای به خورشید سیاهی زده از روی سفید</p></div>
<div class="m2"><p>ماه نو را ز رهت گرد بر ابروی سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنبلی تاب به شاخ گل نسرین زده است:</p></div>
<div class="m2"><p>خم گیسوی تو پیچیده به بازوی سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشود هیچ در آیینه ی روشن پنهان</p></div>
<div class="m2"><p>دل چون سنگ تو پیداست ز پهلوی سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر من کز کمر نازک او بی تابم</p></div>
<div class="m2"><p>نشنیدم که برد دل ز کسی موی سفید!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه غم از تیرگی بخت، وفا گر داری</p></div>
<div class="m2"><p>خوش بود خال سیه بر طرف روی سفید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل و هوش از من بیدل رخ او برد سلیم</p></div>
<div class="m2"><p>از کدامین چمن است این گل خوشبوی سفید؟</p></div></div>