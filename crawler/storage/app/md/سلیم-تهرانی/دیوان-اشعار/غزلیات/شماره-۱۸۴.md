---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>چند نالم، دل ز طبع ناله پردازم گرفت</p></div>
<div class="m2"><p>در قفای سرمه از فریاد، آوازم گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو مرغ رشته بر پا می گذشتم زین چمن</p></div>
<div class="m2"><p>هر خس و خاری سر راهی به پروازم گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نه آن مرغم که با افسون کسی صیدم کند</p></div>
<div class="m2"><p>غمزه ی سحرآفرین او به اعجازم گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کدام آتش کبابم تا کند آن ترک مست</p></div>
<div class="m2"><p>از ترحم نیست گر از چنگ شهبازم گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگرفت انگشت از حرف من و بر لب گذاشت</p></div>
<div class="m2"><p>خوب این دیوانگی از دست غمازم گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تازه شد عشقم پس از وارستگی با او سلیم</p></div>
<div class="m2"><p>جسته بودم خوش ز دام زلف او، بازم گرفت</p></div></div>