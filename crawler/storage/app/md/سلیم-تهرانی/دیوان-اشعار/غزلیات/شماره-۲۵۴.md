---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>عشق را چندان که مهرش بود هم کینش بد است</p></div>
<div class="m2"><p>گرچه خوبی ها بسی دارد، ولی اینش بد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت شیرین ز خون کوهکن خوش غافل است</p></div>
<div class="m2"><p>دشمنی هر کس که دارد خواب سنگینش بد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه پیشت می نهد از خوان قسمت روزگار</p></div>
<div class="m2"><p>چون شراب کهنه تلخش خوب و شیرینش بد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتنه ها در زیر سر داری، ازان سرگشته ای</p></div>
<div class="m2"><p>خواب راحت کی برد آن را که بالینش بد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می توان گل چید چون شاخ گل از هرجای او</p></div>
<div class="m2"><p>خوش کمر می گویی او را، ساق سیمینش بد است؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ننگ کشتی گر نباشد، نیست عیبی در محیط</p></div>
<div class="m2"><p>بادپای موج دریا را همین زینش بد است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کلامم هر چه خواهد گو بگو دشمن سلیم</p></div>
<div class="m2"><p>حرف انکارش همه خوب است تحسینش بد است</p></div></div>