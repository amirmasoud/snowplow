---
title: >-
    شمارهٔ ۸۸۱
---
# شمارهٔ ۸۸۱

<div class="b" id="bn1"><div class="m1"><p>ای سرو همچو سایه دوان در قفای تو</p></div>
<div class="m2"><p>حسرت بهار را به خزان حنای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبان به دیده بستر راحت فکنده اند</p></div>
<div class="m2"><p>از مخمل دوخوابه ی مژگان برای تو!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماند به سبحه، بس که پی وعده ی وصال</p></div>
<div class="m2"><p>خوبان گره زدند به بند قبای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرت دراز باد که خاک مزار ما</p></div>
<div class="m2"><p>دارد دری به خلد ز هر نقش پای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همواره طعنه می شنوی از برای من</p></div>
<div class="m2"><p>ای دوست چون سلیم بمیرم برای تو!</p></div></div>