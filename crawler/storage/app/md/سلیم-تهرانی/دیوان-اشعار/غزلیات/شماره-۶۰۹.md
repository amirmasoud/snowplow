---
title: >-
    شمارهٔ ۶۰۹
---
# شمارهٔ ۶۰۹

<div class="b" id="bn1"><div class="m1"><p>ای خوش آن روز که آن سیب ذقن سبز شود</p></div>
<div class="m2"><p>هرچه می گویمت ای عهدشکن سبز شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطلبی قصد کند هرکه حدیثی سر کرد</p></div>
<div class="m2"><p>می زنم حرف خط او که سخن سبز شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبان بس که ز عشق تو جنون یافت رواج</p></div>
<div class="m2"><p>چوب گل را نگذارد به چمن سبز شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تب سوزان محبت که هلاکش گردم</p></div>
<div class="m2"><p>نگذارد که مرا موی به تن سبز شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه با تیغ شهادت نشود کشته سلیم</p></div>
<div class="m2"><p>سبزه بر تربتش از آب دهن سبز شود</p></div></div>