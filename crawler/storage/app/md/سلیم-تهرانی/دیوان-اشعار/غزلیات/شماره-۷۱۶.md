---
title: >-
    شمارهٔ ۷۱۶
---
# شمارهٔ ۷۱۶

<div class="b" id="bn1"><div class="m1"><p>عقل نگذارد مرا یک دم ز دردسر خلاص</p></div>
<div class="m2"><p>رهزنی کو تا مرا سازد ازین رهبر خلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان شود آسوده، هرگه دل قبول عشق کرد</p></div>
<div class="m2"><p>می شود، چون صاف شد آیینه، روشنگر خلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو گل مشت زری دادم، خریدم خویش را</p></div>
<div class="m2"><p>چون به غیر از این توان شد زین چمن دیگر خلاص؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند در قید زمین وآسمان باشد کسی</p></div>
<div class="m2"><p>تا صدف برپاست، مشکل گر شود گوهر خلاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه او را کار با زنجیر زلف افتاده است</p></div>
<div class="m2"><p>گو دلش گردد مگر از قید در محشر خلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم پوشیدم ازو، فارغ شدم از جور او</p></div>
<div class="m2"><p>شد ز تیغ آفتاب این گونه نیلوفر خلاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد ازین دستی ندارد بر من آسیب جهان</p></div>
<div class="m2"><p>سوختم، گشتم ز هر آفت چو خاکستر خلاص</p></div></div>