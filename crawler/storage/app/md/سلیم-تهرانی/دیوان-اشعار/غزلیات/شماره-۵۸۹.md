---
title: >-
    شمارهٔ ۵۸۹
---
# شمارهٔ ۵۸۹

<div class="b" id="bn1"><div class="m1"><p>می تا به کی ز رنگم، در زیر ننگ باشد</p></div>
<div class="m2"><p>آیینه از سرشکم، گرداب زنگ باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند عافیت نام، در این چمن گلی هست</p></div>
<div class="m2"><p>یارب چه بوی دارد، آیا چه رنگ باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عذر نیست هرگز دلگیر وعده ی او</p></div>
<div class="m2"><p>کاهل همیشه خواهد همراه لنگ باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افلاک در سماعند از کینه جویی خلق</p></div>
<div class="m2"><p>بر اهل فتنه عید است، روزی که جنگ باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز هند و گلرخانش در هیچ کشوری نیست</p></div>
<div class="m2"><p>آهو که خوابگاهش پشت پلنگ باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت چیست ای برهمن، باری سجود خود کن</p></div>
<div class="m2"><p>مهر نماز از خاک، بهتر ز سنگ باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم اگرچه تنگ است بر ما سلیم، اما</p></div>
<div class="m2"><p>نقش دهان یار است، بگذار تنگ باشد</p></div></div>