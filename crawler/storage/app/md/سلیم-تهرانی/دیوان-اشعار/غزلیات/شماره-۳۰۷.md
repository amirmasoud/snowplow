---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>شکوه ی احباب را نتوان به خود مشکل گرفت</p></div>
<div class="m2"><p>تا زبان باشد، نمی باید سخن در دل گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخودی در وصل از من انتقام خود کشید</p></div>
<div class="m2"><p>داد دل از کشتی ما موج در ساحل گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق از قید گرانجانی مرا آزاد کرد</p></div>
<div class="m2"><p>همچو برق از جا جهد، آتش چو در کاهل گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان بود در حشر از اجر شهادت بی نصیب</p></div>
<div class="m2"><p>کز تپیدن خونبهای خویش را بسمل گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سراغ کوی او از کعبه خواهم همتی</p></div>
<div class="m2"><p>از برای راه باید توشه در منزل گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل کجا ماند، سراپای مرا چون عشق سوخت</p></div>
<div class="m2"><p>حال پروانه مپرس آتش چو در محفل گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستی ما کی حریف عشق می گردد سلیم</p></div>
<div class="m2"><p>پیش راه سیل را نتوان به مشتی گل گرفت</p></div></div>