---
title: >-
    شمارهٔ ۳۸۹
---
# شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>از دل گله ی ما ره اظهار نداند</p></div>
<div class="m2"><p>عاشق بود آن طفل که گفتار نداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعلیم ازان گیر که گفتار نداند</p></div>
<div class="m2"><p>شاگرد کسی باش که بسیار نداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خسته دلانیم که ویرانی ما را</p></div>
<div class="m2"><p>همسایه ی دیوار به دیوار نداند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق، کسی را خبر از راز دلم نیست</p></div>
<div class="m2"><p>آتش به سرم سوزد و دستار نداند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحم است به حیرانی آن کز چمن وصل</p></div>
<div class="m2"><p>چون سرو به پا خیزد و رفتار نداند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با یار سلیم این همه اظهار وفا چیست</p></div>
<div class="m2"><p>عیب گهر آن به که خریدار نداند</p></div></div>