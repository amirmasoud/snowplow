---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>برق عشق آمد که سوزد خرمن تدبیر را</p></div>
<div class="m2"><p>با گریبان کار افتد دست دامنگیر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام من در دفتر اهل شهادت داخل است</p></div>
<div class="m2"><p>کرده ام روشن سواد جوهر شمشیر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاسبان مستی ما نیست غیر از تیغ عشق</p></div>
<div class="m2"><p>برگ نی باشد مگس ران وقت خفتن شیر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از طلسم هند آزادی تجرد می دهد</p></div>
<div class="m2"><p>چاره عریانی بود این خاک دامنگیر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون منی را طاقت چندین علایق از کجاست</p></div>
<div class="m2"><p>فیل نتواند کشیدن این قدر زنجیر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو شاهینی که مرغی را کمین سازد سلیم</p></div>
<div class="m2"><p>تا هوا گیرد دل من می‌رباید تیر را</p></div></div>