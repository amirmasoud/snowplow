---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>کی ز تیغ آفتاب خویش باشد غم مرا</p></div>
<div class="m2"><p>سر بود در راه او چون قطرهٔ شبنم مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که همچون سبزه‌ام هر شبنم آب زندگی ست</p></div>
<div class="m2"><p>از چه دارد ابر بر سر منت عالم مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معجز عیسی ز زخم سینهٔ من عاجز است</p></div>
<div class="m2"><p>کی چو داغ آینه سودی دهد مرهم مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به کی از آتش هر کس گدازم همچو موم</p></div>
<div class="m2"><p>کاشکی بردی دلی از سنگ چون خاتم مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در قیامت ساقی کوثر اگر جامی دهد</p></div>
<div class="m2"><p>کافرم گر باشد از آشوب محشر غم مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که دست قدرتت، هم پنجه ی صنع خداست</p></div>
<div class="m2"><p>مشت خاکی قابلم، گر می کنی آدم مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوخته گر آتش غم شاخسارم را سلیم</p></div>
<div class="m2"><p>می‌تواند کرد ابر دست او خرم مرا</p></div></div>