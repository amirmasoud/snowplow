---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>چشم پرافسون او سحرآفرینی می‌کند</p></div>
<div class="m2"><p>تیر مژگانش ز شوخی دل‌نشینی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر حسن است و کار او به زلف افتاده است</p></div>
<div class="m2"><p>داده خرمن را به باد و خوشه‌چینی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش پای خویش را هرکس نمی‌بیند چو شمع</p></div>
<div class="m2"><p>لاف باطل می‌زند گر دوربینی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعمت فغفور را فیضی که در خاصیت است</p></div>
<div class="m2"><p>کاسه چوبین گدا را چوب چینی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سایه را با خویشتن همره نمی‌خواهد سلیم</p></div>
<div class="m2"><p>همچو عنقا هرکه او وحدت‌گزینی می‌کند</p></div></div>