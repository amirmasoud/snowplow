---
title: >-
    شمارهٔ ۴۱۳
---
# شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>دل آشفته از نام شراب ناب می سوزد</p></div>
<div class="m2"><p>گیاه خشک ما را همچو آتش آب می سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان از آتش دل دود آهم مضطرب خیزد</p></div>
<div class="m2"><p>که پنداری درون سینه ام سیماب می سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بسملگاه شوق کینه پردازان من آن صیدم</p></div>
<div class="m2"><p>که از خونگرمی من دامن قصاب می سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنون از داغ های تن چنانم بی خبر دارد</p></div>
<div class="m2"><p>که گویی جامه ی هستی مگر در خواب می سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تاب شمع رویی محفل ما روشن است امشب</p></div>
<div class="m2"><p>که چون پیراهن فانوس ازو مهتاب می سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم از بس دلم سرگرم استغفار عصیان است</p></div>
<div class="m2"><p>اگر اشکی ز مژگانم چکد، محراب می سوزد</p></div></div>