---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>ای دل سفر به لجه ی عمان مبارک است</p></div>
<div class="m2"><p>دریا به ما چو چشمه ی حیوان مبارک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارت اگر چو ابر به دریا فتاده است</p></div>
<div class="m2"><p>غمگین مباش، روی کریمان مبارک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گردباد، چند به خشکی سفر کنم</p></div>
<div class="m2"><p>رفتن چو موج بر سر طوفان مبارک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن را که از محیط طلبکار گوهر است</p></div>
<div class="m2"><p>هر موج همچو کوه بدخشان مبارک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دوش باد، سیر جهان کرده می رویم</p></div>
<div class="m2"><p>کشتی به ما چو تخت سلیمان مبارک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند از تو ابر کرم تند بگذرد</p></div>
<div class="m2"><p>چون گل ترا گشودن دامان مبارک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق کفن چو یافت، اجل احتیاج نیست</p></div>
<div class="m2"><p>هر وقت هست، جامه به عریان مبارک است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلگیر نیست عشق ز آه دلم سلیم</p></div>
<div class="m2"><p>گرد سپاه خویش به سلطان مبارک است</p></div></div>