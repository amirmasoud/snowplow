---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>صد نشان خاک مرا از اثر عصیان است</p></div>
<div class="m2"><p>یکی از جمله ی آنها گل نافرمان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان درین بحر خموشند حقایق دانان</p></div>
<div class="m2"><p>که حبابی چو برآورد نفس، طوفان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوقی از دیدن معشوق به دلگیری نیست</p></div>
<div class="m2"><p>غنچه در چاک قفس، قفل در زندان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باخبر باش فریبت ندهد ای زاهد</p></div>
<div class="m2"><p>می دود در رگ و پی، دختر رز شیطان است!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کام دل جلوه گر و دست تصرف کوتاه</p></div>
<div class="m2"><p>نفس ما همچو سگ خانه ی آهوبان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چهره گلگون نکند جز می انگور، سلیم</p></div>
<div class="m2"><p>رگ تاک است که در هند خطابش پان است</p></div></div>