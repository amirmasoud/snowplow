---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>تا کی بود ز راه خطا، پیچ و تاب ما</p></div>
<div class="m2"><p>یارب کجاست هادی راه صواب ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش زدیم و چون پر پروانه سوختیم</p></div>
<div class="m2"><p>حرف خطا، به آب نرفت از کتاب ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم ز کفر همچو دل شب سیاه شد</p></div>
<div class="m2"><p>کی باشد آنکه تیغ کشد آفتاب ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان انتظار مقدم خورشید می کشد</p></div>
<div class="m2"><p>چون شمع صبح نیست عبث اضطراب ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عذر گنه به حشر بود لطف او سلیم</p></div>
<div class="m2"><p>عاجز نمی شود دل حاضر جواب ما</p></div></div>