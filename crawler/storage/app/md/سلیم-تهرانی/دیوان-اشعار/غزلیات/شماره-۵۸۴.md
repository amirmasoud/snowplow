---
title: >-
    شمارهٔ ۵۸۴
---
# شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>ز بالین همنشینم هر نفس غمناک برخیزد</p></div>
<div class="m2"><p>نشیند غنچه و چون گل گریبان چاک برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پریشانی به خاک هرکس از روز ازل آمیخت</p></div>
<div class="m2"><p>به محشر هم پریشان چون غبار از خاک برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به داغ رشک آن افتاده، همچون لاله می سوزم</p></div>
<div class="m2"><p>که نتواند ز مستی در چمن چون تاک برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می از آلایش هستی کشد دامان عارف را</p></div>
<div class="m2"><p>چو کاغذ تر شود، از روی معجون پاک برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک صد دور می باید که در ایران زمین گردد</p></div>
<div class="m2"><p>که همچون من غبار دیگری زان خاک برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم ایام از پست و بلند خود نمی‌گردد</p></div>
<div class="m2"><p>نشستم من به خاک راه تا افلاک برخیزد</p></div></div>