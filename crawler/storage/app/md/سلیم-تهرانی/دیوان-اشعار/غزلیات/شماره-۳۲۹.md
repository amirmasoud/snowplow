---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>نشان هستی من چون حباب پیرهن است</p></div>
<div class="m2"><p>ز ضعف، بند قبای من آستین من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن ز سایه ی دیوار خویش ما را دور</p></div>
<div class="m2"><p>که آشیانه ی ما چشم زخم این چمن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم به سینه ز آسیب نفس ایمن نیست</p></div>
<div class="m2"><p>که گرگ یوسف ما را درون پیرهن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه در صف مردان عشق پای نهی</p></div>
<div class="m2"><p>که جان عزیز ترا چون چراغ بیوه زن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه چشم بدی در قفای خود داریم</p></div>
<div class="m2"><p>غبار قافله ی ما ز خاک راهزن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو نیست نغمه ی سازی، شراب نتوان خورد</p></div>
<div class="m2"><p>که نان خشک به از آب خشک حرف من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گنج ها که نثار سخن شهان کردند</p></div>
<div class="m2"><p>اگر زمانه دگر شد، سخن همان سخن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلیم، ذوق خموشی مرا ز کار انداخت</p></div>
<div class="m2"><p>دلم ز قطع نفس همچو دلو بی رسن است</p></div></div>