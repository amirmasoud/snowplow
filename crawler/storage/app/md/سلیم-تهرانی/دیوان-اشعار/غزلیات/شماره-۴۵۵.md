---
title: >-
    شمارهٔ ۴۵۵
---
# شمارهٔ ۴۵۵

<div class="b" id="bn1"><div class="m1"><p>تنها من ضعیف ندارم بدن کبود</p></div>
<div class="m2"><p>عشقم چنان فشرده که شد پیرهن کبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بعد مرگ بنگری، از سنگ حادثات</p></div>
<div class="m2"><p>چون لاجورد سوده بود خاک من کبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز من کسی کجاست که گیرد عزای من</p></div>
<div class="m2"><p>همدم! به روز مرگ مرا کن کفن کبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجنون خسته، سنگ برای تو می خورد</p></div>
<div class="m2"><p>لیلی! ترا برای چه گردیده تن کبود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز مرا به چشم نیاید فلک سلیم</p></div>
<div class="m2"><p>در حیرتم که از چه بود چشم من کبود</p></div></div>