---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>می بی منت اگر میل کنی، حیرانی ست</p></div>
<div class="m2"><p>جامه ی مفت اگر می طلبی، عریانی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصه ی افسر کیخسرو و تاج جمشید</p></div>
<div class="m2"><p>به سر خاک نشینان که مرصع خوانی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشنایی همه جا از ره نسبت خیزد</p></div>
<div class="m2"><p>مست را دوستی ابر ز تر دامانی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چنین کوتهی عمر، بیان نتوان کرد</p></div>
<div class="m2"><p>قصه ی طول امل را، که سخن طولانی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می دوند از پس و پیشم به تماشا خلقی</p></div>
<div class="m2"><p>عشق را شور جنون، کوکبه ی سلطانی ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تمنای سجود در او کاسته ام</p></div>
<div class="m2"><p>چون هلال آنچه ز من مانده به جا، پیشانی ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتوان گفت می کوثر و آب زمزم</p></div>
<div class="m2"><p>شرح کیفیت لعل لب او وجدانی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم مستی که ربوده ست ز سر هوش مرا</p></div>
<div class="m2"><p>شیر از نسبت او در پی آهوبانی ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چاره ی زخم دلم مرهم عیسی نکند</p></div>
<div class="m2"><p>خنجری نیست مرا چاک جگر، مژگانی ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صحبت عشق و جنون گرم چو گردید سلیم</p></div>
<div class="m2"><p>کشتی حوصله از شبنم می طوفانی ست</p></div></div>