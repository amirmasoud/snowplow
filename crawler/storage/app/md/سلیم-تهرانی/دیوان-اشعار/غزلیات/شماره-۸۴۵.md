---
title: >-
    شمارهٔ ۸۴۵
---
# شمارهٔ ۸۴۵

<div class="b" id="bn1"><div class="m1"><p>تقدیر خدا را چه علاج است، گداییم</p></div>
<div class="m2"><p>در رزق سگ و گربه شریکیم، هماییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که از ساده دلی صاحب خرمن</p></div>
<div class="m2"><p>برقیم [و] گمان برده که ما کاهرباییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ما نشود سینه ی این کینه دلان صاف</p></div>
<div class="m2"><p>چون آینه هرچند که از اهل صفاییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با مردم عالم نتوانیم به سر برد</p></div>
<div class="m2"><p>ره بر سر خار و خس و ما آبله پاییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سلسله ی باده کشان جام شرابیم</p></div>
<div class="m2"><p>در قافله ی کعبه روان قبله نماییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نقش قدم پیشتر از ماست درین راه</p></div>
<div class="m2"><p>چون چشم حسودان همه کس را به قفاییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک بار نگفتید سلیم این چه خموشی ست</p></div>
<div class="m2"><p>ای اهل کرم، بنده ی انصاف شماییم!</p></div></div>