---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>خمار وصل، دلم را ز اضطراب شکست</p></div>
<div class="m2"><p>ز موج رعشه به کف ساغر شراب شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیاله از کف دشمن مگیر، رحمی کن</p></div>
<div class="m2"><p>که استخوان به تن من ز پیچ و تاب شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مباش در پی نخوت، نگاه کن که چه دید</p></div>
<div class="m2"><p>چو از غرور، کله گوشه را حباب شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکستگی زرهی داده شبنم ما را</p></div>
<div class="m2"><p>که از سرایت آن، تیغ آفتاب شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جای رفت دل من ز نام گریه سلیم</p></div>
<div class="m2"><p>بنای خانهٔ ما از صدای آب شکست</p></div></div>