---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>سبزهٔ خطش دمید و روزگار عاشقی‌ست</p></div>
<div class="m2"><p>فصل گلریزان داغ و نوبهار عاشقی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دلم چون کاوکاو شوق را بیرون کنم؟</p></div>
<div class="m2"><p>نیست این خار کف پا، خارخار عاشقی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان با خاکساران در مقام کبر نیست</p></div>
<div class="m2"><p>این غبار خاطرم از رهگذار عاشقی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیف اوقاتی که صرف کار دیگر می‌شود</p></div>
<div class="m2"><p>فکر فکر می‌پرستی، کار کار عاشقی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بقای جاودان خواهی سلیم از عشق جوی</p></div>
<div class="m2"><p>زان که آب زندگی در جویبار عاشقی‌ست</p></div></div>