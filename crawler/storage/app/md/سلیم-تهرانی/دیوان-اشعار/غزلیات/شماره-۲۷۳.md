---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>شمع از هوای وصلت، در حالت هلاک است</p></div>
<div class="m2"><p>گل تا شنیده بویت، از شوق سینه چاک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شستم غبار خود را با گریه از دل خلق</p></div>
<div class="m2"><p>در عشق او حسابم با کاینات پاک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس که بی رخ او چشمم غبار دارد</p></div>
<div class="m2"><p>چون دانه های سبحه، اشکم تمام خاک است!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند بخیه بتوان بر زخم های خود زد؟</p></div>
<div class="m2"><p>چون موج سینه ام را چاک از قفای چاک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را سلیم بی قدر در عاشقی، وفا کرد</p></div>
<div class="m2"><p>کم قیمت است آری جنسی که عیب ناک است</p></div></div>