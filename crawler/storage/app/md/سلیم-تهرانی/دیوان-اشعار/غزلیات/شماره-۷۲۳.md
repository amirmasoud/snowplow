---
title: >-
    شمارهٔ ۷۲۳
---
# شمارهٔ ۷۲۳

<div class="b" id="bn1"><div class="m1"><p>به قدر جرم برد هرکسی ز رحمت حظ</p></div>
<div class="m2"><p>ز لطف حق کند ابلیس در قیامت حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آن سرم که به دیوانگی زنم خود را</p></div>
<div class="m2"><p>که بی جنون نتوان کرد از محبت حظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای پرسشم ای همنشین چو آمده ای</p></div>
<div class="m2"><p>ببند لب که ندارم من از نصیحت حظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر استخوان هلال است چشم من دایم</p></div>
<div class="m2"><p>ز بس که همچو هما دارم از قناعت حظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه از جنون و نه از عقل، در جهان ما را</p></div>
<div class="m2"><p>نشد چو آینه حاصل به هیچ صورت حظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم مصلحت خویش در جنون دیدم</p></div>
<div class="m2"><p>به کوی عشق ز بس دارم از ملامت حظ</p></div></div>