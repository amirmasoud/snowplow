---
title: >-
    شمارهٔ ۹۰۲
---
# شمارهٔ ۹۰۲

<div class="b" id="bn1"><div class="m1"><p>یک نفس چون لاله جام از کف ز هشیاری منه</p></div>
<div class="m2"><p>چون گل از سر افسر آشفته دستاری منه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو شاخ گل ز کف مگذار جام باده را</p></div>
<div class="m2"><p>بر زمین چیزی که باید باز برداری منه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل معنی را نباشد بر عناصر اعتبار</p></div>
<div class="m2"><p>همچو صورت پشت بر این چاردیواری منه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سست‌پیمان است گردون، تکیه بر عهدش مکن</p></div>
<div class="m2"><p>پشت چون آیینه بر این موم زنگاری منه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عافیت خواهی، درشتی را حصار خویش کن</p></div>
<div class="m2"><p>همچو کبک کوهساری پا به همواری منه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز خمار می نداری علت دیگر سلیم</p></div>
<div class="m2"><p>همچو چشم گلرخان، تهمت به بیماری منه</p></div></div>