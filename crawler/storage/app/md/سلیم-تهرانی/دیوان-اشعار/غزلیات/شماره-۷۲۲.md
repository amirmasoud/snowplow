---
title: >-
    شمارهٔ ۷۲۲
---
# شمارهٔ ۷۲۲

<div class="b" id="bn1"><div class="m1"><p>ای گل روی تو بهار نشاط</p></div>
<div class="m2"><p>قامتت سرو جویبار نشاط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب گل چاک شد ز خمیازه</p></div>
<div class="m2"><p>بی تو دارد ز بس خمار نشاط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت فصل خزان و می آید</p></div>
<div class="m2"><p>موسم عیش و روزگار نشاط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبلان را بود شب نوروز</p></div>
<div class="m2"><p>زر گل، مایه ی قمار نشاط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد بهار و چو غنچه مرغ چمن</p></div>
<div class="m2"><p>مست خفته ست در کنار نشاط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل شکفت و شراب ناب رسید</p></div>
<div class="m2"><p>شد مهیا تمام کار نشاط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاک سازد نسیم عشق، سلیم</p></div>
<div class="m2"><p>زعفران را ز دل غبار نشاط</p></div></div>