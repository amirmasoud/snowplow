---
title: >-
    شمارهٔ ۸۸۹
---
# شمارهٔ ۸۸۹

<div class="b" id="bn1"><div class="m1"><p>ای شعلهٔ حسنت را، جان ها شده پروانه</p></div>
<div class="m2"><p>در حلقهٔ زلفت دل، مجنون و سیه خانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب تا به سحر ای شمع از شوق وصال تو</p></div>
<div class="m2"><p>آغوش دلم باز است همچون پر پروانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگاه به ما ساقی از غمزه اشارت کرد</p></div>
<div class="m2"><p>از کف دل پرخون را دادیم چو پیمانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صحبت خاکستر، گردد دل من روشن</p></div>
<div class="m2"><p>چون آینه برعکس است، کار من دیوانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از میکده رندان را کی منع توان کردن</p></div>
<div class="m2"><p>هر رگ به تن مستان، راهی ست به میخانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیمانه سلیم از کف مگذار به فصل گل</p></div>
<div class="m2"><p>سرمایهٔ عقل این است، دیگر همه افسانه</p></div></div>