---
title: >-
    شمارهٔ ۶۷۳
---
# شمارهٔ ۶۷۳

<div class="b" id="bn1"><div class="m1"><p>گلرخان بینند هرگه بر اسیر یکدگر</p></div>
<div class="m2"><p>آفرین گویند بر هم زخم تیر یکدگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز فریاد دل آید سوی زلف از کاکلش</p></div>
<div class="m2"><p>شبروان یابند هم را از صفیر یکدگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست در دست سبو دارم که در این روزگار</p></div>
<div class="m2"><p>هم مگر باشند مستان دستگیر یکدگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر لبانش الفتی دارند با هم، دور نیست</p></div>
<div class="m2"><p>خورده اند ایام طفلی هر دو شیر یکدگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من زنم بر شیخ طعن، اهل ریا بر می فروش</p></div>
<div class="m2"><p>ناسزا تا کی توان گفتن به پیر یکدگر؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگدستان راچه حاجت درد دل گفتن سلیم</p></div>
<div class="m2"><p>راز هم خوانند از نقش حصیر یکدگر</p></div></div>