---
title: >-
    شمارهٔ ۵۸۱
---
# شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>اضطراب دلم از شوق تو دیدن دارد</p></div>
<div class="m2"><p>مرغ بسمل چه هنر غیر تپیدن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ره دیر و حرم پای مکش همچو غبار</p></div>
<div class="m2"><p>قدمی چند به هر کوچه دویدن دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندهد تیغ سزای سر افسرطلبان</p></div>
<div class="m2"><p>چون سر شمع، به مقراض بریدن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده چون افکند از چهره ی گل، رویش را</p></div>
<div class="m2"><p>دوستان خوب ببینید که دیدن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندهد دل که کسی بگذرد از کوچه ی ما</p></div>
<div class="m2"><p>سیل اینجا هوس خانه خریدن دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست مکتوب، که این غنچه ی خون آلودی ست</p></div>
<div class="m2"><p>راست این است که این نامه دریدن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر متاعی که بود، قابل سنجیدن نیست</p></div>
<div class="m2"><p>بجز از باده ی گلگون که کشیدن دارد!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون برد دست تهی از سر کوی تو سلیم؟</p></div>
<div class="m2"><p>غنچه ای از چمن وصل تو چیدن دارد</p></div></div>