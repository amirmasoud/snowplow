---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>بر آن سرم که کنم فاش گفتگوی شراب</p></div>
<div class="m2"><p>گواه مستی زاهد شوم چه بوی شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسانده ایم به جایی شراب خوردن را</p></div>
<div class="m2"><p>که پشت دست نهد پیش ما سبوی شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به محفلی که نباشی چو موج می خندان</p></div>
<div class="m2"><p>حباب هم نگشاید نظر به روی شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسید فصل بهار و ضرور شد دیگر</p></div>
<div class="m2"><p>دماغ خشک مرا روغن کدوی شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه زور و قوت مردافکنی ست، پنداری</p></div>
<div class="m2"><p>که خاک رستم یک دست شد سبوی شراب!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم چیز دگر جای می نمی گیرد</p></div>
<div class="m2"><p>به خاک برد خم گنج، آرزوی شراب</p></div></div>