---
title: >-
    شمارهٔ ۷۳۵
---
# شمارهٔ ۷۳۵

<div class="b" id="bn1"><div class="m1"><p>کنم به جام می اوقات عمر چون جم صرف</p></div>
<div class="m2"><p>دماغ کو که کند کس به کار عالم صرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرو به کعبه که می بایدت خمار کشید</p></div>
<div class="m2"><p>شراب می شود اینجا چو آب زمزم صرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا به گلشن روحانیان سروکار است</p></div>
<div class="m2"><p>درین چمن چه کنی آبرو چو شبنم صرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز باد دستی ما خضر را نصیبی نیست</p></div>
<div class="m2"><p>که چون حباب کند عمر را به یک دم صرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیم گل به من آرد ز دوزخ و گوید</p></div>
<div class="m2"><p>که در بهشت مکن عمر همچو آدم صرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علاج زخم دلم ای طبیب مفت نشد</p></div>
<div class="m2"><p>که خونبهای ترا کرده ام به مرهم صرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو گل به خنده کسی هرگزم ندید سلیم</p></div>
<div class="m2"><p>به گریه عمر مرا شد چو شمع ماتم صرف</p></div></div>