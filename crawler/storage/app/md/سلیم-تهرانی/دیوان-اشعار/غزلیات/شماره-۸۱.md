---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>چو غنچه جمع کن از خار عشق دامان را</p></div>
<div class="m2"><p>به دست چاک مده همچو گل گریبان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فکر عشق بنازم که خوب پیدا کرد</p></div>
<div class="m2"><p>برای قفل جنون، پره ی بیابان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهشت به ز سر کوی او نخواهد بود</p></div>
<div class="m2"><p>کسی که یافته این را، چه می کند آن را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازو مپرس حدیث سیاه بختی ما</p></div>
<div class="m2"><p>که سرمه دان نکند هیچ کس نمکدان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم تا به کی از شوق دوستان عراق</p></div>
<div class="m2"><p>چو ابر، گل کنم از گریه خاک گیلان را</p></div></div>