---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>شراب غمزه ی مست تو خون بی گنه است</p></div>
<div class="m2"><p>ز فتنه آنچه به عاشق نمی کند، نگه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو کاغذی که بر آن مد کشند از پی مشق</p></div>
<div class="m2"><p>ز تازیانه ی او پای تا سرم سیه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشت آنکه نهد باغبان به ما منت</p></div>
<div class="m2"><p>بهار آمد و عالم تمام سیرگه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمار لشکر غم را همین قدر دانم</p></div>
<div class="m2"><p>که چشم حوصله تا کار می کند، سپه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شکسته ی ما مهر و کین نمی داند</p></div>
<div class="m2"><p>ز هر دری که درآیی سوی خرابه ره است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو حسن کعبه چه دانی که نیستی محرم</p></div>
<div class="m2"><p>ز دور، جامه ی هر کس، گمان بری، سیه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلیم، یوسف دل را خبر چه می پرسی</p></div>
<div class="m2"><p>بجز خدای که داند که در کدام چه است</p></div></div>