---
title: >-
    شمارهٔ ۸۴۲
---
# شمارهٔ ۸۴۲

<div class="b" id="bn1"><div class="m1"><p>ای شبنم وجود مرا آفتاب گرم</p></div>
<div class="m2"><p>چشمت گرسنه است و دل من کباب گرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه زیان ماست، به ما سود می دهد</p></div>
<div class="m2"><p>آتش برای داغ دل ماست آب گرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلشن همیشه بهار ملایمت</p></div>
<div class="m2"><p>اصلاح نخل موم کند، آفتاب گرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم هزار آبله بر تن ز جوش اشک</p></div>
<div class="m2"><p>آتش گرفت پیرهن زین گلاب گرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامان عشق، بستر آسایش من است</p></div>
<div class="m2"><p>همچون سپند سوخت مرا جامه خواب گرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با شعله چاره ی دل مجروح کن سلیم</p></div>
<div class="m2"><p>بر درد سینه فایده دارد شراب گرم</p></div></div>