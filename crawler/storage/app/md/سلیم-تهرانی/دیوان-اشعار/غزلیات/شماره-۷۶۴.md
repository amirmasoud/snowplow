---
title: >-
    شمارهٔ ۷۶۴
---
# شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>سوی خم می این دل مخمور فرستیم</p></div>
<div class="m2"><p>پروانه ی خود را به سوی طور فرستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صحبت هم، ذوق بود تنگدلان را</p></div>
<div class="m2"><p>برخیز سلیمان که پی مور فرستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی ز سفال سر خم باده به ما ده</p></div>
<div class="m2"><p>تا ساغر چینی سوی فغفور فرستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سروی چو تو در گلشن فردوس ندیده ست</p></div>
<div class="m2"><p>در جلوه درآ تا ز پی حور فرستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشتاق ترا تحفه همین عرض نیاز است</p></div>
<div class="m2"><p>جان خود چه بود تا ز ره دور فرستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را به سوی کوی تو، چون طفل دبستان</p></div>
<div class="m2"><p>از بس ز تو رنجیده، به صد زور فرستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نیش حریفان جهان تا شود آزاد</p></div>
<div class="m2"><p>دل را به نهانخانه ی زنبور فرستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را بجز از روح نظیری نشناسد</p></div>
<div class="m2"><p>فیروزه ی خود را به نشابور فرستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواندیم سلیم این غزل تازه به حاسد</p></div>
<div class="m2"><p>تا مرهم الماس به ناسور فرستیم</p></div></div>