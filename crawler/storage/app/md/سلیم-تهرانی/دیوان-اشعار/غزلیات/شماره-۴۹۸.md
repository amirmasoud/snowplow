---
title: >-
    شمارهٔ ۴۹۸
---
# شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>نسیم صبحدم از موسمی نوید دهد</p></div>
<div class="m2"><p>که سرو رعشه ز سرما به یاد بید دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز برگ بید که در آب ریخت باد خزان</p></div>
<div class="m2"><p>حباب یاد ز طاس چهل کلید دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوا ز بس که خنک شد، ز بیم جان زاهد</p></div>
<div class="m2"><p>به قیمت می گلگون زر سفید دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون که آب طراوت به سرو و بید نماند</p></div>
<div class="m2"><p>کجاست ساقی گلچهره تا نبید دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم دل نگذاری به رنگ و بوی بهار</p></div>
<div class="m2"><p>که گل به باغ نشان از حنای عید دهد</p></div></div>