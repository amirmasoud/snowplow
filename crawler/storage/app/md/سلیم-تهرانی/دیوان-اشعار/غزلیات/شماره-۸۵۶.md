---
title: >-
    شمارهٔ ۸۵۶
---
# شمارهٔ ۸۵۶

<div class="b" id="bn1"><div class="m1"><p>چه آب و خاک و چه نیکویی سرشت است این</p></div>
<div class="m2"><p>سبوی باده نگویم، گل بهشت است این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنای عیش به میخانه می نهد دوران</p></div>
<div class="m2"><p>وگرنه هر خم می را به سر چه خشت است این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سجده سر چو نهی، ترک سر کن ای زاهد</p></div>
<div class="m2"><p>نه مسجد است، غلط کرده ای، کنشت است این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حیرتم که دلم زنده چون کباب شده ست</p></div>
<div class="m2"><p>اگر غلط نکنم، طایر بهشت است این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم، نامه ی او را ز بس نهم بر سر</p></div>
<div class="m2"><p>گمان بری که مرا خط سرنوشت است این</p></div></div>