---
title: >-
    شمارهٔ ۸۵۷
---
# شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>از کوی عشقت ای بت دلجوی دیگران</p></div>
<div class="m2"><p>رفتم که جا کنم به سر کوی دیگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بینمت به غیر، که دیدن نمی توان</p></div>
<div class="m2"><p>تیر ترا نشسته به پهلوی دیگران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مذهب دلم که سجود تو می کند</p></div>
<div class="m2"><p>محراب کج بود خم ابروی دیگران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاف از نسب مزن که به مانند آینه</p></div>
<div class="m2"><p>آدم نمی شود کسی از روی دیگران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عاشقی چه فیضی ببیند ز دل سلیم؟</p></div>
<div class="m2"><p>تعویذ خویش بسته به بازوی دیگران</p></div></div>