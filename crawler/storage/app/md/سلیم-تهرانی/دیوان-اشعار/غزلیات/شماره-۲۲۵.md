---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>شمعیم و زندگانی ما در گداز ماست</p></div>
<div class="m2"><p>پروانه ایم و سوختن خود نیاز ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسوا گذشته ایم ازین باغ چون بهار</p></div>
<div class="m2"><p>هر جا گلی شکفته ببینید، راز ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر می کنی به مذهب آزادگان عمل</p></div>
<div class="m2"><p>بگشای دست بسته که شرط نماز ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهمان به خانه دیر چو ماند، عزیز نیست</p></div>
<div class="m2"><p>کوتاهی زمانه ز عمر دراز ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را گریز نیست ز ناز تو چون سلیم</p></div>
<div class="m2"><p>هر حلقه ای ز زلف تو دام نیاز ماست</p></div></div>