---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>همچو لاله روزگارم در قدح نوشی گذشت</p></div>
<div class="m2"><p>حیف ایامی که همچون گل به بیهوشی گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد ازین آتش زبانی را تماشا کن که چیست</p></div>
<div class="m2"><p>سوختم چون شمع، کار من ز خاموشی گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی مروت! این که یاد ما ز یادت رفته است</p></div>
<div class="m2"><p>چون فراموشی توان گفت، از فراموشی گذشت!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز زمین و آسمان کس رتبه ی ما را نیافت</p></div>
<div class="m2"><p>همچو آن حرف بلندی کو به سرگوشی گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوهر شمشیر ظاهر شد ز عریانی سلیم</p></div>
<div class="m2"><p>نیستم آیینه، عمرم در نمدپوشی گذشت</p></div></div>