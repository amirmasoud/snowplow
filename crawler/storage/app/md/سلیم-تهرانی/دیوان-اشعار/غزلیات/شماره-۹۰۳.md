---
title: >-
    شمارهٔ ۹۰۳
---
# شمارهٔ ۹۰۳

<div class="b" id="bn1"><div class="m1"><p>ناله ای از من به کام دل نشد انگیخته</p></div>
<div class="m2"><p>سرمه پنداری به خاک من چو زاغ آمیخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نصیحت آن که آموزد ترا سنگین دلی</p></div>
<div class="m2"><p>بیضه ی فولاد را با موم سازد ریخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در محبت مطلب از آن جو که کام کس نداد</p></div>
<div class="m2"><p>گنج پیدا می شود اینجا ز خاک بیخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر محیط عشق در جوش آید، از هر قطره ای</p></div>
<div class="m2"><p>می شود چون آسمان چندین حباب انگیخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمرها شد تا سلیم از هم جدا افتاده ایم</p></div>
<div class="m2"><p>دوستان چون دانه های سبحه ای بگسیخته</p></div></div>