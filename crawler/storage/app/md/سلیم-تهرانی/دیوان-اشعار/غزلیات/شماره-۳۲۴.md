---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>به چشم همت من عرصه ی زمین تنگ است</p></div>
<div class="m2"><p>گشاده است مرا دست و آستین تنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان رسیده ام از محرمان طره ی دوست</p></div>
<div class="m2"><p>که عیش مور ز پهلوی خوشه چین تنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان ز عهده ی شکر تو چون برون آید؟</p></div>
<div class="m2"><p>که بر بزرگی نام تو این نگین تنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آستان تو عرض نیاز خواهم کرد</p></div>
<div class="m2"><p>بساط سجده گشاد و مرا جبین تنگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جوش سبزه ی خط شد تبسمش دلگیر</p></div>
<div class="m2"><p>فغان که جای ز موران بر انگبین تنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو موج آب روان گشت سوی پیشانی</p></div>
<div class="m2"><p>در آستین تو از بس که جای چین تنگ است!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آن امید که گیرم سراغ طره ی او</p></div>
<div class="m2"><p>همیشه خانه ام از جوش شانه بین تنگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بارگاه سلیمان، بساط طبع سلیم</p></div>
<div class="m2"><p>گشاده است، چه حاصل که این زمین تنگ است</p></div></div>