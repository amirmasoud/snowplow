---
title: >-
    شمارهٔ ۵۲۶
---
# شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>هرکه شوق طایر وصل تو صیادش کند</p></div>
<div class="m2"><p>غیر عنقا هرچه در دام آید، آزادش کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر تیشه حربه ای خسرو نمی گیرد به دست</p></div>
<div class="m2"><p>اندکی مانده که شیرین همچو فرهادش کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگارم قدر نشناسد ز نادانی، که طفل</p></div>
<div class="m2"><p>گنج نامه گر بیابد، کاغذ بادش کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ ما را تاب بی پروایی صیاد نیست</p></div>
<div class="m2"><p>کاشکی بر گرد سر گردانده آزادش کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فراموشان زیر خاک بودن مشکل است</p></div>
<div class="m2"><p>وقت آن کس خوش که چون میرد، کسی یادش کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه بیمار غم عشق بتان باشد سلیم</p></div>
<div class="m2"><p>بر سر بالین اجل بنشیند و یادش کند</p></div></div>