---
title: >-
    شمارهٔ ۶۱۶
---
# شمارهٔ ۶۱۶

<div class="b" id="bn1"><div class="m1"><p>دل در طلب چه گوش به صوت درا کند</p></div>
<div class="m2"><p>مجنون عشق، رقص به آواز پا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست تو پابرهنه به دریا حباب وار</p></div>
<div class="m2"><p>بر روی آب گردد و کسب هوا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درویش عشق را ز قلم دست کوته است</p></div>
<div class="m2"><p>مشق شکستگی ز نی بوریا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریان به عالم آمد و نالان به خاک رفت</p></div>
<div class="m2"><p>چون کوه، سنگ تربت عاشق صدا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ملک هند، بی می انگور سوختیم</p></div>
<div class="m2"><p>کو غوره ای دریغ که کس توتیا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را گمان صبر و شکیبی به خویش هست</p></div>
<div class="m2"><p>معلوم می شود گره خود چو وا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مغرور را سزا رسد از دور آسمان</p></div>
<div class="m2"><p>باد از بروت خوشه برون آسیا کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون قطره، برگرفتهٔ خود را جهان سلیم</p></div>
<div class="m2"><p>بر آسمان رساند و از کف رها کند</p></div></div>