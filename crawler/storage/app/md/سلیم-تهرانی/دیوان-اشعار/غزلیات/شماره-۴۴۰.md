---
title: >-
    شمارهٔ ۴۴۰
---
# شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>عنان شکوه را در بزم او دست ادب پیچد</p></div>
<div class="m2"><p>ز خاموشی زبانم پای در دامان لب پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درازی سر افسانه ی کلکم همان باقی ست</p></div>
<div class="m2"><p>سخن را گرچه برهم، همچو دستار عرب پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی دانم که کار [دل] کجا خواهد رسید آخر</p></div>
<div class="m2"><p>به خودتاکی [چنین] چون زلف خوبان روز و شب پیچد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن رنجورم از بیم فراموشی دم مردن</p></div>
<div class="m2"><p>ز هر رگ رشته ای چون شمع بر انگشت تب پیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب دارم که روی لطف از آیینه هم بیند</p></div>
<div class="m2"><p>ز هرکس آن نگار تندخو روی غضب پیچد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آزادی سلیم از خود برآور نام چون عنقا</p></div>
<div class="m2"><p>چه پرواز آید از مرغی که در دام نسب پیچد</p></div></div>