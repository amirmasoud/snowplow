---
title: >-
    شمارهٔ ۴۰۲
---
# شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>مرا ز بزم خود آن پر عتاب می‌راند</p></div>
<div class="m2"><p>چو سایه کز بر خود آفتاب می‌راند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان به راه تو صید فریب گشته دلم</p></div>
<div class="m2"><p>که هر نسیم چو موجم به آب می‌راند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه دشمنی‌ست ندانم که باز گردش چرخ</p></div>
<div class="m2"><p>مرا ز کوی تو چون آفتاب می‌راند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مریض عشقم و دایم اجل به بالینم</p></div>
<div class="m2"><p>نشسته و مگس از من به خواب می‌راند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم توبه‌شکن شد دگر هوای بهار</p></div>
<div class="m2"><p>نسیم کشتی ما در شراب می‌راند</p></div></div>