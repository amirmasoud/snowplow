---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>گل از هوای تو در رنگ و بو نمی‌گنجد</p></div>
<div class="m2"><p>ز شوق لعل تو می در سبو نمی‌گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مو ضعیف شدم در هوای صحبت تو</p></div>
<div class="m2"><p>ولی میان تو و غیر، مو نمی‌گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آشنایی دل‌ها چه باعثی باید</p></div>
<div class="m2"><p>که در میان دو آیینه رو نمی‌گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فزون ز طاقت منصور بود مستی عشق</p></div>
<div class="m2"><p>شکوه سیل بهاری به جو نمی‌گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم زحمت بیهوده می‌کشند احباب</p></div>
<div class="m2"><p>به زخم سینهٔ تنگم رفو نمی‌گنجد</p></div></div>