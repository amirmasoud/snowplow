---
title: >-
    شمارهٔ ۸۶۹
---
# شمارهٔ ۸۶۹

<div class="b" id="bn1"><div class="m1"><p>زهی ز نرگس تو آهوی ختن مجنون</p></div>
<div class="m2"><p>ز شوق سرو قدت بید در چمن مجنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نامه ام نتوان یافت جز پریشانی</p></div>
<div class="m2"><p>قلم ز شوق تو شوریده و سخن مجنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر حدیث صد آشفتگی نهان داریم</p></div>
<div class="m2"><p>که هست لیلی ما را به پیرهن مجنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به او ز دور همه عمر عشق می بازیم</p></div>
<div class="m2"><p>سیاه خانه ی لیلی ست هند و من مجنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم، مانع هندوستانم افلاس است</p></div>
<div class="m2"><p>که هست بسته ی زنجیر در وطن مجنون</p></div></div>