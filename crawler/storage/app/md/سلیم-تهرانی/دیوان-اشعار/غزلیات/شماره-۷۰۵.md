---
title: >-
    شمارهٔ ۷۰۵
---
# شمارهٔ ۷۰۵

<div class="b" id="bn1"><div class="m1"><p>از روی دل ز عاجزی خود خجل مباش</p></div>
<div class="m2"><p>پامال خصم خویش چو خون بحل مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون غبار، گاه برانگیز خویش را</p></div>
<div class="m2"><p>افتاده زیر پا چو زمین متصل مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامان روزگار فراخ است، می بنوش</p></div>
<div class="m2"><p>شاید گشایشی رسدت، تنگدل مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز مباش در پی لغزیدن کسی</p></div>
<div class="m2"><p>تا خاک راه خلق توان بود، گل مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحمی به مور خسته کن ای پادشاه حسن</p></div>
<div class="m2"><p>جم باش و همچو خاتم جم سنگدل مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شکوه ای که کردی ازان بی وفا سلیم</p></div>
<div class="m2"><p>او منفعل نگشت، تو هم منفعل مباش</p></div></div>