---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>کرشمهٔ تو اگر دست از شراب کشد</p></div>
<div class="m2"><p>ز باده، دست و دهن را سبو به آب کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چون پیاده روی، شاخ گل عنان گیرد</p></div>
<div class="m2"><p>وگر سوار شوی، ماه نو رکاب کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بزم حسن، رخ او کنایهٔ خوبی</p></div>
<div class="m2"><p>به ماه گوید و بر گوش آفتاب کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آن حریف که همچون حباب می دایم</p></div>
<div class="m2"><p>به باده دامن آلوده را به آب کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم رو به خرابات کن ز راه حرم</p></div>
<div class="m2"><p>چه لازم است کسی این همه عذاب کشد</p></div></div>