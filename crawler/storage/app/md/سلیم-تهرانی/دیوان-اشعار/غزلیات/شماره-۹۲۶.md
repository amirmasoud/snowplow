---
title: >-
    شمارهٔ ۹۲۶
---
# شمارهٔ ۹۲۶

<div class="b" id="bn1"><div class="m1"><p>در دعوی عشق تو نه آهی، نه فغانی</p></div>
<div class="m2"><p>چون تیغ درین معرکه ماییم و زبانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می آید و دارد ز پی جان اسیران</p></div>
<div class="m2"><p>چین در خم ابرو، چو خدنگی به کمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاموش ازان گشته ام از شکوه که دارم</p></div>
<div class="m2"><p>همچون گره ابروی او قفل زبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودای گلم نیست، همان به که ازین باغ</p></div>
<div class="m2"><p>چون شمع زنم بر سر خود برگ خزانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بادیه گیرد خبر از مجمع طفلان</p></div>
<div class="m2"><p>دیوانه ز هر قافله ی ریگ روانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر خود چه سلیم از پی هیچ این همه پیچم</p></div>
<div class="m2"><p>مویی شدم از آرزوی موی میانی</p></div></div>