---
title: >-
    شمارهٔ ۹۴۸
---
# شمارهٔ ۹۴۸

<div class="b" id="bn1"><div class="m1"><p>آب آتش تلاش یعنی می</p></div>
<div class="m2"><p>شعلهٔ خوش‌قماش یعنی می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن را جلوهٔ ظهور دهد</p></div>
<div class="m2"><p>آزر بت‌تراش یعنی می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو لاله به آبرو دایم</p></div>
<div class="m2"><p>می‌کنم من معاش یعنی می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل هردم به گوش من گوید</p></div>
<div class="m2"><p>می‌خور و شاد باش یعنی می</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشکفد طبع بی‌شراب، سلیم</p></div>
<div class="m2"><p>مایهٔ انتعاش یعنی می</p></div></div>