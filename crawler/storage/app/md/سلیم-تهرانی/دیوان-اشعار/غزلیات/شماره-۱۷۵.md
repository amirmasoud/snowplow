---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>کدام دل که ز تاب حسد گداخته نیست</p></div>
<div class="m2"><p>که عندلیب بجز در شکست فاخته نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوای تازه ز مرغان این چمن مطلب</p></div>
<div class="m2"><p>که هیچ نغمه درین پرده نانواخته نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام جام و صراحی، چه شیشه و ساغر</p></div>
<div class="m2"><p>همین کدوست درین انجمن که ساخته نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفای دل همه از فیض آتش عشق است</p></div>
<div class="m2"><p>که موم صاف نباشد اگر گداخته نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جدا ز ابروی او در نظر سلیم مرا</p></div>
<div class="m2"><p>هلال عید، کم از تیغ برفراخته نیست</p></div></div>