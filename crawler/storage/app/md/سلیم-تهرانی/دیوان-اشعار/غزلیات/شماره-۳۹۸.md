---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>بی‌لب او باده بر طبع ایاغم می‌خورد</p></div>
<div class="m2"><p>نکهت گل بی‌رخ او بر دماغم می‌خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طریق عشقبازی هرکجا پروانه‌ای‌ست</p></div>
<div class="m2"><p>سرمهٔ خاموشی از دود چراغم می‌خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست می خواهد که گل بر بار باشد صبح و شام</p></div>
<div class="m2"><p>لاله خون از دست گلچینان باغم می‌خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جور بخت تیره را از من درین وادی مپرس</p></div>
<div class="m2"><p>در زمان زندگی دیدم که زاغم می‌خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشمنی دارد مداوا با جراحت‌های من</p></div>
<div class="m2"><p>گر به دست پنبه افتد، خون داغم می‌خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا قیامت روی هشیاری نمی‌بیند سلیم</p></div>
<div class="m2"><p>هرکه چون منصور، رشحی از ایاغم می‌خورد</p></div></div>