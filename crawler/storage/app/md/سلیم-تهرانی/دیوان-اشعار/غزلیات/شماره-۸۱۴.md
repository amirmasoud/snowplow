---
title: >-
    شمارهٔ ۸۱۴
---
# شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>ما همچو گل به چاک گریبان نشسته‌ایم</p></div>
<div class="m2"><p>چون لاله در لباس شهیدان نشسته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر یک به فکر طرّه‌ای آشفته‌خاطریم</p></div>
<div class="m2"><p>جمعیم دوستان و پریشان نشسته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاران و بوی گل همه در سِیْرِ گلشنند</p></div>
<div class="m2"><p>ما همچو غنچه پای به دامان نشسته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز نان خشک، قسمت ما نیست از جهان</p></div>
<div class="m2"><p>گویی مگر به سفرهٔ دهقان نشسته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنبیدن است باعث آزار ما سلیم</p></div>
<div class="m2"><p>گویی چو تیر بر سر پیکان نشسته‌ایم</p></div></div>