---
title: >-
    شمارهٔ ۴۷۱
---
# شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>دلم در باغ نتوانست امشب خواب راحت کرد</p></div>
<div class="m2"><p>سحر را شورش مرغان به من صبح قیامت کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی بر باد دادم در ره مهر و وفای او</p></div>
<div class="m2"><p>که خورشید از غبارش خانه ی خود را عمارت کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای کشته گردیدن به تیغ آفتاب خود</p></div>
<div class="m2"><p>سراپای مرا چون شمع، انگشت شهادت کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا از چین زلف او مگر سوی چمن آمد</p></div>
<div class="m2"><p>که بوی مشک، زخم لاله و گل را جراحت کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیم آزادی هرکس به محشر باعثی دارد</p></div>
<div class="m2"><p>گناه می‌کشان را ساقی کوثر شفاعت کرد</p></div></div>