---
title: >-
    شمارهٔ ۴۵۴
---
# شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>عاشق پرشِکوه خاموش از تغافل می‌شود</p></div>
<div class="m2"><p>طوطی از آیینه چون رو دید، بلبل می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ از زخم خس و خاریم کز فیض چمن</p></div>
<div class="m2"><p>دامنت ما خود به خود چون غنچه پر گل می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست و پایی زن، که نبود در شمار زندگان</p></div>
<div class="m2"><p>هرکه چون من نقش دیوار توکل می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاصل سرمایهٔ خاشاک معلوم است چیست</p></div>
<div class="m2"><p>در گلستانی که سودا با زر گل می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان دارند غوغا در شهادتگاه عشق</p></div>
<div class="m2"><p>فتنه‌ها در خیل شاهان بر سر پل می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد مردن، از پریشانی به خاک من سلیم</p></div>
<div class="m2"><p>تخم هر گل را که افشانند، سنبل می‌شود</p></div></div>