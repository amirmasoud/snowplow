---
title: >-
    شمارهٔ ۵۳۷
---
# شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>بعد ازین از باغ در گلخن وطن خواهیم کرد</p></div>
<div class="m2"><p>ترک همچشمی مرغان چمن خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما اسیران محبت وارث یکدیگریم</p></div>
<div class="m2"><p>با فلک دعوی خون کوهکن خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این زمان خود مصلحت را در خموشی دیده ایم</p></div>
<div class="m2"><p>کار هرگه با سخن افتد، سخن خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خارخار ناامیدی چند روزی مشکل است</p></div>
<div class="m2"><p>همچو ماهی، خو به خار پیرهن خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما حریف این همه هنگامه سازی نیستیم</p></div>
<div class="m2"><p>مژده یاران را که ترک انجمن خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کشد از هند، خاطر جانب ایران سلیم</p></div>
<div class="m2"><p>همچو عنقا چند در غربت وطن خواهیم کرد؟</p></div></div>