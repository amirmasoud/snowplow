---
title: >-
    شمارهٔ ۵۹۶
---
# شمارهٔ ۵۹۶

<div class="b" id="bn1"><div class="m1"><p>در سر کوی تو جمعند پریشانی چند</p></div>
<div class="m2"><p>بند بر بند قبا بافته عریانی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دیوانه ی ما زلف ترا در کار است</p></div>
<div class="m2"><p>باید این سلسله را سلسله جنبانی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتی ما نه چنان هم به کنار آمده است</p></div>
<div class="m2"><p>که توان قطع نظر کرد ز طوفانی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دکانی که گشوده ست جنون، می پرسد</p></div>
<div class="m2"><p>گل چو خمیازه کشان چاک گریبانی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش نگردد دلم از گریه، که خرم نکند</p></div>
<div class="m2"><p>خرمن سوخته را قطره ی بارانی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک نگین وار زمین است و هزاران جمشید</p></div>
<div class="m2"><p>مانده از این ده ویران شده دهقانی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده مرهم طلب ای همنفسان زخم سلیم</p></div>
<div class="m2"><p>نیست گر معدن الماس، نمکدانی چند</p></div></div>