---
title: >-
    شمارهٔ ۸۶۲
---
# شمارهٔ ۸۶۲

<div class="b" id="bn1"><div class="m1"><p>چون توان با غیر دل را خالی از کین ساختن</p></div>
<div class="m2"><p>هرگز از بلبل نمی آید به گلچین ساختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل باشد حسن هرجا دعوی اعجاز کرد</p></div>
<div class="m2"><p>سرمه را در چشم خوبان خواب سنگین ساختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نمی دانستم ای مهمان تو آتش بوده ای</p></div>
<div class="m2"><p>کاسه را هم ورنه می بایست چوبین ساختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت تلخ تو چون دربان آن باشد، چه سود</p></div>
<div class="m2"><p>همچو زنبور از عمارت های شیرین ساختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خموشان چمن لایق نمی باشد سلیم</p></div>
<div class="m2"><p>ورنه از گل می توان صد حرف رنگین ساختن</p></div></div>