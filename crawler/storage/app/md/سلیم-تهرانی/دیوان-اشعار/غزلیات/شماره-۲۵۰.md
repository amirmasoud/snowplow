---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>حاصل سوختگان در ره برق خطر است</p></div>
<div class="m2"><p>لشکری آفتش از مور و ملخ بیشتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذران نوبت ما ساقی اگر شیفته ایم</p></div>
<div class="m2"><p>نیست این بیخودی از باده، ز جای دگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغ بر پیکرم افزون بود از جوهر تیغ</p></div>
<div class="m2"><p>زخم بر سینه ی من بیش ز موج سپر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن اظهار پریشانی خود پیش کسی</p></div>
<div class="m2"><p>بخیه پیدا چو نباشد ز رفوگر هنر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر در کعبه سراغ حرم دل کردم</p></div>
<div class="m2"><p>از درون گفت کسی خانه ی دل پیشتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگذر از مستی اگر دور جوانی بگذشت</p></div>
<div class="m2"><p>پیری و باده ی گلرنگ چو شیر و شکر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کنم خدمت او را به سر و چشم، سلیم</p></div>
<div class="m2"><p>که مرا پیر خرابات به جای پدر است</p></div></div>