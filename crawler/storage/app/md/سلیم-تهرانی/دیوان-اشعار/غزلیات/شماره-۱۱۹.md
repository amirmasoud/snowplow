---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>آتش گل را اگر باشد شراری، خال اوست</p></div>
<div class="m2"><p>گر چراغ آیینه ای دارد، گل تمثال اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو را این جلوه و سامان رعنایی کجاست</p></div>
<div class="m2"><p>طوق قمری کی به حسن نغمه چون خلخال اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستر راحت مرا پهلوست در راه طلب</p></div>
<div class="m2"><p>مرغ را چون خواب آید، بالش پر، بال اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست پاکان را بجز در کار دنیا اعتبار</p></div>
<div class="m2"><p>خلق را تعظیم مصحف از برای فال اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاجزیم، اما نه چندان کز کسی عاجز شویم</p></div>
<div class="m2"><p>هر که از ما می تواند صرفه بردن، مال اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی ما شد بیابان مرگ همچون گردباد</p></div>
<div class="m2"><p>موج این دریا همان از کینه در دنبال اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون توانم دیگری دیدن طلبکارش سلیم؟</p></div>
<div class="m2"><p>رشک دارم من به چشم خود که در دنبال اوست</p></div></div>