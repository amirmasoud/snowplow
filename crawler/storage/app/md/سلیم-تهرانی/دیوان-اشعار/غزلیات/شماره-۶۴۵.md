---
title: >-
    شمارهٔ ۶۴۵
---
# شمارهٔ ۶۴۵

<div class="b" id="bn1"><div class="m1"><p>بهار رفت و دل از ابر کامیاب نشد</p></div>
<div class="m2"><p>پیاله ای نگرفتم که آفتاب نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلاک همت مرغی شوم درین گلشن</p></div>
<div class="m2"><p>که جز به شعله ی آواز خود کباب نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه روز بود که دوران اساس عشق نهاد</p></div>
<div class="m2"><p>جهان خراب شد و این بنا خراب نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیثی از لب لعلش در انجمن نگذشت</p></div>
<div class="m2"><p>که جام و شیشه ی خالی پر از شراب نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس ز نسبت ما روزگار را ننگ است</p></div>
<div class="m2"><p>بر آتشی ننهادیم دل که آب نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم به یاد لبت ناله ای به باغ نکرد</p></div>
<div class="m2"><p>که غنچه چون دل مرغ چمن کباب نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلال ازان دل خود ای سوار مست خورد</p></div>
<div class="m2"><p>که هیچ وقت ترا حاجت رکاب نشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نداشت در غم عشق تو گریه فایده ای</p></div>
<div class="m2"><p>علاج دردسر ما ازین گلاب نشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلیم این غزل طرحی از کجا آمد</p></div>
<div class="m2"><p>که مصرعی ز دوصد بیت انتخاب نشد (؟)</p></div></div>