---
title: >-
    شمارهٔ ۹۴۰
---
# شمارهٔ ۹۴۰

<div class="b" id="bn1"><div class="m1"><p>آهم به درون دل ز تنگی</p></div>
<div class="m2"><p>پیچیده به هم، چو موی زنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمن نشوی، که اندرین بحر</p></div>
<div class="m2"><p>هر موج زند دم از نهنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد حربه در آستین عشق است</p></div>
<div class="m2"><p>همچون خرطوم فیل جنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوفان نکند به کشتی می</p></div>
<div class="m2"><p>تأثیر چو کشتی فرنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیچاره سلیم در زمانه</p></div>
<div class="m2"><p>شمع روز است و خال زنگی</p></div></div>