---
title: >-
    شمارهٔ ۸۴۹
---
# شمارهٔ ۸۴۹

<div class="b" id="bn1"><div class="m1"><p>دهد به ذره چو خورشید آب و تاب، سخن</p></div>
<div class="m2"><p>مباد آن که کسی را کند خراب، سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلسم غم که شکستی؟ اگر ز حلقهٔ گوش</p></div>
<div class="m2"><p>نمی‌گذاشت سر پای در رکاب سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا دماغ حدیث بهشت و دوزخ نیست</p></div>
<div class="m2"><p>گه از شراب کنم، گاهی از کباب سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بریده نگوید سخن، چه عقل است این</p></div>
<div class="m2"><p>ز راز چرخ مپرسید از آفتاب سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر زبان خموشان این چمن دانی</p></div>
<div class="m2"><p>بود تبسم هر غنچه یک کتاب سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث لعل تو کوته نمی شود، چه عجب</p></div>
<div class="m2"><p>اگر دراز شود در سر شراب سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز وصف آن گره زلف بر زبان سلیم</p></div>
<div class="m2"><p>چو حرف لال درآید به پیچ و تاب سخن</p></div></div>