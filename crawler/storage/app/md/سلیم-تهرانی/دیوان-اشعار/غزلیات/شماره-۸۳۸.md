---
title: >-
    شمارهٔ ۸۳۸
---
# شمارهٔ ۸۳۸

<div class="b" id="bn1"><div class="m1"><p>در ورطهٔ کشاکشِ دوران فتاده‌ایم</p></div>
<div class="m2"><p>این بحر را چو موج، کمان کباده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جوش این میحط کسی را چه اختیار</p></div>
<div class="m2"><p>چون موج، ما عنان خود از دست داده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتند دوستان چو گل و لاله زین چمن</p></div>
<div class="m2"><p>چون سرو از برای چه ما ایستاده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در راستی چو شمع نداریم پشت و روی</p></div>
<div class="m2"><p>با اهل روزگار چو دست گشاده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در راه او به عمر نداریم احتیاج</p></div>
<div class="m2"><p>شاطر چه می‌کنیم که ما خود پیاده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما از کجا سلیم و غم عشق از کجا</p></div>
<div class="m2"><p>دل را به دست خویش بر آتش نهاده‌ایم</p></div></div>