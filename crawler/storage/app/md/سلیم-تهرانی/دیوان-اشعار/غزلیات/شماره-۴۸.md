---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>نتوان گفت به رویش سخن آینه را</p></div>
<div class="m2"><p>نسبتی با تن او نیست تن آینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق رویش همه کس را به غریبی دارد</p></div>
<div class="m2"><p>سبب این است جلای وطن آینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ کس نیست که در وصل تو نقشش ننشست</p></div>
<div class="m2"><p>خوب دارد گل روی تو فن آینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرب نرمی ست در اصلاح دلم کار غمت</p></div>
<div class="m2"><p>موم روغن شده این سنگ، تن آینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رونق انجمن از صحبت اهل سخن است</p></div>
<div class="m2"><p>سبز دارد پر طوطی، چمن آینه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن چون پرده گشاید پی تاراج، سلیم</p></div>
<div class="m2"><p>به در آرد ز بدن پیرهن آینه را</p></div></div>