---
title: >-
    شمارهٔ ۶۶۳
---
# شمارهٔ ۶۶۳

<div class="b" id="bn1"><div class="m1"><p>پر شکوه مکن، خاطر آن ماه نگه دار</p></div>
<div class="m2"><p>آیینه به دست است ترا آه نگه دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمنده شو ای دل، گله تا کی کنی از دوست</p></div>
<div class="m2"><p>گر رشته دراز است، تو کوتاه نگه دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صرف ره او کن همه اسباب جهان را</p></div>
<div class="m2"><p>بنشین به دل جمع و سر راه نگه دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی داغ برون رفتن ازین باغ شگون نیست</p></div>
<div class="m2"><p>از لاله بچین برگی و همراه نگه دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتند چو خورشید به افلاک حریفان</p></div>
<div class="m2"><p>چون سایه تو خود را همه در چاه نگه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در لقمه ی درویش بود لذت دیگر</p></div>
<div class="m2"><p>یک کاسه ی چوبین تو هم ای شاه نگه دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خانه هم از گام زدن گرم طلب باش</p></div>
<div class="m2"><p>سر رشته ی این کار چو جولاه نگه دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل را به غم عشق مده مفت سلیما</p></div>
<div class="m2"><p>داغی تو هم ای سوخته تنخواه نگه دار</p></div></div>