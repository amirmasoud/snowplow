---
title: >-
    شمارهٔ ۹۱۱
---
# شمارهٔ ۹۱۱

<div class="b" id="bn1"><div class="m1"><p>ای بت، متاع اهل ریا را چه می کنی</p></div>
<div class="m2"><p>آیینه هست، قبله نما را چه می کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی، سپاه زهد و ورع را چو بشکنی</p></div>
<div class="m2"><p>اول بگو که توبه ی ما را چه می کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست ستاره از مدد رزق کوته است</p></div>
<div class="m2"><p>در جذب دانه اهربا را چه می کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رقص چون سپند نخواهی دمی نشست</p></div>
<div class="m2"><p>چون آمدی به میکده، جا را چه می کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر پر ز بال جغد بود گنجنامه ای</p></div>
<div class="m2"><p>ای خانمان خراب، هما را چه می کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عاشقی سلیم، تلاش وفا مکن</p></div>
<div class="m2"><p>داری هزار عیب، وفا را چه می کنی</p></div></div>