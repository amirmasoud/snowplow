---
title: >-
    شمارهٔ ۴۶۹
---
# شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>چشم توام ز هوش تهیدست می کند</p></div>
<div class="m2"><p>یک سرمه دان شراب، مرا مست می کند!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منعم مکن که گریه ی مستانه را دلم</p></div>
<div class="m2"><p>چنان که می به جام و سبو هست، می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرام، سازگار اسیران عشق نیست</p></div>
<div class="m2"><p>بلبل فغان به شاخ چو بنشست، می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد شد ز خانه ی همسایگان بلند</p></div>
<div class="m2"><p>مطرب ز بس که زمزمه را پست می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن باغبان که همت خود را بلند کرد</p></div>
<div class="m2"><p>دیوار اگر کند به چمن، پست می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز دلم ز آبله لشکر کشیده است</p></div>
<div class="m2"><p>هرجا که رو نهد چو کف دست می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کرده اند نسبت او را به گل سلیم</p></div>
<div class="m2"><p>بوی گلم چو مرغ چمن مست می کند</p></div></div>