---
title: >-
    شمارهٔ ۶۱۸
---
# شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>هم‌نشین از گریهٔ من کاشکی دامان کشد</p></div>
<div class="m2"><p>خویش را بر گوشه‌ای چون موج ازین طوفان کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سموم آه، این ویرانه از بس گرم شد</p></div>
<div class="m2"><p>اشک خود را از دلم در سایهٔ مژگان کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر اگر بر من گران باشد، ز سر وا می‌کنم</p></div>
<div class="m2"><p>درد دندان هرکه نتواند کشد، دندان کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی ز حسن سبز در ایران توان شد کامیاب</p></div>
<div class="m2"><p>هرکه را طاووس باید، رنج هِندُستان کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکل است از هم جدایی هم‌نشینان را سلیم</p></div>
<div class="m2"><p>ز استخوان من به آن ترکش مگر پیکان کشد</p></div></div>