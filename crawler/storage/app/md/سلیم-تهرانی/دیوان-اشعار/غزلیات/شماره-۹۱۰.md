---
title: >-
    شمارهٔ ۹۱۰
---
# شمارهٔ ۹۱۰

<div class="b" id="bn1"><div class="m1"><p>چون نگه دارد مرا زنجیر زلف سنبلی؟</p></div>
<div class="m2"><p>بلبل دیوانه ام، می بایدم چوب گلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره عشق ای دل از سحر و فسون ایمن مباش</p></div>
<div class="m2"><p>خانه ی هر مور این صحراست چاه بابلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر شبنم را چه می پرسی درین گلشن که هست</p></div>
<div class="m2"><p>چتر هر طاووس، نخل ماتم شاخ گلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلستانی که دارد حسن سبزان اعتبار</p></div>
<div class="m2"><p>سایه ی هر برگ باشد آشیان بلبلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش نباشد ساعد کافوری شاخ سمن</p></div>
<div class="m2"><p>می رسد تا دست بر ساق سیاه سنبلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تواند از سر آن بگذرد گردون سلیم</p></div>
<div class="m2"><p>کاشکی می‌داشت آب روی محرومان پلی</p></div></div>