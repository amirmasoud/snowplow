---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>دل بی لب او خراب خفته ست</p></div>
<div class="m2"><p>مستی ز غم شراب خفته ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاک، دلم به یاد تیغش</p></div>
<div class="m2"><p>چون تشنه به یاد آب خفته ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف تو همیشه از نزاکت</p></div>
<div class="m2"><p>در دامن پیچ و تاب خفته ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم سیه تو چون غریبان</p></div>
<div class="m2"><p>بیمار در آفتاب خفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گریه سلیم بی تو امشب</p></div>
<div class="m2"><p>چون موج به روی آب خفته ست</p></div></div>