---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>‎تویی که تیغ چو آب تو کشته آتش را</p></div>
<div class="m2"><p>به زهر چشم، عتاب تو کشته آتش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز باد، آتش اگرچه همیشه زنده شود</p></div>
<div class="m2"><p>نسیم طرف نقاب تو کشته آتش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خون گرم گر آلوده است نیست عجب</p></div>
<div class="m2"><p>به تیغ موجب شراب تو کشته آتش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سوختن بردم عشق، خوش تماشایی ست</p></div>
<div class="m2"><p>به جرم این که کباب تو کشته آتش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجال لاف زبان آوری به شعله نداد</p></div>
<div class="m2"><p>سلیم، طبع چو آب تو کشته آتش را</p></div></div>