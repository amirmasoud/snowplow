---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>کی توان در عشق، بی سامان حیرانی نشست؟</p></div>
<div class="m2"><p>آنچنان در گوشه ای بنشین که بتوانی نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک خلعت خانه ی عالم اگر بر باد رفت</p></div>
<div class="m2"><p>گرد کی بر گوشه ی دامان عریانی نشست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاروان عیش را زین خاکدان ره بسته شد</p></div>
<div class="m2"><p>چند همچون رهزنان بتوان به ویرانی نشست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قفس پهلو تهی گر می کند دل، دور نیست</p></div>
<div class="m2"><p>چند روزی همره مرغان بستانی نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبلی چون من برون آمد ز گلزار عراق</p></div>
<div class="m2"><p>شور و غوغای حریفان خراسانی نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تماشای رخش شد دیده را حاجت روا</p></div>
<div class="m2"><p>در سجود آستانش نقش پیشانی نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه سرو و گل ترا گوید، گهی خورشید و ماه</p></div>
<div class="m2"><p>عقل چون دفتر گشود و در غلط خوانی نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق چند اوقات صرف صحبت عاشق کند؟</p></div>
<div class="m2"><p>پاسبان دلگیر شد از بس به زندانی نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هجوم مرغ دل ها نیست ره در کوی عشق</p></div>
<div class="m2"><p>آخر این صیاد بر تخت سلیمانی نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از گلستان بس که بلبل داشت بر خاطر غبار</p></div>
<div class="m2"><p>در قفس بر خاک از بال و پر افشانی نشست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد بهار و رفت هرکس بر سر کاری سلیم</p></div>
<div class="m2"><p>محتسب هم در پی کاری که می‌دانی نشست</p></div></div>