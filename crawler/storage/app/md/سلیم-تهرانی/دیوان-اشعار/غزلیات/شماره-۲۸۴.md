---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>دماغ ساغر می از شراب ما خشک است</p></div>
<div class="m2"><p>چونان خانه ی درویش، آب ما خشک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر به سنگ تواند نسیم بشکندش</p></div>
<div class="m2"><p>ز بس چو شیشه ی خالی حباب ما خشک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تشنگی چمن ما به کربلا ماند</p></div>
<div class="m2"><p>که همچو دست لئیمان سحاب ما خشک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم که سوخته، او را قبول کی گردد</p></div>
<div class="m2"><p>مزاج مست لطیف و کباب ما خشک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر شکفته نگردد سلیم معذور است</p></div>
<div class="m2"><p>دماغ غنچه ی دل ز آفتاب ما خشک است</p></div></div>