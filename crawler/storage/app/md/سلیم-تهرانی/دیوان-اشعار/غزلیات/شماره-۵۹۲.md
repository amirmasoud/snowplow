---
title: >-
    شمارهٔ ۵۹۲
---
# شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>رهنمای مقصد هرکس توکل می‌شود</p></div>
<div class="m2"><p>همچو ابراهیمش آتش لاله و گل می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز قید ناخدا آزادسازی خویش را</p></div>
<div class="m2"><p>موج دریا همچو مرغابی ترا پل می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفله را کی می‌توان از لاف دولت منع کرد</p></div>
<div class="m2"><p>باغبان چون در چمن گل دید، بلبل می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت مستان تمام عمر در پای گل است</p></div>
<div class="m2"><p>بلبلان را گرچه صحبت بر سر گل می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سایهٔ گل، طشت آتش بر سرم ریزد سلیم</p></div>
<div class="m2"><p>لطف او بر من ز بخت بد، تغافل می‌شود</p></div></div>