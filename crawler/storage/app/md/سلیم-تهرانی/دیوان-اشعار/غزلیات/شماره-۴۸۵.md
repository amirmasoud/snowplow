---
title: >-
    شمارهٔ ۴۸۵
---
# شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>نگاه از شوق دیدارت به چشم من نمی‌گنجد</p></div>
<div class="m2"><p>چراغی کز تو روشن شد در او روغن نمی‌گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای دامن صحرا چنانم مضطرب دارد</p></div>
<div class="m2"><p>که همچون گردبادم پای در دامن نمی‌گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفر کردن به سوی دوستان ذوق دگر دارد</p></div>
<div class="m2"><p>نسیم مصر از شادی به پیراهن نمی‌گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی دارم من دیوانه از ذوق تماشایت</p></div>
<div class="m2"><p>که چون آیینهٔ خورشید در گلخن نمی‌گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون غنچه می‌گنجید بوی گل، ولی اکنون</p></div>
<div class="m2"><p>ز بس رسوا شد از شوق تو، در گلشن نمی‌گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم از بخیه زخم من ندارد قسمتی، آری</p></div>
<div class="m2"><p>دلم از بس پر است از غم، در او سوزن نمی‌گنجد</p></div></div>