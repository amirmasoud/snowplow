---
title: >-
    شمارهٔ ۵۰۳
---
# شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>یاد ایامی که بلبل چون من محزون نبود</p></div>
<div class="m2"><p>همچو قمری، گفتگوی من به یک مضمون نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شراب کهنه شد آخر علاج درد ما</p></div>
<div class="m2"><p>حکمت خم گر نمی شد، کار افلاطون نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهدان باغ را معشوق ما رونق شکست</p></div>
<div class="m2"><p>سرو را دیدیم، همچون قد او موزون نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آبله دارد ز نقش پای من روی زمین</p></div>
<div class="m2"><p>این قدر آوارگی در طالع مجنون نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد اگر جامی به دستم ساقی دوران سلیم</p></div>
<div class="m2"><p>چون گرفتم جام را بر لب، درو جز خون نبود</p></div></div>