---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>چون چشم حسود است جهان برمن شور</p></div>
<div class="m2"><p>آواره ی عالمم چو بیت مشهور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبحم همه شام است، ولی شام فراق</p></div>
<div class="m2"><p>روزم همه شب، ولی شب اول گور</p></div></div>