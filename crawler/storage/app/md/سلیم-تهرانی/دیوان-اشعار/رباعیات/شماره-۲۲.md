---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>دیگر ز چمن خرمی اخراج شده ست</p></div>
<div class="m2"><p>سامان گل از خزان به تاراج شده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه به بخت خویشتن می نازد</p></div>
<div class="m2"><p>بلبل به گل چراغ محتاج شده ست</p></div></div>