---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>تشویش سفر با دل ناشاد بد است</p></div>
<div class="m2"><p>با دست تهی چو کار افتاد بد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راضی شده ام به قرض هم گر باشد</p></div>
<div class="m2"><p>می دانم اگرچه قرض بغداد بد است</p></div></div>