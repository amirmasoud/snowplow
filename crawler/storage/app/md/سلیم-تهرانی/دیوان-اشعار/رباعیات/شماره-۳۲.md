---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>نوروز شد و زد به گلستان ز فرح</p></div>
<div class="m2"><p>طاووس بهار، چتر از قوس قزح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم ز جوش گل ز بس جای نماند</p></div>
<div class="m2"><p>استاده چو لاله بر سر پای، قدح</p></div></div>