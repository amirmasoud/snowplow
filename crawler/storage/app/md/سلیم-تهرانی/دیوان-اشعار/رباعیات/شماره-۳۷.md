---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>عید تو به سامان ز طرب سازی باد</p></div>
<div class="m2"><p>انجام نشاط تو در آغازی باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بال همای عید باشد مه نو</p></div>
<div class="m2"><p>اقبال تو در بلندپروازی باد</p></div></div>