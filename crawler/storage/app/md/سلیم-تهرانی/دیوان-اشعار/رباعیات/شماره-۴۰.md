---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>کو ساقی، تا ره مروت سپرد</p></div>
<div class="m2"><p>یک جام می آرد، غم ما را ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد چه مذاق خیره ای داشته است</p></div>
<div class="m2"><p>نزدیک شده دختر رز را بخورد</p></div></div>