---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>ای خواجه ترا ضعیفی از پیری نیست</p></div>
<div class="m2"><p>از گرسنگی ست سستی، از سیری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسبی که کشند بر ورق نقاشان</p></div>
<div class="m2"><p>گر بی حرکت بود ز جوگیری نیست</p></div></div>