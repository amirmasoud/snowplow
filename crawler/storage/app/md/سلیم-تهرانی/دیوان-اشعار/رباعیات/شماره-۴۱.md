---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>ای آن که مسیح آنچه مجمل دارد</p></div>
<div class="m2"><p>از علم آن را دلت مفصل دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راضی شده ام باز به ضعف دل خویش</p></div>
<div class="m2"><p>زین دردسری که قرص صندل دارد</p></div></div>