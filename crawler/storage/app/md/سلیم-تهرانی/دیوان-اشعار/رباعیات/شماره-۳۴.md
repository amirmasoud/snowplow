---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای غمزه ی تو نهاده رسم بیداد</p></div>
<div class="m2"><p>مژگان تو خونریز چو تیغ جلاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از داغ جدایی تو هفت اعضایم</p></div>
<div class="m2"><p>مچون نی هفت بند، دارد فریاد</p></div></div>