---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>از شوق تو خون در دل گل می جوشد</p></div>
<div class="m2"><p>شمع از هوست به سوختن می کوشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عکس گل روی تو دایم چون گل</p></div>
<div class="m2"><p>آیینه لباس چهره ای می پوشد</p></div></div>