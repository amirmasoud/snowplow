---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>افسوس که از شورش این بحر خطیر</p></div>
<div class="m2"><p>عاجز گردید ناخدای تدبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از موج به موج است گذارم، گویی</p></div>
<div class="m2"><p>مورم که رهم فتاده بر روی حصیر</p></div></div>