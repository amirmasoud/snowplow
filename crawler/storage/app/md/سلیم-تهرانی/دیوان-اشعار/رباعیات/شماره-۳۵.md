---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>عید تو همیشه در طرب سازی باد</p></div>
<div class="m2"><p>کار تو چو خورشید سرافرازی باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم تو چرخ از سرانگشت هلال</p></div>
<div class="m2"><p>چون کاسه ی چینی به خوش آوازی باد</p></div></div>