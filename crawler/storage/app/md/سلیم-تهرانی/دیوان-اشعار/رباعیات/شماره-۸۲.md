---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>با هم دو برادر سیه فام ببین</p></div>
<div class="m2"><p>گویی که دوپاره کرده ای یک سرگین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن به قد پست و بلند ایشان</p></div>
<div class="m2"><p>فرق است چو سایه های پیشین و پسین</p></div></div>