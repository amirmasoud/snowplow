---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ای بزم ترا ساغر می مجمره سوز</p></div>
<div class="m2"><p>هر روز ز ایام تو روز نوروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گلشن اقبال تو کان خرم باد</p></div>
<div class="m2"><p>خورشید بود یک گل بستان افروز</p></div></div>