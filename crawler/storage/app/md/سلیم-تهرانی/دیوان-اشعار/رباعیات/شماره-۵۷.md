---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>ای اهل سخن از کرمت شکرگزار</p></div>
<div class="m2"><p>مثقال هنر پیش تمیزت خروار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عهد تو سامان دگر یافت سخن</p></div>
<div class="m2"><p>آری خرجش کم است و دخلش بسیار</p></div></div>