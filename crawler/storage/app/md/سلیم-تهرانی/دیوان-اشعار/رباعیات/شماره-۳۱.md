---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>در بحر نیابد اگر از فیض تو قوت</p></div>
<div class="m2"><p>اورنگ صدف شود گهر را تابوت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زان که نه لطف تو برو آب زند</p></div>
<div class="m2"><p>در آتش رنگ خود بسوزد یاقوت</p></div></div>