---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>مهری به دلم چو نور در باصره ای</p></div>
<div class="m2"><p>شوری به سرم چو دود در مجمره ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم ز ناله بینوایم، ای کاش</p></div>
<div class="m2"><p>مطرب ز برای من کشد دایره ای</p></div></div>