---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>حال دلم از طرز نوایم پیداست</p></div>
<div class="m2"><p>سودا زده ام، ز هر ادایم پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه تو چون خامه ی بی پروایان</p></div>
<div class="m2"><p>آشفتگی ام ز نقش پایم پیداست</p></div></div>