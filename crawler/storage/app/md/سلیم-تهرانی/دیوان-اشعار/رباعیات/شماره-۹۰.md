---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>این پیکر زرین که جهان گشته بسی</p></div>
<div class="m2"><p>آرام چو سیماب ندارد نفسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید مگو، که این سپهر غماز</p></div>
<div class="m2"><p>هر روز ز بام افکند طشت کسی</p></div></div>