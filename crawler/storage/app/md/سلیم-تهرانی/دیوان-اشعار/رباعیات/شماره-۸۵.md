---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>از تندی مرکب نه ز زین افتادی</p></div>
<div class="m2"><p>گویم ز چه ای قبله ی دین افتادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غیب ترا بشارت فتح رسید</p></div>
<div class="m2"><p>در سجده ی شکر بر زمین افتادی</p></div></div>