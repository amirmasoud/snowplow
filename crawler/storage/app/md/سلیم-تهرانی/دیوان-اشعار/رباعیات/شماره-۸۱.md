---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>تا آمدن تو شوق را گشت یقین</p></div>
<div class="m2"><p>بندد ز طرب، خانه ی دل را آیین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را برسان که شوق از اندازه گذشت</p></div>
<div class="m2"><p>جایی مکن آرام بجز خانه ی زین</p></div></div>