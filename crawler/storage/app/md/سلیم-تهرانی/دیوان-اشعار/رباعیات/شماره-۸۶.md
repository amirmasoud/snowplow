---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>هرگز نرسانیده سعادتمندی</p></div>
<div class="m2"><p>از پهلوی خود فیض به حاجتمندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که چون پیاله گردید دلم</p></div>
<div class="m2"><p>غیر از خم می ندید دولتمندی</p></div></div>