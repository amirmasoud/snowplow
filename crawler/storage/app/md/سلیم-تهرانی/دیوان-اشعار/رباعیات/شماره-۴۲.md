---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>در صبر اگرچه دل رسایی دارد</p></div>
<div class="m2"><p>با جور فلک چه آشنایی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سنگ به او رسد، یقین می شکند</p></div>
<div class="m2"><p>هرچند که شیشه مومیایی دارد</p></div></div>