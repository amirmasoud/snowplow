---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>صبح است و نوای بلبلی می آید</p></div>
<div class="m2"><p>زان طره نسیم سنبلی می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون مژه در دیده ی ما جا دارد</p></div>
<div class="m2"><p>خاری که ازو بوی گلی می آید</p></div></div>