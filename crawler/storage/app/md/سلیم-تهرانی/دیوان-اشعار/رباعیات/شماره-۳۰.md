---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>بی جذبه ی دوستان ز جا نتوان رفت</p></div>
<div class="m2"><p>هر راه که نیست رهنما، نتوان رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد مؤذن بشنو تا دانی</p></div>
<div class="m2"><p>ناخوانده به خانه ی خدا نتوان رفت</p></div></div>