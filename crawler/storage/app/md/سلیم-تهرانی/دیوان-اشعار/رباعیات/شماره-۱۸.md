---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>مال دنیا که منعمان را جان است</p></div>
<div class="m2"><p>فرداست که صرف کار محتاجان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند به پشت گاو، خر خواهد بود</p></div>
<div class="m2"><p>آخر گذر پوست به سراجان است</p></div></div>