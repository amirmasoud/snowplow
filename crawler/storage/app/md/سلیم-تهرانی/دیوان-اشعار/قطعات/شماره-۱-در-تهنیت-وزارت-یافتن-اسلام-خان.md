---
title: >-
    شمارهٔ ۱ - در تهنیت وزارت یافتن اسلام خان
---
# شمارهٔ ۱ - در تهنیت وزارت یافتن اسلام خان

<div class="b" id="bn1"><div class="m1"><p>ای سواد هند از کلکت نگارستان چین</p></div>
<div class="m2"><p>کار و بار ملک هرگز این سر و سامان نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه ی اقبال پیش از کلک دولت پرورت</p></div>
<div class="m2"><p>داشت گرچه رونقی، اما به این عنوان نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشک بر شاه جهان آید سکندر را که او</p></div>
<div class="m2"><p>چون تو دستوری خرداندیش و حکمت دان نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خطت فرمان شه شد چون نگارستان چین</p></div>
<div class="m2"><p>این قدر خیل پری، جمشید در فرمان نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاهی آنچنان را این چنین باید وزیر</p></div>
<div class="m2"><p>آنچه می بایست، شد، زین خوبتر امکان نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار دولت شد قوی از کلک محکم کار تو</p></div>
<div class="m2"><p>خوب شد، آری ستونی این بلندایوان نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دواتت عافیت را ساز شد سامان کار</p></div>
<div class="m2"><p>کز برای سینه های ریش، مرهمدان نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون تو دستوری ندارد هفت اقلیم جهان</p></div>
<div class="m2"><p>این گمان هرگز به بخت خویش، هندستان نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با شجاعت جمع در عهد تو شد دانشوری</p></div>
<div class="m2"><p>در زمان هیچ کس تیر قلم پیکان نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مصرع شمشیر از کلک تو شد بیتی تمام</p></div>
<div class="m2"><p>هیچ دیوانی دو مصرع این چنین چسبان نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پشت شمشیر تو از دلگرمی کلکت قوی ست</p></div>
<div class="m2"><p>قطره ی آبی وگرنه این همه طوفان نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرد او را خامه ات از وادی حیرت خلاص</p></div>
<div class="m2"><p>رهنمایی خضر سوی چشمه ی حیوان نداشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نامت از سرچشمه ی خورشید آبش می دهد</p></div>
<div class="m2"><p>آبرویی کاین زمان دارد نگین در کان نداشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد کف دست تو دل ها را مقام عافیت</p></div>
<div class="m2"><p>گوهر این آسودگی در مخزن عمان نداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا صلای عام، دست گوهرافشانت نداد</p></div>
<div class="m2"><p>جامه همچون پوست بر تن خلق را دامان نداشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مهر جودت بر برات رزق اشیا تا نبود</p></div>
<div class="m2"><p>موج دریا بی دهن بود و صدف دندان نداشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صاحبا! عزم سفر میمون و فرخ فال باد</p></div>
<div class="m2"><p>بی تو خیل شاه را فتح و ظفر امکان نداشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرده بیماری مرا نوعی ضعیف و ناتوان</p></div>
<div class="m2"><p>کاین تن رنجور، پنداری که هرگز جان نداشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غربت و بیماری ام پامال حیرت کرده است</p></div>
<div class="m2"><p>هیچ کس را همچو من، دور جهان حیران نداشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چند روزی رخصتم ده تا کنم درمان خود</p></div>
<div class="m2"><p>گرچه هرگز درد بیماران دل، درمان نداشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مختصر کردم حدیث حال خود در خدمتت</p></div>
<div class="m2"><p>ورنه چون اوصاف تو، درددلم پایان نداشت</p></div></div>