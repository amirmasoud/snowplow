---
title: >-
    شمارهٔ ۹ - اظهار افلاس خود و طلب تدارک
---
# شمارهٔ ۹ - اظهار افلاس خود و طلب تدارک

<div class="b" id="bn1"><div class="m1"><p>ای سروری که بر در دولتسرای تو</p></div>
<div class="m2"><p>هرکس که رو نهاد به دلخواه می رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید با وجود جناب بلند تو</p></div>
<div class="m2"><p>بر آسمان ز همت کوتاه می رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خرمنی که خصم تو دارد، به سوی برق</p></div>
<div class="m2"><p>صد نامه بیش بر پر هر کاه می رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در یک نفس کبوتر عزم تو طی کند</p></div>
<div class="m2"><p>راهی که آفتاب به یک ماه می رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس ندیده کار تو بی مصلحت کسی</p></div>
<div class="m2"><p>یوسف به ریسمان تو در چاه می رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد مدت سه سال که بر درگهت مرا</p></div>
<div class="m2"><p>حرفی نه از وزیر و نه از شاه می رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رحم آیدت به حالم اگر باخبر شوی</p></div>
<div class="m2"><p>کاوقات من به سر به چه اکراه می رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>احوال خویش می کنم از هرکسی نهان</p></div>
<div class="m2"><p>دانم سخن چگونه در افواه می رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راضی نیم که پیشتر از من کسی ترا</p></div>
<div class="m2"><p>گوید فلان غلام نکوخواه می رود</p></div></div>