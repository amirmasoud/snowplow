---
title: >-
    شمارهٔ ۲ - خطاب به بزرگی سروده
---
# شمارهٔ ۲ - خطاب به بزرگی سروده

<div class="b" id="bn1"><div class="m1"><p>ای به دست تو قلم گشته کلید در فیض</p></div>
<div class="m2"><p>ای که از جود تو همت به جهان کامرواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر اگر نیست، غمی نیست، کف جود تو هست</p></div>
<div class="m2"><p>ابر اگر رفت، چه شد، دست سخایت برجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان کیست کزو کام دلی بتوان یافت</p></div>
<div class="m2"><p>هرچه خواهد کسی، از لطف تو می باید خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در بارگه قدر تو چون درویشان</p></div>
<div class="m2"><p>تای جوزی به کف دست فلک از جوزاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن کشتن خصمت به میان چون آید</p></div>
<div class="m2"><p>موی جوهر به تن تیغ تو می گردد راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم عیسی، دم تیغ است بداندیش ترا</p></div>
<div class="m2"><p>دشمن جاه ترا فاتحه تکبیر فناست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاصدی آمد و پیغام تو آورد به من</p></div>
<div class="m2"><p>وه چه قاصد که فرح بخش تر از باد صباست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می توانم که زنم طعنه ی پستی به فلک</p></div>
<div class="m2"><p>از غبار درت از بس که دماغم بالاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه همین منت لطفت به سرم امروز است</p></div>
<div class="m2"><p>سایه پرورد کف جود توام، مدتهاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورم از جود تو نان بر سر خوان دگران</p></div>
<div class="m2"><p>روزی از ابر خورد، گرچه صدف در دریاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پی روشنی بزم تو نورافشان باد</p></div>
<div class="m2"><p>آن چراغی که درو روغن چشم بیناست</p></div></div>