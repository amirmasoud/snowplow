---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>در مجلس وصل یار ای دل</p></div>
<div class="m2"><p>تا کی گویی ره سخن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کار کسی به حرف افتاد</p></div>
<div class="m2"><p>راه سخنی به از دهن نیست</p></div></div>