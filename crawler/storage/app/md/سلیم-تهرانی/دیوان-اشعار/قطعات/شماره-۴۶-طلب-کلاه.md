---
title: >-
    شمارهٔ ۴۶ - طلب کلاه
---
# شمارهٔ ۴۶ - طلب کلاه

<div class="b" id="bn1"><div class="m1"><p>شب خرد دید سربرهنه مرا</p></div>
<div class="m2"><p>گفت ای از تو پشت معنی راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو که سوگند چرخ بر سر توست</p></div>
<div class="m2"><p>سر برهنه چرایی، این چه اداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان تاج آفتاب آرد</p></div>
<div class="m2"><p>گر کلاهی ازو کنی درخواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ای پیر، طفل طبع مرا</p></div>
<div class="m2"><p>کی به سر منت سپهر رواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می فرستد کلاهی ار خواهم</p></div>
<div class="m2"><p>سرفرازی که صاحب سرهاست</p></div></div>