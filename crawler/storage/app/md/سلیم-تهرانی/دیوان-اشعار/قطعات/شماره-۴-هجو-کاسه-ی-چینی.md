---
title: >-
    شمارهٔ ۴ - هجو کاسه ی چینی
---
# شمارهٔ ۴ - هجو کاسه ی چینی

<div class="b" id="bn1"><div class="m1"><p>یکی پیاله ی چینی برای خوردن آب</p></div>
<div class="m2"><p>بداد خواجه مرا از صفات نیکویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گر چو کاسه ی فقرش نهند بر سر راه</p></div>
<div class="m2"><p>ز کهنه کعبی او ننگرد کسی سویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکسته ای که صدا برنیاید از لب او</p></div>
<div class="m2"><p>هزار سنگ زند گر کسی به پهلویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو چاه زمزمش افتاده رخنه ها بر لب</p></div>
<div class="m2"><p>چو حوض کوثر، خورده شکنج ها رویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ساغر دل خاقان زیاده تر گرهش</p></div>
<div class="m2"><p>ز کاسه ی سر فغفور بیشتر مویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حباب خنده بر اندام او زند چون موج</p></div>
<div class="m2"><p>برند چون ز پی آب بر لب جویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیاره دار نماید، ز بس که موج شکست</p></div>
<div class="m2"><p>فشرده همچو حباب دلم ز هر سویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خواجه بخشش این کاسه شد چو حوض یزید</p></div>
<div class="m2"><p>که هرکه آب ازو خورد، شد دعاگویش</p></div></div>