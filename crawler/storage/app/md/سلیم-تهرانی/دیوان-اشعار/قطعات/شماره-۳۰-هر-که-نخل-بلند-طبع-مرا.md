---
title: >-
    شمارهٔ ۳۰ - هر که نخل بلند طبع مرا
---
# شمارهٔ ۳۰ - هر که نخل بلند طبع مرا

<div class="b" id="bn1"><div class="m1"><p>هر که نخل بلند طبع مرا</p></div>
<div class="m2"><p>دید، از اقتضای طالع پست،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ از بس به شاخسارش زد</p></div>
<div class="m2"><p>استخوان های میوه را بشکست</p></div></div>