---
title: >-
    شمارهٔ ۴۲ - هجو حساد شعر
---
# شمارهٔ ۴۲ - هجو حساد شعر

<div class="b" id="bn1"><div class="m1"><p>سلیم اعدای تو حیوان چندند</p></div>
<div class="m2"><p>به ایشان ترک هر بیش و کمی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چوز خمی می رسد از ناکسانت</p></div>
<div class="m2"><p>برآن از چرب نرمی مرهمی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شیطان خود نشاید بود کمتر</p></div>
<div class="m2"><p>اگر خصمی کنی، با آدمی کن</p></div></div>