---
title: >-
    شمارهٔ ۱۵ - در مدح اسلام خان و توصیف قصر او
---
# شمارهٔ ۱۵ - در مدح اسلام خان و توصیف قصر او

<div class="b" id="bn1"><div class="m1"><p>شد بهار و زین حصار نیلگون کرد انتخاب</p></div>
<div class="m2"><p>از پی بزم شرف، برج حمل را آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل شکفت، از رخ نقاب گلستان برداشتند</p></div>
<div class="m2"><p>بر جهان شد رازهای خاک، روشن تر ز آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سمند باد، سوی بوستان آمد بهار</p></div>
<div class="m2"><p>هر طرف افتاده گل های پیاده در رکاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل چون خیل عرب گردید در وادی روان</p></div>
<div class="m2"><p>اشتران سرخ کوهانش ز موج اندر شتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی تعمیر این ویرانه ی دیرین اساس</p></div>
<div class="m2"><p>ابر شد معمار و برق او را به کف زرین طناب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فروغ لاله و گل کز کنار جو شکفت</p></div>
<div class="m2"><p>شد چو برگ غنچه رنگین، پرده ی چشم حباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قتل عاشق کی نهان ماند که از فیض هوا</p></div>
<div class="m2"><p>گل کند بر روی آتش قطره ی خون کباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشت خاشاکی نمی یابد که تعمیرش کند</p></div>
<div class="m2"><p>خانه ی بلبل شد از معموری گلشن خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگ همچون دایه ی بی مهر در شیرش نماند</p></div>
<div class="m2"><p>صبح صادق ریخت از شبنم ز بس در شیر آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مصحف گل را به می خواران فرستاد آن که او</p></div>
<div class="m2"><p>از پر پروانه داد آتش پرستان را کتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عالم از بس باصفا شد، آسمان چون عاشقان</p></div>
<div class="m2"><p>می فرستد نامه سوی خاک از تیر شهاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آتش گل در گلستان کرده از اعجاز حسن</p></div>
<div class="m2"><p>همچو مرغان بهشتی زنده بلبل را کباب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس که شادابی فزون شد باغ را از هر نسیم</p></div>
<div class="m2"><p>رنگ گل در موج می آید چو در ساغر شراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تکیه بر گلبن کند هر لحظه چون مستان نسیم</p></div>
<div class="m2"><p>غوطه در دریا زند هردم چو مرغابی سحاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خیره گردد از فروغش دیده ی نظارگی</p></div>
<div class="m2"><p>برگ گل گویی بود آیینه ای در آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر نفس عاشق ز دامان خود از فیض هوا</p></div>
<div class="m2"><p>در میان پاره های دل کند گل انتخاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوعروسان چمن، مشاطه ی هم گشته اند</p></div>
<div class="m2"><p>خوش تماشایی ست دیگر در کنار جوی آب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سبزه سازد عکس خود را وسمه ی ابروی موج</p></div>
<div class="m2"><p>لاله داغ خویشتن را سرمه ی چشم حباب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گشت بال افشان ز بس هر مرغی از ذوق هوا</p></div>
<div class="m2"><p>از پر مرغابیان شد بالش پر هر حباب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گوهرافشان شد چو دست جود صاحب بر جهان</p></div>
<div class="m2"><p>خوب بیرون آمد آخر ابر نیسانی ز آب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تربیت پرورده ی شاه جهان، اسلام خان</p></div>
<div class="m2"><p>عیسی گردون سوار و آفتاب مه رکاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از برای بزم قدر او قضا چون مطربان</p></div>
<div class="m2"><p>تار بر عود فلک می بندد از سیم شهاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شد جهان همچون درون بیضه امن آباد کبک</p></div>
<div class="m2"><p>زان که در عهدش بجز ناخن نمی گیرد عقاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چندبار منت جود گران سنگش کشد؟</p></div>
<div class="m2"><p>خاک بر سر می کند از دست او فیل سحاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز انتقام عدل او ترسم به جرم خون عشق</p></div>
<div class="m2"><p>حسن، آزادی نبیند هرگز از بند نقاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد لطیف از بس که در عهدش مزاج روزگار</p></div>
<div class="m2"><p>تیغ نتواند در آتش خورد آب بی گلاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون به عهدش بگذرد نخجیر در یاد پلنگ</p></div>
<div class="m2"><p>از دهان او روان گردد چو شیرحوض، آب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای جوان بختی که از آسایش ایام تو</p></div>
<div class="m2"><p>خوابگاه کبک باشد سایه ی بال عقاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>می توان دانست از رنگش، که روزی دیده است</p></div>
<div class="m2"><p>آتش خشم ترا یاقوت زرد آفتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا ترا دولت سرا گردیده، می بوسد سپهر</p></div>
<div class="m2"><p>آستان خانه ی زین ترا، یعنی رکاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرحبا از شعله پیکر توسنت کز چابکی</p></div>
<div class="m2"><p>برق پیش او گران خیز است با چندین شتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باد رفتاری که هرگه گرم جولان می شود</p></div>
<div class="m2"><p>می جهد برق از همه اعضای او همچون شهاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چست در چابک دویدن چون نسیم صبحگاه</p></div>
<div class="m2"><p>تند در منزل بریدن همچو تیغ آفتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از صفیر خواب مخمل چون کبوتر در سماع</p></div>
<div class="m2"><p>از نسیم دامن زین همچو آتش در شتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سنگ را مغزش پریشان می شود همچون شرار</p></div>
<div class="m2"><p>کاسه ی سم را زند چون بر سر او از عتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نیست فرقی، بلکه یک ران چرب تر باشد هنوز</p></div>
<div class="m2"><p>کوه را سنجید با خود چون ترازوی رکاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جلوه ی او از برای آن که آساید سوار</p></div>
<div class="m2"><p>افکند در خانه ی زینش ز مخمل فرش خواب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دامن صحرا ز خون صید گردد لاله زار</p></div>
<div class="m2"><p>در شکار انداختن چون پای آری در رکاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ترکش پرتیر، تابان از کمر خورشیدوار</p></div>
<div class="m2"><p>تیغ آتشبار از کف برق زن همچون سحاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آشکارا از شکنج آستین زرین کمند</p></div>
<div class="m2"><p>آنچنان کز موج دریابار، عکس آفتاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون حباب از طبل بازت دم زند در جویبار</p></div>
<div class="m2"><p>رم دهد مرغابیان موج را از روی آب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاهبازی کان به معنی مرغ دست آموز توست</p></div>
<div class="m2"><p>یارب از جنس چه مرغ او را توان کردن حساب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چشم او چشم کبوتر، رنگ او رنگ تذرو</p></div>
<div class="m2"><p>بال او بال همای و چنگ او چنگ عقاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شاه مرغان است و کوس شاهی او طبل باز</p></div>
<div class="m2"><p>باز صبح است و بود رنگ زر او آفتاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کبک را شهپر اگرچه تیر روی ترکش است</p></div>
<div class="m2"><p>می کند پرتاب، هرگه بیندش، از اضطراب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>موج همچون دام پیچد بر پر مرغابیان</p></div>
<div class="m2"><p>افتد از بال و پر او عکس اگر در جوی آب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون گشاید بال در پرواز، گو نظاره کن</p></div>
<div class="m2"><p>گر ندیده کس به روی سینه ی خوبان کتاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>می توان گفتن که مرغ روح چنگیز است او</p></div>
<div class="m2"><p>نیستش از بس ز قتل عام مرغان اجتناب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>صاحبا! در گلشن مدح تو آن نوبلبلم</p></div>
<div class="m2"><p>کز وجودم یافت گلزار معانی آب و تاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>می روم هرگه فرو، ز اندیشه ی مدحت به خویش</p></div>
<div class="m2"><p>عرش گردد جلوه گاهم چون دعای مستجاب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یک نوازش کن مرا، وان گه ببین طرز سخن</p></div>
<div class="m2"><p>پخته ناید میوه تا گرمی نبیند زآفتاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کار بابخت است و طالع، ورنه خود از بهر چیست</p></div>
<div class="m2"><p>عالمی از لطف تو معمور و حال من خراب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ذره ام، از خاک می باید مرا برداشتن</p></div>
<div class="m2"><p>نیست مفت این سربلندی، آفتابی، آفتاب!</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قافیه تکرار شد، خفاش طبعان بشنوید</p></div>
<div class="m2"><p>آفتاب و آفتاب و آفتاب و آفتاب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>باز سر برزد به وصف قصر گردون رفعتت</p></div>
<div class="m2"><p>مطلعی از مشرق اندیشه ام چون آفتاب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرحبا ای قصر گردون پایه ی عالی جناب</p></div>
<div class="m2"><p>ای تو عرش و ساکنان در تو دعای مستجاب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برج برج این حصار نیلگون را گشته است</p></div>
<div class="m2"><p>شاه برجی مثل تو کم دیده چشم آفتاب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پی نبرده هیچ کس بر پایه ی معراج تو</p></div>
<div class="m2"><p>افکند تیری به تاریکی خرد همچون شهاب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>برفراز بام تو، خورشید باشد در مثل</p></div>
<div class="m2"><p>همچو زرین نقطه بر بالای بیت انتخاب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در هوای فیض بخش ساحتت نبود عجب</p></div>
<div class="m2"><p>همچو طوطی سبز اگر گردد پر مرغ کباب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بحری و گوهر به دامان برده از تو نیک و بد</p></div>
<div class="m2"><p>کوهی و نشنیده هرگز سایلی از تو جواب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از بلندی، طاق ایوان فلک بنیاد تو</p></div>
<div class="m2"><p>سایه افکن گشته چون مد الف بر آفتاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کار خاک از نسبت قدرت ز بس بالا گرفت</p></div>
<div class="m2"><p>آسمان گوید همی یالیتنی کنت تراب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>باد در صحن تو دایم در طرب اسلام خان</p></div>
<div class="m2"><p>کامران و کامجوی و کام بخش و کامیاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>باشد از فیض قدوم او حریمت را شرف</p></div>
<div class="m2"><p>تا کند بیت الشرف برج حمل را آفتاب</p></div></div>