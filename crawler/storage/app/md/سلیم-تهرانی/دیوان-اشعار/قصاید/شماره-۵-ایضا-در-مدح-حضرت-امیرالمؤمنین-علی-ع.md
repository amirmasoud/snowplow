---
title: >-
    شمارهٔ ۵ - ایضا در مدح حضرت امیرالمؤمنین علی (ع)
---
# شمارهٔ ۵ - ایضا در مدح حضرت امیرالمؤمنین علی (ع)

<div class="b" id="bn1"><div class="m1"><p>درین سرای پر افسوس چند باشم آه</p></div>
<div class="m2"><p>پی سفر چو گدایان همیشه بر سر راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی شود چو فلک، دور گردشم آخر</p></div>
<div class="m2"><p>که گفته است که عمر سفر بود کوتاه؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان ز شغل سفر یک دمم امان ندهد</p></div>
<div class="m2"><p>به آن طریق که ماکوی خویش را جولاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین خرابه دریغا کجاست دیواری</p></div>
<div class="m2"><p>که پشت خویش توانم بر آن نهاد چو کاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین که همچو زنانم همیشه در چادر</p></div>
<div class="m2"><p>خطاست دعوی مردی ز من درین خرگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تمام عمر حدیق سفر بود کارم</p></div>
<div class="m2"><p>ز من کسی نشنیده ست یک سخن بی راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رهروان سر و کار است دایمم، گویی</p></div>
<div class="m2"><p>که مشت خاک من است از زمین قافله گاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو گام همره من شو، چو حال من پرسی</p></div>
<div class="m2"><p>که بشنوی چو قلم سرگذشت من در راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس که آرزوی دیدن وطن دارم</p></div>
<div class="m2"><p>نهم برای پریدن به چشم خود پر کاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیافتم به غریبی یک آشنا افسوس</p></div>
<div class="m2"><p>که تا ز حال عزیزان مرا کند آگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شوق نامه ی یاران ز خود رود یوسف</p></div>
<div class="m2"><p>صدای بال کبوتر چو بشنود در چاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوش آن کسی که سفر چون کند، تواند کرد</p></div>
<div class="m2"><p>به سوی خانه ی خود بازگشت همچو نگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نمی روم ز سر راه شوق، حیرانم</p></div>
<div class="m2"><p>کدام خانه خرابم سپرده است به راه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز طوف کعبه و بتخانه، گردش ایام</p></div>
<div class="m2"><p>فکنده دربدرم همچو حرف در افواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز عمر هیچ نماند از غم زمانه مرا</p></div>
<div class="m2"><p>که رشته می شود از خوردن گره کوتاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فغان که پیکرم از آفتاب حادثه سوخت</p></div>
<div class="m2"><p>به وادیی که درو نیست سایه جز در چاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا چو بخت زبون است، یار من چه کند</p></div>
<div class="m2"><p>ز آفتاب نگردیده رنگ سایه سیاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپهر را ز کف انگشت ماه نو افتاد</p></div>
<div class="m2"><p>ولی ز داغ دلم ناخنش نشد کوتاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلا به زیر سپهر این چنین چه می جویی</p></div>
<div class="m2"><p>به حیرتم که چه گم کرده ای درین خرگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فزون ز پایه ی خود، کامی از جهان مطلب</p></div>
<div class="m2"><p>که آب رزق به ماسوره می خورد جولاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز ترک سر چه سخن می کنی، که می میری</p></div>
<div class="m2"><p>اگر چو شمع برد باد از سر تو کلاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فضای روی زمین، جای استراحت نیست</p></div>
<div class="m2"><p>به وقت خواب، کبوتر ازان رود در چاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به چشم پاکروان قلمرو تجرید</p></div>
<div class="m2"><p>جهان کثیف تر است از زمین قافله گاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عبث به تربیت من چه می کشد زحمت</p></div>
<div class="m2"><p>زمانه را نیم از بندگان دولت خواه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حذر ز رسم نوازش، که طفل مغرورم</p></div>
<div class="m2"><p>مرا شکستن سر بهتر از شکست کلاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به این قدر که شب از بام خانه ام گذرد</p></div>
<div class="m2"><p>ز تیر ناله ی من، خارپشت گردد ماه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مکن به حال من ای بخت تیره گریه، که هست</p></div>
<div class="m2"><p>به چشم سرمه کشیده، سرشک آب سیاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز خاک تیره ی هندم شود چو عزم سفر</p></div>
<div class="m2"><p>نمایدم به نظر، شکل جاده بسم الله</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فغان که پیکرم از ضعف همچو سایه شده ست</p></div>
<div class="m2"><p>به خاک تیره برابر، درین زمین سیاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خوش آن که چون روم از ملک هند، آلوده</p></div>
<div class="m2"><p>کشد شمال هری، دامنم به گازرگاه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عجب که پاک شود پیکرم ز آلایش</p></div>
<div class="m2"><p>تنم ز آب شود سوده، گر چو سنگ فراه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به طوف شاه نجف رو کنم که آن راهی ست</p></div>
<div class="m2"><p>که خضر راهنما باشد و خدا همراه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شه سریر ولایت، خلیفه ی بر حق</p></div>
<div class="m2"><p>که مهر اوست به محشر، شفیع اهل گناه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>غرض ز بیت به غیر از ظهور معنی نیست</p></div>
<div class="m2"><p>چو معنی آمده بیرون علی ز بیت الله</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز بندگان دگر، خانه زاد ممتاز است</p></div>
<div class="m2"><p>کسی به نسبت او نیست در حریم اله</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عجب که پا به سر دیگری نهد قدرش</p></div>
<div class="m2"><p>چنین که یافته دیوار آسمان کوتاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نسیم، نکهت خلقش به سوی کنعان برد</p></div>
<div class="m2"><p>گذشت از عرق یوسف آب از سر چاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زهی به سجده ی خاک درت سپهر دوتاه</p></div>
<div class="m2"><p>نوشته سوی درت کعبه، بنده ی درگاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو آن شهی که به عالم یگانه ای در فقر</p></div>
<div class="m2"><p>ازین چه شد که به دور تو صف کشید سپاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کسی که راه به توحید برده، می داند</p></div>
<div class="m2"><p>که هست حلقه ی هاله، کمند وحدت ماه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز آستان تو باید اگر سرافرازی</p></div>
<div class="m2"><p>چو ماه، نقش قدم بر فلک زند خرگاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز جود گنج فشانت درم تعجب کرد</p></div>
<div class="m2"><p>ازان بود به لبش لااله الا الله</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به دور شحنه ی عدل تو برق خرمن سوز</p></div>
<div class="m2"><p>برد ز بیم سوی خاربن چو مرغ پناه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به قصد کین تون طرف کلاه هرکه شکست</p></div>
<div class="m2"><p>همان نفس سر او را چو شمع خورد کلاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز شوق آن که مگر تار سبحه تو شود</p></div>
<div class="m2"><p>کند در آب گهر، رشته همچو موج شناه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به هر دیار که تیغ تو سایه اندازد</p></div>
<div class="m2"><p>چو پای مار کند دست فتنه را کوتاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به دور عدل تو دهقان چو مشعل ماتم</p></div>
<div class="m2"><p>به حکم خویش سر برق را کند پرکاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همین به عهد تو یوسف نشد ز بند آزاد</p></div>
<div class="m2"><p>که چون بخار برون رفت سایه هم از چاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شها تویی که پی یاوری ترا طلبد</p></div>
<div class="m2"><p>ز جور چوب معلم چو طفل گوید شاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مرا که پیر خرد، کودک دبستان است</p></div>
<div class="m2"><p>برم به غیر تو بر دیگری چگونه پناه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز سایه ات نرود سوی آفتاب کسی</p></div>
<div class="m2"><p>که روی او نشود هم ز آفتاب سیاه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دویی به مذهب اخلاص من ز بس کفر است</p></div>
<div class="m2"><p>دو منزل از پی طوفت یکی کنم در راه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به خاک هند فرورفته پای من در قیر</p></div>
<div class="m2"><p>بگیر دست مرا یا علی ولی الله</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سلیم را به نجف جذبه ای عنایت کن</p></div>
<div class="m2"><p>که شد به هند دلش چون سواد هند سیاه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همیشه تا که بود آسمان به سیر و سفر</p></div>
<div class="m2"><p>مرا جناب حریم تو باد منزلگاه</p></div></div>