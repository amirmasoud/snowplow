---
title: >-
    شمارهٔ ۱۷ - تعریف قصر خان مذکور و ستایش او
---
# شمارهٔ ۱۷ - تعریف قصر خان مذکور و ستایش او

<div class="b" id="bn1"><div class="m1"><p>نشود خاک تا به روز شمار</p></div>
<div class="m2"><p>همچو خورشید، پنجه ی معمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که عجب رونقی به عالم داد</p></div>
<div class="m2"><p>زین همایون بنای فیض آثار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده برگ شکوفه ی باغش</p></div>
<div class="m2"><p>باد را همچو ابر، گوهربار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که سامان خرمی دارد</p></div>
<div class="m2"><p>از نم ابر فیض این گلزار،</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گم شود در میان سبزه، اگر</p></div>
<div class="m2"><p>نشود بوی گل به باد سوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در فضایش ز بس که کیفیت</p></div>
<div class="m2"><p>می زند جوش از نسیم بهار،</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو مستان به هر خیابانش</p></div>
<div class="m2"><p>صبح از پی کشان برد دستار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبرد ره به این چمن، هرچند</p></div>
<div class="m2"><p>در همه کوچه ای دویده غبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبد گلفروش را ماند</p></div>
<div class="m2"><p>خانه ی بلبلان این گلزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاخ زنبق که مشرف گل اوست</p></div>
<div class="m2"><p>دارد از غنچه در میان طومار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوی آبی ست سایه ی سروش</p></div>
<div class="m2"><p>که گذشتن ازان بود دشوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین لطافت که هست با خاکش</p></div>
<div class="m2"><p>افتدش رخنه ای چو بر دیوار،</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در زمان همچو چاک جامه ی گل</p></div>
<div class="m2"><p>باغبان دوزدش به سوزن خار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بنای عمارتش، گویی</p></div>
<div class="m2"><p>آینه جای خشت رفته به کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گوهر شبچراغ برده درو</p></div>
<div class="m2"><p>روشنایی ز مهره ی دیوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دارد از ابر فیض در همه فصل</p></div>
<div class="m2"><p>پشت بامش هوای روی بهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از صفا بس که گشته عکس پذیر</p></div>
<div class="m2"><p>این طربخانه را در و دیوار،</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شده از کثرت نظارگیان</p></div>
<div class="m2"><p>همچو آیینه خانه، صورت کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود از نکهت گل قالی</p></div>
<div class="m2"><p>روزنش ناف آهوی تاتار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در فضایش که رشک فردوس است</p></div>
<div class="m2"><p>پای غم کوته است همچون مار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در حریمش چو پا نهی، بینی</p></div>
<div class="m2"><p>مردمی ها ز صورت دیوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کی نسیمی قدم نهاد درو</p></div>
<div class="m2"><p>که به تعظیم برنخاست غبار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بس که رنگینی جهان جمع است</p></div>
<div class="m2"><p>در فضایش چو ساحت گلزار،</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به تماشا چو پا نهاده درو</p></div>
<div class="m2"><p>یافته رنگ رفته را بیمار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفته هردم درو به یکدیگر</p></div>
<div class="m2"><p>نقش قالی و صورت دیوار،</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که درین گلشن بهشت آیین</p></div>
<div class="m2"><p>باد گسترده تا به روز شمار،</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بزم اسلام خان که ساغر جم</p></div>
<div class="m2"><p>نیست آنجا قبول دردی خوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن که شد در بهار تربیتش</p></div>
<div class="m2"><p>قابل کار و بار، دست چنار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن هزبرافکنی که از جرأت</p></div>
<div class="m2"><p>بودش روز جنگ، روز شکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شد ازو زهره ی نهنگان آب</p></div>
<div class="m2"><p>تلخ ازان است آب دریابار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کوه چون سنگ پشت، سردزدد</p></div>
<div class="m2"><p>هرگه افروخت تیغ برق آثار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در تن اوست حلقه های زره</p></div>
<div class="m2"><p>چشمه سار دیار رستمدار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خوشی دور عدل او افکند</p></div>
<div class="m2"><p>سایه تا بر جهان چو ابر بهار،</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نعره ی شیر شد غزالان را</p></div>
<div class="m2"><p>از نیستان صدای موسیقار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در صلاح جهان عدالت او</p></div>
<div class="m2"><p>سرکشی خوش ندارد از اشرار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باغبان چمن بود دلگیر</p></div>
<div class="m2"><p>از درختان شاخ بر دیوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از کف او به بحر آشوب است</p></div>
<div class="m2"><p>موج خود را ازان کشد به کنار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دشمنش را رهی که در پیش است</p></div>
<div class="m2"><p>میل فرسنگ اوست لوح مزار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بس که امنیت از عدالت او</p></div>
<div class="m2"><p>پاسبان شد به کوچه و بازار،</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خال خوبان، نشیمن خود را</p></div>
<div class="m2"><p>همچو هندو ز خط کشیده حصار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گوهر گوشوار خصمش نیست</p></div>
<div class="m2"><p>همچو ضحاک، غیر بیضه ی مار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>طوطیان را ز لذت مدحش</p></div>
<div class="m2"><p>می کند کار نیشکر، منقار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سفره ی نعمتش به صفه ی فیض</p></div>
<div class="m2"><p>آسمانی ست با زمین هموار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گردباد از نهیب تمکینش</p></div>
<div class="m2"><p>خشک گردد به جای خود چو منار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بار سنگین حلم او به زمین</p></div>
<div class="m2"><p>کرده کوهان کوه را هموار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در بهار عدالتش که کسی</p></div>
<div class="m2"><p>جز ستمکر نمی کشد آزار،</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>داغ ها شد ز بس نصیب پلنگ</p></div>
<div class="m2"><p>لاله بی داغ روید از کهسار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای ز قانون مهربانی تو</p></div>
<div class="m2"><p>نقش بالین، طیب هر بیمار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نام گل هرکه بی رضای تو برد</p></div>
<div class="m2"><p>شد چو ماهی زبان او پرخار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چه عجب گر حسود بزم ترا</p></div>
<div class="m2"><p>ندهد گردش جهان آزار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شد ز همواری خرابه ی او</p></div>
<div class="m2"><p>سیل چون موج بوریا هموار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سرورا! از پی دعای تو کرد</p></div>
<div class="m2"><p>بر زبان قلم دو قطعه گذار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا درین چارباغ عقل فریب</p></div>
<div class="m2"><p>بود از قصر آفتاب آثار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این بنا را که روضه ی خلد است</p></div>
<div class="m2"><p>چون هما باد سایه ات معمار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا دلیران به دلربایی خصم</p></div>
<div class="m2"><p>کاکل سر کنند زلف عقار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>باد در پیش پیش خیل ظفر</p></div>
<div class="m2"><p>نیزه ی مردافکنت سردار</p></div></div>