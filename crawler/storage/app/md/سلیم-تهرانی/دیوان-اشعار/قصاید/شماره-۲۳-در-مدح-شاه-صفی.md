---
title: >-
    شمارهٔ ۲۳ - در مدح شاه صفی
---
# شمارهٔ ۲۳ - در مدح شاه صفی

<div class="b" id="bn1"><div class="m1"><p>ما درین کهنه دیر دیر اساس</p></div>
<div class="m2"><p>خشت ویرانه ایم و نقش پلاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می زند روز و شب به خرمن ما</p></div>
<div class="m2"><p>تیغ دهقان برای برق از داس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وای بر جان صید خسته ی ما</p></div>
<div class="m2"><p>که درین دشت پرفریب و هراس،</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانه شد سخت دل چو ریزه ی سنگ</p></div>
<div class="m2"><p>دام شد تنگ چشم چون کرباس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست و دل چون رود به کار مرا؟</p></div>
<div class="m2"><p>نیست چون یک حریف کارشناس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر فطرتم ز بخت زبون</p></div>
<div class="m2"><p>ماند پنهان چو همت از افلاس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستم داخل جهان، آری</p></div>
<div class="m2"><p>جزو معجون نمی شود الماس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم از ننگ لاغری چو علم</p></div>
<div class="m2"><p>می کند از تنم کناره لباس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شدم از اشک و آه خانه خراب</p></div>
<div class="m2"><p>کس نکرده ست سود ازین اجناس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از جهان گر حریر و گر دیبا</p></div>
<div class="m2"><p>طلبیدم، نگفت غیر پلاس!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس که عریانی ام خوش افتاده ست</p></div>
<div class="m2"><p>گفتگو هم نمی کنم به لباس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ره راست، رهنما عبث است</p></div>
<div class="m2"><p>نیست کارم به خضر یا الیاس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دارم از هند، عزم درگه شاه</p></div>
<div class="m2"><p>مانع من مباش ای افلاس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوهر تاج و تخت پادشهی</p></div>
<div class="m2"><p>صفی بن صفی بن عباس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن که باشد غضب در اخلاقش</p></div>
<div class="m2"><p>چون به درج جواهری الماس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر خورد بر خلاف حکمش آب</p></div>
<div class="m2"><p>خوشه را برگ خویش گردد داس</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همتش را نماز حق الله</p></div>
<div class="m2"><p>شیوه ی لطف و جود حق الناس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همچو خورشید دست همت او</p></div>
<div class="m2"><p>آنچنان جود را نهاد اساس،</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کز طلب در وجود محتاجان</p></div>
<div class="m2"><p>پنجه ی خویش جمع کرد حواس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همچو افسونگران عدالت او</p></div>
<div class="m2"><p>بس که دارد جهانیان را پاس،</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زین که ماند به مار زهرآلود</p></div>
<div class="m2"><p>می کند پوست دایم از ریواس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حفظ او گر شبان گله شود</p></div>
<div class="m2"><p>گرگ خود چیست کز سرایت پاس،</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از سر گوسفند نتواند</p></div>
<div class="m2"><p>یک سر موی کم کند رواس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای فزون از جهان و هرچه دروست</p></div>
<div class="m2"><p>با چه سنجد ترا گمان و قیاس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در جهان چشم آفتاب ندید</p></div>
<div class="m2"><p>همچو تو خسروی سپاس شناس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از خدنگ تو بر تن رستم</p></div>
<div class="m2"><p>زره تنگ حلقه چون کرباس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از نهیب تو روح رویین تن</p></div>
<div class="m2"><p>مضطرب همچو مور اندر طاس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در چمن رفت نکهت خلقت</p></div>
<div class="m2"><p>غنچه را شد دماغ پر ز عطاس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دشمنان گرسنه چشم ترا</p></div>
<div class="m2"><p>کشف آید به چشم چون انناس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در زمان تو کار اهل جهان</p></div>
<div class="m2"><p>به گشایش نهاده بس که اساس،</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قفل وسواس را گذاشت ز سر</p></div>
<div class="m2"><p>دارد اکنون کلید آن وسواس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تیغ هندی که همچو آیینه</p></div>
<div class="m2"><p>از تو شد در زمانه روی شناس،</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اسم اصلیش گرچه فولاد است</p></div>
<div class="m2"><p>شد به دور تو نام او الماس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه عجب گر مخالف تو گرفت</p></div>
<div class="m2"><p>کام ازین آسمان سفله اساس،</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که به افسانه و فسون گیرد</p></div>
<div class="m2"><p>روغن از سنگ همچو گاو خراس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رسد آخر سزا ز تیغ کجت</p></div>
<div class="m2"><p>خصم را کز غرور کرد آماس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باد را از بروت خوشه برون</p></div>
<div class="m2"><p>نتوان برد جز به جلوه ی داس</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دولت بی رواج خصم تو هست</p></div>
<div class="m2"><p>از فریب جهان ز روی قیاس،</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>باغبان گوشوار لعل کند</p></div>
<div class="m2"><p>طفل خود را به گوش از گیلاس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا که از قطعه و قصیده کنند</p></div>
<div class="m2"><p>گفتگو شاعران پایه شناس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قطعه قطعه چو این قصیده شوند</p></div>
<div class="m2"><p>دشمنانت به تیغ چون الماس</p></div></div>