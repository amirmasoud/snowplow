---
title: >-
    شمارهٔ ۴ - در قحط سال
---
# شمارهٔ ۴ - در قحط سال

<div class="b" id="bn1"><div class="m1"><p>ز بس شد فعل بد، غماز چون مشک</p></div>
<div class="m2"><p>جهان را شد دماغ آخر ازان خشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خشکی سرو آمد بر لب جو</p></div>
<div class="m2"><p>به پیچ و تاب همچون شاخ آهو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تفاوت نیست از خشکی ایام</p></div>
<div class="m2"><p>میان استخوان و مغز بادام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر را از دم آبی نشان نیست</p></div>
<div class="m2"><p>ز مرگ خود خبر هست و ازان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه بحر در هر کوچه ی موج</p></div>
<div class="m2"><p>ز مرغابی و ماهی داشت صد فوج،</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون او را همین بر سر حباب است</p></div>
<div class="m2"><p>ولی آن هم هوادار سراب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآید بال بط از قحطی آب</p></div>
<div class="m2"><p>کبوتروار خشک از چاه گرداب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هجوم تشنگان بین گاه و بیگاه</p></div>
<div class="m2"><p>که گویی یوسفی افتاده در چاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلبکاری نموده خلق بی تاب</p></div>
<div class="m2"><p>به جای آتش از همسایگان آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هردل، تشنگی افکنده تابی</p></div>
<div class="m2"><p>که یخ بندد به هرجا هست آبی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بس خشکی درو کرده ست تأثیر</p></div>
<div class="m2"><p>گلو را تر نسازد آب شمشیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیارد خوردنش مرد سپاهی</p></div>
<div class="m2"><p>که خنجر تشنگی آرد چو ماهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جست و جوی آب از جان خسته</p></div>
<div class="m2"><p>ازان پیکان چو پیکان زنگ بسته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خضر از تشنگی در وادی غم</p></div>
<div class="m2"><p>عقیق اندر دهن دارد چو خاتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیاید ابر پیش او به سودا</p></div>
<div class="m2"><p>برآمد خشک از بس سنگ دریا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شود گر کاغذ ابری نمایان</p></div>
<div class="m2"><p>ازو دارند مردم چشم باران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بلند از خاک نبود کاغذ باد</p></div>
<div class="m2"><p>به سوی ابر، مکتوبی فرستاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برای جستجوی آب تیشه</p></div>
<div class="m2"><p>به هرسو می دواند نخل ریشه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو لاله باغبان دارد به دل داغ</p></div>
<div class="m2"><p>که بیدستان شد از بی حاصلی باغ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهال از خشکی افتاده ست در پیچ</p></div>
<div class="m2"><p>ز بی برگی ندارد چون الف هیچ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود سرو چمن بی ابر دلگیر</p></div>
<div class="m2"><p>ازان همچون یتیمان قد کشد دیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خراج میوه عمری شد که از تاک</p></div>
<div class="m2"><p>نمی آید به پای تخت تریاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به ناز آن گربه کو را بید پرورد</p></div>
<div class="m2"><p>به موش آسیا می ماند از گرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دود تا در پی آبی به ناکام</p></div>
<div class="m2"><p>به خود مالید روغن، چشم بادام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز خشکی رونق گل ها شکسته</p></div>
<div class="m2"><p>دل هر میوه ای در سینه خسته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نشد کار چمن از بید، آسان</p></div>
<div class="m2"><p>نمی آرد دعای گربه، باران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درین خشکی به مرگ دجله و نیل</p></div>
<div class="m2"><p>فشاند خاک بر سر ابر چون فیل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به زیر گرد خشکی بس که شد گم</p></div>
<div class="m2"><p>کند زاهد به آب جو تیمم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بس در آب، غرقه دلپذیر است</p></div>
<div class="m2"><p>چو میرد تشنه ای، عید غدیر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به طرف چشمه سار از دست مرغان</p></div>
<div class="m2"><p>چو آتش، آب شد در سنگ پنهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بیم فوج کبک کوهساری</p></div>
<div class="m2"><p>چو آب کوزه شد دریا حصاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بس خشکی به دریا روی آورد</p></div>
<div class="m2"><p>کند گرداب همچون آسیا گرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حباب جویبار از دور افلاک</p></div>
<div class="m2"><p>شده چون شیشه ی ساعت پر از خاک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز خشکی شد دم ماهی به گرداب</p></div>
<div class="m2"><p>غبارانگیز چون جاروب بی آب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز مرغابی به هرسو داشت فوجی</p></div>
<div class="m2"><p>نه فوجی ماند دریا را، نه موجی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چه سان دهقان نسازد سینه را چاک</p></div>
<div class="m2"><p>عزیزی همچو گندم کرده در خاک</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زمین را گاو در تحصیل گندم</p></div>
<div class="m2"><p>شکنجه می کند با انبر سم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز گندم دیده تا حسن برشته</p></div>
<div class="m2"><p>فریبش خورده چون آدم، فرشته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز چشم خلق، آب و نان نهان است</p></div>
<div class="m2"><p>جهان را قحط سال آب و نان است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فروشد تا برای قیمت نان</p></div>
<div class="m2"><p>چو گوهر، آب را دزدیده دندان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بس آفت، به خرمن های مردم</p></div>
<div class="m2"><p>تولد کرد مور از فرج گندم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز زنگ فتنه، دهقان را به خانه</p></div>
<div class="m2"><p>شده جو سبز همچون رازیانه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شد ارزان بیضه ی مور جفاکیش</p></div>
<div class="m2"><p>خورد چون مار اما بیضه ی خویش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو اندام سیه بختان مسجد</p></div>
<div class="m2"><p>شپش افتاده در انبار کنجد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ملخ ره می برد زان جسته جسته</p></div>
<div class="m2"><p>که حرص مزرع او را پای بسته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو اسبان عرب، تند و جهنده</p></div>
<div class="m2"><p>ندیده هیچ کس اسب پرنده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز تیغ سبزه با ناخن برد زنگ</p></div>
<div class="m2"><p>به اره شانه سازد تخته ی سنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به چشم مور خرمن کرده انگشت</p></div>
<div class="m2"><p>شکسته خوشه چین را با لگد پشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز گندم کرده دهر تنگ توشه</p></div>
<div class="m2"><p>مرصع تاج شاهان را چو خوشه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنان عالم ز بی برگی خراب است</p></div>
<div class="m2"><p>که مرغ از بهر یک دانه کباب است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نمانده عاشقی در یاد بلبل</p></div>
<div class="m2"><p>به یک جو می فروشد خرمن گل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز جلوه باز مانده کبک و تیهو</p></div>
<div class="m2"><p>زند قمری ز شوق دانه کوکو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نه چتر است آن که ظاهر کرده، افسوس</p></div>
<div class="m2"><p>که نخل ماتم خود بسته طاووس</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو این خشکی علم در عالم افراخت</p></div>
<div class="m2"><p>سموم آسا سوی هندوستان تاخت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جهان آشفته کرد از قحط و تنگی</p></div>
<div class="m2"><p>سواد هند را چون موی زنگی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درین قحطی، مسلمانان گجرات</p></div>
<div class="m2"><p>چو نان بینند، بفرستند صلوات</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زبان از تشنگی در این بر و بوم</p></div>
<div class="m2"><p>عیان کرده ز هر فیلی دو خرطوم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سیاهان پی سوی دریوزه برده</p></div>
<div class="m2"><p>به زیر پوست همچون خون مرده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به نقش پا سری هر ناتوان را</p></div>
<div class="m2"><p>که نان پنجه کش پندارد آن را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز بس بگریست بر احوال مردم</p></div>
<div class="m2"><p>نمانده یک مژه در چشم گندم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز بی قوتی بتان را پنجه ی نغز</p></div>
<div class="m2"><p>چو موسیقار، خشک و خالی از مغز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر حرفی ز خوبان در میان است</p></div>
<div class="m2"><p>حدیث حسن گندمگون نان است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به خانه هرکه بیند میهمان را</p></div>
<div class="m2"><p>خورد در آستین چون فیل نان را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چنان آرند پیش دشمن و دوست</p></div>
<div class="m2"><p>که نان ترش گویی قرص لیموست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نمک از بس درین قحطی گران شد</p></div>
<div class="m2"><p>نمکدان مردمان را سرمه دان شد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سلیمان را بود بر خوان درین شور</p></div>
<div class="m2"><p>نمکدانی به تنگی چون دل مور</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نمک را بس که بد خوردند مردم</p></div>
<div class="m2"><p>نشان آن شد از روی زمین گم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز مصحف آیه ی سوگند حک شد</p></div>
<div class="m2"><p>قسم را کار با نان و نمک شد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز بی قوتی شده چشم بد و نیک</p></div>
<div class="m2"><p>چو نان و آب مفلس خشک و تاریک</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سر از سودای نعمت های الوان</p></div>
<div class="m2"><p>کله را کرده انبان سلیمان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز همت آن که می آراست خوان را</p></div>
<div class="m2"><p>کنون پنهان خورد چون روزه، نان را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کسی چون خضر اگر آبی گزیند</p></div>
<div class="m2"><p>به تاریکی خورد تا کس نبیند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سلیمان را دل از غم بی حضور است</p></div>
<div class="m2"><p>که تنگی در میان خیل مور است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو شط چشم خلیفه گر پر آب است</p></div>
<div class="m2"><p>عجب نبود، که بغدادش خراب است</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کسی را کی شود سیری میسر</p></div>
<div class="m2"><p>که نان دارد بهای آب گوهر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>که این سختی به او هرگز گمان داشت؟</p></div>
<div class="m2"><p>چه شد آن چرب نرمی ها که نان داشت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نشاید سفره ای دیدن به سامان</p></div>
<div class="m2"><p>بجز مطرب، کریمی نیست خوشخوان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بود نان در تنور آن گونه دلخواه</p></div>
<div class="m2"><p>که گویی ماه کنعان است در چاه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>عزیز آن کس که دارد میهمان را</p></div>
<div class="m2"><p>کند شیر و شکر، دستارخوان را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بود بیگانه حرف آش در گوش</p></div>
<div class="m2"><p>نمک شد اهل عالم را فراموش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به صد تلخی، چو دریا، کدخدایی</p></div>
<div class="m2"><p>به جوش آورده دیگ شوربایی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>فتاده سنگی از این سقف مینا</p></div>
<div class="m2"><p>شکسته کاسه ی همسایه را پا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به خود می پیچد از گوشه نشینی</p></div>
<div class="m2"><p>چو تار زلف خوبان، موی چینی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سگان را در دهن از خوان شاهان</p></div>
<div class="m2"><p>نبینی استخوانی غیر دندان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نخورده گربه ها از خوان امید</p></div>
<div class="m2"><p>بجز باد هوا چون گربه ی بید</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به لب حرفی ندارد سفره جز این</p></div>
<div class="m2"><p>که مهمان گر رسد، برخیز و برچین</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دل مردم زند از هر کناره</p></div>
<div class="m2"><p>به بوی گوشت خود را بر قناره</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نیابد گوشتی قصاب دلگیر</p></div>
<div class="m2"><p>که شاهین ترازو را کند سیر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>جهان موشی به صد جان می فروشد</p></div>
<div class="m2"><p>ولی موشی در انبان می فروشد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به هرجا گربه ای از دور دیده</p></div>
<div class="m2"><p>سگ نفس از قفای او دویده</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جهان گردیده بر رواس تاریک</p></div>
<div class="m2"><p>سر فرزند خود افکنده در دیگ</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز کیپایی مپرس و از دکانش</p></div>
<div class="m2"><p>بود پستان مادر شیردانش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به دور افکنده زین قحطی ز دامن</p></div>
<div class="m2"><p>ترازو سنگ خود را چون فلاخن</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چنین کز خوردنی شد دست کوتاه</p></div>
<div class="m2"><p>شکم را می توان گفتن تهی گاه!</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>برآورده نی انبان وار، فریاد</p></div>
<div class="m2"><p>سلیمان را ز قحط، انبان پر باد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>عبث رستم به فکر هفت خوان است</p></div>
<div class="m2"><p>که یک نان هرکه یابد پهلوان است</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گرفته چون گدایان کاسه کشکول</p></div>
<div class="m2"><p>خلیفه از برای آش بهلول</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بجز نرگس نبینم سرفرازی</p></div>
<div class="m2"><p>که باشد صاحب نان و پیازی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز روغن گشت خالی، مغز کنجد</p></div>
<div class="m2"><p>نمانده آرد در انبان سنجد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>رود گلچین پی بلبل به بستان</p></div>
<div class="m2"><p>کزو سازد کبابی همچو مستان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به قمری باغبان زان می دهد رو</p></div>
<div class="m2"><p>که خواهد بیضه اش را بهر کوکو</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ز قحطی، سوده ی آهن به دندان</p></div>
<div class="m2"><p>دهد خاصیت حلوای سوهان</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به دل چون شوق شیرینی نهد بند</p></div>
<div class="m2"><p>سر فرزند باشد کله ی قند</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز بی قوتی ست در هر خانه شیون</p></div>
<div class="m2"><p>ز ناچاری سر شوهر خورد زن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به کشتن گشته هر مادر سزاوار</p></div>
<div class="m2"><p>ز بس فرزند خود را خورده چون مار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>وبا شد آبروی کار قحطی</p></div>
<div class="m2"><p>گل مرگی شکفت از خار قحطی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>پی دفع خلایق زشتی کار</p></div>
<div class="m2"><p>شده چون گرگ یوسف آدمی خوار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چنان دستی به قتل عام بگشاد</p></div>
<div class="m2"><p>که در تیغ اجل صد رخنه افتاد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>نبرد اما ز دست انداز گردون</p></div>
<div class="m2"><p>ز چندین رخنه، یک کس جان به بیرون</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>خضر تا گشت ازین مرگی خبردار</p></div>
<div class="m2"><p>کفن بر دوش می گردد علم وار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>شده نزدیک کز مرگ سیاهان</p></div>
<div class="m2"><p>ازین گلشن برافتد تخم ریحان</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>اجل سرها ز بس بر باد دادی</p></div>
<div class="m2"><p>منار کله شد هر گردبادی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>کثافت فرش در بازار و خانه</p></div>
<div class="m2"><p>مگس حلواخور اهل زمانه</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>رود بوی بد از هرسو به صد میل</p></div>
<div class="m2"><p>به بینی زان گرفته آستین فیل</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>ز جوش مرده، گور و گورکن نیست</p></div>
<div class="m2"><p>به غیر از چشم پوشیدن کفن نیست</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به هرجا زنده ای هم می شود فاش</p></div>
<div class="m2"><p>بود مرده کشی کارش چو نقاش</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چنان از رونق کارش غرور است</p></div>
<div class="m2"><p>که گویی گورکن بهرام گور است</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چنان غسال را چین بر جبین است</p></div>
<div class="m2"><p>که پنداری مگر خاقان چین است</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز ناز تخته کش خود کس چه گوید</p></div>
<div class="m2"><p>الهی مرده شو او را بشوید!</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ز بس سرها به خاک افکنده گردون</p></div>
<div class="m2"><p>بساط کاسه گر شد روی هامون</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نشاید یافتن یک سر، که افلاک</p></div>
<div class="m2"><p>نکرد از دشمنی در کاسه اش خاک</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>درین کشور چنان خوفی به راه است</p></div>
<div class="m2"><p>که در هر گام، صد سر بی کلاه است</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چوما از طالع خود ناامیدیم</p></div>
<div class="m2"><p>چنین وقتی به هندستان رسیدیم</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>درین کشور همه پامال قحطیم</p></div>
<div class="m2"><p>ز بی قدری متاع سال قحطیم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ز دلگیری به ما حسرت نصیبان</p></div>
<div class="m2"><p>سواد هند شد شام غریبان</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>به هرکس باشد او را طبع روشن</p></div>
<div class="m2"><p>زمانه کینه دارد خاصه با من</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>من آن آشفته ی شوریده حالم</p></div>
<div class="m2"><p>که دارد ریشه در آتش نهالم</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>نداند غنچه ام رسم شکفتن</p></div>
<div class="m2"><p>چکد چون زخم، خون از خنده ی من</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو بادامم ز زخم تیر ایام</p></div>
<div class="m2"><p>دلی دارم ولی چون مغز بادام</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>ز زخم تیغ دارم یادگاری</p></div>
<div class="m2"><p>نشان ها چون بنای خشت کاری</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>برم دستی اگر بر غنچه از دور</p></div>
<div class="m2"><p>برآرد خارهمچون نیش زنبور</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>زند بر سینه ی من شاخ گل تیر</p></div>
<div class="m2"><p>به رویم می کشد آیینه شمشیر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ز حیرانی به عکس من چو جوهر</p></div>
<div class="m2"><p>بود آیینه زندان سکندر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>نمی داند به غیر از فتنه زادن</p></div>
<div class="m2"><p>شب گیسو به خون غلتیده ی من</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ز غم آسوده ام دارد حزینی</p></div>
<div class="m2"><p>نمی افتد گره بر موی چینی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>نگردم همچو ابر از قطره سیراب</p></div>
<div class="m2"><p>بود دریاکشی کارم چو گرداب</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>دلم را کی ز جام می فتوح است</p></div>
<div class="m2"><p>که موج باده ام سوهان روح است</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بود بی سایه دیوارم ز پستی</p></div>
<div class="m2"><p>ندارم جای داغ از تنگدستی</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>به من زندان بود این باغ دلگیر</p></div>
<div class="m2"><p>فغان بلبلان آواز زنجیر</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>نفس در سینه ام دایم ز تنگی</p></div>
<div class="m2"><p>به هم پیچیده همچون موی زنگی</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>به بازاری که بختم گشته رهبر</p></div>
<div class="m2"><p>بود گرد یتیمی آب گوهر</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>کند بر فکر شعرم خنده تقدیر</p></div>
<div class="m2"><p>که دارم تکیه بر کاغذ چو تصویر</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>سخن بر من نیفزود آب و تابی</p></div>
<div class="m2"><p>گل کاغذ نمی دارد گلابی</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>به دعوی خصم اگر با من ستیزد</p></div>
<div class="m2"><p>بجز افتادگی از من چه خیزد</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>به گاوی شد یکی گوساله گستاخ</p></div>
<div class="m2"><p>فکندش از خصومت شاخ بر شاخ</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ز کار او خجل شد گاو مسکین</p></div>
<div class="m2"><p>سپر انداخت پیش او ز سرگین</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>فغان کز اقتضای دوربینی</p></div>
<div class="m2"><p>پریشانی نرفت از زلف معنی</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>سواد لفظ بر حالش گواه است</p></div>
<div class="m2"><p>بلی روی پریشانی سیاه است</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>سلیم این گفتگوی عارفان نیست</p></div>
<div class="m2"><p>گزیری از تقاضای زمان نیست</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>یکی گفتا قلندر مشربی را</p></div>
<div class="m2"><p>که بی قوتی مرا افکنده از پا</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>ز ضعفم در تن خشکیده نم نیست</p></div>
<div class="m2"><p>قلندر در جوابش گفت غم نیست</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>جهان شد خلق افیونی ز تنگی</p></div>
<div class="m2"><p>تو هم باریک شو چون فکر بنگی</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>زیان بر اهل معنی سود باشد</p></div>
<div class="m2"><p>الهی عاقبت محمود باشد</p></div></div>