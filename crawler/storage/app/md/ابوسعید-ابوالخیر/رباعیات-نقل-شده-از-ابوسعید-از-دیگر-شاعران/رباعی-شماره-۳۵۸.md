---
title: >-
    رباعی شمارهٔ ۳۵۸
---
# رباعی شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>دارم دلکی غمین بیامرز و مپرس</p></div>
<div class="m2"><p>صد واقعه در کمین بیامرز و مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمنده شوم اگر بپرسی عملم</p></div>
<div class="m2"><p>یا اکرم‌اکرمین بیامرز و مپرس</p></div></div>