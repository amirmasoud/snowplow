---
title: >-
    رباعی شمارهٔ ۳۷۸
---
# رباعی شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>آتش بدو دست خویش بر خرمن خویش</p></div>
<div class="m2"><p>چون خود زده‌ام چه نالم از دشمن خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس دشمن من نیست منم دشمن خویش</p></div>
<div class="m2"><p>ای وای من و دست من و دامن خویش</p></div></div>