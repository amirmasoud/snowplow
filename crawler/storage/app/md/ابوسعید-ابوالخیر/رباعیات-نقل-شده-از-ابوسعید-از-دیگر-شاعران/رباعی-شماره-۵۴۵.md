---
title: >-
    رباعی شمارهٔ ۵۴۵
---
# رباعی شمارهٔ ۵۴۵

<div class="b" id="bn1"><div class="m1"><p>درویشی کن قصد در شاه مکن</p></div>
<div class="m2"><p>وز دامن فقر دست کوتاه مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر دهن مار شو و مال مجوی</p></div>
<div class="m2"><p>در چاه نشین و طلب جاه مکن</p></div></div>