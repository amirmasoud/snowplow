---
title: >-
    رباعی شمارهٔ ۲۳۹
---
# رباعی شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>هوشم نه موافقان و خویشان بردند</p></div>
<div class="m2"><p>این کج کلهان مو پریشان بردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند چرا تو دل بدیشان دادی</p></div>
<div class="m2"><p>والله که من ندادم ایشان بردند</p></div></div>