---
title: >-
    رباعی شمارهٔ ۳۷
---
# رباعی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>تا زلف تو شاه گشت و رخسار تو تخت</p></div>
<div class="m2"><p>افکند دلم برابر تخت تو رخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی بینی مرا شده کشتهٔ بخت</p></div>
<div class="m2"><p>حلقم شده در حلقهٔ سیمین تو سخت</p></div></div>