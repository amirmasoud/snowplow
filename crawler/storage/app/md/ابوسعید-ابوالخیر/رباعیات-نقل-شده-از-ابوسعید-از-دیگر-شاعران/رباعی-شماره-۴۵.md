---
title: >-
    رباعی شمارهٔ ۴۵
---
# رباعی شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>میرفتم و خون دل براهم میریخت</p></div>
<div class="m2"><p>دوزخ دوزخ شرر ز آهم میریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌آمدم از شوق تو بر گلشن کون</p></div>
<div class="m2"><p>دامن دامن گل از گناهم میریخت</p></div></div>