---
title: >-
    رباعی شمارهٔ ۴۳
---
# رباعی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>شیرین دهنی که از لبش جان میریخت</p></div>
<div class="m2"><p>کفرش ز سر زلف پریشان میریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شیخ به کفر زلف او ره می‌برد</p></div>
<div class="m2"><p>خاک ره او بر سر ایمان می‌ریخت</p></div></div>