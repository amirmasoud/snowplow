---
title: >-
    رباعی شمارهٔ ۶۲۱
---
# رباعی شمارهٔ ۶۲۱

<div class="b" id="bn1"><div class="m1"><p>ای روی تو مهر عالم آرای همه</p></div>
<div class="m2"><p>وصل تو شب و روز تمنای همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر با دگران به ز منی وای بمن</p></div>
<div class="m2"><p>ور با همه کس همچو منی وای همه</p></div></div>