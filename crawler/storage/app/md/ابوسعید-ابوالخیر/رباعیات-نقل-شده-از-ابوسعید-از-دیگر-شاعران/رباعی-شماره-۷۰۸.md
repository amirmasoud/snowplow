---
title: >-
    رباعی شمارهٔ ۷۰۸
---
# رباعی شمارهٔ ۷۰۸

<div class="b" id="bn1"><div class="m1"><p>در مدرسه گر چه دانش اندوز شوی</p></div>
<div class="m2"><p>وز گرمی بحث مجلس افروز شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مکتب عشق با همه دانایی</p></div>
<div class="m2"><p>سر گشته چو طفلان نوآموز شوی</p></div></div>