---
title: >-
    رباعی شمارهٔ ۳۴۸
---
# رباعی شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>من بودم دوش و آن بت بنده نواز</p></div>
<div class="m2"><p>از من همه لابه بود و از وی همه ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب رفت و حدیث ما به پایان نرسید</p></div>
<div class="m2"><p>شب را چه گنه قصهٔ ما بود دراز</p></div></div>