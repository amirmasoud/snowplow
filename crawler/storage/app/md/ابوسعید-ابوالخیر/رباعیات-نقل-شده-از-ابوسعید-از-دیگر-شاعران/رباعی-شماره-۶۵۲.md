---
title: >-
    رباعی شمارهٔ ۶۵۲
---
# رباعی شمارهٔ ۶۵۲

<div class="b" id="bn1"><div class="m1"><p>عالم ار نه‌ای ز عبرت عاری</p></div>
<div class="m2"><p>نهری جاری به طورهای طاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وندر همه طورهای نهر جاری</p></div>
<div class="m2"><p>سریست حقیقة الحقایق ساری</p></div></div>