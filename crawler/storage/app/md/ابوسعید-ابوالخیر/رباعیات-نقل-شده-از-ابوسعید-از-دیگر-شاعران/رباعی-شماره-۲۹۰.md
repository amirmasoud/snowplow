---
title: >-
    رباعی شمارهٔ ۲۹۰
---
# رباعی شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>روزی که چراغ عمر خاموش شود</p></div>
<div class="m2"><p>در بستر مرگ عقل مدهوش شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بی دردان مکن خدایا حشرم</p></div>
<div class="m2"><p>ترسم که محبتم فراموش شود</p></div></div>