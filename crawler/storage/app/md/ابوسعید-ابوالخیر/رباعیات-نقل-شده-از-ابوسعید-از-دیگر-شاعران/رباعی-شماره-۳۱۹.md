---
title: >-
    رباعی شمارهٔ ۳۱۹
---
# رباعی شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>یا رب بدو نور دیدهٔ پیغمبر</p></div>
<div class="m2"><p>یعنی بدو شمع دودمان حیدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حال من از عین عنایت بنگر</p></div>
<div class="m2"><p>دارم نظر آنکه نیفتم ز نظر</p></div></div>