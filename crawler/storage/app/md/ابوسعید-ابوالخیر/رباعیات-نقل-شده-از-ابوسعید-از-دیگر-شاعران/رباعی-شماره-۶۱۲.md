---
title: >-
    رباعی شمارهٔ ۶۱۲
---
# رباعی شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>گر جا به حرم ور به کلیسا کرده</p></div>
<div class="m2"><p>زاهد عمل آنچه کرده بی جا کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون علم نباشد عملش خواهد بود</p></div>
<div class="m2"><p>ناکرده چو کرده کرده چون ناکرده</p></div></div>