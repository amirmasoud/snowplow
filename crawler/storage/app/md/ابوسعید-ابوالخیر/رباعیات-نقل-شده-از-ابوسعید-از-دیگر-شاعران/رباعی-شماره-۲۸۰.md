---
title: >-
    رباعی شمارهٔ ۲۸۰
---
# رباعی شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>بخشای بر آنکه جز تو یارش نبود</p></div>
<div class="m2"><p>جز خوردن اندوه تو کارش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو حالتیش باشد که دمی</p></div>
<div class="m2"><p>هم با تو و هم بی تو قرارش نبود</p></div></div>