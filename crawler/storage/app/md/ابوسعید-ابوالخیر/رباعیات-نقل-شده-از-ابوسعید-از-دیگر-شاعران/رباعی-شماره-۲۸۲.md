---
title: >-
    رباعی شمارهٔ ۲۸۲
---
# رباعی شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>جایی که تو باشی اثر غم نبود</p></div>
<div class="m2"><p>آنجا که نباشی دل خرم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که ز فرقت تو یک دم نبود</p></div>
<div class="m2"><p>شادیش زمین و آسمان کم نبود</p></div></div>