---
title: >-
    رباعی شمارهٔ ۳۰
---
# رباعی شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>در رفع حجب کوش نه در جمع کتب</p></div>
<div class="m2"><p>کز جمع کتب نمی‌شود رفع حجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طی کتب بود کجا نشهٔ حب</p></div>
<div class="m2"><p>طی کن همه را بگو الی الله اتب</p></div></div>