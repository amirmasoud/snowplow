---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>یا رب به محمد و علی و زهرا</p></div>
<div class="m2"><p>یا رب به حسین و حسن و آل‌عبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز لطف برآر حاجتم در دو سرا</p></div>
<div class="m2"><p>بی‌منت خلق یا علی الاعلا</p></div></div>