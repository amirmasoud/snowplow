---
title: >-
    رباعی شمارهٔ ۶۵
---
# رباعی شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>گردون کمری ز عمر فرسودهٔ ماست</p></div>
<div class="m2"><p>دریا اثری ز اشک آلودهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوزخ شرری ز رنج بیهودهٔ ماست</p></div>
<div class="m2"><p>فردوس دمی ز وقت آسودهٔ ماست</p></div></div>