---
title: >-
    رباعی شمارهٔ ۳۲۸
---
# رباعی شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>ناقوس نواز گر ز من دارد عار</p></div>
<div class="m2"><p>سجاده نشین اگر ز من کرده کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نیز به رغم هر دو انداخته‌ام</p></div>
<div class="m2"><p>تسبیح در آتش، آتش اندر زنار</p></div></div>