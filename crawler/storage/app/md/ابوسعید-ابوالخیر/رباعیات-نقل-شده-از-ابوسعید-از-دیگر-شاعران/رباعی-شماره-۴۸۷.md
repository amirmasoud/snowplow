---
title: >-
    رباعی شمارهٔ ۴۸۷
---
# رباعی شمارهٔ ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>جانا من و تو نمونهٔ پرگاریم</p></div>
<div class="m2"><p>سر گر چه دو کرده‌ایم یک تن داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نقطه روانیم کنون چون پرگار</p></div>
<div class="m2"><p>در آخر کار سر بهم باز آریم</p></div></div>