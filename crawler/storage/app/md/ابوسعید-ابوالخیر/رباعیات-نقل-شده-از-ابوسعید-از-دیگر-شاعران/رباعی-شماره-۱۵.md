---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>تسبیح ملک را و صفا رضوان را</p></div>
<div class="m2"><p>دوزخ بد را بهشت مر نیکان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیبا جم را و قیصر و خاقان را</p></div>
<div class="m2"><p>جانان ما را و جان ما جانان را</p></div></div>