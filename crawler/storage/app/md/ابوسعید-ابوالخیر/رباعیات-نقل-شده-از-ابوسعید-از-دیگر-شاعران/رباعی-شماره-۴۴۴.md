---
title: >-
    رباعی شمارهٔ ۴۴۴
---
# رباعی شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>گر دست تضرع به دعا بردارم</p></div>
<div class="m2"><p>بیخ و بن کوهها ز جا بردارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن ز تفضلات معبود احد</p></div>
<div class="m2"><p>فاصبر صبرا جمیل را بردارم</p></div></div>