---
title: >-
    رباعی شمارهٔ ۶۱۰
---
# رباعی شمارهٔ ۶۱۰

<div class="b" id="bn1"><div class="m1"><p>ای نیک نکرده و بدیها کرده</p></div>
<div class="m2"><p>و آنگاه نجات خود تمنا کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر عفو مکن تکیه که هرگز نبود</p></div>
<div class="m2"><p>ناکرده چو کرده کرده چون ناکرده</p></div></div>