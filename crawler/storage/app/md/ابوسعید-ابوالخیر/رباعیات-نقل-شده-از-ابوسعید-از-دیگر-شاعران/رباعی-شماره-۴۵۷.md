---
title: >-
    رباعی شمارهٔ ۴۵۷
---
# رباعی شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>زان دم که قرین محنت وافغانم</p></div>
<div class="m2"><p>هر لحظه ز هجران به لب آید جانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محروم ز خاک آستانت زانم</p></div>
<div class="m2"><p>کز سیل سرشک خود گذر نتوانم</p></div></div>