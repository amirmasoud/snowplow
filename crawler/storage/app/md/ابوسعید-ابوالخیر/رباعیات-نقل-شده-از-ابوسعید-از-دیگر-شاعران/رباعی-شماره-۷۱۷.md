---
title: >-
    رباعی شمارهٔ ۷۱۷
---
# رباعی شمارهٔ ۷۱۷

<div class="b" id="bn1"><div class="m1"><p>بردارم دل گر از جهان فرمایی</p></div>
<div class="m2"><p>فرمان برم ار سود و زیان فرمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشینم اگر بر سر آتش گویی</p></div>
<div class="m2"><p>برخیزم اگر از سر جان فرمایی</p></div></div>