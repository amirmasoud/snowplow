---
title: >-
    رباعی شمارهٔ ۲۰
---
# رباعی شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>در دیده به جای خواب آب است مرا</p></div>
<div class="m2"><p>زیرا که به دیدنت شتاب است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند بخواب تا به خواب‌ش بینی</p></div>
<div class="m2"><p>ای بیخبران چه جای خواب است مرا</p></div></div>