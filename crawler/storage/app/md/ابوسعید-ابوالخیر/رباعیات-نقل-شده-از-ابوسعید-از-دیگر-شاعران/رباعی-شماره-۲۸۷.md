---
title: >-
    رباعی شمارهٔ ۲۸۷
---
# رباعی شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>عاشق که غم جان خرابش نرود</p></div>
<div class="m2"><p>تا جان بود از جان تب و تابش نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصیت سیماب بود عاشق را</p></div>
<div class="m2"><p>تا کشته نگردد اضطرابش نرود</p></div></div>