---
title: >-
    رباعی شمارهٔ ۶۱۵
---
# رباعی شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>ما درویشان نشسته در تنگ دره</p></div>
<div class="m2"><p>گه قرص جوین خوریم و گه گشت بره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیران کهن دانند میران سره</p></div>
<div class="m2"><p>هر کس که بما بد نگره جان نبره</p></div></div>