---
title: >-
    رباعی شمارهٔ ۳۰۳
---
# رباعی شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>دل وصل تو ای مهر گسل می‌خواهد</p></div>
<div class="m2"><p>ایام وصال متصل می‌خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصود من از خدای باشد وصلت</p></div>
<div class="m2"><p>امید چنان شود که دل می‌خواهد</p></div></div>