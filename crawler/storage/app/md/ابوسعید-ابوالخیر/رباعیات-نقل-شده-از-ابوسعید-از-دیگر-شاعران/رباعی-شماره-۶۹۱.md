---
title: >-
    رباعی شمارهٔ ۶۹۱
---
# رباعی شمارهٔ ۶۹۱

<div class="b" id="bn1"><div class="m1"><p>خواهی چو خلیل کعبه بنیاد کنی</p></div>
<div class="m2"><p>و آنرا به نماز و طاعت آباد کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی دو هزار بنده آزاد کنی</p></div>
<div class="m2"><p>به زان نبود که خاطری شاد کنی</p></div></div>