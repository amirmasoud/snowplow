---
title: >-
    رباعی شمارهٔ ۴۴۸
---
# رباعی شمارهٔ ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>جهدی بکنم که دل زجان برگیرم</p></div>
<div class="m2"><p>راه سر کوی دلستان برگیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پرده میان من و دلدار منم</p></div>
<div class="m2"><p>برخیزم و خود را ز میان برگیرم</p></div></div>