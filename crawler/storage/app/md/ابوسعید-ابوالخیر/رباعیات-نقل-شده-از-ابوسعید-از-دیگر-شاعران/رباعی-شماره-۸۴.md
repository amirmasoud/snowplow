---
title: >-
    رباعی شمارهٔ ۸۴
---
# رباعی شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>آلودهٔ دنیا جگرش ریش ترست</p></div>
<div class="m2"><p>آسوده‌ترست هر که درویش ترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر خر که برو زنگی و زنجیری هست</p></div>
<div class="m2"><p>چون به نگری بار برو بیش ترست</p></div></div>