---
title: >-
    رباعی شمارهٔ ۱۵۹
---
# رباعی شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>آنرا که قضا ز خیل عشاق نوشت</p></div>
<div class="m2"><p>آزاد ز مسجدست و فارغ ز کنشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانهٔ عشق را چه هجران چه وصال</p></div>
<div class="m2"><p>از خویش گذشته را چه دوزخ چه بهشت</p></div></div>