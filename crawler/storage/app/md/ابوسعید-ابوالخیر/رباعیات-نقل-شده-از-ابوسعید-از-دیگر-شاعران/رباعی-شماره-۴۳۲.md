---
title: >-
    رباعی شمارهٔ ۴۳۲
---
# رباعی شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>عمری به هوس باد هوی پیمودم</p></div>
<div class="m2"><p>در هر کاری خون جگر پالودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر چه زدم دست زغم فرسودم</p></div>
<div class="m2"><p>دست از همه باز داشتم آسودم</p></div></div>