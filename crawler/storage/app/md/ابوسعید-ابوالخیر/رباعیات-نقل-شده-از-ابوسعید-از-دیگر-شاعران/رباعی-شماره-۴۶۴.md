---
title: >-
    رباعی شمارهٔ ۴۶۴
---
# رباعی شمارهٔ ۴۶۴

<div class="b" id="bn1"><div class="m1"><p>با درد تو اندیشهٔ درمان نکنم</p></div>
<div class="m2"><p>با زلف تو آرزوی ایمان نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا تو اگر جان طلبی خوش باشد</p></div>
<div class="m2"><p>اندیشهٔ جان برای جانان نکنم</p></div></div>