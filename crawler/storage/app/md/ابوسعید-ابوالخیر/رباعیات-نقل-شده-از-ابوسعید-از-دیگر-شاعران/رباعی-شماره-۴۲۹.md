---
title: >-
    رباعی شمارهٔ ۴۲۹
---
# رباعی شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>چونان شده‌ام که دید نتوانندم</p></div>
<div class="m2"><p>تا پیش توای نگار بنشانندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید تویی به ذره من مانندم</p></div>
<div class="m2"><p>چون ذره به خورشید همی‌دانندم</p></div></div>