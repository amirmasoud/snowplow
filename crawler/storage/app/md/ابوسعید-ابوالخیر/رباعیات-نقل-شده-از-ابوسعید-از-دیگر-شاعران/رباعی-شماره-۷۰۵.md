---
title: >-
    رباعی شمارهٔ ۷۰۵
---
# رباعی شمارهٔ ۷۰۵

<div class="b" id="bn1"><div class="m1"><p>در کوی تو میدهند جانی به جوی</p></div>
<div class="m2"><p>جانی چه بود که کاروانی به جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از وصل تو یک جو بجهانی ارزد</p></div>
<div class="m2"><p>زین جنس که ماییم جهانی به جوی</p></div></div>