---
title: >-
    رباعی شمارهٔ ۵
---
# رباعی شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>وا فریادا ز عشق وا فریادا</p></div>
<div class="m2"><p>کارم به یکی طرفه نگار افتادا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر داد من شکسته دادا دادا</p></div>
<div class="m2"><p>ور نه من و عشق هر چه بادا بادا</p></div></div>