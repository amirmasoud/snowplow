---
title: >-
    رباعی شمارهٔ ۳۶۳
---
# رباعی شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>ای آینهٔ ذات تو ذات همه کس</p></div>
<div class="m2"><p>مرآت صفات تو صفات همه کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ضامن شدم از بهر نجات همه کس</p></div>
<div class="m2"><p>بر من بنویس سیئات همه کس</p></div></div>