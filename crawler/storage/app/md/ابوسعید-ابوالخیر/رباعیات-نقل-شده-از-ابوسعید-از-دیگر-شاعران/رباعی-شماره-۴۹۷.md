---
title: >-
    رباعی شمارهٔ ۴۹۷
---
# رباعی شمارهٔ ۴۹۷

<div class="b" id="bn1"><div class="m1"><p>افتاده منم به گوشهٔ بیت حزن</p></div>
<div class="m2"><p>غمهای جهان مونس غمخانهٔ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب تو به فضل خویش دندانم را</p></div>
<div class="m2"><p>بخشای به روح حضرت ویس قرن</p></div></div>