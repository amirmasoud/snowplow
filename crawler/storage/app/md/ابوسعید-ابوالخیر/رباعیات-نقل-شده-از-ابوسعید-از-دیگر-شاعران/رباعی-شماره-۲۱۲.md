---
title: >-
    رباعی شمارهٔ ۲۱۲
---
# رباعی شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>از واقعه‌ای ترا خبر خواهم کرد</p></div>
<div class="m2"><p>و آنرا به دو حرف مختصر خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عشق تو در خاک نهان خواهم شد</p></div>
<div class="m2"><p>با مهر تو سر ز خاک بر خواهم کرد</p></div></div>