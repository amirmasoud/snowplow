---
title: >-
    رباعی شمارهٔ ۲۷۰
---
# رباعی شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>ز اول ره عشق تو مرا سهل نمود</p></div>
<div class="m2"><p>پنداشت رسد به منزل وصل تو زود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گامی دو سه رفت و راه را دریا دید</p></div>
<div class="m2"><p>چون پای درون نهاد موجش بربود</p></div></div>