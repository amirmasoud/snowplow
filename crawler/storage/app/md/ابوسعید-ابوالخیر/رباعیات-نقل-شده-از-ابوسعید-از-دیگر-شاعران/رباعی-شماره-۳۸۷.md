---
title: >-
    رباعی شمارهٔ ۳۸۷
---
# رباعی شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>دامان غنای عشق پاک آمد پاک</p></div>
<div class="m2"><p>زآلودگی نیاز با مشتی خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جلوه گر و نظارگی جمله خود اوست</p></div>
<div class="m2"><p>گر ما و تو در میان نباشیم چه باک</p></div></div>