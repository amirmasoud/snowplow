---
title: >-
    رباعی شمارهٔ ۳۲۱
---
# رباعی شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>چشمم که نداشت تاب نظارهٔ یار</p></div>
<div class="m2"><p>شد اشک فشان به پیش آن سیم عذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سیل سرشک عکس رخسارش دید</p></div>
<div class="m2"><p>نقش عجبی بر آب زد آخر کار</p></div></div>