---
title: >-
    رباعی شمارهٔ ۵۷۳
---
# رباعی شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>ای گشته جهان تشنه پرآب از تو</p></div>
<div class="m2"><p>ای رنگ گل و لالهٔ خوش‌آب از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محتاج به کیمیای اکسیر توایم</p></div>
<div class="m2"><p>بیش از همه عقل گشته سیراب از تو</p></div></div>