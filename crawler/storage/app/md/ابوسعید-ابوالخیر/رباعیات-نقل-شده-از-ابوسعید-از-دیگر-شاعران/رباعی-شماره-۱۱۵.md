---
title: >-
    رباعی شمارهٔ ۱۱۵
---
# رباعی شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>آنرا که حلال زادگی عادت و خوست</p></div>
<div class="m2"><p>عیب همه مردمان به چشمش نیکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معیوب همه عیب کسان می‌نگرد</p></div>
<div class="m2"><p>از کوزه همان برون تراود که دروست</p></div></div>