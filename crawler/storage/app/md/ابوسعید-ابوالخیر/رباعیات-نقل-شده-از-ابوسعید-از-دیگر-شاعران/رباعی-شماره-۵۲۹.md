---
title: >-
    رباعی شمارهٔ ۵۲۹
---
# رباعی شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>از باده به روی شیخ رنگ آوردن</p></div>
<div class="m2"><p>اسلام ز جانب فرنگ آوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناقوس به کعبه در درنگ آوردن</p></div>
<div class="m2"><p>بتوان نتوان ترا به چنگ آوردن</p></div></div>