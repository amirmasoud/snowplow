---
title: >-
    رباعی شمارهٔ ۶۰
---
# رباعی شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>گر طالب راه حق شوی ره پیداست</p></div>
<div class="m2"><p>او راست بود با تو، تو گر باشی راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگه که به اخلاص و درون صافی</p></div>
<div class="m2"><p>او را باشی بدان که او نیز تراست</p></div></div>