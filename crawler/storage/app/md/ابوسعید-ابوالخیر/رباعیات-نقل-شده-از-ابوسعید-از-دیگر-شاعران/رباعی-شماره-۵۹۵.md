---
title: >-
    رباعی شمارهٔ ۵۹۵
---
# رباعی شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>ابر از دهقان که ژاله می‌روید ازو</p></div>
<div class="m2"><p>دشت از مجنون که لاله می‌روید ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلد از صوفی و حور عین از زاهد</p></div>
<div class="m2"><p>ما و دلکی که ناله می‌روید ازو</p></div></div>