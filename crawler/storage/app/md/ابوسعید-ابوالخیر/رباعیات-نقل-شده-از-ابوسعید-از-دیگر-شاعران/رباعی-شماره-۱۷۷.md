---
title: >-
    رباعی شمارهٔ ۱۷۷
---
# رباعی شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>زنار پرست زلف عنبر بویت</p></div>
<div class="m2"><p>محراب نشین گوشهٔ ابرویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب تو چه کعبه‌ای که باشد شب و روز</p></div>
<div class="m2"><p>روی دل کافر و مسلمان سویت</p></div></div>