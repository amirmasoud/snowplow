---
title: >-
    رباعی شمارهٔ ۲۳۱
---
# رباعی شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>اسرار وجود خام و ناپخته بماند</p></div>
<div class="m2"><p>و آن گوهر بس شریف ناسفته بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس به دلیل عقل چیزی گفتند</p></div>
<div class="m2"><p>آن نکته که اصل بود ناگفته بماند</p></div></div>