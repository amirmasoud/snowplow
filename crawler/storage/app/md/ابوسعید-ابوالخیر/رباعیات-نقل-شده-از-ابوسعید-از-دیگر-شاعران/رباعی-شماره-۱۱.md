---
title: >-
    رباعی شمارهٔ ۱۱
---
# رباعی شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>گر بر در دیر می‌نشانی ما را</p></div>
<div class="m2"><p>گر در ره کعبه میدوانی ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینها همگی لازمهٔ هستی ماست</p></div>
<div class="m2"><p>خوش آنکه ز خویش وارهانی ما را</p></div></div>