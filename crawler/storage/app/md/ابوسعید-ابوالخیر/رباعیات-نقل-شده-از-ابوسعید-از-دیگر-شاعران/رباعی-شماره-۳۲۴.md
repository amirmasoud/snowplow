---
title: >-
    رباعی شمارهٔ ۳۲۴
---
# رباعی شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>یا رب بگشا گره ز کار من زار</p></div>
<div class="m2"><p>رحمی که زعقل عاجزم در همه کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز در گه تو کی بودم در گاهی</p></div>
<div class="m2"><p>محروم ازین درم مکن یا غفار</p></div></div>