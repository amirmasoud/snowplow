---
title: >-
    رباعی شمارهٔ ۱۲۴
---
# رباعی شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>ای خواجه ترا غم جمال ماهست</p></div>
<div class="m2"><p>اندیشهٔ باغ و راغ و خرمن گاهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما سوختگان عالم تجریدیم</p></div>
<div class="m2"><p>ما را غم لا اله الا اللهست</p></div></div>