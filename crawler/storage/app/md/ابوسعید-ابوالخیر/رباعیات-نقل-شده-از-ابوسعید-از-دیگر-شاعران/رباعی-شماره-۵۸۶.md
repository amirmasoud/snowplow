---
title: >-
    رباعی شمارهٔ ۵۸۶
---
# رباعی شمارهٔ ۵۸۶

<div class="b" id="bn1"><div class="m1"><p>ای در دل من اصل تمنا همه تو</p></div>
<div class="m2"><p>وی در سر من مایهٔ سودا همه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند به روزگار در می‌نگرم</p></div>
<div class="m2"><p>امروز همه تویی و فردا همه تو</p></div></div>