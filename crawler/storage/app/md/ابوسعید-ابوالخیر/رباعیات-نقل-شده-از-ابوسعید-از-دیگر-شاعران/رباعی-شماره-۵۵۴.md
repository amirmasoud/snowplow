---
title: >-
    رباعی شمارهٔ ۵۵۴
---
# رباعی شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>ای گشته سراسیمه به دریای تو من</p></div>
<div class="m2"><p>وی از تو و خود گم شده در رای تو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در تو کجا رسم که در ذات و صفات</p></div>
<div class="m2"><p>پنهانی من تویی و پیدای تو من</p></div></div>