---
title: >-
    رباعی شمارهٔ ۲۴
---
# رباعی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>آن عشق که هست جزء لاینفک ما</p></div>
<div class="m2"><p>حاشا که شود به عقل ما مدرک ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آنکه ز نور او دمد صبح یقین</p></div>
<div class="m2"><p>ما را برهاند ز ظلام شک ما</p></div></div>