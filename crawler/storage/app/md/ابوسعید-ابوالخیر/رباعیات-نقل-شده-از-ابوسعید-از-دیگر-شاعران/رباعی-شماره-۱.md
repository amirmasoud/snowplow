---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>باز آ باز آ هر آنچه هستی باز آ</p></div>
<div class="m2"><p>گر کافر و گبر و بت‌پرستی باز آ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این درگه ما درگه نومیدی نیست</p></div>
<div class="m2"><p>صد بار اگر توبه شکستی باز آ</p></div></div>