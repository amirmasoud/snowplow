---
title: >-
    رباعی شمارهٔ ۶۱۷
---
# رباعی شمارهٔ ۶۱۷

<div class="b" id="bn1"><div class="m1"><p>هجران ترا چو گرم شد هنگامه</p></div>
<div class="m2"><p>بر آتش من قطره فشان از خامه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من رفتم و مرغ روح من پیش تو ماند</p></div>
<div class="m2"><p>تا همچو کبوتر از تو آرد نامه</p></div></div>