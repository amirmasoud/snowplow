---
title: >-
    رباعی شمارهٔ ۴۶۲
---
# رباعی شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>از بیم رقیب طوف کویت نکنم</p></div>
<div class="m2"><p>وز طعنهٔ خلق گفتگویت نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب بستم و از پای نشستم اما</p></div>
<div class="m2"><p>این نتوانم که آرزویت نکنم</p></div></div>