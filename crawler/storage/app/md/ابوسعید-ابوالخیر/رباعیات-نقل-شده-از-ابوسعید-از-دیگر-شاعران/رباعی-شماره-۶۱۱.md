---
title: >-
    رباعی شمارهٔ ۶۱۱
---
# رباعی شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>زاهد خوشدل که ترک دنیا کرده</p></div>
<div class="m2"><p>می خواره خجل که معصیت‌ها کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که کند امید و بیم و آخر کار</p></div>
<div class="m2"><p>ناکرده چو کرده کرده چون ناکرده</p></div></div>