---
title: >-
    رباعی شمارهٔ ۵۲۴
---
# رباعی شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>چون حق به تفاصیل شئون گشت بیان</p></div>
<div class="m2"><p>مشهود شد این عالم پر سود و زیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باز روند عالم و عالمیان</p></div>
<div class="m2"><p>با رتبهٔ اجمال حق آیند عیان</p></div></div>