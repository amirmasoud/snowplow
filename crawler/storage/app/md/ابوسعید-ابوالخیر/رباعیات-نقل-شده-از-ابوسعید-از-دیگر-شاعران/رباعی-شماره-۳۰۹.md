---
title: >-
    رباعی شمارهٔ ۳۰۹
---
# رباعی شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>یاد تو کنم دلم به فریاد آید</p></div>
<div class="m2"><p>نام تو برم عمر شده یاد آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگه که مرا حدیث تو یاد آید</p></div>
<div class="m2"><p>با من در و دیوار به فریاد آید</p></div></div>