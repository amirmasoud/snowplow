---
title: >-
    رباعی شمارهٔ ۳۷۵
---
# رباعی شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>دارم گنهان ز قطره باران بیش</p></div>
<div class="m2"><p>از شرم گنه فگنده‌ام سر در پیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آواز آید که سهل باشد درویش</p></div>
<div class="m2"><p>تو در خور خود کنی و ما در خور خویش</p></div></div>