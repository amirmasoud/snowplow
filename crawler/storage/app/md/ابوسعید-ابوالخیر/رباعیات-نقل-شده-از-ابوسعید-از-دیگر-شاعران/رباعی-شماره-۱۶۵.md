---
title: >-
    رباعی شمارهٔ ۱۶۵
---
# رباعی شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>دل گر چه درین بادیه بسیار شتافت</p></div>
<div class="m2"><p>یک موی ندانست و بسی موی شکافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه ز دلم هزار خورشید بتافت</p></div>
<div class="m2"><p>آخر به کمال ذره‌ای راه نیافت</p></div></div>