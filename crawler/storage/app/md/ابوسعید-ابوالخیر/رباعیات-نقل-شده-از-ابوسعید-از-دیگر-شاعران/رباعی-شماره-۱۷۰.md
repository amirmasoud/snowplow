---
title: >-
    رباعی شمارهٔ ۱۷۰
---
# رباعی شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>آنی که ز جانم آرزوی تو نرفت</p></div>
<div class="m2"><p>از دل هوس روی نکوی تو نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوی تو هر که رفت دل را بگذاشت</p></div>
<div class="m2"><p>کس با دل خویشتن ز کوی تو نرفت</p></div></div>