---
title: >-
    رباعی شمارهٔ ۱۲۶
---
# رباعی شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>در کار کس ار قرار میباید هست</p></div>
<div class="m2"><p>وین یار که در کنار میباید هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجریکه بهیچ کار می‌ناید نیست</p></div>
<div class="m2"><p>وصلی که چو جان بکار میباید هست</p></div></div>