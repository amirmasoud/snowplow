---
title: >-
    رباعی شمارهٔ ۶۰۶
---
# رباعی شمارهٔ ۶۰۶

<div class="b" id="bn1"><div class="m1"><p>از هر چه نه از بهر تو کردم توبه</p></div>
<div class="m2"><p>ور بی تو غمی خوردم از آن غم توبه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن نیز که بعد ازین برای تو کنم</p></div>
<div class="m2"><p>گر بهتر از آن توان از آن هم توبه</p></div></div>