---
title: >-
    رباعی شمارهٔ ۳۹۹
---
# رباعی شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>هر نعت که از قبیل خیرست و کمال</p></div>
<div class="m2"><p>باشد ز نعوت ذات پاک متعال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر وصف که در حساب شرست و وبال</p></div>
<div class="m2"><p>دارد به قصور قابلیات مل</p></div></div>