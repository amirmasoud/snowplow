---
title: >-
    رباعی شمارهٔ ۵۹۷
---
# رباعی شمارهٔ ۵۹۷

<div class="b" id="bn1"><div class="m1"><p>ای دل چو فراق یار دیدی خون شو</p></div>
<div class="m2"><p>وی دیده موافقت بکن جیحون شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان تو عزیزتر نه‌ای از یارم</p></div>
<div class="m2"><p>بی یار نخواهمت زتن بیرون شو</p></div></div>