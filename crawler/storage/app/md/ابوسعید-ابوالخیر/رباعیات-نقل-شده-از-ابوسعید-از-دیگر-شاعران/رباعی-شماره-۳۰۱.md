---
title: >-
    رباعی شمارهٔ ۳۰۱
---
# رباعی شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>دلبر دل خسته رایگان می‌خواهد</p></div>
<div class="m2"><p>بفرستم گر دلش چنان می‌خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگه به نظاره دیده بر ره بنهم</p></div>
<div class="m2"><p>تا مژده که آورد که جان میخواهد</p></div></div>