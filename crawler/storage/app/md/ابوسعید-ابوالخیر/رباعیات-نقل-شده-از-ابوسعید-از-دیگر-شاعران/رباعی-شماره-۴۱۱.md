---
title: >-
    رباعی شمارهٔ ۴۱۱
---
# رباعی شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>من دانگی و نیم داشتم حبهٔ کم</p></div>
<div class="m2"><p>دو کوزه نبید خریده‌ام پارهٔ کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بربط ما نه زیر ماندست و نه بم</p></div>
<div class="m2"><p>تا کی گویی قلندری و غم و غم</p></div></div>