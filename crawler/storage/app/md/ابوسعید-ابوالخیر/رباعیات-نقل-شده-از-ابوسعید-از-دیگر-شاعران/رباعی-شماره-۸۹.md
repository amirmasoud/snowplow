---
title: >-
    رباعی شمارهٔ ۸۹
---
# رباعی شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>سرمایهٔ عمر آدمی یک نفسست</p></div>
<div class="m2"><p>آن یک نفس از برای یک همنفسست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همنفسی گر نفسی بنشینی</p></div>
<div class="m2"><p>مجموع حیات عمر آن یک نفسست</p></div></div>