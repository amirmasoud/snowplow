---
title: >-
    رباعی شمارهٔ ۳۳۰
---
# رباعی شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>یا رب به کرم بر من درویش نگر</p></div>
<div class="m2"><p>در من منگر در کرم خویش نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند نیم لایق بخشایش تو</p></div>
<div class="m2"><p>بر حال من خستهٔ دلریش نگر</p></div></div>