---
title: >-
    رباعی شمارهٔ ۲۶۳
---
# رباعی شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>مردان تو دل به مهر گردون ننهند</p></div>
<div class="m2"><p>لب بر لب این کاسهٔ پر خون ننهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دایرهٔ اهل وفا چون پرگار</p></div>
<div class="m2"><p>گر سر بنهند پای بیرون ننهند</p></div></div>