---
title: >-
    رباعی شمارهٔ ۵۹۳
---
# رباعی شمارهٔ ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>زلفش بکشی شب دراز آید ازو</p></div>
<div class="m2"><p>ور بگذاری چنگل باز آید ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور پیچ و خمش ز یک دگر باز کنی</p></div>
<div class="m2"><p>عالم عالم مشک فراز آید ازو</p></div></div>