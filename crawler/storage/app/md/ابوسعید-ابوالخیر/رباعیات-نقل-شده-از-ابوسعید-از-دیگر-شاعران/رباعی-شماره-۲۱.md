---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>آن رشته که قوت روانست مرا</p></div>
<div class="m2"><p>آرامش جان ناتوانست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لب چو کشی جان کشدم از پی آن</p></div>
<div class="m2"><p>پیوند چو با رشتهٔ جانست مرا</p></div></div>