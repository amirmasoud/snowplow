---
title: >-
    رباعی شمارهٔ ۶۵۹
---
# رباعی شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>یا شاه تویی آنکه خدا را شیری</p></div>
<div class="m2"><p>خندق جه و مرحب کش و خیبر گیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپسند غلام عاجزت یا مولا</p></div>
<div class="m2"><p>ایام کند ذلیل هر بی‌پیری</p></div></div>