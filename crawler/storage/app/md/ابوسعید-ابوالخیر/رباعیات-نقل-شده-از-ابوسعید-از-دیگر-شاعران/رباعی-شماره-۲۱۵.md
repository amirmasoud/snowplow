---
title: >-
    رباعی شمارهٔ ۲۱۵
---
# رباعی شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>خرم دل آنکه از ستم آه نکرد</p></div>
<div class="m2"><p>کس را ز درون خویش آگاه نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع ز سوز دل سراپا بگداخت</p></div>
<div class="m2"><p>وز دامن شعله دست کوتاه نکرد</p></div></div>