---
title: >-
    رباعی شمارهٔ ۵۰۸
---
# رباعی شمارهٔ ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>آن دوست که هست عشق او دشمن جان</p></div>
<div class="m2"><p>بر باد همی دهد غمش خرمن جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در طلبش دربدر و کوی به کوی</p></div>
<div class="m2"><p>او در دل و کرده دست در گردن جان</p></div></div>