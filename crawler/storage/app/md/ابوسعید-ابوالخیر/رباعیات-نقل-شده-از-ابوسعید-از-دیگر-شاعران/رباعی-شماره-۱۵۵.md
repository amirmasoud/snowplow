---
title: >-
    رباعی شمارهٔ ۱۵۵
---
# رباعی شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>کبریست درین وهم که پنهانی نیست</p></div>
<div class="m2"><p>برداشتن سرم به آسانی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمانش هزار دفعه تلقین کردم</p></div>
<div class="m2"><p>این کافر را سر مسلمانی نیست</p></div></div>