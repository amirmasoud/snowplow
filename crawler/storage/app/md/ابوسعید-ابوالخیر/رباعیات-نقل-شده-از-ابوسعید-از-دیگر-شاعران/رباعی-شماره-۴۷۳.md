---
title: >-
    رباعی شمارهٔ ۴۷۳
---
# رباعی شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>ای دوست طواف خانه‌ات می‌خواهم</p></div>
<div class="m2"><p>بوسیدن آستانه‌ات می‌خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌منت خلق توشه این ره را</p></div>
<div class="m2"><p>می‌خواهم و از خزانه‌ات می‌خواهم</p></div></div>