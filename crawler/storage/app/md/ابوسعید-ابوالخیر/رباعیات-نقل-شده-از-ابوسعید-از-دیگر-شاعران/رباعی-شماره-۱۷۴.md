---
title: >-
    رباعی شمارهٔ ۱۷۴
---
# رباعی شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>دی زلف عبیر بیز عنبر سایت</p></div>
<div class="m2"><p>از طرف بناگوش سمن سیمایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پای تو افتاد و بزاری می‌گفت</p></div>
<div class="m2"><p>سر تا پایم فدای سر تا پایت</p></div></div>