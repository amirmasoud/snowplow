---
title: >-
    رباعی شمارهٔ ۱۴۱
---
# رباعی شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>گاهی چو ملایکم سر بندگیست</p></div>
<div class="m2"><p>گه چون حیوان به خواب و خور زندگیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهم چو بهایم سر درندگیست</p></div>
<div class="m2"><p>سبحان الله این چه پراکندگیست</p></div></div>