---
title: >-
    رباعی شمارهٔ ۲۳۳
---
# رباعی شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>آنها که ز معبود خبر یافته‌اند</p></div>
<div class="m2"><p>از جملهٔ کاینات سر یافته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریوزه همی کنند مردان ز نظر</p></div>
<div class="m2"><p>مردان همه از قرب نظر یافته‌اند</p></div></div>