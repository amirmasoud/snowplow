---
title: >-
    رباعی شمارهٔ ۲۶۸
---
# رباعی شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>اول که مرا عشق نگارم بربود</p></div>
<div class="m2"><p>همسایهٔ من ز نالهٔ من نغنود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واکنون کم شد ناله چو دردم بفزود</p></div>
<div class="m2"><p>آتش چو همه گرفت کم گردد دود</p></div></div>