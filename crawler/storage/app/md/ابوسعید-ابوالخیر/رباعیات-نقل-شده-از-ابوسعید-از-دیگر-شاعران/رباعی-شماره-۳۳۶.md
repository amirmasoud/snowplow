---
title: >-
    رباعی شمارهٔ ۳۳۶
---
# رباعی شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>گر دور فتادم از وصالت به ضرور</p></div>
<div class="m2"><p>دارد دلم از یاد تو صد نوع حضور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصیت سایهٔ تو دارم که مدام</p></div>
<div class="m2"><p>نزدیک توام اگر چه می‌افتم دور</p></div></div>