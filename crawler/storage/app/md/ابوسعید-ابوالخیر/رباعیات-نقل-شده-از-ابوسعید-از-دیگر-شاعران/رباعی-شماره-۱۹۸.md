---
title: >-
    رباعی شمارهٔ ۱۹۸
---
# رباعی شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>دل صافی کن که حق به دل می‌نگرد</p></div>
<div class="m2"><p>دلهای پراکنده به یک جو نخرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد که کند صاف دل از بهر خدا</p></div>
<div class="m2"><p>گویی ز همه مردم عالم ببرد</p></div></div>