---
title: >-
    رباعی شمارهٔ ۲۵۰
---
# رباعی شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>خواهی که خدا کار نکو با تو کند</p></div>
<div class="m2"><p>ارواح ملایک همه رو با تو کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا هر چه رضای او در آنست بکن</p></div>
<div class="m2"><p>یا راضی شو هر آنچه او با تو کند</p></div></div>