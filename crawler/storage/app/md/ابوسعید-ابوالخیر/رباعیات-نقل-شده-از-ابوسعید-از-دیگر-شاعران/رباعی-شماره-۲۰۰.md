---
title: >-
    رباعی شمارهٔ ۲۰۰
---
# رباعی شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>من زنده و کس بر آستانت گذرد</p></div>
<div class="m2"><p>یا مرغ بگرد سر کویت بپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار گورم شکسته در چشم کسی</p></div>
<div class="m2"><p>کو از پس مرگ من برویت نگرد</p></div></div>