---
title: >-
    رباعی شمارهٔ ۵۵
---
# رباعی شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>کردم توبه، شکستیش روز نخست</p></div>
<div class="m2"><p>چون بشکستم بتوبه‌ام خواندی چست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصه زمام توبه‌ام در کف تست</p></div>
<div class="m2"><p>یکدم نه شکسته‌اش گذاری نه درست</p></div></div>