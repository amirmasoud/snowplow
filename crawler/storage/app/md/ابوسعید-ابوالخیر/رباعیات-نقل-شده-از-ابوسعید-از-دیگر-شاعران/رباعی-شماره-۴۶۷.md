---
title: >-
    رباعی شمارهٔ ۴۶۷
---
# رباعی شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>تا بردی ازین دیار تشریف قدوم</p></div>
<div class="m2"><p>بر دل رقم شوق تو دارم مرقوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این قصه مرا کشت که هنگام وداع</p></div>
<div class="m2"><p>از دولت دیدار تو گشتم محروم</p></div></div>