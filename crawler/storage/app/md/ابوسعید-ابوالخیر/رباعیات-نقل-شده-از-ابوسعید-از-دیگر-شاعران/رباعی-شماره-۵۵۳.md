---
title: >-
    رباعی شمارهٔ ۵۵۳
---
# رباعی شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تراست عار از دیدن من</p></div>
<div class="m2"><p>مهرت باشد بجای جان در تن من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دست نگار بسته خواهم که زنی</p></div>
<div class="m2"><p>با خون هزار کشته در گردن من</p></div></div>