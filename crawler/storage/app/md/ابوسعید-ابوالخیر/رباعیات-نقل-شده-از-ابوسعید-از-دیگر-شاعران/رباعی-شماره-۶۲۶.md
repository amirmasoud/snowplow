---
title: >-
    رباعی شمارهٔ ۶۲۶
---
# رباعی شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>من کیستم آتش به دل افروخته‌ای</p></div>
<div class="m2"><p>وز خرمن دهر دیده بر دوخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه وفا چو سنگ و آتش گردم</p></div>
<div class="m2"><p>شاید که رسم به صبحت سوخته‌ای</p></div></div>