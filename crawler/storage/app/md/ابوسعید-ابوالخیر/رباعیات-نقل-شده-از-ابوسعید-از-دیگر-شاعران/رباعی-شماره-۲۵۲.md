---
title: >-
    رباعی شمارهٔ ۲۵۲
---
# رباعی شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>عاشق که تواضع ننماید چه کند</p></div>
<div class="m2"><p>شبها که به کوی تو نیاید چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بوسه دهد زلف ترا رنجه مشو</p></div>
<div class="m2"><p>دیوانه که زنجیر نخاید چه کند</p></div></div>