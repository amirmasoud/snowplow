---
title: >-
    رباعی شمارهٔ ۶۱۳
---
# رباعی شمارهٔ ۶۱۳

<div class="b" id="bn1"><div class="m1"><p>بحریست نه کاهنده نه افزاینده</p></div>
<div class="m2"><p>امواج برو رونده و آینده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم چو عبارت از همین امواجست</p></div>
<div class="m2"><p>نبود دو زمان بلکه دو آن پاینده</p></div></div>