---
title: >-
    رباعی شمارهٔ ۱۰
---
# رباعی شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>یا رب مکن از لطف پریشان ما را</p></div>
<div class="m2"><p>هر چند که هست جرم و عصیان ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذات تو غنی بوده و ما محتاجیم</p></div>
<div class="m2"><p>محتاج بغیر خود مگردان ما را</p></div></div>