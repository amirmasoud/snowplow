---
title: >-
    رباعی شمارهٔ ۳۰۸
---
# رباعی شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>معشوقهٔ خانگی به کاری ناید</p></div>
<div class="m2"><p>کودل برد و روی به کس ننماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوقه خراباتی و مطرب باید</p></div>
<div class="m2"><p>تا نیم شبان زنان و کوبان آید</p></div></div>