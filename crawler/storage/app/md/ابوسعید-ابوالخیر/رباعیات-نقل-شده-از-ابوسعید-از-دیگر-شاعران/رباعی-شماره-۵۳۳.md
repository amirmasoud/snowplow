---
title: >-
    رباعی شمارهٔ ۵۳۳
---
# رباعی شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>در راه خدا حجاب شد یک سو زن</p></div>
<div class="m2"><p>رو جملهٔ کار خویش را یک سو زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ماندهٔ نفس خویش گشتی و ترا</p></div>
<div class="m2"><p>یک سو غم مال و دختر و یک سو زن</p></div></div>