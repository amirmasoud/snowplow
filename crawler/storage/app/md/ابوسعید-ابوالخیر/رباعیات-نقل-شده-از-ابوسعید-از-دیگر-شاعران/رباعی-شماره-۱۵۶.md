---
title: >-
    رباعی شمارهٔ ۱۵۶
---
# رباعی شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>دایم نه لوای عشرت افراشتنیست</p></div>
<div class="m2"><p>پیوسته نه تخم خرمی کاشتنیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این داشتنیها همه بگذاشتنیست</p></div>
<div class="m2"><p>جز روشنی رو که نگه داشتنیست</p></div></div>