---
title: >-
    رباعی شمارهٔ ۵۶۶
---
# رباعی شمارهٔ ۵۶۶

<div class="b" id="bn1"><div class="m1"><p>ای در همه شان ذات تو پاک از شین</p></div>
<div class="m2"><p>نه در حق تو کیف توان گفت نه این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی تعقل همه غیرند و صفات</p></div>
<div class="m2"><p>ذاتت بود از روی تحقق همه عین</p></div></div>