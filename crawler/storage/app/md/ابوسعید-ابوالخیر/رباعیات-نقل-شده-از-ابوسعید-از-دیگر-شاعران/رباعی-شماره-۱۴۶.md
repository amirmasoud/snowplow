---
title: >-
    رباعی شمارهٔ ۱۴۶
---
# رباعی شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>افسوس که کس با خبر از دردم نیست</p></div>
<div class="m2"><p>آگاه ز حال چهرهٔ زردم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست برای دوستیها که مراست</p></div>
<div class="m2"><p>دریاب که تا درنگری گردم نیست</p></div></div>