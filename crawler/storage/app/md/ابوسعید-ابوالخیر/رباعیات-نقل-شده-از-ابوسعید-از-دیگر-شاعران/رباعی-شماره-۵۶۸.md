---
title: >-
    رباعی شمارهٔ ۵۶۸
---
# رباعی شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>بر ذره نشینم بچمد تختم بین</p></div>
<div class="m2"><p>موری بدو منزل ببرد رختم بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر لقمه مثل ز قرص خورشید کنم</p></div>
<div class="m2"><p>تاریکی سینه آورد بختم بین</p></div></div>