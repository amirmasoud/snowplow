---
title: >-
    رباعی شمارهٔ ۵۳
---
# رباعی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>چون نیست ز هر چه هست جز باد بدست</p></div>
<div class="m2"><p>چون هست ز هر چه نیست نقصان و شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگار که هر چه هست در عالم نیست</p></div>
<div class="m2"><p>پندار که هر چه نیست در عالم هست</p></div></div>