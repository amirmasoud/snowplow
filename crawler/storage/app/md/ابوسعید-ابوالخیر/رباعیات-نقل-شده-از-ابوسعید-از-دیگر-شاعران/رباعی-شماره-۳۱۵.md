---
title: >-
    رباعی شمارهٔ ۳۱۵
---
# رباعی شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>ای عشق به درد تو سری می‌باید</p></div>
<div class="m2"><p>صید تو ز من قوی‌تری می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من مرغ به یک شعله کبابم بگذار</p></div>
<div class="m2"><p>کین آتش را سمندری می‌باید</p></div></div>