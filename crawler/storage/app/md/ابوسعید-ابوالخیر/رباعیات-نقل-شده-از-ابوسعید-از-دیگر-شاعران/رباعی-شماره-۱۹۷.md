---
title: >-
    رباعی شمارهٔ ۱۹۷
---
# رباعی شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>فردا که به محشر اندر آید زن و مرد</p></div>
<div class="m2"><p>وز بیم حساب رویها گردد زرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من حسن ترا به کف نهم پیش روم</p></div>
<div class="m2"><p>گویم که حساب من ازین باید کرد</p></div></div>