---
title: >-
    رباعی شمارهٔ ۵۳۸
---
# رباعی شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>یا رب نظری بر من سرگردان کن</p></div>
<div class="m2"><p>لطفی بمن دلشدهٔ حیران کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من مکن آنچه من سزای آنم</p></div>
<div class="m2"><p>آنچ از کرم و لطف تو زیبد آن کن</p></div></div>