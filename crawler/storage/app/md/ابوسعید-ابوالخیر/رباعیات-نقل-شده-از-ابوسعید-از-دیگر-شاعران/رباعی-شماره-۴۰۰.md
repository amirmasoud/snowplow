---
title: >-
    رباعی شمارهٔ ۴۰۰
---
# رباعی شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>ای چارده ساله مه که در حسن و جمال</p></div>
<div class="m2"><p>همچون مه چارده رسیدی بکمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب نرسد به حسنت آسیب زوال</p></div>
<div class="m2"><p>در چارده سالگی بمانی صد سال</p></div></div>