---
title: >-
    رباعی شمارهٔ ۱۳۶
---
# رباعی شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>ای دل همه خون شوی شکیبایی چیست</p></div>
<div class="m2"><p>وی جان بدرآ اینهمه رعنایی چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده چه مردمیست شرمت بادا</p></div>
<div class="m2"><p>نادیده به حال دوست بینایی چیست</p></div></div>