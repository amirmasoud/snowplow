---
title: >-
    رباعی شمارهٔ ۲۰۷
---
# رباعی شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>گر پنهان کرد عیب و گر پیدا کرد</p></div>
<div class="m2"><p>منت دارم ازو که بس برجا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاج سر من خاک سر پای کسیست</p></div>
<div class="m2"><p>کو چشم مرا به عیب من بینا کرد</p></div></div>