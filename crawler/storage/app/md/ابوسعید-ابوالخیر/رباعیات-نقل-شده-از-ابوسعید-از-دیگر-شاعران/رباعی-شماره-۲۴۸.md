---
title: >-
    رباعی شمارهٔ ۲۴۸
---
# رباعی شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>نقاش اگر ز موی پرگار کند</p></div>
<div class="m2"><p>نقش دهن تنگ تو دشوار کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن تنگی و نازکی که دارد دهنت</p></div>
<div class="m2"><p>ترسم که نفس لب تو افگار کند</p></div></div>