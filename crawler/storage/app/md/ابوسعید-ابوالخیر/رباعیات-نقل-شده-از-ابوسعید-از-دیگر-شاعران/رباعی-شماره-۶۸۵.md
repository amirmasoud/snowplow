---
title: >-
    رباعی شمارهٔ ۶۸۵
---
# رباعی شمارهٔ ۶۸۵

<div class="b" id="bn1"><div class="m1"><p>ای آنکه دوای دردمندان دانی</p></div>
<div class="m2"><p>راز دل زار مستمندان دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال دل خویش را چه گویم با تو</p></div>
<div class="m2"><p>ناگفته تو خود هزار چندان دانی</p></div></div>