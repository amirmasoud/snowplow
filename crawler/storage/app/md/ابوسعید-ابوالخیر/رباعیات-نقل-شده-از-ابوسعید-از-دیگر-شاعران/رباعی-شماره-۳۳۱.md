---
title: >-
    رباعی شمارهٔ ۳۳۱
---
# رباعی شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>لذات جهان چشیده باشی همه عمر</p></div>
<div class="m2"><p>با یار خود آرمیده باشی همه عمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم آخر عمر رحلتت باید کرد</p></div>
<div class="m2"><p>خوابی باشد که دیده باشی همه عمر</p></div></div>