---
title: >-
    رباعی شمارهٔ ۶۴۰
---
# رباعی شمارهٔ ۶۴۰

<div class="b" id="bn1"><div class="m1"><p>از چهره همه خانه منقش کردی</p></div>
<div class="m2"><p>وز باده رخان ما چو آتش کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی و نشاط ما یکی شش کردی</p></div>
<div class="m2"><p>عیشت خوش باد عیش ما خوش کردی</p></div></div>