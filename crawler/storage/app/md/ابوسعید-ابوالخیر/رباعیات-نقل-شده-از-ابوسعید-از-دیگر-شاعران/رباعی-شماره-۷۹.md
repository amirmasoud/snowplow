---
title: >-
    رباعی شمارهٔ ۷۹
---
# رباعی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>نقاش رخت ز طعنها آسودست</p></div>
<div class="m2"><p>کز هر چه تمام‌تر بود بنمودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخسار و لبت چنانکه باید بودست</p></div>
<div class="m2"><p>گویی که کسی به آرزو فرمودست</p></div></div>