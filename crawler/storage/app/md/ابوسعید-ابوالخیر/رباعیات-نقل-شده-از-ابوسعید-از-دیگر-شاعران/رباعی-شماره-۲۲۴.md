---
title: >-
    رباعی شمارهٔ ۲۲۴
---
# رباعی شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>از شبنم عشق خاک آدم گل شد</p></div>
<div class="m2"><p>شوری برخاست فتنه‌ای حاصل شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر نشتر عشق بر رگ روح زدند</p></div>
<div class="m2"><p>یک قطرهٔ خون چکید و نامش دل شد</p></div></div>