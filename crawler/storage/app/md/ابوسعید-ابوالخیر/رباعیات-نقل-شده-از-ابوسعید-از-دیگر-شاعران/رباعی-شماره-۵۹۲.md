---
title: >-
    رباعی شمارهٔ ۵۹۲
---
# رباعی شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>ما را نبود دلی که کار آید ازو</p></div>
<div class="m2"><p>جز ناله که هر دمی هزار آید ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان گریم که کوچه‌ها گل گردد</p></div>
<div class="m2"><p>نی روید و ناله‌های زار آید ازو</p></div></div>