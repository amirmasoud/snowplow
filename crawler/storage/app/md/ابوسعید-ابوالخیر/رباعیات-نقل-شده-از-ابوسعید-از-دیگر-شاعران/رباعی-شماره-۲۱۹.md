---
title: >-
    رباعی شمارهٔ ۲۱۹
---
# رباعی شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>حورا به نظارهٔ نگارم صف زد</p></div>
<div class="m2"><p>رضوان بعجب بماند و کف بر کف زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن خال سیه بر آن رخ مطرف زد</p></div>
<div class="m2"><p>ابدال زبیم چنگ در مصحف زد</p></div></div>