---
title: >-
    رباعی شمارهٔ ۳۸۲
---
# رباعی شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>کی باشد و کی لباس هستی شده شق</p></div>
<div class="m2"><p>تابان گشته جمال وجه مطلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل در سطوات نور او مستهلک</p></div>
<div class="m2"><p>جان در غلبات شوق او مستغرق</p></div></div>