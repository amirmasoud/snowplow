---
title: >-
    رباعی شمارهٔ ۲۷۹
---
# رباعی شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>دوشم به طرب بود نه دلتنگی بود</p></div>
<div class="m2"><p>سیرم همه در عالم یکرنگی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌رفتم اگرچه از سر لنگی بود</p></div>
<div class="m2"><p>من بودم و سنگ من دو من سنگی بود</p></div></div>