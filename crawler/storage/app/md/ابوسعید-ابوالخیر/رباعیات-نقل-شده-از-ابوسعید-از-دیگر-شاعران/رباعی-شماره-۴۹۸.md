---
title: >-
    رباعی شمارهٔ ۴۹۸
---
# رباعی شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>ای دوست ترا به جملگی گشتم من</p></div>
<div class="m2"><p>حقا که درین سخن نه زرقست و نه فن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو زوجود خود برون جستی پاک</p></div>
<div class="m2"><p>شاید صنما به جای تو هستم من</p></div></div>