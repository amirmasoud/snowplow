---
title: >-
    رباعی شمارهٔ ۵۰۳
---
# رباعی شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>چندین چه زنی نظاره گرد میدان</p></div>
<div class="m2"><p>اینجا دم اژدهاست و زخم پیلان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هر که در آید بنهد او دل و جان</p></div>
<div class="m2"><p>فارغ چه کند گرد سرای سلطان</p></div></div>