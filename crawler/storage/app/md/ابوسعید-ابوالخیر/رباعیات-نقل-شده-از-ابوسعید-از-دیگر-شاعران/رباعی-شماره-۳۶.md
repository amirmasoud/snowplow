---
title: >-
    رباعی شمارهٔ ۳۶
---
# رباعی شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای آینه حسن تو در صورت زیب</p></div>
<div class="m2"><p>گرداب هزار کشتی صبر و شکیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آینه‌ای که غیر حسن تو بود</p></div>
<div class="m2"><p>خواند خردش سراب صحرای فریب</p></div></div>