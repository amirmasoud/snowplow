---
title: >-
    رباعی شمارهٔ ۵۷۱
---
# رباعی شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>ای آینه را داده جلا صورت تو</p></div>
<div class="m2"><p>یک آینه کس ندید بی صورت تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی که ز لطف در همه آینه‌ها</p></div>
<div class="m2"><p>خود آمده‌ای به دیدن صورت تو</p></div></div>