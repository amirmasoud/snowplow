---
title: >-
    رباعی شمارهٔ ۲۳۸
---
# رباعی شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>در تکیه قلندران چو بنگم دادند</p></div>
<div class="m2"><p>در کاسه بجای لوت سنگم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ز چه روی خاست این خواری ما</p></div>
<div class="m2"><p>ریشم بگرفتند و به چنگم دادند</p></div></div>