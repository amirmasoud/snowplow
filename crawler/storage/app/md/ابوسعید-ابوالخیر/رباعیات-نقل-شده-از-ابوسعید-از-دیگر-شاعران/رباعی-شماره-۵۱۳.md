---
title: >-
    رباعی شمارهٔ ۵۱۳
---
# رباعی شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>دارم گله از درد نه چندان چندان</p></div>
<div class="m2"><p>با گریه توان گفت نه خندان خندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در و گهرم جمله بتاراج برفت</p></div>
<div class="m2"><p>آن در و گهر چه بود دندان دندان</p></div></div>