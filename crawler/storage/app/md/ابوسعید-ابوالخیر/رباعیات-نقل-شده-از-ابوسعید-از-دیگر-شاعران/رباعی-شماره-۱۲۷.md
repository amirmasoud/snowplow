---
title: >-
    رباعی شمارهٔ ۱۲۷
---
# رباعی شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>تا در نرسد وعدهٔ هر کار که هست</p></div>
<div class="m2"><p>سودی ندهد یاری هر یار که هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زحمت سرمای زمستان نکشد</p></div>
<div class="m2"><p>پر گل نشود دامن هر خار که هست</p></div></div>