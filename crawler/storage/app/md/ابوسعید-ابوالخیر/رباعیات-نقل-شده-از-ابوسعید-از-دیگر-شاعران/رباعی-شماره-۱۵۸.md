---
title: >-
    رباعی شمارهٔ ۱۵۸
---
# رباعی شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>سیمابی شد هوا و زنگاری دشت</p></div>
<div class="m2"><p>ای دوست بیا و بگذر از هرچه گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر میل وفا داری اینک دل و جان</p></div>
<div class="m2"><p>ور رای جفا داری اینک سر و تشت</p></div></div>