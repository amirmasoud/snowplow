---
title: >-
    رباعی شمارهٔ ۲۶۴
---
# رباعی شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>دشمن چو به ما درنگرد بد بیند</p></div>
<div class="m2"><p>عیبی که بر ماست یکی صد بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما آینه‌ایم، هر که در ما نگرد</p></div>
<div class="m2"><p>هر نیک و بدی که بیند از خود بیند</p></div></div>