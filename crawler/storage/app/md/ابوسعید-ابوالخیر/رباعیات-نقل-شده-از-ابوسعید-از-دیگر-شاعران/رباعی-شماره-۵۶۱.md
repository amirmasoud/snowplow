---
title: >-
    رباعی شمارهٔ ۵۶۱
---
# رباعی شمارهٔ ۵۶۱

<div class="b" id="bn1"><div class="m1"><p>فریاد ز دست فلک آینه گون</p></div>
<div class="m2"><p>کز جور و جفای او جگر دارم خون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی به هزار غم به شب می‌آرم</p></div>
<div class="m2"><p>تا خود فلک از پرده‌چه آرد بیرون</p></div></div>