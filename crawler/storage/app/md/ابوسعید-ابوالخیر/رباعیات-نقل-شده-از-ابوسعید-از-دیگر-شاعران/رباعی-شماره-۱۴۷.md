---
title: >-
    رباعی شمارهٔ ۱۴۷
---
# رباعی شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>گفتار نکو دارم و کردارم نیست</p></div>
<div class="m2"><p>از گفت نکوی بی عمل عارم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشوار بود کردن و گفتن آسان</p></div>
<div class="m2"><p>آسان بسیار و هیچ دشوارم نیست</p></div></div>