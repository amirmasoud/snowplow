---
title: >-
    رباعی شمارهٔ ۵۲۰
---
# رباعی شمارهٔ ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>بحریست وجود جاودان موج زنان</p></div>
<div class="m2"><p>زان بحر ندیده غیر موج اهل جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باطن بحر موج بین گشته عیان</p></div>
<div class="m2"><p>بر ظاهر بحر و بحر در موج نهان</p></div></div>