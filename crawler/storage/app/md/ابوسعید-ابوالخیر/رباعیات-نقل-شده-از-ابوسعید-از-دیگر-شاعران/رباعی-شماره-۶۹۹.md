---
title: >-
    رباعی شمارهٔ ۶۹۹
---
# رباعی شمارهٔ ۶۹۹

<div class="b" id="bn1"><div class="m1"><p>گر در یمنی چو با منی پیش منی</p></div>
<div class="m2"><p>گر پیش منی چو بی منی در یمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با تو چنانم ای نگار یمنی</p></div>
<div class="m2"><p>خود در غلطم که من توام یا تو منی</p></div></div>