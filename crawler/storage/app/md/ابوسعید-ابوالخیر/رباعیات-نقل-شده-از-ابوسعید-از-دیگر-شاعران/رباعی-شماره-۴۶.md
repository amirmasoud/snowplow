---
title: >-
    رباعی شمارهٔ ۴۶
---
# رباعی شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>از کفر سر زلف وی ایمان میریخت</p></div>
<div class="m2"><p>وز نوش لبش چشمهٔ حیوان میریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کبک خرامنده بصد رعنایی</p></div>
<div class="m2"><p>میرفت و ز خاک قدمش جان میریخت</p></div></div>