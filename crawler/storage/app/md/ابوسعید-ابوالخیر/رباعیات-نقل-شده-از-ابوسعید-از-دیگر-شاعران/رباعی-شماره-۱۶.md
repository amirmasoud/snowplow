---
title: >-
    رباعی شمارهٔ ۱۶
---
# رباعی شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای دوست دوا فرست بیماران را</p></div>
<div class="m2"><p>روزی ده جن و انس و هم یاران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما تشنه لبان وادی حرمانیم</p></div>
<div class="m2"><p>بر کشت امید ما بده باران را</p></div></div>