---
title: >-
    رباعی شمارهٔ ۵۰۶
---
# رباعی شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>فریاد و فغان که باز در کوی مغان</p></div>
<div class="m2"><p>می‌خواره ز می نه نام یابد نه نشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانگونه نهان گشت که بر خلق جهان</p></div>
<div class="m2"><p>گشتست نهان گشتن او نیز نهان</p></div></div>