---
title: >-
    رباعی شمارهٔ ۳۴۲
---
# رباعی شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>ای فضل تو دستگیر من، دستم گیر</p></div>
<div class="m2"><p>سیر آمده‌ام ز خویشتن، دستم گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند کنم توبه و تا کی شکنم</p></div>
<div class="m2"><p>ای توبه ده و توبه شکن، دستم گیر</p></div></div>