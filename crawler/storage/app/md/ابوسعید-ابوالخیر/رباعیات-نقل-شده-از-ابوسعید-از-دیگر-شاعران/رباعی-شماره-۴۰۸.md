---
title: >-
    رباعی شمارهٔ ۴۰۸
---
# رباعی شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>با اهل زمانه آشنایی مشکل</p></div>
<div class="m2"><p>با چرخ کهن ستیزه رایی مشکل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جان و جهان قطع نمودن آسان</p></div>
<div class="m2"><p>در هم زدن دل به جدایی مشکل</p></div></div>