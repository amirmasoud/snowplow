---
title: >-
    رباعی شمارهٔ ۴۳۰
---
# رباعی شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>گر خلق چنانکه من منم دانندم</p></div>
<div class="m2"><p>همچون سگ ز در بدر رانندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور زانکه درون برون بگردانندم</p></div>
<div class="m2"><p>مستوجب آنم که بسوزانندم</p></div></div>