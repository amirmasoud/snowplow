---
title: >-
    رباعی شمارهٔ ۵۴۸
---
# رباعی شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>ای چشم من از دیدن رویت روشن</p></div>
<div class="m2"><p>از دیدن رویت شده خرم دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویت شده گل، خرم و خندان گشته</p></div>
<div class="m2"><p>روشن مه من گشته ز رویت دل من</p></div></div>