---
title: >-
    رباعی شمارهٔ ۶۲۰
---
# رباعی شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>چون باز سفید در شکاریم همه</p></div>
<div class="m2"><p>با نفس و هوای نفس یاریم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پرده ز روی کارها بر گیرند</p></div>
<div class="m2"><p>معلوم شود که در چه کاریم همه</p></div></div>