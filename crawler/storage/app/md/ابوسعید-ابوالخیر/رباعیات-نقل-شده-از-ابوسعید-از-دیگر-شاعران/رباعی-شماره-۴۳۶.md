---
title: >-
    رباعی شمارهٔ ۴۳۶
---
# رباعی شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>در کوی تو من سوخته دامن بودم</p></div>
<div class="m2"><p>وز آتش غم سوخته خرمن بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری جانا دوش به بامت بودم</p></div>
<div class="m2"><p>گفتی دزدست دزد نبد من بودم</p></div></div>