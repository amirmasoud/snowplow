---
title: >-
    رباعی شمارهٔ ۴۱۲
---
# رباعی شمارهٔ ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>از گردش افلاک و نفاق انجم</p></div>
<div class="m2"><p>سر رشتهٔ کار خویشتن کردم گم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پای فتاده‌ام مرا دست بگیر</p></div>
<div class="m2"><p>ای قبلهٔ هفتم ای امام هشتم</p></div></div>