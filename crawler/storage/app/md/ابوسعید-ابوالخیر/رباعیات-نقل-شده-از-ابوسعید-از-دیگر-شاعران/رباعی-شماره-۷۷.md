---
title: >-
    رباعی شمارهٔ ۷۷
---
# رباعی شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>ای حیدر شهسوار وقت مددست</p></div>
<div class="m2"><p>ای زبدهٔ هشت و چار وقت مددست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من عاجزم از جهان و دشمن بسیار</p></div>
<div class="m2"><p>ای صاحب ذوالفقار وقت مددست</p></div></div>