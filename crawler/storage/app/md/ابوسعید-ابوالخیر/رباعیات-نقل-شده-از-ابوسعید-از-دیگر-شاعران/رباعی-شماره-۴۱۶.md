---
title: >-
    رباعی شمارهٔ ۴۱۶
---
# رباعی شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>گر من گنه جمله جهان کردستم</p></div>
<div class="m2"><p>عفو تو امیدست که گیرد دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که به روز عجز دستت گیرم</p></div>
<div class="m2"><p>عاجزتر ازین مخواه کاکنون هستم</p></div></div>