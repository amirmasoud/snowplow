---
title: >-
    رباعی شمارهٔ ۱۸۵
---
# رباعی شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>گر درد کند پای تو ای حور نژاد</p></div>
<div class="m2"><p>از درد بدان که هر گزت درد مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دردمنست بر منش رحم آید</p></div>
<div class="m2"><p>از بهر شفاعتم بپای تو فتاد</p></div></div>