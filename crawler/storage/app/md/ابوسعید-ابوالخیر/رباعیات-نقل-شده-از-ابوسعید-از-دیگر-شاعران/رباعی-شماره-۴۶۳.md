---
title: >-
    رباعی شمارهٔ ۴۶۳
---
# رباعی شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>با چشم تو یاد نرگس‌تر نکنم</p></div>
<div class="m2"><p>بی‌لعل تو آرزوی کوثر نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خضر به من بی تو دهد آب حیات</p></div>
<div class="m2"><p>کافر باشم که بی تو لب تر نکنم</p></div></div>