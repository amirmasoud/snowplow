---
title: >-
    رباعی شمارهٔ ۲۶۱
---
# رباعی شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>مردان رهش میل به هستی نکنند</p></div>
<div class="m2"><p>خودبینی و خویشتن پرستی نکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که مجردان حق می نوشند</p></div>
<div class="m2"><p>خم خانه تهی کنند و مستی نکنند</p></div></div>