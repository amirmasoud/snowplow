---
title: >-
    رباعی شمارهٔ ۹۹
---
# رباعی شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>کردیم هر آن حیله که عقل آن دانست</p></div>
<div class="m2"><p>تا بو که توان راه به جانان دانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره می‌نبریم وهم طمع می‌نبریم</p></div>
<div class="m2"><p>نتوان دانست بو که نتوان دانست</p></div></div>