---
title: >-
    رباعی شمارهٔ ۳۱۰
---
# رباعی شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>در باغ روم کوی توام یاد آید</p></div>
<div class="m2"><p>بر گل نگرم روی توام یاد آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سایهٔ سرو اگر دمی بنشینم</p></div>
<div class="m2"><p>سرو قد دلجوی توام یاد آید</p></div></div>