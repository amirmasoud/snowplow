---
title: >-
    رباعی شمارهٔ ۲۲۲
---
# رباعی شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>این گیدی گبر از کجا پیدا شد</p></div>
<div class="m2"><p>این صورت قبر از کجا پیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید مرا ز دیده‌ام پنهان کرد</p></div>
<div class="m2"><p>این لکهٔ ابر از کجا پیدا شد</p></div></div>