---
title: >-
    رباعی شمارهٔ ۳۳۹
---
# رباعی شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>در بزم تو ای شوخ منم زار و اسیر</p></div>
<div class="m2"><p>وز کشتن من هیچ نداری تقصیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با غیر سخن گویی کز رشک بسوز</p></div>
<div class="m2"><p>سویم نکنی نگه که از غصه بمیر</p></div></div>