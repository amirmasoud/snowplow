---
title: >-
    رباعی شمارهٔ ۶۰۲
---
# رباعی شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>اینک سر کوی دوست اینک سر راه</p></div>
<div class="m2"><p>گر تو نروی روندگان را چه گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامه چه کنی کبود و نیلی و سیاه</p></div>
<div class="m2"><p>دل صاف کن و قبا همی پوش و کلاه</p></div></div>