---
title: >-
    رباعی شمارهٔ ۴۵۳
---
# رباعی شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>عیبم مکن ای خواجه اگر می نوشم</p></div>
<div class="m2"><p>در عاشقی و باده پرستی کوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هشیارم نشسته با اغیارم</p></div>
<div class="m2"><p>چون بی‌هوشم به یار هم آغوشم</p></div></div>