---
title: >-
    رباعی شمارهٔ ۲۳۴
---
# رباعی شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>زان پیش که طاق چرخ اعلا زده‌اند</p></div>
<div class="m2"><p>وین بارگه سپهر مینا زده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما در عدم آباد ازل خوش خفته</p></div>
<div class="m2"><p>بی ما رقم عشق تو بر ما زده‌اند</p></div></div>