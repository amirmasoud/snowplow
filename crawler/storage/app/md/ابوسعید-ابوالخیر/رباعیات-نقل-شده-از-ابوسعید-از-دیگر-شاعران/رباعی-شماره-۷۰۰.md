---
title: >-
    رباعی شمارهٔ ۷۰۰
---
# رباعی شمارهٔ ۷۰۰

<div class="b" id="bn1"><div class="m1"><p>از سادگی و سلیمی و مسکینی</p></div>
<div class="m2"><p>وز سرکشی و تکبر و خود بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آتش اگر نشانیم بنشینم</p></div>
<div class="m2"><p>بر دیده اگر نشانمت ننشینی</p></div></div>