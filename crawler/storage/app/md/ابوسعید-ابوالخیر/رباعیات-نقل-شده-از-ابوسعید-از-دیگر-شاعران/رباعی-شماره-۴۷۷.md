---
title: >-
    رباعی شمارهٔ ۴۷۷
---
# رباعی شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>دارم ز خدا خواهش جنات نعیم</p></div>
<div class="m2"><p>زاهد به ثواب و من به امید عظیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دست تهی میروم او تحفه به دست</p></div>
<div class="m2"><p>تا زین دو کدام خوش کند طبع کریم</p></div></div>