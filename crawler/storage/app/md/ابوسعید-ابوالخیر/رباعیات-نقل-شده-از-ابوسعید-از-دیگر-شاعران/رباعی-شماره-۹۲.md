---
title: >-
    رباعی شمارهٔ ۹۲
---
# رباعی شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>دل رفت بر کسیکه سیماش خوشست</p></div>
<div class="m2"><p>غم خوش نبود ولیک غمهاش خوشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان میطلبد نمیدهم روزی چند</p></div>
<div class="m2"><p>در جان سخنی نیست، تقاضاش خوشست</p></div></div>