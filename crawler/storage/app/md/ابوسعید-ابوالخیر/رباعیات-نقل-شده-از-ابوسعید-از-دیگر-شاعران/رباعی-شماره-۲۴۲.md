---
title: >-
    رباعی شمارهٔ ۲۴۲
---
# رباعی شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>مردان خدا ز خاکدان دگرند</p></div>
<div class="m2"><p>مرغان هوا ز آشیان دگرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منگر تو ازین چشم بدیشان کایشان</p></div>
<div class="m2"><p>فارغ ز دو کون و در مکان دگرند</p></div></div>