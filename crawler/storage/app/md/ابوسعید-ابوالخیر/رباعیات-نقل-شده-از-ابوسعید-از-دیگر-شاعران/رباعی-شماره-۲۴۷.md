---
title: >-
    رباعی شمارهٔ ۲۴۷
---
# رباعی شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>عاشق همه دم فکر غم دوست کند</p></div>
<div class="m2"><p>معشوق کرشمه‌ای که نیکوست کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما جرم و گنه کنیم و او لطف و کرم</p></div>
<div class="m2"><p>هر کس چیزی که لایق اوست کند</p></div></div>