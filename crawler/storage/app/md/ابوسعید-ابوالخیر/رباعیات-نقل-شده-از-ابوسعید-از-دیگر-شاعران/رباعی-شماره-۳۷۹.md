---
title: >-
    رباعی شمارهٔ ۳۷۹
---
# رباعی شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>پیوسته مرا ز خالق جسم و عرض</p></div>
<div class="m2"><p>حقا که همین بود و همینست غرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان جسم لطیف را به خلوتگه ناز</p></div>
<div class="m2"><p>فارغ بینم همیشه ز آسیب مرض</p></div></div>