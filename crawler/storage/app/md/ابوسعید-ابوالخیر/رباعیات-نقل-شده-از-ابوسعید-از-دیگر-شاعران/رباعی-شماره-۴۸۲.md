---
title: >-
    رباعی شمارهٔ ۴۸۲
---
# رباعی شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>هر چند زکار خود خبردار نه‌ایم</p></div>
<div class="m2"><p>بیهوده تماشاگر گلزار نه‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حاشیهٔ کتاب چون نقطهٔ شک</p></div>
<div class="m2"><p>بی کارنه‌ایم اگر چه در کار نه‌ایم</p></div></div>