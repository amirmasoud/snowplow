---
title: >-
    رباعی شمارهٔ ۱۲۳
---
# رباعی شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>ای دوست ای دوست ای دوست ای دوست</p></div>
<div class="m2"><p>جور تو از آنکشم که روی تو نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم گویند بهشت خواهی یا دوست</p></div>
<div class="m2"><p>ای بیخبران بهشت با دوست نکوست</p></div></div>