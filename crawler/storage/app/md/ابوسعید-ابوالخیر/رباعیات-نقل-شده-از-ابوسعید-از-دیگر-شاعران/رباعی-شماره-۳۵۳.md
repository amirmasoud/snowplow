---
title: >-
    رباعی شمارهٔ ۳۵۳
---
# رباعی شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>جهدی بکن ار پند پذیری دو سه روز</p></div>
<div class="m2"><p>تا پیشتر از مرگ بمیری دو سه روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنیا زن پیریست چه باشد ار تو</p></div>
<div class="m2"><p>با پیر زنی انس نگیری دو سه روز</p></div></div>