---
title: >-
    رباعی شمارهٔ ۵۸۷
---
# رباعی شمارهٔ ۵۸۷

<div class="b" id="bn1"><div class="m1"><p>ای در دل و جان صورت و معنی همه تو</p></div>
<div class="m2"><p>مقصود همه زدین و دنیی همه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم با همه همدمی و هم بی همه تو</p></div>
<div class="m2"><p>ای با همه تو بی همه تو نی همه تو</p></div></div>