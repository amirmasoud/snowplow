---
title: >-
    رباعی شمارهٔ ۲۱۸
---
# رباعی شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>من صرفه برم که بر صفم اعدا زد</p></div>
<div class="m2"><p>مشتی خاک لطمه بر دریا زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما تیغ برهنه‌ایم در دست قضا</p></div>
<div class="m2"><p>شد کشته هر آنکه خویش را بر ما زد</p></div></div>