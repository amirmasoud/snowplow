---
title: >-
    رباعی شمارهٔ ۴۳۴
---
# رباعی شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>هرگز نبود شکست کس مقصودم</p></div>
<div class="m2"><p>آزرده نشد دلی ز من تا بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد شکر که چشم عیب بینم کورست</p></div>
<div class="m2"><p>شادم که حسود نیستم محسودم</p></div></div>