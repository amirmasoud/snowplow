---
title: >-
    رباعی شمارهٔ ۳۲۷
---
# رباعی شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>یا رب در دل به غیر خود جا مگذار</p></div>
<div class="m2"><p>در دیدهٔ من گرد تمنا مگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم گفتم ز من نمی‌آید هیچ</p></div>
<div class="m2"><p>رحمی رحمی مرا به من وامگذار</p></div></div>