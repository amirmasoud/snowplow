---
title: >-
    رباعی شمارهٔ ۱۳۳
---
# رباعی شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>عاشق نتواند که دمی بی غم زیست</p></div>
<div class="m2"><p>بی یار و دیار اگر بود خود غم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آنکه بیک کرشمه جان کرد نثار</p></div>
<div class="m2"><p>هجران و وصال را ندانست که چیست</p></div></div>