---
title: >-
    رباعی شمارهٔ ۲۲۷
---
# رباعی شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>از لطف تو هیچ بنده نومید نشد</p></div>
<div class="m2"><p>مقبول تو جز مقبل جاوید نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهرت بکدام ذره پیوست دمی</p></div>
<div class="m2"><p>کان ذره به از هزار خورشید نشد</p></div></div>