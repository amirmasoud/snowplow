---
title: >-
    رباعی شمارهٔ ۴۴۶
---
# رباعی شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>از خاک درت رخت اقامت نبرم</p></div>
<div class="m2"><p>وز دست غمت جان به سلامت نبرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردار نقاب از رخ و بنمای جمال</p></div>
<div class="m2"><p>تا حسرت آن رخ به قیامت نبرم</p></div></div>