---
title: >-
    رباعی شمارهٔ ۵۴
---
# رباعی شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>دی طفلک خاک بیز غربال بدست</p></div>
<div class="m2"><p>میزد بدو دست و روی خود را می‌خست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میگفت به های‌های کافسوس و دریغ</p></div>
<div class="m2"><p>دانگی بنیافتیم و غربال شکست</p></div></div>