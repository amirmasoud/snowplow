---
title: >-
    رباعی شمارهٔ ۱۶۴
---
# رباعی شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>سر سخن دوست نمی‌یارم گفت</p></div>
<div class="m2"><p>در یست گرانبها نمی‌یارم سفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که به خواب در بگویم بکسی</p></div>
<div class="m2"><p>شبهاست کزین بیم نمی‌یارم خفت</p></div></div>