---
title: >-
    رباعی شمارهٔ ۳۴۰
---
# رباعی شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>شمشیر بود ابروی آن بدر منیر</p></div>
<div class="m2"><p>و آن دیده به خون خوردن چستست چو شیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یک سو شیر و از دگر سو شمشیر</p></div>
<div class="m2"><p>مسکین دل من میان شیر و شمشیر</p></div></div>