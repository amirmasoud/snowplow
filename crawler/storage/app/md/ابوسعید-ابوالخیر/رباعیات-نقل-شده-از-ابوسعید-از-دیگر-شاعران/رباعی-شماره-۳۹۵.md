---
title: >-
    رباعی شمارهٔ ۳۹۵
---
# رباعی شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>درماند کسی که بست در خوبان دل</p></div>
<div class="m2"><p>وز مهر بتان نگشت پیوند گسل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صورت گل معنی جان دید و بماند</p></div>
<div class="m2"><p>پای دل او تا به قیامت در گل</p></div></div>