---
title: >-
    تکه ۲
---
# تکه ۲

<div class="b" id="bn1"><div class="m1"><p>مرد باید که جگر سوخته چندان بودا</p></div>
<div class="m2"><p>نه همانا که چنین مرد فراوان بودا</p></div></div>