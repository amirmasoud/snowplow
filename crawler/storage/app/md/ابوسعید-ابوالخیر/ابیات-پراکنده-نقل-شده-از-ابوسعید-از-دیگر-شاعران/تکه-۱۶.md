---
title: >-
    تکه ۱۶
---
# تکه ۱۶

<div class="b" id="bn1"><div class="m1"><p>می هست و درم هست و بت لاله رخان هست</p></div>
<div class="m2"><p>غم نیست و گر هست نصیب دل اعداست</p></div></div>