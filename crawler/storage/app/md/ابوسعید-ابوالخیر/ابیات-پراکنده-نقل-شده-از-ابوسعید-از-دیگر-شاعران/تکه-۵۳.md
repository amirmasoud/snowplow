---
title: >-
    تکه ۵۳
---
# تکه ۵۳

<div class="b" id="bn1"><div class="m1"><p>با عاشقان نشین و همه عاشقی گزین</p></div>
<div class="m2"><p>با هر که نیست عاشق کم گوی و کم نشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد که در وصال تو بینند روی دوست</p></div>
<div class="m2"><p>تو نیز در میانهٔ ایشان نه‌ای ببین</p></div></div>