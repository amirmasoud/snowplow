---
title: >-
    تکه ۳
---
# تکه ۳

<div class="b" id="bn1"><div class="m1"><p>کار چون بسته شود بگشایدا</p></div>
<div class="m2"><p>وز پس هر غم طرب افزایدا</p></div></div>