---
title: >-
    تکه ۵
---
# تکه ۵

<div class="b" id="bn1"><div class="m1"><p>به حق هر دو گیسوی محمد</p></div>
<div class="m2"><p>زبون گردان زبردستان ما را</p></div></div>