---
title: >-
    تکه ۴۹
---
# تکه ۴۹

<div class="b" id="bn1"><div class="m1"><p>مدتی هست که ما از خم وحدت مستیم</p></div>
<div class="m2"><p>شیشهٔ کثرت این طایفه را بشکستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینکه گویند فنا هست غلط میگویند</p></div>
<div class="m2"><p>تا خدا هست درین معرکه ما هم هستیم</p></div></div>