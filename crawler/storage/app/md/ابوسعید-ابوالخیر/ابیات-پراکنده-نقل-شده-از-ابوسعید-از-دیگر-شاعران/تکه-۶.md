---
title: >-
    تکه ۶
---
# تکه ۶

<div class="b" id="bn1"><div class="m1"><p>نسیما جانب بستان گذر کن</p></div>
<div class="m2"><p>بگو آن نازنین شمشاد ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تشریف قدوم خود زمانی</p></div>
<div class="m2"><p>مشرف کن خراب آباد ما را</p></div></div>