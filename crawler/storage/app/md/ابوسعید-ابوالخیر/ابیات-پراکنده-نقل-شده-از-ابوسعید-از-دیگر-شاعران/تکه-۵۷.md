---
title: >-
    تکه ۵۷
---
# تکه ۵۷

<div class="b" id="bn1"><div class="m1"><p>حال عالم سر بسر پرسیدم از فرزانه‌ای</p></div>
<div class="m2"><p>گفت: یا خاکیست یا بادیست یا افسانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش، آن کس که او اندر طلب پویان بود؟</p></div>
<div class="m2"><p>گفت: یا کوریست یا کریست یا دیوانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش: احوال عمر ما چه باشد عمر چیست؟</p></div>
<div class="m2"><p>گفت: یا برقیست یا شمعیست یا پروانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر مثال قطرهٔ برفست در فصل تموز</p></div>
<div class="m2"><p>هیچ عاقل در چنین جاگاه سازد خانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا مثال سیل خانست آب در فصل بهار</p></div>
<div class="m2"><p>هیچ زیرک در چنین منزل فشاند دانه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیلسوفی گفت: اندر جانب هندوستان</p></div>
<div class="m2"><p>حکمتی دیدم نوشته بر در بت خانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم: آن حکمت چه حکمت بود؟ گفت: این حکمتست</p></div>
<div class="m2"><p>آدمی را سنگ و شیشه چرخ چون دیوانه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نعمت دنیا و دنیا نزد حق بیگانه است</p></div>
<div class="m2"><p>هیچ عاقل مهر ورزد با چنین بیگانه‌ای؟</p></div></div>