---
title: >-
    تکه ۸
---
# تکه ۸

<div class="b" id="bn1"><div class="m1"><p>گر من این دوستی تو ببرم تا لب گور</p></div>
<div class="m2"><p>بزنم نعره ولیکن ز تو بینم هنرا</p></div></div>