---
title: >-
    تکه ۲۶
---
# تکه ۲۶

<div class="b" id="bn1"><div class="m1"><p>آری چنین کنند کریمان که شاه کرد</p></div>
<div class="m2"><p>سوی رهی بچشم بزرگی نگاه کرد</p></div></div>