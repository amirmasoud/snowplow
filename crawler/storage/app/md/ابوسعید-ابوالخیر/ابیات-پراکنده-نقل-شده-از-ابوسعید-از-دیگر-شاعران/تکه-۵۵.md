---
title: >-
    تکه ۵۵
---
# تکه ۵۵

<div class="b" id="bn1"><div class="m1"><p>بر فلک بر دو مرد پیشه ورند</p></div>
<div class="m2"><p>آن یکی درزی آن دگر جولاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این ندوزد مگر قبای ملوک</p></div>
<div class="m2"><p>و آن نبافد مگر گلیم سیاه</p></div></div>