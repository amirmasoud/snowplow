---
title: >-
    تکه ۴۰
---
# تکه ۴۰

<div class="b" id="bn1"><div class="m1"><p>بده تو بار خدایا درین خجسته سفر</p></div>
<div class="m2"><p>هزار نصرت و شادی هزار فتح و ظفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حق چار محمد به حق چار علی</p></div>
<div class="m2"><p>بدو حسن به حسین و به موسی و جعفر</p></div></div>