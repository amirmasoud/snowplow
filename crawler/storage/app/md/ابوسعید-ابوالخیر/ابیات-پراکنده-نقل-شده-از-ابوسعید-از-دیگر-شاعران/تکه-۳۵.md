---
title: >-
    تکه ۳۵
---
# تکه ۳۵

<div class="b" id="bn1"><div class="m1"><p>بزیر قبهٔ تقدیس مست مستانند</p></div>
<div class="m2"><p>که هر چه هست همه صورت خدا دانند</p></div></div>