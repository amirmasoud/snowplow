---
title: >-
    بخش ۲۷ - اندر باز آمدن دایه به نزدیک رامین به باغ
---
# بخش ۲۷ - اندر باز آمدن دایه به نزدیک رامین به باغ

<div class="b" id="bn1"><div class="m1"><p>چو سر بر زد ز خاور روز دیگر</p></div>
<div class="m2"><p>خور تابان چو روی دلبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جای و عده گه شد باز دایه</p></div>
<div class="m2"><p>نشستند او و رامین زیر سایه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرُو را دید رامین سخت حرّم</p></div>
<div class="m2"><p>چو کشتی خشک گشته یافته نم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفت ای سزاوار فزونی</p></div>
<div class="m2"><p>نگویی تا خود از دی باز چونی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو شادی زانکه روی ویس دیدی</p></div>
<div class="m2"><p>ز نوشین لب سخن نوشین شنیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنک چشمی که بیند روی آن ماه</p></div>
<div class="m2"><p>خنک مغزی که یابد بوی آن ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خنک چشم و دلت را با چنان روی</p></div>
<div class="m2"><p>خنک همسایگانت را در آن کوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس آنگه گفت چونست آن نگارین</p></div>
<div class="m2"><p>که کهتر باد پیشش جان رامین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسانیدی بدو پیغام زارم</p></div>
<div class="m2"><p>مرُو را یاد کردی حال و کارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پاسخ دایه گفت ای شیر جنگی</p></div>
<div class="m2"><p>شکیبا باش در مهر و درنگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که نتوان برد مستی را ز مستان</p></div>
<div class="m2"><p>گشادن بند سرما از زمستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمین را از گلاب و گل بشستن</p></div>
<div class="m2"><p>بدو بر باد و دریا را ببستن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل ویسه به دام اندر کشیدن</p></div>
<div class="m2"><p>ز مهر مادر و ویر بریدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلش زان بند دیرین بر گشادن</p></div>
<div class="m2"><p>ز نو بند دگر بر وی نهادن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدانم هر چه گفتی آن پیامم</p></div>
<div class="m2"><p>بجوشید و به زشتی برد نامم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ندادش پاسخ و با من بر آشفت</p></div>
<div class="m2"><p>چنین گفت و چنین گفت و چنین گفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو رامین هر چه دایه گفت بشنید</p></div>
<div class="m2"><p>به چشمش روز روشن تیره گردید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مر و را گفت مردان جهان پاک</p></div>
<div class="m2"><p>نه یکسر بی وفا باشند و بی باک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نباشد هر کسی را تن پر آهو</p></div>
<div class="m2"><p>نباشد هر کسی را دل به یک خو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه هر خر را به چوبی راند باید</p></div>
<div class="m2"><p>نه هر کس را به نامی خواند باید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر او دیدست راه زشت کیشان</p></div>
<div class="m2"><p>مرا نشمرد باید هم ز ایشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گناهی را که من هرگز نکردم</p></div>
<div class="m2"><p>به دل در زو گمانی هم نبردم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه باید کرد بیهوده ملامت</p></div>
<div class="m2"><p>نه خوب آید ملامت بر سلامت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیام من نگو آن سیمتن را</p></div>
<div class="m2"><p>شکسته زلفکان پر شکن را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگو ماها نگارا حور چشما</p></div>
<div class="m2"><p>پری رویا بهارا تیز خشما</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به مهر اندر بپیوند آشنایی</p></div>
<div class="m2"><p>مبر بر من گناه بی وفایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که من با تو خورم صد گونه سوگند</p></div>
<div class="m2"><p>کنم با تو بدان سوگند پیوند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که دارم تا زیم پیمان مهرت</p></div>
<div class="m2"><p>نیاهنجم سر از فرمان مهرت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی تا جان من باشد تن آرای</p></div>
<div class="m2"><p>بدو با جان من مهر تو بر جای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نفر موشم ز دل یاد تو هرگز</p></div>
<div class="m2"><p>نه روز رام نه روز هزاهز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفت این و ز نرگس اشک چون مل</p></div>
<div class="m2"><p>فرو بارید بر دو خرمن گل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو گفتی دیدگانش در فشان کرد</p></div>
<div class="m2"><p>بدان مهری که اندر دل نهان کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل دایه بدان بیدل ببخضود</p></div>
<div class="m2"><p>کجا از بیدلی بخضودنی بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو گفت ای مرا چون چشم روشن</p></div>
<div class="m2"><p>به مهر اندر بپوش از صبر جوشن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز گریه عشق را رسوایی آمد</p></div>
<div class="m2"><p>ز رسوایی ترا شیدایی آمد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به جای ویس اگر خواهی روانم</p></div>
<div class="m2"><p>ترا بخشم ز بخشش در نمایم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شوم با آن صنم بهتر بکوشم</p></div>
<div class="m2"><p>ز بی شرمی یکی خفتان بپوشم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا تا جان بود زو بر نگردم</p></div>
<div class="m2"><p>که جان خویش در کار تو کردم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ندانم راست تر زین دل که ماراست</p></div>
<div class="m2"><p>بر آید کام دل چون دل بود راست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دگر ره شد به نزد ویس مه روی</p></div>
<div class="m2"><p>سخن در دل نگاریده ز ده روی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرو را دید چون ماه دو هفته</p></div>
<div class="m2"><p>میان عقدهء هجران گرفته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دلش بریان و آن دو دیده گریان</p></div>
<div class="m2"><p>چو تنوری کزو بر خاست طوفان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به چشمش روز روشن چون شب تار</p></div>
<div class="m2"><p>به زیرش خز و دیبا چون سیه مار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دگار باره زبان بگشاد دایه</p></div>
<div class="m2"><p>که چون دریا ز گوهر داشت مایه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همی گفت از جهان گم باد و بی جان</p></div>
<div class="m2"><p>کسی کاو مر ترا کردست پیچان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گران بادش به جان بر انده و درد</p></div>
<div class="m2"><p>چنان کاندوه و درد تو گران کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رتا از خان و مان و خویش و پیوند</p></div>
<div class="m2"><p>جدا کرد و به دام دوری افگند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز نوشین مادر و فرخ برادر</p></div>
<div class="m2"><p>یکی با جان یکی با دل برابر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درین گیهان توی بوده همانا</p></div>
<div class="m2"><p>در انده ناتوان و ناشکیبا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نبرد جانت را از درد و آزار</p></div>
<div class="m2"><p>نضوید دلت را از داغ و تیمار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چه باید این خرد کت داد یزدان</p></div>
<div class="m2"><p>چو دردت را نخواهد بود درمان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بسوزم چون ترا سوزان ببینم</p></div>
<div class="m2"><p>بپیچم چون ترا پیچان ببینم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خردمند از خرد جوید همه چار</p></div>
<div class="m2"><p>به دست چاره بگذارد همه کار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ترا یزدان خرد دادست و دانش</p></div>
<div class="m2"><p>وزین دانش ندادت هیچ رامش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به خر مانی که دارد بار شمشیر</p></div>
<div class="m2"><p>ندارد سود وی را چون رسد شیر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کنون تا کی چنین تیمار داری</p></div>
<div class="m2"><p>چنین بیجاده بر دینار باری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مکن بر روز بُر نایی ببخشای</p></div>
<div class="m2"><p>چنین اندوه بر انده میفزای</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به بیگانه زمین مخروش چندین</p></div>
<div class="m2"><p>مکن بر بخت و بر اورنگ نفرین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ور و شب سال و مه اندر کنارست</p></div>
<div class="m2"><p>به گفتارت همیشه گوش دارست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سروش و بخت را چندین میازار</p></div>
<div class="m2"><p>به گفتاری که باشد نا سزاوار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>توی بانوی ایران ماه توران</p></div>
<div class="m2"><p>خداوند بتان خورشید حوران</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جوانی را به دریا در مینداز</p></div>
<div class="m2"><p>تن سیمین به تاب رنج مگداز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که کوتاهست ما را زندگانی</p></div>
<div class="m2"><p>نپاید دیر عمر این جهانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>روان بس ارجمند و بس عزیزست</p></div>
<div class="m2"><p>چرا نزدت کم از نیمی پشیزست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>عزیزان را بدین آیین ندارند</p></div>
<div class="m2"><p>همیشه خسته و غمگین ندارند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>روانت با تو یاری مهربانست</p></div>
<div class="m2"><p>رفیقی با تو وی را جاودانست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مگر تو سال و مه این کار داری</p></div>
<div class="m2"><p>که یار مهربان را خوار داری</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کجا رامین که با تو مهربان گشت</p></div>
<div class="m2"><p>به چشمت خاک راه شایگان گشت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مکن با دوستان زین رام تر باش</p></div>
<div class="m2"><p>جهان را چون درختی میوه بر باش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بدان بُرنای دلخسته ببخشای</p></div>
<div class="m2"><p>هم او را هم تن خود را مفرسای</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مکن بیگانگی با آن جوانمرد</p></div>
<div class="m2"><p>بپرور مهر آن کاو مهر پرورد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو از تو کس نیابد خوشی و کام</p></div>
<div class="m2"><p>چه روی تو چه چشما روی بر بام</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو بشنید این سخن ویسه بر آشفت</p></div>
<div class="m2"><p>به تندی سخت گفتارش بسی گفت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدو گفت ای بداندیش و بنفرین</p></div>
<div class="m2"><p>مه تو بادی و مه ویس و مه رامین</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مه خوزان باد وا رون جای و بومت</p></div>
<div class="m2"><p>مه این گفتار و این دیدار شومت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز شهر تو نیاید جز بد اختر</p></div>
<div class="m2"><p>ز تخم تو نیاید جز فسونگر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر زایند از آن تخمه هزاران</p></div>
<div class="m2"><p>همه دیوان بُوند و بادساران</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نه شان کردار بتوان آن</p></div>
<div class="m2"><p>نه شان گفتارها بتوان شنودن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مبادا هیچ کس از نیک نامان</p></div>
<div class="m2"><p>که فرزندش دهد بددایه زین سان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو از دایه بگیرد شیر ناپاک</p></div>
<div class="m2"><p>به آلوده نژاد و خوی بی باک</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کند ویژه نژاد پاک گوهر</p></div>
<div class="m2"><p>از آن گوهر که او دارد فروتر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>اگر شیرش خورد فرزند خورشید</p></div>
<div class="m2"><p>به نور او نباید داشت امید</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از ایزد شرم بادا مادرم را</p></div>
<div class="m2"><p>که کرد آلوده ویژه گوهرم را</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مرا در دست چون تو جادوی داد</p></div>
<div class="m2"><p>که با تو نیست شرم و دانش و داد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تو بد خواه منی نه دایهء من</p></div>
<div class="m2"><p>بخواهی برد آب و سایهء من</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>مرا فرهنگ و نیکو نامی آموز</p></div>
<div class="m2"><p>مرا پاینده باش از بد شب و روز</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تو چندان خویشتن را می ستودی</p></div>
<div class="m2"><p>به نام نیک و خود بد نام بودی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بدان خوی سترگ و چشم بی شرم</p></div>
<div class="m2"><p>بدین گفتار و کردار بی آزرم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>همه نامت به خاک اندر فگندی</p></div>
<div class="m2"><p>همه مهر خود از دلها بکندی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ندارد مر ترا مقدار و آزرم</p></div>
<div class="m2"><p>جز آن کاو چون تو باشد شوخ و بی شرم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چه گفتارت مرا چه نامهء مرگ</p></div>
<div class="m2"><p>همی ریزم ازو چون از خزان برگ</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>مرا گویی به کوته زندگانی</p></div>
<div class="m2"><p>چرا خوشی و کام دل نزانی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>اگر نیکو کنم تا زنده مانم</p></div>
<div class="m2"><p>از آن بهتر که کام خویش رانم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بهشت روشن و دیدار یزدان</p></div>
<div class="m2"><p>به کام این جهانی یافت نتوان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>جهان در چشم دانا هست بازی</p></div>
<div class="m2"><p>نباشد هیچ بازی را درازی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پس ای دایه تو جانت را مرنجان</p></div>
<div class="m2"><p>ز بهر من مخور زنهار با جان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>که من ننیوشم این گفتار خامت</p></div>
<div class="m2"><p>نیفتم هرگز اندر پایدامت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نه من طفلم که بفریبم به رنگی</p></div>
<div class="m2"><p>و یا مرغم که بر پرم به سنگی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>سخن که شنیده ای از بی خدر رام</p></div>
<div class="m2"><p>به گوش من فسونست آن نه پیغام</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نگر تا نیز پیش من نگویی</p></div>
<div class="m2"><p>ز من خشنودی دیوان نجویی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>که من دل زین جهان نیزار کردم</p></div>
<div class="m2"><p>خرد را بر روان سالار کردم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به هر سانی خدای دانش و دین</p></div>
<div class="m2"><p>به از دیوان خوزانی و رامین</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>نیازارم خدای آسمان را</p></div>
<div class="m2"><p>نه بفروشم بهشت جاودان را</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز بهر دایهء بی شرم و بی دین</p></div>
<div class="m2"><p>بدابه هر دو گیتی را به رامین</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو دایه خشم ویس دلستان دید</p></div>
<div class="m2"><p>سخنها از خدای آسمان دید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>زمانی با دل اندیشه همی کرد</p></div>
<div class="m2"><p>که درمان چون پدید آرد بدین درد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نیارامید دیو دژ برامش</p></div>
<div class="m2"><p>همان می بود خوی خویش کامش</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>جز آن گاهی که کار ویس و رامین</p></div>
<div class="m2"><p>بیامیزد به هم چون چرب و شیرین</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو افسونها به گرد آورد بی مر</p></div>
<div class="m2"><p>ز هر رنگ و زهر جای و ز هر در</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>دگر باره زبان از بند بگشاد</p></div>
<div class="m2"><p>سخنها گفت همچون نقش نوشاد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بدو گفت ای گرامی تر ز جانم</p></div>
<div class="m2"><p>به زیب و خوبی افزون از گمانم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>همیشه دادجوی و راست گو باش</p></div>
<div class="m2"><p>همیشه نیک نام و نیک خو باش</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>من اندر چه نیاز و چه نهیبم</p></div>
<div class="m2"><p>که چون تو پاک زادی را فریبم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چرا گویم سخن با تو به دستان</p></div>
<div class="m2"><p>که بر چیز کسانم نیست دستان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>مرا رامین نه خویشست و نه پیوند</p></div>
<div class="m2"><p>نه هم گوهر نه هم زاد و نه فرزند</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نگویی تا چه خوبی کرد با من</p></div>
<div class="m2"><p>که با او دوست گردم با تو دشمن</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>مرا از دو جهان کام تو باید</p></div>
<div class="m2"><p>وز آن کامم همی نام تو باید</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بگویم با تو این راز آشکاره</p></div>
<div class="m2"><p>کجا اکنون جزینم نیست چاره</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>هر آیینه تو از مردم بزادی</p></div>
<div class="m2"><p>نه دیوی نه پری نه حور زادی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ز جفت پاک چون ویرو گسستی</p></div>
<div class="m2"><p>به افسون نیز موبد را ببستی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ندیده هیچ مردی از تو شادی</p></div>
<div class="m2"><p>که تا امروز تن کس را نداری</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>تو نیز از کس ندیدی شادکامی</p></div>
<div class="m2"><p>نراندی کام با مردان تمامی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>دو کردی شوی و هر دو از تو پدرود</p></div>
<div class="m2"><p>چه ایشان و چه پولی زان سوی رود</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>اگر خود دید خواهی در جهان مرد</p></div>
<div class="m2"><p>نیابی همچو رامین یک جوانمرد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چه سود ار تو به چهره آفتابی</p></div>
<div class="m2"><p>که کامی زین نکو رویی نیابی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>تو این خوشی ندیدستی ندانی</p></div>
<div class="m2"><p>که بی او خوش نباشد زندگانی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>خدا از بهر نر کردست ماده</p></div>
<div class="m2"><p>توی هم مادهء از نر بزاده</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>زنان مهتران و نامداران</p></div>
<div class="m2"><p>بزرگان جهان و کامگاران</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>همه با شوهرند و با دل شاد</p></div>
<div class="m2"><p>جوانانی چو سرو و مُرد و شمشاد</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>اگر چه شوی نام بردار دارند</p></div>
<div class="m2"><p>نهانی دیگری را یار دارند</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>گهی دارند شوی نغز در بر</p></div>
<div class="m2"><p>به کام ژویش و گاهی یار دلبر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>اگر گنج همه شاهان تو داری</p></div>
<div class="m2"><p>نیابی کام چون بی شوی و یاری</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چه زیورهای شاهانه چه دیبا</p></div>
<div class="m2"><p>چه گوهرهای نیکو رنگ و زیبا</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>زنان را این ز بهر مرد باید</p></div>
<div class="m2"><p>که مردان را نشاط دل فزاید</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>چو نه مرد از تو نازد نه تو از مرد</p></div>
<div class="m2"><p>چرا باشی همی در سرخ و در زرد</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>اگر دانی که گفتم این سخن راست</p></div>
<div class="m2"><p>ز تو دشمان و نفرینم نه زیباست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>من این گفتم ز روی مهربانی</p></div>
<div class="m2"><p>ز مهر مادری و دایگانی</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>که رامین را به تو دیدم سزاوار</p></div>
<div class="m2"><p>تو او را دوستگانی او ترا یار</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>تو خورشیدی و او ماه دو هفته</p></div>
<div class="m2"><p>چو او سروست و تو شاخ شکفته</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>به مهر اندر چو شیر و می بسازید</p></div>
<div class="m2"><p>بسازید و به یکدیگر بنازید</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو من بینم شما را هر دو باهم</p></div>
<div class="m2"><p>نباشد در جهان زان پس مرا غم</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چو دایه این سخنها گفت با ویس</p></div>
<div class="m2"><p>به یاری آمدش با لشکر ابلیس</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>هزاران دام پیش ویس بنهاد</p></div>
<div class="m2"><p>هزاران در ز پیش دلش بگشاد</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بدو گفت این زنان نامداران</p></div>
<div class="m2"><p>نشسته شاد با دلبند و یاران</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>همه کس را به شادی دستگاهست</p></div>
<div class="m2"><p>ترا هنواره درد و وای و آهست</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>به پیری آیدت روز جوانی</p></div>
<div class="m2"><p>تو نا دیده زمانی شادمانی</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>هر آیینه نه سنگینی نه رویین</p></div>
<div class="m2"><p>در انده چون توانی بود چندین</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>ازین اندیشه مهرش گرم تر شد</p></div>
<div class="m2"><p>دل سنگینش لختی نرم تر شد</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>نه دام آمد مهم تن جز زبانش</p></div>
<div class="m2"><p>زبانش داشت پوشیده نهانش</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>به گفتاری چو شکر دایه را گفت</p></div>
<div class="m2"><p>نباشد هیچ زن را چاره از جفت</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>سخنها هر چه گفتی راست گفتی</p></div>
<div class="m2"><p>نکردی با من اندر مهر زُفتی</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>زبان هر چند سست و نا توانند</p></div>
<div class="m2"><p>دل آرای دلیران جهانند</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>هزاران ژوی بد باشد دریشان</p></div>
<div class="m2"><p>سزد گر دل نبندد کس بریشان</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>مرا نیز آنگه گفتم هم ازانست</p></div>
<div class="m2"><p>که تندی کردن از طبع زنانست</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>مرا بود آن سخن در گوش چونان</p></div>
<div class="m2"><p>که در دل رفته زهر آلوده پیکان</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>ازیرا لختکی تندی ننودم</p></div>
<div class="m2"><p>که گفتار از در تندی شنودم</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>زبان خویش را بد گوی کردم</p></div>
<div class="m2"><p>پشیمانی کنون بسیار خوردم</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>نبایستم ترا آن زشت گفتن</p></div>
<div class="m2"><p>نهانت را ببایستم نهفتن</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چو من کاری نخواهم کرد با کس</p></div>
<div class="m2"><p>جواب من خود او را درد من بس</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>کنون آن خواهم از بخشنده دادار</p></div>
<div class="m2"><p>که باشد مر مرا از بد نگهدار</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>نیالاید به آهوی زنانم</p></div>
<div class="m2"><p>نگه دارد ز آهوشان زبانم</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>بدارد تا زیم روشن تن من</p></div>
<div class="m2"><p>به کام دوستان و درد دشمن</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>مرا دوری دهد از تو بد آمروز</p></div>
<div class="m2"><p>که شاگردان تو باشند بدروز</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>چو دیگر روز گیتی بوستان شد</p></div>
<div class="m2"><p>فروغ مهر در وی گلستان شد</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>به جای وعده شد آزاده رامین</p></div>
<div class="m2"><p>بیامد دایه پس با درد و غمگین</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>مرُو را گفت راما چند گویی</p></div>
<div class="m2"><p>در آتش آب روشن چند جویی</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>نشاید باد را در بر گرفتن</p></div>
<div class="m2"><p>نه دریا را به مشتی بر گرفتن</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>نه ویس سنگ دل را مهر دادن</p></div>
<div class="m2"><p>نه با او سر به یک بالین نهادن</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>ز خارا آب مهر آید وزو نه</p></div>
<div class="m2"><p>به مهر اندر که خارا ازو به</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>چو برداری میان شورم آواز</p></div>
<div class="m2"><p>مر آواز ترا پاسخ دهد باز</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>دل ویسه بسی سختر ز شورم</p></div>
<div class="m2"><p>به خوی بد همی ماند به کژدم</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>ترا پاسخ نداد آن سرو آزاد</p></div>
<div class="m2"><p>بلی دشنام صد گونه به من داد</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>عجب ماندم من از فرهنگ آن ماه</p></div>
<div class="m2"><p>که در وی نیست افسون مرا راه</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>فریب و حیله و نیرنگ و دستان</p></div>
<div class="m2"><p>بود پیشش چو حکمت نزد مستان</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>نه او خواهش پذیرد هر گز از من</p></div>
<div class="m2"><p>نه آغارش پذیرد ز اب آهن</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>چو بشنید این سخن ازاده رامین</p></div>
<div class="m2"><p>چو کبگ خسته شد در چنگ شاهین</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>جهان در پیش چشمش تنگ و تاریک</p></div>
<div class="m2"><p>امیدش دور و نیم مرگ نزدیک</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>تنش ابر بلا را گشته منزل</p></div>
<div class="m2"><p>نم اندر دیدگان و برق در دل</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>هم از خشم و هم از گفتار جانان</p></div>
<div class="m2"><p>زده بر جان و دل دو گونه پیکان</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>به فریاب آمد از سختی دگر بار</p></div>
<div class="m2"><p>مگر صد بار گفت ای دایه زنهار</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>مرا فریاد رس یک بار دیگر</p></div>
<div class="m2"><p>که من چون تو ندارم یار دیگر</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>نگیرم باز دست از دامن تو</p></div>
<div class="m2"><p>منم با خون خود در گردن تو</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>گر از امّید تو نومید گردی</p></div>
<div class="m2"><p>بساط زندگانی در نوردم</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>شوم بر راز خود پرده بدرّم</p></div>
<div class="m2"><p>هم از جان و هم از گیتی ببرّم</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>اگر رنجه شوی یک بار دیگر</p></div>
<div class="m2"><p>بگویی حال من با آن سمن بر</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>سپاس جاودان باشندت بر من</p></div>
<div class="m2"><p>که آهر من نیابد راه در من</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>مگر سنگین دلش بر من بسوزد</p></div>
<div class="m2"><p>چراغ مهربانی بر فروزد</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>مگر زین خوی بد گردد پشیمان</p></div>
<div class="m2"><p>نریزد خون و نستاند ز من جان</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>درودش ده درود مهربانان</p></div>
<div class="m2"><p>بگو ای کام پیران و جوانان</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>دل من داری و شاید که داری</p></div>
<div class="m2"><p>که بر دل داشتن چابک سواری</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>توریزی خون من شاید که ریزی</p></div>
<div class="m2"><p>که جان عاشقان را رستخیزی</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>تو بر جان و تن من پادشایی</p></div>
<div class="m2"><p>به چونین پادشایی هم تو شایی</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>اگر جان مرا با من بمانی</p></div>
<div class="m2"><p>گذارم در پرستش زندگانی</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>تو دانی من پرستش را بشایم</p></div>
<div class="m2"><p>نه آن باشم که مردم را ربایم</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>اگر بسیار کس باشند یارت</p></div>
<div class="m2"><p>یکی چون من نباشد دوستداری</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>اگر با من در آمیزی بدانی</p></div>
<div class="m2"><p>که چون باشد وفا و مهربانی</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>تو خورشیدی و گر بر من بتابی</p></div>
<div class="m2"><p>مرا یاقوت مهر خویش یابی</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>اگر شایم به مهر و دوستداری</p></div>
<div class="m2"><p>ز من بردار بار گرم و خواری</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>مرا زنده بمان تا زندگانی</p></div>
<div class="m2"><p>کنم در کار مهرت رایگانی</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>پس ار خواهی که جان من ستانی</p></div>
<div class="m2"><p>هر آن روزی که خواهی خود توانی</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>و گر با خوی تو بیچار گردم</p></div>
<div class="m2"><p>ز جان خویشتن بیزار گردم</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>فرو افتم ز کوه تند بالا</p></div>
<div class="m2"><p>جهم در موج آب ژرف دریا</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>گرفتاری ترا باشد به جانم</p></div>
<div class="m2"><p>بدان سر جان خویش از تو ستانم</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>به پیش داوری کاو داد خواهد</p></div>
<div class="m2"><p>همه داد جهان او داد خواهد</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>بگفتم آنچه دانستم تو به دان</p></div>
<div class="m2"><p>گوا بر ما دو تن بس باد یزدان</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>ز بس زاری و از بس اشک خونین</p></div>
<div class="m2"><p>دل دایه به درد آورد رامین</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>بشد دایه ز پیشش با دل ریش</p></div>
<div class="m2"><p>مرو را درد بر دل زان او بیش</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>چو پیش ویس شد بنشست خاموش</p></div>
<div class="m2"><p>دل از تیمار و اندیشه پر از جوش</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>دگر باره سخنهای نگارین</p></div>
<div class="m2"><p>چو در پیوسته کرد از بهر رامین</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>بگفت ای شاه خوبان ماه حوران</p></div>
<div class="m2"><p>ترا مردند نزدیکان و دوران</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>بخواهم گفت با تو یک سخن راز</p></div>
<div class="m2"><p>مرا شرمت فرو بستست آواز</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>همی ترسم ازین از شاه موبد</p></div>
<div class="m2"><p>که ترسد هر کسی از مردم بد</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>ز ننگ و سرزنش پرهیز دارم</p></div>
<div class="m2"><p>کزیشان تیره گردد روزگار</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>ز دوزخ نیز ترسانم به فرجام</p></div>
<div class="m2"><p>که در دوزخ شوم بد روز و بدنام</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>و لیکن چون براندیشم ز رامین</p></div>
<div class="m2"><p>وزآن رخسار زرد و اشک خونین</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>وزآن گفتن مرا ای دایه زنهار</p></div>
<div class="m2"><p>که شدجان و جهان بر چشم من خوار</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>خرد را در دو دیده او بدوزد</p></div>
<div class="m2"><p>دگر باره دلم بر وی بسوزد</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>بدان مسکین چنان بخشایش آرم</p></div>
<div class="m2"><p>که با زاریش جان را خوار دارم</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>بسی دیدم به گیتی عاشق زار</p></div>
<div class="m2"><p>مژه پراشک خون و دل پر آزار</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>ندیدستم بدین بیچارگی کس</p></div>
<div class="m2"><p>به صد عاشق یکی تیمار او بس</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>سخنهایش تو پنداری که تیغست</p></div>
<div class="m2"><p>همان چشمش تو پنداری که میغست</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>بریده شد قرار من بدان تیغ</p></div>
<div class="m2"><p>نگون شد خانهء صبرم بدان میغ</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>همی ترسم که او ناگه بمیرد</p></div>
<div class="m2"><p>به مرگ او مرا یزدان بگیرد</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>مکن ماها بدان مسکین ببخشای</p></div>
<div class="m2"><p>به خون او روانت را میالای</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>چه بفزایدت گر خونش بریزی</p></div>
<div class="m2"><p>که باشد در خورت چون زو گریزی</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>نه اکنون و نه زین پس تا به صد سال</p></div>
<div class="m2"><p>جوان باشد بدان برز و بدان یال</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>جوان و چابک و راد و سخن دان</p></div>
<div class="m2"><p>بدو پیدا نشان فر یزدان</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>ترا یزدان چو این روی نکو داد</p></div>
<div class="m2"><p>به جان من که خود از بهر او داد</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>ترا چون حور و دیبا روی بنگاشت</p></div>
<div class="m2"><p>پس اندر مهر و در سایه همی داشت</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>بدان تا مهر تو بخشد به رامین</p></div>
<div class="m2"><p>پس او خسرو بود مارا تو شیرین</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>به جان من که جز چونین نباشد</p></div>
<div class="m2"><p>ترا سالار جز رامین نباشد</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>همی تا دایه سوگندان همی خورد</p></div>
<div class="m2"><p>یکایک ویس را باور همی کرد</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>فزون شد در دلش بخشایش رام</p></div>
<div class="m2"><p>گرفت از دوستی آرایش رام</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>ستیزش کم شد و مهرش بیفزود</p></div>
<div class="m2"><p>پدید آمد از آتش لختکی دود</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>وفا چون صبح در جانش اثر کرد</p></div>
<div class="m2"><p>وزان پس روز مهرش سر آورد</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>بشد در پاسخش چیره زبانی</p></div>
<div class="m2"><p>که بودش خامشی همداستانی</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>همی پیچید سر را بر بهانه</p></div>
<div class="m2"><p>گهی دیدی زمین گه آسمانه</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>رخش از شرم دو گونه برشتی</p></div>
<div class="m2"><p>گهی میگون و گاهی زرد گشتی</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>تنش از شرم همچون چشمهء آب</p></div>
<div class="m2"><p>چکان زو خوی چو مروارید خوشاب</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>چنین باشد روان مهرداران</p></div>
<div class="m2"><p>که بخشایش کنند بر نیک یاران</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>دل اندر مهر می بر هنجد از تن</p></div>
<div class="m2"><p>چنان چون سنگ مغناطیس زاهن</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>به یک دل مهر پیوستن نشاید</p></div>
<div class="m2"><p>چو خر کش بار بر یک سو نفاید</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>همی دانست جادو دایهء پیر</p></div>
<div class="m2"><p>کزین بار از کمانش راست شد تیر</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>رمیده گور در داهولش افتاد</p></div>
<div class="m2"><p>وز افسونش به بند آمد سر باد</p></div></div>