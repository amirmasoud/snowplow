---
title: >-
    بخش ۲۴ - اندر بستن دایه مر شاه موبد را بر ویس
---
# بخش ۲۴ - اندر بستن دایه مر شاه موبد را بر ویس

<div class="b" id="bn1"><div class="m1"><p>چو دایه ویس را چونان بیاراست</p></div>
<div class="m2"><p>که خورشید از رخ او نور می خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چشم ویس از گریه نیاسود</p></div>
<div class="m2"><p>تو گفتی هر زمانش درد بفزود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهان از هر کسی مر دایه را گفت</p></div>
<div class="m2"><p>که بخت شور من با من بر آشفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم را سیر کرد از زندگانی</p></div>
<div class="m2"><p>وزو بر کند بیخ شادمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تو مر مرا چاره نجویی</p></div>
<div class="m2"><p>وزین اندیشه جانم را نضویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من این چاره که گفتم زود سازم</p></div>
<div class="m2"><p>بدو کوته کنم رنج درازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا هر گه که موبد را ببینم</p></div>
<div class="m2"><p>تو گویی بر سر آتش نشینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه مرگ آید به پیش من چه موجه</p></div>
<div class="m2"><p>که روزش بادهمچو روز من بد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه دل به آب صبر شستست</p></div>
<div class="m2"><p>هوای دل هنوز از من نجستست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همی ترسم که روزی هم بجویی</p></div>
<div class="m2"><p>نهفته راز دل روزی بگوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز پس آنکه او جوید ز من کام</p></div>
<div class="m2"><p>ترا گسترد باید در رشت دام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که من یک سال نسپارم بدو تن</p></div>
<div class="m2"><p>بپرهیزم ز پادفراه دشمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نباشد سوک قران کم ز یک سال</p></div>
<div class="m2"><p>مرا یک سال بینی هم بدین حال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندارد موبدم یک سال آزرم</p></div>
<div class="m2"><p>کجا او را ز من بیم و نه شرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی نیزنگ سال از هوشمندی</p></div>
<div class="m2"><p>مگر مردیش را بر من ببندی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو سالی بگذرد پس بر گشایی</p></div>
<div class="m2"><p>رهی گرددت چون یابد رهایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صمگر چون زین سخن سالی بر آید</p></div>
<div class="m2"><p>به من بر روز بدبختی سر آیدص</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر این چاره کت گفتم نسازی</p></div>
<div class="m2"><p>تو نیز از بخت من هرگز ننازی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شما را باد کام اینجهانی</p></div>
<div class="m2"><p>تو با موبد همی کن شادمانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که من نیکی به ناکامی نخواهم</p></div>
<div class="m2"><p>همان شادی و بدنامی نخواهم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهل تا کام موبد برنیاید</p></div>
<div class="m2"><p>و گر جانم برآید نیز شاید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به بی کامی نگویی کام او ده</p></div>
<div class="m2"><p>که بیجانی ز بیکامی مرا به</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو گفت این راز را با دایهء پیر</p></div>
<div class="m2"><p>تو گفتی بردلش زد ناو کی تیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دو چشم دایه بر وی ماند خیره</p></div>
<div class="m2"><p>جهان بر هردو چشمش گشت تیره</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت ای چراغ و چشم دایه</p></div>
<div class="m2"><p>نبینم با تو از داد ایچ مایه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سیه دل گشتی از رنج آی</p></div>
<div class="m2"><p>سیاهی از شبه نتوان زدودی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سپاه دیو جادو بر تو ره یافت</p></div>
<div class="m2"><p>ترا از راه داد و مهر بر تافت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ولیکن چون تو بی آرام گشتی</p></div>
<div class="m2"><p>بیکباره خرد را در نوشتی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ندانم چاره جز کام تو جستی</p></div>
<div class="m2"><p>بهافسون شاه را بر تو ببستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کجا آنگه روی هر دو بیاورد</p></div>
<div class="m2"><p>طلسم هر یکی را صروتی کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به آهن هر دوان را بست بر هم</p></div>
<div class="m2"><p>به افسون بند هر دو کرد محکم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی تا بسته ماندی بند آهن</p></div>
<div class="m2"><p>ز بندش بسته ماندی مرد بر زن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>و گر بندش کسی بر هم شکستی</p></div>
<div class="m2"><p>همان گه مردی بسته برستی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو بسته شد به افسون شاه بر ماه</p></div>
<div class="m2"><p>ببرد آن بند ایشان را سحر گاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زمینی بر لب رودی نشان کرد</p></div>
<div class="m2"><p>مر آن را زیر خاک اندر نهان کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو باز آمد یکایک ویس را گفت</p></div>
<div class="m2"><p>که آن افسون کدامین جای بنهفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدو گفت آنچه فرمودی بکردم</p></div>
<div class="m2"><p>اگر چه من ز فرمانت بدردم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز فرمان تو خشنودیت جستم</p></div>
<div class="m2"><p>چنین آزاد مردی را ببستم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به پیمانی که چون یک مه برآید</p></div>
<div class="m2"><p>ترا این روز بدخویی سر آید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به حکم ایزدی خرسند گردی</p></div>
<div class="m2"><p>ستیز و کینه از دل در نوردی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نگویی همچنین باشد یکی سال</p></div>
<div class="m2"><p>که نپسندد خرد بر تو چنین حال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو تو دل خوش کنی با شهریارم</p></div>
<div class="m2"><p>من آن افسون بنهفته بیارم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر آتش بر نهم یکسر بسوزی</p></div>
<div class="m2"><p>شما را دل به شادی برفروزی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کجا تا آن بود در آب و در نم</p></div>
<div class="m2"><p>بود هنواره بند شاه محکم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به گوهر آب دارد طبع سردی</p></div>
<div class="m2"><p>به سردی بسته ماند زور مردی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو آتش بند افسون را بسوزد</p></div>
<div class="m2"><p>دگر ره شمع مردی برفروزد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو دایه ویس را دل کرد خرسند</p></div>
<div class="m2"><p>که تا یک ماه نگشاید ز شه بند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>قصای بد ستیز خویش بننود</p></div>
<div class="m2"><p>نگر تا زهر چون بر شکر آلود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر آمد نیلگون ابری ز دریا</p></div>
<div class="m2"><p>به آب سیل دریا کرد صحرا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رسید آن آب در هر مرغزاری</p></div>
<div class="m2"><p>پدید آمد چو جیحون رودباری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به رود مرو بفزود آب چندان</p></div>
<div class="m2"><p>که نیمی مرو شد از آب ویران</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تبه کرد آن نشان و زمین را</p></div>
<div class="m2"><p>ببردی آن بند شاه بافرین را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قصا کرد آن زمین را رودخانه</p></div>
<div class="m2"><p>بماند آن بند بر شه جاودانه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به چشمش دربماند آن دلبر خویش</p></div>
<div class="m2"><p>چو دینار کسان در چشم درویش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو شیر گرسنه بسته به زنجیر</p></div>
<div class="m2"><p>چران در پیش او بیباک نخچیر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هنوز او زنده بود از بخت کام</p></div>
<div class="m2"><p>فرو مرد از تنش گفتی یک اندام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به راه شادی اندر گشت گمراه</p></div>
<div class="m2"><p>ز خوشی دست کامش گشت کوتاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به کام دشمان در صلت دوست</p></div>
<div class="m2"><p>چو زندان بود گفتی برتنش پوست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به شب در بر گرفته دوست را تنگ</p></div>
<div class="m2"><p>تو گفتی دور بودی شصت فرسنگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همان دو شوی کرده ویس بتروی</p></div>
<div class="m2"><p>به مهر دختری مانده چو بی شوی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نه موبد کام ازو دیده نه ویرو</p></div>
<div class="m2"><p>جهان بنگر چه بازی کرد با او</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بپروردش به ناز و شادکامی</p></div>
<div class="m2"><p>بر آوردش به جاه و نیکنامی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو قدش آفت سرو سهی شد</p></div>
<div class="m2"><p>دو هفته ماه رویش را رهی شد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شکفته شد به رخ بر لالهزارش</p></div>
<div class="m2"><p>به بار آمد زبر سیمثن دونارش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جهان با او ز راه مهر برگشت</p></div>
<div class="m2"><p>سراسر حالهای او دگر گشت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بگویم با یک یک حال آن ماه</p></div>
<div class="m2"><p>چه با دایه چه با رمین چه با شاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بهگفتاری که چون عاشق بخواند</p></div>
<div class="m2"><p>به درد دل ز دیده خون چکانه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بگویم داستان عاشقانه</p></div>
<div class="m2"><p>بدو در عشق را چندین فسانه</p></div></div>