---
title: >-
    بخش ۱۶ - اندر صفت جنگ موبد و ویرو
---
# بخش ۱۶ - اندر صفت جنگ موبد و ویرو

<div class="b" id="bn1"><div class="m1"><p>چو از خاور بر آمد اختران شاه</p></div>
<div class="m2"><p>شهی کش مه وزیرست آسمان گاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو کوس کین بغرید از دو درگاه</p></div>
<div class="m2"><p>به جنگ آمد دو لشکر پیش دو شاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه کوس جنگ بود آن دیو کین بود</p></div>
<div class="m2"><p>که پر کین گشت هرک آن بانگ بشنود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدیل صور شد نای دمنده</p></div>
<div class="m2"><p>تبیره مرده را می کرد زنده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کز بانگ رعد نوبهاران</p></div>
<div class="m2"><p>برون آید بهار از شاخساران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بانگ کوس کین آمد همیدون</p></div>
<div class="m2"><p>ز لشکر گه بهار جنگ بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به قلب اندر دهل فریاد خوانان</p></div>
<div class="m2"><p>که بشتابید هیچ ای جان ستانان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن فریاد صنج او را عدیلی</p></div>
<div class="m2"><p>چو قوالان سرایان با سپیلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم آن شیپور بر صد راه نالان</p></div>
<div class="m2"><p>بسان بلبل اندر آبسالان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خروشان گاو دُم با او به یک جا</p></div>
<div class="m2"><p>چو با هم دو سراینده به همتا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز پیش آنکه بی جان گشت یک تن</p></div>
<div class="m2"><p>همی کرد از شگفتی بوق شیون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به جنگ جنگجویان تیغ رخشان</p></div>
<div class="m2"><p>همی خندید هم بر جان ایشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صف جوشن وران بر روی صحرا</p></div>
<div class="m2"><p>چو کوه اندر میان موج دریا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به موج اندر دلیران چون نهنگان</p></div>
<div class="m2"><p>به کوه اندر سواران چون پلنگان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همان مردم کجا فرزانه بودند</p></div>
<div class="m2"><p>به دشت جنگ چون دیوانه بودند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کجا دیوانه ای باشد به هر باب</p></div>
<div class="m2"><p>که نز آتش بپرهیزد نه از آب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه از نیزه بترسد نی ز شمشیر</p></div>
<div class="m2"><p>نه از پیلان بیندیشد نه از شیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آن صحرا یلان بودند چونین</p></div>
<div class="m2"><p>فدای نام کرده جان شیرین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نترسیدند از مردن گه جنگ</p></div>
<div class="m2"><p>ز نام بد بترسیدند و از ننگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هوا چون بیشهء دد بود یکسر</p></div>
<div class="m2"><p>ز ببر و شیر و گرگ و خوگ پیکر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو سروستان شده دشت از درفشان</p></div>
<div class="m2"><p>ز دیبای درفشان مه درفشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فراز هر یکی زرّین یکی مرغ</p></div>
<div class="m2"><p>عقاب و باز با طاووس و سیمرغ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به زیر باز در شیر نکو رنگ</p></div>
<div class="m2"><p>تو گفتی شیر دارد باز در جنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پی پیلان و سمّ باد پایان</p></div>
<div class="m2"><p>شده آتش فشانان سنگ سایان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زمین از زیر ایشان شد بر افراز</p></div>
<div class="m2"><p>به گردون رفت و پس آمد از او باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نبودش جای بنشستن به گیهان</p></div>
<div class="m2"><p>همی شد در دهان و چشم ایشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بسا اسپ سیاه و مرد برنا</p></div>
<div class="m2"><p>که گشت از گرد خنگ و پیر سیما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلاور آمد از بد دل پدیدار</p></div>
<div class="m2"><p>که این با خرّمی بد آن به تیمار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی را گونه شد همرنگ دینار</p></div>
<div class="m2"><p>یکی را چهره شد مانند گلنار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو آمد هر دو لشکر تنگ در هم</p></div>
<div class="m2"><p>ز کین بردند گردان حمله برهم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو گفتی ناگهان دو کوه پولاد</p></div>
<div class="m2"><p>در آن صحرا به یکدیگر در افتاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پیمبر شد میان هر دو لشکر</p></div>
<div class="m2"><p>خدنگ چار پرّو خشت سه پر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رسولانی که از دل راه جستند</p></div>
<div class="m2"><p>همی در چشم یا در دل نشستند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هر خانه که منزلگاه کردند</p></div>
<div class="m2"><p>ز خانه کدخدایش را ببردند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مصاف جنگ و بیم جان چنان شد</p></div>
<div class="m2"><p>که رستاخیز مردم را عیان شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برادر از برادر گشت بیزار</p></div>
<div class="m2"><p>بجز کردار خود کس را نبد یار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بجز بازو ندیدند ایچ یاور</p></div>
<div class="m2"><p>بجز خنجر ندیدند ایچ داور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر آن کس را که بازو یاوری کرد</p></div>
<div class="m2"><p>به کام خویش خنجر داوری کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو گفتی جنگیان کارنده گشتند</p></div>
<div class="m2"><p>همه در چشم و دل پولاد کشتند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سخن گویان همه خاموش بودند</p></div>
<div class="m2"><p>چو هشیاران همه بیهوش بودند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کسی نشنید آوازی در آن جای</p></div>
<div class="m2"><p>مگر آواز کوس و نالهء نای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گهی اندر زره شد تیغ چون آب</p></div>
<div class="m2"><p>گهی در دیدگان شد تیر چون خواب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گهی رفتی سنان چون عشق در بر</p></div>
<div class="m2"><p>گهی رفتی تبر چون هوش در سر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی دانست گفتی تیغ خونخوار</p></div>
<div class="m2"><p>که جان در تن کجا بنهاد دادار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدان راهی کجا تیغ اندرون شد</p></div>
<div class="m2"><p>ز مردم هم بدان ره جان برون شد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو میغی بود تیغ هندوانی</p></div>
<div class="m2"><p>ازو بارنده سیل ارغوانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو شاخ مُرد بر وی برگ گلنار</p></div>
<div class="m2"><p>چو برگ نار بر وی دانهء نار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به رزم اندر چو درزی بود ژوپین</p></div>
<div class="m2"><p>همی جنگ آوران را دوخت برزین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو بر جان دلیران شد قضا چیر</p></div>
<div class="m2"><p>یکی گور دمنده شد یکی شیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو بر رزم دلیران تنگ شد روز</p></div>
<div class="m2"><p>یکی غُرم دونده شد یکی یوز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در آن انبوه گردان و سواران</p></div>
<div class="m2"><p>وز آن شمشیر زخم و تیرباران</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گرامی باب ویسه گرد قارن</p></div>
<div class="m2"><p>به زاری کشته شد بر دست دشمن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به گرد قارن از گردان ویرو</p></div>
<div class="m2"><p>صد و سی گرد کشته گشت با او</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز کشته پشته ای شد زعفرانی</p></div>
<div class="m2"><p>ز خون رودی به گردش ارغوانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو گفتی چرخ زرین ژاله بارید</p></div>
<div class="m2"><p>به گرد ژاله برگ لاله بارید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو ویرو دید گردان چنان زار</p></div>
<div class="m2"><p>به گرد قارن اندر کشته بسیار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همه جان بر سر جانش نهاده</p></div>
<div class="m2"><p>به زاری کشته با خواری فتاده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بگفت آزادگانش را به تندی</p></div>
<div class="m2"><p>که از جنگ آوران زشتست کندی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شما را شرم باد از کردهء خویش</p></div>
<div class="m2"><p>وزین کشته یلان افتاده در پیش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نبیند این همه یاران و خویشان</p></div>
<div class="m2"><p>که دشمن شاد گشت از مرگ ایشان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز قارن تان نیفزاید همی کین</p></div>
<div class="m2"><p>که ریش پیر او گشتست خونین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدین زاری بکشتستند شاهی</p></div>
<div class="m2"><p>ز لشکر نیست او را کینه خواهی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فرو شد آفتاب نیک نامی</p></div>
<div class="m2"><p>سیه شد روزگار شادکامی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بترسم کافتاب آسمانی</p></div>
<div class="m2"><p>کنون در باختر گردد نهانی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>من از بد خواه او ناخواسته کین</p></div>
<div class="m2"><p>نکرده دشمنانش را بنفرین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همی بینید کامد شب به نزدیک</p></div>
<div class="m2"><p>جهان گردد هم اکنون تنگ و تاریک</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شما از بامدادان تا به اکنون</p></div>
<div class="m2"><p>بسی جنگ آوری کردید و افسون</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هنوز این پیکر وارون به پایست</p></div>
<div class="m2"><p>هنوز این موبد جادو به جایست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کنون با من زمانی یار باشید</p></div>
<div class="m2"><p>به تندی اژدها کردار باشید</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که من زنگ از گهر خواهم زدودن</p></div>
<div class="m2"><p>به کینه رستخیز او را نمودن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جهان را از بدش آزاد کردن</p></div>
<div class="m2"><p>روان قارن از وی شاد کردن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو ویرو با دلیران این سخن گفت</p></div>
<div class="m2"><p>ز مردی پر دلی را هیچ ننهفت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پس آنگه با پسندیده سواران</p></div>
<div class="m2"><p>ستوده خاصگان و نامداران</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز صفّ خویش بیرون تاخت چون باد</p></div>
<div class="m2"><p>چو آتش در سپاه دشمن افتاد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز تندی بود همچون سیل طوفان</p></div>
<div class="m2"><p>کجا او را به مردی بست نتوان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سخن آنجا به شمشیر و تبر بود</p></div>
<div class="m2"><p>همیدون بازی گردان به سر بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نکرد از بُن پدر آزرم فرزند</p></div>
<div class="m2"><p>نه مرد جنگ روی خویش و پیوند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>برادر با برادر کینه ور بود</p></div>
<div class="m2"><p>ز کینه دوست از دشمن بتر بود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>یکی تر یکی از گیتی بر آمد</p></div>
<div class="m2"><p>که پیش از شب رسیدن شب در آمد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در آن دم گشت مردم پاک شبکور</p></div>
<div class="m2"><p>به گرد انبشته شد سرچشمهء هور</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو اندر گرد شد دیدار بسته</p></div>
<div class="m2"><p>برادر را برادر کرد خسته</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>پدر فرزند خود را باز نشناخت</p></div>
<div class="m2"><p>به تیغش سر همی از تن بینداخت</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سنان نیزه گفتی بابزن بود</p></div>
<div class="m2"><p>برو بر مرغ ، مرد تیغ زن بود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>خدنگ چار پر همچون درختان</p></div>
<div class="m2"><p>برُسته از دو چشم شوربختان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>درخت زندگانی رسته از تن</p></div>
<div class="m2"><p>به پیشش ده گشاده خود و جوشن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو خنجر پرده را بر تن بدرّید</p></div>
<div class="m2"><p>درخت زندگانی را ببرّید</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>هوا از نیزه گشته چون نیستان</p></div>
<div class="m2"><p>زمین از خون مردم چون میستان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز بس گرز و ز بس شمشیر خونبار</p></div>
<div class="m2"><p>جهان پر دود و آتش بود هموار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تو گفتی همچو باد تند شد مرگ</p></div>
<div class="m2"><p>سر جنگاوران می ریخت چون برگ</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سر جنگاوران چون گوی میدان</p></div>
<div class="m2"><p>چو دست و پای ایشان بود چوگان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یلان را مرگ بر گل خوابنیده</p></div>
<div class="m2"><p>چو سروستان سغد از بن بریده</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو خورشید فلک در باختر شد</p></div>
<div class="m2"><p>چو روی عاشقان همرنگ زر شد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تو گفتی بخت موبد بود خورشید</p></div>
<div class="m2"><p>جهان از فرّ او ببرید امّید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز شب آن را ستوهی بد به گردون</p></div>
<div class="m2"><p>ز دشمن بود موبد را همیدون</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هم آن بینندگان را شد ز دیدار</p></div>
<div class="m2"><p>جهان بر خیل او زیر و زیر گشت</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>یکی بدبخت و خسته شد به زاری</p></div>
<div class="m2"><p>یکی بدروز و کشته شد به خواری</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>میانجی گر نه شب بودی در آن جنگ</p></div>
<div class="m2"><p>نرستی جان شاهنشه از آن ننگ</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نمودش تیره شب راه رهایی</p></div>
<div class="m2"><p>ز تاریکی بُد او را روشنایی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>عنان بر تافت از راه خراسان</p></div>
<div class="m2"><p>کشید از دینور سوی سپاهان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نه ویرو خود مرو را آمد از پس</p></div>
<div class="m2"><p>نه از گردان و سالاران او کس</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گمان بودش که شاهنشاه بگریخت</p></div>
<div class="m2"><p>به دام تنگ و رسوایی در آویخت</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>دگر لشکر به کوهستان نیارد</p></div>
<div class="m2"><p>دگر آزار او جستن نیارد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دگر گون بود ویرو را گمانی</p></div>
<div class="m2"><p>دگر گون بود حکم آسمانی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چو ویرو چیره شد بر شاه شاهان</p></div>
<div class="m2"><p>بدید از بخت کام نیکخواهان</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>در آمد لشکری از کوه دیلم</p></div>
<div class="m2"><p>گرفته از سپاهش دشت تارم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>سپهداری که آنجا بود بگریخت</p></div>
<div class="m2"><p>ابا دیلم به کوشش در نیاویخت</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>کجا دشمنش پر مایه کسی بود</p></div>
<div class="m2"><p>مرو را زان زمین لشکر بسی بود</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چو آگه شد از آن بدخواه ویرو</p></div>
<div class="m2"><p>شگفت آمدْش کار چرخ بدخو</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>که باشد کام و نازش جفت تیمار</p></div>
<div class="m2"><p>چو روز روشنست جفت شب تار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نه بی رنج است او را شادمانی</p></div>
<div class="m2"><p>نه بی مرگست او را زندگانی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بدو در انده از شادی فزونست</p></div>
<div class="m2"><p>دل دانا به دست او زبونست</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چو از موبد یکی شادیش بنمود</p></div>
<div class="m2"><p>به بدخواه دگر شادیش بربود</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>سپاهی شد ازُو پویان به راهی</p></div>
<div class="m2"><p>ز دیگر سو فراز آمد سپاهی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>هنوزش بود خون آلود خنجر</p></div>
<div class="m2"><p>هنوزش بود گرد آلود پیکر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>دگر ره کار جنگ دشمنان ساخت</p></div>
<div class="m2"><p>دگر ره پیکر کینه بر افراخت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>دگر ره خنجر پر خون بر آهیخت</p></div>
<div class="m2"><p>به جنگ شاه دیلم لشکر انگیخت</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چو ویرو رفت با لشکر بدان راه</p></div>
<div class="m2"><p>ز کارش آگهی آمد بر شاه</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>شهنشه در زمان از راه برگشت</p></div>
<div class="m2"><p>به راه اندر تو گفتی پرّور گشت</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چنان بشتاب لشکر را همی رانگ</p></div>
<div class="m2"><p>که باد اندر هوا زو باز پس پیکر</p></div></div>