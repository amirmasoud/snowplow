---
title: >-
    بخش ۳۱ - آگاه شدن شاه موبد از کار ویس و رامین
---
# بخش ۳۱ - آگاه شدن شاه موبد از کار ویس و رامین

<div class="b" id="bn1"><div class="m1"><p>چو رامین بود با خسرو یکی ماه</p></div>
<div class="m2"><p>به نخچیر و به رامش گاه و بیگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از یک مه به موقان خواست رفتن</p></div>
<div class="m2"><p>درو نخچیر دریایی گرفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهنشه خفته بود و ویس دربر</p></div>
<div class="m2"><p>دل اندر داغ آن خورشید دلبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که در بر داشت چونان دلفروزی</p></div>
<div class="m2"><p>ز پیوندش نشد دلشاد روزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیامد دایه پنهان ویس را گفت</p></div>
<div class="m2"><p>به چونین روز ویسا چون توان خفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که رامین رفت خواهد سوی ارمن</p></div>
<div class="m2"><p>به نخچیر شکار و جنگ دشمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپه را از شدنش آگاه کردند</p></div>
<div class="m2"><p>سرا پرده به دشت ماه بردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم اکنون بانگ کوس و نای رویین</p></div>
<div class="m2"><p>ز در گاهش رسد بر ماه و پروین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر خواهی که رویش باز بینی</p></div>
<div class="m2"><p>بسی نیکوتر از دیبای چینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی بر بام شو بنگر ز بامت</p></div>
<div class="m2"><p>که چون ناگه بخواهد رفت کامت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به تیر و یوز و باز و چرغ و شاهین</p></div>
<div class="m2"><p>شکار دلت ژواهد کرد رامین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخواهد رفتن و دوری ننودن</p></div>
<div class="m2"><p>ز تو آرام وز من جان ربودن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قصا را شاه موبد بود بیدار</p></div>
<div class="m2"><p>شنید از دایه آن وارونه گفتار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بجست از خوابگاه و تند بنشست</p></div>
<div class="m2"><p>چو پیل خشمناک آشفته و مست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زبان بگشاد بر دشمان دایه</p></div>
<div class="m2"><p>همی گفت ای پلید خوار مایه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به گیتی نی ز تو ناپارساتر</p></div>
<div class="m2"><p>ز سگ رسواتر و زو بی بهاتر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیارید این پلید بد کنش را</p></div>
<div class="m2"><p>بلایه گندپیر سگ منش را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که من کاری کنم باوی سزایش</p></div>
<div class="m2"><p>دهم مر دایگانی را جزایش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سزد گر ز آسمان بر شهر خوزان</p></div>
<div class="m2"><p>نبارد جاودان جز سنگ باران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که چونین روسپی خیزد از آن بوم</p></div>
<div class="m2"><p>ز بی شرمی و شوخی بر جهان شوم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بد آموزی کند مر کهتران را</p></div>
<div class="m2"><p>بد اندیشی کند مر مهتران را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز خوزان خود نیاید جز بداندیش</p></div>
<div class="m2"><p>تباهی جوی و بد کردار و بد کیش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مبادا کس که ایشان را پذیرد</p></div>
<div class="m2"><p>و زیشان دوست جوید دایه گیرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کزیشان دایگانی جست شهرو</p></div>
<div class="m2"><p>سرای خویش را پر کرد زاهو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه خوزانی به گاه دایگانی</p></div>
<div class="m2"><p>چه نا بینا به گاه دیدبانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آن کاو زاغ باشد رهنمایش</p></div>
<div class="m2"><p>به گورستان بود هنواره جایش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس آنگه گفت ویسا خویشکابا</p></div>
<div class="m2"><p>ز بهر دیو گشته زشت ناما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه جانت را خرد نه دیده را شرم</p></div>
<div class="m2"><p>نه رایت را راستی نه کارت آزرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بخوردی ننگ و شرم و زینهارا</p></div>
<div class="m2"><p>به ننگ اندر زدی خود را و مارا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز دین و راستی بیزار گشتی</p></div>
<div class="m2"><p>به چشم هر که بودی خوار گشتی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز تو نپسندد این آیین برادر</p></div>
<div class="m2"><p>نه نزدیکان و خویشان و نه مادر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به گونه رویشان چون دوده کردی</p></div>
<div class="m2"><p>که و مه را به ننگ آلوده کردی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همی تا دایه باشد رهنمایت</p></div>
<div class="m2"><p>بود دیو تباهی همسرایت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>معلم چون کند دستان نوازی</p></div>
<div class="m2"><p>کند کودک به پیشش پای بازی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس آنگه نزد ویرو کس فرستاد</p></div>
<div class="m2"><p>بخواند و کرد با او یک به یک یاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بفرمودش که خواهر را بفرهنج</p></div>
<div class="m2"><p>به شفشاهنگ فرهنجش در آهنج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همیدون دایه را لختی بپیرای</p></div>
<div class="m2"><p>به پادافراه و بر جانش مباخشای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر فرهنگشان من کرد بایم</p></div>
<div class="m2"><p>گزند افزون ز اندازه منایم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دو چشم ویس با آتش بسوزم</p></div>
<div class="m2"><p>وزان پس دایه را بر دار دوزم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز شهر خویش رامین را برانم</p></div>
<div class="m2"><p>دگر هر گز به نامش بر نخوانم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بپردازم ز رسوایی جهان را</p></div>
<div class="m2"><p>ز ننگ هر سه بزدایم روان را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نگه کن تا سمن بر ویس گل رخ</p></div>
<div class="m2"><p>به تندی شاه را چون داد پاسخ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر چه شرم بی اندازه بودش</p></div>
<div class="m2"><p>قصا شرم از دو دیده بر ربودش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز تخت شاه چون شمشاد بر جست</p></div>
<div class="m2"><p>به کش کرده بلورین بازو و دست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مرو را گفت شاها کامگارا</p></div>
<div class="m2"><p>چه ترسانی به پادافراه مارا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سخنها راست گفتی هر چه گفتی</p></div>
<div class="m2"><p>نکو کردی که آهو نا نهفتی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کنون خواهی بکش خواهی برانم</p></div>
<div class="m2"><p>و گر خواهی بر آور دیدگانم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>و گر خواهی ببند جاودان دار</p></div>
<div class="m2"><p>و گر خواهی بر هند کن به بازار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که رامینم گزین دو جهانست</p></div>
<div class="m2"><p>تنم را جان و جانم را روانست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چراغ چشم و آرام دلم اوست</p></div>
<div class="m2"><p>خداوندست و یار و دلبر و دوست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چه باشد گر به مهرش جان سپارم</p></div>
<div class="m2"><p>که من خود جان برای مهر دارم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>من از رامین وفا و مهربانی</p></div>
<div class="m2"><p>نبرم تا نبرد زندگانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرا آن رخ بر آن بالای چون سرو</p></div>
<div class="m2"><p>به دل بر خوشترست از ماه و از مرو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مرا رخسار او ماهست و خورشید</p></div>
<div class="m2"><p>مرا دیدار او کامست و امید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مرا رامین گرامی تر ز شهروست</p></div>
<div class="m2"><p>مرا رامین نیازی تر ز ویروست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بگتم راز پیشت آشکارا</p></div>
<div class="m2"><p>تو خواهی خشم کن خواهی مدارا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر خواهی بکش خواهی بر آویز</p></div>
<div class="m2"><p>نه کردم نه کنم از رام پرهیز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو با ویرو به من بر پادشایید</p></div>
<div class="m2"><p>به شاهی هر دوان فرمان روایید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گرم ویرو بسوزد یا ببندد</p></div>
<div class="m2"><p>پسندم هر چه او بر من پسندد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>و گر تیغ تو از من جان ستاند</p></div>
<div class="m2"><p>مرا این نام جاویدان بماند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>که جان بسپرد ویس از بهر رامین</p></div>
<div class="m2"><p>به صد جان می خرم من نام چونین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>و لیکن تابود بر جای زنده</p></div>
<div class="m2"><p>شکاری شیر جان گیر و دمنده</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که دل دارد کنامش را شکفتن</p></div>
<div class="m2"><p>که یارد بچگانش را گرفتن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هزاران سال اگر رامین بماند</p></div>
<div class="m2"><p>که دل دارد که جان من ستاند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو در دستم بود دریای سر کش</p></div>
<div class="m2"><p>چرا پرهیزم از سوزنده آتش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مرا آنگه توانی زو بریدن</p></div>
<div class="m2"><p>که تو مردم توانی آفریدن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مرا نز مرگ بیمست و نه از درد</p></div>
<div class="m2"><p>ببین تا که چه چاره بایدت کرد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو بشنید این سحن ویرو ز خواهر</p></div>
<div class="m2"><p>برو آن حال شد از مرگ بدتر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>برفت و ویس را در خانه ای برد</p></div>
<div class="m2"><p>بدو گفت این نبد پتیاره ای خرد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که تو در پیش من با شاه کردی</p></div>
<div class="m2"><p>هم آب خود هم آب من ببردی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ترا از شاه و از من شرم ناید</p></div>
<div class="m2"><p>که رامین بایدت موبد نباید</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نگویی تا تو از رامین چه دیدی</p></div>
<div class="m2"><p>چرا او را ز هر کس بر گزیدی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به گنجش در چه دارد مرد گنجور</p></div>
<div class="m2"><p>بجز رود و سرود و چنگ و طنبور</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همین داند که طنبوری بسازد</p></div>
<div class="m2"><p>بر او راهی و دستانی نوازد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نبینندش مگر مست و خرشان</p></div>
<div class="m2"><p>نهاده جامه نزد می فروشان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>جهودانش حریف و دوستانند</p></div>
<div class="m2"><p>همیشه زو بهای می ستانند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ندانم تو بدو چون او فتادی</p></div>
<div class="m2"><p>به مهر او را دل از بهر چه دادی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کنون از شرم و از مینو بیندیش</p></div>
<div class="m2"><p>مکن کاری کزو ننگ آیدت پیش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو شهرو مادر و چون من برادر</p></div>
<div class="m2"><p>چرا داری به ننگ خویش در خور</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نماندست از نیاکان تو جز نام</p></div>
<div class="m2"><p>به زشتی نام ایشان را مکن خام</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مضو یکباره کام دیو را رام</p></div>
<div class="m2"><p>بده نام دو گیتی از پی رام</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>اگر رامین همه نوش است و شکر</p></div>
<div class="m2"><p>بهشت جاودان زو هست خوشتر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بگفتم آنچه من دانستم از پیش</p></div>
<div class="m2"><p>تو به دان خدا و شوهر خویش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>همی گفت این سحن ویرو به خواهر</p></div>
<div class="m2"><p>همی بارید ویس از دیده گوهر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بدو گفت ای برادر راست گفتی</p></div>
<div class="m2"><p>درخت راستی را بر تو رفتی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>روانیم نه چنان در آتش افتاد</p></div>
<div class="m2"><p>که آید هیچ پند او را به فریاد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دل من نه چنان در مهر بشکست</p></div>
<div class="m2"><p>که داند مردم او را باز پیوست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>قصا بر من برفت و بودنی بود</p></div>
<div class="m2"><p>از این اندرز و زین گفتار چه سود</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>در خانه کنون بستن چه سودست</p></div>
<div class="m2"><p>که دزدم هرچه در خانه ربودست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>مرا رامین به مهر اندر چنان بست</p></div>
<div class="m2"><p>که نتوانم ز بندش جاودان رست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اگر گویم یکی زین هر دو بگزین</p></div>
<div class="m2"><p>بهشت جاودان و روی رامین</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به جان من که رامین را گزینم</p></div>
<div class="m2"><p>که رویش را بهشت خویش بینم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو بشنید این سحن ویرو ز خواهر</p></div>
<div class="m2"><p>دگر بر خوگ نفشاند ایچ گوهر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>برفت از پیش ایشان دل پر آزار</p></div>
<div class="m2"><p>سفرده کار ایشان را به دادار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو خورشید جهان بر چرخ گردان</p></div>
<div class="m2"><p>چو زرین گوی شد بر روی میدان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>شهنشه گوی زد با نامداران</p></div>
<div class="m2"><p>بجوشیده در آن میدان سواران</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز یک سو شاه موبد بود سالار</p></div>
<div class="m2"><p>ز گردان بر گزیده بیست همکار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ز یک سو شاه ویرو بود مهتر</p></div>
<div class="m2"><p>ز گردان بر گزیده بیست یاور</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>رفیدا یار موبد بود و رامین</p></div>
<div class="m2"><p>چو ارغش یار ویرو بود و شروین</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دگر آزادگان و نامداران</p></div>
<div class="m2"><p>بزرگان و دلیران و سواران</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>پس آنگه گوی در میدان فگندند</p></div>
<div class="m2"><p>به چوگان گوی بر کیوان فگندند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>هنر آن روز ویرو کرد و رامین</p></div>
<div class="m2"><p>گه این زان گوی برد و گاه آن زین</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ز چندان نامداران هنر جوی</p></div>
<div class="m2"><p>به از رامین و ویرو کس نزد گوی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز بام گوشک ویس ماه پیکر</p></div>
<div class="m2"><p>نگه می کرد با خوبان لشکر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>برادر را و رامین را همی دید</p></div>
<div class="m2"><p>ز چندان مردم ایشان را پسندید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز بس اندیشه کردن گشت دلتنگ</p></div>
<div class="m2"><p>رخش بی رنگ و پیشانی پر آژنگ</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تن سیمینش را لرزه بیفتاد</p></div>
<div class="m2"><p>تو گفتی سرو بد لرزند از باد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>خمارین نر گسان را کرد پر آب</p></div>
<div class="m2"><p>به گل بر ریخت مروارید خوشاب</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>به شیرین لابه دایه گفت با ویس</p></div>
<div class="m2"><p>چرا بر تو چنین شد چیره ابلیس</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چرا با جان خود چندین ستیزی</p></div>
<div class="m2"><p>چرا بیهوده چندین اشک ریزی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نه بابت قارنست و مام شهرو</p></div>
<div class="m2"><p>نه شویت موبدست و پشت ویرو</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>نه تو امروز ویس خوب چهری</p></div>
<div class="m2"><p>میان ماه رویان همچو مهری</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نه ایران را توی بابوی مهتر</p></div>
<div class="m2"><p>نه توران را توی خاتون دلبر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به ایران و به توران نامداری</p></div>
<div class="m2"><p>که بر ایران و توران کامگاری</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به روی از گل به موی از مشک نابی</p></div>
<div class="m2"><p>ستیز ماه و رشک آفتابی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به شاهی و به خوبی نام داری</p></div>
<div class="m2"><p>چو رامین دوستی خود کام داری</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>اگر صد گونه غم داری به دل بر</p></div>
<div class="m2"><p>نماند چون ببینی روی دلبر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>فلک خواهد که چون تو ماه دارد</p></div>
<div class="m2"><p>جهان خواهد که چون او شاه دارد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چرا خوانی ز یزدان خیره فریاد</p></div>
<div class="m2"><p>که در گیتی بهشت خود ترا داد</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>مکن بر بخت چندین ناپسندی</p></div>
<div class="m2"><p>که آرد نا پسندی مستمندی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چه دانی خواست از بخشنده یزدان</p></div>
<div class="m2"><p>ازین بهتر که دادست به گیهان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>خداوندی و خوبی و جوانی</p></div>
<div class="m2"><p>تن آسانی و ناز و کامرانی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چو چیزی زین که داری بیش خواهی</p></div>
<div class="m2"><p>ز بیشی خواستن یابی تباهی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مکن ماها به بخت خویش ببسند</p></div>
<div class="m2"><p>بدین کت داد یزدان باش حرسند</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>به تندی شاه را چندین میازار</p></div>
<div class="m2"><p>برادر را مکن بر خود دل آزار</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>که این آزارها چون قطر باران</p></div>
<div class="m2"><p>چو گرد آید شود یک روز طوفان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>جوابش داد خورشید سخن گوی</p></div>
<div class="m2"><p>نگار سر و قدّ یاسمین بوی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بگفت ای دایه تاکی یافه گویی</p></div>
<div class="m2"><p>ز نادانی در آتش آب جویی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>مگر نشنیدی از گیتی شناسان</p></div>
<div class="m2"><p>که باشد جنگ بر نظاره آسان</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>مگر نشنیدی این زرّینه گفتار</p></div>
<div class="m2"><p>که بر چشم کسان درد کسان خوار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>منم همچون پیاده تو سواری</p></div>
<div class="m2"><p>ز رنج رفتن آگاهی نداری</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>منم بیمار و نالان تو درستی</p></div>
<div class="m2"><p>ندانی چیست بر من درد و سستی</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>مرا شاه جهان سالار و شویست</p></div>
<div class="m2"><p>و لیکن بدسگال و کیته جویست</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>اگر شویست بس نا دلپذیرست</p></div>
<div class="m2"><p>کجا بد رای و بد کردار و پیرست</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>و گر ویروست بر من بد گمانس</p></div>
<div class="m2"><p>به چشم من چو دینار کسانست</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>و گر ویرو و به جز ماه سما نیست</p></div>
<div class="m2"><p>مرا چه سود باشد چون مرا نیست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>و گر رامین همه ژوبی و زیبست</p></div>
<div class="m2"><p>تو خود دانی چگونه دل فریبست</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>ندارد مایه جز شیرین زبانی</p></div>
<div class="m2"><p>نجوید راستی در مهرتبانی</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>زبانش را شکر آمد نمایش</p></div>
<div class="m2"><p>نهانش حنظل اندر آزمایش</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>منم با یار در صد کار بی کار</p></div>
<div class="m2"><p>به گاه مهر با صد یار بی یار</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>همم یارست و هم شو هم برادر</p></div>
<div class="m2"><p>من از هر سه همی سوزی بر آذر</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>مرا نامی رسید از شوی داری</p></div>
<div class="m2"><p>مرا رنجی رسید از مهر کاری</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ه شوی من چو شوی بانوانست</p></div>
<div class="m2"><p>نه یار من چو یار نیکوانست</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>چه باید مر مرا آن شوی و آن یار</p></div>
<div class="m2"><p>کزو باشد به جانم رنج و تیمار</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>مرا آن طشت زرین نیست در خور</p></div>
<div class="m2"><p>که دشمن خون من ریزد در و در</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>اگر بختم مرا یاری ننودی</p></div>
<div class="m2"><p>دلارامم به جز ویرو نبودی</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>نه موبد جفت من بودی نه رامین</p></div>
<div class="m2"><p>نبهره دوستان دشمن آیین</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>یکی با من چو غم با جان به گینه</p></div>
<div class="m2"><p>یکی دیگر چو سنگ و آبگینه</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>یکی را با زبان دل نیست یاور</p></div>
<div class="m2"><p>یکی را این و آن هر دو ستمگر</p></div></div>