---
title: >-
    بخش ۴ - گفتار اندر ستایش خواجه ابو نصر منصور بن مهمد
---
# بخش ۴ - گفتار اندر ستایش خواجه ابو نصر منصور بن مهمد

<div class="b" id="bn1"><div class="m1"><p>چو ایزد بنده ای را یار باشد</p></div>
<div class="m2"><p>دو چشم دولتش بیدار باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پیروزی به دست آرد همه کام</p></div>
<div class="m2"><p>ز به روزی به چنگ آرد همه نام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا چیزی بود زیبا و شهوار</p></div>
<div class="m2"><p>کجا مردی بود شایستهء کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهد یزدان بدان بنده سراسر</p></div>
<div class="m2"><p>که او باشد بدان هنواره در خور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدین گونه که داد اکنون به سلطان</p></div>
<div class="m2"><p>گزین از هرچه تو دانی به گیهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه مردان در گاهش چنانند</p></div>
<div class="m2"><p>که با ایشان دگر مردان زنانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیکن هست ازیشان نامداری</p></div>
<div class="m2"><p>دلیری کاردانی هوشیاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکیمی زیر کی مرد آزمایی</p></div>
<div class="m2"><p>کریمی نیکخویی نیک رایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخنگویی سخندانی ظریفی</p></div>
<div class="m2"><p>هنرمندی هنرجویی لطیفی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا در گاه سلطان را عمیدست</p></div>
<div class="m2"><p>به هر کاری و هر حالی حمیدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیروزی و بهروزی مؤیّد</p></div>
<div class="m2"><p>ابونصراست و منصور و محمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خداوندی که از نیکی جهانیست</p></div>
<div class="m2"><p>دُرو رای بلندش آسمانیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازین گیتی سوی دانش گراید</p></div>
<div class="m2"><p>ز دانش یافتن رامش فزاید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همیشه نام نیکو دوست دارد</p></div>
<div class="m2"><p>ابی حقی که باشد حق گزارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کم آزار است و بر مردرم فروتن</p></div>
<div class="m2"><p>مرو را الجرم کس نیست دشمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرا دشمن بود آنرا که جانس</p></div>
<div class="m2"><p>همی بخشاید از خواهندگانش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خرد را پیش خود دستور دارد</p></div>
<div class="m2"><p>دل از هر ناپسندی دور دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر آوازی بداند چون سلیمان</p></div>
<div class="m2"><p>هزاران دیو را دارد به فرمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به رادی هست از حاتم فزونتر</p></div>
<div class="m2"><p>به مردی بهترست از رستم زر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان گوید زبان هفت کضور</p></div>
<div class="m2"><p>که گویی زان زمینش بود گوهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طرازی ظنّ برد کاو از طرازست</p></div>
<div class="m2"><p>حجازی نیز گوید از حجازست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو نثر هر زبانش خوشتر آید</p></div>
<div class="m2"><p>به نظم آن زبان معجز نماید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دری و تازی و ترکی بگوید</p></div>
<div class="m2"><p>به الفاظی که زنگ از دل بضوید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دو شمشیرست ز الماس و بیانش</p></div>
<div class="m2"><p>یکی در دست و دیگر در دهانش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی گاه هنر خارا گذارإد</p></div>
<div class="m2"><p>یکی گاه سخن دانش نگارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بسا گُردا کزان گشته ست پیچان</p></div>
<div class="m2"><p>بسا جانا کزین گشته ست بی جان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که و مه لشکر سلطان عالم</p></div>
<div class="m2"><p>به جان وی خورند سوگند محکم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو با کهتر ز خود، سازد پدروار</p></div>
<div class="m2"><p>چو با مهتر، همی سازد پسروار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو با همسران مثل برادر</p></div>
<div class="m2"><p>نباشد زادمردی زین فزونتر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهر فن گرد او جمع حکیمان</p></div>
<div class="m2"><p>خطیبان و دبیران و ادیبان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز هر شهری بدو گرد آمدستند</p></div>
<div class="m2"><p>به بحر جود او غرقه شدستند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر او نیستی ما را خریدار</p></div>
<div class="m2"><p>نبودی شاعری را هیچ مقدار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>و گر چه شاعری باشد نه دانا</p></div>
<div class="m2"><p>بسی احسنت و زه گوید به عمدا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی از بهر آن تا کاو شود شاد</p></div>
<div class="m2"><p>دگر تا بیشتر باید عطا داد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز مشرق تا به مغرب کار گیهان</p></div>
<div class="m2"><p>به زیر امر و کردست سلطان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بروبر نیست چندان رنج از این کار</p></div>
<div class="m2"><p>که از یک جام می بر دست میخوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بزرگا جود دادار جهان بین</p></div>
<div class="m2"><p>که بخشد مردمی راا فصل چندین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>الا تا در جهان کون و فسادست</p></div>
<div class="m2"><p>وزیشان خاک مبادا و معادست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بقا باد این کریم نیکخو را</p></div>
<div class="m2"><p>بر افزون باد جاه و دولت او را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه بخت او پیروز گرباد</p></div>
<div class="m2"><p>به پیروزی و نیکی نامور باد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>متابع باد او را ملک گیهان</p></div>
<div class="m2"><p>موافق باد وی را فرّ یزدان</p></div></div>