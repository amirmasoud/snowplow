---
title: >-
    بخش ۱۰۴ - نامه نوشتن ویس به پیش رامین
---
# بخش ۱۰۴ - نامه نوشتن ویس به پیش رامین

<div class="b" id="bn1"><div class="m1"><p>حریر و مشک و عنبر خواست و خامه</p></div>
<div class="m2"><p>ز درد دل به رامین کرد نامه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن در نامه از زاری چنان بود</p></div>
<div class="m2"><p>که خون از حرفهای او چکان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الا ای مهربان مهر پرور</p></div>
<div class="m2"><p>چنین کن نامه نزد یار دلبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا این نامه گر خوانی تو بر سنگ</p></div>
<div class="m2"><p>ز سنگ آید به گوشت نالهء چنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز یار مهربان و عاشق زار</p></div>
<div class="m2"><p>به یار سنگدل وز مهر بیزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بی دل بندهء بی خواب و بی خور</p></div>
<div class="m2"><p>سپرده دل به شاهی چون مه و خور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نالان عاشق بیمار و مهجور</p></div>
<div class="m2"><p>به کام دشمنان وز کام دل دور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پیچان چاکری سوزان بر آتش</p></div>
<div class="m2"><p>جهانش تیره گشته بخت سر کش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گریان خادمی بدبخت مسکین</p></div>
<div class="m2"><p>روان از دیدگانش سیل خونین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پی خسته دلی خسته روانی</p></div>
<div class="m2"><p>عقیقین دیده ای زرین رخانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نژندی مستمندی دردمندی</p></div>
<div class="m2"><p>شده بر تنش هر مویی چو بندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نزاری بی قراری دلفگاری</p></div>
<div class="m2"><p>ز هر چشمی رونده رودباری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نوشتم نامه در حال چنین سخت</p></div>
<div class="m2"><p>که چون من نیت اکنون ایچ بد بخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تنم پیچان و چشمم زار و گریان</p></div>
<div class="m2"><p>دلم بر آتش تیمار بریان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تنم چون شمع سوزان اشک ریزان</p></div>
<div class="m2"><p>چو ابر تیره از دل دود خیزان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلا را مونش و غم را رفیقم</p></div>
<div class="m2"><p>به دریای جدایی در غریقم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه مسکینم که گریم زار چندین</p></div>
<div class="m2"><p>یکی دستم به دل دیگر به بالین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عقیق دو لبم پیروز گشته</p></div>
<div class="m2"><p>جهان بر حال من دل سوز گشته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی چشم و هزار ابر گهی بار</p></div>
<div class="m2"><p>یکی جان و هزاران گونه تیمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قراق آمد همه راز نهانم</p></div>
<div class="m2"><p>به خونابه نویسد بر رخانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز جان من یکی آتش بر افروخت</p></div>
<div class="m2"><p>که صبر و رامشم در دل همی سوخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو دریا کرد چشمم را ز بس آب</p></div>
<div class="m2"><p>کنون در آب چشمم غرقه شد خواب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جو جای خواب را پر آب یابم</p></div>
<div class="m2"><p>به آب اندر چگونه خواب یابم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدان دستی که این نامه نبشتم</p></div>
<div class="m2"><p>بسات خرمی را در نوشتم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تنم بگداخت از بس رنج دیدن</p></div>
<div class="m2"><p>دلم بگریخت از بس غم کشیدن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز گیتی چون توانم کام جستن</p></div>
<div class="m2"><p>که جانم را نه دل ماندست نه تن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرا بردی من آن روی چون خور</p></div>
<div class="m2"><p>که چون جان و روانم بود در خور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همی تا دور ماندستم ز رویت</p></div>
<div class="m2"><p>ز باریکی نمانم جز به مویت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به روز انده گسارم آفتابست</p></div>
<div class="m2"><p>که چون رخسار تو با نور و تابست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به شب انده گسارم اخترانند</p></div>
<div class="m2"><p>که چون بینم به دندان تو مانند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خطا گفتم نه آن اندوه دارم</p></div>
<div class="m2"><p>که باشد هیچ کس انده گسارم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر رنج مرا کوه آزماید</p></div>
<div class="m2"><p>به جای آب ازو جز خون نیاید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نصیحت می کنندم دوستانم</p></div>
<div class="m2"><p>ملامت می کنندم دشمنانم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بس کردن نصیحت یا ملامت</p></div>
<div class="m2"><p>مرا کردند در گیتی علامت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه مهرست این که انده بار میغست</p></div>
<div class="m2"><p>نه هجرست این که زهر آلوده تیغست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چرا مردم دل اندر مهر بندد</p></div>
<div class="m2"><p>چرا این بد به جان خود پسندد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر چون من بود هر مهربانی</p></div>
<div class="m2"><p>مبان از مهر در گیتی نشانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسا روزا که خندیدم بریشان</p></div>
<div class="m2"><p>کنون گشتم ز خندیدن پشیمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بخندیدم بریشان همچو دشمن</p></div>
<div class="m2"><p>کنون ایشان همی گریند برمن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرا دیدی ز پیش مهربانی</p></div>
<div class="m2"><p>فروزان تر ز مهر آسمانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کنون بالای سروینم کمان شد</p></div>
<div class="m2"><p>گل رخسارگانم زعفران شد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر دو تا شود شاخ گرانبار</p></div>
<div class="m2"><p>تنم دو تا شدست از بار تیمار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به پیکر چون کمان گشتم خمیده</p></div>
<div class="m2"><p>چو زه بر تن کشیده خون دیده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا ایدر بدین زاری بماندی</p></div>
<div class="m2"><p>برفتی رخش فُرقت را براندی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>غباری کز سم اسپت بجستست</p></div>
<div class="m2"><p>چو پیکان در دو چشم من نشستست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خیال روی تو در دیدگانم</p></div>
<div class="m2"><p>همی گرید ز راه دیده جانم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا گویند بیهوده چه نالی</p></div>
<div class="m2"><p>که از بسیار نالیدن چو نالی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به روز رفته ماند یار رفته</p></div>
<div class="m2"><p>چرا داری به دل تیمار رفته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه چونین است کاندیشید بد گوی</p></div>
<div class="m2"><p>میم بر ریخت لیکن نامدش بوی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شبست اکنون و خورشیدم برفتست</p></div>
<div class="m2"><p>جهان همواره تاریکی گرفتست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>روا باشد که بنشینم به اومید</p></div>
<div class="m2"><p>که باز آید به گاه بام خورشید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بهار رفته باز آید به نوروز</p></div>
<div class="m2"><p>نگارم نیز باز آید یکی روز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نگارا سر و قدا ماهرویا</p></div>
<div class="m2"><p>سوارا شیر گیرا نامجویا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>من اندر مهر آنم کم تو دانی</p></div>
<div class="m2"><p>که دارم جان فدای مهربانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یکی تا موی تو بر من چنانست</p></div>
<div class="m2"><p>که صد باره گرامی تر ز جانست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ترا خواهم نخواهم پاک جان را</p></div>
<div class="m2"><p>ترا جویم نجویم این جحان را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرا در مهر بسیار آزمودی</p></div>
<div class="m2"><p>به مهر اندر ز من خشنود بودی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کنون اندر وفای تو همانم</p></div>
<div class="m2"><p>گوا دارم ز خونین دیدگانم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر تو بر وفایم نه یقینی</p></div>
<div class="m2"><p>بیا تا این گواهان را ببینی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیا تا روی من بینی چو دینار</p></div>
<div class="m2"><p>بر آن دینار باران دُرّ شهوار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیا تا چشم من بینی چو جیحون</p></div>
<div class="m2"><p>جهان از هر دو جیحونم پر از خون</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بیا تا قد من بینی خمیده</p></div>
<div class="m2"><p>نشاط از من من از مردم رمیده</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بیا تا حال من بینی چنان زار</p></div>
<div class="m2"><p>که هستم راست چون دهساله بیمار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بیا تا بخت من بینی چنان شور</p></div>
<div class="m2"><p>که گویی هر زمان چشمم شود کور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بیا تا مهر من بینی بر افزون</p></div>
<div class="m2"><p>شده چون حسنت از اندازه بیرون</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر نه زود نزد من شتابی</p></div>
<div class="m2"><p>چو باز آیی مرا زنده نیابی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اگر خواهی که رویم باز بینی</p></div>
<div class="m2"><p>نه آسایی نه خسپی نه نشینی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو این نامه بخوانی باز گردی</p></div>
<div class="m2"><p>سه روزه ره به روزی در نوردی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همی تا تو رسی فریاد جانم</p></div>
<div class="m2"><p>به راهت بر نشسته دیدبانم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اگر جانم نگیرد رنج و دردم</p></div>
<div class="m2"><p>ز درد عاشقی دیوانه گردم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز دادار این همی خواهم شب و روز</p></div>
<div class="m2"><p>که رویت باز بینم ای دل افروز</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>درود از من فزون از قطر باران</p></div>
<div class="m2"><p>بر آن ماه من و شاه سواران</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>درود از من فزون از آب دریا</p></div>
<div class="m2"><p>بر آن خورشید چهر سرو بالا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>خدایا جان من بگذار چندان</p></div>
<div class="m2"><p>که بینم روی او آنگاه بستان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که با این داغ گر جانم بر آید</p></div>
<div class="m2"><p>ز دود جان من گیتی سر اید</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو ویس دلبر از نامه بپرداخت</p></div>
<div class="m2"><p>نوندی تیزتگ را سوی اوتاخت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز نزدیکان او مردی دلاور</p></div>
<div class="m2"><p>بشد بر کوههء کوهی تگاور</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>که چون کرگس به کوهان بر گذشتی</p></div>
<div class="m2"><p>بیابان را چو نامه در نوشتی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نه شب خفت و نه روز آسود در راه</p></div>
<div class="m2"><p>به رامین برد چونین نامهء ماه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو رامین نامهء سرو روان دید</p></div>
<div class="m2"><p>تو گفتی صورت بخت جوان دید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ببوسیدش به دو یاقوت و شکر</p></div>
<div class="m2"><p>نهادش بر خمارین چشم و برسر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو بند نامه بگشاد و فرو خواند</p></div>
<div class="m2"><p>ز دیده سیل بیجاده بر افشاند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بر آمد دود بی صبری ز جانش</p></div>
<div class="m2"><p>ببارید آب حسرت بر رخانش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سخنهایی بگفت از جان پرتاب</p></div>
<div class="m2"><p>که شاید گر نویسندش به زر آب</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دلا تا کی روا داری چنین حال</p></div>
<div class="m2"><p>که از غم ماه بینی وز بلا سال</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دلا آن کس که کام و نام جوید</p></div>
<div class="m2"><p>نه با فرهنگ و با آرام جوید</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نترسد بی دل از شمشیر بران</p></div>
<div class="m2"><p>نه از پیل دمان و شیر غران</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نه از برف و دمه نز موج دریا</p></div>
<div class="m2"><p>نه از باران نه از سرما و گرما</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>دلا گر عاشقی چندین چه ترسی</p></div>
<div class="m2"><p>ز هر کس چاره و درمان چه پرسی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز تو فریاد و زاری که نیوشد</p></div>
<div class="m2"><p>چو تو خود را نکوشی پس که کوشد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چه باید مهر با چندین زبونی</p></div>
<div class="m2"><p>ترا کمی و دشمن را فزونی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به سر باز افگن این بار گران را</p></div>
<div class="m2"><p>ز دل بیرون کن این راز نهان را</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خوشی کی بیند از کام نهانی</p></div>
<div class="m2"><p>که با هر سود بینی صد زیانی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگر یک روز باشد شاد خواری</p></div>
<div class="m2"><p>یکی سالت بود زاری و خواری</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>کنون یا بند را باید گشادن</p></div>
<div class="m2"><p>و یا یکباره سر بر سر نهادن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نیابم بهتر از دستم برادر</p></div>
<div class="m2"><p>برادر را به از شمشیر یاور</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نه مردم گر کنم زین پس مدارا</p></div>
<div class="m2"><p>بهل تا گردد این راز آشکارا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>جهان جز مرگ پیش من چه آرد</p></div>
<div class="m2"><p>بجز شمشیر بر جانم چه بارد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز دشمان کی حذر جوید خطرجوی</p></div>
<div class="m2"><p>ز دریا کی بپرهیزد گهی جوی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به دریا در گهی جفت نهنگست</p></div>
<div class="m2"><p>چو نوش اندر جهان جفت شرنگست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>شراب کام را جامست شمشیر</p></div>
<div class="m2"><p>چو راه خرمی را راهبان شیر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ز شیران بر گذر وز جام خور می</p></div>
<div class="m2"><p>که دی مه را بود نوروز در پی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ز آسانی نیابی شادمانی</p></div>
<div class="m2"><p>ز بی رنجی نیابی کامرانی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>فراوان رنج یابد دام داری</p></div>
<div class="m2"><p>به دشت و کوه تا گیرد شکاری</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>شکاری نیست چون شاهی و فرمان</p></div>
<div class="m2"><p>مرو را چون بگیرد مردم آسان</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>مرا در پیش چون شاهی شکارست</p></div>
<div class="m2"><p>چو دلبر ویس مه پیکر نگارست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چرا با بخت خود چندین شتیزم</p></div>
<div class="m2"><p>چرا آبی برین آتش نریزم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چرا در خیرگی چندین نشینم</p></div>
<div class="m2"><p>چرا بیرون نیایم زین کمینم</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>من اندر دام و یارم نیز در دام</p></div>
<div class="m2"><p>نهاده دل به درد و رنج ناکام</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چرا این دام را بر هم ندرم</p></div>
<div class="m2"><p>درخت ننگ را از بن نبتم</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>و لیکن چیزها را جایگاهست</p></div>
<div class="m2"><p>همیدون کارها را وقتها هست</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>شکوفه کاو بر آید ماه نیسان</p></div>
<div class="m2"><p>به دی مه بر درختان یافت نتوان</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>مگر روز بلا اکنون سر آمد</p></div>
<div class="m2"><p>برفت آن روز روز دیگر آمد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>گذشت از رنج ما دی ماه سختی</p></div>
<div class="m2"><p>کنون آمد بهار نیکبختی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو رامین گفت ازین سان چند گفتار</p></div>
<div class="m2"><p>ز درد دل همی پیچید چون مار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>تنس در راه بود و دل بر ویس</p></div>
<div class="m2"><p>به چشم اندر بمانده پیکر ویس</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>قرارش رفته بود و صبر تا شب</p></div>
<div class="m2"><p>ز دود دل نشسته گرد بر لب</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>به خاور بود چشمش تا کی آید</p></div>
<div class="m2"><p>سپاه شب که راهش بر گشاید</p></div></div>