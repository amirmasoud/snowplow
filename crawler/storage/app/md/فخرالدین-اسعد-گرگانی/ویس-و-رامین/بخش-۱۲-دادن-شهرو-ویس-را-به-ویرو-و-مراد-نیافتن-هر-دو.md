---
title: >-
    بخش ۱۲ - دادن شهرو  ویس را به ویرو و مراد نیافتن هر دو
---
# بخش ۱۲ - دادن شهرو  ویس را به ویرو و مراد نیافتن هر دو

<div class="b" id="bn1"><div class="m1"><p>چو مادر دید ویس دلستان را</p></div>
<div class="m2"><p>به گونه خوار کرده گلستان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفت ای همه خوبی و فرهنگ</p></div>
<div class="m2"><p>جهان را از تو پیرایه ست و اورنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا خسرو پدر بانوت مادر</p></div>
<div class="m2"><p>ندانم در خورت شویی به کضور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو در گیتی ترا همسر ندانم</p></div>
<div class="m2"><p>به ناهمسرت دادن چون توانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ایران نیست جفتی با تو همسر</p></div>
<div class="m2"><p>مگر ویرو که هستت خود برادر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو او را جفت باش و دوده بفروز</p></div>
<div class="m2"><p>وزین پیوند فرّخ کن مرا روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زن ویرو بود شایسته خواهر</p></div>
<div class="m2"><p>عروس من بود بایسته دختر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازان خوشتر نباشد روزگارم</p></div>
<div class="m2"><p>که ارزانی به ارزانی سپارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بشنید این سخن ویسه ز مادر</p></div>
<div class="m2"><p>شد از بس شرم رویش چون مُعصفر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجنیدش به دل بر مهربانی</p></div>
<div class="m2"><p>نمود از خامشی همداستانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگفت از نیک و بد بر روی مادر</p></div>
<div class="m2"><p>که بود اندر دلش مهر برادر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلش از مهربانی شادمان شد</p></div>
<div class="m2"><p>فروزان همچو ماه آسمان شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رنگی می شدی هر دم عذارش</p></div>
<div class="m2"><p>به رو افتاده زلف تابدارش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدانست از دلش مادر همانگاه</p></div>
<div class="m2"><p>که آمد دخترش را خامشی راه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا او بود پیر کار دیده</p></div>
<div class="m2"><p>بد و نیک جهان بسیار دیده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به بُرنایی همان حال آه</p></div>
<div class="m2"><p>همان خاموش او را نیز بوده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دید از مهر دختر را نکو رای</p></div>
<div class="m2"><p>بخواند اخترشناسان را ز هر جای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بپرسید از شمار آسمانی</p></div>
<div class="m2"><p>کزو کی سود باشد کی زیانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از اختر کی بود روز گزیده</p></div>
<div class="m2"><p>بد بهرام و کیوان زو بریده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که بیند دخترش شوی و پسر زن</p></div>
<div class="m2"><p>که بهتر آن ز هر شوی این ز هر زن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه اختر شناسان زیج بردند</p></div>
<div class="m2"><p>شمار اختران یک یک بکردند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو گردشهای گردون را بدیدند</p></div>
<div class="m2"><p>ز آذر ماه روزی بر گزیدند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کجا آنگه ز گشت روزگاران</p></div>
<div class="m2"><p>در آذر ماه بودی نوبهاران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو آذر ماه روز دی در آمد</p></div>
<div class="m2"><p>همان از روز شش ساعت بر آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به ایوان کیانی رفت شهرو</p></div>
<div class="m2"><p>گرفته دست ویس و دست ویرو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بسی کرد آفرین بر پاک دادار</p></div>
<div class="m2"><p>چو بر دیو دژم نفرین بسیار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سروشان را به نام نیک بستود</p></div>
<div class="m2"><p>نیایشهای بی اندازه بنمود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس آنگه گفت با هر دو گرامی</p></div>
<div class="m2"><p>شما را باد ناز و شاد کامی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نباید زیور و چیزی دلارای</p></div>
<div class="m2"><p>برادر را و خواهر را به یک جای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به نامه مُهر موبد هم نباید</p></div>
<div class="m2"><p>گوا گر کس نباشد نیز شاید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گواتان بس بود دادار داور</p></div>
<div class="m2"><p>سروش و ماه و مهر و چرخ و اختر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس آنگه دست ایشان را به هم داد</p></div>
<div class="m2"><p>بسی کرد آفرین بر هر دوان یاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که شال و ماهتان از خرّمی باد</p></div>
<div class="m2"><p>همیشه کارتان از مردمی باد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به نیکی یکدگر را یار باشید</p></div>
<div class="m2"><p>وزین پیوند بر خوردار باشید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بمانید اندرین پیوند جاوید</p></div>
<div class="m2"><p>فروزنده به هم چون ماه و خورشید</p></div></div>