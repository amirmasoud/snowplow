---
title: >-
    بخش ۳۷ - سرزنش کردن موبد  ویس را
---
# بخش ۳۷ - سرزنش کردن موبد  ویس را

<div class="b" id="bn1"><div class="m1"><p>چو در مرو گزین شد شاه شاهان</p></div>
<div class="m2"><p>دلش خرم به روی ماه ماهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روی ویس بودی آفتابش</p></div>
<div class="m2"><p>ز موی ویس بودی مشک نابش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشسته شاد روزی با دلارام</p></div>
<div class="m2"><p>سخن گفت از هوای ویس با رام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بنشستی به بوم ماه چندین</p></div>
<div class="m2"><p>ز بهر آنکه جفتت بود رامین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر رامین نبودی غمگسارت</p></div>
<div class="m2"><p>نبودی نیم روز آنجا قرارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوابش داد خورشید سمن بر</p></div>
<div class="m2"><p>مبر چندین گمان بد به من بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی گویی که با تو بود ویرو</p></div>
<div class="m2"><p>کنی دیدار ویرو بر من آهو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی گویی که با تو بود رامین</p></div>
<div class="m2"><p>چرا بر من زنی بیغاره چندین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدان دوزخ بدان گرمی که گویند</p></div>
<div class="m2"><p>نه اهریمن بدان زشتی که جویند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه دزد را دزدی بود کار</p></div>
<div class="m2"><p>دروغش نیز هم گویند بسیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو خود دانی که ویرو چون جوانست</p></div>
<div class="m2"><p>به دشت و کوه بر نخچیر گانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نداند کار جز نخچیر کردن</p></div>
<div class="m2"><p>نشستن با بزرگان باده خوردن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به عادت نیز رامین همچنین است</p></div>
<div class="m2"><p>مرو را دوستدار راستین است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به هم بودند هر دو چون برادر</p></div>
<div class="m2"><p>نشسته روز و شب با رود و ساغر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جوان را هم جوان باشد دلارام</p></div>
<div class="m2"><p>کجا باشد جوانی خوشترین کام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوانی ایزد از مینو سرشتست</p></div>
<div class="m2"><p>مرو را بوی چون بوی بهشتست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو رامین آمد اندر کضور ماه</p></div>
<div class="m2"><p>به رامش جفت ویرو بود شش ماه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به ایوان و به میدان و به نخچیر</p></div>
<div class="m2"><p>به اندوه و به شادی و به تدبیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر ویروست او را بد برادر</p></div>
<div class="m2"><p>و گر شهروست او را بود مادر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه هر کاو دوستی ورزید جایی</p></div>
<div class="m2"><p>به زیر دوستی بودش خطایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه هر کاو جایگاهی مهربانی</p></div>
<div class="m2"><p>کند، دارد به دل در بد گمانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه هر دل چون دلت ناپاک باشد</p></div>
<div class="m2"><p>نه هر مردی چو تو بی باک باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهنشه گفت نیکست ار چنینست</p></div>
<div class="m2"><p>دل رامین سزای آفرینست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدین پیمان توانی حورد سوگند</p></div>
<div class="m2"><p>که رامین را نبودش با تو پیوند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر سوگند بتوانی بدین خورد</p></div>
<div class="m2"><p>نباشد در جهان چون تو جوانمرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جوابش داد ویس و گفت سوگند</p></div>
<div class="m2"><p>خورم شاید بدین نابوده پیوند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرا ترسم ز ناکرده گناهی</p></div>
<div class="m2"><p>به سوگندان نمایم خوب راهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نپیچد جرم ناکرده روانی</p></div>
<div class="m2"><p>نگندد سیر ناخورده دهانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به پیمان و به سوگندم مترساد</p></div>
<div class="m2"><p>که دارد بی گنه سوگند آسان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو در زیرش نباشد ناصوابی</p></div>
<div class="m2"><p>چه سوگندی خوری چه سرد آبی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شهنشه گفت ازین بهتر چه باشد</p></div>
<div class="m2"><p>به پا کی خود جزین در خورچه باشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بخور سوگند وز تهمت برستی</p></div>
<div class="m2"><p>روان را از ملامتا بشستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنون من آتشی روشن فروزم</p></div>
<div class="m2"><p>برو بسیار مشک و عود سوزد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو آنجا پیش دینداران عالم</p></div>
<div class="m2"><p>بدان آتش بخور سوگند محکم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر آن گاهی که تو سوگند خوردی</p></div>
<div class="m2"><p>روان را از گنه پاکیزه کردی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا با تو نباشد نیز گفتار</p></div>
<div class="m2"><p>نه پرخاش و نه پیگار و آزاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ازین پس تو مرا جان و جهانی</p></div>
<div class="m2"><p>برابر دارمت با زندگانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو پیدا گردد از تو پرسایی</p></div>
<div class="m2"><p>ترا بخشم سراسر پادشایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه باشد خرشید زان پادشایی</p></div>
<div class="m2"><p>که بپسندد مرو را پارسایی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرو را گفت ویسه همچنین کن</p></div>
<div class="m2"><p>مرا و حویشتن را پاک دید کن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همی تا به من بربد گمانی</p></div>
<div class="m2"><p>از آن در مر ترا باشد زیانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گناه بوده بر مردم نهفتن</p></div>
<div class="m2"><p>باسی نیکوتر از نابوده گفتن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شهنشه خواند یکسر موبدان را</p></div>
<div class="m2"><p>ز لشکر سروران و کهبدان را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به آتشگاه چیزی بی کران داد</p></div>
<div class="m2"><p>که نتوان کرد آن را سربسر یاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز دینار و ز گوهرهای شهوار</p></div>
<div class="m2"><p>زمین و آسیا و باغ بسیار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گزیده مادیانان تگاور</p></div>
<div class="m2"><p>همیدون گوسفند و گاو بی مر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز آتشگاه لختی آتش آورد</p></div>
<div class="m2"><p>به میدان آتشی چون کوه بر کرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بسی از صندل و عودش خورش داد</p></div>
<div class="m2"><p>به کافور و به مشکش پرورش داد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز میدان آتش سوزان بر آمد</p></div>
<div class="m2"><p>که با گردون گردان همبر آمد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو زرّین گنبدی بر چرم یازان</p></div>
<div class="m2"><p>شده لرزان و زرّش پاک ریزان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به سان دلبری در لعل و ملحم</p></div>
<div class="m2"><p>گرازان و خورشان مست و خرّم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو روز وصلت او را روشنایی</p></div>
<div class="m2"><p>هنو سوزنده چون روز جدایی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز چهره نور در گیتی فگنده</p></div>
<div class="m2"><p>ز نورش باز تاریکی رمنده</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نبود آگاه در گیتی زن و مرد</p></div>
<div class="m2"><p>که شاهنشاه آن آتش چرا کرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو از میدان برآمد آتش شاه</p></div>
<div class="m2"><p>همی سود از بلندی سرش با ماه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز بام گوشک موبد ویس و رامین</p></div>
<div class="m2"><p>بدیدند آتشی یازان به پروین</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بزرگان خراسان ایستاده</p></div>
<div class="m2"><p>سراسر روی زی آتش نهاده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز چندان مهتران یک تن نه آگاه</p></div>
<div class="m2"><p>بدان آتش چه خواهد سوختن شاه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همان گه ویس در رامین نگاه کرد</p></div>
<div class="m2"><p>مرو را گفت بنگر حال این مرد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که آتش چون بلند افروخت مارا</p></div>
<div class="m2"><p>بدین آتش بخواهد سوخت مارا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیا تا هر دو بگریزیم از ایدر</p></div>
<div class="m2"><p>بسوزانیم او را هم به آذر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مرا بفریفت موبد دی به سوگند</p></div>
<div class="m2"><p>به شیرینی سخنها گفت چون قند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مرو را نیز دام خود نهادم</p></div>
<div class="m2"><p>نه آن بودم که در دام او فتادم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدو گفتم خورم صد باره سوگند</p></div>
<div class="m2"><p>که رامین را نبد با ویس پیوند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو زین با وی سخن گفتم فراوان</p></div>
<div class="m2"><p>دلش بفریفتم ناگه به دستان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کنون در پاش شهری و سپاهی</p></div>
<div class="m2"><p>ز من خواهد ننودن بی گناهی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مرا گوید به آتش بر گذر کن</p></div>
<div class="m2"><p>جهان را از تن پاکت خبر کن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بدان تا کهتر و مهتر بدانند</p></div>
<div class="m2"><p>کجا در ویس و رامین بدگمانند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بیا تا پیش ازین کاومان بخواند</p></div>
<div class="m2"><p>ورا این راستی در دل بماند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پس آنگه دایه را گفتا چه گویی</p></div>
<div class="m2"><p>وزین آتش مرا چاره چه جویی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو دانی کاین نه هنگام ستیزاست</p></div>
<div class="m2"><p>که این هنگام هنگام گریزست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو چاره دانی و نیرنگ بازی</p></div>
<div class="m2"><p>نگر در کار ما چاره چه سازی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کجا در جای چونین چاره بهتر</p></div>
<div class="m2"><p>که در جای دگر مردی و لشکر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>جوابش داد رنگ آمیز دایه</p></div>
<div class="m2"><p>نیفتادست کار خوار مایه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>من این را چاره چون دانم نهاد</p></div>
<div class="m2"><p>سر این بند چون دانم گشادن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>مگر مارا دهد دادار یاری</p></div>
<div class="m2"><p>برافروزد چراغ بختیاری</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کنون افتاد کار، ایدر مپایید</p></div>
<div class="m2"><p>کجو من میروم با من بیایید</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>پس آنگه رفت بر بام شبستان</p></div>
<div class="m2"><p>نگر زانجا چگونه ساخت دستان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فراوان زر و گوهر بر گرفتند</p></div>
<div class="m2"><p>پس آنگه هر سه در گرمابه رفتند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>رهی از گلخن اندر بوستان بود</p></div>
<div class="m2"><p>چنان راهی که از هر کس نهان بود</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدان ره هر سه اندر باغ رفتند</p></div>
<div class="m2"><p>ز موبد با دلی پرداغ رفتند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سبک بر رفت رامین روی دیوار</p></div>
<div class="m2"><p>فرو هشت از سر دیوار دستار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به جاره بر کشید آن هر دوان را</p></div>
<div class="m2"><p>به دیگر سو فرو هشت این و آن را</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>پس آنگه خود فرود آمد ز دیوار</p></div>
<div class="m2"><p>به چادر هر سه بربستند رخسار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو دیوان چهره از مردم نهفتند</p></div>
<div class="m2"><p>به آیین زنان هر سه برفتند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همی دانست رامین بوستانی</p></div>
<div class="m2"><p>بدو در کار دیده باغبانی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>همان گه پیش مرد باغبان شد</p></div>
<div class="m2"><p>بیارامید چون در بوستان شد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>فرستادش به حانه باغبان را</p></div>
<div class="m2"><p>بخواند از خانه پنهان قهرمان را</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بفرمودش که رو اسپان بیاور</p></div>
<div class="m2"><p>گزیده هر چه آن باشد تگاور</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>همیدون خوردنی چیزی که داری</p></div>
<div class="m2"><p>سلاحم با همه ساز شکاری</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بیاوردند آن چیزی که او خواست</p></div>
<div class="m2"><p>نماز شام رفتن را بیاراست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز مرو اندر بیابان رفت چون باد</p></div>
<div class="m2"><p>ندیده روی او را آدمی زاد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بیابانی که آرام بلا بود</p></div>
<div class="m2"><p>ز ناخوشی چو کام اژدها بود</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز روی ویس و رامین گشته فرخار</p></div>
<div class="m2"><p>ز بوی هر دوان چون طبل عطار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>کویر و شوره و ریگ رونده</p></div>
<div class="m2"><p>سنوم جانکش و شیر دمنده</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>دو عاشق را شده چون باغ خرم</p></div>
<div class="m2"><p>از آن شادی کجا بودند باهم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز گرما و کویر آنگه نبودند</p></div>
<div class="m2"><p>تو گفتی شب در ره نبودند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>به چین اندر به سنگی برنبشتست</p></div>
<div class="m2"><p>که دوزخ عاشقان را چون بهشتست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو باشد مرد عاشق در بر دوست</p></div>
<div class="m2"><p>همه زشتی به چشمش سخت نیکوست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>کویر و کوه او را بوستانست</p></div>
<div class="m2"><p>فراز برف گمچون گلستانست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کجا عاشق به مرد مست ماند</p></div>
<div class="m2"><p>که در مستی غم و شادی نداند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به ده روز آن بیابان را بریدند</p></div>
<div class="m2"><p>ز مرو شاهجان زی ری رسیدند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به روی در رامین را یکی دوست</p></div>
<div class="m2"><p>به گاه مردمی با او ز یک پوست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>جوانمرد هنرمند و بی آهو</p></div>
<div class="m2"><p>مرو را دستگاهی سخت نیکو</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به بهروزی بداده بخت کامش</p></div>
<div class="m2"><p>که خود بهروز شیرو بود نامش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز خوشی چون بهشتی خان و مانش</p></div>
<div class="m2"><p>همیشه شاد از وی دوستانش</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>شبی تاریک بود و با مهر</p></div>
<div class="m2"><p>ز بیننده نهفته اختران چهر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>جهان چون چاه سیصد باز گشته</p></div>
<div class="m2"><p>هوا با تیرگی انباز گشته</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>همی شد رام تا درگاه بهروز</p></div>
<div class="m2"><p>به کام خویش فرخ بخت و پیروز</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو رامین را بدید آن مهر پرور</p></div>
<div class="m2"><p>نبودش دیده را دیدار باور</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>همی گفت ای عجب هنگام چونین</p></div>
<div class="m2"><p>که باید نیک مهمانی چو رامین</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>مرو را گفت رامین ای برادر</p></div>
<div class="m2"><p>بپوش این راز ما در زیر چادر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>مگو کس را که رامین آمد از راه</p></div>
<div class="m2"><p>مکن کس را ز مهمانانت آگاه</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>جوابش داد بهروز جوانمرد</p></div>
<div class="m2"><p>ترا بختم به مهمان من آورد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>خداوندی و من پیش تو چاکر</p></div>
<div class="m2"><p>نه چا کر بل ز چا کر نیز کمتر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ترا فرمان برم تا زنده باشم</p></div>
<div class="m2"><p>به پیش بندگانت بنده باشم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>اگر فرمان دهی تا من هم اکنون</p></div>
<div class="m2"><p>شوم با چاکران از خانه بیرون</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سرای و سرایم مر ترا باد</p></div>
<div class="m2"><p>یکی خشنودی جانت مرا باد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>پس آنگه ویس با رامین و بهروز</p></div>
<div class="m2"><p>به کام خویش بنشستند هر روز</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گشاده دل به کام و در ببسته</p></div>
<div class="m2"><p>به می گرد از رخان خویش شسته</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>به روز اندر نشط و شادمانی</p></div>
<div class="m2"><p>به شب در خرّمی و کامرانی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>گهی می بر کف و گه دوست در بر</p></div>
<div class="m2"><p>شده می نوش بر رخسار دلبر</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چراغ نیکوان ویس گل اندر</p></div>
<div class="m2"><p>به شادی و به رامش با دلارام</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>به شب چون زهره شبگیران بر آمد</p></div>
<div class="m2"><p>به بنگ مطرب از خواب اندر آمد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>هنوز از باده بودی مست و در خواب</p></div>
<div class="m2"><p>نهادندیش بر کف بادهء ناب</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>نشسته پیش او رامین دلبر</p></div>
<div class="m2"><p>گهی طنبور و گاهی چنگ در بر</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>همی گفتی سرود مهربازان</p></div>
<div class="m2"><p>به دستان و نوای دلنوازان</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>همی گفتی که دو نیک یاریم</p></div>
<div class="m2"><p>به یاری یکدگر را جان سپاریم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>به هنگام وفا گنج وفاییم</p></div>
<div class="m2"><p>به چشم دشمنان تیز جفاییم</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چو ما را خرّمی و شاد خواریست</p></div>
<div class="m2"><p>بد اندیشان ما را رنج و زریست</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>به رنج از دوستی سیری نیابیم</p></div>
<div class="m2"><p>ز راه مهربانی رخ نتابیم</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>به مهر اندر چو دو روشن چراغیم</p></div>
<div class="m2"><p>به ناز اندر چو دو بشکفته باغیم</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ز مهر خویش جز شادی نبینیم</p></div>
<div class="m2"><p>که از پیروزی ارزانی بدینیم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>خوشا ویسا نشسته پیش رامین</p></div>
<div class="m2"><p>چنان کبگ دری در پیش شاهین</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>خوشا ویسا نشسته جام بر دست</p></div>
<div class="m2"><p>هم از باده هم از خوبی شده مست</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>خوشا ویسا به کام دل نشسته</p></div>
<div class="m2"><p>امید اندر دل موبد شکسته</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>خوشا ویسا به خنده لب گشاده</p></div>
<div class="m2"><p>لب آنگه بر لب رامین نهاده</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>خوشا ویسا به مستی پیش رامین</p></div>
<div class="m2"><p>ز عشقش کیش همچون کیش رامین</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>زهی رامین نکو تدبیر کردی</p></div>
<div class="m2"><p>که چون ویسه یکی نخچیر کردی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>زهی رامین به کام دل همی ناز</p></div>
<div class="m2"><p>که داری کام دل را نیک انباز</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>زهی رامین که در باغ بهشتی</p></div>
<div class="m2"><p>همیشه با گل اردبهشتی</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>زهی رامین که جفت آفتابی</p></div>
<div class="m2"><p>به فروش هر چه تو خواهی بیابی</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>هزاران آفرین بر کضور ماه</p></div>
<div class="m2"><p>که چون ویس آمدست یکی ماه</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>هزاران آفرین بر جان شهرو</p></div>
<div class="m2"><p>که دختش ویسه بود و پور بیرو</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>هزاران آفرین بر جان قارن</p></div>
<div class="m2"><p>که از پشت آمدش این ماه روشن</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>هزاران آفرین بر خندهء ویس</p></div>
<div class="m2"><p>که کردست این جهان را بندهء ویس</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بسیار ای ویس جام خسروانی</p></div>
<div class="m2"><p>درو می چون رخانت ارغوانی</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>چو از دست تو گیرم جام مستی</p></div>
<div class="m2"><p>مرا مستی نیارد هیچ سستی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>ندارم مست چون گشتم به کامت</p></div>
<div class="m2"><p>ز رویت یا ز مهرت یا ز جامت</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>گر از دست تو جام هوش گیرم</p></div>
<div class="m2"><p>چنان دانم که جام نوش گیرم</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>نشط من ز تو آرام یابد</p></div>
<div class="m2"><p>غمان من ز تو انجام یابد</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>دلم درج است و در وی گوهری تو</p></div>
<div class="m2"><p>کنارم برج و در وی اختری تو</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ابی گوهر مبادا هر گز این درج</p></div>
<div class="m2"><p>ابی اختر مبادا هر گز این برج</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>همیشه باد باغ رویت آباد</p></div>
<div class="m2"><p>دو دست من به باغت باغبان باد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بسا روزا که نام ما بخوانند</p></div>
<div class="m2"><p>خردمندان شکفت از ما بمانند</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>چنان خوبی و چونین مهربانی</p></div>
<div class="m2"><p>سزد گر نام دارد جاودانی</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>دلا بسیار درد و ریش دیدی</p></div>
<div class="m2"><p>کنون از دوست کام خویش دیدی</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>دلی چون خویشن دیدی پر از مهر</p></div>
<div class="m2"><p>و یا این گل رخی تابان از مهر</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>تو روز و شب بدین چهره همی ناز</p></div>
<div class="m2"><p>نبرد بد سگالان را همی ساز</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>که خرما در جهان با خار باشد</p></div>
<div class="m2"><p>نشاط عشق با تیمار باشد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>کنون اژز جان کنی در کار مهرش</p></div>
<div class="m2"><p>نباشد در خور دیدار مهرش</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>روان از بهر چونین یار باید</p></div>
<div class="m2"><p>جهان از بهر چونین کار باید</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>تو اکنون می خور از فردا میندیش</p></div>
<div class="m2"><p>که جز فرمان یزدان نایدت پیش</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>مگر کارت بود در مهر کاری</p></div>
<div class="m2"><p>ازان بهتر که تو امید داری</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>هران گاهی که رامین باده خوردی</p></div>
<div class="m2"><p>جنین گفتارها را یاد کردی</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>ازین سو ویس با کام و هوا بود</p></div>
<div class="m2"><p>وزان سو شاه با رنج و بلا بود</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>گرایشان را به ناز اندر خوشی بود</p></div>
<div class="m2"><p>شهنشه را شتاب و ناخوشی بود</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>که او سوگند ویسه خواست دادن</p></div>
<div class="m2"><p>دل از بند گمانی بر گشادن</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چو ویس ماه پیکر را طلب کرد</p></div>
<div class="m2"><p>زمانه روز او را تیره شب کرد</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>همی جستش ز هر سو یک شبانروز</p></div>
<div class="m2"><p>به دل آتشی مانده خردسوز</p></div></div>