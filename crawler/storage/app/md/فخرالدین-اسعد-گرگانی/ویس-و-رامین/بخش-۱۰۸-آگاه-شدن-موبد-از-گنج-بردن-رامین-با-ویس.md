---
title: >-
    بخش ۱۰۸ - آگاه شدن موبد از گنج بردن رامین با ویس
---
# بخش ۱۰۸ - آگاه شدن موبد از گنج بردن رامین با ویس

<div class="b" id="bn1"><div class="m1"><p>چو آگاهی به لشکرگاه بردند</p></div>
<div class="m2"><p>بزرگان شاه را آگه نکردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا او پادشاهی بود بدجو</p></div>
<div class="m2"><p>وزین بدتر شهان را نیست آهو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیارست ایچ کس او را بگفتن</p></div>
<div class="m2"><p>همه کس رای دید آن را نهفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سه روز این راز ماند از وی نهفته</p></div>
<div class="m2"><p>تمامی کار رامین شد شکفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آگه شد جهان بر وی سر آمد</p></div>
<div class="m2"><p>تو گفتی رستخیز او بر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مساعد بخت او با او بر آشفت</p></div>
<div class="m2"><p>خرد یکباره از وی روی بنهفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندانست ایچ گونه چارهء خویش</p></div>
<div class="m2"><p>تو گفتی بسته شد راگش پس و پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فهی فگتی شوم سوی خراسان</p></div>
<div class="m2"><p>مه رامین باد و مه ویس و مه گرگان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی گفتی که گر من باز گردم</p></div>
<div class="m2"><p>به زشتی در جهان آواز گردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا گویند گشت از رام ترسان</p></div>
<div class="m2"><p>و گر نه نامدی سوی خراسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهی گفتی که گر با وی بکوشم</p></div>
<div class="m2"><p>ندانم چون دهد یاری سروشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپاه من همه با من به کینند</p></div>
<div class="m2"><p>به شاهی پاک رامین را گزینند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جوانست او و هم بختش جوانست</p></div>
<div class="m2"><p>درخت دولتش تا آسمانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دست آورد گنج من سراسر</p></div>
<div class="m2"><p>منم مفلس کنون و او توانگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه خوردم آن همه نعمت نه دادم</p></div>
<div class="m2"><p>ز بهر او همه بر هم نهادم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا مادر بدین پتیاره افگند</p></div>
<div class="m2"><p>که بر رامین دلم را کرد خرسند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سزد گر من به بد روزی نشستم</p></div>
<div class="m2"><p>که گفتار زنان را کام بستم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی هفته سپه را روی ننمود</p></div>
<div class="m2"><p>دو صد دریای اندیشه نپیمود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین افتاد تدبیرش به فرجام</p></div>
<div class="m2"><p>که با رامین بکوشد کام و ناکام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی ننگ آمدش بر گشتن از جنگ</p></div>
<div class="m2"><p>ز گرگان سوی آمل کرد آهنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو لشکرگه بزد بر دشت آمل</p></div>
<div class="m2"><p>جهان از ساز لشکر گشت پر گل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز خیمه گشت صحرا چون کهستان</p></div>
<div class="m2"><p>کهستان از خوشی همچون گلستان</p></div></div>