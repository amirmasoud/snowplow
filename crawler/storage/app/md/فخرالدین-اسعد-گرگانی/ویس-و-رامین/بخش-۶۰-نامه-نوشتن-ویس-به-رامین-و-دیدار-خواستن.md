---
title: >-
    بخش ۶۰ - نامه نوشتن ویس به رامین و دیدار خواستن
---
# بخش ۶۰ - نامه نوشتن ویس به رامین و دیدار خواستن

<div class="b" id="bn1"><div class="m1"><p>چو بشنید این سخن فرزانه مشکین</p></div>
<div class="m2"><p>به فرهنگش جهان را کرد مشکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی نامه نوشت از ویس دژکام</p></div>
<div class="m2"><p>به رامین نکوبخت و نکو نام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حریر نامه بود ابریشم چین</p></div>
<div class="m2"><p>چو مشک از تبت و عنبر ز نسرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم از مصر بود آب گل از جور</p></div>
<div class="m2"><p>دویت از عنبرین عود سمندور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دبیر از شهر بابل جادوی تر</p></div>
<div class="m2"><p>سخن آمیخته شکر به گوهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریرش چون بر ویس پری روی</p></div>
<div class="m2"><p>مدادش همچو زلف ویس خوشبوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قلم چون قامت ویس از نزاری</p></div>
<div class="m2"><p>ز بس کز رام دید آزار و خواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دبیر از جادوی چون دیدگانش</p></div>
<div class="m2"><p>سخن چون در و شکر در دهانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر نامه به نام یک خداوند</p></div>
<div class="m2"><p>وزان پس کرده یاد مهر و پیوند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سروی سوخته وز بن گسسته</p></div>
<div class="m2"><p>به سروی از چمن شاداب رسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز ماهی در محاق مهر پنهان</p></div>
<div class="m2"><p>به ماهی در سپهر کام تابان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز باغی سر بسر آفت گرفته</p></div>
<div class="m2"><p>به باغی سر بسر خرم شکفته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز شاخی خشک گشته هامواره</p></div>
<div class="m2"><p>به شاخی بار او و ستاره</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز کانی کنده و بی بر بمانده</p></div>
<div class="m2"><p>به کانی در جهان غوهر فشانده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز روزی بر هد مغرب رسیده</p></div>
<div class="m2"><p>به یاقوتی به تاجی در نشانده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز گلزاری سموم هجر دیده</p></div>
<div class="m2"><p>به گلزاری ز خوبی بشکفیده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز دریایی شده بی در و بی آب</p></div>
<div class="m2"><p>به دریایی پر آب و در خوشاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بختی تیره چون شوریده آبی</p></div>
<div class="m2"><p>به بختی نامور چون آفتابی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز مهری تا گه محشر فزایان</p></div>
<div class="m2"><p>به مهری هر زمان کاهش نمایان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز عشقی تاب او از حد گذشته</p></div>
<div class="m2"><p>به عشقی گرم بوده سرد گشته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز جانی در عذاب و رنج و سختی</p></div>
<div class="m2"><p>به جانی در هوای نیک بختی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز طبعی در هوا بیدار گشته</p></div>
<div class="m2"><p>به طبعی در هوا بیزار گشته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز چهری آب جوبی زو رمیده</p></div>
<div class="m2"><p>به چهری آب خوبی زو دمیده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز رویی همچو دیبای بر آتش</p></div>
<div class="m2"><p>به رویی همچو دیبای منقش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز چشمش سال ومه بی خواب و پر آب</p></div>
<div class="m2"><p>به چشمی سال و مه بی آب و پر خواب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز یاری نیک پر مهر و وفا جوی</p></div>
<div class="m2"><p>به یاری شوخ و بی شرم و جفا جوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز ماهی بی کس و بی یار گشته</p></div>
<div class="m2"><p>به شاهی بر جهان سالار گشته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نبشتم نامه در حال چنین زار</p></div>
<div class="m2"><p>که جان از تن تن از جان بود بیزار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>منم در آتش هجران گدازان</p></div>
<div class="m2"><p>توی در مجلس شادی نوازان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>منم گنج وفا را گشته گنجور</p></div>
<div class="m2"><p>توی دست جفا را گشته دستور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی بر تو دهم در نامه سوگند</p></div>
<div class="m2"><p>به حق دوستی و مهر و پیوند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به حق آنکه با هم جفت بودیم</p></div>
<div class="m2"><p>به حق آنکه ما هم گفت بودیم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به حق صحبت ما سالیانی</p></div>
<div class="m2"><p>به حق دوستی و مهربانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که این نامه ز سر تا بن بخوانی</p></div>
<div class="m2"><p>یکایک حال من جمله بدانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدان راما که گیتی گرد گردست</p></div>
<div class="m2"><p>ازو گه تن درستی گاه دردست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گهی رنجست و گاهی شادمانی</p></div>
<div class="m2"><p>گهی مرگست و گاهی زندگانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به نیک و بد جهان بر ما سرآید</p></div>
<div class="m2"><p>وزان پس خود جهان دیگر آید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز ما ماند به گیتی در فسانه</p></div>
<div class="m2"><p>در آن گیتی خدای جاودانه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فسان ما همه گیتی بخوانند</p></div>
<div class="m2"><p>یکایک خوب و زشت ما بداند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو خود دانی که از ما کیدت بد نام</p></div>
<div class="m2"><p>کجا از نام بد جوید همه کام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من آن بودم به پاکی کم دیدی</p></div>
<div class="m2"><p>به خوبی از جهانم بر گزیدی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من از پاکی چو قطر ژاله بودم</p></div>
<div class="m2"><p>به خوبی همچو برگ لاله بودم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ندیده کام جز تو مرد بر من</p></div>
<div class="m2"><p>زمانه نا فشانده گرد بر من</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو گوری بودم اندر مرغزاران</p></div>
<div class="m2"><p>ندیده دام و داس دام دادن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو بودی دام دار و داس دارم</p></div>
<div class="m2"><p>نهادی داس و دام اندر گذارم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرا در دام رسوایی فگندی</p></div>
<div class="m2"><p>کنون در چاه تنهایی فگندی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا بفریفتی وز ره ببردی</p></div>
<div class="m2"><p>کنون زنهار با جانم بخوردی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدان سر مر ترا طرار دیدم</p></div>
<div class="m2"><p>بدین سر مر ترا غدار دیدم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همی گویی که خوردی سخن سوگند</p></div>
<div class="m2"><p>که با ویسم نباشد نیز پیوند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نه با من نیز هم سوگند خوردی</p></div>
<div class="m2"><p>که تا جان داری از من بر نگردی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کدامین راست گیرم زین دو سوگند</p></div>
<div class="m2"><p>کدامین راست گیرم زین دو پیوند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ترا سوگند چون باد بزانست</p></div>
<div class="m2"><p>ترا پیوند چون آب روانست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بزرگست از جهان این هر دو را نام</p></div>
<div class="m2"><p>ولیکن نیست شان بر جای آرام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تو همچون سندسی گردان به هر رنگ</p></div>
<div class="m2"><p>و یا همچون زری گردان به هر چنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کرا دانی چو من در مهربانی</p></div>
<div class="m2"><p>چو تو با من نمانی با که مانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نگر تا چند کار بد بکردی</p></div>
<div class="m2"><p>که آب خویش و آب من ببردی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی بفریفتی جفت کسان را</p></div>
<div class="m2"><p>به ننگ آلوده دودمان را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دوم سوگندها بدروغ کردی</p></div>
<div class="m2"><p>ابا ژنهاریان زنها خوردی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سوم برگشتی از یار وفادار</p></div>
<div class="m2"><p>بی آب کزوی رسیدت رنج و آزار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چهارم ناسزا گفتی بر آن کس</p></div>
<div class="m2"><p>که او را خود توی اندر جهان بس</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>من آن ویسم که رویم آفتابست</p></div>
<div class="m2"><p>من آن ویسم که مویم مشک نابست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من آن ویسم که چهرم نوبهارست</p></div>
<div class="m2"><p>من آن ویسم که مهرم پایدارست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>من آن ویسم که ماه نیکوانم</p></div>
<div class="m2"><p>من آن ویسم که شاه جادوانم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>من آن ویسم که ماهم بر رخانست</p></div>
<div class="m2"><p>من آن ویسم که نوشم در لبانست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>من آن ویسم من آن ویسم من آن ویسم</p></div>
<div class="m2"><p>که بودی تو سلیمان من چو بلقیس</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مرا باشد به از تو در جهان شاه</p></div>
<div class="m2"><p>ترا چون من نباشد بر زمین ماه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر آن گاهی که دل از من بتابی</p></div>
<div class="m2"><p>چو باز آیی مرا دوشوار یابی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مکن راما که خود گردی پشیمان</p></div>
<div class="m2"><p>نیابی درد را جز ویس در مان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مکن راما که از گل سیر گردی</p></div>
<div class="m2"><p>نیابی ویس را آنگه بمردی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مکن راما که تو امروز مستی</p></div>
<div class="m2"><p>ز مستی عهد من بر هم شکستی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مکن راما که چون هشیار گردی</p></div>
<div class="m2"><p>ز گیتی بی زن وبی یار گردی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بسا روزا که تو پیشم بنالی</p></div>
<div class="m2"><p>دو رخ بر خاک پای من بمالی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دل از کینه به سوی مهر تابی</p></div>
<div class="m2"><p>مرا جویی به صد دست و نیابی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو از من سیر گشتی وز لبانم</p></div>
<div class="m2"><p>ز گل هم سیر گردی بی گمانم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رو چون بامن نسازی با که سازی</p></div>
<div class="m2"><p>هوا با من نبازی با که بازی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>همی گوید هر آن کاو مهر بازد</p></div>
<div class="m2"><p>کرا ویسه نسازد مرگ سازد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز بدبختیت بس باد این نشانی</p></div>
<div class="m2"><p>گلی دادت چو بستد گلستانی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ترا بنمود رخشان ماهتابی</p></div>
<div class="m2"><p>ز تو بستد فروزان آفتابی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همی نازی که داری ارغوانی</p></div>
<div class="m2"><p>ندانی کز تو گم شد بوستانی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همانا کردی آن تلخی فراموش</p></div>
<div class="m2"><p>که بودی از هوا بی صبر و بی هوش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خیالم گر به خواب اندر بدیدی</p></div>
<div class="m2"><p>گمان بردی که بر شاهی رسیدی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو بودی من به مغزت بر گذشتی</p></div>
<div class="m2"><p>تنت گر مرده بودی زنده گشتی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چنین است آدمی بی رام و بی هوش</p></div>
<div class="m2"><p>کند سختی و شادی را فراموش</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دگر گفتی که گم کردم جوانی</p></div>
<div class="m2"><p>همی گویی دریغا زندگانی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>مرا گم شد جوانی در هوایت</p></div>
<div class="m2"><p>همیدون زندگانی در وفایت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گمان بردم که شاخ شکری تو</p></div>
<div class="m2"><p>بکارم تا شکر بار آوری تو</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بکشتم پس بپروردم به تیمار</p></div>
<div class="m2"><p>چو بر رستی کبست آوردیم بار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو یاد آرم از آن رنجی که بردم</p></div>
<div class="m2"><p>وز آن دردی که از مهر تو خوردم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>یکی آتش به مغز من در آید</p></div>
<div class="m2"><p>کزو جیحون ز چشم من بر آید</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چه مایه سختی و خواری کشیدم</p></div>
<div class="m2"><p>به فرجام از تو آن دیدم که دیدم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مرا تو چاه کندی دایه زد دست</p></div>
<div class="m2"><p>به جاه افگنده و خود آسوده بنشست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>تو هیزم دادی او آتش برافروخت</p></div>
<div class="m2"><p>به کام دشمنان در آتشم سوخت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ندانم کز تو نالم یا ز دایه</p></div>
<div class="m2"><p>که رنجم زین دوان بردست مایه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگر چه دیدم از تو بی وفایی</p></div>
<div class="m2"><p>نهادی بر دلم داغ جدایی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>و گر چه آتشمدر دل فگندی</p></div>
<div class="m2"><p>مرا مانند خر در گل فگندی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>و گر چه چشم من خون بار کردی</p></div>
<div class="m2"><p>کنارم رود جیحون بار کردی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>دلم ناید به یزدانت سپردن</p></div>
<div class="m2"><p>جفایت پیش یزدان بر شمردن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مبیند ایچ دردت دیدگانم</p></div>
<div class="m2"><p>که باشد درد تو هم بر روانم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>کنون ده در بخواهم گفت نامه</p></div>
<div class="m2"><p>به گفتاری که خون بارد ز خامه</p></div></div>