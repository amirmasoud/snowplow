---
title: >-
    بخش ۷۲ - مویه کردن ویس بر جدایى رامین
---
# بخش ۷۲ - مویه کردن ویس بر جدایى رامین

<div class="b" id="bn1"><div class="m1"><p>چو ویس دلبر آذین را گسی کرد</p></div>
<div class="m2"><p>به درد و داغ دل مویه بسی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر آن مردی که این مویه بخواند</p></div>
<div class="m2"><p>اگر با دل بود بی دل بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا شد آن خجسته روزگارم</p></div>
<div class="m2"><p>که بودی آفتاب اندر کنارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا کز آفتاب آمد جدایی</p></div>
<div class="m2"><p>چگونه پیشم آید روشنایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برانم زین دو چشم تیره دو رود</p></div>
<div class="m2"><p>که ماه و آفتابم کرد پدرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه آفتاب از من جدا شد</p></div>
<div class="m2"><p>جهان بر چشم من تیره چرا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم بیمار و نالان در شب تار</p></div>
<div class="m2"><p>که در شب بیش باشد درد بیمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکردم بد به کس تا نبینم</p></div>
<div class="m2"><p>چرا اکنون ز بد روزی چنینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بخت بد دلم را هر زمانی</p></div>
<div class="m2"><p>تو پنداری در آید کاروانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدرّد این دل از بس غم که در اوست</p></div>
<div class="m2"><p>بدرّد نار چون پر گرددش پوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلی بسته به چندین گونه بیدار</p></div>
<div class="m2"><p>نه تابد خور درو و نه وزد باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همیشه در دل من ابر دارد</p></div>
<div class="m2"><p>ازیرا زین دو چشمم سیل بارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببندد ابر و آنگه بر گشاید</p></div>
<div class="m2"><p>چرا ابر دلم چندین بپاید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازیرا شد رخم همرنگ دینار</p></div>
<div class="m2"><p>که گردد کشت زرد از ابر بسیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیامختست عشق من دبیری</p></div>
<div class="m2"><p>بدین پژمرده رخار زریری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خون من نویسد گونه گونه</p></div>
<div class="m2"><p>حروف غم به خطهای نمونه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه رویست این که رنگش چون زریرست</p></div>
<div class="m2"><p>چه بختست این که عشق اورا دبیرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا عشق آتشی در دل بر افروخت</p></div>
<div class="m2"><p>دلم با هر چه در دل بد همه سوخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا بر دل همیشه رحمت آید</p></div>
<div class="m2"><p>ز بس کز عشق وی را محنت آید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر بی دانشی کرد این دل ریش</p></div>
<div class="m2"><p>چنین شد لاجرم از کردهء خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدا کارا که بود این مهربانی</p></div>
<div class="m2"><p>ببرد از من دل و جان و جوانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر اورا خود من آوردم به گیهان</p></div>
<div class="m2"><p>جزای من بسست این داغ هجران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین داغی کزو تا جاودانی</p></div>
<div class="m2"><p>بماند بر روان من نشانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کجایی ای نگار تیر بالا</p></div>
<div class="m2"><p>مرا بین چون کمانی گشته دو تا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو تیری من کمانم در جدایی</p></div>
<div class="m2"><p>چو رفتی نیز با زی من نیایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بپیچم چون به یاد آرم جفایت</p></div>
<div class="m2"><p>چو آن شمشادگون زلف دو تایت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بلرزم چون بیندیشم ز هجران</p></div>
<div class="m2"><p>چو گنجشگی که تر گردد ز باران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلی دارم به دستت زینهاری</p></div>
<div class="m2"><p>ندید از تو مگر زنهار خواری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلت چون داد آزارش فزودن</p></div>
<div class="m2"><p>قرارش بردن و دردش نمودن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه گیتی را به چشم تو همی دید</p></div>
<div class="m2"><p>ز چشم بد همی بر تو بترسید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه دیدار تو بودش کام و امید</p></div>
<div class="m2"><p>نه رخسار تو بودش ماه و خورشید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه بالای تو بودش سرو و شمشاد</p></div>
<div class="m2"><p>نه زین شمشاد بودی جان او شاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بنفشه بر دو زلفت کی گزیدی</p></div>
<div class="m2"><p>طبرزد با لبانت کس مزیدی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چرا با جان من چندین ستیزی</p></div>
<div class="m2"><p>چرا بیهوده خون من بریزی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه من آنم که بودم دلفروزت</p></div>
<div class="m2"><p>رخم ماه شب و خورشید روزت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه مهرت بود هموراه ندیمم</p></div>
<div class="m2"><p>نه بویت بود همواره نسیمم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه روی من ز عشقت بود زرین</p></div>
<div class="m2"><p>نه اشک من ز جورت بود خونین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه رود از هجر تو بر رخ گشادم</p></div>
<div class="m2"><p>نه سنگ از مهر تو بر دل نهادم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه جز تو نیست در گیتی مرا کس</p></div>
<div class="m2"><p>درین گیتی هوای من توی بس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرا دیدی ز پیش مهربانی</p></div>
<div class="m2"><p>کنون گر بینیم گویی نه آنی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نه آنم که تو دیدستی نه آنم</p></div>
<div class="m2"><p>در آن گه تیر و اکنون چون کمانم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زدم بر رخ دو دست خویش چندان</p></div>
<div class="m2"><p>که نیلوفر شد آن گلنار خندان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دهم آبش همی زین چشم بی خواب</p></div>
<div class="m2"><p>که نیلوگر نباشد تازه بی آب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بنام تا بنالد زیر بر مل</p></div>
<div class="m2"><p>ببارم تا ببارد ابر برگل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دو چشم من ز سرخی مثل لاله ست</p></div>
<div class="m2"><p>برو بر اشک من مانند ژاله ست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>درخت رنج من گشست بی بر</p></div>
<div class="m2"><p>تن امید من ماندست بی سر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا دل دشمنست ای وای بر من</p></div>
<div class="m2"><p>چرا چاره همی جویم ز دشمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه نادانم که از دل چاره جویم</p></div>
<div class="m2"><p>که خودیکباره دل برد آب رویم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دل من گر نبودی دشمن من</p></div>
<div class="m2"><p>چنین عاصی نبودی در تن من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پر آتش شد دلم چون گشت سر کش</p></div>
<div class="m2"><p>بلی باشد سزای سر کش آتش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بنال ای دل که ارزانی بدینی</p></div>
<div class="m2"><p>که هم در این جهان دوزخ ببینی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قصا ما را چنین کردست روزی</p></div>
<div class="m2"><p>که من گریم همه ساله تو سوزی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بدین سان زندگانی چون بود خوش</p></div>
<div class="m2"><p>که من باشد در آب و تو در آتش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهان دریا کنم از دیدگانم</p></div>
<div class="m2"><p>پس آنگه کشتی اندر وی برانم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز خونین جامه سازم بادبانم</p></div>
<div class="m2"><p>به باد سرد خود کشتی برانم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو باد از من بود دریا هم از من</p></div>
<div class="m2"><p>نباشد کشتیم را موج دشمن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عدیل ماهیان باشم به دریاب</p></div>
<div class="m2"><p>که خود چون ماهیم همواره در آب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فرستادم به نزد دوست نامه</p></div>
<div class="m2"><p>برو پیچیده خون آلوده جامه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بخواند نامهء من یا نخوانم</p></div>
<div class="m2"><p>بداند زاری من یا نداند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ببخشاید مرا از مهر گوی</p></div>
<div class="m2"><p>کند با من به پاسخ مهر جویی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نباشد عاشقان را زین بتر روز</p></div>
<div class="m2"><p>که چشم نامه ای دارند هر روز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بشد روز وصال و روز خوشی</p></div>
<div class="m2"><p>که من با دوست کردم ناز و گشّی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کنون با او به نامه گشت گفتار</p></div>
<div class="m2"><p>و گر خسپم بود در خواب دیدار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بماندم تا چنین روزی بدیدم</p></div>
<div class="m2"><p>وزان پایه بدین پایه رسیدم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چرا زهر گزاینده نخوردم</p></div>
<div class="m2"><p>چرا روزی به بهروزی نبردم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر مرگ من آنگه در رسیدی</p></div>
<div class="m2"><p>مگر چشمم چنین روزی ندیدی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>روان را مرگ روز کامرانی</p></div>
<div class="m2"><p>بسی خوشتر ز چونین زندگانی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>جهانا خود ترا اینست پیشه</p></div>
<div class="m2"><p>که با بی دل کنی خواری همیشه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همان ابری که باری در دو زاری</p></div>
<div class="m2"><p>ازو بر بیدلانت سنگ باری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همان بادی که آرد بود گلزار</p></div>
<div class="m2"><p>همی نادر به من بوی تن یار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چه بد کردم که او با من چنینست</p></div>
<div class="m2"><p>مگرباد تو با من هم به کینست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بهار خاک را بینم شکفته</p></div>
<div class="m2"><p>زمین را در گل و دیبا گرفته</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بهار من ز من مهجور مانده</p></div>
<div class="m2"><p>چو جان پاک از تن دور مانده</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همانا خاک در گیتی ز من به</p></div>
<div class="m2"><p>که او را نو بهاست و مرا نه</p></div></div>