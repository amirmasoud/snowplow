---
title: >-
    بخش ۴۶ - مویه کردن شهرو پیش موبد
---
# بخش ۴۶ - مویه کردن شهرو پیش موبد

<div class="b" id="bn1"><div class="m1"><p>چو باز آمد ز قلعه شاه شاهان</p></div>
<div class="m2"><p>نبد همراه با او ماه ماهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش شاه شد شهرو خروشان</p></div>
<div class="m2"><p>به فندق ماه تابان را خراشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی گفت ای نیازی جان مادر</p></div>
<div class="m2"><p>به هر دردی رخت در مان مادر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا موبد نیاوردت بدین بار</p></div>
<div class="m2"><p>چه بد دیدی ازین دیو ستمگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه پیش امد ترا از بخت بد ساز</p></div>
<div class="m2"><p>چه تیمار و چه سختی دیده ای باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس آنگه گفت موبد را به زاری</p></div>
<div class="m2"><p>چه عذر آری که ویسم را نیاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کردی آفتاب دلبران را</p></div>
<div class="m2"><p>چرا بی ماه کردی اختران را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبستانت بدو بودی شبستان</p></div>
<div class="m2"><p>کنون چه این شبستان چه بیابان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرایت را همی بی نور بینم</p></div>
<div class="m2"><p>بهشتت را همی بی حور بینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر دخت مرا با من سپاری</p></div>
<div class="m2"><p>وگر نه خون کنم دریا به زاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنالم تا بنالد کوه با من</p></div>
<div class="m2"><p>خورد تا جاودان اندوه با من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگیریم تا بگیرد دهر با من</p></div>
<div class="m2"><p>جهان گردد ترا همواره دشمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر ویس مرا با من نمایی</p></div>
<div class="m2"><p>وگرنه زین شهنشاهی بر آیی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگیرد خون ویس دلربایت</p></div>
<div class="m2"><p>شود انگشت پایت بند پایت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو شهرو پیش موبد زار بگریست</p></div>
<div class="m2"><p>شهنشه نیز هم بسیار بگریست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدو گفت ار بنالی ور ننالی</p></div>
<div class="m2"><p>مرا زشتی و یا خوبی سگالی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکردم آنچه پیش و پس نکردم</p></div>
<div class="m2"><p>شکوه خویش و آب تو ببردم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر تو روی آن بت روی بینی</p></div>
<div class="m2"><p>میان خاک بینی نقش چینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی سرو سهی بینی بریده</p></div>
<div class="m2"><p>میان خاک و خون در خوابنیده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوانی بر تن سیمینش نالان</p></div>
<div class="m2"><p>چه خوبی بر رخ گلگونش گریان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهفته ابر گل خورشید رویش</p></div>
<div class="m2"><p>بخورده زنگ خون زنجیر مویش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو بشنید این سخن شهرو ز موبد</p></div>
<div class="m2"><p>چو کوهی خویشتن را بر زمین زد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زمین ز اندام او شد خر من گل</p></div>
<div class="m2"><p>سرای از اشک او شد ساغر مل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز گیتی خورده بر دل تیر تیمار</p></div>
<div class="m2"><p>به خاک اندر همی پیچید چون مار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی گفت ای فرو مایه زمانه</p></div>
<div class="m2"><p>بدزدیدی ز من در یگانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مگر گفتست با تو هوشیاری</p></div>
<div class="m2"><p>که گر دزدی کنی در دزد باری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مگر چون من بدان در سخت شادی</p></div>
<div class="m2"><p>که چون گنجش به خاکاندر نهادی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگر چون دیدی آن سرو بهشتی</p></div>
<div class="m2"><p>به باغ جاودانی در بکشتی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرا بر کندی آن سرو بار</p></div>
<div class="m2"><p>چو بر کندی چرا کردی نگونسار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نگون گشته صنوبر چون بروید</p></div>
<div class="m2"><p>به زیر خاک عنبر چون ببوید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>الا ای خاک مردم خوار تا کی</p></div>
<div class="m2"><p>خوری ماه و نگار و خرو و کی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه بس بود آنکه خوردی تا به امروز</p></div>
<div class="m2"><p>کنون خوردی چنان ماه دل افروز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بتیزد ترسم آن سیمین تن پاک</p></div>
<div class="m2"><p>کجا بی شک بریزد سیم در خاک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چرا تیره نباشد اختر من</p></div>
<div class="m2"><p>که در خاک است ریزان گوهر من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به باغ اندر نبالد بیش ازین سرو</p></div>
<div class="m2"><p>که سرو من بریده گشت در مرو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به چرا اندر نتابد بیش ازین ماه</p></div>
<div class="m2"><p>که ماه من نهفته گشت در چاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مگر پروین به دردو شد نظاره</p></div>
<div class="m2"><p>که گرد آمد بهم چندین ستاره</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نگارا شرو قدا ماه رویا</p></div>
<div class="m2"><p>بتا زنجیر مویا مشک بویا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو بودی غمگسار روزگارم</p></div>
<div class="m2"><p>کنون اندوه تو با که گسارم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من این مُست گران را با که گویم</p></div>
<div class="m2"><p>من این بیداد را داد از که جویم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جهانی را بکشت آنکه ترا کشت</p></div>
<div class="m2"><p>ولیکن زان همه بدتر مرا کشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پزشک آرمز روم و هند و ایران</p></div>
<div class="m2"><p>مگر درد مرا دانند درمان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نگارا در جهان بودی تو تنها</p></div>
<div class="m2"><p>ندیدی هیچ کس را با تو همتا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دلت بگرفت از گیتی برفتی</p></div>
<div class="m2"><p>به مینو در سزا جفتی گرفتی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بتا تا مرگ جان تو ببردست</p></div>
<div class="m2"><p>بزرگ امید من با تو بمردست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کرا شاید کنون پیرایهء تو</p></div>
<div class="m2"><p>کرا یابم به سنگ و سایهء تو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به که شاید پرند پر نگارت</p></div>
<div class="m2"><p>قبا و عقد و تاج و گوشوارت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که یارد بردن آگاهی به ویرو</p></div>
<div class="m2"><p>که گریان شد به مرگ ویسه شهرو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بشد ویس آفتاب ماهرویان</p></div>
<div class="m2"><p>بماندم ویس گویان ویس جویان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بشد ویس و ببرد آب خور و ماه</p></div>
<div class="m2"><p>که تابان بود چون ماه و خور از گاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مه کوه غور بادا مه دز غور</p></div>
<div class="m2"><p>که آنجا گشت چشم من کور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به کوه غور ماهم را بکشتند</p></div>
<div class="m2"><p>چنان کشته در اشکفتی بهشتند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به کوه غور در اشکفت دیوان</p></div>
<div class="m2"><p>همی شادی کنند امروز دیوان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه دانند زین خون خود چه خیزی</p></div>
<div class="m2"><p>چه مایه خون آزادان بریزد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به خون ویسه گر جیحون برانم</p></div>
<div class="m2"><p>ز خون دشمان وز دیدگانم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نباشد قیمت یک قطره خونش</p></div>
<div class="m2"><p>که آمد زان رخان لاله گونش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>الا ای مرو پیرایهء خراسان</p></div>
<div class="m2"><p>مدار این خون و این پتیاره آسان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز کوه غور گر آب تو زاید</p></div>
<div class="m2"><p>بجای آب زین پس خون نماید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>شود امسال خونین جویبارت</p></div>
<div class="m2"><p>بلا روید ز کوه و مرغزارت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فزون از برگها بر شاخساران</p></div>
<div class="m2"><p>سنان بینی و تیغ نامداران</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نیارامد شه تو تا به شاهی</p></div>
<div class="m2"><p>ببارد زی تو طوفان تباهی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کمر بندد به خون ویس دلبر</p></div>
<div class="m2"><p>ز بوم با ختر تا بوم خاور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو آیند از همه گیتی سواران</p></div>
<div class="m2"><p>بسایندت به سم راهواران</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جهان بر دست موبد گشت ویران</p></div>
<div class="m2"><p>نیازی دخترم چون شد ز گیهان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شکر اکنون بود خوش طعم و شیرین</p></div>
<div class="m2"><p>که منده نیست آن یاقوت رنگین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به باغ اکنون ببالد سرو و شمشاد</p></div>
<div class="m2"><p>که منده نیست آن شمشاد آزاد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کنون خوشبوی باشد مشک و عنبر</p></div>
<div class="m2"><p>که مانده نیست آن دو زلف دلبر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کنون لاله دمد بر کوه و هامون</p></div>
<div class="m2"><p>که منده نیست آن رخسار گلگون</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>حسود ویس بودی روز نوروز</p></div>
<div class="m2"><p>که نه چون روی او بودی دل افروز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کنون امسال گل زیبا بر آید</p></div>
<div class="m2"><p>نبیند چون رخش رعناتر آید</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بهار امسال نیکوتر بخندد</p></div>
<div class="m2"><p>که شرم ویس بر وی ره نبندد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دریغا ویس من بانوی ایران</p></div>
<div class="m2"><p>دریغا ویس من خاتون توران</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دریغا ویس من مهر خراسان</p></div>
<div class="m2"><p>دریغا ویس من ماه کهستان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دریغا ویس من ماه سخن گوی</p></div>
<div class="m2"><p>دریغا ویس من سرو سمن بوی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دریغا ویس من خورشید کشور</p></div>
<div class="m2"><p>دریغا ویس من امید مادر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کجایی ای نگار من کجایی</p></div>
<div class="m2"><p>چرا جویی همی از من جدایی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کجا جویم ترا ای ماه تابان</p></div>
<div class="m2"><p>به طارم یا به گلشن یا به ایوان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هر آن روزی بنشستی به طارم</p></div>
<div class="m2"><p>به طارم در تو بودی باغ خرم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هر آن روزی که بنشستی به گلشن</p></div>
<div class="m2"><p>به گلشن در نگشتی ماه روشن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هر آن روزی که بنشستی به ایوان</p></div>
<div class="m2"><p>به ایوان در نبودی تاج کیوان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>اگر بی تو ببینم لاله در باغ</p></div>
<div class="m2"><p>نهد لاله برین خسته دلم داغ</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>اگر بی تو ببینم در چمن گل</p></div>
<div class="m2"><p>شود آن گل همه در گردنم غل</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اگر بی تو ببینم بر فلک ماه</p></div>
<div class="m2"><p>به چشمم ماه مار است و فلک جاه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ندانم چون توانم زیست بی تو</p></div>
<div class="m2"><p>که چشمم رودخون بگریست بی تو</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ببایستم همی مرگ تو دیدن</p></div>
<div class="m2"><p>به پیری زهر هجرانت چشیدن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>اگر بر کوه خارا باشد این درد</p></div>
<div class="m2"><p>به یک ساعت کند مر کوه را گرد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>وگر بر ژرف دریا باشد این غم</p></div>
<div class="m2"><p>به یک ساعت کند چون سنگ بی نم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چرا زادم چنین بدنخت فرزند</p></div>
<div class="m2"><p>چرا کردم چنین وارونه پیوند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نبایستم به پیری ماه زادن</p></div>
<div class="m2"><p>بپروردن به دست دیو دادن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>روم تا مرگ بنشینم غریوان</p></div>
<div class="m2"><p>بنالم بر دز اشکفت دیوان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بر آرم زین دل سوزان یکی دم</p></div>
<div class="m2"><p>بدرم سنگ آن دز یکسر ازهم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دزی کان جای دیوان بود و گر بز</p></div>
<div class="m2"><p>چرا بردند حورم را در آن دز</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>روم خود را بیندازم از آن کوه</p></div>
<div class="m2"><p>که چون جشنی بود مرگی به انبوه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نبینم کام دل تا زو جدا ام</p></div>
<div class="m2"><p>به ناکامی چنین زنده چرا ام</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>روم آنجا سپارم جان پاکم</p></div>
<div class="m2"><p>بر آمیزم به خاک ویس خاکم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ولیکن جان خویش آنگه سپارم</p></div>
<div class="m2"><p>که دود از جان شاهنشه بر آرم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نشاید ویس من در خاک خفته</p></div>
<div class="m2"><p>شهنشه دیگری در بر گرفته</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نشاید ویس من در خاک ریزان</p></div>
<div class="m2"><p>شهنشه می خورد در برگ ریزان</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>شوم فتنه برانگیزم ز گیهان</p></div>
<div class="m2"><p>بگویم با همه کس راز پنهان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>شوم با باد گویم تو همانی</p></div>
<div class="m2"><p>که بوی از ویس من بردی نهانی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به حق آنکه بو از وی گرفتی</p></div>
<div class="m2"><p>هر آن گاهی که بر زلفش برفتی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مرا در خون آن بت باشد یاور</p></div>
<div class="m2"><p>هلاک از دشمان او بر آور</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>شوم با ماه گویم تو همانی</p></div>
<div class="m2"><p>که بر ویسم حسد بردی نهانی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به حق آنکه بودی آن دلارم</p></div>
<div class="m2"><p>ترا اندر جهان هم چهر و هم نام</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>مرا یاری ده اندر خون آن ماه</p></div>
<div class="m2"><p>که من خونش همی خواهم ز بدخواه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>شوم با مهر گویم کامگارا</p></div>
<div class="m2"><p>به نام خویش یاور باش مارا</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>کجا خود ویس را افسر تو بودی</p></div>
<div class="m2"><p>و یا بر افسرش گوهر تو بودی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به حق آنکه تو مانند اویی</p></div>
<div class="m2"><p>چو او خوبی چو او رخشنده رویی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>به شهر دوستانش نور بفزای</p></div>
<div class="m2"><p>به شهر دشمانش روی منمای</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>روم با ابر گویم تو همانی</p></div>
<div class="m2"><p>که چون گفتار ویسم در فشانی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>دو دست ویس با تو یار بودی</p></div>
<div class="m2"><p>همیشه چون تو گوهر بار بودی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به حق آنکه او بود ابر رادی</p></div>
<div class="m2"><p>بجای برق خنده ش بود و شادی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>به شهر دشمنش بر بار طوفان</p></div>
<div class="m2"><p>به سیل اندر جهنده برق رخشان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>شوم لابه کنم در پیش دادار</p></div>
<div class="m2"><p>به خاک اندر بمالم هر دو رخسار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>خدایا تو حکیم و بردباری</p></div>
<div class="m2"><p>که بر موبد همی آتش نباری</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>جهان دادی به دست این ستمگر</p></div>
<div class="m2"><p>که هست اندر بدی هر روز بدتر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>نبخشاید همی بر بندگانت</p></div>
<div class="m2"><p>به بیدادی همی سوزد جهانت</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو تیغ آمد همه کارش بریدن</p></div>
<div class="m2"><p>چو گرگ آمد همه رایش دریدن</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>خدایا داد من بستان ز جانش</p></div>
<div class="m2"><p>تهی کن زو سرای و خان و مانش</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو دود از من بر آورد این ستمگر</p></div>
<div class="m2"><p>تو دود از شادی و جانش برآور</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو موبد دید زریهای شهرو</p></div>
<div class="m2"><p>هم از وی بیمش آمد هم ز ویرو</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بدو گفت ای گرامی تر ز دیده</p></div>
<div class="m2"><p>ز من بسیار گونه رنج دیده</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>مرا تو خواهری ویرو برادر</p></div>
<div class="m2"><p>سمنبر ویسه ام بانو و دلبر</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مرا ویس است چشم و روشنایی</p></div>
<div class="m2"><p>فزون از جان و چوز و پادشایی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بر آن بی مهر چو نان مهربانم</p></div>
<div class="m2"><p>که او را دوستر دارم ز جانم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گر او نا راستی با من نکردی</p></div>
<div class="m2"><p>به کام دل ز مهرم بر بخوردی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>کنون حالش همی از تو نهفتم</p></div>
<div class="m2"><p>ازیرا با تو این بیهوده گفتم</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>من آن کس را بکشتن چون توانم</p></div>
<div class="m2"><p>که جانش دوستر دارم ز جانم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>اگر چه من به دست او اسیرم</p></div>
<div class="m2"><p>همی خواهم که در پیشش بمیرم</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>اگر چه من به داغ او چنینم</p></div>
<div class="m2"><p>همی خواهم که او را شاد بینم</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>تو بر دردش مخوان فریاد چندین</p></div>
<div class="m2"><p>مزن بر روی زرین دست سیمین</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>کجا من نیز همچون تو نژندم</p></div>
<div class="m2"><p>نژندی خویشتن را کی پسندم</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>فرستم ویس را از دز بیارم</p></div>
<div class="m2"><p>که با دردش همی طاقم ندارم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ندانم زو چه خواهد دید جانم</p></div>
<div class="m2"><p>خطا گفتم ندانم نیک دانم</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بسا تلخی که من خواهم چشیدن</p></div>
<div class="m2"><p>بسا سختی که من خواهم کشیدن</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>مرا تا ویس باشد در شبستان</p></div>
<div class="m2"><p>نبینم زو مگر نیرنگ و دستان</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>مرا تا ویس جفت و یار باشد</p></div>
<div class="m2"><p>همین اندوه خوردن کار باشد</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>هر آن رنجی که از ویس آیدم پیش</p></div>
<div class="m2"><p>همی بینم سراسر زین دل ریش</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>دلی دارم که در فرمان من نیست</p></div>
<div class="m2"><p>تو پنداری که این دل زان من نیست</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>به تخت پادشاهی بر نشسته</p></div>
<div class="m2"><p>چنان گورم به چنگ شیر خسته</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>در کامم شده بسته به صد بند</p></div>
<div class="m2"><p>به بخت من مزایاد ایچ فرزند</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>مرا کزدست دل روزی طرب نیست</p></div>
<div class="m2"><p>گر از ویسم نباشد بس عجب نیست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>پس آنگه زرد را فرمود خسرو</p></div>
<div class="m2"><p>که چون باد شتابان سوی دز رو</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>ببر با خویشتن دو صد دلاور</p></div>
<div class="m2"><p>دگر ره ویس را از دز بیاور</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بشد زرد سپهبد با دو صد مرد</p></div>
<div class="m2"><p>به یک مه ویس را پیش شه آورد</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>هنوز از زخم شه آزرده اندام</p></div>
<div class="m2"><p>چنانچون خسته گوری جسته از دام</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بد آن یک ماه رامین دل شکسته</p></div>
<div class="m2"><p>به خان زرد متواری نشسته</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>پس آنگه زرد پیش شاه شاهن</p></div>
<div class="m2"><p>سخن گفت از پی رامین فراوان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>دگر ره شاه رامین را عفو کرد</p></div>
<div class="m2"><p>دریده بخت رامین را رفو کرد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>دگر ره دیو کینه روی بنهفت</p></div>
<div class="m2"><p>گل شادی به باغ مهر بشکفت</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>دگر ره در سرای شاه شاهان</p></div>
<div class="m2"><p>فروزان گشت روی ماه ماهان</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>به رامش گشت عیش شاه شیرین</p></div>
<div class="m2"><p>به باده بود دست ماه رنگین</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>گشاده دست شادی بند رادی</p></div>
<div class="m2"><p>گرفته باز رادی کبگ شادی</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>دگر باره بر آمد روزگاری</p></div>
<div class="m2"><p>که جز رامش نکردند ایچ کاری</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>زمین را در گل و نسرین گرفتند</p></div>
<div class="m2"><p>روان را در می نوشین گرفتند</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>جهنده شد به نیکی باد ایشان</p></div>
<div class="m2"><p>برفت آن رنجها از یاد ایشان</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>نه غم ماند نه شادی این جهان را</p></div>
<div class="m2"><p>فنا فرجام باشد هردوان را</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>به شادی دار را تا توانی</p></div>
<div class="m2"><p>که بفزاید ز شادی زندگانی</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چو روز ما همی بر ما نپاید</p></div>
<div class="m2"><p>درو بیهوده غم خوردن چه باید</p></div></div>