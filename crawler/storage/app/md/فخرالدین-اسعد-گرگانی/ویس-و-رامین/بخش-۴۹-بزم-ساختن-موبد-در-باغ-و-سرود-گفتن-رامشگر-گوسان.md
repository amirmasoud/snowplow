---
title: >-
    بخش ۴۹ - بزم ساختن موبد در باغ و سرود گفتن رامشگر گوسان
---
# بخش ۴۹ - بزم ساختن موبد در باغ و سرود گفتن رامشگر گوسان

<div class="b" id="bn1"><div class="m1"><p>مه اردیبهشت و روز خرداد</p></div>
<div class="m2"><p>جهان از خرمی چون کرخ بغداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیابان از خوشی همچون گلستان</p></div>
<div class="m2"><p>گلستان از صنم همچون بتستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درخت رود باری سیم ریزان</p></div>
<div class="m2"><p>نسیم نو بهاری مشک بیزان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چمن مجلس بهاران مجلس آرای</p></div>
<div class="m2"><p>زنان بلبلش چنگ و فاخته نای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درو نرگس چو ساقی جام بردست</p></div>
<div class="m2"><p>بنفشه سر فرو افگنده چون مست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گوهر شاخها چون تاج کسری</p></div>
<div class="m2"><p>ز پیکر باگها چون روی لیلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سبزه روی هامون چون زمرد</p></div>
<div class="m2"><p>ز لاله کوه سنگین چون زبرجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه صحرا ز لاله روی حورا</p></div>
<div class="m2"><p>همه مرز از بنفشه جعد زیبا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهشت آسا زمین با زیب و خوشی</p></div>
<div class="m2"><p>عروس آسا جهان با ناز و گشّی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به باغ اندر نشسته شاه شاهان</p></div>
<div class="m2"><p>به نزدش ویس بانو ماه ماهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دست راست بر آزاده ویرو</p></div>
<div class="m2"><p>به دست چپ جهان آرای شهرو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشسته گرد رامینش برابر</p></div>
<div class="m2"><p>به پیش رام گوسان نواگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی زد راههای خوشگواران</p></div>
<div class="m2"><p>همی کردند شادی نامداران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می آسوده در مجلس همی گشت</p></div>
<div class="m2"><p>رخ میخواره همچون می همی رشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرودی گفت گوسان نو آیین</p></div>
<div class="m2"><p>درو پوشید حال ویس و رامین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر نیکو بیندیشی بدانی</p></div>
<div class="m2"><p>که معنی چیست زیر این نهانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>           </p></div>
<div class="m2"><p>           </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درختی رسته دیدم بر سر کوه</p></div>
<div class="m2"><p>که از دلها زداید زنگ اندوه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درختی سر کشیده تا به کیوان</p></div>
<div class="m2"><p>گرفته زیر سایه نیم گیهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به زیبایی همی ماند به خورشید</p></div>
<div class="m2"><p>جهان در برگ و بارش بسته امید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به زیرش سخت روشن چشمهء آب</p></div>
<div class="m2"><p>که آبش نوش و رییگش در خوشاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شکفته بر کنارش لاله و گل</p></div>
<div class="m2"><p>بنفشه رسته و خیری و سنبل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چرنده گاو کیلی بر کنارش</p></div>
<div class="m2"><p>گهی آبش خورد گه نو بهارش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه آب این چشمه روان باد</p></div>
<div class="m2"><p>درختش بارور گاوش جوان باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شهنشه گفت با گوسان نائی</p></div>
<div class="m2"><p>زهی شایستهء گوسان نوائی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرودی گوی بر رامین بد ساز</p></div>
<div class="m2"><p>بدر بر روی مهرش پردهء راز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بشنید این ویس سمنبر</p></div>
<div class="m2"><p>بکند از گیسوان صد حلقهء زر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به گوسان داد و گفت این مر ترا باد</p></div>
<div class="m2"><p>به حال من سرودی نغز کن یاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرودی گوی هم بر راست پرده</p></div>
<div class="m2"><p>ز روی مهر ما بردار پرده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو شاهت راز ما فرمود گفتن</p></div>
<div class="m2"><p>ز دیگر کس چرا باید نهفتن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگر باره بزد گوسان نوائی</p></div>
<div class="m2"><p>نوائی بود بر رامین گوائی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همان پیشین سرود نغز را باز</p></div>
<div class="m2"><p>بگفت و آشکارا کرد کرد او راز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درخت بارور شاه شهانست</p></div>
<div class="m2"><p>که زیر سایه اش نیمی جهانست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برش عز است و برگش نیکنامی</p></div>
<div class="m2"><p>سرش جاهست و بیخش شاد کامی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهان را در بر و برگش امید است</p></div>
<div class="m2"><p>میان هر دو پیداتر ز شید است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به زیرش ویس بانو چشمهء آب</p></div>
<div class="m2"><p>لبانش نوش و دندان درّ خوشاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شکفته بر رخانش لاله و گل</p></div>
<div class="m2"><p>بنفشه رسته و خیری و سنبل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو گیلی گاو رامین بر کنارش</p></div>
<div class="m2"><p>گهی آبش خورد گه نوبهارش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بماند این درخت سایه گستر</p></div>
<div class="m2"><p>ز مینو باد وی را سایه خوشتر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه آب این چشمه رونده</p></div>
<div class="m2"><p>همیشه گاو گیلی زو چرنده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو گوسان این نوا را کرد پایان</p></div>
<div class="m2"><p>به یاد دوستان و دل ربایان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شه شاهان به خشم از جای بر جست</p></div>
<div class="m2"><p>گرفتش ریش رامین را به یک دست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به دیگر دست زهر آلود خنجر</p></div>
<div class="m2"><p>بدو گفت ای بداندیش و بد اختر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بخور با من به مهر و ماه سوگند</p></div>
<div class="m2"><p>که با ویست نباشد مهر و پیوند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>و گرنه سرت را بردارم از تن</p></div>
<div class="m2"><p>که از ننگ تو بی سر شد تن من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی سوگند خورد آزاده رامین</p></div>
<div class="m2"><p>به دادار جهان و ماه و پروین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که تا من زنده باشم در دو گیهان</p></div>
<div class="m2"><p>نمی خواهم که بر گردم ز جانان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرا قبله بود آن روی گلگون</p></div>
<div class="m2"><p>چنان چون دیگران را مهر گردون</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا او جان شیرینست و از جان</p></div>
<div class="m2"><p>به کام خویشتن ببرید نتوان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شهنشه را فزون شد کینهء رام</p></div>
<div class="m2"><p>زبان بگشاد یکباره به دشنام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیفگندش بدان تا سر ببرد</p></div>
<div class="m2"><p>به خنجر جای مهرش را بدرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سبک رامین دودست شاه بگرفت</p></div>
<div class="m2"><p>تو گفتی شیر نر روباه بگرفت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز شادروان به خاک اندر فگندش</p></div>
<div class="m2"><p>ز دستش بستد آن هندی پرندش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شهنشه مست بود از باده بیهوش</p></div>
<div class="m2"><p>گسسته آگهی و رفته نیروش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نبودش آگهی از کار رامین</p></div>
<div class="m2"><p>نماند اندر دلش آزار رامین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خرد را چند گونه رنج و سستی</p></div>
<div class="m2"><p>پدید آید همی از عشق و مستی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر این دو رنج بر موبد نبودی</p></div>
<div class="m2"><p>مرو را هیچ گونه بد نبودی</p></div></div>