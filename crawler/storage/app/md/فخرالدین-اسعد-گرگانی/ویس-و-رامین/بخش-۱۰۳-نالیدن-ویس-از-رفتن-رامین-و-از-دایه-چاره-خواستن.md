---
title: >-
    بخش ۱۰۳ - نالیدن ویس از رفتن رامین و از دایه چاره خواستن
---
# بخش ۱۰۳ - نالیدن ویس از رفتن رامین و از دایه چاره خواستن

<div class="b" id="bn1"><div class="m1"><p>چو رامین دور گشت از ویس دلبند</p></div>
<div class="m2"><p>نشاط و کام ازو ببرید پیوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه ماه بود آنگاه شد خور</p></div>
<div class="m2"><p>چنو زرد و چنو بی خواب و بی خور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیاسود از حدیث و یاد رامین</p></div>
<div class="m2"><p>نگارین رخ به خون کرده نگارین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دایه گفت دایه چاره ای ساز</p></div>
<div class="m2"><p>که رفته یار بد مهر آیدم باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مهر ای دایه بر جانم ببخشای</p></div>
<div class="m2"><p>مرا راهی به وصل دوست بنمای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که من با این بلا طاقت ندارم</p></div>
<div class="m2"><p>شکیب درد این فرقت ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز من بنیوش دایه داستانم</p></div>
<div class="m2"><p>که چون آب روان بر تو بخوانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدانم دل به نادانی ز دستم</p></div>
<div class="m2"><p>کنون از بیدلی گویی که مستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن زین بیدلی بر من ملامت</p></div>
<div class="m2"><p>که خود بر خاست از هجرم قیامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی آتش بیامد در من افتاد</p></div>
<div class="m2"><p>مرا در دل ترا در دامن افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیش آب هر آتش زبون شد</p></div>
<div class="m2"><p>مرا از آب چشم آتش فزون شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی ریزم برو سیل بهاران</p></div>
<div class="m2"><p>که دید آتش فزاینده ز باران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شب من دوش چونان بُد که گفتم</p></div>
<div class="m2"><p>مگر بر سوزن و بر خار خفتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون روزست و وقت چاشتگاهست</p></div>
<div class="m2"><p>به چشمس چون شب تازی سیاهست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا روز از خان دوست باشد</p></div>
<div class="m2"><p>چو درمان از لبان دوست باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی تا هجر آن دلسوز بینم</p></div>
<div class="m2"><p>نه درمان یابم و نه روز بینم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندانم بر سر من چه نبشتست</p></div>
<div class="m2"><p>که کار بخت با من سخت زشتست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شوم در دشت گردم با شبانان</p></div>
<div class="m2"><p>نگردم نیز گرد مهربانان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به شهر از گریه ام طوفان بخیزد</p></div>
<div class="m2"><p>به کوه از ناله ام خارا بریزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندانم چون کنم با که نشینم</p></div>
<div class="m2"><p>به جای دوست در عالم که بینم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نبینم گیتی و دیده ببندم</p></div>
<div class="m2"><p>کجا از هر چه بینم مستمندم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چسود آمد دلم را زینکه دیدم</p></div>
<div class="m2"><p>جز آنک از خواب و آرامم بریدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فراوان بخت خود را آزمودم</p></div>
<div class="m2"><p>ازو جز خسته و غمگین نبودم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تباهی روزگار خود فزایم</p></div>
<div class="m2"><p>چو بخت آزموده آزمایم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شنیدی داستان من سراسر</p></div>
<div class="m2"><p>کنون درمان کارم چیست بنگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جوابش داد دایه گفت هرگز</p></div>
<div class="m2"><p>نباید بودن اندر کار عاجز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازین گریه وزین ناله چه آید</p></div>
<div class="m2"><p>جز آن کت غم به غم بر می فزاید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همالان تو در شادی و نازند</p></div>
<div class="m2"><p>به کام دل همه گردن فرازند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو همواره چنین در رنج و دردی</p></div>
<div class="m2"><p>به غم خوردن قرارم را ببردی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهان از بهر جان خویش باید</p></div>
<div class="m2"><p>همه دارو ز بهر ریش باید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ترا درمان و هم ریشت بدستست</p></div>
<div class="m2"><p>چرا دست تو از چاره ببستست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ترا دادست یزدان پادشایی</p></div>
<div class="m2"><p>تمامی و بزرگی و روایی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شهرو داری اندر خانه مادر</p></div>
<div class="m2"><p>چو ویرو یاور و رخ برادر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو رامین یار شایسته تو داری</p></div>
<div class="m2"><p>سزای خسروی و شهریاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همت گنجست آگنده به گوهر</p></div>
<div class="m2"><p>همت پشتست با بسیار لشکر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بزرگی را همین باشد بهانه</p></div>
<div class="m2"><p>بزرگی جوی و کم کن این فسانه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو موبد را بسی زشتی نمودی</p></div>
<div class="m2"><p>همیدون چند بارش آزمودی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه دیو خیم او گشتست بهتر</p></div>
<div class="m2"><p>نه کوه خشم او گشتست کمتر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همانست او که بود و تو همانی</p></div>
<div class="m2"><p>همین خواهید بودن جاودانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس اکنون چاره و درمان خود جوی</p></div>
<div class="m2"><p>که هم تخمست و هم آبست و هم جوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز پیش آنکه موبد دست یابد</p></div>
<div class="m2"><p>ز کین دل به خون ما شتابد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که او را دل ز ماهر سه به کین است</p></div>
<div class="m2"><p>به کین ما چو شیر اندر کمین است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو در دل کن که او یک روزناگاه</p></div>
<div class="m2"><p>چو ره یابد بیاید از کمینگاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نیابی همرهی بهتر ز رامین</p></div>
<div class="m2"><p>به سر بر نه مرورا تاج زرین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو بانو باش تا او شاه باشد</p></div>
<div class="m2"><p>بهم با تو چو خوربا ماه باشد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نماند در زمانه شاه و سالار</p></div>
<div class="m2"><p>که نه در کار او با تو بودیار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نخستین یاورت باشد برادر</p></div>
<div class="m2"><p>پس آنگه نامور شاهان دیگر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که شاهان پاک با موبد به کینند</p></div>
<div class="m2"><p>همه رامین و ویرو را گزینند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مدارا با خرد بسیار کردی</p></div>
<div class="m2"><p>بلا از بهر دل بسیار خوردی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کنون چاری به دست اور ز دانش</p></div>
<div class="m2"><p>که این اندوهها گرددت رامش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کنون کن گر توانی کرد کاری</p></div>
<div class="m2"><p>که زین بهتر نیابی روزگاری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به مرو اندر نه شاهست و نه لشکر</p></div>
<div class="m2"><p>تو داری گنج شاهنشاه یک سر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چه مایه رنج بر دست او بدین گنج</p></div>
<div class="m2"><p>کنون تو یافتی همواره بی رنج</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به دینارش بخر شاهی و فرمان</p></div>
<div class="m2"><p>که شاهی را بها داری فراوان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز پیش آنکه او بر تو خوردشام</p></div>
<div class="m2"><p>تو بر وی چاشم خور تا توبری نام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر این تدبیر خواهی کرد منشین</p></div>
<div class="m2"><p>ز حال خویش نامه کن به رامین</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بگویش تا ز موبد باز گردد</p></div>
<div class="m2"><p>به رفتن باد را انباز گردد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو او آید یکی چاره بسازیم</p></div>
<div class="m2"><p>که موبد را به بدروزی بتازیم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بشنید این سخن ویس سمن بوی</p></div>
<div class="m2"><p>بر آمد لالهء شادیش از روی</p></div></div>