---
title: >-
    بخش ۳ - گفتار اندر ستایش سلطان ابوطالب  طغرلبك
---
# بخش ۳ - گفتار اندر ستایش سلطان ابوطالب  طغرلبك

<div class="b" id="bn1"><div class="m1"><p>سه طاعت واجب آمد بر خردمند</p></div>
<div class="m2"><p>که آن هر سه به هم دارند پیوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یشانست دل را شاد کامی</p></div>
<div class="m2"><p>وزیشانست جان را نیک نامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از فرمان این هر سه مگردان</p></div>
<div class="m2"><p>اگر شواهی که یابی هر دو گیهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین گیتی ستوده زندگانی</p></div>
<div class="m2"><p>بدان گیتی نهشت جاودانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی فرمان دادار جهانست</p></div>
<div class="m2"><p>که جان را زو نجات جاودانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوم فرمان پیغمبر محمد</p></div>
<div class="m2"><p>که آن را کافی بی دین کند رد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیم فرمان سلطان جهاندار</p></div>
<div class="m2"><p>به ملک اندر بهای دین دادار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابوطالب شهنشاه معظم</p></div>
<div class="m2"><p>خداوند خداوندان عالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک طغرلبک آن خورشید همت</p></div>
<div class="m2"><p>به هر کس زو رسیده عز و نعمت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظفر وی را دلیل و جود گنجور</p></div>
<div class="m2"><p>وفا وی را امین و عقل دستور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مر آن را کاوست هم نام محمد</p></div>
<div class="m2"><p>چو او منصور شد چون او مؤید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پدید آمد ز مشرق همچو خورشید</p></div>
<div class="m2"><p>به دولت شاه شاهان شد چو جمشید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هندی تیغ بسته هند و خاور</p></div>
<div class="m2"><p>به تر کی جنگ جویان روم و بربر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میان بسته ست بر ملک گشادن</p></div>
<div class="m2"><p>جهان گیرد همی از دست دادن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه خوانی قصهء ساسانیان را</p></div>
<div class="m2"><p>همیدون دفتر سامانیان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخوان اخبار سلطان را یکی بار</p></div>
<div class="m2"><p>که گردد آن همه بر چشم تو خوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیابی اندرو چنان که خواهی</p></div>
<div class="m2"><p>شگفتیهای پیروزی و شاهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نوادرها و دولتهای دوران</p></div>
<div class="m2"><p>عجایبها و قدرتهای یزدان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخوان اخبار او را تا بدانی</p></div>
<div class="m2"><p>که کس ملکت نیابد رایگانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمین ماورالنهر و خراسان</p></div>
<div class="m2"><p>سراسر شاه را بوده ست میدان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نبردی کرده بر هر جایگاهی</p></div>
<div class="m2"><p>برو بشکسته سالاری و شاهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو از توران سوی ایران سفر کرد</p></div>
<div class="m2"><p>چو کیخسرو به جیحون بر گذر کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ستورش بود کشتی بخت رهبر</p></div>
<div class="m2"><p>خدایش بود پشت و چرخ یاور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگر تا چون یقین دلش بد پک</p></div>
<div class="m2"><p>که بر رودی چنان بگذشت بی باک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو نشکوهید او را دل ز جیحون</p></div>
<div class="m2"><p>چرا بشکوهد از حال دگر گون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه از گرما شکوهد نه ز سرما</p></div>
<div class="m2"><p>نه از ریگ و کویر و کوه و دریا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بیابانهای خوارزم و خراسان</p></div>
<div class="m2"><p>به چشمش همچنان آید که بستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیدون شخ های کوه قارن</p></div>
<div class="m2"><p>به چشمش همچنان آید که گلشن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه چون شاهان دیگر جام جویست</p></div>
<div class="m2"><p>که از رنج آن نام جویست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همی تا آب جیحون راز پس ماند</p></div>
<div class="m2"><p>دو صد جیحون ز خون دشمنان راند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی طوفان ز شمشیرش بر آمد</p></div>
<div class="m2"><p>کزو روز همه شاهان سر آمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدان گیتی روان شاه مسعود</p></div>
<div class="m2"><p>خجل بود از روان شاه محمود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کجا او سرزنش کردی فراوان</p></div>
<div class="m2"><p>که بسپردی به نادانی خراسان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنون از بس روان شهریاران که</p></div>
<div class="m2"><p>که با باد روان گشتند یاران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه از دست او شمشیر خوردند</p></div>
<div class="m2"><p>همه شاهی و ملک او را سپردند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روان او برست از شرمساری</p></div>
<div class="m2"><p>که بسیارند همچون او به زاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به نزدیک پدر گشته ست معذور</p></div>
<div class="m2"><p>که بهتر زو بسی شه دید مقهور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کدامین شاه در مشرق گه رزم</p></div>
<div class="m2"><p>توانستی زدن با شاه خوارزم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شناسد هر که در ایام ما بود</p></div>
<div class="m2"><p>که کار شه ملک چون برسما بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سوار ترک بودش صد هزاری</p></div>
<div class="m2"><p>که بس بد با سپاهی زان سواری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بس کاو تاختن برد و شبیخون</p></div>
<div class="m2"><p>شکوهش بود ز آن رستم افزون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خداوند جهان سلطان اعظم</p></div>
<div class="m2"><p>به تدبیر صواب و رای محکم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان لشکر بدرد روز کینه</p></div>
<div class="m2"><p>که سندان گران مر آبگینه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هم از سلطان هزیمت شد به خواری</p></div>
<div class="m2"><p>هم اندر راه کشته شد به زاری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بد اندیشان سلطان آنچه بودند</p></div>
<div class="m2"><p>همین روز و همین حال آند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر آن کهتر که با مهتر ستیزد</p></div>
<div class="m2"><p>چنان افتد که هرگز برنخیزد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تنش گردد شقاوت را فسانه</p></div>
<div class="m2"><p>روانش تیر خذلان را نشانه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>و لیکن گر ورا دشمن نبودی</p></div>
<div class="m2"><p>پس این چندین هنر با که نمودی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر ظلمت ننودی سایه گستر</p></div>
<div class="m2"><p>نبودی قدر خورشید منور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همیدون شاه گیتی قدر والاش</p></div>
<div class="m2"><p>پدید آورد مردم را به اعداش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو صافی کرد خوارزم خراسان</p></div>
<div class="m2"><p>فرود آمد به طبرستان و گرگان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زمینی نیست در عالم سراسر</p></div>
<div class="m2"><p>ازو پژموده تر از وی عجبتر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سه گونه جای باشد صعب و دشوار</p></div>
<div class="m2"><p>یکی دریا دگر آجام و کهسار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سراسر کوه او قلعه همانا</p></div>
<div class="m2"><p>چو خندق گشته در دامانش دریا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نداند زیرک آن را وصف کردن</p></div>
<div class="m2"><p>نداند دیو در وی راه بردن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درو مردان جنگی گیل و دیلم</p></div>
<div class="m2"><p>دلیران و هنرجویان عالم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هنرشان غارتست و جنگ پیشه</p></div>
<div class="m2"><p>بیامخته دران دریا و بیشه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو رایتهای سلطان را بدیدند</p></div>
<div class="m2"><p>چو دیو از نام یزدان در رمیدند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از آن دریا که آنجا هست افزون</p></div>
<div class="m2"><p>ازیشان ریخت سلطان جهان خون</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کنون یابند آنجا بر درختان</p></div>
<div class="m2"><p>به جای میوه مغز شوربختان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو صافی گشت شهر و آن ولایت</p></div>
<div class="m2"><p>از انجا سوی ری آورد رایت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به هر جایی سپهداران فرستاد</p></div>
<div class="m2"><p>که یک یک مختصر با تو کنم یاد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سپهداری به مکران رفت و گرگان</p></div>
<div class="m2"><p>یکی دیگر به موصل رفت و خوزان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی دیگر به کرمان رفت و شیراز</p></div>
<div class="m2"><p>یکی دیگر به ششتر رفت و اهواز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یکی دیگر به اران رفت و ارمن</p></div>
<div class="m2"><p>فگند اندر دیار روم شیون</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سپهداران او پیروز گشتند</p></div>
<div class="m2"><p>بد اندیشان او بدروز گشتند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>رسول آمد بدو از ارسلان خان</p></div>
<div class="m2"><p>به نامه جست ازو پیوند و پیمان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فرستادش به هدیه مال بسیار</p></div>
<div class="m2"><p>پذیرفتش خراج ملک تاتار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جهان سالار با وی کرد پیوند</p></div>
<div class="m2"><p>که دید او را به شاهی بس خردمند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وزان پس مرد مال آمد ز قیصر</p></div>
<div class="m2"><p>چنان کاید ز کهتر سوی مهتر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خراج روم ده ساله فرستاد</p></div>
<div class="m2"><p>اسیران را ز بندش کرد آزاد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به عنوریه با قصرش برابر</p></div>
<div class="m2"><p>مناره کرد و مسجد کرد و منبر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نوشته نام سلطان بر مناره</p></div>
<div class="m2"><p>شده زو دین اسلام آشکاره</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز شاه شام نیز آمد رسولی</p></div>
<div class="m2"><p>ننوده عهد را بهتر قبولی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>فرستاده به هدیه مال بسیار</p></div>
<div class="m2"><p>وزآن جمله یکی یاقوت شهوار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>یکی یاقوت رمانی بشکوه</p></div>
<div class="m2"><p>بزرگ و گرد و ناهنوار چون کوه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز رخشانی چو خورشید سما بود</p></div>
<div class="m2"><p>خراج شام یک سالش بها بود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ابا خوبی و با نغزی و رنگش</p></div>
<div class="m2"><p>بر آمد سی و شش مثقال سنگش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ازان پس آمدش منضور و خلعت</p></div>
<div class="m2"><p>لوای پادشاهی از خلیفت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بپوشید آن لوا را در صفاهان</p></div>
<div class="m2"><p>بدانش تهنیت کردند شاهان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به یک رویه ز چین تا مصر و بربر</p></div>
<div class="m2"><p>شدند او را ملوک دهر چاکر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>میان دجله و جیهون جهانیست</p></div>
<div class="m2"><p>ولیکن شاه را چون بوستانیست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>رهی گشتند او را زور دستان</p></div>
<div class="m2"><p>ز دل کردند بیرون مکور دستان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>همی گردد در این شاهانه بستان</p></div>
<div class="m2"><p>به کام خویش با درگه پرستان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هزاران آفتاب اندر کنارش</p></div>
<div class="m2"><p>هزاران اژدها اندر حصارش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گهی دارد نشست اندر خراسان</p></div>
<div class="m2"><p>گهی در اصفهان و گه به گرگان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>از اطراف ولایت هر زمانی</p></div>
<div class="m2"><p>به فتهی آورندش مژدگانی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز بانگ طبل و بوق مژده خواهان</p></div>
<div class="m2"><p>نخفتم هفت مه اندر صفاهان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به ماهی در نباشد روزگاری</p></div>
<div class="m2"><p>کز اقلیمی نیارندش نثاری</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جهان او راست می دارد شادی</p></div>
<div class="m2"><p>که و مه را همی بخشد به رادی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مرادش زین جهان جز مردمی نه</p></div>
<div class="m2"><p>ز یزدان ترسد و از آدمی نه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بر اطراف جهان شاهان نامی</p></div>
<div class="m2"><p>ازو جویند جاه و نیک نامی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ازیشان هر کرا او به نوازد</p></div>
<div class="m2"><p>ز بخت خویش آن کس بیش نازد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>به درگاه آنکه او را کهترانند</p></div>
<div class="m2"><p>مه از خانان و بیش از قیصرانند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>کجا از خان و قیصر سال تا سال</p></div>
<div class="m2"><p>همی آید پیاپی گونه گون مال</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کرا دیدی تو از شاهان کشور</p></div>
<div class="m2"><p>بدین نام و بدین جاه و بدین فر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>کدامین پادشه را بود چندین</p></div>
<div class="m2"><p>ز مصر و شام و موصل تا در چین</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>کدامین پادشه را این هنر بود</p></div>
<div class="m2"><p>که نزرنج و نه از مرگش حذر بود</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>سزد گر جان او چندان بماند</p></div>
<div class="m2"><p>که افزونتر ز جویدان بماند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>هزاران آفرین بر جان او باد</p></div>
<div class="m2"><p>مدار چرخ بر فرمان او باد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ستاره رهنمای کام او باد</p></div>
<div class="m2"><p>زمانه نیک خواه نام او باد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>شهنشاهی و نامش جاودان باد</p></div>
<div class="m2"><p>تنش آسوده و دل شادمان باد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>کجا رزمش بود پیروزگر باد</p></div>
<div class="m2"><p>کجا بزمش بود با جاه و فرباد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به هر کامی نشاط او را قرین باد</p></div>
<div class="m2"><p>به هر کاری خدا او را معین باد</p></div></div>