---
title: >-
    بخش ۴۲ - بردن شاه موبد  ویس را به دز اشکفت و صفت دز و خبر یافتن رامین از ویس
---
# بخش ۴۲ - بردن شاه موبد  ویس را به دز اشکفت و صفت دز و خبر یافتن رامین از ویس

<div class="b" id="bn1"><div class="m1"><p>دز اشکفت بر کوه کلان بود</p></div>
<div class="m2"><p>نه کوهی بود بر جی زاسمان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سختی سنگ او مانند سندان</p></div>
<div class="m2"><p>نکردی کار بر وی هیچ سوهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس پهنا یکی نیم جهان بود</p></div>
<div class="m2"><p>ز بس بالا ستونی زاسمان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شب بالاش بودی شمع پیکر</p></div>
<div class="m2"><p>به سر بر آتش او را ماه و اختر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو مردم ندیم ماه بودی</p></div>
<div class="m2"><p>ز راز آسمان آگاه بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بر دز برد موبد دلستان را</p></div>
<div class="m2"><p>مهی دیگر بیفزود آسمان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیکر دز چو سنگین مجمری بود</p></div>
<div class="m2"><p>نگه کن تا چه نیکو پیکری بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مجمر در رخان ویس آتش</p></div>
<div class="m2"><p>بر آن آتش عبیر آن خال دلکش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حصار از روی آن ماه حصاری</p></div>
<div class="m2"><p>شکفت همچو باغ نو بهاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سمنبر ویس با دایه نشسته</p></div>
<div class="m2"><p>شهنشه پنج در بر وی ببسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه در ها به مهر خویش کرده</p></div>
<div class="m2"><p>همه مهرش برادر را سپرده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در صد گنج بر ویسه گشاده</p></div>
<div class="m2"><p>در آن جا ساز صد ساله نهاده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آن دز بود بختش را همه کام</p></div>
<div class="m2"><p>مگر پیوند یار و دیدن رام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شاهنشه ز کار دز بپردخت</p></div>
<div class="m2"><p>سوی مرو آمد و کام سفر ساخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپاهی بود همچون کوه آهن</p></div>
<div class="m2"><p>بتر مردی درو بهتر ز بیژن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به رفتن هر یکی خندان و نازان</p></div>
<div class="m2"><p>مگر رامین که گریان بود و نالان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز تاب مهر سوزان تب گرفته</p></div>
<div class="m2"><p>چو کبگی باز در مخلب گرفته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غبار حسرتش بر رخ نشسته</p></div>
<div class="m2"><p>امید وصلتش در دل شکسته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به جسمش جان شیرین خوار گشته</p></div>
<div class="m2"><p>به زیرش خزو دیبا خار گشته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهروز او را قرار و نه شب آرام</p></div>
<div class="m2"><p>به کام دشمنان افتاده بی کام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جگر پر ریش گشته دل پر از نیش</p></div>
<div class="m2"><p>همی گفتی نهانی با دل خویش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه عشقست اینکه هر گز کم نگردد</p></div>
<div class="m2"><p>دلم روزی ازو خرم نگردد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا تا هست با عشق آشنایی</p></div>
<div class="m2"><p>نبیند چشم بختم روشایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر هر بار میزد بر دلم خار</p></div>
<div class="m2"><p>خدنگ زهر پیکان زد ازین بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برفت از پیش چشمم آن دلارام</p></div>
<div class="m2"><p>که بی او نیست در تن صبر و آرام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به عشق اندر وفاداری نکردم</p></div>
<div class="m2"><p>چو روز هجر او دیدم نمردم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو سنگینه دلم چه آهنینم</p></div>
<div class="m2"><p>که گیتی را همی بی او ببینم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر باشد تنم بی روی جانان</p></div>
<div class="m2"><p>همان بهتر که باشم نیز بی جان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رفیقا حال ازین بتر چه دانی</p></div>
<div class="m2"><p>که مر گم خوشترست از زندگانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر جنان من با من نباشد</p></div>
<div class="m2"><p>همان خوشتر که جان در تن نباشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بهر دوست خواهم جان شیرین</p></div>
<div class="m2"><p>چنان کز بهر دیدارش جهان بین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کنون کز بخت خود بی یار گشتم</p></div>
<div class="m2"><p>ز جان و دیدگان بیزار گشتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو نالیدی چنثن از بخت بد ساز</p></div>
<div class="m2"><p>به دل کردی سرودی دیگر آغاز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دلاگر عاشقی ناله بیاور</p></div>
<div class="m2"><p>که بیدار هوا را نیست داور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که بخشاید به گیتی عاشقان را</p></div>
<div class="m2"><p>که بخشایش کند درد کسان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر نالم همی بر داد نالم</p></div>
<div class="m2"><p>که ببریدند شادی را نهالم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ببردند آفتابم را ز پیشم</p></div>
<div class="m2"><p>ز هجرش پر نمک کردند ریشم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ببار ای چشم من خونابم اکنون</p></div>
<div class="m2"><p>کدامین روز را داری همی خون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا هر گز غمی چونین نباشم</p></div>
<div class="m2"><p>سزد کت اشک جز خونین نباشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر بودی به غم زین پیش خونبار</p></div>
<div class="m2"><p>سزد گر جان فرو باری بدین بار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به باران تازه گردد روی گیهان</p></div>
<div class="m2"><p>چرا پژمرده شد رویم ز باران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دلم را آتش تیمار بگدخت</p></div>
<div class="m2"><p>به چشم آورد و بر زرین رخم تاخت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرستن گرچه از مردان نه نیکوست</p></div>
<div class="m2"><p>زمن نیکوست در هجر چنان دوست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو باز آمد ز راه دز شهنشاه</p></div>
<div class="m2"><p>ز حال ویس، رامین گشت آگاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>غمش بر غم فزود و درد بردرد</p></div>
<div class="m2"><p>نشستش گرد هجران بر رخ زرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو طوفان از مژه بارید باران</p></div>
<div class="m2"><p>بشست از روی زردش گرد هجران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی گفتی سحنهای دل انگیز</p></div>
<div class="m2"><p>که باشد مرد عاشق را دل آویز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من آن خسته دلم کز دوست دورم</p></div>
<div class="m2"><p>ز بخت آزرده ام وز دل نفورم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنانم تا حصاری گشت یارم</p></div>
<div class="m2"><p>که گویی بسته در رویین حصارم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ببر بادا پیام من به دلبر</p></div>
<div class="m2"><p>بگو صد داغ تو دارم به دل بر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا در دیده دیدار تو ماندست</p></div>
<div class="m2"><p>چو اندر یاد گفتار تو ماندست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی خواب از دو چشمم من ستردست</p></div>
<div class="m2"><p>یکی گیتی ز یاد من ببردست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درین سختی اگر من آهنینم</p></div>
<div class="m2"><p>نمانم تا رخانت باز بینم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر درد مرا قسمت توان کرد</p></div>
<div class="m2"><p>نماند در جهان یک جان بی درد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چنان گشتم ز درد و ناتوانی</p></div>
<div class="m2"><p>که مرگم خوشترست از زندگانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرا زین درد کی باشم رهایی</p></div>
<div class="m2"><p>که درمانم توی وز من جدایی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو رامین را به روی آمد چنین حال</p></div>
<div class="m2"><p>شد از مویه موی از ناله چون نال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همان دشمن که دیرین دشمنش بود</p></div>
<div class="m2"><p>چو روی او بدید او را ببخضود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به یک گفته ز بیماری چنان شد</p></div>
<div class="m2"><p>که سیمین تیر وی زرین کمان شد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فتاده در عماری زار و نالان</p></div>
<div class="m2"><p>بیامد با شهنشه تا به گرگان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جنان شد کز جهان امید برداشت</p></div>
<div class="m2"><p>تو گفتی زهر پیکان در جگرداشت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بزرگان پیش شاهنشاه رفتند</p></div>
<div class="m2"><p>یکایک حال او با شه بگفتند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به خواهش باز گفتند ای خداوند</p></div>
<div class="m2"><p>ترا رامین برادر هست و فرزند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نیایی در جهان چون او سواری</p></div>
<div class="m2"><p>به هر فرهنگ چون او نامداری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همه کس را چو او کهتر بیاید</p></div>
<div class="m2"><p>کزو بسیار کام دل بر آید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ترا در پیش چون او یک برادر</p></div>
<div class="m2"><p>اگر دانی به از بسیار لشکر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ازو دندان دشمن بر تو کندست</p></div>
<div class="m2"><p>که او شیر دمان و پیل تندست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اگر روزی ازو آزرده بودی</p></div>
<div class="m2"><p>عفو کردی و خشنودی ننودی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کنون تازهمکن آزار رفته</p></div>
<div class="m2"><p>به کینه مشکن این شاخ شکفته</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کزو تا مرگ بس راهی نماندست</p></div>
<div class="m2"><p>ز کوهش باز جز کاهی نماندست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همین یک بار بر جانش ببخشای</p></div>
<div class="m2"><p>مرو را این سفر کردن مفرمای</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سفر خود خوش نباشد با درستی</p></div>
<div class="m2"><p>نگر تا چون بود با درد و سستی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نمانش تا بیاساید یکی ماه</p></div>
<div class="m2"><p>که بس خسته شد او از شدت راه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو گردد درد لشتی بر وی آسان</p></div>
<div class="m2"><p>به دسرورت شود سوی خراسان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مگر به سازدش آن آب آن شهر</p></div>
<div class="m2"><p>که این کضور چو زهرست آن چو پازهر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو بشنید این سخن شاه از بزرگان</p></div>
<div class="m2"><p>نماند آزاده رامین را به گرگان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو شاهنشه بشد رامین بیاسود</p></div>
<div class="m2"><p>همه دردی از اندامش بپالود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>دگر ره ز عفرانش گشت</p></div>
<div class="m2"><p>کمانش باز شمشاد جوان گشت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فتادش یوبهء دیدار دلبر</p></div>
<div class="m2"><p>چو آتش در دل و چون تیر در بر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>برفت از شهر گرگان یک سواره</p></div>
<div class="m2"><p>به زیرش تندرو بادی تخاره</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سرایان بود چون بلبل همه راه</p></div>
<div class="m2"><p>به گوناگون سرود و گونه گون راه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نخواهم بی تو یارا زندگانی</p></div>
<div class="m2"><p>نه آسانی نه کام این جهانی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نترسم چون ترا جویم ز دشمن</p></div>
<div class="m2"><p>اگر باشد جهانی دشمن من</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>و گر راهم سراسر مار باشد</p></div>
<div class="m2"><p>برو صد آهنین دیوار باشد</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همه آبش بود جای نهنگان</p></div>
<div class="m2"><p>همه کوهش بود جای پلنگان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گیا بر دشت اگر شمشیر باشد</p></div>
<div class="m2"><p>وگر ریگش چو ببر و شیر باشد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سنومش باد باشد صاعقه میغ</p></div>
<div class="m2"><p>نبارد بر سرم زان میغ چز تیغ</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بود مر باد او را گرد پیکان</p></div>
<div class="m2"><p>چنان چون ابر او را سنگ باران</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به جان تو کز آن ره بر نگردم</p></div>
<div class="m2"><p>و گر چونانکه بر گردم نه مردم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگر دیدار تو باشد در آتش</p></div>
<div class="m2"><p>نهم دو چشم بینایم بر آتش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>و گر وصل تو باشد در دم شیر</p></div>
<div class="m2"><p>مرا با او سخن باشد به شمشیر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ره وصلت مرا کوتاه باشد</p></div>
<div class="m2"><p>سه ماهه راه گامی راه باشد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو باشد گر بود شمشیر در راه</p></div>
<div class="m2"><p>شهاب و برق بارد بر سر ماه</p></div></div>