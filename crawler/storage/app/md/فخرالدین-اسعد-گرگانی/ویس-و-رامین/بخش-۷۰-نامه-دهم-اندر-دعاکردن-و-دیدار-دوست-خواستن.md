---
title: >-
    بخش ۷۰ - نامهء دهم اندر دعاکردن و دیدار دوست خواستن
---
# بخش ۷۰ - نامهء دهم اندر دعاکردن و دیدار دوست خواستن

<div class="b" id="bn1"><div class="m1"><p>دل پر آتش و جانی پر از دود</p></div>
<div class="m2"><p>تنی چون موی و رخساری زر اندود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برم هر شب سحرگه پیش دادار</p></div>
<div class="m2"><p>بمالم پیش او بر خاک رخسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروش من بدرد پشت ایوان</p></div>
<div class="m2"><p>فغان من ببندد راه کیوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان گریم که گرید ابر آذار</p></div>
<div class="m2"><p>چنان نالم که نالد کبگ کهسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان جوشم که جوشد بحر از باد</p></div>
<div class="m2"><p>چنان لرزم که لرزد سرو و شمشاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اشک از شب فرو شویم سیاهی</p></div>
<div class="m2"><p>بیاغارم زمین تا پشت ماهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان از حسرت دل بر کشم آه</p></div>
<div class="m2"><p>کجا ره گم کند بر آسمان ماه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس کز دل کشم آه جهان سوز</p></div>
<div class="m2"><p>ز خاور بر نیارد آمدن روز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس کز جان بر آرم دود اندوه</p></div>
<div class="m2"><p>ببندد ابر تیره کوه تا کوه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدین خواری بدین زاری بدین درد</p></div>
<div class="m2"><p>مژه پر آب و روی زرد و پر گرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی گویم خدایا کردگارا</p></div>
<div class="m2"><p>بزرگا کامگارا برد بارا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو یار بی دلان و نی کسانی</p></div>
<div class="m2"><p>همیشه چارهء بیچارگانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیام گفت راز خویس با کس</p></div>
<div class="m2"><p>مگر با تو که یار من توی بس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی دانی که چون خسته روانم</p></div>
<div class="m2"><p>همی دانی که چون بسته زبانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زبانم با تو گوید هر چه گوید</p></div>
<div class="m2"><p>روانم از تو جوید هرچه جوید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو ده جان مرا زین غم رهایی</p></div>
<div class="m2"><p>تو بردان از دلم بند جدایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل آن سنگدل را نرم گردان</p></div>
<div class="m2"><p>به تاب مهربانی گرم گردان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به یاد آور دلش را مهر دیرین</p></div>
<div class="m2"><p>پس آنگه در دلش کن مهر شیرین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی زین غم که من دارم برو نه</p></div>
<div class="m2"><p>که باشد بار او از هر کهی مه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به فصل خویش وی را زی من آور</p></div>
<div class="m2"><p>و یازیدر مرا نزدیک او بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گشاده کن به ما بر راه دیدار</p></div>
<div class="m2"><p>کجا خود بسته گردد راه تیمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی تا باز بینم روی آن ماه</p></div>
<div class="m2"><p>نگه دارش ز چشم و دست بدخواه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بجز مهر منش تیمار منمای</p></div>
<div class="m2"><p>بجز عشق منش آزار مفزای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>و گر رویش نخواهم دید ازین پس</p></div>
<div class="m2"><p>مرا بی روی او جان و جهان بس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم اکنون جان من بستان بدو ده</p></div>
<div class="m2"><p>که من بی جان و آنبت با دو جان به</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگارا چند نالم چند گویم</p></div>
<div class="m2"><p>به زاری چند گریم چند مویم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نگویم بیس ازین در نامه گفتار</p></div>
<div class="m2"><p>و گرچه هست صد چندین سزاوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نباشد گفته بر گوینده تاوان</p></div>
<div class="m2"><p>چه باشد اندک و سودش فراوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگفتم هر چه دیدم از جفایت</p></div>
<div class="m2"><p>ازین پس خود تو می دان با خدایت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر کردار تو با کوه گویم</p></div>
<div class="m2"><p>بموید سنگ او چون من بمویم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببخشاید مرا سنگ و دل نه</p></div>
<div class="m2"><p>به گاه مردمی سنگ از دلت به</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا چون سنگ بودی این دل مست</p></div>
<div class="m2"><p>دلت پولاد گشت و سنگ بشکست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درود از من بداد شمشاد آزاد</p></div>
<div class="m2"><p>که دارد در میان پوشیده پولاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درود از من بدان یاقوت سفته</p></div>
<div class="m2"><p>که دارد سی گهر در وی نهفته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>درود از من بدان عیار نرگس</p></div>
<div class="m2"><p>که دارد مر مرا از خواب مفلس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>درود از من بدان ماه دو هفته</p></div>
<div class="m2"><p>که دارد ماه بخت من گرفته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درود از من بدان باغ شکفته</p></div>
<div class="m2"><p>که دارد خانهء صرم کآشفته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درود از من بدان شاخ صنوبر</p></div>
<div class="m2"><p>که دارد شاژ بختم خشک و بی بر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درود از من بدان گلبرگ خندان</p></div>
<div class="m2"><p>که دارد مر مرا همواره گریان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>درود از من بدان خود روی لاله</p></div>
<div class="m2"><p>که دارد چشمم آگنده به ژاله</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درود از من بدان دو رسته گوهر</p></div>
<div class="m2"><p>درود از من بدان دو خوشه عنبر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>درود از من بدان عیار سرکش</p></div>
<div class="m2"><p>که دارد مرمرا در خواب ناخوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>درود از من بدان دیبای رنگین</p></div>
<div class="m2"><p>درود از من بدان مهناب و پروین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درود از من بدان سر و گل اندام</p></div>
<div class="m2"><p>که دارد مر مرا دل خسته مادام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درود از من بدان زلفین عطار</p></div>
<div class="m2"><p>که زو مر مشک را بشکست بازار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>درود از من بدان چشم فسونگر</p></div>
<div class="m2"><p>که دارد مر مرا بی خواب و بی خور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درود از من بدان رخسار مهوش</p></div>
<div class="m2"><p>که دارد جانم از محنت بر آتش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>درود از من بدان ماه دو هفته</p></div>
<div class="m2"><p>که دارد مر مرا بیهوش و تفته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درود از من بدان مشهور آفاق</p></div>
<div class="m2"><p>که دارد مر مرا از کام دل طاق</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>درود از من بدانگلروی خوشبوی</p></div>
<div class="m2"><p>که دارد سال و ماهم در تگ و پوی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>درود از من بدانزلف رسن باز</p></div>
<div class="m2"><p>که دارد مر مرا مشهور شیراز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درود از من بدان ناز و عاتبش</p></div>
<div class="m2"><p>که آبم برد زنخدان خوشابش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درود از من بدانآیین و آن فر</p></div>
<div class="m2"><p>که دارد رویم از تیمار چون زر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>درود از من بدان گنج نگویی</p></div>
<div class="m2"><p>که دارد پیشه با من کینه جویی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درود از من بدان خورشید تابان</p></div>
<div class="m2"><p>که دارد حسن بر خورشید گیهان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درود از من بدان روی چو گلبرگ</p></div>
<div class="m2"><p>که دارد شرم رخش رریزد ز گل برگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درود از من بدان سرو سمن روی</p></div>
<div class="m2"><p>که ندهد همچو بوی او سمن بوی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>درود از من بدان پیروزگر شاه</p></div>
<div class="m2"><p>درود از من بدان بیدادگر ماه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>درود از من بدان تاج سواران</p></div>
<div class="m2"><p>درود از من بدان رشک بهاران</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>درود از من بدان جان جهانم</p></div>
<div class="m2"><p>درود از من بدان جفت جوانم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>درود از من بدان ماه سمن بوی</p></div>
<div class="m2"><p>درود از من بدان یار جفا جوی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درود از من بدان کاورا درودست</p></div>
<div class="m2"><p>مرا بی او دو دیده چون دو رودست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>درود از من فزون از هر شماری</p></div>
<div class="m2"><p>درود از من فزون از هر بهاری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فزون از ریگ کهسار و بیابان</p></div>
<div class="m2"><p>فزون از قطرهء دریا و باران</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>فزون از رستنی بر کوه و صحرا</p></div>
<div class="m2"><p>فزون از جانوز بر خشک و دریا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فزون از روزگار هر دو دوران</p></div>
<div class="m2"><p>فزون از اختران چرخ گردان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>فزون از گونه گونه تخم عالم</p></div>
<div class="m2"><p>فزون از نر و ماده نسل آدم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فزون از پر مرغ و موی حیوان</p></div>
<div class="m2"><p>فزون از حرف دفترهای دیوان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>فزون از فکرت و اندیشهء ما</p></div>
<div class="m2"><p>فزون از از و هم و کیش و پیشهء ما</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ترا از من درود جاودانی</p></div>
<div class="m2"><p>مرا از تو وفا و مهربانی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ترا از من درود آتشنایی</p></div>
<div class="m2"><p>مرا از ماه رویت روشنایی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هزاران بار چونین باد چونین</p></div>
<div class="m2"><p>دعا از من ز بخت نیک آمین</p></div></div>