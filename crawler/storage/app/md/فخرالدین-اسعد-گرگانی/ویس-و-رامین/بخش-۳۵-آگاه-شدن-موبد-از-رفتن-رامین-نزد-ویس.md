---
title: >-
    بخش ۳۵ - آگاه شدن موبد از رفتن رامین نزد ویس
---
# بخش ۳۵ - آگاه شدن موبد از رفتن رامین نزد ویس

<div class="b" id="bn1"><div class="m1"><p>چو آگه گشت شاهنشاه موبد</p></div>
<div class="m2"><p>که پیدا کرد رامین گوهر بد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر باره بشد با ویس بنشست</p></div>
<div class="m2"><p>گسسته مهر دیگر ره بپیوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل رام آنگهی بشکیبد از ویس</p></div>
<div class="m2"><p>که از کردار بد بشکیبد ابلیس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خر گوش روزی شیر گردد</p></div>
<div class="m2"><p>دل رامین ز ویسه سیر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و گر گنجشک روزی باز گردد</p></div>
<div class="m2"><p>دل رامین ازین خو باز گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان گه شاه شد تا پیش مادر</p></div>
<div class="m2"><p>به دلتنگی گله کرد از برادر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرو را گفت نیکه باشد این کار</p></div>
<div class="m2"><p>نگه کن تا پسندد هیچ هشیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که رامین با زنم جوید تباهی</p></div>
<div class="m2"><p>کند بدنام بر من گاه شاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی زن چون بود با دو برادر</p></div>
<div class="m2"><p>چه باشد در جهان زین ننگ بدتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم یکباره بُر گشت از مدارا</p></div>
<div class="m2"><p>ازیرا کردم این راز آشکارا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من این ننگ از تو بسیاری نهفتم</p></div>
<div class="m2"><p>چو بیچاره شدم با تو بگفتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدان تا تو بدانی حال رامین</p></div>
<div class="m2"><p>نخوانی مر مرا بیهوده نفرین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که من زان ساک کشم او را به زاری</p></div>
<div class="m2"><p>که گردد چشم تو ابر بهاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا تو دوزخی هم تو بهشتی</p></div>
<div class="m2"><p>تو نپسندی به من این نام زشتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپید آنگه شود از ننگ رویم</p></div>
<div class="m2"><p>که رویم را به خون وی بضویم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جوابش داد مادر گفت هرگز</p></div>
<div class="m2"><p>دو دست خود نبرد هیچ گربز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مکش او را که او هستت برادر</p></div>
<div class="m2"><p>ترا چون او برادر نیست دیگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه در رزمت بود انبار و یاور</p></div>
<div class="m2"><p>نه در بزمت بود خورشیدانور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بی رامین شود بی کس بمانی</p></div>
<div class="m2"><p>نه خوش باشدت بی او زندگانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو بنشینی نباشد همنشینت</p></div>
<div class="m2"><p>همان انباز و پشت راستینت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترا ایزد ندادست ایچ فرزند</p></div>
<div class="m2"><p>که روزی بر جهان باشد خداوند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بمان تا کاو بود پشت و پناهت</p></div>
<div class="m2"><p>به دست او بماند جایگاهت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نباشد عمر مردم جاودانی</p></div>
<div class="m2"><p>برو روزی سر آید زندگانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو فرمان خدا آید به جانت</p></div>
<div class="m2"><p>به دست دشمن افتد خان و مانت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همان بهتر که او بر جای باشد</p></div>
<div class="m2"><p>مگر چون تو جهان آرای باشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مگر شاهی درین گوهر بماند</p></div>
<div class="m2"><p>نژاد ما درین کضور بماند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برادر را مکش زن را گسی کن</p></div>
<div class="m2"><p>کلید مهر در دست کسی کن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بتان و خوبرویان بی شمارند</p></div>
<div class="m2"><p>که زلف از مشک و بر ازسیم دارند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی را بت گزین و دل برو نه</p></div>
<div class="m2"><p>کلید گنجها در دست او ده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مگر کت زان صدف دری بیاید</p></div>
<div class="m2"><p>که شاهی را و شادی را بشاید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه داری از نژاد ویسه امید</p></div>
<div class="m2"><p>جز آن کاو آمدست از تخم جمشید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نژادش گرچه شگوارست و نیکوست</p></div>
<div class="m2"><p>ابا این نیکوی صد گونه آهوست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مکن شاها خود را کار فرمای</p></div>
<div class="m2"><p>روانت را بدین کینه میالای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هزاران جفت همچون ویس یابی</p></div>
<div class="m2"><p>چرا دل زان بلایه برنتابی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من این را آگهی دیگر شنیدم</p></div>
<div class="m2"><p>چنان دانم که من بدتر شنیدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شنیدستم که آن بدمهر بدخو</p></div>
<div class="m2"><p>دگر باره شد اندر بند ویرو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به خوردن روز و شب با او نشستست</p></div>
<div class="m2"><p>ز می گه هوشیار و گاه مستست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همیشه ویس از بختش همی خواست</p></div>
<div class="m2"><p>کنون چون دید درد دلش بر خاست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو از رامین بیچاره چه خواهد</p></div>
<div class="m2"><p>کت از ویرو همی آید تباهی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آگر رامین به همدانست ازانست</p></div>
<div class="m2"><p>که او بر ویسه چون تو مهربانست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>و لیکن زین سخن آنجا بماندست</p></div>
<div class="m2"><p>که ویسه مهر او از دل براندستص</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همین آهوست ویس بد نشان را</p></div>
<div class="m2"><p>بدو هر روز دیگر دوستان را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان زیبایی و خوبی چه باید</p></div>
<div class="m2"><p>که مهرش بر کسی ماهی نپاید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به گل ماند که چه خوب رنگست</p></div>
<div class="m2"><p>نپاید دیر و مهرش ی در نگست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو بشنید این سخن موبد ز مادر</p></div>
<div class="m2"><p>دلش خوش گشت لختی بر برادر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنان بر ویس و بر ویرو بیازرد</p></div>
<div class="m2"><p>که گشت از خشم دل رنگ رخش زرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همان گه نزد ویرو کرد نامه</p></div>
<div class="m2"><p>ز تندی کرد چون شمشیر خامه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدو گفت این که فرمودت نگویی</p></div>
<div class="m2"><p>که بر من بیشی و بیداد جویی ؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پناهت کیست یا پشتت کدامست</p></div>
<div class="m2"><p>که رایت بس بلند و خویش کامست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نگویی تا که دادت این دلیری</p></div>
<div class="m2"><p>که روباهی و طبع شیر گیری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو با شیران چرا شیری نمایی</p></div>
<div class="m2"><p>که با گور دمنده بر نیایی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو از من بانوم را چون ستانی</p></div>
<div class="m2"><p>بدین بیچارگی و ناتوانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر چه هست ویسه خواهر تو</p></div>
<div class="m2"><p>زن من چون نشیند در بر تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چرا داری مرو را تو به خانه</p></div>
<div class="m2"><p>بدین کار از تو ننیوشم بهانه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کجا دیدی یکی زن جفت دو شوی</p></div>
<div class="m2"><p>دو پیل کینه ور بسته به یک موی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مگر تا من ندیدم جایگاهت</p></div>
<div class="m2"><p>فزون شد زانکه بد پشت و پناهت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همی تا تو دلیر و شیر مردی</p></div>
<div class="m2"><p>ندیدم در جهان نامی که کردی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نه روزی پادشاهی را ببستی</p></div>
<div class="m2"><p>نه روزی بد سگالی را شکستی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نه باجی بر یکی کضور نهادی</p></div>
<div class="m2"><p>نه شهری را به پیروزی گشادی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هنرهای ترا هر گز ندیدم</p></div>
<div class="m2"><p>نه نیز از دوست وز دشمن شنیدم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نژاد خویشتن دانی که چونست</p></div>
<div class="m2"><p>به هنگام بلندی سر نگونست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو از گوهر همی مانی به استر</p></div>
<div class="m2"><p>که چون پرسند فخر آرد به مادر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ترا تیر افگند بپنم به هر کار</p></div>
<div class="m2"><p>به نخچیر و به بازی نه به پیکار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به میدان اسپ تازی نیک تازی</p></div>
<div class="m2"><p>همیدون گوی تنها نیک بازی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همی تا در شبستان و سرایی</p></div>
<div class="m2"><p>هنرهای یلان نیکو نمایی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو در میدان شوی با هم نبردان</p></div>
<div class="m2"><p>گریزی چون زنان از پیش مردان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همی شیری کنی در کضور ماه</p></div>
<div class="m2"><p>ازو رفته زبون داردت روباه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همانا زخم من کردی فراموش</p></div>
<div class="m2"><p>که از جانت خود برد از تنت هوش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همیدون زخمهای نامداران</p></div>
<div class="m2"><p>ستوده مرغزی چابک سواران</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به کینه همچو شیر مرغزاری</p></div>
<div class="m2"><p>به کوشش همچو رعد نوبهاری</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هنوز از مرزهای کضور ماه</p></div>
<div class="m2"><p>همی آید همانا آوخ و آه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مرا آن تیغ و آن باز و به جایست</p></div>
<div class="m2"><p>که از روی زمین دشمن زدایست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو این نامه بخوانی گوش من دار</p></div>
<div class="m2"><p>که شمشیرم خون تست ناهار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شنیدم هر چه تو گفتی ازین پیش</p></div>
<div class="m2"><p>ننودی مردمان را مردی خویش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همی گفتی که شاه آمد ز ناگاه</p></div>
<div class="m2"><p>چو شیر تند جسته از کمینگاه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ازیرا برد ویسم را ز گوراب</p></div>
<div class="m2"><p>که من بودم به سان مست در خواب</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر من بودمی در کضور ماه</p></div>
<div class="m2"><p>نبردی ویس را هر گز شهنشاه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کنون باری نه مستی هوشیاری</p></div>
<div class="m2"><p>به جای خویش فرخ شهریاری</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز کار خود ترا آگاه کردم</p></div>
<div class="m2"><p>به پیگار تو دل یکتاه کردم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به هر راه برون کن دیدبانی</p></div>
<div class="m2"><p>به هر مرزی همیدون مرزبانی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به گرد آور سپاه بوم ایران</p></div>
<div class="m2"><p>از آذربایگان و ری و گیلان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همی کن ساز لشکر تا من آیم</p></div>
<div class="m2"><p>که من خود زود بندت بر گشایم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>برافشان تو به باد کینه گنجت</p></div>
<div class="m2"><p>که همچون باد بهاشد یافته رنجت</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به جنگی نه چنان آیم من این بار</p></div>
<div class="m2"><p>که تو یابی به جان از جنگ زنهار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>کنم از کشتگان کضورت هامون</p></div>
<div class="m2"><p>به هامون بر برانم دجلهء خون</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بیارم ویس را بی کفش و چادر</p></div>
<div class="m2"><p>پیاده چون سگان در پیش لشکر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چنان رسوا کنم وی را کزین پس</p></div>
<div class="m2"><p>نجوید دشمنی با مهتران کس</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو شاه این نامه زی ویرو فرستاد</p></div>
<div class="m2"><p>همان گه مهتران را آگهی داد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز راه ماه وز پیگار ویرو</p></div>
<div class="m2"><p>همه کردند ساز خویش نیکو</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سحرگاهان بر آمد نالهء نای</p></div>
<div class="m2"><p>روان شد همچو دریا لشکر از جای</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تو گفتی رود جیحون از خراسان</p></div>
<div class="m2"><p>همی آمد دمان سوی کهستان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هر آن جایی که لشکر گه زدی شاه</p></div>
<div class="m2"><p>نیارستی گذشتن بر سرش ماه</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>زمین از بار لشکر بود بستوه</p></div>
<div class="m2"><p>که می رفتند همچون آهنین کوه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تو گفتی سد یأجوجست لشکر</p></div>
<div class="m2"><p>هم ایشان باز چون مأجوج بی مر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>همی شد پیگ در پیش شهنشاه</p></div>
<div class="m2"><p>شهنشاه از قفای پیگ در راه</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو پیگ آمد به نزد شاه ویرو</p></div>
<div class="m2"><p>بشد وی را ز دست و فای نیرو</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>جهان بر چشم ویرو تیره گون شد</p></div>
<div class="m2"><p>ز خشم شاه چشمش نمچو خون شد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همه گفت ای عجب چندین سخن چیست</p></div>
<div class="m2"><p>مرو را این همه پرخاش با کیست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نشانده خواهرم را در شبستان</p></div>
<div class="m2"><p>برون کرده به دی ماه زمستان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>هم او زد پس هنو برداشت فریاد</p></div>
<div class="m2"><p>بدان تا باشد از دو گونه بیداد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گزیده خواهرم اکنون زن اوست</p></div>
<div class="m2"><p>تو گویی بدسگال و دشمن اوست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به صد خواری ز پیش خود براندش</p></div>
<div class="m2"><p>به یک نامه دگر باره نخواندش</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>گناه او کرد و بر ما کینه ور گشت</p></div>
<div class="m2"><p>چنین باشد کسی کز داد بر گشت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>نه سنگینست شاهنشه نه رویین</p></div>
<div class="m2"><p>چه بایستش بگفتن لاف چندین</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>سپاه آورد یک بار و مرا دید</p></div>
<div class="m2"><p>چنان کم دید دانم کم پسندید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز پیش من به بدروزی چنان شد</p></div>
<div class="m2"><p>که از خواری به گیتی داستان شد</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نه پنهان بود چنگ ما دو سالار</p></div>
<div class="m2"><p>که دیگر گون توان کردن به گفتار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>از آن پس کاو ز دست ما بیفتاد</p></div>
<div class="m2"><p>چرا پینود بر ما این همه باد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>عجبتر زین ندیدم داستانی</p></div>
<div class="m2"><p>دو تن ترسد ز بشکسته کمانی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چه ترساند مرا کاو بود ترسان</p></div>
<div class="m2"><p>ندارد هیچ بخرد جنگم آسان</p></div></div>