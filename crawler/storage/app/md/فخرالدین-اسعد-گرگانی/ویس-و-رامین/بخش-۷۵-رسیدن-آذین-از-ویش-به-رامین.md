---
title: >-
    بخش ۷۵ - رسیدن آذین از ویش به رامین
---
# بخش ۷۵ - رسیدن آذین از ویش به رامین

<div class="b" id="bn1"><div class="m1"><p>خوشا بادا که از مشرق در آید</p></div>
<div class="m2"><p>تو گویی کز گلستانی بر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خرخیز و سمندور و ز قیصور</p></div>
<div class="m2"><p>بیارد بوی مشک و عود و کافور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خوش باشد نسیم باد خاور</p></div>
<div class="m2"><p>به خاصه چون بود با بوی دلبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیمی کز کنار دلبر آید</p></div>
<div class="m2"><p>ز بوی مشک و عنبر خوشتر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیامد از گلستان بوی نسرین</p></div>
<div class="m2"><p>چنان چون بوی ویس آمد به رامین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی گفت این نه بوی گلستانست</p></div>
<div class="m2"><p>همانا بوی ویش دلستانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه بادست این که اومید بهی داد</p></div>
<div class="m2"><p>مرا از بوی دلبر آگهی داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین اندیشه بود آزاده رامین</p></div>
<div class="m2"><p>که آمد پیش بخت افروز آذین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو آذین را بدید از دور بشناخت</p></div>
<div class="m2"><p>همانگه رخش گلگون را بدو تاخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیام آور فرود آمد ز باره</p></div>
<div class="m2"><p>نه باره بد یکی پیل تخاره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکفته روی و خندان رفت آذین</p></div>
<div class="m2"><p>زمین بوسه کنان در پیش رامین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دمان زو بوی مشک و بوی عنبر</p></div>
<div class="m2"><p>نه بوی مشک و عنبر بوی دلبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه فرخ بود آذین پیش رامین</p></div>
<div class="m2"><p>چه در خور بود رامین پیش آذین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شده هر دو به روی یکدگر شاد</p></div>
<div class="m2"><p>چنانک اندر بهاران سرو و شمشاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس آنگه هر دو اسپان را ببستند</p></div>
<div class="m2"><p>به دشت سبر بر مرزی نشستند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیام آور بپرسیدش فراوان</p></div>
<div class="m2"><p>ز رفته حالهای روزگاران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن پس داد وی را نامهء ویس</p></div>
<div class="m2"><p>همان پیراهن و واشمهء ویس</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو رامین نامهء آن سیم بر دید</p></div>
<div class="m2"><p>تو گفتی گور دشتی شیر نر دید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز لرزه سست شد دو دست و پایش</p></div>
<div class="m2"><p>ربودش هوش یاد دلربایش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان لرزه به دست او بر افتاد</p></div>
<div class="m2"><p>که آن نامه ز دست او در افتاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی تا نامهء دلبر همی خواند</p></div>
<div class="m2"><p>ز دیده سیل بیجاده همی راند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گهی بر رخ نهادی نامه ویس</p></div>
<div class="m2"><p>گهی بر دل نهادی جامه ویس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گهی بوبید مشک آلود جامه</p></div>
<div class="m2"><p>گهی بوسید خون آلود نامه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی ابر از دو چشم او بر آمد</p></div>
<div class="m2"><p>که بارانش وقیق و گوهر آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وز آن ابر او فتادش برق بر دل</p></div>
<div class="m2"><p>بدیدش برق آتش سوز در دل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گهی از دیده راندی گوهرین جوی</p></div>
<div class="m2"><p>گهی از دل کشیده آذرین هوی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گهی چون دیو زد بیگوش گشتی</p></div>
<div class="m2"><p>فغان کردی و پس خاموش گشتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گهی بیخود به روی اندر گتادی</p></div>
<div class="m2"><p>ز بیهوشیش گریه برفتادی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه لختی هوش باز آمد به جانش</p></div>
<div class="m2"><p>صدف شد در دندان را دهانش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همی گفت آه ازین بخت نگونسار</p></div>
<div class="m2"><p>که تخم رنج کشت و شاخ تیمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا ببرید از آن سرو جوانه</p></div>
<div class="m2"><p>که سروستان او کاخست و خانه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا ببرید از آن خورشید تابان</p></div>
<div class="m2"><p>که گردونش شبستانست و ایوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز چشم من ببرد آن خوب دیدار</p></div>
<div class="m2"><p>چو از گوشم ببرد آن نوش گفتار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز دیدارش بدل دادست جامه</p></div>
<div class="m2"><p>ز گفتارش بدل دادست نامه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قرار جان من زین جامه آمد</p></div>
<div class="m2"><p>بهار بخت من زین نامهء آمد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس آنگه پاسخی بنوشت زیبا</p></div>
<div class="m2"><p>بسی نیکوتر از منسوج دیبا</p></div></div>