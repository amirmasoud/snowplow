---
title: >-
    بخش ۵۲ - پاسخ دادن ویس  موبد را
---
# بخش ۵۲ - پاسخ دادن ویس  موبد را

<div class="b" id="bn1"><div class="m1"><p>چو بشنید این سخن ویس دلارای</p></div>
<div class="m2"><p>چو سرو بوستانی جست از جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدو گفت ای گرانمایه خداوند</p></div>
<div class="m2"><p>گران تر حکمت از کوه دماوند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل تو پیشه کرده بردباری</p></div>
<div class="m2"><p>کف تو پیشه کرده در باری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا دادست یزدان هر چه باید</p></div>
<div class="m2"><p>هنرهایی که اورنگت فزاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنرهای تو پیداتر ز خورشید</p></div>
<div class="m2"><p>کنشهای تو زیباتر ز امید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توی فرخ شهنشاه زمانه</p></div>
<div class="m2"><p>بمان اندر زمانه جاودانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به همت آسمان نامداری</p></div>
<div class="m2"><p>به دولت آفتاب کامگاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خجسته نام چون خورشید تابان</p></div>
<div class="m2"><p>رونده حکم چون تقدیر یزدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خداوندا تو خود دانی که گردون</p></div>
<div class="m2"><p>کند هر ساعتی لونی دگرگون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنشهایی کزو بینیم هموار</p></div>
<div class="m2"><p>بدو بر حکم و بر فرمان دادار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدا او را به اندازه براندست</p></div>
<div class="m2"><p>کم و بیشش بر آن اندازه ماندست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آغاز جهان تا روز فرجام</p></div>
<div class="m2"><p>به رفتن سربسر یکسان نهد گام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان گردد که دادارش بفرمود</p></div>
<div class="m2"><p>چنان چون خواست او را راه بنمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهی و بتری در ما سرشتست</p></div>
<div class="m2"><p>چنان چون نیک و بد بر ما نبشتست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه از دانش دگر گردد سرشته</p></div>
<div class="m2"><p>نه از مردی دگر گردد نوشته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درین گیتی چه نادان و چه گربز</p></div>
<div class="m2"><p>به کار خویش حیرانند و عاجز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آگر پاکست طبعم یا پلیدست</p></div>
<div class="m2"><p>چنانست او که یزدان آفریدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو از آغاز گشتم آفریده</p></div>
<div class="m2"><p>بدان آندازه گشتم پروریده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو یزدان مر ترا پیروز کردست</p></div>
<div class="m2"><p>مگر جان مرا بد روز کردست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من از خوبی و زشتی بی گناهم</p></div>
<div class="m2"><p>کجا من خویشتر را بد نخواهم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه من گفتی که نپذیرم سلامت</p></div>
<div class="m2"><p>همه غم خواهم و رنج و ملامت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا از بهر سختی آفریدند</p></div>
<div class="m2"><p>چنان کز بهر خواری پروریدند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه من گفتم که گونه زرد خواهم</p></div>
<div class="m2"><p>همیشه جان و دل پر درد خواهم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آن روزی که گفتم شادمانم</p></div>
<div class="m2"><p>شکنجه گشت شادی بر روانم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا چه چاره چون بختم چنینست</p></div>
<div class="m2"><p>تو گویی چرخ با جانم به کینست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز گمراهی دلم همرنگ نیلست</p></div>
<div class="m2"><p>همانا غول بختم را دلیلست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کنون از جان خود گشتی چنان سیر</p></div>
<div class="m2"><p>که خواهم خویشتن را خوردهء شیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به ناخن پردهء دل را بدرم</p></div>
<div class="m2"><p>به دندان رشتهء جان را ببرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه دل باید مرا زین بیش نه جان</p></div>
<div class="m2"><p>که خورد تیمار و دردم هست ازیشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه اندر دل مرا روزی وزد باد</p></div>
<div class="m2"><p>نه جان اندر تنم روزی شود شاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو کار من چنین آشفته ماندست</p></div>
<div class="m2"><p>همیشه چشم بختم خفته ماندست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چرا ورزم بدین سان مهربانی</p></div>
<div class="m2"><p>کزو دردست و ننگ جاودانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا دشمن شده چون تو خداوند</p></div>
<div class="m2"><p>ز من بیزار گشته خویش و پیوند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز رازم دشمنان آگاه گشته</p></div>
<div class="m2"><p>جهان بر چشم من چون چاه گشته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدین سختی چه باید مهر کاری</p></div>
<div class="m2"><p>بدین خواری چه باید دوستداری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز بس کامد به گوش من ملامت</p></div>
<div class="m2"><p>شدم یکباره در گیتی علامت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دری در جان تاریکم گشادند</p></div>
<div class="m2"><p>چراغی اندر آن درگه نهادند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فتاد اندر دل من روشنایی</p></div>
<div class="m2"><p>خرد از جان من جست آشنایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز راه مهر جستن باز گشتم</p></div>
<div class="m2"><p>ز رخت مهر دل پرداز گشتم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدانستم که از مهرم به پایان</p></div>
<div class="m2"><p>نیاید جز هلاک هر دو گیهان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مثال مهر همچون ژرف دریاست</p></div>
<div class="m2"><p>کنار و قعر او هر دو نه پیداست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر تا جاودان در وی نشینم</p></div>
<div class="m2"><p>بدو دیده کنارش را نبینم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر جان هزاران نوح دارم</p></div>
<div class="m2"><p>یکی جان را ازو بیرون نیارم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چرا با جان بیچاره ستیزم</p></div>
<div class="m2"><p>چرا بیهوده خون خویش ریزم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چرا از تو نصیحت نه پذیرم</p></div>
<div class="m2"><p>چرا راه سلاممت بر نگیرم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگر بینی ز من دیگر تباهی</p></div>
<div class="m2"><p>بکن با من ز کینه هرچه خواهی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگر رامین ازین پس شیر گردد</p></div>
<div class="m2"><p>نپندارم که بر من چیر گردد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر بادست بویمن نیابد</p></div>
<div class="m2"><p>گذر بر بام و کوی من نیابد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر جادوست از کارم بماند</p></div>
<div class="m2"><p>و گر کیدست از چارم بماند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پذیرفتم هم از تو هم ز یزدان</p></div>
<div class="m2"><p>که هرگز نشکنم این عهد و پیمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر کار پرستش را سزایم</p></div>
<div class="m2"><p>ازین پس تو مرایی من ترایم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دلت خشنود کن یک بار دیگر</p></div>
<div class="m2"><p>کزین پس با تو باشم همچو شکر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همانا گر دهانم را ببویی</p></div>
<div class="m2"><p>ازو آیدت بوی راستگویی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شهنشه چشم و رویش را ببوسید</p></div>
<div class="m2"><p>که بشنید آنکه زو هرگز بنشنید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دگر باره نوازشها نمودش</p></div>
<div class="m2"><p>به نیکو و ستایش بر فزودش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز یکدیگار جدا گشتند خرم</p></div>
<div class="m2"><p>میان دل شکسته لشکر غم</p></div></div>