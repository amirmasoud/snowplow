---
title: >-
    بخش ۶۵ - نامهء پنجم اندر جفا بردن از دوست
---
# بخش ۶۵ - نامهء پنجم اندر جفا بردن از دوست

<div class="b" id="bn1"><div class="m1"><p>ترا دیدم که چونین گش نبودی</p></div>
<div class="m2"><p>چنین تند و چنین سرکش نبودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا دیدم که چون می بر زدی آه</p></div>
<div class="m2"><p>ز آه تو سیه شد بر فلک ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خواری همچو خاک راه بودی</p></div>
<div class="m2"><p>به کام دشمن و بدخواه بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دوزخ بود جان ز بس تاب</p></div>
<div class="m2"><p>چون دریا بود چشم تو ز بس آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن روزی که تو کمتر گرستی</p></div>
<div class="m2"><p>جهان را دجالهء دیگر ببستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون افزونتر از جمشید گشتی</p></div>
<div class="m2"><p>مگر همسایهء خورشید گشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر آن روزها کردی فراموش</p></div>
<div class="m2"><p>که تو بودی زمن بی صبر و بی هوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر آنگاه گشتی از نهانم</p></div>
<div class="m2"><p>که من بر تو چگونه مهربانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر رنجی که دیدی رفت از یاد</p></div>
<div class="m2"><p>کجا بر من کشیدی دست بیدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا با من به تلخی همچو هوشی</p></div>
<div class="m2"><p>که با هر کس به شیرینی چو نوشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کس را همی خوشی نمایی</p></div>
<div class="m2"><p>مرا باری چرا گشی فزایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو با صد گنج پیروزی و نازی</p></div>
<div class="m2"><p>به چندین گنج شاید گر بنازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه باشد گر تو نازی از تن خویش</p></div>
<div class="m2"><p>که ناز من به تو از ناز تو بیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به تو نازم که تو زیبای نازی</p></div>
<div class="m2"><p>بسازم با تو گر با من بسازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ولیکن گر چه روی تو بهارست</p></div>
<div class="m2"><p>همیشه بر رخانت گل بیار است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهار نیکوی بر کس نماند</p></div>
<div class="m2"><p>جهان روزی دهد روزی ستاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مکش چندین کمان بر دوستانت</p></div>
<div class="m2"><p>که ناگه بشکند روزی کمانت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>و گر پر تیر داری جعبهء ناز</p></div>
<div class="m2"><p>همه تیرت به یک عاشق مینداز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا دل چون کبابست ای پریچهر</p></div>
<div class="m2"><p>فگنده روز و شب بر آتش مهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهل تا باشد این آتش فروزان</p></div>
<div class="m2"><p>کبابی را که ببرشتی مسوزان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مکن کاری که من با تو نکردم</p></div>
<div class="m2"><p>مبر آبم که من آبت نبردم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مکن چندین ستم جانا برین دل</p></div>
<div class="m2"><p>که ما هر دو از این خاکیم و زین گل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدم من نیز همچون تو نیازی</p></div>
<div class="m2"><p>نکردم با تو چندین سرفرازی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نباشد دوستی را هیچ خوشی</p></div>
<div class="m2"><p>چو باشد دوستی با عجب و گشّی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه بس جان مرا در جدایی</p></div>
<div class="m2"><p>که نیزش درد بیزاری نمایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز گشّی بر فلک بردی تن خویش</p></div>
<div class="m2"><p>ز عجب آتش زدی در خرمن خویش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو چون من مردمی نه چون خدایی</p></div>
<div class="m2"><p>مرا چندین جفا تا کی نمایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر هستی تو چون خورشید والا</p></div>
<div class="m2"><p>شبانگه هم فرود آیی ز بالا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلی مثل دلت خواهم ز یزدان</p></div>
<div class="m2"><p>سیاه و سرکش و بدمهر و نادان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خداوند چنیندل رسته باشد</p></div>
<div class="m2"><p>جهان از دست این دل خسته باشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رخی بینم ترا چون باغ رنگی</p></div>
<div class="m2"><p>دلی بینم ترا چون کوه سنگین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دریغ آید مرا کت دل چنینست</p></div>
<div class="m2"><p>به گاه بی وفایی آهنینست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر تو هجر جویی من نجویم</p></div>
<div class="m2"><p>و گر تو سرد گویی من نگویم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وفا کارم اگر تو جور کاری</p></div>
<div class="m2"><p>من آب آرم اگر تو آتش آری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وفا را زاد مادر چون مرا زاد</p></div>
<div class="m2"><p>جفا را زاد مادر چون ترا زاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دل من کرد گر با من جفا کرد</p></div>
<div class="m2"><p>که شد طبع وفا در بی وفا کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نشانه کردی او را لاجرم زه</p></div>
<div class="m2"><p>نکو کردی به تیر نرگسان ده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همی زن تا بگویند کاین چرا کرد</p></div>
<div class="m2"><p>بلا بخرید و جان را بها کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ازان خوانند آرش را کمانگیر</p></div>
<div class="m2"><p>که از ساری به مرو انداخت یک تیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو اندازی به جان من ز گوراب</p></div>
<div class="m2"><p>همی هر ساعتی صد تیر پرتاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ترا زیبد نه آرش را سواری</p></div>
<div class="m2"><p>که صد فرسنگ بگذشتی ز ساری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جفا پیشه کنی از راه چندین</p></div>
<div class="m2"><p>چه بی حمت دلی داری چه سنگین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رخم کردی ز خون دیده جیحون</p></div>
<div class="m2"><p>دلم کردی ز درد هجر قارون</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عجبتر آنکه چندین جور بینم</p></div>
<div class="m2"><p>نفرسایم همانا آهنیم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مرا گویند مگری کز گرستن</p></div>
<div class="m2"><p>چو مویی شد به باریکی ترا تن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کسی گرید چنین کز مهر و خویش</p></div>
<div class="m2"><p>شود نومید از دیدار رویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حسودا تو مگر آگه نداری</p></div>
<div class="m2"><p>که در باران بود امیدواری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بهار آید چو بارد ابر بسیار</p></div>
<div class="m2"><p>مگر باز آمد از باران من یار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بهار آمد کنم بر وی گل افشان</p></div>
<div class="m2"><p>چو یار آید کنم بروی دل افشان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به هجرش بر فشانم در و مرجان</p></div>
<div class="m2"><p>به وصلش بر فشانم دیده و جان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر روزی کند یک روز دادار</p></div>
<div class="m2"><p>خوشا روزا که باشد روز دیدار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر جانی فروشندم به صد جان</p></div>
<div class="m2"><p>برافشانم دو صد جان پیش جانان</p></div></div>