---
title: >-
    بخش ۱۱۰ - نشستن رامین بر تخت شهنشاهى
---
# بخش ۱۱۰ - نشستن رامین بر تخت شهنشاهى

<div class="b" id="bn1"><div class="m1"><p>چو آگاهی به رامین شد ز موبد</p></div>
<div class="m2"><p>که او را چون فرو برد اختر بد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی هفته سران لشکر وی</p></div>
<div class="m2"><p>به سوک اندر نشسته همبر وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهانی شکر دادار جهان کرد</p></div>
<div class="m2"><p>که او فرجام موبد را چنان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه جنگی بود مرگش را بهانه</p></div>
<div class="m2"><p>نه خونی ریخته شد در میانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر آمد روز چونان پادشاهی</p></div>
<div class="m2"><p>نبوده هیچ رامین را گناهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزاران سجده برد او پیش دادار</p></div>
<div class="m2"><p>همی گفت ای خداوند نکو کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو دانی گونه گون درها گشادن</p></div>
<div class="m2"><p>که چونین کارها دانی نهادن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برانی هر کرا خواهی ز گیهان</p></div>
<div class="m2"><p>بر آری هر کرا خواهی به کیوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بذیرفتم ز تو تا زنده باشم</p></div>
<div class="m2"><p>که خشنودیت را جویند باشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان بندگانت داد جویم</p></div>
<div class="m2"><p>همیشه راست باشم راست گویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بُوم در پادشاهی داد فرمای</p></div>
<div class="m2"><p>به درویشان کام بخشای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توم یاری ده اندر پادشایی</p></div>
<div class="m2"><p>که یاری دادنم را خود تو شایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توی پشتی توم یاری به هر کار</p></div>
<div class="m2"><p>مرا از چشم و دست بد نگه دار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خداوندم توی من بندهء بند</p></div>
<div class="m2"><p>مرا شاهی تو دادی ای خداوند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خداوندم توی من بندهء تو</p></div>
<div class="m2"><p>که من خود بندام دارندهء تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون کردی چو سالار جهانم</p></div>
<div class="m2"><p>بدار اندر پناه سایبانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو لاله کرد لختی پیش دادار</p></div>
<div class="m2"><p>وزین معنی سخنها گفت بسیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان گه بار را فرمود بستن</p></div>
<div class="m2"><p>سواران سپه را بر نشستن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر آمد بانگ کوس و نالهء نای</p></div>
<div class="m2"><p>روان شد همچو جیحون لشکر از جای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روارو در سپاه افتاد چونان</p></div>
<div class="m2"><p>که از باد صبا در ابر نیسان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو راه حشر گشت آن ره ز غلغل</p></div>
<div class="m2"><p>ز کوه دیلمان تا شهر آمل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان افروز رامین با دل افروز</p></div>
<div class="m2"><p>همی آمد همه ره شاد و فیروز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به شادی روز رام و روز شنبد</p></div>
<div class="m2"><p>فرود آمد به لشکرگاه موبد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزرگان پیش او رفتند یکسر</p></div>
<div class="m2"><p>به دیهیمش بر افشاندند گوهر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرو را پاک شاهنشاه خواندند</p></div>
<div class="m2"><p>ز فر و داد و خیره بماندند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو ابری بود دستش نوبهاری</p></div>
<div class="m2"><p>همی بارید در شاهواری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی هفته به آمل بود خرم</p></div>
<div class="m2"><p>دمادم زد همی رطل دمادم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پس آنگه داد طبرستان به رهام</p></div>
<div class="m2"><p>جوانمرد نکوبخت نکونام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به ایران در نژاد او کیانی</p></div>
<div class="m2"><p>بزرگی در نژادش باستانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیدون داد شهر ری به بهروز</p></div>
<div class="m2"><p>که بودش دوستدار و نیک آموز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدان گاهی که او با ویس بگریخت</p></div>
<div class="m2"><p>به دام شاه موبد در نیاویخت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به ری بهروز کردش میزبانی</p></div>
<div class="m2"><p>به خانه داشتش چندی نهانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به نیکی لاجرم نیکی جزا بود</p></div>
<div class="m2"><p>کجا او خود به نیکی سزا بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بکن نیکی و در دریاش انداز</p></div>
<div class="m2"><p>که روزی گشته لولو یابیش باز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وز آن پس داد گرگان را به آذین</p></div>
<div class="m2"><p>کهبا او یکدل بود و دیرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به درگاهش سپهبد بود ویرو</p></div>
<div class="m2"><p>چو سرهنگ سرایش بود شیرو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دو پیل مست و دو شیر دلاور</p></div>
<div class="m2"><p>به گوهر ویس بانو را برادر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو هر شهری به شاهی دادگر داد</p></div>
<div class="m2"><p>نگهبانی به هر مرزی فرستاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به راه افتاد با لشکر سوی مرو</p></div>
<div class="m2"><p>کجا دیدار او بد داروی مرو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خراسان سر بسر آذین ببستند</p></div>
<div class="m2"><p>پری رویان بر آذینها نشستند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همه راهی ورا چون بوستان شد</p></div>
<div class="m2"><p>همه دستی برو گوهر فشان شد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زبانها بود بر وی آفرین خوان</p></div>
<div class="m2"><p>چو دلها در وفای وی گروگان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو در مرو گزین شد شاه رامین</p></div>
<div class="m2"><p>بهشتی دید در وی بسته آذین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به خوبی همچو نوروز درختان</p></div>
<div class="m2"><p>ز خوشی همچو روز نیک بختان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هزار آوا به دستان رود سازان</p></div>
<div class="m2"><p>شکوفه جامهای دلنوازان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فرازش ابر دود مشک و عنبر</p></div>
<div class="m2"><p>و زو بارنده سیم و زر و گوهر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سه مه آذینها بسته بماندند</p></div>
<div class="m2"><p>وزیشان روز و شب گوهر فشاندند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدین رامش نه خود مرو گزین بود</p></div>
<div class="m2"><p>کجا یکسر خراسان همچنین بود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز موبد سالیان سختی کشیدند</p></div>
<div class="m2"><p>پس از مرگش به آسانی رسیدند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو از بیدار او آزاد گشتند</p></div>
<div class="m2"><p>به داد شاه رامین شاه گشتند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو گفتی یکسر از دوزخ بر ستند</p></div>
<div class="m2"><p>به زیر سایهء طوبی نشستند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدان را بد بود روزی سرانجام</p></div>
<div class="m2"><p>بماند نامشان جاوید بد نام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مکن بد در جهان و بد میندیش</p></div>
<div class="m2"><p>کجا گر بد کنی بد آیدت پیش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه نیکو گفت خسرو کهبدان را</p></div>
<div class="m2"><p>ز دوزخ آفرید ایزد بدان را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از آن گوهر که شان آورد ز آغاز</p></div>
<div class="m2"><p>به پایان هم بدان گوهر برد باز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو رامین دادجوی و دادگر شد</p></div>
<div class="m2"><p>جهان از خفتگان آسوده تر شد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سپهداران او هر جا که رفتند</p></div>
<div class="m2"><p>به فر او همه گیتی گرفتند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو رنج دشمنانش بود بی بر</p></div>
<div class="m2"><p>جهان او را شد از چین تا به بربر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به هر شهری شد از وی شهریاری</p></div>
<div class="m2"><p>به هر مرزی شد از وی مرزداری</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همه ویرانها آباد کردند</p></div>
<div class="m2"><p>هزاران شهر و ده بنیاد کردند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بداندیشان همه بر دار بودند</p></div>
<div class="m2"><p>و یا در چاه و زندان خوار بودند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به هر راهی رباطی کرد و خانی</p></div>
<div class="m2"><p>نشانده بر کنارش راهبانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جهان آسوده گشت از دزد و طرار</p></div>
<div class="m2"><p>ز کرد و لور و از ره گیر و عیار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز بس کاو داد سیم و زر سبیلی</p></div>
<div class="m2"><p>نماند اندر جهان نام بخیلی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز بس کاو داد زر و سیم و گوهر</p></div>
<div class="m2"><p>همه گشتند درویشان توانگر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز دلها گشت بیدادی فراموش</p></div>
<div class="m2"><p>توانگر شد هر آن کاو بود بی توش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نه جستی گرگ بر میشی فزونی</p></div>
<div class="m2"><p>نه کردی میش گرگی را زبونی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به هر هفته سپه را بار دادی</p></div>
<div class="m2"><p>به نیکی پندشان بسیار دادی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به دوار گه نشاندی داوران را</p></div>
<div class="m2"><p>بکندی بیخ و بن بد گوهران را</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به داورگاه او بر شاه و چاکر</p></div>
<div class="m2"><p>یکی بودی و درویش و توانگر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چه پیش او شدی شاهی جهانگیر</p></div>
<div class="m2"><p>به گاه داد جستن چه زنی پیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ور آمد پیش او مرد خدایی</p></div>
<div class="m2"><p>ستوده بود همچون پادشایی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به نزدش مرد پر فرهنگ و دانا</p></div>
<div class="m2"><p>گرامی بود همچون چشم بینا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در ایران هر کسی دانش بیاموخت</p></div>
<div class="m2"><p>بدان راز خود نزدش بر افروخت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>صد و ده سال رامین در جهان بود</p></div>
<div class="m2"><p>از آن هشتاد و سه شاه زمان بود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>میان ملک و جاه و حشمت و مال</p></div>
<div class="m2"><p>بماند آن نامور هشتاد و سه سال</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زمین از داد او آباد گشته</p></div>
<div class="m2"><p>زمان از فر او دلشاد گشته</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به فرش گشته سه چیز از جهان کم</p></div>
<div class="m2"><p>یکی رنج و دوم درد وسوم غم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گهی جان را خورش دادی ز دانش</p></div>
<div class="m2"><p>گهی تن را جوان کردی به رامش</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گهی کردی تماشا در خراسان</p></div>
<div class="m2"><p>گهی نخچیر کردی در کهستان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گهی بودی به طبرستان آباد</p></div>
<div class="m2"><p>گهی رفتی به خوزستان و بغداد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هزاران چشمه و کاریز بگشاد</p></div>
<div class="m2"><p>بریشان شهر و ده بسیار بنهاد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یکی زان شهرها اهواز ماندست</p></div>
<div class="m2"><p>کش او آگهاه شهر رام خواندست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>کنونش گر چه هم اهواز خوانند</p></div>
<div class="m2"><p>به دفتر رام شهرش نام دانند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شهی خوش زندگی بودست خوش نام</p></div>
<div class="m2"><p>که خود در لطف ایشان خوش بود رام</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نه چون اوبد به شاهی سر فرازی</p></div>
<div class="m2"><p>نه چون او بد به رامش رود سازی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نگر تا چنگ چون نیکو نهادست</p></div>
<div class="m2"><p>نکوتر زان نهادی که گشادست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>نشانست این که چنگ بافرین کرد</p></div>
<div class="m2"><p>که او را نام چنگ رامین کرد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو بر رامین مقررگشت شاهی</p></div>
<div class="m2"><p>ز دادش گشت پرمه تا به ماهی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جهان در دست ویس سیمتن کرد</p></div>
<div class="m2"><p>مرو را پادشاه خویشتن کرد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دو فرزند آمدش زان ماه پیکر</p></div>
<div class="m2"><p>چو مالک خوب و چون بابک دلاور</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دو خسرو نامشان خورشید و جمشید</p></div>
<div class="m2"><p>جهان در فر هر دو بسته اومید</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>زمین خاوران دادش به خورشید</p></div>
<div class="m2"><p>زمین باختر دادش به جمشید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>یکی را سغد و خوارزم و چغان داد</p></div>
<div class="m2"><p>یکی را شام و مصر و قیروان داد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>جهان در دست ویس دلستان بود</p></div>
<div class="m2"><p>و ڵیکن خاصش آذربایگان بود</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>همیدون کشور ارّان و ارمن</p></div>
<div class="m2"><p>سراسر بد به دست آن سمن تن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به شاهی سالیان با هم بماندند</p></div>
<div class="m2"><p>به نیکی کام دل یکسر براندند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مهار عمر خود چندان کشیدند</p></div>
<div class="m2"><p>که فرزنان فرزندان بدیدند</p></div></div>