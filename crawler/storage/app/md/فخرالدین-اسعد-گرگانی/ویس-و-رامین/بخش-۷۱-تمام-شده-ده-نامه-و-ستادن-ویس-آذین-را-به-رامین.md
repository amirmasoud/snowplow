---
title: >-
    بخش ۷۱ - تمام شده ده نامه و ستادن ویس  آذین را به رامین
---
# بخش ۷۱ - تمام شده ده نامه و ستادن ویس  آذین را به رامین

<div class="b" id="bn1"><div class="m1"><p>نویسنده چو از نامه بپرداخت</p></div>
<div class="m2"><p>به جای آورد هر چاری که بشناخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مشکین کرد مشکین نوک خامه</p></div>
<div class="m2"><p>به نوک خامه مشکین کرد نامه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفت آن نامه را ویسه ز مشکین</p></div>
<div class="m2"><p>بمالیدش بدان دو زلف مشکین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک فرسنگ بوی نامهء ویس</p></div>
<div class="m2"><p>همی شد همچو بوی جامهء ویس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس آنگه خواند آذین را بر خویش</p></div>
<div class="m2"><p>بدو گفت ای به من شایسته چون خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بودی تو تا امروز چاکر</p></div>
<div class="m2"><p>ازین پس باشی آزاده برادر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جاه اندر ترا انباز دارم</p></div>
<div class="m2"><p>به مهر اندر ترا همراز دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا خواهم فرستاده به رامین</p></div>
<div class="m2"><p>مرا در خورتر از جان و جهان بین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو فرزندی مرا رامین خداوند</p></div>
<div class="m2"><p>عزیز دل خداوندست و فرزند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن در ره درنگ و زود بشتاب</p></div>
<div class="m2"><p>چو باد دی مهی و تیر پرتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که من زین پس به راهت چشم دارم</p></div>
<div class="m2"><p>گهی روز و گهی ساعت شمارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان کن کت نبیند دوست و دشمن</p></div>
<div class="m2"><p>به رامین بر پیام و نامهء من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درودش ده ز من بیش از ستاره</p></div>
<div class="m2"><p>بگو ای ناکس زنهار خواره</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من از تو بد کنش آن رنج دیدم</p></div>
<div class="m2"><p>که درد مرگ را صد ره چشیدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرامش کردی آن سوگند و زنهار</p></div>
<div class="m2"><p>که خوردی بامن و کردی دو صد بار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه آن سوگند و چه باد گذاری</p></div>
<div class="m2"><p>چه آن زنهار و چه ابر بهاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو آن کردی بدین مسکین دل من</p></div>
<div class="m2"><p>که هر گز نه کند دشمن به دشمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکایک آنچه کردی پیشت آیاد</p></div>
<div class="m2"><p>به جابی کت نیاید کس به فریاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو پنداری که بامن کردی این بد</p></div>
<div class="m2"><p>به جان من که کردی با تن خود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشانه شد روانست سرزنش را</p></div>
<div class="m2"><p>که بگزید از کنشها این کنش را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کجا این را به نکته بر شمارند</p></div>
<div class="m2"><p>پس از ما بر نگارستان نگارند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرا از دوستان دل بر گرفتی</p></div>
<div class="m2"><p>چرا از دشمنان دلبر گرفتی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا چون اژدها بر جان گزیدی</p></div>
<div class="m2"><p>چو در شهر کسان جانان گزیدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کجا یابی تو چون من دوستداری</p></div>
<div class="m2"><p>چو شاهنشاه موبد شهریاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به خوشی چون خراسان جایگاهی</p></div>
<div class="m2"><p>چو مرو شایگان محکم پناهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرامش کردی آن نیکی که دیدی</p></div>
<div class="m2"><p>ز من وز شه به هر کامی رسیدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز شاهی بود موبد را یکی نام</p></div>
<div class="m2"><p>ترا بود آن دگر گونه همه کام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بر گنجش همه فرمان مرا بود</p></div>
<div class="m2"><p>به گنج اندر همه چیزی ترا بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو بر خوردی ز گنج شاهوارش</p></div>
<div class="m2"><p>چنان کز ساز و رخت بی شمارش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ستوران جز گزیده نه نشستی</p></div>
<div class="m2"><p>کمرها جز گرانمایه نبستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نپوشیدی مگر دیبای صد رنگ</p></div>
<div class="m2"><p>ز چین آورده نیکو تر ز ارژنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نخوردی می جز از یاقوت رخشان</p></div>
<div class="m2"><p>چو مریخ از میان مهر تابان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بت رویان ستاره پیشکارت</p></div>
<div class="m2"><p>چو ویسه آفتاب اندر کنارت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنین حال و چنین مال و چنین جای</p></div>
<div class="m2"><p>دلاویز و دل افروز و دلارای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدل کردی مرا آخر چه بودت</p></div>
<div class="m2"><p>به جای این زیان چندست سودت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نکردی سود و مایه بر فشاندی</p></div>
<div class="m2"><p>نبردی هیج و بی مایه بماندی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قصا برداشت از پیش تو صد گنج</p></div>
<div class="m2"><p>کنون دانگی همی جوبی به صدرنج</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه نادانی که این مایه ندانی</p></div>
<div class="m2"><p>که از بسیار نیکی بر زیانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدل داری ز هر چیزی یکی چیز</p></div>
<div class="m2"><p>چنان کز زر بدل دارند ارزیز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به جای سیم ناب و زر خود روی</p></div>
<div class="m2"><p>بدل دادت زمانه آهن و روی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به جای ناز و مهرت رنج و کینه</p></div>
<div class="m2"><p>به جای در خوشاب آبگینه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به جای آب رویت آب جویست</p></div>
<div class="m2"><p>به جای مشک نابت خاک کویست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عجب دارم اگرتو هوشمندی</p></div>
<div class="m2"><p>چنین بد خویشتن را چون پسندی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گلی کاو با تو بسیاری نپاید</p></div>
<div class="m2"><p>بدین سان دل درو بستن چه باید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گلی به یا گلستانی شکفته</p></div>
<div class="m2"><p>گلش نیکوتر از ماه دو هفته</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو آذین سربسر پیغام بشنید</p></div>
<div class="m2"><p>همان گه باد پایی خنگ بگزید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به بلا و به پهنا کوه پیکر</p></div>
<div class="m2"><p>به رفتار و به پویه باد صرصر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به کوه اندر چو سیلاب رونده</p></div>
<div class="m2"><p>به دشت اندر چو عفریت دونده</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به بلا بر شدی همچون پلنگان</p></div>
<div class="m2"><p>به دریا در شدی مثل نهنگان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به پای او چه کهسار و چه هامون</p></div>
<div class="m2"><p>به چشم او چه دریا و چه جیحون</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به پشتش بر سوار آسوده در راه</p></div>
<div class="m2"><p>چنان بودی که مرد خفته برگاه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بیابان را چو نامه در نوشتی</p></div>
<div class="m2"><p>چو پرنده به گردون بر گذشتی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به راه اندر نه خوردش بود و نه خواب</p></div>
<div class="m2"><p>به دو هفته ز مرو آمد به گوراب</p></div></div>