---
title: >-
    بخش ۷۹ - پاسخ دادن رامین  ویس را
---
# بخش ۷۹ - پاسخ دادن رامین  ویس را

<div class="b" id="bn1"><div class="m1"><p>چو رامین دید بانو را دلازار</p></div>
<div class="m2"><p>ز لب بارنده زهر آلود گفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران گونه لابه کرد و پوزش</p></div>
<div class="m2"><p>ز جان پر نهیب از درد و سوزش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفت ای بهار مهربانان</p></div>
<div class="m2"><p>به چهره آفتاب دل ستانان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهشت دلبران اورنگ شاهان</p></div>
<div class="m2"><p>طراز نیکوان سلار ماهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستارهء بامداد و ماه روشن</p></div>
<div class="m2"><p>چراغ کشور و خورشید برزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل صد گنبد و آزاده سوسن</p></div>
<div class="m2"><p>خداوند من و کام دل من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا چندین به خون من شتابی</p></div>
<div class="m2"><p>چرا رویت همی از من بتابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم رامین ترا باجان برابر</p></div>
<div class="m2"><p>توی ویسه مرا از جان فزونتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم رامین ترا شایسته کهتر</p></div>
<div class="m2"><p>توئی ویسه مرا بایسته مهتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم رامین که شاه بی دلانم</p></div>
<div class="m2"><p>ز مهر تو به گیتی داستانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توی ویسه که ماه نیکوانی</p></div>
<div class="m2"><p>به چشم و زلف شاه جادوانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همانم من که تو دیدی همانم</p></div>
<div class="m2"><p>همان شایسته یار مهربانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همانم من که بودم تو نه آنی</p></div>
<div class="m2"><p>چرا بر من نمایی دل گرانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر کردی به گفت دشمنان گوش</p></div>
<div class="m2"><p>که زی تلخ شد آن مهر چون نوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر سوگندها به دروغ کردی</p></div>
<div class="m2"><p>مگر زنهار با جانم بخوردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگر یکدل شدی با دشمن من</p></div>
<div class="m2"><p>مگر آتش زدی در خرمن من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دریغ آن مهر و آن امیدواری</p></div>
<div class="m2"><p>که جانم را بد اندر مهر کاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بکشتم عشق در باغ جوانی</p></div>
<div class="m2"><p>به جان خویش کردم باغبانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی ورزید باغم با دل شاد</p></div>
<div class="m2"><p>چنان کز دیدگان آبش همی داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه یک شب خفت و نه یک روز آسود</p></div>
<div class="m2"><p>به رنج باغبانی در بفرسود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو آمد نوبهار ودل روشن</p></div>
<div class="m2"><p>بر آمد لاله و خیزی و سوسن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گل بود اندرو صد جای توده</p></div>
<div class="m2"><p>دمان بویش چو بوی مشک سوده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنار و بید او شد سایه گستر</p></div>
<div class="m2"><p>چنان چون مورد و سروش شاخ پرور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شکفته شد دگر گونه درختان</p></div>
<div class="m2"><p>ز خوبی همچو کام نیکبختان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به بانگ آمد درو قمری و بلبل</p></div>
<div class="m2"><p>دگر مرغان بر آوردند غلغل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وگا پیر امنش آهییخت دیوار</p></div>
<div class="m2"><p>نه دیواری که کوهی نام بردار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به پای کوه نوشین رودباری</p></div>
<div class="m2"><p>به گرد رود زرین مرغزاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز رامش بود کبگ کوهساری</p></div>
<div class="m2"><p>چنان کز رنگ شیر مرغزاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون آمد زمستان جدایی</p></div>
<div class="m2"><p>بدو در ابر و باد بی وفایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بدبختی در آمد سال و ماهی</p></div>
<div class="m2"><p>که ویران شد درو هر جایگاهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بی آبی در آمد روزگاری</p></div>
<div class="m2"><p>که در وی خشک شد هر رودباری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه آن دیوار ماندست و نه آن باغ</p></div>
<div class="m2"><p>نه آن کوه و نه آن رود و نه آن راغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بد اندیشان در ختانش بکندند</p></div>
<div class="m2"><p>در و دیوار او بر هم فگندند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رمیدند آن همه مرغانش اکنون</p></div>
<div class="m2"><p>چه کبگ از کوه و چه بلبل ز هامون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دریغا آن همه سرو و گل و بید</p></div>
<div class="m2"><p>دریغا روزگار رنج و اومید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه از زر بود مهر ما ز گل بود</p></div>
<div class="m2"><p>که چون بشکست بی بر گشت و بی سود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دل از دل دور گشت و یار از یار</p></div>
<div class="m2"><p>غم اندر غم فزود و کار در کار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به کام دل رسید از ما بد آموز</p></div>
<div class="m2"><p>که چون ماباد بد فرجام و بدروز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنون بدگوی ما از رنج ما روت</p></div>
<div class="m2"><p>بیاسوده به کام خویش بنشست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه پیغامبر بود اکنون نه همراز</p></div>
<div class="m2"><p>نه بدگوی و بدخواه و نه غماز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نه داید رنج بیند نه تو تیمار</p></div>
<div class="m2"><p>نه من درد دل و نه موبد آزار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بجز من در میان کس را گنه نیست</p></div>
<div class="m2"><p>که بخت کس چوبخت من سیه نیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به ناله زین سیه بخت نگونم</p></div>
<div class="m2"><p>که با او من همه جایی زبونم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرا گوهر چنان شد پوزش آرای</p></div>
<div class="m2"><p>که آزاده زبون باشد به هر جای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر نه خواستی بختم سیاهی</p></div>
<div class="m2"><p>مرا نفریفتی دیو تباهی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کسی کان دیو را باشد به فرمان</p></div>
<div class="m2"><p>به دل چون من بود کور و پشیمان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به جای عود خام و مشک سارا</p></div>
<div class="m2"><p>گرفته چوب بید و ریگ صحرا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به جای زر ناب و در شهوار</p></div>
<div class="m2"><p>به چنگ من سفال و سنگ کهسار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به جای باد رفتار اسپ تازی</p></div>
<div class="m2"><p>گرفته کم بها اسپ طرازی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نگارا نه همه پنداشتی کن</p></div>
<div class="m2"><p>زمانی دوستی و اشتی کن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر کردم جفا و زشت کاری</p></div>
<div class="m2"><p>تو با من کن وفا و مهر و یاری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گناه از بن ترا بود ای دلارام</p></div>
<div class="m2"><p>گرفتاری مرا آمد به فرجام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گناهی را که تو کردی یکی روز</p></div>
<div class="m2"><p>هزاران عذر خواهم از تو اموز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کنم پیش تو چندان لابهء زار</p></div>
<div class="m2"><p>که بزدایم ز جانت زنگ آزار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گناه از خویشتن بینم همیشه</p></div>
<div class="m2"><p>کنم تا مرگ با تو عذر پیسه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گهی گویم چو خواهم از تو زنهار</p></div>
<div class="m2"><p>گنهگارم گنهگارم گنهگار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گهی گویم چو خواهم از تو درمان</p></div>
<div class="m2"><p>پشیمانم پشیمانم پشیمان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خداوندی و بر من پادشایی</p></div>
<div class="m2"><p>توانی کم عقوبتها نمایی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>و لیکن پس کجا باشد کریمی</p></div>
<div class="m2"><p>خداوندی و رادی و رحیمی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر بخشایش از من باز گیری</p></div>
<div class="m2"><p>ز من زاری وپوزش نه پذیری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همین جا بند درگاه تو گیرم</p></div>
<div class="m2"><p>همی گریم به زاری تا بمیرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بع دیگر جای رفتن چون توانم</p></div>
<div class="m2"><p>که بخشاینده ای چون تو ندانم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مکن ماها و بر جانم ببخشای</p></div>
<div class="m2"><p>بلا زین بیش بر جانم میفزای</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چه بود ار من گنه کردم یکی بار</p></div>
<div class="m2"><p>نه جز من نیست در گیتی گنهگار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گناه آید ز گیهان دیده پیران</p></div>
<div class="m2"><p>خطا آید ز داننده دبیران</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دونده باره هم در سر در آید</p></div>
<div class="m2"><p>برنده ثیغ هم کندی نماید</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر آمد ناگهان از من خطایی</p></div>
<div class="m2"><p>مرا منمای داغ هر جفایی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>منم بنده توی زیبا خداوند</p></div>
<div class="m2"><p>ز بیزاری منه بر پای من بند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همه جوری توانم بردن از یار</p></div>
<div class="m2"><p>جز آن کز من شود یکباره بیزار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مرا کوری به از هجر تو دیدن</p></div>
<div class="m2"><p>مرا کرّی به از طعنت شنیدن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مرا هرگز مبادا از تو دوری</p></div>
<div class="m2"><p>ترا هرگز مباد از من صبوری</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نگارا تا تو بر من دل گرانی</p></div>
<div class="m2"><p>به چشم من سبک شد زندگانی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همیشه دج گران باشی به بیداد</p></div>
<div class="m2"><p>گران باشد همیشه سنگ و پولاد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نباشد مهرت اندر دل گه جنگ</p></div>
<div class="m2"><p>نباشد آب در پولاد و در سنگ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مرا خود از دلت آتش در افتاد</p></div>
<div class="m2"><p>که خود آتش فتد از سنگ و پولاد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بر آتش سوز گرد آید همه کس</p></div>
<div class="m2"><p>تو هم فریاد اتش سوز من رس</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر دریا برین آتش فشانی</p></div>
<div class="m2"><p>نیاید آتشم را زو زیانی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جهان پر دود گشت از دود جانم</p></div>
<div class="m2"><p>چو بختم شد به تاریکی جهانم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>جهان بر من همی گرید بدین سان</p></div>
<div class="m2"><p>ازیرا امشب این برفست و باران</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به آتشگاه می مانه درونم</p></div>
<div class="m2"><p>به کوه برف می ماند برونم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بدین گونه تنم را مهر کردست</p></div>
<div class="m2"><p>که نیمی سوخته نیمی فسردست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو من بر آسمان دیک فرشتست</p></div>
<div class="m2"><p>که ایزد ز آتش و برفش سرشتست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نشد برف من از آتش گدازان</p></div>
<div class="m2"><p>که دید آتش چنین با برف سازان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>کسی کاو را وفا با جان سرشتست</p></div>
<div class="m2"><p>به برف اندر بکشتن سخت زشتست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گمان بردم که از آتش رهانی</p></div>
<div class="m2"><p>ندانستم که در برفم نشانی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>منم مهمانت ای ماه دو هفته</p></div>
<div class="m2"><p>به دو هفته دو ماهه راه رفته</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به مهمانان همه خوبی پسندند</p></div>
<div class="m2"><p>نه زین سان در میان برف بندند</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>اگر شد کشتنم بر چشمت آسان</p></div>
<div class="m2"><p>به برف اندر مکش باری بدین سان</p></div></div>