---
title: >-
    بخش ۵۷ - رسیدن پیگ رامین به مروشاهجان و آگاه شدن ویس ازان
---
# بخش ۵۷ - رسیدن پیگ رامین به مروشاهجان و آگاه شدن ویس ازان

<div class="b" id="bn1"><div class="m1"><p>چو پیگ و نامهء رامین در آمد</p></div>
<div class="m2"><p>طرافی از دل ویسه بر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلش داد اندر آن ساعت گوایی</p></div>
<div class="m2"><p>که رامین کرد با او بی وفایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو موبد نامهء رامین بدو داد</p></div>
<div class="m2"><p>درخش حسرت اندر جانش افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سختی خونش اندر تن بجوشید</p></div>
<div class="m2"><p>ولیکن راز از مردم بپوشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لبش بود از برون چون لاله خندان</p></div>
<div class="m2"><p>شده دل زاندرون چون تفته سندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مینو بود خرم از برونش</p></div>
<div class="m2"><p>چو دوزخ بود تفسیده درونش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خنده می نهفت از دلش تنگی</p></div>
<div class="m2"><p>به رهواری همی پوشید لنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخش از نامه خوندن شد زریری</p></div>
<div class="m2"><p>که خود دانست کم مایه دبیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت از خدا این خواستم من</p></div>
<div class="m2"><p>که روزی گم کند بازار دشمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر شاهم دگر زشتی نگوید</p></div>
<div class="m2"><p>بهانه هر زمان بر من نجوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدین شادی به درویشان دهم چیز</p></div>
<div class="m2"><p>بسی گوهر به آتشگه برم نیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم او از غم برست اکنون و هم من</p></div>
<div class="m2"><p>بیفتاد از میان بازار دشمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون اندر لهانم هیچ غم نیست</p></div>
<div class="m2"><p>که جانم راز بیم تو ستم نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من اندر کام و ناز و بخت پیروز</p></div>
<div class="m2"><p>نه خوش خوردم نه خوش خفتم یکی روز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون دلشاد باشم در جوانی</p></div>
<div class="m2"><p>به آسانی گذارم زندگانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا گر مه بشد ماندست خورشید</p></div>
<div class="m2"><p>همه کس را به خورشیدست امید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا از تو شود روشن جهان بین</p></div>
<div class="m2"><p>چه باشد گر نبینم روی رامین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی گفت این سخن دل با زبان نه</p></div>
<div class="m2"><p>سخن را آشکارا چون نهان نه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بیرون رفت شاه او را تب آمد</p></div>
<div class="m2"><p>ز تاب مهر جانش بر لب آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلش در بر تپان شد چون کبوتر</p></div>
<div class="m2"><p>که در چنگال شاهین باشدش سر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چکان گشته ز اندامش خوی سرد</p></div>
<div class="m2"><p>چو شمنم کاو نشیند بر گل زرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سهی سروش چو بید از باد لرزان</p></div>
<div class="m2"><p>ز نرگس بر سمن یاقوت ریزان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به زرین یاره سیمین سینه کوبان</p></div>
<div class="m2"><p>به مشکین زلف خاک خانه روبان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی غلتید در خاک و همی گفت</p></div>
<div class="m2"><p>چه تیرست این که آمد چشم من سفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه بختست این که روزم را سیه کرد</p></div>
<div class="m2"><p>چه روزست این که جانم راتبه کرس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیا ای دایه این غم بین که ناگاه</p></div>
<div class="m2"><p>بیامد مثل طوفان از کمینگاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز تخت زر مرا در خاک افگند</p></div>
<div class="m2"><p>خسک در راه صبر من پراگند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو خود داری خبر یا من بگویم</p></div>
<div class="m2"><p>که از رامین چه رنج آمد به رویم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بشد رامین و در گوراب زن کرد</p></div>
<div class="m2"><p>پس آنگه مژدگان نامه به من کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که من گل کشتم و گل پروریدم</p></div>
<div class="m2"><p>ز مرود و سوسن و خیری بریدم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به مرو اندر مرا اکنون چه گویند</p></div>
<div class="m2"><p>سزد ار مرد و زن بر من بمویند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی درمان بجو از بهر جانم</p></div>
<div class="m2"><p>که من زین درد جان را چون رهانم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا چون این خبر بشنید بایست</p></div>
<div class="m2"><p>گرم مرگ آمدی زین پیش شایست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرا اکنون نه زر باید نه گوهر</p></div>
<div class="m2"><p>نه جان باید نه مادر نه برادر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا کام جهان با رام خوش بود</p></div>
<div class="m2"><p>کنون چون رام رفت از کام چه سود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا او جان شیرین بود و بی جان</p></div>
<div class="m2"><p>نیابد هیچ شادی تن ز گیهان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>روم از هر گناهی تن بشویم</p></div>
<div class="m2"><p>وز ایزد خویشتن را چاره جویم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به درویشان دهم چیزی که دارم</p></div>
<div class="m2"><p>مگر گاه دعا باشد یارم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به لابه خواهم از دادار گیهان</p></div>
<div class="m2"><p>که رامین گردد از کرده پشیمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به تاری شب به مرو آید ز گوراب</p></div>
<div class="m2"><p>ز باران ترّ بفسرده برو آب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تنش همچون تن من سست و لرزان</p></div>
<div class="m2"><p>دلش همچون دل من زار و سوزان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گه از سرمای سخت و گه تیمار</p></div>
<div class="m2"><p>همی خواهد ز ویس و دایه زنهار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز ما بیند همین بد مهری آن روز</p></div>
<div class="m2"><p>که از وی ما همی بینیم امروز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خدایا داد من بستانی از رام</p></div>
<div class="m2"><p>کنی او را چو من بی صبر و آرام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جوابش داد دایه گفت چندین</p></div>
<div class="m2"><p>مبر اندوه کت بردن نه آیین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مخور اندوه و بزادی از دلت زنگ</p></div>
<div class="m2"><p>به خرسندی و خاموشی و فرهنگ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تن آزاده را چندین مرنجان</p></div>
<div class="m2"><p>دل آسوده را چندین مپیچان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مکن بیداد بر جان و جوانی</p></div>
<div class="m2"><p>که جان را مرگ به زین زندگانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز بس کاین روی گلگون را زنی تو</p></div>
<div class="m2"><p>ز بس کاین موی مشکین را کنی تو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رجی نیکوتر از باغ بهشتی</p></div>
<div class="m2"><p>چو روی اهرمن کردی به زشتی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جهان چندان که داری بیش باید</p></div>
<div class="m2"><p>ولیک از بهر جان خویش باید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر آن گاهی که نبود جان شیرین</p></div>
<div class="m2"><p>مه دایه باد و مه شاه و مه رامین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو بسپردم من اندر تشنگی جان</p></div>
<div class="m2"><p>مبادا در جهان یک قطره باران</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر آن گاهی که گیتی گشت بی من</p></div>
<div class="m2"><p>مرا چه دوست در گیتی چه دشمن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه مردان به زن دیدن دلیرند</p></div>
<div class="m2"><p>به مهر اندر چو رامین زود سیرند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر از تو سیر شد رامین بد مهر</p></div>
<div class="m2"><p>که رویت را همی سجده برد مهر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز مهر گل همیدون سیر گردد</p></div>
<div class="m2"><p>همین مومین زبان شمشیر گردد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر بیند هزاران ماه و اختر</p></div>
<div class="m2"><p>نیاید زان همه نور یکی خور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گل گورابی ار چه ماهرویست</p></div>
<div class="m2"><p>به خواری پیش تو چون خاک کویست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نکوتر زیر پای تو ز رویش</p></div>
<div class="m2"><p>چو خوشتر خاک پای تو ز بویش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو رامین از تو تنها ماند و مهجور</p></div>
<div class="m2"><p>اگر زن کردی زی من بود معذور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کسی کز بادهءخوش دور باشد</p></div>
<div class="m2"><p>اگر دردی خورد معذور باشد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سمن بر ویس گفت ای دایه دانی</p></div>
<div class="m2"><p>که گم کردم به صبر اندر جوانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زنان را شوهرست و یار برسر</p></div>
<div class="m2"><p>مرا اکنون نه یارست و نه شوهر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اگر شویست بر من بدگمانست</p></div>
<div class="m2"><p>وگر یارست با من بدنهانست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ببردم خویشتن را آب و سایه</p></div>
<div class="m2"><p>چو گم کردم ز بهر سود مایه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بیفگندم درم از بهر دینار</p></div>
<div class="m2"><p>کنون بی هردوان ماندم به تیمار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مده دایه به خرسندی مرا پند</p></div>
<div class="m2"><p>که بر آتش نخسپد هیچ خرسند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مرا بالین و بستر آتشین است</p></div>
<div class="m2"><p>بر آتش دیو عشقم همنشین است</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بر آتش صبر کردن چون توانم</p></div>
<div class="m2"><p>اگر سنگین و رویینست جانم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مرا زین بیش خرسندی مفرمای</p></div>
<div class="m2"><p>به من بر باد بیهوده مپیمای</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مرا درمان نداند هیچ دانا</p></div>
<div class="m2"><p>مرا چاره نداند هیچ کانا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرا صد تیر زهر آلوده تا پر</p></div>
<div class="m2"><p>نشاند این پیگ واین نامه به دل بر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چه گویی دایه زین پیگ روان گیر</p></div>
<div class="m2"><p>که نه گه بر دلم زد ناوک تیر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز رام آورد مشک آلود نامه</p></div>
<div class="m2"><p>برد از ویس خون آلود جامه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بگریم زار برنالان دل خویش</p></div>
<div class="m2"><p>ببارم خون دیده بر دل ریش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>الا ای عاشقان مهرپرور</p></div>
<div class="m2"><p>منم بر عاشقان امروز مهتر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شما را من ز روی مهربانی</p></div>
<div class="m2"><p>نصیحت کرد خواهم رایگانی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نصیحت دوستان از من پذیرید</p></div>
<div class="m2"><p>دهم پند شما گر پند گیرید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مرا بینیدحال من نیوشید</p></div>
<div class="m2"><p>دگر در عشق ورزیدن مکوشید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مرا بینید و خود هشیار باشید</p></div>
<div class="m2"><p>ز مهر ناکسان بیزار باشید</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نهال عاشقای در دل مکارید</p></div>
<div class="m2"><p>و گر کارید جان او را سپارید</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اگر چونانکه حال من ندانید</p></div>
<div class="m2"><p>به خون بر رخ نوشتستم بخوانید</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مرا عشق آتشی در دل برافروخت</p></div>
<div class="m2"><p>که هر چند بیش کشتم بیشتر سوخت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>جهان کردم ز آب دیده پر گل</p></div>
<div class="m2"><p>نمود از آب چشمم آتش دل</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چه چشمست این که خوابش نگیرد</p></div>
<div class="m2"><p>چه آبست این کزو آتش نمیرد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>مرا پروردن باشه بدی آز</p></div>
<div class="m2"><p>بپروردم یکی باشه به صد ناز</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به روزش داشتم بر دست سیمین</p></div>
<div class="m2"><p>شبش هرگز نبستم جز به بالین</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو پر مادر آوردش بیفگند</p></div>
<div class="m2"><p>دگر پرها بر آورد و پر آگند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بدانست او ز دست من پریدن</p></div>
<div class="m2"><p>به خود کامی سوی کبگان دویدن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>گمان بردم که او گیرد شکاری</p></div>
<div class="m2"><p>مرا باشد همیشه غمگساری</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>یکی ناگه ز دست من رها شد</p></div>
<div class="m2"><p>به ابر اندر ز چشم من جدا شد</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کنون خسته شدم از بس که پویم</p></div>
<div class="m2"><p>نشان باشهء گم کرده جویم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دریغا رفته رنج و روزگارم</p></div>
<div class="m2"><p>دریغا این دل امیدوارم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دریغا رنج بسیارا که بردم</p></div>
<div class="m2"><p>که خود روزی ز رنجم بر نخوردم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بگردم در جهان چون کاروانی</p></div>
<div class="m2"><p>که تا یابم ز گمگشته نشانی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>مرا هم دل بشد هم دوست از بر</p></div>
<div class="m2"><p>نباشم بی دل و بی دوست ایدر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>کنم بر کوهساران سنگ بالین</p></div>
<div class="m2"><p>ز جور آن دل چون کوه سنگین</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>دل از من رفت اگر یابم نشانش</p></div>
<div class="m2"><p>دهم این خسته جان را مژدگانش</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>مرا تا جان چنین پر دود باشد</p></div>
<div class="m2"><p>دلم از بخت چون خشنود باشد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>منم کم دوست ناخشنود گشتست</p></div>
<div class="m2"><p>منم کم بخت خشم آلود گشتست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مرا بی کارد ای دایه تو کشتی</p></div>
<div class="m2"><p>که تخم عشق در جانم بکشتی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>درین راهم تو بودی کور رهبر</p></div>
<div class="m2"><p>چو در چاهم فگندی تو بر آور</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>مرا چون از تو آمد درد شاید</p></div>
<div class="m2"><p>که درمانم کنون هم از تو آید</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بسیچ راه کن بر خیز و منشین</p></div>
<div class="m2"><p>ببر پیغام من یک یک به رامین</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بگو ای بدگمان بی وفا زه</p></div>
<div class="m2"><p>تو کردی بر کمان ناکسی زه</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تو چشم راستی را کور کردی</p></div>
<div class="m2"><p>تو بخت مردی را شور کردی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>تو از گوهر چو گزدم جان گزایی</p></div>
<div class="m2"><p>به سنگ ار بگذری گوهر نمایی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>تو ماری از تو ناید جز گزیدن</p></div>
<div class="m2"><p>تو گرگی از تو ناید جز دریدن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز طبع تو همین آید که کردی</p></div>
<div class="m2"><p>که با زنهاریان زنهار خوردی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>اگر چه من ز کارت دل فگارم</p></div>
<div class="m2"><p>بدین آهوت ارزانی ندارم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>مکن بد با کسی و بد میندیش</p></div>
<div class="m2"><p>کجا چون بد کنی آید بدت پیش</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>اگر یکسر بشد مهرم ز یادت</p></div>
<div class="m2"><p>میان مهربانان شرم بادت</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بدین زشتی که از پیشم برفتی</p></div>
<div class="m2"><p>فرامش کردی آن خوبی که گفتی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو برگ لاله بودت خوب گفتار</p></div>
<div class="m2"><p>به زیر لاله در خفته سیه مار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>اگر تو یار نو کردی روا باد</p></div>
<div class="m2"><p>ز گیتی آنچه می خواهی ترا باد</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>مکن چندین به نومیدی مرا بیم</p></div>
<div class="m2"><p>آ هر کاو زو بیابد بفگند سیم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>اگر تو جوی نو کندی به گوراب</p></div>
<div class="m2"><p>نباید بستن از جوی کهن آب</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>وگر تو خانه کردی در کهستان</p></div>
<div class="m2"><p>کهن خانه مکن در مرو ویران</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>به باغ ار گل بکشتی فرخت باد</p></div>
<div class="m2"><p>ز مرزش بر مکن آزاده شمشاد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>زن نو با دلارام کهن دار</p></div>
<div class="m2"><p>که هر تخمی ترا کامی دهد بار</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>همی گفت این سخنها ویس بتروی</p></div>
<div class="m2"><p>زهر چشمی روان بر هر رخی جوی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>تو گفتی چشم بود ابر نوروز</p></div>
<div class="m2"><p>همی بارید بر راغ دل افروز</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>دل دایه بر آن بت روی سوزان</p></div>
<div class="m2"><p>همی گفت ای بهار دلفروزان</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>مرا بر آتش سوزنده منشان</p></div>
<div class="m2"><p>گلاب از دیده بر گلنار مفشان</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>که اکنون من بگیرم ره به گوراب</p></div>
<div class="m2"><p>بوم در راه چون ره بی خور و خواب</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>کنم با رام هر چاری که دانم</p></div>
<div class="m2"><p>مگر جان ترا زین غم رهانم</p></div></div>