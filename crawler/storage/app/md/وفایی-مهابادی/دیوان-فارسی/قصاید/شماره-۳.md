---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>نمی دانم چرا ای دیده چندین خون فشان هستی؟</p></div>
<div class="m2"><p>همانا داغدار هجر یار مهربان هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ای ابر بهاری از چه گریان و خروشانی</p></div>
<div class="m2"><p>مگر در آرزوی وصل باغ و بوستان هستی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ای باد سحرگاهی مگر جویای گلزاری</p></div>
<div class="m2"><p>که در کوه و بیابان ها به هر سویی دوان هستی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ای قمری که می نالی به طرف جوی باغ و راغ</p></div>
<div class="m2"><p>چنان دانم پی سروی، چو من کوکوزنان هستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ای نرگس مگر در خواب دیدی چشم دلدارم</p></div>
<div class="m2"><p>که چون من عاشق و بیمار و مست و ناتوان هستی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو ای سنبل مگر بویی ز زلف یار بگرفتی</p></div>
<div class="m2"><p>که چون من بی قرار و درهم و آشفته جان هستی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ای گل وصف یار من مگر از باد بشنیدی</p></div>
<div class="m2"><p>که چون من پاره دل خونین درون و خون فشان هستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو ای مسکین بنفشه از کجا دیدی خط و خالش</p></div>
<div class="m2"><p>که محزون هم چو من در کسوت ماتم نهان هستی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو ای سوسن مگر عاشق شدی چون من به روی یار</p></div>
<div class="m2"><p>که در شرح غم هجران جانان صد زبان هستی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو خود ای لاله زلف و روی جانان از کجا دیدی</p></div>
<div class="m2"><p>که چون من داغدار افتاده اندر بوستان هستی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو ای آتش به جان افتاده بلبل از برای گل</p></div>
<div class="m2"><p>چون من تا کی به عشق اندر زبان ها داستان هستی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو ای مرغ شباویز چو من در زلف جانانه</p></div>
<div class="m2"><p>بگو تا کی به یاد صبح آن گردن چنان هستی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو ای بلبل که در توصیف گل خوش نغمه ای گریان</p></div>
<div class="m2"><p>مرید خاندان حضرت قطب زمان هستی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وفایی از پی گلزار می نالی عجب نبود</p></div>
<div class="m2"><p>که خوشخوان بلبل روی گل آن گلستان هستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو کز تاج سلاطین عار داری هم چنین دانم</p></div>
<div class="m2"><p>غلام درگه پیران کیوان آستان هستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو کز اورنگ شاهی ننگ داری هیچ شک نبود</p></div>
<div class="m2"><p>سگ عالی جناب آستان راستان هستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امام راستان قطب خداجویان عبیدالله</p></div>
<div class="m2"><p>تویی کایینه ی نور خدای لا مکان هستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فروغ ظلمت دل ها تویی ای سید و سرور</p></div>
<div class="m2"><p>که نسل آل طه را چراغ خاندان هستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به اعجاز هدا بخشی پیمبر نیستی لکن</p></div>
<div class="m2"><p>به آیات پیمبر! مرشد آخر زمان هستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مسیحا نیستی، لیکن به انفاس مسیحایی</p></div>
<div class="m2"><p>روان بخش هزاران هم چو من دل مردگان هستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کلیم الله نه ای، لیکن پی فرعون نفس ما</p></div>
<div class="m2"><p>به طور همت پاک از ید بیضا بیان هستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو خاک انبیایی وین عجب کاندر شهود حق</p></div>
<div class="m2"><p>به طور نیستی بی «لن ترانی» دیده بان هستی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به صورت بنده ای مطلق، به معنی با خدا ملحق</p></div>
<div class="m2"><p>تو ای مرآت نور حق چه پیدا نهان هستی؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهی چون پیر بسطامی ز چشم کاروان دوری</p></div>
<div class="m2"><p>گهی چون غوث خرقانی دلیل کاروان هستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مقیم شرع پیغمبر تویی در صورت و معنی</p></div>
<div class="m2"><p>غیاث ملتی و رهبر اسلامیان هستی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر اقلیم رشادت خواجگی الحق ترا زیبد</p></div>
<div class="m2"><p>که بر تخت نیابت افتخار خواجگان هستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز تأثیر حرور نفس بدفر، ما چه غم داریم</p></div>
<div class="m2"><p>تو چون ابر کرم بر فرق ملت سایه بان هستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مریدان ترا دیدم به چشم خویش انس و جان</p></div>
<div class="m2"><p>خطا نبود اگر گویم امام انس و جان هستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هزاران پیر دیدم نوجوان از لطف انفاست</p></div>
<div class="m2"><p>روا باشد که گویم مرشد پیر و جوان هستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زبان سگ اگر تر شد زیان بحر کی گردد</p></div>
<div class="m2"><p>چه غم با این کمال از در دهان منکران هستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه باک از طعن بدخواهان تو را بدخواه پندارند</p></div>
<div class="m2"><p>بگو حق باش و جان می ده تو خورشید جهان هستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مقامات ترا اهل بصیرت سخت دریابد</p></div>
<div class="m2"><p>که با این خواجگی دایم غلام بندگان هستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شوم قربان آن مژگان..... خدنگ بر ابرو</p></div>
<div class="m2"><p>پی صید دل و جان ها عجب تیر و کمان هستی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به دیدار تو من هرگز نخواهم سیر شدن زان رو</p></div>
<div class="m2"><p>که با این رو فرات عالم مستسقیان هستی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر بی تو نبیند مردم چشمم جهان، شاید</p></div>
<div class="m2"><p>که بی این مردمی چشم و چراغ مردمان هستی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر دور از تو من بی جان و بی دل مانده ام باید</p></div>
<div class="m2"><p>که با روی جهان آرا تو جان بی دلان هستی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر از درد نهانی در تمنایت همی سوزم</p></div>
<div class="m2"><p>چه سازم چون نسوزم مرهم درد نهان هستی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز هجرت گر نیاراید روان من عجب نبود</p></div>
<div class="m2"><p>که با این طلعت زیبا تو آرام روان هستی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو نیلوفر اگر من غرق دریای سرشک استم</p></div>
<div class="m2"><p>چه سازم چون کنم آخر تو مهر شعشعان هستی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روا باشد اگر بر حال زار من ببخشایی</p></div>
<div class="m2"><p>که من مردی گدا هستم تو مردی مرزبان هستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جز این عیبی نداری در مقامات کمالاتت</p></div>
<div class="m2"><p>که با جان وفایی اندکی نامهربان هستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>«وفایی» چون تواند گفت توصیف کمالاتت</p></div>
<div class="m2"><p>درین آینه چون گنجی؟ که تو مرد کلان هستی</p></div></div>