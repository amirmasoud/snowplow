---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>کرم خواندی، ستم راندی، وفا گفتی، جفا کردی</p></div>
<div class="m2"><p>تو ای ماه سمن سیما ببین با ما چه ها کردی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روم از سوز دل آتش زنم در هر نیستانی</p></div>
<div class="m2"><p>به بانگ نی بگویم آن چه با این بی نوا کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وفایی! داستان گریه، من با کس نمی گفتم</p></div>
<div class="m2"><p>قلم بگرفتی از مژگان تو شرح ماجرا کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وفایی! از زبانت مشک چین، عطر ختن ریزد</p></div>
<div class="m2"><p>مگر با خاک کوی قطب عالم آشنا کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وفایی! چشم بینایت رنگ نور طور می پاشد</p></div>
<div class="m2"><p>مگر از خاک پای غوث عالم سرمه سا کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چراغ آل آدم غوث اعظم، ای عبیدالله</p></div>
<div class="m2"><p>تویی کز یک نظر قلب جهان را کیمیا کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دور آخر از جام حقیقت نشئه ای دادی</p></div>
<div class="m2"><p>که محفل ار سراسر مست نور کبریا کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقاب از روی بگشودی جمال خویش بنمودی</p></div>
<div class="m2"><p>جهان را شش جهت از نور خود «بدرالدجی» کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نسیمی از بهار فیض [فیاضت چو افشاندی]</p></div>
<div class="m2"><p>زمین را غنچه گل دادی، زمان را مشک زا کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دل های مریدان هر کجا عکس رخت افتاد</p></div>
<div class="m2"><p>که شرح معنی «بدرالدجی، شمس الضحی» کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین آخر به جز نام بقا یکباره فانی بود</p></div>
<div class="m2"><p>دری بگشادی و جان فنا را پر بقا کردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر دست دل دیوانه را لطف تو نگرفتی</p></div>
<div class="m2"><p>کجا دل روی در خلوت سرای دلربا کردی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هر کس یک نظر کز روی لطف قهر افکندی</p></div>
<div class="m2"><p>گدارا پادشاه و شاه را مسکین گدا کردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ترکستان چرا گویم لبت آب بقا بخشید</p></div>
<div class="m2"><p>که ترکستان معنی را ز لب عین بقا کردی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سگم خواندی و خوشنودم، بدم گفتی و افزودم</p></div>
<div class="m2"><p>جزاک الله کرم گفتی، عفاک الله عطا کردی</p></div></div>