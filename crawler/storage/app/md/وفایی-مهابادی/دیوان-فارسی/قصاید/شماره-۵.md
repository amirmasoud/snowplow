---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ندانم با وفاداران جفا کاری چرا کردی؟</p></div>
<div class="m2"><p>چه نیکی از جفا دیدی که بر جای وفا کردی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر ابروت بنمودت ره و رسم کمان بازی</p></div>
<div class="m2"><p>مگر آن غمزه فرمودت که خون ها بی بها کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که گفتت عندلیبی را ز باغ گل برون فرما؟</p></div>
<div class="m2"><p>که فرمودت غرابی را در ایوان هما کردی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حرف دشمنان فتوای خون ما چرا دادی</p></div>
<div class="m2"><p>به قول مدعی قصد دل ما را چرا کردی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگ کوی تو بودم پای دل بستی بر آن گیسو</p></div>
<div class="m2"><p>به جان صید حرم بودم گرفتار بلا کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در عشق زنخدان تو لنگر بست جان من</p></div>
<div class="m2"><p>چه بنویسم جفاهایی کز آن زلف دو تا کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وفا این بود کاندر کشتی [مواج زلفانت]</p></div>
<div class="m2"><p>درین گردابه شیدا مشربی را ناخدا کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دام زلف و تیرش زد به تیغ غمزه صید دل</p></div>
<div class="m2"><p>گرفتی خستی و بستی و جستی پس رها کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی شد دل گرفتار تو، داد از دست مژگانت</p></div>
<div class="m2"><p>در آن محراب ابرو هر چه کردی از دعا کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تاب آفتاب چشم من سیماب خیز آمد</p></div>
<div class="m2"><p>مگر چشم مرا تعلیم علم سیمیا کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به مژگانم خستی به چشمانم دوا گفتی</p></div>
<div class="m2"><p>نه زخمم را به هم بستی، نه دردم را دوا کردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکستی قلب ما گفتی، درستت می دهم دیدم</p></div>
<div class="m2"><p>نه حکم مومیا دادی، نه کاری کیمیا کردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به قامت جلوه دادی ریختی خون «وفایی» را</p></div>
<div class="m2"><p>پی یک قطره خون بنگر قیامت را رها کردی</p></div></div>