---
title: >-
    مثنوی پیک صبا
---
# مثنوی پیک صبا

<div class="b" id="bn1"><div class="m1"><p>زد صفیری مرغ جان بالای عرش</p></div>
<div class="m2"><p>کرد مشکین از نفس سیمای عرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواند بر دل یک ورق آیات عشق</p></div>
<div class="m2"><p>گشت دل سرمست تسلیمات عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلبر ترسا خمار و می زده</p></div>
<div class="m2"><p>شد گلابی زد به راه میکده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشوه ای زد نرگس مستانه را</p></div>
<div class="m2"><p>حلقه زد ناگه در میخانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی آمد آب می بر مست زد</p></div>
<div class="m2"><p>مطرب آمد بر رگ دل دست زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازم از می ساقی روحانیان</p></div>
<div class="m2"><p>[برد ما را تا] در پیر مغان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازم از نی مطرب دیوان عشق</p></div>
<div class="m2"><p>دست دل بگرفت تا ایوان عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز شد شیرین به شکر خنده زن</p></div>
<div class="m2"><p>تازه شد داغ درون کوه کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موسی جان باز شد بر طور عشق</p></div>
<div class="m2"><p>جامه ی جان پاره شد از نور عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داد لیلی سنبل مشکین به باد</p></div>
<div class="m2"><p>باز مجنون در بیابان سر نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز قمری سر به سر دستان کشید</p></div>
<div class="m2"><p>وز نفیر عشق، نای خود درید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز شد گل بانگ بلبل در چمن</p></div>
<div class="m2"><p>تازه شد عشق گلش در جان و تن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز شد جان «وفایی» نغمه گو</p></div>
<div class="m2"><p>در فراق دلبر گل رنگ و بو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان نثار قبله ابروی عشق</p></div>
<div class="m2"><p>بنده ی زنار زلف ز روی عشق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حلقه در گوش در پیر مغان</p></div>
<div class="m2"><p>زند خوان مذهب آه و فغان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قمری آشفته بر بالای سرو</p></div>
<div class="m2"><p>سرخوش و مدهوش رفتار تذرو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نغمه پرداز نگار جنگ ساز</p></div>
<div class="m2"><p>مست صهبای بت عاشق نواز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داغدار چین زلف دوستان</p></div>
<div class="m2"><p>طوطی هجر آمد از هندوستان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مطرب خوش نغمه ی دیوان گل</p></div>
<div class="m2"><p>بلبل خونین دل خوش خوان گل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عاشق شیدا «وفایی» دم به دم</p></div>
<div class="m2"><p>خوش نوایی داد اندر صبحدم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کای نسیم منزل جانان من</p></div>
<div class="m2"><p>مرحبا ای غمگسار جان من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای انیس گلشن و بستان دوست</p></div>
<div class="m2"><p>مرحبا ای پیک مشتاقان دوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای صبا دست من و دامان تو</p></div>
<div class="m2"><p>جان من بادا فدای جان تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حوریانه می خرامی هر زمان</p></div>
<div class="m2"><p>بر صف نسرین و گل دامن کشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>توده توده عنبر افشان بینمت</p></div>
<div class="m2"><p>دسته دسته گل به دامان بینمت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یا به گلزار خطا رو کرده ای</p></div>
<div class="m2"><p>نافه ی مشک غزال آورده ای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یا گلستان را شبیخون کرده ای</p></div>
<div class="m2"><p>غارت عطر گلستان کرده ای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یا به بازی باغبان خلد بود</p></div>
<div class="m2"><p>تاری از گیسوی حورانش گشود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یا گذاری کرده ای در کوی یار</p></div>
<div class="m2"><p>کاین چنین عنبر فشانی باربار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زنده کردی جان مشتاقان دوست</p></div>
<div class="m2"><p>گر نه انفاس مسیحا، این چه بود؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون تویی در بزم جانان دادرس</p></div>
<div class="m2"><p>عاشقان را [اندکی هم] داد رس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روی کن در کوچه ی آن دلستان</p></div>
<div class="m2"><p>از من دیوانه پیغامی رسان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بوسه ای زن بر در و دیوار یار</p></div>
<div class="m2"><p>عطر سازی کن تو در گلزار یار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زینهار آهسته شو اندر حصار</p></div>
<div class="m2"><p>تا نگیرد زلف جانانم غبار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کاندرین بیت حرم صیدم به ناز</p></div>
<div class="m2"><p>می کند بازیچه با زلف دراز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دلبری بینی به حسن آراسته</p></div>
<div class="m2"><p>ابروانش چون مه نو کاسته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خال دارد هندوانه مشک ناب</p></div>
<div class="m2"><p>زلف دارد زنگیانه پر زتاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مهر و مه آیینه دار روی او</p></div>
<div class="m2"><p>مشتری پا بسته در گیسوی او</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یک طبق گل بسته بر سرو روان</p></div>
<div class="m2"><p>یک قدح می کرده اندر ناروان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>طره ای مشکین برو آویخته</p></div>
<div class="m2"><p>سنبل و نسرین به هم آمیخته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر دو زلفش سایبان مهر و ماه</p></div>
<div class="m2"><p>آهوان را سایه ی زلفش پناه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حلقه در گوش لبش لعل یمان</p></div>
<div class="m2"><p>بنده ی زلف سیاهش ضیمران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>غمزه اش از ابروان خون ریزتر</p></div>
<div class="m2"><p>نشتر مژگان ز خنجر تیزتر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر رگ دل غمزه ی او نیشتر</p></div>
<div class="m2"><p>خستگان را خنده ی او گل شکر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هشته در زلف سیه سحر حلال</p></div>
<div class="m2"><p>کرده در تنگ شکر آب زلال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اشک زارش آب حیوان پرورد</p></div>
<div class="m2"><p>آب حیوانش دل و جان پرورد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>[عود خالش بر] رخ افروخته</p></div>
<div class="m2"><p>عود تر باشد بر آتش سوخته</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ظلمت است و آه از تاب و تبش</p></div>
<div class="m2"><p>هم چو گرداب سکندر غبغبش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چون خضر وان خال هندو نسبش</p></div>
<div class="m2"><p>می نماید آب حیوان در لبش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آفت ایمان به زلف عنبرین</p></div>
<div class="m2"><p>غارت جان از دو لعل شکرین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>صورتش طعنه به شکر می زند</p></div>
<div class="m2"><p>آتش اندر جان آذر می زند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چشم بند نرگسش انگیخته</p></div>
<div class="m2"><p>خون عاشق را به مژگان ریخته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قد نگویم قامتی چون نخل طور</p></div>
<div class="m2"><p>رخ نخوانم عارضی صد لمعه نور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سرکش و سرمست و تند و شوخ و شنگ</p></div>
<div class="m2"><p>پنجه از خون عزیزان لاله رنگ</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بر سر حوض است چون یک شعله نور</p></div>
<div class="m2"><p>بر لب کوثر خرامان هم چو حور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>لب گل و بالا گل و گل رنگ و بو است</p></div>
<div class="m2"><p>گر نمی دانی «گلندام» من او است</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>قبله ی جان «وفایی» روی او</p></div>
<div class="m2"><p>کعبه ی دل از دو عالم کوی او</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شانه کن زلف سیاهش با ادب</p></div>
<div class="m2"><p>دور دار از خاک راهش با ادب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سجده ای بر بر قد و بالای او</p></div>
<div class="m2"><p>عرضه ای کن از من شیدای او</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کای شکر لب، دلبر عیار من</p></div>
<div class="m2"><p>ای ستم گر، تند خو، دلدار من</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کای بدین بالا، بلای مرد و زن</p></div>
<div class="m2"><p>کای دو چشمت مایه ی صد مکر و فن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کای بدین حسن ملک رشک پری</p></div>
<div class="m2"><p>دلبر بی باک و یار سرسری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>من هم از خیل شهیدان توام</p></div>
<div class="m2"><p>قمری سرو خرامان توام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>با دو نرگس تا ز دستم برده ای</p></div>
<div class="m2"><p>باده ی ناخورده مستم کرده ای</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>داغدار خال هندوی تو کرد</p></div>
<div class="m2"><p>بی قرار زلف و ابروی تو کرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تا نهادم در پی زلف تو سر</p></div>
<div class="m2"><p>نیست جز سودا مرا کاری دگر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گلبن شیرین به جان پروردمش</p></div>
<div class="m2"><p>هم چو بلبل جان به قربان کردمش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تند بادی ناگهان سحری نمود</p></div>
<div class="m2"><p>آن گل نورسته از دستم ربود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>عمر چندین ساله را دادم به باد</p></div>
<div class="m2"><p>تا غزالی مست در دامم فتاد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>قبله کردم دلگشا ابروی او</p></div>
<div class="m2"><p>از دل و جان بنده ی هندوی او</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آن چنان مست نگاهم کرده بود</p></div>
<div class="m2"><p>تا شدم هشیار شیرش برده بود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>غمگسار خویش سروی داشتم</p></div>
<div class="m2"><p>در خرامیدن تذرویی داشتم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کردمش پرورده از خون جگر</p></div>
<div class="m2"><p>آبیاری کردم از اشک بصر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از من دلداده چون وحشی رمید</p></div>
<div class="m2"><p>عاقبت از بی وفایی سر کشید</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بلبل روی تو بودم روز و شب</p></div>
<div class="m2"><p>رو به رو سینه به سینه لب به لب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>قبله گاهم طاق ابروی تو بود</p></div>
<div class="m2"><p>بوسه گاهم چشم جادوی تو بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>دست در گردن به هم بازی کنان</p></div>
<div class="m2"><p>بوسه می کردم ازان لعل لبان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چشم بینای مرا بردند نور</p></div>
<div class="m2"><p>تا به یکبار از منت کردند دور</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چشم زخمی ناگه از ما کار کرد</p></div>
<div class="m2"><p>آفتاب بخت ما را تار کرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دور کردند از من آن یار مرا</p></div>
<div class="m2"><p>ای خدا گیرد سبب کار مرا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>عاشقم کردی به آن چشم سیاه</p></div>
<div class="m2"><p>از نگاهی دین و دل بردی ز راه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سوی چشم [ای تو ای جانان] من</p></div>
<div class="m2"><p>رحمتی بر درد بی درمان من</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تا چه شد آن مهربانی های تو</p></div>
<div class="m2"><p>وان همه شیرین زبانی های تو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چشم دارم کز غم آزادم کنی</p></div>
<div class="m2"><p>با نگاهی یک رهی شادم کنی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بر «وفایی» بی وفایی تا به کی؟</p></div>
<div class="m2"><p>ای دل آرام! آن جدایی تا به کی؟</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نی غلط گفتم که خورشید هدی است</p></div>
<div class="m2"><p>طلعتش آیینه ی نور خداست</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گلبنی از باغ طه نازنین</p></div>
<div class="m2"><p>قطب عالم فخر آل یاء و سین</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گر به دل ها سکه ی آگاهی است</p></div>
<div class="m2"><p>ماه تا ماهی عبیداللهی است</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>مطلع نور خدا تا غایتی</p></div>
<div class="m2"><p>آفتاب از عکس رویش آیتی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>خنده اش مشکل گشا، معجز نظام</p></div>
<div class="m2"><p>از پی دل مردگان «یحیی العظام»</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ابروش از «قاب قوسین» با خبر</p></div>
<div class="m2"><p>رویش از آیات «و انشق القمر»</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نفس بد را آن چه چشم مست او است</p></div>
<div class="m2"><p>«ذو الفقار حیدر» ی در دست او است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ابروش در قلب نفس کفر کار</p></div>
<div class="m2"><p>آیت «لا سیف الا ذوالفقار»</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تاجدار خطه ی فقر و فنا</p></div>
<div class="m2"><p>شهسوار عرصه ی فخر و غنا</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مرشدی کز التفات یک نگاه</p></div>
<div class="m2"><p>جام جم سازد دل و جان سیاه</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>خواجگان را بنده در هر دو سرا</p></div>
<div class="m2"><p>بندگان را خواجه ی مشگل گشا</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>از جمالش نور مطلق روشن است</p></div>
<div class="m2"><p>زین سبب نور دل و جان من است</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>تا عروس شرع ازو زیور گرفت</p></div>
<div class="m2"><p>دید احمد رونقی دیگر گرفت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>هر که نبود چون سگان خاک درش</p></div>
<div class="m2"><p>سگ از او به، خاک عالم بر سرش</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>در سمرقند لبش معجز نماست</p></div>
<div class="m2"><p>آری آری خواجه ی احرار ماست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ای چراغ خلوت صدق و صفا</p></div>
<div class="m2"><p>ای فراغ سینه ی اهل وفا</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>شب چراغ روی تو بر هر که تافت</p></div>
<div class="m2"><p>در سیاهی آب حیوان دیده یافت</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هست سرو قامتت طوبا شبیه</p></div>
<div class="m2"><p>سایه اش «طوبی لمن دخل فیه»</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دل گرفتار بلای عشق تو است</p></div>
<div class="m2"><p>جان شهید کربلای عشق تواست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بس که از دل موج خون افشانده ام</p></div>
<div class="m2"><p>در میان آب و آتش مانده ام</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دیده ای دارم پر از خوناب ناب</p></div>
<div class="m2"><p>سینه ای چون جان مشتاقان کباب</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>دیده بر دریای خون طوفان زده</p></div>
<div class="m2"><p>سینه از از سوز درون آتشکده</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>رویت از گلزار چین مشک ختن</p></div>
<div class="m2"><p>مویت از مشک خطای صد ختن</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>با صبا بفرست ازان گل دسته ای</p></div>
<div class="m2"><p>و از نسیم عطر سنبل بسته ای</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ما زیاده غمگسار آواره ایم</p></div>
<div class="m2"><p>در غریبی بی کس و بی چاره ایم</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ما ز داغ زلف زندان دیده ایم</p></div>
<div class="m2"><p>محنت شام غریبان دیده ایم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>آن چه در راه غریبان منزوی است</p></div>
<div class="m2"><p>گر بلرزد عرش اعظم دور نیست</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>حق ذات آن خداوند عظیم</p></div>
<div class="m2"><p>که بود بر بندگان خود رحیم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>حق آن اسمی که بر لوح از قلم</p></div>
<div class="m2"><p>از شرافت ابتدا آمد رقم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>حق آن نوری که [آمد در جهات]</p></div>
<div class="m2"><p>غلغله پیدا شد اندر کائنات</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>با خدایی ها که بود از بی خودی</p></div>
<div class="m2"><p>ناله های «ما عرفنا» می زدی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>جسم پاک از بندگی پر ناله بود</p></div>
<div class="m2"><p>جان نوای «ما عرفنا» می سرود</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>حق روح آن دلیل المرسلین</p></div>
<div class="m2"><p>فخر آدم، رحمة للعالمین</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>حق آن نوری که از ابر قدم</p></div>
<div class="m2"><p>گشت عرش و کرسی و لوح و قلم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>حق آن روحانیانی با هم اند</p></div>
<div class="m2"><p>که به جان حمال عرش اعظم اند</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>حق جان جمله ی پیغمبران</p></div>
<div class="m2"><p>بر طریق حق دلیل بندگان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>حق روح خواجگان نقشبند</p></div>
<div class="m2"><p>وارهان جان وفایی را ز بند</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>پای بند نفس نا فرمان شدم</p></div>
<div class="m2"><p>زیر بار ذلت و عصیان شدم</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>همتی کن شاد کن از رحمتم</p></div>
<div class="m2"><p>دست دل گیر و بر آر از ظلمتم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>جوش زن دریای رحمت را دمی</p></div>
<div class="m2"><p>دردمندان را ببخشا مرهمی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>دست دل بگشا و چشم سر ببند</p></div>
<div class="m2"><p>التفاتی کن به جان دردمند</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>دست بگشا در حضور کبریا</p></div>
<div class="m2"><p>لب بجنبان در مقام التجا</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>کای خداوند خطا بخش کریم</p></div>
<div class="m2"><p>ای خدای پاک و دادار رحیم</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بگذر از جرم «وفایی» هر چه هست</p></div>
<div class="m2"><p>پس به حسن اختتامش گیر دست</p></div></div>