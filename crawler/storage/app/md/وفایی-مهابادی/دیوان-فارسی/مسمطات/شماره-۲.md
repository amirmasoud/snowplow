---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>بگذار تا بگریم در دیر راهبانان</p></div>
<div class="m2"><p>بگذار تا بنالم در کوی زند خوانان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار تا بگیریم زنار زلف جانان</p></div>
<div class="m2"><p>بگذار تا بیابم سر رشته ای ز ایمان</p></div></div>
<div class="b2" id="bn3"><p>این کار کار عشق است دخلی به دین ندارد</p></div>
<div class="b" id="bn4"><div class="m1"><p>من رند و لا أبالی سرمست و دلستانم</p></div>
<div class="m2"><p>دفتر ز من چه خواهی، من پارسی ندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدهوش یک سرودی از لهجه ی مغانم</p></div>
<div class="m2"><p>من بعد ازین برانم درس مغان بخوانم</p></div></div>
<div class="b2" id="bn6"><p>این کار کار عشق است دخلی به دین ندارد</p></div>
<div class="b" id="bn7"><div class="m1"><p>ترسای نا مسلمان چون آهوی رمیده</p></div>
<div class="m2"><p>آرام من گرفته، در زلف خود کشیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بویی ز زلف و رویش بر جان من وزیده</p></div>
<div class="m2"><p>یک جای کفر و ایمان آخر بگو، که دیده؟</p></div></div>
<div class="b2" id="bn9"><p>این کار کار عشق است دخلی به دین ندارد</p></div>
<div class="b" id="bn10"><div class="m1"><p>ای شیخ پاک دامن ای پادشاه شاهم</p></div>
<div class="m2"><p>آخر بگو خدا را تا چیست روی راهم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد بار اگر گناه است این عشق پر گناهم</p></div>
<div class="m2"><p>حاشا اگر بهشت است بی دلستان نخواهم</p></div></div>
<div class="b2" id="bn12"><p>این کار کار عشق است دخلی به دین ندارد</p></div>
<div class="b" id="bn13"><div class="m1"><p>بالله اگر «وفایی» زان باغ گل نچیند</p></div>
<div class="m2"><p>از باغبان برنجد با داغ دل نشیند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیرون ازین دو عالم یک خلوتی گزیند</p></div>
<div class="m2"><p>تا در جهان بمانی بی او جهان نبیند</p></div></div>
<div class="b2" id="bn15"><p>این کار کار عشق است دخلی به دین ندارد</p></div>