---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>مرا که دوش دو چشم از غم نگار تر آمد</p></div>
<div class="m2"><p>پگاه آن که ستاره روان شود سحر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان که گل شکفد سرو بالا از اثر ابر</p></div>
<div class="m2"><p>ز گریه ام صنم من به خنده جلوه گر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که هان مرغ «وفایی» شب فراق سرآمد</p></div>
<div class="m2"><p>ترا مراد برآمد که آفتاب برآمد</p></div></div>