---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>دل دیوانه ای دارم دمی بی غم نخواهد شد</p></div>
<div class="m2"><p>سر شوریده ای دارم به سامان هم نخواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلارام من از روزی که آرام دلم برده</p></div>
<div class="m2"><p>دلم یک دم نیارامد به من همدم نخواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زلف خویشتن حال دلم زیر و زبر کردی</p></div>
<div class="m2"><p>چرا مهدر نباشد دل؟ چرا درهم نخواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگوی این اشک ریزی چیست اندر آستان من؟</p></div>
<div class="m2"><p>-دل و دینم فدایت- کعبه بی زمزم نخواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو با بخت سعید من محمد گشته پشتیبان</p></div>
<div class="m2"><p>یقین زخم دل دیوانه بی مرهم نخواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«وفایی» گر شود سیراب از آن دریای حق، خوانی</p></div>
<div class="m2"><p>تو حق گویی و می دانی ز دریا کم نخواهد شد</p></div></div>