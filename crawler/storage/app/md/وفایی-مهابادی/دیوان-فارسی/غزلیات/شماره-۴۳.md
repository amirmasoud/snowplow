---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>چشم سیاه مستت با ما ببین چه ها کرد</p></div>
<div class="m2"><p>با یک نگه دل و دین از دست ما رها کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بوسه خون بها کرد لعل لبش ندانم</p></div>
<div class="m2"><p>گر دشمنی به دل داشت این دوستی چرا کرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با هر نگاه و نازم صد بار کشت و خون ریخت</p></div>
<div class="m2"><p>شیرین کجا به فرهاد این جور و این جفا کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد صبا ندانم با گل چه نکته ای گفت؟</p></div>
<div class="m2"><p>کز حسرت دهانت گل جامه را قبا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها نه من شهیدم از دست تیغ نازش</p></div>
<div class="m2"><p>هر گوشه را که بینی صحرای کربلا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قربان چشم مستم خونم بریخت، رستم</p></div>
<div class="m2"><p>در ضمن یک عداوت، کار صد آشنا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هندو اگر به کوثر گویند ره ندارد</p></div>
<div class="m2"><p>بهر چه شد لب تو خال سیاه جا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیسو نه، باغ سنبل، عارض نه، خرمن گل</p></div>
<div class="m2"><p>این است آن بهشتی خدا برای ما کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی تو، سرو بالا، دیدم ز خود برفتم</p></div>
<div class="m2"><p>کز عالم بلندی ماه مرا صدا کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برقع فکند آتش در ما زد و ندانم</p></div>
<div class="m2"><p>خود کرد راز خود فاش ما را چرا رسوا کرد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زلفت نمی گذارد روی تو سیر بینم</p></div>
<div class="m2"><p>این پاره ابر تاریک روز مرا سیا کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیر مغان شب دوش می گفت کوزه بر دوش:</p></div>
<div class="m2"><p>هی می بگیر و می نوش، هی می تو را رها کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صوفی مشو گران جان، صافی شو و خدا خوان</p></div>
<div class="m2"><p>پابند و بنده ی نان، کی روی در خدا کرد؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیوانه باش و هوشیار، آدم شو و سیه کار</p></div>
<div class="m2"><p>میخانه گیر بگذار، گویند ره خطا کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن ماه رو گشاده، رویش هم رنگ باده</p></div>
<div class="m2"><p>هم شوخ شیخ زاده، با عهد خود وفا کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دیدم آمد شتابان، آمد چو ماه تابان</p></div>
<div class="m2"><p>هم شهر و هم بیابان، چون روز روشنا کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتم: لبت ببوسم، چشمش به غمزه ام کشت</p></div>
<div class="m2"><p>خونی نکرده بودم این ترک قصد ما کرد!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتا: توام بمیری، بی جان لبم نگیری</p></div>
<div class="m2"><p>آب حیات ما را «وفایی» کم بها کرد</p></div></div>