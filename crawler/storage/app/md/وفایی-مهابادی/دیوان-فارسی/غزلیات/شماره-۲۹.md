---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>به راه دوست کسی سر نهد سبک بار است</p></div>
<div class="m2"><p>اسیر دام محبت مگو گرفتار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرم به افسر دارا فرو نمی آید</p></div>
<div class="m2"><p>که افسر سر من خاک مقدم یار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هیچ روی ز من وا نمی شود غم دوست</p></div>
<div class="m2"><p>به دور نقطه تو گویی که خط پرگار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو یوسفی و من از غم اسیر زندانم</p></div>
<div class="m2"><p>عزیز من! تو بفرمای آخر این کار است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا به باغ و به گلزار حاجتی نبود</p></div>
<div class="m2"><p>مرا که یاد جمال تو باغ گلزار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به هیچ نگیرم رواست شاخ نبات</p></div>
<div class="m2"><p>که لعل خسرو شیرین لبان شکر بار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکن هر آن چه کنی بر من از جفاکاری</p></div>
<div class="m2"><p>که نازنین صنم دلربا، دل آزار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به محفلی که تویی چشم بر کف دگران</p></div>
<div class="m2"><p>حرارتم نبرد جام اگرچه سرشار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خاک پای تو دادیم جان که تا گویند</p></div>
<div class="m2"><p>به راه دوست «وفایی» به جان وفادار است</p></div></div>