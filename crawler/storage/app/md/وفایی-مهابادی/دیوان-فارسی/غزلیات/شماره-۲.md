---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>بتم بر طلعت خود شانه زد زلف چلیپا را</p></div>
<div class="m2"><p>پریشان بر صباح عید دارد شام یلدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلاکم کرده بی پروا فرنگی زاده ترسایی</p></div>
<div class="m2"><p>که گر دستش رسد یکباره خون ریزد مسیحا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از رخسار و گیسوی تو حیرانم نمی دانم</p></div>
<div class="m2"><p>که دخلش چیست این کافر ید بیضای موسی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر محراب ابروی تو را از گریه می بیند</p></div>
<div class="m2"><p>به فرق خویشتن ویران کند راهب کلیسا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال قد جانان در دل سوزان و حیرانم</p></div>
<div class="m2"><p>که ترسم آتش دوزخ بسوزد نخل طوبی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار آمد بیا ساقی به رغم چرخ مینایی</p></div>
<div class="m2"><p>وبال گردن زاهد بریزان خون مینا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یک خنده دل و دین «وفایی» برده، حیرانم</p></div>
<div class="m2"><p>مگر برق یمان زد خرمن جان تمنا را</p></div></div>