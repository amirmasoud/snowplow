---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>دل که با طره ی جانان سر سودا دارد</p></div>
<div class="m2"><p>روزگارش که پریشان گذرد جا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع را سوزش پروانه به جایی نرساند</p></div>
<div class="m2"><p>یار در کشتنم از ناله چه پروا دارد!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهن تنگ ترا خنده اگر معجزه نیست</p></div>
<div class="m2"><p>پس چرا تنگ شکر عقد ثریا دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک زمان بخت من از خواب گران در نشود</p></div>
<div class="m2"><p>یادگاری است کزان نرگس شهلا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غم طوبی و فردوس فراغی دگر است</p></div>
<div class="m2"><p>هر که با یاد تو در کوی تو مأوا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف را خم شده در پیش خطش دیدم و گفت:</p></div>
<div class="m2"><p>رو سیه آن که به نو کیسه مدارا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از من غم زده دل می طلبد غمزده ی دوست</p></div>
<div class="m2"><p>دوستان! دلبر ما نرگس گویا دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم: از لعل لبت بوسه «وفایی» طلبید</p></div>
<div class="m2"><p>گفت: دیوانه که از هیچ تمنا دارد</p></div></div>