---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>ای رخ و زلفت شب تاریک و روز روشن است</p></div>
<div class="m2"><p>بی شب و روز تو روز و شب فغان کار من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرهٔ مشکین مزن بر هم دگر مشکن دلم</p></div>
<div class="m2"><p>زان که مشکین طره ات مسکین دلم را مسکن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع کافوری همی گویند بی دود است و من</p></div>
<div class="m2"><p>حیرتم از سنبل زلف و بیاض گردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا بینم ترا در من فتد شوری دگر</p></div>
<div class="m2"><p>نغمه ی بلبل بود آری که هر جا گلشن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان ماهی ندارد، بوستان سروی چو من</p></div>
<div class="m2"><p>ماه من مشکین کمند و سرو من سیمین تن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناتوان و خسته ام بی خنده ی شیرین لبت</p></div>
<div class="m2"><p>آری آن آرام جان و وان دگر جان من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفت جان «وفایی» در سر بازار عشق</p></div>
<div class="m2"><p>زلف مشکین و لب شیرین و چشم پر فن است</p></div></div>