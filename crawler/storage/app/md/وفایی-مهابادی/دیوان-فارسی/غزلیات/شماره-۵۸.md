---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>هم چو شاهین به هوس بال و پری بگشادم</p></div>
<div class="m2"><p>در سر سیر و تماشا سر خود بنهادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آشیان شهپر سیاره کشی کردم راست</p></div>
<div class="m2"><p>یک نظر جلوه کنان بر سر چرخ استادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس آینه چو طوطی به شکر بالیدم</p></div>
<div class="m2"><p>غافل از دست خط و درس و خط استادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه سرو گلی گشت مرا دیر مغان</p></div>
<div class="m2"><p>نشئه ی عشق گل آمد به مبارک بادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرتو شمع بدیدم چو پروانه ز دور</p></div>
<div class="m2"><p>با همه سرکشی و کبر به دو جان دادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک دل نیلوفر آسا بزدم بر لب آب</p></div>
<div class="m2"><p>ز آتش مهر بر انداخته شد بنیادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به شیرین شکری دادم چون خسرو عشق</p></div>
<div class="m2"><p>عاقبت کرد جگر سوخته چون فرهادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل مارا هوس خال تو و زلف و لب است</p></div>
<div class="m2"><p>دانه ناچیده و در دام بلا افتادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به «وفایی» نگهی کن چو تو سودای منی</p></div>
<div class="m2"><p>به که فریاد کنم گر تو نپرسی دادم؟</p></div></div>