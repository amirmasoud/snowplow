---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>طره از باد وزان لرزان به روی دلبر است</p></div>
<div class="m2"><p>کافرم گر باغبان باغ جنت کافر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروان نازنینی بی قرارم کرده است</p></div>
<div class="m2"><p>یا رب این آتش مگر از مهر در نیلوفر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زوصف زلف و لب در باغ رویت دم زدم</p></div>
<div class="m2"><p>نو نهال خامه ام را شکر و عنبر بر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طره ی آشفته است نازم که بر ابروی کج</p></div>
<div class="m2"><p>راست چون در قلب کافر ذوالفقار حیدر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با قد خم، ناله ی جانکاه، آه سینه ام</p></div>
<div class="m2"><p>بی تو گویی نغمه های عود و و دود و مجمر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غمت با چشم خونین می خورم خون جگر</p></div>
<div class="m2"><p>بی تو در بزم غم آنم باده اینم ساغر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده روشن کن مرا باری به لطف از در درآ</p></div>
<div class="m2"><p>تا کی آخر، دلبرا، چشم «وفایی» بر در است!</p></div></div>