---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>شنیدم کز وفایی لطف کردی یاد فرمودی</p></div>
<div class="m2"><p>ز شاهی کم مبادت بنده ای را شاد فرمودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غمزه گر دلم بردی، به نازم باز دل دادی</p></div>
<div class="m2"><p>جزاک الله، خرابم کردی و آباد فرمودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی تسخیر عالم خرق عادت جز تو کس ننمود</p></div>
<div class="m2"><p>به خنده شکرستان از عدم ایجاد فرمودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فتح دل خط آوردی و پشتیبان گیسو شد</p></div>
<div class="m2"><p>به بیت الله حبش را از خطا امداد فرمودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانستم کدامین بود از خال و خطت قاتل</p></div>
<div class="m2"><p>همین دانم نگاهی کردی و جلاد فرمودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر یک غمزه صد خون ریختن نشنیده ام، جانا</p></div>
<div class="m2"><p>به این چشم کمان ابرو تو این بیداد فرمودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز استغنای ناز، ای خسرو خوبان، چه ها کردی</p></div>
<div class="m2"><p>لب شیرین گشادی قتل صد فرهاد فرمودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گلشن سرورا از حسرت بالای خود کشتی</p></div>
<div class="m2"><p>کرم کردی که این پا بسته را آزاد فرمودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوا گلساز و شکر خیز و مشکین شد نمی دانم</p></div>
<div class="m2"><p>چه حرفی از زبان گل به گوش باد فرمودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به چشم تر غبار از دل بشو، گفتی «وفایی» را</p></div>
<div class="m2"><p>پیاپی جام می ده چون شط بغداد فرمودی</p></div></div>