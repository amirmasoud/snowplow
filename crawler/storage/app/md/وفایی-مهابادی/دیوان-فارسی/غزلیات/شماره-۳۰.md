---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>دلبری دارم که در عالم نظیرش کم تر است</p></div>
<div class="m2"><p>رخ قمر، بالا صنوبر، لب شکر، تن مرمر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف و رو، بالا و ابرو، وان بناگوش و لبش</p></div>
<div class="m2"><p>عقرب و خورشید، تیر و قوس و شیر و شکر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامت دل کش، جمال خوش، دهان تنگ او</p></div>
<div class="m2"><p>نخل طوبی، باغ جنت، سبزه زار کوثر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه خالی، شمع سوزان، یار در بر، می به کف</p></div>
<div class="m2"><p>هر که را ممکن بود این عیش و نوش اسکندر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب به لب، سینه به سینه، ناف بر بالای ناف</p></div>
<div class="m2"><p>ای مسلمانان ازین عالم چه عالم خوش تر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم: ای نامهربان این خانه آن کیست؟ گفت:</p></div>
<div class="m2"><p>چشمه ی حیوان اگر خواهی کمی پایین تر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمه و معشوقه ی ایمان ستان در خلوتی</p></div>
<div class="m2"><p>هر کسی داند «وفایی» را مسلمان، کافر است</p></div></div>