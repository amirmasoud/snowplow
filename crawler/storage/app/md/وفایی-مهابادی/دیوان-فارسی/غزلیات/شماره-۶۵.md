---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>بی تو ای دوست ندانی که چه گویم چونم</p></div>
<div class="m2"><p>به جمال تو که چون سوخت در و بیرونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه ای کن به من و شمع وجودم بردار</p></div>
<div class="m2"><p>وعده ی وصل من آن است بریزی خونم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانم آزاد کن از واهمه ی وصل و فراق</p></div>
<div class="m2"><p>دست من آر تو در گردن خود یا خونم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ندانم که ز زلف تو رهایی طلبم</p></div>
<div class="m2"><p>گر ز زنجیر تو سرباز کشم مجنونم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد میدان جفای تو «وفایی» است، بیا</p></div>
<div class="m2"><p>خنجر ناز به دل زن که به جان ممنونم</p></div></div>