---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای فتنهٔ عالم به نگاه! این چه جمال است؟</p></div>
<div class="m2"><p>کز وصف جمال تو زبانم همه لال است!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا دل من سوخت ز داغ لبت، آخر:</p></div>
<div class="m2"><p>تا کی دل من تشنه ی این آب زلال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار که قربان شوم این شمع رخت را</p></div>
<div class="m2"><p>در مذهب ما سوزش پروانه وصال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را هوس وصل لب و زلف تو هیهات!</p></div>
<div class="m2"><p>عمر خضر و آب بقا، فکر محال است!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این طلعت زیبای ترا مه نتوان گفت</p></div>
<div class="m2"><p>کاین ثابت و آن سوخته ی برق زوال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«شاید به دمم فاتحه ای عین کمال است</p></div>
<div class="m2"><p>کس ماه ندیده است که در عین کمال است»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور از تو چنان زار و نزار است «وفایی»</p></div>
<div class="m2"><p>گویی که زمهجوری خورشید هلال است</p></div></div>