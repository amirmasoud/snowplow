---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>آن روز کزان طره به رخ بست شکن را</p></div>
<div class="m2"><p>در گردن خورشید و مه افکند رسن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیسوی تو خود رنگی و روی تو فرنگی</p></div>
<div class="m2"><p>کافر شده زان، برده ز دل حب وطن را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز آ ای بت من، در سر این طره چه داری؟</p></div>
<div class="m2"><p>زان رو که به هم برزده ای چین و ختن را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زلف تو از قامت و رخ ناله ی دل هاست</p></div>
<div class="m2"><p>چون بلبل و قمری که سبب سرو سمن را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین گونه که خیزد زلبت خنده دمادم</p></div>
<div class="m2"><p>شکر نشنیدم که بود لعل یمن را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخرام به باغ سمن و سنبله امروز</p></div>
<div class="m2"><p>زان پیش که سنبل دمد این برگ سمن را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در باغ گذر کرد مگر سرو چمانت</p></div>
<div class="m2"><p>کز گریه به گل برده فرو سرو چمن را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشکین سر زلفت دل مسکین «وفایی»</p></div>
<div class="m2"><p>مشکن دگر این زلف پر از تاب و شکن را</p></div></div>