---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>رنجیده ی یاران ز دوران گله دارم</p></div>
<div class="m2"><p>آزرده ی خارم ز گلستان گله دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زاغ و زغن هم قفسم کرده زمانه</p></div>
<div class="m2"><p>از صحبت ناجنس به چندان گله دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم تو مرا کشت چو با ابرو و مژگان</p></div>
<div class="m2"><p>این تیر و کمان چیست ز ترکان گله دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطلوب من آن خال سیاه لب تو است</p></div>
<div class="m2"><p>حاشا که من از چشمه ی حیوان گله دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کافر چه کند گر صنم بت نپرستد</p></div>
<div class="m2"><p>با آن همه پاکی ز مسلمان گله دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن به کمر بر زده هر کس به طریقی</p></div>
<div class="m2"><p>از غفلت این قوم فراوان گله دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریاد که نازک دلی من به مقامی است</p></div>
<div class="m2"><p>کز جنبش و آلودگی جان گله دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارف کمر یار و معارف دهن دوست</p></div>
<div class="m2"><p>شاید که ز خود ظاهر پنهان گله دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هر سر بازار بگویم به دف و نی</p></div>
<div class="m2"><p>این نکته ی سر بسته که من زان گله دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بفروخت به چندین هنرم هیچ نفرمود</p></div>
<div class="m2"><p>از خواجه ی خود بنده هزاران گله دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اطوار من بهر خدا نیست «وفایی»</p></div>
<div class="m2"><p>از ورد شبت نیز به قرآن گله دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خویش بیرون آی و خدادان و خداخوان</p></div>
<div class="m2"><p>دیگر سخن از غیر به یزدان گله دارم</p></div></div>