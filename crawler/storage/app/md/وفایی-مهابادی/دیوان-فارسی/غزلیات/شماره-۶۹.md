---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>فدای جان پاکت ای غلام در به در کرده</p></div>
<div class="m2"><p>ز هجران تو جان از تن، قرار از دل سفر کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به امید شفایی دل در آن چشم سیه بستم</p></div>
<div class="m2"><p>که مژگان تو در آن چشم جان پر نیشتر کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فزون شد از نگاهت درد من با آن که خندیدی</p></div>
<div class="m2"><p>چرا گویند پس بیمار را گل در شکر کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط آورد از پی امداد زلف از بهر قتل من</p></div>
<div class="m2"><p>به چین قانع نشد جیش خطا را هم خبر کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهیدان کی اند افتاده سر خونین کفن در باغ</p></div>
<div class="m2"><p>مگر با لاله زاران، نازنین از ره گذر کرده؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی دانم چرا در گوش گل باری نخواهد رفت</p></div>
<div class="m2"><p>فغان بلبل مسکین جهان زیر و زبر کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزیبد جز «وفایی» افسر و تخت وفاداری</p></div>
<div class="m2"><p>که در ملک محبت ترک سر را ترگ سر کرده</p></div></div>