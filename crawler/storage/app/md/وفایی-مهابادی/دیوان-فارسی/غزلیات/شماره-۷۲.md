---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>ای روی تو ابروی تو وان دو لب شیرین</p></div>
<div class="m2"><p>هم قبله و هم کعبه و هم جان وفایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سرو نه ای جلوه ی جان بخش چه داری؟</p></div>
<div class="m2"><p>گر ماه نه ای بر سر این چرخ چرایی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتند که بر چشمه کند سرو سهی جای</p></div>
<div class="m2"><p>باور نکنم تا به سر چشم نیایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون روی بگردانم از ابروی تو جانا!</p></div>
<div class="m2"><p>کارباب وفارا به خدا قبله نمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی نام تو هر چند بود فتح جهان خام</p></div>
<div class="m2"><p>بی خامه ی من هم نبود نامه گشایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که بگیرد نمک ساعد شاهم</p></div>
<div class="m2"><p>نامردم اگر باز نگردم چو «وفایی»</p></div></div>