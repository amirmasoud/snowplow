---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>به کوی یار هردم از فراق یار می‌نالم</p></div>
<div class="m2"><p>چو بلبل از غم گلزار در گلزار می‌نالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهید چشم مستم، با خیال طره می‌گریم</p></div>
<div class="m2"><p>چو بیمارم به زاری در شبان تار می‌نالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توانم را به چشم ناتوان بردی و خود رفتی</p></div>
<div class="m2"><p>کنون در کنج غم بی‌یار و بی‌غمخوار می‌نالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسیر زلف یارم عاشق محراب ابرویش</p></div>
<div class="m2"><p>مسلمانم ولی در حلقهٔ زَنّار می‌نالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در و دیوار گلشن شد ز عکس روی تو زآن رو</p></div>
<div class="m2"><p>چو بلبل هر زمان بر هر در و دیوار می‌نالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌دانم بلای مردمان از چیست عاشق را</p></div>
<div class="m2"><p>ترا بیمار چشمان سیه، من زار می‌نالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرابی ساقیا! آبی برین آتش فشان کامشب</p></div>
<div class="m2"><p>مغنی‌وار می‌سوزم، چو موسیقار می‌نالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه بنویسد «وفایی» شرح هجران تو؟ چون هردم</p></div>
<div class="m2"><p>به یاد سرو بالای تو قمری‌وار می‌نالم</p></div></div>