---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>به جان آمد دلم تا کی دل، ای جان، بی دوا باشد</p></div>
<div class="m2"><p>نگاهی کن به دل کاین یک نگاهم اکتفا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر در وحدتم از کثرت حیرت نیم خالی</p></div>
<div class="m2"><p>دهانت از تبسم تا فنای پر بقا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جا برخیز تا طوفی کنم گرد سرت گردم</p></div>
<div class="m2"><p>نماز عاشقان را در قیامت هم قضا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جز دلبر نمی گویم غم دل با کسی زان رو</p></div>
<div class="m2"><p>غم دل با کسی گویم که با دل آشنا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفتم قبله در دست آمد و عمری در احرامم</p></div>
<div class="m2"><p>چه سود این سعی بی حاصل اگر دل بی صفا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین از خنجر ناز تو خون می بارد از هر سو</p></div>
<div class="m2"><p>به عالم هر که را بینی شهید کربلا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چشمان سیه بردی دل و دین «وفایی» را</p></div>
<div class="m2"><p>ترا آهوی چین گفتم خطا نبود روا باشد</p></div></div>