---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای ترک خطا، ماه ختن، سرو خودآرا</p></div>
<div class="m2"><p>بر سوخته ی خویش ببخشای خدارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما تشنه لبانیم تو هم آب بقایی</p></div>
<div class="m2"><p>بر تشنه یکی جرعه ببخش آب بقا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر شکن افتاده هزاران چو دل ما</p></div>
<div class="m2"><p>مشکن دل ما، شانه مزن زلف دو تا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرایش روی تو صلا زد به قیامت</p></div>
<div class="m2"><p>یارا در این روی دلارای میارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند «وفایی» ز فلانی تو بکش دست</p></div>
<div class="m2"><p>با پادشهان کی سر و کار است گدا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مکتب ما حرف جفا خوانده نمی شد</p></div>
<div class="m2"><p>چشم تو اگر درس به من داد وفا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ناکسی من، همه ملت گله دارند</p></div>
<div class="m2"><p>در صومعه و دیر مسلمان و نصارا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لطفی کن و بر حال «وفایی» نظری کن</p></div>
<div class="m2"><p>ای خواجه ی احرار من ای شاه بخارا</p></div></div>