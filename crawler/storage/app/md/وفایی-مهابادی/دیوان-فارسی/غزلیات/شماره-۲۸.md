---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>خطا گفتم که زلفت مشک چین است</p></div>
<div class="m2"><p>سواد چشم مستت حور عین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو دل خواهی، من از جان دست دارم</p></div>
<div class="m2"><p>که در مهر وفا رسم این چنین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جفا خواهی، وفا خواهی بفرما</p></div>
<div class="m2"><p>که کار نازنینان نازنین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه گویم وصفت از شیرین دهانی</p></div>
<div class="m2"><p>که سر روح با روح آفرین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو فرمودی قدم سرو روان است</p></div>
<div class="m2"><p>مرا زین راستی علم الیقین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خطای ما اگر حدی ندارد</p></div>
<div class="m2"><p>عطای حق تعالی بیش ازین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مترس از ترک خدمت ها «وفایی»</p></div>
<div class="m2"><p>که خواجه «رحمة للعالمین» است</p></div></div>