---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>گر من سوخته بر شاهد خوبان برسم</p></div>
<div class="m2"><p>پشت شاهین شکند شهپر بال مگسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاجز نفس شدم سنگ دل از صحبت او</p></div>
<div class="m2"><p>مرغ باغ ارمم هم نفس خار و خسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوطیان در چمن هند به شکر شکنی</p></div>
<div class="m2"><p>من بی چاره گرفتار بلای قفسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی خود از ناله فریاد دلم، وای به من</p></div>
<div class="m2"><p>کاروان رفته و غافل ز فغان جرسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ کس نیست چو من بی خبر افتاده ز راه</p></div>
<div class="m2"><p>دست من گیر خدایا که عجب هیچ کسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که از ناله ی زلف تو شدم نغمه سرا</p></div>
<div class="m2"><p>خواند اکنون همه کس بلبل مسکین نفسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهبازان به تو نازان، به تو پرواز کنند</p></div>
<div class="m2"><p>من با این بال و پر ریخته اندر که رسم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرم دوست به جای است «وفایی» مخروش</p></div>
<div class="m2"><p>جای دارد که برآرند همه ملتمسم</p></div></div>