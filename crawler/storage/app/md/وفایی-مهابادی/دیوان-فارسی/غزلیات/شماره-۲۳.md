---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>زلف مشکین بین که برعارض پریشان کرده است</p></div>
<div class="m2"><p>روضهٔ اسلام را چون کافرستان کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه تکلم گه تبسم در لبش از دلبری</p></div>
<div class="m2"><p>یک طبق شهد و شکر اندر نمکدان کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو در گل مانده است از حسرت و گل غرق خون</p></div>
<div class="m2"><p>سرو بالایم مگر رو در گلستان کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ وقتی با اسیران، دیلم کافر نکرد</p></div>
<div class="m2"><p>آن چه با من غمزه ی آن نامسلمان کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که نالید از رخت خون شد دلم در طره ات</p></div>
<div class="m2"><p>از غم گل بین چه با خود، مرغ شب خوان کرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داغم از خال لبت، ای جان شیرینم لبت</p></div>
<div class="m2"><p>زان همی نالم که هندو غارت جان کرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهره و زلفت مرا تنها نه چون پروانه سوخت</p></div>
<div class="m2"><p>ای بسا خون ها که این شمع شبستان کرده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این چه غوغایی است کاندر عالم افتاده مگر:</p></div>
<div class="m2"><p>چشم مستت رو به کوی می پرستان کرده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر «وفایی» جان فدای طاق ابروی تو کرد</p></div>
<div class="m2"><p>کفر نبود بر هلال عید قربان کرده است</p></div></div>