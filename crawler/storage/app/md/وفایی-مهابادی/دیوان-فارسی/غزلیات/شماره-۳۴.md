---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>رفتم کنارش امروز جا گوشواره‌ام داد</p></div>
<div class="m2"><p>این تخت و بخت و دولت ماه ستاره‌ام داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد شکر و شادمانی گیسو فکند یک سو</p></div>
<div class="m2"><p>یک گوشه در گلستان راه نظاره‌ام داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من شکر این چه گویم؟ آورد پیش رویم</p></div>
<div class="m2"><p>من یک دو بوسه گفتم، او بی‌شماره‌ام داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستم گرفت و پایی آهسته بر سرم زد</p></div>
<div class="m2"><p>جان بر در مقابل خلخال و یاره‌ام داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم: لبت بگیرم، بگذارمت بمیرم</p></div>
<div class="m2"><p>لب غنچه کرد و خندید، عمر دوباره‌ام داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم خوشش «وفایی» رسوای عالمم کرد</p></div>
<div class="m2"><p>پیر مغان چه پنهان می آشکاره‌ام داد</p></div></div>