---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>نگارینم! دل و جانم! حبیبم!</p></div>
<div class="m2"><p>همه درد درونم را طبیبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدستم غریبان می نوازی</p></div>
<div class="m2"><p>منت هم عاشقستم، هم غریبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گناه نرگس و زلف دراز است</p></div>
<div class="m2"><p>که من هم ناتوان و هم ناشکیبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایا پرده بردار از رخ وصل</p></div>
<div class="m2"><p>که دایم زین میان من خود حجیبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسلمانی ندانم دست من گیر</p></div>
<div class="m2"><p>کرم کن از میان بگشا صلیبم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دریای کرم زن دفترم را</p></div>
<div class="m2"><p>مکن شرمنده از روز حسیبم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«وفایی» داد از آن دست نگارین</p></div>
<div class="m2"><p>شهید پنجهٔ کف الخضیبم</p></div></div>