---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>بر چهره زلف خویش، پر از پیچ و تاب کرد</p></div>
<div class="m2"><p>یعنی به حسن، حلقه به گوش آفتاب کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیسو به باد داد به صد جلوه در چمن</p></div>
<div class="m2"><p>از سرو سر کشید و به سنبل عتاب کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تاب شد ز حسرت و غم چون رسید خط</p></div>
<div class="m2"><p>گویا که زلف یاد ز عهد شباب کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آوخ که یار رفت و نچیدم گلی ز وصل</p></div>
<div class="m2"><p>کامی ندیده عمر عزیزم شتاب کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم در آرزوی بتان بس که خون بریخت</p></div>
<div class="m2"><p>معموره ی وجود «وفایی» خراب کرد</p></div></div>