---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>من که در گوشه کاشانه دلی خوش دارم</p></div>
<div class="m2"><p>پادشاهم دل خویش از چه مشوش دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک طرف نغمه ی نی، یک طرف آوازه ی دف</p></div>
<div class="m2"><p>یک طرف دیده به روی بت مهوش دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلوت دل به خیال رخ و زلف دلبر</p></div>
<div class="m2"><p>هم چو بتخانه ی چین جای منقش دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغم ازحلقه زلف تو به رویت شب و روز</p></div>
<div class="m2"><p>که نسوزم چه کنم؟ نعل در آتش دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب خوش نفس و ساقی گل رخ می ناب</p></div>
<div class="m2"><p>چشم بد دور، عجب مجلس بی غش دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده ی سرو وفادار، منم در عالم</p></div>
<div class="m2"><p>چه کنم جان وفا، روح وفاکش دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا پای «وفایی» است سر آن جا بنهم</p></div>
<div class="m2"><p>این قدر مردم دیوانه نیم، هش دارم</p></div></div>