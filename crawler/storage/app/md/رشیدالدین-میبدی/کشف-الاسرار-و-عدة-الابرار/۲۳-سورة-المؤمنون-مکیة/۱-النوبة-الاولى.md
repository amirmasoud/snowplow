---
title: >-
    ۱ - النوبة الاولى
---
# ۱ - النوبة الاولى

<div class="b" id="bn1"><div class="m1"><p>«بِسْمِ اللَّهِ الرَّحْمنِ الرَّحِیمِ» بنام خداوند بزرگ بخشایش مهربان.</p></div>
<div class="m2"><p>«قَدْ أَفْلَحَ الْمُؤْمِنُونَ» (۱) جاوید پیروز آمد گرویدگان.</p></div></div>
<div class="n" id="bn2"><p>«الَّذِینَ هُمْ فِی صَلاتِهِمْ خاشِعُونَ» (۲) ایشان که در نماز خویش آرامیدگان و فرو شکستگانند.</p></div>
<div class="n" id="bn3"><p>«وَ الَّذِینَ هُمْ عَنِ اللَّغْوِ مُعْرِضُونَ» (۳) و ایشان که از نابکار روی برگردانند، «وَ الَّذِینَ هُمْ لِلزَّکاةِ فاعِلُونَ» (۴) و ایشان که زکاة مال دهند.</p></div>
<div class="n" id="bn4"><p>«وَ الَّذِینَ هُمْ لِفُرُوجِهِمْ حافِظُونَ» (۵) و ایشان که فرجهای خویش نگه دارند.</p></div>
<div class="n" id="bn5"><p>«إِلَّا عَلی‌ أَزْواجِهِمْ» مگر بر جفتان خویش، «أَوْ ما مَلَکَتْ أَیْمانُهُمْ» یا بر بردگان خویش، «فَإِنَّهُمْ غَیْرُ مَلُومِینَ» (۶) که ایشان که زنان دارند یا کنیزکان نکوهیده نیستند.</p></div>
<div class="n" id="bn6"><p>«فَمَنِ ابْتَغی‌ وَراءَ ذلِکَ» هر که بیرون از آن چیزی جوید، «فَأُولئِکَ هُمُ العادُونَ» (۷) ایشان از اندازه پسند در گذشتگانند.</p></div>
<div class="n" id="bn7"><p>«وَ الَّذِینَ هُمْ لِأَماناتِهِمْ وَ عَهْدِهِمْ راعُونَ» (۸) و ایشان که امانتها و عهدهای خویش را گوشوانانند.</p></div>
<div class="n" id="bn8"><p>«وَ الَّذِینَ هُمْ عَلی‌ صَلَواتِهِمْ یُحافِظُونَ» (۹) و ایشان که بر هنگام نمازهای خویش بر ایستادگانند.</p></div>
<div class="n" id="bn9"><p>«أُولئِکَ هُمُ الْوارِثُونَ» (۱۰) ایشانند که بهشت را میراث برانند.</p></div>
<div class="n" id="bn10"><p>«الَّذِینَ یَرِثُونَ الْفِرْدَوْسَ» ایشان که بیافتند بهشت، «هُمْ فِیها خالِدُونَ» (۱۱) ایشان در آن جاویدانند، «وَ لَقَدْ خَلَقْنَا الْإِنْسانَ» بدرستی که بیافریدیم مردم را، «مِنْ سُلالَةٍ مِنْ طِینٍ» (۱۲) از گلی ساخته کشیده، «ثُمَّ جَعَلْناهُ نُطْفَةً فِی قَرارٍ مَکِینٍ» (۱۳) آن گه او را نخست نطفه کردیم در آرامگاهی استوار.</p></div>
<div class="n" id="bn11"><p>«ثُمَّ خَلَقْنَا النُّطْفَةَ عَلَقَةً» پس آن نطفه را خونی بسته کردیم، «فَخَلَقْنَا الْعَلَقَةَ مُضْغَةً» آن گه آن خون را پاره گوشت کردیم، «فَخَلَقْنَا الْمُضْغَةَ عِظاماً» آن گه آن مضغه را استخوانها کردیم، «فَکَسَوْنَا الْعِظامَ لَحْماً» آن گه آن استخوان و اندامها را گوشت پوشانیدیم، «ثُمَّ أَنْشَأْناهُ خَلْقاً آخَرَ» پس او را آفریدیم آفریدنی دیگر، «فَتَبارَکَ اللَّهُ أَحْسَنُ الْخالِقِینَ» (۱۴) با آفرین خدای اللَّه تعالی که نیکونگارتر همه نگارندگانست و نیکو آفریدگارتر همه آفرینندگان است.</p></div>
<div class="n" id="bn12"><p>«ثُمَّ إِنَّکُمْ بَعْدَ ذلِکَ لَمَیِّتُونَ» (۱۵) پس آن گه شما پس آن مردگانید.</p></div>
<div class="n" id="bn13"><p>«ثُمَّ إِنَّکُمْ یَوْمَ الْقِیامَةِ تُبْعَثُونَ» (۱۶) پس آن گه شما را روز رستاخیز از خاک بر انگیزانیم.</p></div>