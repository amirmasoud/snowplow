---
title: >-
    ۳ - النوبة الثانیة
---
# ۳ - النوبة الثانیة

<div class="n" id="bn1"><p>قوله تعالی: فَما أُوتِیتُمْ مِنْ شَیْ‌ءٍ فَمَتاعُ الْحَیاةِ الدُّنْیا، ای: اموالکم تنفعکم مدة حیاتکم فی الدنیا، و هو نفع یسیر، وَ ما عِنْدَ اللَّهِ خَیْرٌ وَ أَبْقی‌ لِلَّذِینَ آمَنُوا وَ عَلی‌ رَبِّهِمْ یَتَوَکَّلُونَ. و منافع الآخرة المعدة للمؤمنین المتوکلین، خیر لانه امتع و الذ و ابقی، لانه دائم لا ینقطع، و قیل معناه فَما أُوتِیتُمْ مِنْ ریاش الدنیا فَمَتاعُ الْحَیاةِ الدُّنْیا لیس من زاد المعاد. و ثواب الآخرة، لا خیر وَ أَبْقی‌ لِلَّذِینَ آمَنُوا فیه بیان ان المؤمن و الکافر، یستویان، فی ان الدنیا متاع لهما یتمتعان بها و اذا صارا الی الآخرة کان ما عند اللَّه خیرا للمؤمن الذی یتوکل علیه و یفوض امره الیه و یفزع الیه بالدعاء فی السراء و الضراء.</p></div>
<div class="n" id="bn2"><p>بیان آیت آنست که: دنیا و هر چه در آنست از لذات و شهوات و منافع، متاعی اندک است، بقدر حیاة آدمیان، مؤمن و کافر در آن یکسان: عرض حاضر یا کل منه البر و الفاجر. نیکان و بدان را از آن برخورداری است، چندان که زندگانی است، پس چون بآخرت بازگردند و بر اللَّه رسند، آنچه اللَّه ساخته، مؤمنان و متوکلان را بنزدیک خویش، از آن نعیم باقی و ملک جاودانی، آن نیکوتر است و بهتر که هرگز بترسد و منقطع نگردد، چنانک رب العزة فرمود: أُکُلُها دائِمٌ وَ ظِلُّها لا مَقْطُوعَةٍ وَ لا مَمْنُوعَةٍ، عَطاءً غَیْرَ مَجْذُوذٍ.</p></div>
<div class="n" id="bn3"><p>قوله: وَ الَّذِینَ یَجْتَنِبُونَ، عطف علی الذین آمنوا، و محله جر، و معنی آنست که نعیم باقی پاینده، مؤمنانرا ساخته و متوکلان را و ایشان را که از کبائر و فواحش پرهیز کنند، و من الکبائر و الفواحش، الاشراک باللّه و الیأس من روح اللَّه و الامن من مکر اللَّه و عقوق الوالدین و قتل النفس التی حرّم اللَّه و قذف المحصنات و اکل مال الیتیم و الفرار من الزحف و اکل الربوا، و السحر و الزنا و الیمین الفاجرة و الغلول و منع الزکاة و شهادة الزور و کتمان الشهادة و شرب الخمر و نقض العهد و قطیعة الرحم. و اختلاف العلماء فی عد الکبائر ذکرناه فی سورة النساء و قوله: کَبائِرَ الْإِثْمِ، اضاف الی الاثم، فان من الاثم الصغیرة و الکبیرة، و الصغیرة مغفورة اذا اجتنبت الکبیرة لقوله تعالی إِنْ تَجْتَنِبُوا کَبائِرَ ما تُنْهَوْنَ عَنْهُ، نُکَفِّرْ عَنْکُمْ سَیِّئاتِکُمْ قرأ حمزة و الکسائی کبیر الاثم علی الواحد هاهنا و فی سورة و النجم و المراد به الشرک. قاله ابن عباس قوله: وَ إِذا ما غَضِبُوا هُمْ یَغْفِرُونَ یحلمون و یکظمون الغیظ.</p></div>
<div class="n" id="bn4"><p>وَ الَّذِینَ اسْتَجابُوا لِرَبِّهِمْ اجابوه الی ما دعاهم الیه من طاعته، وَ أَقامُوا الصَّلاةَ یعنی الصلوات الخمس فی مواقیتها بشرائطها وَ أَمْرُهُمْ شُوری‌ بَیْنَهُمْ ای: اذا حزنهم امر استشاروا ذوی الرأی، منهم. اصله من الشور و هو الإخراج. سمی به لان کل واحد من المتشاورین فی الامر کذلک یستخرج من صاحبه ما عنده، وَ مِمَّا رَزَقْناهُمْ یُنْفِقُونَ.</p></div>
<div class="n" id="bn5"><p>فی طاعة اللَّه و الدین و قیل ینفقون مقرین بانه من رزق اللَّه فان الکافر ایضا ینفق مما رزقه اللَّه لکنه جاحد. وَ الَّذِینَ إِذا أَصابَهُمُ الْبَغْیُ ای: الظلم هُمْ یَنْتَصِرُونَ. ینتقمون من ظالمیهم من غیر ان یعتدوا، قال مقاتل: هذا فی المجروح ینتصر من الجارح، فیقتص منه. مدح هاهنا المنتقم من ظالمه و عذره فی الایة الثالثة و هی قوله: وَ لَمَنِ انْتَصَرَ بَعْدَ ظُلْمِهِ الایة. و مدحه فی آخر الشعراء فی قوله: وَ انْتَصَرُوا مِنْ بَعْدِ ما ظُلِمُوا، و ذلک لانه یکره للمؤمن ان یعرض نفسه للذل. و فی الخبر: لا ینبغی للمؤمن ان یذل نفسه، قیل یا رسول اللَّه و کیف هو، قال: یتعرض من البلاء لما یطیق.</p></div>
<div class="n" id="bn6"><p>و قیل نزلت هذه الآیات فی ابی بکر الصدیق و قال ابن زید: جعل اللَّه المؤمنین صنفین.</p></div>
<div class="n" id="bn7"><p>صنف یعفون عن ظالمیهم، فبدأ بذکرهم و هو قوله: وَ إِذا ما غَضِبُوا هُمْ یَغْفِرُونَ، و صنف ینتصرون من ظالمیهم، و هم الذین ذکروا فی هذه الایة، و قال عطاء: هم الذین اخرجهم الکفار من مکة و بغوا علیهم، ثم مکّنهم اللَّه فی الارض حتی انتصروا ممن ظلمهم ثم ذکر الانتصار. بقوله: وَ جَزاءُ سَیِّئَةٍ سَیِّئَةٌ مِثْلُها، الاولی هی السیئة فی اللفظ و المعنی، و الثانیة سیئة فی اللفظ و عاملها لیس بمسی‌ء لانها مجازاة بالسوء لا توجب ذنبا کقوله: فَمَنِ اعْتَدی‌ عَلَیْکُمْ فَاعْتَدُوا عَلَیْهِ بِمِثْلِ مَا اعْتَدی‌ عَلَیْکُمْ قال مقاتل: یرید به القصاص فی الجراحات و الدماء و قال السدّی هو ان یجاب قائل الکلمة القبیحة بمثلها من غیر ان یعتدی فاذا قال اخزاک اللَّه یقول، اخزاک اللَّه. ثم ذکر العفو، فقال: فَمَنْ عَفا یعنی عن ظلمه وَ أَصْلَحَ بالعفو بینه و بین ظالمه، فَأَجْرُهُ عَلَی اللَّهِ. و فی الخبر اذا کان یوم القیمة نادی مناد: من کان له اجر علی اللَّه فلیقم، قال: فیقوم عنق کثیر، قال: فیقال ما اجرکم علی اللَّه، قال: فیقولون نحن الذین عفونا عمن ظلمنا و ذلک قوله عز و جل: فَمَنْ عَفا وَ أَصْلَحَ فَأَجْرُهُ عَلَی اللَّهِ، فیقال لهم: ادخلوا الجنة باذن اللَّه. و قال صلی اللَّه علیه و آله و سلم: ما زاد عبد بعفو الا عزا، و قال (ص): من سرّه ان یشرف له البنیان او ترفع له الدرجات فلیعف عمن ظلمه، و لیصل من قطعه و لیعط من حرمه.</p></div>
<div class="n" id="bn8"><p>إِنَّهُ لا یُحِبُّ الظَّالِمِینَ. هذا راجع الی السیئة الاولی. قال ابن عباس: یعنی الذین یبدئون بالظلم.</p></div>
<div class="n" id="bn9"><p>قوله: وَ لَمَنِ انْتَصَرَ بَعْدَ ظُلْمِهِ ای بعد ظلم الظالم، ایاه فَأُولئِکَ یعنی المنتصرین، ما عَلَیْهِمْ مِنْ سَبِیلٍ. بعقوبة و مؤاخذة و ملام.</p></div>
<div class="n" id="bn10"><p>إِنَّمَا السَّبِیلُ عَلَی الَّذِینَ یَظْلِمُونَ النَّاسَ، ای یبدئون بالظلم وَ یَبْغُونَ فِی الْأَرْضِ بِغَیْرِ الْحَقِّ یعملون فیها بالمعاصی، ای: یطلبون فیها ما لیس لهم بحق، أُولئِکَ لَهُمْ، عَذابٌ أَلِیمٌ.</p></div>
<div class="n" id="bn11"><p>وَ لَمَنْ صَبَرَ وَ غَفَرَ ای صبر علی مظلمة و لم یقتصّ و لم ینتصر و تجاوز عنه، إِنَّ ذلِکَ الصبر و المغفرة لَمِنْ عَزْمِ الْأُمُورِ. عزم الامور جدها و حقیقتها تقول عزمت علیک، ای: امرتک امرا جدا، و العزیمة و الصریمة الرأی الجد، و قوله: فَإِذا عَزَمَ الْأَمْرُ، ای: جد الامر.</p></div>
<div class="n" id="bn12"><p>و فی الخبر عن رسول اللَّه (ص) فی بعض الاحکام عزمة من عزمات ربی و العازم قریب من الحالف و تقول عزمت علی الامر اذا اجمعت علیه جدّک و صدق له قصدک.</p></div>
<div class="n" id="bn13"><p>وَ مَنْ یُضْلِلِ اللَّهُ، بالخذلان، فَما لَهُ مِنْ وَلِیٍّ مِنْ بَعْدِهِ، ای: ما له احد یلی هدایته بعد اضلال اللَّه ایاه و خذلانه، وَ تَرَی الظَّالِمِینَ لَمَّا رَأَوُا الْعَذابَ یوم القیمة یَقُولُونَ هَلْ إِلی‌ مَرَدٍّ مِنْ سَبِیلٍ. ای: هل الی رجعة الی الدنیا من حیلة فنؤمن بک.</p></div>
<div class="n" id="bn14"><p>وَ تَراهُمْ یُعْرَضُونَ عَلَیْها ای: یساقون الیها. انّث العذاب حملا علی المعنی و هو النار خاشِعِینَ ای: ساکتین متواضعین، مِنَ الذُّلِّ و الخزی، یَنْظُرُونَ مِنْ طَرْفٍ خَفِیٍّ ای بعین ضعیفة و طرف ساقط من الذل. و الطرف: العین، و اصله مصدر، فلم یجمع و قیل معناه: من طرف خفی النظر. ای: یسارقون النظر الی النار من الفزع لا یملئون منها اعینهم فینظرون الیها ببعض ابصارهم و قیل: الطرف الخفی عین القلب، ای: ینظرون الی النار بقلوبهم لانهم یحشرون عمیا، وَ قالَ الَّذِینَ آمَنُوا، فی الایة اضمار، یعنی: و قال الذین آمنوا، یوم القیمة اذا عاینوا المشرکین علی هذه الحالة، إِنَّ الْخاسِرِینَ الَّذِینَ خَسِرُوا أَنْفُسَهُمْ بان صاروا الی النار، وَ أَهْلِیهِمْ: ای خسروا اهلیهم فی الجنة اذ صاروا لغیرهم، أَلا إِنَّ الظَّالِمِینَ فِی عَذابٍ مُقِیمٍ. دائم لا یزول عنهم.</p></div>
<div class="n" id="bn15"><p>وَ ما کانَ لَهُمْ مِنْ أَوْلِیاءَ، من اقرباء، یَنْصُرُونَهُمْ مِنْ دُونِ اللَّهِ ای یمنعونهم من عذاب اللَّه، وَ مَنْ یُضْلِلِ اللَّهُ فَما لَهُ مِنْ سَبِیلٍ. طریق الی الوصول الی الحق فی الدنیا و الجنة فی العقبی قد انسد علیهم طریق الخیر.</p></div>
<div class="n" id="bn16"><p>اسْتَجِیبُوا لِرَبِّکُمْ بالایمان و الطاعة، مِنْ قَبْلِ أَنْ یَأْتِیَ یَوْمٌ و هو یوم القیمة. و قیل یوم الموت، لا مَرَدَّ لَهُ مِنَ اللَّهِ ای: لا یرده اللَّه، و قیل معناه یوم من اللَّه لا یقدر احد علی رده و دفعه، ما لَکُمْ مِنْ مَلْجَإٍ یَوْمَئِذٍ، الملجأ هاهنا هو الوزر فی سورة القیامة، و المناص فی سورة ص، وَ ما لَکُمْ مِنْ نَکِیرٍ. منکر یغیر ما حل بکم من العذاب.</p></div>
<div class="n" id="bn17"><p>فَإِنْ أَعْرَضُوا عن الایمان، فَما أَرْسَلْناکَ عَلَیْهِمْ حَفِیظاً هذا کقوله: وَ ما أَنْتَ عَلَیْهِمْ بِوَکِیلٍ، و لَسْتَ عَلَیْهِمْ بِمُصَیْطِرٍ، و قیل: ما ارسلناک علیهم حفیظا، تحفظهم علی الایمان و تمنعهم من الکفر، إِنْ عَلَیْکَ إِلَّا الْبَلاغُ ای لیس علیک الا تبلیغ الرسالة، و قد فعلت و هذا قبل ان امر بالقتال، وَ إِنَّا إِذا أَذَقْنَا الْإِنْسانَ مِنَّا رَحْمَةً، نعمة و خصبا و سعة، فَرِحَ بِها ای: بطر لاجلها و زهی اعجابا بها، فلم یشکر من ازلّها و اسداها، وَ إِنْ تُصِبْهُمْ سَیِّئَةٌ محنة و قحط و ضیق، بِما قَدَّمَتْ أَیْدِیهِمْ ای: بسبب معاصیهم عقوبة لها، فَإِنَّ الْإِنْسانَ کَفُورٌ. هذا من کفران النعمة، ای یسخط من قضاء اللَّه و لم یره عقوبة و قیل ینسی و یجحد باول شدة جمیع ما سلف من النعم. و یحتمل انه خاص و المراد به الکفر باللّه سبحانه و لهذا ذکر بلفظ المبالغة ثم عظّم نفسه عز و جل فقال: لِلَّهِ مُلْکُ السَّماواتِ وَ الْأَرْضِ، المعنی: فان لم یستجیبوا لک فاعرض عنهم و اعبد اللَّه الذی له ملک السماوات و الارض له التصرف فیهما بما یرید یَخْلُقُ ما یَشاءُ من غیر اعتراض علیه، یَهَبُ لِمَنْ یَشاءُ إِناثاً فلا یکون له ولد ذکر. و فی الخبر: ان من یمن المرأة تبکیرها بالانثی قبل الذکر، و ذلک لان اللَّه عز و جل بدأ بالاناث، فقال: یَهَبُ لِمَنْ یَشاءُ إِناثاً وَ یَهَبُ لِمَنْ یَشاءُ الذُّکُورَ. فلا تکون له انثی.</p></div>
<div class="n" id="bn18"><p>أَوْ یُزَوِّجُهُمْ ذُکْراناً وَ إِناثاً ای: یجمع له بینهما فیولد له الذکور و الاناث.</p></div>
<div class="n" id="bn19"><p>معنی هذا التزویج التصنیف و الازواج الاصناف، کقوله عز و جل: مِنْ کُلِّ زَوْجٍ بَهِیجٍ ای من کل صنف حسن.</p></div>
<div class="n" id="bn20"><p>قال مجاهد: هو ان تلد المراة غلاما ثم جاریة ثم غلاما ثم جاریة و قال ابن الحنیفة: تلد توأما غلاما و جاریة و العرب تقول هؤلاء ولد فلان شطرة اذا کانوا بنین و بنات: وَ یَجْعَلُ مَنْ یَشاءُ عَقِیماً. فلا تلد و لا یولد له. قیل: هذه الایة خاصة فی الانبیاء، یَهَبُ لِمَنْ یَشاءُ یعنی لوطا لم یولد له ذکر انما ولد له ابنتان، وَ یَهَبُ لِمَنْ یَشاءُ الذُّکُورَ. ابراهیم (ع)، لم یولد له انثی، کان له اولاد ذکور أَوْ یُزَوِّجُهُمْ ذُکْراناً وَ إِناثاً، محمد (ص) ولد له بنون و بنات، وَ یَجْعَلُ مَنْ یَشاءُ عَقِیماً عیسی و یحیی کانا عقیمین لم یولد لهما ولد و قیل هذا علی وجه التمثیل، و الایة عامة فی حق کافة الناس.</p></div>
<div class="n" id="bn21"><p>و عن عائشة قالت: قال رسول اللَّه (ص): ان اولادکم هبة اللَّه لکم، یَهَبُ لِمَنْ یَشاءُ إِناثاً وَ یَهَبُ لِمَنْ یَشاءُ الذُّکُورَ، و اموالهم لکم اذا احتجتم الیها و قیل معنی الایة یَهَبُ لِمَنْ یَشاءُ إِناثاً: و یهب لمن یشاء الدنیا وَ یَهَبُ لِمَنْ یَشاءُ الذُّکُورَ الآخرة. أَوْ یُزَوِّجُهُمْ ذُکْراناً وَ إِناثاً، الدنیا و الآخرة، وَ یَجْعَلُ مَنْ یَشاءُ عَقِیماً لا دنیا و لا عقبی، إِنَّهُ عَلِیمٌ بمصالح العباد، قَدِیرٌ، قادر علی الکمال.</p></div>
<div class="n" id="bn22"><p>وَ ما کانَ لِبَشَرٍ أَنْ یُکَلِّمَهُ اللَّهُ إِلَّا وَحْیاً، سبب نزول این آیت آن بود که: جهودان گفتند: ای محمد تو دعوی نبوت میکنی، می‌گویی پیغامبرم و فرستاده اللَّه بخلق، هیچ با اللَّه سخن گویی و در وی نگری چنان که موسی باللّه سخن گفت و در اللَّه مینگریست؟ و تا ترا با اللَّه این کلام و این نظر نبود، چنان که موسی را بود با او، ما بتو ایمان نیاریم. مصطفی (ص) فرمود: لم ینظر موسی الی اللَّه، حدیث نظر مکنید در حق موسی، که موسی اللَّه را ندید، سخن شنید و لکن گوینده را ندید. رب العالمین بر وفق این سخن وی، این آیت فرستاد: قوله تعالی: وَ ما کانَ لِبَشَرٍ، هرگز هیچ بشر را نبود پیش از تو ای محمد که اللَّه با وی سخن گفتی مگر از سه گونه: اما وحیا یوحی الیه او فی المنام او بالهام، و رؤیا الانبیاء وحی. یک وجه آنست که با نمودن در خواب یا افکندن در دل، و بیشترین وحی پیغامبران پیش از مصطفی (ص) از این دو وجه بوده، یا الهام یا رؤیا. گفته‌اند، که داود (ع) بالهام حق جل جلاله زبور بدانست تا از حفظ بنوشت، اما پیغمبران مرسل که سیصد و سیزده‌اند ایشان فرشته‌ای را دیدند، یا آواز فرشته شنیدند، یا کلام حق از پس پرده شنیدند. و روی ان النبی (ص) قال: من الانبیاء من یسمع الصوت فیکون بذلک نبیا و منهم من ینفث فی اذنه و قلبه فیکون بذلک نبیا و انّ جبرئیل، یأتینی فیکلمنی کما یکلم احدکم صاحبه.</p></div>
<div class="n" id="bn23"><p>هشام بن عروة عن ابیه عن عایشه: انّ الحرث بن هشام، سأل رسول اللَّه (ص): کیف یأتیک الوحی فقال احیانا یأتینی مثل صلصلة الجرس و هو اشده علی، فیفصم عنی و قد وعیت عنه ما قال، و احیانا یتمثل لی الملک رجلا، فیکلمنی فاعی ما یقول، قالت عائشة: و لقد رأیته ینزل علیه الوحی من الیوم الشدید البرد، فیفصم عنه و ان جبینه، لیتفصد عرقا.</p></div>
<div class="n" id="bn24"><p>وجه دیگر سخن گفتن اللَّه است، با بشر از پس پرده، چنان که با موسی (ع) گفت، کلّمه و بینهما حجاب من نار، موسی از حق بی‌واسطه سخن شنید، حجاب در میان و رؤیت نه. و مصطفی (ص) شب معراج از حق جل جلاله سخن شنید بی‌واسطه و حق را دید بی‌حجاب، و مؤمنان فردای قیامت در بهشت حق را جل جلاله ببینند بی‌حجاب، و سخن وی شنوند بی‌واسطه. وجه سوم آنست که فرمود: أَوْ یُرْسِلَ رَسُولًا امّا جبرئیل او غیره من الملائکة فیوحی ذلک الرسول الی المرسل الیه، باذن اللَّه ما یشاء اللَّه. ارسال رسول یک قسم نهاد از اقسام کلام یعنی که رسول فرستد تا پیغام رساند بدستوری و فرمان وی، چنان که جبرئیل را فرستاد بمحمد (ص) تا پیغام اللَّه بگزارد و محمد (ص) را فرستاد بخلق تا پیغام اللَّه برسانید. اللَّه با جبرئیل فرمود و جبرئیل با محمد (ص) گفت و محمد با خلق گفت: قرأ نافع او یرسل برفع اللام علی الاستیناف تقدیره او هو یرسل رسولا فیوحی ساکنة الیاء، و قرأ الآخرون او یرسل بنصب اللام، فَیُوحِیَ، بنصب الیاء عطفا علی محل الوحی، لان معناه، و ما کان لبشر ان یکلمه اللَّه الا ان یوحی الیه، او یرسل رسولا إِنَّهُ عَلِیٌّ حَکِیمٌ. یدبر ما یرید.</p></div>
<div class="n" id="bn25"><p>وَ کَذلِکَ، یعنی و کما اوحینا الی سائر رسلنا أَوْحَیْنا إِلَیْکَ رُوحاً مِنْ أَمْرِنا، الروح هاهنا الوحی و الکتاب سمی روحا لانه حیاة القلوب کما ان الارواح حیاة الاجساد، ما کنت تدری یعنی قبل الوحی فی اربعین سنة، مَا الْکِتابُ وَ لَا شرایع الْإِیمانُ و معالمه، یعنی لو لا اصطفاؤنا ایاک بالایمان و الکتاب و الرسالة، ما کُنْتَ تَدْرِی، قال محمد بن اسحاق بن خزیمة: الایمان فی هذا الموضع الصلاة کقوله: وَ ما کانَ اللَّهُ لِیُضِیعَ إِیمانَکُمْ، و اهل الاصول علی ان الانبیاء (ع) کانوا مؤمنین قبل الوحی و کان النبی (ص)، یعبد اللَّه قبل الوحی علی دین ابراهیم، و لم یتبین له شرایع دینه، وَ لکِنْ جَعَلْناهُ نُوراً الهاء راجعة الی الکتاب لانه الاصل و الایمان، فرع، و الکتاب دلیل علی الایمان، و معنی جعلناه: الزمناه و رسمناه. و لیس الجعل الخلق، و قوله: فَجَعَلَهُمْ کَعَصْفٍ مَأْکُولٍ، لیس معناه جعل الخلق، انما معنی الکلام، صیرناه، نَهْدِی بِهِ، ای: نرشد بالکتاب، مَنْ نَشاءُ مِنْ عِبادِنا وَ إِنَّکَ لَتَهْدِی، ای لتدعو إِلی‌ صِراطٍ مُسْتَقِیمٍ. یعنی الاسلام، هدی اللَّه الارشاد و هدی الرسول الدعوة.</p></div>
<div class="n" id="bn26"><p>صِراطِ اللَّهِ الَّذِی لَهُ ما فِی السَّماواتِ وَ ما فِی الْأَرْضِ خلقا و ملکا، الا، کلمة تذکرة لتبصرة او تنبیه لحجة، إِلَی اللَّهِ تَصِیرُ الْأُمُورُ. ای: امور الخلائق فی الآخرة، فیجزیهم باعمالهم. هذا وعید بالجحیم و وعد بالجنة و النعیم. قال بعض السلف: احترق مصحف فلم یبق الا قوله: أَلا إِلَی اللَّهِ تَصِیرُ الْأُمُورُ.</p></div>