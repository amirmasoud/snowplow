---
title: >-
    ۷ - النوبة الثالثة
---
# ۷ - النوبة الثالثة

<div class="n" id="bn1"><p>قوله تعالی: إِنْ تَجْتَنِبُوا کَبائِرَ ما تُنْهَوْنَ عَنْهُ الآیة کبائر اهل خدمت در راه شریعت اینست که شنیدی، کبائر اهل صحبت در کوی طریقت بزبان اشارت نوعی دیگر است، و ذوقی دیگر دارد. از آنکه اهل خدمت دیگراند و اهل صحبت دیگر. خدمتیان مزدوران‌اند، و صحبتیان مقرّبان طاعت خدمتیان کبائر مقرّبانست.</p></div>
<div class="n" id="bn2"><p>چنین می‌آید در آثار که: «حسنات الأبرار سیّآت المقرّبین»، و هم ازین بابست سخن آن پیر طریقت که گفت: «ریاء العارفین خیر من اخلاص المریدین».</p></div>
<div class="n" id="bn3"><p>و مستند این قاعده آنست که مصطفی (ص) از نکته غین خبر داد، و از آن استغفار کرد، گفت: «انّه لیغان علی قلبی فاستغفر اللَّه فی الیوم سبعین مرّة»</p></div>
<div class="n" id="bn4"><p>ابو بکر صدیق گفت: لیتنی شهدت ما استغفر منه رسول اللَّه.</p></div>
<div class="n" id="bn5"><p>و نشان کبائر ایشان آنست که در عالم روش خویش ایشان را گاه‌گاهی فترتی بیفتد که فطرت ایشان مغلوب اوصاف بشریّت شود، و حیات ایشان در معرض رسوم و عادات افتد، و حقائق ایمان ایشان بشوائب اغراض و شواهد حظوظ خویش ممزوج گردد. اگر در آن حال ایشان را بریدی از صحّت ارادت و صدق افتقار و سرور وجد استقبال نکند، و دست نگیرد از چاه خودی خود بیرون نیایند.</p></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز چاه جاه خواهی تا بر آیی مردوار</p></div>
<div class="m2"><p>چنگ در زنجیر گوهر دار عنبر بار زن‌</p></div></div>
<div class="n" id="bn7"><p>بزرگان دین گفتند: که مرد تا بسر این خطرگاه نرسد، و این مقام فترت باز نگذارد، پیر طریقت نشود، و مرید گرفتن را نشاید. مردی باید که هزار بار راه گم کرده بود و براه بازآمده، تا کسی را از بیراهی براه بازآرد، که اوّل راه براه باید، آن گه راه باید. آن کس که همه بر راه باشد راه داند، امّا راه براه نداند و سرّ زلّت انبیاء و وقوع فترت ایشان اینست، و اللَّه اعلم و هو قرع باب عظیم طوبی لمن فتح علیه، و هدی الیه.</p></div>
<div class="n" id="bn8"><p>وَ لا تَتَمَنَّوْا ما فَضَّلَ اللَّهُ بِهِ بَعْضَکُمْ عَلی‌ بَعْضٍ الآیة ابو بکر کتانی گفت: من ظنّ انّه بغیر بذل الجهود یصل، فهو متمنٍّ، و من ظنّ انّه ببذل الجهود یصل فمتعنٍّ. هر که پنداشت که رنج نابرده بمقصود میرسد متمنّی است، و العاجز من اتّبع هواها و تمنّی علی اللَّه، و او که پنداشت که برنج و طلب بمطلوب میرسد متعنّی است.</p></div>
<div class="n" id="bn9"><p>شیخ الاسلام انصاری قدّس اللَّه روحه گفت: او را بطلب نیاوند. امّا طالب یاود، و تاش نیاود طلب نکند. هر چه بطلب یافتنی بود فرومایه است، یافت حق رهی را پیش از طلب. امّا طلب او را پیشین پایه است. عارف طلب از یافتن یافت، نه یافتن از طلب. چنان که مطیع طاعت از اخلاص یافت نه اخلاص از طاعت، و سبب از معنی یافت، نه معنی از سبب. الهی چون یافت تو پیش از طلب و طالب است، پس رهی از آن در طلب است که بی‌قراری برو غالب است، طالب در طلب و مطلوب‌حاصل پیش از طلب، اینت کاریست بس عجب! عجب‌تر آنست که یافت نقد شد و طلب برنخاست، حق دیده‌ور شد و پرده عزّت بجاست!.</p></div>
<div class="b" id="bn10"><div class="m1"><p>دریای ملاحتی و موج حسنات</p></div>
<div class="m2"><p>قانونه مکرّماتی و ذات حیات‌</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر طلب تو عاشقان در حسرات</p></div>
<div class="m2"><p>چون ذو القرنین و جستن آب حیات‌</p></div></div>
<div class="n" id="bn12"><p>و قیل فی معنی الآیة: تتمنّوا مقام السّادة دون أن تسلکوا سنّتهم، و تلازموا سیرتهم، و تعملوا عملهم. حال بزرگان خواهی، و راه بزرگان نارفته! کعبه مواصلت جویی، با دیده مجاهدت نابریده! نهایت دولت دوستان بینی، محنت ایشان نادیده. تعنی من ان تمنی ان یکون کمن تعنی. تو پنداری قلم عهد بر جان عاشقان آسان کشیدند! یا رقم دوستی بر دل ایشان رایگان زدند! ایشان بهر چشم زدن زخمی بر جان و دل خورده‌اند، و شربتی زهرآلوده چشیده‌اند!</p></div>
<div class="b" id="bn13"><div class="m1"><p>ای بسا شب کز برای دیدن دیدار تو</p></div>
<div class="m2"><p>از سگ کوی تو بر سر زخم سیلی خورده‌ایم</p></div></div>
<div class="n" id="bn14"><p>و لکن نه هر کسی سزای زخم اوست، و نه هر جانی شایسته غم خوردن اوست.</p></div>
<div class="n" id="bn15"><p>رحمت خدا بر آن جوانمردان باد که جان خویش هدف تیر بلاء او ساخته‌اند، و بار غم او را دل خویش محمل شناخته‌اند، و آن گه در آن بلا و اندوه این ترنّم میکنند:</p></div>
<div class="b" id="bn16"><div class="m1"><p>گر بود غم خوردنت شایسته جان رهی</p></div>
<div class="m2"><p>این نصیب از دولت عشق تو بس باشد مرا</p></div></div>
<div class="n" id="bn17"><p>آری، زخم هر کسی بر اندازه ایمان او، و بار هر کس بر قدر قوّت او، هر که‌را قوّت تمام‌تر، با روی گران‌تر. اینست سرّ آن آیت که گفت: «الرِّجالُ قَوَّامُونَ عَلَی النِّساءِ» مردان را بر زنان افزونی داد که بار، همه بر ایشانست، از آنکه کمال قوّت و شرف همّت ایشان را است، و بار بقدر قوّت کشند، یا بقدر همّت. علی قدر اهل العزم تأتی العزائم.</p></div>