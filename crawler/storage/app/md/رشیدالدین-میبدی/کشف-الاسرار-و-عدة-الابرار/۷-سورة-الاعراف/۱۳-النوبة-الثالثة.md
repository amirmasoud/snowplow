---
title: >-
    ۱۳ - النوبة الثالثة
---
# ۱۳ - النوبة الثالثة

<div class="n" id="bn1"><p>قوله تعالی: وَ أَوْرَثْنَا الْقَوْمَ الایة من صبر علی مقاساة الذلّ فی اللَّه وضع اللَّه علی رأسه قلنسوة العزّ. هر که را روزی از بهر خدا خاک مذلت بر سر آید، عن قریب او را تاج کرامت بر فرق نهند. هر که رنج برد روزی بسر گنج رسد. هر که غصه محنت کشد شراب محبت چشد. آن مستضعفان بنی اسرائیل که روزگاری در دست قهر فرعون گرفتار بودند، ببین تا سرانجام کار ایشان چون بود؟! و بر ولایت و نواحی فرعونیان چون دست یافتند، و بسرای و وطن ایشان نشستند؟! اینست که میگوید جلّ جلاله: وَ أَوْرَثْنَا الْقَوْمَ الَّذِینَ کانُوا یُسْتَضْعَفُونَ مَشارِقَ الْأَرْضِ وَ مَغارِبَهَا. آن گه گفت: بِما صَبَرُوا این بآن دادیم ایشان را که در بلیّات و مصیبات صبر کردند. دانستند که صبر کلید فرج است، و سبب زوال ضیق و حرج است، صبر تریاق زهر بلا است، و کلید گنج و مایه تقوی و محل نور فراست. صبر همه خیر است، که میگوید عزّ جلاله: وَ أَنْ تَصْبِرُوا خَیْرٌ لَکُمْ صبر از حق است و بحق است که میگوید: وَ اصْبِرْ وَ ما صَبْرُکَ إِلَّا بِاللَّهِ.</p></div>
<div class="n" id="bn2"><p>«وَ اصْبِرْ» فرمان است بعبودیت «وَ ما صَبْرُکَ إِلَّا بِاللَّهِ» اخبار است از حق ربوبیت. «وَ اصْبِرْ» تکلیف است «وَ ما صَبْرُکَ إِلَّا بِاللَّهِ» تعریف است. «وَ اصْبِرْ» تعنیف است «وَ ما صَبْرُکَ إِلَّا بِاللَّهِ» تخفیف است. وَ واعَدْنا مُوسی‌ ثَلاثِینَ لَیْلَةً چه عزیز است وعده دادن در دوستی! و چه بزرگوار است نشستن بوعده‌گاه دوستی! چه شیرین است خلف وعده در مذهب دوستی! پیر طریقت گفت در رموز این آیت: مواعید الا حبّة ان اخلفت فانها تونس. ثمّ قال:</p></div>
<div class="b" id="bn3"><div class="m1"><p>امطلینی و سوّفی</p></div>
<div class="m2"><p>و عدینی و لا تفی</p></div></div>
<div class="n" id="bn4"><p>وعده واپس داشتن و روزها در پیش وعده افکندن نپسندیده‌اند الا در مذهب دوستی، که در دوستی بی‌وفایی عین وفاست، و ناز دوستی. نبینی که رب العالمین با موسی کلیم این معاملت کرده او را سی روز وعده داد. چون بسر وعده رسید، ده روز دیگر درافزود. از آن درافزود که موسی در آن خوش می‌بود. موسی آن سی روز سرمایه شمرد و این ده روز سود، گفت: باری نقدی یک بار دیگر کلام حق شنیدم چون آن می‌افزود:</p></div>
<div class="b" id="bn5"><div class="m1"><p>رقیّ لعمرک لا تهجرینا</p></div>
<div class="m2"><p>و منّی لقاءک ثمّ امطلینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عدی و امطلی ما تشائین انّا</p></div>
<div class="m2"><p>نحبّک ان تمطلی العاشقینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فان تنجز الوعد تفرح و الا</p></div>
<div class="m2"><p>نعیش بوعدک راضین حینا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقّی شعفتنا لا تهجرینا</p></div>
<div class="m2"><p>و منّینا المنی ثم امطلینا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عدینا من غد ما شئت انّا</p></div>
<div class="m2"><p>نحب و ان مطلت الواعدینا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فاما تنجزی نفرح و الا</p></div>
<div class="m2"><p>نعیش بما نؤمّک منک حینا»</p></div></div>
<div class="n" id="bn11"><p>موسی (ع) درین سفر سی روز در انتظار بماند که طعام و شرابش یاد نیامد، و از گرسنگی خبر نداشت، از آن که محمول حق بود، در سفر کرامت، در انتظار مناجات. باز در سفر اول که او را به طالب علمی بر خضر فرستادند یک نیم روز در گرسنگی‌ طاقت نداشت، تا می‌گفت: «آتِنا غَداءَنا»، از آن که سفر تأدیب و مشقت بود، و در بدایت روش بود متحملا لا محمولا. از رنج خود خبر داشت که با خود بود، و از گرسنگی نشان دید که در راه خلق بود.</p></div>
<div class="n" id="bn12"><p>وَ قالَ مُوسی‌ لِأَخِیهِ هارُونَ اخْلُفْنِی فِی قَوْمِی چون قصد مناجات حق داشت هارون را در قوم بگذاشت، و تنها رفت، که در دوستی مشارکت نیست، و صفت دوستان در راه دوستی جز تنهایی و یکتایی نیست:</p></div>
<div class="b" id="bn13"><div class="m1"><p>گر مشغله‌ای نداری و تنهایی</p></div>
<div class="m2"><p>با ما بوفا درآ که ما را شائی‌</p></div></div>
<div class="n" id="bn14"><p>پس چون بر فرعون میشد، صحبت هارون بخواست، گفت: أَشْرِکْهُ فِی أَمْرِی، از آنکه رفتن بخلق بود، و با خلق همه وحشت است و نفرت، و در کشش بار وحشت نگریزد از رفیق و صحبت. پس چون موسی از مناجات باز گشت، و بنی اسرائیل را دید سر از چنبر طاعت بیرون برده، و گوساله پرست شده، عتابی که کرد با هارون کرد نه با ایشان که مجرم بودند، تا بدانی که نه هر که گناه کرد مستوجب عتاب گشت. عتاب هم کسی را سزد که از دوستی بر وی بقیتی مانده بود، از بیم فراق کسی سوزد که عز وصال شناسد:</p></div>
<div class="b" id="bn15"><div class="m1"><p>عشق جانان باختن کی در خور هردون بود</p></div>
<div class="m2"><p>مهر لیلی داشتن هم بابت مجنون بود</p></div></div>
<div class="n" id="bn16"><p>وَ لَمَّا جاءَ مُوسی‌ لِمِیقاتِنا موسی را دو سفر بود: یکی سفر طلب، دیگر سفر طرب. سفر طلب لیلة النار بود، و ذلک فی قوله تعالی: آنَسَ مِنْ جانِبِ الطُّورِ ناراً، و سفر طرب این بود که: وَ لَمَّا جاءَ مُوسی‌ لِمِیقاتِنا، موسی آمد از خود بیخود گشته، سر در سر خود گم کرده، از جام قدس شراب محبت نوش کرده، درد شوق این حدیث در درون وی تکیه زده، و از بحار عشق موج ارنی برخاسته. بر محلتهای بنی اسرائیل می‌گشت، و کلمتها جمع میکرد از پیغام و رسالت و مقاصد ایشان، تا چون بحضرت شود سخنش دراز گردد:</p></div>
<div class="b" id="bn17"><div class="m1"><p>حرام دارم با دیگران سخن گفتن</p></div>
<div class="m2"><p>کجا حدیث تو گویم سخن دراز کنم‌</p></div></div>
<div class="n" id="bn18"><p>پس چون بحضرت مناجات رسید مست شراب شوق گشت. سوخته سماع کلام حق شد. آن همه فراموش کرد. نقد وقتش این برآمد که: أَرِنِی أَنْظُرْ إِلَیْکَ.</p></div>
<div class="n" id="bn19"><p>فریشتگان سنگ ملامت در ارادت وی میزدند که: یا ابن النّساء الحیّض! أ تطمع أن تری رب العزة؟ ما للتّراب و لربّ الارباب؟! خاکی و آبی را چه رسد که حدیث قدم کند! لم یکن ثمّ کان را چون سزد که وصال لم یزل و لا یزال جوید! موسی از سرمستی و بیخودی بزبان تفرید جواب می‌دهد که: معذورم دارید که من نه بخویشتن اینجا افتادم.</p></div>
<div class="n" id="bn20"><p>نخست او مرا خواست نه من خواستم. دوست بر بالین دیدم که از خواب برخاستم.</p></div>
<div class="n" id="bn21"><p>من بطلب آتش میشدم که اصطناع پیش آمد که: «وَ اصْطَنَعْتُکَ لِنَفْسِی»، بی‌خبر بودم که آفتاب تقریب برآمد که: «وَ قَرَّبْناهُ نَجِیًّا»:</p></div>
<div class="b" id="bn22"><div class="m1"><p>ز اوّل تو حدیث عشق کردی آغاز</p></div>
<div class="m2"><p>اندر خور خویش کار ما را می‌ساز</p></div></div>
<div class="n" id="bn23"><p>فرمان آمد بفریشتگان که: دست از موسی بدارید که آن کس که شراب «وَ اصْطَنَعْتُکَ لِنَفْسِی» از جام «وَ أَلْقَیْتُ عَلَیْکَ مَحَبَّةً مِنِّی» خورده باشد، عربده کم ازین نکند. موسی در آن حقائق مکاشفات از خم خانه لطف شراب محبت چشید. دلش در هوای فردانیّت بپرید. نسیم انس وصلت از جانب قربت بر جانش دمید. آتش مهر زبانه زد، صبر از دل برمید، بی‌طاقت شد، گفت: أَرِنِی أَنْظُرْ إِلَیْکَ، آخر نه کم از نظری:</p></div>
<div class="b" id="bn24"><div class="m1"><p>گر زین دل سوخته برآید شرری</p></div>
<div class="m2"><p>در دائره ثری نماند اثری‌</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر پیش توام هست نگارا خطری</p></div>
<div class="m2"><p>بردار حجاب هجر قدر نظری‌</p></div></div>
<div class="n" id="bn26"><p>پیر طریقت گفت: هر کس را امیدی، و امید عارف دیدار. عارف را بی‌دیدار نه بمزد حاجت است نه با بهشت کار. همگان بر زندگانی عاشق‌اند و مرگ بر ایشان دشخوار. عارف بمرگ محتاج است بر امید دیدار، گوش بلذت سماع بر خوردار، لب حقّ مهر را وام گزار، دیده آراسته روز دیدار، جان از شراب وجود مستی بی‌خمار:</p></div>
<div class="b" id="bn27"><div class="m1"><p>دل زان خواهم که بر تو نگزیند کس</p></div>
<div class="m2"><p>جان زانکه نزد بی‌غم عشق تو نفس</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تن زانکه بجز مهر توأش نیست هوس</p></div>
<div class="m2"><p>چشم از پی آنکه خود ترا بیند و بس‌</p></div></div>
<div class="n" id="bn29"><p>‌قالَ لَنْ تَرانِی گفته‌اند که موسی آن ساعت که لَنْ تَرانِی شنید، مقام وی برتر بود از آن ساعت که میگفت: أَرِنِی أَنْظُرْ إِلَیْکَ. زیرا که این ساعت در مراد حق بود، و آن ساعت در مراد خود، و بود موسی در مراد حق او را تمامتر بود از بود وی در مراد خود، که این تفرقه است، و آن جمع، و عین جمع لا محاله تمامتر، قالَ لَنْ تَرانِی موسی را زخم لَنْ تَرانِی رسید امّا هم در حال مرهم بر نهاد که و لکن. گفت: ای موسی زخم لَنْ تَرانِی زدیم لکن مرهم نهادیم، تا دانی که که آن نه قهری است، که آن عذری است.</p></div>
<div class="n" id="bn30"><p>فَلَمَّا تَجَلَّی رَبُّهُ لِلْجَبَلِ چون از آیات جلال و آثار عزت احدیت شطیه‌ای بآن کوه رسید بحال نیستی باز شد، و از وی نشان نماند، گفت: پادشاها! اگر سنگ سیاه طاقت این حدیث داشتی، خود در بدو وجود امانت قبول کردی، و بجان و دل خریدار آن بودی.</p></div>
<div class="n" id="bn31"><p>اینجا لطیفه‌ای است که کوه بدان عظیمی برنتافت، و دلهای مستضعفان و پیر زنان امّت احمد برتافت، یقول اللَّه تعالی: وَ أَشْفَقْنَ مِنْها وَ حَمَلَهَا الْإِنْسانُ.</p></div>
<div class="n" id="bn32"><p>وَ خَرَّ مُوسی‌ صَعِقاً چون هستی موسی در آن صعقه از میان برخاست، و بشریت وی با کوه دادند، نقطه حقیقی را تجلّی افتاد که اینک مائیم. چون تو از میان برخاستی ما دیده وریم.</p></div>
<div class="n" id="bn33"><p>پیر طریقت گفت: الهی! یافته میجویم، با دیده‌ور میگویم. که دارم؟ چه جویم؟ که می‌بینم؟ چه گویم؟ شیفته این جست و جویم. گرفتار این گفت و گویم. الهی! بهای عزّت تو جای اشارت نگذاشت، قدم وحدانیت تو راه اضافت برداشت تا گم کرد رهی هر چه در دست داشت، و ناچیز شد هر چه می‌پنداشت. الهی! زان تو میفزود، و زان رهی میکاست، تا آخر همان ماند که اول بود راست:</p></div>
<div class="b" id="bn34"><div class="m1"><p>گفتی کم و کاست باش خوب آمد و راست</p></div>
<div class="m2"><p>تو هست بسی رهیت شاید کم و کاست‌</p></div></div>
<div class="n" id="bn35"><p>فَلَمَّا أَفاقَ قالَ سُبْحانَکَ تُبْتُ إِلَیْکَ چون باهوش آمد، گفت: خداوندا! پاکی از آنکه بشری بنیل صمدیت تو طمع کند، یا کسی بخود ترا جوید، یا دلی و جانی امروز حدیث دیدار تو کند؟ خداوندا! توبه کردم. گفتند: ای موسی؟ چنین بیکبار سپر فرو نهند که نهادی، چنین بیکبار جولان کنند که تو کردی؟ و بدین زودی و آسانی برگشتی؟ و زبان حال موسی می‌گوید:</p></div>
<div class="b" id="bn36"><div class="m1"><p>ارید وصاله و یرید هجری</p></div>
<div class="m2"><p>فأترک ما ارید لما یرید</p></div></div>
<div class="n" id="bn37"><p>چکنم چون مقصودی برنیامد، باری بمحل خدمت و بمقام عجز بندگی باز گردم، و با ابتداء فرمان شوم:</p></div>
<div class="b" id="bn38"><div class="m1"><p>آن کس که بکار خویش سر گشته شود</p></div>
<div class="m2"><p>به زان نبود که با سر رشته شود</p></div></div>
<div class="n" id="bn39"><p>چون بعجز بندگی بمحل خدمت و مقام توبه باز شد، رب العالمین تدارک دل وی کرد، و برفق با وی سخن گفت: یا مُوسی‌ إِنِّی اصْطَفَیْتُکَ عَلَی النَّاسِ بِرِسالاتِی وَ بِکَلامِی یا موسی انی منعتک عن شی‌ء واحد، و هو الرؤیة، فلقد خصصتک بکثیر من الفضائل، اصطفیتک بالرسالة و أکرمتک بشرف الحالة، فاشکر هذه الجملة و اعرف هذه النعمة. وَ کُنْ مِنَ الشَّاکِرِینَ و لا تتعرض لمقام الشکوی، و فی معناه انشدوا:</p></div>
<div class="b" id="bn40"><div class="m1"><p>ان اعرضوا فهم الذین تعطفوا</p></div>
<div class="m2"><p>کم قد وفوا فاصبر لهم ان اخلفوا</p></div></div>