---
title: >-
    ۴ - النوبة الاولى
---
# ۴ - النوبة الاولى

<div class="n" id="bn1"><p>قوله تعالی: قُلْ إِنَّما أَعِظُکُمْ بِواحِدَةٍ بگوی شما را پند میدهم بیک چیز أَنْ تَقُومُوا لِلَّهِ که خیزید خدای را، مَثْنی‌ وَ فُرادی‌ دوگانه و یگانه، ثُمَّ تَتَفَکَّرُوا آن گه با خود بیندیشید و با یکدیگر باز گوئید: ما بِصاحِبِکُمْ مِنْ جِنَّةٍ برین مرد شما هیچ دیوانگی نیست و پوشیده خرد نیست، إِنْ هُوَ إِلَّا نَذِیرٌ لَکُمْ نیست او مگر بیم نمایی شما را، بَیْنَ یَدَیْ عَذابٍ شَدِیدٍ (۴۶) پیش عذابی سخت.</p></div>
<div class="n" id="bn2"><p>قُلْ ما سَأَلْتُکُمْ مِنْ أَجْرٍ بگوی هر چه از شما خواهم از مزد، فَهُوَ لَکُمْ آن شما را باد، إِنْ أَجْرِیَ إِلَّا عَلَی اللَّهِ نیست مزد من مگر بر اللَّه، وَ هُوَ عَلی‌ کُلِّ شَیْ‌ءٍ شَهِیدٌ (۴۷) و او بر همه چیز گواه است.</p></div>
<div class="n" id="bn3"><p>قُلْ إِنَّ رَبِّی بگوی خداوند من، یَقْذِفُ بِالْحَقِّ سخن راست و پیغام پاک می‌افکند، عَلَّامُ الْغُیُوبِ (۴۸) آن دانای نهانها.</p></div>
<div class="n" id="bn4"><p>قُلْ جاءَ الْحَقُّ بگوی پیغام راست آمد از خدای، وَ ما یُبْدِئُ الْباطِلُ وَ ما یُعِیدُ (۴۹) و باطل نه بآغاز چیز تواند و نه بسر انجام.</p></div>
<div class="n" id="bn5"><p>قُلْ إِنْ ضَلَلْتُ بگوی اگر من گم شوم از راه، فَإِنَّما أَضِلُّ عَلی‌ نَفْسِی گمراهی من بر من، وَ إِنِ اهْتَدَیْتُ و اگر بر راه راست روم، فَبِما یُوحِی إِلَیَّ رَبِّی آن بآن پیغام است که خداوند من می‌فرستد بمن إِنَّهُ سَمِیعٌ قَرِیبٌ (۵۰) که او شنوای است بپاسخ از خواننده نزدیک.</p></div>
<div class="n" id="bn6"><p>وَ لَوْ تَری‌ إِذْ فَزِعُوا اگر تو بینی آن گه که بیم زنند ایشان را، فَلا فَوْتَ از دست بشدن را توان نیست، وَ أُخِذُوا مِنْ مَکانٍ قَرِیبٍ (۵۱) و فرا گیرند ایشان را از جایگاهی نزدیک.</p></div>
<div class="n" id="bn7"><p>وَ قالُوا آمَنَّا بِهِ گویند بگرویدیم باللّه، وَ أَنَّی لَهُمُ التَّناوُشُ و چون تواند بود ایشان را فرا چیزی یازیدن، مِنْ مَکانٍ بَعِیدٍ (۵۲) از جایی دور.</p></div>
<div class="n" id="bn8"><p>وَ قَدْ کَفَرُوا بِهِ مِنْ قَبْلُ و کافر شده بودند بایمان پیش از روز مرگ، وَ یَقْذِفُونَ بِالْغَیْبِ مِنْ مَکانٍ بَعِیدٍ (۵۳) و پنداره خویش در آنچه فرا ایشان میگفتند دور می‌انداختند.</p></div>
<div class="n" id="bn9"><p>وَ حِیلَ بَیْنَهُمْ وَ بَیْنَ ما یَشْتَهُونَ جدا کردند میان ایشان و میان آنچه آرزو میکردند، کَما فُعِلَ بِأَشْیاعِهِمْ مِنْ قَبْلُ هم چنان که با هم دینان ایشان کردند از پیش، إِنَّهُمْ کانُوا فِی شَکٍّ مُرِیبٍ (۵۴) که ایشان در گمانی بودند دل را شورنده.</p></div>