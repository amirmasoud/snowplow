---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>کجا کسی که چو او را صبوح دست دهد</p></div>
<div class="m2"><p>یکی قدح به من پیر نیم مست دهد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سماع جان من از نعره ی بلی سازد</p></div>
<div class="m2"><p>می روان من از ساغر الست دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پاش درفتم ارچون پیاله برخیزد</p></div>
<div class="m2"><p>ازآنچه در دل خم سالها نشست دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو، زکهنه و از نو، هرآنچه هست دهم</p></div>
<div class="m2"><p>گرم زباقی دوشین، هرآنچه هست دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهان پرست مشو، می پرست شو زیرا</p></div>
<div class="m2"><p>زمانه داد مرد پی پرست دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشوی دست زنان کسان به آب قدح</p></div>
<div class="m2"><p>که ماهی از پی یک لقمه، جان به شست دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم جهان چه خوری؟زانکه گر به چرخ بلند</p></div>
<div class="m2"><p>رسی، که آخر کارت به خاک پست دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این طریق، سبکبار و تندرست، بهی</p></div>
<div class="m2"><p>از آنکه بارگران، پشت را شکست دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا چون بر همه قادر نمی توانی بود</p></div>
<div class="m2"><p>بسنده باید کردن بدانچه دست دهد</p></div></div>