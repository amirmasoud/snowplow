---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ای به دو چشم نرگسین آفت روزگار من</p></div>
<div class="m2"><p>طرهٔ بی‌قرار تو برده ز من قرار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه خمار وصل تو گشت ملازم سرم</p></div>
<div class="m2"><p>هم به شراب لعل تو، دفع شود خمار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای یمنی ستاره بر آرزوی مه رخت</p></div>
<div class="m2"><p>شرط بود که هر شبی دجله کنی کنار من؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سحری زخون دل، مردمک دو چشم من</p></div>
<div class="m2"><p>اطلس سرخ درکشد بر رخ زرنگار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر زبخار چشم من نم نشدی بر آسمان</p></div>
<div class="m2"><p>هقت فلک بسوختی از دن پر اشرار من</p></div></div>