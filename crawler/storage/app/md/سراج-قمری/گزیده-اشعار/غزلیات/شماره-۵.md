---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>هین در دهید باده که آنها که آگهند</p></div>
<div class="m2"><p>حلقه بگوش این نمط و خاک این رهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ضدند جان و تن، قدح باده دردهید</p></div>
<div class="m2"><p>تا یک دم از مصاحبت خویش وارهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگی زرنگ باده ندیدند خوبتر</p></div>
<div class="m2"><p>آنها که رنگ یافته ی صبغة الللهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز سوی جام دست درازی نمی کنند</p></div>
<div class="m2"><p>آنها که از متاع جهان دست کوتهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش جهان امر، در این جام دیده اند</p></div>
<div class="m2"><p>خلقی که از حقایق اسرار آگهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشن دل اندو، پاک و پگه خیز همچو صبح</p></div>
<div class="m2"><p>کآماده از برای شراب سحر گهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قومی زچشمه ی قدح، آبی نمی خورند</p></div>
<div class="m2"><p>تا لاجرم به خویش فرو رفته چون چهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روشیر گیرشو، زشرابی چو چشم شیر</p></div>
<div class="m2"><p>کاینها زحیله های مزور چو روبهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جامی بخواه غیرت جام جهان نمای</p></div>
<div class="m2"><p>زان ساقی بی که پیشش حوران کله نهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حالی زحور و باده نشین در بهشت نقد</p></div>
<div class="m2"><p>زیرا که در بهشت همین وعده می دهند</p></div></div>