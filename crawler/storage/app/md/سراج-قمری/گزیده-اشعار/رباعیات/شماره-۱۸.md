---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>که رنگم و، باده، لعل چون بیجاده</p></div>
<div class="m2"><p>میلم همه زان بود به سوی باده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که بود قدح چو جانش زمی است</p></div>
<div class="m2"><p>لب بر لب من نهاده و جان داده</p></div></div>