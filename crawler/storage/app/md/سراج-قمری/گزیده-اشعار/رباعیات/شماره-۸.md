---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>از آتش اهل عصر جز دودی نیست</p></div>
<div class="m2"><p>وز هیچ کسم امید بهبودی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی که زجور چرخ بر سر دارم</p></div>
<div class="m2"><p>در دامن هرکه می زنم سودی نیست</p></div></div>