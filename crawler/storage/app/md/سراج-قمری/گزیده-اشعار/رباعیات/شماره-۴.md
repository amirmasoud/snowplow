---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای برده نسیم لطفت از روی گل آب</p></div>
<div class="m2"><p>وی در چمن از شرم رخت گشته گل آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی خوشم آرزوست، بفشان سر زلف</p></div>
<div class="m2"><p>تا خاک عبیر گردد و آب گلاب</p></div></div>