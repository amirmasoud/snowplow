---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>غنچه گر پیش آن دهن خندد</p></div>
<div class="m2"><p>بر بتر جای خویشتن خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شکر خنده گر گشاید لب</p></div>
<div class="m2"><p>مغز در استخوان من خندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهن غنچه گرید از خجلت</p></div>
<div class="m2"><p>راست کان غنچه ی دهن خندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا برآمد نفشه از گل او</p></div>
<div class="m2"><p>سبزه بر برگ نسترن خندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش شمشاد زلف پر شکنش</p></div>
<div class="m2"><p>باغ بر زلف یاسمن خندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از در خنده باشد، ار پس ازین</p></div>
<div class="m2"><p>با رخش لاله در چمن خندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برمن، ار دل ز زلف او طلبم</p></div>
<div class="m2"><p>دلی از زیر هرشکن خندد</p></div></div>