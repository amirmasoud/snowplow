---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>بیا زچهره ی گلگون می، نقاب انداز</p></div>
<div class="m2"><p>به جام چون مه نو، جرم آفتاب انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی زعنبر خط، عود تر در آتش نه</p></div>
<div class="m2"><p>گهی زپسته، نمک در دل کباب انداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بخواهی تا صورت پری ببینی</p></div>
<div class="m2"><p>یکی نظر سوی قارور، حباب انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا به باده کن یک ره و، نکویی کن</p></div>
<div class="m2"><p>که گفته اند:«نکویی کن و به آب انداز»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبند گیسو، در پای چنگ حلقه فکن</p></div>
<div class="m2"><p>زنور می، به سوی دیوغم، شهاب انداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرت بباید تا زلف خود کنی همه پیچ</p></div>
<div class="m2"><p>ز وعده، درشکن زلف خویش، تاب انداز</p></div></div>