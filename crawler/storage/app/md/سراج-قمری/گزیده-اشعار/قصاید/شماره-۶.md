---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>چو باز شد به شکر خنده پستهٔ دهنش</p></div>
<div class="m2"><p>گشاد تنگ شکر طوطی شکر سخنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکند نافه ی خود آهو از حسد بر خاک</p></div>
<div class="m2"><p>به پیش چیندو زلف چو نافهٔ ختنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبهر خدمت قد چو سرو او در باغ</p></div>
<div class="m2"><p>بنفشه وار شود قد سرو، برچمنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر از شکوفه کند نرگس پرآب مرا</p></div>
<div class="m2"><p>رخ چو نسترن و قامت چو نارونش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خط و نقطه بغایت رساند حسن ورا</p></div>
<div class="m2"><p>میان چون خط موهوم و نقطهٔ دهنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شرم گشت سهیل سمن به رنگ ادیم</p></div>
<div class="m2"><p>به پیش نور رخ چون سهیل در یمنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهاب وار دود بر رخم ستارهٔ اشک</p></div>
<div class="m2"><p>مگر به دست کند گیسوی چو اهرمنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چاه، ماه مقنع برآمده‌ست و کنون</p></div>
<div class="m2"><p>میان ماه مقنع نگر چه ذقش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا چو حلقه ی شست است پشت، تا دیدم</p></div>
<div class="m2"><p>که همچو ماهی شیم است در حبال تنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بشست چشم مرا اشک تا سپیدش کرد</p></div>
<div class="m2"><p>بران امید که یابم نسیم پیرهنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تویی که یاسمن زلف تو چو دید بهار</p></div>
<div class="m2"><p>زغم شکست درآمد به زلف یاسمنش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آنکه زلف تو برچشمه حیات توزد</p></div>
<div class="m2"><p>هزار جان بود اندر میان هرشکنش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو سر بتافت زخط چو مشک بی آهوت</p></div>
<div class="m2"><p>به سان نافه ی آهو به خاک برفکنش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ضیاء دولت و دین احمد ابوبکر آنک</p></div>
<div class="m2"><p>بود صفات علی در خلایق حسنش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار فن بودش در هنر که هیچ نظر</p></div>
<div class="m2"><p>ندید عالم پر مکر و فن به هیچ فنش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عجب نباشد اگر چون منش ثنا گوید</p></div>
<div class="m2"><p>که هست سوسن آزاد بنده همچومنش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زشرم سرخ شود چون رخ عقیق یمن</p></div>
<div class="m2"><p>در عدن زسخنهای چون در عدنش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر نه نسر فلک بال در هواش زند</p></div>
<div class="m2"><p>کند زمحور گردون زمانه با بزنش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گمان بری که میان نجوم، خورشید است</p></div>
<div class="m2"><p>دران زمان که ببینی میان انجمنش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بنات وار کند تفرقه به دست چو ابر</p></div>
<div class="m2"><p>زری که جمع کند آفتاب چون پرنش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فراز گردش گردون گرفت مسکن خویش</p></div>
<div class="m2"><p>ازان گزند نباشد زگردش زمنش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قلم زدست قضا عنبرین زبان نشدی</p></div>
<div class="m2"><p>اگر نبودی دریای دست تو وطنش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو شمع رای تو دید این زمردین پنگان</p></div>
<div class="m2"><p>زسینه کرد برون مهر آن زرین لگنش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رهی که خاک تو شد لاله و گل آوردت</p></div>
<div class="m2"><p>اگر نسیم فرستی زخلق خویشتنش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپهر تا زره آب را زر اندوه</p></div>
<div class="m2"><p>کند زماه سپردار و مهر تیغ زنش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرآنکه تخم هوایت نکارد اندر دل</p></div>
<div class="m2"><p>وگرچه طوبی باشد ز بیخ و بن بکنش</p></div></div>