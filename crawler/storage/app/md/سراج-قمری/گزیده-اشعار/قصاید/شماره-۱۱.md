---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>زهی صیت عدلت همه جا گرفته</p></div>
<div class="m2"><p>مقامت محل ثریا گرفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زکلک سیه فرق زر چهره ی تو</p></div>
<div class="m2"><p>جهان جمله لؤلوی لالا گرفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیمت، جهان خوشتر از خلد کرده</p></div>
<div class="m2"><p>علوت مکان برتر از جا گرفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زقدرت، محل، چرخ و انجم فزوده</p></div>
<div class="m2"><p>زذاتت، شرف، دین و دنیا گرفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرشک عدو چون مثالث روان شد</p></div>
<div class="m2"><p>زشنگرف چون آل تمغا گرفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنور تجلی رای منیرت</p></div>
<div class="m2"><p>درت پایه ی طور سینا گرفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دستت درون، تیغ گوهر نگارت</p></div>
<div class="m2"><p>نهنگی است مسکن به دریا گرفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صابون خورشید تا دست شویی</p></div>
<div class="m2"><p>جهان پیشت این طشت مینا گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زسهم شورهای کین تو، آتش</p></div>
<div class="m2"><p>وطن در دل سنگ خارا گرفته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو خورشید تیغی برآورده رایت</p></div>
<div class="m2"><p>به یک دم زدن، عالمی را گرفته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به یاری شمشیر عزمت قضا را</p></div>
<div class="m2"><p>نبینی یکی دشمن نا گرفته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان اقتضا کرد تقویم حکمت</p></div>
<div class="m2"><p>که یا کشته بینی عدو، یا گرفته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ملک سیرتا! کمترین بنده قمری</p></div>
<div class="m2"><p>مه بود از جهان کنج عنقا گرفته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بحمدالله اکنون به فر همایت</p></div>
<div class="m2"><p>چو سیمرغ شد راه صحرا گرفته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نظر بر وی افکن که نیکو نباشد</p></div>
<div class="m2"><p>زچون او غریبی نظر وا گرفته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الا تا بود عقل با آستانت</p></div>
<div class="m2"><p>کم این نهم سقف اعلا گرفته،</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زجیب فلک رای پیرت زبرباد</p></div>
<div class="m2"><p>کفت دامن بخت برنا گرفته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به یک دست زلف نگارین بسوده</p></div>
<div class="m2"><p>به دست دگر جام صهبا گرفته</p></div></div>