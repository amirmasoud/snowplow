---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>نی نی، زهر که هست فروتر، فروترم</p></div>
<div class="m2"><p>خاک رهم، بجز ره ادبار نسپرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه بگوش و روی پر از چین چو سفره ام</p></div>
<div class="m2"><p>زین روی سرگرفته ام و بسته ی زرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دایم ز حرص باده-که خونش حلال باد-</p></div>
<div class="m2"><p>تن جملگی دهان شده مانند ساغرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر من چو خم نبوده ای جمله تن شکم</p></div>
<div class="m2"><p>از دوستی می، نبدی خاک بر سرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا عالمی فروبرم از حرص همچو شام</p></div>
<div class="m2"><p>خون دل و سیاهی روی است در خورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چوگان شده ست هیأت پشتم زحرص آنک</p></div>
<div class="m2"><p>گوی زمین به جملگی آید به کف درم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون عروس رز خوردم و دانم آن مباح</p></div>
<div class="m2"><p>زیرا که همچو بحر برآشفته، کافرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان غم که همچو شمع، زبان آفت من است</p></div>
<div class="m2"><p>در خود فروشده ست تن زرد لاغرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد کرت از سمع احادیث خوشتر است</p></div>
<div class="m2"><p>در بزمگه سماع خوش چنگ دلبرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سرزنش کجا بودم باک از آنکه من</p></div>
<div class="m2"><p>رخ زرد و دل سیاه چو کلک و چو دفترم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در صف روشنان که چو آبند صاف دل</p></div>
<div class="m2"><p>شوریده، تیره حال، چو آبی مکدرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در صومعه کجا بودم راه، تا به طبع</p></div>
<div class="m2"><p>چون راه، خاک پای سگان قلندرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه بابت مساجد و نه لایق کنشت</p></div>
<div class="m2"><p>نه مستحق دار و نه در خورد منبرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاید که گوشه گیرم و رود رکشم از آنک</p></div>
<div class="m2"><p>چون سایه پایمال و چو ذره محقرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من دوستدار صدر جهانم چرا رسد</p></div>
<div class="m2"><p>چون دشمنانش هر نفسی رنج دیگرم؟</p></div></div>