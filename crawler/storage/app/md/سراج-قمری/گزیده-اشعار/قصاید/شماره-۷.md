---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>روزی چو آه خویش، سوی سدره برپرم</p></div>
<div class="m2"><p>با آنکه منتهاست، هم از سدره بگذرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکی است این جهان که به بادی معلق است</p></div>
<div class="m2"><p>بس خاکسارم، ار به جهان سردرآورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردون اشهب است مرا بار گیر خاص</p></div>
<div class="m2"><p>در خاک اگر مراغه کنم، کمتر از خرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چرخ وسمه رنگ به کردار آینه است</p></div>
<div class="m2"><p>زن باشم ار به وسمه و آیینه بنگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردون خراس کهنه ومن، با خران، به طبع</p></div>
<div class="m2"><p>گر گرد این خراس بگردم، برابرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قرص سال خورده این سفره ی کبود</p></div>
<div class="m2"><p>گر من طمع کنم، زسگ زرد، کمترم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرصش خوری است آتش و من گرچه چون تنور</p></div>
<div class="m2"><p>ناری است معده ام نشود قرص او خورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فر همای فضلم و بازی نمی کنم</p></div>
<div class="m2"><p>با آنکه قد خمیده چو طوق کبوترم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچند روشنان فلک مشتی ارزنند</p></div>
<div class="m2"><p>من طوطیم نه گرسنه قمری که در پرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از راه لفظ اگرچه شکرخای طوطیم</p></div>
<div class="m2"><p>لکن زدست غم، نه شکر، زهر می خورم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدار همچو اخترو، روشن دلم ولیک</p></div>
<div class="m2"><p>پیوسته در هبوط و وبال است اخترم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی مثل و روشن و به دمی مرده زنده کن</p></div>
<div class="m2"><p>گویی نه آدمی صفتم، صبح محشرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیمرغ بی نظیر شود هریکی زقدر</p></div>
<div class="m2"><p>برطایران قدسی اگر بال گسترم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بر زمین زمهر دلم ذره یی فتد</p></div>
<div class="m2"><p>از قعر چاه ظلمت سایه ی برون برم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در بحر جایز است تیم که همچو ریگ</p></div>
<div class="m2"><p>لب خشک شد زآتش طبع خوش ترم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچون کمرنبد به زر غیرم احتیاج</p></div>
<div class="m2"><p>من آهنم به گوهر ذاتی توانگرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دستم تهی و پاک و، تنم عوروسر کش است</p></div>
<div class="m2"><p>زان پایدار همچو چنار و صنوبرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آسمان حربا چیزی نیایدم</p></div>
<div class="m2"><p>وزجرم ماه ابرص و خورشید اعورم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آزاده ام چو سرو و مرا سروری رسد</p></div>
<div class="m2"><p>زیرا که بنده زاده ی دستور اکبرم</p></div></div>