---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>دوستی کن که هریک دوست بود</p></div>
<div class="m2"><p>هیچکس در جهانش دشمن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به هم نیست جمع آتش و موم</p></div>
<div class="m2"><p>شب تاریک جمع، روشن نیست</p></div></div>