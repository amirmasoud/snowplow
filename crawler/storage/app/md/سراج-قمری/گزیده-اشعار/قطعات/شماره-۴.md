---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>کوزهٔ دولاب را ماند همی</p></div>
<div class="m2"><p>هرکه زیر چرخ دولابی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز پس اوج و بلندی، حاصلش</p></div>
<div class="m2"><p>سر نگونساری و بی‌آبی بود</p></div></div>