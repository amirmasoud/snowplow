---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>تا پر نشد زبوی محبّت دماغ دل</p></div>
<div class="m2"><p>چون لاله پرده بر نگرفتم ز داغ دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتاده عکس ساقی گلچهره در شراب</p></div>
<div class="m2"><p>گلها شکفته گشت بر اطراف باغ دل</p></div></div>