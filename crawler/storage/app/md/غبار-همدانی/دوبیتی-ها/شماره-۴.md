---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>سر و کارم بدست نازنینی است</p></div>
<div class="m2"><p>که در هر گوشه اش گوشه نشینی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی کش غم بخاطر ره ندارد</p></div>
<div class="m2"><p>چه غم دارد که او را دل غمینی است</p></div></div>