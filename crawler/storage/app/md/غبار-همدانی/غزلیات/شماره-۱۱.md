---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>گریزی نیست از کوی تو ای دوست</p></div>
<div class="m2"><p>چسان برگردم از کوی تو ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا در حلقۀ زلف تو افکند</p></div>
<div class="m2"><p>فریب چشم جادوی تو ای دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فشاندی زلف مشکین را و پُر شد</p></div>
<div class="m2"><p>مشام جانم از بوی تو ای دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکن چندان که خواهی جور بر من</p></div>
<div class="m2"><p>نمیرنجم من از خوی تو ای دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار از هر دوعالم چشم پوشید</p></div>
<div class="m2"><p>نمی بیند بجز روی تو ای دوست</p></div></div>