---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>از آتش دل آهم تا روی پوش برداشت</p></div>
<div class="m2"><p>سیلاب اشک چشمم چون دیگ جوش برداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیبش نمی توان کرد چون دردمند عشقی</p></div>
<div class="m2"><p>از صبر عاجز آمد از دل خروش برداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باری که پشت گردون از هیبتش دوتا شد</p></div>
<div class="m2"><p>آن را به دوش مستی بی تاب و توش برداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افغان ز ‌چشم ساقی کان ترک بی مروت</p></div>
<div class="m2"><p>هم رخت عقل دزدید هم نقد هوش برداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روپوش عیب ما بود پشمینه ای و او را</p></div>
<div class="m2"><p>در رهن ساغری مِی دی مِی فروش برداشت</p></div></div>