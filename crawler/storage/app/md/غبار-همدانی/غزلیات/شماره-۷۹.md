---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>خم می افلاک و جامش آفتاب و ارض عالم میکده</p></div>
<div class="m2"><p>عشق را مِی میشمار و عاشقان را میزده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینۀ عالم چو خالی از بت موزون ماست</p></div>
<div class="m2"><p>مؤمنم میخوان اگر خوانم جهان را بتکده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثبت شد تا نام من در دفتر عشّاق او</p></div>
<div class="m2"><p>غیرت زردشتیم دارد بدل آتشکده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیست آن کو خورده از صهبای عشق آن صنم</p></div>
<div class="m2"><p>آتشی در خانمان و دین و ایمان نازده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه عشق آن پری دیوانه ام دارد چرا</p></div>
<div class="m2"><p>بر سرم از شیخ و شاب و مرد و زن غوغا زده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردش چرخم فسرده دارد ای ساقی بیا</p></div>
<div class="m2"><p>از لب کوثر خاصت بوسۀ گرمی بده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هان غبار اندر طلب عشقت اگر یکره کند</p></div>
<div class="m2"><p>دل نبردی زو که جز او کس نبرده ره به دِه</p></div></div>