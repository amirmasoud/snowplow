---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>فریاد که آتش نهانم</p></div>
<div class="m2"><p>افتاد به مغز استخوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز دل و آتش درونم</p></div>
<div class="m2"><p>افکنده شرر به خانمانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون زورق اگر روم به دریا</p></div>
<div class="m2"><p>هست آتش و آه بادبانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آتش اوفتاده در آب</p></div>
<div class="m2"><p>آوازۀ مرگ شده فغانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج غم تو به سینه دارم</p></div>
<div class="m2"><p>شد خانۀ دل خراب از آنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بحر غم تو بیکران است</p></div>
<div class="m2"><p>من ماهی بحر بیکرانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون جگر است و پارۀ دل</p></div>
<div class="m2"><p>بر سفرۀ عشق آب و نانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاهیده ز بسکه پیر گردون</p></div>
<div class="m2"><p>از درد و بلا تن جوانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مانند کمان شکسته مشتی</p></div>
<div class="m2"><p>بی پاره و خرد استخوانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بازا نفسی که بی تو چون نی</p></div>
<div class="m2"><p>در سینه گره شده فغانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کی خس و خار آشیانه</p></div>
<div class="m2"><p>مهجور کند ز گلستانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقت است که برق خانمان سوز</p></div>
<div class="m2"><p>آید به طواف آشیانم</p></div></div>