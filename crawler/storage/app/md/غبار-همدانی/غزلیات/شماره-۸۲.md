---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>ای سلسله زلفت سرمایۀ رسوایی</p></div>
<div class="m2"><p>باز آی که رسوا کرد ما را دل شیدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی سلسلۀ زلفت دانی که چها کردست</p></div>
<div class="m2"><p>تاریک شب هجران با این سر سودایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سرو سهی قامت وقت است که از بستان</p></div>
<div class="m2"><p>بخرامی و بنمایی بر سرو دلارایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کوکب بخت خود تا روز همی جنگم</p></div>
<div class="m2"><p>بی مِهر رخت هر شب در گوشۀ تنهایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر طلعت خویش از زلف گر پرده همی پوشی</p></div>
<div class="m2"><p>باید که بپوشی چشم از چشم تماشایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخسارۀ رنگینت خونخواره نمی خواهد</p></div>
<div class="m2"><p>خون دل ما تا چند از دیده بپالایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز دل مجنونم آرام نمیگیرد</p></div>
<div class="m2"><p>تا از خَم زلفینت یک سلسله نگشایی</p></div></div>