---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>به کامم زهر شد تریاق هستی</p></div>
<div class="m2"><p>به مِی ساقی بشوی اوراق هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود ار نیستی پایان هر هست</p></div>
<div class="m2"><p>نگشتی هیچ کس مشتاق هستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر دستم نبستی رشتۀ مهر</p></div>
<div class="m2"><p>شکست افکندمی بر طاق هستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهان در جوهر عشق آتشی هست</p></div>
<div class="m2"><p>که تاثیر وی است احراق هستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا با دوست پیمان محبت</p></div>
<div class="m2"><p>بود محکمتر از میثاق هستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نَرد نیستی تا باختم عشق</p></div>
<div class="m2"><p>گرو بردم ز جفت و طاق هستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلیل راه عشق آمد وگرنه</p></div>
<div class="m2"><p>ندارد عقل استحقاق هستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر همت نبودی همره خاک</p></div>
<div class="m2"><p>نگشتی قابل اشراق هستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غبار از بحر حیرت کی برآید</p></div>
<div class="m2"><p>که سر تاپاست استغراق هستی</p></div></div>