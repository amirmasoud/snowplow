---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>فکندم رخت در میخانۀ عشق</p></div>
<div class="m2"><p>کشیدم دُردی از پیمانۀ عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بحر اشک خونین غوطه خوردم</p></div>
<div class="m2"><p>ربودم گوهر یکدانۀ عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخوان زاهد به فردوسم که الحق</p></div>
<div class="m2"><p>ز قصر خلد بِه ویرانۀ عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زنجیر مِی از راهم بگردان</p></div>
<div class="m2"><p>که رسوا می شود دیوانۀ عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو واعظ که در یک سر نگنجد</p></div>
<div class="m2"><p>حدیث عقل با افسانۀ عشق</p></div></div>