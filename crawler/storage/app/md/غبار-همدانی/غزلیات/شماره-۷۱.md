---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>چشم اگر پوشی ز کار خویشتن</p></div>
<div class="m2"><p>لطف ها بینی ز یار خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غربت مردم به شهر مردم است</p></div>
<div class="m2"><p>من غریبم در دیار خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش در بزمی که بودی با رقیب</p></div>
<div class="m2"><p>آزمودم اعتبار خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار سنگین است جان در راه دوست</p></div>
<div class="m2"><p>ما سبک کردیم بار خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد ازین گو دیگری با من مساز</p></div>
<div class="m2"><p>ساختم با کردگار خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک گر گردی به راه او غبار</p></div>
<div class="m2"><p>کیمیا بینی غبار خویشتن</p></div></div>