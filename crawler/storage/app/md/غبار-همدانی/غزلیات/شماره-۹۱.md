---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>نباشم راحت از دستت چه در خواب و چه بیداری</p></div>
<div class="m2"><p>به هرجا رو کنم آنجا تو دست سلطنت داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراپای وجود خود بسی در سال و مه گشتم</p></div>
<div class="m2"><p>به غیر از تو به ملک دل ندیدم هیچ دیّاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم دل چه من دیدم دل موری عیان دیدم</p></div>
<div class="m2"><p>سلیمانی در او اندر به کار مملکت داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اشراقات انوار جمالش محو و هم هیچم</p></div>
<div class="m2"><p>بمانند سراجی کش بر خورشید بگذاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی باقی بود یارب که زلف آن مه نخشب</p></div>
<div class="m2"><p>نگیرد بند و زنجیرش به صد نیرنگ و طراری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن از می پرستی مَنعم ای زاهد که درعالم</p></div>
<div class="m2"><p>فکنده نرگس مست نگارم طرز هشیاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو روی خود در آئینه ببیند آن جفا گستر</p></div>
<div class="m2"><p>غبار از سوز دل گردید هم چون ابر آزاری</p></div></div>