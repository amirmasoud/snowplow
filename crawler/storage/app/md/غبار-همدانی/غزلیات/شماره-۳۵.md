---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>هرکه دست از جان نشوید کی ز جانان کام جوید</p></div>
<div class="m2"><p>وان که در آتش نسوزد کی ز آب آرام جوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق و آرام دل هیهات هیهات این بلا کش</p></div>
<div class="m2"><p>کی به یاد خویشتن پرداخت تا آرام جوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل پناه از جور گردونم به جانان برده هِی هِی</p></div>
<div class="m2"><p>آهوی مسکین امان از شیر خون آشام جوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقی را خاک بر سر کن که وصل و کام خواهد</p></div>
<div class="m2"><p>عارفی را ننگ عارف دان که ننگ و نام جوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مَنعم از زلفش مکن زاهد که چون صید بلاکش</p></div>
<div class="m2"><p>عاشق صیّاد شد پیوسته حلقۀ دام جوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شکنج زلف جانان دل به رویش گشت مایل</p></div>
<div class="m2"><p>صبح گم کردست او را در میان شام جوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حجاب زلف بیرون آ که این بیچاره عاشق</p></div>
<div class="m2"><p>بگسلد زُنّار و بیزاری از این اصنام جوید</p></div></div>