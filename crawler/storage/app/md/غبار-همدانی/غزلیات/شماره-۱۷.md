---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>صبا از زلف مُشکین عُقده وا کرد</p></div>
<div class="m2"><p>مرا سرگشته چون باد صبا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنازم طعنۀ بیگانگان را</p></div>
<div class="m2"><p>که آخر با تو ما را آشنا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون آن شاخ مرجان را توان یافت</p></div>
<div class="m2"><p>که در خون مردم چشمم شنا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیامت قامت من تا به پا خاست</p></div>
<div class="m2"><p>قیامتها از آن قامت به پا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبارا غارت دین و دل ما</p></div>
<div class="m2"><p>خدا کِی بر نکورویان روا کرد؟</p></div></div>