---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>ساقیا می ده که تا ما عاقلیم</p></div>
<div class="m2"><p>در فنون عشقبازی جاهلیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه غافل خفته وقت کشت و کار</p></div>
<div class="m2"><p>گاه محصول است و ما بیحاصلیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذت هستی به مستی حاصل است</p></div>
<div class="m2"><p>ما به کلی زین دو معنی غافلیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر امید زخم دیگر زنده ایم</p></div>
<div class="m2"><p>ورنه از زخم نخستین بسملیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو باغ خلد بودیم و کنون</p></div>
<div class="m2"><p>بر لب جوی جهان پا در گلیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلعت جانان ز جان محجوب نیست</p></div>
<div class="m2"><p>ما میان جان و جانان حایلیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بلا غافل نشیند تنگ دل</p></div>
<div class="m2"><p>ما گروه عاشقان دریا دلیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موج طوفان بلا از سر گذشت</p></div>
<div class="m2"><p>ما چنان فارغ که اندر ساحلیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بار بگشودند همراهان و ما</p></div>
<div class="m2"><p>هم در اول گام و اول منزلیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهر رخشانیم لیکن چون خلیل</p></div>
<div class="m2"><p>رو به ما آرد غبارا آفلیم</p></div></div>