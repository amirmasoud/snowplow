---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>نگار من ز شبستان به در نمی‌آید</p></div>
<div class="m2"><p>زمان گل مگر آخر به سر نمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار می‌گذرد ساقیا تعلل چیست</p></div>
<div class="m2"><p>مگر خزان دو سه روز دگر نمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شد که لاف کلیمی نمی‌زند بلبل</p></div>
<div class="m2"><p>ز شاخ گل مگر آتش به در نمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشت عمر عزیزی که بود دولت وصل</p></div>
<div class="m2"><p>ولی زمان جدایی به سر نمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه بود هم‌آواز من چه شد امشب</p></div>
<div class="m2"><p>به گوش نالهٔ مرغ سحر نمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشم ز صومعه دیگر به سوی میکده رخت</p></div>
<div class="m2"><p>که بوی خیر ازین بام و در نمی‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی ز منزل جانان خبر به ما نرساند</p></div>
<div class="m2"><p>که هر که می‌رود از وی خبر نمی‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه نخل مرادست سرو قامت دوست</p></div>
<div class="m2"><p>دریغ و درد که هرگز به بر نمی‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غمین مباش غبارا که عیب بی‌هنران</p></div>
<div class="m2"><p>به چشم مردم صاحب نظر نمی‌آید</p></div></div>