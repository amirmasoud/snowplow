---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>تو را بر سر از فقر افسر نباشد</p></div>
<div class="m2"><p>گرت خاک میخانه بر سر نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ظلمات عشق آب حیوان نیابی</p></div>
<div class="m2"><p>گرت خضر فرخنده رهبر نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بحری فرو برده ام سر کز آنجا</p></div>
<div class="m2"><p>توان سر بر آورد  اگر سر نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سمندر عجب گر در آخر بسوزد</p></div>
<div class="m2"><p>مگر در دلش مهر آذر نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا در صف عاشقان سر بر آری</p></div>
<div class="m2"><p>گرت بر سر از خاک افسر نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو عیسی به گردون رود مرد سالک</p></div>
<div class="m2"><p>گرش در دل اندیشۀ خر نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پای تو جانی است خواهم سپردن</p></div>
<div class="m2"><p>مرا با تو سودای دیگر نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبارا از این بحر نتوان برون شد</p></div>
<div class="m2"><p>به کشتی گرت صبر لنگر نباشد</p></div></div>