---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>ز جان بیزارم از دست دل خویش</p></div>
<div class="m2"><p>خدایا با که گویم مشکل خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل من خار غم در پا ندارد</p></div>
<div class="m2"><p>که چندان فارغست از بلبل خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دریای غمت نازم که بازم</p></div>
<div class="m2"><p>به قعر خویش برد از ساحل خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من می ندانم مایل کیست</p></div>
<div class="m2"><p>که هیچش می نبینم مایل خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دی از پروانۀ وصل تو تا صبح</p></div>
<div class="m2"><p>شدم از شوق شمع محفل خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مرغ بیضه ضایع کرده دایم</p></div>
<div class="m2"><p>دلم گیرد کنار از منزل خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندانم آخر این صیّاد بی رحم</p></div>
<div class="m2"><p>چرا پوشید چشم از بسمل خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا تا چند کاری تخم هستی</p></div>
<div class="m2"><p>به باد نیستی ده حاصل خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهل تا اوفتان خیزان بیاید</p></div>
<div class="m2"><p>غبار خسته ره با محمل خویش</p></div></div>