---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>تا کی رودم خون دل از هر مژه چون جوی</p></div>
<div class="m2"><p>تا بو که تو چون سرو خرامان بِبَرآئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیمار غمت جان به لب و دل نگران است</p></div>
<div class="m2"><p>شاید که به آئین طبیبان بدر آئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من شمع صفت گریه کنان جان دهم از شوق</p></div>
<div class="m2"><p>چون صبح تو گر با لب خندان ببر آئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شک نیست که جمعیت خاطر دهدم دست</p></div>
<div class="m2"><p>آن شب که تو با زلف پریشان ببر آئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانه صفت جامۀ جان پیش تو سوزم</p></div>
<div class="m2"><p>چون شمع اگر ای کوکب رخشان ببر آئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشکل بتوان زیست به هجران تو هر چند</p></div>
<div class="m2"><p>باور نتوان کرد که آسان ببرآیی</p></div></div>