---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>هرکه بیند عکس ساقی را به جام</p></div>
<div class="m2"><p>از می تلخش نگردد تلخ کام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نمیدانی که تیر انداز کیست</p></div>
<div class="m2"><p>لاجرم از زخم مینالی مدام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چراغ عقل نبود آن فروغ</p></div>
<div class="m2"><p>کآدمی را وارهاند از ظلام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم مگر خورشید عشق آرد به روز</p></div>
<div class="m2"><p>یا فروغ جام این تاریک شام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من دوای درد خود دانسته ام</p></div>
<div class="m2"><p>از کف ساقی شراب لعل فام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهباز دست شه بودم که بست</p></div>
<div class="m2"><p>حلقۀ زلف تو پایم چون حمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوسه و دشنام را یک یک بده</p></div>
<div class="m2"><p>تا بدانم زان دو شیرین تر کدام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شِکّر از نِی کسب شیرینی نکرد</p></div>
<div class="m2"><p>چاشنی از لعل جانان کرده وام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر نزد هرگز به سعی باغبان</p></div>
<div class="m2"><p>سروی از بستان بدینسان خوشخرام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز مِی مردافکن عشق غیور</p></div>
<div class="m2"><p>عقل سرکش را که خواهد کرد رام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باده میباید بدین شکرانه خورد</p></div>
<div class="m2"><p>که به زاهد شد می گلگون حرام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساقیا زان شیشه ام جامی بیار</p></div>
<div class="m2"><p>تا زنم بر سنگ شیشۀ ننگ و نام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می فروش از ذوق می آگه تر است</p></div>
<div class="m2"><p>هیچ کس جز جم نداند سرّ جام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می ندانم وصل و هجران از چه روست</p></div>
<div class="m2"><p>آنقدر ناپایدار و مستدام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاشقان را گر نبود امید وصل</p></div>
<div class="m2"><p>عمر را با هجر کی بودی دوام؟</p></div></div>