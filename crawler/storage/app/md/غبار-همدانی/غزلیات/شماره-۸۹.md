---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>هلالم ز ابرو بتا تا نمودی</p></div>
<div class="m2"><p>دل از دست دیوانۀ خود ربودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشد گر به بتخانه نقشت، مصّور</p></div>
<div class="m2"><p>به یکدفعه بت ها کنندت سجودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم چرا شش جهت شد معطر</p></div>
<div class="m2"><p>سحر شانه تا گیسوان را نمودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فتراک بستی دلم همچو آهو</p></div>
<div class="m2"><p>چو عقده ز گیسوی مشکین گشودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نماید به رویت دو زلف پریشان</p></div>
<div class="m2"><p>چو پیچیده دودی به مجمر ز عودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سوز نهانی ز عشق تو اینک</p></div>
<div class="m2"><p>لب خشک و چشم تر من شهودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نقشت قلم زد چو نقاش قدرت</p></div>
<div class="m2"><p>ز نقشت همی خویشتن را ستودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزاران هزار آفرین صانعی را</p></div>
<div class="m2"><p>که داد از عدم چون تویی را وجودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بیداریش چون نیابی غباری</p></div>
<div class="m2"><p>هماره چو چشم خمارش غنودی</p></div></div>