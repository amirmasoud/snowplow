---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>ای مطرب دل ترسم زین پرده که بنوازی</p></div>
<div class="m2"><p>از پرده برون رازم یکباره بیندازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساقی جان جامی بر می زده ای عطشان</p></div>
<div class="m2"><p>از بهر یکی جرعه تا چند همی نازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کثرت مهر تو وز صرصر قهر تو</p></div>
<div class="m2"><p>چون شمع ستادستم آمادۀ جانبازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عشق تو چون موسی دل گشته مرا یاور</p></div>
<div class="m2"><p>تا آتشی از رویت در طور دل اندازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خسرو مهرویان از کثرت مشتاقان</p></div>
<div class="m2"><p>بر حال من مسکین ترسم که نپردازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظلمی که ز چشمانت وارد به دلم آمد</p></div>
<div class="m2"><p>بر کبک دری نامد از پنجۀ شهبازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سوختۀ هجران عشق تو کند پنهان؟</p></div>
<div class="m2"><p>کش روی غبار آلود دارد سر غمازی</p></div></div>