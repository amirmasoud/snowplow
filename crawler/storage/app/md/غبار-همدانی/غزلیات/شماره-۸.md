---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>خروشی دوش از میخانه برخاست</p></div>
<div class="m2"><p>که هوش از عاقل و فرزانه برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مُغان خشت از سر خُم برگرفتند</p></div>
<div class="m2"><p>خروش از مردم میخانه برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروغ روی ساقی در مِی افتاد</p></div>
<div class="m2"><p>زبانۀ آتش از پیمانه برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس با آشنایان جور کردی</p></div>
<div class="m2"><p>فغان از مردم بیگانه برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان زنجیر گیسو تاب دادی</p></div>
<div class="m2"><p>که فریاد از دل دیوانه برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پی افروختن چون شمع بنشست</p></div>
<div class="m2"><p>برای سوختن پروانه برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا ساقی بیاور کشتی مِی</p></div>
<div class="m2"><p>که طوفان غم از کاشانه برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب نبود زتاب جوشش مِی</p></div>
<div class="m2"><p>گر از خُم نعرۀ مستانه برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مرغ دل شکنج دام او دید</p></div>
<div class="m2"><p>نخست از روی آب و دانه برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غبارا هر که دست از جان بشوید</p></div>
<div class="m2"><p>تواند از پی جانانه برخاست</p></div></div>