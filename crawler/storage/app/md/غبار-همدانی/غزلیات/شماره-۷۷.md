---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>از تغافل ساقی سرمست بی پروای من</p></div>
<div class="m2"><p>خون دل ریزد بجای باده در مینای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفلسان را گر مِی و مطرب نباشد گو مباش</p></div>
<div class="m2"><p>سینۀ من بربط من اشک من صهبای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد هزاران سرو را پامال خاک ره کند</p></div>
<div class="m2"><p>چون خِرامد در چمن سرو سهی بالای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا صاف ار نداری دُردِئی کز تاب دود</p></div>
<div class="m2"><p>خون دل پالوده دارد چشم خون پالای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نگریم از غمش خود از تبسم بشکند</p></div>
<div class="m2"><p>آن لب چون لعل نرخ لؤلؤ لالای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سالها دهقان قدرت بوستانبانی کند</p></div>
<div class="m2"><p>تا تماشا را به باغ آرد سمن سیمای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لامکان پیماست رخش همّتم لیکن چه سود</p></div>
<div class="m2"><p>لنگ شد در سنگلاخ غم جهانپیمای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقبت دانم که در میدان جانبازی عشق</p></div>
<div class="m2"><p>در سراندازی سر اندازد مرا سودای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلب دل در بوتۀ هجران سیه گردید و نیست</p></div>
<div class="m2"><p>کیمیای وصل جانان را جویی پروای من</p></div></div>