---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>اگر دانستمی آیین آن زلف پریشان را</p></div>
<div class="m2"><p>ز پا بگشودمی یکباره قید کفر و ایمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سرگردانی دل پی بدان زلف سیه بردم</p></div>
<div class="m2"><p>که چون بیننده گویی دید غلطان یافت چوگان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث توبه زاهد با خمار آلودگان کم گو</p></div>
<div class="m2"><p>که بس بستند و نتوانند محکم داشت پیمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنزدیکی نگار خویش را در بر نمی بینم</p></div>
<div class="m2"><p>نبیند در درون دیده مردم چشم انسان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بلبل ز بال افشانی مرغ سحر خون شد</p></div>
<div class="m2"><p>که میدانست توام صبح وصل و شام هجران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو سیرابی، ترا امواج دریا وحشت افزاید</p></div>
<div class="m2"><p>درون تشنه داند لذت طغیان طوفان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خریداران تهی دستند زآن ترسم که نیکویان</p></div>
<div class="m2"><p>متاع حسن برچینند و بربندند دکّان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبار از عشق دارد گنجی اندر دل نهان ساقی</p></div>
<div class="m2"><p>خرابش ساز تا پیدا کند آن گنج پنهان را</p></div></div>