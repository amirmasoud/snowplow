---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>مطرب دلم  ز پرده به در میرود بگو</p></div>
<div class="m2"><p>ساقی بیا که آتش عشقم به جان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدی دلا که شعلۀ جوالاهه فراق</p></div>
<div class="m2"><p>مانند نقطه عاقبتم در میان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس که سوخت کوکب بختم در آسمان</p></div>
<div class="m2"><p>ظلمت فضای خانه کرّوبیان گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوبان به مهر دلشدگان را کنند اسیر</p></div>
<div class="m2"><p>کِی مُلک دل به قوّت بازو توان گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانه را وصال نماید شب فراق</p></div>
<div class="m2"><p>تا شمع را ز سوز من آتش به جان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما در پناه پیر مغانیم گو بگیر</p></div>
<div class="m2"><p>گر گرگ گوسفند ز دست شُبان گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز نبرد ره به سرا بوستان گل</p></div>
<div class="m2"><p>الّا کسی که الفت با باغبان گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارف شناخت قدر خموشی از آن که دید</p></div>
<div class="m2"><p>آتش به جان شمع ز دست زبان گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کِی اسیر غول بیابانی ای غبار</p></div>
<div class="m2"><p>آنکس بریده ره که پی کاروان گرفت</p></div></div>