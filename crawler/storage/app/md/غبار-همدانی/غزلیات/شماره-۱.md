---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>دلم دارد بدان زلف چلیپا</p></div>
<div class="m2"><p>همان الفت که با زنّار ترسا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره از کار مجنون کی گشاید</p></div>
<div class="m2"><p>کسی کو عقده زد بر زلف لیلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی تند است و سرکش آتش عشق</p></div>
<div class="m2"><p>ولیکن خار از او ترسد نه خارا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندیدم هرگز این آشفته دل را</p></div>
<div class="m2"><p>مگر در بند آن زلف چلیپا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی شد شیخ و آن دیگر برهمن</p></div>
<div class="m2"><p>که دارد عشق در سرها اثرها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبودی کوه کندن کار فرهاد</p></div>
<div class="m2"><p>گرش شیرین نبودی کارفرما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو لا خواهی شدن مگذار مگذار</p></div>
<div class="m2"><p>درون خانۀ دل غیر الّا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر مشتاق صاحب خانه باشی</p></div>
<div class="m2"><p>ندارد فرق مسجد با کلیسا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان خرگاه لیلی سایه افکند</p></div>
<div class="m2"><p>که مجنون گم شد اندر راه صحرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سری را کآتش عشق است در دل</p></div>
<div class="m2"><p>نمی گنجد عِقال عقل برپا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی کز تیشه کوه از پا فکندی</p></div>
<div class="m2"><p>فکندش تیشۀ عشق تو از پا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلا سستی مکن در خوردن غم</p></div>
<div class="m2"><p>که زین دارو توانی شد توانا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غبارا زین میان برخیز برخیز</p></div>
<div class="m2"><p>که با خود می نشاید بود و با ما</p></div></div>