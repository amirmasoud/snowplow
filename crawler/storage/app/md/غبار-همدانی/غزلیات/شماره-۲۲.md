---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>به کوی میکده دی هاتفی بشارت برد</p></div>
<div class="m2"><p>که فضل حق گنه مِی کشان به غارت برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم ز پیر خرابات شکرها دارد</p></div>
<div class="m2"><p>که رنجها پی تعمیر این عمارت برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنگ توبه شد آلوده خرقۀ صوفی</p></div>
<div class="m2"><p>بکوی باده فروشش پی قصارت برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر ز بوی قدح تر نکرده شیخ دماغ</p></div>
<div class="m2"><p>که نام درد کشان را بدین حقارت برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکوی میکده گر دل مقیم شد چه عجب</p></div>
<div class="m2"><p>که اجرهای فراوان ازین زیارت برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نداشت در دل شه چون غم رعیت راه</p></div>
<div class="m2"><p>دل شکستۀ ما را به استعارت برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا به راه طلب چست کرد مرشد عشق</p></div>
<div class="m2"><p>که هر چه داشتم از کف به یک اشارت برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدین عطیه چه شکر آورم که مردم چشم</p></div>
<div class="m2"><p>مرا به میکدۀ عشق با طهارت برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به راه عشق تو ساقی مرا سبک رو کرد</p></div>
<div class="m2"><p>که هر چه داشتم از جرعه ای به غارت برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدست عشق ده ای دل عنان خویش و مترس</p></div>
<div class="m2"><p>که او به هر طرفت برد با بصارت برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غبار از خم چوگان عشق گوی مراد</p></div>
<div class="m2"><p>به صبر برد ولیکن به صَد مَرارت برد</p></div></div>