---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>دلی که در خم زلف بتی اسیر نباشد</p></div>
<div class="m2"><p>عجب مدار که بر ملک تن امیر نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان گسسته ام از ما سوا علاقۀ الفت</p></div>
<div class="m2"><p>که غیر زلف توام هیچ دستگیر نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا مگوی چرا پند عاقلان نپذیری</p></div>
<div class="m2"><p>که غیر عشق توام هیچ دلپذیر نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کُشی و زنده کنی ورنه چون تو هیچ ستمگر</p></div>
<div class="m2"><p>به قتل بی گنهان اینقدر دلیر نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار مرتبه داری زمن گریز ولیکن</p></div>
<div class="m2"><p>چو نیک بنگرم از تو مرا گریز نباشد</p></div></div>