---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>گر به دنبال تو یک ناله ام از دل خیزد</p></div>
<div class="m2"><p>ناله جای جرس از ناقه و محمل خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز ای دل که در این قافله امشب من و تو</p></div>
<div class="m2"><p>نگذاریم که افغال ز جلاجل خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفروشم به دو صد زمزمۀ چنگ و رباب</p></div>
<div class="m2"><p>ناله ای راکه شب هجر تو از دل خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق را شیفته ای باید جان بر کف دست</p></div>
<div class="m2"><p>این نه کاریست که از مردم عاقل خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه در مزرع دل تخم وفا بتوانی</p></div>
<div class="m2"><p>رو بیفشان که ازین مزرعه حاصل خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون هما مرغ دلم از سر عالم برخاست</p></div>
<div class="m2"><p>از سر کوی تو ای مه به چه مشکل خیزد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن آلوده به خون دل بلبل بینی</p></div>
<div class="m2"><p>در گلستان جهان هر گلی از گل خیزد</p></div></div>