---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>هان ای مُغَنّی صبح شد برخیز و چنگی ساز کن</p></div>
<div class="m2"><p>ناخن براندامش بزن وز خواب چشمش باز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخاست مرغ صبح خوان برداشت نوبت را فغان</p></div>
<div class="m2"><p>از خواب مستی خیز هان برگ صبوحی ساز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی بهنگام صبوح آن جام راحت بخش را</p></div>
<div class="m2"><p>پیش آر از دوران نوح، انجام را آغاز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچون مسیحا رخت خود بر گردۀ گردون بنه</p></div>
<div class="m2"><p>وین زنگ پر آهنگ را از گردن خر باز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شام فراق ار پیش شد صبح وصال آمد زپِی</p></div>
<div class="m2"><p>برخیز و ساز راز دل با آن بت طنّاز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خرقۀ زهد و ورع زنّار نتوان داشتن</p></div>
<div class="m2"><p>یا سُبحه از کف باز نِه یا رشته ای ابراز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چند سازی در جهان با خار و خاشاک آشیان</p></div>
<div class="m2"><p>ای مرغ لاهوتی مکان بر اوج خود پرواز کن</p></div></div>