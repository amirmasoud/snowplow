---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>بغیر از باده داروی طرب چیست</p></div>
<div class="m2"><p>بیا ساقی تعلّل را سبب چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به می ده هرچه داری تا بدانی</p></div>
<div class="m2"><p>به گیتی حاصل رنج و تعب چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گره بگشا ز ابرو چون زدی تیر</p></div>
<div class="m2"><p>به صید بسمل این قهر و غضب چیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که خواند از نقش موجودات حرفی</p></div>
<div class="m2"><p>که میداند که چندین بوالعجب چیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیبان درد بی درمان پسندند</p></div>
<div class="m2"><p>به تاب عشق باید سوخت تب چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بیدردی طبیبان را چه جرم است</p></div>
<div class="m2"><p>تو در خوابی گناه مرغ شب چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پا تا نشکنی خار مغیلان</p></div>
<div class="m2"><p>چه دانی ذوق صحرای طلب چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبار از حسرت آن لعل لب دوش</p></div>
<div class="m2"><p>به دندان جان فشردی دست و لب چیست</p></div></div>