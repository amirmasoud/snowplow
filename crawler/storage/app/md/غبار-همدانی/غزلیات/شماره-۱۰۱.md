---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>سفالین خُمُّ و در وی لعلگون می</p></div>
<div class="m2"><p>کبدر فی الدجی والشّمس فی فِی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زجاجی جام بین کز عهد جمشید</p></div>
<div class="m2"><p>گذر ننموده سنگ فتنه بر وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشاید فرق کرد از غایت لطف</p></div>
<div class="m2"><p>که می در جام یا جام است در می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در او نشکسته دور چرخ گردون</p></div>
<div class="m2"><p>حبابی را که آورد از جَم و کِی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیابانی است در پیشم خطرناک</p></div>
<div class="m2"><p>که در وی خنگ گردون افکند پی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیاسایم در او هر چند بر من</p></div>
<div class="m2"><p>سر آید روزگار بهمن و دی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر صد باره عمر من سر آید</p></div>
<div class="m2"><p>دگر ره نفخۀ عشقم کند حی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن گم کرده پی دارم سراغی</p></div>
<div class="m2"><p>که هی بر اسب همت میزنم هی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان خالی ز مجنون است ورنه</p></div>
<div class="m2"><p>ز لیلی نیست خالی هرگز این حی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه گوشم که خواند مطرب غیب</p></div>
<div class="m2"><p>به راه راستم با نالۀ نی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا ساقی بیا تا دست شوئیم</p></div>
<div class="m2"><p>درین سرچشمه من از جان و تو از می</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غبارا از میان برخیز و برخیز</p></div>
<div class="m2"><p>که با خود می نشاید بود و با وی</p></div></div>