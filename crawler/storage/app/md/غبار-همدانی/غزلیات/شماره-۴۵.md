---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>حدیث روضۀ رضوان و نار نیرانش</p></div>
<div class="m2"><p>حکایتی است ز اوضاع وصل و هجرانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بریز سیل سرشکم که جان به در نبرد</p></div>
<div class="m2"><p>هزار کشتی نوح از بلای طوفانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خضر راه شو ای عشق تا در این دم مرگ</p></div>
<div class="m2"><p>رسانی از ظلماتم به آب حیوانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علاج این دل دیوانه را توانم کرد</p></div>
<div class="m2"><p>به دست افتد اگر طرّۀ پریشانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا متاع گرانمایه ایست گوهر عمر</p></div>
<div class="m2"><p>ولی چه سود که ما میدهیم ارزانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی نمانده که یکباره برطرف گردد</p></div>
<div class="m2"><p>سحاب چشم من از بس که ریخت بارانش</p></div></div>