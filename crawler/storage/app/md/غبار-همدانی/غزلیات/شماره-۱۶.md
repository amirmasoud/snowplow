---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>بر زلف تو تا باد صبا را گذر افتاد</p></div>
<div class="m2"><p>بس نافۀ چین بر سر هر رهگذر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس یوسف دل دید در آن چاه زنخدان</p></div>
<div class="m2"><p>دیوانه دلم بر سَر آنان به سر افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را ز سر زلف تو این شیوه خوش آمد</p></div>
<div class="m2"><p>کاشفته چو ما بر سر و پای تو در افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک حلقه از آن زلف گره گیر گشودند</p></div>
<div class="m2"><p>صد عُقده به کار من بی پا و سر افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگر خبر مردم هشیار نپرسد</p></div>
<div class="m2"><p>آن مست که در کوی مغان بی خبر افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا زلف تو در دست نسیم سحر افتاد</p></div>
<div class="m2"><p>کار من سودا زده زیر و زبر افتاد</p></div></div>