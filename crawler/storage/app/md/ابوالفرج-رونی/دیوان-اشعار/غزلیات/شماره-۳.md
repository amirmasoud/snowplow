---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>چه دلبری چه عیاری چه صورتی چه نگاری</p></div>
<div class="m2"><p>به گاه خلوت جفتی به گاه عشرت یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غمزه عقل گدازی به چنگ چنگ نوازی</p></div>
<div class="m2"><p>به وعده روبه بازی به عشق شیر شکاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بوی خواهم رنگی چو صلح جویم جنگی</p></div>
<div class="m2"><p>چو راست رانم لنگی چه خوست اینکه تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شگفت یوسف روئی چرا نه یوسف خوئی</p></div>
<div class="m2"><p>یکی قرینه اوئی ولیک گرگ تباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه سائی و نه بسودی نه کاهی و نه فزودی</p></div>
<div class="m2"><p>نه بندی و نه گشودی چه دیو دست سواری</p></div></div>