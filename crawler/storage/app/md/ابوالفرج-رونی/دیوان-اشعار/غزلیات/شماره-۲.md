---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>بیامدی صنما بر دو پای بنشستی</p></div>
<div class="m2"><p>دلم ز دست برون کردی و به در جستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه مست بودی و پنداشتم که چون مستان</p></div>
<div class="m2"><p>همی به حیله شناسی بلندی از پستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سه روز شد پس از آن تا ز درد فرقت تو</p></div>
<div class="m2"><p>نه هوشیاری دانم که چیست نه مستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درست گشت که جان منی بدان معنی</p></div>
<div class="m2"><p>که تا زمن بگسستی به من نه پیوستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان جانان گر تو به دست خویش دلم</p></div>
<div class="m2"><p>چنانکه بردی امروز باز نفرستی</p></div></div>