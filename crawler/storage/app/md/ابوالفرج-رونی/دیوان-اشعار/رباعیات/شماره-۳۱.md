---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ای دل به سفر چرا نبندی مفرش</p></div>
<div class="m2"><p>کاندر حضرت عیش نمی باشد خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آهن آب داده اندر آتش</p></div>
<div class="m2"><p>نرمی میکن دلا و سختی می کش</p></div></div>