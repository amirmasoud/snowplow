---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>بادی که درآئی به تنم همچو نفس</p></div>
<div class="m2"><p>ناری که بسوزی دل خلقی به هوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبی که به تو زنده توان بودن و بس</p></div>
<div class="m2"><p>خاکی که به تست بازگشت همه کس</p></div></div>