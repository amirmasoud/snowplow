---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ای معطی دولت ای سرافراز عمید</p></div>
<div class="m2"><p>ای صاحب روزگار منصور سعید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شادی و غم ردیف وعد است و وعید</p></div>
<div class="m2"><p>بدخواه تو عود باد و ایام تو عید</p></div></div>