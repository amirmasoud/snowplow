---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>گر عاشق دلسوخته بی تدبیر</p></div>
<div class="m2"><p>پیغام دهد که از توام نیست گزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفرا چه کنی رحم کن ای بدر منیر</p></div>
<div class="m2"><p>پای تو گرفته است رهی دستش گیر</p></div></div>