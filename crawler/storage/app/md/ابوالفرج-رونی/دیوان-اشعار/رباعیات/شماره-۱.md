---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ارزانی عشوه تو هستم صنما</p></div>
<div class="m2"><p>تا چون دل خسته به تو بستم صنما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نیز ترا به دوستی نپرستم</p></div>
<div class="m2"><p>چون زلف تو خورشید پرستم صنما</p></div></div>