---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>تا جزع هوات را دلم حرز افتاد</p></div>
<div class="m2"><p>زو چون تب لرزه بر تنم لرز افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشق توام کار به اندرز افتاد</p></div>
<div class="m2"><p>وزدی بچه زخم تو به آن درز افتاد</p></div></div>