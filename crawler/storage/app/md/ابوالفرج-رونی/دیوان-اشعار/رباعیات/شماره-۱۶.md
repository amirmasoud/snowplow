---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای جوی فراق در تو پایاب نماند</p></div>
<div class="m2"><p>با موج تو کشتی مرا تاب نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کعبه وصل بی توام خواب نماند</p></div>
<div class="m2"><p>خرسندیم از تو جز به محراب نماند</p></div></div>