---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>در ظلمت شبهای فراق ای دلبر</p></div>
<div class="m2"><p>بینی که چگونه می برم عمر به سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ضایع نشود ریختن خون جگر</p></div>
<div class="m2"><p>کاخر بدمد صبح امید چاکر</p></div></div>