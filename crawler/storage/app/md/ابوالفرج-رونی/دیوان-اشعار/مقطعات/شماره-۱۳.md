---
title: >-
    شمارهٔ  ۱۳
---
# شمارهٔ  ۱۳

<div class="b" id="bn1"><div class="m1"><p>به حکم ایزد و اقرار جمله تاجوران</p></div>
<div class="m2"><p>پناه و پشت جهان عز دین تواند بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستوده نصرت دین آنکه ذات نصرت و فتح</p></div>
<div class="m2"><p>همیشه رایت او را قرین تواند بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شد حسام و یمینش یمین فتح و ظفر</p></div>
<div class="m2"><p>بدان خجسته حسام و یمین تواند بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک چو بر قد او کسوت بقا دوزد</p></div>
<div class="m2"><p>سعادتش علم آستین تواند بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک ز اوج فلک می دهد به طبع اقرار</p></div>
<div class="m2"><p>که او مهین ملوک زمین تواند بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی ستوده کریمی که عهد دولت تو</p></div>
<div class="m2"><p>جمال و زیب شهور و سنین تواند بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی سخن ز خرد دوش باز پرسیدم</p></div>
<div class="m2"><p>که او جواب گران مهین تواند بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که هر کسی ز سخای شه است خرم و شاد</p></div>
<div class="m2"><p>برای چیست که طبعم حزین تواند بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب داد خرد کاین گمان مبر بسخاش</p></div>
<div class="m2"><p>که در گمان همه غث و سمین تواند بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر شود به مثل زنده حاتم طائی</p></div>
<div class="m2"><p>ز خرمن کرمش خوشه چین تواند بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه نیز گویم شعرت بد است و نازیبا</p></div>
<div class="m2"><p>از آنکه طبع تو سحر آفرین تواند بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بخت تست مگر کز سخاش محرومی</p></div>
<div class="m2"><p>حقیقت است که حال اینچنین تواند بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو این سخن بشنیدم از او بدانستم</p></div>
<div class="m2"><p>که هر چه گفت خرد آن یقین تواند بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دعای روح امین باد حرز بازوی تو</p></div>
<div class="m2"><p>که حصن دعوت او بس حصین تواند بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>معین و یار تو بادا خدای عزوجل</p></div>
<div class="m2"><p>به از خدای که یار و معین تواند بود</p></div></div>