---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>سرافرازا تو آن صدری که طبعت</p></div>
<div class="m2"><p>به جز تخم نکو نامی نکارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلستان کرم را بشکفد گل</p></div>
<div class="m2"><p>اگر ابر کفت بر وی به بارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان هر چه زان عاجز شود وهم</p></div>
<div class="m2"><p>حکومتها همه رایت گذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من دریاب و این یک نکته بشنو</p></div>
<div class="m2"><p>که هر کان بشنود بر دل نگارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبی کامد به تو نز بهر آن بود</p></div>
<div class="m2"><p>که تا بر خاطرت رنجی گمارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس کامیخت با دونان بترسید</p></div>
<div class="m2"><p>که او را هر کس از دونان شمارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دریای لطافت کان تن تست</p></div>
<div class="m2"><p>فروشد تا مگر غسلی برآرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس آنگه زود برگردید و دانست</p></div>
<div class="m2"><p>که تاب حمله دریا ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سزد گر طبعت از روی بزرگی</p></div>
<div class="m2"><p>چنین بی خردگی زو در گذارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نصیب خصم بی آب تو با دا</p></div>
<div class="m2"><p>تبی کورا به خاک و گل سپارد</p></div></div>