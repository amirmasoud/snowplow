---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>بدان خدای که بر روی رقعه عظمت</p></div>
<div class="m2"><p>کمیته بیدق حکمش هزار فرزین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چاکرند همی صبح و شام بر در او</p></div>
<div class="m2"><p>که آن یکی گهرافشان و این گهر چین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر زیر کف قهرمان قدرت اوست</p></div>
<div class="m2"><p>چو حقه ای که پر از مهره های زرین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو کفه قدرتش از روز و شب پدید آورد</p></div>
<div class="m2"><p>که خط محور بر هر دو کفه شاهین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که شرح شوقم نتوان به صد زبان دادن</p></div>
<div class="m2"><p>که زی جناب همایون مخلص الدین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستوده صاحب سرور محمد بن علی</p></div>
<div class="m2"><p>که زین ملت از ا و با نفاذ و تمکین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر صدور اکابر که صدر مجلس او</p></div>
<div class="m2"><p>ز بوی خلق خوشش پر گل است و نسرین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن مکان که ز خلق خوشش سخن گویند</p></div>
<div class="m2"><p>نسیم باد تو گوئی که عنبرآگین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آن گروه که اندر پناه صدر ویند</p></div>
<div class="m2"><p>زامن و راحتشان بستر است و بالین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عهد دولت او خوش نشین که فتنه و جور</p></div>
<div class="m2"><p>چو کبک و تیهو عدلش چو باز و شاهین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فلک دهد به کف او زمام حکم جهان</p></div>
<div class="m2"><p>هنوز باش که این پایه نخستین است</p></div></div>