---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>برآمد یکی آرزو ملک را</p></div>
<div class="m2"><p>که بود اندر آن آرزو سال‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دست وزارت به صدری رسید</p></div>
<div class="m2"><p>که گیرد سعود از رخش فال‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این پیش بی‌رای او مملکت</p></div>
<div class="m2"><p>چنان بد که بی‌روح تمثال‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابوالقاسم آن کز فلک قسم اوست</p></div>
<div class="m2"><p>چه تعظیم‌ها و چه اجلال‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دندان کند تیز مر کلک را</p></div>
<div class="m2"><p>شود فتنه را کند چنگال‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو چرخ از این پس تلافی کند</p></div>
<div class="m2"><p>اگر پیش از این کرد اخلال‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر داشت بیداد حال نکو</p></div>
<div class="m2"><p>از این پس بگردد ورا حال‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علی الجمله او در زبان‌های خلق</p></div>
<div class="m2"><p>نباشد جز این بیت از امثال‌ها</p></div></div>