---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>هر تیر که در جعبه افلاک بود</p></div>
<div class="m2"><p>آماج گهش این دل غمناک بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چرخ چنین ظالم و بی باک بود</p></div>
<div class="m2"><p>آسوده کسی بود که در خاک بود</p></div></div>