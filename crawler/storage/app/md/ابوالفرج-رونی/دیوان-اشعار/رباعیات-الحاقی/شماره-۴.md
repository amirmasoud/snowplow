---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>شاهی که جهان را به وجودش ناز است</p></div>
<div class="m2"><p>بر خیل قضا خنجر او طناز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با رایت او فتح و ظفر دمساز است</p></div>
<div class="m2"><p>عزالدین ابوالعصب خباز است</p></div></div>