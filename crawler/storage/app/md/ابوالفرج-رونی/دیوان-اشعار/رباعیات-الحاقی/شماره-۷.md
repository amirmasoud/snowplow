---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چون یار به بوسه دادنم بار گرفت</p></div>
<div class="m2"><p>زلفش بگرفتم از من آزار گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون یاری من یار همی خوار گرفت</p></div>
<div class="m2"><p>زان خواست به دست من هی سار گرفت</p></div></div>