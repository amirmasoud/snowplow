---
title: >-
    شمارهٔ  ۸۶ - در مدح علاء الدین اسعد
---
# شمارهٔ  ۸۶ - در مدح علاء الدین اسعد

<div class="b" id="bn1"><div class="m1"><p>ای باد صبحدم که زدم روح پروری</p></div>
<div class="m2"><p>خوشخو چو نوبهاری و خوشبو چو عنبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم طره بنفشه پریشان کنی به صبح</p></div>
<div class="m2"><p>هم غنچه را به وقت سحر پیرهن دری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن پیک رایگانی کز مهر پروران</p></div>
<div class="m2"><p>پیغام سر به مهر بر دلبران بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوگند می دهم به خدا بر تو کان زمان</p></div>
<div class="m2"><p>کز جیب صبحدم نفس خوش برآوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برده گر گره ز درود و سلام من</p></div>
<div class="m2"><p>الا به حضرت سر احرار نگذری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راد زمانه سرور عالم علاء دین</p></div>
<div class="m2"><p>اسعد که هست مایه رادی و سروری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زو مشتری سعادت کلی همی برد</p></div>
<div class="m2"><p>زیرا که اوست اسعد و سعد است مشتری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داده به مهر چهره زیباش روشنی</p></div>
<div class="m2"><p>بر عرش جسته همت عالیش برتری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با نور رأی او نکند مهر هم روی</p></div>
<div class="m2"><p>با جود دست او نکند ابر هم سری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طوقی ز منت او هر مه ز ماه نو</p></div>
<div class="m2"><p>بر چشم خلق عرضه دهد چرخ چنبری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لطف نسیم خلد که جان در تن آورد</p></div>
<div class="m2"><p>با لطف خلق او نکند خود برابری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ننشست هیچ کس چو وی اندر صف هنر</p></div>
<div class="m2"><p>برخاست لاجرم فلک او را به چاکری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای آنکه همتت چو کند خطبه علو</p></div>
<div class="m2"><p>گردون هفت پایه کند میل منبری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قدرت ورای صفحه افلاک خیمه زد</p></div>
<div class="m2"><p>از روی صورت ارچه بدین طارم اندری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فضل و سخا و همت و زین گونه ها هنر</p></div>
<div class="m2"><p>هست از ستاره بیش ترا چون که بشمری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نی آنکه اندرو هنری یا دو یافتند</p></div>
<div class="m2"><p>او را بود مسلم نام هنروری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن خلعت رفیع بود لایق کسی</p></div>
<div class="m2"><p>کو را رسد حقیقت بر سروران سری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک اسبه ران هنر که مسمای آن توئی</p></div>
<div class="m2"><p>الحق ترا چه لایق و یارب چه در خوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از تو هنر غریب نباشد به هیچ حال</p></div>
<div class="m2"><p>از ماه و صبح طرفه ندارند رهبری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترکیب یافت نام تو از چار حرف و تو</p></div>
<div class="m2"><p>زان هر چهار نامی هر هفت کشوری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا چتر نور بر سر گیتی بگسترد</p></div>
<div class="m2"><p>سلطان صبحدم ز سر مهرپروری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خندان نشین و خوش که بهر صبح و شام چرخ</p></div>
<div class="m2"><p>در روی بدسگال تو گوید که خون گری</p></div></div>