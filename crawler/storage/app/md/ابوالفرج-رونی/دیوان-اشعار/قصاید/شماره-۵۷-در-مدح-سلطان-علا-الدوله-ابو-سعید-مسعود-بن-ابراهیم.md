---
title: >-
    شمارهٔ  ۵۷ - در مدح سلطان علاء الدوله ابو سعید مسعود بن ابراهیم
---
# شمارهٔ  ۵۷ - در مدح سلطان علاء الدوله ابو سعید مسعود بن ابراهیم

<div class="b" id="bn1"><div class="m1"><p>شه باز به حضرت رسید هین</p></div>
<div class="m2"><p>یکران مرا برنهید زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خوی کند از شرم او زمان</p></div>
<div class="m2"><p>چون طی کنم از نعل او زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آباد بر این چرخ تیز گرد</p></div>
<div class="m2"><p>از نور سراپای او عجین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم زور چون شیرانش بر کتف</p></div>
<div class="m2"><p>هم موی چون گورانش بر سرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نیزه گذارد شهاب او</p></div>
<div class="m2"><p>دیوی فکند لعب او لعین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور حمله پذیرد سوار او</p></div>
<div class="m2"><p>حصنی بودش پشت او حصین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد آخر او هر نفس هزار</p></div>
<div class="m2"><p>بر صورت او خواند آفرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر میل به جرمش به حق کند</p></div>
<div class="m2"><p>یعنی عوض کهرباست این</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پروانه که در جلوه بیندش</p></div>
<div class="m2"><p>با پیرهن شمعی و سمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لبیک زند گوید ای فلک</p></div>
<div class="m2"><p>جان بازی من بین و شمع بین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای باد هوا ای براق جم</p></div>
<div class="m2"><p>ای قاصد روم و رسول چین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکران من اندر سبق مگر</p></div>
<div class="m2"><p>چین حسدت بست بر جبین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کز منظر او در گذر همی</p></div>
<div class="m2"><p>بر آب نشانی خطوک چین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایزد نه به از به بیافرید</p></div>
<div class="m2"><p>از رشک چرائی دژم چنین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در خاک مکش خویشتن به خشم</p></div>
<div class="m2"><p>بر سنگ مزن خویشتن بکین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواهی که بیکران من رسی</p></div>
<div class="m2"><p>بر سایه یکران من نشین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا شاد فرود آردت چو من</p></div>
<div class="m2"><p>بر درگه سلطان داد و دین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بوسعد سلیمان روزگار</p></div>
<div class="m2"><p>مسعود فریدون آ بین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن شاه که چشم فلک ندید</p></div>
<div class="m2"><p>در خاتم شاهی چنو نگین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وآن شیر که شمشیر حق نیافت</p></div>
<div class="m2"><p>در مالش باطل چنو معین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>راحت ز درد عدل او به ملک</p></div>
<div class="m2"><p>چون بوی درآمد به یاسمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فترت بتف باس او ز شرع</p></div>
<div class="m2"><p>چون موم جدا شد ز انگبین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صیت ملک و ذکر جم شنو</p></div>
<div class="m2"><p>این صوت زئیر آمد آن طنین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عرض شه و جرم فلک نگر</p></div>
<div class="m2"><p>این نفس نفیس آمد آن مهین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یک پنجه نیارد برون فلک</p></div>
<div class="m2"><p>چون پنجه رادیش ز آستین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با همت او آشنا شود</p></div>
<div class="m2"><p>پیش از حرکت قالب جنین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عزمش که بتابد به کف کند</p></div>
<div class="m2"><p>ملکی و نباشد بدان ضنین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رمحش که بیازد فرو خورد</p></div>
<div class="m2"><p>خلقی و نگردد بدان به طین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیلک به کمانش به جان خصم</p></div>
<div class="m2"><p>چون . . .ـین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شعله ز حسامش در آب غرق</p></div>
<div class="m2"><p>چون برق بایما دهد دفین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاها ملکا از گمان تو</p></div>
<div class="m2"><p>رخشنده بود گوهر یقین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در خلد با عزاز پرورد</p></div>
<div class="m2"><p>تکبیر غزات تو حور عین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر قول نه قولی است چون بیانت</p></div>
<div class="m2"><p>آحاد . . .ـین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر بحر نه بحری است چون ذلت</p></div>
<div class="m2"><p>قیفال . . . از وتین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا طعمه بازان شود تذرو</p></div>
<div class="m2"><p>تا سکنه شیران بود عرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باد اختر سلطان تو مضئی</p></div>
<div class="m2"><p>باد آیت برهان تو مبین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>با دولت تو ناصحت رفیق</p></div>
<div class="m2"><p>با طالع تو مادحت قرین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر درگه حق شأن تو بزرگ</p></div>
<div class="m2"><p>در نصرت دین رأی تو رزین</p></div></div>