---
title: >-
    شمارهٔ  ۱۶ - ایضاً له
---
# شمارهٔ  ۱۶ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>روزگار عصیر انگور است</p></div>
<div class="m2"><p>خم ازو مست و چنک مخمور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز تا سوی باغ بشتابیم</p></div>
<div class="m2"><p>کز می و میوه اندر او سور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیب سیمین سلب چو گوی بلور</p></div>
<div class="m2"><p>یا چو نوخواسته بر حور است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش ترش زرد چهره آبی را</p></div>
<div class="m2"><p>طبع مرطوب و رنگ محرور است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخ امرود گوئی وامرود</p></div>
<div class="m2"><p>دسته و کردنای طنبور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نارسیده ترنج بار ورش</p></div>
<div class="m2"><p>چون فقع کوزه و چو سنگور است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نار ازو ناردانه گشته جدا</p></div>
<div class="m2"><p>چون عزب خانهای زنبور است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاج نرگس به فرق نرگس بر</p></div>
<div class="m2"><p>جام زرین خواجه منصور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صاحب عالم آنکه عالم فضل</p></div>
<div class="m2"><p>تا ز املاک اوست معمور است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست از عقل و علم او بیرون</p></div>
<div class="m2"><p>هر چه بر سطر لوح مسطور است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کار دنیا و شغل عقبی پاک</p></div>
<div class="m2"><p>بر هوا و رضاش مقصور است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرخ با اوج قدر او باطل</p></div>
<div class="m2"><p>بحر با موج کف او زور است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نظم و لفظش چو گوهر منظوم</p></div>
<div class="m2"><p>نثر خطش چو در منثور است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نقشبند طراز مهرش را</p></div>
<div class="m2"><p>صد هزار آفتاب مزدور است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گردباد سراب کینش را</p></div>
<div class="m2"><p>تا فلک باژگونه در دور است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن سهیل است برق هیبت او</p></div>
<div class="m2"><p>که تجلیش سکنه طور است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وان شهاب است رای ثاقب او</p></div>
<div class="m2"><p>که از او دیو فتنه مقهور است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرکب فرخ همایونش</p></div>
<div class="m2"><p>آهنین برج و آتشین سور است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود چون آفتاب تیز ولیک</p></div>
<div class="m2"><p>تیز چون آفتاب با حور است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سایه در نور اگر ندیدستی</p></div>
<div class="m2"><p>جرم او بین که سایه در نور است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در تک ایدون بود که بادبزان</p></div>
<div class="m2"><p>که تو گوئی قضای مقدور است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شکل او بی شکال بر چیزی</p></div>
<div class="m2"><p>نیک مشکل شود که مجبور است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قالب نصرت است و نیست بدیع</p></div>
<div class="m2"><p>که بر او ذات خواجه منصور است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایزد از عرض خواجه دور کناد</p></div>
<div class="m2"><p>هر غرض کز مراد او دور است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل او گنج راز خسرو باد</p></div>
<div class="m2"><p>تا زمین رازدار و گنجور است</p></div></div>