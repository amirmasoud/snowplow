---
title: >-
    شمارهٔ  ۷ - ایضاً له
---
# شمارهٔ  ۷ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>زرود زاوه عبر کرد بحر ما</p></div>
<div class="m2"><p>نبیره رجای خلق ابوالرجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابوالحسن علی که نعت خلق او</p></div>
<div class="m2"><p>خبر دهد ز نام والدش ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمید ملک شهریار محتشم</p></div>
<div class="m2"><p>عماد دین مصطفای مجتبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسیده جاه او به جرم مشتری</p></div>
<div class="m2"><p>پرید جسم او به روح اولیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشته قدر او ز اوج آسمان</p></div>
<div class="m2"><p>چو از قدر او رضای پادشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیانتش به کشته آتش ستم</p></div>
<div class="m2"><p>تواضعش به برده آب کبریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه نعل مرکبش چه شکل ماه نو</p></div>
<div class="m2"><p>چه گرد موکبش چه کحل توتیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر ثنا دروده چون بر زمین</p></div>
<div class="m2"><p>در عطا گشوده چون در هوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهال عرق فضل او ذوی الحسب</p></div>
<div class="m2"><p>عیال ذات جود او ذوی النها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پوی سوی آفتاب دولتش</p></div>
<div class="m2"><p>کز اوست آفتاب چرخ را ضیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگرد گرد آب گرد هیبتش</p></div>
<div class="m2"><p>که در کشد بدم ترا چو اژدها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عذاب او حریق در جحیم زد</p></div>
<div class="m2"><p>خلاص جست او و گفت عافنا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بارگاه او ملک ز خلد شد</p></div>
<div class="m2"><p>ندا شنید کاندر آی مرحبا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جدا کند عقیم کره او ز تن</p></div>
<div class="m2"><p>نشاط دل فضول سر بالتقا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برون برد نسیم رفق او ز یم</p></div>
<div class="m2"><p>هم اجنبی هم آشنا به آشنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دوان رود سوال سایلش بدو</p></div>
<div class="m2"><p>چنانکه که دوان رود به کهربا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غنی شود امید زائرش ازو</p></div>
<div class="m2"><p>چنانکه مس غنی شود به کیمیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همیشه تا برآید از کلام حق</p></div>
<div class="m2"><p>شریف ذکر انبیاء و اولیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز عشرت و ز لهو بادش امتحان</p></div>
<div class="m2"><p>به دولت و به بخت بادش التجا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قوی به عون و سعی در حق ولی</p></div>
<div class="m2"><p>یلی به امر و نهی در تن ملا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه مرتقاش سوده نعل مرتقی</p></div>
<div class="m2"><p>نه مقتدیش دیده عزل مقتدا</p></div></div>