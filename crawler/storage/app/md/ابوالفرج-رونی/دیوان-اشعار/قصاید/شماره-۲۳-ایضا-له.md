---
title: >-
    شمارهٔ  ۲۳ - ایضاً له
---
# شمارهٔ  ۲۳ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>آفرین بر شاه و جشن شاه باد</p></div>
<div class="m2"><p>جشن ملک آرای او هر ماه باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست بذل از گنج او کوتاه نیست</p></div>
<div class="m2"><p>دست عزل از جشن او کوتاه باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رأی گردون قدر او را تاج بخش</p></div>
<div class="m2"><p>اوج کیوان صدر او را گاه باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابش خاکروب و پیل گوش</p></div>
<div class="m2"><p>وآسمانش گنبد و خرگاه باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظل عدلش بر سر خلق خدای</p></div>
<div class="m2"><p>پایدار ایدون چو ظل چاه باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر غزوش در بلاد اهل شرک</p></div>
<div class="m2"><p>رهگذار ایدون چو سیر ماه باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ستاره بر براق همتش</p></div>
<div class="m2"><p>اوج خواهد اوج او را کاه باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور زمانه بی سلاح نصرتش</p></div>
<div class="m2"><p>جنگ جوید شیر او روباه باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در فضای شرق و غرب از حزم او</p></div>
<div class="m2"><p>سال و مه منهی و کار آگاه باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیک و بد را زو به گاه خیر و شر</p></div>
<div class="m2"><p>نوبت پاداش و بادافراه باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشتری با عرض او همنام گشت</p></div>
<div class="m2"><p>عرض او با مشتری هم جاه باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جهان فتح او ایام غضر</p></div>
<div class="m2"><p>در جهای مدح در افواه باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز حرب از پیش او خرچنگ وار</p></div>
<div class="m2"><p>پس خزیدن عادت بدخواه باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دم زده کژدم ندیدی زان عمل</p></div>
<div class="m2"><p>اژدها در حرب او جولاه باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون خم ایوان کسری در حضر</p></div>
<div class="m2"><p>بر خم قصرش خم درگاه باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بنات النعش صغری در سفر</p></div>
<div class="m2"><p>آخر خیلش صد و پنجاه باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه از فرمان او گردن کشد</p></div>
<div class="m2"><p>سکنه زو پر ویل و وا ویلاه باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وآخرش مانند راه کهکشان</p></div>
<div class="m2"><p>بی ستور و بی جو و بی کاه باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا بود با نفس نالان ناله جفت</p></div>
<div class="m2"><p>حاسدش را ناله واغوثاه باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رزم غزو و بزم جشن فرخش</p></div>
<div class="m2"><p>گه سگالش کرده که ناگاه باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آفرین بر خسرو و بر غزو او</p></div>
<div class="m2"><p>آفرین بر شاه و جشن شاه باد</p></div></div>