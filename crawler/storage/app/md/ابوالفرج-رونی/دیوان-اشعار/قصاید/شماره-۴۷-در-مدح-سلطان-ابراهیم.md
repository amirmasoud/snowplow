---
title: >-
    شمارهٔ  ۴۷ - در مدح سلطان ابراهیم
---
# شمارهٔ  ۴۷ - در مدح سلطان ابراهیم

<div class="b" id="bn1"><div class="m1"><p>ای به ذات تو ملک گشته جلیل</p></div>
<div class="m2"><p>وی به نام تو زنده نام خلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بیان تو طبع فضل فرح «فره »</p></div>
<div class="m2"><p>وز بنان تو چشم جود کحیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش حلم تو آب نرم درشت</p></div>
<div class="m2"><p>پیش عزم تو برق تیز کلیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهر با شور هیبت تو جبان</p></div>
<div class="m2"><p>بحر با بذل همت تو بخیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل تو شرع را بحق ضامن</p></div>
<div class="m2"><p>کف تو خلق را به رزق کفیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اعتقاد تو صافی از شبهات</p></div>
<div class="m2"><p>اجتهاد تو خالی از تعطیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار حکمت بریدن دعوی</p></div>
<div class="m2"><p>شغل عفوت خریدن تاویل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر تو پوشیده نی صلاح و فساد</p></div>
<div class="m2"><p>وز تو دزدیده نی کثیر و قلیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسر وهمها شوی بقیاس</p></div>
<div class="m2"><p>بدم رمزها رسی بدلیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه سازی ز امهات شگفت</p></div>
<div class="m2"><p>و آنچه دانی ز مفردات جمیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسمانی به کوشش و بخشش</p></div>
<div class="m2"><p>آفتابی به گردش و تحویل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حصن امنت کشیده برج ببرج</p></div>
<div class="m2"><p>راه عدلت گشاده میل به میل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهی تو نهی و شرط او آرام</p></div>
<div class="m2"><p>امر تو امر و حکم او تعجیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در کشد مهر تو کلنگ از چرخ</p></div>
<div class="m2"><p>برکشد قهر تو نهنگ از نیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز حرب تو کز تحیر وقت</p></div>
<div class="m2"><p>اندر افتد سپه بقال و بقیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تیغ بینی ز مرد و مرد از تیغ</p></div>
<div class="m2"><p>این بدان آن بدین عزیز و ذلیل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاکها چهره سرخ کرده به خون</p></div>
<div class="m2"><p>گردها جامه رنگ کرده به نیل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هوش اجسام سوی جای نزول</p></div>
<div class="m2"><p>گوش ارواح سوی طبل رحیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کر و فر ترا نظاره کنند</p></div>
<div class="m2"><p>از فلک جبرئیل و میکائیل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه بتفسی ز لعبهای سبک</p></div>
<div class="m2"><p>نه بترسی ز حملهای ثقیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باره تازی در آتشین میدان</p></div>
<div class="m2"><p>گر ز یازی بر آهنین اکلیل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بکنی بیخ شاخ های بزرگ</p></div>
<div class="m2"><p>بزنی شاخ بیخ های طویل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خسروا بنده از اریکه ظلم</p></div>
<div class="m2"><p>شاهرخهای زفت خورد از پیل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گشته گریان ز بنده تا آزاد</p></div>
<div class="m2"><p>مانده عریان ز موزه تا مندیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی عمل عزل دید بر بالین</p></div>
<div class="m2"><p>بی گنه سنگ یافت در قندیل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باده اقبال حضرت عالیت</p></div>
<div class="m2"><p>گر نجستی بر این فقیر معیل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شخص او را حیات نفزودی</p></div>
<div class="m2"><p>جز به آواز صور اسرافیل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا که از دیدن شگفتیها</p></div>
<div class="m2"><p>برود بر زمانه ها تهلیل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باد عمر تو بادوام انباز</p></div>
<div class="m2"><p>باد ملک تو با نظام عدیل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیک خواهانت جفت شادی و لهو</p></div>
<div class="m2"><p>بدسگالانت یار ویل و عویل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>«قاری جشنهای خاص ترا</p></div>
<div class="m2"><p>نوبت سال و ماه گشته رسیل »</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرجع ملکها به حضرت تو</p></div>
<div class="m2"><p>چون به مجموع مرجع تفصیل</p></div></div>