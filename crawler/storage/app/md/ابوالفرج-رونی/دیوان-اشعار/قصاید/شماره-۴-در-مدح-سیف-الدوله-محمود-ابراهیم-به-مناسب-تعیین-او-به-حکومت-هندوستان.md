---
title: >-
    شمارهٔ  ۴ - در مدح سیف الدوله محمود ابراهیم به مناسب تعیین او به حکومت هندوستان
---
# شمارهٔ  ۴ - در مدح سیف الدوله محمود ابراهیم به مناسب تعیین او به حکومت هندوستان

<div class="b" id="bn1"><div class="m1"><p>شاها نظام ملک و قوام جهانیا</p></div>
<div class="m2"><p>با دولت مساعد و بخت جوانیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم است بختیاری و در چشم نوریا</p></div>
<div class="m2"><p>جسم ست کامکاری و در جسم جانیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ملت از رسول به پاکی ستوده</p></div>
<div class="m2"><p>چون رحمت ازخدای به نیکی نشانیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوئی دعائی آنچه به جوئی بدان رسی</p></div>
<div class="m2"><p>گوئی قضائی آنچه به خواهی برانیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردون ترا سکالد کیخسروی همی</p></div>
<div class="m2"><p>اینک بنفذ والی هندوستانیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت بلند باید کردن که تو هنوز</p></div>
<div class="m2"><p>بر پایه نخستین از نردبانیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایدون شنیده ایم که صاحبقران شود</p></div>
<div class="m2"><p>هنگام تو کسی ملکا و تو آنیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز روی عقل یک تنی اندر جهان ولیک</p></div>
<div class="m2"><p>اندر هنر تمامتر از صد جهانیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدار خواست چشم زمانه ز قدر تو</p></div>
<div class="m2"><p>در گوش او نهاد قضا لن ترانیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر آسمان بدرد روزی ز هیبتت</p></div>
<div class="m2"><p>ناید ز همت تو مگر آسمانیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اقبال خلق کرد به حکم تو کردگار</p></div>
<div class="m2"><p>تا تو به شرط داد بهر کس رسانیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اسباب نیک بختی در حل و عقد تست</p></div>
<div class="m2"><p>فرمان تراست گر دهی وگر ستانیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکر آن خدای را که به جاه تو باز بست</p></div>
<div class="m2"><p>این شغل و این ولایت و این قهرمانیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز آمدند با تو همه بندگان تو</p></div>
<div class="m2"><p>با عاملی و شحنگی و پهلوانیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اندر پناه عدل تو اکنون درین دیار</p></div>
<div class="m2"><p>بر گرگ محرمی بود اندر شبانیا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دزدی که ره گرفتی بر کاروانیان</p></div>
<div class="m2"><p>آید کنون به بدرقه کاروانیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بس گردنان که گردن چون گوی بردرند</p></div>
<div class="m2"><p>گردد همی ز صولت تو صولجانیا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواب ست حیله فتنه بیدار گشته را</p></div>
<div class="m2"><p>چون گشت پیشه تیغ ترا پاسبانیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا در جهان نیارد حاصل به سیم و زر</p></div>
<div class="m2"><p>کس نعمتی بزرگتر از زندگانیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیوسته باد با تو و با روزگار تو</p></div>
<div class="m2"><p>عز و بقا و مملکت جاودانیا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عالم شکسته خصم ترا در دل آرزو</p></div>
<div class="m2"><p>دولت نموده حکم ترا خوش عنانیا</p></div></div>