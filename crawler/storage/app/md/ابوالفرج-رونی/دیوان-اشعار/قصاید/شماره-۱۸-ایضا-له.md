---
title: >-
    شمارهٔ  ۱۸ - ایضاً له
---
# شمارهٔ  ۱۸ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>ای بار خدایا که جهان چون تو ندید است</p></div>
<div class="m2"><p>نام تو رسید است به جائی که رسید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردار تو در جسم جوانمردی جان ست</p></div>
<div class="m2"><p>دیدار تو در چشم خردمندی دید است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وهم تو اسرار فلک روی گشاد است</p></div>
<div class="m2"><p>با عدل تو اسباب بلا دست کشید است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحری است دلت کورا صد ابر غلام است</p></div>
<div class="m2"><p>ابری است کفت کز وی صد بحر چکید است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخرید عطای تو خریدار عطا را</p></div>
<div class="m2"><p>جز وی که شنیدی که خریدار خرید است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدر تو هوای تو همی دارد در سر</p></div>
<div class="m2"><p>زان است که چون کیوان بر اوج رسید است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خصم تو رضای تو همی جوید در خاک</p></div>
<div class="m2"><p>زان ست که چون آب در او جای گزید است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانند افاضل که به فضل تو بزرگی</p></div>
<div class="m2"><p>تا گوش بزرگی شنوا شد نشنید است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پیش دوات و قلمت عرض و رسالت</p></div>
<div class="m2"><p>این دست بلر کرده و آن پشت خمید است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی تیشه عقل تو خرد نیم تراش است</p></div>
<div class="m2"><p>بی جرعه طبع تو ادب نیم گزید است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سطری ز تو جز آیت رحمت ننوشته است</p></div>
<div class="m2"><p>تاری ز تو جز دولت باقی نتنید است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنجا که توئی دهر ز هیبت ننهد پی</p></div>
<div class="m2"><p>وان را که توئی چرخ به باطل نخلید است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این بنده چه کرده است که بی زلت و بی جرم</p></div>
<div class="m2"><p>از بیم فخ حادثه چون مرغ رمید است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کم داهیه مانده است که آن را نبسوده است</p></div>
<div class="m2"><p>کم زاویه مانده است که در وی نخزید است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نالی است تنش بی دل و آن نال گسسته است</p></div>
<div class="m2"><p>ناری است دلش بی تن وان نار کفید است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درویش ندیدند که محسود بود هیچ</p></div>
<div class="m2"><p>محسود بدینگونه که بنده است که دید است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر صورت حالی که نمودند جز آن نیست</p></div>
<div class="m2"><p>پس بنده به هم کنیت تو ناگروید است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا حکم غم و شادی بر لوح نوشته است</p></div>
<div class="m2"><p>تا باد بدو نیک بر آفاق وزید است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از دولت تو دست حسد کوته خواهم</p></div>
<div class="m2"><p>با دولت تو خود که چخد یا که چخید است</p></div></div>