---
title: >-
    شمارهٔ  ۱۴ - در مدح منصور سعید
---
# شمارهٔ  ۱۴ - در مدح منصور سعید

<div class="b" id="bn1"><div class="m1"><p>جشن فرخنده فروردین است</p></div>
<div class="m2"><p>روز بازار گل و نسرین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب چون آتش عود افروز است</p></div>
<div class="m2"><p>باد چون خاک عبیر آگین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ پیراسته گلزار بهشت</p></div>
<div class="m2"><p>گلبن آراسته حورالعین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برج ثور است مگر شاخ سمن</p></div>
<div class="m2"><p>که گلشن را شبه پروین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد بستان ز فروغ لاله</p></div>
<div class="m2"><p>گوئی آتشکده بر زین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیشه از سبزه وز جوی و درخت</p></div>
<div class="m2"><p>چون زمین دگر از غزنین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب چین یافته در حوض از باد</p></div>
<div class="m2"><p>همچو پر کار حریر چین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بط چینی که به باد است درو</p></div>
<div class="m2"><p>چون پیاد است که با نعلین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بچه ماند به عروسی عالم</p></div>
<div class="m2"><p>که سبک روح و گران کابین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شه او زیبد منصور سعید</p></div>
<div class="m2"><p>که همین خسرو و آن شیرین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ذوفنون شاهی کاندر فن ملک</p></div>
<div class="m2"><p>بر شاه عجمش تمکین است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در لفظش چو به سد شاخ انگیز</p></div>
<div class="m2"><p>مشک خطش چو شکر شیرین است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روش تنین دارد قلمش</p></div>
<div class="m2"><p>گرچه تریاک دو صد تنین است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرد آئین کف رادش دید</p></div>
<div class="m2"><p>مایه رزق جهان گفت این است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون بها در گهر بیش بها</p></div>
<div class="m2"><p>هنر اندر گهرش تضمین است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن دبیری است که در جوزا تیر</p></div>
<div class="m2"><p>بار قومش رقم ترقین است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وان سواری است که بر گردون ماه</p></div>
<div class="m2"><p>پیش او چون زین بر خرزین است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه چنو باشد و ماننده او</p></div>
<div class="m2"><p>او شه و هر که جز او فرزین است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کبک را دل چو دل شاهین نیست</p></div>
<div class="m2"><p>اگرش پر چو پر شاهین است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست معراج نه چون خدمت اوست</p></div>
<div class="m2"><p>هست بهرام نه چون چوبین است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنگ در همت او زن که ترا</p></div>
<div class="m2"><p>همتش رهبر علیین است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جود او کعبه زوار شناس</p></div>
<div class="m2"><p>کعبه کش دربی زرفین است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تکیه بر بالش اقبالش دار</p></div>
<div class="m2"><p>که ز تأییدش دار آفرین است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آفرین باد بر آن شخص کز او</p></div>
<div class="m2"><p>حاسد او ز در نفرین است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با بقا ساخته با داش نفس</p></div>
<div class="m2"><p>تا دعا ساخته با آمین است</p></div></div>