---
title: >-
    شمارهٔ  ۴۶ - در مدح خواجه ابوسعد بابو
---
# شمارهٔ  ۴۶ - در مدح خواجه ابوسعد بابو

<div class="b" id="bn1"><div class="m1"><p>فلک در سایه پر حواصل</p></div>
<div class="m2"><p>زمین را پر طوطی کرد حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوا بر سیرت ضحاک ظالم</p></div>
<div class="m2"><p>گزید آئین نوشروان عادل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خزان را با بهار از لعب شطرنج</p></div>
<div class="m2"><p>بوجه سهو شد نوبت محامل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نرگس مانده باغ و جوی مفلس</p></div>
<div class="m2"><p>ز لاله گشته کوه و دشت حامل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب سور است پنداری جهان را</p></div>
<div class="m2"><p>که برکردند از ایوانش مشاعل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر سوسن نشد بر باغ عاشق</p></div>
<div class="m2"><p>چه مانده است اندر او پایش فرو گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل از پیروزه گوئی شکل دستی است</p></div>
<div class="m2"><p>گرفته جام لعل اندر انامل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و صحرا که شد صحرا به معنی</p></div>
<div class="m2"><p>چو صحن مجلس عین افاضل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمید مملکت بوسعد بابو</p></div>
<div class="m2"><p>که باب هیبتش بابی است مشکل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرا دانی به حضرت پیش خسرو</p></div>
<div class="m2"><p>چو او فرزانه مقبول مقبل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مقدم عقل و در جمع اواخر</p></div>
<div class="m2"><p>مؤخر عهد با علم اوائل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز جودش گر عروضی بحر سازد</p></div>
<div class="m2"><p>از او ناقص نماید بحر کامل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز اندر غایت انعام و اکرام</p></div>
<div class="m2"><p>در او لاله چه داند گفت عامل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو ابر هاطل اندر حق شوره</p></div>
<div class="m2"><p>ببیند عقلت اندر حق غافل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برآرد بیخ طمع از خاک آدم</p></div>
<div class="m2"><p>کز او مسئول گردد طمع سائل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه شخص است آن براق خواجه یارب</p></div>
<div class="m2"><p>کز او هر جستنی برقی است هایل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به تن زو کوس خورده کوه ساکن</p></div>
<div class="m2"><p>بتک زو کاغ کرده باد عاجل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه رفتن چو خضر از کل عالم</p></div>
<div class="m2"><p>نه مسکن دانی او را و نه منزل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گه گشتن چو مور از خط ناورد</p></div>
<div class="m2"><p>نه خارج یا بی او را و نه داخل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وزان برق دگر هیهات هیهات</p></div>
<div class="m2"><p>که شد زین براقش را حمایل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو دل میدان او در صدر قالب</p></div>
<div class="m2"><p>چو عقل آرام او در مغز عاقل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حصار روح او را روح کاره</p></div>
<div class="m2"><p>فساد طبع او را طبع قایل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گشاده در اجل ها راه حیوان</p></div>
<div class="m2"><p>کشیده بر املها خط باطل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا بود تقطیع این وزن</p></div>
<div class="m2"><p>مفاعیلن مفاعیلن مفاعل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هزاران نوبت نوروز بیناد</p></div>
<div class="m2"><p>چنین با عید اضحی گشته همدل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سعادت پیشکارش در مساکن</p></div>
<div class="m2"><p>سلامت پاسبانش در مراحل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>«جهان تیز روز و کنده بر پای</p></div>
<div class="m2"><p>ز بار طبع او چون حلم کامل »</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به نام او . . . بوالفرج را</p></div>
<div class="m2"><p>برین ترتیب و رتبت صدر سایل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>موافق در همه احوال با او</p></div>
<div class="m2"><p>جمال صدر و دیوان رسائل</p></div></div>