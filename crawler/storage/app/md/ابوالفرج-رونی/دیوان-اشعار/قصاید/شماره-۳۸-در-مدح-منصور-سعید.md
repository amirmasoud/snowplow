---
title: >-
    شمارهٔ  ۳۸ - در مدح منصور سعید
---
# شمارهٔ  ۳۸ - در مدح منصور سعید

<div class="b" id="bn1"><div class="m1"><p>ای سرافراز عالم ای منصور</p></div>
<div class="m2"><p>وی به صدر تو اختلاف صدور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای به‌قدر آسمانِ قایم‌ذات</p></div>
<div class="m2"><p>ای به رای آفتاب زاید نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگاری وز تو دشمن و دوست</p></div>
<div class="m2"><p>به مصیبت رسیده اند و به سور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسته حکم تو در قلوب و رقاب</p></div>
<div class="m2"><p>جسته امر تو در سنین و شهور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه گفتار تو به حق نزدیک</p></div>
<div class="m2"><p>همه کردار تو ز باطل دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق لامع به جای فهم تو کند</p></div>
<div class="m2"><p>صبح صادق به جنب و هم تو زور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر بی پاس تو شکار شگال</p></div>
<div class="m2"><p>باز بی عون تو خور عصفور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیش کره تو بردم کژدم</p></div>
<div class="m2"><p>نوش رفق تو در سر زنبور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به خواهی حمایت تو شود</p></div>
<div class="m2"><p>چون حرم حامی وحوش و طیور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور بکوشی کفایت تو نهد</p></div>
<div class="m2"><p>یوغ بر گردن صبا و دبور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در سیاقت بگاه خیره تر است</p></div>
<div class="m2"><p>روز بدخواه تو ز ضرب کسور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کار داریست عدل تو معمار</p></div>
<div class="m2"><p>گشته اسباب ملک از او معمور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پادشاهی است نفس تو قاهر</p></div>
<div class="m2"><p>شده دیو هوا به دو مقهور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیگ مقهور چرخ ناپخته</p></div>
<div class="m2"><p>بوی علم تو آید از مقدور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لوح محفوظ را همانا نیست</p></div>
<div class="m2"><p>از وقوف تو خیر و شر مستور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ویحک آن مصری مجوف چیست</p></div>
<div class="m2"><p>لون او لون عاشق مهجور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نظم تو نقش و سحر واو نقاش</p></div>
<div class="m2"><p>نثر تو گنج در واو گنجور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زو هراسان جهان واو ساکن</p></div>
<div class="m2"><p>زو تن آسان سپاه و او رنجور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دست بر سر گرفته والی ظلم</p></div>
<div class="m2"><p>از چنو والی و چنو دستور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گاه تفویض کرده آمر عدل</p></div>
<div class="m2"><p>نه چو تو آمر و چنو مامور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>منعما مکرما خداوندا</p></div>
<div class="m2"><p>شاکرند از تو خلق و تو مشکور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خشم و حلم تو در ثواب و عقاب</p></div>
<div class="m2"><p>دو بزرگند ناصبور و صبور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نکشی چو به سهو حری غین</p></div>
<div class="m2"><p>نخری جز به عرق جود غرور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش معروف تو چه وزن آرد</p></div>
<div class="m2"><p>حاصل حق عرض لو هاور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا نگردد می مروق تلخ</p></div>
<div class="m2"><p>هم در انگور شیره انگور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فضل جاه ترا مباد شکست</p></div>
<div class="m2"><p>ربع تخت ترا مباد قصور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>موکبت جفت فتح باد و ظفر</p></div>
<div class="m2"><p>مجلست یار لهو باد و سرور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ساخته عرضت از هنر مرقد</p></div>
<div class="m2"><p>یافته عمرت از بقا منشور</p></div></div>