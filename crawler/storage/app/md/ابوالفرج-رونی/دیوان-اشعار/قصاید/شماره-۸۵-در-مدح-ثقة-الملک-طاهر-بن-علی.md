---
title: >-
    شمارهٔ  ۸۵ - در مدح ثقة الملک طاهر بن علی
---
# شمارهٔ  ۸۵ - در مدح ثقة الملک طاهر بن علی

<div class="b" id="bn1"><div class="m1"><p>ثقة الملک خاص و خازن شاه</p></div>
<div class="m2"><p>خواجه طاهر علیک عین الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قدوم عزیز لوهاور</p></div>
<div class="m2"><p>مصر کرد و ز مصر بیش به جاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور او نور یوسف چاهی است</p></div>
<div class="m2"><p>جاه او نابسوده سایه چاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاف فضلش به بذل گشته رهی</p></div>
<div class="m2"><p>چشم شعرش به شرع کرده نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستهای دراز نهی گران</p></div>
<div class="m2"><p>شده از نهی منکرش کوتاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میغ دوشا به بازوی و کف او</p></div>
<div class="m2"><p>شیر دوشیده در گلوی گیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حبذا آن زمین که عبره کند</p></div>
<div class="m2"><p>موکبش طول و عرض آن به سپاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه بدو ظلم را کنند مرح</p></div>
<div class="m2"><p>نه در او قحط را دهند پناه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرش ارشیر آسمان باشد</p></div>
<div class="m2"><p>بی اجل جرم او نگیرد راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوهش ار کوه کهربا باشد</p></div>
<div class="m2"><p>بی بها طبع او نیابد کاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شادباش ای چو عدل نوشروان</p></div>
<div class="m2"><p>ذکر عدل تو سجده افواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیر زی ای چو سد اسکندر</p></div>
<div class="m2"><p>سد حزم تو حایل بدخواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عین فضلی و روزگار تراست</p></div>
<div class="m2"><p>بر مراعات خلق وسعت گاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دور چرخی . . .</p></div>
<div class="m2"><p>در مهمات ملک سرعت ماه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیچ دعوی نکرده همت تو</p></div>
<div class="m2"><p>کز دو علوی نداشته دو گواه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هیچ منزل نکوفت اختر تو</p></div>
<div class="m2"><p>بر دو نیر نساخته دو سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی نگوید که . . .</p></div>
<div class="m2"><p>سعی رفتن . . .</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا به زجر و به فال نیک بود</p></div>
<div class="m2"><p>بر سر راه دیدن روباه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کام کام تو باد در نیکی</p></div>
<div class="m2"><p>کار کار تو باد بر درگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قرن عمر تو سی و پنج ولی</p></div>
<div class="m2"><p>سال قرن تو سیصد و پنجاه</p></div></div>