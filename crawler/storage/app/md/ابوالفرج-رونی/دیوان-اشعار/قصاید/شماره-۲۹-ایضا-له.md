---
title: >-
    شمارهٔ  ۲۹ - ایضاً له
---
# شمارهٔ  ۲۹ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>این مبارک پی بنای محکم گردون نهان</p></div>
<div class="m2"><p>کرده شاگردیش گردون خوانده او را اوستاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب در آفتاب و سایه اقبال و بخت</p></div>
<div class="m2"><p>جای ابراهیم بن مسعود ابراهیم باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشرق میدان شاه دین فروز دین پرست</p></div>
<div class="m2"><p>دیده بان بارگاه خسرو خسرو نژاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا جهان را بیخ و شاخ و برگ و بار اندر بقا</p></div>
<div class="m2"><p>آتش گرم ست و آب سرد و خاک خشگ و باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه ابراهیم نازان بر فراز آن بنا</p></div>
<div class="m2"><p>تن درست و دل قوی و طبع راد و روح شاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او به جاه از جم گذشته کامران بر تخت ملک</p></div>
<div class="m2"><p>بندگان او رسیده زو به جاه کیقباد</p></div></div>