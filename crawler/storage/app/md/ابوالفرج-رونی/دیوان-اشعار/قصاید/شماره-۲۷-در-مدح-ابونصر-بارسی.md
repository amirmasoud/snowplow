---
title: >-
    شمارهٔ  ۲۷ - در مدح ابونصر بارسی
---
# شمارهٔ  ۲۷ - در مدح ابونصر بارسی

<div class="b" id="bn1"><div class="m1"><p>با مال جود خواجه بکین باشد</p></div>
<div class="m2"><p>وز جود مال خواجه حزین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسان از او به رزق رسد هر کس</p></div>
<div class="m2"><p>بخشنده خدای چنین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش دل غنی و کف رادش</p></div>
<div class="m2"><p>دریا فقیر و ابر ضنین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عطر نسیم خلقش گرد آید</p></div>
<div class="m2"><p>در ناف آهوئی که به چین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر شاخ نظم و نثر بر طبعش</p></div>
<div class="m2"><p>سحر حلال و در ثمین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش یقین گمانش چنان بیند</p></div>
<div class="m2"><p>گوئی گمانش عین یقین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عامر کند خراب زمین رایش</p></div>
<div class="m2"><p>بنگر که رای او چه رزین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاندر حیات خاک خراب او</p></div>
<div class="m2"><p>چون نفخ صور بازپسین باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بختش مزاج خاتم جم دارد</p></div>
<div class="m2"><p>دنیا و دینش زیر نگین باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر زین همتش بکشد نفسی</p></div>
<div class="m2"><p>بر شیر آسمانش زین باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صعبا صهیل مرکب او صعبا</p></div>
<div class="m2"><p>در حق او زبر طنین باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که از صدای او به انین آمد</p></div>
<div class="m2"><p>آری صداش جفت انین باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم تک او براق بهشت افتد</p></div>
<div class="m2"><p>گر شیر یال و گور سرین باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا بازمان ثبات زمین بینی</p></div>
<div class="m2"><p>تا در مکان قرار مکین باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر وی سوار باد ابونصری</p></div>
<div class="m2"><p>کز دین پاک ناصر دین باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>«بروی به تخت باد سرافرازی</p></div>
<div class="m2"><p>کش تخت آسمان به زمین باشد</p></div></div>