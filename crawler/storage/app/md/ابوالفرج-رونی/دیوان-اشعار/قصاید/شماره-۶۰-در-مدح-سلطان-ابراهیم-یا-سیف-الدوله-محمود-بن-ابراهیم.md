---
title: >-
    شمارهٔ  ۶۰ - در مدح سلطان ابراهیم یا سیف الدوله محمود بن ابراهیم
---
# شمارهٔ  ۶۰ - در مدح سلطان ابراهیم یا سیف الدوله محمود بن ابراهیم

<div class="b" id="bn1"><div class="m1"><p>ماه ملک آمد از خسوف برون</p></div>
<div class="m2"><p>تخت ازو یافت رتبت گردون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد نورش ز ثابتات شکوه</p></div>
<div class="m2"><p>داد سیرش به حادثات سکون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز بر برگرفت باطل دست</p></div>
<div class="m2"><p>باز بر هم نهاد فتنه جفون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرم شد نرم چرخ تیز و درشت</p></div>
<div class="m2"><p>رام شد رام دهر تند و حرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب در جوی عدل گشت گلاب</p></div>
<div class="m2"><p>نوش در کام ظلم شد افیون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برکشید از نیام صیقل ملک</p></div>
<div class="m2"><p>سیف دولت زدوده آینه گون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم زخمی که بر هدی زده بود</p></div>
<div class="m2"><p>برزند خویشتن به شرک اکنون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رای سیفی سرای پرده فتح</p></div>
<div class="m2"><p>سوی هندوستان برد بیرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تف تیغ لشکر اسلام</p></div>
<div class="m2"><p>بر رگ کفر در بجوشد خون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میغ بندد بلا و ژاله زند</p></div>
<div class="m2"><p>بشکند پشت کفر و کافر دون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه چنان ژاله کش بگرداند</p></div>
<div class="m2"><p>ژاله را نان ز کشتها بفسون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک جهان بت پرست و بت بینی</p></div>
<div class="m2"><p>لگد روزگار کرده نگون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پای رایان گرفته دست زمین</p></div>
<div class="m2"><p>بشکم درکشیده چون قارون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسروا چون ولایت آذر</p></div>
<div class="m2"><p>آمد اندر تصرف کانون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رزم را آذری فروز چنانک</p></div>
<div class="m2"><p>دل مهیال باشدش کانون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آذری کز نهیب سوزش او</p></div>
<div class="m2"><p>شوربخت است راسل ملعون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آتشی کاندر او دو جوهر اوست</p></div>
<div class="m2"><p>جوهر دیو پال بود اندون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا چو پروانه حرص جمع کند</p></div>
<div class="m2"><p>خلق را گرد آتش التون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باره ملک را تو دار قوی</p></div>
<div class="m2"><p>خانه عدل را تو باش ستون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>امر تو باد بر زمانه روان</p></div>
<div class="m2"><p>عمر تو باد با ابد مقرون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیک خواهانت مقبل و شادان</p></div>
<div class="m2"><p>بدسگالانت مدبر و محزون</p></div></div>