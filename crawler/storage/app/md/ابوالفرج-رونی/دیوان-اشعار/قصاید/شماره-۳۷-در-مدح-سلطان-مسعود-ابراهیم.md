---
title: >-
    شمارهٔ  ۳۷ - در مدح سلطان مسعود ابراهیم
---
# شمارهٔ  ۳۷ - در مدح سلطان مسعود ابراهیم

<div class="b" id="bn1"><div class="m1"><p>ای ملک را جمال تو افزوده کار و بار</p></div>
<div class="m2"><p>مسعود بیخ و شاخ تو مسعود برگ و بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرسوده زیر پایه قدر تو آسمان</p></div>
<div class="m2"><p>آسوده زیر سایه چتر تو روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم کف ذات جود ترا میغ درفشان</p></div>
<div class="m2"><p>هم عکس حزم رای ترا تیغ جزع بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عهد زمانه عهد تو آورده بر کتف</p></div>
<div class="m2"><p>دور سپهر دور تو پرورده در کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغ نشسته حزم تو از اختیار چرخ</p></div>
<div class="m2"><p>ناظر نشانده عزم تو در عین اختیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناداشته به پاس تو یک تاج تاجور</p></div>
<div class="m2"><p>نایافته برفق تو یک شهر شهریار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلطان دادگستری و شاه دین پناه</p></div>
<div class="m2"><p>بحر ستم نوردی و خورشید حق گذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیتی دل تو جوید هر ساعت اند ره</p></div>
<div class="m2"><p>گردون در تو گیرد هر لحظه اندبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتش به فخر یال به عیوق برکشد</p></div>
<div class="m2"><p>چون همت تو بیند تن در دهد به عار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دندان و چنگ درد در کام و کف پلنگ</p></div>
<div class="m2"><p>از هیبت تو دایم در پره شکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شرق امید خواند رای تو را قضا</p></div>
<div class="m2"><p>کز جیب آن شکافد صبح امید وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رجم شهاب گوید سهم ترا قدر</p></div>
<div class="m2"><p>کز زخم آن خروشد شیطان جان سپار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخش درخش نعل ملک راست درنبرد</p></div>
<div class="m2"><p>آری درخش باشد از اینگونه تابدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایدون سبک ستاند سیرش ز خاک پی</p></div>
<div class="m2"><p>گوئی نیافت خواهد باد از پیش غبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش از خیال خویش گه حمله قالبش</p></div>
<div class="m2"><p>لشکر فرو گذارد در دیده سوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صمصام شاه چون ز هنر چاشنی دهد</p></div>
<div class="m2"><p>زخمش برابر آید با زخم ذوالفقار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با حد او نگنجد حد فلک بدانج</p></div>
<div class="m2"><p>قدش دو مغزه گردد چون قد ذوالخمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاها خدایگانا اکنون که از خزان</p></div>
<div class="m2"><p>آمد شکست فاحش در نوبت بهار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لشکر ز سردسیر فراران به گرم سیر</p></div>
<div class="m2"><p>چون لشکر کلنگ قطار از پس قطار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قنوج را و بانرسی را خطر منه</p></div>
<div class="m2"><p>این را گرفته انگار آن را زده شمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>«گه مال و دست و حشمت بر سمت او فکن</p></div>
<div class="m2"><p>گه فتح و عون ایزد بر فتح برگمار»</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>معبود مشرکان را زانجا کشان کشان</p></div>
<div class="m2"><p>برپای پیل بسته به خاری به حضرت آر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا ز آستین صنع برآید گشاده چنگ</p></div>
<div class="m2"><p>بر ساعد چنار قوی پنجه چنار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شمشیر امر و نهیی با دشمنان بکوش</p></div>
<div class="m2"><p>باران عدل و فضلی بر دوستان به بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بهتر به طاعت اندر امروز توزدی</p></div>
<div class="m2"><p>خوشتر به نعمت اندر امسال تو ز پار</p></div></div>