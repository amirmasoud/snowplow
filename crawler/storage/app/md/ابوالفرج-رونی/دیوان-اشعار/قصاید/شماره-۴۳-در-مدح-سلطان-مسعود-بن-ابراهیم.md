---
title: >-
    شمارهٔ  ۴۳ - در مدح سلطان مسعود بن ابراهیم
---
# شمارهٔ  ۴۳ - در مدح سلطان مسعود بن ابراهیم

<div class="b" id="bn1"><div class="m1"><p>گاه مسعود تاجدار ملک</p></div>
<div class="m2"><p>تاج ماه است گاه بار ملک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک آورده یمن و یسر از خلد</p></div>
<div class="m2"><p>به یمین داده و یسار ملک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رانده کلک شمار گیر قضا</p></div>
<div class="m2"><p>عدلی عدل در شمار ملک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده رای قطار دار قدر</p></div>
<div class="m2"><p>بختی بخت در قطار ملک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرسد عقل اگر دو اسبه رود</p></div>
<div class="m2"><p>در تک وهم بی غبار ملک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه شاهین آسمان سنجد</p></div>
<div class="m2"><p>خوار سنجد مگر عیار ملک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برگرفت آدمی و دیو و پری</p></div>
<div class="m2"><p>مذهب و سنت و شعار ملک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دین و دنیا بیافرید و نهاد</p></div>
<div class="m2"><p>آفریننده در کنار ملک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب از فلک نیارد خواست</p></div>
<div class="m2"><p>شرف عرض حق گذار ملک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زحل از قوس برنداند داشت</p></div>
<div class="m2"><p>قزح نفس شاد خوار ملک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب دارد که آتش افروزد</p></div>
<div class="m2"><p>جوهر تیغ آبدار ملک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بار گیرد چو خاک پیماید</p></div>
<div class="m2"><p>جرم یکران بی قرار ملک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ماه چون سنگ پشت سر به کتف</p></div>
<div class="m2"><p>درکشد روز کارزار ملک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا ذنب وار نور او نبرد</p></div>
<div class="m2"><p>سایه گرز گاو سار ملک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ویحک آن کوکب عجول چه بود</p></div>
<div class="m2"><p>که قران کرد با وقار ملک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منزلی تاخت عالمی پرداخت</p></div>
<div class="m2"><p>عزم کوه و کمر گذار ملک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کشوری سوخت لشکری افروخت</p></div>
<div class="m2"><p>رزم پرشعله و شرار ملک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرد افغان و جت به رغبت و حرص</p></div>
<div class="m2"><p>پرده زد موکب سوار ملک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز شکاری برون نشد ز میان</p></div>
<div class="m2"><p>یک تن از پره شکار ملک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر بدان کوه پایه بازرسی</p></div>
<div class="m2"><p>کاندر او فتح بود یار ملک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشنواند صدای کوه ترا</p></div>
<div class="m2"><p>جز همه کر و فر کار ملک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تن به قربان مشرکان در داد</p></div>
<div class="m2"><p>اندرین عید ذوالفقار ملک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به چنین رسم تا جهان باشد</p></div>
<div class="m2"><p>مقتدا باد روزگار ملک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بارور گشته سال و مه به ظفر</p></div>
<div class="m2"><p>شاخ شاداب اختیار ملک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دست بر سر گرفته والی ظلم</p></div>
<div class="m2"><p>از ره بند و گیر و دار ملک</p></div></div>