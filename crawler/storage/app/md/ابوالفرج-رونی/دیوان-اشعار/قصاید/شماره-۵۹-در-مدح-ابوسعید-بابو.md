---
title: >-
    شمارهٔ  ۵۹ - در مدح ابوسعید بابو
---
# شمارهٔ  ۵۹ - در مدح ابوسعید بابو

<div class="b" id="bn1"><div class="m1"><p>آمد آن تیر ماه سرد سخن</p></div>
<div class="m2"><p>گرم در گفتگوی شد با من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر او در سؤال با من تیز</p></div>
<div class="m2"><p>بم من در جواب او الکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه مرا با تکاب او پایاب</p></div>
<div class="m2"><p>نه مرا با گشاد او جوشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرصهای بنات نعش تنم</p></div>
<div class="m2"><p>گشت از او تنگ تر ز شکل پرن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه های گل است پنداری</p></div>
<div class="m2"><p>همه اطراف من کفیده دهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غربت و عزل ای مسلمانان</p></div>
<div class="m2"><p>به زمستان نبرده بودم ظن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیولاخی چنین که دیو همی</p></div>
<div class="m2"><p>زو به دوزخ فرو خزد برسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جویش از آب بسته پر سیماب</p></div>
<div class="m2"><p>کوهش از برق جسته بر آهن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مسام زمین گذشته هواش</p></div>
<div class="m2"><p>چون به درز حریر در سوزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من مسکین مقیم گشته در او</p></div>
<div class="m2"><p>اهل بدرود کرده و مسکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مار کردار دست و پای مرا</p></div>
<div class="m2"><p>شکم از آستین و از دامن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدن از سنگ نی و از آتش طبع</p></div>
<div class="m2"><p>بی خبر مانده کوره های بدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ درمان و هیچ حیلت نی</p></div>
<div class="m2"><p>جز بر خواجه عمید شدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا فرو پوشدم به آذر ماه</p></div>
<div class="m2"><p>ز آفتاب تموز پیراهن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواجه بوسعد بابو آنکه نهد</p></div>
<div class="m2"><p>کشف قدرش بگرد مه خرمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حکم او را قضا جواد عنان</p></div>
<div class="m2"><p>امر او را زمانه خوش گردن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عزم و حزمش دو نفس هر دو قوی</p></div>
<div class="m2"><p>خلق و خلقش دو نقش هر دو حسن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تفاخر چو کرم پیله سپهر</p></div>
<div class="m2"><p>تار مهرش تنیده بر سر و تن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در ترازوی همت اعلاش</p></div>
<div class="m2"><p>دانگ سنگ آمدست پر و پرن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>موش سوراخ غور کینه او</p></div>
<div class="m2"><p>کرده افسوس بر چه بیژن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز آفرینش برون نهاده قدم</p></div>
<div class="m2"><p>نظر رحم او بمرد و بزن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوستان سعادتش فلکی است</p></div>
<div class="m2"><p>چون مجره در او هزار چمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تربتش عین منشاء احرار</p></div>
<div class="m2"><p>بدل نشو عرعر و سوسن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طفل او چون رسیده غنچه گل</p></div>
<div class="m2"><p>پیر او چون جوانه شاخ سمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یارنی با نعیمهاش زوال</p></div>
<div class="m2"><p>جفت نی با سرورهاش حزن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میوه دارانش میوه دلها</p></div>
<div class="m2"><p>بعضی آورده بعضی آبستن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای ز اصل کرم «عزیز» نهال</p></div>
<div class="m2"><p>وز نهال شرف بدیع فتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زنده کی ماندن این چراغ امید</p></div>
<div class="m2"><p>گر ز جودش نیامدی روغن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر که حرز سخات بر جان بست</p></div>
<div class="m2"><p>نایدش دیو فقر پیرامن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنده بی موی روبه بلغار</p></div>
<div class="m2"><p>زده بر ابره ها خز ادکن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه همانا که بر تواند کند</p></div>
<div class="m2"><p>سبلت از روی او دی و بهمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا جهان را ز گردش گردون</p></div>
<div class="m2"><p>شب و روز است تیره و روشن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مجلسی باد نیک خواه ترا</p></div>
<div class="m2"><p>با می و با مغنی و گلشن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خانه ای باد بدسگال ترا</p></div>
<div class="m2"><p>بی در و بی دریچه و روزن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طبع تو زورمند روزه گشا</p></div>
<div class="m2"><p>عمر تو روزمند و عیدافکن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لفظها را ثنای تو دستان</p></div>
<div class="m2"><p>فرقها را مدیح تو گرزن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>«بوالفرج را ز غایت اخلاص</p></div>
<div class="m2"><p>در مدیح تو حور روح سخن »</p></div></div>