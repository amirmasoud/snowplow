---
title: >-
    شمارهٔ  ۷۰ - در مدح سلطان مسعود بن ابراهیم همانا در سال فراز آمدن آن پادشاه والاجاه بر تخت پادشاهی گفته شده است
---
# شمارهٔ  ۷۰ - در مدح سلطان مسعود بن ابراهیم همانا در سال فراز آمدن آن پادشاه والاجاه بر تخت پادشاهی گفته شده است

<div class="b" id="bn1"><div class="m1"><p>درود داد خلافت رسید و عهد و لوی</p></div>
<div class="m2"><p>به بارگاه همایون حضرت اعلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بارگاهی کز فخر خلعتش جوید</p></div>
<div class="m2"><p>ز ظل پرده او دوش آفتاب ردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بارگاهی کز حرص طاعتش خواهد</p></div>
<div class="m2"><p>ز لفظ حاجب او گوش روزگار ندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تیر ماه بهاری شگفت حضرت را</p></div>
<div class="m2"><p>گشاده چهره تر از کارنامه مانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل نشاط و سرورش به رنگ معجب گشت</p></div>
<div class="m2"><p>هنوز عهد و لوی ناگرفته بوی نوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی برای تماشا به خشک رود برآی</p></div>
<div class="m2"><p>کری کند که برآئی به خشک رود کری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«نهاده گوئی رضوان به شاهراهش پر</p></div>
<div class="m2"><p>میان هر دو سه گامی نهالی از طوبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شکل و هیئت جرم سپهر معذور است</p></div>
<div class="m2"><p>اگر نیارد با او به قبه کرد مری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرد به ساحت او بر دلیل قربان دید</p></div>
<div class="m2"><p>چنانکه عادت باشد به موسم اضحی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به نفس ناطقه تکبیر کرد و ایدون گفت</p></div>
<div class="m2"><p>که قصر خسرو کعبه است و خشک رود منی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزرگوارا شهرا که شهر غزنین است</p></div>
<div class="m2"><p>چه شهر عالم کبری به عالم صغری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آنکه عالم صغری ز خشک رودش خود</p></div>
<div class="m2"><p>نباشد الا عضوی کمینه از عضوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدای تربت او را عزیز دنیا کرد</p></div>
<div class="m2"><p>به فر مولد میمون خسرو دنیی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نظام دولت محمودیان ملک مسعود</p></div>
<div class="m2"><p>امین عهد و امام و یمین دین و هدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ستوده سیرت شاهی که روزه مظلمتش</p></div>
<div class="m2"><p>بدو پناهد عالم ز سیرت کبری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به عزم تیزتر از برق راند خنگ ظفر</p></div>
<div class="m2"><p>به حفظ نرم تر از آب کرد صحف نبی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گشاده رایت منصور او در قنوج</p></div>
<div class="m2"><p>شکسته هیبت شمشیر او دل ملهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مدار هیچ عجب گر ز حول قوت او</p></div>
<div class="m2"><p>به شرق و غرب نیابند فتنه را مأوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به ایمنیش برون تازد از کمین مهدی</p></div>
<div class="m2"><p>به دوستیش فرود آید از فلک عیسی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همیشه تا نبود کبک را پر شاهین</p></div>
<div class="m2"><p>همیشه تا نبود بنده را دل مولی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپهر موکب او باد و مهر مرکب او</p></div>
<div class="m2"><p>ستاره کفش بساط و زمانه کبش فدی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>براق همت او اوج مشتری و زحل</p></div>
<div class="m2"><p>سریر دولت او فرق فرقد و شعری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه از جمالش طبع جمال را سیری</p></div>
<div class="m2"><p>نه در کمالش عین کمال را دعوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدین عیار سپرده رسول و آل رسول</p></div>
<div class="m2"><p>به تخت ملکش تشریف تاج و عهد و لوی</p></div></div>