---
title: >-
    شمارهٔ  ۳ - در مدح سیف الدوله محمود ابراهیم
---
# شمارهٔ  ۳ - در مدح سیف الدوله محمود ابراهیم

<div class="b" id="bn1"><div class="m1"><p>بادبان برکشید باد صبا</p></div>
<div class="m2"><p>معتدل گشت باز طبع هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک دیبا شدست پر صورت</p></div>
<div class="m2"><p>جانور گشته صورت دیبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاخ چون کرم پیله گوهر خویش</p></div>
<div class="m2"><p>برتند گرد تن همی عمدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبزه اندر حمایت شبنم</p></div>
<div class="m2"><p>سر ز پستی کشید بر بالا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر بی شرط مهر و عقد نکاح</p></div>
<div class="m2"><p>گشت حامل به لؤلؤ لالا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینک از شرم آن همی فکند</p></div>
<div class="m2"><p>لؤلؤ نارسیده بر صحرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمها بر گشاده غنچه گل</p></div>
<div class="m2"><p>تا به بیند جمال خسرو ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنجها بر فراخت سرو سهی</p></div>
<div class="m2"><p>تا کند بر کمال شاه دعا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر محمود سیف دولت و دین</p></div>
<div class="m2"><p>آن فلک سیرت فلک سیما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه اندر ابد نظر کرد است</p></div>
<div class="m2"><p>سوی عدلش قضا بعین رضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه اندر ازل کمربسته است</p></div>
<div class="m2"><p>بر فلک پیش طالعش جوزا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیبتش جوهری ست از آتش</p></div>
<div class="m2"><p>همتش عالمی ست از علیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کجا پاس اوست نیست خطر</p></div>
<div class="m2"><p>هر کجا خوف اوست نیست رجا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سهم او رعد و برق را بنمود</p></div>
<div class="m2"><p>گفت از این اصل گشته ایم جدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نکشد بار حلم او کونین</p></div>
<div class="m2"><p>چون کشد طبع او همی تنها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای متابع ترا سپاه زمین</p></div>
<div class="m2"><p>وی موافق ترا نجوم سما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ز مهر تو دانه سازد عقل</p></div>
<div class="m2"><p>اندر آید به دام او عنقا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور ز جود تو مایه گیرد روح</p></div>
<div class="m2"><p>ذات او صورتی شود پیدا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا برآرد هزار لعب همی</p></div>
<div class="m2"><p>در شبان روز گنبد خضرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه امروزهای دولت تو</p></div>
<div class="m2"><p>باز پیوسته باد با فردا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دهر پیش تو مانده دست بکش</p></div>
<div class="m2"><p>چرخ پیش تو گشته (کرده) پشت دو تا</p></div></div>