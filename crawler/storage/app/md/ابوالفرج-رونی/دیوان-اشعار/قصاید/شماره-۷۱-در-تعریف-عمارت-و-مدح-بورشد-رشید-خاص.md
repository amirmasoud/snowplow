---
title: >-
    شمارهٔ  ۷۱ - در تعریف عمارت و مدح بورشد رشید خاص
---
# شمارهٔ  ۷۱ - در تعریف عمارت و مدح بورشد رشید خاص

<div class="b" id="bn1"><div class="m1"><p>ای همایون بنای آهو پای</p></div>
<div class="m2"><p>آهویی نانهاده در تو خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمن از مکر و قصد یکدیگر</p></div>
<div class="m2"><p>در تو شیران و آهوان سرای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سقف تو چون فلک نگارپذیر</p></div>
<div class="m2"><p>حسن تو چون بهشت روح افزای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش دلبند دلگشای ترا</p></div>
<div class="m2"><p>خامه فتنه بود چهره گشای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده با مطربان صدای خمت</p></div>
<div class="m2"><p>به نشاط تمام هایاهای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته با زایران صریر درت</p></div>
<div class="m2"><p>مرحبا مرحبا درآی درآی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی دیوار تو ز بس پیکر</p></div>
<div class="m2"><p>شکل عالم گرفته سر تا پای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم در او مرکبان گور سرین</p></div>
<div class="m2"><p>هم در او سرکشان تیغ گرای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورده آسیب شیر او نخجیر</p></div>
<div class="m2"><p>مانده خرطوم پیل او در وای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست چنگیش بر دویده به چنگ</p></div>
<div class="m2"><p>لب نائیش دردمیده بنای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می پرستش میی چشیده به رنگ</p></div>
<div class="m2"><p>رشگ تاج خروس و چشم همای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوده از رزمگاه مجلس او</p></div>
<div class="m2"><p>قالب رزم خواه بزم آرای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک آرام داده هر یک را</p></div>
<div class="m2"><p>حشمت خاص شاه بر یک جای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناصر حق جمال ملت و ملک</p></div>
<div class="m2"><p>صدر دنیا رشید روشن رای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه با عدل او نیارد گفت</p></div>
<div class="m2"><p>سخن کاه طبع کاه ربای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وآنکه بی حرز او نداند گشت</p></div>
<div class="m2"><p>گرد سوراخ مارمار افسای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دایمش در چنین بنا خواهم</p></div>
<div class="m2"><p>شاد کامی و خرمی افزای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سایه قصر او نپیموده</p></div>
<div class="m2"><p>قرص خورشید آسمان پیمای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جامه عمر او نفرسوده</p></div>
<div class="m2"><p>گردش گنبد جهان فرسای</p></div></div>