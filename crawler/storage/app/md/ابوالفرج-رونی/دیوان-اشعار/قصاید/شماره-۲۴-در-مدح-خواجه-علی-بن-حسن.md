---
title: >-
    شمارهٔ  ۲۴ - در مدح خواجه علی بن حسن
---
# شمارهٔ  ۲۴ - در مدح خواجه علی بن حسن

<div class="b" id="bn1"><div class="m1"><p>میزان فلک قسم شب و روز جدا کرد</p></div>
<div class="m2"><p>از روز نوا بستد و شب را به نوا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سخت به انصاف همین را و همان را</p></div>
<div class="m2"><p>چون هر دو به تقویم رسیدند رها کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی بی سبب آمد به میان اندر میزان</p></div>
<div class="m2"><p>احکام قضا راند و ازین حکم قضا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود حال بدینگونه کجا ماند فردا</p></div>
<div class="m2"><p>شب نیز دعا گوید چون روز دعا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ساعت او شرع کند شش مه و شاید</p></div>
<div class="m2"><p>زیرا که جفا بیند هر کس که جفا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای طبع ره و رسم شب و روز چه دانی</p></div>
<div class="m2"><p>گر عقل بر این داشت ترا عقل خطا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خواجه علی بن . . . مدح و ثناگوی</p></div>
<div class="m2"><p>کاوقات شب و روز بر او مدح و ثنا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بار خدائی که اهل نهمت عالم</p></div>
<div class="m2"><p>در همت او بسته و تا خواست وفا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد بار به چنگ آمد معلوم جهانش</p></div>
<div class="m2"><p>زین دست به چنگ آمد و زان دست عطا کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از چرخ مشعبد نخورد شعبده لیکن</p></div>
<div class="m2"><p>خواهنده برو شعبده طمع روا کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جودش نه حیائی ست طبیعی و حقیقی است</p></div>
<div class="m2"><p>علت نپذیرد که به تکلیف حیا کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آری چو سخاوت را اصل از عرب آمد</p></div>
<div class="m2"><p>نشگفت که با اصل عرب خواجه سخا کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن ست که در دولت او گردش گردون</p></div>
<div class="m2"><p>اصحاب بلا را به بلا جفت عنا کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>واتست که از حشمت او حادثه دهر</p></div>
<div class="m2"><p>انگشت سرو آنجا کانگشت فرا کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او دار و نقیض است به کردار و به دیدار</p></div>
<div class="m2"><p>این شغل ملاراند و آن شغل خلا کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از رحمت کردارش با چرخ زمین گشت</p></div>
<div class="m2"><p>چون قدرت دیدارش با آب هوا کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای معجزه عدل تو با جادوی ظلم</p></div>
<div class="m2"><p>آن کرد که با جادوی کفر عصا کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از بنده اگر پرسد حاسد که خداوند</p></div>
<div class="m2"><p>این شغل ز تو بنده جدا کرد چرا کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تدبیر جز آن نیست که تقصیر نهد عذر</p></div>
<div class="m2"><p>گوید که ندانستم خدمت به سزا کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جاوید بقا بادت باعز و بزرگی</p></div>
<div class="m2"><p>کاین عز و بزرگی به بقای تو بقا کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدخواه ترا ظاهر چون روی علا باد</p></div>
<div class="m2"><p>تا با تو چرا باطن خود همچو علا کرد</p></div></div>