---
title: >-
    شمارهٔ  ۷۵ - ایضاً له
---
# شمارهٔ  ۷۵ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>زهی دست وزارت از تو با زور</p></div>
<div class="m2"><p>ندیده چشم گیتی چون تو دستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ربیب الدین و دولت ای ز رایت</p></div>
<div class="m2"><p>گرفته دین و دولت حظ موفور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تو بنیاد دولت سقف مرفوع</p></div>
<div class="m2"><p>ز تو صدر وزارت بیت معمور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عدلت لشکر بیداد مخذول</p></div>
<div class="m2"><p>ز حکمت رایت اقبال منصور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دیده خاطرت امروز رازی</p></div>
<div class="m2"><p>که اندر پرده فرداست مستور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی تابد ز نور روی و رایت</p></div>
<div class="m2"><p>جهان ملک را نور علی نور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تو دست وزارت آن شرف یافت</p></div>
<div class="m2"><p>که موسی کلیم از ذروه طور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه در خوابی است بخت حاسد تو</p></div>
<div class="m2"><p>که بیدارش کند جز نفخه صور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به توقیعت چو شد منشور مطوی</p></div>
<div class="m2"><p>همانگه شد لوای حمد منشور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز توقیع همایون تو گردد</p></div>
<div class="m2"><p>چو از لاحول دیو فتنه مدحور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عهدی کز تحکم بر قلم داشت</p></div>
<div class="m2"><p>نفاذ تیغ یازان گشت مغرور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندیدم عهد میمونت که در وی</p></div>
<div class="m2"><p>قلم را تیغ شد منهی و مأمور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو آید در لطافت ذوق طبعت</p></div>
<div class="m2"><p>نماید نوش نحل از نیش زنبور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو گردد رایت رای تو مرفوع</p></div>
<div class="m2"><p>شود خیل عدو مکسور و مجرور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا زان دولت و عمر است ممدود</p></div>
<div class="m2"><p>که داری همتی بر عدل مقصور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخا وجود گنجی دان که امروز</p></div>
<div class="m2"><p>دل و دستت بدان گنج است گنجور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر صاحب ابوالقاسم در آن عهد</p></div>
<div class="m2"><p>برادی و کفایت بود مشهور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ربیب الدین ابوالقاسم در این عهد</p></div>
<div class="m2"><p>توئی مانند او مشهور و مذکور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه چندانت مکارم جمع شدکان</p></div>
<div class="m2"><p>به آسانی بود معدود و محصور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه مرد باشق و باز است تیهو</p></div>
<div class="m2"><p>چه هم ناورد شاهین است عصفور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو فردی در کفایت ور کسی را</p></div>
<div class="m2"><p>همی گویند آن قولی بود زور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بران کافی نباشد اعتمادی</p></div>
<div class="m2"><p>بسی باشد سیه را نام کافور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منم عالی جنابت را دعاگو</p></div>
<div class="m2"><p>گر از نزدیک نتوانم هم از دور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بران منگر که از نور جمالت</p></div>
<div class="m2"><p>به کنجی مانده ام ممنوع و مهجور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببین کاندر دعای دولت تو</p></div>
<div class="m2"><p>سخن می پرورم منظوم و منثور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دعا نیکوترین چیزی است کان را</p></div>
<div class="m2"><p>شمارد مرد عاقل گنج مذخور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مبارک دان دعای گوشه گیران</p></div>
<div class="m2"><p>به روز روشن و شبهای دیجور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه تا کریمان را به گیتی</p></div>
<div class="m2"><p>بماند نام باقی سعی مشکور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مقدم باد بر هم نام نامت</p></div>
<div class="m2"><p>چو قرآن بر همه مسموع و مأثور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه دوستانت شاد و خرم</p></div>
<div class="m2"><p>همیشه دشمنان مخذول و مقهور</p></div></div>