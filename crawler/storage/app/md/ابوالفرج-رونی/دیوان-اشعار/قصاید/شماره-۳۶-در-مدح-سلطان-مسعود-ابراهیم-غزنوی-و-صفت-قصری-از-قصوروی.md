---
title: >-
    شمارهٔ  ۳۶ - در مدح سلطان مسعود ابراهیم غزنوی و صفت قصری از قصوروی
---
# شمارهٔ  ۳۶ - در مدح سلطان مسعود ابراهیم غزنوی و صفت قصری از قصوروی

<div class="b" id="bn1"><div class="m1"><p>این بهار طرب نهال سرور</p></div>
<div class="m2"><p>که به فرمان شاه شد معمور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روضه عشر تست و بیضه لهو</p></div>
<div class="m2"><p>موقف رامش است و موضع سور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب او آب زمزم و کوثر</p></div>
<div class="m2"><p>خاک او خاک عنبر و کافور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکل او نابسوده دست صبا</p></div>
<div class="m2"><p>شبه او ناسپرده پای دبور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفت او به گوش دل نزدیک</p></div>
<div class="m2"><p>صورت او ز چشم حادثه دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شده بر مدح مادحش مولع</p></div>
<div class="m2"><p>گشته در عشق عاشقش معذور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوئی از مایه مزاج فلک</p></div>
<div class="m2"><p>قبه رست از زمین پر نور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلقا با بهشت سوده عنان</p></div>
<div class="m2"><p>به بقا یافت از ازل منشور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کامران باد و کامکار در او</p></div>
<div class="m2"><p>خسرو عصر در سنین و شهور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پشت محمودیان ملک مسعود</p></div>
<div class="m2"><p>روی بازار دولت منصور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه جوید رضای او قیصر</p></div>
<div class="m2"><p>وانکه دارد هوای او فغفور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه در قمع کفر و نصرت حق</p></div>
<div class="m2"><p>ننگرد همتش به حور و قصور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وانکه از عدل او رحیق شود</p></div>
<div class="m2"><p>آب مسموم در دم زنبور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانکه در ملک او جدا ماند</p></div>
<div class="m2"><p>چنگ شاهین ز دامن عصفور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا ز لهو و نشاط بهره دهند</p></div>
<div class="m2"><p>ناله چنگ و نغمه طنبور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه را در چنین بنا خواهم</p></div>
<div class="m2"><p>شده خرم ز شیره انگور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راوی بنده خوانده در مجلس</p></div>
<div class="m2"><p>مدحت فتح مرو و نیشابور</p></div></div>