---
title: >-
    شمارهٔ  ۲۶ - ایضاً له
---
# شمارهٔ  ۲۶ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>خسروا بخت پاسبان تو باد</p></div>
<div class="m2"><p>قاهر دهر قهرمان تو باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشتری نامور به نام تو گشت</p></div>
<div class="m2"><p>بشری جانور به جان تو باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر کیوان و تندی بهرام</p></div>
<div class="m2"><p>از رکاب تو و عنان تو باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منبر عدل و خطبه انصاف</p></div>
<div class="m2"><p>در زمین تو و زمان تو باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شجر دولت موافق را</p></div>
<div class="m2"><p>نشو در صحن بوستان تو باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جگر تشنه مخالف را</p></div>
<div class="m2"><p>آب از چشمه سنان تو باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روش مسرعان سهم الغیب</p></div>
<div class="m2"><p>همه بر شه زه کمان تو باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاف پرتابیان شست شهاب</p></div>
<div class="m2"><p>همه از قبضه کمان تو باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه در ملک روزگار آید</p></div>
<div class="m2"><p>بذل آن پیشه به نان تو باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه بر عقل مشتبه گردد</p></div>
<div class="m2"><p>کشف آن سخره بیان تو باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لب دریا به موج خیز اندر</p></div>
<div class="m2"><p>حاکی و راوی جنان تو باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جرم مه چون هلال و بدر شود</p></div>
<div class="m2"><p>نعل یکران و قرص خوان تو باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر قضا آسمان بفرساید</p></div>
<div class="m2"><p>اوج قدر تو آسمان تو باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور فنا بر جهان ببخشاید</p></div>
<div class="m2"><p>عرصه فضل تو جهان تو باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا کمر صحبت میان طلبد</p></div>
<div class="m2"><p>کمر ملک بر میان تو باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شکر شکر نعمت ایزد</p></div>
<div class="m2"><p>قسم کام تو و زبان تو باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فتح قنوج و صید شاه آورد</p></div>
<div class="m2"><p>اصل دستان و داستان تو باد</p></div></div>