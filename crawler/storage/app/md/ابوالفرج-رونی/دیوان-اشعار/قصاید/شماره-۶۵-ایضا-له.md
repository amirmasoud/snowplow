---
title: >-
    شمارهٔ  ۶۵ - ایضاً له
---
# شمارهٔ  ۶۵ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>ای سرافراز تاج و والاگاه</p></div>
<div class="m2"><p>ملک را تهنیت کنید به شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه مسعود کز قران سعود</p></div>
<div class="m2"><p>نظرش قدر بیش دارد و جاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکد بی مدح او فلک ننهاد</p></div>
<div class="m2"><p>تیغهای کلام در افواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وآنکه بی نام او زمانه نکرد</p></div>
<div class="m2"><p>حجت وقف ملک و سعی گواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوستانیست عدل او خرم</p></div>
<div class="m2"><p>قهرمانی است پاس او برناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زود دو عزم او فراز و نشیب</p></div>
<div class="m2"><p>تیز بین حزم او سپید و سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حکم او قاضی زمین و زمان</p></div>
<div class="m2"><p>امر او والی سپهر و سپاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتح باب عنایتش بکرم</p></div>
<div class="m2"><p>بد ماند ز شوره مهر گیاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«آفتاب کفایتش بطلوع</p></div>
<div class="m2"><p>آتش اندر زند به سایه چاه »</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه رایش محرمان زمین</p></div>
<div class="m2"><p>چاره یابند بحر را بشناه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز بارش مدبران فلک</p></div>
<div class="m2"><p>خاک روبند پیش او بجباه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تازه گشت از جلوس معجز او</p></div>
<div class="m2"><p>شرط پاداش و رسم باد افراه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خیره ماند از قیام غالب او</p></div>
<div class="m2"><p>حمله شیر و حیله روباه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کوه ببسود زخم تیرش گفت</p></div>
<div class="m2"><p>صاعقه است این نه تیر واغوثاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه دراز و دراز یازش او</p></div>
<div class="m2"><p>امل خصم را کند کوتاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یارب این سهمناک روز چه بود</p></div>
<div class="m2"><p>داعی فتنه اندر او پنجاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه دعوی پرست و فرصت جوی</p></div>
<div class="m2"><p>همه معنی گذار و بیعت خواه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه عرق و رحم سپرده بپای</p></div>
<div class="m2"><p>همه عهد و وفا فکنده براه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خسرو اندر مقام پیروزی</p></div>
<div class="m2"><p>سوده اوج هوا به پر کلاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باره در زیر ران چو هیکل چرخ</p></div>
<div class="m2"><p>چتر از افراز سر چو خرمن ماه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خاصگانش باهل بغی و خروج</p></div>
<div class="m2"><p>اندر افتاده با دوار بکاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ده ده آورده پیش او طاغی</p></div>
<div class="m2"><p>یک یک اندامشان مقر بگناه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک خسروا کیا شاها</p></div>
<div class="m2"><p>دولت افزای و کام حاسد کاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا همی تابد آفتاب بفلک</p></div>
<div class="m2"><p>بر سرما تو باش ظل الله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کار تو غزو باد و یار تو حق</p></div>
<div class="m2"><p>عرش تو تاج باد و فرش تو گاه</p></div></div>