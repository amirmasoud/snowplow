---
title: >-
    شمارهٔ  ۸۰ - ایضاً له
---
# شمارهٔ  ۸۰ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>حضرتی شد بزرگ چون غزنین</p></div>
<div class="m2"><p>لاهوار از قدوم شاه زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشت مسعودیان ملک مسعود</p></div>
<div class="m2"><p>روی بازار آل ناصر دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاجور خسروی که رشک برد</p></div>
<div class="m2"><p>به شب از در تاج او پروین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه ماهی است روشن اندر صدر</p></div>
<div class="m2"><p>وانکه شیری است شرزه اندر زین</p></div></div>