---
title: >-
    شمارهٔ  ۳۱ - ایضاً له
---
# شمارهٔ  ۳۱ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>ساقیا جام دل افروز بیار</p></div>
<div class="m2"><p>فتح شه یاد کن و می بگسار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتح قنوج که شمشیرش کرد</p></div>
<div class="m2"><p>اندرین فتح شه آورد شکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لشکرش گرد برآورد از خون</p></div>
<div class="m2"><p>هیبتش کوه فرو برده به غار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شل او بر کتف گرگ نشست</p></div>
<div class="m2"><p>جوهر گرگ فرو ماند ز کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرعه او به لب شیر رسید</p></div>
<div class="m2"><p>بسر شیر درافتاد خمار</p></div></div>