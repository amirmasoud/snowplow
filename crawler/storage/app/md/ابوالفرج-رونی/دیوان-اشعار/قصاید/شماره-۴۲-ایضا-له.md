---
title: >-
    شمارهٔ  ۴۲ - ایضاً له
---
# شمارهٔ  ۴۲ - ایضاً له

<div class="b" id="bn1"><div class="m1"><p>ای چو نام تو اعتقاد تو پاک</p></div>
<div class="m2"><p>انجم همت تو بر افلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غایت شادی تو از رادی</p></div>
<div class="m2"><p>غارت رادی تو از املاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرم خوان قمر ترا سفره</p></div>
<div class="m2"><p>نعل خنک ترا شهاب شراک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وفاقت مجالهای امان</p></div>
<div class="m2"><p>در خلافت مضیقهای هلاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دین حق را نه چون تو یک سرور</p></div>
<div class="m2"><p>ملک شه را نه چون تو یک سر باک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ملک رفق تو بکاود پر</p></div>
<div class="m2"><p>وز فلک باس تو ندارد باک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش برق و بانگ رعد آیند</p></div>
<div class="m2"><p>پیش فرمان امتحان توشاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قعر دریا و بیخ کوه نهند</p></div>
<div class="m2"><p>پیش گرداب و گردباد تو خاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حذق وهم تو در اصابت رای</p></div>
<div class="m2"><p>آفتاب یقین کند کاواک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنگ جود تو در مصیبت مال</p></div>
<div class="m2"><p>بر گریبان نخل بندد چاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرخ زاید ز شهد امن تو موم</p></div>
<div class="m2"><p>زرد روید ز کان خوف تو لاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گهر عقل را تو پالایی</p></div>
<div class="m2"><p>سیم را گرم داروی سباک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلک فضل را تو گردانی</p></div>
<div class="m2"><p>دوک را بادریسه افلاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخردان در تموزها گوئی</p></div>
<div class="m2"><p>از نهال تو برده اند ستاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خشم دیدند مسته حلمت</p></div>
<div class="m2"><p>زهر کردند مسته تریاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منعما مکرما خداوندا</p></div>
<div class="m2"><p>کوته است از تو دست استدراک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دهر چون تو نیاورد چابک</p></div>
<div class="m2"><p>چرخ چون تو نپرورد چالاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنده گر چه ز ناتوانی و ضعف</p></div>
<div class="m2"><p>کوب خورد اندرین سفر حاشاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عزم او باره گرم کرد همی</p></div>
<div class="m2"><p>در فراز و نشیب چون اتراک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خاکهای سیرده زلزله وار</p></div>
<div class="m2"><p>آبهای گذشته ولوله ناک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کوره مالیده قعر او بسمک</p></div>
<div class="m2"><p>پشته پیموده اوج او بسماک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه امیدش آنکه خدمت تو</p></div>
<div class="m2"><p>به سرش برنهد ز بخت بساک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بازگردد عنان گشاده به جای</p></div>
<div class="m2"><p>بسته اشراف پیک بر فتراک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا ببوی و بطعم در عالم</p></div>
<div class="m2"><p>خوش و زفت اوفتند عود و اراک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در صواب و خطا مسیحا باد</p></div>
<div class="m2"><p>کلمات تو دنده حکاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل لهو تو باد بی اندوه</p></div>
<div class="m2"><p>سیل عیش تو باد بی خاشاک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدسکال تو سال و مه ببکا</p></div>
<div class="m2"><p>نیک خواه تو روز و شب ضحاک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>«بود این یک به تخت چون فرخ</p></div>
<div class="m2"><p>بود آن یک به سجن چون ضحاک »</p></div></div>