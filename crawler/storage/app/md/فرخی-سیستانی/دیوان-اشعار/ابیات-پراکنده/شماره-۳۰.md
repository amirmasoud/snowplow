---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>به مهمان هوازی شاد گردم</p></div>
<div class="m2"><p>ز دست رنج و غم آزاد گردم</p></div></div>