---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>بخت شماو عز شما هر دو بر فزون</p></div>
<div class="m2"><p>وان مخالفان و بد اندیش در نهار</p></div></div>