---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>راست گفتی عتاب او بر من</p></div>
<div class="m2"><p>هست از بهر بردن جناب</p></div></div>