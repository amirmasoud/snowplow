---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>تا نبود چون همای فرخ کرگس</p></div>
<div class="m2"><p>همچو نباشد به شبه باز خشین پند</p></div></div>