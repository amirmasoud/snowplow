---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>نامه مانی با نامه تو ژاژست</p></div>
<div class="m2"><p>شعر خوارزمی با شعر تو لامانی</p></div></div>