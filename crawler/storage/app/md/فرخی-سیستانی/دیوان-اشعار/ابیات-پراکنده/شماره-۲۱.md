---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ای کرده مرا خنده خریش همه کس</p></div>
<div class="m2"><p>مارا ز تو بس جانا مارا ز تو بس</p></div></div>