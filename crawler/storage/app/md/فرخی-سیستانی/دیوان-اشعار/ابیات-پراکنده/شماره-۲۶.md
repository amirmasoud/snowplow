---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>زسر ببرد شاخ و ز تن بدرد پوست</p></div>
<div class="m2"><p>بصید گاه ز بهر زه کمان تو رنگ</p></div></div>