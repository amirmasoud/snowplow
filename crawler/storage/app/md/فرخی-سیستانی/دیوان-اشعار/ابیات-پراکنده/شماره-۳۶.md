---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>از ره صورت باشد چون او</p></div>
<div class="m2"><p>گونه عنبر دارد و لادن</p></div></div>