---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>پیوسته مرا همی نمایی بیداد</p></div>
<div class="m2"><p>وانگاه ز من چشم همی داری داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو پنداری که با تو من باشم شاد</p></div>
<div class="m2"><p>زین دستخوشی منت که آگاهی داد</p></div></div>