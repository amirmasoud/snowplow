---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>گویند که معشوق تو زشتست و سیاه</p></div>
<div class="m2"><p>گر زشت و سیاهست مرا نیست گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من عاشقم و دلم بر او گشته تباه</p></div>
<div class="m2"><p>عاشق نبود ز عیب معشوق آگاه</p></div></div>