---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای ساده گل و ساده می و ساده شکر</p></div>
<div class="m2"><p>زین کار که با تو کردم اندوه مخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان باشدکه به شوی جان پدر</p></div>
<div class="m2"><p>حال تو دگر گردد و کار تو دگر</p></div></div>