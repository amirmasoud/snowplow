---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>گفتم که مرا زغم به سه بوسه بخر</p></div>
<div class="m2"><p>دل تافته گشتی و گران کردی سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر سه بوسه ای بت بوسه شمر</p></div>
<div class="m2"><p>چو گاو به چرمگر، به من در منگر</p></div></div>