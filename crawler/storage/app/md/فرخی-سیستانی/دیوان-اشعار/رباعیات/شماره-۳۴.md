---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای دوست مرادید همی نتوانی</p></div>
<div class="m2"><p>بیهوده چرا روی زمن گردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیجرم و جنایتی که از من دانی</p></div>
<div class="m2"><p>چون پیر خر از نیش، زمن ترسانی</p></div></div>