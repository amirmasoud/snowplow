---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>غم دیدم از آن کس که مرا می باید</p></div>
<div class="m2"><p>ببریدم از و تادل من بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نا دیدن او مرا همی بگزاید</p></div>
<div class="m2"><p>گرگ آشتیی کنم چه تا پیش آید</p></div></div>