---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>گویند گرفت یار تو یار دگر</p></div>
<div class="m2"><p>از رشک همی گویند ای جان پدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا تو به گفتگوی ایشان منگر</p></div>
<div class="m2"><p>خر خوبیند که غرقه شد پالانگر</p></div></div>