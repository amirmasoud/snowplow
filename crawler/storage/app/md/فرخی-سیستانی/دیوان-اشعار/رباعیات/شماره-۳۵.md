---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای دوست تر، از دو دیده و بینایی</p></div>
<div class="m2"><p>ای آنکه ز پیش چشم ناپیدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روز که آمدی مرا دربایی</p></div>
<div class="m2"><p>گر تا به قیامت تو غذانی (؟) نایی</p></div></div>