---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>آن مشک سیه که با سمن پیوسته ست</p></div>
<div class="m2"><p>از دیدن او دل جهانی خسته ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب زنخست هم بر آنسان رسته ست</p></div>
<div class="m2"><p>یا او به تکلف فراوان بسته ست</p></div></div>