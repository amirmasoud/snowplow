---
title: >-
    شمارهٔ ۳۷ - در ذکر شکار جرگه سلطان محمود پس از بازگشت از جنگ
---
# شمارهٔ ۳۷ - در ذکر شکار جرگه سلطان محمود پس از بازگشت از جنگ

<div class="b" id="bn1"><div class="m1"><p>ای ز کار آمده و روی نهاده بشکار</p></div>
<div class="m2"><p>تیغ و تیر تو همی سیر نگردند ز کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه تیغ تو بر آرد ز سر دشمن گرد</p></div>
<div class="m2"><p>گاه تیر تو بر آرد ز بر شیر دمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیبت تیغ تو و تیر تو دارد شب و روز</p></div>
<div class="m2"><p>ملک بر خصم تبه بیشه بر شیر حصار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وای آن خصم که در رزم بدو گویی گیر</p></div>
<div class="m2"><p>وای آن شیر که در صید بدو گویی دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز صید تو بچشم تو چه روباه و چه شیر</p></div>
<div class="m2"><p>روز رزم تو بر تو چه پیاده چه سوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من درین صید گه آن دیدم از تو ملکا</p></div>
<div class="m2"><p>که صفت کردن آن گشت بمن بردشوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه در صحرا درنده و دام و دد بود</p></div>
<div class="m2"><p>همه را گرد بهم کردی در یک دیوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد ایشان پره ای بستی تا تند عقاب</p></div>
<div class="m2"><p>زان برون رفت ندانست هم از هیچ کنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز سر بالا چون ژاله روان کردی تیر</p></div>
<div class="m2"><p>هر که را گفتی بر دیده برم تیر بکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دویدند بسوی تو قطار از سر کوه</p></div>
<div class="m2"><p>باز گستردی در دامن کهشان بقطار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون درختان کشن بودند از دور و بتیر</p></div>
<div class="m2"><p>بفتادند بدانسان که فتد میوه ز دار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بامدادان همه کهسار پر از وحشی بود</p></div>
<div class="m2"><p>شامگاه از همه پرداخته بودی کهسار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در زمانی همه دشت ز خون دد ودام</p></div>
<div class="m2"><p>لعل کردی چو گلستانی هنگام بهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه کرانست مر آن را که تو کردی بقیاس</p></div>
<div class="m2"><p>نه کنارست مر آنرا که تو کردی بشمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ظن برم من که چنین بود همانا دشمن</p></div>
<div class="m2"><p>کشته و پیش تو افکنده سرو جانی خوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواهمی من که بجایستی بهرام امروز</p></div>
<div class="m2"><p>تا بدیدی و بیاموختی از شاه شکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاد باش ای ملک بار خدایان که گرفت</p></div>
<div class="m2"><p>دولت و همت و شادی و شهی بر تو قرار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو بکردار چنین قادر و ما در همه وقت</p></div>
<div class="m2"><p>پیش کردار تو درمانده بعجز از گفتار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نام تو نام همه شاهان بسترد و ببرد</p></div>
<div class="m2"><p>شاهنامه پس از این هیچ ندارد مقدار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مر ترا بار خدایا به لقب نیست نیاز</p></div>
<div class="m2"><p>نام تو برتر و بهتر ز لقب سیصد بار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کجا گویی محمود، بدانند که کیست</p></div>
<div class="m2"><p>از فراوانی کردار و بلندی آثار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ز محمود یقینم که لقب نتوان کرد</p></div>
<div class="m2"><p>وین سخن نزد همه خلق عیانست و جهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نام تو در خور تو، خوی تو اندر خور نام</p></div>
<div class="m2"><p>اینت نامی و خوی ساخته معنی دار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر جهانداری کو را بلقب باشد فخر</p></div>
<div class="m2"><p>هیچ شک نیست کز آن فخرترا باشد عار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرد باید که مسلمان بود و پاک بود</p></div>
<div class="m2"><p>چه بکار آید چندین سخنان بیکار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای بهر جای ترا سروری و پیشروی</p></div>
<div class="m2"><p>وی بهر کار ترا دسترس و دست گذار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شهریارانرا فخری چه ببزم و چه برزم</p></div>
<div class="m2"><p>پادشاهان را نازی چه بتاج و چه ببار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرخت باد برون آمدن از خانه به صید</p></div>
<div class="m2"><p>شاد بادی به دل از صید و ز تن برخوردار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شادمانه بتو آنکس که ترا دارد دوست</p></div>
<div class="m2"><p>شادمانه بتو آنکس که ترا باشد یار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سال و ماهش برخ از شادی رویت گل سرخ</p></div>
<div class="m2"><p>روز و شب بر رخش از رامش عشقت گلنار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عهد بسته دل او با تو به مهر و به وفا</p></div>
<div class="m2"><p>شرط کرده تن او با تو به بوس و به کنار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گاه در موکب شاهانه تو جوشن پوش</p></div>
<div class="m2"><p>گاه در مجلس فرخنده تو باده گسار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر که از شادی تو شاد نباشد به جهان</p></div>
<div class="m2"><p>یک زمان دور مباد از غم و از ناله زار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مجلس افروز بتو باغ تو امروز شها</p></div>
<div class="m2"><p>مجلس نو کن و نو گیر و می نوش گوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا بزرگان سپاه تو بهر باغ کنند</p></div>
<div class="m2"><p>پیش تو از قبل تهنیت باغ نثار</p></div></div>