---
title: >-
    شمارهٔ ۱۳۷ - در مدح سلطان محمد بن محمود غزنوی
---
# شمارهٔ ۱۳۷ - در مدح سلطان محمد بن محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>سوسن داری شکفته برمه روشن</p></div>
<div class="m2"><p>بر مه روشن شکفته داری سوسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهی گر ماه درقه دارد و شمشیر</p></div>
<div class="m2"><p>سروی گر سرو درع پوشد و جوشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوزن سیمین شده ست و سوزن زرین</p></div>
<div class="m2"><p>لاله رخانا! ترا میان و مرا تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زر ببها بیشتر ز سیم ولیکن</p></div>
<div class="m2"><p>زرین سوزن فدای سیمین سوزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حور بهشتی سرای منت بهشتست</p></div>
<div class="m2"><p>باز سپیدی کنار منت نشیمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف تو از مشک ناب چنبر چنبر</p></div>
<div class="m2"><p>روی تو از لاله برگ خرمن خرمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بتی و من هوای دل زتو خواهم</p></div>
<div class="m2"><p>از بت خواهد هوای خویش برهمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از لب تومرمرا هزار امیدست</p></div>
<div class="m2"><p>وز سر زلفین تو هزار زلیفن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آیی و گویی که: بوسه خواهی ؟ خواهم</p></div>
<div class="m2"><p>کور چه خواهد به جز دو دیده روشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوسه گر از بهر دل دهی نستانم</p></div>
<div class="m2"><p>دل بهوای ملک فروخته ام من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطب معالی ملک محمد محمود</p></div>
<div class="m2"><p>آن ز همه خسروان ستوده به هر فن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه فروتر ز جای همت او ماه</p></div>
<div class="m2"><p>آنکه سبکتر ز حلم او که قارن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه به راون دو هفته بود و ز عدلش</p></div>
<div class="m2"><p>صد اثر دلپذیر هست به راون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه چو او را پدر به بلخ همی خواند</p></div>
<div class="m2"><p>خطبه همی ساخت خاطبش به سجستن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای به میزد اندرون هزار فریدون</p></div>
<div class="m2"><p>ای به نبرد اندرون هزار تهمتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چه تو خواهی بکن که دایم دارد</p></div>
<div class="m2"><p>دولت با دامن تو دوخته دامن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روی به شهر مخالفان نه و بشتاب</p></div>
<div class="m2"><p>لشکر خویش اندرین جهان بپراکن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>و برضای پدر به غزو سوی روم</p></div>
<div class="m2"><p>در فکن اندر سرای قیصر شیون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کستی هر قل به تیغ هندی بگسل</p></div>
<div class="m2"><p>بر سر قیصر صلیبها همه بشکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم زره روم سوی چین رو و برگیر</p></div>
<div class="m2"><p>از چمن و باغ چین نهاله چندن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بادیه بر پشت زنده پیلان بگذار</p></div>
<div class="m2"><p>رایت بر کوه بوقبیس فرو زن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حج بکن و کام دل بخواه ز ایزد</p></div>
<div class="m2"><p>کانچه بخواهی تو بدهد ایزد ذوالمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شاد ببلخ ای وخسرو آیین بنشین</p></div>
<div class="m2"><p>همچو پدر گنجهای خویش بیاکن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خیمه دولت کن از موشح رومی</p></div>
<div class="m2"><p>پوشش پیلان کن از پرند ملون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از ادبا عالمی فرست به ماچین</p></div>
<div class="m2"><p>وز امرا شحنه ای فرست به ارمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنچه به کین خواهی از تو آید فردا</p></div>
<div class="m2"><p>نه ز قباد آمدای ملک نه ز بهمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هان که کنون روشنی گرفت چراغت</p></div>
<div class="m2"><p>چند برد دشمنت چراغ به روزن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دولت تو روغنست وملک چراغست</p></div>
<div class="m2"><p>زنده توان داشتن چراغ به روغن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنچه تو اکنون همی کنی به بزرگی</p></div>
<div class="m2"><p>بنگر تا هیچکس تواند کردن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گویند ار اشتری ز سوزن نگذشت</p></div>
<div class="m2"><p>گوبگذشت، اینک اشتر، اینک سوزن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو بقیاس آهنی و دشمن کوهست</p></div>
<div class="m2"><p>کوه فراوان فکنده اند به آهن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیست عجب گر ز بهر کم شدن نسل</p></div>
<div class="m2"><p>بار نگیرد بشهر دشمن تو زن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وانچه گرفته ست پیش ازین پسرانش</p></div>
<div class="m2"><p>عنین آیند و دخترانش سترون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دشمن گویم همی به شعر ولیکن</p></div>
<div class="m2"><p>من بجهان در ترا ندانم دشمن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در هنر تو من آنچه دعوی کردم</p></div>
<div class="m2"><p>حجت من سخت روشنست و مبرهن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا پدر تو ترا به شاهی بنشاند</p></div>
<div class="m2"><p>گیتی از فر تو شده ست چو گلشن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلخ شنیدم که بوستان بهشتست</p></div>
<div class="m2"><p>کز همه گیتی درو گرفتی مسکن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مسکن تو گر بهشت باشد نشگفت</p></div>
<div class="m2"><p>زانکه ملک را بهشت باد معدن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا ز بدخشان پدید آید لؤلؤ</p></div>
<div class="m2"><p>چون گهر از سنگ و کهرباز خماهن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا چو بر آید نبات و تیره شود ابر</p></div>
<div class="m2"><p>در مه اردیبهشت و در مه بهمن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هامون گردد چو چادر وشی سبز</p></div>
<div class="m2"><p>گردون گردد چو مطرف خز ادکن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاد زی و شاد باش تا همه شاهان</p></div>
<div class="m2"><p>نام بدیوان تو کنند مدون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کمتر حاجب ترا چو جم و چو کسری</p></div>
<div class="m2"><p>کهتر چاکر ترا چو گیو و چو بیژن</p></div></div>