---
title: >-
    شمارهٔ ۱۵۴ - در مدح سلطان مسعود و فتوحات اوو کشتن او شیر را
---
# شمارهٔ ۱۵۴ - در مدح سلطان مسعود و فتوحات اوو کشتن او شیر را

<div class="b" id="bn1"><div class="m1"><p>بدان خوشی وبدان نیکویی لب و دندان</p></div>
<div class="m2"><p>اگر بجان بتوانی خرید نیست گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب چنان را غازی به سیم و زر بفروخت</p></div>
<div class="m2"><p>عجب تر از دل غازی دلی بود به جهان ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطیفه ییست در آن لب چنانکه نتوان گفت</p></div>
<div class="m2"><p>اگر دلم دهدی خلق را نمایم آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گمان برم که همی بوسه ریخت خواهد ازو</p></div>
<div class="m2"><p>چودر سخن شود آن آفتاب ترکستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نه از قبل شرم آن نگارستی</p></div>
<div class="m2"><p>ز بوسه ندهمی او را بهیچ وقت امان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر هزار دلستی مرا چنانکه یکی</p></div>
<div class="m2"><p>همی فدا کنمی پیش آن لب و دندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار سال ملامت کشیدن از پی او</p></div>
<div class="m2"><p>توان و زان بت روزی جدا شدن نتوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا که خواهد گفتن که دوست را منواز</p></div>
<div class="m2"><p>که گفت خواهد معشوق را مخواه و مخوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عزیزتر ز همه خلق یار نیک بود</p></div>
<div class="m2"><p>ولی عزیزتر از یار خدمت سلطان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خدایگان مهان خسرو جهان مسعود</p></div>
<div class="m2"><p>که روزگارش مسعود باد وبخت جوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدایگانی کورا هزار بنده سزد</p></div>
<div class="m2"><p>چو کیقباد و چو کیخسرو و چو نوشروان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز ملک گیتی یک نیمه یافت او ز پدر</p></div>
<div class="m2"><p>دگر گرفته بشمشیر و تیر و گرز و کمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و گر بکنجی یکپاره ناگرفته بماند</p></div>
<div class="m2"><p>هم از شمار گرفته است، ناگرفته مدان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنامه راست شود، نامه کرد باید و بس</p></div>
<div class="m2"><p>بتیغ کار نگردد درست و با سروجان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شد آن زمان که به شمشیر کار باید کرد</p></div>
<div class="m2"><p>کنون بنامه همی کرد باید و بزبان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه سماع و شرابست و گاه لهو و طرب</p></div>
<div class="m2"><p>گه نهادن گنج و گه نهادن خوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر به صید و به چوگان زدن رود پس از این</p></div>
<div class="m2"><p>ز بهر گشتن صحرا و دیدن میدان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر نه در همه عالم کسی نماند که او</p></div>
<div class="m2"><p>گذشت خواهد ازین طاعت و ازین فرمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ملوک را همه بیمال کردو دل بشکست</p></div>
<div class="m2"><p>بر آنچه کرد سر خسروان به سر جاهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گزاف داری چندان هزار مرد دلیر</p></div>
<div class="m2"><p>که شوخ وار بجنگ شه آمدند چنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلاورانی پر حیله از سپاه عراق</p></div>
<div class="m2"><p>مبارزانی بگزیده از که گیلان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زپای تا سر در آهن زدوده چو تیغ</p></div>
<div class="m2"><p>گرفته تیغ بدست و دودست شسته زجان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز کوه آهن و کوه سپر گرفته پناه</p></div>
<div class="m2"><p>وزین دو کوه قوی چون ستاره خشت روان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ملک در آمد و با لشکری کم از دو هزار</p></div>
<div class="m2"><p>همه بداسپه و خالی ز خود و از خفتان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو روی کرد بدان کوه و آن سپاه بدید</p></div>
<div class="m2"><p>ندید کوه و سپه را ز هیچگونه کران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز پای تا سر که مرد کارزاری دید</p></div>
<div class="m2"><p>بکار زار ملک عهد بسته و پیمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدایگان جهان روی را بلشکر کرد</p></div>
<div class="m2"><p>بشرم گفت بلشکر که ای جوان مردان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پدر مرا و شمارا بدین زمین بگذاشت</p></div>
<div class="m2"><p>جدا فکند مرا با شما زخان و زمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه ساز داد که از بهر خویش سازم ملک</p></div>
<div class="m2"><p>نه خواسته که بجای شما کنم احسان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنام نیک ازینجا روان شدن بهتر</p></div>
<div class="m2"><p>که باز گشتن نزد پدر بدیگر سان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگر کز اینجا تا جای ما رهیست دراز</p></div>
<div class="m2"><p>ز راست و زچپ ما دشمنان و مابمیان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدین ره اندر چندانکه مرد سیر شود</p></div>
<div class="m2"><p>نه زاد یابد مرد هزیمتی و نه نان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان کنید که مردان شیر مرد کنند</p></div>
<div class="m2"><p>بهیچگونه متابید ازین نبرد عنان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر مراد برآید چنان کنم که شما</p></div>
<div class="m2"><p>بمال و ملک شوید از میان خلق نشان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زیان رسید شما را زبهر من بسیار</p></div>
<div class="m2"><p>چنان کنم که فرامش کنید نام زیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه سپاه نهادند رویها بزمین</p></div>
<div class="m2"><p>وز آنچه شاه جهان گفت چشمها گریان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بجمله گفتند ای شهریار روز افزون</p></div>
<div class="m2"><p>خدایگان بلند اختر بلند مکان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که در سپه که چو تو میر پیش جنگ بود</p></div>
<div class="m2"><p>اگر ز پیل بترسد بر او بود تاوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان کنیم کنون روی کوه را که شود</p></div>
<div class="m2"><p>زخون دشمن تو پر شقایق نعمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خدایگان جهان چون جوابها بشنود</p></div>
<div class="m2"><p>بخواست نیزه و توفیق خواست از یزدان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>میان آن سپه اندر فتاد چونکه فتد</p></div>
<div class="m2"><p>میان گور و میان گوزن شیر ژیان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همی گرفت بدست و همی فکند بپای</p></div>
<div class="m2"><p>جز این که کرد و چه دانست رستم دستان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیک زمان سپه بیکرانه را بشکست</p></div>
<div class="m2"><p>شکستگان را بگرفت و جمله دادامان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خبر شنید که شیری براه دید کسی</p></div>
<div class="m2"><p>ز جنگ روی بدان صید کرد هم بزمان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدان بزرگی جنگ و بدان بزرگی فتح</p></div>
<div class="m2"><p>بکرد وشیر بکشت اینت قدرت و امکان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ازین نکوتر و مردانه تر فراوان کرد</p></div>
<div class="m2"><p>بپای قلعه غور و بکوه غرجستان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خدای ناصر او باد و روزگار معین</p></div>
<div class="m2"><p>ملوک بنده و چاکر بآشکار و نهان</p></div></div>