---
title: >-
    شمارهٔ ۶۱ - در مدح امیر ابو یعقوب یوسف و تهنیت ولادت پسری از وی
---
# شمارهٔ ۶۱ - در مدح امیر ابو یعقوب یوسف و تهنیت ولادت پسری از وی

<div class="b" id="bn1"><div class="m1"><p>مرا بپرسید از رنج راه و شغل سفر</p></div>
<div class="m2"><p>بت من آن صنم ماهروی سیمین بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخست گفت که جانا ترا چه شد که چنین</p></div>
<div class="m2"><p>شکسته گونه ای و کار بر تو گشته غیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سرو سیمین بودی چو نال زرد شدی</p></div>
<div class="m2"><p>مگر ز رنج بنالیده ای براه اندر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر دل تو بجای دگر فریفته شد</p></div>
<div class="m2"><p>مگر ز عشق کسی پر خمار داری سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر ترا ز کس نکبتی رسید بروی</p></div>
<div class="m2"><p>مگر مخاطره ای کرده ای بجای خطر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر ز خوابگه شیر برگرفتی صید</p></div>
<div class="m2"><p>مگر ز بازوی سیمرغ باز کردی پر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر ز مار سیه داشتی بشب بالین</p></div>
<div class="m2"><p>مگر ز کژدم جراره داشتی بستر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر هوای دلی از تو بستدند بقهر</p></div>
<div class="m2"><p>مگر شرنگ غذا کرده ای بجای شکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب دادم کای ماه روی غالیه موی</p></div>
<div class="m2"><p>نه من زرنج کشیدن چنین شدم لاغر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا جدایی درگاه میر ابو یعقوب</p></div>
<div class="m2"><p>چنین نزار و سر افکنده کرد و خسته جگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سه ماه بودم دور از در سرای امیر</p></div>
<div class="m2"><p>مرا درین سه مه اندر نه خواب بود و نه خور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون که باز رسیدم بدین مظفر شاه</p></div>
<div class="m2"><p>کنون که چشم فکندم بدین مبارک در</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قوی شدم به امید و غنی شدم به نشاط</p></div>
<div class="m2"><p>دلم گرفت قرار و غمم رسید بسر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوقتی آمدم اینجا که در گهر بفزود</p></div>
<div class="m2"><p>یکی فریشته زین خسرو فریشته فر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی فریشته آمدبه خوشترین هنگام</p></div>
<div class="m2"><p>یکی فریشته آمد به بهترین اختر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به طالعی که امارت همی فزود شرف</p></div>
<div class="m2"><p>به ساعتی که سعادت همی نمود اثر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر همی به پسر تهنیت شود واجب</p></div>
<div class="m2"><p>بدین پسر که ملک یافته ست واجب تر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که این خجسته پسر، وین بزرگوار خلف</p></div>
<div class="m2"><p>زهر دوسوی بزرگ آمد و شریف گهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپه کشان پسرانرا ز بهر خدمت او</p></div>
<div class="m2"><p>همی دهند هم از کودکی کلاه و کمر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بنیکویی پدرش را امیدهاست درو</p></div>
<div class="m2"><p>وفا کناد خدای اندر و امید پدر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>امیر یوسف را اندر اینجهان شجریست</p></div>
<div class="m2"><p>که جز بشارت و جز تهنیت ندارد بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گمان برم که من اندر زمین همان شجرم</p></div>
<div class="m2"><p>شجر که دید نیایش بر و ستایش گر ؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شجر نباشم ،لیکن گمان برم که خدای</p></div>
<div class="m2"><p>ز بهر تهنیت میرم آفرید مگر؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که تا بخدمت او اندرم همی نرسم</p></div>
<div class="m2"><p>ز شغل تهنیت او بشغلهای دگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گهش بپیل کنم تهنیت گهش بغلام</p></div>
<div class="m2"><p>گهی بحاجب شایسته و گهی بپسر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه حال چنین باد و روزگار چنین</p></div>
<div class="m2"><p>امیر شاد و بدو شاد کهتر و مهتر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بشاد کامی در کاخ نو نشسته بعیش</p></div>
<div class="m2"><p>ز کاخ بر شده تا زهره ناله مزمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چگونه کاخی، کاخی چو گنبد هرمان</p></div>
<div class="m2"><p>ز پای تا سر، چون مصحفی نبشته بزر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چهار صفه و از هر یکی گشاده دری</p></div>
<div class="m2"><p>چنانکه چشم کند از چهار گوشه نظر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دری ازو سوی باغ و دری ازو سوی راغ</p></div>
<div class="m2"><p>دری از و سوی بحر و دری از و سوی بر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپید کرده بکافور سوده و بگلاب</p></div>
<div class="m2"><p>بکار برده در و یشم ترکی ومرمر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بجای شنگرف اندر نگار هاش عقیق</p></div>
<div class="m2"><p>بجای ساروج اندر مسامهاش درر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسقفش اندر عود سپید و چندن سرخ</p></div>
<div class="m2"><p>بخاکش اندر مشک سیاه و عنبرتر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو بخت میر بلند و چو عزم میر قوی</p></div>
<div class="m2"><p>چوخوی میر بدیع و چو لفظ او در خور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز برج او بتوان برد ز آسمان پروین</p></div>
<div class="m2"><p>ز بام او بتوان دید سد اسکندر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر چه سیر قمر بر صحیفه فلکست</p></div>
<div class="m2"><p>برابر سر دیوار اوست سیر قمر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز بس بلندی بالای او، نداند کرد</p></div>
<div class="m2"><p>شمار کنگره برج او ستاره شمر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فرود کاخ یکی بوستان چو باغ بهشت</p></div>
<div class="m2"><p>هزار گونه درو شکل و تندس دلبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز لاله های مخالف میانش چون فرخار</p></div>
<div class="m2"><p>ز سروهای مرادف کرانش چون کشمر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هزار دستان بر شاخ سرو او بخروش</p></div>
<div class="m2"><p>چو عاشقان فراق آزموده وقت سحر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو زلف خوبان در جویهاش مرز نگوش</p></div>
<div class="m2"><p>چو خط خوبان بر مرزهاش سیسنبر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپهر برده ازین کاخ و بوستان خجلت</p></div>
<div class="m2"><p>خدایگانا! زین کاخ و بوستان بر خور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خجسته ای ز همه خسروان بفضل و هنر</p></div>
<div class="m2"><p>بقدر و منزلت از هفت آسمان بگذر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بروز بزم حدیثی ز تو و صد بدره</p></div>
<div class="m2"><p>به روز رزم غلامی ز تو و صد لشکر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ستوده ای بکمال و ستوده ای بجمال</p></div>
<div class="m2"><p>ستوده ای به نوال و ستوده ای به سیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مقدمی به علوم و مقدمی به ادب</p></div>
<div class="m2"><p>مقدمی به سخا و مقدمی به هنر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بسا کسا که نه چون منظرست مخبر او</p></div>
<div class="m2"><p>تراست منظر زیبا موافق مخبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز مردی آنچه تو کردی همی به اندک سال</p></div>
<div class="m2"><p>بسال های فراوان نکرد رستم زر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر اوبصیدگه اندر غزال گور فکند</p></div>
<div class="m2"><p>تو شیر شرزه فکندی وکرگ شیر شکر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وگر که رستم پیلی بکشت در خردی</p></div>
<div class="m2"><p>هزار پیل دمان کشته ای تو در بربر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نکو دلی و نکو مذهب ونکو سیرت</p></div>
<div class="m2"><p>نکو خویی و نکو مخبر و نکو منظر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همیشه از پی کین خواستن ز دشمن دین</p></div>
<div class="m2"><p>قبای تو زره است و کلاه تو مغفر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همه کسی ز قضا و قدر بترسد وباز</p></div>
<div class="m2"><p>ز ناوک تو بترسد همی قضا و قدر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه ابربا کف دینار بار تو و چه گرد</p></div>
<div class="m2"><p>چه بحر بادل پهناور تو و چه شمر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کسیکه بسته بود نام چاکریت بدو</p></div>
<div class="m2"><p>زمانه بنده او باشد و فلک چاکر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بروز معرکه از تو حذر نداند کرد</p></div>
<div class="m2"><p>کسی که او ز قضای خدای کرد حذر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همیشه تا نبود نزد مردم بخرد</p></div>
<div class="m2"><p>گمان بجای یقین وعیان بجای خبر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>امیر باش و خداوند و پادشاه جهان</p></div>
<div class="m2"><p>زمانه پیش تو از هر بدی همیشه سپر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نهاده ملکان را بکام خود برگیر</p></div>
<div class="m2"><p>خنیده ملکان را به ایمنی بر خور</p></div></div>