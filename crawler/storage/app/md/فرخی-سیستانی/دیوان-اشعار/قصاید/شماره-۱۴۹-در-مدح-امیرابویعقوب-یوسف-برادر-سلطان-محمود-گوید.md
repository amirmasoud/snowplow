---
title: >-
    شمارهٔ ۱۴۹ - در مدح امیرابویعقوب یوسف برادر سلطان محمود گوید
---
# شمارهٔ ۱۴۹ - در مدح امیرابویعقوب یوسف برادر سلطان محمود گوید

<div class="b" id="bn1"><div class="m1"><p>همه گره گره است آن دو زلف چین بر چین</p></div>
<div class="m2"><p>گره به غالیه و چین به مشک ناب عجین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکسته زلف تو تازه بنفشه طبریست</p></div>
<div class="m2"><p>رخ و دو عارض تو تازه لاله و نسرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو لاله دیدی و شمشاد پوش و سنبل تاج</p></div>
<div class="m2"><p>بنفشه دیدی عنبر سرشت و مشک آگین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنفشه زلفا گرد بنفشه زار مگرد</p></div>
<div class="m2"><p>مگرد لاله رخا گرد لاله رنگین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا بسنده بود لاله تو، لاله مجوی</p></div>
<div class="m2"><p>بنفشه تو ترا بس بود، بنفشه مچین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا دهانک تنگ تو تنگدل دارد</p></div>
<div class="m2"><p>میان لاغر تو، لاغر و نزار و حزین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا چه خوانم ماه زمین وسرو سرای</p></div>
<div class="m2"><p>مرا تو بنده سرو سرای و ماه زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلند قد سروست و روی خوب تو ماه</p></div>
<div class="m2"><p>نه سرو باغ چنان ونه ماه چرخ چنین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که دید ماه برو کرده غالیه حلقه</p></div>
<div class="m2"><p>که دید سرو بر او بسته آفتاب آذین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا به عشق ملامت مکن که عشق مرا</p></div>
<div class="m2"><p>ز روی خوب تو گشت ای بهشت روی آیین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگربخواهی تا گردی ای صنوبر قد</p></div>
<div class="m2"><p>به عشق خویش گرفتار چون من مسکین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آفتاب رو و در نگر بسایه خویش</p></div>
<div class="m2"><p>در آینه نگر و روی خوب خویش ببین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بتیر نرگس تو با دل من آن کرده ست</p></div>
<div class="m2"><p>که تیر شاه جهان با مخالفان لعین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امیر و بارخدای ملوک ابو یعقوب</p></div>
<div class="m2"><p>معین دین هدی، یوسف بن ناصر دین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برادر ملکی کز نهیب او غمیند</p></div>
<div class="m2"><p>به روم قیصر روم و به چین سپهبد چین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مکین دولت و در مرتبت گرفته مکان</p></div>
<div class="m2"><p>ملک نژاده و اندر مکان ملک مکین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنو جواد ندیده ست روز بزم زمان</p></div>
<div class="m2"><p>چنو سوار ندیده ست روز رزم زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی که بر سر او بگذرد هزار قران</p></div>
<div class="m2"><p>نبیند آن ملک راد را همال و قرین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اجل میان سنان و خدنگ او گشته ست</p></div>
<div class="m2"><p>ازین رونده بدان و از آن دونده بدین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کشد مخالف را و کشد معادی را</p></div>
<div class="m2"><p>خدنگ او ز کمان و کمند او ز کمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهیب هیبت او صید زنده بستاند</p></div>
<div class="m2"><p>زیشک پیل دمان و ز چنگ شیر عرین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گنگ دیز بفرمان شاه بستاند</p></div>
<div class="m2"><p>هزار پیل دمان هر یکی چو حصن حصین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدست خویش قضا را بسوی خویش کشد</p></div>
<div class="m2"><p>هر آنکه جوید از آن شاه کینه جویان کین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کند به تیر پراکنده چون بنات النعش</p></div>
<div class="m2"><p>بهم شده سپهی را بگونه پروین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرو برد بگه حمله روستم کردار</p></div>
<div class="m2"><p>بزخم گرز گران گردن سوار به زین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به نوک تیر فرو افکند ز کرگ سرون</p></div>
<div class="m2"><p>به ضرب تیغ فرود آورد ز پیل سرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز فخر نامش نقش نگین پذیرد آب</p></div>
<div class="m2"><p>گر آزمایش را برنهد بر آب نگین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر آرزوی کف راد او ز کان گهر</p></div>
<div class="m2"><p>گهر بر آید بی کوه کاف و بی میتین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خجسته بخت بر او آفرین کند شب و روز</p></div>
<div class="m2"><p>کند فریشته بر آفرین او آمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کدام کس که نه او را بطبع گشت رهی</p></div>
<div class="m2"><p>کدام دل که نه اورا بمهر گشت رهین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ایا سپهر ادب را دل تو چشمه روز</p></div>
<div class="m2"><p>ایا بهشت سخا را کف تو ماء معین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به روی سایل از آنگونه شادمانه شوی</p></div>
<div class="m2"><p>که روز حشر بهشتی به روی حور العین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان خوش آید بر گوش تو سؤال کجا</p></div>
<div class="m2"><p>بگوش مردم دل مرده بانگ رود حزین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ترا به روز عطا دادن و بروز وغا</p></div>
<div class="m2"><p>سخا کند تعلیم و هنر کند تلقین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در سرای ترا خسروان نماز برند</p></div>
<div class="m2"><p>چنانکه دهقان در پیش آذر برزین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فکندگان سنان ترا بروز نبرد</p></div>
<div class="m2"><p>ز کشتگان بود ای شاه بستر و بالین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عزیز گشت هر آنکس که شد بر تو عزیز</p></div>
<div class="m2"><p>گزیده گشت هر آنکس که شد بر تو گزین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همیشه تا که بهاران و روزگار بهار</p></div>
<div class="m2"><p>فرو نهد ز بر کوه سر بهامون هین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه تا نقطی برزنند بر سر زی</p></div>
<div class="m2"><p>همیشه تا سه نقط بر نهند بر سر شین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فلک مطیع تو بادا و بخت نیک سکال</p></div>
<div class="m2"><p>خدای ناصرتو باد و روزگار معین</p></div></div>