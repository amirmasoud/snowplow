---
title: >-
    شمارهٔ ۲۱۲ - در مدح خواجه عمید حامد بن محمدالمهتدی گوید
---
# شمارهٔ ۲۱۲ - در مدح خواجه عمید حامد بن محمدالمهتدی گوید

<div class="b" id="bn1"><div class="m1"><p>تادل من ز دست من بستدی</p></div>
<div class="m2"><p>سر بسر ای نگار دیگر شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاره و راه خویش گم کرده ام</p></div>
<div class="m2"><p>تا تو مرا به راه پیش آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من زهمه جهان دلی داشتم</p></div>
<div class="m2"><p>آمدی وز دست من بستدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به تو دادم و دلت نستدم</p></div>
<div class="m2"><p>مردم دیدی تو بدین بی بدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی بیدلی و با من دو دل</p></div>
<div class="m2"><p>لاجرم ای صنم به کام خودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان ودل من آن خواجه ست و تو</p></div>
<div class="m2"><p>چنگ به چیز خواجه اندر زدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم فضل و علم خواجه عمید</p></div>
<div class="m2"><p>حامد بن محمد المهتدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن که همه درفشد از روی او</p></div>
<div class="m2"><p>رادی و فضل و فره ایزدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای همه حری و همه مردمی</p></div>
<div class="m2"><p>وی همه رادی و همه بخردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رادی را تو اول و آخری</p></div>
<div class="m2"><p>حری را تو ضظغ و ابجدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باخبر از فنون فضل وادب</p></div>
<div class="m2"><p>هست به پیش تو کم از مبتدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقت کفایت ار چه کافی کسیست</p></div>
<div class="m2"><p>گوید کاستاد چومن صد شد ی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>موبد اگر امام دانش بود</p></div>
<div class="m2"><p>توبه همه طریقها موبدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایل اگر چه جان بخواهد زتو</p></div>
<div class="m2"><p>بدهی و همچنین بدی تا بدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باشد اگر صد هنری مرد، تو</p></div>
<div class="m2"><p>پیشتر و بیشتر از هر صدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو زهمه جهان به پیشی و نام</p></div>
<div class="m2"><p>همچو ز جمع روزهاشنبدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا شبهی نیاید از آبنوس</p></div>
<div class="m2"><p>همچو ز دار پرنیان تربدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گنبد بر شده فرود تو باد</p></div>
<div class="m2"><p>همچو بهشت از زبر گنبدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عید مبارکست می خواه از آن</p></div>
<div class="m2"><p>کز رخ اوبه لب همی گل چدی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گشته ز رنگ سبزه و ارغوان</p></div>
<div class="m2"><p>باغ و چمن زمردی و بسدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشم مخالف را بیاژن به تیر</p></div>
<div class="m2"><p>چون کف یاران که به زر آژدی</p></div></div>