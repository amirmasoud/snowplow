---
title: >-
    شمارهٔ ۱۴ - در مدح میر ابوالفتح فرزند سید الوزراء احمد بن حسن میمندی
---
# شمارهٔ ۱۴ - در مدح میر ابوالفتح فرزند سید الوزراء احمد بن حسن میمندی

<div class="b" id="bn1"><div class="m1"><p>من ندانم که عاشقی چه بلاست</p></div>
<div class="m2"><p>هر بلایی که هست عاشق راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زرد و خمیده گشتم از غم عشق</p></div>
<div class="m2"><p>دو رخ لعل فام و قامت راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاشکی دل نبودیم که مرا</p></div>
<div class="m2"><p>اینهمه درد وسختی از دل خاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بود جای عشق و چون دل شد</p></div>
<div class="m2"><p>عشق را نیز جایگاه کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من چون رعیتیست مطیع</p></div>
<div class="m2"><p>عشق چون پادشاه کامرواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برد و برد هر چه بیند و دید</p></div>
<div class="m2"><p>کند و کرد هرچه خواهد و خواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وای آن کو بدام عشق آویخت</p></div>
<div class="m2"><p>خنک آن کو زدام عشق رهاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق بر من در عنا بگشاد</p></div>
<div class="m2"><p>عشق سر تابسر عذاب و عناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان سخت تر ز آتش عشق</p></div>
<div class="m2"><p>خشم فرزند سیدالوزراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میر ابوالفتح کز فتوت و فضل</p></div>
<div class="m2"><p>در جهان بی شبیه و بی همتاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفتش مهتر گشاده کفست</p></div>
<div class="m2"><p>لقبش خواجه بزرگ عطاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسخا نامورتر از دریاست</p></div>
<div class="m2"><p>گر چه او را کمینه فضل سخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست او هست ابر و دریا دل</p></div>
<div class="m2"><p>ابر شاگرد و نایبش دریاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخشش او طبیعی و گهریست</p></div>
<div class="m2"><p>بخشش دیگران بروی و ریاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>راد مرد و کریم و بی خللست</p></div>
<div class="m2"><p>راد ویکخوی و یکدل یکتاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیکویی را ثواب هفتاد است</p></div>
<div class="m2"><p>از خدا و برین رسول گواست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندکست این ز فضل او هر چند</p></div>
<div class="m2"><p>کس نگفته ست کاندکیش چراست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن خواجه غریب تر که ازو</p></div>
<div class="m2"><p>خدمتی را هزار گونه جزاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اثر نعمت و عنایت او</p></div>
<div class="m2"><p>بر همه کس چو بنگری پیداست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ادبا را شریک دولت کرد</p></div>
<div class="m2"><p>دولت خواجه دولت ادباست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شعرا را رفیق نعمت کرد</p></div>
<div class="m2"><p>نعمت خواجه نعمت شعراست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر تنی زیر بار منت اوست</p></div>
<div class="m2"><p>هر زبانی بشکر او گویاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او ز جود و ز فضل تنها نیست</p></div>
<div class="m2"><p>در همانند خویشتن تنهاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طبع او چون هواست روشن و پاک</p></div>
<div class="m2"><p>روشن و پاک بی بهانه هواست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر که با او بدشمنی کوشد</p></div>
<div class="m2"><p>روز او از قیاس بی فرداست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تیغ او بر سر مخالف او</p></div>
<div class="m2"><p>از خدای جهان نبشته قضاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دشمن او ازو بجان نرهد</p></div>
<div class="m2"><p>ور همه پروریده عنقاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر چه آباش سیدان بودند</p></div>
<div class="m2"><p>او بهر فضل سید آباست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دست او را مکن قیاس به ابر</p></div>
<div class="m2"><p>که روانیست این قیاس و خطاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر چه گیتی ز ابر تازه شود</p></div>
<div class="m2"><p>اندرو بیم صاعقه ست و بلاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا هوا را گشادگی و خوشیست</p></div>
<div class="m2"><p>تا زمین را فراخی و پهناست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شادمان باد و یافته ز خدای</p></div>
<div class="m2"><p>هر چه او را مرداد و کام و هواست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مهرگانش خجسته باد چنان</p></div>
<div class="m2"><p>کو خجسته پی و خجسته لقاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کاندرین مهرگان فرخ پی</p></div>
<div class="m2"><p>زو مرا نیم موزه نیم قباست</p></div></div>