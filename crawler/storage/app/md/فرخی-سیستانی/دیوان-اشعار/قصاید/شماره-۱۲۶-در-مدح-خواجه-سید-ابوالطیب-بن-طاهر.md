---
title: >-
    شمارهٔ ۱۲۶ - در مدح خواجه سید ابوالطیب بن طاهر
---
# شمارهٔ ۱۲۶ - در مدح خواجه سید ابوالطیب بن طاهر

<div class="b" id="bn1"><div class="m1"><p>بار بر بست مه روزه وبر کند خیم</p></div>
<div class="m2"><p>مهرگان طبل زد و عید برون برد علم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز چون بلبل بی جفت ببانگ آمد زیر</p></div>
<div class="m2"><p>باز چون عاشق بیدل به خروش آمد بم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده گیران زبان بسته گشادند زبان</p></div>
<div class="m2"><p>باده خوران پراکنده نشستند بهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعل کردند بیک سیکی لبهای کبود</p></div>
<div class="m2"><p>شاد کردند بیک مجلس دلهای دژم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیز بت رویا !تا مابه سر کار شویم</p></div>
<div class="m2"><p>که نه ایشان را سور آمدو مارا ماتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان می لعل قدح پر کن و نزدیک من آر</p></div>
<div class="m2"><p>بر تن و جان نتوان کردازین بیش ستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزه پیریست که از هیبت واز حشمت او</p></div>
<div class="m2"><p>نتوان زد به مراد دل، یک ساعت دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شدآن پیر جوانی بگرفتندجهان</p></div>
<div class="m2"><p>ما و ایشان و می لعل، نه اندوه ونه غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باش تا خواجه درین باب چه گوید، چه کند</p></div>
<div class="m2"><p>آب چون زنگ خورد یا می چون آب بقم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواجه سید ابوالطیب طاهر که بدوست</p></div>
<div class="m2"><p>دل سلطان و دل خواجه و دلهای حشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه به فضل او را جفتی ز بزرگان عرب</p></div>
<div class="m2"><p>نه به علم او را یاری زبزرگان عجم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جوانمردی جاییست که آنجا نرسید</p></div>
<div class="m2"><p>هیچ بخشنده و زین پس نرسد هرگز هم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عالمی بینم بر درگه اوخواسته خواه</p></div>
<div class="m2"><p>واو همی گوید هر کس را کآری و نعم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که را بینی با بخشش و با خلعت اوست</p></div>
<div class="m2"><p>همتی دارد در کار سخا بلکه همم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیشماری همه چون ریگ همی بخشد مال</p></div>
<div class="m2"><p>راست پنداری داردبه یمین اندر یم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخرد جامه بسیار به تخت و چو خرید</p></div>
<div class="m2"><p>نام زوار زند زود بر آن تخت رقم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که را بینی دینار و درم دارد دوست</p></div>
<div class="m2"><p>نه بر اینگونه ست آن مهتر آزاده شیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>او چودانست که دینار نه چون نام نکوست</p></div>
<div class="m2"><p>مهر برداشت بیکبار ز دینار و درم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از عطا دادن پیوسته آن بار خدای</p></div>
<div class="m2"><p>خانه زایر او باز ندانی ز حرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با چنین بخشش پیوسته که او پیش گرفت</p></div>
<div class="m2"><p>رود جیحون را شک نیست که آب آید کم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ایزد آن بار خدای بسخا را بدهاد</p></div>
<div class="m2"><p>گنج قارون و بزرگی و توانایی جم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دست بخشنده او از دل پیران ببرد</p></div>
<div class="m2"><p>غم برنایی و بیچارگی و ضعف هرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من به هر چیر که خواهی تو سوگند خورم</p></div>
<div class="m2"><p>که نه چون او بوجود آید هرگز ز عدم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لاجرم خلق جهان بر خوی او شیفته اند</p></div>
<div class="m2"><p>چون گل سوری بر باد سحر گاهی ونم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه بجان و سر او محتشمانرا چه بتن</p></div>
<div class="m2"><p>چه حریم در او محترمان را چه حرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه بیهوده مر اورا ملک روی زمین</p></div>
<div class="m2"><p>مملکت زیر نگین کرد و جهان زیر قلم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رای و اندیشه بدو کرد و بدو داشت نگاه</p></div>
<div class="m2"><p>زانکه دانست که راییست مراورا محکم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شادمان باد همه ساله و با ناز و نعیم</p></div>
<div class="m2"><p>دشمن و حاسد او مانده به تیمار و ندم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عید اوفرخ و از آمدن عید شریف</p></div>
<div class="m2"><p>در دل او طرب و در دل بدخواه الم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چشم او سوی نگاری که برو عید بود</p></div>
<div class="m2"><p>جعد و زلفش را چون غالیه وز غالیه شم</p></div></div>