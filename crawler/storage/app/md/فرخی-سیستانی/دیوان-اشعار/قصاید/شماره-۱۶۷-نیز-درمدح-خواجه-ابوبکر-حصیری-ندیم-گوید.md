---
title: >-
    شمارهٔ ۱۶۷ - نیز درمدح خواجه ابوبکر حصیری ندیم گوید
---
# شمارهٔ ۱۶۷ - نیز درمدح خواجه ابوبکر حصیری ندیم گوید

<div class="b" id="bn1"><div class="m1"><p>من پار دلی داشتم بسامان</p></div>
<div class="m2"><p>امسال دگرگون شد و دگرسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرمان دگر کس همی برد دل</p></div>
<div class="m2"><p>این را چه حیل باشد و چه درمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باری دلکی یابمی نهانی</p></div>
<div class="m2"><p>نرخش چه گران باشدو چه ارزان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بس کنمی زین دل مخالف</p></div>
<div class="m2"><p>وین غم کنمی برد گر دل آسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوروز جهان چون بهشت کرده ست</p></div>
<div class="m2"><p>پر لاله و پر گل که و بیابان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون چادر مصقول گشته صحرا</p></div>
<div class="m2"><p>چون حله منقوش گشته بستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در باغ به نوبت همی سراید</p></div>
<div class="m2"><p>تا روز همه شب هزار دستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشغول شده هر کسی به شادی</p></div>
<div class="m2"><p>من در غم دل دست شسته از جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دلبر من باش یک زمانک</p></div>
<div class="m2"><p>تا مدحت خواجه برم به پایان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورشید همه خواجگان دولت</p></div>
<div class="m2"><p>بوبکر حصیری ندیم سلطان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن بار خدایی که در بزرگی</p></div>
<div class="m2"><p>جاییست که آنجا رسید نتوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همزانوی شاه جهان نشسته</p></div>
<div class="m2"><p>در مجلس و بارگاه و بر خوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در زیر مرادش همه ولایت</p></div>
<div class="m2"><p>در زیر ننگینش همه خراسان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سلطان که به فرمان اوست گیتی</p></div>
<div class="m2"><p>او را چون پسر مشفق و بفرمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر پند کزو بشنود به مجلس</p></div>
<div class="m2"><p>بنیوشد و مویی بنگذرد زان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>داند که مصالح نگاه دارد</p></div>
<div class="m2"><p>وان پند بود ملک را نگهبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زو دوست تر اندر جهان ملک را</p></div>
<div class="m2"><p>بنمای وگر نه سخن بدو مان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین لشکر چندین به عهد خسرو</p></div>
<div class="m2"><p>زو پیش که آورده بود ایمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او را سزد امروز فخر کردن</p></div>
<div class="m2"><p>کو بود نگهدار عهد و پیمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پاداش همی یابد از شهنشاه</p></div>
<div class="m2"><p>بر دوستی و خدمت فراوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هستند ز نیم روز تا شب</p></div>
<div class="m2"><p>درخدمت او مهتران ایران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و او نیز به خدمت همی شتابد</p></div>
<div class="m2"><p>مکروه جهان دور بادش از جان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای بار خدای بلند همت</p></div>
<div class="m2"><p>معروف به رادی و فضل و احسان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خواهنده همیشه ترا دعا گوی</p></div>
<div class="m2"><p>گوینده همه ساله آفرین خوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این عز ترا خواسته زایزد</p></div>
<div class="m2"><p>وان عمر ترا خواسته ز یزدان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جاوید زیادی به شاد کامی</p></div>
<div class="m2"><p>شادیت بر افزون و غم به نقصان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نوروز تو فرخنده و خجسته</p></div>
<div class="m2"><p>کار تو چو کردار تو بدو جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کردار تو نیکوتر از تعبد</p></div>
<div class="m2"><p>زیرا که نکو دینی و مسلمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مخدوم زیادی و تو مبادی</p></div>
<div class="m2"><p>از خدمت شاه جهان پشیمان</p></div></div>