---
title: >-
    شمارهٔ ۲ - مدح خواجه عمید ابومنصور سیداسعد گوید
---
# شمارهٔ ۲ - مدح خواجه عمید ابومنصور سیداسعد گوید

<div class="b" id="bn1"><div class="m1"><p>نیگلون پرده برکشید هوا</p></div>
<div class="m2"><p>باغ بنوشت مفرش دیبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبدان گشت نیلگون رخسار</p></div>
<div class="m2"><p>و آسمان گشت سیمگون سیما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بلور شکسته، بسته شود</p></div>
<div class="m2"><p>گر براندازی آب را بهوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لوح یاقوت زرد گشت بباغ</p></div>
<div class="m2"><p>بر درختان صحیفه مینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بینوا گشت باغ مینا رنگ</p></div>
<div class="m2"><p>تا درو زاغ برگرفت نوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب بینوا نوا نزند</p></div>
<div class="m2"><p>اندر آن مجلسی که نیست نوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نه عاشق شدست برگ درخت</p></div>
<div class="m2"><p>از چه رخ زرد گشت و پشت دوتا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد را کیمیای سوده که داد</p></div>
<div class="m2"><p>که ازو زر ساو گشت گیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر گیا زرد گشت باک مدار</p></div>
<div class="m2"><p>بس بود سرخ روی خواجه ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواجه سید اسعد آنکه ازوست</p></div>
<div class="m2"><p>هرچه سعدست زیر هفت سما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه با رای او یکیست قدر</p></div>
<div class="m2"><p>آنکه با امر او یکیست قضا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیر تدبیر محکمش آفاق</p></div>
<div class="m2"><p>زیر اعلام همتش دنیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا بدریا رسید باد سخاش</p></div>
<div class="m2"><p>در شکستست زایش دریا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کل جودست دست او دایم</p></div>
<div class="m2"><p>وان دگر جودها همه اجزا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که امروز کرد خدمت او</p></div>
<div class="m2"><p>خدمت او ملک کند فردا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که خالی شد از عنایت او</p></div>
<div class="m2"><p>عالم او را دهد عنان عنا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز ایرانرا سرای او حرمست</p></div>
<div class="m2"><p>مسند او منا و صدر صفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که تنها شود ز خدمت او</p></div>
<div class="m2"><p>از همه چیزها شود تنها</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آفرین خدای باد بر او</p></div>
<div class="m2"><p>کافرین را بلند کرد بنا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با بها گشت صدر و بالش ازو</p></div>
<div class="m2"><p>که ثنا زو گرفت فر و بها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>او کند فرق نیک را از بد</p></div>
<div class="m2"><p>او شناسد صواب را ز خطا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاطر من مگر بمدحت او</p></div>
<div class="m2"><p>ندهد بر مدیح خلق رضا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرچه دورم بتن ز خدمت او</p></div>
<div class="m2"><p>نکنم بی بهانه رسم رها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر زمان مدحتی فرستم نو</p></div>
<div class="m2"><p>ای رساننده زود باش هلا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای سزاوارتر بمدح و ثناست</p></div>
<div class="m2"><p>جهد کن تا رسد سزا بسزا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای ستوده خوی ستوده سخن</p></div>
<div class="m2"><p>ای بلند اختر بلند عطا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر بخدمت نیامدم بر تو</p></div>
<div class="m2"><p>عذر کی تازه رخ نمود مرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا ز درگاه تو جدا گشتم</p></div>
<div class="m2"><p>هر زمانی مرا غمیست جدا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرقت پرده تو گشت مرا</p></div>
<div class="m2"><p>پرده ای بر دو دیده بینا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من به مدح و دعا ز دستم چنگ</p></div>
<div class="m2"><p>گر بسنده کنی بمدح و دعا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا نمازست مایه مؤمن</p></div>
<div class="m2"><p>تا صلیبست قبله ترسا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شادمان باش و بختیار و عزیز</p></div>
<div class="m2"><p>جاودان، کامران و کامروا</p></div></div>