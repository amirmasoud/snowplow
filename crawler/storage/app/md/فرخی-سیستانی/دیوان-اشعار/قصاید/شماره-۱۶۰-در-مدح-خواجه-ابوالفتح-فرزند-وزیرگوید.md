---
title: >-
    شمارهٔ ۱۶۰ - در مدح خواجه ابوالفتح فرزند وزیرگوید
---
# شمارهٔ ۱۶۰ - در مدح خواجه ابوالفتح فرزند وزیرگوید

<div class="b" id="bn1"><div class="m1"><p>سیه زلف آن سرو سیمین من</p></div>
<div class="m2"><p>همه تاب و پیچست و بند و شکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگار مرا سرو آزاد خوان</p></div>
<div class="m2"><p>کنار من آن سرو بن را چمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلندی و سبزی بود سرو را</p></div>
<div class="m2"><p>بلندست و سبزست معشوق من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و تن فدا کردم آن ماه را</p></div>
<div class="m2"><p>نه دل ماند با من کنون و نه تن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تن کردم آن بی میان را میان</p></div>
<div class="m2"><p>ز دل کردم آن بی دهن را دهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا جز پرستیدنش کارنیست</p></div>
<div class="m2"><p>بلی بت پرستیست کار شمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنازم از و همچو فضل و ادب</p></div>
<div class="m2"><p>به فززند دستور شاه زمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابوالفتح کازادگان جهان</p></div>
<div class="m2"><p>شد ستند بر جود او مفتنن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رهایی بدو یابد اندر جهان</p></div>
<div class="m2"><p>ز دست محن مردم ممتحن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان کو بجوید هوای ولی</p></div>
<div class="m2"><p>برهمن نجوید هوای وثن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آن کس که بر کین او دست سود</p></div>
<div class="m2"><p>به دستش دهد دست محنت رسن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسوزد ز دور آتش خشم او</p></div>
<div class="m2"><p>بر اندام اعدای او پیرهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایا خوانده صلح تو و جنگ تو</p></div>
<div class="m2"><p>کتاب امان و کتاب فتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر بر یمن خشم تو بگذرد</p></div>
<div class="m2"><p>نتابد سهیل یمن از یمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر بر عدن خلق تو بگذرد</p></div>
<div class="m2"><p>ازو جنت عدن گردد عدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی کز رضای تو بیرون شود</p></div>
<div class="m2"><p>زمانه بدوزد مر او را کفن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر کرگدن پیشت آید به جنگ</p></div>
<div class="m2"><p>بپردازی او را ز شغل بدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سواری بلند اسب را ره کند</p></div>
<div class="m2"><p>سنان تو در الیه کرگدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندانم که با دست یا آتشست</p></div>
<div class="m2"><p>به زیر تو آن باره پیلتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ازو رفتن نرم و از گور تک</p></div>
<div class="m2"><p>ز پرنده پرواز و زو تاختن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر از ژرف دریا بخواهی گذشت</p></div>
<div class="m2"><p>از او بگذرد زین بر او برفکن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ایا دیده فضل و دست هنر</p></div>
<div class="m2"><p>ایا بازوی دین و پشت سنن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به حری ز تو گستریده ست نام</p></div>
<div class="m2"><p>به هر جایگاه و به هر انجمن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز عدل و ز انصاف تو در جهان</p></div>
<div class="m2"><p>نیندیشد از شیر شرزه شدن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر آن کز تو ای خواجه دور اوفتاد</p></div>
<div class="m2"><p>براو کارگر گشت تیغ محن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رهی تا ز درگاه تو دور شد</p></div>
<div class="m2"><p>بمانده ست از دولت خویشتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی تا سپیده دم اندر بهار</p></div>
<div class="m2"><p>نوا برکشد زند خوان از فنن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به شادی بناز و به دولت برآر</p></div>
<div class="m2"><p>سر برج دولت به برج پرن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به فضل تو گویندگان متفق</p></div>
<div class="m2"><p>به شکر تو آزادگان مرتهن</p></div></div>