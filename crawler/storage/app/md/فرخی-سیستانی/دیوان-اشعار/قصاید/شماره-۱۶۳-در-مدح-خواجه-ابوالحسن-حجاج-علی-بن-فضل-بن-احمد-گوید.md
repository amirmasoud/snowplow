---
title: >-
    شمارهٔ ۱۶۳ - در مدح خواجه ابوالحسن حجاج علی بن فضل بن احمد گوید
---
# شمارهٔ ۱۶۳ - در مدح خواجه ابوالحسن حجاج علی بن فضل بن احمد گوید

<div class="b" id="bn1"><div class="m1"><p>پیچان درختی نام او نارون</p></div>
<div class="m2"><p>چون سرو زرین پر عقیق یمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازنده چون بالای آن زاد سرو</p></div>
<div class="m2"><p>تابنده چون رخسار آن سیمتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاخش ملون همچو قوس قزح</p></div>
<div class="m2"><p>برگش درخشان همچو نجم پرن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زلف خوبان بیخ او پر گره</p></div>
<div class="m2"><p>چون جعد خوبان شاخ او پر شکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون آفتاب و جزوی از آفتاب</p></div>
<div class="m2"><p>چون گوهر و با گوهر از یک وطن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دلبری اندر عقیقین و شاخ</p></div>
<div class="m2"><p>چون لعبتی در بسدین پیرهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالنده همچون من ز هجران یار</p></div>
<div class="m2"><p>لرزنده و پیچنده بر خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی گنهکاریست کو راهمی</p></div>
<div class="m2"><p>در پیش خواجه گفت باید سخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستور زاده شاه ایران زمین</p></div>
<div class="m2"><p>حجاج تاج خواجگان بوالحسن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرورده اندر دامن مملکت</p></div>
<div class="m2"><p>پستان دولت روز و شب در دهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آزادگی آموخته زو طریق</p></div>
<div class="m2"><p>رادی گرفته زو رسوم و سنن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او برگرفته راه و رسم پدر</p></div>
<div class="m2"><p>چون جستن او طاعت ذوالمنن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و آزادگان را بر کشیده ز چاه</p></div>
<div class="m2"><p>چاهی که پایانش نیابد رسن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس مبتلا کو را رهاند ازبلا</p></div>
<div class="m2"><p>بس ممتحن کو را رهاند از محن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایزد کند رحمت برآن کس که او</p></div>
<div class="m2"><p>رحمت کندبر مردم ممتحن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اندر کفایت صاحب دیگرست</p></div>
<div class="m2"><p>واندر سیاست سیف بن ذوالیزن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او ایدر ست ورای وتدبیر او</p></div>
<div class="m2"><p>گردان میان قیروان تا ختن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرمان او وامراو طوقهاست</p></div>
<div class="m2"><p>بر گردن میران لشکر شکن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر کلک بر کاغذ نهد از نهیب</p></div>
<div class="m2"><p>شمشیر کاغذ گردد و مرد زن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر ساعتی زنهار خواهد همی</p></div>
<div class="m2"><p>از کلک او شمشیر شمشیر زن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از عدل او آرام یابد همی</p></div>
<div class="m2"><p>با شیر شرزه اشتر اندر عطن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چندان بیان دارد به فضل از مهان</p></div>
<div class="m2"><p>کاندر محاسن حور عین ز اهرمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اوآتش تیزست بر تیغ کوه</p></div>
<div class="m2"><p>وان دیگران چون شمع بر بادخن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چونانکه دستش را پرستد سخا</p></div>
<div class="m2"><p>بت را پرستیدن نیارد شمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با بردباری طبع او متفق</p></div>
<div class="m2"><p>با نیکنامی جود او مقترن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سختم شگفت آید که تا چون شده ست</p></div>
<div class="m2"><p>چندان فضایل جمع در یک بدن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرمایه فضلست بس کار نیست</p></div>
<div class="m2"><p>فرزند فضلست آن چراغ زمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نزد خردمندان نباشد غریب</p></div>
<div class="m2"><p>بوی از گل و نور از سهیل یمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زایر کز آنجا باز گردد برد</p></div>
<div class="m2"><p>دیبا به تخت و رزمه و زر به من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بس کس که او چون قصد وی کرد باز</p></div>
<div class="m2"><p>بانهمت و با کام دل شد چو من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر ظن نیکو قصد کردم بدو</p></div>
<div class="m2"><p>آزادگی کرد و وفا کرد ظن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روز نخستم خلعتی داد زرد</p></div>
<div class="m2"><p>از جامه ای کآن را ندانم ثمن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با جامه زری زرد چون شنبلید</p></div>
<div class="m2"><p>با زر سیمی پاک چون نسترن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زان زر و سیمم روز و شب پیش خویش</p></div>
<div class="m2"><p>بر پای کرده کودکی چون وثن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مهتر چنین باید موال نواز</p></div>
<div class="m2"><p>مهتر چنین باید معادی شکن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای آفتاب صد هزار آفتاب</p></div>
<div class="m2"><p>ای پیشکار صد هزار انجمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جشن سده ست از بهر جشن سده</p></div>
<div class="m2"><p>شادی کن و اندیشه از دل بکن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می خور ز دست لعبتی حور زاد</p></div>
<div class="m2"><p>چون زاد سروی پر گل ویاسمن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ماهی به کش در کش چو سیمین ستون</p></div>
<div class="m2"><p>جامی به کف برنه چو زرین لگن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا می پرستی پیشه موبد ست</p></div>
<div class="m2"><p>تا بت پرستی پیشه برهمن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قسم تو باد از این جهان خرمی</p></div>
<div class="m2"><p>قسم بداندیش تو گرم و حزن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از تیرهای حادئات جهان</p></div>
<div class="m2"><p>دولت گرفته پیش رویت مجن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باغ امیدت پر گل و لاله باد</p></div>
<div class="m2"><p>چون باغ فضلت پر گل و نسترن</p></div></div>