---
title: >-
    شمارهٔ ۱۶۸ - در مدح خواجه عمید المک ابوبکرقهستانی عارض لشکر
---
# شمارهٔ ۱۶۸ - در مدح خواجه عمید المک ابوبکرقهستانی عارض لشکر

<div class="b" id="bn1"><div class="m1"><p>بوستانیست روی کودک من</p></div>
<div class="m2"><p>واندر آن بوستان شکفته سمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سمن سال و مه در آن بستان</p></div>
<div class="m2"><p>لاله یابی و نرگس و سوسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبانی بباید آن بت را</p></div>
<div class="m2"><p>بایکی پاسدار چو بکزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مرا پاسدار خویش کند</p></div>
<div class="m2"><p>خدمت او کنم به جان و به تن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد بر گرد باغ او گردم</p></div>
<div class="m2"><p>بر در باغ او کنم مسکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که زان گل گلی بخواهد کند</p></div>
<div class="m2"><p>گویم آن گل گل تو نیست، مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور بدین یک سخن مرا بزند</p></div>
<div class="m2"><p>گوش او کر کنم به نعره زدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاکر خواجه را که یارد زد</p></div>
<div class="m2"><p>چاکر خواجه عمیدم من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه با خطر زدوده او</p></div>
<div class="m2"><p>تیره باشد ستاره روشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوبتر چیز درجهان سخنست</p></div>
<div class="m2"><p>خلق آن خواجه خوبتر ز سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست او جود رابکارترست</p></div>
<div class="m2"><p>زآنکه تاری چراغ را روغن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چه یابد ببخشد و ننهد</p></div>
<div class="m2"><p>بر ستانندگان مال منن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر دلش ز ایران بدانندی</p></div>
<div class="m2"><p>باژگونه بر او نهندی من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زایران را مثل نماز برد</p></div>
<div class="m2"><p>چون شمن در بهار پیش وثن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این قیاسیست ورنه زایر او</p></div>
<div class="m2"><p>نه وثن باشد و نه خواجه شمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قلم او چو لعبتیست بدیع</p></div>
<div class="m2"><p>زیر انگشت او گرفته وطن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روزی دوستان ازو زاید</p></div>
<div class="m2"><p>چون ز امضاش گردد آبستن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای بزرگ بزرگوار کریم</p></div>
<div class="m2"><p>ای دلت جود و علم را معدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این جهان با دل تو تنگترست</p></div>
<div class="m2"><p>از دل زفت و چشمه سوزن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فضل و کردارهای خوب ترا</p></div>
<div class="m2"><p>نتوان کرد هیچ پاداشن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر ترا دسترس فزونستی</p></div>
<div class="m2"><p>زر به پیمانه می ببخشی و من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زر دنیا به پیش بخشش تو</p></div>
<div class="m2"><p>نگراید به دانه ارزن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کس نیابد بهیچ روی و نیافت</p></div>
<div class="m2"><p>نیکنامی به زرق و حیله وفن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو بزرگی و نیکنامی وعز</p></div>
<div class="m2"><p>به سخایافتی و خلق حسن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هیچکس جزبه نام نیک و به فضل</p></div>
<div class="m2"><p>بر نیاورد نام تو به دهن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فضل تو رایض موفق بود</p></div>
<div class="m2"><p>نیکنامی چو کره توسن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رایضان کرگان به زین آرند</p></div>
<div class="m2"><p>گر چه توسن بوند ومرد افکن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا بود در دو زلف خوبان پیچ</p></div>
<div class="m2"><p>واندر آن پیچ صد هزار شکن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا بود لهو و خوشی اندر عشق</p></div>
<div class="m2"><p>خوشیی باهزار گونه فتن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کامران باش و شادمانه بزی</p></div>
<div class="m2"><p>دشمنانت اسیر گرم و حزن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فرخت باد و فر خجسته بواد</p></div>
<div class="m2"><p>سده و عید فرخ بهمن</p></div></div>