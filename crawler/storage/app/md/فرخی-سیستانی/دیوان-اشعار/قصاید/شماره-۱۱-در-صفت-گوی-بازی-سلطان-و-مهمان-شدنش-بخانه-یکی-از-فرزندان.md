---
title: >-
    شمارهٔ ۱۱ - در صفت گوی بازی سلطان و مهمان شدنش بخانه یکی از فرزندان
---
# شمارهٔ ۱۱ - در صفت گوی بازی سلطان و مهمان شدنش بخانه یکی از فرزندان

<div class="b" id="bn1"><div class="m1"><p>ای فعل تو ستوده و گفتارهات راست</p></div>
<div class="m2"><p>دایم ترا بفضل و بآزادگی هواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوشش تو شاه، بهر جای هیبتست</p></div>
<div class="m2"><p>وز بخشش تو میر بهر خانه یی نواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فضل ترا همی نبود منتهی پدید</p></div>
<div class="m2"><p>آنرا که از شماره برون شد چه منتهاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چوگان زدی بشادی با بندگان خویش</p></div>
<div class="m2"><p>چوگان زدن ز خلق جهان مر ترا سزاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوی ترا ستاره نیایش کندهمی</p></div>
<div class="m2"><p>گوید که قدر و منزلت و مرتبت تراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خواهمی که چون تو بمیدان شتابمی</p></div>
<div class="m2"><p>کانجای جای مرتبت و عز و کبریاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر اختیار مابود آنجای جای ماست</p></div>
<div class="m2"><p>آنجایگاه بودن ما نه بدست ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوی تو بر ستاره شرف دارد ای امیر</p></div>
<div class="m2"><p>گوی به از ستاره، به جز مر ترا کراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این جاه و این شرف ز تو گوی ترا فزود</p></div>
<div class="m2"><p>تو آگهی که این سخن بنده است راست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیدا بود که گوی ترا تا کجاست قدر</p></div>
<div class="m2"><p>پیدا بود که گوی ترا تا کجا بهاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویی بخدمت تو بدین جایگه رسید</p></div>
<div class="m2"><p>گو را بر آسمان سخن افتاد و نام خاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرماکه بندگان تو باشیم بگذریم</p></div>
<div class="m2"><p>از آسمان بمنزلت و مرتبت رواست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکس که بنده تو شد ای شاه بنده نیست</p></div>
<div class="m2"><p>آنکس که بنده تو شد ای شاه پادشاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای میزبان لشکر سلطان و آن خویش</p></div>
<div class="m2"><p>امروز میزبان چو تو اندر جهان کجاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهمان تو به خوان تو برحق گمان برد</p></div>
<div class="m2"><p>گوید که از خدای مرا این شرف عطاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بنگرد بزرگی بیند بدست چپ</p></div>
<div class="m2"><p>چون بنگرد سعادت بیند بدست راست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا این سمای روی گشاده نه چون زمی است</p></div>
<div class="m2"><p>تا این زمین باز کشیده نه چون سماست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندر جهان تو باش او پدر میزبان خلق</p></div>
<div class="m2"><p>کاین عادت از ملوک جهان خاصه شماست</p></div></div>