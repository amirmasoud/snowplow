---
title: >-
    شمارهٔ ۲۰۶ - در تهنیت مهرگان و مدح عضد الدوله امیر یوسف
---
# شمارهٔ ۲۰۶ - در تهنیت مهرگان و مدح عضد الدوله امیر یوسف

<div class="b" id="bn1"><div class="m1"><p>مهرگان رسم عجم داشت به پای</p></div>
<div class="m2"><p>جشن اوبود چو چشم اندربای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا در شدم از اول روز</p></div>
<div class="m2"><p>با می اندر شدم و بر بط و نای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تامه روزه در آمیخت بدوی</p></div>
<div class="m2"><p>آنهمه رسم نکو ماند به جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارها تنگ گرفته ست بدوی</p></div>
<div class="m2"><p>روزه تنگخوی کج فرمای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنین ماه چنین جشن بود</p></div>
<div class="m2"><p>همچو در مزکت آدینه سرای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین سبب دان که تسلی منست</p></div>
<div class="m2"><p>میر ابو یعقوب آن بار خدای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عضد دولت یوسف کز فضل</p></div>
<div class="m2"><p>هر چه بایست بدو داد خدای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بزرگان و ز تدبیر گران</p></div>
<div class="m2"><p>پیشدستست به تدبیر و به رای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زو مبارزتر و زو پر دلتر</p></div>
<div class="m2"><p>ننهد کس به رکیب اندر پای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دایم از زنگ زره بر تن او</p></div>
<div class="m2"><p>چون پرباز بود پشت قبای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جنگجوییست که با حمله او</p></div>
<div class="m2"><p>نبود هیچ مبارز را پای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیچ کس نیست که با شاه جهان</p></div>
<div class="m2"><p>یک سخن گوید ازین شاه ستای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوید: ای بار خدای ملکان</p></div>
<div class="m2"><p>یا همایونتر از بال همای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن دل را دو تن نازک را</p></div>
<div class="m2"><p>رنج واندیشه چندین منمای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا کی این رنج ره و گرد سفر</p></div>
<div class="m2"><p>وین تکاپوی دراز و شو و آی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لشکر آرای چنین یافته ای</p></div>
<div class="m2"><p>تو بیاسای و ز شادی ماسای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چه ناکرده بمانده ست ترا</p></div>
<div class="m2"><p>در بر او کن و اورافرمای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>او خود اندیشه کار تو برد</p></div>
<div class="m2"><p>دل زاندیشه به یکره بزدای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تاببینی که به یک سال کند</p></div>
<div class="m2"><p>پر زدینار و درم قلعه نای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>او همانست که پیش تو ستد</p></div>
<div class="m2"><p>دره کشمیر از لشکر رای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>او همانست که از گردن خویش</p></div>
<div class="m2"><p>مرد را کرد به رمح اندروای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جوشن خویش در او پوش و مپوش</p></div>
<div class="m2"><p>تو برو بازوی خوبان فرسای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر همه گیتی اورا بگمار</p></div>
<div class="m2"><p>وانگهی برهمه گیتی بخشای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گربه جنگ آید پوشیده زره</p></div>
<div class="m2"><p>وای برهر که به جنگ آید وای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شیر آهن خای آن روز شود</p></div>
<div class="m2"><p>از نهیب و ز فزع بازو خای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اسب اورا چه لقب ساخته اند</p></div>
<div class="m2"><p>مملکت گیر و ولایت پیمای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اسب او با کوس آموخته تر</p></div>
<div class="m2"><p>ز اشتر پیر به آواز درای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای فریدن ظفر رستم دل</p></div>
<div class="m2"><p>ای مبارز شکر گرد ربای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آخر این کارترا باید کرد</p></div>
<div class="m2"><p>دل بدین دارو بدین کار گرای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو بدین از همه شایسته تری</p></div>
<div class="m2"><p>همچنین باش و همه ساله تو شای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ناگشاده به جهان آنچه بماند</p></div>
<div class="m2"><p>تو به فرمان شهنشه بگشای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دوستانش را یک یک بنواز</p></div>
<div class="m2"><p>دشمنانش را یک یک بگزای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو بزی خرم و پاینده بباش</p></div>
<div class="m2"><p>روز و شب مجلس و میدان آرای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گل و می خواه بر این جشن امشب</p></div>
<div class="m2"><p>از رخ نخشبی و دولب قای</p></div></div>