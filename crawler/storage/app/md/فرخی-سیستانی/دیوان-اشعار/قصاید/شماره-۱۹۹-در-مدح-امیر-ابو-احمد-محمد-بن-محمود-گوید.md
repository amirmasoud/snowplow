---
title: >-
    شمارهٔ ۱۹۹ - در مدح امیر ابو احمد محمد بن محمود گوید
---
# شمارهٔ ۱۹۹ - در مدح امیر ابو احمد محمد بن محمود گوید

<div class="b" id="bn1"><div class="m1"><p>ای باد بهاری خبر از یار چه داری</p></div>
<div class="m2"><p>پیغام گل سرخ سوی باده کی آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم ز اول روز از تو همی بوی خوش آید</p></div>
<div class="m2"><p>گویی همه شب سوخته ای عود قماری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف بت من داشته ای دوش در آغوش</p></div>
<div class="m2"><p>نی نی تو هنوز این دل و این زهره نداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید بر آن ماه زمین تافت نیارد</p></div>
<div class="m2"><p>دانم که تو باز لفک او جست نیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو با گل و سوسن زن و و من با لب و زلفش</p></div>
<div class="m2"><p>ور برگ بود بنشین تا بوسه شماری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دوش به کف داشتم آن زلف همه شب</p></div>
<div class="m2"><p>وز دولب او کرده ام امروز نهاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای فرخی این قصه و این حال چه چیزست</p></div>
<div class="m2"><p>پیش ملک شرق همی خواب گزاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه ملکان میر محمد که مر اوراست</p></div>
<div class="m2"><p>از آمل و از ساری تازان سوی باری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهی که ترا نعمت صد ساله بریزد</p></div>
<div class="m2"><p>گر بر در او نیم زمان پای فشاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شادی و خوشی خواهی رو خدمت او کن</p></div>
<div class="m2"><p>تا عمر به شای و به خوشی بگذاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون خدمت او کردی و او در تو نگه کرد</p></div>
<div class="m2"><p>فربه بشوی از نعمت او گر چه نزاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>افزون دهداز طمع و ز اندیشه توبر</p></div>
<div class="m2"><p>تخمی که در آن خدمت فرخنده بکاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بار خدای ملکان ای ملک راد</p></div>
<div class="m2"><p>ای آنکه همی حق همه کس بگزاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گویی که خدا از پی آن داد ترا ملک</p></div>
<div class="m2"><p>تا کار تبه کرده هر کس بنگاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک دست تو ابرست و دگردست تو دریا</p></div>
<div class="m2"><p>هرگز نتوانی که نبخشی و نباری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رسم شعرا ازتو هزار و دو هزارست</p></div>
<div class="m2"><p>آخر ده هزاری شوی و بیست هزاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فردا همه کار تو دگر خواهد گشتن</p></div>
<div class="m2"><p>امروز میندیش که در اول کاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خوابم نبرد تابه سرای تو نبینم</p></div>
<div class="m2"><p>چون کوه فرو ریخته دینار نثاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از دولت سلطان و ز نیکو نیت تو</p></div>
<div class="m2"><p>این کار شود ساخته و محکم و کاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گیتی همه همواره ترا خواهد گشتن</p></div>
<div class="m2"><p>زان گونه که هرگز به دگر کس نسپاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن روز خورم خوش که درین خابه ببینم</p></div>
<div class="m2"><p>زین پنج هزاری رده ترکان حصاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وین درگه و این دشت پر از خیمه و پر میر</p></div>
<div class="m2"><p>شهر از بنه ایشان پر مهد و عماری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از روم رسیده بر تو هدیه رومی</p></div>
<div class="m2"><p>و آورده ز بلغار ترا باز شکاری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شاهان جهان روی نهاده بردر تو</p></div>
<div class="m2"><p>وز درد شده روی بداندیش تو تاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من شاد همی گردم ز آنجای بدانجای</p></div>
<div class="m2"><p>وین شعر به آواز برآورده چو قاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوالحارث ما آمده و ساخته با هم</p></div>
<div class="m2"><p>چون طوطیک و شاری و چون طوطی و ساری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در خانه تو دولت و درخانه تو ملک</p></div>
<div class="m2"><p>در خانه آن کس که جز این خواهد زاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وآن کس که تر از دل و جان دوست ندارد</p></div>
<div class="m2"><p>چون سنگ ز بیقدری و چون خاک ز خواری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو اسی تو باروحی کالوی و فخری (؟)</p></div>
<div class="m2"><p>بدخواه تو مانده پی بی باره و داری (؟)</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ارجو که ترا تا ابد الد هر به هر کار</p></div>
<div class="m2"><p>توفیق بود ز ایزد و ازدولت یاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آزاده خداوندی و خوشخوی کریمی</p></div>
<div class="m2"><p>بافر شهنشاهی وبا زیب سواری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پردانش و پر خیری و پر فضلی و پر شرم</p></div>
<div class="m2"><p>باسایه و با سنگی و با حلم و وقاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن چیست ز کردار بسنده که ترا نیست</p></div>
<div class="m2"><p>آن چیست زنیکویی و خوبی که نداری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از دانش و فضل تو سخنهاست به هر جا</p></div>
<div class="m2"><p>اندازه ندارد هنرو فضل تو باری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برخور تو ازین دانش و برخور تو ار این فضل</p></div>
<div class="m2"><p>برخور تو از ین جشن و از این فصل بهاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاهی کن و شادی کن و آنکن که تو خواهی</p></div>
<div class="m2"><p>ای داده ترا هر چه بباید همه باری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شادی ز بتان خیزد، در پیش بتاندار</p></div>
<div class="m2"><p>با جعد سمر قندی و با زلف بخاری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همواره بود در بر تو هر شب و هر روز</p></div>
<div class="m2"><p>ترکی که کند طره او غالیه باری</p></div></div>