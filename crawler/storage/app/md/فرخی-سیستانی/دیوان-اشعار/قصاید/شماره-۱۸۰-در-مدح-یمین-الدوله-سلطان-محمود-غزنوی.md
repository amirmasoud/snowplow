---
title: >-
    شمارهٔ ۱۸۰ - در مدح یمین الدوله سلطان محمود غزنوی
---
# شمارهٔ ۱۸۰ - در مدح یمین الدوله سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>به فرخی و به شادی و شاهی ایران شاه</p></div>
<div class="m2"><p>به مهرگانی بنشست بامداد پگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآن که چون بکند مهرگان به فرخ روز</p></div>
<div class="m2"><p>به جنگ دشمن واژون کشد به سغد سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مهر ماه ز بهر نشستن و خوردن</p></div>
<div class="m2"><p>به تابخانه فرستند شهریاران گاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایگان جهان آنکه از خدای جهان</p></div>
<div class="m2"><p>جهانیان را پاداشنست و باد افراه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مهرگان بکند خانه را ز سر فکند</p></div>
<div class="m2"><p>به جنگ و تاختن دشمنان بودشش ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی سپه به فرازی برون برد که به چشم</p></div>
<div class="m2"><p>چو زو نگاه کنی مه نماید اندر چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی به ژرف نشیبی سرای پرده زند</p></div>
<div class="m2"><p>چنانکه ماهی از افراز آن نماید ماه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه زمستان در پیش برگرفته بود</p></div>
<div class="m2"><p>رهی دراز دراز و شبی سیاه سیاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی گشاید گیتی همی کشد دشمن</p></div>
<div class="m2"><p>به مردمی که جهان راجز او نزیبد شاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی شهی که مه و سال در پرستش تو</p></div>
<div class="m2"><p>همی کنند شهان بزرگ پشت دوتاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شهریاری کس چون تو بسته نیست کمر</p></div>
<div class="m2"><p>به خسروی چو تو کس نیست بر نهاده کلاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تویی که مردی را نام نیک تست فروغ</p></div>
<div class="m2"><p>تویی که رادی را دست رادتست پناه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز پادشاهان کس را ستوده نام نبود</p></div>
<div class="m2"><p>بجز ترا که نکوهیده شد به تو بدخواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گاه کینه کند ناو تو از گل گل</p></div>
<div class="m2"><p>به روز رزم کند خنجر تو از که کاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار شیر شناسم که پیشت آمد و تو</p></div>
<div class="m2"><p>در او چنان نگریدی که شیر در روباه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمین اگر چه فراخست جای نیست درو</p></div>
<div class="m2"><p>که تو درو نزدی بیست راه لشکر گاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشستگان شهان باغ و کاخ و خانه بود</p></div>
<div class="m2"><p>نشستگاه تو دشتست و خوابگه خرگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسا شها که نیارد ز خرد جوی گذشت</p></div>
<div class="m2"><p>تو چند راه گذشتی چنین ز رود بیاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو ز آبهایی بگذشته ای به شب که ازو</p></div>
<div class="m2"><p>به روز پیل نیارد برون شدن به شناه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز پادشاهان نگرفت جز تو در یک روز</p></div>
<div class="m2"><p>ز کرگ سی و سه، وز پیل پانصد و پنجاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ایا ستوده به مردی، چو پیش بین به خرد</p></div>
<div class="m2"><p>ایا زدوده ز آهو چو پارساز گناه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خدایت از پی جنگ آفرید و ز پی جود</p></div>
<div class="m2"><p>بسیج رزم کن و جنگ جوی و دشمن کاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همیشه تا چو گل از گل بروید و ندمد</p></div>
<div class="m2"><p>ز روی آتش سوزنده سبز و تازه گیاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا نتواند شد ایچ کس به جهان</p></div>
<div class="m2"><p>زر از ایزد همچون زر از خویش آگاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خدایگان جهان باش و پادشاه زمین</p></div>
<div class="m2"><p>ستوده بر کش و از بندگان ستایش خواه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو نو بهار به تو چشمها همه روشن</p></div>
<div class="m2"><p>چو روزگار ز تو دستها همه کوتاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خجسته بادت و فرخنده جشن و فخر باد</p></div>
<div class="m2"><p>به سغد رفتن و بیرون شدن ز خانه به راه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تباه کرده هر کس همی شود به تو راست</p></div>
<div class="m2"><p>مباد کس که کند راست کرده تو تباه</p></div></div>