---
title: >-
    شمارهٔ ۱۳ - در مدح خواجه بزرگ احمد بن حسن میمندی گوید
---
# شمارهٔ ۱۳ - در مدح خواجه بزرگ احمد بن حسن میمندی گوید

<div class="b" id="bn1"><div class="m1"><p>ای وعده تو چون سر زلفین تو نه راست</p></div>
<div class="m2"><p>آن وعده های خوش که همی کرده ای کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من همه حدیث وفا داشتی عجب</p></div>
<div class="m2"><p>آگه نبوده ام که ترا پیشه جز وفاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بر تو بستم و بتو بس کردم از جهان</p></div>
<div class="m2"><p>وندر جهان ز من دل من دیدن تو خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دشمنان کرانه گرفتی ز دوستان</p></div>
<div class="m2"><p>تا قول دشمنان من اندر تو گشت راست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی ترا زمن نرسد غم نه این غمست</p></div>
<div class="m2"><p>گفتی ترا جفا ننمایم نه این جفاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با اینهمه جفا که دلم را نموده ای</p></div>
<div class="m2"><p>دل بر تو شیفته ست ندانم چنین چراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد عیب دارد این دل مسکین و یک هنر</p></div>
<div class="m2"><p>کو را بکدخدای جهان از جهان هواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه بزرگ شمس کفاة احمد حسن</p></div>
<div class="m2"><p>کاحسان او و نعمت او دستگیر ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن معطیی که روز و شب از بهر نام نیک</p></div>
<div class="m2"><p>در پوزش مروت و در دادن عطاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از فضلهای صاحب سید سخا یکیست</p></div>
<div class="m2"><p>هر چند برترین همه فضلها سخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر همه جهان بر خلق همه جهان</p></div>
<div class="m2"><p>این فضل واین مروت و این نعمت آشناست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خواجگان دولت سلطان بهر نماز</p></div>
<div class="m2"><p>او را دعا کنید که او در خور دعاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با دشمنان دولت او دشمنی کنید</p></div>
<div class="m2"><p>از بهر آنکه دولت او دولت شماست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا او نشسته باشد شاد اندرین مکان</p></div>
<div class="m2"><p>شور و بلا ز جای نیارد بپای خاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنجا که اوست راحت و آرام عالمست</p></div>
<div class="m2"><p>وانجا که نیست او همه شور و همه بلاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اندر سلامتش همه کس را سلامتست</p></div>
<div class="m2"><p>واندر بقاش دولت اسلام را بقاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چند کس بسر نشود پیش هیچکس</p></div>
<div class="m2"><p>پیشش بسر شوید و مگویید کاین خطاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر هیچکس بخدمت نیکو سزا بود</p></div>
<div class="m2"><p>او را کنید خدمت نیکو که او سزاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او را شما بچشم وزارت نگه کنید</p></div>
<div class="m2"><p>او بر همه جهان و همه چیز پادشاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر چه بود وزارت او حشمت بزرگ</p></div>
<div class="m2"><p>این حشمت وزارت او حشمت خداست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>او را چنانکه اوست ندانم همی ستود</p></div>
<div class="m2"><p>از چند سال باز دل من دراین عناست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درفضل و در کفایت او چون رسد سخن</p></div>
<div class="m2"><p>این فضل واین کفایت او را چه منتهاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرخ پی است بر ملک و برهمه جهان</p></div>
<div class="m2"><p>وین ایمنی و نعمت چندین برین گواست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شور جهان بحشمت خواجه فرونشست</p></div>
<div class="m2"><p>در هر دلی نشاط بیفزود و غم بکاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر ملک و خاندان ملک مشفقی نمود</p></div>
<div class="m2"><p>گر مشفقی نمود مر او را ملک رواست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنرا که او همی بود اندر هوای شاه</p></div>
<div class="m2"><p>این نعمت و کرامت و این نیکویی جزاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دایم صلاح خواجه هوای ملک بود</p></div>
<div class="m2"><p>کاندر هوای شاه دل خواجه چون هواست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با دوستان شاه جهان خواجه یکدلست</p></div>
<div class="m2"><p>با دشمنان او همه ساله دلش دو تاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر چشم دشمنانش چون نوک سوزنست</p></div>
<div class="m2"><p>در چشم دوستانش چون سوده توتیاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا این سمای بر شده باشد بر از زمین</p></div>
<div class="m2"><p>تا این زمین پست شده زیر این سماست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بادا فرود همت تو بر شده سپهر</p></div>
<div class="m2"><p>چونانکه دون رفعت نصر تواش بناست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دایم ترا وزارت و شه را شهنشهی</p></div>
<div class="m2"><p>پیوسته باد کاین دو همی آرزوی ماست</p></div></div>