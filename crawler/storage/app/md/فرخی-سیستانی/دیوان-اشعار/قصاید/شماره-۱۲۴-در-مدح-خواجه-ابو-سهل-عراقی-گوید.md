---
title: >-
    شمارهٔ ۱۲۴ - در مدح خواجه ابو سهل عراقی گوید
---
# شمارهٔ ۱۲۴ - در مدح خواجه ابو سهل عراقی گوید

<div class="b" id="bn1"><div class="m1"><p>کی نشینیم نگارا من و تو هر دوبهم</p></div>
<div class="m2"><p>کی نهم روی بدان روی و بدان زلف بخم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندازین فرقت و بر جان ز غم فرقت رنج</p></div>
<div class="m2"><p>چند ازین دوری و بر دل ز پی دوری غم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب و آتش به تکلف بهم آیند همی</p></div>
<div class="m2"><p>چه فتاده ست که ماهیچ نیاییم بهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونکه در نیکوییت بر من و بر تو ستمست</p></div>
<div class="m2"><p>ما بر اینگونه ستم دیده و ناکرده ستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاشکی کار من و توبه درم راست شدی</p></div>
<div class="m2"><p>تا من از بهر ترا کردمی از دیده درم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد کرد درم از دیده چرا باید کرد</p></div>
<div class="m2"><p>مرمرا با کرم خواجه درم ناید کم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجه سید بوسهل عراقی که بفضل</p></div>
<div class="m2"><p>نه عرب دیده چنو بار خدا و نه عجم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه زو بیشتر و پیشتر اندر همه فضل</p></div>
<div class="m2"><p>بر سلطان ملک مشرق ننهاد قدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا از کف او وز دل او یاد کنی</p></div>
<div class="m2"><p>یاد کردی ز سخا یاد نمودی ز کرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر تو گویی که مر اورا به کرم نیست نظیر</p></div>
<div class="m2"><p>همه گویند بلی و همه گویند نعم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نتوان کرد بتدبیر فراوان و بتیغ</p></div>
<div class="m2"><p>آنچه او داند کردن به دوات و به قلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هنر ملک جهان زیر قلم کرد و سزید</p></div>
<div class="m2"><p>که بزرگان جهان را به قلم کرد خدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس از ایزد به دوات و قلم فرخ اوست</p></div>
<div class="m2"><p>روزی لشکر سلطان و همه خیل و حشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آصف است او و ملک جم پیمبر بقیاس</p></div>
<div class="m2"><p>آری او آصف باشد چو ملک باشد جم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا شه او را بوزارت بنشانده ست شده ست</p></div>
<div class="m2"><p>صدر دیوان بدو آراسته چون باغ ارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بس ره خوب که در مجلس دیوان ملک</p></div>
<div class="m2"><p>بوجود آورد آن خواجه سید زعدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الم از دلها بر گیردو تابوده هگرز</p></div>
<div class="m2"><p>بر دل کس ننهاده ست به یکموی الم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از کریمی چو در آید بر او زایر او</p></div>
<div class="m2"><p>از کریمی چو شمن گردد و زایر چو صنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ابر خوانی کف او را بگه جود مخوان</p></div>
<div class="m2"><p>کز کف خواجه درم بارد و از ابر دیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بخشش ابر نگویند بر بخشش او</p></div>
<div class="m2"><p>سخن از جوی نرانند بر وادی زم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مدحت آنست که بد را بسخن خوب کند</p></div>
<div class="m2"><p>چو جز این گفتی آن مدح همه باشد ذم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ابر پیش کف او همچو بر یم شمرست</p></div>
<div class="m2"><p>زشت باشد که بگویی به شمر ماند یم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او به رادی و جوانمردی معروفترست</p></div>
<div class="m2"><p>زانکه باران بزاینده به تری و به نم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر کجا گویی بوسهل وزیر شه شرق</p></div>
<div class="m2"><p>همه گویند کریم و سخی و خوب شیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لاجرم روی بزرگان همه سوی در اوست</p></div>
<div class="m2"><p>حاجبند ایشان گویی و در خواجه حرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا می لعل گزیده ست به خوبی و به رنگ</p></div>
<div class="m2"><p>تا گل سرخ ستوده ست به دیدار و به شم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا بود شادی جایی که بود زاری زیر</p></div>
<div class="m2"><p>تا بود رامش جایی که بود ناله بم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شادمان باد و بشادی وطرب نوش کناد</p></div>
<div class="m2"><p>باده از دست بتی خوبتر از بدر ظلم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیکخواهانش پیوسته بشادی و به عز</p></div>
<div class="m2"><p>بدسکالانش همواره به تیمار و ندم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دست و پای از تن دشمنش جدا باد بتیغ</p></div>
<div class="m2"><p>تا خزد دشمن چون مارهمیشه به شکم</p></div></div>