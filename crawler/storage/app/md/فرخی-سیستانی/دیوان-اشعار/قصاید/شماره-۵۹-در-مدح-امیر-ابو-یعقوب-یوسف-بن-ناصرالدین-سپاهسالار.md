---
title: >-
    شمارهٔ ۵۹ - در مدح امیر ابو یعقوب یوسف بن ناصرالدین سپاهسالار
---
# شمارهٔ ۵۹ - در مدح امیر ابو یعقوب یوسف بن ناصرالدین سپاهسالار

<div class="b" id="bn1"><div class="m1"><p>دوش متواریک بوقت سحر</p></div>
<div class="m2"><p>اندر آمد به خیمه آن دلبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست گفتی شده ست خیمه من</p></div>
<div class="m2"><p>میغ و او در میان میغ قمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنگ در بر گرفت و خوش بنواخت</p></div>
<div class="m2"><p>وز دو بسد فرو فشاند شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست گفتی به بتکده ست درون</p></div>
<div class="m2"><p>بتی و بت پرستی اندر بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنج شش می کشید و پر گل گشت</p></div>
<div class="m2"><p>روی آن روی نیکوان یکسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست گفتی رخش گلستان بود</p></div>
<div class="m2"><p>می سوری بهار گل پرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست گشت و ز بهر خفتن ساخت</p></div>
<div class="m2"><p>خویش را از کنار من بستر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راست گفتی کنار من صدفست</p></div>
<div class="m2"><p>کاندر و جای خویش ساخت گهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلف مشکین بروی بر پوشید</p></div>
<div class="m2"><p>روی خود زیر کردو زلف زبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راست گفتی کسی نهان کرده ست</p></div>
<div class="m2"><p>سمن تازه زیر سیسنبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زلف او را بدست بگرفتم</p></div>
<div class="m2"><p>زنخ گرد او بدست دگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راست گفتی نشسته ام بر او</p></div>
<div class="m2"><p>گوی و چوگان شه بدست اندر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پادشه زاده یوسف آنکه هنر</p></div>
<div class="m2"><p>جز بنزدیک او نکرد مقر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راست گفتی هنر یتیمی بود</p></div>
<div class="m2"><p>فرد مانده ز مادر و ز پدر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس بازی گوی شد خسرو</p></div>
<div class="m2"><p>بر یکی تازی اسب که پیکر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>راست گفتی بباد بر، جم بود</p></div>
<div class="m2"><p>گر بود باد را ستام به زر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خم چوگان بگوی بر زد و شد</p></div>
<div class="m2"><p>گوی او با ستارگان همبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راست گفتی برابر خورشید</p></div>
<div class="m2"><p>خواهد از گوی ساختن اختر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از سر گوی زیر او برخاست</p></div>
<div class="m2"><p>آن که که گذار بحر گذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>راست گفتی سپهر کانون گشت</p></div>
<div class="m2"><p>و اختران اندر آن میان اخگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلزله در زمین فتاد و خروش</p></div>
<div class="m2"><p>از تکاپوی آن که ره بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راست گفتی زمین بخود میگشت</p></div>
<div class="m2"><p>زیر آن باد بیستون منظر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کوه بر تافت این زمین و نتافت</p></div>
<div class="m2"><p>بار آن کوه سنب کوه سپر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>راست گفتی جبال حلم امیر</p></div>
<div class="m2"><p>بار آن کوه پاره بود مگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بر آیین نشسته بود بر او</p></div>
<div class="m2"><p>آن شه گردبند شیر شکر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>راست گفتی قضای نیکستی</p></div>
<div class="m2"><p>بر نشسته مکابره به قدر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دیدی او را بدین گران رتبت</p></div>
<div class="m2"><p>که چسان کشت شیر شرزه نر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>راست گفتی که همچو فرهادست</p></div>
<div class="m2"><p>بیتسون را همی کند به تبر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر به لاهور بودتی دیدی</p></div>
<div class="m2"><p>که چه کرد از دلیری و ز هنر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>راست گفتی درختها بودند</p></div>
<div class="m2"><p>بارشان: تیر و نیزه و خنجر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رده گرد سپاه بگرفتند</p></div>
<div class="m2"><p>گیر ها گیر شد همه که ودر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>راست گفتی سپاه یأجوج اند</p></div>
<div class="m2"><p>که نه اندازه شان پدید و نه مر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شاه ایران به تاختن شد تیز</p></div>
<div class="m2"><p>رفت و با شاه نی سپاه و حشر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>راست گفتی همی بمجلس رفت</p></div>
<div class="m2"><p>یا از آن تاختن نداشت خبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پشت آن لشکر قوی بشکست</p></div>
<div class="m2"><p>وز پس آن نشست بی لشکر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>راست گفتی که نره شیری بود</p></div>
<div class="m2"><p>گله غرم و آهو اندر بر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تیر او خورده بودی اندر دل</p></div>
<div class="m2"><p>هر که ز ایشان فرو نهادی سر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>راست گفتی جدای گشت به تیر</p></div>
<div class="m2"><p>دل ایشان یکایک از پیکر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روزی اندر حصار برهمنان</p></div>
<div class="m2"><p>اوفتاد آن شه ستوده سیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>راست گفتی که آن حصار بلند</p></div>
<div class="m2"><p>خیبر ستی و میر ما حیدر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دی همی آمد از بر سلطان</p></div>
<div class="m2"><p>آن نکو منظر نکو مخبر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>راست گفتی سفندیارستی</p></div>
<div class="m2"><p>بر نهاده کلاه و بسته کمر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفتم از خلق او سخن گویم</p></div>
<div class="m2"><p>نوز نابرده این حدیث بسر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>راست گفتی کسی بمن بر بیخت</p></div>
<div class="m2"><p>نافه مشک و بیضه عنبر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خود مر او را بخواب دیدم دوش</p></div>
<div class="m2"><p>پیش او توده کرده زیور و زر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>راست گفتی یکی درختی بود</p></div>
<div class="m2"><p>برگ او زر و بار او زیور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شادمان باد و می دهش صنمی</p></div>
<div class="m2"><p>که چنویی ندیده صورتگر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>راست گفتی بدستش اندر گشت</p></div>
<div class="m2"><p>جام با رنگ شعله آذر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر کفش سال و ماه باد میی</p></div>
<div class="m2"><p>کز خمش چون بکند دهقان سر،</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>راست گفتی بر آمد از سر خم</p></div>
<div class="m2"><p>ماهی از آفتاب روشن تر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فرخش باد عید آنکه به عید</p></div>
<div class="m2"><p>کارد بنهاد بر گلوی پسر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>راست گفتی دو نیمه خواهد کرد</p></div>
<div class="m2"><p>لاله یی را ببرگ نیلوفر</p></div></div>