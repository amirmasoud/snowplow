---
title: >-
    شمارهٔ ۱۲۱ - در مدح عضدالدوله امیر یوسف سپاهسالار گوید
---
# شمارهٔ ۱۲۱ - در مدح عضدالدوله امیر یوسف سپاهسالار گوید

<div class="b" id="bn1"><div class="m1"><p>ای ز سیمینه فکنده در بلورینه مدام</p></div>
<div class="m2"><p>هم بساعد چون بلوری هم بتن چون سیم خام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو داری ماه بار و ماه داری لاله پوش</p></div>
<div class="m2"><p>لاله داری باده رنگ و باده داری لعل فام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف تو مشک سیاه و جعد تو شمشاد تر</p></div>
<div class="m2"><p>قد تو سرو بلند و روی تو ماه تمام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف تو دامست و دایم بر دو رخ گسترده دام</p></div>
<div class="m2"><p>گر نه صیادی چه حاجت دام گستردن مدام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور همیگویی بگیرم تا مرا گردد حلال</p></div>
<div class="m2"><p>دل بتو بخشیدم و بخشیده کی باشد حرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بتو دادم تو نیز از روی رحمت گه گهی</p></div>
<div class="m2"><p>نیکویی کن با من و از من سوی دل بر پیام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقم برتو و چون دانی که بر تو عاشقم</p></div>
<div class="m2"><p>عاشقم خوانی همی اندر میان خاص و عام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقم آری و لیکن نام من عاشق مکن</p></div>
<div class="m2"><p>مرمرا ای ماه منظر مادح میرست نام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر یوسف یادگار نصر الدین آنکه دین</p></div>
<div class="m2"><p>زو همی گردد قوی و زو همی گیرد قوام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش سایل زر بر افشاند به هنگام جواب</p></div>
<div class="m2"><p>پیش نحوی موی بشکافد به هنگام کلام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز ز شاه شرق سلطان فضل او بر هر شهی</p></div>
<div class="m2"><p>همچنان دانم که فضل نور باشد برظلام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس بیابان بادسا و کوهها کوبا ملک (؟)</p></div>
<div class="m2"><p>هم محلها بریمه کرده ست او از حسام (؟)</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رایتش ساکن نگردد یک زمان در یک زمین</p></div>
<div class="m2"><p>رخشش آرامش نگیرد ساعتی در یک مقام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از نهیب خنجر خونخوار او روز نبرد</p></div>
<div class="m2"><p>خون برون آید بجای خوی عدو را از مسام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ز تیغش تافتی آتش فشاندی آفتاب</p></div>
<div class="m2"><p>ورز کفش خاستی دینار باریدی غمام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماهی اندرآب روشن راه چون داند برید</p></div>
<div class="m2"><p>هم بدانسان راه برد تیر او اندر عظام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای امارت را چو جمشید، ای ولایت را چو جم</p></div>
<div class="m2"><p>ای شجاعت را چو سهراب ای سیاست را چو سام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم موفق پادشاهی هم مظفر شهریار</p></div>
<div class="m2"><p>هم مؤید رای میری، هم همایون فرهمام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با همه پیغمبران اندر فضیلت همسری</p></div>
<div class="m2"><p>جز که از ایزد نیاوردی بما وحی و کلام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از پی قدر و بزرگی روز می خوردن ترا</p></div>
<div class="m2"><p>آسمان خواهد که باشد ساقی و خورشید جام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روز رزم و روز بزم اندر هنر داری هنر</p></div>
<div class="m2"><p>هم سرافراز ملوکی هم سر افراز کرام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حاتم طایی که چندین نام دارد در سخا</p></div>
<div class="m2"><p>اشتری کشتی و دادی سایلی را زو طعام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو زمال خویش نندیشی و هم بدهی به طبع</p></div>
<div class="m2"><p>گر ثواب از تو بخواهد سایلی روز قیام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از فراوان طوف سایل گرد قصرت روز و شب</p></div>
<div class="m2"><p>قصر تو نشناسد ای خسرو کس از بیت الحرام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بس نیاید تا زدینار تو چون شداد عاد</p></div>
<div class="m2"><p>سایل تو خانه را زرین کند دیوار و بام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عالمی زرین کنی چون بر نهی باده به دست</p></div>
<div class="m2"><p>کشوری پر خون کنی چون بر کشی تیغ زنیام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک سوار از موکب تو و ز عدو پنجاه پیل</p></div>
<div class="m2"><p>صد سوار از موکب بدخواه و از تو یک غلام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رایت تو سایه افکنده ست بر دریای سند</p></div>
<div class="m2"><p>کی بود شاها که سایه افکند برکوه شام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اسب تو هنگام جستن نسبتی دارد ز باد</p></div>
<div class="m2"><p>وقت آسایش نهادی دارد از کوه سیام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر ز غزنینش برانگیزی بوقت چاشتگاه</p></div>
<div class="m2"><p>بگذراند مر ترا از شام پیش از وقت شام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن زمان هشیارتر باشد که در پوشی زره</p></div>
<div class="m2"><p>وان زمان بیدارتر باشد که بر گیری حسام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا ندیدم مرکبت را من ندانستم که هست</p></div>
<div class="m2"><p>باد را سیمین رکاب و کوه را زرین ستام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای به هر رایی موافق، ای به هر کاری مصیب</p></div>
<div class="m2"><p>ای به هر علمی ستوده، ای به هر فضلی تمام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر که را بینم مهیا بینم اندر شکر تو</p></div>
<div class="m2"><p>همچو من کز نعمت تو بهره ای دارم تمام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شکر تو بر من فراوان واجبست ای شهریار</p></div>
<div class="m2"><p>از فراوانی ندانم گفت شکرت را کدام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چیست نیکوتر زجاه، از تو رسیدستم به جاه</p></div>
<div class="m2"><p>چیست شیرین تر ز کام، از تو رسیدستم به کام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مدح گفتن مر ترا آسان بود زیرا که تو</p></div>
<div class="m2"><p>عاشق خوی کرامی، دشمن خوی لئام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در خصال تو شهنشاها چنان آمد مدیح</p></div>
<div class="m2"><p>کز مدیح تو صدف لؤلؤ همیخواهد به وام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ازفراوان مدح کاندر خلق تو پایم همی</p></div>
<div class="m2"><p>خویشتن راباز نشناسم همی از بوتمام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا بود چون روی رومی، روزتابان و سپید</p></div>
<div class="m2"><p>تا بود چون روی زنگی، شب دژم گون و نفام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا چو سیمین دستی اندر آستین شعرا همی</p></div>
<div class="m2"><p>سر بر آرد پیش روز ار پیش مشرق صبح تام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عمر تو پاینده باد و نعمت تو با بقا</p></div>
<div class="m2"><p>بخت تو پیروز باد و دولت تو با نظام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>روز و شب خورشید و ماه از روی عجز و انکسار</p></div>
<div class="m2"><p>آید اندر درگه عالیت از بهر سلام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عیدرا شادان گذار و ناطلب کرده بیاب</p></div>
<div class="m2"><p>ز ایزد پاداش ده پاداشن ماه صیام</p></div></div>