---
title: >-
    شمارهٔ ۱۳۸ - نیز در مدح سلطان محمد بن سلطان محمود گوید
---
# شمارهٔ ۱۳۸ - نیز در مدح سلطان محمد بن سلطان محمود گوید

<div class="b" id="bn1"><div class="m1"><p>گفتم مرا سه بوسه ده ای شمسه بتان</p></div>
<div class="m2"><p>گفتا ز حور بوسه نیابی درین جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ز بهر بوسه جهانی دگر مخواه</p></div>
<div class="m2"><p>گفتابهشت را نتوان یافت رایگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم نهان شوی تو چرا از من ای پری</p></div>
<div class="m2"><p>گفتا پری همیشه بود زآدمی نهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ترا همی نتوان دید ماه ماه</p></div>
<div class="m2"><p>گفتاکه ماه را نتوان دید هر زمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم نشان تو ز که پرسم، نشان بده</p></div>
<div class="m2"><p>گفتا آفتاب را بتوان یافت بی نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که کوژ کرد مرا قدت ای رفیق</p></div>
<div class="m2"><p>گفتا رفیق تیرکه باشد به جز کمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم غم تو چشم مرا پر ستاره کرد</p></div>
<div class="m2"><p>گفتاستاره کم نتوان کرد ز آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم ستاره نیست سرشکست ای نگار</p></div>
<div class="m2"><p>گفتا سرشک بر نتوان چید ز آبدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم به آب دیده من روی تازه کن</p></div>
<div class="m2"><p>گفتا به آب تازه توان داشت بوستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم بروی روشن تو روی برنهم</p></div>
<div class="m2"><p>گفتا که آب گل ببرد رنگ زعفران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم مرا فراق تو ای دوست پیر کرد</p></div>
<div class="m2"><p>گفتا بمدحت شه گیتی شوی جوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم کدام شاه نشان ده مرا بدو</p></div>
<div class="m2"><p>گفتا خجسته پی پسر خسرو زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم ملک محمد محمود کامکار</p></div>
<div class="m2"><p>گفتا ملک محمد محمود کامران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتم مرابه خدمت او رهنمای کیست</p></div>
<div class="m2"><p>گفتا ضمیر روشن و طبع و دل و زبان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم بروز بار توان رفت پیش او</p></div>
<div class="m2"><p>گفتا چو یک مدیح نو آیین بری توان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم نخست گوچه نثاری برش برم</p></div>
<div class="m2"><p>گفتا نثار شاعر مدحست، مدح خوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتم چه خوانمش که ز نامش رسم بمدح</p></div>
<div class="m2"><p>گفتا امیر و خسرو و شاه و خدایگان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم ثواب خدمت او چیست خلق را</p></div>
<div class="m2"><p>گفت اینجهان هوای دل و آنجهان جنان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم همه دلایل سودست خدمتش</p></div>
<div class="m2"><p>گفتابلی معاینه سودست بی زیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتم چو خوی نیکوی او هیچ خو بود</p></div>
<div class="m2"><p>گفتاچو روزگار بهاری بود خزان ؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتم چو رای روشن او باشد آفتاب؟</p></div>
<div class="m2"><p>گفتابهیچ حال چو آتش بود دخان ؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفتم زمین برابر حلمش گران بود</p></div>
<div class="m2"><p>گفتاشگفت کاه برکه بود گران ؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتم به علم و عدل چنو هیچ شه بود ؟</p></div>
<div class="m2"><p>گفتاخبر برابر بوده ست با عیان ؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتم زمانه شاه گزیند بر او دگر</p></div>
<div class="m2"><p>گفتاگزیده هیچ کسی بر یقین گمان ؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم چه مایه داد بدو مملکت خدای</p></div>
<div class="m2"><p>گفتااز این کران جهان تابدان کران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفتم که قهرمان همه گنجهاش کیست</p></div>
<div class="m2"><p>گفتاسخای او نه بسنده ست قهرمان ؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفتم بگرد مملکتش پاسدار کیست</p></div>
<div class="m2"><p>گفتامهابتش نه بسنده ست پاسبان ؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتم گه عطا به چه ماند دو دست او</p></div>
<div class="m2"><p>گفتا دو دست او بدو ابر گهر فشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتم نهند روی بدو زایران ز دور</p></div>
<div class="m2"><p>گفتا زکاروان نبریده ست کاروان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم کزو بشکر چه مقدار کس بود</p></div>
<div class="m2"><p>گفتا زشاکرانش تهی نیست یک مکان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفتم بخدمتش ملکان متصل شوند</p></div>
<div class="m2"><p>گفتاستاره نیز کند با قمر قران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتم سنان نیزه او چیست باز گوی</p></div>
<div class="m2"><p>گفتاستاره ای که بود برجش استخوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتم چگونه بگذرد از درقه روز جنگ</p></div>
<div class="m2"><p>گفتاکجا چنان سر سوزن ز پرنیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتم خدنگ او چه ستاند بروز رزم؟</p></div>
<div class="m2"><p>گفتا از مبارزان سپاه عدو روان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفتم چو صاعقه ست گهر دار تیغ او</p></div>
<div class="m2"><p>گفتاجدا کننده جسم عدو ز جان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفتم امان نیابد از آن تیغ هیچ کس؟</p></div>
<div class="m2"><p>گفتاموافقان همه یابند ازو امان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفتم چو برگ نیلوفر بود پیش ازین</p></div>
<div class="m2"><p>گفتاکنون ز خون عدو شد چو ارغوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفتم چو بنگری به چه ماند، به دست میر</p></div>
<div class="m2"><p>گفتابه اژدها که گشاده کند دهان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفتم که شادمانه زیاد آن سر ملوک</p></div>
<div class="m2"><p>گفتاکه شاد و آنکه بدو شاد، شادمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفتم زمانه خاضع اوباد سال و ماه</p></div>
<div class="m2"><p>گفتاخدای ناصر او باد جاودان</p></div></div>