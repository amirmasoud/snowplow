---
title: >-
    شمارهٔ ۱۵۹ - در مدح شمس الکفات احمد بن الحسن میمندی
---
# شمارهٔ ۱۵۹ - در مدح شمس الکفات احمد بن الحسن میمندی

<div class="b" id="bn1"><div class="m1"><p>گفتم گلست یا سمنست آن رخ و ذقن</p></div>
<div class="m2"><p>گفتا یکی شکفته گلست و یکی سمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم در آن دو زلف شکن بیش یا گره</p></div>
<div class="m2"><p>گفتایکی همه گره است و یکی شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم چه چیز باشد زلفت در آن رخت</p></div>
<div class="m2"><p>گفتا یکی پرند سیاه و یکی پرن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم دو زلف تو چه فشانند بر دورخ</p></div>
<div class="m2"><p>گفتا یکی به تنگ عبیر و یکی به من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم زمن چه بردند آن نرگس دو چشم</p></div>
<div class="m2"><p>گفتا یکی قرار تو برد ویکی وسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم تن من و دل من چیست مر ترا</p></div>
<div class="m2"><p>گفتا یکی میان منست و یکی دهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم بلای من همه زین دیده و دلست</p></div>
<div class="m2"><p>گفتا یکی از این دو بسوز و یکی بکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم مراد و بوسه فروش و بها بخوان</p></div>
<div class="m2"><p>گفتا یکی به جان بخر از من یکی به تن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم که جان طلب کنی از من به بوسه ای</p></div>
<div class="m2"><p>گفتا یکی همی ز تو باشد یکی زمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم دو چیز چیست ز روی تو خوبتر</p></div>
<div class="m2"><p>گفتا یکی سخاوت صاحب یکی سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم که نام صاحب و نام پدرش چیست</p></div>
<div class="m2"><p>گفتا یکی خجسته پی احمد یکی حسن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم رضا و خدمت صاحب چه کم کند</p></div>
<div class="m2"><p>گفتا یکی نیاز ولی و یکی محن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم دو دست خواجه چه چیزست جود را</p></div>
<div class="m2"><p>گفتا یکی خجسته مکان و یکی وطن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتم دو گونه طوق به هر گردن افکند</p></div>
<div class="m2"><p>گفتا یکی ز شکر فکند و یکی ز من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم دلش چه دارد وعقلش چه پرورد</p></div>
<div class="m2"><p>گفتا یکی مودت دین و یکی سنن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم چه پیشه دارد مهر و هوای او</p></div>
<div class="m2"><p>گفتا یکی ملال زداید یکی حزن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتم چه چیز یابد ازو ناصح و عدو</p></div>
<div class="m2"><p>گفتا یکی نوازش و خلعت یکی کفن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم موافقانرا مهرو هواش چیست</p></div>
<div class="m2"><p>گفتا یکی سلیح تمام و یکی مجن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم که گر دو تیر گشاید سوی چگل</p></div>
<div class="m2"><p>گفتا یکی چگل بستاند یکی ختن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتم که گردونامه فرستد سوی عمان</p></div>
<div class="m2"><p>گفتا یکی عمان بستاندیک عدن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتم چه باد حاسد او وان دگرچه باد</p></div>
<div class="m2"><p>گفتا یکی به مادر غمگین یکی به زن</p></div></div>