---
title: >-
    شمارهٔ ۲۰۰ - در مدح امیر ابواحمد محمد بن محمود غزنوی
---
# شمارهٔ ۲۰۰ - در مدح امیر ابواحمد محمد بن محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>دل من خواهی و اندوه دل من نبری</p></div>
<div class="m2"><p>اینْت بی‌رحمی و بی‌مهری و بیدادگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو بر آنی که دل من ببری دل ندهی</p></div>
<div class="m2"><p>من بدین پرده نیم، گر تو بدین پرده دری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم تو چند خورم و انده تو چند برم</p></div>
<div class="m2"><p>نخورم تا نخوریّ و نبرم تا نبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زمان گویی بر دو رخ و بر عارض من</p></div>
<div class="m2"><p>قمرست و سمن تازه خوشبوی طری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کنم گر تو به عارض چو شکفته سمنی</p></div>
<div class="m2"><p>چه کنم گر تو به رخ همچو دو هفته قمری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش از آن باشد کز عشق تومن موی شدم</p></div>
<div class="m2"><p>سال تا سال خروش و ماه تا ماه گری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع افروخته بینم چو به تو در نگرم</p></div>
<div class="m2"><p>شمع ناسوخته بینی چو به من درنگری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بندگی خواهی از من بخر از میر مرا</p></div>
<div class="m2"><p>بنده تو نشوم تا تو ز میرم نخری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاصه آن بنده که ماننده من بنده بود</p></div>
<div class="m2"><p>مدح گوینده و داننده الفاظ دری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سال تا سال همه مدحت او نظم کنم</p></div>
<div class="m2"><p>نکند میر دل از مهر چنین بنده بری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میر ابو احمد شهزاده محمد ملکی</p></div>
<div class="m2"><p>حق شناسنده و معروف به نیکو سیری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر گهر باید او هست امیری گهری</p></div>
<div class="m2"><p>ور هنر بادی اوهست امیری هنری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ملکزاده امیریکه ز ابناء ملوک</p></div>
<div class="m2"><p>به کمال و به خرد بیشتر و پیشتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس پسر کونه به کام و به مراد پدرست</p></div>
<div class="m2"><p>تو ملکزاده به کام و به مراد پدری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مراد پدری وین ز قوی دولت تست</p></div>
<div class="m2"><p>لاجرم چون به مراد پدری بر بخوری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پدر از خوی تو شادست تو هم شادان باش</p></div>
<div class="m2"><p>که همی سخت نکو دانی کردن پیری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پسر آن ملکی تو که زبان رنجه شود</p></div>
<div class="m2"><p>گر ز آثار فتوحش تو یکی برشمری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پسر آن ملکی تو که ز پولاد سپر</p></div>
<div class="m2"><p>با سر ناوک او کرد نداند سپری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گوهری نیست پسندیده تر از گوهر تو</p></div>
<div class="m2"><p>با پسندیدگی گوهر فخر گهری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاه فرخنده پی و میری آزاده خویی</p></div>
<div class="m2"><p>گرد لشکر شکن و شیری دشمن شکری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برترین چیزی شاهان را نیکو نظریست</p></div>
<div class="m2"><p>هیچ کس نیست ترا یار به نیکو نظری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به علی مردمی و مردی نامی شد و تو</p></div>
<div class="m2"><p>گر علی نیستی ای میر علی رادگری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بادل حیدری و برخوی عثمان، چه عجب</p></div>
<div class="m2"><p>زانکه بادانش بوبکری و عدل عمری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هم به رادی علمی و هم به مردی علمی</p></div>
<div class="m2"><p>هم به حری سمری هم به کریمی سمری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خطری شاهی، وز نعمت و جاه تو شود</p></div>
<div class="m2"><p>مردم خطی اندر کنف تو خطری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بحر، جایی که کف راد تو باشد ثمرست</p></div>
<div class="m2"><p>بلکه پیش کف تو کرد نداند شمری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون بر آهنجی شمشیر و فروپوشی درع</p></div>
<div class="m2"><p>پشت و روی سپهی اصل و فروع ظفری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باش تا با پدر خویش به کشمیر شوی</p></div>
<div class="m2"><p>لشکر ساخته خویش به کشمیر بری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن نمایی که فرامرز ندانست نمود</p></div>
<div class="m2"><p>به دلیری و به تدبیر نه از خیره سری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کافر کشته بهم بر نهی و تابه تبت</p></div>
<div class="m2"><p>به سم باره به کافور همی پی سپری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من به نظاره جنگ آیم و از بخشش تو</p></div>
<div class="m2"><p>مرمرا باره پدید آیدو ساز سفری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>میر مر ساز سفر داد مرا لیکن من</p></div>
<div class="m2"><p>همه ناچیز و تبه کردم از بی بصری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیش ازین شاه ترا جنگ نفرمود همی</p></div>
<div class="m2"><p>تا بدید آنکه تو چون پر دلی وپرجگری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون بفرمود که امسال به جنگ آی و برو</p></div>
<div class="m2"><p>تا بداند که تو با زهره تر از شیر نری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا نیامیزد با زاغ سیه باز سپید</p></div>
<div class="m2"><p>تانیامیزد با باز خشین کبک دری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا نباشد به هنر آهو همتای هزبر</p></div>
<div class="m2"><p>تا نباشد به گهر مردم همتای پری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شادبادی و همه ساله به تو شاد پدر</p></div>
<div class="m2"><p>شادیی کان نشود تا به قیامت سپری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در حضر گوشه تو همچو نگار چگلی</p></div>
<div class="m2"><p>در سفرمرکب تو همچو بت کاشغری</p></div></div>