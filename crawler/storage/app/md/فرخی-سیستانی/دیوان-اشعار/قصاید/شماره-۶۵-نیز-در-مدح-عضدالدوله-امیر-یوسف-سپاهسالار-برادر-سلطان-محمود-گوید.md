---
title: >-
    شمارهٔ ۶۵ - نیز در مدح عضدالدوله امیر یوسف سپاهسالار برادر سلطان محمود گوید
---
# شمارهٔ ۶۵ - نیز در مدح عضدالدوله امیر یوسف سپاهسالار برادر سلطان محمود گوید

<div class="b" id="bn1"><div class="m1"><p>همی نسیم گل آرد بباغ بوی بهار</p></div>
<div class="m2"><p>بهار چهر منا! خیز و جام باده بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه باده حرامست ظن برم که مگر</p></div>
<div class="m2"><p>حلال گردد بر عاشقان بوقت بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدای، نعمت، مارا ز بهر خوردن داد</p></div>
<div class="m2"><p>بیا و نعمت او را ز ما دریغ مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه نعمتست به از باده باده خوارانرا</p></div>
<div class="m2"><p>همین بسست و گر چند نعمتش بسیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخاصه اکنون کز سنگ خاره لاله دمید</p></div>
<div class="m2"><p>ز لاله کوه چو دیبای لعل شد هموار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گلبنان شکفته چنان نماید باغ</p></div>
<div class="m2"><p>که میر پره زدستی بدشت بهر شکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امیر ما عضد دولت و مؤید دین</p></div>
<div class="m2"><p>درامید بزرگان وقبله احرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزرگواری کاندر میان گوهر خویش</p></div>
<div class="m2"><p>پدیدتر زعلم در میان صف سوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مبارزی که بمردی و چیره دستی و رنگ</p></div>
<div class="m2"><p>چنو یکی نبود در میان بیست هزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دومرد زنده نماند که صلح تاند کرد</p></div>
<div class="m2"><p>در آن حصار که او یک دو تیر برد بکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بروی باره اگر برزند ببازی تیر</p></div>
<div class="m2"><p>ز سوی دیگر تیرش برون شود زحصار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلاح در خور قوت هزار من کندی</p></div>
<div class="m2"><p>اگر نیابد او را ز بهر بازی یار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کمان اورا بینی فتاده پنداری</p></div>
<div class="m2"><p>مهینه شاخی افتاده از مهینه چنار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنو سوار نیارد نگاشتن به قلم</p></div>
<div class="m2"><p>اگر چه باشد صورتگری بدیع نگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دور هر که مراورا بدید یکره گفت</p></div>
<div class="m2"><p>زهی سوار نکو طلعت نکو دیدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خوب طلعتی و از نکو سواری کوست</p></div>
<div class="m2"><p>ز دیدنش نشود سیر دیده انظار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نکو لقا و نکو عادت و نکو سخنست</p></div>
<div class="m2"><p>نکو خصال و نکو مذهب و نکو کردار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درم کشست و کریمی که در خزانه او</p></div>
<div class="m2"><p>درم نیابد چندانکه بر کشد زوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درم که بر همه شاهان بزرگ دارد قدر</p></div>
<div class="m2"><p>بر امیر ندارد به ذره ای مقدار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر بیابد روزی هزار تنگ درم</p></div>
<div class="m2"><p>هزار و صد بدهد کارش این بود هموار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا غم آید اگر چه مرا دلیست فراخ</p></div>
<div class="m2"><p>زمال دادن و بخشیدن بدان کردار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان ملک را بایدکه باشدی هر روز</p></div>
<div class="m2"><p>خزانه پردرم و پر سلیح و پر دینار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو خرج خویش فزونتر زدخل خویش کند</p></div>
<div class="m2"><p>ز زر وسیم خزانه تهی شود ناچار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دگر که نام نکو یافته ست، و نام نکو</p></div>
<div class="m2"><p>نکوتر از گهر نابسوده صد خروار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شریفتر زان چیزی بود که محتشمان</p></div>
<div class="m2"><p>همی کنند بهر جای فضل او تکرار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بزرگتر زان چیزی کجا بود که ازو</p></div>
<div class="m2"><p>همی رسد ز دل و دست او به دستگزار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر آنچه من ز کریمی و فضل او گویم</p></div>
<div class="m2"><p>کنند باور و بر من نباید استغفار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رسد ز خدمت او بی خطر بجاه و خطر</p></div>
<div class="m2"><p>کند ز خدمت او بی یسار ملک و یسار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا بخدمتش امروز بهترست از دی</p></div>
<div class="m2"><p>مرا بدولتش امسال خوشترست از پار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هزار سال زیاد این بزرگوار ملک</p></div>
<div class="m2"><p>عزیز باد و عدو را ذلیل کرده وخوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خجسته بادش نوروز و همچنان همه روز</p></div>
<div class="m2"><p>بشادکامی برکف گرفته جام عقار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همیشه در بر او کودکی چو لعبت چین</p></div>
<div class="m2"><p>همیشه مونس او لعبتی چو نقش بهار</p></div></div>