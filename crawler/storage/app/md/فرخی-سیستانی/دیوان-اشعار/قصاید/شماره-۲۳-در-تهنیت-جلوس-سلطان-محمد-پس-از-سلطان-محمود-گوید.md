---
title: >-
    شمارهٔ ۲۳ - در تهنیت جلوس سلطان محمد پس از سلطان محمود گوید
---
# شمارهٔ ۲۳ - در تهنیت جلوس سلطان محمد پس از سلطان محمود گوید

<div class="b" id="bn1"><div class="m1"><p>هر که بود از یمین دولت شاد</p></div>
<div class="m2"><p>دل بمهر جمال ملت داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که او حق نعمتش بشناخت</p></div>
<div class="m2"><p>میر مارا نوید خدمت داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاعت آن ملک بجا آورد</p></div>
<div class="m2"><p>هرکه او دل برین امیر نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت رفتن ملک بمیر سپرد</p></div>
<div class="m2"><p>لشکر خویش و بنده و آزاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت بر تخت مملکت بنشین</p></div>
<div class="m2"><p>تا بتو نام من بماند یاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه ویران شد ازتغافل من</p></div>
<div class="m2"><p>جهد کن تا مگر کنی آباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینت نیکو وصیت و فرمان</p></div>
<div class="m2"><p>ایزد آن شاه را بیامرزاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر آن شاه جاودانه نزیست</p></div>
<div class="m2"><p>این خداوند جاودانه زیاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل بخندد زیاد این بر سنگ</p></div>
<div class="m2"><p>آب گردد ز درد آن پولاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انده او دل گشاده ببست</p></div>
<div class="m2"><p>رامش میر بسته ها بگشاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمع داریم و شمع پیش نهیم</p></div>
<div class="m2"><p>گر بکشت آن چراغ ما را باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر برفت آن ملک بما بگذاشت</p></div>
<div class="m2"><p>پادشاهی کریم و پاک نژاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخت خوب آید این دو بیت مرا</p></div>
<div class="m2"><p>که شنیدم ز شاعری استاد :</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>« پادشاهی گذشت پاک نژاد</p></div>
<div class="m2"><p>پادشاهی نشست فرخ زاد»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر گذشته همه جهان غمگین</p></div>
<div class="m2"><p>وز نشسته همه جهان دلشاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چراغی زما گرفت جهان</p></div>
<div class="m2"><p>باز شمعی بپیش ما بنهاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای خداوند خسروان جهان</p></div>
<div class="m2"><p>ای جهانرا بجای جم و قباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملک با رای تو قرار گرفت</p></div>
<div class="m2"><p>بخت در پیش تو بپا استاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کارهای جهان بکام تو گشت</p></div>
<div class="m2"><p>گفتگوی تو در جهان افتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه شگفت ار ز فر دولت تو</p></div>
<div class="m2"><p>روید از شوره پیش تو شمشاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا بشاهی نشستی از پی تو</p></div>
<div class="m2"><p>هفت کشور همی شود هفتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خلق را قبله گشت خانه تو</p></div>
<div class="m2"><p>همچو زین پیش خانه نوشاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پدر پیش بین تو بتو شاه</p></div>
<div class="m2"><p>بس قوی کرد ملک را بنیاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ملک چو کشت گشت و تو باران</p></div>
<div class="m2"><p>این جهان چون عروس و تو داماد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چاکرانند بر در تو کنون</p></div>
<div class="m2"><p>برتر از طوس و نوذر و کشواد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از پی تهنیت خلیفه بتو</p></div>
<div class="m2"><p>بفرستد کس، ار بنفرستاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای امیری که در زمانه تو</p></div>
<div class="m2"><p>نیست شد نام زفتی و بیداد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کف برادی گشاده چشم به مهر</p></div>
<div class="m2"><p>دست دادت خدای با کف راد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زائر از تو بخرمی و طرب</p></div>
<div class="m2"><p>درم از تو بناله و فریاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تخت شاهی و پادشاهی و ملک</p></div>
<div class="m2"><p>برتو و بر زمانه فرخ باد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون پدر کامکار باش که تو</p></div>
<div class="m2"><p>پدر دیگری برسم و نهاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ماه خرداد بر تو فرخ باد</p></div>
<div class="m2"><p>آفرین باد بر مه خرداد</p></div></div>