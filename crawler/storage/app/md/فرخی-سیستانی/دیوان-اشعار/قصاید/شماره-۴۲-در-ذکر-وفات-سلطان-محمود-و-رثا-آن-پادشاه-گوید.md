---
title: >-
    شمارهٔ ۴۲ - در ذکر وفات سلطان محمود و رثاء آن پادشاه گوید
---
# شمارهٔ ۴۲ - در ذکر وفات سلطان محمود و رثاء آن پادشاه گوید

<div class="b" id="bn1"><div class="m1"><p>شهر غزنین نه همانست که من دیدم پار</p></div>
<div class="m2"><p>چه فتاده ست که امسال دگرگون شده کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه ها بینم پر نوحه و پر بانگ وخروش</p></div>
<div class="m2"><p>نوحه و بانگ وخروشی که کند روح فکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کویها بینم پر شورش و سر تاسر کوی</p></div>
<div class="m2"><p>همه پر جوش و همه جوشش از خیل سوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسته ها بینم بی مردم و درهای دکان</p></div>
<div class="m2"><p>همه بربسته و بر در زده هر یک مسمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاخها بینم پرداخته از محتشمان</p></div>
<div class="m2"><p>همه یکسر ز ربض برده به شارستان بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهتران بینم بر روی زنان همچمو زنان</p></div>
<div class="m2"><p>چشمهاکرده زخونابه برنگ گلنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجبان بینم خسته دل و پوشیده سیه</p></div>
<div class="m2"><p>کله افکنده یکی از سر و دیگر دستار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بانوان بینم بیرون شده از خانه بکوی</p></div>
<div class="m2"><p>بر در میدان گریان و خروشان هموار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجگان بینم برداشته از پیش دوات</p></div>
<div class="m2"><p>دستها بر سر و سرها زده اندر دیوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاملان بینم باز آمده غمگین ز عمل</p></div>
<div class="m2"><p>کار ناکرده و نارفته بدیوان شمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مطربان بینم گریان و ده انگشت گزان</p></div>
<div class="m2"><p>رودها بر سر و بر روی زده شیفته وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لشکری بینم سر گشته سراسیمه شده</p></div>
<div class="m2"><p>چشمها پرنم و از حسرت و غم گشته نزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این همان لشکریانند که من دیدم دی؟</p></div>
<div class="m2"><p>وین همان شهرو زمین است که من دیدم پار؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر امسال ملک باز نیامد ز عزا ؟</p></div>
<div class="m2"><p>دشمنی روی نهاده ست برین شهر و دیار؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر امسال زهر خانه عزیزی گم شد ؟</p></div>
<div class="m2"><p>تا شد از حسرت و غم روز همه چون شب تار؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگر امسال چو پیرار بنالید ملک ؟</p></div>
<div class="m2"><p>نی من آشوب ازین گونه ندیدم پیرار؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو نگویی چه فتادست؟ بگو گر بتوان</p></div>
<div class="m2"><p>من نه بیگانه ام، این حال زمن باز مدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این چه شغلست و چه آشوب و چه بانگست و خروش</p></div>
<div class="m2"><p>این چه کارست و چه با رست و چه چندین گفتار؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاشکی آنشب و آنروز که ترسیدم از آن</p></div>
<div class="m2"><p>نفتادستی و شادی نشدستی تیمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کاشکی چشم بد اندر نرسیدی به امیر</p></div>
<div class="m2"><p>آه ترسم که رسید و شده مه زیر غبار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رفت و ما را همه بیچاره و درمانده بماند</p></div>
<div class="m2"><p>من ندانم که چه درمان کنم این را و چه چار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آه ودردا ودریغاکه چو محمود ملک</p></div>
<div class="m2"><p>همچو هر خاری در زیر زمین ریزد خوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آه و دردا که همی لعل به کان باز شود</p></div>
<div class="m2"><p>او میان گل و از گل نشود برخوردار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آه ودردا که بی او هرگز نتوانم دید</p></div>
<div class="m2"><p>باغ فیروزی پر لاله و گلهای ببار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آ و دردا که بیکبار تهی بینم ازو</p></div>
<div class="m2"><p>کاخ محمودی و آن خانه پر نقش و نگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آه و دردا که کنون قرمطیان شاد شوند</p></div>
<div class="m2"><p>ایمنی یابند از سنگ پراکنده و دار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آه ودردا که کنون قیصر رومی برهد</p></div>
<div class="m2"><p>از تکاپوی بر آوردن برج و دیوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آه و دردا که کنون برهمنان همه هند</p></div>
<div class="m2"><p>جای سازند بتان را دگر از نو به بهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>میر ما خفته بخاک اندر و ما از بر خاک</p></div>
<div class="m2"><p>این چه روزست بدین تاری یا رب ز نهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فال بدچون زنم این حال جز اینست مگر</p></div>
<div class="m2"><p>زنم آن فال که گیرد دل از آن فال قرار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>میر می خورده مگر دی وبخفته ست امروز</p></div>
<div class="m2"><p>دی خفتست مگر رنج رسیدش زخمار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کوس نوبتش همانا که همی زان نزنند</p></div>
<div class="m2"><p>تا بخسبد خوش وکمتر بودش بر دل بار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای امیر همه میران و شهنشاه جهان</p></div>
<div class="m2"><p>خیز و از حجره برون آی که خفتی بسیار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خیز شاها! که جهان پر شغب و شور شده ست</p></div>
<div class="m2"><p>شور بنشان و شب و روز بشادی بگذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خیز شاها! که به قنوج سپه گرد شده است</p></div>
<div class="m2"><p>روی زانسو نه و بر تار کشان آتش بار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خیز شاها! که رسولان شهان آمده اند</p></div>
<div class="m2"><p>هدیه ها دارند آورده فراوان و نثار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خیز شاها! که امیران بسلام آمده اند</p></div>
<div class="m2"><p>بارشان ده که رسیده ست همانا گه بار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خیز شاها! که به فیروزی گل باز شده ست</p></div>
<div class="m2"><p>بر گل نو قدحی چند می لعل گسار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خیز شاها! که به چوگانی گرد آمده اند</p></div>
<div class="m2"><p>آنکه با ایشان چوگان زده ای چندین بار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خیز شاها! که چو هر سال به عرض آمده اند</p></div>
<div class="m2"><p>از پس کاخ تو و باغ تو، پیلی دوهزار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خیز شاها! که همه دوخته و ساخته گشت</p></div>
<div class="m2"><p>خلعت لشکر وگردید بیکجای انبار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خیز شاها! که بدیدار تو فرزند عزیز</p></div>
<div class="m2"><p>بشتاب آمد بنمای مر اورا دیدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>که تواند که برانگیرد زین خواب ترا</p></div>
<div class="m2"><p>خفتی آن خفتن کز بانگ نگردی بیدار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر چنان خفتی ای شه که نخواهی برخاست</p></div>
<div class="m2"><p>ای خداوند! جهان خیز و بفرزند سپار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خفتن بسیار ای خسرو خوی تو نبود</p></div>
<div class="m2"><p>هیچکس خفته ندیده ست ترا زین کردار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خوی تو تاختن و شغل سفر بود مدام</p></div>
<div class="m2"><p>بنیا سودی هر چند که بودی بیمار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در سفر بودی تا بودی و درکار سفر</p></div>
<div class="m2"><p>تن چون کوه تو از رنج سفر گشته نزار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سفری کانرا باز آمدن امید بود</p></div>
<div class="m2"><p>غم او کم بود، ار چند که باشد دشوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سفری داری امسال شها اندر پیش</p></div>
<div class="m2"><p>که مر آنرا نه کرانست پدید و نه کنار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یک دمک باری درخانه ببایست نشست</p></div>
<div class="m2"><p>تا بدیدندی روی تو عزیزان و تبار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رفتن تو به خزان بودی وهر سال شها</p></div>
<div class="m2"><p>چه شتاب آمد کامسال برفتی به بهار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون کنی صبر و جدا چند توانی بودن</p></div>
<div class="m2"><p>زان برادر که بپروردی او را بکنار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تن او از غم و تیمار تو چون موی شده ست</p></div>
<div class="m2"><p>رخ چون لاله او زرد به رنگ دینار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از فراوان که بگرید بسر گور تو شاه</p></div>
<div class="m2"><p>آب دیده بشخوده ست مر اورا رخسار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آتشی دارد در دل که همه روز از آن</p></div>
<div class="m2"><p>برساند بسوی گنبد افلاک شرار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر برادر غم تو خورد شها نیست عجب</p></div>
<div class="m2"><p>دشمنت بی غم تو نیست به لیل و به نهار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرغ و ماهی چو زنان برتو همی نوحه کنند</p></div>
<div class="m2"><p>همه با ما شده اندر غم و اندوه تو یار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>روزو شب بر سر تابوت تو از حسرت تو</p></div>
<div class="m2"><p>کاخ پیروزی چون ابر همی گرید زار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بحصار از فزع و بیم تو رفتند شهان</p></div>
<div class="m2"><p>تو شها از فزع و بیم که رفتی بحصار؟</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تو بباغی چو بیابانی دلتنگ شدی</p></div>
<div class="m2"><p>چون گرفتستی در جایگهی تنگ قرار؟</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نه همانا که جهان قدر تو دانست همی</p></div>
<div class="m2"><p>لاجرم نزد خردمند ندارد مقدار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زینت و قیمت و مقدار، جهان را بتو بود</p></div>
<div class="m2"><p>تا تو رفتی ز جهان این سه برون شد یکبار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شعرا را بتو بازار برافروخته بود</p></div>
<div class="m2"><p>رفتی و با تو بیکبار شکست آن بازار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ای امیری که وطن داشت بنزدیک تو فخر</p></div>
<div class="m2"><p>ای امیری که نگشته ست بدرگاه توعار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همه جهد تو در آن بود که ایزد فرمود</p></div>
<div class="m2"><p>رنج کش بودی در طاعت ایزد هموار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بگذاراد و بروی تو میاراد هرگز</p></div>
<div class="m2"><p>زلتی را که نکردی تو بدان استغفار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>زنده بادا بولیعهد تو نام تو مدام</p></div>
<div class="m2"><p>ای شه نیکدل نیکخوی نیکوکار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دل پژمان بولیعهد تو خرسند کناد</p></div>
<div class="m2"><p>این برادر که ز درد تو زد اندر دل نار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اندر آن گیتی ایزد دل تو شاد کناد</p></div>
<div class="m2"><p>به بهشت و به ثواب و به فراوان کردار</p></div></div>