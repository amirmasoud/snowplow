---
title: >-
    شمارهٔ ۴ - در مدح امیر ابواحمد محمد بن محمود بن ناصرالدین سبکتگین
---
# شمارهٔ ۴ - در مدح امیر ابواحمد محمد بن محمود بن ناصرالدین سبکتگین

<div class="b" id="bn1"><div class="m1"><p>تا ببردی از دل و از چشم من آرام و خواب</p></div>
<div class="m2"><p>گه ز دل در آتش تیزم گه از چشم اندر آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو باچار چیزم یار دارد هشت چیز</p></div>
<div class="m2"><p>مرمرا هر ساعتی زین غم جگر گردد کباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با رخم زر و زریر و با دلم گرم و زحیر</p></div>
<div class="m2"><p>با دو چشمم آب و خون و با تنم رنج و عذاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وین عجایب تر که چون این هشت با من یار کرد</p></div>
<div class="m2"><p>هشت چیز از من ببرد و هشت چیز تنگیاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راحت وآرام روح ورامش و تسکین دل</p></div>
<div class="m2"><p>نزهت ودیدار چشم و زینت و فرشباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در رگ و اندر تن و اندر دل و در چشم من</p></div>
<div class="m2"><p>خواب و صبر و روح و خونم را بر افتاد انقلاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنج دارد جای خون و درد دارد جای روح</p></div>
<div class="m2"><p>عشق دارد جای صبر و آب دارد جای خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این تنم از هجر تو چون برگ بید اندر خزان</p></div>
<div class="m2"><p>این دلم در عشق تو چون توزی اندر ماهتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی تو بسترد و بربود و بیفکند و ببرد</p></div>
<div class="m2"><p>چارچیز از چارچیز و هر یکی را کرد غاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرمی از نوبهار و تازگی از سرخ گل</p></div>
<div class="m2"><p>نیکویی از گرد ماه و روشنی از آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چار چیز تو نباشد سال و مه بی هشت چیز</p></div>
<div class="m2"><p>هر یکی زان هشت دارد سوی دل بردن شتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم تو بی خواب و سهر و روی تو بی سیم و گل</p></div>
<div class="m2"><p>جعد توبی چین و پیچ و زلف تو بی بند و تاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تاب زلفین و خم جعد تو نشناسم همی</p></div>
<div class="m2"><p>از خم و تاب کمند خسرو مالک رقاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میر ابواحمد محمد خسرو ایران زمین</p></div>
<div class="m2"><p>کایزد او را چند چیز نیک داد از چند باب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از هنر نام بلند و از شرف جاه عریض</p></div>
<div class="m2"><p>از ادب لفظ بدیع و از خرد رای صواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با هنر دست سخی و با شرف روی نکو</p></div>
<div class="m2"><p>با خرد خوی نکو با سخن فصل الخطاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر گز او در چار وقت از چار چیز اندر نماند</p></div>
<div class="m2"><p>عجز هرگز پیش یک نهمت نگشت او راحجاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وقت کردار از توان و وقت پیکار از عدو</p></div>
<div class="m2"><p>وقت دیدار از صواب و وقت گفتار از جواب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هشت چیز از او ببرد از هشت مایه هشت چیز</p></div>
<div class="m2"><p>سال و ماه این هشت چیزش را همینست اکتساب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حلم او سنگ زمین و طبع او لطف هوا</p></div>
<div class="m2"><p>روی او دیدار ماه و کف او جود سحاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رسم او حسن بهار و لفظ او قدر شکر</p></div>
<div class="m2"><p>خلق او بازار مشک و خوی او بوی گلاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در دیار گوزگانان اندرین عهد قریب</p></div>
<div class="m2"><p>چار چیز نامور کرد از پی مزد و ثواب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مسجد آدینه و عالی منار میمنه</p></div>
<div class="m2"><p>سد رود شور بار و جوی آب نوسراب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از پی خوبی و از بهر صلاح مردمان</p></div>
<div class="m2"><p>کشت کرد اندر بیابان، آب راند اندر سراب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دولت و اقبال او بی حیلت و بی رنج و ذل</p></div>
<div class="m2"><p>بوستان وسبزه کرد از سوخته دشتی خراب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هشت چیزش را برابر یافتم با هشت چیز</p></div>
<div class="m2"><p>هر یکی زان هشت سوی فضل او دارد مآب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیغ او را با قضا وتیر او را با قدر</p></div>
<div class="m2"><p>دست او را با سپهر و خشت او را با شهاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حزم او را با امان و عزم او را با ظفر</p></div>
<div class="m2"><p>لفظ او را با قران و حفظ او را با کتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جان خصمش هر زمانی سوی خویش اندر کشد</p></div>
<div class="m2"><p>تیغ او را از غلاف و تیر او را از قراب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اصل رادی و بزرگی را دو چیز اندر دوچیز</p></div>
<div class="m2"><p>دست او را در عنان و پای او را در رکاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تابه فروردین زمین از لاله بر پوشد ردا</p></div>
<div class="m2"><p>تا به دی ماه آسمان از ابر بر بندد نقاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا چو شهریور درآید بازگردد عندلیب</p></div>
<div class="m2"><p>تا چو فروردین درآید پشت بنماید غراب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شادمان باد او ز ایزد بر گناه او را عفو</p></div>
<div class="m2"><p>دشمنش را بر نکوتر طاعت ایزد عقاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چارچیزش را مبادا جاودانه چار چیز</p></div>
<div class="m2"><p>این دعا نشگفت اگر گردد بساعت مستجاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مدت او را کران و لشکر او را عدد</p></div>
<div class="m2"><p>ملکت او را زوال و نعمت او را حساب</p></div></div>