---
title: >-
    شمارهٔ ۲۰۴ - در مدح محمدبن محمودبن ناصرالدین گوید
---
# شمارهٔ ۲۰۴ - در مدح محمدبن محمودبن ناصرالدین گوید

<div class="b" id="bn1"><div class="m1"><p>مرا دلیست گروگان عشق چندین جای</p></div>
<div class="m2"><p>عجب تر از دل من دل نیافریده خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم یکی و درو عاشقی گروه گروه</p></div>
<div class="m2"><p>تودرجهان چو دل من دلی دگر بنمای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شگفت و خیره فرو مانده ام که چندین عشق</p></div>
<div class="m2"><p>بیک دل اندریارب چگونه گیرد جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریصتر دلی از عاشقی ملول شود</p></div>
<div class="m2"><p>دلم همی نشود، وای از این دل من وای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نداند این دل غافل که عشق حادثه ایست</p></div>
<div class="m2"><p>که کوه آهن با رنج او ندارد پای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا میانه چندین هزار شغل اندر</p></div>
<div class="m2"><p>چگونه سازی مدح امیر بار خدای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلال دولت عالی محمد محمود</p></div>
<div class="m2"><p>امام داد گران شاه راستی فرمای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستوده ای که گرامی تر از ستایش او</p></div>
<div class="m2"><p>سخن بهم نکند خاطر ملوک ستای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن شناسی کز بیم نقد کردن او</p></div>
<div class="m2"><p>شودزبان سخنگوی، گنگ و یافه درای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بر اوو عطاهای اوهمیشه بود</p></div>
<div class="m2"><p>چو تختهای عروسان سرای مدح سرای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر ترا سخن اندر خور ستایش اوست</p></div>
<div class="m2"><p>زخسروان جهان جزبه خدمتش مگرای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر پسند کند خدمت ترا یک روز</p></div>
<div class="m2"><p>به روز جز بدر او مکن درنگ و مپای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو دل به خدمت او دادی و ترا پذیرفت</p></div>
<div class="m2"><p>زخدمت دگران دل چو آینه بزدای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی که خدمت جز اوکند همیشه بود</p></div>
<div class="m2"><p>ز بهر عاقبت خویشتن دل اندروای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>توفرخی! که ترا از جهان امید بدوست</p></div>
<div class="m2"><p>همیشه تا بتوانی زخدمتش ماسای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به عون دولت او آرزوی خویش بیاب</p></div>
<div class="m2"><p>به جاه خدمت او سربه آسمان برسای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بقای او طلب و وقت هر نماز بگوی</p></div>
<div class="m2"><p>که یا الهی !اندربقای او بفزای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایا جمال جهان را و عز دولت را</p></div>
<div class="m2"><p>چو روح در خور و همچون دو دیده اندر بای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به علم خواندن و قرآن نهاده ای دل و گوش</p></div>
<div class="m2"><p>جز از تو گوش نهاده به بانگ بربط و نای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بروز ده ره بر دولت تو حکم کنند</p></div>
<div class="m2"><p>منجمان به سطرلاب آسمان پیمای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بزرگی و شرف و دولت وسعادت و ملک</p></div>
<div class="m2"><p>همی درفشد ازین فرخجسته پرده سرای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شهان پیشین فر همای بودندی</p></div>
<div class="m2"><p>زبهر فال به هر کس کشان فتادی رای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر همای نبودی خجسته رایت تو</p></div>
<div class="m2"><p>که داندی که همایون بود به فال همای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به کبک ماند در پیش آن همای جهان</p></div>
<div class="m2"><p>تو ازمیانه درون تاز و کبک رابربای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مثال ملک چو باغیست پر شکوفه و گل</p></div>
<div class="m2"><p>تو شادمانه تماشا کنان به باغ درآی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز تاج شاهان پر کن حصار شادخ را</p></div>
<div class="m2"><p>چو شاه شرق ز گنج ملوک قلعه نای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه ولایت خالی کن از سپاه عدو</p></div>
<div class="m2"><p>چنان که شاه جهان هند را زلشکر رای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو در ولایت و دولت همی گسارمدام</p></div>
<div class="m2"><p>مخالفان را در بندو غم همی فرسای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه تا که شود روز وشب به یک میزان</p></div>
<div class="m2"><p>چو آفتاب به برج حمل بگیرد جای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو آفتاب فروزان به تخت ملک بمان</p></div>
<div class="m2"><p>چو آسمان فرا پایه در زمانه بپای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>موافقان را مهرت نبید نوش گوار</p></div>
<div class="m2"><p>مخالفان را خشم تو زهر زود گزای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سرای ملکت و در وی سرای پرده تو</p></div>
<div class="m2"><p>چو باغ پر سرو از لعبتان چین و ختای</p></div></div>