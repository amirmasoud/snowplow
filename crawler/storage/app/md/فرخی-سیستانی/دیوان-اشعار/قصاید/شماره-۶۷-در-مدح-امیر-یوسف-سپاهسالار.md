---
title: >-
    شمارهٔ ۶۷ - در مدح امیر یوسف سپاهسالار
---
# شمارهٔ ۶۷ - در مدح امیر یوسف سپاهسالار

<div class="b" id="bn1"><div class="m1"><p>ای پسر! جنگ بنه، بوسه بیار</p></div>
<div class="m2"><p>این همه جنگ و درشتی به چه کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنگ یکسو نه و دلشاد بزی</p></div>
<div class="m2"><p>خویشتن را و مرا رنجه مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو روزی سخنی پیش مگیر</p></div>
<div class="m2"><p>هر زمان تازه خویی پیش میار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نگارا ز جفا سیر شود</p></div>
<div class="m2"><p>بس عزیزا که ازین گرددخوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه من ای دوست ترا دیدم و بس</p></div>
<div class="m2"><p>من ببند آمده ام چندین بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو من ای دوست ترا دارم دوست</p></div>
<div class="m2"><p>تو حق دوستی من بگزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار کی یافته ای در خور خویش</p></div>
<div class="m2"><p>جهد آن کن که نکو داری یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو چو من یار نیابی بجهان</p></div>
<div class="m2"><p>من چو تو یابم هر روز هزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من اگر خواهم از بخشش میر</p></div>
<div class="m2"><p>کودکانی خرمی همچو نگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میر یوسف پسر ناصر دین</p></div>
<div class="m2"><p>لشکر آرای شه شیر شکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن نکو طلعت و فرخنده امیر</p></div>
<div class="m2"><p>آن بآیین و پسندیده سوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن سر افراز و گرانمایه هنر</p></div>
<div class="m2"><p>آن گرانمایه پر مایه تبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جنگها کرده فراوان و بجنگ</p></div>
<div class="m2"><p>از بد اندیش بر آورده دمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرد جنگست چو پیش آید جنگ</p></div>
<div class="m2"><p>مردکارست چو پیش آید کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز جنگ و شغب از شادی جنگ</p></div>
<div class="m2"><p>بر فروزد دو رخان چون گلنار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بچنین روز بگوشش غو کوس</p></div>
<div class="m2"><p>زارغنون خوشترو از موسیقار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه دم جنگست اندیشه او</p></div>
<div class="m2"><p>گر چه خفته ست و گر چه بیدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبرد حمله بهنگام نبرد</p></div>
<div class="m2"><p>جز بر آنسو که مبارز بسیار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر مبارز که برو روی نهاد</p></div>
<div class="m2"><p>خورد بر جان گرامی زنهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیغش از کوهی دو کوه کند</p></div>
<div class="m2"><p>چون خدنگش ز چناری دو چنار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هیچ تیری نزد اوبر تن خصم</p></div>
<div class="m2"><p>که نه از پشت برون شد سوفار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیراو گر چه سبک سنگ بود</p></div>
<div class="m2"><p>کنگره بفکنداز برج حصار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غیر محمود که داند کردن</p></div>
<div class="m2"><p>نره شیری بخدنگی اشکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگسلاند سر شیر از تن شیر</p></div>
<div class="m2"><p>هم بدانسان که کسی میوه زدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لشکری را که چنو پشت بود</p></div>
<div class="m2"><p>از همه خلق نباشد تیمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در جوانمردی جاییست که نیست</p></div>
<div class="m2"><p>وهم را از بر او جای گذار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هیچ شب نیست که از مجلس او</p></div>
<div class="m2"><p>نبرد زایر او زر بکنار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از پس سلطان امروز جز او</p></div>
<div class="m2"><p>که دهد بخشش پانصد دینار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لاجرم بر در او چون ملکان</p></div>
<div class="m2"><p>چاکرانند بملک و به یسار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شادمان باد و بهمت برساد</p></div>
<div class="m2"><p>آن نکو عادت نیکو کردار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از دل شاه جهان نیرومند</p></div>
<div class="m2"><p>وز تن وجان بجهان بر خوردار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لهو رابا دل او باد سکون</p></div>
<div class="m2"><p>بخت را بر در او باد قرار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا بر آیین بزرگان عجم</p></div>
<div class="m2"><p>بزم سازد بخزان وببهار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همچنین مهر بشادی و طرب</p></div>
<div class="m2"><p>بگذارد صد دیگر بشمار</p></div></div>