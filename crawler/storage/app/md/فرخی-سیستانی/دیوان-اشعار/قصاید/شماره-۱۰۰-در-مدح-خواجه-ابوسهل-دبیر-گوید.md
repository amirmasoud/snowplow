---
title: >-
    شمارهٔ ۱۰۰ - در مدح خواجه ابوسهل دبیر گوید
---
# شمارهٔ ۱۰۰ - در مدح خواجه ابوسهل دبیر گوید

<div class="b" id="bn1"><div class="m1"><p>کوس فرو کوفت ماه روزه بیکبار</p></div>
<div class="m2"><p>روزه نهان کرد لشکر از پس دیوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بط خاموش بوده گشت سخنگوی</p></div>
<div class="m2"><p>محتسب سرد سیر گشت ز گفتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده ز پنهان نهاد روی بمجلس</p></div>
<div class="m2"><p>خیز و بکار آی و کار مجلس بگزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه ز بیگانگان خام تهی کن</p></div>
<div class="m2"><p>باده رنگین بیار و بر بط بردار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست کن امروز مرمرا و میندیش</p></div>
<div class="m2"><p>تاکی هشیار چند باشم هشیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاکم شرعی که می نگیرم هرگز</p></div>
<div class="m2"><p>زاهد عصرم که روزه دارم هموار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهدی و حاکمی بمن نرسیده ست</p></div>
<div class="m2"><p>ور برسد کار پیش گیرم ناچار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز و شب خویش را کنم به دو قسمت</p></div>
<div class="m2"><p>هر دو بیکجای راست دارم چون تار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرمک نرمک همی کشم همه شب می</p></div>
<div class="m2"><p>روز به صد رنج ودرد دارم دستار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آیم و چون کخ به گوشه ای بنشینم</p></div>
<div class="m2"><p>پوست بیک بار بر کشم ز ستغفار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راست چو شب گاو گون شودبگریزم</p></div>
<div class="m2"><p>گویم تا در نگه کنند به مسمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آروزی خویش را بخوانم و گویم</p></div>
<div class="m2"><p>شب همه بگذشت خیز وداروی خواب آر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون سرم از مستی و ز خواب گران گشت</p></div>
<div class="m2"><p>در کشم او را به جامه شب و افشار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرخی آخر نفایه گفتی و دانی</p></div>
<div class="m2"><p>این چه سخن بودپیش خواجه بیکبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواجه سید وکیل سلطان بوسهل</p></div>
<div class="m2"><p>آنکه بدو سهل گشت کار بر احرار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بارخدای بزرگوار که او بود</p></div>
<div class="m2"><p>فضل و ادب را بطوع و طبع خریدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اهل ادب را به خانه برد و وطن داد</p></div>
<div class="m2"><p>علم و ادب را فزودقیمت و مقدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواسته خویش پیش خلق فدا کرد</p></div>
<div class="m2"><p>خصلت نیکوی خویش کرد پدیدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برهمه گیتی در سرای گشاده ست</p></div>
<div class="m2"><p>پیش همه خلق باز رفته بکردار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خلق ز هر سو نهاده روی سوی او</p></div>
<div class="m2"><p>راه ز انبوه گشته چون ره بازار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که در آید همی ستاند بی منع</p></div>
<div class="m2"><p>هرکه بخواهد همی درآید بی بار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر چه فراوان دهد دلش بنگیرد</p></div>
<div class="m2"><p>مانده نگردد ز مال دادن بسیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امروز آیی مطیع تر بود از دی</p></div>
<div class="m2"><p>امسال آیی گشاده تر بود از پار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بار نهد بر دل از همه کس و هر گز</p></div>
<div class="m2"><p>بر دل دشمن به ذره یی ننهد بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اینت کریمی بزرگوار که تا بود</p></div>
<div class="m2"><p>هیچکسی زو دژم نبود ودل آزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خستن دل را بخاصه مرد جوانرا</p></div>
<div class="m2"><p>ایزد داند که هول باشد و دشوار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آری هر کس که نام جوید بی شک</p></div>
<div class="m2"><p>با دل و با نفس کرد باید پیکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لاجرم از هر کسی که پرسی گوید</p></div>
<div class="m2"><p>خواجه بهر نیک در خورست و سزاوار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روزش همواره نیک باد و بهرنیک</p></div>
<div class="m2"><p>دسترسش باد تا همی بودش کار</p></div></div>