---
title: >-
    شمارهٔ ۷۸ - در تهنیت عید فطر و مدح خواجه جلیل عبدالرزاق بن احمد بن حسن میمندی گوید
---
# شمارهٔ ۷۸ - در تهنیت عید فطر و مدح خواجه جلیل عبدالرزاق بن احمد بن حسن میمندی گوید

<div class="b" id="bn1"><div class="m1"><p>حدیث نوشدن مه شنیده ای به خبر</p></div>
<div class="m2"><p>بکاخ در شو و ماه و ستارگان باز نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ز نو شدن مه غرض مبارکی است</p></div>
<div class="m2"><p>چو ماه بینی بشتاب و روزگار مبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان شتاب که من خواهم ار ندانی تاخت</p></div>
<div class="m2"><p>میان تاختن آوازه ده که با ده بخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نصیب روزه نگه داشتم دگر چکنم</p></div>
<div class="m2"><p>فکند خواهم چو دیگران بر آب سپر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهی گذشت که بر دست من نیامد می</p></div>
<div class="m2"><p>چگونه باشم ازین پارساتر و بهتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم ز روزه بپوسید وهم ز توبه گرفت</p></div>
<div class="m2"><p>چنین همی نتوان برد روزگار بسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چنگ روزه بزنهار عید خواهم رفت</p></div>
<div class="m2"><p>بر او بنالم و گویم مرا ز روزه بخر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر تو خود نخری خواجه را کنم آگاه</p></div>
<div class="m2"><p>که این معامله را او کند ز تو بهتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیث آنکه من از روزه چون غمی شده ام</p></div>
<div class="m2"><p>بگوش خواجه رسد بر زبان عید مگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جلیل خواجه آفاق احمد آنکه بود</p></div>
<div class="m2"><p>بزرگوار به فضل و به دانش و به هنر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزرگوار جهان خواجه بلند نسب</p></div>
<div class="m2"><p>خنک روان پدر زین حلال زاده پسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چه گوهرش از گوهر شریف وی است</p></div>
<div class="m2"><p>چنین شریف نبود اندرین شریف گهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز جاه و حشمت او در تبار و گوهر او</p></div>
<div class="m2"><p>همی فزاید جاه و جمال وقدر و خطر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فضایل و هنر ذات او بحیله و جهد</p></div>
<div class="m2"><p>شماره کرد نداند همی ستاره شمر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر از کفایت گویند با کفایت او</p></div>
<div class="m2"><p>همه کفایت صاحب شود هبا و هدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور از مروت گویند با مروت او</p></div>
<div class="m2"><p>همه مروت آل برامکه ست ابتر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سخای او را روز عطا وفا نکند</p></div>
<div class="m2"><p>سرشک ابر و نبات زمین و برگ شجر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در سرای گشاده ست بر وضیع و شریف</p></div>
<div class="m2"><p>نهاده روی جهانی بدان مبارک در</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرا و مجلس پر مردم و دو رویه بپای</p></div>
<div class="m2"><p>غلام و چاکر هر یک بخدمت اندر خور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی برون نشود تادرون نیاید ده</p></div>
<div class="m2"><p>چنین سرای که بیند بدین جهان اندر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وگر زمانی خالی شود ز خلق، سرای</p></div>
<div class="m2"><p>بجستجوی فرستد بهر سویی چاکر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزرگوار دلا کو چنین تواند کرد</p></div>
<div class="m2"><p>نبود هیچ دل اندر جهان بدین گوهر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل پدر ز پسرگاه گاه سیر شود</p></div>
<div class="m2"><p>دلش همی نشود سیر از ربیع و مضر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزرگ نامی جوید همی و نام بزرگ</p></div>
<div class="m2"><p>نهاده نیست بکوی و فکنده نیست بدر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بفضل و خوی پسندیده جست باید نام</p></div>
<div class="m2"><p>دگر بدامن مال و به بذل کردن زر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آنچه باید ازین باب کردو خواهد کرد</p></div>
<div class="m2"><p>چو تخم نیک فکنده ست نیک یابد بر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه بیهده سخنش در میان خلق افتاد</p></div>
<div class="m2"><p>نه خیر خیر ثنا گوی او شد آن لشکر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرا جز او را آواز نام نیک نخاست</p></div>
<div class="m2"><p>ازین سران و بزرگان که حاضرند ایدر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر چنو دگرستی بمردی و بفضل</p></div>
<div class="m2"><p>چنو شدستی معروف و گستریده اثر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بقاش بادو بکام و مراد دل برساد</p></div>
<div class="m2"><p>مباد خانه او خالی از سعادت و فر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه یافته از دوستان خویش مراد</p></div>
<div class="m2"><p>همیشه یافته بر دشمنان خویش ظفر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خزان و آمدن عید و رفتن رمضان</p></div>
<div class="m2"><p>خجسته باد برآن میر فر خجسته اثر</p></div></div>