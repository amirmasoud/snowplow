---
title: >-
    شمارهٔ ۲۱۰ - در مدح سلطان مسعود بن سلطان محمود گوید
---
# شمارهٔ ۲۱۰ - در مدح سلطان مسعود بن سلطان محمود گوید

<div class="b" id="bn1"><div class="m1"><p>خوشا عاشقی خاصه وقت جوانی</p></div>
<div class="m2"><p>خوشا با پریچهرگان زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا با رفیقان یکدل نشستن</p></div>
<div class="m2"><p>بهم نوش کردن می ارغوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به وقت جوانی بکن عیش زیرا</p></div>
<div class="m2"><p>که هنگام پیری بود ناتوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوانی و از عشق پرهیز کردن</p></div>
<div class="m2"><p>چه باشد ندانی، به جز جان گرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوانی که پیوسته عاشق نباشد</p></div>
<div class="m2"><p>دریغست ازو روزگار جوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شادمانی بود عشق خوبان</p></div>
<div class="m2"><p>بباید گشادن در شادمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در شادمانی گشاده ست بر تو</p></div>
<div class="m2"><p>که مدحتگر پادشاه جهانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهاندار مسعود محمود غازی</p></div>
<div class="m2"><p>که مسعود باد اخترش جاودانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر خسروان افسر تاجداران</p></div>
<div class="m2"><p>که اورا سزد تاج و تخت کیانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمین را مهیا به مالک رقابی</p></div>
<div class="m2"><p>فلک را مسمی به صاحبقرانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به مردانگی از همه شهریاران</p></div>
<div class="m2"><p>پدیدار همچون یقین از گمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به جنگ اندرون کامرانست لیکن</p></div>
<div class="m2"><p>ندانم کجا راند این کامرانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبینی دل جنگ اوهیچ کس را</p></div>
<div class="m2"><p>تو بنمای گر هیچ دیدی و دانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن سومر او راست تاغرب شاهی</p></div>
<div class="m2"><p>وز این سومر اوراست تا شرق خانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپاهیست او را که از دخل گیتی</p></div>
<div class="m2"><p>به سختی توان دادشان بیستگانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر نیستی کوه غزنین توانگر</p></div>
<div class="m2"><p>بدین سیم روینده و زر کانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به اندازه لشکر او نبودی</p></div>
<div class="m2"><p>گر از خاک و از گل زدندی شیانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خداوند چشم بدان دور دارد</p></div>
<div class="m2"><p>از این شاه و زنی دولت آسمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین شهریار و چنین شاهزاده</p></div>
<div class="m2"><p>که دیدو که داده ست هرگز نشانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدین شرمناکی بدین خوب رسمی</p></div>
<div class="m2"><p>بدین تازه رویی بدین خوش زبانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حدیث ار کند با تواز شرم گردد</p></div>
<div class="m2"><p>دورخسار او چون گل بوستانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه هرگز بدان را به بد داده یاری</p></div>
<div class="m2"><p>نه هرگز به بد کرده همداستانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان رابه عدل و به انصاف دادن</p></div>
<div class="m2"><p>بیاراست چون شعر نیک از معانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به جوی اندرون آب نوش روان شد</p></div>
<div class="m2"><p>ازین عدل و انصاف نوشیروانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان گشت بازارهای ولایت</p></div>
<div class="m2"><p>که برخاست از پاسبان پاسبانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سپاه و رعیت نیابند فرصت</p></div>
<div class="m2"><p>به شغل دگر کردن ازمیزبانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز پاکیزگی شهر و از ایمنی ده</p></div>
<div class="m2"><p>روان گشت بازار بازارگانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زهی شهریاری که گویی ز ایزد</p></div>
<div class="m2"><p>به رزق همه عالم اندر ضمانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به کردار نیکو و گفتار شیرین</p></div>
<div class="m2"><p>همی آرزوها به دلها رسانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل من پر از آرزو بودشاها</p></div>
<div class="m2"><p>وز اندیشه رخسار من زعفرانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه زان کاندرین خدمت این رنج بردم</p></div>
<div class="m2"><p>که واجب کند بر من این مهربانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا شاد کردی و آبادکردی</p></div>
<div class="m2"><p>سرای من از فرش و مال و اوانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بیاراستم خانه از نعمت تو</p></div>
<div class="m2"><p>به کاکویی و رومی خسروانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خدایت معین بادو دولت مساعد</p></div>
<div class="m2"><p>توباقی و بدخواه تو گشته فانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سرای تو پر سرو و پر ماه و پر گل</p></div>
<div class="m2"><p>ز یغمایی و چینی و خلخانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همایون وفرخنده بادت نشستن</p></div>
<div class="m2"><p>بدین جشن فرخنده مهرگانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به تو بگذرد روزگاران به خوشی</p></div>
<div class="m2"><p>دوصدجشن دیگر چنین بگذرانی</p></div></div>