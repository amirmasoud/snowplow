---
title: >-
    شمارهٔ ۲۰۵ - در مدح امیر یوسف بن ناصر الدین
---
# شمارهٔ ۲۰۵ - در مدح امیر یوسف بن ناصر الدین

<div class="b" id="bn1"><div class="m1"><p>دوش همه شب همی گریست به زاری</p></div>
<div class="m2"><p>ماه من آن ترک خوبروی حصاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد و بناگوش سایبانش همی کرد</p></div>
<div class="m2"><p>یک ز دگر حلقه های زلف بخاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس کآب دو چشم او بهم آمد</p></div>
<div class="m2"><p>قیمت عود سیه گرفت سماری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرمک نرمک مرا به شرم همی گفت :</p></div>
<div class="m2"><p>با بنه می‌رقصد رفتن داری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم: دارم، که امر میر چنینست</p></div>
<div class="m2"><p>گفت: به غزنین مرا همی بگذاری؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو مرا دست بازداری بی تو</p></div>
<div class="m2"><p>زیر نباشد چو من به زردی و زاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میر نگفته ست مرترا که: روا نیست</p></div>
<div class="m2"><p>کآرزوی خویش را به راه بیاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بتوانی ببر مرا گه رفتن</p></div>
<div class="m2"><p>تا نشود روز من ز هجر تو تاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به ره انده گسار باتو نباشد</p></div>
<div class="m2"><p>انده و تیمار خویش با که گساری ؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم: کانده گسار من به ره اندر</p></div>
<div class="m2"><p>خدمت میرست گفت: محکم کاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پشت سپه میر یوسف آنکه ستوده ست</p></div>
<div class="m2"><p>نزد سواران همه به نیک سواری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه ز باران جود او چو بخیلان</p></div>
<div class="m2"><p>وقت بهاران خجل شد ابر بهاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای درم از دست تو رسیده به پستی</p></div>
<div class="m2"><p>زر ز بخشیدنت فتاده به خواری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روز عطا هرکفی از آن توابریست</p></div>
<div class="m2"><p>پس تو شب و روز در میان بخاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بحرت خوانم همی و ابرت خوانم</p></div>
<div class="m2"><p>نه ز پی آن که دود روی بحاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلکه بدان خوانمت که تو به دل و دست</p></div>
<div class="m2"><p>گوهر بپراکنی و لؤلؤباری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخشش پیوسته را شمار نگیری</p></div>
<div class="m2"><p>خدمت خدمتگران همی بشماری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نامزد ز ایران کنی گه کشتن</p></div>
<div class="m2"><p>گر به مثل گلبنی به باغ بکاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بند گشای خزانه تو چه کرده ست</p></div>
<div class="m2"><p>کو را هزمان به دست جود سپاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جود هلاک خزانه باشد و هر روز</p></div>
<div class="m2"><p>تازه هلاکی تو بر خزانه گماری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>معدن علمی چنان که مکمن فضلی</p></div>
<div class="m2"><p>مایه حلمی چنان که اصل وقاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جم سیر و سام رزم و دارا بزمی</p></div>
<div class="m2"><p>رستم کرداری و فریدون کاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرچه تبار تو خسروان جهانند</p></div>
<div class="m2"><p>توبه همه روی سرفراز و تباری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا تو به رزمی چو زهر زود گزایی</p></div>
<div class="m2"><p>تا تو به بزمی چو شهد نوش گواری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیش تن دوستان ز رنج پناهی</p></div>
<div class="m2"><p>در جگر دشمنان فروخته ناری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حلق بداندیش رابرنده چو تیغی</p></div>
<div class="m2"><p>دیده بدخواه را خلنده چو خاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روز و شب از آرزوی جنگ و شبیخون</p></div>
<div class="m2"><p>جز سخن جنگ بر زبان نگذاری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیل قوی تن زیشک یاری خواهد</p></div>
<div class="m2"><p>توزدو بازوی خویش خواهی یاری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خون ز دل سنگ خاره بردمد ار تو</p></div>
<div class="m2"><p>صورت تیرو کمان بر او بنگاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گاو ز ماهی فرو جهد گه رزمت</p></div>
<div class="m2"><p>گر تو زمین را زنوک نیزه بخاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باد خزانی ز ابر پیلان کرده ست</p></div>
<div class="m2"><p>از پی آن تا ترا کشند عماری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا نکند موم فعل عنبر هندی</p></div>
<div class="m2"><p>تا ندهد بید بوی عود قماری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شاد زی ای رایت تو مایه دولت</p></div>
<div class="m2"><p>شاد زی ای خدمت تو طاعت باری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تابه قوی بخت توو دولت سلطان</p></div>
<div class="m2"><p>امر تواندر زمانه گرددجاری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قصر تو باشد بلاد بصره و بغداد</p></div>
<div class="m2"><p>باغ تو باشد زمین آمل و ساری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز که ری در نهاله گاه تو رانند</p></div>
<div class="m2"><p>روز شکار تو صد هزار شکاری</p></div></div>