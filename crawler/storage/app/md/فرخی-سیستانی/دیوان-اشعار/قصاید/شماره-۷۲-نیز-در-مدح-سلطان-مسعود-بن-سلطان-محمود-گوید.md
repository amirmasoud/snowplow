---
title: >-
    شمارهٔ ۷۲ - نیز در مدح سلطان مسعود بن سلطان محمود گوید
---
# شمارهٔ ۷۲ - نیز در مدح سلطان مسعود بن سلطان محمود گوید

<div class="b" id="bn1"><div class="m1"><p>ماه دو هفته من برد مه روزه بسر</p></div>
<div class="m2"><p>بامداد آمد و از عید مرا داد خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردمان دوش خبر یافته بودند ز عید</p></div>
<div class="m2"><p>که گمان برد که من غافلم از عید مگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او مگر تهنیت عید همی خواست بدین</p></div>
<div class="m2"><p>هیچ شک نیست همین خواست بدین آن دلبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ازین شادی برجستم ودو چنگ زدم</p></div>
<div class="m2"><p>اندر آن زلف که با مشک زند بویش بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر زبان داشت زمه آن مه دو هفته سخن</p></div>
<div class="m2"><p>از لب او لب من یافت بخروار شکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه یک مهه گرد آمده بودم بر دوست</p></div>
<div class="m2"><p>نیمه ای داد و همی خواهم یک نیم دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیم دیگر بتفاریق همی خواهم خواست</p></div>
<div class="m2"><p>تا شمارم نشود یکسره با دوست بسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جه حدیثست، من این بوسه شماری بنهم</p></div>
<div class="m2"><p>بشود عیش چو معشوق شود بوسه شمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشقان بوسه شمرده به مه روزه دهند</p></div>
<div class="m2"><p>زانکه وقتش ز گه شام بود تا بسحر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درمه شوال این تنگی و تاریکی نیست</p></div>
<div class="m2"><p>تو بچشم دگر اندر مه شوال نگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خطر روزه بزرگست و مه روزه شریف</p></div>
<div class="m2"><p>از مه روزه گشاده ست به خلد اندر در</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیکن این ماه که پیش آمده ماهیست که او</p></div>
<div class="m2"><p>با طرب گردد و بارامش و با رامشگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای رفیقان سخنی راست بگویم شنوید</p></div>
<div class="m2"><p>طبع من باری با شوال آمیخته تر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نه ماه طربست این ز چه غرید همی</p></div>
<div class="m2"><p>دوش هر پاسی کوس ملک شیر شکر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسرو مشرق و مغرب ملک روی زمین</p></div>
<div class="m2"><p>شاه مسعود مبارک پی مسعود اختر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه تادست به تیر و بکمان برد ببرد</p></div>
<div class="m2"><p>آب سام یل و قدر و خطر رستم زر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زخم تیر ملکان دید و ندید آن ملک</p></div>
<div class="m2"><p>آنکه او از قبل تیر همی ساخت سپر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر ملک تیر و کمان درخور بازو کندی</p></div>
<div class="m2"><p>بر سر که بردی ترکش او ترکش گر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از برو بازوی او چشم همی خیره شود</p></div>
<div class="m2"><p>چشم بد دور کناد ایزد از آن بازو و بر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جنگجو هست و لیکن بجهان نیست کسی</p></div>
<div class="m2"><p>که بجنگش بتواند بست امروز کمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>او همیگوید من تیغ زنم رنج کشم</p></div>
<div class="m2"><p>تا بزرگی بهنر گیرم و کیتی بهنر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ایزد از عرش همیگوید تو رنج مکش</p></div>
<div class="m2"><p>کاینجهان جمله ترا دادم، بنشین و بخور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنچه میران مبارز نگرفتند بگیر</p></div>
<div class="m2"><p>آنچه شاهان مظفر نخریدند بخر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مهر از آنکس که بمهر تو گرو نیست ببر</p></div>
<div class="m2"><p>دولت از خانه آنکس که ترا نیست ببر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بتن آسانی بر بالش دولت بنشین</p></div>
<div class="m2"><p>چه کنی تاختن و تافتن رنج سفر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بندگان دادم اندر خور تو کار ترا</p></div>
<div class="m2"><p>که بکام تو از ایشان همه خیر آید و شر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کار در گردن ایشان کن تا من بکنم</p></div>
<div class="m2"><p>نا رسانیده بیک بنده تو هیچ ضرر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همچنین کرد وبهر گوشه فرستاد یکی</p></div>
<div class="m2"><p>با سپاهی که مر آن را نه قیاسست و نه مر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هیچ لشکر نفرستاد براهی که ز راه</p></div>
<div class="m2"><p>بر او باز نیامد خبر فتح و ظفر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اندر این مدت یکسال در اقصای جهان</p></div>
<div class="m2"><p>همچو دریای دمان کرد بگیتی لشکر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از لب جیحون تا دجله ز بسیار سپاه</p></div>
<div class="m2"><p>چون ره مورچگانست همه راهگذر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر زمان مژده بر آید که فلان بنده او</p></div>
<div class="m2"><p>بفلان شهر فلان قلعه بکند از بن و بر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>موکب وخیل فلان میر پراکند ز هم</p></div>
<div class="m2"><p>آلت و ساز فلان شاه، فرستاد ایدر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مژده آن مژده بود کزپس این خواهد خواست</p></div>
<div class="m2"><p>باش تا مغز سر جمله کند زیر و زبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بندگانند ملک را که چنین کار کنند</p></div>
<div class="m2"><p>با دل و دولت او کار چنین راچه خطر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کار فرمای همی داند فرمودن کار</p></div>
<div class="m2"><p>لاجرم کارگر از کار همی یابد بر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حشمت و سایه او لشکر او را مددست</p></div>
<div class="m2"><p>که نبرد ز پی لشکر او تا محشر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>لشکری راکه بود سایه مسعود مدد</p></div>
<div class="m2"><p>پیش ایشان زهوا مرغ فرو ریزد پر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دایم این حشمت و این سایه همی بادبجای</p></div>
<div class="m2"><p>وندر این خانه همی بادا این دولت و فر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای بمردی و کف راد و مروت چو علی</p></div>
<div class="m2"><p>وی به انصاف و دل پاک و عدالت چو عمر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از خداوند نظر چشم همیداشت جهان</p></div>
<div class="m2"><p>بجهانداری نیکو نیت و خوب سیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون خداوند جهانداری و شاهی بتو داد</p></div>
<div class="m2"><p>گفت من یافتم اینک زخداوند نظر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تهنیت باد جهان را بجهانداری تو</p></div>
<div class="m2"><p>بر خور ای شه بمراد دل و از او برخور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا جهانست جهاندار تو بادی و مباد</p></div>
<div class="m2"><p>در جهانداری و در دولت تو هیچ غیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سال و ماه تو و ایام تو چون نام تو باد</p></div>
<div class="m2"><p>عادت و عاقبت کار تو چون نام پدر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>روز عید رمضانست و سر سال نوست</p></div>
<div class="m2"><p>هر دو فرخنده کناد ای ملک ایزد بتو بر</p></div></div>