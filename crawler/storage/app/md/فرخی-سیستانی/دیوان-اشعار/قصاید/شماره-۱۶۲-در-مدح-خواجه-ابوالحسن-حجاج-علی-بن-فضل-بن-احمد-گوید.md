---
title: >-
    شمارهٔ ۱۶۲ - در مدح خواجه ابوالحسن حجاج، علی بن فضل بن احمد گوید
---
# شمارهٔ ۱۶۲ - در مدح خواجه ابوالحسن حجاج، علی بن فضل بن احمد گوید

<div class="b" id="bn1"><div class="m1"><p>بت من آن به دو رخ چون شکفته لاله ستان</p></div>
<div class="m2"><p>چو دید روی مرا روی خویش کرد نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آینه که بهار اندرون شود به حجاب</p></div>
<div class="m2"><p>در آن زمان که برون آید از حجاب خزان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو روی خویش بپوشید روز من بشکست</p></div>
<div class="m2"><p>نبود جای شگفت و شگفتم آمد از آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آینه که چو خورشید ناپدید شود</p></div>
<div class="m2"><p>سیاه و تیره شود گرچه روشنست جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا بدید و به مژگان فرو کشید ابرو</p></div>
<div class="m2"><p>ز بیم در تن من زلزله گرفت روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرآینه که بترسد کسی چو دشمن او</p></div>
<div class="m2"><p>برابر دل او تیر برنهد به کمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سه بوسه زو بخریدم دلی بدو دادم</p></div>
<div class="m2"><p>نداد بوسه و بر من گرفت روی گران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرآینه چو زیان کرد بر خریده نو</p></div>
<div class="m2"><p>ز من بپوشد کایدون ستوده نیست زیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا ببیند معشوق من بخندد خوش</p></div>
<div class="m2"><p>چو او بخندد بر من فتد خروش و فغان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آینه که چو دل خستگان بنالد رعد</p></div>
<div class="m2"><p>چو برق باز کند پیش او به خنده دهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زلف با دل من چند گاه بازی کرد</p></div>
<div class="m2"><p>دلم بخست و جراحت گرفت و ماند نشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آینه که نشان گیرد از جراحت گوی</p></div>
<div class="m2"><p>چوبی محابا هر سو همی خورد چوگان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلم بخست و لیکن کنون همی ترسد</p></div>
<div class="m2"><p>ز خشم خواجه فاضل ستوده سلطان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آینه که بترسد ز خشم خواجه که او</p></div>
<div class="m2"><p>به زلف گنج مدیحش همی کند پنهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابوالحسن علی فضل احمد آنکه چوکف</p></div>
<div class="m2"><p>به که نماید همواره کوه گردد کان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آینه که ز دیدار آفتاب شود</p></div>
<div class="m2"><p>به کوه سنگ عقیق و به دشت گل عقیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهاد خوب وره مردمی ازو گیرند</p></div>
<div class="m2"><p>ستودگان و بزرگان تازی و دهقان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر آینه که ز خورشید ماه گیرد نور</p></div>
<div class="m2"><p>چنانکه میوه زمه رنگ و گونه الوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر چه کامل و کافی کسیست، چون براو</p></div>
<div class="m2"><p>فرو نشست پدید آید اندر و نقصان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر آینه چوستاره به آفتاب رسید</p></div>
<div class="m2"><p>چنان نماید کاندر میانه اقران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چهار حد بساط از فروغ طلعت او</p></div>
<div class="m2"><p>ز نور طور تولی شناختن نتوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر آینه که همی روشنی به چشم آید</p></div>
<div class="m2"><p>کجا فروخته شمعی بود زبانه زنان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو نهادند از رکنهای عالم روی</p></div>
<div class="m2"><p>گزیدگان زمین و ستودگان جهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صفی که خواجه بدورونهاد روز نبرد</p></div>
<div class="m2"><p>تهی شود ز سوار و پیاده هم بزمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر آینه شود از رنگ مرغزار تهی</p></div>
<div class="m2"><p>چو روی کرد سوی مرغزار شیر ژیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سخنوران و ستایشگران گیتی را</p></div>
<div class="m2"><p>همی نگردد جزبر مدیح خواجه زبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر آینه نستاید زمین شوره کسی</p></div>
<div class="m2"><p>که پر شکوفه وگل باغ بیند وبستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخن چو تن بود اندر ستایش همه کس</p></div>
<div class="m2"><p>چو در ستایش او راه یافت گشت چو جان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر آینه که سخن در ستایش مردم</p></div>
<div class="m2"><p>چنان نیاید کاندر ستایش رحمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فزونتر از همه کس دارد آلت هستی</p></div>
<div class="m2"><p>ز بخشش کف او مدحگوی مدحت خوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه باد و بدو شاد باد خلق که او</p></div>
<div class="m2"><p>به جود روزی خلق از خدای کرده ضمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر آینه چو دعا در صلاح خلق بود</p></div>
<div class="m2"><p>اجابتش را امید باشد از یزدان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خجسته باد بر او مهرگان ودست مباد</p></div>
<div class="m2"><p>زمانه را و جهان را بر او و بر سلطان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر آینه نبود دست خاک را بر باد</p></div>
<div class="m2"><p>چنانکه آتش سوزنده رابر آب روان</p></div></div>