---
title: >-
    شمارهٔ ۱۱۵ - در مدح سلطان محمد بن محمود غزنوی
---
# شمارهٔ ۱۱۵ - در مدح سلطان محمد بن محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>دوش تا اول سپیده بام</p></div>
<div class="m2"><p>می همی خورد می به رطل و به جام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سماعی که از حلاوت بود</p></div>
<div class="m2"><p>مرغ را پایدام ودل را دام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با بتانی که می ندانم گفت</p></div>
<div class="m2"><p>که از ایشان هوای من به کدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه با جعدهای مشکین بوی</p></div>
<div class="m2"><p>همه با زلفهای غالیه فام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرهی را نشانده بودم پیش</p></div>
<div class="m2"><p>برنهاده به دست جام مدام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرهی رابپای تا همه شب</p></div>
<div class="m2"><p>کارمی را همی دهنده نظام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ایستاده به رشک سرو سهی</p></div>
<div class="m2"><p>وز نشسته به درد ماه تمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حال ازینگونه بود در همه شب</p></div>
<div class="m2"><p>زین کس آگه نبود، تا گه بام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون چنین بودپس چرا گفتم</p></div>
<div class="m2"><p>قصه خویش پیش شاه انام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاه گیتی محمد محمود</p></div>
<div class="m2"><p>زینت ملک ومفخر ایام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه دولت بدو گرفت قرار</p></div>
<div class="m2"><p>آنکه گیتی بدو گرفت قوام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دولت او را به ملک داده نوید</p></div>
<div class="m2"><p>وآمده تازه روی و خوش بخرام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه امیدها بدوست قوی</p></div>
<div class="m2"><p>خاصه امید آنکه جوید نام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میر ما را خوییست، چون خوی که ؟</p></div>
<div class="m2"><p>چون خوی مصطفی علیه سلام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در عطا دادن و سخاست مقیم</p></div>
<div class="m2"><p>در کریمی و مردمیست مدام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از بخیلی چنان کند پرهیز</p></div>
<div class="m2"><p>که خردمند پارسا ز حرام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بود ممکن و تواند کرد</p></div>
<div class="m2"><p>نکند جز به کار خیر قیام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سالی از خویشتن خجل باشد</p></div>
<div class="m2"><p>گر کسی را به حق دهد دشنام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خشم ز انسان فرو خوردکه خورد</p></div>
<div class="m2"><p>مردم گرسنه شراب و طعام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر مثل خصم را بیازارد</p></div>
<div class="m2"><p>خویشتن را خجل کندبه ملام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عاشق مردمی و نیکخوییست</p></div>
<div class="m2"><p>دشمن فعل زشت وخوی لئام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تازه رویی و راد مردی وشرم</p></div>
<div class="m2"><p>باز یابی ازو بهر هنگام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر تکلف کندکه این نکند</p></div>
<div class="m2"><p>باز ازین راه بر گذارد گام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر کجا گرم گشت، با خوی او</p></div>
<div class="m2"><p>راد مردی برون دمد ز مسام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هیچ مرد تمام وپخته نگفت</p></div>
<div class="m2"><p>که ازو هیچ کاری آمد خام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لاجرم هر چه در جهان فراخ</p></div>
<div class="m2"><p>شیر مردست و رادمرد تمام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه چون من فدای میر منند</p></div>
<div class="m2"><p>همه از بهر او زنند حسام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جاودان شاد بادو در همه وقت</p></div>
<div class="m2"><p>ناصرش ذوالجلال و الاکرام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کاخ او پر بتان آهو چشم</p></div>
<div class="m2"><p>باغ او پر بتان کبک خرام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در همه شغلها که دست برد</p></div>
<div class="m2"><p>نیکش آغاز و نیکتر انجام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عید قربان بر او مبارک باد</p></div>
<div class="m2"><p>هم بر آنسان که بودعید صیام</p></div></div>