---
title: >-
    شمارهٔ ۱۷۰ - در مدح ابو منصور دواتی قراتکین حاکم غرجستان
---
# شمارهٔ ۱۷۰ - در مدح ابو منصور دواتی قراتکین حاکم غرجستان

<div class="b" id="bn1"><div class="m1"><p>مرا دلیست که از چشم بد رسیده به جان</p></div>
<div class="m2"><p>بلای من ز دلست اینت درد بی درمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا چه گویم گویم مرا چشم بدزد</p></div>
<div class="m2"><p>ترا چه گویم گویم مرا ز دل بستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم ز چشم ندزدی تباه گردد عیش</p></div>
<div class="m2"><p>ورم ز دل نستانی نفور گردد جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که شادی دل دیدو روشنایی چشم</p></div>
<div class="m2"><p>یکی ازین دو بندهد به صد هزار جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس آنکسی که مرادوست تر ز جان و دلست</p></div>
<div class="m2"><p>مرا تو گویی زو دور شو چگونه توان ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اختیار کس از یار خویش دور شود ؟</p></div>
<div class="m2"><p>به روز وصل کسی آرزوکند هجران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی زکام دل خویشتن بتابد روی ؟</p></div>
<div class="m2"><p>کسی به بازی با دوست بشکند پیمان ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چه گر تو نیایی زدست دوست بیاب</p></div>
<div class="m2"><p>مرا چه گر تو بمانی به دست دوست بمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من اینهمه ز طریق مطایبت گفتم</p></div>
<div class="m2"><p>مگر نگویی کاین ژاژ باشد و هذیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی که ژاژ دراید به درگهی نشود</p></div>
<div class="m2"><p>که چرب گویان آنجا شوند کند زبان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا زدوست به هر حال دور خواهد کرد</p></div>
<div class="m2"><p>هوای خدمت میر آن گزیده سلطان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصال دوست اگر چه موافقست و خوشست</p></div>
<div class="m2"><p>وصال خدمت درگاه میر بهتر از آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهبد سپه شاه شرق ابو منصور</p></div>
<div class="m2"><p>فراتگین دواتی امیر غرجستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امیر دوست نواز و امیر خصم گداز</p></div>
<div class="m2"><p>امیر شاعر خواه و امیر زایر خوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو تیغ گیرد بهرام دیس شور انگیز</p></div>
<div class="m2"><p>چو جام گیرد خورشیدوار زر افشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرای اوگه خوان و بساط او گه بزم</p></div>
<div class="m2"><p>زمدح خوانان خالی ندید هرگز خوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سخنوران جهان را که شعر جمع شده ست</p></div>
<div class="m2"><p>قراتگین دواتی ست اول دیوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هنر نماید چندان که چشم خیره شود</p></div>
<div class="m2"><p>به تیر و نیزه وزوبین و پهنه و چوگان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مقدم سپه خسروست او که به جنگ</p></div>
<div class="m2"><p>زپیش هیچ سپه بر نتافته ست عنان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به روز معر که وقتی که حرب سخت شود</p></div>
<div class="m2"><p>به تازیانه کند با مبارزان جولان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به حربگاهی کو تیغ بر کشد زنیام</p></div>
<div class="m2"><p>به صید گاهی کوتیر بر نهد به کمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ترس ناوک او شیر بفکند چنگال</p></div>
<div class="m2"><p>زبیم ضربت او پیل بفکند دندان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سیاستست مر او را که در ولایت او</p></div>
<div class="m2"><p>پلنگ رفت نیارد مگر گشاده دهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در این دیار به هنگام شار چندین بار</p></div>
<div class="m2"><p>پلنگ وار نمودند غرچگان عصیان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بجز به صلح و به شایستگی و خلعت و ساز</p></div>
<div class="m2"><p>به سر همی نتوانست برد با ایشان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگاه کن که امیر جلیل تا بنشست</p></div>
<div class="m2"><p>به جای شار به فرمان خسرو ایران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی از آنان کردن زراه راست بتافت</p></div>
<div class="m2"><p>کرانه کرد به مویی زطاعت و فرمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جز آن سبک خرد شور بخت سوخته مغز</p></div>
<div class="m2"><p>که غره کرد مر او را به خویشتن شیطان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به استواری جای وبه نامداری کوه</p></div>
<div class="m2"><p>فریفته شد و از راه راست کرد کران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه گفت گفت مرا جایگاه برفلکست</p></div>
<div class="m2"><p>به معدنی که همی زیر من رود کیوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمینیان رابا من کجا رود دیدار</p></div>
<div class="m2"><p>مرا نباشد جز با ستاره سیر وقران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر این حصار که من باشم ایمنم که مرا</p></div>
<div class="m2"><p>ز هیچ خلق نخواهد رسید هیچ زیان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همی ندید که برگاه شار شیر دلیست</p></div>
<div class="m2"><p>به تیغ شهر گشای و به تیر قلعه ستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به حیله ساختن استاد بخردان زمین</p></div>
<div class="m2"><p>به حرب کردن شاگرد پادشاه زمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گشاده شاه جهان پیش او به تیغ و سپر</p></div>
<div class="m2"><p>هزار قلعه صعب و هزار شارستان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر این حدیث سبک داشت لا جرم امروز</p></div>
<div class="m2"><p>همی کشید به دو پا سبک دو بند گران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آن حصار مر او را چنان فرود آورد</p></div>
<div class="m2"><p>که بخردان جهان را شگفتی آمد از آن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به کیمیا و طلسمات میرابو منصور</p></div>
<div class="m2"><p>طلسمهای سکندر همی کند ویران</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خهی گزیده و زیبا و بی بدل چو خرد</p></div>
<div class="m2"><p>زهی ستوده و بی عیب و پاک چون قرآن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به رادی و به سخا وبه مردی و به هنر</p></div>
<div class="m2"><p>همه جهان را دعویست مر ترا برهان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در این ولایت پیش از تو ای ستوده امیر</p></div>
<div class="m2"><p>کس ندید ز فضل و سخا دلیل و نشان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به روزگار تو پیدا شد و پدیدآمد</p></div>
<div class="m2"><p>سخای گم شده و فضل روی کرده نهان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زمین ز عدل تو بغداد دیگرست امروز</p></div>
<div class="m2"><p>تو چون خلیفه بغداد نایب یزدان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوان که قادر گردد در از دست شود</p></div>
<div class="m2"><p>امیر کوته دستست و قادرست و جوان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>غریب و نادر باشد جوان با پرهیز</p></div>
<div class="m2"><p>تو خویشتن ز جوانان غریب و نادر دان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه مایه مردم کز خانمان خویش برفت</p></div>
<div class="m2"><p>فرو گذاشت ضیاع و سرای آبادان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز ایمنی به وطن کردن اندر آمد باز</p></div>
<div class="m2"><p>به نام عدل تو ای یادگار نوشروان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدان امید که نانی به ایمنی بخورند</p></div>
<div class="m2"><p>غریب وار بپوشند جامه خلقان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زعدل وداد تو اندر همه ولایت که</p></div>
<div class="m2"><p>زیان زده نشد از هیچ گرگ هیچ شبان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کنون ندانند از خرمی و خوشی عیش</p></div>
<div class="m2"><p>که چون زیند خوش ار عدل پادشاه زمان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه شان ز دزدان ترس و نه از مصادره بیم</p></div>
<div class="m2"><p>نه خشک ریش ز همسایه و ز هم دندان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ولایت تو ز امن ای امیر چون حرم است</p></div>
<div class="m2"><p>ز خرمی وخوشی همچون روضه رضوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همی نمایی عدل و امانت و انصاف</p></div>
<div class="m2"><p>همی فزایی فضل و سخاوت و احسان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بسا پیاده که در خدمت تو گشت سوار</p></div>
<div class="m2"><p>بسا غریب که از تو به خان رسید و به مان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه جهان ز پی نام و نان دوند همی</p></div>
<div class="m2"><p>زخدمت تو همی نام حاصل آید و نان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همیشه تا گل سوری بود به فصل بهار</p></div>
<div class="m2"><p>چنانکه نرگس مشکین بودبه وقت خزان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همیشه تا به همه جایگه پدید بود</p></div>
<div class="m2"><p>هوای تیر مهی از هوای تابستان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>امیر باش و جهان را به کام خویش گذار</p></div>
<div class="m2"><p>هوای خویش بیاب و مراد خویش بران</p></div></div>