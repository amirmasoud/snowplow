---
title: >-
    شمارهٔ ۶۶ - در مدح امیر یوسف بن ناصر الدین گوید
---
# شمارهٔ ۶۶ - در مدح امیر یوسف بن ناصر الدین گوید

<div class="b" id="bn1"><div class="m1"><p>کاشکی کردمی از عشق حذر</p></div>
<div class="m2"><p>یا کنون دارمی از دوست خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دریغا که من از دست شدم</p></div>
<div class="m2"><p>نوز ناخورده تمام از دل بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون توان بود برین درد صبور</p></div>
<div class="m2"><p>چون توان برد چنین روز بسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق با من سفری گشت و بماند</p></div>
<div class="m2"><p>مونس من به حضر خسته جگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور بودن ز چنان روی ،غمیست</p></div>
<div class="m2"><p>هر چه دشوارتر و هر چه بتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیک غزنین نرسیده ست که من</p></div>
<div class="m2"><p>خبری یابم از دوست مگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سفر از دوست جدا کرد مرا</p></div>
<div class="m2"><p>گم شود از دو جهان نام سفر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من شفاعت کنم امسال ز میر</p></div>
<div class="m2"><p>تا مرا دست بدارد ز حضر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر یوسف پسر ناصر دین</p></div>
<div class="m2"><p>لشکر آرای شه شیر شکر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شه ایران والا به نسب</p></div>
<div class="m2"><p>با شه ایران همتا به گهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه بر درگه سلطان جهان</p></div>
<div class="m2"><p>جای او پیشتر از جای پسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه نازیدن میر از ملک است</p></div>
<div class="m2"><p>زین ستوده ست بر اهل هنر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچنان در خور از روی قیاس</p></div>
<div class="m2"><p>کان ملک شمست این میر قمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ملک او را بسزا دارد از آنک</p></div>
<div class="m2"><p>یاد گارست ملک را ز پدر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لاجرم میر گرفته ست مدام</p></div>
<div class="m2"><p>خدمت او چو نماز اندر بر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز و شب پیش همه خلق زبان</p></div>
<div class="m2"><p>بثنا گفتن او دارد تر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه از دولت او جوید نام</p></div>
<div class="m2"><p>همه در خدمت او دارد سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا ثنای ملک شرق بود</p></div>
<div class="m2"><p>بثنای دگران رنج مبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این هم از خدمت باشد که ز من</p></div>
<div class="m2"><p>بخرد مدح شه شرق بزر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوستانرا دل از اینگونه بود</p></div>
<div class="m2"><p>دوستارانرا زین نیست گذر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاد باد آن هنری میر که هست</p></div>
<div class="m2"><p>پادشاهی و شهی را در خور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن نکو سیرت و نیکو مذهب</p></div>
<div class="m2"><p>آن نکو منظر ونیکو مخبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنکه اندر سپه شاه کسی</p></div>
<div class="m2"><p>پیش او نام نگیرد زهنر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون عطا بخشد اقرار کنی</p></div>
<div class="m2"><p>که جهانرا بر او نیست خطر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بجنگ آید گویی که مگر</p></div>
<div class="m2"><p>نرسیده ست بدونام حذر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از حریصی که بجنگست مثل</p></div>
<div class="m2"><p>جنگ را بندد هر روز کمر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دشمنانرا چو کمان خواهد میر</p></div>
<div class="m2"><p>هیچ امید نماند به سپر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه کتب عرب و کتب عجم</p></div>
<div class="m2"><p>بر تو بر خواند چون آب زبر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سخنانش همه یکسر نکتست</p></div>
<div class="m2"><p>چون سخن گوید تو نکته شمر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا همی سرخ بود آذر گون</p></div>
<div class="m2"><p>تا همی سبزبود سیسنبر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا بود لعلی نعت گل نار</p></div>
<div class="m2"><p>چون کبودی صفت نیلوفر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شادمان باد و بکام دل خویش</p></div>
<div class="m2"><p>آن پسندیده خوی خوب سیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیکوانی چو نگار اندر پیش</p></div>
<div class="m2"><p>دلبرانی چو بهار اندر بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همچو این عید بشادی و خوشی</p></div>
<div class="m2"><p>بگذاراد و هزاران دگر</p></div></div>