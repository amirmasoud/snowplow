---
title: >-
    شمارهٔ ۱۹۴ - در مدح سلطان محمود غزنوی گوید
---
# شمارهٔ ۱۹۴ - در مدح سلطان محمود غزنوی گوید

<div class="b" id="bn1"><div class="m1"><p>مهرگان آمد و سیمرغ بجنبید از جای</p></div>
<div class="m2"><p>تا کجا پرزند امسال و کجا دارد رای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت آن شد که به دشت آید طاوس و تذرو</p></div>
<div class="m2"><p>تاشود بر سر شخ کبک دری شعر سرای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیز در بیشه و در دشت همانا نبود</p></div>
<div class="m2"><p>باز را از پی مرغان شکاری شو وآی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز و جز باز کنون روی نیارند نمود</p></div>
<div class="m2"><p>گاه آنست که سیمرغ شود روی نمای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه مرغان جهان سر به خس اندر شده اند</p></div>
<div class="m2"><p>اندرآن وقت که سیمرغ بجنبید از جای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندرین وقت چه شاهین و چه باز و چه عقاب</p></div>
<div class="m2"><p>جمله محبوس سپاهند بر ایشان بخشای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مثل جنبش سیمرغ چه چیزست بگوی</p></div>
<div class="m2"><p>مثل جنبش شاه آن ملک شهر گشای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروغازی محمود خداوند جهان</p></div>
<div class="m2"><p>آنکه بگرفت جهان جمله به توفیق خدای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بجنبید ز غزنین همه شاهان جهان</p></div>
<div class="m2"><p>بیشه گیرند و بیابان بدل باغ وسرای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهراسند و به فتح و ظفرش فال زنند</p></div>
<div class="m2"><p>گر مثل بر سر ایشان فکند سایه همای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او چو سیمرغست آری و شهان جمله چو مرغ</p></div>
<div class="m2"><p>مرغ با هیبت سیمرغ کجا دارد پای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاد باد آن هنری شاه جهانگیر که کرد</p></div>
<div class="m2"><p>همه شاهان جهان را به هنر دست گرای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اوبه سند وبه سر اندیب و به جیپور بود</p></div>
<div class="m2"><p>هیبت او به ختاخان و به فرغانه تغای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوش نخسبند همی از فزع و هیبت او</p></div>
<div class="m2"><p>نه به روم اندر قیصر نه به هند اندر رای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وقت جنبیدن او هیچ مخالف نبود</p></div>
<div class="m2"><p>که نه با حسرت وغم باشدو با ناله و رای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این همی گوید: کای بخت! بیکباره مرو</p></div>
<div class="m2"><p>وان همی گوید: کای دولت! یکروز بپای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخت و دولت بر آن کس چه کند کو نکند</p></div>
<div class="m2"><p>به تن و جان و به دل خدمت آن بار خدای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه او خدمت فرخنده او پیش گرفت</p></div>
<div class="m2"><p>بر جهان کامروا گردد و فرمانفرمای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا قدر خان کمر خدمت او بست ببست</p></div>
<div class="m2"><p>از پی خدمت او یکرهه فغفور قبای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه ترکستان بگرفت و به خانی بنشست</p></div>
<div class="m2"><p>به شرف روز فزون و به هنر روز افزای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دولت سلطان بر هر که بتابد نشگفت</p></div>
<div class="m2"><p>گر شود باد هوا بر سر او عنبر سای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سال و مه دولت آن بار خدای ملکان</p></div>
<div class="m2"><p>همچنان باد ولی پرور و دشمن فرسای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از همه شاهان امروز که دانی جز ازو</p></div>
<div class="m2"><p>مملکت را و بزرگی و شهی را دربای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر کسی گوید: ماننده او هیچ شهست</p></div>
<div class="m2"><p>گو: بروخام درایی مکن و ژاژ مخای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنکه او را بستاید چه بود: پاک سخن</p></div>
<div class="m2"><p>وانکه او را نستاید چه بود: یافه درای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر ستایش که جز او راست نکوهش به از آن</p></div>
<div class="m2"><p>فرخی تا بتوانی جز از او را مستای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا چو بیجاده نباشد به نکو رنگی سنگ</p></div>
<div class="m2"><p>تا چو یاقوت نباشد به بها کاهربای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شادمان با دو تن آسان و به کام دل خویش</p></div>
<div class="m2"><p>دشمنان را ز نهیبش دل وجان اندروای</p></div></div>