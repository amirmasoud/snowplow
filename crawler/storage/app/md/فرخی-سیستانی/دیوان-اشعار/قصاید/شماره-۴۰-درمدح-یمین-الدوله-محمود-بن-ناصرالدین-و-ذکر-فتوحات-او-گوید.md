---
title: >-
    شمارهٔ ۴۰ - درمدح یمین الدوله محمود بن ناصرالدین و ذکر فتوحات او گوید
---
# شمارهٔ ۴۰ - درمدح یمین الدوله محمود بن ناصرالدین و ذکر فتوحات او گوید

<div class="b" id="bn1"><div class="m1"><p>سال و ماه نیک و روز خرم و فرخ بهار</p></div>
<div class="m2"><p>بر شه فرخنده پی فرخنده بادا هر چهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو غازی سر شاهان و تاج خسروان</p></div>
<div class="m2"><p>میر محمود آن شه دریا دل دریا گذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه بر درگاه او خدمتگرانند از ملوک</p></div>
<div class="m2"><p>هر یکی اندر دیار خویش روی صد تبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشاهی کو بداند نام نیک از نام بد</p></div>
<div class="m2"><p>خدمت سلطان کند بر پادشاهی اختیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدمت سلطان بجان از شهریاری خوشترست</p></div>
<div class="m2"><p>وین کسی داند که خواهد بر خورد از روزگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کسی کو خدمت محمود را شایسته گشت</p></div>
<div class="m2"><p>عاقبت محمود خواهد کردن اورا کردگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را توفیق یارست او بدان خدمت رسد</p></div>
<div class="m2"><p>بخ بر آن کس بادکان کس را بود توفیق یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شه پاکیزه دین! ای پادشاه راستین !</p></div>
<div class="m2"><p>ای مبارک خدمت تو خلق را امیدوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان خذلان ندانم برتر از عصیان تو</p></div>
<div class="m2"><p>یارب این خذلان ز شهر ما و از ما دور دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باغهایی دیده ام من چون بهشت اندر بهشت</p></div>
<div class="m2"><p>کاخهایی دیده من چون بهار اندر بهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون درو خذلان و عصیان توای شه راه یافت</p></div>
<div class="m2"><p>کاخها شد جای جغد و باغها شد جای مار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کجا مردم رسید و هر کجا مردم رسند</p></div>
<div class="m2"><p>تو رسیدستی ولشکر بردی آنجا چند بار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بیابانهای بی ره با سپه بیرون شدی</p></div>
<div class="m2"><p>چون مراد آمد ترا بگذاشتی دریا سوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جنگ دریا کردی و از خون دریا باریان</p></div>
<div class="m2"><p>روی دریا لعل کردی چو شکفته لاله زار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من شکار آب مرغابی و ماهی دیده ام</p></div>
<div class="m2"><p>تو در آب امسال شیران سیه کردی شکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کجا گردنکشی اندر جهان سر بر کشید</p></div>
<div class="m2"><p>تو بر آوری بشمشیر از تن و جانش دمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طاغیان و عاصیان را سر بسر کردی مطیع</p></div>
<div class="m2"><p>ملحدان و گمرهانرا جمله بر کردی بدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عیشهای بت پرستان تلخ کردی چون کبست</p></div>
<div class="m2"><p>روزهای دشمنان دین سیه کردی چو قار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خانمان دوستان را خوب کردی چون بهشت</p></div>
<div class="m2"><p>روزگار نیکخواهان تازه کردی چون بهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر چه در هندوستان پیل مصاف آرای بود</p></div>
<div class="m2"><p>پیش کردی و در آوردی بدشت شا بهار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زین به کرگان بر نهادی در میان بیشه شان</p></div>
<div class="m2"><p>اندر آوردی بلشکر گه چو اشتر بر قطار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر سر آوردی نهنگان را بخشت از قعر آب</p></div>
<div class="m2"><p>سر نگون کردی پلنگان را بتیر از کوهسار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیشه ها بی شیر کردی، دشتها بی اژدها</p></div>
<div class="m2"><p>قلعه ها بی مرد کردی ،شهرها بی شهریار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خسروی از خسروانی بستدی پیروز بخت</p></div>
<div class="m2"><p>تخت و ملک ازخانه هایی برگرفتی نامدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خانه یعقوبیان وخانه مأمونیان</p></div>
<div class="m2"><p>خانه چیپالیان و این چنین صد برشمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لشکر ایشان شکستی کشور ایشان گرفت</p></div>
<div class="m2"><p>با کدامین شاه خواهی کرد زین پس کار زار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کارهای شیر مردان کردی و از رشک تو</p></div>
<div class="m2"><p>حاسدانت یاوه گو هستند و جمله ژاژ خوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر کسی خواهد که در گیتی چو تو کاری کند</p></div>
<div class="m2"><p>چون کند، چون در همه گیتی نیابد هیچ کار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عمرهای نوح باید تا شهی خیزد دگر</p></div>
<div class="m2"><p>هم از آن شاهان که تو بر کنده ای از بیخ و بار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یاد کن تا برچه لشکرها شدستی کامران</p></div>
<div class="m2"><p>یاد کن تا برچه کشورها شدستی کامگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این جهان از دست شاهانی برون کردی که بود</p></div>
<div class="m2"><p>هر یکی را چون فریدون ملک، صد پیشکار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرغزاری هست گیتی وتو شیری از قیاس</p></div>
<div class="m2"><p>بس هزبران را که تو کردی برون از مرغزار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مردمان اندر حصار امید امنی را شوند</p></div>
<div class="m2"><p>کس نیارد شد همی از بیم تو اندر حصار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا تو ای خسرو حصار سیستان بگشاده ای</p></div>
<div class="m2"><p>استواری نیست کس را بر حصار استوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همچنان خواهم که باشی خسرو و شادان دلت</p></div>
<div class="m2"><p>تن درست و شادمان و شاد کام و شادخوار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خسرو پیروز بختی شهریار چیره دست</p></div>
<div class="m2"><p>فتح و نصرت بر یمین و بخت و دولت بر یسار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>روز تو فرخنده باد و عمر تو پاینده باد</p></div>
<div class="m2"><p>دولت تو بیکران و ملت تو بیکنار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گاه می خوردن می تو بر کف معشوق تو</p></div>
<div class="m2"><p>وقت آسایش بتت را پای تو اندر کنار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مر مرا در خدمت تو زندگانی باد دیر</p></div>
<div class="m2"><p>تا ببینم مر ترا در مکه با اهل و تبار</p></div></div>