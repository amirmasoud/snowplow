---
title: >-
    شمارهٔ ۲۴ - در مدح خواجه عبد الرزاق بن احمد بن حسن میمندی گوید
---
# شمارهٔ ۲۴ - در مدح خواجه عبد الرزاق بن احمد بن حسن میمندی گوید

<div class="b" id="bn1"><div class="m1"><p>ای دل من ترا بشارت داد</p></div>
<div class="m2"><p>که ترا من بدوست خواهم داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو بدو شادمانه ای بجهان</p></div>
<div class="m2"><p>شاد باد آنکه توبدویی شاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نگویی که مرمرا مفرست</p></div>
<div class="m2"><p>که کسی دل بدوست نفرستاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست از من ترا همی طلبد</p></div>
<div class="m2"><p>رو بر دوست هر چه بادا باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست و پایش ببوس و مسکن کن</p></div>
<div class="m2"><p>زیر آن زلفکان چون شمشاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز بیداد چشم او برهی</p></div>
<div class="m2"><p>از لب لعل او بیابی داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف او حاجب لبست و لبش</p></div>
<div class="m2"><p>نپسندد بهیچکس بیداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاصه بر تو که تو فزون ز عدد</p></div>
<div class="m2"><p>آفرین های خواجه داری یاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجه سید ستوده هنر</p></div>
<div class="m2"><p>خواجه پاک طبع پاک نژاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عبد رزاق احمد حسن آنک</p></div>
<div class="m2"><p>هیچ مادر چو او کریم نزاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه کافی تر و سخی تر ازو</p></div>
<div class="m2"><p>بر بساط زمین قدم ننهاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوی او خوب و روی چون خو خوب</p></div>
<div class="m2"><p>دل او را و دست چون دل راد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کافیان جهان همی خوانند</p></div>
<div class="m2"><p>از دل پاک خواجه را استاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسته هایی گشاده گشت بدو</p></div>
<div class="m2"><p>که ندانست روزگار گشاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از وزیران چو او یکی ننشست</p></div>
<div class="m2"><p>بر بساط جم و بساط قباد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فیلسوفی بسر نداند برد</p></div>
<div class="m2"><p>سخنی را که او نهد بنیاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسخن گفتن آن ستوده سخن</p></div>
<div class="m2"><p>نرم گرداند آهن و پولاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راد مردان بدو روند همی</p></div>
<div class="m2"><p>کو رسد راد مرد را فریاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زو تواند بپایگاه رسید</p></div>
<div class="m2"><p>هر که از پایگاه خویش افتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بس کسا کو بفر دولت او</p></div>
<div class="m2"><p>کار ویران خویش کرد آباد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خانه او بهشت شد که درو</p></div>
<div class="m2"><p>غمگنان را ز غم کنند آزاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نزد آن خواجه خادمانش را</p></div>
<div class="m2"><p>هست پاداش خدمتی هفتاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هیچ شه را چنین وزیر نبود</p></div>
<div class="m2"><p>هیچ مادر چنو کریم نزاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جمع شد نزد او هزار هنر</p></div>
<div class="m2"><p>که بشادی هزار سال زیاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پدر و مادر سخاوت وجود</p></div>
<div class="m2"><p>هر دو خوانند خواجه را داماد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیش دو دست او سجود کنند</p></div>
<div class="m2"><p>چون مغان پیش آذر خرداد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر که او معدن کریمی جست</p></div>
<div class="m2"><p>بدر کاخ او فرو استاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آفتاب کرام خواهد کرد</p></div>
<div class="m2"><p>لقب او ،خلیفه بغداد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا به مرداد گرم گردد آب</p></div>
<div class="m2"><p>تا به دی ماه سرد گردد باد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بوقت خزان چودشت شود</p></div>
<div class="m2"><p>باغهای چو بتکده نوشاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بادل شاد باد چون شیرین</p></div>
<div class="m2"><p>دشمنش مستمند چون فرهاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روزگارش خجسته باد وبراو</p></div>
<div class="m2"><p>مهر گان فرخ و همایون باد</p></div></div>