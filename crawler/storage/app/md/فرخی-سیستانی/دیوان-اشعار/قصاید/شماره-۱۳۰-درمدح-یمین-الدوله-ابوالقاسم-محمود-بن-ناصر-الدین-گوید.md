---
title: >-
    شمارهٔ ۱۳۰ - درمدح یمین الدوله ابوالقاسم محمود بن ناصر الدین گوید
---
# شمارهٔ ۱۳۰ - درمدح یمین الدوله ابوالقاسم محمود بن ناصر الدین گوید

<div class="b" id="bn1"><div class="m1"><p>بنفشه زلف من آن آفتاب ترکستان</p></div>
<div class="m2"><p>همی بنفشه پدید آرد از دو لاله ستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا بنفشه و لاله بکار نیست که او</p></div>
<div class="m2"><p>بنفشه دارد و زیر بنفشه لاله نهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنگ لاله او وز دم بنفشه او</p></div>
<div class="m2"><p>جهان نگار نمایست و باد مشک افشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی ندانم کاین را که رنگ داد چنین</p></div>
<div class="m2"><p>همی ندانم کانرا که بوی داد چنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا روا بود ار سر بسر بنفشه دمد</p></div>
<div class="m2"><p>بگرد لاله آن سرو قد موی میان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون ز سنگ بنفشه دمد عجب نبود</p></div>
<div class="m2"><p>اگر بنفشه دمد زیر عارض جانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهشت وار شود بوستان عارض او</p></div>
<div class="m2"><p>چنان کجا شود اکنون بهشت وار جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون برافکند از پرنیان درخت ردا</p></div>
<div class="m2"><p>کنون بگسترد از حله باغ شادروان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون چو مست غلامان سبز پوشیده</p></div>
<div class="m2"><p>ببوستان شوداز باد زاد سرو نوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون سپیده دمان فاخته ز شاخ چنار</p></div>
<div class="m2"><p>چو عاشقان غمین برکشد خروش و فغان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه باغ را بشناسی ز کلبه عطار</p></div>
<div class="m2"><p>نه راغ را بشناسی ز مجلس سلطان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یمین دولت ابوالقاسم آفتاب ملوک</p></div>
<div class="m2"><p>امین ملت محمود پادشاه زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدایگان خرد پرور مروت ارز</p></div>
<div class="m2"><p>بلند همت و زایر نواز و حرمت دان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازو شود همه امیدهای خلق روا</p></div>
<div class="m2"><p>بدو شود همه دشوارهای دهر آسان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی که مدحش اندر دهان او بگذشت</p></div>
<div class="m2"><p>نسوزد ار بکف آتش در افکند بدهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر چه قرآن فاضل بود بیابد مرد</p></div>
<div class="m2"><p>ز مدح خواندن او مزد خواندن قرآن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بوصف کردن او در ببارد و عنبر</p></div>
<div class="m2"><p>ز طبع مدحت گوی و ز لفظ مدحت خوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزرگ نام کندنزد خلق دیوان را</p></div>
<div class="m2"><p>سخنوری که کند مدح او سر دیوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهانیان چو ازیشان کسی سخن طلبد</p></div>
<div class="m2"><p>سخن طلب را نزدیک او دهند نشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سخن شناسان بر جود او شدند یقین</p></div>
<div class="m2"><p>کجا یقین بود آنجا بکار نیست گمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عطای وافر، برهان جود او بنمود</p></div>
<div class="m2"><p>عطا بود بهمه حال جود را برهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی نگردد چندانکه دم زنی فارغ</p></div>
<div class="m2"><p>ز بر کشیدن زر عطای او وزان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عنان چرمین گر سایدی ز فیض سخاش</p></div>
<div class="m2"><p>بدستش اندر زرین شدی دوال عنان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بحیله پایگه همتش همی طلبد</p></div>
<div class="m2"><p>ازین قبل شده بر چرخ هفتمین کیوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرا ز فر همای ای شگفت یاد کند</p></div>
<div class="m2"><p>کسی که دیده بود فر سایه یزدان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همای چون بکسی سایه برفکند آن کس</p></div>
<div class="m2"><p>جز آن بود که بزرگی و جاه یابد از آن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>امیر اگر زبر کشته سایه برفکند</p></div>
<div class="m2"><p>ز فر سایه او کشته باز یابد جان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه دلایل فرهنگ را به اوست مآب</p></div>
<div class="m2"><p>همه مسایل سربسته را ازوست بیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بروز معرکه اندر مصاف دشمن او</p></div>
<div class="m2"><p>ز بیم ضربت او پیل بفکند دندان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرآن سوار که نزدیک او بجنگ آید</p></div>
<div class="m2"><p>اجل فرو شود اندر تنش بجای روان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مبارزان عدو پیش او چنان آیند</p></div>
<div class="m2"><p>چو مورچه که بود بر گرفته دانه گران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسوی باز شد از پیش او چنان تازند</p></div>
<div class="m2"><p>چو سوی ژرفی خاشاکها بر آب روان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر عدو بتن اندر فرو برد به دبوس</p></div>
<div class="m2"><p>چنانکه پتک زن اندر زمین برد سندان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کمان فروفتد از دست دشمن اندر جنگ</p></div>
<div class="m2"><p>بدانگهی که ملک برد دست سوی کمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زسهم نامش دست دبیر سست شود</p></div>
<div class="m2"><p>چو کرد خواهد برنامه نام او عنوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همیشه باشد از مهر او و کینه او</p></div>
<div class="m2"><p>ولی مقارن سود و عدو عدیل زیان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز کین او دل دشمن چنان شود که شود</p></div>
<div class="m2"><p>ز نور ماه درخشنده جامه کتان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز قدر او نپذیرد خدای عز و جل</p></div>
<div class="m2"><p>ز هیچ دشمن او روز رستخیز امان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه تا چو گل نسترن بو لؤلؤ</p></div>
<div class="m2"><p>چنان کجا چو گل ارغوان بود مرجان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه تابود آز و امید در دل خلق</p></div>
<div class="m2"><p>چنان چو آتش در سنگ وگوهر اندر کان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خدایگان جهان باد و پادشاه زمین</p></div>
<div class="m2"><p>بعون ایزد کشور گشاو شهرستان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ازو هر آنکه بود بدسکال او غمگین</p></div>
<div class="m2"><p>بدو هر آنکه بود نیکخواه او شادان</p></div></div>