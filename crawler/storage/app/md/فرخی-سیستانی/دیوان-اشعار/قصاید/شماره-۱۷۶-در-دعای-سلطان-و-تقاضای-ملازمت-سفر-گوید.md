---
title: >-
    شمارهٔ ۱۷۶ - در دعای سلطان و تقاضای ملازمت سفر گوید
---
# شمارهٔ ۱۷۶ - در دعای سلطان و تقاضای ملازمت سفر گوید

<div class="b" id="bn1"><div class="m1"><p>ای بر گذشته از ملکان پایگاه تو</p></div>
<div class="m2"><p>قدر تو بر سپهر بر آورده گاه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه منیر صورت ماه درفش تو</p></div>
<div class="m2"><p>روز سپید سایه چتر سیاه تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان ملوک را فزع آید زتیغ تو</p></div>
<div class="m2"><p>جاه ملوک راحسد آید ز جاه تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مریخ روز معرکه شاها غلام تست</p></div>
<div class="m2"><p>چونانکه زهره روز میزدست داه تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز جود بر تو هیچ کسی پادشاه نیست</p></div>
<div class="m2"><p>گنج ترا تهی کند این پادشاه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برتر گناه نزد تو بخلست و هیچ کس</p></div>
<div class="m2"><p>زین روی بر تو چیره نبیند گناه تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو کارها تبه نکنی و رتبه کنی</p></div>
<div class="m2"><p>از راست کرده های جهان به تباه تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دشمنی که بند تو و چاه تو بدید</p></div>
<div class="m2"><p>او را اجل برون برد از بند و چاه تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر گرد رزمگاه تو گر باد بگذرد</p></div>
<div class="m2"><p>ناخسته گشته نگذرد از رزمگاه تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کیست کو به جان نبود مهر جوی تو</p></div>
<div class="m2"><p>وآن کیست کو به دل بود نیکخواه تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز عدوی تو بهراسد ز کبک تو</p></div>
<div class="m2"><p>کوه مخالف تو، نسنجد به کاه تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فربه شده ست و روز فزون گنج و ملک تو</p></div>
<div class="m2"><p>زان نیز کاسته تن بدخواه جاه تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای پیشگاه بار خدایان روزگار</p></div>
<div class="m2"><p>ای بهر بهشت جسته شرف پیشگاه تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر عزم رفتنی ومرا رای رفتنست</p></div>
<div class="m2"><p>از بهر خدمت تو ملک با سپاه تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با بندگان مرا به ره اندر عدیل کن</p></div>
<div class="m2"><p>تا در دو دیده سرمه کنم خاک راه تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اندر پناه خویش مرا جایگاه ده</p></div>
<div class="m2"><p>کایزد نگاهدار تو باد و پناه تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر شاعری به گاه امیری بزرگ شد</p></div>
<div class="m2"><p>نشگفت اگر بزرگ شدم من به گاه تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فضل تو بر همه شعرا گستریده شد</p></div>
<div class="m2"><p>گسترده باد برتو رضای اله تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باشد همیشه عزو سعادت ترا قرین</p></div>
<div class="m2"><p>کردار تو بود به سعادت گواه تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ماه منیر و مهر فروزنده پرتوی</p></div>
<div class="m2"><p>هست از مه درفش و ز چتر سیاه تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تاسال و ماه و روز وشبست اندرین جهان</p></div>
<div class="m2"><p>فرخنده باد روز وشب و سال و ماه تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندر نبرد پشت و پناه تو کردگار</p></div>
<div class="m2"><p>وندر میزد مونس جان تو ماه تو</p></div></div>