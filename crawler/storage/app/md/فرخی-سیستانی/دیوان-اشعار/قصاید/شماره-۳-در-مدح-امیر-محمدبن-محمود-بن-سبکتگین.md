---
title: >-
    شمارهٔ ۳ - در مدح امیر محمدبن محمود بن سبکتگین
---
# شمارهٔ ۳ - در مدح امیر محمدبن محمود بن سبکتگین

<div class="b" id="bn1"><div class="m1"><p>دوست دارم کودک سیمین‌برِ بیجاده لب</p></div>
<div class="m2"><p>هر کجا زیشان یکی بینی مرا آنجا طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاصه با روی سپید و پاک چون تابنده روز</p></div>
<div class="m2"><p>خاصه با موی سیاه و تیره چون تاریک شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که را زینگونه باشد ماهرویی مشکموی</p></div>
<div class="m2"><p>نیست معذور، ار بیاساید زمانی از طرب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ستاده ست از دو چشمش بر نباید داشت چشم</p></div>
<div class="m2"><p>تا نشسته ست از دو لعلش بر نشاید داشت لب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مرا زین کودک بت روی دادستی خدای</p></div>
<div class="m2"><p>بر لب او بوسه ها میدادمی دادن عجب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خوشا زین پیشتر کاندر سرایم زین صفت</p></div>
<div class="m2"><p>کودکان بودند سیمین سینه و زرین سلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با سرینهای سپید و گرد چون تل سمن</p></div>
<div class="m2"><p>با میانهای نزار و زار چون تار قصب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دلارامی و نغزی چون غزلهای شهید</p></div>
<div class="m2"><p>وز دلاویزی و خوبی چون ترانه بوطلب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر تهی شد زین بتان اکنون سرایم باک نیست</p></div>
<div class="m2"><p>دل پرست از آفرین خسرو خسرونسب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پادشه زاده محمد خسرو پیروز بخت</p></div>
<div class="m2"><p>سر فراز تاجداران عجم و آن عرب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسروان را گر نسب نیکوترین چیزی بود</p></div>
<div class="m2"><p>هم نسب دارد ملک زاده بملک و هم حسب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای قرین آورده اندر فضل بر خوی ملک</p></div>
<div class="m2"><p>ای هزینه کرده ملک و مال برنام و نسب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش از این هر شاهی و هر خسروی فرزند را</p></div>
<div class="m2"><p>از پی فرهنگ شاگرد فلان کردی لقب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهمن آنگه روستم را چند گه شاگرد شد</p></div>
<div class="m2"><p>تا خصالش بیخلل گشت و فعالش منتخب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنان کیخسرو واسفندیار گرد را</p></div>
<div class="m2"><p>رستم دستان همی آموخت فرهنگ و ادب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو هم از خردی بدانستی همه فرهنگها</p></div>
<div class="m2"><p>ناکشیده ذل شاگردی ونادیده تعب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو دلی داری چو دریا و کفی داری چو ابر</p></div>
<div class="m2"><p>زان همی پاشی جواهر، زین همی باری ذهب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در هنر شاگرد خویشی چون نکوتر بنگری</p></div>
<div class="m2"><p>فضلهای خویشتن را هم تو بودستی سبب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم خداوند سخایی هم خداوند سخن</p></div>
<div class="m2"><p>هم خداوند حسامی هم خداوند حسب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جز ملک محمود را، هر خسروی را خسروی</p></div>
<div class="m2"><p>هیچ خسرو رانیاید زین که من گفتم غضب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پادشاهی چون تویی از پادشاهان جهان</p></div>
<div class="m2"><p>پادشاهی را به تست ای پادشه زاده نسب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فر شاهی چون تو داری لاجرم شاهی توراست</p></div>
<div class="m2"><p>من چه دانم کردن ار پیداستی خار از رطب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عامل بصره بنام تو همی خواهد خراج</p></div>
<div class="m2"><p>خاطب بغداد بر نامت همی خواند خطب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرت فرمان آید از سلطان که خالی کن عراق</p></div>
<div class="m2"><p>گردن گردنکشانرا نرم گردان چون عصب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نامه فتح تو از شام آید و دیگر ز مصر</p></div>
<div class="m2"><p>منزلی زان تو حلوان باشد و دیگر حلب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خانه بی طاعتان از تیغ تو گردد خراب</p></div>
<div class="m2"><p>گنجهای مغربی از دست تو گردد خرب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ور بر این سوی دگر فرمان دهد شمشیر تو</p></div>
<div class="m2"><p>فرد گرداند ز خانان تا که چین از فرب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همچنان چون طبع تو بر راد مردی شیفته است</p></div>
<div class="m2"><p>تیغ تو بر کشتن و خون ریختن دارد سغب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اندر آن صحرا که شیران دو لشکر صف کشند</p></div>
<div class="m2"><p>و آسمان از بر همی خواند برایشان «اقترب »</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چشمه روشن نبیند دیده از گرد سپاه</p></div>
<div class="m2"><p>بانگ تندر نشنود گوش از غو کوس و چلپ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گشته از تیر خدنگ اندر کف مردان بجنگ</p></div>
<div class="m2"><p>درقها چون کاغذ آماج سلطان پر ثقب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سیل خون اندر میانشان رفته و برخاسته</p></div>
<div class="m2"><p>بر سر خون همچنان بیجاده گنبدهاحبب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تیغها چون ارغوان و رویها چون شنبلید</p></div>
<div class="m2"><p>آن ز خون خلق و این از بیم تاراج و نهب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون همای رایت تو روی بنماید ز دور</p></div>
<div class="m2"><p>زان دو لشکر در زمان بنشیند آشوب و شغب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نامجویانشان بجای نام بپسندند ننگ</p></div>
<div class="m2"><p>پیشدستانشان همی پیشی کنند اندر هرب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رزمگه زیشان چنان گردد که پنداری بود</p></div>
<div class="m2"><p>هیبت تو باد و ایشان کاه و آن صحرا خشب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جامه نادوخته پوشد هم از روز نخست</p></div>
<div class="m2"><p>هر کسی کو را گرفت از هیبت تیغ تو تب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای محمد سیرت و نامت محمد هر که او</p></div>
<div class="m2"><p>از محمد بازگردد بازگشت از دین رب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دشمنان تو شریک دشمنان ایزدند</p></div>
<div class="m2"><p>بر تو یک یک راز گیتی بر گرفتن «قدوجب»</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از قیاس نام تو مر بدسگالان ترا</p></div>
<div class="m2"><p>گاه بوجهل لعین خوانیم و گاهی بولهب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرد بوجهل آنکسی گردد که نندیشد ز جهل</p></div>
<div class="m2"><p>بولهب را بر خود آن خواند که بپسندد لهب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر کسی گوید: من و تو، آسمان گوید بدو</p></div>
<div class="m2"><p>تو چو او باشی، اگر باشد روا که همچو حب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>من یقین دانم همی گر چه رجب را فضلهاست</p></div>
<div class="m2"><p>یکشب از ماه مبارک به که سی روز از رجب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای تمامی طالع سعد تو ناکرده پدید</p></div>
<div class="m2"><p>دشمنانت چون ستاره بر فلک زیر ذنب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زانکه زین پس تو بزخم هندی و تاب کمند</p></div>
<div class="m2"><p>کرد خواهی گردن هر بدسگالی را ادب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدسگال تو زه پیراهن از بیم مسد</p></div>
<div class="m2"><p>باز نشناسد همی در گردن خویش از کنب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا چو بنویسی بصورت هر یکی چون هم بوند</p></div>
<div class="m2"><p>شیر و شیر و دیر و دیر و زیر و زیر و حب و حب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا نسازد کامل اندر دایره با منسرح</p></div>
<div class="m2"><p>تا نباشد وافر اندر دایره با مقتضب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شادمان باش ای کریم و در کریمی بی ریا</p></div>
<div class="m2"><p>پادشا باش ای جواد و در جوادی بی ریب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دشمنان و حاسدان و بدسگالان ترا</p></div>
<div class="m2"><p>مرگ اندر بیکسی و زندگانی در تعب</p></div></div>