---
title: >-
    شمارهٔ ۱۶ - در مدح خواجه ابوبکر حصیری گوید
---
# شمارهٔ ۱۶ - در مدح خواجه ابوبکر حصیری گوید

<div class="b" id="bn1"><div class="m1"><p>دل آن ترک نه اندر خور سیمین بر اوست</p></div>
<div class="m2"><p>سخن او نه ز جنس لب چون شکر اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با لب شیرین با من سخنان گوید تلخ</p></div>
<div class="m2"><p>سخن تلخ نداند که نه اندر خور اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه باندازه کند کار و نگویم که مکن</p></div>
<div class="m2"><p>چکنم پس که مرا جان جهان در بر اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از همه خلق دل من سوی او دارد میل</p></div>
<div class="m2"><p>بیهده نیست پس آن کبر که اندر سر اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو را ماند کآورده گل سوری بار</p></div>
<div class="m2"><p>بینی آن سرو که خندان گل سوری بر اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مادرش گفت پسر زایم سرو و مه زاد</p></div>
<div class="m2"><p>پس مرا این گله و مشغله با مادر اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن رخ چون گل بشکفته وبالای چو سرو</p></div>
<div class="m2"><p>خواجه دیده ست همانا که رهش بردر اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه سیدبوبکر حصیری که خدای</p></div>
<div class="m2"><p>هرچه داده ست بدو، در خور او، وز در اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهتر محتشمانست بحشمت نه بزاد</p></div>
<div class="m2"><p>از همه محتشمان هر که بود کهتر اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که از چاکری و خدمت او رنج برد</p></div>
<div class="m2"><p>رنج نادیده جهان چاکر و خدمتگر اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چاکری کردن او در شرف از میری به</p></div>
<div class="m2"><p>ورنه چون چشم همه میران بر چاکر اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دشمنی کردن با مرد چنو بیخردیست</p></div>
<div class="m2"><p>خرد دشمن او در سخن مضمر اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دشمن خواجه ببال و پر مغرور مباد</p></div>
<div class="m2"><p>که هلاک و اجل مورچه بال و پر اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر مخالف که بدو قصد کند نیست شود</p></div>
<div class="m2"><p>ور مثل سعد فلکها همه از اختر اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آتشی دان تو خلافش را در سوزش و تف</p></div>
<div class="m2"><p>که مثل چرخ اثیر از تف خاکستر اوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مهر فرزندی بر خواجه فکنده ست جهان</p></div>
<div class="m2"><p>زانکه چون مادرانده خوروانده براوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دشمن ار مهر طمع دارد ازو بیهدگیست</p></div>
<div class="m2"><p>که جهان مادر او نیست که مادندر اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کس در این گیتی با دشمن او دوست مباد</p></div>
<div class="m2"><p>کاژدهاییست جهان دشمن خواجه خور اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او کریمیست عطا بخش و کریمی که مدام</p></div>
<div class="m2"><p>روزی خلق بدان دست ولی پرور اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل او وقت عطا دادن بحریست فراخ</p></div>
<div class="m2"><p>که مه زود رو اندر طلب معبر اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نتوان گفت که دریای دمان را دگرست</p></div>
<div class="m2"><p>نتوان گفت که درهای دگر جز در اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از کریمی دل او سیر شود هرگز نه</p></div>
<div class="m2"><p>این سرشتیست که در خلقت و در گوهر اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دست او همچو درختیست که چشم همه خلق</p></div>
<div class="m2"><p>ببهار و بخزان برگل و برگ و بر اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برتن هیچکس از هیچ ستمگر نبود</p></div>
<div class="m2"><p>آن ستم کز کف بخشنده او برزر اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر بکف گیرد ساغر بخروش آید زر</p></div>
<div class="m2"><p>آن خروش از کف او ناید کز ساغر اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر چه در گیتی از معنی خواهند گیست</p></div>
<div class="m2"><p>نام او با صلت نیکو در دفتراوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این عطا دادن دایم خوی پیغمبر ماست</p></div>
<div class="m2"><p>ای خنک آنکس کورا خوی پیغمبر اوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سببی باید تا فخر توان کرد بدان</p></div>
<div class="m2"><p>رادی و فخرو بزرگی سبب مفخر اوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مخبری باید بر منظر پاکیزه گواه</p></div>
<div class="m2"><p>مخبری در خور منظر بجهان مخبر اوست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه خوبی و نکویی بود او را ز خدای</p></div>
<div class="m2"><p>وین رهی را که ستایشگر و مدحتگر اوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عید او فرخ و او شاد بفرخنده بتی</p></div>
<div class="m2"><p>که گه استاده می اندر کف و گه در بر اوست</p></div></div>