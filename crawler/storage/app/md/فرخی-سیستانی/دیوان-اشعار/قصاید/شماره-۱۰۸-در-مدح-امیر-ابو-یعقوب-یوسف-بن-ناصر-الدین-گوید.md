---
title: >-
    شمارهٔ ۱۰۸ - در مدح امیر ابو یعقوب یوسف بن ناصر الدین گوید
---
# شمارهٔ ۱۰۸ - در مدح امیر ابو یعقوب یوسف بن ناصر الدین گوید

<div class="b" id="bn1"><div class="m1"><p>همی بنفشه دمد گرد روی آن سرهنگ</p></div>
<div class="m2"><p>همی به آینه چینی اندر آید زنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن بنفشه که زیر دو زلف دوست دمید</p></div>
<div class="m2"><p>بسی نماند که بر لاله جای گردد تنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بنفشه فروشی همی بخواهم کرد</p></div>
<div class="m2"><p>مرا بنفشه بسنده ست زلف آن سرهنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فری دو زلف سیه رنگ او چو چفته دو زاغ</p></div>
<div class="m2"><p>بر آفتاب و دو گل هر یکی گرفته بچنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بت پرستی بر مانوی ملامت نیست</p></div>
<div class="m2"><p>اگر چو صورت او صورتیست درارتنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمانکشیست بتم با دو گونه تیر بر او</p></div>
<div class="m2"><p>وز آن دو گونه همی دل خلد به صلح و به جنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوقت صلح دل من خلد به تیر مژه</p></div>
<div class="m2"><p>بوقت جنگ دل دشمنان به تیر خدنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تیر مژگان ز آهن فرو چکاند خون</p></div>
<div class="m2"><p>چنانکه میر به پولاد سنگ از دل سنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امیر سید یوسف برادر سلطان</p></div>
<div class="m2"><p>در سخا و سر فضل و مایه فرهنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برادر ملکی کز همه ملوک چنو</p></div>
<div class="m2"><p>سپه نبرد کسی بیست روزه آن سوی گنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشیده خنجر جودش ز روی زفتی پوست</p></div>
<div class="m2"><p>ز دوده بخشش دستش ز روی رادی زنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر خزینه او بار جود او کشدی</p></div>
<div class="m2"><p>درم به توده بما بخشدی و ز ربا تنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خزینه های پر از بس درم چو پروین پر</p></div>
<div class="m2"><p>همی پراکند از بس عطا چو هفت اورنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسی نماند که شاه جهان برادر او</p></div>
<div class="m2"><p>سر علامت او بگذراند از خر چنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هنوز باش هم آخر چنان شود که سزاست</p></div>
<div class="m2"><p>همی کشند بر اسب مرادش اینک تنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ایا بر آنسوی گنگ و بر آنسوی تبت</p></div>
<div class="m2"><p>ز کرگ شاخ بون کرده و ز شیران چنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر آن سپاه که تو پیش او بجنگ شوی</p></div>
<div class="m2"><p>در آن سپاه نماند مه سپه را رنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان رمند ز آوای تو سران سپاه</p></div>
<div class="m2"><p>که مرغ آبی ز آوای طبل و وحش از زنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بباد حمله بهم بر زنی مصاف عدو</p></div>
<div class="m2"><p>چنانکه باز بهم بر زند صفوف کلنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شجاعت از هنر و بازوی تو گیرد نام</p></div>
<div class="m2"><p>مروت از سیر و همت تو گیرد هنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به تیر پاره کنی درقه های پهلوی کرگ</p></div>
<div class="m2"><p>بنیزه حلقه کنی غیبه های پشت پلنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تراک دل شنود خصم تو ز سینه خویش</p></div>
<div class="m2"><p>چو از کمان تو آید بگوش خصم ترنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز باز تو بهراسد میان ابر عقاب</p></div>
<div class="m2"><p>ز یوز تو برمد برشخ بلند پلنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بروز بزم کند خوی تو ز حنظل شهد</p></div>
<div class="m2"><p>بروز رزم کند خشم تو ز شهد شرنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سخنوران ز سخن پیش تو فرو مانند</p></div>
<div class="m2"><p>چنان کسیکه به پیمانه خورده باشد بنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ترازوی صلت زایرانت را ملکا!</p></div>
<div class="m2"><p>کم از هزار ندارد خزانه دارت سنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بوقت آنکه صلتها دهی موالی را</p></div>
<div class="m2"><p>ز یک دو صلت این خسروانت آید ننگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بس شتاب که جود تو بر خزینه کند</p></div>
<div class="m2"><p>درم همی نکند در خزانه تو درنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه تا چو شود بوستان ز فاخته فرد</p></div>
<div class="m2"><p>ز دشت زاغ سوی بوستان کند آهنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه تا چو شود شاخ گل چو چوگان سست</p></div>
<div class="m2"><p>چو گوی زرین گردد ببار بر نارنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نشستگاه توبر تخت خسروانی باد</p></div>
<div class="m2"><p>نشستگاه عدوی تو در چه ارژنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نصیب دشمن تو ویل و وای و ناله زار</p></div>
<div class="m2"><p>نصیب تو طرب و خرمی و ناله چنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همیشه همچو کنون شاد باد و گلگون باد</p></div>
<div class="m2"><p>دل تو از طرب و دو کف از نبید چو زنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خجسته بادت عید ای خجسته پی ملکی</p></div>
<div class="m2"><p>که با سیاست سامی و باهش هوشنگ</p></div></div>