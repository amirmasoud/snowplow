---
title: >-
    شمارهٔ ۱۷۴ - در توصیف شکار سلطان گوید
---
# شمارهٔ ۱۷۴ - در توصیف شکار سلطان گوید

<div class="b" id="bn1"><div class="m1"><p>اندر این هفته شکاری کرد کز اخبار آن</p></div>
<div class="m2"><p>قصر بر قیصر قفس شد، خانه بر خان آشیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زمین ساکن شد اندر کشوری رامش فزود</p></div>
<div class="m2"><p>چون فلک برگشت گرد کشوری رامش کنان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه ترنجی در بنان و گه کمانی بر کتف</p></div>
<div class="m2"><p>گاه زو بینی به دست و گاه رطلی بر دهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تازیان گرد حصاری قافله در قافله</p></div>
<div class="m2"><p>بختیان گرد شکاری کاروان در کاروان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرکنون جوید عقاب از پشت آن کهسار گوشت</p></div>
<div class="m2"><p>ور کنون جوید همای از روی آن دشت استخوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بینداز بس چشم نخجیر و بناگوش تذرود</p></div>
<div class="m2"><p>دشتها پر نرگس و کهپایه ها پر ناردان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان نکرد آهنگ شیر شرزه از بیم سنانش</p></div>
<div class="m2"><p>رخنه گشتی چرخ و جستی برج شیر از آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیکبختان را پناهی نیکبختی را سبب</p></div>
<div class="m2"><p>پادشاهان را ملاذی پادشاهی را روان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیزی شمشیر دینی سبزی باغ امید</p></div>
<div class="m2"><p>قوت بازوی عدلی سرخی روی امان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خشمت اندر سوز خصم و نهیت اندر شر خلق</p></div>
<div class="m2"><p>فتنه آتش کشست و آتش فتنه نشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نگشتی شادمان از رنگ روی دشمنت</p></div>
<div class="m2"><p>کس ندانستی که باشد شادیی در زعفران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ثنا نقصان عیبی و کمال آفرین</p></div>
<div class="m2"><p>در سخا سود امیدی و زیان سو زیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنچه من دیدم درین تحویل سال ازجود تو</p></div>
<div class="m2"><p>نی بهار از ابر دیده ست و نه از خورشید کان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناگهان در عیش پیوستی و پیوندی ابد</p></div>
<div class="m2"><p>شادمان درمی نشستی و نشینی جاودان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برسرشاهان نهادی تا جهای پر گهر</p></div>
<div class="m2"><p>بر میان خسروان بستی کمرهای گران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسمان دیبا سلب گشت و هوا عنبر غبار</p></div>
<div class="m2"><p>گلستان زرین درخت و آدمی سیمین مکان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچ می بر دست ننهادی که ننهادی ز دست</p></div>
<div class="m2"><p>آنچه زو شد تاقیامت خسروی با نام و نان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از ثریا منتقش گشت این بزرگی تاثری</p></div>
<div class="m2"><p>وز سر اندیب این حکایت گفته شد تا قیروان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داستان پادشاهان خوانده ام ای پادشاه</p></div>
<div class="m2"><p>کس بدین بخشش نبوده ست از جهان همداستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همچنین در تاج داری و جهانداری بپای</p></div>
<div class="m2"><p>همچنین در ملک بخشی و جهانگیری بمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نابریده عشرت عید تو از تحویل سال</p></div>
<div class="m2"><p>ناگسسته بزم نوروزت ز جشن مهرگان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دشمنت زیر زمین و اخترت زیر مراد</p></div>
<div class="m2"><p>عالمت زیر نگین و دولتت زیر عنان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیش عکس تاج تو شمع هوا گوهر پرست</p></div>
<div class="m2"><p>زیر پایه دست تو دست سپهر اختر فشان</p></div></div>