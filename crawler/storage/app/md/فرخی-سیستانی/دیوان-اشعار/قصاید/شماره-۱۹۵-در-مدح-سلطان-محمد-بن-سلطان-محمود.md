---
title: >-
    شمارهٔ ۱۹۵ - در مدح سلطان محمد بن سلطان محمود
---
# شمارهٔ ۱۹۵ - در مدح سلطان محمد بن سلطان محمود

<div class="b" id="bn1"><div class="m1"><p>ای دوست به صدگونه بگردی به زمانی</p></div>
<div class="m2"><p>گه خوش سخنی گیری و گه تلخ زبانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ناز کنی ناز ترا نیست قیاسی</p></div>
<div class="m2"><p>چون خشم کنی خشم ترا نیست کرانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانند میان تو و همچون دهن تو</p></div>
<div class="m2"><p>من تن کنم از موی و دل از غالیه دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویم ز دل خویش دهانت کنم ای ماه</p></div>
<div class="m2"><p>گویی نتوان کردز یک نقطه دهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویم ز تن خویش میان سازمت ای دوست</p></div>
<div class="m2"><p>گویی نتوان ساخت ز یک موی میانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانیست مراجان پدر جز دل و جزتن</p></div>
<div class="m2"><p>وین نیز بر من نکند صبر زمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گویی بفرست نگویم نفرستم</p></div>
<div class="m2"><p>با دوست بخیلی نتوان کرد به جانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانی بدهم تا به زیانی ز تو برهم</p></div>
<div class="m2"><p>من سود کنم گر ز تو برهم به زیانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان بدهم و دل ندهم کاندر دل من هست</p></div>
<div class="m2"><p>مدح ملکی مال دهی شکر ستانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهزاده محمد ملک عالم عادل</p></div>
<div class="m2"><p>کز شاکر او نیست تهی هیچ مکانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا او به امارت بنشست از پی گنجش</p></div>
<div class="m2"><p>هر روز به کوه از زر بفزاید کانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیتی چو یکی کالبدست او چو روانست</p></div>
<div class="m2"><p>چاره نبود کالبدی را ز روانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کافیتر ازو دهر نپرورده امیری</p></div>
<div class="m2"><p>وافیتر ازو ملک ندیده ست جوانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او را ز پی فال پدر تخت فرستاد</p></div>
<div class="m2"><p>تختی همه پر صورت و پر صنعت مانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با تخت فرستاد یکی پیل چو کوهی</p></div>
<div class="m2"><p>پیلی که بر او شیفته گشته ست جهانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مر دولت را برتر ازین نیست دلیلی</p></div>
<div class="m2"><p>مر شاهی را برتر ازین نیست نشانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنچیز کزین پیش گمان بود یقین گشت</p></div>
<div class="m2"><p>دانی نتوان داد یقینی به گمانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن چیز کزین پیش خبر بود عیان گشت</p></div>
<div class="m2"><p>دانی که نگیرد خبری جای عیانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آب و شرف و عز جهان روز بهان راست</p></div>
<div class="m2"><p>نا روز بهان جمله نیرزند به نانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بخشش او خالی کم یابم دستی</p></div>
<div class="m2"><p>وزنعمت او خالی کم یابم خوانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بابخشش او بحر چه چیزست: سرابی</p></div>
<div class="m2"><p>باهمت او چرخ چه چیزست: کیانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>او را ز جفا دهر امان داد و نداده ست</p></div>
<div class="m2"><p>مر هیچ شهی را ز جفا دهر امانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با او به وفا ملک ضمان کرد و نکرده ست</p></div>
<div class="m2"><p>با هیچ ملک ملک بدینگونه ضمانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای بار خدایی که کجا رای تو باشد</p></div>
<div class="m2"><p>خورشید درخشنده نماید چو دخانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زیر سخن خوب تو صد نکته نهانست</p></div>
<div class="m2"><p>زان هر نکتی راست دگرگونه بیانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فضل تو همی جوید هر فضل ستایی</p></div>
<div class="m2"><p>مدح تو همی خواند هر مدحت خوانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر چند نهان همه خلق ایزد داند</p></div>
<div class="m2"><p>از خاطر تو نیست نهان هیچ نهانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیکان تو مانند ستاره ست که نونو</p></div>
<div class="m2"><p>هر روز کند بر دل خصم تو قرانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اندر دل هر شیر ز قربان تو تیریست</p></div>
<div class="m2"><p>وندر برهر گرد ز رمح تو سنانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون تیر و کمان خواستی اندر صف دشمن</p></div>
<div class="m2"><p>انگشت کسی برد نیاردبه کمانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون تیغ به کف گیری هر جای بجویی</p></div>
<div class="m2"><p>ا ز کشته و از خسته نگونی و ستانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا گیتی راست به هر فصلی طبعی</p></div>
<div class="m2"><p>تا ایزد راست به هر روزی شانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شاه ملکان باش و خداوند جهان باش</p></div>
<div class="m2"><p>بگشای جهان را ز کرانی به کرانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در خدمت تو هر چه به ترکستان ماهی</p></div>
<div class="m2"><p>زیر علمت هر چه در آفاق میانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دایم دل تو شاد به دیدار نگاری</p></div>
<div class="m2"><p>شیرین سخنی نوش لبی لاله رخانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چشم من و آن روز که بینم لب دجله</p></div>
<div class="m2"><p>از رنگ علمهای تو چون لاله ستانی</p></div></div>