---
title: >-
    شمارهٔ ۳۵ - در ذکر سفر سومنات و فتح آنجا و شکستن منات و رجعت سلطان گوید
---
# شمارهٔ ۳۵ - در ذکر سفر سومنات و فتح آنجا و شکستن منات و رجعت سلطان گوید

<div class="b" id="bn1"><div class="m1"><p>فسانه گشت و کهن شد حدیث اسکندر</p></div>
<div class="m2"><p>سخن نوآر که نو را حلاوتیست دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فسانه کهن و کارنامه بدروغ</p></div>
<div class="m2"><p>بکار ناید رو در دروغ رنج مبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث آنکه سکندرکجا رسید و چه کرد</p></div>
<div class="m2"><p>ز بس شنیدن گشته ست خلق را ازبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنیده ام که حدیثی که آن دوباره شود</p></div>
<div class="m2"><p>چوصبرگردد تلخ ،ار چه خوش بودچو شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر حدیث خوش و دلپذیر خواهی کرد</p></div>
<div class="m2"><p>حدیث شاه جهان پیش گیرو زین مگذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یمین دولت محمود شهریار جهان</p></div>
<div class="m2"><p>خدایگان نکو منظر و نکو مخبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهی که روز و شب او را جز این تمنانیست</p></div>
<div class="m2"><p>که چون زند بت و بتخانه بر سر بتگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی ز جیحون لشکر کشد سوی سیحون</p></div>
<div class="m2"><p>گهی سپه برد از باختر سوی خاور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کارنامه او گر دو داستان خوانی</p></div>
<div class="m2"><p>بخنده یاد کنی کارهای اسکندر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلی سکندر سرتاسر جهان را گشت</p></div>
<div class="m2"><p>سفر گزید و بیابان برید و کوه و کمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولیکن اوزسفر آب زندگانی جست</p></div>
<div class="m2"><p>ملک، رضای خدا و رضای پیغمبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و گر تو گویی در شأنش آیتست رواست</p></div>
<div class="m2"><p>نیم من این را منکر که باشد آن منکر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوقت آنکه سکندر همی امارت کرد</p></div>
<div class="m2"><p>نبد نبوت را بر نهاده قفل بدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوقت شاه جهان گر پیمبری بودی</p></div>
<div class="m2"><p>دویست آیت بودی بشأن شاه اندر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه حدیث سکندر بدان بزرگ شده ست</p></div>
<div class="m2"><p>که دل بشغل سفر بست و دوست داشت سفر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر سکندر با شاه یک سفر کردی</p></div>
<div class="m2"><p>ز اسب تازی زود آمدی فرودبه خر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درازتر سفر او بدان رهی بوده ست</p></div>
<div class="m2"><p>که ده زده نگسسته ست و کردر از کردر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملک سپاه براهی برد که دیو درو</p></div>
<div class="m2"><p>شمیده گردد و گمراه و عاجز و مضطر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین سفر که شه امسال کرد، در همه عمر</p></div>
<div class="m2"><p>خدای داند کو را نیامده ست بسر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گمان که برد که هرگز کسی ز راه طراز</p></div>
<div class="m2"><p>بسومنات بود لشکر و چنین لشکر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه لشکری که مر آن را کسی بداند حد</p></div>
<div class="m2"><p>نه لشکری که مر آنراکسی بداند مر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شمار لختی از آن بر تر از شمار حصی</p></div>
<div class="m2"><p>عداد برخی از آن برتر از عداد مطر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بلشکر گشن و بیکران نظر چه کنی</p></div>
<div class="m2"><p>تودوری ره صعب و کمی آب نگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رهی که دیو درو گم شدی بوقت زوال</p></div>
<div class="m2"><p>چومرد کم بین در تنگ بیشه وقت سحر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درازتر ز غم مستمند سوخته دل</p></div>
<div class="m2"><p>کشیده تر ز شب دردمند خسته جگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بصد پی اندر، ده جای ریگ چون سرمه</p></div>
<div class="m2"><p>بده پی اندر، صد جای سنگ چون نشتر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چوچشم شوخ همه چشمه های او بی آب</p></div>
<div class="m2"><p>چو قول سفله همی کشتهای او بی بر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هوای او دژم وباد او چو دود جحیم</p></div>
<div class="m2"><p>زمین او سیه و خاک او چوخاکستر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه درخت و میان درخت خار کشن</p></div>
<div class="m2"><p>نه خار بلکه سنان خلنده و خنجر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه مرد را سر آن کاندر آن نهادی پی</p></div>
<div class="m2"><p>نه مرغ رادل آن کاندر آن گشادی پر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همی ز جوشن برکند غیبه جوشن</p></div>
<div class="m2"><p>همی ز مغفر بگسست رفرف مغفر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سوار با سر اندر شدی بدو و ازو</p></div>
<div class="m2"><p>برون شدی همه تن چون هزار پای بسر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هزار خار شکسته درو و خسته ازو</p></div>
<div class="m2"><p>بچند جای سرو روی و پشت و پهلو و بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کمر کشان سپه را جدا جدا هر روز</p></div>
<div class="m2"><p>کمر برهنه بمنزل شدی ز حلیه زر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو پای باز در آن بیشه پر جلاجل بود</p></div>
<div class="m2"><p>ستاکهای درخت از پشیزهای کمر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گهی گیاهی پیش آمدی چو نوک خدنگ</p></div>
<div class="m2"><p>گهی زمینی پیش آمدی چو روی تبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در آن بیابان منزلگهی عجایب بود</p></div>
<div class="m2"><p>که گر بگویم کس را نیاید آن باور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگونه شب، روزی برآمد ازسر کوه</p></div>
<div class="m2"><p>که هیچگونه بر آن کارگر نگشت بصر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نماز پیشین انگشت خویش رابردست</p></div>
<div class="m2"><p>همی ندیدم من این عجایبست و عبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عجب تر آنکه ملک را چنین همی گفتند</p></div>
<div class="m2"><p>که اندرین ره مار دو سر بود بیمر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ترا بزرگ سپاهیست وین دراز رهیست</p></div>
<div class="m2"><p>همه سراسر پر خار و مار و لوره و جر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بشب چو خفته بود مرد سر برآرد مار</p></div>
<div class="m2"><p>همی کشد بنفس خفته تا برآید خور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چوخور برآید و گرمی بمرد خفته رسد</p></div>
<div class="m2"><p>سبک نگردد زان خواب تا گه محشر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خدایگان جهان زان سخن نیندیشید</p></div>
<div class="m2"><p>سپه براند بیاری ایزد داور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدین درشتی و زشتی رهی که کردم یاد</p></div>
<div class="m2"><p>گذاره کرد بتوفیق خالق اکبر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پیادگان را یک یک بخواند و اشتر داد</p></div>
<div class="m2"><p>بتوشه کرد سفر بر مسافران چو حضر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جمازه ها را در بادیه دمادم کرد</p></div>
<div class="m2"><p>بآب کرد همه ریگ آن بیابان تر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بساخت از پی پس ماندگان و گمشدگان</p></div>
<div class="m2"><p>میان بادیه ها حوضهای چون کوثر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همه سپه را زان بادیه برون آورد</p></div>
<div class="m2"><p>شکفته چون گل سیراب وهمچو نیلوفر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدان ره اندر چندین حصار و شهر بزرگ</p></div>
<div class="m2"><p>خراب کرد و بکند اصل هر یک از بن و بر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نخست لدروه کز روی برج وباره آن</p></div>
<div class="m2"><p>چو کوه کوه فروریخت آهن و مرمر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>حصار او قوی و باره حصار قوی</p></div>
<div class="m2"><p>حصاریان همه برسان شیر شرزه نر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مبارزانی همدست و لشکری همپشت</p></div>
<div class="m2"><p>درنگ پیشه به فر و شتاب کار به کر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نبرد کرده و اندر نبرد یافته دست</p></div>
<div class="m2"><p>دلیر گشته و اندر دلیری استمگر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو چیکودر که چه صندوقهای گوهر یافت</p></div>
<div class="m2"><p>بکوه پایه او شهریار شیر شکر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو کوه البرز، آن کوه کاندرو سیمرغ</p></div>
<div class="m2"><p>گرفت مسکن و بازال شد سخن گستر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چگونه کوهی چونانکه از بلندی آن</p></div>
<div class="m2"><p>ستارگان را گویی فرود اوست مقر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مبارزانی بر تیغ او بتیغ گذاشت</p></div>
<div class="m2"><p>که هر یکی را صد بنده بود چون عنتر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو نهر واله که اندر دیار هند بهیم</p></div>
<div class="m2"><p>به نهر واله همی کرد بر شهان مفخر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بزرگ شهری ودرشهر کاخهای بزرگ</p></div>
<div class="m2"><p>رسیده کنگره کاخها به دو پیکر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدخل نیک و بتربت خوش و بآب تمام</p></div>
<div class="m2"><p>به کشتمند وبباغ و ببوستان برور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دویست پیل دمان پیش وده هزار سوار</p></div>
<div class="m2"><p>نود هزار پیاده مبارز و صفدر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همیشه رای بهیم اندرو مقیم بدی</p></div>
<div class="m2"><p>نشسته ایمن و دل پر نشاط و ناز و بطر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چومندهیر که در مندهیر حوضی بود</p></div>
<div class="m2"><p>چنانکه خیره شدی اندرو دو چشم فکر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چگونه حوضی چونانکه هر چه بندیشم</p></div>
<div class="m2"><p>همی ندانم گفتن صفاتش اندر خور</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز دستبرد حکیمان برو پدید نشان</p></div>
<div class="m2"><p>زمال های فراوان برو پدید اثر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>فرات پهنا حوضی بصد هزار عمل</p></div>
<div class="m2"><p>هزار بتکده خرد گرد حوض اندر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بزرگ بتکده ای پیش و درمیانش بتی</p></div>
<div class="m2"><p>بحسن ماه ولیکن بقامت عرعر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دگر چو دیو لواره که همچو دیو سپید</p></div>
<div class="m2"><p>پدید بود سر افراشته میان گذر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>درو درختان چون گوز هندی و پوپل</p></div>
<div class="m2"><p>که هر درخت بسالی دهد مکرر بر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یکی حصار قوی بر کران شهر و درو</p></div>
<div class="m2"><p>ز بت پرستان گرد آمده یکی معشر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بکشت مردم و بتخانه ها بکندو بسوخت</p></div>
<div class="m2"><p>چنانکه بتکده دارنی و تانیسر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نرست ازو بره اندر مگر کسی که بماند</p></div>
<div class="m2"><p>نهفته زیر خسی چون بهیم شوم اختر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نهفتگانرا ناجسته زان قبل بگذاشت</p></div>
<div class="m2"><p>که شغل داشت جز آن، آن شه فریشته فر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کسیکه بتکده سومنات خواهد کند</p></div>
<div class="m2"><p>به جستگان نکند روزگار خویش هدر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ملک همی بتبه کردن منات شتافت</p></div>
<div class="m2"><p>شتاب او هم ازین روی بوده بود مگر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>منات و لات و عزی در مکه سه بت بودند</p></div>
<div class="m2"><p>ز دستبرد بت آرای آن زمان آزر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همه جهان همی آن هر سه بت پرستیدند</p></div>
<div class="m2"><p>جز آن کسی که بدو بود از خدای نظر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دو زان پیمبر بشکست و هر دو را آنروز</p></div>
<div class="m2"><p>فکنده بود ستان پیش کعبه پای سپر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>منات را ز میان کافران بدزدیدند</p></div>
<div class="m2"><p>بکشوری دگر انداختند از آن کشور</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بجایگاهی کز روزگار آدم باز</p></div>
<div class="m2"><p>بر آن زمین ننشست و نرفت جز کافر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز بهر آن بت، بتخانه ای بنا کردند</p></div>
<div class="m2"><p>بصد هزار تماثیل و صد هزار صور</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بکار بردند از هر سویی تقرب را</p></div>
<div class="m2"><p>چو تخته سنگ بر آن خانه ، تخته تخته زر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به بتکده در، بت را خزینه ای کردند</p></div>
<div class="m2"><p>در آن خزینه بصندوقهای پیل، گهر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گهر خریدند او رابشهرها چندان</p></div>
<div class="m2"><p>که سیر گشت ز گوهر فروش، گوهر خر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>برابر سر بت کله ای فرو هشتند</p></div>
<div class="m2"><p>نگار کار به یاقوت و بافته به درر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز زر پخته یکی جود ساختند او را</p></div>
<div class="m2"><p>چو کوه آتش و گوهر برو بجای شرر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>خراج مملکتی تاج و افسرش بوده ست</p></div>
<div class="m2"><p>کمینه چیز وی آن تاج بود و آن افسر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>پس آنگه آنرا کردند سومنات لقب</p></div>
<div class="m2"><p>لقب که دید که نام اندرو بود مضمر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>خبر فکندند اندر جهان که از دریا</p></div>
<div class="m2"><p>بتی برآمد زینگونه و بدین پیکر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مدبر همه خلقست و کردگار جهان</p></div>
<div class="m2"><p>ضیا دهنده شمسست و نور بخش قمر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بعلم این بود اندر جهان صلاح و فساد</p></div>
<div class="m2"><p>بحکم این رود اندر جهان قضا و قدر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گروه دیگر گفتند، نی که این بت را</p></div>
<div class="m2"><p>برآسمان برین بود جایگاه و مقر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کسی نیاورد این را بدین مقام که این</p></div>
<div class="m2"><p>ز آسمان بخودی خودآمده ست ایدر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بدین بگوید روز و بدان بگوید شب</p></div>
<div class="m2"><p>بدین بگوید بحر و بدان بگوید بر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو این ز دریا سر برزد و بخشک آمد</p></div>
<div class="m2"><p>سجود کردنداین راهمه نبات و شجر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به شیر خویش مر اورا بشست گاو و کنون</p></div>
<div class="m2"><p>بدین تقرب خوانند گاو را مادر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ز بهر سنگی چندین هزارخلق خدای</p></div>
<div class="m2"><p>بقول دیو فرو هشته بر خطر لنگر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>فریضه هر روز آن سنگ را بشستندی</p></div>
<div class="m2"><p>به آب گنگ و به شیر و به زعفران و شکر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز بهر شستن آب بت ز گنگ هر روزی</p></div>
<div class="m2"><p>دو جام آب رسیدی فزون زده ساغر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>از آب گنگ چه گویم که چندفرسنگست</p></div>
<div class="m2"><p>به سومنات بدانجایگاه زلت و شر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گه گرفتن خور صد هزار کودک و مرد</p></div>
<div class="m2"><p>بدو شدندی فریاد خواه و پوزش گر</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ز کافران که شدندی به سومنات به حج</p></div>
<div class="m2"><p>همی گسسته نگشتی بره نفر ز نفر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خدای خوانند آن سنگ را همی شمنان</p></div>
<div class="m2"><p>چه بیهده ست سخنست این که خاکشان بر سر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>خدای حکم چنان کرده بودکان بت را</p></div>
<div class="m2"><p>زجای برکندآن شهریار دین پرور</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بدان نیت که مر او را بمکه باز برد</p></div>
<div class="m2"><p>بکند واینک با ماهمی برد همبر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو بت بکند از آنجا و مال و زر برداشت</p></div>
<div class="m2"><p>بدست خویش به بتخانه در فکند آذر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>برهمنان را چندانکه دید سر برید</p></div>
<div class="m2"><p>بریده به، سر آن کز هدی بتابد سر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ز خون کشته کز آن بتکده بدریا راند</p></div>
<div class="m2"><p>چو سرخ لاله شد، آبی چوسبز سیسنبر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز بت پرستان چندان بکشت و چندان بست</p></div>
<div class="m2"><p>که کشته بود و گرفته ز خانیان به کتر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>خدای داند کآنجا چه مایه مردم بود</p></div>
<div class="m2"><p>همه در آرزوی جنگ و جنگ را از در</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>میان بتکده استاده سلیح بچنگ</p></div>
<div class="m2"><p>چو روز جنگ میان مصاف، رستم زر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>خدنگ ترکی بر روی و سر همی خوردند</p></div>
<div class="m2"><p>همی نیامد بر رویشان پدید غیر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بجنگ جلدی کردند، لیکن آخر کار</p></div>
<div class="m2"><p>بتیر سلطان بردند عمر خویش بسر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>خدایگان را اندر جهان دو حاجت بود</p></div>
<div class="m2"><p>همیشه این دو همی خواست زایزد داور</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>یکی که جایگه حج هندوان بکند</p></div>
<div class="m2"><p>دگر که حج کند و بوسه بر دهد بحجر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>یکی از آن دو مراد بزرگ حاصل کرد</p></div>
<div class="m2"><p>دگر بعون خدای بزرگ کرده شمر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>خراب کردن بتخانه خرد کار نبود</p></div>
<div class="m2"><p>بدانچه کرده بیابد ملک ثواب و ثمر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چودل ز سوختن سومنات فارغ کرد</p></div>
<div class="m2"><p>گرفت راه بدر باز رفتگان دگر</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>خمی ز گردش دریا براه پیش آمد</p></div>
<div class="m2"><p>گسسته شد ز ره امید مردمان یکسر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نبود رهبر کان خلق را بجستی راه</p></div>
<div class="m2"><p>نبود ممکن کان آب را کنند عبر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>سوی درازا یکماه راه ویران بود</p></div>
<div class="m2"><p>رهی بصعبی و زشتی در آن دیار سمر</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>ز سوی پهنا چندانکه کشتیی دو سه روز</p></div>
<div class="m2"><p>همی رود چو رود مرغ گرسنه سوی خور</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>درون دریا مد آمدی بروز دو بار</p></div>
<div class="m2"><p>چنانکه چرخ زدی اندر آب او چنبر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چو مد باز شدی برکرانش صیادان</p></div>
<div class="m2"><p>فرو شدندی وکردندی از میانه حذر</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ملک چو حال چنان دید خلق را دل داد</p></div>
<div class="m2"><p>براند و گفت که این مایه آبرا چه خطر</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>امید خویش بایزد فکند و پیش سپاه</p></div>
<div class="m2"><p>فکند باره فرخنده پی بآب اندر</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بفال نیک، شه پر دل آب را بگذاشت</p></div>
<div class="m2"><p>روان شدند همه از پی شه آن لشکر</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بر آمدند بر آن پی ز آب آن دریا</p></div>
<div class="m2"><p>چنانکه گفتی آن آب بد همی فرغر</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>نه آنکه هیچکسی را بتن رسید آسیب</p></div>
<div class="m2"><p>نه آنکه هیچ کسی را بجان رسید ضرر</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>دو روز و دو شب از آنجا همی سپاه گذشت</p></div>
<div class="m2"><p>که مد نیامد و نگذشت آبش از میزر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>جدا ز مردم بگذشت ز آب آن دریا</p></div>
<div class="m2"><p>بر از دویست هزار اسب و اشتر واستر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بدین طریق زیزدان چنین کرامت یافت</p></div>
<div class="m2"><p>تو این کرامت زاجناس معجزات شمر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>جز اینکه گفتم، چندین غزات دیگر کرد</p></div>
<div class="m2"><p>بباز گشتن سوی مقام عز و مقر</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>حصار کند هه را از بهیم خالی کرد</p></div>
<div class="m2"><p>بهیم را بجهان آن حصار بود مفر</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>قوی حصاری بر تیغ نامدار کهی</p></div>
<div class="m2"><p>میان دشتی سیراب نا شده ز مطر</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>میان سنگ، یکی کنده، کنده گرد حصار</p></div>
<div class="m2"><p>نه زان عمل که بود کار کرد های بشر</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>نه راه یافته خصم اندر آن حصار بجهد</p></div>
<div class="m2"><p>نه زان حصار فرود آمدی یکی بخبر</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>وز آن حصار به منصوره روی کردو براند</p></div>
<div class="m2"><p>بر آن شماره کجا راند حیدر از خیبر</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>خفیف چون خبر خسرو جهان بشنید</p></div>
<div class="m2"><p>دوان گذشت و به جوی اندر اوفتاد و به جر</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>بآب شور و بیابان پر گزند افتاد</p></div>
<div class="m2"><p>بماندش خانه ویران ز طارم وز طزر</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>خفیف را سپه و پیل ومال چندان بود</p></div>
<div class="m2"><p>که بیش از آن نبود در هوا همانا ذر</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>نداشت طاقت سلطان، ز پیش او بگریخت</p></div>
<div class="m2"><p>چنان که زو بگریزند صد هزار دگر</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>نگاه کن که بدین یک سفر که کرد، چه کرد</p></div>
<div class="m2"><p>خدایگان جهان شهریار شیر شکر</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>جهان بگشت و اعادی بکشت و گنج بیافت</p></div>
<div class="m2"><p>بنای کفر بیفکند، اینت فتح و ظفر</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>زهی مظفر فیروز بخت دولت یار</p></div>
<div class="m2"><p>که گوی برده ای از خسروان بفضل و هنر</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>از این هنر که نمودی و ره که پیمودی</p></div>
<div class="m2"><p>شهان غافل سرمست راهمی چه خبر</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>تو برکناره دریای شور خیمه زدی</p></div>
<div class="m2"><p>شهان شراب زده بر کناره های شمر</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>تو سومنات همی سوختی به بهمن ماه</p></div>
<div class="m2"><p>شهان دیگر عود مثلث و عنبر</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>بوقت آنکه همه خلق گرم خواب شوند</p></div>
<div class="m2"><p>تو در شتاب سفر بوده ای و رنج سهر</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>تو آن شهی که ز بهر غزات رایت تو</p></div>
<div class="m2"><p>به سومنات رود گاه وگه به کالنجر</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>خدایگانا زین پس چو رای غزو کنی</p></div>
<div class="m2"><p>ببر سپاه کشن سوی روم و سوی خزر</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>به سند و هند کسی نیست مانده کان ارزد</p></div>
<div class="m2"><p>کز آن تو شود آنجا بجنگ یک چاکر</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>خراب کردی و بیمرد خاندان بهیم</p></div>
<div class="m2"><p>مگر کنی پس از این قصد خانه قیصر</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>سپه کشیدی زین روی تالب دریا</p></div>
<div class="m2"><p>بجایگاهی کز آدمی نبود اثر</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بما نمودی آن چیزها که یاد کنیم</p></div>
<div class="m2"><p>گمان بریم که این در فسانه بود مگر</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>زمین بماند برین روی و آب پیش آمد</p></div>
<div class="m2"><p>بهیچ روی ازین آب نیست روی گذر</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>اگر نه دریا پیش آمدی براه ترا</p></div>
<div class="m2"><p>کنون گذشته بدی از قمار و از بربر</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>ایا بمردی و پیروزی از ملوک پدید</p></div>
<div class="m2"><p>چنان که بود به هنگام مصطفی حیدر</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>شنیده ام که همیشه چنین بود دریا</p></div>
<div class="m2"><p>که بر دو منزل از آواش گوش گردد کر</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>همی نماید هیبت، همی فزاید شور</p></div>
<div class="m2"><p>همی بر آید موجش برابر محور</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>سه بار با تو بدریای بیکرانه شدم</p></div>
<div class="m2"><p>نه موج دیدم و نه هیبت و نه شور ونه شر</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>نخست روز که دریا ترا بدید، بدید</p></div>
<div class="m2"><p>که پیش قدر تو چون ناقصست و چون ابتر</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>بمال با تو نتاند شد، ار بخواهد، جفت</p></div>
<div class="m2"><p>بقدر باتو و نیارد زد، ار بخواهد، بر</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>چو گرد خویش نگه کرد، مارو ماهی دید</p></div>
<div class="m2"><p>بگرد تو مه تابان و زهره ازهر</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>ز تو خلایق راخرمی وشادی بود</p></div>
<div class="m2"><p>وزو همه خطر جان و بیم غرق و غرر</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>چو قدرت تو نگه کرد و عجز خویش بدید</p></div>
<div class="m2"><p>چو آبگینه شد آب اندرو زشرم و حجر</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>ز آب دریا گفتی همی بگوش آمد</p></div>
<div class="m2"><p>که شهریارا دریا تویی و من فرغر</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>همه جهان ز تو عاجز شدند تا دریا</p></div>
<div class="m2"><p>نداشت هیچکس این قدر و منزلت زبشر</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>بزرگوارا کاری که آمد از پدرت</p></div>
<div class="m2"><p>بدولت پدر تو نبود هیچ پدر</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>بملک داری تابود بود و وقت شدن</p></div>
<div class="m2"><p>بماند از و بجهان چون تو یادگار پسر</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>همیشه تا نبود جان چو جسم و عقل چو جهل</p></div>
<div class="m2"><p>همیشه تا نبود دین چو کفر و نفع چو ضر</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>همیشه تا علوی را نسب بودبه علی</p></div>
<div class="m2"><p>همیشه تا عمری را شرف بود به عمر</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>خدایگانی جز مر ترا همی نسزد</p></div>
<div class="m2"><p>خدایگان جهان باش و از جهان برخور</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>جهان و مال جهان سر بسر خنیده تست</p></div>
<div class="m2"><p>بشهریاری و فیروزی از خنیده بچر</p></div></div>