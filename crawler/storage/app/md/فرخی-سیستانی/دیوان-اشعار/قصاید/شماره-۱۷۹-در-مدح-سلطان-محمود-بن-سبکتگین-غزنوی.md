---
title: >-
    شمارهٔ ۱۷۹ - در مدح سلطان محمود بن سبکتگین غزنوی
---
# شمارهٔ ۱۷۹ - در مدح سلطان محمود بن سبکتگین غزنوی

<div class="b" id="bn1"><div class="m1"><p>با من به شابهار به سر برد چاشتگاه</p></div>
<div class="m2"><p>ماه من آنکه رشک برد زود و هفته ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت: این فراخ پهنا دشت گشاده چیست</p></div>
<div class="m2"><p>گفتم: که عرضه گه شه بیعدد سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتا: چه خوانم این شه آزاده را بنام ؟</p></div>
<div class="m2"><p>گفتم:یمین دولت محمود دین پناه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتا: پناه شرع رسولست و پشت دین ؟</p></div>
<div class="m2"><p>گفتم: بلی و پیشرو طاعت اله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتا: کنون کجاست مرا ده نشان ازو ؟</p></div>
<div class="m2"><p>گفتم: که زیر سایه آن رایت سیاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت : آنکه پیش عرضه گهش ایستاده است</p></div>
<div class="m2"><p>گفتم: به پیشگاه بود جای پیشگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتا:ز هیبتش بهر اسد همی دلم</p></div>
<div class="m2"><p>گفتم: زهیبتش دل چون که شود چو کاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت: آن هزار و هفتصد و اند کوه چیست ؟</p></div>
<div class="m2"><p>گفتم: هزارو هفتصد و اند پیل شاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت: آنهمه ز پیشرو هندوان ستد ؟</p></div>
<div class="m2"><p>گفتم: بلی و داشت به مردانگی نگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت : آن زره و ران زبر هر یکی که اند ؟</p></div>
<div class="m2"><p>گفتم: بتان مملکت آرای رزمخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتا: که سرو خوانمشان یا مه تمام ؟</p></div>
<div class="m2"><p>گفتم: که سرو با کمر و ماه با کلاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتا: که عرضه گاه شه این دشت خرمست ؟</p></div>
<div class="m2"><p>گفتم: بلی و نیست چنین هیچ عرضه گاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتا: چنو دگر به جهان هیچ شه بود؟</p></div>
<div class="m2"><p>گفتم: ز من مپرس به شهنامه کن نگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتا: که شاهنامه دروغست سر بسر</p></div>
<div class="m2"><p>گفتم: تو راست گیر و دروغ از میان بکاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتا: ملک به پیلان چه استاند از ملوک ؟</p></div>
<div class="m2"><p>گفتم: ولایت و سپه و گنج و تاج و گاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتا: چرا همی نبردشان بسوی روم ؟</p></div>
<div class="m2"><p>گفتم: کنون برد که کنون آمده ست گاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتا: چگونه گردد از ایشان بلاد روم ؟</p></div>
<div class="m2"><p>گفتم: چنانکه کوه گهردار چاه چاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتا: ز کفر پاک شود شهرهای روم ؟</p></div>
<div class="m2"><p>گفتم: چنانکه سیم نفایه میان گاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتا: که اسب اوبه گه رزم چون بود ؟</p></div>
<div class="m2"><p>گفتم: میان خون اعادی کند شناه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتا: چسان رود چو به رودی و رسد فراز ؟</p></div>
<div class="m2"><p>گفتم: چو مرغ برگذرد بر سرمیاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتا: که برتر ازملکان چون از و گذشت ؟</p></div>
<div class="m2"><p>گفتم: کسی که یابد ازو جاه و پایگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفتا: که خدمتش ملکان را چه بردهد ؟</p></div>
<div class="m2"><p>گفتم: که تخت و مملکت و آبروی و جاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتا: گناهکار که زی وی شود به عذر؟</p></div>
<div class="m2"><p>گفتم: ثواب و خدمت یابد بر آن گناه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتا: زمانه خاضع او باد روز و شب</p></div>
<div class="m2"><p>گفتم: خدای ناصر او باد سال و ماه</p></div></div>