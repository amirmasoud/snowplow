---
title: >-
    شمارهٔ ۳۳ - نیز ازاوست
---
# شمارهٔ ۳۳ - نیز ازاوست

<div class="b" id="bn1"><div class="m1"><p>من بدین بیدلی و دوست بدین سنگدلی</p></div>
<div class="m2"><p>من بدین محتملی یار بدین مستحلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار معشوق من از مستحلی بر نخورد</p></div>
<div class="m2"><p>تا نیاید زمن این بیدلی و محتملی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفریباند هر روز دلم رازسخن</p></div>
<div class="m2"><p>آن سرا پای فریبندگی و مفتعلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از آن ساده دلی بیهده برهر سخنی</p></div>
<div class="m2"><p>پای می کوبم چون گیلان بر نای گلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند گردم بر آن کس که نگردد برمن</p></div>
<div class="m2"><p>چند گویم که مرا تو ز دل و جان بدلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من غزل گویم پیوسته به یادتو غزال</p></div>
<div class="m2"><p>تا تو پیوسته خریدار نوای غزلی</p></div></div>