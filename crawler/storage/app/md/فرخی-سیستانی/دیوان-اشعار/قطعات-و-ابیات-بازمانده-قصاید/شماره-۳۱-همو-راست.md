---
title: >-
    شمارهٔ ۳۱ - همو راست
---
# شمارهٔ ۳۱ - همو راست

<div class="b" id="bn1"><div class="m1"><p>گفتم چو به گرد سمنت سنبل کاری</p></div>
<div class="m2"><p>دعوی ز دلم بگسلی ای ترک حصاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی تو ای ترک فزونتر شد تا تو</p></div>
<div class="m2"><p>گرد سمن تازه همی سنبل کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعوی توزینگونه نبوده ست ونبوده ست</p></div>
<div class="m2"><p>از عشق تو اندر دل من چندین زاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروزهمه حال دگر گشت و بتر گشت</p></div>
<div class="m2"><p>فردا نه عجب باشد اگر زین بتر آری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ترک سمن عارض بودی نه چنین بود</p></div>
<div class="m2"><p>امروز چنین شد که بت مشک عذاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با عارض ساده ز در دیدن بودی</p></div>
<div class="m2"><p>با خط دمیده ز در بوس و کناری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا من بزیم چنگ ز تو باز ندارم</p></div>
<div class="m2"><p>دانم که سه بوسه تو زمن باز نداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان و دل و دین را به کنار تو گذارم</p></div>
<div class="m2"><p>تاتو به کنار خودم از مهر گذاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من با توهمی از در یاری به در آیم</p></div>
<div class="m2"><p>شاید که تو آیی ز درم از در یاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نازاز تو سزد برمن مسکین که توایدون</p></div>
<div class="m2"><p>باطره مشکین وخط غالیه باری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باطره مشکین همگی فتنه چینی</p></div>
<div class="m2"><p>با غالیه گون خط سیه شور تتاری</p></div></div>