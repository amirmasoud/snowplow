---
title: >-
    شمارهٔ ۲۹ - و او راست
---
# شمارهٔ ۲۹ - و او راست

<div class="b" id="bn1"><div class="m1"><p>ای جهانی ز تو به آزادی</p></div>
<div class="m2"><p>بر من از تو چراست بیدادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من دادی و نبود مرا</p></div>
<div class="m2"><p>از دل بیوفای تو شادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل دهان دل به دوستی دادند</p></div>
<div class="m2"><p>تومرا دل به دشمنی دادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصدکردی به دل ربودن من</p></div>
<div class="m2"><p>برهلاک دلم بر استادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دلم نستدی نیاسودی</p></div>
<div class="m2"><p>چون توان کرد از تو آزادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ببردی و جان شد از پس دل</p></div>
<div class="m2"><p>ای تن اندر چه محنت افتادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بردل دوستان فرامشتی</p></div>
<div class="m2"><p>بر دل دشمنان همه یادی</p></div></div>