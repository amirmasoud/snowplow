---
title: >-
    شمارهٔ ۱۵ - همو راست
---
# شمارهٔ ۱۵ - همو راست

<div class="b" id="bn1"><div class="m1"><p>هندوی بد که ترا باشد و زان تو بود</p></div>
<div class="m2"><p>بهتر از ترکی کان تو نباشد، صد بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هندوان شوخک و شیرینک و خوش با نمکند</p></div>
<div class="m2"><p>نیز بی مشغله باشند گه بوس و کنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ترا ترکی سه بوسه دزدیده دهد</p></div>
<div class="m2"><p>هندویی را بتوان برد و بپرداخت ز کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف هندو را بندی بود و تاب دویست</p></div>
<div class="m2"><p>جعد هندو را تابی بود و پیچ هزار</p></div></div>