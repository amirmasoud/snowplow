---
title: >-
    شمارهٔ ۲۳ - و او راست
---
# شمارهٔ ۲۳ - و او راست

<div class="b" id="bn1"><div class="m1"><p>ای رفته من از رفتن تو باغم ودردم</p></div>
<div class="m2"><p>مردم زتو وزین قبل از شادی فردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا وصل ترا هجر تو ای ماه فرو خورد</p></div>
<div class="m2"><p>دردی نشناسم که به صد باره نخوردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چهره تو بتکده بوده ست مرا چشم</p></div>
<div class="m2"><p>امروز درین بتکده از آب به دردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند کز آتش تبش و گرمی باشد</p></div>
<div class="m2"><p>پس چون که من از آتش غم بادم سردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا دوست بگشتی تو از آن حال که بودی</p></div>
<div class="m2"><p>من روزی ازین درد به صدبار بگردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه بامژه ترم گه بالب خشکم</p></div>
<div class="m2"><p>گه با دل پرخونم گه با رخ زردم</p></div></div>