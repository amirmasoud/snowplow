---
title: >-
    شمارهٔ ۱۸ - همو راست
---
# شمارهٔ ۱۸ - همو راست

<div class="b" id="bn1"><div class="m1"><p>بهشت روی منا گر همی روی به سفر</p></div>
<div class="m2"><p>مرا ببر به سفر یا دل مرا تو مبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ز رفتن تو چند گونه درد سرست</p></div>
<div class="m2"><p>وگر چه درد مرا تو همی ندانی سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی که تو زبر من همی روی نه بکام</p></div>
<div class="m2"><p>دگر که با تو دل من همی رود به سفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه باشد حال کسی که دلبر او</p></div>
<div class="m2"><p>همی سفر کند اندر جهان و او به حضر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا و روی به روی من ای صنم بر نه</p></div>
<div class="m2"><p>منه که روی تو بریان کنم ز تف جگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر همی تو روی و دلم همی ببری</p></div>
<div class="m2"><p>برو برآنکه غمت خورد زینهار مخور</p></div></div>