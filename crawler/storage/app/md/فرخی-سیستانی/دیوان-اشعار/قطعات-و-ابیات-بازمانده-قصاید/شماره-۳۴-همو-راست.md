---
title: >-
    شمارهٔ ۳۴ - همو راست
---
# شمارهٔ ۳۴ - همو راست

<div class="b" id="bn1"><div class="m1"><p>بر وعده مرا شکیب فرمایی</p></div>
<div class="m2"><p>تا کی کنم ای صنم شکیبایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر سه بوسه مستمندی را</p></div>
<div class="m2"><p>خواهی که سه سال صبر فرمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راز دل خویش با تو بگشادم</p></div>
<div class="m2"><p>باشدکه بر این مرا ببخشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بربرگ سمن به مشک بنبشتی</p></div>
<div class="m2"><p>تا راز مرا به خلق بنمایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بد مهر بتی و سنگدل یاری</p></div>
<div class="m2"><p>لیکن چو دل و چو دیده دریابی</p></div></div>