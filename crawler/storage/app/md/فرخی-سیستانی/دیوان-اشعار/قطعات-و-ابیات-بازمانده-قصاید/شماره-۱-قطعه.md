---
title: >-
    شمارهٔ ۱ - قطعه
---
# شمارهٔ ۱ - قطعه

<div class="b" id="bn1"><div class="m1"><p>خواستم از لعل او دو بوسه و گفتم</p></div>
<div class="m2"><p>تربیتی کن به آب لطف خسی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت یکی بس بود و گر دو ستانی</p></div>
<div class="m2"><p>فتنه شود آزموده ایم بسی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر دوباره ست بوسه ی من و هرگز</p></div>
<div class="m2"><p>عمر دوباره نداده اند کسی را</p></div></div>