---
title: >-
    شمارهٔ ۲۶ - نیز او راست
---
# شمارهٔ ۲۶ - نیز او راست

<div class="b" id="bn1"><div class="m1"><p>باغبان! زیر سرو بن منشین</p></div>
<div class="m2"><p>نه کجا سرو نیست نیست زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه همه سایه زیر سرو بود</p></div>
<div class="m2"><p>زیر شاخ سمن شو و بنشین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ تو پر درخت سایه ورست</p></div>
<div class="m2"><p>از پی خویشتن یکی بگزین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد آن سرو نارسیده مگرد</p></div>
<div class="m2"><p>رنگ آن سرو نارسیده مبین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو را، دست باز دار به من</p></div>
<div class="m2"><p>رحم کن بردل من مسکین</p></div></div>