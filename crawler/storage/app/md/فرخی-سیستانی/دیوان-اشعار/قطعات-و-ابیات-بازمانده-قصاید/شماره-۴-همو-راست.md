---
title: >-
    شمارهٔ ۴ - همو راست
---
# شمارهٔ ۴ - همو راست

<div class="b" id="bn1"><div class="m1"><p>سیاه چشما! مهر تو غمگسار منست</p></div>
<div class="m2"><p>به روزگار خزان روی تو بهار منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم شکار سیه چشمکان تست و رواست</p></div>
<div class="m2"><p>از آنکه دولب شیرین تو شکار منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مهر تو دل من وام دار صحبت تست</p></div>
<div class="m2"><p>لب تو باز به سه بوسه وامدار منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جفا نمودن بی جرم کار تست مدام</p></div>
<div class="m2"><p>وفا نمودن و اندیشه تو کارمنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تو ماهی، گردون تو سرای منست</p></div>
<div class="m2"><p>اگر تو سروی بستان تو کنار منست</p></div></div>