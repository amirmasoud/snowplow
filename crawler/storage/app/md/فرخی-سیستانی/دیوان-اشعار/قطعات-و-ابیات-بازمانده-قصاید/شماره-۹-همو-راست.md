---
title: >-
    شمارهٔ ۹ - همو راست
---
# شمارهٔ ۹ - همو راست

<div class="b" id="bn1"><div class="m1"><p>همی روی و من از رفتن تو ناخشنود</p></div>
<div class="m2"><p>نگر به روی منا تا مرا کنی پدرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرو که گر بروی باز جان من برود</p></div>
<div class="m2"><p>من از تو ناخشنود و خدای ناخشنود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز رفتن تو وز نهیب فرقت تو</p></div>
<div class="m2"><p>دو چشم چشمه خون گشت و جامه خون آلود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر فراق ترا پیشه زرگری بوده ست</p></div>
<div class="m2"><p>که کرد دو رخ من زرد فام و زر اندود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو رفتی و ز پس رفتن تو از غم تو</p></div>
<div class="m2"><p>خدای داند تا من چگونه خواهم بود</p></div></div>