---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>در غربتم افتاده ز هجران حبیب</p></div>
<div class="m2"><p>از شدت ضعف گشته با مرگ قریب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاری نه که آرد بسر خسته طبیب</p></div>
<div class="m2"><p>زاری نه که جوید کفن از بهر غریب</p></div></div>