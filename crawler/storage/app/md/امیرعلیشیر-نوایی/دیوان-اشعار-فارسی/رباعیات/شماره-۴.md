---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>آمد ز نسیم صبح بوی تو مرا</p></div>
<div class="m2"><p>در روضه نمود جلوه کوی تو مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل دیدم و شد نشان روی تو مرا</p></div>
<div class="m2"><p>معلوم نشد ولیک خوی تو مرا</p></div></div>