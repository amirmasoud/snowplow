---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ساقی بقدح می طربناک انداز</p></div>
<div class="m2"><p>عکس رخ پاک در می پاک انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس شور و شغب در من بی باک انداز</p></div>
<div class="m2"><p>زان غلغله در گنبد افلاک انداز</p></div></div>