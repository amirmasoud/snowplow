---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>از محنت عاشقی به جانم چه کنم؟</p></div>
<div class="m2"><p>دیوانه و رسوای جهانم چه کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبرست مرا چاره و دانم چه کنم؟</p></div>
<div class="m2"><p>دانم چه کنم چون نتوانم چه کنم؟</p></div></div>