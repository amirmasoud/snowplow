---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>در دیر مغان مغبچگان چالاک</p></div>
<div class="m2"><p>کردند مرا به باده مست و بی باک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخموریم افکند چو بر خاک هلاک</p></div>
<div class="m2"><p>وانگه نگرفتند به می خرقه چاک</p></div></div>