---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ضعفم را آن میان چون مو باعث</p></div>
<div class="m2"><p>قتلم را آن طره هندو باعث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرم را آن قامت دلجو باعث</p></div>
<div class="m2"><p>جانم را آن لعل سخنگو باعث</p></div></div>