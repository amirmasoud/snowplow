---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>یاران چو کشند در بهاران می صاف</p></div>
<div class="m2"><p>من گر چه ز زهد و توبه پیش آرم لاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارندم اگر ز می بدان هرزه معاف</p></div>
<div class="m2"><p>در عالم یاری نبود از انصاف</p></div></div>