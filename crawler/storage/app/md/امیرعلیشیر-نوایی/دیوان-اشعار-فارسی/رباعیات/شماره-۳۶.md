---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>خواهی که ترا رسد ز درویشان فیض</p></div>
<div class="m2"><p>تو نیز رسان ز جود با ایشان فیض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینگونه گرت رسد به دل ریشان فیض</p></div>
<div class="m2"><p>شاید که ترا در رسد از پیشان فیض</p></div></div>