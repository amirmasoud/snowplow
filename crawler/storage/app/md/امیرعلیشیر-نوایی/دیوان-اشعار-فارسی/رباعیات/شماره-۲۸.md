---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>رفتی و دل از غمت فگارست هنوز</p></div>
<div class="m2"><p>وز شوق تو چشمم اشکبارست هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واگرد که جان ز هجر زارست هنوز</p></div>
<div class="m2"><p>باز آی که دل در انتظارست هنوز</p></div></div>