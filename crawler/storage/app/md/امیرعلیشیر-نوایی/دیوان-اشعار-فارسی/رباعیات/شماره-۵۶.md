---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>در فصل خزان برگ رزان ای ساقی</p></div>
<div class="m2"><p>شد کارگه رنگرزان ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان می که خوری دهم ازان ای ساقی</p></div>
<div class="m2"><p>تا نوشم از آن مزان مزان ای ساقی</p></div></div>