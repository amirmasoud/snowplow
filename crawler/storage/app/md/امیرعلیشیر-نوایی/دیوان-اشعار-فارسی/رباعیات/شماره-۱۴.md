---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>دل نیست که در زلف پریشان تو نیست</p></div>
<div class="m2"><p>جان نیست که سرگشته هجران تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که دلت زان منست آن تو نیست</p></div>
<div class="m2"><p>جان آن منست گوئیا جان تو نیست</p></div></div>