---
title: >-
    شمارهٔ ۲۷۵ - اختراع
---
# شمارهٔ ۲۷۵ - اختراع

<div class="b" id="bn1"><div class="m1"><p>پیمانه می جویان رفتم سوی میخانه</p></div>
<div class="m2"><p>بیرون نروم زانجا پر ناشده پیمانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیخان مناجاتی رندان خراباتی</p></div>
<div class="m2"><p>جویند ترا جانا در کعبه و بتخانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میکده سر مستم از ننگ خودی رستم</p></div>
<div class="m2"><p>نوشم قدح باده با نعره مستانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد جام میم قاتل جز این چه کند حاصل؟</p></div>
<div class="m2"><p>چون یار قدح دارد با عشق دیوانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واعظ بسر منبر گوید همه از کوثر</p></div>
<div class="m2"><p>من مست و برد خوابم زین بلعجب افسانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو هم گل بستانی هم شمع شبستانی</p></div>
<div class="m2"><p>هم روز منت بلبل هم شب شده پروانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی سخنم بشنو جویای خرابی شو</p></div>
<div class="m2"><p>در گنج نشد معمور کس ناشده ویرانه</p></div></div>