---
title: >-
    شمارهٔ ۹۶ - نعت
---
# شمارهٔ ۹۶ - نعت

<div class="b" id="bn1"><div class="m1"><p>زهی صد پیر کنعانی مریدت</p></div>
<div class="m2"><p>دو صد یوسف غلام زر خریدت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلت بشکفت اندر گلشن قرب</p></div>
<div class="m2"><p>نسیم باغ وحدت چون وزیدت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدی افلاک رو چون مژده وصل</p></div>
<div class="m2"><p>رسانید آسمان پیما بریدت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه باک از رنج عالم پیکرت را</p></div>
<div class="m2"><p>چو دل در مأمن قرب آرمیدت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بام کعبه پایت نارسیده</p></div>
<div class="m2"><p>لوای قدر بر گردون رسیدت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقفل چون بود درهای رحمت</p></div>
<div class="m2"><p>چون زلفین آمده پیچان کلیدت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دران در فانیا گویی گدایم</p></div>
<div class="m2"><p>ز خیل آن سگان کو که دیدت؟</p></div></div>