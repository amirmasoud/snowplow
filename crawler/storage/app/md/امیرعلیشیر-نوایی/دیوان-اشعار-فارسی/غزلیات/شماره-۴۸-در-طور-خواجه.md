---
title: >-
    شمارهٔ ۴۸ - در طور خواجه
---
# شمارهٔ ۴۸ - در طور خواجه

<div class="b" id="bn1"><div class="m1"><p>منم و میکده و مغبچه مست امشب</p></div>
<div class="m2"><p>هر دم از مستی او داده دل از دست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پری هر نفس از جلوه مستانه او</p></div>
<div class="m2"><p>کرده چون اهل جنون نعره پیوست امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست چون طره گهی بر ذقنت کرده دراز</p></div>
<div class="m2"><p>که چو گیسو شده زیر قدمش پست امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ممکنم نیست خلاصی ز دو زلفش که شدست</p></div>
<div class="m2"><p>دل بهر حلقه ازان سلسله پابست امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع آن چهره چو پروانه وجودم را سوخت</p></div>
<div class="m2"><p>تا نگویی اثر از هستی من هست امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بت ترسا میم از میکده چندان بنمود</p></div>
<div class="m2"><p>که دل شیفته از ننگ خودی رست امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوش تا روز چرا رو ندهد فانی را</p></div>
<div class="m2"><p>این چنین کز کف آن مغبچه شد مست امشب</p></div></div>