---
title: >-
    شمارهٔ ۲۴۷ - تتبع خواجه
---
# شمارهٔ ۲۴۷ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>در خرابات ار شبی میل قدح کمتر کنم</p></div>
<div class="m2"><p>عذر آن را روزها اندر سر ساغر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم از داغ جفا وز زخم گردون لاله‌وار</p></div>
<div class="m2"><p>خاک و خون بر سر کنم وز خاک و خون سر بر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که در آتشگه دیر مغانم شعله‌سان</p></div>
<div class="m2"><p>گاه مردن بستر راحت ز خاکستر کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هردم از کیفیت می چون فتم در عالمی</p></div>
<div class="m2"><p>مست باشم چونکه میل عالم دیگر کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که دارم خار غم زین گلشن نیلوفری</p></div>
<div class="m2"><p>کی به گلشن میل بر گل‌های نیلوفر کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که و نام وصال این بس که در دیوانگی</p></div>
<div class="m2"><p>جان فدای عشق آن حور پری‌پیکر کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار بی‌تقدیر چون ممکن نباشد ای حکیم</p></div>
<div class="m2"><p>کی نظر بر سیر چرخ و گردش اختر کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فانیا چون سرخ‌رویی بایدم در راه فقر</p></div>
<div class="m2"><p>خرقه و سجاده زان رهن می احمر کنم</p></div></div>