---
title: >-
    شمارهٔ ۱۹۱ - تتبع خواجه
---
# شمارهٔ ۱۹۱ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>باده صافست و خرابات صفایی دارد</p></div>
<div class="m2"><p>روم آن سو که عجب آب و هوایی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامه سرخ ببر کرد و دلیلست به خون</p></div>
<div class="m2"><p>زانکه از خون کسان رنگ قبایی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلوه ابرویت از می سرم افکند بزیر</p></div>
<div class="m2"><p>گو بکن سجده که خوش قبله نمایی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سگ وفادار و جفاجو به جهان سگ منشان</p></div>
<div class="m2"><p>من سگ آن کسم ای دل که وفایی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل که از حسرت رخسار و لبت رنجور است</p></div>
<div class="m2"><p>ز گل و قند همانا که دوایی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سگ او خون دل من خورد و من غم او</p></div>
<div class="m2"><p>زین که رنج و الم آلوده غذایی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانیا درد کشانند بلاکش گو رو</p></div>
<div class="m2"><p>سوی میخانه طمع آنکه بلایی دارد</p></div></div>