---
title: >-
    شمارهٔ ۱۶۲ - تتبع حضرت شیخ
---
# شمارهٔ ۱۶۲ - تتبع حضرت شیخ

<div class="b" id="bn1"><div class="m1"><p>چشمم چو بر آن روی چو رشک قمر افتاد</p></div>
<div class="m2"><p>از چشم دوید انجم و بر روی در افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خوی به رخت اختر دری به شفق نیست</p></div>
<div class="m2"><p>کز شبنم فردوس به گلبرگ تر افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون دلم از دیده ز بس کرد غمم فاش</p></div>
<div class="m2"><p>هر چند جگر گوشه نمود از نظر افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شامم که ز هجران تو شد روز قیامت</p></div>
<div class="m2"><p>چون روز شدم وعده به روز دگر افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ناله من تا به سحر خواب نبردش</p></div>
<div class="m2"><p>وین تهمتی در گردن مرغ سحر افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حسرت بوسی که به جانم ز لبت بود</p></div>
<div class="m2"><p>زان لعل چه خونها که مرا در جگر افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی بره سعدی اگر زد قدمی چند</p></div>
<div class="m2"><p>با او سخنش بین که چو شیر و شکر افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی نی چه حد آنکه درآید به مقابل</p></div>
<div class="m2"><p>کز پرتو اکسیر وی این خاک زر افتاد</p></div></div>