---
title: >-
    شمارهٔ ۳۳ - تتبع خواجه
---
# شمارهٔ ۳۳ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>میکند وقت صبح نعره سحاب</p></div>
<div class="m2"><p>که زمان صبوح را دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گلستان کشید مرغ صفیر</p></div>
<div class="m2"><p>در شبستان نمود ناله رباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین فغانها ز هر طرف یک یک</p></div>
<div class="m2"><p>برگرفتند سر ز خواب احباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیم مخمور و نیم مست شدند</p></div>
<div class="m2"><p>جانب بزم صبحگاه بشتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحریفان دوش ساقی بزم</p></div>
<div class="m2"><p>کرد بنیاد دور جام شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نمودی تو نیز بگشا چشم</p></div>
<div class="m2"><p>رنجه فرما قدم سوی اصحاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک دو دم وقت را غنیمت دان</p></div>
<div class="m2"><p>به حریفان بنوش باده ناب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باش تا آن زمان که دارد چرخ</p></div>
<div class="m2"><p>جام زرین مهر مست و خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد بیدار زانکه چون گردی</p></div>
<div class="m2"><p>مست بیشک دگر شوی در خواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواب دورو درازت اندر پیش</p></div>
<div class="m2"><p>خیز یکدم ز خواب روی بتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگشاید ز خواب ای فانی</p></div>
<div class="m2"><p>در چشمت مفتح الابواب</p></div></div>