---
title: >-
    شمارهٔ ۲۱۰ - تتبع خواجه
---
# شمارهٔ ۲۱۰ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>اگر تو جرعه فشانی کمی بریز به خاک</p></div>
<div class="m2"><p>مرا ز خاک شدن در طریق عشق چه باک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به می فتاده‌ام اما ز ضعف جسم درو</p></div>
<div class="m2"><p>نیم غریق و روانم بر آب چون خاشاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشک دیده بشستم ز غیر رخ بنمای</p></div>
<div class="m2"><p>که جز پی نظر پاک نیست منظر پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فغان که جان و دلم سوخت ساقی گلچهر</p></div>
<div class="m2"><p>گهی ز آتش می گه ز روی آتشناک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرم زدی و خوشم بر امید پابوسَت</p></div>
<div class="m2"><p>اگر ببندیش ای شهسوار بر فتراک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وزد چو صرصر عشق آن زمان رود به عدم</p></div>
<div class="m2"><p>به سان اخگر و خاکستر انجم و فلاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز غنچه و گل باغ فنا مگر فانی</p></div>
<div class="m2"><p>نشان دهد با دل چاک و خرقه صد چاک</p></div></div>