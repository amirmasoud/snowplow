---
title: >-
    شمارهٔ ۲۷۰ - تتبع میر وفایی
---
# شمارهٔ ۲۷۰ - تتبع میر وفایی

<div class="b" id="bn1"><div class="m1"><p>هست رویت چون گل و خال لبت بالای او</p></div>
<div class="m2"><p>چون حریر آل کز عنبر بود تمغای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ دل را زان به سوی گلشن وصلت هواست</p></div>
<div class="m2"><p>تا نگردد رنجه خاک آن چمن در پای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با قبای لاله گون در جلوه شد گویا که آب</p></div>
<div class="m2"><p>خورده در جوی جگر نخل قد رعنای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خیالش دیده و دل را بود نور و سرور</p></div>
<div class="m2"><p>زانکه گاهی دیده و گاهی دل آید جای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باعث قید جنون آمد ترا زنجیر زلف</p></div>
<div class="m2"><p>زانکه آرد هر شبم آشفتگی سودای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر دیرم زان به من دلرا ز غمها کرد چاک</p></div>
<div class="m2"><p>کز ردای زهد پاکان شد قدح پالای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دین فدا کردم به عشوه مغبچه سویم ندید</p></div>
<div class="m2"><p>کش هزاران دین فدای ناز و استغنای او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل ز رعنایان باغ دهر برکندم که نیست</p></div>
<div class="m2"><p>جز دو رنگی و دورویی در گل رعنای او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو فانی بنده شاهم کز آرایش جهان</p></div>
<div class="m2"><p>چو نموداری‌ست از باغ جهان‌آرای او</p></div></div>