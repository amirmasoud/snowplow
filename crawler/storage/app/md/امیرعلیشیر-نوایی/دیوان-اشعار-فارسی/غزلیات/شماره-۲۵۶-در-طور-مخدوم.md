---
title: >-
    شمارهٔ ۲۵۶ - در طور مخدوم
---
# شمارهٔ ۲۵۶ - در طور مخدوم

<div class="b" id="bn1"><div class="m1"><p>من هلاک از هجر و آن مه دلستان دیگران</p></div>
<div class="m2"><p>زنده بودن کی توان ممکن به جان دیگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال خود را کی بود خود عرضه دارم زان که هست</p></div>
<div class="m2"><p>اعتمادم بیش بر شرح و بیان دیگران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشدم صد گونه تهمت وه که رنجورند خلق</p></div>
<div class="m2"><p>از زبان خویشتن من از زبان دیگران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزمون دیگران را تیغ بیدادم زنی</p></div>
<div class="m2"><p>دیگر سوزی ز بهر امتحان دیگران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگران مقبول و من مردود کز قسم ازل</p></div>
<div class="m2"><p>محنت آمد زان من عشرت ازان دیگران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگران را وصل و من جویای قد و عارضش</p></div>
<div class="m2"><p>عشق سرو و گلم از بوستان دیگران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کام جان یابد ز وصلت دیگران فانی نکوست</p></div>
<div class="m2"><p>گر برون آیی پی قتل از میان دیگران</p></div></div>