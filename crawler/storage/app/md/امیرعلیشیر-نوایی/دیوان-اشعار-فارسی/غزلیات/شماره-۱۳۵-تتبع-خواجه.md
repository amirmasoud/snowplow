---
title: >-
    شمارهٔ ۱۳۵ - تتبع خواجه
---
# شمارهٔ ۱۳۵ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>آن بیوفا چه شد که نظر سوی ما کند</p></div>
<div class="m2"><p>وعده کند وفا و به وعده وفا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکاو ز جور مغبچگان در شکایت است</p></div>
<div class="m2"><p>باید به پیر دیر مغان التجا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوخی که التفات به سوی شهان نکرد</p></div>
<div class="m2"><p>با من که مست و رند و گدایم کجا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی که بعد عمری اگر داردم شراب</p></div>
<div class="m2"><p>من ناگرفته جام وی از کف رها کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چ آیدت به پیش چو بی اختیار تست</p></div>
<div class="m2"><p>درویش با که شکوه ز چون و چرا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشاق را ز بهر دل خویش رحم کرد</p></div>
<div class="m2"><p>شاید بدین غریب ز بهر خدا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می ده که جرم ما به دو صد زاری و نیاز</p></div>
<div class="m2"><p>زان زهد که به شیخ به عجب و ریا کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روحم بکوی دوست شد و نیستش علاج</p></div>
<div class="m2"><p>مرغی که سوی گلشن اصلی هوا کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فانی که خواست ملک بقا فتح گرددش</p></div>
<div class="m2"><p>نبود عجب که میل به دشت فنا کند</p></div></div>