---
title: >-
    شمارهٔ ۱۱۲ - تتبع مخدومی
---
# شمارهٔ ۱۱۲ - تتبع مخدومی

<div class="b" id="bn1"><div class="m1"><p>خط بر فراز لعل تو از مشک ناب چیست؟</p></div>
<div class="m2"><p>بر آب زندگیت ز ظلمت نقاب چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل چو مرغ وصل به سویت نمود میل</p></div>
<div class="m2"><p>یک دم قرار پیشه کن این اضطراب چیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شامش ار نه رنج خمارست از صبوح</p></div>
<div class="m2"><p>لرزان به خاک در شدن آفتاب چیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاری برون ز امر قضا نیست گر ترا</p></div>
<div class="m2"><p>رنجی رسد با نجم و گردون عتاب چیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفی اگر نه مغبچگانش زدند راه</p></div>
<div class="m2"><p>افتادنش به میکده مست خراب چیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر عکس روی شاهد مقصود بایدت</p></div>
<div class="m2"><p>جز جام باده آئینه بی حجاب چیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی مگو گذشته ام از خواب و از خیال</p></div>
<div class="m2"><p>کین نقش کون غیر خیالات و خواب چیست؟</p></div></div>