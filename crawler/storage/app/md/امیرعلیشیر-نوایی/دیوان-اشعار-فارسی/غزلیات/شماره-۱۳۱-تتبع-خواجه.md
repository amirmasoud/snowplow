---
title: >-
    شمارهٔ ۱۳۱ - تتبع خواجه
---
# شمارهٔ ۱۳۱ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>بیا که لشکر دی خیل سبزه غارت کرد</p></div>
<div class="m2"><p>بسوی باده ز یخ شوشه ها اشارت کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز باده جوی حرارت که رفت آن کآتش</p></div>
<div class="m2"><p>بگرم رویی خود دعوی حرارت کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بریم دفتر و سجاده بهر می سوی دیر</p></div>
<div class="m2"><p>«که سود کرد هرانکس که این تجارت کرد»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آن کسیکه درین فصل توبه چون بشکست</p></div>
<div class="m2"><p>گناه خود بشکست خودی کفارت کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شراب گشت به ما باعث خرابی ها</p></div>
<div class="m2"><p>خداش خیر دهاد ار چه پر شرارت کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای میکده عشرت فزاست باده فروش</p></div>
<div class="m2"><p>مگر بآب می این خانه را عمارت کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز لوث زهد ریای معاشری شد پاک</p></div>
<div class="m2"><p>که بهر سجده به ابریق می طهارت کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو سر بکوه و بیابان نهی دلی دریاب</p></div>
<div class="m2"><p>که یافت حج قبول آنکه این زیارت کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز لفظ بگذر و معنی طلب کن ای فانی</p></div>
<div class="m2"><p>که اهل معنی ابا ز آفت عبارت کرد</p></div></div>