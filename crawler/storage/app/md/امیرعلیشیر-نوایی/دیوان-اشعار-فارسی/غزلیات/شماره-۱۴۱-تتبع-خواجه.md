---
title: >-
    شمارهٔ ۱۴۱ - تتبع خواجه
---
# شمارهٔ ۱۴۱ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>ز سر آب حیاتم می آگهی آورد</p></div>
<div class="m2"><p>به کوی میکده خضرم به همرهی آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان که کشتی سایل سپهر بین ز هلال</p></div>
<div class="m2"><p>ببزم دردکشان ساغر تهی آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می صبوح کشان جان به باد خواهم داد</p></div>
<div class="m2"><p>که بوی دوست نسیم سحرگهی آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه میکده است که درد سفال او در سر</p></div>
<div class="m2"><p>گدای را هوس افسر شهی آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برهن خرقه اگر عاقلی بیا می نوش</p></div>
<div class="m2"><p>که شیخ جانب دیرش ز ابلهی آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خورد باده به خرگاه ماه من ناهید</p></div>
<div class="m2"><p>ز بهر بزم وی آهنگ خر گهی آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمند زلف تو عشاق را به سلسله بست</p></div>
<div class="m2"><p>به یک گره سوی ما رو به کوتهی آورد</p></div></div>