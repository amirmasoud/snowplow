---
title: >-
    شمارهٔ ۲۵۴ - تتبع مخدوم
---
# شمارهٔ ۲۵۴ - تتبع مخدوم

<div class="b" id="bn1"><div class="m1"><p>ساقی حیات بخشد چون باد نوبهاران</p></div>
<div class="m2"><p>چون ابر رخت هستی کش سوی کوهساران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر سخن شنو کن دفتر به می گرو کن</p></div>
<div class="m2"><p>در طور فقر نو کن آئین کامکاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد از مسیح دم زد لاله به که علم زد</p></div>
<div class="m2"><p>خوش باد کو قدم زد آنسو به خیل یاران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کن به می پرستی دیوانگی و مستی</p></div>
<div class="m2"><p>کو شو ز رنگ هستی شرمنده هوشیاران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هاد را نشانه در محنت زمانه</p></div>
<div class="m2"><p>از باده مغانه خوش وقت میگساران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بودم به روز خرم وز روزگار بی غم</p></div>
<div class="m2"><p>کو ای رفیق همدم آن روز و روزگاران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی بود به شیون کز دور چرخ پر فن</p></div>
<div class="m2"><p>هم دوست گشت دشمن هم جمله دوستداران</p></div></div>