---
title: >-
    شمارهٔ ۵۴ - تتبع خواجه
---
# شمارهٔ ۵۴ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>کار در دیر به غیر از جستن آن ماه نیست</p></div>
<div class="m2"><p>کش ز اهل خانقه جستم یکی آگاه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک قدح خوردم که شد دود از دماغم سوی چرخ</p></div>
<div class="m2"><p>چرخ گو خون خور ازین معنی که دود آه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سفال کهنه در دیر مغان شد جام جم</p></div>
<div class="m2"><p>زانکه آنجا هیچ فرقی در گدا و شاه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوخت از گل میخ انجم پای گردون شام هجر</p></div>
<div class="m2"><p>کش تحرک سوی صبح وصل آن دلخواه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل نه بندی جز به هست مطلق ار عقلیت هست</p></div>
<div class="m2"><p>زانکه هستی‌های موهومت شده ناگاه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر لب و زلفت بود زنار می ای مغبچه</p></div>
<div class="m2"><p>سجده پیش ابرویت هیچم کنون اکراه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد اندر سجده دور از حق فتاده رند را</p></div>
<div class="m2"><p>دست بردن سوی ساغر جز به بسم الله نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فانیا در کشتزار عشق بر خوردن ز وصل</p></div>
<div class="m2"><p>جز به رخسار چو کاه و ناله‌ای جانکاه نیست</p></div></div>