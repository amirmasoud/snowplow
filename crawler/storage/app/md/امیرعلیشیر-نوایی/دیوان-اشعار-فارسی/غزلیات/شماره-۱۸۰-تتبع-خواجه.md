---
title: >-
    شمارهٔ ۱۸۰ - تتبع خواجه
---
# شمارهٔ ۱۸۰ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>دوشم از سوز فنا با قد خم یاد آمد</p></div>
<div class="m2"><p>شمع در گریه شد و چنگ به فریاد آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همه سنگدلی رحم کنی گر دانی</p></div>
<div class="m2"><p>کز غم هجر توام دوش چه بیداد آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم تاراج خرابی چو بدید ابر بهار</p></div>
<div class="m2"><p>گریه اش ناله کنان بر گل و شمشاد آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خزان ریخت جوانان چمن سرو مگر</p></div>
<div class="m2"><p>کز چنان تفرقه از راستی آزاد آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اجل از هجر تو میخواست که قتل آموزد</p></div>
<div class="m2"><p>از پی کسب هنر جانب ارشاد آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش رفتم به خرابات و به جامی شد دفع</p></div>
<div class="m2"><p>آنچه از دور زمان بر دل ناشاد آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانیا قطع بیابان خودی دشوار است</p></div>
<div class="m2"><p>مگر آن کس که به توفیق خداداد آمد</p></div></div>