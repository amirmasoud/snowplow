---
title: >-
    شمارهٔ ۲۵۱ - تتبع امیر سهیلی
---
# شمارهٔ ۲۵۱ - تتبع امیر سهیلی

<div class="b" id="bn1"><div class="m1"><p>توان دلیر به خورشید آسمان دیدن</p></div>
<div class="m2"><p>ولیک ماه رخش را نمیتوان دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدش رقیب و هزارش هراس چون دیدم</p></div>
<div class="m2"><p>به دل رسید بلا صد هزار زان دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند جنون مرا لحظه لحظه در طغیان</p></div>
<div class="m2"><p>رخ پریوش خود را زمان زمان دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ز هجر رخش خارها به دیده به است</p></div>
<div class="m2"><p>هزار بار ز گل های بوستان دیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سوی ابروی اود بنگرم بگوشه چشم</p></div>
<div class="m2"><p>بدان مثال که در گوشه کمان دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنانکه نقطه موهوم دیدنست محال</p></div>
<div class="m2"><p>به نزد عقل چنان آمد آن دهان دیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال فوت مکن بهر دیدنش ایدل</p></div>
<div class="m2"><p>بروز دور به خورشید آسمان دیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عشق خود به غلط افکنم رقیبان را</p></div>
<div class="m2"><p>به چشم ازو ستده سوی این و آن دیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چشم خویش نهان ساز غیر او فانی</p></div>
<div class="m2"><p>که روی یار ز اغیار به نهان دیدن</p></div></div>