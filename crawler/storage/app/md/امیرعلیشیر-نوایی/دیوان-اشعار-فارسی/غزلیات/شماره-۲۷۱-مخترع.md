---
title: >-
    شمارهٔ ۲۷۱ - مخترع
---
# شمارهٔ ۲۷۱ - مخترع

<div class="b" id="bn1"><div class="m1"><p>چو آتشی است لب لعل پر فسانه او</p></div>
<div class="m2"><p>زبان به عشوه برآوردنش زبانه او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهانه در دم قتلم اگر کنده چه زیان</p></div>
<div class="m2"><p>به نقد می کندم قتل چون بهانه او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهی است باده فروش و خزانه میخانه</p></div>
<div class="m2"><p>ز لعل پر خم بسیار در خزانه او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش است بحر می آنسان که عقل یارد شد</p></div>
<div class="m2"><p>ازین کرانه او تا بآن کرانه او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطیع پیر مغان گرد پس مگو که چه کرد</p></div>
<div class="m2"><p>هوای مغبچه و باده مغانه او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشو فریفته زلف و خال شاهد دهر</p></div>
<div class="m2"><p>که جست طایر زیرک ز دام و دانه او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو فانی آنکه نشانی به دوست برد ایدل</p></div>
<div class="m2"><p>درین جهان نتوان یافتن نشانه او</p></div></div>