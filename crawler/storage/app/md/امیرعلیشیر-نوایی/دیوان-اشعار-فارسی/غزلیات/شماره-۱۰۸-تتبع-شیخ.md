---
title: >-
    شمارهٔ ۱۰۸ - تتبع شیخ
---
# شمارهٔ ۱۰۸ - تتبع شیخ

<div class="b" id="bn1"><div class="m1"><p>نیست یکدل کز جفای چشم او بیمار نیست</p></div>
<div class="m2"><p>یا که چشمی کز غم دل تا سحر بیدار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه ناهمواری گردون برون از غایت است</p></div>
<div class="m2"><p>در جفا چون آن مه بی مهر ناهموار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل خاصان را بود کنج غم و مرغ دلم</p></div>
<div class="m2"><p>تا بود ویران مقام جغد در گلزار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود اندر عشق با فرهاد و شیرین نسبتم</p></div>
<div class="m2"><p>زانکه با اهل خرد دیوانگان را کار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شبی از شدت هجران ز عشق آیم به تنگ</p></div>
<div class="m2"><p>باز تا وقت سحر کارم جز استغفار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تار ادبارم بگردن زهد و دین از من مجوی</p></div>
<div class="m2"><p>ای مقیم دیر دان کین رشته زنار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست شد فانی دران کو از خیالش ای رقیب</p></div>
<div class="m2"><p>دست کوته کن تو هم او را همان پندار نیست</p></div></div>