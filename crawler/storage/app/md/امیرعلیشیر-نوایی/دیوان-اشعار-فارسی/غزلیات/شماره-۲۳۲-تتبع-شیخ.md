---
title: >-
    شمارهٔ ۲۳۲ - تتبع شیخ
---
# شمارهٔ ۲۳۲ - تتبع شیخ

<div class="b" id="bn1"><div class="m1"><p>به دو چشم یار اسیرم که همی زنند تیرم</p></div>
<div class="m2"><p>به من غریب رحمی که به کافران اسیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بکوی و قد او شیفته ام اگر چه واعظ</p></div>
<div class="m2"><p>دهدم ز روضه و حور فسانه کی پذیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اجلم رقیب و عمرم شده آن لب روان بخش</p></div>
<div class="m2"><p>نه از آن بود گریزم نه ازین بود گریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از آرزوی آن تیغ هلاک و او کشد غیر</p></div>
<div class="m2"><p>بکشید جای آنست ز غیرت ار نمیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من از تو جور ناید به کجا رسد وصالت</p></div>
<div class="m2"><p>که ترا جناب عالی من زار بس حقیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو به میهمانم آیی کشم آه و نیم جانی</p></div>
<div class="m2"><p>که جزین متاع نبود ز قلیل و از کثیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی یک نظر به کوی تو درآمدم چو فانی</p></div>
<div class="m2"><p>نظری نهفته گه گه تو ز حال وامگیرم</p></div></div>