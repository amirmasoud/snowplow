---
title: >-
    شمارهٔ ۱۱۵ - مخترع
---
# شمارهٔ ۱۱۵ - مخترع

<div class="b" id="bn1"><div class="m1"><p>اگر ز عین جفا چشم او دلم بشکست</p></div>
<div class="m2"><p>چه مردمی متوقع بود ز کافر مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که مرغ دل از قید دام فارغ بود</p></div>
<div class="m2"><p>به حلقه موی یکی طره شد دگر پا بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو نخل قد تو در باغ سینه بنشاندم</p></div>
<div class="m2"><p>به سینه تیر تو چون نخل دیگرم ننشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خودی فروخته رندان می مغانه خرند</p></div>
<div class="m2"><p>نه خود پرست چو شیخ است رند باده پرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اوج میکده نایم به بام چرخ فرو</p></div>
<div class="m2"><p>که همتم نکند میل سوی منزل پست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دست مغبچه می نوش سازم ای زاهد</p></div>
<div class="m2"><p>که هست کوثر و حور این دو در جهان گر هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به باده هستی خود را بشوی ای فانی</p></div>
<div class="m2"><p>که از هزار بلا رست آنکه از خود رست</p></div></div>