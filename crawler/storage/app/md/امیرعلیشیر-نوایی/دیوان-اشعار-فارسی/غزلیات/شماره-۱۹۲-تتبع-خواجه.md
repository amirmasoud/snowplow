---
title: >-
    شمارهٔ ۱۹۲ - تتبع خواجه
---
# شمارهٔ ۱۹۲ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>واعظان تا چند منع جام و ساغر می‌کنند</p></div>
<div class="m2"><p>چون دماغ خویش را هم گه گهی تر می‌کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قدح آنانکه گاه نشئه می‌یابند فیض</p></div>
<div class="m2"><p>زین شرف چون منع محرومان دیگر می‌کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صفا از توبه اهل زهد را ظاهر نشد</p></div>
<div class="m2"><p>چون بدین تکلیف رندان را مکدر می‌کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌فروشان باده را روزی که می‌سازند صاف</p></div>
<div class="m2"><p>مجمر روحانیان زان بو معطر می‌کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی نظاره بلبل خوشست اوراق گل</p></div>
<div class="m2"><p>باد و باران آن صحایف از چه ابتر می‌کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جرعه پیر مغانم ده به دست ای مغبچه</p></div>
<div class="m2"><p>رغم آنانی که وصف خود و کوثر می‌کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساده‌دل واعظ که گوید هرچه آید بر زبانش</p></div>
<div class="m2"><p>ساده‌تر آنانکه این افسانه باور می‌کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن چه چشمانند کز مژگان دو صف آراسته</p></div>
<div class="m2"><p>عالمی در طرفة‌العینی مسخر می‌کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خازنان روضه از اشعار فانی لعل و در</p></div>
<div class="m2"><p>برده بر رخسار و گوش حور زیور می‌کنند</p></div></div>