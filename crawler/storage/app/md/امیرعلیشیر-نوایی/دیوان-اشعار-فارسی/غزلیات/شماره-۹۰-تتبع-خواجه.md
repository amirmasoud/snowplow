---
title: >-
    شمارهٔ ۹۰ - تتبع خواجه
---
# شمارهٔ ۹۰ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>دل چو پروانه ز شمع رخ جانانه بسوخت</p></div>
<div class="m2"><p>وه چه پروانه که از شعله او خانه بسوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موی خال تو بران شعله عارض عجب است</p></div>
<div class="m2"><p>نشود سبز چو هر گه به زمین دانه بسوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق در سینه ام افتاد کزان سوخت دلم</p></div>
<div class="m2"><p>آتش افتاد به ویرانه که دیوانه بسوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق در هجر نشد دفع و به دل آتش زد</p></div>
<div class="m2"><p>شمع را شب بنشاندند و ازان خانه بسوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرقه پر می شد و در خلوتم افتاد آتش</p></div>
<div class="m2"><p>شعله در رخت در افتاد که کاشانه بسوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله شمع رخت شاند به خاک سیهم</p></div>
<div class="m2"><p>گرم خاکیست چو بال و پر پروانه بسوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماند عریان و ذلیلم که ز جرم تو به</p></div>
<div class="m2"><p>پیر میخانه مرا خرقه به جرمانه بسوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فانی ار دردکش میکده شد نیست عجب</p></div>
<div class="m2"><p>باده پالاش چو از آتش میخانه بسوخت</p></div></div>