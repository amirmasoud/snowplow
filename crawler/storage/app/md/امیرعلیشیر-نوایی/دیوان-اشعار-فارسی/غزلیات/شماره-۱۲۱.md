---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>چیست دانی ناله مرغ سحر هنگام صبح</p></div>
<div class="m2"><p>با حریفان صبوحی می‌دهد پیغام صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی اول می چو بگرفتند شوخان چمن</p></div>
<div class="m2"><p>لاله از یاقوت و نرگس نیز از زر جام صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده گلرنگ را در جام چون خورشید نوش</p></div>
<div class="m2"><p>شسته شد از چشمه خور خون رخ گلفام صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بخندد همچو غنچه صبح کاینک دم به دم</p></div>
<div class="m2"><p>غنچه‌آسا زعفران آید برون از کام صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبح هم جام صبوحی زد که جیبش گشت چاک</p></div>
<div class="m2"><p>ورنه چون چاک گریبان از چه شد اندام صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چمن اکنون سوی میخانه باید زد علم</p></div>
<div class="m2"><p>گشت چون ظاهر ز روی کوهسار اعلام صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک سحر آن گل صبوحی کرد با من زان نفس</p></div>
<div class="m2"><p>پیرهن چون گل درم هرکس که گیرد نام صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با حریفان یک سحر تا شام شوم است خراب</p></div>
<div class="m2"><p>چون به شامت عاقبت خواهد کشید انجام صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح چون فانی صفای وقت از طاعت بیافت</p></div>
<div class="m2"><p>باری اولی آنکه می ندهد ز کف هنگام صبح</p></div></div>