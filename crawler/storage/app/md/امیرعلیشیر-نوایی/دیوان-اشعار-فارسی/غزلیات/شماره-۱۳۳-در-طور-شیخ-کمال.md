---
title: >-
    شمارهٔ ۱۳۳ - در طور شیخ کمال
---
# شمارهٔ ۱۳۳ - در طور شیخ کمال

<div class="b" id="bn1"><div class="m1"><p>ملمع خرقه‌ام از وصله‌ها بادپالا شد</p></div>
<div class="m2"><p>بدان هیأت که گویی داغ‌های باده هرجا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز آنها پاره ای دیگر بسان جامه کعبه</p></div>
<div class="m2"><p>به بین کش دوخته بر روی محراب مصلا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه عالی رتبه شد دیر مغان کز نام او مستی</p></div>
<div class="m2"><p>اگر یک پایه بالا جست بر نام مسیحا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مر زان مغبچه زاهد که ترک عشق فرمودی</p></div>
<div class="m2"><p>چو دیدش سبحه و سجاده زنار و چلیپا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوانی دل ز دستم برد و عشقش می به دستم داد</p></div>
<div class="m2"><p>مرا پیرانه سر اسباب رسوایی مهیا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بآئین صلاح و تقویم آراست شیخ آوه</p></div>
<div class="m2"><p>همه بر باد رفت از دور چون آنشوخ پیدا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه پرسی در خراباتم که نقد صبر و هوشت کو</p></div>
<div class="m2"><p>هم اول روز ز آشوب می و شاهد به یغما شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا در خانقه زهد و خرد بس تیره میدارد</p></div>
<div class="m2"><p>خوش آن رندی که در دیر مغان سر مست شیدا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبوده وادی عشق و محبت را کران ای دل</p></div>
<div class="m2"><p>که شد آواره‌تر فانی درین دشت بلا تا شد</p></div></div>