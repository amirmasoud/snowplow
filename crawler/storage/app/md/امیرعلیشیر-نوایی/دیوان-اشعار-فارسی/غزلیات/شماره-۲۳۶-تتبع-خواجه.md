---
title: >-
    شمارهٔ ۲۳۶ - تتبع خواجه
---
# شمارهٔ ۲۳۶ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>نیست دل اینکه منِ زارِ بلاکش دارم</p></div>
<div class="m2"><p>از تو در سینه خود پاره‌ای آتش دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقیا جرعه میْ ده که به امید وصال</p></div>
<div class="m2"><p>درکشم چند دل از هجر جفاکش دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روکشا جمعیتم را که من سودایی</p></div>
<div class="m2"><p>دل چو آن طره آشفته مشوش دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر طرف چابک رعنای من اندر جولان</p></div>
<div class="m2"><p>من سر خویش چو خاک سم ابرش دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قد خم شده چون ماه نو انگشت‌نما</p></div>
<div class="m2"><p>این همه شهرت ازان چابک مهوش دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر رسد ناخوشی از خلق خوشم چون فانی</p></div>
<div class="m2"><p>زانکه با ناخوشی اهل زمان خوش دارم</p></div></div>