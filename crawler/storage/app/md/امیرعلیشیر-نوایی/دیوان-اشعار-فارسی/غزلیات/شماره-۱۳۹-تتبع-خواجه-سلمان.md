---
title: >-
    شمارهٔ ۱۳۹ - تتبع خواجه سلمان
---
# شمارهٔ ۱۳۹ - تتبع خواجه سلمان

<div class="b" id="bn1"><div class="m1"><p>هر که را دل مبتلای چون تو جانانی بود</p></div>
<div class="m2"><p>هم فدا سازد گرش هر مو بتن جانی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خیال آرم کشیدن آن تن نازک ببر</p></div>
<div class="m2"><p>منکه هر سو کرده سر از سینه پیکانی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای مسلمانان چه خوانیدم به مسجد چون به دیر</p></div>
<div class="m2"><p>رخنه در دینم فکنده نا مسلمانی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی من مجنون توانم دید روی آن پری</p></div>
<div class="m2"><p>کش بدیدن عقل کل مدهوش حیرانی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک خون پیدا برویم راند وان کاندر دلش</p></div>
<div class="m2"><p>از فراق لاله رویی داغ پنهانی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زو جدا گشتم ندانستم که از بی طاقتی</p></div>
<div class="m2"><p>آه و اشکم در غمش هر لحظه طوفانی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منع فانی کرد ناصح از جنون و عاشقی</p></div>
<div class="m2"><p>کی بود باور که او این نوع نادانی بود</p></div></div>