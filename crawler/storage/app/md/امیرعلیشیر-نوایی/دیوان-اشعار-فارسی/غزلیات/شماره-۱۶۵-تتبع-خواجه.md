---
title: >-
    شمارهٔ ۱۶۵ - تتبع خواجه
---
# شمارهٔ ۱۶۵ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>علی الصباح مغان قفل دیر باز کنید</p></div>
<div class="m2"><p>دو جام باعث گفتار اهل راز کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گر اهل زهد و ریا بگذرند معاذالله</p></div>
<div class="m2"><p>به وقت نکته ز نامحرم احتراز کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیاز ما ز شما کشتن است ای خوبان</p></div>
<div class="m2"><p>ز غمزه خواه شما قتل و خواه ناز کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بابروی بت خود سجده کرده جان دادم</p></div>
<div class="m2"><p>اگر برآیدتان این چنین نماز کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جلوه گر شود ای اهل روضه طوبی را</p></div>
<div class="m2"><p>فدای قامت آن سرو سرفراز کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو عاشق آمده فانی به گاه کشتن خلق</p></div>
<div class="m2"><p>میان مجرم و بی جرم امتیاز کنید</p></div></div>