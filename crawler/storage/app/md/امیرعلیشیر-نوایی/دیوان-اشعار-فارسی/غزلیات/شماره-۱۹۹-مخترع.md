---
title: >-
    شمارهٔ ۱۹۹ - مخترع
---
# شمارهٔ ۱۹۹ - مخترع

<div class="b" id="bn1"><div class="m1"><p>دل صدپاره‌ام از لعل تو خونست دگر</p></div>
<div class="m2"><p>هر دم از رهگذر دیده برونست دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مجنون که دران زلف شد ای باد صبا</p></div>
<div class="m2"><p>گو که در حلقه آن سلسله چونست دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سر نو مگر آراسته مشاطه صنع</p></div>
<div class="m2"><p>که رخت چون مه و خط غالیه گونست دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن پری عشوه کنان جام میم داد به دست</p></div>
<div class="m2"><p>در سرم آتش مستی و جنونست دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل که پیرانه دم از بازوی تقوی میزد</p></div>
<div class="m2"><p>در کف عشق یکی طفل زبونست دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسته بودم ز غم عشق چو یار آمد وای</p></div>
<div class="m2"><p>کان غمم از حد و اندازه فزونست دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای طبیب از سر فانی مگذر زان که ز هجر</p></div>
<div class="m2"><p>ضعف بیرونش به اندوه درونست دگر</p></div></div>