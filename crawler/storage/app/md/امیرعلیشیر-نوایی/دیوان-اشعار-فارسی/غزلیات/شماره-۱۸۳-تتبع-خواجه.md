---
title: >-
    شمارهٔ ۱۸۳ - تتبع خواجه
---
# شمارهٔ ۱۸۳ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>صبح رندان صبوحی در میخانه زدند</p></div>
<div class="m2"><p>در خرابات مغان ساغر مستانه زدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رنگین به خم عشق که بد مالامال</p></div>
<div class="m2"><p>دوره کرده قدح و جام به پیمانه زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رازهایی که شنیدن نتوانست ملک</p></div>
<div class="m2"><p>می ز پیمانه بآن نکته و افسانه زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونکه من دیر رسیدم به لبم یکجرعه</p></div>
<div class="m2"><p>ریخته دم به دم و طعنه جرمانه زدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر باری که ازان باده نماندم محروم</p></div>
<div class="m2"><p>که دران انجمن آن زمره فرزانه زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آتش شمع نه تنها دل پروانه بسوخت</p></div>
<div class="m2"><p>کاتش شمع هم از شعله پروانه زدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل عشاق فتادند بخاک ره دیر</p></div>
<div class="m2"><p>طره مغبچگانرا ز چه رو شانه زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشم از شادی طفلان پریوش گر چه</p></div>
<div class="m2"><p>سنگ بیداد و ستم بر من دیوانه زدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فانیا بیش مکن ناله ز ویرانی ازانک</p></div>
<div class="m2"><p>گنج معنی طلبان خیمه به ویرانه زدند</p></div></div>