---
title: >-
    شمارهٔ ۲۱۳ - اختراع
---
# شمارهٔ ۲۱۳ - اختراع

<div class="b" id="bn1"><div class="m1"><p>به مخموری پیاپی می‌تپد دل</p></div>
<div class="m2"><p>مگر از مژده می می‌تپد دل؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبالب ساغرش سازد مگر دفع</p></div>
<div class="m2"><p>چو زینسانم پیاپی می‌تپد دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر ساقی مهوش خواهم داشت</p></div>
<div class="m2"><p>قدح کز شادی وی می‌تپد دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تپد دل از میم نی ز آب حیوان</p></div>
<div class="m2"><p>نه پنداری ز هر شی می‌تپد دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گل شبنم چه تسکینم دهد ز آنک</p></div>
<div class="m2"><p>ازان رخسار پر خوی می‌تپد دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تپش دل را از آن مهوش مغنی است</p></div>
<div class="m2"><p>مگو کز نغمه نی می‌تپد دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل فانی تپد بی ساقی و جام</p></div>
<div class="m2"><p>ز خور و کوثرم کی می‌تپد دل؟</p></div></div>