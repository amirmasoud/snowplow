---
title: >-
    شمارهٔ ۵۰ - تتبع خواجه
---
# شمارهٔ ۵۰ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>مطلب صبح ازل طلعت درویشان است</p></div>
<div class="m2"><p>مخزن نقد ابد خلعت درویشان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع خورشید که گلزار ازو شد روشن</p></div>
<div class="m2"><p>گلی از بزمگه نزهت درویشان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام جمشید کزو کار جهان است عیان</p></div>
<div class="m2"><p>یک سفال کهن از صحبت درویشان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرش اعظم که بود بال ملک جاروبش</p></div>
<div class="m2"><p>قبه بارگه حشمت درویشان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طایر قدس که بر عرش نشیمن دارد</p></div>
<div class="m2"><p>پیک پیغام ده حضرت درویشان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ اطلس که مکلل به در انجم گشت</p></div>
<div class="m2"><p>پرده‌ای از حرم عصمت درویشان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر جهد تیر ملایک طرف از شست قضا</p></div>
<div class="m2"><p>باز گرداندنش از همت درویشان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فانیا روشنی وقت ز درویشان جوی</p></div>
<div class="m2"><p>کین کشاد از نظر رحمت درویشان است</p></div></div>