---
title: >-
    شمارهٔ ۸۷ - تتبع خواجه
---
# شمارهٔ ۸۷ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>هرگز گدای میکده از شاه غم نداشت</p></div>
<div class="m2"><p>کز التفات پیر مغان هیچ کم نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنها نه من به خاک مذلت فتاده‌ام</p></div>
<div class="m2"><p>هرگز فلک بر اهل خرد جز ستم نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم سفال کهنه میخانه پر شراب</p></div>
<div class="m2"><p>کین آیینه سکندر و این جام جم نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هجر گو بمیر هر آنکس که در وصال</p></div>
<div class="m2"><p>جور و جفای دلبر خود مغتنم نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز قامتت که سوی اسیران خرام کرد</p></div>
<div class="m2"><p>در باغ دهر سرو سهی این قدم نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن طفل شوخ مرغ دل خلق صید کرد</p></div>
<div class="m2"><p>با آنکه دام طره پر پیچ و خم نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی به فکر آن دهن ار مرد نیست عیب</p></div>
<div class="m2"><p>کی بود کو عزیمت ملک عدم نداشت؟</p></div></div>