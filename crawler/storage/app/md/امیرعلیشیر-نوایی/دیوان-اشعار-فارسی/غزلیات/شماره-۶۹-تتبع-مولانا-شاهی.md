---
title: >-
    شمارهٔ ۶۹ - تتبع مولانا شاهی
---
# شمارهٔ ۶۹ - تتبع مولانا شاهی

<div class="b" id="bn1"><div class="m1"><p>پیش جام پر می رخشنده مه را تاب نیست</p></div>
<div class="m2"><p>ساغر خورشید را گر تاب هست این آب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ستایی واعظا کوثر ز دست حور و عین</p></div>
<div class="m2"><p>خود ز دست ساقی گلرخ شراب ناب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست قلاب محبت از دل یاران کشش</p></div>
<div class="m2"><p>حاجت پولاد و آهن سر این قلاب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درگه پیر مغانست آنکه رندان سر نهد</p></div>
<div class="m2"><p>گر سجود آنست زین به بهر او محراب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می مده بیهوش دار و ده مرا ساقی که باز</p></div>
<div class="m2"><p>چند شب شد کز غم هجران به چشمم خواب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مغنی چون خراش سینه ام خواهی به لحن</p></div>
<div class="m2"><p>کش بروی تار ناخن حاجت مضراب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نشستی فانیا از عشق بر خاک سیه</p></div>
<div class="m2"><p>شاه وقتی بر بساطت حاجت سنجاب نیست</p></div></div>