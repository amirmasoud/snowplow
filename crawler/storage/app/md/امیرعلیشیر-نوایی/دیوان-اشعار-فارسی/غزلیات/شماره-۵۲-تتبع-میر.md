---
title: >-
    شمارهٔ ۵۲ - تتبع میر
---
# شمارهٔ ۵۲ - تتبع میر

<div class="b" id="bn1"><div class="m1"><p>خیال مغبچگان تا درون جان من است</p></div>
<div class="m2"><p>بکوی دیر مغان ناله و فغان من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمند زلف بتی این که ساختم زنار</p></div>
<div class="m2"><p>درون دیر بهر بزم داستان من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بین بصافی ساغر درو به حمرت می</p></div>
<div class="m2"><p>که آن نشانه ای از چشم خون فشان من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کوهکن نگر و بیستون که آن گویی</p></div>
<div class="m2"><p>دل طپنده و این یک غم گران من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو فتاده به من موی از دهان سبو</p></div>
<div class="m2"><p>که در سرشک مژه چشم ناتوان من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو من به نیستی از بی نشانی افتادم</p></div>
<div class="m2"><p>درین ره آنکه ز خود نیست شد نشان من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار تیغ بلا گر کشی نتابم روی</p></div>
<div class="m2"><p>مباش رنجه گر از بهر امتحان من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلطف بکر معانی نگر دلا و مپرس</p></div>
<div class="m2"><p>که از کجاست که گلهای گلستان من است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپرد نقد دل و جان به مخزنت فانی</p></div>
<div class="m2"><p>دگر مگو که ازان تو یا ازان من است</p></div></div>