---
title: >-
    شمارهٔ ۱۲۹ - تتبع میر
---
# شمارهٔ ۱۲۹ - تتبع میر

<div class="b" id="bn1"><div class="m1"><p>بوی شراب عشق تو بیهوشی آورد</p></div>
<div class="m2"><p>رنگش ز رنگ عقل فراموشی آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد تکلم تو زبان بند اهل عشق</p></div>
<div class="m2"><p>کش استماع مایه خاموشی آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعلت عجب می است که کیفیتش به دل</p></div>
<div class="m2"><p>بیحالی او فزاید و مدهوشی آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیر مغان که فیض کفش مستدام باد</p></div>
<div class="m2"><p>هر چند ساغر کرمش نوشی آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوخی که وقت قتل قلندروشی نمود</p></div>
<div class="m2"><p>وای آنزمان که رسم قباپوشی آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون هر سفال میکده جام جهان نماست</p></div>
<div class="m2"><p>گر روی در طریق فنا کوشی آورد</p></div></div>