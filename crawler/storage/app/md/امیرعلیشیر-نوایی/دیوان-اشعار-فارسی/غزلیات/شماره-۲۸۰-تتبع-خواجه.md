---
title: >-
    شمارهٔ ۲۸۰ - تتبع خواجه
---
# شمارهٔ ۲۸۰ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>باز در کوی مغان عاشق مستم جایی</p></div>
<div class="m2"><p>نی ز خویشم خبر و نی ز کسم پروایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که رسوایی من میطلبیدی خوش باش</p></div>
<div class="m2"><p>که ز عشقت نتوان یافت چو من رسوایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست در دیر بهر نغمه که از عشق کشند</p></div>
<div class="m2"><p>دستی افشانم ازان حالت و کوبم پایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواریم بین که به بزم دگران باید شد</p></div>
<div class="m2"><p>آرزو گر شودم صحبت بزم آرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیر و برنا اگرم طعنه زنانند چه عیب؟</p></div>
<div class="m2"><p>من که پیرانه سرم شیفته برنایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار با غیر کشد باده بگو من چه کشم</p></div>
<div class="m2"><p>جز به بد حالی خود ناله واویلایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانیا نیست به جز تفرقه ز ابنای زمان</p></div>
<div class="m2"><p>گر کشی رخت به سر حد فنا آسایی</p></div></div>