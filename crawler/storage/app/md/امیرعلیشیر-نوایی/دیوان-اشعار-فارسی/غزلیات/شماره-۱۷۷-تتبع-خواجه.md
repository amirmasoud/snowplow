---
title: >-
    شمارهٔ ۱۷۷ - تتبع خواجه
---
# شمارهٔ ۱۷۷ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>اگر به میکده ام یکشب انجمن باشد</p></div>
<div class="m2"><p>چراغ انجمن آن به که یار من باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه میل باغ کنم با وجود قد و رخش</p></div>
<div class="m2"><p>که صد فراغتم از سرو و یاسمن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زلف پرشکنش صید دل چسان برهد</p></div>
<div class="m2"><p>که صد کمند بلا زیر هر شکن باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهید عشق تو از خاک چون برآرد سر</p></div>
<div class="m2"><p>چو لاله غرق می و داغ بر کفن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روضه دیر مغان آرزو کنم در حشر</p></div>
<div class="m2"><p>غریب را دل محزون سوی وطن باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجاست می که بشوید ز لوح خاطر پاک</p></div>
<div class="m2"><p>گرت ز محنت دوران دو صد سخن باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به دشت فنا خاک ره شود فانی</p></div>
<div class="m2"><p>به باد سوی تواش میل آمدن باشد</p></div></div>