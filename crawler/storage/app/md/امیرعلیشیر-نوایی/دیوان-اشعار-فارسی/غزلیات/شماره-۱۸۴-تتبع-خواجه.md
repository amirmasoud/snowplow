---
title: >-
    شمارهٔ ۱۸۴ - تتبع خواجه
---
# شمارهٔ ۱۸۴ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>غزال زر ز فلک در مقام ما افتد</p></div>
<div class="m2"><p>که چون تو آهوی وحشی به دام ما افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روز وصل تو جمشید نوشد آب خضر</p></div>
<div class="m2"><p>اگر به ساغرش از درد جام ما افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مراد چرخ شود فوت اگر پس عمری</p></div>
<div class="m2"><p>ز دور جام مرادی به کام ما افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز روز هجر بتر تیره شد به ما شب غم</p></div>
<div class="m2"><p>چه شد که پرتو آن مه بشام ما افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرام آن مه اگر ز اوج رفعتست چسان</p></div>
<div class="m2"><p>به دست دامنش از اهتمام ما افتد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز باده ای که خوری با حیات و جان با هم</p></div>
<div class="m2"><p>چو بوی آنقدح اندر مشام ما افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نام رفت ز ما فانیا چه سود اکنون</p></div>
<div class="m2"><p>ازانکه قرعه دولت بنام ما افتد؟</p></div></div>