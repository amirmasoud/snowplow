---
title: >-
    شمارهٔ ۱۹۸ - تتبع خواجه
---
# شمارهٔ ۱۹۸ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>غیر ازو نیست مرا در دو جهان یار دگر</p></div>
<div class="m2"><p>جز ویم یار دگر جز غم او کار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نه آن بلبلم ای گل که دو صد خار جفا</p></div>
<div class="m2"><p>گر خلد از تو کنم روی به گلزار دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکنم کفر قبول ار کندم پیر مغان</p></div>
<div class="m2"><p>به جز از طره آن مغبچه زنار دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدح دور بما چون برسد ای ساقی</p></div>
<div class="m2"><p>تا به لب چونکه شود ریخته مقدار دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناصحا پند تو یکبار قبولم نفتاد</p></div>
<div class="m2"><p>گفته پندار ازین موعظه صد بار دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جم وقتم که گرفتم ز سر خرقه رهن</p></div>
<div class="m2"><p>جام دیگر به دو صد زاری و زنهار دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانیا درد طلب گر طلبی پیدا کن</p></div>
<div class="m2"><p>دل ریش دگر و سینه افکار دگر</p></div></div>