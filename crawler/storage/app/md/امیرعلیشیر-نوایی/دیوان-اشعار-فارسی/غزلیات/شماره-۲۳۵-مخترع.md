---
title: >-
    شمارهٔ ۲۳۵ - مخترع
---
# شمارهٔ ۲۳۵ - مخترع

<div class="b" id="bn1"><div class="m1"><p>ای کافر بد مست که بردی دل و دین هم</p></div>
<div class="m2"><p>جان نیز فدایت مگذر غافل ازین هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن طره بگوش تو سخن گوید و ابرو</p></div>
<div class="m2"><p>خم بهر شنیدن ز کمین حال چنین هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش عجب و صورت مشکل که ز حسنت</p></div>
<div class="m2"><p>نقاش ختا هم زد و صورت گر چین هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پیر مغان بار دگر توبه شکستیم</p></div>
<div class="m2"><p>یک رطل گرانم ده و در جرم مبین هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد حلقه انگشتری از تیر تو در دل</p></div>
<div class="m2"><p>دارم ز خط مهر تواش نقش نگین هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فانی چه غم از جور چو عشاق بلا کش</p></div>
<div class="m2"><p>دارند عوض وصل گمان بلکه یقین هم</p></div></div>