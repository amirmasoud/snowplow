---
title: >-
    شمارهٔ ۱۵۸ - تتبع میر
---
# شمارهٔ ۱۵۸ - تتبع میر

<div class="b" id="bn1"><div class="m1"><p>حسن روی حور جنت را فلک اظهار کرد</p></div>
<div class="m2"><p>چون رخ خوب تو دید از کعبه استغفار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وه چه کافر بود آن کز دیر مست آمد برون</p></div>
<div class="m2"><p>بهر قیدم رشته تسبیح را زنار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبان تا کرد تشبیه دهانش غنچه را</p></div>
<div class="m2"><p>در دلم از زخم پیکانها فزونتر کار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلک قدرت حل آن در دوره ساغر نهاد</p></div>
<div class="m2"><p>مشکلاتی را که در نه گنبد دوار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رندیش بادا حلال آن کو به عشق مغبچه</p></div>
<div class="m2"><p>خرقه سجاده رهن کلبه خمار کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لعل جان بخشش ز مردم جان به آسانی گرفت</p></div>
<div class="m2"><p>سخت‌خوانی‌های من این قصه را دشوار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر بازار حسنش خود فروشی را گذاشت</p></div>
<div class="m2"><p>یوسف و پیش رخش بر بندگی اقرار کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش چون میمردم از هجران صراحی خون گریست</p></div>
<div class="m2"><p>شمع نیز از سوز دردم خودکشی بسیار کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مست و عاشق فانی از دیر مغان آمد برون</p></div>
<div class="m2"><p>هر دو ثابت شد باو گرچه بسی انکار کرد</p></div></div>