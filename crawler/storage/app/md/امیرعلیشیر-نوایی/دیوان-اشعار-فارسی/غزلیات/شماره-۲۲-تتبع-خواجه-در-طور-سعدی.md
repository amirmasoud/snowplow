---
title: >-
    شمارهٔ ۲۲ - تتبع خواجه در طور سعدی
---
# شمارهٔ ۲۲ - تتبع خواجه در طور سعدی

<div class="b" id="bn1"><div class="m1"><p>ز روی بستر شاهی به بین گهی ما را</p></div>
<div class="m2"><p>به زیر پهلو خار و به زیر سر خارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو لب به عشوه گزی دست اگر نهم بر دل</p></div>
<div class="m2"><p>بگو چه چاره کنم جان ناشکیبا را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث وصل ترا بر زبان اگر نآرم</p></div>
<div class="m2"><p>ز سر چگونه برون آرم این تمنا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز کوی او که روی ای ملک به سوی بهشت</p></div>
<div class="m2"><p>چرا ز دست دهی این چنین تماشا را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگر به میکده راکع سبوی باده به دوش</p></div>
<div class="m2"><p>ز دوش آنکه نه انداختی مصلا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خواهد از پی حسن تو زهد مشاطه</p></div>
<div class="m2"><p>بآفتاب کشد زحمت از پی آرا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سعدی است قدم بر قدم زده فانی</p></div>
<div class="m2"><p>که پی دو فهم نگردد خیال دانا را</p></div></div>