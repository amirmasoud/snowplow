---
title: >-
    شمارهٔ ۱۱۶ - در طور خواجه
---
# شمارهٔ ۱۱۶ - در طور خواجه

<div class="b" id="bn1"><div class="m1"><p>بیا که پیر مغان جام پر ز صهبا ساخت</p></div>
<div class="m2"><p>ز بهر دردکشان بزم می مهیا ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به مجمع رندان رسید صف نعال</p></div>
<div class="m2"><p>ازانکه مغبچه در رو بروی من جا ساخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبوکشان دهم صاف باده قسمت کرد</p></div>
<div class="m2"><p>نه بهر صدرنشینان قدح مصفا ساخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبو مکرر و در پیش چند جام و تغار</p></div>
<div class="m2"><p>همه پر از می و عاری ز باده پالا ساخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خیل مغبچگان نیز چند ساقی را</p></div>
<div class="m2"><p>ز بهر خاطر رندان باده پیما ساخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مغنیان خوش الحان به وقت رقص و سرود</p></div>
<div class="m2"><p>به نقد دین حریفان ز بهر یغما ساخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو گشت مغبچه ساقی و دور گردان شد</p></div>
<div class="m2"><p>مرا هنوز قدح نارسیده رسوا ساخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بردن دل و دین مجمعی بدین آئین</p></div>
<div class="m2"><p>نیافتم ز کجا پیر دیر پیدا ساخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین شراب کسی کام یافت ای فانی</p></div>
<div class="m2"><p>که رسم خویش فنا ساخت بلکه افنا ساخت</p></div></div>