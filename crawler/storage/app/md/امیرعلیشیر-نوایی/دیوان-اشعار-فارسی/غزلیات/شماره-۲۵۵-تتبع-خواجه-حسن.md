---
title: >-
    شمارهٔ ۲۵۵ - تتبع خواجه حسن
---
# شمارهٔ ۲۵۵ - تتبع خواجه حسن

<div class="b" id="bn1"><div class="m1"><p>گر چه بلای خمار کرد فزون حزن من</p></div>
<div class="m2"><p>مغبچه و می ولیک اذهب عن الحزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شکن زلف او جای هزاران دلست</p></div>
<div class="m2"><p>طرفه که آن بند زلف هست شکن بر شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم خوی بر گلش بر شفق آمد نجوم</p></div>
<div class="m2"><p>نی شفق و نی نجوم خون دل و اشک من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر تن او پیرهن هست گران از حریر</p></div>
<div class="m2"><p>نیست عجب گر ز رشک پاره کنم پیرهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آتش آه دلم هست زبانه زبان</p></div>
<div class="m2"><p>لب شده پر آبله بین اثرش در دهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینه من کوه غم میکنم از ناخنش</p></div>
<div class="m2"><p>بار دلم بی ستون من به تهش کوهکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو و حافظ ترا فانی اگر هادی اند</p></div>
<div class="m2"><p>پیروی جامی ات هست به وجه حسن</p></div></div>