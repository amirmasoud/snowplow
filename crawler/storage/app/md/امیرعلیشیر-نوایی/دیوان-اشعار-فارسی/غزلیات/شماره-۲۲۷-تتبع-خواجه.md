---
title: >-
    شمارهٔ ۲۲۷ - تتبع خواجه
---
# شمارهٔ ۲۲۷ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>چه خوش باشد که باشد در بهارم</p></div>
<div class="m2"><p>کنار جوی و سروی در کنارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی باشد به سبزه افت و خیزم</p></div>
<div class="m2"><p>گهی باشد به ساغر گیر و دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود چون آب چشم و آتش دل</p></div>
<div class="m2"><p>تذروان بر کنار جویبارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست ساقی گلرخ دمادم</p></div>
<div class="m2"><p>می گلگون کند دفع خمارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همینم گر بود ارزانی از چرخ</p></div>
<div class="m2"><p>توقع شوکتی دیگر ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلندان را چو آخر خاکساریست</p></div>
<div class="m2"><p>بحمدالله کز اول خاکسارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بت گردید سوی قبله‌ام رو</p></div>
<div class="m2"><p>ز پیر دیر ازین رو شرمسارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خودی از خود بیفکندم چو فانی</p></div>
<div class="m2"><p>سبک‌تر شد براه عشق بارم</p></div></div>