---
title: >-
    شمارهٔ ۲۵۲ - تتبع خواجه
---
# شمارهٔ ۲۵۲ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>بپوشان روی خویش از خرقه پوشان</p></div>
<div class="m2"><p>به رندان باده نوش آنگه بنوشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آتشگاه سودای تو سوزند</p></div>
<div class="m2"><p>ریایی دلق خود را خرقه‌پوشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ردای رهن ما برمی‌فکندند</p></div>
<div class="m2"><p>ز بهر باده‌پالا می‌فروشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خروش از می خوش است ای دل ازان بیش</p></div>
<div class="m2"><p>که کم کردیم در کوی خموشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتاد اندر دل دُردی‌کشان جوش</p></div>
<div class="m2"><p>چو درد باده در خم گشت جوشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمارم سوزد ای ساقی کرم کن</p></div>
<div class="m2"><p>ز آب باده این آتش‌فروشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز رندان منصب فانی به هر بزم</p></div>
<div class="m2"><p>به دوش خود کشیدن شد سبوشان</p></div></div>