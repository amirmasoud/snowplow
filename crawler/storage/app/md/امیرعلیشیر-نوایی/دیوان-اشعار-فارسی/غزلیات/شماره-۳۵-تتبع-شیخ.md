---
title: >-
    شمارهٔ ۳۵ - تتبع شیخ
---
# شمارهٔ ۳۵ - تتبع شیخ

<div class="b" id="bn1"><div class="m1"><p>ای ز رویت ماه را صدگونه تاب</p></div>
<div class="m2"><p>مه مگو باشد سخن در آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر در کویت عذابم می کند</p></div>
<div class="m2"><p>هیچکس نشنیده در جنت عذاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ندیدم خواب در چشمم ز اشک</p></div>
<div class="m2"><p>چشمرا اکنون نمی بینم به خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تن خاکی است از لعل تو جوش</p></div>
<div class="m2"><p>خاک را در جوش می آرد شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون خیال دیدن رویت کنم</p></div>
<div class="m2"><p>در دل افتد ضعف از بس اضطراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر دیر و مغبچه مستم کنند</p></div>
<div class="m2"><p>خوش دلم در میکده از شیخ و شاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانیا در قطع وادیهای عشق</p></div>
<div class="m2"><p>از جگر باید غذا وز دیده آب</p></div></div>