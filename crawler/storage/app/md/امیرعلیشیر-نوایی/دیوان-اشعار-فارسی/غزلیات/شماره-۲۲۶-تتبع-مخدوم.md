---
title: >-
    شمارهٔ ۲۲۶ - تتبع مخدوم
---
# شمارهٔ ۲۲۶ - تتبع مخدوم

<div class="b" id="bn1"><div class="m1"><p>از باده تبرا چه کنم چون نتوانم</p></div>
<div class="m2"><p>اندیشه تقوا چه کنم چون نتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آشفتگی باده و درماندگی عشق</p></div>
<div class="m2"><p>با این دل شیدا چه کنم چون نتوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخمورم اگر کوثرم آری که بنوشش</p></div>
<div class="m2"><p>جز ساغر صهبا چه کنم چون نتوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بینمش از حال روم وه که غم دل</p></div>
<div class="m2"><p>در پیش وی افشا چه کنم چون نتوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دادن سر مویت که ستاند همه کونین</p></div>
<div class="m2"><p>این بیهده سودا چه کنم چون نتوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن مه شد و بی طاقتم این جان حزین را</p></div>
<div class="m2"><p>در هجر شکیبا چه کنم چون نتوانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی بره عشق نظر بر رخ خوبان</p></div>
<div class="m2"><p>جز آن رخ زیبا چه کنم چون نتوانم</p></div></div>