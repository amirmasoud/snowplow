---
title: >-
    شمارهٔ ۴۴ - اختراع
---
# شمارهٔ ۴۴ - اختراع

<div class="b" id="bn1"><div class="m1"><p>چون به دیر آمد ز بهر خم شکستن محتسب</p></div>
<div class="m2"><p>شد دل رندان چو چشم شوخ ساقی مضطرب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجتناب افتاد اهل دیر را از وحشتش</p></div>
<div class="m2"><p>اهل دین نبود عجب گشتن ز شیطان محتسب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتش افکند در دور حریفان انقلاب</p></div>
<div class="m2"><p>کش به جان آفت رسد از دور چرخ منقلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شام هجران حمرت گردون چه باشد از شفق</p></div>
<div class="m2"><p>گر نگشته آتش آهم به گردون ملتهب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کامکاران را نکودان نیکوان را ارتکاب</p></div>
<div class="m2"><p>آن چو نبود بد بود گشتن بدی را مرتکب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقی و دردمندی پیشه کن ور نیستی</p></div>
<div class="m2"><p>خویش را با اهل درد عشق میکن منتسب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانیا گر قطع صحرای فنا را طالبی</p></div>
<div class="m2"><p>بایدت از باطن پیر مغان شد مکتسب</p></div></div>