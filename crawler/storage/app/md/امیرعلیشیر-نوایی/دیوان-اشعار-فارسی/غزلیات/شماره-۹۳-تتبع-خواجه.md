---
title: >-
    شمارهٔ ۹۳ - تتبع خواجه
---
# شمارهٔ ۹۳ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>بیا که هاتف میخانه دوش پنهان گفت</p></div>
<div class="m2"><p>به من حکایتی از سر می که نتوان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گشت واقف ازین حال پیر باده فروش</p></div>
<div class="m2"><p>میم به تهنیه داد و به لطف و احسان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ای گدای خرابات ناامید مباش</p></div>
<div class="m2"><p>چرا که هاتف غیب آنچه بایدت آن گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازان زمان که دلم نشاء یافت از می عشق</p></div>
<div class="m2"><p>به خویشتن همه دشوار دهر آسان گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>براه از سخن پیر دیر افتادم</p></div>
<div class="m2"><p>که شیخ خانقه این نکته ها دگر سان گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن به لاله دلم خون شده درونم سوخت</p></div>
<div class="m2"><p>که درد خون دل و رنج داغ هجران گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث بستن زنار و بت پرستی من</p></div>
<div class="m2"><p>به خلق عاقبت آن شوخ نامسلمان گفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دل ز زلف وی آشفته بود از خاکش</p></div>
<div class="m2"><p>هر آنچه پرسه نمودم همه پریشان گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی خلاص ز گرداب غم شد ای فانی</p></div>
<div class="m2"><p>که زیر دور فلک ترک اهل دوران گفت</p></div></div>