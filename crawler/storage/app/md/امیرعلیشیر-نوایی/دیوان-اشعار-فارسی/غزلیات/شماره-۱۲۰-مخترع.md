---
title: >-
    شمارهٔ ۱۲۰ - مخترع
---
# شمارهٔ ۱۲۰ - مخترع

<div class="b" id="bn1"><div class="m1"><p>نسیم صبح نمود از تغار صهبا موج</p></div>
<div class="m2"><p>چو باد شرطه که آرد ز روی دریا موج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به باده زورق می را گر افکنی باشد</p></div>
<div class="m2"><p>چو کشتی ای که به دریا روان بود با موج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی هلاک حریفان ز حله دختر رز</p></div>
<div class="m2"><p>نموده هر طرف از لاله رنگ خارا موج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی که بود برش غیر مردم از غیرت</p></div>
<div class="m2"><p>ازان سبب که فکندم به بحر سودا موج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چین ابرو و چاه ذقن فرو گشتم</p></div>
<div class="m2"><p>چرا که هست ز گرداب غرقه ها تا موج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر برآوردم از بحر باده موج کرم</p></div>
<div class="m2"><p>غریق را نبرد بر کنار دریا موج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگر به بحر فنا شرح حال فانی خوان</p></div>
<div class="m2"><p>ازان خطوط که سازد ز لجه پیدا موج</p></div></div>