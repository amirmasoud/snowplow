---
title: >-
    شمارهٔ ۷۶ - ایضا له
---
# شمارهٔ ۷۶ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>زهی از تاب می گل گل شگفته باغ رخسارت</p></div>
<div class="m2"><p>ز هر گلخار خاری در دل عشاق بیمارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان رخسار و قامت گر نمایی جلوه در گلشن</p></div>
<div class="m2"><p>قیامت افتد از قامت ز رعنایی رفتارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دام زلف هر سو دانه خالت عجب نبود</p></div>
<div class="m2"><p>اگر مرغان باغ قدس را سازی گرفتارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان سان حسن و استغنای خوبی گر نگه داری</p></div>
<div class="m2"><p>حق یاری امیدم آنکه باشد حق نگهدارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا شد عشق و قسمت شد ترا زهد و ریا ای شیخ</p></div>
<div class="m2"><p>به کار من مرا بگذار و رو تو هم پی کارت!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون از دورت ای گردون محقر کلبه ای خواهم</p></div>
<div class="m2"><p>که می بارد غبار درد و غم از طاق زر کارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درم بگشای پیر دیر که اینک آمدم سرخوش</p></div>
<div class="m2"><p>به عذر توبه و تقوی بگردن بسته زنارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گدای عشق را اندک تفقد گر کنی امروز</p></div>
<div class="m2"><p>بود ای پادشاه حسن فردا اجر بسیارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو ای فانی که در سر هرچه بودت رهن می‌کردی</p></div>
<div class="m2"><p>عجب نبود که سر مانی کنون چون نیست دستارت</p></div></div>