---
title: >-
    شمارهٔ ۱۴۶ - تتبع خواجه
---
# شمارهٔ ۱۴۶ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>از رخت عکس مگر در می گلفام افتاد</p></div>
<div class="m2"><p>یا گل از گوشه دستار تو در جام افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گل خشک بود بسته به گلدسته تر</p></div>
<div class="m2"><p>شکل داغ تو که بر جسم گل‌اندام افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نه آن مغبچه از دیر برون آمد مست</p></div>
<div class="m2"><p>چیست این آفت و یغما که در اسلام افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرق‌ها هست به رسوایی عشق ای زاهد</p></div>
<div class="m2"><p>کز تو نام نکو افتاد و ز ما نام افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر فتنه است در ایام تو خوبان را لیک</p></div>
<div class="m2"><p>چشم فتان تو سرفتنه ایام افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده‌دار است شب و پرده‌دری شیوهٔ روز</p></div>
<div class="m2"><p>زین سبب عیش نهانی طرف شام افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل فانی ز گل روی تو شد بسته به زلف</p></div>
<div class="m2"><p>مرغی از گلشن قدس آمد و در دام افتاد</p></div></div>