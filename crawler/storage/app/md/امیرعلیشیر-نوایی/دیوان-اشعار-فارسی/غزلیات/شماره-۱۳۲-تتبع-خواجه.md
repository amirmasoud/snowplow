---
title: >-
    شمارهٔ ۱۳۲ - تتبع خواجه
---
# شمارهٔ ۱۳۲ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>دوش در میخانه جانان همدم عشاق بود</p></div>
<div class="m2"><p>تا سحر غوغای رندان را به جان مشتاق بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهره را آورده بود او از سر مستان به رقص</p></div>
<div class="m2"><p>کان صدا اندر خم این گنبد نه طاق بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه زو صد ناز و از ما بوده صد چندان نیاز</p></div>
<div class="m2"><p>در طریق حسن و عشق از روی استحقاق بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صفا و نور مجلس گر نهان بود آفتاب</p></div>
<div class="m2"><p>بهتر از وی لمعه جام می براق بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستی رندان ز می هم بود لیکن بیشتر</p></div>
<div class="m2"><p>از نوازش‌های آن ماه نکو اخلاق بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اهل آن هنگامه من بیهوش‌تر بودم ازانک</p></div>
<div class="m2"><p>با منش از جمله رندان بیشتر اشفاق بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست شو گر مخلصت باید که هرگز وانرست</p></div>
<div class="m2"><p>از جفای اهل آفاق آنکه در آفاق بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر آن شب هر کرا یک جرعه زان می‌شد نصیب</p></div>
<div class="m2"><p>تا قیامت در جهان رند علی الاطلاق بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فانی اندر سیر اطوار طریقت هرچه دید</p></div>
<div class="m2"><p>بود نیکو لیکنش زهد ریایی شاق بود</p></div></div>