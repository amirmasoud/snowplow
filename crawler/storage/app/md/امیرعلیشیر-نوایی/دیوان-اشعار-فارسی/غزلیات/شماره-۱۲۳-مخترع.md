---
title: >-
    شمارهٔ ۱۲۳ - مخترع
---
# شمارهٔ ۱۲۳ - مخترع

<div class="b" id="bn1"><div class="m1"><p>به خوبی شد چنان آن سیم بر شوخ</p></div>
<div class="m2"><p>که در خوبان چو او نبود دگر شوخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چسان هوشم به جا ماند که هستند</p></div>
<div class="m2"><p>دو چشم مستت از هم بیشتر شوخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا شوخی چنان باشد که باشد</p></div>
<div class="m2"><p>به تمکین تر کسی پیش تو هر شوخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگیرد دل بآن ظالم ز بیداد</p></div>
<div class="m2"><p>ز شوخی مانده کشتن نیست بر شوخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه تسکین یابد از پند پدروار</p></div>
<div class="m2"><p>که بیحد هست آن زیبا پسر شوخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر صورت مرا دیوانه دارند</p></div>
<div class="m2"><p>پری رویان به تمکینند اگر شوخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود فانی هلاک جان پیران</p></div>
<div class="m2"><p>اگر افتاد طفل سیمبر شوخ</p></div></div>