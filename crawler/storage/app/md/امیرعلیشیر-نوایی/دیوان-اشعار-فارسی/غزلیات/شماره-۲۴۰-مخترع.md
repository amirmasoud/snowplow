---
title: >-
    شمارهٔ ۲۴۰ - مخترع
---
# شمارهٔ ۲۴۰ - مخترع

<div class="b" id="bn1"><div class="m1"><p>گر چه دوش از داغ هجران آتشین تب داشتم</p></div>
<div class="m2"><p>سوز این آتش فزونتر بود کامشب داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد و ضعفم هر دو مهلک بود گر میآمدی</p></div>
<div class="m2"><p>پرسه را اسباب جان دادن مرتب داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو و خود در زمان هجر و ایام فراق</p></div>
<div class="m2"><p>روح را بی‌قالب و بی‌روح قالب داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تقرب جستم اندر شرب با مستان دیر</p></div>
<div class="m2"><p>عیب نبود چون به ایشان قرب مشرب داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خیال خار مژگانش نبودم خواب دوش</p></div>
<div class="m2"><p>ز آنکه اندر پیرهن صد نیش عقرب داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تب غم فانیا سوز درونم کم نشد</p></div>
<div class="m2"><p>زانکه از تبخاله بود آبی که بر لب داشتم</p></div></div>