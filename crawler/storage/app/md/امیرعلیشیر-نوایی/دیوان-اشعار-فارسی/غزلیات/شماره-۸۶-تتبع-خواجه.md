---
title: >-
    شمارهٔ ۸۶ - تتبع خواجه
---
# شمارهٔ ۸۶ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>ز بحر چرخ به کشتی عمر صد خلل است</p></div>
<div class="m2"><p>دواش بحر شراب و سفینه غزل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رقیق چو نقد حیات بی‌مثل است</p></div>
<div class="m2"><p>بت شفیق چو عمر عزیز بی‌بدل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به وصل او ندهم راه احتمال ای عشق</p></div>
<div class="m2"><p>اگرچه پیش خرد این فسانه محتمل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث رند خرابات نیست جز تسلیم</p></div>
<div class="m2"><p>که اهل خانقه است آنکه سربه‌سر جدل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو ای فقیه که باشد فنا نتیجه عشق</p></div>
<div class="m2"><p>ولیک عشق به دل نشئه می ازل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید قطع کن ای مرغ دل ز گلشن دهر</p></div>
<div class="m2"><p>که دام طایر قدسی ز دشته امل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قطع راه فنا وصل یافتی فانی</p></div>
<div class="m2"><p>بلی مراد درین راه در خور عمل است</p></div></div>