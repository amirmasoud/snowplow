---
title: >-
    شمارهٔ ۱۵۵ - تتبع خواجه
---
# شمارهٔ ۱۵۵ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>هر که در دیر مغان جام شرابی دارد</p></div>
<div class="m2"><p>رسدش گر به فلک ناز و عتابی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه در میکده بگرفت به کف رطل گران</p></div>
<div class="m2"><p>چرخ بر روی میش حکم حبابی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روضه و حور به کوثر چه کند یاد آنک او</p></div>
<div class="m2"><p>شاهد و منزل امن و می نابی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گدایان خرابات نیامد به حساب</p></div>
<div class="m2"><p>هر که از سلطنت دهر حسابی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش روی عرقناک تو چون چشمه مهم</p></div>
<div class="m2"><p>باز در حسن عجب آبی و تابی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صوت بلبل ز چه معنی نبود فیض رسان</p></div>
<div class="m2"><p>که ز اوراق گل تازه کتابی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشور حسن وی آباد ز آسیب زوال</p></div>
<div class="m2"><p>کز وفا پرسه احوال خرابی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می و معشوق رسان یاربش از لطف بآن</p></div>
<div class="m2"><p>کز خمار اندوه و از هجر عذابی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فانی از چرخ و نجوم از چه بنالد که به دهر</p></div>
<div class="m2"><p>شاه خورشیدوش عرش جنابی دارد</p></div></div>