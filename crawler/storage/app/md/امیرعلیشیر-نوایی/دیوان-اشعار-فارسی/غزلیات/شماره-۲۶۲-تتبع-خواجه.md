---
title: >-
    شمارهٔ ۲۶۲ - تتبع خواجه
---
# شمارهٔ ۲۶۲ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>صبح است فیض اگر طلبی ترک خواب کن</p></div>
<div class="m2"><p>تا چند مست خواب قدح پر شراب کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را به شیشه می فکن و از عتاب و لطف</p></div>
<div class="m2"><p>نی سنگ خاره افکن و نی لعل ناب کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم در انتظار تو ای عمر نازنین</p></div>
<div class="m2"><p>یکره بآمدن نه به رفتن شتاب کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی مقدر است نگردد زیاد و کم</p></div>
<div class="m2"><p>گر تو وقار ورزی و گر اضطراب کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مه ترا همی رسد از مستی و غرور</p></div>
<div class="m2"><p>خواهی به چرخ ناز و به انجم عتاب کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فانی شب وصال میی بی حساب دار</p></div>
<div class="m2"><p>وانرا به ما به عمر مخلد حساب کن</p></div></div>