---
title: >-
    شمارهٔ ۲۳ - اختراع
---
# شمارهٔ ۲۳ - اختراع

<div class="b" id="bn1"><div class="m1"><p>ز عشق هست به دل بار صد هزار مرا</p></div>
<div class="m2"><p>هنوز شکر بود صدهزار بار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم بود می گلگون ز ساقی گلرخ</p></div>
<div class="m2"><p>به حور و کوثرت ای پارسا چه کار مرا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بوسه ای که دهی و کشی منه منت</p></div>
<div class="m2"><p>چه منتم ز تو چون گشت انتظار مرا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جرم عشق و می از شحنه ام ضمان طلبد</p></div>
<div class="m2"><p>به پیر میکده عشق گو سپار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز می خمار بود و ز خمار می نوشم</p></div>
<div class="m2"><p>به دور باده تسلسل شد آشکار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو مرو بسوی دیر وه چو مغبچگان</p></div>
<div class="m2"><p>برند موی کشادم چه اختیار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جرم باده چه باکم که دوش هاتف غیب</p></div>
<div class="m2"><p>ز لطف شامل او کرد امیدوار مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر مگوی که چون مست سازمت بکشم</p></div>
<div class="m2"><p>چه سود ازین سخنت کشت چرن خمار مرا!</p></div></div>