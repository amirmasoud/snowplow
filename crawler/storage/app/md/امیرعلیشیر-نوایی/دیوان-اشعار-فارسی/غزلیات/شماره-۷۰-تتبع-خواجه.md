---
title: >-
    شمارهٔ ۷۰ - تتبع خواجه
---
# شمارهٔ ۷۰ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>آن کاکل مشکین که به رخ گشت حجابت</p></div>
<div class="m2"><p>آهست مرا کار پی رفع نقابت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنجی است ترا حسن کزو دهر شد آباد</p></div>
<div class="m2"><p>لیکن دل دیوانه من گشت خرابت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی می روشن که دل غم‌زده تیرست</p></div>
<div class="m2"><p>از گردش دوران ز سر زلف به تابت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ماه نه‌ای چون شده از دور گذارت؟</p></div>
<div class="m2"><p>گر عمر نه‌ای در شدن از چیست شتابت!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویی ز لبم کام تو چه بود که دهی جام</p></div>
<div class="m2"><p>هم گوی خود ای جان که درین چیست جوابت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسانه خود چون به تو گویم پس عمری</p></div>
<div class="m2"><p>چون بخت من آن لحظه رود چشم به خوابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی ز غم مغبچگان چند وه و آه</p></div>
<div class="m2"><p>شد در نظر پیر مغان وقت انابت</p></div></div>