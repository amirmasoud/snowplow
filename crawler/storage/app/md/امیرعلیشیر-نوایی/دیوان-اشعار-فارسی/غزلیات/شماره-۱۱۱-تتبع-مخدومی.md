---
title: >-
    شمارهٔ ۱۱۱ - تتبع مخدومی
---
# شمارهٔ ۱۱۱ - تتبع مخدومی

<div class="b" id="bn1"><div class="m1"><p>باز در دیر مغان عربده مستانست</p></div>
<div class="m2"><p>سخن از گنج فشانی تهی دستانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطرب از همت حاتم چه سرود آراید</p></div>
<div class="m2"><p>که فلک پست ترین منزل سر مستانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادن جان و ستاندن ز لب ساقی پرس</p></div>
<div class="m2"><p>تا شبانگاه بدین سان بده و بستانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو اگر چند بلند است ز اشجار چمن</p></div>
<div class="m2"><p>پیش نخل قد رعنای تو از پستانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وادی کعبه اسلام چه پویم ای شیخ</p></div>
<div class="m2"><p>که بت کافر من جانب ترکستانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستی و رندیم ار چرخ به دستان‌ها گفت</p></div>
<div class="m2"><p>چه کنم بهر من این نوع صدش دستانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی ار نیستی ات هست به مطلوب رسی</p></div>
<div class="m2"><p>که به وصلش سببی نیست اگر هست آنست</p></div></div>