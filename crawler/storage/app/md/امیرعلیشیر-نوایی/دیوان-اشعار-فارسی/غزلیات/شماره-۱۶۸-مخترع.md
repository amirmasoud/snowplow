---
title: >-
    شمارهٔ ۱۶۸ - مخترع
---
# شمارهٔ ۱۶۸ - مخترع

<div class="b" id="bn1"><div class="m1"><p>ساقی ما که به گردش می گلفام افکند</p></div>
<div class="m2"><p>ای بسا فتنه که در گردش ایام افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوش رندان بشد از جام می او گویا</p></div>
<div class="m2"><p>داروی بیهوشی آمیخته در جام افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتشین می که به من داشت ولی داد به غیر</p></div>
<div class="m2"><p>آتشم در دل بی طاقت و آرام افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوخت مرغ چمنم وقت صبوح از افغان</p></div>
<div class="m2"><p>چون نظر جانب آن سرو گلندام افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست بر آب شمر موج ز تحریک نسیم</p></div>
<div class="m2"><p>که اجل از پی مرغان طرف دام افکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر آن مغبچه مست برون رفت از دیر</p></div>
<div class="m2"><p>کین همه تفرقه در کشور اسلام افکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ شد مست به دیر و بربت مصحف سوخت</p></div>
<div class="m2"><p>تهمتش بر من دیوانه بدنام افکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاید از خوبی انجام رسد ز آغازش</p></div>
<div class="m2"><p>هر که ز آغاز نظر جانب انجام افکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چاره ای نیست به جز دادن دین فانی را</p></div>
<div class="m2"><p>چون که دل در کف آن کافر خودکام افکند</p></div></div>