---
title: >-
    شمارهٔ ۸۵ - ایضا له
---
# شمارهٔ ۸۵ - ایضا له

<div class="b" id="bn1"><div class="m1"><p>در میکده آنرا که به کف جام شراب است</p></div>
<div class="m2"><p>عیبش مکن ار شام و سحر مست و خراب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برداشتن از می نتواند سر خود را</p></div>
<div class="m2"><p>از باده هوا در سر آن کو چو حباب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر رشته کارش کشد آخر به خرابی</p></div>
<div class="m2"><p>هر مست که افتاد درین دیر خراب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میکشی می کوثر ز کف حور بهشتی</p></div>
<div class="m2"><p>در دوزخ مخموریت ای دل که عذاب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دیر فنا جام یقین جوی که مطلوب</p></div>
<div class="m2"><p>از پرده پندار تو بر بسته نقاب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مغبچه چون روی تذروست عذارت</p></div>
<div class="m2"><p>زانروی که چون خون تذروت می ناب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این دور به یک چشم زدن گشته دگرگون</p></div>
<div class="m2"><p>ای عمر به رفتن ز بر ما چه شتاب است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در میکده گر غرق میم توبه شکسته</p></div>
<div class="m2"><p>ای شخ ز ما در گذران عالم آب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فانی چه شوی شیفته دهر که کونین</p></div>
<div class="m2"><p>در دشت فنا جمله نمودار سراب است</p></div></div>