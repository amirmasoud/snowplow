---
title: >-
    شمارهٔ ۲۵۹ - اختراع
---
# شمارهٔ ۲۵۹ - اختراع

<div class="b" id="bn1"><div class="m1"><p>تو گشتی کج کلاه جمله شاهان</p></div>
<div class="m2"><p>غلط گفتم که شاه کج کلاهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه حسنست اینکه خون هر که ریزی</p></div>
<div class="m2"><p>نهد سر پیش رویت عذرخواهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو عشقت دعوی خونم نموده</p></div>
<div class="m2"><p>دو چشم خونفشانم شد گواهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنخدانت چو یابم بر کنم دل</p></div>
<div class="m2"><p>ز سیب روضه نی سیب سپاهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هجرت خون رود از مردم چشم</p></div>
<div class="m2"><p>به عشقم سرخ رو زین رو سیاهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز محرومی گناه خود نداند</p></div>
<div class="m2"><p>چو ریزی خون خیل بی‌گناهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فانی بین که اندازند گاهی</p></div>
<div class="m2"><p>نظر سوی گدایان پادشاهان</p></div></div>