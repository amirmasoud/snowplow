---
title: >-
    شمارهٔ ۲۲۱ - تتبع مخدوم
---
# شمارهٔ ۲۲۱ - تتبع مخدوم

<div class="b" id="bn1"><div class="m1"><p>باز در دیر مغان آه و فغان آورده‌ام</p></div>
<div class="m2"><p>عالمی را از فغان خود به جان آورده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گناه توبه با زنار گبر خویش را</p></div>
<div class="m2"><p>دست و گردن بسته در دیر مغان آورده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه میخواهی به این رسوا بکن ای مغبچه</p></div>
<div class="m2"><p>کانچنان کِت خواستی دل آنچنان آورده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف پیر دیر هست افزون ز جرم من از آن</p></div>
<div class="m2"><p>ار کند شرمنده سر بر آستان آورده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا رطل گرانم ده که از شرمندگی</p></div>
<div class="m2"><p>سر به زیر افکنده خود را سرگران آورده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه از نام و نشان آزادم اما داغ عشق</p></div>
<div class="m2"><p>بر جگر از بی‌نشانی‌ها نشان آورده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لایقالی گفته سر عشق را چون پیر دیر</p></div>
<div class="m2"><p>فانیا چون گویم ار صد داستان آورده‌ام</p></div></div>