---
title: >-
    شمارهٔ ۶۰ - تتبع خواجه
---
# شمارهٔ ۶۰ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>در دلم آتش محبت اوست</p></div>
<div class="m2"><p>آب چشمم ز دود فرقت اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست دود دلم به هیئت سرو</p></div>
<div class="m2"><p>از دلم رسته سرو قامت اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب لعلش که شد می‌ْآلوده</p></div>
<div class="m2"><p>چشمم آلوده خون ز حسرت اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخشش ابرو باد و لمعه نعل</p></div>
<div class="m2"><p>درگه پویه برق آفت اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ذلیلم به عشق و می ای شیخ</p></div>
<div class="m2"><p>این مذلت هم از مشیت اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده پیر دیرم ای زاهد</p></div>
<div class="m2"><p>که فراغم ز درد صحبت اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانی و دلبر خراباتی</p></div>
<div class="m2"><p>که فنا حاصلش ز خدمت اوست</p></div></div>