---
title: >-
    شمارهٔ ۱۶۰ - تتبع خواجه
---
# شمارهٔ ۱۶۰ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>چه عجب گر خوی آن چهره دل ما ببرد</p></div>
<div class="m2"><p>کوه را سیل چنین گر رسد از جا ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تماشای چمن رفتن آن سرو خوش است</p></div>
<div class="m2"><p>نیست این خوش که رقیبش به تماشا ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مجنون شده چون صید غزال لیلست</p></div>
<div class="m2"><p>سود نبود عربش گر چه ز صحرا ببرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که بی عاشقی افسرده نماید ای کاش</p></div>
<div class="m2"><p>که سمومیش درین دشت به یغما ببرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر آن کوی مگو نام تو شیدا چون شد</p></div>
<div class="m2"><p>پیش او کیست که نام من شیدا ببرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار مهمان و مرا ضعف که آیا بی می</p></div>
<div class="m2"><p>طرف میکده تسبیح و مصلا ببرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فانیا گشته ز دیرش مگر آرند برون</p></div>
<div class="m2"><p>دل آن را که می و ساقی ترسا ببرد</p></div></div>