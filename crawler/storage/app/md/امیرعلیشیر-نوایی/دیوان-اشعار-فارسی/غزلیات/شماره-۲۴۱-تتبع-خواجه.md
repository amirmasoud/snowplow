---
title: >-
    شمارهٔ ۲۴۱ - تتبع خواجه
---
# شمارهٔ ۲۴۱ - تتبع خواجه

<div class="b" id="bn1"><div class="m1"><p>چون بیاد لعل او میل می گلگون کنم</p></div>
<div class="m2"><p>ساغر دوران ز خوناب جگر پر خون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی دفع خمار مفلسان میکده</p></div>
<div class="m2"><p>گر ندارم وجه می سجاده را مرهون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرح آبادی به می گر افکنم باشد چنانک</p></div>
<div class="m2"><p>کز پی عشرت بنای خانه بر جیحون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که همواره قدح گردان بود باشد عجب</p></div>
<div class="m2"><p>التفات ار سوی ناهمواری گردون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستم رند ار ز آشوب حوادث دهر را</p></div>
<div class="m2"><p>وضع دیگرگون شود من حال دیگرگون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق را حزن از پی جاهست و من در میکده</p></div>
<div class="m2"><p>فانیا دارم قدح خود را چرا محزون کنم؟</p></div></div>