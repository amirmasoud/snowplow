---
title: >-
    شمارهٔ ۱۲ - تتبع مخدومی جامی
---
# شمارهٔ ۱۲ - تتبع مخدومی جامی

<div class="b" id="bn1"><div class="m1"><p>از تغار می چنان نوشم شراب ناب را</p></div>
<div class="m2"><p>کبر نتواند ز دریا آنچنان برد آب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جفا دارد قرار آن چشم و در بیداد خواب</p></div>
<div class="m2"><p>زانکه بردند از دل و چشمم قرار و خواب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا قیامت شام تنهایی بود در دیده خواب</p></div>
<div class="m2"><p>یک صبوحی مغتنم دان صحبت احباب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر وفا ز اهل زمان یابی شراب لعل نوش</p></div>
<div class="m2"><p>زانکه هرگز کس ندید این گوهر نایاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایکه گویی در جوانی باده نوش اینک به بین</p></div>
<div class="m2"><p>مست در دیر مغان افتاده شیخ و شاب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشته ای آن چشم خونریزم که بهر قصد دین</p></div>
<div class="m2"><p>کرده جا در عین مستی گوشه محراب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از در اهل جهان جستن مراد از فقر نیست</p></div>
<div class="m2"><p>وصل خواهی فانیا مسدود کن این باب را</p></div></div>