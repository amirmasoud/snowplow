---
title: >-
    شمارهٔ ۸۲ - تتبع شیخ
---
# شمارهٔ ۸۲ - تتبع شیخ

<div class="b" id="bn1"><div class="m1"><p>ملک آفاق به جز دیر مغان این همه نیست</p></div>
<div class="m2"><p>مایه عیش به جز رطل گران این همه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واعظا این همه از باغ جنان قصه مگوی</p></div>
<div class="m2"><p>که من و کوی کسی باغ جنان این همه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش گفتست ز بس نعره و آشوبم باز</p></div>
<div class="m2"><p>هست او ورنه علالای سگان این همه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جاه و اقبال جهان جمله حباب است و نمود</p></div>
<div class="m2"><p>بود این سلسله شعبده سان این همه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این همه حسن و لطافت که پریزاد مراست</p></div>
<div class="m2"><p>گر سوی جنس بشر بنگری آن این همه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از زمان هر نفسم صد غم بیداد رسید</p></div>
<div class="m2"><p>ور نه جور و ستم اهل زمان این همه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک کویش مگر از چشم ملائک شده نقش</p></div>
<div class="m2"><p>ورنه از چهره عشاق نشان این همه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فانیا جان ده و از محنت هجران واره</p></div>
<div class="m2"><p>حاجت ناله و آشوب و فغان این همه نیست</p></div></div>