---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>چو عزت بایدت ترک طمع کن</p></div>
<div class="m2"><p>گدایان را ازین معنی است خواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه از خورشید روشن چون ضیا خواست</p></div>
<div class="m2"><p>سیه رو گشت از آن بی اعتباری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درفشان گشت چون بر فرق مردم</p></div>
<div class="m2"><p>بشاهان چتر شد ابر بهاری</p></div></div>