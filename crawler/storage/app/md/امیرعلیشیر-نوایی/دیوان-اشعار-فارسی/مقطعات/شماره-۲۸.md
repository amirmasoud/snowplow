---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>فانیا گر شادیت باید طمع بگسل ز خلق</p></div>
<div class="m2"><p>زانکه از رنج طمع دل را رسد هر دم غمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بود در خاطرت رازی فرو خور دم مزن</p></div>
<div class="m2"><p>چون ز ابنای زمان ممکن نباشد محرمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بایدت از عالم آزادی برو آزاده باش</p></div>
<div class="m2"><p>زانکه باشد عالم آزادگی خوش عالمی</p></div></div>