---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>دلا در دور گردون گوشه ای گیر</p></div>
<div class="m2"><p>که از گرداب به جستن کناری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کناره گر میسر نیست خود را</p></div>
<div class="m2"><p>میفکن اندر این غرقاب باری</p></div></div>