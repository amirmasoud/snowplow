---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>چو عالم از پی بالا نشستن</p></div>
<div class="m2"><p>بهر مجلس رود خوش پای‌کوبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه عالم جاهلش دان زانکه او را</p></div>
<div class="m2"><p>نماید خوش به جز بالای خوبان</p></div></div>