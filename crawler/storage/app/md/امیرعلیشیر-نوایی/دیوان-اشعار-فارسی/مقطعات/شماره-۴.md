---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>توکل هر که سازد پیشه خویش</p></div>
<div class="m2"><p>ز بار منت مردم خلاص است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که نبود بار مردم را دران بزم</p></div>
<div class="m2"><p>که او بر خوان نعمت‌هاش خاص است</p></div></div>