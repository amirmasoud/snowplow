---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>ز حوض ماهیان دزدند ماهی</p></div>
<div class="m2"><p>که از خلق اوفتاده بر کرانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی کو حفظ سازد ماهیانرا</p></div>
<div class="m2"><p>مقرر سازم او را ماهیانه</p></div></div>