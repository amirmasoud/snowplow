---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>کاتبی کز نقطه‌ای بی‌جا گهش</p></div>
<div class="m2"><p>خوب شکل چوب گیرد نیک ننگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید آن ننگ خلائق را شکست</p></div>
<div class="m2"><p>هم به چوب انگشت‌ها هم سر به سنگ</p></div></div>