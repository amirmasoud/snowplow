---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>تتبع کردن فانی در اشعار</p></div>
<div class="m2"><p>نه از دعوی و نی از خودنمایی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ارباب سخن صاحبدلانند</p></div>
<div class="m2"><p>مرادش از در دل‌ها گدایی‌ست</p></div></div>