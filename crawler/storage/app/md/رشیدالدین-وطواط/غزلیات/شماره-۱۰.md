---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>مشکست توده توده نهاده بر ارغوان </p></div>
<div class="m2"><p>زلفین حلقه حلقهٔ آن ماه دلستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان توده تودهٔ مشک آیدم حقیر </p></div>
<div class="m2"><p>زین حلقه حلقهٔ تنگ آیدم بجان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون قطره قطره آب لطیفست عارضش</p></div>
<div class="m2"><p>وز نور شعله شعله نهاده بر ارغوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان قطره قطرهٔ آبست چون بخار </p></div>
<div class="m2"><p>زین شعله شعلهٔ نارست چون دخان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر روز دجله دجله بر آرم من از سرشک </p></div>
<div class="m2"><p>کو طرفه طرفه گل شکفاند ببوستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان دجله دجله دجلهٔ بغداد را مدد</p></div>
<div class="m2"><p>زین طرفه طرفه طرفهٔ شمشاد شد نوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا پشته پشته بار فراقش همی کشم</p></div>
<div class="m2"><p>چون ذره ذره کرد مرا بر هوا هوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان پشته پشته پشتهٔ کوه آیدم سبک</p></div>
<div class="m2"><p>زین ذره ذره ذرهٔ گرد آیدم گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هجرانش باره باره زمن برد خواب و خور</p></div>
<div class="m2"><p>من خیره خیره مانده ز دست عنا عیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان باره باره بارهٔ ...............</p></div>
<div class="m2"><p>زین خیره خیره خیرهٔ ...................</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نکته نکته در غزل آرم ز وصف او</p></div>
<div class="m2"><p>بختم ز تحفه تحفهٔ دولت دهد نشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان نکته نکته نکتهٔ رنج و جراحتست </p></div>
<div class="m2"><p>زیم تحفه تحفه تحفه قبول خدایگان</p></div></div>