---
title: >-
    شمارهٔ ۴ - نیز در مدح اتسز
---
# شمارهٔ ۴ - نیز در مدح اتسز

<div class="b" id="bn1"><div class="m1"><p>ای توتیای دیدهٔ من خاک کوی تو</p></div>
<div class="m2"><p>کم گشته آبروی من از عشق روی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون نسیم خلد بود نزد من بقدر</p></div>
<div class="m2"><p>بادی که او نشان دهد از خاک کوی تو </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای یوسف زمانه، چو یعقوب بود</p></div>
<div class="m2"><p>کارد بمن نسیم سحرگاه بوی تو ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشتم خمیده گشت چو چوگان ز بار غم</p></div>
<div class="m2"><p>در آرزوی آن ز نخ همچو گوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوریده کار گشته ام و تیره روزگار</p></div>
<div class="m2"><p>در انتظار روی تو ، مانند موی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی تو جان فزاید و خوی تو جان برد</p></div>
<div class="m2"><p>الحق که نه لایقست بروی تو خوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدخو مباش ، تانبرد نزد شهریار</p></div>
<div class="m2"><p>خوی بد تو رونق روی نکوی تو</p></div></div>
<div class="b2" id="bn8"><p>خسرو علاء دولت ، آن شاه روزگار</p>
<p>افزودن شده ز دولت او جاه روزگار</p></div>
<div class="b" id="bn9"><div class="m1"><p>ای فتنه گشته بر رخ خوب تو نیکویی</p></div>
<div class="m2"><p>در نیکویی نظیر تو از خلق هم توی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس دل که بسته طرهٔ جعدت بچابکی </p></div>
<div class="m2"><p>بس جان که خسته دیدهٔ شوخت بجادوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون آهو، ای نگار، ز من گشته ای رمان</p></div>
<div class="m2"><p>وین بس شگفت نیست ، که با چشم آهوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جسم مرا بسحر دو بادام آفتی</p></div>
<div class="m2"><p>درد مرا بلطف دو یاقوت داروی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آیم همیشه من بمراعات سوی تو</p></div>
<div class="m2"><p>لیکن تو از طریق مراعات یک سوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی تو خوب و خوی تو بد، این ستوده نیست </p></div>
<div class="m2"><p>ای روی خوب ، شرم نداری ز بد خوی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من مدح خوان شاهم و با من تو بد کنی</p></div>
<div class="m2"><p>و آنگه امید داری از شاه نیکوی ؟</p></div></div>
<div class="b2" id="bn16"><p>خسرو علاء دولت ، آن شاه روزگار</p>
<p>افزودن شده ز دولت او جاه روزگار</p></div>
<div class="b" id="bn17"><div class="m1"><p>تا پسندی ز من سر زلف ، ای نگار من</p></div>
<div class="m2"><p>شوریده گشت چون سر زلف تو کار من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر عارض تو زلف تو گشتست بی قرار</p></div>
<div class="m2"><p>زان بی قرار زلف ربودی قرار من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو جفت دیگری شدی و درد جفت من</p></div>
<div class="m2"><p>تو یار دیگری شدی و هجر یار من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پر شد ز خون دیده کنار من ، ای نگار </p></div>
<div class="m2"><p>تا از تو ، ای نگار، تهی شد کنار من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در انتظار روی تو بر خاک کوی تو</p></div>
<div class="m2"><p>دردا! که شد بباد همه روزگار من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همچون گل بهاری و اندر فراق تو </p></div>
<div class="m2"><p>از باد سرد همچو خزان شد بهار من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در بی شمار اندهم و مکرمات شاه</p></div>
<div class="m2"><p>بیرون برد ز دفتر انده شمار من</p></div></div>
<div class="b2" id="bn24"><p>خسرو علاء دولت ، آن شاه روزگار</p>
<p>افزودن شده ز دولت او جاه روزگار</p></div>
<div class="b" id="bn25"><div class="m1"><p>ایام حاسدان شریعت سیاه گشت</p></div>
<div class="m2"><p>احوال راعیان ضلالت تباه گشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا مقتدای دولت و تا پشتکار ملت </p></div>
<div class="m2"><p>خوارزمشاه ، اتسز خوارزمشاه گشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن خسروی ، که اهل هدی را جناب او </p></div>
<div class="m2"><p>از حادثات عالم جافی پناه گشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مار از نهیب خنجر او همچو مور شد</p></div>
<div class="m2"><p>کوه از هوای هیبت او همچو کاه گشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رأی بلند او بیضا همچو مهر شد </p></div>
<div class="m2"><p>عزم روان او بمضا همچو ماه گشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کینش اساس مهلکهٔ بدسگال شد</p></div>
<div class="m2"><p>مهرش لباس مفخرت نیک خواه گشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن کس که گشت معتصم حبل خدمتش</p></div>
<div class="m2"><p>از چاه ذل بر آمد و با عز و جاه گشت</p></div></div>
<div class="b2" id="bn32"><p>خسرو علاء دولت ، آن شاه روزگار</p>
<p>افزودن شده ز دولت او جاه روزگار</p></div>
<div class="b" id="bn33"><div class="m1"><p>آنگه که صحن معرکه درای خون شد</p></div>
<div class="m2"><p>در دست مرگ جان دلیران زبون شود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خون در رگ مبارز پیکار بفسرد</p></div>
<div class="m2"><p>جان در تن مقاتل غدار خون شود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زان بر فراخته شده رایات کارزار</p></div>
<div class="m2"><p>رایات عمرهای دلیران نگون شود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تن را سوی زمین و روان را سوی فلک</p></div>
<div class="m2"><p>از حربگه دلیل قضا رهنمون شود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاها ، که داند آن که زتیغ و سنان تو</p></div>
<div class="m2"><p>احوال دشمنان تو آن لحظه چون شود؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>با زخم گرز و خنجر فیروزه فام تو</p></div>
<div class="m2"><p>صحن زمین معرکه بیجاده گون شود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در تارک مخالف تو و زنهاد او </p></div>
<div class="m2"><p>شمشیر تو درون شود و جان برون شود</p></div></div>
<div class="b2" id="bn40"><p>خسرو علاء دولت ، آن شاه روزگار</p>
<p>افزودن شده ز دولت او جاه روزگار</p></div>
<div class="b" id="bn41"><div class="m1"><p>شاها، مخالفان تو بیچاره گشته اند</p></div>
<div class="m2"><p>از خانمان خویشتن آواره گشته اند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو همچو قطب ثابت و ایشان ز بیم تو</p></div>
<div class="m2"><p>سرگشته چون کواکب سیاره گشته اند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از صد هزار واقعه دل خسته ماده اند</p></div>
<div class="m2"><p>وز صد هزار حادثه غم خواره گشته اند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دور از منافقان جناب رفیع تو </p></div>
<div class="m2"><p>از هیبت تو با دل صد پاره گشته اند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا اهل دشمنی تو گشتند ، نزد عقل</p></div>
<div class="m2"><p>اهل هزار طعنه و بیغاره گشته اند </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر خلق بوده اند ستمگاره ، لاجرم</p></div>
<div class="m2"><p>مخذول روزگار ستم گاره گشته اند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یک بار نیست این که ز قهر تو دیده اند </p></div>
<div class="m2"><p>مقهور دستبرد تو صد باره گشته اند</p></div></div>
<div class="b2" id="bn48"><p>خسرو علاء دولت ، آن شاه روزگار</p>
<p>افزودن شده ز دولت او جاه روزگار</p></div>
<div class="b" id="bn49"><div class="m1"><p>شاها ، جلالت تو بجوزا رسیده باد </p></div>
<div class="m2"><p>اعلام فتح تو بثریا رسیده باد </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آن تیغ صفدری ، که بزیر رکاب تست</p></div>
<div class="m2"><p>از دست تو بتارک اعدا رسیده باد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آن بارگاه ، کز پی بارت کنند نصب</p></div>
<div class="m2"><p>اطناب او بساحل دریا رسیده باد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن باز ، کز کف تو پرد در شکارگاه </p></div>
<div class="m2"><p>منقار او پدیدهٔ عنقا رسیده باد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آن چاکری ، که غایشهٔ مهر تو کشد </p></div>
<div class="m2"><p>ز آلای او بدولت والا رسیده باد </p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آن نوبتی ، که بر در عالمی تو زنند </p></div>
<div class="m2"><p>بانگش باوج گنبد خضرا رسیده باد </p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بر تخت مملکت برکات دوام تو </p></div>
<div class="m2"><p>در وارثان آدم و حوا رسیده باد</p></div></div>