---
title: >-
    شمارهٔ ۱۱۱ - قصیدۀ مخذوف الالف در مدح علاء الدوله اتسز
---
# شمارهٔ ۱۱۱ - قصیدۀ مخذوف الالف در مدح علاء الدوله اتسز

<div class="b" id="bn1"><div class="m1"><p>خسرو ملک بخش کشور گیر </p></div>
<div class="m2"><p>که ز خلقش بعدل نیست نظیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو شرق ، کز سر تیغش </p></div>
<div class="m2"><p>هست دشمن همیشه جفت نفیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصر مجد و شرف بدوست رفیع</p></div>
<div class="m2"><p>چشم فضل و هنر بدوست قریر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدمتش عهده وضیع و شریف </p></div>
<div class="m2"><p>حضرتش کعبهٔ صغیر و کبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چو قدرش علو شمس و قمر</p></div>
<div class="m2"><p>نه چو خلقش نصیب مشک و عبیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همتش هست همچو چرخ بلند </p></div>
<div class="m2"><p>فکرتش هست همچو بدر منیر </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست جز عین صدق و صورت حق</p></div>
<div class="m2"><p>هر چه لفظش همی کند تقریر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست جز عقد درو عقدهٔ سحر</p></div>
<div class="m2"><p>هر چه دستش همی کند تحریر </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زرد روی و نحیف تن گشته </p></div>
<div class="m2"><p>دشمن دولتش چو زر و زریر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسرو حق تویی ، که نیست ز خلق </p></div>
<div class="m2"><p>مثل تو جمله بخش و حمله پذیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چه بخشند بحر و کان در عمر </p></div>
<div class="m2"><p>هست در جنب بخشش تو حقیر </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیضهٔ مملکت ز تست مصون </p></div>
<div class="m2"><p>روضهٔ مکرمت ز تست نضیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طبع بیندهٔ تو وقت خطر </p></div>
<div class="m2"><p>مطلع گشته بر قلیل و کثیر </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همت تو ز روی رفعت قدر </p></div>
<div class="m2"><p>برده بر گوشهٔ سپهر سریر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وقت بخشش ز دست مکرم تو </p></div>
<div class="m2"><p>بحر قلزم همی خور تشویر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شرع گشته بحشمت تو قوی </p></div>
<div class="m2"><p>ملک گشته بصحبت تو خطیر </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جز بحکم تو در بروج فلک </p></div>
<div class="m2"><p>هیچ کوکب نکرده عزم مسیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرخ در بند قدر تو چو زمین </p></div>
<div class="m2"><p>بحر در پیش قدر تو چو غدیر </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر چه تدبیر تو بود در ملک </p></div>
<div class="m2"><p>پس تدبیر تو رود تقدیر </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون ز تف خدنگ و شعلهٔ تیغ </p></div>
<div class="m2"><p>عرصهٔ حربگه شود چو سعیر </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عیش هر صفدری شود چو شرنگ </p></div>
<div class="m2"><p>روی هر پردلی شود چو زریر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیغ هندی بسوی مرگ دلیل </p></div>
<div class="m2"><p>رمح خطی بصوب حرب صفیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در چنین حربگه بدوزی تو </p></div>
<div class="m2"><p>دل دشمن بنوک نیزه و تیر </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بحمیم و نعیم دشمن و دوست </p></div>
<div class="m2"><p>کین و مهرت شود نذیر و بشیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزه بگذشت و روز عید رسید </p></div>
<div class="m2"><p>قصد عشرت کن و نبیذ بگیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو قرین سرور و لهو و زرشک </p></div>
<div class="m2"><p>دشمن تو قرین کرم و زحیر </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بندهٔ حضرت تو خرد و بزرگ </p></div>
<div class="m2"><p>سفرهٔ خدمت تو میر و وزیر </p></div></div>