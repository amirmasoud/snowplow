---
title: >-
    شمارهٔ ۱۰۱ - در مدح ملک اتسز
---
# شمارهٔ ۱۰۱ - در مدح ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>ای در مصاف رستم دستان روزگار</p></div>
<div class="m2"><p>با باس تو هدر شده دستان روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقهور دستبرد تو اجرام آسمان</p></div>
<div class="m2"><p>مجبور پای بند تو ارکان روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش براق و هم تو هنگام کر و فر</p></div>
<div class="m2"><p>تنگ آمده مسافت میدان روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقل عطیت تو شکسته بگاه وزن</p></div>
<div class="m2"><p>درهم عمود و کفهٔ میزان روزگار </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی ولیت کعبهٔ تأیید ایزدی</p></div>
<div class="m2"><p>چشم عدوت جعبهٔ پبکان روزگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قبهٔ جلال تو تحویل آفتاب</p></div>
<div class="m2"><p>در عرصهٔ کمال تو جولان روزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندک بر بنان تو بسیار مکرمت</p></div>
<div class="m2"><p>پیدا بر بیان تو پنهان روزگار </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیارگان چرخ نهاده چو دایگان </p></div>
<div class="m2"><p>در دولت مراد تو پستان روزگار </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نسج دولت تو خرامنده هر زمان</p></div>
<div class="m2"><p>با گونه گون نوا تن عریان روزگار </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بارگاه حادثه از سفرهٔ عنا</p></div>
<div class="m2"><p>حسرت خورد عدوی تو بر خوان روزگار </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیزست از خصال تو بازار محمدت </p></div>
<div class="m2"><p>کندست با جلال تو دندان روزگار </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندر سداد سیرت و اندر جلال قدر</p></div>
<div class="m2"><p>سلمان عالمی و سلیمان روزگار </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر روزگار سر بکشد از هوای تو</p></div>
<div class="m2"><p>آن را شناس غایت خذلان روزگار </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای طالع تو رایت تمکین مشتری </p></div>
<div class="m2"><p>وی طلعت تو آیت امکان روزگار </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن کس که در سفینهٔ اقبال تو نشست </p></div>
<div class="m2"><p>شد ایمن از بلیت توفان روزگار </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دور از تو مدتی من مسکین، نه از خرد </p></div>
<div class="m2"><p>بودم بخوان حادثه مهمان روزگار </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گاهی کشیده ضربت دندان مستخف</p></div>
<div class="m2"><p>گاهی چشیده شربت زندان روزگار </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اخوان من، که بود بر ایشان امید من </p></div>
<div class="m2"><p>گشتند بر جفای من اخوان روزگار </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دل تنگم از جنایت اجرام آسمان</p></div>
<div class="m2"><p>رخ زردم از خیانت اعیان روزگار </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با این همه چو من دگری پشت کی نهد</p></div>
<div class="m2"><p>بر مسند کمال در ایوان روزگار </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در صد هزار سال بتأثیر آفتاب </p></div>
<div class="m2"><p>لعلی چون من نخیزد از کان روزگار </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آثار من ستارهٔ گردون مفخرت</p></div>
<div class="m2"><p>و اخبار من شکوفهٔ بستان روزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از فضل من فزوده عدد ذات اختران</p></div>
<div class="m2"><p>وز نثر من گرفته مدد جان روزگار </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با این همه فضیلت، از آنجا که راستیست</p></div>
<div class="m2"><p>باشم دریغ در کف احزان روزگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>غبنی بود، اگر بکساد اندر اوفتد </p></div>
<div class="m2"><p>این پربها متاع بد کان روزگار </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آخر هم پس از مضرت و حرمان بی قیاس</p></div>
<div class="m2"><p>آید بمن مبرت و احسان روزگار؟ </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روزی کند سپهر مفوض برای من </p></div>
<div class="m2"><p>تدبیرحل و عقد بدیوان روزگار ؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گردم بدان صفت که نباشد بشرق و غرب </p></div>
<div class="m2"><p>بی یاد من مجالس اعیان روزگار </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا هست از شرایط سامان آدمی </p></div>
<div class="m2"><p>تا هست بر طبایع بنیان روزگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بادا بمهر همه میثاق آسمان </p></div>
<div class="m2"><p>بادا بحکم تو همه پیمان روزگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قسم عدوت محنت انواع حادثات</p></div>
<div class="m2"><p>بخش ولیت نعمت الوان روزگار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این رفته در حدیقهٔ افضال ایزدی </p></div>
<div class="m2"><p>و آن مانده در مفازهٔ حرمان روزگار</p></div></div>