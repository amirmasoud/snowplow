---
title: >-
    شمارهٔ ۹۸ - هم در ستایش ملک اتسز
---
# شمارهٔ ۹۸ - هم در ستایش ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>منت خدای را، که باقبال شهریار</p></div>
<div class="m2"><p>شد رکن ملک و قاعدهٔ ملت استوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوارزمشاه عالم عادل ، که در جهان </p></div>
<div class="m2"><p>ناورد گشت چرخ چنو هیچ شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید خسروان ملک اتسز ، که کردهاش</p></div>
<div class="m2"><p>فهرست آسمان شد و تاریخ روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن صورت جلالت و آن جوهر شرف</p></div>
<div class="m2"><p>آن عنصر سیادت و آن مفخر تبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی ، که ملک را بیسارش بود یمین</p></div>
<div class="m2"><p>شاهی، که خلق را بیمینش بود یسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عزم او نفاذ ربودست آسمان </p></div>
<div class="m2"><p>وز حزم او ثبات گرفتست کوهسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه دست بر نوال گشاده چنو جواد</p></div>
<div class="m2"><p>نه پای در رکاب نهاده چنو سوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جاه او حدیقهٔ حق گشته پر نعیم </p></div>
<div class="m2"><p>وز فر او صحیفهٔ دین گشته پر نگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناهید هست ز آیت بزمش کمین اثر</p></div>
<div class="m2"><p>بهرام هست ز آتش رزمش کهین شرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده بدیده های کواکب ز سالها </p></div>
<div class="m2"><p>ایام ملک او را افلاک انتظار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کار موافقان شده از سعی او تمام</p></div>
<div class="m2"><p>جان مخالفان شده از تیر او فگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای چرخ را بقدر رفیع تو اقتدا</p></div>
<div class="m2"><p>وی عدل بملک بسیط تو افتخار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از یمن طالع تو و اقبال بخت تو </p></div>
<div class="m2"><p>شد سعد با سعادت و شد بخت بختیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>طبع ترا ز عصمت و مردانگی لباس</p></div>
<div class="m2"><p>جان حکمت و فرزانگی شعار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باقیست طبع تو ، که معانی دهد ثمر</p></div>
<div class="m2"><p>بازیست جود تو ، که محامد کند شکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باعظم همت تو ، کم از ذره ای فلک</p></div>
<div class="m2"><p>با فیض بخشش ، تو کم از قطره ای بحار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون از فزع بلرزد میدان طعن و ضرب </p></div>
<div class="m2"><p>چون از یلان بخیزد آواز گیرو دار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اوهام سرکشان شود حیران ز هول</p></div>
<div class="m2"><p>اشخاص صفدارن همه عاجز شود ز کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مطروح شخص فوجی در پای اضطراب </p></div>
<div class="m2"><p>مجروح جان قومی از دست اضطرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر خوان فتنه مطعم آن سخت بی مزه</p></div>
<div class="m2"><p>وز حوض مرگ مشرک او نیک بدگوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از تو فلک نماند آن روز بی گزند</p></div>
<div class="m2"><p>وز تو جهان نیابد آن لحظه زینهار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوبد عمود تو سر ابنای کین چو گوز</p></div>
<div class="m2"><p>دوزد خدنگ تو عدل اعدای دین چو نار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای بس بلند را! که کند همت تو پست</p></div>
<div class="m2"><p>وی بس از عزیز را ! که کند خنجر تو خوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شاها ، خدایگانا ، در هر مراد هست </p></div>
<div class="m2"><p>تدبیر تو موافق تقدیر کردگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در عهد تست اختر تابنده را مسیر</p></div>
<div class="m2"><p>در حکم تست انجم گردنده را مدار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رفتی ز دار ملک دو بار و هزار شهر</p></div>
<div class="m2"><p>از حشمت تو گشت گشاده در این دیار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اول بلاد ترک گشادی و آمدند</p></div>
<div class="m2"><p>در بند بندگی تو خانان نامدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کردند سرمه گرد رهت را باعتقاد</p></div>
<div class="m2"><p>بردند سجده خاک درت را باختیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>افراسیاب را نه همانا بدست بود</p></div>
<div class="m2"><p>ملکی ، که آن نهاد ترا چرخ در کنار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بار دگر بسوی خراسان شدی و بود</p></div>
<div class="m2"><p>هم یمن بر یمینت و هم یسر بر یسار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گردونت مائده ده و دولت شراب ده </p></div>
<div class="m2"><p>گیتیت غاشیه کش و نصرت سلاح دار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در هر بلاد کز تو خبر گشت منتشر</p></div>
<div class="m2"><p>در هر دیار کز تو اثر گشت آشکار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جستند بندگیت و نمودند چاکریت</p></div>
<div class="m2"><p>شاهان آن بلاد و بزرگان آن دیار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کردند جاه و مال باخلاص بندگی </p></div>
<div class="m2"><p>در زیر پای مرکب میمون تو نصار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون رایت مبارک تو رفت سوی مرو</p></div>
<div class="m2"><p>تا کار مرو گیرد از قبال تو قرار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در اول انقیاد نمودند اهل مرو</p></div>
<div class="m2"><p>بستند پیش تو کمر صدق بنده وار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو نیز بر اذیت خلق حمید خویش </p></div>
<div class="m2"><p>کردی بجای هر یک ایادی بی شمار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>معجب شدند ناگه و معثر بلطف تو</p></div>
<div class="m2"><p>خود چیست در جهان بتراز عجب و اعتثار؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پس عاقبت بدامن خلدان زدند دست</p></div>
<div class="m2"><p>تا لاجرم بر آمد از آن ابلهان دمار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با خنجر چو آتش و آب تو از گزاف</p></div>
<div class="m2"><p>کردند باد ساری و گشتند خاکسار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یا رب! چه روز بود که نیلوفری حسام</p></div>
<div class="m2"><p>از خون خلق کرد همه مرو لاله زار؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در هیچ کار زار ندیدند هیچ خلق </p></div>
<div class="m2"><p>تا در جهان پدید شد آیین کار زار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یک طایفه بسلسهٔ بند بسته سخت </p></div>
<div class="m2"><p>یک طایفه بصاعقهٔ تیغ کشته زار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چندان هزار کشته گروه از پی گروه</p></div>
<div class="m2"><p>چندان هزار بسته قطار از پس قطار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اینست چاشنی ز شراب حسام تو</p></div>
<div class="m2"><p>هان ! ای ملوک عرصهٔ آفاق اعتبار!</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آنگه شدی بخطهٔ آموی و در زمان</p></div>
<div class="m2"><p>بگرفتی آن ولایت و بگشادی آن حصار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با حملهٔ تو قلعهٔ آموی را چه قدر ؟</p></div>
<div class="m2"><p>خیبر چه پای دارد با مرد ذوالفقار ؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>امروز در نواحی عالم نماند کس </p></div>
<div class="m2"><p>کورا بپیش تیغ تو قدرست و اقتدار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو شیر شرزه ای و گوزنان عدم شوند</p></div>
<div class="m2"><p>هر گه که شیر شرزه بجنبد ز مرغزار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باز آمدی بمرکز اقبال بر مراد </p></div>
<div class="m2"><p>نصرة قرین و چرخ مطیع و خدای یار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از میوهٔ مطالب آمال بهره مند</p></div>
<div class="m2"><p>در روضهٔ لطایف و لذات شاد خوار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بر عطف دولت تو جلالت بود ردا</p></div>
<div class="m2"><p>در دست حشمت تو سعادت شود سوار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اکنون جهان ترا شده ، چونانکه خواستی</p></div>
<div class="m2"><p>مگذر تو از جهان و جهان را همی گذار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>می نوش باده ای ، ز امانیش رنگ و بوی</p></div>
<div class="m2"><p>می پوش جامه ای ، ز معالیش پود و تار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خوارزم بی مواکب تو بود چندگاه </p></div>
<div class="m2"><p>ایام او شده بصف چون شبان تار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>و اکنون بفر موکب و اقبال موردت </p></div>
<div class="m2"><p>با خوشی ارم شد و با حسن قندهار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شاها، ز بنده یک دوسخن ، کز نفایسست</p></div>
<div class="m2"><p>داننده یاد گیرد بر وجه یادگار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بی عدل نیست کنگرهٔ ملک مرتفع </p></div>
<div class="m2"><p>بی علم نیست قاعدهٔ عدل پایدار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر ملک را بعدل ثباتست و انتظام</p></div>
<div class="m2"><p>هر علم را بعقل ظهورست و اشتهار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون ملک بر و بحر ترا داد آسمان </p></div>
<div class="m2"><p>آنرا بعلم و عدل همی باش حق گزار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اعلام عدل را بمساعی بلند کن</p></div>
<div class="m2"><p>و ارباب علم را بایادی نگاه دار </p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از مخطیان مپوش رخ عفو و عاطفت </p></div>
<div class="m2"><p>بر مجرمان مبند در توبه و اعتذار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو شاه بردباری و در حق مجرمان</p></div>
<div class="m2"><p>جز عفو نیست عادت شاهان بردبار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خیر ملوک عصری و در خیر کوش ، از انک </p></div>
<div class="m2"><p>پاداش خیر خیر دهد آفریدگار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا نور و نار هست به از ظلمت و دخان</p></div>
<div class="m2"><p>تا راح و روح هست به از محنت و خمار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هرگز مباد موکب عمر ترا غروب</p></div>
<div class="m2"><p>هرگز مباد مرکب عز ترا عثار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بادا بهار حاسد ملک تو چون خزان</p></div>
<div class="m2"><p>بادا خزان ناصح صدر تو چون بهار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از گلبن سعادت و اقبال و خرمی</p></div>
<div class="m2"><p>گل باد در کف تو در چشم خصم خار</p></div></div>