---
title: >-
    شمارهٔ ۱۵۸ - قصیدۀ ذوقافتین در مدح خاقان کمال‌الدین نظام‌الدوله ارسلان خان ابوالقاسم محمود
---
# شمارهٔ ۱۵۸ - قصیدۀ ذوقافتین در مدح خاقان کمال‌الدین نظام‌الدوله ارسلان خان ابوالقاسم محمود

<div class="b" id="bn1"><div class="m1"><p>ای دلبری، که نیست نظیر تو در جهان </p></div>
<div class="m2"><p>جانی مرا و بلکه گران‌مایه تر ز جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدار تو سپهر نشاطست بر زمین </p></div>
<div class="m2"><p>رخسار تو بهشت جمالست در جهان </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داری دو لب چو ساخته دو پاره لعل</p></div>
<div class="m2"><p>وندر دو پاره لعل دو رسته درر نهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظاهر نگرددت، چو نگویی سخن، دهن </p></div>
<div class="m2"><p>پیدا نیایدت، چو نبندی کمر، دهان </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لب شکرستانی و من شکرها کنم </p></div>
<div class="m2"><p>گر زان شکرستان تو گردم شکرستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون جگر لطیفی و همچون روان عزیز </p></div>
<div class="m2"><p>و ز دیده بی توام شده خون از جگر روان </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمری فروختم بهوای تو و مرام </p></div>
<div class="m2"><p>زین عمر نیست حاصل سودی مگر زیان </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیدادگر بتی و بعدل کمال‌ دین </p></div>
<div class="m2"><p>یابم ز دست جور تو بیداد گرامان </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاقان، نظام دولت، محمود، آنکه هست </p></div>
<div class="m2"><p>از رهگذار کینهٔ او چرخ بر کران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دستبرد اوست فغانی بهر دیار</p></div>
<div class="m2"><p>از کارکرد اوست نشانی بمهرکان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی سر شدست دشمن و بازر شدست دوست </p></div>
<div class="m2"><p>ز آن تیغ سرفشان وزان دست زرفشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دستش کند بدایع جود و کرم پدید </p></div>
<div class="m2"><p>لفظش کند دقایق فضل و هنر عیان </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در علم او ز مایهٔ علم علی اثر</p></div>
<div class="m2"><p>در عدل او ز سایهٔ عدل عمر نشان </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیزد ز بهر مدت عمرش امان ز چرخ </p></div>
<div class="m2"><p>زاید ز بهر عدت جودش گهر ز کان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای گشته کوشش تو بحفظ هدی</p></div>
<div class="m2"><p>وی کرده بخشش تو برزق بشر ضمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در پیش ناصح تو فگنده قضا سپر</p></div>
<div class="m2"><p>در روی حاسد تو کشیده قدر کمان </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با آیت خلاف تو گشته بلا قرین</p></div>
<div class="m2"><p>با رایت وفاق تو کرده ظفر قران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گردد بریده سر چو قلم، هر که چون قلم </p></div>
<div class="m2"><p>در پیش خدمت تو نگردد بسر دوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>افلاک را هوای تو همواره در ضمیر</p></div>
<div class="m2"><p>و ایام را ثنای تو پیوسته بر زبان </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مداح را سحاب سخای تو چون صدف </p></div>
<div class="m2"><p>پر در کند بیک صلهٔ ماحضر دهان </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا در علو نباشد همچون فلک زمین </p></div>
<div class="m2"><p>تا در ضیا نباشد همچون شرر دخان </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر لحظه از کواکب عزی دگر بیاب </p></div>
<div class="m2"><p>هر روز بر اعادی کامی دگر بران </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ادبار شد نصیب عدوی تو بر مراد</p></div>
<div class="m2"><p>تا روز رستخیز باقبال در بمان</p></div></div>