---
title: >-
    شمارهٔ ۱۴۳ - در مدح اتسز
---
# شمارهٔ ۱۴۳ - در مدح اتسز

<div class="b" id="bn1"><div class="m1"><p>مفخر ملک عرصهٔ عالم</p></div>
<div class="m2"><p>گوهر تاج گوهر آدم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه غازی علاء دولت و دین </p></div>
<div class="m2"><p>آن فلک حشمت ستاره حشم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهریاری ، که طبع بازل او </p></div>
<div class="m2"><p>گردن فقر بشکند بکرم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کامگاری ، که دست فایض او </p></div>
<div class="m2"><p>دامن آز پر کند ز درم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن بهر جای پیشوای ملوک </p></div>
<div class="m2"><p>و آن بهر وقت مقتدای امم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه هستش فلک ز خیل عبید </p></div>
<div class="m2"><p>و آنکه هستش ملک ز خیل خدم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حشمتش در زمانه بوده قدیم </p></div>
<div class="m2"><p>همتش بر ستاره سوده قدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهمه چیز فعل اوست مشار</p></div>
<div class="m2"><p>بهمه علم ذات اوست علم </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناصح از مهر او قرین نشاط </p></div>
<div class="m2"><p>حاسد از کین او ندیم ندم </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای مطاعی ، که بهر خدمت تو</p></div>
<div class="m2"><p>بسته زاد از زمین میان قلم </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وی شجاعی تست کیمیای وجود</p></div>
<div class="m2"><p>کوشش تست توتیای عدم </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سد امن تو بسته راه بلا</p></div>
<div class="m2"><p>تیر عدل تو خسته جان ستم </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرده مر علم را و علم را </p></div>
<div class="m2"><p>طبع تو شاد و صنع تو خرم </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داده مر دوست را و دشمن را </p></div>
<div class="m2"><p>لطف تو سور و قهر تو ماتم </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک در دست نیک خواه تو زر </p></div>
<div class="m2"><p>نوش در کام بدسگال تو سهم </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا دلی را دهد زمانه سرور </p></div>
<div class="m2"><p>تا رخی را کند ستاره دژم </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیک خواه تو باد یار نشاط </p></div>
<div class="m2"><p>بدسگال تو باد جفت الم </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شخص هر تو سنی ز سهم تو رام</p></div>
<div class="m2"><p>پشت هر گردنی بپیش تو خم </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با دل تو همیشه شادی جفت </p></div>
<div class="m2"><p>با کف تو همیشه رادی ضم </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دشمن تو غمین و طبع تو شاد</p></div>
<div class="m2"><p>دولت تو فزون و خصم تو کم </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون زمان بندهٔ تو گیتی نیز </p></div>
<div class="m2"><p>چون زمین چاکر تو گردون هم</p></div></div>