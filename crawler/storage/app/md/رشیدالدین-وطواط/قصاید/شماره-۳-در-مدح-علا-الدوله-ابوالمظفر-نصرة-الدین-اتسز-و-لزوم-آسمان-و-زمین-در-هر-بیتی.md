---
title: >-
    شمارهٔ ۳ - در مدح علاء الدوله ابوالمظفر نصرة الدین اتسز و لزوم آسمان و زمین در هر بیتی
---
# شمارهٔ ۳ - در مدح علاء الدوله ابوالمظفر نصرة الدین اتسز و لزوم آسمان و زمین در هر بیتی

<div class="b" id="bn1"><div class="m1"><p>ای زمین را از رخت ، چون آسمان ، فرو بها</p></div>
<div class="m2"><p>بوسه ای را از لبت ملک زمین زیبد بها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک زمان دو زلف را زان روی چون مه دور کن </p></div>
<div class="m2"><p>تا زمین را بیش گردد ز آسمان فرو بها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر زمانی چون زمین نزدیک من گیرد قرار </p></div>
<div class="m2"><p>من بفر تو زجور آسمان گردم رها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نشاند آسمان پیش توام بر یک زمین </p></div>
<div class="m2"><p>از زمین گردم بشکر ، از آسمان یابم وفا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زمین در طاعت عشق تو گر خاضع ترم </p></div>
<div class="m2"><p>با منت چون آسمان تا کی بود قصد جفا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای زمینی سرو سیمین و آسمانی صورتی</p></div>
<div class="m2"><p>رؤیت مه بینمت بر روی و زهره بر قفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو سرو بوستانی بر روان رفتن زچیست؟ </p></div>
<div class="m2"><p>ور تو ماه آسمانی بر زمین رفتن چرا؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برای وعدهٔ یک بوسه ، ای ماه زمین </p></div>
<div class="m2"><p>چند سر گردان چو ماه آسمان داری مرا؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسمان را در ملاحت چون تویی حاصل نشد </p></div>
<div class="m2"><p>از زمین روم و چین و خاک خر خیز و ختا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمانی طالعی دارم ، که بی سودای تو</p></div>
<div class="m2"><p>بر زمین گامی نهادن نیست نزد من روا </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نگیری در کنارم چون زمین را آسمان </p></div>
<div class="m2"><p>گشته گیر از آسمان روزم سیه ، رنجم هبا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آسمان حسن اگر چون مه بتابی بر زمین </p></div>
<div class="m2"><p>پارسایان را کنی در یک زمان ناپارسا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون تو در خوبی بزیر آسمان ماهی کدام ؟ </p></div>
<div class="m2"><p>بر زمین یارای چون خوارزمشاهی را کجا؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوالمظفر ، خسرواتسز ، شاه ترکستان که هست</p></div>
<div class="m2"><p>بر زمین مملکت چون آسمان فرمانروا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن علاء دولت ودین ، کاسمانگون تیغ او </p></div>
<div class="m2"><p>بر زمین دادست عدل و علم عالم را علا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در زمین از شهریاران جز مر و را کس ندید</p></div>
<div class="m2"><p>آفتابی با کلاه و آسمانی با قبا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچ موضع در زمین بی عدل او باقی نماند </p></div>
<div class="m2"><p>تا خدای آسمان ملک زمین دادش عطا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای خداوندی ، که نزدیک زمین و آسمان </p></div>
<div class="m2"><p>آفتاب افتخاری ، آسمان کبریا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آسمانی در جلال و قدر و شاهان زمین</p></div>
<div class="m2"><p> در خطابت آسمان خوانند و این باشد سزا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برزمین چون روزی خلقان رساند جود تو</p></div>
<div class="m2"><p>زآسمان خواندن نیفتد این خطاب تو خطا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آسمان تا نامهٔ خوارزمشاهی بر تو خواند </p></div>
<div class="m2"><p>خوانده شد بر عمر خصمت نامهٔ عزل و فنا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آسمان داد و دینی ، آفتاب فضل و عدل </p></div>
<div class="m2"><p>بر زمین از تاج و تختت منشر نور و ضیا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیست در روی زمین یک پادشه با قدر تو </p></div>
<div class="m2"><p>آسمان در خدمت تو پشت از آن دارد دو تا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جز بعدل اندر زمین راضی نباشد لاجرم </p></div>
<div class="m2"><p>آسمان در بندگی دادست حکمت را رضا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر کجا رزم تو باشد بر زمین ، آنجا کند </p></div>
<div class="m2"><p>آسمان در موج خون بدسگالان آشنا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دادگر شاه زمین از آسمانی وز خدای </p></div>
<div class="m2"><p>تا زتیغ تو رعایت یافت شرع مصطفا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آسمان را بر زمین یک عمر وبن عنتر نماند</p></div>
<div class="m2"><p>تا زشمشیر تو نو شد نام تیغ مرتضا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بفکند تیر تو در رزم آسمان را بر زمین </p></div>
<div class="m2"><p>گر ندارد نیزهٔ تو آسمان را بر هوا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر گریزد دشمن تو از زمین بر آسمان </p></div>
<div class="m2"><p>جانش از تن چون زمین از آسمان گردد جدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زآسمان تیغ تو بارد همی فتح و ظفر </p></div>
<div class="m2"><p>در زمین ملک تو روید گیاهی کیمیا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آسمان با آن سخا ، کندر سعادتهای اوست</p></div>
<div class="m2"><p>بر زمین دست ترا خواند همی ابر سخا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در طریق خدمتت تو هر که پیماید زمین</p></div>
<div class="m2"><p>زآسمان آید بگوش او ندای مرحبا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر که را باشد مقام اندر زمین ملک تو </p></div>
<div class="m2"><p>در جهان هرگز ندارد آسمانش بی نوا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عاجزست از رمح تو بر آسمان تیر شهاب </p></div>
<div class="m2"><p>قاصرست از مرکب تو بر زمین باد صبا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اژدها با گنج باشد در زمین ، زان ساختست </p></div>
<div class="m2"><p>آسمان رمح ترا بر گنج نصرت اژدها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در زمین هر کاشنایی یافت با درگاه تو </p></div>
<div class="m2"><p>آسمان او را کند با بی نیازی آشنا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدسگال تو زمین گشتست و تیغت آسمان </p></div>
<div class="m2"><p>بر زمین از آسمان دایم همی بارد بلا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون بحرب آیی ، زخون حلق و روی خصم تو</p></div>
<div class="m2"><p>آسمان پر ارغوان گردد ، زمین پر کهربا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر زمین ، هر لحظه ای ، کندر شمار ملک تست </p></div>
<div class="m2"><p>نگذرد هرگز زحکم آسمان بروی و با</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر زمین از بس که خون دشمن دین ریختی </p></div>
<div class="m2"><p>شد فتوحت چون نجوم آسمان بی منتها</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زان گرفتست آسمان جرم زمین را در کنار</p></div>
<div class="m2"><p>تا مصون دارد ز آفت عرصهٔ ملک ترا </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر زمین بیشی ، بر تبت ، زآفتاب و آسمان </p></div>
<div class="m2"><p>آسمان تخت زیبد ، آفتابت متکا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حضرت تو بر زمین محراب اصحاب ثناست</p></div>
<div class="m2"><p>هم چنان چون آسمان محراب اصحاب دعا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از پی آن تا بماند ملک و نامت جاودان </p></div>
<div class="m2"><p>آسمانت را پر دعا کردم ، زمین را پر ثنا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آسمان شد لفظ من در مدحت شاه زمین </p></div>
<div class="m2"><p>جاودان از آسمان ملک زمین بادت قضا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر زمین بادت ظفر ، تا بر سما تابد نجوم</p></div>
<div class="m2"><p>آسمان بادت رهی ، از زمین روید گیا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عدل را ، شاها ، بقا اندر بقای ملک تست</p></div>
<div class="m2"><p>ملک بادت بر زمین ، تا آسمان دارد بقا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خدمت تو پیشه کرده پادشاهان زمین </p></div>
<div class="m2"><p>و آسمان کرده ترا در پادشاهی مقتدا</p></div></div>