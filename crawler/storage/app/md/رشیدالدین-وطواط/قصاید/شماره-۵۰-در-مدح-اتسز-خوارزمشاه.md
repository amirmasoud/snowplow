---
title: >-
    شمارهٔ ۵۰ - در مدح اتسز خوارزمشاه
---
# شمارهٔ ۵۰ - در مدح اتسز خوارزمشاه

<div class="b" id="bn1"><div class="m1"><p>چون بر وزد بچهرهٔ تو ، ای نگار ، باد</p></div>
<div class="m2"><p>گردد ز نقش چهرهٔ تو پرنگار باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستم غلام باد ، که هر صبح دم مرا</p></div>
<div class="m2"><p>آرد نسیم طرهٔ تو ، ای نگار ، باد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر روز بامداد ز آسیب زلف تو </p></div>
<div class="m2"><p>گردد عیبر بیز و شود مشکبار باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خدمت دو زلف و دو رخسار تو شدست </p></div>
<div class="m2"><p>مقبل ترین خلق درین روزگار باد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد اختیار چاکری باد جان من </p></div>
<div class="m2"><p>تا چاکری زلف تو کرد اختیار باد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو یوسفی بحسن و چو یعقوب داردم </p></div>
<div class="m2"><p>هر شب ز بهر بوی تو در انتظار باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از اهتمام روی تو و سعی موی تو </p></div>
<div class="m2"><p>سازد مشک و لاله تست مرا غمگسار باد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسم بمهر خاک سر کوی تو ، چنانک</p></div>
<div class="m2"><p>بوسد بجز بعجز خاک در شهریار باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوارزمشاه ، اتسز غازی ، که روزم رزم </p></div>
<div class="m2"><p>خواهد ز حد خنجر او زینهار باد </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عرصهٔ ممالک و صحن بلاد او </p></div>
<div class="m2"><p>از بیم او ربود نیارد غبار باد </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زادبار آنکه مایهٔ انفاس خصم اوست </p></div>
<div class="m2"><p>ماندشت تا بروز جزا خاکسار باد </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارد ز گام بارهٔ او اضطراب خاک </p></div>
<div class="m2"><p>گیرد ز سیر بیکک او اعتبار باد </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نی چون صهیل بارهٔ او در جبال رعد</p></div>
<div class="m2"><p>نی با نفاذ حملهٔ او در قفار باد </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با قدر او نهد قلم ارتفاع چرخ </p></div>
<div class="m2"><p>با عزم او زند قدم اضطرار باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاها ، تویی که از فزع تیغ تیز تو </p></div>
<div class="m2"><p>اطراف خویش را نکند آشکار باد </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون کاه ، زارها که تو با طاغیان کنی </p></div>
<div class="m2"><p>با عادیان نکرد چنان کار زار باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بادی بوقت حمله و رخش تو آتشست </p></div>
<div class="m2"><p>هر گر که دید گشته بر آتش سوار باد </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باشد بپیش حزم تو چون باد کوهسار </p></div>
<div class="m2"><p>باشد بپیش عزم تو چون کوهسار باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در سیر خامهٔ تو چو بادست ، اگر کند </p></div>
<div class="m2"><p>بر صفحهٔ بیاض جواهر نثار باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از باد صولت کند احتراز خاک </p></div>
<div class="m2"><p>و ز خاک درگه تو کند افتخار باد </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مخمور وار جوهر بادست بی قرار </p></div>
<div class="m2"><p>گویی که دارد از می سهمت خمار باد </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حلم ترا شدست جام تیغ تو دهر بلاد خاک </p></div>
<div class="m2"><p>بردست بوی خلق تو در هر دیار باد </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر باد را بگیرد حلم تو ناصیت </p></div>
<div class="m2"><p>بر جای همچو کوه شود استوار باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان مار شکل نیزه ، که باشد بدست تو</p></div>
<div class="m2"><p>بی دست و پای گشته بمانند مار باد </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در سیر خامهٔ تو چو با دست ، ارکند </p></div>
<div class="m2"><p>بر صفحهٔ بیاض جواهر نثار باد </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پوشد هزار گونه زره هر سپید دم </p></div>
<div class="m2"><p>از بیم زخم گرز تو در جویبار باد </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با دوستان بجود کنی آنچه می کنند </p></div>
<div class="m2"><p>با بوستان بتقویت نو بهار باد </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاها ، منم که چرخ پراگند در جهان </p></div>
<div class="m2"><p>اشعار من ، چنانکه از آتش شرار باد </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با سرعت بدیههٔ من وقت امتحان </p></div>
<div class="m2"><p>دم کی زند چو من ز سر اقتدار باد ؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در پیش سیر خامهٔ چون مار در کفم</p></div>
<div class="m2"><p>بی دست و پای ماند مانند مار باد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با صفوت فضایل من هست تیره آب </p></div>
<div class="m2"><p>با عزت شمایل من هست خوار باد </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نی ، همچو ذکر من ، بگه اشتهار مهر </p></div>
<div class="m2"><p>نی ، همچو صیت من ، بگه انتشار باد </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از بی شمار لطف ، که در خاطر منست </p></div>
<div class="m2"><p>غیرت برد ز خاطر من بی شمار باد </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دارم ، چو کوه ،از کف تو در کنار زر </p></div>
<div class="m2"><p>زان پس که داشتم ، چو فضا ، در کنار باد </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا نیست در صفای طبیعتی چو آب خاک </p></div>
<div class="m2"><p>تا نیست در علو حقیقتی چو نار باد </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باد از جهان عدوی ترا جایگاه خاک </p></div>
<div class="m2"><p>باد از فلک حسود ترا یادگار باد </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در دست ناصحان تو چون کیمیا گیا </p></div>
<div class="m2"><p>بر شخص حاسدان تو چون ذوالفقار باد </p></div></div>