---
title: >-
    شمارهٔ ۹۷ - در مدح ملک اتسز
---
# شمارهٔ ۹۷ - در مدح ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>مظلم شبی دراز از طرهٔ نگار </p></div>
<div class="m2"><p>گشته سیه زمان و شده تیر روزگار </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افلاک شسته چهرهٔ خود را برنک تیر</p></div>
<div class="m2"><p>و آفاق کرده جامهٔ خود را بلون قار </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خلق تنگ گشته مساکن چو کام کور </p></div>
<div class="m2"><p>بر چرخ داده نور کواکب چو چشم مار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب پر بلا و واقعه چون روز رستخیز </p></div>
<div class="m2"><p>ره پر نهیب و حادثه چون خشم کردگار </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من همچو آتشی بصمیم شب اندرون </p></div>
<div class="m2"><p>ظلمت مراد خان و کواکب مرا شرار </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تازان گهی چو شعلهٔ آتش سوی هوا </p></div>
<div class="m2"><p>یازان گهی چو قطرهٔ باران سوی قفار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مالیده گشت قالبم از پای آسمان </p></div>
<div class="m2"><p>فرموده گشت پیکرم از دست اضطرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی نی ، که اندرین ره مهلک نداشتم </p></div>
<div class="m2"><p>جز عیش هیچ صنعت و جز لهو هیچ کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خدمت رکاب علایی گذشت خوش </p></div>
<div class="m2"><p>آن هول بی کرانه و آن خوف بی شمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد در رکاب تاخته وز دستبرد او </p></div>
<div class="m2"><p>ایام در تعجب و گردون در اعتبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عنقای مهر خورده ز زوبین او دلم </p></div>
<div class="m2"><p>تنین چرخ گشته ز پیکان او فگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از بانک صید گشته همه کوه ناله گاه </p></div>
<div class="m2"><p>وز خون کشته گشته همه دشت لاله زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شیران شرزه را شده از بیم تیر او </p></div>
<div class="m2"><p>دل همچو تفته نار و جگر همچو کفته نار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیر زمین که باشد؟کاقبال اتسزی </p></div>
<div class="m2"><p>بی عون دور شیر فلک را کند شکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اختر نکرد یارد بی امر او مسیر </p></div>
<div class="m2"><p>گردون نکرد یارد بی حکم او مدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با رأی او چو ماه سپر ماه آسمان </p></div>
<div class="m2"><p>با سهم او چو شیر علم شیر مرغزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنجا که عزم او ، نه شریفتست آسمان</p></div>
<div class="m2"><p>و آنجا که حزم او ، نه متینست کوهسار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دریا بپیش بخشش او نیست جز شمر</p></div>
<div class="m2"><p>گردون بجنب همت او نیست جز غبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در ظل او بماند نکوه خواه با نوا</p></div>
<div class="m2"><p>وز تیغ او ندید بد اندیش زینهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از وی شب موافق او گشته همچو روز</p></div>
<div class="m2"><p>وز وی گل مخالف او گشته همچو خار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از عون رأی او شده دست هنر قوی</p></div>
<div class="m2"><p>وز سهم عدل او شده شخص ستم نزار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای در سخا شده بهمه جای مشتهر</p></div>
<div class="m2"><p>وی در وفا شده ز همه خلق اختیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در عزم همچو بادی و در حزم همچو کوه</p></div>
<div class="m2"><p>در لطف همچو آبی و در عنف همچو نار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در دست عقل تیغی و در پیش دین سپر</p></div>
<div class="m2"><p>بر پای ظلم بندی و بر دست حق سوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون دهر پایداری و چون چرخ باشکوه</p></div>
<div class="m2"><p>چون کوه مایه داری و چون بحر کامگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حکم ترا مضای قدر بوده پیش رو</p></div>
<div class="m2"><p>امر ترا نفاذ قضا بوده پیش کار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همواره تا بتابد بر چرخ مهر و ماه</p></div>
<div class="m2"><p>پیوسته تا بروید از شاخ و برگ و بار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یمنت همیشه باد شب و روز بر یمین</p></div>
<div class="m2"><p>یسرت همیشه باد مه و سال بر یسار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بادا بهار حاسد جاه تو چون خزان</p></div>
<div class="m2"><p>بادا خزان ناصح ملک تو چون بهار</p></div></div>