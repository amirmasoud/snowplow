---
title: >-
    شمارهٔ ۱۶۲ - د رمدح اتسز
---
# شمارهٔ ۱۶۲ - د رمدح اتسز

<div class="b" id="bn1"><div class="m1"><p>نظام حال زمانه ، قوام کار جهان </p></div>
<div class="m2"><p>تمام گشت باقبال شهریار جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علا دولت و دین خسرو جهان ، اتسز</p></div>
<div class="m2"><p>کزو گرفت نظام و قرار کار جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مظفری ، که خدای جهان پدید آورد </p></div>
<div class="m2"><p>ز بی قرارحسامش همه قرار جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخسروان جهان مثل او جوان بختی</p></div>
<div class="m2"><p>نپرورد فلک پیر در کنار جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ربوده زلزلهٔ هیبتش قرار زمین</p></div>
<div class="m2"><p>ببرده قاعدهٔ عدلش اقتدار جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر امر و نهی مساعیش اتفاق فلک </p></div>
<div class="m2"><p>بحل و عقد معالیش افتخار جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه پاسا، دریادلا ، فلک قدرا </p></div>
<div class="m2"><p>شدست خدمت صدر تو اختیار جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قضا اسیر تو و سرکشان اسیر قضا</p></div>
<div class="m2"><p>جهان شکار تو و صفدران شکار جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطیع حکم تو اجرام بی قیاس فلک</p></div>
<div class="m2"><p>غلام امر تو اجسام بی شمار جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدیدهای کواکب ز عهد آدم باز </p></div>
<div class="m2"><p>وجود ملک ترا بوده انتظار جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگاه مکرمت ، ای یمن و یسر بندهٔ تو </p></div>
<div class="m2"><p>بر یمین تو اندک بود یسار جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز پای پند نهیب تو احتراز سپهر </p></div>
<div class="m2"><p>ز دستبر حسام تو اعتبار جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگاه بزم ببخش تویی جهان سخا</p></div>
<div class="m2"><p>بروز رزم بکوشش تویی سوار جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شعار خدمت درگاه تو نشان فلک </p></div>
<div class="m2"><p>نشان طاعت فرمان تو شعار جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنظم قاعدهٔ ملک تو نظام هدی </p></div>
<div class="m2"><p>ز خمر صاعقهٔ تیغ تو خمار جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز عنف تست وز لطف تو خوف و امن بشر</p></div>
<div class="m2"><p>ز مهر تست وز کین تو فخر و عار جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدای عز و جل سخرهٔ ضمیر تو کرد </p></div>
<div class="m2"><p>همه دقایق پنهان و آشکار جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همیشه تا که بصنع خدای عز و جل </p></div>
<div class="m2"><p>بر اختلاف طبایع بود مدار جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مباد کس زبشر ، جز تو ، پیشوای بشر </p></div>
<div class="m2"><p>مباد کس بجهان ، جز تو شهریار جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مخالفان تو در خوف حادثات فلک</p></div>
<div class="m2"><p>موافقان تو در عهد زینهار جهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بهر عدد جود تو مایهای زمین</p></div>
<div class="m2"><p>ز بهر خدمت عمر تو روزگار جهان</p></div></div>