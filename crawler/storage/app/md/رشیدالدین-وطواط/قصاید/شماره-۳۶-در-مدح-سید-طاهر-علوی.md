---
title: >-
    شمارهٔ ۳۶ - در مدح سید طاهر علوی
---
# شمارهٔ ۳۶ - در مدح سید طاهر علوی

<div class="b" id="bn1"><div class="m1"><p>آنی ، که حشمت تو در ایام ظاهرست </p></div>
<div class="m2"><p>ذات تو از عیوب چو نام تو طاهرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرخنده رعایت شرف تو بهر مکان</p></div>
<div class="m2"><p>چون آیت بنوت جد تو ظاهرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کف باذل تو قوام مکارمست</p></div>
<div class="m2"><p>و ز جاه کامل تو نظام مفاخرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چرخ محمدت هنر تو کواکبست </p></div>
<div class="m2"><p>و زکان مکرمت اثر تو جواهرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بایسته صورت تو بری از معایبست</p></div>
<div class="m2"><p> شایسته سیرت تو جدا از مفاخرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق را ، گه بیان ، کلمات تو مظهرست</p></div>
<div class="m2"><p>دین را، گه بقا، سطوات تو ناصرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حشمت منیع تو ایام عاجزست</p></div>
<div class="m2"><p>از همت رفیع تو افلاک قاصرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر ضیا بنان تو خورشید انورست</p></div>
<div class="m2"><p>وندر ثنا بیان تو دریای ذاخرست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آیات بخل را کف راد تو ناسخست</p></div>
<div class="m2"><p>ارکان علم را دل پاک تو عامرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حزم تو هچو عزم متین تو صایبست</p></div>
<div class="m2"><p>علم تو همچو حلم مبین تو وافرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چان نموده شد زر سومت بدایعست</p></div>
<div class="m2"><p>هر چان ستوده شد زخصالت نوادرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر قمع خیل نیستی و لشکر ستم</p></div>
<div class="m2"><p>خیل سخا و لشکر عدل تو قادرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندر همه نواحی عالم ، بشرق و غرب </p></div>
<div class="m2"><p>چون ذکر سایر تو عطای تو سایرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دارند قصد سایر و زایر بتو از انک </p></div>
<div class="m2"><p>صدرت پناه سایر و مساوای زایرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جوید همیشه بخت خجسته مجاورت</p></div>
<div class="m2"><p>با آنکه او خجسته درت را مجاورست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای صدر آل حیدر ، اگر غایبم زتو </p></div>
<div class="m2"><p>اندر دلم محبت صدر تو حاضرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دارد تنم زخدمت اهل زمانه عار</p></div>
<div class="m2"><p>لیکن بخدمت در والات فاخرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر حمد و شکر مدح تو مقصور کرده ام </p></div>
<div class="m2"><p>تا در تنم دلست و زبانست و خاطرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من خود کیم که شکر تو گویم؟که در بهشت</p></div>
<div class="m2"><p>جان نبی و جان وصی از تو شاکرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همواره تا زمین بر اجسام ساکنست</p></div>
<div class="m2"><p>پیوسته تا سپهر بر اجرام سایرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در کارها باول و آخرت یار باد</p></div>
<div class="m2"><p>آنکس که اول همه اشیا و آخرست</p></div></div>