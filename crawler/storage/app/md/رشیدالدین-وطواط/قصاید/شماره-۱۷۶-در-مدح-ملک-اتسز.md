---
title: >-
    شمارهٔ ۱۷۶ - در مدح ملک اتسز
---
# شمارهٔ ۱۷۶ - در مدح ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>هست دولت را اساس و هست ملت را پناه</p></div>
<div class="m2"><p>حضرت خوارزمشاه و خدمت خوارزمشاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو عادل ، علاء دولت ، آن کز عدل اوست</p></div>
<div class="m2"><p>هم خلایق را امان و هم شرایع را پناه </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسند خوارزمشاهی تا مسلم شود بدو </p></div>
<div class="m2"><p>شرع را بفزود قدر و ملک را بفزود جاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داعیان کینه را تأیید او خستست جان</p></div>
<div class="m2"><p>ساعیان فتنه را تهدید او بستست راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پناه دولت او کاه گردد همچو کوه</p></div>
<div class="m2"><p>وز شکوه هیبت او کوه گردد همچو کاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه بی مهرست عالم، کی کند مهرش رها ؟</p></div>
<div class="m2"><p>ور چه بد عهدست ، گیتی کی کند عهدش تباه؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه تیره گردد از شرم جمالش وقت وقت</p></div>
<div class="m2"><p>ابر نوحه گیرد از رشک نوالش گاه گاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست جود او گشاده مایهٔ دریا و کوه</p></div>
<div class="m2"><p>پای قدر او سپرده تارک خورشید و ماه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک پیام او نهد بر خصم بار صد حسام </p></div>
<div class="m2"><p>یک غلام او کند در حرب کار صد سپاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نهیب او منقض گشته عمر بدسگال</p></div>
<div class="m2"><p>وز عطای او مهنا گشته عیش نیک خواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لفظ او چون لفظ یوسف فارغ از زرق و دروغ </p></div>
<div class="m2"><p>ذات او چوت ذات یحیی خالی از لغو و گناه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان دشمن وقت فرمانش چو فرمانش روان</p></div>
<div class="m2"><p>پشت گردون پیش ایوانش چو ایوانش دو تاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همت او را نماید ، گر بپستی بنگرد</p></div>
<div class="m2"><p>چشمهٔ خورشید همچون چشم مور از قعر چاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای خداوندی که اطراف ممالک را هنوز </p></div>
<div class="m2"><p>دارد از هر آفتی اطراف تیغ تو نگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دستگاه بحر داری پایگاه آسمان</p></div>
<div class="m2"><p>اینت کامل دستگاه و اینت کامل پایگاه!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر تو با اهل جهان موجود گشستسی چه شد؟</p></div>
<div class="m2"><p>نه شود موجود گل با خار و لاله با گیاه ؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا که باشد عاقلی را از معالی افتخار</p></div>
<div class="m2"><p>تا که افتد عالمی را در معانی اشتباه </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو کلاه خسروی دار و قبای عدل پوش</p></div>
<div class="m2"><p>تو نظام مملکت افزای و جان خصم کاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه طبع تو ز وصل نیکوان دیده طرب</p></div>
<div class="m2"><p>گاه چشم تو بروی دلبران کرده نگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از قباه و از کلاه تو نصیب دشمنان </p></div>
<div class="m2"><p>باد تصحیف قبا و باد مقلوب کلاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همچو روی دلبران چشم عدوی تو سپید</p></div>
<div class="m2"><p>همچو چشم نیکوان روز حسود تو سیاه</p></div></div>