---
title: >-
    شمارهٔ ۱۶۶ - نیز در مدح اتسز
---
# شمارهٔ ۱۶۶ - نیز در مدح اتسز

<div class="b" id="bn1"><div class="m1"><p>چو از حدیقهٔ مینای چرخ سقلاطون</p></div>
<div class="m2"><p>نهفته گشت علامات چتر آینه گون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نقشهای عجیب و ز شکلهای غریب </p></div>
<div class="m2"><p>صحیفه های فلک شد چو صحف انگلیون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جناح نسر و سلاح سماک هر دو شدند </p></div>
<div class="m2"><p>ز دست چرخ مرصع بلؤلو مکنون </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحسن روی قمر همچو طلعت لیلی </p></div>
<div class="m2"><p>بضعف شکل سها همچو قالب مجنون </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعاع شعری اندر میان ظلمت شب </p></div>
<div class="m2"><p>چنان که در دل جهال و هم افلاطون </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهاب همچو حسامی برهنه کرده بحرب </p></div>
<div class="m2"><p>سهیل همچو سنانی خضاب کرده بخون </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی دراز وز حیرت فلک درو ساکن </p></div>
<div class="m2"><p>و لیکن از دل من برده هجر یار سکون </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهی ، که کردتم را ببند فتنه اسیر</p></div>
<div class="m2"><p>بتی ، که کرد دلم را بدست عشوه زبون </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبان من شده در وصف زلف او عاجر </p></div>
<div class="m2"><p>روان من شده بر نقش روی او مفتون </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو نون و چون الفست او بابرو و بالا</p></div>
<div class="m2"><p>وزوست قد الف شکل من خمیده چو نون </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فراق یار بود صعب در همه هنگام </p></div>
<div class="m2"><p>و لیک باشد هنگام نوبهار فزون </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون که دست طبایع بسان فراشان </p></div>
<div class="m2"><p>بباغ وراق ستبرق فگند و بوقلمون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فشاند مشک و قرنفل بجای گرد ریاح</p></div>
<div class="m2"><p>نمود لعل و زبرجد بجای میوهٔ غصون </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنار باغ همه پر خزاین دارا </p></div>
<div class="m2"><p>فضای راغ همه پر دفاین قارون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فراق از گل و گلرخ بدین چنین فصلی</p></div>
<div class="m2"><p>ز امهات جنونست و الجنون فنون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منم که بهر تماشای باغ، همچو صبا</p></div>
<div class="m2"><p>ز لهو رفتم و رفتم ز باغ و راغ برون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بران براق نشستم ، که هست پیکر او</p></div>
<div class="m2"><p>چو بیستونی ، در زیر او چهار ستون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گهی چو شکل پلنگان دونده بر کهسار</p></div>
<div class="m2"><p>گهی بشبه نهنگان رونده در جیحون </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بزیر زینش نیایی بوقت پویه شموس</p></div>
<div class="m2"><p>بزیر رانش نبینی بگاه وقفه حرون </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهاده رخ برهی ، کندرو نیابد کس</p></div>
<div class="m2"><p>بجز لقای فنا و بجز خیال منون </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هزار خوف در اطراف او شده موجود </p></div>
<div class="m2"><p>هزار فتنه باکناف او شده مقرون </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قرارگاه افاعی همه جبال و قفار </p></div>
<div class="m2"><p>مقامگاه شیاطین همه سهول و حرون </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درو بهیبت نازل نوایب گیتی </p></div>
<div class="m2"><p>درو بعبرت ناظر کواکب گردون </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز سهم راه مرا آیت طرب منسوخ </p></div>
<div class="m2"><p>ز هجر یار مرا رایت نشاط نگون </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گهی چو هامون از آتش دلم دریا </p></div>
<div class="m2"><p>گهی چو. دریا از آب دیده ام هامون </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز بهر حفظ تن و جانم اندرو خوانده </p></div>
<div class="m2"><p>ثنای صدر بزرگ خدایگان چو فسون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عنا بلهو بدل شد ، چو سوی حضرت شاه</p></div>
<div class="m2"><p>مرا ستارهٔ اقبال گشت راهنمون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ابوالمظفر ، خورشید خسروان ، اتسز</p></div>
<div class="m2"><p>که هست تابع حکمتش قضای کن فیکون </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بجنت جاه بزرگش فضای عالم خرد</p></div>
<div class="m2"><p>بپیش قدر بلندش محل گردون دون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بمهر حضرت او جان عاقلان مشعوف </p></div>
<div class="m2"><p>بدست منت او شخص فاضلان مرهون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خدایگانا ، آنی که در خرد نارد</p></div>
<div class="m2"><p>قران انجم گردون قرین تو بقرون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببحر کف تو قواص مکرمات از در</p></div>
<div class="m2"><p>سفینهای امل را همی کند مشحون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زسعی بخت تو اقبال کوکب مسعود</p></div>
<div class="m2"><p>بگرد صدر تو پرواز طایر میمون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>محیط فضل و هنر را ضمیر تو مرکز</p></div>
<div class="m2"><p>حساب مجد و شرف را جلال تو قانون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بیت احزان یاد تو سلوت یعقوب</p></div>
<div class="m2"><p>بجوف ماهی نام تو دعوت ذوالنون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هزار صاعقه در یک شکوه تو مضمر</p></div>
<div class="m2"><p>هزار فایده در یک حدیث تو مضمون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بقدر مرتبه دار تو همچو کیکاوس </p></div>
<div class="m2"><p>بجاه غاشیه دار تو همچو افریدون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز شخص تیر فلک سهم تو ربوده حیات</p></div>
<div class="m2"><p>ز فرق گاو زمین باس تو شکسته سرون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هوای بزم بطیب سخای تو ممزوج</p></div>
<div class="m2"><p>زمین رزم بخون عدوی تو معجون</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برنده نسل عدو خنجر تو چون کافور</p></div>
<div class="m2"><p>سپرده هوش یلان هیبت تو چون افیون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز وصف بر تو عاجز شده بیان عقول</p></div>
<div class="m2"><p>ز کنه قدر تو قاصر شده مجال ظنون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بامر و نهی ترا بهر چاکری منقاد</p></div>
<div class="m2"><p>بحل و عقد ترا چرخ بنده ای ماذون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز هر ذنوب دل تو منزهست و بری </p></div>
<div class="m2"><p>ز هر عیوب تن تو مطهرست و مصون</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بحشمت تو قوی گشت پشت دین رسول</p></div>
<div class="m2"><p>چو پشت موسی عمران بشرکت هارون</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بشد خلاف دربان کاخ مأمونی </p></div>
<div class="m2"><p>بعهد تو ز شرف چون خلاف مامون</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو بیضهٔ حرمست و چون روضهٔ ارمست</p></div>
<div class="m2"><p>به ایمنی و خوشی از تو این بلاد اکنون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سکون گرفت و مطهر شد از همه آفات </p></div>
<div class="m2"><p>ز حد ری بحسام تو تا بآبسکون</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر عدوی ترا در سرست سودایی </p></div>
<div class="m2"><p>بدفع سودا تیغت بسست افتینون</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همیشه تا که بود در فراق عاشق را</p></div>
<div class="m2"><p>دلی چو آذر و رخساره ای چو آذریون</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>موافقان تو بادند سال و مه مسرور</p></div>
<div class="m2"><p>مخالفان تو بادند روز و شب محذون </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه حدیث خلایق ثنای صدر تو باد</p></div>
<div class="m2"><p>و گر چه هست در امثال : کلحدیث شجون</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بحشمت تو شده رام گنبد توسن</p></div>
<div class="m2"><p>بهیبت تو شده نرم اختر وارون</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز حادثات جهان و ز نایبات فلک </p></div>
<div class="m2"><p>نگاهدار تن و جانت ایزد بی چون</p></div></div>