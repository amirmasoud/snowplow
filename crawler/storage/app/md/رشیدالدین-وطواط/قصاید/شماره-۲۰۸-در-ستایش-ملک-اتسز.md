---
title: >-
    شمارهٔ ۲۰۸ - در ستایش ملک اتسز
---
# شمارهٔ ۲۰۸ - در ستایش ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>ای بازوی شریعت از اقبال تو قوی</p></div>
<div class="m2"><p>تابنده از جمال تو آثار خسروی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گه که در مهم معالی ندا دهی</p></div>
<div class="m2"><p>جز پاسخ متابعت از چرخ نشوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتنه غنوده گشت در ایام تو، که تو</p></div>
<div class="m2"><p>از بهر قهر فتنه همی هیچ نغنوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کف گرفته خنجر چون آب و پس بطبع </p></div>
<div class="m2"><p>آتش‌ نهاده سوی معالی همی روی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر روز و هر شب از فلک اندر کنار ملک </p></div>
<div class="m2"><p>اقبال تو نوست و در اقبال تو نوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتست ملک مزرعهٔ تو، که اندرو</p></div>
<div class="m2"><p>جز مکرمت نکاری و جز شکر ندروی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سهم تو مشرکان جهان را همی ‌کند </p></div>
<div class="m2"><p>اندر مضیق زاویهٔ مرک منزوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ و زمین و هرچه در این دو میانه اند</p></div>
<div class="m2"><p>باشند در طفیل تو جایی که تو روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از حسن نقش صورت توقیعهای تو</p></div>
<div class="m2"><p>منسوخ گشت نقش تصاویر مانوی </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هستند از طریق مساوات نزد عقل </p></div>
<div class="m2"><p>جاه تو و فلک چو دو مصراع مثنوی </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عزی که بیند از تو همی خاک درگهت</p></div>
<div class="m2"><p>هرگز ندیده مسند و ایوان کسروی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>موقوف بر دعای تو تازی و پارسی</p></div>
<div class="m2"><p>مقصود بر ثنای تو عبری و پهلوی </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لطف بر ولی تو ظلی بود ظلیل </p></div>
<div class="m2"><p>عنف تو بر عدوی تو دایی بود دوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اوصاف تو مبارک و آثار تو رضی</p></div>
<div class="m2"><p>افعال تو مهذب و اخلاق تو سوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو از میان زمرهٔ شاهان ممیزی</p></div>
<div class="m2"><p>چون از میان زمرهٔ اشراف موسوی </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرگز ز کید هیچ مصنعی تو نشکنی</p></div>
<div class="m2"><p>هرگز بزرق هیچ مزور تو نگروی </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهج صیانت تو صراطیست مستقیم</p></div>
<div class="m2"><p>راه دیانت تو طریقیست مستوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بعد از کلام ایزد و اخبار مصطفی </p></div>
<div class="m2"><p>در راه شرع قول تو مقبول و معنوی </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طبع تو بر دقایق ایام مطلع </p></div>
<div class="m2"><p>لفظ تو بر حقایق آفاق محتوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حساد را حسام تو پی کرد و پست کرد</p></div>
<div class="m2"><p>اقدام با تعدی و اعناق ملتوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جاه تو ایمنست ز عین الکمال، از آنک</p></div>
<div class="m2"><p>در جاه هر زمان تو فزون تر همی شوی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا از حروف هست در اعجاز قافیت</p></div>
<div class="m2"><p>تأسیس ردف و حرف صلت حایل و روی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرگز مباد نقش جلال تو مندرس </p></div>
<div class="m2"><p>هرگز مباد فرش کمال تو منطوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر قمع کفر و نصر هدی باد تا بحشر</p></div>
<div class="m2"><p>شمشیر تو برنده و بازوی تو قوی </p></div></div>