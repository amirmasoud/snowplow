---
title: >-
    شمارهٔ ۹۵ - در مدح اتسز
---
# شمارهٔ ۹۵ - در مدح اتسز

<div class="b" id="bn1"><div class="m1"><p>زهی! خطهٔ ملک‌ را شهریار </p></div>
<div class="m2"><p>خهی! از تو بنیاد دین استوار </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تن را بجان و چو جان را بعلم</p></div>
<div class="m2"><p>بجاه تو اسلام را افتخار </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه در بیضهٔ حشمت تو خلل </p></div>
<div class="m2"><p>نه در صفحهٔ دولت تو غبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان همچو اطفال را مادران </p></div>
<div class="m2"><p>بپرورده ملک ترا در کنار </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک بر مراد تو گشته روان</p></div>
<div class="m2"><p>زمین بر رضای تو داده قرار </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مهر تو واضح بشارات فخر</p></div>
<div class="m2"><p>ز کین تو لایح اشارات عار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه در مجلس بزم چون تو جواد </p></div>
<div class="m2"><p>نه در عرصهٔ رزم چون تو سوار </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پیدا شود آیت رستخیز </p></div>
<div class="m2"><p>چو شعله زند آتش کارزار </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خون تر شود دامن آسمان </p></div>
<div class="m2"><p>ز غم خون شود زهرهٔ روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بتفسد هوا از تیغ و تیر </p></div>
<div class="m2"><p>بلرزد زمین از غو گیر ودار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود باده عیش هم طعم زهر</p></div>
<div class="m2"><p>شود چهرهٔ روز همرنگ قار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز صف باره غرنده چون شر ز مشیر </p></div>
<div class="m2"><p>بکف نیزه پیچنده چون گرزه مار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بس نیزه بیشه اطراف دشت </p></div>
<div class="m2"><p>ز بس کشته چون پیشه اکناف غار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرو بسته افواه مردا ز نطق</p></div>
<div class="m2"><p>فرو مانده اعضای گردان ز کار </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امل سرکشیده ز بهر امان </p></div>
<div class="m2"><p>اجل پر گشاده بحرص شکار </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در آن حال از تیغ جان‌سوز تر</p></div>
<div class="m2"><p>نیابد بجان هیچ کس زینهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکوبی سر سرکشان را چو گوی</p></div>
<div class="m2"><p>بدری دل گردنان را چو نار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هوا گردد از رمح تو ناله گاه</p></div>
<div class="m2"><p>زمین گردد از تیغ تو لاله‌زار </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بآیات تیغ و علامات رمح</p></div>
<div class="m2"><p>کنی صفحهٔ فتح را پرنگار </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زهی! یمن جیش ترا بر یمین </p></div>
<div class="m2"><p>خهی! یسر خیل ترا بر یسار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترا محمدتها فزون از قیاس</p></div>
<div class="m2"><p>ترا مکرمتها برون از شمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهار عدو از تو همچون خزان </p></div>
<div class="m2"><p>خزان ولی از تو همچون بهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز گردان ترانیست در حرب جفت</p></div>
<div class="m2"><p>ز شاهان ترانیست در جنگ یار </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی تا ز افلاک تابد نجوم </p></div>
<div class="m2"><p>همی تا ز اشجار آید ثمار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شب نیک‌خواه تو بادا چو روز</p></div>
<div class="m2"><p>گل بدسکال تو بادا چو خار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همایونت عید و پذیرفته صوم</p></div>
<div class="m2"><p>ولی کامگار و عدو خاکسار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ستوده خصال ترا آدمی </p></div>
<div class="m2"><p>فزوده جلال ترا کردگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برآورده‌ از دشمن مملکت </p></div>
<div class="m2"><p>بتیغ چو آب و چو آتش دمار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترا یادگارست از اسلاف فلک </p></div>
<div class="m2"><p>و لیکن مبادا ز تو یادگار</p></div></div>