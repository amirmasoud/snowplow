---
title: >-
    شمارهٔ ۱۳۹ - در مدح ادیب صابر بن اسمعیل ترمذی
---
# شمارهٔ ۱۳۹ - در مدح ادیب صابر بن اسمعیل ترمذی

<div class="b" id="bn1"><div class="m1"><p>بدیع شعر تو ، ای صابر بن اسمعیل </p></div>
<div class="m2"><p>مرا بسوی امانی وامن گشت دلیل </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بساحت تن و در جان من بهم کردند </p></div>
<div class="m2"><p>قصیدهٔ تو نزول و سپاه رنج رحیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصیده ای همه الفاظ او نشاط حزین </p></div>
<div class="m2"><p>قصیده ای همه ابیات او شفای علیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلیل مرتبه ، لیکن دقیق در معنی</p></div>
<div class="m2"><p>کثیر فایده ، لیکن ز روی لفظ قلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سلسبیل بود لفظ تو لطیف ، مگر </p></div>
<div class="m2"><p>که سلسبیل سخن بر تو کردن سبیل ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی ریاحین خیزد ترا ز آتش طبع </p></div>
<div class="m2"><p>مگر تو داری میراث معجزات خلیل ؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان ز شعر تو پوشد ملابس زینت </p></div>
<div class="m2"><p>فلک ز نطم تو سازد جواهر اکلیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>متانتیست ترا در هنر ، رفیع و منیع </p></div>
<div class="m2"><p>ولایتیست ترا در سخن ، عریض و طویل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعلم بر همه عالم بود ترا ترجیح </p></div>
<div class="m2"><p>بفضل بر همه گیتی ترا تفضیل </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایا بلند ضمیری ، که در فنون هنر </p></div>
<div class="m2"><p>شدست طبع تو آگاه از دقیق و جلیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزادن چو تو فحل و بدادن چو توشهم</p></div>
<div class="m2"><p>زمانه گشت عقیم و ستاره گشت بخیل </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تراست هرچه معالیست ، اندک و بسیار </p></div>
<div class="m2"><p>تراست هر چه معانیست ، جمله و تفضیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تویی امیر امور ولایت دانش </p></div>
<div class="m2"><p>در آن ولایت جز تو همه غریب و دخیل </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سواد خط تو کحلیست بر بیاض صحف</p></div>
<div class="m2"><p>کزوست چشم عروسان نظم و نثر کحیل </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چگونه ای تو در اندوه حبس آن صدری</p></div>
<div class="m2"><p>که در معالی و عقلست چون علی و عقیل ؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه عهد بود که در مجلس مقدس تو </p></div>
<div class="m2"><p>بشعر جزیل همی یافتی عطای جزیل ؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گهت رسیدی از جود دست او انعام </p></div>
<div class="m2"><p>گهت رسیدی از سعی جاه او تبجیل </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چگونه باشد در حبس ، آنکه بود او را </p></div>
<div class="m2"><p>سرای پردهٔ حشمت کشید میل بمیل ؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چگونه صبر کند از مکارم و افضال </p></div>
<div class="m2"><p>کسی که بود با رزاق اهل فضل کفیل؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر ز حبس بحبسش همی برند بقهر</p></div>
<div class="m2"><p>چه شد ؟ نه برج ببر جست شمس را تحویل ؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی تواند در حبس دیدنش گردون ؟ </p></div>
<div class="m2"><p>کشیده بادا در دیدهای گردون میل ؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رسیده شعر تو ، ای بی دلیل در هر باب </p></div>
<div class="m2"><p>بلهو کرد همه انده مرا تبدیل </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بجان خستهٔ من کرد نامهٔ تو ز لطف </p></div>
<div class="m2"><p>چنانکه جامهٔ یوسف بچشم اسراییل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدیع نیست چنان عهد و صدق و لطف و وفا</p></div>
<div class="m2"><p>از آن خصال حمید و از آن جمال جمیل </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تبارک الله ! هرگز بود بر غم فلک </p></div>
<div class="m2"><p>مرا بصحن جوار تو در مبیت و مقیل ؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رسیده از کنف جاه تو بحصن حصین </p></div>
<div class="m2"><p>رسیده از لطف لطف تو بظل ظلیل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ثنای تست عدیل زبان من پیوست </p></div>
<div class="m2"><p>اگر چه نیست مرا در زمانه هیچ عدیل </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از آن نویسم کمتر ، که خدمتی دانم </p></div>
<div class="m2"><p>نگاه داشتن مجلس تو از تثقیل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه تا که بود در بسیطهٔ گیتی </p></div>
<div class="m2"><p>یکی ز بخت عزیز و یکی ز چرخ ذلیل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بتو مراسم ؟ آداب زنده باد و عدوت </p></div>
<div class="m2"><p>بتیغ حادثهٔ روزگار باد قتیل </p></div></div>