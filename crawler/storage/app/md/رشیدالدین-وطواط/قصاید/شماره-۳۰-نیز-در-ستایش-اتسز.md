---
title: >-
    شمارهٔ ۳۰ - نیز در ستایش اتسز
---
# شمارهٔ ۳۰ - نیز در ستایش اتسز

<div class="b" id="bn1"><div class="m1"><p>جانا ، رهی ز مهر تو بردل رقم زدست</p></div>
<div class="m2"><p>مردانه وار در صف عشقت قدم زدست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر جان ز حادث زمانه رقم زدند </p></div>
<div class="m2"><p>آنرا که او ز عشق تو بر دل رقم زدست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس دل که در رکاب تو دست متابعت </p></div>
<div class="m2"><p>اندر دوال گوشهٔ فتراک غم زدست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم ز هجرت ، ای بقم از روی تو خجل</p></div>
<div class="m2"><p>بر برگ زعفران من آبقم زدست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر پشت غم گرفته زد امروز چاکرت </p></div>
<div class="m2"><p>دستی که دی در آن سر زلف بخم زدست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرمایهٔ طرب دل من بر بساط عشق </p></div>
<div class="m2"><p>با نقش کعبتین خیال تو کم زدست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر دهد مرا ز ستمهای تو خلاص </p></div>
<div class="m2"><p>شاهی که عدل او کنف هر ستم زدست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو علاء دولت و دین ، آنکه همتش </p></div>
<div class="m2"><p>بر طارم سپهر ثوابت علم زدست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن خسروی که بر سر او از پی کنف</p></div>
<div class="m2"><p>از هفت چرخ حفظ خدایی خیم زدست </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در امر اوست هم عرب و هم عجم ، از آنک </p></div>
<div class="m2"><p>گرزش همه بلاد عرب بر عجم زدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسیار وقت از سر مردی بیک مقام </p></div>
<div class="m2"><p>در روی خصم ساغر و خنجر بهم زدست </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از بهر کردگار و پرستندگان او </p></div>
<div class="m2"><p>آتش بتیغ در دشمن و در صنم زدست </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنگو نخواستست بقای وجود او </p></div>
<div class="m2"><p>از منزل بقا قدم اندر عدم زدست </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواهد زمانه کرد در انگشت او همی </p></div>
<div class="m2"><p>آن خاتمی که از پی انگشت جم زدست </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای عاجز از تو وقت بیان ، آنکه لاف علم </p></div>
<div class="m2"><p>از کشف معضلات حدوث و قدم زدست </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواهد گشاده کرد کنون بر بیان تو </p></div>
<div class="m2"><p>آن قادری که عقدهٔ جذر اصم زدست </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>معنی زائد تو ندیدست در کرم </p></div>
<div class="m2"><p>آن کو ز معن زائده لاف کرم زدست </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیشت بسر هر آنکه نرفتست چون قلم </p></div>
<div class="m2"><p>از تن سرش حسام تو همچون قلم زدست </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خود دشمن تو دم نزدست از نهیب تو </p></div>
<div class="m2"><p>ور دم ز دست از سر کوی ندم زدست </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر دم زدن ز چرخ کشیدست صد بار </p></div>
<div class="m2"><p>آن کس که بر خلاف رضای تو دم زدست </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کامدست سوی تو دست امید را </p></div>
<div class="m2"><p>در دامن مکارم و بحر کرم زدست </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بادی همیشه در حرم حق ، ز بهر آنک </p></div>
<div class="m2"><p>عونت سرای پردهٔ حق در حرم زدست</p></div></div>