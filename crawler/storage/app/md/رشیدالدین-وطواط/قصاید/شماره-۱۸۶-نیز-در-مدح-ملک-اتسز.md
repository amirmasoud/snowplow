---
title: >-
    شمارهٔ ۱۸۶ - نیز در مدح ملک اتسز
---
# شمارهٔ ۱۸۶ - نیز در مدح ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>ای صیت دولت تو بعلم علم شده </p></div>
<div class="m2"><p>بدخواه دولت تو ندیدم ندم شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شاه شرق و غربی وز آثار عدل تو </p></div>
<div class="m2"><p>اطراف شرق و غرب چو صحن حرم شده </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبع مبارک تو و دست جواد تو </p></div>
<div class="m2"><p>دریای علم گشته و کان کرم شده </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیات بخشش تو و آثار کوششت </p></div>
<div class="m2"><p>اندر جهان نشان وجود و عدم شده </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لفظ تو در طویلهٔ دانش گهر شده </p></div>
<div class="m2"><p>نام تو در صحیفهٔ مردی رقم شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از روی ارتفاع محل و جلال قدر </p></div>
<div class="m2"><p>اکلیل آسمانت بزیر قدم شده </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرخنده بارگاه رفیع ترا بقدر </p></div>
<div class="m2"><p>دهر از عبید گشته و چرخ از خدم شده </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جاه تو دافع صدمات و بلا شده </p></div>
<div class="m2"><p>عقل تو کاشف ظلمات ستم شده </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر فضای دشت بایام عدل تو </p></div>
<div class="m2"><p>گرگ غنم ربای شبان غنم شده </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حشمت تو آیت حق منتشر شده </p></div>
<div class="m2"><p>وز حرمت تو بیضهٔ دین محترم شده </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از جود بیدریغ و ز انعام عام تو </p></div>
<div class="m2"><p>زوار با خزاین در ودرم شده </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از فعل و قول تو همه ارباب فضل را </p></div>
<div class="m2"><p>آغوش و گوش پر نعیم و پر نغم شده </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خصم تو سر بریده و سینه شکافته </p></div>
<div class="m2"><p>از تیغ و نیزهٔ تو بسان قلم شده </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مهر ستانهٔ تو و کین جناب تو </p></div>
<div class="m2"><p>منشور شادمانی و توقیع غم شده </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک بندهٔ تو روز قتال مخالفان </p></div>
<div class="m2"><p>در طعن و ضرب صاحب صدور ستم شده </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طبع موافق تو قرین طرب شده </p></div>
<div class="m2"><p>جان مخالف تو رهین الم شده </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندر میان باطل و حق در میان خلق </p></div>
<div class="m2"><p>عدلت براستی و درستی حکم شده </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای قبلهٔ ملوک جهان ، ای بحشمتت </p></div>
<div class="m2"><p>چندین هزار اهل هنر محتشم شده </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بودست مدتی ز جفاهای روزگار </p></div>
<div class="m2"><p>انوار عیش بنده سراسر ظلم شده </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه شخص بنده بستهٔ بند عنا شده </p></div>
<div class="m2"><p>گه جان بنده خستهٔ تیر ستم شده </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از شعلهای غم دل من پر ز تف شده </p></div>
<div class="m2"><p>وز قطره های خون رخ من پر ز نم شده </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مالی ، که آن بروز جوانیم بد بدست </p></div>
<div class="m2"><p>جمله شده ز دست و جوانیم هم شده </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منت خدای را که کنون هست بر دلم </p></div>
<div class="m2"><p>آن رنجها بعز قبول تو کم شده </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در چشم من دیار بخارا بخرمی </p></div>
<div class="m2"><p>از اصطناع تو چو ریاض ارم شده </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا امت پیمبر خیرالامم بود </p></div>
<div class="m2"><p>صدر تو باد مرجه خیرالامم شده </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر مسند جلال ترا پشت و پیش تو </p></div>
<div class="m2"><p>پشت همه سران زمانه بخم شده </p></div></div>