---
title: >-
    شمارهٔ ۱۹۵ - در مدح ملک اتسز
---
# شمارهٔ ۱۹۵ - در مدح ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>ز عشقت ای عمل غمزهٔ تو خون خواری </p></div>
<div class="m2"><p>بسی کشید تن مستمند من خواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مراست عیش دژم تا شدی ز دست آسان </p></div>
<div class="m2"><p>چگونه عیشی؟ با صد هزار دشواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان دو چشم دژم عیش من دژم خواهی </p></div>
<div class="m2"><p>بدان دو زلف سیه روز من سیه داری </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلب همی‌کنی آزار من، خداوندا </p></div>
<div class="m2"><p>روا بود که مرا بی‌گنه بیازاری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که پای تو بوسد چگونه دل دهدت </p></div>
<div class="m2"><p>که همچنین بگزافش ز دست بگذاری؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنفشه قد و سمن موی و لاله اشکم از آنک</p></div>
<div class="m2"><p>بنفشه جعد و سمن ساق و لاله رخساری </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی بگریم بر یاد تو بصد حسرت </p></div>
<div class="m2"><p>گهی بنالم در عشق تو بصد زاری </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر هزار چو من در فراق خود بکشی</p></div>
<div class="m2"><p>از آنت باک نیاید ، زهی ستمگاری! </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن برنج گرفتار بیش ازین دل من </p></div>
<div class="m2"><p>کزان بود بقیامت ترا گرفتاری </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دست جور توام کشته گیر ، اگر ندهد </p></div>
<div class="m2"><p>مرا عنایت عدل خدایگان یاری </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علاء دولت ، شاهی ، که دولت تیغش </p></div>
<div class="m2"><p>ببرد از سر بدخواه باد جباری </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدایگانی کاجرام چرخ را با او </p></div>
<div class="m2"><p>منازعت نرسد در بلند مقداری </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعهد دولت او در جوار افضالش </p></div>
<div class="m2"><p>برست دیدهٔ فتنه ز رنج بیداری </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدوست طایفهٔ شرع را قوی دستی </p></div>
<div class="m2"><p>وز وست قاعدهٔ شرک را نگونساری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو ابر بارد هنگام مکرمت کف او </p></div>
<div class="m2"><p>ولیک عادت او نیست جز گهرباری </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مظفرا ، تویی از خسروان ، که در گیتی </p></div>
<div class="m2"><p>بدانچه یافته ای ، از علو ، سزاواری </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نجوم چرخ نیارند زد همی ز حیا </p></div>
<div class="m2"><p>بپیش کثرت فضل تو لاف بسیاری </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ستاره را نرود با تو هیچ بوالعجبی </p></div>
<div class="m2"><p>زمانه را نرود با تو هیچ مکاری </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کسی که با تو طریق مخالفت سپرد </p></div>
<div class="m2"><p>بیک زمانه بدو چنگ فناش بسپاری </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بتیر تیز ، فلک را اگر بخواهی تو </p></div>
<div class="m2"><p>ز سقف دایرهٔ آسمان فرود آری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بپایگاه و برفعت تو سپهر تأثیری </p></div>
<div class="m2"><p>بدستگاه و بحشمت زمانه آثاری </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حریم صدر تو احرار از آن گزیدستند </p></div>
<div class="m2"><p>که از جفای زمانه پناه احراری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همیشه تا که جهان راست فعل بد عهدی </p></div>
<div class="m2"><p>همیشه تا که فلک راست پیش غداری </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>امانت باد ز دست فنا و قهر ، که تو </p></div>
<div class="m2"><p>فنای مبتدعانی و قهر کفاری </p></div></div>