---
title: >-
    شمارهٔ ۲ - در مدح ملک اتسز و حسب حال خود
---
# شمارهٔ ۲ - در مدح ملک اتسز و حسب حال خود

<div class="b" id="bn1"><div class="m1"><p>ای جاه تو فراخته اعلام کبریا </p></div>
<div class="m2"><p>صافیست اعتقاد تو از کبر و از ریا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عقد ملک در جلال تو واسطه</p></div>
<div class="m2"><p>در چشم فتح گرد براق تو توتیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست مبارک تو و طبع کریم تو </p></div>
<div class="m2"><p>موقوف بر سخاوت و مجبول بر سخا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نکتهای خوب تو مضمون شده هنر </p></div>
<div class="m2"><p>با وعدهای دست تو مقرون شده وفا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درماندهٔ حوادث و مجروح چرخ را</p></div>
<div class="m2"><p>از همت تو راحت و از سعی تو شفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سودیست مهر تو ، که نبیند کسش زیان </p></div>
<div class="m2"><p>دردیست کین تو ، که نیابد کسش دوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوج جلالت تو به رفعت چو آفتاب </p></div>
<div class="m2"><p>خاک ستانهٔ تو بصنعت چو کیمیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از طبع توست سینه ناهید را طرب</p></div>
<div class="m2"><p>وز رأی توست چشمه خورشید را ضیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بوستان عیش ، نهاد امید خلق </p></div>
<div class="m2"><p>از ابر مکرمات تو با نشو و با نما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خصم صد ولایت و از تو یکی پیام</p></div>
<div class="m2"><p>وز مال صد خزانه و از تو یکی عطا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حساد آنچه از تو و رمح تو دیده اند</p></div>
<div class="m2"><p>فرعون از کلیم ندیدست و از عصا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقتی که زد زمانه فتد نعرهٔ جدال </p></div>
<div class="m2"><p>جایی که پر ستاره شود شعلهٔ وغا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از فیض خون کشته ملمع شود زمین </p></div>
<div class="m2"><p>وز گرد سم باره مقنع شود هوا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ارواح سر کشان همه چون باد بی خطر </p></div>
<div class="m2"><p>و اجسام صفدران همه چون خاک بی بها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در دستها نهاده فلک نامهٔ اجل </p></div>
<div class="m2"><p>بر شخصها دریده جهان جامهٔ بقا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنجا بگرز خرد کنی تارک قدر</p></div>
<div class="m2"><p>وانگه بتیر کور کنی دیده قضا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گردد زبیم خنجر فیروز فام تو </p></div>
<div class="m2"><p>بیجاده رنگ چهره گردان چو کهربا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آسایش مخلف دولت کنی تعب </p></div>
<div class="m2"><p>پیشانی منازع ملت کنی قفا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قانع شوی زحمله و بیرون شوی زحرب</p></div>
<div class="m2"><p>پرداخته مهم و بر افراخته لوا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرهای سرکشان همه در صحن معرکه</p></div>
<div class="m2"><p>چون گندنا دروده بتیغ چو گندنا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شیری بوصف و نیزهٔ تو اژدها بشکل </p></div>
<div class="m2"><p>کس را بود مقاوت شیر و اژدها؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای گنج محمدت چو تو نادیده قهرمان</p></div>
<div class="m2"><p>وی تخت مملکت چو تو نادیده پادشا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاک زمین زحزم تو یابد همی سکون</p></div>
<div class="m2"><p>باد هوا ز عزم تو گیرد همی مضا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از شعله نهیب تو و لطف طبع تو</p></div>
<div class="m2"><p>در آتش و در آب لهیب آمد و صفا </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>احرار را هوای تو چو روزه و نماز </p></div>
<div class="m2"><p>زوار را جناب تو چون مروه و صفا </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آسوده نیک‌خواه تو در روضهٔ نعیم</p></div>
<div class="m2"><p>فرسوده بدسگال تو در قبضهٔ بلا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با زایر جناب تو گوید عطای تو :</p></div>
<div class="m2"><p>«وافیت دام عزک ، اهلا و مرحبا!» </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بحر محیط پیش بنان تو چون شمر</p></div>
<div class="m2"><p>بدر منیر پیش سنان تا چون سها</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا آب امر و نهی او روان شد بجوی تو </p></div>
<div class="m2"><p>سرگشته شد عدوی تو چون چرخ آسیا </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در چشم من زنور لقای تو روشنیست</p></div>
<div class="m2"><p>مصروف باد چشم بد از نور آن لقا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>والله: که نزد من بیکی منزلت بود</p></div>
<div class="m2"><p>نادیدن لقای تو و دیدن فنا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جرمی بزرگ کرده ام و جز دو حال نیست: </p></div>
<div class="m2"><p>یا عفو و یا عقوبت، یا خوف و یا رجا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لایق بود به حال من و روزگار تو</p></div>
<div class="m2"><p>گر همت تو عفو کند ذلت مرا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پس گر عقوبت کنی، اهل عقوبتم</p></div>
<div class="m2"><p>لابد گناه‌ را به عقوبت بود جزا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وز جمله حکم حاکم تو و امر امر تست</p></div>
<div class="m2"><p>گفتن خطاست با تو: این چون و آن چرا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای در نهاد تو همه سرمایهٔ کرم</p></div>
<div class="m2"><p>وی در سرشت تو همه پیرایهٔ سخا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در فوت من مکوش، مبادا زحب فضل </p></div>
<div class="m2"><p>وقتی تحسری بود از فوت من ترا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در خون من مشو، که بخون شسته‌ام دورخ</p></div>
<div class="m2"><p>بی تو، به حق خون شهیدان کربلا </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هستند در هوای تو بر سر پاک من</p></div>
<div class="m2"><p>روحانیان و خالق روحانیان گوا </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نظمم مدیح توست و چه باشد به از مدیح؟</p></div>
<div class="m2"><p>نثرم دعای توست و چه باشد به از دعا؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تاریخ دستبرد تو چون نظم من کدام؟ </p></div>
<div class="m2"><p>فهرست کارکرد تو چون نثر من کجا؟</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ماند نهان شعار مقامات ملک تو </p></div>
<div class="m2"><p>گر نظم من هدر شود و نثر من هبا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کارم همیشه محمدت بارگاه تست</p></div>
<div class="m2"><p>ای بارگاه تو بهمه محمدت سزا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گویم همه ثنای تو در غیبت و حضور</p></div>
<div class="m2"><p>جویم همه رضای تو در شدت و رخا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا شاخ گل ز وصل دی و هجر بلبلست</p></div>
<div class="m2"><p>از برگ و از نوا شده بی برگ و بی نوا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از روی ساقیان و ز آواز مطربان</p></div>
<div class="m2"><p>بزم تو با پر گل و جشن تو پر نوا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا گاه اندهست در آفاق و گه نشاط</p></div>
<div class="m2"><p>تا گاه راحتست در ایام و گه عنا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بادا ترا کرامت و ضد ترا هوان</p></div>
<div class="m2"><p>بادا ترا ترا سعادت و خصم ترا شقا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>با ناصحت گشاده جهان چهرهٔ لطف</p></div>
<div class="m2"><p>بر حاسدت کشیده فلک دهرهٔ هجا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شغلت همه متابعت شرع ایزدی</p></div>
<div class="m2"><p>کارت همه مشایعت دین مصطفا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نفس ترا کمال عقول ملائکه </p></div>
<div class="m2"><p>جان ترا سعادت ارواح انبیا</p></div></div>