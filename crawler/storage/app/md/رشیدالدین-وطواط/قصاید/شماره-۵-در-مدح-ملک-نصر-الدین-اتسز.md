---
title: >-
    شمارهٔ ۵ - در مدح ملک نصر الدین اتسز
---
# شمارهٔ ۵ - در مدح ملک نصر الدین اتسز

<div class="b" id="bn1"><div class="m1"><p>ای طلعت تو نیکو وی قامت تو زیبا </p></div>
<div class="m2"><p>زلفین تو چون عنبر، رخسار تو چون دیبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ماه سما دارای افروخته تر چهره </p></div>
<div class="m2"><p>وز سرو سهی دارای افراخته تر بالا </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاشانهٔ شخص تو زیبد که بود جنت </p></div>
<div class="m2"><p>مشاطهٔ روی تو زیبد که بود حورا </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این گشته تو چون خرما سنگین دل</p></div>
<div class="m2"><p>خارست غمت، آری، با خار بود خرما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق کند قبله تیمار مرا وامق </p></div>
<div class="m2"><p>در حسن برد سجده دیدار ترا عذرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آورده به جان دادن وصل تو دم معجز</p></div>
<div class="m2"><p>بنموده بجان بردن هجر تو ید بیضا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی دو رخ رنگینت رونق ندهد باده</p></div>
<div class="m2"><p>بی دو لب نوشینت لذت ندهد صهبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسنبل پر تابت صد لعب شده پنهان </p></div>
<div class="m2"><p>وز نرگس پر خوابت صد فتنه شده پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خلق، ز عشق تو، هر روز یکی فتنه</p></div>
<div class="m2"><p>در شهر، ز مهر تو هر لحظه یکی غوغا </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را نبود یارای در دفع بلای تو </p></div>
<div class="m2"><p>گر نصرة دین حق نصرة ندهد ما را </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاهی، که بود قدرش با مرتبت گردون</p></div>
<div class="m2"><p>شاهی که بود دستش با مکرمت دریا </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست کرمش داده مال کرهٔ اغبر</p></div>
<div class="m2"><p>پای شرفش سوده اوج فلک خضرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خورشید بود تابان همچون دل او لیکن</p></div>
<div class="m2"><p>آنگه که زند رأیت بر کنگره جوزا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای‌آنکه بعنف تو درفتد از گردون</p></div>
<div class="m2"><p>وی آنکه به لطف تو گل بردمد از خارا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از تیغ تو با زینت این سلطنت عالی </p></div>
<div class="m2"><p>وز کلک تو با رتبت این مملکت والا </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن روز که بگذارد سندان ز تف جمله </p></div>
<div class="m2"><p>و آن وقتکه بگریزد شیطان ز صف هیجا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیغ تو قلاع دین ، چون تیغ علی یکسر</p></div>
<div class="m2"><p>خالی کند از کافر، تاری کند از اعدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از شخص جهانگیران چون کوه شود هامون </p></div>
<div class="m2"><p>وز خون عدو بندان چون بحر شود صحرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آفاق شود تاریک، چون سینهٔ نامؤمن </p></div>
<div class="m2"><p>خورشید شود تیره چون دیدهٔ نابینا </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از نور سنان گردد مانند فلک پستی </p></div>
<div class="m2"><p>وزگرد سپه گردد مانند زمین بالا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بابأس تو آن ساعت چون موم بود آهن </p></div>
<div class="m2"><p>و ز بیم تو آن لحظه چون پیر بود برنا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از حرص زند نعره یکران تو چون تندر</p></div>
<div class="m2"><p>و ز طبع کند جولان شبدیز تو چون نکبا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رمح تو شود یاران در خصم تو چون تنین</p></div>
<div class="m2"><p>خصم تو شود پنهان از بیم چون عنقا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از تن بکنی همچون مزمار سر خصمان</p></div>
<div class="m2"><p>و ز هم بدری همچون پرگار تن اعدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شاهی نبود هرگز در جنگ ترا ثانی</p></div>
<div class="m2"><p>گردی نبود هرگز در حرب ترا همتا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از حکم تو برگشتن کس را نبود زهره</p></div>
<div class="m2"><p>و ز خط تو سر بردن کس را نبود یارا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شمسی تو بهر معنی و اولاد فزون ز انجم</p></div>
<div class="m2"><p>کلی تو بهر دانش و ابنای جهان اجزا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در دهر به تو نازد هم دولت و هم نعمت </p></div>
<div class="m2"><p>در حشر بنازد هم آدم و هم حوا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای خدمت تو گشته پیرایهٔ هر عاقل</p></div>
<div class="m2"><p>وی مدحت تو گشته سرمایهٔ هر دانا </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از صدر منیع تو هرگز نشوم یک سو </p></div>
<div class="m2"><p>وز نظم ثنای تو هرگز نشوم تنها </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در نظم ید بیضا کس نبود جز من </p></div>
<div class="m2"><p>و آن کس که کند دعوی بیهوده پزد سودا </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دارم ز عجم منشأ، لیکن بفصاحت در</p></div>
<div class="m2"><p>بسیار فزون آیند از طایفهٔ بطحا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آنم که به من گردد ارکان سخن محکم </p></div>
<div class="m2"><p>و آنم که به من یابد فرمان هنر امضا </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گشتست زبانم ده،چون سوسن آزاده</p></div>
<div class="m2"><p>در مالش هر دشمن دو رو چو گل رعنا </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با قوت عون تو کس را نشوم عاجز </p></div>
<div class="m2"><p>با حشمت جاه تو با کس نکنم ابقا </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>امروز بسعی تو حالیست مرا نیکو</p></div>
<div class="m2"><p>و امروز به فر تو کاریست مرا زیبا </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>احباب مرار آرد آسایش من شادی </p></div>
<div class="m2"><p>و اعدای مرا سازد آرامش من شیدا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از غصه همی‌ گویند او باش ملک با هم؛ </p></div>
<div class="m2"><p>آخر ز کجا آمد این فکر فلک پیما؟ </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای آنکه ز عزمت امروز همی ‌پیچی</p></div>
<div class="m2"><p>زین گونه بسی پیچی، گر صبر کنی فردا </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا کس ز فلک هرگز غمگین نشود خیره </p></div>
<div class="m2"><p>تا کس ز جهان هرگز رسوا نشود عمدا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بادا دل بدخواهت خیره زفلک غمگین ! </p></div>
<div class="m2"><p>بادا تن بدگویت عمداً بجهان رسوا!</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اوقات صیام آمد و آورد بصدر تو</p></div>
<div class="m2"><p>از خلد برین تحفه، صد نعمت و صد آلا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرخنده کنار ایزد، بر ذات کریم تو</p></div>
<div class="m2"><p>این ماه مکرم را وین نعمت آلارا</p></div></div>