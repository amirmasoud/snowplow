---
title: >-
    شمارهٔ ۱۳۷ - از زبان علاءالدوله اتسز خوارزمشاه گوید
---
# شمارهٔ ۱۳۷ - از زبان علاءالدوله اتسز خوارزمشاه گوید

<div class="b" id="bn1"><div class="m1"><p>منم ، که نیست مرا در جهان نظیر و همال</p></div>
<div class="m2"><p>ببزم دشمن مالم ، برزم دشمن مال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم ، که جز بمدیحم زبان نجنباند </p></div>
<div class="m2"><p>هر آن که بر سر یک بیت بر نویسد قال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیل موکب میمون من شده تأیید </p></div>
<div class="m2"><p>عدیل رایت منصور من شده اقبال </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خجسته حضرت من گشته منبع لذات </p></div>
<div class="m2"><p>گزیده مجلس من گشته مقصد آمال </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر مآثر من بی محل علو اثیر </p></div>
<div class="m2"><p>بر شمایل من بی خطر نسیم شمال </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کفم بجود شده واهب قلیل و کثیر </p></div>
<div class="m2"><p>دلم بعلم شده حاکم حرام و حلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بحر باشد مانند دست من بسخا </p></div>
<div class="m2"><p>نه چرخ باشد مانند قدر من بجلال </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بطبع من متجمع لطایف آداب </p></div>
<div class="m2"><p>ز کف من متفرق خزاین اموال </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من آن کسم که نیارد قرین من یک شخص </p></div>
<div class="m2"><p>قران انجم و گردون بصدهزاران سال </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمینهٔ بندهٔ من هست در صف هیجا</p></div>
<div class="m2"><p>هزار بیژن گیو و هزار رستم زال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز من مخالف ملک مرا عنا و فنا </p></div>
<div class="m2"><p>ز من موافق جاه مرا جمال و کمال </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عراق و جند و سمرقند از شجاعت من </p></div>
<div class="m2"><p>جواب گویند، ار عاقلان کنند سوال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین سه بقعه که اعلام من فراشته شد </p></div>
<div class="m2"><p>شدند پیر ز بیم حسام من اطفال </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مخالفان مرا پشت در مواقف حرب </p></div>
<div class="m2"><p>ز تیغ چون الف من خمیده همچون دال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه هست جان شریف مرا ز علم فراق </p></div>
<div class="m2"><p>نه هست طبع کریم مرا ز جود ملال </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لطیفهای من اندر فنون دانش و علم </p></div>
<div class="m2"><p>همه چو سحر حلال و همه چو آب زلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز تیغ من ، که درو روشنایی ظفرست </p></div>
<div class="m2"><p>شدست تیره عدوی مرا همه احوال </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ثنای درگه من گشته سروران را حرز </p></div>
<div class="m2"><p>لقای مجلس من گشته خسروان را فال </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همیشه تا بابد آفتاب جاه مرا</p></div>
<div class="m2"><p>بر آسمان معالی مباد خوف زوال</p></div></div>