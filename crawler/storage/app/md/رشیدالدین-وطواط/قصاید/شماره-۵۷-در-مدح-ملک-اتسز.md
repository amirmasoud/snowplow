---
title: >-
    شمارهٔ ۵۷ - در مدح ملک اتسز
---
# شمارهٔ ۵۷ - در مدح ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>ای تو بر آزادگان عصر خداوند </p></div>
<div class="m2"><p>گیتی هرگز نزاد مثل تو فرزند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک سخن مایه هزار سخن سنج</p></div>
<div class="m2"><p>یک هنرت زیور هزار هنرمند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست فلک را بوفق رأی تو پیمان </p></div>
<div class="m2"><p>هست جهان را به خاک پای تو سوگند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست مواقف ز اصطناع تو پر در</p></div>
<div class="m2"><p>پای مخالف ز انتقام تو در بند </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه نهال سخاست کف تو بنشاند</p></div>
<div class="m2"><p>هر چه درخت جفاست لطف تو برکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام نکو به ز مال و همت عالیت</p></div>
<div class="m2"><p>نام نکو جمع کرد و مال پراکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست تو حساد را ز پای درآورد </p></div>
<div class="m2"><p>پای تو افلاک را ز دست در افکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داده همه طالبان ملک جهان را </p></div>
<div class="m2"><p>در صف هیجا زبان نیزهٔ تو پند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خنجر تو از برای حرمت قرآن </p></div>
<div class="m2"><p>فایده زند برد و حرمت پا زدند </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیغ تو چندان بکشت مبتدعان را</p></div>
<div class="m2"><p>تا شکم خاک را ز کشته بیاگند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رستم سکزی نکرده‌ است بعمری</p></div>
<div class="m2"><p>آنچه تو یک لحظه کرده ای بسمرقند </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر در خیبر ندیده اند ز حیدر</p></div>
<div class="m2"><p>آنچه ز باس تو دیده شد بدر چند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمرو نکردهست آنچه شخص تو کرده است</p></div>
<div class="m2"><p>با سپه طاغیان بدشن نهاوند </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نعرهٔ تو چون بگوش خصم در آید</p></div>
<div class="m2"><p>از تن خصمت فرو گشاید پیوند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای شه عادل ، چو بنده مدح سرایی</p></div>
<div class="m2"><p>خوار چرا شد، بعهد چون تو خداوند ؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جور کشیدم ز روزگار فراوان </p></div>
<div class="m2"><p> دهر تنم را اسیر حادثه مپسند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرخ اگر چند زشت کرد بجابم</p></div>
<div class="m2"><p>آخر، ای حضور در، جور تو تا چند؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا که نباشد شراب را هنر آب</p></div>
<div class="m2"><p>چون برضای تو بوده، هستم خرسند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باد در اندوه و رنج خصم تو گریان</p></div>
<div class="m2"><p>تو بطرب در نشاط و ناز همی خند</p></div></div>