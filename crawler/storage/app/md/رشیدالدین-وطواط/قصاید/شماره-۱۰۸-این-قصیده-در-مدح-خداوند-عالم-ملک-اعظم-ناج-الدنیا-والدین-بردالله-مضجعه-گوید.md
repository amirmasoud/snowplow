---
title: >-
    شمارهٔ ۱۰۸ - این قصیده در مدح خداوند عالم ملک اعظم ناج الدنیا والدین بردالله مضجعه گوید
---
# شمارهٔ ۱۰۸ - این قصیده در مدح خداوند عالم ملک اعظم ناج الدنیا والدین بردالله مضجعه گوید

<div class="b" id="bn1"><div class="m1"><p>ای دولت جوان ترا بنده چرخ پیر</p></div>
<div class="m2"><p>در قبضهٔ ارادت تو آسمان اسیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون گرفته بر خط پیمان تو مدار</p></div>
<div class="m2"><p>و اختر گزیده بر ره فرمان تو مسیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن عالمی بجاه ، که از روی منقبت </p></div>
<div class="m2"><p>در جنب تو صغیر بود عالم کبیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشت ولی ز کلک ضعیف تو شد قوی</p></div>
<div class="m2"><p>عمر عدو ز رمح طویلت شده قصیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دستم ستور تو هنگام کر و فر</p></div>
<div class="m2"><p>اندر دماغ فتح و ظفر خوشتر از عبیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رنج تف خنجر چون آفتاب تو</p></div>
<div class="m2"><p>در سایهٔ سپهر وطن ساخته اثیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جود تو داعیان رجا را شده مجیب </p></div>
<div class="m2"><p>جاه تو خایفان بلا را شده مجیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرزند خصم تو ، که ز مادر جدا شود</p></div>
<div class="m2"><p>گردونش جام مرگ چشاند بجای شیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبع ولیت مایهٔ شادیست همچو زر</p></div>
<div class="m2"><p>شخص عدوت صورت زاریست همچو زیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بر سریر شرع برد نام تو خطیب </p></div>
<div class="m2"><p>از فخر بر ستاره رسد پایهٔ سریر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>احباب را وفاق تو سازنده چون نعیم </p></div>
<div class="m2"><p>حساد را خلاف تو سوزنده چون سعیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیتی ز سهم گرز تو در نوحه و خروش </p></div>
<div class="m2"><p>گردون ز بیم تیغ تو در ناله و نفیر </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیرون کشیده خصم ترا از میان ماز</p></div>
<div class="m2"><p>انگشت اقتدار تو چون موی از خمیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از هیبت حسام چو نیلوفرت عدو</p></div>
<div class="m2"><p>با رنگ همچو لاله و با روی چون زریر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای در سخا بنان تو بحری شده محیط</p></div>
<div class="m2"><p>وی در ضیا ضمیر تو بدری شده منیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گردون بخدمت تو و گیتی بمدح تو </p></div>
<div class="m2"><p>بسته میان چو رمح و گشاده دهان چو تیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو یک تنی که خیر دو عالم زذات تست</p></div>
<div class="m2"><p>وان دیگران ، کثیر ، که لاخیر فی کثیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیش یمین تو ، که یمانی قرین اوست </p></div>
<div class="m2"><p>وقت سخا بسا که جزا میبرد یسیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در عهد تو کمینه ثنا خوان صدر تو</p></div>
<div class="m2"><p>ممدوح صد فرزدق و مخدوم صد جریر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاها ، خدایگانا ، دانی که من رهی</p></div>
<div class="m2"><p>در نظم بی همالم و در نثر بی نظیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باغی شکفته دارم از سحر در بنان</p></div>
<div class="m2"><p>گنجی نهفته دارم از فضل در ضمیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من وصف علم خویش چه گویم ترا؟ که تو</p></div>
<div class="m2"><p>هم عالمی خبیری و هم ناقدی بصیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سی سال و پنج سال بمانند بلبلان </p></div>
<div class="m2"><p>در باغ مدح خسرو ماضی زدم صفیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اندر فنون فضل منش بودمی امام </p></div>
<div class="m2"><p>وندر امور ملک منش بودمی مشیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>او رفت و تا بقا بودم در ثنای تو </p></div>
<div class="m2"><p>گوهر فشاند خواهم زین خاطر خطیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مدح ترا نشاید الا چو من فصیح </p></div>
<div class="m2"><p>ملک ترا نشاید الا چو من دبیر</p></div></div>