---
title: >-
    شمارهٔ ۱۵۶ - د رمدح یکی از وزراء
---
# شمارهٔ ۱۵۶ - د رمدح یکی از وزراء

<div class="b" id="bn1"><div class="m1"><p>ای خرم از مکارم اخلاق تو جهان</p></div>
<div class="m2"><p>منقاد امر و نهی تو اجرام آسمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زیر پای همت تو تاریک سپهر</p></div>
<div class="m2"><p>در زیر دست حشمت تو عرصهٔ جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خیر محمدت دل تو گشته پادشاه </p></div>
<div class="m2"><p>بر گنج مکرمت کف تو گشته قهرمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دولتست خاطر جاه ترا نگین</p></div>
<div class="m2"><p>وز نصرتست مرکب عزم ترا عنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوید هم ملک ز علوم تو فایده</p></div>
<div class="m2"><p>گوید همی فلک ز رسوم تو داستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا که حشمت تو ، ز دولت بود اثر</p></div>
<div class="m2"><p>و آنجا که همت تو ، ز رحمت بود نشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایام را سعادت تو بوده بدرقه </p></div>
<div class="m2"><p>و اسلام را سیاست تو گشته پاسبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رأی تو بر دقایق آفاق مطلع</p></div>
<div class="m2"><p>کلک تو از سرایر افلاک ترجمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر تو دیدهٔ کرم و جود را بصر</p></div>
<div class="m2"><p>طبع تو پیکر هنر و فضل را روان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم طمع چو جود تو نادیده مایده</p></div>
<div class="m2"><p>گوش امل چو لطف تو نشنیده میزبان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از سعی تو منافع دولت شده پدید </p></div>
<div class="m2"><p>وز کلک تو مصالح ملت شده عیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خیل مراد کرده دلت را متابعت</p></div>
<div class="m2"><p>حفظ خدای گشته تنت را نگاهبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر چان بگفته اند در اخبار انبیا</p></div>
<div class="m2"><p>گشتست خلق را ز کرامات تو عیان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای گشته با زمانه مساعی تو قرین</p></div>
<div class="m2"><p>وی کرده با ستاره معالی تو قران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آل و دودمان نبی و وصی تویی</p></div>
<div class="m2"><p>وندر جهان تراست چنین آل و دودمان ؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منت خدای را که درین خطه کس ندید</p></div>
<div class="m2"><p>ما را ، مگر بمجلس عالیت مدح خوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جز مهر تو نگشت مرا هیچ در دماغ</p></div>
<div class="m2"><p>جز مدح تو نرفت مرا هیچ بر زبان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صد را، بفر تو ، که نهشتم بعمر خود</p></div>
<div class="m2"><p>عرض کریم را بهوی در کف هوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز آنها نیم که بر در هرکس کنم قرار</p></div>
<div class="m2"><p>همچون سگان ز بهر یکی پاره استخوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بهر خرقه ای نکشم طعنه های این</p></div>
<div class="m2"><p>وز بهر لقمه ای نخورم عقبه های آن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر مال نیست ، هست مرا فضل بی شمار</p></div>
<div class="m2"><p>ور سیم نیست ، هست مرا علم بیکران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یک فضل به مرا که بسی در شاهوار</p></div>
<div class="m2"><p>یک علم به مرا که بسی گنج شایگان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نگذاردم هنر که من از روزگار خویش</p></div>
<div class="m2"><p>رازی شوم به جامه و قانع شوم بنان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آخر همان زمانه بکوبد در عنا</p></div>
<div class="m2"><p>بر کام دل مرا کند اقبال کامران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آرام بفضل موکب حشمت بزیر چنگ</p></div>
<div class="m2"><p>و آرام بعلم مرکب دولت بزیر ران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من کرده خویشتن سره از فضل و آن گهی </p></div>
<div class="m2"><p>در کنج خانه مانده چو بر خایه ماکیان </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لؤلؤ چه قدر داردد اندر صمیم بحر ؟</p></div>
<div class="m2"><p>گوهر چه قیمت آرد اندر میان کان ؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کاری کنم که ماندم از مکرمات اثر </p></div>
<div class="m2"><p>جایی روم که باشدم از حادثات امان </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خواهم شدن چو تیر ازین جا سوی عراق </p></div>
<div class="m2"><p>با قامتی ز بار عطای تو چون کمان </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگشاده چون دوات باوصاف تو دهن </p></div>
<div class="m2"><p>بر بسته چون قلم بثناهای تو میان </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مسکین ضعیفه والدهٔ گنده پیر من</p></div>
<div class="m2"><p>برخود همی بپیچد ازین غم چو خیزران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>داود سری گران ز دل و خاطری سبک </p></div>
<div class="m2"><p>دارد دلی سبک ز غم وانده گران </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جانش رسیده در کف تیمار من بلب</p></div>
<div class="m2"><p>کارش رسیده از غم دیدار من بجان </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون تار ریسمان تن او شد نزار و من</p></div>
<div class="m2"><p>بسته کجا شوم بیکی تار ریسمان؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پوشیده رفت خواهم ازو ، کز گریستن </p></div>
<div class="m2"><p>بر بندد اشک دیدهٔ او راه کاروان </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یا رب چگونه صبر کند در فراق من ؟</p></div>
<div class="m2"><p>آن طبع ناشکیبش و آن شخص ناتوان </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هستش دلی شکافته چون نار و از عنا</p></div>
<div class="m2"><p>رویی چو مغز نار و سرکشی چو ناردان </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از زخمهای پنجه و از بادهای سرد</p></div>
<div class="m2"><p>بر چون بنفشه دارد و چهره چو زعفران </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شبهای تیره زار بسی گفت خواهد او :</p></div>
<div class="m2"><p>یا رب ، تو آن غریب مرا باز من رسان </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حالی شگفت دیده ام امروز من ازو</p></div>
<div class="m2"><p>والله! که نیست هیچ خلاف اندرین میان </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شد ناگهان ز عزم من آگاه وز جزع </p></div>
<div class="m2"><p>خوناب شد دو گوهر تابانش ناگهان </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فرزند دیده ای تو ازین گونه بی وفا ؟</p></div>
<div class="m2"><p>مادر شنیده ای تو بدین شکل مهربان؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر حق این ضعیفهٔ بیچاره نیستی </p></div>
<div class="m2"><p>در دل مرا کجا بودی یاد خانمان؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در مجلس ملوک مرا باشدی مقر </p></div>
<div class="m2"><p>در محفل صدور مرا باشدی مکان </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>غبنا و حسرتا ! که رساند بمن همی </p></div>
<div class="m2"><p>یک سود را زمانه بخروارها زیان؟</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای گشته شرع را بهمه تقویت ضمین</p></div>
<div class="m2"><p>وی کرده خلق را بهمه مکرمت ضمان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تیمار این ضعیفه ، چو رفتم ، نکوبدار</p></div>
<div class="m2"><p>مقدار آن عفیفه ، که گفتم ، نکو بدان </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا شرح داده ای تو گویم بهر زمین </p></div>
<div class="m2"><p>تا مدح کرده های تو خوانم بهر زبان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جز من که گفت داند مدح ترا سزا ؟</p></div>
<div class="m2"><p>جز من که کرد داند وصف ترا میان ؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آنم که در دقایق تازی و پارسی </p></div>
<div class="m2"><p>دوران چراغ پیر نیارد چو من جوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آن پیشوای معرکهٔ دانشم ، که من </p></div>
<div class="m2"><p>هرگز سپر نیفگنم از تیر امتحان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از صوت من خجل شود الحان عندلیب</p></div>
<div class="m2"><p>وز طبع من حسد برد اطراف بوستان </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حسان کجاست ؟ تا که در آموزش بنظم </p></div>
<div class="m2"><p>در دو زبان مدایح اوصاف خاندان </p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا باغ هست چون رخ دلبر بنوبهار</p></div>
<div class="m2"><p>تا باد هست چون دم عاشق بمهرگان </p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اندر کمال باد وجود تو پایدار </p></div>
<div class="m2"><p>وندر جلال باد بقای تو جاودان </p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بادا مخالف تو در ادبار مستمند </p></div>
<div class="m2"><p>بادا موافق تو در اقبال شادمان </p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ای زر فشانده بر سر مردی بکمرمت </p></div>
<div class="m2"><p>کردم بشکر بر سرت امروز زر فشان </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اندر جهان بماند جاوید این ثنا </p></div>
<div class="m2"><p>تا این ثنا بماند اندر جهان بمان</p></div></div>