---
title: >-
    شمارهٔ ۴۵ - در ستایش شمس‌الدین وزیر
---
# شمارهٔ ۴۵ - در ستایش شمس‌الدین وزیر

<div class="b" id="bn1"><div class="m1"><p>جمال‌الملک شمس‌الدین جهانیست</p></div>
<div class="m2"><p>که از هر نعمتی او را نشانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهانی گفتم او راوین غلط بود</p></div>
<div class="m2"><p>که هر مویی ز شخص او جهانیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کفش و زجود بحری وین چه بحریست</p></div>
<div class="m2"><p>دلش در فضل کانی و آنچه کانیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ضیای حشمت او آفتابیست</p></div>
<div class="m2"><p>علو همت او آسمانیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بیدار او گنج هنر را</p></div>
<div class="m2"><p>چه دانی تا چه مایه قهرمانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دولت است جاهش را دلیلیست</p></div>
<div class="m2"><p>ز نصرة تیغ عزمش را فسانیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز از بهر مهم عون نیست</p></div>
<div class="m2"><p>اگر بر چرخ تیر یا کمانیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز از بهر هلاک خسم او نیست</p></div>
<div class="m2"><p>اگر در دهر تیغی یا سنانیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایا صدری، که هر نکته ز صدرت</p></div>
<div class="m2"><p>بر ارباب دانش داستانیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بپیش خاطر تو هست پیدا</p></div>
<div class="m2"><p>هر آنچ اندر همه عالم نهانیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسال تو ملک را مقتدایست</p></div>
<div class="m2"><p>جلال تو فلک را دیدبانیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه چون گفت کرم ‌را کار گاهست</p></div>
<div class="m2"><p>نه چون کلکت خرد را ترجمانیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ثریا پای قدرت را رکابیست</p></div>
<div class="m2"><p>مجره دست جاهت را عنانیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز حکمت نیست خالی در بد و نیک</p></div>
<div class="m2"><p>اگر وقتی دو کوکب را قرانیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بعالم نیست، الا حضرت تو</p></div>
<div class="m2"><p>اگر جایی افاضل را مکانیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بصدرت رخ نهاده هر زمانی</p></div>
<div class="m2"><p>ز اصحاب حوایج کار وانیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خداوندا، تویی کامل هنر را</p></div>
<div class="m2"><p>ز سعی جاه تو نامی و نانیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در تست آنکه ابنای خرد را</p></div>
<div class="m2"><p>درو از نکبت گیتی امانیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بهر نقش و نظم مدحت تست</p></div>
<div class="m2"><p>اگر ما را بنایی یا بیانیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بزرگا، رأی پیر تو رهی را</p></div>
<div class="m2"><p>نکو داند که : چون دانا جوانیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا وقت بیان اندر فصاحت</p></div>
<div class="m2"><p>تو گویی زیر هر مویی زبانیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بصورت آمدم جویای عزت</p></div>
<div class="m2"><p>که شخصی از حوادث در هوانیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بحرص سود ، حاشا، هر ‌زمانی</p></div>
<div class="m2"><p>مرا از چرخ گوناگون زیانیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یقینم شد، چو دیدم حضرت تو</p></div>
<div class="m2"><p>که آخر رنج عالم را کرانیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا حالیست پژمرده و لیکن</p></div>
<div class="m2"><p>دل از دانش چو تازه بوستانیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنم جانم را فدای خدمت تو</p></div>
<div class="m2"><p>خود اندر دست من امروز جانیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نخواهی اینک اهل فضل گویند :</p></div>
<div class="m2"><p>فلان در سایهٔ جاه فلانیست!</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دو عاقل را بهم تا اتفاقیست</p></div>
<div class="m2"><p>دو کوکب را بهم تا افترانیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مبادا جز بکام تو، هر آنجا</p></div>
<div class="m2"><p>که در اطراف عالم کامرانیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهان جاتو تو آباد بادا</p></div>
<div class="m2"><p>که صحن جاه تو خوش‌تر جهانیست</p></div></div>