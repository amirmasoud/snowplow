---
title: >-
    شمارهٔ ۹ - هم او راست و اعجاز ابیات دو بیت دوبیت مجانست
---
# شمارهٔ ۹ - هم او راست و اعجاز ابیات دو بیت دوبیت مجانست

<div class="b" id="bn1"><div class="m1"><p>زهی! دشمنت از طرب بینوا </p></div>
<div class="m2"><p>زده چرخ در مدحت تو نوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هدی یافته از جلالت جمال </p></div>
<div class="m2"><p>جهان ساخته از نوالت نوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معانی بلفظ تو یابد شرف </p></div>
<div class="m2"><p>معانی ز جاه تو گیر بها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوقت سخا گوهر آبدار </p></div>
<div class="m2"><p>چو خاکست نزدیک تو بی بها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولی را وفاقت صوایی صواب </p></div>
<div class="m2"><p>عدو را خلافت خطایی خطا </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه چون رای تو اختران فلک </p></div>
<div class="m2"><p>نه چون خط تو لعبتان ختا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده امر تو در ممالک روان</p></div>
<div class="m2"><p>شده حکم تو بر خلایق روا </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تو گشته پشت محامد قوی </p></div>
<div class="m2"><p>ز تو برده روی مکارم روا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه در گفت های تو سهو و غلط </p></div>
<div class="m2"><p>نه در کرد های تو چون و چرا </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعهد تو از غایت عدل تو </p></div>
<div class="m2"><p>کند میش در ظل گرگان چرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا حلم خاک و علو اثیر </p></div>
<div class="m2"><p>ترا لطف آب و صفای هوا </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمدح تو فرزانگان را شرف </p></div>
<div class="m2"><p>بصدر تو آزادگان را هوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهادست در ذات تو کردگار </p></div>
<div class="m2"><p>وفور مروت ، کمال صفا </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازین رویی بر زایران صدر تو </p></div>
<div class="m2"><p>گزینست چون مروه و چون صفا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه همچون جوار کریمت جوار </p></div>
<div class="m2"><p>نه همچون فنای رفیعت فنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نصیب نکو خواه از تو حیات </p></div>
<div class="m2"><p>نصیب بد اندیش از تو فنا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو عین صوابی «علی من اطاع»</p></div>
<div class="m2"><p>تو محض عقابی «علی من عصا »</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پدید آمده از تو وز کلک تو </p></div>
<div class="m2"><p>همان کز کلیم آمده وز عصا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه صورت تو کمال و جمال </p></div>
<div class="m2"><p>همه سیرت تو وفا و حیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز دست تو زایند فیض کرم </p></div>
<div class="m2"><p>چو از ابر بارنده صوب حیا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز گفتار تو گوش ها را نشاط</p></div>
<div class="m2"><p>ز دیدار تو دیده هارا جلا </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز خوف تو اعدای صدر تو را </p></div>
<div class="m2"><p>ز اوطان خویش اوفتاده جلا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز لطف تو خیره نعیم بهشت </p></div>
<div class="m2"><p>ز خلق تو طیره نسیم صبا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عزیزست بر جان من خدمتت </p></div>
<div class="m2"><p>چو روز شباب و چو عهد صبا </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بنوشیدم از تو شراب طرب </p></div>
<div class="m2"><p>بپوشیدم از تو لباس غنا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ثنای تو در گوش من خوشترست</p></div>
<div class="m2"><p>ز آواز چنگ و ز لحن غنا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا نیست از لفظ تو جز لطف </p></div>
<div class="m2"><p>مرا نیست از دست تو جز ندا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه از ره مردمی و کرم </p></div>
<div class="m2"><p>بگوش من آید ز لطفت ندا </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>الا، تا بود میغ ها را سرشک </p></div>
<div class="m2"><p>الا، تا بود تیغ ها را مضا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تورا باد اقبال در مابقی !</p></div>
<div class="m2"><p>فزون ز آنچه بودست در ما مضا </p></div></div>