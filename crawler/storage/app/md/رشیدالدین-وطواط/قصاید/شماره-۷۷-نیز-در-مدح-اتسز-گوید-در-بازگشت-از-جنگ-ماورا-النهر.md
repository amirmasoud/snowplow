---
title: >-
    شمارهٔ ۷۷ - نیز در مدح اتسز گوید در بازگشت از جنگ ماوراء النهر
---
# شمارهٔ ۷۷ - نیز در مدح اتسز گوید در بازگشت از جنگ ماوراء النهر

<div class="b" id="bn1"><div class="m1"><p>ای ز آب تیغ تو تازه ریاحین ظفر </p></div>
<div class="m2"><p>وی بنور رأی تو روشن قوانین هنر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا اوصاف تو ، آنجا بود مجد و شرف</p></div>
<div class="m2"><p>هر کجا اصناف تو ، آنجا بود فتح و ظفر </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی را مرکب عزت سپرده زیر پای </p></div>
<div class="m2"><p>امتی را طایر عدلت گرفته زیر پر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با جلال تو سپهر پر صنایع نیم کار</p></div>
<div class="m2"><p>با کمال تو جهان پر بدایع مختصر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو نشانست از عطای دست تو : بحر و سحاب</p></div>
<div class="m2"><p>دو نمونه از ضیای رأی تو :شمس و قمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر در پیمان تو چرخ فلک افگنده رخت</p></div>
<div class="m2"><p>بر خط فرمان تو خلق جهان آورده سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک را تدبیر تو چونان که خنجر را فسان </p></div>
<div class="m2"><p>علم را تقریر تو چونان که بستان را مطر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیکر افضال را فیض عطای تو روان </p></div>
<div class="m2"><p>دیدهٔ اقبال را نور لقای تو بصر </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدحت اخلاق محمود تو تسبیح انام </p></div>
<div class="m2"><p>ساحت درگاه میمون تو محراب بشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست در اصناف دانش گفتهای تو مثل </p></div>
<div class="m2"><p>هست در انواع مردی کردهای تو سمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک حدیقه است از ریاض عالم عفوت بهشت</p></div>
<div class="m2"><p>یک نتیجه است از حریم آتش خشمت سقر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرخ اعظم با الف قدر تو همچو زمین </p></div>
<div class="m2"><p>بحر قلزم با سخای دست تو همچو شمر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرد و زن را کرد قارون جودت از زر و درم </p></div>
<div class="m2"><p>بحر و کان را کرد مفلس دستت از در و گهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نور در دست بداندیشان گشته ظلام</p></div>
<div class="m2"><p>زهر در کام نکوخواهان تو گشته شکر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از هنر صد درج و از تو یک بیان ماجری</p></div>
<div class="m2"><p>وز گهر صد گنج و از تو یک نوال ماحضر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دستیار دولت تو هم شهور و هم سنین </p></div>
<div class="m2"><p>پایکار هیبت تو هم قضا و هم قدر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ناصح صدر ترا و حاسد قدر ترا</p></div>
<div class="m2"><p>بهره از افلاک خیر و حصه از ایام شر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در شرف بیشی زعالم ، گر چه هستی اندرون </p></div>
<div class="m2"><p>نی لئالی در صدف باشد جواهر در حجر؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو جهان داری و دارای جهان ، تا رستخیز</p></div>
<div class="m2"><p>کس نخواهد بود چون تو جهانداری دگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از وجود تو بیان شد هرچه بود آفاق را </p></div>
<div class="m2"><p>در عرب وندر عجم از حیدر و رستم خبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست یک خطه ، که آنجا نیست عدلت منتشر</p></div>
<div class="m2"><p>نیست یک بقعه ، که آنجا نیست حکمت معتبر </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گاه از سقسین بری رایت بحد بر سخان </p></div>
<div class="m2"><p>گاه از خاور کشی لشکر بسوی باختر </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گاه آری جند و منقشلاق اندر زیر خنگ</p></div>
<div class="m2"><p>گه سمرقند و بخارا ، گه تراز و کاشغر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آسمانی ، ز انت آرامش نباشد از مسیر </p></div>
<div class="m2"><p>آفتابی ، ز انت آسایش نباشد از سفر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا درخت فتح آب از چشمهٔ تیغ تو یافت </p></div>
<div class="m2"><p>هر زمان از وی همی ملکی دگر آید ببر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خسروا ، بردی سپه سوی سمرقند ترا </p></div>
<div class="m2"><p>بود نصرة هم عنان و بود دولت راهبر </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از جلالت سوی اقدامت عدد اندر عدر</p></div>
<div class="m2"><p>و ز سعادت سوی اعلامت حشر اندر حشر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لشکری از سرکشان در خدمتت بی ترس و باک </p></div>
<div class="m2"><p>زمره ای از صفدران در موکبت جان سپر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر کجا بفراخت جاه تو بفیروزی لوا </p></div>
<div class="m2"><p>هر کجا بنمود بخت تو ز بهروزی اثر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خسروان آن طراف کردند صدرت را سجود </p></div>
<div class="m2"><p>سروران آن زمین بستند امرت را کمر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خطبهٔ آن خطبه گشت از نعمت تو با جاه و قدر </p></div>
<div class="m2"><p>سکهٔ آن بقعه گشت از نام تو با زیب و فر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای بسا ، ناکام ، کو از دولتت شد کامگار !</p></div>
<div class="m2"><p>وی بسا ، بی نام ، کو از حشمتت شد نامور! </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خازن اقبال گویی از قدیم الدهر باز </p></div>
<div class="m2"><p>داشت آماده ز بهر خدمت تو سربسر </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر خزاین کو نهده بود اندر شرق و غرب </p></div>
<div class="m2"><p>هر دفاین کو نهفته بود اندر بحر و بر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بنده و آزاد اعدا را بمالیدی بتیغ </p></div>
<div class="m2"><p>چون فتد در مرغزار آتش بسوزد خشک و تر </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رایت تو در سمرقند و ز باد تیغ تو </p></div>
<div class="m2"><p>روم گشته چون بلاد عادیان زیر و زبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون ز حال ماوراء النهر فارغ آمدی </p></div>
<div class="m2"><p>با هزاران دولت و نعمت بسوی مستقر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یسر با خیل تو کرده خواب در یک خوابگه </p></div>
<div class="m2"><p>یمن با جیش تو شد خورده آب در یک آبخور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خطهٔ خوارزم اکنون با جلال مقدمت </p></div>
<div class="m2"><p>از ارم مانوس شد و ز حرم محروس تر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>منت ایزد را که حاصل کرد تأیید فلک </p></div>
<div class="m2"><p>هر چه از دولت ترا موعود بود و منتظر </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سایهٔ عدلت گرفت از ترک تا حد حجاز</p></div>
<div class="m2"><p>بسطت ملک رسد از هند تا حد خزر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رایت جاهت در اکناف هدی شد مرتفع </p></div>
<div class="m2"><p>آیت عزت در اکناف جهان شد منتشر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرجع اهل هدی گشتند در دوران تو </p></div>
<div class="m2"><p>زان سپس کاهل هدی بودند مانده در بدر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون بتو ایزد زمام جملهٔ عالم سپرد </p></div>
<div class="m2"><p>تو بعالم در ، طریق بخشش و نیکی سپر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر خلایق داد کن ، زیرا که در آفاق نیست</p></div>
<div class="m2"><p>نزد ایزد کس گرامی تر زشاه دادگر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هست نیکی آن شجر، کز شاخ او ناید همی</p></div>
<div class="m2"><p>جز ثواب و جز ثنا در آجل و عاجل ثمر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سیم و زر در وجه نام نیک نه ، کز روی عقل</p></div>
<div class="m2"><p>هست گنج نیک نامی به ز گنج سیم و زر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا بصنع ایزدی ، بر اوج گردون ، شکل ماه </p></div>
<div class="m2"><p>گاه گردد چون کان و گاه گردد چون سپر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بهرهٔ احباب تو باد از جهان لهو و طرب</p></div>
<div class="m2"><p>حصهٔ اعدای تو باد از فلک رنج و ضرر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خسروان را خاک ایوان رفیع تو مقام </p></div>
<div class="m2"><p>صفدران را صحن در گاه شریف تو مقر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باد مژگان همچو بیکانها شده در دیده ها </p></div>
<div class="m2"><p>بر بدی آن را که باشد سوی درگاهت نظر</p></div></div>