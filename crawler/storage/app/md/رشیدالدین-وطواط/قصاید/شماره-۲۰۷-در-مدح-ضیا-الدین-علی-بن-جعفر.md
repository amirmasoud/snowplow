---
title: >-
    شمارهٔ ۲۰۷ - در مدح ضیاء الدین علی بن جعفر
---
# شمارهٔ ۲۰۷ - در مدح ضیاء الدین علی بن جعفر

<div class="b" id="bn1"><div class="m1"><p>ضیاء الدین، ترا در کامرانی </p></div>
<div class="m2"><p>هزاران سال بادا زندگانی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وفاک الله نائبة اللیالی</p></div>
<div class="m2"><p>وصانک من ملمات الزمانی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آن صدری که در صدر تو یابند</p></div>
<div class="m2"><p>همه انواع دانش را نشانی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنابک روضة الاقبال تزری</p></div>
<div class="m2"><p>اطایبها بروضات الجنانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بطلعت صد هزاران آفتابی </p></div>
<div class="m2"><p>برفعت صد هزاران آسمانی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یشق اذا بدا بدر الدیاجی</p></div>
<div class="m2"><p>جبینک والدجی ملقی الجرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو در چشم معالی همچو نوری</p></div>
<div class="m2"><p>تو در جسم معانی همچو جانی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایابن المصطفی قفت البرایا</p></div>
<div class="m2"><p>باصناف المعالی و المعانی </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود از خلق و خلق چون تو فرزند </p></div>
<div class="m2"><p>روان مصطفی را شادمانی </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غدا ولداک بل عضداک بحرا</p></div>
<div class="m2"><p>وسیفا بالبیان والبنانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو تحفه گشته دین را از تو ظاهر </p></div>
<div class="m2"><p>کز ایشانست دین را کامرانی </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هما غیثان لکن فی العطایا</p></div>
<div class="m2"><p>هما لیثان لکن فی الطعانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز شمس دین کرم را تندرستی </p></div>
<div class="m2"><p>ز زین دین ستم را ناتوانی </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فهذا ماله فی العلم ند</p></div>
<div class="m2"><p>فهذا ماله فی الحلم ثانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی را در مکارم پیشوایی </p></div>
<div class="m2"><p>یکی را بر محامد قهرمانی </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رئیس المشرقین غدوت صدرا</p></div>
<div class="m2"><p>جمال شمال اولادالقرانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگاه حزم چون کوه متینی</p></div>
<div class="m2"><p>بوقت عزم چون باد بزانی </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حوی قصب السباق من البرایا</p></div>
<div class="m2"><p>یمینک فی العلی یوم الرهانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر زیر سپهری صد سپهری </p></div>
<div class="m2"><p>وگر اندر جهانی صد جهانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فسخطک کله سودالمنایا</p></div>
<div class="m2"><p>و غلوک کله بیض الامانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بزرگا، در فصاحت داستانم </p></div>
<div class="m2"><p>چنان کندر سخاوت داستانی </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فنثری دونه نثر الئالی</p></div>
<div class="m2"><p>و نظمی دونه نظم الجمانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا با حملهٔ گردون بسذه است</p></div>
<div class="m2"><p>زبانی همچو شمشیر یمانی </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>و فی طی اللسانی تری لعمری</p></div>
<div class="m2"><p>جمال المرء لا فی الطیلسانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>و لیکن چون نبینم رونق فضل</p></div>
<div class="m2"><p>چه سود از فضل و از چیره زبانی ؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سکنت الارض لکن عظم قدری</p></div>
<div class="m2"><p>ثنی عن مدح اهلیها عنانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ندارم دانش خود را سبک سنک</p></div>
<div class="m2"><p>نیارم جز بصدر تو گرانی </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>علیک لدی الوری ماعشت اثنی</p></div>
<div class="m2"><p>نعم و بحرمة سبع المثانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه مدح من خادم ترا باد </p></div>
<div class="m2"><p>که قدر مدح من خادم تو دانی </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نعش الدا علیرغم العادی</p></div>
<div class="m2"><p>طلیق الوجه ، فیاض البنانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز تو دین را بقای جاودانست </p></div>
<div class="m2"><p>ترا بادا بقای جاودانی </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>توتع دائماً مالاح فجر</p></div>
<div class="m2"><p>برفعة منصب و علو شانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بپیش خاطر تو باد پیدا</p></div>
<div class="m2"><p>فلک را جمله اسرار نهانی </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اتاک الموسم المیمون فاسعد</p></div>
<div class="m2"><p>له والبس جاابیب الامانی</p></div></div>