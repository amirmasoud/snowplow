---
title: >-
    شمارهٔ ۱۳۲ - قصیدۀ ملمع در مدح جمال الدین وزیر
---
# شمارهٔ ۱۳۲ - قصیدۀ ملمع در مدح جمال الدین وزیر

<div class="b" id="bn1"><div class="m1"><p>ای جمال دولت ، ای چرخ جلال </p></div>
<div class="m2"><p>وی ز تو افزوده گیتی را جمال </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سعادت را بصدرت انتما </p></div>
<div class="m2"><p>وی سیادت را بقدرت اتصال </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختران از رای تو جویندنور </p></div>
<div class="m2"><p>سروران از روی تو گیرند فال </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده بودن جز ترا باشد خطا </p></div>
<div class="m2"><p>مدح گفتن جز ترا باشد محال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکرمت را از کف تو نظم کار </p></div>
<div class="m2"><p>محمدت را از دل تو حسن حال </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مساعی نیست طبعت را ستوه </p></div>
<div class="m2"><p>وز ایادی نیست دستت را ملال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو اسلاف بزرگ تو نبود </p></div>
<div class="m2"><p>بوستان دین و دولت را نهال </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهل حاجت را ز اطراف جهان </p></div>
<div class="m2"><p>نسیت الا سوی صدرت ارتحال </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انت فی بدر المعالی کامل </p></div>
<div class="m2"><p>صانک الرحمن عن عین الکمال </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کفک الوطفاء ضیعت للندی </p></div>
<div class="m2"><p>تبذل الاموال من قبل السؤال </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دارک الفیحاء ماوی للعلی</p></div>
<div class="m2"><p>رفقک المأمول مثوی للرجال </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آلک الاخیار کانوا فی الهدی </p></div>
<div class="m2"><p>باتفاق من بنیه خیر آل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ان عرتهم حظته ماز عز عوا</p></div>
<div class="m2"><p>ان ریح ز عزمت شم الجبال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کلهم کانوا لیوثا للوغا</p></div>
<div class="m2"><p>کلهم کانو عنوثا للنوال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کان فی ایدیهم اقلامهم </p></div>
<div class="m2"><p>فاعلات للعدی فعل ال نصال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای افاضل را ظلال جاه تو </p></div>
<div class="m2"><p>از سموم نکبت گردون مآل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاشکی من نیز همچون دیگران </p></div>
<div class="m2"><p>هستمی اندر پناه آل ظلال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست دیدار تو مر چشم مرا </p></div>
<div class="m2"><p>چون دهان تشنه آب زلال </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست روزم بی لقای تو چو شب </p></div>
<div class="m2"><p>هست ماهم بی جمال تو چو سال </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چرخ آورد از سر نامردمی </p></div>
<div class="m2"><p>نعمت دیدار را بر من زوال </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کی تواند کرد جز چرخ لئیم </p></div>
<div class="m2"><p>این چنین نامردمی را احتمال ؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بادیا چون بدر بر برج شرف </p></div>
<div class="m2"><p>پشت بدخواهت بخم همچون هلال</p></div></div>