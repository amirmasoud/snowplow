---
title: >-
    شمارهٔ ۱۷۵ - در مدح شمس الدین وزیر
---
# شمارهٔ ۱۷۵ - در مدح شمس الدین وزیر

<div class="b" id="bn1"><div class="m1"><p>مایهٔ افتخار و صورت جاه </p></div>
<div class="m2"><p>ای هدی را ز حادثات پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمس دین آنکه عدل شامل او </p></div>
<div class="m2"><p>بر سر خلق هست ظل الله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن بد و پشت نیک خواه قوی </p></div>
<div class="m2"><p>وان بدو حال بدسگال تباه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک پیامش به از هزار حسام </p></div>
<div class="m2"><p>یک غلامش به از هزار سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاه با عزم او گران چون کوه </p></div>
<div class="m2"><p>کوه با هضم او سبک چون کاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بر احوال آسمان واقف</p></div>
<div class="m2"><p>وی ز اسرار روزگار آگاه </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مژه پیکان شود در آن دیده </p></div>
<div class="m2"><p>که کند سوی او بکینه نگاه </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محمدت چون سپهر و طبع تو مهر </p></div>
<div class="m2"><p>مکرمت چون عروس و دست تو شاه </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو یوسف منزهی ز دروغ</p></div>
<div class="m2"><p>همچو یحیی مطهری ز گناه </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه جهانی و نیستت نخصان</p></div>
<div class="m2"><p>نه خدایی و نیستت اشباه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زده انواع سعد و پیروزی</p></div>
<div class="m2"><p>بر جناب تو خیمه و خرگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آمده از تو محنت و نعمت</p></div>
<div class="m2"><p>حضهٔ بدسگال و نیک خواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفتابی بوقت پاداشن </p></div>
<div class="m2"><p>اژده های بوقت بادا فراه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فتنه نقش طلعت تو عیون</p></div>
<div class="m2"><p>عاشق خاک حضرت تو شفاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دهر مدحت گر تو بی اجبار</p></div>
<div class="m2"><p>چرخ فرمان بر تو بی اکراه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرورا بی قبول تو گشتست</p></div>
<div class="m2"><p>همه انفاس من ز حسرت آه؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی گل و لالهٔ مکارم تو </p></div>
<div class="m2"><p>خوار ماندم بسان خار و گیاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هیچ باشد چو دیده بگشایم </p></div>
<div class="m2"><p>جای خود بینم اندر آن درگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیکی سعی مجلس عالیت</p></div>
<div class="m2"><p>یافته صد هزار حشمت و جاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پا کرده دراز با گردون </p></div>
<div class="m2"><p>دست گردون زمن شده کوتاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا بود باغ جای لاله و گل </p></div>
<div class="m2"><p>تا بود چرخ جای زهره و ماه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باد چشم مخالف تو سپید</p></div>
<div class="m2"><p>باد روز منازع تو سیاه </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بارگاهت مخیم اقبال </p></div>
<div class="m2"><p>پایگاهت مقبل افواه</p></div></div>