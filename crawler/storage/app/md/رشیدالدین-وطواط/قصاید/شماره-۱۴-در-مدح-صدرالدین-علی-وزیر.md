---
title: >-
    شمارهٔ ۱۴ - در مدح صدرالدین علی وزیر
---
# شمارهٔ ۱۴ - در مدح صدرالدین علی وزیر

<div class="b" id="bn1"><div class="m1"><p>تا کی کند حوادث گیتی مرا طلب ؟</p></div>
<div class="m2"><p>کز دست آن طلب شدم آواره طرب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با عزیمت هرب و فتنه از قفا </p></div>
<div class="m2"><p>من با هزیمت ضرر و چرخ در طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه مالدم بعربدها دهر بوالفضول</p></div>
<div class="m2"><p>گه بنددم بشعبدها چرخ بوالعجب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا که من روم عقب من رود عقاب</p></div>
<div class="m2"><p>هر جا که من بوم تبع من بود تعب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس که جور روز و شب آمد بروی من </p></div>
<div class="m2"><p>چونان شدم که روز ندانم همی ز شب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر روز چرخ حادثه‌ای آردم عجیب </p></div>
<div class="m2"><p>هر لحظه دهر واقعی ای زایدم عجب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند: هر رجب عجبی آورد جهان </p></div>
<div class="m2"><p>پس عمر من شدست سراسر همه رجب </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با من سپهر نرد محالات باخته</p></div>
<div class="m2"><p>برده هزار گنج مرا در یکی ندب </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اکنون ز درد بردمنم مانده در فغان</p></div>
<div class="m2"><p>و اکنون ز رنج گنج منم مانده در شغب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دایم بدان سبب ز فلک در شکایتم</p></div>
<div class="m2"><p>کزمن فلک دمار برآورد بی سبب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جرمی دگر نکرده جز از رادی و وفا </p></div>
<div class="m2"><p>عیبی دگر ندارم جز دانش و ادب </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معنی معجز من و لفظ بدیع من</p></div>
<div class="m2"><p>سرمایهٔ عجم شد و پیرایهٔ عرب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با پاک زادگی من آزادگیست نیک</p></div>
<div class="m2"><p>و آن به که ضم شود نسب پاک با حسب </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کانیست طبع من که بود دانشش گهر</p></div>
<div class="m2"><p>نخلیست جان من که بود حکمتش رطب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فخرم پس آنکه در صفت صدر دین حق</p></div>
<div class="m2"><p>« لی منطق تحیر عن حدتی شطب»</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرخ علو علی که بنازد لقب بدو</p></div>
<div class="m2"><p>گر سروران عصر بنازد بر لقب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صدری، که هست مرجع او از بلا </p></div>
<div class="m2"><p>صدری، که هست مجلس او مأمن از نوب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر صحن صدر او ز اکابر نشان رخ</p></div>
<div class="m2"><p>بر پشت دست او ز افاضل نشان لب </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فرزند حیدرست و خداوند مفخرست </p></div>
<div class="m2"><p>چون او کراست منتسب امروز و مکتسب؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فارغ شدست دولتش از خوف انتقال </p></div>
<div class="m2"><p>آمن شدست حشمتش از بیم منقلب </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از فتنهٔ سیاست او چرخ در هراس </p></div>
<div class="m2"><p>وز حملهٔ مهابت او دهر در هرب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قدرش سپهر جاه و معالی درو نجوم </p></div>
<div class="m2"><p>کفش درخت جود و ایادی برو شعب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از قدر او مراتب عیوق منتسحل</p></div>
<div class="m2"><p>وز صدر او مطایب فردوس منتخب </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای ناصحت خزانهٔ رحمت چو بوتراب</p></div>
<div class="m2"><p>وی حاسدت نشانه لعنت چو بولهب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>منسوخ نفس صدق تو شد آیت ریا</p></div>
<div class="m2"><p>مقهور آب عفو تو شد آتش غضب </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مهر تو جرم مهر و نکوخواه تو گهر </p></div>
<div class="m2"><p>جاه تو طبع ماه و بداندیش تو قصب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دوزخ بود ز تف نهیب تو یک شرار</p></div>
<div class="m2"><p>کوثر بود ز آب عطای تو یک حبب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شخص ترا ز عصمت مردانگی لباس</p></div>
<div class="m2"><p>جان ترا ز حکمت فرزانگی سلب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از خدمت جوار تو احباب در حرم </p></div>
<div class="m2"><p>و ز ضربت نهیب تو حساد در ضرب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نزدیک خاص و عام بقدر و بمرتبت</p></div>
<div class="m2"><p>تو چون ذوابه ای و عدوی تو چون ذنب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حاشا که گویمت چو تو خصمت کجا بود </p></div>
<div class="m2"><p>«سیف من الحدید کسیف من الخشب»؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از خدمت بساط تو حاصل شود نشاط </p></div>
<div class="m2"><p>از قربت جناب تو زایل شود کرب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اولی تری بمجد و معالی ز کل خلق</p></div>
<div class="m2"><p>«کالنمل بالورانة و الجار بالعصب»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جود تو طالبان عطار است مرتجی </p></div>
<div class="m2"><p>عفو تو راکبان خطا راست مرتجب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دین را باجتهاد تو واضح شده طریق</p></div>
<div class="m2"><p>حق را باعتقاد تو محکم شده سبب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>داری ذهب برای عطا و سخای خلق </p></div>
<div class="m2"><p>در مذهب تو نیست دفین کردن ذهب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ملک از جمال جاه تو خرم چنان که باغ </p></div>
<div class="m2"><p>از عارض شکوفه و از دیدهٔ عنب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا نام نیک خاطب ابکار نظم شد </p></div>
<div class="m2"><p>جود تو شد صداق و ثنای تو شد خطب </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>منت خدای را که در ایام تو نماند </p></div>
<div class="m2"><p>ابکار نظم بیوه و نام نکو عزب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر کو تعصب تو گزیند ببردش </p></div>
<div class="m2"><p>گردون بتیغ حادثه هم رسغ و هم عصب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدگوی تو چو حاطب لیلست و عاقبت </p></div>
<div class="m2"><p>اسباب ویل خویش ببیند در آن حطب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو اندرین بلاد و سخای تو در حجاز </p></div>
<div class="m2"><p>تو اندرین دیار و ثنای تو در حلب </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا در امور شرع فریضه است و نافله </p></div>
<div class="m2"><p>تا در بحور شعر سریعست و مقتضب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بادا همیشه دولت تو ناظر الریاض !</p></div>
<div class="m2"><p>بادا همیشه همت تو عالی الرطب!</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ماه صیام آمد و آوردت از بهشت </p></div>
<div class="m2"><p>خلعت ردای رحمت و تحفه رضای رب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صوم تو با صیانت و فطر تو بی شبه </p></div>
<div class="m2"><p>فعل تو با امانت و قول تو بی ریب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>روز قضا مقام تو در ظل مصطفی </p></div>
<div class="m2"><p>و آن عدو چو بولهب اندر تف لهب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>باد آن زمان نصاب کرامت نصیب تو </p></div>
<div class="m2"><p>و آنگاه از نصیب تو بدخواه در نصب</p></div></div>