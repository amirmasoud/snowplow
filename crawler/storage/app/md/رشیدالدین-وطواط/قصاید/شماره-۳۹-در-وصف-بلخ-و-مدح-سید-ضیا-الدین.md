---
title: >-
    شمارهٔ ۳۹ - در وصف بلخ و مدح سید ضیاء الدین
---
# شمارهٔ ۳۹ - در وصف بلخ و مدح سید ضیاء الدین

<div class="b" id="bn1"><div class="m1"><p>فدای بلخ دل من،که روضهٔ ارمست</p></div>
<div class="m2"><p>حریم او بامان همچو بیضهٔ حرمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه سعادت بلخ و همه سلامت او </p></div>
<div class="m2"><p>که بیضهٔ حرمست و چو روضهٔ ارمست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه بحر و چرخ و لیکن چو بحر و چرخ مقیم</p></div>
<div class="m2"><p>پر از جواهر مجد و کواکب حرمست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین مواخر آن خطه را بسیست و لیک </p></div>
<div class="m2"><p>همه بجنب وجود ضیاء دین عدمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پناه دودهٔ حیدر که از سیاست او</p></div>
<div class="m2"><p>تفاخر عربست و تظاهر عجمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزرگواری ، فرزانه ای ، خداوندی</p></div>
<div class="m2"><p>که پیش درگه او پشت آسمان بخمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلند همت او همچو چرخ مرفوعست</p></div>
<div class="m2"><p>بزرگ مجلس او همچو کعبه محترمست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر کجا که نهد در طریق دین قدمی </p></div>
<div class="m2"><p>همه ذخایر عقبی طفیل آن قدمست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعلم و حلم و سخا و وفا عدل و حیا</p></div>
<div class="m2"><p>بعالم چون اندر جد خویشتن علمست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ضیاء دین پیمبر تو آن سرافرازی </p></div>
<div class="m2"><p>که بر صحیفهٔ اقبال نام تو رقمست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>معلقست بفرخنده کلک میمونت </p></div>
<div class="m2"><p>همه مصالح دنیا ، مگر نگین جمست؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آنکه پیش تو همچو قلم بسر نرود</p></div>
<div class="m2"><p>سرش بریده و سینه دریده چون قلمست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنظم و نثر در الفاظ تو همه نکتست</p></div>
<div class="m2"><p>بامر و نهی در احکام تو همه حکمست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ضمیر ناصح صدرت خزانهٔ طربست</p></div>
<div class="m2"><p>روان حاسد جاهت نشانهٔ المست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منم که تا ز جناب و دور ماندستم </p></div>
<div class="m2"><p>هر آن دمی که برآرم ندیدم او ندمست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زشوق مجلس و هجر رخ توام دل و چشم</p></div>
<div class="m2"><p>یکی عدیل تفست و یکی ندیم نمست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عنای طبع من و روح روح من بی تو</p></div>
<div class="m2"><p>چو دولت تو فزون و چو حاسد تو کمست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مراست غم که کنون صدر تو نمیبینم</p></div>
<div class="m2"><p>کسی که صدرتو بیند بعالمش چه غمست؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همیشه تا که حدوثست و صف هر موجود </p></div>
<div class="m2"><p>مگر خدای تعالی که وصف او قدمست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل تو شاد و رخت تازه باد ، کز بر چرخ </p></div>
<div class="m2"><p>دل عدوی تو پرانده و رخش دژمست</p></div></div>