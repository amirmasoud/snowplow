---
title: >-
    شمارهٔ ۸۰ - در تهنیت ولادت دو فرزند ملک اتسز
---
# شمارهٔ ۸۰ - در تهنیت ولادت دو فرزند ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>بر آمد ز چرخ معالی دو اختر</p></div>
<div class="m2"><p>فزون گشته در عقد شاهی دو گوهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از این هر دو گوهر هدی شد مزین</p></div>
<div class="m2"><p>وزین هر دو اختر جهان شد منور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیفزود در نسل خوارزمشاهی </p></div>
<div class="m2"><p>دو مقبل ، دو مقصد ، دو سرور ، دو اختر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو بحر مکارم ، دو چرخ مناقب </p></div>
<div class="m2"><p>دو گرد سپه کش ، دو شیر دلاور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو شمع سعادت ، که از نور ایشان </p></div>
<div class="m2"><p>زیاده شد آرایش هفت کشور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی را عنایات افلاک همره </p></div>
<div class="m2"><p>یکی را سعادت ایام همبر </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه یکی را محب و موافق </p></div>
<div class="m2"><p>ستاره یکی را مطیع و مسخر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی همچو شمس از قبایح منزه </p></div>
<div class="m2"><p>یکی همچو عقل از معایب مطهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این عیش احباب گشته مصفا</p></div>
<div class="m2"><p>وزان عمر حساد گشته مکدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو جد و پدر هر دو ملک و دولت</p></div>
<div class="m2"><p>چو شمس و قمر هر دو در جاه و مفخر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن اصل طاهر چنین نسل زاید </p></div>
<div class="m2"><p>که آید زپشت غضنفر غضنفر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بجز در نیابند از موضع در</p></div>
<div class="m2"><p>بجز زر نبینند از معدن زر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو فرزند از روی صورت و لیکن </p></div>
<div class="m2"><p>ازیشان هراس عدو رادو لشکر </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان گشت خواهند در صف کینه</p></div>
<div class="m2"><p> زپیکان ایشان بترسد دو پیکر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبودست زین رو ز موسی و هارون</p></div>
<div class="m2"><p>بعالم درون مثل این دو برادر </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز اولاد خوارزمشه صحن گیتی </p></div>
<div class="m2"><p>نخواهد تهی گشت تار و زمحشر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خداوند خوارزمشاهست شاهی </p></div>
<div class="m2"><p>که از مردی و مردمی شد مصور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی عالمی گشت اندر بزرگی </p></div>
<div class="m2"><p>که در جنب او هست عالم محقر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چهارند ابنای او هم بدان سان </p></div>
<div class="m2"><p>که ارکان عالم چهارند یکسر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بحلم و بحیله ، بیمن و بکینه</p></div>
<div class="m2"><p>چو خاکند و بادند و آبند و آذر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سرافراز ایل ارسلان و سلیمان</p></div>
<div class="m2"><p>که هستد شاهنشه گاه و افسر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گزین تقل تز سن ، ستوده و طاحون</p></div>
<div class="m2"><p>که این تخمه باشد بدیشان مطهر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه ملک را مستحقند و لایق </p></div>
<div class="m2"><p>همه تاج را مستحقن و در خور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بحیرت ز الفاظشان در و لؤلؤ </p></div>
<div class="m2"><p>بغیرت ز اوصافشان تنگ شکر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برین چار صفدر ، کاندر معالی </p></div>
<div class="m2"><p>چو ایشان نیاورد ایام دیگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تفاخر نمایند دین الهی </p></div>
<div class="m2"><p>تظاهر فزایند شرع پیمبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کسی را که اولاد زین گونه باشد</p></div>
<div class="m2"><p>بود ملک در خاندانشان مقدر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>الا تا بود اجتماع دو مردم </p></div>
<div class="m2"><p>الا تا بود اقتران دو اختر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باولاد او باد عالم مزین</p></div>
<div class="m2"><p>باخبار او باد گیتی معطر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بماناد خوارزم شه تا قیامت </p></div>
<div class="m2"><p>بر احباب میمون ، بر اعدا مظفر</p></div></div>