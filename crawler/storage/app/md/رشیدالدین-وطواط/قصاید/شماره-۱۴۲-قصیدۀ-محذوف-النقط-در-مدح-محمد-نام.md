---
title: >-
    شمارهٔ ۱۴۲ - قصیدۀ محذوف النقط در مدح محمد نام
---
# شمارهٔ ۱۴۲ - قصیدۀ محذوف النقط در مدح محمد نام

<div class="b" id="bn1"><div class="m1"><p>که کرد کار کرم مرد وار در عالم ؟</p></div>
<div class="m2"><p>که کرد اساس ممالک ممهد و محکم ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عماد عالم عدل و سوار ساعد ملک </p></div>
<div class="m2"><p>اساس طارم اسلام ، سرور عالم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک علو و عطارد علوم و مهر عطا </p></div>
<div class="m2"><p>سماک رمح واسد حمله و هلال علم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرور اهل محامد ، هلاک عمر عدو</p></div>
<div class="m2"><p>سر ملوک و دلارام ملک واصل حکم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محمد اسم و عمر عدل ، کامر او دهر </p></div>
<div class="m2"><p>ملوک وار در و رسم داد عدل و کرم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلام او سحر حلال در همه کار </p></div>
<div class="m2"><p>مراد او همه اعطای مال در هر دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل مطهر او همدم کمال علوم </p></div>
<div class="m2"><p>در مکرم او مورد صلاح امم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسوم عارک او کرده حکم عالم رد </p></div>
<div class="m2"><p>سموم حملهٔ او کرده کار اعدا کم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم او و هم دل او دار عدل را معمار </p></div>
<div class="m2"><p>هم او و هم دم او درد ملک را مرهم </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مدام طالع مسعود کرده حاصل او </p></div>
<div class="m2"><p>همه رسوم مکارم ، همه علوم همم</p></div></div>