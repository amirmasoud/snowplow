---
title: >-
    شمارهٔ ۱۵۴ - هم در مدح اتسز گوید
---
# شمارهٔ ۱۵۴ - هم در مدح اتسز گوید

<div class="b" id="bn1"><div class="m1"><p>ایا چرخ از حملهٔ تو جهان </p></div>
<div class="m2"><p>غلام جناب رفیعت جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شده فتح را تیغ تو سازگار </p></div>
<div class="m2"><p>شده عدل را ملک تو قهرمان </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاه ترا گشته عصمت پناه</p></div>
<div class="m2"><p>حسام ترا گشته نصرة فسان </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمین پایهٔ قدر تو مهر و ماه </p></div>
<div class="m2"><p>کهین پایهٔ جود تو بحر و کان </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا عالم محمدت زیر دست </p></div>
<div class="m2"><p>ترا بارهٔ مفخرت زیر ران </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه ایام را از تو کاری نهفت </p></div>
<div class="m2"><p>نه افلاک را از تو رازی نهان </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلال تو با چرخ گشته قرین </p></div>
<div class="m2"><p>لوای تو با نصر کرده قران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>براه امل جود تو بدرقه </p></div>
<div class="m2"><p>ز سر ظفر تیغ تو ترجمان </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شده بندهٔ صدر تو مرد و زن </p></div>
<div class="m2"><p>سده سخرهٔ حکم تو انس و جان </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بر بندی از بهر کوشش کمر </p></div>
<div class="m2"><p>چو بگشایی از بهر بخشش بنان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمین جمله رنگین کنی چون بهار </p></div>
<div class="m2"><p>جهان جمله زرین کنی چون خزان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تویی آسمان و زبیم تو خصم </p></div>
<div class="m2"><p>نداند زمین را همی زآسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترا از حوادث زیان نیست هیچ</p></div>
<div class="m2"><p>حوادث را فلک ندارد زیان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنالد عدو چون کمان از فزع</p></div>
<div class="m2"><p>چو تیر تو گردد قرین کمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بشمشیر فتنه نشانت نماند</p></div>
<div class="m2"><p>زفتنه در اطراف عالم نشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وحوش و طیوری ، که در دشتهاست</p></div>
<div class="m2"><p>شده جمله تیغ ترا میهمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو مهر سپهری وزین کرده ای </p></div>
<div class="m2"><p>بروز و بروزی خلقان ضمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو گردد گران صفدران را رکاب </p></div>
<div class="m2"><p>چو گردد سبک سر کشان را عنان </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بتفسد زمین از فروغ ضراب</p></div>
<div class="m2"><p>بجوشد جهان از نهیب طعان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو صرصر برد حمله رخش و سمند</p></div>
<div class="m2"><p>چو آتش زند شعله تیغ و سنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمین را ز خون سپه پیرهن</p></div>
<div class="m2"><p>هوا را ز گرد سیه طیلسان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوان پایها در نهاده بمرگ </p></div>
<div class="m2"><p>یلان دستها برگرفته زجان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز تیغ چو نیلوفر اطراف دشت</p></div>
<div class="m2"><p>شود لعل مانندهٔ ارغوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آن لحظه باشی میان دو صف</p></div>
<div class="m2"><p>چو پیل دمان و چو شیر ژیان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه ایام یابد ز رمحت نجات</p></div>
<div class="m2"><p>نه افلاک یابد ز تیغت امان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز گرزت همه صفدران در خروش</p></div>
<div class="m2"><p>ز تیرت همه سرکشان در فغان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بسا جان که از بدسگالان ملک </p></div>
<div class="m2"><p>ستانی از آن خنجر جان ستان </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خدنگ تو چون شد روان در مصاف </p></div>
<div class="m2"><p>چو خاک زمین خوار گردد روان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو شاه زمینی و ملک زمین</p></div>
<div class="m2"><p>بخواهی گرفتن زمان تا زمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگیتی شود ملک تو مشتهر</p></div>
<div class="m2"><p>بعالم شود جود تو داستان </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بتیغ سرافشان بر آری دمار</p></div>
<div class="m2"><p>هم از قصر قصیر ، هم از خان خان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هموای تو جویند خردو بزرگ </p></div>
<div class="m2"><p>ثنای تو گویند پیرو جوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه پیشوایان دنیا و دین </p></div>
<div class="m2"><p>بخدمت ببندند پیشت میان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زهی ! مشتری طلعتت را رهی</p></div>
<div class="m2"><p>زهی ! آسمان همتت را مکان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تویی بر همه صفدران کامگار</p></div>
<div class="m2"><p>تویی بر همه خسروان کامران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد ز آتش تیغ چون آب تو </p></div>
<div class="m2"><p>دل دشمنان پر شرار و دخان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چرا چشم عیش عدو تیره گشت؟</p></div>
<div class="m2"><p>گرش گر ز تو سرمه کرد استخوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز باران لطف تو در ماه دی </p></div>
<div class="m2"><p>بروید ز خارا همی ضیمران</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهان داشت آیین و سان ستم </p></div>
<div class="m2"><p>ز خوف دگر کرده آیین و سان </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شها ، حال بنده دانسته ای </p></div>
<div class="m2"><p>که هستم بمدحت گشاده زبان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بمهر تو پرداخته جان و دل </p></div>
<div class="m2"><p>ز بهر تو بگذاشته خانمان </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بغیبت ترا بوده ام شکر گوی</p></div>
<div class="m2"><p>بحضرت ترا بوده ام مدح خوان </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو دانی که : امروز از اهل فضل </p></div>
<div class="m2"><p>منم بر سپاه سخن پهلوان </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز نثرم بخجلت نجوم سپهر </p></div>
<div class="m2"><p>ز نظم بحیرت عقود جمان </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه چون صوت من نفمهٔ عندلیت</p></div>
<div class="m2"><p>نه چون طبع من ساحت بوستان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بنظم سخن دو زبانم ولیک </p></div>
<div class="m2"><p>بنقل سخن نیستم دو زبان </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نبینی در افعال من هیچ بد </p></div>
<div class="m2"><p>اگر سیرت من کنی امتحان </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>منم بندهٔ مخلص تو بطبع </p></div>
<div class="m2"><p>مرا بندهٔ مخلص خویش خوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مشو بدگمان در چو من بنده ای </p></div>
<div class="m2"><p>بتمویه این و بتزویر آن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بیک قصد صاحب غرض مدتی </p></div>
<div class="m2"><p>فتادم بکنجی درون ناتوان </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه روح مرا راحت لهو و عیش </p></div>
<div class="m2"><p>نه شخص مرا لذت آب و نان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز مویه شدم همچو مویی نحیف </p></div>
<div class="m2"><p>ز ناله شدم همچو نالی نوان </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بحق معالی ، که بودی دریغ </p></div>
<div class="m2"><p>اگر در عنا مردمی رایگان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه کردم ؟ چه آمد ز من در وجود؟</p></div>
<div class="m2"><p>که گشتم بدین سان اسیر هوان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه با لذت و راحتم اتصال</p></div>
<div class="m2"><p>نه با نعمت و حرمتم اقتران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در آنروزگاری که حاصل نبود</p></div>
<div class="m2"><p>مرا عشر این نظم و نثر و بیان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>باین طول مدت درین بارگاه</p></div>
<div class="m2"><p>باین حق خدمت درین آستان </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رسیدم هر ساله شش هفت بار</p></div>
<div class="m2"><p>زصدر تو تشریفهای گران</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گهی جامهایی چو باغ بهار</p></div>
<div class="m2"><p>گهی بارهایی چو باد بزان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گهی بدرهایی پر از زر سرخ</p></div>
<div class="m2"><p>گهی بردگانی چو حور جنان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو شد بیکران خدمت و فضل من</p></div>
<div class="m2"><p>چرا کم شد آن بخشش بیکران؟</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همی تا نباشد عیان چون خبر</p></div>
<div class="m2"><p>همی تا نباشد یقین چون گمان </p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ترا باد مجد و معالی مدام</p></div>
<div class="m2"><p>ترا باد ملک بقا جاودان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ولی ترا روی چون لاله سرخ</p></div>
<div class="m2"><p>عدوی ترا چهره چون زعفران</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بماناد تا دامن رستخیز</p></div>
<div class="m2"><p>همی پادشاهی در این خاندان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همایونت عید و ز خوف و وعید</p></div>
<div class="m2"><p>عدو مانده غمگین و تو شادمان</p></div></div>