---
title: >-
    شمارهٔ ۱۲۶ - در مدح اتسز
---
# شمارهٔ ۱۲۶ - در مدح اتسز

<div class="b" id="bn1"><div class="m1"><p>شبی که هست کف او خزانهٔ ارزاق </p></div>
<div class="m2"><p>ز طبع اوست وجود مکارم الاخلاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابوالمظفر ، شاه مظفر ، اتسز ، کوست</p></div>
<div class="m2"><p>خدایگان همه خسروان علی الاطلاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنام اوست کمال صحیهٔ آداب </p></div>
<div class="m2"><p>بجاه اوست جمال بسیطهٔ آفاق </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محبت در او نقش گشته در ارواح </p></div>
<div class="m2"><p>عطیهٔ کف او طوق گشته در اعناق </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آسمان شرف مهر و ماه دولت او </p></div>
<div class="m2"><p>نرفته سوی غروب و ندیده روی محاق </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جریدهای سخن را بنقش مدحت اوست</p></div>
<div class="m2"><p>تفاخر صفحات و تظاهر اوراق </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدایگانا ، چند از وغا؟که عاجز گشت </p></div>
<div class="m2"><p>ز زخم حد حسام وز حمله گام براق </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجام جام گساران شدست وقت وصال </p></div>
<div class="m2"><p>ز تیغ تیغ گزاران شدست گاه فراق </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار بار سپردی بگام نصرة و فتح </p></div>
<div class="m2"><p>همه بلاد حجاز و همه دیار عراق </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار قلعه گشادی ، که هیچ قلعه نبود </p></div>
<div class="m2"><p>کم از حصار سمرقند و حصن منقشلاق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگیر آخر ، یک باده ، بی هزار مصاف </p></div>
<div class="m2"><p>ز ساقیان سمن ساعدین و سیمین ساق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلند باد بتو نام خنجر و خامه </p></div>
<div class="m2"><p>که مرد خجر و خامه تویی باستحقاق </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بطبع با تو جهان را بچاکری پیمان </p></div>
<div class="m2"><p>بطوع با تو فلک را ببندگی میثاق </p></div></div>