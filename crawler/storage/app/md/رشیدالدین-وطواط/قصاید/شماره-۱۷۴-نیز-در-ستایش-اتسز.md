---
title: >-
    شمارهٔ ۱۷۴ - نیز در ستایش اتسز
---
# شمارهٔ ۱۷۴ - نیز در ستایش اتسز

<div class="b" id="bn1"><div class="m1"><p>ای یک غلام تو به گه حرب صد سپاه</p></div>
<div class="m2"><p>اندر جوار جاه تو اسلام را پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقبل‌ترین عالمی و طالع تو را</p></div>
<div class="m2"><p>هشت آسمان معسکر و هفت اختران سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج سخاوت تو رسیده به شرق و غرب</p></div>
<div class="m2"><p>اوج جلالت تو گذشته ز مهر و ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی ولی تو ز وفاق تو شد سفید</p></div>
<div class="m2"><p>روی عدوی تو ز خلاف تو شد سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بر سریر ملک نشینی بر غم خصم</p></div>
<div class="m2"><p>بر تن قبای دولت و بر سر کلاه جاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تصحیف گشته بر تن حساد تو قبا</p></div>
<div class="m2"><p>مقلوب گشته بر سر اعدای تو کلاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید از آن قرار بگیرد همی بشب</p></div>
<div class="m2"><p>تا پیش بارگاه تو حاضر شود پگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای پادشاه عادل و ای آنکه در جهان</p></div>
<div class="m2"><p>هم آفتاب ملکی و هم سایهٔ الاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سی سال شد که بنده بصف نعال تو</p></div>
<div class="m2"><p>بوده مدیح خوان تو بر تخت و مدح خواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داند خدای عرش که هرگز نایستاد</p></div>
<div class="m2"><p>چون بنده مدح خوانی در هیچ بارگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اکنون دلت ز بندهٔ سی ساله شده ملول</p></div>
<div class="m2"><p>در دل ز طول مدت یابد ملال راه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از بنده یک گناه بسی سال مانده است</p></div>
<div class="m2"><p>دانی اگر بچشم حقیقت کنی نگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیکن مثل زننده : چو مخدوم شد ملول</p></div>
<div class="m2"><p>جوید گناه چاکر بیچاره بی گناه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای بس شبا! که در غم درگاه فرخت</p></div>
<div class="m2"><p>نغنوده ام ز ناله و ناسوده ام ز آه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جانم شده تباه بدست مخالفان</p></div>
<div class="m2"><p>عهد ولا و طاعت تو ناشده تباه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو حق خدمت من مسکین نگاه دار</p></div>
<div class="m2"><p>چونانکه حق خدمت تو داشتم نگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا کاه پایدار نباشد بسان کوه</p></div>
<div class="m2"><p>تا کوه بی قرار نباشد بسان کاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بادا مقامگاه ولی تو اوج چرخ</p></div>
<div class="m2"><p>بادا قرارگاه عدوی تو قعر چاه</p></div></div>