---
title: >-
    شمارهٔ ۱۰۴ - نیز در مدح ملک اتسز گوید
---
# شمارهٔ ۱۰۴ - نیز در مدح ملک اتسز گوید

<div class="b" id="bn1"><div class="m1"><p>جهان سرای غرورست، نی سرای سرور</p></div>
<div class="m2"><p>طمع مدار سرور اندرین سرای غرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعاقبت بحسام هوان شود مجروح</p></div>
<div class="m2"><p>دلی که او بحطام جهان شود مسرور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فساد دین همه از جمع خواسته است و ترا</p></div>
<div class="m2"><p>همیشه همت بر جمع خواسته مقصور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حال عقبی چون گمرهان مشو غافل</p></div>
<div class="m2"><p>بمال دنیا چون ابلهان مشو مغرور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بریده کن طمع باطل از طعام خبیث </p></div>
<div class="m2"><p>گر اعتقاد تو حقست در شراب طهور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مده بدنیی عقبی ،که عاقلان ندهند</p></div>
<div class="m2"><p>بدین سفینهٔ ظلمت چنان حدیقهٔ نور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا دلیست بدام هوی شده مأخوذ</p></div>
<div class="m2"><p>ترا سریست بجام هوس شده مخمور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه هیچ رسم تو اندر سبیل حقی مرضی</p></div>
<div class="m2"><p>نه هیچ سعی تو اندر طریق دین مشکور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو در معاصی و حور و قصور داری چشم</p></div>
<div class="m2"><p>بدین طریق نیابد بدست حور و قصور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخیر کوش،که الا بخیر نتواند یافت</p></div>
<div class="m2"><p>نعیم روضهٔ خلد ونسیم طرهٔ حور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بساز کار، که وقت رحیل شد نزدیک</p></div>
<div class="m2"><p>مدار آنچه نه دورست از دل خود دور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زبارگاه الهی رسول این بست </p></div>
<div class="m2"><p>که عارضین چو مشک تو گشت کافور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گناهکار، اگرچه جوان، نه معذورست</p></div>
<div class="m2"><p>پس از طلایع پیری کجا بود معذور؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بقهر خلق مشو شادمان ،که خواهی گشت</p></div>
<div class="m2"><p>ز دست مرگ،اگرچند قاهری،مقهور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگاه کن که شهنشاه شرق خانهٔ شرع</p></div>
<div class="m2"><p>چگونه کرد باخلاق خوب خود معمور؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابوالمظفر، خورشید خسروان ،اتسز</p></div>
<div class="m2"><p>که هست رایت ایمان بتیغ او منصور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدایگانی ، کز جاه اوست در اسلام </p></div>
<div class="m2"><p>همه نظام عقود و همه قوام امور </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز فیض مکرمت او نماند کس درویش</p></div>
<div class="m2"><p>ز لطف عاطفت او نماند کس رنجور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رفیع همت او فرق ملک را افسر </p></div>
<div class="m2"><p>شریف خاطر او گنج فضل را گنجور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهان چرخ همه پیش علم او مکشوف</p></div>
<div class="m2"><p>گناه خلق همه نزد او مغفور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بروز معرکه پیکان تیر او کرده </p></div>
<div class="m2"><p>تن مخالف دین همچو خانه زنبور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گسسته در صف پیکار دست قدرت او</p></div>
<div class="m2"><p>سر عدو زکنف همچو خوشه انگور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه هیچ کار جهان در پناه او مشکل</p></div>
<div class="m2"><p>نه هیچ راز فلک از ضمیر او مستور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هوای مجلس او را نسیم ساحت خلد</p></div>
<div class="m2"><p>غریو موکب او را نهیب نفحهٔ صور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نموده چهره ظفر از غبار شبدیزش</p></div>
<div class="m2"><p>چنان که روشنی صبح در شب دیجور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کمینه عامل او همچو کسری و دارا</p></div>
<div class="m2"><p>کهینه حاجب او همچو قیصر و فغفور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زهی بعدل تو آسایش صغار و کبار</p></div>
<div class="m2"><p>زهی بعهد تو آرامش سنین و شهور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ترا بنشر ایادی صنایع معروف </p></div>
<div class="m2"><p>ترا بقهر اعادی وقایع مشهور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بعلم و عدل تو رونق گرفته دولت و دین </p></div>
<div class="m2"><p>بنوح و موسی حرمت گرفت جودی و طور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مخالفان ترا و موافقان ترا</p></div>
<div class="m2"><p>زکین تو همه ماتم،زمهر تو همه سور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بپیش حلم تو بس بادسار بوده جبال</p></div>
<div class="m2"><p>بپیش علم تو بس خاکسار گشته بحور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برزمگاه تو از شخص بدسگالانت</p></div>
<div class="m2"><p>شگرف مایده ای ساخته وحوش و طیور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خدایگانا ،سی ساله مدح خوان توام</p></div>
<div class="m2"><p>ز مدحت تو شدم در همه جهان مذکور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بکامرانی دارم ز جاه تو توقیع</p></div>
<div class="m2"><p>بشادمانی دارم ز جود تو منشور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر آسیای بلا بر سرم بگردانند</p></div>
<div class="m2"><p>ز بندگیت نگردم بغیبت و بحضور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>منم که با صدمات بلا مرا دادند</p></div>
<div class="m2"><p>تنی عظیم حمول و دلی عظیم صبور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نفور باد زمن راحت حیوة،اگر</p></div>
<div class="m2"><p>شوم ز طاعت تو تا بوقت مرگ نفور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همیشه تا که سلامت بود ز صدق و سداد</p></div>
<div class="m2"><p>همیشه تا که ملامت بود ز فسق و فجور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سرای عالی تو بادقبلهٔ اقبال</p></div>
<div class="m2"><p>جناب فرخ تو کعبهٔ جمهور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ترا زمانه غلام و ترا جهان چاکر</p></div>
<div class="m2"><p>ترا ستاره مطیع وترا فلک مأمور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بعید فطر صلوة و زکوة تو مقبول </p></div>
<div class="m2"><p>بماه روزه صیام و قیام تو مبرور</p></div></div>