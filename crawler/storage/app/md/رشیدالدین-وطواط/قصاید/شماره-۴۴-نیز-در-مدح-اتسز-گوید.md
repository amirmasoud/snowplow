---
title: >-
    شمارهٔ ۴۴ - نیز در مدح اتسز گوید
---
# شمارهٔ ۴۴ - نیز در مدح اتسز گوید

<div class="b" id="bn1"><div class="m1"><p>در زلف تو ، ای نگار ، تابیست</p></div>
<div class="m2"><p>زان تاب دلم قرین تابیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تاب دو زلف تو دلم را </p></div>
<div class="m2"><p>نی هیچ توان ، نه هیچ تابیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نا مؤمنم از چو دو لب تو </p></div>
<div class="m2"><p>در میکدهٔ مغان شرابیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چهره نقاب از چه باشی ؟</p></div>
<div class="m2"><p>خود حشمت تو مرا حجابیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آتش هجر خاک کویت </p></div>
<div class="m2"><p>جانم همه ساله در عذابیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من باد فروخته بهر جای </p></div>
<div class="m2"><p>یعنی که مرا بر تو آبیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هجر تو اگر چه زود گیریست</p></div>
<div class="m2"><p>وصل تو اگر چه دیریابیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در محنت تو مرا در نگریست </p></div>
<div class="m2"><p>در کشتن من ترا شتابیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جور تو ما و حضرت شاه </p></div>
<div class="m2"><p>کز حادث ها بهین مآبیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوارزمشه اتسز ، آنکه گردون </p></div>
<div class="m2"><p>از هیبت او در اضطرابیست </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر نقطهٔ جاه او جهانیست </p></div>
<div class="m2"><p>هر نکتهٔ خط او کتابیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مملوک جناب فرخ اوست </p></div>
<div class="m2"><p>هر جای که مالک الرقابیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دریای محیط پر جواهر </p></div>
<div class="m2"><p>در پیش بنان او سرابیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رایش بگه ضیا سهیلیست </p></div>
<div class="m2"><p>عزمش بگه مضا شهابیست </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مر دشمن و دوست را ز فعلش </p></div>
<div class="m2"><p>هر لحظه ثوابی و عقابیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای آنکه بهر نفس در آفاق </p></div>
<div class="m2"><p>از فیض کف تو فتح بابیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنی که بدست تو هر انگشت </p></div>
<div class="m2"><p>هنگام عطا دهی سحابیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در دست تو از ظفر عنانیست</p></div>
<div class="m2"><p>در پای تو از شرف رکابیست</p></div></div>