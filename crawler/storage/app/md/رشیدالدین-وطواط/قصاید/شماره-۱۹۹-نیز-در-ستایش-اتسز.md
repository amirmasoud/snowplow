---
title: >-
    شمارهٔ ۱۹۹ - نیز در ستایش اتسز
---
# شمارهٔ ۱۹۹ - نیز در ستایش اتسز

<div class="b" id="bn1"><div class="m1"><p>ای همایون در تو کعبهٔ بهروزی </p></div>
<div class="m2"><p>رایت عالی تو آیت پیروزی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت بهروز هر آن کس که بتو پیوست </p></div>
<div class="m2"><p>خدمتت نیست مگر مایهٔ بهروزی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی خلق زمین نیست مگر از تو </p></div>
<div class="m2"><p>آسمان ، ملکا ، کز تو بود روزی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشت ملکی و همی ملک بیفزاری</p></div>
<div class="m2"><p>روی شرعی و همی شرع بیفروزی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار ناصح بکف راد همی سازی </p></div>
<div class="m2"><p>جان حاسد بتف تیغ همی سوزی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ چو گه حمله بکف گیری </p></div>
<div class="m2"><p>مرگ را غارت ارواح بیآموزی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بنیزه همه جز سینه نمی دری </p></div>
<div class="m2"><p>تو بپیکان همه جز دیده نمی دوزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیکی تیر ، که در حرب بیندازی </p></div>
<div class="m2"><p>بی عدد کینه ز بدخواه بیندوزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مال ایام باحرار همی بخشی </p></div>
<div class="m2"><p>وام احرار ز ایام همی توزی </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بندهٔ دست تو و چاکر جودتست</p></div>
<div class="m2"><p>ابر نوروزی و باران شبانروزی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عید و نوروز اگر نیست ، تو باقی مان </p></div>
<div class="m2"><p>که جهان را بکرم عیدی و نوروزی </p></div></div>