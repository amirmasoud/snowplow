---
title: >-
    شمارهٔ ۱۲۴ - نیز در ستایش شمس‌الدین وزیر
---
# شمارهٔ ۱۲۴ - نیز در ستایش شمس‌الدین وزیر

<div class="b" id="bn1"><div class="m1"><p>ای صدر سروران زمانه به اتفاق</p></div>
<div class="m2"><p>جود تویی نهایت و بر تویی نفاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شمسی و ز نور تو حساد محترق</p></div>
<div class="m2"><p>آری ستاره را بود از شمس احتراق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایام با مخالف تو هست در خلاف</p></div>
<div class="m2"><p>اقبال با موافق تو هست در وفاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر مناقب تو شده ایمن از زوال</p></div>
<div class="m2"><p>ماه فضایل تو شده فارغ از محاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هر نشاط ناصح جاه تو گشته جفت</p></div>
<div class="m2"><p>وز هر مراد حاسد صدر تو مانده طاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پیش رأی تو ز سعادت بود دلیل</p></div>
<div class="m2"><p>در زیر زین تو ز سیادت بود براق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای آفتاب حشمت تو دایم الضیا</p></div>
<div class="m2"><p>وی بارگاه دولت تو عالی الرواق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو ساکنی بخطهٔ خوارزم وصیت تو</p></div>
<div class="m2"><p>چون صبح منتشر شده در بغهٔ عراق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقد معالی ار تو فزونست اتظام</p></div>
<div class="m2"><p>کار ممالک از تو گرفتست اتساق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده دل ولی تو لذات را نکاح</p></div>
<div class="m2"><p>داده تن عدوی تو راحات و اطلاق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انصاف ملک را بجناب تو اجتماع</p></div>
<div class="m2"><p>اعدای شرع راز نهیبت تو افتراق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تابر سپهر پیکر جوزا کند طلوع</p></div>
<div class="m2"><p>تا بر میان پیکر جوزا بود نطاق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بادا منازعان ترا باعنا وصال</p></div>
<div class="m2"><p>بادا معاندان ترا از طرب فراق</p></div></div>