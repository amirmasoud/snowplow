---
title: >-
    شمارهٔ ۴۹ - در مدح شمس الدین وزیر
---
# شمارهٔ ۴۹ - در مدح شمس الدین وزیر

<div class="b" id="bn1"><div class="m1"><p>کریمی ، که رسم معلی نهاد </p></div>
<div class="m2"><p>بزرگی ، که راه ایادی گشاد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجل شمس دین پیمبر ، کزوست</p></div>
<div class="m2"><p>رخ فضل تازه ، دل عقل شاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آبا و از امهات جهان </p></div>
<div class="m2"><p>چنو هیچ فرزند صالح نزاد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تبار و نژادش بزرگند و اوست </p></div>
<div class="m2"><p>چراغ تبار و جمال نژاد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آثار او زینت جود و علم </p></div>
<div class="m2"><p>بایام او رونق دین و داد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مجد و معالیست او را سرشت</p></div>
<div class="m2"><p>ز جود و ایادیست او را نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه ماند از دقایق که طبعش ندید؟</p></div>
<div class="m2"><p>چه مانداز دفاین که دستش نداد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز حزمش گرفتست آرام خاک </p></div>
<div class="m2"><p>ز عزمش ربودست تعجیل باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شده فتح را داد او راهبر </p></div>
<div class="m2"><p>شده بحر را جود او اوستاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیفراخت اعلام فضل و کرم </p></div>
<div class="m2"><p>بدان طبع پاک و بدان دست راد </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیک صدمت کین او بدسگال </p></div>
<div class="m2"><p>ز پای اندر آمد ، ز دست او فتاد </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی تا بخوانند اهل خرد </p></div>
<div class="m2"><p>تواریخ کیخسرو و کیقباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز دور سپهر وز شیر نجوم </p></div>
<div class="m2"><p>نصیبش همه عز و اقبال باد </p></div></div>