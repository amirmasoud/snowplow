---
title: >-
    شمارهٔ ۲۰۹ - در مدح اتسز
---
# شمارهٔ ۲۰۹ - در مدح اتسز

<div class="b" id="bn1"><div class="m1"><p>بزلف مشکی، جانا، بچهره دیبایی </p></div>
<div class="m2"><p>چو تو نباشد، دانم، کسی بزیبایی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا تو گویی: در هجر من شکیبا شو </p></div>
<div class="m2"><p>کرا بود ز چنین صورتی شکیبایی ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان ببندی و هر ساعت از حدیث مرا</p></div>
<div class="m2"><p>هزار چشمهٔ خون از دو دیده بگشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی بخار جفا جان من بیازاری</p></div>
<div class="m2"><p>گهی ببار عنا شخص من بفرسایی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جورت، ای شده جانم نشانهٔ غم تو </p></div>
<div class="m2"><p>بجان رسیده مرا کار و هم نبخشایی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز در بر من نامدی شبی بمراد</p></div>
<div class="m2"><p>بر چو سیم دردا که رفت و برنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز پای درآورد درد و اندوه تو</p></div>
<div class="m2"><p>بگیر دست من، آخر کرا همی بایی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر خیال تو شب‌ها نیامدی بر من </p></div>
<div class="m2"><p>مرا چه بود برین حال ؟ اینت رسوایی ! </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیال تو ز وفا هیچ می نیاساید </p></div>
<div class="m2"><p>چنان‌که تو ز جفا هیچ می نیاسایی </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو از خیال نگارین بتیره شب‌ها در </p></div>
<div class="m2"><p>چو روز کلبهٔ ادبار من بیارایی </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلاه گوشهٔ حکم تو چون پدید آید </p></div>
<div class="m2"><p>در افتد ز سرمه کلاه رعنایی </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بموکب تو روان همچو بندگان خورشید </p></div>
<div class="m2"><p>میان بفخر ببندد، اگر بفرمایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگرد من سپه غم گرفته‌ای تو چنان </p></div>
<div class="m2"><p>دلیروار، ندانم که از کجا آیی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بهر جستن من همچو امر خسرو شرق</p></div>
<div class="m2"><p>بیک زمان همه آفاق را بپیمایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>علاء دولت، خوارزمشاه عالی رأی</p></div>
<div class="m2"><p>که برد هیبت رایش ز دهر خود رایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدایگانی، کندر مصاف هیبت اوست </p></div>
<div class="m2"><p>هزار لشکر آراسته بتنهایی </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ضیای طلعت فرخندهٔ مبارک او</p></div>
<div class="m2"><p>شدست دیدهٔ اقبال را چو بینایی </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خدایگانا، چونانکه کهربا که را</p></div>
<div class="m2"><p>ز پشت اسب عدو را بنیزه بربایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر نه عاشق فتحست خنجر تو چرا </p></div>
<div class="m2"><p>رخش ز خون چو رخ عاشقان بیالایی؟ </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان‌که خسرو سیارگان ز خیل نجوم </p></div>
<div class="m2"><p>ز خسروان جهان تو بجاه والایی </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بقدر صاحب چرخ هزار خورشیدی </p></div>
<div class="m2"><p>بجود ناسخ موج هزار دریایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدست جود دفین زمین همی ‌پاشی </p></div>
<div class="m2"><p>بپای قدر جبین فلک همی سایی </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برزم جز همه اخبار فتح ننیوشی</p></div>
<div class="m2"><p>برزم جز همه آثار فتح ننمایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه منافق سوزی و ملت افروزی </p></div>
<div class="m2"><p>همه مخالف کاهی و دولت افزایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بروز مکرمت اقبال عمر احبابی</p></div>
<div class="m2"><p>بروز معرکه آشوب جان اعدایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آن عدو که چو سرو سهی بر آرد سر</p></div>
<div class="m2"><p>بسان سرو مسطح سرش بپیرایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بصیقل خرد صافی و دل روشن</p></div>
<div class="m2"><p>ز روی آینه ملک زنگ بزدایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه سعادت ملک و فروغ دین از تست </p></div>
<div class="m2"><p>که مشتری اثر و آفتاب سیمایی </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بطوع مسند شاهی ترا مهیا شد </p></div>
<div class="m2"><p>کراست مسند شاهی بدین مهیایی ؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه بادی بر جای ، زانکه شرع رسول</p></div>
<div class="m2"><p>بود منزه ز آفات، تا تو بر جایی </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مبادا هیچ پریشانیی بمجلس تو</p></div>
<div class="m2"><p>مگر ز طرهٔ مه پیکران یغمایی</p></div></div>