---
title: >-
    شمارهٔ ۵۹ - و هم در ستایش  اتسز گوید
---
# شمارهٔ ۵۹ - و هم در ستایش  اتسز گوید

<div class="b" id="bn1"><div class="m1"><p>ای آنکه در جهان ز تو سری نهان نماند </p></div>
<div class="m2"><p>با عدل تو نشان ستم در جهان نماند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چرخ تیغ فتنه نشان در کفت نهاد </p></div>
<div class="m2"><p>از فتنه هدف در نواحی عالم نشان نماند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خسروان عرصهٔ عالم، بعلم و حلم</p></div>
<div class="m2"><p>بر تخت خسروی چو تو صاحب قران نماند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کوکبان جاه تو در کل خافقین </p></div>
<div class="m2"><p>آوازهٔ کواکب هفت آسمان نماند </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن کس که کرد با تو بجان باختن خطر</p></div>
<div class="m2"><p>در ششدر نهیب تو جز رایگان نماند </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر طیر و وحش گرسنه را در فضای دشت</p></div>
<div class="m2"><p>چون تیغ بی‌دریغ تو یک میزان نماند </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخان جود تو شکم هیچ کس تهی</p></div>
<div class="m2"><p>زیر سپهر، جز شکم و بحر و کان نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر همت رفیع ترا در علو جاه</p></div>
<div class="m2"><p>جز گنبد محیط شریک عیان نماند </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر حفظ جان و مال بشبها ز عمل تو </p></div>
<div class="m2"><p>بر هیچ نقطه مشغلهٔ پاسبان نماند </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در راه‌های مهلک بی خوف و بی رجا </p></div>
<div class="m2"><p>جز عصمت تو بدرقهٔ کاروان نماند </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آثار خنجر تو که دارد نهاد جان </p></div>
<div class="m2"><p>اندر نهاد خصم تو آثار جان نماند </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با بد سگال تو ز نشان مبارزت </p></div>
<div class="m2"><p>جز قامت خمیده بشکل کمان نماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خط اعتبار بر اوراق روزگار </p></div>
<div class="m2"><p>بی شرح کرده های تو یک داستان نماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای خسرو جوان ، ز جفاهای بخت پیر </p></div>
<div class="m2"><p>جز حضرت تو ملجأ پیر و جوان نماند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از حادثات عالم غدار بی وفا</p></div>
<div class="m2"><p>جز در پناه جاه تو کس را امان نماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اندر حریم دولت جاوید تو کسی </p></div>
<div class="m2"><p>سر گشتهٔ حوادث آخر زمان نماند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک اهل فضل در همه اطراف شرق و غرب </p></div>
<div class="m2"><p>در عهد روزگار تو بی نام و نان نماند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای در جهان یقین شده آثار خیر تو </p></div>
<div class="m2"><p>اند خلود ذکر تو کس را گمان نماند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن خسروان ، که نام نکو کسب کرده اند </p></div>
<div class="m2"><p>رفتند و یادگار از ایشان جز آن نماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایشان نهان شدند در این جوف خاکدان </p></div>
<div class="m2"><p>لیکن شعار کردهٔ ایشان نهان نماند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نوشین روان ، اگر چه فراوانش گنج بود </p></div>
<div class="m2"><p>جز نام نیک از پس نوشین روان نماند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عید آمدست ، باش بدو شادمان ، که خصم </p></div>
<div class="m2"><p>از آفت و وعید قضاد شادمان نماند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای عید مومنان ، بجهان جاودان بمان </p></div>
<div class="m2"><p>ور چند هیچ کس بجهان جاودان نماند</p></div></div>