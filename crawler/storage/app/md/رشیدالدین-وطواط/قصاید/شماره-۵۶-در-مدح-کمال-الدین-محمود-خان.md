---
title: >-
    شمارهٔ ۵۶ - در مدح کمال‌الدین محمود خان
---
# شمارهٔ ۵۶ - در مدح کمال‌الدین محمود خان

<div class="b" id="bn1"><div class="m1"><p>ای بعزم تو فتح را پیوند </p></div>
<div class="m2"><p>پست با همت تو چرخ بلند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خان عادل تویی و از عدلت</p></div>
<div class="m2"><p>هست چون خلد عدن خطبه چند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دین حق را کمالی و مرساد</p></div>
<div class="m2"><p>بکمال تو از زوال گزند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو بر فرق نیک خواهان تاج </p></div>
<div class="m2"><p>و زتو بر پای بدسگالان بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسروان را بمهر تو ایمان </p></div>
<div class="m2"><p>سروران را به جان تو پیوند </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خار با یاد مجلس تو چو گل </p></div>
<div class="m2"><p>زهر با مهر حضرت تو چو قند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناصح از دولت تو یابد کام </p></div>
<div class="m2"><p>حاسد از صولت تو گیرد پند </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مملکت بر کنار نشاندست</p></div>
<div class="m2"><p>از تو امیدوار تر فرزند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشت زار امید شد تازه</p></div>
<div class="m2"><p>تا کفت تخم جود بپراکند </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پردهٔ جهل علم تو بدرید</p></div>
<div class="m2"><p>خانه ظلم عدل تو بر کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لفظ تو گوشها بدر آراست</p></div>
<div class="m2"><p>خلق تو مغزها بعطر آکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برنخیزد مگر به نفع الصور</p></div>
<div class="m2"><p>هر مه را زخم تیغ تو بفکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خسروا، بوده‌ام بهر وقتی </p></div>
<div class="m2"><p>از قبول در تو روزی مند </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دسته‌ اعراض تو مرا امروز </p></div>
<div class="m2"><p>سوخت بر آتش عنا چو سپند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشته ام بی عنایت تو دژم</p></div>
<div class="m2"><p>مانده ام بی رعایت تو نژند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرخ بیدادگر بروی آورده </p></div>
<div class="m2"><p>این چنین محنتم بهریک چند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حال بنده به کام بدخواهست </p></div>
<div class="m2"><p>اندرین حال بنده را مپسند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بود پشت عاشقان چو کمان </p></div>
<div class="m2"><p>تا بود زلف دلبران چو کمند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باد گیتی به امر تو راضی</p></div>
<div class="m2"><p>باد گردون بحکم تو خرسند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سوی صدر رفیعت آورد</p></div>
<div class="m2"><p>عاملان تو مال چاچ و خجند</p></div></div>