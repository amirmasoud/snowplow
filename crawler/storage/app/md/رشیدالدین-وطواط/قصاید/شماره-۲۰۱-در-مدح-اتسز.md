---
title: >-
    شمارهٔ ۲۰۱ - در مدح اتسز
---
# شمارهٔ ۲۰۱ - در مدح اتسز

<div class="b" id="bn1"><div class="m1"><p>نگارینا ، بهر معنی تمامی </p></div>
<div class="m2"><p>دل از وصل تو یابد شاد کامی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقامت حسرت سرو بلندی </p></div>
<div class="m2"><p>بطلعت غیرت ماه تمامی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثغوزک مثل عقدالدر حسنا </p></div>
<div class="m2"><p> و عقدالدر متشق النظامی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و مجهک کالهلال اذاتبدی </p></div>
<div class="m2"><p>یشق سناه اودیةالظلامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خور تابان ، که سلطان نجومست </p></div>
<div class="m2"><p>کند پیش جمال تو غلامی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پریروی و پری خویی و با ما </p></div>
<div class="m2"><p>نه جفت جامه و نه یار جامی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جبینک ساطع کالبرق ، منه </p></div>
<div class="m2"><p>جفونی هاطلات کالغمامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و جفنک لیس یدعی الجفن الا</p></div>
<div class="m2"><p>و فی الحاظه عمل الحسامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببوس و غمزه همچون نوش و نیشی </p></div>
<div class="m2"><p>بجعد و طره همچون صبح و شامی </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر رویم چو زر پخته کردی </p></div>
<div class="m2"><p>سزد ، زیرا ببر چون سیم خامی </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یمللنی فراقک کل میل </p></div>
<div class="m2"><p>علی لهب شدید الاضطرامی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابیت و فی جفونی ماء حزن </p></div>
<div class="m2"><p>و ما بینالضلوع نطی غرامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو ، ای موی نگار ، از روی معنی </p></div>
<div class="m2"><p>همه مشکی ، اگر چه زلف نامی </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>معطر کرده عالم را نسیمت </p></div>
<div class="m2"><p>مگر خلق شهنشاه انامی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کریم فی السخا کفیه بحر</p></div>
<div class="m2"><p>خضیم زاخر الامواج طامی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شعارنهاه للایام زین </p></div>
<div class="m2"><p>و حصن علاء للاسلام حامی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برای او جهان ملک روشن </p></div>
<div class="m2"><p>بسعی او درخت عدل نامی </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جناب فرخ او زایران را </p></div>
<div class="m2"><p>شده چون خطهٔ کعبه گرامی </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صفاءالبشر من لعناه بادا </p></div>
<div class="m2"><p>و غیثالبر من غیاه حامی </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>و منه الملک محمل النواحی </p></div>
<div class="m2"><p>و منه الشرع مرعی الذمامی </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خداوندا ، تویی کزداد و دانش </p></div>
<div class="m2"><p>امور دولت و دین را قوامی </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان را آسمان افتخاری </p></div>
<div class="m2"><p>هدی را آفتاب احترامی </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولیک فی سعود و ابتهاج </p></div>
<div class="m2"><p>و خصمک فی عناء و اغتمامی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فهذا ساحب ذیل الامانی</p></div>
<div class="m2"><p>و هذا شارب الکاس الحمامی </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه همچون عزم تو شمشیر هندی </p></div>
<div class="m2"><p>نه همچون رای تو شعر ای شامی </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ظفر با تو خرامد ، هر کجا تو </p></div>
<div class="m2"><p>ز بهر نصرة ملت خرامی </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فدیتک قد عمرت برغم دهر </p></div>
<div class="m2"><p>بناء مفاخر بعد انهدامی </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فمنک ملکت ناصیة المباعی </p></div>
<div class="m2"><p>ومنک بلغت قاصیةالمرامی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا پیرایهٔ امنی و یمنی </p></div>
<div class="m2"><p>مرا سرمایهٔ نامی و کامی </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهان پر بند و دام حادثانست </p></div>
<div class="m2"><p>مرا اصل امان زان بند و دامی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بقیت منعما و حماک کهف</p></div>
<div class="m2"><p>یلوذ به جماهیر الانامی </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>و قدرک راسخ البیان راس </p></div>
<div class="m2"><p>و مجدک شامخ الار کان سامی</p></div></div>