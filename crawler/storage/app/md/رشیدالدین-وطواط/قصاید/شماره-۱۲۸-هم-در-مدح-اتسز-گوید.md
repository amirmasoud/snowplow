---
title: >-
    شمارهٔ ۱۲۸ - هم در مدح اتسز گوید
---
# شمارهٔ ۱۲۸ - هم در مدح اتسز گوید

<div class="b" id="bn1"><div class="m1"><p>ای ز حلم تو ساکنی در خاک </p></div>
<div class="m2"><p>گام ننهاده چون تویی در خاک </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست عزم ترا مقابل باد </p></div>
<div class="m2"><p>نیست حزم ترا برابر خاک </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکشد از هوای تو سر چرخ </p></div>
<div class="m2"><p>نزند با وقار تو بر خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا علم تو ، محقر بحر</p></div>
<div class="m2"><p>هر کجا حلم تو ، مزور خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشته بر فرق اختران فلک </p></div>
<div class="m2"><p>از جناب تو همچو افسر خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شده در دست طالبان شرف </p></div>
<div class="m2"><p>در رکاب تو همچو عنبر خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پی جشن نیک خواه ترا </p></div>
<div class="m2"><p>کند از شکل لاله ساغر خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی قمع بدسگال ترا </p></div>
<div class="m2"><p>کشد از برگ بید خنجر خاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست از فرح دولت قدمت</p></div>
<div class="m2"><p>مایهٔ یاسمین و عبهر خاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست از بهر عدت کرمت </p></div>
<div class="m2"><p>معدن صدهزار گوهر خاک </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای ز بهر قرار دین رسول</p></div>
<div class="m2"><p>خیلت افگنده زلزله در خاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه بر کوه کرده بالین سنگ </p></div>
<div class="m2"><p>گاه در دشت کرده بستر خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از غبار سپاهت اغبر چرخ</p></div>
<div class="m2"><p>وز ضراب حسامت احمر خاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خشک ناکرده مرکبان تو خوی</p></div>
<div class="m2"><p>کردی از خون طاغیان تر خاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن زمان ، لا اله الا الله ! </p></div>
<div class="m2"><p>که شد از تیغ تو معصفر خاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گاه در حمله تو حیران باد </p></div>
<div class="m2"><p>گاه از وقفهٔ تو مضطر خاک </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از سنانها و تیغهای یلان</p></div>
<div class="m2"><p>شد چو روی فلک پر اختر خاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در بر خویشتن کشیده بطبع</p></div>
<div class="m2"><p>بدسگال ترا چو مادر خاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رزمگه گشته احمر و از خون </p></div>
<div class="m2"><p>موج زن همچو بحر اخضر خاک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون ندیدش خصایص پسری</p></div>
<div class="m2"><p>کرد پنهانش همچو دختر خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای ز نشر روایح فتحت </p></div>
<div class="m2"><p>گشته چون غالیه معطر خاک </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وی ز گنج مدایع سعیت </p></div>
<div class="m2"><p>یافته صدهزار زیور خاک </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همپو گردون ز چشمهٔ خورشید </p></div>
<div class="m2"><p>شده ز اقدام تو منور خاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از حسام چو آتش و آیت </p></div>
<div class="m2"><p>کرده دشمن چو باد بر سر خاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تویی آنکس ، که از نوال تو یافت </p></div>
<div class="m2"><p>مدد مایهای کوثر خاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از برای دعا و ذکر تو گشت </p></div>
<div class="m2"><p>جای محراب و جای منبر خاک </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا بود عنصری مصفا آب </p></div>
<div class="m2"><p>تا بود جوهری مکدر خاک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باد از بهر زیور ملکت </p></div>
<div class="m2"><p>جای در آب و معدن زر خاک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همچو اسرار دشمنان ترا </p></div>
<div class="m2"><p>در دل خویش کرده مضمر خاک</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از علمهات دیده رتبت چرخ</p></div>
<div class="m2"><p>وز قدمهات برده مفخر خاک</p></div></div>