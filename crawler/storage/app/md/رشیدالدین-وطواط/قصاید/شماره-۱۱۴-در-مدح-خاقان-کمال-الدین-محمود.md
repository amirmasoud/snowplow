---
title: >-
    شمارهٔ ۱۱۴ - در مدح خاقان کمال الدین محمود
---
# شمارهٔ ۱۱۴ - در مدح خاقان کمال الدین محمود

<div class="b" id="bn1"><div class="m1"><p>در هجر روی و لعل تو،ای لعبت طراز </p></div>
<div class="m2"><p>بر روی زرد کرده ام از خون دل تراز </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناکامم از تو ، ور چه برآوردمت بکام </p></div>
<div class="m2"><p>رنجورم از تو ، ور چه بپروردمت بناز </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستم ز حسرت بر چون سیم خام تو </p></div>
<div class="m2"><p>چونان که زر پخته بود در دهان گاز </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز آرزوی آن لب چون انگبین تو </p></div>
<div class="m2"><p>چون موم مانده ام ز تف سینه در گداز </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را نیاز روی تو بی آبروی کرد </p></div>
<div class="m2"><p>کس را مباد نیز بروی بتان نیاز </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر من فراز گشت در وصل تو و لیک </p></div>
<div class="m2"><p>درهای مکرمات خداوند هست باز </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاقان کمال دولت ، آن خسروی که اوست </p></div>
<div class="m2"><p>چون شمس نور گستر و چون چرخ سر فراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محمود ، آسمان محامد ، که در کرم</p></div>
<div class="m2"><p>محض حقیقتست و جزو صورت مجاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از روی او مواکب نصرة در ابتهاج </p></div>
<div class="m2"><p>وز رأی او مناکب دولت در اهتزاز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایام را هدایت او بوده راهبر</p></div>
<div class="m2"><p>اسلام را عنایت او گشته کار ساز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک ضربت از حسامش و یک لشکر از عدو</p></div>
<div class="m2"><p>یک تاختن ز بیژن و یک بیشه از گراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خسروی ، که نیست در آفاق مثل تو </p></div>
<div class="m2"><p>گردی عدو گداز و جوادی ولی نواز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندر حریم عدل تو آهو قرین شیر</p></div>
<div class="m2"><p>وندر جوار امن تو تیهو قرین باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از ورطهٔ خلاف تو گیتی بر اجتناب</p></div>
<div class="m2"><p>وز حملهٔ نهیب تو گردون در احتزاز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاها، درین میانه گهی چند بوده ام </p></div>
<div class="m2"><p>جفت بلا و رنج و قرین عنا و آز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی حیله مانده در کف ایام حیله گر</p></div>
<div class="m2"><p>چون مهره گشته در کف گردون مهره باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کوتاه کرد بر تو ناگه زدامنم</p></div>
<div class="m2"><p>دستی ، که بود چادثهٔ چرخ را دراز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا عهدها نباشد بی نذر و بی یمین </p></div>
<div class="m2"><p>تا عقدها نباشد بی مهر و بی جهاز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جز رأی نشر سروری و صفدری مپوی</p></div>
<div class="m2"><p>جز سوی کسب محمدت و مفخرت متاز</p></div></div>