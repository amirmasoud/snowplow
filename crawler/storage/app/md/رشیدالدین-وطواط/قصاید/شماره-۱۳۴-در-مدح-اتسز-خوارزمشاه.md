---
title: >-
    شمارهٔ ۱۳۴ - در مدح اتسز خوارزمشاه
---
# شمارهٔ ۱۳۴ - در مدح اتسز خوارزمشاه

<div class="b" id="bn1"><div class="m1"><p>ای ز نعل مرکبانت صحن عالم پر هلال </p></div>
<div class="m2"><p>آفتابی در معالی ، آسمانی در جلال </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ تو روز وغا آباد کرده گنج فتح</p></div>
<div class="m2"><p>وسعت تو روز سخا تاراج کرده گنج مال </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست از مالیدن کفار تیغت را ستم </p></div>
<div class="m2"><p>نیست از بخشیدن اموال دستت را ملال </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیشوایی گشت تیغت ، عون افلاکش تیغ </p></div>
<div class="m2"><p>که خدایی گشت جودت، خلق آفاقش عیال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از برای عدت جودت وجود سیم و زر</p></div>
<div class="m2"><p>وز برای مدت عمرت دوام ماه و سال </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست بر وفق تو اجرام فلک را اتفاق </p></div>
<div class="m2"><p>هست از عدل تو احکام جهان را اعتدال </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ضمیر روشن تو اختران یابند نور </p></div>
<div class="m2"><p>وز لقای فرخ تو خسروان گیرند فال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیش بدگوی تو تیره همچو ایام فراق </p></div>
<div class="m2"><p>عمر بدخواه تو کوته همچو ایام وصال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هراس تو نهان کردند ماران دست و پا </p></div>
<div class="m2"><p>در پناه تو برآوردند موران پر و بال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیدهٔ تقوی ز نور عدل تو دارد بصر </p></div>
<div class="m2"><p>چهرهٔ معنی ز حسن لفظ تو گیرد جمال </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد بحار از جود تو بی لؤلؤ ، ای ابر سخا</p></div>
<div class="m2"><p>شد جبال از بر تو بی گوهر ، ای شمس نوال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو ابری چون شد از جود تو بی لؤلؤ بحار ؟</p></div>
<div class="m2"><p>ور تو شمسی چون شد از بر تو بی گوهر جبال؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست از اولاد آدم چون تو مرضی السیر</p></div>
<div class="m2"><p>نیست از ابنای عالم چون تو محمود الخصال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو نهال دولتی در بوستان مملکت</p></div>
<div class="m2"><p>اینت فرخ بوستان و اینت فرخنده نهال!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دولت فرخندهٔ تو فارغست از انقلاب </p></div>
<div class="m2"><p>حشمت پایندهٔ تو ایمنست از انتقال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کمالی را بود خوف زوال اندر عقب </p></div>
<div class="m2"><p>هست ملک را کمالی خالی از خوف زوال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیغ تو در هر دماغی جای سازد چون هوس</p></div>
<div class="m2"><p>خیل تو در هر مضیقی راه یابد چون خیال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رمح تو در عیبه های جوشن گردان شوب</p></div>
<div class="m2"><p>سخت آسان ، همچو اندر فرجهٔ دندان خلال </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهریارا ، بابل و خوارزم جای سحر شد</p></div>
<div class="m2"><p>سحر این عین رشاد و سحر آن عین ضلال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست بر بابل تفاخرها بسی خوارزم را</p></div>
<div class="m2"><p>کان فاخرها نباشد نزد دانایان محال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خطهٔ بابل اگر گشتست پر سحر حرام</p></div>
<div class="m2"><p>شد ز شعرم خطهٔ خوارزم پر سحر حلال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیست دریا معدن آب زلال و شد کنون </p></div>
<div class="m2"><p>طبع من دریا ، و لیکن معدن آب زلال </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نکتهای تو ز نثر من جدا کرد اضطراب </p></div>
<div class="m2"><p>نقدهای تو ز نظم برون برد اختلال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بود جایز دو کوکب را بیک جا اقتران </p></div>
<div class="m2"><p>تا شود حاصل دو اختر را بیک جا اتصال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کوکب احباب تو بادا همیشه در شرف</p></div>
<div class="m2"><p>و اختر اعدای تو بادا همیشه در وبال </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در جهان عین الکمالست آفت ملک و ملک </p></div>
<div class="m2"><p>باد ملک تو مصون از آفت عین الکمال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو نشسته کامران در پیشگاه مملکت </p></div>
<div class="m2"><p>و ایستاده خسروان پیش تو در صف نعال</p></div></div>