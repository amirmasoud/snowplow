---
title: >-
    شمارهٔ ۱۸۱ - د رمدح علاء الدین ابوبکر بن قماج
---
# شمارهٔ ۱۸۱ - د رمدح علاء الدین ابوبکر بن قماج

<div class="b" id="bn1"><div class="m1"><p>ای بتو ایام افتخار گرفته</p></div>
<div class="m2"><p>دامن تو دولت استوار گرفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق بسداد تو اهتزاز نموده</p></div>
<div class="m2"><p>دین بر شاد تو افتخار گرفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نفحات نسیم عدل تو گیتی </p></div>
<div class="m2"><p>در مه دی نزهت بهار گرفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوانده سپهرت علاء دین و زنامت </p></div>
<div class="m2"><p>روی معالی همه نگار گرفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معتبرانی ، که سرکشان جهانند</p></div>
<div class="m2"><p>از سر شمشیرت اعتبار گرفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صولت سهم تو صد مصاف شکسته</p></div>
<div class="m2"><p>هیبت جاه تو صد حصار گرفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آتش تیغت ، که آب شرع بیفزود</p></div>
<div class="m2"><p>طارم ازرق همه شرار گرفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناظر عزم تو و طلیعهٔ حزمت</p></div>
<div class="m2"><p>راه بر احداث روزگار گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخت در اقبال تو مقام گزیده </p></div>
<div class="m2"><p>چرخ بفرمان تو مدار گرفته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با دل و جان مخالفان جلالت</p></div>
<div class="m2"><p>لشکر اندوه کار زار گرفته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خصم تو از گلبن امان و امانی</p></div>
<div class="m2"><p>گل بتو کرده رها و خار گرفته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در صف هیجا ز مرکبان سپاهت </p></div>
<div class="m2"><p>چهرهٔ عیش عدو غبار گرفته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای چو پیمبر فلک ببوتهٔ هجرت</p></div>
<div class="m2"><p>ذات شریف ترا عیار گرفته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پی آن تا جهان قرار پذیرد</p></div>
<div class="m2"><p>در وطن مشرکا قرار گرفته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شخص ترا کردگار از بد کفار</p></div>
<div class="m2"><p>در کنف صدق زینهار گرفته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باز خرامیده سوی قبهٔ اسلام</p></div>
<div class="m2"><p>وز تو هدی قدر و اقتدار گرفته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رایت عالیت را ، که آیت یمنست</p></div>
<div class="m2"><p>فتح بصد مهر در کنار گرفته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شرع بجاه تو فرو فخر فزوده</p></div>
<div class="m2"><p>ملک بعدل تو کار و بار گرفته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از قبل خدمت رکاب رفیعت </p></div>
<div class="m2"><p>عرصهٔ عالم همه سوار گرفته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چرخ بدرگاه تو از آنچه که کردست</p></div>
<div class="m2"><p>آمده و راه اعتذار گرفته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا که بود همچو قعر بحر بشبها</p></div>
<div class="m2"><p>سطح فلک در شاهوار گرفته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باد معالیت بی شمار و افاضل </p></div>
<div class="m2"><p>از تو ایادی بی شمار گرفته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صدر تو معمور باد و هر چه صدورند </p></div>
<div class="m2"><p>خدمت صدر تو اختیار گرفته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حافظ تو کردگار و پیشه حسامت</p></div>
<div class="m2"><p>تقویت شرع کردگار گرفته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پنجهٔ گشاده بکینه شیر حوادث</p></div>
<div class="m2"><p>جان عدوی ترا شکار گرفته</p></div></div>