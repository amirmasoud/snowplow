---
title: >-
    شمارهٔ ۱۷۰ - در مدح ملک اتسز
---
# شمارهٔ ۱۷۰ - در مدح ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>مقتدای همه زمان و زمین </p></div>
<div class="m2"><p>شاه غازی، علاء دولت و دین </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاه بوالمظفر اتسز، آنک</p></div>
<div class="m2"><p>هست در حکم او زمان و زمین </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه اجرام هفت گردون را </p></div>
<div class="m2"><p>خدمت اوست پیشه و آیین </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه شاهان هفت کشور را </p></div>
<div class="m2"><p>حضرت او بستر و بالین </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همتش را ستاره زیر عنان </p></div>
<div class="m2"><p>حشمتش را زمانه زیر نگین </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ز آثار عدل شامل تو </p></div>
<div class="m2"><p>جای عصفور دیدهٔ شاهین </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنه از هیبت تو گشته نزار </p></div>
<div class="m2"><p>جود از نعمت تو گشته سمین </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خوی از غیرت تو ابر بهار </p></div>
<div class="m2"><p>در تب از هیبت تو شیر عرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جاه تو با ستاره کرده قران</p></div>
<div class="m2"><p>ملک تو با زمانه گشته قرین </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتهای تو در مجالس بزم </p></div>
<div class="m2"><p>مدد روح را چو ماه معین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کردهای تو در مواقف رزم </p></div>
<div class="m2"><p>صدف فتح راست در ثمین </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهمه رزق ها کف تو کفیل </p></div>
<div class="m2"><p>بهمه خیرها دل تو ضمین </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفدران را ز خدمت تو یسار </p></div>
<div class="m2"><p>خسروان را بنعمت تو یمین </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از قبول تو مستقر کرده </p></div>
<div class="m2"><p>نیکخواه تو در مقام امین </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز نهیب توی قرار شده </p></div>
<div class="m2"><p>بدسگال تو در قرار مکین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست رایات دولت تو بلند </p></div>
<div class="m2"><p>هست آیات حشمت تو مبین </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بندهٔ امر تو صغار و کبار</p></div>
<div class="m2"><p>سخرهٔ حکم تو شهور و سنین </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خیل احداث روزگار چو باد </p></div>
<div class="m2"><p>بر صف دشمنت گشاده کمین </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نور اجرام آسمان بر خاک </p></div>
<div class="m2"><p>از پی خدمتت نهاده جبین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جور از هیبت تو گشته نزار </p></div>
<div class="m2"><p>جود از نعمت تو گشته سمین </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حضرتت بارگاه میر و وزیر </p></div>
<div class="m2"><p>درگهت پیشگاه خان و تگین </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشته بر شخص حاسدت پیدا </p></div>
<div class="m2"><p>وحشت ذل و ظلمت نفرین </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شده در شأن ناصحت منزل </p></div>
<div class="m2"><p>آیت مجد و سورهٔ تمکین </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عفو تو همچو چشمهٔ حیوان </p></div>
<div class="m2"><p>خشم تو همچو آذر برزین </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای بسا رزمگاه کز هولت </p></div>
<div class="m2"><p>در رحم پیر گشت فرق جنین </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیزه در دست سرکشان کرده </p></div>
<div class="m2"><p>بردن روح مرگ را تلقین </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیشش از عقد چون دم کژدم </p></div>
<div class="m2"><p>نوکش از زهر چون سر تنین </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرم گشته بعرصه گاه فنا </p></div>
<div class="m2"><p>روز بازار خنجر و زوبین </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شخص گردان ببند عجز اسیر </p></div>
<div class="m2"><p>جان مردان بدست مرگ رهین </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو در آن چون خلیل و آتش رزم</p></div>
<div class="m2"><p>گشته بر تو چو سوسن و نسرین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>و آن غلامان تو، که رایت حق </p></div>
<div class="m2"><p>برکشیدند تا بعلیین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پشت کفر از هراسشان پر خم </p></div>
<div class="m2"><p>روی شرک از نهیبشان پر چین </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حافظ عیش مؤمنان گهر مهر </p></div>
<div class="m2"><p>مهلک جیش مشرکان گه کین </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه دیده بهر همچو عنب </p></div>
<div class="m2"><p>همه دل در مصاف همچو تین </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سپهت هر کجا که رو آرد</p></div>
<div class="m2"><p>یمن و یسرست بر یسار و یمین </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بقعهایی گرفته ، سخت مخوف </p></div>
<div class="m2"><p>حصنهایی گشاده ، سخت حصین </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو بتعلیمهای بخت بلند</p></div>
<div class="m2"><p>تو بتدبیرهای رأی رزین </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زود بینی بکام خویش شده </p></div>
<div class="m2"><p>خطهٔ چین و بقعهٔ ماچین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>صله داده خزاین فغور </p></div>
<div class="m2"><p>برده کرده نتایج تکسین </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خسروا ، رفتی و سیاست تو </p></div>
<div class="m2"><p>کرد ابنای شرک را غمگین </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آمدی باز ، فر موکب تو </p></div>
<div class="m2"><p>داد احوال شرع را تزیین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از قدوم تو خطهٔ خوارزم </p></div>
<div class="m2"><p>گشته آراسته چو خلد رین </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای حریم تو مأمن مؤمن </p></div>
<div class="m2"><p>وی جناب تو مسکن مسکین </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنگ در خدمت زدیم که هست </p></div>
<div class="m2"><p>خلق را خدمت تو حبل متین </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شد مکرم ز خاک درگه تو </p></div>
<div class="m2"><p>هر که موجود شد بماء معین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از تو شد حال زشت من نکو </p></div>
<div class="m2"><p>وز تو شد عیش تلخ من شیرین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گاه یابم ز کف تو احسان </p></div>
<div class="m2"><p>گاه بینم ز لطف تو تحسین </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کی بود، کی ؟ عروس طبع مرا </p></div>
<div class="m2"><p>بهتر از مکرمات تو کابین </p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا بعشق و بحسن مشهورند </p></div>
<div class="m2"><p>نام فرهاد و قصهٔ شیرین </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باد در خون عدوی تو غرقه </p></div>
<div class="m2"><p>باد در خاک دشمن تو دفین </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دشمنت را بعاجل و آجل </p></div>
<div class="m2"><p>جای در سجن باد و در سجین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گه می ناز و شادمانی نوش </p></div>
<div class="m2"><p>گه گل عز و کامرانی چین </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در زمانه بنصرت ایزد </p></div>
<div class="m2"><p>این چنین صد هزار فتح ببین</p></div></div>