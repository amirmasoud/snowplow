---
title: >-
    شمارهٔ ۹۴ - در ترجمه از شعر تازی
---
# شمارهٔ ۹۴ - در ترجمه از شعر تازی

<div class="b" id="bn1"><div class="m1"><p>چون ز شعبان گذشت بیست ، مباش </p></div>
<div class="m2"><p>بشب و روز بی شراب دمی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه جام بزرگ خواه ، که خرد </p></div>
<div class="m2"><p>نکند وقت احتمال همی </p></div></div>