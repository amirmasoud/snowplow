---
title: >-
    شمارهٔ ۶۲ - نیز در حق شمس الدین
---
# شمارهٔ ۶۲ - نیز در حق شمس الدین

<div class="b" id="bn1"><div class="m1"><p>ای صدر شرق و غرب خداوند شمس دین</p></div>
<div class="m2"><p>خصم ترا مباد ز انده فراغ دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آثار شکر تو فضلا را غذای روح</p></div>
<div class="m2"><p>انوار مهر تو عقلا را چراغ دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مجلس تو هست مرا رحمت روان </p></div>
<div class="m2"><p>وز رشک بدسگال مرا هست داغ دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من باغبان مدح تو گشتم ، که هر زمان</p></div>
<div class="m2"><p>نو تحفه ای بصدر تو آرم ز باغ دل</p></div></div>