---
title: >-
    شمارهٔ ۷۹ - در مدیحه
---
# شمارهٔ ۷۹ - در مدیحه

<div class="b" id="bn1"><div class="m1"><p>ای همه عاقلان بطوع و بطبع</p></div>
<div class="m2"><p>سوی خاک در تو پوینده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای همه فاضلان بنظم و بنثر</p></div>
<div class="m2"><p>مدحت مجلس تو گوینده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نسیم شمایل تو نیافت </p></div>
<div class="m2"><p>در خوشی هیچ حس بوینده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در روضهٔ معالی تو</p></div>
<div class="m2"><p>جز گل مکرمات روینده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی آمال را بآب نوال </p></div>
<div class="m2"><p>دست افضال تست شوینده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یافتی آنچه جستی از دولت</p></div>
<div class="m2"><p>هست یابنده مرد جوینده</p></div></div>