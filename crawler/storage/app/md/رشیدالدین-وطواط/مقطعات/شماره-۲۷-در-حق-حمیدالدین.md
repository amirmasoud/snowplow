---
title: >-
    شمارهٔ ۲۷ - در حق حمیدالدین
---
# شمارهٔ ۲۷ - در حق حمیدالدین

<div class="b" id="bn1"><div class="m1"><p>با جمال تو ، ای حمیدالدین</p></div>
<div class="m2"><p>رونق ماه و آفتاب نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جهان با مکارم دستت</p></div>
<div class="m2"><p>نام و آوازهٔ سحاب نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش الطاف تو ز غایت لطف</p></div>
<div class="m2"><p>آب را هیچ قدر و آب نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلعت فضل و چهرهٔ دانش</p></div>
<div class="m2"><p>از ضمیر و در حجاب نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو معمار گشته ای، یک گام</p></div>
<div class="m2"><p>در جهان هنر خراب نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تو ما را ، بحق نعمت تو</p></div>
<div class="m2"><p>در دل و دیده صبر و خواب نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا من از تو جدا شدم بخطا</p></div>
<div class="m2"><p>در دلم فکرت صواب نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جامهٔ عیش را تراز برفت</p></div>
<div class="m2"><p>خیمهٔ لهو را طناب نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شخص آمال را حیات بشد</p></div>
<div class="m2"><p>جام لذات را شراب نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در فراق تو چرخ را با من</p></div>
<div class="m2"><p>جز بلفظ جفا خطاب نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چه خواهد کنون تواند گفت</p></div>
<div class="m2"><p>که مرا قدرت جواب نماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی تو در جمله روزگار مرا</p></div>
<div class="m2"><p>هیچ راحت ز هیچ باب نماند</p></div></div>