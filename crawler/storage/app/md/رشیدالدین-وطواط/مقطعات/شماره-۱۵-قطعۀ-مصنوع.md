---
title: >-
    شمارهٔ ۱۵ - قطعۀ مصنوع
---
# شمارهٔ ۱۵ - قطعۀ مصنوع

<div class="b" id="bn1"><div class="m1"><p>بعلم نظیرش نیابی بهمت </p></div>
<div class="m2"><p>بگیتی مثالش نیابی بحکمت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقدم بدانش ، ممیز ببخشش</p></div>
<div class="m2"><p>مظفر بکوشش ، موفر بخدمت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زمزم حلاوت چشاند بسیرت </p></div>
<div class="m2"><p>ز رضوان نشانی نماید بعصمت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دما دم وفاتش رساند سعادت </p></div>
<div class="m2"><p>پیاپی خلافش چشاند مذمت </p></div></div>