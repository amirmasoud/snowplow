---
title: >-
    شمارهٔ ۱۲ - در عشق گوید
---
# شمارهٔ ۱۲ - در عشق گوید

<div class="b" id="bn1"><div class="m1"><p>آه ! از عشق بی کران ، کین عشق </p></div>
<div class="m2"><p>همه رنج دلست و دردسرست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر درد من به عالم رفت </p></div>
<div class="m2"><p>ای دریغا ! که یار بی خبرست </p></div></div>