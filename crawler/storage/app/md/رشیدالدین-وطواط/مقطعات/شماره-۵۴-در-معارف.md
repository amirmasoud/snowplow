---
title: >-
    شمارهٔ ۵۴ - در معارف
---
# شمارهٔ ۵۴ - در معارف

<div class="b" id="bn1"><div class="m1"><p>از خدمت مخلوق باز کش </p></div>
<div class="m2"><p>ای نفس عنا کش ، عنان خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سیر شوی تو ز نان او </p></div>
<div class="m2"><p>سیر آمده باشی ز جان خویش </p></div></div>