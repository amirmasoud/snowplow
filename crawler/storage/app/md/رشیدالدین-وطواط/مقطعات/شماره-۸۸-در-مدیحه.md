---
title: >-
    شمارهٔ ۸۸ - در مدیحه
---
# شمارهٔ ۸۸ - در مدیحه

<div class="b" id="bn1"><div class="m1"><p>من نگویم: به ابر مانندی </p></div>
<div class="m2"><p>که نکو ناید از خردمندی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او همی‌بخشد و همی‌گرید </p></div>
<div class="m2"><p>تو همی‌بخشی و همی‌خندی </p></div></div>