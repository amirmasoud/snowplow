---
title: >-
    شمارهٔ ۹۱ - نیز در معارف
---
# شمارهٔ ۹۱ - نیز در معارف

<div class="b" id="bn1"><div class="m1"><p>خرد نزدیک دولت رفت و گفتا </p></div>
<div class="m2"><p>که : می خواهم که با من یار باشی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوابش داد دولت ، گفت : جایی</p></div>
<div class="m2"><p>که من باشم تو خود ناچار باشی </p></div></div>