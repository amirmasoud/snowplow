---
title: >-
    شمارهٔ ۲۲ - در حق جمال الدین وزیر
---
# شمارهٔ ۲۲ - در حق جمال الدین وزیر

<div class="b" id="bn1"><div class="m1"><p>ای فخر جهان ، جمال دولت </p></div>
<div class="m2"><p>آفاق بتو جمال دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون ، که بروست مطلع سعد </p></div>
<div class="m2"><p>دیدار تو را بفال دارد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر اهل هنر ، که در جهان هست </p></div>
<div class="m2"><p>از سعی تو جاه و مال دارد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای نام گرفته ، مال داده </p></div>
<div class="m2"><p>از حال توبه که حال دارد ؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز نام ، دگر هر آنچه باشد </p></div>
<div class="m2"><p>از مال جهان زوال دارد </p></div></div>