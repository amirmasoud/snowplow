---
title: >-
    شمارهٔ ۳۳ - در حق شمس الدین
---
# شمارهٔ ۳۳ - در حق شمس الدین

<div class="b" id="bn1"><div class="m1"><p>آمدم سوی سرای شمس دین</p></div>
<div class="m2"><p>بازگشتم چون جمال او نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چه خواهم کرد بی او آن سرای؟</p></div>
<div class="m2"><p>تشنه را از ساغر فارغ چه سود؟</p></div></div>