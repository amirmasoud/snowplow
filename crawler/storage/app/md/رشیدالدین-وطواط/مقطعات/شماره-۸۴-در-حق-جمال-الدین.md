---
title: >-
    شمارهٔ ۸۴ - در حق جمال الدین
---
# شمارهٔ ۸۴ - در حق جمال الدین

<div class="b" id="bn1"><div class="m1"><p>ای یگانه جمال دولت و دین </p></div>
<div class="m2"><p>دلم از هجر خویش خستستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی آورده ای بعیش و مرا </p></div>
<div class="m2"><p>پشت بی روی خود شکستستی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان ریاض همچو بهشت </p></div>
<div class="m2"><p>با سمن ساعدان نشستستی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست عشرت گشاده ای و ببند </p></div>
<div class="m2"><p>پای احداث چرخ بستستی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرت خود نیست راحتی ، باری </p></div>
<div class="m2"><p>از گرانی بنده رستستی </p></div></div>