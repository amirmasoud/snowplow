---
title: >-
    شمارهٔ ۴۱ - لغز در نام قطب
---
# شمارهٔ ۴۱ - لغز در نام قطب

<div class="b" id="bn1"><div class="m1"><p>مهی ، کندر سمرقند از لب او </p></div>
<div class="m2"><p>نبات مصر را جان می فزاید </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر طوطی طبع جان فزاید </p></div>
<div class="m2"><p>بشاخ شکر نابش گراید </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشولد مر طبق را بر طریقی </p></div>
<div class="m2"><p>که در تک آنچه باشد بر سر آید </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس آنگه بر سر شاخ فراغت </p></div>
<div class="m2"><p>بهر لحنی ، که خواهی ، می سراید</p></div></div>