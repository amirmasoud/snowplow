---
title: >-
    شمارهٔ ۶۸ - د رحق فخرالدین
---
# شمارهٔ ۶۸ - د رحق فخرالدین

<div class="b" id="bn1"><div class="m1"><p>فخر دین ، اعتقاد من دانی </p></div>
<div class="m2"><p>که همیشه هوای تو جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دورم از مجلست و لیک مقیم</p></div>
<div class="m2"><p>در مجالس دعای تو گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سال و مه ورد مدح تو خوانم </p></div>
<div class="m2"><p>روز و شب راه مهر تو پویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در فراق رکاب فرخ تو</p></div>
<div class="m2"><p>چهره از خون دل همی شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لعل شد بی لقای تو اشکم</p></div>
<div class="m2"><p>زرد شد بی جمال تو رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تو من بنده را نمی جویی</p></div>
<div class="m2"><p>آب حشمت نمانده در جویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در نوایب ز ناله چون نالم </p></div>
<div class="m2"><p>در مصایب ز مویه چون مویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش چوگان حادثات فلک</p></div>
<div class="m2"><p>باز سر گشته مانده چون گویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوی صدر تو گر بمن برسد</p></div>
<div class="m2"><p>دیدهٔ بینا شود بد آن بویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تو تشریف نامه دارم چشم</p></div>
<div class="m2"><p>که چنین کرد لطف تو خویم</p></div></div>