---
title: >-
    شمارهٔ ۳۶ - در مرثیۀ فخرالدین
---
# شمارهٔ ۳۶ - در مرثیۀ فخرالدین

<div class="b" id="bn1"><div class="m1"><p>گزین فخر دین ، آن شجاعی، که چرخ</p></div>
<div class="m2"><p>مرو را بمردی قرینه ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجام رضا همچو مردان مرد</p></div>
<div class="m2"><p>شراب قضا بستد و در کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دار فنا شد بدار بقا</p></div>
<div class="m2"><p>در آن خطه جای اقامت گزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مپنداردا، هیچ کوته نظر</p></div>
<div class="m2"><p>که آن شخص در زیر خاک آرمید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلی ، از جهان فرومایه رفت</p></div>
<div class="m2"><p>و لیکن بفردوس اعلی رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپر سعادت چو روح الامین</p></div>
<div class="m2"><p>بدین سقف زنگارگون بر پرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از عیب هاجان او پاک باد</p></div>
<div class="m2"><p>برون رفت ازین خاکدان پلید</p></div></div>