---
title: >-
    شمارهٔ ۷۴ - در تهنیت وزارت
---
# شمارهٔ ۷۴ - در تهنیت وزارت

<div class="b" id="bn1"><div class="m1"><p>صدرا ، وزارت از تو جمالی گرفت نو </p></div>
<div class="m2"><p>دیوان بمنصب تو جلالی گرفت نو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار ممالک از پس نقصان بی قیاس </p></div>
<div class="m2"><p>از نوک خامهٔ تو کمالی گرفت نو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مفسلی ، که بود در اطراف شرق و غرب</p></div>
<div class="m2"><p>از فیض بخشش تو نوالی گرفت نو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بعد چشم زخم فراوان ، که اوفتاد</p></div>
<div class="m2"><p>خسرو بروزگار تو فالی گرفت نو</p></div></div>