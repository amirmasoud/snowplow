---
title: >-
    شمارهٔ ۹ - در مدح امام حمیدالدین ابوبکر بن عمر محمودی
---
# شمارهٔ ۹ - در مدح امام حمیدالدین ابوبکر بن عمر محمودی

<div class="b" id="bn1"><div class="m1"><p>حمیدالدین ، در انواع محامد </p></div>
<div class="m2"><p>که از مادر نظیر تو نزادست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره آزادگی خلقت نمودست </p></div>
<div class="m2"><p>در فرزانگی طبعت گشادست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی گردون فراز دهر و در دهر </p></div>
<div class="m2"><p>هنر کس را چو تو گردن ندادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشستی تو در اقبال و معالی </p></div>
<div class="m2"><p>بخدمت بر در تو ایستادست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواری در علوم و هر سواری </p></div>
<div class="m2"><p>بمیدان سخن با تو پیادست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواد خط تو بر روی کاغذ </p></div>
<div class="m2"><p>چو مشک سوده بر کافور سادست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل فرزانگان را نظم خوبت </p></div>
<div class="m2"><p>سرور افزای همچون جام بادست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهی از بهر نظمت مدتی شد </p></div>
<div class="m2"><p>که چشم و گوش سوی تو نهادست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی بیند خطابات شریفت </p></div>
<div class="m2"><p>نگویی تا چه معنی اوفتادست ؟</p></div></div>