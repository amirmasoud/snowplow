---
title: >-
    شمارهٔ ۵۰ - در مدح عمر بن عبدالعزیز
---
# شمارهٔ ۵۰ - در مدح عمر بن عبدالعزیز

<div class="b" id="bn1"><div class="m1"><p>دو عمر آورد پیدا روزگار </p></div>
<div class="m2"><p>هر دو را نام پدر عبدالعزیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن عمر را عدل بود آیین و رسم</p></div>
<div class="m2"><p> این عمر را عدل هست و علم نیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن عمر مر ظالمان را کرد قهر</p></div>
<div class="m2"><p>این عمر مر سایلان را داد چیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن عمر تا اندر آن عالم بود</p></div>
<div class="m2"><p>این عمر بادا درین عالم عزیز</p></div></div>