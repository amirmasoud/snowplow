---
title: >-
    شمارهٔ ۸۹ - در حق امیر مسعود
---
# شمارهٔ ۸۹ - در حق امیر مسعود

<div class="b" id="bn1"><div class="m1"><p>ای گرانمایه تربت میمون </p></div>
<div class="m2"><p>ای مکان امیر مسعودی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تازه بادی چو خلد عدن ، که تو </p></div>
<div class="m2"><p>خوابگاه شجاعت و جودی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بقاع زمین نه ای ، که بدو </p></div>
<div class="m2"><p>از ریاض بهشت معدودی </p></div></div>