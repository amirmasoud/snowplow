---
title: >-
    شمارهٔ ۱۷ - در حق شمس الدین ابو الفتح
---
# شمارهٔ ۱۷ - در حق شمس الدین ابو الفتح

<div class="b" id="bn1"><div class="m1"><p>شمس دین ، صورت ، بو الفتح </p></div>
<div class="m2"><p>ای در فضل بر دلت مفتوح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای تو شیر و غابروز مصاف </p></div>
<div class="m2"><p>وی تو ابر سخا بوقت صبوح </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو ایام را هزار شرف </p></div>
<div class="m2"><p>وز تو اسلام را هزار فتوح </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضل را از تو چرخ جموش</p></div>
<div class="m2"><p>نیست باحشمت تو دهر جموح </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشارات تو ز بیدادی </p></div>
<div class="m2"><p>توبه کردست روزگار و نصوح </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دفع توفان جور گردون را </p></div>
<div class="m2"><p>شده درگاه تو سفینهٔ نوح </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاز بختست خاطری خرم </p></div>
<div class="m2"><p>تا ز چرخست سینه ای مجروح </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد در زیر پای نکبت دهر </p></div>
<div class="m2"><p>شخص بدخواه دولتت مطروح</p></div></div>