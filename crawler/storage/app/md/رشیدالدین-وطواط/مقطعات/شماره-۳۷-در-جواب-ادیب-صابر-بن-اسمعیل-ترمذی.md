---
title: >-
    شمارهٔ ۳۷ - در جواب ادیب صابر بن اسمعیل ترمذی
---
# شمارهٔ ۳۷ - در جواب ادیب صابر بن اسمعیل ترمذی

<div class="b" id="bn1"><div class="m1"><p>جواب کریم جمال خراسان</p></div>
<div class="m2"><p>رسید و بدان انس جان می فزاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر خط او اندهی می گریزد</p></div>
<div class="m2"><p>ز هر لفظ او مشکلی می گشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی مهتری کرد در لفظ و معنی</p></div>
<div class="m2"><p>وز او جز چنین مهتری‌ها نیاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا کف کافی او داد راحت</p></div>
<div class="m2"><p>گرش پای میمون نرنجید ، شاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از گنج اقلام او رنجه بودم</p></div>
<div class="m2"><p>مرا رنج اقدام او می نباید</p></div></div>