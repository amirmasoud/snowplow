---
title: >-
    شمارهٔ ۴۰ - در حق حمید الدین
---
# شمارهٔ ۴۰ - در حق حمید الدین

<div class="b" id="bn1"><div class="m1"><p>مجلس سامی حمید الدین </p></div>
<div class="m2"><p>که همه جز بمهر نگراید </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخصال حمید خویش همی </p></div>
<div class="m2"><p>رونق محمدت بیفزاید </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرع را زیور شمایل او </p></div>
<div class="m2"><p>گوش و گردن همی بیاراید </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کف او از کرم نیارامد </p></div>
<div class="m2"><p>دل او از هنر نیاساید </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفعت قدر تو بزیر قدم </p></div>
<div class="m2"><p>آسمان را همی بفرساید </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زادهٔ بحر و کان خجل گردد</p></div>
<div class="m2"><p>زان سخن ، کز ضمیر او زاید </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ بر کینه روز و شب ، دندان </p></div>
<div class="m2"><p>بر بداندیش او همی خاید </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدتی بس مدید شد ، که مرا </p></div>
<div class="m2"><p>هیچ تشریف می نفرماید </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر گزافه چنین روا نبود </p></div>
<div class="m2"><p>آخر این را بهانه ای باید </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دل او اگر غباری هست </p></div>
<div class="m2"><p>من چه دانم ؟ چو باز ننماید </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هاش الله ! اگر ز من چیزی </p></div>
<div class="m2"><p>بر خلاف رضای او آید </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی خطابات او همی رخ من </p></div>
<div class="m2"><p>دیده از خون دل بیالاید </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست جایز که آن چنان مخدوم </p></div>
<div class="m2"><p>بر چنین خادمی نبخشاید </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اوست آن قاضیی که دانش او </p></div>
<div class="m2"><p>کار عالم همی بپیراید </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود نگه کرد بایدش کین حکم </p></div>
<div class="m2"><p>خاصه در شرع مردمی ، شاید ؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا وفای سپهر سود کند </p></div>
<div class="m2"><p>تا جفای زمانه بگزاید </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او بماناد شاد ، کاتش غم </p></div>
<div class="m2"><p>جان خصمش همی بپالاید </p></div></div>