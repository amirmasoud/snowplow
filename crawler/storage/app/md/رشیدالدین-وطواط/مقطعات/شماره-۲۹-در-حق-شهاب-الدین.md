---
title: >-
    شمارهٔ ۲۹ - در حق شهاب الدین
---
# شمارهٔ ۲۹ - در حق شهاب الدین

<div class="b" id="bn1"><div class="m1"><p>جز بنان تو، ای شهاب الدین</p></div>
<div class="m2"><p>مشکلات علوم حل نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه رأی تو کرد در گیتی</p></div>
<div class="m2"><p>قرص خورشید در حمل نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعد از حاسدت خبر نکند</p></div>
<div class="m2"><p>نحس در ناصحت عمل نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستی موزه ای و راهی نیست</p></div>
<div class="m2"><p>عوض موزه هم خلل نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موزه گر نیست، پای تا بدهد</p></div>
<div class="m2"><p>ویل اگر نیست کم ز طل نکند</p></div></div>