---
title: >-
    شمارهٔ ۳۳ - در شکوه
---
# شمارهٔ ۳۳ - در شکوه

<div class="b" id="bn1"><div class="m1"><p>گفتم که : بمدحت تو خورشید شوم </p></div>
<div class="m2"><p>نی آنکه چو گل آیم و چون بید شوم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نومید دلیر باشد و چیره زبان </p></div>
<div class="m2"><p>زنهار ! چنان مکن ، که نومید شوم</p></div></div>