---
title: >-
    شمارهٔ ۱ - در مدح ملک اتسز
---
# شمارهٔ ۱ - در مدح ملک اتسز

<div class="b" id="bn1"><div class="m1"><p>ای همت تو تاخته بر گردون اسب </p></div>
<div class="m2"><p>هر مدح ، که گویند ، ترا باشد حسب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شاه جهانی و جهان جمله تراست </p></div>
<div class="m2"><p>یک نیمه بمیراث و دگر نیمه بکسب</p></div></div>