---
title: >-
    شمارهٔ ۹ - در مدح کمال الدین
---
# شمارهٔ ۹ - در مدح کمال الدین

<div class="b" id="bn1"><div class="m1"><p>عنوان ظفر نام کمال الدینست </p></div>
<div class="m2"><p>مقصود جهان کام کمال الدینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا که یکی صاحب فضلست امروز </p></div>
<div class="m2"><p>در سایهٔ انعام کمال الدینست </p></div></div>