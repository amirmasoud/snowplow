---
title: >-
    شمارهٔ ۱۷ - در شکوه
---
# شمارهٔ ۱۷ - در شکوه

<div class="b" id="bn1"><div class="m1"><p>در محنت ای زمانهٔ بی فریاد </p></div>
<div class="m2"><p>دور از تو چنانم که بداندیش تو باد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نسخه اصلی این بیت نوشته نشده</p></div>
<div class="m2"><p>...........</p></div></div>