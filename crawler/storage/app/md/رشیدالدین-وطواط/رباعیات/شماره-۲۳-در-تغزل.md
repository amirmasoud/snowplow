---
title: >-
    شمارهٔ ۲۳ - در تغزل
---
# شمارهٔ ۲۳ - در تغزل

<div class="b" id="bn1"><div class="m1"><p>می رفت و گلاب از سمنش می بارید </p></div>
<div class="m2"><p>مشک از خط عنبر شکنش می بارید </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گفتهٔ من دوبیتیی در حق خویش </p></div>
<div class="m2"><p>می خواند و شکر از دهنش می بارید </p></div></div>