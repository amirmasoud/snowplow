---
title: >-
    شمارهٔ ۱۲ - در تغزل
---
# شمارهٔ ۱۲ - در تغزل

<div class="b" id="bn1"><div class="m1"><p>چون چرخ همیشه رسم تو طنازیست </p></div>
<div class="m2"><p>کار تو همه فریب و صنعت بازیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس عهد ، که همچو زلف خود بشکستی </p></div>
<div class="m2"><p>در مذهب تو عهد شکستن بازیست </p></div></div>