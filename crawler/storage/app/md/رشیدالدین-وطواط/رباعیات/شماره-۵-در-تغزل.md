---
title: >-
    شمارهٔ ۵ - در تغزل
---
# شمارهٔ ۵ - در تغزل

<div class="b" id="bn1"><div class="m1"><p>با عیب خریدی تو مرا روز نخست </p></div>
<div class="m2"><p>امروز اگر رد کنیم ، عیب از تست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دوست همی در خور خود خواهی جست </p></div>
<div class="m2"><p>پس دست بباید از همه گیتی شست </p></div></div>