---
title: >-
    شمارهٔ ۳ - هم در مدح اتسز گوید
---
# شمارهٔ ۳ - هم در مدح اتسز گوید

<div class="b" id="bn1"><div class="m1"><p>ای برده آتش رخ تو آب روی من </p></div>
<div class="m2"><p>رفته دل از محبت و رفته ز کوی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با روی نیکوی تو نماندست هیچ بد </p></div>
<div class="m2"><p>کز روی نیکوی تو نیامد بروی من </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی از تو هیچ وقت سلامی بنزد من </p></div>
<div class="m2"><p>نی از تو هیچ گونه پیامی بسوی من </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بی تو وز برای تو بر خاسته مقیم </p></div>
<div class="m2"><p>شهری بجستجوی من و گفتگوی من </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبم مبر ، که بارد گر در پناه شاه </p></div>
<div class="m2"><p>خواهد روان شد آب سعادت بجوی من </p></div></div>
<div class="b2" id="bn6"><p>خوارزمشاه ، اتسز غازی ، علاء دین </p>
<p>پشت و پناه ملت تازی ، علاء دین </p></div>
<div class="b" id="bn7"><div class="m1"><p>جانا ، ز مهر مگذر و آهنگ کین مکن </p></div>
<div class="m2"><p>بر جان من ز لشکر هجران کمین مکن </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر فراق آن دهن همچو خاتمت </p></div>
<div class="m2"><p>از گوهر سرشک رخم پر نگین مکن </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو ماه آسمانی و ما را بدست ظلم </p></div>
<div class="m2"><p>در زیر پای هجر چو خاک زمین مکن </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با خصم من مساز و بآتش مرا مسوز </p></div>
<div class="m2"><p>با من رهی ز بهر چنین مکن </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن دل که مدح خسرو صاحب قران دروست</p></div>
<div class="m2"><p>او را تو با جفای زمانه قرین مکن </p></div></div>
<div class="b2" id="bn12"><p>آن خسروی که بر همه اعدا مظفرست </p>
<p>خورشید دین و داد ، ملک بوالمظفرست</p></div>
<div class="b" id="bn13"><div class="m1"><p>شاهی ، کزو ممالک دنیا شرف گرفت </p></div>
<div class="m2"><p>تیرش صمیم سینهٔ اعدا هدف گرفت </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او هست از سلف خلف صدق ، لاجرم </p></div>
<div class="m2"><p>عهد و لوا و مسند و تخت سلف گرفت </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اسلام در جوار امانش پناه یافت </p></div>
<div class="m2"><p>و اقبال در ظلال جلالش کنف گرفت </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>او در دولتست و گرفتست ملک ازو </p></div>
<div class="m2"><p>آن رتبت و جلال که از در صدف گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>افزود جاه لشکر ایمان بعون او</p></div>
<div class="m2"><p>تا او بحرب تیغ یمانی بکف گرفت</p></div></div>
<div class="b2" id="bn18"><p>گیتی همیشه بستهٔ پیمان او بود</p>
<p>چرخ آن کند که موجب فرمان او بود </p></div>
<div class="b" id="bn19"><div class="m1"><p>شاها ، تویی که دولت و دین در کنار تست </p></div>
<div class="m2"><p>اقبال تابع تو و تأیید یار تست </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در هیچ روزگار نبودیت ملک را </p></div>
<div class="m2"><p>این قدر و این جلال که در روزگارست </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قهر عدو بنصرة اسلام شغل تست </p></div>
<div class="m2"><p>کسب ثنا ببخشش اموال کار تست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرخ ارچه مسرعست ، اسیر مضای تست </p></div>
<div class="m2"><p>کوه ارچه ساکنست ، غلام وقار تست </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فخر تبار خویشی و جاوید باد فخر </p></div>
<div class="m2"><p>گر چه تفاخر همه خلق از تبار تست</p></div></div>
<div class="b2" id="bn24"><p>ای رأی پیر تو همه اسلام کرده شاد </p>
<p>بخت جوان موافق آن رأی پیر باد</p></div>
<div class="b" id="bn25"><div class="m1"><p>آن صفدری که نیست همال تو صفدری </p></div>
<div class="m2"><p>در کان چون تو نیودست گوهری </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر نقطه‌ای ز جاه عریض تو عالمی </p></div>
<div class="m2"><p>هر شعله‌ای ز رأی منبر تو اختری </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صدر تو خلد و مدحت من آب کوثرست </p></div>
<div class="m2"><p>مر خلد را گزیر نباشد ز کوثری </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آراسته به مدح ستایشگران بود </p></div>
<div class="m2"><p>هر جای که که هست سریری و افسری </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این دولت مبارک و این بارگاه را </p></div>
<div class="m2"><p>دانی که نیست چاره ز چون من ثناگری</p></div></div>
<div class="b2" id="bn30"><p>ای بس خزانه‌ها که به مداح داده‌اند </p>
<p>تا این خزانه‌های مدایح نهاده‌اند </p></div>
<div class="b" id="bn31"><div class="m1"><p>ای آنکه از کمال تو در روزگار تو </p></div>
<div class="m2"><p>همچون بهشت گشته بلاد و دیار تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>انصاف کی بود که کشد جور روزگار </p></div>
<div class="m2"><p>مداح پایگاه تو در روزگار تو ؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بازی کزو شکار تو مرغی بود حقیر </p></div>
<div class="m2"><p>باشد عزیز در کنف زینهار تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر دست دولت تو من آن باز فرخم</p></div>
<div class="m2"><p>کز من بود ثنای مخلد شکار تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>انصاف آن بود که: مرا همچو دیگران</p></div>
<div class="m2"><p>امنی بود ز جور فلک در جوار تو</p></div></div>
<div class="b2" id="bn36"><p>جورست پیشه گنبد فیروزه فام را </p>
<p>آخر غلام توست، ادب کن غلام را</p></div>
<div class="b" id="bn37"><div class="m1"><p>در دولت تو اسب معالی بتاختم</p></div>
<div class="m2"><p>وز نعمت تو نرد امانی بباختم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در صدرها بمدحت تو رخ فروختم</p></div>
<div class="m2"><p>بر سروران بخدمت تو سر فراختم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو حق من بمکارم شناختی</p></div>
<div class="m2"><p>من حق صدر تو مدایح شناختم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفتم ترا ثنای چو شهد و روان خویش</p></div>
<div class="m2"><p>چون موم اندر آتش فکرت گداختم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خاطر بسوختم بشبان، تا بمدح تو</p></div>
<div class="m2"><p>چندین هزار عقد جواهر بساختم</p></div></div>
<div class="b2" id="bn42"><p>بر جامهٔ ثنای تو کردم من آن تراز </p>
<p>کز حسن آن برشک بود لعبت طراز</p></div>
<div class="b" id="bn43"><div class="m1"><p>قومی، که در تتبع احوال بنده اند</p></div>
<div class="m2"><p>نام وفا ز دفتر مردی فگنده اند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پیوسته در محاسد فضل خادمند</p></div>
<div class="m2"><p>همواره در منازعت کار بنده اند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از هیبت تغیر رأی رفیع تو</p></div>
<div class="m2"><p>من روز و شب بگریه و ایشان بخنده اند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پازهر التفات تو باید همی مرا</p></div>
<div class="m2"><p>کین طایفه بفعل چو زهر گزنده اند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از چاه غم بر آیم، اگر دست گیرم </p></div>
<div class="m2"><p>وین ها در او فتند بچاهی که کنده اند</p></div></div>
<div class="b2" id="bn48"><p>آن کس که مدح خان چو تو پادشاه بود </p>
<p>ترسان ز کید هرکس و ناکس چرا بود؟</p></div>
<div class="b" id="bn49"><div class="m1"><p>شاها، بنام نیک یکی شمع برفروز</p></div>
<div class="m2"><p>پروانه وار نقش بدیها بدان بسوز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مقبل کسا! که مدح و ثنا جست، پیش از انک</p></div>
<div class="m2"><p>عاجز شد از مصایب این عالم عجوز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرچه عطا کند بمکافات یک ثنا</p></div>
<div class="m2"><p>والله گزارده نشود حق او هنوز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون نان روز روز بمادح همی دهند</p></div>
<div class="m2"><p>یابند جاودانه ازو نام دل فروز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دادند زیرکان که: بود بنزد عقل </p></div>
<div class="m2"><p>این نام جاودانه از آن نان روز روز</p></div></div>
<div class="b2" id="bn54"><p>جز سوی نام نیک خردمند ننگرد</p>
<p>نام نکو بماند و این عمر بگذرد</p></div>
<div class="b" id="bn55"><div class="m1"><p>شاها، ز حضرت تو حوادث تو بعید باد</p></div>
<div class="m2"><p>و اوقات تو بیمن چو فرخنده عید باد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در تیغ تست باس شدید وز خشم تو</p></div>
<div class="m2"><p>بر خصم دولت تو عذاب شدید باد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چون اوج چرخ گوشهٔ قصرت رفیع گشت </p></div>
<div class="m2"><p>تا روز حشر مدت عمرت مدید باد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از دست فتنه در صف بازار اضطراب </p></div>
<div class="m2"><p>ارواح دشمنان تو در من یزید باد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ای گشته کار کرد تو عنوان مملکت</p></div>
<div class="m2"><p>عنوان مدح های تو شعر رشید باد</p></div></div>