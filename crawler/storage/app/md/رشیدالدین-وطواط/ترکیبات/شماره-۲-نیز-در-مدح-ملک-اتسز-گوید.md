---
title: >-
    شمارهٔ ۲ - نیز در مدح ملک اتسز گوید
---
# شمارهٔ ۲ - نیز در مدح ملک اتسز گوید

<div class="b" id="bn1"><div class="m1"><p>ملک بهار گشت مقرر بنام گل </p></div>
<div class="m2"><p>ناکام شد ولایت بستان بکام گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل خطیب وار بر اطراف شاخها</p></div>
<div class="m2"><p>بر خواند خطبهٔ ملکانه بنام گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دام گل گرفت حذر زاغ حیله گر</p></div>
<div class="m2"><p>وندر فتاد بلبل مسکین بدام گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانند خلد گشت ز آثار گل جهان </p></div>
<div class="m2"><p>گل را چنین بود اثر ، ای من غلام گل </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد صبا ،که نایب اخلاق خسروست</p></div>
<div class="m2"><p>هر صبح دم بباده رساند پیام گل</p></div></div>
<div class="b2" id="bn6"><p>ای رشک گل ، بوقت گل آماده دار جام</p>
<p>وی دولت چو باده ، پر از باده دار جام</p></div>
<div class="b" id="bn7"><div class="m1"><p>اطراف بوستان ز گل آرایشی گرفت</p></div>
<div class="m2"><p>آن کم شده طراوتش افزایشی گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آرایشی نبود ببستان درون ، ولی </p></div>
<div class="m2"><p>از مقدم سپاه گل آرایشی گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الحان جان فزای بر آورد عندلیب</p></div>
<div class="m2"><p>زاغ از نفیر بی مزه آسایشی گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر باغ و راغ دیدهٔ ابر گهر فشان </p></div>
<div class="m2"><p>چون دیدهٔ عنا زده پالایشی گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز لاله کوهسار چنان شد که گوییا</p></div>
<div class="m2"><p>از خون گشتگان شه آلایشی گرفت</p></div></div>
<div class="b2" id="bn12"><p>باغست آن ، ندانم ، یا بزم خسروست؟</p>
<p>راغست آن ، ندانم ، یا رزم خسروست؟</p></div>
<div class="b" id="bn13"><div class="m1"><p>بار دگر هوای علم فتح باب زد </p></div>
<div class="m2"><p>وز ابر پرده پیش رخ آفتاب زد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باد صبا درید ببستان حجاب گل </p></div>
<div class="m2"><p>تا ابر پیش چهرهٔ انجم حجاب زد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گل روی چون نگار بنامحرمان نمود</p></div>
<div class="m2"><p>بلبل ز رشک عربدهٔ احتساب زد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باد از نسیم در ره صحرا عبیر بیخت</p></div>
<div class="m2"><p>ابزار سرشک برخ ز لاله گلاب زد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در باغ شهریار ، بهنگام نو بهار </p></div>
<div class="m2"><p>خرم کسی که دست بجام شراب زد ! </p></div></div>
<div class="b2" id="bn18"><p>خسرو علاء دولت ، خورشید روزگار</p>
<p>آن کیقباد عالم و جمشید روزگار</p></div>
<div class="b" id="bn19"><div class="m1"><p>پیرایهٔ لباس معانی بیان اوست</p></div>
<div class="m2"><p>سرمایهٔ اساس ایادی بنان اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملت سپهر و طلعت او آفتاب اوست</p></div>
<div class="m2"><p>دولت جهان و همت او آسمان اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرخ ، ارچه گردنست ، مطیع زمام اوست</p></div>
<div class="m2"><p>مهر ، ارچه توسنست، اسیر عنان اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از نکبت حوادث ایام ایمنست </p></div>
<div class="m2"><p>آن کس که در حمایت حفظ و امان اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دانندهٔ عجایب ایام رأی اوست</p></div>
<div class="m2"><p>بینندهٔ سرایر آفاق جان اوست</p></div></div>
<div class="b2" id="bn24"><p>شاهی ، کزو لوای شریعت مظفرست</p>
<p>فخر ملوک ، نصرت دین ، بوالمظفرست</p></div>
<div class="b" id="bn25"><div class="m1"><p>ای شاه ، در فنون معالی ممیزی</p></div>
<div class="m2"><p>انواع فضل را سبب و اصل و حیزی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هنگام نطق صاحب الفاظ معجبی </p></div>
<div class="m2"><p>هنگام حرب حامل اسباب معجزی </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو آن ممیزی ، که بعهد وجود تو</p></div>
<div class="m2"><p>معدوم گشت قاعدهٔ نا ممیزی </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عالم ز تست عاجز و من بی عنایت </p></div>
<div class="m2"><p>مقهور عالمی شده ام ، اینت عاجزی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وین عجز صعب تر که: بباید گذاشتن</p></div>
<div class="m2"><p>بی کام دل ستانهٔ والای اتسزی</p></div></div>
<div class="b2" id="bn30"><p>ای برده شور صولت تو اقتدار چرخ</p>
<p>بر موجب مراد تو بادا مدار چرخ</p></div>
<div class="b" id="bn31"><div class="m1"><p>شاها ، فلک قبول در تو طلب کند</p></div>
<div class="m2"><p>در وصلت تو بکر معانی طرب کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کیوان ، که از نجوم برفعت فزون ترست</p></div>
<div class="m2"><p>چون ارتفاع قدر تو بیند عجب کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در زهر حسن تربیت تو شفا نهد</p></div>
<div class="m2"><p>وز خار عون منفعت تو رطب کند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز آثار دودمان تو آرد همه دلیل </p></div>
<div class="m2"><p>آن کو بیان فخر عجم در عرب کند </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در عین اعتراض بدود ، هر که از ملوک </p></div>
<div class="m2"><p>جز تو حدیث مکتسب و منتسب کند </p></div></div>
<div class="b2" id="bn36"><p>بر من همه شداید ایام رفته گیر</p>
<p>وز خاک حضرت تو بناکام رفته گیر</p></div>
<div class="b" id="bn37"><div class="m1"><p>ای شاه ، جز دل تو خرد را خزانه نیست </p></div>
<div class="m2"><p>جز درگه تو تیر امل را نشانه نیست </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هست این ستانه مأمن احرار و کی بود </p></div>
<div class="m2"><p>ایمن کسی که در کنف این ستانه نیست ؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از تو مرا جفای زمانه جدا فگند </p></div>
<div class="m2"><p>وین بر تنم نخست جفای زمانه نیست </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر من زمانه بد کند ، ایرا نگشته ام </p></div>
<div class="m2"><p>منقاد اهل او و جزینش بهانه نیست ؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای جاه بی کرانهٔ تو با رهی قرین </p></div>
<div class="m2"><p>چون جاه تو جفای فلک را کرانه نیست </p></div></div>
<div class="b2" id="bn42"><p>آخر زمانهٔ همه پرخاش بگذرد </p>
<p>این ریشخند جملهٔ اوباش بگذرد</p></div>
<div class="b" id="bn43"><div class="m1"><p>شاها ، من این جلالت و آلا گذاشتم </p></div>
<div class="m2"><p>وز عجز این ستانهٔ والا گذاشتم </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وین حضرتی ، که خاک جنابش کشید می </p></div>
<div class="m2"><p>چون سرمه در دو دیدهٔ بینا ، گذاشتم </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زین جا بعجز رفتم و بسیار یادگار </p></div>
<div class="m2"><p>در مدح تو ز طبع خود این جا گذاشتم </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اقبال بی نهایت درگاه فرخت </p></div>
<div class="m2"><p>از جور بی نهایت اعدا گذاشتم </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از حادثات گنبد خضرا ، نه بر مراد </p></div>
<div class="m2"><p>این صدر همچو گنبد خضرا گذاشتم </p></div></div>
<div class="b2" id="bn48"><p>گر آفت اجل نرسد بندهٔ ترا </p>
<p>هم باز بیند این در فرخندهٔ ترا </p></div>
<div class="b" id="bn49"><div class="m1"><p>ای آنکه ذات فرخ تو محض کبریاست </p></div>
<div class="m2"><p>در اعتقاد پاک تو نه کبر و نه ریاست </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گفتم ثنای تو ، که ثنای تو واجبست </p></div>
<div class="m2"><p>وندر زمانه جز تو که مستوجب ثناست ؟</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>واکنون خدای داند کندر ضمیر من </p></div>
<div class="m2"><p>اندوه اجتناب جناب تو ت کجاست ؟</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر چه صواب نیست رحیل از درت و لیک </p></div>
<div class="m2"><p>بودن میان زمرهٔ حساد هم خطاست </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو جاودان بمان ، که صلاح جهان ز تست </p></div>
<div class="m2"><p>ور خود چو من کسی نبود در جهان رواست </p></div></div>
<div class="b2" id="bn54"><p>ما را بحضرت تو نیاز آمدن بود</p>
<p>گر بخت برنگردد و باز آمدن بود</p></div>
<div class="b" id="bn55"><div class="m1"><p>شاها ، کمینه بندهٔ تو روزگار باد </p></div>
<div class="m2"><p>احسانت پیشه باد و ایادت کار باد </p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در کارزار خصم تو رفتست روزگار </p></div>
<div class="m2"><p>از روز خصم تو در کارزار باد </p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از آبدار خنجر آتش نهیب تو </p></div>
<div class="m2"><p>مانند باد دشمن تو خاکسار باد </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ای از سیاست تو بهار عدو خزان </p></div>
<div class="m2"><p>بر خاک فرخ تو خزان چون بهار باد </p></div></div>
<div class="b" id="bn59"><div class="m1"><p>پیوسته سال و ماه بهر کام و هر مراد </p></div>
<div class="m2"><p>یار تو باد و ناصر تو کردگار باد </p></div></div>