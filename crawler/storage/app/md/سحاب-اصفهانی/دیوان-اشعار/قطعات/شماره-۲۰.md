---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>جناب واعظ و مفتی کز آن دو گر گویم</p></div>
<div class="m2"><p>صفات نیک فزون از شماره خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو یافتند که در آتش خصومت هم</p></div>
<div class="m2"><p>زمان زمان دلشان پر شراره خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن مخاصمه و جنگ عاقبت حاصل</p></div>
<div class="m2"><p>مراد چرخ و امید ستاره خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قرار شد که دو دختر به یکدیگر بدهند</p></div>
<div class="m2"><p>به فکر اینکه در این کار چاره خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان آن دو نخواهد شد التیام، عبث</p></div>
<div class="m2"><p>در این میانه... چند پاره خواهد شد</p></div></div>