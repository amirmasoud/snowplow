---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>شود چو شاد زقتلم دل من و تو مترس</p></div>
<div class="m2"><p>ز یک گناه که در ضمن آن بود دو ثواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپرس حال دل من بخون کیست ببین</p></div>
<div class="m2"><p>تو را به پنجه نگار و مرا به چهره خضاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خواب روی تو آید به دیده ی من اگر</p></div>
<div class="m2"><p>جدا ز روی تو در دیده ی من آید خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکرد منع نگه یا نکرد رازم فاش</p></div>
<div class="m2"><p>کدام اثر که به حالم نکرد چشم پر آب؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زروی کار بر افکندیم نقاب آنگه</p></div>
<div class="m2"><p>که بر فکندی از آن روی دلفریب نقاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه دارد عشق تو جای در دل من</p></div>
<div class="m2"><p>چرا که عشق تو گنج است و جای گنج خراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبس که تاب دل بی قرار من بر بود</p></div>
<div class="m2"><p>از این سبب سر زلف تو دارد اینهمه تاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم تو با دل و عشق تو با تنم آن کرد</p></div>
<div class="m2"><p>که با گیاه کند برق و با کتان مهتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین به جام تو لعل مذاب ریزی و من</p></div>
<div class="m2"><p>به یاد لعل تو ریزم زدیده لعل مذاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان جدا ز تو گرید که فرق نتواند</p></div>
<div class="m2"><p>کسی که چشم (سحاب) است یا که چشم سحاب</p></div></div>