---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>فرید روی زمین زین عابدین که رخت</p></div>
<div class="m2"><p>به چرخ عزو شرف کرده مهری و ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایا سپهر مکانی که زهره و کیوان</p></div>
<div class="m2"><p>به درگه تو کند این غلامی، آن داهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایا رفیع جنابی که شاه قدر ترا</p></div>
<div class="m2"><p>سزد که مهر کند تاجی آسمان گاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمیم لطف ترا متصف چو جان بخشی</p></div>
<div class="m2"><p>سموم قهر ترا خاصیت چو جانکاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفته وصف جلال تو شرق را تا غرب</p></div>
<div class="m2"><p>رسیده صیت کمالت ز ماه تا ماهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوامرت به امور است چرخ را آمر</p></div>
<div class="m2"><p>نواهیت ز معانی است زهره را ناهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا چو اصل فراق است عمر دشمن و دوست</p></div>
<div class="m2"><p>ولیکن این به درازی و آن به کوتاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سخای تو کز آن حصول می یابد</p></div>
<div class="m2"><p>هر آنچه می طلبی و هر آنچه میخواهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روایتی بود اکرام معن بی معنی</p></div>
<div class="m2"><p>حکایتی بود اوصاف یحیی افواهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این دو روزه مرا نامه ای فرستادی</p></div>
<div class="m2"><p>چو لطف خویش به جانبخشی و به غم کاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا به ماهی و نارنج یاد کردی و داد</p></div>
<div class="m2"><p>زشفقت توام ارسال این دو آگاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همیشه ماهی و نارنج تا که بخشد نفع</p></div>
<div class="m2"><p>زعلت یرقان چهره چون شود کاهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخ حبیب تو با دابه رنگ چون نارنج</p></div>
<div class="m2"><p>تن عدوی تو غلطان به خاک چون ماهی</p></div></div>