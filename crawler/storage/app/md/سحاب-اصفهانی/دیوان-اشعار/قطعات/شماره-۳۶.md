---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>فلان گنده بینی را نظر کن</p></div>
<div class="m2"><p>که او را غیر خود بینی نبینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زند از میرزائی لاف و در وی</p></div>
<div class="m2"><p>هم استعداد تا بینی نبینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز دیوانه یا مصروعی او را</p></div>
<div class="m2"><p>به چشم عقل اگر بینی نبینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی او که خود کافر مبیناد</p></div>
<div class="m2"><p>اگر بینی به جز بینی نبینی</p></div></div>