---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>به سینه تیرنگاهت همان کند که کند</p></div>
<div class="m2"><p>به روز رزم سنان خدیو عرش جناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدیو عهد محمد حسین خان که مدام</p></div>
<div class="m2"><p>به طوع اوست قلوب و به طوق اوست رقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر خنگی رستم دلی که رخشش را</p></div>
<div class="m2"><p>زآفتاب سزد زین و از هلال رکاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دور او که شود گرگ پاسبان غزال</p></div>
<div class="m2"><p>به عهد او که بود صعوه همنشین عقاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخورده روی کسی سیلیئی به غیر از دف</p></div>
<div class="m2"><p>ندیده گوش کسی مالشی به غیر رباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی به کشور آباد او خراب ندید</p></div>
<div class="m2"><p>به غیر جغد که از این غمش دلی است خراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر دمند به هر دم هزار انفخه ی صور</p></div>
<div class="m2"><p>هنوز بخت بد اندیش او بود در خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وجود خصم ضرور است ورنه شمشیرش</p></div>
<div class="m2"><p>ز کله ای که دهد طعمه ی ذباب و کلاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهی زوصف تو یک نکته و هزار حدیث</p></div>
<div class="m2"><p>خهی ز مدح تو یک شمه و هزار کتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیام جاه تو را فرش ار چه ز اطلس چرخ</p></div>
<div class="m2"><p>ستارگان سپهرش چو میخ های طناب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر زلطف تو یک رشحه بر (سحاب) چکد</p></div>
<div class="m2"><p>هزار چشمهٔ حیوان روان شود ز سراب</p></div></div>