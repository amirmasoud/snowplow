---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>گویند که در قریه ی فین کآب و هوایش</p></div>
<div class="m2"><p>مستحسن اطباع و پسند سلق افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صبح که خورشید بر آرد ز افق سر</p></div>
<div class="m2"><p>تا شام که خور باز به سمت افق افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر لحظه شتابند سوی چشمه زنی چند</p></div>
<div class="m2"><p>کز پرتو روشان به صحاری تتق افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز بیم قرقچی گه آمد شد ایشان</p></div>
<div class="m2"><p>غوغا و فغانی به تمام طرق افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیچاره فقیری که فتد گیر قرقچی</p></div>
<div class="m2"><p>بیچاره تر آن کس که میان قرق افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس صدمه مراین را که به سر می رسد از سنگ</p></div>
<div class="m2"><p>بس قید مر آن را که زپا بر عنق افتد</p></div></div>