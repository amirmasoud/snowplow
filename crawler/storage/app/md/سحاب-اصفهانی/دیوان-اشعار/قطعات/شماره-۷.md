---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>طراز محفل ایجاد میرزا احمد</p></div>
<div class="m2"><p>توئی که ملک جهان خالیت زمانند است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش طبع تو چون قطره بحر عمان است</p></div>
<div class="m2"><p>به جنب علم تو چون کاه، کوه الوند است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان به نشو و نما از نسیم الطفات</p></div>
<div class="m2"><p>چنانکه نشو نبات از نسیم اسفند است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بیم قهر تو گر خصم خود فریدون است</p></div>
<div class="m2"><p>دلش به سینه چو ضحاک در دماوند است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه آگه است زتاثیر خاک مقدم تو</p></div>
<div class="m2"><p>به آب زندگی آن کس که آرزومند است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ی فلان گنهم از ره وفا گفتی</p></div>
<div class="m2"><p>که: خاطرم نه چو پیش از تو شاد و خرسند است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از این خطای ندانسته سخت می ترسم</p></div>
<div class="m2"><p>چنان بدانی کاین بنده سست پیوند است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه دور زعفو است این گنه، از آن</p></div>
<div class="m2"><p>دلم به دام ندامت مدام در بند است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولی به گفته ی شیرین دلکش (آذر)</p></div>
<div class="m2"><p>که در مذاق خردمند خوشتر از قند است:</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«نیم زلطف تو نومید گر خطائی رفت</p></div>
<div class="m2"><p>گنه ز بنده و بخشایش از خداوند است»</p></div></div>