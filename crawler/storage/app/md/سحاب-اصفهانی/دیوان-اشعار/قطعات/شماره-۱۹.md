---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>خان بزرگ خطهٔ شروان اگرچه فخر</p></div>
<div class="m2"><p>بر میر گنجه والی تفلیس می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر پیش او فرود نیارم، چگونه کس</p></div>
<div class="m2"><p>تعظیم این چنین جنبی پیس می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابلیس را ز سجدهٔ آدم چو بود ننگ</p></div>
<div class="m2"><p>آدم چگونه سجده بر ابلیس می‌کند</p></div></div>