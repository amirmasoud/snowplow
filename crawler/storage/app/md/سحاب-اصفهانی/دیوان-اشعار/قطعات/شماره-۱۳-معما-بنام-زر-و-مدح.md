---
title: >-
    شمارهٔ ۱۳ - معما بنام زر و مدح
---
# شمارهٔ ۱۳ - معما بنام زر و مدح

<div class="b" id="bn1"><div class="m1"><p>چیست آن لعبت که زیبا شکل و نیکو منظر است؟</p></div>
<div class="m2"><p>منظرش چون وصل زیبا منظران جان پرور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش نام پادشاهان شوق گنج خسروان</p></div>
<div class="m2"><p>بر جبینش ثبت و اندر خاطر او مضمر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهد آن از بی ثباتی همچو عهد روزگار</p></div>
<div class="m2"><p>طبع او از دون نوازی همچو طبع اختر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک نظر هر کس که بر لوح ضمیرش بنگرد</p></div>
<div class="m2"><p>می شناسد کز چه شهر و از کدامین کشور است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرم آن سیار و تابان همچو جرم کوکب است</p></div>
<div class="m2"><p>پیکر او مستدیر و زرد چون قرص خور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انتقالش از کف نو دولتان این زمان</p></div>
<div class="m2"><p>ممتنع مانند اعراض عرض از جوهر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصل او کمتر به کام حضرت مخدوم ماست</p></div>
<div class="m2"><p>ور بود در بی ثباتی همچو عهد دلبر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفخر گیتی نشاط آن کو به بزم اهل فضل</p></div>
<div class="m2"><p>صحبت او هم نشاط‌افزای و هم جان‌پرور است</p></div></div>