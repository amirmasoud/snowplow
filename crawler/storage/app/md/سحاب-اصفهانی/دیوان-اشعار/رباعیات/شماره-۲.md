---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>هر لحظه دل از یکی است خشنود مرا</p></div>
<div class="m2"><p>گویی بت تازه ایست مقصود مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش فزون نبود دلبر ز یکی</p></div>
<div class="m2"><p>با آنکه هزار دل فزون بود مرا</p></div></div>