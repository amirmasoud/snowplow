---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>صد بار اگر به تیغ کین می کشدم</p></div>
<div class="m2"><p>غم نیست که یار نازنین می کشدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونم ریزد لیک برای دل غیر</p></div>
<div class="m2"><p>بهر دل آن می کشد این می کشدم</p></div></div>