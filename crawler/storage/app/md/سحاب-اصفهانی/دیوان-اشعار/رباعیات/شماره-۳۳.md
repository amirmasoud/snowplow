---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>این خاک مدینه است یا ماه معین؟</p></div>
<div class="m2"><p>این روضه ی مصطفاست یا عرش برین؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این روی من و ضریح شاه مدنی است</p></div>
<div class="m2"><p>یا جرم زحل گشته به خورشید قرین؟</p></div></div>