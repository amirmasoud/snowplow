---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>دلاک به شغل سر تراشی بر من</p></div>
<div class="m2"><p>وز خون سر آغشته بود پیکر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دست طمع کشیده ام از سر خویش</p></div>
<div class="m2"><p>او دست نمی کشد هنوز از سر من</p></div></div>