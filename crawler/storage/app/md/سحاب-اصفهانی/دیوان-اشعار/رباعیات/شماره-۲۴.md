---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>ای از غم تو ناله ی زار همه کس</p></div>
<div class="m2"><p>ای از تو سیاه روزگار همه کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو کار من نه مشکل شد و بس</p></div>
<div class="m2"><p>مشکل شده از عشق تو کار همه کس</p></div></div>