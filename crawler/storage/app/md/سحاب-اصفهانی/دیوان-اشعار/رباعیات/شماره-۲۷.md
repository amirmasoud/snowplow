---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>در بزم تو دورای گل خود روی دو رنگ</p></div>
<div class="m2"><p>تا کی بودم چو غنچه خون در دل تنگ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دیده ی تر گهی بسوزم چون شمع</p></div>
<div class="m2"><p>با قامت خم گهی بنالم چو چنگ</p></div></div>