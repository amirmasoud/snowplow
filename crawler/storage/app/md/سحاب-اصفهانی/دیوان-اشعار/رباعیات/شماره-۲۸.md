---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ای در چمن حسن رخت خرمن گل</p></div>
<div class="m2"><p>خط گر درخت چو سبزه پیرامن گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسوس که در دامن پاک تو رقیب</p></div>
<div class="m2"><p>آویخته همچو خار بر دامن گل</p></div></div>