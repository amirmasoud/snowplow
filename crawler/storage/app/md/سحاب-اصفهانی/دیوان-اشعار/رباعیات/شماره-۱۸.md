---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>میخواست فلک که خوارو زارم بکشد</p></div>
<div class="m2"><p>وز محنت و درد بی شمارم بکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسپرد عنانم به کف سنگ دلی</p></div>
<div class="m2"><p>تا روزو شبی هزار بارم بکشد</p></div></div>