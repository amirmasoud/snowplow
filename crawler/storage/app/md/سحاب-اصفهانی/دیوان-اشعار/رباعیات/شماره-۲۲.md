---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>مانند شمایلت شمایل نشود</p></div>
<div class="m2"><p>دل نیست که بر رخ تو مایل نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سال دگر دلی نماند به جهان</p></div>
<div class="m2"><p>امسال اگر حسن تو زایل نشود</p></div></div>