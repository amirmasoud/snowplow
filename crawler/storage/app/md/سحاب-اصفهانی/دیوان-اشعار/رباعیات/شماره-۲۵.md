---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ای باعث افلاس و تهیدستی خلق</p></div>
<div class="m2"><p>از نهیاتی شکایت مشتی خلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم خلق ملول هم تو دلگیر زما</p></div>
<div class="m2"><p>خلق از غم هستی و تو از هستی خلق</p></div></div>