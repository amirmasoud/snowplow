---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>در بزم رقیب ای زتوام سوز فراق</p></div>
<div class="m2"><p>وصلت بتر از درد غم اندوز فراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس به فراق کرد یاد شب وصل</p></div>
<div class="m2"><p>من در شب وصل آرزو روز فراق</p></div></div>