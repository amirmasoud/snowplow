---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>زین الم در دم مرگم المی بیش نباشد</p></div>
<div class="m2"><p>گر تو را بینم و از عمر دمی بیش نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود قوت رفتار به پای طلب من</p></div>
<div class="m2"><p>ورنه تا منزل جانان قدمی بیش نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهل باشد که بر آری تو تمنای دلی را</p></div>
<div class="m2"><p>کآرزویش ز تو جز لطف کمی بیش نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقطه ی لعل لبت را نبود هیچ وجودی</p></div>
<div class="m2"><p>یا وجودیست که هیچ از عدمی بیش نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد جانیست مرا در کف و دردا که بهایش</p></div>
<div class="m2"><p>بر خوبان جهان از درمی بیش نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی تلافی شب هجر کند روز وصالش</p></div>
<div class="m2"><p>کآن ز عمریست فزون این ز دمی بیش نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با (سحابم) نبود میل ستم گفتی ازین پس</p></div>
<div class="m2"><p>زانکه دانی که به او زین ستمی بیش نباشد</p></div></div>