---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>به زلف او همه دل‌های دل فگاران بین</p></div>
<div class="m2"><p>به بی قرار دگر جای بی قراران بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بزم وصل بود گو مباد گشت چمن</p></div>
<div class="m2"><p>به جای عارض گل روی گلعذاران بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بی وفائی یار و به بی ثباتی عمر</p></div>
<div class="m2"><p>گواه عهد گل و موسم بهاران بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گلشن آی و ز شوق عذار همچو گلت</p></div>
<div class="m2"><p>هزار ناله زهر گوشه از هزاران بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ندیده ای از زلف خویش تیره تری</p></div>
<div class="m2"><p>به تیره روزی ما تیره روزگاران بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وفاست چون گنه ما خوشست عفو اما</p></div>
<div class="m2"><p>به زیر تیغ امید گناه کاران بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جدا ز ماه رخ آن نگار همچو سحاب</p></div>
<div class="m2"><p>روان ز چشم (سحاب) اشک همچو باران بین</p></div></div>