---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>پایان شب هجر تو خواهم پی حاجات</p></div>
<div class="m2"><p>هر گه که بر آرم به فلک دست مناجات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که به خواب تو بیایم شب هجران</p></div>
<div class="m2"><p>در دیده ی من خواب و شب هجر تو هیهات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم جان دهد و هم بستاند لبت آری</p></div>
<div class="m2"><p>از معجز عیسی بودش بیش کرامات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با یاد توام دوش عجب بزم خوشی بود</p></div>
<div class="m2"><p>تا بر سرم امروز چه آید ز مکافات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط سر زده ز آن رخ ولی از طره و مژگان</p></div>
<div class="m2"><p>پیدا بود از حسن تو آثار و علامات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پای زنم تا به لبش بوسه که یک بار</p></div>
<div class="m2"><p>سالک نتواند که کند طی مقامات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سیل سر شکم به فغان اهل زمینند</p></div>
<div class="m2"><p>وز شعله آهم به حذر اهل سماوات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیگانه چنان معتبر امروز که بر من</p></div>
<div class="m2"><p>سهل است که جوید به سگان تو مباهات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر جای که از مصحف خوبی بگشودم</p></div>
<div class="m2"><p>در شان تو دیدم شده نازل همه آیات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرقی که میان من و او هست به محشر</p></div>
<div class="m2"><p>من منفعل از معصیتم شیخ ز طاعات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با آنکه پشیمان شدم از صحبت زاهد</p></div>
<div class="m2"><p>ترسم که زمن نگذری ای پیر خرابات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانسته (سحاب) از بر او دور نگردم</p></div>
<div class="m2"><p>این نیست که چندان نکنم درک کنایات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سلطان جهان فتحعلی شاه که افلاک</p></div>
<div class="m2"><p>از بندگی حضرت او جسته مباهات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ذاتش ز حوادث ابدالدهر مصون باد</p></div>
<div class="m2"><p>از معدلتش دهر مصون از همه آفات</p></div></div>