---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>بگشای پای ما که کمند وفای تو</p></div>
<div class="m2"><p>محکم تر است از همه بندی به پای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتیم از پی دل و دانی به راه عشق</p></div>
<div class="m2"><p>گمراه تر از ما که بود رهنمای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چون تو دلبری برد از کف دل ترا</p></div>
<div class="m2"><p>آگه شوی ز حال دل مبتلای ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مرگ غیر باعث آزردگی تست</p></div>
<div class="m2"><p>شادیم از این که نیست اثر در دعای ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ از میان کشیدی و از دل کشیدی آه</p></div>
<div class="m2"><p>کشتی از آن و دادی ازین خونبهای ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهیم مرگ مدعی و خویش تا شود</p></div>
<div class="m2"><p>هم مطلب تو حاصل و هم مدعای ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیماری رقیب به حالم نکرد سود</p></div>
<div class="m2"><p>سازد مگر وسیله ی دیگر خدای ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیران که در دل که کند جا غم جهان</p></div>
<div class="m2"><p>تا شد بر آستانه ی میخانه جای ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چیست سر اینکه به بیگانگان (سحاب)</p></div>
<div class="m2"><p>زود آشنا شود بت دیر آشنای ما</p></div></div>