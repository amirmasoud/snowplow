---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>به کوی یار مرا جور آسمان نگذاشت</p></div>
<div class="m2"><p>گذاشت اینکه بمانم به کویش آن نگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان ز بیم خزان داشت بلبل و گلچین</p></div>
<div class="m2"><p>گلی بگلشن تا موسم خزان نگذاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه از هلاک من پیر آن جوان نگذشت</p></div>
<div class="m2"><p>که در جهان کسی از پیر و از جوان نگذاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا جفای تو نگذاشت بر در تو و من</p></div>
<div class="m2"><p>ز شرم روی تو گفتم که پاسبان نگذاشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال هیچ غمی هرگزم بدل نگذشت</p></div>
<div class="m2"><p>که روزگار همان غم مرا به جان نگذاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آشیانه نیارم بکنج دام تو یاد</p></div>
<div class="m2"><p>که ذوق او بدلم شوق آشیان نگذاشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زرشک دوستی اش آسمان دو کس با هم</p></div>
<div class="m2"><p>به عهد آن مه بی مهر مهربان نگذاشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش تیر کمان ابرویی (سحاب) دلم</p></div>
<div class="m2"><p>نشانه ایست که تیرم از آن نشان نگذاشت</p></div></div>