---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>مرا طاقت پرو غم اندکی نیست</p></div>
<div class="m2"><p>گر این بسیار آن نیز اندکی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم افزون، صبر کم امید بسیار</p></div>
<div class="m2"><p>به دل در عاشقی دردم یکی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند منع رمیدن شوق دامت</p></div>
<div class="m2"><p>تو پنداری که صید زیرکی نیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی افتاده سر در عرصه ی عشق</p></div>
<div class="m2"><p>ولی زخمی پدید از تارکی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یقینم گشت از بی مهری او</p></div>
<div class="m2"><p>که در مهر (سحاب) او را شکی نیست</p></div></div>