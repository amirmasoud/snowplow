---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>گفتم: به دل شکیب تو حسرت نصیب کو؟</p></div>
<div class="m2"><p>گفتا: به درد عشق نکویان شکیب کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش محفلی که باده ی ناب از سبوبه جام</p></div>
<div class="m2"><p>ریزی بدست خویش و نپرسی رقیب کو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد مرا به ترک تو هر دم دهد فریب</p></div>
<div class="m2"><p>یک جلوه زان شمایل زاهد فریب کو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر ناکسی به کوی وی آمد بگفت کیست؟</p></div>
<div class="m2"><p>هر بیدلی که رفت نگفت آن غریب کو؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای غیر اگر بدوری او سالها زیم</p></div>
<div class="m2"><p>خوشتر از اینکه از تو بپرسم حبیب کو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن خسته قدر لذت درد (سحاب) یافت</p></div>
<div class="m2"><p>کز درد جان سپرد و نگفتا طبیب کو؟</p></div></div>