---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>مپندار نا شادی یار دارم</p></div>
<div class="m2"><p>یک امشب که با او دلی شاد دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سازیم با هم که نه تاب افغان</p></div>
<div class="m2"><p>تو داری نه من تاب بیداد دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتار سرو چمن قمری و من</p></div>
<div class="m2"><p>گرفتاری سرو آزاد دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طمع بین که با مدعی چون ستیزم</p></div>
<div class="m2"><p>از آن بی وفا چشم امداد دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرامم بود گر کنم یاد گلشن</p></div>
<div class="m2"><p>فراغی که در دام صیاد دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهی گرز فریاد داد ضعیفان</p></div>
<div class="m2"><p>کی از ضعف قدرت به فریاد دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زحرمان شیرین لبی آن چنانم</p></div>
<div class="m2"><p>که حسرت بر احوال فرهاد دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به رغم سپهر است و ناشادی او</p></div>
<div class="m2"><p>(سحاب) ار دمی خویش را شاد دارم</p></div></div>