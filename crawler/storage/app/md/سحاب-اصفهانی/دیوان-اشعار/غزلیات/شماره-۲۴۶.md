---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>نیم جانی بود تا جا بود در میخانه‌ام</p></div>
<div class="m2"><p>پر نشد پیمانه تا خالی نشد پیمانه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل دیوانه‌ام دیوانه‌تر دانی که کیست؟</p></div>
<div class="m2"><p>من که دایم در علاج این دل دیوانه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آورد هر چند خواب افسانه اما نایدت</p></div>
<div class="m2"><p>هرگز اندر دیده خواب ار بشنوی افسانه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فریب خال او در دام زلفش دل فتاد</p></div>
<div class="m2"><p>مبتلای دام او دل گشت از این دانه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو گر در گلخنم چون عندلیبم در چمن</p></div>
<div class="m2"><p>بی‌تو گر در گلشنم چون جغد در ویرانه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جنون عشق تا کی منعم ای فرزانگان</p></div>
<div class="m2"><p>این جنون خوش‌تر بود از عقل هر فرزانه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک نگاه آشنای چشم مست او (سحاب)</p></div>
<div class="m2"><p>این چنین بیگانه کرد از خویش و از بیگانه‌ام</p></div></div>