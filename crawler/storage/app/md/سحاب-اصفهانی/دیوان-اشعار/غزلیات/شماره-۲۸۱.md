---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>ز خاک کویش ای دل گاه‌گاهی دیده روشن کن</p></div>
<div class="m2"><p>وگر ز آن هم نه‌ای خرسند یاد از حسرت من کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی آسایش ای مرغ چمن در دام مسکن کن</p></div>
<div class="m2"><p>شوی هر گه که دل تنگ از اسیر یاد گلشن کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آن حالم که در دل داشت شوق دیدنش عمری</p></div>
<div class="m2"><p>کنون ای همدم از بالین من برخیز و شیون کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کیش دوستی منع رقیبان غایتی دارد</p></div>
<div class="m2"><p>که می‌گوید تمام خلق را با خویش دشمن کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رغم من به بزم غیر شب‌ها تا سحر ماندی</p></div>
<div class="m2"><p>به ز غم غیر هم گاهی نگاهی جانب من کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از سنگ جفا ای طایر دل ایمنی خواهی</p></div>
<div class="m2"><p>به هر بامی که بینی عزتی داری نشیمن کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به باغ دوستی هر گل کز آب دیده پروردی</p></div>
<div class="m2"><p>(سحاب) از دیده مانند منش اکنون به دامن کن</p></div></div>