---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>مرا مشکل گشا تا دل نباشد</p></div>
<div class="m2"><p>ز دل کارم چنین مشکل نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شرم قاتلم هر کشته ی را</p></div>
<div class="m2"><p>به محشر شکوه از قاتل نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگوئید آنکه غافل شد زحالم</p></div>
<div class="m2"><p>ز آه غافلم غافل نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزد با سرو من پای تذروی</p></div>
<div class="m2"><p>به گل از سر و پا در گل نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خط حسنش نشد زایل بگو دل</p></div>
<div class="m2"><p>در این اندیشه ی باطل نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>(سحاب) آن را که تخم مهری افشاند</p></div>
<div class="m2"><p>بجز بیحاصلی حاصل نباشد</p></div></div>