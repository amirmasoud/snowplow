---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>دانی چه اثر داشت دعای سحر ما؟</p></div>
<div class="m2"><p>این بود که نگذاشت به عالم اثر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاول قدم از پای فتادیم و ازین پس</p></div>
<div class="m2"><p>تا در ره عشق تو چه آید به سر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز بار غم از شاخ وفای تو نچیدیم</p></div>
<div class="m2"><p>از نخل جفایت چه بود تا ثمر ما؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرگ همه اغیار خوش است ارنه چه حاصل؟</p></div>
<div class="m2"><p>یک خار اگر کم شود از رهگذر ما؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانه ی محروم ز شمعیم و، ز دل خاست</p></div>
<div class="m2"><p>این آتش سوزنده که بینی ز پر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز اینکه شود وقت نگه مانع دیدار</p></div>
<div class="m2"><p>دیگر چه بود خاصیت این چشم تر ما؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیداد گرا خوبه ستم کردی و ترسم</p></div>
<div class="m2"><p>گیرد ز تو داد دل ما دادگر ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دی پیر مغان گفت دو چیز است که بخشد</p></div>
<div class="m2"><p>عمر ابد و آب خضر خاک در ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افغان چو جرس خاست (سحاب) از همه دلها</p></div>
<div class="m2"><p>بنشست به محمل چو مه نو سفر ما</p></div></div>