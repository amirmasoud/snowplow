---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>ای صاف‌تر تو را ز هر آئینه سینه‌ای</p></div>
<div class="m2"><p>آرد مگر در آینه رویت قرینه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایل به رحم شد فلک کینه‌جو به من</p></div>
<div class="m2"><p>با من ولی هنوز تو در فکر کینه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید اگر چه شاه سپهر است لیک هست</p></div>
<div class="m2"><p>در خیل بندگان کمینت کمینه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل هوس چو ما به تو مایل ولی کجا</p></div>
<div class="m2"><p>دارد صفای آینه هر آبگینه‌ای؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم ز اشکم ار شده ویران تو را چه غم</p></div>
<div class="m2"><p>از اینکه ایمن است ز طوفان سفینه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندیشه‌ای ز مفلسیم نیست تا مراست</p></div>
<div class="m2"><p>از گنج عشق در دل ویران دفینه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوبان به جای زر نستانند در نظم</p></div>
<div class="m2"><p>ورنه (سحاب) دارم از این در خزینه‌ای</p></div></div>