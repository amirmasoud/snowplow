---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>این چه دام است ندانم که در آن افتادم؟!</p></div>
<div class="m2"><p>کآشیان و گل و گلشن همه رفت از یادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سوی من نظر زاهدی افتاده مگر</p></div>
<div class="m2"><p>که چنین از نظر پیر مغان افتادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ خواهد کند اجزای وجودم همه خاک</p></div>
<div class="m2"><p>لیک جائی که به کوی تو نیارد بادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر به پیچم اگر از صحبت رندان یارب</p></div>
<div class="m2"><p>مبتلا ساز به هم صحبتی زهادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون پی هجر وصالست و پی وصل فراق</p></div>
<div class="m2"><p>نه ز هجران تو غمگین نه ز وصلت شادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید من قابل فتراک نه اما چه شود</p></div>
<div class="m2"><p>کز یکی زخم رسد بهره ای از صیادم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده بنیاد غم گیتی ام از دل بکند</p></div>
<div class="m2"><p>ورنه یک دم غم گیتی بکند بنیادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانع شاعریم غصهٔ دهر است (سحاب)</p></div>
<div class="m2"><p>ورنه در دهر کسی نیست به استعدادم</p></div></div>