---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>زاهل امتحان کس در میان نیست</p></div>
<div class="m2"><p>که بر دست تو تیغ امتحان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفای او به کام تست از آن دل</p></div>
<div class="m2"><p>به فکر انتقام آسمان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حباب آسا گشودم چشم و دیدم</p></div>
<div class="m2"><p>که چیزی از وجودم در میان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر دستار بر رطل گران داد</p></div>
<div class="m2"><p>که شیخ شهر با ما سر گران نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه باکش از هجوم دادخوهان</p></div>
<div class="m2"><p>که اهل شکوه او را بر زبان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفت آهم عنانش را که رخشش</p></div>
<div class="m2"><p>زتک مانده است و دستی بر عنان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(سحاب) آن را به خون ریزی قرینی</p></div>
<div class="m2"><p>بجز تیغ شه صاحبقران نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدیو بحر و بر فتحعلی شاه</p></div>
<div class="m2"><p>که چرخش جز غبار آستان نیست</p></div></div>