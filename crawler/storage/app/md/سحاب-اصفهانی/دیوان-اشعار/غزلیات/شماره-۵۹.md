---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>فغان زاغ درین باغ با هزار یکی است</p></div>
<div class="m2"><p>گلیش کاین دو یکی نیست از هزار یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی است جور و جفا گر چه پیش غیر دو تاست</p></div>
<div class="m2"><p>دو تاست عشق و هوس گرچه پیش تار یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان بود به دل عندلیب خار ملال</p></div>
<div class="m2"><p>به باغ اگر همه گل صد هزار و خار یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بقصد صید دلم هر کس افگند تیری</p></div>
<div class="m2"><p>چه شد که این همه صیاد را شکار یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لطف او نشود هر دل خراب آباد</p></div>
<div class="m2"><p>چرا که شهر بسی هست و شهر یار یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اکتساب هنر کوش و ترک جهل (سحاب)</p></div>
<div class="m2"><p>اگر چه هر دو بر اهل روزگار یکی است</p></div></div>