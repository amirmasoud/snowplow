---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>میل رفتن گر از آن گوشهٔ بامم باشد</p></div>
<div class="m2"><p>لذت سنگ جفای تو حرامم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتنی باشد و خون ریختنی تا امروز</p></div>
<div class="m2"><p>دل خود کام ترا میل کدامم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بود وصل تو بی مدعی ای ماه تمام</p></div>
<div class="m2"><p>چه شود دگر شبی این عیش تمامم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش سر و قدت همچو قد سرو سهی است</p></div>
<div class="m2"><p>گفت آنهم خجل از طرز خرامم باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آشیانم خوش و صحن چمنم دلکش لیک</p></div>
<div class="m2"><p>نه چو کنج قفس و حلقه ی دامم باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او در اندیشه ی منع نگه گاه بگاه</p></div>
<div class="m2"><p>من مسکین طمع وصل مدامم باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت سر گشته چنین تا به که ئی همچو (سحاب)</p></div>
<div class="m2"><p>گفتمش تا به کف عشق زمامم باشد</p></div></div>