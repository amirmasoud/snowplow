---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>این نه خط است که آرایش حسن تو فزود</p></div>
<div class="m2"><p>سایه ی زلف سیاه تو رخت کرد کبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نه خط است که آرایش او خواست چو دود</p></div>
<div class="m2"><p>دود آه من از آیینهٔ او عکس نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط او سر زد و شادم که به عشاق او را</p></div>
<div class="m2"><p>التفاتیست که هر روز فزون خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صیقل حسن کز آئینه ی جان زنگ زداست</p></div>
<div class="m2"><p>زنگ از آئینه ی خود چون نتوانست زدود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن لب لعل که از کار جهان عقده گشاست</p></div>
<div class="m2"><p>عقده از کار فروبسته ی خود چون نگشود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت آن است که آن طره ی طرار دهد</p></div>
<div class="m2"><p>باز پس نقد دلی را که از عشاق ربود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تخم مهری که در آن باغ فشاندیم (سحاب)</p></div>
<div class="m2"><p>حاصلش راز خجالت نتوانیم درود</p></div></div>