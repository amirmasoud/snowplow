---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>اگر این روزگار و این زمانه است</p></div>
<div class="m2"><p>به عالم قصهٔ راحت فسانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به من دارد گمان نیم‌جانی</p></div>
<div class="m2"><p>به قاصد مژدهٔ وصلم بهانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کنج دام او جایی که هرگز</p></div>
<div class="m2"><p>نمی‌آید به یادم آشیانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خلقی از آن زلف پریشان</p></div>
<div class="m2"><p>پریشان همچو زلف او ز شانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردد صید صیادی دل من</p></div>
<div class="m2"><p>مگر کز زلف و خالش دام و دانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه هر دل واقف از اسرار عشق است</p></div>
<div class="m2"><p>نه این دُر هر صدف را در میانه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بشکافی درون صد صدف را</p></div>
<div class="m2"><p>یکی را در میان دری یگانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبانی نیست تا رازم کند فاش</p></div>
<div class="m2"><p>ولی ز آه درونم صد زبانه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در این ره کی رسد بارم به منزل</p></div>
<div class="m2"><p>که راه عشق راهی بیکرانه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>(سحاب) از چشمهٔ حیوان خضر را</p></div>
<div class="m2"><p>مرا ز آن لب حیات جاودانه است</p></div></div>