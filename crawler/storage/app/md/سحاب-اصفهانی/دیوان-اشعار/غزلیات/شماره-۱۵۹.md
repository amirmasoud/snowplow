---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>عنان او چو گرفتم دل از فغان افتاد</p></div>
<div class="m2"><p>کنون که وقت فغان بود از زبان افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز باده روی کس اینگونه لاله گون نشود</p></div>
<div class="m2"><p>مگر به چشم تو این چشم خونفشان افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مراست تمنای قوت آهی</p></div>
<div class="m2"><p>مگر به فکر مکافات آسمان افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث چشمه ی نوشت به هر کسی که رسید</p></div>
<div class="m2"><p>چو خضر در هوس عمر و جاودان افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهادشست قضا هر خدنگ کین به کمان</p></div>
<div class="m2"><p>نشان سینه ی من بود و بر نشان افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمید خط ز رخ دل ستان و جان آسود</p></div>
<div class="m2"><p>زرشک غیر که عمری بلای جان افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلی ز صدمه ی خار و هجوم زاغ چه باک</p></div>
<div class="m2"><p>مرا که فصل خزان ره به گلستان افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی ز آتش روی تو گه ز آه (سحاب)</p></div>
<div class="m2"><p>چه شعله‌های جهان‌سوز در جهان افتاد</p></div></div>