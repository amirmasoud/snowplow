---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>آن دل آرام که دل آینه دار رخ اوست</p></div>
<div class="m2"><p>دوستش دارم و داند که ورا دارم دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ دل صید شد از تیر نگاهش زیرا</p></div>
<div class="m2"><p>آن کمانکش مژه اش تیر و کمانش ابروست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم مست سیهش رهزن هوش و خرد است</p></div>
<div class="m2"><p>دام دلها شکن طره ی آن مشکین موست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر لب جوی فرحزاست بسی بزم طرب</p></div>
<div class="m2"><p>تا که آن سرو سهی سایه فکن بر لب جوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکنم رو بسوی کعبه و بتخانه و دیر</p></div>
<div class="m2"><p>هر کجا دوست در آنجاست مرا رو سوی اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز دل رفع نگردد ز مداوای طبیب</p></div>
<div class="m2"><p>وصل یار است که بیماری دل را داروست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگزین یار خوش آواز و نکو چهره (سحاب)</p></div>
<div class="m2"><p>ز آنکه قوت دلت آواز خوش و روی نکوست</p></div></div>