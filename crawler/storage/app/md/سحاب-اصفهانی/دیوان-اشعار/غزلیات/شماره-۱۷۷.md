---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>دل بدرد عشق اگر چندی گرفتارت کند</p></div>
<div class="m2"><p>شاید از درد گرفتاران خبردارت کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبری بایست هر جائی تو را تا هر زمان</p></div>
<div class="m2"><p>با خبر از اضطراب رشک اغیارت کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گربیاری تاب در آئینه بنگر روی خویش</p></div>
<div class="m2"><p>تا چو من زین نیکوان، حسن تو بیزارت کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو را این است دل باید نگار پر فنی</p></div>
<div class="m2"><p>تا به هر ساعت هزاران عشوه در کارت کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهگاهی گوش بر افغان بیداران کنی</p></div>
<div class="m2"><p>گر فغان دل شبی از خواب بیدارت کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت پاداش آن جوری که کردی با (سحاب)</p></div>
<div class="m2"><p>سخت میترسم بروز خود گرفتارت کند</p></div></div>