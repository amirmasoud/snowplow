---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>آن کز دل خود ندیده باشی</p></div>
<div class="m2"><p>رحم است اگر شنیده باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که زخود گذشته باشم</p></div>
<div class="m2"><p>وقتی به سرم رسیده باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا رب چه بود در او به جز مهر</p></div>
<div class="m2"><p>حسنی که نیافریده باشی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی طاقتی دل ای دل دوست</p></div>
<div class="m2"><p>هنگام وصال دیده باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وصل ز بیم هجر گاهی</p></div>
<div class="m2"><p>در سینه اگر طپیده باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ساغر هجر زهر حرمان</p></div>
<div class="m2"><p>آن روز به خون کشیده باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رشته ی صبر و ناخن شوق</p></div>
<div class="m2"><p>گه دوخته گه دریده باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش آن که شبی به محفل من</p></div>
<div class="m2"><p>پیمانه ی می کشیده باشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیمان (سحاب) و عهد اغیار</p></div>
<div class="m2"><p>این بسته و آن بریده باشی</p></div></div>