---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ره به این ضعف ار به کوی یار می باید مرا</p></div>
<div class="m2"><p>سیلها از دیده ی خونبار می باید مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایل عشاق صادق نیستند این نیکوان</p></div>
<div class="m2"><p>بعد ازین از عشق پاک انکار می باید مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بخواهم شرم گردد مانع این نظاره ام</p></div>
<div class="m2"><p>دیده ای چون دیده ی اغیار می باید مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نیفتد در جهان آتش ز آب چشم تر</p></div>
<div class="m2"><p>چاره ی این آه آتشبار می باید مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میل ماندن در سر کویش زرشک مدعی</p></div>
<div class="m2"><p>نیست اما قوت رفتار می باید مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ننالد از فغانم خلقی آن بی رحم را</p></div>
<div class="m2"><p>جور کم یا طاقت بسیار می باید مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکوه ی بیهوده چند از جور خار و بانگ زاغ</p></div>
<div class="m2"><p>قوت پرواز از گلزار می باید مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا نباید این قدر نالید از جورش (سحاب)</p></div>
<div class="m2"><p>یا اثر در ناله های زار می باید مرا</p></div></div>