---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>تا پیر می فروش چه آمد سعادتش</p></div>
<div class="m2"><p>کآمد سعادت ابدی در ارادتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را که چون شهید جفا کرد تا بحشر</p></div>
<div class="m2"><p>در حق من قبول نباشد شهادتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما معصیت بقصد ریا خود نمیکنیم</p></div>
<div class="m2"><p>تا شیخ را چه قصد بود از عبادتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون شدم هلاک بیا بر مزار من</p></div>
<div class="m2"><p>بیمار خویش را که نکردی عیادتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با پیر ما کسی که نبودش ارادتی</p></div>
<div class="m2"><p>هر طاعتی که کرد بباید اعادتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کشته ای که روز جزا دامنش گرفت</p></div>
<div class="m2"><p>گردید بی نصیب ز اجر شهادتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندیست کرده ترک دل آزاری (سحاب)</p></div>
<div class="m2"><p>کآزار تازه ای رسد از ترک عادتش</p></div></div>