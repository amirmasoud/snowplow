---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>مدعی بی تو در بلای من است</p></div>
<div class="m2"><p>دور گردون به مدعای من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در افتد بکار من گرهی</p></div>
<div class="m2"><p>تا لب او گره گشای من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه خلق آگهند و من به گمان</p></div>
<div class="m2"><p>کآگه از حال من خدای من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بمقصد رسم که در ره عشق؟</p></div>
<div class="m2"><p>دل گمراه رهنمای من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جفا پیشه با وفاست اگر</p></div>
<div class="m2"><p>بیوفا یار بی وفای من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان درد عشقم افزون باد</p></div>
<div class="m2"><p>که مرا درد من دوای من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دادمش دل منش بدست و، کشم</p></div>
<div class="m2"><p>هر چه از دست او سزای من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه آخر (سحاب) شد کوتاه</p></div>
<div class="m2"><p>از سر کوی دوست پای من است</p></div></div>