---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>سوزد هزار شمع به بیت الحزن مرا</p></div>
<div class="m2"><p>زین داغها که از تو فروزد به تن مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم ز دلبران جفا پیشه داد دل</p></div>
<div class="m2"><p>یک چند دل اگر بگذارد به من مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناصح بدست دل بگذار اختیار من</p></div>
<div class="m2"><p>یا وارهان ز دست دل خویشتن مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیمان ز بد گمانی من هر گهی شکست</p></div>
<div class="m2"><p>افزود مهر آن بت پیمان شکن مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستی که باشدم به گریبان چه می کنی</p></div>
<div class="m2"><p>روزی که باشد از تو به جیب کفن مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شادم که پا ز انجمن این و آن کشید</p></div>
<div class="m2"><p>از بس (سحاب) دید به هر انجمن مرا</p></div></div>