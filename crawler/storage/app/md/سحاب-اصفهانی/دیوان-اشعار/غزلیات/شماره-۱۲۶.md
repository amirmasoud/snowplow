---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>چو زلف بر مه رویش نقاب می‌گردد</p></div>
<div class="m2"><p>نهان به زیر سحاب آفتاب می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان ز شرم تو هر روز خوی فشاند مهر</p></div>
<div class="m2"><p>که شام غرقهٔ دریای آب می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم به کوی تو دایم به جستجوی وفاست</p></div>
<div class="m2"><p>چو تشنه‌ای که به گرد سراب می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حباب بحر سرشک منست چرخ اما</p></div>
<div class="m2"><p>خراب آخر از آن چون حباب می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلی ز قطرهٔ باران شود حباب پدید</p></div>
<div class="m2"><p>ولی ز قطرهٔ دیگر خراب می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمیم طرهٔ او گر رسد به نافه چین</p></div>
<div class="m2"><p>دوباره خون ز حسد و مشک ناب می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شمع روی تو پروانه‌وار مفتون است</p></div>
<div class="m2"><p>که خور به گرد تو با این شتاب می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمان هجر تو تا نگذرد به گردن عمر</p></div>
<div class="m2"><p>خیال زلف بلندت طناب می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لبش هر آب که نوشد (سحاب) خون من است</p></div>
<div class="m2"><p>که پیش لعل وی از شرم آب می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عنب به طارم تاک است کوکبی که از آن</p></div>
<div class="m2"><p>هلال ساغر شاه آفتاب می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گزیده فتحعلی شه که نعل اشهب او</p></div>
<div class="m2"><p>طراز افسر افراسیاب می‌گردد</p></div></div>