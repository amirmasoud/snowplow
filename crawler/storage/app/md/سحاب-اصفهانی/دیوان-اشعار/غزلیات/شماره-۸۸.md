---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>جان کیست ندانم که در این شهر برایت</p></div>
<div class="m2"><p>ناکرده فدای همه جان‌ها به فدایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنت ز حد افزون شد و غیرت نگذارد</p></div>
<div class="m2"><p>کز چشم بد خلق سپارم به خدایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تأثیر دعایش سبب ترک جفا شد</p></div>
<div class="m2"><p>ای کاش نبودی اثری ای دل به دعایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با این که بود جای تو دایم به دل من</p></div>
<div class="m2"><p>هر لحظه ندانم که به جویم زکجایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ترک جفایت ستمی تازه مراد است</p></div>
<div class="m2"><p>اکنون که گرفته است دلم خو به جفایت</p></div></div>