---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>کشم ار جور از او باز وفا می‌خواهم</p></div>
<div class="m2"><p>بین چه‌ها می‌کشم از یار و چه‌ها می‌خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خدا بر کف او تیغ جفا می‌خواهم</p></div>
<div class="m2"><p>راحت خلق خدا را ز خدا می‌خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که خواهم که به آمیزش کس خو نکنی</p></div>
<div class="m2"><p>خویش را هم ز تو پیوسته جدا می‌خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه بیداد بتان کشت مرا لیک ای دل</p></div>
<div class="m2"><p>داد خود را ز تو در روز جزا می‌خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس دگرباره مرا شوق گرفتاری توست</p></div>
<div class="m2"><p>خویشتن را ز کمند تو رها می‌خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساده خواهم لبت از سبزهٔ خط آری دور</p></div>
<div class="m2"><p>خضری را ز لب آب بقا می‌خواهم</p></div></div>