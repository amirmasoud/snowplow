---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>گله تا حشر اگر زیار کنم</p></div>
<div class="m2"><p>شرح یک شمه از هزار کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاره ی یأس شد زو عده ی وصل</p></div>
<div class="m2"><p>تا چه با درد انتظار کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگارم سیه تو کردی و من</p></div>
<div class="m2"><p>شکوه از جور روزگار کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در او چه جای نومیدیست</p></div>
<div class="m2"><p>به چه دل را امیدوار کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم جانی که هست قابل نیست</p></div>
<div class="m2"><p>که به پای تواش نثار کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم ز بهر نثار نیست مرا</p></div>
<div class="m2"><p>بجز این نیم جان چه کار کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تو چشم (سحاب) را چو سحاب</p></div>
<div class="m2"><p>خجل از چشم اشک بار کنم</p></div></div>