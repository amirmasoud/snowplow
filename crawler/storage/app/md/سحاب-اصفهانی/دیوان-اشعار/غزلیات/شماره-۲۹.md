---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>تا این دل دیوانه بود عاقلهٔ ما</p></div>
<div class="m2"><p>از طعن جنون بیهده باشد گلهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسوده غم عشق ز تنگی دو عالم</p></div>
<div class="m2"><p>روزی که نمودند به او حوصلهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادابی دل داده به خار و خس این دشت</p></div>
<div class="m2"><p>خونی که به هر گام چکد ز آبلهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمریست که پوییم ره کویش اگرچه</p></div>
<div class="m2"><p>از او نبود جز قدمی فاصلهٔ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دربار، به جز عشق نداریم متاعی</p></div>
<div class="m2"><p>ایمن بود از راه زنان قافلهٔ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر شاه (سحاب) ار رسد این نظم عجب نیست</p></div>
<div class="m2"><p>کز لعل لب یار ببخشد صلهٔ ما</p></div></div>