---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>در دلم خار غم خلیدهٔ اوست</p></div>
<div class="m2"><p>پشتم از بار دل خمیدهٔ اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ کین هر زمان کشیدهٔ اوست</p></div>
<div class="m2"><p>صید دل‌ها به خون تپیدهٔ اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من یا تنور پیرزن است</p></div>
<div class="m2"><p>کآتش آه، آب دیدهٔ اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهد مهر آنکه کام او بخشد</p></div>
<div class="m2"><p>کام زهر آنکه غم چشیدهٔ اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه آن چاک پیرهن دارد</p></div>
<div class="m2"><p>جامهٔ صبر من دریدهٔ اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگه او به جان و دل بی‌رحم</p></div>
<div class="m2"><p>چه کنم دل از او و دیدهٔ اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رام کس نیست چشم وحشی او</p></div>
<div class="m2"><p>آخر این آهوی رمیدهٔ اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این رخ دلربا (سحاب) کز او</p></div>
<div class="m2"><p>خلقی از جان طمع بریدهٔ اوست</p></div></div>