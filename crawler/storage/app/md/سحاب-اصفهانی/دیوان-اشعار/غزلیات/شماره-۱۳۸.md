---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>بی بند کجا پای دل من بگذارد</p></div>
<div class="m2"><p>زلفی که تو را بند به گردن بگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که روم یک دو سه گام از پی قاتل</p></div>
<div class="m2"><p>گر حسرت در خاک تپیدن بگذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید عوض مرهم اگر خنجر جورت</p></div>
<div class="m2"><p>صد منتم از زخم تو بر تن بگذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خور بهر تماشای وثاق تو عجب نیست</p></div>
<div class="m2"><p>چون شیشه اگر چشم به روزن بگذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه دل فرهاد زرشک شکر آخر</p></div>
<div class="m2"><p>داغی به دل شاهد ار من بگذارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیف است برد غیر به خاک آرزویش را</p></div>
<div class="m2"><p>گامی به سرش گردم مردن بگذارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سوز دلم نیست (سحاب) آگه و ترسم</p></div>
<div class="m2"><p>دستی ز ترحم به دل من بگذارد</p></div></div>