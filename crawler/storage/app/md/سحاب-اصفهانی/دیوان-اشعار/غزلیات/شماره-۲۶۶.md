---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>در خیال تو به افتاده است دل از باده‌ام</p></div>
<div class="m2"><p>زین سبب از چشمت ای پیر مغان افتاده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانهٔ دل شد ز هر نقش و نگاری بی‌نیاز</p></div>
<div class="m2"><p>با وجود نقش مهر آن نگار ساده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا میان بندگی بستم به کوی می‌فروش</p></div>
<div class="m2"><p>یافت آزادی زهر قیدی دل آزاده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک طرف رشک رقیبان یک طرف درد فراق</p></div>
<div class="m2"><p>بهر مردن هرچه گردون خواست کرد آماده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنگ شد بر دل فضای سینه بی‌او گرچه من</p></div>
<div class="m2"><p>در به روی او بسی از دست خود بگشاده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر هوای مقصدی دارد که هرگز کس ندید</p></div>
<div class="m2"><p>گرچه پا در وادی عشقت همان ننهاده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به کیش عشق رو آورده‌ام شادم که نیست</p></div>
<div class="m2"><p>فکر زنار و صلیب و سجه و سجاده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بهای بوسه خواهد یار آنکه از (سحاب)</p></div>
<div class="m2"><p>نقد جانی را که در آغاز عشقش داده‌ام</p></div></div>