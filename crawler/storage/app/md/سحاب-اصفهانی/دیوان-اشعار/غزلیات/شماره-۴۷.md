---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>از زلف و رخت روز و شبم تیره و تار است</p></div>
<div class="m2"><p>زان هر دو عیان حادثه ی لیل و نهار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روز شمار ار بشمارم عجبی نیست</p></div>
<div class="m2"><p>غم های شب هجر که بیرون ز شمار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر عارض گلگون مگرش دیده ی لاله</p></div>
<div class="m2"><p>آن خال سیه دید که داغش به عذار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل هوس آزرده شدند از غمت آری</p></div>
<div class="m2"><p>نا صافی صهبا سبب رنج خمار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وصل تو کمتر بود آرام دل آری</p></div>
<div class="m2"><p>بی طاقتی مرغ چمن فصل بهار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک بلبل شیداست همین شیفته ی گل</p></div>
<div class="m2"><p>شیدای گل روی تو هر گوشه هزار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بخت نصیب که کند زخم خدنگش</p></div>
<div class="m2"><p>ترکی که کنونش بدل آهنگ شکار است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتم ز پی تجربه ز آنکو دو سه گامی</p></div>
<div class="m2"><p>دیدم که قرار دل من در چه قرار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذشت جدا ز آن سر کو کار من از کار</p></div>
<div class="m2"><p>تا در سر کوی تو دل من به چه کار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذشت جدا ز آن سر کو کار من از کار</p></div>
<div class="m2"><p>تا در سر کوی تو دل من به چه کار است؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیر از در میخانه که ماوای (سحاب) است</p></div>
<div class="m2"><p>از حادثه ی چرخ کجا جای قرار است</p></div></div>