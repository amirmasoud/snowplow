---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>جز دام توام شکر که جای دگری نیست</p></div>
<div class="m2"><p>ور هست مرا جای دگر بال و پری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نوش چه اندیشه ات از رنج خمار است</p></div>
<div class="m2"><p>آن عیش کدام است که بی دردسری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ضدند نکویی و وفا ورنه که دیده است</p></div>
<div class="m2"><p>هرگز پدری را که بفکر پسری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانم اگر این است مرا قوت پرواز</p></div>
<div class="m2"><p>وقتی که بگلشن رسم از گل خبری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لعل شکر بار مرا هیچ نصیبی</p></div>
<div class="m2"><p>غیر از لب خشکی و جز از چشم تری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانی که زهم عشق و هوس کی بشناسی</p></div>
<div class="m2"><p>روزی که در آن کوی بجز من دگری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذار قدم همچو (سحاب) ای دل گمراه</p></div>
<div class="m2"><p>پا در ره عزلت که در آن ره خطری نیست</p></div></div>