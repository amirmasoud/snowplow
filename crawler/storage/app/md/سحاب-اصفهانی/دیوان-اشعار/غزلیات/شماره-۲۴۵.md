---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>چه غم که ریخته شد بال و پر ز سنگ توام</p></div>
<div class="m2"><p>که بی نیاز زبال از پر خدنگ توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی زمحفل من زود و دو دیر باز آیی</p></div>
<div class="m2"><p>هم از شتاب تو غمگین هم از درنگ توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان به پیش نظر در غمت چنان تنگ است</p></div>
<div class="m2"><p>که کار بر دل تنگ از دهان تنگ توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مباد ذوق اسیران چنگ عشق تو را</p></div>
<div class="m2"><p>اگر خیال رهایی بود ز چنگ توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دل غم تو نیاید برون اگر چه بدل</p></div>
<div class="m2"><p>هزار رخنه فزون است از خدنگ توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جانبم نکنی جز به وقت مرگ نگاه</p></div>
<div class="m2"><p>از آن ز آشتی ات خوشتر است جنگ توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به می (سحاب) چه حاجت که بی نیاز از آن</p></div>
<div class="m2"><p>به یاد لعل وی و اشک لعل رنگ توام</p></div></div>