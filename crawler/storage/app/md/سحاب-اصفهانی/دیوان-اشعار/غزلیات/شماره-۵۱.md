---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>چه عجب گر دلم از عشق تو در تاب و تب است؟</p></div>
<div class="m2"><p>هر که در تاب و تبی نیست ازین غم عجب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخل آرد رطب اما چو قدت موزون نیست</p></div>
<div class="m2"><p>قد موزون تو سرویست که بارش رطب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چه ملت بگزینیم و چه آئین که پدید</p></div>
<div class="m2"><p>کف موسی زرخ انفاس مسیحش ز لب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی ام تا بکنی رهسپر ودای شوق</p></div>
<div class="m2"><p>تا مرا قوت رفتار به پای طلب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد روی تو به دل رشحه ابر است و گیاه</p></div>
<div class="m2"><p>غم عشق تو به جان پرتو ماه و قصب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که دید آن رخ چون شب به رخ چون روزت</p></div>
<div class="m2"><p>روز عشاق تو دانست که مانند شب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گنه دوستی آمد سبب قتل (سحاب)</p></div>
<div class="m2"><p>کس نگوید که جفای تو به من بی سبب است</p></div></div>