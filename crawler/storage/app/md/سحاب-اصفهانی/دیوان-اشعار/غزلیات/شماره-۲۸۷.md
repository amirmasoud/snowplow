---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>غافل است آن که از دلم دل او</p></div>
<div class="m2"><p>گوبیندیش ز آه غافل او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده ام جا به بزم غیر که یار</p></div>
<div class="m2"><p>ناید از شرم من به محفل او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون جرس دل فغان کند گویی</p></div>
<div class="m2"><p>بر شتر بسته اند محمل او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم فلک بود خصم دل هم یار</p></div>
<div class="m2"><p>تا کدامند زین دو قاتل او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه دارد ز دلبری دارد</p></div>
<div class="m2"><p>جز ترحم که نیست در دل او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل چنین کرده کار من مشکل</p></div>
<div class="m2"><p>که خود آسان مباد مشکل او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به که گیرم سراغ دل چو مرا</p></div>
<div class="m2"><p>ندهد کس نشان ز منزل او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون فتد در خیال وصل (سحاب)</p></div>
<div class="m2"><p>عقل خندد به فکر باطل او</p></div></div>