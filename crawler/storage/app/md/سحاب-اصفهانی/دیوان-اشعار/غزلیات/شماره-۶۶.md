---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>تا زهر صید نه در دام تو غوغایی هست</p></div>
<div class="m2"><p>میتوان گفت که خوش تر زچمن جایی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه خواهند از او داد من، اما نتوان</p></div>
<div class="m2"><p>بی سبب کشته شد امروز که فردایی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجودم به دلی غم نه و تا من هستم</p></div>
<div class="m2"><p>غم نداند که به غیر از دل من جایی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان به کف دارم و دانم به کلافی ندهند</p></div>
<div class="m2"><p>یوسفی را که به هر گوشه زلیخایی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسرت قوت رفتار چه آرد به سرم</p></div>
<div class="m2"><p>چون به کویی نگرم نقش کف پایی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاد از اینم که نداری سر سودای کسی</p></div>
<div class="m2"><p>گر چه در هر سری از عشق تو سودایی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سالک راه سخن طبع (سحابست) امروز</p></div>
<div class="m2"><p>اگر این مرحله را مرحله پیمایی هست</p></div></div>