---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>رقیب یافته در کوی یار بار امشب</p></div>
<div class="m2"><p>چه گونه بار ببندم ز کوی یار امشب؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوید کشتنم آن شوخ داده امشب آه</p></div>
<div class="m2"><p>که او نکشت و مرا کشت انتظار امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمع سوزم ازین رشک کز اجابت غیر</p></div>
<div class="m2"><p>به محفل تو مرا داده اند بار امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد امشب از برم از جور روزگار آیا</p></div>
<div class="m2"><p>دگر به کام که گردید روزگار امشب؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز انتظار چه سودی نه همچو امروزش</p></div>
<div class="m2"><p>گرفتم این که نشستم به رهگذر امشب؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شکر اینکه من امروز هر دم از غم تو</p></div>
<div class="m2"><p>سزد که آئیم ای شمع بر مزار امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وثاق غیر زیک شمع روشن است و بود</p></div>
<div class="m2"><p>هزار شمع مرا ز آه شعله بار امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغان که رفت زشوق وصال او جانی</p></div>
<div class="m2"><p>که داشتیم بتن از پی نثار امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کناره کرد (سحاب) آن مه از کنارم و کرد</p></div>
<div class="m2"><p>سرشک حسرتم از دیده در کنار امشب</p></div></div>