---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>یاد بی تابی روز وصل یار آمد مرا</p></div>
<div class="m2"><p>چون بگوش افغان بلبل در بهار آمد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل بی تاب زلفی تابدار آمد مرا</p></div>
<div class="m2"><p>بی قراری آفت صبر و قرار آمد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار تا مشکل نشد در عشق مرگ آسان نشد</p></div>
<div class="m2"><p>عقده های کار من آخر بکار آمد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد عیش روزگار وصل پاداشش چه بود؟</p></div>
<div class="m2"><p>آنچه بر سر از جفای روزگار آمد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت و دل برد از من و اکنون غمش ریزد ز چشم</p></div>
<div class="m2"><p>قطره های خون که از دل یادگار آمد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو تا روز شمار افغان که نتوانم شمرد</p></div>
<div class="m2"><p>غصه های دل که بیرون از شمار آمد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سرت گفتا که آیم امشب و بر سر (سحاب)</p></div>
<div class="m2"><p>آنچه از هجران نیامد زانتظار آمد مرا</p></div></div>