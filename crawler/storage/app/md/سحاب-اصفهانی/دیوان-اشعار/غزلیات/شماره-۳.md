---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>تا سازم آشنایت نا آشنا نگارا</p></div>
<div class="m2"><p>بیگانه کردم از خویش یاران آشنا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه جز صبا نیست پیکی ز من به کویش</p></div>
<div class="m2"><p>خواهم که ره نباشد در کوی او صبا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون من کسی گذارد سر بر خط غلامیش</p></div>
<div class="m2"><p>بیرون چرا نهد کس از حد خویش پارا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نو چه خواهد آرد کس را به دام عشقش</p></div>
<div class="m2"><p>با عاشقان دیرین کمتر کند جفا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با جور آن جفاجو چندان نکرده ام خو</p></div>
<div class="m2"><p>کآرم به خاطر از او اندیشه ی وفا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردم بلای هجران درمان وصال جانان</p></div>
<div class="m2"><p>دردا که نیست درمان این درد بی دوارا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که: گویم امشب تنها به او غم دل</p></div>
<div class="m2"><p>بی مدعی نیاید چون یافت مدعا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از رخ به خلق بنمود آثار صنع معبود</p></div>
<div class="m2"><p>وز خویش کرد خوشنود هم خلق و هم خدا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اکنون (سحاب) کآنجاره یافتند اغیار</p></div>
<div class="m2"><p>شادم از این که ره نیست در کوی دوست ما را</p></div></div>