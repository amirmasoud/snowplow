---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ندارد تحفهٔ جان گر حقارت</p></div>
<div class="m2"><p>زما صد جان و از او یک اشارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عبارت از حیات جاودان چیست؟</p></div>
<div class="m2"><p>لب شیرین آن شیرین عبارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهای بوسه ی جان بخش جان خواست</p></div>
<div class="m2"><p>ندانم چیست سودش زین تجارت؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از شوق خواهی نسپرم جان</p></div>
<div class="m2"><p>نهان از من به قتلم کن اشارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه داری پر عتاب آن لعل شیرین؟</p></div>
<div class="m2"><p>ز شیرینی نکو نبود مرارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس از زهاد بوی عشق یابد</p></div>
<div class="m2"><p>گر از کافور کس یابد حرارت!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمین برگشت از آن کو قاصد من</p></div>
<div class="m2"><p>مگر بر قتل من دارد بشارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دمید از باغ حسنش سبزه ی خط</p></div>
<div class="m2"><p>فزون شد گلستانش را نضارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نماند دل به دست کس (سحابا)</p></div>
<div class="m2"><p>چو ترک من گشاید دست غارت</p></div></div>