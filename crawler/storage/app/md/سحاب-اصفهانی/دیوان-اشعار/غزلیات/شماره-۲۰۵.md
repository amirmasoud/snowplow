---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>نه همین ز شرم در بر نکشیده‌ام هنوزش</p></div>
<div class="m2"><p>که برم نشسته عمری و ندیده‌ام هنوزش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم از تو خرم و خوش به سوالی و جوابی</p></div>
<div class="m2"><p>که نگفته‌ام همان و نشنیده‌ام هنوزش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مشام غیر خواهم نرسد از او شمیمی</p></div>
<div class="m2"><p>ز ریاض حسن آن گل که نچیده‌ام هنوزش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خویش کرده‌ام خوش به متاع کم‌بهایی</p></div>
<div class="m2"><p>که زلعل او به صد جان نخریده‌ام هنوزش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رهش اگر چه شد خون دل و شد ز دیده بیرون</p></div>
<div class="m2"><p>کشد انتظار مقدم او دل و دیده‌ام هنوزش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب است اینکه در سر بودم (سحاب) مستی</p></div>
<div class="m2"><p>ز شراب وصل اگرچه نچشیده‌ام هنوزش</p></div></div>