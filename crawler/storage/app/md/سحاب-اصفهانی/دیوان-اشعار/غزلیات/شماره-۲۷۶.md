---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>آه که آخر نماند ای بت دمساز من</p></div>
<div class="m2"><p>حسن بدانجام تو عشق خوش آغاز من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طایر دل آشیان بست به شاخی دگر</p></div>
<div class="m2"><p>نغمه ی دیگر گرفت مرغ خوش آواز من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانه میفشان دگر بهر فریبم که هست</p></div>
<div class="m2"><p>جانب بام دگر خواهش پرواز من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که به خواری مدام راندیم از کوی خویش</p></div>
<div class="m2"><p>از چه کنون می کنی این همه اعزاز من؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز نگاه تو هست از پی صیدم ولی</p></div>
<div class="m2"><p>قوت پرواز نیست در پر شهباز من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که نکردی نگاه سوی من از کبر و ناز</p></div>
<div class="m2"><p>از چه پسندی کنون کبر من و ناز من؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه که چون گل درید پرده ی حسنش (سحاب)</p></div>
<div class="m2"><p>پرده نشینی که بود پرده در راز من</p></div></div>