---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>یاری از یار من و چرخ ستمکار مخواه</p></div>
<div class="m2"><p>یاری از چرخ اگر خواستی از یار مخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز چشم تو اگر چشم عنایت دارد</p></div>
<div class="m2"><p>گو پرستاری بیمار ز بیمار مخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین طبیبان که توانند علاجی بکنند</p></div>
<div class="m2"><p>چاره ی درد دل خویش به ناچار مخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که را زیب دکان جنس وفا و هنر است</p></div>
<div class="m2"><p>به جوی گو برسان نرخ و خریدار مخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی از سنگ جفایش اگر ایمن باشی</p></div>
<div class="m2"><p>هرگز آسایش از آن سایه ی دیوار مخواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاری ای که رقیبش نباشد مطلب</p></div>
<div class="m2"><p>در گلستان جهان یک گل بیخار مخواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که در غمکده ی دهر گرفتار غمی است</p></div>
<div class="m2"><p>گو بده جان چو (سحاب) از غم و غمخوار مخواه</p></div></div>