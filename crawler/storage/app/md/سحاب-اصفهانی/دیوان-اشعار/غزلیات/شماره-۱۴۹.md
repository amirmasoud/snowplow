---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>ما و رقیب را دل نا شاد و شاد داد</p></div>
<div class="m2"><p>گردون به هر کس آنچه ببایست داد، داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکس که کرد قسمت رنج جهان مرا</p></div>
<div class="m2"><p>چندان که بود حوصله کم غم زیاد داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخمت نشد نصیب دل نا مراد من</p></div>
<div class="m2"><p>تا اختر خجسته کرا این مراد داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس نبود معتقد عفو ایزدی</p></div>
<div class="m2"><p>گوشی به پند زاهد سست اعتقاد داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الفت دل ترا نتوان داد با وفا</p></div>
<div class="m2"><p>اضداد را بهم نتوان اتحاد داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه داد من نداد همین پادشاه من</p></div>
<div class="m2"><p>شاهی به ملک حسن نیاید که داد، داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آگه نیم که بی تو چه بگذشت بر سرم</p></div>
<div class="m2"><p>خاکی غم فراق تو دانم به باد داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوتاه کردی از در میخانه پای من</p></div>
<div class="m2"><p>آی آسمان ز دست جفای تو، داد، داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن روز راحت دو جهانم زیاد برد</p></div>
<div class="m2"><p>گردون که مهر روی بتانم به یاد داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک دم گدائی در دیر مغان (سحاب)</p></div>
<div class="m2"><p>کی میتوان به مملکت کیقباد داد</p></div></div>