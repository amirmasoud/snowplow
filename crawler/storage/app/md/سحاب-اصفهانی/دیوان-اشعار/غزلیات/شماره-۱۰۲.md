---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ز صاف راح بکش هر صباح جام صبوح</p></div>
<div class="m2"><p>که صبح موسم عیش است و راح لذت روح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صباح عید و لب جویبار و جام صبوح</p></div>
<div class="m2"><p>روا بود که پشیمان شود ز تو به نصوح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه سود از این که لبش مرهم جراحت هاست</p></div>
<div class="m2"><p>مرا که هست جگر داغدار و دل مجروح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دری که هست بدست رقیب ما مفتاح</p></div>
<div class="m2"><p>روا بود که نباشد به روی ما مفتوح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سود کایمنی از اشک چشم خویش (سحاب)</p></div>
<div class="m2"><p>همین نه بس که سلامت بود سفینه ی نوح</p></div></div>