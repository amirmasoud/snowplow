---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>چون شادی بی غم به جهان یاد ندارم</p></div>
<div class="m2"><p>دلشاد از اینم که دل شاد ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد آن شه چینی که دهد داد ندارم</p></div>
<div class="m2"><p>اما چو تو بیداد گری یاد ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن روز کدام است که بی آن لب شیرین</p></div>
<div class="m2"><p>صد رشک به ناکامی فرهاد ندارم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آئینه گرفت از کف مشاطه و گفتا</p></div>
<div class="m2"><p>منت ز تو با حسن خداداد ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اکنون که ز فریاد دهد داد ضعیفان</p></div>
<div class="m2"><p>از ضعف دگر قوت فریاد ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آنکس که ننالد چو (سحاب) از تو بخاطر</p></div>
<div class="m2"><p>در خیل تو از بنده و آزاد ندارم</p></div></div>