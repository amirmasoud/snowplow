---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>در دام صیاد ای فلک یا ذوق فریادم مده</p></div>
<div class="m2"><p>یا آن که از فریاد من رحمی به صیادم مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا در مکافات خوشی ای بخت ناشادم مکن</p></div>
<div class="m2"><p>ورزان که یک سان میکنی چون خاک بر بادم مده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رهگذار خویشتن با خاک یک سانم مکن</p></div>
<div class="m2"><p>یا آن که از عیش جهان هرگز دل شادم مده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دادی پی دل بردنم گرداد خلقی داد من</p></div>
<div class="m2"><p>بهر فریب دیگران چون دل زکف دادم مده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کرده ام ای هم آشیان خو با اسیری، آگهی</p></div>
<div class="m2"><p>از ذوق بال افشانی مرغان آزادم مده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زآن شوخ شیرین لب ز من محروم تر نبود کسی</p></div>
<div class="m2"><p>ای همنشین تسکین دل از حال فرهادم مده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چون (سحاب) از زخم تو نومید باشد مدعی</p></div>
<div class="m2"><p>گر نالم از بیدادت ای بیدادگر دادم مده</p></div></div>