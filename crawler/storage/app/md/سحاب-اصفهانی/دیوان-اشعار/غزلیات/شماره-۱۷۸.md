---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>گلی کز آتش غیرت گلاب می‌سازد</p></div>
<div class="m2"><p>برای آن گل عارض گلاب می‌سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد محفل وصلت که می‌کند محکم</p></div>
<div class="m2"><p>سرشک من همه عالم خراب می‌سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کام زهر مکافات خواهدم عمری</p></div>
<div class="m2"><p>دمی ز خویشم اگر کامیاب می‌سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگو که بی‌خودی ما نقاب دیدهٔ ماست</p></div>
<div class="m2"><p>چرا جمال نهان در نقاب می‌سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس که تاب ندارد (سحاب) طبع دلم</p></div>
<div class="m2"><p>از آن به سنبل پر پیچ و تاب می‌سازد</p></div></div>