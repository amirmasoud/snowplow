---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>تا زحال دل من دلبر من غافل بود</p></div>
<div class="m2"><p>آنچه کام من و دل بود از او حاصل بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رست جان از تن و صد شکر که زایل گردید</p></div>
<div class="m2"><p>آنچه یک چند میان من و او حایل بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نه جان بود فدای تو از این بودم شرم</p></div>
<div class="m2"><p>شرمم اکنون همه آن است که ناقابل بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقی از راز دل من نبد آگاه ولی</p></div>
<div class="m2"><p>به زبان تو فتاد آنچه مرا در دل بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدم آن سرو که مشهور به آزادی گشت</p></div>
<div class="m2"><p>آنهم از غیرت بالای تو پا در گل بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بیتاب غم اشک رقیب افزون داشت</p></div>
<div class="m2"><p>روزگاری که دل او به وفا مایل بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت نا دیدن یار است (سحاب) آنچه بد است</p></div>
<div class="m2"><p>آه کز دیدن روی دگران غافل بود</p></div></div>