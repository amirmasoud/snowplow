---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>به زیر تیغ از بس جان خود قابل نمی‌بینم</p></div>
<div class="m2"><p>ز شرم جان ناقابل سوی قاتل نمی‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی را کزوی آسان است کام دل نمی‌بینم</p></div>
<div class="m2"><p>ولی چون کار این دل کار کس مشکل نمی‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را با اینکه می‌بینم به خون خلق مستعجل</p></div>
<div class="m2"><p>به خون خویشتن خویش مستعجل نمی‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم عقل جز دیوانگان وادی عشقت</p></div>
<div class="m2"><p>چو بینم هیچ کس را در جهان عاقل نمی‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیفشاند به جز تخم وفا در کشت زار دل</p></div>
<div class="m2"><p>ولی زین کشت جز بی‌حاصلی حاصل نمی‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل از یاری او هرکس که بینم کنده و کس را</p></div>
<div class="m2"><p>به غیر از خود درین اندیشهٔ باطل نمی‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلیل بی‌وفایی‌های یار بی‌وفا این بس</p></div>
<div class="m2"><p>که او را هیچ با اهل وفا مایل نمی‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو را یا رب سرشته دست قدرت از چه آب و گل</p></div>
<div class="m2"><p>که جز بی‌مهریت نقصی در آب و گل نمی‌بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل غمگین (سحاب) و جز می صافی دگر چیزی</p></div>
<div class="m2"><p>کزین آیینه زنگ غم کند زایل نمی‌بینم</p></div></div>