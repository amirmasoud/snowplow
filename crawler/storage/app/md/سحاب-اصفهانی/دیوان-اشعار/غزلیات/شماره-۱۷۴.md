---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>روزی که از سبو به قدح باده خواستند</p></div>
<div class="m2"><p>اسباب زندگی همه آماده خواستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل کسی به نقش و نگار جهان نیست</p></div>
<div class="m2"><p>نقش جمال لاله رخان ساده خواستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادند پیچ و تاب به گیسوی دلبران</p></div>
<div class="m2"><p>دامی برای مردم آزاده خواستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن خرقه را که طالب تذویر بافتند</p></div>
<div class="m2"><p>در قید دلق و سجه و سجاده خواستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ما به کوی دوست که بیش از دو گام نیست</p></div>
<div class="m2"><p>هر سو به اختلاف بسی جاده خواستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادند جلوه در پر پروانه شمع را</p></div>
<div class="m2"><p>خاکسترش به باد فنا داده خواستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بستند بر میان تیغ و (سحاب) را</p></div>
<div class="m2"><p>بر خاک و خون چو صید شه افتاده خواستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاقان دهر فتحعلی شه که انجمش</p></div>
<div class="m2"><p>بهر نثار سبحه و سجاده خواستند</p></div></div>