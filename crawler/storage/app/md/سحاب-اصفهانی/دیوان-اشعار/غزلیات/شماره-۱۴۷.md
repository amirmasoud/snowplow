---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>ز روی پردگی ما چو پرده بر گیرند</p></div>
<div class="m2"><p>به روی پرده نکویان پرده در گیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنانم از ستمت کز پی تسلی خویش</p></div>
<div class="m2"><p>بلا کشان تو از حال من خبر گیرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آب عفو بشو جرم عالمی ورنه</p></div>
<div class="m2"><p>شراره ی که تر و خشک جمله در گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر این بود ره عشق تو سالکان طریق</p></div>
<div class="m2"><p>سراغ منزل از این راه پر خطر گیرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفتم اینکه توان بست دل به غیر تویی</p></div>
<div class="m2"><p>کدام دل که توانند از تو بر گیرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز تازه بود داستان عشق (سحاب)</p></div>
<div class="m2"><p>هزار بار گر این قصه مختصر گیرند</p></div></div>