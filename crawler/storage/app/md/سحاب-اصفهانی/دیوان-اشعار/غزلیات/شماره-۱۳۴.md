---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>کدام تیر بلا ترکش زمانه ندارد</p></div>
<div class="m2"><p>که از تنم هدف و از دلم نشانه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخوان در این چمن ای مرغ دل سرود محبت</p></div>
<div class="m2"><p>چرا که گوش کسی میل این ترانه ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلش اگر چه چو سنگ است لیک قصه ی خود را</p></div>
<div class="m2"><p>نگویمش که دلش تاب این فسانه ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین نه عمر فزاید چو آب چشمه ی حیوان</p></div>
<div class="m2"><p>کدام خاصیت آن خاک آستانه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم به ورطه ی دریای عشق چیست غریقی</p></div>
<div class="m2"><p>که میل ساحل ازین بحر بی کرانه ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تار زلف تو دلها گرفته اند ز بس جا</p></div>
<div class="m2"><p>عجب نه گر سر زلف تو جای شانه ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جرم دوستی خود (سحاب) کردمش آگه</p></div>
<div class="m2"><p>چو یافتم که پی کشتنم بهانه ندارد</p></div></div>