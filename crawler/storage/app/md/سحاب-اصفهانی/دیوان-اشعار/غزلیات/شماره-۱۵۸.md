---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>اسیر زلف تو فارغ ز هر گزند شود</p></div>
<div class="m2"><p>خوشا دلی که گرفتار آن کمند شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراغ تربتم از کس پس از هلاک مکن</p></div>
<div class="m2"><p>ببین که شعله ی آه از کجا بلند شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن دیار که دلبستگی به زلفی نیست</p></div>
<div class="m2"><p>کسی چگونه در آن قید پای بند شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند نصیحت ناصح به حال من چه اثر</p></div>
<div class="m2"><p>جز اینکه آتش من تیز تر ز پند شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سکون در آتش عشق بتان مخواه از دل</p></div>
<div class="m2"><p>مقیم آتش سوزنده چون سپند شود</p></div></div>