---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>به بزم ای زاهد اینک می سبیل است</p></div>
<div class="m2"><p>اگر وقتی به جنت سلسبیل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود جان از تن و جانان به محمل</p></div>
<div class="m2"><p>من و او هر دو را عزم رحیل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم او بر خلاف نار نمرود</p></div>
<div class="m2"><p>در اول چون گلستان خلیل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلیل من به گمراهی همین بس</p></div>
<div class="m2"><p>که دل در وادی عشقم دلیل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندادم بوسه ای زان لب چه حاصل؟</p></div>
<div class="m2"><p>ازین خرما که او را بر نخیل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خجل زین چشم گریان و دل تنگ</p></div>
<div class="m2"><p>کف بخشنده و چشم بخیل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خون گل نگارین پنجه ی او</p></div>
<div class="m2"><p>چو تیغ قاتل از خون قتیل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم مهجوری روی جمیلش</p></div>
<div class="m2"><p>علاجش مرگ یا صبر جمیل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خموشی پیشه کن در مکتب عشق</p></div>
<div class="m2"><p>که درس عاشقی بی قال و قیل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جنان و سلسبیلی هست اما</p></div>
<div class="m2"><p>جنان میخانه و می سلسبیل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیاید هم به پایان در شب هجر</p></div>
<div class="m2"><p>حدیث زلف یار از بس طویل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>(سحاب) ار عهد دلبر بی ثبات است</p></div>
<div class="m2"><p>دوام حسن او هم زین قبیل است</p></div></div>