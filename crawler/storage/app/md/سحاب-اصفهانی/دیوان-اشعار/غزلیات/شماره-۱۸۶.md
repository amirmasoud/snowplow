---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>کشیم بارز کویت به سوی یار دگر</p></div>
<div class="m2"><p>بر تو یابد اگر غیر بار بار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از جفای تو در کار جان سپردن و تو</p></div>
<div class="m2"><p>هنوز بهر هلاکم به فکر کار دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امید من ز تو امشب روا نشد گویا</p></div>
<div class="m2"><p>بود به راه تو چشم امیدوار دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجل رسیده و وصل تو باز می طلبم</p></div>
<div class="m2"><p>من انتظار تو دارم تو انتظار دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بزم وصل شبی بارده مرازان پیش</p></div>
<div class="m2"><p>که نخل حسن تو آرد به بار بار دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین سیه نکنی روز من اگر دانی</p></div>
<div class="m2"><p>که داری از پی امروز روزگار دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(سحاب) را به سگانش چه نسبت است کزو</p></div>
<div class="m2"><p>بقدر خود طلبد هر کس اعتبار دگر</p></div></div>