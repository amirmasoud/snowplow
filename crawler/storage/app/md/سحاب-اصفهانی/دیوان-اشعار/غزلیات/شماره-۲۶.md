---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>گر سر انکار قتل چون منی دارد زننگ</p></div>
<div class="m2"><p>به که در محشر گواه خود کند قاتل مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ضعفم افزون باد یا رب در شب هجران (سحاب)</p></div>
<div class="m2"><p>زآنکه فارغ کرد از این افغان بیحاصل مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیش از اینم طاقت بیداد نبود چون کنم</p></div>
<div class="m2"><p>گه از جورش رهاند خسرو عادل مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه نشان فتحعلی شاه آنکه بنوازد (سحاب)</p></div>
<div class="m2"><p>با هزاران جرم باز از رحمت شامل مرا</p></div></div>