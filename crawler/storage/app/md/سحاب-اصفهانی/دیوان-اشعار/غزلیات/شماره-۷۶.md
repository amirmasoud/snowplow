---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>چو حاصل است ترا وصل یار حور سرشت</p></div>
<div class="m2"><p>خلاف منطق و عقل است آرزوی بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خود به دور زمان سرنوشت ساز خودی</p></div>
<div class="m2"><p>چنانکه میدرود هر چه را که دهقان کشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر دچار شود چشم دل به بدبینی</p></div>
<div class="m2"><p>بدیده منظر زیبای دهر باشد زشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه زنده به دلهاست نام او آنکو</p></div>
<div class="m2"><p>برفت و نام نکوئی بیادگار بهشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسد بگوش به هر جا که نام دوست (سحاب)</p></div>
<div class="m2"><p>بگوش گیر و مگو مسجد است یا که کنشت</p></div></div>