---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>تا روان اشک من از دیده ی تر خواهد بود</p></div>
<div class="m2"><p>خلق را ز آتش خویت چه اثر خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آشیانی که در این باغ نهادم شادم</p></div>
<div class="m2"><p>کآن هم از باد خزان زیر و زبر خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان یافتن از خنده ی این برق که باغ</p></div>
<div class="m2"><p>آخر از گریه ی ابرش چه ثمر خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به فکر غم فرهاد نباشد شیرین</p></div>
<div class="m2"><p>تلخکامی وی از رشک شکر خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق مستی ز کسی پرس که در محفل عشق</p></div>
<div class="m2"><p>لعل گون ساغرش از خون جگر خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بود خاصیت لعل تو با طبع (سحاب)</p></div>
<div class="m2"><p>بی بهاتر ز صدف در و گهر خواهد بود</p></div></div>