---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>نصیبم باد یا رب وصل او دور از رقیب اما</p></div>
<div class="m2"><p>رقیب از وصل او هم باد یارب بی نصیب اما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین نه در چمن مرغ چمن را جاست گاهی هم</p></div>
<div class="m2"><p>گرفتار شکنج دام گردد عندلیب اما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوای درد خود را از طبیبان هر کسی جوید</p></div>
<div class="m2"><p>مرا هم هست درد بی دوایی از طبیب اما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر کس داده یار دلفریبم وعده وصلی</p></div>
<div class="m2"><p>به من میدهد از وعده ی وصلش فریب اما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث دوستی بود آشنا در گوشت اکنون، هم</p></div>
<div class="m2"><p>حدیث آشنایی هست در گوشت غریب اما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین کاغیار را جایی نباشد جز سر آن کو</p></div>
<div class="m2"><p>مرا هم نیست جایی در سر کوی حبیب اما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو من کز آن کو دور گشتم عاقبت یا رب</p></div>
<div class="m2"><p>رقیبان هم شوند آواره زان کو عنقریب اما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل آن سنگدل گر ناشکیبا نیست دور از من</p></div>
<div class="m2"><p>مرا هم نیست دور از روی او در دل شکیب اما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رقیب از هجر روی او سپارد جان (سحاب) آن هم</p></div>
<div class="m2"><p>جدا از روی او خواهد سپارد جان رقیب اما</p></div></div>