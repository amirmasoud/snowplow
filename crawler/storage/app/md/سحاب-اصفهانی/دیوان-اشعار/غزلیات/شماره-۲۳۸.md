---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>روز جزا چون ادعای جان ناقابل کنم</p></div>
<div class="m2"><p>کز شرم نتوانم نگاهی جانب قاتل کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رشکم آید زین و آن گیرم سراغت هر زمان</p></div>
<div class="m2"><p>هرگه تو را ای دلستان خواهم نشان از دل کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بزم اغیار ای صنم در بزم ننهادم قدم</p></div>
<div class="m2"><p>یا نقد جان از کف دهم یا کام دل حاصل کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آنکه امید وصال از وی بود امری محال</p></div>
<div class="m2"><p>از سادگی هر ماه و سال این فکر بی‌حاصل کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مایل چو با بیگانگانت دیدم ای ناآشنا</p></div>
<div class="m2"><p>زین حیله رفتم تا تو را با خویشتن مایل کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی بده جامی از آن صافی که باشد قوت جان</p></div>
<div class="m2"><p>تا زنگ اندوه جهان از لوح دل زایل کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانم (سحاب) آخر مرا آید به بالین از وفا</p></div>
<div class="m2"><p>اما نمی‌دانم چه با این مرگ مستعجل کنم</p></div></div>