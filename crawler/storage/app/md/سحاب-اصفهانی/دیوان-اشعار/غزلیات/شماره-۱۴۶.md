---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>یکی است بیتو گرم زهر در گلو ریزند</p></div>
<div class="m2"><p>و گر شراب به جام من از سبو ریزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باده ها که بشوق تو از سبو به قدح</p></div>
<div class="m2"><p>کنند و از قدحش باز در سبو ریزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهای می چو نباشد به کوی باده فروش</p></div>
<div class="m2"><p>سزد که از پی این آب آبرو ریزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کراست جرأت پرسش به محشر از خونی</p></div>
<div class="m2"><p>که تیر غمزه ی ترکان تندخو ریزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نماند در گل او رنگ و بو که رنگی نیست</p></div>
<div class="m2"><p>در آن سرشک که از یاد روی او ریزند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبر دهید بدلها (سحاب) آن خونها</p></div>
<div class="m2"><p>که از جفای وی از دیده ها فرو ریزند</p></div></div>