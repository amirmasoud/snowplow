---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>کی ز امتداد روز قیامت حذر کند</p></div>
<div class="m2"><p>هر کس که در فراق شبی را سحر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ناورد خدنگ خود از سینه ام مباد</p></div>
<div class="m2"><p>دل از شکاف سینه بر او یک نظر کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک ناله می کشم ز جفای تو در پی اش</p></div>
<div class="m2"><p>صد ناله ی دگر که مبادا اثر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن را که سر به خاک نیفتد ز تیغ تو</p></div>
<div class="m2"><p>از شرم چون به حشر سر از خاک بر کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این آب چشم خلق به کویش همیشه نیست</p></div>
<div class="m2"><p>وقتی رسد کز آتش آهم حذر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با داستان عشق تو فرزانه عاشقی</p></div>
<div class="m2"><p>کافسانه های هر دو جهان مختصر کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بی خبر ز خود کندم وقت دیدنش</p></div>
<div class="m2"><p>اول مرا ز مژده ی وصلش خبر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد اگر (سحاب) به دارای دادگر</p></div>
<div class="m2"><p>فریاد از جفای تو بیدادگر کند</p></div></div>