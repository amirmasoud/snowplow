---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>آنرا که درد عشق تو ره یافت در مزاج</p></div>
<div class="m2"><p>مرگ است یا وصال یکی زین دواش علاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملک حسن و ملک ملاحت تو آن شهی</p></div>
<div class="m2"><p>کالحق سزد اگر دهدت شاه مصر باج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهی تو وز طره ات اینک برخ کلف</p></div>
<div class="m2"><p>شاهی تو وز کاکلت اینک به فرق تاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژگان تو سنان و دو چشمان دو ترک مست</p></div>
<div class="m2"><p>زلف تو صولجان و دو پستان دو گوی عاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شه به شهر حسن تو باید زجان گذشت</p></div>
<div class="m2"><p>کآنجا بغیر جان نستانی زکس خراج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جان چو درد تست بدرمان چه حاجت است</p></div>
<div class="m2"><p>در دل چو زخم تست به مرهم چه احتیاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با جان همان کنی که کند برق با گیاه</p></div>
<div class="m2"><p>با دل همان کنی که کند سنگ با زجاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی او (سحاب) خواب نیاید به دیده ات</p></div>
<div class="m2"><p>گر از پرند بالش و از پرنیان دواج</p></div></div>