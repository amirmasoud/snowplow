---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>در حریم وصل تو از رشک کارم مشکل است</p></div>
<div class="m2"><p>کز تو گل بر دامن و از غیر خارم بر دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ز سرو خوش خرام چون تویی پا در گلم</p></div>
<div class="m2"><p>قمری از سروی که از شرم تو پایش در گل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون ز حسرت در تنم جوشد چو بینم هر کجا</p></div>
<div class="m2"><p>پنجهٔ صیدافگنی رنگین به خون بسمل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو میفزا از تغافل در دل آه حسرتم</p></div>
<div class="m2"><p>زانکه زآه غافل حسرت نصیبان غافل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریخت آن شوخ از نگاهی ای فلک خون (سحاب)</p></div>
<div class="m2"><p>هم تمنای تو، هم کام من از وی حاصل است</p></div></div>