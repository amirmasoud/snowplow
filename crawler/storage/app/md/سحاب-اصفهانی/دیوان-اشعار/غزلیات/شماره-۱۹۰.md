---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>از عاشقی نگشته دلت مهربان هنوز</p></div>
<div class="m2"><p>دل برده ای ز دست و کنی قصد جان هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سنگ پاسبان دگر پیکر تو ریش</p></div>
<div class="m2"><p>سنگ جفا تو را به کف پاسبان هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل در فغان ز دست ستم پیشه ای چو خود</p></div>
<div class="m2"><p>هر لحظه خلقی از ستمت در فغان هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دشمنی که با تو تواند نمی کند</p></div>
<div class="m2"><p>آگه نئی زحال و دل دوستان هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زیر بار عشق مرو همچو من که نیست</p></div>
<div class="m2"><p>دوش تو را تحمل بار گران هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آنکه همچون من شده راز دل تو فاش</p></div>
<div class="m2"><p>دل میبری زکف به نگاه نهان هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زآه (سحاب) ای بت نامهربان به تو</p></div>
<div class="m2"><p>او مهربان شده است و تو نامهربان هنوز</p></div></div>