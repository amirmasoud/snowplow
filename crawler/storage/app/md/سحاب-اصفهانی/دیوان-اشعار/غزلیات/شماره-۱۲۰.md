---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>آن شوخ را ز دوستیم تا خبر نبود</p></div>
<div class="m2"><p>هر لحظه دوستیش بمن بیشتر نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تدبیر اگر چه جز به دعای سحر نبود</p></div>
<div class="m2"><p>کردم بسی دعا و یکی را اثر نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد مهربان زناله دلش لیک با رقیب</p></div>
<div class="m2"><p>خوش آن زمان که ناله ی ما را اثر نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرزانه آنکه بر درد و نان نرفت و ساخت</p></div>
<div class="m2"><p>با لقمه ی جوینی اگر بود یا نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مه در حذر ز آه دلم بود دوش و باز</p></div>
<div class="m2"><p>آن ماه را ز آه دل من حذر نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بستگی نبود مرا تا به آشیان</p></div>
<div class="m2"><p>از تند باد حادثه زیر و زبر نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چیز کآیدت بنظر پرتوی ازوست</p></div>
<div class="m2"><p>خوش آن که هر چه دید جز او در نظر نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز راه کوی خویش (سحابت) در انتظار</p></div>
<div class="m2"><p>یک شب نشد که بر سر هر رهگذر نبود</p></div></div>