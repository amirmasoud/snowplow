---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>ز ناتوانی پیری اگر به جان آیی</p></div>
<div class="m2"><p>برو به میکده یک چند تا جوان آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بزم وصل من اغیار ره کجا یابد؟</p></div>
<div class="m2"><p>اگر ز چشم فلک در برم نهان آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر ز حسرت من در زوال حسن رخش</p></div>
<div class="m2"><p>شوی اگر به چمن موسم خزان آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن ضعیف فرو مانده زیر بار دلم</p></div>
<div class="m2"><p>ز بس که بر دلم ای بار غم گران آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شعله‌ها که مرا از زبان زبانه کشد</p></div>
<div class="m2"><p>چو ای حدیث غم هجر بر زبان آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه گویم اگر بینمت سپارم جان</p></div>
<div class="m2"><p>مگر که بر سرم از بهر امتحان آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همین نه سنگ بنالد ز ناله‌ام شاید</p></div>
<div class="m2"><p>اگر به پیش تو نالم تو در فغان آیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عاشقان بت ناسازگار ما سازد</p></div>
<div class="m2"><p>اگر تو بر سر سازش به آسمان آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحاب رشک گهی بر غریق عشق بری</p></div>
<div class="m2"><p>که خود به ساحل از این بحر بیکران آیی</p></div></div>