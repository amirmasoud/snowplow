---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>بس آنجا رفته بر خاک آرزویش</p></div>
<div class="m2"><p>سراسر آرزو شد خاک کویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نشناسمش چندان عجب نیست</p></div>
<div class="m2"><p>ز بس کم دیده ام از شرم رویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدلها جای او شد از چه هر دم</p></div>
<div class="m2"><p>به هر دل آتشی افگنده خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز درد رشک تا آسوده باشم</p></div>
<div class="m2"><p>نباید کرد هرگز جستجویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرنگ و بوی خود گل پیش بلبل</p></div>
<div class="m2"><p>کند شرم از گل خوش رنگ و بویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبار یکی آن موی میان بین</p></div>
<div class="m2"><p>چسان در پیچ و تاب افتاده مویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(سحاب) این درد را در دل دهد جای</p></div>
<div class="m2"><p>گر آب بحر گنجد در سبویش</p></div></div>