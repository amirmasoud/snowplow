---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>یار رقیب شد به فسون یار من دریغ</p></div>
<div class="m2"><p>جبرئیل گشت هم نفس اهرمن دریغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای مدعی که جان تو باشد به تن دریغ</p></div>
<div class="m2"><p>یار است با تو یار من از یار من دریغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری گذشت و یوسف من از وفا نکرد</p></div>
<div class="m2"><p>یکبار یاد ساکن بیت الحزن دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با یار تازه عهد نوی بسته یار من</p></div>
<div class="m2"><p>نشناخت قدر صحبت یار کهن دریغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بینم چگونه خلعت زیبای وصل یار؟</p></div>
<div class="m2"><p>بر قامتی که آیدم از وی کفن دریغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیهوده رفته ام سوی غربت ز غیرت آه</p></div>
<div class="m2"><p>گر دیده ام جدا زوطن از وطن دریغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوبان نشسته اند چو پروین به محفلم</p></div>
<div class="m2"><p>آن مه (سحاب) نیست درین انجمن دریغ</p></div></div>