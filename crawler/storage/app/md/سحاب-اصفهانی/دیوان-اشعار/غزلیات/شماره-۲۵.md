---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ماند آخر حسرت دیدار او در دل مرا</p></div>
<div class="m2"><p>منتی در دل نهاد این مرگ مستعجل مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بارها دل بر وفای خوبرویان بسته ام</p></div>
<div class="m2"><p>باز می خواهم که داند هر کسی عاقل مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفگند از دور هرگز ناوکی بر سینه ام</p></div>
<div class="m2"><p>تا بماند یادگار شست او در دل مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آب چشم خویش می بستم ره این کاروان</p></div>
<div class="m2"><p>گردو گامی ضعف بگذارد پی محمل مرا</p></div></div>