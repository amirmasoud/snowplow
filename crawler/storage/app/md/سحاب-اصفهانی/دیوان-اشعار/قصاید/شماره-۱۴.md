---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>هست روز واپسینم حسرت این</p></div>
<div class="m2"><p>کافتدم بر وی نگاه واپسین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشدم تا دامنش ناید به دست</p></div>
<div class="m2"><p>بر گریبان دست و بر چشم آستین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نباشد مانع آب دیده ام</p></div>
<div class="m2"><p>عالمی سوزد به آه آتشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفریده است ایزدش از جان پاک</p></div>
<div class="m2"><p>کآفرین بر جانش از جان آفرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفان را زلف او شد دام دل</p></div>
<div class="m2"><p>زاهدان را چشم او زد راه دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست با شهد لب آن نوش لب</p></div>
<div class="m2"><p>همچو حنظل تلخ طعم انگبین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصد قتلم دارد و زین غافل است</p></div>
<div class="m2"><p>کآرزوی من بود از وی همین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باید آنکو پا نهد در راه عشق</p></div>
<div class="m2"><p>بگذرد از جان به گام اولین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاجتی نبو به تیغ آنکو که هست</p></div>
<div class="m2"><p>ساعد سیمین و بازوی سمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاقبت جان گیرد از من یا ز نار</p></div>
<div class="m2"><p>یا بهای بوسه یار نازنین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دور از او شد درفشان مژگان من</p></div>
<div class="m2"><p>همچو نوک خامه ی فخر زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زینت دوران و زیب روزگار</p></div>
<div class="m2"><p>میرزای دهر زین العابدین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اختر برج سعادت آنکه هست</p></div>
<div class="m2"><p>منفعل خورشیدش از رای رزین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شوق نیل حضرتش دارد نیال</p></div>
<div class="m2"><p>تکیه بر خاک درش خواهد تکین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر زمین مهر فلک هر شامگاه</p></div>
<div class="m2"><p>از پی تعظیم او ساید جبین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حزم او اشرار را سدی سدید</p></div>
<div class="m2"><p>حفظ او احرار را حصنی حصین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می برد رشک آسمانش ز آستان</p></div>
<div class="m2"><p>چون بر آید دست جودش ز آستین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میهمانش از صریر در نیافت</p></div>
<div class="m2"><p>جز ندای فاد خلوها خالدین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنکه باعث خلقت ذات وی است</p></div>
<div class="m2"><p>ز امتزاج خاک و باد و ماء و طین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای زبانت مظهر سر خرد</p></div>
<div class="m2"><p>وی بیانت مظهر سحر مبین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باشدت دل مخزنی کآمد در آن</p></div>
<div class="m2"><p>در معنی، گوهر دانش دفین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشته از کلکت عطارد بهره یاب</p></div>
<div class="m2"><p>نیست آری خرمنی بی خوشه چین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با سحاب آن فرق دارد خامه ات</p></div>
<div class="m2"><p>کآن فشاند قطره این در ثمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون زمین جای تو شد نبود عجب</p></div>
<div class="m2"><p>بر سپهر از فخر نازد گر زمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دشمنان را قهر تو نار سقر</p></div>
<div class="m2"><p>دوستان را مهر تو ماء معین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مور اگر عون از تو خواهد کی شود</p></div>
<div class="m2"><p>هم نبرد از عار با شیر عرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نبندد دل به شوق خدمتت</p></div>
<div class="m2"><p>در رحم صورت کجا بندد جنین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>داغ فرمان تو را از ماه نو</p></div>
<div class="m2"><p>کرده خنگ آسمان زیب سرین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صاحبا امید گاها ای که باد</p></div>
<div class="m2"><p>توسن گردون ترا در زیر زین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چند ار دولت به کام غیر شد</p></div>
<div class="m2"><p>زین نباشد خاطرت اندوهگین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گه بود عزت گهی ذلت که هست</p></div>
<div class="m2"><p>رسم گیتی گه چنان گاهی چنین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دور گردون گر نگین جم نهاد</p></div>
<div class="m2"><p>چند روزی در کف دیو لعین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مدتی نگذشت از آن چندان که باز</p></div>
<div class="m2"><p>جم گرفت از دست اهریمن نگین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خواستم در چند بیتی ذم کنم</p></div>
<div class="m2"><p>دشمنان را از کهین و از مهین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیر عقلم گفت کز این داستان</p></div>
<div class="m2"><p>خامشی اولی است خاموشی گزین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کآنچه گوئی بیش از آنند این گروه</p></div>
<div class="m2"><p>لعنت الله علیهم اجمعین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قبله گاها بسکه دور از خدمتت</p></div>
<div class="m2"><p>جان بود اندوهناک و دل حزین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خویش را بندم به قید مشق خط</p></div>
<div class="m2"><p>تا ز بند غم رهد جان غمین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>لیک دارم جر و قر طاسی که هست</p></div>
<div class="m2"><p>زین یسارم در شکایت ز آن یمین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کاغذی بفرست با قدری مداد</p></div>
<div class="m2"><p>تا رهی را منتت دارد رهین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در سفیدی همچو قلب دوست آن</p></div>
<div class="m2"><p>در سیاهی همچو روی دشمن این</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در صفا چون روی ترکان ختا</p></div>
<div class="m2"><p>در صفت چون طره ی خوبان چین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>این چو مرآت سکندر صاف و آن</p></div>
<div class="m2"><p>همچو آب خضر با ظلمت قرین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این چو داغ لاله مشک آگین و آن</p></div>
<div class="m2"><p>در نزاکت همچو برگ یا سیمین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این چو رای جاهلان بی خرد</p></div>
<div class="m2"><p>آن چو عقل کاملان خرده بین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مظلم این چون خاطر ارباب شرک</p></div>
<div class="m2"><p>روشن آن چون باطن اهل یقین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر شود لطفت معینم ز آن دو چیز</p></div>
<div class="m2"><p>باد بختت ناصر و طالع معین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر فلک گرد زمین دارد مدار</p></div>
<div class="m2"><p>مهر و مه تا در شهور و در سنین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دوستت را پای بر فوق فلک</p></div>
<div class="m2"><p>دشمنت را جای در زیر زمین</p></div></div>