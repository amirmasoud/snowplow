---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>مگر زناز بر افشانده دلستان کاکل</p></div>
<div class="m2"><p>که عالمی است معطر زبوی آن کاکل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زلف وکاکل او بسته زان دل و جانم</p></div>
<div class="m2"><p>که بوی دل دهدش زلف و عطر جان کاکل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسان بید موله فراز لاله و گل</p></div>
<div class="m2"><p>به گلستان رخش گشته سایبان کاکل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیان شود زنقاب سحاب گوئی مهر</p></div>
<div class="m2"><p>چو در کله کند از روی خود نهان کاکل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه حاجتش به حریر و قصب که بر سرو بر</p></div>
<div class="m2"><p>پرند پوشش آن زلف و پرنیان کاکل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای چاه زنخدان او گرفت دلم</p></div>
<div class="m2"><p>زسادگی و به او داد ریسمان کاکل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرید گرد سرش مرغ دل بسی حیران</p></div>
<div class="m2"><p>گزید عاقبت از بهر آشیان کاکل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فرق کاکل او هر که دید گفت عیان</p></div>
<div class="m2"><p>که شد به فرق مه چارده عیان کاکل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در این چمن خردش سرو بوستان خواندی</p></div>
<div class="m2"><p>به فرق داشتی ار سرو بوستان کاکل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خط جوان به رخش خواند کاکلش را پیر</p></div>
<div class="m2"><p>به پیچ و تاب فتادش به فرق زان کاکل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیچ و تابم چون موی او که رویش را</p></div>
<div class="m2"><p>گرفته تنگ به بر زلف و در میان کاکل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حدیث زلفش نگفت شانه از غیرت</p></div>
<div class="m2"><p>نهاد ناگهش انگشت بر دهان کاکل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر جود محمد حسین خان که سزد</p></div>
<div class="m2"><p>به دستش ابلق چرخ سبکعنان کاکل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عبیر ریزد از کاکلش مگر سوده است</p></div>
<div class="m2"><p>به خاک پای سمند خدایگان کاکل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلاوری که به فرقش همی بر افشاند</p></div>
<div class="m2"><p>لوای فتح به هر رزم چتر سان کاکل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر کرام که بر فراق او همی نازد</p></div>
<div class="m2"><p>کلاه مجد چو بر فرق دلبران کاکل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا رفیع جنابی که آسمان ساید</p></div>
<div class="m2"><p>چو بندگان شب و روزت بر آستان کاکل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو تا به فرق زمین پا نهاد ه ای زیبد</p></div>
<div class="m2"><p>که از تفاخر سر ساید بر آستان کاکل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زیمن تربیتت روید از زمین سنبل</p></div>
<div class="m2"><p>اگر فشاند دهقان به خاکدان کاکل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدایگانا چو من به طنز یاری گفت</p></div>
<div class="m2"><p>قصیده ئیست فلان را ردیف آن کاکل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قصیده نغزو مردف بسی توان گفتن</p></div>
<div class="m2"><p>ردیف کردنش اما نمیتوان کاکل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من این قصیده به مدح تو گفتم و کردم</p></div>
<div class="m2"><p>ردیف آنهمه از بهر امتحان کاکل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زبس به ناخن فکرت زدم به کاکل چنگ</p></div>
<div class="m2"><p>چو تار چنگ زدستم کند فغان کاکل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا که به بزم است عنبر افشان زلف</p></div>
<div class="m2"><p>مدام تا که به رزم است خونچکان کاکل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ولیت را بود از غالیه چو سنبل زلف</p></div>
<div class="m2"><p>عدوت را بود از خون چو ارغوان کاکل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دو نوگلند که روشن ستاره اند و بود</p></div>
<div class="m2"><p>همیشه پاک زگرد ملالشان کاکل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی رباید از تارک سما اکلیل</p></div>
<div class="m2"><p>یکی گشاید از فرق فرقدان کاکل</p></div></div>