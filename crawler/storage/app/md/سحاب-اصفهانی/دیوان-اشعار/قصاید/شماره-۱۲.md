---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>صبح بنمود رخ از چاه افق چون بیژن</p></div>
<div class="m2"><p>دیده ی رستم گردون ز رخش شد روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو روم به سر مغفر رومی چو نهاد</p></div>
<div class="m2"><p>چاک زد شاه حبش جوشن سیمین بر تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز افروخت یکی نیزه ی سیمینه سنان</p></div>
<div class="m2"><p>چرخ افراشت یکی خیمه ی زرینه رسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد عیان موکب سلطان خور از بهر نثار</p></div>
<div class="m2"><p>گشت گنجور فلک را تهی از زر مخزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا تو گوئی که درر ریخت از این نیلی ابر</p></div>
<div class="m2"><p>یا تو گوئی که گهر بیخت ازین پرویزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داد از محفل جم بزم جهان یاد از آنک</p></div>
<div class="m2"><p>جام جمشید برون آمد از این نیلی دن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا سلیمان فلک کردت گر باره به دست</p></div>
<div class="m2"><p>خاتمی را که زدستش بربود اهریمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن اثر باد سحر کرد به گل های نجوم</p></div>
<div class="m2"><p>باز تا صبح کند بزم جهان را روشن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز تا صبح دهد بزم جهان را زینت</p></div>
<div class="m2"><p>که به هنگام خزان باد خزان در گلشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لعل و کان باده بر آورد ز مینایی خم</p></div>
<div class="m2"><p>بستد این شمع و بر افروخت به فیروزه لگن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کسی چید بساط طرب و محفل عیش</p></div>
<div class="m2"><p>هر کسی شد به تماشای گل و گشت چمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشت در باغ مقام همه چه شیخ و چه شاب</p></div>
<div class="m2"><p>شد به گلزار مکان همه چه مرد و چه زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاشقی نه که نه یاری بودش در پهلو</p></div>
<div class="m2"><p>شاهدی نه که نه دستی بودش در گردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لعل گون جام کشید این به سرود بربط</p></div>
<div class="m2"><p>ارغوانی قدح این زد به نوای ارغن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک طرف ماهرخی دست زنان رقص کنان</p></div>
<div class="m2"><p>یک طرف زهره وشی نغمه سرا دستان زن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش از این زمزمه ی فاخته چون نوحه ی زاغ</p></div>
<div class="m2"><p>بر آن جلوه ی طاووس چو رفتار زغن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من به کنجی به فغان از ستم چرخ که او</p></div>
<div class="m2"><p>چند با من ستمش پیشه بود کینش فن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیده ام ز اشک پیاپی به زمین قطره فشان</p></div>
<div class="m2"><p>سینه ام ز آه دمادم به فلک شعله فکن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه سوزان تنم از کثرت آلام و غموم</p></div>
<div class="m2"><p>گاه نالان دلم از فرط بلایا و محن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گاه در این غم جانسوز که تا چند کند</p></div>
<div class="m2"><p>آتش فرقت احباب به جان و دل من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنچه هرگز نکند آتش سوزان به گیاه</p></div>
<div class="m2"><p>آنچه هرگز نکند برق یمان با خرمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گاه دل کرد تمنای سروری که مرا</p></div>
<div class="m2"><p>بلکه جانی به تن افزاید و روحی به بدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گاه اندیشه به امید نشاطی که از آن</p></div>
<div class="m2"><p>یکزمان جان حزین را برهاند ز حزن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ناگهان سوی من از خطه ی جان پرور ریزد</p></div>
<div class="m2"><p>قاصدی آمد چون باد صبا از گلشن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قاصدی دیدن آن هوش رباینده ز سر</p></div>
<div class="m2"><p>قاصدی طلعت آن روح فزاینده به تن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جیبش از نامه ز بس گشته معطر گوئی</p></div>
<div class="m2"><p>که نسیمی است گذر کرده به صحرای ختن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نامه ای داد که شد خاطر زارم خرم</p></div>
<div class="m2"><p>نامه ای داد که شد دیده ی تارم روشن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نامه نه عقد گهر ریخته بر صفحه ی سیم</p></div>
<div class="m2"><p>نامه نه عنبرتر بیخته بر برگ سمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیده ام دید از آن نامه ضیائی که ندید</p></div>
<div class="m2"><p>دیده ی ساکن بیت الحزن از پیراهن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هم به غیرت خط مشکین نگارش ز خطوط</p></div>
<div class="m2"><p>هم به خجلت شکن طره ی یارش ز شکن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه حرفش به نظر نافه ای از مشک ختا</p></div>
<div class="m2"><p>همه سطرش به مثل رشته ای از در عدن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صفحه اش حجله و الفاظ لطیفش هر یک</p></div>
<div class="m2"><p>نو عروسی به معانی دقیق و روشن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خط او چون خط خوبان و نگارنده ی آن</p></div>
<div class="m2"><p>فخر ارباب ذکا مفخر اصحاب فطن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>میرزای متعالی نسب احمد که بود</p></div>
<div class="m2"><p>سر احرار زمان سرور اشراف ز من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنکه باشد کف او در سخا را دریا</p></div>
<div class="m2"><p>آنکه آمد دل او بحر عطا را مخزن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آنکه هنگام سخط کوه شود از بیمش</p></div>
<div class="m2"><p>آنچنان نرم که در پنجه ی داوود آهن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>با ضمیرش نبرد مهر منیر اسم ضیا</p></div>
<div class="m2"><p>با کلامش نکند در ثمین یاد ثمن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم بیان خرد از ذکر مدیحش عاجز</p></div>
<div class="m2"><p>هم زبان قلم از شرح بیانش الکن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو شود پرتو رایش به سها شعشعه بخش</p></div>
<div class="m2"><p>چو شود ابر عطایش به دمن سایه فکن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پرتو مهر شود منفعل از نورسها</p></div>
<div class="m2"><p>خضرت ربع کند شرم ز خضرای دمن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بس گهر زای تراز بحر بود در نیسان</p></div>
<div class="m2"><p>بس درر بارتر از ابر بود در بهمن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>طبع او گاه سخاوت کف او گاه کرم</p></div>
<div class="m2"><p>کلک او گاه نگارش لب او گاه سخن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر غرض بذل کف بحر نظیرش نبود</p></div>
<div class="m2"><p>کش به هنگام سخا بحر نشاید گفتن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پرورش لعل بدخش ار چه پذیرد به بدخش</p></div>
<div class="m2"><p>تربیت در عدن ارچه گزیند به عدن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ای گشوده به چمن لاله به بستان سوسن</p></div>
<div class="m2"><p>به دعای تو زبان و به ثنای تو دهن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای فلک قدر فلک مرتبه ای کز مه نو</p></div>
<div class="m2"><p>طوق فرمان تو را کرده فلک در گردن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای که جاه تو عروسی است کز آراستگی</p></div>
<div class="m2"><p>در خور زیور گوشش نبود عقد پرن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به خدائی که نهال قد خوبان شد از او</p></div>
<div class="m2"><p>ثمر آور به ترنج ز نخ و سیب ذقن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به طراوت ده رخسار نکویان جوان</p></div>
<div class="m2"><p>به ضیابخش دل خسته ی پیران کهن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به غبار قدم و خاک درت کآمده اند</p></div>
<div class="m2"><p>توتیائی که از آن چشم خرد شد روشن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که جدا تا شده ام از سر کوی تو بود</p></div>
<div class="m2"><p>ساحت گلشن دهرم به نظر چون گلخن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اشک من گشته گهربار تر از ابر مطیر</p></div>
<div class="m2"><p>آه من گشته شرر بار تر از برق یمن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اشک سرخم به رخ زرد بدآنسان پیدا</p></div>
<div class="m2"><p>که همی رویت از برگ زریری ریون</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر کس آری ز تو شد دور مدامش دل و جان</p></div>
<div class="m2"><p>تیر غم راست هدف تیغ بلا راست مجن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>و آن که آمد به پناهت بود ایمن که بود</p></div>
<div class="m2"><p>کلبه ی وادی ایمن ز حوادث ایمن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هر که روزی به دیار تو نماید منزل</p></div>
<div class="m2"><p>هر که چندی به جناب تو گزیند مسکن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نیست خوشدل، شودش گرچه به جنت ماوا</p></div>
<div class="m2"><p>نیست خرم، شودش گرچه به فردوس وطن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به که کوشم به دعای تو که تطویل کلام</p></div>
<div class="m2"><p>نبود در بر ارباب خرد مستحسن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تا که بهر امن و بیجاده ز تاثیر نجوم</p></div>
<div class="m2"><p>پرورش داده به گل تربیت اندر معدن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اشک اعدای تو از بیم تو چون بیجاده</p></div>
<div class="m2"><p>روی احباب تو از لطف تو بهر امن</p></div></div>