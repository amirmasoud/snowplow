---
title: >-
    بخش ۹۲
---
# بخش ۹۲

<div class="b" id="bn1"><div class="m1"><p>آه از این درد که از عشق مرا در جان است</p></div>
<div class="m2"><p>این چه سوزی است که در سینه مرا پنهان است</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>در دلم آتش جوع از هوس بریان است</p></div>
<div class="m2"><p>دل من در رخ جان پرور نان حیران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرهم درد دل من نبود جز کشکک</p></div>
<div class="m2"><p>«این چه دردی است که در سینه مرا پنهان است»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست بی یاد برنج و حبشی یک نفسم</p></div>
<div class="m2"><p>ور بر آید به من دل شده صد تاوان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که جان داد و دلی، بره بریان بخرید</p></div>
<div class="m2"><p>گو غنیمت شمر امروز که بس ارزان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو اطعمه ها گوشت بود در عالم</p></div>
<div class="m2"><p>باد پاینده در آفاق چو او سلطان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش می برد یکی صحن برنجی ز غمش</p></div>
<div class="m2"><p>وه که امروز چو پالوده دلم لرزان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوی حلوای برنج و نخوداب آمد دوش</p></div>
<div class="m2"><p>صوفی امروز ازین واقعه سرگردان است</p></div></div>