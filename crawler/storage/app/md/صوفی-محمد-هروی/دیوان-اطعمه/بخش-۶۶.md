---
title: >-
    بخش ۶۶
---
# بخش ۶۶

<div class="b" id="bn1"><div class="m1"><p>چو بیابی به دهر یار خلف</p></div>
<div class="m2"><p>ننهی جام باده را از کف</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>چون برآید ز دیگ حلوا تف</p></div>
<div class="m2"><p>لوت خواران کشند صف در صف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرم آن ساعتی که باشم شاد</p></div>
<div class="m2"><p>من و بغرا نهاده کف بر کف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوقها یافتم ز صحن برنج</p></div>
<div class="m2"><p>راحت خاطر است یار خلف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که بی بی نجست و لوت زدن</p></div>
<div class="m2"><p>می کند در زمانه عمر تلف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بانگ نان تنک ربود غمم</p></div>
<div class="m2"><p>سبب عیش باشد آری دف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم از دور گفت با بریان</p></div>
<div class="m2"><p>هست ما را ز تو امید شرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت صوفی سحر به نان و نمک</p></div>
<div class="m2"><p>«ما عرفناک حقه اعرف»</p></div></div>