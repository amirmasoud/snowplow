---
title: >-
    بخش ۸۹
---
# بخش ۸۹

<div class="b" id="bn1"><div class="m1"><p>سر بلندی بین که دایم در سرم سودای اوست</p></div>
<div class="m2"><p>قیمت هر کس به قدر همت والای اوست</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>تا به بریانی که صد جان در جهان شیدای اوست</p></div>
<div class="m2"><p>«سر بلندی بین که دایم در سرم سودای اوست»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون هریسه فارغ است از روغن و سوز درون</p></div>
<div class="m2"><p>زان همه فریاد مشتاقان ز استغنای اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتنه آن گرده نانم که از سودای او</p></div>
<div class="m2"><p>در میان شهر در هر گوشه ای غوغای اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رخ پالوده گر باشد هزاران خط و خال</p></div>
<div class="m2"><p>از هوای گرده گندم که را پروای اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز شوق دنبه بر زناج گشتی مبتلی</p></div>
<div class="m2"><p>در بلا تسلیم شو، کان هم ز مرهمهای اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دل من، بنده بریان شد و غم حاصل است</p></div>
<div class="m2"><p>مایه شادی همه از دولت غمهای اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ز وصل نان و بریان صوفی مسکین جداست</p></div>
<div class="m2"><p>گوئیا غمهای عالم بر تن تنهای اوست</p></div></div>