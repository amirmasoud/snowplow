---
title: >-
    بخش ۱۸
---
# بخش ۱۸

<div class="b" id="bn1"><div class="m1"><p>بوسی زلبش اگر کنی نوش</p></div>
<div class="m2"><p>غمهای جهان شود فراموش</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>گر آرده و عسل کنی نوش</p></div>
<div class="m2"><p>عیش دو جهان کنی فراموش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان تازه کند بیا و بنگر</p></div>
<div class="m2"><p>آن اشکنه لطیف سر جوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون از تو شود کسی خریدار</p></div>
<div class="m2"><p>یک نان به دو عالمش بمفروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بریان که عروس اطعمه اوست</p></div>
<div class="m2"><p>باشد که بگیرمش در آغوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بوی جگر کباب بازار</p></div>
<div class="m2"><p>برد از دل خسته طاقت و هوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن دانه درکه هست انگور</p></div>
<div class="m2"><p>بی او منشین، بگیر در گوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من آب برای شرب صوفی</p></div>
<div class="m2"><p>بر دوش کشیده بوده ام دوش</p></div></div>