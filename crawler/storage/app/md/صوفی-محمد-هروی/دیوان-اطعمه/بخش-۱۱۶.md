---
title: >-
    بخش ۱۱۶
---
# بخش ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>دلم به درد و غم عشق مبتلی تا کی</p></div>
<div class="m2"><p>ز شوق زلف تو در دام صد بلا تا کی</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>مرا به سینه تمنای ماس وا تا کی</p></div>
<div class="m2"><p>دلم به صحنک پالوده مبتلی تا کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون به دهر چو ماقوت نیست صابونی</p></div>
<div class="m2"><p>به سینه ولوله شوق زلبیا تا کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدند جلبک و روغن به تابه اندر تحت</p></div>
<div class="m2"><p>میان عاشق و معشوق ماجرا تا کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حد گذشت بیا مطبخی کرم فرما</p></div>
<div class="m2"><p>بگوی در دل من شوق زیروا تا کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ربوده قامت زناج جمله دلها را</p></div>
<div class="m2"><p>میان خلق ندانم که این بلا تا کی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوای مرغ بود خوش به گلشن منقل</p></div>
<div class="m2"><p>بگوی نان تنک سفره بی نوا تا کی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به درد جوع اسیرست صوفی مسکین</p></div>
<div class="m2"><p>مقیم در شب و روز از پی دوا تا کی</p></div></div>