---
title: >-
    بخش ۹۷
---
# بخش ۹۷

<div class="b" id="bn1"><div class="m1"><p>تا بدین غایت که رفت از من نیامد هیچ کار</p></div>
<div class="m2"><p>راستی باید، نه بازی صرف کردم روزگار</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>بهر بغرا در جهان هر کس نهد دیگی به بار</p></div>
<div class="m2"><p>یارب این توفیق را گردان رفیق، ای کردگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی بود یارب که در دستم فتد بریانیی</p></div>
<div class="m2"><p>تا من تنها بر آرم از دل و جانش دمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که بریانهای فربه چون به چنگ افتد مرا</p></div>
<div class="m2"><p>بهر کنگر ماس باشد در دل من خوار خوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فی المثل در معده ام گر جا نماند یک نفس</p></div>
<div class="m2"><p>همچنان با صحن بغرا دل بود امیدوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد مرگم ای که خواهی داد حلوائی به کس</p></div>
<div class="m2"><p>هم به من ده این زمان اکنون که هستم در دیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کارم از یک کاسه بغرا نمی گردد تمام</p></div>
<div class="m2"><p>باری از دفع ضرورت کمترینه دو تغار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ندارد صوفی مسکین به غیر اشتها</p></div>
<div class="m2"><p>می گذارد این سخنها را به عالم یادگار</p></div></div>