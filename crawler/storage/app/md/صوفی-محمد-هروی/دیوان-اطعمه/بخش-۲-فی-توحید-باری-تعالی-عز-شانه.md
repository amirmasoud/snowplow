---
title: >-
    بخش ۲ - فی توحید باری تعالی عز شانه
---
# بخش ۲ - فی توحید باری تعالی عز شانه

<div class="b" id="bn1"><div class="m1"><p>من شکسته گشایم به صد هزار زبان</p></div>
<div class="m2"><p>ثنای حضرت ذات خدای کون و مکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقسمی که فکندست سفره روزی</p></div>
<div class="m2"><p>زلطف عام خود از قاف تا به قاف عیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زسنگ کرده پدیدار آب بی کیفی</p></div>
<div class="m2"><p>زخاک تیره هزاران هزار گرده نان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چوب خشک که آتش بود برو اولی</p></div>
<div class="m2"><p>ببین که میوه پدیدار کرد چند الوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فسرده کرد جهان را و یخ پدید آورد</p></div>
<div class="m2"><p>برای شربت و آب خنک به تابستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عسل پدید کند او ز نیش زنبوری</p></div>
<div class="m2"><p>ز شاخ نیشکر و قند کرده است عیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من فقیر چگویم به این زبان که مراست</p></div>
<div class="m2"><p>کدام نعمت او را در آورم به بیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو درد جوع نهان است روز و شب به دلم</p></div>
<div class="m2"><p>به ذکر اطعمه امروز می کنم درمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه بیان نعمهای او کند صوفی</p></div>
<div class="m2"><p>که ذکر عیش بود نصف عیش در دوران</p></div></div>