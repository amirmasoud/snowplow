---
title: >-
    بخش ۵۲
---
# بخش ۵۲

<div class="b" id="bn1"><div class="m1"><p>چه گونه عرضه کنم با تو داستان فراق</p></div>
<div class="m2"><p>قلم مگر بدهد شرح از زبان فراق</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>فتاده در سر من تا هوای آش سماق</p></div>
<div class="m2"><p>بجز خیال وی این دم نمی پزم به وثاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر اطعمه چون در سرم خیالی نیست</p></div>
<div class="m2"><p>نصیب من «چه کنم»، این شدست در میثاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوس کند من بیچاره را، چه چاره کنم</p></div>
<div class="m2"><p>برنج و روغن زرد این زمان چو اهل نفاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تلاش پشت کنم روز دعوت سلطان</p></div>
<div class="m2"><p>اگر ز پهلوی من بگذرد هزار چماق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل شکسته، ندارد هوای نان شعیر</p></div>
<div class="m2"><p>سخن درست بگویم مرا چو نیست نفاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمال کله بریان به شهر اگر نبود</p></div>
<div class="m2"><p>برآید از دل بیچاره جان ز عین فراق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ذکر عیش بود نصف عیش صوفی را</p></div>
<div class="m2"><p>به غیر اطعمه ننوشت اندر این اوراق</p></div></div>