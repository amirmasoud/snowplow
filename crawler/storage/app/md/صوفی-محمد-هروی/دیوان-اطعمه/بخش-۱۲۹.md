---
title: >-
    بخش ۱۲۹
---
# بخش ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>عکس روی تو چو در آینه جام افتاد</p></div>
<div class="m2"><p>عاشق سوخته دل در طمع خام افتاد</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>دوش در خانه مرا نان جوین شام افتاد</p></div>
<div class="m2"><p>چه کنم هیچ جز از صبر که ناکام افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحن کاچی به دو صد درد دل آمد به شکم</p></div>
<div class="m2"><p>رنجه گردد چو کسی در طمع خام افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راز سر بسته سنبوسه نگفتم با کس</p></div>
<div class="m2"><p>چه کنم آه که اندر دهن عام افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخ رویی کلنبه همه از حلوا بود</p></div>
<div class="m2"><p>زعفران نقش همین داشت که بدنام افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کند گر نرود در پی نان، دل شب و روز</p></div>
<div class="m2"><p>چو ازین دایره در محنت ایام افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رشته بگسیخت دل و در بر ماهیچه گریخت</p></div>
<div class="m2"><p>گر از آن بند بشد، باز درین دام افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاسه اشکنه ای داد به صوفی طباخ</p></div>
<div class="m2"><p>این گدا بین که چه شایسته انعام افتاد</p></div></div>