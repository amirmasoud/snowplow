---
title: >-
    بخش ۱۱۵
---
# بخش ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>هر که او دیده چهره یارم</p></div>
<div class="m2"><p>می کد رحم بر دل زارم</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>آن چنان من زگشنگی زارم</p></div>
<div class="m2"><p>که به جان گرده را خریدارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت نان ز آتش فراق کباب</p></div>
<div class="m2"><p>در تف و تاب رو به دیوارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشت خلقی فراقت ای بریان</p></div>
<div class="m2"><p>گفت از آن روی بر سر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نان و بریان خوش است و کنگر ماس</p></div>
<div class="m2"><p>گل به آن خار آرزو دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بره ای گر نباشد اندر پیش</p></div>
<div class="m2"><p>هرگز او را ز شام نشمارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوع را چون نهان کنم در دل</p></div>
<div class="m2"><p>چون گواهند هر دو رخسارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صوفیا گر نمی کنی باور</p></div>
<div class="m2"><p>فهم کن شمه ای ز گفتارم</p></div></div>