---
title: >-
    بخش ۸۷
---
# بخش ۸۷

<div class="b" id="bn1"><div class="m1"><p>واعظان کاین جلوه در محراب و منبر می کنند</p></div>
<div class="m2"><p>چون به خلوت می روند آن کار دیگر می کنند</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>تا به مطبخ همدمان بغرا مخمر می کنند</p></div>
<div class="m2"><p>از شمیم قلیه عالم را معطر می کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکلی دارم بپرسید این زمان از مطبخی</p></div>
<div class="m2"><p>تا چرا در قلیه رنگین چغندر می کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده ای باشد حجاب اندر میان دوستان</p></div>
<div class="m2"><p>بهر قیمه زان نخودها را مقشر می کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسم گل چون بر سر شاخ است، اصحاب خرد</p></div>
<div class="m2"><p>صحن بغرا را از آن رو قلیه بر سر می کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تسلی می شود مشتاق را بر روی خوان</p></div>
<div class="m2"><p>صورت مرغان و ماهی را مصور می کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلغلی در خانقه افتاده گفتم چیست گفت</p></div>
<div class="m2"><p>لوت خوارانند، شعر صوفی از بر می کنند</p></div></div>