---
title: >-
    بخش ۷۹
---
# بخش ۷۹

<div class="b" id="bn1"><div class="m1"><p>کتابه ای ز مسیحا بر این کهن دیرست</p></div>
<div class="m2"><p>که نا امید نباشی که عاقبت خیرست</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>دلم که در پی بغرا همیشه در سیر است</p></div>
<div class="m2"><p>چه پخته کار غنیمی که داعی خیرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل گرسنه من دوش گفت با بریان</p></div>
<div class="m2"><p>درآ در آی که این خانه خالی از غیرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا رود سوی مطبخ روان دل من گفت</p></div>
<div class="m2"><p>به دانه های برنج او اسیر چون طیرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روم به خدمت خباز، زان که هر روزی</p></div>
<div class="m2"><p>به نو چو خوردن نان رسم این کهن دیرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب مدار که صوفی رسد به دعوت عام</p></div>
<div class="m2"><p>که از برای همین کار خاصه در سیرست</p></div></div>