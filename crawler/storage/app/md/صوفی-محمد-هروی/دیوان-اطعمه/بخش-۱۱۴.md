---
title: >-
    بخش ۱۱۴
---
# بخش ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>کسی که دیده بود آن نگار رعنا را</p></div>
<div class="m2"><p>عجب عجب که ملامت کند دل ما را</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>در آن زمان که کنی ساز دیگ بغرا را</p></div>
<div class="m2"><p>ز غم خلاص کنی گشنه های شیدا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی صحنک پالوده چیست این آرا</p></div>
<div class="m2"><p>«به خط و خال چه حاجت جمال زیبا را»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نان گرم شود زنده این دل مرده</p></div>
<div class="m2"><p>ببین به صورت او معنی مسیحا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دعوتی چو رسیدی بیا غنیمت دان</p></div>
<div class="m2"><p>به کس نداد چو چرخ اختیار فردا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا به صحبت سنبوسه ره برد آسان</p></div>
<div class="m2"><p>به فکر، کس بگشاید مگر معما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عمر خود ننهد او بخور بر آتش</p></div>
<div class="m2"><p>کسی که بشنود از دور بوی گیپا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کاینات برابر نمی کند صوفی</p></div>
<div class="m2"><p>جمال گرده بریان و شیر خرما را</p></div></div>