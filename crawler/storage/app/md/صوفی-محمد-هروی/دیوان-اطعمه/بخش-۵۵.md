---
title: >-
    بخش ۵۵
---
# بخش ۵۵

<div class="b" id="bn1"><div class="m1"><p>در سرم تا هوای جانان است</p></div>
<div class="m2"><p>دلم از اشتیاق بریان است</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>در سرم تا هوای بریان است</p></div>
<div class="m2"><p>دیده ام چون کباب گریان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برکشیدست گردن از صحنک</p></div>
<div class="m2"><p>مرغ و در نان میده حیران است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد جوعی که هست در دل من</p></div>
<div class="m2"><p>نان شمسی و کله درمان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جگرم شد کباب ار پرسی</p></div>
<div class="m2"><p>دل بریان که راحت جان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم از شوق صحنک فرنی</p></div>
<div class="m2"><p>همچو پالوده آه لرزان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد روغن برنج را پامال</p></div>
<div class="m2"><p>خاطر او ازین پریشان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کسی را هوای اطعمه ای است</p></div>
<div class="m2"><p>دل صوفی به تابه بریان است</p></div></div>