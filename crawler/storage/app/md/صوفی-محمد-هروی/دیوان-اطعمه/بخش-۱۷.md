---
title: >-
    بخش ۱۷
---
# بخش ۱۷

<div class="b" id="bn1"><div class="m1"><p>عید صیام و فصل گل و موسم بهار</p></div>
<div class="m2"><p>من مست و یار همدم و ساقی است گلعذار</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>بیا که عید صیام است و باز فصل بهار</p></div>
<div class="m2"><p>غم زمانه بدل کن به باده گلنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن زمان که گشایم من فقیر نهار</p></div>
<div class="m2"><p>ترید شیر و عسل جوش بایدم دو طغار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برنج زرد چو یابی بنوش چندانی</p></div>
<div class="m2"><p>که در شکم نبود جای یک نفس زنهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیحتی کنمت چون رسی به دعوت عام</p></div>
<div class="m2"><p>مدار شرم، چو مردان در آی اندر کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سفره آنچه بود نوش کن تو از تر و خشک</p></div>
<div class="m2"><p>برای زله کشی نیز سفره را بردار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو یار همدم و یک مسلخی ز بریانی</p></div>
<div class="m2"><p>خوش است اگر نشود ناکسی بدین دوچار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکم چو سیر طعام است اندکی باید</p></div>
<div class="m2"><p>بیار خواجه ز انگور مسکه یک خروار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به از حیات، ز پر خوردن ار بمیرد کس</p></div>
<div class="m2"><p>به نزد صوفی بیچاره این زمان صد بار</p></div></div>