---
title: >-
    بخش ۹۶
---
# بخش ۹۶

<div class="b" id="bn1"><div class="m1"><p>فضل خدای را که تواند شمار کرد</p></div>
<div class="m2"><p>یا کیست آن که شکر یکی از هزار کرد</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس که دیگ قلیه برنجی به بار کرد</p></div>
<div class="m2"><p>فضل خدای را به جهان آشکار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگ هریسه آن که به شب کرد سر به دم</p></div>
<div class="m2"><p>دم خور تو ای عزیز که فکر نهار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل مشو ز پختن بغرا که گفته اند</p></div>
<div class="m2"><p>«مزد آن گرفت جان برادر که کار کرد »</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کو نپخت کله وگیپا امید داشت</p></div>
<div class="m2"><p>دانه نکشت ابله و دخل انتظار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانم ز عیش هر دو جهان گشت بی خبر</p></div>
<div class="m2"><p>روزی که صحن قلیه برنجی دو چار کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرهیز کن زگشنگی و گرده شعیر</p></div>
<div class="m2"><p>جنت چو جای مردم پرهیزگار کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صوفی اگر چه اطعمه گوید مکن تو عیب</p></div>
<div class="m2"><p>گویم بیان نعمت پروردگار کرد</p></div></div>