---
title: >-
    بخش ۱۲۷
---
# بخش ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>چراغ روی ترا شمع گشته پروانه</p></div>
<div class="m2"><p>مرا ز حال تو با جان خویش پروا نه</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>مرا که در دل و جان آرزوی بریانه</p></div>
<div class="m2"><p>دو دیده در غم نان چون کباب گریانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنیده ای صفت اشتیاق مجنون را</p></div>
<div class="m2"><p>مرا به کله و گیپا هزار چندانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زشوق حلقه زنجیر زلبیا امروز</p></div>
<div class="m2"><p>بیا ببین که دلم گشته است دیوانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای خوشه انگور تا مراست به سر</p></div>
<div class="m2"><p>نمی کند دل من میل سوی دردانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اشک و سوز دمادم که دیده از بریان</p></div>
<div class="m2"><p>برنج از آن سبب این دم چنین پریشانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجاست صحنک پالوده این زمان یارب</p></div>
<div class="m2"><p>که مرهم دل ریش است و راحت جانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر ز دار جهان رفت صوفی مسکین</p></div>
<div class="m2"><p>هنوز در سر او آرزوی بریانه</p></div></div>