---
title: >-
    بخش ۴۵
---
# بخش ۴۵

<div class="b" id="bn1"><div class="m1"><p>یارب آن روی است یا قرص قمر</p></div>
<div class="m2"><p>یارب آن لبهاست یا شهد و شکر</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>گرده های میده و ماس بقر</p></div>
<div class="m2"><p>گر به صد جان می فروشندش بخر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرهم دلهای ریش صائمان</p></div>
<div class="m2"><p>هست نان گرم و حلوای شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با پنیر خشک می گفت آن یکی</p></div>
<div class="m2"><p>«یارب این روی است یا قرص قمر»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند نازی کله از گیپای خود</p></div>
<div class="m2"><p>از برنج و قلیه هستی بی خبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو پسته در دل من گشته خشک</p></div>
<div class="m2"><p>آرزوی صحنک بادام تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغ بریان سر برآورد از برنج</p></div>
<div class="m2"><p>می کند در چهره نانها نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه پر خوردن بود عیب تمام</p></div>
<div class="m2"><p>صوفیان را هست این عین هنر</p></div></div>