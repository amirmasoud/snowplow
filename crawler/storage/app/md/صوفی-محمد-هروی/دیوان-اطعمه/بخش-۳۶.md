---
title: >-
    بخش ۳۶
---
# بخش ۳۶

<div class="b" id="bn1"><div class="m1"><p>خاطرت هست چو برگ گل نسرین نازک</p></div>
<div class="m2"><p>نکشم آه و شد اندر دل من جور تو لک</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>نان که چون خاطر یارست درین سفره تنک</p></div>
<div class="m2"><p>ای عزیزان گرانمایه بنوشید سبک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشت را زود بدرید چو دل از دشمن</p></div>
<div class="m2"><p>چو تنی خسته بدخواه کنیدش جک جک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوربا را که بود گرم چو دلهای محب</p></div>
<div class="m2"><p>همچو دیدار رقیبانش مسازید خنک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحن پالوده که لرزان چو دل من باشد</p></div>
<div class="m2"><p>هست شیرین چو لب لعل نگار چابک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لشکر اطعمه چون صف بکشد در میدان</p></div>
<div class="m2"><p>می زن از هر طرفش همچو سوار چابک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم از بیم که نان کم نشود در سفره</p></div>
<div class="m2"><p>می کند این دل سودازده من تک تک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر سفره چو همکاسه شوی با صوفی</p></div>
<div class="m2"><p>بس که از کینه اشکم تو بگریی جک جک</p></div></div>