---
title: >-
    بخش ۴۸
---
# بخش ۴۸

<div class="b" id="bn1"><div class="m1"><p>دارم هوس جمال بغرا</p></div>
<div class="m2"><p>دورم چو من از وصال بغرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند پزیم روز تا شام</p></div>
<div class="m2"><p>در دیگ هوس خیال بغرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مالیده شد او، به خویش پیچید</p></div>
<div class="m2"><p>ماهیچه ز انفعال بغرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بعد ثنای گوشت گویم</p></div>
<div class="m2"><p>گر قلیه بود کمال بغرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خلوت دیگ جز نخود کس</p></div>
<div class="m2"><p>واقف نشود ز حال بغرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افتاده به دست عام مسکین</p></div>
<div class="m2"><p>در روز و شبان و بال بغرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیبش نکنی که گشته مسکین</p></div>
<div class="m2"><p>صوفی فقیر لال بغرا</p></div></div>