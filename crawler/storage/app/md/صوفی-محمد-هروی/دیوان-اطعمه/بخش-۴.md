---
title: >-
    بخش ۴
---
# بخش ۴

<div class="b" id="bn1"><div class="m1"><p>امروز دیگرم به فراق تو شام شد</p></div>
<div class="m2"><p>در آرزوی روی تو عمرم تمام شد</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>در سفره بود گرده چندی تمام شد</p></div>
<div class="m2"><p>فکری بکن که رفت نهاری و شام شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترشی چو با مویز سیه دید گوشت گفت</p></div>
<div class="m2"><p>باز این سیاه روی غلام غلام شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد شب و به مطبخ ما نیست آتشی</p></div>
<div class="m2"><p>ای دیده پاس دار که خوابت حرام شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ مسمنی که زپیشم رمیده بود</p></div>
<div class="m2"><p>منت خدای را که دگر باره رام شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن نو عروس حجره که پالوده نام اوست</p></div>
<div class="m2"><p>خرم کسی که از لب لعلش به کام شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ننگ و نام اطعمه از لحم برده بود</p></div>
<div class="m2"><p>زان دل تمام در پی این ننگ و نام شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صوفی هر آن طعام که می پخت در خیال</p></div>
<div class="m2"><p>ماه صیام آمد و آن جمله خام شد</p></div></div>