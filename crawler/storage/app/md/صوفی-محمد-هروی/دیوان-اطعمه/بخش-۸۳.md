---
title: >-
    بخش ۸۳
---
# بخش ۸۳

<div class="b" id="bn1"><div class="m1"><p>آن سرو ناز من که خجل گشت ماه ازو</p></div>
<div class="m2"><p>دل برده است و در پی جان است آه ازو</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>آن قرص میده ای که خجل گشت ماه ازو</p></div>
<div class="m2"><p>«دل برده است و در پی جان است آه ازو»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن طاق ابروان دلارای مطبخی</p></div>
<div class="m2"><p>محراب جان ماست تو حاجت بخواه ازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشک و تر آنچه هست به سفره نثار کن</p></div>
<div class="m2"><p>کاین گشنگی بلاست، الهی پناه ازو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این ماس وا ز قلیه زنگی کجا شود</p></div>
<div class="m2"><p>نتوان به سعی شست چو بخت سیاه ازو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این قرص میده، سلمه الله، دلبرست</p></div>
<div class="m2"><p>کاین دم ببین به سر نکند هیچ شاه ازو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر جا که هست بره بریان مکرم است</p></div>
<div class="m2"><p>در هیچ حال کم نشود عز و جاه ازو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بر دل برنج غباری ز قند هست</p></div>
<div class="m2"><p>صوفی مستمند، تو عذری بخواه ازو</p></div></div>