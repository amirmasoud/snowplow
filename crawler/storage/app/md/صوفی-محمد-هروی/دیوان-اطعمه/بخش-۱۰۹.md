---
title: >-
    بخش ۱۰۹
---
# بخش ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>باغ مرا چه حاجت سرو و صنوبرست</p></div>
<div class="m2"><p>شمشاد سایه پرور ما از که کمترست</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>از بوی قلیه باز مشامم معطرست</p></div>
<div class="m2"><p>این نکهت این زمان زعبیر که کمترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیپا چو حاضرست به پیشت به روی خوان</p></div>
<div class="m2"><p>از نافه های مشک مگو کان مکررست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بعد روی ما و قدمهای کله پز</p></div>
<div class="m2"><p>هست این سرای بخت و سعادت درین سر ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد درون شفا زعسل جوش می شود</p></div>
<div class="m2"><p>تشخیص کرده ایم و مداوا مقررست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من آب روی خود بر دونان چرا برم</p></div>
<div class="m2"><p>این خشک نان ما چو ز خورشید بهترست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم برنج خدمت بریان از آن کند</p></div>
<div class="m2"><p>کو در میان اطعمه سلطان و سرورست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صوفی خسته بر نخوداب است از آن اسیر</p></div>
<div class="m2"><p>کز بوی عطر او دل و جانش معطرست</p></div></div>