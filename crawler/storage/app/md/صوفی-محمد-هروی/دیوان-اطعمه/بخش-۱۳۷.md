---
title: >-
    بخش ۱۳۷
---
# بخش ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>ز دیگ ترشی ای، قسام روزی</p></div>
<div class="m2"><p>به جا می کرد ترشی، ای برادر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناگه دیدم اندر دیگ ترشی</p></div>
<div class="m2"><p>که گم شد گوشت سربر زد چغندر</p></div></div>