---
title: >-
    بخش ۱۳۸
---
# بخش ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>دیگ حبشی مفاخرت داشت</p></div>
<div class="m2"><p>کامروز به مطبخم افاضل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نوع حوایجی که خواهی</p></div>
<div class="m2"><p>از باطن من شدست حاصل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر کاسه زبان گشاد چمچه</p></div>
<div class="m2"><p>آن لحظه به قهر در مقابل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کون سوخته سیاه رو بین</p></div>
<div class="m2"><p>امروز چها گرفته در دل</p></div></div>