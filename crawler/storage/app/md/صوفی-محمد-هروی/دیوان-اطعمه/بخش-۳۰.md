---
title: >-
    بخش ۳۰
---
# بخش ۳۰

<div class="b" id="bn1"><div class="m1"><p>هر که را مهر تو اندر دل شیدا باشد</p></div>
<div class="m2"><p>پای او را همه بر دیده من جا باشد</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>نخودابی که پر از لحم مهرا باشد</p></div>
<div class="m2"><p>خرم آن دم که نصیب من شیدا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیمه بر صحنک ماهیچه بغایت خوب است</p></div>
<div class="m2"><p>همچو آن زلف که بر عارض زیبا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن زمانی که کشم سر به گریبان کفن</p></div>
<div class="m2"><p>روحم آن روز روان در پی حلوا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسته را چون بکنم، آید از آن سبز قبا</p></div>
<div class="m2"><p>خوش بود پیش من ار صحن منقا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زحکیمان نبود رنج مرا روی بهی</p></div>
<div class="m2"><p>که مداوای من از کاسه بغرا باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وه در آن دم که طغاری بود از بولانی</p></div>
<div class="m2"><p>من و همکاسه یکی، وه چه تماشا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیخ تسبیح به من داد که برخوان صوفی</p></div>
<div class="m2"><p>من نخواهم مگر از خسته خرما باشد</p></div></div>