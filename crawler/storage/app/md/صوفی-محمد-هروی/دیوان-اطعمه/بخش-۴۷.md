---
title: >-
    بخش ۴۷
---
# بخش ۴۷

<div class="b" id="bn1"><div class="m1"><p>چند گاهی است که دل می کشدم سوی برنج</p></div>
<div class="m2"><p>لله الحمد که دیدم به جهان روی برنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حبشی گشته دعاگوی مزعفر شب و روز</p></div>
<div class="m2"><p>آن سیه چرده بلی هست چو هندوی برنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می زند بر من سودازده روغن چشمک</p></div>
<div class="m2"><p>چون به بازار نگاهی بکنم سوی برنج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر سفره بدم همنفس آش حلیم</p></div>
<div class="m2"><p>که پدیدار شد از دور هیاهوی برنج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دهد دست من بی سر و پا را این دم</p></div>
<div class="m2"><p>دو جهان را بکنم وقف به یک موی برنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاجت پیک اجل نیست که اندر مطبخ</p></div>
<div class="m2"><p>جان دهد صوفی سودا زده از بوی برنج</p></div></div>