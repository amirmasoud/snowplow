---
title: >-
    بخش ۱۴۲
---
# بخش ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>ماهیچه باریک که خاتون مال است</p></div>
<div class="m2"><p>گر قیمه بسیار بود پر حال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس قیمت او درین جهان کی داند</p></div>
<div class="m2"><p>چون هر دو جهان فدای یک چنگال است</p></div></div>