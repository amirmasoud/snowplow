---
title: >-
    بخش ۱۰۷
---
# بخش ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>کسی که دیده بود آن نگار را رقاص</p></div>
<div class="m2"><p>عجب مدار که گردد ز درد غصه خلاص</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>به صحن دیگ چو بغرای میده شد رقاص</p></div>
<div class="m2"><p>کسی که دید ز اندوه جوع گشت خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درست قلیه و بغرا صدف به چنگ آور</p></div>
<div class="m2"><p>به بحر کاسه هر آن دست گر شود رقاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه دعوت عام است لیک پندارم</p></div>
<div class="m2"><p>که این برنج برای من است خاص الخاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنیرتر اگر امروز آب شد سهل است</p></div>
<div class="m2"><p>ظهور، باطن هر چیز می شود به خواص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببرده است دلم را جمال بریانی</p></div>
<div class="m2"><p>من شکسته همو را همی برم به خواص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دعوتی که ببینم به آخرش حلوا</p></div>
<div class="m2"><p>هزار فاتحه بخشم در آن دم از اخلاص</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر کجا که شمیم برنج را شنود</p></div>
<div class="m2"><p>رود ز صومعه صوفی به آن طرف رقاص</p></div></div>