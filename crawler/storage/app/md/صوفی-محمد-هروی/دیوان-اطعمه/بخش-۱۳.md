---
title: >-
    بخش ۱۳
---
# بخش ۱۳

<div class="b" id="bn1"><div class="m1"><p>دوای غم نتوان ساخت جز به باده ناب</p></div>
<div class="m2"><p>مرا به لطف خود امروز ساقیا دریاب</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>سحر چو طلعت زناج دیدم اندر خواب</p></div>
<div class="m2"><p>ز شوق آن دل بریان جگر شدست کباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علی الصباح به از کوزه عسل باشد</p></div>
<div class="m2"><p>به پیش خاطر مخمور کاسه سیراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خوش بود به جهان سایه بان نان تنک</p></div>
<div class="m2"><p>که میخ او ز گرز باشد و لغانه طناب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم چو خسته جوع است می کنم پرهیز</p></div>
<div class="m2"><p>بیار گرده بریان و شربت عناب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خانقه نشود نان تمام، می ترسم</p></div>
<div class="m2"><p>مکن تو عیب اگر می روم به عین شتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دید کله بریان به نان شمسی ضم</p></div>
<div class="m2"><p>نه شرم کله قندست این زمان دریاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکش تو منت دو نان چو صوفی مسکین</p></div>
<div class="m2"><p>به نان خشک قناعت کن و به کوزه آب</p></div></div>