---
title: >-
    بخش ۱۵۳ - قال رخوت البیت فی لسان الحال المسماه
---
# بخش ۱۵۳ - قال رخوت البیت فی لسان الحال المسماه

<div class="b" id="bn1"><div class="m1"><p>دوش در خانه به من تخت شد اندر گفتار</p></div>
<div class="m2"><p>گفت دارم ز تو ای خواجه شکایت بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که محبوس درین کنج سرای تو شدم</p></div>
<div class="m2"><p>در شب و روز بگو چند نشینم بیکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه امید که به زیر تو در آیم روزی</p></div>
<div class="m2"><p>چند سایم من درویش بگو بر دیوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوردن من ز تو گر نیست به روی صندوق</p></div>
<div class="m2"><p>برهنه چند نشینم تو بگو بی ایزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن تو بر تن من می نرسد در صحبت</p></div>
<div class="m2"><p>مگر آن روز که تو رنجه شوی یا بیمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت بالشت که من نیز ازین می نالم</p></div>
<div class="m2"><p>چند شینی تو دل غمزده رو بر دیوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد لحاف آن نفس اندر غضب و می لندید</p></div>
<div class="m2"><p>که چه خاوند...گشته به ماها دوچار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم ای...قواده تو خمش کن باری</p></div>
<div class="m2"><p>که مرا هست ز نام تو درین ساعت عار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت آن گاه به من خنده زنان سرگفتی</p></div>
<div class="m2"><p>باش تا برف شود از افق چرخ به بار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عار خود را به تو آن گاه عیان بنمایم</p></div>
<div class="m2"><p>که تو لرزان شده باشی به زمستان چون خار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناگهان نعره برآورد ز یک گوشه پلاس</p></div>
<div class="m2"><p>که مرا هم سخنی هست بگویم ناچار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدتی شد که درین گوشه فتادم حیران</p></div>
<div class="m2"><p>گرد از جان من خسته برآورد دمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوریا گفت که من نقش زمین دارم لیک</p></div>
<div class="m2"><p>کس ندیدم که نشیند به روی من یک بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بحث اینها چو به مطبخ برسید از سر درد</p></div>
<div class="m2"><p>دیگ فریاد برآورد که آه از غم نار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم ای سنگ دل رو سیه سوخته کون</p></div>
<div class="m2"><p>دم خور این لحظه چه فریاد کنی ناهنجار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کاسه و چمچه و کفلیز ز یک سوی دگر</p></div>
<div class="m2"><p>باز کردند به دشنام دهان همچو طغار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود در ناله و فریاد و فغان دنبه گداز</p></div>
<div class="m2"><p>زین جهت ترش پلا داشت دلی پر آزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سیرکو گفت که من نیز ازین کوفته ام</p></div>
<div class="m2"><p>که مرا نیست نوا و بچه دارم به کنار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>انبه جولی ز سر قهر سوی دیگ دوید</p></div>
<div class="m2"><p>که تو باری خمش ای رو سیه ناهموار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شکمی پر بچه داری و سر پوشیده</p></div>
<div class="m2"><p>بر سر خشت که نایی به سلامت به کنار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چند گرمی کنی امروز تو با خویش بجوش</p></div>
<div class="m2"><p>کز تو هستند بسی به زصغار و زکبار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیگ را دود به سر رفت ز قهر جولی</p></div>
<div class="m2"><p>جوش می زد دلش از غصه آن بی مقدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خمچه سرکه بگفتا که دلی پر دارم</p></div>
<div class="m2"><p>می زند جوش، درون من ازین غم بسیار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صوفیا چند نشینی تو درین کنج سرا</p></div>
<div class="m2"><p>ما همه مضطر و حیران و بمانده بیکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فکر کردم که جواب چه دهم اینها را</p></div>
<div class="m2"><p>سر فرو بردم ازین واقعه چون بوتیمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خنب آب از طرفی بانگ بر اینها زد و گفت</p></div>
<div class="m2"><p>که چه فریاد و فغان است و چه کار است و چه بار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من به تردامنی خویش، ازو خورسندم</p></div>
<div class="m2"><p>نیست اندر دل و جان من ازو هیچ آزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کوزه گفتا که ازو هست دل من صافی</p></div>
<div class="m2"><p>سفره گفتا که نواهاست مرا زو بسیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کاسه و کوزه و این خم وتغار و کفلیز</p></div>
<div class="m2"><p>درهم آویخته و جنگ و جدل شد بسیار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من از آن کنج سرا روی به بام آوردم</p></div>
<div class="m2"><p>گفتم این رخت سرا نیست مرا هیچ به کار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من ندارم سر این جنگ، شما این ساعت</p></div>
<div class="m2"><p>ترک گیرید که دارم غم دل من بسیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دل ز من برد یکی سرو قدی سیم بری</p></div>
<div class="m2"><p>که من از فرقت او بی خورم و خواب و قرار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خواب چون نیست، ایا تخت کجا تکیه کنم</p></div>
<div class="m2"><p>سر به بالشت کجا می رسدم بی رخ یار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بجز از غصه و غم هیچ نمی نوشم چون</p></div>
<div class="m2"><p>چه برم غیر خیال رخ آن لاله عذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون درین واقعه من بی خور و خوابم ای دیگ</p></div>
<div class="m2"><p>لطف فرما و درین واقعه معذورم دار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هست چندانی غم آن بت عیار مرا</p></div>
<div class="m2"><p>که شدم از خود و از جمله عالم بیزار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر رسم من به مراد دل خود در عالم</p></div>
<div class="m2"><p>به نوایی بر سر کاسه و کفلیز و تغار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ور نه حقا که ز خود سیرم و از خلق ملول</p></div>
<div class="m2"><p>غم آن دوست برآرد ز من خسته دمار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر که او هست مشرف به وصال صنمی</p></div>
<div class="m2"><p>گو غنیمت شمر آن لحظه که هست او با یار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه، مرادی به از آن نیست که در خلوت جان</p></div>
<div class="m2"><p>دوست در خواب خوش و عاشق مسکین بیدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که را دولت این کار میسر گردد</p></div>
<div class="m2"><p>او شکایت نکند از فلک ناهنجار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مکنید از من درویش فراموش، آن دم</p></div>
<div class="m2"><p>ای عزیزان چو نشینید به خلوت با یار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>من ازین حسرت اگر جان بدهم معذورم</p></div>
<div class="m2"><p>که جدا مانده ام از روی چو خورشید نگار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مردن و باز رهیدن زغم و محنت و درد</p></div>
<div class="m2"><p>بهتر از فرقت یارست به عالم صد بار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صوفیا عمر چو بی دوست بود، مردن به</p></div>
<div class="m2"><p>گل چو در باغ نباشد چه کشی زحمت خار</p></div></div>