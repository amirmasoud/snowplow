---
title: >-
    بخش ۵۴
---
# بخش ۵۴

<div class="b" id="bn1"><div class="m1"><p>ای دل اگر از خویشتن امروز بمیری</p></div>
<div class="m2"><p>جاوید شوی زنده چو از من بپذیری</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>دارم هوس امروز بلونی خمیری</p></div>
<div class="m2"><p>جای است اگر در هوسش زار بمیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر صحن برنجی فتد امروز، به دستت</p></div>
<div class="m2"><p>زنهار بنوشی همه را بر سر سیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باید که بود معده پر امروز، به هر حال</p></div>
<div class="m2"><p>چه نان جوین و چه خمیر و چه فطیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی دانه انگور نباشی تو به فصلش</p></div>
<div class="m2"><p>فخری نبود این دم، چو رسیدی به امیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شوش جگر بند برو حاضر دل باش</p></div>
<div class="m2"><p>هر چند به خون...تو پرورده به شیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می گفت کدک من به ام از کله به گیپا</p></div>
<div class="m2"><p>گفتش که مکن عیب کبیران چو صغیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صوفی بشوی عاقبت از خوان نعم سیر</p></div>
<div class="m2"><p>زنهار مینداز دل این دم ز فقیری</p></div></div>