---
title: >-
    بخش ۹۹
---
# بخش ۹۹

<div class="b" id="bn1"><div class="m1"><p>تا مرا واله آن سرو روان ساخته اند</p></div>
<div class="m2"><p>مهر او را وطن اندر دل و جان ساخته اند</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>تا ز گندم به جهان گرده نان ساخته اند</p></div>
<div class="m2"><p>جای او را وطن اندر دل و جان ساخته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر اسرار محبت که نهان در گیپاست</p></div>
<div class="m2"><p>در رخ کله نگر نیک عیان ساخته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد رناج بلائی است که در روی زمین</p></div>
<div class="m2"><p>ظاهرا فتنه این خلق جهان ساخته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلبیا خاتم حوران بهشتی است مگر</p></div>
<div class="m2"><p>به جهان بهر دل خلق روان ساخته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دعوتی هست دگر، قتلمه او را نام است</p></div>
<div class="m2"><p>دل مجروح مرا مرهم از آن ساخته اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل مسکین مرا بر رخ جانپرور نان</p></div>
<div class="m2"><p>چه کنم آه که این دم نگران ساخته اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه کس را نشود دولت بریان روزی</p></div>
<div class="m2"><p>همچو صوفی ستم دیده به نان ساخته اند</p></div></div>