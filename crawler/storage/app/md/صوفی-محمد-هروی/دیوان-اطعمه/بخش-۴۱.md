---
title: >-
    بخش ۴۱
---
# بخش ۴۱

<div class="b" id="bn1"><div class="m1"><p>مهر جمال یار که چون روح در تن است</p></div>
<div class="m2"><p>منت خدای را که نهان در دل من است</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>امروز روز کاچی و دوشاب و روغن است</p></div>
<div class="m2"><p>مرغان برف را چو به دنیا نشیمن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جوز مغز سوده بود روی باش او</p></div>
<div class="m2"><p>بی شک بدان که مرهم جان و دل من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مطبخی مدار تو دوشاب ازو دریغ</p></div>
<div class="m2"><p>کاین آب دار چون جسد، آن روح این تن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تخم گیاه چون نکند در میان او</p></div>
<div class="m2"><p>هر مطبخی که پخت یقین دان که دشمن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همکاسه را چو رغبت کامل نیافتم</p></div>
<div class="m2"><p>بی دولتی اوست ولی دولت من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس نخواهد و نکند فکر کاچی ای</p></div>
<div class="m2"><p>مردش مخوان که در دل مردان کم از زن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صوفی به غیر لوت زدن هیچدان بود</p></div>
<div class="m2"><p>آری نصیب او ز ازل باز این فن است</p></div></div>