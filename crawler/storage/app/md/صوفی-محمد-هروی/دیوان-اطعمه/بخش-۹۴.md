---
title: >-
    بخش ۹۴
---
# بخش ۹۴

<div class="b" id="bn1"><div class="m1"><p>تا کوکب جمالش در زمانه لامع</p></div>
<div class="m2"><p>بهر مشعله داریش خورشید گشته طالع</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>تا قرص ماه نان شد در روی سفره طالع</p></div>
<div class="m2"><p>بر آفتاب شد تنگ گردون که هست واسع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شاه خربزه شد ناگه عیان به بازار</p></div>
<div class="m2"><p>بنگر خیار پیشش چون گشته است راکع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اشتیاق بریان ای نان شیر پرور</p></div>
<div class="m2"><p>آه دل مرا بین شبها چو برق لامع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاغد حجاب قندست اندر دکان قناد</p></div>
<div class="m2"><p>یارب برانداز، آن را که هست مانع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را به سعی اکنون روزی نشد مزعفر</p></div>
<div class="m2"><p>ای نان جو دریغا زان رنجهای ضایع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دی ناصحی مرا گفت کن ترک نان و بریان</p></div>
<div class="m2"><p>آری دل مسامح گویند هست واسع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صوفی به دهر می داشت طاس هریسه امید</p></div>
<div class="m2"><p>اکنون به نان خشکی مسکین شدست قانع</p></div></div>