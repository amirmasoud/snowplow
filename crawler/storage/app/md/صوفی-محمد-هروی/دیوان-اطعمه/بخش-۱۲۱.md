---
title: >-
    بخش ۱۲۱
---
# بخش ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>نصیب ما ز می لعل دوست گشته خمار</p></div>
<div class="m2"><p>کنیم جان به سر و کار عشق آخر کار</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>سحر کسی که کند نوش یک دو جو زاسرار</p></div>
<div class="m2"><p>علی الصباح بباید سه کله اش ناچار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نه بره بریان بود به نیمه روز</p></div>
<div class="m2"><p>چو روزه دار بر آرد گرسنگیش دمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم به صحنک ماهیچه آرزومندست</p></div>
<div class="m2"><p>به شرط آن که برو قیمه باشدی بسیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مزاج من که به سودا کشد ز بی برگی</p></div>
<div class="m2"><p>دواش گرده گرم است و دنبه پروار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقدمی بود اندر میان هر قومی</p></div>
<div class="m2"><p>میان آشپزان کله پز بود سردار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه بر سر گنج است مار، و قیمه نگر</p></div>
<div class="m2"><p>به روی صحنک ماهیچه، گنج بر سر مار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کار صوفی مسکین نیرزد از بد و نیک</p></div>
<div class="m2"><p>به لوت خوردن و زله بزیست یک دینار</p></div></div>