---
title: >-
    بخش ۵۹
---
# بخش ۵۹

<div class="b" id="bn1"><div class="m1"><p>به هر درد و غمی دل مبتلا شد</p></div>
<div class="m2"><p>چرا یکباره یار از ما جدا شد</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>به گیپا و کدک دل مبتلا شد</p></div>
<div class="m2"><p>چرا نان تنک از ما جدا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم بیگانه از نان و پیازست</p></div>
<div class="m2"><p>چو با صحن مزعفر آشنا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو ای روح در گوش دل من</p></div>
<div class="m2"><p>که لحم بره اندر ماسوا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر قند این زمان نایافت گشته</p></div>
<div class="m2"><p>که در چشم مزعفر توتیا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به قدر قامت زناج این دم</p></div>
<div class="m2"><p>دلم خو کرد، یاران این بلا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بیچاره را بر باد برداد</p></div>
<div class="m2"><p>چو بوی قلیه همراه صبا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شلاین گشته بر حلواگرانبار</p></div>
<div class="m2"><p>دل صوفی چو مست زلبیا شد</p></div></div>