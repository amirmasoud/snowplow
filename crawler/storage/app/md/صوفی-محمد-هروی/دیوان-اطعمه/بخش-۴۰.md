---
title: >-
    بخش ۴۰
---
# بخش ۴۰

<div class="b" id="bn1"><div class="m1"><p>رسید فصل بهار و جهان گلستان شد</p></div>
<div class="m2"><p>گریست ابر بهاری و باغ خندان شد</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که معده او پر ز نان و بریان شد</p></div>
<div class="m2"><p>اگر کیمنه گدائی بود، که سلطان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گریه ها که همی کرد دوش بریانی</p></div>
<div class="m2"><p>علی الصباح به رغمش برنج خندان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشوی دست و پس آن گه طواف مطبخ کن</p></div>
<div class="m2"><p>که بی طهارت ظاهر، به کعبه نتوان شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببرد ه است به دزدی دلم چو بریانی</p></div>
<div class="m2"><p>از آن بدار، چو دزدان گهی به زندان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قعر صحن برآورده مرغ بریان سر</p></div>
<div class="m2"><p>به روی سفره و در نان میده حیران شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برنج را چو ز روغن رسید مالشها</p></div>
<div class="m2"><p>عجب مدار که آشفته و پریشان شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل شکسته صوفی مثال پالوده</p></div>
<div class="m2"><p>ز شوق صحنک فرنی قند لرزان شد</p></div></div>