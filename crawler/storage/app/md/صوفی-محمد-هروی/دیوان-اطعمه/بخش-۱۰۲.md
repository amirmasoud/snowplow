---
title: >-
    بخش ۱۰۲
---
# بخش ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ای لب شیرین تو حلوای قند</p></div>
<div class="m2"><p>قامت رعنای تو سرو بلند</p></div></div>
<div class="n" id="bn2"><p>در جواب او</p></div>
<div class="b" id="bn3"><div class="m1"><p>صحن برنجی که بود پر زقند</p></div>
<div class="m2"><p>مرهم جان است و بسی سودمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش قد دلکش زناج بین</p></div>
<div class="m2"><p>گشته خجل قامت سرو بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ دلم گشته اسیر اسیب</p></div>
<div class="m2"><p>روی خلاصی نبود زان کمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چهره برافروخته بریان صباح</p></div>
<div class="m2"><p>تا برباید دل مسکین چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عرق آید شکر از انفعال</p></div>
<div class="m2"><p>کاک به دکان چو کند نیمخند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیب مکن کله، تو کورماج را</p></div>
<div class="m2"><p>زان که به جائی نرسد خود پسند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که چو صوفی به عسل داد دل</p></div>
<div class="m2"><p>پند کلیچه نبود سودمند</p></div></div>