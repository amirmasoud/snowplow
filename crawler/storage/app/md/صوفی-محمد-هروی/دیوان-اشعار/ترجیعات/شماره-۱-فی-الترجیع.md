---
title: >-
    شمارهٔ  ۱ - فی الترجیع
---
# شمارهٔ  ۱ - فی الترجیع

<div class="b" id="bn1"><div class="m1"><p>جانا حق دوستی نگه دار</p></div>
<div class="m2"><p>دل را به جفا دگر میازار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان و جهان گناه دل نیست</p></div>
<div class="m2"><p>چشم سیه تو کرده این کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوزم من بی مراد چون شمع</p></div>
<div class="m2"><p>شبهای دراز رو به دیوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمت که به خواب ناز رفته</p></div>
<div class="m2"><p>او را چه خبر ز حال بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که تو سر خوش و خرامان</p></div>
<div class="m2"><p>از خانه روی به سوی گلزار</p></div></div>
<div class="b2" id="bn6"><p>برخیزم و دامن تو گیرم</p>
<p>سر بر قمدت نهم بمیرم</p></div>
<div class="b" id="bn7"><div class="m1"><p>ای دوست نظر به حال ما کن</p></div>
<div class="m2"><p>درد دل خسته را دوا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چند کشم جفا و جورت</p></div>
<div class="m2"><p>آخر نفسی به ما وفا کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خسرو جمله ماهرویان</p></div>
<div class="m2"><p>روی نظری به این گدا کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با من ز چه روی سر گرانی</p></div>
<div class="m2"><p>ای عمر عزیز من صفا کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مستانه چو باد راز سازی</p></div>
<div class="m2"><p>گویی که به خشم خویش جا کن</p></div></div>
<div class="b2" id="bn12"><p>برخیزم و دامن تو گیرم</p>
<p>سر بر قدمت نهم بمیرم</p></div>
<div class="b" id="bn13"><div class="m1"><p>ای دوست بیا که نوبهارست</p></div>
<div class="m2"><p>درکش قدحی که وقت کار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل در چمن است از آن معطر</p></div>
<div class="m2"><p>کز پیرهن تو یادگار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گل را چه کنم که بی جمالت</p></div>
<div class="m2"><p>در سینه بنده خار خارست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنمای جمال و پرده بردار</p></div>
<div class="m2"><p>امروز نه وقت انتظار است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون سوی چمن روی خرامان</p></div>
<div class="m2"><p>مستانه که موسم بهار است</p></div></div>
<div class="b2" id="bn18"><p>برخیزم و دامن تو گیرم</p>
<p>سر بر قدمت نهم بمیرم</p></div>
<div class="b" id="bn19"><div class="m1"><p>دل برده ز من رخ تو ناگاه</p></div>
<div class="m2"><p>پروای دلم نمی کنی آه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای از تو مرا هزار غم بیش</p></div>
<div class="m2"><p>بنگر تو به سوی بنده ناگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نالم شب و روز در فراقت</p></div>
<div class="m2"><p>دارم چو به سینه درد جان کاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشتم چو کواکب از دو چشمت</p></div>
<div class="m2"><p>سرگشته من فقیر ای ماه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روزی که دوچار من شوی تو</p></div>
<div class="m2"><p>مستان و قبا گشاده در راه</p></div></div>
<div class="b2" id="bn24"><p>برخیزم و دامن تو گیرم</p>
<p>سر بر قدمت نهم بمیرم</p></div>
<div class="b" id="bn25"><div class="m1"><p>آرام دل فقیر من تو</p></div>
<div class="m2"><p>جان و دل و شاه و میر من تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دریاب مرا که هستی ای دوست</p></div>
<div class="m2"><p>در عشق چو دستگیر من تو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همتای تو در جهان ندیدم</p></div>
<div class="m2"><p>ای دلبر بی نظیر من تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آخر نظری به حال من کن</p></div>
<div class="m2"><p>ای خسرو دلپذیر من تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روزی که به کوری رقیبان</p></div>
<div class="m2"><p>گویی که بیا اسیر من تو</p></div></div>
<div class="b2" id="bn30"><p>برخیزم و دامن تو گیرم</p>
<p>سر بر قدمت نهم بمیرم</p></div>
<div class="b" id="bn31"><div class="m1"><p>چون از تو مرا به سر نباشد</p></div>
<div class="m2"><p>جز تو هوس دگر نباشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صد جان بکنم فدای رویت</p></div>
<div class="m2"><p>ای جان جهان مگر نباشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دریاب که ز انتظار چیزی</p></div>
<div class="m2"><p>اندر دو جهان بتر نباشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جانا چو به زر بود ترا میل</p></div>
<div class="m2"><p>بیچاره مرا که زر نباشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مستان چو گذر کنی به سویم</p></div>
<div class="m2"><p>روزی که مرا خبر نباشد</p></div></div>
<div class="b2" id="bn36"><p>برخیزم و دامن تو گیرم</p>
<p>سر بر قدمت نهم بمیرم</p></div>
<div class="b" id="bn37"><div class="m1"><p>حال دل من اگر بدانی</p></div>
<div class="m2"><p>بر من قلم جفا نرانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جان زنده به یاد تست امروز</p></div>
<div class="m2"><p>دل را چه محل که جان جانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون نامه مرا بخوان خدا را</p></div>
<div class="m2"><p>تا کی تو مرا به سر دوانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یارب چه شود که چون سگ خویش</p></div>
<div class="m2"><p>صوفی فقیر را بخوانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر از سر کوی خویش گویی</p></div>
<div class="m2"><p>اکنون که بپر برو کرانی</p></div></div>
<div class="b2" id="bn42"><p>برخیزم و دامن تو گیرم</p>
<p>سر بر قدمت نهم بمیرم</p></div>