---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>امروز مرا فرقت آن روی چو ماه است</p></div>
<div class="m2"><p>در هجر ندانم که در آفاق چه ماه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خلق نخواهم که کنم درد تو پنهان</p></div>
<div class="m2"><p>اما چه کنم این دو رخ زرد گواه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوید که مکش آه، مرا آه چه سازد</p></div>
<div class="m2"><p>خورسندی بیمار چو از ناله و آه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف آمد و بگرفت همه عارض او را</p></div>
<div class="m2"><p>آشفتگی من همه از زلف سیاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم نظری و دلم افتاد درین قید</p></div>
<div class="m2"><p>از قید دل ای دیده ترا عین گناه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل چو اسیر سر زلف و ذقنی تو</p></div>
<div class="m2"><p>شب حاضر خود باش که در راه تو چاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشد رخ صوفی و قدوم تو چو در شهر</p></div>
<div class="m2"><p>مقبول بود زر که برو سکه شاه است</p></div></div>