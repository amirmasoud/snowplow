---
title: >-
    شمارهٔ  ۶۱
---
# شمارهٔ  ۶۱

<div class="b" id="bn1"><div class="m1"><p>دل از ما برد آن شوخ و روان کرد این زمان پهلو</p></div>
<div class="m2"><p>چرا کرد ای مسلمانان زیار مهربان پهلو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روی او برابر کرد ماه چارده خود را</p></div>
<div class="m2"><p>تهی کرد او شب دیگر ببین بر آسمان پهلو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سوی من کند پشت و دعاگو را دهد دشنام</p></div>
<div class="m2"><p>مرا می دارد او آری میان عاشقان پهلو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زند پهلو به مسکینان رقیب از غایت نخوت</p></div>
<div class="m2"><p>بگو او را تو آن ساعت چو گویی بر خزان پهلو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان در عشق آن مهوش ضعیف و لاغر و زارم</p></div>
<div class="m2"><p>که از روی قبای من توان دیدن عیان پهلو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیاده بر سر راه توام ای شهسوار من</p></div>
<div class="m2"><p>اگر رحمی نمی آری مزن بر ناتوان پهلو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میاور بر دل صوفی جفا افزون ز عشاقان</p></div>
<div class="m2"><p>مگر او چرب‌تر داند مرا از دیگران پهلو</p></div></div>