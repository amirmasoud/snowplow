---
title: >-
    شمارهٔ  ۵۰
---
# شمارهٔ  ۵۰

<div class="b" id="bn1"><div class="m1"><p>پرده بردار ز رخ ای بت خورشید جبین</p></div>
<div class="m2"><p>دیده تر، لب خشک و دل مجروحم بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بناگوش تو آن خط بود از نیلی خام</p></div>
<div class="m2"><p>چشم زخمی است چنان...قضا ساخت چنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می رود بهر شکار دل صاحب نظران</p></div>
<div class="m2"><p>مست برخاسته باز آن بت خرگاه نشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظاهرا مهر سلیمانت که پنهان شده بود</p></div>
<div class="m2"><p>آن دهان خاتم و لعل تو درو هست نگین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناوک غمزه به ابروی کمان پیوسته</p></div>
<div class="m2"><p>در شب و روز برای دل زاهد به کمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش افتاد هزاران دل شیدا به رهت</p></div>
<div class="m2"><p>زان سبب پای تو امروز نیاید به زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفی دل شده را بار دگر روزی باد</p></div>
<div class="m2"><p>دیدن طلعت آن ماه، بگویید آمین</p></div></div>