---
title: >-
    شمارهٔ  ۲۰
---
# شمارهٔ  ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای خجل در چمن از قامت تو سرو بلند</p></div>
<div class="m2"><p>چشم شوخ تو سبق برده زبادام خجند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل زابروی تو پیوسته گرفتار بلاست</p></div>
<div class="m2"><p>چون رهد مرغ که افتاده بود دام کمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چه بالا و میان است و چه ابرو و چه چشم</p></div>
<div class="m2"><p>یارب از چشم بدانت نرسد هیچ گزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه زنی لاف ز میم دهن تنگ حبیب</p></div>
<div class="m2"><p>برو ای غنچه سیراب، تو بر خویش بخند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای پری در غم سودای توام دیوانه</p></div>
<div class="m2"><p>ناز تا کی بود و جور و جفاهای تو چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه نشینی به رقیبان و دلم خون سازی</p></div>
<div class="m2"><p>گر وفایی نکنی جور و جفا هم مپسند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفیا چند کنی ناله، ز خوبان گفتی</p></div>
<div class="m2"><p>گر تو خواهی که به کس دل ندهی دیده ببند</p></div></div>