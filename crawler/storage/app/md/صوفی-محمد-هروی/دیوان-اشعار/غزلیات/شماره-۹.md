---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>مرا ز آدم خاکی چو غم بود میراث</p></div>
<div class="m2"><p>کجاست دامن ساقی کزو رسم به غیاث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان بود حدثی بازمانده از فرعون</p></div>
<div class="m2"><p>بشو به آب قناعت تو دست ازین احداث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گنده پیر قبیح است دهر مردم خوار</p></div>
<div class="m2"><p>برو چو اهل یقینش طلاق کن به ثلاث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک چو حادثه زای است زو مشو ایمن</p></div>
<div class="m2"><p>پناه بر به در می فروش از آن احداث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان چو بر سر راه قیامت است بلی</p></div>
<div class="m2"><p>منه اساس اقامت که نیست جای اثاث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبیثه ای است جهان و تو باز عالم قدس</p></div>
<div class="m2"><p>چو کرکسان مکن امروز خوی بر اخباث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنه تو دل به غم و غصه جهان صوفی</p></div>
<div class="m2"><p>چو این رسید ز حوا و آدمت میراث</p></div></div>