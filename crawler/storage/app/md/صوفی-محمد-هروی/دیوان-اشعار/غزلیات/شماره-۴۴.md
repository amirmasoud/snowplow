---
title: >-
    شمارهٔ  ۴۴
---
# شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>عید صیام آمد، موسم نوبهار هم</p></div>
<div class="m2"><p>وصل نگار باید و باده خوشگوار هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار سپاه غمزه را چون به در آرد از حجاب</p></div>
<div class="m2"><p>لشکر صد پیاده را بشکند و سوار هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم تو کشت خلق را، رحم کن ای نگار من</p></div>
<div class="m2"><p>ورچه وفا نمی کنی ظلم روا مدار هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز شمار بندگان دلبر شوخ دیده ام</p></div>
<div class="m2"><p>از کرم این شکسته را بنده خود شمار هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف و رخ تو برد وه در شب و روز ای صنم</p></div>
<div class="m2"><p>خواب ز دیده من و از دل و جان قرار هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نالم از آن که این زمان هست من شکسته را</p></div>
<div class="m2"><p>سینه جراحت از غم و دیده اشکبار هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل اگر تو عاقلی رغم همه منازعان</p></div>
<div class="m2"><p>باده به چنگ آور و ساقی گلعذار هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر خیال او مرا نیست به کنج صومعه</p></div>
<div class="m2"><p>در سر و کار عشق شد حاصل کار و بار هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صوفی مستمند را آمده پیش، چون کند</p></div>
<div class="m2"><p>پیری و عشق و مفلسی محنت روزگار هم</p></div></div>