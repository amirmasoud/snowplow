---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>آن ترک مست، دیده چو از خواب باز کرد</p></div>
<div class="m2"><p>با عاشقان غمزده آهنگ ناز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شانه چو ره به گیسوی او برد لاجرم</p></div>
<div class="m2"><p>با دست کوته او عملی بس دراز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور اوفتد ز کعبه مقصود سالها</p></div>
<div class="m2"><p>از کوی دوست هرکه هوای حجاز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محراب ابروی تو ندیدست از آن سبب</p></div>
<div class="m2"><p>زاهد به کنج صومعه شبها نماز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کس که دید طلعت خوب تو دیده را</p></div>
<div class="m2"><p>بر روی دلبران دو عالم فراز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی بیار باده که دارم دلی حزین</p></div>
<div class="m2"><p>زان لعبها که این فلک حقه باز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفی به آب دیده کند غسل هر شبی</p></div>
<div class="m2"><p>چون شمع گریه ها که به سوز و گداز کرد</p></div></div>