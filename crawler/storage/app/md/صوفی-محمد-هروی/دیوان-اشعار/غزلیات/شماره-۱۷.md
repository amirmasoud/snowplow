---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>ایا حکیم مسیحا دم ستوده خصال</p></div>
<div class="m2"><p>عوارضات ز تشخیص تو بپرهیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر مریض که چشم عنایت تو فتد</p></div>
<div class="m2"><p>درین زمانه یقین دان نصیحت آمیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مراست یک مرضی واقع این زمان به بدن</p></div>
<div class="m2"><p>که از تردد او دیده خون همی ریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مثانه سرد و ازین غم جگر مرا شده گرم</p></div>
<div class="m2"><p>که هیچ مرغ بدن شهوتی نه انگیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکبری به دماغش چنان شدست پدید</p></div>
<div class="m2"><p>که گر سلام کند فرج، بر نمی خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثال خسته یکساله سر نهاده به جای</p></div>
<div class="m2"><p>چو...بنده همین آب دیده می ریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حیله جانب خلوت سراش چون ببرم</p></div>
<div class="m2"><p>مثال خر فکند سر به پیش و بستیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بخت کرد فراموش یار ازین جهتم</p></div>
<div class="m2"><p>دل شکسته بگوئید با که آمیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایا حکیم ز لطف و کرم دوایی کن</p></div>
<div class="m2"><p>وگر نه صوفی مسکین ز شهر بگریزد</p></div></div>