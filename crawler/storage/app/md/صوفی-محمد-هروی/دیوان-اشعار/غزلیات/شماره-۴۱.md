---
title: >-
    شمارهٔ  ۴۱
---
# شمارهٔ  ۴۱

<div class="b" id="bn1"><div class="m1"><p>چشمه آب حیات است دهان یارم</p></div>
<div class="m2"><p>از لب چون شکرش کام طمع می دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در همه کون و مکان غیر تو دلدارم نیست</p></div>
<div class="m2"><p>جان کنم در سر و سودای تو در دل دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه روم سوی گلستان و چمن را چه کنم</p></div>
<div class="m2"><p>بی تو در دیده خونبار بود گل خارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چمن در نظر گل منشین بی پرده</p></div>
<div class="m2"><p>که ترا در نظر غیر رواکی دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق زارم و جز دیدن او ای واعظ</p></div>
<div class="m2"><p>به نصیحت که کنی، باد هوا پندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست این کار مرا قسمت روز ازلی</p></div>
<div class="m2"><p>گر ترا دیده بیناست مکن انکارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو صوفی سر و دستار به می بفروشم</p></div>
<div class="m2"><p>جرعه ای باده به این حیله مگر دست آرم</p></div></div>