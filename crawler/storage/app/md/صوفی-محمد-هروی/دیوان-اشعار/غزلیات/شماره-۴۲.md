---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>وقت آن شد که اقامت به خرابات برم</p></div>
<div class="m2"><p>چند در مدسه بی فایده اوقات برم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر بی صحبت رندان خرابات گذشت</p></div>
<div class="m2"><p>باقی عمر به آنجا به مکافات برم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند آن عنب آینه دل، صافی</p></div>
<div class="m2"><p>خواهم این زنگ غم از طلعت مرآت برم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود فروشی است کرامات بگو زاهد را</p></div>
<div class="m2"><p>من چرا رنج دل از بهر کرامات برم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف آن شاه پسر فتنه دور قمرست</p></div>
<div class="m2"><p>زان شر و شور امان سوی خرابات برم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طاق محراب دو ابروی تو کو در شب زلف</p></div>
<div class="m2"><p>تا دل غمزده خود به مناجات برم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کسی روز جزا فخر کند از عملی</p></div>
<div class="m2"><p>من چو صوفی غم او را به مباهات برم</p></div></div>