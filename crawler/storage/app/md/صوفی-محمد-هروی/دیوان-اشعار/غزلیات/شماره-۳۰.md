---
title: >-
    شمارهٔ  ۳۰
---
# شمارهٔ  ۳۰

<div class="b" id="bn1"><div class="m1"><p>خواهم نظری در رخ خوب تو دگر بار</p></div>
<div class="m2"><p>از عمر چو سیری نبود ای بت عیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تو به هم برزده حال دل ما را</p></div>
<div class="m2"><p>دانم که چنینها نکند مردم هشیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل خار شده باز و چمن گشته معطر</p></div>
<div class="m2"><p>کاکل زده ای شانه مگر دوش به گلزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر عارض آن ماه خط سبز عیان شد</p></div>
<div class="m2"><p>ای دل حذر از عین بلاکن به شب تار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مذهب رندان می پنهان دو گناه است</p></div>
<div class="m2"><p>با ناله نی باده خور و عود به چنگ آر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب پرتو روی تو مرا در نظر آمد</p></div>
<div class="m2"><p>از نور بلی بهره برد دیده دیدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن غمزه شد از کشتن عشاق پشیمان</p></div>
<div class="m2"><p>آری چو انابت بود اندر دل بیمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صوفی به حذر باش که گفتند ازین پیش</p></div>
<div class="m2"><p>خواهی که به کس دل ندهی دیده نگه دار</p></div></div>