---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>می کشم هر نفس از درد جدایی آوخ</p></div>
<div class="m2"><p>جگری دارم از اندوه فراقش لخ لخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشو ای دل تو ملول از سخن سرد رقیب</p></div>
<div class="m2"><p>خنکی را نتوان کرد برون از دل یخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر از جان دل خسته من بازمگیر</p></div>
<div class="m2"><p>گر فزونند محبان تو از مور و ملخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش هفت سقر، لاشی و معدوم شود</p></div>
<div class="m2"><p>شرری گر فتد از نار دلم بر دوزخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>...شده گفتند رقیب از کویش</p></div>
<div class="m2"><p>بود آیا که زبنیاد بر افتد آن رخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می پزم باز خیال لب آن مه، بنشین</p></div>
<div class="m2"><p>تا که حلوای شکر پخته شود در مطبخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که در عشق ببیند تن صوفی گوید</p></div>
<div class="m2"><p>اندرین خرقه چه باریک و ضعیف است این نخ</p></div></div>