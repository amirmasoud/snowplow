---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>آن چشم نرگس به سر آن پسر مرا</p></div>
<div class="m2"><p>چون لاله ساخت غرقه به خون جگر مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک و بدی چو هست به تقدیر چون کنم</p></div>
<div class="m2"><p>زاهد رسید هم زقضا این قدر مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردی است در دلم که مداوا لبان اوست</p></div>
<div class="m2"><p>باشد دوای سینه بلی گلشکر مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دیده ها نشست چو تیر تو کاشکی</p></div>
<div class="m2"><p>بودی به جای هر مژه چشم دگر مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق ترا چگونه نهان سازمت که هست</p></div>
<div class="m2"><p>لبهای خشک شاهد و چشمان تر مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال دلم شدست پریشان چو خط یار</p></div>
<div class="m2"><p>بر هم ز دست شورش دور قمر مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفی مگر به منزل مقصود راه یافت</p></div>
<div class="m2"><p>آری، دلیل عشق تو شد راهبر مرا</p></div></div>