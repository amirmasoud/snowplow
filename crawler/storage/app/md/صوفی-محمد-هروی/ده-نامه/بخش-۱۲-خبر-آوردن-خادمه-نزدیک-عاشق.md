---
title: >-
    بخش ۱۲ - خبر آوردن خادمه نزدیک عاشق
---
# بخش ۱۲ - خبر آوردن خادمه نزدیک عاشق

<div class="b" id="bn1"><div class="m1"><p>خادمه آمد به بر آن جوان</p></div>
<div class="m2"><p>گفت سخنها که شنود آن زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت جوان را که بخندید یار</p></div>
<div class="m2"><p>من شدم از خنده اش امیدوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوب شود عاقبت کار تو</p></div>
<div class="m2"><p>رحم کند بر دل بیمار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار به تعجیل شود بس خراب</p></div>
<div class="m2"><p>پس تو درین کار مکن اضطراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقبت کار تو نیکو شود</p></div>
<div class="m2"><p>صوفی اگر صبر کنی بی عدد</p></div></div>