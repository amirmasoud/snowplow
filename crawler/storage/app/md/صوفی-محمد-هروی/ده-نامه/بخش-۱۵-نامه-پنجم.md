---
title: >-
    بخش ۱۵ - نامه پنجم
---
# بخش ۱۵ - نامه پنجم

<div class="b" id="bn1"><div class="m1"><p>باز چو مسکین بشنید این سخن</p></div>
<div class="m2"><p>نال برآورد تو عیبش مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که گرفتار غم یار شد</p></div>
<div class="m2"><p>ناله او روز و شبان زار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده پر از آب و دلی پر زنار</p></div>
<div class="m2"><p>گر بکشد آه، تو معذور دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه ز عشق تو چها می کشم</p></div>
<div class="m2"><p>روز و شبان درد و بلا می کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل که ز عشق تو پر از آتش است</p></div>
<div class="m2"><p>بر من بیچاره به غایت خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد تو بهتر ز دوای دگر</p></div>
<div class="m2"><p>نیست مرا جز تو هوای دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهرخا تا به تنم جان بود</p></div>
<div class="m2"><p>درد توام مایه درمان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نام تو اوراد زبان من است</p></div>
<div class="m2"><p>درد تو هم صحبت جان من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من به فراق تو گرفتم قرار</p></div>
<div class="m2"><p>دیده نهادم به ره انتظار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاد رقیب و من مسکین به غم</p></div>
<div class="m2"><p>آتش دل می زند اکنون علم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چه کنی من بکشم ای حبیب</p></div>
<div class="m2"><p>از تو درین درد و بلا با نصیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک ترا نیک نیاید، مکن</p></div>
<div class="m2"><p>از من دلسوخته بشنو سخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آرزوی روی تو دارم همین</p></div>
<div class="m2"><p>ماهرخا در همه روی زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشقم و بی کس و بی اعتبار</p></div>
<div class="m2"><p>سینه فرسوده پر از درد یار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رحم نیاید صنما بر منت</p></div>
<div class="m2"><p>روز جزا دست من و دامنت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل ز من خسته ربودی نهان</p></div>
<div class="m2"><p>آه چه سازم، چه کنم این زمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ز فراق تو بر آرم نفیر</p></div>
<div class="m2"><p>بر من مسکین پریشان مگیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صوفی اگر ناله کند همچو نی</p></div>
<div class="m2"><p>ای بت عیار مکن عیب وی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خسته ز در دست که نالد بسی</p></div>
<div class="m2"><p>تا نکشد، نیز چه داند کسی</p></div></div>