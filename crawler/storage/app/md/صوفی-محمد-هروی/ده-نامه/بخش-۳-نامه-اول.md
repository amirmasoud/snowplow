---
title: >-
    بخش ۳ - نامه اول
---
# بخش ۳ - نامه اول

<div class="b" id="bn1"><div class="m1"><p>ای صنم لاله رخ گلعذار</p></div>
<div class="m2"><p>گوش به حال من بیچاره دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو قد سیمبر نوجوان</p></div>
<div class="m2"><p>از من درویش سلامی بخوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآتش غم گشت کباب این دلم</p></div>
<div class="m2"><p>چاره من کن که درین مشکلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحم نما بر دل شیدای من</p></div>
<div class="m2"><p>گر نکنی رحم تو ای وای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد مرا مایه درمان تویی</p></div>
<div class="m2"><p>آرزوی دیده گریان تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تو ندارم سر خویش ای نگار</p></div>
<div class="m2"><p>عشق برآورد ز جانم دمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه من در غم تو چاک شد</p></div>
<div class="m2"><p>دود دلم جانب افلاک شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبر ندارم چه کنم آه آه</p></div>
<div class="m2"><p>سوی من افکن نظری گاه گاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند کشم آه به شبهای تار</p></div>
<div class="m2"><p>چند بگریم ز غمت زار زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل ز تو برداشتنم مشکل است</p></div>
<div class="m2"><p>مهر تو چون همدم جان و دل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چاره من کن که اسیر توام</p></div>
<div class="m2"><p>بیدل و مسکین و فقیر توام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی تو مرا نیست صبوری مجال</p></div>
<div class="m2"><p>مرغ دلم چند زند پر و بال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تو ندارم من مسکین گریز</p></div>
<div class="m2"><p>ای بت عیار مرا دست گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چاره کار من بیچاره کن</p></div>
<div class="m2"><p>دفع دل خسته صد پاره کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر خدا در من مسکین نگر</p></div>
<div class="m2"><p>ای صنم لاله رخ لب شکر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صوفی درویش هوا خواه تست</p></div>
<div class="m2"><p>رد مکنش گو سگ درگاه تست</p></div></div>