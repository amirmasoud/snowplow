---
title: >-
    بخش ۹ - آمدن خادمه
---
# بخش ۹ - آمدن خادمه

<div class="b" id="bn1"><div class="m1"><p>چون که جوابی نشنید از سوال</p></div>
<div class="m2"><p>خادمه باز آمد و برگفت حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت جوانا غم بیهوده چیست</p></div>
<div class="m2"><p>بر تو بباید همه عالم گریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست چو او را سر و پروای تو</p></div>
<div class="m2"><p>بی خبر است از دل شیدای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بدهی جان زغم آن صنم</p></div>
<div class="m2"><p>عاشق دلسوخته، او را چه غم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاره خود کن تو به صبر این زمان</p></div>
<div class="m2"><p>از من مسکین بشنو ای فلان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر گشاینده هر مشکل است</p></div>
<div class="m2"><p>صبر به هر واقعه ای مقبل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست کلید در گنج مراد</p></div>
<div class="m2"><p>صبر، تو بشنو ز من ای خوش نهاد</p></div></div>