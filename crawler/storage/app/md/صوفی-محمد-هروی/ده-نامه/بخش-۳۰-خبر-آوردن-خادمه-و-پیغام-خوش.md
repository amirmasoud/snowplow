---
title: >-
    بخش ۳۰ - خبر آوردن خادمه و پیغام خوش
---
# بخش ۳۰ - خبر آوردن خادمه و پیغام خوش

<div class="b" id="bn1"><div class="m1"><p>خادمه خندان و دل شادمان</p></div>
<div class="m2"><p>آمد و بنشست به پیش جوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت جوان را که شبت گشت روز</p></div>
<div class="m2"><p>هیچ دگر در غم هجران مسوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شام فراق تو به پایان رسید</p></div>
<div class="m2"><p>درد ترا نوبت درمان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخت ببین باز مددگار تست</p></div>
<div class="m2"><p>این اثر دیده بیدار تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت بیا امشب و بر گیر کام</p></div>
<div class="m2"><p>از لب جان بخش من ای نیک نام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق بیچاره چو این را شنید</p></div>
<div class="m2"><p>روح روان از تن او بر پرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیش جفا بر دل او نوش شد</p></div>
<div class="m2"><p>محنت دیرینه فراموش شد</p></div></div>