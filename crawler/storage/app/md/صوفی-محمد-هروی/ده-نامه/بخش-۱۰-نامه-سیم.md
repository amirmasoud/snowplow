---
title: >-
    بخش ۱۰ - نامه سیم
---
# بخش ۱۰ - نامه سیم

<div class="b" id="bn1"><div class="m1"><p>دید چو اندر غم دل مضطرش</p></div>
<div class="m2"><p>خادمه این گفت و برفت از برش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون که شنید این سخن آن دلفگار</p></div>
<div class="m2"><p>قطع طمع دید ز رخسار یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله او تا به ثریا رسید</p></div>
<div class="m2"><p>راست چو مرغی به قفس می طپید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد دلش هر نفس افزون شده</p></div>
<div class="m2"><p>دیده اش از گریه چو جیحون شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رخ دلدار شده نا امید</p></div>
<div class="m2"><p>روز سیاهی و دو چشم سفید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک روان بر رخ چون زعفران</p></div>
<div class="m2"><p>خسته و مجروح دل و ناتوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی به دیوار و غم یار پیش</p></div>
<div class="m2"><p>مضطر و حیران شده در کار خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در غمش آن دلشده بگداخته</p></div>
<div class="m2"><p>و از همه خلق بپرداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر به سر زانو و غم بی شمار</p></div>
<div class="m2"><p>سینه پر از آتش و دل نزد یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زآه دلش خاطر همسایه ریش</p></div>
<div class="m2"><p>بر جگرش فرقت دلبر چو نیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلبر ازو غافل و او بی حضور</p></div>
<div class="m2"><p>سینه تفتان به مثال تنور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>واقف این واقعه نی هیچ کس</p></div>
<div class="m2"><p>دست به سر مانده بسان مگس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او چو نمک گشته گدازان در آب</p></div>
<div class="m2"><p>آن بت عیار اوز در حجاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راست چو مرغی شده در دام او</p></div>
<div class="m2"><p>چشم نهاده به سر بام او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر سر کویش همه شب تا سحر</p></div>
<div class="m2"><p>شسته جوان منتظر و بی خبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خادمه آمد که ترا حال چیست</p></div>
<div class="m2"><p>دید که آن غمزده خون می گریست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رفت دگر بار به سوی حبیب</p></div>
<div class="m2"><p>گفت بگویم سخنی یا نصیب</p></div></div>