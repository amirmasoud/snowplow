---
title: >-
    بخش ۲۶ - رفتن خادمه از بر عاشق نزدیک معشوق
---
# بخش ۲۶ - رفتن خادمه از بر عاشق نزدیک معشوق

<div class="b" id="bn1"><div class="m1"><p>خادمه باز این سخن جان گداز</p></div>
<div class="m2"><p>کرد روان عرضه به آن دلنواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز بیان کرد که او در غم است</p></div>
<div class="m2"><p>روز و شبان با غم تو همدم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز غم تو هیچ ندارد به دل</p></div>
<div class="m2"><p>مانده ز باران مزه پا به گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولوله در جان و دو چشم پر آب</p></div>
<div class="m2"><p>هست درین کوی تو در اضطراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بکنی رحم، بود جای آن</p></div>
<div class="m2"><p>پیر شد امروز درین غم جوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز بخندید چو شهد و شکر</p></div>
<div class="m2"><p>گفت مرا می دهد این دردسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می نتواند که ز من بگذرد</p></div>
<div class="m2"><p>قوت آن نی که به من بنگرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من به میان همه دشمنان</p></div>
<div class="m2"><p>چون کنم امروز دوایی چو آن</p></div></div>