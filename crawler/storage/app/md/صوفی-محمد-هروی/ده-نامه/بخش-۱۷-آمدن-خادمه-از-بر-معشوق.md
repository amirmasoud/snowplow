---
title: >-
    بخش ۱۷ - آمدن خادمه از بر معشوق
---
# بخش ۱۷ - آمدن خادمه از بر معشوق

<div class="b" id="bn1"><div class="m1"><p>خادمه از خدمت آن دلنواز</p></div>
<div class="m2"><p>گشت سوی عاشق بیچاره باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنده زنان با دل بس شادمان</p></div>
<div class="m2"><p>آمد و بنشست به پیش جوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت بیا غم مخور ای مستمند</p></div>
<div class="m2"><p>مرغ دلت یافت گشادی ز بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بکنم فکر تو، فرمود یار</p></div>
<div class="m2"><p>صبر کن و ناله مکن زینهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست مرا ترس زبان رقیب</p></div>
<div class="m2"><p>ورنه ز من یافته بودی نصیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صوفی ازین صبر برآید مراد</p></div>
<div class="m2"><p>کس چو تو بی صبر به عالم مباد</p></div></div>