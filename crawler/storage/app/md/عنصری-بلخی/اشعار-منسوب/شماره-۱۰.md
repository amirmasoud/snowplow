---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ابر زیر و بم شعر اعشیّ قیس</p></div>
<div class="m2"><p>همی زد زننده بمضرابها</p></div></div>