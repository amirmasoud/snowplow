---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای شریعت را قرار و ای مدیحت را مدار</p></div>
<div class="m2"><p>شهریار بامداری پادشاه با قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دین و دانش را ز جبهۀ رای تو باشد فخور</p></div>
<div class="m2"><p>جور و بخشش را ز هیجۀ راد تو باشد مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راح را هنگام لطف آموخت طبع تو شتاب</p></div>
<div class="m2"><p>خاکرا فرحین عفو آموخت علم تو وقار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حمله بشتابد چو رجس رجس نشکیبد ز حمد</p></div>
<div class="m2"><p>عزم و حلمت هر دو کو گویند بشتاب و بدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برد خواند از خصایل برتر و این هر دو راست</p></div>
<div class="m2"><p>بر بنانت اقتداء و بر بیانت افتخار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به اشنج لطف ورزی زو برویانی سخن</p></div>
<div class="m2"><p>ور با خضر کینه توزی زو برانگیزی شرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکجا ابیض نمایی غله برگیرد هوا</p></div>
<div class="m2"><p>هرکجا باره دوانی ذله بردارد غبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم ترا زیبد که باشی فرس را خیرالجواد</p></div>
<div class="m2"><p>هم ترا شاید که باشی علم را فخرالکبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با وفاق تو برویاند همی کانون خرد</p></div>
<div class="m2"><p>با خلاف تو پدید آرد همی سنجر قیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای سهامجدی که گر پرسند ازر کوه ستون</p></div>
<div class="m2"><p>کانکه یاردبد که چون راح تواش بر دوقار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همت تو ملطفت را همچو شایح را رواج</p></div>
<div class="m2"><p>خانۀ تو مملکت را همچو دین را ذوالفقار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با وجود این نشانیها بآواز بلند</p></div>
<div class="m2"><p>در زمان گوید که آن فخرالمم خیرالکبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یوسف الدین آنکه گر بر قیر تا بدرای او</p></div>
<div class="m2"><p>همچنان گردد که نزدش مور باشد کم زمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اردوان اعزاز و راحم جود و جمشید احتشام</p></div>
<div class="m2"><p>لوحیا آیین و کیتر جاه و تقدیر اقتدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای ترا بر مجدیان از روی مجدت امجدی</p></div>
<div class="m2"><p>وی ترا بر مفخران از روی طاعت افتخار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کجا لطفت علق گردد بهار اندر خریف</p></div>
<div class="m2"><p>هر کجا عنفت سمر گردد خریف اندر بهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر زو افدا و فدی بینی زوفد خویش بین</p></div>
<div class="m2"><p>ور ز خور روشنتری یابی زرای خود شمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نزد رایت بیضۀ میخور بی پرتو چو قیر</p></div>
<div class="m2"><p>نزد وفدت تپۀ بر جیس در پستی چو غار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وهم تو چون ذیل عطفت کم پذیرد گرد بخل</p></div>
<div class="m2"><p>ذیل تو چون جیب عصمت برنگیر ز عدعار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با جنابت بی جلال آمد همی چرخ هزبر</p></div>
<div class="m2"><p>با اجارت بی عیار آمد همی غم عیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زشت آن گر بخشششت دارد همی در همبرت</p></div>
<div class="m2"><p>زشت آن گر بیعتت دارد عدو در زینهار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رودت ناصح نوازت را چو فصلت بی نهاست</p></div>
<div class="m2"><p>ابیض عبهر گدازت را چو رمحت آبدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نحم و رجم زود عنفت را نیارم گفت میل</p></div>
<div class="m2"><p>هر یکی گر چار گردند آخشیجان چهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر بصف اندر کنی آهوی اعتدرا حفاظ</p></div>
<div class="m2"><p>ور بعنف اندر دهی محرور آهن را فشار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شمع ازین آهو زبون گردد چو از مسحی حمام</p></div>
<div class="m2"><p>آب از آهن فرو بارد چو از بخشش نثار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سهم با بأس تو هار و حصن با سهم تو هیر</p></div>
<div class="m2"><p>خضر با بد تو شمر و ضیم با بیض تو قار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با وقار و عزم و شمشیرت بصیر عجل و حرب</p></div>
<div class="m2"><p>حمد حرص و حرص حمد و نور هر دو صحب شار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ور بحویی از هرام شمس سازی مرقشیش</p></div>
<div class="m2"><p>ور بگویی از صبا تو و شبی سازی طرار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یک فراس از حبس تو و ز ضیق اعدا صد کرنگ</p></div>
<div class="m2"><p>یک دو شاخ از کف تو وز ظن کیتر صد بهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر کجا رخشت دهد بر تو سعادین چو جلای</p></div>
<div class="m2"><p>هر کجا شهوت کند سرعت رواحین چون کوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از نواهت خالس را سد فوتیا بر میتیان</p></div>
<div class="m2"><p>از نوات جبهت صد آزیون بر احمرار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آبی ای رخ ار مر تو بو ید شود حمش غلال</p></div>
<div class="m2"><p>صص بی مهر تو باید شود بیشین ذخار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در تکلم چیست نیلت شاعی ایلوج سکن</p></div>
<div class="m2"><p>در سخاوت چیست ابلت صفرۀ اکلیل بار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برنگردد آز را از تو اله هرگز بطن</p></div>
<div class="m2"><p>بر سرسید عطایت تا نگردد چشم خوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چیست دستت در سخاوت اخضر و خضر شمر</p></div>
<div class="m2"><p>در شجاعت چیست بیضت ارقم نسرین قبار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آسمانرا در سهامت بر جشن نبود خشنود</p></div>
<div class="m2"><p>زانکه مور او مرا عین است و مر این را عقار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قاف تا قاف جهان بر عقر جودت یک عقیق</p></div>
<div class="m2"><p>پای تا فرق فلک بر آجنابت سلمشار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دشمنت بر شعر مکمخت ماند آهخته تیر</p></div>
<div class="m2"><p>ناصحت درارم دولت مانده را احبحه سهار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نیست گردیدت جناب از بهر چه رو حاعدی</p></div>
<div class="m2"><p>بر همیدست و همی بارد در ریر همه عار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دشمنت راندب بردار شهقه نامد برحدیم</p></div>
<div class="m2"><p>تا بمعجز آمد درون این روزه را حالت گمار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رنج جیش از سمق صد غربال کم برآی</p></div>
<div class="m2"><p>کاسمانرا حاجت صید جزا نبود بهار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اشجمال ای در بر قازبچه اندوختم</p></div>
<div class="m2"><p>و انتباه ای در بر اندیشۀ اندر ختار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هفت اخضر نزد یک اشباح دستت نیم شبر</p></div>
<div class="m2"><p>هفت اسپهپا نزدیک صمصامت بیضۀ سومنار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با علو حضرت تو بی علو آمد سهام</p></div>
<div class="m2"><p>با توان اسود تو بی توان آمد عضار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یک بطالی از تو و از صد جشامی صد ستام</p></div>
<div class="m2"><p>یک بر آویز از تو و از صد ترازی صد غمار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر شهر برخیزد از ایام کوهان بر شهان</p></div>
<div class="m2"><p>کاینچنین ایام را بهر شهر شخرت ایار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر بعالم در بود شیروی نبود جز که حزم</p></div>
<div class="m2"><p>دشمن خدو خزان افهورای شهریار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای ترا بر هر که هست از سروران سرور شدی</p></div>
<div class="m2"><p>وی ترا بر هرچه هست از شهر باران شهریار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>با یکی خاصه ز نافت سهل باشد شهرقی</p></div>
<div class="m2"><p>با یکی نیمه زهانت سهل باشد انتمار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر ز تو فرمان بود تیزی بزآرد حسن باغ</p></div>
<div class="m2"><p>ور زتو باغی آیی بر آرد فتح کار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سیحه از رای تو با اندر قهر صرصهال</p></div>
<div class="m2"><p>حمد از حلم تو بار حیلولم عیار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>می نمیگویم که با کین تو با غم هم شکست</p></div>
<div class="m2"><p>این همی گویم ناعم اندرو به ز انضمار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ملک را آرا بود از تیغ برق آسای تو</p></div>
<div class="m2"><p>قفعلو از بد اشارت همی مهن تبار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ارقم رمح ترا اعجاز بیضای قرین</p></div>
<div class="m2"><p>بختم بختم ترا اقبال یزدی مبار</p></div></div>