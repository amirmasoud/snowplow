---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>بگویش که من نامه‌ای نغزناک</p></div>
<div class="m2"><p>فراز آوریدستم از مغز پاک</p></div></div>