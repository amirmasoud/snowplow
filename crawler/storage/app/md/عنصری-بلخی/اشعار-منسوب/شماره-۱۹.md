---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>از صفات حرام لفظی را </p></div>
<div class="m2"><p>باز گردان و پس مصحف کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بدانی که آن مصحف چیست</p></div>
<div class="m2"><p>ضد او گیر و نقش بر کف کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودن دال پیش او بنگار</p></div>
<div class="m2"><p>عرب اندر عجم مؤلف کن</p></div></div>