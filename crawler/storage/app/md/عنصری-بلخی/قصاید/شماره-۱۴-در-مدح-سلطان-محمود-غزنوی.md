---
title: >-
    شمارهٔ ۱۴ -  در مدح سلطان محمود غزنوی
---
# شمارهٔ ۱۴ -  در مدح سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>چه چیزست رخساره و زلف دلبر</p></div>
<div class="m2"><p>گل مشگبوی و شب روز پرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل اندر شده زیر نور سته سنبل</p></div>
<div class="m2"><p>شب اندر شده زیر خورشید انور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همانا که خورشید رنگ لبش را </p></div>
<div class="m2"><p>بدزدد که بخشد بیاقوت احمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخش گلستانست و میگون لبانش</p></div>
<div class="m2"><p>بگونه به اردی بهشت و به آذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرنگ رخش پر گل سرخ مجلس</p></div>
<div class="m2"><p>زرنگ لبش پر می لعل ساغر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکوتر ز روشن رخش تیره زلفش</p></div>
<div class="m2"><p>وگر چند روشن ز تیره نکوتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکوتر ز فربی است لاغر میانش</p></div>
<div class="m2"><p>وگر چند فربی نکوتر ز لاغر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی تابد آن زلف مشکینش دایم</p></div>
<div class="m2"><p>همی جوشد آن خط چفته چو چنبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بتابد بگل بر علی حال سنبل</p></div>
<div class="m2"><p>بجوشد بر آتش علی حال عنبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بماه منوّرش ماننده کردم</p></div>
<div class="m2"><p>مرا روز شب کرد ماه منوّر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبم روز شد باز چون بازگشتم</p></div>
<div class="m2"><p>ز ماه منور بشاه مظفر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهاندار محمود کاندر محامد</p></div>
<div class="m2"><p>یکی عالم است از کفایت مصور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یمین است مر دولت ایزدی را</p></div>
<div class="m2"><p>امین است بر حکم دین پیمبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی همتش را بخیر آزمودم </p></div>
<div class="m2"><p>کز آیات رایات او هست مفخر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو دولت جوان و چو دانش به نیرو</p></div>
<div class="m2"><p>چو آتش بلند و چو دریا توانگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز عرعر تراشند منبرش ازیرا</p></div>
<div class="m2"><p>نریزد ز باد خزان برگ عرعر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به غزنی کشد بر صنوبر عدو را</p></div>
<div class="m2"><p>ازان خیزد از کوه غزنی صنوبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر چوب عودست و کافور و چندن</p></div>
<div class="m2"><p>از آنست کش چوب تختست و منبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایا زیردست تو هرچ آن مجسم </p></div>
<div class="m2"><p>ایا زیر قدر تو هرچ آن مقدّر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه سعدی بگردون ترا نامساعد</p></div>
<div class="m2"><p>نه مرزی بگیتی ترا نامسخر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کند زشت را فعل رای تو نیکو</p></div>
<div class="m2"><p>کند سنگ را فعل خورشید گوهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو آنی که زرین شود کشتۀ تو</p></div>
<div class="m2"><p>به پیش خدای جهان روز محشر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که زرین شود رویش و مانده باشد</p></div>
<div class="m2"><p>ز پیکان تو استخوانهاش پر زر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نکارد بهندوستان زعفران کس</p></div>
<div class="m2"><p>از آن پس که شان زغفران بود زیور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازیرا که شان باشد از هیبت تو</p></div>
<div class="m2"><p>همه ساله بی زعفران رخ مزعفر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدان سنگ رنگ آتش آب چهره</p></div>
<div class="m2"><p>نه آب و نه آتش هم آب و هم آذر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درختی است گویی بمینا منقش</p></div>
<div class="m2"><p>پرندیست گویی بلؤلؤ مشجر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز دیبای رومی ستاره نماید</p></div>
<div class="m2"><p>ز پولاد هندی پرند مطیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زمانست چون گوهر او مجسم </p></div>
<div class="m2"><p>سپهرست چون شکل او نامدور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رونده است و رفتنش در مغز شیران</p></div>
<div class="m2"><p>خورنده است و خوردنش هم جان کافر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه با بند و آثار او بند دولت</p></div>
<div class="m2"><p>نه با پشت و آثارش او پشت لشکر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه وهمست و گشتنش چون وهم در دل</p></div>
<div class="m2"><p>نه مغزست و بودنش چون مغز در سر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه رخشد چو او رخشد از گرد هیجا</p></div>
<div class="m2"><p>درخش مصفّا ز ابر مکدّر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بوفتی که گرد سواران برآید</p></div>
<div class="m2"><p>بپوشد زمین و بجوشد معسکر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در اندر اجلها املها گشاده</p></div>
<div class="m2"><p>اجلها شده با املها برابر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو آنجا چنان باشی ای شاه گیتی</p></div>
<div class="m2"><p>که باشد میان گوزنان غضنفر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز فرّ تو ظاهر شده رزم دشمن</p></div>
<div class="m2"><p>ز پیروزی کوس تو گوش او کر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بجان عدو بر تو خط اجل را</p></div>
<div class="m2"><p>قلم سازی از تیغ وز نیزه مسطر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شگفت آید از مرکب تو خرد را</p></div>
<div class="m2"><p>کش از باد طبعست و از خاک منظر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو تختست بر جای و چون مرغ پران</p></div>
<div class="m2"><p>قوامیش هم پایۀ تخت و هم پر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زمان گذشته است کاندر گذشت او</p></div>
<div class="m2"><p>ازیرا کش اندر نیابد کس آور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برجعت بدانگونه باشد که گویی</p></div>
<div class="m2"><p>همی بازگردد زمانه مکرر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بکردار کشتی ولیکن نه کشتی</p></div>
<div class="m2"><p>چو کشتی به پرد ز معبر بمعبر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نجنبد چو لنگر گران گشت کشتی</p></div>
<div class="m2"><p>روان گردد او کش گرانست لنگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نپرد بکشتی کس این نوع هرگز</p></div>
<div class="m2"><p>که پری تو ای شاه گیتی بدو در</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ببالا چو صندوق نمرود باشد</p></div>
<div class="m2"><p>بدریا چو صندوق فرخ سکندر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو وهم اندر آید بهیجا زبیره</p></div>
<div class="m2"><p>چو روز اندر آید به بیدا ز کردر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بگام پسین بردود گر برانی</p></div>
<div class="m2"><p>به تقریبش از باختر تا بخاور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه جستن کند کم ز دریا بدریا</p></div>
<div class="m2"><p>نه منزل کند کم ز کشور بکشور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز پیلان جنگیت گر وصف گویم</p></div>
<div class="m2"><p>ندارد خردمند نادیده باور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه چرخند لیکن همه چرخ گردش</p></div>
<div class="m2"><p>نه کوهند لیکن همه کوه پیکر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از ایشان بلا بر سر بد سگالان</p></div>
<div class="m2"><p>وز ایشان تباهی بر اعدای ابتر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو اندر هوا کوه بر قوم موسی</p></div>
<div class="m2"><p>چو بر قوم عاد آیت باد صرصر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چنان گردد از عرضشان دشت گویی</p></div>
<div class="m2"><p>بموج اندر آید همی بحر اخضر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو زنجیر داود خرطوم ایشان</p></div>
<div class="m2"><p>که آویخته بد ز چرخ مدور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بگردون گردنده مانند و زیشان</p></div>
<div class="m2"><p>جهانرا هم از خیر بهره ، هم از شر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز گردون روان رجم تابنده انجم</p></div>
<div class="m2"><p>از ایشان روان شلّ و تابنده خنجر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زمین کوه باشد چو آیند پیدا</p></div>
<div class="m2"><p>چو اندر گذشتند چاه مقعر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بتک راه گیرند بر آب و آتش </p></div>
<div class="m2"><p>بدندان بدرّند پولاد و مرمر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ایا پادشاهی که حکم جهان را</p></div>
<div class="m2"><p>ز ایزد جز از تو نبودست داور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دو نعمت بزرگ آمده در دو گیتی </p></div>
<div class="m2"><p>ز دنیا کف تو ز فردوس کوثر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نشد جز بتو پادشاهی ستوده</p></div>
<div class="m2"><p>نشد جز بتو شهریاری مشهر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تویی و آفتابست دهر و فلک را</p></div>
<div class="m2"><p>یکی جود گستر یکی نور گستر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ازو نزد تو نور و دایم تو اینجا</p></div>
<div class="m2"><p>ز تو نزد او قدر و او دایم ایدر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جهان و بزرگی و دولت تو داری</p></div>
<div class="m2"><p>مر این هر سه را بگذران و بمگذر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز بهر تو دولت ، نه تو بهر دولت</p></div>
<div class="m2"><p>ز بهر سر افسر ، نه سر بهر افسر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ثنا جانور گشت با سیرت تو</p></div>
<div class="m2"><p> ز هر چیز حکم بقارا مدخر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سخن : جسم و جان و خرد : نظم و معنی</p></div>
<div class="m2"><p>قلم : عمر و سمع و بصر : حبر و دفتر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همی تا نسوزد بآب اندر آذر</p></div>
<div class="m2"><p>نگیرد عقاب ژیان را کبوتر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>جهان گیر و کینه کش از بدسگالان</p></div>
<div class="m2"><p>ملک باش و از نعمت و ملک برخور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>متابع ترا دولت و عید فرّخ</p></div>
<div class="m2"><p>مسخر ترا عالم و بخت چاکر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ولی را همه طالع سعد بیحد</p></div>
<div class="m2"><p>عدو را همه اختر نحس بیمر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>زهر ماده ای نرش فاضلتر آید</p></div>
<div class="m2"><p>در اعدای تو ماده فاظلتر از نر</p></div></div>