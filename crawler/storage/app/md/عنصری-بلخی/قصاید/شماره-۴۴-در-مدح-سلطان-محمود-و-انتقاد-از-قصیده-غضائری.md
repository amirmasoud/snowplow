---
title: >-
    شمارهٔ ۴۴ -  در مدح سلطان محمود و انتقاد از قصیدهٔ غضائری
---
# شمارهٔ ۴۴ -  در مدح سلطان محمود و انتقاد از قصیدهٔ غضائری

<div class="b" id="bn1"><div class="m1"><p>خدایگان خراسان و آفتاب کمال</p></div>
<div class="m2"><p>که وقف کرد برو ذوالجلال عزّو جلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یمین دولت و دولت بدو نموده هنر</p></div>
<div class="m2"><p>امین ملت و ملت بدو گرفته جمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی خدای ز بهر بقای دولت او</p></div>
<div class="m2"><p>از آفرینش بیرون کند فنا و زوال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی درخت بر آمد ز جود او بفلک</p></div>
<div class="m2"><p>که برگ او همه جاه است و بار او همه مال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار خندان از رنگ آن درخت اثر</p></div>
<div class="m2"><p>درخت طوبی از شاخ آن درخت مثال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن به هشت بهشت آیتی است روز قضا</p></div>
<div class="m2"><p>وزین به هفت زمین نعمتی است گاه نوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آن عطا که پراکنده داد جمع شود</p></div>
<div class="m2"><p>ز حدّ دریا بیش آید و ز وزن جبال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عقل خاطر او را هزار مرتبتست</p></div>
<div class="m2"><p>چو چرخ همت او را دو صد هزار خیال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه آب بحر ز ابر سخای او قطره است</p></div>
<div class="m2"><p>نه سنگ کوه بوزن عطای او مثقال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو نام او شنوی شادمانه گردد دل</p></div>
<div class="m2"><p>چو روی او نگری فر خجسته گردد فال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بهمت او بودی اصل و غایت ملک</p></div>
<div class="m2"><p>فلکش دیوان بودی ، ستارگان عمال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگرش پیش نیاید بجود بحر و جبل</p></div>
<div class="m2"><p>بپیشش آید جبر و قدر بروز قتال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر بترک بکاوند مشهد ایلک</p></div>
<div class="m2"><p>وگر به هند بجویند دخمۀ چیپال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خاک تیره خروش هزیمتی شنوند</p></div>
<div class="m2"><p>چنانکه زو بزمین اندر اوفتد زلزال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز زخم آن گهر آگین پرند مینا رنگ</p></div>
<div class="m2"><p>ز گام آن فرس مهر سمّ ماه نعال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بترک جایگهی نیست ناشده رنگین</p></div>
<div class="m2"><p>به هند ناحیتی نیست ناشده اطلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا ستارۀ تأیید و عالم تو قیر</p></div>
<div class="m2"><p>قوام و قاعدۀ ملک و قبلۀ اقبال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز سال و ماه نویسند مردمان تاریخ</p></div>
<div class="m2"><p>بتو نویسد تاریخ خویشتن مه و سال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بهر کجا خردست و بهر کجا هنرست</p></div>
<div class="m2"><p>همی ز دانش و کردار تو زنند مثال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خرد هنر نکند تا نخواهد از تو نظر</p></div>
<div class="m2"><p>هنر اثر نکند تا نگیرد از تو مثال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هوا که تیر تو بیند بر آیدش دندان</p></div>
<div class="m2"><p>اجل که تیغ تو بیند بریزدش چنگال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درنگ ز امر تو آموخته است خاک زمین</p></div>
<div class="m2"><p>شتاب ز اسب تو آموخته است باد شمال </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز بیم تیغ تو تیره بود دل کافر</p></div>
<div class="m2"><p>بنور دین تو روشن بود دل ابدال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سیاست تو بگیتی علامت مهدیست </p></div>
<div class="m2"><p>کجا سیاست تو نیست ، فتنۀ دجال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>«بس ای ملک » ز عطای تو خیره چون گویند</p></div>
<div class="m2"><p>که «بس» نشان ملالت بود ز کبر دلال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه بس بود که تو بر خلق رحمتی ز ایزد</p></div>
<div class="m2"><p>بجای رحمت ایزد خطاست لفظ ملال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همینکه گفت همه فخر شاعران بمن است</p></div>
<div class="m2"><p>ز شعر گویان پرسید بایدش احوال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر بدعوی او شاعران مقر آیند</p></div>
<div class="m2"><p>درست گشت و نماند اندرین حدیث جدال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فغان کنند ز جودت ، فغان نباید کرد</p></div>
<div class="m2"><p>فغان ز محنت و از رنج باید و اهوال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همینکه گوید : از شاعری مرا بس بود</p></div>
<div class="m2"><p>اگر بداندش از شاعری بسست مقال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نماند گوید ازین بیش جای شکر مرا</p></div>
<div class="m2"><p>بهر دو گیتی در روزنامۀ اعمال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگفته شکر چنین بیکرانه جاه گرفت</p></div>
<div class="m2"><p>اگر بگفتی خود چند یافتی اجلال ؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ترا نصیحت کردست کز کفایت و جود</p></div>
<div class="m2"><p>کرانه گیر و بتقدیر سال بخش اموال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه بسته گشت ترا دخل کت نماند چیز</p></div>
<div class="m2"><p>نه جز گشادن ملک است فعل تو ز افعال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کدام سال بود کاندرو تو نستانی</p></div>
<div class="m2"><p>ولایتی که زر و مال او فزون ز رمال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همی بگوید کاندر تو آن همی شنوم</p></div>
<div class="m2"><p>که در مسیح ز جهال و جملۀ عذال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر خدای بخواهد نگفت و آن بترست</p></div>
<div class="m2"><p>که گفت وصف ترا در روایت جهال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان خبر که شنیدم ز معجزات مسیح</p></div>
<div class="m2"><p>عیانش در تو همی بینم ای شه ابطال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر بدعوت او مرده زنده کرد خدای</p></div>
<div class="m2"><p>خرد بحجت تو رسته شد ز بند ضلال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیاز کشته ز جود تو زنده گشت بسی</p></div>
<div class="m2"><p>گشاده کف تو پوشیدش از بقا سربال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ملک فریب نهادست خویشتن را نام</p></div>
<div class="m2"><p>کش از عطای تو ای شاه خوب گشت احوال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>غلط کند که کس اندر جهان ترا نفریفت</p></div>
<div class="m2"><p>نرفت و هم نرود در تو حیلت محتال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر فریفته باشد کسی بدادن چیز</p></div>
<div class="m2"><p>فریفته است بروزی مهیمن متعال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مگر نداند اندازۀ عطات همی</p></div>
<div class="m2"><p>که صرّه هاش همی بدره گشت و بدره جوال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زمین بسیم تو سیمین همی کند چهره</p></div>
<div class="m2"><p>هوا بزرّ تو زرّین همی کند اشکال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دویست خدمت تو بار نیست بر یکدل</p></div>
<div class="m2"><p>یکی عطای تو بارست بر دو صد حمال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سؤال رفتی پیش عطا پذیره کنون</p></div>
<div class="m2"><p>همی عطای تو آید پذیره پیش سؤال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نخست گفت که بس از عطا که سیر شدم</p></div>
<div class="m2"><p>بکرد باز تقاضای بدره و خرطال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>محال باشد سیری نمودن از نعمت</p></div>
<div class="m2"><p>دگر بریدن از خدمت تو نیز محال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چه عرضه باید کردن بفخر خدمت خویش</p></div>
<div class="m2"><p>بر آن کسی که جهان بر سخای اوست عیال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بخاره بر بنتابد فروغ طلعت شمس</p></div>
<div class="m2"><p>بشوره بر بنبارد سرشک آب زلال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر نه عمر من از بهر خدمتت خواهم </p></div>
<div class="m2"><p>حرام کردم بر خویشتن هر آنچه حلال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز عمر مرد چه جوید فزون ز خدمت تو</p></div>
<div class="m2"><p>بدشت یوز چه خواهد به از سرین غزال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جز آنکه بست و ببندد بخدمت تو میان</p></div>
<div class="m2"><p>که آسمانش مطیعست و بخت نیک سگال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه با ولیت ببزم تو ماند اصل نیاز</p></div>
<div class="m2"><p>نه با عدوت برزم تو ماند اصل جدال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کند حسام تو ز اسقف تهی بلادالروم</p></div>
<div class="m2"><p>چنانکه کشور هند از برهمن و چیپال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>قضا نشان علامت کنی بجای حریر</p></div>
<div class="m2"><p>قدر عنان جنیبت کنی بجای دوال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نهی بپای عدو بر اجل بشکل شکیل</p></div>
<div class="m2"><p>که هست زخم ترا شیر شرزه شکل شکال</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر بنور کسی خاک را صفت گوید</p></div>
<div class="m2"><p>از آن صوابتر آید که مر ترا بهمال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر ببزم تو دریا بود خزینۀ تو</p></div>
<div class="m2"><p>بیک عطای تو بیشک سراب گردد و نال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همیشه تا فلک است و جهان و جانورست</p></div>
<div class="m2"><p>همی بخندد آجال بر سر آمال</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دوام دولت را با تو باد مهر و وفا</p></div>
<div class="m2"><p>قوام ملت را با تو باد قرب و وصال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هنر بطبع بپرور سخن بفضل بگوی</p></div>
<div class="m2"><p>جهان بعدل بگیر و عدو به تیغ بمال</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ایا غضایری ای شاعری که در دل تو</p></div>
<div class="m2"><p>بجز تو هر که بود جمله ناقص اند ونکال</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نگاهدار تو در خدمت ملوک زبان</p></div>
<div class="m2"><p>بجد بکوش و مده عقل را بهزل و هزال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بیک دو بیت حدیث شریف گفته بدی</p></div>
<div class="m2"><p>چنانکه از غرضت نقش بر نبد تمثال</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دو نوع را تو ز یک جنس می قیاس کنی</p></div>
<div class="m2"><p>مجانست نبود در میان زرّ و سفال</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اگر بگفتن مفضال فاضلست بد قصد</p></div>
<div class="m2"><p>نخست باری بشناس فاضل از مفضال</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در آنکه قسمت کردی نکو تأمل کن</p></div>
<div class="m2"><p>اگر بگرد دولت عقل را ره است و مجال</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هنر بدست بیانست از اختیار سخن</p></div>
<div class="m2"><p>چنانکه زیر زبانست پایگاه رجال</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زیادتی چکنی کان بنقص باز شود</p></div>
<div class="m2"><p>کزین سبیل نکوهیده گشت مذهب غال</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مباش کم ز کسی کو سخن نداند گفت</p></div>
<div class="m2"><p>ز لفظ معنی باید همی نه بالابال</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از آنکه خواهد گفت اشارتی بکند</p></div>
<div class="m2"><p>اگر بحرف بگردد زبان مردم لال</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سخن فرستی خام و نبشته بر سر شعر</p></div>
<div class="m2"><p>بجای تاج نهی بیهده همی خلخال</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چنان مخاطبه از شاعران نکو نبود</p></div>
<div class="m2"><p>که این مخاطبه باشد همال را بهمال</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ازو رسید بتو نقد سه هزار درم</p></div>
<div class="m2"><p>ز بنده بودن او چون کشید باید یال</p></div></div>