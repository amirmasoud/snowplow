---
title: >-
    شمارهٔ ۶۰ -  در مدح سلطان محمود غزنوی
---
# شمارهٔ ۶۰ -  در مدح سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>قویست دین محمد بآیت فرقان</p></div>
<div class="m2"><p>چنانکه حجت سلطان به رایت سلطان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یمین دولت و پیراسته بتیغش ملک</p></div>
<div class="m2"><p>امین ملت و آراسته بدو ایمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خیر هرچه رسول خدای را خبرست</p></div>
<div class="m2"><p>همی نماید از سایۀ خدای عیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسول گفت که بیغوله های روی زمین</p></div>
<div class="m2"><p>مرا همه بنمودند از کران بکران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزین سپس برسد دست و تیغ محمودی</p></div>
<div class="m2"><p>بهر کجا بنمودند ازو مرا یکسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی درست شود آنکه مصطفی فرمود</p></div>
<div class="m2"><p>کنون بحکم خدای از خدایگان جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب مدار تو زو این صفت که دولت او</p></div>
<div class="m2"><p>خدای را غرضست و رسول را برهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه از قبل آفرین و خدمت او</p></div>
<div class="m2"><p>خرد گشاده زبانست و کلک بسته میان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیک سفر ملکانرا نبود جز یک فتح</p></div>
<div class="m2"><p>وگر ببود ازو سود بود و بود زیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سفر یکیست خداوند را و پنجه فتح</p></div>
<div class="m2"><p>کزو نکرد یکی اردشیر و نوشروان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دزی گشاده که وهم اندر و بود عاجز</p></div>
<div class="m2"><p>رهی بریده که دیو اندر و شود حیران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>براند خسرو مشرق بسوی بیلارام</p></div>
<div class="m2"><p>بدان حصاری کز برج وی خجل ثهلان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی بیابان بود اندر آن نواحی صعب</p></div>
<div class="m2"><p>که بود پهناش از رود سند تا هند آن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بطول و عرض همی کرد با سپهر مری</p></div>
<div class="m2"><p>زبس نشیب همی بست با سقر پیمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بروز از بر سر آفتاب چون آتش</p></div>
<div class="m2"><p>بزیر پای بشب سنگریزه چون پیکان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بچاره بودی گر بودی اندر و نخچیر</p></div>
<div class="m2"><p>به بیم رفتی گر رفتی اندرو شیطان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رهی شکسته تر از عهد مردم بیدین</p></div>
<div class="m2"><p>درازتر زغم یار در شب هجران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بساطهاش همه سنگهای همچو خسک</p></div>
<div class="m2"><p>نباتهاش همه خارهای چون سوهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به خار غیبه ربودی درختش از حوشن</p></div>
<div class="m2"><p>بلمس جامه دریدی گیاهش از خفتان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان قعیر که هنگام برگذشتن ازو</p></div>
<div class="m2"><p>کسی ندید ز پیل بلند ، جز پالان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان گذشتی زو شاه خسروان گفتی</p></div>
<div class="m2"><p>که باد مرکب او را گرفته بود عنان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز موج آب چو بگذشت رایت منصور</p></div>
<div class="m2"><p>فکند دولت او مر فتوح را بنیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم از نخست به شر ساوه برکشیده سپاه</p></div>
<div class="m2"><p>یکی حصاری کش سر برابر سرطان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بپشت ماهی قعرش ، بماه کنگره ها</p></div>
<div class="m2"><p>ز سنگ خاره مر او را قواعد و ارکان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگرد خندق او بر دمیده بیشه ز رمح</p></div>
<div class="m2"><p>چنان که غرم در آن بیشه نگذرد آسان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بساعتی بگرفت آن حصار و غارت کرد</p></div>
<div class="m2"><p>خدایگان زمین خسرو حصار ستان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درونه سایر ماند و نه طایر از بر خاک</p></div>
<div class="m2"><p>دو لک ز لشکر او شد بزیر خاک نهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حصار دیگر بکواره شد که شاه عجم</p></div>
<div class="m2"><p>بکندش از بن و یک ساعتش نداد امان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرادش آنکه زیادت کند مر ایمانرا</p></div>
<div class="m2"><p>بکفر و لشکر کفر اندر آورد نقصان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حصار دیگر برنه ، امیر او هردت</p></div>
<div class="m2"><p>سپاه او قوی و گنج خانه آبادان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرفت حصنش و پیلان و گنج او برداشت</p></div>
<div class="m2"><p>حصاریانش مسلمان شدند پیر و جوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دگر حصار مهاون که برجش از بالا</p></div>
<div class="m2"><p>همی ببستی با چرخ آسمان پیمان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همی بنالد گفتی زمین و رنجه شود</p></div>
<div class="m2"><p>ز بار بارۀ آن سنگپاره شارستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بگرد خندق او بیشه ای که هرگز وهم</p></div>
<div class="m2"><p>بدو درون نتواند شد از کران بکران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در او سپاهی محکم چو کوه و جمله چو ابر </p></div>
<div class="m2"><p>ز تیزی آتش و از مره قطرۀ باران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز جان خویش بپرخاش دست شسته همه</p></div>
<div class="m2"><p>برزمگه بکف دست بر نهاده روان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فروغ تیغ یمانی بدیتشان به نبرد </p></div>
<div class="m2"><p>شعاع داده چو بهرام در کف کیوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدان حصار درون لشکری قوی گر چند</p></div>
<div class="m2"><p>فریفته شده و ایمن نشسته از حدثان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی بگفت که : با من که بس بود به سپاه</p></div>
<div class="m2"><p>به گنج خانه و پیلان آهنین دندان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو دید رایت منصور شاه بر در حصن</p></div>
<div class="m2"><p>فرو گرفت گریبانش ناگهان خذلان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به مغز ، قصد سر تیغ های آینه رنگ</p></div>
<div class="m2"><p>به دیده قصد سر نیزه های خون افشان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نخست رزمی پیوست کز نهیب و شعاع</p></div>
<div class="m2"><p>سپهر اخضر را باز داشت از دوران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی زدندی شمشیر آهوان سرای</p></div>
<div class="m2"><p>دو زلفشان به سمن بر همی زدی چوگان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>حصار و نعمت از آن لشکر قوی بستد</p></div>
<div class="m2"><p>بیک چهار یک از روز خسرو ایران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو دید نصرت شاه زمانه و دانست</p></div>
<div class="m2"><p>بدست او اجل خویش را بدید عیان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گریخت ، خویشتن اندر میان آب افکند</p></div>
<div class="m2"><p>بکشت خویشتن و دیگران در آب روان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی در آب فکندند خویشتن قومش</p></div>
<div class="m2"><p>دو صد هزار فزون از رجال و از نسوان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وگرچه هست دگر من دگر نگویم از آنک</p></div>
<div class="m2"><p>دراز گردد اگر گویم از فلان و فلان</p></div></div>