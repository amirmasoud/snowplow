---
title: >-
    شمارهٔ ۲۰ -  در مدح سلطان محمود غزنوی
---
# شمارهٔ ۲۰ -  در مدح سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>اگر به تیر مه از جامه بیش باید تیر</p></div>
<div class="m2"><p>چرا برهنه شود بوستان چو آید تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر زره نبرد باد بر هوای لطیف</p></div>
<div class="m2"><p>چنین که برد زره پاره ها صغیر و کبیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر فرو شود آهن بآب ، و طبع اینست</p></div>
<div class="m2"><p>چرا برآید جوشن همی بر وی غدیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رز از فراق صبا خون گری و زرد رخیست</p></div>
<div class="m2"><p>رخان زردش برگست و خون دیده عصیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خون شده است سرشک رزان ناشده خون</p></div>
<div class="m2"><p>که رز بصورت پیران شده است ناشده پیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رز ار ز پیری پژمرد و تیره گشت رواست</p></div>
<div class="m2"><p>جوان و تازه و روشن بسست دولت میر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یمین دولت عالی امین ملت حق</p></div>
<div class="m2"><p>که زیر طاعت و عصیان اوست خلد و سعیر </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدای عزّوجلّ آنچه تو بیندیشی</p></div>
<div class="m2"><p>بیافرید و مر او را نیافرید نظیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلوح بر چو قلم رفت از ابتدا سیرش</p></div>
<div class="m2"><p>همی نبشت و همی گفت مدح او بصریر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همیشه هست چهارم سپهر حاسد چوب</p></div>
<div class="m2"><p>از آنکه او را چو بین بود حنا و سریر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سند و هند ز عکس رخ هزیمتیانش</p></div>
<div class="m2"><p>مر ارغوان را نتوان شناختن ز زریر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بصیر اگر بعداوت بسوی او نگرد</p></div>
<div class="m2"><p>برون جهد ز قفا دیده از دو چشم بصیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هوای او بلطیفی بصر برون آرد</p></div>
<div class="m2"><p>چو بوی پیرهن یوسف دو چشم ضریر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدانکه آرد عفو و عطا برد بر او</p></div>
<div class="m2"><p>ز بیگناه غنی بر گناهکار فقیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدای سخت و قوی گفت باش آهن را</p></div>
<div class="m2"><p>ز بهر آنکه دو بود اندر آهنش تدبیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی که تیغ بود زو بدست شاه اندر</p></div>
<div class="m2"><p>دگر که باشد در گردن عدو زنجیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هنر سرشته کند یا گهر برشته کند </p></div>
<div class="m2"><p>محرّری که کند مدح شاه را تحریر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بلفظ دریا گویی ، کفش بود معنی</p></div>
<div class="m2"><p>بخواب دولت بینی ، رخش بود تعبیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه مر جلالت را جز از خصال او اصلست</p></div>
<div class="m2"><p>نه مر کفایت را جز از رسوم او تفسیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز مس و روی با کسیر زر کنند همی</p></div>
<div class="m2"><p>ز نطق زر کند از مدح او به از اکسیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنات براند تدبیر ها که پنداری</p></div>
<div class="m2"><p>همی برابر تدبیر او رود تقدیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ببوسه دادن نامش بمدح در عنوان </p></div>
<div class="m2"><p>فرو دود بصر از دیده سوی دست دبیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزرگ همتش اندر ستارگان سپهر</p></div>
<div class="m2"><p>سخن بواسطه پیدا کند همی بسفیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز قوت حرکاتش همی ز سیاره</p></div>
<div class="m2"><p>منجمان نشناسند خیر را ز شریر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همیشه بودی تأثیر آسمان بزمین</p></div>
<div class="m2"><p>ز فضل اوست کنون اندر آسمان تأثیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز حلم او اثر ناقصست کوه بلند</p></div>
<div class="m2"><p>ز خشم او عرض زایلست چرخ اثیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو شاه قصد عدو کرد ، ور چه دور بود</p></div>
<div class="m2"><p>اجل پذیره شود آردش گرفته اسیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدانکه تیر کشیده است شاه حمله کند</p></div>
<div class="m2"><p>ز باد حمله بسوفار زه بدرّد تیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قیاس شاه چو ابر و محامدش چو سرشک</p></div>
<div class="m2"><p>ضمیر ما چو صدف شاعری چو بحر غزیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بجود مر کف او را همی حسد کند ابر</p></div>
<div class="m2"><p>از ان سیه ز حسد گشت روی ابر مطیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گهی ز گرد سپاهش زمانه سرمه کند </p></div>
<div class="m2"><p>گهی بخویشتن اندر دمد بجای عبیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنان زیند بشادی موافقان ملک</p></div>
<div class="m2"><p>کز آسمان نبود بر مرادشان تقصیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بجاه و علم و باقبال و فضل و عزّ و هنر</p></div>
<div class="m2"><p>با من و دین و زی و عقل و رتبت و تو قیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مخالفان را از بیم او همی دارد</p></div>
<div class="m2"><p>چنانکه دم نتوانند زد مگر ز زحیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برنج آز و بذلّ نیاز و شدّت فقر</p></div>
<div class="m2"><p>بجهد مور و ببانگ درای و زاری زیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز بسکه بیند پیکان شاه روز شکار</p></div>
<div class="m2"><p>به کوه زرین گشته ست دیدۀ نخجیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز حرص مدحش اندر زمین ایرانشهر</p></div>
<div class="m2"><p>همی بروید شعر ار پراکنند شعیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جگر شکافد هنگام زخم ، شمشیرش </p></div>
<div class="m2"><p>بطبع شیر ، مگر شیرش آب داد بشیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه مرکب او عالمی است پر حرکات</p></div>
<div class="m2"><p>همی خورد حرکات سپهر ازو تشویر </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بکوه ماند و سیر ستارگان دارد </p></div>
<div class="m2"><p>بود عجب که کند کوه چون ستاره مسیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بزیر پای مر او را چه دشت و چه دریا</p></div>
<div class="m2"><p>چه قلعه های فلک برج بیستون همشیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدست کندن مر نعل را بسنگ سیاه </p></div>
<div class="m2"><p>فرو نشاند چونانکه سنگ را بخمیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدایگانا عزم تو فال فتح دهد</p></div>
<div class="m2"><p>ز مهرگان همایون بفتح مژده پذیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جهان هر آنچه گرفتی ببندگان دادی</p></div>
<div class="m2"><p>ز بهر آنکه نماند آنچه را که مانده بگیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همیشه تا ز مدار سپهر و گردش روز</p></div>
<div class="m2"><p>گهی هلال بود ماه و گاه بدر منیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بزیر دست تو باد این جهان و نعمت او</p></div>
<div class="m2"><p>اگر چه همت تو بیش ازین جهان حقیر</p></div></div>