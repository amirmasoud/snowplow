---
title: >-
    شمارهٔ ۲۶ -  در مدح سلطان محمود غزنوی
---
# شمارهٔ ۲۶ -  در مدح سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>مراد عالم و شاه زمین و گنج هنر</p></div>
<div class="m2"><p>قوام ملک و نظام هدی و فخر بشر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یمین دولت و دولت بدو فزوده شرف </p></div>
<div class="m2"><p>امین ملت و ملت بدو گرفته خطر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهار چیز بود در چهار وقت نصیب</p></div>
<div class="m2"><p>خدایگان جهانرا چو کرد رای سفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو عزم کرد : صواب و چو رای زد : توفیق</p></div>
<div class="m2"><p>چو باز گردد : فتح و چو جنگ کرد : ظفر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مراد او ز همۀ خلق حاصل است بدو </p></div>
<div class="m2"><p>نه حکم طالع بایدش نه سپاه و حشر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلند همت او را همه فلک تبعست</p></div>
<div class="m2"><p>بزرگ دولت او را همه جهان لشکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزیر سایۀ جاهش بود کفایت و فخر</p></div>
<div class="m2"><p>بزیر رایت رایش بود قضا و قدر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهاری ابر چو دستش بدیدگاه عطا</p></div>
<div class="m2"><p>همه سخاوت خویشش نمود هزل و هدر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخویشتن بر خندید و از حسد بگریست</p></div>
<div class="m2"><p>دلیل خنده ش رعدست و آب دیده مطر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلشکر عدو اندر چو رای حرب کند</p></div>
<div class="m2"><p>پسر حسد برد از بیم شاه بر دختر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چه مرگ ز پرّندگان ندارد جنس</p></div>
<div class="m2"><p>برزمگاه بود تیر شاه مرگ بپر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلند مجلس او آسمان دولت گشت</p></div>
<div class="m2"><p>خجسته دولت او اندر آسمان اختر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هنر بسیست و لیکن شمایلش اغلب </p></div>
<div class="m2"><p>عدو بسیست ولیکن فضایلش اکثر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر کسی بنویسد فضایلش جزوی</p></div>
<div class="m2"><p>بساط هفت زمینش نه بس بود دفتر </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببحر گفتند : از جود او ترا اصلست </p></div>
<div class="m2"><p>بکوه گفتند : از حلم او تراست اثر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز فخر جودش بنمود بحر مروارید</p></div>
<div class="m2"><p>ز فخر حلمش بنمود کوه کان گهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان بفایده گیرد همی ز شاه مثال</p></div>
<div class="m2"><p>فلک بمرتبه خواهد همی ز شاه نظر </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه هر که شاعر باشد بمدح او برسد</p></div>
<div class="m2"><p>نه بر نهاد زمانه بهر سری افسر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه هر چه نظم شود مدح شاه را شاید</p></div>
<div class="m2"><p>نه هرچه گونه سیه دارد او بود عنبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برو براه مرادش که دولت آید پیش</p></div>
<div class="m2"><p>بکار تخم مدیحش که گوهر آرد بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چراش عالم خوانی مخوان که عالم را</p></div>
<div class="m2"><p>نیاز و ناز عدیلست و نفع و ضر همبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هوای او همه نازست و هیچ نیست نیاز</p></div>
<div class="m2"><p>رضای او همه نفعست و هیچ نیست ضرر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رسوم او نه رسومست عالم صورست</p></div>
<div class="m2"><p>پدید جان همه خسروان درو بصور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دهان گشاده میان بسته ایستاده فلک</p></div>
<div class="m2"><p>بمدح و خدمت شاه سپه کش صفدر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دهان او را شد مشتری بجای زبان</p></div>
<div class="m2"><p>میان او را شد جوزهر بجای کمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز بهر آنکه از او بد زمانه را زینت</p></div>
<div class="m2"><p>زمانه گفت مرا بگذران و خود مگذر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سخاوت و سخن و طبع ورای او گویی</p></div>
<div class="m2"><p>ز خاک و آب و ز باد آمدند و از آذر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز آذر آید نور و ز باد زاید جان</p></div>
<div class="m2"><p>ز آب خیزد درّ و ز خاک زاید زر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز تیغ او عجب آید مرا که صورت او</p></div>
<div class="m2"><p>نگارهای حریرست و رشته های درر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روان ندارد و اندر شود بتن چو روان</p></div>
<div class="m2"><p>جگر ندارد و اندر شود چو خون بجگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ستاره نی و همه روی او ستاره صفت </p></div>
<div class="m2"><p>فلک نه و همه بالای او فلک چنبر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کمان وری که سر تیر او بهیچ صفت</p></div>
<div class="m2"><p>ز جای خویش نجنبد چو راست کرد نظر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نشانه سازد سوفار تیر پیشین را</p></div>
<div class="m2"><p>برو نشاند پیکان تیرهای دگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p> خبر کنند ز شاهان و ما همی نکنیم</p></div>
<div class="m2"><p>که تیغ شاه نماند همی بگیتی شر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدان زمین که بدو در ز وقت آدم باز</p></div>
<div class="m2"><p>نبود جز همه کفر و نرفت جز کافر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کشید لشکر ایمان و کرد مجلس علم</p></div>
<div class="m2"><p>بساط نور بگسترد شاه حق گستر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>میان موج ضلالت جز او که برد هدی</p></div>
<div class="m2"><p>میان زمرۀ دیوان جز او که خواند زیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سپاس و شکر خداوند را که کرد تهی</p></div>
<div class="m2"><p>جهان بقّوت معروف خسرو از منکر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر بکاوی آتش بود زبانه زنان </p></div>
<div class="m2"><p>زمین آن همه بتخانه تا گه محشر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مثل زنند که از گل هوا نیاید و شاه</p></div>
<div class="m2"><p>بنعل اسب هوا کرد خاک کالنجر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زرام و از درۀ رام اگر حدیث کنی</p></div>
<div class="m2"><p>همی بماند گوش از شنیدنش مضطر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپاه گبر بدو در چو لشکر یأجوج </p></div>
<div class="m2"><p>نهاد آن دره محکم چو سدّ اسکندر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدایگان بگشاد آن بنصرت یزدان</p></div>
<div class="m2"><p>براند دجله ز اوداج گبر کان کبر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بنیزه زو همه دل شد ز پشتها بیرون </p></div>
<div class="m2"><p>ز تیغ مغز همی جوش کرد بر مغفر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بجای دیدنشان در میان دیده سنان</p></div>
<div class="m2"><p>بجای فکرتشان در میان دل خنجر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه مایه گردن گردنکشان شکسته بگرز</p></div>
<div class="m2"><p>چه مایه مغز بداندیش کوفته بتبر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هوا چو معدن نیلوفر از نمایش تیغ</p></div>
<div class="m2"><p>سنان نیزه بدو در چو برگ نیلوفر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز بسکه ریخته گردید خون در آن دره</p></div>
<div class="m2"><p>برنگ روین روید گیاه و برگ شجر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نوشت بر در و بامش هر آنچه گفت خدا</p></div>
<div class="m2"><p>فرو سترد بشمشیر آنچه کرد آزر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خدایگانا جشن خدایگانانست</p></div>
<div class="m2"><p>بخواه باده بفروز خسروی آذر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وگر نباشد باده بدیل آب حیات</p></div>
<div class="m2"><p>ز کف خویش در افکن بکام در ساغر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وگر نباشد آتش سیاست تو بسست</p></div>
<div class="m2"><p>سیاست تو ز آتش بسی فروزانتر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر ازو شرری درفتد بکوه بلند</p></div>
<div class="m2"><p>ازو نماند جز توده های خاکستر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سیاست تو یکی آتش است عالم را</p></div>
<div class="m2"><p>چنانکه باشد در آتش از اثیر اثر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سخات آب حیاتست هر کجا بچکد</p></div>
<div class="m2"><p>بطبع زنده شود گر چه برچکد بحجر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همیشه تا بود از پیش ماه دی آذر</p></div>
<div class="m2"><p>همیشه تا بود از پیش مهر شهریور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ملک تو باش ولایت تو بخش و ملک توگیر</p></div>
<div class="m2"><p>هنر تو ورز و بزرگی تو جوی و نوش تو خور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>براستی تو گرای و بمردمی تو بسیج</p></div>
<div class="m2"><p>بدشمنان تو شتاب و بدوستان تو نگر</p></div></div>