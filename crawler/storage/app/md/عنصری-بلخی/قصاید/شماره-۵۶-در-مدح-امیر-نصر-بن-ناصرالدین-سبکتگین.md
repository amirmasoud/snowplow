---
title: >-
    شمارهٔ ۵۶ -  در مدح امیر نصر بن ناصرالدین سبکتگین
---
# شمارهٔ ۵۶ -  در مدح امیر نصر بن ناصرالدین سبکتگین

<div class="b" id="bn1"><div class="m1"><p>گل نوشکفته است و سرو روان</p></div>
<div class="m2"><p>برآمیخته مهر او با روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد چهر او برنگارد بدل</p></div>
<div class="m2"><p>که دل مهر او باز بندد بجان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بنگری سوی رخسار او</p></div>
<div class="m2"><p>بروید بچشم اندرت ارغوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بمن گر بانگشت اشارت کنی</p></div>
<div class="m2"><p>ز ناخنت بیرون دمد زعفران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به از شکّرش لفظ شکّرشکن</p></div>
<div class="m2"><p>به از عنبرش زلف عنبر فشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نام پیچیده زلفش بری</p></div>
<div class="m2"><p>پر از مشک یابی تو کام و دهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر وصف گوئی ز شیرین لبش</p></div>
<div class="m2"><p>روان گرددت انگبین بر زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر نیست خواهی که هستی شود</p></div>
<div class="m2"><p>ببینش چو بندد کمر بر میان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگارست گوئی میان سپاه</p></div>
<div class="m2"><p>نگاری چو آراسته بوستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه سود از نگار سپاهی ترا</p></div>
<div class="m2"><p>سخن را بمدح سپهبد رسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خداوند علم و خداوند عدل</p></div>
<div class="m2"><p>خداوند ایمان و یمن و امان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملک نصر بن ناصر الدین کزو</p></div>
<div class="m2"><p>قوی گشت فرهنگ و دولت جوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طبایع ز حزمش بود بی خلل</p></div>
<div class="m2"><p>زمانه بعزمش زند داستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدی به ز نیکی در اعدای او</p></div>
<div class="m2"><p>کژی بهتر از راستی در کمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ادب را برسمش کنند اقتراح </p></div>
<div class="m2"><p>خرد را به رایش کنند امتحان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان کآسمانست در همتش</p></div>
<div class="m2"><p>جهان همچنانست در آسمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بزرگیش را در جهان جای نیست</p></div>
<div class="m2"><p>که پر گشت از آثار نیکش جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر عکس تیغش در افتد به پیل</p></div>
<div class="m2"><p>بجوش آیدش مغز در استخون </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ابا ضربت و زور بازوی او</p></div>
<div class="m2"><p>چه ضایع تر از درع و بر گستوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز پیکار او شد همه مرغزار</p></div>
<div class="m2"><p>سراسر همه دشت هندوستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رگ بدسگالان درو جوی خون</p></div>
<div class="m2"><p>پی بت پرستان درو خیزران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدان مرکب فرخش ننگری</p></div>
<div class="m2"><p>که ساکن یقین است و جنبان گمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بادست و زو بر هوا بار نه</p></div>
<div class="m2"><p>چو کوه است و بر خاک بار گران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چرا کوه را باد باشد نقاب</p></div>
<div class="m2"><p>چرا باد را کوه دارد عنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز تیزی تو گوئی مکان گیر نیست</p></div>
<div class="m2"><p>که بر لامکان گیر گیرد مکان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر عرض او نیستی ، نیستی</p></div>
<div class="m2"><p>سخن گفتن عقل را ترجمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وگر صورت او نبودی ز فضل</p></div>
<div class="m2"><p>همه رمز بودی نبودی بیان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کسی رایگان چیز ندهد بکس</p></div>
<div class="m2"><p>همی جود او زر دهد رایگان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بساط زمین شد مسخر ز بس</p></div>
<div class="m2"><p>که راندند زوّار او کاروان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نشاید بد اندر جهان نعمتی</p></div>
<div class="m2"><p>که از داغ جودش ندارد نشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پسندیدنش هست سودی بزرگ</p></div>
<div class="m2"><p>بهر دو جهان ناپسندش زیان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ایا پاکدین شاه دانش گزین</p></div>
<div class="m2"><p>ز دین تو اهل هوا راهوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهان بی تو تاراج اهریمن است</p></div>
<div class="m2"><p>بره گرک درّد چو نبود شبان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بزرگی و شاهی مثل آتش است </p></div>
<div class="m2"><p>از آتش تو نوری و جز تو دخان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی تا فصول طبایع ز سال</p></div>
<div class="m2"><p>تموز و دی است و بهار و خزان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بمان تا زمین است شاه زمین</p></div>
<div class="m2"><p>بزی تا زمانست فخر زمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به نیکی بکوش و بهمت برش</p></div>
<div class="m2"><p>بشادی بباش و برادی بمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همایون و فرخنده بادت عید</p></div>
<div class="m2"><p>عدو مستمند و ولی کامران</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو از قدرت ایزدی بر زمین</p></div>
<div class="m2"><p>همی باش بر قدرتش جاودان</p></div></div>