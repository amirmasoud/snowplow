---
title: >-
    شمارهٔ ۴۶ -  در مدح سلطان محمود
---
# شمارهٔ ۴۶ -  در مدح سلطان محمود

<div class="b" id="bn1"><div class="m1"><p>نوروز بزرگ آمد آرایش عالم</p></div>
<div class="m2"><p>میراث بنزدیک ملوک عجم از جم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دولت شاه ملکان فرّخ و فیروز</p></div>
<div class="m2"><p>آن قبلۀ فخر و شرف گوهر آدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالار خراسان ملک عالم عادل</p></div>
<div class="m2"><p>از جملۀ شاهان بهمه فضل مقدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون بر او جز که بخدمت نکند کار</p></div>
<div class="m2"><p>دولت بر او جز که بطاعت نزند دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنجا که خورد باده ز شادی بچکد زر</p></div>
<div class="m2"><p>و آنجا که زند نیزه ز آهن بدمد دم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون تیر گشاده کند از چرخ بهیجا</p></div>
<div class="m2"><p>از هیبت او چرخ گشاده شود از هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر لشکر شادی شود آفاق دمادم</p></div>
<div class="m2"><p>هرگه که دمادم کشد او رطل دمادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنجا که بود جودش هرگز نبود فقر</p></div>
<div class="m2"><p>و آنحا که بود نامش هرگز نبود غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر زهر خورد چاکر او گردد چون نوش</p></div>
<div class="m2"><p>ور نوش خورد حاسد او گردد چون سم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بزم ببخشش بکشد آتش ادبار</p></div>
<div class="m2"><p>در رزم به نیزه بکند دیدۀ ضیغم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حاتم و رستم نکنم یاد که او را</p></div>
<div class="m2"><p>انگشت کهین است به از حاتم و رستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرهنگ و کمال و خرد و ردای و مردی</p></div>
<div class="m2"><p>هر پنج بطبع و کف او گشت مسلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر جا که بود شیمت او مشک فراخست</p></div>
<div class="m2"><p>گوئی برد از شیمت او مشک همی شم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بحریست دلش جز همه حکمت نزند موج</p></div>
<div class="m2"><p>ابریست کفش جز همه گوهر ندهد نم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از گرد سپاهش همه ادهم شود اشقر</p></div>
<div class="m2"><p>وز ضربت تیغش همه اشقر شود ادهم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کعبه است سرایش ز بزرگی ملکانرا</p></div>
<div class="m2"><p>کلکش حجرالاسود و کف چشمۀ زمزم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کس پیش نرفت از همه گیتی بنبردش</p></div>
<div class="m2"><p>کآنروز بر او اهلش ننشست بماتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از رونق رایش خرد آراسته گردد</p></div>
<div class="m2"><p>کش رای نگین است و خرد حلقۀ خاتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر چند بگیتی خرد و اصل کریم است</p></div>
<div class="m2"><p>اندر حرم میر کریم است و مکرّم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قسّام بدو داد همه قسمت نیکی</p></div>
<div class="m2"><p>گوئی که بدو بود عنایتش مقسّم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا هیبت و جودش ندهد مایه به هر دو</p></div>
<div class="m2"><p>نه تیز بود آتش و نه موج زند یم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بربستۀ رنج از دل او یابد راحت</p></div>
<div class="m2"><p>بر خستۀ آز از کف او بارد مرهم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او را بپرستند ، چه آزاد و چه بنده</p></div>
<div class="m2"><p>او را بستایند ، چه گویا و چه ابکم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در نیک و بد غور سخن فکرت دانا</p></div>
<div class="m2"><p>بیش است ز هر چیزی و ز مدحت او کم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چونانکه سر نیزه اش بیرون رود از سنگ</p></div>
<div class="m2"><p>بیرون نشود سوزن فولاد ز بیرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا چرخ همیگردد و پاینده بود خاک</p></div>
<div class="m2"><p>تا پیشرو سال بود ماه محرّم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر صدر بزرگیش بقا باد بشادی</p></div>
<div class="m2"><p>بنیاد هنر مانده باحکامش محکم</p></div></div>