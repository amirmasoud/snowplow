---
title: >-
    شمارهٔ ۴۸ -  در مدح سلطان محمود غزنوی
---
# شمارهٔ ۴۸ -  در مدح سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>آن زلف سرافکنده بر آن عارض خرّم</p></div>
<div class="m2"><p>از بهر چه چیزست بدان بوی و بدان خم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند همی مالد خمّش نشود راست</p></div>
<div class="m2"><p>هر چند همی شوید بویش نشود کم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انگیخته از هم همه و آمیخته با هم</p></div>
<div class="m2"><p>آویخته اندر هم و توده شده بر هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکس که یمین است و امین دولت و دین را</p></div>
<div class="m2"><p>زیبا ملک غازی شاهنشه عالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا درگه او یابی مگذر بدر کس</p></div>
<div class="m2"><p>زیرا که حرامست تیمم بلب یم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش از ملکان فضلش و عصرش پس از ایشان</p></div>
<div class="m2"><p>از عصر مؤخر شد و از فضل مقدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمن که سخن گوید از آن تیغ جهانسوز</p></div>
<div class="m2"><p>گردد بزمان اندر هر دو لبش اعلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از رسم جوانمردی و ز فخر مدیحش</p></div>
<div class="m2"><p>گوینده و بیننده شود اکمه و ابکم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای مایۀ هر نیکی و اندازۀ شادی</p></div>
<div class="m2"><p>نیکی بتو نیکو شد و شادی بتو خرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دانش و رسمت خرد اندازه گرفتست</p></div>
<div class="m2"><p>زین روی خردمند عزیزست و مکرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردنده فلک خدمت او پیشه گرفته است</p></div>
<div class="m2"><p>از عیب و فساد از پی آنست مسلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جای تو بغزنی در و جاه تو ببغداد</p></div>
<div class="m2"><p>جیش تو ببلخ اندر و جوش تو بدیلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پاکیزه تر از نوری و سوزنده تر از نار</p></div>
<div class="m2"><p>بخشنده تر از ابری و بایسته تر از نم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از بوی خوش مدح تو هر کس که بخواند</p></div>
<div class="m2"><p>چون نافه شود راوی و مدّاح ترا نم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در سایۀ خلق تو بود عنبر اشهب</p></div>
<div class="m2"><p>از خلق تو گشتست بدان بوی و بدان شم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از طاعت تو سر نکشد هر که کشد سر</p></div>
<div class="m2"><p>بی خدمت تو دم نزند هر که زند دم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنی تو که با قیمت و آراسته گشتست</p></div>
<div class="m2"><p>چون عقدۀ یاقوت بتو گوهر آدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عدل از تو مشهر شد و فضل از تو منوّر</p></div>
<div class="m2"><p>ملک از تو مهنا شد و دین از تو مقدم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا تیغ جهاندار تو برخاست بکوشش</p></div>
<div class="m2"><p>دل سوزۀ بدخواه تو بنشست بماتم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در امن تو ضیغم نکشد دست بر آهو</p></div>
<div class="m2"><p>با امر تو آهو بکند ناخن ضیغم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آهن و سیم است قضا و قدر ایرا</p></div>
<div class="m2"><p>کز آهن و سیمست ترا خنجر و خاتم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عزست نگین تو و خیرست حسامت</p></div>
<div class="m2"><p>گر عزّ منقش بود و خیر مجسم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای بس ملک نامورا کش تن و نعمت</p></div>
<div class="m2"><p>بخشیده شد از تیغ تو در معرک و منقم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آهن همه تیغست ولیکن نه چنان تیغ</p></div>
<div class="m2"><p>دریا همه آبست ولیکن نه چنان یم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گویند که فرمان بر جم بود جهان پاک</p></div>
<div class="m2"><p>دیو و پری و دام و دد و خلق رمارم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر بوده چنین ، یا جم را جاه تو بودست</p></div>
<div class="m2"><p>یا نام تو بوده ست بر انگشتری جم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا روز بدیدار بود خوبتر از شب</p></div>
<div class="m2"><p>تا زیر بآواز بود تیز تر از بم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا حکم سر سال عجم باشد نوروز</p></div>
<div class="m2"><p>چون حکم سر سال عرب ماه محرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جاوید جهاندار و خداوند جهان باش</p></div>
<div class="m2"><p>تو شاد بکام دل و اعدات مغمم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وین عید همایون بتو بر فرخ و میمون</p></div>
<div class="m2"><p>تو منعم و آنکس که تو خواهی بتو منعم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رطل تو دمادم شده و فتح دمادم</p></div>
<div class="m2"><p>بر فتح دمادم رو و بر رطل دمادم</p></div></div>