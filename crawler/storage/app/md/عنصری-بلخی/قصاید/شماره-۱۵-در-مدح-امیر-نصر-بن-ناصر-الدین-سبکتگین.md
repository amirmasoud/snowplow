---
title: >-
    شمارهٔ ۱۵ -  در مدح امیر نصر بن ناصر الدین سبکتگین
---
# شمارهٔ ۱۵ -  در مدح امیر نصر بن ناصر الدین سبکتگین

<div class="b" id="bn1"><div class="m1"><p>غنودستند بر ماه منور</p></div>
<div class="m2"><p>خط و زلفین آن بت روی دلبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی را سنبل نو رسته بالین</p></div>
<div class="m2"><p>یکی را لالۀ خود روی بستر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مشکین جعد زنجیرست گویی</p></div>
<div class="m2"><p>ز عنبر حلقۀ زلفین چنبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی را نقرۀ بی بار نافه است</p></div>
<div class="m2"><p>یکی را آینۀ بی زنگ مجمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ و چشمش ز دو وقت مخالف</p></div>
<div class="m2"><p>دو چیز آرند هر دو مست بنگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی از ماه آذار آب لاله</p></div>
<div class="m2"><p>یکی از ماه آذر چشم عبهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نیکو چهره و قدّش ببیند</p></div>
<div class="m2"><p>شود از نعت هر دو عقل مضطر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی را لعبت کشمیر خواند</p></div>
<div class="m2"><p>یکی را برکشیده سرو کشمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر وی و موی او بنگر که بینی</p></div>
<div class="m2"><p>بی آذر هر دوانرا فعل آذر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی بی دود ، سال و ماه تیره</p></div>
<div class="m2"><p>یکی بی نور ، روز و شب منور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدندان و لبش بنگر بعبرت</p></div>
<div class="m2"><p>دو معنی هر یکی را بود همبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی لؤلوی عمانی و پروین</p></div>
<div class="m2"><p>یکی یاقوت رمّانی و شکر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا بهره دو چیز آمد ز گیتی</p></div>
<div class="m2"><p>دل پاک و زبان مدح گستر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی بر مهر جانان وقف کردم</p></div>
<div class="m2"><p>یکی بر آفرین شاه کشور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپهسالار مشرق کز جمالش</p></div>
<div class="m2"><p>دو پیکر کرد عقل اندر دو پیکر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی از فرّ یزدانی مهیّا</p></div>
<div class="m2"><p>یکی از عقل نورانی مصور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نظام آنگه پذیرد ملک و دولت</p></div>
<div class="m2"><p>که نصرت با ظفر باشد برابر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی از نصر خیزد ، نام خسرو</p></div>
<div class="m2"><p>یکی از کنیت او بوالمظفر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مبارک دست او دو گونه ابرست</p></div>
<div class="m2"><p>کشندۀ دشمنست و دوست پرور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی با تیغ و بارانش همه خون</p></div>
<div class="m2"><p>یکی با بذل و بارانش همه زر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بروز رزم او بسیار بینی</p></div>
<div class="m2"><p>گو لشکر شکار و گرد صفدر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی را زخم تیرش کرده بیجان</p></div>
<div class="m2"><p>یکی را ضرب تیغش کرده بیسر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر خواهندۀ رزمش به میدان</p></div>
<div class="m2"><p>بود اسفندیار و رستم زر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی را مغز خارد نیش افعی</p></div>
<div class="m2"><p>یکی را دیده درآید غضنفر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بأس و همتش دو صورت آمد</p></div>
<div class="m2"><p>مرکب گشته هر دو یک بدیگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی را آتش رخشنده بنده</p></div>
<div class="m2"><p>یکی را گنبد گردنده چاکر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر فرمان دهدشان رای خسرو</p></div>
<div class="m2"><p>بفال نیک او بی رنج لشکر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی از خلخ آرد خرگه خان</p></div>
<div class="m2"><p>یکی از روم شادروان قیصر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وگر لشکر بودشان وقت جنبش</p></div>
<div class="m2"><p>مناقبهای شاه فرخ اختر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی را خلد منزلگاه باید</p></div>
<div class="m2"><p>یکی را عالم علوی معسکر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وگر شاه جهان از خامصۀ خویش</p></div>
<div class="m2"><p>دهدشان خلعت زیبا و درخور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی را باید از تقدیر مرکب</p></div>
<div class="m2"><p>یکی را باید از توفیق افسر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز کلک شاه وصفی کرد خواهم</p></div>
<div class="m2"><p>دو شاخش را بدو معنی مفسّر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی مر جهل را ضرّی است بی نفع</p></div>
<div class="m2"><p>یکی مر علم را نفعی است بی ضر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دو برهان بینی اندر جنبش او</p></div>
<div class="m2"><p>بهر دو باز بسته اصل و گوهر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی داند ز رمز فضل معنی</p></div>
<div class="m2"><p>یکی دارد ز راز غیب چادر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بجنبد تا همی پیرایه بخشد</p></div>
<div class="m2"><p>بجنبش لفظ معنی را ز گوهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی چون عقد مروارید خوشاب</p></div>
<div class="m2"><p>یکی چون رشتۀ یاقوت احمر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی نقش و ادب را سخره دارد</p></div>
<div class="m2"><p>دو شاخ او بدست خسرو اندر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یکی چون خامه اندر دست مانی</p></div>
<div class="m2"><p>یکی چون رنده اندر دست آزر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همیشه خدمتش دو کار دارد</p></div>
<div class="m2"><p>نبندد ساعتی آن هر دو را در</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی معروف گرداند بمعروف</p></div>
<div class="m2"><p>یکی منکر کند دل را ز منکر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر مر جاه و جودش را خداوند</p></div>
<div class="m2"><p>بدادی صورتی مخصوص و منظر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یکی اندر فلک خورشیدی بودی</p></div>
<div class="m2"><p>یکی اندر زمین دریای اخضر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کرام الکاتبینش گر ببیند</p></div>
<div class="m2"><p>که بنویسد بروز داد داور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی گوید که مهدی گشت پیدا </p></div>
<div class="m2"><p>یکی گوید نبی الله اکبر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یکی منجوق و شادروان ملکش</p></div>
<div class="m2"><p>بجای دولت او هر دو معبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یکی پیوسته از ماهیست تا ماه</p></div>
<div class="m2"><p>یکی گسترده از چین تا بخاور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بروز جنگ تیغ او و گرزش</p></div>
<div class="m2"><p>بزور بازوی شاه دلاور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی جیحون خون راند بصحرا</p></div>
<div class="m2"><p>یکی هامون کند سد سکندر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بهیجا پیشه آموزد ز دستش</p></div>
<div class="m2"><p>سنان نیزۀ خطی و خنجر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی دل بیند اندر درع و خفتان</p></div>
<div class="m2"><p>یکی مو بیند اندر ترک و مغفر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو برمالد برزم اندر کمان را</p></div>
<div class="m2"><p>اجل بینی نهان درباد صرصر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی گشته کمانش را زه و توز</p></div>
<div class="m2"><p>یکی مر تیر او را تولی و پر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سیاست راندن و فرّش بمجلس</p></div>
<div class="m2"><p>دو فرع آمد ز یک اصل مطهر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یکی مر عدل را سایۀ خدایی</p></div>
<div class="m2"><p>یکی مر فضل را مهر پیمبر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز عالی همت و جسم همایون </p></div>
<div class="m2"><p>دو عالم را دو سالارست و سرور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی سالار ارواحست آنجا</p></div>
<div class="m2"><p>یکی سالار اجسام است ایدر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر علم و شجاعت را بجویی</p></div>
<div class="m2"><p>بنزد او بیابیشان مجاور </p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یکی را عالم علوی متابع</p></div>
<div class="m2"><p>یکی را عالم سفلی مسخر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اگر تنصیف گیرد آفرینش </p></div>
<div class="m2"><p>شود گیتیش دو گونه مسخر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یکی موجود گردانندۀ خیر</p></div>
<div class="m2"><p>یکی معدوم گردانندۀ شر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همی تا باغ و راغ و رود و کشته</p></div>
<div class="m2"><p>چو آید ماه فروردین بآخر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی را ابر بخشد کله سبز</p></div>
<div class="m2"><p>یکی را باد دیبای مطیّر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز آب روی چشم شاه جز وی</p></div>
<div class="m2"><p>معنبر گشت و مایه نامعنبر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شود آبستن از گل شاخ و گردد</p></div>
<div class="m2"><p>زمین چون کودکی با زیب و با فر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی را لؤلؤ ناسفته فرزند</p></div>
<div class="m2"><p>یکی را ابر لؤلؤ بار مادر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بملک اندر همی بادند باقی</p></div>
<div class="m2"><p>بکام دوستان آن دو برادر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یکی شاه جهان چونانکه خود هست</p></div>
<div class="m2"><p>یکی سالار و از شادی توانگر</p></div></div>