---
title: >-
    شمارهٔ ۴۰ -  در مدح امیر نصر بن ناصرالدین سبکتگین
---
# شمارهٔ ۴۰ -  در مدح امیر نصر بن ناصرالدین سبکتگین

<div class="b" id="bn1"><div class="m1"><p>گر از عشقش دلم باشد همیشه زیر بار اندر</p></div>
<div class="m2"><p>چرا گم شد رخش باری بزلف مشکبار اندر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر طعنه زند قدّش بسر و جویبار اندر</p></div>
<div class="m2"><p>چرا رخنه کند غمزه ش بتیغ ذوالفقار اندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکسته زلف مشک افشان بگرد روی یار اندر</p></div>
<div class="m2"><p>بشیطانی نیت ماند بیزدانی نگار اندر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جفا گوئی گرفتستی وفا را در کنار اندر</p></div>
<div class="m2"><p>تو پنداری گل سوری شکفتستی بقار اندر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل از رویش برد گونه بهنگام بهار اندر</p></div>
<div class="m2"><p>مغ از چهرش برد صورت بفغفوری نگار اندر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>........................................</p></div>
<div class="m2"><p>ز خوبی او بنور اندر ز عشقش من بنار اندر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان کو جادویی دارد بچشم پرخمار اندر</p></div>
<div class="m2"><p>دل من جادویی دارد بمدح شهریار اندر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپهبد نصر با نصرت بکار کارزار اندر</p></div>
<div class="m2"><p>ز عزم و حزم با قوت بجبر و اختیار اندر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان یاقوت پیوسته بدرّ شاهوار اندر</p></div>
<div class="m2"><p>بیابد مخلص شعری به شعری بر شعار اندر (؟)</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس خون گردد از نامش بکام کامگار اندر</p></div>
<div class="m2"><p>ز نام او شکست آید بنام نامدار اندر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهارستش کف و نعمت بدان فاضل بهار اندر</p></div>
<div class="m2"><p>بحارستش دل و حکمت بدان ز اخر بحار اندر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هنر گسترد جاهش را بقدر و اقتدار اندر</p></div>
<div class="m2"><p>خرد پرورد عرضش را بجاه و افتخار اندر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بهر زایران باشد همی در انتظار اندر</p></div>
<div class="m2"><p>گرفته نقش مهر او بچشم روزگار اندر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقار آرد وقار او بطبع بیوقار اندر</p></div>
<div class="m2"><p>قرار آرد قرار او به رای بیقرار اندر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ردای دولتش را حق میان پود و تار اندر</p></div>
<div class="m2"><p>پراکنده است فضل او ببلدان و دیار اندر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعدلش زهر شد بسته بنیش گرزه مار اندر</p></div>
<div class="m2"><p>بفضلش خوشۀ خرما پدید آید به خار اندر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهیجا چون برون آید چو خورشید از غبار اندر</p></div>
<div class="m2"><p>نشاند تیر را چون مژه در چشم سوار اندر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود مختار و قادر زو بجبر و اضطرار اندر</p></div>
<div class="m2"><p>بجنگ اندر تو پنداری که هست او در شکار اندر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نورزد جز جوانمردی بعمر مستعار اندر</p></div>
<div class="m2"><p>همه فعلش هنر گردد بدهر پر عوار اندر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمار او کنار آرد بگنح بی کنار اندر</p></div>
<div class="m2"><p>نگنحد جز وی از فضلش بقانون شمار اندر </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عبارت کردن فضلش بصدر اعتبار اندر</p></div>
<div class="m2"><p>عنان عفو او دایم بدست اعتذار اندر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخندان از یمین او بیمن کردگار اندر</p></div>
<div class="m2"><p>سخنگو از یسار او بتوقیر و یسار اندر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نباشد زو عدو ایمن بپولادی حصار اندر</p></div>
<div class="m2"><p>گذر باشد سپاهش را ببحر بیگذار اندر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی تا روشنی باشد برخشنده نهار اندر</p></div>
<div class="m2"><p>چو تاریکی بار کان شب دیجور و تار اندر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بقا بادش بمجلس گاه شادی و عقار اندر</p></div>
<div class="m2"><p>ز شرّ خویش بد خواهش بسوزنده شرار اندر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>........................................</p></div>
<div class="m2"><p>مبارک اورمزد او ببخت غمگسار اندر</p></div></div>