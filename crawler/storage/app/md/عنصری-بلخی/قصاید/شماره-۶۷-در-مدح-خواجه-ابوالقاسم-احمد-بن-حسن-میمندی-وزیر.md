---
title: >-
    شمارهٔ ۶۷ -  در مدح خواجه ابوالقاسم احمد بن حسن میمندی وزیر
---
# شمارهٔ ۶۷ -  در مدح خواجه ابوالقاسم احمد بن حسن میمندی وزیر

<div class="b" id="bn1"><div class="m1"><p>ای شکسته زلف یار از بسکه تو دستان کنی</p></div>
<div class="m2"><p>دست دست تست اگر با ساحران پیمان کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه بر ماه دو هفته گرد مشک آری پدید</p></div>
<div class="m2"><p>گاه مر خورشید را در غالیه پنهان کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه بی جوش از بر گلبرگ بر جوشی همی</p></div>
<div class="m2"><p>گاه بی مشک از بر کافور مشک افشان کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سامری از ساحری بر زرّ گوساله نکرد</p></div>
<div class="m2"><p>نیم از آن هرگز که تو با عارض جانان کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم زره پوشی و هم چوگان زنی بر ارغوان</p></div>
<div class="m2"><p>خویشتن را گه زره سازی و گه چوگان کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشکنی بر خویشتن تا نرخ عنبر بشکنی</p></div>
<div class="m2"><p>خویشتن لرزان کنی تا نرخ مشک ارزان کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستی دیوانه بر آتش چرا غلتی همی</p></div>
<div class="m2"><p>نیستی پروانه گرد شمع چون جولان کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بخواهی گشت گردشگاه تو دیبا بود</p></div>
<div class="m2"><p>چون بخواهی خفت بستر لالۀ نعمان کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل نگهدار ای تن از دردش که دل باید ترا</p></div>
<div class="m2"><p>تا ثنای کدخدای خسرو ایران کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواجه بوالقاسم عمید سید آن کز نعت او</p></div>
<div class="m2"><p>شعرهای عنصری پر لؤلؤ و مرجان کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عادلی کز بس بزرگی و تمامی عدل او</p></div>
<div class="m2"><p>عار دارد گر حدیث عدل نوشروان کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اصل فرمان دادن اندر طاعت و فرمان اوست</p></div>
<div class="m2"><p>بر جهان فرمان دهی گر خواجه را فرمان کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای خداوندی که گر بی کام تو گردد فلک</p></div>
<div class="m2"><p>آرزوی خویش را تو بر فلک تاوان کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرد ره یابد بشعر از نعمت و احسان تو</p></div>
<div class="m2"><p>تو ز بس احسان کنی مدّاح را حسّان کنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وعده را نیسان نباشد جایز اندر طبع تو</p></div>
<div class="m2"><p>ور وعیدی کرد باید ساعتی نسیان کنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از نجوم آسمان چاکر فزون بینم ترا</p></div>
<div class="m2"><p>گاه آن آمد که تو بر آسمان دیوان کنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر چو ابراهیم در آذر بود مداح تو</p></div>
<div class="m2"><p>چون دعای مستجاب آتش برو ریحان کنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور بدریا بر گذاری تو سموم قهر خویش</p></div>
<div class="m2"><p>ماهیانرا زیر آب اندر همه بریان کنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از دو برهان دو پیغمبر ترا بینم نصیب</p></div>
<div class="m2"><p>وین دو بینم شغل تو گر این کنی ور آن کنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از عطا تو معجزات عیسی مریم کنی</p></div>
<div class="m2"><p>از قلم تو معجزات موسی عمران کنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر صدف باری غریب آورده ای زیرا که او</p></div>
<div class="m2"><p>گوهر از باران کند تو گوهر از قطران کنی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از خردمندان که بر درگاه تو گرد آمدند</p></div>
<div class="m2"><p>تربت حضرت همی چون تربت یونان کنی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون خرد بر هرچه روحانی همی واقف شوی</p></div>
<div class="m2"><p>چون فلک بر هرچه جسمانی همی دوران کنی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر بخواهی از درستی وز عین اعتقاد</p></div>
<div class="m2"><p>کفر گیتی را بایمانی همه ایمان کنی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهد خلق از بهر خشنودی تست اندر جهان</p></div>
<div class="m2"><p>تو همی جهد از پی خشنودی یزدان کنی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از درازی دست و فرمان رونده مر ترا</p></div>
<div class="m2"><p>دست بر کیوان رسد گر دست بر کیوان کنی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا بدید ایوان تو کیوان همی جوید شرف</p></div>
<div class="m2"><p>ز آرزوی اینکه ا و را شرفۀ ایوان کنی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز آرزوی آنکه بوسد پای تو حور بهشت</p></div>
<div class="m2"><p>خواهدی کز روی او تو نقش شادروان کنی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گرچه سندانرا کنی چون موم روز عزم خویش</p></div>
<div class="m2"><p>موم را در زیر حزم خویش چون سندان کنی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این جهان چون نامه بنوردد همی در دست تو</p></div>
<div class="m2"><p>تا مگر بر نامه نام خویش را عنوان کنی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر نه خورشیدی جرا خیره شود دیده زتو</p></div>
<div class="m2"><p>ور نه جانی پس چرا اوصاف را حیران کنی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیستی خورشید و داری فعل خورشید از کرم</p></div>
<div class="m2"><p>نیستی جان و همی از لفظ کار جان کنی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گنج پردازی همی تا رنج برداری ز خلق</p></div>
<div class="m2"><p>رنج برداری همی تا عالم آبادان کنی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن سرکشی تو که از رخها بشویی زنگ غم</p></div>
<div class="m2"><p>وان پزشکی تو که درد آز را درمان کنی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا جهان باقی بود بادت بقا تا علم را</p></div>
<div class="m2"><p>پایه بفزائی و کار ملکرا سامان کنی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اورمزد و عید فرخ باد تا بر بدسگال</p></div>
<div class="m2"><p>روز او نیران کنی و دلش را بریان کنی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گوسفند و گاو و اشتر مردمان قربان کنند</p></div>
<div class="m2"><p>باز تو آز و نیاز و جهل را قربان کنی</p></div></div>