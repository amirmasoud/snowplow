---
title: >-
    شمارهٔ ۲۲ -  ایضاً در مدح سلطان فرماید
---
# شمارهٔ ۲۲ -  ایضاً در مدح سلطان فرماید

<div class="b" id="bn1"><div class="m1"><p>چنین نماید شمشیر خسروان آثار</p></div>
<div class="m2"><p>چنین کنند بزرگان چو کرد باید کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتیغ شاه نگر نامۀ گذشته مخوان</p></div>
<div class="m2"><p>که راستگوی تر از نامه تیغ او بسیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مرد بر هنر خویش ایمنی دارد</p></div>
<div class="m2"><p>شود پذیرۀ دشمن بجستن پیکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه رهنمای بکار آیدش نه اختر گر</p></div>
<div class="m2"><p>نه فال گوی بکار آیدش نه خواب گزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رود چنانکه خداوند شرق رفت برزم</p></div>
<div class="m2"><p>زمانه گشته مر او را دلیل و ایزد یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپیش آن سپه کوه صفت و سیل صفت</p></div>
<div class="m2"><p>سپهر تاختن و مار زخم و مور شمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبارزانش بنیروی پیل و زهرۀ شیر</p></div>
<div class="m2"><p>به پای آهو و کبر پلنگ و قد چنار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه سپر تن و شمشیر دست و تیر انگشت</p></div>
<div class="m2"><p>همه سپه شکن و دیو بند و شیر شکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوقت آنکه زمین تفته بد ز باد سموم</p></div>
<div class="m2"><p>هوا چو آتش و گرد اندرو بجای شرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تف بروز بجوش آید در جیحون</p></div>
<div class="m2"><p>بشب ز پشه در و بد توان گرفت قرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدولت ملک مشرق و سعادت او</p></div>
<div class="m2"><p>نه پشه بود و نه گرما ، نه زین دو هیچ آثار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرو گذشت بآموی شهریار جهان</p></div>
<div class="m2"><p>بفال اختر نیک و بنصرت دادار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فروغ دولت او همچو روز وقت زوال</p></div>
<div class="m2"><p>مصاف لشکر او همچو کوه وقت بهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه زمین شده از بندگان او کشمیر</p></div>
<div class="m2"><p>همه هوا شده از عکس جامه شان فرخار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمین آمو شد در زمان فراز و نشیب</p></div>
<div class="m2"><p>ز توده توده سر و کوه کوه زین افزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پرند چهرۀ الماس رنگ شمشیرش</p></div>
<div class="m2"><p>در آن دیار نماند از مخالفان دیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهنگ مرد اوبارش بخورد در جیحون</p></div>
<div class="m2"><p>هر آنکسی که برست از نهنگ جان او بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر آب در همه غرقه شدند چون فرعون</p></div>
<div class="m2"><p>چو بر گذشت از آن آب شاه موسی وار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فراخ جیحون چون کوه شد ز بسکه درو</p></div>
<div class="m2"><p>کلاه و ترکش وزین و دراعه بود انبار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ازین سپس بدل بانگ و نعره از جیحون</p></div>
<div class="m2"><p>نخواهد آمد جز های های و نالۀ زار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عقیق زار شدست آن زمین ز بسکه ز خون</p></div>
<div class="m2"><p>بروی دشت و بیابان فرو شدست آغار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همی شدند ببیچارگی هزیمیتان</p></div>
<div class="m2"><p>شکسته پشت و گرفته گریغ را هنجار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسی که زنده بماندست از آن هزیمتیان</p></div>
<div class="m2"><p>اگر چه تنش درستست هست جان بیمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بمغزش اندر تیغست اگر بود خفته</p></div>
<div class="m2"><p>بچشمش اندر تیرست اگر بود بیدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر بجنبد بند قبای او از باد</p></div>
<div class="m2"><p>گمان برد که همی خورد بر جگر مسمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر نماز کند آه باشدش تکبیر</p></div>
<div class="m2"><p>وگر گنه کند آوخ بودش استغفار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر سؤال کند ، گوید : ای سوار ! مزن</p></div>
<div class="m2"><p>وگر جواب دهد ، گوید : ای ملک ! زنهار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ور از اسیران گویی گرفت چندانی</p></div>
<div class="m2"><p>که تنگ بود ز انبوهشان بلاد و قفار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گروه ایشان بگرفت طول و عرض جهان</p></div>
<div class="m2"><p>بهر رهی و بهر برزنی قطار قطار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگر زخواسته کوبر گرفت از گر گنج</p></div>
<div class="m2"><p>سخن نمایم عاجز شود درو گفتار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدرجها گهرست و بتختها دیبا</p></div>
<div class="m2"><p>بگنجها در مست و بتنگها دینار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قیاس گیر نداند قیاس سیم سپید </p></div>
<div class="m2"><p>شمار گیر نداند شمار زرّ عیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز عکس جامۀ رنگین هوا چو باغ ارم</p></div>
<div class="m2"><p>زمین ز تودۀ یاقوت سرخ چون گلنار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز توده نافۀ مشک و شمامۀ کافور</p></div>
<div class="m2"><p>شده نسیم صبا همچو طلبۀ عطار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عمود زرین با گوهر کمر شمشیر</p></div>
<div class="m2"><p>سلاح نغز و پریچهرگان گلرخسار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بکشت دشمن و برداشت گنج و مال ببرد</p></div>
<div class="m2"><p>ز بهر نصرت دین محمد مختار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آنکه تربت گرگانج و شهر و برزن او</p></div>
<div class="m2"><p>مقام قرمطیان بود و معدن کفار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همیشه تا صفت تیرگی نصیب شبست</p></div>
<div class="m2"><p>چنان کجا صفت روشنی نصیب نهار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نصیب شاه جهان غز و باد و نصرت و فتح</p></div>
<div class="m2"><p>نصیب دشمن او مرگ و محنت و تیمار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هزار فتح چنین و هزار غزو چنین </p></div>
<div class="m2"><p>برو برآمده و گفته عنصری اشعار</p></div></div>