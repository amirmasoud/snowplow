---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>همیشه سر زلف آن سیمتن</p></div>
<div class="m2"><p>گره بر گره یا شکن بر شکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپیچد همی چون من از عشق او</p></div>
<div class="m2"><p>گره بر بنفشه شکن بر سمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشب ماند آن زلف و هرگز که دید</p></div>
<div class="m2"><p>شب قیر گون روز را پیر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چمیده یکی سرو شد در سرای</p></div>
<div class="m2"><p>مر او را دل مهربانان چمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم فتنه بر غمزگانش بلی </p></div>
<div class="m2"><p>شود فتنه مردم چو بیند فتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خردی دهانش اندرو مانده ام</p></div>
<div class="m2"><p>که دارد دهن یا ندارد دهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد از مهر من چیره بر من چنانک</p></div>
<div class="m2"><p>من از عشق او مانده اندر حزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر یاد کردم من اندر غمش</p></div>
<div class="m2"><p>ببندد مرا غمزگانش به فن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولیکن نبندد که دارد نگاه</p></div>
<div class="m2"><p>مرا از فنونش خداوند من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک نصر بن ناصر الدین کزو</p></div>
<div class="m2"><p>گرفتند شاهان به نیکی سنن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به روشن دلش تازه باشد خرد</p></div>
<div class="m2"><p>به پاکیزه جان زنده باشد بدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو دستش ببین تا ببینی عیان</p></div>
<div class="m2"><p>دو بحر گهر موج شمشیر زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرستیدنش رکن آزادگیست</p></div>
<div class="m2"><p>دل راد مردان بدو مرتهن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قرین زوال است دشمن از آن </p></div>
<div class="m2"><p>که شد با ازل همتش مقترن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه جز راستی امر او را رهیست</p></div>
<div class="m2"><p>نه جز نیستی زخم او را مجن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر چشم گردد دل بدسکال</p></div>
<div class="m2"><p>بدو در شود تیغ او چون وسن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جدائی ز فرمانش بخت بد است</p></div>
<div class="m2"><p>مکن امتحان گر نخواهی محن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وفایش همه طاعت ایزدی است</p></div>
<div class="m2"><p>خلافش همه طاعت اهرمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز هندی نهنگش به هندوستان</p></div>
<div class="m2"><p>نه چنگی برسته ست نه بر همن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرا نزد او معدن خدمت است</p></div>
<div class="m2"><p>نه صنعا به کار آیدش نی عدن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که دینار او هر زمانی کفش</p></div>
<div class="m2"><p>درفشان کند چون سهیل یمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز زوّار بر گنج او هر شبی</p></div>
<div class="m2"><p>برد قیروان تاختن تا ختن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه پاک شد سیستان از بدی</p></div>
<div class="m2"><p>بدین نیکخو شاه پاکیزه تن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درفشی است از حق بدست بقا</p></div>
<div class="m2"><p>خرد داشته بر در داشتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به درج اربری بی ثمن گوهرش</p></div>
<div class="m2"><p>به درج آوری گوهر با ثمن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امینی است نزد خدای جهان</p></div>
<div class="m2"><p>هر آن کس که نزدیک او مؤتمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیابی تو اندر جهان گردنی</p></div>
<div class="m2"><p>که نز جود او زیر بار منن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر باز مانی ز کانی کفش</p></div>
<div class="m2"><p>به جود اندر آید شکست و وهن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>معالی بکردار او شد علی</p></div>
<div class="m2"><p>محاسن به کردار او شد حسن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گه دست شستنش نشگفت اگر</p></div>
<div class="m2"><p>شود چشمۀ زندگانی لگن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>الا تا به هنگام گل زندواف</p></div>
<div class="m2"><p>بنالد ز شاخ گل و یاسمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درختی که کردش برهنه خزان</p></div>
<div class="m2"><p>بهارش کند پر جواهر فنن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر شاه سبز و دل شاه شاد</p></div>
<div class="m2"><p>ولی شادکام و عدو در شجن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر او اورمزد اوّل مهر ماه</p></div>
<div class="m2"><p>همایون به شادی و لهو و ......ن؟</p></div></div>