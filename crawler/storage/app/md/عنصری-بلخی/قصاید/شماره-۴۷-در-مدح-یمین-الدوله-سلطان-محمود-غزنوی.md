---
title: >-
    شمارهٔ ۴۷ -  در مدح یمین الدوله سلطان محمود غزنوی
---
# شمارهٔ ۴۷ -  در مدح یمین الدوله سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>امید نیکی و تاج ملوک و صدر کرام</p></div>
<div class="m2"><p>بزرگ خسرو آزادگان و فخر انام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمین دولت و دولت بدو همیشه عزیز</p></div>
<div class="m2"><p>امین ملت و ملت بدو گرفته نظام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر کلی و جزوی بدو نموده هنر</p></div>
<div class="m2"><p>جهان علوی و سفلی بدو گرفته قوام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نبودی از بهر ملک او نبدی</p></div>
<div class="m2"><p>نه چرخ را حرکات و نه خاکرا آرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پای مرکب توقیر برگرفت شکال</p></div>
<div class="m2"><p>بملک تو سن بی بند بر نهاد لگام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز لفظ مدحت او طعم نوش گیرد نظم</p></div>
<div class="m2"><p>ز ذکر دشمن او طعم زهر گیرد کام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجاه بی اثر او کسی نیابد راه</p></div>
<div class="m2"><p>ز بخت جز بدر او کسی نیابد کام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که کینۀ او را بدل بیندیشد</p></div>
<div class="m2"><p>ز موی خویش نهد دام مرگ بر اندام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگاه کردن شادی بمن برحمت او</p></div>
<div class="m2"><p>کنون برحم ز من سوی او شود پیغام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همیشه سعیش مشکور باد و فالش نیک</p></div>
<div class="m2"><p>که کار من بنوا کرد و عیش من پدرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنام خدمت میمون او گرفتم فال</p></div>
<div class="m2"><p>بیمن دولت منصور او گرفتن نام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو شکر او بدل اندیشه کردم از بس فخر</p></div>
<div class="m2"><p>ز طبع خاطر من شکر داد نظم کلام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p> همی نوشتم اشعار شکر او روزی</p></div>
<div class="m2"><p>صریر منظوم آمد بشکر او ز اقلام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجا خزینۀ زرّ و سفینۀ گهرست</p></div>
<div class="m2"><p>بدست شاه جهانست هر دو را انجام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدایگان خراسان همی بپردازد</p></div>
<div class="m2"><p>خزینه را بسخا و سفینه را بحسام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کلام و تیغ شه است آنکه جبرئیل امین</p></div>
<div class="m2"><p>ز آسمان سخن آورد وانگهی صمصام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدین ضمیر بخسته است در دل حساد</p></div>
<div class="m2"><p>بدان روان بفزوده است در تن خدّام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کدام زایر با فضل دید و نعمت او</p></div>
<div class="m2"><p>که بر نیامدش آواز شکر او ز مسام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بنعمتش بفزوده ست در زمین رتبت</p></div>
<div class="m2"><p>بهمتش بفزوده ست بر سپهر اجرام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز رای اوست خیالی خرد بجان اندر</p></div>
<div class="m2"><p>ز خشم اوست مثالی بر آسمان بهرام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر چه مایۀ تاریخ عالم ایام است</p></div>
<div class="m2"><p>فتوح اوست تواریخ گردش ایام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دلیل لشکر او هر کجا رود ظفرست</p></div>
<div class="m2"><p>خجسته مرکب او را ز نصرتست اعلام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون عجبتر از آن فتح فتح غرجستان</p></div>
<div class="m2"><p>که شد بدولت او مر سپاه او را رام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی حصارش کش سر همی ستارۀ گرای</p></div>
<div class="m2"><p>بناش کیوان بالا و سنگ آینه فام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p> شمیده مرغ بر آن بام برفشاند پر</p></div>
<div class="m2"><p>رمیده رنگ بر آن سنگ بر گذارد گام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زمینش آهن و پولاد و برج گونۀ کوه</p></div>
<div class="m2"><p>بسان بیشه سر برج او پر از ضرغام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان فکندی از منجنیق سنگ عدوی</p></div>
<div class="m2"><p>که پر شدی دل نور از نهیب او بظلام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپاه خسرو مشرق بفر دولت او</p></div>
<div class="m2"><p>چنان گرفتند آن حصن را چو باز حمام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدولت ملک آن ناحیت بدست آمد </p></div>
<div class="m2"><p>نه قلعه ماند و نه شاه و نه چاکر و نه غلام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خجسته بادش آغاز هر چه خواهد کرد</p></div>
<div class="m2"><p>وزان خجسته ترش نیز حاصل فرجام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بکامکاری و اقبال و روز و روز بهی</p></div>
<div class="m2"><p>نگاهدارش یا ذوالجلال و الاکرام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین که هست عزیز و چنین که هست بزرگ</p></div>
<div class="m2"><p>چنین که هست قوی و چنین که هست تمام</p></div></div>