---
title: >-
    شمارهٔ ۳۰ -  در صفت عمارت و باغ خواجه ابوالقاسم احمد بن حسن میمندی گوید
---
# شمارهٔ ۳۰ -  در صفت عمارت و باغ خواجه ابوالقاسم احمد بن حسن میمندی گوید

<div class="b" id="bn1"><div class="m1"><p>بهار زینت باغی نه باغ بلکه بهار</p></div>
<div class="m2"><p>بهار خانۀ مشکوی و مشکبوی بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرشت طبعش را هر چهار طبع هواست</p></div>
<div class="m2"><p>نهاد سالش را هر چهار فصل بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنگ صورت او کارنامۀ نقاش</p></div>
<div class="m2"><p>ز بوی تربت او بارنامۀ عطار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوا ز نکهت بویندگان او تبّت</p></div>
<div class="m2"><p>زمین ز نضرت بینندگان او فرخار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بصر ز صورت او عالم صور گردد</p></div>
<div class="m2"><p>اگر نگاه کنی ژرف سوی آن اشجار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مرغزار یکی شیر دارد اندر بر</p></div>
<div class="m2"><p>چو واق واق یکی مردم خرد آثار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسان کرگ یکی پیل بر گرفته بشاخ</p></div>
<div class="m2"><p>بسان ارگ یکی بر هوا کشیده حصار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حصارهای پر امثالهای مینا رنگ</p></div>
<div class="m2"><p>ارم نیند و جدا هر یکی ارم کردار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسان قبه و ارتنگ مانویش غلاف</p></div>
<div class="m2"><p>بسان کعبه و دیبای خسرویش ازار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دیبهی که برنگ پرند هندی هست</p></div>
<div class="m2"><p>زبر جدینش بود پود و زمردینش تار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی نشاط کند بلبل اندر گوئی</p></div>
<div class="m2"><p>چغانه دارد در کام و در گلو مزمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوای زیر و بم آرد ز حلق بی بم و زیر</p></div>
<div class="m2"><p>همی فسوس کند بر نوای موسیقار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درخت نارنج ، از خامه گوئیا شنگرف</p></div>
<div class="m2"><p>بریخته است کسی مشت مشت بر زنگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسان مجمر میناست گرد مشک بر او</p></div>
<div class="m2"><p>بخار مشگ برآید همی ز شعلۀ نار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز برگ و بار همه طوطیان پرانند</p></div>
<div class="m2"><p>که برگشان همه پرّست و بارشان منقار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو گنج خانۀ پرویز روی تربت او</p></div>
<div class="m2"><p>ز سیم و نقره و یاقوت و زرّ مشت افشار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خجسته باز گشاده دهان مشکین دم</p></div>
<div class="m2"><p>گشاده نرگس چشم دژم ز خواب و خمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو جام زرّین کاندر میان او عنبر</p></div>
<div class="m2"><p>چو جام سیمین کاندر میان او دینار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی نه چشم ولیکن بگونۀ چشمی</p></div>
<div class="m2"><p>که دیده اش از شبه باشد مژه ز زرّ عیار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی نه چتر ولیکن بگونۀ چتری</p></div>
<div class="m2"><p>که سیم خامش و میناش چون سرین زنگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنفشه زارش گوئی حریر سبزستی</p></div>
<div class="m2"><p>که نیل ریزه برو بر پراکنی هموار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو مهره های کبودست بر بریشم سبز</p></div>
<div class="m2"><p>بطبع بسته و پیوسته بی گره ستوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه صحایف اقلیدس است پنداری</p></div>
<div class="m2"><p>که شکلهای دهد مر مهندسان را کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپهر نی و بسان سپهر مرکز نور</p></div>
<div class="m2"><p>ستاره هست ولیکن ستارۀ سیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مجرّه وار یکی جوی اندرو گذرد</p></div>
<div class="m2"><p>بر آب خضر تبه کرد آب او بازار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو رای عالم صافی چو جان عارف پاک</p></div>
<div class="m2"><p>چو شعر نیک روان و چو دین حق دوّار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر بجنبد گوئی همی بجنبد جان</p></div>
<div class="m2"><p>اگر بپیچد گوئی همی بپیچد مار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسان قارون گاهی فرو شود بزمین</p></div>
<div class="m2"><p>گهی شود بهوا بر چو جعفر طیاّر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گهی ببینی گشته چو پشت بازخشین</p></div>
<div class="m2"><p>گهی منقط بینی چو پشت سنگین سار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بخار او که بخیزد ز دل فروزد شمع</p></div>
<div class="m2"><p>ز دیده عقد کند ، عقد لؤلوی شهسوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر زبان بگشائی بوصف خم بزرگ</p></div>
<div class="m2"><p>روا بود که دهد وصف او بشعر شعار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو همت ملکانست بر گذشته ز وهم</p></div>
<div class="m2"><p>کنارۀ شرفش بر شرف گرفته قرار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بزرگ طاقش را کالبد فلک بوده</p></div>
<div class="m2"><p>بلند گنبد او را قضا زده پرگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نبشته هاش جمال است و خشتهاش لقا</p></div>
<div class="m2"><p>نگارهاش کمال و عیارهاش فخار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لطیف تر ز جوانی و خوشتر از نعمت</p></div>
<div class="m2"><p>وزو برون نشود آن دو چیز را هنجار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگر بخانۀ کافوری اندرون نگری</p></div>
<div class="m2"><p>زمان مشرق بینی در ابتدای نهار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو کف موسی کایت همی نمود از جیب</p></div>
<div class="m2"><p>چنانکه روی بهشتی بود بروز شمار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طراز زرّین بر جامۀ ملوک بود</p></div>
<div class="m2"><p>که ماند او را زرّین طراز بر دیوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر کنی صفت خانۀ نگارستان</p></div>
<div class="m2"><p>برون شود ز طبایع بر آتش تیمار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدیع گنبد او همچو جام کیخسرو</p></div>
<div class="m2"><p>درو دوازده و هفت را مسیر و مدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بسان بتکده ها طاقهاش پر صورت</p></div>
<div class="m2"><p>شکفته چون گل و بی عیب چون دل ابرار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فروغ روی چو مهشان همی نماید گل</p></div>
<div class="m2"><p>شکنج زلف سیه شان همی فشاند قار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نه وشّی و همه با جامه های وشّی رنگ</p></div>
<div class="m2"><p>نه جانور همه باغمزگان جان او بار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه کان زرّ و همه زرّ سرخ بی تخلیط</p></div>
<div class="m2"><p>نه کان سیم و همه سیم نقرۀ بی بار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درو نگاشته بر فال نیک و اختر سعد</p></div>
<div class="m2"><p>خدایگانرا بر بزم و رزم و گاه شکار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شکار : دولت عالی و رزم : قهر عدو</p></div>
<div class="m2"><p>بقا و نعمت را کرده بزمگاه اظهار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قرار دلشدگانست و گنج بی دربان</p></div>
<div class="m2"><p>نجات ممتحنانست و داروی بیمار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وگر بگنبد فروار خانه آری دل</p></div>
<div class="m2"><p>سخن منقش گردد ز فرّ آن فروار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو جعد زلف بتانست در شکسته بهم</p></div>
<div class="m2"><p>گره گرهش میان و شکن شکنش کنار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شکن یکی و گره برشکن هزار افزون</p></div>
<div class="m2"><p>گره یکی و شکن بر گره فزون ز هزار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وگر ز صفۀ خانه نظر کنی سوی باغ</p></div>
<div class="m2"><p>زبر جدین شود اندر دو چشم تو دیدار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اثر اثیر کند برزمین ز بهر چرا</p></div>
<div class="m2"><p>که عکس او باثیر اندرون کند آثار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز حسن گوئی پیوسته گوهرش بهنر</p></div>
<div class="m2"><p>ز لطف گوئی پرورده دولتش بکنار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>درخت او که بروید لطیف تر ز نجوم</p></div>
<div class="m2"><p>بخار او که بخیزد شریف تر ز فخار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدین صفات به میمند باغ خواجۀ ماست</p></div>
<div class="m2"><p>که کدخدای جهانست و سیّد احرار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عمید دولت ابوالقاسم احمد بن حسن</p></div>
<div class="m2"><p>که هست طاغت او بر سر زمانه فسار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چنار کرد دعا تا مگر بود سخنش</p></div>
<div class="m2"><p>از آن چو پنجۀ مردم شدست برگ چنار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سیاست و کرم خواجه گردش فلک است</p></div>
<div class="m2"><p>کزو سوار پیاده شود ، پیاده سوار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بخواجه عیب و عوار زمانه گشت هنر</p></div>
<div class="m2"><p>گرفت از آن هنر خواجه جای عیب و عوار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز نور روز گریزد همیشه ظلمت شب </p></div>
<div class="m2"><p>چو فخر پیدا گردد نهفته ماند عار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز خواجه جود پدید آید و ز گردون بخل</p></div>
<div class="m2"><p>ز ابر آب پدید آید و ز خاک غبار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زمین که کوه کشد بار آن کسی نکشد</p></div>
<div class="m2"><p>که او بعمر یکی پیش خواجه یابد بار </p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو دیده چهرش در چشم مردمست مقیم </p></div>
<div class="m2"><p>چو عقل مهرش باجان کند همیشه جوار </p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همه ستایش آفاق خواجه را صفت است</p></div>
<div class="m2"><p>همی کنند ستایندگان ازو تکرار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بسی کس است که منکر بود بصانع خویش </p></div>
<div class="m2"><p>همی دهد ببزرگی و فضل او اقرار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بایستند بزرگان چو پیش او برسند </p></div>
<div class="m2"><p>چو در شوند بدریا بایستند انهار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کفش پدید بمقدار و جود ازو خیزد</p></div>
<div class="m2"><p>اگر چه نیست پدیدار جود را مقدار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مثالش آنکه سخن خیزد از حروف همی</p></div>
<div class="m2"><p>اگر چه هست حروف اندک و سخن بسیار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو بدره مهر کند مهر اوست للشعراء</p></div>
<div class="m2"><p>چو باره داغ کند داغ اوست للزّوار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بصورت لب مردم بود ز بوس کرام</p></div>
<div class="m2"><p>بهر کجا شود ، او را همه زمین و دیار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از آنکه چشم شقاوت بود عداوت او</p></div>
<div class="m2"><p>شود بدیدن اعدای او دو دیده فگار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که داندش که نیارد حسد ز دانش خویش</p></div>
<div class="m2"><p>که بیندش که نخواهدش چشم خویش نثار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز دوستی که عفو دارد از گنهکاران</p></div>
<div class="m2"><p>سپاس دارد و گیرد ز دیگران آزار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نبود و هم نبود جز بعرض خویش بخیل</p></div>
<div class="m2"><p>نکرد و هم نکند جز برای دین پیکار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چنان بداند احکام بود نی گوئی</p></div>
<div class="m2"><p>نهفته نیست ازو مر زمانه را اسرار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بنقش سیرت او مهر کرده شد معنی</p></div>
<div class="m2"><p>بنام مدحت او داغ کرده شد اشعار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از ایمنی که کندشان عجب نباشد اگر</p></div>
<div class="m2"><p>کند روان بر زنهاریان خود زنهار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بچوب ماند هر دو خلاف و طاعت او</p></div>
<div class="m2"><p>ازین : ولی را منبر . وزان : عدو را دار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بیک عطاش چنان سائلس غنی گردد</p></div>
<div class="m2"><p>که بدره هاش بود گنج و کیسه ها قنطار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همیشه تا همی امروز باشد از پس دی</p></div>
<div class="m2"><p>همیشه تا همی امسال باشد از پس پار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بقاش باد و سرش سبز باد و کار بکام</p></div>
<div class="m2"><p>فلک مساعد و دولت رفیق و ایزد یار</p></div></div>