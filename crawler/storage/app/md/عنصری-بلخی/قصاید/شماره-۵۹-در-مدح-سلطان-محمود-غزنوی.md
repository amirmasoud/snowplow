---
title: >-
    شمارهٔ ۵۹ -  در مدح سلطان محمود غزنوی
---
# شمارهٔ ۵۹ -  در مدح سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>چیست آن آبی چو آتش و آهنی چون پرنیان</p></div>
<div class="m2"><p>بیروان تن پیکری پاکیزه چون بی‌تنْ روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بجنبانیش آب است، ار بلرزانی درخش</p></div>
<div class="m2"><p>ور بیندازیش تیر است، ار بدو یازی کمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خرد آگاه نه در مغز باشد چون خرد</p></div>
<div class="m2"><p>از گمان آگاه نه در دل بود همچون گمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آینه دیدی بر او گسترده مروارید خرد</p></div>
<div class="m2"><p>ریزۀ الماس دیدی بافته بر پرنیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر از رنجش به چشم اندر نماینده درست</p></div>
<div class="m2"><p>چون به آب روشن اندر پر ستاره آسمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوستان دیدار و آتش کار و نشناسد خرد</p></div>
<div class="m2"><p>کآتش افروخته ست آن یا شکفته بوستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب داده بوستانی سبز چون مینا برنگ</p></div>
<div class="m2"><p>زخم او همرنگ آتش بشکفاند ارغوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پرند او چشمۀ سیماب دارد بی کنار</p></div>
<div class="m2"><p>و اندر آهن گنج مروارید دارد بیکران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچکس دیده است مر سیماب را چشمۀ پرند</p></div>
<div class="m2"><p>هیچکس دیده است مروارید را پولاد کان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از گل تیره است و شاخ رزم را روشن گل است</p></div>
<div class="m2"><p>گلستان رزمگه گردد از او چون گلسِتان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به دست شاه باشد مار باشد بی‌فُسون</p></div>
<div class="m2"><p>کشتن بدخواه او را تیز باشد بی‌فَسان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه گیتی خسرو لشکرکش لشکرشکن</p></div>
<div class="m2"><p>سایهٔ یزدان شه کشورده کشورسِتان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیر کردارش بزرگی ، زیر گفتارش خرد</p></div>
<div class="m2"><p>زیر پیمانش سپهر و زیر فرمانش جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر سخن گوید ، خرد او را ستاید در سخن</p></div>
<div class="m2"><p>ور میان بندد ، بزرگی پیش او بندد میان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان سخن گوید ، بنامش آفرین گوید خرد</p></div>
<div class="m2"><p>دل دهان گردد بدان گفتار و اندیشه زبان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرنه از بهر زمین بوسیدنستی پیش او</p></div>
<div class="m2"><p>مرمیان را نیستی پیوند و بند اندر میان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پست گشته راستی از نام او گردد بلند</p></div>
<div class="m2"><p>پیر گشته مردمی از یاد او گردد جوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای خرد را جان و جانرا دانش و دلرا امید</p></div>
<div class="m2"><p>پادشاهی را چراغ و نیکنامی را نشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سوختت تیغت درفش لشکر ترکان چین</p></div>
<div class="m2"><p>بر زده گرد سپاهت لشکر هندوستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر دل تیره نهاده پیش یزدان برده اند</p></div>
<div class="m2"><p>داغ تمییز تو ای شاه جهان چیپال و خان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر سپهر مهر مهری ، در نگین دادمهر</p></div>
<div class="m2"><p>در سر گفتار چشمی ، در سر کردار جان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خواسته بخشی که خواهنده چنان داند که هست</p></div>
<div class="m2"><p>زیر هر پیچی ز انگشت تو گنجی شایگان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندر ایران از عطای تو بوادی زین سپس</p></div>
<div class="m2"><p>زر نستاند ستاننده از دهنده رایگان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کوه کان باد وزان گردد به جنبش اسب تست</p></div>
<div class="m2"><p>کوه گردد زیر زین و باد گردد زیر ران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرت نیل و ناردان باید به جنگش تیز کن</p></div>
<div class="m2"><p>گرد میدان : نیل گردد ، سنگریزه : ناردان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رجم دیوان را ستاره چون شود در تیره شب</p></div>
<div class="m2"><p>تیر تو چونان رود در جوشن و بر گستوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تن بامید تو دارد زندگانیرا بکام </p></div>
<div class="m2"><p>جان ز بیم تیغ تو بر مرگ دارد دیده بان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از هنر نیکی نیاید بی دل و بازوی تو</p></div>
<div class="m2"><p>وز رمه چیزی نماند چون بماند بی شبان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کارخواهی ، کاربخشی ، کاربندی کارده</p></div>
<div class="m2"><p>کاربینی ، کارجوئی ، کارسازی ، کاردان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شادی و شاهی تو داری شاد باش و شاه باش</p></div>
<div class="m2"><p>جامۀ شادی تو پوش و نامۀ شادی تو خوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نیک باد آن جان همیشه کز تو باشد نیک بخت</p></div>
<div class="m2"><p>شاد باش آن دل همیشه کز تو باشد شادمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا به نوروز اندرون باشد نشان نوبهار</p></div>
<div class="m2"><p>تا سپاه تیر ماه آرد نشان مهرگان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خرمّی و زندگانی و بزرگی و هنر</p></div>
<div class="m2"><p>با تو باد این هر چهار ، ای شاه گیتی جاودان</p></div></div>