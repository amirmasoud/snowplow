---
title: >-
    شمارهٔ ۸ -  در مدح امیر نصر بن ناصر الدین سبکتگین گوید
---
# شمارهٔ ۸ -  در مدح امیر نصر بن ناصر الدین سبکتگین گوید

<div class="b" id="bn1"><div class="m1"><p>سده جشن ملوک نامدارست</p></div>
<div class="m2"><p>ز افریدون و از جم یادگارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین گویی تو امشب کوه طورست</p></div>
<div class="m2"><p>کزو نور تجلّی آشکارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر این روزست شب خواندش نباید</p></div>
<div class="m2"><p>و گر شب روز شد خوش روزگارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همانا کاین دیار اندر بهشت است</p></div>
<div class="m2"><p>که بس پرنور و روحانی دیارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک را با زمین انبازی آمد</p></div>
<div class="m2"><p>که رسم هر دو تن در یک شمارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه اجرام آن ارکان نورست</p></div>
<div class="m2"><p>همه اجسام این اجزای نارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه کان بیجاده است گردون</p></div>
<div class="m2"><p>چرا باد هوا بیجاده بارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه چیزست آن درخت روشنایی</p></div>
<div class="m2"><p>که بر یک اصل و شاخش صد هزارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کهی سرو بلندست و گهی باز</p></div>
<div class="m2"><p>عقیقین گنبد زرّین نگارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ار ایدون کو بصورت روشن آمد</p></div>
<div class="m2"><p>چرا تیره دمش همرنگ قارست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر از فصل زمستانست بهمن</p></div>
<div class="m2"><p>چرا امشب جهان چون لاله زارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلاله ماند این لیکن نه لاله است</p></div>
<div class="m2"><p>شرار آتش نمرود و نارست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی مر موج دریا را بسوزد</p></div>
<div class="m2"><p>بدان ماند که خشم شهریارست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپهبد میر نصر ناصر دین</p></div>
<div class="m2"><p>که دین را پشت و دولت را شعارست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بجائی کز نیاز آنجا تموزست</p></div>
<div class="m2"><p>نسیم جود او تازه بهارست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بجای زخم او خارا خمیرست</p></div>
<div class="m2"><p>بجای بخششش دریا غبارست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر شمشیر او مغفر شکافست</p></div>
<div class="m2"><p>سر پیکان او جوشن گذارست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به پیش عزم او صحرا و دشت است</p></div>
<div class="m2"><p>حصار دشمن ، ار چند استوارست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>امارت را بلفظش اتفاقست</p></div>
<div class="m2"><p>حکومت را برأیش اعتبارست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بکار اندر حکیم پیش بین است</p></div>
<div class="m2"><p>ببار اندر امیر بختیارست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بشادی در کریم و چیز بخش است</p></div>
<div class="m2"><p>بخشم اندر حلیم و بردبارست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر او را بنده باشی عزّ و فخرست</p></div>
<div class="m2"><p>جز او را بنده باشی ذلّ و عارست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بتیغ قهرش اندر فلسفی را</p></div>
<div class="m2"><p>نشان جبر و آن اختیارست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بحد فضلش اندر هندسی را</p></div>
<div class="m2"><p>طریق هندسه علم نزارست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن زردست دایم روی دینار</p></div>
<div class="m2"><p>که نزد جود او دینار خوارست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امیر ار خوار دینارست شاید </p></div>
<div class="m2"><p>کزو مدّاح او دینار خوارست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شکار خسروان مرغ است و نخجیر</p></div>
<div class="m2"><p>سپهبد خسرو خسرو شکارست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نشاط شهر یاران روز بزم است</p></div>
<div class="m2"><p>نشاط او بروز کارزارست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر او ممتحن را دستگاهست</p></div>
<div class="m2"><p>بر او منهزم را زینهارست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان خواهند ازو خواهندگان چیز</p></div>
<div class="m2"><p>که پنداری که نزدش مستعارست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان را آسمان پر نوال است</p></div>
<div class="m2"><p>خدم را پادشاه حق گزارست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بروز جنگ مر شمشیر او را</p></div>
<div class="m2"><p>دنی تر چیز شیر مرغزارست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p> ازو خواهند یمن و یسر کورا</p></div>
<div class="m2"><p>میان یمن و یسر اندر قرارست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همانجا یمن باشد کو یمین است</p></div>
<div class="m2"><p>همانجا یسر باشد کو یسارست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رسومش مر نکایت را مزاج است</p></div>
<div class="m2"><p>مثالش مر جلالت را عیارست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز حرص عفو کو دارد بگیتی</p></div>
<div class="m2"><p>گرامی تر بنزدش اعتذارست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>الا تا مایۀ ظلمت ز نورست</p></div>
<div class="m2"><p>الا تا مایۀ نور از نهارست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>الا تا هر کجا نازست رنج است</p></div>
<div class="m2"><p>الا تا هر کجا خرماست خارست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بقا بادش چنان کورا مرادست</p></div>
<div class="m2"><p>همی تا دور گردون را مدارست</p></div></div>