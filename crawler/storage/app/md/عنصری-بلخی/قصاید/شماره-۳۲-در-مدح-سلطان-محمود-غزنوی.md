---
title: >-
    شمارهٔ ۳۲ -  در مدح سلطان محمود غزنوی
---
# شمارهٔ ۳۲ -  در مدح سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>گر نه مشکست از چه معنی شد سر زلفین یار ؟</p></div>
<div class="m2"><p>مشکبوی و مشکرنگ و مشکسای و مشکبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ار دل ما را ببست او خود چرا در بند شد ؟</p></div>
<div class="m2"><p>ور قرار ما ببرد او خود چرا شد بیقرار ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور نشد ابروش عاشق چند باشد گوژ پشت</p></div>
<div class="m2"><p>ورنه می خورده است چشمش از چه باشد خمار ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهتابستش بنا گوش و خطش سنبل بود</p></div>
<div class="m2"><p>آفتابستش رخ و بالاش سرو جویبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچکس دیدست ماهی کاندرو سنبل دمید ؟</p></div>
<div class="m2"><p>هیچکس دیدست سروی کافتاب آورد بار ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ار شوی نزدیک زلفش تا بکاوی جعد او</p></div>
<div class="m2"><p>آستین پر مشک باز آیی و پر عنبر کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بکاویدمش جعد و تا ببوئیدمش زلف</p></div>
<div class="m2"><p>تا بخاییدمش لعل و تاش بگرفتم کنار ،</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دو دستم عنبرست و در مشامم غالیه</p></div>
<div class="m2"><p>در دهانم انگبین و در کنارم لاله زار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرخی از خون نگسلد هرگز چنان کز نار نور</p></div>
<div class="m2"><p>مرمان گویند ، لیکن من ندارم استوار ،</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانکه من بارم برخ بر خون و روی اوست سرخ</p></div>
<div class="m2"><p>زانکه رویش جای نور است و دل من جای نار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او و من هر دو همی نازیم و ناز من بهست</p></div>
<div class="m2"><p>کو بحسن خویش نازد من بمدح شهریار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خسرو مشرق یمین دولت و بنیاد مجد</p></div>
<div class="m2"><p>آفتاب ملک ، امین ملت و فخر تبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا ببندد ، یا گشاید ، یا ستاند ، یا دهد</p></div>
<div class="m2"><p>تا جهان همی مر شاه را این چار کار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنچه بستاند : ولایت ، آنچه بدهد : خواسته</p></div>
<div class="m2"><p>آنچه بندد : دست دشمن ، آنچه بگشاید : حصار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نصرت و فتح است بازی کردن شاه جهان</p></div>
<div class="m2"><p>نصرتش عزمست و حاصل فتح و بازی کار زار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تیغ او هرگز نجوید جز دل شیران نیام</p></div>
<div class="m2"><p>تیر او ترکش نخواهد جز تن مرد سوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیزۀ خسرو ستاره است و دل شیران فلک</p></div>
<div class="m2"><p>تیغ او شیرست و مغز جنگجویان مرغزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جز زبان چیزی نگوید پیش او هنگام حرب</p></div>
<div class="m2"><p>جز دهان چیزی نجنبد پیش او هنگام بار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن دهان جنبان بود کو شاه را بوسد زمین</p></div>
<div class="m2"><p>وان زبان گو یا بود کز شاه خواهد زینهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از هوای باغ او بوی بهشت آرد نسیم</p></div>
<div class="m2"><p>وز زمین مجلس او مشکبو خیزد بخار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زیر پای نیکخواهش روید از پولاد گل</p></div>
<div class="m2"><p>زیر پای بدسگالش خیزد از دریا غبار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هم بدو مجبور گردد ، هم بدو مختار مرد </p></div>
<div class="m2"><p>جز بدو پیدا نیاید حکم جبر از اختیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور چه حکم پادشاهی هر که را باشد یکیست</p></div>
<div class="m2"><p>پادشاهی را به محمودست فخر و اعتبار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرچه از طبعند هر دو ، به بود شادی ز غم</p></div>
<div class="m2"><p>ور چه از چوبند هر دو ، به بود منبر ز دار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور کسی بی او زیادت گیرد و فخر آورد</p></div>
<div class="m2"><p>آن زیادت سر بسر نقصان بود ، آن فخر عار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جز بکام او نگردد تا بگردد آسمان</p></div>
<div class="m2"><p>جز برای او نباشد تا بباشد روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر مرا صد سال باشد عمر و گویم شکر او</p></div>
<div class="m2"><p>هم نگویم شکر کردارش یکی از صد هزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جامه ای پوشید بخت من رهی را جود او</p></div>
<div class="m2"><p>جامه ای کو را سعادت بود پود و فخر تار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شکر را بر جان شیرین صورتی کردم بدیع</p></div>
<div class="m2"><p>پیش ایزد برد خواهم صورتش روز شمار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بگویم پیش او جز کرد گارش هیچکس</p></div>
<div class="m2"><p>شکر او پیش که گویم جز که پیش کردگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا همی گردد فصول عالم از گشت فلک</p></div>
<div class="m2"><p>گه تموز و گاه تیر و گه زمستان ، گه بهار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاه را سر سبز باد و جان بجای و تن قوی</p></div>
<div class="m2"><p>تیغ تیز و امر نافذ بادش و دل شاد خوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تاج داران جهان پیش بساطش خاکبوس</p></div>
<div class="m2"><p>دشمنان ملک از گرد سپاهش خاکسار</p></div></div>