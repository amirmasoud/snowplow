---
title: >-
    شمارهٔ ۶۸ -  در مدح سلطان محمود غزنوی
---
# شمارهٔ ۶۸ -  در مدح سلطان محمود غزنوی

<div class="b" id="bn1"><div class="m1"><p>چو آفرید بتا روی تو ز دوده خدای</p></div>
<div class="m2"><p>مجوی فتنه و روی ز دوده را مزدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعارض تو آن گرد مشک سوده بسست</p></div>
<div class="m2"><p>بچشم سرمه مکن ، خلق را بلا منمای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلای تافته جعدت بسست بر دل خلق</p></div>
<div class="m2"><p>متاب زلف و دگر بر بلا بلا مفزای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببستن کمر و لب گشادن از خنده</p></div>
<div class="m2"><p>همی میان و دهان ترا نبیند رای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نمود نخواهی همی میان و دهان</p></div>
<div class="m2"><p>یکی ببند لب از خنده و مبان بگشای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر بجور مکوشی که جور نپسندد</p></div>
<div class="m2"><p>خدایگان خراسان امیر بار خدای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یمین دولت پیروز روز ملک افروز</p></div>
<div class="m2"><p>امین ملت پیغمبر جهان آرای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه امر نافذ او خلق را چه گردش چرخ</p></div>
<div class="m2"><p>چه سایۀ علمش ملک را چه فرّ همای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک بنای سعادت همی بپای کند</p></div>
<div class="m2"><p>بر آن زمین که همی شاه بسپردش بپای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوا چو خاک بطبعش فرو نشیند پست</p></div>
<div class="m2"><p>زمین چو ذره ز حلمش بماند اندر وای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیال همت او را اگر بپیماید</p></div>
<div class="m2"><p>بعمر خویش نپیماید آسمان پیمای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمند او ببرد زور پیل گردنکش</p></div>
<div class="m2"><p>سنان او بکند یشک شیر دندان خای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی نگون شود از بأس و از مهابت شاه</p></div>
<div class="m2"><p>به ترک خانۀ خان و به هند رایت رای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هنر بمایۀ فرهنگ او ندارد سنگ</p></div>
<div class="m2"><p>خرد بمرتبت رای او نگیرد جای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر جمال پرستی سیرش را بپرست</p></div>
<div class="m2"><p>وگر کمال ستایی هنرش را بستای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگفت عادت او هیچ حلم را که برو</p></div>
<div class="m2"><p>نگفت فکرت او هیچ خلق را که میای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برای بردن نامش دهان بعنبر شوی</p></div>
<div class="m2"><p>بحال گفتن مدحش زبان بزر اندای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مجوی دولت خود را جز آن مبارک در</p></div>
<div class="m2"><p>زمانه را مطلب جز در آن خجسته سرای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمان کینه ورش هم بزخم کینۀ اوست</p></div>
<div class="m2"><p>بزخم مار بود هم زمان ما را فسای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدایگانا علمی نماند و فایده ای</p></div>
<div class="m2"><p>که خاطر تو مر آنرا نکرد دست گرای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تراست نعمت ،پروردنی همی پرور</p></div>
<div class="m2"><p>تراست فرمان ، فرمودنی همی فرمای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مبارکت باد این جشن مهرگان بزرگ</p></div>
<div class="m2"><p>نصیب شادی ازین جشن برگذر بربای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بساط بزم کن از گونه گونه تحفۀ باغ</p></div>
<div class="m2"><p>سرای خلد کن از نعمۀ سرود سرای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نشستگاه یکی نوبهار ساز بدیع</p></div>
<div class="m2"><p>بجای گل می سوری بجای بلبل نای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدار بسته همیدون دل ولی و عدو</p></div>
<div class="m2"><p>ولی بنعمت و ناز و عدو بقلعۀ نای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر زمانه نگردد تو با زمانه بگرد</p></div>
<div class="m2"><p>وگر سپهر نپاید تو با سپهر بپای</p></div></div>