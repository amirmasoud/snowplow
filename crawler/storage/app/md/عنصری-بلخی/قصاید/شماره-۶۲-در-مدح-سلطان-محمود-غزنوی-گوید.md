---
title: >-
    شمارهٔ ۶۲ -  در مدح سلطان محمود غزنوی گوید
---
# شمارهٔ ۶۲ -  در مدح سلطان محمود غزنوی گوید

<div class="b" id="bn1"><div class="m1"><p>بفال نیک و بفرخنده روزگار ، جهان</p></div>
<div class="m2"><p>بسان دولت شاه جهان شدست جوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ز گوهر ناسفته ابر شد چو صدف</p></div>
<div class="m2"><p>چرا شد از گل ناکشته دشت چون بستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فکند شادروانی بدشت باد صبا</p></div>
<div class="m2"><p>که تار و پودش هست از زبر جد و مرجان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مجلس ملک الشرق از نثار ملوک</p></div>
<div class="m2"><p>بجعفری و بعدلی نهفته شادروان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنار پر گل از آن کرد گل که ابر سیاه</p></div>
<div class="m2"><p>فرو گذشت بدو پر گلاب کرده دهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درخت را حسد آمد همی ز شاعر شاه</p></div>
<div class="m2"><p>که شعر خواند بر شاه و بیندش بعیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان و چشم برآرد همی کنون ز حسد</p></div>
<div class="m2"><p>شکوفه هاش همه چشم و برگهاش زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دخان از آتش جستی همیشه تا بوده است</p></div>
<div class="m2"><p>کنون چه بود که آتش همی جهد ز دخان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان جهد که تو گویی همی پدید آید</p></div>
<div class="m2"><p>ز گرد ، لشکر جرار حملۀ سلطان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یمین دولت عالی امین ملت حق</p></div>
<div class="m2"><p>نظام دولت تازی و ملت یزدان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بروزگار عزیرش عزیز گشت خرد</p></div>
<div class="m2"><p>باعتقاد درستش درست شد ایمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بندگیش علامت بود میان بستن</p></div>
<div class="m2"><p>ملوک ازیرا زرین کنند بند میان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخدمتش ملکان سر فرو برند نخست</p></div>
<div class="m2"><p>از آن بتاج سزاوار شد سر ملکان </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اجل بیاید و انگشت برنهد بعدو</p></div>
<div class="m2"><p>بساعت اندر کو تیر برنهد بکمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزرگ چون خردست و عزیز چون دولت</p></div>
<div class="m2"><p>قوی چو حجت اسلام و پاک چون فرقان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چگونه دست گذارد بدین جهان جودش</p></div>
<div class="m2"><p>که جود او را باید چنین هزار جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود عطای امیران بکیسه و کاغذ</p></div>
<div class="m2"><p>عطای میر خراسان بگنج خانه و کان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی رود بر هر لفظی از مدایح او</p></div>
<div class="m2"><p>هزار حجت و با هر یکی هزار زبان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بسکه آتش زد شاه در ولایت هند</p></div>
<div class="m2"><p>کشیده دود ز بتخانه هاش بر کیوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آن زمین ز تفش گرمسیر گشت هوا</p></div>
<div class="m2"><p>سیاه گشت هم از دود چهرۀ ایشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بعمر شاه جهان بر زمین قیامت را</p></div>
<div class="m2"><p>رسوم شاه به تیغ است و شاه هندستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز آه سرد برآوردن هزیمتیان</p></div>
<div class="m2"><p>زمین ترکستان سرد سیر گشت چنان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قیامت آید این هر دو داغ مانده بود</p></div>
<div class="m2"><p>ز تیغ شاه به هندوستان و ترکستان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر بخواهی دیدن تو روزنامۀ فخر</p></div>
<div class="m2"><p>رسوم شاه ببین و مدیح شاه بخوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بعمر و روزی غمگین مباش تا دهمت</p></div>
<div class="m2"><p>نشان روزی بی رنج و عمر جاویدان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بشاه رو که ده انگشت شاه در دو کفش</p></div>
<div class="m2"><p>کلید روزی خلق است و چشمۀ حیوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سخن فروشان آیند نزد ، او چو روند</p></div>
<div class="m2"><p>ز جود او شده گوهر فروش و بازرگان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی مبارک حرزست قصد خدمت او</p></div>
<div class="m2"><p>کجا که آفت درویشی اندروست عیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان رسید بلندی که او نماید راه</p></div>
<div class="m2"><p>بدان دهند بزرگی که او دهد فرمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شود اشارت تیغش دعای پیغمبر</p></div>
<div class="m2"><p>اگر عدو کند از ماه جوشن و خفتان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز جان و عقل مصور شده است پنداری</p></div>
<div class="m2"><p>که سیرتش همه عقلست و صورتش همه جان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر آنکسی که خدایش عزیز خواهد کرد</p></div>
<div class="m2"><p>بسوی خدمت شاهش دهد نخست نشان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیاز عرضه بدو کن که بی نیاز شوی</p></div>
<div class="m2"><p>حدیث او کن تا رسته گردی از حدثان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سخن بدو بر تابخت زی تو آرد رخت</p></div>
<div class="m2"><p>دلت بدو ده و آنگه دل ملوک ستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدوست قصد همه مردمان ، بدو باید</p></div>
<div class="m2"><p>که جز ولایت او جای نیست آبادان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مبارکست پی رای او بهر چه رود</p></div>
<div class="m2"><p>هزار گونه پدید آمدست ازو برهان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم از مبارکی رای شهریار آمد</p></div>
<div class="m2"><p>امیر زادۀ بغداد سوی او مهمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نگه توانستی داشتن ز آفت و عیب</p></div>
<div class="m2"><p>سپاه خانه خویش و ولایت کرمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ولیکن از قبل آن که او همی دانست</p></div>
<div class="m2"><p>کفایت و کرم و فضل خسرو ایران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیامد ایدر تا دولت استوار کند</p></div>
<div class="m2"><p>هم از نخستش محکم فرو نهد بنیان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زمین توانستی داشتن خدای نگاه</p></div>
<div class="m2"><p>گر استوار نکردی چنین بکوه گران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بزرگتر بود آن دولتی که شاه دهد</p></div>
<div class="m2"><p>بدست دولت و تأیید گر دهدش عنان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو طالعند بزرگان [و] او قران بزرگ</p></div>
<div class="m2"><p>ز حکم طالع باقی ترست حکم قران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه دولتی که ازو رفت ره برد بزوال</p></div>
<div class="m2"><p>نه مر زیارت او را تبه کند نقصان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رونده دولت و پاینده ملکتش پس از این</p></div>
<div class="m2"><p>چو پایدار زمین باشد و رونده زمان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همانکه با او پیکار جست و دندان زد</p></div>
<div class="m2"><p>کنون بطاعت او آمد از بن دندان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ایا گشاده بحق دست و آفریدۀ حق</p></div>
<div class="m2"><p>بتست دولت او را کفایت توران (؟)</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بگرید آنکه بخندد بکینه جستن تو</p></div>
<div class="m2"><p>نماند آنکه ببندد بکین تو پیمان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر مخالف تو جان آهنین دارد</p></div>
<div class="m2"><p>کندش ریزه سرنیزۀ تو چون سوهان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو شیر بیند دو چشم او شود تیره</p></div>
<div class="m2"><p>مگر ز دیدۀ شیر آب داده ای تو سنان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چنان که تازی زان کشور ای ملک تو بدین</p></div>
<div class="m2"><p>کسی نتازد از آن سر بدین سر میدان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جهان اگر چه بزرگست بر علامت تست</p></div>
<div class="m2"><p>بنامه ماند و نام تو از برش عنوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همیشه تا بخزان باد زرگری سازد</p></div>
<div class="m2"><p>شود بنوبت نوروز باد مشک افشان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بملک خویش بپای و به رای خویش برو</p></div>
<div class="m2"><p>بنام خویش بناز و بجای خویش بمان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زمانه داد تو داده است داد ملک بده</p></div>
<div class="m2"><p>خدای کام تو رانده است کام خویش بران</p></div></div>