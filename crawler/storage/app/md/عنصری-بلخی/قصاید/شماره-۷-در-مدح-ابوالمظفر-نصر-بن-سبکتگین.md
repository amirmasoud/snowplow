---
title: >-
    شمارهٔ ۷ -  در مدح ابوالمظفر نصر بن سبکتگین
---
# شمارهٔ ۷ -  در مدح ابوالمظفر نصر بن سبکتگین

<div class="b" id="bn1"><div class="m1"><p>بت که بتگر کندش دلبر نیست</p></div>
<div class="m2"><p>دلبری دستبرد بتگر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت من دل برد که صورت اوست</p></div>
<div class="m2"><p>آزری وار و صنع آزر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بدیعی ببوستان بهشت</p></div>
<div class="m2"><p>جفت بالای او صنوبر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست آن جعد سلسله که همی</p></div>
<div class="m2"><p>بوی عنبرده است و عنبر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ موئی شکافته از بالا</p></div>
<div class="m2"><p>زار تر زان میان لاغر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بینی آن چشم پر کرشمه و ناز</p></div>
<div class="m2"><p>که بدان چشم هیچ عبهر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیم بی بار اگر چه پاک بود</p></div>
<div class="m2"><p>چون بنا گوش آن سمنبر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد مه زان دو زلف دایره ایست</p></div>
<div class="m2"><p>نقطه ای زان دهانش کمتر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلطیفی دگر چنو نبود </p></div>
<div class="m2"><p>بکریمی چو میر دیگر نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردمی چیست ، مردمی عرض است</p></div>
<div class="m2"><p>جز دل پاک اوش جوهر نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ذات آزادگی است صورت او</p></div>
<div class="m2"><p>گرچه آزادگی مصوّر نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست رازی بزیر پردۀ عقل</p></div>
<div class="m2"><p>که دل شاه را مقرر نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای بسانیک مخبرا که همی</p></div>
<div class="m2"><p>منظرش را سزای مخبر نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه را مخبری بداد خدای</p></div>
<div class="m2"><p>کش از آن بیش هیچ منظر نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کجا کف او گشاده نشد</p></div>
<div class="m2"><p>دعوت جود را پیمبر نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بجز آن کش را دو صف کن که جز او</p></div>
<div class="m2"><p>بخل فرسا و جود پرور نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هست اندر جهان ظفر لیکن</p></div>
<div class="m2"><p>جز بر میر ابوالمظفر نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دست او روز جود پنداری </p></div>
<div class="m2"><p>چشمۀ کوثر است و کوثر نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خطبۀ ملک را بگرد جهان</p></div>
<div class="m2"><p>بجز از تخت شاه منبر نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لشکر جود را بگیتی در</p></div>
<div class="m2"><p>جز کف راد معسکر نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه دریا ز ابر پر گهرست</p></div>
<div class="m2"><p>چون ثنا گوی او توانگر نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اصل فهرست راد مردی را</p></div>
<div class="m2"><p>جز دل شاه درج و دفتر نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیست چون مهر او بخلد نسیم</p></div>
<div class="m2"><p>بجهنم چو خشمش آذر نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چیست آن تیر او که بگشاید</p></div>
<div class="m2"><p>که چنو هیچ باد صرصر نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرگ پرنده خوانمش به نبرد</p></div>
<div class="m2"><p>نی نخوانم که مرگ را پر نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر کجا میر رفت فتح آمد</p></div>
<div class="m2"><p>گرچه با میر هیچ لشکر نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کمتر از نثر باشد آن نظمی</p></div>
<div class="m2"><p>که بر او مدح میر زیور نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بچه کار آید و چه نرخ آرد</p></div>
<div class="m2"><p>صدفی کاندرونش گوهر نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>داد را کی شناسد آن شهری</p></div>
<div class="m2"><p>کاندرو شهریار داور نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا همی گردش مسیر نجوم</p></div>
<div class="m2"><p>جز بدین گنبد مدور نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>روزه پذ رفته باد و فرخ عید</p></div>
<div class="m2"><p>که بجز فرخیش اختر نیست</p></div></div>