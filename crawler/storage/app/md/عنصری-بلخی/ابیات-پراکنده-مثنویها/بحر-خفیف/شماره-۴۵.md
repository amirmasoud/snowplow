---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>مهر ایشان بود فیا وارم</p></div>
<div class="m2"><p>غمتان من بهر دو بگسارم</p></div></div>