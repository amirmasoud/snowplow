---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>بارگی خواست شاد بهر شکار</p></div>
<div class="m2"><p>بر نشست و بشد بدیدن شار</p></div></div>