---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>سر که یابد گسسته کیسنه را</p></div>
<div class="m2"><p>دور باشد بتاوه کرسنه را</p></div></div>