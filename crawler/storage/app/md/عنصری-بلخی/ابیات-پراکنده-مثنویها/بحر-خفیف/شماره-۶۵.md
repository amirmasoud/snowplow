---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ساخت آنگه یکی بیوگانی</p></div>
<div class="m2"><p>هم بر آئین و رسم یونانی</p></div></div>