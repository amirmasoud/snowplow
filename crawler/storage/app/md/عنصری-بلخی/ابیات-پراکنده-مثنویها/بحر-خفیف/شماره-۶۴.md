---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>هرچه یابی وزان فرومولی</p></div>
<div class="m2"><p>نشمرند از تو آن ببشکولی</p></div></div>