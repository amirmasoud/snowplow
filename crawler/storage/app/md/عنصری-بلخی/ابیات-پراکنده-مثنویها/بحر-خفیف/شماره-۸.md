---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ببر آورد بخت پوده درخت</p></div>
<div class="m2"><p>من بدین شادم و تو شادی سخت</p></div></div>