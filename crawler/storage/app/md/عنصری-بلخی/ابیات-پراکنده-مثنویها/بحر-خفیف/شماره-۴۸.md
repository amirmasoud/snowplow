---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ویحک ای ابر گنهکاران</p></div>
<div class="m2"><p>سنگک و برف باری و باران</p></div></div>