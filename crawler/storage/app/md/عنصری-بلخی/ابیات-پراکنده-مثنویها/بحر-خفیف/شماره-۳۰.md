---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>تیز شد عشق و در دلش پیچید</p></div>
<div class="m2"><p>جز غریو و غرنگ نپسیچید</p></div></div>