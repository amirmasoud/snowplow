---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>دل پهلو پسر بساز آورد</p></div>
<div class="m2"><p>ساز مهرش همه فراز آورد</p></div></div>