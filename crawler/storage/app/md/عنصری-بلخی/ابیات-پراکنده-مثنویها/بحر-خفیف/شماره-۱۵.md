---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>بیوفا هست دوخته بدو نخ</p></div>
<div class="m2"><p>بد گهر هست هیزم دوزخ</p></div></div>