---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>آب و آتش بهم نیامیزد</p></div>
<div class="m2"><p>بالوایه ز خاک بگریزد</p></div></div>