---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>منظر او بلند چون خوازه</p></div>
<div class="m2"><p>هر یکی زو بزینت تازه</p></div></div>