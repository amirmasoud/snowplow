---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>خواستم با نیاز و داشادش</p></div>
<div class="m2"><p>پدر اینجا بمن فرستادش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرکاتش همه ره هنرست</p></div>
<div class="m2"><p>برم از جان من عزیزترست</p></div></div>