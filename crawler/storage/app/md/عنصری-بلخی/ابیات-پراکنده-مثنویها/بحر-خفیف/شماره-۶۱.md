---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>بندیان داشت بی زوار و پناه</p></div>
<div class="m2"><p>برد با خویشتن بجمله براه</p></div></div>