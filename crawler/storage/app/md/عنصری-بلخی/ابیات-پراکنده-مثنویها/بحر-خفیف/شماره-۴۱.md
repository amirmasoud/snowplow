---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>رخ ز دیده نگاشته بسر شک</p></div>
<div class="m2"><p>وان سرشکش برنگ تازه زرشک</p></div></div>