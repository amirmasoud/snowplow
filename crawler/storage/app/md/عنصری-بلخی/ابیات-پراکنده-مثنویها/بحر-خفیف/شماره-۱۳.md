---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>رای دانا سر سخن ساریست</p></div>
<div class="m2"><p>نیک بشنو که این سخن باریست</p></div></div>