---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>مثل مو بود بدین اندر</p></div>
<div class="m2"><p>مثل زو فرین و آهن در</p></div></div>