---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>گاه اقبال آبگینه خنور</p></div>
<div class="m2"><p>بستاند ز تو عدو به بلور</p></div></div>