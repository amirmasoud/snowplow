---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>هرچه واجب شود ز باد افراه</p></div>
<div class="m2"><p>بکنید و جز این ندارم راه</p></div></div>