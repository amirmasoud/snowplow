---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>ترا هست محشر رسول حجاز</p></div>
<div class="m2"><p>دهنده بپول چنیوت جواز</p></div></div>