---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>یکی مهره بازست گیتی که دیو</p></div>
<div class="m2"><p>ندارد بترفند او هیچ تیو</p></div></div>