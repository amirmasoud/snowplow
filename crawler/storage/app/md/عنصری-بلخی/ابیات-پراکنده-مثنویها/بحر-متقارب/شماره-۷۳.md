---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>چو راهی بباید سپردن بگام</p></div>
<div class="m2"><p>بود راندن تعبیه بی نظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقیبان ز دیدن بمانند کند</p></div>
<div class="m2"><p>گر ایشان همیشه نباشند غند</p></div></div>