---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ز گرمی بر آن کوکبه بانگ زد</p></div>
<div class="m2"><p>که آن بانگ تب لرزه بر مانگ زد</p></div></div>