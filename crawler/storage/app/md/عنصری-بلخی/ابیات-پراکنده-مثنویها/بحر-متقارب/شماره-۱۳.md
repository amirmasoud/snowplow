---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ولیکن روانم ز تو سیر نیست</p></div>
<div class="m2"><p>دلم چون دل تو بکفشیر نیست</p></div></div>