---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>چنان دان که این هیکل از پهلوی</p></div>
<div class="m2"><p>بود نام بتخانه ار بشنوی</p></div></div>