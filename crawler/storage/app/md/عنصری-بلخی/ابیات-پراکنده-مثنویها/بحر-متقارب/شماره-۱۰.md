---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>به آسیب پای و بزانو و دست</p></div>
<div class="m2"><p>همی مردم افکند چون پیل مست</p></div></div>