---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>نشست و همی راند بر گل سرشک</p></div>
<div class="m2"><p>از آن روزگار گذشته برشک</p></div></div>