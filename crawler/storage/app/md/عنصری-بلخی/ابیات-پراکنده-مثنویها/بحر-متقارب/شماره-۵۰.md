---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>فزاینده شان خوبی از چهر ولاف</p></div>
<div class="m2"><p>سراینده شان از گلو زند واف</p></div></div>