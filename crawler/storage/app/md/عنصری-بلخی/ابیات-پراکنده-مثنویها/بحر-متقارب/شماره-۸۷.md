---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>گرانمایه کاری بفرّ و شکوه</p></div>
<div class="m2"><p>برفت و شدند آن بآیین گروه</p></div></div>