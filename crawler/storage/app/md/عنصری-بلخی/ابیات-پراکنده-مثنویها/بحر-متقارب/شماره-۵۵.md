---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>نیابد همی کوهکان سیم پاک</p></div>
<div class="m2"><p>بکان اندرون گوهرش گشته خاک</p></div></div>