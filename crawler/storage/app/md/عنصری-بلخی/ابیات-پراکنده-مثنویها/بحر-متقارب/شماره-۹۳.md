---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>درو آب چشمه ، در او آب جوی</p></div>
<div class="m2"><p>که رنجه نبودی درو آب جوی</p></div></div>