---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>بآیین یکی شهر شامس بنام </p></div>
<div class="m2"><p>یکی شهریار اندرو شادکام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلقراط نام از در مهتری</p></div>
<div class="m2"><p>هم از تخم آقوس بن مشتری</p></div></div>