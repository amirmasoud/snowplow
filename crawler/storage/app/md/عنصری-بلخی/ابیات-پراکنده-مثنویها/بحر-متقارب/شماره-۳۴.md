---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>که فرخ منوس آن شه دادگر</p></div>
<div class="m2"><p>که بد پادشاه جهان سریسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا ماند بیچاره از تاج و تخت</p></div>
<div class="m2"><p>بدرویشی افتاد و شد شور بخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر تخت بختش برآمد بماه</p></div>
<div class="m2"><p>دگر باره شد شاه و بگرفت گاه</p></div></div>