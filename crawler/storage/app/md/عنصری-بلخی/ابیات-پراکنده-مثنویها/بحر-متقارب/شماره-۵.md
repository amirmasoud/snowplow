---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>بود مرد آرمده در بند سخت</p></div>
<div class="m2"><p>چو جنبنده گردد شود نیکبخت</p></div></div>