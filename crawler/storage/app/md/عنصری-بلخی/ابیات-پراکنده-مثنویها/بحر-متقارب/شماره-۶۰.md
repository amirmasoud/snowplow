---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>بتندید عذرا چو مردان جنگ</p></div>
<div class="m2"><p>ترنجید بر بارگی تنگ تنگ</p></div></div>