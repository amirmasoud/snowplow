---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>همی گفت و پیچید بر خشک خاک</p></div>
<div class="m2"><p>ز خون دلش خاک همرنگ لاک</p></div></div>