---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>فرو کوفتند آن بتان را بگزر</p></div>
<div class="m2"><p>نه شان رنگ ماند و نه فرّ و نه برز</p></div></div>