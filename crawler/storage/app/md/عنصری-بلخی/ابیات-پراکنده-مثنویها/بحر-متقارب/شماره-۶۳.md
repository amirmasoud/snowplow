---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>بزرینه جام اندرون لعل مل</p></div>
<div class="m2"><p>فروزنده چون لاله بر زرد گل</p></div></div>