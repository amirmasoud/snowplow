---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>چو روزی که دارد به خاور گریغ</p></div>
<div class="m2"><p>هم از باختر بر زند باز تیغ</p></div></div>