---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>بدل گفت اگر جنگجوئی کنم</p></div>
<div class="m2"><p>بپیکار او سرخ رویی کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگریند مر دوده و میهنم</p></div>
<div class="m2"><p>که بی سر ببینند خسته تنم</p></div></div>