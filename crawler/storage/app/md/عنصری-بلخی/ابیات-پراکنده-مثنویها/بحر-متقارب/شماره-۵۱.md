---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>کزو بتکده گشت هامون چو کف</p></div>
<div class="m2"><p>بآتش همه سوخته همچو خف</p></div></div>