---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>کف یوز پر مغز آهو بره</p></div>
<div class="m2"><p>همه چنگ شاهین دل گودره</p></div></div>