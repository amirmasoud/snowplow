---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>تو شیری و شیران بکردار غرم</p></div>
<div class="m2"><p>برو تا رهانی دلم را ز گرم</p></div></div>