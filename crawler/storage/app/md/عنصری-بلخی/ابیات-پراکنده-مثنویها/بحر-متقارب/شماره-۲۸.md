---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>سزد ار چه او نیز تکبر کند</p></div>
<div class="m2"><p>که شه نیکویی با کسندر کند</p></div></div>