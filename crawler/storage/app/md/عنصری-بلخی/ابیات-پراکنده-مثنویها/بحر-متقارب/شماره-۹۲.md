---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ز جوی خورا به چه کمتر بگوی</p></div>
<div class="m2"><p>که بسیار گردد بیکبار اوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیابان از آن آب دریا شود</p></div>
<div class="m2"><p>که ابر از بخارش ببالا شود</p></div></div>