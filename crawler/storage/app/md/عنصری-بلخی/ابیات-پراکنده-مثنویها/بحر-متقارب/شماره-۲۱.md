---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ز بس کینه جوی و دژ آهنگ بود</p></div>
<div class="m2"><p>فراخای گیتی برو تنگ بود</p></div></div>