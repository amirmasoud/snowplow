---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>چو شب رفت و بر دشت پستی گرفت</p></div>
<div class="m2"><p>هوا چون مغ آتش پرستی گرفت</p></div></div>