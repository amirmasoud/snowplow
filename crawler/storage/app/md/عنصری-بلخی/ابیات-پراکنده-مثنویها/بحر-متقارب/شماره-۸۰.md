---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>جهان خیره ماند ز فرهنگ او</p></div>
<div class="m2"><p>از آن برز و بالا و اورنگ او</p></div></div>