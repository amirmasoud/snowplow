---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>لب بخت پیروز را خنده ای</p></div>
<div class="m2"><p>مرا نیز مرو ای فرخنده ای</p></div></div>