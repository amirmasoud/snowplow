---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>سلیسون شه فرّخ اخترش بود</p></div>
<div class="m2"><p>فلقراط شه را برادرش بود</p></div></div>