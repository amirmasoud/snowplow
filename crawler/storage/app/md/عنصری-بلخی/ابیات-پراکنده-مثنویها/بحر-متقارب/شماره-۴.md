---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>دل دمخسینوس شد ناشکیب</p></div>
<div class="m2"><p>کخ در کار عذار چه سازد فریب</p></div></div>