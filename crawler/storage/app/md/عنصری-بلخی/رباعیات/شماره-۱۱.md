---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>آن زلف که او به بوی مرزَنگوش است</p></div>
<div class="m2"><p>گه بر جَبَه است و گه به زیر گوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین باز عجبتر آن لب خاموش است</p></div>
<div class="m2"><p>زو شهر و جهان به بانگ نوشانوش است</p></div></div>