---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>بر آتش هجر عمری ار بنشینم</p></div>
<div class="m2"><p>خاک در تو همی بدل بگزینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باد همه نسیم زلفت بویم</p></div>
<div class="m2"><p>در آب همه خیال رویت بینم</p></div></div>