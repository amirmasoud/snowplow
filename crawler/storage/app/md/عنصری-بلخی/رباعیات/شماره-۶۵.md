---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>خوبی ز رخ تو بر گرفته است پری</p></div>
<div class="m2"><p>رفتن ز تو آموخت مگر کبک دری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان شده را بمردگان باز بری</p></div>
<div class="m2"><p>گوئی که دم پیمبر بی پدری</p></div></div>