---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>چون نار رخی ز نور پر مایه که دید؟</p></div>
<div class="m2"><p>گسترده به روز بر، ز شب سایه که دید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر توبه از گناه پیرایه که دید؟</p></div>
<div class="m2"><p>ایمان و نفاق هر دو همسایه که دید؟</p></div></div>