---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>آن لب نمزم گرچه مرا آن سازد</p></div>
<div class="m2"><p>زیرا که شکر چون بمزی بگدازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم ز غمانش زرگری آغازد</p></div>
<div class="m2"><p>تا بگدازد عقیق و بر زر یازد</p></div></div>