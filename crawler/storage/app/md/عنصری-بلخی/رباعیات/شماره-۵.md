---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ابروت به زه کرده کمان آمد راست</p></div>
<div class="m2"><p>مژگانت چو تیر بر کمان آمد راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را ز تو دلبری گمان آمد راست</p></div>
<div class="m2"><p>ای دوست ترا پیشه همان آمد راست</p></div></div>