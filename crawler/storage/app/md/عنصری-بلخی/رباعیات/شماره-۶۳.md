---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>رخ پاکتر از ضمیر صادق داری</p></div>
<div class="m2"><p>زلفین سیه چون دل فاسق داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خویشتنم بدین دو عاشق داری</p></div>
<div class="m2"><p>مؤمن سخن و وفا منافق داری</p></div></div>