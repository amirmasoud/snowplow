---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>ای دل ز وصال تو نشانی دارم</p></div>
<div class="m2"><p>وی جان ز فراق تو امانی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره تنم همه جهان داشت بتو</p></div>
<div class="m2"><p>و اکنون بهز ار حیله جانی دارم</p></div></div>