---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>کی عیب سر زلف بت از کاستن است</p></div>
<div class="m2"><p>چه جای بغم نشستن و خاستن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز طرب و نشاط و می خواستن است</p></div>
<div class="m2"><p>کاراستن سرو ز پیراستن است</p></div></div>