---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>بر چهرۀ خوبت آفرین کرده کسی</p></div>
<div class="m2"><p>کس با تو شود ازین جهان دسترسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر میزنم از آتش عشقت نفسی</p></div>
<div class="m2"><p>تا سوخته در جهان نمانند بسی</p></div></div>