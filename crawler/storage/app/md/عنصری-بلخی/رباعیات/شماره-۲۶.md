---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>تا در دو جهان قضای معبود بود</p></div>
<div class="m2"><p>تا خلق جهان و چرخ موجود بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ملک بود بدست محمود بود</p></div>
<div class="m2"><p>ور سعد بود بدست مسعود بود</p></div></div>