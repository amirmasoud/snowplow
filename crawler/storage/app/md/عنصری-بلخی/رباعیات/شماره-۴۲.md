---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>ای دل چو بغمهای جهان درمانم</p></div>
<div class="m2"><p>از دیده سرشکهای رنگین رانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را چه دهم عشوه یقین میدانم</p></div>
<div class="m2"><p>کاندر سر دل بآخر جانم</p></div></div>