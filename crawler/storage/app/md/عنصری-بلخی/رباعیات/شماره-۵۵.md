---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>با روز رخ تو گرچه ای دوست چو ماه</p></div>
<div class="m2"><p>از روز و شب جهان نبودم آگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمود چو چشم بد فروبست آن ماه</p></div>
<div class="m2"><p>شبهای فراق تو مرا روز سیاه</p></div></div>