---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>من گفت نیارم که تو ماهی صنما</p></div>
<div class="m2"><p>روشن بتو گشت ماه و ماهی صنما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من شاه جهان مرا تو شاهی صنما</p></div>
<div class="m2"><p>فرمانت روا بهر چه خواهی صنما</p></div></div>