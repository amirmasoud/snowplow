---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>از بوسه تو مرده با روان تانی کرد</p></div>
<div class="m2"><p>وز چهره دل پیر جوان تانی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ گاه گل و گه ارغوان تانی کرد</p></div>
<div class="m2"><p>وز غمزه فریب جاودان تانی کرد</p></div></div>