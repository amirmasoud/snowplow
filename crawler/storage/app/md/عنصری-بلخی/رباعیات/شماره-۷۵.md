---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ای رخ نه رخی ، که لالۀ سیرابی</p></div>
<div class="m2"><p>ای لب نه لبی ، بنوش در عنابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای غمزه بجادوئی مگر قصابی</p></div>
<div class="m2"><p>تو غمزه نه ای که نرگس پر خوابی</p></div></div>