---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>منگر تو بدو تا نشود دلت از راه</p></div>
<div class="m2"><p>ور سیر شدی ز دل برو کن تو نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور درد نخواهی تو برو عشق مخواه</p></div>
<div class="m2"><p>عشق ار خواهی مکن دل از درد تباه</p></div></div>