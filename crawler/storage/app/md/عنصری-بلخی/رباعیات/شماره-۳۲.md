---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>چون بگشائی بخنده آن چشمۀ نوش</p></div>
<div class="m2"><p>شکر بفغان آید و پروین بخروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز چشم بدش در آن دو زلفین بپوش</p></div>
<div class="m2"><p>کو غارت کرد کلبۀ مشک فروش</p></div></div>