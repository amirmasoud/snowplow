---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>زان ملک را نظام و ازین عهد را بقا</p></div>
<div class="m2"><p>زان دوستان بفخر و ازین دشمنان شمان</p></div></div>