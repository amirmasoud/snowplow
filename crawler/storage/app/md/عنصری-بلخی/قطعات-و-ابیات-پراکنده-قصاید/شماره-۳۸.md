---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>دل و دامن تنور کرد و غدیر</p></div>
<div class="m2"><p>سرو و لاله کناغ کرد و زریر</p></div></div>