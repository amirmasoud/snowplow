---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>مگر ز چشمۀ خورشید روز دولت تو</p></div>
<div class="m2"><p>ندید خواهد تا روزگار حشر زوال</p></div></div>