---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>دلبر صنمی دارم شکر لب و مرمر بر</p></div>
<div class="m2"><p>مرمر ز برش خیزد شکر ز لبش بارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنبر به خم زلفش عبهر به دل چشمش</p></div>
<div class="m2"><p>خنجر سر مژگانش عرعر به قدش ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>.......................................</p></div>
<div class="m2"><p>بتگر نکند چون او پیکر چو پری دارد</p></div></div>