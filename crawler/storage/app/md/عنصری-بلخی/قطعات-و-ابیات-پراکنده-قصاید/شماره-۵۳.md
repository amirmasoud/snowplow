---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>رکاب عالی بگذشت و لشکر از پس او</p></div>
<div class="m2"><p>چنان کجا برود فوج فوج موج بحار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فزو نشان همه کم کرد و رویشان همه پشت</p></div>
<div class="m2"><p>نشاطشان همه غم کرد و فخرشان همه عار</p></div></div>