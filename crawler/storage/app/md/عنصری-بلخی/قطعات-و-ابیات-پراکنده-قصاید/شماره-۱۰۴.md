---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>بکرد با دل تو ای ملک وفا بیعت</p></div>
<div class="m2"><p>بکرد با سیر پاک تو هنر پیمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز طبع و دست تو گیرد همی سخا حجت</p></div>
<div class="m2"><p>ز خاطر تو نماید همی خطر برهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بطاعت تو نیارد همی قضا غفلت</p></div>
<div class="m2"><p>بخدمت تو نجوید همی قدر عصیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنور مدح تو گیرد همی ذکا زینت</p></div>
<div class="m2"><p>بآفرین تو گیرد همی فکر سامان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخیر مال ، ترا هست آشنا دولت</p></div>
<div class="m2"><p>بسعد کام ، ترا هست راهبر دوران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هیبت تو نگردد همی روا فکرت </p></div>
<div class="m2"><p>ز همت تو نیابد همی گذر کیوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سیرت تو برد زینت و بها حکمت</p></div>
<div class="m2"><p>ز عادت تو پذیرد جمال و فر احسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نجست یارد پیش تو اژدها وقعت</p></div>
<div class="m2"><p>نکرد یارد پیش تو شیر نر جولان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تراست بر همه مردان پارسا منت</p></div>
<div class="m2"><p>تراست بر همه گردان نامور فرمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شدست کام تو بر کامۀ عطا صورت</p></div>
<div class="m2"><p>شدست نام تو بر نامۀ ظفر عنوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شدست بر کرم و فضل تو گوا فکرت</p></div>
<div class="m2"><p>نکرد از خرد و فضل مختصر یزدان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز تو نخواهد شد جاودان جدا ملکت</p></div>
<div class="m2"><p>ز تو نخواهد شد هیچ زاستر ایمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود زمین عدوی ترا گیا شدّت</p></div>
<div class="m2"><p>بود درخت حسود ترا ثمر خذلان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نکرد طبع تو هرگز بناسزا رغبت</p></div>
<div class="m2"><p>نکرد رای تو هرگز ببد سیر فرمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>براستی برد از تو همی ضیا ملت</p></div>
<div class="m2"><p>بفرّخی برد از تو همی اثر ایمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی کنند بنیکی ترا دعا امت</p></div>
<div class="m2"><p>همی برند بشادی ز تو قبا فتیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دهد بصحبت اعدای تو رضا محنت</p></div>
<div class="m2"><p>کند بجان بداندیش تو نظر احزان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی بجوید مهر ترا هوا رحمت</p></div>
<div class="m2"><p>همی ببندد امر ترا کمر کیوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرفت با طرب سال تو بلاقلت</p></div>
<div class="m2"><p>گرفت با شرف ماه تو حذر احسان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنانکه سال نو آورد مر ترا نزهت</p></div>
<div class="m2"><p>خجسته بادت امسال سر بسر کیهان</p></div></div>