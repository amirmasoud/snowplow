---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>گرفت از ماه فروردین جهان فر</p></div>
<div class="m2"><p>چو فردوس برین شد هفت کشور</p></div></div>