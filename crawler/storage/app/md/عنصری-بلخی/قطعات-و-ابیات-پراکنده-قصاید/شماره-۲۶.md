---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>بگرد ماه بر از غالیه حصار که کرد</p></div>
<div class="m2"><p>بروی روز بر از تیره شب نگار که کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود یار بطبع و بجنس ظلمت و نور</p></div>
<div class="m2"><p>بروی خوب تو این هر دو چیز یار که کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا که کرد بتا از بهار خانه برون</p></div>
<div class="m2"><p>جهان بروی تو بر جان من بهار که کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بماه مانی آنگه که تو سوار شوی</p></div>
<div class="m2"><p>چگونه ای عجبی ماه را سوار که کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز عشق تو پر ناز گشت جان و دلم</p></div>
<div class="m2"><p>مرا بگوی رخانت برنگ نار که کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر استوار نبودی ز دور بر دل من</p></div>
<div class="m2"><p>مرا بمهر تو نزدیک و استوار کرد</p></div></div>