---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ز میغ نزم کزان روز روشن از مه تیر</p></div>
<div class="m2"><p>چنان نمود که تاری شب از مه آبان</p></div></div>