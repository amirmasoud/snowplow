---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>بلبل همی سرآید چون بارید</p></div>
<div class="m2"><p>قالوس و قفل رومی و جالینوس</p></div></div>