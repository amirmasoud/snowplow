---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>روسبی را محتسب داند زدن</p></div>
<div class="m2"><p>شاد باش ای روسبی زن محتسب</p></div></div>