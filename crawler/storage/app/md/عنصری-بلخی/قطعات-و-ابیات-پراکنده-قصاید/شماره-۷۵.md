---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>تو چگونه رهی که دست اجل </p></div>
<div class="m2"><p>بر سر تو همی زند سر پاس</p></div></div>