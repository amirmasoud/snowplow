---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>بسوی خورابه رایت کشید</p></div>
<div class="m2"><p>که بد خانه ای مستقر و مقر</p></div></div>