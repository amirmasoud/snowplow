---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>چون آب ز بالا بگراید سوی پستی</p></div>
<div class="m2"><p>وز پست چو آتش بگراید سوی بالا</p></div></div>