---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>بهر تلی بر از خسته گروهی</p></div>
<div class="m2"><p>بهر غفچی بر از فر خسته پنجاه</p></div></div>