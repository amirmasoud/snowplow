---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>بنام و کنیتت آراسته باد</p></div>
<div class="m2"><p>ستایشگاه شعر و خطبه تا حشر</p></div></div>