---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>دندان و عارض بتم از من ببرد هوش</p></div>
<div class="m2"><p>کاین در نوش طعمست ، آن ماه مشکپوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوشان شده دو زلف بت من بروی بر</p></div>
<div class="m2"><p>جانم بر آتش است از آن آمده بجوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر چهار چیزش دارم چهار چیز</p></div>
<div class="m2"><p>هر شب از آن ببردن دل گشته سخت کوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر سمن بنفشه و اندر صدف گهر</p></div>
<div class="m2"><p>اندر سهیل سنبل و اندر عقیق نوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای زلف او نه زلفی ، وی دو لبش نه لب</p></div>
<div class="m2"><p>رند عبیر سایی و دزد شکر فروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف ار فرو کشد بمیان بر کمر کند</p></div>
<div class="m2"><p>چون دست باز دارد حلقه شود بگوش</p></div></div>