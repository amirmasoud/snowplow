---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ای دریغا کزین منور جای</p></div>
<div class="m2"><p>زیر خاک مغاک باید شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاک ناکرده تن ز گرد گناه</p></div>
<div class="m2"><p>پیش یزدان پاک باید شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چنین خاطری چو آتش و آب</p></div>
<div class="m2"><p>باد پیمود و خاک باید شد</p></div></div>