---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>خدایگانا امشب نشاط ساز بدانک</p></div>
<div class="m2"><p>پدرش ز آهن بودست و مادرش حجرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بصورت شجری و ز خفچه او را برگ</p></div>
<div class="m2"><p>که از عقیق و ز یاقوت بار آن شجرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبانهاش چو شمشیر های زر اندود</p></div>
<div class="m2"><p>کزو به جان خطرست ، ار چه زرّ بی خطرست</p></div></div>