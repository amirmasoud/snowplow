---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>تو گفتی ز اسرار ایشان همی</p></div>
<div class="m2"><p>فرستد بدو آفتاب اسکدار</p></div></div>