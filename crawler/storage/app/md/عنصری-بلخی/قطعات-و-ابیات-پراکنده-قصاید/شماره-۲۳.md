---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بمجلس اندر کان بت مرا شراب دهد</p></div>
<div class="m2"><p>بمن نشاط و ببد خواه من عذاب دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی چنانکه خدایش همه عذاب دهد</p></div>
<div class="m2"><p>یکی چنان که خدایش همه صواب دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان برم که بمن آفتاب خواهد داد</p></div>
<div class="m2"><p>بلی چو ساقی مه باشد آفتاب دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بخوانم آن را کجا که دوزخ اوست</p></div>
<div class="m2"><p>هر آینه گل حمری مرا جواب دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز باد حلقۀ زلفین او بر آن رخسار</p></div>
<div class="m2"><p>همی شتاب کند تا مرا شتاب دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدین جهان چه شناسی عجبتر از خط او</p></div>
<div class="m2"><p>که مشک نیست ولی بوی مشک ناب دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیاه و سبز و قوّی است و ماه و مهرش روی</p></div>
<div class="m2"><p>خرد زهر دو نشانی همی صواب دهد</p></div></div>