---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>ز پالان فزونست ریش رشید</p></div>
<div class="m2"><p>تنیده در آن خانه صد دیو پای</p></div></div>