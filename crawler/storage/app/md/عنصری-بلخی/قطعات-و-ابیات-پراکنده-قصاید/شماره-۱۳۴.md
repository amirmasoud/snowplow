---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>گل سوری بماه اندر شکفته</p></div>
<div class="m2"><p>بر او بر کژدم جرّاره خفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو لب چون دانۀ نارست لیکن</p></div>
<div class="m2"><p>بنوک سوزن اندیشه سفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکوروئی که از فردوس حورا</p></div>
<div class="m2"><p>برو خوبی فرستاده است سفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب تار آشکارا گشته دائم</p></div>
<div class="m2"><p>بزیرش روز رخشنده نهفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بآیین صورتی کاندر جهان کس</p></div>
<div class="m2"><p>نظیر او ندیده ست و نگفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گل گویی شکفته عارضینش</p></div>
<div class="m2"><p>وز او زلفین مشکین گرد رفته</p></div></div>