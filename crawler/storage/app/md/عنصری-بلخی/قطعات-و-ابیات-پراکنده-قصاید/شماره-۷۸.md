---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p> خود فزاید همیشه مهر فروغ</p></div>
<div class="m2"><p>خود نماید همیشه گوهر اخش</p></div></div>