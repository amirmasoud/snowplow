---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>جزوی و کلی از دو برون نیست آنچ هست</p></div>
<div class="m2"><p>جز وی همه تو بخشی و کلی همه خدای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از خدای و از تو همی خواهم این دو چیز</p></div>
<div class="m2"><p>تا او ترا بقا دهد و تو مرا قبای</p></div></div>