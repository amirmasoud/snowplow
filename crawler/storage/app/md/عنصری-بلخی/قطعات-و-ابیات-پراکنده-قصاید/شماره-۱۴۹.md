---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>ای مایۀ طربم و آرام روز و شبم</p></div>
<div class="m2"><p>من خنج تو طلبم تو رنج من طلبی</p></div></div>