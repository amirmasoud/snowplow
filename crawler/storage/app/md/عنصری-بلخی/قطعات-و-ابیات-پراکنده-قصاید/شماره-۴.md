---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>از دولت عشق است به من بر دو موکل</p></div>
<div class="m2"><p>هر دو متقاضی به دو معنی نه به همتا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این وصف دلارام تقاضا کند از من</p></div>
<div class="m2"><p>وان باز کند مدح جهاندار تقاضا</p></div></div>