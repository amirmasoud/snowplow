---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p> بشاهنامه همی خوانده ام که رستم زال</p></div>
<div class="m2"><p>گهی بشد ز ره هفتخوان بمازندر</p></div></div>