---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>حلقهٔ زلفش به گل بر غالیه دارد همی</p></div>
<div class="m2"><p>گل به بوی غالیه سنبل به بار آرد همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست سنبل کان خط مشکین آن ترک منست</p></div>
<div class="m2"><p>دیده چون آنرا ببیند سنبل انگارد همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عذر جانست آن رخ و آن غمزگان آزار دل</p></div>
<div class="m2"><p>آن رخان چو عذر خواهد این دل آزارد همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغبانند آن دو زلفش ، باغ دو رخسار او</p></div>
<div class="m2"><p>آنک آنک باغبان در باغ گل کارد همی</p></div></div>