---
title: >-
    بخش ۲ - پادشاهی هرمز یک سال بود
---
# بخش ۲ - پادشاهی هرمز یک سال بود

<div class="b" id="bn1"><div class="m1"><p>چو هرمز برآمد به تخت پدر</p></div>
<div class="m2"><p>به سر برنهاد آن کیی تاج زر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو پیروز را ویژه گفتی ز خشم</p></div>
<div class="m2"><p>همی آب رشک اندر آمد به چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی شاه هیتال شد ناگهان</p></div>
<div class="m2"><p>ابا لشکر و گنج و چندی مهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چغانی شهی بد فغانیش نام</p></div>
<div class="m2"><p>جهانجوی با لشکر و گنج و کام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغانیش را گفت کای نیک‌خواه</p></div>
<div class="m2"><p>دو فرزند بودیم زیبای گاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدر تاج شاهی به کهتر سپرد</p></div>
<div class="m2"><p>چو بیدادگر بد سپرد و بمرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو لشکر دهی مر مرا گنج هست</p></div>
<div class="m2"><p>سلیح و بزرگی و نیروی دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغانی بدو گفت که آری رواست</p></div>
<div class="m2"><p>جهاندار هم بر پدر پادشاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پیمان سپارم سپاهی تو را</p></div>
<div class="m2"><p>نمایم سوی داد راهی تو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که باشد مرا ترمذ و ویسه گرد</p></div>
<div class="m2"><p>که خون عهد این دارم از یزدگرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو گفت پیروز کری رواست</p></div>
<div class="m2"><p>فزون زان بتو پادشاهی سزاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو داد شمشیرزن سی‌هزار</p></div>
<div class="m2"><p>ز هیتالیان لشکری نامدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپاهی بیاورد پیروزشاه</p></div>
<div class="m2"><p>که از گرد تاریک شد چرخ ماه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برآویخت با هرمز شهریار</p></div>
<div class="m2"><p>فراوان ببودستشان کارزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرانجام هرمز گرفتار شد</p></div>
<div class="m2"><p>همه تاجها پیش او خوار شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو پیروز روی برادر بدید</p></div>
<div class="m2"><p>دلش مهر پیوند او برگزید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بفرمود تا بارگی برنشست</p></div>
<div class="m2"><p>بشد تیز و ببسود رویش بدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرستاد بازش بایوان خویش</p></div>
<div class="m2"><p>بدو خوانده بد عهد و پیمان خویش</p></div></div>