---
title: >-
    بخش ۲۲
---
# بخش ۲۲

<div class="b" id="bn1"><div class="m1"><p>چو آمد سر ماه هنگام جنگ</p></div>
<div class="m2"><p>ز پیمان بگشتند و از نام و ننگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خروشی برآمد ز هر دو سپاه</p></div>
<div class="m2"><p>برفتند یکسر سوی رزمگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس ناله بوق و هندی درای</p></div>
<div class="m2"><p>همی آسمان اندر آمد ز جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم از یال اسپان و دست و عنان</p></div>
<div class="m2"><p>ز گوپال و تیغ و کمان و سنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گفتی جهان دام نر اژدهاست</p></div>
<div class="m2"><p>وگر آسمان بر زمین گشت راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبد پشه را روزگار گذر</p></div>
<div class="m2"><p>ز بس گرز و تیغ و سنان و سپر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی میمنه گیو گودرز بود</p></div>
<div class="m2"><p>رد و موبد و مهتر مرز بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی میسره اشکش تیزچنگ</p></div>
<div class="m2"><p>که دریای خون راند هنگام جنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یلان با فریبرز کاوس شاه</p></div>
<div class="m2"><p>درفش از پس پشت در قلبگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریبرز با لشکر خویش گفت</p></div>
<div class="m2"><p>که ما را هنرها شد اندر نهفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک امروز چون شیر جنگ آوریم</p></div>
<div class="m2"><p>جهان بر بداندیش تنگ آوریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کزین ننگ تا جاودان بر سپاه</p></div>
<div class="m2"><p>بخندند همی گرز و رومی کلاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی تیرباران بکردند سخت</p></div>
<div class="m2"><p>چو باد خزانی که ریزد درخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو گفتی هوا پر کرگس شدست</p></div>
<div class="m2"><p>زمین از پی پیل پامس شدست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبد بر هوا مرغ را جایگاه</p></div>
<div class="m2"><p>ز تیر و ز گرز و ز گرد سپاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درفشیدن تیغ الماس گون</p></div>
<div class="m2"><p>بکردار آتش بگرد اندرون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو گفتی زمین روی زنگی شدست</p></div>
<div class="m2"><p>ستاره دل پیل جنگی شدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بس نیزه و گرز و شمشیر تیز</p></div>
<div class="m2"><p>برآمد همی از جهان رستخیز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز قلب سپه گیو شد پیش صف</p></div>
<div class="m2"><p>خروشان و بر لب برآورده کف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ابا نامداران گودرزیان</p></div>
<div class="m2"><p>کزیشان بدی راه سود و زیان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بتیغ و بنیزه برآویختند</p></div>
<div class="m2"><p>همی ز آهن آتش فرو ریختند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو شد رزم گودرز و پیران درشت</p></div>
<div class="m2"><p>چو نهصد تن از تخم پیران بکشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو دیدند لهاک و فرشیدورد</p></div>
<div class="m2"><p>کزان لشکر گشن برخاست گرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی حمله بردند برسوی گیو</p></div>
<div class="m2"><p>بران گرزداران و شیران نیو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببارید تیر از کمان سران</p></div>
<div class="m2"><p>بران نامداران جوشن‌وران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان شد که کس روی کشور ندید</p></div>
<div class="m2"><p>ز بس کشتگان شد زمین ناپدید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی پشت بر دیگری برنگاشت</p></div>
<div class="m2"><p>نه بگذاشت آن جایگه را که داشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین گفت هومان به فرشیدورد</p></div>
<div class="m2"><p>که با قلبگه جست باید نبرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فریبرز باید کزان قلبگاه</p></div>
<div class="m2"><p>گریزان بیاید ز پشت سپاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پس آسان بود جنگ با میمنه</p></div>
<div class="m2"><p>بچنگ آید آن رزمگاه و بنه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برفتند پس تا بقلب سپاه</p></div>
<div class="m2"><p>بجنگ فریبرز کاوس شاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز هومان گریزان بشد پهلوان</p></div>
<div class="m2"><p>شکست اندر آمد برزم گوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدادند گردنکشان جای خویش</p></div>
<div class="m2"><p>نبودند گستاخ با رای خویش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکایک بدشمن سپردند جای</p></div>
<div class="m2"><p>ز گردان ایران نبد کس بپای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بماندند بر جای کوس و درفش</p></div>
<div class="m2"><p>ز پیکارشان دیده‌ها شد بنفش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دلیران بدشمن نمودند پشت</p></div>
<div class="m2"><p>ازان کارزار انده آمد بمشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نگون گشته کوس و درفش و سنان</p></div>
<div class="m2"><p>نبود ایچ پیدا رکیب از عنان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو دشمن ز هر سو بانبوه شد</p></div>
<div class="m2"><p>فریبرز بر دامن کوه شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برفتند ز ایرانیان هرک زیست</p></div>
<div class="m2"><p>بران زندگانی بباید گریست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همی بود بر جای گودرز و گیو</p></div>
<div class="m2"><p>ز لشکر بسی نامبردار نیو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو گودرز کشواد بر قلبگاه</p></div>
<div class="m2"><p>درفش فریبرز کاوس شاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ندید و یلان سپه را ندید</p></div>
<div class="m2"><p>بکردار آتش دلش بردمید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عنان کرد پیچان براه گریز</p></div>
<div class="m2"><p>برآمد ز گودرزیان رستخیز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو گفت گیو ای سپهدار پیر</p></div>
<div class="m2"><p>بسی دیده‌ای گرز و گوپال و تیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر تو ز پیران بخواهی گریخت</p></div>
<div class="m2"><p>بباید بسر بر مرا خاک ریخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نماند کسی زنده اندر جهان</p></div>
<div class="m2"><p>دلیران و کارآزموده مهان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز مردن مرا و ترا چاره نیست</p></div>
<div class="m2"><p>درنگی تر از مرگ پتیاره نیست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو پیش آمد این روزگار درشت</p></div>
<div class="m2"><p>ترا روی بینند بهتر که پشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بپیچیم زین جایگه سوی جنگ</p></div>
<div class="m2"><p>نیاریم بر خاک کشواد ننگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز دانا تو نشنیدی آن داستان</p></div>
<div class="m2"><p>که برگوید از گفتهٔ باستان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که گر دو برادر نهد پشت پشت</p></div>
<div class="m2"><p>تن کوه را سنگ ماند بمشت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو باشی و هفتاد جنگی پسر</p></div>
<div class="m2"><p>ز دوده ستوده بسی نامور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بخنجر دل دشمنان بشکنیم</p></div>
<div class="m2"><p>وگر کوه باشد ز بن برکنیم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو گودرز بشنید گفتار گیو</p></div>
<div class="m2"><p>بدید آن سر و ترگ بیدار نیو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پشیمان شد از دانش و رای خویش</p></div>
<div class="m2"><p>بیفشارد بر جایگه پای خویش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گرازه برون آمد و گستهم</p></div>
<div class="m2"><p>ابا برته و زنگهٔ یل بهم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بخوردند سوگندهای گران</p></div>
<div class="m2"><p>که پیمان شکستن نبود اندران</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کزین رزمگه برنتابیم روی</p></div>
<div class="m2"><p>گر از گرز خون اندر آید بجوی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وزان جایگه ران بیفشاردند</p></div>
<div class="m2"><p>برزم اندرون گرز بگذاردند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز هر سو سپه بیکران کشته شد</p></div>
<div class="m2"><p>زمانه همی بر بدی گشته شد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به بیژن چنین گفت گودرز پیر</p></div>
<div class="m2"><p>کز ایدر برو زود برسان تیر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بسوی فریبرز برکش عنان</p></div>
<div class="m2"><p>بپیش من آر اختر کاویان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مگر خود فریبرز با آن درفش</p></div>
<div class="m2"><p>بیاید کند روی دشمن بنفش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو بشنید بیژن برانگیخت اسپ</p></div>
<div class="m2"><p>بیامد بکردار آذرگشسپ</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بنزد فریبرز و با او بگفت</p></div>
<div class="m2"><p>که ایدر چه داری سپه در نهفت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عنان را چو گردان یکی برگرای</p></div>
<div class="m2"><p>برین کوه سر بر فزون زین مپای</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اگر تو نیایی مرا ده درفش</p></div>
<div class="m2"><p>سواران و این تیغهای بنفش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو بیژن سخن با فریبرز گفت</p></div>
<div class="m2"><p>نکرد او خرد با دل خویش جفت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یکی بانگ برزد به بیژن که رو</p></div>
<div class="m2"><p>که در کار تندی و در جنگ نو</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مرا شاه داد این درفش و سپاه</p></div>
<div class="m2"><p>همین پهلوانی و تخت و کلاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>درفش از در بیژن گیو نیست</p></div>
<div class="m2"><p>نه اندر جهان سربسر نیو نیست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>یکی تیغ بگرفت بیژن بنفش</p></div>
<div class="m2"><p>بزد ناگهان بر میان درفش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بدو نیمه کرد اختر کاویان</p></div>
<div class="m2"><p>یکی نیمه برداشت گرد از میان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بیامد که آرد بنزد سپاه</p></div>
<div class="m2"><p>چو ترکان بدیدند اختر براه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یکی شیردل لشکری جنگجوی</p></div>
<div class="m2"><p>همه سوی بیژن نهادند روی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کشیدند گوپال و تیغ بنفش</p></div>
<div class="m2"><p>به پیکار آن کاویانی درفش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چنین گفت هومان که آن اخترست</p></div>
<div class="m2"><p>که نیروی ایران بدو اندر است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>درفش بنفش ار بچنگ آوریم</p></div>
<div class="m2"><p>جهان جمله بر شاه تنگ آوریم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>کمان را بزه کرد بیژن چو گرد</p></div>
<div class="m2"><p>بریشان یکی تیرباران بکرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سپه یکسر از تیر او دور شد</p></div>
<div class="m2"><p>همی گرگ درنده را سور شد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بگفتند با گیو و با گستهم</p></div>
<div class="m2"><p>سواران که بودند با او بهم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>که مان رفت باید بتوران سپاه</p></div>
<div class="m2"><p>ربودن ازیشان همی تاج و گاه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز گردان ایران دلاور سران</p></div>
<div class="m2"><p>برفتند بسیار نیزه‌وران</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بکشتند زیشان فراوان سوار</p></div>
<div class="m2"><p>بیامد ز ره بیژن نامدار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سپاه اندر آمد بگرد درفش</p></div>
<div class="m2"><p>هوا شد ز گرد سواران بنفش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دگر باره از جای برخاستند</p></div>
<div class="m2"><p>بران دشت رزمی نو آراستند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به پیش سپه کشته شد ریونیز</p></div>
<div class="m2"><p>که کاوس را بد چو جان عزیز</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>یکی تاجور شاه کهتر پسر</p></div>
<div class="m2"><p>نیاز فریبرز و جان پدر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سر و تاج او اندر آمد بخاک</p></div>
<div class="m2"><p>بسی نامور جامه کردند چاک</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ازان پس خروشی برآورد گیو</p></div>
<div class="m2"><p>که ای نامداران و گردان نیو</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چنویی نبود اندرین رزمگاه</p></div>
<div class="m2"><p>جوان و سرافراز و فرزند شاه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نبیره جهاندار کاوس پیر</p></div>
<div class="m2"><p>سه تن کشته شد زار بر خیره خیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>فرود سیاوش چون ریونیز</p></div>
<div class="m2"><p>بگیتی فزون زین شگفتی چه چیز</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگر تاج آن نارسیده جوان</p></div>
<div class="m2"><p>بدشمن رسد شرم دارد روان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>اگر من بجنبم ازین رزمگاه</p></div>
<div class="m2"><p>شکست اندر آید بایران سپاه</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نباید که آن افسر شهریار</p></div>
<div class="m2"><p>بترکان رسد در صف کارزار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>فزاید بر این ننگها ننگ نیز</p></div>
<div class="m2"><p>ازین افسر و کشتن ریو نیز</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چنان بد که بشنید آواز گیو</p></div>
<div class="m2"><p>سپهبد سرافراز پیران نیو</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>برامد بنوی یکی کارزار</p></div>
<div class="m2"><p>ز لشکر بران افسر نامدار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>فراوان ز هر سو سپه کشته شد</p></div>
<div class="m2"><p>سربخت گردنکشان گشته شد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>برآویخت چون شیر بهرام گرد</p></div>
<div class="m2"><p>بنیزه بریشان یکی حمله برد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بنوک سنان تاج را برگرفت</p></div>
<div class="m2"><p>دو لشکر بدو مانده اندر شگفت</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>همی بود زان گونه تا تیره گشت</p></div>
<div class="m2"><p>همی دیده از تیرگی خیره گشت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چنین هر زمانی برآشوفتند</p></div>
<div class="m2"><p>همی بر سر یکدگر کوفتند</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ز گودرزیان هشت تن زنده بود</p></div>
<div class="m2"><p>بران رزمگه دیگر افگنده بود</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>هم از تخمهٔ گیو چون بیست و پنج</p></div>
<div class="m2"><p>که بودند زیبای دیهیم و گنج</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>هم از تخم کاوس هفتاد مرد</p></div>
<div class="m2"><p>سواران و شیران روز نبرد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>جز از ریونیز آن سر تاجدار</p></div>
<div class="m2"><p>سزد گر نیاید کسی در شمار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو سیصد تن از تخم افراسیاب</p></div>
<div class="m2"><p>کجا بختشان اندر آمد بخواب</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز خویشان پیران چو نهصد سوار</p></div>
<div class="m2"><p>کم آمد برین روز در کارزار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>همان دست پیران بد و روز اوی</p></div>
<div class="m2"><p>ازان اختر گیتی‌افروز اوی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>نبد روز پیکار ایرانیان</p></div>
<div class="m2"><p>ازان جنگ جستن سرآمد زمان</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>از آوردگه روی برگاشتند</p></div>
<div class="m2"><p>همی خستگان خوار بگذاشتند</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بدانگه کجا بخت برگشته بود</p></div>
<div class="m2"><p>دمان بارهٔ گستهم کشته بود</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>پیاده همی رفت نیزه بدست</p></div>
<div class="m2"><p>ابا جوشن و خود برسان مست</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو بیژن بگستهم نزدیک شد</p></div>
<div class="m2"><p>شب آمد همی روز تاریک شد</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>بدو گفت هین برنشین از پسم</p></div>
<div class="m2"><p>گرامی‌تر از تو نباشد کسم</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>نشستند هر دو بران بارگی</p></div>
<div class="m2"><p>چو خورشید شد تیره یکبارگی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>همه سوی آن دامن کوهسار</p></div>
<div class="m2"><p>گریزان برفتند برگشته کار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>سواران ترکان همه شاددل</p></div>
<div class="m2"><p>ز رنج و ز غم گشته آزاددل</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بلشکرگه خویش بازآمدند</p></div>
<div class="m2"><p>گرازنده و بزم ساز آمدند</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>ز گردان ایران برآمد خروش</p></div>
<div class="m2"><p>همی کر شد از نالهٔ کوس گوش</p></div></div>