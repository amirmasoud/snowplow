---
title: >-
    بخش ۱۴
---
# بخش ۱۴

<div class="b" id="bn1"><div class="m1"><p>چو خورشید تابنده شد ناپدید</p></div>
<div class="m2"><p>شب تیره بر چرخ لشکر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیران دژدار مردی هزار</p></div>
<div class="m2"><p>ز سوی کلات اندر آمد سوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دژ ببستند زین روی تنگ</p></div>
<div class="m2"><p>خروش جرس خاست و آوای زنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جریره بتخت گرامی بخفت</p></div>
<div class="m2"><p>شب تیره با درد و غم بود جفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخواب آتشی دید کز دژ بلند</p></div>
<div class="m2"><p>برافروختی پیش آن ارجمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سراسر سپد کوه بفروختی</p></div>
<div class="m2"><p>پرستنده و دژ همی سوختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلش گشت پر درد و بیدار گشت</p></div>
<div class="m2"><p>روانش پر از درد و تیمار گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بباره برآمد جهان بنگرید</p></div>
<div class="m2"><p>همه کوه پرجوشن و نیزه دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخش گشت پرخون و دل پر ز دود</p></div>
<div class="m2"><p>بیامد به بالین فرخ فرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفت بیدار گرد ای پسر</p></div>
<div class="m2"><p>که ما را بد آمد ز اختر بسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سراسر همه کوه پر دشمنست</p></div>
<div class="m2"><p>در دژ پر از نیزه و جوشنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمادر چنین گفت جنگی فرود</p></div>
<div class="m2"><p>که از غم چه داری دلت پر ز دود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا گر زمانه شدست اسپری</p></div>
<div class="m2"><p>زمانه ز بخشش فزون نشمری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بروز جوانی پدر کشته شد</p></div>
<div class="m2"><p>مرا روز چون روز او گشته شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدست گروی آمد او را زمان</p></div>
<div class="m2"><p>سوی جان من بیژن آمد دمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بکوشم نمیرم مگر غرم‌وار</p></div>
<div class="m2"><p>نخواهم ز ایرانیان زینهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپه را همه ترگ و جوشن بداد</p></div>
<div class="m2"><p>یکی ترگ رومی بسر برنهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میانرا بخفتان رومی ببست</p></div>
<div class="m2"><p>بیامد کمان کیانی بدست</p></div></div>