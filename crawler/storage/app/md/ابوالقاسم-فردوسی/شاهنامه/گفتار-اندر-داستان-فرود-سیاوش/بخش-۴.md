---
title: >-
    بخش ۴
---
# بخش ۴

<div class="b" id="bn1"><div class="m1"><p>پس آگاهی آمد بنزد فرود</p></div>
<div class="m2"><p>که شد روی خورشید تابان کبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نعل ستوران وز پای پیل</p></div>
<div class="m2"><p>جهان شد بکردار دریای نیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بشنید ناکار دیده جوان</p></div>
<div class="m2"><p>دلش گشت پر درد و تیره روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرمود تا هرچ بودش یله</p></div>
<div class="m2"><p>هیونان وز گوسفندان گله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فسیله ببند اندر آرند نیز</p></div>
<div class="m2"><p>نماند ایچ بر کوه و بر دشت چیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه پاک سوی سپد کوه برد</p></div>
<div class="m2"><p>ببند اندرون سوی انبوه برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جریره زنی بود مام فرود</p></div>
<div class="m2"><p>ز بهر سیاوش دلش پر ز دود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر مادر آمد فرود جوان</p></div>
<div class="m2"><p>بدو گفت کای مام روشن‌روان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ایران سپاه آمد و پیل و کوس</p></div>
<div class="m2"><p>بپیش سپه در سرافراز طوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه گویی چه باید کنون ساختن</p></div>
<div class="m2"><p>نباید که آرد یکی تاختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جریره بدو گفت کای رزمساز</p></div>
<div class="m2"><p>بدین روز هرگز مبادت نیاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بایران برادرت شاه نوست</p></div>
<div class="m2"><p>جهاندار و بیدار کیخسروست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترا نیک داند به نام و گهر</p></div>
<div class="m2"><p>ز هم خون وز مهرهٔ یک پدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برادرت گر کینه جوید همی</p></div>
<div class="m2"><p>روان سیاوش بشوید همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر او کینه جوید همی از نیا</p></div>
<div class="m2"><p>ترا کینه زیباتر و کیمیا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برت را بخفتان رومی بپوش</p></div>
<div class="m2"><p>برو دل پر از جوش و سر پر خروش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به پیش سپاه برادر برو</p></div>
<div class="m2"><p>تو کینخواه نو باش و او شاه نو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که زیبد کز این غم بنالد پلنگ</p></div>
<div class="m2"><p>ز دریا خروشان برآید نهنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وگر مرغ با ماهیان اندر آب</p></div>
<div class="m2"><p>بخوانند نفرین به افراسیاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که اندر جهان چون سیاوش سوار</p></div>
<div class="m2"><p>نبندد کمر نیز یک نامدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به گردی و مردی و جنگ و نژاد</p></div>
<div class="m2"><p>باورنگ و فرهنگ و سنگ و بداد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو داد پیران مرا از نخست</p></div>
<div class="m2"><p>وگر نه ز ترکان همی زن نجست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نژاد تو از مادر و از پدر</p></div>
<div class="m2"><p>همه تاجدار و هم نامور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو پور چنان نامور مهتری</p></div>
<div class="m2"><p>ز تخم کیانی و کی‌منظری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کمربست باید بکین پدر</p></div>
<div class="m2"><p>بجای آوریدن نژاد و گهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنین گفت ازان پس بمادر فرود</p></div>
<div class="m2"><p>کز ایران سخن با که باید سرود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که باید که باشد مرا پایمرد</p></div>
<div class="m2"><p>ازین سرفرازان روز نبرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کز ایشان ندانم کسی را بنام</p></div>
<div class="m2"><p>نیامد بر من درود و پیام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو گفت ز ایدر برو با تخوار</p></div>
<div class="m2"><p>مدار این سخن بر دل خویش خوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کز ایران که و مه شناسد همه</p></div>
<div class="m2"><p>بگوید نشان شبان و رمه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بهرام وز زنگهٔ شاوران</p></div>
<div class="m2"><p>نشان جو ز گردان و جنگ‌آوران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همیشه سر و نام تو زنده باد</p></div>
<div class="m2"><p>روان سیاوش فروزنده باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازین هر دو هرگز نگشتی جدای</p></div>
<div class="m2"><p>کنارنگ بودند و او پادشای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نشان خواه ازین دو گو سرفراز</p></div>
<div class="m2"><p>کز ایشان مرا و ترا نیست راز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سران را و گردنکشان را بخوان</p></div>
<div class="m2"><p>می و خلعت آرای و بالا و خوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز گیتی برادر ترا گنج بس</p></div>
<div class="m2"><p>همان کین و آیین به بیگانه کس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سپه را تو باش این زمان پیش رو</p></div>
<div class="m2"><p>تویی کینه‌خواه جهاندار نو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ترا پیش باید بکین ساختن</p></div>
<div class="m2"><p>کمر بر میان بستن و تاختن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو گفت رای تو ای شیر زن</p></div>
<div class="m2"><p>درفشان کند دوده و انجمن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو برخاست آوای کوس از چرم</p></div>
<div class="m2"><p>جهان کرد چون آبنوس از میم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکی دیده‌بان آمد از دیده‌گاه</p></div>
<div class="m2"><p>سخن گفت با او ز ایران سپاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که دشت و در و کوه پر لشکرست</p></div>
<div class="m2"><p>تو خورشید گویی ببند اندرست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز دربند دژ تا بیابان گنگ</p></div>
<div class="m2"><p>سپاهست و پیلان و مردان جنگ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فرود از در دژ فرو هشت بند</p></div>
<div class="m2"><p>نگه کرد لشکر ز کوه بلند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وزان پس بیامد در دژ ببست</p></div>
<div class="m2"><p>یکی بارهٔ تیز رو بر نشست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برفتند پویان تخوار و فرود</p></div>
<div class="m2"><p>جوان را سر بخت بر گرد بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از افراز چون کژ گردد سپهر</p></div>
<div class="m2"><p>نه تندی بکار آید از بن نه مهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گزیدند تیغ یکی برز کوه</p></div>
<div class="m2"><p>که دیدار بد یکسر ایران گروه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جوان با تخوار سرایند گفت</p></div>
<div class="m2"><p>که هر چت بپرسم نباید نهفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کنارنگ وز هرک دارد درفش</p></div>
<div class="m2"><p>خداوند گوپال و زرینه کفش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو بینی به من نام ایشان بگوی</p></div>
<div class="m2"><p>کسی را که دانی از ایران بروی</p></div></div>