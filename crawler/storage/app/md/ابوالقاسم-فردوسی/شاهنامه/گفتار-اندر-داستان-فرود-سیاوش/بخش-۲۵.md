---
title: >-
    بخش ۲۵
---
# بخش ۲۵

<div class="b" id="bn1"><div class="m1"><p>چو برزد سر از کوه تابنده شید</p></div>
<div class="m2"><p>برآمد سر تاج روز سپید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاه پراگنده گردآمدند</p></div>
<div class="m2"><p>همی هر کسی داستانها زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که چندین ز ایرانیان کشته شد</p></div>
<div class="m2"><p>سربخت سالار برگشته شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین چیره دست ترکان بجنگ</p></div>
<div class="m2"><p>سپه را کنون نیست جای درنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر شاه باید شدن بی‌گمان</p></div>
<div class="m2"><p>ببینیم تا بر چه گردد زمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر شاه را دل پر از جنگ نیست</p></div>
<div class="m2"><p>مرا و تو را جای آهنگ نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پسر بی‌پدر شد پدر بی‌پسر</p></div>
<div class="m2"><p>بشد کشته و زنده خسته جگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر جنگ فرمان دهد شهریار</p></div>
<div class="m2"><p>بسازد یکی لشکر نامدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیاییم و دلها پر از کین و جنگ</p></div>
<div class="m2"><p>کنیم این جهان بر بداندیش تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برین رای زان مرز گشتند باز</p></div>
<div class="m2"><p>همه دل پر از خون و جان پر گداز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برادر ز خون برادر به درد</p></div>
<div class="m2"><p>زبانشان ز خویشان پر از یاد کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برفتند یکسر سوی کاسه رود</p></div>
<div class="m2"><p>روانشان ازان کشتگان پر درود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طلایه بیامد بپیش سپاه</p></div>
<div class="m2"><p>کسی را ندید اندران جایگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپیران فرستاد زود آگهی</p></div>
<div class="m2"><p>کز ایرانیان گشت گیتی تهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بشنید پیران هم اندر زمان</p></div>
<div class="m2"><p>بهر سو فرستاد کارآگهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو برگشتن مهتران شد درست</p></div>
<div class="m2"><p>سپهبد روان را ز انده بشست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیامد بشبگیر خود با سپاه</p></div>
<div class="m2"><p>همی گشت بر گرد آن رزمگاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه کوه و هم دشت و هامون و راغ</p></div>
<div class="m2"><p>سراپرده و خیمه بد همچو باغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بلشکر ببخشید خود برگرفت</p></div>
<div class="m2"><p>ز کار جهان مانده اندر شگفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که روزی فرازست و روزی نشیب</p></div>
<div class="m2"><p>گهی شاد دارد گهی با نهیب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همان به که با جام مانیم روز</p></div>
<div class="m2"><p>همی بگذرانیم روزی بروز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدان آگهی نزد افراسیاب</p></div>
<div class="m2"><p>هیونی برافگند هنگام خواب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپهبد بدان آگهی شاد شد</p></div>
<div class="m2"><p>ز تیمار و درددل آزاد شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه لشکرش گشته روشن‌روان</p></div>
<div class="m2"><p>ببستند آیین ره پهلوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه جامهٔ زینت آویختند</p></div>
<div class="m2"><p>درم بر سر او همی ریختند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو آمد بنزدیکی شهر شاه</p></div>
<div class="m2"><p>سپهبد پذیره شدش با سپاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برو آفرین کرد و بسیار گفت</p></div>
<div class="m2"><p>که از پهلوانان ترا نیست جفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دو هفته ز ایوان افراسیاب</p></div>
<div class="m2"><p>همی بر شد آواز چنگ و رباب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سیم هفته پیران چنان کرد رای</p></div>
<div class="m2"><p>که با شادمانی شود باز جای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی خلعت آراست افراسیاب</p></div>
<div class="m2"><p>که گر برشماری بگیرد شتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز دینار وز گوهر شاهوار</p></div>
<div class="m2"><p>ز زرین کمرهای گوهرنگار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از اسپان تازی بزرین ستام</p></div>
<div class="m2"><p>ز شمشیر هندی بزرین نیام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی تخت پرمایه از عاج و ساج</p></div>
<div class="m2"><p>ز پیروزه مهد و ز بیجاده تاج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پرستار چینی و رومی غلام</p></div>
<div class="m2"><p>پر از مشک و عنبر دو پیروزه جام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بنزدیک پیران فرستاد چیز</p></div>
<div class="m2"><p>ازان پس بسی پندها داد نیز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که با موبدان باش و بیدار باش</p></div>
<div class="m2"><p>سپه را ز دشمن نگهدار باش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نگه کن خردمند کارآگهان</p></div>
<div class="m2"><p>بهرجای بفرست گرد جهان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که کیخسرو امروز با خواستست</p></div>
<div class="m2"><p>بداد و دهش گیتی آراستست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نژاد و بزرگی و تخت و کلاه</p></div>
<div class="m2"><p>چو شد گرد ازین بیش چیزی مخواه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز برگشتن دشمن ایمن مشو</p></div>
<div class="m2"><p>زمان تا زمان آگهی خواه نو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بجایی که رستم بود پهلوان</p></div>
<div class="m2"><p>تو ایمن بخسپی بپیچد روان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پذیرفت پیران همه پند اوی</p></div>
<div class="m2"><p>که سالار او بود و پیوند اوی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپهدار پیران و آن انجمن</p></div>
<div class="m2"><p>نهادند سر سوی راه ختن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بپای آمد این داستان فرود</p></div>
<div class="m2"><p>کنون رزم کاموس باید سرود</p></div></div>