---
title: >-
    بخش ۱۰
---
# بخش ۱۰

<div class="b" id="bn1"><div class="m1"><p>گرازان بدرگاه شاه آمدند</p></div>
<div class="m2"><p>گشاده دل و نیک خواه آمدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو رفتند و بردند پیشش نماز</p></div>
<div class="m2"><p>برآشفت و پاسخ نداد ایچ باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی بانگ بر زد به گیو از نخست</p></div>
<div class="m2"><p>پس آنگاه شرم از دو دیده بشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که رستم که باشد که فرمان من</p></div>
<div class="m2"><p>کند پست و پیچد ز پیمان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگیر و ببر زنده بردارکن</p></div>
<div class="m2"><p>وزو نیز با من مگردان سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گفتار او گیو را دل بخست</p></div>
<div class="m2"><p>که بردی برستم بران‌گونه دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآشفت با گیو و با پیلتن</p></div>
<div class="m2"><p>فرو ماند خیره همه انجمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بفرمود پس طوس را شهریار</p></div>
<div class="m2"><p>که رو هردو را زنده برکن به دار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود از جای برخاست کاووس کی</p></div>
<div class="m2"><p>برافروخت برسان آتش ز نی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بشد طوس و دست تهمتن گرفت</p></div>
<div class="m2"><p>بدو مانده پرخاش جویان شگفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که از پیش کاووس بیرون برد</p></div>
<div class="m2"><p>مگر کاندر آن تیزی افسون برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تهمتن برآشفت با شهریار</p></div>
<div class="m2"><p>که چندین مدار آتش اندر کنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه کارت از یکدگر بدترست</p></div>
<div class="m2"><p>ترا شهریاری نه اندرخورست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو سهراب را زنده بر دار کن</p></div>
<div class="m2"><p>پرآشوب و بدخواه را خوار کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزد تند یک دست بر دست طوس</p></div>
<div class="m2"><p>تو گفتی ز پیل ژیان یافت کوس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بالا نگون اندرآمد به سر</p></div>
<div class="m2"><p>برو کرد رستم به تندی گذر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به در شد به خشم اندرآمد به رخش</p></div>
<div class="m2"><p>منم گفت شیراوژن و تاج‌بخش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو خشم آورم شاه کاووس کیست</p></div>
<div class="m2"><p>چرا دست یازد به من طوس کیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمین بنده و رخش گاه من‌ست</p></div>
<div class="m2"><p>نگین گرز و مغفر کلاه من‌ست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب تیره از تیغ رخشان کنم</p></div>
<div class="m2"><p>به آورد گه بر سرافشان کنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر نیزه و تیغ یار من‌اند</p></div>
<div class="m2"><p>دو بازو و دل شهریار من‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه آزاردم او نه من بنده‌ام</p></div>
<div class="m2"><p>یکی بندهٔ آفریننده‌ام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به ایران ار ایدون که سهراب گرد</p></div>
<div class="m2"><p>بیاید نماند بزرگ و نه خرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شما هر کسی چارهٔ جان کنید</p></div>
<div class="m2"><p>خرد را بدین کار پیچان کنید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به ایران نبینید ازین پس مرا</p></div>
<div class="m2"><p>شما را زمین پر کرگس مرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>غمی شد دل نامداران همه</p></div>
<div class="m2"><p>که رستم شبان بود و ایشان رمه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به گودرز گفتند کاین کار تست</p></div>
<div class="m2"><p>شکسته بدست تو گردد درست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهبد جز از تو سخن نشنود</p></div>
<div class="m2"><p>همی بخت تو زین سخن نغنود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به نزدیک این شاه دیوانه رو</p></div>
<div class="m2"><p>وزین در سخن یاد کن نو به نو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سخنهای چرب و دراز آوری</p></div>
<div class="m2"><p>مگر بخت گم بوده بازآوری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سپهدار گودرز کشواد رفت</p></div>
<div class="m2"><p>به نزدیک خسرو خرامید تفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به کاووس کی گفت رستم چه کرد</p></div>
<div class="m2"><p>کز ایران برآوردی امروز گرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فراموش کردی ز هاماوران</p></div>
<div class="m2"><p>وزان کار دیوان مازندران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که گویی ورا زنده بر دار کن</p></div>
<div class="m2"><p>ز شاهان نباید گزافه سخن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو او رفت و آمد سپاهی بزرگ</p></div>
<div class="m2"><p>یکی پهلوانی به کردار گرگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که داری که با او به دشت نبرد</p></div>
<div class="m2"><p>شود برفشاند برو تیره گرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یلان ترا سر به سر گژدهم</p></div>
<div class="m2"><p>شنیدست و دیدست از بیش و کم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همی گوید آن روز هرگز مباد</p></div>
<div class="m2"><p>که با او سواری کند رزم یاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کسی را که جنگی چو رستم بود</p></div>
<div class="m2"><p>بیازارد او را خرد کم بود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بشنید گفتار گودرز شاه</p></div>
<div class="m2"><p>بدانست کاو دارد آیین و راه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پشیمان بشد زان کجا گفته بود</p></div>
<div class="m2"><p>بیهودگی مغزش آشفته بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به گودرز گفت این سخن درخورست</p></div>
<div class="m2"><p>لب پیر با پند نیکوترست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خردمند باید دل پادشا</p></div>
<div class="m2"><p>که تیزی و تندی نیارد بها</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شما را بباید بر او شدن</p></div>
<div class="m2"><p>به خوبی بسی داستانها زدن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سرش کردن از تیزی من تهی</p></div>
<div class="m2"><p>نمودن بدو روزگار بهی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو گودرز برخاست از پیش اوی</p></div>
<div class="m2"><p>پس پهلوان تیز بنهاد روی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برفتند با او سران سپاه</p></div>
<div class="m2"><p>پس رستم اندر گرفتند راه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو دیدند گرد گو پیلتن</p></div>
<div class="m2"><p>همه نامداران شدند انجمن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ستایش گرفتند بر پهلوان</p></div>
<div class="m2"><p>که جاوید بادی و روشن‌روان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جهان سر به سر زیر پای تو باد</p></div>
<div class="m2"><p>همیشه سر تخت جای تو باد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو دانی که کاووس را مغز نیست</p></div>
<div class="m2"><p>به تیزی سخن گفتنش نغز نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بجوشد همانگه پشیمان شود</p></div>
<div class="m2"><p>به خوبی ز سر باز پیمان شود</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تهمتن گر آزرده گردد ز شاه</p></div>
<div class="m2"><p>هم ایرانیان را نباشد گناه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هم او زان سخنها پشیمان شدست</p></div>
<div class="m2"><p>ز تندی بخاید همی پشت دست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تهمتن چنین پاسخ آورد باز</p></div>
<div class="m2"><p>که هستم ز کاووس کی بی‌نیاز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرا تخت زین باشد و تاج ترگ</p></div>
<div class="m2"><p>قبا جوشن و دل نهاده به مرگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چرا دارم از خشم کاووس باک</p></div>
<div class="m2"><p>چه کاووس پیشم چه یک مشت خاک</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سرم گشت سیر و دلم کرد بس</p></div>
<div class="m2"><p>جز از پاک یزدان نترسم ز کس</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز گفتار چون سیر گشت انجمن</p></div>
<div class="m2"><p>چنین گفت گودرز با پیلتن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که شهر و دلیران و لشکر گمان</p></div>
<div class="m2"><p>به دیگر سخنها برند این زمان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کزین ترک ترسنده شد سرفراز</p></div>
<div class="m2"><p>همی رفت زین گونه چندی به راز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که چونان که گژدهم داد آگهی</p></div>
<div class="m2"><p>همه بوم و بر کرد باید تهی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو رستم همی زو بترسد به جنگ</p></div>
<div class="m2"><p>مرا و ترا نیست جای درنگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از آشفتن شاه و پیگار اوی</p></div>
<div class="m2"><p>بدیدم بدرگاه بر گفت‌وگوی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز سهراب یل رفت یکسر سخن</p></div>
<div class="m2"><p>چنین پشت بر شاه ایران مکن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنین بر شده نامت اندر جهان</p></div>
<div class="m2"><p>بدین بازگشتن مگردان نهان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>و دیگر که تنگ اندرآمد سپاه</p></div>
<div class="m2"><p>مکن تیره بر خیره این تاج و گاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به رستم بر این داستانها بخواند</p></div>
<div class="m2"><p>تهمتن چو بشنید خیره بماند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بدو گفت اگر بیم دارد دلم</p></div>
<div class="m2"><p>نخواهم که باشد ز تن بگسلم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ازین ننگ برگشت و آمد به راه</p></div>
<div class="m2"><p>گرازان و پویان به نزدیک شاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو در شد ز در شاه بر پای خاست</p></div>
<div class="m2"><p>بسی پوزش اندر گذشته بخواست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که تندی مرا گوهرست و سرشت</p></div>
<div class="m2"><p>چنان زیست باید که یزدان بکشت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>وزین ناسگالیده بدخواه نو</p></div>
<div class="m2"><p>دلم گشت باریک چون ماه نو</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدین چاره جستن ترا خواستم</p></div>
<div class="m2"><p>چو دیر آمدی تندی آراستم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو آزرده گشتی تو ای پیلتن</p></div>
<div class="m2"><p>پشیمان شدم خاکم اندر دهن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بدو گفت رستم که گیهان تراست</p></div>
<div class="m2"><p>همه کهترانیم و فرمان تراست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کنون آمدم تا چه فرمان دهی</p></div>
<div class="m2"><p>روانت ز دانش مبادا تهی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدو گفت کاووس کامروز بزم</p></div>
<div class="m2"><p>گزینیم و فردا بسازیم رزم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بیاراست رامشگهی شاهوار</p></div>
<div class="m2"><p>شد ایوان به کردار باغ بهار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز آواز ابریشم و بانگ نای</p></div>
<div class="m2"><p>سمن عارضان پیش خسرو به پای</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>همی باده خوردند تا نیم شب</p></div>
<div class="m2"><p>ز خنیاگران برگشاده دولب</p></div></div>