---
title: >-
    بخش ۲۱
---
# بخش ۲۱

<div class="b" id="bn1"><div class="m1"><p>وزان جایگه شاه لشکر براند</p></div>
<div class="m2"><p>به ایران خرامید و رستم بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان تا زواره بیاید ز راه</p></div>
<div class="m2"><p>بدو آگهی آورد زان سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آمد زواره سپیده دمان</p></div>
<div class="m2"><p>سپه راند رستم هم اندر زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس آنگه سوی زابلستان کشید</p></div>
<div class="m2"><p>چو آگاهی از وی به دستان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه سیستان پیش باز آمدند</p></div>
<div class="m2"><p>به رنج و به درد و گداز آمدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو تابوت را دید دستان سام</p></div>
<div class="m2"><p>فرود آمد از اسپ زرین ستام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تهمتن پیاده همی رفت پیش</p></div>
<div class="m2"><p>دریده همه جامه دل کرده ریش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشادند گردان سراسر کمر</p></div>
<div class="m2"><p>همه پیش تابوت بر خاک سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی گفت زال اینت کاری شگفت</p></div>
<div class="m2"><p>که سهراب گرز گران برگرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشانی شد اندر میان مهان</p></div>
<div class="m2"><p>نزاید چنو مادر اندر جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی گفت و مژگان پر از آب کرد</p></div>
<div class="m2"><p>زبان پر ز گفتار سهراب کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آمد تهمتن به ایوان خویش</p></div>
<div class="m2"><p>خروشید و تابوت بنهاد پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازو میخ برکند و بگشاد سر</p></div>
<div class="m2"><p>کفن زو جدا کرد پیش پدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تنش را بدان نامداران نمود</p></div>
<div class="m2"><p>تو گفتی که از چرخ برخاست دود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهان جهان جامه کردند چاک</p></div>
<div class="m2"><p>به ابر اندر آمد سر گرد و خاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه کاخ تابوت بد سر به سر</p></div>
<div class="m2"><p>غنوده بصندوق در شیر نر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو گفتی که سام است با یال و سفت</p></div>
<div class="m2"><p>غمی شد ز جنگ اندر آمد بخفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بپوشید بازش به دیبای زرد</p></div>
<div class="m2"><p>سر تنگ تابوت را سخت کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی گفت اگر دخمه زرین کنم</p></div>
<div class="m2"><p>ز مشک سیه گردش آگین کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو من رفته باشم نماند بجای</p></div>
<div class="m2"><p>وگرنه مرا خود جزین نیست رای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی دخمه کردش ز سم ستور</p></div>
<div class="m2"><p>جهانی ز زاری همی گشت کور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنین گفت بهرام نیکو سخن</p></div>
<div class="m2"><p>که با مردگان آشنایی مکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه ایدر همی ماند خواهی دراز</p></div>
<div class="m2"><p>بسیچیده باش و درنگی مساز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به تو داد یک روز نوبت پدر</p></div>
<div class="m2"><p>سزد گر ترا نوبت آید بسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنین است و رازش نیامد پدید</p></div>
<div class="m2"><p>نیابی به خیره چه جویی کلید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در بسته را کس نداند گشاد</p></div>
<div class="m2"><p>بدین رنج عمر تو گردد بباد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی داستانست پر آب چشم</p></div>
<div class="m2"><p>دل نازک از رستم آید بخشم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برین داستان من سخن ساختم</p></div>
<div class="m2"><p>به کار سیاووش پرداختم</p></div></div>