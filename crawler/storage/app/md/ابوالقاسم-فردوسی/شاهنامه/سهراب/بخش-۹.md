---
title: >-
    بخش ۹
---
# بخش ۹

<div class="b" id="bn1"><div class="m1"><p>یکی نامه فرمود پس شهریار</p></div>
<div class="m2"><p>نوشتن بر رستم نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخست آفرین کرد بر کردگار</p></div>
<div class="m2"><p>جهاندار و پروردهٔ روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر آفرین کرد بر پهلوان</p></div>
<div class="m2"><p>که بیدار دل باش و روشن روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و پشت گردان ایران تویی</p></div>
<div class="m2"><p>به چنگال و نیروی شیران تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشایندهٔ بند هاماوران</p></div>
<div class="m2"><p>ستانندهٔ مرز مازندران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گرز تو خورشید گریان شود</p></div>
<div class="m2"><p>ز تیغ تو ناهید بریان شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو گرد پی رخش تو نیل نیست</p></div>
<div class="m2"><p>هم‌آورد تو در جهان پیل نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمند تو بر شیر بندافگند</p></div>
<div class="m2"><p>سنان تو کوهی ز بن برکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تویی از همه بد به ایران پناه</p></div>
<div class="m2"><p>ز تو برفرازند گردان کلاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گزاینده کاری بد آمد به پیش</p></div>
<div class="m2"><p>کز اندیشهٔ آن دلم گشت ریش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشستند گردان به پیشم به هم</p></div>
<div class="m2"><p>چو خواندیم آن نامهٔ گژدهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان باد کاندر جهان جز تو کس</p></div>
<div class="m2"><p>نباشد به هر کار فریادرس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان‌گونه دیدند گردان نیو</p></div>
<div class="m2"><p>که پیش تو آید گرانمایه گیو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو نامه بخوانی به روز و به شب</p></div>
<div class="m2"><p>مکن داستان را گشاده دو لب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر با سواران بسیارهوش</p></div>
<div class="m2"><p>ز زابل برانی برآری خروش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر اینسان که گژدهم زو یاد کرد</p></div>
<div class="m2"><p>نباید جز از تو ورا هم نبرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به گیو آنگهی گفت برسان دود</p></div>
<div class="m2"><p>عنان تگاور بباید بسود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بباید که نزدیک رستم شوی</p></div>
<div class="m2"><p>به زابل نمانی و گر نغنوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر شب رسی روز را بازگرد</p></div>
<div class="m2"><p>بگویش که تنگ اندرآمد نبرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وگرنه فرازست این مرد گرد</p></div>
<div class="m2"><p>بداندیش را خوار نتوان شمرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ازو نامه بستد به کردار آب</p></div>
<div class="m2"><p>برفت و نجست ایچ آرام و خواب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو نزدیکی زابلستان رسید</p></div>
<div class="m2"><p>خروش طلایه به دستان رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تهمتن پذیره شدش با سپاه</p></div>
<div class="m2"><p>نهادند بر سر بزرگان کلاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیاده شدش گیو و گردان بهم</p></div>
<div class="m2"><p>هر آنکس که بودند از بیش و کم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز اسپ اندرآمد گو نامدار</p></div>
<div class="m2"><p>از ایران بپرسید وز شهریار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز ره سوی ایوان رستم شدند</p></div>
<div class="m2"><p>ببودند یکبار و دم برزدند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگفت آنچ بشنید و نامه بداد</p></div>
<div class="m2"><p>ز سهراب چندی سخن کرد یاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تهمتن چو بشنید و نامه بخواند</p></div>
<div class="m2"><p>بخندید و زان کار خیره بماند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که مانندهٔ سام گرد از مهان</p></div>
<div class="m2"><p>سواری پدید آمد اندر جهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از آزادگان این نباشد شگفت</p></div>
<div class="m2"><p>ز ترکان چنین یاد نتوان گرفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من از دخت شاه سمنگان یکی</p></div>
<div class="m2"><p>پسر دارم و باشد او کودکی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هنوز آن گرامی نداند که جنگ</p></div>
<div class="m2"><p>توان کرد باید گه نام و ننگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فرستادمش زر و گوهر بسی</p></div>
<div class="m2"><p>بر مادر او به دست کسی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنین پاسخ آمد که آن ارجمند</p></div>
<div class="m2"><p>بسی برنیاید که گردد بلند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی می خورد با لب شیربوی</p></div>
<div class="m2"><p>شود بی‌گمان زود پرخاشجوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بباشیم یک روز و دم برزنیم</p></div>
<div class="m2"><p>یکی بر لب خشک نم برزنیم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ازان پس گراییم نزدیک شاه</p></div>
<div class="m2"><p>به گردان ایران نماییم راه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مگر بخت رخشنده بیدار نیست</p></div>
<div class="m2"><p>وگرنه چنین کار دشوار نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو دریا به موج اندرآید ز جای</p></div>
<div class="m2"><p>ندارد دم آتش تیزپای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>درفش مرا چون ببیند ز دور</p></div>
<div class="m2"><p>دلش ماتم آرد به هنگام سور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدین تیزی اندر نیاید به جنگ</p></div>
<div class="m2"><p>نباید گرفتن چنین کار تنگ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به می دست بردند و مستان شدند</p></div>
<div class="m2"><p>ز یاد سپهبد به دستان شدند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دگر روز شبگیر هم پرخمار</p></div>
<div class="m2"><p>بیامد تهمتن برآراست کار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز مستی هم آن روز باز ایستاد</p></div>
<div class="m2"><p>دوم روز رفتن نیامدش یاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سه دیگر سحرگه بیاورد می</p></div>
<div class="m2"><p>نیامد ورا یاد کاووس کی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به روز چهارم برآراست گیو</p></div>
<div class="m2"><p>چنین گفت با گرد سالار نیو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که کاووس تندست و هشیار نیست</p></div>
<div class="m2"><p>هم این داستان بر دلش خوار نیست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>غمی بود ازین کار و دل پرشتاب</p></div>
<div class="m2"><p>شده دور ازو خورد و آرام و خواب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به زابلستان گر درنگ آوریم</p></div>
<div class="m2"><p>ز می باز پیگار و جنگ آوریم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شود شاه ایران به ما خشمگین</p></div>
<div class="m2"><p>ز ناپاک رایی درآید بکین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدو گفت رستم که مندیش ازین</p></div>
<div class="m2"><p>که با ما نشورد کس اندر زمین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بفرمود تا رخش را زین کنند</p></div>
<div class="m2"><p>دم اندر دم نای رویین کنند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سواران زابل شنیدند نای</p></div>
<div class="m2"><p>برفتند با ترگ و جوشن ز جای</p></div></div>