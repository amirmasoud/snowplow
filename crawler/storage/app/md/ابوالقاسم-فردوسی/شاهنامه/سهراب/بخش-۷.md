---
title: >-
    بخش ۷
---
# بخش ۷

<div class="b" id="bn1"><div class="m1"><p>چو آگاه شد دختر گژدهم</p></div>
<div class="m2"><p>که سالار آن انجمن گشت کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنی بود برسان گردی سوار</p></div>
<div class="m2"><p>همیشه به جنگ اندرون نامدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا نام او بود گردآفرید</p></div>
<div class="m2"><p>زمانه ز مادر چنین ناورید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان ننگش آمد ز کار هجیر</p></div>
<div class="m2"><p>که شد لاله رنگش به کردار قیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپوشید درع سواران جنگ</p></div>
<div class="m2"><p>نبود اندر آن کار جای درنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهان کرد گیسو به زیر زره</p></div>
<div class="m2"><p>بزد بر سر ترگ رومی گره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرود آمد از دژ به کردار شیر</p></div>
<div class="m2"><p>کمر بر میان بادپایی به زیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش سپاه اندر آمد چو گرد</p></div>
<div class="m2"><p>چو رعد خروشان یکی ویله کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که گردان کدامند و جنگ‌آوران</p></div>
<div class="m2"><p>دلیران و کارآزموده سران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سهراب شیراوژن او را بدید</p></div>
<div class="m2"><p>بخندید و لب را به دندان گزید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین گفت کامد دگر باره گور</p></div>
<div class="m2"><p>به دام خداوند شمشیر و زور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپوشید خفتان و بر سر نهاد</p></div>
<div class="m2"><p>یکی ترگ چینی به کردار باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیامد دمان پیش گرد آفرید</p></div>
<div class="m2"><p>چو دخت کمندافگن او را بدید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کمان را به زه کرد و بگشاد بر</p></div>
<div class="m2"><p>نبد مرغ را پیش تیرش گذر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به سهراب بر تیر باران گرفت</p></div>
<div class="m2"><p>چپ و راست جنگ سواران گرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگه کرد سهراب و آمدش ننگ</p></div>
<div class="m2"><p>برآشفت و تیز اندر آمد به جنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپر بر سرآورد و بنهاد روی</p></div>
<div class="m2"><p>ز پیگار خون اندر آمد به جوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو سهراب را دید گردآفرید</p></div>
<div class="m2"><p>که برسان آتش همی بردمید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کمان به زه را به بازو فگند</p></div>
<div class="m2"><p>سمندش برآمد به ابر بلند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر نیزه را سوی سهراب کرد</p></div>
<div class="m2"><p>عنان و سنان را پر از تاب کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برآشفت سهراب و شد چون پلنگ</p></div>
<div class="m2"><p>چو بدخواه او چاره گر بد به جنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عنان برگرایید و برگاشت اسپ</p></div>
<div class="m2"><p>بیامد به کردار آذرگشسپ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زدوده سنان آنگهی در ربود</p></div>
<div class="m2"><p>درآمد بدو هم به کردار دود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزد بر کمربند گردآفرید</p></div>
<div class="m2"><p>زره بر برش یک به یک بردرید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز زین برگرفتش به کردار گوی</p></div>
<div class="m2"><p>چو چوگان به زخم اندر آید بدوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو بر زین بپیچید گرد آفرید</p></div>
<div class="m2"><p>یکی تیغ تیز از میان برکشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بزد نیزهٔ او به دو نیم کرد</p></div>
<div class="m2"><p>نشست از بر اسپ و برخاست گرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به آورد با او بسنده نبود</p></div>
<div class="m2"><p>بپیچید ازو روی و برگاشت زود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپهبد عنان اژدها را سپرد</p></div>
<div class="m2"><p>به خشم از جهان روشنایی ببرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو آمد خروشان به تنگ اندرش</p></div>
<div class="m2"><p>بجنبید و برداشت خود از سرش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رها شد ز بند زره موی اوی</p></div>
<div class="m2"><p>درفشان چو خورشید شد روی اوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدانست سهراب کاو دخترست</p></div>
<div class="m2"><p>سر و موی او ازدر افسرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شگفت آمدش گفت از ایران سپاه</p></div>
<div class="m2"><p>چنین دختر آید به آوردگاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سواران جنگی به روز نبرد</p></div>
<div class="m2"><p>همانا به ابر اندر آرند گرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز فتراک بگشاد پیچان کمند</p></div>
<div class="m2"><p>بینداخت و آمد میانش ببند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو گفت کز من رهایی مجوی</p></div>
<div class="m2"><p>چرا جنگ جویی تو ای ماه روی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیامد بدامم بسان تو گور</p></div>
<div class="m2"><p>ز چنگم رهایی نیابی مشور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدانست کاویخت گردآفرید</p></div>
<div class="m2"><p>مر آن را جز از چاره درمان ندید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو روی بنمود و گفت ای دلیر</p></div>
<div class="m2"><p>میان دلیران به کردار شیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دو لشکر نظاره برین جنگ ما</p></div>
<div class="m2"><p>برین گرز و شمشیر و آهنگ ما</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کنون من گشایم چنین روی و موی</p></div>
<div class="m2"><p>سپاه تو گردد پر از گفت‌وگوی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که با دختری او به دشت نبرد</p></div>
<div class="m2"><p>بدین سان به ابر اندر آورد گرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نهانی بسازیم بهتر بود</p></div>
<div class="m2"><p>خرد داشتن کار مهتر بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز بهر من آهو ز هر سو مخواه</p></div>
<div class="m2"><p>میان دو صف برکشیده سپاه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کنون لشکر و دژ به فرمان تست</p></div>
<div class="m2"><p>نباید برین آشتی جنگ جست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دژ و گنج و دژبان سراسر تراست</p></div>
<div class="m2"><p>چو آیی بدان ساز کت دل هواست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو رخساره بنمود سهراب را</p></div>
<div class="m2"><p>ز خوشاب بگشاد عناب را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یکی بوستان بد در اندر بهشت</p></div>
<div class="m2"><p>به بالای او سرو دهقان نکشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دو چشمش گوزن و دو ابرو کمان</p></div>
<div class="m2"><p>تو گفتی همی بشکفد هر زمان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدو گفت کاکنون ازین برمگرد</p></div>
<div class="m2"><p>که دیدی مرا روزگار نبرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>برین بارهٔ دژ دل اندر مبند</p></div>
<div class="m2"><p>که این نیست برتر ز ابر بلند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بپای آورد زخم کوپال من</p></div>
<div class="m2"><p>نراندکسی نیزه بر یال من</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عنان را بپیچید گرد آفرید</p></div>
<div class="m2"><p>سمند سرافراز بر دژ کشید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همی رفت و سهراب با او به هم</p></div>
<div class="m2"><p>بیامد به درگاه دژ گژدهم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درباره بگشاد گرد آفرید</p></div>
<div class="m2"><p>تن خسته و بسته بر دژ کشید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در دژ ببستند و غمگین شدند</p></div>
<div class="m2"><p>پر از غم دل و دیده خونین شدند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز آزار گردآفرید و هجیر</p></div>
<div class="m2"><p>پر از درد بودند برنا و پیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بگفتند کای نیکدل شیرزن</p></div>
<div class="m2"><p>پر از غم بد از تو دل انجمن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که هم رزم جستی هم افسون و رنگ</p></div>
<div class="m2"><p>نیامد ز کار تو بر دوده ننگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بخندید بسیار گرد آفرید</p></div>
<div class="m2"><p>به باره برآمد سپه بنگرید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو سهراب را دید بر پشت زین</p></div>
<div class="m2"><p>چنین گفت کای شاه ترکان چین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چرا رنجه گشتی کنون بازگرد</p></div>
<div class="m2"><p>هم از آمدن هم ز دشت نبرد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بخندید و او را به افسوس گفت</p></div>
<div class="m2"><p>که ترکان ز ایران نیابند جفت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چنین بود و روزی نبودت ز من</p></div>
<div class="m2"><p>بدین درد غمگین مکن خویشتن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همانا که تو خود ز ترکان نه‌ای</p></div>
<div class="m2"><p>که جز به آفرین بزرگان نه‌ای</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بدان زور و بازوی و آن کتف و یال</p></div>
<div class="m2"><p>نداری کس از پهلوانان همال</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ولیکن چو آگاهی آید به شاه</p></div>
<div class="m2"><p>که آورد گردی ز توران سپاه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شهنشاه و رستم بجنبد ز جای</p></div>
<div class="m2"><p>شما با تهمتن ندارید پای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نماند یکی زنده از لشکرت</p></div>
<div class="m2"><p>ندانم چه آید ز بد بر سرت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دریغ آیدم کاین چنین یال و سفت</p></div>
<div class="m2"><p>همی از پلنگان بباید نهفت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ترا بهتر آید که فرمان کنی</p></div>
<div class="m2"><p>رخ نامور سوی توران کنی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نباشی بس ایمن به بازوی خویش</p></div>
<div class="m2"><p>خورد گاو نادان ز پهلوی خویش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو بشنید سهراب ننگ آمدش</p></div>
<div class="m2"><p>که آسان همی دژ به چنگ آمدش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به زیر دژ اندر یکی جای بود</p></div>
<div class="m2"><p>کجا دژ بدان جای بر پای بود</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به تاراج داد آن همه بوم و رست</p></div>
<div class="m2"><p>به یکبارگی دست بد را بشست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنین گفت کامروز بیگاه گشت</p></div>
<div class="m2"><p>ز پیگارمان دست کوتاه گشت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>برآرم به شبگیر ازین باره گرد</p></div>
<div class="m2"><p>ببینند آسیب روز نبرد</p></div></div>