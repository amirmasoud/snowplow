---
title: >-
    بخش ۹ - سخن دقیقی
---
# بخش ۹ - سخن دقیقی

<div class="b" id="bn1"><div class="m1"><p>چو آگاهی آمد به گشتاسپ شاه</p></div>
<div class="m2"><p>که سالار چین جملگی با سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاراسته آمد از جای خویش</p></div>
<div class="m2"><p>خشاش یلش را فرستاد پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بشنید کو رفت با لشکرش</p></div>
<div class="m2"><p>که ویران کند آن نکو کشورش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپهبدش را گفت فردا پگاه</p></div>
<div class="m2"><p>بیارای پیل و بیاور سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی مرزدارانش نامه نوشت</p></div>
<div class="m2"><p>که خاقان ره راد مردی بهشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیایید یکسر به درگاه من</p></div>
<div class="m2"><p>که بر مرز بگذشت بد خواه من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نامه سوی راد مردان رسید</p></div>
<div class="m2"><p>که آمد جهانجوی دشمن پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپاهی بیامد به درگاه شاه</p></div>
<div class="m2"><p>که چندان نبد بر زمین بر گیاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بهر جهانگیر شاه کیان</p></div>
<div class="m2"><p>ببستند گردان گیتی میان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به درگاه خسرو نهادند روی</p></div>
<div class="m2"><p>همه مرزداران به فرمان اوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برین برنیامد بسی روزگار</p></div>
<div class="m2"><p>که گرد از گزیده هزاران هزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فراز آمده بود مر شاه را</p></div>
<div class="m2"><p>کی نامدار و نکو خواه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به لشکرگه آمد سپه را بدید</p></div>
<div class="m2"><p>که شایسته بد رزم را برگزید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازان شادمان گشت فرخنده شاه</p></div>
<div class="m2"><p>دلش خیره آمد زبی مر سپاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دگر روز گشتاسپ با موبدان</p></div>
<div class="m2"><p>ردان و بزرگان و اسپهبدان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گشاد آن در گنج پر کرده جم</p></div>
<div class="m2"><p>سپه را بداد او دو ساله درم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو روزی ببخشید و جوشن بداد</p></div>
<div class="m2"><p>بزد نای و کوس و بنه بر نهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بفرمود بردن ز پیش سپاه</p></div>
<div class="m2"><p>درفش همایون فرخنده شاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سوی رزم ارجاسپ لشکر کشید</p></div>
<div class="m2"><p>سپاهی که هرگز چنان کس ندید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز تاریکی و گرد پای سپاه</p></div>
<div class="m2"><p>کسی روز روشن ندید ایچ راه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بس بانگ اسپان و از بس خروش</p></div>
<div class="m2"><p>همی نالهٔ کوس نشنید گوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درفش فراوان برافراشته</p></div>
<div class="m2"><p>همه نیزه‌ها ز ابر بگذاشته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو رسته درخت از بر کوهسار</p></div>
<div class="m2"><p>چو بیشه نیستان به وقت بهار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازین سان همی رفت گشتاسپ شاه</p></div>
<div class="m2"><p>ز کشور به کشور همی شد سپاه</p></div></div>