---
title: >-
    بخش ۲۰ - سخن دقیقی
---
# بخش ۲۰ - سخن دقیقی

<div class="b" id="bn1"><div class="m1"><p>کی نامبردار فرخنده شاه</p></div>
<div class="m2"><p>سوی گاه باز آمد از رزمگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بستور گفتا که فردا پکاه</p></div>
<div class="m2"><p>سوی کشور نامور کش سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیامد سپهبد هم از بامداد</p></div>
<div class="m2"><p>بزد کوس و لشکر بنه برنهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ایران زمین باز کردند روی</p></div>
<div class="m2"><p>همه خیره دل گشته و جنگجوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه خستگان را ببردند نیز</p></div>
<div class="m2"><p>نماندند از خواسته نیز چیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ایران زمین باز بردندشان</p></div>
<div class="m2"><p>به دانا پزشکان سپردندشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شاه جهان باز شد بازجای</p></div>
<div class="m2"><p>به پور مهین داد فرخ همای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپه را به بستور فرخنده داد</p></div>
<div class="m2"><p>عجم را چنین بود آیین و داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدادش از آزادگان ده هزار</p></div>
<div class="m2"><p>سواران جنگی و نیزه گزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بفرمود و گفت ای گو رزمسار</p></div>
<div class="m2"><p>یکی بر پی شاه توران بتاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ایتاش و خلج ستان برگذر</p></div>
<div class="m2"><p>بکش هرک یابی به کین پدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز هرچیز بایست بردش به کار</p></div>
<div class="m2"><p>بدادش همه بی‌مر و بی‌شمار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم‌آنگاه بستور برد آن سپاه</p></div>
<div class="m2"><p>و شاه جهان از بر تخت و گاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشست و کیی تاج بر سر نهاد</p></div>
<div class="m2"><p>سپه را همه یکسره بار داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در گنج بگشاد وز خواسته</p></div>
<div class="m2"><p>سپه را همه کرد آراسته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سران را همه شهرها داد نیز</p></div>
<div class="m2"><p>سکی را نماند ایچ ناداده چیز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرا پادشاهی سزا بد بداد</p></div>
<div class="m2"><p>کرا پایه بایست پایه نهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو اندر خور کارشان داد ساز</p></div>
<div class="m2"><p>سوی خانهاشان فرستاد باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خرامید بر گاه و باره ببست</p></div>
<div class="m2"><p>به کاخ شهنشاهی اندر نشست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بفرمود تا آذر افروختند</p></div>
<div class="m2"><p>برو عود و عنبر همی سوختند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمینش بکردند از زر پاک</p></div>
<div class="m2"><p>همه هیزمش عود و عنبرش خاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه کاخ را کار اندام کرد</p></div>
<div class="m2"><p>پسش خان گشتاسپیان نام کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بفرمود تا بر در گنبدش</p></div>
<div class="m2"><p>بدادند جاماسپ را موبدش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سوی مرزدارانش نامه نوشت</p></div>
<div class="m2"><p>که ما را خداوند یافه نهشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شبان شده تیره‌مان روز کرد</p></div>
<div class="m2"><p>کیان را به هر جای پیروز کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به نفرین شد ارجاسپ ناآفرین</p></div>
<div class="m2"><p>چنین است کار جهان آفرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو پیروزی شاهتان بشنوید</p></div>
<div class="m2"><p>گزیتی به آذر پرستان دهید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو آگاه شد قیصر آن شاه روم</p></div>
<div class="m2"><p>که فرخ شد آن شاه و ارجاسپ شوم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرسته فرستاد با خواسته</p></div>
<div class="m2"><p>غلامان و اسپان آراسته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شه بت‌پرستان و رایان هند</p></div>
<div class="m2"><p>گزیتش بدادند شاهان سند</p></div></div>