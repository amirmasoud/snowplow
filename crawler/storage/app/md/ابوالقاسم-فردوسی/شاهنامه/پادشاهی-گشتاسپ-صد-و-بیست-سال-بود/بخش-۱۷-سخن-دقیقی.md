---
title: >-
    بخش ۱۷ - سخن دقیقی
---
# بخش ۱۷ - سخن دقیقی

<div class="b" id="bn1"><div class="m1"><p>بدو داد پس شاه بهزاد را</p></div>
<div class="m2"><p>سپه جوشن و خود پولاد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس شاه کشته میان را ببست</p></div>
<div class="m2"><p>سیه رنگ بهزاد را برنشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرامید تا رزمگاه سپاه</p></div>
<div class="m2"><p>نشسته بران خوب رنگ سیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش صف دشمنان ایستاد</p></div>
<div class="m2"><p>همی برکشید از جگر سرد باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم گفت بستور پور زریر</p></div>
<div class="m2"><p>پذیره نیاید مرا نره شیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا باشد آن جادوی بیدرفش</p></div>
<div class="m2"><p>که بردست آن جمشیدی درفش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو پاسخ ندادند آزاد را</p></div>
<div class="m2"><p>برانگیخت شبرنگ بهزاد را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بکشت از تگینان لشکر بسی</p></div>
<div class="m2"><p>پذیره نیامد مر او را کسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وزان سوی دیگر گو اسفندیار</p></div>
<div class="m2"><p>همی کشتشان بی‌مر و بی‌شمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سالار چین دید بستور را</p></div>
<div class="m2"><p>کیان زاده آن پهلوان پور را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به لشکر بگفت این که شاید بدن</p></div>
<div class="m2"><p>کزین سان همی نیزه داند زدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکشت از تگینان من بی‌شمار</p></div>
<div class="m2"><p>مگر گشت زنده زریر سوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که نزد من آمد زریر از نخست</p></div>
<div class="m2"><p>برین سان همی تاخت باره درست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجا رفت آن بیدرفش گزین</p></div>
<div class="m2"><p>هم‌اکنون سوی منش خوانید هین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخواندند و آمد دمان بیدرفش</p></div>
<div class="m2"><p>گرفته به دست آن درفش بنفش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نشسته بران بارهٔ خسروی</p></div>
<div class="m2"><p>بپوشیده آن جوشن پهلوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خرامید تا پیش لشکر ز شاه</p></div>
<div class="m2"><p>نگهبان مرز و نگهبان گاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرفته همان تیغ زهر آبدار</p></div>
<div class="m2"><p>که افگنده بد آن زریر سوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگشتند هر دو به ژوپین و تیر</p></div>
<div class="m2"><p>سر جاودان ترک و پور زریر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس آگاه کردند زان کارزار</p></div>
<div class="m2"><p>پس شاه را فرخ اسفندیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی تاختش تا بدیشان رسید</p></div>
<div class="m2"><p>سر جاودان چون مر او را بدید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برافگند اسپ از میان نبرد</p></div>
<div class="m2"><p>بدانست کش بر سر افتاد مرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بینداخت آن زهر خورده به روی</p></div>
<div class="m2"><p>مگر کس کند زشت رخشنده روی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیامد برو تیغ زهر آبدار</p></div>
<div class="m2"><p>گرفتش همان تیغ شاه استوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زدش پهلوانی یکی بر جگر</p></div>
<div class="m2"><p>چنان کز دگر سو برون کرد سر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو آهو ز باره در افتاد و مرد</p></div>
<div class="m2"><p>بدید از کیان زادگان دستبرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرود آمد از باره اسفندیار</p></div>
<div class="m2"><p>سلیح زریر آن گزیده سوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ازان جادوی پیر بیرون کشید</p></div>
<div class="m2"><p>سرش را ز نیمه‌تن اندر برید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نکو رنگ بارهٔ زریر و درفش</p></div>
<div class="m2"><p>ببرد و سر بی‌هنر بیدرفش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپاه کیان بانگ برداشتند</p></div>
<div class="m2"><p>همی نعره از ابر بگذاشتند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که پیروز شد شاه و دشمن فگند</p></div>
<div class="m2"><p>بشد بازآورد اسپ سمند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شد آن شاهزاده سوار دلیر</p></div>
<div class="m2"><p>سوی شاه برد آن سمند زریر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر پیر جادوش بنهاد پیش</p></div>
<div class="m2"><p>کشنده بکشت اینت آیین و کیش</p></div></div>