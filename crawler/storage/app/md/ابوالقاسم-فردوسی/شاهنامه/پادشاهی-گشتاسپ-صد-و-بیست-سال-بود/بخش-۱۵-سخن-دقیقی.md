---
title: >-
    بخش ۱۵ - سخن دقیقی
---
# بخش ۱۵ - سخن دقیقی

<div class="b" id="bn1"><div class="m1"><p>پس آگاهی آمد به اسفندیار</p></div>
<div class="m2"><p>که کشته شد آن شاه نیزه گزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدرت از غم او بکاهد همی</p></div>
<div class="m2"><p>کنون کین او خواست خواهد همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی گوید آنکس کجاکین اوی</p></div>
<div class="m2"><p>بخواهد نهد پیش دشمنش روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مر او را دهم دخترم را همای</p></div>
<div class="m2"><p>وکرد ایزدش را برین بر گوای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی نامور دست بر دست زد</p></div>
<div class="m2"><p>بنالید ازان روزگاران بد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه ساله زین روز ترسیدمی</p></div>
<div class="m2"><p>چو او را به رزم اندرون دیدمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریغا سوارا گوا مهترا</p></div>
<div class="m2"><p>که بختش جدا کرد تاج از سرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که کشت آن سیه پیل نستوه را</p></div>
<div class="m2"><p>که کند از زمین آهنین کوه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درفش و سرلشکر و جای خویش</p></div>
<div class="m2"><p>برادرش را داد و خود رفت پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به قلب اندر آمد به جای زریر</p></div>
<div class="m2"><p>به صف اندر استاد چون نره شیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیش اندر آمد میان را ببست</p></div>
<div class="m2"><p>گرفت آن درفش همایون به دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برادرش بد پنج دانسته راه</p></div>
<div class="m2"><p>همه از در تاج و همتای شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه ایستادند در پیش اوی</p></div>
<div class="m2"><p>که لشکر شکستن بدی کیش اوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به آزادگان گفت پیش سپاه</p></div>
<div class="m2"><p>که ای نامداران و گردان شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگر تا چه گویم یکی بشنوید</p></div>
<div class="m2"><p>به دین خدای جهان بگروید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگر تا نترسید از مرگ و چیز</p></div>
<div class="m2"><p>که کس بی‌زمانه نمردست نیز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرا کشت خواهد همی روزگار</p></div>
<div class="m2"><p>چه نیکوتر از مرگ در کارزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدانید یکسر که روزیست این</p></div>
<div class="m2"><p>که کافر پدید آید از پاک دین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شما از پس پشتها منگرید</p></div>
<div class="m2"><p>مجویید فریاد و سر مشمرید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگر تا نبینید بگریختن</p></div>
<div class="m2"><p>نگر تا نترسید ز آویختن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سر نیزه‌ها را به رزم افگنید</p></div>
<div class="m2"><p>زمانی بکوشید و مردی کنید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدین اندرون بود اسفندیار</p></div>
<div class="m2"><p>که بانگ پدرش آمد از کوهسار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که این نامداران و گردان من</p></div>
<div class="m2"><p>همه مر مرا چون تن و جان من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مترسید از نیزه و گرز و تیغ</p></div>
<div class="m2"><p>که از بخش‌مان نیست روی گریغ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به دین خدا ای گو اسفندیار</p></div>
<div class="m2"><p>به جان زریر آن نبرده سوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که آید فرود او کنون در بهشت</p></div>
<div class="m2"><p>که من سوی لهراسپ نامه نوشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پذیرفتم اندرز آن شاه پیر</p></div>
<div class="m2"><p>که گر بخت نیکم بود دستگیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که چون بازگردم ازین رزمگاه</p></div>
<div class="m2"><p>به اسفندیارم دهم تاج و گاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپه را همه پیش رفتن دهم</p></div>
<div class="m2"><p>ورا خسروی تاج بر سر نهم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنانچون پدر داد شاهی مرا</p></div>
<div class="m2"><p>دهم همچنان پادشاهی ورا</p></div></div>