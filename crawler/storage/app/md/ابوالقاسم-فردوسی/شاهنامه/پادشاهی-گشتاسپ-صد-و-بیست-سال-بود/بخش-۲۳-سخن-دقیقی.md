---
title: >-
    بخش ۲۳ - سخن دقیقی
---
# بخش ۲۳ - سخن دقیقی

<div class="b" id="bn1"><div class="m1"><p>بدان روزگار اندر اسفندیار</p></div>
<div class="m2"><p>به دشت اندرون بد ز بهر شکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازان دشت آواز کردش کسی</p></div>
<div class="m2"><p>که جاماسپ را کرد خسرو گسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آن بانگ بشنید آمد شگفت</p></div>
<div class="m2"><p>بپیچید و خندیدن اندر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پسر بود او را گزیده چهار</p></div>
<div class="m2"><p>همه رزم‌جوی و همه نیزه‌دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی نام بهمن دوم مهرنوش</p></div>
<div class="m2"><p>سیم نام او بد دلافروز طوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چهارم بدش نام نوشاذرا</p></div>
<div class="m2"><p>نهادی کجا گنبد آذرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شاه جهان گفت بهمن پسر</p></div>
<div class="m2"><p>که تا جاودان سبز بادات سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی ژرف خنده بخندید شاه</p></div>
<div class="m2"><p>نیابم همی اندرین هیچ راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت پورا بدین روزگار</p></div>
<div class="m2"><p>کس آید مرا از در شهریار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که آواز بشنیدم از ناگهان</p></div>
<div class="m2"><p>بترسم که از گفتهٔ بی‌رهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز من خسرو آزار دارد همی</p></div>
<div class="m2"><p>دلش از رهی بار دارد همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرانمایه فرزند گفتا چرا</p></div>
<div class="m2"><p>چه کردی تو با خسرو کشورا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر شهریارانش گفت ای پسر</p></div>
<div class="m2"><p>ندانم گناهی به جای پدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر آنک تا دین بیاموختم</p></div>
<div class="m2"><p>همی در جهان آتش افروختم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان ویژه کردم به برنده تیغ</p></div>
<div class="m2"><p>چرا داد از من دل شاه میغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همانا دل دیو بفریفتست</p></div>
<div class="m2"><p>که بر کشتن من بیاشیفتست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی تا بدین اندرون بود شاه</p></div>
<div class="m2"><p>پدید آمد از دور گرد سیاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چراغ جهان بود دستور شاه</p></div>
<div class="m2"><p>فرستادهٔ شاه زی پور شاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو از دور دیدش ز کهسار گرد</p></div>
<div class="m2"><p>بدانست کامد فرستاده مرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پذیره شدش گرد فرزند شاه</p></div>
<div class="m2"><p>همی بود تا او بیامد ز راه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بارهٔ چمنده فرود آمدند</p></div>
<div class="m2"><p>گو پیر هر دو پیاده شدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بپرسید ازو فرخ اسفندیار</p></div>
<div class="m2"><p>که چونست شاه آن گو نامدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خردمند گفتا درستست و شاد</p></div>
<div class="m2"><p>برش را ببوسید و نامه بداد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درست از همه کارش آگاه کرد</p></div>
<div class="m2"><p>که مر شاه را دیو بی‌راه کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خردمند را گفتش اسفندیار</p></div>
<div class="m2"><p>چه بینی مرا اندرین روی کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر ایدونک با تو بیایم به در</p></div>
<div class="m2"><p>نه نیکو کند کار با من پدر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ور ایدونک نایم به فرمانبری</p></div>
<div class="m2"><p>برون کرده باشم سر از کهتری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی چاره‌ساز ای خردمند پیر</p></div>
<div class="m2"><p>نیابد چنین ماند بر خیره خیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خردمند گفت ای شه پهلوان</p></div>
<div class="m2"><p>به دانندگی پیر و بختت جوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو دانی که خشم پدر بر پسر</p></div>
<div class="m2"><p>به از جور مهتر پسر بر پدر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببایدت رفت چنینست روی</p></div>
<div class="m2"><p>که هرچ او کند پادشاهست اوی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برین بر نهادند و گشتند باز</p></div>
<div class="m2"><p>فرستاده و پور خسرو نیاز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی جای خویش فرود آورید</p></div>
<div class="m2"><p>به کف بر گرفتند هر دو نبید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به پیشش همی عود می‌سوختند</p></div>
<div class="m2"><p>تو گفتی همی آتش افروختند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دگر روز بنشست بر تخت خویش</p></div>
<div class="m2"><p>ز لشکر بیامد فراوان به پیش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه لشکرش را به بهمن سپرد</p></div>
<div class="m2"><p>وزانجا خرامید با چند گرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیامد به درگاه آزاد شاه</p></div>
<div class="m2"><p>کمر بسته بر نهاده کلاه</p></div></div>