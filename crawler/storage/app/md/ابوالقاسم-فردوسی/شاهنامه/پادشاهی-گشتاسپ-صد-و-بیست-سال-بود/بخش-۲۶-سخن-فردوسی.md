---
title: >-
    بخش ۲۶ - سخن فردوسی
---
# بخش ۲۶ - سخن فردوسی

<div class="b" id="bn1"><div class="m1"><p>چو این نامه‌ا فتاد در دست من</p></div>
<div class="m2"><p>به ماه گراینده شد شست من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه کردم این نظم سست آمدم</p></div>
<div class="m2"><p>بسی بیت ناتندرست آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من این زان بگفتم که تا شهریار</p></div>
<div class="m2"><p>بداند سخن گفتن نابکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو گوهر بد این با دو گوهر فروش</p></div>
<div class="m2"><p>کنون شاه دارد به گفتار گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن چون بدین گونه بایدت گفت</p></div>
<div class="m2"><p>مگو و مکن طبع با رنج جفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بند روان بینی و رنج تن</p></div>
<div class="m2"><p>به کانی که گوهر نیابی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو طبعی نباشد چو آب روان</p></div>
<div class="m2"><p>مبر سوی این نامهٔ خسروان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دهن گر بماند ز خوردن تهی</p></div>
<div class="m2"><p>ازان به که ناساز خوانی نهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی نامه بود از گه باستان</p></div>
<div class="m2"><p>سخنهای آن برمنش راستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو جامی گهر بود و منثور بود</p></div>
<div class="m2"><p>طبایع ز پیوند او دور بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گذشته برو سالیان شش هزار</p></div>
<div class="m2"><p>گر ایدونک پرسش نماید شمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبردی به پیوند او کس گمان</p></div>
<div class="m2"><p>پر اندیشه گشت این دل شادمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفتم به گوینده بر آفرین</p></div>
<div class="m2"><p>که پیوند را راه داد اندرین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگرچه نپیوست جز اندکی</p></div>
<div class="m2"><p>ز رزم و ز بزم از هزاران یکی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همو بود گوینده را راه بر</p></div>
<div class="m2"><p>که بنشاند شاهی ابر گاه‌بر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی یافت از مهتران ارج و گنج</p></div>
<div class="m2"><p>ز خوی بد خویش بودی به رنج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ستایندهٔ شهریاران بدی</p></div>
<div class="m2"><p>به کاخ افسر نامداران بدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به شهر اندرون گشته گشتی سخن</p></div>
<div class="m2"><p>ازو نو شدی روزگار کهن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من این نامه فرخ گرفتم به فال</p></div>
<div class="m2"><p>بسی رنج بردم به بسیار سال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندیدم سرافراز بخشنده‌ای</p></div>
<div class="m2"><p>به گاه کیان‌بر درخشنده‌ای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا این سخن بر دل آسان نبود</p></div>
<div class="m2"><p>بجز خامشی هیچ درمان نبود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نشستنگه مردم نیک‌بخت</p></div>
<div class="m2"><p>یکی باغ دیدم سراسر درخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جایی نبد هیچ پیدا درش</p></div>
<div class="m2"><p>بجز نام شاهی نبد افسرش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که گر در خور باغ بایستمی</p></div>
<div class="m2"><p>اگر نیک بودی بشایستمی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سخن را چو بگذاشتم سال بیست</p></div>
<div class="m2"><p>بدان تا سزاوار این رنج کیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ابوالقاسم آن شهریار جهان</p></div>
<div class="m2"><p>کزو تازه شد تاج شاهنشاهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهاندار محمود با فر و جود</p></div>
<div class="m2"><p>که او را کند ماه و کیوان سجود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر نامه را نام او تاج گشت</p></div>
<div class="m2"><p>به فرش دل تیره چون عاج گشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به بخش و به داد و به رای و هنر</p></div>
<div class="m2"><p>نبد تاج را زو سزاوارتر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بیامد نشست از بر تخت داد</p></div>
<div class="m2"><p>جهاندار چون او ندارد به یاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز شاهان پیشی همی بگذرد</p></div>
<div class="m2"><p>نفس داستان را همی نشمرد(؟)</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه دینار بر چشم او بر چه خاک</p></div>
<div class="m2"><p>به رزم و به بزم اندرش نیست باک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گه بزم زر و گه رزم تیغ</p></div>
<div class="m2"><p>ز خواهنده هرگز ندارد دریغ</p></div></div>