---
title: >-
    بخش ۳۰
---
# بخش ۳۰

<div class="b" id="bn1"><div class="m1"><p>یکی مایه‌ور پور اسفندیار</p></div>
<div class="m2"><p>که نوش آذرش خواندی شهریار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بران بام دژ بود و چشمش به راه</p></div>
<div class="m2"><p>بدان تا کی آید ز ایران سپاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پدر را بگوید چو بیند کسی</p></div>
<div class="m2"><p>به بالای دژ درنمانده بسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو جاماسپ را دید پویان به راه</p></div>
<div class="m2"><p>به سربر یکی نغز توزی کلاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گفت کامد ز توران سوار</p></div>
<div class="m2"><p>بپویم بگویم به اسفندیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرود آمد از بارهٔ دژ دوان</p></div>
<div class="m2"><p>چنین گفت کای نامور پهلوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سواری همی بینم از دیدگاه</p></div>
<div class="m2"><p>کلاهی به سر بر نهاده سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوم باز بینم که گشتاسپیست</p></div>
<div class="m2"><p>وگر کینه‌جویست و ارجاسپیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر ترک باشد ببرم سرش</p></div>
<div class="m2"><p>به خاک افگنم نابسوده برش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین گفت پرمایه اسفندیار</p></div>
<div class="m2"><p>که راه گذر کی بوده بی‌سوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همانا کز ایران یکی لشکری</p></div>
<div class="m2"><p>سوی ما بیامد به پیغمبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کلاهی به سر بر نهاده دوپر</p></div>
<div class="m2"><p>ز بیم سواران پرخاشخر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بشنید نوش آذر از پهلوان</p></div>
<div class="m2"><p>بیامد بران بارهٔ دژ دوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو جاماسپ تنگ اندر آمد ز راه</p></div>
<div class="m2"><p>هم از باره دانست فرزند شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیامد به نزدیک فرخ پدر</p></div>
<div class="m2"><p>که فرخنده جاماسپ آمد به در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفرمود تا دژ گشادند باز</p></div>
<div class="m2"><p>درآمد خردمند و بردش نماز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدادش درود پدر سربسر</p></div>
<div class="m2"><p>پیامی که آورده بد در بدر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین پاسخ آورد اسفندیار</p></div>
<div class="m2"><p>که ای از خرد در جهان یادگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خردمند و کنداور و سرفراز</p></div>
<div class="m2"><p>چرا بسته را برد باید نماز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسی را که بر دست و پای آهنست</p></div>
<div class="m2"><p>نه مردم نژادست کهرمنست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درود شهنشاه ایران دهی</p></div>
<div class="m2"><p>ز دانش ندارد دلت آگهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درودم از ارجاسپ آمد کنون</p></div>
<div class="m2"><p>کز ایران همی دست شوید به خون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا بند کردند بر بی‌گناه</p></div>
<div class="m2"><p>همانا گه رزم فرزند شاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنین بود پاداش رنج مرا</p></div>
<div class="m2"><p>به آهن بیاراست گنج مرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنون همچنین بسته باید تنم</p></div>
<div class="m2"><p>به یزدان گوای منست آهنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که بر من ز گشتاسپ بیداد بود</p></div>
<div class="m2"><p>ز گفت گرزم اهرمن شاد بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مبادا که این بد فرامش کنم</p></div>
<div class="m2"><p>روان را به گفتار بیهش کنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو گفت جاماسپ کای راست‌گوی</p></div>
<div class="m2"><p>جهانگیر و کنداور و نیک‌خوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلت گر چنین از پدر خیره گشت</p></div>
<div class="m2"><p>نگر بخت این پادشا تیره گشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو لهراسپ شاه آن پرستنده مرد</p></div>
<div class="m2"><p>که ترکان بکشتندش اندر نبرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همان هیربد نیز یزدان‌پرست</p></div>
<div class="m2"><p>که بودند با زند و استا به دست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بکشتند هشتاد از موبدان</p></div>
<div class="m2"><p>پرستنده و پاک‌دل بخردان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز خونشان به نوش‌آذر آذر بمرد</p></div>
<div class="m2"><p>چنین بدکنش خوار نتوان شمرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بهر نیا دل پر از درد کن</p></div>
<div class="m2"><p>برآشوب و رخسارگان زرد کن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز کین یا ز دین گر نجنبی ز جای</p></div>
<div class="m2"><p>نباشی پسندیدهٔ رهنمای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنین داد پاسخ که ای نیک‌نام</p></div>
<div class="m2"><p>بلنداختر و گرد و جوینده کام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>براندیش کان پیر لهراسپ را</p></div>
<div class="m2"><p>پرستنده و باب گشتاسپ را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پسر به که جوید همی کین اوی</p></div>
<div class="m2"><p>که تخت پدر داشت و ایین اوی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو گفت ار ایدونک کین نیا</p></div>
<div class="m2"><p>نجویی نداری به دل کیمیا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همای خردمند و به آفرید</p></div>
<div class="m2"><p>که باد هوا روی ایشان ندید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به ترکان سیراند با درد و داغ</p></div>
<div class="m2"><p>پیاده دوان رنگ رخ چون چراغ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنین پاسخ آوردش اسفندیار</p></div>
<div class="m2"><p>که من بسته بودم چنین زار و خوار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نکردند زیشان ز من هیچ یاد</p></div>
<div class="m2"><p>نه برزد کس از بهر من سردباد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه گویی به پاسخ که روزی همای</p></div>
<div class="m2"><p>ز من کرد یاد اندرین تنگ جای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دگر نیز پرمایه به آفرید</p></div>
<div class="m2"><p>که گفتی مرا در جهان خود ندید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو گفت جاماسپ کای پهلوان</p></div>
<div class="m2"><p>پدرت از جهان تیره دارد روان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به کوه اندرست این زمان با سران</p></div>
<div class="m2"><p>دو دیده پر از آب و لب ناچران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سپاهی ز ترکان بگرد اندرش</p></div>
<div class="m2"><p>همانا نبینی سر و افسرش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نیاید پسند جهان‌آفرین</p></div>
<div class="m2"><p>که تو دل بپیچی ز مهر و ز دین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>برادر که بد مر ترا سی و هشت</p></div>
<div class="m2"><p>ازان پنج ماند و دگر درگذشت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چنین پاسخ آوردش اسفندیار</p></div>
<div class="m2"><p>که چندین برادر بدم نامدار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همه شاد با رامش و من به بند</p></div>
<div class="m2"><p>نکردند یاد از من مستمند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر من کنون کین بسیچم چه سود</p></div>
<div class="m2"><p>کزیشان برآورد بدخواه دود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو جاماسپ زین گونه پاسخ شنود</p></div>
<div class="m2"><p>دلش گشت از درد پر داغ و دود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همی بود بر پای و دل پر ز خشم</p></div>
<div class="m2"><p>به زاری همی راند آب از دو چشم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدو گفت کای پهلوان جهان</p></div>
<div class="m2"><p>اگر تیره گردد دلت با روان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چه گویی کنون کار فرشیدورد</p></div>
<div class="m2"><p>که بود از تو همواره با داغ و درد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به هر سو که بودی به رزم و به بزم</p></div>
<div class="m2"><p>پر از درد و نفرین بدی بر گرزم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>پر از زخم شمشیر دیدم تنش</p></div>
<div class="m2"><p>دریده برو مغفر و جوشنش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همی زار می بگسلد جان اوی</p></div>
<div class="m2"><p>ببخشای بر چشم گریان اوی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو آواز دادش ز فرشیدورد</p></div>
<div class="m2"><p>دلش گشت پرخون و جان پر ز درد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو باز آمدش دل به جاماسپ گفت</p></div>
<div class="m2"><p>که این بد چرا داشتی در نهفت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بفرمای کاهنگران آورند</p></div>
<div class="m2"><p>چو سوهان و پتک گران آورند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بیاورد جاماسپ آهنگران</p></div>
<div class="m2"><p>چو سندان پولاد و پتک گران</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بسودند زنجیر و مسمار و غل</p></div>
<div class="m2"><p>همان بند رومی به کردار پل</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو شد دیر بر سودن بستگی</p></div>
<div class="m2"><p>به بد تنگدل بسته از خستگی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به آهنگران گفت کای شوربخت</p></div>
<div class="m2"><p>ببندی و بسته ندانی گسخت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همی گفت من بند آن شهریار</p></div>
<div class="m2"><p>نکردم به پیش خردمند خوار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بپیچید تن را و بر پای جست</p></div>
<div class="m2"><p>غمی شد به پابند یازید دست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بیاهیخت پای و بپیچید دست</p></div>
<div class="m2"><p>همه بند و زنجیر بر هم شکست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو بگسست زنجیر بی‌توش گشت</p></div>
<div class="m2"><p>بیفتاد از درد و بیهوش گشت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ستاره شمرکان شگفتی بدید</p></div>
<div class="m2"><p>بران تاجدار آفرین گسترید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو آمد به هوش آن گو زورمند</p></div>
<div class="m2"><p>همی پیش بنهاد زنجیر و بند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چنین گفت کاین هدیه‌های گرزم</p></div>
<div class="m2"><p>منش پست بادش به بزم و به رزم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به گرمابه شد با تن دردمند</p></div>
<div class="m2"><p>ز زنجیر فرسوده و مستمند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو آمد به در پس گو نامدار</p></div>
<div class="m2"><p>رخش بود همچون گل اندر بهار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>یکی جوشن خسروانی بخواست</p></div>
<div class="m2"><p>همان جامهٔ پهلوانی بخواست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بفرمود کان بارهٔ گام زن</p></div>
<div class="m2"><p>بیارید و آن ترگ و شمشیر من</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو چشمش بران تیزرو برفتاد</p></div>
<div class="m2"><p>ز یزدان نیکی دهش کرد یاد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همی گفت گر من گنه کرده‌ام</p></div>
<div class="m2"><p>ازینسان به بند اندر آزرده‌ام</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چه کرد این چمان بارهٔ بربری</p></div>
<div class="m2"><p>چه بایست کردن بدین لاغری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بشویید و او را بی‌آهو کنید</p></div>
<div class="m2"><p>به خوردن تنش را به نیرو کنید</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>فرستاد کس نزد آهنگران</p></div>
<div class="m2"><p>هرانکس که استاد بود اندران</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>برفتند و چندی زره خواستند</p></div>
<div class="m2"><p>سلیحش یکایک بپیراستند</p></div></div>