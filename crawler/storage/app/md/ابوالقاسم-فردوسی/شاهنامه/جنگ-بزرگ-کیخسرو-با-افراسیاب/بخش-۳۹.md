---
title: >-
    بخش ۳۹
---
# بخش ۳۹

<div class="b" id="bn1"><div class="m1"><p>به ایرانیان گفت هنگام من</p></div>
<div class="m2"><p>فراز آمد و تازه شد کام من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخواهید چیزی که باید ز من</p></div>
<div class="m2"><p>که آمد پراگندن انجمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه مهتران زار و گریان شدند</p></div>
<div class="m2"><p>ز درد شهنشاه بریان شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گفت هرکس که ای شهریار</p></div>
<div class="m2"><p>که را مانی این تاج را یادگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بشنید دستان خسرو پرست</p></div>
<div class="m2"><p>زمین را ببوسید و برپای جست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین گفت کای شهریار جهان</p></div>
<div class="m2"><p>سزد کآرزوها ندارم نهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو دانی که رستم به ایران چه کرد</p></div>
<div class="m2"><p>به رزم و به بزم و به ننگ و نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کاووس کی شد به مازندران</p></div>
<div class="m2"><p>رهی دور و فرسنگهای گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو دیوان ببستند کاووس را</p></div>
<div class="m2"><p>چو گودرز گردنکش و طوس را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تهمتن چو بشنید تنها برفت</p></div>
<div class="m2"><p>به مازندران روی بنهاد تفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیابان و تاریکی و دیو و شیر</p></div>
<div class="m2"><p>همان جادوی و اژدهای دلیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدان رنج و تیمار ببرید راه</p></div>
<div class="m2"><p>به مازندران شد به نزدیک شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدرید پهلوی دیو سپید</p></div>
<div class="m2"><p>جگرگاه پولاد غندی و بید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر سنجه را ناگه از تن بکند</p></div>
<div class="m2"><p>خروشش برآمد به ابر بلند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو سهراب فرزند کاندر جهان</p></div>
<div class="m2"><p>کسی را نبود از کهان و مهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بکشت از پی کین کاووس شاه</p></div>
<div class="m2"><p>ز دردش بگرید همی سال و ماه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز آن پس کجا رزم کاموس کرد</p></div>
<div class="m2"><p>به مردی به ابر اندر آورد گرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز کردار او چند رانم سخن</p></div>
<div class="m2"><p>که هم داستانها نیاید به بن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر شاه سیر آمد از تاج و گاه</p></div>
<div class="m2"><p>چه ماند بدین شیردل نیک‌خواه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین داد پاسخ که کردار اوی</p></div>
<div class="m2"><p>به نزدیک ما رنج و تیمار اوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که داند مگر کردگار سپهر</p></div>
<div class="m2"><p>نمایندهٔ کام و آرام و مهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخنهای او نیست اندر نهفت</p></div>
<div class="m2"><p>نداند کس او را به آفاق جفت</p></div></div>