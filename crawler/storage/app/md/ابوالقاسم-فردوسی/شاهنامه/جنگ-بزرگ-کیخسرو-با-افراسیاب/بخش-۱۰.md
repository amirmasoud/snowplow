---
title: >-
    بخش ۱۰
---
# بخش ۱۰

<div class="b" id="bn1"><div class="m1"><p>بفرمود تا پیش او شد دبیر</p></div>
<div class="m2"><p>بیاورد قرطاس و مشک و عبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبشتند نامه به کاووس شاه</p></div>
<div class="m2"><p>چنانچون سزا بود زان رزمگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر نامه کرد از نخست آفرین</p></div>
<div class="m2"><p>ستایش سزای جهان آفرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر گفت شاه جهانبان من</p></div>
<div class="m2"><p>پدروار لرزیده بر جان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزرگیش با کوه پیوسته باد</p></div>
<div class="m2"><p>دل بدسگالان او خسته باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسیدم ز ایران به ریگ فرب</p></div>
<div class="m2"><p>سه جنگ گران کرده شد در سه شب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمار سواران افراسیاب</p></div>
<div class="m2"><p>نبیند خردمند هرگز به خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بریده چو سیصد سر نامدار</p></div>
<div class="m2"><p>فرستادم اینک بر شهریار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برادر بد و خویش و پیوند اوی</p></div>
<div class="m2"><p>گرامی بزرگان و فرزند اوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز آن نامداران بسته دویست</p></div>
<div class="m2"><p>که صد شیر با جنگ هر یک یکیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه رزم بر دشت خوارزم بود</p></div>
<div class="m2"><p>ز چرخ آفرین بر چنان رزم بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برفت او و ما از پس اندر دمان</p></div>
<div class="m2"><p>کشیدیم تا بر چه گردد زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر این رزمگاه آفرین باد گفت</p></div>
<div class="m2"><p>همه ساله با اختر نیک جفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نهادند بر نامه مهری ز مشک</p></div>
<div class="m2"><p>از آن پس گذر کرد بر ریگ خشک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو ز آن رود جیحون شد افراسیاب</p></div>
<div class="m2"><p>چو باد دمان تیز بگذشت آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به پیش سپاه قراخان رسید</p></div>
<div class="m2"><p>همی گفت هر کس ز جنگ آنچه دید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپهدار ترکان چه مایه گریست</p></div>
<div class="m2"><p>بر آن کس که از تخمهٔ او بزیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بهر گرانمایه فرزند خویش</p></div>
<div class="m2"><p>بزرگان و خویشان و پیوند خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خروشی یر آمد تو گفتی که ابر</p></div>
<div class="m2"><p>همی خون چکاند ز چشم هژبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همی بودش اندر بخارا درنگ</p></div>
<div class="m2"><p>همی خواست کایند شیران به جنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آن پس چو گشت انجمن آنچه ماند</p></div>
<div class="m2"><p>بزرگان برتر منش را بخواند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو گشتند پر مایگان انجمن</p></div>
<div class="m2"><p>ز لشکر هر آن کس که بد رای زن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زبان بر گشادند بر شهریار</p></div>
<div class="m2"><p>چو بیچاره شدشان دل از کارزار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که از لشکر ما بزرگان که بود</p></div>
<div class="m2"><p>گذشتند و ز ایشان دل ما شخود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همانا که از صد نماندست بیست</p></div>
<div class="m2"><p>بر آن رفتگان بر بباید گریست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنون ما دل از گنج و فرزند خویش</p></div>
<div class="m2"><p>گسستیم چندی ز پیوند خویش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدان روی جیحون یکی رزمگاه</p></div>
<div class="m2"><p>بکردیم زان پس که فرمود شاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بی دانشی آنچه آمد به روی</p></div>
<div class="m2"><p>تو دانی که شاهی و ما چاره‌جوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر ایدون که روشن بود رای شاه</p></div>
<div class="m2"><p>از ایدر به چاچ اندر آرد سپاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو کیخسرو آید به کین خواستن</p></div>
<div class="m2"><p>بباید تو را لشکر آراستن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو شاه اندر این کار فرمان برد</p></div>
<div class="m2"><p>ز گلزریون نیز هم بگذرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بباشد به آرام به بهشت گنگ</p></div>
<div class="m2"><p>که هم جای جنگست و جای درنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر این بر نهادند یکسر سخن</p></div>
<div class="m2"><p>کسی رای دیگر نیفگند بن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برفتند یکسر به گلزریون</p></div>
<div class="m2"><p>همه دیده پر آب و دل پر ز خون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به گلزریون شاه توران سه روز</p></div>
<div class="m2"><p>ببود و برآسود با باز و یوز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برفتند زان جایگه سوی گنگ</p></div>
<div class="m2"><p>به جایی نبودش فراوان درنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی جای بود آن به سان بهشت</p></div>
<div class="m2"><p>گلش مشک سارا بد و زر خشت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدان جایگه شاد و خندان بخفت</p></div>
<div class="m2"><p>تو گفتی که با ایمنی گشت جفت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سپه خواند از هر سوی بی‌کران</p></div>
<div class="m2"><p>بزرگان گردنکش و مهتران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>می و گلشن و بانگ چنگ و رباب</p></div>
<div class="m2"><p>گل و سنبل و رطل و افراسیاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همی بود تا بر چه گردد جهان</p></div>
<div class="m2"><p>بدین آشکارا چه دارد نهان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو کیخسرو آمد بر این روی آب</p></div>
<div class="m2"><p>از او دور شد خورد و آرام و خواب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپه چون گذر کرد زان سوی رود</p></div>
<div class="m2"><p>فرستاد زان پس به هر کس درود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کز این آمدن کس مدارید باک</p></div>
<div class="m2"><p>بخواهید ما را ز یزدان پاک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گرانمایه گنجی به درویش داد</p></div>
<div class="m2"><p>کسی را کز او شاد بد بیش داد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وز آنجا بیامد سوی شهر سغد</p></div>
<div class="m2"><p>یکی نو جهان دید رسته ز چغد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ببخشید گنجی بر آن شهر نیز</p></div>
<div class="m2"><p>همی خواست کآباد گردد به چیز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به هر منزلی زینهاری سوار</p></div>
<div class="m2"><p>همی آمدندی بر شهریار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از آن پس چو آگاهی آمد به شاه</p></div>
<div class="m2"><p>ز گنگ و ز افراسیاب و سپاه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که آمد به نزدیک او گلگله</p></div>
<div class="m2"><p>ابا لشکری چون هژبر یله</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که از تخم تورست پرکین و درد</p></div>
<div class="m2"><p>بجوید همی روزگار نبرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فرستاد بهری ز گردان به چاج</p></div>
<div class="m2"><p>که جوید همی تخت ترکان و تاج</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سپاهی به سوی بیابان سترگ</p></div>
<div class="m2"><p>فرستاد سالار ایشان طورگ</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پذیرفت ز این هر یکی جنگ شاه</p></div>
<div class="m2"><p>که بر نامداران ببندند راه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جهاندار کیخسرو آن خوار داشت</p></div>
<div class="m2"><p>خرد را به اندیشه سالار داشت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سپاهی که از بردع و اردبیل</p></div>
<div class="m2"><p>بیامد بفرمود تا خیل خیل</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بیایند و بر پیش او بگذرند</p></div>
<div class="m2"><p>رد و موبد و مرزبان بشمرند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>برفتند و سالارشان گستهم</p></div>
<div class="m2"><p>که در جنگ شیران نبودی دژم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همان گفت تا لشکر نیمروز</p></div>
<div class="m2"><p>برفتند با رستم نیوسوز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بفرمود تا بر هیونان مست</p></div>
<div class="m2"><p>نشینند و گیرند اسبان به دست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به سغد اندرون بود یک ماه شاه</p></div>
<div class="m2"><p>همه سغد شد شاه را نیک‌خواه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سپه را درم داد و آسوده کرد</p></div>
<div class="m2"><p>همی جست هنگام روز نبرد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر آن کس که بود از در کارزار</p></div>
<div class="m2"><p>بدانست نیرنگ و بند حصار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بیاورد و با خویشتن یار کرد</p></div>
<div class="m2"><p>سر بدکنش پر ز تیمار کرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وز آن جایگه گردن افراخته</p></div>
<div class="m2"><p>کمر بسته و جنگ را ساخته</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز سغد کشانی سپه بر گرفت</p></div>
<div class="m2"><p>جهانی در او مانده اندر شگفت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>خبر شد به ترکان که آمد سپاه</p></div>
<div class="m2"><p>جهانجوی کیخسرو کینه‌خواه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همه سوی دژها نهادند روی</p></div>
<div class="m2"><p>جهان شد پر از جنبش و گفت و گوی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به لشکر چنین گفت پس شهریار</p></div>
<div class="m2"><p>که امروز به گونه شد کارزار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز ترکان هر آن کس که فرمان کند</p></div>
<div class="m2"><p>دل از جنگ جستن پشیمان کند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مسازید جنگ و مریزید خون</p></div>
<div class="m2"><p>مباشید کس را به بد رهنمون</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>وگر جنگ جوید کسی با سپاه</p></div>
<div class="m2"><p>دل کینه دارش نیاید به راه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>شما را حلال است خون ریختن</p></div>
<div class="m2"><p>به هر جای تاراج و آویختن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به ره بر خورشها مدارید تنگ</p></div>
<div class="m2"><p>مدارید کین و مسازید جنگ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>خروشی بر آمد ز پیش سپاه</p></div>
<div class="m2"><p>که گفتی بدرد همی چرخ و ماه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سواران به دژها نهادند روی</p></div>
<div class="m2"><p>جهان شد پر از غلغل و گفتگوی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هر آن کس که فرمان به جا آورید</p></div>
<div class="m2"><p>سپاه شهنشه بدو ننگرید</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هر آن کو برون شد ز فرمان شاه</p></div>
<div class="m2"><p>سرانشان بریدند یکسر سپاه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز ترکان کس از بیم افراسیاب</p></div>
<div class="m2"><p>لب تشنه نگذاشتندی بر آب</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>وگر باز ماندی کسی ز این سپاه</p></div>
<div class="m2"><p>تن بی سرش یافتندی به راه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دلیران به دژها نهادند روی</p></div>
<div class="m2"><p>به هر دژ که بودی یکی جنگجوی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>شدی بارهٔ دژ هم آنگاه پست</p></div>
<div class="m2"><p>نماندی در و بام وجای نشست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>غلام و پرستنده و چارپای</p></div>
<div class="m2"><p>نماندی بد و نیک چیزی به جای</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بر این گونه فرسنگ بر صد گذشت</p></div>
<div class="m2"><p>نه دژ ماند آباد جایی نه دشت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو آورد لشکر به گلزریون</p></div>
<div class="m2"><p>به هر سو بگردید با رهنمون</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>جهان دید بر سان باغ بهار</p></div>
<div class="m2"><p>در و دشت و کوه و زمین پرنگار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>همه کوه نخچیر و هامون درخت</p></div>
<div class="m2"><p>جهان از در مردم نیک بخت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>طلایه فرستاد و کارآگهان</p></div>
<div class="m2"><p>بدان تا نماند بدی در نهان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سراپردهٔ شهریار جهان</p></div>
<div class="m2"><p>کشیدند بر پیش آب روان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جهاندار بر تخت زرین نشست</p></div>
<div class="m2"><p>خود و نامداران خسروپرست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شبی کرد جشنی که تا روز پاک</p></div>
<div class="m2"><p>همی مرده برخاست از تیره خاک</p></div></div>