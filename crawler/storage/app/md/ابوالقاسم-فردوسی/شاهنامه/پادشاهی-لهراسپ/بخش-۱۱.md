---
title: >-
    بخش ۱۱
---
# بخش ۱۱

<div class="b" id="bn1"><div class="m1"><p>ز میرین یکی بود کهتر به سال</p></div>
<div class="m2"><p>ز گردان رومی برآورده یال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوی بر منش نام او اهرنا</p></div>
<div class="m2"><p>ز تخم بزرگان رویین تنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاد نزدیک قیصر پیام</p></div>
<div class="m2"><p>که دانی که ما را نژادست و نام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز میرین به هر گوهری بگذرم</p></div>
<div class="m2"><p>به تیغ و به گنج درم برترم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من ده کنون دختر کهترت</p></div>
<div class="m2"><p>به من تازه کن لشکر و افسرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین داد پاسخ که پیمان من</p></div>
<div class="m2"><p>شنیدی مگر با جهانبان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که داماد نگزیند این دخترم</p></div>
<div class="m2"><p>ز راه نیاکان خود نگذرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو میرین یکی کار بایدت کرد</p></div>
<div class="m2"><p>ازان پس تو باشی ورا هم نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کوه سقیلا یکی اژدهاست</p></div>
<div class="m2"><p>که کشور همه پاک ازو در بلاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر کم کنی اژدها را ز روم</p></div>
<div class="m2"><p>سپارم ترا دختر و گنج و بوم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که همتای آن گرگ شیراوژنست</p></div>
<div class="m2"><p>دمش زهر و او دام آهرمنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین داد پاسخ که فرمان کنم</p></div>
<div class="m2"><p>بدین آرزو جان گروگان کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز نزدیک قیصر بیامد برون</p></div>
<div class="m2"><p>دلش زان سخن کفته جان پر زخون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به یاران چنین گفت کان زخم گرگ</p></div>
<div class="m2"><p>نبد جز به شمشیر مردی سترگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز میرین کی آید چنین کارکرد</p></div>
<div class="m2"><p>نداند همی قیصر از مرد مرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شوم زو بپرسم بگوید مگر</p></div>
<div class="m2"><p>سخن با من از بی‌پی چاره‌گر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بشد تا به ایوان میرین چوگرد</p></div>
<div class="m2"><p>پرستنده‌ای رفت و آواز کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشستنگهی داشت میرین که ماه</p></div>
<div class="m2"><p>به گردون ندارد چنان جایگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهانجوی با گبر کنداوری</p></div>
<div class="m2"><p>یکی افسری بر سرش قیصری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پرستنده گفت اهرن پیلتن</p></div>
<div class="m2"><p>بیامد به در با یکی انجمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشستنگهی ساخت شایسته‌تر</p></div>
<div class="m2"><p>برفت آنک بودند بایسته‌تر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ایوان میرین نماندند کس</p></div>
<div class="m2"><p>دو مهتر نشستند بر تخت بس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو میرین بدیدش به بر درگرفت</p></div>
<div class="m2"><p>بپرسیدن مهتر اندر گرفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدو گفت اهرن که با من بگوی</p></div>
<div class="m2"><p>ز هرچت بپرسم بهانه مجوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا آرزو دختر قیصرست</p></div>
<div class="m2"><p>کجا روم را سربسر افسرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتیم و پاسخ چنین داد باز</p></div>
<div class="m2"><p>که در کوه با اژدها رزم ساز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر بازگویی تو آن کار گرگ</p></div>
<div class="m2"><p>بوی مر مرا رهنمای بزرگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بشنید میرین ز اهرن سخن</p></div>
<div class="m2"><p>بپژمرد و اندیشه افگند بن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که گر کار آن نامدار جهان</p></div>
<div class="m2"><p>به اهرن بگویم نماند نهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سرمایهٔ مردمی راستیست</p></div>
<div class="m2"><p>ز تاری و کژی بباید گریست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگویم مگر کان نبرده سوار</p></div>
<div class="m2"><p>نهد اژدهار را سر اندر کنار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو اهرن بود مر مرا یار و پشت</p></div>
<div class="m2"><p>ندارد مگر باد دشمن به مشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برآریم گرد از سر آن سوار</p></div>
<div class="m2"><p>نهان ماند این کار یک روزگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به اهرن چنین گفت کز کار گرگ</p></div>
<div class="m2"><p>بگویم چو سوگند یابم بزرگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که این کار هرگز به روز و به شب</p></div>
<div class="m2"><p>نگویی نداری گشاده دو لب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بخورد اهرن آن سخت سوگند اوی</p></div>
<div class="m2"><p>بپذرفت سرتاسر آن بند اوی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو قرطاس را جامهٔ خامه کرد</p></div>
<div class="m2"><p>به هیشوی میرین یکی نامه کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که اهرن که دارد ز قیصر نژاد</p></div>
<div class="m2"><p>جهانجوی با گنج و با تخت و داد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بخواهد ز قیصر همی دختری</p></div>
<div class="m2"><p>که ماندست از دختران کهتری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همی اژدها دام اهرن کند</p></div>
<div class="m2"><p>بکوشد کزان بدنشان تن کند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بیامد به نزدیک من چاره‌جوی</p></div>
<div class="m2"><p>گذشته سخنها گشادم بدوی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ازان گرگ و آن رزم دیده‌سوار</p></div>
<div class="m2"><p>بگفتم همه هرچ آمد به کار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان هم که کار مرا کرد خوب</p></div>
<div class="m2"><p>کند بی‌گمان کار این مرد خوب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دو تن را بدین مرز مهتر کند</p></div>
<div class="m2"><p>چو خورشید را بر سر افسر کند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بیامد دوان اهرن چاره‌جوی</p></div>
<div class="m2"><p>به نزدیک هیشوی بنهاد روی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو اهرن به نزدیک دریا رسید</p></div>
<div class="m2"><p>جهانجوی هیشوی پیشین دوید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ازو بستد آن نامهٔ دلپسند</p></div>
<div class="m2"><p>برو آفرین کرد و بگشاد بند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدو گفت هیشوی کای راد مرد</p></div>
<div class="m2"><p>بیاید کنون او به کردار گرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی نامداری غریب و جوان</p></div>
<div class="m2"><p>فدی کرد بر پیش میرین روان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کنون چون کند رزم نر اژدها</p></div>
<div class="m2"><p>به چاره نیابد مگر زو رها</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا گفتن و کار بر دست اوست</p></div>
<div class="m2"><p>سخن گفتن نیک هرجا نکوست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو امشب بدین میزبان رای کن</p></div>
<div class="m2"><p>بنه شمع و دریا دل‌آرای کن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که فردا بیاید گو نامجوی</p></div>
<div class="m2"><p>بگویم بدو هرچ گویی بگوی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به شمع آب دریا بیاراستند</p></div>
<div class="m2"><p>خورشها بخوردند و می خواستند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چنین تا سپیده ز یاقوت زرد</p></div>
<div class="m2"><p>بزد شید بر شیشهٔ لاژورد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پدید آمد از دشت گرد سوار</p></div>
<div class="m2"><p>ز دورش بدید اهرن نامدار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو تنگ اندر آمد پیاده دوان</p></div>
<div class="m2"><p>پذیره شدش مرد روشن روان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فرود آمد از باره جنگی سوار</p></div>
<div class="m2"><p>می و خوردنی خواست از نامدار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکی تیز بگشاد هیشوی لب</p></div>
<div class="m2"><p>که شادان بدی نامور روز و شب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نگه کن بدین مرد قیصر نژاد</p></div>
<div class="m2"><p>که گردون گردان بدو گشت شاد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هم از تخمهٔ قیصرانست نیز</p></div>
<div class="m2"><p>همش فر و نام و همش گنج و چیز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به دامادی قیصر آمدش رای</p></div>
<div class="m2"><p>همی خواهد اندر سخن رهنمای</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چنو نیست مر قیصران را همال</p></div>
<div class="m2"><p>جوانیست با فر و با برز و یال</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ازو خواست یک‌بار و پاسخ شنید</p></div>
<div class="m2"><p>کنون چارهٔ دیگر آمد پدید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همی گویدش اژدهاگیر باش</p></div>
<div class="m2"><p>گر از خویشی قیصر آژیر باش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به پیش گرانمایگان روز و شب</p></div>
<div class="m2"><p>بجز نام میرین نراند به لب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هرانکس که باشند زیبای بخت</p></div>
<div class="m2"><p>بخواهد که ماند بدو تاج و تخت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>یکی برز کوهست از ایدر نه دور</p></div>
<div class="m2"><p>همه جای خوردن گه کام و سور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یکی اژدها بر سر تیغ کوه</p></div>
<div class="m2"><p>شده مردم روم زو در ستوه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همی ز آسمان کرگس اندر کشد</p></div>
<div class="m2"><p>ز دریا نهنگ دژم برکشد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همی دود زهرش بسوزد زمین</p></div>
<div class="m2"><p>نخواند برین مرز و بوم آفرین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گر آن کشته آید به دست تو بر</p></div>
<div class="m2"><p>شگفتی شوی در جهان سربسر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ازو یاورت پاک یزدان بود</p></div>
<div class="m2"><p>به کام تو خورشید گردان بود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بدین زور و بالا و این دستبرد</p></div>
<div class="m2"><p>ندانیم همتای تو هیچ گرد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بدو گفت رو خنجری کن دراز</p></div>
<div class="m2"><p>ازو دسته بالاش چون پنج باز</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز هر سوش برسان دندان مار</p></div>
<div class="m2"><p>سنانی برو بسته برسان خار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>همی آب داده به زهر و به خون</p></div>
<div class="m2"><p>به تیزی چو الماس و رنگ آب‌گون</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به فرمان یزدان پیروزبخت</p></div>
<div class="m2"><p>نگون اندر آویزمش بر درخت</p></div></div>