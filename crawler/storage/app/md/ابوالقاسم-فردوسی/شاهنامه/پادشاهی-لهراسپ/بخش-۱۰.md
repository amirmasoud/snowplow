---
title: >-
    بخش ۱۰
---
# بخش ۱۰

<div class="b" id="bn1"><div class="m1"><p>چو نزدیک شد بیشه و جای گرگ</p></div>
<div class="m2"><p>بپیچید میرین و مرد سترگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گشتاسپ بنمود به انگشت راست</p></div>
<div class="m2"><p>که آن اژدها را نشیمن کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وزو بازگشتند هر دو به درد</p></div>
<div class="m2"><p>پر از خون دل و دیده پر آب زرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین گفت هیشوی کان سرفراز</p></div>
<div class="m2"><p>دلیرست و دانا و هم رزمساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بترسم بروبر ز چنگال گرگ</p></div>
<div class="m2"><p>که گردد تباه این جوان سترگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گشتاسپ نزدیک آن بیشه شد</p></div>
<div class="m2"><p>دل رزمسازش پر اندیشه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرود آمد از بارهٔ سرفراز</p></div>
<div class="m2"><p>به پیش جهاندار و بردش نماز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی گفت ایا پاک پروردگار</p></div>
<div class="m2"><p>فروزندهٔ گردش روزگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو باشی بدین بد مرا دستگیر</p></div>
<div class="m2"><p>ببخشای بر جان لهراسپ پیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که گر بر من این اژدهای بزرگ</p></div>
<div class="m2"><p>که خواند ورا ناخردمند گرگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود پادشاه چون پدر بشنود</p></div>
<div class="m2"><p>خروشان شود زان سپس نغنود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بماند پر از درد چون بیهشان</p></div>
<div class="m2"><p>به هر کس خروشان و جویا نشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر من شوم زین بد دد ستوه</p></div>
<div class="m2"><p>بپوشم سر از شرم پیش گروه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت این و بر بارگی برنشست</p></div>
<div class="m2"><p>خروشان و جوشان و تیغی به دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کمانی به زه بر به بازو درون</p></div>
<div class="m2"><p>همی رفت بیدار دل پر زخون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز ره چون به تنگ اندر آمد سوار</p></div>
<div class="m2"><p>بغرید برسان ابر بهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو گرگ از در بیشه او را بدید</p></div>
<div class="m2"><p>خروشی به ابر سیه برکشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی کند روی زمین را به چنگ</p></div>
<div class="m2"><p>نه بر گونهٔ شیر و چنگ پلنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو گشتاسپ آن اژدها را بدید</p></div>
<div class="m2"><p>کمان را به زه کرد و اندر کشید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو باد از برش تیرباران گرفت</p></div>
<div class="m2"><p>کمان را چو ابر بهاران گرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دد از تیر گشتاسپی خسته شد</p></div>
<div class="m2"><p>دلیریش با درد پیوسته شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیاسود و برخاست از جای گرگ</p></div>
<div class="m2"><p>بیامد بسان هیون سترگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سرو چون گوزنان به پیش اندرون</p></div>
<div class="m2"><p>تن از زخم پر درد ودل پر زخون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو نزدیک اسپ اندر آمد ز راه</p></div>
<div class="m2"><p>سرونی بزد بر سرین سیاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که از خایه تا ناف او بردرید</p></div>
<div class="m2"><p>جهانجوی تیغ از میان برکشید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیاده بزد بر میان سرش</p></div>
<div class="m2"><p>بدو نیم شد پشت و یال و برش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بیامد به پیش خداوند دد</p></div>
<div class="m2"><p>خداوند هر دانش و نیک و بد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همی آفرین خواند بر کردگار</p></div>
<div class="m2"><p>که ای آفرینندهٔ روزگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تویی راه گم کرده را رهنمای</p></div>
<div class="m2"><p>تویی برتر برترین یک خدای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه کام و پیروزی از کام تست</p></div>
<div class="m2"><p>همه فر و دانایی از نام تست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو برگشت از جایگاه نماز</p></div>
<div class="m2"><p>بکند آن دو دندان که بودش دراز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وزان بیشه تنها سر اندر کشید</p></div>
<div class="m2"><p>همی رفت تا پیش دریا رسید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر آب هیشوی و میرین به درد</p></div>
<div class="m2"><p>نشسته زبانها پر از یاد کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سخنشان ز گشتاسپ بود و ز گرگ</p></div>
<div class="m2"><p>که زارا سوار دلیر و سترگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که اکنون به رزمی بزرگ اندرست</p></div>
<div class="m2"><p>دریده به چنگال گرگ اندرست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو گشتاسپ آمد پیاده پدید</p></div>
<div class="m2"><p>پر از خون و رخ چون گل شنبلید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو دیدنش از جای برخاستند</p></div>
<div class="m2"><p>به زاری خروشیدن آراستند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به زاری گرفتندش اندر کنار</p></div>
<div class="m2"><p>رخان زرد و مژگان چو ابر بهار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که چون بود با گرگ پیکار تو</p></div>
<div class="m2"><p>دل ما پر از خون بد از کار تو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدو گفت گشتاسپ کای نیک رای</p></div>
<div class="m2"><p>به روم اندرون نیست بیم از خدای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بران سان یکی اژدهای دلیر</p></div>
<div class="m2"><p>به کشور بمانند تا سال دیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برآید جهانی شود زو هلاک</p></div>
<div class="m2"><p>چه قیصر مر او را چه یک مشت خاک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به شمشیر سلمش زدم به دو نیم</p></div>
<div class="m2"><p>سرآمد شما را همه ترس و بیم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شوید آن شگفتی ببینید گرم</p></div>
<div class="m2"><p>کزان بیشتر کس ندیدست چرم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکی ژنده پیلست گویی به پوست</p></div>
<div class="m2"><p>همه بیشه بالا و پهنای اوست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بران بیشه رفتند هر دو دوان</p></div>
<div class="m2"><p>ز گفتار او شاد و روشن‌روان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدیدند گرگی به بالای پیل</p></div>
<div class="m2"><p>به چنگال شیران و همرنگ نیل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدو زخم کرده ز سر تا به پای</p></div>
<div class="m2"><p>دو شیرست گویی فتاده به جای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو دیدند کردند زو آفرین</p></div>
<div class="m2"><p>بران فرمند آفتاب زمین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دلی شاد زان بیشه باز آمدند</p></div>
<div class="m2"><p>بر شیر جنگی فراز آمدند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بسی هدیه آورد میرین برش</p></div>
<div class="m2"><p>بر آن‌سان که بد مرد را در خورش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بجز دیگر اسپی نپذرفت زوی</p></div>
<div class="m2"><p>وزانجا سوی خانه بنهاد روی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو آمد ز دریا به آرام خویش</p></div>
<div class="m2"><p>کتایون بینادلش رفت پیش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدو گفت جوشن کجا یافتی</p></div>
<div class="m2"><p>کز ایدر به نخچیر بشتافتی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چنین داد پاسخ که از شهر من</p></div>
<div class="m2"><p>بیامد یکی نامور انجمن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرا هدیه این جوشن و تیغ و خود</p></div>
<div class="m2"><p>بدادند و چندی ز خویشان درود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کتایون می‌آورد همچون گلاب</p></div>
<div class="m2"><p>همی خورد با شوی تا گاه خواب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بخفتند شادان دو اختر گرای</p></div>
<div class="m2"><p>جوانمرد هزمان بجستی ز جای</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدیدی به خواب اندرون رزم گرگ</p></div>
<div class="m2"><p>به کردار نر اژدهای سترگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کتایون بدو گفت امشب چه بود</p></div>
<div class="m2"><p>که هزمان بترسی چنین نابسود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چنین داد پاسخ که من تخت خویش</p></div>
<div class="m2"><p>بدیدم به خواب اختر و بخت خویش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کتایون بدانست کو را نژاد</p></div>
<div class="m2"><p>ز شاهی بود یک‌دل و یک نهاد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بزرگست و با او نگوید همی</p></div>
<div class="m2"><p>ز قیصر بلندی نجوید همی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدو گفت گشتاسپ کای ماهروی</p></div>
<div class="m2"><p>سمن خد و سیمین‌بر و مشکبوی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بیارای تا ما به ایران شویم</p></div>
<div class="m2"><p>از ایدر به جای دلیران شویم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ببینی بر و بوم فرخنده را</p></div>
<div class="m2"><p>همان شاه با داد و بخشنده را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کتایون بدو گفت خیره مگوی</p></div>
<div class="m2"><p>به تیزی چنین راه رفتن مجوی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو ز ایدر به رفتن نهی روی را</p></div>
<div class="m2"><p>هم آواز کن پیش هیشوی را</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مگر بگذراند به کشتی ترا</p></div>
<div class="m2"><p>جهان تازه شد چون گذشتی ترا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>من ایدر بمانم به رنج دراز</p></div>
<div class="m2"><p>ندانم که کی بینمت نیز باز</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به نارفته در جامه گریان شدند</p></div>
<div class="m2"><p>بران آتش درد بریان شدند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو از چرخ بفروخت گردنده شید</p></div>
<div class="m2"><p>جوانان بیداردل پر امید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ازان خانهٔ بزم برخاستند</p></div>
<div class="m2"><p>ز هرگونه‌ای گفتن آراستند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>که تا چون شود بر سر ما سپهر</p></div>
<div class="m2"><p>به تندی گذارد جهان گر به مهر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>وزان روی چون باد میرین برفت</p></div>
<div class="m2"><p>به نزدیک قیصر خرامید تفت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنین گفت کای نامدار بزرگ</p></div>
<div class="m2"><p>به پایان رسید آن زیانهای گرگ</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>همه بیشه سرتابسر اژدهاست</p></div>
<div class="m2"><p>تو نیز ار شگفتی ببینی رواست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بیامد دمان کرد آهنگ من</p></div>
<div class="m2"><p>یکی خنجری یافت از چنگ من</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز سر تا میانش بدو نیم شد</p></div>
<div class="m2"><p>دل دیو زان زخم پر بیم شد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ببالید قیصر ز گفتار اوی</p></div>
<div class="m2"><p>برافروخت پژمرده رخسار اوی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بفرمود تا گاو گردون برند</p></div>
<div class="m2"><p>سراپرده از شهر بیرون برند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>یکی بزمگاهی بیاراستند</p></div>
<div class="m2"><p>می و رود و رامشگران خواستند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ببردند گاوان گردون کشان</p></div>
<div class="m2"><p>بران بیشه کز گرگ بودی نشان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>برفتند ودیدند پیلی ژیان</p></div>
<div class="m2"><p>به خنجر بریده ز سر تا میان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو بیرون کشیدندش از مرغزار</p></div>
<div class="m2"><p>به گاوان گردون‌کش تاودار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>جهانی نظاره بران پیر گرگ</p></div>
<div class="m2"><p>چه گرگ آن ژیان نره شیر سترگ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چو قیصر بدید آن تن پیل مست</p></div>
<div class="m2"><p>ز شادی بسی دست بر زد به دست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>همان روز قیصر سقف را بخواند</p></div>
<div class="m2"><p>به ایوان و دختر به میرین رساند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نوشتند نامه بهر کشوری</p></div>
<div class="m2"><p>سکوبا و بطریق و هر مهتری</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که میرین شیر آن سرافرازم روم</p></div>
<div class="m2"><p>ز گرگ دلاور تهی کرد بوم</p></div></div>