---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>چو یک هفته گرگین بره‌بر بپای</p></div>
<div class="m2"><p>همی بود و بیژن نیامد بجای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر سوش پویان بجستن گرفت</p></div>
<div class="m2"><p>رخان را بخوناب شستن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پشیمانی آمدش زان کار خویش</p></div>
<div class="m2"><p>که چون بد سگالید بر یار خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشد تازیان تا بدان جشنگاه</p></div>
<div class="m2"><p>کجا بیژن گیو گم کرد راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه بیشه برگشت و کس را ندید</p></div>
<div class="m2"><p>نه نیز اندرو بانگ مرغان شنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی گشت بر گرد آن مرغزار</p></div>
<div class="m2"><p>همی یار کرد اندرو خواستار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکایک ز دور اسب بیژن بدید</p></div>
<div class="m2"><p>که آمد ازان مرغزاران پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گسسته لگام و نگون کرده زین</p></div>
<div class="m2"><p>فرو مانده بر جای اندوهگین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدانست کو را تباهست کار</p></div>
<div class="m2"><p>بایران نیاید بدین روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر دار دارد اگر چاه و بند</p></div>
<div class="m2"><p>از افراسیاب آمدستش گزند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمند اندرافگند و برگاشت روی</p></div>
<div class="m2"><p>ز کرده پشیمان و دل جفت جوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازان مرغزار اسب بیژن براند</p></div>
<div class="m2"><p>بخیمه در آورد و روزی بماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس آنگه سوی شهر ایران شتافت</p></div>
<div class="m2"><p>شب و روز آرام و خوردن نیافت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آگاهی آمد ز گرگین بشاه</p></div>
<div class="m2"><p>که بیژن نبودست با او براه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفت این سخن گیو را شهریار</p></div>
<div class="m2"><p>بدان تا ز گرگین کند خواستار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس آگاهی آمد همانگه بگیو</p></div>
<div class="m2"><p>ز گم بودن رزمزن پور نیو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز خانه بیامد دمان تا بکوی</p></div>
<div class="m2"><p>دل از درد خسته پر از آب روی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی گفت بیژن نیامد همی</p></div>
<div class="m2"><p>بارمان ندانم چه ماند همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بفرمود تا بور کشواد را</p></div>
<div class="m2"><p>کجا داشتی روز فریاد را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بروبر نهادند زین خدنگ</p></div>
<div class="m2"><p>گرفته بدل گیو کین پلنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همانگه بدو اندر آورد پای</p></div>
<div class="m2"><p>بکردار باد اندر آمد ز جای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پذیره شدش تا کند خواستار</p></div>
<div class="m2"><p>که بیژن کجا ماند و چون بود کار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی گفت گرگین بدو ناگهان</p></div>
<div class="m2"><p>همانا بدی ساخت اندر نهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شوم گر ببینمش بی بیژنم</p></div>
<div class="m2"><p>همانگه سرش را ز تن بر کنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیامد چو گرگین مر او را بدید</p></div>
<div class="m2"><p>پیاده شد و پیش او در دوید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی گشت غلتان بخاک اندرا</p></div>
<div class="m2"><p>شخوده رخان و برهنه سرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بپرسید و گفت ای گزین سپاه</p></div>
<div class="m2"><p>سپهدار سالار و خورشید گاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پذیره بدین راه چون آمدی</p></div>
<div class="m2"><p>که با دیدگان پر ز خون آمدی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا جان شیرین نباید همی</p></div>
<div class="m2"><p>کنون خوارتر گر برآید همی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو چشمم بروی تو آید ز شرم</p></div>
<div class="m2"><p>بپالایم از دیدگان آب گرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون هیچ مندیش کو را بجان</p></div>
<div class="m2"><p>نیامد گزند و بگویم نشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو اسب پسر دید گرگین بدست</p></div>
<div class="m2"><p>پر از خاک و آسیمه برسان مست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو گفتار گرگینش آمد بگوش</p></div>
<div class="m2"><p>ز اسب اندر افتاد و زو رفت هوش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخاک اندرون شد سرش ناپدید</p></div>
<div class="m2"><p>همه جامهٔ پهلوی بردرید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی کند موی از سر و ریش پاک</p></div>
<div class="m2"><p>خروشان بسر بر همی ریخت خاک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همی گفت کای کردگار سپهر</p></div>
<div class="m2"><p>تو گستردی اندر دلم هوش و مهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر از من جدا ماند فرزند من</p></div>
<div class="m2"><p>روا دارم ار بگسلد بند من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>روانم بدان جای نیکان بری</p></div>
<div class="m2"><p>ز درد دل من تو آگه‌تری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا خود ز گیتی هم او بود و بس</p></div>
<div class="m2"><p>چه انده گسار و چه فریادرس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنون بخت بد کردش از من جدا</p></div>
<div class="m2"><p>بماندم چنین در جهان مبتلا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز گرگین پس آنگه سخن بازجست</p></div>
<div class="m2"><p>که چون بود خود روزگار از نخست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زمانه بجایش کسی برگزید</p></div>
<div class="m2"><p>وگر خود ز چشم تو شد ناپدید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز بدها چه آمد مر او را بگوی</p></div>
<div class="m2"><p>چه افگند بند سپهرش بروی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه دیو آمدش پیش در مرغزار</p></div>
<div class="m2"><p>که او را تبه کرد و برگشت کار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو این مرده‌ری اسب چون یافتی</p></div>
<div class="m2"><p>ز بیژن کجا روی برتافتی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو گفت گرگین که بازآر هوش</p></div>
<div class="m2"><p>سخن بشنو و پهن بگشای گوش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که این کار چون بود و کردار چون</p></div>
<div class="m2"><p>بدان بیشه با خوک پیکار چون</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بدان پهلوانا و آگاه باش</p></div>
<div class="m2"><p>همیشه فروزندهٔ گاه باش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برفتیم ز ایدر بجنگ گراز</p></div>
<div class="m2"><p>رسیدیم نزدیک ارمان فراز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی بیشه دیدیم کرده چو دست</p></div>
<div class="m2"><p>درختان بریده چراگاه پست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه جای گشته کنام گراز</p></div>
<div class="m2"><p>همه شهر ارمان از آن در کزاز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو ما جنگ را نیزه برگاشتیم</p></div>
<div class="m2"><p>ببیشه درون بانگ برداشتیم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گراز اندر آمد بکردار کوه</p></div>
<div class="m2"><p>نه یک یک بهر جای گشته گروه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بکردیم جنگی بکردار شیر</p></div>
<div class="m2"><p>بشد روز و نامد دل از جنگ سیر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو پیلان بهم بر فگندیمشان</p></div>
<div class="m2"><p>بمسمار دندان بکندیمشان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وزآنجا بایران نهادیم روی</p></div>
<div class="m2"><p>همه راه شادان و نخچیر جوی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>برآمد یکی گور زان مرغزار</p></div>
<div class="m2"><p>کزان خوبتر کس نبیند نگار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بکردار گلگون گودرز موی</p></div>
<div class="m2"><p>چو خنگ شباهنگ فرهاد روی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو سیمش دو پا و چو پولاد سم</p></div>
<div class="m2"><p>چو شبرنگ بیژن سر و گوش و دم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بگردن چو شیر و برفتن چو باد</p></div>
<div class="m2"><p>تو گفتی که از رخش دارد نژاد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بر بیژن آمد چو پیلی نژند</p></div>
<div class="m2"><p>برو اندر افگند بیژن کمند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فگندن همان بود و رفتن همان</p></div>
<div class="m2"><p>دوان گور و بیژن پس اندر دمان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز تازیدن گور و گرد سوار</p></div>
<div class="m2"><p>برآمد یکی دود زان مرغزار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بکردار دریا زمین بردمید</p></div>
<div class="m2"><p>کمندافگن و گور شد ناپدید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پی اندر گرفتم همه دشت و کوه</p></div>
<div class="m2"><p>که از تاختن شد سمندم ستوه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز بیژن ندیدم بجایی نشان</p></div>
<div class="m2"><p>جزین اسب و زین از پس ایدر کشان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دلم شد پر آتش ز تیمار اوی</p></div>
<div class="m2"><p>که چون بود با گور پیکار اوی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بماندم فراوان بر آن مرغزار</p></div>
<div class="m2"><p>همی کردمش هر سوی خواستار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ازو باز گشتم چنین ناامید</p></div>
<div class="m2"><p>که گور ژیان بود و دیو سپید</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو بشنید گیو این سخن هوشیار</p></div>
<div class="m2"><p>بدانست کو را تباهست کار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز گرگین سخن سربسر خیره دید</p></div>
<div class="m2"><p>همی چشمش از روی او تیره دید</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>رخش زرد از بیم سالار شاه</p></div>
<div class="m2"><p>سخن لرزلرزان و دل پر گناه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو فرزند را گیو گم بوده دید</p></div>
<div class="m2"><p>سخن را برآنگونه آلوده دید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ببرد اهرمن گیو را دل ز جای</p></div>
<div class="m2"><p>همی خواست کو را درآرد ز پای</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بخواهد ازو کین پور گزین</p></div>
<div class="m2"><p>وگر چند نیک آید او را ازین</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>پس اندیشه کرد اندران بنگرید</p></div>
<div class="m2"><p>نیامد همی روشنایی پدید</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چه آید مرا گفت از کشتنا</p></div>
<div class="m2"><p>مگر کام بدگوهر آهرمنا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به بیژن چه سود آید از جان اوی</p></div>
<div class="m2"><p>دگرگونه سازیم درمان اوی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بباشیم تا زین سخن نزد شاه</p></div>
<div class="m2"><p>شود آشکارا ز گرگین گناه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ازو کین کشیدن بسی کار نیست</p></div>
<div class="m2"><p>سنان مرا پیش دیوار نیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بگرگین یکی بانگ برزد بلند</p></div>
<div class="m2"><p>که ای بدکنش ریمن پرگزند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تو بردی ز من شید و ماه مرا</p></div>
<div class="m2"><p>گزین سواران و شاه مرا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>فگندی مرا در تک و پوی پوی</p></div>
<div class="m2"><p>بگرد جهان اندرون چاره‌جوی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>پس اکنون بدستان و بند و فریب</p></div>
<div class="m2"><p>کجا یابی آرام و خواب و شکیب</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نباشد ترا بیش ازین دستگاه</p></div>
<div class="m2"><p>کجا من ببینم یکی روی شاه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>پس آنگه بخواهم ز تو کین خویش</p></div>
<div class="m2"><p>ز بهر گرامی جهانبین خویش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>وز آنجا بیامد بنزدیک شاه</p></div>
<div class="m2"><p>دو دیده پر از خون و دل کینه‌خواه</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>برو آفرین کرد کای شهریار</p></div>
<div class="m2"><p>همیشه جهان را بشادی گذار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>انوشه جهاندار نیک اخترا</p></div>
<div class="m2"><p>نبینی که بر سر چه آمد مرا</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز گیتی یکی پور بودم جوان</p></div>
<div class="m2"><p>شب و روز بودم بدوبر نوان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بجانش پر از بیم گریان بدم</p></div>
<div class="m2"><p>ز درد جداییش بریان بدم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>کنون آمد ای شاه گرگین ز راه</p></div>
<div class="m2"><p>زبان پر ز یافه روان پر گناه</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بدآگاهی آورد از پور من</p></div>
<div class="m2"><p>ازان نامور پاک دستور من</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>یکی اسب دیدم نگونسار زین</p></div>
<div class="m2"><p>ز بیژن نشانی ندارد جزین</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>اگر داد بیند بدین کار ما</p></div>
<div class="m2"><p>یکی بنگرد ژرف سالار ما</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ز گرگین دهد داد من شهریار</p></div>
<div class="m2"><p>کزو گشتم اندر جهان خاکسار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>غمی شد ز درد دل گیو شاه</p></div>
<div class="m2"><p>برآشفت و بنهاد فرخ کلاه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>رخ شاه بر گاه بی‌رنگ شد</p></div>
<div class="m2"><p>ز تیمار بیژن دلش تنگ شد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بگیو آنگهی گفت گرگین چه گفت</p></div>
<div class="m2"><p>چه گوید کجا ماند از نیک جفت</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز گفتار گرگین پس آنگاه گیو</p></div>
<div class="m2"><p>سخن گفت با خسرو از پور نیو</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو از گیو بشنید خسرو سخن</p></div>
<div class="m2"><p>بدو گفت مندیش و زاری مکن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>که بیژن بجانست خرسند باش</p></div>
<div class="m2"><p>بر امید گم بوده فرزند باش</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>که ایدون شنیدستم از موبدان</p></div>
<div class="m2"><p>ز بیدار دل نامور بخردان</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>که من با سواران ایران بجنگ</p></div>
<div class="m2"><p>سوی شهر توران شوم بی‌درنگ</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بکین سیاوش کشم لشکرا</p></div>
<div class="m2"><p>بپیلان سرآرم از آن کشورا</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بدان کینه اندر بود بیژنا</p></div>
<div class="m2"><p>همی رزم جوید چو آهرمنا</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تو دل را بدین کار غمگین مدار</p></div>
<div class="m2"><p>من این را همانا بسم خواستار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بشد گیو یکدل پر اندوه و درد</p></div>
<div class="m2"><p>دو دیده پر از آب و رخساره زرد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو گرگین بدرگاه خسرو رسید</p></div>
<div class="m2"><p>ز گردان در شاه پردخته دید</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز تیمار بیژن همه مهتران</p></div>
<div class="m2"><p>ز درگاه با گیو رفته سران</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>همه پر ز درد و همه پر زرنج</p></div>
<div class="m2"><p>همه همچو گم کرده صد گونه گنج</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>پراگنده رای و پراگنده دل</p></div>
<div class="m2"><p>همه خاک ره ز اشک کرده چو گل</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>وزین روی گرگین شوریده رفت</p></div>
<div class="m2"><p>بنزدیک ایوان درگاه تفت</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چو در پیش کیخسرو آمد زمین</p></div>
<div class="m2"><p>ببوسید و بر شاه کرد آفرین</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو الماس دندانهای گراز</p></div>
<div class="m2"><p>بر تخت بنهاد و بردش نماز</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>که خسرو بهر کار پیروز باد</p></div>
<div class="m2"><p>همه روزگارش چو نوروز باد</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>سر دشمنان تو بادا بگاز</p></div>
<div class="m2"><p>بریده چنان کان سران گراز</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بدندانها چون نگه کرد شاه</p></div>
<div class="m2"><p>بپرسید و گفتش که چون بود راه</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>کجا ماند از تو جدا بیژنا</p></div>
<div class="m2"><p>بروبر چه بد ساخت آهرمنا</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو خسرو چنین گفت گرگین بجای</p></div>
<div class="m2"><p>فرو ماند خیره همیدون بپای</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ندانست پاسخ چه گوید بدوی</p></div>
<div class="m2"><p>فروماند بر جای بر زرد روی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>زبان پر ز یافه روان پر گناه</p></div>
<div class="m2"><p>رخان زرد و لرزان تن از بیم شاه</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چو گفتارها یک بدیگر نماند</p></div>
<div class="m2"><p>برآشفت وز پیش تختش براند</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>همش خیره سر دید هم بدگمان</p></div>
<div class="m2"><p>بدشنام بگشاد خسرو زبان</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بدو گفت نشنیدی آن داستان</p></div>
<div class="m2"><p>که دستان زدست از گه باستان</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>که گر شیر با کین گودرزیان</p></div>
<div class="m2"><p>بسیچد تنش را سر آید زمان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>اگر نیستی از پی نام بد</p></div>
<div class="m2"><p>وگر پیش یزدان سرانجام بد</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بفرمودمی تا سرت را ز تن</p></div>
<div class="m2"><p>بکندی بکردار مرغ اهرمن</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بفرمود خسرو بپولادگر</p></div>
<div class="m2"><p>که بندگران ساز و مسمارسر</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>هم اندر زمان پای کردش ببند</p></div>
<div class="m2"><p>که از بند گیرد بداندیش پند</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بگیو آنگهی گفت بازآر هوش</p></div>
<div class="m2"><p>بجویش بهر جای و هر سو بکوش</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>من اکنون ز هر سو فراوان سپاه</p></div>
<div class="m2"><p>فرستم بجویم بهر جا نگاه</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ز بیژن مگر آگهی یابما</p></div>
<div class="m2"><p>بدین کار هشیار بشتابما</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>وگر دیر یابیم زو آگهی</p></div>
<div class="m2"><p>تو جای خرد را مگردان تهی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بمان تا بیاید مه فرودین</p></div>
<div class="m2"><p>که بفروزد اندر جهان هور دین</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>بدانگه که بر گل نشاندت باد</p></div>
<div class="m2"><p>چو بر سر همی گل فشاندت باد</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>زمین چادر سبز در پوشدا</p></div>
<div class="m2"><p>هوا بر گلان زار بخروشدا</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بهرسو شود پاک فرمان ما</p></div>
<div class="m2"><p>پرستش که فرمود یزدان ما</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>بخواهم من آن جام گیتی نمای</p></div>
<div class="m2"><p>شوم پیش یزدان بباشم بپای</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>کجا هفت کشور بدو اندرا</p></div>
<div class="m2"><p>ببینم بر و بوم هر کشورا</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>کنم آفرین بر نیاکان خویش</p></div>
<div class="m2"><p>گزیده جهاندار و پاکان خویش</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>بگویم ترا هر کجا بیژنست</p></div>
<div class="m2"><p>بجام اندرون این مرا روشنست</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>چو بشنید گیو این سخن شاد شد</p></div>
<div class="m2"><p>ز تیمار فرزند آزاد شد</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بخندید و بر شاه کرد آفرین</p></div>
<div class="m2"><p>که بی‌تو مبادا زمان و زمین</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بکام تو بادا سپهر بلند</p></div>
<div class="m2"><p>بجان تو هرگز مبادا گزند</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ز نیکی دهش بر تو باد آفرین</p></div>
<div class="m2"><p>که بر تو برازد کلاه و نگین</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>چو گیو از بر گاه خسرو برفت</p></div>
<div class="m2"><p>ز هر سو سواران فرستاد تفت</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بجستن گرفتند گرد جهان</p></div>
<div class="m2"><p>که یابد مگر زو بجایی نشان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>همه شهر ارمان و تورانیان</p></div>
<div class="m2"><p>سپردند و نامد ز بیژن نشان</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>چو نوروز فرخ فراز آمدش</p></div>
<div class="m2"><p>بدان جام روشن نیاز آمدش</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>بیامد پر امید دل پهلوان</p></div>
<div class="m2"><p>ز بهر پسر گوژ گشته نوان</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>چو خسرو رخ گیو پژمرده دید</p></div>
<div class="m2"><p>دلش را بدرد اندر آزرده دید</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>بیامد بپوشید رومی قبای</p></div>
<div class="m2"><p>بدان تا بود پیش یزدان بپای</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>خروشید پیش جهان آفرین</p></div>
<div class="m2"><p>بخورشید بر چند برد آفرین</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>ز فریادرس زور و فریاد خواست</p></div>
<div class="m2"><p>از آهرمن بدکنش داد خواست</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>خرامان ازان جا بیامد بگاه</p></div>
<div class="m2"><p>بسر بر نهاد آن خجسته کلاه</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>یکی جام بر کف نهاده نبید</p></div>
<div class="m2"><p>بدو اندرون هفت کشور پدید</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>زمان و نشان سپهر بلند</p></div>
<div class="m2"><p>همه کرده پیدا چه و چون و چند</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>ز ماهی بجام اندون تا بره</p></div>
<div class="m2"><p>نگاریده پیکر همه یکسره</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>چو کیوان و بهرام و ناهید و شیر</p></div>
<div class="m2"><p>چو خورشید و تیر از بر و ماه زیر</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>همه بودنیها بدو اندرا</p></div>
<div class="m2"><p>بدیدی جهاندار افسونگرا</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>نگه کرد و پس جام بنهاد پیش</p></div>
<div class="m2"><p>بدید اندرو بودنیها ز بیش</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>بهر هفت کشور همی بنگرید</p></div>
<div class="m2"><p>ز بیژن بجایی نشانی ندید</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>سوی کشور گرگساران رسید</p></div>
<div class="m2"><p>بفرمان یزدان مر او را بدید</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>بچاهی ببسته ببند گران</p></div>
<div class="m2"><p>ز سختی همی مرگ جست اندران</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>یکی دختری از نژاد کیان</p></div>
<div class="m2"><p>ز بهر زوارش ببسته میان</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>سوی گیو کرد آنگهی روی شاه</p></div>
<div class="m2"><p>بخندید و رخشنده شد پیشگاه</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>که زندست بیژن دلت شاد دار</p></div>
<div class="m2"><p>ز هر بد تن مهتر آزاد دار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>نگر غم نداری بزندان و بند</p></div>
<div class="m2"><p>ازان پس که بر جانش نامد گزند</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>که بیژن بتوران ببند اندرست</p></div>
<div class="m2"><p>زوارش یکی نامور دخترست</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>ز بس رنج و سختی و تیمار اوی</p></div>
<div class="m2"><p>پر از درد گشتم من از کار اوی</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>بدان سان گذارد همی روزگار</p></div>
<div class="m2"><p>که هزمان بروبر بگرید زوار</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>ز پیوند و خویشان شده ناامید</p></div>
<div class="m2"><p>گرازنده بر سان یک شاخ بید</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>دو چشمش پر از خون و دل پر ز درد</p></div>
<div class="m2"><p>زبانش ز خویشان پر از یاد کرد</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>چو ابر بهاران ببارندگی</p></div>
<div class="m2"><p>همی مرگ جوید بدان زندگی</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>بدین چاره اکنون که جنبد ز جای</p></div>
<div class="m2"><p>که خیزد میان بسته این را بپای</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>که دارد بدین کار ما را وفا</p></div>
<div class="m2"><p>که آرد ز سختی مر او را رها</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>نشاید جز از رستم تیز چنگ</p></div>
<div class="m2"><p>که از ژرف دریا برآرد نهنگ</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>کمر بند و برکش سوی نیمروز</p></div>
<div class="m2"><p>شب از رفتن راه مأسا و روز</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>ببر نامهٔ من بر رستما</p></div>
<div class="m2"><p>مزن داستان را بره ‌بردما</p></div></div>