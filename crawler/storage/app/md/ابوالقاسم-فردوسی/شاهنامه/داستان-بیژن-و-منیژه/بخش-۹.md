---
title: >-
    بخش ۹
---
# بخش ۹

<div class="b" id="bn1"><div class="m1"><p>چو سالار نوبت بیامد بدر</p></div>
<div class="m2"><p>بشبگیر بستند گردان کمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه نیزه داران جنگ آوران</p></div>
<div class="m2"><p>همه مرزبانان ناماوران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه نیزه و تیر بار هیون</p></div>
<div class="m2"><p>همه جنگ را دست شسته بخون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپیده دمان گاه بانگ خروس</p></div>
<div class="m2"><p>ببستند بر کوههٔ پیل کوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تهمتن بیامد چو سرو بلند</p></div>
<div class="m2"><p>بچنگ اندرون گرز و بر زین کمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپاه از پس پشت و گردان ز پیش</p></div>
<div class="m2"><p>نهاده بکف بر همه جان خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برفت از در شاه با لشکرش</p></div>
<div class="m2"><p>بسی آفرین خواند برکشورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نزدیکی مرز توران رسید</p></div>
<div class="m2"><p>سران را ز لشکر همه برگزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلشکر چنین گفت پس پهلوان</p></div>
<div class="m2"><p>که ایدر بباشید روشن روان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجنبید از ایدر مگر جان من</p></div>
<div class="m2"><p>ز تن بگسلد پاک یزدان من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسیچیده باشید مر جنگ را</p></div>
<div class="m2"><p>همه تیز کرده بخون چنگ را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپه بر سر مرز ایران بماند</p></div>
<div class="m2"><p>خود و سرکشان سوی توران براند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه جامه برسان بازارگان</p></div>
<div class="m2"><p>بپوشید و بگشاد بند از میان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گشادند گردان کمرهای سیم</p></div>
<div class="m2"><p>بپوشیدشان جامه های گلیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوی شهر توران نهادند روی</p></div>
<div class="m2"><p>یکی کاروانی پر از رنگ و بوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرانمایه هفت اسب با کاروان</p></div>
<div class="m2"><p>یکی رخش و دیگر نشست گوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صد اشتر همه بار او گوهرا</p></div>
<div class="m2"><p>صد اشتر همه جامهٔ لشکرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بس‌های و هوی و درنگ درای</p></div>
<div class="m2"><p>بکردار تهمورثی کرنای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی شهر بر شهر هودج کشید</p></div>
<div class="m2"><p>همی رفت تا شهر توران رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو آمد بنزدیک شهر ختن</p></div>
<div class="m2"><p>نظاره بیامد برش مرد و زن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه پهلوانان توران بجای</p></div>
<div class="m2"><p>شده پیش پیران ویسه بپای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو پیران ویسه ز نخچیر گاه</p></div>
<div class="m2"><p>بیامد تهمتن بدیدش براه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی جام زرین پر از گوهرا</p></div>
<div class="m2"><p>بدیبا بپوشید رستم سرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ده اسب گرانمایه با زیورش</p></div>
<div class="m2"><p>بدیبا بیاراست اندر خورش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بفرمانبران داد و خود پیش رفت</p></div>
<div class="m2"><p>بدرگاه پیران خرامید تفت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برو آفرین کرد کای نامور</p></div>
<div class="m2"><p>بایران و توران ببخت و هنر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان کرد رویش جهاندار ساز</p></div>
<div class="m2"><p>که پیران مر او را ندانست باز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بپرسید و گفت از کجایی بگوی</p></div>
<div class="m2"><p>چه مردی و چون آمدی پوی پوی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدو گفت رستم ترا کهترم</p></div>
<div class="m2"><p>بشهر تو کرد ایزد آبشخورم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ببازارگانی ز ایران بتور</p></div>
<div class="m2"><p>بپیمودم این راه دشوار و دور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فروشنده‌ام هم خریدار نیز</p></div>
<div class="m2"><p>فروشم بخرم ز هر گونه چیز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بمهر تو دارم روان را نوید</p></div>
<div class="m2"><p>چنین چیره شد بر دلم بر امید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر پهلوان گیردم زیر بر</p></div>
<div class="m2"><p>خرم چارپای و فروشم گهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم از داد تو کس نیازاردم</p></div>
<div class="m2"><p>هم از ابر مهرت گهر باردم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس آن جام پر گوهر شاهوار</p></div>
<div class="m2"><p>میان کیان کرد پیشش نثار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرانمایه اسبان تازی‌نژاد</p></div>
<div class="m2"><p>که بر مویشان گرد نفشاند باد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بسی آفرین کرد و آن خواسته</p></div>
<div class="m2"><p>بدو داد و شد کار آراسته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو پیران بدان گوهران بنگرید</p></div>
<div class="m2"><p>کزان جام رخشنده آمد پدید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برو آفرین کرد وبنواختش</p></div>
<div class="m2"><p>بران تخت پیروزه بنشاختش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که رو شاد و ایمن بشهر اندرا</p></div>
<div class="m2"><p>کنون نزد خویشت بسازیم جا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کزین خواسته بر تو تیمار نیست</p></div>
<div class="m2"><p>کسی را بدین با تو پیکار نیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برو هرچ داری بهایی بیار</p></div>
<div class="m2"><p>خریدار کن هر سوی خواستار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرود آی در خان فرزند من</p></div>
<div class="m2"><p>چنان باش با من که پیوند من</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو گفت رستم که ای پهلوان</p></div>
<div class="m2"><p>هم ایدر بباشیم با کاروان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که با ما ز هر گونه مردم بود</p></div>
<div class="m2"><p>نباید که زان گوهری گم بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو گفت رو برزو گیر جای</p></div>
<div class="m2"><p>کنم رهنمایی بپیشت بپای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یکی خانه بگزید و بر ساخت کار</p></div>
<div class="m2"><p>بکلبه درون رخت بنهاد و بار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خبر شد کز ایران یکی کاروان</p></div>
<div class="m2"><p>بیامد بر نامور پهلوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز هر سو خریدار بنهاد گوش</p></div>
<div class="m2"><p>چو آگاهی آمد ز گوهر فروش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خریدار دیبا و فرش و گهر</p></div>
<div class="m2"><p>بدرگاه پیران نهادند سر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو خورشید گیتی بیاراستی</p></div>
<div class="m2"><p>بدان کلبه بازار برخاستی</p></div></div>