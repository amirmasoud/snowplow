---
title: >-
    بخش ۴۵
---
# بخش ۴۵

<div class="b" id="bn1"><div class="m1"><p>ازان پس به هرسو یکی نامه کرد</p></div>
<div class="m2"><p>به جایی که درویش بد جامه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپرسید هرجا که بی‌رنج کیست</p></div>
<div class="m2"><p>به هرجای درویش و بی‌گنج کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کار جهان یکسر آگه کنید</p></div>
<div class="m2"><p>دلم را سوی روشنی ره کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیامدش پاسخ ز هر کشوری</p></div>
<div class="m2"><p>ز هر نامداری و هر مهتری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که آباد بینیم روی زمین</p></div>
<div class="m2"><p>به هرجای پیوسته شد آفرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر مرد درویش کز شهریار</p></div>
<div class="m2"><p>بنالد همی از بد روزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که چون می گسارد توانگر همی</p></div>
<div class="m2"><p>به سر بر ز گل دارد افسر همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آواز رامشگران می خورند</p></div>
<div class="m2"><p>چو ما مردمان را به کس نشمرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تهی دست بی‌رود و گل می خورد</p></div>
<div class="m2"><p>توانگر همانا ندارد خرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخندید زان نامه بیدار شاه</p></div>
<div class="m2"><p>هیونی برافگند پویان به راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به نزدیک شنگل فرستاد کس</p></div>
<div class="m2"><p>چنین گفت کای شاه فریادرس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازان لوریان برگزین ده هزار</p></div>
<div class="m2"><p>نر و ماده بر زخم بربط سوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ایران فرستش که رامشگری</p></div>
<div class="m2"><p>کند پیش هر کهتری بهتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو برخواند آن نامه شنگل تمام</p></div>
<div class="m2"><p>گزین کرد زان لوریان به نام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ایران فرستاد نزدیک شاه</p></div>
<div class="m2"><p>چنان کان بود در خور نیک‌خواه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو لوری بیامد به درگاه شاه</p></div>
<div class="m2"><p>بفرمود تا برگشادند راه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هریک یکی گاو داد و خری</p></div>
<div class="m2"><p>ز لوری همی ساخت برزیگری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان نیز خروار گندم هزار</p></div>
<div class="m2"><p>بدیشان سپرد آنک بد پایدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان تا بورزد به گاو و به خر</p></div>
<div class="m2"><p>ز گندم کند تخم و آرد به بر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کند پیش درویش رامشگری</p></div>
<div class="m2"><p>چو آزادگان را کند کهتری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بشد لوری و گاو و گندم بخورد</p></div>
<div class="m2"><p>بیامد سر سال رخساره زرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفت شاه این نه کار تو بود</p></div>
<div class="m2"><p>پراگندن تخم و کشت و درود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خری ماند اکنون بنه برنهید</p></div>
<div class="m2"><p>بسازید رود و بریشم دهید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون لوری از پاک گفتار اوی</p></div>
<div class="m2"><p>همی گردد اندر جهان چاره‌جوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سگ و کبک بفزود بر گفت شاه</p></div>
<div class="m2"><p>شب و روز پویان به دزدی به راه</p></div></div>