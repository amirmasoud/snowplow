---
title: >-
    بخش ۳۴
---
# بخش ۳۴

<div class="b" id="bn1"><div class="m1"><p>ز بهرام شنگل شد اندرگمان</p></div>
<div class="m2"><p>که این فر و این برز و تیر و کمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نماند همی این فرستاده را</p></div>
<div class="m2"><p>نه هندی نه ترکی نه آزاده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر خویش شاهست گر مهترست</p></div>
<div class="m2"><p>برادرش خوانم هم اندر خورست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخندید و بهرام را گفت شاه</p></div>
<div class="m2"><p>که ای پرهنر با گهر پیشگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برادر توی شاه را بی‌گمان</p></div>
<div class="m2"><p>بدین بخشش و زور و تیر و کمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که فر کیان داری و زور شیر</p></div>
<div class="m2"><p>نباشی مگر نامداری دلیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفت بهرام کای شاه هند</p></div>
<div class="m2"><p>فرستادگان را مکن ناپسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه از تخمهٔ یزدگردم نه شاه</p></div>
<div class="m2"><p>برادرش خوانیم باشد گناه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ایران یکی مرد بیگانه‌ام</p></div>
<div class="m2"><p>نه دانش پژوهم نه فرزانه‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا بازگردان که دورست راه</p></div>
<div class="m2"><p>نباید که یابد مرا خشم شاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدو گفت شنگل که تندی مکن</p></div>
<div class="m2"><p>که با تو هنوزست ما را سخن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبایدت کردن به رفتن شتاب</p></div>
<div class="m2"><p>که رفتن به زودی نباشد صواب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر ما بباش و دل آرام گیر</p></div>
<div class="m2"><p>چو پخته نخواهی می خام گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس‌انگاه دستور را پیش خواند</p></div>
<div class="m2"><p>ز بهرام با او سخن چند راند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر این مرد بهرام را خویش نیست</p></div>
<div class="m2"><p>گر از پهلوان نام او بیش نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو گویی دهد او تن‌اندر فریب</p></div>
<div class="m2"><p>گر از گفت من در دل آرد نهیب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو گویی مر او را نکوتر بود</p></div>
<div class="m2"><p>تو آن گوی با وی که در خور بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگویش بران رو که باشد صواب</p></div>
<div class="m2"><p>که پیش شه هند بفزودی آب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون گر بباشی به نزدیک اوی</p></div>
<div class="m2"><p>نگه‌داری آن رای باریک اوی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرانجا که خوشتر ولایت تراست</p></div>
<div class="m2"><p>سپهداری و باژ و ملکت تراست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به جایی که باشد همیشه بهار</p></div>
<div class="m2"><p>نسیم بهار آید از جویبار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گهر هست و دینار و گنج درم</p></div>
<div class="m2"><p>چو باشد درم دل نباشد به غم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نوازنده شاهی که از مهر تو</p></div>
<div class="m2"><p>بخندد چو بیند همی چهر تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به سالی دو بارست بار درخت</p></div>
<div class="m2"><p>ز قنوج برنگذرد نیک‌بخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو این گفته باشی به پرسش ز نام</p></div>
<div class="m2"><p>که از نام گردد دلم شادکام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مگر رام گردد بدین مرز ما</p></div>
<div class="m2"><p>فزون گردد از فر او ارز ما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ورا زود سالار لشکر کنیم</p></div>
<div class="m2"><p>بدین مرز با ارز ما سر کنیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیامد جهاندیده دستور شاه</p></div>
<div class="m2"><p>بگفت این به بهرام و بنمود راه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بهرام زان پس بپرسید نام</p></div>
<div class="m2"><p>که بی‌نام پاسخ نبودی تمام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بشنید بهرام رنگ رخش</p></div>
<div class="m2"><p>دگر شد که تا چون دهد پاسخش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به فرجام گفت ای سخن‌گوی مرد</p></div>
<div class="m2"><p>مرا در دو کشور مکن روی زرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من از شاه ایران نپیجم به گنج</p></div>
<div class="m2"><p>گر از نیستی چند باشم به رنج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جزین باشد آرایش دین ما</p></div>
<div class="m2"><p>همان گردش راه و آیین ما</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرانکس که پیچد سر از شاه خویش</p></div>
<div class="m2"><p>به برخاستن گم کند راه خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فزونی نجست آنک بودش خرد</p></div>
<div class="m2"><p>بد و نیک بر ما همی بگذرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خداوند گیتی فریدون کجاست</p></div>
<div class="m2"><p>که پشت زمانه بدو بود راست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کجا آن بزرگان خسرونژاد</p></div>
<div class="m2"><p>جهاندار کیخسرو و کیقباد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دگر آنک دانی تو بهرام را</p></div>
<div class="m2"><p>جهاندار پیروز خودکام را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر من ز فرمان او بگذرم</p></div>
<div class="m2"><p>به مردی سرآرد جهان بر سرم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نماند بر و بوم هندوستان</p></div>
<div class="m2"><p>به ایران کشد خاک جادوستان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همان به که من باز گردم بدر</p></div>
<div class="m2"><p>ببیند مرا شاه پیروزگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر از نام پرسیم برزوی نام</p></div>
<div class="m2"><p>چنین خواندم شاه و هم باب و مام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه پاسخ من بشنگل رسان</p></div>
<div class="m2"><p>که من دیر ماندم به شهر کسان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو دستور بشنید پاسخ ببرد</p></div>
<div class="m2"><p>شنیده سخن پیش او برشمرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز پاسخ پر آژنگ شد روی شاه</p></div>
<div class="m2"><p>چنین گفت اگر دور ماند ز راه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی چاره سازم کنون من که روز</p></div>
<div class="m2"><p>سرآید بدین مرد لشکر فروز</p></div></div>