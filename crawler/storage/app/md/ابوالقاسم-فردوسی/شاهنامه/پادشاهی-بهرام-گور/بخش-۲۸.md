---
title: >-
    بخش ۲۸
---
# بخش ۲۸

<div class="b" id="bn1"><div class="m1"><p>چو خورشید بر چرخ بنمود دست</p></div>
<div class="m2"><p>شهنشاه بر تخت زرین نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرستادهٔ قیصر آمد به در</p></div>
<div class="m2"><p>خرد یافته موبد پرگهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیش شهنشاه رفتند شاد</p></div>
<div class="m2"><p>سخنها ز هرگونه کردند یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرستاده را موبد شاه گفت</p></div>
<div class="m2"><p>که ای مرد هشیار بی‌یار و جفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز گیتی زیانکارتر کار چیست</p></div>
<div class="m2"><p>که بر کردهٔ او بباید گریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه دانی تو اندر جهان سودمند</p></div>
<div class="m2"><p>که از کردنش مرد گردد بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرستاده گفت آنک دانا بود</p></div>
<div class="m2"><p>همیشه بزرگ و توانا بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن مرد نادان ز گل خوارتر</p></div>
<div class="m2"><p>به هر نیکئی ناسزاوارتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نادان و دانا زدی داستان</p></div>
<div class="m2"><p>شنیدی مگر پاسخ راستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدو گفت موبد که نیکو نگر</p></div>
<div class="m2"><p>بیندیش و ماهی به خشکی مبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرستاده گفت ای پسندیده مرد</p></div>
<div class="m2"><p>سخن‌ها ز دانش توان یاد کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو این گر دگرگونه دانی بگوی</p></div>
<div class="m2"><p>که از دانش افزون شود آبروی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفت موبد که اندیشه کن</p></div>
<div class="m2"><p>کز اندیشه بازیب گردد سخن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز گیتی هرانکو بی‌آزارتر</p></div>
<div class="m2"><p>چنان دان که مرگش زیانکارتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مرگ بدان شاد باشی رواست</p></div>
<div class="m2"><p>چو زاید بد و نیک تن مرگ راست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازین سودمندی بود زان زیان</p></div>
<div class="m2"><p>خرد را میانجی کن اندر میان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بشنید رومی پسند آمدش</p></div>
<div class="m2"><p>سخنهای او سودمند آمدش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بخندید و بر شاه کرد آفرین</p></div>
<div class="m2"><p>بدو گفت فرخنده ایران زمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که تخت شهنشاه بیند همی</p></div>
<div class="m2"><p>چو موبد بروبر نشیند همی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دانش جهان را بلند افسری</p></div>
<div class="m2"><p>به موبد ز هر مهتری برتری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر باژ خواهی ز قیصر رواست</p></div>
<div class="m2"><p>ک دستور تو بر جهان پادشاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گفتار او شاد شد شهریار</p></div>
<div class="m2"><p>دلش تازه شد چو گل اندر بهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برون شد فرستاده از پیش شاه</p></div>
<div class="m2"><p>شب آمد برآمد درفش سیاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پدید آمد آن چادر مشکبوی</p></div>
<div class="m2"><p>به عنبر بیالود خورشید روی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شکیبا نبد گنبد تیزگرد</p></div>
<div class="m2"><p>سر خفته از خواب بیدار کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>درفشی بزد چشمهٔ آفتاب</p></div>
<div class="m2"><p>سر شاه گیتی سبک شد ز خواب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در بار بگشاد سالار بار</p></div>
<div class="m2"><p>نشست از بر تخت خود شهریار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بفرمود تا خلعت آراستند</p></div>
<div class="m2"><p>فرستاده را پیش او خواستند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز سیمین و زرین و اسپ و ستام</p></div>
<div class="m2"><p>ز دینار گیتی که بردند نام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز دینار و گوهر ز مشک و عبیر</p></div>
<div class="m2"><p>فزون گشت از اندیشهٔ تیزویر</p></div></div>