---
title: >-
    بخش ۲۹
---
# بخش ۲۹

<div class="b" id="bn1"><div class="m1"><p>چو از کار رومی بپردخت شاه</p></div>
<div class="m2"><p>دلش گشت پیچان ز کار سپاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بفرمود تا موبد رای‌زن</p></div>
<div class="m2"><p>بشد با یکی نامدار انجمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببخشید روی زمین سربسر</p></div>
<div class="m2"><p>ابر پهلوانان پرخاشخر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درم داد و اسپ و نگین و کلاه</p></div>
<div class="m2"><p>گرانمایه را کشور و تاج و گاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر از راستی کرد یکسر جهان</p></div>
<div class="m2"><p>وزو شادمانه کهان و مهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرانکس که بیداد بد دور کرد</p></div>
<div class="m2"><p>به نادادن چیز و گفتار سرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وزان پس چنین گفت با موبدان</p></div>
<div class="m2"><p>که ای پرهنر پاک‌دل بخردان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان را ز هرگونه دارید یاد</p></div>
<div class="m2"><p>ز کردار شاهان بیداد و داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی دست شاهان ز بیداد و آز</p></div>
<div class="m2"><p>تهی ماند و هم تن ز آرام و ناز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان از بداندیش در بیم بود</p></div>
<div class="m2"><p>دل نیک‌مردان به دو نیم بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه دست کرده به کار بدی</p></div>
<div class="m2"><p>کسی را نبد کوشش ایزدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبد بر زن و زاده کس پادشا</p></div>
<div class="m2"><p>پر از غم دل مردم پارسا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هر جای گستردن دست دیو</p></div>
<div class="m2"><p>بریده دل از بیم گیهان خدیو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر نیکویها و دست بدیست</p></div>
<div class="m2"><p>در دانش و کوشش بخردیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه پاک در گردن پادشاست</p></div>
<div class="m2"><p>که پیدا شود زو همه کژ و راست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پدر گر به بیداد یازید دست</p></div>
<div class="m2"><p>نبد پاک و دانا و یزدان‌پرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مدارید کردار او بس شگفت</p></div>
<div class="m2"><p>که روشن دلش رنگ آتش گرفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببینید تا جم و کاوس شاه</p></div>
<div class="m2"><p>چه کردند کز دیو جستند راه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پدر همچنان راه ایشان بجست</p></div>
<div class="m2"><p>به آب خرد جان تیره نشست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه زیردستانش پیچان شدند</p></div>
<div class="m2"><p>فراوان ز تندیش بیجان شدند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون رفت و زو نام بد ماند و بس</p></div>
<div class="m2"><p>همی آفرین او نیابد ز کس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ما باد بر جان او آفرین</p></div>
<div class="m2"><p>مبادا که پیچد روانش ز کین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون بر نشستم بر گاه اوی</p></div>
<div class="m2"><p>به مینو کشد بی‌گمان راه اوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی خواهم از کردگار جهان</p></div>
<div class="m2"><p>که نیرو دهد آشکار و نهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که با زیردستان مدارا کنیم</p></div>
<div class="m2"><p>ز خاک سیه مشک سارا کنیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که با خاک چون جفت گردد تنم</p></div>
<div class="m2"><p>نگیرد ستمدیده‌ای دامنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شما همچنین چادر راستی</p></div>
<div class="m2"><p>بپوشید شسته دل از کاستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که جز مرگ را کس ز مادر نزاد</p></div>
<div class="m2"><p>ز دهقان و تازی و رومی نژاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به کردار شیرست آهنگ اوی</p></div>
<div class="m2"><p>نپیچد کسی گردن از چنگ اوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همان شیر درنده را بشکرد</p></div>
<div class="m2"><p>به خواری تن اژدها بسپرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کجا آن سر و تاج شاهنشهان</p></div>
<div class="m2"><p>کجا آن بزرگان و فرخ مهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کجا آن سواران گردنکشان</p></div>
<div class="m2"><p>کزیشان نبینم به گیتی نشان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کجا ان پری چهرگان جهان</p></div>
<div class="m2"><p>کزیشان بدی شاد جان مهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرانکس که رخ زیر چادر نهفت</p></div>
<div class="m2"><p>چنان دان که گشتست با خاک جفت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه دست پاکی و نیکی بریم</p></div>
<div class="m2"><p>جهان را به کردار بد نشمریم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به یزدان دارنده کو داد فر</p></div>
<div class="m2"><p>به تاج و به تخت و نژاد و گهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که گر کارداری به یک مشک خاک</p></div>
<div class="m2"><p>زبان جوید اندر بلند و مغاک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم‌انجا بسوزم به آتش تنش</p></div>
<div class="m2"><p>کنم بر سر دار پیراهنش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر در گذشته ز شب چند پاس</p></div>
<div class="m2"><p>بدزدد ز درویش دزدی پلاس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به تاوانش دیبا فرستم ز گنج</p></div>
<div class="m2"><p>بشویم دل غمگنان را ز رنج</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وگر گوسفندی برند از رمه</p></div>
<div class="m2"><p>به تیره شب و روزگار دمه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی اسپ پرمایه تاوان دهم</p></div>
<div class="m2"><p>مبادا که بر وی سپاسی نهم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو با دشمنم کارزاری بود</p></div>
<div class="m2"><p>وزان جنگ خسته سواری بود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فرستمش یکساله زر و درم</p></div>
<div class="m2"><p>نداریم فرزند او را دژم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز دادار دارنده یکسر سپاس</p></div>
<div class="m2"><p>که اویست جاوید نیکی‌شناس</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به آب و به آتش میازید دست</p></div>
<div class="m2"><p>مگر هیربد مرد آتش‌پرست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مریزید هم خون گاوان ورز</p></div>
<div class="m2"><p>که ننگست در گاو کشتن به مرز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز پیری مگر گاو بیکار شد</p></div>
<div class="m2"><p>به چشم خداوند خود خوار شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نباید ز بن کشت گاو زهی</p></div>
<div class="m2"><p>که از مرز بیرون شود فرهی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همه رای با مرد دانا زنید</p></div>
<div class="m2"><p>دل کودک بی‌پدر مشکنید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از اندیشهٔ دیو باشید دور</p></div>
<div class="m2"><p>گه جنگ دشمن مجویید سور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر خواهم از زیردستان خراج</p></div>
<div class="m2"><p>ز دارنده بیزارم و تخت عاج</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر بدکنش بد پدر یزدگرد</p></div>
<div class="m2"><p>به پاداش آن داد کردیم گرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه دل ز کردار او خوش کنید</p></div>
<div class="m2"><p>به آزادی آهنگ آتش کنید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ببخشد مگر کردگارش گناه</p></div>
<div class="m2"><p>ز دوزخ به مینو نمایدش راه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کسی کو جوانست شادی کنید</p></div>
<div class="m2"><p>دل مردمان جوان مشکنید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به پیری به مستی میازید دست</p></div>
<div class="m2"><p>که همواره رسوا بود پیر مست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گنهکار یزدان مباشید هیچ</p></div>
<div class="m2"><p>به پیری به آید به رفتن بسیچ</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو خشنود گردد ز ما کردگار</p></div>
<div class="m2"><p>به هستی غم روز فردا مدار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دل زیردستان به ما شاد باد</p></div>
<div class="m2"><p>سر سرکشان از غم آزاد باد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همه نامداران چو گفتار شاه</p></div>
<div class="m2"><p>شنیدند و کردند نیکو نگاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همه دیده کردند پیشش پر آب</p></div>
<div class="m2"><p>ازان شاه پردانش و زودیاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خروشان برو آفرین خواندند</p></div>
<div class="m2"><p>ورا پادشا زمین خواندند</p></div></div>