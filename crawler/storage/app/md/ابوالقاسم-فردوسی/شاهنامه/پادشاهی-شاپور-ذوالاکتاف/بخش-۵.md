---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>چنان بد که یک روز با تاج و گنج</p></div>
<div class="m2"><p>همی داشت از بودنی دل به رنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تیره شب اندر گذشته سه پاس</p></div>
<div class="m2"><p>بفرمود تا شد ستاره‌شناس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپرسیدش از تخت شاهنشهی</p></div>
<div class="m2"><p>هم از رنج وز روزگار بهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منجم بیاورد صلاب را</p></div>
<div class="m2"><p>بینداخت آرامش و خواب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگه کرد روشن به قلب اسد</p></div>
<div class="m2"><p>که هست او نماینده فتح و جد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان تا رسد پادشا را بدی</p></div>
<div class="m2"><p>فزاید بدو فره ایزدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دیدند گفتندش ای پادشا</p></div>
<div class="m2"><p>جهانگیر و روشن‌دل و پارسا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی کار پیش است با رنج و درد</p></div>
<div class="m2"><p>نیارد کس آن بر توبر یاد کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین داد شاپور پاسخ بدوی</p></div>
<div class="m2"><p>که ای مرد داننده و راه‌جوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه چارست تا این ز من بگذرد</p></div>
<div class="m2"><p>تنم اختر بد به پی نسپرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ستاره‌شمر گفت کای شهریار</p></div>
<div class="m2"><p>ازین گردش چرخ ناپایدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مردی و دانش نیابی گذر</p></div>
<div class="m2"><p>خردمند گر مرد پرخاشخر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بباشد همه بودنی بی‌گمان</p></div>
<div class="m2"><p>نتابیم با گردش آسمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین داد پاسخ گرانمایه شاه</p></div>
<div class="m2"><p>که دادار باشد ز هر بد نگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که گردان بلند آسمان آفرید</p></div>
<div class="m2"><p>توانایی و ناتوان آفرید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگسترد بر پادشاهیش داد</p></div>
<div class="m2"><p>همی بود یک چند بی‌رنج و شاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آباد شد زو همه مرز و بوم</p></div>
<div class="m2"><p>چنان آرزو کرد کاید به روم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببیند که قیصر سزاوار هست</p></div>
<div class="m2"><p>ابا لشکر و گنج و نیروی دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان راز بگشاد با کدخدای</p></div>
<div class="m2"><p>یک پهلوان گرد با داد و رای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه راز و اندیشه با او بگفت</p></div>
<div class="m2"><p>همی داشت از هرکس اندر نهفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین گفت کاین پادشاهی به داد</p></div>
<div class="m2"><p>بدارید کزداد باشید شاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شتر خواست پرمایه ده کاروان</p></div>
<div class="m2"><p>به هر کاروان بر یکی ساروان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دینار وز گوهران بار کرد</p></div>
<div class="m2"><p>ازان سی شتر بار دینار کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بیامد پراندیشه ز آبادبوم</p></div>
<div class="m2"><p>همی رفت زین سان سوی مرز روم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی روستا بود نزدیک شهر</p></div>
<div class="m2"><p>که دهقان و شهری بدو بود بهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیامد به خان یکی کدخدای</p></div>
<div class="m2"><p>بپرسید کاید مرا هست جای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برو آفرین کرد مهتر بسی</p></div>
<div class="m2"><p>که چون تو نیابیم مهمان کسی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ببود آن شب و خورد و بخشید چیز</p></div>
<div class="m2"><p>ز دهقان بسی آفرین یافت نیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپیده برآمد بنه برنهاد</p></div>
<div class="m2"><p>سوی خانهٔ قیصر آمد چو باد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بیامد به نزدیک سالار بار</p></div>
<div class="m2"><p>برو آفرین کرد و بردش نثار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بپرسید و گفتش چه مردی بگوی</p></div>
<div class="m2"><p>که هم شاه‌شاخی و هم شاه‌روی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین داد پاسخ که ای پادشا</p></div>
<div class="m2"><p>یکی پارسی مردم و پارسا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به بازارگانی برفتم ز جز</p></div>
<div class="m2"><p>یکی کاروان دارم از خز و بز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنون آمدستم بدین بارگاه</p></div>
<div class="m2"><p>مگر نزد قیصر گشاینده راه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ازین بار چیزی کش اندر خورست</p></div>
<div class="m2"><p>همه گوهر و آلت لشکرست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پذیرد سپارد به گنجور گنج</p></div>
<div class="m2"><p>بدان شاد باشم ندارم به رنج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دگر را فروشم به زر و به سیم</p></div>
<div class="m2"><p>به قیصر پناهم نپیچم ز بیم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بخرم هرانچم بباید ز روم</p></div>
<div class="m2"><p>روم سوی ایران ز آباد بوم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز درگاه برخاست مرد کهن</p></div>
<div class="m2"><p>بر قیصر آمد بگفت این سخن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بفرمود تا پرده برداشتند</p></div>
<div class="m2"><p>ز در سوی قیصرش بگذاشتند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو شاپور نزدیک قیصر رسید</p></div>
<div class="m2"><p>بکرد آفرینی چنان چون سزید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نگه کرد قیصر به شاپور گرد</p></div>
<div class="m2"><p>ز خوبی دل و دیده او را سپرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بفرمود تا خوان و می ساختند</p></div>
<div class="m2"><p>ز بیگانه ایوان بپرداختند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جفادیده ایرانیی بد به روم</p></div>
<div class="m2"><p>چنانچون بود مرد بیداد و شوم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به قیصر چنین گفت کای سرفراز</p></div>
<div class="m2"><p>یکی نو سخن بشنو از من به راز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که این نامور مرد بازارگان</p></div>
<div class="m2"><p>که دیبا فروشد به دینارگان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شهنشاه شاپور گویم که هست</p></div>
<div class="m2"><p>به گفتار و دیدار و فر و نشست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو بشنید قیصر سخن تیره شد</p></div>
<div class="m2"><p>همی چشمش از روی او خیره شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نگهبانش برکرد و با کس نگفت</p></div>
<div class="m2"><p>همی داشت آن راز را در نهفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو شد مست برخاست شاپور شاه</p></div>
<div class="m2"><p>همی داشت قیصر مر او را نگاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیامد نگهبان و او را گرفت</p></div>
<div class="m2"><p>که شاپور نرسی توی ای شگفت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به جای زنان برد و دستش ببست</p></div>
<div class="m2"><p>به مردی ز دام بلا کس نجست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو زین باره دانش نیاید به بر</p></div>
<div class="m2"><p>چه باید شمار ستاره‌شمر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بر مست شمعی همی سوختند</p></div>
<div class="m2"><p>به زاریش در چرم خر دوختند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همی گفت هرکس که این شوربخت</p></div>
<div class="m2"><p>همی پوست خر جست و بگذاشت تخت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یکی خانه‌ای بود تاریک و تنگ</p></div>
<div class="m2"><p>ببردند بدبخت را بی‌درنگ</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدان جای تنگ اندر انداختند</p></div>
<div class="m2"><p>در خانه را قفل بر ساختند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کلیدش به کدبانوی خانه داد</p></div>
<div class="m2"><p>تنش را بدان چرم بیگانه داد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به زن گفت چندان دهش نان و آب</p></div>
<div class="m2"><p>که از داشتن زو نگیرد شتاب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر زنده ماند به یک چندگاه</p></div>
<div class="m2"><p>بداند مگر ارج تخت و کلاه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همان تخت قیصر نیایدش یاد</p></div>
<div class="m2"><p>کسی را کجا نیست قیصر نژاد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زن قیصر آن خانه را در ببست</p></div>
<div class="m2"><p>به ایوان دگر جای بودش نشست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکی ماه‌رخ بود گنجور اوی</p></div>
<div class="m2"><p>گزیده به هر کار دستور اوی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>که ز ایرانیان داشتی او نژاد</p></div>
<div class="m2"><p>پدر بر پدر بر همی داشت یاد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کلید در خانه او را سپرد</p></div>
<div class="m2"><p>به چرم اندرون بسته شاپور گرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همان روز ازان مرز لشکر براند</p></div>
<div class="m2"><p>ورا بسته در پوست آنجا بماند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو قیصر به نزدیک ایران رسید</p></div>
<div class="m2"><p>سپه یک به یک تیغ کین برکشید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از ایران همی برد رومی اسیر</p></div>
<div class="m2"><p>نبود آن یلان را کسی دستگیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به ایران زن و مرد و کودک نماند</p></div>
<div class="m2"><p>همان چیز بسیار و اندک نماند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نبود آگهی در میان سپاه</p></div>
<div class="m2"><p>نه مرده نه زنده ز شاپور شاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گریزان همه شهر ایران ز روم</p></div>
<div class="m2"><p>ز مردم تهی شد همه مرز و بوم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از ایران بی‌اندازه ترسا شدند</p></div>
<div class="m2"><p>همه مرز پیش سکوبا شدند</p></div></div>