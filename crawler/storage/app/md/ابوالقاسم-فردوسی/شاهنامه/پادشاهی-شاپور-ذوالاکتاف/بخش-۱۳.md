---
title: >-
    بخش ۱۳
---
# بخش ۱۳

<div class="b" id="bn1"><div class="m1"><p>یکی مرد بود از نژاد سران</p></div>
<div class="m2"><p>هم از تخمهٔ نامور قیصران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برانوش نام و خردمند بود</p></div>
<div class="m2"><p>زبان و روانش پر از بند بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفت لشکر که قیصر تو باش</p></div>
<div class="m2"><p>برین لشکر و بوم مهتر تو باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گفتار تو گوش دارد سپاه</p></div>
<div class="m2"><p>بیفروز تاج و بیارای گاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاراستند از برش تخت عاج</p></div>
<div class="m2"><p>برانوش بنشست بر سرش تاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جای بزرگیش بنشاندند</p></div>
<div class="m2"><p>همه رومیان آفرین خواندند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برانوش بنشست و اندیشه کرد</p></div>
<div class="m2"><p>ز روم و ز آوردگاه نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدانست کو را ز شاه بلند</p></div>
<div class="m2"><p>ز روم و ز آویزش آید گزند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرستاده‌ای جست بارای و شرم</p></div>
<div class="m2"><p>که دانش سراید به آواز نرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دبیری بزرگ و جهاندیده‌ای</p></div>
<div class="m2"><p>خردمند و دانا پسندیده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیاورد و بنشاند نزدیک خویش</p></div>
<div class="m2"><p>بگفت آن سخنهای باریک خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی نامه بنوشت پرآفرین</p></div>
<div class="m2"><p>ز دادار بر شهریار زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که جاوید تاج تو پاینده باد</p></div>
<div class="m2"><p>همه مهتران پیش تو بنده باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو دانی که تاراج و خون ریختن</p></div>
<div class="m2"><p>چه با بیگنه مردم آویختن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهان سرافراز دارند شوم</p></div>
<div class="m2"><p>چه با شهر ایران چه با مرز روم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر این کین ایرج به دست از نخست</p></div>
<div class="m2"><p>منوچهر کرد آن به مردی درست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تن سلم زان کین کنون خاک شد</p></div>
<div class="m2"><p>هم از تور روی زمین پاک شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر کین داراست و اسکندری</p></div>
<div class="m2"><p>که نو شد بر وی زمین داوری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مر او را دو دستور بد کشته بود</p></div>
<div class="m2"><p>و دیگر کزو بخت برگشته بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرت کین قیصر فزاید همی</p></div>
<div class="m2"><p>به زندان تو بند ساید همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نباید که ویران شود بوم روم</p></div>
<div class="m2"><p>که چون روم دیگر نبودست بوم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وگر غارت و کشتنت بود رای</p></div>
<div class="m2"><p>همه روم گشتند بی‌دست و پای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زن و کودکانش اسیر تواند</p></div>
<div class="m2"><p>جگر خسته از تیغ و تیر تواند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گه آمد که کمتر کنی کین و خشم</p></div>
<div class="m2"><p>فرو خوابنی از گذشته دو چشم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فدای تو بادا همه خواسته</p></div>
<div class="m2"><p>کزین کین همی جان شود کاسته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو دل خوش کن و شهر چندین مسوز</p></div>
<div class="m2"><p>نباید که روز اندر آید به روز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نباشد پسند جهان‌آفرین</p></div>
<div class="m2"><p>که بیداد جوید جهاندار کین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درود جهاندار بر شاه باد</p></div>
<div class="m2"><p>بلند اخترش افسر ماه باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نویسنده بنهاد پس خامه را</p></div>
<div class="m2"><p>چو اندر نوشت آن کیی نامه را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نهادند پس مهر قیصر بروی</p></div>
<div class="m2"><p>فرستاده بنهاد زی شاه روی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیامد خردمند و نامه بداد</p></div>
<div class="m2"><p>ز قیصر به شاپور فرخ نژاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو آن نامور نامه برخواندند</p></div>
<div class="m2"><p>سخنهای نغزش برافشاندند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ببخشود و دیده پر از آب کرد</p></div>
<div class="m2"><p>بروهای جنگی پر از تاب کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم‌اندر زمان نامه پاسخ نوشت</p></div>
<div class="m2"><p>بگفت آنکجا رفته بد خوب و زشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که مهمان به چرم خر اندر که دوخت</p></div>
<div class="m2"><p>که بازار کین کهن برفروخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو گرد بخردی خیز پیش من آی</p></div>
<div class="m2"><p>خود و فیلسوفان پاکیزه رای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو زنهار دادم نسازمت جنگ</p></div>
<div class="m2"><p>گشاده کنم بر تو این راه تنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فرستاده برگشت و پاسخ ببرد</p></div>
<div class="m2"><p>سخنها یکایک همه برشمرد</p></div></div>