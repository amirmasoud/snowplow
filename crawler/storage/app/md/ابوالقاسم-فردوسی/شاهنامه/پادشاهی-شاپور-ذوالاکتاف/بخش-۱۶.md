---
title: >-
    بخش ۱۶
---
# بخش ۱۶

<div class="b" id="bn1"><div class="m1"><p>ز شاپور زان‌گونه شد روزگار</p></div>
<div class="m2"><p>که در باغ با گل ندیدند خار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز داد و ز رای و ز آهنگ اوی</p></div>
<div class="m2"><p>ز بس کوشش و جنگ و نیرنگ اوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر او را به هر بوم دشمن نماند</p></div>
<div class="m2"><p>بدی را به گیتی نشیمن نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نومید شد او ز چرخ بلند</p></div>
<div class="m2"><p>بشد سالیانش به هفتاد و اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفرمود تا پیش او شد دبیر</p></div>
<div class="m2"><p>ابا موبد موبدان اردشیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوانی که کهتر برادرش بود</p></div>
<div class="m2"><p>به داد و خرد بر سر افسرش بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورا نام بود اردشیر جوان</p></div>
<div class="m2"><p>توانا و دانا به سود و زیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پسر بد یکی خرد شاپور نام</p></div>
<div class="m2"><p>هنوز از جهان نارسیده به کام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین گفت پس شاه با اردشیر</p></div>
<div class="m2"><p>که ای گرد و چابک سوار دلیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر با من از داد پیمان کنی</p></div>
<div class="m2"><p>زبان را به پیمان گروگان کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که فرزند من چون به مردی رسد</p></div>
<div class="m2"><p>به گاه دلیری و گردی رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپاری بدو تخت و گنج و سپاه</p></div>
<div class="m2"><p>تو دستور باشی ورا نیک‌خواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من این تاج شاهی سپارم به تو</p></div>
<div class="m2"><p>همان گنج و لشکر گذارم به تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپذرفت زو این سخن اردشیر</p></div>
<div class="m2"><p>به پیش بزرگان و پیش دبیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که چون کودک او به مردی رسد</p></div>
<div class="m2"><p>که دیهیم و تاج کیی را سزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپارم همه پادشاهی ورا</p></div>
<div class="m2"><p>نسازم جز از نیک‌خواهی ورا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بشنید شاپور پیش مهان</p></div>
<div class="m2"><p>بدو داد دیهیم و مهر شهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنین گفت پس شاه با اردشیر</p></div>
<div class="m2"><p>که کار جهان بر دل آسان مگیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان ای برادر که بیداد شاه</p></div>
<div class="m2"><p>پی پادشاهی ندارد نگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آگندن گنج شادان بود</p></div>
<div class="m2"><p>به زفتی سر سرفرازان بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خنک شاه باداد و یزدان پرست</p></div>
<div class="m2"><p>کزو شاد باشد دل زیردست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به داد و به بخشش فزونی کند</p></div>
<div class="m2"><p>جهان را بدین رهنمونی کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نگه دارد از دشمنان کشورش</p></div>
<div class="m2"><p>به ابر اندر آرد سر و افسرش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به داد و به آرام گنج آگند</p></div>
<div class="m2"><p>به بخشش ز دل رنج بپراگند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گناه از گنهکار بگذاشتن</p></div>
<div class="m2"><p>پی مردمی را نگه داشتن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرانکس که او این هنرها بجست</p></div>
<div class="m2"><p>خرد باید و حزم و رای درست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بباید خرد شاه را ناگزیر</p></div>
<div class="m2"><p>هم آموزش مرد برنا و پیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دل پادشا چون گراید به مهر</p></div>
<div class="m2"><p>برو کامها تازه دارد سپهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گنهکار باشد تن زیردست</p></div>
<div class="m2"><p>مگر مردم پاک و یزدان پرست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل و مغز مردم دو شاه تنند</p></div>
<div class="m2"><p>دگر آلت تن سپاه تنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو مغز و دل مردم آلوده گشت</p></div>
<div class="m2"><p>به نومیدی از رای پالوده گشت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدان تن سراسیمه گردد روان</p></div>
<div class="m2"><p>سپه چون زید شاه بی‌پهلوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو روشن نباشد بپراگند</p></div>
<div class="m2"><p>تن بی‌روان را به خاک افگند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنین همچو شد شاه بیدادگر</p></div>
<div class="m2"><p>جهان زو شود زود زیر و زبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدوبر پس از مرگ نفرین بود</p></div>
<div class="m2"><p>همان نام او شاه بی دین بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدین دار چشم و بدان دار گوش</p></div>
<div class="m2"><p>که اویست دارنده جان و هوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هران پادشا کو جزین راه جست</p></div>
<div class="m2"><p>ز نیکیش باید دل و دست شست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز کشورش بپراگند زیردست</p></div>
<div class="m2"><p>همان از درش مرد خسروپرست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نبینی که دانا چه گوید همی</p></div>
<div class="m2"><p>دلت را ز کژی بشوید همی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که هر شاه کو را ستایش بود</p></div>
<div class="m2"><p>همه کارش اندر فزایش بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نکوهیده باشد جفا پیشه مرد</p></div>
<div class="m2"><p>به گرد در آزداران مگرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدان ای برادر که از شهریار</p></div>
<div class="m2"><p>بجوید خردمند هرگونه کار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی آنک پیروزگر باشد اوی</p></div>
<div class="m2"><p>ز دشمن نتابد گه جنگ روی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دگر آنک لشکر بدارد به داد</p></div>
<div class="m2"><p>بداند فزونی مرد نژاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کسی کز در پادشاهی بود</p></div>
<div class="m2"><p>نخواهد که مهتر سپاهی بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چهارم که با زیردستان خویش</p></div>
<div class="m2"><p>همان باگهر در پرستان خویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ندارد در گنج را بسته سخت</p></div>
<div class="m2"><p>همی بارد از شاخ بار درخت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بباید در پادشاهی سپاه</p></div>
<div class="m2"><p>سپاهی در گنج دارد نگاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر گنجت آباد داری به داد</p></div>
<div class="m2"><p>تو از گنج شاد و سپاه از تو شاد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سلیحت در آرایش خویش دار</p></div>
<div class="m2"><p>سزد کت شب تیره آید به کار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بس ایمن مشو بر نگهدار خویش</p></div>
<div class="m2"><p>چو ایمن شدی راست کن کار خویش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سرانجام مرگ آیدت بی‌گمان</p></div>
<div class="m2"><p>اگر تیره‌ای گر چراغ جهان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>برادر چو بشنید چندی گریست</p></div>
<div class="m2"><p>چو اندرز بنوشت سالی بزیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>برفت و بماند این سخن یادگار</p></div>
<div class="m2"><p>تو اندر جهان تخم زفتی مکار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که هم یک زمان روز تو بگذرد</p></div>
<div class="m2"><p>چنین برده رنج تو دشمن خورد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو آدینه هر مزد بهمن بود</p></div>
<div class="m2"><p>برین کار فرخ نشیمن بود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>می لعل پیش آور ای هاشمی</p></div>
<div class="m2"><p>ز خمی که هرگز نگیرد کمی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو شست و سه شد سال شد گوش کر</p></div>
<div class="m2"><p>ز بیشی چرا جویم آیین و فر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کنون داستانهای شاه اردشیر</p></div>
<div class="m2"><p>بگویم ز گفتار من یادگیر</p></div></div>