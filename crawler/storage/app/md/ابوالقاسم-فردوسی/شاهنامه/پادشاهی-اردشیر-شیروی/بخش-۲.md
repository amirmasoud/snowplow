---
title: >-
    بخش ۲
---
# بخش ۲

<div class="b" id="bn1"><div class="m1"><p>پس آگاهی آمد به نزد گراز</p></div>
<div class="m2"><p>که زو بود خسرو به گرم و گداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرستاد گوینده‌ای را ز روم</p></div>
<div class="m2"><p>که در خاک شد تاج شیروی شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که جانش به دوزخ گرفتار باد</p></div>
<div class="m2"><p>سر دخمهٔ او نگون سار باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دانست هرگز که سرو بلند</p></div>
<div class="m2"><p>به باغ از گیا یافت خواهد گزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خسرو که چشم و دل روزگار</p></div>
<div class="m2"><p>نبیند چنو نیز یک شهریار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شیروی را شهریاری دهد</p></div>
<div class="m2"><p>همه شهر ایران به خواری دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنو رفت شد تاجدار اردشیر</p></div>
<div class="m2"><p>بدو شادمان جان برنا و پیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا گر ز ایران رسد هیچ بهر</p></div>
<div class="m2"><p>نخواهم که بر وی رسد باد شهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبودم من آگه که پرویز شاه</p></div>
<div class="m2"><p>به گفتار آن بدتنان شد تباه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیایم کنون با سپاهی گران</p></div>
<div class="m2"><p>ز روم و ز ایران گزیده سران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببینیم تا کیست این کدخدای</p></div>
<div class="m2"><p>که باشد پسندش بدین گونه رای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان برکنم بیخ او را ز بن</p></div>
<div class="m2"><p>کزان پس نراند ز شاهی سخن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نوندی برافگند پویان به راه</p></div>
<div class="m2"><p>به نزدیک پیران ایران سپاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگرگونه آهنگ بدکامه کرد</p></div>
<div class="m2"><p>به پیروز خسرو یکی نامه کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که شد تیره این تخت ساسانیان</p></div>
<div class="m2"><p>جهانجوی باید که بندد میان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توانی مگر چاره‌ای ساختن</p></div>
<div class="m2"><p>ز هرگونه اندیشه انداختن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بجویی بسی یار برنا و پیر</p></div>
<div class="m2"><p>جهان را بپردازی از اردشیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازان پس بیابی همه کام خویش</p></div>
<div class="m2"><p>شوی ایمن و شاد زارام خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ایدون که این راز بیرون دهی</p></div>
<div class="m2"><p>همی خنجر کینه را خون دهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من از روم چندان سپاه آورم</p></div>
<div class="m2"><p>که گیتی به چشمت سیاه آورم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به ژرفی نگه‌دار گفتار من</p></div>
<div class="m2"><p>مبادا که خوار آیدت کار من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو پیروز خسرو چنان نامه دید</p></div>
<div class="m2"><p>همه پیش و پس رای خودکامه دید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل روشن نامور شد تباه</p></div>
<div class="m2"><p>که تا چون کند بد بدان زادشاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ورا خواندی هر زمان اردشیر</p></div>
<div class="m2"><p>که گوینده مردی بد و یادگیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برآسای دستور بودی ورا</p></div>
<div class="m2"><p>همان نیز گنجور بودی ورا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیامد شبی تیره گون بار یافت</p></div>
<div class="m2"><p>می روشن و چرب گفتار یافت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشسته به ایوان خویش اردشیر</p></div>
<div class="m2"><p>تنی چند با او ز برنا و پیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو پیروز خسرو بیامد برش</p></div>
<div class="m2"><p>تو گفتی ز گردون برآمد سرش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بفرمود تا برکشیدند رود</p></div>
<div class="m2"><p>شد ایوان پر از بانگ رود و سرود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو نیمی شب تیره اندرکشید</p></div>
<div class="m2"><p>سپهبد می یک منی در کشید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شده مست یاران شاه اردشیر</p></div>
<div class="m2"><p>نماند ایچ رامشگر و یادگیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بد اندیش یاران او را براند</p></div>
<div class="m2"><p>جز از شاه و پیروز خسرو نماند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جفا پیشه از پیش خانه بجست</p></div>
<div class="m2"><p>لب شاه بگرفت ناگه به دست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همی‌داشت تا شد تباه اردشیر</p></div>
<div class="m2"><p>همه کاخ شد پر ز شمشیر و تیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه یار پیروز خسرو شدند</p></div>
<div class="m2"><p>اگر نو جهانجوی اگر گو بدند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هیونی برافگند نزد گراز</p></div>
<div class="m2"><p>یکی نامه‌ای نیز با آن دراز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فرستاده چون شد به نزدیک او</p></div>
<div class="m2"><p>چو خورشید شد جان تاریک اوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بیاورد زان بوم چندان سپاه</p></div>
<div class="m2"><p>که بر مور و بر پشه بر بست راه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی‌تاخت چون باد تا طیسفون</p></div>
<div class="m2"><p>سپاهش همه دست شسته به خون</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز لشکر نیارست دم زد کسی</p></div>
<div class="m2"><p>نبد خود دران شهر مردم بسی</p></div></div>