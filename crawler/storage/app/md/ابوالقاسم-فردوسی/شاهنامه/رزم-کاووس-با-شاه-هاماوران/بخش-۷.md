---
title: >-
    بخش ۷
---
# بخش ۷

<div class="b" id="bn1"><div class="m1"><p>بیامد سوی پارس کاووس کی</p></div>
<div class="m2"><p>جهانی به شادی نوافگند پی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاراست تخت و بگسترد داد</p></div>
<div class="m2"><p>به شادی و خوردن دل اندر نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستاد هر سو یکی پهلوان</p></div>
<div class="m2"><p>جهاندار و بیدار و روشن‌روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مرو و نشاپور و بلخ و هری</p></div>
<div class="m2"><p>فرستاد بر هر سویی لشکری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهانی پر از داد شد یکسره</p></div>
<div class="m2"><p>همی روی برتافت گرگ از بره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس گنج و زیبایی و فرهی</p></div>
<div class="m2"><p>پری و دد و دام گشتش رهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهان پیش کاووس کهتر شدند</p></div>
<div class="m2"><p>همه تاجدارنش لشکر شدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان پهلوانی به رستم سپرد</p></div>
<div class="m2"><p>همه روزگار بهی زو شمرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی خانه کرد اندر البرز کوه</p></div>
<div class="m2"><p>که دیو اندران رنج‌ها شد ستوه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بفرمود کز سنگ خارا کنند</p></div>
<div class="m2"><p>دو خانه برو هر یکی ده کمند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیاراست آخر به سنگ اندرون</p></div>
<div class="m2"><p>ز پولاد میخ و ز خارا ستون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ببستند اسپان جنگی بدوی</p></div>
<div class="m2"><p>هم اشتر عماری‌کش و راه جوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو خانه دگر ز آبگینه بساخت</p></div>
<div class="m2"><p>زبرجد به هر جایش اندر نشاخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان ساخت جای خرام و خورش</p></div>
<div class="m2"><p>که تن یابد از خوردنی پرورش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو خانه ز بهر سلیح نبرد</p></div>
<div class="m2"><p>بفرمو کز نقرهٔ خام کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی کاخ زرین ز بهر نشست</p></div>
<div class="m2"><p>برآورد و بالاش داده دو شست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبودی تموز ایچ پیدا ز دی</p></div>
<div class="m2"><p>هوا عنبرین بود و بارانش می</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به ایوانش یاقوت برده بکار</p></div>
<div class="m2"><p>ز پیروزه کرده برو بر نگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه ساله روشن بهاران بدی</p></div>
<div class="m2"><p>گلان چون رخ غمگساران بدی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز درد و غم و رنج دل دور بود</p></div>
<div class="m2"><p>بدی را تن دیو رنجور بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به خواب اندر آمد بد روزگار</p></div>
<div class="m2"><p>ز خوبی و از داد آموزگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به رنجش گرفتار دیوان بدند</p></div>
<div class="m2"><p>ز بادافرهٔ او غریوان بدند</p></div></div>