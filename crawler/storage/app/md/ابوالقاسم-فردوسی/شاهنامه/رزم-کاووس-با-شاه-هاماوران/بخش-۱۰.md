---
title: >-
    بخش ۱۰
---
# بخش ۱۰

<div class="b" id="bn1"><div class="m1"><p>چه گفت آن سراینده مرد دلیر</p></div>
<div class="m2"><p>که ناگه برآویخت با نره شیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گر نام مردی بجویی همی</p></div>
<div class="m2"><p>رخ تیغ هندی بشویی همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بدها نبایدت پرهیز کرد</p></div>
<div class="m2"><p>که پیش آیدت روز ننگ و نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه چو آمد بتنگی فراز</p></div>
<div class="m2"><p>هم از تو نگردد به پرهیز باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو همره کنی جنگ را با خرد</p></div>
<div class="m2"><p>دلیرت ز جنگ‌آوران نشمرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرد را و دین را رهی دیگرست</p></div>
<div class="m2"><p>سخنهای نیکو به بند اندرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون از ره رستم جنگجوی</p></div>
<div class="m2"><p>یکی داستانست با رنگ و بوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شنیدم که روزی گو پیلتن</p></div>
<div class="m2"><p>یکی سور کرد از در انجمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جایی کجا نام او بد نوند</p></div>
<div class="m2"><p>بدو اندرون کاخهای بلند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا آذر تیز برزین کنون</p></div>
<div class="m2"><p>بدانجا فروزد همی رهنمون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزرگان ایران بدان بزمگاه</p></div>
<div class="m2"><p>شدند انجمن نامور یک سپاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو طوس و چو گودرز کشوادگان</p></div>
<div class="m2"><p>چو بهرام و چون گیو آزادگان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو گرگین و چون زنگهٔ شاوران</p></div>
<div class="m2"><p>چو گستهم و خراد جنگ‌آوران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو برزین گردنکش تیغ زن</p></div>
<div class="m2"><p>گرازه کجا بد سر انجمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابا هر یک از مهتران مرد چند</p></div>
<div class="m2"><p>یکی لشکری نامدار ارجمند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیاسود لشکر زمانی ز کار</p></div>
<div class="m2"><p>ز چوگان و تیر و نبید و شکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به مستی چنین گفت یک روز گیو</p></div>
<div class="m2"><p>به رستم که ای نامبردار نیو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر ایدون که رای شکار آیدت</p></div>
<div class="m2"><p>چو یوز دونده به کار آیدت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به نخچیرگاه رد افراسیاب</p></div>
<div class="m2"><p>بپوشیم تابان رخ آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز گرد سواران و از یوز و باز</p></div>
<div class="m2"><p>بگیریم آرام روز دراز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به گور تگاور کمند افگنیم</p></div>
<div class="m2"><p>به شمشیر بر شیر بند افگنیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدان دشت توران شکاری کنیم</p></div>
<div class="m2"><p>که اندر جهان یادگاری کنیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو گفت رستم که بی‌کام تو</p></div>
<div class="m2"><p>مبادا گذر تا سرانجام تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سحرگه بدان دشت توران شویم</p></div>
<div class="m2"><p>ز نخچیر و از تاختن نغنویم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ببودند یکسر برین هم سخن</p></div>
<div class="m2"><p>کسی رای دیگر نیفگند بن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سحرگه چو از خواب برخاستند</p></div>
<div class="m2"><p>بران آرزو رفتن آراستند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برفتند با باز و شاهین و مهد</p></div>
<div class="m2"><p>گرازنده و شاد تا رود شهد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به نخچیرگاه رد افراسیاب</p></div>
<div class="m2"><p>ز یک دست ریگ و ز یک دست آب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر سو سرخس و بیابانش پیش</p></div>
<div class="m2"><p>گله گشته بر دشت آهو و میش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه دشت پر خرگه و خیمه گشت</p></div>
<div class="m2"><p>از انبوه آهو سراسیمه گشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز درنده شیران زمین شد تهی</p></div>
<div class="m2"><p>به پرنده مرغان رسید آگهی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تلی هر سویی مرغ و نخجیر بود</p></div>
<div class="m2"><p>اگر کشته گر خستهٔ تیر بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز خنده نیاسود لب یک زمان</p></div>
<div class="m2"><p>ببودند روشن دل و شادمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به یک هفته زین‌گونه با می بدست</p></div>
<div class="m2"><p>گهی تاختن گه نشاط نشست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بهشتم تهمتن بیامد پگاه</p></div>
<div class="m2"><p>یکی رای شایسته زد با سپاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنین گفت رستم بدان سرکشان</p></div>
<div class="m2"><p>بدان گرزداران مردم‌کشان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که از ما به افراسیاب این زمان</p></div>
<div class="m2"><p>همانا رسید آگهی بی‌گمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی چاره سازد بیاید بجنگ</p></div>
<div class="m2"><p>کند دشت نخچیر بر یوز تنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بباید طلایه به ره بر یکی</p></div>
<div class="m2"><p>که چون آگهی یابد او اندکی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیاید دهد آگهی از سپاه</p></div>
<div class="m2"><p>نباید که گیرد بداندیش راه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرازه به زه بر نهاده کمان</p></div>
<div class="m2"><p>بیامد بران کار بسته میان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سپه را که چون او نگهدار بود</p></div>
<div class="m2"><p>همه چارهٔ دشمنان خوار بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به نخچیر و خوردن نهادند روی</p></div>
<div class="m2"><p>نکردند کس یاد پرخاشجوی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس آگاهی آمد به افراسیاب</p></div>
<div class="m2"><p>ازیشان شب تیره هنگام خواب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز لشکر جهان‌دیدگان را بخواند</p></div>
<div class="m2"><p>ز رستم بسی داستانها براند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وزان هفت گرد سوار دلیر</p></div>
<div class="m2"><p>که بودند هر یک به کردار شیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که ما را بباید کنون ساختن</p></div>
<div class="m2"><p>بناگاه بردن یکی تاختن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گراین هفت یل را بچنگ آوریم</p></div>
<div class="m2"><p>جهان پیش کاووس تنگ آوریم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بکردار نخچیر باید شدن</p></div>
<div class="m2"><p>بناگاه لشکر برایشان زدن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گزین کرد شمشیر زن سی‌هزار</p></div>
<div class="m2"><p>همه رزمجو از در کارزار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چنین گفت با نامداران جنگ</p></div>
<div class="m2"><p>که ما را کنون نیست جای درنگ</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به راه بیابان برون تاختند</p></div>
<div class="m2"><p>همه جنگ را گردن افراختند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز هر سو فرستاد بی‌مر سپاه</p></div>
<div class="m2"><p>بدان سرکشان تا بگیرند راه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گرازه چو گرد سپه را بدید</p></div>
<div class="m2"><p>بیامد سپه را همه بنگرید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدید آنک شد روی گیتی سیاه</p></div>
<div class="m2"><p>درفش سپهدار توران سپاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ازانجا چو باد دمان گشت باز</p></div>
<div class="m2"><p>تو گفتی به زخم اندر آمد گراز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بیامد دمان تا به نخچیرگاه</p></div>
<div class="m2"><p>تهمتن همی خورد می با سپاه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنین گفت با رستم شیرمرد</p></div>
<div class="m2"><p>که برخیز و از خرمی بازگرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که چندان سپاهست کاندازه نیست</p></div>
<div class="m2"><p>ز لشکر بلندی و پستی یکیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>درفش جفاپیشه افراسیاب</p></div>
<div class="m2"><p>همی تابد از گرد چون آفتاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو بشنید رستم بخندید سخت</p></div>
<div class="m2"><p>بدو گفت با ماست پیروز بخت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو از شاه ترکان چه ترسی چنین</p></div>
<div class="m2"><p>ز گرد سواران توران زمین</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سپاهش فزون نیست از صدهزار</p></div>
<div class="m2"><p>عنان پیچ و بر گستوان‌ور سوار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدین دشت کین بر گر از ما یکی‌ست</p></div>
<div class="m2"><p>همی جنگ ترکان بچشم اندکی‌ست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شده هفت گرد سوار انجمن</p></div>
<div class="m2"><p>چنین نامبردار و شمشیرزن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یکی باشد از ما وزیشان هزار</p></div>
<div class="m2"><p>سپه چند باید ز ترکان شمار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>برین دشت اگر ویژه تنها منم</p></div>
<div class="m2"><p>که بر پشت گلرنگ در جوشنم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چنو کینه خواهی بیاید مرا</p></div>
<div class="m2"><p>از ایران سپاهی نباید مرا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تو ای می‌گسار از می بابلی</p></div>
<div class="m2"><p>بپیمای تا سر یکی بلبلی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بپیمود می ساقی و داد زود</p></div>
<div class="m2"><p>تهمتن شد از دادنش شاد زود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به کف بر نهاد آن درخشنده جام</p></div>
<div class="m2"><p>نخستین ز کاووس کی برد نام</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که شاه زمانه مرا یاد باد</p></div>
<div class="m2"><p>همیشه بروبومش آباد باد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ازان پس تهمتن زمین داد بوس</p></div>
<div class="m2"><p>چنین گفت کاین باده بر یاد طوس</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سران جهاندار برخاستند</p></div>
<div class="m2"><p>ابا پهلوان خواهش آراستند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که ما را بدین جام می جای نیست</p></div>
<div class="m2"><p>به می با تو ابلیس را پای نیست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>می و گرز یک زخم و میدان جنگ</p></div>
<div class="m2"><p>جز از تو کسی را نیامد به چنگ</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>می بابلی سرخ در جام زرد</p></div>
<div class="m2"><p>تهمتن بروی زواره بخورد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زواره چو بلبل به کف برنهاد</p></div>
<div class="m2"><p>هم از شاه کاووس کی کرد یاد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بخورد و ببوسید روی زمین</p></div>
<div class="m2"><p>تهمتن برو برگرفت آفرین</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که جام برادر برادر خورد</p></div>
<div class="m2"><p>هژبر آنک او جام می بشکرد</p></div></div>