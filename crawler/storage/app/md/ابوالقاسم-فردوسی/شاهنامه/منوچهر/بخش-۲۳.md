---
title: >-
    بخش ۲۳
---
# بخش ۲۳

<div class="b" id="bn1"><div class="m1"><p>همی رند دستان گرفته شتاب</p></div>
<div class="m2"><p>چو پرنده مرغ و چو کشتی برآب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی را نبد ز آمدنش آگهی</p></div>
<div class="m2"><p>پذیره نرفتند با فرهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خروشی برآمد ز پرده سرای</p></div>
<div class="m2"><p>که آمد ز ره زال فرخنده‌رای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پذیره شدش سام یل شادمان</p></div>
<div class="m2"><p>همی داشت اندر برش یک زمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرود آمد از باره بوسید خاک</p></div>
<div class="m2"><p>بگفت آن کجا دید و بشنید پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشست از بر تخت پرمایه سام</p></div>
<div class="m2"><p>ابا زال خرم دل و شادکام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنهای سیندخت گفتن گرفت</p></div>
<div class="m2"><p>لبش گشت خندان نهفتن گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین گفت کامد ز کابل پیام</p></div>
<div class="m2"><p>پیمبر زنی بود سیندخت نام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز من خواست پیمان و دادم زمان</p></div>
<div class="m2"><p>که هرگز نباشم بدو بدگمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هر چیز کز من به خوبی بخواست</p></div>
<div class="m2"><p>سخنها بران برنهادیم راست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخست آنکه با ماه کابلستان</p></div>
<div class="m2"><p>شود جفت خورشید زابلستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر آنکه زی او به مهمان شویم</p></div>
<div class="m2"><p>بران دردها پاک درمان شویم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرستاده‌ای آمد از نزد اوی</p></div>
<div class="m2"><p>که پردخته شد کار بنمای روی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون چیست پاسخ فرستاده را</p></div>
<div class="m2"><p>چه گوییم مهراب آزاده را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز شادی چنان شد دل زال سام</p></div>
<div class="m2"><p>که رنگش سراپای شد لعل فام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنین داد پاسخ که ای پهلوان</p></div>
<div class="m2"><p>گر ایدون که بینی به روشن روان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپه رانی و ما به کابل شویم</p></div>
<div class="m2"><p>بگوییم زین در سخن بشنویم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دستان نگه کرد فرخنده سام</p></div>
<div class="m2"><p>بدانست کورا ازین چیست کام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سخن هر چه از دخت مهراب نیست</p></div>
<div class="m2"><p>به نزدیک زال آن جز از خواب نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بفرمود تا زنگ و هندی درای</p></div>
<div class="m2"><p>زدند و گشادند پرده سرای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هیونی برافگند مرد دلیر</p></div>
<div class="m2"><p>بدان تا شود نزد مهراب شیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگوید که آمد سپهبد ز راه</p></div>
<div class="m2"><p>ابا زال با پیل و چندی سپاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرستاده تازان به کابل رسید</p></div>
<div class="m2"><p>خروشی برآمد چنان چون سزید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنان شاد شد شاه کابلستان</p></div>
<div class="m2"><p>ز پیوند خورشید زابلستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که گفتی همی جان برافشاندند</p></div>
<div class="m2"><p>ز هر جای رامشگران خواندند</p></div></div>