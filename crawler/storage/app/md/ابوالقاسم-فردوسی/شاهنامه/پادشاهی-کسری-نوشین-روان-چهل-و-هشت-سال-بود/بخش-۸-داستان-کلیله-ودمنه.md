---
title: >-
    بخش ۸ - داستان کلیله ودمنه
---
# بخش ۸ - داستان کلیله ودمنه

<div class="b" id="bn1"><div class="m1"><p>نگه کن که شادان برزین چه گفت</p></div>
<div class="m2"><p>بدانگه که بگشاد راز ازنهفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدرگه شهنشاه نوشین روان</p></div>
<div class="m2"><p>که نامش بماناد تا جاودان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهردانشی موبدی خواستی</p></div>
<div class="m2"><p>که درگه بدیشان بیاراستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پزشک سخنگوی وکنداوران</p></div>
<div class="m2"><p>بزرگان وکارآزموده سران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابرهردری نامور مهتری</p></div>
<div class="m2"><p>کجا هرسری رابدی افسری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پزشک سراینده برزوی بود</p></div>
<div class="m2"><p>بنیرو رسیده سخنگوی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهردانشی داشتی بهره‌ای</p></div>
<div class="m2"><p>بهربهره‌ای درجهان شهره‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان بد که روزی بهنگام بار</p></div>
<div class="m2"><p>بیامد برنامور شهریار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین گفت کای شاه دانش‌پذیر</p></div>
<div class="m2"><p>پژوهنده ویافته یادگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من امروز دردفتر هندوان</p></div>
<div class="m2"><p>همی‌بنگریدم بروشن روان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین بدنبشته که برکوه هند</p></div>
<div class="m2"><p>گیاییست چینی چورومی پرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که آن را چو گردآورد رهنمای</p></div>
<div class="m2"><p>بیامیزد ودانش آرد بجای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بر مرده بپراگند بی‌گمان</p></div>
<div class="m2"><p>سخنگوی گرددهم اندر زمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون من بدستوری شهریار</p></div>
<div class="m2"><p>بپیمایم این راه دشوار خوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسی دانشی رهنمای آورم</p></div>
<div class="m2"><p>مگر کین شگفتی بجای آورم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تن مرده گرزنده گردد رواست</p></div>
<div class="m2"><p>که نوشین روان برجهان پادشاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت شاه این نشاید بدن</p></div>
<div class="m2"><p>مگر آزموده رابباید شدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببر نامهٔ من بر رای هند</p></div>
<div class="m2"><p>نگر تاکه باشد بت آرای هند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدین کارباخویشتن یارخواه</p></div>
<div class="m2"><p>همه یاری ازبخت بیدار خواه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر نوشگفتی شود درجهان</p></div>
<div class="m2"><p>که این گفته رمزی بود درنهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ببر هرچ باید به نزدیک رای</p></div>
<div class="m2"><p>کزو بایدت بی‌گمان رهنمای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درگنج بگشاد نوشین روان</p></div>
<div class="m2"><p>زچیزی که بد درخور خسروان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز دینار و دیبا و خز و حریر</p></div>
<div class="m2"><p>ز مهر و ز افسر ز مشک و عبیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شتروار سیصد بیاراست شاه</p></div>
<div class="m2"><p>فرستاده برداشت آمد به راه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیامد بر رای ونامه بداد</p></div>
<div class="m2"><p>سربارها پیش اوبرگشاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو برخواند آن نامهٔ شاه رای</p></div>
<div class="m2"><p>بدو گفت کای مرد پاکیزه رای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زکسری مرا گنج بخشیده نیست</p></div>
<div class="m2"><p>همه لشکر وپادشاهی یکیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز داد و ز فر و ز اورند شاه</p></div>
<div class="m2"><p>وزان روشنی بخت وآن دستگاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نباشد شگفت ازجهاندار پاک</p></div>
<div class="m2"><p>که گر مردگان را برآرد زخاک</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برهمن بکوه اندرون هرک هست</p></div>
<div class="m2"><p>یکی دارد این رای رابا تودست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بت آرای وفرخنده دستور من</p></div>
<div class="m2"><p>هم آن گنج وپرمایه گنجور من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدونیک هندوستان پیش تست</p></div>
<div class="m2"><p>بزرگی مرا درکم وبیش تست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بیاراستندش به نزدیک رای</p></div>
<div class="m2"><p>یکی نامور چون ببایست جای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خورشگر فرستاد هم خوردنی</p></div>
<div class="m2"><p>همان پوشش نغز وگستردنی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برفت آن شب ورای زد با ردان</p></div>
<div class="m2"><p>بزرگان قنوج با بخردان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چوبرزد سر از کوه رخشنده روز</p></div>
<div class="m2"><p>پدید آمد آن شمع گیتی فروز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پزشکان فرزانه را خواند رای</p></div>
<div class="m2"><p>کسی کو بدانش بدی رهنمای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو برزوی بنهاد سرسوی کوه</p></div>
<div class="m2"><p>برفتند بااو پزشکان گروه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پیاده همه کوهساران بپای</p></div>
<div class="m2"><p>بپیمود با دانشی رهنمای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گیاها ز خشک و ز تر برگزید</p></div>
<div class="m2"><p>ز پژمرده و آنچ رخشنده دید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز هرگونه دارو ز خشک و ز تر</p></div>
<div class="m2"><p>همی بر پراگند بر مرده بر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی مرده زنده نگشت ازگیا</p></div>
<div class="m2"><p>همانا که سست آمد آن کیمیا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه کوه بسپرد یک یک بپای</p></div>
<div class="m2"><p>ابر رنج اوبرنیامد بجای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدانست کان کار آن پادشا ست</p></div>
<div class="m2"><p>که زنده است جاوید و فرمانرواست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دلش گشت سوزان ز تشویر شاه</p></div>
<div class="m2"><p>هم ازنامداران هم از رنج راه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وزان خواسته نیز کاورده بود</p></div>
<div class="m2"><p>زگفتار بیهوده آزرده بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زکارنبشته ببد تنگدل</p></div>
<div class="m2"><p>که آن مرد بیدانش و سنگدل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چرا خیره بر باد چیزی نبشت</p></div>
<div class="m2"><p>که بد بار آن رنج گفتار زشت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنین گفت زان پس بران بخردان</p></div>
<div class="m2"><p>که‌ای کاردیده ستوده ردان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که دانید داناتر از خویشتن</p></div>
<div class="m2"><p>کجا سرفرازد بدین انجمن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به پاسخ شدند انجمن همسخن</p></div>
<div class="m2"><p>که داننده پیرست ایدر کهن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به سال و خرد او ز ما مهترست</p></div>
<div class="m2"><p>به دانش ز هر مهتری بهترست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چنین گفت برزوی با هندوان</p></div>
<div class="m2"><p>که ای نامداران روشن روان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>برین رنجها برفزونی کنید</p></div>
<div class="m2"><p>مرا سوی او رهنمونی کنید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مگر کان سخنگوی دانای پیر</p></div>
<div class="m2"><p>بدین کار باشد مرا دستگیر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ببردند برزوی رانزد اوی</p></div>
<div class="m2"><p>پراندیشه دل سرپرازگفت وگوی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چونزدیک اوشد سخنگوی مرد</p></div>
<div class="m2"><p>همه رنجها پیش او یاد کرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>زکار نبشته که آمد پدید</p></div>
<div class="m2"><p>سخنها که ازکاردانان شنید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدو پیر دانا زبان برگشاد</p></div>
<div class="m2"><p>ز هر دانشی پیش اوک رد یاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که من در نبشته چنین یافتم</p></div>
<div class="m2"><p>بدان آرزو تیز بشتافتم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو زان رنجها برنیامد پدید</p></div>
<div class="m2"><p>ببایست ناچار دیگر شنید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گیا چون سخن دان و دانش چو کوه</p></div>
<div class="m2"><p>که همواره باشد مر او راشکوه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تن مرده چون مرد بیدانشست</p></div>
<div class="m2"><p>که دانا بهرجای با رامشست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدانش بود بی‌گمان زنده مرد</p></div>
<div class="m2"><p>چودانش نباشد بگردش مگرد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چومردم زدانایی آید ستوه</p></div>
<div class="m2"><p>گیاچوکلیله ست ودانش چوکوه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کتابی بدانش نماینده راه</p></div>
<div class="m2"><p>بیابی چوجویی توازگنج شاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو بشنید برزوی زو شاد شد</p></div>
<div class="m2"><p>همه رنج برچشم اوبادشد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بروآفرین کرد وشد نزد شاه</p></div>
<div class="m2"><p>بکردار آتش بپیمود راه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بیامد نیایش کنان پیش رای</p></div>
<div class="m2"><p>که تا جای باشد توبادی بجای</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کتابیست ای شاه گسترده کام</p></div>
<div class="m2"><p>که آن را بهندی کلیله ست نام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به مهرست تا درج درگنج شاه</p></div>
<div class="m2"><p>برای وبدانش نماینده راه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به گنج‌ور فرمان دهد تا زگنج</p></div>
<div class="m2"><p>سپارد بمن گر ندارد به رنج</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دژم گشت زان آرزو جان شاه</p></div>
<div class="m2"><p>بپیچید برخویشتن چندگاه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ببرزوی گفت این کس از ما نجست</p></div>
<div class="m2"><p>نه اکنون نه از روزگار نخست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ولیکن جهاندار نوشین روان</p></div>
<div class="m2"><p>اگر تن بخواهد ز ما یا روان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نداریم ازو باز چیزی که هست</p></div>
<div class="m2"><p>اگر سرفرازست اگر زیردست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ولیکن بخوانی مگر پیش ما</p></div>
<div class="m2"><p>بدان تا روان بداندیش ما</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نگوید به دل کان نبشتست کس</p></div>
<div class="m2"><p>بخوان و بدان و ببین پیش و پس</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بدو گفت برزوی کای شهریار</p></div>
<div class="m2"><p>ندارم فزون ز آنچ گویی مدار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>کلیله بیاورد گنجور شاه</p></div>
<div class="m2"><p>همی‌بود او را نماینده راه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>هران در که ازنامه بو خواندی</p></div>
<div class="m2"><p>همه روز بر دل همی‌راندی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز نامه فزون ز آنک بودیش یاد</p></div>
<div class="m2"><p>ز برخواندی نیز تا بامداد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>همی‌بود شادان دل و تن درست</p></div>
<div class="m2"><p>بدانش همی جان روشن بشست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو زو نامه رفتی بشاه جهان</p></div>
<div class="m2"><p>دری از کلیله نبشتی نهان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بدین چاره تا نامهٔ هندوان</p></div>
<div class="m2"><p>فرستاد نزدیک نوشین روان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدین گونه تا پاسخ نامه دید</p></div>
<div class="m2"><p>که دریای دانش برما رسید</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز ایوان بیامد به نزدیک رای</p></div>
<div class="m2"><p>بدستوری بازگشتن به جای</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو بگشاد دل رای بنواختش</p></div>
<div class="m2"><p>یکی خلعت هندویی ساختش</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>دو یاره بهاگیر و دو گوشوار</p></div>
<div class="m2"><p>یکی طوق پرگوهر شاهوار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>هم از شارهٔ هندی و تیغ هند</p></div>
<div class="m2"><p>همه روی آهن سراسر پرند</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بیامد ز قنوج برزوی شاد</p></div>
<div class="m2"><p>بسی دانش نوگرفته بیاد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز ره چون رسید اندر آن بارگاه</p></div>
<div class="m2"><p>نیایش کنان رفت نزدیک شاه</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بگفت آنچ از رای دید و شنید</p></div>
<div class="m2"><p>بجای گیا دانش آمد پدید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بدو گفت شاه‌ای پسندیده مرد</p></div>
<div class="m2"><p>کلیله روان مرا زنده کرد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تواکنون ز گنجور بستان کلید</p></div>
<div class="m2"><p>ز چیزی که باید بباید گزید</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بیامد خرد یافته سوی گنج</p></div>
<div class="m2"><p>به گنج‌ور بسیار ننمود رنج</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>درم بود و گوهر چپ و دست راست</p></div>
<div class="m2"><p>جز از جامهٔ شاه چیزی نخواست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گرانمایه دستی بپوشید و رفت</p></div>
<div class="m2"><p>بر گاه کسری خرامید تفت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چو آمد به نزدیک تختش فراز</p></div>
<div class="m2"><p>برو آفرین کرد و بردش نماز</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بدو گفت پس نامور شهریار</p></div>
<div class="m2"><p>که بی بدره و گوهر شاهوار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چرا رفتی ای رنج دیده ز گنج</p></div>
<div class="m2"><p>کسی را سزد گنج کو دید رنج</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چنین پاسخ آورد برزو بشاه</p></div>
<div class="m2"><p>که ای تاج تو برتر از چرخ ماه</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هرآنکس که او پوشش شاه یافت</p></div>
<div class="m2"><p>ببخت و بتخت مهی راه یافت</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دگر آنک با جامهٔ شهریار</p></div>
<div class="m2"><p>ببیند مرا مرد ناسازگار</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دل بدسگالان شود تار و تنگ</p></div>
<div class="m2"><p>بماند رخ دوست با آب و رنگ</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>یکی آرزو خواهم از شهریار</p></div>
<div class="m2"><p>که ماند ز من در جهان یادگار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو بنویسد این نامه بوزرجمهر</p></div>
<div class="m2"><p>گشاید برین رنج برزوی چهر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نخستین در از من کند یادگار</p></div>
<div class="m2"><p>به فرمان پیروزگر شهریار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بدان تا پس از مرگ من در جهان</p></div>
<div class="m2"><p>ز داننده رنجم نگردد نهان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بدو گفت شاه این بزرگ آرزوست</p></div>
<div class="m2"><p>بر اندازهٔ مرد آزاده خوست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ولیکن به رنج تو اندر خورست</p></div>
<div class="m2"><p>سخن گرچه از پایگه برترست</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به بوزرجمهر آن زمان شاه گفت</p></div>
<div class="m2"><p>که این آرزو را نشاید نهفت</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نویسنده از کلک چون خامه کرد</p></div>
<div class="m2"><p>ز بر زوی یک در سرنامه کرد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>نبشت او بران نامهٔ خسروی</p></div>
<div class="m2"><p>نبود آن زمان خط جز پهلوی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>همی‌بود با ارج در گنج شاه</p></div>
<div class="m2"><p>بدو ناسزا کس نکردی نگاه</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چنین تا بتازی سخن راندند</p></div>
<div class="m2"><p>ورا پهلوانی همی‌خواندند</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چو مامون روشن روان تازه کرد</p></div>
<div class="m2"><p>خور روز بر دیگر اندازه کرد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>دل موبدان داشت و رای کیان</p></div>
<div class="m2"><p>ببسته بهر دانشی بر میان</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>کلیله به تازی شد از پهلوی</p></div>
<div class="m2"><p>بدین سان که اکنون همی‌بشنوی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بتازی همی‌بود تا گاه نصر</p></div>
<div class="m2"><p>بدانگه که شد در جهان شاه نصر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>گرانمایه بوالفضل دستور اوی</p></div>
<div class="m2"><p>که اندر سخن بود گنجور اوی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بفرمود تا پارسی و دری</p></div>
<div class="m2"><p>نبشتند و کوتاه شد داوری</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>وزان پس چو پیوسته رای آمدش</p></div>
<div class="m2"><p>بدانش خرد رهنمای آمدش</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>همی‌خواست تا آشکار و نهان</p></div>
<div class="m2"><p>ازو یادگاری بود درجهان</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>گزارنده را پیش بنشاندند</p></div>
<div class="m2"><p>همه نامه بر رودکی خواندند</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>بپیوست گویا پراگنده را</p></div>
<div class="m2"><p>بسفت اینچنین در آگنده را</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بدان کو سخن راند آرایشست</p></div>
<div class="m2"><p>چو ابله بود جای بخشایشست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>حدیث پراگنده بپراگند</p></div>
<div class="m2"><p>چوپیوسته شد جان و مغزآگند</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>جهاندار تا جاودان زنده باد</p></div>
<div class="m2"><p>زمان و زمین پیش او بنده باد</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>از اندیشه دل را مدار ایچ تنگ</p></div>
<div class="m2"><p>که دوری تو از روزگار درنگ</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>گهی برفراز و گهی بر نشیب</p></div>
<div class="m2"><p>گهی با مراد و گهی با نهیب</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>ازین دو یکی نیز جاوید نیست</p></div>
<div class="m2"><p>ببودن تو را راه امید نیست</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>نگه کن کنون کار بوزرجمهر</p></div>
<div class="m2"><p>که از خاک برشد به گردان سپهر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>فراز آوریدش بخاک نژند</p></div>
<div class="m2"><p>همان کس که بردش با بر بلند</p></div></div>