---
title: >-
    بخش ۶
---
# بخش ۶

<div class="b" id="bn1"><div class="m1"><p>بغرید شنگل ز پیش سپاه</p></div>
<div class="m2"><p>منم گفت گرداوژن رزم‌خواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگویید کان مرد سگزی کجاست</p></div>
<div class="m2"><p>یکی کرد خواهم برو نیزه راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آواز شنگل برستم رسید</p></div>
<div class="m2"><p>ز لشکر نگه کرد و او را بدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفت هان آمدم رزمخواه</p></div>
<div class="m2"><p>نگر تا نگیری بلشکر پناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گفت رستم که از کردگار</p></div>
<div class="m2"><p>نجستم جزین آرزوی آشکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که بیگانه‌ای زان بزرگ انجمن</p></div>
<div class="m2"><p>دلیری کند رزم جوید ز من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه سقلاب ماند ازیشان نه هند</p></div>
<div class="m2"><p>نه شمشیر هندی نه چینی پرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پی و بیخ ایشان نمانم بجای</p></div>
<div class="m2"><p>نمانم بترکان سر و دست و پای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر شنگل آمد به آواز گفت</p></div>
<div class="m2"><p>که ای بدنژاد فرومایه جفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا نام رستم کند زال زر</p></div>
<div class="m2"><p>تو سگزی چرا خوانی ای بدگهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نگه کن که سگزی کنون مرگ تست</p></div>
<div class="m2"><p>کفن بی‌گمان جوشن و ترگ تست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی گشت با او به آوردگاه</p></div>
<div class="m2"><p>میان دو صف برکشیده سپاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی نیزه زد برگرفتش ز زین</p></div>
<div class="m2"><p>نگونسار کرد و بزد بر زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برو بر گذر کرد و او را نخست</p></div>
<div class="m2"><p>بشمشیر برد آنگهی شیر دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برفتند زان روی کنداوران</p></div>
<div class="m2"><p>بزهر آب داده پرندآوران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شنگل گریزان شد از پیلتن</p></div>
<div class="m2"><p>پراگنده گشتند زان انجمن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو بهره ازیشان بشمشیر کشت</p></div>
<div class="m2"><p>دلیران توران نمودند پشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بجان شنگل از دست رستم بجست</p></div>
<div class="m2"><p>زره بود و جوشن تنش را نخست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین گفت شنگل که این مرد نیست</p></div>
<div class="m2"><p>کس او را بگیتی هم آورد نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی ژنده پیلست بر پشت کوه</p></div>
<div class="m2"><p>مگر رزم سازند یکسر گروه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بتنها کسی رزم با اژدها</p></div>
<div class="m2"><p>نجوید چو جوید نیابد رها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدو گفت خاقان ترا بامداد</p></div>
<div class="m2"><p>دگر بود رای و دگر بود یاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپه را بفرمود تا همگروه</p></div>
<div class="m2"><p>برانند یکسر بکردار کوه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرافراز را در میان آورند</p></div>
<div class="m2"><p>تنومند را جان زیان آورند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بشمشیر برد آن زمان شیر دست</p></div>
<div class="m2"><p>چپ لشکر چینیان برشکست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آنگه که خنجر برانداختی</p></div>
<div class="m2"><p>همه ره تن بی سر انداختی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه با جنگ او کوه را پای بود</p></div>
<div class="m2"><p>نه با خشم او پیل را جای بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدان سان گرفتند گرد اندرش</p></div>
<div class="m2"><p>که خورشید تاریک شد از برش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان نیزه و خنجر و گرز و تیر</p></div>
<div class="m2"><p>که شد ساخته بر یل شیرگیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گمان برد کاندر نیستان شدست</p></div>
<div class="m2"><p>ز خون روی کشور میستان شدست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیک زخم ده نیزه کردی قلم</p></div>
<div class="m2"><p>خروشان و جوشان و دشمن دژم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دلیران ایران پس پشت اوی</p></div>
<div class="m2"><p>بکینه دل آگنده و جنگ جوی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بس نیزه و گرز و گوپال و تیغ</p></div>
<div class="m2"><p>تو گفتی همی ژاله بارد ز میغ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز کشته همه دشت آوردگاه</p></div>
<div class="m2"><p>تن و پشت و سر بود و ترگ و کلاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز چینی و شگنی و از هندوی</p></div>
<div class="m2"><p>ز سقلاب و هری و از پهلوی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سپه بود چون خاک در پای کوه</p></div>
<div class="m2"><p>ز یک مرد سگزی شده همگروه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که با او بجنگ اندرون پای نیست</p></div>
<div class="m2"><p>چنو در جهان لشکر آرای نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کسی کو کند زین سخن داستان</p></div>
<div class="m2"><p>نباشد خردمند همداستان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که پرخاشخر نامور صد هزار</p></div>
<div class="m2"><p>بسنده نبودند با یک سوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ازین کین بد آمد بافراسیاب</p></div>
<div class="m2"><p>ز رستم کجا یابد آرام و خواب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین گفت رستم بایرانیان</p></div>
<div class="m2"><p>کزین جنگ دشمن کند جان زیان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هم‌اکنون ز پیلان و از خواسته</p></div>
<div class="m2"><p>همان تخت و آن تاج آراسته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ستانم ز چینی بایران دهم</p></div>
<div class="m2"><p>بدان شادمان روز فرخ نهم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نباشد جز ایرانیان شاد کس</p></div>
<div class="m2"><p>پی رخش و ایزد مرا یار بس</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>یکی را ز شگنان و سقلاب و چین</p></div>
<div class="m2"><p>نمانم که پی برنهد بر زمین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که امروز پیروزی روز ماست</p></div>
<div class="m2"><p>بلند آسمان لشکر افروز ماست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر ایدونک نیرو دهد دادگر</p></div>
<div class="m2"><p>پدید آورد رخش رخشان هنر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برین دشت من گورستانی کنم</p></div>
<div class="m2"><p>برومند را شارستانی کنم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی از شما سوی لشکر شوید</p></div>
<div class="m2"><p>بکوشید و با باد همبر شوید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بکوبید چون من بجنبم ز جای</p></div>
<div class="m2"><p>شما برفرازید سنج و درای</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زمین را سراسر کنید آبنوس</p></div>
<div class="m2"><p>بگرد سواران و آوای کوس</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بکوبید گوپال و گرز گران</p></div>
<div class="m2"><p>چو پولاد را پتک آهنگران</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از انبوه ایشان مدارید باک</p></div>
<div class="m2"><p>ز دریا بابر اندر آرید خاک</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه دیده بر مغفر من نهید</p></div>
<div class="m2"><p>چو من بر خروشم دمید و دهید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدرید صفهای سقلاب و چین</p></div>
<div class="m2"><p>نباید که بیند هوا را زمین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وزان جایگه رفت چون پیل مست</p></div>
<div class="m2"><p>یکی گرزهٔ گاوپیکر بدست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خروشان سوی میمنه راه جست</p></div>
<div class="m2"><p>ز لشکر سوی کندر آمد نخست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه میمنه پاک بر هم درید</p></div>
<div class="m2"><p>بسی ترگ و سر بد که تن را ندید</p></div></div>