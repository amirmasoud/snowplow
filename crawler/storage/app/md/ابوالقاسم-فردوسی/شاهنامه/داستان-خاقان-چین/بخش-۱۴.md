---
title: >-
    بخش ۱۴
---
# بخش ۱۴

<div class="b" id="bn1"><div class="m1"><p>وزان جایگه لشکر اندر کشید</p></div>
<div class="m2"><p>بیک منزلی بر یکی شهر دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا نام آن شهر بیداد بود</p></div>
<div class="m2"><p>دژی بود وز مردم آباد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه خوردنیشان ز مردم بدی</p></div>
<div class="m2"><p>پری چهره‌ای هر زمان گم بدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخوان چنان شهریار پلید</p></div>
<div class="m2"><p>نبودی جز از کودک نارسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرستندگانی که نیکو بدی</p></div>
<div class="m2"><p>به دیدار و بالا بی‌آهو بدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن ساختندی بخوان بر خورش</p></div>
<div class="m2"><p>بدین گونه بد شاه را پرورش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تهمتن بفرمود تا سه هزار</p></div>
<div class="m2"><p>زرهدار بر گستوان ور سوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان دژ فرستاد با گستهم</p></div>
<div class="m2"><p>دو گرد خردمند با اوبهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرین مرد را نام کافور بود</p></div>
<div class="m2"><p>که او را بران شهر منشور بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بپوشید کافور خفتان جنگ</p></div>
<div class="m2"><p>همه شهر با او بسان پلنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمندافگن و زورمندان بدند</p></div>
<div class="m2"><p>بزرم اندرون پیل دندان بدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو گستهم گیتی بران گونه دید</p></div>
<div class="m2"><p>جهان در کف دیو وارونه دید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بفرمود تا تیر باران کنند</p></div>
<div class="m2"><p>بریشان کمین سواران کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین گفت کافور با سرکشان</p></div>
<div class="m2"><p>که سندان نگیرد ز پیکان نشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه تیغ و گرز و کمند آورید</p></div>
<div class="m2"><p>سر سرکشان را ببند آورید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمانی بران سان برآویختند</p></div>
<div class="m2"><p>که آتش ز دریا برانگیختند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فراوان ز ایرانیان کشته شد</p></div>
<div class="m2"><p>بسر بر سپهر بلا گشته شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببیژن چنین گفت گستهم زود</p></div>
<div class="m2"><p>که لختی عنانت بباید بسود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برستم بگویی که چندین مایست</p></div>
<div class="m2"><p>بجنبان عنان با سواری دویست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بشد بیژن گیو برسان باد</p></div>
<div class="m2"><p>سخن بر تهمتن همه کرد یاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گران کرد رستم زمانی رکیب</p></div>
<div class="m2"><p>ندانست لشکر فراز از نشیب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدانسان بیامد بدان رزمگاه</p></div>
<div class="m2"><p>که باد اندر آید ز کوه سیاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فراوان ز ایرانیان کشته دید</p></div>
<div class="m2"><p>بسی سرکش از جنگ برگشته دید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بکافور گفت ای سگ بدگهر</p></div>
<div class="m2"><p>کنون رزم و رنج تو آمد بسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی حمله آورد کافور سخت</p></div>
<div class="m2"><p>بران بارور خسروانی درخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بینداخت تیغی بکردار تیر</p></div>
<div class="m2"><p>که آید مگر بر یل شیرگیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بپیش اندر آورد رستم سپر</p></div>
<div class="m2"><p>فرو ماند کافور پرخاشخر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کمندی بینداخت بر سوی طوس</p></div>
<div class="m2"><p>بسی کرد رستم برو بر فسوس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عمودی بزد بر سرش پور زال</p></div>
<div class="m2"><p>که بر هم شکستش سر و ترگ و یال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنین تا در دژ یکی حمله برد</p></div>
<div class="m2"><p>بزرگان نبودند پیدا ز خرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در دژ ببستند وز باره تیز</p></div>
<div class="m2"><p>برآمد خروشیدن رستخیز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بگفتند کای مرد بازور و هوش</p></div>
<div class="m2"><p>برین گونه با ما بکینه مکوش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پدر نام تو چون بزادی چه کرد</p></div>
<div class="m2"><p>کمندافگنی گر سپهر نبرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دریغست رنج اندرین شارستان</p></div>
<div class="m2"><p>که داننده خواند ورا کارستان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو تور فریدون ز ایران براند</p></div>
<div class="m2"><p>ز هر گونه دانندگان را بخواند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی باره افگند زین گونه پی</p></div>
<div class="m2"><p>ز سنگ و ز خشت و ز چوب و ز نی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برآودر ازینسان بافسون و رنج</p></div>
<div class="m2"><p>بپالود رنج و تهی کرد گنج</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسی رنج بردند مردان مرد</p></div>
<div class="m2"><p>کزین بارهٔ دژ برآرند گرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نبدکس بدین شارستان پادشا</p></div>
<div class="m2"><p>بدین رنج بردن نیارد بها</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سلیحست و ایدر بسی خوردنی</p></div>
<div class="m2"><p>بزیر اندرون راه آوردنی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر سالیان رنج و رزم آوری</p></div>
<div class="m2"><p>نباشد بدستت جز از داوری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نیاید برین باره بر منجنیق</p></div>
<div class="m2"><p>از افسون سلم و دم جاثلیق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو بشنید رستم پر اندیشه شد</p></div>
<div class="m2"><p>دلش از غم و درد چون بیشه شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یکی رزم کرد آن نه بر آرزوی</p></div>
<div class="m2"><p>سپاه اندر آورد بر چار سوی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بیک روی گودرز و یک روی طوس</p></div>
<div class="m2"><p>پس پشت او پیل با بوق و کوس</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بیک روی بر لشکر زابلی</p></div>
<div class="m2"><p>زره‌دار با خنجر کابلی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو آن دید رستم کمان برگرفت</p></div>
<div class="m2"><p>همه دژ بدو ماند اندر شگفت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر آنکس که از باره سر بر زدی</p></div>
<div class="m2"><p>زمانه سرش را بهم در زدی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ابا مغز پیکان همی راز گفت</p></div>
<div class="m2"><p>ببدسازگاری همی گشت جفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بن باره زان پس بکندن گرفت</p></div>
<div class="m2"><p>ز دیوار مردم فگندن گرفت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ستونها نهادند زیر اندرش</p></div>
<div class="m2"><p>بیالود نفط سیاه از برش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو نیمی ز دیوار دژکنده شد</p></div>
<div class="m2"><p>بچوب اندر آتش پراگنده شد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فرود آمد آن بارهٔ تور گرد</p></div>
<div class="m2"><p>ز هر سو سپاه اندر آمد بگرد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بفرمود رستم که جنگ آورید</p></div>
<div class="m2"><p>کمانها و تیر خدنگ آورید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گوان از پی گنج و فرزند خویش</p></div>
<div class="m2"><p>همان از پی بوم و پیوند خویش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همه سر بدادند یکسر بباد</p></div>
<div class="m2"><p>گرامی‌تر آنکو ز مادر نزاد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دلیران پیاده شدند آن زمان</p></div>
<div class="m2"><p>سپرهای چینی و تیر و کمان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>برفتند با نیزه‌داران بهم</p></div>
<div class="m2"><p>بپیش اندرون بیژن و گستهم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دم آتش تیز و باران تیر</p></div>
<div class="m2"><p>هزیمت بود زان سپس ناگزیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو از بارهٔ دژ بیرون شدند</p></div>
<div class="m2"><p>گریزان گریزان بهامون شدند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در دژ ببست آن زمان جنگجوی</p></div>
<div class="m2"><p>بتاراج و کشتن نهادند روی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چه مایه بکشتند و چندی اسیر</p></div>
<div class="m2"><p>ببردند زان شهر برنا و پیر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بسی سیم و زر و گرانمایه چیز</p></div>
<div class="m2"><p>ستور و غلام و پرستار نیز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تهمتن بیامد سر و تن بشست</p></div>
<div class="m2"><p>بپیش جهانداور آمد نخست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز پیروز گشتن نیایش گرفت</p></div>
<div class="m2"><p>جهان آفرین را ستایش گرفت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بایرانیان گفت با کردگار</p></div>
<div class="m2"><p>بیامد نهانی هم از آشکار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بپیروزی اندر نیایش کنید</p></div>
<div class="m2"><p>جهان آفرین را ستایش کنید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بزرگان بپیش جهان‌آفرین</p></div>
<div class="m2"><p>نیایش گرفتند سر بر زمین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو از پاک یزدان بپرداختند</p></div>
<div class="m2"><p>بران نامدار آفرین ساختند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که هر کس که چون تو نباشد بجنگ</p></div>
<div class="m2"><p>نشستن به آید بنام و بننگ</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تن پیل داری و چنگال شیر</p></div>
<div class="m2"><p>زمانی نباشی ز پیگار سیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تهمتن چنین گفت کین زور و فر</p></div>
<div class="m2"><p>یکی خلعتی باشد از دادگر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>شما سربسر بهره دارید زین</p></div>
<div class="m2"><p>نه جای گله‌ست از جهان آفرین</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بفرمود تا گیو با ده هزار</p></div>
<div class="m2"><p>سپردار و بر گستوان ور سوار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شود تازیان تا بمرز ختن</p></div>
<div class="m2"><p>نماند که ترکان شوند انجمن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو بنمود شب جعد زلف سیاه</p></div>
<div class="m2"><p>از اندیشه خمیده شد پشت ماه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بشد گیو با آن سواران جنگ</p></div>
<div class="m2"><p>سه روز اندر آن تاختن شد درنگ</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدانگه که خورشید بنمود تاج</p></div>
<div class="m2"><p>برآمد نشست از بر تخت عاج</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز توران بیامد سرافراز گیو</p></div>
<div class="m2"><p>گرفته بسی نامداران نیو</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بسی خوب چهر بتان طراز</p></div>
<div class="m2"><p>گرانمایه اسپان و هرگونه ساز</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>فرستاد یک نیمه نزدیک شاه</p></div>
<div class="m2"><p>ببخشید دیگر همه بر سپاه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>وزان پس چو گودرز و چون طوس و گیو</p></div>
<div class="m2"><p>چو گستهم و شیدوش و فرهاد نیو</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ابا بیژن گیو برخاستند</p></div>
<div class="m2"><p>یکی آفرین نو آراستند</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چنین گفت گودرز کای سرفراز</p></div>
<div class="m2"><p>جهان را بمهر تو آمد نیاز</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>نشاید که بی‌آفرین تو لب</p></div>
<div class="m2"><p>گشاییم زین پس بروز و بشب</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>کسی کو بپیمود روی زمین</p></div>
<div class="m2"><p>جهان دید و آرام و پرخاش و کین</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بیک جای زین بیش لشکر ندید</p></div>
<div class="m2"><p>نه از موبد سالخورده شنید</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز شاهان و پیلان وز تخت عاج</p></div>
<div class="m2"><p>ز مردان و اسپان و از گنج و تاج</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ستاره بدان دشت نظاره بود</p></div>
<div class="m2"><p>که این لشکر از جنگ بیچاره بود</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بگشتیم گرد دژ ایدر بسی</p></div>
<div class="m2"><p>ندیدیم جز کینه درمان کسی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>که خوشان بدیم از دم اژدها</p></div>
<div class="m2"><p>کمان تو آورد ما را رها</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>توی پشت ایران و تاج سران</p></div>
<div class="m2"><p>سزاوار و ما پیش تو کهتران</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>مکافات این کار یزدان کند</p></div>
<div class="m2"><p>که چهر تو همواره خندان کند</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بپاداش تو نیست‌مان دسترس</p></div>
<div class="m2"><p>زبانها پر از آفرینست و بس</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بزرگیت هر روز بافزون ترست</p></div>
<div class="m2"><p>هنرمند رخش تو صد لشکرست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تهمتن بریشان گرفت آفرین</p></div>
<div class="m2"><p>که آباد بادا بگردان زمین</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>مرا پشت ز آزادگانست راست</p></div>
<div class="m2"><p>دل روشنم بر زبانم گواست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ازان پس چنین گفت کایدر سه روز</p></div>
<div class="m2"><p>بباشیم شادان و گیتی فروز</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>چهارم سوی جنگ افراسیاب</p></div>
<div class="m2"><p>برانیم و آتش برآریم ز آب</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>همه نامداران بگفتار اوی</p></div>
<div class="m2"><p>ببزم و بخوردند نهادند روی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>پس آگاهی آمد بافراسیاب</p></div>
<div class="m2"><p>که بوم و بر از دشمنان شد خراب</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>دلش زان سخن پر ز تیمار شد</p></div>
<div class="m2"><p>همه پرنیان بر تنش خار شد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بدل گفت پیگار او کار کیست</p></div>
<div class="m2"><p>سپاهست بسیار و سالار کیست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>گر آنست رستم که من دیده‌ام</p></div>
<div class="m2"><p>بسی از نبردش بپیچیده‌ام</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بپیچید وزان پس به آواز گفت</p></div>
<div class="m2"><p>که با او که داریم در جنگ جفت</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>یکی کودکی بود برسان نی</p></div>
<div class="m2"><p>که من لشکر آورده بودم بری</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بیامد تن من ز زین برگرفت</p></div>
<div class="m2"><p>فرو ماند زان لشکر اندر شگفت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چنین گفت لشکر بافراسیاب</p></div>
<div class="m2"><p>که چندین سر از جنگ رستم متاب</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>تو آنی که از خاک آوردگاه</p></div>
<div class="m2"><p>همی جوش خون اندر آری بماه</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>سلیحست بسیار و مردان جنگ</p></div>
<div class="m2"><p>دل از کار رستم چه داری بتنگ</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ز جنگ سواری تو غمگین مشو</p></div>
<div class="m2"><p>نگه کن بدین نامداران نو</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چنان دان که او یکسر از آهنست</p></div>
<div class="m2"><p>اگر چه دلیرست هم یک تنست</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>سخنهای کوتاه زو شد دراز</p></div>
<div class="m2"><p>تو با لشکری چارهٔ او را بساز</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>سرش را ز زین اندرآور بخاک</p></div>
<div class="m2"><p>ازان پس خود از شاه ایران چه باک</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نه کیخسرو آباد ماند نه گنج</p></div>
<div class="m2"><p>نداریم این زرم کردن برنج</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نگه کن بدین لشکر نامدار</p></div>
<div class="m2"><p>جوانان و شایستهٔ کارزار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ز بهر بر و بوم و پیوند خویش</p></div>
<div class="m2"><p>زن و کودک خرد و فرزند خویش</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>همه سر به سر تن به کشتن دهیم</p></div>
<div class="m2"><p>به آید که گیتی به دشمن دهیم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو بشنید افراسیاب این سخن</p></div>
<div class="m2"><p>فراموش کرد آن نبرد کهن</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بفرمود تا لشکر آراستند</p></div>
<div class="m2"><p>بکین نو از جای برخاستند</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ز بوم نیاکان وز شهر خویش</p></div>
<div class="m2"><p>یکی تازه اندیشه بنهاد پیش</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چنین داد پاسخ که من ساز جنگ</p></div>
<div class="m2"><p>بپیش آورم چون شود کار تنگ</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>نمانم که کیخسرو از تخت خویش</p></div>
<div class="m2"><p>شود شاد و پدرام از بخت خویش</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>سر زابلی را بروز نبرد</p></div>
<div class="m2"><p>بچنگ دراز اندر آرم بگرد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>برو سرکشان آفرین خواندند</p></div>
<div class="m2"><p>سرافراز را سوی کین خواندند</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>که جاوید و شادان و پیروز باش</p></div>
<div class="m2"><p>بکام دلت گیتی افروز باش</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>سپهبد بسی جنگها دیده بود</p></div>
<div class="m2"><p>ز هر کار بهری پسندیده بود</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>یکی شیر دل بود فرغار نام</p></div>
<div class="m2"><p>قفس دیده و جسته چندی ز دام</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ز بیگانگان جای پردخته کرد</p></div>
<div class="m2"><p>بفرغار گفت ای گرانمایه مرد</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>هم اکنون برو سوی ایران سپاه</p></div>
<div class="m2"><p>نگه کن بدین رستم رزمخواه</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>سواران نگه کن که چنداند و چون</p></div>
<div class="m2"><p>که دارد برین بوم و بر رهنمون</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>وزان نامداران پرخاشجوی</p></div>
<div class="m2"><p>ببینی که چنداند و بر چند روی</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ز گردان پهلومنش چند مرد</p></div>
<div class="m2"><p>که آورد سازند روز نبرد</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چو فرغار برگشت و آمد براه</p></div>
<div class="m2"><p>بکارآگهی شد بایران سپاه</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>غمی شد دل مرد پرخاشجوی</p></div>
<div class="m2"><p>ببیگانگان ایچ ننمود روی</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>فرستاد و فرزند را پیش خواند</p></div>
<div class="m2"><p>بسی راز بایسته با او براند</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>بشیده چنین گفت کای پر خرد</p></div>
<div class="m2"><p>سپاه تو تیمار تو کی خورد</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چنین دان که این لشکر بی‌شمار</p></div>
<div class="m2"><p>که آمد برین مرز چندین هزار</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>سپهدارشان رستم شیر دل</p></div>
<div class="m2"><p>که از خاک سازد بشمشیر گل</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>گو پیلتن رستم زابلیست</p></div>
<div class="m2"><p>ببین تا مر او را هم‌آورد کیست</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو کاموس و منشور و خاقان چین</p></div>
<div class="m2"><p>گهار و چو گرگوی با آفرین</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>دگر کندر و شنگل آن شاه هند</p></div>
<div class="m2"><p>سپاهی ز کشمیر تا پیش سند</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>بنیروی این رستم شیر گیر</p></div>
<div class="m2"><p>بکشتند و بردند چندی اسیر</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>چهل روز بالشکر آویز بود</p></div>
<div class="m2"><p>گهی رزم و گه بزم و پرهیز بود</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>سرانجام رستم بخم کمند</p></div>
<div class="m2"><p>ز پیل اندر آورد و بنهاد بند</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>سواران و گردان هر کشوری</p></div>
<div class="m2"><p>ز هر سو که بود از بزرگان سری</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بدین کشور آمد کنون زین نشان</p></div>
<div class="m2"><p>همان تاجداران گردنکشان</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>من ایدر نمانم بسی گنج و تخت</p></div>
<div class="m2"><p>که گردان شدست اندرین کار سخت</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>کنون هرچ گنجست و تاج و کمر</p></div>
<div class="m2"><p>همان طوق زرین و زرین سپر</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>فرستم همه سوی الماس رود</p></div>
<div class="m2"><p>نه هنگام جامست و بزم و سرود</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>هراسانم از رستم تیز چنگ</p></div>
<div class="m2"><p>تن‌آسان که باشد بکام نهنگ</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>بمردم نماند بروز نبرد</p></div>
<div class="m2"><p>نپیچد ز بیم و ننالد ز درد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ز نیزه نترسد نه از تیغ تیز</p></div>
<div class="m2"><p>برآرد ز دشمن همی رستخیز</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>تو گفتی که از روی وز آهنست</p></div>
<div class="m2"><p>نه مردم نژادست کهرمنست</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>سلیحست چندان برو روز کین</p></div>
<div class="m2"><p>که سیر آمد از بار پشت زمین</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>زره دارد و جوشن و خود و گبر</p></div>
<div class="m2"><p>بغرد بکردار غرنده ابر</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>نه برتابد آهنگ او ژنده پیل</p></div>
<div class="m2"><p>نه کشتی سلیحش بدریای نیل</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>یکی کوه زیرش بکردار باد</p></div>
<div class="m2"><p>تو گویی که از باد دارد نژاد</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>تگ آهوان دارد و هول شیر</p></div>
<div class="m2"><p>بناورد با شیر گردد دلیر</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>سخن گوید ار زو کنی خواستار</p></div>
<div class="m2"><p>بدریا چو کشتی بود روز کار</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>مرا با دلاور بسی بود جنگ</p></div>
<div class="m2"><p>یکی جوشنستش ز چرم پلنگ</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>سلیحم نیامد برو کارگر</p></div>
<div class="m2"><p>بسی آزمودم بگرز و تبر</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>کنون آزمون را یکی کارزار</p></div>
<div class="m2"><p>بسازیم تا چون بود روزگار</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>گر ایدونک یزدان بود یارمند</p></div>
<div class="m2"><p>بگردد ببایست چرخ بلند</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>نه آن شهر ماند نه آن شهریار</p></div>
<div class="m2"><p>سرآید مگر بر من این کارزار</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>اگر دست رستم بود روز جنگ</p></div>
<div class="m2"><p>نسازم من ایدر فراوان درنگ</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>شوم تا بدان روی دریای چین</p></div>
<div class="m2"><p>بدو مانم این مرز توران زمین</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بدو شیده گفت ای خردمند شاه</p></div>
<div class="m2"><p>انوشه بدی تا بود تاج و گاه</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>ترا فر و برزست و مردانگی</p></div>
<div class="m2"><p>نژاد و دل و بخت و فرزانگی</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>نباید ترا پند آموزگار</p></div>
<div class="m2"><p>نگه کن بدین گردش روزگار</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>چو پیران و هومان و فرشیدورد</p></div>
<div class="m2"><p>چو کلباد و نستیهن شیر مرد</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>شکسته سلیح و گسسته دلند</p></div>
<div class="m2"><p>ز بیم و ز غم هر زمان بگسلند</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>تو بر باد این جنگ کشتی مران</p></div>
<div class="m2"><p>چو دانی که آمد سپاهی گران</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>ز شاهان گیتی گزیده توی</p></div>
<div class="m2"><p>جهانجوی و هم کار دیده توی</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>بجان و سر شاه توران سپاه</p></div>
<div class="m2"><p>بخورشید و ماه و بتخت و کلاه</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>که از کار کاموس و خاقان چین</p></div>
<div class="m2"><p>دلم گشت پر خون و سر پر ز کین</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>شب تیره بگشاد چشم دژم</p></div>
<div class="m2"><p>ز غم پشت ماه اندر آمد بخم</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>جهان گشت برسان مشک سیاه</p></div>
<div class="m2"><p>چو فرغار برگشت ز ایران سپاه</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>بیامد بنزدیک افراسیاب</p></div>
<div class="m2"><p>شب تیره هنگام آرام و خواب</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>چنین گفت کز بارگاه بلند</p></div>
<div class="m2"><p>برفتم سوی رستم دیوبند</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>سراپردهٔ سبز دیدم بزرگ</p></div>
<div class="m2"><p>سپاهی بکردار درنده گرگ</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>یکی اژدهافش درفشی بپای</p></div>
<div class="m2"><p>نه آرام دارد تو گفتی نه جای</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>فروهشته بر کوههٔ زین لگام</p></div>
<div class="m2"><p>بفتراک بر حلقهٔ خم خام</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>بخیمه درون ژنده پیلی ژیان</p></div>
<div class="m2"><p>میان تنگ بسته به ببر بیان</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>یکی بور ابرش به پیشش بپای</p></div>
<div class="m2"><p>تو گفتی همی اندر آید ز جای</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>سپهدار چون طوس و گودرز و گیو</p></div>
<div class="m2"><p>فریبرز و شیدوش و گرگین نیو</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>طلایه گرازست با گستهم</p></div>
<div class="m2"><p>که با بیژن گیو باشد بهم</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>غمی شد ز گفتار فرغار شاه</p></div>
<div class="m2"><p>کس آمد بر پهلوان سپاه</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>بیامد سپهدار پیران چو گرد</p></div>
<div class="m2"><p>بزرگان و مردان روز نبرد</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>ز گفتار فرغار چندی بگفت</p></div>
<div class="m2"><p>که تا کیست با او به پیکار جفت</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>بدو گفت پیران که ما را ز جنگ</p></div>
<div class="m2"><p>چه چارست جز جستن نام و ننگ</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>چو پاسخ چنین یافت افراسیاب</p></div>
<div class="m2"><p>گرفت اندران کینه جستن شتاب</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>بپیران بفرمود تا با سپاه</p></div>
<div class="m2"><p>بیاید بر رستم کینه‌خواه</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>ز پیش سپهبد به بیرون کشید</p></div>
<div class="m2"><p>همی رزم را سوی هامون کشید</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>خروش آمد از دشت و آوای کوس</p></div>
<div class="m2"><p>جهان شد ز گرد سپاه آبنوس</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>سپه بود چندانک گفتی جهان</p></div>
<div class="m2"><p>همی گردد از گرد اسپان نهان</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>تبیره زنان نعره برداشتند</p></div>
<div class="m2"><p>همی پیل بر پیل بگذاشتند</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>از ایوان بدشت آمد افراسیاب</p></div>
<div class="m2"><p>همی کرد بر جنگ جستن شتاب</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>بپیران بگفت آنچ بایست گفت</p></div>
<div class="m2"><p>که راز بزرگان بباید نهفت</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>یکی نامه نزدیک پولادوند</p></div>
<div class="m2"><p>بیارای وز رای بگشای بند</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>بگویش که ما را چه آمد بسر</p></div>
<div class="m2"><p>ازین نامور گرد پرخاشخر</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>اگر یارمندست چرخ بلند</p></div>
<div class="m2"><p>بیاید بدین دشت پولادوند</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>بسی لشکر از مرز سقلاب و چین</p></div>
<div class="m2"><p>نگونسار و حیران شدند اندرین</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>سپاهست برسان کوه روان</p></div>
<div class="m2"><p>سپهدارشان رستم پهلوان</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>سپهکش چو رستم سپهدار طوس</p></div>
<div class="m2"><p>بابر اندر اورده آوای کوس</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>چو رستم بدست تو گردد تباه</p></div>
<div class="m2"><p>نیابد سپهر اندرین مرز راه</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>همه مرز را رنج زویست و بس</p></div>
<div class="m2"><p>تو باش اندرین کار فریادرس</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>گر او را بدست تو آید زمان</p></div>
<div class="m2"><p>شود رام روی زمین بی‌گمان</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>من از پادشاهی آباد خویش</p></div>
<div class="m2"><p>نه برگیرم از رنج یک رنج بیش</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>دگر نیمه دیهیم و گنج آن تست</p></div>
<div class="m2"><p>که امروز پیگار و رنج آن تست</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>نهادند بر نامه بر مهر شاه</p></div>
<div class="m2"><p>چو برزد سر از برج خرچنگ ماه</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>کمر بست شیده ز پیش پدر</p></div>
<div class="m2"><p>فرستاده او بود و تیمار بر</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>بکردار آتش ز بیم گزند</p></div>
<div class="m2"><p>بیامد بنزدیک پولادوند</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>برو آفرین کرد و نامه بداد</p></div>
<div class="m2"><p>همه کار رستم برو کرد یاد</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>که رستم بیامد ز ایران بجنگ</p></div>
<div class="m2"><p>ابا او سپاهی بسان پلنگ</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>ببند اندر آورد کاموس را</p></div>
<div class="m2"><p>چو خاقان و منشور و فرطوس را</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>اسیران بسیار و پیلان رمه</p></div>
<div class="m2"><p>فرستاد یکسر بایران همه</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>کنارنگ و جنگ‌آوران را بخواند</p></div>
<div class="m2"><p>ز هر گونه‌ای داستانها براند</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>بدیشان بگفت انچ در نامه بود</p></div>
<div class="m2"><p>جهانگیر برنا و خودکامه بود</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>بفرمود تا کوس بیرون برند</p></div>
<div class="m2"><p>سراپردهٔ او به هامون برند</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>سپاه انجمن شد بکردار دیو</p></div>
<div class="m2"><p>برآمد ز گردان لشکر غریو</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>درفش از پس و پیش پولادوند</p></div>
<div class="m2"><p>سپردار با ترکش و با کمند</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>فرود آمد از کوه و بگذاشت آب</p></div>
<div class="m2"><p>بیامد بنزدیک افراسیاب</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>پذیره شدندش یکایک سپاه</p></div>
<div class="m2"><p>تبیره برآمد ز درگاه شاه</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>ببر در گرفتش جهاندیده مرد</p></div>
<div class="m2"><p>ز کار گذشته بسی یاد کرد</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>بگفت آنک تیمار ترکان ز کیست</p></div>
<div class="m2"><p>سرانجام درمان این کار چیست</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>خرامان بایوان خسرو شدند</p></div>
<div class="m2"><p>برای و باندیشهٔ نو شدند</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>سخن راند هر گونه افراسیاب</p></div>
<div class="m2"><p>ز کار درنگ و ز بهر شتاب</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>ز خون سیاوش که بر دست اوی</p></div>
<div class="m2"><p>چه آمد ز پرخاش وز گفت و گوی</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>ز خاقان و منشور و کاموس گرد</p></div>
<div class="m2"><p>گذشته سخنها همه برشمرد</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>بگفت آنک این رنجم از یک تنست</p></div>
<div class="m2"><p>که او را پلنگینه پیراهنست</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>نیامد سلیحم بدو کارگر</p></div>
<div class="m2"><p>بران ببر و آن خود و چینی سپر</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>بیابان سپردی و راه دراز</p></div>
<div class="m2"><p>کنون چارهٔ کار او را بساز</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>پر اندیشه شد جان پولادوند</p></div>
<div class="m2"><p>که آن بند را چون شود کاربند</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>چنین داد پاسخ بافراسیاب</p></div>
<div class="m2"><p>که در جنگ چندین نباید شتاب</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>گر آنست رستم که مازندران</p></div>
<div class="m2"><p>تبه کرد و بستد بگرز گران</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>بدرید پهلوی دیو سپید</p></div>
<div class="m2"><p>جگرگاه پولاد غندی و بید</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>مرا نیست پایاب با جنگ اوی</p></div>
<div class="m2"><p>نیارم ببد کردن آهنگ اوی</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>تن و جان من پیش رای تو باد</p></div>
<div class="m2"><p>همیشه خرد رهنمای تو باد</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>من او را بر اندیشه دارم بجنگ</p></div>
<div class="m2"><p>بگردش بگردم بسان پلنگ</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>تو لشکر برآغال بر لشکرش</p></div>
<div class="m2"><p>بانبوه تا خیره گردد سرش</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>مگر چاره سازم و گر نی بدست</p></div>
<div class="m2"><p>بر و یال او را نشاید شکست</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>ازو شاد شد جان افراسیاب</p></div>
<div class="m2"><p>می روشن آورد و چنگ و رباب</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>بدانگه که شد مست پولادوند</p></div>
<div class="m2"><p>چنین گفت با او ببانگ بلند</p></div></div>
<div class="b" id="bn245"><div class="m1"><p>که من بر فریدون و ضحاک و جم</p></div>
<div class="m2"><p>خور و خواب و آرام کردم دژم</p></div></div>
<div class="b" id="bn246"><div class="m1"><p>برهمن بترسد ز آواز من</p></div>
<div class="m2"><p>وزین لشکر گردن‌افراز من</p></div></div>
<div class="b" id="bn247"><div class="m1"><p>من این زابلی را بشمشیر تیز</p></div>
<div class="m2"><p>برآوردگه بر کنم ریز ریز</p></div></div>