---
title: >-
    بخش ۱۷
---
# بخش ۱۷

<div class="b" id="bn1"><div class="m1"><p>چو بهرام و خسرو به هامون شدند</p></div>
<div class="m2"><p>بر شیر با دل پر از خون شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خسرو بدید آن دو شیر ژیان</p></div>
<div class="m2"><p>نهاده یکی افسر اندر میان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان موبدان گفت تاج از نخست</p></div>
<div class="m2"><p>مر آن را سزاتر که شاهی بجست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و دیگر که من پیرم و او جوان</p></div>
<div class="m2"><p>به چنگال شیر ژیان ناتوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بران بد که او پیش‌دستی کند</p></div>
<div class="m2"><p>به برنایی و تن‌درستی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو گفت بهرام کری رواست</p></div>
<div class="m2"><p>نهانی نداریم گفتار راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی گرزه گاوسر برگرفت</p></div>
<div class="m2"><p>جهانی بدو مانده اندر شگفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو گفت موبد که ای پادشا</p></div>
<div class="m2"><p>خردمند و بادانش و پارسا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی جنگ شیران که فرمایدت</p></div>
<div class="m2"><p>جز از تاج شاهی چه افزایدت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو جان از پی پادشاهی مده</p></div>
<div class="m2"><p>خورش بی‌بهانه به ماهی مده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه بی‌گناهیم و این کار تست</p></div>
<div class="m2"><p>جهان را همه دل به بازار تست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدو گفت بهرام کای دین‌پژوه</p></div>
<div class="m2"><p>تو زین بی‌گناهی و دیگر گروه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم‌آورد این نره شیران منم</p></div>
<div class="m2"><p>خریدار جنگ دلیران منم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدو گفت موبد به یزدان پناه</p></div>
<div class="m2"><p>چو رفتی دلت را بشوی از گناه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان کرد کو گفت بهرامشاه</p></div>
<div class="m2"><p>دلش پاک شد توبه کرد از گناه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی رفت با گرزهٔ گاوروی</p></div>
<div class="m2"><p>چو دیدند شیران پرخاشجوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی زود زنجیر بگسست و بند</p></div>
<div class="m2"><p>بیامد بر شهریار بلند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزد بر سرش گرز بهرام گرد</p></div>
<div class="m2"><p>ز چشمش همی روشنایی ببرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر دیگر آمد بزد بر سرش</p></div>
<div class="m2"><p>فرو ریخت از دیده خون از برش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهاندار بنشست بر تخت عاج</p></div>
<div class="m2"><p>به سر بر نهاد آن دلفروز تاج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به یزدان پناهید کو بد پناه</p></div>
<div class="m2"><p>نمایندهٔ راه گم کرده راه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بشد خسرو و برد پیشش نماز</p></div>
<div class="m2"><p>چنین گفت کای شاه گردن‌فراز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشست تو بر گاه فرخنده باد</p></div>
<div class="m2"><p>یلان جهان پیش تو بنده باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو شاهی و ما بندگان توایم</p></div>
<div class="m2"><p>به خوبی فزایندگان توایم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزرگان برو گوهر افشاندند</p></div>
<div class="m2"><p>بران تاج نو آفرین خواندند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز گیتی برآمد سراسر خروش</p></div>
<div class="m2"><p>در آذر بد این جشن روز سروش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برآمد یکی ابر و شد تیره‌ماه</p></div>
<div class="m2"><p>همی تیر بارید ز ابر سیاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه دریا پدید و نه دشت و نه راغ</p></div>
<div class="m2"><p>نبینم همی در هوا پر زاغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حواصل فشاند هوا هر زمان</p></div>
<div class="m2"><p>چه سازد همی زین بلند آسمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نماندم نمکسود و هیزم نه جو</p></div>
<div class="m2"><p>نه چیزی پدیدست تا جودرو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدین تیرگی روز و بیم خراج</p></div>
<div class="m2"><p>زمین گشته از برف چون کوه عاج</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه کارها را سراندر نشیب</p></div>
<div class="m2"><p>مگر دست گیرد حسین قتیب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنون داستانی بگویم شگفت</p></div>
<div class="m2"><p>کزان برتر اندازه نتوان گرفت</p></div></div>