---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>وزان پس غم و شادی یزدگرد</p></div>
<div class="m2"><p>چنان گشت بر پور چون باد ارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برین نیز چندی زمان برگذشت</p></div>
<div class="m2"><p>به ایران پدر پور فرخ به دشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شاهی پراندیشه شد یزدگرد</p></div>
<div class="m2"><p>ز هر کشوری موبدان کرد گرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اخترشناسان بفرمود شاه</p></div>
<div class="m2"><p>که تا کردهر یک به اختر نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که تا کی بود در جهان مرگ اوی</p></div>
<div class="m2"><p>کجا تیره گردد سر و ترگ اوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه باشد کجا باشد آن روزگار</p></div>
<div class="m2"><p>که پژمرده گردد گل شهریار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ستاره‌شمر گفت کاین خود مباد</p></div>
<div class="m2"><p>که شاه جهان گیرد از مرگ یاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بخت شهنشاه بدرو شود</p></div>
<div class="m2"><p>از ایدر سوی چشمهٔ سو شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فراز آورد لشکر و بوق و کوس</p></div>
<div class="m2"><p>به شادی نظاره شود سوی طوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر آن جایگه بر بود هوش اوی</p></div>
<div class="m2"><p>چو این راز بگذشت بر گوش اوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازین دانش ار یادگیری به دست</p></div>
<div class="m2"><p>که این راز در پردهٔ ایزدست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بشنید زو شاه سوگند خورد</p></div>
<div class="m2"><p>به خراد برزین و خورشید زرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که من چشمهٔ سو نبینم به چشم</p></div>
<div class="m2"><p>نه هنگام شادی نه هنگام خشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برین نیز برگشت گردون سه ماه</p></div>
<div class="m2"><p>زمانه به جوش آمد از خون شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بیدادگر شد شبان با رمه</p></div>
<div class="m2"><p>بدو بازگردد بدیها همه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بینیش بگشاد یک روز خون</p></div>
<div class="m2"><p>پزشک آمد از هر سوی رهنمون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به دارو چو یک هفته بستی پزشک</p></div>
<div class="m2"><p>دگر هفته خون آمدی چون سرشک</p></div></div>