---
title: >-
    بخش ۵
---
# بخش ۵

<div class="b" id="bn1"><div class="m1"><p>دگر هفته با لشکری سرفراز</p></div>
<div class="m2"><p>به نخچیرگه رفت با یوز و باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برابر ز کوهی یکی شیر دید</p></div>
<div class="m2"><p>کجا پشت گوری همی بر درید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآورد زاغ سیه را بزه</p></div>
<div class="m2"><p>به تندی به شست سه‌پر زد گره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل گور بردوخت با پشت شیر</p></div>
<div class="m2"><p>پر از خون هژبر از بر و گور زیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو او گور و شیر دلاور بکشت</p></div>
<div class="m2"><p>به ایوان خرامید تیغی به مشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر هفته نعمان و منذر به راه</p></div>
<div class="m2"><p>همی رفت با او به نخچیرگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی نامور برده از تازیان</p></div>
<div class="m2"><p>کزیشان بدی راه سود و زیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی خواست منذر که بهرام گور</p></div>
<div class="m2"><p>بدیشان نماید سواری و زور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شترمرغ دیدند جایی گله</p></div>
<div class="m2"><p>دوان هر یکی چون هیونی یله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بهرام‌گور آن شترمرغ دید</p></div>
<div class="m2"><p>به کردار باد هوا بردمید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمان را بمالید خندان به چنگ</p></div>
<div class="m2"><p>بزد بر کمر چار تیر خدنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکایک همی راند اندر کمان</p></div>
<div class="m2"><p>بدان تا سرآرد بریشان زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی برشکافید پرشان به تیر</p></div>
<div class="m2"><p>بدین سان زند مرد نخچیرگیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به یک سوزن این زان فزون‌تر نبود</p></div>
<div class="m2"><p>همان تیر زین تیر برتر نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برفت و بدید آنک بد نامدار</p></div>
<div class="m2"><p>به یک موی‌بر بود زخم سوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همی آفرین خواند منذر بدوی</p></div>
<div class="m2"><p>همان نیزه‌داران پرخاشجوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو گفت منذر که ای شهریار</p></div>
<div class="m2"><p>بتو شادمانم چو گلبن به بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مبادا که خم آورد ماه تو</p></div>
<div class="m2"><p>وگر سست گردد کمرگاه تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم‌انگه چون منذر به ایوان رسید</p></div>
<div class="m2"><p>ز بهرام رایش به کیوان رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فراوان مصور بجست از یمن</p></div>
<div class="m2"><p>شدند این سران بر درش انجمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بفرمود تا زخم او را به تیر</p></div>
<div class="m2"><p>مصور نگاری کند بر حریر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سواری چو بهرام با یال و کفت</p></div>
<div class="m2"><p>بلند اشتری زیر و زخمی شگفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمان مهره و شیر و آهو و گور</p></div>
<div class="m2"><p>گشاده بر و چربه دستی به زور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شترمرغ و هامون و آن زخم تیر</p></div>
<div class="m2"><p>ز قیر سیه تازه شد بر حریر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سواری برافگند زی شهریار</p></div>
<div class="m2"><p>فرستاد نزدیک او آن نگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرستاده چون شد بر یزدگرد</p></div>
<div class="m2"><p>همه لشکر آمد بران نامه گرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه نامداران فروماندند</p></div>
<div class="m2"><p>به بهرام بر آفرین خواندند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وزان پس هنرها چو کردی به کار</p></div>
<div class="m2"><p>همی تاختندی بر شهریار</p></div></div>