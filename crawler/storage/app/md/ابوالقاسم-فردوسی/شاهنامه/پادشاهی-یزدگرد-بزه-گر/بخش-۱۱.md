---
title: >-
    بخش ۱۱
---
# بخش ۱۱

<div class="b" id="bn1"><div class="m1"><p>پس آگاهی آمد به بهرام گور</p></div>
<div class="m2"><p>که از چرخ شد تخت را آب شور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدرت آن سرافراز شاهان بمرد</p></div>
<div class="m2"><p>به مرد و همه نام شاهی ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی مرد بر گاه بنشاندند</p></div>
<div class="m2"><p>به شاهی همی خسروش خواندند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخوردند سوگند یکسر سپاه</p></div>
<div class="m2"><p>کزان تخمه هرگز نخواهیم شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که بهرام فرزند او همچو اوست</p></div>
<div class="m2"><p>از آب پدر یافت او مغز و پوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بشنید بهرام رخ را بکند</p></div>
<div class="m2"><p>ز مرگ پدر شد دلش مستمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآمد دو هفته ز شهر یمن</p></div>
<div class="m2"><p>خروشیدن کودک و مرد و زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو یک ماه بنشست با سوک شاه</p></div>
<div class="m2"><p>سر ماه نو را بیاراست گاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برفتند نعمان و منذر بهم</p></div>
<div class="m2"><p>همه تازیان یمن بیش و کم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه زار و با شاه گریان شدند</p></div>
<div class="m2"><p>ابی آتش از درد بریان شدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبان برگشادند زان پس ز بند</p></div>
<div class="m2"><p>که ای پرهنر شهریار بلند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه در جهان خاک را آمدیم</p></div>
<div class="m2"><p>نه جویای تریاک را آمدیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بمیرد کسی کو ز مادر بزاد</p></div>
<div class="m2"><p>زهش چون ستم بینم و مرگ داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به منذر چنین گفت بهرام گور</p></div>
<div class="m2"><p>که اکنون چو شد روز ما تار و تور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازین تخمه گر نام شاهنشهی</p></div>
<div class="m2"><p>گسسته شود بگسلد فرهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز دشت سواران برآرند خاک</p></div>
<div class="m2"><p>شود جای بر تازیان بر مغاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پراندیشه باشید و یاری کنید</p></div>
<div class="m2"><p>به مرگ پدر سوگواری کنید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بهرام بشنید منذر سخن</p></div>
<div class="m2"><p>به مردی یکی پاسخ افگند بن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین گفت کاین روزگار منست</p></div>
<div class="m2"><p>برین دشت روز شکار منست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو بر تخت بنشین و نظاره باش</p></div>
<div class="m2"><p>همه ساله با تاج و با یاره باش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه نامداران برین هم‌سخن</p></div>
<div class="m2"><p>که نعمان و منذر فگندند بن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز پیش جهانجوی برخاستند</p></div>
<div class="m2"><p>همه تاختن را بیاراستند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بفرمود منذر به نعمان که رو</p></div>
<div class="m2"><p>یکی لشکری ساز شیران نو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز شیبان و از قیسیان ده هزار</p></div>
<div class="m2"><p>فرازآر گرد از در کارزار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من ایرانیان را نمایم که شاه</p></div>
<div class="m2"><p>کدامست با تاج و گنج و سپاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیاورد نعمان سپاهی گران</p></div>
<div class="m2"><p>همه تیغ‌داران و نیزه‌وران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بفرمود تا تاختنها برند</p></div>
<div class="m2"><p>همه روی کشور به پی بسپرند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ره شورستان تا در طیسفون</p></div>
<div class="m2"><p>زمین خیره شد زیر نعل اندرون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زن و کودک و مرد بردند اسیر</p></div>
<div class="m2"><p>کس آن رنجها را نبد دستگیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پر از غارت و سوختن شد جهان</p></div>
<div class="m2"><p>چو بیکار شد تخت شاهنشهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس آگاهی آمد به روم و به چین</p></div>
<div class="m2"><p>به ترک و به هند و به مکران زمین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که شد تخت ایران ز خسرو تهی</p></div>
<div class="m2"><p>کسی نیست زیبای شاهنشهی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه تاختن را بیاراستند</p></div>
<div class="m2"><p>به بیدادی از جای برخاستند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو از تخم شاهنشهان کس نبود</p></div>
<div class="m2"><p>که یارست تخت کیی را بسود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به ایران همی هرکسی دست آخت</p></div>
<div class="m2"><p>به شاهنشهی تیز گردن فراخت</p></div></div>